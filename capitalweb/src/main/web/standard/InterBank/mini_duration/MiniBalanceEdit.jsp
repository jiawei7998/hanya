<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title></title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
<!-- <fieldset class="mini-filedset">
	<legend>结清合同信息</legend> -->
<span><a href="javascript:CommonUtil.activeTab();">结清</a> >> 结清列表信息</span>
<div id="BalanceManager" class="fieldset-body">
    <table id="filed_form" class="mini-table" style="width:100%">
        <!-- 第一行 -->
        <tr>
            <td>
                <input id="refNo" name="refNo" class="mini-textbox" labelField="true" enabled="false" label="原交易单号："
                       style="width:85%;" labelStyle="width:160px;"/>
            </td>
            <td>
                <input id="cName" name="cName" class="mini-textbox" labelField="true" enabled="false" label="交易对手名称："
                       style="width:80%;" labelStyle="width:160px;"/>
                <input class="mini-hidden" id="dealNo" name="dealNo"/>
                <input class="mini-hidden" id="version" name="version"/>
                <input class="mini-hidden" id="cNo" name="cNo"/>
                <input class="mini-hidden" id="aDate" name="aDate"/>
            </td>
            <td>
                <input id="prdName" name="prdName" class="mini-textbox" labelField="true" enabled="false" label="产品名称："
                       style="width:80%;" labelStyle="width:160px;"/>
            </td>
        </tr>
        <!-- 第二行 -->
        <tr>
            <td>
                <input id="prdTypeName" name="prdTypeName" class="mini-textbox" labelField="true" enabled="false"
                       label="产品种类：" style="width:85%;" labelStyle="width:160px;"/><input class='mini-hidden'
                                                                                          id='prdType' name='prdType'/>
            </td>
            <td>
                <input id="amt" name="amt" class="mini-spinner" labelField="true" enabled="false"
                       maxValue="100000000000" format="n2" label="原交易本金(元)：" style="width:80%;"
                       labelStyle="width:160px;"/>
            </td>
            <td>
                <input id="sponsorName" name="sponsorName" class="mini-textbox" labelField="true" enabled="false"
                       label="原交易发起人：" style="width:80%;" labelStyle="width:160px;"/>
                <input id="sponsor" name="sponsor" class="mini-hidden"/>
            </td>

        </tr>
        <!-- 第三行 -->
        <tr>
            <td>
                <input id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true" enabled="false"
                       label="原交易发起机构：" style="width:85%;" labelStyle="width:160px;"/>
                <input id="sponInst" name="sponInst" class="mini-hidden"/>
            </td>
            <td>
                <input id="rateType" name="rateType" class="mini-combobox"
                       data="CommonUtil.serverData.dictionary.rateType" labelField="true" enabled="false" label="利率类型："
                       style="width:80%;" labelStyle="width:160px;"/>
            </td>
            <td>
                <input id="vDate" name="vDate" class="mini-datepicker" labelField="true" enabled="false" label="起息日期："
                       style="width:80%;" labelStyle="width:160px;"/>
            </td>

        </tr>

        <!-- 第四行 -->
        <tr>
            <td>
                <input id="mDate" name="mDate" class="mini-datepicker" labelField="true" enabled="false" label="到期日期："
                       style="width:85%;" labelStyle="width:160px;"/>
            </td>
            <td>
                <input id="acturalRate" name="acturalRate" class="mini-spinner" labelField="true" enabled="false"
                       format="p4" label="交易利率(%)：" style="width:80%;" labelStyle="width:160px;"/>
            </td>
            <td>
                <input id="orgiMdate" name="orgiMdate" class="mini-datepicker" labelField="true" enabled="false"
                       label="原业务到期日期：" style="width:80%;" labelStyle="width:160px;"/>
            </td>
        </tr>
        <!-- 第五行 -->
        <tr>
            <td>
                <input id="endDate" name="endDate" class="mini-datepicker" labelField="true" enabled="false"
                       label="结清日期：" style="width:85%;" labelStyle="width:160px;"/>
                <input id="dealDate" name="dealDate" class="mini-hidden" labelField="true" enabled="false" label="交易日期："
                       style="width:85%;" labelStyle="width:160px;"/>
            </td>
            <td>
                <input id="remAmt" name="remAmt" class="mini-spinner" labelField="true" maxValue="100000000000"
                       format="n2" enabled="false" label="剩余本金：" style="width:80%;" labelStyle="width:160px;"/>
            </td>

        </tr>
        <tr>
            <td colspan="3" style="text-align:center;">
                <a onclick="save()" id="save_btn" class="mini-button" style="display: none">保存</a>
                <a id="cancel_btn" class="mini-button" style="display: none" onclick="close()">关闭</a>
            </td>
        </tr>


    </table>

</div>
<!-- </fieldset> -->
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp" %>
<!-- <div  class="miniui-elem-quote" style="margin-bottom:70px" >
		<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
</div> -->

<script>
    mini.parse();
    var _bizDate = '<%=__bizDate %>';
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    var row = top["BalanceManager"].getData(action);
    var form = new mini.Form("#filed_form");

    var tradeData = {};
    tradeData.selectData = row;
    tradeData.task_id = row.taskId;
    tradeData.serial_no = row.dealNo;
    tradeData.operType = action;

    //关闭表单
    function close() {
        top["win"].closeMenuTab();
    }

    function initParam() {
        if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
            var param = {};
            param['dealNo'] = row.refNo;
            CommonUtil.ajax({
                url: "/TdBalanceController/getTdProductAppMain",
                data: mini.encode(param),
                callback: function (data) {
                    form.setData(row);
                    mini.get("refNo").setValue(data.dealNo);
                    mini.get("cName").setValue(data.counterParty.party_name);
                    mini.get("cNo").setValue(data.counterParty.party_id);
                    mini.get("prdName").setValue(data.product.prdName);
                    mini.get("prdTypeName").setValue(data.product.prdTypeName);
                    mini.get("prdType").setValue(data.product.prdType);
                    mini.get("amt").setValue(data.amt);
                    mini.get("rateType").setValue(data.rateType);
                    mini.get("dealDate").setValue(data.dealDate);
                    mini.get("vDate").setValue(data.vDate);
                    mini.get("mDate").setValue(data.mDate);
                    mini.get("endDate").setValue(row.endDate);
                    mini.get("sponsor").setValue(data.user.userId);
                    mini.get("sponInst").setValue(data.institution.instId);
                    mini.get("sponsorName").setValue(data.user.userName);
                    mini.get("sponInstName").setValue(data.institution.instName);
                    mini.get("version").setValue(data.version);
                    mini.get("dealNo").setValue(row.dealNo);
                    mini.get("acturalRate").setValue(data.contractRate);
                }
            });

        } else {
            var data = {dealNo: row.dealNo};
            CommonUtil.ajax({
                url: "/ProductApproveController/searchProductApproveForDuration",
                data: mini.encode(data),
                callback: function (data) {
                    form.setData(data.obj);
                    mini.get("dealDate").setValue(_bizDate);
                    mini.get("orgiMdate").setValue(data.obj.mDate);
                    mini.get("refNo").setValue(data.obj.dealNo);
                    mini.get("cName").setValue(data.obj.counterParty.party_name);
                    mini.get("prdName").setValue(data.obj.product.prdName);
                    mini.get("prdTypeName").setValue(data.obj.product.prdTypeName);
                    mini.get("prdType").setValue(data.obj.product.prdType);
                    mini.get("amt").setValue(data.obj.amt);
                    mini.get("sponsor").setValue(data.obj.user.userId);
                    mini.get("endDate").setValue(_bizDate);
                    mini.get("sponInst").setValue(data.obj.institution.instId);
                    mini.get("sponsorName").setValue(data.obj.user.userName);
                    mini.get("sponInstName").setValue(data.obj.institution.instName);
                    mini.get("version").setValue(data.obj.version);
                    mini.get("acturalRate").setValue(data.obj.contractRate);
                    mini.get("remAmt").setValue(data.obj.tpos.settlAmt);

                    mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
                    mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
                    mini.get("aDate").setValue("<%=__bizDate %>");
                }

            });

        }

    }

    //保存
    function save() {
        form.validate();
        if (form.isValid == false) {
            mini.alert("请检查输入是否正确", "系统提示");
            return;
        }

        var endDate = mini.get("endDate");
        var mDate = mini.get("mDate");
        if (_bizDate > endDate) {
            mini.alert("结清日期必须大于等于系统时间", "系统提示");
            return false;
        }
        /* if (_bizDate<mDate) {
          mini.alert("该笔业务还未到期","系统提示");
          return false;
      }   */
        var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
        var data = form.getData(true, false);
        if (form.isValid() == false) {
            //mini.alert("hahaha !","提示信息")
            return;
        }
        data['trdtype'] = "3001_DEALEND";  //业务类型
        data['dealNo'] = '';
        var dealType = data.dealType;
        if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
            dealType = row.dealType;
            data.trade_id = row.dealNo;
        }
        var saveUrl = "/TradeManageController/saveTrade";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.hideMessageBox(messageid);
                mini.alert("保存成功", "提示", function () {
                    setTimeout(function () {
                        top["win"].closeMenuTab()
                    }, 10);
                });

            }
        });
    }

    $(document).ready(function () {
        if ($.inArray(action, ["approve", "detail"]) > -1) {
            mini.get("save_btn").setVisible(false);
            form.setEnabled(false);
        }
        initParam();
    });

</script>
<script type="text/javascript"
        src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>