<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<span id="labell"><a href="javascript:CommonUtil.activeTab();">收息计划</a> >> 修改</span>
<fieldset class="mini-fieldset">
<legend>交易详情</legend>
<div id="TrasForProductInterestScheduleEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td>
			<input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:110px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="aDate" name="aDate">
			<input class="mini-hidden" id="version" name="version">
			<input class="mini-hidden" id="effectDate" name="effectDate"/>
			</td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  maxValue="9999999999999"  format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
		</tr>
		<tr>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="acturalRate" name="acturalRate" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  format="p4" enabled="false" label="投资利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tfoot>
			<tr>
				<td><input id="mDate" name="mDate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="intFre" name="intFre" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.CouponFrequently"  enabled="false" label="原交易收息频率" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="remainAmt" name="remainAmt" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  maxValue="9999999999999"  format="n2" enabled="false" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td colspan="3"><input id="changeReason" name="changeReason" required="true"  vtype="maxLength:500" class="mini-textarea"  labelField="true"  label="收息调整说明" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
		</tfoot>
	</table>
</div>
</fieldset>
<fieldset class="mini-fieldset">
<legend>收息计划调整</legend>
<div id="InterestSchedule_from_div">
	<input id="dayCounter" name="dayCounter" class="mini-hidden">
	<table class='mini-table' id="InterestSchedule" name="InterestSchedule" style="width:100%">
		<tr>
			<td><input id="cfType" name="cfType" class="mini-combobox" value="Interest" style="width:80%;" labelField="true" data="CommonUtil.serverData.dictionary.cfType"  enabled="false" label="现金流类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="executeRate" name="executeRate" class="mini-spinner" style="width:80%;"  changeOnMousewheel='false' enabled="false"  format="p4"  labelField="true" required="true"   label="区间利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="interestAmt" name="interestAmt" class="mini-spinner" style="width:80%;"  changeOnMousewheel='false' enabled="false" labelField="true" required="true"  maxValue="9999999999999"  format="n2"   label="收息金额（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="refBeginDate" name="refBeginDate" class="mini-datepicker" enabled="false" style="width:80%;" labelField="true"  required="true"  label="区间起始日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="refEndDate" name="refEndDate" class="mini-datepicker" onValuechanged="refEndDateValuechanged" style="width:80%;" labelField="true"  required="true"  label="区间结束日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="theoryPaymentDate" name="theoryPaymentDate" class="mini-datepicker" style="width:80%;" labelField="true"  required="true"  label="收息日期（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td colspan="3" >
				<div  class="mini-toolbar" style="text-align:left">
					<a  id="update_btn" class="mini-button" style="display: none"   onclick="update()">修改</a>
					<a  id="reset_btn" class="mini-button" style="display: none"    onclick="reset()">重置</a>
				</div>
			</td>
		</tr>
	</table>
</div>
</fieldset>
<fieldset style="min-width:0;">
<legend>收息计划详情</legend>
<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:300px;" 
	allowResize="true"  border="true" sortMode="client" showPager="true"
	pageSize="10" sizeList="[10,20]" multiSelect="true" 
	allowCellEdit="true" allowCellSelect="true" multiSelect="false" onrowclick="onRowClick" >
	<div property="columns">
		<div type="indexcolumn"     headerAlign="center"  allowSort="false" width="40" >序号</div>
		<div field="cfType" headerAlign= "center" allowSort= "true" width="120" renderer="CommonUtil.dictRenderer" data-options="{dict:'cfType'}" align="center">现金流类型</div> 
		<div field="executeRate"  headerAlign= "center" allowSort= "true" width="120" align = "right"  numberFormat="p4">区间利率</div>
		<div field="refBeginDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">区间起始日期</div>
		<div field="refEndDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">区间结束日期</div>
		<div field="theoryPaymentDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">收息日期（计划）</div>
		<div field="interestAmt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00">收息金额（计划）</div>
		<div field="actualDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">收息日期（实际）</div>
		<div field="actualRamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00" >收息金额（实际）</div>
	</div>
</div>
</fieldset>
<table class="mini-form" width="100%" cols="4">
	<tr>
		<td colspan="4" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="red">收息调整说明:</font></span><BR/>
			&nbsp;<span>已收息过的计划不允许调整，调整的计划必须首尾相接;</span><BR/>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">

	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action = CommonUtil.getParam(url,"action");
	var _bizDate = '<%=__bizDate %>';
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;

	var form = new mini.Form("#field_form");

	var grid = mini.get("grid1");
	var dataResult = null;

	grid.on("beforeload", function (e){
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function search(pageSize,pageIndex)
	{
		if(CommonUtil.isNull(dataResult)){
			dataResult = [];
		}
		var totalCount = dataResult.length;
		var data = dataResult;
        var arr = [];
        var start = pageIndex * pageSize, end = start + pageSize;
		for (var i = start, l = end; i < l; i++) 
		{
            var record = data[i];
            if (!record) continue;
            arr.push(record);
        }
		grid.setTotalCount(totalCount);
		grid.setPageIndex(pageIndex);
		grid.setPageSize(pageSize);
		grid.setData(arr);
	}// end search

	function reset(){
		mini.confirm("重置后未保存的计划将被清除,确认重置收息计划吗？","系统提示",function(action){
			if(action == 'ok'){
				loadData();
			}
		});
	}

	function refEndDateValuechanged(e){
		mini.get("theoryPaymentDate").setValue(mini.get("refEndDate").getFormValue());
	}

	//加载数据
	function loadData(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			CommonUtil.ajax({
				url:"/tmCalCashFlowController/getApproveInterestForApprove",
				data:mini.encode({dealNo : row.dealNo,dealType:row.dealType}),
				callback : function(data) {
					data = mini.decode(data);
					dataResult = data.obj;
					search(grid.pageSize,0);
				}
			});

		}else{
			CommonUtil.ajax({
				url:"/tmCalCashFlowController/getInterestCashFlowForApprove",
				data:mini.encode({dealNo : row.dealNo, dealType:row.dealType}),
				callback : function(data) {
					data = mini.decode(data);
					dataResult = data.obj;
					search(grid.pageSize,0);
				}
			});
		}
	}


	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>收息计划</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("refNo").setValue( data.dealNo);
					mini.get("cName").setValue( data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue( data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("rateType").setValue( data.rateType);
					mini.get("vDate").setValue( data.vDate);
					mini.get("mDate").setValue( data.mDate);
					mini.get("sponsor").setValue( data.user.userId);
					mini.get("sponInst").setValue(data.institution.instId);
					mini.get("sponsorName").setValue( data.user.userName);
					mini.get("sponInstName").setValue( data.institution.instName);
					mini.get("version").setValue(data.version);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("acturalRate").setValue(  data.contractRate);
					mini.get("effectDate").setValue(  row.effectDate);
					mini.get("changeReason").setValue(  row.changeReason);
					mini.get("intFre").setValue( data.intFre);
				}
			});
			/******************************/
			loadData();

		}else {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>收息计划</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveForDuration",
				data:mini.encode(data),
				callback : function(data) {
					form.setData(data.obj);
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("aDate").setValue(_bizDate);

					mini.get("effectDate").setValue( _bizDate);
					mini.get("dealNo").setValue('');
					mini.get("refNo").setValue(data.obj.dealNo);
					mini.get("cName").setValue(data.obj.counterParty.party_name);
					mini.get("cNo").setValue(data.obj.counterParty.party_id);
					mini.get("prdName").setValue(data.obj.product.prdName);
					
					mini.get("sponsorName").setValue(data.obj.user.userName);
					mini.get("sponInstName").setValue(data.obj.institution.instName);
					mini.get("version").setValue(data.obj.version);
					mini.get("acturalRate").setValue( data.obj.contractRate);
					mini.get("originalAmt").setValue( data.obj.amt);
					mini.get("remainAmt").setValue(row.settlAmt);
							
				}
			});

			/******************************/
			loadData();
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		if(grid.data.length <= 0){
			mini.alert("请先新增交易至列表中!","提示");
			return;
		}
		//收息计划需满足条件
		var param = form.getData(true);
		var vDate = mini.get("vDate").getFormValue();//起息日
		var mDate = mini.get("mDate").getFormValue();//到期日
		var intdata = dataResult;
		for(var i = 0; i < intdata.length; i++){
			if(i < intdata.length - 1){
				if(CommonUtil.stampToDate(intdata[i].refEndDate) != CommonUtil.stampToDate(intdata[i+1].refBeginDate)){
					mini.alert("区间计划必须首尾相接(上一区间结束日和下一区间起始日需相同)!","提示");
					return false;
				}
			}
			if(CommonUtil.stampToDate(intdata[i].refBeginDate) < vDate 
				|| CommonUtil.stampToDate(intdata[i].refBeginDate) > mDate)
			{
				mini.alert("区间起始日应在起息日和到期日之间!","提示");
				return false;
			}
			if(CommonUtil.stampToDate(intdata[i].refEndDate) < vDate
				|| CommonUtil.stampToDate(intdata[i].refEndDate) > mDate){
				mini.alert("区间结束日应在起息日和到期日之间!","提示");
				return false;
			}
			if(CommonUtil.stampToDate(intdata[i].theoryPaymentDate) < vDate
				|| CommonUtil.stampToDate(intdata[i].theoryPaymentDate) > mDate){
				mini.alert("计划收息日应在起息日和到期日之间!","提示");
				return false;
			}
		}
		var remainAmt = mini.get("remainAmt").getValue();
		var param = form.getData(true);
		param['trdtype']="3001_CFCHANGE_INT";
		param['approveInterests'] = intdata;
		
		var saveUrl = "/ApproveManageController/saveApprove";
		if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		}
		var params=mini.encode(param);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}
	//添加收息计划
	function add(){
		var addform = new mini.Form("InterestSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		};
		var row = addform.getData(true);
		var param=form.getData(true);
		if(row.refBeginDate<param.vDate){
			mini.alert("区间起始日期不能小于起息日.","提示");
			return;
		}
		if(row.refEndDate<row.refBeginDate||row.refBeginDate>param.mDate){
			mini.alert("当前区间起息日不能大于区间到期日或者大于到期日","提示");
			return;
		}
		if(row.refEndDate<param.vDate||row.refEndDate>param.mDate){
			mini.alert("区间结束日期不能小于起息日或大于到期日.","提示");
			return;
		}
		if(row.theoryPaymentDate<param.vDate||row.theoryPaymentDate>param.mDate){
			mini.alert("收息日期不能小于起息日或大于到期日","提示");
			return;
		}	
		if(grid.data[0]!=null&&grid.data[0]!=''){
			if(row.refEndDate<grid.data[0].refEndDate){
				mini.alert("区间结束日期不能小于上一区间到期日","提示");
				return;
			}
		}
		grid.addRow(row, grid.data.length);
	}

	//修改
	function update(){
		var selectData = grid.getSelected();
		// if(CommonUtil.isNull(selectData))
		// {
		// 	mini.alert("请选中一条需要修改的收息计划!","提示");
		// 	return;
		// }
		if(CommonUtil.isNotNull(selectData.actualDate))
		{
			mini.alert("该条计划已经确认过,不能再次修改！","系统提示");
			return;
		}

		var addform = new mini.Form("InterestSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		};
		var row = addform.getData(true);
		var param = form.getData(true);
		if(row.refBeginDate < param.vDate){
			mini.alert("区间起始日期不能小于区间起息日.","提示");
			return;
		}
		if(row.refEndDate < row.refBeginDate || row.refBeginDate > param.mDate){
			mini.alert("当前区间起息日不能大于区间到期日或者交易到期日","提示");
			return;
		}
		if(row.refEndDate < param.vDate || row.refEndDate > param.mDate){
			mini.alert("区间结束日期不能小于区间起息日或大于交易到期日.","提示");
			return;
		}
		if(row.theoryPaymentDate < param.vDate || row.theoryPaymentDate > param.mDate){
			mini.alert("计划收息日期不能小于起息日或大于到期日","提示");
			return;
		}
		//refNo tmCashflowInterest tmCashflowInterestList
		var paramData = {};
		paramData['refNo'] = mini.get("refNo").getValue();
		paramData['dealType'] = '2';
		paramData['tmCashflowInterest'] = row;
		paramData['tmCashflowInterestList'] = dataResult;
		CommonUtil.ajax({
			url : '/DurationController/reSetInterestPlanForTemp',
			data : mini.encode(paramData),
			callback:function(data){
				if(data && data.obj && data.obj.code == '000'){
					dataResult = data.obj.data;
					search(grid.pageSize,grid.pageIndex);

					mini.alert("操作成功！","系统提示");
				}else{
					mini.alert(data.obj.msg,"系统提示");
				}
			}
		});
	}

	//删除收息计划
	function del(){
		var rows = grid.getSelecteds();
		if (rows.length > 0) {
			mini.confirm("确定删除记录？", "确定?",
				function (action) {
					if (action == "ok") 
					{
						grid.removeRows(rows, true);
						mini.alert("删除成功!","消息提示");
					} 
				}
			);
		}else{
			mini.alert("请选择要删除的列!","消息提示");
		}
	}

	function onRowClick()
	{
		var form = new mini.Form("InterestSchedule_from_div");
		var row =  grid.getSelected();
		form.setData(row,true,false);
		form.setIsValid(true);
	}

	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			mini.get("update_btn").setVisible(false);
			mini.get("reset_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
