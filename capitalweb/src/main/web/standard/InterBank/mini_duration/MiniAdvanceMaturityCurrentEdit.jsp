<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">提前支取</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易信息</legend>
<div id="AdvanceMaturityCurrent">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:150px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="aDate" name="aDate" />
			<input class="mini-hidden" id="dealDate" name="dealDate" />
			<input class="mini-hidden" id="version" name="version"></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  maxValue="999999999999"  format="n2" align="right"  enabled="false" label="原交易本金(元)" style="width:100%;"  labelStyle = "width:150px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:150px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:150px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
		</tr>
		<tr>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="acturalRate" name="acturalRate" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="p4" enabled="false" label="交易利率" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="remainAmt" name="remainAmt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="999999999999" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amDate" name="amDate" class="mini-datepicker"   required="true"  labelField="true"  label="提前支取日期" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amAmt" name="amAmt" class="mini-spinner"  onValuechanged="amAmtchanged" changeOnMousewheel='false'  labelField="true" maxValue="999999999999" format="n2" label="提前支取金额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="amInt" name="amInt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true" maxValue="999999999999"  enabled="false" format="n2" label="提前支取利息(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amFiamt" name="amFiamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true" maxValue="999999999999" format="n2" enabled="false" label="本金对应利息(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td colspan="3"><input id="remark" name="remark" class="mini-textarea"  labelField="true" required="true"  vtype="maxLength:100"  label="提前支取备注" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
	</table>
</div>
</fieldset>
<table class="form_table" width="100%" cols="4">
	<tr>
		<td colspan="4" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="red">提前支取说明:</font></span><BR/>
			&nbsp;<span>活期交易部分提前支取时没有提前支取利息</span><BR/>
			&nbsp;<span>只有全部提前支取时才会支取利息;</span>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var _bizDate = '<%=__bizDate %>';
	var form=new mini.Form("#field_form");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	//加载信息
	function initform(){
		mini.get("amDate").setValue(CommonUtil.serverData.bizDate);
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>提前支取</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("refNo").setValue( data.dealNo);
					mini.get("cName").setValue( data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue( data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("rateType").setValue(data.rateType);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("vDate").setValue( data.vDate);
					mini.get("sponsor").setValue(data.user.userId);
					mini.get("sponInst").setValue(data.institution.instId);
					mini.get("sponsorName").setValue( data.user.userName);
					mini.get("sponInstName").setValue( data.institution.instName);
					mini.get("remark").setValue( row.remark);//修改
					mini.get("version").setValue(data.version);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("acturalRate").setValue(  data.contractRate);
					mini.get("amDate").setValue( row.amDate);
				}
			});
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>提前支取</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
			url:"/ProductApproveController/searchProductApproveForDuration",
			data:mini.encode(data),
			callback : function(data) {
				form.setData(data.obj);
				mini.get("dealDate").setValue( _bizDate);
				mini.get("dealNo").setValue('');
				mini.get("refNo").setValue(data.obj.dealNo);
				mini.get("cName").setValue(data.obj.counterParty.party_name);
				mini.get("cNo").setValue(data.obj.counterParty.party_id);
				mini.get("prdName").setValue(data.obj.product.prdName);
				mini.get("sponsorName").setValue(data.obj.user.userName);
				mini.get("sponInstName").setValue(data.obj.institution.instName);
				mini.get("version").setValue(data.obj.version);
				mini.get("acturalRate").setValue(data.obj.contractRate);
				mini.get("originalAmt").setValue(data.obj.amt);
				mini.get("remainAmt").setValue(row.settlAmt);
				mini.get("amAmt").setValue(row.settlAmt);
				mini.get("amFiamt").setValue(data.obj.tpos.accruedTint);
				mini.get("amInt").setValue(data.obj.tpos.accruedTint);

				mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
				mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
				mini.get("aDate").setValue("<%=__bizDate %>");
			}
		});
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		var data=form.getData(true,false);
		data['trdtype']="3001_PREEND_CURRENT";
		var amt =isNaN(parseFloat(mini.get("remainAmt").getValue()))==true?0:parseFloat(mini.get("remainAmt").getValue());
		var amAmt =isNaN(parseFloat(mini.get("amAmt").getValue()))==true?0:parseFloat(mini.get("amAmt").getValue());
		if(amt<amAmt){
			mini.alert("赎回金额不能大于剩余可赎回金额!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		if(CommonUtil.dateCompare(data.vDate,data.amDate) || data.vDate==data.amDate){
			mini.alert("提前支取日须大于起息日!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		if(CommonUtil.dateDiff(data.dealDate,data.amDate)<0){
			mini.alert("提前支取日须大于账务日期,不支持倒起息!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		if(!amAmt){
			mini.alert("提前支取金额不能为零!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		var saveUrl = "/ApproveManageController/saveApprove";
		if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		}
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}

	/***************************************/
	function amAmtchanged(){
		if($.inArray(action,["edit","add"])>-1){
			var remainAmt = mini.get("remainAmt").getValue();
			var amAmt = mini.get("amAmt").getValue();
			if(parseFloat(remainAmt)>parseFloat(amAmt)){
				mini.get("amFiamt").setRequired(true);
				mini.get("amInt").setValue(0);//部分支取不提取利息
			}else if(parseFloat(remainAmt)==parseFloat(amAmt)){
				var amFiamt = mini.get("amFiamt").getValue();
				mini.get("amInt").setValue(amFiamt);//默认全部
			}else{
				mini.get("amAmt").setValue(remainAmt);
				mini.alert("赎回金额不能大于剩余可赎回金额!","系统提示");
			} 
		}
	}
	
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
