<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<span id="labell"><a href="javascript:CommonUtil.activeTab();">还本计划</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易详情</legend>
<div id="TradeOverDueEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:110px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"/>
			<input class="mini-hidden" id="cNo" name="cNo"/>
			<input class="mini-hidden" id="aDate" name="aDate" />
			<input class="mini-hidden" id="version" name="version"/></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"  changeOnMousewheel="false"labelField="true"  maxValue="9999999999999"  format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
		</tr>
		<tr>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="acturalRate" name="acturalRate" class="mini-spinner"  changeOnMousewheel="false"labelField="true"  format="p4" enabled="false" label="投资利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tfoot>
			<tr>
				<td><input id="mDate" name="mDate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="amtFre" name="amtFre" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.CouponFrequently"  enabled="false" label="原交易还本频率" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="remainAmt" name="remainAmt" class="mini-spinner"  changeOnMousewheel="false"labelField="true"  maxValue="9999999999999"  format="n2" enabled="false" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td><input id="effectDate" name="effectDate" class="mini-datepicker"  labelField="true"  enabled="false" label="还本调整生效日期" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3"><input id="changeReason" name="changeReason" required="true"  vtype="maxLength:2000" class="mini-textarea"  labelField="true"  label="还本调整说明" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
		</tfoot>
	</table>
</div>
</fieldset>
<fieldset class="mini-fieldset">
	<legend>还本计划调整</legend>
<div id="CapitalSchedule_from_div">
	<table class='mini-table' id="CapitalSchedule" name="CapitalSchedule"  style="width:100%">
		<tr>
			<td><input id="cfType" name="cfType" class="mini-combobox" value="Principal"  labelField="true" data="CommonUtil.serverData.dictionary.cfType"  enabled="false" label="现金流类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="payDirection" name="payDirection" class="mini-combobox" value="Recevie"  labelField="true" data="CommonUtil.serverData.dictionary.feeSituation"  enabled="false" label="收付方向" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="repaymentSdate" name="repaymentSdate" class="mini-datepicker" labelField="true"   required="true"  label="还本日期（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="repaymentSamt" name="repaymentSamt" class="mini-spinner"  changeOnMousewheel="false"labelField="true" required="true"  maxValue="9999999999999"  format="n2"   label="还本金额（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td></td>
				<td></td>
		</tr>
		<tr>
			<td colspan="3" >
				<div style="text-align:left">
					<a  id="add_btn" class="mini-button" style="display: none"    onclick="add()">新增</a>
					<a  id="delete_btn" class="mini-button" style="display: none"    onclick="del()">删除</a>
				</div>
			</td>
		</tr>
	</table>
</div>
</fieldset>
<fieldset style="min-width:0;">
		<legend>还本计划详情</legend>
<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;" mini-fit="true"
	allowResize="true"  border="true" sortMode="client" showPager="false"
	pageSize="100"  multiSelect="true" 
	allowCellEdit="true" allowCellSelect="true" multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true">

	<div property="columns">
		<div type="indexcolumn"     headerAlign="center"  allowSort="false" width="40" >序号</div>
		<div type="checkcolumn"     headerAlign="center"></div>    
		
		<div field="cfType"         headerAlign= "center" allowSort= "true" width="120" 
			renderer="CommonUtil.dictRenderer" data-options="{dict:'cfType'}" align="center">现金流类型
		</div> 
		
		<div field="payDirection"        headerAlign= "center" allowSort= "true" width="120"
			renderer="CommonUtil.dictRenderer" data-options="{dict:'feeSituation'}"  align="center">收付方向
		</div> 
		
		<div field="repaymentSdate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(计划)
			<input  class="mini-datepicker"   required="true"  property="editor"/>
		</div> 
		
		<div field="repaymentSamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00">还本金额(计划)
			<input class="mini-spinner" required="true"  property="editor"
				minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
		</div>             
		
		<div field="repaymentTdate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">还本日期(实际)
			<input class="mini-datepicker"  />
		</div>                          
		
		<div field="repaymentTamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00" >还本金额(实际)
			<input class="mini-spinner" 
				minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
		</div>                              
	</div>
</div>
</fieldset>
<table class="mini-form" width="100%" cols="4">
	<tr>
		<td colspan="4" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="red">还本调整说明:</font></span><BR/>
			&nbsp;<span>新增或更改的计划还本日必须大于等于当前账务日期，小于等于到期日期</span><BR/>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left">
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">

	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var _bizDate = '<%=__bizDate %>';
	var form=new mini.Form("#field_form");
	var grid=mini.get("grid1");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;

	grid.on("cellbeginedit",function(e){
		if(CommonUtil.isNotNull(e.row.repaymentTdate))
		{
			e.cancel = true;
		}
	});

	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>还本计划</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("refNo").setValue( data.dealNo);
					mini.get("cName").setValue( data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue( data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("rateType").setValue( data.rateType);
					mini.get("vDate").setValue( data.vDate);
					mini.get("mDate").setValue( data.mDate);
					mini.get("sponsor").setValue( data.user.userId);
					mini.get("sponInst").setValue(data.institution.instId);
					mini.get("sponsorName").setValue( data.user.userName);
					mini.get("sponInstName").setValue( data.institution.instName);
					mini.get("version").setValue(data.version);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("acturalRate").setValue(  data.contractRate);
					mini.get("effectDate").setValue(  row.effectDate);
					mini.get("changeReason").setValue(  row.changeReason);
					mini.get("amtFre").setValue( data.amtFre);
				}
			});
			/******************************/
			CommonUtil.ajax( {
				url:"/tmCalCashFlowController/getApproveCapitalForApprove",
				data:mini.encode({dealNo : row.dealNo,
				dealType:row.dealType}),
				callback : function(data) {
					data = mini.decode(data);
					grid.setData(data.obj);
				}
			});

		}else {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>还本计划</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveForDuration",
				data:mini.encode(data),
				callback : function(data) {
					form.setData(data.obj);
					mini.get("effectDate").setValue( _bizDate);
					mini.get("dealNo").setValue('');
					mini.get("refNo").setValue(data.obj.dealNo);
					mini.get("cName").setValue(data.obj.counterParty.party_name);
					mini.get("cNo").setValue(data.obj.counterParty.party_id);
					mini.get("prdName").setValue(data.obj.product.prdName);
					mini.get("sponsorName").setValue(data.obj.user.userName);
					mini.get("sponInstName").setValue(data.obj.institution.instName);
					mini.get("version").setValue(data.obj.version);
					mini.get("acturalRate").setValue( data.obj.contractRate);
					mini.get("originalAmt").setValue( data.obj.amt);
					mini.get("remainAmt").setValue(row.settlAmt);

					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("aDate").setValue("<%=__bizDate %>");
							
				}
			});
			/******************************/
			CommonUtil.ajax({
				url:"/tmCalCashFlowController/getCapitalCashFlowForApprove",
				data:mini.encode({dealNo : row.dealNo,
				dealType:row.dealType}),
				callback : function(data) {
					data = mini.decode(data);
					grid.setData(data.obj);
				}
			});
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		if(grid.data.length <= 0){
			mini.alert("请新增还本计划至列表中!","提示");
			mini.hideMessageBox(messageid);
			return;
		}
		var mDate = mini.get("mDate").getFormValue();
		var remainAmt = mini.get("originalAmt").getValue();
		var data = grid.getData();
		var param = form.getData(true);
		param['trdtype'] = "3001_CFCHANGE";
		param['approveCapitals'] = data;
		var amt = 0;
		var flag = false;
		var tempDateArr = {};
		var repaymentSdate = null;
		for(var i = 0; i < data.length; i++)
		{
			if(data[i].repaymentSdate == "" || data[i].repaymentSdate == null){
				mini.alert("计划还本日期不能为空!","提示");
				mini.hideMessageBox(messageid);
				return false;
			}
			repaymentSdate =  CommonUtil.stampToDate(data[i].repaymentSdate);
			if(mDate == repaymentSdate)
			{
				flag = true;	
				if(parseFloat(data[i].repaymentSamt) <= 0){
					mini.alert("到期日的还本计划金额必须大于0!","提示");
					mini.hideMessageBox(messageid);
					return;
				}
			}
			if(CommonUtil.isNotNull(tempDateArr[repaymentSdate]))
			{
				mini.alert("计划还本日期["+repaymentSdate+"]不能重复!","提示");
				mini.hideMessageBox(messageid);
				return false;
			}else{
				tempDateArr[repaymentSdate] = 1;
			}

			amt += parseFloat(data[i].repaymentSamt);
		}// end for

		if(flag == false){
			mini.alert("到期日必须填写还本计划!", "提示");
			mini.hideMessageBox(messageid);
			return false;
		}

		if(parseFloat(remainAmt) != parseFloat(amt)){
			mini.alert("还本总金額必须等于本金!","提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		
		var saveUrl = "/ApproveManageController/saveApprove";
		if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		}
		var params=mini.encode(param);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}
	//添加还本计划
	function add(){
		var addform = new mini.Form("CapitalSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		};
		var row = addform.getData(true);
		var param=form.getData(true);
		if(CommonUtil.dateDiff(param.vDate,param.effectDate)<0){
			mini.alert("还本调整生效日期大于等于起息日期!","提示");
			return false;
		}
		if(CommonUtil.dateDiff(row.repaymentSdate,_bizDate)>0||CommonUtil.dateDiff(row.repaymentSdate,param.mDate)<0){
			mini.alert("计划还本日期必须大于等于当前账务日期，小于等于到期日期!","提示");
			return false;
		}
		grid.addRow(row, grid.data.length);
	}
	//删除还本计划
	function del(){
		var rows = grid.getSelecteds();
		for(var i=0;i<rows.length;i++){
			if(rows[i].repaymentTdate!=null||rows[i].repaymentTamt>0){
				mini.alert("已经还本的计划不能删除!","系统提示！");
				return false;
			}
		}
		if (rows.length > 0) {
			mini.confirm("确定删除记录？", "确定?",
				function (action) {
					if (action == "ok") 
					{
						grid.removeRows(rows, true);
						mini.alert("删除成功!","消息提示");
					} 
				}
			);
		}else{
			mini.alert("请选择要删除的列!","消息提示");
		}
	}
	//计划还本日期变化触发
	grid.on("cellcommitedit", function (e) {
		var bizDate=mini.encode(_bizDate);
		var mdate=mini.encode(mini.get("mDate").getValue(),"yyyy-MM-dd");
		var newDate=mini.encode(e.value,"yyyy-MM-dd");
		if (e.field == "repaymentSdate") {
			if(newDate<bizDate||newDate>mdate){
				e.value="";
				mini.alert("计划还本日期必须大于等于当前账务日期，小于等于到期日期!","提示");
				return false;
			}
		}
	});
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			mini.get("add_btn").setVisible(false);
			mini.get("delete_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
