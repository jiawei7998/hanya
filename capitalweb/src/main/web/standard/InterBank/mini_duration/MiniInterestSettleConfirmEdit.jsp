<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body style="width:100%;height:100%;background:white">
<span id="labell"><a href="javascript:CommonUtil.activeTab();">结息确认</a> >> 修改</span>
<fieldset class="mini-fieldset title">
        <legend>结息确认原交易信息</legend>
<div id="InterestSettleConfirmEdit">
<table id="field_form" class="mini-table" style="width:100%">
        <tr>
            <td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
            <td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手" style="width:100%;" labelStyle = "width:110px;" />
            <input class="mini-hidden" id="dealNo" name="dealNo"  />
            <input class="mini-hidden" id="cNo" name="cNo"/>
            <input class="mini-hidden" id="aDate" name="aDate" />
            <input class="mini-hidden" id="dealDate" name="dealDate" />
            <input class="mini-hidden" id="version" name="version"/></td>
            <td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
        </tr>
        <tr>
            <td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true"   enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" /></td>
            <td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true"   enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
                <input class ="mini-hidden" id ="sponInst" name="sponInst"/>
                <input class ="mini-hidden" id="sponsor" name="sponsor">
            </td>
            <td><input id="vDate" name="vDate" class="mini-datepicker" valueType='String'   labelField="true"  enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
        </tr>
        <tr>
            <td><input id="originalAmt" name="originalAmt" class="mini-spinner"   labelField="true" format="n2"  enabled="false" label="原交易本金(元)" maxValue="100000000000" style="width:100%;" labelStyle = "width:110px;" />
            <input id="settleFlag" name="settleFlag" class="mini-hidden"/></td>
            <td><input id="rate" name="rate" class="mini-spinner" changeOnMousewheel='false' enabled="false"  format="p4" labelField="true"  label="投资利率(%)" style="width:100%;" labelStyle = "width:110px;" /></td>
            <td><input id="interestSettlAmt" name="interestSettlAmt" class="mini-spinner" enabled="false" labelField="true" changeOnMousewheel='false' maxValue="100000000000" label="当期结息金额" style="width:100%;" labelStyle = "width:110px;" /></td>
        </tr>
        <tfoot>
            <tr>
                <td><input id="settlInt" name="settlInt" class="mini-spinner"  labelField="true" format="n2"  label="实际结息金额" minValue ='0' maxValue="100000000000" style="width:100%;" changeOnMousewheel='false' labelStyle = "width:110px;" /></td>
                <td ><input id="amReason" name="amReason" class="mini-textbox" required="true"  vtype="maxLength:100" labelField="true"  label="结息备注" style="width:100%;" labelStyle = "width:110px;" /></td>
            </tr>
        </tfoot>
    </table>
</div>
</fieldset>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%> 
<div class="mini-toolbar" style="text-align:left">
    <a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
    <a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script>
mini.parse();
var currTab = top["win"].tabs.getActiveTab();
var params = currTab.params;
var row = params.selectData;
var url = window.location.search;
var _bizDate = '<%=__bizDate %>';
var userId ='<%=__sessionUser.getUserId()%>';
var instId ='<%=__sessionUser.getInstId()%>';
var action = CommonUtil.getParam(url, "action");
var tradeData={};
tradeData.selectData=row;
tradeData.operType=action;
tradeData.serial_no=row.dealNo;
tradeData.task_id=row.taskId;


$(document).ready(function(){
    //初始化操作
    init();
})
function init(){
    if(action=='add'){//新增操作
        $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>结息确认</a> >> 新增");
        var param={"dealNo":row.dealNo};
        CommonUtil.ajax({
			url:"/ProductApproveController/searchProductApproveForDuration",
			data:mini.encode(param),
			callback : function(data) {
                if(data.obj){
                    
                    mini.get("refNo").setValue(data.obj.dealNo);
                    mini.get("version").setValue(data.obj.version);
                    mini.get("dealDate").setValue(_bizDate);
                    mini.get("cName").setValue(data.obj.counterParty.party_name);
                    mini.get("cNo").setValue(data.obj.counterParty.party_id);
                    mini.get("prdName").setValue(data.obj.product.prdName);//产品种类名称
                    mini.get("sponsorName").setValue(data.obj.user.userName);

                    mini.get("sponInstName").setValue(data.obj.institution.instName);
                    mini.get("sponsor").setValue(userId);
                    mini.get("sponInst").setValue(instId);
                    mini.get("vDate").setValue(data.obj.vDate);
                    mini.get("originalAmt").setValue(data.obj.amt);
                    mini.get("settleFlag").setValue(0);
                    mini.get("rate").setValue(data.obj.contractRate);
                    mini.get("interestSettlAmt").setValue(data.obj.tpos.interestSettleAmt);
                    mini.get("settlInt").setValue(data.obj.tpos.interestSettleAmt);

                    mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
				    mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
				    mini.get("aDate").setValue("<%=__bizDate %>");

                }
               
            }
        })
    }
    if(action=='edit' || action=='detail' || action=='approve'){
        if(action=='edit'){
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>结息确认</a> >> 修改");
        }
        if(action=='approve'){
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>结息确认</a> >> 审批");
            mini.get("save_btn").setVisible(false);
            mini.get("close_btn").setVisible(false);
           
            mini.get("rate").setEnabled(false);
            mini.get("interestSettlAmt").setEnabled(false);
            mini.get("settlInt").setEnabled(false);
            mini.get("amReason").setEnabled(false);
        }
        if(action=='detail'){
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>结息确认</a> >> 详情");
            mini.get("settleFlag").setEnabled(false);
            mini.get("rate").setEnabled(false);
            mini.get("interestSettlAmt").setEnabled(false);
            mini.get("settlInt").setEnabled(false);
            mini.get("amReason").setEnabled(false);
            mini.get("save_btn").setVisible(false);
        }
        var params={};
        params['dealNo']=row.refNo;
        params['version']=row.version;
        CommonUtil.ajax({
            url: "/DurationController/getTdProductApproveMain",
            data: mini.encode(params),
            callback: function (data) {
                //row 上的数据
                mini.get("dealNo").setValue(row.dealNo);
                mini.get("settleFlag").setValue(row.settleFlag);
                mini.get("amReason").setValue(row.amReason);
                mini.get("settlInt").setValue(row.settlInt);
                mini.get("interestSettlAmt").setValue(row.interestSettlAmt);

                //基础数据
                mini.get("refNo").setValue(data.dealNo);
                mini.get("version").setValue(data.version);
                mini.get("dealDate").setValue(_bizDate);
                mini.get("cName").setValue(data.counterParty.party_name);
                mini.get("cNo").setValue(data.counterParty.party_id);
                mini.get("prdName").setValue(data.product.prdName);//产品种类名称
                mini.get("sponsorName").setValue(data.user.userName);

                mini.get("sponInstName").setValue(data.institution.instName);
                mini.get("sponsor").setValue(row.sponsor);
                mini.get("sponInst").setValue(row.sponInst);
                mini.get("vDate").setValue(data.vDate);
                mini.get("originalAmt").setValue(data.amt);
                mini.get("rate").setValue(data.contractRate);

            }
        })
    }
}
//关闭表单
function colse() {
    top["win"].closeMenuTab();
} 

//保存操作
function save(){
    var form =new mini.Form("field_form");
    form.validate();
    if (form.isValid() == false) {
        mini.alert("信息填写有误，请重新填写", "系统也提示");
        return;
    }
    var data=form.getData();

    var interestSettlAmt = data.interestSettlAmt;
    if(parseFloat(interestSettlAmt) <= 0){
        mini.alert("当期结息金额必须大于0!","系统提示");
        return;
    }

    var settlInt = data.settlInt;
    if(parseFloat(settlInt) <= 0){
        mini.alert("实际结息金额必须大于0!","系统提示");
        return;
    }

    data['trdtype']="3001_InterSettle";
    var saveUrl = "/ApproveManageController/saveApprove";
    if(row.dealType=="2"){
        saveUrl = "/TradeManageController/saveTrade";
    }
    var params=mini.encode(data);
    CommonUtil.ajax({
        url:saveUrl,
        data:params,
        callback:function(data){
            mini.alert("保存成功","提示",function(){
                setTimeout(function(){top["win"].closeMenuTab()},10);
            });
        }
    });
}
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>