<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">提前还款</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易信息</legend>
<div id="TradeOffsetEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:150px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="aDate" name="aDate" />
			<input class="mini-hidden" id="dealDate" name="dealDate" />
			<input class="mini-hidden" id="version" name="version"></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  maxValue="999999999999"  format="n2" align="right"  enabled="false" label="原交易本金(元)" style="width:100%;"  labelStyle = "width:150px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:150px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:150px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
		</tr>
		<tr>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="mDate" name="mDate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="acturalRate" name="acturalRate" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="p4" enabled="false" label="交易利率" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="intType" name="intType" class="mini-combobox" onValuechanged="intTypechanged"  labelField="true" data="CommonUtil.serverData.dictionary.intType"  enabled="false" label="收息模式"  style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amDate" name="amDate" class="mini-datepicker" onValuechanged="amDatechanged" required="true"   labelField="true"  label="提前还款日期" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
	<tfoot>
		<tr>
			<td><input id="remainAmt" name="remainAmt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="999999999999" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="remainFiamt" name="remainFiamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="999999999999" label="持仓本金应收利息(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="remainAddFiamt" name="remainAddFiamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="999999999999" label="持仓本金计提差额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="amorFiamt" name="amorFiamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" maxValue="999999999999" enabled="false" label="先收息已摊销金额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amorAddFiamt" name="amorAddFiamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="999999999999" label="先收息摊销差额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="intAmt" name="intAmt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" label="先收息金额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="amAmt" name="amAmt" class="mini-spinner"   changeOnMousewheel='false' onValuechanged="amAmtchanged"  labelField="true"  format="n2" maxValue="999999999999"  label="提前还款金额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amFiamt" name="amFiamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="999999999999" label="提前还款本金应收利息(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amAddFiamt" name="amAddFiamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="999999999999" label="提前还款本金计提差额(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td><input id="amInt" name="amInt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true" maxValue="999999999999" enabled="false" format="n2" label="提前还款利息(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
			<td><input id="amPenalty" name="amPenalty" class="mini-spinner"   changeOnMousewheel='false' maxValue="999999999999" labelField="true"  format="n2"  label="提前还款违约金(元)" style="width:100%;" labelStyle = "width:150px;" visible="false"/></td>
			<td><input id="returnIamt" name="returnIamt" class="mini-spinner"   changeOnMousewheel='false'  labelField="true" maxValue="999999999999"  format="n2" enabled="false" label="归还利息(元)" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
		<tr>
			<td colspan="3"><input id="remark" name="remark" class="mini-textarea"  labelField="true" required="true"  vtype="maxLength:100"  label="提前还款备注" style="width:100%;" labelStyle = "width:150px;" /></td>
		</tr>
	</tfoot>	
	</table>
</div>
</fieldset>
<table class="form_table" width="100%" cols="4">
	<tr>
		<td colspan="2" style="font-size:16px;border-bottom: 1px solid #B3DE94;border-top: 1px solid #B3DE94;">
			&nbsp;<span><font color="#0065d2">提前还款说明:</font></span><BR/>
			&nbsp;<span>"先收息"交易提前还款必须是"全额还款";</span><BR/>
			&nbsp;<span>"后收息"交易提前还款可以选择"全额"或"部分"还款;</span>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	// var row=top["advanceMaturityManage"].getData(action);
	var _bizDate = '<%=__bizDate %>';
	var form=new mini.Form("#field_form");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	//
	function calc(){
		var amAmt = mini.get("amAmt").getValue();
		var remainAmt = mini.get("remainAmt").getValue();
		if(parseFloat(amAmt) < parseFloat(remainAmt)){
			mini.get("amFiamt").setValue(0);
			mini.get("amAddFiamt").setValue(0);
			mini.get("amInt").setValue(0);
			return;
		}

		var param = form.getData();
		param.isFees = "true";
		var params = mini.encode(param);
		CommonUtil.ajax({
			url : "/DurationController/getAmInt",
			data : params,
			callback : function(data) {
				if(param.intType == 1){
					mini.get("amorAddFiamt").setValue(data.obj);
				}else{
					mini.get("remainAddFiamt").setValue(data.obj);
					if(parseFloat(remainAmt) >= parseFloat(amAmt))
					{
						var remainAddFiamt =mini.get("remainAddFiamt").getValue(); 
						mini.get("amAddFiamt").setValue(remainAddFiamt*amAmt/remainAmt);
						var remainFiamt =mini.get("remainFiamt").getValue();
						mini.get("amFiamt").setValue(remainFiamt*amAmt/remainAmt);

					}else {
			        	mini.alert("提前还款金额不能大于剩余本金金额!","提示");
			        }
			    }
			    fiamtchanged();
			}
		});
	}
	//加载信息
	function initform(){
		mini.get("amDate").setValue(CommonUtil.serverData.bizDate);
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>提前还款</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("refNo").setValue( data.dealNo);
					mini.get("cName").setValue( data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue( data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("rateType").setValue(data.rateType);
					mini.get("vDate").setValue( data.vDate);
					mini.get("mDate").setValue( data.mDate);
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("sponsorName").setValue( data.user.userName);
					mini.get("sponInstName").setValue( data.institution.instName);
					mini.get("remark").setValue( row.remark);//修改
					mini.get("version").setValue(data.version);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("acturalRate").setValue(  data.contractRate);
					mini.get("intType").setValue(row.intType);
					mini.get("intAmt").setValue( data.intAmt);
					mini.get("amDate").setValue( row.amDate);
					intTypechanged();
				}
			});
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>提前还款</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
			url:"/ProductApproveController/searchProductApproveForDuration",
			data:mini.encode(data),
			callback : function(data) {
				form.setData(data.obj);
				mini.get("dealDate").setValue( _bizDate);
				mini.get("dealNo").setValue('');
				mini.get("refNo").setValue(data.obj.dealNo);
				mini.get("cName").setValue(data.obj.counterParty.party_name);
				mini.get("cNo").setValue(data.obj.counterParty.party_id);
				mini.get("prdName").setValue(data.obj.product.prdName);
				mini.get("intType").setValue( data.obj.intType);
				mini.get("sponsor").setValue( data.obj.user.userId);
				mini.get("sponInst").setValue(data.obj.institution.instId);
				mini.get("sponsorName").setValue(data.obj.user.userName);
				mini.get("sponInstName").setValue(data.obj.institution.instName);
				mini.get("version").setValue(data.obj.version);
				mini.get("acturalRate").setValue(data.obj.contractRate);
				mini.get("originalAmt").setValue(data.obj.amt);
				mini.get("remainAmt").setValue(row.settlAmt);
				mini.get("amAmt").setValue(row.settlAmt);
				if(data.obj.intType == 1){//先收息
					mini.get("amorFiamt").setValue(data.obj.tpos.amorTint);
					mini.get("amorAddFiamt").setValue(0);
					mini.get("amFiamt").setValue(0);
					mini.get("amAddFiamt").setValue(0);
					mini.get("remainFiamt").setValue(0);
					mini.get("remainAddFiamt").setValue(0);
					mini.get("returnIamt").setValue(0);//默认0
					mini.get("intAmt").setValue(data.obj.intAmt);
				}else{//后收息
					mini.get("amorFiamt").setValue(0);
					mini.get("amorAddFiamt").setValue(0);
					mini.get("amFiamt").setValue(data.obj.tpos.accruedTint);
					mini.get("amAddFiamt").setValue(0);
					mini.get("remainFiamt").setValue(data.obj.tpos.accruedTint);
					mini.get("remainAddFiamt").setValue(0);
					mini.get("amInt").setValue(data.obj.tpos.accruedTint);//默认全部
					mini.get("amPenalty").setValue(0);//默认0
				}
				mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
				mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
				mini.get("aDate").setValue("<%=__bizDate %>");
				mini.get("amDate").setValue("<%=__bizDate %>");

				intTypechanged();
			}
		});
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		var data=form.getData(true,false);
		data['trdtype']="3001_PREEND";
		var amt =isNaN(parseFloat(mini.get("remainAmt").getValue()))==true?0:parseFloat(mini.get("remainAmt").getValue());
		var amAmt =isNaN(parseFloat(mini.get("amAmt").getValue()))==true?0:parseFloat(mini.get("amAmt").getValue());
		if(amt<amAmt){
			mini.alert("提前还款金额不能大于剩余本金金额!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		if(CommonUtil.dateCompare(data.vDate,data.amDate) || data.vDate==data.amDate){
			mini.alert("提前还款日须大于起息日!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		if(CommonUtil.dateCompare(data.amDate,data.mDate)){
			mini.alert("提前还款日须小于交易到期日!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		if(CommonUtil.dateDiff(data.dealDate,data.amDate)<0){
			mini.alert("提前还款日须大于账务日期,不支持倒起息!","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		var saveUrl = "/ApproveManageController/saveApprove";
		if(row.dealType=="2"){
			saveUrl = "/TradeManageController/saveTrade";
		}
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}

	/***************************************/
	function amDatechanged(){
		var amDate = mini.get("amDate").getFormValue();
		if(amDate < _bizDate){
			mini.alert("提前还款日期不能小于当前账务日期!","提示");
			mini.get("amDate").setValue(_bizDate);
		}
		calc();
	}

	function amAmtchanged(){
		var remainAmt = mini.get("remainAmt").getValue();
		var amAmt = mini.get("amAmt").getValue();
		if(parseFloat(amAmt) == parseFloat(remainAmt)){
			var remainFiamt =mini.get("remainFiamt").getValue();
			var remainAddFiamt = mini.get("remainAddFiamt").getValue();

			mini.get("amFiamt").setValue(remainFiamt*amAmt/remainAmt);
			mini.get("amAddFiamt").setValue(remainAddFiamt*amAmt/remainAmt);
			calc();
			
		}else if(parseFloat(amAmt) > parseFloat(remainAmt)){
			mini.alert("提前还款金额不能大于剩余本金金额!","提示");
			mini.get("amAmt").setValue(remainAmt);

		}else if(parseFloat(amAmt) < parseFloat(remainAmt)){
			mini.get("amFiamt").setValue(0);
			mini.get("amAddFiamt").setValue(0);
			mini.get("amInt").setValue(0);
		}
	}

	function fiamtchanged(){
		var amAddFiamt = mini.get("amAddFiamt").getValue();
		var amFiamt = mini.get("amFiamt").getValue();
		mini.get("amInt").setValue(amAddFiamt + amFiamt);
	}
	function intTypechanged(){
		var value=mini.get("intType").getValue();
		if (value == "1") {//先收息
			mini.get("amorAddFiamt").setRequired(true);		
			mini.get("amorFiamt").setRequired(true);		

			mini.get("returnIamt").setRequired(true);
			
			mini.get("amPenalty").setRequired(true);
			mini.get("amPenalty").setValue("");
			mini.get("amPenalty").setVisible(false);

			mini.get("amInt").setRequired(true);
			mini.get("amInt").setValue("");
			mini.get("amInt").setVisible(false);
			
			mini.get("amFiamt").setRequired(false);
			mini.get("amFiamt").setValue("");
			mini.get("amFiamt").setVisible(false);
		
			mini.get("amAddFiamt").setRequired(false);
			mini.get("amAddFiamt").setValue("");
			mini.get("amAddFiamt").setVisible(false);

			mini.get("remainFiamt").setRequired(false);
			mini.get("remainFiamt").setValue("");
			mini.get("remainFiamt").setVisible(false);

			mini.get("remainAddFiamt").setRequired(false);
			mini.get("remainAddFiamt").setValue("");
			mini.get("remainAddFiamt").setVisible(false);
			
		} else if (value == "2") {//后收息
			mini.get("amorAddFiamt").setRequired(false);
			mini.get("amorAddFiamt").setValue("");
			mini.get("amorAddFiamt").setVisible(false);

			mini.get("amorFiamt").setRequired(false);
			mini.get("amorFiamt").setValue("");
			mini.get("amorFiamt").setVisible(false);
			
			mini.get("returnIamt").setRequired(false);
			mini.get("returnIamt").setValue("");
			mini.get("returnIamt").setVisible(false);

			mini.get("intAmt").setRequired(false);
			mini.get("intAmt").setValue("");
			mini.get("intAmt").setVisible(false);

			mini.get("amFiamt").setRequired(true);
			mini.get("amAddFiamt").setRequired(true);
			mini.get("remainFiamt").setRequired(true);
			mini.get("remainAddFiamt").setRequired(true);

		}
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
