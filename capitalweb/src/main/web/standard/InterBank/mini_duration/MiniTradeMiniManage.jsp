<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
	<legend>交易查询</legend>
</fieldset>
<div id="TradeMiniManage" >   
	<table id="search_form" class="mini-form" >
		<tr>
			<td><input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="交易单号" style="width:100%;" /></td>
			<td><input id="custom_product_search" name="custom_product_search" class="mini-textbox" labelField="true"  label="产品种类" style="width:100%;" /></td>
			<td><input id="product_name_search" name="product_name_search" class="mini-textbox" labelField="true"  label="产品名称" style="width:100%;" /></td>
		</tr>
		<tr>
			<td colspan="6">
				<a  id="clear_btn" class="mini-button" style="display: none"   onClick="clear">清空</a>
				<a  id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
			</td>
		</tr>
	</table>
</div>
<div id="TradeMiniManage" class="mini-fit">   
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
	idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true"  frozenStartColumn="0" frozenEndColumn="1">
		<div property="columns" >
			<div field="dealNo" width="150" align="center"  headerAlign="center" allowSort="true">交易单号</div>    
			<div field="counterParty.party_name" width="150" align="center"  headerAlign="center" allowSort="true">客户简称</div>                            
			<div field="conTitle" width="180" align="center" headerAlign="center" >产品名称</div>
			<div field="product.prdName"  width="180" align="center" headerAlign="center" allowSort="true">产品种类</div>                                
			<div field="amt" width="180" align="center" headerAlign="center" allowSort="true" >合同额（元）</div>
			<div field="vDate" width="180" align="center" headerAlign="center" >起息日</div>
		</div>
	</div>
</div> 
<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	var businessType=CommonUtil.getParam(url,"businessType");
	var grid=mini.get("datagrid");
	var form=new mini.Form("#search_form");
	//清空
	function clear(){
		form.clear();
	}
	//查询
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误","系统提示");
			return ;
		}
		var data=form.getData();
		data['pageSize']=pageSize;
		data['pageNumber']=pageIndex+1;
		data['businessType']=businessType;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/ProductApproveController/getOrderVoForCancelPage",
			data:params,
			callback : function(data) {
				var grid = mini.get("datagrid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	grid.on("beforeload",function(e){
		e.cancel = true;
		var pageSize=e.data.pageSize;
		var pageIndex=e.data.pageIndex;
		search(pageSize,pageIndex);
	});

	function onRowDblClick(e){
		var row=e.record;
		var data={'refNo':row.dealNo,'ApproveStatus':['6','3','7','8']};
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/DurationController/ifExistApproveCancel",
			data:params,
			callback : function(data) {
				if(data.obj != null){
					mini.alert( '该交易单已存在未完成撤销记录<br>【业务员：'+data.obj.sponsor+'】<br>【单号：'+data.obj.dealNo+'】','系统提示');
					return false;
				}else{
					CloseWindow("ok");
				}
			}
		});
		
	}
	function CloseWindow(action) {
	   if (window.CloseOwnerWindow) return window.CloseOwnerWindow(action);
	   else window.close();
	}
	function GetData() {
	    var row = grid.getSelected();
	    return row;
	}


	$(document).ready(function(){
		search(10,0);
	});
</script>
</body>
</html>