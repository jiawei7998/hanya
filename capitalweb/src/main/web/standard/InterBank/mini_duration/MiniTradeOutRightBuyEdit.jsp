<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">资产转让</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易详情</legend>
<div id="TradeOutRightBuyEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:110px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="aDate" name="aDate" />
			<input class="mini-hidden" id="dealDate" name="dealDate" />
			<input class="mini-hidden" id="version" name="version"></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  maxValue="9999999999999"  format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
		</tr>
		<tr>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="orgiRate" name="orgiRate" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  format=p4 enabled="false" label="原交易利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="orgiMdate" name="orgiMdate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="term" name="term" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  enabled="false" label="占款期限(天)" maxValue="10000" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="basis" name="basis" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.DayCounter"  enabled="false" label="计息基础" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="mInt" name="mInt" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" label="到期利息(元)" maxValue="9999999999999" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="mAmt" name="mAmt" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" label="到期金额(元)"  maxValue="9999999999999" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="remainAmt" name="remainAmt" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  format="n2" enabled="false" maxValue="9999999999999" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tfoot>
			<tr>
				<td><input id="osaleCno" name="osaleCno" class="mini-buttonedit" allowInput="false"  onbuttonclick="onInsQuery" vtype="maxLength:32"  labelField="true"  required="true"  label="客户编号(交易方)" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osalePartyName" name="osalePartyName" class="mini-textbox"   labelField="true"  enabled="false" label="客户名称" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osaleVdate" name="osaleVdate" class="mini-datepicker" onvaluechanged="osaleVdatechange"  labelField="true" required="true"   label="转让起息日" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td><input id="osaleAmt" name="osaleAmt" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  format="n2" maxValue="9999999999999" label="转让金额(元)" enabled="false" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osaleRate" name="osaleRate" class="mini-spinner"  changeOnMousewheel='false' onvaluechanged="osaleRatechange"  labelField="true"  format="p4"  label="转让利率" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="osaleSrate" name="osaleSrate" class="mini-spinner"  changeOnMousewheel='false'  labelField="true"  format="p4" enabled="false" label="转让利差" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
			<tr>
				<td colspan="3"><input id="resReason" name="resReason" required="true"  vtype="maxLength:100" class="mini-textarea"  labelField="true"  label="交易转让备注" style="width:100%;" labelStyle = "width:110px;" /></td>
			</tr>
		</tfoot>
	</table>
</div>
</fieldset>
<table class="mini-form" width="100%" cols="4">
	<tr>
		<td colspan="4" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="#0065d2">转让交易说明:</font></span><BR/>
			&nbsp;<span>"转让金额"必须是持仓余额全额转让</span><BR/>
			&nbsp;<span>转让后还本计划、收息计划、中收计划全部截止</span><BR/>
			&nbsp;<span>转让后需要从来账分拣里面确认真实转让利息</span><BR/>
			&nbsp;<span>转让前后的应收利息都已经减去通道计提</span><BR/>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	// var row=top["tradeOutRightBuyManage"].getData(action);
	var _bizDate = '<%=__bizDate %>';
	var form=new mini.Form("#field_form");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	//查询客户
	function onInsQuery(){
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_cp/MiniCoreCustQueryEdits.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						if(data.party_id){
							data.cifNo=data.party_id;
						}
						if(data.party_name){
							data.custName=data.party_name;
						}
						var cName =mini.get("cName").getValue();
						if(cName == data.custName) {
							mini.alert("转让交易对手不能和原交易对手相同！","提示");
						}else{
							btnEdit.setValue(data.cifNo);
							btnEdit.setText(data.cifNo);
							mini.get("osalePartyName").setText(data.custName);
							mini.get("osalePartyName").setValue(data.custName);
							btnEdit.validate();
							btnEdit.focus();
						}
					}
				}

			}
		});
	}
	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资产转让</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("refNo").setValue(data.dealNo);
					mini.get("cName").setValue(data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("osaleCno").setValue(row.osaleCno);
					mini.get("osaleCno").setText(row.osaleCno);
					mini.get("osalePartyName").setValue(row.osalePartyName);
					mini.get("prdName").setValue(data.product.prdName);
					mini.get("originalAmt").setValue(data.amt);
					mini.get("rateType").setValue(data.rateType);
					mini.get("vDate").setValue(data.vDate);
					mini.get("orgiMdate").setValue(data.mDate);
					mini.get("sponsor").setValue(data.user.userId);
					mini.get("sponInst").setValue(data.institution.instId);
					mini.get("sponsorName").setValue(data.user.userName);
					mini.get("sponInstName").setValue(data.institution.instName);
					mini.get("version").setValue(data.version);
					mini.get("basis").setValue(data.basis);
					mini.get("mInt").setValue(data.mInt);
					mini.get("mAmt").setValue(data.mAmt);
					mini.get("term").setValue(data.occupTerm);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("osaleVdate").setValue(row.osaleVdate);
					mini.get("orgiRate").setValue( data.contractRate);	
				}
			});
		}else {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资产转让</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveForDuration",
				data:mini.encode(data),
				callback : function(data) {
					form.setData(data.obj);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("dealNo").setValue('');
					mini.get("refNo").setValue( data.obj.dealNo);
					mini.get("cName").setValue( data.obj.counterParty.party_name);
					mini.get("cNo").setValue(data.obj.counterParty.party_id);
					mini.get("prdName").setValue(data.obj.product.prdName);
					mini.get("sponsorName").setValue( data.obj.user.userName);
					mini.get("sponInstName").setValue( data.obj.institution.instName);
					mini.get("version").setValue(data.obj.version);
					mini.get("mInt").setValue(data.obj.mInt);
					mini.get("mAmt").setValue(data.obj.mAmt);
					mini.get("term").setValue(data.obj.occupTerm);
					mini.get("orgiRate").setValue(data.obj.contractRate);
					mini.get("osaleRate").setValue(data.obj.contractRate);
					mini.get("osaleSrate").setValue(0.00);
					mini.get("originalAmt").setValue(data.obj.amt);
					mini.get("osaleVdate").setValue( _bizDate);
					mini.get("orgiMdate").setValue( data.obj.mDate);
					mini.get("remainAmt").setValue( row.settlAmt);
					mini.get("osaleAmt").setValue(row.settlAmt);

					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("aDate").setValue("<%=__bizDate %>");
				}
			});
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		var datas=form.getData(true);
		datas['trdtype']="3001_ORSALE";
		datas['osaleType']="T";
		var remainAmt =isNaN(parseFloat(mini.get("remainAmt").getValue()))==true?0:parseFloat(mini.get("remainAmt").getValue());
		var osaleAmt =isNaN(parseFloat(mini.get("osaleAmt").getValue()))==true?0:parseFloat(mini.get("osaleAmt").getValue());
		var rate = isNaN(parseFloat(mini.get("osaleRate").getValue()))==true?0:parseFloat(mini.get("osaleRate").getValue());
		var sRate= isNaN(mini.get("osaleSrate").getValue()) == true ? 0 : mini.get("osaleSrate").getValue();//转让利差
		sRate = sRate.toFixed(6);
		if(sRate <= 0){
			mini.alert("转让利差应大于0","系统提示");
			mini.hideMessageBox(messageid);
			return false;
		}
		
		//判断转让利差大于通道费费率
		var json={refNo : mini.get("refNo").getValue()};
		CommonUtil.ajax( {
			url:"/TdFeesPassAgewayController/getSumFeeRateByRefNo",
			data:mini.encode(json),
			callback : function(data) {
				var feeRate = data.obj;
				if(sRate <= feeRate){
					mini.alert("转让利差应大于通道费费率","系统提示");
					mini.hideMessageBox(messageid);
					return false;
				}else{
					if(rate<=0){
						mini.alert("利率不能小于零!","系统提示");
						mini.hideMessageBox(messageid);
						return false;
					}
					if(remainAmt != osaleAmt){
						mini.alert("转让交易金额必须等于交易剩余本金","系统提示");
						mini.hideMessageBox(messageid);
						return false;
					}
					if(datas.osaleVdate < _bizDate){
						mini.alert("转让交易起息日期不能小于当前账务日期!","系统提示");
						mini.hideMessageBox(messageid);
						return false;
					}
					if(datas.osaleVdate > datas.orgiMdate){
						mini.alert("转让交易起息日期不能大于原交易到期日期!","系统提示");
						mini.hideMessageBox(messageid);
						return false;
					}
					var saveUrl = "/ApproveManageController/saveApprove";
					if(row.dealType=="2"){
						saveUrl = "/TradeManageController/saveTrade";
					}
					var params=mini.encode(datas);
					CommonUtil.ajax({
						url:saveUrl,
						data:params,
						callback:function(data){
							mini.hideMessageBox(messageid);
							mini.alert("保存成功","提示",function(){
								setTimeout(function(){top["win"].closeMenuTab()},10);
							});
						}
					});
				}
			}
		});
		
	}
	//起息日变更
	function osaleVdatechange(e){
		var formData=form.getData(true);
		var newData=formData.osaleVdate;
		var dDate='<%=__bizDate %>';
		var orgiMdate = formData.orgiMdate; 
		var refNo =mini.get("refNo").getValue();
		if(refNo == null) {
			return false;
		}
		if(CommonUtil.dateDiff(dDate,newData)<0 && action != "detail"){
			mini.alert("转让交易起息日期不能小于当前账务日期!","提示");
			return false;
		}
		if(CommonUtil.dateDiff(newData,orgiMdate) < 0 && action != "detail"){
			mini.alert("转让交易起息日期不能大于到期日期!","提示");
			return false;
		}
	}
	//转让利率变更
	function osaleRatechange(e){
		var newValue=e.value;
		var orgiRate =mini.get("orgiRate").getValue();
		
		if(orgiRate<=newValue){
			mini.alert("转让利率必须小于原交易利率","提示");
			mini.get("osaleRate").setValue(0);
			newValue = 0;
		}
		
		//转让利差(%)
		var osaleSrate = orgiRate - newValue;
		mini.get("osaleSrate").setValue(osaleSrate);
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
