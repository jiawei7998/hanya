<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
		<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>收息计划调整</legend>
<div id="InterestSchedule_from_div">
	<table class='mini-table' id="InterestSchedule" name="InterestSchedule" style="width:100%">
		<tr>
			<td><input id="cfType" name="cfType" class="mini-combobox" value="Interest"  labelField="true" data="CommonUtil.serverData.dictionary.cfType"  enabled="false" label="现金流类型" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="executeRate" name="executeRate" class="mini-spinner" changeOnMousewheel='false' format="p4"  labelField="true" required="true"   label="区间利率" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="refBeginDate" name="refBeginDate" class="mini-datepicker" labelField="true"  required="true"  label="区间起始日期" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="refEndDate" name="refEndDate" class="mini-datepicker" labelField="true"  required="true"  label="区间结束日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="theoryPaymentDate" name="theoryPaymentDate" class="mini-datepicker" labelField="true"  label="收息日期（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="interestAmt" name="interestAmt" class="mini-spinner" changeOnMousewheel='false'  labelField="true" maxValue="100000000000"  format="n2"   label="收息金额（计划）" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td colspan="2" >
				<div  class="mini-toolbar" style="text-align:left" >
					<a  id="int_add_btn" class="mini-button" style="display: none"    onclick="addint()">新增</a>
					<a  id="int_delete_btn" class="mini-button" style="display: none"    onclick="delint()">删除</a>
				</div>
			</td>
		</tr>
	</table>
</div>
</fieldset>
<fieldset style="min-width:0;">
		<legend>收息计划详情</legend>
<div id="interest_grid" class="mini-datagrid  borderAll" style="width:100%;" mini-fit="true"
	allowResize="true"  border="true" sortMode="client" showPager="false"
	pageSize="100"  multiSelect="true" 
	allowCellEdit="true" allowCellSelect="true" multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true">

	<div property="columns">
		<div type="indexcolumn"     headerAlign="center"  allowSort="false" width="40" >序号</div>
		<div type="checkcolumn"     headerAlign="center"></div>    
		
		<div field="cfType"         headerAlign= "center" allowSort= "true" width="120" type="comboboxcolumn" align="center">现金流类型
			<input class="mini-combobox" property="editor"
				enabled="false" data="CommonUtil.serverData.dictionary.cfType" />
		</div> 
		
		<div field="executeRate"  headerAlign= "center" allowSort= "true" width="120" align = "right"  numberFormat="p4">区间利率
			<input class="mini-spinner" required="true"  property="editor"
				minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
		</div>  
		
		<div field="refBeginDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">区间起始日期
			<input class="mini-datepicker"  required="true"  property="editor"/>
		</div>

		<div field="refEndDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">区间结束日期
			<input class="mini-datepicker"  required="true"  property="editor"/>
		</div> 

		<div field="theoryPaymentDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">收息日期（计划）
			<input class="mini-datepicker"  required="true"  property="editor"/>
		</div>

		
		<div field="interestAmt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00">收息金额（计划）
			<input class="mini-spinner" required="true"  property="editor"
				minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
		</div>             
		
		<div field="actualDate" headerAlign= "center" allowSort= "true" width="100" dateFormat="yyyy-MM-dd" align="center">收息日期（实际）
			<input class="mini-datepicker" property="editor" />
		</div>                          
		
		<div field="actualRamt"  headerAlign= "center" allowSort= "true" width="120" align = "right" dataType="float" numberFormat="#,0.00" >收息金额（实际）
			<input class="mini-spinner" property="editor"
				minValue="0" maxValue="999999999999999" changeOnMousewheel="false"/>
		</div>                              
	</div>
</div>
</fieldset>
<script type="text/javascript">

	mini.parse();
	top["interestScheduleTab"]=window;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var row=top["tradeExtendMangage"].getData(action);
	var _bizDate = '<%=__bizDate %>';
	var intGrid=mini.get("interest_grid");
	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			/******************************/
			CommonUtil.ajax( {
				url:"/tmCalCashFlowController/getApproveInterestForApprove",
				data:mini.encode({dealNo : row.dealNo,
				dealType:row.dealType}),
				callback : function(data) {
					data=mini.decode(data);
					intGrid.setData(data.obj);
				}
			});

		}else {
			intGrid.setData(row);
		}
	}
	//添加收息计划
	function addint(){
		var addform = new mini.Form("InterestSchedule_from_div");
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return
		};
		var row = addform.getData(true);
		intGrid.addRow(row, intGrid.data.length);
	}
	//删除收息计划
	function delint(){
		var rows = intGrid.getSelecteds();
		if (rows.length > 0) {
			mini.confirm("确定删除记录？", "确定?",
				function (action) {
					if (action == "ok") 
					{
						intGrid.removeRows(rows, true);
						mini.alert("删除成功!","消息提示");
					} 
				}
			);
		}else{
			mini.alert("请选择要删除的列!","消息提示");
		}
	}
	//获取表格数据
	function getGridData(){
		return intGrid.getData();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("int_add_btn").setVisible(false);
			mini.get("int_delete_btn").setVisible(false);
		}
		initform();
	});
		
</script>
</body>
</html>
