<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">利率重订</a> >> 修改</span>
<fieldset class="mini-fieldset title">
	<legend>交易详情</legend>

<div id="field_form">
	<table  class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手" style="width:100%;" labelStyle = "width:110px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="version" name="version">
			<input class="mini-hidden" id="aDate" name="aDate" />
			<input class="mini-hidden" id="dealDate" name="dealDate" />
			</td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="合同发起人" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponsor" name="sponsor" class="mini-hidden" /></td>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="合同发起机构" style="width:100%;" labelStyle = "width:110px;" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="mDate" name="mDate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="rate" name="rate" class="mini-spinner"  changeOnMousewheel="false"  labelField="true"  format="p4" enabled="false" label="投资利率" style="width:100%;" maxValue = "1000" labelStyle = "width:110px;" /></td>
		</tr>
		<tr>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"  changeOnMousewheel="false"  labelField="true" format="n2"  enabled="false" label="合同本金(元)" maxValue="900000000000" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="rateCode" name="rateCode" class="mini-buttonedit" onbuttonclick="onRateQuery" allowInput="false"  labelField="true" required="true"   label="利率代码" style="width:100%;" labelStyle = "width:110px;" /></td>
			<td><input id="rateCodeValue" name="rateCodeValue" class="mini-spinner"  changeOnMousewheel="false"  labelField="true"  format="p4" enabled="false" label="参考利率" style="width:100%;" labelStyle = "width:110px;" /></td>
		</tr>
		<tfoot>
			<tr>
				<td><input id="rateDiff" name="rateDiff" class="mini-spinner" allowNull="true"  changeOnMousewheel="false"  onvaluechanged="rateDiffchanged" format="n0" labelField="true" minValue="-100000" maxValue="100000"  required="true"   label="浮息点差(BP)" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="changeRate" name="changeRate" class="mini-spinner"  changeOnMousewheel="false" minValue="0" maxValue = "1000" labelField="true"  format="p4" enabled="false" label="重订后利率" style="width:100%;" labelStyle = "width:110px;" /></td>
				<td><input id="effectDate" name="effectDate" onvaluechanged="effectDateChanged" class="mini-datepicker"  labelField="true"    label="生效日期" style="width:100%;" labelStyle = "width:110px;" required="true" /></td>
			</tr>
			<tr>
				<td colspan="3"><input id="changeReason" name="changeReason" class="mini-textarea" required="true"  vtype="maxLength:500" labelField="true"  label="变更原因" style="width:100%;" labelStyle = "width:110px;" /></td>

			</tr>
		</tfoot>
	</table>
</div>
</fieldset>
<table class="mini-form" width="100%" cols="4">
	<tr>
		<td colspan="4" style="font-size:16px;border-bottom: 2px solid #B3DE94;border-top: 2px solid #B3DE94;">
			&nbsp;<span><font color="red">利率重订说明:</font></span><BR/>
			&nbsp;<span>"利率重订"只能调整浮动利率交易;浮动利率请挂钩LIBOR/SHOBOR/PBOC</span><BR/>
			&nbsp;<span>利率重订审批完成后，自动更改自"变更日"起到"原交易结束日"之间的利率</span>
		</td>
	</tr>
</table>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	// var row=top["rateResetManage"].getData(action);
	var _bizDate = '<%=__bizDate %>';
	var form=new mini.Form("#field_form");
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.refNo;
	tradeData.task_id=row.taskId;
	//加载信息
	function initform(){
	
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>利率重订</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("rateCode").setText(row.rateCode);
					mini.get("cName").setValue( data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue( data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("sponsor").setValue(data.user.userId);
					mini.get("sponsorName").setValue( data.user.userName);
					mini.get("sponInstName").setValue( data.institution.instName);
					mini.get("sponInst").setValue(data.institution.instId);
					mini.get("rateType").setValue(data.rateType);
					mini.get("vDate").setValue(data.vDate);
					mini.get("mDate").setValue(data.mDate);
					mini.get("dealNo").setValue(row.dealNo);
				}
			});
		}else {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>利率重订</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveForDuration",
				data:mini.encode(data),
				callback : function(data) {
					form.setData(data.obj);
					mini.get("effectDate").setValue(_bizDate);
					mini.get("dealNo").setValue('');
					mini.get("version").setValue(data.obj.version);
					mini.get("dealDate").setValue( _bizDate);
					mini.get("refNo").setValue( data.obj.dealNo);
					mini.get("cName").setValue( data.obj.counterParty.party_name);
					mini.get("cNo").setValue(data.obj.counterParty.party_id);
					mini.get("prdName").setValue( data.obj.product.prdName);
					mini.get("sponsorName").setValue( data.obj.user.userName);
					mini.get("rateCode").setValue( data.obj.rateCode);
					mini.get("rateCode").setText( data.obj.rateCode);
					mini.get("rate").setValue(data.obj.rate);
					mini.get("rateCodeValue").setValue(0);//默认为0
					mini.get("rateDiff").setValue(data.obj.rateDiff);
					mini.get("sponInstName").setValue( data.obj.institution.instName);
					mini.get("rate").setValue(data.obj.contractRate);
					mini.get("originalAmt").setValue( data.obj.amt);

					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("aDate").setValue("<%=__bizDate %>");

					CommonUtil.ajax({
						url : "/QdTtMktIrController/selectTtMktSeriessBySize",
						data : mini.encode({i_code : data.obj.rateCode,searchSize:1,"beg_date":_bizDate}),
						callback : function(data) {
							if(data.obj.length != 0){
								var rateDiff = mini.get("rateDiff").getValue();
								if(rateDiff !=0){
									rateDiff = rateDiff/10000;
								}
								mini.get("rateCodeValue").setValue(data.obj[0].s_close);
								mini.get("changeRate").setValue(data.obj[0].s_close + rateDiff);
							}
						}
					});
				}
			});
		}
	}

	function effectDateChanged(){
		var effectDateValue =mini.get("effectDate").getFormValue();
		var mDate=mini.get("mDate").getFormValue();
		if(effectDateValue<_bizDate||effectDateValue>mDate){
			mini.alert("生效日期必须大于等于账务日期，小于等于到期日","系统提示",function(){
				mini.get("effectDate").setValue("");
			});
		}
	}
	//
	function rateDiffchanged(){
		var diff = parseFloat(mini.get("rateDiff").getValue())/10000;
		var rate = parseFloat(mini.get("rateCodeValue").getValue());
		mini.get("changeRate").setValue(rate+diff);
	}
	//
	function onRateQuery(){
		var btnEdit = this;
		var icode = mini.get("rateCode").getValue();
		mini.open({
			url:CommonUtil.baseWebPath() +"/mini_cp/MiniIrSeriesMini.jsp?i_code=" + icode,
			title: "基准利率行情选择",
			width: 800,
			height: 400,
			ondestroy: function (action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data);    //必须
					if (data) {
						mini.get("rateCode").setValue(data.i_code);
						mini.get("rateCode").setText(data.i_code);
						mini.get("rateCodeValue").setValue(data.s_close);
						var changeRate = (mini.get("rateDiff").getValue())/10000 + mini.get("rateCodeValue").getValue() ;
						mini.get("changeRate").setValue(changeRate);
					}
				}

			}
		});
	}
	//保存信息
	function save(){
		var changeRate=mini.get("changeRate").getValue();
		var form=new mini.Form("#field_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		//判断转让利差大于通道费费率
		var json={refNo : mini.get("refNo").getValue()};
		CommonUtil.ajax( {
			url:"/TdFeesPassAgewayController/getBaseAssetListVo",
			data:mini.encode(json),
			callback : function(data) {
				var feeRate=0;
				for(var i=0;i<data.obj.length;i++){
					if(data.obj[i].feeSituation=="Pay"){
						feeRate=feeRate+parseFloat(data.obj[i].feeRate);
					}
				}
				if(changeRate<=feeRate){
					mini.alert("重订后利差应大于通道费费率","系统提示");
					mini.hideMessageBox(messageid);
					return false;
				}else{
					var datas=form.getData(true,false);
					datas['trdtype']="3001_RR";
					var saveUrl = "/ApproveManageController/saveApprove";
					if(row.dealType=="2"){
						saveUrl = "/TradeManageController/saveTrade";
					}
					var params=mini.encode(datas);
					CommonUtil.ajax({
						url:saveUrl,
						data:params,
						callback:function(data){
							mini.hideMessageBox(messageid);
							mini.alert("保存成功","提示",function(){
								setTimeout(function(){top["win"].closeMenuTab()},10);
							});
						}
					});
				}
			}
		});
		
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
