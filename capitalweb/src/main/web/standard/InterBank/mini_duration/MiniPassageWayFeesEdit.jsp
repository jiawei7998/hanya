<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<span id="labell"><a href="javascript:CommonUtil.activeTab();">通道计划</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>交易详情</legend>
<div id="PassageWayFeesEdit">
	<table id="field_form" class="mini-table" style="width:100%">
		<tr>
			<td><input id="refNo" name="refNo" class="mini-textbox"  labelField="true" enabled="false"  label="原交易单号" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="cName" name="cName" class="mini-textbox"  labelField="true" enabled="false" label="交易对手名称" style="width:100%;" labelStyle = "width:100px;" />
			<input class="mini-hidden" id="dealNo" name="dealNo"  >
			<input class="mini-hidden" id="cNo" name="cNo">
			<input class="mini-hidden" id="aDate" name="aDate">
			<input class="mini-hidden" id="version" name="version">
			<input id="sponsor" name="sponsor" class="mini-hidden" />
			<input id="sponInst" name="sponInst" class="mini-hidden" /></td>
			<td><input id="prdName" name="prdName" class="mini-textbox"  labelField="true" enabled="false" label="产品名称" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="acturalRate" name="acturalRate" class="mini-spinner"  changeOnMousewheel='false'   labelField="true"    format="p4" enabled="false" label="投资利率" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="originalAmt" name="originalAmt" class="mini-spinner"  changeOnMousewheel='false'   labelField="true"  maxValue="999999999999"  format="n2" enabled="false" label="原交易本金(元)" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="sponsorName" name="sponsorName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起人" style="width:100%;" labelStyle = "width:100px;" />
		</tr>
		<tr>
			<td><input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false" label="原交易发起机构" style="width:100%;" labelStyle = "width:100px;" />
			<td><input id="rateType" name="rateType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.rateType"  enabled="false" label="利率类型" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="remainAmt" name="remainAmt" class="mini-spinner"  changeOnMousewheel='false'   labelField="true"  maxValue="999999999999"  format="n2" enabled="false" label="持仓本金金额(元)" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="vDate" name="vDate" class="mini-datepicker"  labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="mDate" name="mDate" class="mini-datepicker"  labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="effectDate" name="effectDate" class="mini-datepicker"  labelField="true"   label="生效日期" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
	</table>
</div>
</fieldset>
<fieldset class="mini-fieldset">
	<legend>通道计划调整</legend>
<div id="PassageWayFees_from_div">
	<table class='mini-table' id="PassageWayFees" name="PassageWayFees" style="width:100%" >
		<tr>
			<td><input id="passagewayCno" name="passagewayCno" class="mini-buttonedit" onbuttonclick="onCustQuery"  maxLength="25" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  allowInput="true" labelField="true" label="客户编号" required="true"  style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="passagewayName" name="passagewayName" class="mini-textbox"  labelField="true"  label="客户名称" required="true"  vtype="maxLength:50" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="custType" name="custType" class="mini-combobox"  labelField="true" data="CommonUtil.serverData.dictionary.cType" required="true"  vtype="maxLength:10" label="客户类型" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="bigKind" name="bigKind" class="mini-combobox" labelField="true" required="true"  onvaluechanged="onBigCatChanged"  label="行业大类" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="midKind" name="midKind" class="mini-combobox" labelField="true"  label="行业中类" onvaluechanged="onMiddleCatChanged" data="CommonUtil.serverData.dictionary.MT0001" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="smallKind" name="smallKind" class="mini-combobox" labelField="true"  label="行业小类" style="width:100%;"  labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="contact" name="contact" class="mini-textbox" vtype="maxLength:15" labelField="true"  label="联系人" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="contactPhone" name="contactPhone" class="mini-textbox" vtype="maxLength:25" onvalidation="CommonUtil.onValidation(e,'mobile',[null])" labelField="true"  label="联系电话" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="contactAddr" name="contactAddr" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="联系地址" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="feeType" name="feeType" class="mini-combobox"  labelField="true" required="true"  data="CommonUtil.serverData.dictionary.feeCategory" value="pass" label="费用类型" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="intFre" name="intFre" class="mini-combobox" labelField="true" required="true"   data="CommonUtil.serverData.dictionary.CouponFrequently" label="费用频率" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="cashtDate" name="cashtDate" class="mini-datepicker" labelField="true"  label="扣划日期" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="feeSituation" name="feeSituation" class="mini-combobox" onvaluechanged="onFeeSituationChanged" labelField="true" required="true"  value="Pay" data="CommonUtil.serverData.dictionary.feeSituation" label="收付方向" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="isPass" name="isPass" class="mini-combobox" labelField="true" required="true"  data="CommonUtil.serverData.dictionary.YesNo" value="1" label="是否参与利息计算" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="feeCcy" name="feeCcy" class="mini-combobox" labelField="true" required="true"  enabled="false" data="CommonUtil.serverData.dictionary.Currency" value="CNY" label="费用币种" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="feeRate" name="feeRate" class="mini-spinner"  changeOnMousewheel='false'   labelField="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])"   format="p4" allowlimitvalue="false"  value="null" label="费率" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="feeBasis" name="feeBasis" class="mini-combobox" labelField="true" required="true"  data="CommonUtil.serverData.dictionary.DayCounter" value="Actual/360" label="计息基准" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="passagewayBankacccode" name="passagewayBankacccode" class="mini-textbox" maxLength = "32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" required="true"  labelField="true"  label="帐号" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td><input id="passagewayBankaccname" name="passagewayBankaccname" class="mini-textbox" required="true"  labelField="true"  label="账户名称" vtype="maxLength:50" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="passagewayBankcode" name="passagewayBankcode" class="mini-textbox" required="true"  labelField="true"  label="开户行行号" maxLength = "14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" style="width:100%;" labelStyle = "width:100px;" /></td>
			<td><input id="passagewayBankname" name="passagewayBankname" class="mini-textbox" required="true"  labelField="true"  label="开户行名称" vtype="maxLength:50" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td colspan="3"><input id="remark" name="remark" class="mini-textarea" labelField="true" vtype="maxLength:100" label="备注" style="width:100%;" labelStyle = "width:100px;" /></td>
		</tr>
		<tr>
			<td colspan="3" >
				<div  class="mini-toolbar" style="text-align:left" >
					<a  id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
					<a id="update_btn" class="mini-button" style="display: none"    onclick="update">修改</a>
					<a  id="delete_btn" class="mini-button" style="display: none"    onclick="del">删除</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onclick="clear">清空</a>
				</div>
			</td>
		</tr>
	</table>
</div>
</fieldset>
<fieldset style="min-width:0;">
	<legend>通道计划详情</legend>
<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:200px;"  mini-fit="true"
	allowResize="true"  border="true" sortMode="client" showPager="false"  multiSelect="true" 
	multiSelect="false" onrowdblclick="onRowDblClick">
	<div property="columns">
		<div type="indexcolumn" headerAlign="center"></div>
		<div type="checkcolumn" headerAlign="center"></div>    
		<div field="dealNo" width="180" align="center" headerAlign="center" allowSort="true" visible="false">交易单号</div>   
		<div field="refNo" width="180" align="center" headerAlign="center" allowSort="true" visible="false">原交易单号</div>   
		<div field="passagewayCno" width="100" headerAlign="center" allowSort="true">客户编号</div>   
		<div field="passagewayName" width="210" headerAlign="center" allowSort="true">客户名称</div>   
		<div field="contact" width="80" align="center" headerAlign="center" allowSort="true">联系人</div>   
		<div field="contactPhone" width="120"  headerAlign="center" allowSort="true">联系电话</div>   
		<div field="contactAddr" width="180"  headerAlign="center" allowSort="true">联系人地址</div>   
		<div field="bigKind" width="180" headerAlign="center" allowSort="true" visible="false">行业大类</div>   
		<div field="midKind" width="180" headerAlign="center" allowSort="true" visible="false">行业中类</div>   
		<div field="smallKind" width="180" headerAlign="center" allowSort="true" visible="false">行业小类</div> 
		<div field="bigKindName"      headerAlign="center" allowSort="true" width="120">行业大类名称</div>                                                               	
		<div field="midKindName"      headerAlign="center" allowSort="true" width="120">行业中类名称</div>                                                                     	
		<div field="smallKindName"    headerAlign="center" allowSort="true" width="120">行业小类名称</div>    
		<div field="feeType" width="150" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'feeCategory'}">费用类型</div>   
		<div field="intFre" width="150" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'CouponFrequently'}">费用频率</div>   
		<div field="cashtDate" width="100" align="center" headerAlign="center" allowSort="true">扣划日期</div>   
		<div field="custType"  width="120" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'cType'}">客户类型</div>                                
		<div field="isPass"  width="120" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否参与利息计算</div>                                
		<div field="feeCcy"  width="120" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">费用币种</div>                                
		<div field="feeBasis"  width="120" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'DayCounter'}">计息基础</div>                                
		<div field="feeSituation"  width="120" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'feeSituation'}">收付方向</div>                                
		<div field="feeRate" width="120" align="center" headerAlign="center" allowSort="true" numberFormat="p4">费率</div>   
		<div field="remark" width="180" align="center" headerAlign="center" allowSort="true">备注</div>   
		<div field="passagewayBankacccode" width="150" align="center" headerAlign="center" allowSort="true">银行帐号</div>   
		<div field="passagewayBankaccname" width="120" align="center" headerAlign="center" allowSort="true">账户名称</div>   
		<div field="passagewayBankcode" width="120" align="center" headerAlign="center" allowSort="true">开户行行号</div>   
		<div field="passagewayBankname" width="120" align="center" headerAlign="center" allowSort="true">开户行名称</div>   
	</div>
</div>
</fieldset>
<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
<div  class="mini-toolbar" style="text-align:left" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">

	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	// var row=top["PassageWayFeesManage"].getData(action);
	var _bizDate = '<%=__bizDate %>';
	var form=new mini.Form("#field_form");
	var addform = new mini.Form("PassageWayFees_from_div");
	var grid=mini.get("grid1");
	var partyAccData="";
	var totalfeeRate=0;
	var passagewayCnos=new Array();
	var tradeRate=0;
	var tradeData={};
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
	//客户选择
	function onCustQuery(){
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_cp/MiniCoreCustQueryEdits.jsp",
			title : "客户选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					var row = mini.clone(data); //必须
					var addform = new mini.Form("PassageWayFees_from_div");
					clear();
					if(row){
						if(row.party_id){
							row.cifNo=row.party_id;
						}
						if(row.party_name){
							row.custName=row.party_name;
						}
						var cNo =mini.get("cNo").getValue(); 
						// if(cNo == row.cifNo) {
						// 	mini.alert("费用方客户不能和交易对手方相同", "提示");
						// }else{
							mini.get("passagewayCno").setValue(row.cifNo);
							mini.get("passagewayCno").setText(row.cifNo);
							mini.get("passagewayCno").validate();
							mini.get("passagewayName").setValue(row.custName);
							mini.get("custType").setValue(row.custType);
							// if(row.party_bigkind != null) {
							// 	mini.get("bigKind").setValue(row.party_bigkind);//产品大类
							// 	mini.get("midKind").setValue(row.party_midkind);//产品中类
							// 	mini.get("smallKind").setValue(row.party_smallkind);//产品小类
							// } else {
							// 	mini.get("bigKind").setValue("");
							// 	mini.get("midKind").setValue("");
							// 	mini.get("smallKind").setValue("");
							// }
							//产品大类
							mini.get("bigKind").setValue(data.party_bigkind);
							//产品中类
							if(CommonUtil.isNotNull(data.party_bigkind)){
								CommonUtil.loadPartyKindDict("midKind",data.party_bigkind);
								mini.get("midKind").setValue(data.party_midkind);
							}
							
							//产品小类
							if(CommonUtil.isNotNull(data.party_midkind)){
								CommonUtil.loadPartyKindDict("smallKind",data.party_midkind);
								mini.get("smallKind").setValue(data.party_smallkind);
							}
							if (CommonUtil.isNotNull(row.constact_list) && row.constact_list.length > 0) {//联系人
								mini.get("contact").setValue(row.constact_list[0].name);
								mini.get("contactPhone").setValue(row.constact_list[0].cellphone);
								mini.get("contactAddr").setValue(row.constact_list[0].address);

							} else {
								mini.get("contact").setValue('');
								mini.get("contactPhone").setValue('');
								mini.get("contactAddr").setValue('');
							}

							CommonUtil.ajax({
								url : "/TdCustAccController/searchTdCustAccPage",
								data : mini.encode({custmerCode:row.cifNo, state:'6',cny:'CNY'}),
								callback : function(data) {
									if(data){
										if(data.obj.rows){
											partyAccData = data.obj.rows[0];
											if(partyAccData){
												mini.get("passagewayBankacccode").setValue(partyAccData.bankaccid);
												mini.get("passagewayBankaccname").setValue(partyAccData.bankaccname);
												mini.get("passagewayBankcode").setValue(partyAccData.openBankLargeAccno);
												mini.get("passagewayBankname").setValue(partyAccData.openBankName);
											}
										}
									}
								}
							});
						}
						
					}
				}
			// }
		});
	}

	function onFeeSituationChanged(e){
		var value = mini.get("feeSituation").getValue();
		if(value == 'Pay'){
			mini.get("isPass").setEnabled(true);
		}else{
			mini.get("isPass").setValue('0');
			mini.get("isPass").setEnabled(false);
			mini.get("isPass").setIsValid(true);
		}
	}

	//加载信息
	function initform(){
		if($.inArray(action,["edit","approve","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>通道计划</a> >> 详情");
			var params={};
			params['dealNo']=row.refNo;
			params['version']=row.version;
			CommonUtil.ajax({
				url:"/DurationController/getTdProductApproveMain",
				data:mini.encode(params),
				callback:function(data){
					form.setData(row);
					mini.get("refNo").setValue(data.dealNo);
					mini.get("cName").setValue(data.counterParty.party_name);
					mini.get("cNo").setValue(data.counterParty.party_id);
					mini.get("prdName").setValue(data.product.prdName);
					mini.get("originalAmt").setValue( data.amt);
					mini.get("rateType").setValue( data.rateType);
					mini.get("vDate").setValue( data.vDate);
					mini.get("mDate").setValue( data.mDate);
					mini.get("sponsor").setValue(userId);
					mini.get("sponInst").setValue(instId);
					mini.get("sponsorName").setValue(data.user.userName);
					mini.get("sponInstName").setValue(data.institution.instName);
					mini.get("version").setValue(data.version);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("acturalRate").setValue(  data.contractRate);
					mini.get("effectDate").setValue(  row.effectDate);
					tradeRate = data.contractRate;
				}
			});
			/******************************/
			CommonUtil.ajax( {
				url:"/DurationController/getPassageWayByDealNo",
				data:mini.encode({dealNo : row.dealNo}),
				callback : function(data) {
					grid.setData(data.obj);
				}
			});

		}else {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>通道计划</a> >> 新增");
			var data={dealNo:row.dealNo};
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveForDuration",
				data:mini.encode(data),
				callback : function(data) {
					form.setData(data.obj);
					mini.get("sponsor").setValue("<%=__sessionUser.getUserId()%>");
					mini.get("sponInst").setValue("<%=__sessionUser.getInstId() %>");
					mini.get("aDate").setValue(_bizDate);

					mini.get("effectDate").setValue( _bizDate);
					mini.get("dealNo").setValue('');
					mini.get("refNo").setValue( data.obj.dealNo);
					mini.get("cName").setValue( data.obj.counterParty.party_name);
					mini.get("cNo").setValue(data.obj.counterParty.party_id);
					mini.get("prdName").setValue( data.obj.product.prdName);
					mini.get("sponsorName").setValue( data.obj.user.userName);
					mini.get("sponInstName").setValue( data.obj.institution.instName);
					mini.get("version").setValue(data.obj.version);
					mini.get("acturalRate").setValue(  data.obj.contractRate);
					mini.get("originalAmt").setValue(  data.obj.amt);
					mini.get("remainAmt").setValue( row.settlAmt);
					tradeRate = data.obj.contractRate;
							
				}
			});

			/******************************/
			CommonUtil.ajax({
				url:"/TdFeesPassAgewayController/getBaseAssetListVo",
				data:{refNo:row.dealNo},
				callback : function(data) {
					grid.setData(data.obj);
				}
			});
		}
	}
	//保存信息
	function save(){
		form.validate();
		if(form.isValid() == false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var messageid = mini.loading("保存中, 请稍等 ...", "Loading");
		if(grid.data.length <= 0){
			mini.alert("请至少增加一条通道信息!","提示");
			mini.hideMessageBox(messageid);
			return;
		}
		var vDate=mini.get("vDate").getFormValue();
		var mDate=mini.get("mDate").getFormValue();
		var effectDate=mini.get("effectDate").getFormValue();
		var cashtDate = mini.get("cashtDate").getFormValue();
		var data=grid.getData(true,false);
		if(CommonUtil.isNotNull(cashtDate)){
			if(cashtDate < vDate || cashtDate > mDate){
				mini.alert("扣划日期必须大于等于起息日，小于等于到期日!","消息提示")
				mini.hideMessageBox(messageid);
				return false;
			}
		}
		if(effectDate<_bizDate||effectDate>mDate){
			mini.alert("生效日期必须大于等于账务日期，小于等于到期日!","消息提示")
			mini.hideMessageBox(messageid);
			return;
		}
		for(var i=0;i<data.length;i++){
			if(data[i].feeSituation == 'Pay'){
				tempRate = parseFloat(data[i].feeRate);
				totalfeeRate=totalfeeRate+tempRate;//利率相加的总和
			}
		}
		
		if (totalfeeRate >= tradeRate) {
			if(totalfeeRate==tradeRate){
				mini.alert("对应利率必须小于原交易利率","提示");
				mini.hideMessageBox(messageid);
				return false;
				
			}else{
				var moreRate = totalfeeRate - tradeRate;
				mini.alert("对应利率已超过交易利率" + mini.formatNumber(moreRate * 100, 'n4') + "%，请核对后重新输入", "提示");
				mini.hideMessageBox(messageid);
				totalfeeRate = 0;
				moreRate = 0;
				return false;
			}
			
		}
		var param=form.getData(true,false);
		param['trdtype']="3001_PASS";
		param['tdFeesPassAgeways'] = data;
		saveUrl = "/TradeManageController/saveTrade";
		var params=mini.encode(param);
		CommonUtil.ajax({
			url:saveUrl,
			data:params,
			callback:function(data){
				mini.hideMessageBox(messageid);
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}
	//
	function validateForm(form,flag){
		form.validate();
		if(form.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return false;
		}

		var row = form.getData(true);
		var feeRate = row.feeRate;
		if(CommonUtil.isNotNull(feeRate) && feeRate <= 0){
			mini.alert("费率必须大于0!","消息提示")
			return false;
		}

		var cashtDate = mini.get("cashtDate").getFormValue();
		if(CommonUtil.isNotNull(cashtDate)){
			var vDate = mini.get("vDate").getFormValue();
			var mDate = mini.get("mDate").getFormValue();
			var effectDate = mini.get("effectDate").getFormValue();
			// if(cashtDate < effectDate){
			// 	mini.alert("扣划日期必须大于生效日期!","消息提示")
			// 	return false;
			// }
			if(cashtDate < vDate || cashtDate > mDate){
				mini.alert("扣划日期必须大于等于起息日，小于等于到期日!","消息提示")
				return false;
			}
		}
		return true;
	}
	//添加通道计划
	function add(){
		if(validateForm(addform) == false){
			return;
		}
		addform.validate();
		if(addform.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
			return;
		};
		var data=grid.getData(true,false);
		var passagewayCno = mini.get("passagewayCno").getValue();
		for(var i=0;i<data.length;i++){
			passagewayCnos[i]=data[i].passagewayCno;
		}
		if($.inArray(passagewayCno,passagewayCnos) > -1){
			mini.alert( "该客户已经添加，请核对后重新输入", "提示");
			passagewayCnos.clear();
			return false;
		}	 
		var sponinst=mini.get("sponInst").getValue();
		
		var row = addform.getData(true);
		row.bigKindName = mini.get("bigKind").getText();
		row.midKindName = mini.get("midKind").getText();
		row.smallKindName = mini.get("smallKind").getText();
		var data=grid.getData(true,false);
		var currfeeRate = mini.get("feeRate").getValue();
	
		
		if (currfeeRate == 0) {
			mini.alert("费率不能为0，请核对后重新输入", "提示");
			return false;
		}
	
			
		grid.addRow(row, grid.data.length);
		clear();
	}
	//修改通道计划
	function update(){
		var row =  grid.getSelected();
		if(row){
			if(validateForm(addform) == false){
				return;
			}
			var data = addform.getData(true);
			data.bigKindName = mini.get("bigKind").getText();
			data.midKindName = mini.get("midKind").getText();
			data.smallKindName = mini.get("smallKind").getText();

			grid.updateRow(row,data);

			mini.alert("修改成功!","消息提示");
		}else{
			mini.alert("请选择要修改的列!","消息提示");
		}
	}
	//删除通道计划
	function del(){
		var rows = grid.getSelecteds();
		if (rows.length > 0) {
			mini.confirm("确定删除记录？", "确定?",
				function (action) {
					if (action == "ok") 
					{
						grid.removeRows(rows, true);
						mini.alert("删除成功!","消息提示");
					} 
				}
			);
		}else{
			mini.alert("请选择要删除的列!","消息提示");
		}
	}
	//清空
	function clear(){
		addform.clear();
		mini.get("feeCcy").setValue("CNY");
	}
	//查看详细
	function onRowDblClick(){
		var row=grid.getSelected();
		addform.setData(row,true,false);
		addform.setIsValid(true);

		var controls = mini.findControls(function(control){
			if(control.type == "buttonedit") return true;    //获取所有mini-textbox控件
		});
		for (var i = 0; i< controls.length; i++) 
		{
			if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
				controls[i].setText(controls[i].getValue());
			} 
		}

		CommonUtil.loadPartyKindDict("midKind",row.bigKind);
		mini.get("midKind").setValue(row.midKind);

		CommonUtil.loadPartyKindDict("smallKind",row.midKind);
		mini.get("smallKind").setValue(row.smallKind);
	}
	/*
	*
    *行业类型控制
	*
	*
	*
	**/

	//异步加载行业类型数据字典
	CommonUtil.loadPartyKindDict("bigKind", "-1");

	//产品大类控制产品中类
	function onBigCatChanged(e) {
		CommonUtil.loadPartyKindDict("midKind", mini.get("bigKind").getValue());
		mini.get("smallKind").setData([]);
	}

	//产品中类控制产品小类
	function onMiddleCatChanged(e) {
		CommonUtil.loadPartyKindDict("smallKind", mini.get("midKind").getValue());
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			mini.get("add_btn").setVisible(false);
			mini.get("delete_btn").setVisible(false);
			mini.get("update_btn").setVisible(false);
			mini.get("clear_btn").setVisible(false);
			form.setEnabled(false);
		}
		initform();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>
