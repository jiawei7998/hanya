<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>资产转让查询</legend>

<div id="TradeOutRightBuyMangage" >   
	<div id="search_form" class="mini-form" width="80%">
		<input id="sponInst" name="sponInst" class="mini-buttonedit" allowInput="false" onbuttonclick="onInsQuery" labelField="true"  label="所属机构：" labelStyle="text-align:right;" emptyText="请选择机构"/>
		<input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号：" labelStyle="text-align:right;" emptyText="请输入交易单号"/>
		<input id="refNo" name="refNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="原交易单号：" labelStyle="text-align:right;" emptyText="请输入原交易单号"/>
		<span style="float:right;margin-right: 150px">
			<a  id="search_btn" class="mini-button" style="display: none"   onclick="searchs(10,0)">查询</a>
			<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
		</span>
	</div>
</div>
</fieldset>
<span style="margin:2px;display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
	<a  id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
	<a  id="approve_log" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
	<a  id="print_bk_btn" class="mini-button" style="display: none"    onclick="print()">打印</a>
	<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" labelField="true" label="审批列表选择：" labelStyle="text-align:right;" 
				value="approve" textField="text" valueField="id" multiSelect="false"
				data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]" onvaluechanged="checkBoxValuechanged">
	</div>
</span>
<div id="TradeOutRightBuyMangage" class="mini-fit">  
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" multiSelect="true"
	idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true"  pageSize="10" sortMode="client" allowAlternating="true">
		<div property="columns" >
			<div type="indexcolumn" width="50" align="left"  headerAlign="center">序号</div>
			<div type="checkcolumn"></div>
			<div field="dealNo" width="180" align="left"  headerAlign="center" allowSort="true">交易单号</div>    
			<div field="refNo" width="180" align="left"  headerAlign="center" allowSort="true">原交易单号</div>                            
			<div field="party.party_name" width="210" align="left" headerAlign="center" >客户</div>
			<div field="dealType"  width="80" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealType'}">审批类型</div>                                
			<div field="approveStatus" width="80" align="center" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" >审批状态</div>
			<div field="orgiMdate" width="100" align="center" headerAlign="center" allowSort="true">原合同到期日</div>
			<div field="osaleVdate" width="100" align="center" headerAlign="center" allowSort="true">转让起息日</div>
			<div field="orgiRate" width="100" align="right" headerAlign="center" numberFormat="p4" allowSort="true">原合同利率</div>
			<div field="osaleRate" width="100" align="right" headerAlign="center" numberFormat="p4" allowSort="true">转让利率</div>
			<div field="osaleAmt" width="100" align="right" headerAlign="center" numberFormat="#,0.00" allowSort="true">转让金额(元)</div>
			<div field="remainAmt" width="100" align="right" headerAlign="center" numberFormat="#,0.00" allowSort="true">持仓本金金额(元)</div>
			<div field="lastUpdate" width="150" align="center" headerAlign="center" allowSort="true">更新时间</div>
			<div field="resReason" width="180" align="left" headerAlign="center" >备注</div>
		</div>
	</div>
</div>
<script>
	mini.parse();

	// top['tradeOutRightBuyManage']=window;
	var offsetDate ="<%=__bizDate %>";
	var row="";
	var grid = mini.get("datagrid");
	var form=new mini.Form("#search_form");
	function search(){
		searchs(10,0);
	}
	//查询
	function searchs(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['osaleType']="T";
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/DurationController/searchPageTradeOutRightBuyByMyself";
		}else if(approveType == "approve"){
			url = "/DurationController/searchPageTradeOutRightBuyUnfinished";
		}else{
			url = "/DurationController/searchPageTradeOutRightBuyFinished";
		}

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		searchs(pageSize,pageIndex);
	});

	grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		if(row.dealType == "1"){
			if(row.approveStatus == "3"){
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
			}
			if( row.approveStatus == "4" || row.approveStatus == "5"){
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
			}
		}
		if(row.dealType == "2"){
			if(row.approveStatus == "3"){
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
			}
			if( row.approveStatus == "11" || row.approveStatus == "15"){
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
			}
		}
	});
	
	function checkBoxValuechanged(e){
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			searchs(10, 0);
			var approveType = mini.get("approveType").getValue();
			if (approveType == "approve") {
				initButton();
			} else if (approveType == "finished") {
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
			} else {
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(true);
			}
		});
	}

	function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instId);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}
	
	//查看详情
	function onRowDblClick(){
		var row=grid.getSelected();
		var url = CommonUtil.baseWebPath() +"/mini_duration/MiniTradeOutRightBuyEdit.jsp?action=detail";
		var tab = {"id": "TradeOutRightBuyDetail",name:"TradeOutRightBuyDetail",url:url,title:"单笔转让详情",parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
		// top["win"].showTab(tab);
	}
	// 传递参数
	// function getData(action){
	// 	if($.inArray(action,["edit","approve","detail"])>-1){
	// 		row = grid.getSelected();
	// 	}
	// 	return row;
	// }
	//初始化按钮
	function initButton(){
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
			if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
			if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
			if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);
			if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
		});
	}
	/**************************事件定义********************************************/
	//清空
	function clear(){
		form.clear();
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		var dealNos = new Array();
		$.each(rows, function(i, n){
				dealNos.push(n.dealNo);
		});
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=dealNos;
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/DurationController/removeTradeOutRightBuy",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						searchs(10,0);
					}
				});
			}
		});
	}
	//修改
	function edit(){
		var row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/mini_duration/MiniTradeOutRightBuyEdit.jsp?action=edit";
			var tab = {"id": "TradeOutRightBuyEdit",name:"TradeOutRightBuyEdit",url:url,title:"单笔转让修改",parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		} else {
			mini.alert("请选中一条记录！","消息提示");
		}

	}
	//新增
	function add(e){
		var btnAdd = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_duration/MiniDurationTradeManage.jsp?businessType=TradeOutRightSaleZCZR&trdtype=3001_ORSALE&mDate="+offsetDate,
			title : "交易选择",
			width : 850,
			height : 500,
			ondestroy : function(action) {	
				if (action == "ok"){
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					row=data;
					var url=CommonUtil.baseWebPath() +"/mini_duration/MiniTradeOutRightBuyEdit.jsp?action=add";
					var tab={id:"TradeOutRightBuyAdd",name:"TradeOutRightBuyAdd",title:"单笔转让新增",url:url,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
					var paramData = {selectData:row};
					CommonUtil.openNewMenuTab(tab,paramData);
					// top["win"].showTab(tab);
				}

			}
		});
	}
	/**************************审批相关****************************/
	//审批日志查看
	function appLog(selections){
		var flow_type = null;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		if(selections[0].dealType == "1"){
			flow_type = "1";
		}else if(selections[0].dealType == "2"){
			flow_type = "4";
		}
		Approve.approveLog(flow_type,selections[0].dealNo);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("暂不支持多笔提交","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url=CommonUtil.baseWebPath() +"/mini_duration/MiniTradeOutRightBuyEdit.jsp?action=approve";
			var tab={id:"TradeOutRightBuyApprove",name:"TradeOutRightBuyApprove",title:"单笔转让审批",url:url,parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
			// top["win"].showTab(tab);
		}else{
			Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["dealNo"],Approve.OrderStatus.New,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	//打印
	function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].dealNo;
		
		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/PrintUtilController/exportload/print/304/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}

	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			initButton();
			searchs(10, 0);
		});
	});
</script>
</body>
</html>