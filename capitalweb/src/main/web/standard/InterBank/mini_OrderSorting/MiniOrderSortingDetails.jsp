<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title></title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js"  type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
    <span><a href="javascript:CommonUtil.activeTab();">还本计划确认</a> >> 还本计划确认信息</span>
    <div id="allData" width="100%">
		<fieldset class="mini-fieldset">
		<legend>审批单基础信息</legend>
		<div id="field_form">
			<table  class="mini-table" style="width:100%">
			<!-- 第一行 -->
			<tr>
				<td>
					<input id="dealNo" name="dealNo" class="mini-textbox"  labelField="true" enabled="false"  label="审批单号" style="width:100%;" labelStyle = "width:100px;" />
					<input id="dealType" name="dealType" class="mini-hidden" value="2"/>
					<input id="trdtype"  name="trdtype" class="mini-hidden" value="3001_RE"/>
					<input id="refPrd"   name="refPrd" class="mini-hidden"  value="1"/>
					<input id='cno'      name="cno" class="mini-hidden" value="1"/>
					<input id='orderSortNo' name="orderSortNo" class="mini-hidden" />
					<input id='cDealNo' name="cDealNo" class="mini-hidden" />
					<input id="approveStatus" name="approveStatus" class="mini-hidden" value="3"/>
				</td>
				<td>
					<input id="conTitle" name="conTitle" class="mini-textbox" value="来账分拣" labelField="true" enabled="false" label="业务名称" style="width:100%;" labelStyle = "width:100px;" />
				</td>
				<td>
					<input id="aDate" name="aDate" class="mini-textbox" value="<%=__bizDate %>" labelField="true" enabled="false"  label="审批开始日期" style="width:100%;" labelStyle = "width:100px;" />
				</td>
			</tr> 
			<!-- 第二行 -->
			<tr>
				<td>
					<input id="sponsor" name="sponsor" class="mini-hidden" value="<%=__sessionUser.getUserId()%>"/>
					<input id="sponsorName" name="sponsorName" class="mini-textbox" value="<%=__sessionUser.getUserName()%>" labelField="true" enabled="false"  label="审批发起人" style="width:100%;" labelStyle = "width:100px;"/>
				</td>
				<td> 
					<input id="sponInst" name="sponInst"  class="mini-hidden" value="<%=__sessionUser.getInstId() %>"/>
					<input id="sponInstName" name="sponInstName" class="mini-textbox"  labelField="true" enabled="false"  label="审批发起机构" style="width:100%;" labelStyle = "width:100px;"  value="<%=__sessionUser.getInstName() %>"    />
				</td>    
			</tr> 
			</table>
		</div>
		</fieldset>

		<fieldset class="mini-fieldset">
		<legend>业务信息</legend>
			<div id="deal_form">
			<table  id="dealForm" name="dealForm" class="mini-table" style="width:100%">
				<!-- 第一行 -->
				<tr>
					<td>
						<input id="refNo" name="refNo" class="mini-textbox"   labelField="true"   enabled="false" label="原交易编号" style="width:100%;" labelStyle = "width:100px;" />
					</td>
					<td>
						<input id="prdName" name="prdName" class="mini-combobox"   labelField="true"   enabled="false" label="业务名称" style="width:100%;" labelStyle = "width:100px;" />
					</td>
					<td>
						<input id="partyName" name="partyName" class="mini-textbox"   labelField="true"   enabled="false" label="交易对手" style="width:100%;" labelStyle = "width:100px;" />
					</td>
				</tr>
				<!-- 第二行 -->
				<tr>
				<td>
					<input id="vDate" name="vDate" class="mini-datepicker"   labelField="true"   enabled="false" label="起息日期" style="width:100%;" labelStyle = "width:100px;" />
				</td>
				<td>
					<input id="mDate" name="mDate" class="mini-datepicker"   labelField="true"   enabled="false" label="到期日期" style="width:100%;" labelStyle = "width:100px;" />
				</td>
				<td>
					<input id="term" name="term" class="mini-textbox"   labelField="true"   enabled="false" label="计息天数" style="width:100%;" labelStyle = "width:100px;" />
				</td>

				</tr>
				<!-- 第三行 -->
				<tr>
					<td>
						<input id="tDate" name="tDate" class="mini-datepicker"   labelField="true"   enabled="false" label="划款日期" style="width:100%;" labelStyle = "width:100px;" />
					</td>
					<td>
						<input id="meDate" name="meDate" class="mini-datepicker"   labelField="true"   enabled="false" label="到账日期" style="width:100%;" labelStyle = "width:100px;" />
					</td>
					<td>
						<input id="occupTerm" name="occupTerm" class="mini-textbox"   labelField="true"   enabled="false" label="资金占用天数" style="width:100%;" labelStyle = "width:100px;" />
					</td>
				</tr>
				<!-- 第四行 -->
				<tr>
					<td>
					<input id="intFre" name="intFre" class="mini-combobox"   labelField="true"   enabled="false" label="收息频率" style="width:100%;" labelStyle = "width:100px;" data="CommonUtil.serverData.dictionary.CouponFrequently"/>
					</td>
					<td>
					<input id="amtFre" name="amtFre" class="mini-combobox"   labelField="true"   enabled="false" label="还本频率" style="width:100%;" labelStyle = "width:100px;" data="CommonUtil.serverData.dictionary.CouponFrequently"/>
					</td>
				</tr>
			</table>
			</div>
		</fieldset>

		<fieldset class="mini-fieldset">
			<legend><label>同业现金流系统详细信息</label></legend>
			<!-- 修改 -->
			<div id="dataGrid" class="mini-datagrid  borderAll" style="width:100%;height:200px;" allowResize="true" border="true" sortMode="client"
				showPager="false" pageSize="10" multiSelect="true"  allowCellEdit="true" allowCellSelect="true"  allowAlternating="true"
				multiSelect="false" editNextOnEnterKey="true" editNextRowCell="true"   sortMode="client">
				<div property="columns">
					<div type="indexcolumn" headerAlign="center" allowSort="false" width="50">序号</div>
					<div field="revType" width="180" headerAlign="center"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'amtType'}" >现金流类型</div>
					<div field="refNo" width="160"  headerAlign="center" allowSort="true" visible="false"></div>
					<div field="srevAmt" width="160" headerAlign="center" align="right" allowSort="true" dataType="float" numberFormat="#,0.00">应收金额</div>
					<div field="revAmt" width="160"   headerAlign="center" align="right" allowSort="true" dataType="float" numberFormat="#,0.00">实收金额
						<input class="mini-spinner" property="editor" minValue="0" maxValue="999999999999999" changeOnMousewheel="false" required="true" />
					</div>
					<div field="revDate" width="160" headerAlign="center"  align="center" allowSort="true">收款日期</div>
					<div field="srevDate" width="160" headerAlign="center" align="center" allowSort="true" dateFormat="yyyy-MM-dd">实际收款日期</div>
				</div>
			</div>
		</fieldset> 
		<%@ include file="../mini_base/mini_flow/MiniApproveOpCommon.jsp"%>
		<div  class="miniui-elem-quote" style="margin-bottom:70px" >
			<a id="save_btn"  class="mini-button" style="display: none"    onclick="save()" >保存</a>
			<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close()">关闭</a>
		</div>
	</div>

<script type="text/javascript">
	mini.parse();
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;

	var action = params.action;
	var row = params.row;
	var dealNo = params.dealNo;
	var type = params.type;
	var refNo = row.refNo;
	
	var tradeData={};
	tradeData.selectData=row;
	tradeData.task_id=row.taskId;
	tradeData.serial_no=row.dealNo;
	tradeData.operType=action;
	var _bizDate = '<%=__bizDate %>';
	var grid=mini.get("dataGrid"); //修改
	var form = new mini.Form("field_form");

	//加载信息  业务信息框
	function initForm(){
		if ($.inArray(action,['edit','approve','detail']) > -1) {
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveByCondition",
				data:{'dealNo' : refNo},
				callback : function(data){
					mini.get("aDate").setValue(row.aDate);
					mini.get("sponsor").setValue(row.user.userId);
					mini.get("sponsorName").setValue(row.user.userName); 
					mini.get("sponInst").setValue(row.institution.instId);
					mini.get("sponInstName").setValue(row.institution.instName);
					mini.get("dealNo").setValue(row.dealNo);
					mini.get("orderSortNo").setValue(row.dealNo);
					mini.get("cDealNo").setValue(row.cDealNo);


					//原交易单编号
					mini.get("refNo").setValue(data.obj.dealNo); 
					//客户号
					mini.get("cno").setValue(data.obj.cNo);
					//业务名称
					mini.get("prdName").setValue(data.obj.product.prdName);
					//交易对手
					mini.get("partyName").setValue(data.obj.counterParty.party_name);
					//起息日
					mini.get("vDate").setValue(data.obj.vDate);
					//划款日期
					mini.get("tDate").setValue(data.obj.tDate);
					//到期日期
					mini.get("mDate").setValue(data.obj.mDate);
					//到账日期
					mini.get("meDate").setValue(data.obj.meDate);
					//计息天数
					mini.get("term").setValue(data.obj.term);
					//资金占用天数
					mini.get("occupTerm").setValue(data.obj.occupTerm);
					//收息频率
					mini.get("intFre").setValue(data.obj.intFre);
					//还本频率
					mini.get("amtFre").setValue(data.obj.amtFre);
					//原交易类型
					mini.get("refPrd").setValue(data.obj.prdNo);
				}
			});
			
			CommonUtil.ajax({
				url:"/TdOrdersortingApproveController/getOrderSortingDetail",
				data:{'dealNo':row.dealNo,'type':type},
				callback : function(data){
					grid.setData(data.obj); 
				}
			});
		}else{
			//新增
			var data= {dealNo : row.dealNo};
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveByCondition",
				data:mini.encode(data),
				callback : function(data){
					//原交易单编号 1
					mini.get("refNo").setValue(data.obj.dealNo); 
					//客户号
					mini.get("cno").setValue(data.obj.cNo);
					//业务名称2
					mini.get("prdName").setValue(data.obj.product.prdName);
					//交易对手3
					mini.get("partyName").setValue(data.obj.counterParty.party_name);
					//起息日5
					mini.get("vDate").setValue(data.obj.vDate);
					//划款日期6
					mini.get("tDate").setValue(data.obj.tDate);
					//到期日期7
					mini.get("mDate").setValue(data.obj.mDate);
					//到账日期8
					//
					mini.get("meDate").setValue(data.obj.meDate);
					//计息天数9
					mini.get("term").setValue(data.obj.term);
					//资金占用天数10
					mini.get("occupTerm").setValue(data.obj.occupTerm);
					//收息频率11
					mini.get("intFre").setValue(data.obj.intFre);
					//还本频率12
					mini.get("amtFre").setValue(data.obj.amtFre); 
					mini.get("refPrd").setValue(data.obj.prdNo);
				}
			});

			CommonUtil.ajax({
				url:"/OrderSortingController/getOrderSortingByType",
				data:mini.encode({'dealNo' : row.dealNo,'type':type,'serNo':row.serNo}),
				callback : function(data){
					/**
					dealType   revType   现金流类型
					actAmt     revAmt    实收金额
					refDate    revDate   收款日期 
					__bizDate  srevDate 实际收款日期
					serNo      refNo    现金流编号
					amAmt      srevAmt  应收金额 */
					for(var i = 0;i < data.obj.length; i++)
					{
						data.obj[i].revType = data.obj[i].dealType;//现金流类型
						data.obj[i].revAmt = data.obj[i].amAmt;//实收金额
						data.obj[i].revDate = data.obj[i].refDate;//收款日期 
						data.obj[i].srevDate = _bizDate;//实际收款日期
						data.obj[i].refNo = data.obj[i].serNo;//现金流编号
						data.obj[i].srevAmt = data.obj[i].amAmt;//应收金额
					}
					grid.setData(data.obj);
					mini.get("cDealNo").setValue(row.cDealNo);
				}
			});
		}// end if
	}// end function

	//保存
	function save(){
		var rowData = grid.getData();
		var refPrd = mini.get("refPrd").getValue();
		var rows = [];
		var refAmt = 0;
		for(var i = 0; i < rowData.length ; i++)
		{
			var model = {
				"revType" : rowData[i].revType,	
				"refNo"   : rowData[i].refNo,		
				"srevAmt" : rowData[i].srevAmt,
				"revAmt"  : rowData[i].revAmt,
				"revDate" : rowData[i].revDate,
				"srevDate": rowData[i].srevDate,	
				"dealNo"  : 'DD123',	
				"trdtype" : '3001_RE',
				"refPrd"  : refPrd,
			};
			rows.push(model);
			refAmt = refAmt + parseFloat(rowData[i].revAmt);
		}
		var param = form.getData(true,false);
		param['refNo'] = mini.get("refNo").getValue();
		param['refAmt'] = refAmt;
		param['jsonStr']= rows;
		var saveUrl = "/TdOrdersortingApproveController/saveTdOrdersortingApprove";
		if (rows.length > 0) {
			CommonUtil.ajax({
				url: saveUrl,
				data: mini.encode(param),
				callback: function (data) {
					mini.alert("保存成功", "提示", function () {
						setTimeout(function(){top["win"].closeMenuTab()},10);
					});	
				}
			});
		}else{
			mini.alert("无来账数据!", "提示");
			return;   
		}			
	}				
		
	//关闭表单
	function close(){
		top["win"].closeMenuTab();
	}
	
	$(document).ready(function()
	{
		if ($.inArray(action,['detail','approve']) > -1) 
		{
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false); 
		}
		initForm();
	});
</script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>
</body>
</html>