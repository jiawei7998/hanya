<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
<head>
<title>收息计划确认</title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script  src="<%=basePath%>/miniScript/approveFlow.js"   type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
		<legend>交易查询</legend>
		<div id="search_form"  style="width:100%;">
			<input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="审批单号：" labelStyle="text-align:right;"emptyText="请输入审批单号"/>
			<input id="refNo" name="refNo" class="mini-textbox"  labelField="true"  label="原交易单号：" labelStyle="text-align:right;" emptyText="请输入原交易单号"/>
			<input id="aDate" name="aDate" class="mini-datepicker" valueType='String' labelField="true"  label="审批发起时间：" labelStyle="text-align:right;" emptyText="请输入审批发起时间"/>
			<span style="float:right;margin-right: 150px">
					<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
					<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
			</span>
		</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
		<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
		<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
		<a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
		<a  id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
		<a  id="approve_log" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
		<a  id="print_bk_btn" class="mini-button" style="display: none"    onclick="print()">打印</a>
		<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" labelField="true" label="审批列表选择：" labelStyle="text-align:right;" 
					value="approve" textField="text" valueField="id" multiSelect="false"
					data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]" onvaluechanged="checkBoxValuechanged">
		</div>
	</span>	
	 <div id="OrderSortingManage" class="mini-fit">     
		<div id="datagrid2" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
					idField="dealNo" sortMode="client"  allowAlternating="true" 
					frozenStartColumn="0" frozenEndColumn="1" onrowdblclick="onRowDblClick" border="true"  >
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
				<div field="dealNo" width="180" headerAlign="center" align="center" allowSort="true">审批单编号</div>
				<div field="approveStatus" width="60" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div>
				<div field="user.userName" width="100" headerAlign="center">审批发起人</div>
				<div field="institution.instName" width="180" headerAlign="center" >审批发起机构</div>
				<div field="aDate" width="80" headerAlign="center" align="center">审批发起时间</div>
				<div field="refNo" width="160" headerAlign="center" align="center">原交易单号</div>
				<div field="cnName" width="200" headerAlign="center">交易对手</div>
				<div field="refAmt"  width="120"  numberFormat="#,0.00"headerAlign="center" align="right">来账总金额</div>
			</div>
		</div>  
	 </div>  
<script>
	mini.parse();
	top['MiniIntOrderSortingManager'] = window;
	var row;
	//返回全部setVisible(true)的按钮id

	var form=new mini.Form("search_form");
	var grid = mini.get("datagrid2");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	}); 
	//查询
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
		var data = form.getData();
		data['pageNumber'] = pageIndex+1;
		data['pageSize'] = pageSize;
		//data['prdNo'] = prdNo;
		data['dealType'] = '2';
		var url = null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){
			url = "/TdOrdersortingApproveController/searchTdOrdersortingApproveByMyselfByType?type=2";

		}else if(approveType == "approve"){
			url = "/TdOrdersortingApproveController/searchTdOrdersortingApproveUnfinishedByType?type=2";

		}else{
			url = "/TdOrdersortingApproveController/searchTdOrdersortingApproveFinishedByType?type=2";
		}

		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				//var grid = mini.get("datagrid2");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}// end search


	grid.on("select",function(e){
		var row = e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		/**
		1	:就绪,2	:计算中,3	:新建,4	:待审批,                 
		5	:审批中,6	:审批通过,10:执行完成,11:待核实,         
		15:核实中,12:核实完成,16:核实拒绝,13:出账中,         
		14:出账完成,17:交易冲正,7	:审批拒绝,8	:审批注销,     
		9	:执行中,1	:待经办,2	:待复核,3	:复核通过,0	:复核拒绝
		***/
		if(row.dealType == "1"){
			if(row.approveStatus == "3"){
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
			}
			if( row.approveStatus == "4" || row.approveStatus == "5"){  //MiniOrderSOrtingmanager1是11 和 15
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
			}
			if (row.approveStatus == "4") {
				mini.get("edit_btn").setEnabled(true);
			}
		}
		if(row.dealType == "2"){
			if(row.approveStatus == "3"){
				mini.get("approve_mine_commit_btn").setEnabled(true);
				mini.get("approve_commit_btn").setEnabled(false);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setEnabled(true);
			}
			if( row.approveStatus == "11" || row.approveStatus == "15" || row.approveStatus == "5"){
				mini.get("approve_mine_commit_btn").setEnabled(false);
				mini.get("approve_commit_btn").setEnabled(true);
				mini.get("edit_btn").setEnabled(false);
			}
		}
	});

	function query(){
		search(grid.pageSize,0);
	}

	//根据tab标签显示按钮  增加权限
	function checkBoxValuechanged(e){
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			query();
			var approveType = mini.get("approveType").getValue();
			if (approveType == "approve") {
				initButton();
			} else if (approveType == "finished") {
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
			} else {
				if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
				if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
				if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
				if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
				if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(true);
			}
		});
	}
	//初始化按钮
	function initButton(){
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
			if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
			if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
			if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);
			if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
		});
	}

	//参数传递
	function getData(action){
		var row;
		if($.inArray(action,['detail','edit','approve']) >-1){
			row = grid.getSelected();
		}
		return row;
	}
	
	//详情
	function onRowDblClick(){
		var row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/mini_OrderSorting/MiniOrderSortingDetails2.jsp";
			var tab = {
				id : "IntOrderSortingManagerDetails",
				name : "IntOrderSortingManagerDetails",
				iconCls : "",
				title : "收息计划确认详情",
				url : url,
				parentId:top["win"].tabs.getActiveTab().name,
				showCloseButton:true
			};
			var paramData = {'action':'detail','dealNo':row.dealNo,'type':'2','row':row};
			
			CommonUtil.openNewMenuTab(tab,paramData);

		} else {
			mini.alert("请选中一条记录！","消息提示");
		}  
	}

	//新增
	function add(e){
		var btnAdd = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_OrderSorting/MiniOrderSortingSelect.jsp?type=2",
			title : "选择分拣记录",
			width : 900,
			height : 510,
			allowDrag: true,         //允许拖拽位置
			showCloseButton: true,   //显示关闭按钮
			showMaxButton: true,     //显示最大化按钮
			ondestroy : function(action) {	
				if (action == "ok"){
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					var row = mini.clone(data); //必须
					var url = CommonUtil.baseWebPath() +"/mini_OrderSorting/MiniOrderSortingDetails2.jsp";
					var tab = {
						id : "OrderSortingManagerAdd2",
						name : "OrderSortingManagerAdd2",
						iconCls : "",
						title : "收息计划确认新增",
						url : url,
						parentId:top["win"].tabs.getActiveTab().name,
						showCloseButton:true
					};
					row.refNo = row.dealNo;
					var paramData = {'action':'add','dealNo':row.dealNo,'serNo':row.serNo,'type':'2',
						'closeFun':'query','pWinId':'MiniIntOrderSortingManager','row':row};
					
					CommonUtil.openNewMenuTab(tab,paramData);
				}

			}
		});
	}

	function edit(){
		row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/mini_OrderSorting/MiniOrderSortingDetails2.jsp";
			var tab = {
				id : "IntOrderSortingManagerEdit",
				name : "IntOrderSortingManagerEdit",
				iconCls : "",
				title : "收息计划确认修改",
				url : url,
				parentId:top["win"].tabs.getActiveTab().name,
				showCloseButton:true
			};
			var paramData = {'action':'edit','dealNo':row.dealNo,'type':'2',
				'closeFun':'query','pWinId':'MiniIntOrderSortingManager','row':row};
			CommonUtil.openNewMenuTab(tab,paramData);

		}  else {
			mini.alert("请选中一条记录！","消息提示");
		}  
	}

	function clear(){
		form.clear();
	}

	//删除
	function del(){
		row = grid.getSelected();
		if (row) {
			mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
				if (value=='ok'){
					CommonUtil.ajax( {
						url:"/TdOrdersortingApproveController/deleteTdOrdersortingApprove",
						data:row,
						callback : function(data) {
							mini.alert("删除成功.","系统提示");
							query();
						}
					});
				}
			});

		}else{
			mini.alert("请先选择一条记录","操作提示")
			return;	
		}
	}
	
	/////////////////////审批相关
	function appLog(selections){
		var flow_type = null;
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		if(selections[0].tradeSource == "3"){
			mini.alert("初始化导入的业务没有审批日志","系统提示");
			return false;
		}
		if(selections[0].dealType == "1"){
			flow_type = "1";
		}else if(selections[0].dealType == "2"){
			flow_type = "4";
		}
		Approve.approveLog(flow_type,selections[0].dealNo);
	};
	//提交正式审批、待审批
	function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("暂不支持多笔提交","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"/mini_OrderSorting/MiniOrderSortingDetails2.jsp";
			var tab = {
				id : "IntOrderSortingApprove",
				name : "IntOrderSortingApprove",
				iconCls : "",
				title : "收息计划确认审批",
				url : url,
				parentId : top["win"].tabs.getActiveTab().name,
				showCloseButton:true
			};
			var paramData = {'action':'approve','dealNo':selections[0]["dealNo"],'type':'2',
				'closeFun':'query','pWinId':'MiniIntOrderSortingManager','row':selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);

		}else{
			Approve.approveCommit("31",selections[0]["dealNo"],Approve.OrderStatus.New,function(){
				search(grid.pageSize,grid.pageIndex);
			});
		}
	};
	//审批
	function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}
	//提交审批
	function commit(){
		mini.get("approve_mine_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		}
		mini.hideMessageBox(messageid);	
		mini.get("approve_mine_commit_btn").setEnabled(true);
	}
	//审批日志
	function searchlog(){
		appLog(grid.getSelecteds());
	}
	//打印
	function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].dealNo;
		
		if(!CommonUtil.isNull(canPrint)){
			var actionStr = CommonUtil.pPath + "/sl/PrintUtilController/exportload/print/1802/" + canPrint;
			$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
		};
	}
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			checkBoxValuechanged();
			query();
		});
	});
		
</script>
</body>
</html>