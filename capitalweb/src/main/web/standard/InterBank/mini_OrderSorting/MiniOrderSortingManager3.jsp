<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>中收计划确认</title>
    <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>交易查询</legend>


    <div id="search_form" style="width:100%;">
        <input id="dealNo" name="dealNo" class="mini-textbox" onbuttonclick="onInsQuery" labelField="true" label="审批单号："
               labelStyle="text-align:right;" emptyText="请输入审批单号"/>
        <!-- <td><input id="refNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="原交易单号" style="width:100%;" emptyText="请输入原交易单号"/></td> -->
        <input id="dDate" name="dDate" class="mini-datepicker" labelField="true" label="交易时间："
               labelStyle="text-align:right;" emptyText="请输入交易时间"/>
        <span style="float:right;margin-right: 150px">
						<a id="search_btn" class="mini-button" style="display: none" onclick="query()">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none" onclick='clear()'>清空</a>
				</span>
        <!-- <div id="toolbar1" class="mini-toolbar" style="padding:2px;">
    <table style="width:100%;">
        <tr>
            <td style="width:100%;">
                    <a  id="clear_btn" class="mini-button" style="display: none"   onclick='clears()'>清空</a>
                    <a  id="search_btn" class="mini-button" style="display: none"    onclick="query()">查询</a>
                    <a  id="add_btn" class="mini-button" style="display: none"    onclick="add()">新增</a>
                    <a  id="edit_btn" class="mini-button" style="display: none"    onclick="edit()">修改</a>
                    <a  id="delete_btn" class="mini-button" style="display: none"    onclick="del()">删除</a>
                    <a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
                    <a  id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
                    <a  id="approve_log" class="mini-button" style="display: none"    onclick="searchlog()">审批日志</a>
                    <a  id="print_bk_btn" class="mini-button" style="display: none"   onclick="print()">打印</a>
                </td>
        </tr>
    </table>
</div> -->
    </div>
</fieldset>

<span style="margin:2px;display: block;">
		<a id="add_btn" class="mini-button" style="display: none" onclick="add()">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none" onclick="edit()">修改</a>
		<a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>
		<a id="approve_commit_btn" class="mini-button" style="display: none" onclick="approve()">审批</a>
		<a id="approve_mine_commit_btn" class="mini-button" style="display: none" onclick="commit()">提交审批</a>
		<a id="approve_log" class="mini-button" style="display: none" onclick="searchlog()">审批日志</a>
		<a id="print_bk_btn" class="mini-button" style="display: none" onclick="print()">打印</a>
		<div id="approveType" name="approveType" class="mini-checkboxlist" style="float:right;" labelField="true"
             label="审批列表选择：" labelStyle="text-align:right;"
             value="approve" textField="text" valueField="id" multiSelect="false"
             data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]"
             onvaluechanged="checkBoxValuechanged">
		</div>
	</span>
<!-- <div id = "approveType" name = "approveType" labelField="true" label="审批列表选择:"  class="mini-checkboxlist"  repeatItems="0" repeatLayout="flow"
  value="approve" textField="text" valueField="id" multiSelect="false"
  data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]"
  onvaluechanged="checkBoxValuechanged">
</div> -->

<div id="OrderSortingManage" class="mini-fit">
    <div id="datagrid3" class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client"
         allowAlternating="true"
         frozenStartColumn="0" frozenEndColumn="1" idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick"
         border="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
            <!-- <div type="checkcolumn"></div> -->
            <div field="dealNo" width="180" headerAlign="center" align="center" allowSort="true">审批单编号</div>
            <div field="dealType" width="180" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'DealType'}">审批类型
            </div>
            <div field="approveStatus" width="180" headerAlign="center" allowSort="true"
                 renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态
            </div>
            <div field="sponsorName" width="180" headerAlign="center" allowSort="true">审批发起人</div>
            <div field="sponInstName" width="180" headerAlign="center" allowSort="true">审批发起机构</div>
            <div field="aDate" width="180" headerAlign="center" align="center" allowSort="true">审批发起时间</div>
            <div field="refNo" width="180" headerAlign="center" align="center" allowSort="true">原交易单号</div>
            <div field="cno" width="180" headerAlign="center" allowSort="false">交易对手</div>
            <div field="remAmt" width="180" headerAlign="center" align="right" allowSort="false" numberFormat="#,0.00">
                来账总金额
            </div>
            <div field="refPrd" width="180" headerAlign="center" allowSort="true">原交易类型</div>
        </div>
    </div>
</div>
<script>
    mini.parse();
    top['datagrid3'] = window;
    //var gridtDate3 ="<%=__bizDate %>";
    var url = window.location.search;
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var prdName = CommonUtil.getParam(url, "prdName");

    //初始化方法 页面加载完成后被调用
    function init(param) {

    }

    //返回全部setVisible(true)的按钮id

    var grid = mini.get("datagrid3");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    function search(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息")
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        //data['prdNo'] = prdNo;
        //data['dealType'] = '2';
        var url = null;

        var approveType = mini.get("approveType").getValue();  //下面3个备注是还本计划和收息计划的url
        if (approveType == "mine") {
            url = "/TdOrdersortingApproveController/searchTdOrdersortingApproveByMyselfByType?type=2";
            ///TdOrdersortingApproveController/searchTdOrdersortingApproveByMyself

        } else if (approveType == "approve") {
            url = "/TdOrdersortingApproveController/searchTdOrdersortingApproveUnfinishedByType?type=2";
            ///TdOrdersortingApproveController/searchTdOrdersortingApproveUnfinished

        } else {
            url = "/TdOrdersortingApproveController/searchTdOrdersortingApproveFinishedByType?type=2";
            ///TdOrdersortingApproveController/searchTdOrdersortingApproveFinished
        }

        var params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                var grid = mini.get("datagrid3");
                grid.setTotalCount(data.obj.total);
                grid.setData(data.obj.rows);
            }
        });

    }// end search

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    grid.on("select", function (e) {
        var row = e.record;
        mini.get("approve_mine_commit_btn").setEnabled(false);
        mini.get("approve_commit_btn").setEnabled(false);
        mini.get("edit_btn").setEnabled(false);
        mini.get("delete_btn").setEnabled(false);
        mini.get("approve_log").setEnabled(true);
        if (row.dealType == "2") {
            if (row.approveStatus == "3") {
                mini.get("approve_mine_commit_btn").setEnabled(true);
                mini.get("approve_commit_btn").setEnabled(false);
                mini.get("edit_btn").setEnabled(true);
                mini.get("delete_btn").setEnabled(true);
            } else if (row.approveStatus == "4" || row.approveStatus == "5") {
                mini.get("approve_mine_commit_btn").setEnabled(false);
                mini.get("approve_commit_btn").setEnabled(true);
                mini.get("edit_btn").setEnabled(false);
            } else if (row.approveStatus == "11" || row.approveStatus == "15") {
                mini.get("approve_mine_commit_btn").setEnabled(false);
                mini.get("approve_commit_btn").setEnabled(true);
                if (row.approveStatus == "11") {
                    mini.get("edit_btn").setEnabled(true);
                } else {
                    mini.get("edit_btn").setEnabled(false);
                }
            }
        }
    });

    function query() {
        search(grid.pageSize, 0);
    }

    //根据tab标签显示按钮  增加权限
    function checkBoxValuechanged(e) {
        search(10, 0);
        //var approveType=this.getValue();
        var approveType = mini.get("approveType").getValue();
        if (approveType == "approve") {
            initButton();
        } else if (approveType == "finished") {
            if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
            if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
            if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
            if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
            if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
        } else {
            if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
            if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
            if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
            if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
            if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(true);
        }
    }

    //初始化按钮
    function initButton() {
        if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
        if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
        if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
        if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(true);
        if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
    }

    //参数传递
    function getData(action) {
        //var  row="";
        if ($.inArray(action, ['detail', 'edit', 'approve']) > -1) {
            row = grid.getSelected();
        }
        return row;
    }


    //详情
    function onRowDblClick() {
        var url = CommonUtil.baseWebPath() + "/mini_OrderSorting/MiniOrderSortingDetails3.jsp?action=detail";
        var tab = {
            "id": "OrderSortingManagerDetails3",
            "name": "OrderSortingManagerDetails3",
            text: "还本计划确认详情",
            url: url,
            parentId: top['win'].tabs.getActiveTab().name
        };
        top["win"].showTab(tab);
    }

    function add(e) {
        var btnAdd = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/mini_OrderSorting/MiniOrderSortingSelect.jsp?type=999",
            title: "选择分拣记录",
            width: 800,
            height: 500,
            allowDrag: true,         //允许拖拽位置
            showCloseButton: true,   //显示关闭按钮
            showMaxButton: true,     //显示最大化按钮
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    row = data;
                    var url = CommonUtil.baseWebPath() + "/mini_OrderSorting/MiniOrderSortingDetails3.jsp?dealNo=" + row.dealNo + "&type=999&contype=2";
                    var tab = {
                        id: "OrderSortingManagerAdd3",
                        name: "OrderSortingManagerAdd3",
                        text: "中收计划确认新增",
                        url: url
                    };
                    top['win'].showTab(tab);
                }

            }
        });
    }

    //修改
    function edit() {
        var row = grid.getSelected();
        if (row) {
            var url = CommonUtil.baseWebPath() + "/mini_OrderSorting/MiniOrderSortingDetails3.jsp?action=edit";
            var tab = {
                "id": "OrderSortingManagerEdit3",
                "name": "OrderSortingManagerEdit3",
                text: "中收计划确认修改",
                url: url,
                parentId: top['win'].tabs.getActiveTab().name
            };
            top["win"].showTab(tab);

        } else {
            mini.alert("请选中一条记录！", "消息提示");
        }
    }

    function clear() {
        var form = new mini.Form("#search_form");
        form.clear();
    }

    //删除

    function del() {
        var grid = mini.get("grid");

    }

    /////////////////////审批相关
    function appLog(selections) {
        var flow_type = null;
        if (selections.length <= 0) {
            mini.alert("请选择要操作的数据", "系统提示");
            return;
        }
        if (selections.length > 1) {
            mini.alert("系统不支持多笔操作", "系统提示");
            return;
        }
        if (selections[0].tradeSource == "3") {
            mini.alert("初始化导入的业务没有审批日志", "系统提示");
            return false;
        }
        if (selections[0].dealType == "1") {
            flow_type = "1";
        } else if (selections[0].dealType == "2") {
            flow_type = "4";
        }
        Approve.approveLog(flow_type, selections[0].dealNo);
    };

    //提交正式审批、待审批
    function verify(selections) {
        if (selections.length == 0) {
            mini.alert("请选中一条记录！", "消息提示");
            return false;
        } else if (selections.length > 1) {
            mini.alert("暂不支持多笔提交", "系统提示");
            return false;
        }
        if (selections[0]["approveStatus"] != "3") {
            var url = CommonUtil.baseWebPath() + "/mini_duration/MiniTrasForProductCapitalSchedule.jsp?action=approve";
            var tab = {
                id: "TrasForProductCapitalScheduleApprove",
                name: "TrasForProductCapitalScheduleApprove",
                text: "还本计划调整审批",
                url: url,
                parentId: "8-1-1"
            };
            top["win"].showTab(tab);
        } else {
            Approve.approveCommit(Approve.FlowType.VerifyApproveFlow, selections[0]["dealNo"], Approve.OrderStatus.New, function () {
                search(grid.pageSize, grid.pageIndex);
            });
        }
    };

    //审批
    function approve() {
        mini.get("approve_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_commit_btn").setEnabled(true);
    }

    //提交审批
    function commit() {
        mini.get("approve_mine_commit_btn").setEnabled(false);
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            verify(grid.getSelecteds());
        } catch (error) {

        }
        mini.hideMessageBox(messageid);
        mini.get("approve_mine_commit_btn").setEnabled(true);
    }

    //审批日志
    function searchlog() {
        appLog(grid.getSelecteds());
    }

    //打印
    function print() {
        var selections = grid.getSelecteds();
        if (selections == null || selections.length == 0) {
            mini.alert('请选择一条要打印的数据！', '系统提示');
            return false;
        } else if (selections.length > 1) {
            mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！', '系统提示');
            return false;
        }
        var canPrint = selections[0].dealNo;

        if (!CommonUtil.isNull(canPrint)) {
            var actionStr = CommonUtil.pPath + "/sl/PrintUtilController/exportload/print/700/" + canPrint;
            $('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
        }
        ;

    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            checkBoxValuechanged();
            search(10, 0);
        });
    });

</script>
</body>
</html>
>