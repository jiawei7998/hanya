<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset title">
		<legend>查询</legend>
		<div id="search_form" >
				<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" vtype="maxLength:50" label="业务编号：" emptyText='请输入业务编号' labelStyle="text-align:right;" />
				<input id="fuName" name="fuName" class="mini-textbox" labelField="true"  label="业务名称：" emptyText='请输入业务名称' labelStyle="text-align:right;" />
				
				<span style="float:right;margin-right: 150px">
						<a class="mini-button" style="display: none"  id="search_btn"  onclick="query()" >查询</a>
						<a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
				</span>
		</div>
	</fieldset>
	<div class="mini-fit">
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo"
		allowResize="true" onrowdblclick="onRowDblClick" border="true" frozenStartColumn="0" frozenEndColumn="1" sortMode="client"  >
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center" allowSort="false" width="40">序号</div>
				<div field="dealNo" width="170" align="left"  headerAlign="center" allowSort="true">业务编号</div>    
				<div field="dealName" width="70" align="center"  headerAlign="center" allowSort="false">业务名称</div>                            
				<div field="prdName" width="180" align="left" headerAlign="center" >产品名称</div>
				<div field="partyName"  width="200" align="left" headerAlign="center" allowSort="true">客户名称</div>                                
				<div field="amAmt" width="100" numberFormat="#,0.00"  headerAlign="center" align="right" allowSort="false" >应收金额</div>
				<div field="refDate" width="80" align="center" headerAlign="center" >收款日期</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	
	//参数获取
	var type=CommonUtil.getParam(url,"type");
	var dealNo=CommonUtil.getParam(url,"dealNo");

	var grid=mini.get("datagrid");
	grid.on("beforeload",function(e){
		e.cancel = true;
		var pageSize=e.data.pageSize;
		var pageIndex=e.data.pageIndex;
		search(pageSize,pageIndex);
	});

	//查询
	function search(pageSize,pageIndex){
		var form=new mini.Form("search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误","系统提示");
			return ;
		}
		var data=form.getData();
		data['pageSize']=pageSize;
		data['pageNumber']=pageIndex+1;
		data['opType']=1;
		data['type']=type;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/OrderSortingController/searchOrderSortingForSelect",
			data:params,
			callback : function(data) {
				var grid = mini.get("datagrid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	function query(){
		search(grid.pageSize,0);
	}

	//清空按钮
	 function clear(){
		 mini.get("dealNo").setValue("");
		 mini.get("fuName").setValue("");
	 }
	//关闭
	function close(){
		CloseWindow("ok")
	}
	//保存
	/* function save(){
		var grid=mini.get("datagrid");
		var row=grid.getSelected();
		if(CommonUtil.isNull(selectedObj)){
			mini.alert("提示","请选择一条同业收款记录!");
			return;
		}
		CommonUtil.ajax({
			url:"/OrderSortingController/ifExistOrderSorting",
			data:{'refNo':selectedObj.dealNo,orderType:type,refDate:selectedObj.refDate},
			callback : function(data){
				if(!data){
					var str='';
					if(type=='1'){
						str='还本计划确认';
					}else if(type=='2'){
						str='收息计划确认';
					}else{
						str='中收计划确认';
					}
					$.messager.alert('系统提示', '该交易已存在'+str+'业务');
					return false;
				}else{
					   CloseWindow("ok");
				}
				
			}
		});	
		
	} */
	
	function onRowDblClick(){
		var row=grid.getSelected();
		CommonUtil.ajax({
			url:"/OrderSortingController/ifExistOrderSorting",
			data:{'refNo':row.dealNo,orderType:type,refDate:row.refDate},
			callback : function(data){
				if (!data) {
					var str='';
					if(type=='1'){
						str='还本计划确认';
					}else if(type=='2'){
						str='收息计划确认';
					}else{
						str='中收计划确认';
					}
					mini.alert('该'+str+'业务已存在','系统提示');
					return false;
				}else{
					CommonUtil.ajax({
						url:"/DurationController/ifExistDuration",
						data:{'refNo':row.dealNo},
						callback : function(data) {
							if(data.obj != null){
								if(data.obj.sponsor != null){
									mini.alert('该交易已存在存续期业务<br>【业务类型：'+data.obj.remark+'】<br>【业务员：'+data.obj.sponsor+'】<br>【单号：'+data.obj.dealNo+'】','系统提示');
								}else{
									mini.alert('该交易已存在未到期存续期业务<br>【单号：'+data.obj.dealNo+'】','系统提示');
								}
								return false;
							}else{
								CloseWindow("ok");	
							}
						}
					});	
				}
			}
		});	
	}

	
	function CloseWindow(action) {
	   if (window.CloseOwnerWindow) return window.CloseOwnerWindow(action);
	   else window.close();
	}

	function GetData() {
	    return grid.getSelected();
	}


	$(document).ready(function(){
		search(grid.pageSize,0);
	});
</script>
	
