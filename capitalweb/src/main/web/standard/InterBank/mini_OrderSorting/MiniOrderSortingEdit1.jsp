<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>

    <script>
    var url=window.location.search;
    var prdNo=CommonUtil.getParam(url,"prdNo");
    var prdName=CommonUtil.getParam(url,"prdName");

    function init(param){
		var a = {"formType" : "1", "prdNo" : prdNo, "textAreaWidth" : "876.5", "fdsType" : "0"};
		var b = mini.encode(a);
		CommonUtil.ajax({
			data: b,
			url: "/ProductApproveController/searchPageTotalVerify",
			callback:function(data){
				var table = $("#productApprove_form");
				table.append(data.detail);
				mini.parse();

				initCom(param);
				
			}
		});
	}

    
    </script>

        <fieldset id="fd1" class="mini-fieldset">
                <legend><label>审批单基础信息</label></legend>
                    <div id="find" class="fieldset-body">
                        <table id="field_form"  class="form-table" width="100%">
                            <tr>
                                <td>审批单号:</td>
                                <td><input id="sdealNo" name="sdealNo" readonly="true" class="mini-textbox"/></td>
                                <td>业务名称:</td>
                                <td><input id="conTitle" name="conTitle" readonly="true" class="mini-textbox"/></td>
                           </tr>
                           <tr>
                                <td>审批发起人:</td>
                                <td><input id="sponsor" name="sponsor" readonly="true" class="mini-textbox"/></td>
                                <td>审批发起机构:</td>
                                <td><input id="sponorg" name="sponorg" readonly="true" class="mini-textbox"/></td>
                           </tr>
                           <tr>
                                <td>发起人手机:</td>
                                <td><input id="sponsorCellphone" name="sponsorCellphone" readonly="true" class="mini-textbox"/></td>
                                <td>审批开始日期:</td>
                                <td><input id="red-star" name="red-star" readonly="true" class="mini-textbox"/></td>
                           </tr>
                        </table>
                    </div>
    </fieldset>

    <fieldset id="fd2" class="mini-fieldset">
                <legend><label>业务信息</label></legend>
                    <div id="find2" class="fieldset-body">
                        <table id="field_form2"  class="form-table" width="100%">
                            <tr>
                                <td>原交易编号:</td>
                                <td><input id="refNo" name="refNo" readonly="true" class="mini-textbox"/></td>
                                <td>业务名称:</td>
                                <td><input id="prdName" name="prdName" readonly="true" class="mini-textbox"/></td>
                           </tr>
                           <tr>
                                <td>交易对手:</td>
                                <td><input id="partyName" name="partyName" readonly="true" class="mini-textbox"/></td>
                                <td>交易日期:</td>
                                <td><input id="dealDate" name="dealDate" readonly="true" class="mini-textbox"/></td>
                           </tr>
                           <tr>
                                <td>起息日期:</td>
                                <td><input id="vDate" name="vDate" readonly="true" class="mini-textbox"/></td>
                                <td>划款日期:</td>
                                <td><input id="tDate" name="tDate" readonly="true" class="mini-textbox"/></td>
                           </tr>

                           <tr>
                                <td>到息日期:</td>
                                <td><input id="mDate" name="mDate" readonly="true" class="mini-textbox"/></td>
                                <td>到款日期:</td>
                                <td><input id="meDate" name="meDate" readonly="true" class="mini-textbox"/></td>
                           </tr>

                           <tr>
                                <td>计息天数:</td>
                                <td><input id="term" name="term" readonly="true" class="mini-textbox"/></td>
                                <td>资金占用天数:</td>
                                <td><input id="occupTerm" name="occupTerm" readonly="true" class="mini-textbox"/></td>
                           </tr>

                           <tr>
                                <td>收息频率:</td>
                                <td><input id="intFre" name="intFre" readonly="true" class="mini-textbox"/></td>
                                <td>还本频率:</td>
                                <td><input id="amtFre" name="amtFre" readonly="true" class="mini-textbox"/></td>
                           </tr>
                           <tr>
                                <td>到期利息(元):</td>
                                <td><input id="term" name="term" readonly="true" class="mini-textbox"/></td>
                                <td>到期金额(元):</td>
                                <td><input id="occupTerm" name="occupTerm" readonly="true" class="mini-textbox"/></td>
                           </tr>
                        </table>
                    </div> 
    </fieldset>
    <fieldset id="fd3" class="mini-fieldset">
            <legend><label>同业现金流详细信息</label></legend>
                <div id="find3" class="fieldset-body">
                    <table id="field_form3"  class="form-table" width="100%">
                        <tr>
                            <td>现金流类型:</td>
                            <td><input id="serNo" name="serNo" readonly="true" class="mini-textbox"/></td>
                       </tr>
                       <tr>
                            <td>应收金额:</td>
                            <td><input id="srevAmt" name="srevAmt" readonly="true" class="mini-textbox"/></td>
                            <td>实收金额:</td>
                            <td><input id="revAmt" name="revAmt" readonly="true" class="mini-textbox"/></td>
                       </tr>
                       <tr>
                            <td>收款日期:</td>
                            <td><input id="revDate" name="revDate" readonly="true" class="mini-textbox"/></td>
                            <td>实际收款日期:</td>
                            <td><input id="srevDate" name="srevDate" readonly="true" class="mini-textbox"/></td>
                       </tr>
                    </table>
                </div>
</fieldset>
