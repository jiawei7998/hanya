<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
<h1 style="text-align:center">利率互换成交单</h1>
<div style="width:100%;">
	<div style="width:50%;float:left;text-align:center; ">
		<input id="dealDate" name="dealDate" class="mini-textbox "  labelField="true" style="font-size:18px;"  label="成交日期：" labelStyle="width:100px;" />
	</div>
	<div style="width:50%;float:left;text-align:center;">	
		<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" style="font-size:18px;" label="成交编号：" labelStyle="width:100px" />
	</div>
	<table id="field_form" style="text-align:left" class="mini-sltable">
		<tr>
			<td style="width:50%" > 
				<h3>固定利率支付方信息</h3>
				<input id="fixed_inst" name="fixed_inst" class="mini-textbox" labelField="true"  label="机构："  style="width:100%;"  labelStyle="width:80px;" />
				<input id="fixed_trader" name="fixed_trader" class="mini-textbox" labelField="true"  label="交易员："  labelStyle="width:80px" style="width:100%;"   />
				<input id="fixed_tel" name="fixed_tel" class="mini-textbox" labelField="true"  label="电话："  labelStyle="width:80px" style="width:100%;"   />
				<input id="fixed_fax" name="fixed_fax" class="mini-textbox" labelField="true"  label="传真："  labelStyle="width:80px" style="width:100%;"   />
				<input id="fixed_corp" name="fixed_corp" class="mini-textbox" labelField="true"  label="法人代表："  labelStyle="width:80px" style="width:100%;"   />
				<input id="fixed_addr" name="fixed_addr" class="mini-textbox" labelField="true"  label="地址："  labelStyle="width:80px" style="width:100%;"   />
			</td>
			<td>
				<h3>浮动利率支付方信息</h3>
				<input id="float_inst" name="float_inst" class="mini-textbox" labelField="true"  label="机构："  style="width:100%;"  labelStyle="width:80px" />
				<input id="float_trader" name="float_trader" class="mini-textbox" labelField="true"  label="交易员："  labelStyle="width:80px" style="width:100%;"   />
				<input id="float_tel" name="float_tel" class="mini-textbox" labelField="true"  label="电话："  labelStyle="width:80px" style="width:100%;"   />
				<input id="float_fax" name="float_fax" class="mini-textbox" labelField="true"  label="传真："  labelStyle="width:80px" style="width:100%;"   />
				<input id="float_corp" name="float_corp" class="mini-textbox" labelField="true"  label="法人代表："  labelStyle="width:80px" style="width:100%;"   />
				<input id="float_addr" name="float_addr" class="mini-textbox" labelField="true"  label="地址："  labelStyle="width:80px" style="width:100%;"   />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input id="prdName" name="prdName" class="mini-textbox" labelField="true"  label="产品名称"   labelStyle="width:80px" />
				<input id="term" name="term" class="mini-textbox" labelField="true"  label="期限"  labelStyle="width:80px"   />
				<input id="amt" name="amt" class="mini-textbox" labelField="true"  label="名义本金"  labelStyle="width:80px"   />
				<input id="expTerm" name="expTerm" class="mini-textbox" labelField="true"  label="计息天数调整"  labelStyle="width:100px"   />
				<input id="vDate" name="vDate" class="mini-textbox" labelField="true"  label="起息日"   labelStyle="width:80px" />
				<input id="first_vDate" name="first_vDate" class="mini-textbox" labelField="true"  label="首期起息日"  labelStyle="width:100px"   />
				<input id="expMDate" name="expMDate" class="mini-textbox" labelField="true"  label="到期日"  labelStyle="width:80px"   />
				<input id="expMeDate" name="expMeDate" class="mini-textbox" labelField="true"  label="支付日调整"  labelStyle="width:100px"   />
				<input id="countIns" name="countIns" class="mini-textbox" labelField="true"  label="计算机构"  labelStyle="width:80px"   />
				<input id="liquidWay" name="liquidWay" class="mini-textbox" labelField="true"  label="清算方式"  labelStyle="width:80px"   />
			</td>
		</tr>
		<tr>
			<td>
				<h3>固定利率明细</h3>
				<input id="fixedrate" name="fixedrate" class="mini-textbox" labelField="true"  label="固定利率(%)"  style="width:100%;"  labelStyle="width:100px" />
				<input id="fixed_paycycle" name="fixed_paycycle" class="mini-textbox" labelField="true"  label="支付周期"  labelStyle="width:80px" style="width:100%;"   />
				<input id="fixed_firstPday" name="fixed_firstPday" class="mini-textbox" labelField="true"  label="首期定期支付日"  labelStyle="width:100px" style="width:100%;"   />
				<input id="fixed_irbase" name="fixed_irbase" class="mini-textbox" labelField="true"  label="计息基准"  labelStyle="width:80px" style="width:100%;"   />
			</td>
			<td>
				<h3>浮动利率明细</h3>
				<input id="rateCodeValue" name="rateCodeValue" class="mini-textbox" labelField="true"  label="参考利率"  style="width:100%;"  labelStyle="width:80px" />
				<input id="rateDiff" name="rateDiff" class="mini-textbox" labelField="true"  label="利差(bps)"  labelStyle="width:80px" style="width:100%;"   />
				<input id="float_firstPday" name="float_firstPday" class="mini-textbox" labelField="true"  label="首期定期支付日"  labelStyle="width:100px" style="width:100%;"   />
				<input id="float_paycycle" name="float_paycycle" class="mini-textbox" labelField="true"  label="支付周期"  labelStyle="width:80px" style="width:100%;"   />
				<input id="FirstIrConfDay" name="FirstIrConfDay" class="mini-textbox" labelField="true"  label="首次利率确定日"  style="width:100%;"  labelStyle="width:100px" />
				<input id="resetFreq" name="resetFreq" class="mini-textbox" labelField="true"  label="重置频率"  labelStyle="width:80px" style="width:100%;"   />
				<input id="irWay" name="irWay" class="mini-textbox" labelField="true"  label="计息方式"  labelStyle="width:80px" style="width:100%;"   />
				<input id="float_irbase" name="float_irbase" class="mini-textbox" labelField="true"  label="计息基准"  labelStyle="width:80px" style="width:100%;"   />
			</td>
		</tr>
		<tr>
			<td>
				<h3>固定利率支付方账户</h3>
				<input id="fixed_accname" name="fixed_accname" class="mini-textbox" labelField="true"  label="资金账户户名："  style="width:100%;"  labelStyle="width:100px" />
				<input id="fixed_OpBank" name="fixed_OpBank" class="mini-textbox" labelField="true"  label="资金开户行："  labelStyle="width:100px" style="width:100%;"   />
				<input id="fixed_accnum" name="fixed_accnum" class="mini-textbox" labelField="true"  label="资金账号："  labelStyle="width:100px" style="width:100%;"   />
				<input id="fixed_PSNum" name="fixed_PSNum" class="mini-textbox" labelField="true"  label="支付系统行号："  labelStyle="width:100px" style="width:100%;"   />
			</td>
			<td>
				<h3>浮动利率支付方账户</h3>
				<input id="float_accname" name="float_accname" class="mini-textbox" labelField="true"  label="资金账户户名："  style="width:100%;"  labelStyle="width:100px" />
				<input id="float_OpBank" name="float_OpBank" class="mini-textbox" labelField="true"  label="资金开户行："  labelStyle="width:100px" style="width:100%;"   />
				<input id="float_accnum" name="float_accnum" class="mini-textbox" labelField="true"  label="资金账号："  labelStyle="width:100px" style="width:100%;"   />
				<input id="float_PSNum" name="float_PSNum" class="mini-textbox" labelField="true"  label="支付系统行号："  labelStyle="width:100px" style="width:100%;"   />
			</td>
		</tr>
	</table>
	<div style="width:50%;float:left; ">
		<span>*&nbsp;&nbsp;交易合同一方违约，违约方应根据双方约定向对手方承担违约责任</span>
	</div>
	<div style="width:50%;float:left; ">	
		<span style="float:right;font-size:20px">成交序列号：000007</span>
	</div>
<script type="text/javascript">
	$(document).ready(function(){
		var form=new mini.Form("#field_form");
		CommonUtil.ajax2({
			url:"standard/InterBank/mini_market/MiniIrSwap.txt",
			data:"",
			callback : function(data) {
				data=mini.decode(data);
				form.setData(data);
			}
		});
	});
</script>
</body>
</html>
