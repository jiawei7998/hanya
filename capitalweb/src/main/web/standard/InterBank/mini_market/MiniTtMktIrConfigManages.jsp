<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>基准利率来源查询</legend>
<div id="search_form" style="width:100%;">
	<div class="mini-form" >
		<input id="i_code" name="i_code" type="text"  labelField="true"  label="基准利率代码：" labelStyle="text-align:right;"   allowInput='false' emptyText='请选择基准利率代码' onbuttonclick="onButtonEdit" class="mini-buttonedit searchbox" />
		<span style="float:right;margin-right: 150px">
			<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
			<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
		</span>
	</div>
</div>
</fieldset> 
<span style="margin:2px;display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
</span>	 
<div class="mini-fit"  >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
			onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" align="center"  headerAlign="center">序号</div>
			<div field="i_code" width="100" align="left"  headerAlign="center">基准利率代码</div>    
			<div field="ccy" width="100" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>                            
			<div field="term" width="100" align="left"  headerAlign="center">期限</div>
			<div field="sourceSys" width="100" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'SourceSys'}">数据来源系统</div>                                
			<div field="dataSource" width="200" align="left"  headerAlign="center">数据来源配置</div>
		</div>
	</div>
</div>   
<script type="text/javascript">
	mini.parse();	
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");

	top['ttMktIrManage']=window;
	var row="";
	grid.on("beforeload",function(e){
		e.cancel=true;
		var pageIndex=e.data.pageIndex;
		var pageSize=e.data.pageSize;
		search(pageSize,pageIndex);
	});
	//	查询信息
	function search(pageSize,pageIndex){
		form.validate();
		if (form.isValid()==false) return;
		//提交的数据
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
	 	data['pageSize']=pageSize;
		var params=mini.encode(data);
		//发送ajax请求
		CommonUtil.ajax({
			url:'/TtMktIrConfigController/selectTtMktIrConfigPage',
			data:params,
			callback : function(data) {
				var grid=mini.get("datagrid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/*
	加载基准利率列表
	*/
	function onButtonEdit(e){
		 var btnEdit = this;
         mini.open({
			 url:CommonUtil.baseWebPath() +"/mini_market/MiniIrSelectManages.jsp",
             title: "基准利率查询",
             width: 800,
             height: 600,
             ondestroy: function (action) {
                 if (action == "ok") {
                     var iframe = this.getIFrameEl();
                     var data = iframe.contentWindow.GetData();
                     data = mini.clone(data);    //必须
                     if (data) {
                         btnEdit.setValue(data.i_code);
                         btnEdit.setText(data.i_code);
                     }
                 }

             }
         });
	}
	function GetData() {
		var grid=mini.get("datagrid");
		var row = grid.getSelected();
		return row;
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
	}
	//查看详情
	function onRowDblClick(){
		var url = CommonUtil.baseWebPath() +"/mini_market/MiniTtMktIrConfigEdits.jsp?action=detail";
		var tab = {"id": "TtMktIrConfigDetail",url:url,text:"基准利率来源详情",parentId:top["win"].tabs.getActiveTab().name};
		top["win"].showTab(tab);
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/mini_market/MiniTtMktIrConfigEdits.jsp?action=add";
		var tab = {"id": "TtMktIrConfigAdd",url:url,text:"基准利率来源添加",parentId:top["win"].tabs.getActiveTab().name};
		top["win"].showTab(tab);
	}
	//修改
	function edit(){
		var url = CommonUtil.baseWebPath() +"/mini_market/MiniTtMktIrConfigEdits.jsp?action=edit";
		var tab = {"id": "TtMktIrConfigEdit",url:url,text:"基准利率来源修改",parentId:top["win"].tabs.getActiveTab().name};
		top["win"].showTab(tab);
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/TtMktIrConfigController/daleteTtMktIrConfig",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});	
</script>
</body>
</html>