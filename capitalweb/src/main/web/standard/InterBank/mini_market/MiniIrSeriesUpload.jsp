<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <%@ include file="../../global.jsp"%> 
 <div id="stockManage" class="mini-fit" >  
	<div style="margin: 10px;">
		基准利率导入：<input class="mini-htmlfile" name="Fdata"  id="file1" style="width:300px;"/>
		<a id="uploadBtn" class="mini-button" style="display: none"    onclick="ajaxFileUpload">上传</a>
    </div>
    </div>
	<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
<script>
	 
	  function ajaxFileUpload() {
   
        var inputFile = $("#file1 > input:file")[0];
        var messageid = mini.loading("加载中,请稍后... ", "提示信息");
        $.ajaxFileUpload({
            url: '<%=basePath%>/sl/QdTtMktIrController/importIrByExcel', //用于文件上传的服务器端请求地址
            fileElementId: inputFile,               //文件上传域的ID
            //data: { a: 1, b: true },            //附加的额外参数
            dataType: 'text',    
            //返回值类型 一般设置为json
            success: function (data, status)    //服务器成功响应处理函数
            {
            	mini.hideMessageBox(messageid);
                mini.alert("提示: " + data);

            },
            error: function (data, status, e)   //服务器响应失败处理函数
            {
            	mini.hideMessageBox(messageid);
                mini.alert(e);
            },
            complete: function () {
                var jq = $("#file1 > input:file");
                jq.before(inputFile);
                jq.remove();
            }
        });
    }
</script>
      	