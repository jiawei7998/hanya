<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
	<body style="width:100%;height:100%;background:white">
		<fieldset class="mini-fieldset">
			<legend>基准利率查询</legend>
		
			<div style="width:100%;">
				<table id="search_form" width="100%">
					<tr>
						<td><input id="ir_name" name="ir_name" type="text" class="mini-textbox" labelField="true" labelStyle="width:50px;"  label="名称:"  style="width:80%;" emptyText="请输入基准利率名称" /></td>
						<td><input id="i_code" name="i_code" type="text" class="mini-textbox" labelField="true" labelStyle="width:50px;" label="代码:"  style="width:80%;" emptyText="请输入基准利率代码" /></td>	
						<td>
							<a onClick="query"  id="search_btn" class="mini-button" style="display: none"  >查询</a>
						</td>
					</tr>
				</table>
			</div> 
		</fieldset>
		<div class="mini-fit" >
			<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id" allowResize="true" 
				onrowdblclick="onRowDblClick" sizeList="[10,20,30,50]" pageSize="10" fit="true" border="true" allowAlternating="true" sortMode="client">
				<div property="columns">
					<div type="indexcolumn" ></div>
					<div field="i_code" width="120">基准利率代码</div>    
					<div field="ir_name" width="120">名称</div>                            
					<div field="currency" width="100">币种</div>
					<div field="ir_term" width="100" >期限</div>                                
					<div field="imp_date" width="100" >导入日期</div>
				</div>
			</div>
		</div>   

		<script>
			mini.parse();
			var grid=mini.get("datagrid");
			var url=window.location.search;
        	var action=CommonUtil.getParam(url,"action");
			grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				search(pageSize,pageIndex);
			});

			function search(pageSize,pageIndex){
				var form = new mini.Form("#search_form");
				form.validate();
				if (form.isValid()==false) return;
				//提交的数据
				var data=form.getData();
				
				data['pageNumber']=pageIndex+1;
				data['pageSize']=pageSize;
				var params=mini.encode(data);
				//发送ajax请求
				CommonUtil.ajax({
					url:'/IrController/searchPageIr',
					data:params,
					callback : function(data) {
					var grid=mini.get("datagrid");
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
					}
				});
			}

			function query(){
				search(grid.pageSize,0);
			}
			function onRowDblClick(e) {
				if(action == "add" ){
				var row=grid.getSelected();
				CommonUtil .ajax({
					url:'/TtMktIrConfigController/selectTtMktIrConfigPage',
					data:({'i_code':row.i_code}),
					callback : function(data){
						if(data.obj.total !=0){
							mini.alert("基准利率已配置，不可重复新建","系统提示");
							return  false;
						}else{
							onOk();
						}
					}
				});
				}else{
					onOk();
				}
				
			}

			function CloseWindow(action) {
				if (window.CloseOwnerWindow){
					 return window.CloseOwnerWindow(action);
				}else{ 
					window.close();
				}
			}
			
			function onOk() {
				CloseWindow("ok");
			}
			
			function GetData() {
				var grid=mini.get("datagrid");
				var row = grid.getSelected();
				return row;
			}

			//添加
			function add(){
				var url = CommonUtil.baseWebPath() +"/mini_market/MiniIrEdits.jsp";
				var tab = {"id": "IrEdit",url:url,text:"基准利率添加"};
				top["win"].showTab(tab);
			}
			$(document).ready(function(){
				query();
			});
		</script>
	</body>
</html>
