<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>基准利率行情查询</legend>
	<div>
		<div id="search_form" style="width:100%">
			<input id="ir_search" name="ir_search" type="text"  labelField="true"  label="基准利率："  labelStyle="text-align:right;" onbuttonclick="onButtonEdit" class="mini-buttonedit searchbox" />
			<input id="beg_date" name="beg_date"  class="mini-datepicker" valueType="String" labelField="true"  label="市场数据日期："  labelStyle="text-align:right;" />
			<span style="float:right;margin-right: 150px">
				<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<span style="margin:2px;display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a id="submit_btn" class="mini-button" style="display: none"      onclick="submit()">提交复核</a>
	<a id="irImport_btn" class="mini-button" style="display: none"      onclick="irImport()">基准利率导入</a>
</span>		
<div class="mini-fit">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true" multiSelect="true"
			onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<!-- <div type="checkcolumn" ></div> -->
			<div field="i_code" width="120px"  align="left"  headerAlign="center" >基准利率代码</div>    
			<!-- <div field="ir_name" width="90px" align="left"  headerAlign="center" >基准利率名称</div>                             -->
			<div field="status" width="100px" align="left"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DoubleApproveStatus'}">审批状态</div>
			<div field="beg_date" width="100px" align="center"  headerAlign="center"  >市场数据日期</div>                                
			<div field="s_close" width="100px"   headerAlign="center" numberFormat="p4" align="right" >收盘价</div>
			<div field="s_high" width="100px"   headerAlign="center" numberFormat="p4" align="right" >最高价</div>
			<div field="imp_date" width="100px" align="center"  headerAlign="center"  >导入日期</div>
		</div>
	</div>
</div>   
<script type="text/javascript">
	mini.parse();
	top['irSeriesManage']=window;
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";
	//分页
	grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				search(pageSize,pageIndex);
			});
	
	function search(pageSize,pageIndex){
		var form= new mini.Form("#search_form");
		form.validate();
		if (form.isValid()==false) return;
		//提交的数据
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:'/QdTtMktIrController/selectTtMktSeriessPage',
			data:params,
			
			callback : function(data) {
				var grid=mini.get("datagrid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//根据审批状态判断按钮是否启用
	grid.on("select",function(e){
		var row=e.record;
		if(row.status == "0"){
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(false);
			mini.get("submit_btn").setEnabled(false);
		}else if(row.status == "1"){
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("submit_btn").setEnabled(true);
		}else  if(row.status == "2"){
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("submit_btn").setEnabled(false);
		}else if(row.status == "3"){
			//mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(false);
			mini.get("submit_btn").setEnabled(false);
		}

	});

	/*
	加载基准利率列表
	*/
	function onButtonEdit(e){
	 var btnEdit = this;
	 mini.open({
	     url:CommonUtil.baseWebPath() +"/mini_market/MiniIrManages.jsp",
	     title: "基准利率查询",
	     width: 800,
	     height: 600,
	     ondestroy: function (action) {
	         if (action == "ok") {
	             var iframe = this.getIFrameEl();
	             var data = iframe.contentWindow.GetData();
	             data = mini.clone(data);    //必须
	             if (data) {
	                 btnEdit.setValue(data.ir_name);
	                 btnEdit.setText(data.ir_name);
	             }
	         }
	
	     }
	 });
	}
	function GetData() {
		var grid=mini.get("datagrid");
		var row = grid.getSelected();
		return row;
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
	}
	//提交复核
	function submit(){
		var selected= grid.getSelected();
		if(!selected){
			mini.alert("请先选择一条记录！","提示")
			return false;
		}
		CommonUtil.ajax( {
			url:'/QdTtMktIrController/TtMktSeriessVerify',
			data:selected,
			callback : function(data) {
				mini.alert("提交成功.","提示");
				search(10,0);
			}
		});
	}
	//基准利率导入
	function irImport(){
		mini.open({
			url: CommonUtil.baseWebPath() +"/mini_market/MiniIrSeriesUpload.jsp",
			width: 420,
			height: 150,
			title: "文件上传",
			ondestroy: function (action) {
				search(10,0);//重新加载页面
			}
		});
	}
	//查看详情
	function onRowDblClick(){
		var url = CommonUtil.baseWebPath() +"/mini_market/MiniIrSeriesEdits.jsp?action=detail";
		var tab = {"id": "IrSeriesDetail",url:url,text:"基准利率行情详情",parentId:top["win"].tabs.getActiveTab().name};
		top["win"].showTab(tab);
	}
	//添加
	function add(){
		var url = CommonUtil.baseWebPath() +"/mini_market/MiniIrSeriesEdits.jsp?action=add";
		var tab = {"id": "IrSeriesEdit",url:url,text:"基准利率行情添加",parentId:top["win"].tabs.getActiveTab().name};
		top["win"].showTab(tab);
	}
	//修改
	function edit(){
		var url = CommonUtil.baseWebPath() +"/mini_market/MiniIrSeriesEdits.jsp?action=edit";
		var tab = {"id": "IrSeriesEdit",url:url,text:"基准利率行情修改",parentId:top["win"].tabs.getActiveTab().name};
		top["win"].showTab(tab);
	}
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		////debugger
		mini.confirm("您确认要删除选中记录?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				CommonUtil.ajax( {
					url:"/IrController/deleteIrSeries",
					data:data,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	$(document).ready(function(){
	 search(10,0);
	
	
	});	
</script>
</body>
</html>


