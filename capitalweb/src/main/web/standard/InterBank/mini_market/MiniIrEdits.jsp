<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">基准利率</a> >> 修改</span>
<!-- <fieldset class="mini-fieldset">
	<legend>基准利率详情</legend> -->
<div id="IrEdit" >   
	<table id="ir_form" class="mini-table"  width="100%" >
		<tr>
			<td><input id="ir_name" name="ir_name" type="text" class="mini-textbox" labelField="true" required='true' label="名称"  style="width:80%;" labelStyle="width:80px"
				maxLength = "7"  onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/></td>
			<td><input id="ir_term" name="ir_term" type="text" class="mini-combobox" labelField="true"  label="期限" required="true"  data = "CommonUtil.serverData.dictionary.Term" labelStyle="width:80px" style="width:80%;"  /></td>
			<td><input id="i_code" name="i_code" type="text" class="mini-textbox" labelField="true"  enabled="false"   label="基准利率代码"  style="width:80%;" labelStyle="width:80px" /></td>
		</tr>
		<tr>
			<td><input id="currency" name="currency" type="text" class="mini-combobox" labelField="true"  label="币种"  style="width:80%;"  required="true"  labelStyle="width:80px" data = "CommonUtil.serverData.dictionary.Currency"   allowInput="false" value="CNY" enabled="false"/></td>
			<td><input id="ir_daycount" name="ir_daycount" type="text" class="mini-combobox" labelField="true"  label="计息基准"  style="width:80%;" data = "CommonUtil.serverData.dictionary.DayCounter" labelStyle="width:80px"   required="true"   /></td>
			<td><input id="a_type" name="a_type" type="text" class="mini-combobox" labelField="true"  label="资产类型"  style="width:80%;" data = "CommonUtil.serverData.dictionary.AType" labelStyle="width:80px"   required="true"   /></td>
		</tr>
	</table>
</div>
<!-- </fieldset> -->
<div  class="mini-toolbar" style="text-align:center" >
	<a onclick="saveIr()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	var form=new mini.Form("#ir_form");
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var row=top["irManage"].GetData();
	//加载表单数据
	function initForm(){
		if($.inArray(action,["edit","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>基准利率</a> >> 详情");
			form.setData(row);

			mini.get("ir_name").setEnabled(false);
			mini.get("ir_term").setEnabled(false);
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>基准利率</a> >> 新增");
		}
	}
	//按钮事件：保存
	function saveIr(){
		form.validate();
		if (form.isValid() == false) return;
		var data=form.getData();
		data["m_type"] = "BANK";
		data["q_type"] = "IR";
		data["type"]=action;
		if(data.i_code==null||data.i_code==""){
			data['i_code']=mini.get("ir_name").getValue()+"_"+mini.get("ir_term").getValue();
		}
		var param=mini.encode(data);
		CommonUtil.ajax({
			url:"/IrController/saveIr",
			data:param,
			//relbtn:"save_btn",
			callback:function(data){
				if(data.code == 'error.common.0000'){
					mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
				}else{
					mini.alert(data.desc);
				}	
			}
		});
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
		}
		initForm();
	})
</script>
</body>
</html>
