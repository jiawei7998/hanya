<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">基准利率行情</a> >> 修改</span>
<!-- <fieldset class="mini-fieldset title">
	<legend>基准利率行情详情</legend> -->

<div id="IrSeriesEdit"> 
	<table id="ir_form" class="mini-table"  width="100%"   >
		<tr>
			<td><input id="i_code" name="i_code" type="text"  labelField="true"  label="基准利率代码"  style="width:100%;" required='true'  allowInput="false" onbuttonclick="onButtonEdit" class="mini-buttonedit searchbox" /></td>
			<td><input id="ir_name" name="ir_name" type="text"  class="mini-textbox" required="true"  readonly="true" labelField="true"  label="基准利率名称"  style="width:100%;" /></td>
			<td><input id="beg_date" name="beg_date" type="text" class="mini-datepicker" required="true"  labelField="true"  label="开始日期"  style="width:100%;"  /></td>
		</tr>
		<tr>
			<td><input id="s_close" name="s_close" type="text" class="mini-spinner" vtype="float" changeOnMousewheel='false' required="true"  labelField="true" format="p4" label="收盘价"  style="width:100%;"  /></td>
			<td><input id="s_high" name="s_high" type="text" class="mini-spinner" vtype="float" changeOnMousewheel='false' required="true"  labelField="true" format="p4" label="最高价"  style="width:100%;"  /></td>
			<td>
				<input id="a_type" name="a_type" class="mini-hidden" />
				<input id="m_type" name="m_type" class="mini-hidden" />
			</td>
		</tr>
	</table>
</div>
<!-- </fieldset> -->
<div  class="mini-toolbar" style="text-align:center" >
	<a onclick="saveAndSubmitIr()" id="submit_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	var form=new mini.Form("#ir_form");
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var row=top["irSeriesManage"].GetData();
	//加载表单数据
	function initForm(){
		if($.inArray(action,["edit","detail"])>-1){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>基准利率行情</a> >> 详情");
			form.setData(row);
			mini.get("i_code").setValue(row.i_code);
			mini.get("i_code").setText(row.i_code);
			mini.get("i_code").setEnabled(false);
			mini.get("ir_name").setEnabled(false);
			mini.get("beg_date").setEnabled(false);
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>基准利率行情</a> >> 新增");
			//基准利率名称   不可修改，根据基准利率代码获取的
			mini.get("ir_name").setEnabled(false);
		}
	}
	//关闭页面
	function colse(){
		top['win'].closeMenuTab();
	}
	//方法：保存  
	function save(data, saveUrl, btn){
		data["s_close"] = data["s_close"];
		data["s_high"] = data["s_high"];
		data['type']=action;
		var param=mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:param,
			relbtn:btn,
			callback:function(data){
				mini.alert("保存成功","提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			}
		});
	}
	//加载基准利率列表
	function onButtonEdit(e){
		 var btnEdit = this;
		 var irName=mini.get("ir_name");
		 var atype=mini.get("a_type");
		 var mtype=mini.get("m_type");
         mini.open({
			url:CommonUtil.baseWebPath() +"/mini_market/MiniIrManages.jsp",
             title: "基准利率查询",
             width: 1000,
             height: 600,
             ondestroy: function (action) {
                 if (action == "ok") {
                     var iframe = this.getIFrameEl();
                     var data = iframe.contentWindow.GetData();
					 ////debugger
                     data = mini.clone(data);    //必须
                     if (data) {
                         btnEdit.setValue(data.i_code);
                         btnEdit.setText(data.i_code);
                         irName.setValue(data.ir_name);
                         irName.setText(data.ir_name);
						 atype.setValue(data.a_type);
						 mtype.setValue(data.m_type);
						 btnEdit.setIsValid(true);
                     }
                 }

             }
         });
	}
	
	//按钮事件：保存并提交
	function saveAndSubmitIr(){
		var form=new mini.Form("#ir_form");
		form.validate();
	    if (form.isValid() == false) return;
		var data=form.getData(true);
		var saveUrl = "/QdTtMktIrController/saveIrSeriesHandle";
		save(data, saveUrl, "submit_btn");
	}
	$(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){
			mini.get("submit_btn").setVisible(false);
			form.setEnabled(false);
		}
		initForm()
	})
</script>
</body>
</html>