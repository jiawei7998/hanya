<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>基准利率行情查询</legend>
	<div id="search_form" style="width:100%;">
		<div width="100%">
			<input id="ir_search" name="ir_search" type="text"  labelField="true"  label="基准利率：" allowInput="false" labelStyle="text-align:right;"  onbuttonclick="onButtonEdit" class="mini-buttonedit searchbox" />
			<input id="beg_date" name="beg_date"  class="mini-datepicker" valueType="string" allowInput="false" labelField="true"  label="市场数据日期："  labelStyle="text-align:right;" />
			<span style="float:right;margin-right: 150px">
				<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<span style="margin:2px;display: block;">
	<a id="pass_btn" class="mini-button" style="display: none"      onclick="pass()">复核通过</a>
	<a id="refuse_btn" class="mini-button" style="display: none"      onclick="refuse()">复核拒绝</a>
</span>		
<div class="mini-fit">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true" multiSelect="true"
			 allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<!-- <div type="checkcolumn" ></div> -->
			<div field="i_code" width="120px"  align="left"  headerAlign="center" >基准利率代码</div>    
			<div field="ir_name" width="90px" align="left"  headerAlign="center" >基准利率名称</div>                            
			<div field="status" width="100px" align="left"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'DoubleApproveStatus'}">审批状态</div>
			<div field="beg_date" width="100px" align="center"  headerAlign="center"  >市场数据日期</div>                                
			<div field="s_close" width="100px"   headerAlign="center" numberFormat="p4" align="right" >收盘价</div>
			<div field="s_high" width="100px"  headerAlign="center" numberFormat="p4" align="right" >最高价</div>
			<div field="imp_date" width="100px" align="center"  headerAlign="center"  >导入日期</div>
		</div>
	</div>
</div>   
<script type="text/javascript">
	mini.parse();
	var grid=mini.get("datagrid");
	//分页
	grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				search(pageSize,pageIndex);
			});

	function search(pageSize,pageIndex){
		var form= new mini.Form("#search_form");
		form.validate();
		if (form.isValid()==false) return;
		//提交的数据
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['status']=2;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:'/QdTtMktIrController/searchIrSeriesVerify',
			data:params,
			
			callback : function(data) {
				var grid=mini.get("datagrid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	/*
	加载基准利率列表
	*/
	function onButtonEdit(e){
	 var btnEdit = this;
	 mini.open({
	     url:CommonUtil.baseWebPath() +"/mini_market/MiniIrSelectManages.jsp",
	     title: "基准利率查询",
	     width: 800,
	     height: 600,
	     ondestroy: function (action) {
	         if (action == "ok") {
	             var iframe = this.getIFrameEl();
	             var data = iframe.contentWindow.GetData();
	             data = mini.clone(data);    //必须
	             if (data) {
	                 btnEdit.setValue(data.ir_name);
	                 btnEdit.setText(data.ir_name);
	             }
	         }
	
	     }
	 });
	}
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
	}
	//复核通过
	function pass(){
		var selected= grid.getSelected();
		if(!selected){
			mini.alert("请先选择一条记录！","提示")
			return false;
		}
		CommonUtil.ajax( {
			url:'/QdTtMktIrController/TtMktSeriessPass',
			data:selected,
			callback : function(data) {
				mini.alert("提交成功.","提示");
				search(10,0);
			}
		});
	}
	//复核拒绝
	function refuse(){
		var selected= grid.getSelected();
		if(!selected){
			mini.alert("请先选择一条记录！","提示")
			return false;
		}
		CommonUtil.ajax( {
			url:'/QdTtMktIrController/TtMktSeriessRefuse',
			data:selected,
			callback : function(data) {
				mini.alert("提交成功.","提示");
				search(10,0);
			}
		});
	}
	$(document).ready(function(){
	 search(10,0);
	});	
</script>
</body>
</html>