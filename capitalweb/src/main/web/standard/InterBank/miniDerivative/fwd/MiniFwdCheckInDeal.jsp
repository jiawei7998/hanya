<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<head>
  <head>
  <title>远期外汇买卖-登记</title>
  </head>
  
  <body style="background:white">
    <div class="miniui-elem-quote">
        <a class="mini-button" style="display: none" >检验要素</a>
        <a class="mini-button" style="display: none" >保存交易</a>
        <a class="mini-button" style="display: none" >提交复核</a>
        <a class="mini-button" style="display: none" >拒绝交易</a>
        <a class="mini-button" style="display: none" >复核交易</a>
        <a class="mini-button" style="display: none" >关闭界面</a>
        
        <a class="mini-button" style="display: none" >交易费用</a>
        <a class="mini-button" style="display: none" >交易凭证</a>
        <a class="mini-button" style="display: none" >会计传票</a>
    </div>
	<div>
		<form class="mini-form" style="width:100%">
			<fieldset class="mini-fieldset title">
    			<legend>远期外汇买卖-登记</legend>
				
				<table style="width:100%">
			        <tr>
			            <td style="width:50%;"><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="机构号" emptyText="请通过选择按钮……" /></td>
			            <td style="width:50%;"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="机构名称"  emptyText="同步反显" enabled="false"/></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="业务编号" emptyText="系统自动生成" enabled="false"/></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="客户代码" emptyText="请通过选择按钮……"/></td>
			        </tr>
			    </table>
			    <input class="mini-textbox" labelField="true" label="客户名称" style="width:70%;" labelStyle = "width:160px;" emptyText="同步反显" enabled="false">
			    <input class="mini-textbox" labelField="true" label="客户地址" style="width:70%;" labelStyle = "width:160px;" emptyText="同步反显" enabled="false">
			    <table style="width:100%">
			        <tr>
			             <td style="width:50%;"><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="结汇/售汇"  emptyText="请选择交易类型" /></td>
			             <td style="width:50%;"></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="原币种"  emptyText="请选择币种代码" /></td>
			            <td ><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="原账号"  emptyText="请通过选择按钮……"/></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="目的币种"  emptyText="请选择币种代码" /></td>
			            <td ><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="目的账号"  emptyText="请通过选择按钮……"/></td>
			       </tr>
			        <tr>
			        	<td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="交割总金额（外币）"  emptyText="请输入交割外币金额"/></td>
			        	<td ></td>
			        </tr>
			        <tr>
			        	<td ><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="询价业务编号"  emptyText="请通过选择按钮……"/></td>
			        	<td ></td>
			        </tr>
			        <tr>
			        	<td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="我行报客户汇率"  emptyText="同步回显" enabled="false"/></td>
			        	<td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="总行平盘汇率"  emptyText="同步回显" enabled="false"/></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="交割方式"  emptyText="请选择交割方式" /></td>
			            <td ><input class="mini-datepicker" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="固定交割日"  emptyText="交割方式为'固定交割'则填写'固定交割日'" format="yyyy-MM-dd" showOkButton="true" showClearButton="false"/></td>
			        </tr>
			        <tr>
			        	<td >
			        	  <input class="mini-datepicker" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="择期交割日"  emptyText="交割方式为'择期交割'则填写'择期交割范围'" format="yyyy-MM-dd" showOkButton="true" showClearButton="false"/>
			        	</td>
			        	<td>
			        	  <input class="mini-datepicker" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="至："  emptyText="交割方式为'择期交割'则填写'择期交割范围'" format="yyyy-MM-dd" showOkButton="true" showClearButton="false"/>
			        	</td>
			        </tr>
			    </table>
			</fieldset>
		    <fieldset class="mini-fieldset title">
    			<legend>额度 保证金信息</legend>
			
				<table style="width:100%">
			        <tr>
			             <td style="width:50%;"><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="占用额度币种"  emptyText="请选择币种代码" /></td>
			             <td style="width:50%;"></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="金额"  emptyText="请填写额度金额" /></td>
			            <td ></td>
			        </tr>
			        <tr>
			        	<td colspan="2"><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="冻结编号一"  emptyText="请通过选择按钮……" /></td>
			        	
			        </tr>
			        <tr>
			            <td colspan="2"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="保证金账号"  emptyText="同步回显" enabled="false"/></td>
			            
			        </tr>
			        <tr>
			             <td style="width:50%;"><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="币种"  emptyText="请选择币种代码" /></td>
			             <td style="width:50%;"></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="金额"  emptyText="请填写额度金额" /></td>
			            <td ></td>
			        </tr>
			        <tr>
			        	<td colspan="2"><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="冻结编号二"  emptyText="请通过选择按钮……" /></td>
			        	
			        </tr>
			        <tr>
			            <td colspan="2"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="保证金账号"  emptyText="同步回显" enabled="false"/></td>
			            
			        </tr>
			        <tr>
			             <td style="width:50%;"><input class="mini-combobox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="币种"  emptyText="请选择币种代码" /></td>
			             <td style="width:50%;"></td>
			        </tr>
			        <tr>
			            <td ><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="金额"  emptyText="请填写额度金额" /></td>
			            <td ></td>
			        </tr>
			        <tr>
			        	<td colspan="2"><input class="mini-buttonedit" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="冻结编号三"  emptyText="请通过选择按钮……"/></td>
			        	
			        </tr>
			        <tr>
			            <td colspan="2"><input class="mini-textbox" style="width:70%;" labelStyle = "width:160px;" labelField="true" label="保证金账号"  emptyText="同步回显" enabled="false"/></td>
			            
			        </tr>
			    </table>
		    </fieldset>
	    </form>
    </div>
    
  </body>
</html>
