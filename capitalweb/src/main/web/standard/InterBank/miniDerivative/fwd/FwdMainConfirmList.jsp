<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../../global.jsp"%>
<head>
  <head>
  <title>远期外汇买卖-交割列表</title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset title">
    	<legend>远期外汇买卖-交割列表</legend>
	</fieldset>
	<div id="searchForm" style="height:60px;">
		<table >
        <tr>
		     <td style="width:310px;">
		     	<input class="mini-textbox" name="custmerCode" labelField="true" label="客户代码" style="width:100%;" emptyText="请输入精确客户代码" >
		     </td>
		     <td style="width:410px;">
		    	<input class="mini-textbox" name="cnName" labelField="true" label="客户名称" style="width:100%;" emptyText="请输入模糊客户名称" >
		     </td>
		     <td style="width:10px;">
		     </td>
		     <td>
		     	<a class="mini-button" style="display: none"  onclick="search(10,0)">查询</a>
		     	<a class="mini-button" style="display: none"  onclick="btnCustSync()">新增</a>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				
			</td>
		</tr>
		</table>
	</div>
	<div class="mini-fit">
	    <div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" idField="id"  frozenStartColumn="0" frozenEndColumn="1" >
	    </div>
    </div>
    <script type="text/javascript">
    	function btnCustSync(){
		var url = CommonUtil.baseWebPath() + "/miniDerivative/fwd/MiniFwdCheckInDeal.jsp";
		var tab = {"id": "fwd_checkin_sync", iconCls: "fa fa-send-o", text: "远期外汇买卖-登记" ,url:url,innerText: "远期外汇买卖-登记"};
		top["win"].showTab(tab);
		}
	
        mini.parse();
        var grid = mini.get("datagrid1");
        grid.set({
        columns: [
        		{ field: "tyCustId", width: 120, headerAlign: "center", allowSort: true, header: "业务编号"},
	            { field: "custmerCode", width: 70, headerAlign: "center", allowSort: true, header: "机构号"},
	            { field: "cnName", width: 120, headerAlign: "center", allowSort: true, header: "机构名称"},
	            { field: "idType", width: 80, headerAlign: "center", allowSort: true, header: "结汇/售汇"},
	            { field: "idNumber", width: 80, headerAlign: "center", allowSort: true, header: "客户号"},
	            { field: "inputDt", width: 180, headerAlign: "center", allowSort: true, header: "客户名称"},
	            { field: "sfCptyCountry", width: 120, headerAlign: "center", allowSort: true, header: "原帐号"},
	            { field: "sfCptyCountry", width: 60, headerAlign: "center", allowSort: true, header: "原币种"},
	            { field: "merType", width: 120, headerAlign: "center", allowSort: true, header: "交割总金额（外币）"},
	            { field: "merType", width: 120, headerAlign: "center", allowSort: true, header: "未交割金额（外币）"},
	            { field: "sfCptyCountry", width: 70, headerAlign: "center", allowSort: true, header: "目的币种"},
	            { field: "strent", width: 80, headerAlign: "center", allowSort: true, header: "我行报客户汇率"},
	            { field: "bankType", width: 80, headerAlign: "center", allowSort: true, header: "交割方式"},
	            { field: "manager", width: 80, headerAlign: "center", allowSort: true, header: "固定交割日"},
	            { field: "managerProp", width: 80, headerAlign: "center", allowSort: true, header: "择期交割日期"},
	            { field: "managerProp", width: 80, headerAlign: "center", allowSort: true, header: "交割状态"}
	        ]
	    });
       
        grid.frozenColumns(0, 1);
        grid.on("beforeload", function (e) {
	      e.cancel = true;
	      var pageIndex = e.data.pageIndex; 
	      var pageSize = e.data.pageSize;
	      search(pageSize,pageIndex);
	    });
	    search(10,0);
	    function search(pageSize,pageIndex) {
	    	var param = new mini.Form("searchForm")
		    param.validate();
		    var data = param.getData();
		    data.pageNumber=pageIndex+1;
		        data.pageSize=pageSize;
		    var json = mini.encode(data);
		    url = "";
		    CommonUtil.ajax({
		      url : url,
		      data : json,
		      callback : function(data) {
		        grid.setTotalCount(data.obj.total);
		        grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
		        grid.setData(data.obj.rows);
		      }
		    });
		 };
    </script>
</body>
</html>
