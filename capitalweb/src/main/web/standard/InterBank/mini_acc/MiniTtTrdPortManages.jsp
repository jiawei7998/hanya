<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>

<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
		<legend>信息查询</legend>
		<div id="TtTrdPortManageGrid">
				<input id="portfolio"  name="portfolio" class="mini-textbox" labelField="true" label="投资组合代码：" labelStyle="text-align:right;" emptyText="请输入组合代码" vtype="maxLength:50"/>
				<input id="portdesc" name="portdesc" class="mini-textbox" labelField="true" label="备注信息：" labelStyle="text-align:right;" emptyText="请输入备注信息"  vtype="maxLength:50"/>
				<input id="cost" name="cost" class="mini-textbox" labelField="true" label="成本中心：" labelStyle="text-align:right;" emptyText="请输入成本中心" vtype="maxLength:15"/>
				<span style="float:right;margin-right: 150px">
					<a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
					<a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
				</span>
			</div>
</fieldset>
<span style="margin:2px;display: block;">
	<a class="mini-button" style="display: none"  id="add_btn" onClick="add();" >新增</a>
	<a class="mini-button" style="display: none"  id="edit_btn" onClick="edit();" >修改</a>
	<!-- <a class="mini-button" style="display: none"  id="delete_btn" onClick="del();" >删除</a> -->
</span>
<div class="mini-fit">
	<div class="mini-datagrid borderAll" id="grid" style="width:100%;height:100%;" allowResize="true"  pageSize="10" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" headerAlign="center">序列</div>
			<div field="portfolio" headerAlign="center" allowSort="true">投资组合代码</div>
			<div field="portdesc" headerAlign="center" allowSort="true">投资组合备注信息</div>
			<div field="cost" headerAlign="center" >成本中心号</div>
			<div field="lstmntdte" headerAlign="center" align="center">修改日期</div>
		</div>
	</div>
</div>
</body>
<script>
mini.parse();

var grid = mini.get("grid");
$(document).ready(function(){
	//控制按钮显示
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		search(10, 0);
	});
})
//新增	
function add(){
	var url = CommonUtil.baseWebPath() + "/mini_acc/MiniTtTrdPortEdits.jsp";
	var tab = { id: "TtdPortManagesAdd", name: "TtdPortManagesAdd", text: "投资组合新增", url: url,parentId:top["win"].tabs.getActiveTab().name };
	top['win'].showTab(tab);
}
top['TtTrdPortEdit'] = window;
function ShowTtdPort() {
	var record = grid.getSelected();
	return record;
}
//修改
function edit(){
	var record = grid.getSelected();
        if (record) {
            var url = CommonUtil.baseWebPath() + "/mini_acc/MiniTtTrdPortEdits.jsp?action=edit";
            var tab = { id: "TtTrdPortEdit", name: "TtTrdPortEdit", text: "投资组合修改", url: url, parentId: top["win"].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        } else {
            mini.alert("请选择一条数据");
        }
}
// //删除
// function del(){
// 	var record = grid.getSelected();
// 	var arr=new Array();
// 	arr.push(record.portfolio);
// 	if(record){
// 		CommonUtil.ajax({
// 		url: "/TtTrdPortController/deleteTtTrdPortById",
// 		data: mini.encode(arr),
// 		callback: function (data) {
// 			if (data.code == 'error.common.0000') {
// 				mini.alert("操作成功", "系统提示",function(){
// 					search(10,0);
// 				});
// 			} else {
// 				mini.alert("操作失败", "系统提示");
// 			}
			
// 		}
// 	});

// 	}else{
// 		mini.alert("请选择一条要删除的数据！","系统提示");
// 	}
// }
//查询
function search(pageSize,pageIndex) {
	var form = new mini.Form("TtTrdPortManageGrid");
	form.validate();
	if (form.isValid() == false) return;//表单验证
	var data = form.getData();
	data.pageNumber=pageIndex+1;
	data.pageSize=pageSize;
	var param = mini.encode(data); //序列化成JSON
	CommonUtil.ajax({
		url: "/TtTrdPortController/searchTtTrdPorts",
		data: param,
		callback: function (data) {
			
			grid.setTotalCount(data.obj.total);
			grid.setPageIndex(pageIndex);
			grid.setPageSize(pageSize);
			grid.setData(data.obj.rows);
		}
	});
}
function clear() {
	var form = new mini.Form("#TtTrdPortManageGrid");
	form.clear();
}   
</script>