<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="../../global.jsp"%>
<span id="labell"><a href="javascript:CommonUtil.activeTab();">投资组合</a> >> 新增</span>	
<div id="TtTrdPortEditGrid">
	<table id="search_form" width="100%" class="mini-table">
		<tr>
			<td><input id="portfolio" labelField="true" required="true"  label="投资组合代码：" emptyText='请输入投资组合代码'
				name="portfolio" class="mini-textbox" maxLength = "4"  onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
				style="width:80%;"/>
			</td>
			<td><input id="portdesc" vtype="maxLength:50" labelField="true" required="true"  label="投资组合备注信息：" emptyText='请输入投资组合备注信息'
				name="portdesc" class="mini-textbox" style="width:80%;"/>
			</td>
			<td><input id="cost" name="cost" class="mini-buttonedit" allowInput='false' labelField="true" required="true"  label="成本中心号："  emptyText='请选择成本中心号'
				onbuttonclick="onButtonEdit"  style="width:80%;"/>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:center;">
				<a onclick="save();"   id="save_btn" class="mini-button" style="display: none" >保存</a>
			</td>
			<td>

			</td>
			<td>

			</td>
		</tr>
	</table>
</div>
<script>
	mini.parse();
	
    $(document).ready(function(){
		if(GetQueryString('action')=='edit'){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>投资组合</a> >> 修改");
			init();
		}
	})

	function init(){
		if(GetQueryString('action')=='edit'){
			
			var form =new mini.Form("search_form");
			var data=top["TtTrdPortEdit"].ShowTtdPort();
			var costId=data.cost;
			var costcent='';
			var costdesc='';
			CommonUtil.ajax({
				url: "/TtTrdCostController/selectTtTrdCostById",
				data: mini.encode({ 'costId': costId }),
				callback: function (data) {
					costcent = data.obj.costcent;
					costdesc =data.obj.costdesc;
					mini.get("cost").setValue(costcent);
			        mini.get("cost").setText(costdesc);
				}
			});
			form.setData(data);
			mini.get("portfolio").setEnabled(false);
			
		}

	}
    function save(){
    	var form =new mini.Form("TtTrdPortEditGrid");
    	form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data = form.getData();
		var portfolio = data.portfolio;
		if(portfolio.length != 4){
			mini.alert("投资组合长度必须为4","系统提示");
			return;
		}
    	var param=mini.encode(data);
		var url='';
		if(GetQueryString('action')=='edit'){
			url='/TtTrdPortController/updateTtTrdPort';
		}else{
			url='/TtTrdPortController/insertTtTrdPorts';
		}
    	CommonUtil.ajax({
			url:url,
			data:param,
			callback:function(data){
				if (data.code == 'error.common.0000') {
					mini.alert("操作成功","系统提示",function(){
						setTimeout(function(){top["win"].closeMenuTab()},10);
					});
				} else {
					mini.alert("操作失败","系统提示");
				}	
			}
		});
    	
    }
    
    function onButtonEdit(){
    	 var btnEdit = this;
         mini.open({
             url:  CommonUtil.baseWebPath() + "/mini_acc/MiniCostMiniManages.jsp?type=1",
             title: "选择父代码",
             width: 750,
             height: 380,
             ondestroy: function (action) {
                 //if (action == "close") return false;
                 if (action == "ok") {
                     var iframe = this.getIFrameEl();
                     var data = iframe.contentWindow.GetData();
                     data = mini.clone(data);    //必须
                     if (data) {
                         btnEdit.setValue(data.costcent);
                         btnEdit.setText(data.costdesc);//设置空白处显示内容
                     }
                 }

             }
         });
    }
	
function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}
	</script>