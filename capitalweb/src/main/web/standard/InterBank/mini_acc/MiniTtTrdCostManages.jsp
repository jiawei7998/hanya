<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>

<body style="width:100%;height:100%;background:white">

<fieldset>
	<legend>查询</legend>
	<div id="search_form">
		<input id="costcent" name="costcent" labelField="true" label="成本中心：" emptyText="请输入成本中心" labelStyle="text-align:right;" class="mini-textbox"
		/>
		<input id="costdesc" name="costdesc" labelField="true" label="成本中心名称：" emptyText="请输入名称" labelStyle="text-align:right;" class="mini-textbox"
		/>
		<span style="float:right;margin-right: 150px">
			<a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
			<a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
		</span>
	</div>
</fieldset>
<span style="margin:2px;display: block;">
	<a class="mini-button" style="display: none"  id="add_btn" onClick="add();" >新增</a>
	<a class="mini-button" style="display: none"  id="edit_btn" onClick="edit();" >修改</a>
	<!-- <a class="mini-button" style="display: none"  id="delete_btn" onClick="del();" >删除</a> -->
</span>
	<div class="mini-fit">
		<div id="TtTrdCostManageTreegrid" class="mini-treegrid" expandOnLoad ="1" height="100%" showTreeIcon="true" treeColumn="costcent"
		idField="id" parentField="parentcostcent" resultAsTree="false" allowAlternating="true">
			<div property="columns">
				<div type="indexcolumn"></div>
				<div name="costcent" headerAlign="center" field="costcent">成本中心代码</div>
				<div field="costdesc" headerAlign="center">成本中心名称</div>
				<div field="parentcostcent" headerAlign="center">父节点代码</div>
				<div field="lstmntdte" headerAlign="center" align="center">修改日期</div>
			</div>
		</div>
	</div>
</body>
<script>
mini.parse();

var grid = mini.get("TtTrdCostManageTreegrid");
$(document).ready(function(){
	//控制按钮显示
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		search();
	});
})
		
function search() {
	var form = new mini.Form("#search_form");
	form.validate();
	if(form.isValid() == false){
		mini.alert("表单填写错误,请确认!","提示信息")
		return;
	}
	var data = form.getData();
	CommonUtil.ajax({
		url: "/TtTrdCostController/searchTtTrdCost",
		data: mini.encode(data),
		callback: function (data) {
			
			grid.setData(data.obj);
		}
	});
}
//新增
function add(){
	var url = CommonUtil.baseWebPath() + "/mini_acc/MiniTtTrdCostEdits.jsp";
	var tab = { id: "TtTrdCostManagesAdd", name: "TtTrdCostManagesAdd", text: "成本中心新增", url: url,parentId:top["win"].tabs.getActiveTab().name };
	top['win'].showTab(tab);
}
top['TtTrdCost']=window;
function showTtTrdCost(){
	return grid.getSelected();
}
//修改
function edit(){
	var record = grid.getSelected();
        if (record) {
            var url = CommonUtil.baseWebPath() + "/mini_acc/MiniTtTrdCostEdits.jsp?action=edit";
            var tab = { id: "TtTrdCostEdit", name: "TtTrdCostEdit", text: "成本中心修改", url: url, parentId: top["win"].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        } else {
            mini.alert("请选择一条数据");
        }
}
function clear() {
	var form = new mini.Form("#search_form");
	form.clear();
} 
</script>