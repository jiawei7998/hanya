<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="../../global.jsp"%>
<span id="labell"><a href="javascript:CommonUtil.activeTab();">成本中心</a> >> 新增</span>
<div id="TtTrdCostEditGrid">
	<table id="search_form" class="mini-table" width="100%">
		<tr>
			<td>
				<input id="costcent" required="true"  labelField="true" label="成本中心代码：" emptyText='请输入成本中心代码' name="costcent" class="mini-textbox"
					maxLength = "10"  onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" style="width:80%;"/>
			</td>
			<td>
				<input id="costdesc" vtype="maxLength:50"  required="true"  labelField="true" label="成本中心名称：" emptyText='请输入成本中心名称' name="costdesc" class="mini-textbox"
					style="width:80%;"/>
			</td>
			<td>
				<input id="parentcostcent" vtype="maxLength:270" allowInput='false'  labelField="true" label="成本中心父代码：" emptyText='请选择成本中心父代码' onbuttonclick="onButtonEdit" name="parentcostcent" class="mini-buttonedit"
				 style="width:80%;"/>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:center;">
					<a onclick="save();"   id="save_btn" class="mini-button" style="display: none" >保存</a>
			</td>
			<td>
			
			</td>
			
		</tr>
	</table>
</div>
<script>
mini.parse();
$(document).ready(function () {
	if (GetQueryString('action') == 'edit') {
		$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>成本中心修改</a> >> 修改");
		init();
	}
})
//初始化
function init(){
	if(GetQueryString('action')=='edit'){
			var form =new mini.Form("search_form");
			var data=top["TtTrdCost"].showTtTrdCost();
			var costId=data.parentId;
			var costcent='';
			var costdesc='';
			CommonUtil.ajax({
				url: "/TtTrdCostController/selectTtTrdCostById",
				data: mini.encode({ 'costId': costId }),
				callback: function (data) {
					costcent = data.obj.costcent;
					costdesc =data.obj.costdesc;
					mini.get("parentcostcent").setValue(costcent);
			        mini.get("parentcostcent").setText(costdesc);
				}
			});
			form.setData(data);
			mini.get("costcent").setEnabled(false);
			
		}
}
function save() {
	mini.parse();
	var form = new mini.Form("TtTrdCostEditGrid");
	form.validate();
	if (form.isValid() == false) {
		return;
	}
	var param = mini.encode(form.getData());
	//发送ajax
	var url='';
	if(GetQueryString('action') == 'edit'){
		url='/TtTrdCostController/updateTtTrdCost';

	}else{
		url='/TtTrdCostController/insertTtTrdCost';
	}
	CommonUtil.ajax({
		url : url,
		data : param,
		callback : function(data) {
			if (data.code == 'error.common.0000') {
				mini.alert("操作成功","系统提示",function(){
					setTimeout(function(){top["win"].closeMenuTab()},10);
				});
			} else {
				mini.alert("操作失败","系统提示");
			}
		}
	});
}
	
function onButtonEdit() {
	var btnEdit = this;
	mini.open({
		url : CommonUtil.baseWebPath() + "/mini_acc/MiniCostMiniManages.jsp",
		title : "选择父代码",
		width : 750,
		height : 380,
		ondestroy : function(action) {
			//if (action == "close") return false;
			if (action == "ok") {
				var iframe = this.getIFrameEl();
				var data = iframe.contentWindow.GetData();
				data = mini.clone(data); //必须
				if (data) {
					btnEdit.setValue(data.costcent);
					btnEdit.setText(data.costdesc);//设置空白处显示内容
				}
			}

		}
	});
}
function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}
</script>