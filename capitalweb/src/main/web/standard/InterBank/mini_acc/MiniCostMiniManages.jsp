<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
	<body style="width:100%;height:100%;background:white">
		<fieldset class="mini-fieldset">
			<legend>查询</legend>
			<table id="search_form" style="width:100%">
				<tr>
					<td>
						<input id="costcent" name="costcent" class="mini-textbox" labelField="true" labelStyle="width:60px;"  label="代码:"  style="width:90%;" emptyText="请输入成本中心代码" onenter="onKeyEnter"/>
					</td>
					
					<td>
						<input id="costdesc" name="costdesc" class="mini-textbox" labelField="true" labelStyle="width:60px;"  label="名称:" style="width:90%;" emptyText="请输入成本中心名称" onenter="onKeyEnter"/>
						
					</td>

					<td>
						<a	class="mini-button" style="display: none"   id="search_btn" onclick="query">查询</a>
					</td>
				</tr>
			</table>
		</fieldset>

		<div class="mini-fit" >
			<div id="grid1" class="mini-datagrid borderAll" allowResize="true" pageSize="10" width="100%" height = "100%" onrowdblclick="onRowDblClick"
			  allowAlternating="true" sortMode="client">
				<div property="columns">
					<div type="indexcolumn"  width="10">序号</div>
					<div field="costcent" headerAlign="center" allowSort="false"  width="30">成本中心代码</div>
					<div field="costdesc" headerAlign="center" allowSort="false"  width="60">成本中心名称</div>
				</div>
			</div>
		</div>
		<script>
			var type = "";
			type = GetQueryString("type");
			mini.parse();
			var grid =mini.get("grid1");
			grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				search(pageSize,pageIndex);
			});

			$(document).ready(function() {
				query();
			})
			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if (r != null)
					return unescape(r[2]);
				return null;
			}

			function query(){
				search(grid.pageSize,0);
			}
		
			function GetData() {
				var grid = mini.get("grid1");
				var row = grid.getSelected();
				return row;
			}
			function search(pageSize,pageIndex) {
				var url = "";
				var param = new mini.Form("search_form")
				param.validate();
				if(param.isValid() == false){
					mini.alert("表单填写错误,请确认!","提示信息")
					return;
				}
				var data = param.getData();
				data.pageNumber=pageIndex+1;
				data.pageSize=pageSize;
				var json = mini.encode(data);
				if (type == "1") {
					url = "/TtTrdCostController/selectAllCostsExcept";
				} else {
					url = "/TtTrdCostController/selectAllCosts";
				}
				CommonUtil.ajax({
					url : url,
					data : json,
					callback : function(data) {
						var grid = mini.get("grid1");
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
					}
				});
		
			}
			function onKeyEnter(e) {
				query();
			}
			function onRowDblClick(e) {
				onOk();
			}
			function CloseWindow(action) {
				if (window.CloseOwnerWindow)
					return window.CloseOwnerWindow(action);
				else
					window.close();
			}
		
			function onOk() {
				CloseWindow("ok");
			}
			function onCancel() {
				CloseWindow("cancel");
			}
		</script>
	</body>
</html>


