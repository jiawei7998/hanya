<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
	<body style="width:100%;height:100%;background:white">
		<fieldset class="mini-fieldset">
			<legend>查询</legend>
			<div class="mini-toolbar" id="searchForm" style="padding:2px;border-bottom:0;">
				<table width="100%">
					<tr>
						<td>
							<input id="portfolio" name="portfolio"class="mini-textbox" onenter="onKeyEnter" labelField="true" labelStyle="width:80px;"  label="投资组合:"  style="width:90%;" emptyText="请输入投资组合代码"/> 
						</td>
						<td>
							<input id="portdesc" name="portdesc" class="mini-textbox" onenter="onKeyEnter" labelField="true" labelStyle="width:80px;"  label="备注信息:"  style="width:90%;" emptyText="请输入投资组合备注"/>  
						</td>
						<td>
							<input id="cost" name="cost" class="mini-textbox" onenter="onKeyEnter" labelField="true" labelStyle="width:80px;"  label="成本中心:"  style="width:90%;" emptyText="请输入成本中心"/> 
						</td>
						<td>
							<a class="mini-button" style="display: none"  onclick="query"  style="margin-right: 10px;">查询</a>
						</td>
					</tr>
				</table>
			</div>
		</fieldset>
		<div class="mini-fit" >
			<div id="grid1" class="mini-datagrid borderAll" allowResize="true" width="100%" height = "100%" pageSize="10" 
			onrowdblclick="onRowDblClick" allowAlternating="true" sortMode="client">
				<div property="columns">
					<div type="indexcolumn" headerAlign="center" width="50">序号</div>
					<div field="portfolio" headerAlign="center" allowSort="false" width="100">投资组合代码</div>
					<div field="portdesc"  headerAlign="center" allowSort="false" width="150">投资组合备注信息</div>
					<div field="cost"      headerAlign="center" allowSort="false" width="80">成本中心号</div>
				</div>
			</div>
		</div>
		<script>
			var type = "";
			type = GetQueryString("type");
			mini.parse();
			var grid =mini.get("grid1");
			grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				search(pageSize,pageIndex);
			});

			$(document).ready(function() {
				query();
			});
			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if (r != null)
					return unescape(r[2]);
				return null;
			}
		
			function GetData() {
				var grid = mini.get("grid1");
				var row = grid.getSelected();
				return row;
			}

			function query(){
				search(grid.pageSize,0);
			}

			function search(pageSize,pageIndex) {
				var url = "";
				var param = new mini.Form("searchForm")
				param.validate();
				var data = param.getData();
				data.pageNumber=pageIndex+1;
				data.pageSize=pageSize;
				var json = mini.encode(data);
		
				url = "/TtTrdPortController/searchTtTrdPorts";
				CommonUtil.ajax({
					url : url,
					data : json,
					callback : function(data) {
						var grid = mini.get("grid1");
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
					}
				});
			}
		
			function onKeyEnter(e) {
				query();
			}
			function onRowDblClick(e) {
				onOk();
			}
			function CloseWindow(action) {
				if (window.CloseOwnerWindow)
					return window.CloseOwnerWindow(action);
				else
					window.close();
			}
		
			function onOk() {
				CloseWindow("ok");
			}
			function onCancel() {
				CloseWindow("cancel");
			}
		</script>
	</body>
</html>