﻿
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
	<legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<input id="bankaccid" name="bankaccid" class="mini-textbox" labelField="true" label="银行账号：" labelStyle="text-align:right;" emptyText="请输入银行账号"/>
		<input id="openBankName" name="openBankName" class="mini-textbox" labelField="true" label="开户行名称：" labelStyle="text-align:right;" emptyText="请输入开户行名称"/>
		<input id="custmerCode" name="custmerCode" class="mini-textbox" labelField="true" label="客户代码：" labelStyle="text-align:right;" emptyText="请输入客户代码"/>
		<span style="float:right;margin-right: 160px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
		<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
	</span>
	<div id="AccOutCashManage" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<!-- <div type="checkcolumn" headerAlign="center"></div> -->
				<div field="custmerCode" width="80" allowSort="true" headerAlign="center" align="center">客户代码</div>
				<div field="custmerName" width="150" allowSort="true" headerAlign="center">客户名称</div>
				<div field="cny" width="60" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
				<div field="bankaccid" width="220" allowSort="false" headerAlign="center" align="">银行账号</div>
				<div field="bankaccname" width="200" allowSort="false" headerAlign="center" align="">银行账户名称</div>
				<div field="openBankLargeAccno" width="120" allowSort="false" headerAlign="center" align="">开户行号</div>
				<div field="openBankName" width="180" allowSort="false" headerAlign="center" align="">开户行名称</div>
				<div field="operateUser" width="80" allowSort="false" headerAlign="center" align="">提交人</div>
			</div>
		</div>  
	</div>

	<script>
		mini.parse();
		top['AccOutCashManage'] = window;
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}

			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/TdCustAccController/searchCustAccPage',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}

		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}
			
		//双击详情页

		function add(){
			var url = CommonUtil.baseWebPath() + "/mini_acc/MiniAccOutCashEdits.jsp?action=add";
			var tab = { id: "MiniAccOutCashAdd", name: "MiniAccOutCashAdd", text: "资金账户新增", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}

		function update()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/mini_acc/MiniAccOutCashEdits.jsp?action=edit";
				var tab = { id: "MiniAccOutCashEdit", name: "MiniAccOutCashEdit", title: "资金账户修改", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				top["win"].openNewTab(tab, row);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function onRowDblClick() {
			var grid = mini.get("datagrid");
			var row = grid.getSelected();
			if(row){
					var url = CommonUtil.baseWebPath() + "/mini_acc/MiniAccOutCashEdits.jsp?action=detail";
					var tab = { id: "MiniAccOutCashDetail", name: "MiniAccOutCashDetail", title: "资金账户详情", url: url ,showCloseButton:true,parentId:top["win"].tabs.getActiveTab().name};
					top["win"].openNewTab(tab, row);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}
		
		function query(){
			search(grid.pageSize,0);
		}
		
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}
		
		//删除
		function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/TdCustAccController/deleteTdCustAcc",
							data: {custmerCode: row.custmerCode,bankaccid:row.bankaccid,openBankLargeAccno:row.openBankLargeAccno,cny:row.cny},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功")
									search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
			function getData(action) {
					var row = null;
					if (action != "add") {
						row = grid.getSelected();
					}
					return row;
				}


		$(document).ready(function()
		{
			search(10,0);
		});
		
	</script>
</body>
</html>
