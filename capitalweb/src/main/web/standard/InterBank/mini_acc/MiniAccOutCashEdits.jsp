<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<title></title>
</head>
<body>
	<div id="form1">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">资金账户</a> >> 修改</span>
		<div id="MiniAccOutCashManage" class="fieldset-body" > 
			<table class="mini-table" id="accout_form"  width="100%">
				<tr>
					<td>
						<input id="custmerCode" name="custmerCode" labelField="true" label="客户代码："  allowinput="false"
							onbuttonclick="onButtonEdit" required="true"  requiredErrorText="该输入项为必输项" class="mini-buttonedit searchbox" emptyText="请选择..."
							labelStyle="width:100px" style="width:90%"/>
					</td>
					<td>
						<input id="openBankLargeAccno" name="openBankLargeAccno" class="mini-textbox mini-validatebox" labelField="true" label="开户行号："
							required="true"  maxLength="14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" requiredErrorText="该输入项为必输项" labelStyle="width:100px" style="width:90%"/>
					</td>
					<td>
						<input id="code" name="code" class="mini-hidden" vtype="int">

						<input id="openBankName" name="openBankName" class="mini-textbox mini-validatebox" labelField="true" label="开户行名称：" 
							required="true"  vtype="rangeLength:1,50" requiredErrorText="该输入项为必输项" labelStyle="width:100px" style="width:90%" />
					
					</td>
				</tr>
				<tr>
					<td>
						<input id="cny" name="cny" class="mini-combobox" labelField="true" label="币种："  data="CommonUtil.serverData.dictionary.Currency"
							value="CNY" emptyText="" enabled = "false" required="true"  requiredErrorText="该输入项为必输项:" labelStyle="width:100px" style="width:90%"/>
					</td>
					<td>
						<input id="bankaccid" name="bankaccid" class="mini-textbox mini-validatebox" labelField="true" label="银行账号：" 
							required="true"  maxLength="32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" requiredErrorText="该输入项为必输项" labelStyle="width:100px" style="width:90%"/>
					</td>
					<td>
						<input id="bankaccname" name="bankaccname" class="mini-textbox mini-validatebox" labelField="true" label="银行账户名称：" 
						required="true"  vtype="rangeLength:1,50" requiredErrorText="该输入项为必输项" labelStyle="width:100px" style="width:90%" />
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center;">
						<a id="save_btn" class="mini-button" style="display: none"    onclick="add()">保存</a>
						<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close">取消</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
<script type="text/javascript">
	mini.parse();
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var param = top["AccOutCashManage"].getData(action);
	//保存的方法  
	function add() {
		var form = new mini.Form("accout_form");
		form.validate();
		if (form.isValid() == false) {
			mini.alert("信息填写有误，请重新填写!", "消息提示")
			return;
		}

		//提交数据																					
		var saveUrl = $.inArray(action, ["add"]) > -1 ? "/TdCustAccController/insertTdCustAcc" : "/TdCustAccController/updateCustAccService";
		var data = form.getData(true); //获取表单多个控件的数据  
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url: saveUrl,
			data: param,
			callback: function (data_1) {
				if (data_1.detail == '110') { 
					mini.alert("保存失败,该客户银行账号为["+data.bankaccid+"],开户行号为["+data.openBankLargeAccno+"]的清算信息已经存在!");
				} else {
					mini.alert("保存成功",'提示信息',function(){
						setTimeout(function(){top["win"].closeMenuTab()},10);
					});
				}
			}
		});
	}

	function close() {
		top["win"].closeMenuTab();
	}

	function initform() {
		if (action == "edit") {
			var form = new mini.Form("#accout_form");
			mini.get("custmerCode").setText(param.custmerCode);
			mini.get("custmerCode").setValue(param.custmerCode);
			mini.get("custmerCode").setEnabled(false);
			mini.get("cny").setEnabled(false);
			mini.get("bankaccid").setEnabled(false);
			mini.get("openBankLargeAccno").setEnabled(false);
			var form = new mini.Form("#accout_form");
			form.setData(param);
		} else if (action == "detail") {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资金账户</a> >> 详情");
			var form = new mini.Form("#accout_form");
			mini.get("custmerCode").setText(param.custmerCode);
			form.setData(param, false);
			form.setEnabled(false);
			mini.get("save_btn").setEnabled(false);
			mini.get("cancel_btn").setEnabled(false);
		} else if(action == "add") {
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>资金账户</a>>>新增");
		}


	}

	//查询客户的方法
	function onButtonEdit(e) {
		var btnEdit = this;
		mini.open({
			url: CommonUtil.baseWebPath() +"/mini_cp/MiniCoreCustQueryEdits.jsp",
			title: "客户选择",
			width: 600,
			height: 500,
			ondestroy: function (action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); 
					if (data) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						btnEdit.setIsValid(true);
					}
				}

			}
		
		});
	}

	$(document).ready(function () {
		initform();
	})
</script>
</body>