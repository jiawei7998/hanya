<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<head>
  <head>
  <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
  <title>客户额度综合查询</title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset title">
    	<legend>客户额度综合查询</legend>
	</fieldset>
	<div id="searchForm" style="margin-top:10px;height:50px;">
		<table >
        <tr>
		     <td style="width:310px;">
		     	<input class="mini-textbox" name="custNo" labelField="true" label="客户代码" style="width:100%;" emptyText="请输入精确客户代码" >
		     </td>
		     <td style="width:310px;">
		    	<input class="mini-textbox" name="partyName" labelField="true" label="客户名称" style="width:100%;" emptyText="请输入模糊客户名称" >
		     </td>
		     <td style="width:10px;">
		     </td>
		     <td>
		     	<a class="mini-button" style="display: none" onclick="search(10,0)">查询信息</a>
		     	<a class="mini-button" style="display: none" onclick="">额度占用明细</a>
		     	<a class="mini-button" style="display: none" onclick="">额度串用明细</a>
		     	<a class="mini-button" style="display: none" onclick="">额度被串用明细</a>
		     	<a class="mini-button" style="display: none" onclick="">额度冻结明细</a>
		     	<a class="mini-button" style="display: none" onclick="">额度预占用明细</a>
		     	<a class="mini-button" style="display: none" onclick="">额度预串用明细</a>
		     	<a class="mini-button" style="display: none" onclick="">额度预被串用明细</a>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				
			</td>
		</tr>
		</table>
	</div>
	<div class="mini-splitter" style="width:100%;height:100%;">
	    <div size="50%" showCollapseButton="true" style="width:100%;height:100%;">
	        
			<div class="mini-fit">
			    <div id="datagrid1" class="mini-datagrid borderAll" onselectionchanged="onSelectionChanged"
			    style="width:100%;height:100%;" allowResize="true" idField="id"  frozenStartColumn="0" frozenEndColumn="2" >
			    </div>
		    </div>
	    </div>
	    <div showCollapseButton="true">
	        <div class="mini-splitter" style="width:100%;height:100%;" vertical="true">
			    <div size="40%" showCollapseButton="true">
				    <div id="datagrid2" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" idField="id"  frozenStartColumn="0" frozenEndColumn="3" >
				    </div>
			    </div>
			    <div size="60%" showCollapseButton="true">
			        2
			    </div>     
			</div>
	    </div>        
	</div>
	
    <script type="text/javascript">
    	function btnCustSync(){
		var url = CommonUtil.baseWebPath() + "/miniDerivative/fwd/MiniFwdCheckInDeal.jsp";
		var tab = {"id": "fwd_checkin_sync", iconCls: "fa fa-send-o", text: "远期外汇买卖-登记" ,url:url,innerText: "远期外汇买卖-登记"};
		top["win"].showTab(tab);
		}
	
        mini.parse();
        var grid = mini.get("datagrid1");
        grid.set({
        columns: [
        		{ type: "indexcolumn"},
        		{ field: "ecifNum", width: 90, headerAlign: "center", allowSort: true, header: "客户编号"},
	            { field: "cnName", width: 210, headerAlign: "center", allowSort: true, header: "客户名称"},
	            { field: "loanAmt", width: 130, headerAlign: "center", align: "right",allowSort: true, header: "授信额度(元)",numberFormat:"#,0.00"},
	            { field: "adjAmt", width: 130, headerAlign: "center", align: "right",allowSort: true, header: "额度调整(元)",numberFormat:"#,0.00"},
	            { field: "frozenAmt", width: 130, headerAlign: "center",align: "right", allowSort: true, header: "冻结额度(元)",numberFormat:"#,0.00"},
	            { field: "avlAmt", width: 130, headerAlign: "center",align: "right", allowSort: true, header: "可用额度(元)",numberFormat:"#,0.00"}
	        ]
	    });
       
        grid.frozenColumns(0, 2);
        grid.on("beforeload", function (e) {
	      e.cancel = true;
	      var pageIndex = e.data.pageIndex; 
	      var pageSize = e.data.pageSize;
	      search(pageSize,pageIndex);
	    });
	    search(10,0);
	    function search(pageSize,pageIndex) {
	    	var param = new mini.Form("searchForm")
		    param.validate();
		    var data = param.getData();
		    data.pageNumber=pageIndex+1;
		        data.pageSize=pageSize;
		    var json = mini.encode(data);
		    url = "/edCustController/getTdEdCustsPage";
		    CommonUtil.ajax({
		      url : url,
		      data : json,
		      callback : function(data) {
		        grid.setTotalCount(data.obj.total);
		        grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
		        grid.setData(data.obj.rows);
		        search2(10,0);
		      }
		    });
		 };
		 
		 /**
		 *
		 */
		 var grid2 = mini.get("datagrid2");
		 grid2.set({
	        columns: [
	        		{ type: "indexcolumn"},
		            { field: "cnName", width: 210, headerAlign: "center", allowSort: true, header: "客户名称"},
		            { field: "creditName", width: 70, headerAlign: "center", allowSort: true, header: "额度品种"},
		            { field: "term", width: 40, headerAlign: "center", allowSort: true, header: "期限"},
		            { field: "termType", width: 40, headerAlign: "center", allowSort: true, header: "单位",renderer:"CommonUtil.dictRenderer", dict:'termType'},
		            { field: "loanAmt", width: 130, headerAlign: "center", align: "right",allowSort: true, header: "授信额度(元)",numberFormat:"#,0.00"},
		            { field: "adjAmt", width: 130, headerAlign: "center", align: "right",allowSort: true, header: "额度调整(元)",numberFormat:"#,0.00"},
		            { field: "frozenAmt", width: 130, headerAlign: "center",align: "right", allowSort: true, header: "冻结额度(元)",numberFormat:"#,0.00"},
		            { field: "avlAmt", width: 130, headerAlign: "center",align: "right", allowSort: true, header: "可用额度(元)",numberFormat:"#,0.00"},
		            { field: "dueDate", width: 120, headerAlign: "center", align: "center",allowSort: true, header: "额度有效期"}
		        ]
		    });
		grid2.frozenColumns(0, 3);
        grid2.on("beforeload", function (e) {
	      e.cancel = true;
	      var pageIndex = e.data.pageIndex; 
	      var pageSize = e.data.pageSize;
	      search2(pageSize,pageIndex);
	    });
	    function search2(pageSize,pageIndex,row) {
		    var data = {};
		    data.pageNumber=pageIndex+1;
		    data.pageSize=pageSize;
		    if(row != null && row != "undefinded")
		    	data.custNo = row.ecifNum;
		    else{
		    	data.custNo = "undefinded";
		    }
		    var json = mini.encode(data);
		    url = "/edCustController/getTdEdCustDetailPage";
		    CommonUtil.ajax({
		      url : url,
		      data : json,
		      callback : function(data) {
		        grid2.setTotalCount(data.obj.total);
		        grid2.setPageIndex(pageIndex);
		        grid2.setPageSize(pageSize);
		        grid2.setData(data.obj.rows);
		      }
		    });
		 };
		 function onSelectionChanged(e) {
        
            var grid = e.sender;
            var record = grid.getSelected();
            if (record) {
                 search2(10,0,record);
            }else{
            	
            }
        }
    </script>
</body>
</html>
