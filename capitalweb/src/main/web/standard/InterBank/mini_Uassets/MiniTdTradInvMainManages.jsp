<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>委外业务管理页面</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset title">
			<legend>查询</legend>
			<div id="search_form">
				<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="审批单编号：" labelStyle="text-align:right;" emptyText="请输入审批单编号"/>
				<input id="productName" name="productName" class="mini-textbox" labelField="true" label="资产名称：" labelStyle="text-align:right;" emptyText="请输入资产名称"/>
				<input id="productCode" name="productCode" class="mini-textbox" labelField="true" label="资产编码："labelStyle="text-align:right;" emptyText="请输入资产代码"/>
				<span style="float:right;margin-right: 190px">
					<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
				</span>
			</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a class="mini-button" style="display: none"    id="upload_btn" onclick="uploadFile();">上传</a>
				<a class="mini-button" style="display: none"    id="down_btn" onclick="download();">下载模板</a>
				<a class="mini-button" style="display: none"   id="detail_btn" onclick="sllinkbutton();">查看委外业务底层资产</a>
			</span>
			<div id="TdTradInvMainManages" class="mini-fit" style="margin-top: 2px;">
				<div id="form_grid" class="mini-datagrid borderAll" width="100%" height="100%" sizeList="[10,20]" pageSize="10" allowAlternating="true" sortMode="client">
					<div property="columns">
						<div type="indexcolumn"></div>
						<div field="dealNo" width="200" allowSort="true" headerAlign="center" align="center">审批单编号</div>
						<div field="product.prdName" width="150" allowSort="false" headerAlign="center">产品名称</div>
						<div field="productName" width="150" allowSort="false" headerAlign="center">资产名称</div>
						<div field="productCode" width="80" allowSort="true" headerAlign="center" align="left">资产编码</div>
						<div field="ccy" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
						<div field="amt" width="80" numberFormat="#,0.00" allowSort="true" headerAlign="center" align="right">本金</div>
						<div field="rateType" width="100" renderer="CommonUtil.dictRenderer" data-options="{dict:'rateType'}" align="center" headerAlign="center">利率类型</div>
						<div field="contractRate" width="80" allowSort="true" numberFormat="p" headerAlign="center" align="right">合同利率</div>
						<div field="vDate" width="80" allowSort="true" headerAlign="center" align="center">起息日期</div>
						<div field="mDate" width="80" allowSort="true" headerAlign="center" align="center">到期日期</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				top['TdTradInvMainManages'] = window;
				var grid = mini.get("form_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});

				function search(pageSize, pageIndex) {
					var form = new mini.Form("search_form");
					form.validate();
					if (form.isValid == false)
						return;
					//提交数据
					var data = form.getData();//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: '/ProductAppController/selectPageProduct',
						data: param,
						callback: function (data) {
							var grid = mini.get("form_grid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}

				//上传
				function uploadFile() {
						var grid = mini.get("form_grid");
						var row = grid.getSelected();
						if (row) {
							mini.open({
								url: CommonUtil.baseWebPath() + "/mini_Uassets/MiniTdTradInvMainEdit.jsp",
								width: "410px",
								height: "250px",
								title: "文件上传",
								onload: function (action) {
									var iframe = this.getIFrameEl();
									var data = { action: "edit", dealNo: row.dealNo };
									iframe.contentWindow.mini.get("dealNo").setText(data.dealNo);
									iframe.contentWindow.mini.get("dealNo").setValue(data.dealNo);

								},
								ondestroy: function (action) {
									var iframe = this.getIFrameEl();
									grid.reload();

								}
							})
						} else {
							mini.alert("请选中一条记录");
						}
				}
				
				//下载模板
					function download() {
						//window.location.href = '<%=basePath%>/sl/PrintController/downloadTemplate/' + '200';
						window.location.href = '<%=basePath%>/sl/QdReportsController/exportTradInvMain';
					}
					
				
				function sllinkbutton(e) {
					var grid = mini.get("form_grid");
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_Uassets/MiniTdTradeInvDetailMainManage.jsp";
						var tab = { id: "ParamsDetail", name: "ParamsDetail", title: "委外业务基础资产", url: url, showCloseButton: true };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}
				
				function getData() {
						var row = null;
						var grid = mini.get("form_grid");
						row = grid.getSelected();

						return row;
					}
				
				function clear() {
					var form = new mini.Form("#search_form");
					form.clear();
				}

				function query() {
					var grid = mini.get("form_grid");
					search(grid.pageSize, 0);
				}

				function checkBoxValuechanged() {
					var grid = mini.get("form_grid");
					search(grid.pageSize, 0);
				}

				$(document).ready(function () {
					search(10, 0);
				})
			</script>
		</body>
		</html>