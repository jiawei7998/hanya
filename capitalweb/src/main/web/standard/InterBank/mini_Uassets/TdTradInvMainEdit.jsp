<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../../global.jsp"%>
<script type="text/javascript">
	/**
	*委外业务文件导入
	*
	* @author zlf
	*/
$(document).ready(function(){

<% String param=request.getParameter("dealNo"); %>
	var param='<%=param %>';

		//上传文件	
	$('#upload_btn').click(function() {
	    if(CommonUtil.isNull($("#file").val())){
	      $.messager.alert("提示信息","请选择文件！");
	      return false;
	    }
		$.messager.confirm('系统提示', '确认导入委外业务信息文件？', function(r) {
			if (r) {
				$.messager.progress({
					title : '提示信息',
					msg : '正在初始化,请稍后...'
				});
			
				
				$('#frm').form('submit', {
					onSubmit : function() {
						return true;
					},
					success : function(data) {
						
						$.messager.progress('close');
						$.messager.alert("提示信息", data, 'info', function(){
							CommonUtil.closedailog("");
                        }); 
                        /* if(data=='委外业务信息上传成功'){
                        	var urls = '/Uassets/TdTradeInvDetailMainManage.jsp';
							$.messager.confirm('提示信息', '是否需要查看委外业务信息？', function(r){
								if (r == true){
								$.ajax({
									url:'./Uassets/TdTradeInvDetailMainManage.jsp'
									
								});
									//window.location.href = '../standard/Uassets/TdTradeInvDetailMainManage.jsp';
								}
							});
                        } */
                        TdTradInvMainManage.loadData();
					},
				});
			}
		});
	});
	
});
</script>

<div id="TtMktCustNewsEdit" class="easyui-panel" autoWidth="true" title="" border="false" fit="true" style="overflow-y: auto;">
	<div class="easyui-panel" autoWidth="true" border="false" hborder="true">
		<form id="frm" method="post" enctype="multipart/form-data" action="../sl/TdTradInvMainController/setTdTradInvMainByExcel?tradeid="+'<%=param %>'>                   
			<table id="fileUploadTable" class="form_table">
				<tr>
				  <td>审批单编号</td>
		       	  <td><input name="tradeid" value='<%=param %>' class="easyui-textbox" /></td>
				</tr>
				<tr>
				  <td>委外业务文件：</td>
				  <td><input name="file[]" id="file" type="file" multiple="multiple"/></td>
				</tr>
			</table>
		</form>
		<div class="btn_div">
			<a href="javascript:void(0)" id="upload_btn" class="easyui-linkbutton" >上传</a>
	  	</div>
	  	
	</div>
	<div id="win"></div>  
</div>