<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%> 
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<div id="TtMkCustNewsManage" style="width:100%;height:100%;">
		 <div class="mini-toolbar" id="search" style="border-bottom:0;padding:0px;">
			<table id="search_form" class="form_table" width="100%" cols="6">
	          <tr>
		            <td>版本号：</td>
		            <td>
		            <input id="version" name="version" class="mini-textbox" emptyText="版本号" />
		            </td>
		            
	           		 <td  style="white-space:nowrap;">
	                 	<a class="mini-button" style="display: none"  id="search_btn" name="search_btn"  onclick="search(10,0);" >查询</a>
	                </td>
	           </tr>
        	</table>
        </div>
	
	<div id="custNewsGrid" class="mini-datagrid borderAll" style="width:100%;height:420px;"
         sizeList="[20,30,50,100]" pageSize="20">
        <div property="columns">
		<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
		<div field="version" width="100" allowSort="false" headerAlign="center" align="center"  >版本号</div>
		<div field="dealNo" width="180" allowSort="true" headerAlign="center" align="center"  >交易单号</div>
		<div field="cnm" width="100" allowSort="false" headerAlign="center" align="left">基础资产客户</div>
		<div field="baseAssetType" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'baseAssetType'}" >基础资产类型</div>
		<div field="isCust" width="100" allowSort="false" headerAlign="center" align="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否本行客户</div>
		<div field="bigCatName" width="100" allowSort="false" headerAlign="center" align="left" >行业大类</div>
		<div field="middleCatName" width="100" allowSort="false" headerAlign="center" align="left">行业中类</div>
		<div field="smallCatName" width="100" allowSort="false" headerAlign="center" align="center" >行业小类</div>
		<!-- <div field="guarType" width="100" allowSort="false"headerAlign="center" align="left" >质押物名称</div> -->
		<div field="pledgeType" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'pledgeType'}">质押物类型</div>
		<div field="pledgeAmt" width="100" allowSort="false" headerAlign="center" align="right" numberFormat="#,0.00">质押物金额</div>
		<div field="pledgeVdate" width="100" allowSort="false" headerAlign="center" align="center">质押起始日</div>
		<div field="pledgeMdate" width="100" allowSort="false" headerAlign="center" align="center">质押到期日</div>
		<div field="pledgeRate" width="100" allowSort="false" headerAlign="center" align="left">质押利率_%</div>
		<div field="pledgeSpread" width="100" allowSort="false" headerAlign="center" align="left">质押利率差_%</div>
		<div field="mortgageType" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'icWay'}">基础资产增信方式</div>
		<div field="mortgageRemark" width="100" allowSort="false" headerAlign="center" align="left">基础资产增信描述</div>
		<div field="shareType" width="100" allowSort="false" headerAlign="center" align="left"  renderer="CommonUtil.dictRenderer" data-options="{dict:'shoreholderType'}">股东性质</div>
		<div field="guarantor" width="100" allowSort="false" headerAlign="center" align="center">保证人名称</div>
		<div field="guarantorIndus" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'GBT4754_2011'}">保证人所属行业</div>
		<div field="secId" width="100" allowSort="false" headerAlign="center" align="center">债券编号</div>
		<!-- <div field="secNm" width="100" allowSort="false" headerAlign="center" align="center">债券名称</div> -->
		<div field="optExerDate" width="100" allowSort="false" headerAlign="center" align="center">行权日期</div>
		<div field="stockId" width="100" allowSort="false" headerAlign="center" align="center">股票代码</div>
		<!-- <div field="stockNm" width="100" allowSort="false" headerAlign="center" align="center">股票名称</div> -->
		<div field="warnLinePrice" width="100" allowSort="false" headerAlign="center" align="left">预警线</div>
		<div field="coverLinePrice" width="100" allowSort="false" headerAlign="center" align="left">补仓线</div>
		<div field="openLinePrice" width="100" allowSort="false" headerAlign="center" align="left">平仓线</div>
		<!-- <div field="lmtId" width="100" allowSort="false" headerAlign="center" align="center">额度编号</div>
		<div field="colRightNo" width="100" allowSort="false" headerAlign="center" >质押物权证号</div> -->
		<div field='coefficient'  headerAlign= "center" allowSort= "true"       align='right' numberFormat="#,0.00">风险资产系数</div>                    
		<div field='inBalance'    headerAlign= "center" allowSort= "true"       align='right' numberFormat="#,0.00">对应投资金额</div>                    
		<div field='assetBalanceAuto' headerAlign= "center" allowSort= "true"   align='right' numberFormat="#,0.00">风险资产占用</div>      
		<div field='rsrAmt' headerAlign= "center" allowSort= "true"   align='right' numberFormat="#,0.00">风险缓释额</div>
		<div field="remark" width="100" allowSort="false" headerAlign="left" >备注</div>
        </div>   
	</div>
	</div>

	<script  type="text/javascript">
    	mini.parse();
    	$(document).ready(function(){
    		search(10,0);
    	})
    	var dealNo = "";
    	dealNo = GetQueryString("dealNo");
    	var grid=mini.get("custNewsGrid");
    	grid.on("beforeload", function (e) {
    		e.cancel = true;
    		var pageIndex = e.data.pageIndex; 
    		var pageSize = e.data.pageSize;
    		search(pageSize,pageIndex);
    	});
    	
    	
    	//查询事件
function search(pageSize, pageIndex) {
	var form = new mini.Form("search");
	form.validate();
	if (form.isValid == false) return;
	var data = form.getData();//获取表单多个控件的数据  
	data['pageNumber'] = pageIndex + 1;
	data['pageSize'] = pageSize;
	data['dealNo'] = dealNo;
	var param = mini.encode(data); //序列化成JSON
	CommonUtil.ajax({
		url: '/TdBaseAssetController/getBaseAssetHisLists',
		data: param,
		callback: function (data) {
			
			grid.setTotalCount(data.obj.total);
			grid.setPageIndex(pageIndex);
			grid.setPageSize(pageSize);
			grid.setData(data.obj.rows);
		}
	});
}
    	
function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}
</script>