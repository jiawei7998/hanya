<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>

		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>查看委外业务底层资产页面</title>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset title">
				<legend>查询</legend>
				<div id="search_form">
					<input id="iDate" name="iDate" class="mini-datepicker" labelField="true" label="上传日期：" labelStyle="text-align:right;" emptyText="请选择上传日期"/>
					<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="审批单编号：" labelStyle="text-align:right;" emptyText="请输入审批单编号"/>
					<span style="float:right;margin-right: 400px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query()" style="margin-left:5px">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					</span>	
				</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a id="delete_btn" class="mini-button" style="display: none"  style="margin-left:5px" onclick="del">删除</a>
			</span>
			<div id="TdTradeInvDetailMainManage" class="mini-fit" style="margin-top: 2px;">
				<div id="form_grid" class="mini-datagrid borderAll" title="委外业务底层基础资产" width="100%" height="35%"  sizeList="[10,20]" pageSize="10" allowAlternating="true" 
				    sortMode="client" onselectionchanged="onSelectionChanged" >
					<div property="columns">
						<div type="indexcolumn"></div>
						<div field="iDate" width="80" allowSort="true" headerAlign="center" align="center">上传日期</div>
						<div field="version" width="80" allowSort="true" headerAlign="center" align="center">版本号</div>
						<div field="totalSecInv" width="120" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">证券投资合计</div>
						<div field="asset" width="120" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">资产类合计</div>
						<div field="debt" width="120" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">负债类合计</div>
						<div field="cpriceAssetCost" width="120" allowSort="true" headerAlign="center" align="right"  numberFormat="#,0.00">资产净值（按成本）</div>
						<div field="cpriceAssetMarket" width="120" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">资产净值（按市值）</div>
						<div field="aName" width="250" headerAlign="center">资产名称</div>
					</div>
				</div>
				<div id="detail_grid" class="mini-datagrid borderAll" title="委外业务底层基础资产明细" width="100%" height="65%" sizeList="[10,20]" pageSize="10" allowAlternating="true" sortMode="client">
					<div property="columns">
						<div type="indexcolumn"></div>
						<div field="iDate" width="80" allowSort="true" headerAlign="center" align="center">上传日期</div>
						<div field="version" width="80" allowSort="true" headerAlign="center" align="center">版本号</div>
						<div field="aType" width="60" allowSort="false" headerAlign="center" align="center">资产类型</div>
						<div field="secId" width="100" allowSort="true" headerAlign="center" align="center">债券编号</div>
						<div field="secNm" width="200" allowSort="false" headerAlign="center">债券名称</div>
						<div field="faceamt" width="100" allowSort="true" headerAlign="center" numberFormat="#,0.00" align="right">面额</div>
						<div field="unitCost" width="100"  allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">单位成本</div>
						<div field="costAmt" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">成本总价</div>
					</div>
				</div>
			</div>
			
			<script type="text/javascript">
				mini.parse();
				mini.get("dealNo").setEnabled(false);
				var param = top["TdTradInvMainManages"].getData();
				var grid = mini.get("form_grid");
				var gridDetail = mini.get("detail_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					searchMain(pageSize, pageIndex);
				});

				gridDetail.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					searchDetail(pageSize, pageIndex);
				});
				
				function initform() {
					mini.get("dealNo").setValue(param.dealNo);
					mini.get("dealNo").setText(param.dealNo);
				}
				
				function searchMain(pageSize, pageIndex) {
					var form = new mini.Form("search_form");
					form.validate();
					if (form.isValid == false)
						return;
					//提交数据
					var data = form.getData();//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					data['iDate'] = mini.get('iDate').getFormValue();
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: '/TdTradInvMainController/selectTdTradInvMainHisPage',
						data: param,
						callback: function (data) {
							var grid = mini.get("form_grid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}

				function onSelectionChanged(e){
					searchDetail(grid.pageSize,0);
				}

				function searchDetail(pageSize, pageIndex) {
					var form = new mini.Form("search_form");
					form.validate();
					if (form.isValid() == false){
						return;
					}

					var row = grid.getSelected();
					if(!row){
						mini.alert("请先选中一条汇总信息","系统提示");
						return;
					}
					
					var data = form.getData();//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					data['iDate'] = row.iDate;
					data['version'] = row.version;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: '/TdTradInvMainController/selectTdTradInvDetailHisPage',
						data: param,
						callback: function (data) {
							var gridDetail = mini.get("detail_grid");
							gridDetail.setTotalCount(data.obj.total);
							gridDetail.setPageIndex(pageIndex);
							gridDetail.setPageSize(pageSize);
							gridDetail.setData(data.obj.rows);
						}
					});
				}
				function clear() {
					// var form = new mini.Form("#search_form");
					// form.clear();
					mini.get("iDate").setValue("");
				}

				function query() {
					searchMain(grid.pageSize, 0);
					gridDetail.clearRows();
				}

				function del() {
					var grid = mini.get("form_grid");
					var row = grid.getSelected();
					if (row) {
						mini.confirm("你确定要删除所选的记录吗?", "系统警告", function (value) {
							if (value == "ok") {
								CommonUtil.ajax({
									url: "/TdTradInvMainController/deleteTdtradeInv",
									data: {tradeid : row.tradeId, version: row.version, iDate : row.iDate},
									callback: function (data) {
										if (data.code == "error.common.0000") {
											mini.alert("删除成功","系统提示",function(){
												query();
												gridDetail.clearRows();
											});
											
										} else {
											mini.alert("删除失败，请重新选择");
										}
									}
								});
							}
						});
					} else {
						mini.alert("请选择一条汇总记录!", "消息提示");
					}
				}
				
				$(document).ready(function () {
					initform();
					searchMain(grid.pageSize, 0);
				})
			</script>
		</body>
		</html>