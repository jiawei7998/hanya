<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript">
	var TdTradInvMainManage={};
	/* tab页参数对象  */
	TdTradInvMainManage.tabOptions=$.getTabOptions();
	
	/**
	 * 加载委外业务列表
	 */
	TdTradInvMainManage.loadData=function(){
		$("#TdTradInvMainManage").find("#search_btn").click();
	},
	$(document).ready(function(){
		/********************************************************************
		 *组件定义
		 *******************************************************************/
		/* 委外业务列表  */
	TdTradInvMainManage.datagrid=	$("#TdTradInvMainManage").find('#TdTradInvMain_grid').sldatagrid({
			fit:true,
			title:"委外业务列表", 
			idField:'dealNo',
			singleSelect:true,
			rownumbers:true, 
			pagination:true,
			columns:[[
				{field:'dealNo',title:'审批单编号',width:200},
				{field:'prdName',title:'产品名称',width:200},
				{field:'productName',title:'资产名称',width:100},
				{field:'productCode',title:'资产编码',width:100},
				{field:'ccy',title:'币种',width:60,dict:'Currency'},
				{field:'amt',title:'本金',width:60},
				{field:'rateType',title:'利率类型',width:100,dict:'rateType'},
				{field:'acturalRate',title:'实际利率',width:100},
				{field:'vDate',title:'起息日期',width:100},
				{field:'mDate',title:'到期日期',width:100}
			]]
		//	search:{button:$("#TdTradInvMainManage").find("#search_btn"),form:$("#TdTradInvMainManage").find("#search_form"),href:'/ProductAppController/selectPageProduct'}
		});
		
			//事件：导入
		$("#TdTradInvMainManage").find('#uploading_btn').click(function() {
		//必须选中一条记录
			var selected = $("#TdTradInvMainManage").find('#TdTradInvMain_grid').datagrid('getSelected');
			if(!selected){
				$.messager.alert("系统提示","请选一条需要上传的记录!");
				return false;
			}else{
 				/* var dealNos = new Array();
				dealNos.push(selected.dealNo);
			     CommonUtil.ajax( {
						url:"/ProductApproveController/getTdProductApproveMain/",
						data:dealNos,
						callback : function(data) {
			    			TdTradInvMainManage.loadData();
					}
				});  */
				var urls = './Uassets/TdTradInvMainEdit.jsp?dealNo='+selected.dealNo;
				CommonUtil.dialog({
					title:"文件上传",
				    modal:true,
				    resizable:true,
				    height:220,
				    width:380,
				    urls:urls
				});
			}
		});
			//按钮事件：查询委外业务底层基础资产
		$("#TdTradInvMainManage").find("#detail_btn").sllinkbutton({
			onClick : function() {
					//必须选中一条记录
				var selected = $("#TdTradInvMainManage").find('#TdTradInvMain_grid').datagrid('getSelected');
				if(!selected){
					$.messager.alert("系统提示","请选一条需要上传的记录!");
					return false;
				}else{
					CommonUtil.dialog({
						title:"委外业务基础资产",
					    modal:true,
					    resizable:true,
					    height:700,
					    width:1000,
					    urls:"./Uassets/TdTradeInvDetailMainManage.jsp?dealNo="+selected.dealNo,
					    //queryParams: { offsetDate:offsetDate,mDate:offsetDate},
					    onCloseed:function(data){
					    	
					    }
					});
				
				}
				
			}
		});
		
			//模板下载
		$("#TdTradInvMainManage").find("#down_btn").click(function(){
				window.location.href = '../sl/PrintController/downloadTemplate/' + '200';
		});
			
			
$("#TdTradInvMainManage").find("#search_btn").click(function(){
			
	TdTradInvMainManage.datagrid.datagrid('getPager').pagination('select', 1); 
		});
		
		//查询某页
		TdTradInvMainManage.datagrid.datagrid('getPager').pagination({
			onSelectPage:function(pageNumber, pageSize){
				var params = CommonUtil.getJson('TdTradInvMainManage #search_form');
				params.pageNumber = pageNumber;
				params.pageSize = pageSize;
				CommonUtil.ajax( {
					url:'/ProductAppController/selectPageProduct',
					data:params,
					callback : function(data) {
						TdTradInvMainManage.datagrid.datagrid('loadData',data.obj);
					}
				});
			}
		});
		/********************************************************************
		 *数据初始化 
		 *******************************************************************/
		//默认查询用户列表
		TdTradInvMainManage.loadData();
	});
</script>
<div id="TdTradInvMainManage" class="easyui-layout" data-options="fit:true,border:true">   
    <div data-options="region:'north',title:'查询',collapsible:false,iconCls:'icon-search'" style="height:100px;">
    	<table id="search_form" class="form_table" data-option="width:'100%'">
	    	 <tr>
		      	<td>审批单编号</td>
		        <td><input id="dealNo" class="easyui-textbox" /></td>
		        <td>资产名称</td>
		        <td><input id="productName" class="easyui-textbox" /></td>
		        <td>资产编码</td>
		        <td><input id="productCode" class="easyui-textbox" /></td>
		        <!-- <td>审批发起人</td>
		        <td><input id="sponsor" class="easyui-textbox" /></td> -->
			</tr> 
        </table>
        <div class="btn_div">
        	<a href="javascript:void(0)"  id="clear_btn" class="easyui-linkbutton" data-options="iconCls:'icon-clear',onClick:function(){$('#'+TdTradInvMainManage.tabOptions.id).find('#search_form').first('form').form('clear');}">清空</a>
        	<a href="javascript:void(0)"  id="search_btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a>
        	<a href="javascript:void(0)" id="uploading_btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'">上传</a>
        	<a href="javascript:void(0)"  id="down_btn" class="easyui-linkbutton" data-options="iconCls:'icon-down'">下载模板</a>
        	<a href="javascript:void(0)" id="detail_btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查看委外业务底层资产</a>
        </div>
    </div>   
    <div data-options="region:'center'" >
    	<table id="TdTradInvMain_grid"></table>
    </div>   
</div>