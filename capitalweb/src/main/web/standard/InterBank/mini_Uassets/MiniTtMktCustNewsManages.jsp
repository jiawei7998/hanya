<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>

<body style="width:100%;height:100%;background:white">
		
			<fieldset class="mini-fieldset">
					<legend>查询</legend>
					<div id="search_form" >
						<input id="cno" name="cno" class="mini-textbox"  labelField="true" label="客户号：" labelStyle="text-align:right;" emptyText="客户号" />
						<input id="cname" name="cname" class="mini-textbox"  labelField="true" label="客户名称：" labelStyle="text-align:right;" emptyText="客户名称" />
						<span style="float:right;margin-right: 150px">
								<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
								<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
							</span>
						<!-- <div class="mini-toolbar" style="padding:2px;">
							<table style="width:100%;">
								<tr>
									<td colspan="6">
									<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
									<a class="mini-button" style="display: none"   id="delete_btn" onclick="clear();">清空</a>
									<a class="mini-button" style="display: none"   id="imp_btn" name="imp_btn"   onclick="uploadFile();">上传</a>
									<a class="mini-button" style="display: none"  id="download_search" name="download_search"    onclick="download();" >下载模板</a>
								</tr>
							</table>
						</div> -->
					</div>
			</fieldset>	
			<span style="margin:2px;display: block;">
					<a class="mini-button" style="display: none"   id="imp_btn" name="imp_btn"   onclick="uploadFile();">上传</a>
					<a class="mini-button" style="display: none"  id="download_search" name="download_search"    onclick="download();" >下载模板</a>
				</span>	
		<div id="stockManage" class="mini-fit" >
			<div id="custNewsGrid" class="mini-datagrid borderAll"  allowAlternating="true" style="width:100%;height:100%;"
			sortMode="client" idField="cno" >
				<div property="columns">
						<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
					<div field="importDate" width="100" allowSort="false" headerAlign="center" align="center" >导入时间</div>
					<div field="cno" width="100" allowSort="false" headerAlign="center" align="center" >客户号</div>
					<div field="cname" width="100" allowSort="false" headerAlign="center"  >客户名称</div>
					<div field="title" width="100" allowSort="false" headerAlign="center" >标题</div>
					<div field="news" width="100" allowSort="false" headerAlign="center" >详情</div>
				</div>   
			</div>
		</div>
	</div>	
</body>
	<script  type="text/javascript">
	mini.parse();
	var grid = mini.get("custNewsGrid");
	var form = new mini.Form("#search_form");
	grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
    	$(document).ready(function(){
    		search(10,0);
		}); 
		function clear(){
			form.clear();
		}
		function query(){
			search(grid.pageSize, 0);
		}
		
		function search(pageSize,pageIndex){
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息");
				return;
			}
			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var param = mini.encode(data); //序列化成JSON
			//ajax请求
			CommonUtil.ajax({
				url:'/TtMktCustNewsController/getTtMktCustNewsList',
				data:param,
				callback:function(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		//模板下载
		function  download(){
			//window.location.href = '<%=basePath%>/sl/PrintController/downloadTemplate/' + '600';
			window.location.href = '<%=basePath%>/sl/QdReportsController/exportCustNews';
		}
		//上传
		function uploadFile(){
			mini.open({
				url:CommonUtil.baseWebPath() +"/mini_Uassets/MiniTtMktCustNewsEdits.jsp",
				width:"420px",
				height:"150px",
				title:"文件上传",
				ondestroy: function (action) {
						// mini.alert("文件导入成功","提示");
							search(10,0);//重新加载页面
				}
			});
		}
	</script>
	</body>