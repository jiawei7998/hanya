<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js" type="text/javascript"></script>
	<title>委外业务管理上传页面</title>
</head>

<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset title">
		<legend>文件上传</legend>
		<table id="search_form" class="mini_table">
			<tr>
				<td>
					<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="审批单编号:" style="width:300px;" emptyText="请输入审批单编号"/>
				</td>
			</tr>
			<tr>
				<td>
					<input class="mini-htmlfile" name="file1" id="file1" labelField="true" label="委外业务文件:" style="width:300px;" />
				</td>
			</tr>
		</table>
	</fieldset>
	<div class="mini-toolbar" style="margin-left: 320px; margin-top: 10px;">
		<table style="width:100%;">
			<td style="width:100%;">
				<a class="mini-button" style="display: none"    id="upload_btn" onclick="ajaxFileUpload();">上传</a>
			</td>
		</table>
	</div>
	<script type="text/javascript">
		mini.parse();
		mini.get("dealNo").setEnabled(false);
		function ajaxFileUpload() {
				var inputFile = $("#file1 > input:file")[0];
				var messageid = mini.loading("加载中,请稍后... ", "提示信息");
				var dealNo = mini.get("dealNo").getValue();
				var url = '<%=basePath%>/sl/TdTradInvMainController/setTdTradInvMainByExcel';
				$.ajaxFileUpload({
					url: url, //用于文件上传的服务器端请求地址
					fileElementId: inputFile,               //文件上传域的ID
					data: {'dealNo' :  dealNo},
					dataType: 'json',
					success: function (data, status)    //服务器成功响应处理函数
					{
						mini.hideMessageBox(messageid);
						mini.alert("上传成功!");
					},
					error: function (data, status, e)   //服务器响应失败处理函数
					{
						mini.hideMessageBox(messageid);
						mini.alert("提示信息:"+data.responseText,"系统提示",function(){
							cancel();
						});
					},
					complete: function () {
						var jq = $("#file1 > input:file");
						jq.before(inputFile);
						jq.remove();
					}
				});
			}
			function cancel() {
				window.CloseOwnerWindow();
			}		
	</script>
</body>
</html>