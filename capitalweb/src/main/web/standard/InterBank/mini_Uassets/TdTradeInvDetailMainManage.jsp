<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript">
	<% String param=request.getParameter("dealNo"); %>
	var TdTradeInvDetailMainManage={};
	/* tab页参数对象  */
	TdTradeInvDetailMainManage.tabOptions=$.getTabOptions();
	
	/**
	 * 加载委外业务列表
	 */
	TdTradeInvDetailMainManage.loadData=function(){
		$("#TdTradeInvDetailMainManage").find("#search_btn").click();
	},
	$(document).ready(function(){
		/* 委外业务列表  */
		$("#TdTradeInvDetailMainManage").find('#main_grid').sldatagrid({
			fit:true,
			title:"委外业务底层基础资产", 
			//idField:'tradeid',
			singleSelect:true,
			rownumbers:true, 
			pagination:true,
			columns:[[
				//{field:'tradeId',title:'交易编号',width:100},
				{field:'totalSecInv',title:'证券投资合计',width:100},
				{field:'asset',title:'资产类合计',width:100},
				{field:'debt',title:'负债类合计',width:100},
				{field:'cpriceAssetCost',title:'资产净值（按成本）',width:100},
				{field:'cpriceAssetMarket',title:'资产净值（按市值）',width:100},
				//{field:'cpriceAssetUnit',title:'基金单位净值',width:100},
				//{field:'cpriceCost',title:'净值（按成本）',width:100,},
				//{field:'cpriceMarket',title:'净值（按市值）',width:100},
				{field:'aName',title:'资产 名称',width:100},
				{field:'version',title:'日期',width:100},
				{field:'remark',title:'备注',width:100}
			]],
			search:{button:$("#TdTradeInvDetailMainManage").find("#search_btn"),form:$("#TdTradeInvDetailMainManage").find("#search_form"),href:'/TdTradInvMainController/selectTdTradInvMainHisPage'}
		});
		/* 委外业务明细列表  */
		$("#TdTradeInvDetailMainManage").find('#detail_grid').sldatagrid({
			fit:true,
			//title:"委外业务底层基础资产明细", 
			//idField:'tradeid',
			singleSelect:true,
			rownumbers:true, 
			pagination:true,
			columns:[[
				{field:'tradeId',title:'交易编号',width:100},
				{field:'aType',title:'资产类型',width:100},
				{field:'secId',title:'债券编号',width:100},
				{field:'secNm',title:'债券名称',width:100},
				{field:'faceamt',title:'面额',width:100},
				{field:'unitCost',title:'单位成本',width:100},
				{field:'costAmt',title:'成本总价',width:100},
				{field:'version',title:'日期',width:100},
				{field:'remark',title:'备注',width:100}
			]],
			search:{button:$("#TdTradeInvDetailMainManage").find("#search_btn"),form:$("#TdTradeInvDetailMainManage").find("#search_form"),href:'/TdTradInvMainController/selectTdTradInvDetailHisPage'}
		});
		
		//清空按钮定义
		TdTradeInvDetailMainManage.clearForm = function() {
			$("#TdTradeInvDetailMainManage").find("#search_form").find("#version").datebox({value:""});
		};
		//清空
		$('#TdTradeInvDetailMainManage #clear_btn').click(function(){TdTradeInvDetailMainManage.clearForm();});
		
			/* 按钮事件：删除用户  */
		$("#TdTradeInvDetailMainManage").find("#delete_btn").on("click",function(){
		//必须选中一条记录
			var selected = $("#TdTradeInvDetailMainManage").find('#main_grid').datagrid('getSelected');
			if(!selected){
				return false;
			}
			$.messager.confirm("系统警告","您确认要删除委外业务基础资产主表以及明细表<span style='color:#FF3333;font-weight:bold;'>" +  selected.version  + ")</span>吗?",function(value){   
				 if (value){
					CommonUtil.ajax( {
						url:"/TdTradInvMainController/deleteTdtradeInv",
						data:{version : selected.version},
						callback : function(data) {
					    	$.messager.alert("系统提示","已成功删除删除委外业务基础资产主表以及明细表<span style='color:#FF3333;font-weight:bold;'>" + selected.version +  ")</span>.");
					    	TdTradeInvDetailMainManage.loadData();
					    }
					});
				 }   
			}); 
		});
		/********************************************************************
		 *数据初始化 
		 *******************************************************************/
		//默认查询用户列表
		TdTradeInvDetailMainManage.loadData();
	});
</script>

<div id="TdTradeInvDetailMainManage" class="easyui-layout" data-options="fit:true,border:true">
	   <div data-options="region:'north',title:'查询',collapsible:false,iconCls:'icon-search'" style="height:100px;">
    	<table id="search_form" class="form_table" data-option="width:'100%'">
    	 	<tr>
		      	<td>上传日期</td>
		        <td><input id="version" type="text" class="easyui-datebox easyui-validatebox" /></td>
			</tr> 
	    	 <tr style="display:none">
		      	<td>审批单编号</td>
		        <td><input id="dealNo" name="dealNo" value='<%=param %>' class="easyui-textbox" /></td>
		      <!--   <td>审批发起人</td>
		        <td><input id="sponsor" class="easyui-textbox" /></td> -->
			</tr> 
        </table>
        <div class="btn_div">
        	<!-- <a href="javascript:void(0)"  id="clear_btn" class="easyui-linkbutton" data-options="iconCls:'icon-clear',onClick:function(){$('#'+TdTradeInvDetailMainManage.tabOptions.id).find('#search_form').first('form').form('clear');}">清空</a> -->
        	<a href="javascript:void(0)"  id="search_btn" class="easyui-linkbutton" >查询</a>
        	<a href="javascript:void(0)" id="clear_btn" class="easyui-linkbutton" >清空</a>
        	<a href="javascript:void(0)"  id="delete_btn" class="easyui-linkbutton" >删除</a>
        </div>
    </div>      
    <div data-options="region:'center'" >
    	<table id="main_grid"></table>
    </div>   
    <div data-options="region:'south',border:true" title="委外业务底层基础资产明细" style="height:60%;overflow-y:auto;">
		<table id="detail_grid"></table>
	</div>
</div>
