<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%> 
<html>
<head>
	<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset>
		<legend>
			<label>查询</label>
		</legend>
		 <div id="search_form" style="width:100%;">
					<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="交易单号：" labelStyle="text-align:right;" emptyText="交易单号" />
					<input id="assetType" name="assetType" class="mini-combobox" labelField="true" label="基础资产类别：" labelStyle="text-align:right;"  
				disabled = "true" data = "CommonUtil.serverData.dictionary.baseAssetType" showNullItem="false"  allowInput="false"  />
				<span style="float:right;margin-right: 150px">
						<a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)" >查询</a>
						<a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
					</span>	
					
        </div>
		</fieldset>
		<span style="margin:2px;display: block;">
				<a class="mini-button" style="display: none"   id="imp_btn" name="imp_btn"   onclick="importFile();">上传</a>
				<a class="mini-button" style="display: none"  id="download_search" name="download_search"    onclick="download();" >下载模板</a>
				<a  class="mini-button" style="display: none"  id="history_search" name="history_search"   onclick="search2();">查询历史记录</a>

			</span>
		<div id="OrderSortingManage" class="mini-fit">
	<div id="custNewsGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;"   allowAlternating="true"
        idField="dealNo" sortMode="client"  frozenStartColumn="0" frozenEndColumn="1">
        <div property="columns">
			<div type="indexcolumn" headerAlign="center" align="center" width="50px">序号</div>
	         <div field="dealNo" width="180" allowSort="true" headerAlign="center" align="center"  >交易单号</div>
			 <div field="cnm" width="100" allowSort="false" headerAlign="center" align="center">基础资产客户</div>
			 <div field='cno'       headerAlign= "center" allowSort= "true"     width='120' align="center">客户号</div> 
	         <div field="baseAssetType" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'baseAssetType'}">基础资产类别</div>
			 <div field='inBalance'      headerAlign= "center" allowSort= "true"     width='120' align = 'right' numberFormat="#,0.00">对应投资金额</div>                            
			 <div field="isCust" width="100" allowSort="false" headerAlign="center" align="left"  renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否本行客户</div>
			 <div field="moneyMarketType" width="100" allowSort="false" headerAlign="center" align="left"  renderer="CommonUtil.dictRenderer" data-options="{dict:'moneyMarketType'}">货币市场类型</div>
			 <div field="bigCatName" width="100" allowSort="false" headerAlign="center" align="left" >行业大类</div>
			 <div field="middleCatName" width="100" allowSort="false" headerAlign="center" align="left">行业中类</div>
			 <div field="smallCatName" width="100" allowSort="false" headerAlign="center" align="center" >行业小类</div>
			 <div field='innerLevel'     headerAlign= "center" allowSort= "true"     width='80'>行内评级</div>                        
			 <div field='outerLevel'     headerAlign= "center" allowSort= "true"     width='80'>公开评级</div> 
	         <div field="pledgeType" width="100" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'pledgeType'}">质押物类型</div>
	         <div field="pledgeAmt" width="100" allowSort="false" headerAlign="center" align='right' numberFormat="#,0.00">质押物金额</div>
	         <div field="pledgeVdate" width="100" allowSort="false" headerAlign="center" align="center">质押起始日</div>
	         <div field="pledgeMdate" width="100" allowSort="false" headerAlign="center" align="center">质押到期日</div>
	         <div field="pledgeRate" width="100" allowSort="false" headerAlign="center" numberFormat='p4' align="left">质押利率_%</div>
	         <div field="pledgeSpread" width="100" allowSort="false" headerAlign="center" numberFormat='p4' align="left">质押利率差_%</div>
	         <div field="mortgageType" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'icWay'}" >基础资产增信方式</div>
	         <div field="mortgageRemark" width="100" allowSort="false" headerAlign="center" >基础资产增信描述</div>
	         <div field="shareType" width="100" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'shoreholderType'}">股东性质</div>
	         <div field="guarantor" width="100" allowSort="false" headerAlign="center" >保证人名称</div>
	         <div field="guarantorIndus" width="100" allowSort="false" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'GBT4754_2011'}" >保证人所属行业</div>
	         <div field="secId" width="100" allowSort="false" headerAlign="center" align="left">债券编号</div>
	         <div field="optExerDate" width="100" allowSort="false" headerAlign="center" align="center">行权日期</div>
	         <div field="stockId" width="100" allowSort="false" headerAlign="center" align="center">股票代码</div>
	         <div field="warnLinePrice" width="100" allowSort="false" headerAlign="center" align="left">预警线</div>
	         <div field="coverLinePrice" width="100" allowSort="false" headerAlign="center" align="left">补仓线</div>
	         <div field="openLinePrice" width="100" allowSort="false" headerAlign="center" align="left">平仓线</div>
			 <div field='assetType'    headerAlign= "center" allowSort= "true" width='180'>风险基础资产类型</div>                
			 <div field='coefficient'  headerAlign= "center" allowSort= "true"       align='right' numberFormat="#,0.00">风险资产系数</div>                    
			 <div field='inBalance'    headerAlign= "center" allowSort= "true"       align='right' numberFormat="#,0.00">对应投资金额</div>                    
			 <div field='assetBalanceAuto' headerAlign= "center" allowSort= "true"   align='right' numberFormat="#,0.00">风险资产占用</div>      
			 <div field='rsrAmt' headerAlign= "center" allowSort= "true"   align='right' numberFormat="#,0.00">风险缓释额</div>
	         <div field="remark" width="100" allowSort="false" headerAlign="center" >备注</div>
	                   
        </div>   
	</div>
	</div>
</body>
	
<script  type="text/javascript">
	mini.parse();
	var grid=mini.get("custNewsGrid");
	var form=  new mini.Form("#search_form");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});


	$(document).ready(function(){
		search(10,0);
	});
	function clear(){
		form.clear();
	}

	function search(pageSize,pageIndex){
		
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息");
			return;
		}
		var data = form.getData();
		data['pageNumber'] = pageIndex+1;
		data['pageSize'] = pageSize;
		var param = mini.encode(data); //序列化成JSON
		CommonUtil.ajax({
			url:'/TdBaseAssetController/getBaseAssetList',
			data:param,
			callback:function(data){
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//历史查询记录
	function search2(){
		var grid = mini.get("custNewsGrid");
		var row = grid.getSelected();
		if(row){
			mini.open({
				url:CommonUtil.baseWebPath() +"/mini_Uassets/MiniTdBaseAssetManageHistory.jsp?dealNo="+row.dealNo,
				title: "原交易选择", 
				width: 900,
				height: 510,
				ondestroy : function (action) {
					if(action == "ok"){
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data);    //必须
					}	
				}	
			});
		}else{
			mini.alert("请先选择一条记录")
		}
	}
	//下载模板
	function download(){
		window.location.href = '<%=basePath%>/sl/QdReportsController/exportBassAsset';
		// var urls = CommonUtil.pPath + "/sl/QdReportsController/exportBassAsset";                                                                                                                           
		// $('<form action="'+ urls +'" method="post"></form>').appendTo('body').submit().remove();   
	}
	//上传
	function importFile(){
		mini.open({
			url: CommonUtil.baseWebPath() +"/mini_Uassets/MiniTdBaseAssetEdits.jsp",
			width: 420,
			height: 150,
			title: "文件上传",
			ondestroy: function (action) {
				search(grid.pageSize,grid.pageIndex);//重新加载页面
			}
		});
	} 
	</script>
</html>