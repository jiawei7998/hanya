<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>用户参数</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset id="fd2" class="mini-fieldset title">
    <legend><label>查询条件</label></legend>
    <div id="find" >
        <div id="search_form"  >
            <input id="p_id" name="p_id" class="mini-textbox" labelField="true" 
                label="参数编号："  emptyText="请输入参数编号" labelStyle="text-align:right;"/>
            <input id="p_name" name="p_name" class="mini-textbox" labelField="true" 
                label="参数名称："  emptyText="请输入参数名称" labelStyle="text-align:right;"/>
            <input id="p_value" name="p_value" class="mini-textbox" labelField="true" 
                label="参数值："  emptyText="请输入参数值" labelStyle="text-align:right;"/>
            <input id="p_sub_type" name="p_sub_type" class="mini-combobox" 
            emptyText="请选择参数小类" data="CommonUtil.serverData.dictionary.UserParameterType" 
            labelField="true"  label="参数小类："  labelStyle="text-align:right;"/>
            <input id="p_sort" name="p_sort" class="mini-textbox" labelField="true" 
                label="参数排序："  emptyText="请输入参数排序" labelStyle="text-align:right;"/>
            <input id="p_sub_name" name="p_sub_name" class="mini-textbox" labelStyle="text-align:right;"
            labelField="true"  label="参数小类描述："  emptyText="请输入参数小类描述"/>
            <input id="status" name="status" class="mini-combobox" emptyText="请选择状态" 
            data="CommonUtil.serverData.dictionary.status" labelStyle="text-align:right;"
            labelField="true"  label="状态："  />
            <span style="float:right;margin-right: 150px">
                <a id="search_btn" class="mini-button" style="display: none"   onclick="search1()" >查询</a>
                <a id="clear_btn" class="mini-button" style="display: none"    onClick="clearData">清空</a>
                
            </span>
        </div>
        
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
        <a id="edit_btn" class="mini-button" style="display: none"    onclick="edit">修改</a>
        <a id="delete_btn" class="mini-button" style="display: none"     onclick="removeRow">删除</a>
        <a id="submit_btn" class="mini-button" style="display: none"      onclick="submit()">提交复核</a>
    </span>
    
    <div id="tradeManage" class="mini-fit" style="margin-top: 2px;"> 
    <div id="UserParamManage" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
     idField="p_id"   multiSelect="false"  sortMode="client" allowAlternating="true"
     onRowdblclick="custDetail" onrowclick="rowClick">
        <div property="columns" >
            <div type="indexcolumn" width="40px" headerAlign="center" align="center">序号</div>
            <div field="p_id" width="80px" headerAlign="center" align="center" allowSort="true" dataType="int">参数编号</div>    
            <div field="status"   headerAlign="center"  width="80px"renderer="CommonUtil.dictRenderer" data-options="{dict:'status'}">状态</div>                            
            <div field="p_type"   headerAlign="center" width="80px" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ParameterBigKind'}">参数大类</div>
            <div field="p_sub_type"    headerAlign="center"   width="120px"align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'UserParameterType'}">参数小类</div>                                
            <div field="p_sub_name"  headerAlign="center"  width="180px">参数小类描述</div>
            <div field="p_name" headerAlign="center"  width="140px">参数名称</div>
            <div field="p_value" headerAlign="center"  width="100px">参数值</div>
            <div field="p_sort" headerAlign="center" allowSort="true"  width="60px" dataType="int">排序</div>
            <div field="p_code" headerAlign="center" align="center"  width="80px">参数Id</div>
            <div field="p_parent_code" headerAlign="center" align="center"  width="80px">参数父节点</div>
        </div>
    </div>
    </div>

    <script type="text/javascript">

        
        mini.parse();

		$(document).ready(function(){
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                var grid = mini.get("UserParamManage");
                grid.on("beforeload", function (e) {
                    e.cancel = true;
                    var pageIndex = e.data.pageIndex;
                    var pageSize = e.data.pageSize;
                    search(pageSize, pageIndex);
                });
                search(grid.pageSize, 0);
            });
		});

        function search(pageSize,pageIndex){
			var form =new mini.Form("find");
			form.validate();
			if (form.isValid() == false) return;//表单验证
			var data =form.getData();//获取表单数据
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url:"/UserParamController/pageTaUserParams/",
				data:param,
				callback:function(data){
						var grid =mini.get("UserParamManage");
						//设置分页
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						//设置数据
						grid.setData(data.obj.rows);
					}
				});
		}

        function search1(){
            var grid =mini.get("UserParamManage");
            search(grid.pageSize,0);
        }

         //行点击时发生 如果状态为"新增"或者"已复核"或者"退回"，则此条记录可修改，
         //如果状态为"经办待复核"，修改按钮禁
        function rowClick(e){
            var grid = e.sender;
            var row = grid.getSelected();
            if(row.p_type == "0"){//系统参数
                mini.get("edit_btn").setEnabled(false);
                mini.get("delete_btn").setEnabled(false);
                mini.get("submit_btn").setEnabled(false);
            }else{//用户参数
                mini.get("edit_btn").setEnabled(true);
                mini.get("delete_btn").setEnabled(true);
                mini.get("submit_btn").setEnabled(true);
                if(row.status == "1"){//状态为经办待复核
                    mini.get("edit_btn").setEnabled(false);
                    mini.get("delete_btn").setEnabled(false);
                    mini.get("submit_btn").setEnabled(false);
                }
                if(row.status == "2"){//已复核
                    mini.get("submit_btn").setEnabled(false);
                }

            }
        } 

        //清空
        function clearData(){
            //var grid =mini.get("UserParamManage");
            var form =new mini.Form("find");
            form.clear();
            //search(grid.pageSize,0);
        }

        //删除
        function removeRow() {
            var grid = mini.get("UserParamManage");
            //获取选中行
            var rows =grid.getSelected();
            
            //获取行索引号,行索引号从0开始
            var indexRow = grid.indexOf(rows);
            
            if (indexRow > -1) {
                
                if(rows.p_type == "0"){
				    mini.alert("系统参数不能删除","系统提示");
				    return false;
			    }
                
                var params = {"p_id":grid.data[indexRow]["p_id"],
                              "status":grid.data[indexRow]["status"],
                              "p_type":grid.data[indexRow]["p_type"],
                              "p_sub_type":grid.data[indexRow]["p_sub_type"],
                              "p_sub_name":grid.data[indexRow]["p_sub_name"],
                              "p_name":grid.data[indexRow]["p_name"],
                              "p_value":grid.data[indexRow]["p_value"],
                              "p_sort":grid.data[indexRow]["p_sort"],
                              "p_code":grid.data[indexRow]["p_code"],
                              "p_parent_code":grid.data[indexRow]["p_parent_code"]}

                mini.confirm("您确认要删除用户参数<span style='color:#FF3333;font-weight:bold;'>" + grid.data[indexRow]["p_name"] + "(" + grid.data[indexRow]["p_id"] + ")</span>吗?","系统警告",function(value){   
			        if (value == "ok"){   
			    	    CommonUtil.ajax({
						    url : '/UserParamController/delTaUserParam',
						    data : params,
						    callback : function(data){
                                if("error.common.0000" == data.code){
                                    mini.alert("删除成功！","系统提示");
                                    var grid = mini.get("UserParamManage");
                                    search(grid.pageSize,0);
                                }
						    }
					    });
					
				    }   
			    });       
                    
            }else{
                mini.alert("请选择要删除的数据！");
            }
        }

        //提交复核
        function submit(){
            var grid = mini.get("UserParamManage");
            var row = grid.getSelected();
            if(row){
                //只有参数状态为新增 和 退回 可以提交复核
                if(row.status != '0' && row.status != 'B'){
                    mini.alert("只有参数状态为'新增'和'退回'可以提交复核!","系统提示");
                    return;
                }
                CommonUtil.ajax({
                    url:"/UserParamController/submitVerify",
                    data:row,
                    callback:function(){
                        mini.alert("提交成功！");
                        search(grid.pageSize,0);
                    }
                });
            }else{
                mini.alert("请先选择一条记录！");
            }
        }

        

        

        top["userParamManage"]=window;
        function getData(){
            var grid = mini.get("UserParamManage");
            var record =grid.getSelected(); 
            return record;
        }

        //新增
        function add(){
            var url = CommonUtil.baseWebPath() + "/mini_system/MiniUserParamEdit.jsp?action=new";
            var tab = { id: "userParamManageAdd", name: "userParamManageAdd", text: "用户参数新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }
        //修改
        function edit(){
            var grid = mini.get("UserParamManage");
            var  record =grid.getSelected();
            if(record){
                var url = CommonUtil.baseWebPath() + "/mini_system/MiniUserParamEdit.jsp?action=edit";
                var tab = { id: "userParamManageEdit", name: "userParamManageEdit", text: "用户参数修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

        //双击一行打开详情信息
        function custDetail(e){
            var grid = e.sender;
            var row = grid.getSelected();
            if(row){
                var url = CommonUtil.baseWebPath() + "/mini_system/MiniUserParamEdit.jsp?action=detail";
                var tab = { id: "userParamManageDetail", name: "userParamManageDetail", text: "用户参数详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }
        }
    </script>
</body>
</html>