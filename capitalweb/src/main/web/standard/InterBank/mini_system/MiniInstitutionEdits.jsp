<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title></title>
		</head>
		<body>
			<div id="form1">
				<span id="labell"><a href="javascript:CommonUtil.activeTab();">机构信息</a> >> 修改</span>
					<div id="MiniInstitutionAddEdits" class="fieldset-body" >   
					<table class="mini-table" id="accout_form"  width="100%">
					<tr>
						<td>
							<input id="instId" name="instId" requiredErrorText="该输入项为必输项" class="mini-textbox" required="true"  vtype="rangeLength:1,16"
							 labelField="true" label="机构代码："  emptyText="请输入机构代码" />
						</td>
						<td>
							<input id="instFullname" name="instFullname" class="mini-textbox" requiredErrorText="该输入项为必输项" required="true"  vtype="rangeLength:1,125"
							 labelField="true" label="机构全称："  emptyText="请输入机构全称" />
						</td>
						<td>
							<input id="instName" name="instName" class="mini-textbox" requiredErrorText="该输入项为必输项" required="true"  vtype="rangeLength:1,32"
							 labelField="true" label="机构简称：" emptyText="请输入机构简称" />
						</td>
					</tr>
					<tr>
						<td>											
							<input id="pinstId" name="pinstId" class="mini-buttonedit searchbox" onbuttonclick="onInsQuery"  labelField="true" label="上级机构："
							 emptyText="请选择上级机构..." allowinput='false'
							 />
						</td>
						<td>
							<input id="instType" name="instType" class="mini-ComboBox" vtype="" requiredErrorText="该输入项为必输项" data="CommonUtil.serverData.dictionary.InstitutionType"
							 required="true"  emptyText="请选择机构类型..." labelField="true" label="机构类型：" />
						</td>
						<td>
							<input id="instStatus" name="instStatus" class="mini-ComboBox" vtype="" requiredErrorText="该输入项为必输项" data="CommonUtil.serverData.dictionary.AccStatus"
							 required="true"  emptyText="请选择机构状态..." labelField="true" label="机构状态："  emptyText="请选择机构状态..."
							/>
						</td>
					</tr>
					<tr>
						<td>
							<input id="onlineDate" name="onlineDate" class="mini-datepicker" requiredErrorText="该输入项为必输项"  labelField="true"
							 label="上线日期："  emptyText="请选择上线日期..." />
						</td>
						<td>
							<input id="instLrInstCode" name="instLrInstCode" class="mini-textbox"  requiredErrorText="该输入项为必输项" 
							 vtype="rangeLength:1,32" labelField="true" label="机构代码证："  emptyText="请输入机构代码证" />
						</td>
						<td class="">
							<input id="bInstId" name="bInstId" class="mini-textbox" labelField="true" vtype="rangeLength:1,16" label="记账机构号：" emptyText="请输入记账机构号"
							/>
						</td>
					</tr>
					<tr>
						<td>
							<input id="instSimpleName" name="instSimpleName" class="mini-textbox" labelField="true" vtype="rangeLength:1,100" label="简名：" 
							 emptyText="请输入简名" />
						</td>
						<td>
							<input id="linkMan" name="linkMan" class="mini-textbox" labelField="true" vtype="rangeLength:1,15" label="联系人："  emptyText="请输入联系人"
							/>
						</td>
						<td>
							<input id="telephone" name="telephone" class="mini-textbox" labelField="true" label="手机："  emptyText="请输入手机号码"
							vtype="maxLength:50;" onvalidation="CommonUtil.onValidation(e,'mobile',[null])"/>
						</td>
					</tr>
					<tr>
						<td>
							<input id="isFta" name="isFta" class="mini-ComboBox" data="CommonUtil.serverData.dictionary.YesNo" labelField="true" label="是否自贸区机构："
							 emptyText="请选择是否自贸区机构..." />
						</td>
						<td>
							<input id="businessUnit" name="businessUnit" class="mini-textbox"  vtype="rangeLength:1,2" labelField="true"
							 requiredErrorText="该输入项为必输项" label="业务单元：" emptyText="请输入业务单元" />
						</td>
						<td>
							<input id="printInstId" name="printInstId" class="mini-textbox" labelField="true" label="打票机构：" vtype="rangeLength:1,16" emptyText="请输入打票机构"
							/>
							<input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
						</td>
					</tr>
					<tr>
						<td colspan="3" style="text-align:center;">
							<a id="save_btn" class="mini-button" style="display: none"    onclick="save()">保存</a>
							<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close">取消</a>
						</td>
					</tr>
				</table>
			</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				var url = window.location.search;
				var action = CommonUtil.getParam(url, "action");
				var param = top["InstitutionManger"].getData(action);
				var form = new mini.Form("#accout_form");
				var instIdValue =mini.get("instId");
				//验证机构代码
				
				var saveUrl = "";
					if (action == "add") {
						saveUrl = "/InstitutionController/addInstitution";
					} else if (action == "edit") {
						saveUrl = "/InstitutionController/updateInstitution";
					}
				function save() {
					form.validate();
					if (form.isValid() == false)
					// mini.alert("信息填写有误，请重新填写!", "消息提示")
						return;
					//提交数据
					var data = form.getData(true); //获取表单多个控件的数据  
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: saveUrl, 
						data: param,
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功",'提示信息',function(){
									setTimeout(function(){top["win"].closeMenuTab()},10);
								});
							} else {
								mini.alert("保存成功");
							}
						}
					});
				}
				function close() {
							top["win"].closeMenuTab();
						}
				function initform() {
						if (action == "add") {
							$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>机构信息</a>>>新增");
						}

						else if (action == "edit") {
							form.setData(param);
							mini.get("instId").setEnabled(false);
							mini.get("pinstId").setText(param.pInstId);
							mini.get("pinstId").setValue(param.pInstId);
						} else if (action == "detail") {
							$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>机构信息</a>>>详情");
							form.setData(param);
							mini.get("pinstId").setValue(param.pInstId);
							mini.get("pinstId").setText(param.pInstId);
							form.setEnabled(false);
							mini.get("save_btn").setEnabled(false);
							mini.get("cancel_btn").setEnabled(false);
						}

				
					}
			
				// 查询机构的方法
				function onInsQuery(e) {
					var btnEdit = this;
					mini.open({
						url: CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionSelectManages.jsp",
						title: "机构选择",
						width: 700,
						height: 600,
						ondestroy: function (action) {
							if (action == "ok") {
								var iframe = this.getIFrameEl();
								var data = iframe.contentWindow.GetData();
								data = mini.clone(data); //必须
								if (data) {
									btnEdit.setValue(data.instId);
									btnEdit.setText(data.instName);
									btnEdit.focus();
								}
							}

						}
					});
				}

				$(document).ready(function () {
					initform();
					instIdValue.on("validation", function (e) {
						if (e.isValid) {
							if (EnglistAndNumberValidation(e.value) == false) {
								e.errorText = "请输入字母或者数字"; e.isValid = false;
							}

						}
					});
				})
				//验证只能输入字母数字
				function EnglistAndNumberValidation(userId) {
						var reg = new RegExp("^[0-9a-zA_Z]+$");
						if (!reg.test(userId)) {
							return false;
						}
						return true;
					}
			</script>
		</body>
		</html>