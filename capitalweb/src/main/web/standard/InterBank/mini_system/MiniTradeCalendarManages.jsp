<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<link href="<%=basePath%>/miniScript/calendar/calendar.css" rel="stylesheet"></link>
		<script src="<%=basePath%>/miniScript/calendar/calendarChange.js"></script>
		<script src="<%=basePath%>/miniScript/calendar/calendar.js"></script>

		<div class="mini-splitter" style="width:100%;height:100%">
			<div showCollapseButton="true" size="50%">
				<div id="StockPriceManage" class="mini-fit">
					<fieldset class="mini-fieldset">
						<legend>查询</legend>
						<div class="mini-toolbar" id="ser" style="border-bottom: 0; padding: 0px;">
							<table id="search_form" class="form_table" width="100%" cols="6">
								<tr>
									<td>
										<input id="year" name="year" class="mini-combobox" labelField="true" width="70%" labelStyle="width:60px;" label="年份选择"/>
									</td>
									<td>
										<input id="month" name="month" class="mini-combobox" labelField="true" width="70%" labelStyle="width:60px;" label="月份选择"/>
									</td>
								</tr>
								<tr>
									<td>
										<input id="startTimeSearch" name="startTimeSearch" value="<%=__bizDate%>" class="mini-datepicker" labelField="true"
										width="70%" labelStyle="width:60px;" label="开始日期" />
									</td>
									<td>
										<input id="endTimeSearch" name="endTimeSearch" value="<%=__bizDate%>" class="mini-datepicker" labelField="true"
										width="70%" labelStyle="width:60px;" label="结束日期" />
									</td>
								</tr>
							</table>
							<div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
								<a class="mini-button" style="display: none"  id="search_btn" name="search_btn"  onclick="query">查询</a>
								<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
								<!-- <a class="mini-button" style="display: none"  id="add_btn" name="add_btn"  onclick="add">新增</a>
									<a class="mini-button" style="display: none"  id="update_btn" name="update_btn"  onclick="update">修改</a>
									<a class="mini-button" style="display: none"  id="delete_btn" name="delete_btn"  onclick="delete">删除</a> -->
							</div>
						</div>
					</fieldset>
					<div id="TradeCalendarManage" class="mini-fit" style="margin-top: 2px;">
						<div id="calGrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" sizeList="[20,30,50,100]" pageSize="20">
							<div property="columns">
								<div type="indexcolumn"></div>
								<div field="calDate" width="100" allowSort="false">日期</div>
								<div field="calFlag" width="100" allowSort="false" renderer="onGenderRenderer">工作日</div>
								<div field="calCode" width="100" allowSort="false">日历代码</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div showCollapseButton="true" size="50%">
				<div class="mini-fit">
					<div id="calendar"></div>
					<div style="padding:10px 0 0 15px">
						<a class="mini-button" style="display: none"  id="setHolDay" name="setHolDay"  onclick="setHolidays">设置节假日</a>
						<a class="mini-button" style="display: none"  id="setWorkDay" name="setWorkDay"  onclick="setWorkdays">取消节假日</a>
						<a class="mini-button" style="display: none"  id="setWeekend" name="setWeekend"  onclick="createWeekendHolidays">设置周末为节假日</a>
					</div>
				</div>
			</div>
		</div>




	<script type="text/javascript">
		mini.parse();

		var currentDate = null;

		var bizDate ='<%=__bizDate%>';

		var grid = mini.get("calGrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		
		function search(pageSize,pageIndex) {
			mini.parse();
			var form = new mini.Form("ser");
			form.validate();
			if (form.isValid == false)
				return;
			//提交数据
			var data = form.getData(true);//获取表单多个控件的数据  
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url: '/CalendarsController/pageCalendarDate',
				data: param,
				callback: function (data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);

				}
			});
		}
		
		function query(){
			search(grid.pageSize,0)
		}
		
		function clear() {
			var form = new mini.Form("#search_form");
			form.clear();
		}

		///////////////////////////////////////////////////////
		var Genders = [{ id: 0, text: '否' }, { id: 1, text: '是' }];
		function onGenderRenderer(e) {
			for (var i = 0, l = Genders.length; i < l; i++) {
				var g = Genders[i];
				if (g.id == e.value) return g.text;
			}
			return " ";
		}

		function getHoliDay(newDate)
		{
			currentDate = newDate;
			if(calendar && calendar.holiDays && calendar.holiDays.date && calendar.holiDays.date.length > 0)
			{
				return 	calendar.holiDays.date;
			}
			var year  = newDate.getFullYear();
			var month = newDate.getMonth() + 1;
			var day = newDate.getDate();
			var date = year + '-' + (month < 10 ? "0" + month : month)  + '-' + (day < 10 ? "0" + day : day);
			var holiDays = [];
			CommonUtil.ajax({
				url: "/CalendarsController/getTtMktCalendarByMonth",
				data : {'bizDate':date},
				callback: function (data) {
					var date_1 = null;
					for (var index = 0; index < data.obj.length; index++) {
						date_1 = parseDate(data.obj[index].calDate);
						holiDays.push(date_1.getTime());
					}
				}
			});
			return holiDays;
		}

		//yyyy-MM-dd
		function parseDate(dateStr){
			var year = dateStr.substring(0,4); // 2018-01-12
			var month = parseInt(dateStr.substring(5,7)) - 1;
			var day = dateStr.substring(8,10);
			return new Date(year, month, day);
		}

		$("#calendar").calendar({
			newDate: parseDate(bizDate),
			getHoliDay: getHoliDay
		});

		var calendar = $("#calendar").data("calendar");
		
		function setWorkdays() {
			if(calendar.focusDays.length == 0){
				mini.alert("请选择日期");
				return;
			}

			var focusDays = {
				date: calendar.focusDays
			}
			CommonUtil.ajax({
				url: "/CalendarsController/deleteTtMktCalendarDates",
				data: mini.encode(focusDays.date),
				callback: function (data) {
					if (data.code == 'error.common.0000') {
						calendar.setUnHolidays(focusDays);
						mini.alert("设置成功",'提示信息')
						search(grid.pageSize,grid.pageIndex);
					}
				}
			});
		}
		
		function setHolidays() {
			var holiDays = {
				date: calendar.focusDays
			}
			if(holiDays.date.length == 0){
				mini.alert("请选择日期");
			}else{
				CommonUtil.ajax({
					url: "/CalendarsController/insertTtMktCalendarDates",
					data: mini.encode(holiDays.date),
					callback: function (data) {
						if (data.code == 'error.common.0000') {
							calendar.setHolidays(holiDays);
							mini.alert("设置成功",'提示信息')
							search(grid.pageSize,grid.pageIndex);
						}
					}
				});
			}
			
		}

		//设置周末为节假日
		function createWeekendHolidays(){
			var year  =  currentDate.getFullYear();
			var month =  currentDate.getMonth() + 1;

			var _newDate = new Date(year, month - 1, 1);
			_newDate.setDate(1);
			_newDate.setDate(_newDate.getDate() - 1);
			var holiDays = [];
			//当前月天数 不要要获取
			var monthCount = getMonthDayCount(year,month);
			var holiDay = null;
			for (var i = 0; i < monthCount; i++) {
				_newDate.setDate(_newDate.getDate() + 1);

				if (_newDate.getDay() == 0 || _newDate.getDay() == 6) 
				{
					holiDay = year + '-' + (month < 10 ? '0' + month : month) + '-' 
						+ (_newDate.getDate() < 10 ? '0' + _newDate.getDate() : _newDate.getDate());
					holiDays.push(holiDay);
				}
			}

			CommonUtil.ajax({
				url: "/CalendarsController/insertTtMktCalendarDates",
				data: mini.encode(holiDays),
				callback: function (data) {
					if (data.code == 'error.common.0000') {
						calendar.setHolidays({date: holiDays});
						mini.alert("设置成功",'提示信息')
						search(grid.pageSize,grid.pageIndex);
					}
				}
			});
		}

		function getMonthDayCount(year, month){
			var d = new Date(year, month, 0);
			return d.getDate();
		}

		mini.get("year").on("valuechanged",function(){
			setSearchConditions();
			query();
		});

		mini.get("month").on("valuechanged",function(){
			setSearchConditions();
			query();
		});

		function setSearchConditions()
		{
			if(CommonUtil.isNotNull(mini.get("year").getValue()) && CommonUtil.isNotNull(mini.get("month").getValue())){
				var year = parseInt(mini.get("year").getValue());
				var month = parseInt(mini.get("month").getValue());
				var monthDayCount = getMonthDayCount(year,month);
				mini.get("startTimeSearch").setValue(year + '-' + month + '-01');
				mini.get("endTimeSearch").setValue(year + '-' + month + '-' +(monthDayCount < 10 ? '0' + monthDayCount : monthDayCount));
				
			}else if(CommonUtil.isNotNull(mini.get("year").getValue()) && CommonUtil.isNull(mini.get("month").getValue())){
				var year = parseInt(mini.get("year").getValue());
				mini.get("startTimeSearch").setValue(year + '-01-01');
				mini.get("endTimeSearch").setValue(year + '-12-31');
			}
			
		}

		$(document).ready(function () {
			var year = parseInt(bizDate.substring(0,4));
			var month = parseInt(bizDate.substring(5,7));
			var monthDayCount = getMonthDayCount(year,month);

			var years = [];
			var year_1 = parseInt(bizDate.substring(0,4)) - 5; // 2018-01-12
			for(var i = 0;i < 10; i++){
				years.push({id : year_1,text : year_1 + '年'});
				year_1++;
			}
			mini.get("year").setData(years);
			mini.get("year").setValue(year);

			var months = [
				{id:'1',text:'1月'},
				{id:'2',text:'2月'},
				{id:'3',text:'3月'},
				{id:'4',text:'4月'},
				{id:'5',text:'5月'},
				{id:'6',text:'6月'},
				{id:'7',text:'7月'},
				{id:'8',text:'8月'},
				{id:'9',text:'9月'},
				{id:'10',text:'10月'},
				{id:'11',text:'11月'},
				{id:'12',text:'12月'}
			];
			mini.get("month").setData(months);
			mini.get("month").setValue(month);

			
			mini.get("startTimeSearch").setValue(year + '-' + month + '-01');
			mini.get("endTimeSearch").setValue(year + '-' + month + '-' +(monthDayCount < 10 ? '0' + monthDayCount : monthDayCount));

			search(grid.pageSize,0);
		});

	</script>