<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 	
    <title>估值数据管理</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"  >
				<input id="prodCode" name="prodCode" class="mini-textbox"  labelField="true" 
					label="资产代码：" emptyText="请输入资产代码" labelStyle="text-align:right;"/>

				<input id="valDate" name="valDate" value="<%=__bizDate%>" class="mini-datepicker" labelField="true"
					 label="估值日期"  labelStyle="width:60px;"/>
						
				<span style="float:right;margin-right: 150px">
					<a id="search_btn" class="mini-button" style="display: none"    onclick="search(10,0)">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
				</span>	
			</div>
        </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a id="upload_btn" class="mini-button" style="display: none"      onclick="importFile()">估值数据导入</a>
		<!--
		<a id="val_btn" class="mini-button" style="display: none"      onclick="createVal()">创建估值账务</a>
		-->
    </span>

    <div id="TdTrdValueAssessment" class="mini-fit" >  
		<div id="TdTrdValueAssessmentGrid" class="mini-datagrid borderAll" style="width:100%;height:100%"  allowAlternating="true"
		idField="stockId" sortMode="client" >
			<div property="columns">
			<div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
			<div field="dealNo" width="100" allowSort="false"  headerAlign="center" align="center" >交易单号</div>
			<div field="prodCode" width="100" allowSort="false"  headerAlign="center" align="left">资产代码</div>
			<div field="val" width="100" allowSort="false" numberFormat="#,0.00" align="right" headerAlign="center" >估值</div>
			<div field="valDate" width="100" allowSort="false" align="center" headerAlign="center"  align="center" >录入时间</div>
        </div>   
	 </div>
	 </div>
    

<script>
    mini.parse();
	top['TdTrdValueAssessmentGrid']=window;
	var grid =mini.get("TdTrdValueAssessmentGrid");
	var form=  new mini.Form("search_form");
	//分页
	var grid =mini.get("TdTrdValueAssessmentGrid");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function query(){
		search(grid.pageSize, 0);
	}

	$(document).ready(function(){
		search(10,0);
	}); 

	//清空
	function clear(){
		form.clear();
	}
	//查询
	function search(pageSize,pageIndex){
		form.validate();
		if (form.isValid == false) return;
		//提交数据
		var data=form.getData(true);//获取表单多个控件的数据  
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var param = mini.encode(data); //序列化成JSON
		//ajax请求
		CommonUtil.ajax({
			url:'/TdTrdValueAssessmentController/pageList',
			data:param,
			callback:function(data){
				//设置分页
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				//设置数据
				grid.setData(data.obj.rows);
			}
		});
	}

	//导入文件
	function importFile(){
		mini.open({
			url: CommonUtil.baseWebPath() +"/mini_system/MiniTdTrdValueAssessmentEdit.jsp",
			width: 420,
			height: 150,
			title: "文件上传",
			ondestroy: function (action) {
				search(10,0);//重新加载页面
			}
		});
	}

	function createVal(){
		var data=form.getData(true);//获取表单多个控件的数据 
		if(grid.getSelected()){
			data["dealNo"] = grid.getSelected()["dealNo"];
		}
		var param = mini.encode(data); //序列化成JSON
		//ajax请求
		CommonUtil.ajax({
			url:'/TdTrdValueAssessmentController/createVal',
			data:param,
			callback:function(data){
				mini.alert("成功");
			}
		});
	}
</script>
</body>
</html>