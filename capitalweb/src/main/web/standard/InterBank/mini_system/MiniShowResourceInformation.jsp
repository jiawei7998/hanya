<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<%
String branchId =__sessionUser.getBranchId();
%>
<script>
    //点击时获取的那条记录
    var selectRecord = {};
    var RoleInfo = { NullFunList: {} };
    $(document).ready(function () {
        // mini.parse();
        // var grid =mini.get("RoleManageDatagrid");
        // grid.on("beforeload", function (e) {
        //     e.cancel = true;
        //     var pageIndex = e.data.pageIndex; 
        //     var pageSize = e.data.pageSize;
        //     search(pageSize,pageIndex);
        // });
        // search(10,0);
        loadResourceTree();
    })
    var user = {};

    //加载机构
    function SetDataObj(data) {

        mini.parse();
        var data = mini.clone(data);
        var inst = {};
        user.userId = data.userId;
        inst.userId = data.userId;
        inst.instsId = data.instsId;

        CommonUtil.ajax({
            url: "/InstitutionController/getTtInstitutionByUserId",
            data: mini.encode(inst),
            callback: function (data) {
                var grid = mini.get("Insts");
                // //console.log(data.obj);
                grid.setData(data.obj);

            }
        });

    }


    // function SetDataObj(data) {
    //         mini.parse();
    //         var data = mini.clone(data);
    //         //console.log(data);
    //         CommonUtil.ajax({
    //             url: "/RoleController/getRoleByUserIdAndInstId",
    //             data: mini.encode(data.param),
    //             callback: function (data) {
    //                 var grid = mini.get("RoleManageDatagrid");
    //                 grid.setData(data.obj.rows);
    //             }
    //         });

    //     }

    //===修改===
    function edit() {
        var row = mini.get("RoleManageDatagrid").getSelected();
        if (row) {
            mini.open({
                url: CommonUtil.baseWebPath() +"/mini_system/MiniRoleUpdate.jsp",
                title: "修改角色", width: 600, height: 400,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    var data = { action: "edit", record: row };
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                }
            });
        } else {
            alert("请选中一条记录");
        }
    }
    //===保存资源树信息===
    function save() {
        mini.parse();
        var saveObject = {};
        if (selectRecord.record.roleId) {
            saveObject.roleId = selectRecord.record.roleId;
        }
        //读取到角色信息 赋值给新的对象
        var list = new Array();
        var ResourceTree = mini.get("ResourceTree");//获取树的对象
        //创建一个数组
        var arr = new Array();
        var str = ResourceTree.getValue();
        arr = str.split(",");
        for (var i = 0; i < arr.length; i++) {
            var module = {};
            module.module_id = arr[i];
            if (RoleInfo.NullFunList[arr[i]]) {
                module.fun_list = RoleInfo.NullFunList[arr[i]];
            } else {
                module.fun_list = new Array();
            }
            list.push(module);
        }
        saveObject.module_list = list
        var param = mini.encode(saveObject);
        CommonUtil.ajax({
            url: "/RoleController/saveModuleAndFunction",
            data: param,
            callback: function (data) {
                if (data.code == 'error.common.0000') {
                    alert("操作成功");
                } else {
                    alert("操作失败")
                }
            }
        });
    }
    //===默认加载资源树===
    function loadResourceTree() {
        mini.parse();
        var data = {};
        var param = mini.encode(data);
        CommonUtil.ajax({
            url: "/RoleController/searchAllModule",
            data: param,
            callback: function (data) {
                var grid = mini.get("ResourceTree");
                grid.setData(data.obj);

            }
        });
    }
    //===点击角色列表加载需要选择的树===
    function onSelectionChanged(e) {
        //清空对象
        for (var key in selectRecord) {
            delete selectRecord[key];
        }
        var grid = e.sender;
        var record = grid.getSelected();
        selectRecord.record = record;
        var ResourceTree = mini.get("ResourceTree");
        ResourceTree.setValue();//每次访问时清空树中数据
        if (record) {
            //查询需要渲染的数据  RoleController/searchModuleByRoleId
            var data = {};
            data.roleId = record.roleId;
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/RoleController/searchModuleByRoleId",
                data: param,
                callback: function (data) {
                    var list = new Array();
                    $.each(data, function (i, item) {
                        RoleInfo.NullFunList[item.moduleId] = item.list;
                        list.push(item.moduleId);
                    });
                    ResourceTree.setValue(list);
                }
            });
        }
    }
    //===加上这句话；可以渲染节点后再次操作树(不可少)====
    function onBeforeNodeCheck(e) {
        var tree = e.sender;
        var node = e.node;
        if (tree.hasChildren(node)) {
        }
    }
    //机构字典
    var InstitutionType = [{ "id": "2", "text": "支行" }, { "id": "1", "text": "分行" }, { "id": "0", "text": "总行" }, { "id": "9", "text": "部门" }];
    //渲染机构字段
    function onInstitutionTypeRenderer(e) {
        for (var i = 0, l = InstitutionType.length; i < l; i++) {
            var g = InstitutionType[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }
    //启用状态字典
    var YesOrNo = [{ "id": "1", "text": "是" }, { "id": "0", "text": "否" }];
    //渲染启用状态
    function onYesOrNoRenderer(e) {
        for (var i = 0, l = YesOrNo.length; i < l; i++) {
            var g = YesOrNo[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }
    //角色级别字典
    var roleLevel = [{ "id": "2", "text": "负责人" }, { "id": "1", "text": "管理员" }];
    //渲染级别字典
    function onroleLevelRenderer(e) {
        for (var i = 0, l = roleLevel.length; i < l; i++) {
            var g = roleLevel[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }

    function searchRoles() {
        mini.parse();
        var form = new mini.Form("RoleSearch");
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        var data = form.getData();
        data.userId = user.userId;
        var param = mini.encode(data);
        ////console.log(param);
        CommonUtil.ajax({
            url: "/RoleController/getRoleByUserIdAndInstIds",
            data: param,
            callback: function (data) {
                var grid = mini.get("RoleManageDatagrid");

                // grid.setTotalCount(data.obj.total);
                // grid.setPageIndex(pageIndex);
                // grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });

    }
    //搜索角色列表
    function search(pageSize, pageIndex) {
        mini.parse();
        // var form = new mini.Form("RoleSearch");
        // form.validate();
        // var data = form.getData();
        // var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/RoleController/pageRole",
            data: mini.encode({ "pageSize": pageSize, "pageNumber": pageIndex + 1 }),
            callback: function (data) {
                var grid = mini.get("RoleManageDatagrid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
</script>


<body style="width:100%;height:100%;background:white">

    <div class="mini-panel" title="角色列表" style="width:100%;height:300px;"  showToolbar="true" showCloseButton="false"
        showFooter="true">
        <div id="RoleSearch">
            <input id="Insts" name="instId" class="mini-combobox" style="width:300px;" textField="instFullname" valueField="instId" emptyText="请选择..."
                value="" required="true"  allowInput="true" showNullItem="true" nullItemText="请选择..." />
            <a class="mini-button" style="display: none"  onclick="searchRoles()">查询</a>
        </div>
        <!-- <div>
            <div class="mini-toolbar">
                <table>
                    <tr>
                        <td>
                             <a class="mini-button" style="display: none"   onclick="add()">增加</a>
                            <a class="mini-button" style="display: none"   onclick="edit()">修改</a>
                            <a class="mini-button" style="display: none"   onclick="remove()">删除</a>
                            <a class="mini-button" style="display: none"  onclick="copy()">复制</a>
                            <a class="mini-button" style="display: none"   onclick="save()">保存</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div> -->
        <div id="RoleManageDatagrid" class="mini-datagrid borderAll" style="width:100%;height:280px;" allowResize="true" onselectionchanged="onSelectionChanged"
            idField="id" pageSize="10" multiSelect="true">
            <div property="columns">
                <div type="checkcolumn"></div>
                <div field="roleName" headerAlign="center" allowSort="true">角色名称</div>
                <div field="instType" headerAlign="center" allowSort="true" renderer="onInstitutionTypeRenderer">所属机构类型</div>
                <div field="isActive" headerAlign="center" allowSort="true" renderer="onYesOrNoRenderer">启用状态</div>
                <div field="roleLevel" headerAlign="center" allowSort="true" renderer="onroleLevelRenderer">角色级别</div>
                <div field="indexPage" headerAlign="center" allowSort="true">首页类型</div>
                <div field="instId" headerAlign="center" allowSort="true" visible="false">机构ID</div>
            </div>
        </div>
    </div>
    <div class="mini-panel" title="资源信息" style="width:100%;height:280px;"  showToolbar="true" showCloseButton="false"
        showFooter="false">
        <div>
            <!-- checkRecursive="false" 这个玩意儿是个坑 -->
            <ul id="ResourceTree" class="mini-tree" showTreeIcon="true" textField="text" idField="id" parentField="pid" resultAsTree="false"
                showCheckBox="true" checkRecursive="false" autoCheckParent="false" onbeforenodecheck="onBeforeNodeCheck" allowSelect="true"
                enableHotTrack="false">
            </ul>
        </div>
    </div>
</body>