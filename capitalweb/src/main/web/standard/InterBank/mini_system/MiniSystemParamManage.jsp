﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<title>系统参数</title>   
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 	
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset title">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"  >
					<input id="p_code" name="p_code" class="mini-textbox"  labelField="true" 
						 label="参数代码：" emptyText="请输入参数代码" labelStyle="text-align:right;"/>
					<input id="p_name" name="p_name" class="mini-textbox" labelField="true" 
						 label="参数名称：" emptyText="请输入参数名称" labelStyle="text-align:right;"/>
					<input id="p_value" name="p_value" class="mini-textbox" labelField="true"  
						label="参数值：" emptyText="请输入参数值"labelStyle="text-align:right;" />
					<input id="p_editabled" name="p_editabled" class="mini-combobox"  
						labelField="true"  label="可修改：" emptyText="请选择是否修改"  
						data="CommonUtil.serverData.dictionary.YesNo" labelStyle="text-align:right;"/>
					<input id="p_type" name="p_type" class="mini-textbox" labelField="true"  
						label="参数类型：" emptyText="请输入参数类型" labelStyle="text-align:right;"/>
					<span style="float:right;margin-right: 150px">
						<a id="search_btn" class="mini-button" style="display: none"    onclick="search1()">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
					</span>	
			</div>
		</div>
		
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="edit_btn" class="mini-button" style="display: none"     onclick="modifyedit()">修改</a>
	</span>
	<div id="tradeManage" class="mini-fit" style="margin-top: 2px;">  
		<div id="SystemParamManage" class="mini-datagrid borderAll" style="width:100%;height:100%;"  
		idField="p_code" allowResize="true" pageSize="10" onRowdblclick="custDetail" sortMode="client"
		allowAlternating="true" onrowclick="rowClick">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="25px" >序号</div>
				<div field="p_code"   headerAlign="center" align="center" width='80px' allowSort="true" dataType="int">参数代码</div>    
				<div field="p_name"  headerAlign="center" width="180px">参数名称</div>                            
				<div field="p_value"  headerAlign="center" align="center" width='70px'>参数值</div>
				<div field="p_type" headerAlign="center" align="center" width='80px' allowSort="true" dataType="int">参数类型</div>                                
				<div field="p_type_name" headerAlign="center" width='70px'>参数类型名称</div>
				<div field="p_editabled"  width='40px'  align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}" >可修改</div>
				<div field="p_prop1" headerAlign="center" align="center" width='70px'>参数扩展属性1</div>
				<div field="p_prop2" headerAlign="center" align="center" width='70px'>参数扩展属性2</div>
				<div field="p_memo"  headerAlign="center">备注</div>                
			</div>
		</div>
	</div>
	


<script type="text/javascript">

		mini.parse();

		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var grid = mini.get("SystemParamManage");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				search(grid.pageSize, 0);
			});
		});

		function search(pageSize,pageIndex){
			var form =new mini.Form("find");
			form.validate();
			if (form.isValid() == false) return;//表单验证
			var data =form.getData();//获取表单数据
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url:"/SysParamController/searchSysParam",
				data:param,
				callback:function(data){
						var grid =mini.get("SystemParamManage");
						//设置分页
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						//设置数据
						grid.setData(data.obj.rows);
					}
				});
		}
 	
 	
	top["systemParamManage"]=window;
    function getData(){
		var grid =mini.get("SystemParamManage");
        var record =grid.getSelected(); 
        return record;
    }


    //修改
    function modifyedit(){
		var grid =mini.get("SystemParamManage");
        var  record =grid.getSelected();
        if(record){
            var url = CommonUtil.baseWebPath() + "/mini_system/MiniSysParamEdit.jsp?action=edit";
            var tab = { id: "systemParamManageEdit", name: "systemParamManageEdit", text: "系统参数修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }else{
            mini.alert("请选中一条记录!","系统提示"); 
        }
    }

	//行点击时发生 如果可修改为否，则此条记录不可修改，修改按钮禁用
	function rowClick(e){
		var grid = e.sender;
		var row = grid.getSelected();
		if(row.p_editabled == "0"){
			mini.get("edit_btn").setEnabled(false);
			
		}else{
			mini.get("edit_btn").setEnabled(true);
		}
	}

	 //双击一行打开详情信息
	 function custDetail(e){
		var grid = e.sender;
		var row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() + "/mini_system/MiniSysParamEdit.jsp?action=detail";
			var tab = { id: "systemParamManageDetail", name: "systemParamManageDetail", text: "系统参数详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
			top['win'].showTab(tab);
		}
    }
	
       
	//清空
    function clear(){
		var grid =mini.get("SystemParamManage");
		var param = new mini.Form("search_form");
		param.clear();
		search(grid.pageSize,0);
	} 

	function search1(){
		var grid =mini.get("SystemParamManage");
		search(grid.pageSize,0);	
	} 
	

 </script> 
 </body>
 </html>      