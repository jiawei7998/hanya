<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%> 
<html>
<head>
	<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js"  type="text/javascript"></script>

</head>
<body >
			<span id="labell"><a href="javascript:CommonUtil.activeTab();">数据字典维护</a> >> 修改</span>
	 	<div  id="editForm"  class="fieldset-body">
	 		<table id="edit_form"  width="100%" class="mini-table">
	 		 	<tr>
		            <td>
		            <input id="dict_id" name="dict_id"  class="mini-textbox" label="代码编号：" labelField="true" vtype="maxLength:16" required='true'  emptyText="请输入代码编号"  />
		            </td>
		            <td>
		             <input id="dict_name" name="dict_name" class="mini-textbox"  labelField="true" label="中文名称：" vtype="maxLength:16" required='true' emptyText="请输入中文名称" />
		            </td>
					<td>
						<input id="dict_module" name="dict_module" class="mini-textbox"   labelField="true" label="所属主题：" vtype="maxLength:16" required='true' emptyText="请输入所属主题" />
					 </td>
				</tr>
	           
	           <tr>
					<td>
							<input id="dict_parentid" name="dict_parentid" class="mini-textbox" labelField="true" label="上级代码：" vtype="maxLength:16"   emptyText="请输入上级代码值"   />
						 </td>
	           	 <td>
	           		<input id="dict_key" name="dict_key" class="mini-textbox" labelField="true" onblur='verifyUniquenessDictKey()'  label="代码值：" required="true"  vtype="maxLength:20" required='true' emptyText="请输入代码值" />
	           	 </td>
	           	 <td>
	           		<input id="dict_value" name="dict_value" class="mini-textbox" labelField="true" label="代码名称：" emptyText="请输入代码名称" required='true'  vtype="maxLength:100" />
	           	 </td>
	           	 </tr>
					<tr>
	           	 <td>
	           		<input id="dict_level" name="dict_level" class="mini-textbox" labelField="true"  label="代码级别：" emptyText="请输入代码级别"  vtype="maxLength:16"  />
	           	 </td>
	           	 <td>
	           		<input id="dict_desc" name="dict_desc" class="mini-textbox" labelField="true"  label="字典描述：" emptyText="请输入字典描述"  vtype="maxLength:100" />
					</td>
					<td><input id="is_active" class="mini-combobox" name="is_active" disabled = "true"  required="true"  labelField="true" label="是否启用："
						data = "CommonUtil.serverData.dictionary.YesNo"  showNullItem="false"  allowInput="false" emptyText="请选择..." ></td>
	           </tr>
			   <tr>
				<td colspan="3" style="text-align:center;">
					<a id="save_btn" class="mini-button" style="display: none"    onclick="add">保存</a>
					<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close()">关闭</a>
				</td>
			</tr>
	           
			  </table>
	 	</div>
	</div>
	<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var param=top['dictManagerList'].getData(action);
	var form=new mini.Form("#edit_form");
		$(document).ready(function(){
			initForm();
        });
		//关闭表单
		function close(){
			top["win"].closeMenuTab();
		}
		 function initForm(){
			var form=new mini.Form("#edit_form");
			if (action=="edit") {
				mini.get("dict_id").setEnabled(false);  
				mini.get("dict_name").setEnabled(false); 
				mini.get("dict_module").setEnabled(false); 
				mini.get("dict_parentid").setEnabled(false); 
				mini.get("dict_key").setEnabled(false);
				form.setData(param);
			}else if (action=="add") { 
				$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>数据字典维护</a>>>新增");
				form.setData(param);
				mini.get("dict_parentid").setValue("-");
				mini.get("is_active").setValue("1")
				mini.get("dict_id").setEnabled(false);  
				mini.get("dict_name").setEnabled(false); 
				mini.get("dict_module").setEnabled(false); 
				mini.get("dict_parentid").setEnabled(false);
				
			}else  if(action=="add2"){
				$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>数据字典维护</a>>>新增");
				mini.get("dict_parentid").setValue("-");
				mini.get("is_active").setValue("1");
				mini.get("dict_parentid").setEnabled(false);
			}
		} 
		
		//验证代码值得唯一性 
		function verifyUniquenessDictKey(){
			mini.get("dict_id").setEnabled(false);
			var dict_key = mini.get("dict_key").getValue();
			var dict_id = mini.get("dict_id").getValue();
			if (dict_key) {
				CommonUtil.ajax({
					url: "/TaDictController/getTaDictByDictIdForList",
					data: mini.encode({ "dict_id": dict_id,"dict_key": dict_key}),
					callback: function (data) {
						if (data.obj.length > 0) {
							mini.alert("该代码值已经存在", "系统提示", function () {
								mini.get("dict_key").setValue();
							});
						}
					}
				});
			}
		}
    	function add(){
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息");
				return;
			}
			var data=form.getData();// 获取表单多个控件数据
			var param=mini.encode(data)// 序列化json
			var saveUrl ="" //$.inArray(action, ["add"]) > -1 ? "/TaDictController/insertTaDictVo" : "/TaDictController/updateTaDictVo";
			if (action=="edit") {
				saveUrl="/TaDictController/updateTaDictVo";
			}else if(action=="add" || action=="add2"){
				saveUrl="/TaDictController/insertTaDictVo"
			}
			CommonUtil.ajax({
			url:saveUrl,
			data:param,
			callback:function(data){	
				if('error.common.0000' == data.code){
						mini.alert("保存成功","提示",function(){
							setTimeout(function(){top["win"].closeMenuTab()},10);
							top["DictEdit"].clearGrid2();
						});
					}else{
						mini.alert("保存失败");
					}
			}
		});
		}
    	</script>
</body>
</html>

