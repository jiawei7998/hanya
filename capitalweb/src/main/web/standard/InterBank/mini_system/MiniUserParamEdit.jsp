<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body>
    <div id="form1" >
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">用户参数</a> >> 修改</span>
        <div id="userParam" class="fieldset-body" >   
            <table id="user_param_form" width="100%" class="mini-table">
                <tr>
                    <td><input id="p_id" name="p_id" class="mini-textbox"  required="false"
                        labelField="true"  label="参数编号"  emptyText='' 
                        vtype="maxLength:30;float;" width="300px" enabled = "false"/>
                        <input id="status" name="status" class="mini-hidden" />
                    </td>
                    <td><input id='p_type' name="p_type" class="mini-combobox" required="true"   enabled="false" value="1"
                        data = "CommonUtil.serverData.dictionary.ParameterBigKind"
                        labelField="true"  label="参数大类"  emptyText='请选择参数大类' width="300px"/>
                    </td>
                    <td><input id="p_sub_type" name="p_sub_type" required="true"  width="300px"
                        labelField="true"  label="参数小类"  emptyText='请选择参数小类' class="mini-combobox"
                            data="CommonUtil.serverData.dictionary.UserParameterType"/>
                    </td>
                    
                </tr>
                <tr>
                    <td><input  id="p_name" name="p_name" required="true"  class="mini-textbox"
                        labelField="true"  label="参数名称"  emptyText='请输入参数名称'
                        vtype="maxLength:75" width="300px"/>
                    </td>
                    <td><input id="p_value" name="p_value"  class="mini-textbox"
                        labelField="true"  label="参数值"  emptyText='请输入参数值' required="true"
                        vtype="maxLength:50" width="300px"/>
                    </td>
                    <td><input id="p_sort" name="p_sort" class="mini-textbox"
                        labelField="true"  label="参数排序"  emptyText='请输入参数排序'
                        vtype="maxLength:10;float;" width="300px"/>
                    </td>
                </tr>
                <tr>
                    <td><input id="p_sub_name" name="p_sub_name" class="mini-textbox"
                        labelField="true"  label="参数小类描述"  emptyText='请输入参数小类描述'
                        vtype="maxLength:15" width="300px"/>
                    </td>
                    <td><input id="p_code" name="p_code" class="mini-textbox" 
                        labelField="true"  label="参数Id"  emptyText='请输入Id'
                        vtype="maxLength:15" width="300px"/>
                    </td>
                    <td><input id="p_parent_flag" name="p_parent_flag" class="mini-combobox" 
                        data="CommonUtil.serverData.dictionary.YesNo" onvaluechanged="onParentFlagChanged"
                        labelField="true"  label="是否为父节点" value="0" width="300px"/>
                    </td>
                </tr>
                <tr>
                    <td><input id="p_parent_code" name="p_parent_code" class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="false"
                        labelField="true"  label="参数父节点"  emptyText='请输入参数父节点'
                        vtype="maxLength:15" width="300px" required="true" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align:center;">
                        <a id="save_btn" class="mini-button" style="display: none"    onclick="SaveData">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
            
        </div>
    </div>

    <script type="text/javascript">
        mini.parse();
        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");
        var form1 = new mini.Form("user_param_form");
        var data=top["userParamManage"].getData();
        $(document).ready(function(){
            init();
        });

        function init(){
            if(action == "new"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>用户参数</a> >> 新增");
                
            }else if(action == "edit" || action == "detail"){
                form1.setData(data);
                //修改时，参数编号不可修改
                mini.get("p_id").setEnabled(false);
                mini.get("p_type").setEnabled(false);
                mini.get("p_sub_type").setEnabled(false);
                mini.get("p_name").setEnabled(false);

                var parent_code = mini.get("p_parent_code");
                parent_code.setValue(data.p_parent_code);
                parent_code.setText(data.p_parent_code);

                if(data.p_parent_code == '-1'){
                    mini.get("p_parent_flag").setValue("1");
                }else{
                    mini.get("p_parent_flag").setValue("0");
                }
                
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>用户参数</a> >> 详情");
                    form1.setEnabled(false);
                    document.getElementById("save_btn").style.display="none";
                    var parent_code=mini.get("p_parent_code");
                    parent_code.setValue(data.p_parent_code);
                    parent_code.setText(data.p_parent_code);
                }
            }
        }

        function onParentFlagChanged()
        {
            var p_parent_flag = mini.get("p_parent_flag").getValue();
            if(p_parent_flag == '0'){//否
                mini.get("p_parent_code").setValue("");
                mini.get("p_parent_code").setText("");

            }else{
                mini.get("p_parent_code").setValue("-1");
                mini.get("p_parent_code").setText("-1");
                mini.get("p_parent_code").setIsValid(true);
            }
        }
         
        //保存按钮
        function SaveData(){
            var form = new mini.Form("user_param_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            }
	        var data = form.getData();
            if(CommonUtil.isNull(data.p_parent_code)){
                data.p_parent_code = '-1';
            }

            var saveUrl="";

            if(action == "new"){
                saveUrl = "/UserParamController/addTaUserParam";
                data.status = 0;
            }else if(action == "edit"){
                saveUrl = "/UserParamController/editTaUserParam";
            }
            var param = mini.encode(data); //序列化成JSON
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(ret){
					 if(ret.detail =="120"){
				        mini.alert("保存失败,该分类下参数名称为["+data.p_name+"]的参数已存在！");
				
                    }else{
                        mini.alert("保存成功！","系统提示",function(){
                            setTimeout(function(){top['win'].closeMenuTab()},10);
                        });
                    }	 
                }
			});
        }
        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();

        }
        //参数父节点
        function  onButtonEdit(){
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniParamParentCode.jsp",
				title : "参数父节点选择",
				width : 900,
				height : 400,
				ondestroy : function(action) {
					//if (action == "close") return false;
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.p_code);
                            btnEdit.setText(data.p_code);
                            btnEdit.setIsValid(true);
						}
					}

				}
			});
		}
       
    </script>
</body>
</html>