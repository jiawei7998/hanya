<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>股票参数</title>
    <%@ include file="../../global.jsp" %>
    <script src="<%=basePath %>/sl/TaDictController/dictionary.js" type="text/javascript"></script>

</head>
<body style="width:100%;height:100%;background:white">
<fieldset id="fd2" class="mini-fieldset title">
    <legend><label>查询条件</label></legend>
    <div id="search_form">
        <input id="stockId" name="stockId" class="mini-textbox" labelField="true" label="股票代码："
               labelStyle="text-align:right;" emptyText="请输入股票代码"/>
        <input id="stockName" name="stockName" class="mini-textbox" labelField="true" label="股票名称："
               labelStyle="text-align:right;" emptyText="请输入股票名称"/>
        <span style="float:right;margin-right: 150px">
					<a class="mini-button" style="display: none" id="search_btn" onclick="query()">查询</a>
					<a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
			</span>
        <div>
            <!-- <div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
                <table style="width:100%;">
                    <tr>
                            <td  style="width:100%">
                                <a class="mini-button" style="display: none"   id="imp_clear" name="imp_clear"  onclick="clear">清空</a>
                             <a class="mini-button" style="display: none"  id="search_btn" name="search_btn"  onclick="search(10,0)" >查询</a>
                             <a class="mini-button" style="display: none"  id="add_btn" name="add_btn"  onclick="add();" >新增</a>
                            <a class="mini-button" style="display: none"  id="edit_btn" name="edit_btn"  onclick="edit" >修改</a>
                            <a class="mini-button" style="display: none"  id="delete_btn" name="delete_btn"  onclick="del();" >删除</a>
                            <a class="mini-button" style="display: none"   id="imp_btn" name="imp_btn"   onclick="importFile();">股票价格导入</a>
                        </td>
                       </tr>
                </table>
        </div> -->
        </div>
</fieldset>
<span style="margin:2px;display: block;">
		<a class="mini-button" style="display: none" id="add_btn" name="add_btn" onclick="add();">新增</a>
		<a class="mini-button" style="display: none" id="edit_btn" name="edit_btn" onclick="edit">修改</a>
		<a class="mini-button" style="display: none" id="delete_btn" name="delete_btn" onclick="del();">删除</a>
		<a class="mini-button" style="display: none" id="imp_btn" name="imp_btn" onclick="importFile();">股票价格导入</a>
	</span>

<div id="stockManage" class="mini-fit">
    <div id="stockManagerGrid" class="mini-datagrid borderAll" style="width:100%;height:100%" allowAlternating="true"
         idField="stockId" sortMode="client" onrowdblclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
            <div field="stockId" width="100" allowSort="false" headerAlign="center">股票代码</div>
            <div field="stockName" width="100" allowSort="false" headerAlign="center">股票名称</div>
            <div field="price" width="100" allowSort="false" numberFormat="#,0.00" align="right" headerAlign="center">
                股票价格
            </div>
            <div field="importDate" width="100" allowSort="false" align="center" headerAlign="center">录入时间</div>
        </div>
    </div>
</div>


<script type="text/javascript">
    mini.parse();
    top['stockManagerGrid'] = window;
    var grid = mini.get("stockManagerGrid");
    var form = new mini.Form("search_form");
    //分页
    var grid = mini.get("stockManagerGrid");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        ////console.log("起始页="+pageIndex);
        var pageSize = e.data.pageSize;
        ////console.log("每页大小="+pageSize);
        search(pageSize, pageIndex);
    });

    function query() {
        search(grid.pageSize, 0);
    }

    $(document).ready(function () {
        search(10, 0);
    });

    //查询
    function search(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) return;
        //提交数据
        var data = form.getData();//获取表单多个控件的数据
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var param = mini.encode(data); //序列化成JSON
        ////console.log(param)
        //ajax请求
        CommonUtil.ajax({
            url: '/StockController/pageStock',
            data: param,
            callback: function (data) {
                ////console.log(data.obj.rows);
                var grid = mini.get("stockManagerGrid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);

            }
        });
    }

    //导入文件
    function importFile() {
        mini.open({
            url: CommonUtil.baseWebPath() + "/mini_system/MiniStockPriceUpload.jsp",
            width: 420,
            height: 150,
            title: "文件上传",
            ondestroy: function (action) {
                //mini.alert("股票价格导入成功","提示");
                search(10, 0);//重新加载页面
            }
        });
    }

    //新增
    function add() {
        var url = CommonUtil.baseWebPath() + "/mini_system/MiniStockPriceEdits.jsp?action=add";
        var tab = {
            id: "StockPriceEdit",
            name: "StockPriceEdit",
            text: "股票价格新增",
            url: url,
            parentId: top["win"].tabs.getActiveTab().name
        };
        top['win'].showTab(tab);

    }

    //修改
    /* function edit(){
        var grid=mini.get("stockGrid");
    var row = grid.getSelected();
    if(row){
        var url = CommonUtil.baseWebPath() + "/mini_system/MiniStockPriceEdits.jsp?action=edit";
        var tab = { id: "stockEdit", name: "stockEdit", text: "股票修改", url: url,parentId:top["win"].tabs.getActiveTab().name };
        top["win"].showTab(tab);

    } else {
        mini.alert("请选中一条记录！","消息提示");
    }
    } */

    function edit() {
        var grid = mini.get("stockManagerGrid");
        var record = grid.getSelected();
        if (record) {
            var url = CommonUtil.baseWebPath() + "/mini_system/MiniStockPriceEdits.jsp?action=edit";
            var tab = {
                id: "stockEdit",
                name: "stockEdit",
                text: "股票价格修改",
                url: url,
                parentId: top['win'].tabs.getActiveTab().name
            };

            top['win'].showTab(tab);
        } else {
            mini.alert("请选中一条记录", "系统提示");
        }
    }

    //详情
    function onRowDblClick() {
        var url = CommonUtil.baseWebPath() + "/mini_system/MiniStockPriceEdits.jsp?action=detail";
        var tab = {
            id: "stockDetail",
            name: "stockDetail",
            text: "股票价格详情",
            url: url,
            parentId: top["win"].tabs.getActiveTab().name
        };
        top['win'].showTab(tab);
    }

    function getData(action) {
        var grid = mini.get("stockManagerGrid");
        if (action == "edit" || action == "detail") {
            var row = null;
            row = grid.getSelected();
        }
        return row;
    }

    //删除
    function del() {
        var grid = mini.get("stockManagerGrid");
        var row = grid.getSelected();
        var param = {};
        if (row) {
            param.stockId = row.stockId;
            var stockId = mini.encode(param);
            mini.confirm("您确定要删除吗？", "温馨提示", function (action) {
                if (action == 'ok') {
                    CommonUtil.ajax({
                        url: "/StockController/deleteStock",
                        data: stockId,
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("操作失败");
                            } else {
                                mini.alert("操作成功");
                                search(10, 0);
                            }

                        }
                    });
                }
            });
        } else {
            mini.alert("请先选择一条数据操作", "操作提示");

        }
    }

    //批量删除
    /* function del(){
        var rows=grid.getSelecteds();
        if(rows.length==0){
            mini.alert("请选中一行","提示");
            return;
        }
        var dealNos = new Array();
        $.each(rows, function(i, n){
            if(n.approveStatus=="3"){
                dealNos.push(n.dealNo);
            }
        });
        mini.confirm("您确认要删除选中记录?","系统警告",function(value){
            if (value=='ok'){
                var data=dealNos;
                params=mini.encode(data);
                CommonUtil.ajax( {
                    url:"/DurationController/removeTrasForProductCapitalSchedule",
                    data:params,
                    callback : function(data) {
                        mini.alert("删除成功.","系统提示");
                        search(10,0);
                    }
                });
            }
        });
    } */
    //清空
    function clear() {
        mini.get("stockId").setValue("");
        mini.get("stockName").setValue("");
    }
</script>
</body>
</html>
	