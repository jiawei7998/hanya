<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>用户参数</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset id="fd2" class="mini-fieldset title">
    <legend><label>查询条件</label></legend>
    <div id="find" >
        <div id="search_form"  >
            <!-- <input id="p_id" name="p_id" class="mini-textbox" labelField="true" 
                label="参数编号："  emptyText="请输入参数编号" labelStyle="text-align:right;"/> -->
            <input id="p_name" name="p_name" class="mini-textbox" labelField="true" 
                label="参数名称："  emptyText="请输入参数名称" labelStyle="text-align:right;"/>
            <input id="p_value" name="p_value" class="mini-textbox" labelField="true" 
                label="参数值："  emptyText="请输入参数值" labelStyle="text-align:right;"/>
            <input id="p_sub_type" name="p_sub_type" class="mini-combobox" 
            emptyText="请选择参数小类" data="CommonUtil.serverData.dictionary.UserParameterType" 
            labelField="true"  label="参数小类："  labelStyle="text-align:right;"/>
            <span style="float:right;margin-right: 150px">
                <a id="search_btn" class="mini-button" style="display: none"   onclick="search1()" >查询</a>
                <a id="clear_btn" class="mini-button" style="display: none"    onClick="clearData">清空</a>
                
            </span>
        </div>
        
    </div>
    </fieldset>
     <!--
    <span style="margin:2px;display: block;">
        <a id="add_btn" class="mini-button" style="display: none"    onclick="add">新增</a>
        <a id="edit_btn" class="mini-button" style="display: none"    onclick="edit">修改</a>
        <a id="delete_btn" class="mini-button" style="display: none"     onclick="removeRow">删除</a>
        <a id="submit_btn" class="mini-button" style="display: none"      onclick="submit()">提交复核</a>
    </span> -->
    
    <div id="tradeManage" class="mini-fit" style="margin-top: 2px;"> 
    <div id="UserParamManage" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
     idField="p_id"   multiSelect="false"  sortMode="client" allowAlternating="true"
     onRowdblclick="custDetail" >
        <div property="columns" >
            <div type="indexcolumn" width="40px" headerAlign="center" align="center">序号</div>
            <!-- <div field="p_id" width="80px" headerAlign="center" align="center" allowSort="true" dataType="int">参数编号</div>  -->   
            <div field="status"   headerAlign="center"  width="80px"renderer="CommonUtil.dictRenderer" data-options="{dict:'status'}">状态</div>                            
            <div field="p_type"   headerAlign="center" width="80px" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ParameterBigKind'}">参数大类</div>
            <div field="p_sub_type"    headerAlign="center"   width="120px"align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'UserParameterType'}">参数小类</div>                                
            <<!-- div field="p_sub_name"  headerAlign="center"  width="180px">参数小类描述</div> -->
            <div field="p_name" headerAlign="center"  width="140px">参数名称</div>
            <!-- <div field="p_value" headerAlign="center"  width="100px">参数值</div> -->
            <!-- <div field="p_sort" headerAlign="center" allowSort="true"  width="60px" dataType="int">排序</div> -->
            <div field="p_code" headerAlign="center" align="center"  width="80px">参数Id</div>
            <div field="p_parent_code" headerAlign="center" align="center"  width="80px">参数父节点</div>
        </div>
    </div>
    </div>

    <script type="text/javascript">

        
        mini.parse();
		

		$(document).ready(function(){
			var grid =mini.get("UserParamManage");
			grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				search(pageSize,pageIndex);
			});
			search(grid.pageSize,0);
    	
		});

        function search(pageSize,pageIndex){
			var form =new mini.Form("find");
			form.validate();
			if (form.isValid() == false) return;//表单验证
			var data =form.getData();//获取表单数据
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url:"/UserParamController/pageTaUserParams/",
				data:param,
				callback:function(data){
						var grid =mini.get("UserParamManage");
						//设置分页
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						//设置数据
						grid.setData(data.obj.rows);
					}
				});
		}

        function search1(){
            var grid =mini.get("UserParamManage");
            search(grid.pageSize,0);
        }

        //清空
        function clearData(){
            var form =new mini.Form("find");
            form.clear();
        }
        
       // top["userParamManage"]=window;
        function getData(){
            var grid = mini.get("UserParamManage");
            var record =grid.getSelected(); 
            return record;
        }


    function GetData() {
	var grid = mini.get("UserParamManage");
	var row = grid.getSelected();
	return row;
}

        //双击一行打开详情信息
        function custDetail(e) {
            onOk();
        }
        function CloseWindow(action) {
            if (window.CloseOwnerWindow)
                return window.CloseOwnerWindow(action);
            else
                window.close();
        }
        function onOk() {
        CloseWindow("ok");
        }
    </script>
</body>
</html>