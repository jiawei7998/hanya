<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>用户信息查询</legend>
    <div id="UserManageGrid">
        <input id="instId" name="instId" class="mini-buttonedit" onbuttonclick="onInsQuery" emptyText="请选择机构..." labelField="true" allowInput="false"
            label="所属机构：" labelStyle="text-align:right;" />
        <input id="empId" name="empId" class="mini-textbox" labelField="true" label="工号：" labelStyle="text-align:right;" emptyText="请输入工号" 
        />
        <input id="userName" name="userName" class="mini-textbox" labelField="true" label="姓名：" labelStyle="text-align:right;" emptyText="请输入用户名" 
        />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search"  onclick="search(10,0)">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="modify"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete"  onClick="deleteUser();">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" allowAlternating="true"   idField="userId" id="grid"  >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="userId" headerAlign="center" allowSort="true" >用户UM</div>
                <div field="empId" headerAlign="center" allowSort="true">工号</div>
                <div field="userName" headerAlign="center"  >姓名</div>
                <div field="isActive" headerAlign="center" align="center" renderer="onUserStatusRenderer" allowSort="true">用户状态</div>
                <div field="insts" headerAlign="center" width="350px" allowSort="true" >所属机构</div>
                <div field="roles" headerAlign="center" allowSort="true">角色</div>
                <div field="limit" headerAlign="center" align="center" renderer="onLimitRenderer" allowSort="true">权限分配</div>
                
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("grid");
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                search(pageSize, pageIndex);
            });
            search(grid.pageSize, grid.pageIndex);
        });
    })
    //用户状态的渲染
    var userStatus = [{ "id": "0", "text": "注销" }, { "id": "1", "text": "正常" }];
    function onUserStatusRenderer(e) {
        for (var i = 0, l = userStatus.length; i < l; i++) {
            var g = userStatus[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }
       
    //显示效果
    function successtips() {
        mini.showTips({
            content: '<span class="tips-icontext">操作成功</span>',
            state: "success",
            offset: [0, 0],
            x: "center",		  
            y: "center",
            timeout: 3000
        });
    }
    top['userEdit'] = window;
        function ShowUser() {
            var record = grid.getSelected();
            return record;
        }
    //用户的修改
    function modify() {
        var record = grid.getSelected();
        if (record) {
            var url = CommonUtil.baseWebPath() + "/mini_system/MiniUserEdits.jsp?action=edit";
            var tab = { id: "userEdit", name: "userEdit", text: "用户修改", url: url, parentId: top["win"].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        } else {
            mini.alert("请选择一条数据");
        }
    }
    
    //用户的删除
    function deleteUser() {
        var record = grid.getSelected();
        if (record) {
            var list = new Array();
            list.push(record.userId);
            mini.confirm("您确定要删除<span style='color:#FF3333;font-weight:bold;'>" + record.userName + "</span>用户吗?", "系统提示", function (r) {
                if (r == 'ok') {
                    CommonUtil.ajax({
                        url: "/UserController/deleteUser/",
                        data: list,
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                successtips();
                                search(10, 0);
                            } else {
                                mini.alert("删除失败", "系统提示");
                            }
                        }
                    });
                }
            })
        } else {
            mini.alert("请选择要删除的记录", "系统提示");
        }
    }
        
    function onLimitRenderer(e) {
        var row = e.record;
        var userId = row.userId;
        var instsId = row.instsId;
        var s = "<a class='mini-button' height=22  onclick='QueryRole(\"" + userId + "\",\"" + row.userName + "\")' >角色</a>";
        return s;
    }
    //根据用户Id 查询权限--废弃
    function QueryPermissions(userId, instsId) {
        if (userId) {
            mini.open({
                url: CommonUtil.baseWebPath() +"/mini_system/MiniShowResourceInformation.jsp",
                title: "角色权限", width: 800, height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    var data = { userId: userId, instsId: instsId };
                    iframe.contentWindow.SetDataObj(data);
                },
                ondestroy: function (action) {
                    if (action == 'ok') {
                        search(10, 0);
                    }
                }
            });
        } else {
            mini.alert("请选择一个用户");
        }
    }
    //用户修改角色
    function QueryRole(userId, userName) {
        if (userId) {
            mini.open({
                url: CommonUtil.baseWebPath() +"/mini_system/MiniShowUserRole.jsp?action=" + "edit" + "&userId=" + userId + "&userName=" + userName ,
                title: "角色管理",
                width: 800,
                height: 400,
                ondestroy: function (action) {
                    search(10, 0);
                }
            });
        } else {
            mini.alert("请选择一条记录");
        }
    }
    //搜索
    function search(pageSize, pageIndex) {
        var form = new mini.Form("UserManageGrid");
        var data = form.getData();
        form.validate();
        if(form.isValid()==false){
            return;
        }
        data.branchId =branchId;
        data.pageNumber = pageIndex + 1;
        data.pageSize = pageSize;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/UserController/searchPageUser",
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    function onInsQuery(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionSelectManages.jsp",
            title: "机构选择",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.instId);
                        btnEdit.setText(data.instName);
                        btnEdit.focus();
                    }
                }
            }
        });
    }
    //新增用户
    function add() {
            var url = CommonUtil.baseWebPath() + "/mini_system/MiniUserEdits.jsp";
            var tab = { id: "UserAdd", name: "UserAdd", text: "用户新增", url: url, parentId: top["win"].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }
    function clear() {
        var form = new mini.Form("#UserManageGrid");
        form.clear();
        }     
    </script>