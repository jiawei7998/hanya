﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
	<legend>日终任务查询</legend>
<div id="search_form" style="width:100%;">
	<div class="mini-form" >
		<input  id="bizDate" name="bizDate" value="<%=__bizDate%>" class="mini-datepicker" labelField="true" label="业务日期："  labelStyle="text-align:right;" >
		<span style="float:right;margin-right: 150px">
			<a  id="search_btn" class="mini-button" style="display: none"  onclick="search()"  >查询</a>
			<a  id="start_btn" class="mini-button" style="display: none"  onclick="start()"  >开始</a>
			<a  id="stop_btn" class="mini-button" style="display: none"  onclick="stop()"  >停止</a>
		</span>
	</div>
</div>
</fieldset>   
<div region="center" class="mini-fit">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%" 
			idField="id" allowResize="true" multiSelect="true" showPager="false"
			border="true" onshowrowdetail="onShowRowDetail">
		<div property="columns">
			<div type="indexcolumn" width="50" align="left"  headerAlign="center">序号</div>
			<div type="expandcolumn"></div>
			<div field="bizDate" width="80" headerAlign="center" align="center">业务日期</div>    
			<div field="stepId" width="60" headerAlign="center" align="center">步骤编号</div>                            
			<div field="stepName" width="100" headerAlign="center" align="left">步骤名称</div>
			<div field="status" width="60" renderer="onStatusRenderer" headerAlign="center" align="center">状态</div>                                
			<div field="isSkip" width="60" renderer="onRenderer" headerAlign="center" align="center">可跳过</div>
			<div field="isLoop" width="60" renderer="onRenderer" headerAlign="center" align="center">可重复执行</div>
			<div field="operateTime" width="100" headerAlign="center" align="center">操作时间</div>
		</div>
	</div>
</div> 
<script type="text/javascript">
	mini.parse();
	//事件定义***********************************/
	//查询信息
	function search(){
		var form = new mini.Form("#search_form");
		form.validate();
		if (form.isValid()==false) return;
		var data=form.getData(true);
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/DayendController/searchDayendStepLog",
			data:params,
			callback : function(data) {
				var grid=mini.get("datagrid"); 
				grid.setData(data.obj);
			}
		});
	}
	//开始
	function start(){
		mini.confirm("您确定要开始日终吗?","系统提示",function(action){
			if (action == "ok"){   
				CommonUtil.ajax( {
					url:"/DayendController/startDayendTask/",
					data:"",
					callback : function(data) {
						mini.alert(data.desc,"系统提示");		    			
						search();
					}
				});
			}
		});
	}
	//停止
	function stop(){
		mini.confirm("您确定要停止日终吗?",'系统提示',function(action){
			if (action == "ok"){   
				CommonUtil.ajax( {
					url:"/DayendController/stopDayendTask/",
					data:"",
					callback : function(data) {
						mini.alert(data.desc,"系统提示");		    			
						search();
					}
				});
			}
		});
	}
	/*
		显示详细信息
	*/
	function onShowRowDetail(e){
		 var grid = e.sender;
         var row = e.record;
         var logs = row.logs==null || row.logs==""?"":row.logs;
         var td = grid.getRowDetailCellEl(row);
         td.innerHTML="执行日志：<pre>" + logs + "</pre>";
	}
	
	//显示效果处理
	function onRenderer(e){
		if("1"==e.value) return "是";
		return "否";
	}
	function onStatusRenderer(e){
		if("Success"==e.value){
			return "<span style='color:#008F00;font-weight:bold;'>" + e.value + "</span>";
		}else{
			return "<span style='color:#FF3333;font-weight:bold;'>" + e.value + "</span>";

		}
	}
	$(document).ready(function(){
		search();
		
	});
</script>  
</body>
</html>