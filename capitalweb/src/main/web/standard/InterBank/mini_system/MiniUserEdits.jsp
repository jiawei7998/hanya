<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/miniScript/md5.js"></script>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body>
<div id="UserEdit"  style="width: 100%; height: 800px;">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">用户添加</a> >> 新增</span>
		<div id="StaffBasicInformation" style="padding:5px;">
			<input class="mini-hidden" name="id" />
			<table style="width:100%;" class="mini-table">
				<tr>
					<td>
						<input id="userId" name="userId" class="mini-textbox" labelField="true" label="用户UM：" emptyText="请输入用户UM" required="true"  width="300px"
						 validateOnLeave="true" onblur='verifyUniqueness()' vtype="maxLength:10" requiredErrorText="用户UM不能为空" />
					</td>
					<td>
						<input id="empId" name="empId" class="mini-textbox" labelField="true" label="工号：" width="300px" emptyText="请输入工号" required="true"  validateOnLeave="true"
						 requiredErrorText="工号不能为空" vtype="maxLength:10"/>
					</td>
					<td>
						<input id="isActive" name="isActive" labelField="true" label="用户状态：" width="300px" emptyText="请选择用户状态" required="true"  class="mini-combobox"
						 data="[{'id':'0','text':'注销'},{'id':'1','text':'正常'}]" />
					</td>
				</tr>
				<tr>
					<td>
						<input id="userName" name="userName" class="mini-textbox" labelField="true" label="姓名：" width="300px" required="true"  validateOnLeave="true"
						 vtype="maxLength:15" requiredErrorText="姓名不能为空" emptyText="请输入姓名" />
					</td>
					<td>
						<input id="instId" name="instId" valueField="id" showFolderCheckBox="true" labelField="true" label="所属机构：" required="true"  showCheckBox="true" width="300px"
						 showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId" class="mini-treeselect" resultAsTree="false"
						 valueFromSelect="true" multiSelect="true" emptyText="请选择机构" />
					</td>
					<td>
						<input id="userMemo" name="userMemo" labelField="true" label="备注：" vtype="maxLength:50" width="300px" emptyText="请输入备注" required="true"  class="mini-textbox"
						/>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center;">
							<a class="mini-button" style="display: none"    onClick="save()" id="save_btn">保存</a>
							<a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
						</td>
					
				</tr>
			</table>
		</div>
</div>
</body>
<script>
mini.parse();
var grid = mini.get("instId");
var userIdValue = mini.get("userId");
var empIdValue = mini.get("empId");
$(document).ready(function(){
	if(GetQueryString("action")){
		$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>用户新增</a> >> 修改");
		init();
	}else{
		institution();
	}
	userIdValue.on("validation",function(e){
		if(e.isValid){
			if(EnglistAndNumberValidation(e.value)==false){
				e.errorText ="请输入字母或者数字";e.isValid=false;
			}

		}
	});
	empIdValue.on("validation",function(e){
		if(e.isValid){
			if(EnglistAndNumberValidation(e.value)==false){
				e.errorText ="请输入字母或者数字";e.isValid=false;
			}

		}
	})

})
//验证唯一性
function verifyUniqueness(){
	var userUm =mini.get("userId").getValue();
	if(userUm){
		CommonUtil.ajax({
			url: "/UserController/verifyUniqueness",
			data: mini.encode({ "userId":userUm,"branchId":branchId }),
			callback: function (data) {
				if(data.obj.length>0){
					mini.alert("该用户已经存在","系统提示",function(){
						mini.get("userId").setValue();
					});
				}
			}
		});
	}
}

function GetQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}
//渲染数据
function init() {
	if(GetQueryString("action")=='edit'){
		var form =new mini.Form("StaffBasicInformation");
		var data=top["userEdit"].ShowUser();

		form.setData(data);
		mini.get("isActive").setValue(data.isActive);
		var instIdValue='';
		//根据userId 查询用户所在的机构
		CommonUtil.ajax({
			url: "/InstitutionController/searchInstByUserId",
			data: mini.encode({ "userId": data.userId }),
			callback: function (data) {
				if (data.obj.length > 0) {
					for (var i = 0; i < data.obj.length; i++) {
						instIdValue = instIdValue + data.obj[i] + ","
					}
				}
			}
		});
		var param = mini.encode({ "branchId": branchId }); //序列化成JSON
		CommonUtil.ajax({
			url: "/InstitutionController/searchInstitutionByBranchId",
			data: param,
			callback: function (data) {
				
				grid.setData(data.obj);
				grid.setValue(instIdValue);
			}
		});
		mini.get("userId").setEnabled(false);
		mini.get("empId").setEnabled(false);
	}
}
// 验证只能输入字母和数字
function EnglistAndNumberValidation(userId){
	var reg = new RegExp("^[0-9a-zA-Z]+$");
	if(!reg.test(userId)){
       return false;
	}
	return true;
}


//保存
	function save() {
		var url = '';
		var form = new mini.Form("UserEdit");
		form.validate();
		if (form.isValid() == false) {
			return;
		}
		var data = form.getData();
		if (GetQueryString("action") == 'edit') {
			url = "/UserController/updateUserInfo";
		} else {
			url = "/UserController/addUser";
			data.userPwd = hex_md5("123456").toUpperCase();//用户名默认密码
			//设置银行id
			data.branchId = branchId;
		}
		if (data.instId) {
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url: url,
				data: param,
				callback: function (data) {
					if (data.code == 'error.common.0000') {
						mini.alert("操作成功","系统提示",function(){setTimeout(function(){top["win"].closeMenuTab()},10);});
					} else {
						mini.alert("操作失败","系统提示");
					}
				}
			});
		} else {
			mini.alert("机构不能为空");
		}
	}
//加载机构树
function institution() {
	var param = mini.encode({ "branchId": branchId }); //序列化成JSON
	CommonUtil.ajax({
		url: "/InstitutionController/searchInstitutionByBranchId",
		data: param,
		callback: function (data) {
			grid.setData(data.obj);
		}
	});
}
//取消
function onCancel(e) {
	window.CloseOwnerWindow();
}
//取消按钮
function cancel(){
	window.CloseOwnerWindow();
}
</script>