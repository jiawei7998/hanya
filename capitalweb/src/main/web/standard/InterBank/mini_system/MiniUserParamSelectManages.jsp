<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
	<body style="width:100%;height:100%;background:white">
		<fieldset class="mini-fieldset">
			<legend>参数查询</legend>
			<div id="search_form" class="mini-form" width="80%">
				<input id="p_type" name="p_type" class="mini-hidden" value=""/>
				<input id="p_sub_type" name="p_sub_type" class="mini-hidden" value=""/>
				<input id="p_name" name="p_name" type="text" class="mini-textbox" style="margin-left: 50px;width:300px;" labelField="true"  label="参数名称："  emptyText="请输入参数名称" />
				<span style="float:right;margin-right: 150px">
					<a onClick="search"  id="search_btn" class="mini-button" style="display: none"  >查询</a>
				</span>
			</div>  
		</fieldset>
		<div class="mini-fit" >
			<div id="treegrid" class="mini-treegrid borderAll" showTreeIcon="true" treeColumn="p_name" idField="id" parentField="parentId" 
				resultAsTree="true" allowResize="false" expandOnLoad="true" onrowdblclick="onRowDblClick">
				<div property="columns">
					<div type="indexcolumn" width="50" headerAlign="center">序号</div>
					<div field="p_name" name = "p_name" width="180" headerAlign="center">参数名称</div>
					<div field="p_value" name = "p_value" width="120" headerAlign="center">参数值</div>
				</div>
			</div>
		</div>   

		<script>
			mini.parse();

			function search(){
				var form = new mini.Form("search_form");
				//提交数据
				var data = form.getData();//获取表单多个控件的数据  
				data.status = '2';
				var params = mini.encode(data); //序列化成JSON
				CommonUtil.ajax({			
					url : "/UserParamController/searchParamByTypeByMap",
					data : params,
					callback : function(data) 
					{
						var grid = mini.get("treegrid");
						grid.setData(data.obj);
					}
				}); 
			}
			
			function onRowDblClick(e) {
				onOk();
			}

			function CloseWindow(action) {
				if (window.CloseOwnerWindow){
					 return window.CloseOwnerWindow(action);
				}else{ 
					window.close();
				}
			}
			
			function onOk() {
				CloseWindow("ok");
			}
			
			function GetData() {
				var grid = mini.get("treegrid");
				var row = grid.getSelected();
				return row;
			}

			function init(param)
			{
				mini.get("p_type").setValue(param.p_type);
				mini.get("p_sub_type").setValue(param.p_sub_type);
				search();
			};
		</script>
	</body>
</html>
