﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset">
				<legend>机构查询</legend>
				<div id="institution_search_form" style="width:100%;">
					<input id="instId" name="instId" class="mini-textbox" labelField="true" label="机构ID：" labelStyle="text-align:right;" emptyText="请输入机构代码"
					/>
					<input id="instName" name="instName" class="mini-textbox" labelField="true" label="机构简称：" labelStyle="text-align:right;"
					 emptyText="请输入机构简称" />
					<input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
					<span style="float:right;margin-right: 400px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="search">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</span>

				</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a class="mini-button" style="display: none"   id="add_btn" onclick="add();">新增</a>
				<a class="mini-button" style="display: none"   id="edit_btn" onclick="update();">修改</a>
				<a class="mini-button" style="display: none"    id="collaspe_btn" onclick="collaspe();">收起</a>
			</span>
			<div class="mini-fit">
				<div id="treegrid" class="mini-treegrid" showTreeIcon="true" treeColumn="instName" idField="id" parentField="parentId" resultAsTree="false"
				 allowResize="true" expandOnLoad="true" height="100%" onrowdblclick="onRowDblClick" allowAlternating="true">
					<div property="columns" allowMove="true" allowResize="true">
						<div type="indexcolumn" width="50" headerAlign="center"></div>
						<div field="instId" width="100" headerAlign="center">机构代码</div>
						<div name="instName" field="instName" width="250" headerAlign="center">机构简称</div>
						<div field="instFullname" width="300" headerAlign="center">机构全称</div>
						<div field="instType" width="100" renderer="onInstitutionTypeRenderer" headerAlign="center" align="center">机构类型</div>
						<!-- <div field="instBusLicense" width="100" headerAlign="center">营业执照</div> -->
						<div field="instLrInstCode" width="100" headerAlign="center">机构代码证</div>
						<div field="onlineDate" width="100" headerAlign="center" align="center">上线日期</div>
						<div field="linkMan" width="100" headerAlign="center">联系人</div>
						<div field="telephone" width="100" headerAlign="center">手机</div>
						<div field="instSimpleName" width="100" headerAlign="center">简名</div>
						<div field="isFta" width="200" renderer="onGenderRenderers" headerAlign="center" align="center">是否自贸区机构</div>
						<!-- <div field="prdName" width="200" headerAlign="center">准入产品</div> -->
					</div>
				</div>
			</div>
		<script type="text/javascript">
			mini.parse();
			top['InstitutionManger'] = window;
			var form = new mini.Form("#institution_search_form");
			var grid = mini.get("treegrid");
			var InstitutionType =CommonUtil.serverData.dictionary.InstitutionType;
			
			//渲染机构字段
			function onInstitutionTypeRenderer(e) {
				for (var i = 0, l = InstitutionType.length; i < l; i++) {
					var g = InstitutionType[i];
					if (e != null && g.id == e.value) return g.text;
				}
				return "";
			}

			function onGenderRenderers(e) {
				var Genders = [{
					id: 0,
					text: '是'
				}, {
					id: 1,
					text: '否'
				}
				];
				for (var i = 0, l = Genders.length; i < 2; i++) {
					var g = Genders[i];
					if (g.id == e.value)
						return g.text;
				}
				return " ";
			}

			function search() {
				var form = new mini.Form("institution_search_form");
				form.validate();
				var data = form.getData();//获取表单多个控件的数据  
				var params = mini.encode(data); //序列化成JSON
				CommonUtil.ajax({
					url: '/InstitutionController/searchInstitutions',
					data: params,
					callback: function (data) {
						var grid = mini.get("treegrid");
						grid.setData(data.obj);
					}
				});
			}
			// 新增的方法

			function add() {
				var url = CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionEdits.jsp?action=add";
				var tab = { id: "MiniInstitutionAddEdits", name: "MiniInstitutionAddEdits", text: "机构新增", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
				top['win'].showTab(tab);
			}

			//修改的方法
			function update() {
				var row = mini.get("treegrid").getSelected();
				if (row) {
					var url = CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionEdits.jsp?action=edit";
					var tab = { id: "MiniInstitutionEdits", name: "MiniInstitutionEdits", title: "机构修改", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
					top["win"].openNewTab(tab, row);
				} else {
					mini.alert("请选中一条记录");
				}
			}

			function getData(action) {
				if (action != "add") {
					var row = null;
					row = grid.getSelected();
				}
				return row;
			}

			function clear() {
				var form = new mini.Form("#institution_search_form");
				form.clear();
			}

			function collaspe() {
				var grid = mini.get("treegrid");
				grid.collapseAll();
			}

			function onRowDblClick(e) {
				var row = grid.getSelected();
				if (row) {
					var url = CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionEdits.jsp?action=detail";
					var tab = { id: "MiniInstitutionDetail", name: "MiniInstitutionDetail", title: "机构详情", url: url, showCloseButton: true ,parentId: top["win"].tabs.getActiveTab().name };
					top["win"].openNewTab(tab, row);

				} else {
					mini.alert("请选中一条记录！", "消息提示");
				}
			}

			$(document).ready(function () {
				search();
			})
		</script>
		</body>
		</html>