<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<!--
    资源树
-->
<div id="resource" class="mini-panel" title="菜单资源" style="width:700px;height:600px;float:left;" showToolbar="true" showCloseButton="false"
    showFooter="false">
    <ul id="treeGridMenu" class="mini-tree" showTreeIcon="true" textField="text" idField="id" parentField="pid" resultAsTree="false"
         allowSelect="true" enableHotTrack="false" onnodeclick="getNote">
    </ul>

</div>
<!--
    台子列表
-->
<div id="deskList" class="mini-panel" title="菜单列表" style="width:100%;height:500px;" showToolbar="true" showCloseButton="false" showFooter="true">
    <div id="MemuGrid" class="mini-datagrid borderAll" style="width:100%;height:500px;" allowResize="true" onrowdblclick="getNote"
        idField="deskId" pageSize="20" multiSelect="false" allowAlternating="true">
        <div property="columns">
            <div type="checkcolumn" field="deskId"></div>
            <div field="deskId"   headerAlign="center" allowSort="true">工作台Id</div>
            <div field="deskName" headerAlign="center" allowSort="true">工作台名称</div>
            <div field="branchId" headerAlign="center" visible="false" allowSort="true">银行归属</div>
            <div field="deskUrl"  headerAlign="center" allowSort="true">URL地址</div>
        </div>
    </div>
</div>
<script>
    mini.parse();
    var deskList=mini.get("deskList");
    var resource=mini.get("resource");
    var MemuGrid=mini.get("MemuGrid");
    var treeGrid = mini.get("treeGridMenu");
    $(document).ready(function(){
        if(GetQueryString("action")=='1'){//加载树
            document.getElementById("deskList").style.display="none";
            loadResourceTree();
        }else{//加载列表
            document.getElementById("resource").style.display="none";
            MemuGrid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                deskMenu(pageSize, pageIndex);
            });
            deskMenu(20,0);

        }
    })
    function deskMenu(pageSize, pageIndex) {
    var memuObj = {};
    memuObj.branchId = branchId;
    memuObj.pageSize = pageSize;
    memuObj.pageNumber = pageIndex + 1;
    if (branchId) {
        CommonUtil.ajax({
            url: "/deskMenuController/getAllMenuByBranchId",
            data: mini.encode(memuObj),
            callback: function (data) {
                MemuGrid.setTotalCount(data.obj.total);
                MemuGrid.setPageIndex(pageIndex);
                MemuGrid.setPageSize(pageSize);
                MemuGrid.setData(data.obj.rows);
            }
        });

    }
}  
    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return unescape(r[2]);
        return null;
    }
    function GetData(){
        if(GetQueryString("action")=='1'){
            return treeGrid.getSelectedNode();
        }else{
            return MemuGrid.getSelected();
        }
    }
    function onCancel(e) {
        window.CloseOwnerWindow();
    }
    function getNote(){
        onCancel();
    }
    function loadResourceTree() {
            var data = {};
            data.branchId = branchId;
            var param = mini.encode(data);
            CommonUtil.ajax({
                url: "/RoleController/getAllResourceByBranchId",
                data: param,
                callback: function (data) {
                    var grid = mini.get("treeGridMenu");
                    
                    grid.setData(data.obj);
                }
            });
        }

</script>