<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>角色信息</legend>
    <table>
        <tr>
            <td>
                角色名称：
            </td>
            <td style="padding-right: 10px;">
                <input id="roleName" name="roleName" enabled="false" allowInput="false" class="mini-textbox" vtype="maxLength:50" />
            </td>
            <!-- <td>
                机构名称：
            </td>
            <td>
                <input id="instName" name="instName" allowInput="false" class="mini-textbox" enabled="false" vtype="maxLength:50" />
            </td> -->
            <td>
                <a class="mini-button" style="display: none"  onclick="save()"  >保存</a>
            </td>
        </tr>
    </table>
</fieldset>
<div class="mini-splitter" vertical="true" handlerSize="2px" style="width:100%;height:100%;">
        <div size="40%" showCollapseButton="true">
                <div class="mini-fit">
                <div class="mini-panel" title="菜单列表" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true">
                        <div id="MemuGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" 
                            idField="deskId" pageSize="20" multiSelect="true" sortMode="client" showPager="false" allowAlternating="true">
                            <div property="columns" >
                                <div type="checkcolumn" field="deskId" width="10px"></div>
                                <div type="indexcolumn" field="deskId" width="10px">序号</div>
                                <div field="deskId" headerAlign="center"  allowSort="true" width="20px">工作台Id</div>
                                <div field="deskName" headerAlign="center"  allowSort="true"  width="30px">工作台名称</div>
                                <div field="branchId" headerAlign="center"  allowSort="true" width="20px">银行归属</div>
                                <div field="deskUrl" headerAlign="center" allowSort="true">URL地址</div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div showCollapseButton="true">
                <div class="mini-fit">
                <div class="mini-panel" title="菜单资源功能点" style="width:100%;height:100%;float:left;" showToolbar="true" showCloseButton="false"
                showFooter="false">
                <div id="treegrid1" class="mini-treegrid" expandOnLoad="true" showCheckBox="true"  treeColumn="text" idField="id" parentField="pid" 
                    resultAsTree="false"  autoCheckParent="false" checkRecursive="false" showTreeIcon="true" allowSelect="true" allowCellSelect="true" ondrawcell="ondrawcell" allowAlternating="true">
                    <div property="columns">
                        <div  type="indexcolumn" width="4%" headerAlign="center">序列</div>
                        <div field="deskId" name="deskId" width="7%" headerAlign="center">归属工作台</div>
                        <div name="text" field="text" width="30%" headerAlign="center">模块名称</div>
                        <div field="functions"width="69%" headerAlign="center">权限</div>
                        <div field="selectedAll" align='center' headerAlign="center" width='50px'>全选</div>
                    </div>
                </div>
            </div>
        </div>  
        </div>      
    </div>
</body>

<script type="text/javascript">
mini.parse();
var grid = mini.get("MemuGrid");
var treegrid1 = mini.get("treegrid1");
$(document).ready(function(){
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        deskMenu(pageSize, pageIndex);
    });
    deskMenu(20, 0);
    showInstAndRoleName();
    loadResourceTree();
})

//全选功能
function quanxuan(id) {
    var record = treegrid1.getRecord(id);
    if (record) {
        var funcs = record.attributes;
        if (funcs != null && funcs.length > 0) {
            for (var i = 0; i < funcs.length; i++) {
                var funcsId = funcs[i].functionId;
                checkFunc(id, funcsId, true);
                $("#" + funcsId).attr("checked", true);
            }
        }
    }
}
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
}
  
//角色ID 查询所拥有的台子  页面查询按钮
function haveDesk() {
    var paramObj = {};
    //paramObj.instId = GetQueryString("instId");
    paramObj.roleId = GetQueryString("roleId");
    CommonUtil.ajax({
        url: "/RoleController/getHaveDeskByRoleIdAndInstId",
        data: mini.encode(paramObj),
        callback: function (data) {
            //处理菜单选中
            grid.selects(data.obj, true);
        }
    });
}
//渲染机构名称 角色名称
function showInstAndRoleName() {
    // var instName=mini.get("instName");
    // instName.setValue(decodeURI(GetQueryString("instName")));
    var roleName = mini.get("roleName");
    roleName.setValue(decodeURI(GetQueryString("roleName")));
}

//根据银行Id 刷出改银行下所有的菜单列表
function deskMenu(pageSize, pageIndex) {
    var memuObj = {};
    memuObj.branchId = branchId;
    memuObj.pageSize = pageSize;
    memuObj.pageNumber = pageIndex + 1;
    if (branchId) {
        CommonUtil.ajax({
            url: "/deskMenuController/getAllMenuByBranchId",
            data: mini.encode(memuObj),
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
                haveDesk();
            }
        });
    }
}

    //默认加载资源树
function loadResourceTree() {
    var data = {};
    data.roleId = GetQueryString("roleId");
    data.branchId = branchId;
    var param = mini.encode(data);
    CommonUtil.ajax({
        url: "/RoleController/getAllResourceByBranchId",
        data: param,
        callback: function (data) {
            treegrid1.setData(data.obj);
            showHistory();
        }
    });
}
    //===加上这句话；可以渲染节点后再次操作树(不可少)====
function onBeforeNodeCheck(e) {
    var tree = e.sender;
    var node = e.node;
    if (tree.hasChildren(node)) {
    }
}
    
    
/*
*   此方法能不动就别动 如果修改--请查看数据结构再修改   ||  保存角色菜单，角色权限，权限功能点
*/
function save() {
    //获取菜单选中的内容
    var roleId = GetQueryString("roleId");
    var functionList = new Array();//功能点保存对象
    var deskMemuRecords = grid.getSelecteds();
    var deskMenuList = new Array();//保存台子对象
    var moduleList = new Array();//保存模块对象
    for (var i = 0; i < deskMemuRecords.length; i++) {
        var str = deskMemuRecords[i];
        var deskmenued = {};
        deskmenued.roleId = roleId;
        deskmenued.deskId = str.deskId;
        deskmenued.accessFlag = '1';
        deskmenued.deskUrl = str.deskUrl;
        deskMenuList.push(deskmenued);
    }
    var paramObj = {};
    paramObj.roleId = roleId;
    paramObj.branchId = branchId;
    paramObj.deskMemuSelected = deskMenuList;
    //获取选中的模块
    var resourceList = treegrid1.getValue().split(",");
    
    if (resourceList != null && resourceList.length > 0) {
        for (var i = 0; i < resourceList.length; i++) {
            var taRoleModule = {};
            taRoleModule.roleId = roleId;
            taRoleModule.moduleId = resourceList[i];
            taRoleModule.accessFlag = '1';
            moduleList.push(taRoleModule);
        }
    }
    paramObj.moduleSelected = moduleList;
    //获取选中的功能点
    var treeAllNote = treegrid1.getData();
    for (var i = 0; i < treeAllNote.length; i++) {
        var firstLayer = treeAllNote[i];
        if (firstLayer.children != null && firstLayer.children.length > 0) {
            var secondFloor = firstLayer.children;
            for (var j = 0; j < secondFloor.length; j++) {
                var secondFloorFunction = secondFloor[j].attributes;
                if (secondFloorFunction != null && secondFloorFunction.length > 0) {
                    for (var n = 0; n < secondFloorFunction.length; n++) {
                        if (secondFloorFunction[n].checked) {
                            var funtionPoint = {};
                            funtionPoint.roleId = roleId;
                            funtionPoint.moduleId =secondFloor[j].id;
                            funtionPoint.functionId = secondFloorFunction[n].functionId;
                            functionList.push(funtionPoint);
                        }
                    }
                }
                if (secondFloor != null && secondFloor.length > 0) {
                    var thirdLayer = secondFloor[j].children;//三级对象数据
                    if (thirdLayer != null && thirdLayer.length > 0) {
                        for (var k = 0; k < thirdLayer.length; k++) {
                            var attributesList = thirdLayer[k].attributes;
                            if (attributesList != null && attributesList.length > 0) {
                                for (var v = 0; v < attributesList.length; v++) {
                                    var functionNote = attributesList[v];
                                    if (functionNote.checked) {
                                        var funtionPoint = {};
                                        funtionPoint.roleId = roleId;
                                        funtionPoint.moduleId=thirdLayer[k].id;
                                        funtionPoint.functionId = functionNote.functionId;
                                        functionList.push(funtionPoint);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    paramObj.functionSelected = functionList;
    if (paramObj) {
        if (paramObj.deskMemuSelected != null && paramObj.deskMemuSelected.length > 0) {
            //发送ajax
            mini.confirm("您确定分配吗？", "温馨提示", function (action) {
                if (action == 'ok') {
                    CommonUtil.ajax({
                        url: "/RoleController/savaAllData",
                        data: mini.encode(paramObj),
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("分配成功","系统提示",function(){
                                    onCancel();
                                });
                            } else {
                                mini.alert("分配失败，请重试~","系统提示");
                            }
                        }
                    });
                }
            })
        } else {
            mini.alert("请先选择菜单","系统提示");
        }
    } else {
        mini.alert("请先选择菜单列表","系统提示");
    }
}

function onCancel(e) {
    window.CloseOwnerWindow();
}
function ondrawcell(e) {
    var tree = e.sender,
        record = e.record,
        column = e.column,
        field = e.field,
        id = record[tree.getIdField()],
        funs = record.attributes;
        var s = "<a class='mini-button blue' height=22  onclick='quanxuan(\"" + id + "\")' >全选</a>";
    function createCheckboxs(funs) {
        if (!funs) return "";
        var html = "";
        
        for (var i = 0, l = funs.length; i < l; i++) {
            var fn = funs[i];
            var clickFn = 'checkFunc(\'' + id + '\',\'' + fn.functionId + '\', this.checked)';
            var checked = fn.checked ? 'checked' : '';
            html += '<label class="function-item"><input onclick="' + clickFn + '" ' + checked + ' type="checkbox" name="'
                + fn.functionId + '" id='+fn.functionId+' hideFocus/>' + fn.functionName + '</label>';
        }
        return html;
    }
    if (field == 'functions') {
        e.cellHtml = createCheckboxs(funs);
    }
    if(field == 'selectedAll'){
        if(funs.length>0){
            e.cellHtml =s;
        }
       
    }
    
}
    //显示出该树下那些已经选中过
function showHistory() {
    CommonUtil.ajax({
        url: "/RoleController/searchModuleIdForRoleId",
        data: mini.encode({ roleId: GetQueryString("roleId") }),
        callback: function (data) {
            var list = new Array();
            $.each(data.obj, function (i, item) {
                list.push(item);
            });
            treegrid1.setValue(list);
        }
    });
}
//检查按钮
    function checkFunc(id, functionId, checked) {
        var record = treegrid1.getRecord(id);
        if (!record) return;
        var funs = record.attributes;
        if (!funs) return;
        function getAction(functionId) {
            for (var i = 0, l = funs.length; i < l; i++) {
                var o = funs[i];
                if (o.functionId == functionId) return o;
            }
        }
        var obj = getAction(functionId);
        if (!obj) return;
        obj.checked = checked;
    }
</script>
