<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>用户参数经办</title>   
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset id="fd2" class="mini-fieldset title">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<table id="search_form"   width="100%">
				<tr>
					<td><input id="p_code" name="p_code" class="mini-textbox"  labelField="true"  label="参数代码" emptyText="请输入参数代码" /></td>
					<td><input id="p_name" name="p_name" class="mini-textbox" labelField="true"  label="参数名称" emptyText="请输入参数名称" /></td>
					<td><input id="p_value" name="p_value" class="mini-textbox" labelField="true"  label="参数值" emptyText="请输入参数值" /></td>
				</tr>
				<tr>
					<td><input id="p_editabled" name="p_editabled" class="mini-combobox"  labelField="true"  label="可修改" emptyText="请选择"  data="CommonUtil.serverData.dictionary.YesNo"/></td> 
					<td><input id="p_type" name="p_type" class="mini-textbox" labelField="true"  label="参数类型" emptyText="请输入参数类型" /></td>
					<td></td>
				</tr>
			</table>
		</div>
		<div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
			<table style="width:100%;">
				<tr>
				<td style="width:100%;">
					<a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
					<a id="search_btn" class="mini-button" style="display: none"    onclick="search(10,0)">查询</a>
					<a id="submit_btn" class="mini-button" style="display: none"      onclick="submit()">提交复核</a>
				</td>
				</tr>
			</table>
		</div>
	</fieldset>
    <div id="tradeManage" class="mini-fit" style="margin-top: 2px;">  
        <div id="UserParamHandleManage" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="p_id"  allowResize="true" multiSelect="false"
        >
            <div property="columns" >
                <div type="indexcolumn"></div>
                <div field="p_id" >参数编号</div>    
                <div field="p_type"  renderer="paramBig" >参数大类</div>
                <div field="p_sub_type"   renderer="paramSmall" >参数小类</div>                                
                <div field="p_sub_name"  >参数小类描述</div>
                <div field="p_name"  >参数名称</div>
                <div field="p_value" >参数值</div>
                <div field="p_sort">排序</div>
                <div field="p_code" >参数子节点</div>
                <div field="p_parent_code">参数父节点</div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function(){
            mini.parse();
            var grid =mini.get("UserParamHandleManage");
		    grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex; 
                var pageSize = e.data.pageSize;
                search(pageSize,pageIndex);
		    });
            search(10,0);
        });

        //查询
        function search(pageSize,pageIndex){
            var data = {};
            data.pageNumber = pageIndex+1;
            data.pageSize = pageSize;
            var param = mini.encode(data);
            CommonUtil.ajax({
                url:"/UserParamController/taUserParamWaitingHandle/",
                data:param,
                callback:function(data){
                    var grid = mini.get("UserParamHandleManage");
                    //设置分页
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    //设置数据
					grid.setData(data.obj.rows);
                }

            });
        }

        //提交复核
        function submit(){
            var grid = mini.get("UserParamHandleManage");
            var row = grid.getSelected();
            if(row){
                CommonUtil.ajax({
                    url:"/UserParamController/submitVerify",
                    data:row,
                    callback:function(){
                        mini.alert("提交成功！");
                        search(10,0);
                    }
                });
            }else{
                mini.alert("请先选择一条记录！");
            }
        }

        //参数大类显示
        function paramBig(e){
            if("0" == e.value){
                return "系统参数";
            }else{
                return "用户参数";
            }
        }
        //参数小类显示
        function paramSmall(e){
            if("0" == e.value){
                return "资产计划类型";
            }else if("1" == e.value){
                return "系统配置类型";
            }else if("2" == e.value){
                return "债券授信类型";
            }else if("3" == e.value){
                return "存放同业活期账户";
            }else if("5" == e.value){
                return "风险评级";
            }else if("7" == e.value){
                return "风险资产系数";
            }
        }
    </script>
</body>
</html>