<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%> 
<html>
<head>
	<title></title>
</head>
<body >
	<!-- <fieldset class="mini-fieldset">
		<legend>股票价格信息</legend> -->
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">股票价格管理</a> >> 修改</span>
		<div  id="editForm"  class="fieldset-body">
	 		<table id="search_form"   width="100%" class="mini-table">
	 		 	<tr>
		            <td>
					<input id="stockId" name="stockId" class="mini-textbox" labelField="true" label="股票代码：" emptyText="请输入股票代码"   required="true"
					  maxLength="20"  onvalidation="CommonUtil.onValidation(e,'stockId','')" style="width:80%;"/>
		            </td>
					<td>
						<input  id="stockName" name="stockName" class="mini-textbox" style="width:80%;" labelField="true" label="股票名称：" emptyText="请输入股票名称" required="true"  vtype="maxLength:35"/>
					</td>
					<td>
						<input  id="price" name="price" class="mini-spinner" style="width:80%;" changeOnMousewheel="false" labelField="true"  label="股票价格：" maxValue="100000000000" format="n2" emptyText="请输入股票价格" required="true" />
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center;">
						<a id="save_btn" class="mini-button" style="display: none"    onclick="add">保存</a>
						<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close()">取消</a>
					</td>
				</tr>
			</table>
		</div>
	          <!-- </fieldset> -->
			 <!--  <div  style="float: right; margin: 10px">
                        <a class="mini-button" style="display: none"    onclick="add(10,0);">保存</a>
               	</div> -->



<script type="text/javascript">

	$(document).ready(function(){
		initform();
	});

	mini.parse();
	var url = window.location.search;
	var action = CommonUtil.getParam(url, "action");
	var param = top['stockManagerGrid'].getData(action);

	var form = new mini.Form("search_form");
	//关闭页面
	function close(){
		top["win"].closeMenuTab();
	}

	function initform() {
		if (action == "edit") {
			mini.get("stockId").setEnabled(false);
			form.setData(param);
		}else if(action=="add"){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>股票价格管理</a> >> 新增");
		}	else if(action=="detail"){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>股票价格管理</a> >> 详情");
			form.setData(param);
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);

		}
	}
    
	function add(){
		var form= new mini.Form("search_form");
		form.validate();
		if(form.isValid() == false){
			mini.alert("表单填写错误,请确认!","提示信息")
			return;
		}
		var data=form.getData();
		var saveUrl = $.inArray(action, ["add"]) > -1 ? "/StockController/addStock" : "/StockController/updateStock";
		var param = mini.encode(data); //序列化成JSON
		//验证股票代码的唯一性
		if ($.inArray(action, ["add"]) > -1) {
			CommonUtil.ajax({
				url: "/StockController/queryStockById",
				data: mini.encode({ "stockId": data.stockId }),
				callback: function (data) {
					if (data.obj == null) {
						CommonUtil.ajax({
							url: saveUrl,
							data: param,
							callback: function (data) {
								if ('error.common.0000' == data.code) {
									mini.alert("保存成功", "提示", function () {
										setTimeout(function () { top["win"].closeMenuTab() }, 10);
									});

								} else {
									mini.alert("保存失败");
								}
							}
						});

					} else {
						mini.alert("该股票代码已存在", "系统提示");
					}
				}
			});
		} else {
			CommonUtil.ajax({
				url: saveUrl,
				data: param,
				callback: function (data) {
					if ('error.common.0000' == data.code) {
						mini.alert("保存成功", "提示", function () {
							setTimeout(function () { top["win"].closeMenuTab() }, 10);
						});

					} else {
						mini.alert("保存失败");
					}
				}
			});
		}
}
</script>
</body>
</html>