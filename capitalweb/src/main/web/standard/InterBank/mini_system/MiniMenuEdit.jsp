<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <div id="MenuModule"> 
            <span id="labell"><a href="javascript:CommonUtil.activeTab();">菜单新增</a> >> 新增</span>
        <table cols='4' width='100%' class="mini-table">
            <tr>
                <td>
                    <input id="deskId" name="deskId" class="mini-textbox" labelField="true" label="菜单节点代码"  emptyText='菜单节点代码自动生成' enabled="false" required="true"  validateOnLeave="true" enable="true" />
                    <input id="moduleId" name="moduleId" class="mini-textbox" labelField="true" label="菜单节点代码" emptyText='菜单节点代码自动生成'  enabled="false" required="true"  validateOnLeave="true" enable="true" />
                </td>
                <td>
                    <input id="MenuType" name="MenuType" labelField="true" label="菜单类型" emptyText='请选择菜单类型'  class="mini-combobox" required="true"  showNullItem="请选择菜单类型" data=" [{ 'id': '0', 'text': '台子' }, { 'id': '1', 'text': '模块' }]"
                        valueFromSelect="true" />
                </td>
                <td>
                    <input id="topNodeType" name="topNodeType" labelField="true" label="节点挂靠"  emptyText='请选择节点挂靠' class="mini-combobox" required="true"  showNullItem="请选择菜单类型" data=" [{ 'id': '0', 'text': '台子' }, { 'id': '1', 'text': '模块' }]"
                        valueFromSelect="true" />
                </td>
                
            </tr>
            <tr>
                <td>
                    <input id="ModulePid" name="ModulePid" labelField="true" label="菜单父节点" emptyText='请选择菜单父节点'  class="mini-buttonedit" onbuttonclick="onButtonEdit" allowInput="false"
                    />
                </td>
                <td>
                    <input id="moduleName" name="moduleName" labelField="true" label="菜单名称"  emptyText='请输入菜单名称' class="mini-textbox" vtype="maxLength:10"  required="true"  requiredErrorText="菜单名不能为空"/>
                    <input id="deskName" name="deskName" labelField="true" label="菜单名称" emptyText='请输入菜单名称' class="mini-textbox" vtype="maxLength:10"  required="true"  requiredErrorText="菜单名不能为空"
                    />
                </td>
                <td>
                    <input id="IsActive" name="IsActive" labelField="true" label="是否启用" emptyText='请选择是否启用' class="mini-combobox"  required="true"  data="CommonUtil.serverData.dictionary.YesNo" />
                </td>
                
            </tr>
            <tr>
                <td>
                    <input id="deskUrl" name="deskUrl" labelField="true" label="菜单URl" emptyText='请输入菜单url' class="mini-textbox" required="true"  requiredErrorText="菜单名URL不能为空"
                    />
                    <input id="moduleUrl" name="moduleUrl" labelField="true" label="模块URl" emptyText='请输入模块url' class="mini-textbox" required="true"  requiredErrorText="模块URL不能为空"
                    />
                </td>
                <td>
                    
                </td>
                <td>

                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:center;">
                    <a class="mini-button" style="display: none"    id="save"  onClick="save();">保存</a>
                </td>
            </tr>
        </table>
    </div>

<script>
    mini.parse();
    $(document).ready(function () {
        var MenuType=mini.get("MenuType");
        var topNodeType =mini.get("topNodeType");
        var ModulePid =mini.get("ModulePid");
        topNodeType.setEnabled(false);
        if(GetQueryString("deskId")){
            //渲染菜单数据
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>菜单新增</a> >> 修改");
            mini.get("moduleName").setVisible(false);
            mini.get("moduleUrl").setVisible(false);
            deskEdit(GetQueryString("deskId"));

        }else if(GetQueryString("moduleId")){
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>菜单新增</a> >> 修改");
            mini.get("deskName").setVisible(false);
            mini.get("deskUrl").setVisible(false);
            moduleEdit(GetQueryString("moduleId"));
        }else{
            mini.get("moduleId").setVisible(false);
            mini.get("moduleName").setVisible(false);
            mini.get("moduleUrl").setVisible(false);
        }
        MenuType.on("valuechanged", function (e) {
            if(MenuType.getValue()=='0'){
                topNodeType.setEnabled(false);
                mini.get("deskUrl").setValue("./menu.jsp");
               
            }else{
                topNodeType.setEnabled(true);
                mini.get("deskUrl").setValue();
            }
        })
        topNodeType.on("valuechanged",function(e){
            if(topNodeType.getValue()=='0'){
                mini.get("deskUrl").setEnabled(false);
            }else{
                mini.get("deskUrl").setEnabled(true);
            }
        });
        ModulePid.on("valuechanged",function(e){
            var moduleId=ModulePid.getValue();
            checkMene(moduleId);
        });


    });
    //渲染菜单数据
    function deskEdit(deskId) {
            if (deskId) {
                //获取台子信息
                CommonUtil.ajax({
                    url: "/deskMenuController/getDeskMenuByBranchIdAndDeskId",
                    data: mini.encode({ "deskId": deskId, "branchId": branchId }),
                    callback: function (data) {
                        var MenuModule = new mini.Form("MenuModule");
                        MenuModule.setData(data.obj);
                        mini.get("moduleId").setVisible(false);
                        mini.get("IsActive").setValue(data.obj.isActive);
                        mini.get("MenuType").setValue(0);
                        mini.get("MenuType").setEnabled(false);
                        mini.get("ModulePid").setEnabled(false);
                    }
                });
            }
        }
    //渲染module数据
    function moduleEdit(moduleId) {
            if (moduleId) {
                //获取module数据
                CommonUtil.ajax({
                    url: "/ModuleController/getModuleByBranchIdAndModuleId",
                    data: mini.encode({ "moduleId": moduleId, "branchId": branchId }),
                    callback: function (data) {
                        var MenuModule = new mini.Form("MenuModule");
                        MenuModule.setData(data.obj);
                        mini.get("IsActive").setValue(data.obj.isActive);
                        mini.get("deskId").setVisible(false);
                        mini.get("ModulePid").setEnabled(false);
                        mini.get("MenuType").setValue(1);
                        mini.get("MenuType").setEnabled(false);
                        mini.get("deskId").setEnabled(false);
                    }
                });
            }
        }

    function GetQueryString(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if (r != null)
                return unescape(r[2]);
            return null;
        }
    function checkMene(moduleId){
        var c = "-";
        var regex = new RegExp(c, 'g');
        var result = moduleId.match(regex);
        if(result.length>=2){
            mini.alert("只能挂两级");
            mini.get("ModulePid").setText();
            mini.get("ModulePid").setValue();
        }
    }

    //菜单父节点选择
    function onButtonEdit(e){
        var btnEdit = this;
        var MenuType =mini.get("MenuType");
        var topNodeType =mini.get("topNodeType");
        if(MenuType.getValue() !=null && MenuType.getValue() !=''){
                var url="";
                var topNodeTypeValue=topNodeType.getValue();
            if(MenuType.getValue()==1){
                url=CommonUtil.baseWebPath() + "/mini_system/MiniShowMenuType.jsp?action="+topNodeTypeValue;
                mini.open({
                url:url,
                title: "菜单的选择",
                width: 700,
                height: 600,
                ondestroy: function (action) {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        if (topNodeTypeValue == '1') {
                            btnEdit.setValue(data.id);
                            btnEdit.setText(data.text);
                            var menuType = mini.get("MenuType");
                            menuType.setReadOnly(true);
                            topNodeType.setReadOnly(true);
                            mini.get("ModulePid").doValueChanged();

                        } else {
                            btnEdit.setValue(data.deskId);
                            btnEdit.setText(data.deskName);
                            var menuType = mini.get("MenuType");
                            menuType.setReadOnly(true);
                            topNodeType.setReadOnly(true);
                        }
                    }
                }
            });
            }
        }else{
            mini.alert("请选择菜单类型");
        }
    }
    //保存数据
    function save(){
        var form = new mini.Form("MenuModule");
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        var data = form.getData();
        data.branchId =branchId;
        var url='';
        if(GetQueryString("deskId") || GetQueryString("moduleId")){//修改的URL判断
            if(GetQueryString("deskId")){
                url="/deskMenuController/updateDeskBranchId";
            }else{
                url="/ModuleController/updateModuleBranchId";
            }
        }else{//保存的URL 判断
            if (data.MenuType == 1) {
                url = '/ModuleController/saveModuleByBranchId';
            } else {
                url = '/deskMenuController/saveDeskByBranchId';
        }
        }
        mini.confirm("您确定要保存吗？", "温馨提示", function (action) {
            if (action == 'ok') {
                CommonUtil.ajax({
                    url: url,
                    data: mini.encode(data),
                    callback: function (data) {
                        if (data.code == 'error.common.0000') {
                            mini.alert("操作成功","系统提示",function(){
                                setTimeout(function(){top["win"].closeMenuTab()},10);
                            });
                        } else {
                            mini.alert("操作失败","系统提示");
                        }
                    }
                });
            }
        });
    }
</script>
