<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp" %>
<html>
<head>
    <title>机构清算信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset id="fd2" class="mini-fieldset title">
    <legend>查询</legend>
    <div id="search_form">
        <input id="instId" name="instId" class="mini-textbox" labelField="true" label="机构代码："
               labelStyle="text-align:right;" emptyText="请输入机构代码"/>
        <input id="instFullname" name="instFullname" class="mini-textbox" labelField="true" label="机构名称："
               labelStyle="text-align:right;" emptyText="请输入机构名称"/>
        <input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>"/>
        <span style="float:right;margin-right: 150px">
                    <a class="mini-button" style="display: none" id="search_btn" onclick="query()">查询</a>
                    <a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
            </span>
    </div>
</fieldset>
<span style="margin:2px;display: block;">
                <a class="mini-button" style="display: none" id="add_btn" name="add_btn" onclick="add();">新增</a>
                <a class="mini-button" style="display: none" id="edit_btn" name="edit_btn" onclick="edit">修改</a>
                <a class="mini-button" style="display: none" id="delete_btn" name="delete_btn" onclick="del();">删除</a>
        </span>
<div id="InsitutionSettles" class="mini-fit">
    <div id="InsitutionSettlesManagerGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         idField="instId"
         sortMode="client" allowAlternating="true" onrowdblclick="onRowDblClick">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
            <div field="instId" width="100" headerAlign="center" align="center">机构代码</div>
            <div field="instFullname" width="180" headerAlign="center">机构全称</div>
            <div field="busiType" width="80" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'busiType'}">业务类别
            </div>
            <div field="instAcctNm" width="180" headerAlign="center">账户名</div>
            <div field="instAcctNo" width="100" headerAlign="center" align="center">账户号</div>
            <div field="mediumType" width="80" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'mediumType'}">账号类型
            </div>
            <div field="instAcctBknm" width="180" headerAlign="center">开户行名</div>
            <div field="instAcctPbocNo" width="100" headerAlign="center" align="center">开户行号</div>
            <div field="instRemark" width="100" headerAlign="center">备注</div>
            <div field="isActive" width="60" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'YesNo'}">是否启用
            </div>
            <div field="lastUpdate" width="150" headerAlign="center" align="center">更新时间</div>
            <div field="userId" width="100" headerAlign="center">操作用户</div>
            <div field="userInstName" width="150" headerAlign="center">操作用户所属机构</div>
            <div field="ccy" width="60" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Currency'}">币种代码
            </div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var form = new mini.Form("#search_form");
    var grid = mini.get("InsitutionSettlesManagerGrid");
    top['InsitutionSettles'] = window;
    var row = "";
    //分页
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        ////console.log("起始页="+pageIndex);
        var pageSize = e.data.pageSize;
        ////console.log("每页大小="+pageSize);
        search(pageSize, pageIndex);
    });

    //查询
    function query() {
        search(grid.pageSize, 0);
    }

    //清空
    function clear() {
        form.clear();
    }

    //新增
    function add() {
        var url = CommonUtil.baseWebPath() + "/mini_system/MiniInsitutionSettlesEdits.jsp?action=add";
        var tab = {
            id: "InsitutionSettlesAdd",
            name: "InsitutionSettlesAdd",
            text: "清算新增",
            url: url,
            parentId: top["win"].tabs.getActiveTab().name
        };
        top['win'].showTab(tab);

    }

    //修改
    function edit() {
        // var grid = mini.get("stockManagerGrid");
        var record = grid.getSelected();
        if (record) {
            var url = CommonUtil.baseWebPath() + "/mini_system/MiniInsitutionSettlesEdits.jsp?action=edit";
            var tab = {
                id: "InsitutionSettlesEdits",
                name: "InsitutionSettlesEdits",
                text: "清算修改",
                url: url,
                parentId: top['win'].tabs.getActiveTab().name
            };

            top['win'].showTab(tab);
        } else {
            mini.alert("请选中一条记录", "系统提示");
        }
    }

    //详情
    function onRowDblClick() {
        var url = CommonUtil.baseWebPath() + "/mini_system/MiniInsitutionSettlesEdits.jsp?action=detail";
        var tab = {
            id: "InsitutionSettlesDetail",
            name: "InsitutionSettlesDetail",
            text: "机构清算详情",
            url: url,
            parentId: top["win"].tabs.getActiveTab().name
        };
        top['win'].showTab(tab);
    }


    //参数传递
    function getData(action) {
        if ($.inArray(action, ['detail', 'edit']) > -1) {
            row = grid.getSelected();
        }
        return row;
    }

    //删除
    function del() {
        var row = grid.getSelected();
        var param = {};
        if (row) {
            param.settlId = row.settlId;
            var settlId = mini.encode(param);
            mini.confirm("您确定要删除吗？", "温馨提示", function (action) {
                if (action == 'ok') {
                    CommonUtil.ajax({
                        url: "/InstitutionSettlsController/deleteSettl",
                        data: settlId,
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("操作成功");
                                search(10, 0);
                            } else {
                                mini.alert("操作失败");

                            }

                        }
                    });
                }
            })
        } else {
            mini.alert("请先选择一条数据操作", "操作提示");

        }
    }

    function search(pageSize, pageIndex) {
        form.validate();
        if (form.isValid == false) return;
        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        //银行id查询
        data['branchId'] = branchId;
        var param = mini.encode(data);
        CommonUtil.ajax({
            url: "/InstitutionSettlsController/settlsList",
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    $(document).ready(function () {
        search(10, 0);
    });
</script>

</body>
</html>