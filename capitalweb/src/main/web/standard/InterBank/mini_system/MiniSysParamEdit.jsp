﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 	
</head>
<body>

	<div id="fd2" >
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">系统参数</a> >> 修改</span>
		<div id="field" class="fieldset-body">
			<table id="sysParam_form"   width="100%" class="mini-table">
				
				<tr>
					<td>
						<input id="p_code" name="p_code" class="mini-textbox"
						labelField="true"  label="参数代码"  emptyText='请输入参数代码' vtype="maxLength:30;float;"
						width="300px" enabled="false"/>
					</td>
					<td>
						<input id="p_name"  name="p_name" class="mini-textbox"
						labelField="true"  label="参数名称"  emptyText='请输入参数名称' vtype="maxLength:100"
						width="300px"/>
					</td>
					<td>
						<input id="p_value"  name="p_value" class="mini-textbox"
						labelField="true"  label="参数值"  emptyText='请输入参数值' vtype="maxLength:100"
						width="300px" enabled="false"/>
					</td>
				</tr>
				<tr>
					
					<td>
						<input id="p_type"  name="p_type" class="mini-textbox"
						labelField="true"  label="参数类型"  emptyText='请输入参数类型' vtype="maxLength:15"
						width="300px"/>
					</td>
					<td>
						<input id="p_type_name"  name="p_type_name" class="mini-textbox"
						labelField="true"  label="参数类型名称"  emptyText='请输入参数类型名称' 
						vtype="maxLength:25" width="300px"/>
					</td>
					<td>
						<input id="p_editabled" name="p_editabled" class="mini-combobox" 
						data="CommonUtil.serverData.dictionary.YesNo" width="300px"
						labelField="true"  label="可修改"  emptyText='请选择是否修改'/>
					</td>
				</tr>
				
				<tr>
					<td>
						<input id="p_prop1" name="p_prop1" class="mini-textbox"
						labelField="true"  label="扩展属性1"  emptyText='请输入扩展属性1' 
						vtype="maxLength:100" width="300px"/>
					</td>
					<td>
						<input id="p_prop2" name="p_prop2" class="mini-textbox"
						labelField="true"  label="扩展属性2"  emptyText='请输入扩展属性2' 
						vtype="maxLength:100" width="300px"/>
					</td>
					<td>
						<textarea  id="p_memo" name="p_memo" class="mini-textarea"
						labelField="true"  label="备注"  emptyText='请输入备注' vtype="maxLength:100"
						width="300px">
						</textarea>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center;">
						<a id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
						<a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
					</td>
				</tr>
				
			</table>
		</div>
	</div>
  

<script type="text/javascript">
	mini.parse();
	$(document).ready(function(){
		init();
	});

	var url = window.location.search;
    var action = CommonUtil.getParam(url,"action");
	
	function init(){
		if(action == "new"){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>系统参数</a> >> 新增");
			
		}else if(action == "edit" || action == "detail"){
			var form1 = new mini.Form("sysParam_form");
			var data=top["systemParamManage"].getData();
			form1.setData(data);
			
			if(action == "detail"){
				$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>系统参数</a> >> 详情");
				form1.setEnabled(false);
				document.getElementById("save_btn").style.display="none";
				//document.getElementById("cancel_btn").style.display="none";
				
			}
		}
	}

	//按钮事件：保存
	function save(){
		var form = new mini.Form("sysParam_form");
		form.validate();
		if(form.isValid() == false){//表单验证
			mini.alert("请输入有效数据！","系统提示");
			return;
        } 
		var data = form.getData();
		var param = mini.encode(data); //序列化成JSON
		
		CommonUtil.ajax({
		   url:"/SysParamController/editTaSysParam",
		   data:param,
		   callback:function(data){
			   	if('error.common.0000' == data.code){
					mini.alert("保存成功!","系统提示",function(value){
						 //关闭并刷新
						 setTimeout(function(){ top["win"].closeMenuTab() },100);
					});
					
				}else{
					mini.alert("保存失败！","系统提示");
                }	
				
		   }
	   });

	}
	//取消按钮
	function cancel(){
        window.CloseOwnerWindow();

    }
	


</script>

</body>
</html>