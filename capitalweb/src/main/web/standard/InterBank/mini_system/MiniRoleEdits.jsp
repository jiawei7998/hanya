<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>

<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<div id="RoleEdit_div">
			<span id="labell"><a href="javascript:CommonUtil.activeTab();">角色新增</a> >> 新增</span>
	<table id="role_form" width="100%" class="mini-table" >
		<tr>
			<td>
				<input id="roleName" name="roleName" class="mini-textbox" labelField="true" label="角色名称" emptyText="请输入角色名称" vtype="maxLength:25"
				 required="true"  />
			</td>
			<td>
				<input id="roleId" name="roleId" class="mini-textbox" labelField="true" label="角色代码" emptyText="自动生成" allowInput="false" />
			</td>
			<td>
				<input id="instId" name="instId" valueField="id" labelField="true" label="所属机构" showFolderCheckBox="true" showCheckBox="true"
				 showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId" class="mini-treeselect" resultAsTree="false"
				 valueFromSelect="true" multiSelect="true" emptyText="请选择机构" required="true" />
			</td>
		</tr>
		<tr>
			<td>
				<input id="roleFlag" name="roleFlag" class="mini-combobox" labelField="true" label="角色类型" required="true"  emptyText="请选择角色类型" data="CommonUtil.serverData.dictionary.RoleType"
				/>
			</td>
			<td>
				<select id="isActive" name="isActive" class="mini-combobox" labelField="true" label="是否启用" required="true"  emptyText="请选择启用状态" data="CommonUtil.serverData.dictionary.YesNo"></select>
			</td>
			
			<td>
				<input id="roleLevel" name="roleLevel" class="mini-combobox" labelField="true" label="角色级别" required="true"  emptyText="请选择角色级别" data="CommonUtil.serverData.dictionary.roleLevel"
				/>
			</td>
		</tr>
		<tr>
			<td>
				<input id="indexPage" name="indexPage" class="mini-textbox" labelField="true" label="首页类型" emptyText="请输入首页类型" value="index_approve.jsp" vtype="maxLength:16"
				/>
			</td>
			<td>
				<input id="roleMemo" name="roleMemo" labelField="true" label="备注" emptyText="请输入备注" class="mini-textbox" vtype="maxLength:50"
				/>
			</td>
			<td>
			
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:center;">
					<a id="save_btn" class="mini-button" style="display: none"    onclick="save();">保存</a>
					<a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
			</td>
		</tr>
	</table>
</div>
<script>
mini.parse();
var grid = mini.get("instId");
mini.get("indexPage").setEnabled();
var url = window.location.search;
var action = CommonUtil.getParam(url, "action");
var roleEditId='';

$(document).ready(function(){
	if(action=='edit'){
		$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>角色新增</a> >> 修改");
		mini.get("roleId").setEnabled(false);
		mini.get("roleLevel").setEnabled(false);
		mini.get("roleFlag").setEnabled(false);
		init();
	}
	mini.get("roleId").setEnabled(false);
	institution();
})
//修改
function init(){
	var form =new mini.Form("role_form");
	var record=top["roleManage"].getData();
	roleEditId =record.roleId;
	form.setData(record);
}
//加载机构树
function institution() {
	var instHaving='';
	if(action=='edit'){//根据角色Id查询该角色下的机构
				CommonUtil.ajax({
					url:"/RoleController/searchInstIdByRoleId",
					data:mini.encode({"roleId":roleEditId}),
					callback: function (data) {
						for(var i=0;i<data.obj.length;i++){
							instHaving=instHaving+data.obj[i]+","
						}
					}
				})
			}
	var param = mini.encode({branchId:branchId}); //序列化成JSON
	CommonUtil.ajax({
		url: "/InstitutionController/searchInstitutionByBranchId",
		data: param,
		callback: function (data) {
			grid.setData(data.obj);
			grid.setValue(instHaving);
		}
	});
}
//保存操作
function save() {
	var form = new mini.Form("RoleEdit_div");
	form.validate();
	if (form.isValid() == false) return;
	var paramObj=form.getData();
	paramObj.branchId=branchId;
	var param = mini.encode(paramObj);
	var url='';
	if(action=='edit'){
		url='/RoleController/updateRole';
	}else{
		url='/RoleController/saveRole';
	}
	CommonUtil.ajax({
		url: url,
		data: param,
		callback: function (data) {
			if (data.code == 'error.common.0000') {
				mini.alert("操作成功","系统提示",function(){setTimeout(function(){
					top["win"].closeMenuTab()
				},10);
					
				});
			} else {
				mini.alert("操作失败","系统提示");
			}
		}
	});
}
//取消按钮
function cancel(){
	window.CloseOwnerWindow();
}
</script>
	