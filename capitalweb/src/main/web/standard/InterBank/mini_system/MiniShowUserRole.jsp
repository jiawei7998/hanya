<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
<script type="text/javascript">
var userObj = {};
$(document).ready(function(){
    mini.parse();
    ShowRoleData();
    
});
function ShowRoleData() {
    if ( GetQueryString("action")== "edit") {
        var listbox2 = mini.get("listbox2");
        listbox2.removeAll();
        userObj.userId =GetQueryString("userId");
        userObj.userName = GetQueryString("userName");
        var param = {};
        param.adminUserId = userId;
        param.userId = GetQueryString("userId");//用户Id
        param.branchId =branchId;
        if (param) {
            //加载权限分配listBox  根据银行号进行查询
            CommonUtil.ajax({
                url: '/RoleController/searchAllRole',
                data: mini.encode(param),
                callback: function (data) {
                    var userRoleList = data.obj.right;
                    var grid = mini.get("listbox1");
                    grid.setData(data.obj.left);
                    $.each(userRoleList, function (i, n) {
                        grid.select(n.roleId);
                        add();
                    });
                }
            });
        }
    }
}
function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
}
function add() {
    var listbox1 = mini.get("listbox1");
    var listbox2 = mini.get("listbox2");
    var items = listbox1.getSelecteds();
    listbox1.removeItems(items);
    listbox2.addItems(items);
}


//向右移动全部（listbox1==》listbox2） 
function addAll() {
    var listbox1 = mini.get("listbox1");
    var listbox2 = mini.get("listbox2");
    var items = listbox1.getData();
    listbox1.removeItems(items);
    listbox2.addItems(items);
}
//向左移动一个（listbox1《==listbox2） 
function removes() {
    var listbox1 = mini.get("listbox1");
    var listbox2 = mini.get("listbox2");
    var items = listbox2.getSelecteds();
    if(items !=null && items.length>0){
        listbox2.removeItems(items);
        listbox1.addItems(items);
    }else{
        mini.alert("请选择一个角色移除","系统提示");
    }
    
}
//向左移动全部（listbox1《==listbox2） 
function removeAll() {
    var listbox1 = mini.get("listbox1");
    var listbox2 = mini.get("listbox2");
    var items = listbox2.getData();
    listbox2.removeItems(items);
    listbox1.addItems(items);
    
}

var RoleType =CommonUtil.serverData.dictionary.RoleType;
function onRoleType(e) {
    for (var i = 0, l = RoleType.length; i < l; i++) {
        var g = RoleType[i];
        if (g.id == e.value) return g.text;
    }
    return "";
}
//角色级别字典
var roleLevel =CommonUtil.serverData.dictionary.roleLevel;
//渲染级别字典
function onroleLevelRenderer(e) {
    for (var i = 0, l = roleLevel.length; i < l; i++) {
        var g = roleLevel[i];
        if (g.id == e.value) return g.text;
    }
    return "";
}
//机构字典
var InstitutionType =CommonUtil.serverData.dictionary.InstitutionType;
//渲染机构字段
function onInstitutionTypeRenderer(e) {
    for (var i = 0, l = InstitutionType.length; i < l; i++) {
        var g = InstitutionType[i];
        if (g.id == e.value) return g.text;
    }
    return "";
}

function save() {
    var saveObj = {};
    //获取用户信息
    if (userObj.userId != '') {
        saveObj.userId = userObj.userId;
        var listbox2 = mini.get("listbox2");
        if (listbox2.getData().length != 0) {
            var recordList = listbox2.getData();
            var roleLsit=new Array();
            $.each(recordList, function (i, n) {
                roleLsit.push(n.roleId);
            });
            var uniqueList=unique5(roleLsit);

            var str = "";
            $.each(uniqueList, function (i, n) {
                str += n + ",";
            });
            saveObj.roleId = str;
            //发送保存操作
            var param = mini.encode(saveObj);
            mini.confirm("您确定要保存<span style='color:#FF3333;font-weight:bold;'>" + userObj.userName + "</span>权限吗?", "系统提示", function (r) {
                if (r == 'ok') {
                    CommonUtil.ajax({
                        url: '/UserController/saveUserRole',
                        data: param,
                        callback: function (data) {
                            if ("error.common.0000" == data.code) {
                                mini.alert("操作成功","系统提示",function(){
                                    onCancel();
                                });
                            } else {
                                mini.alert("操作失败","系统提示");
                            }
                        }
                    });
                }
            });
        } else {
            mini.alert("请选择一个权限","系统提示");
        }
    } else {
        mini.alert("请选择用户进行分配权限","系统提示");
    }
}
//根据机构Id 角色名称搜索角色内容
function search() {
    var form = new mini.Form("RoleGrid");
    form.validate();
    if (form.isValid() == false) {
        return;
    }
    var data = form.getData();
    data.userId = userObj.userId;//用户Id
    data.branchId =branchId;
    var param = mini.encode(data); //序列化成JSON
    CommonUtil.ajax({
        url: "/RoleController/getRolesByInstAndRoleName",
        data: param,
        callback: function (data) {
           var grid = mini.get("listbox1");
            grid.setData(data.obj.rows);
        }
    });
}

function onInsQuery(e) {
    var btnEdit = this;
    mini.open({
        url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
        title : "机构选择",
        width : 900,
        height : 600,
        ondestroy : function(action) {	
            if (action == "ok") 
            {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data); //必须
                if (data) {
                    btnEdit.setValue(data.instId);
                    btnEdit.setText(data.instName);
                    btnEdit.focus();
                }
            }
        }
    });
}
function onCancel(e) {
    window.CloseOwnerWindow();
}
//去重
function unique5(array){ 
var r = []; 
for(var i = 0, l = array.length; i < l; i++) { 
 for(var j = i + 1; j < l; j++) 
  if (array[i] === array[j]) j = ++i; 
 r.push(array[i]); 
 } 
 return r; 
}
</script>
<body style="width:100%;height:100%;background:white">
<div id="RoleGrid" class="mini-toolbar">
    <table>
        <tr>
            <td>
                <fieldset class="mini-fieldset" style="width:400px;height:300px;float:left">
                    <legend>未分配角色</legend>
                    <div class="mini-fit">
                        <div id="listbox1" class="mini-listbox" textField="roleName" valueField="roleId" style="width:100%;height:100%;" showCheckBox="true"
                            multiSelect="true">
                            <div property="columns">
                                <div type="indexcolumn" headerAlign="center">序列</div>
                                <div field="roleId" header="角色编号"></div>
                                <div field="roleName" header="角色名称"></div>
                                <div field="roleFlag" renderer="onRoleType" header="角色类别"></div>
                                <div field="instType" renderer="onInstitutionTypeRenderer" header="机构类型"></div>
                                <div field="roleLevel" renderer="onroleLevelRenderer" header="角色级别"></div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </td>
            <td>
                <fieldset class="mini-fieldset" style="width:400px;height:300px;float:left">
                    <legend>已分配角色</legend>
                    <div class="mini-fit">
                        <div id="listbox2" class="mini-listbox" style="width:100%;height:100%;" textField="roleName" valueField="roleId" showCheckBox="true"
                            multiSelect="true">
                            <div property="columns">
                                <div field="roleId" header="角色编号"></div>
                                <div field="roleName" header="角色名称"></div>
                                <div field="roleFlag" renderer="onRoleType" header="角色类别"></div>
                                <div field="instType" renderer="onInstitutionTypeRenderer" header="机构类型"></div>
                                <div field="roleLevel" renderer="onroleLevelRenderer" header="角色级别"></div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </td>
        </tr>
    </table>
</div>
</body>