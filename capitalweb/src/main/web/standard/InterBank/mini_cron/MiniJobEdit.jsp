<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
		<span id="labell"><a href="javascript:CommonUtil.activeTab();">定时任务</a> >> 修改</span>
<fieldset class="mini-fieldset">
	<legend>定时任务详情</legend>
<div>
	<table id="job_form" class="mini-table" style="width:100%">
		<tr id="pk">
			<td class="red-star">任务编号</td>
			<td><input id="jobId" name="jobId" class="mini-textbox"   vtype="range:1,999999" required="true"  /></td>
			<td class="red-star">任务名称</td>
			<td><input id="jobName" name="jobName" class="mini-textbox" vtype="maxLength:100" required="true"  /></td>
			<td class="red-star">时间表</td>
			<td><input id="schedule" name="schedule" class="mini-textbox" vtype="maxLength:30" required="true"  /></td>
		</tr>
		<tr>
			<td class="red-star">执行类列表</td>
			<td><input id="classList" name="classList" class="mini-textbox" vtype="maxLength:4000" required="true"  /></td>
			<td>执行类参数</td>
			<td><input id="parameters" name="parameters" class="mini-textbox" vtype="maxLength:4000" /></td>	
			<td class="red-star">是否停用</td>
			<td><input id="isDisabled" name="isDisabled" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="0" showNullItem="false"  allowInput="false" emptyText="请选择..."/></td>
				
		</tr>
		<tr>
			<td>是否终止</td>
			<td><input id="isKilled" name="isKilled" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="0" showNullItem="false"  allowInput="false" emptyText="请选择..." /></td>
			<td class="red-star">是否立即启动</td>
			<td><input id="isStartup" name="isStartup" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="0" showNullItem="false"  allowInput="false" emptyText="请选择..." /></td>
			<td class="red-star">是否单例执行</td>
			<td><input id="isSingleton" name="isSingleton" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="1" showNullItem="false"  allowInput="false" emptyText="请选择..." /></td>
		</tr>
		<tr>
			<td class="red-star">是否允许手动执行</td>
			<td><input id="isManual" name="isManual" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="1" showNullItem="false"  allowInput="false" emptyText="请选择..." /></td>
			<td class="red-star">是否允许关闭</td>
			<td><input id="isDisable" name="isDisable" class="mini-combobox" required="true"  data = "CommonUtil.serverData.dictionary.YesNo" value="1" showNullItem="false"  allowInput="false" emptyText="请选择..." /></td>
		</tr>
	</table>
</div>
</fieldset>
<div  class="mini-toolbar" style="text-align:center" >
	<a onclick="save()" id="save_btn" class="mini-button" style="display: none"   >保存</a>
	<a onclick="colse()" id="close_btn" class="mini-button" style="display: none"   >关闭</a>
</div>
<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
	var row=top['miniJobManage'].getData();
	var saveurl="";
	//表单赋值
	function initData(){
		if(action=="add"){
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>定时任务</a> >> 新增");
			saveurl="/JobController/addJob";
		}else{
			$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>定时任务</a> >> 详情");
			saveurl="/JobController/editJob";
			var form=new mini.Form("#job_form");
			form.setData(row);
		}
	}
	//保存
	function save(){
		var form=new mini.Form("#job_form");
		form.validate();
		if(form.isValid==false) return ;
		var data=form.getData();
		params =mini.encode(data);
		CommonUtil.ajax({
			url:saveurl,
			data:params,
			callback:function(data){
				top["win"].closeMenuTab();
				mini.alert("保存成功.","提示");
			}
		});
	}
	//关闭表单
	function colse(){
		top["win"].closeMenuTab();
	}
	$(document).ready(function(){
		initData();
	})
</script>
</body>
</html>