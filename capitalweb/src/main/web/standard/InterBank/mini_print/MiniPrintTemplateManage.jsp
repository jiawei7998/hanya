<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<title>打印模板管理</title>
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="find" class="fieldset-body">
			<div id="search_form"  >
                <input id="templateName" name="templateName" class="mini-textbox" width="320px"
                    labelField="true"  label="模板名称：" labelStyle="text-align:right;"
                    emptyText="请输入模板名称" />
                <span style="margin-left:10px;">
                    <a id="search_btn" class="mini-button" style="display: none"    onclick="search(10,0)">查询</a>
                    <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
                </span>
            </div>
            <span style="margin:2px;display: block;">
        <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改</a>
<%--        <a id="add_btn" class="mini-button" style="display: none"    onclick="add()">上传模板</a>--%>
        <a id="down_btn" class="mini-button" style="display: none"     onclick="down()">下载模板</a>
    </span>
		</div>
	</fieldset>

	
		
	<div id="tradeManage" class="mini-fit" style="margin-top: 2px;">	
        <div id="print_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
        idField="tempCode"  pageSize="10" sortMode="client" allowAlternating="true"
        onRowdblclick="custDetail">
            <div property="columns" >
                <div type="indexcolumn" headerAlign="center"  align="center">序号</div>
                <div field="tempCode" width="80" headerAlign="center"  align="center" allowSort="true" dataType="int">模板编号</div>    
                <div field="templateName" width="150"  headerAlign="center"  >模板名称</div>                            
                <div field="beanName" width="230"    headerAlign="center">数据加载类bean名</div>
                <div field="tempDesc"  width="280" headerAlign="center">描述</div>                                
                                                                                                        
            </div>
        </div>
    </div>
    <script>

        mini.parse();
    $(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            var grid = mini.get("print_grid");
            grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                search(pageSize, pageIndex);
            });
            search(grid.pageSize, 0);
        });
	});

    //查询(带分页)
    function search(pageSize,pageIndex){
        var form =new mini.Form("search_form");
        form.validate();
        if (form.isValid() == false) return;//表单验证
        var data =form.getData();//获取表单数据
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url:"/PrintController/searchPrintTemplatePage",
            data:param,
            callback:function(data){
                    
                var grid =mini.get("print_grid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear(){
        var grid =mini.get("print_grid");
        mini.get("templateName").setValue("");
        search(grid.pageSize,0);
        
    }


    top["printTemplateManage"]=window;
    function getData(){
        var grid = mini.get("print_grid");
        var record =grid.getSelected(); 
        return record;
    }

    //修改
    function modifyedit(){
        var grid = mini.get("print_grid");
        var  record =grid.getSelected();
        if(record){
            var url =  '<%=basePath%>'+ "/standard/InterBank/mini_print/MiniPrintTemplateEdit.jsp?action=edit";
            var tab = { id: "printTemplateManageEdit", name: "printTemplateManageEdit", text: "打印模板修改", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
            top['win'].showTab(tab);
        }else{
            mini.alert("请选中一条记录","系统提示"); 
        }
    }

    //新增
    function add() {
        var url = '<%=basePath%>'+ "/standard/InterBank/mini_print/MiniPrintTemplateEdit.jsp?action=new";
        var tab = { id: "printTemplateManageEdit", name: "printTemplateManageEdit", text: "打印模板新增", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
        top['win'].showTab(tab);
    }
    //下载
    function down(){
        var grid = mini.get("print_grid");
        var record =grid.getSelected();
        if(!record){
            mini.alert("请选择要下载的模板!","系统提示");
            return;
        } 
        window.location.href = '<%=basePath%>/sl/PrintController/downloadTemplate/'+record.tempCode;
    }

    //双击一行打开详情信息
    function custDetail(e){
        var grid = e.sender;
        var row = grid.getSelected();
        if(row){
            var url = '<%=basePath%>'+ "/standard/InterBank/mini_print/MiniPrintTemplateEdit.jsp?action=detail";
            var tab = { id: "printTemplateManageDetail", name: "printTemplateManageDetail", text: "打印模板详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }
    }

   


    
    </script>

</body>
</html>