
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>非债业务减值基础数据表</title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
    <legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<input id="instName" name="instName" class="mini-buttonedit" onbuttonclick="onInsQuery" labelField="true" label="分行/支行名称：" labelStyle="text-align:right;" emptyText="请输入分行/支行名称"/>
		<input id="prodType" name="prodType" class="mini-textbox" labelField="true" label="产品细类："  labelStyle="text-align:right;" emptyText="请输入产品细类"/>
		<input id="vDate1" name="vDate1" class="mini-datepicker" labelField="true" label="债项起息日："  labelStyle="text-align:right;" emptyText="债项起息日的开始日期"/>
		<input id="vDate2" name="vDate2" class="mini-datepicker" labelField="true" label="~" onValuechanged="compareDate" labelStyle="text-align:center;" emptyText="债项起息日的结束日期"/>
		<input id="mDate1" name="mDate1" class="mini-datepicker" labelField="true" label="债项到期日：" onValuechanged="compareDate1"  labelStyle="text-align:right;" emptyText="债项到期日的起始日期"/>
		<input id="mDate2" name="mDate2" class="mini-datepicker" labelField="true" label="~" onValuechanged="compareDate2" labelStyle="text-align:center;" emptyText="债项到期日的结束日期"/>
		<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="expt_btn" class="mini-button" style="display: none"   onclick="expt">导出EXCEL</a>
	</span>	
	<div id="ReportShowBond" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true"  border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="instName" width="120" allowSort="false" headerAlign="center">分行/支行名称</div>
				<div field="prodBigType" width="120" allowSort="false" headerAlign="center" align="">产品大类</div>
				<div field="prodType" width="150" allowSort="false" headerAlign="center" align="">产品细类</div>
				<div field="prodNo" width="100" allowSort="false" headerAlign="center" align="center">产品代码</div>
				<div field="rate" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="p4">利率</div>
				<div field="vDate" width="80" allowSort="true" headerAlign="center" align="center">起息日</div>
				<div field="mDate" width="80" allowSort="true" headerAlign="center" align="center">到期日</div>
				<div field="intAmt" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">应计利息</div>
				<div field="amt" width="130" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">账面金额</div>
				<div field="settAmt" width="200" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">摊余成本</div>
				<div field="intType" width="150" allowSort="false" headerAlign="center" align="center" >付息方式</div>
				<div field="innerLevel" width="120" allowSort="false" headerAlign="center" align="left">内部评级(分类为同业信用和企业信用)</div>
				<div field="shortProdNo" width="120" allowSort="false" headerAlign="center" align="center">产品代码缩写</div>
				<div field="baseAssetMdate" width="120" allowSort="true" headerAlign="center" align="center">底层债券到期日</div>
			</div>
		</div>  
	</div>

	<script>
		mini.parse();
		top['AccOutCashManage'] = window;
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}
		
			var data = form.getData(true);
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/QdReportsController/pageReportDeValTradeBean',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}


		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}
		
		function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instName);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}
		
		//导出Excel                                                                                                                                                                                 
		function expt(){                                                                                                             
			mini.confirm("您确认要导出Excel吗?","系统提示",  
				function (action) 
				{
					if (action == "ok") 
					{
						var form = new mini.Form("#search_form");
						var data = form.getData(true);
						var val = null;
						var fields = '<input type="hidden" id="reportDate" name="reportDate" value="<%=__bizDate%>">';
						for(var id in data){
							fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
						}

						var urls = CommonUtil.pPath + "/sl/QdReportsController/exportDeValTrade";                                                                                                                           
						$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
					}
				}
			);                                                                                                                                                                                 
		}
		function query(){
			search(grid.pageSize,0);
		}
		function compareDate() {
					var sta = mini.get("vDate1");
					var end = mini.get("vDate2");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}

				function compareDate1() {
					var sta = mini.get("vDate2");
					var end = mini.get("mDate1");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}
				function compareDate2() {
					var sta = mini.get("mDate1");
					var end = mini.get("mDate2");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}


		$(document).ready(function()
		{
			search(10,0);
		});
		
	</script>
</body>
</html>
