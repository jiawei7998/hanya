
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>非债业务估值基础数据表</title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
    <legend>查询</legend>
	<div id="search_form" style="width:100%;">
			<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="投资代号：" labelStyle="text-align:right;" emptyText="请输入投资代号"/>
		<input id="custName" name="custName" class="mini-textbox" labelField="true" label="客户名称：" labelStyle="text-align:right;" emptyText="请输入客户名称"/>
		<input id="vDate1" name="vDate1" class="mini-datepicker" labelField="true" label="起息日：" labelStyle="text-align:right;" emptyText="请选择起息开始日期"/>
		<input id="vDate2" name="vDate2" class="mini-datepicker" labelField="true" label="~" onValuechanged="compareDate" labelStyle="text-align:center;" emptyText="请选择起息结束日期"/>
		<input id="mDate1" name="mDate1" class="mini-datepicker" labelField="true" label="到期日：" onValuechanged="compareDate1" labelStyle="text-align:right;" emptyText="请选择到期起始日期"/>
		<input id="mDate2" name="mDate2" class="mini-datepicker" labelField="true" label="~" onValuechanged="compareDate2" labelStyle="text-align:center;" emptyText="请选择到期结束日期"/>
		<span style="float:right;margin-right: 150px">
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
		<span style="margin:2px;display: block;">
			<a id="expt_btn" class="mini-button" style="display: none"   onclick="expt">导出EXCEL</a>
		</span>
	<div id="ReportShowBond" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true"  border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="dealNo" width="160" allowSort="true" headerAlign="center" align="center">投资代号</div>
				<div field="custName" width="120" allowSort="false" headerAlign="center" align="">客户名称</div>
				<div field="amt" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">面值</div>
				<div field="rate" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="p4">利率</div>
				<div field="vDate" width="80" allowSort="true" headerAlign="center" align="center">起息日</div>
				<div field="mDate" width="80" allowSort="true" headerAlign="center" align="center">到期日</div>
				<div field="ccy" width="80" allowSort="false" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
				<div field="intAmt" width="80" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">应计利息</div>
				<div field="balance" width="80" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">账面余额</div>
				<div field="intPayCycle" width="80" allowSort="false" headerAlign="center" align="left" >付息频率(INTPAYCYCLE)</div>
				<div field="intType" width="80" allowSort="false" headerAlign="center" align="center">计息方式</div>
				<div field="basis" width="80" allowSort="false" headerAlign="center" align="center">计息基准(BASIS)</div>
				<div field="intCalcRule" width="80" allowSort="false" headerAlign="center" align="center">计息规则(INTCALCRULE)</div>
			</div>
		</div>  
	</div>

	<script>
		mini.parse();
		top['AccOutCashManage'] = window;
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}
		
			var data = form.getData(true);
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var params = mini.encode(data);
			CommonUtil.ajax({				
				url:'/QdReportsController/pageReportValTradeBean',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}


		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}

		function query(){
			search(grid.pageSize,0);
		}
		function compareDate() {
					var sta = mini.get("vDate1");
					var end = mini.get("vDate2");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}

				function compareDate1() {
					var sta = mini.get("vDate2");
					var end = mini.get("mDate1");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}
				function compareDate2() {
					var sta = mini.get("mDate1");
					var end = mini.get("mDate2");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}		
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}
		
		//导出Excel                                                                                                                                                                                 
		function expt(){                                                                                                             
			mini.confirm("您确认要导出Excel吗?","系统提示",  
				function (action) 
				{
					if (action == "ok") 
					{
						var form = new mini.Form("#search_form");
						var data = form.getData(true);
						var val = null;
						var fields = '<input type="hidden" id="reportDate" name="reportDate" value="<%=__bizDate%>">';
						for(var id in data){
							fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
						}

						var urls = CommonUtil.pPath + "/sl/QdReportsController/exportValTradeRep";                                                                                                                           
						$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
					}
				}
			);                                                                                                                                                                                 
		}
			function getData(action) {
					if (action != "add") {
						row = null;
						row = grid.getSelected();
					}
					return row;
				}

		$(document).ready(function()
		{
			search(10,0);
		});
		
	</script>
</body>
</html>
