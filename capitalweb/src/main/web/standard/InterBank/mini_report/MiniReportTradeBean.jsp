
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>非标资产交易台账</title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
    <legend>查询</legend>
	<div id="search_form" style="width:100%;">
		<input id="sale_type" name="sale_type" class="mini-combobox" data = "CommonUtil.serverData.dictionary.saleType" labelField="true" label="资产类别：" labelStyle="text-align:right;" emptyText="请输入资产类别"/>
		<input id="productName" name="productName" class="mini-textbox" labelField="true" label="债券名称：" labelStyle="text-align:right;" emptyText="请输入债券名称"/>
		<input id="productCode" name="productCode" class="mini-textbox" labelField="true" label="债券代号：" labelStyle="text-align:right;" emptyText="请输入债券代号"/>
		<input id="cost" name="cost" class="mini-buttonedit" onbuttonclick="onCostQuery" labelField="true" label="成本中心：" labelStyle="text-align:right;" emptyText="请输入成本中心"/>
		<input id="vDate1" name="vDate1" class="mini-datepicker" labelField="true" label="起息日：" labelStyle="text-align:right;" emptyText="请选择起息日期"/>
		<input id="vDate2" name="vDate2" class="mini-datepicker" labelField="true" label="~" onValuechanged="compareDate" labelStyle="text-align:center;" emptyText="请选择起息日期"/>
		<input id="mDate1" name="mDate1" class="mini-datepicker" labelField="true" label="到期日：" onValuechanged="compareDate1" labelStyle="text-align:right;"emptyText="请选择到期日期"/>
		<input id="mDate2" name="mDate2" class="mini-datepicker" labelField="true" label="~" labelStyle="text-align:center;" onValuechanged="compareDate2" emptyText="请选择到期日期"/>
		<input id="port" name="port" class="mini-buttonedit" onbuttonclick="onPortQuery" labelField="true" label="投资组合：" labelStyle="text-align:right;" emptyText="请输入投资组合"/>
		<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
		</span>
	</div>
	</fieldset>
	<span style="margin:2px;display: block;">
			<a id="expt_btn" class="mini-button" style="display: none"   onclick="expt">导出EXCEL</a>
		</span>
	<div id="ReportShowTrade" class="mini-fit" style="margin-top: 2px;">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true" allowResize="true"  border="true" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="40">序号</div>
				<div field="zhfl" width="100" allowSort="false" headerAlign="center" align="center">账户分类</div>
				<div field="prdName" width="120" allowSort="false" headerAlign="center" align="">资产类别</div>
				<div field="productName" width="120" allowSort="false" headerAlign="center" align="">债券名称</div>
				<div field="productCode" width="120" allowSort="false" headerAlign="center" align="">债券代号</div>
				<div field="zcqd" width="120" allowSort="false" headerAlign="center" align="">资产清单</div>
				<div field="cost" width="80" allowSort="false" headerAlign="center" align="left">成本中心</div>
				<div field="port" width="80" allowSort="false" headerAlign="center" align="left">投资组合</div>
				<div field="baseProductRemark" width="120" allowSort="false" headerAlign="center" align="">资产说明</div>
				<div field="invType" width="100" allowSort="false" headerAlign="center" align="center">资产分类</div>
				<div field="amt" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">面值</div>
				<div field="gyamt" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">公允价值</div>
				<div field="tyamt" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">摊余成本</div>
				<div field="rateType" width="80" allowSort="" headerAlign="center" align="center">计息方式</div>
				<div field="contractRate" width="100" allowSort="true" headerAlign="center" align="right" numberFormat="p4">实际利率</div>
				<div field="vDate" width="80" allowSort="true" headerAlign="center" align="center">起息日</div>
				<div field="mDate" width="80" allowSort="true" headerAlign="center" align="center">到期日</div>
				<div field="sDate" width="80" allowSort="true" headerAlign="center" align="center">本期付息起息日</div>
				<div field="lastbdate" width="80" allowSort="true" headerAlign="center" align="center">最后一个买入日期</div>
				<div field="lastsdate" width="80" allowSort="true" headerAlign="center" align="center">最后一个卖出日期</div>
				<div field="mInt" width="80" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">一次还本利息</div>
				<div field="actualTint" width="80" allowSort="true" headerAlign="center" align="right" numberFormat="#,0.00">应收利息</div>
			</div>
		</div>  
	</div>

	<script>
		mini.parse();
		top['AccOutCashManage'] = window;
		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});

		function search(pageSize,pageIndex)
		{
			
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}
		
			var data = form.getData(true);
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/QdReportsController/getReportTradePage',
				data:params,
				callback : function(data) {
					var grid = mini.get("datagrid");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}

		function checkBoxValuechanged(){
			search(grid.pageSize,0);
		}

		function onCostQuery(e) {
				var btnEdit = this;
				mini.open({
					url: CommonUtil.baseWebPath() + "/mini_acc/MiniCostMiniManages.jsp",
					title: "成本中心选择",
					width: 750,
					height: 450,
					ondestroy: function (action) {
						if (action == "ok") {
							var iframe = this.getIFrameEl();
							var data = iframe.contentWindow.GetData();
							data = mini.clone(data); //必须
							if (data) {
								btnEdit.setValue(data.costcent);
								btnEdit.setText(data.costcent);
								btnEdit.focus();
							}
						}

					}
				});
			}
			function compareDate() {
					var sta = mini.get("vDate1");
					var end = mini.get("vDate2");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}

				function compareDate1() {
					var sta = mini.get("vDate2");
					var end = mini.get("mDate1");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}
				function compareDate2() {
					var sta = mini.get("mDate1");
					var end = mini.get("mDate2");
					if (sta.getValue() > end.getValue()) {
						mini.alert("开始时间不能大于结束时间", "系统提示");
						return end.setValue(sta.getValue());
					}
				}
			function onPortQuery(e) {
				var btnEdit = this;
				mini.open({
					url: CommonUtil.baseWebPath() + "/mini_acc/MiniPortMiniManages.jsp",
					title: "投资组合",
					width: 850,
					height: 450,
					ondestroy: function (action) {
						if (action == "ok") {
							var iframe = this.getIFrameEl();
							var data = iframe.contentWindow.GetData();
							data = mini.clone(data); //必须
							if (data) {
								btnEdit.setValue(data.portfolio);
								btnEdit.setText(data.portfolio);
								btnEdit.focus();
							}
						}

					}
				});
			}	
		
		function query(){
			search(grid.pageSize,0);
		}
		
		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}
		//导出Excel                                                                                                                                                                                 
		function expt(){                                                                                                             
			mini.confirm("您确认要导出Excel吗?","系统提示",  
				function (action) 
				{
					if (action == "ok") 
					{
						var form = new mini.Form("#search_form");
						var data = form.getData(true);
						var val = null;
						var fields = '<input type="hidden" id="reportDate" name="reportDate" value="<%=__bizDate%>">';
						for(var id in data){
							fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
						}

						var urls = CommonUtil.pPath + "/sl/QdReportsController/exportTradeRep";                                                                                                                           
						$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
					}
				}
			);                                                                                                                                                                                 
		}
			
			
		function getData(action) {
				if (action != "add") {
					row = null;
					row = grid.getSelected();
				}
				return row;
			}


		$(document).ready(function()
		{
			search(10,0);
		});
		
	</script>
</body>
</html>
