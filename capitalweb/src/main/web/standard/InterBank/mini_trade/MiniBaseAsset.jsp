<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>基础资产录入</legend>
		<div id="data_form">
			<table width="100%">
				<tr>
					<td class="red-star">基础资产类别</td>
					<td><input id="baseAssetType" name="baseAssetType" class="mini-combobox" required="true"  vtype="maxLength:20" data="CommonUtil.serverData.dictionary.baseAssetType"/></td>
					<td>客户号</td>
					<td><input id="cno" name="cno" class="mini-buttonedit" onbuttonclick="onCustQuery" allowInput="false" vtype="maxLength:30" editable="false"/></td>  
					<td>客户名称</td>
					<td><input id="cnm" name="cnm" class="mini-textbox" vtype="maxLength:40" /></td>       
				</tr>
				<tr>
					<td>是否行内客户</td>                                                                        
					<td><input id="isCust" name="isCust" class="mini-combobox" data="CommonUtil.serverData.dictionary.YesNo" value="1"/></td>
					<td>行内评级</td>                                                                        
					<td><input id="innerLevel" name="innerLevel" class="mini-textbox" vtype="maxLength:20" /></td>
					<td>公开评级</td>                                                                        
					<td><input id="outerLevel" name="outerLevel" class="mini-textbox" vtype="maxLength:20" /></td>
				</tr>
				<tr>
					<td>行业大类</td>
					<td><input id="bigCat" name="bigCat" class="mini-combobox" onvaluechanged="onBigCatChanged" vtype="maxLength:20"/></td>       
					<td>行业中类</td>                                                                        
					<td><input id="middleCat" name="middleCat" class="mini-combobox" onvaluechanged="onMiddleCatChanged" vtype="maxLength:20"/></td> 
					<td>行业小类</td>
					<td><input id="smallCat" name="smallCat" class="mini-combobox"  /></td>     
				</tr>
				<tr>
					<td>对应投资金额</td>
					<td><input id="inBalance" name="inBalance" class="mini-spinner" changeOnMousewheel="false" allowNull="true" maxlength="15" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" format="#,0.00" allowlimitvalue="false" onvaluechanged="onInBalanceChanged" value="null"/></td>
					<td>风险资产类型</td>
					<td><input id="assetType" name="assetType" class="mini-buttonedit" allowInput="false" onbuttonclick="onUserParamQuery" name="assetType"/></td>
					<td>风险资产系数</td>
					<td><input id="coefficient" name="coefficient" name="coefficient" class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])" format="#,0.00" allowlimitvalue="false" value="null" enabled="false"/></td>
				</tr>
				<tr>
					<td>风险资产占用</td>
					<td><input id="assetBalanceAuto" name="assetBalanceAuto" class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" format="#,0.00" allowlimitvalue="false"  value="null" onvaluechanged="onAssetBalanceAutoChanged"/></td>
					<td>风险缓释金额</td>
					<td><input id="rsrAmt" name="rsrAmt" class="mini-spinner" maxlength="15" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[-999999999999,999999999999])" format="#,0.00" allowlimitvalue="false"  value="null"/></td>
					<td>货币市场类型</td>
					<td><input id="moneyMarketType" name="moneyMarketType" class="mini-combobox" data="CommonUtil.serverData.dictionary.moneyMarketType"/></td>
				</tr>
				<tr>
					<td>股东性质</td>
					<td><input id="shareType" name="shareType" class="mini-combobox" data="CommonUtil.serverData.dictionary.shoreholderType" /></td>
					<td>保证人名称</td>                                                                                  
					<td><input id="guarantor" name="guarantor" class="mini-textbox" vtype="maxLength:32" /></td>      
					<td>保证人所属行业</td>
					<td><input id="guarantorIndus" name="guarantorIndus" class="mini-combobox" data="CommonUtil.serverData.dictionary.GBT4754_2011"/></td>
				</tr>
				<tr>
					<td>债券编号</td>                                                                         
					<td><input id="secId" name="secId" class="mini-textbox" maxLength = "25"  onvalidation="CommonUtil.onValidation(e,'stockId','')"/></td>
					<td>行权日期</td>
					<td><input id="optExerDate" name="optExerDate" class="mini-datepicker" vtype="maxLength:10"/></td>
					<td>股票代码</td>                                                                                           
					<td><input id="stockId" name="stockId" class="mini-textbox" maxLength = "25"  onvalidation="CommonUtil.onValidation(e,'stockId','')"/></td>
				</tr>
				<tr>
					<td>预警线</td>
					<td><input id="warnLinePrice"  name="warnLinePrice" class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100000])" format="#,0.00" maxlength="10" allowlimitvalue="false" value="null"/></td>	
					<td>补仓线</td>                                                                                               
					<td><input id="coverLinePrice" name="coverLinePrice" class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100000])" format="#,0.00" maxlength="10" allowlimitvalue="false" value="null"/></td>     
					<td>平仓线</td>
					<td><input id="openLinePrice"  name="openLinePrice" class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100000])" format="#,0.00" maxlength="10" allowlimitvalue="false" value="null"/></td>
				</tr>
				<tr>
					<td>质押物类型</td>
					<td><input id="pledgeType" name="pledgeType" class="mini-combobox" data="CommonUtil.serverData.dictionary.pledgeType" /></td>
					<td>质押物金额</td>
					<td><input id="pledgeAmt"  name="pledgeAmt" class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" maxlength="15" format="#,0.00" allowlimitvalue="false"  value="null"/></td>
					<td>质押利率</td>                                                                                                                 
					<td><input id="pledgeRate" name="pledgeRate" class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])" format="p4" allowlimitvalue="false"  value="null"/></td>    
				</tr>
				<tr>
					<td>质押起始日</td>
					<td><input id="pledgeVdate" name="pledgeVdate" class="mini-datepicker" /></td>
					<td>质押到期日</td>
					<td><input id="pledgeMdate" name="pledgeMdate" class="mini-datepicker" /></td>
					<td>质押利差</td>
					<td><input id="pledgeSpread" name="pledgeSpread"  class="mini-spinner" changeOnMousewheel="false" allowNull="true" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])" format="p4" allowlimitvalue="false"  value="null"/></td>
				</tr>
				<tr>
					<td>增信方式</td>
					<td><input id="mortgageType" name="mortgageType" class="mini-combobox" data="CommonUtil.serverData.dictionary.icWay"/></td>
	
					<td>增信描述</td>
					<td colspan="4"><input id="mortgageRemark" name="mortgageRemark" style="width:80%" class="mini-textbox" vtype="maxLength:100"/></td>
				</tr>
				<tr>
					<td>备注</td>
					<td colspan="6"><input id="remark" name="remark" class="mini-textarea" vtype="maxLength:100" width="700" height="44"/></td>
				</tr>
				<tr>
					<td colspan="3">
						<a id="add_btn"    class="mini-button" style="display: none"   onclick="add">新增</a>
						<a id="update_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
						<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
					</td>	
				</tr>
			</table>
		</div>
	</fieldset>
	
	<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:350px;" 
		idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client" showPager="false"
		pageSize="20"  multiSelect="false" allowAlternating="true" frozenStartColumn="0" frozenEndColumn="3"  sortMode="client">
		<div property="columns">
			<div type="indexcolumn"  width="40" headerAlign="center" allowSort="false">序号</div> 
			<div field='refNo'        headerAlign= "center" allowSort= "true"     width='120' visible="false">原交易号</div>                  
			<div field='baseAssetType'headerAlign= "center" allowSort= "true"     width='120' data-options="{dict:'baseAssetType'}" renderer="CommonUtil.dictRenderer">基础资产类别</div>                                        
			<div field='cno'          headerAlign= "center" allowSort= "true"     width='120' align="center">客户号</div>   
			<div field='cnm'          headerAlign= "center" allowSort= "true"     width='120'>客户名称</div>    
			<div field='isCust'       headerAlign= "center" allowSort= "true"     width='100' align="center" data-options="{dict:'YesNo'}" renderer="CommonUtil.dictRenderer">是否行内客户</div>            
			<div field='inBalance'      headerAlign= "center" allowSort= "true"     width='120' align = 'right' numberFormat="#,0.00">对应投资金额</div>                            
			<div field='bigCat'       headerAlign= "center" allowSort= "true"     width='120' visible="false">行业大类</div>                        
			<div field='middleCat'    headerAlign= "center" allowSort= "true"     width='120' visible="false">行业中类</div>                        
			<div field='smallCat'     headerAlign= "center" allowSort= "true"     width='120' visible="false">行业小类</div>
			<div field='moneyMarketType' headerAlign= "center" allowSort= "true"  width='120' data-options="{dict:'moneyMarketType'}" renderer="CommonUtil.dictRenderer">货币市场类型</div>  
			<div field='innerLevel'     headerAlign= "center" allowSort= "true"     width='80'>行内评级</div>                        
			<div field='outerLevel'     headerAlign= "center" allowSort= "true"     width='80'>公开评级</div>                        
			<div field="bigKindName"   headerAlign="center" allowSort="true" width="120">行业大类</div>                                                               	
			<div field="midKindName"   headerAlign="center" allowSort="true" width="120">行业中类</div>                                                                     	
			<div field="smallKindName" headerAlign="center" allowSort="true" width="120">行业小类</div>   
			<div field='guarantor'    headerAlign= "center" allowSort= "true"     width='120'>保证人名称</div>                      
			<div field='guarantorIndus' headerAlign= "center" allowSort= "true"   width='120' data-options="{dict:'GBT4754_2011'}" renderer="CommonUtil.dictRenderer">保证人所属行业</div>                  
			<div field='secId'        headerAlign= "center" allowSort= "true"     width='120' align="center" >债券编号</div>                        
			<div field='optExerDate'  headerAlign= "center" allowSort= "true"     width='80' align="center" >行权日期</div>                        
			<div field='stockId'      headerAlign= "center" allowSort= "true"     width='80' align="center" >股票代码</div>  
			<div field='warnLinePrice'headerAlign= "center" allowSort= "true"     width='120' align='right' numberFormat="#,0.00">预警线</div>                          
			<div field='coverLinePrice'headerAlign="center" allowSort= "true"     width='120' align='right' numberFormat="#,0.00">补仓线</div>                          
			<div field='openLinePrice'headerAlign= "center" allowSort= "true"     width='120' align='right' numberFormat="#,0.00">平仓线</div>                          
			<div field='shareType'    headerAlign= "center" allowSort= "true"     width='120' data-options="{dict:'shoreholderType'}" renderer="CommonUtil.dictRenderer" >股东性质</div>                        
			<div field='pledgeType'   headerAlign= "center" allowSort= "true"     width='120' data-options="{dict:'pledgeType'}" renderer="CommonUtil.dictRenderer">质押物类型</div>                      
			<div field='pledgeAmt'    headerAlign= "center" allowSort= "true"     width='120' align='right' numberFormat="#,0.00">质押物金额</div>                      
			<div field='pledgeVdate'  headerAlign= "center" allowSort= "true"     width='80'>质押起始日</div>                      
			<div field='pledgeMdate'  headerAlign= "center" allowSort= "true"     width='80'>质押到期日</div>                      
			<div field='pledgeRate'   headerAlign= "center" allowSort= "true"     width='100' align='right' numberFormat="p4">质押利率</div>                      
			<div field='pledgeSpread' headerAlign= "center" allowSort= "true"     width='100' align='right' numberFormat="p4">质押利差</div>                      
			<div field='mortgageType' headerAlign= "center" allowSort= "true"     width='120' data-options="{dict:'icWay'}" renderer="CommonUtil.dictRenderer">增信方式</div>                
			<div field='assetType'    headerAlign= "center" allowSort= "true" width='180'>风险资产类型</div>                
			<div field='coefficient'  headerAlign= "center" allowSort= "true"       align='right' numberFormat="#,0.00">风险资产系数</div>                    
			<div field='inBalance'    headerAlign= "center" allowSort= "true"       align='right' numberFormat="#,0.00">对应投资金额</div>                    
			<div field='assetBalanceAuto' headerAlign= "center" allowSort= "true"   align='right' numberFormat="#,0.00">风险资产占用</div>      
			<div field='rsrAmt'           headerAlign= "center" allowSort= "true"   align='right' numberFormat="#,0.00">风险缓释金额</div>
			<div field='mortgageRemark'   headerAlign= "center" allowSort= "true"   width='120'>增信描述</div>
			<div field='remark'           headerAlign= "center" allowSort= "true">备注</div>
		</div>
	</div>
	<script type="text/javascript">
		//获取参数传递	
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url,"prdNo");
		//操作类型
		var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
		var dealNo = CommonUtil.getParam(url,"dealNo");

		mini.parse();
		function tradeManagerWin()
		{
			if(operType == 'A'){
				return top["tradeEditTabs_" + prdNo + "_" + operType];
			} else {
				return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
			}
		}

		
		//父页面的参数值
		var tradeData = tradeManagerWin().param;
		
		
		if($.inArray(operType,['A','U']) == -1){//不是新增和修改 禁用按钮
			mini.get("add_btn").setEnabled(false);
			mini.get("update_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("clear_btn").setEnabled(false);
		}

		//获取组件
		var grid = mini.get("grid1");
		//获取参数值
		CommonUtil.ajax({
			url:'/ProductApproveController/searchBaseAssetListVo',
			data:{dealNo:tradeData.dealNo},
			callback:function(data){
				grid.setData(data.obj);
			}
		});

		//异步加载行业类型数据字典 bigCat middleCat smallCat
		CommonUtil.loadPartyKindDict("bigCat","-1");
		
		//产品大类控制产品中类
		function onBigCatChanged(e){
			CommonUtil.loadPartyKindDict("middleCat",mini.get("bigCat").getValue());
			mini.get("smallCat").setData([]);
		}

		//产品中类控制产品小类
		function onMiddleCatChanged(e){
			CommonUtil.loadPartyKindDict("smallCat",mini.get("middleCat").getValue());
		}
		
        function save() {
			if(grid.data.length <= 0){
				mini.alert("请先新增交易至列表中!","提示");
				return;
			}
			var param = {dealNo : tradeData.dealNo};
			param["baseAssets"] = grid.getData(true);
			
            var json = mini.encode(param);
			CommonUtil.ajax({
				url:"/ProductApproveController/updateProductApproveSub",
				data:json,
				callback:function(data){
					mini.alert("保存成功.","提示");
				}
			});
		}
		
		function onUserParamQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniUserParamSelectManages.jsp",
				title : "参数查询",
				width : 750,
				height : 450,
				onload : function(){
					var iframe = this.getIFrameEl();
					var param = {p_type:'1',p_sub_type:'7'};
					if(iframe && iframe.contentWindow && iframe.contentWindow.init){
						iframe.contentWindow.init(param);
					}
				},
				ondestroy : function(action) {		
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.p_name);
							btnEdit.setText(data.p_name);
							btnEdit.focus();
							var coefficient = mini.get("coefficient");
							if(coefficient){
								coefficient.setValue(data.p_value);
							}
							calRisk();
						}
					}
				}
			});
		}

		function onCustQuery(e) {
			var btnEdit = e.sender;
			mini.open({
				url : CommonUtil.baseWebPath() + "/mini_cp/MiniCoreCustQueryEdits.jsp",
				title : "客户选择",
				width : 750,
				height : 500,
				ondestroy : function(action) {		
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data && !data.cifNo) {
							btnEdit.setValue(data.party_id);
							btnEdit.setText(data.party_id);
							setCustData(data);

						}else if(data && data.cifNo){
							data.core_custno = data.cifNo;
							data.party_id = data.cifNo;
							data.party_name = data.custName;

							btnEdit.setValue(data.party_id);
							btnEdit.setText(data.party_id);

							CommonUtil.ajax({
								url:'/CounterPartyController/selectCounterPartyList',
								data:{'party_id' : data.party_id},
								callback:function(data){
									setCustData(data[0]);
								}
							});	
						}
					}// end if
				}
			});
		}

		function setCustData(data){
			mini.get("cnm").setValue(data.party_name);
			// bigCat middleCat smallCat
			//产品大类 
			mini.get("bigCat").setValue(data.party_bigkind);

			//产品中类
			if(CommonUtil.isNotNull(data.party_bigkind)){
				CommonUtil.loadPartyKindDict("middleCat",data.party_bigkind);
				mini.get("middleCat").setValue(data.party_midkind);
			}
			
			//产品小类
			if(CommonUtil.isNotNull(data.party_midkind)){
				CommonUtil.loadPartyKindDict("smallCat",data.party_midkind);
				mini.get("smallCat").setValue(data.party_smallkind);
			}
		}

		//计算风险缓释
		function calRisk(){
			var inBalance = mini.get("inBalance");
			var coefficient = mini.get("coefficient");
			var assetBalanceAuto = mini.get("assetBalanceAuto");
			var rsrAmt = mini.get("rsrAmt");
			if (inBalance) {
				try {
					assetBalanceAuto.setValue(parseFloat(inBalance.getValue()) * (coefficient.getValue()));
					rsrAmt.setValue(inBalance.getValue() - assetBalanceAuto.getValue());
				} catch (error) {
					
				}
			}
		}

		function onInBalanceChanged(){
			calRisk();
		}

		function onAssetBalanceAutoChanged(){
			var inBalance = mini.get("inBalance");
			if (inBalance) {
				try {
					mini.get("rsrAmt").setValue(inBalance.getValue() - mini.get("assetBalanceAuto").getValue());
				} catch (error) {
					
				}
			}
		}
		

		function add(){
			var form = new mini.Form("data_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
				return
			};

			var row = form.getData(true);
			row.bigKindName = mini.get("bigCat").getText();
			row.midKindName = mini.get("middleCat").getText();
			row.smallKindName = mini.get("smallCat").getText();
			grid.addRow(row, grid.data.length);
			mini.alert("操作成功!","消息提示");
		}

		function del(){
			var rows = grid.getSelecteds();
            if (rows.length > 0) {
				grid.removeRows(rows, true);
				mini.alert("删除成功!","消息提示");
            }else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		function clear(){
			var form = new mini.Form("#data_form");
			form.clear();
		}

		function onRowDblClick()
		{
			var form = new mini.Form("data_form");
			var row =  grid.getSelected();
			form.setData(row,true,false);
			form.setIsValid(true);

			var controls = mini.findControls(function(control){
				if(control.type == "buttonedit") return true;    //获取所有mini-textbox控件
			});
			for (var i = 0; i< controls.length; i++) 
			{
				if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
					controls[i].setText(controls[i].getValue());
				} 
			}
			CommonUtil.loadPartyKindDict("middleCat",row.bigCat);
			mini.get("middleCat").setValue(row.middleCat);

			CommonUtil.loadPartyKindDict("smallCat",row.middleCat);
			mini.get("smallCat").setValue(row.smallCat);
		}

		function update(){
			var row =  grid.getSelected();
			if(row){
				var form = new mini.Form("data_form");
				form.validate();
				if(form.isValid() == false){
					mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
					return
				};
				var data = form.getData(true);
				data.bigKindName = mini.get("bigCat").getText();
				data.midKindName = mini.get("middleCat").getText();
				data.smallKindName = mini.get("smallCat").getText();

				grid.updateRow(row,data);
				
				mini.alert("操作成功!","消息提示");
			}else{
				mini.alert("请选择要修改的列!","消息提示");
			}
		}
    </script>
	</body>
</html>

