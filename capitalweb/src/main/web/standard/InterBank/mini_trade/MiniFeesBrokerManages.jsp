﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>
		<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
		<title>中介费查询页面</title>

		<head>
		</head>

		<body style="width:100%;height:100%;background:white">
			<fieldset class="mini-fieldset">
				<legend>查询</legend>
				<div id="fees_search_form" style="width:100%;">
					<input id="refNo" name="refNo" class="mini-textbox" labelField="true"
					 label="原交易流水号：" labelStyle="text-align:right;" emptyText="请输入原交易流水号..." />
					<input id="feeBasis" name="feeBasis" class="mini-combobox" data="CommonUtil.serverData.dictionary.DayCounter" labelField="true"
					 label="计息基础：" labelStyle="text-align:right;" emptyText="请选择计息基础..." />
					<input id="feeType" name="feeType" class="mini-combobox" data="CommonUtil.serverData.dictionary.feePayType" labelField="true"
					 label="费用类型：" labelStyle="text-align:right;" emptyText="请选择费用类型..." />
					<input id="dealDate" class="mini-datepicker" name="dealDate" vtype="date:yyyy-MM-dd" validateOnLeave="true" dateErrorText="格式为yyyy-MM-dd"
					 valueType="string" labelField="true" label="交易日期：" labelStyle="text-align:right;" emptyText="请选择交易日期...">
					<input id="cost" name="cost" class="mini-buttonedit" onbuttonclick="onCostQuery" labelField="true" label="成本中心：" labelStyle="text-align:right;"
					 emptyText="请输入成本中心" />
					<input id="port" name="port" class="mini-buttonedit" onbuttonclick="onPortQuery" labelField="true" label="投资组合：" labelStyle="text-align:right;"
					 emptyText="请输入投资组合" />
					 <span style="float:right;margin-right: 160px">
						<a id="search_btn" class="mini-button" style="display: none"   onclick="query()" style="margin-left:5px">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					</span>
				</div>
			</fieldset>
			<span style="margin:2px;display: block;">
				<a id="edit_btn" class="mini-button" style="display: none"   style="margin-left:5px" onclick="update()">修改</a>
				<a id="export_btn" class="mini-button" style="display: none"    style="margin-left:5px" onclick="expt">导出EXCEL</a>
			</span>

			<div class="mini-fit" id="FeesBrokerManages" title="收息计划列表" style="margin-top: 2px;">
				<div id="datagrid" class="mini-datagrid borderAll" allowResize="true" width="100%" height="100%" allowAlternating="true" onrowdblclick="onRowDblClick"
				 sortMode="client">
					<div property="columns">
						<div type="indexcolumn" width="30" headerAlign="center" allowSort="true" align="center"></div>
						<!-- <div field="dealNo" width="200" headerAlign="center" align="center">流水号</div> -->
						<div field="refNo" width="180" headerAlign="center" align="center">原交易流水</div>
						<div field="partyName" width="180" headerAlign="center">客户名称</div>
						<div field="organization" width="180" headerAlign="center">中介机构</div>
						<div field="prdName" width="150" headerAlign="center">产品名称</div>
						<div field="aDate" width="80" headerAlign="center" allowSort="true" align="center">交易日期</div>
						<div field="cost" width="80" allowSort="false" headerAlign="center" align="left">成本中心</div>
						<div field="port" width="80" allowSort="false" headerAlign="center" align="left">投资组合</div>
						<div field="feeBasis" width="80" headerAlign="center" align="left">计息基础</div>
						<div field="feeType" width="80" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'feePayType'}">费用类型</div>
						<div field="feeRate" width="80" headerAlign="center" align="right" numberFormat="p4">费率</div>
						<div field="feesAmt" headerAlign="center" allowSort="true" align="right" numberFormat="#,0.00">费用</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				top['FeesBrokerManages'] = window;
				var grid = mini.get("datagrid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				function search(pageSize, pageIndex) {
					var form = new mini.Form("fees_search_form");
					form.validate();
					if (form.isValid == false)
						return;
					//提交数据
					var data = form.getData(true);//获取表单多个控件的数据  
					data['pageNumber'] = pageIndex + 1;
					data['pageSize'] = pageSize;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: '/TdFeesBrokerController/pageFeesBrokerList',
						data: param,
						callback: function (data) {
							var grid = mini.get("datagrid");
							grid.setTotalCount(data.obj.total);
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);
						}
					});
				}

				function update() {
					var row = grid.getSelected();
					if (row) {						
						var url = CommonUtil.baseWebPath() + "/mini_trade/MiniFeesBrokerEdit.jsp?action=edit"
						var tab = { id: "FeesBrokerManageEdit", name: "FeesBrokerManageEdit", title: "中介费用修改", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);
					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				function onRowDblClick(e) {
					var grid = mini.get("datagrid");
					var row = grid.getSelected();
					if (row) {
						var url = CommonUtil.baseWebPath() + "/mini_trade/MiniFeesBrokerEdit.jsp?action=detail";
						var tab = { id: "FeesBrokerManageDetail", name: "FeesBrokerManageDetail", title: "中介费用详情", url: url, showCloseButton: true,parentId: top["win"].tabs.getActiveTab().name };
						top["win"].openNewTab(tab, row);

					} else {
						mini.alert("请选中一条记录！", "消息提示");
					}
				}

				function expt() {
					mini.confirm("您确认要导出Excel吗?", "系统提示",
						function (action) {
							if (action == "ok") {
								var form = new mini.Form("#fees_search_form");
								var data = form.getData(true);
								var val = null;
								var fields = '<input type="hidden" id="reportDate" name="reportDate" value="<%=__bizDate%>">';
								for (var id in data) {
									fields += '<input type="hidden" id="' + id + '" name="' + id + '" value="' + data[id] + '">';
								}

								var urls = CommonUtil.pPath + "/sl/QdReportsController/expFeesBroker";
								$('<form action="' + urls + '" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
							}
						}
					);
				}

				function onCostQuery(e) {
						var btnEdit = this;
						mini.open({
							url: CommonUtil.baseWebPath() + "/mini_acc/MiniCostMiniManages.jsp",
							title: "成本中心选择",
							width: 750,
							height: 450,
							ondestroy: function (action) {
								if (action == "ok") {
									var iframe = this.getIFrameEl();
									var data = iframe.contentWindow.GetData();
									data = mini.clone(data); //必须
									if (data) {
										btnEdit.setValue(data.costcent);
										btnEdit.setText(data.costcent);
										btnEdit.focus();
									}
								}

							}
						});
					}

					function onPortQuery(e) {
						var btnEdit = this;
						mini.open({
							url: CommonUtil.baseWebPath() + "/mini_acc/MiniPortMiniManages.jsp",
							title: "投资组合",
							width: 850,
							height: 450,
							ondestroy: function (action) {
								if (action == "ok") {
									var iframe = this.getIFrameEl();
									var data = iframe.contentWindow.GetData();
									data = mini.clone(data); //必须
									if (data) {
										btnEdit.setValue(data.portfolio);
										btnEdit.setText(data.portfolio);
										btnEdit.focus();
									}
								}

							}
						});
					}
				function getData() {
					var row = null;
					row = grid.getSelected();
					return row;
				}

				function clear() {
					var form = new mini.Form("#fees_search_form");
					form.clear();
				}

				function query() {
					var grid = mini.get("datagrid");
					search(grid.pageSize, 0);
				}

				$(document).ready(function () {
					search(10, 0);
				})
			</script>

		</body>
		</html>