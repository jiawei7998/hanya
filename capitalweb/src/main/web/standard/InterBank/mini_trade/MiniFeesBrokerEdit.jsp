<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>

		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title>中介费用信息</title>
		</head>

		<body>
			<div id="form1">
				<span id="labell">
					<a href="javascript:CommonUtil.activeTab();">中介费用</a> >> 修改</span>
				<div id="FeesBrokerEdit" class="fieldset-body">
				<table class="mini-table" id="accout_form" width="100%" class="mini-table">
					<tr>
						<td>
							<input id="dealNo" name="dealNo" class="mini-textbox" Enabled="false" vtype="" labelField="true" label="流水号:"
							 style="width:300px;"  emptyText="" />
						</td>
						<td>
							<input id="refNo" name="refNo" requiredErrorText="该输入项为必输项" class="mini-textbox"  vtype="" Enabled="false"
							 labelField="true" label="原交易流水:" style="width:300px;" emptyText=" " />
						</td>
						<td>
							<input id="prdName" name="prdName" class="mini-textbox" labelField="true" label="产品名称:" Enabled="false" style="width:300px"
							 emptyText="" />
						</td>
					</tr>
					<tr>
						<td>
							<input id="dealDate" name="dealDate" class="mini-datepicker" labelField="true" label="交易日期:" Enabled="false" style="width:300px"
							 emptyText="" />
						</td>
						<td>
							<input id="feeBasis" name="feeBasis" class="mini-combobox" labelField="true"  label="计息基础:" style="width:300px" data="CommonUtil.serverData.dictionary.DayCounter" value="Actual/360"
							 emptyText="" />
						</td>
						<td>
							<input id="feeType" name="feeType" class="mini-combobox" labelField="true" data="CommonUtil.serverData.dictionary.feePayType"  label="费用类型:" style="width:300px" onvaluechanged="onfeeTypeChange"
							 emptyText="" />
						</td>
					</tr>
					<tr>
						<td>
							<input id="feeRate" name="feeRate" class="mini-spinner" changeOnMousewheel="false" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])" allowLimitValue="false"    format="p4" labelField="true"  label="中介费率:" style="width:300px"
							 emptyText="" />
						</td>
						<td>
							<input id="feesAmt" name="feesAmt" class="mini-spinner" labelField="true"  maxValue="999999999999" Format="n2" required="true"  label="费用:"  style="width:300px" emptyText="" />
						</td>
					</tr>
					<tr>
						<td colspan="3" style="text-align:center;">
							<a id="save_btn" class="mini-button" style="display: none"    onclick="add">保存</a>
							<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close">取消</a>
						</td>
					</tr>
				</table>
			<script type="text/javascript">
				mini.parse();
				var url = window.location.search;
				var action = CommonUtil.getParam(url, "action");
				var param = top["FeesBrokerManages"].getData(action);
				function add() {
					var form = new mini.Form("accout_form");
					form.validate();
					if (form.isValid() == false) {
						mini.alert("信息填写有误，请重新填写!", "消息提示")
						return;
					}
					var data = form.getData(true); //获取表单多个控件的数据  
					if(data.feeType=='2'){
						if(data.feeRate<=0){
							mini.alert("中介费率必须大于0","系统提示");
							return ;
						}
					}else{
						if(data.feesAmt<=0){
							mini.alert("费用必须大于0","系统提示");
							return ;
						}
					}
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({ 
						url: "/TdFeesBrokerController/updateFees",
						data: param,
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功",'提示信息',function(){
									setTimeout(function(){top["win"].closeMenuTab()},10)
								});
							} else {
								mini.alert("保存失败");
							}
						}
					});

				}
				//
				function close() {
							top["win"].closeMenuTab();
						}
				function initform() {
					if (action == "edit") {
						var form = new mini.Form("#accout_form");
						var feeType = param.feeType;
						if(feeType == '1'){
							mini.get("feeRate").setEnabled(false);
						}else{
							mini.get("feesAmt").setEnabled(false);
						}
						form.setData(param);
					} else {
						$("#labell").html("<a href='javascript:CommonUtil.activeTab();'>中介费用</a>>>详情");
						var form = new mini.Form("#accout_form");
						form.setData(param);
						form.setEnabled(false);
						mini.get("save_btn").setEnabled(false);
					}

				}
				
				function onfeeTypeChange(){
					var feeType = mini.get("feeType").getValue();
					if(feeType == '2'){
						setFieldStatus("feeRate",true,true);
						setFieldStatus("feeBasis",true,true);
						setFieldStatus("feesAmt",false,false);

					}else{
						setFieldStatus("feeRate",false,false);
						setFieldStatus("feeBasis",false,false);
						setFieldStatus("feesAmt",true,true);
					}
				}

				//设置必输字段
				function setFieldStatus(id,required,enabled){
					if(CommonUtil.isNotNull(mini.get(id)))
					{
						mini.get(id).setRequired(required);

						mini.get(id).setEnabled(enabled);

						if(!enabled){
							mini.get(id).setValue(null);
							mini.get(id).setIsValid(true);
						}
					}
				}
				$(document).ready(function () {
					initform();
					onfeeTypeChange();
				})
			</script>
		</body>

		</html>