﻿	mini.parse();
	//获取参数传递	
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url,"prdNo");
	var dealNo = CommonUtil.getParam(url,"dealNo");
	//操作类型
	var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批

	var tradeData = tradeManagerWin().param;
	tradeData.isTrade = true;
	var _bizDate = sysDate;
	var MktlrRate = null;
	var prdName  = tradeData.prdName;
	var dealType = tradeData.dealType;

	//表单参数配置
	var queryParam = {"formType" : "1", "prdNo" : prdNo, "textAreaWidth" : "876.5", "fdsType" : "0"};
	var json = mini.encode(queryParam);
	//初始化数据参数
	var param = {'dealNo':dealNo,'operType':operType};
	//获取表单信息
	CommonUtil.ajax({
		data: json,
		url: "/ProductController/searchProductFormFieldsetFieldToHtml",
		showLoadingMsg:false,
		callback:function(data){
			var table = $("#productApprove_form");
			table.append(data.detail);
			mini.parse();
			//初始化commoBox值
			initCom(param);
		}
	});

	function tradeManagerWin()
	{
		if(operType == 'A'){
			return top["tradeEditTabs_" + prdNo + "_" + operType];
		} else {
			return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
		}
	}

	function initCom(param){
		var tip = new mini.ToolTip();
		tip.set({
			target: document,
			selector: '[data-tooltip], [title]'
		});

		if(param.operType != "A"){ // 如果不是新增则需要设置
			CommonUtil.ajax({
				url:"/ProductApproveController/searchProductApproveActivated",
				data:{dealNo : param.dealNo},
				showLoadingMsg:false,
				callback : function(data) {
					if(CommonUtil.isNull(data) || CommonUtil.isNull(data.obj))
					{
						mini.alert("交易已被删除!","系统提示");
						return;	
					}

					var form = new mini.Form("#trade_info");
					form.setData(data.obj, false);
					
					try {
						//设置text 审批发起人 审批发起机构 成本中心 投资组合 营销机构
						setFieldValue("sponsor", data.obj.sponsor, data.obj.user.userName);
						setFieldValue("sponInst", data.obj.sponInst, data.obj.institution.instName);
						setFieldValue("cost", data.obj.cost, data.obj.trdCost.costdesc);
						setFieldValue("port", data.obj.port, data.obj.trdPort.portdesc);
						setFieldValue("marketingOrg", data.obj.marketingOrg, data.obj.marketIns.instName);
					} catch (error) {
						
					}
					
					//获取所有mini-buttonedit控件
					var controls = mini.findControls(function(control){
						if(control.type == "buttonedit") return true;    
					});
					//给buttonedit的text赋值
					for (var i = 0; i< controls.length; i++) 
					{
						if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
							if(CommonUtil.isNull(controls[i].getText())){
								controls[i].setText(controls[i].getValue());
							}
						}
					}
					//加载客户信息
					setCustData(data.obj.counterParty,'init');

					//初始化下拉框值
					loadComboxDict();
					
					//显示右侧按钮
					showBtns();

					//添加监听事件
					addListener();

					//设置不可编辑,设置延时，否则会与监听事件中部分处理冲突
					if($.inArray(param.operType,["D","approve"]) > -1){
						setTimeout(function(){form.setEnabled(false);},200);
					}
				}
			});

		}else{
			//添加监听事件
			initValues = false;
			addListener();

			//显示右侧按钮
			showBtns();
		}
	}// end initCom

	/**
	修改和详情界面初始化下拉框数据字典,由于直接加载界面下拉框所有数据字典会导致界面卡顿,所以先给下拉框附一个值
	点击下拉框时才加载所有字典项
	**/
	function loadComboxDict(){
		//获取所有mini-combobox控件
		var controls = mini.findControls(function(control){
			if(control.type == "combobox") return true;    
		});

		var dictArr = new Array();
		for (var i = 0; i< controls.length; i++) 
		{
			if(CommonUtil.isNotNull(controls[i].dictStatus) && CommonUtil.isNotNull(controls[i].getValue()) && controls[i].dictStatus == 'init' )
			{
				dictArr.push(controls[i].id + '#' + controls[i].dict + '#' + controls[i].getValue());
			}
		}
		CommonUtil.ajax({
			url : '/TaDictController/getTaDictByCodeAndKeyMap',
			data : {'dicts' : dictArr},
			showLoadingMsg:false,
			callback:function(data){
				var dicts = eval("(" + data.desc + ")");
				var val = null;
				var dict = null;
				for (var i = 0; i< controls.length; i++) 
				{
					if(CommonUtil.isNotNull(controls[i].getValue()))
					{
						dict = dicts[controls[i].id];
						if(CommonUtil.isNotNull(dict))
						{
							setFieldValue(controls[i].id, controls[i].getValue(), dict[0].text);
						}
					}
				}
			}
		});		

	}

	//显示右侧按钮
	function showBtns(){
		//307 存放同业（活期） 334 投资理财（活期） 344 证券投资基金
		if($.inArray(prdNo,['307','334','344']) > -1)
		{
			tradeManagerWin().setVisible('btn_capitalCashFlow',false);
			tradeManagerWin().setVisible('btn_intCashFlow',false);
		}

		//结构化融资基础资产
		if($("#baseAssetStruct").length > 0){
			tradeManagerWin().setVisible('btn_baseAssetStruct',true);
		}
		//中介费	agencyFeeBtn
		if($("#agencyFeeBtn").length > 0){
			tradeManagerWin().setVisible('btn_agencyFeeBtn',true);
		}
		//通道费用	feesPassAgeWay
		if($("#feesPassAgeWay").length > 0){
			tradeManagerWin().setVisible('btn_feesPassAgeWay',true);
			//费用计划
			tradeManagerWin().setVisible('btn_feesPlan',true);
		}
		//资料影像	image
		if($("#image").length > 0){
			tradeManagerWin().setVisible('btn_image',true);
		}
		//基础资产	baseAsset
		if($("#baseAsset").length > 0){
			tradeManagerWin().setVisible('btn_baseAsset',true);
		}
		//显示摊销计划
		var disAmt = getFieldFormValueById('disAmt');
		if(CommonUtil.isNotNull(disAmt) && disAmt != 0){
			tradeManagerWin().setVisible('btn_amorPlan',true);
		}

		//显示存放同业活期费用计划
		var nostroFlag = getFieldFormValueById('nostroFlag');
		if(CommonUtil.isNotNull(nostroFlag) && nostroFlag == '1'){
			tradeManagerWin().setVisible('btn_nostroIntPlan',true);
		}
	}

	//设置值
	function setFieldValue(id,value,text){
		if(CommonUtil.isNotNull(mini.get(id)))
		{
			mini.get(id).setValue(value);
			mini.get(id).setIsValid(true);

			if(CommonUtil.isNotNull(mini.get(id).setText)){
				if(CommonUtil.isNotNull(text)){
					CommonUtil.isNotNull(mini.get(id).setText(text));

				}else if(mini.get(id).type == 'buttonedit'){
					CommonUtil.isNotNull(mini.get(id).setText(value));

				}
			}
		}
	}

	//设置data
	function setFieldData(id,data){
		if(CommonUtil.isNotNull(mini.get(id)))
		{
			mini.get(id).setData(data);
		}
	}

	//为表单设置注册 valuechanged事件
	function setValueChangedFunction(id,fn){
    	if(mini.get(id)){
    		mini.get(id).on("valuechanged",fn);
    	}
	}
	
	//为表单设置注册 失去焦点事件
	function setFieldBlurFunction(id,fn){
    	if(mini.get(id)){
    		mini.get(id).on("blur",fn);
    	}
    }
	
	//设置表单可用
    function setFieldEnabled(id,enabled){
		if(CommonUtil.isNotNull(mini.get(id)))
		{
			mini.get(id).setEnabled(enabled);
		}
	}
	
	//设置必输字段
	function setFieldRequired(id,required){
		if(CommonUtil.isNotNull(mini.get(id)))
		{
			mini.get(id).setRequired(required);
			if(required){
				$("#" + id + "_label").addClass("red-star");
			}else{
				$("#" + id + "_label").removeClass("red-star");
			}
		}
	}

	//获取表单值
	function getFieldValueById(id){
		var obj = mini.get(id);
		if(CommonUtil.isNotNull(obj)){
			if(CommonUtil.isNotNull(obj.type) && obj.type == 'datepicker'){
				return obj.getFormValue();
			}else{
				return obj.getValue();
			}
		}
		return null;
	}

	//获取表单值 用于日期获取yyyy-MM-dd
	function getFieldFormValueById(id){
		var obj = mini.get(id);
		if(CommonUtil.isNotNull(obj)){
			return obj.getFormValue();
		}
		return null;
	}

	//设置字段隐藏、显示
	function setFieldVisible(id,visible){
		if(CommonUtil.isNotNull(mini.get(id))){
			if(visible){
				//隐藏字段
				$("#"+id+"_label").css("display","");
				$("#"+id+"_td").css("display","");
			}else{
				//隐藏字段
				$("#"+id+"_label").css("display","none");
				$("#"+id+"_td").css("display","none");
			}
		}
	}

	//获取界面参数传递给子页面 被tabs调用
	function getData(){
		var form = new mini.Form("trade_info");
		return form.getData(true);
	}

	function dateFormat(date) {
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		var d = date.getDate();
		return y + "-" +m+ "-" + d;
	}

	//主动调用doValueChanged事件
	function fieldDoValueChanged(id){
		var obj = mini.get(id);
		if(CommonUtil.isNotNull(obj)){
			if(CommonUtil.isNotNull(obj.doValueChanged)){
				obj.doValueChanged();
			}
		}
	}
	
	function onCustQuery(e) {
		var btnEdit = e.sender;
		mini.open({
			url : CommonUtil.baseWebPath() + "/mini_cp/MiniCoreCustQueryEdits.jsp",
			title : "客户选择",
			width : 750,
			height : 500,
			ondestroy : function(action) {
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data && !data.cifNo) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						btnEdit.setIsValid(true);
						btnEdit.focus();

						//结构化融资的要给资产名称一个默认值，默认值为：客户名称+结构化融资 '341','343','332','348','339'
						if($.inArray(prdNo,['341','343','332','348','339']) > -1)
						{
							mini.get("productName").setValue(data.party_name+"-结构化融资");
						}
						setCustData(data);

					}else if(data && data.cifNo){
						data.core_custno = data.cifNo;
						data.party_id = data.cifNo;
						data.party_name = data.custName;
						data.party_code = data.cifNo;

						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						btnEdit.setIsValid(true);
						btnEdit.focus();

						var partyName = mini.get("partyName");
						if(CommonUtil.isNotNull(partyName))
						{
							partyName.setValue(data.party_name);
						}

						//结构化融资的要给资产名称一个默认值，默认值为：客户名称+结构化融资
						if($.inArray(prdNo,['341','343','332','348','339']) > -1)
						{
							mini.get("productName").setValue(data.party_name+"-结构化融资");
						}
						
						CommonUtil.ajax({
							url:'/CounterPartyController/saveTtcounterParty',
							data:data,
							callback:function(data_1){
								CommonUtil.ajax({
									url:'/CounterPartyController/selectCounterParty',
									data:{'party_id':data.party_id},
									callback:function(data_2){
										setCustData(data_2);
									}
								});					
							}
						});
						
					}
				}// end if

			}
		});
	}

	function onCreditCustQuery(e) {
		var btnEdit = e.sender;
		mini.open({
			url : CommonUtil.baseWebPath() + "/mini_cp/MiniCoreCustQueryEdits.jsp",
			title : "客户选择",
			width : 750,
			height : 500,
			ondestroy : function(action) {			
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data && !data.cifNo) {
						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						btnEdit.setIsValid(true);
						btnEdit.focus();

					}else if(data && data.cifNo){
						data.core_custno = data.cifNo;
						data.party_id = data.cifNo;
						data.party_name = data.custName;
						data.party_code = data.cifNo;

						btnEdit.setValue(data.party_id);
						btnEdit.setText(data.party_id);
						btnEdit.setIsValid(true);
						btnEdit.focus();
					}
				}// end if
			}
		});
	}

	function setCustData(data,init){
		setFieldValue("partyName",data.party_name);

		setFieldValue("custType",data.custType);
		CommonUtil.onclickOption({type:'1',obj:mini.get("custType")});

		//产品大类
		setFieldValue("big_kind",data.party_bigkind,data.party_bigkind_name);

		//产品中类	
		setFieldValue("mid_kind",data.party_midkind,data.party_midkind_name);

		//产品小类	
		setFieldValue("small_kind",data.party_smallkind,data.party_smallkind_name);

		//客户会计类型
		setFieldValue("custAccty",data.custAccty);
		//加载下拉框
		CommonUtil.onclickOption({type:'1',obj:mini.get("custAccty")});

		if(CommonUtil.isNull(init))//详情和修改页面初始化值时不自动带出联系人
		{
			if(CommonUtil.isNotNull(data.constact_list) && data.constact_list.length > 0) {//联系人
				setFieldValue("contact",data.constact_list[0].name);
				setFieldValue("contactPhone",data.constact_list[0].cellphone);
				setFieldValue("contactAddr",data.constact_list[0].address);
	
			} else {
				setFieldValue("contact",'');
				setFieldValue("contactPhone",'');
				setFieldValue("contactAddr",'');
			}
		}
		
	}

	function onCostQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_acc/MiniCostMiniManages.jsp",
			title : "成本中心选择",
			width : 750,
			height : 450,
			ondestroy : function(action) {			
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.costcent);
						btnEdit.setText(data.costdesc);
						btnEdit.setIsValid(true);
						btnEdit.focus();

						//成本中心控制
						//二分类 doubleSubject
						if(CommonUtil.isNotNull(mini.get("doubleSubject"))){
							CommonUtil.ajax({
								url : '/ProductApproveController/queryDoubleSubjectByCost',
								data : {'costcent' : data.costcent},
								showLoadingMsg:false,
								callback:function(data){
									if(data && data.obj){
										mini.get("doubleSubject").setValue(data.obj.value);

										CommonUtil.onclickOption({type:'1',obj:mini.get("doubleSubject")});
									}
								}
							});
						}// end if
					}
				}

			}
		});
	}

	//存放同业活期账户选择
	function onNostQuery(e){
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_system/MiniUserParamSelectManages.jsp",
			title : "参数查询",
			width : 750,
			height : 450,
			onload : function(){
				var iframe = this.getIFrameEl();
				var param = {p_type:'1',p_sub_type:'3'};
				if(iframe && iframe.contentWindow && iframe.contentWindow.init){
					iframe.contentWindow.init(param);
				}
			},
			ondestroy : function(action) {		
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						CommonUtil.ajax({
							url : '/ProductApproveController/checkNostAccNo',
							data : {'subjCode' : data.p_value},
							showLoadingMsg:false,
							callback:function(ret){
								if(ret && ret.detail && ret.detail == 'SUCC')
								{
									btnEdit.setValue(data.p_value);
									btnEdit.setText(data.p_value);
									btnEdit.validate();
									btnEdit.focus();

								}else{
									mini.alert("所选的活期账户无效,需要先在[会计核算台=>科目管理]中维护该账户!","系统提示");
									btnEdit.setValue('');
									btnEdit.setText('');
									btnEdit.validate();
									btnEdit.focus();
								}
							}
						});
					}
				}
			}
		});
	}// end function

	function onPortQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_acc/MiniPortMiniManages.jsp",
			title : "投资组合",
			width : 850,
			height : 450,
			ondestroy : function(action) {			
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.portfolio);
						btnEdit.setText(data.portdesc);
						btnEdit.setIsValid(true);
						btnEdit.focus();
					}
				}

			}
		});
	}

	function onInsQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
			title : "机构选择",
			width : 900,
			height : 600,
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						btnEdit.setValue(data.instId);
						btnEdit.setText(data.instName);
						btnEdit.setIsValid(true);
						btnEdit.focus();
					}
				}

			}
		});
	}

	function onRateTypesQuery(e) {
		var btnEdit = this;
		mini.open({
			url : CommonUtil.baseWebPath() +"/mini_market/MiniIrSelectManages.jsp",
			title : "基准利率",
			width : 850,
			height : 450,
			ondestroy : function(action) {			
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data); //必须
					if (data) {
						CommonUtil.ajax({
							url:"/QdTtMktIrController/selectSerieByDate",
							data:{i_code:data.i_code,beg_date:_bizDate,status:"3"},
							showLoadingMsg:false,
							callback : function(data_1) {	
								data = data_1.obj;
								if(data == null) {
									mini.alert("所选基准利率无行情数据,请先维护行情数据","提示");
								} else {
									//今天的基准行情利率
									MktlrRate = parseFloat(data.s_close);
									//设置交易利率
									if(CommonUtil.isNotNull(mini.get('contractRate')))
									{
										var rateDiff = getFieldFormValueById('rateDiff');
										if(CommonUtil.isNull(rateDiff)){
											rateDiff = 0;
										}
										rateDiff = parseFloat(rateDiff) / 10000;

										mini.get('contractRate').setValue(MktlrRate + parseFloat(rateDiff));
									}
									
									btnEdit.setValue(data.i_code);
									btnEdit.setText(data.i_code);
									btnEdit.setIsValid(true);
									btnEdit.focus();
								}
							}// end func
						});
					}// end if
				}

			}
		});
	}

	//查询产品代码对应的描述信息
	function queryProdCodeDesc()
	{
		if(CommonUtil.isNotNull(mini.get("productCode")))
		{
			var productCode = mini.get("productCode").getValue();
			if(CommonUtil.isNotNull(productCode)){
				CommonUtil.ajax({
					url : '/ProductApproveController/queryProductCodeDesc',
					data : {'productCode' : productCode},
					showLoadingMsg:false,
					callback:function(data){
						if(data && data.obj && data.obj.value){
							if($("#productCode_span").length > 0){
								$("#productCode_span").text(data.obj.value);
							}
						}else{
							if($("#productCode_span").length > 0){
								$("#productCode_span").text("");
							}
						}
					}
				});
			}else{
				if($("#productCode_span").length > 0){
					$("#productCode_span").text("");
				}
			}
		}
	}// end func

	/**************************************************************************/
	var initValues = true;//是否是初始化表单,避免表单联动时覆盖掉原来的值
	//注册表单事件
	function addListener()
	{
		//匹配负债类型	
		setValueChangedFunction("transType",function(e){
			var newValue = e.sender.getValue();
			if(newValue == 1) {
				//匹配负债种类
				setFieldValue("matchLiabType","");
				setFieldEnabled("matchLiabType",false);
			} else if(newValue == 2) {
				setFieldEnabled("matchLiabType",true);
			}
		});	
		
		//会计四分类 
		setValueChangedFunction("invtype",function(e){
			var newValue = e.sender.getValue();
			if(newValue == 2 || newValue == 1) {
				setFieldValue("procAmt",getFieldValueById("amt"));
				setFieldEnabled("procAmt",false);
				setFieldValue("disAmt",0);
		
			} else if(newValue == 0 || newValue == 3) {
				if($.inArray(prdNo,['307','334','344']) > -1) {return false}

				setFieldEnabled("procAmt",true);
				
			}
		});	
		
		   //利率类型框
		setValueChangedFunction("rateType",function(e){
			var newValue = e.sender.getValue();
			if (newValue == "1") {//固息
				setFieldValue("rateCode","");//利率代码
				setFieldEnabled("rateCode",false);
				setFieldRequired("rateCode",false);
				
				setFieldValue("rateDiff","");//点差
				setFieldEnabled("rateDiff",false);
				setFieldRequired("rateDiff",false);
				
				if(mini.get("contractRate")){//合同利率
					setFieldEnabled("contractRate",true);
					setFieldRequired("contractRate",true);
				}
				
			} else if (newValue == "2") {//浮息
			
				if(mini.get("rateCode")){//利率代码
					setFieldEnabled("rateCode",true);
					setFieldRequired("rateCode",true);
				}
				if(mini.get("rateDiff")){//利率代码
					setFieldEnabled("rateDiff",true);
					setFieldRequired("rateDiff",true);
				}
				if(mini.get("contractRate")){//利率代码
					setFieldEnabled("contractRate",false);
					setFieldRequired("contractRate",false);
				}

			} else if(newValue == "3"){//净值型
				setFieldValue("rateCode","");//利率代码
				setFieldEnabled("rateCode",false);
				setFieldRequired("rateCode",false);
				
				setFieldValue("rateDiff","");//点差
				setFieldEnabled("rateDiff",false);
				setFieldRequired("rateDiff",false);
				
				setFieldEnabled("contractRate",false);
				setFieldRequired("contractRate",false);

				if(!initValues){
					setFieldValue("contractRate","");//点差
				}
			}
		});
		
		//浮息点差(BP)
		setValueChangedFunction("rateDiff",function(e){
			var newValue = e.sender.getValue();
			if(!initValues){
				if(CommonUtil.isNull(MktlrRate)){
					var i_code = getFieldValueById("rateCode");
					if(CommonUtil.isNull(i_code)){
						mini.alert("请先选择基准利率代码!","提示");
						return;
					}
					CommonUtil.ajax({
						url:"/QdTtMktIrController/selectSerieByDate",
						data:{'i_code':i_code, 'beg_date' : _bizDate,'status' : "3"},
						showLoadingMsg:false,
						callback : function(data) {
							if(CommonUtil.isNull(data) || CommonUtil.isNull(data.obj)) {
								mini.alert("所选基准利率无行情数据,请先维护行情数据","提示");
							} else {
								//今天的基准行情利率
								MktlrRate = parseFloat(data.obj.s_close);
								//设置交易利率
								if(CommonUtil.isNotNull(mini.get('contractRate')))
								{
									newValue = parseFloat(newValue) / 10000;

									mini.get('contractRate').setValue(MktlrRate + parseFloat(newValue));
								}
							}
						}// end func
					});// end ajax

				}else{
					setFieldValue("contractRate",(MktlrRate + parseFloat(newValue)/10000));//点差

				}
			}// end if
		});

		//首次收息日
		setValueChangedFunction("sDate",function(e){
			var newValue = getFieldFormValueById("sDate");
			//起息日期
			var vDate = getFieldValueById("vDate");
			if (CommonUtil.isNotNull(vDate) && CommonUtil.isNotNull(newValue)){
				if(vDate > newValue) {
					mini.alert("首次收息日不能小于起息日期!","系统提示");
					calSdate();//计算首次收息日和最后收息日
					return;
				}
				if(!initValues)
				{
					calSdate(newValue);//计算首次收息日和最后收息日
				}// end if
			}
		});
		
		//最后收息日
		setValueChangedFunction("eDate",function(e){
			var newValue = e.sender.getValue();
			//到期日期
			var mDate = getFieldValueById("mDate");
			if (CommonUtil.isNotNull(mDate)) {
				if(mDate < newValue) {
					setFieldValue("eDate",getFieldFormValueById("mDate"));
					mini.alert("最后收息日不能大于到期日期!","系统提示");
				}
			}
		});
		
		//起息日期
		setValueChangedFunction("vDate",function(e){
			var newValue = e.sender.text;
			setFieldValue("sDate",newValue);
			setFieldValue("tDate",newValue);
			//重置收息频率
			//setFieldValue("intFre",0);

			if(!initValues)
			{
				calSdate();//计算首次收息日和最后收息日
			}
		});
		
		//到期日期
		setValueChangedFunction("mDate",function(e){
			var newValue = e.sender.text;
			//到期日期联动
			setFieldValue("meDate",newValue);
			setFieldValue("eDate",newValue);
			//重置收息频率
			//setFieldValue("intFre",0);

			if(!initValues)
			{
				calSdate();//计算首次收息日和最后收息日
			}
		});
		
		//本金
		setValueChangedFunction("amt",function(e){
			var newValue = e.sender.getValue();
			if(mini.get("procAmt")){
				//默认的成交金额=合同本金金额
				setFieldValue("procAmt",newValue);
				setFieldValue("disAmt",parseFloat(newValue) - parseFloat(getFieldValueById("procAmt")));		
			}
		});
		
		//默认的成交金额=合同本金金额
		setValueChangedFunction("procAmt",function(e){
			var newValue = e.sender.getValue();
			var disAmt = mini.get("disAmt");
			if(disAmt){
				var amtVal = getFieldValueById("amt");
				disAmt.setValue(parseFloat(amtVal) - parseFloat(newValue));
			}
		});
		
		//额度类型选择
		setValueChangedFunction("amtManage",function(e){
			var newValue = e.sender.getValue();
			if(mini.get("creditCust")){
				if(newValue == 'A') {
					setFieldEnabled("creditCust",false);
					setFieldRequired("creditCust",false);
					setFieldValue("creditCust","");

				} else {
					setFieldEnabled("creditCust",true);
					setFieldRequired("creditCust",true);
					setFieldValue("creditCust",getFieldValueById("cNo"));
				}
			}
		});
				
		//下拉框；一次性收息 没有第一次和最后一次收息约定  利随本清
		setValueChangedFunction("intFre",function(e){
			var newValue = e.sender.getValue();
			if(CommonUtil.isNull(mini.get("sDate")) || CommonUtil.isNull(mini.get("eDate"))
				|| CommonUtil.isNull(mini.get("vDate")) || CommonUtil.isNull(mini.get("mDate")))
			{
				return;
			}
			if(newValue == '0'){
				setFieldRequired("sDate",false);
				setFieldVisible("sDate",false);

				setFieldRequired("eDate",false);
				setFieldVisible("eDate",false);

				setFieldVisible("nDays",false);
				
			}else{
				setFieldRequired("sDate",true);
				setFieldVisible("sDate",true);

				setFieldRequired("eDate",true);
				setFieldVisible("eDate",true);

				setFieldVisible("nDays",true);
			}
			
			if(!initValues)
			{
				calSdate();//计算首次收息日和最后收息日
			}// end if
				
		});
		
		//收息方式选择框
		setValueChangedFunction("intType",function(e){
			var newValue = e.sender.getValue();
			if (newValue == "1") {//先收息
				setFieldRequired("intMDate",true);
				setFieldVisible("intMDate",true);

				setFieldRequired("intTerm",true);
				setFieldVisible("intTerm",true);

				setFieldRequired("intAmt",true);
				setFieldVisible("intAmt",true);
				//先收息的时候，需要将先收息的到期日默认为交易到期日，把先收息的金额=利息总和
				var vDate = getFieldValueById("vDate");
				var intMDateVal = getFieldValueById("mDate");
				setFieldValue("intMDate",intMDateVal);//默认提前收息的到期日=交易到期日
				if (CommonUtil.isDate(vDate) && CommonUtil.isDate(intMDateVal)) 
				{
					var term = CommonUtil.dateDiff(vDate, intMDateVal);
					setFieldValue("intTerm",term);
				}
				
			} else if (newValue == "2") {//后收息
				setFieldRequired("intMDate",false);
				setFieldValue("intMDate",null);
				setFieldVisible("intMDate",false);
				
				setFieldRequired("intTerm",false);
				setFieldValue("intTerm",null);
				setFieldVisible("intTerm",false);
				
				setFieldRequired("intAmt",false);
				setFieldValue("intAmt",null);
				setFieldVisible("intAmt",false);
			}
		});
		
		//是否包含活期科目
		setValueChangedFunction("nostroFlag",function(e){
			var newValue = e.sender.getValue();
			var vDate = getFieldValueById("vDate");
			if(newValue != '1'){//不含
				setFieldRequired("nostroAcct",false);
				setFieldVisible("nostroAcct",false);
				setFieldValue("nostroAcct","");
				
				setFieldRequired("nostroVdate",false);
				setFieldVisible("nostroVdate",false);
				setFieldValue("nostroVdate","");
				
				setFieldRequired("nostroRate",false);
				setFieldVisible("nostroRate",false);
				setFieldValue("nostroRate",null);
				
			}else{
				setFieldRequired("nostroAcct",true);
				setFieldVisible("nostroAcct",true);
				
				setFieldRequired("nostroVdate",true);
				setFieldVisible("nostroVdate",true);
				
				setFieldRequired("nostroRate",true);
				setFieldVisible("nostroRate",true);
			}
		});
		
		//是否包含活期科目
		setValueChangedFunction("nostroVdate",function(e){
			var newValue = e.sender.getValue();
			var vDate = getFieldValueById("vDate");//起息日期
			var tDate = getFieldValueById("tDate");//划款日期
			
			if(newValue < vDate){
				mini.alert("存放同业活期起息日期应大于等于起息日", "提示");
				mini.get("nostroVdate").setValue(oldValue);
				return false;
				
			}else if(newValue > tDate){
				mini.alert("存放同业活期起息日期应小于等于划款日", "提示");
				mini.get("nostroVdate").setValue(oldValue);
				return false;
				
			}
		});

		setValueChangedFunction("singleOrGather",function(e){
			var newValue = e.sender.getValue();
			if(newValue == 1) {
				setFieldEnabled("fundInvestment",false);
				setFieldRequired("fundInvestment",false);
				setFieldValue("fundInvestment",null);

			} else {
				setFieldEnabled("fundInvestment",true);
				setFieldRequired("fundInvestment",true);
			}
		});

		setFieldBlurFunction("productCode",function(e){
			queryProdCodeDesc();
		});

		fieldDoValueChanged("transType");
		fieldDoValueChanged("intFre");
		fieldDoValueChanged("rateType");
		fieldDoValueChanged("amtManage");
		fieldDoValueChanged("intType");
		fieldDoValueChanged("nostroFlag");
		fieldDoValueChanged("singleOrGather");
		fieldDoValueChanged("invtype");
		queryProdCodeDesc();

		setTimeout(function(){
			initValues = false;
		},1500);
		
	}//end addListener

	//转换计息基础
	function switchBasis(basis){
		if (basis == "Actual/365") {
			basis = 365;
		} else if (basis == "Actual/Actual(ISMA)") {
			basis = 365;
		} else if (basis == "Actual/365(Fixed)") {
			basis = 365;
		} else if (basis == "Actual/360") {
			basis = 360;
		} else if (basis == "30/360") {
			basis = 360;
		}
		return basis;
	}// end switchBasis

	//计算首次收息日和最后收息日
	function calSdate(sDate_1){
		var vDate = getFieldFormValueById("vDate");//首次收息日
		var mDate = getFieldFormValueById("mDate");//最后收息日
		var intFre = getFieldValueById("intFre");
		var sDate = '';
		if(CommonUtil.isNotNull(sDate_1)){
			sDate = sDate_1;
		}
		if(intFre == '0') {
			setFieldValue("sDate",vDate);
			setFieldValue("eDate",mDate);

		}else{
			if(CommonUtil.isNotNull(intFre) && CommonUtil.isNotNull(vDate) && CommonUtil.isNotNull(mDate))
			{
				CommonUtil.ajax({
					url:"/ProductApproveController/calIntPaydate",
					data:{'intFre':intFre, 'vDate':vDate, 'mDate':mDate ,'sDate':sDate},
					showLoadingMsg:false,
					async:true,
					callback:function(data){
						setFieldValue("sDate",data.obj.sDate);
						setFieldValue("eDate",data.obj.eDate);

						//结构化产品首次收息日置空
						if(vDate == mDate){
							if($.inArray(prdNo,['341','343','332','348','339']) > -1)
							{
								setFieldValue("sDate",null);

							} else {
								setFieldValue("sDate",vDate);

							}
						}
					}// func
				});
			}
		}
	}//end func

	//计算先收息期限
	function intTermCalc () {
		var vDate = getFieldFormValueById("vDate");
		var intMDate = getFieldFormValueById("intMDate");
		var term = null;
		if (CommonUtil.isDate(vDate) && CommonUtil.isDate(intMDate)) {
			term = CommonUtil.dateDiff(vDate, intMDate);
			setFieldValue("intTerm",term);
		}
		var amt = getFieldValueById("amt");
		var contractRate = getFieldValueById("contractRate");
		var basis = switchBasis(getFieldValueById("basis"));
		if (CommonUtil.isNotNull(amt) && CommonUtil.isNotNull(contractRate) && CommonUtil.isNotNull(term) && CommonUtil.isNotNull(basis)) 
		{
			var mInt = CommonUtil.accDiv((amt * contractRate * term),basis);
			mInt = mInt < 0 ? 0 : mInt;
			setFieldValue("intAmt",mInt);
			setFieldValue("mAmt",amt + mInt);
			setFieldValue("mInt",mInt);

			var intType = getFieldValueById("intType");
			if(intType == '1'){
				setFieldValue("procAmt",(amt - mInt));
				setFieldValue("mAmt",amt);
				setFieldValue("mInt",0);
			}else{
				setFieldValue("procAmt",amt);
			}
		}else{
			setFieldValue("mInt",0);
		}
	}// end intTermCalc

	/**************************************************************************/

	/*****************************************/
	//检查表单项,自动计算
	function valid(flag){
		var form = new mini.Form("trade_info");
		form.validate();
		if(form.isValid() == false){
			mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示");
			return false;
		}

		//计息天数计算
		//检查起息日、到期日
		if(CommonUtil.isNotNull(mini.get("vDate")) && CommonUtil.isNotNull(mini.get("mDate")))
		{
			var vDate = mini.get("vDate").getFormValue();
			var mDate = mini.get("mDate").getFormValue();
			if (CommonUtil.isNotNull(vDate) && CommonUtil.isNotNull(mDate)) {
				if(CommonUtil.dateCompare(vDate,mDate)){
					mini.alert("到期日期必须大于起息日期!","消息提示")
					return false;
				}
				setFieldValue("term",CommonUtil.dateDiff(vDate, mDate));
			}else{
				setFieldValue("term",0);
			}
		}

		//检查首次收息日 最后收息日 sDate eDate
		if(CommonUtil.isNotNull(mini.get("sDate")) && CommonUtil.isNotNull(mini.get("eDate")))
		{
			var vDate = getFieldFormValueById("vDate");
			var sDate = getFieldFormValueById("sDate");
			var eDate = getFieldFormValueById("eDate");
			var mDate = getFieldFormValueById("mDate");
			if (CommonUtil.isNotNull(vDate) && CommonUtil.isNotNull(sDate)) {
				if(vDate > sDate){
					mini.alert("首次收息日必须大于等于起息日期!","消息提示")
					return false;
				}
			}

			if (CommonUtil.isNotNull(sDate) && CommonUtil.isNotNull(eDate)) {
				if(sDate > eDate){
					mini.alert("最后收息日必须大于等于首次收息日!","消息提示")
					return false;
				}
			}

			if (CommonUtil.isNotNull(eDate) && CommonUtil.isNotNull(mDate)) {
				if(eDate > mDate){
					mini.alert("最后收息日必须小于等于到期日!","消息提示")
					return false;
				}
			}
		}

		//起息日期、结息日期
		if(CommonUtil.isNotNull(mini.get("vDate")) && CommonUtil.isNotNull(mini.get("iDate")))
		{
			var vDate = mini.get("vDate").getFormValue();
			var iDate = mini.get("iDate").getFormValue();
			if (CommonUtil.isNotNull(vDate) && CommonUtil.isNotNull(iDate)) {
				if(CommonUtil.dateCompare(vDate,iDate)) {
					mini.alert("结息日期必须大于起息日期!","系统提示");
					return false;
				} 
			}
		}

		if(CommonUtil.isNotNull(mini.get("nostroFlag")) && CommonUtil.isNotNull(mini.get("nostroVdate")))
		{
			var nostroFlag = getFieldValueById("nostroFlag");
			if(nostroFlag == "1")
			{
				var nostroVdate = getFieldValueById("nostroVdate");
				var vDate = getFieldValueById("vDate");
				var tDate = getFieldValueById("tDate");
				var nostroRate = getFieldValueById("nostroRate");
				if(parseFloat(nostroRate) <= 0){
					mini.alert("活期利率必须大于0!","系统提示");
					return false;
				}
				if(CommonUtil.isNotNull(nostroVdate) && CommonUtil.isNotNull(vDate) && CommonUtil.isNotNull(tDate)){
					if(nostroVdate < vDate){
						mini.alert("活期起息日必须大于等于起息日期!","系统提示");
						return false;
					}
					if(nostroVdate > tDate){
						mini.alert("活期起息日期必须小于等于划款日期!","系统提示");
						return false;
					}
				}
			}
		}

		//占款期限计算
		//划款日期 到账日期
		if(CommonUtil.isNotNull(mini.get("tDate")) && CommonUtil.isNotNull(mini.get("meDate")))
		{
			var tDate = mini.get("tDate").getFormValue();
			var meDate = mini.get("meDate").getFormValue();
			if (CommonUtil.isNotNull(tDate) && CommonUtil.isNotNull(meDate)) {
				if(CommonUtil.dateCompare(tDate,meDate)) {
					mini.alert("到账日必须大于划款日!","系统提示");
					return false;
				}
				setFieldValue("occupTerm",CommonUtil.dateDiff(tDate, meDate));
			}else{
				setFieldValue("occupTerm",0);
			}
		}
		
		//检查productCode唯一性
		if(checkProdCode() == false){
			return false;
		}

		//额度客户号检查,额度客户号必须是 交易对手号 或者 基础资产中录入的客户号、或者通道客户号
		if(checkCreditCust() == false){
			mini.alert("授信客户号必须从交易对手号、通道方客户号、基础资产客户号中选取!","消息提示");
			return false;
		}

		//到期利息 到期金额计算
		var amt = getFieldValueById("amt");
		if(CommonUtil.isNotNull(amt) &&  amt <= 0){
			mini.alert("本金必须大于0!","消息提示");
			return false;
		}

		var procAmt = getFieldValueById("procAmt");
		if(CommonUtil.isNotNull(procAmt)){
			if(procAmt <= 0){
				mini.alert("成交金额必须大于0!","消息提示");
				return false;
			}
			if(amt > 0){
				if(CommonUtil.accDiv(parseFloat(procAmt) , parseFloat(amt)) >= 100){
					mini.alert("成交金额必须处于本金金额的0至100倍之间!","消息提示");
					return false;
				}
			}
		}


		var contractRate = getFieldValueById("contractRate"); 
		var rateType_1 = getFieldValueById("rateType");
		if(CommonUtil.isNotNull(mini.get("contractRate")) && rateType_1 != null && $.inArray(rateType_1,['1','2']) > -1 && contractRate <= 0){
			mini.alert("利率必须大于0!","消息提示");
			return false;
		}

		var term = getFieldValueById("term"); 
		var basis = switchBasis(getFieldValueById("basis")); 
		if (CommonUtil.isNotNull(amt) && CommonUtil.isNotNull(contractRate) && CommonUtil.isNotNull(term) && CommonUtil.isNotNull(basis)) 
		{
			var mInt = CommonUtil.accDiv((amt * contractRate * term),basis);
			mInt = mInt < 0 ? 0 : mInt;
			setFieldValue("mInt",mInt);
			setFieldValue("intAmt",mInt);
			setFieldValue("mAmt",mInt + amt);
		}

		//计算先收息期限
		var intType = getFieldValueById("intType"); 
		if(CommonUtil.isNotNull(intType) && intType == '1'){
			intTermCalc();
		}

		if(flag){
			mini.alert("校验成功!","系统提示");
		}
		return true;
	}

	//检查prodcode是否重复
	function checkProdCode()
	{
		var flag = true;
		var dealNo = getFieldValueById("dealNo");
		var productCode = getFieldValueById("productCode");
		if(CommonUtil.isNotNull(productCode)){
			CommonUtil.ajax({
				url:"/ProductApproveController/checkProductCode",
				data:{productCode:productCode,dealNo:dealNo},
				async:false,
				callback:function(data){
					if(data && data.obj && data.obj.code != '000'){
						mini.alert(data.obj.msg,"消息提示");
						flag = false;
					}
				}
			});
		}
		return flag;
	}

	//检查授信客户号
	function checkCreditCust(){
		var flag = true;
		var creditCust = getFieldValueById("creditCust");//授信客户号
		var creditType = getFieldValueById("amtManage");//额度类型
		if(CommonUtil.isNotNull(creditCust) && creditType != 'A')
		{
			var cNo = getFieldValueById("cNo");//交易对手号
			var dealNo = getFieldValueById("dealNo"); 
			var param = {
				dealNo     : dealNo ,
				creditCust : creditCust, 
				cNo        : cNo,
				creditType : (creditType == 'C' ? 'company' : creditType)
			};
			CommonUtil.ajax({
				url:"/fbz003/checkCreditCust",
				data:param,
				async:false,
				callback:function(data){
					if(data == false) {
						flag = false;	
					}
				}
			});
		}
		return flag;
	}

	var underlyICode="";
	var underlyAType="";
	var underlyMType="";

	function save(){
		//验证表单数据
		if(valid() == false){
			return;
		}
		//保存前进行额度检查
		var amtManage = getFieldValueById("amtManage");
		if(amtManage == "B"){//占同业额度
			var creditCust = getFieldValueById("creditCust");
			var ccy = getFieldValueById("ccy");
			var amt = getFieldValueById("amt");
			var vDate = getFieldFormValueById("vDate");
			var mDate = getFieldFormValueById("mDate");
			//查询信管系统额度
			CommonUtil.ajax({
				url:"/fbz002/getFbz002",
				data:{mfCustomerId : creditCust,ccy : ccy,amt : amt,vdate : vDate,mdate : mDate},
				callback:function(resp){
					if(resp.obj != null)
					{
						setFieldValue("dealNoticeNo",resp.obj.lmtID);
						var maturity = getFieldFormValueById("mDate");
						maturity = maturity != null ? maturity : getFieldFormValueById("iDate");
						var creditData = {
							mfCustomerId : creditCust,
							dealNo       : getFieldValueById("dealNo"),
							frzcurrency  : ccy,
							frzSum       : amt,
							lmtID        : resp.obj.lmtID,
							maturity     : maturity.replace(/-/g,'/'),
							operTyp      : "00",
							contentType  : "2",
							customerType : "",
							serialNo     : ""
						};
						saveTrade(creditData);

					}else{
						mini.alert(resp.desc,"信管系统提示");
						return;	
					}
					
				},
				callerror:function(resp) {
					mini.alert("向信管校验额度失败","信管系统提示");
					return;
				}
			});

		}else if(amtManage == "C"){//占企业额度
			var creditCust = getFieldValueById("creditCust");
			var dealNo = getFieldValueById("dealNo");
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_trade/MiniFbzManage.jsp",
				title : "信管系统出账记录选择",
				width : 900,
				height : 450,
				onload : function(){
					var iframe = this.getIFrameEl();
					
					var amt = getFieldValueById("amt");

					var param = {cno:creditCust, dealNo:dealNo, amt:amt};
					if(iframe && iframe.contentWindow && iframe.contentWindow.init){
						iframe.contentWindow.init(param);
					}
				},
				ondestroy : function(action) {
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.getData();
						data = mini.clone(data); //必须
						if (data) {
							setFieldValue("dealNoticeNo",data.lmtID);
							var maturity = getFieldFormValueById("mDate");
							maturity = maturity != null ? maturity : getFieldFormValueById("iDate");
							var creditData = {
								mfCustomerId : creditCust,
								dealNo       : dealNo,
								frzcurrency  : data.currency,
								frzSum       : data.putoutSum,
								lmtID        : data.lmtID,
								maturity     : maturity.replace(/-/g,'/'),
								operTyp      : "01",
								contentType  : "1",
								customerType : "",
								serialNo     : data.serialNo
							};
							saveTrade(creditData);
						}
					}// end if
				} //end ondestroy
			});

		}else{//不占额度 直接保存
			saveTrade(null);
		}
	}

	//保存交易数据
	function saveTrade(creditData)
	{
		var subParams =  null;
		var partyParams = null;
		var form = new mini.Form("trade_info");
		var data = form.getData(true);
		data.trdtype = "3001";//业务类型
		data.underlyICode=underlyICode;
		data.underlyAType=underlyAType;
		data.underlyMType=underlyMType;
		data.dealType = dealType;
		data.prdNo = prdNo;
		data["productApproveSubs"] = subParams;
		data["participantApprove"] = partyParams;

		var saveUrl ="/ApproveManageController/saveApprove";
		if(data.dealType == "2")
		{
			saveUrl = "/TradeManageController/saveTrade";
		}else{
			saveUrl = "/ApproveManageController/saveApprove";
		}

		var paramData = mini.encode(data);
		CommonUtil.ajax({
			url:saveUrl,
			data:paramData,
			callback:function(data){
				tradeManagerWin().param.dealNo = data.trade_id;
				mini.get("dealNo").setValue(data.trade_id);
				mini.alert("保存成功.","提示");
				//保存授信信息
				if(CommonUtil.isNotNull(creditData)){
					if(CommonUtil.isNull(creditData.dealNo)){
						creditData.dealNo = data.trade_id;
					}
					saveLimit(creditData);
				}
				//显示按钮
				showBtns();
			}
		});
	}

	/***
	 * 保存额度记录到后台
	 * mfCustomerId 授信客户号
	 * dealNo 交易流水号
	 * frzcurrency 币种
	 * frzSum 占用金额
	 * lmtID 授信编号
	 * maturity 到期日期
	 * operTyp 00 01
	 * contentType 企业 1 同业 2
	 * customerType 客户类型
	 * serialNo 出账流水号
	 * ***/
	function saveLimit(param){
		CommonUtil.ajax({
			url:"/fbz003/saveLimit",
			data:param,
			callback:function(resp){
				
			},
			callerror:function(resp) {
				return 'error';
			}
		});
	}