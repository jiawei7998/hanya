﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <title></title>
  </head>
  <body style="width:100%;height:100%;background:white">
		
	<div class="mini-splitter" style="width:100%;height:100%;">
	    <div size="90%" showCollapseButton="false">
	        <div class="mini-fit"  style="background:white">
				<div id="tabs1" class="mini-tabs brief"  activeIndex="0" style="height:100%;background:white">
				</div>
			</div>
	    </div>
	    <div id="functionIds" showCollapseButton="true">
	    	<div style="margin-bottom:10px; margin-top:50px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_valid"   onclick="valid(true)">校验要素</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_save"          onclick="save">保存交易</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_capitalCashFlow"    onclick="addCapitalCashFlow">还本计划</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_intCashFlow"        onclick="addIntCashFlow">收息计划</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_baseAsset"        visible="false"   onclick="addBaseAsset">基础资产</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_image"            visible="false"   onclick="addAttachTreeManage">资料影像</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_baseAssetStruct"  visible="false"   onclick="addBaseAssetStruct">基础资产(结构化融资)</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_feesPassAgeWay"   visible="false"   onclick="addPassageWayFees">通道费用</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_agencyFeeBtn"     visible="false"   onclick="addAgencyFees">中介费</a></div>
			
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_amorPlan"      visible="false"   onclick="addAmorPlan">摊销计划</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_feesPlan"      visible="false"   onclick="addFeesPlan">费用计划</a></div>
			<div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px" id="btn_nostroIntPlan" visible="false"   onclick="addNostroIntPlan">存放同业活期收息计划</a></div>
		</div>      
	</div>
	

	<script>
		mini.parse();
		//获取当前tab
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;

		var param = params;
		var dealType = params.dealType;
		var prdName = params.prdName;
		var prdProp = params.prdProp;
		var prdRootType = params.prdRootType;

		var operType = params.operType;// A新增 D详情 U更新 approve审批
		if(CommonUtil.isNull(operType)){
			operType = "A";
		}
		var prdNo = params.prdNo;
		var winId = null;
		if(operType == "A"){
			winId = "tradeEditTabs_" + prdNo + "_" + operType;

		} else {
			winId = "tradeEditTabs_" + prdNo + "_" + operType + "_" + param.dealNo;

		}
		top[winId] = window;

		var approveTypeName = dealType == '2' ? '正式交易' : '事前审批';

		var attachTreeMap = null;

		
		if($.inArray(operType,['A','U']) == -1){//不是新增和修改 禁用按钮
			mini.get("btn_save").setVisible(false);
			mini.get("btn_valid").setVisible(false);	
		}

		var tabs = mini.get("tabs1");

		function addTab(tab) {
			if(CommonUtil.isNotNull(tabs.getTab(tab.name)))
			{
				tabs.activeTab(tabs.getTab(tab.name));
				return;
			}
			tabs.addTab(tab);
			tabs.activeTab(tab);
		}// end add

		function removeTab() {
			var tab = tabs.getActiveTab();
			if (tab) {
				tabs.removeTab(tab);
			}
		}

		function addBaseAsset(){
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniBaseAsset.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "基础资产",id : "baseAsset",name : "baseAsset", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addBaseAssetStruct(){
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniBaseAssetStruct.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "结构化融资基础资产",id : "baseAssetStruct",name : "baseAssetStruct", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addPassageWayFees(){
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniPassageWayFeesPlan.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "通道费用",id : "passageWayFees",name : "passageWayFees", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addAgencyFees(){
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniAgencyFees.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "中介费用",id : "agencyFees",name : "agencyFees", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addAttachTreeManage(){
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniAttachTreeManage.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "资料影像",id : "attachTreeManage",name : "attachTreeManage", url: url, showCloseButton: true};
 
			if(CommonUtil.isNotNull(param.dealNo)){
				//refType 1 客户 2产品
				attachTreeMap = {prdNo:prdNo,prdName:prdName, refType:"1", refId:param.dealNo, dealNo:param.dealNo};
			}
			addSubTab(tab);
		}

		function addCapitalCashFlow(){
			var url = CommonUtil.baseWebPath() + "/mini_cashflow/MiniCapitalCashFlowEdit.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "还本计划",id : "capitalCashFlow",name : "capitalCashFlow", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addIntCashFlow(){
			var url = CommonUtil.baseWebPath() + "/mini_cashflow/MiniIntCashFlowEdit.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "收息计划",id : "intCashFlow",name : "intCashFlow", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addAmorPlan(){
			var url = CommonUtil.baseWebPath() + "/mini_cashflow/MiniAmorCashFlow.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "摊销计划",id : "amorPlan",name : "amorPlan", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addFeesPlan(){
			var url = CommonUtil.baseWebPath() + "/mini_cashflow/MiniFeeCashFlowEdit.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "费用计划",id : "feesPlan",name : "feesPlan", url: url, showCloseButton: true};
			addSubTab(tab);
		}

		function addNostroIntPlan(){
			var url = CommonUtil.baseWebPath() + "/mini_cashflow/MiniNostroIntCashFlowEdit.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			var tab = { title: "存放同业活期费用",id : "nostroIntPlan",name : "nostroIntPlan", url: url, showCloseButton: true};
			addSubTab(tab);
		}
		
		function getTabData(tabName){
			var tab = tabs.getTab(tabName);
			if(CommonUtil.isNotNull(tab)){
				var iframe = tabs.getTabIFrameEl(tab);
				//获取子页面返回数据
				if(iframe.contentWindow.getData  && typeof(iframe.contentWindow.getData)=="function"){
					return mini.clone(iframe.contentWindow.getData());
				}
			}
			return null;
		}

		function getFieldValue(tabName,id){
			var tab = tabs.getTab(tabName);
			if(CommonUtil.isNotNull(tab)){
				var iframe = tabs.getTabIFrameEl(tab);
				//获取子页面返回数据
				if(iframe.contentWindow && iframe.contentWindow.getFieldValueById  && typeof(iframe.contentWindow.getFieldValueById)=="function"){
					return mini.clone(iframe.contentWindow.getFieldValueById(id));
				}
			}
			return null;
		}
		
		//打开二级Tabs
		function addSubTab(tab){
			var trade = tabs.getTab("tadeInfo");
			if(CommonUtil.isNotNull(trade)){
				var iframe = tabs.getTabIFrameEl(trade);
				//获取子页面返回数据
				var tradeId = CommonUtil.isNotNull(iframe.contentWindow.getFieldValueById) ? iframe.contentWindow.getFieldValueById("dealNo") : "";
				if(CommonUtil.isNotNull(tradeId))
				{
					addTab(tab);
				}else{
					mini.alert("请先保存合同信息!","提示信息");
				}
			}else{
				mini.alert("请先保存合同信息!","提示信息");
			}
		}

		function save(){
			var tab = tabs.getActiveTab();
			var iframe = tabs.getTabIFrameEl(tab);
			if(CommonUtil.isNotNull(iframe.contentWindow.save)){
				try{
				  iframe.contentWindow.save();
				}catch(e){	
				}
			}
		}
		
		function valid(flag){
			var tab = tabs.getActiveTab();
			var iframe = tabs.getTabIFrameEl(tab);
			if(CommonUtil.isNotNull(iframe.contentWindow.valid)){
				iframe.contentWindow.valid(flag);
			}
		}

		function setVisible(id,visible){
			if(CommonUtil.isNotNull(mini.get(id)))
			{
				mini.get(id).setVisible(visible);
			}
		}
		

		$(document).ready(function(){
			//请求URL
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEdit.jsp?prdNo=" + prdNo + "&operType=" + operType + "&dealNo=" + param.dealNo;
			//打开新的tabs
			var tab = {title:"合同信息" ,id : "tadeInfo", name : "tadeInfo",url: url, showCloseButton: false};
			addTab(tab);
		});

	</script>
</body>
</html>