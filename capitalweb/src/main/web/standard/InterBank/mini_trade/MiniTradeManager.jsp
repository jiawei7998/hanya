<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
    <legend>交易查询</legend>
	<div style="width:100%;">
		<div id="search_form" class="mini-form" width="80%">
			<input id="sponInst" name="sponInst" class="mini-buttonedit" onbuttonclick="onInsQuery" labelField="true"  label="所属机构：" labelStyle="text-align:right;" allowInput="false" emptyText="请选择机构"/>
			<input id="dealNo" name="dealNo" class="mini-textbox" vtype="maxLength:50" labelField="true"  label="交易单号：" labelStyle="text-align:right;"  emptyText="请输入交易单号"/>
			<input id="cNo" name="cNo" class="mini-textbox" labelField="true"  label="客户号：" labelStyle="text-align:right;" emptyText="请输入客户号"/>

			<input id="amtFrom" name="amtFrom" class="mini-textbox" vtype="float" labelField="true"  label="本金区间从：" labelStyle="text-align:right;" emptyText="请输入金额起始区间"/>
			<input id="amtTo" name="amtTo" class="mini-textbox" vtype="float" labelField="true"  label="本金区间到：" labelStyle="text-align:right;" emptyText="请输入金额到期区间"/>
			<input id="approveStatus" name="approveStatus" class="mini-combobox" data = "CommonUtil.serverData.dictionary.ApproveStatus" labelField="true"  label="审批状态：" labelStyle="text-align:right;" emptyText="请选择审批单状态">

			<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"  >查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
			</span>
		</div>
	</div>
	</fieldset>

	<table width="100%">
		<tr>
			<td width="60%" style="padding:10px 0 5px 5px">
					<a id="add_btn" class="mini-button" style="display: none"  >新增</a>
					<a id="edit_btn" class="mini-button" style="display: none"  >修改</a>
					<a id="delete_btn" class="mini-button" style="display: none"  >删除</a>
					<a id="approve_commit_btn" class="mini-button" style="display: none"   >审批</a>
					<a id="verify_commit_btn" class="mini-button" style="display: none"   >放款</a>
					<a id="approve_mine_commit_btn" class="mini-button" style="display: none"  >提交预审</a>
					<a id="verify_mine_commit_btn" class="mini-button" style="display: none"  >提交正式审批</a>
					<a id="approve_log" class="mini-button" style="display: none"   >审批日志</a>
					<a id="print_bk_btn" class="mini-button" style="display: none"    onclick="print">打印</a>
			</td>

			<td width="40%">
				<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;width:100%; " labelField="true" 
					label="审批列表选择：" labelStyle="text-align:right;" 
					value="approve" textField="text" valueField="id" multiSelect="false"
					data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'},{id:'mine',text:'我发起的'}]" 
					onvaluechanged="checkBoxValuechanged">
				</div>
			</td>
		</tr>
	</table>
	<div id="tradeManage" class="mini-fit">      
		<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
			allowAlternating="true" allowResize="true" onrowdblclick="onRowDblClick" border="true" onrowclick="onRowClick"
			frozenStartColumn="0" frozenEndColumn="1" multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" width="50">序号</div>
				<div field="dealNo" width="180" headerAlign="center" align="center" allowSort="true">交易单号</div>   
				<div field="dealType" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'DealType'}" align="center" headerAlign="center">审批类型</div>
				<div field="approveStatus" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" align="center" headerAlign="center">审批状态</div>
				<div field="counterParty.party_name" width="200" headerAlign="center" allowSort="true">客户名称</div>   
				<div field="product.prdName" width="200" headerAlign="center" allowSort="true">产品名称</div>   
				<div field="aDate" width="100" headerAlign="center" align="center" allowSort="true">审批开始日期</div>   
				<div field="ccy" width="80" headerAlign="center" align="center" allowSort="true">币种</div>   
				<div field="amt" numberFormat="#,0.00" align="right" width="150" headerAlign="center" allowSort="true">本金</div>
				<div field="contractRate" numberFormat="p4" align="right" width="80" allowSort="true">合同利率</div>
				<div field="vDate" width="80" headerAlign="center" align="center" allowSort="true">起息日期</div>   
				<div field="mDate" width="80" headerAlign="center" align="center" allowSort="true">到期日期</div>   
				<div field="user.userName" width="100" headerAlign="center" allowSort="true">审批发起人</div>   
				<div field="institution.instName" width="120" headerAlign="center" allowSort="true">审批发起机构</div>   
				<div field="lastUpdate" width="130" headerAlign="center" align="center" allowSort="true">更新时间</div>
			</div>
		</div>  
	</div>

	<script>
		//获取当前tab
		var currTab = top["win"].tabs.getActiveTab();
		var params = currTab.params;
		var dealType = params.dealType;
		var prdNo = params.prdNo;
		var prdName = params.prdName;
		var prdProp = params.prdProp;
		var prdRootType = params.prdRootType;

		var approveTypeName = dealType == '2' ? '正式交易' : '事前审批';
		
		mini.parse();
		top["tradeManager_" + prdNo] = window;

		//返回全部setVisible(true)的按钮id

		var grid = mini.get("datagrid");
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
		function search(pageSize,pageIndex)
		{
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("表单填写错误,请确认!","提示信息")
				return;
			}

			var data = form.getData();
			data['pageNumber'] = pageIndex+1;
			data['pageSize'] = pageSize;
			data['prdNo'] = prdNo;
			data['dealType'] = dealType;
			data['branchId'] = branchId;
			var url = null;

			var approveType = mini.get("approveType").getValue();
			if(approveType == "mine"){
				url = "/ProductApproveController/searchPageProductApproveByMyself";

			}else if(approveType == "approve"){
				url = "/ProductApproveController/searchPageProductApproveUnfinished";

			}else{
				url = "/ProductApproveController/searchPageProductApproveFinished";
			}

			var params = mini.encode(data);
			CommonUtil.ajax({
				url:url,
				data:params,
				showLoadingMsg:false,
				messageId:mini.loading('正在查询数据...', "请稍后"),
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
		        	grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
			
		}// end search

		function query(){
			search(grid.pageSize,0);
		}

		function setBtnVisibleByTab(){
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var approveType = mini.get("approveType").getValue();
				if (approveType == "approve") {
					if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
					if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
					if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
					if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
					if (visibleBtn.verify_commit_btn) mini.get("verify_commit_btn").setVisible(true);
					if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
					if (visibleBtn.verify_mine_commit_btn) mini.get("verify_mine_commit_btn").setVisible(false);

				} else if (approveType == "finished" || approveType == "抄送我的") {
					if (visibleBtn.add_btn) mini.get("add_btn").setVisible(false);
					if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(false);
					if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(false);
					if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
					if (visibleBtn.verify_commit_btn) mini.get("verify_commit_btn").setVisible(false);
					if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
					if (visibleBtn.verify_mine_commit_btn) mini.get("verify_mine_commit_btn").setVisible(false);

				} else {
					if (visibleBtn.add_btn) mini.get("add_btn").setVisible(true);
					if (visibleBtn.edit_btn) mini.get("edit_btn").setVisible(true);
					if (visibleBtn.delete_btn) mini.get("delete_btn").setVisible(true);
					if (visibleBtn.approve_commit_btn) mini.get("approve_commit_btn").setVisible(false);
					if (visibleBtn.verify_commit_btn) mini.get("verify_commit_btn").setVisible(false);
					if (visibleBtn.approve_mine_commit_btn) mini.get("approve_mine_commit_btn").setVisible(false);
					if (visibleBtn.verify_mine_commit_btn) mini.get("verify_mine_commit_btn").setVisible(true);

				}
				if (dealType == "1") {
					if (visibleBtn.verify_commit_btn) mini.get("verify_commit_btn").setVisible(false);
					if (visibleBtn.verify_mine_commit_btn) mini.get("verify_mine_commit_btn").setVisible(false);
				}
			});
		}

		function checkBoxValuechanged(){
			setBtnVisibleByTab();
			search(grid.pageSize,0);
		}

		function onRowClick(e) {
			var row = grid.getSelected();
			if(row){
				mini.get('approve_mine_commit_btn').setEnabled(false);
				mini.get('approve_commit_btn').setEnabled(false);
				mini.get('verify_mine_commit_btn').setEnabled(false);
				mini.get('verify_commit_btn').setEnabled(false);

				mini.get('delete_btn').setEnabled(false);
				mini.get('edit_btn').setEnabled(false);
				/**
				1	:就绪,2	:计算中,3	:新建,4	:待审批,                 
				5	:审批中,6	:审批通过,10:执行完成,11:待核实,         
				15:核实中,12:核实完成,16:核实拒绝,13:出账中,         
				14:出账完成,17:交易冲正,7	:审批拒绝,8	:审批注销,     
				9	:执行中,1	:待经办,2	:待复核,3	:复核通过,0	:复核拒绝
				 * **/
				if(row.dealType == "1" ){
					if(row.approveStatus == "3"){
						mini.get('approve_mine_commit_btn').setEnabled(true);
						mini.get('delete_btn').setEnabled(true);
					}
					if(row.approveStatus == "4" || row.approveStatus == "5"){
						mini.get('approve_commit_btn').setEnabled(true);
					}
					if(row.approveStatus == "3" || row.approveStatus == "4"){
						mini.get('edit_btn').setEnabled(true);
					}
				}else if(row.dealType == "2"){
					if(row.approveStatus == "3"){
						mini.get('verify_mine_commit_btn').setEnabled(true);
						mini.get('delete_btn').setEnabled(true);
						mini.get('approve_log').setEnabled(false);
					}else{
						mini.get('approve_log').setEnabled(true);
					}
					if(row.approveStatus == "11" || row.approveStatus == "15"){
						mini.get('verify_commit_btn').setEnabled(true);
					}
					if(row.approveStatus == "3" || row.approveStatus == "11"){
						mini.get('edit_btn').setEnabled(true);
					}
				}
			}
		}
			
		function onRowDblClick(e) {
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp";
				var dealNo = row.dealNo;
				var tab = {
					id : prdNo + "_" + dealType + "_" + dealNo + "_detail",
					name : prdNo + "_" + dealType + "_" + dealNo + "_detail",
					iconCls : "",
					title : approveTypeName + "交易详情("+dealNo+")",
					url : url,
					showCloseButton:true
				};
				var paramData = {'dealNo':dealNo,'dealType':dealType,'prdNo':prdNo,'prdName':prdName,
					'prdProp':prdProp,'prdRootType':prdRootType,'operType':'D',selectData:row};
				
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function add(){
			var url = CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp";
			var tab = {
				id : prdNo + "_" + dealType + "_add",
				name : prdNo + "_" + dealType + "_add",
				iconCls : "",
				title : prdName + "_" + approveTypeName + "新增",
				url : url,
				showCloseButton:true
			};
			var paramData = {'dealType':dealType,'prdNo':prdNo,'prdName':prdName,
				'prdProp':prdProp,'prdRootType':prdRootType,'operType':'A','closeFun':'query','pWinId':'tradeManager_' + prdNo};
			
			CommonUtil.openNewMenuTab(tab,paramData);
		}

		function update()
		{
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp";
				var dealNo = row.dealNo;
				var tab = {
					id : prdNo + "_" + dealType + "_" + dealNo + "_update",
					name : prdNo + "_" + dealType + "_" + dealNo + "_update",
					iconCls : "",
					title : approveTypeName + "交易修改("+dealNo+")",
					url : url,
					showCloseButton:true
				};
				var paramData = {'dealNo':dealNo,'dealType':dealType,'prdNo':prdNo,'prdName':prdName,
					'prdProp':prdProp,'prdRootType':prdRootType,'operType':'U',selectData:row,
					'closeFun':'query','pWinId':'tradeManager_' + prdNo};
				
				CommonUtil.openNewMenuTab(tab,paramData);

			} else {
				mini.alert("请选中一条记录！","消息提示");
			}
		}

		function clear(){
			var form = new mini.Form("#search_form");
			form.clear();
		}
		
		//删除
		function del(){
			//必须选中一条记录
			var rows = grid.getSelecteds();
			if(rows.length == 0){
				mini.alert('请至少选择一条记录！', '消息提示');
				return;
			}
			mini.confirm("您确认要删除选中记录?", "系统提示",
				function (action) {
					if (action == "ok") {
						var dealNos = new Array();
						for(var n = 0; n < rows.length; n++)
						{
							if(rows[n].approveStatus == "3")
							{
								dealNos.push(rows[n].dealNo);
							}else{
								mini.alert("只能删除新增状态的交易.","系统提示");
								return;
							}
						}
						CommonUtil.ajax({
							url:"/ProductApproveController/removeProductApprove",
							data:dealNos,
							callback : function(data) {
								mini.alert("删除成功.","系统提示");
								search(grid.pageSize,grid.pageIndex);
							}
						});
					}
				}
			);
		}

		function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instId);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}

		//打印
		function print(){
			var selections = grid.getSelecteds();
			if(selections == null || selections.length == 0){
			    mini.alert('请选择一条要打印的数据！','系统提示');
			    return false;
			}else if(selections.length>1){
			    mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			    return false;
			}
			var canPrint = selections[0].dealNo;
			
			if(!CommonUtil.isNull(canPrint)){
				var actionStr = CommonUtil.pPath + "/sl/PrintUtilController/exportload/print/1900/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			};
		}

		/**************审批**************/
		//	查询日志
		function appLog(selections){
			var flow_type = null;
			if(selections.length <= 0){
				mini.alert("请选择要操作的数据","系统提示");
				return;
			}
			if(selections.length > 1){
				mini.alert("系统不支持多笔操作","系统提示");
				return;
			}
			if(selections[0].tradeSource == "3"){
				mini.alert("初始化导入的业务没有审批日志","系统提示");
				return false;
			}
			if(selections[0].dealType == "1"){
				flow_type = "1";
			}else if(selections[0].dealType == "2"){
				flow_type = "4";
			}
			Approve.approveLog(flow_type,selections[0].dealNo);
		};
		
		//	提交预审 、预申请审批	
		function approve(selections)
		{
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("系统不支持多笔提交","系统提示");
				return false;
			}
			if(selections[0]["approveStatus"] != "3" ){ //不等于新建状态进入交易审批界面
				CommonUtil.ajax( {
					url : "/ProductApproveController/searchPageTotalVerify",
					data : {dealNo : selections[0]["dealNo"],
					        dealType : dealType,
					        approveStatus : selections[0]["approveStatus"]},
					callback : function(data) {
						var tab = {
							id   : prdNo + "_" + dealType + "_" + selections[0]["dealNo"] + "_approve",
							name : prdNo + "_" + dealType + "_" + selections[0]["dealNo"] + "_approve",
							iconCls:null,
							title  : prdName + (dealType == "1" ? "预审批" : "正式审批"),
							url:CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp",
							showCloseButton:true
						};
						var paramData = {'dealNo':selections[0]["dealNo"],'dealType':dealType,'prdNo':prdNo,'prdName':prdName,
							'prdProp':prdProp,'prdRootType':prdRootType,'operType':'approve','closeFun':'query','pWinId':'tradeManager_' + prdNo};
						
						paramData.flow_type = Approve.FlowType.BusinessApproveFlow;//预审批
						paramData.task_id = selections[0]["taskId"];//流程任务
						paramData.serial_no = selections[0]["dealNo"];//交易流水
						CommonUtil.openNewMenuTab(tab,paramData);
					}
				});
			}else{
				//弹出流程选择
				Approve.approveCommit(Approve.FlowType.BusinessApproveFlow,selections[0]["dealNo"],Approve.OrderStatus.New,function(){
					search(grid.pageSize,grid.pageIndex);
				},"",prdNo);			
			}
		};
		
		//	提交正式审批 、 正式申请审批
		function verify(selections)
		{
			if(selections.length == 0){
				mini.alert("请选中一条记录！","消息提示");
				return false;
			}else if(selections.length > 1){
				mini.alert("暂不支持多笔提交","系统提示");
				return false;
			}
			
			if(selections[0]["approveStatus"] != "3" ){
				CommonUtil.ajax({
					url : "/ProductApproveController/searchProductApproveUnfinishedCount",
					data : {'dealNo' : selections[0]["dealNo"], 'dealType' : dealType, 'branchId' : branchId},
					callback : function(data) {
						if(data && data.detail == null){
							mini.alert("该笔交易状态已经改变,请刷新后查看!","系统提示");
							search(grid.pageSize,grid.pageIndex);

						}else{
							CommonUtil.ajax({
								url : "/ProductApproveController/searchPageTotalVerify",
								data : {dealNo:selections[0]["dealNo"], dealType : dealType, approveStatus : selections[0]["approveStatus"]},
								callback : function(data) {
									var tab = {
										id   : prdNo + "_" + dealType + "_" + selections[0]["dealNo"] + "_verify",
										name : prdNo + "_" + dealType + "_" + selections[0]["dealNo"] + "_verify",
										iconCls:null,
										title  : prdName + (dealType == "1" ? "预审批" : "正式审批"),
										url:CommonUtil.baseWebPath() + "/mini_trade/MiniTradeEditTabs.jsp",
										showCloseButton:true
									};
									var paramData = {'dealNo':selections[0]["dealNo"],'dealType':dealType,'prdNo':prdNo,'prdName':prdName,
										'prdProp':prdProp,'prdRootType':prdRootType,'operType':'approve','closeFun':'query','pWinId':'tradeManager_' + prdNo};
									
									paramData.flow_type = Approve.FlowType.VerifyApproveFlow;//正式审批
									paramData.task_id = selections[0]["taskId"];//流程任务
									paramData.serial_no = selections[0]["dealNo"];//交易流水
									CommonUtil.openNewMenuTab(tab,paramData);
								}
							});

						}// end if
					}// end function
				});	

			}else{
				//先检查有没有交易单撤销
				CommonUtil.ajax({
					url:"/DurationController/ifExistApproveCancel",
					data:{'refNo':selections[0]["dealNo"],'ApproveStatus':['3','7','8']},
					callback : function(data) {
						if(data.obj != null){
							mini.alert('该交易单已存在撤销记录<br>【业务员：'+data.obj.sponsor+'】<br>【单号：'+data.obj.dealNo+'】','系统提示');
							return false;
						}else{
							Approve.approveCommit(Approve.FlowType.VerifyApproveFlow,selections[0]["dealNo"],Approve.OrderStatus.New,function(){
								search(grid.pageSize,grid.pageIndex);
							},"",prdNo);
						}
					}
				});
			}
		};

		//注册按钮事件
		//查询
		mini.get("search_btn").on("click",function(){
			mini.get("search_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				query();
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
			mini.get("search_btn").setEnabled(true);
		});

		//新增
		mini.get("add_btn").on("click",function(){
			mini.get("add_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				add();
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
			mini.get("add_btn").setEnabled(true);
		});

		//修改
		mini.get("edit_btn").on("click",function(){
			mini.get("edit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				update();
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);
			mini.get("edit_btn").setEnabled(true);
		});

		//删除
		mini.get("delete_btn").on("click",function(){
			mini.get("delete_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				del();
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);			
			mini.get("delete_btn").setEnabled(true);
		});

		//清空
		mini.get("clear_btn").on("click",function(){
			mini.get("clear_btn").setEnabled(false);
			clear();
			mini.get("clear_btn").setEnabled(true);
		});

		//事前审批 提交审批
		mini.get("approve_mine_commit_btn").on("click",function(){
			mini.get("approve_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				approve(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_mine_commit_btn").setEnabled(true);
		});
		
		//事前审批 审批
		mini.get("approve_commit_btn").on("click",function(){
			mini.get("approve_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				approve(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("approve_commit_btn").setEnabled(true);
		});


		//正式交易 提交审批
		mini.get("verify_mine_commit_btn").on("click",function(){
			mini.get("verify_mine_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	

			mini.get("verify_mine_commit_btn").setEnabled(true);
		});

		//正式交易 审批
		mini.get("verify_commit_btn").on("click",function(){
			mini.get("verify_commit_btn").setEnabled(false);
			var messageid = mini.loading("系统正在处理...", "请稍后");
			try {
				verify(grid.getSelecteds());
			} catch (error) {
				
			}
			mini.hideMessageBox(messageid);	
			mini.get("verify_commit_btn").setEnabled(true);
		});

		//审批日志
		mini.get("approve_log").on("click",function(){
			appLog(grid.getSelecteds());
		});

		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				setBtnVisibleByTab();
				search(10, 0);
			});
		});
	</script>
</body>
</html>