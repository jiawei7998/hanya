<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<title>正式交易查询页面</title>
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset class="mini-fieldset">
	<legend>查询</legend>
	<div id="search_form" >
		<input id="sponInst" name="sponInst" class="mini-buttonedit" onbuttonclick="onInsQuery" emptyText="请选择机构..." labelField="true"
			label="所属机构：" labelStyle="text-align:right;" />
		<input id="dealNo" name="dealNo" class="mini-textbox" labelField="true" label="交易单编号：" labelStyle="text-align:right;" emptyText="请输入交易单编号"/>
		<input id="amtFrom" name="amtFrom" class="mini-textbox" vtype="float" labelField="true" label="本金区间从：" labelStyle="text-align:right;" emptyText="请输入本金起始金额"/>
		<input id="amtTo" name="amtTo" class="mini-textbox" vtype="float" labelField="true" label="~" labelStyle="text-align:center;" emptyText="请输入本金结束金额" />
		<input id="approveStatus" name="approveStatus" class="mini-combobox" data="CommonUtil.serverData.dictionary.ApproveStatus"
			labelField="true" label="审批状态：" labelStyle="text-align:right;" emptyText="请选择审批状态...">
		<input id="productName" name="productName" class="mini-textbox" labelField="true" label="资产名称：" labelStyle="text-align:right;"
			emptyText="请输入资产名称" />
		<input id="productCode" name="productCode" class="mini-textbox" labelField="true" label="资产代码：" labelStyle="text-align:right;"
			emptyText="请输入资产代码" />
		<input id="saleType" name="saleType" class="mini-combobox" data="CommonUtil.serverData.dictionary.saleType" labelField="true"
			label="业务类型：" labelStyle="text-align:right;" emptyText="请选择业务类型..." />
		<input id="cost" name="cost" class="mini-buttonedit" onbuttonclick="onCostQuery" labelField="true" label="成本中心：" labelStyle="text-align:right;"
			emptyText="请输入成本中心" />
		<input id="port" name="port" class="mini-buttonedit" onbuttonclick="onPortQuery" labelField="true" label="投资组合：" labelStyle="text-align:right;"
			emptyText="请输入投资组合" />
		<input id="verifyVDate" name="verifyVDate" class="mini-datepicker" vtype="date:yyyy-MM-dd" validateOnLeave="true"
			dateErrorText="格式为yyyy-MM-dd" valueType="string" labelField="true" label="起息日开始：" labelStyle="text-align:right;" emptyText="请选择开始日期"/>
		<input id="verifyVDate2" name="verifyVDate2" class="mini-datepicker" vtype="date:yyyy-MM-dd" validateOnLeave="true"
			dateErrorText="格式为yyyy-MM-dd" valueType="string" labelField="true" label="起息日结束：" labelStyle="text-align:right;" emptyText="请选择结束日期"/>
		
		<input id="verifyVDateRange" name="verifyVDateRange" class="mini-combobox" data=""
			labelField="true" label="起息日区间：" labelStyle="text-align:right;" onvaluechanged="onVerifyVDateRangeChanged" emptyText="请选择起息日区间...">

		<span style="float:right;margin-right: 155px">
			<a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
			<a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
		</span>
	</div>
	</fieldset>
	<div id="tradeManage" class="mini-fit" >
		<div id="datagrid1" class="mini-datagrid borderAll" allowAlternating="true" allowResize="true" onshowrowdetail="onShowRowDetail"
		sortMode="client" height="100%">
			<div property="columns">
				<div type="indexcolumn" width="" headerAlign="true"></div>
				<div type="expandcolumn" width="" headerAlign="true"></div>
				<div field="dealNo" width="180" headerAlign="center" align="center" allowSort="true">交易单号</div>
				<div field="product.prdName" width="250" headerAlign="center">产品名称</div>
				<div field="approveStatus" width="70" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}" 
					headerAlign="center">审批状态</div>
				<div field="counterParty.party_name" width="120" headerAlign="center">客户名称</div>
				<div field="aDate" width="80" headerAlign="center" align="center"  allowSort="true">审批日期</div>
				<div field="ccy" width="60" headerAlign="center" align="left" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
				<div field="amt" width="100"  numberFormat="#,0.00" align="right" allowSort="true">本金</div>
				<div field="rateType" width="80" headerAlign="center" renderer="onRateType" align="left">利率类型</div>
				<div field="contractRate" headerAlign="center" numberFormat="p4" align="right" width="80" allowSort="true">投资利率</div>
				<!-- <div field="acturalRate" headerAlign="center" numberFormat="p4" align="right" width="80" allowSort="true">实际利率</div> -->
				<div field="vDate" width="80" headerAlign="center" align="center" allowSort="true">起息日期</div>
				<div field="mDate" width="80" headerAlign="center" align="center" allowSort="true">到期日期</div>
				<div field="productName" width="150" headerAlign="center">资产名称</div>
				<div field="cost" width="80" allowSort="false" headerAlign="center" align="left">成本中心</div>
				<div field="port" width="80" allowSort="false" headerAlign="center" align="left">投资组合</div>
				<div field="productCode" width="80" headerAlign="center" allowSort="true">资产代码</div>
				<div field="saleType" width="120" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'saleType'}" align="left">业务类型</div>
				<div field="dealNoticeNo" width="100" headerAlign="center">额度批复号</div>
				<div field="user.userName" width="100" headerAlign="center">审批发起人</div>
				<div field="institution.instName" width="120" headerAlign="center">审批发起机构名称</div>
				<div field="lastUpdate" width="150" headerAlign="center" align="center">更新时间</div>
			</div>
		</div>
	</div>
	<div id="childgrid" style="display: none;">
		<div id="TotalVerifyDetail" class="mini-datagrid borderAll" allowAlternating="true" allowResize="true" onshowrowdetail="onShowEvent" sortMode="client" height="" showPager="false">
			<div property="columns">
				<div type="expandcolumn" width="10"></div>
				<div field="settlAmt" numberFormat="#,0.00" align="right" width="30" headerAlign="center">持仓本金(元)</div>
				<div field="conctAmt" numberFormat="#,0.00" align="right" width="30" headerAlign="center">合同金额</div>
				<div field="retrnAmt" numberFormat="#,0.00" align="right" width="30" headerAlign="center">归还本金累计</div>
				<div field="accruedTint" numberFormat="#,0.00" align="right" width="30" headerAlign="center">应计利息累计至当前日</div>
				<div field="actualTint" numberFormat="#,0.00" align="right" width="30" headerAlign="center">利息收入累计至当日</div>
				<div field="amorTint" numberFormat="#,0.00" align="right" width="30" headerAlign="center">摊销累计至当日</div>
				<div field="procAmt" numberFormat="#,0.00" align="right" width="30" headerAlign="center">实际清算金额</div>
				<div field="disAmt" numberFormat="#,0.00" align="right" width="30" headerAlign="center">折溢价金额</div>
			</div>
		</div>
	</div>
	<div id="grandchildgrid" style="display: none;">
		<div id="TotalVerifyEvent" allowAlternating="true" allowResize="true" class="mini-datagrid borderAll" sortMode="client" height="" showPager="false">
			<div property="columns">
				<div field="version" width="10" headerAlign="center">版本</div>
				<div field="eventType" width="10" headerAlign="center" align="center">事件类型</div>
				<div field="eventRefno" width="10" headerAlign="center" align="center">事件对应流水</div>
				<div field="eventTable" width="10" headerAlign="center">事件所在表名</div>
				<div field="opTime" width="10" headerAlign="center" align="center" allowSort="true">操作时间</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		mini.parse();
		/* 查询审批列表的方法 */
		var grid = mini.get("datagrid1");
			grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex;
				var pageSize = e.data.pageSize;
				search(pageSize, pageIndex);
			});
		function search(pageSize, pageIndex) {
			var form = new mini.Form("search_form");
			form.validate();
			if (form.isValid() == false){
				mini.alert("表单填写错误,请检查!","系统提示");
				return;
			}
				
			//提交数据
			var data = form.getData(true);//获取表单多个控件的数据  
			if(parseFloat(data.amtFrom)>parseFloat(data.amtTo)){
				mini.alert("本金区间的起始金额不能大于结束金额!","系统提示");
				return;
			}
			data['pageNumber'] = pageIndex + 1;
			data['pageSize'] = pageSize;
			data['branchId'] = branchId;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({   
				url: '/ProductApproveController/searchPageTotalVerify2',
				data: param,
				callback: function (data) {
					var grid = mini.get("datagrid1");
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}

		function clear() {
			var form = new mini.Form("#search_form");
			form.clear();
		}

		//查询的方法
		function query() {
			var grid = mini.get("datagrid1");
			search(grid.pageSize, 0);
		}
		//查询交易详情
		function TotalVerifyDetailSearch(dealNo, version) {
			var data = {};
			data['dealNo'] = dealNo;
			data['version'] = 0;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url: '/trdTposController/searchTdTrdTposByKey',
				data: params,
				callback: function (data) {
					if(data.obj[0] != null){
					var grid = mini.get("TotalVerifyDetail");
					grid.setData(data.obj);
					} 
				}
			});

		}

		//查询交易事件
		function TotalVerifyDetailEventSearch(dealNo) {
			var data = {};
			data['dealNo'] = dealNo;
			var params = mini.encode(data);
			CommonUtil.ajax({
				url: '/trdTposController/searchTrdEventByKey',
				data: params,
				callback: function (data) {
					var grid = mini.get("TotalVerifyEvent");
					grid.setData(data.obj);
				}
			});

		}
		function onCostQuery(e) {
			var btnEdit = this;
			mini.open({
				url: CommonUtil.baseWebPath() + "/mini_acc/MiniCostMiniManages.jsp",
				title: "成本中心选择",
				width: 750,
				height: 450,
				ondestroy: function (action) {
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.costcent);
							btnEdit.setText(data.costcent);
							btnEdit.focus();
						}
					}

				}
			});
		}

		function onPortQuery(e) {
			var btnEdit = this;
			mini.open({
				url: CommonUtil.baseWebPath() + "/mini_acc/MiniPortMiniManages.jsp",
				title: "投资组合",
				width: 850,
				height: 450,
				ondestroy: function (action) {
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.portfolio);
							btnEdit.setText(data.portfolio);
							btnEdit.focus();
						}
					}

				}
			});
		}
		function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url: CommonUtil.baseWebPath() + "/mini_system/MiniInstitutionSelectManages.jsp",
				title: "机构选择",
				width: 700,
				height: 600,
				ondestroy: function (action) {
					if (action == "ok") {
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instId);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}
				}
			});
		}

		function onShowRowDetail(e) {
			var grid = e.sender;
			var row = e.record;
			var td = grid.getRowDetailCellEl(row);
			var childGrid = mini.get("TotalVerifyDetail");
			var gridEl = childGrid.getEl();
			td.appendChild(gridEl);
			TotalVerifyDetailSearch(row.dealNo, row.version);
		}
		
		function onShowEvent(e) {
			var grid = e.sender;
			var row = e.record;
			var td = grid.getRowDetailCellEl(row);
			var childGrid = mini.get("TotalVerifyEvent");
			var gridEl = childGrid.getEl();
			td.appendChild(gridEl);
			TotalVerifyDetailEventSearch(row.dealNo);
		}

		function checkBoxValuechanged() {
			search(grid.pageSize, 0);
		}

		var rateTypes = [{
				id: '3',
				text: '净值'
			}, {
				id: '2',
				text: '浮息'
			}, {
				id: '1',
				text: '固息'
			}];
		function onRateType(e) {
			for (var i = 0, l = rateTypes.length; i < l; i++) {
				var g = rateTypes[i];
				if (g.id == e.value)
					return g.text;
			}
			return " ";
		}

		var _bizDate = '<%=__bizDate %>';
		function onVerifyVDateRangeChanged(e){
			var value = e.value;
			var str = _bizDate.replace(/-/g,"/");
			var dateStart = new Date(str); 
			var dateEnd = new Date(str); 

			if(value == '0'){//最近一周
				dateStart.setDate(dateStart.getDate() - 7);
				mini.get("verifyVDate").setValue(dateStart);
				mini.get("verifyVDate2").setValue(dateEnd);

			}else{//最近一月
				dateStart.setMonth(dateStart.getMonth() - 1);
				mini.get("verifyVDate").setValue(dateStart);
				mini.get("verifyVDate2").setValue(dateEnd);
			}
		}

		$(document).ready(function () {
			search(grid.pageSize, 0);
			//最近一周、最近一月查询
			var data = [
				{id:'0',text:'最近一周'},
				{id:'1',text:'最近一月'}
			];
			mini.get("verifyVDateRange").setData(data);
		});
	</script>
</body>
</html>