﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<div style="width:100%;">
	<div id="trade_info" style="background:white">
		<form class="mini-form" style="width:100%">
			<div id="Participant_form"></div>
			<div id="productApprove_form"></div>
			<div id="customizeInfo_form"></div>
		</form>
	</div>
	<div id="ApproveOperate_div"  style='background:white;' width="100%">
		<fieldset><legend>审批</legend>
			<table id="approve_operate_form" width="100%" height="100%" cols="2">
				<tr>
					<td width="15%" id="td_user">指定审批人</td>
					<td width="35%">
						<input id="user" name="user" class="mini-combobox" style='width:70%;'/>
						<span id="td_user_text"></span>
					</td>
					<td width="15%">常用语</td>
					<td width="35%">
						<input id="add_text_combobox" class="mini-combobox" style='width:70%;' 
							data="[{'id':'1','text':'同意'},{'id':'2','text':'已阅'},{'id':'3','text':'不同意'},{'id':'4','text':'退回修改'},{'id':'5','text':'请领导审批'}]"/>
						<a  id="add_text_btn" class="mini-button" style="display: none"   >添加</a>
					</td>
				</tr>
				<tr>
					<td style="width:100%;" colspan="4">
						<div title="审批意见">
							<input id="approve_msg" class='mini-textarea' style="width:95%;height:70px;" required="true"  vtype="maxLength:500"/>
						</div>
					</td>
				</tr>

				<tr>
					<td style="width:100%;" colspan="4">
						<a id="pass_btn"     class="mini-button" style="display: none"   visible="false">直接通过</a>
						<a id="continue_btn" class="mini-button" style="display: none"   visible="false">通过</a>
						<a id="skip_btn"   class="mini-button" style="display: none"   visible="false">加签</a>
						<a id="redo_btn"   class="mini-button" style="display: none"   visible="false">退回发起</a>
						<a id="back_btn"   class="mini-button" style="display: none"   visible="false">驳回</a>
						<a id="nopass_btn" class="mini-button" style="display: none"   visible="false">撤销</a>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
</div>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_trade/MiniTradeEdit.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/standard/InterBank/mini_base/mini_flow/MiniApproveOpCommon.js"></script>