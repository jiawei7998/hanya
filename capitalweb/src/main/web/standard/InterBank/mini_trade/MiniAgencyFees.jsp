<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>中介费</legend>
	<input id="dealNo" name="dealNo" class="mini-hidden" >
	<input id="refNo"  name="refNo"  class="mini-hidden" >
	<div id="data_form">
		<table width="100%">
			<tr>
				<td class="red-star" width="15%">中介机构</td>
				<td width="35%">
					<input id='organization' name='organization' class="mini-buttonedit" style="width:80%;" onbuttonclick="onInsQuery" required="true"  emptyText="请输入机构" maxLength="60"/>
				</td>
				<td id="feeType_label" class="red-star" width="15%">费率类型</td>
				<td  width="35%">
					<input id="feeType" name="feeType" class="mini-combobox" style="width:80%;" data="CommonUtil.serverData.dictionary.rateCashTyp" required="true"  value="2" onvaluechanged="onfeeTypeChange"/>
				</td>
			</tr>			
			<tr>
				<td id="feeRate_label" width="15%">费率</td>
				<td width="35%"><input id="feeRate" name="feeRate" class="mini-spinner" changeOnMousewheel="false" style="width:80%;" required="true"  onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])" format="p4" allowlimitvalue="false"  allowNull="true"  value="null"/></td>
				
				<td id="feeBasis_label" width="15%">计息基准</td>
				<td width="35%"><input id="feeBasis" name="feeBasis" class="mini-combobox" style="width:80%;" required="true"  data="CommonUtil.serverData.dictionary.DayCounter" value="Actual/360"/></td>
			</tr>
			
			<tr>
				<td id="feesAmt_label" width="15%">金额</td>
				<td width="35%"><input id="feesAmt" name="feesAmt" class="mini-spinner" changeOnMousewheel="false" style="width:80%;" maxLength="15" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" format="#,0.00" allowlimitvalue="false"  allowNull="true"  value="null"/></td>	
			</tr>
			<tr>
				<td colspan="3">
					<a id="add_btn"    class="mini-button" style="display: none"   onclick="add">新增</a>
					<a id="update_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
					<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
				</td>	
			</tr>
		</table>
	</div>
	</fieldset>
	<div class="mini-fit">
		<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:100%;" 
			idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client" showPager="false"
			pageSize="20"  multiSelect="false" allowAlternating="true" frozenStartColumn="0" frozenEndColumn="3">
			<div property="columns">
				<div type="indexcolumn"   headerAlign="center"  allowSort="false" width="40" >序号</div>  
				<div field='dealNo'       headerAlign= "center" allowSort= "true" width='120' visible="false">费用流水号</div> 
				<div field='refNo'        headerAlign= "center" allowSort= "true" width='120' visible="false">原交易流水号</div> 
				<div field='feeType'        headerAlign= "center" allowSort= "true" width='120' data-options="{dict:'rateCashTyp'}" renderer="CommonUtil.dictRenderer">费用类型</div>                    
				<div field='feeBasis'       headerAlign= "center" allowSort= "true" width='80'  align = 'center' data-options="{dict:'DayCounter'}" renderer="CommonUtil.dictRenderer">计息基础</div>  
				<div field='feeRate'        headerAlign= "center" allowSort= "true" width='80'  align = 'right' numberFormat="p4">费率</div>                            
				<div field='feesAmt'        headerAlign= "center" allowSort= "true" width='120' align = 'right' numberFormat="#0.00">金额</div>                              
				<div field='organization'   headerAlign= "center" allowSort= "true" width='120'>中介机构</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//获取参数传递	
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url,"prdNo");
		//操作类型
		var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
		var dealNo = CommonUtil.getParam(url,"dealNo");
		
		mini.parse();

		function tradeManagerWin()
		{
			if(operType == 'A'){
				return top["tradeEditTabs_" + prdNo + "_" + operType];
			} else {
				return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
			}
		}

		//获取miniui组件
		var grid = mini.get("grid1");
		//父页面的参数值
		var tradeData = tradeManagerWin().param;
		
		if($.inArray(operType,['A','U']) == -1){//不是新增和修改 禁用按钮
			mini.get("add_btn").setEnabled(false);
			mini.get("update_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("clear_btn").setEnabled(false);
		}

		//设置隐藏域
		mini.get("refNo").setValue(tradeData.dealNo);
		
		//初始化数据
		function init(){
			mini.get("feeType").doValueChanged();
			CommonUtil.ajax({
				url:'/TdFeesBrokerController/getFeesBrokerListByDealNo',
				data:{dealNo:tradeData.dealNo},
				callback:function(data){
					grid.setData(data.obj);
				}
			});
		}
		
		init();

        function save() {
			if(grid.data.length <= 0){
				mini.alert("请先新增交易至列表中!","提示");
				return;
			}
			var refNo = mini.get("refNo").getValue();
			var param = {dealNo : refNo};
			param["tdFeesBrokers"] = grid.getData(true);
			
            var json = mini.encode(param);
			CommonUtil.ajax({
				url:"/ProductApproveController/updateProductApproveSub",
				data:json,
				callback:function(data){
					mini.alert("保存成功.","提示");
				}
			});
		}

		function onfeeTypeChange(){
			var feeType = mini.get("feeType").getValue();
			if(feeType == '2'){
				setFieldStatus("feeRate",true,true);
				setFieldStatus("feeBasis",true,true);
				setFieldStatus("feesAmt",false,false);

			}else{
				setFieldStatus("feeRate",false,false);
				setFieldStatus("feeBasis",false,false);
				setFieldStatus("feesAmt",true,true);
			}
		}

		//设置必输字段
		function setFieldStatus(id,required,enabled){
			if(CommonUtil.isNotNull(mini.get(id)))
			{
				mini.get(id).setRequired(required);

				mini.get(id).setEnabled(enabled);

				if(!enabled){
					mini.get(id).setValue(null);
					mini.get(id).setIsValid(true);
				}

				if(required){
					$("#" + id + "_label").addClass("red-star");
				}else{
					$("#" + id + "_label").removeClass("red-star");
				}
			}
		}

		function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instName);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}

		function add(){
			var form = new mini.Form("data_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
				return;
			};
			var row = form.getData(true);
			var feeRate = row.feeRate;
			var feeType = row.feeType;
			if(feeType == '2' && CommonUtil.isNotNull(feeRate) && feeRate <= 0){
				mini.alert("费率必须大于0!","消息提示");
				return;
			}
			var feesAmt = row.feesAmt;
			if(feeType == '1' && CommonUtil.isNotNull(feesAmt) && feesAmt <= 0){
				mini.alert("金额必须大于0!","消息提示");
				return;
			}

			grid.addRow(row, grid.data.length);
			mini.alert("新增成功!","消息提示");
		}

		function del(){
			var rows = grid.getSelecteds();
            if (rows.length > 0) {
				grid.removeRows(rows, true);
				mini.alert("删除成功!","消息提示");
            }else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		function clear(){
			var form = new mini.Form("#data_form");
			form.clear();
		}

		function onRowDblClick()
		{
			var form = new mini.Form("data_form");
			var row =  grid.getSelected();
			form.setData(row,true,false);
			form.setIsValid(true);

			var controls = mini.findControls(function(control){
				if(control.type == "buttonedit") return true;    //获取所有mini-textbox控件
			});
			for (var i = 0; i< controls.length; i++) 
			{
				if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
					controls[i].setText(controls[i].getValue());
				} 
			}

			mini.get("feeType").doValueChanged();
		}

		function update(){
			var row =  grid.getSelected();
			if(row){
				var form = new mini.Form("data_form");
				form.validate();
				if(form.isValid() == false){
					mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
					return
				};
				var data = form.getData(true);

				var feeRate = data.feeRate;
				var feeType = data.feeType;
				if(feeType == '2' && CommonUtil.isNotNull(feeRate) && feeRate <= 0){
					mini.alert("费率必须大于0!","消息提示");
					return;
				}
				var feesAmt = data.feesAmt;
				if(feeType == '1' && CommonUtil.isNotNull(feesAmt) && feesAmt <= 0){
					mini.alert("金额必须大于0!","消息提示");
					return;
				}
				
				grid.updateRow(row,data);

				mini.alert("修改成功!","消息提示");
			}else{
				mini.alert("请选择要修改的列!","消息提示");
			}
		}
    </script>
	</body>
</html>
