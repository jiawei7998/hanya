<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>表单录入</legend>
	<input class="mini-hidden" id="refNo" name="refNo" />
	<div id="data_form" >
		<table width="100%">
			<tr>			
				<td class="red-star">币种</td>
				<td><input id="ccy" name="ccy" class="mini-combobox" style="width:80%;" enabled="false" required="true"  vtype="maxLength:3" data="CommonUtil.serverData.dictionary.Currency" /></td>
				<td class="red-star">本金</td>
				<td><input id="amt" name="amt" class="mini-spinner" style="width:80%;" enabled="false" changeOnMousewheel="false" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" format="#,0.00" allowlimitvalue="false" required="true" /></td>
				<td>余额</td>
				<td><input id="banlance" name="banlance" class="mini-spinner" style="width:80%;" enabled="false" changeOnMousewheel="false" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" format="#,0.00" allowlimitvalue="false" equired="true"/></td>
			</tr>
			<tr>
				<td class="red-star">利率类型</td>
				<td><input id="rateType" name="rateType" class="mini-combobox" style="width:80%;" enabled="false" required="true"  data="CommonUtil.serverData.dictionary.rateType"/></td>
				<td>基准利率代码</td>
				<td><input id="rateCode" name="rateCode" class="mini-buttonedit" style="width:80%;" enabled="false" onbuttonclick="onRateTypesQuery" vtype="maxLength:32"/></td>
				<td>利差</td>
				<td><input id="rateDiff" name="rateDiff" class="mini-textbox" style="width:80%;" enabled="false" vtype="range:0,100000"/></td>
			</tr>
			<tr>
				<td class="red-star">合同利率</td>
				<td><input id="acturalRate" name="acturalRate" format="p4" class="mini-spinner" style="width:80%;" enabled="false" changeOnMousewheel="false" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])"  allowlimitvalue="false" required="true" /></td>
				<td class="red-star">计息基础</td>
				<td><input id="basis" name="basis" class="mini-combobox" enabled="false" style="width:80%;" required="true"  data="CommonUtil.serverData.dictionary.DayCounter"/></td>
				<td class="red-star">收息方式</td>
				<td><input id="intType" name="intType" class="mini-combobox" enabled="false" style="width:80%;" required="true"  data="CommonUtil.serverData.dictionary.intType" value="2"/></td>
			</tr>
			<tr>
				<td class="red-star">起息日</td>
				<td><input id="vDate" name="vDate" class="mini-datepicker" enabled="false" style="width:80%;" required="true" /></td>
				<td class="red-star">到期日</td>
				<td><input id="mDate" name="mDate" class="mini-datepicker" enabled="false" style="width:80%;" required="true" /></td>
				<td class="red-star">计息天数（天）</td>
				<td><input id="term" name="term" class="mini-spinner" enabled="false" style="width:80%;" changeOnMousewheel="false"  onvalidation="CommonUtil.onValidation(e,'numberRange',[0,1000000])" format="#,0.00" allowlimitvalue="false" required="true" /></td>
			</tr>
			
			<tr>
				<td>第一个付息日</td>
				<td><input id="sDate" name="sDate" class="mini-datepicker" style="width:80%;" enabled="false"/></td>
				<td>最后一个付息日</td>
				<td><input id="eDate" name="eDate" class="mini-datepicker" style="width:80%;" enabled="false"/></td>
				<td class="red-star">收息频率</td>
				<td><input id="intFre" name="intFre" class="mini-combobox" style="width:80%;" enabled="false" required="true"  data="CommonUtil.serverData.dictionary.CouponFrequently"/></td>
			</tr>
			
			<tr>
				<td class="red-star">到期利息（元）</td>
				<td><input id="mInt" name="mInt" class="mini-spinner" style="width:80%;" enabled="false" changeOnMousewheel="false"  onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" format="#,0.00" allowlimitvalue="false" required="true" /></td>
				<td class="red-star">到期金额（元）</td>
				<td><input id="mAmt" name="mAmt" class="mini-spinner" style="width:80%;" enabled="false" changeOnMousewheel="false"  onvalidation="CommonUtil.onValidation(e,'numberRange',[0,999999999999])" format="#,0.00" allowlimitvalue="false" required="true" /></td>
				<td>客户号</td>
				<td><input id="cifNo" name="cifNo" class="mini-buttonedit" style="width:80%;" onbuttonclick="onCustQuery" allowInput="false"  maxLength="30" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/></td>
			</tr>
			<tr>
				<td>客户名称</td>
				<td><input id="assetCname" name="assetCname" class="mini-textbox"  style="width:80%;" vtype="maxLength:40"/></td>
				<td>客户所属地区</td>
				<td><input id="assetCustLocate" name="assetCustLocate" class="mini-textbox" style="width:80%;" vtype="maxLength:25"/></td>
				<td>社会信用代码证号</td>
				<td><input id="assetSno" name="assetSno" class="mini-textbox" maxLength="40" style="width:80%;" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/></td>
			</tr>
			<tr>
				<td>行业大类</td>
				<td ><input id="indusCatBig" name="indusCatBig" onvaluechanged="onBigCatChanged" style="width:80%;" class="mini-combobox"/></td>
				<td>行业中类</td>
				<td><input id="indusCatMid" name="indusCatMid" onvaluechanged="onMiddleCatChanged" style="width:80%;" class="mini-combobox"/></td>
				<td>行业小类</td>
				<td><input id="indusCatSmall" name="indusCatSmall" class="mini-combobox" style="width:80%;" /></td>  
			</tr>

			<tr>
				<td>是否本行客户</td>
				<td><input id="assetInnerCust" name="assetInnerCust" class="mini-combobox" style="width:80%;" data="CommonUtil.serverData.dictionary.YesNo" value="1"/></td>
				<td>是否为平台</td>
				<td><input id="isPlat" name="isPlat" class="mini-combobox" style="width:80%;" data="CommonUtil.serverData.dictionary.YesNo"/></td>
				<td>企业控股类型</td>
				<td><input id="holdingType" name="holdingType" class="mini-combobox"  vtype="maxLength:20" style="width:80%;" data="CommonUtil.serverData.dictionary.holdingType"/></td>
			</tr>
			
			<tr>
				<td>企业规模</td>
				<td><input id="companySize" name="companySize" class="mini-combobox" vtype="maxLength:20" style="width:80%;" data="CommonUtil.serverData.dictionary.companySize"/></td>
				<td>行内评级</td>
				<td><input id="innerLevel" name="innerLevel" class="mini-textbox"  vtype="maxLength:10" style="width:80%;" /></td>
				<td>公开评级</td>
				<td><input id="outerLevel" name="outerLevel" class="mini-textbox" vtype="maxLength:10" style="width:80%;" /></td>
			</tr>

			<tr>
				<td>注册资本</td>
				<td><input id="regCapital" name="regCapital"  class="mini-spinner" style="width:80%;" changeOnMousewheel="false" minValue="0" maxValue="9999999999999" format="#,0.00" maxlength="15"/></td>
				<td>基础资产类型</td>
				<td><input id="assetType" name="assetType" class="mini-combobox" style="width:80%;" data="CommonUtil.serverData.dictionary.baseAssetType"/></td>
				<td>基础资产标的物</td>
				<td><input id="assetTarget" name="assetTarget"  class="mini-textbox" style="width:80%;" vtype="maxLength:50"/></td>
			</tr>

			<tr  id="secStockId">
				<td>债券编号</td>                                                                         
				<td><input id="secId" name="secId" class="mini-textbox" maxLength="25" style="width:80%;" onvalidation="CommonUtil.onValidation(e,'stockId','')"/></td>
				<td>股票代码</td>                                                                                           
				<td><input id="stockId" name="stockId" class="mini-textbox" maxLength="25"  style="width:80%;" onvalidation="CommonUtil.onValidation(e,'stockId','')"/></td> 
				<td>预警线</td>
				<td><input id="warnLinePrice" name="warnLinePrice" class="mini-spinner" style="width:80%;" changeOnMousewheel="false" minValue="0" maxValue="100000" format="#,0.00"/></td>	
			</tr>
			<tr id="coverOpenLine">
				<td>补仓线</td>                                                                                               
				<td><input id="coverLinePrice" name="coverLinePrice" class="mini-spinner" style="width:80%;" changeOnMousewheel="false" minValue="0" maxValue="100000" format="#,0.00"/></td>     
				<td>平仓线</td>
				<td><input id="openLinePrice" name="openLinePrice" class="mini-spinner" style="width:80%;" changeOnMousewheel="false" minValue="0" maxValue="100000" format="#,0.00"/></td>
				<td>融资用途</td>
				<td><input id="financingUse" name="financingUse" class="mini-textbox" style="width:80%;" vtype="maxLength:25"/></td>
			</tr>
			<tr>
				<td>备注信息</td>
				<td colspan="6"><input name="remark" id="remark" class="mini-textarea" height="45" width="700" style="width:80%;" vtype="maxLength:100"/></td>
			</tr>
			<tr>
				<td colspan="6">
				<div class="mini-toolbar">
					<a id="fresh_btn"    class="mini-button" style="display: none"    onclick="queryTradeInfo">同步合同信息</a>
					<a id="add_btn"    class="mini-button" style="display: none"   onclick="add">新增</a>
					<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
					<a id="update_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">重置</a>
					<a id="delGuar_btn" class="mini-button" style="display: none"   onclick="delGuarData" style="float:right;margin-left:5px;">删除质押物</a>
					<a id="addGuar_btn"    class="mini-button" style="display: none"   onclick="queryGuarData" style="float:right;margin-left:5px;">查询质押物</a>
					<input id="lmtNo" name="lmtNo" class="mini-combobox" onclick="lmtNoClick" style="float:right;margin-left:5px;" emptyText="请选择额度编号"/>
				</div>
				</td>
			</tr>
		</table> 
	</div>
	</fieldset>
	<fieldset style="min-width:0;"><legend>基础资产列表</legend>
	<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:200px;" 
		idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client" showPager="false"
		pageSize="20"  multiSelect="false" allowAlternating="true" frozenStartColumn="0" frozenEndColumn="2" sortMode="client">
		<div property="columns">
			<div type="indexcolumn"       headerAlign="center" allowSort="true" width="40">序号</div>                                                                                                                                                                                 
			<div field="dealNo"           headerAlign="center" allowSort="true" width="120" visible="false">交易流水号</div>                                      
			<div field="ccy"              headerAlign="center" allowSort="true" width="60"       align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>        
			<div field="amt"              headerAlign="center" allowSort="true" width="120"      align='right'  allowSort="true" numberFormat="#,0.00">本金（元）</div>                                        
			<div field="banlance"         headerAlign="center" allowSort="true" width="120"      align='right'  allowSort="true" numberFormat="#,0.00">余额（元）</div>                                        
			<div field="rateType"         headerAlign="center" allowSort="true" width="120"       align='right' renderer="CommonUtil.dictRenderer" data-options="{dict:'rateType'}">利率类型</div>    
			<div field="rateCode"         headerAlign="center" allowSort="true" width="120">基准利率代码</div>                                                                         
			<div field="rateDiff"         headerAlign="center" allowSort="true" width="120"  align='right'>利率点差（BP）</div>                                                                       
			<div field="acturalRate"      headerAlign="center" allowSort="true" width="120"       align='right' allowSort="true" numberFormat="p4">合同利率</div>                                                        
			<div field="vDate"            headerAlign="center" allowSort="true" width="80" align="center">起息日期</div>
			<div field="mDate"            headerAlign="center" allowSort="true" width="80" align="center">到期日期</div>
			<div field="sDate"            headerAlign="center" allowSort="true" width="80" align="center">第一个付息日</div>
			<div field="eDate"            headerAlign="center" allowSort="true" width="80" align="center">最后一个付息日</div>
			<div field="term"             headerAlign="center" allowSort="true" width="120">计息天数（天）</div>
			<div field="basis"            headerAlign="center" allowSort="true" width="60" align="center"     renderer="CommonUtil.dictRenderer" data-options="{dict:'DayCounter'}">计息基础</div>       
			<div field="mInt"             headerAlign="center" allowSort="true" width="120"      align='right' allowSort="true" numberFormat="#,0.00">到期利息（元）</div>                                    
			<div field="mAmt"             headerAlign="center" allowSort="true" width="120"      align='right'  allowSort="true" numberFormat="#,0.00">到期金额（元）</div>                                           
			<div field="intFre"           headerAlign="center" allowSort="true" width="120"      renderer="CommonUtil.dictRenderer" data-options="{dict:'CouponFrequently'}">收息频率</div>                                                           	
			<div field="intType"          headerAlign="center" allowSort="true" width="60"  align="center"    renderer="CommonUtil.dictRenderer" data-options="{dict:'intType'}">收息方式</div> 
			<div field="financingUse"     headerAlign="center" allowSort="true" width="120">融资用途</div> 
			<div field="cifNo"            headerAlign="center" allowSort="true" width="100" align="center">客户号</div>
			<div field="assetCname"       headerAlign="center" allowSort="true" width="180">客户名称</div>
			<div field="assetCustLocate"  headerAlign="center" allowSort="true" width="120">客户所属地区</div>
			<div field="indusCatBig"      headerAlign="center" allowSort="true" width="120" visible="false">行业大类</div>
			<div field="indusCatMid"      headerAlign="center" allowSort="true" width="120" visible="false">行业中类</div>      
			<div field="indusCatSmall"    headerAlign="center" allowSort="true" width="120" visible="false">行业小类</div>
			<div field="bigKindName"      headerAlign="center" allowSort="true" width="120">行业大类名称</div>
			<div field="midKindName"      headerAlign="center" allowSort="true" width="120">行业中类名称</div>      
			<div field="smallKindName"    headerAlign="center" allowSort="true" width="120">行业小类名称</div>
			<div field="assetSno"         headerAlign="center" allowSort="true" width="120">社会信用代码证号</div>
			<div field="holdingType"      headerAlign="center" allowSort="true" width="120" renderer="CommonUtil.dictRenderer" data-options="{dict:'holdingType'}">企业控股类型</div>                                                                         
			<div field="companySize"      headerAlign="center" allowSort="true" width="120" renderer="CommonUtil.dictRenderer" data-options="{dict:'companySize'}">企业规模</div> 
			<div field="innerLevel"       headerAlign="center" allowSort="true" width="120">行内评级</div>
			<div field="outerLevel"       headerAlign="center" allowSort="true" width="120">公开评级</div>  
			<div field="regCapital"       headerAlign="center" allowSort="true" width="120">注册资本</div>
			<div field="assetInnerCust"   headerAlign="center" allowSort="true" width="60"  align="center"    renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否本行客户</div>                                                                            
			<div field="assetType"        headerAlign="center" allowSort="true" width="120" renderer="CommonUtil.dictRenderer" data-options="{dict:'baseAssetType'}">基础资产类型</div>                                                          	
			<div field="assetTarget"      headerAlign="center" allowSort="true" width="120">基础资产标的物</div>                                                                
			<div field="secId"            headerAlign="center" allowSort="true" width="120">债券编号</div>                                                                             
			<div field="stockId"          headerAlign="center" allowSort="true" width="80">股票代码</div>                                                                              
			<div field='warnLinePrice'    headerAlign="center" allowSort="true" width='120' align='right' numberFormat="#,0.00">预警线</div>
			<div field="coverLinePrice"   headerAlign="center" allowSort="true" width="120" align='right' allowSort="true"  numberFormat="#,0.00">补仓线</div>                                             
			<div field="openLinePrice"    headerAlign="center" allowSort="true" width="120" align='right' allowSort="true"  numberFormat="#,0.00">平仓线</div>
			<div field="lmtNo"            headerAlign="center" allowSort="true" width="120" align="center">额度编号</div>
			<div field="isPlat"           headerAlign="center" allowSort="true" width="80"  align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否为平台</div>
			<div field="remark"           headerAlign="center" allowSort="true" width="200">备注</div>
		</div>
	</div>
	</fieldset>

	<fieldset><legend>质押物列表</legend>
	<div id="grid2" class="mini-datagrid  borderAll" style="width:100%;height:200px;" 
		idField="dealNo" allowResize="true" border="true" sortMode="client" showPager="false"
		pageSize="20"  multiSelect="false" allowAlternating="true" sortMode="client">
		<div property="columns">
			<div type="indexcolumn"      headerAlign="center" allowSort="true" width="40">序号</div>
			<div field="guartyMode"      headerAlign="center" allowSort="true" width="80">担保方式</div>
			<div field="pledgeAmt"       headerAlign="center" allowSort="true" width="100" align='right'>抵质押物额度</div>
			<div field="guarType"        headerAlign="center" allowSort="true" width="150" align='right'>抵质押物名称</div>
			<div field="pledgeRate"      headerAlign="center" allowSort="true" width="80" align='right'>抵质押率(%)</div>
			<div field="colRightNo"      headerAlign="center" allowSort="true" width="120">抵质押物权证号</div>
			<div field="guarantor"       headerAlign="center" allowSort="true" width="120">保证人名称</div>
			<div field="guarantorIndus"  headerAlign="center" allowSort="true" width="120">保证人行业</div>
		</div>
	</div>
	</fieldset>

	<script type="text/javascript">
		//获取参数传递	
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url,"prdNo");
		//操作类型
		var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
		var dealNo = CommonUtil.getParam(url,"dealNo");
		
		mini.parse();

		function tradeManagerWin()
		{
			if(operType == 'A'){
				return top["tradeEditTabs_" + prdNo + "_" + operType];
			} else {
				return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
			}
		}
		
		//父页面的参数值
		var tradeData = tradeManagerWin().param;
		//获取miniui组件
		var grid = mini.get("grid1");
		//设置隐藏值
		mini.get("refNo").setValue(tradeData.dealNo);

		if($.inArray(operType,['A','U']) == -1){//不是新增和修改 禁用按钮
			mini.get("fresh_btn").setEnabled(false);
			mini.get("add_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("update_btn").setEnabled(false);
			mini.get("clear_btn").setEnabled(false);
			mini.get("delGuar_btn").setEnabled(false);
			mini.get("addGuar_btn").setEnabled(false);	
		}

		//初始化数据
		CommonUtil.ajax({
			url:'/ProductApproveController/searchBaseAssetStructListVo',
			data:{dealNo:tradeData.dealNo},
			callback:function(data){
				grid.setData(data.obj);
			}
		});

		var grid2 = mini.get("grid2");
		CommonUtil.ajax({
			url:'/ProductApproveController/searchBaseAssetGuartyList',
			data:{dealNo:tradeData.dealNo},
			callback:function(data){
				grid2.setData(data.obj);
			}
		});

		//异步加载行业类型数据字典
		CommonUtil.loadPartyKindDict("indusCatBig","-1");

		//产品大类控制产品中类
		function onBigCatChanged(e){
			CommonUtil.loadPartyKindDict("indusCatMid",mini.get("indusCatBig").getValue());
			mini.get("indusCatSmall").setData([]);
		}

		//产品中类控制产品小类
		function onMiddleCatChanged(e){
			CommonUtil.loadPartyKindDict("indusCatSmall",mini.get("indusCatMid").getValue());
		}
		
		function save() {
			if(grid.data.length <= 0){
				mini.alert("请先新增交易至列表中!","提示");
				return;
			}
			if(chechCredit(grid.data) == false){
				mini.alert("基础资产客户号与授信客户号不一致!","消息提示")
				return;
			}

			var refNo = mini.get("refNo").getValue();
			var param = {dealNo : refNo};
			param["tdBaseAssetStructs"] = grid.getData(true);
			param["tdBaseAssetGuartys"] = grid2.getData(true);
            var json = mini.encode(param);
			CommonUtil.ajax({
				url:"/ProductApproveController/updateProductApproveSub",
				data:json,
				callback:function(data){
					mini.alert("保存成功.","提示");
				}
			});
		}

		function chechCredit(rows){
			//额度客户号检查,额度客户号必须是 交易对手号 或者 基础资产中录入的客户号、或者通道客户号
			var data = tradeManagerWin().getTabData("tadeInfo");
			if(CommonUtil.isNotNull(data.creditCust) && data.amtManage == 'C')//占用企业额度
			{
				var row = null;
				var flag = false;
				for(var i = 0; i < rows.length; i++)
				{
					row = rows[i];
					if(CommonUtil.isNotNull(row.cifNo) && row.cifNo == data.creditCust){
						flag = true;
						break;
					}
				}
				return flag;
			}
			return true;
		}

		function onInsQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_system/MiniInstitutionSelectManages.jsp",
				title : "机构选择",
				width : 900,
				height : 600,
				ondestroy : function(action) {
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.instName);
							btnEdit.setText(data.instName);
							btnEdit.focus();
						}
					}

				}
			});
		}

		//设置值
		function setFieldValue(id,value){
			if(CommonUtil.isNotNull(mini.get(id)))
			{
				mini.get(id).setValue(value);

				if(CommonUtil.isNotNull(mini.get(id).setText)){
					CommonUtil.isNotNull(mini.get(id).setText(value));
				}
			}
		}

		//设置表单可用
		function setFieldEnabled(id,enabled){
			if(CommonUtil.isNotNull(mini.get(id)))
			{
				mini.get(id).setEnabled(enabled);
			}
		}
		
		//设置必输字段
		function setFieldRequired(id,required){
			if(CommonUtil.isNotNull(mini.get(id)))
			{
				mini.get(id).setRequired(required);
				if(required){
					$("#" + id + "_label").addClass("red-star");
				}else{
					$("#" + id + "_label").removeClass("red-star");
				}
			}
		}

		mini.get("rateType").on("valueChanged",function(e){
			var newValue = e.sender.getValue();
			if (newValue == "1") {//固息
				setFieldValue("rateCode","");//利率代码
				setFieldEnabled("rateCode",false);
				setFieldRequired("rateCode",false);
				
				setFieldValue("rateDiff","");//点差
				setFieldEnabled("rateDiff",false);
				setFieldRequired("rateDiff",false);
				
				if(mini.get("contractRate")){//合同利率
					setFieldEnabled("contractRate",true);
					setFieldRequired("contractRate",true);
				}
				
			} else if (newValue == "2") {//浮息
			
				if(mini.get("rateCode")){//利率代码
					setFieldEnabled("rateCode",true);
					setFieldRequired("rateCode",true);
				}
				if(mini.get("rateDiff")){//利率代码
					setFieldEnabled("rateDiff",true);
					setFieldRequired("rateDiff",true);
				}
				if(mini.get("contractRate")){//利率代码
					setFieldEnabled("contractRate",false);
					setFieldRequired("contractRate",false);
				}

			} else if(newValue == "3"){//净值型
				setFieldValue("rateCode","");//利率代码
				setFieldEnabled("rateCode",false);
				setFieldRequired("rateCode",false);
				
				setFieldValue("rateDiff","");//点差
				setFieldEnabled("rateDiff",false);
				setFieldRequired("rateDiff",false);
				
				setFieldValue("contractRate","");//点差
				setFieldEnabled("contractRate",false);
				setFieldRequired("contractRate",false);
			}
		});

		//同步合同页面数据
		function queryTradeInfo(){
			var data = tradeManagerWin().getTabData("tadeInfo");
			var form = new mini.Form("data_form");
			form.setData(data,false,false);
			mini.get("banlance").setValue(data.amt);
			mini.get("acturalRate").setValue(data.contractRate);
			mini.get("rateCode").setText(data.rateCode);
			
			//客户号
			mini.get("cifNo").setValue(data.cNo);
			mini.get("cifNo").setText(data.cNo);
			//客户名称
			mini.get("assetCname").setValue(data.partyName);
			//设置行业分类字典和值
			setPartyKindData(data.big_kind,data.mid_kind,data.small_kind);
		}

		//设置行业分类字典和值
		function setPartyKindData(big_kind,mid_kind,small_kind)
		{
			//行业大类
			mini.get("indusCatBig").setValue(big_kind);

			//行业中类字典
			CommonUtil.loadPartyKindDict("indusCatMid",big_kind);
			mini.get("indusCatMid").setValue(mid_kind);

			//行业小类字典
			CommonUtil.loadPartyKindDict("indusCatSmall",mid_kind);
			mini.get("indusCatSmall").setValue(small_kind);
		}

		function onRateTypesQuery(e) {
			var btnEdit = this;
			mini.open({
				url : CommonUtil.baseWebPath() +"/mini_market/MiniIrSelectManages.jsp",
				title : "基准利率",
				width : 850,
				height : 450,
				ondestroy : function(action) {			
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data) {
							btnEdit.setValue(data.i_code);
							btnEdit.setText(data.i_code);
							btnEdit.focus();
						}
					}

				}
			});
		}

		function add(){
			var form = new mini.Form("data_form");
			form.validate();
			if(form.isValid() == false){
				mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
				return;
			};
			if(grid.getData().length > 0){
				mini.alert("只能新增一条基础资产!","消息提示")
				return;
			}
			var row = form.getData(true);
			row.bigKindName = mini.get("indusCatBig").getText();
			row.midKindName = mini.get("indusCatMid").getText();
			row.smallKindName = mini.get("indusCatSmall").getText();
			grid.clearRows();
			grid.addRow(row, grid.data.length);
			mini.alert("新增成功!","消息提示");
		}

		function del(){
			var rows = grid.getSelecteds();
			if (rows.length > 0) {
				grid.removeRows(rows, true);
				mini.alert("删除成功!","消息提示");
			}else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		function onRowDblClick()
		{
			var form = new mini.Form("data_form");
			var row =  grid.getSelected();
			form.setData(row,true,false);
			form.setIsValid(true);

			var controls = mini.findControls(function(control){
				if(control.type == "buttonedit") return true;    //获取所有mini-textbox控件
			});
			for (var i = 0; i< controls.length; i++) 
			{
				if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
					controls[i].setText(controls[i].getValue());
				} 
			}
			/***
			 * indusCatBig 行业大类
			 * indusCatMid 行业中类
			 * indusCatSmall 行业小类
			 */
			setPartyKindData(row.indusCatBig,row.indusCatMid,row.indusCatSmall);
		}

		function update(){
			var row =  grid.getSelected();
			if(row){
				var form = new mini.Form("data_form");
				form.validate();
				if(form.isValid() == false){
					mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
					return
				};
				var data = form.getData(true);
				data.bigKindName = mini.get("indusCatBig").getText();
				data.midKindName = mini.get("indusCatMid").getText();
				data.smallKindName = mini.get("indusCatSmall").getText();
				grid.updateRow(row,data);
				
				mini.alert("操作成功!","消息提示");
			}else{
				mini.alert("请选择要修改的列!","消息提示");
			}
		}

		function clear(){
			var form = new mini.Form("#data_form");
			form.clear();
			mini.get("intType").setValue("2");
			mini.get("assetInnerCust").setValue("1");
		}

		function delGuarData(){
			var rows = grid2.getSelecteds();
			if (rows.length > 0) {
				grid2.removeRows(rows, true);
				mini.alert("删除成功!","消息提示");
			}else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		var oldCifNo = null;  
		function lmtNoClick(){
			var cifNo = mini.get("cifNo").getValue();
			if(CommonUtil.isNull(cifNo)){
				mini.get("lmtNo").setData(null);
				mini.alert("请输入基础资产客户号!","消息提示");
				return;
			}
			if(oldCifNo != cifNo){
				oldCifNo = cifNo;
				getLimitNo(cifNo);
			}else{
				var data = mini.get("lmtNo").getData();
				if(CommonUtil.isNull(data)){
					getLimitNo(cifNo);
				}
			}
		}

		//从信管系统获取额度流水信息
		function getLimitNo(cifNo){
			CommonUtil.ajax({
				url:"/fbz005/queryLimitNo",
				data:{'cifNo':cifNo,'start':1},
				loadingMsg:'正在查询信管系统额度编号,请稍后...',
				callback : function(data) {
					if(data.obj){
						if(data.obj.length == 0)
						{
							mini.alert('信管系统未查询到该企业客户的额度批复信息!','系统提示');								    	
						}else{
							mini.get("lmtNo").setData(data.obj);
						}
					}else{
						mini.alert('信管系统未查询到该企业客户的额度批复信息!','系统提示');	
					}
				}
			});	
		}

		//接口查询信管系统客户信息、质押物信息
		function queryGuarData(){
			var cif = mini.get("cifNo").getValue();
			var lmtNo = mini.get("lmtNo").getValue();
			if(cif == null || cif == "") {
				mini.alert('基础资产客户号为空，请确认参数！','提示');
				return false;
			} else if (lmtNo == null || lmtNo == "") {
				mini.alert('额度编号为空，请确认参数！','提示');
				return false;
			}
			grid2.clearRows();

			//查询质押物
			CommonUtil.ajax({
				url:"/fbz004/getFbz004",
				data:{cif:cif,lmtNo:lmtNo},
				callback:function(resp){
					if(resp != null && resp.obj != null){
						if(resp.obj[0].respCde!='15700000000000'){
							mini.alert('信管系统提示：'+resp.obj[0].respMsg,'提示');
							return;
						}
						if(resp.obj.length == 0){
							mini.alert('信管系统未查询到相关质押数据','提示');
							return;
						}

						var datas = [];
						for ( var i = 0; i < resp.obj.length; i++) 
						{
							datas.push({
								seq           :i,
								guartyMode    :resp.obj[i].guartyMode,
								guarType      :resp.obj[i].guarType,
								pledgeAmt     :resp.obj[i].confirmValue,
								pledgeRate    :resp.obj[i].confirmLTV,
								colRightNo    :resp.obj[i].colRightNo,
								guarantor     :resp.obj[i].colAssetOwner,
								guarantorIndus:resp.obj[i].colHolderInd
							});
						}
						grid2.setData(datas);
					}
				},
				callerror:function(resp) {
					return 'error';
				}
			});

			//查询信管客户信息
			CommonUtil.ajax({
				url:"/fbz001/getFbz001",
				data:{cif:cif},
				callback:function(resp){
					if(resp != null && resp.obj != null)
					{
						mini.get("assetCname").setValue(resp.obj.customerName);
						mini.get("holdingType").setValue(resp.obj.entHoldingType);
						mini.get("companySize").setValue(resp.obj.sCope);
						mini.get("regCapital").setValue(resp.obj.regCapital);
						mini.get("innerLevel").setValue(resp.obj.inGrade);
						mini.get("outerLevel").setValue(resp.obj.outGrade);
						mini.get("assetCustLocate").setValue(resp.obj.area);
						mini.get("assetSno").setValue(resp.obj.fiCilityCredit);
						/***
						 * indusCatBig 行业大类
						 * indusCatMid 行业中类
						 * indusCatSmall 行业小类
						 */
						setPartyKindData(resp.obj.indusCatBig,resp.obj.indusCatMid,resp.obj.indusCatSmall);
					}
				}
			});
		}

		function delGuarData(){
			var rows = grid2.getSelecteds();
			if (rows.length > 0) {
				grid2.removeRows(rows, true);
				mini.alert("删除成功!","消息提示");
			}else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		function onCustQuery(e) {
			var btnEdit = e.sender;
			mini.open({
				url : CommonUtil.baseWebPath() + "/mini_cp/MiniCoreCustQueryEdits.jsp",
				title : "客户选择",
				width : 750,
				height : 500,
				ondestroy : function(action) {		
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						if (data && !data.cifNo) {
							btnEdit.setValue(data.party_id);
							btnEdit.setText(data.party_id);
							mini.get("assetCname").setValue(data.party_name);

						}else if(data && data.cifNo){
							data.core_custno = data.cifNo;
							data.party_id = data.cifNo;
							data.party_name = data.custName;

							btnEdit.setValue(data.party_id);
							btnEdit.setText(data.party_id);
							mini.get("assetCname").setValue(data.party_name);
						}

					}// end if
				}
			});
		}


	</script>
</body>
</html>

