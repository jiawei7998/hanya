<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<html>
<body style="width:100%;height:100%;background:white">
	<fieldset><legend>费用录入</legend>
	<div id="data_form" height = "300px;">
		<table width="100%">
			<tr>
				<td class='red-star'>客户编号</td>
				<td>	
					<input class="mini-hidden" id="dealNo" name="dealNo" />
					<input class="mini-hidden" id="refNo" name="refNo" />
					<input id='passagewayCno' name = 'passagewayCno' style="width:80%;" class="mini-buttonedit" onbuttonclick="onCustQuery" required="true"  maxLength="25" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"  allowInput="true"/>
				</td>
				<td class='red-star'>客户名称</td>
				<td><input id='passagewayName' name='passagewayName' style="width:80%;" class="mini-textbox" required="true"  vtype="maxLength:50"/></span></td>
				<td class='red-star'>客户类型</td>
				<td><input id='custType' name='custType' style="width:80%;" class="mini-combobox" required="true"  vtype="maxLength:10" data="CommonUtil.serverData.dictionary.cType" /></td>
			</tr>
			<tr>
				<td class='red-star'>行业大类</td>
				<td><input id='bigKind' name='bigKind' style="width:80%;" class="mini-combobox" onvaluechanged="onBigCatChanged" required="true" /></td>
				<td>行业中类</td>
				<td><input id='midKind' name='midKind' style="width:80%;" class="mini-combobox" onvaluechanged="onMiddleCatChanged" required="false"/></td>
				<td>行业小类</td>
				<td><input id="smallKind" name="smallKind" style="width:80%;" class="mini-combobox" required="false"/></td> 
			</tr>
				 
			<tr>
				<td>联系人</td>
				<td><input id='contact' name='contact' style="width:80%;" class="mini-textbox" required="false" vtype="maxLength:15"/></td>
				<td>联系电话</td>
				<td><input id='contactPhone' name='contactPhone' style="width:80%;" class="mini-textbox" required="false" vtype="maxLength:25" onvalidation="CommonUtil.onValidation(e,'mobile',[null])" />
				</td>
				<td>联系地址</td>
				<td><input id='contactAddr' name='contactAddr' style="width:80%;" class="mini-textbox" required="false" vtype="maxLength:50" /></td>
			</tr>

			<tr>
				<td class='red-star'>费用类型</td>
				<td><input id='feeType' name='feeType' style="width:80%;" class="mini-combobox" value="pass" required="true"  data="CommonUtil.serverData.dictionary.feeCategory" /></td>
				<td class="red-star">收付方向</td>
				<td><input id="feeSituation" name="feeSituation" onvaluechanged="onFeeSituationChanged" style="width:80%;" class="mini-combobox" required="true"  data="CommonUtil.serverData.dictionary.feeSituation" value='Pay'/></td>
				<td class='red-star'>费用频率</td>
				<td><input id='intFre' name='intFre' style="width:80%;" class="mini-combobox" required="true"  data="CommonUtil.serverData.dictionary.CouponFrequently"/></td>
			</tr>
			<tr>
				<td class="red-star">参与计算</td>
				<td>
					<input id="isPass" name="isPass" style="width:80%;" class="mini-combobox" required="true"  data="CommonUtil.serverData.dictionary.YesNo" value='1'/>
				</td>
				<td>扣划日期</td>
				<td><input id="cashtDate" name="cashtDate" style="width:80%;" class="mini-datepicker"/></td>
				<td class="red-star">费用币种</td>
				<td>
					<input id="feeCcy" name="feeCcy" style="width:80%;" class="mini-combobox" required="true"  data="CommonUtil.serverData.dictionary.Currency" value='CNY' enabled="false"/>
				</td>	
			</tr>
			<tr>
				<td>费率</td>												
				<td><input id="feeRate" name="feeRate" style="width:80%;" class="mini-spinner" changeOnMousewheel="false" onvalidation="CommonUtil.onValidation(e,'numberRange',[0,100])" format="p4" allowlimitvalue="false"  value="null"/></td>
				<td class='red-star'>计息基准</td>
				<td><input id="feeBasis" name="feeBasis" style="width:80%;" class="mini-combobox" required="true"  data="CommonUtil.serverData.dictionary.DayCounter" value='Actual/360'/></td>
				<td class='red-star'>帐号</td>
				<td><input id="passagewayBankacccode" name="passagewayBankacccode" style="width:80%;" class="mini-textbox" required="true"  maxLength = "32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/></td>
			</tr>
			<tr>
				<td class='red-star'>账户名称</td>
				<td><input id="passagewayBankaccname" name="passagewayBankaccname" style="width:80%;" class="mini-textbox" required="true"  vtype="maxLength:50"/></td>
				<td class="red-star">开户行号</td>
				<td><input id="passagewayBankcode" name="passagewayBankcode" style="width:80%;" class="mini-textbox" required="true"  maxLength = "14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"/></td>
				<td class="red-star">开户行名</td>
				<td><input id="passagewayBankname" name="passagewayBankname" style="width:80%;" class="mini-textbox" required="true"  vtype="maxLength:50"/></td>
			</tr>
			<tr>
				<td>备注</td>
				<td colspan="6"><input id="remark" name="remark" class="mini-textarea" vtype="maxLength:100" style="width:80%;" height="50"/></td>
			</tr>
			<tr>
				<td colspan="6">
					<a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
					<a id="update_btn" class="mini-button" style="display: none"   onclick="update">修改</a>
					<a id="delete_btn" class="mini-button" style="display: none"   onclick="del">删除</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear">清空</a>
				</td>	
			</tr>
		</table>
	</div> 
	</fieldset>
	<div id="grid1" class="mini-datagrid  borderAll" style="width:100%;height:350px;" 
		idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client" showPager="false"
		pageSize="20"  multiSelect="false" allowAlternating="true" frozenStartColumn="0" frozenEndColumn="3">
		<div property="columns">
			<div type="indexcolumn" width="40" headerAlign="center" allowSort="false">序号</div>                                                                                                           
			<div field="dealNo"                            headerAlign="center" allowSort="false" visible="false">费用流水号</div> 
			<div field="refNo"                             headerAlign="center" allowSort="false" visible="false">原交易流水号</div> 
			<div field="passagewayCno"         width="80" headerAlign="center" allowSort="false" align="center">客户编号</div> 
			<div field="passagewayName"        width="280" headerAlign="center" allowSort="false">客户名称</div> 
			<div field="contact"               width="80"  headerAlign="center" allowSort="false">联系人</div> 
			<div field="contactPhone"          width="100" headerAlign="center" allowSort="false">联系电话</div> 
			<div field="contactAddr"           width="120" headerAlign="center" allowSort="false">联系人地址</div> 
			<div field="bigKind"               width="80"  headerAlign="center" allowSort="false" visible="false">行业大类</div>    
			<div field="midKind"               width="120" headerAlign="center" allowSort="false" visible="false">行业中类</div>    
			<div field="smallKind"             width="80"  headerAlign="center" allowSort="false" visible="false">行业小类</div> 
			<div field="bigKindName"      headerAlign="center" allowSort="true" width="120">行业大类名称</div>                                                               	
			<div field="midKindName"      headerAlign="center" allowSort="true" width="120">行业中类名称</div>                                                                     	
			<div field="smallKindName"    headerAlign="center" allowSort="true" width="120">行业小类名称</div>    
			<div field="feeType"               width="80"  headerAlign="center" allowSort="false"  renderer="CommonUtil.dictRenderer" data-options="{dict:'feeCategory'}"        >费用类型</div>    
			<div field="intFre"                width="120" headerAlign="center" allowSort="false"  renderer="CommonUtil.dictRenderer" data-options="{dict:'CouponFrequently'}"   >费用频率</div>     
			<div field="cashtDate"             width="80"  headerAlign="center" allowSort="false" align="center">扣划日期</div> 
			<div field="custType"          width="80"  headerAlign="center" allowSort="false"  renderer="CommonUtil.dictRenderer" data-options="{dict:'cType'}"              >客户类型</div> 
			<div field="isPass"                width="120" headerAlign="center" allowSort="false"  renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}" align="center">是否参与利息计算</div> 
			<div field="feeCcy"                width="70"  headerAlign="center" allowSort="false"  renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}"           >费用币种</div>      
			<div field="feeBasis"              width="80"  headerAlign="center" allowSort="false"  renderer="CommonUtil.dictRenderer" data-options="{dict:'DayCounter'}"  align="center">计息基础</div>    
			<div field="feeSituation"          width="80"  headerAlign="center" allowSort="false"  renderer="CommonUtil.dictRenderer" data-options="{dict:'feeSituation'}" align="center">收付方向</div> 
			<div field="feeRate"               width="80"  headerAlign="center" allowSort="false" align="right" numberFormat="p4">费率</div> 
			<div field="passagewayBankacccode" width="120" headerAlign="center" allowSort="false">银行帐号</div> 
			<div field="passagewayBankaccname" width="210" headerAlign="center" allowSort="false">账户名称</div> 
			<div field="passagewayBankcode"    width="120" headerAlign="center" allowSort="false">开户行行号</div> 
			<div field="passagewayBankname"    width="210" headerAlign="center" allowSort="false">开户行名称</div> 
			<div field="remark"                width="200" headerAlign="center" allowSort="false">备注</div> 
		</div>
	</div>

	<script type="text/javascript">
		//获取参数传递	
		var url = window.location.search;
		var prdNo = CommonUtil.getParam(url,"prdNo");
		//操作类型
		var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
		var dealNo = CommonUtil.getParam(url,"dealNo");
		
		mini.parse();

		function tradeManagerWin()
		{
			if(operType == 'A'){
				return top["tradeEditTabs_" + prdNo + "_" + operType];
			} else {
				return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
			}
		}

		var grid = mini.get("grid1");
		//父页面的参数值
		var tradeData = tradeManagerWin().param;
		
		if($.inArray(operType,['A','U']) == -1){//不是新增和修改 禁用按钮
			mini.get("add_btn").setEnabled(false);
			mini.get("update_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
			mini.get("clear_btn").setEnabled(false);
		}

		//初始化数据
		CommonUtil.ajax({
			url:"/TdFeesPassAgewayController/getBaseAssetListVo",
			data:{refNo:tradeData.dealNo},
			callback:function(data){
				grid.setData(data.obj);
			}
		});

		//异步加载行业类型数据字典
		CommonUtil.loadPartyKindDict("bigKind","-1");

		//产品大类控制产品中类
		function onBigCatChanged(e){
			CommonUtil.loadPartyKindDict("midKind",mini.get("bigKind").getValue());
			mini.get("smallKind").setData([]);
		}

		//产品中类控制产品小类
		function onMiddleCatChanged(e){
			CommonUtil.loadPartyKindDict("smallKind",mini.get("midKind").getValue());
		}

		function onFeeSituationChanged(e){
			var value = mini.get("feeSituation").getValue();
			if(value == 'Pay'){
				mini.get("isPass").setEnabled(true);
			}else{
				mini.get("isPass").setValue('0');
				mini.get("isPass").setEnabled(false);
			}
		}

		function onCustQuery(e) {
			var btnEdit = e.sender;
			mini.open({
				url : CommonUtil.baseWebPath() + "/mini_cp/MiniCoreCustQueryEdits.jsp",
				title : "客户选择",
				width : 750,
				height : 500,
				ondestroy : function(action) {	
					if (action == "ok") 
					{
						var iframe = this.getIFrameEl();
						var data = iframe.contentWindow.GetData();
						data = mini.clone(data); //必须
						clear();
						if (data && !data.cifNo) {
							btnEdit.setValue(data.party_id);
							btnEdit.setText(data.party_id);
							btnEdit.focus();

							setCustData(data);

						}else if(data && data.cifNo){
							data.core_custno = data.cifNo;
							data.party_id = data.cifNo;
							data.party_name = data.custName;
							data.party_code = data.cifNo;

							btnEdit.setValue(data.party_id);
							btnEdit.setText(data.party_id);
							btnEdit.focus();
							
							CommonUtil.ajax({
								url:'/CounterPartyController/selectCounterPartyList',
								data:{'party_id' : data.party_id},
								callback:function(data){
									setCustData(data[0]);
								}
							});	
							
						}
					}// end if

				}
			});
		}

		function setCustData(data){
			mini.get("passagewayName").setValue(data.party_name);
			mini.get("custType").setValue(data.custType);

			//产品大类
			mini.get("bigKind").setValue(data.party_bigkind);

			//产品中类
			if(CommonUtil.isNotNull(data.party_bigkind)){
				CommonUtil.loadPartyKindDict("midKind",data.party_bigkind);
				mini.get("midKind").setValue(data.party_midkind);
			}
			
			//产品小类
			if(CommonUtil.isNotNull(data.party_midkind)){
				CommonUtil.loadPartyKindDict("smallKind",data.party_midkind);
				mini.get("smallKind").setValue(data.party_smallkind);
			}
			
			if(CommonUtil.isNotNull(data.constact_list) && data.constact_list.length > 0) {//联系人
				mini.get("contact").setValue(data.constact_list[0].name);
				mini.get("contactPhone").setValue(data.constact_list[0].cellphone);
				mini.get("contactAddr").setValue(data.constact_list[0].address);

			} else {
				mini.get("contact").setValue('');
				mini.get("contactPhone").setValue('');
				mini.get("contactAddr").setValue('');
			}
			
			CommonUtil.ajax({
				url: "/TdCustAccController/searchTdCustAccPage",
				data: mini.encode({ custmerCode: data.party_id, state: '6', cny: 'CNY' }),
				callback: function (data) {
					if (data) {
						if (data.obj.rows) {
							partyAccData = data.obj.rows[0];
							if (partyAccData) {
								mini.get("passagewayBankacccode").setValue(partyAccData.bankaccid);
								mini.get("passagewayBankaccname").setValue(partyAccData.bankaccname);
								mini.get("passagewayBankcode").setValue(partyAccData.openBankLargeAccno);
								mini.get("passagewayBankname").setValue(partyAccData.openBankName);
							}
						}
					}
				}
			});
		}

        function save() {
			if(grid.data.length <= 0){
				mini.confirm("列表中无数据，确认要保存吗?","系统提示",function(action){
					if(action == 'ok'){
						saveData();
					}// end if
				});
			}else{
				saveData();
			}
		}

		function saveData(){
			//判断客户号不能重复
			var passagewayCnos = {};
			var cnos = [];
			var tempCno = null;
			//判断付费总利率不超过合同利率  
			var totalRate = 0;
			var tempRate = 0;
			for(var i = 0; i < grid.data.length; i++){
				tempCno = passagewayCnos[grid.data[i].passagewayCno];
				if(CommonUtil.isNull(tempCno)){
					passagewayCnos[grid.data[i].passagewayCno] = grid.data[i].passagewayCno;

				}else{
					mini.alert("通道客户["+grid.data[i].passagewayCno+"]不能重复!","消息提示")
					return;
				}
				
				if(grid.data[i].feeSituation == 'Pay')
				{
					tempRate = parseFloat(grid.data[i].feeRate);
					totalRate += tempRate;
				}

				cnos.push(grid.data[i].passagewayCno);
			}// end for

			if(chechCredit(cnos) == false){
				mini.alert("通道方客户号或者对手客户号中必须包含授信客户!","消息提示")
				return;
			}

			var contractRate = tradeManagerWin().getFieldValue("tadeInfo","contractRate");
			if(CommonUtil.isNotNull(contractRate)){
				if(totalRate >= parseFloat(contractRate)){
					mini.alert("付费总利率不能大于等于合同利率!","提示");
					return;
				}
			}

			var param = {dealNo : tradeData.dealNo};
			param["feesPassagewyList"] = grid.getData(true);
			
			var json = mini.encode(param);
			CommonUtil.ajax({
				url:"/ProductApproveController/updateProductApproveSub",
				data:json,
				callback:function(data){
					mini.alert("保存成功.","提示");
				}
			});
		}

		function chechCredit(cnos){
			//额度客户号检查,额度客户号必须是 交易对手号 或者 基础资产中录入的客户号、或者通道客户号
			var data = tradeManagerWin().getTabData("tadeInfo");
			if(CommonUtil.isNotNull(data.creditCust) && data.amtManage == 'B')//占用同业额度
			{
				var flag = false;
				cnos.push(data.cNo);
				if($.inArray(data.creditCust,cnos) > -1){
					flag = true;
				}
				return flag;
			}
			return true;
		}

		function add(){
			var form = new mini.Form("data_form");
			if(validateForm(form) == false){
				return;
			}

			var data = form.getData(true);
			data.bigKindName = mini.get("bigKind").getText();
			data.midKindName = mini.get("midKind").getText();
			data.smallKindName = mini.get("smallKind").getText();
			grid.addRow(data, grid.data.length);
			mini.alert("新增成功!","消息提示");
		}

		function del(){
			var rows = grid.getSelecteds();
            if (rows.length > 0) {
				grid.removeRows(rows, true);
				mini.alert("删除成功!","消息提示");
            }else{
				mini.alert("请选择要删除的列!","消息提示");
			}
		}

		function clear(){
			var form = new mini.Form("#data_form");
			form.clear();
			mini.get("feeCcy").setValue("CNY");
		}

		function onRowDblClick()
		{
			var form = new mini.Form("data_form");
			var row =  grid.getSelected();
			form.setData(row,true,false);
			form.setIsValid(true);

			var controls = mini.findControls(function(control){
				if(control.type == "buttonedit") return true;    //获取所有mini-textbox控件
			});
			for (var i = 0; i< controls.length; i++) 
			{
				if(CommonUtil.isNotNull(controls[i].setText) && CommonUtil.isNotNull(controls[i].getValue)){
					controls[i].setText(controls[i].getValue());
				} 
			}

			CommonUtil.loadPartyKindDict("midKind",row.bigKind);
			mini.get("midKind").setValue(row.midKind);

			CommonUtil.loadPartyKindDict("smallKind",row.midKind);
			mini.get("smallKind").setValue(row.smallKind);
		}

		function validateForm(form,flag){
			form.validate();
			if(form.isValid() == false){
				mini.alert("交易验证失败,请检查表单是否填写正确!","消息提示")
				return false;
			}

			var row = form.getData(true);
			var feeRate = row.feeRate;
			if(CommonUtil.isNotNull(feeRate) && feeRate <= 0){
				mini.alert("费率必须大于0!","消息提示")
				return false;
			}

			var cashtDate = mini.get("cashtDate").getFormValue();
			if(CommonUtil.isNotNull(cashtDate)){
				var vDate = tradeManagerWin().getFieldValue("tadeInfo","vDate");
				var mDate = tradeManagerWin().getFieldValue("tadeInfo","mDate");
				if(cashtDate < vDate || cashtDate > mDate){
					mini.alert("扣划日期必须介于起息日和到期日之间!","消息提示")
					return false;
				}
			}
			return true;
		}

		function update(){
			var row =  grid.getSelected();
			if(row){
				var form = new mini.Form("data_form");
				if(validateForm(form) == false){
					return;
				}
				var data = form.getData(true);
				data.bigKindName = mini.get("bigKind").getText();
				data.midKindName = mini.get("midKind").getText();
				data.smallKindName = mini.get("smallKind").getText();

				grid.updateRow(row,data);

				mini.alert("修改成功!","消息提示");
			}else{
				mini.alert("请选择要修改的列!","消息提示");
			}
		}

	</script>
</body>
</html>
		
	
