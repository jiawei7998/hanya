﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>

<script src="<%=basePath%>/miniScript/miniui/res/swfupload.js" type="text/javascript"></script>
<script src="<%=basePath%>/miniScript/miniui/res/multiupload.js" type="text/javascript"></script>
<link href="<%=basePath%>/miniScript/miniui/res/multiupload.css" rel="stylesheet" type="text/css" />

	<style>
	    body{
        margin:0;padding:0;border:0;width:100%;height:100%;overflow:hidden;
    }  
	</style>

<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="25%" showCollapseButton="true">
		<div id="toolbar1" class="mini-toolbar">
			<input id="key" class="mini-textbox" onenter="onKeyEnter" emptyText="请输入目录名称"/>
			<a class="mini-button" style="display: none"  onclick="search()"  >查询</a>
		</div>
		<ul id="fileTree" class="mini-tree" style="height:90%;" showTreeIcon="true" textField="text" 
		   idField="id" parentField="parentId" expandOnLoad="true" onnodeclick="searchAttach">        
		</ul>
	</div>
	<div showCollapseButton="true">
		<div id="multiupload1" class="uc-multiupload borderAll" style="width:100%;height:50%;" 
			flashurl="<%=basePath%>/miniScript/miniui/res/swfupload.swf"
			uploadurl="<%=basePath%>/sl/AttachController/uploadAttachByJqueryFileUpload"
			onuploaderror="onUploadError" onuploadsuccess="onUploadSuccess" onbeforeupload = "onBeforeUpload">
		</div>
		<div id="fileDatagrid" class="mini-datagrid borderAll" style="width:100%;height:50%;" showPager="false" 
			allowAlternating="true" sortMode="client" allowCellEdit="true">
			<div property="columns">
				<div type="indexcolumn"></div>                
				<div field="name" width="150" headerAlign="center" allowSort="true">文件名称</div>    
				<div field="size" width="100" headerAlign="center" allowSort="true" numberFormat="#,0" align="right">文件大小(byte)</div> 
				<div field="createUserName" width="100" headerAlign="center" allowSort="true" align="center">创建人</div>
				<div field="action" headerAlign="center" allowSort="true" align="center" renderer="onActionRenderer">操作</div> 
			</div>	
		</div>
	</div>
</div>
<script type="text/javascript">
	//获取参数传递	
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url,"prdNo");
	var operType = CommonUtil.getParam(url,"operType");// A新增 D详情 U更新 approve审批
	var dealNo = CommonUtil.getParam(url,"dealNo");

	mini.parse();

	function tradeManagerWin()
	{
		if(operType == 'A'){
			return top["tradeEditTabs_" + prdNo + "_" + operType];
		} else {
			return top["tradeEditTabs_" + prdNo + "_" + operType + "_" + dealNo];
		}
	}

	//父页面的参数值
	var tradeData = tradeManagerWin().param;
	var attachTreeMap = tradeManagerWin().attachTreeMap;

	//加载Tree  
	var json = {
		'refType':attachTreeMap.refType,
		'refId':attachTreeMap.dealNo,
		'prdNo':attachTreeMap.prdNo,
		'prdName':attachTreeMap.prdName,
		'dealNo':attachTreeMap.dealNo
	};
	CommonUtil.ajax({
		url:"/AttachTreeMapController/searchAttachTreeMap",
		data:json,
		callback : function(data) {
			mini.get('fileTree').setData(data.obj);
		}
	});

	var tree = mini.get("fileTree");
	function search() {
		var key = mini.get("key").getValue();
		if (key == "") {
			tree.clearFilter();
		} else {
			key = key.toLowerCase();                
			tree.filter(function (node) {
				var text = node.text ? node.text.toLowerCase() : "";
				if (text.indexOf(key) != -1) {
					return true;
				}
			});
		}
	}

	function onActionRenderer(e) {
		var grid = e.sender;
		var record = e.record;
		var id = record.id;

		var s = '<a class="mini-button" style="display: none"  onclick="downLoad(\'' + id + '\')">下载</a> '
              + '<a class="mini-button" style="display: none"   onclick="del(\'' + id + '\')">删除</a>';
		return s;
	}

	function del(id){
		var url = "/AttachController/removeAttach/"+id;
		CommonUtil.ajax({
			url : url,
			data : {},
			callback : function(data) {
				mini.alert("删除成功","系统提示");
				searchAttach();
			}
		});
	}

	function downLoad(id){
		var url = CommonUtil.pPath + "/sl/AttachController/downloadAttach/"+id;                                                                                                 
		$('<form action="'+ url +'" method="get"></form>').appendTo('body').submit().remove();
	}

	function searchAttach(){
		var selectedNode = tree.getSelectedNode();  
		var param = {'refType':selectedNode.refType,'refId':selectedNode.id};
		//加载上传完成的附件
		CommonUtil.ajax({
			url:"/AttachController/searchAttach",
			data : param,
			callback : function(data) {
				var rows = [];
				if(CommonUtil.isNotNull(data.obj) && data.obj.length > 0)
				{	
					var files;
					for(var i = 0; i < data.obj.length; i++){
						files = data.obj[i].files;
						if(CommonUtil.isNotNull(files) && files.length > 0){
							rows.push({
								'id':files[0].id,
								'name':files[0].name,
								'size':files[0].size,
								'createUserName':files[0].createUserName,
								'url':files[0].url,
								'deleteUrl':files[0].deleteUrl,
								'action':''
							});
						}
					}
				}// end if
				mini.get('fileDatagrid').setData(rows);
			}
		});
	}

	function onKeyEnter(e) {
		search();
	}
		
	//上传附件
    function getData() {
        var grid = mini.get("multiupload1");
        var data = grid.getData();
        var json = mini.encode(data);
        alert(json);
    }

    function onUploadSuccess(e) {
		//加载上传完成的附件
		searchAttach();
	}
	
    function onUploadError(e) {
        //e = { file, code, message }
        alert("上传失败：" + e.file + ' ' + e.message);
    }

	function onBeforeUpload(e) {
	   //判断是否选中tree node
	   var selectedNode = tree.getSelectedNode(); 
		if(typeof(selectedNode) == "undefined")
		{
			mini.alert('请选择文件夹！','提示信息');
			e.cancel = true;

		}else{
			e.sender.postParam = {
				'prdNo':attachTreeMap.prdNo,
				'dealNo':attachTreeMap.dealNo,
				'refType':attachTreeMap.refType,
				'refId':selectedNode.id
			}
		}
    }
</script>