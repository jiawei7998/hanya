<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<%
	String amt=request.getParameter("amt");
	String vDate=request.getParameter("vDate");
	String mDate=request.getParameter("mDate");
%>
<script type="text/javascript">
	var cashFees_${param.type}_${param.prdNo}={};
	cashFees_${param.type}_${param.prdNo}.tabOptions=$.getTabOptions();
	var dealIndex = null;
	var amt='<%=amt %>';
	var vDate='<%=vDate %>';
	var mDate='<%=mDate %>';
	cashFees_${param.type}_${param.prdNo}.loadDta = function(){
		CommonUtil.ajax({
			url:"/TdCashflowInterestController/searchPageTdTrdFees",
			data:{dealNo:'${param.dealNo}' == '' ? '0' : '${param.dealNo}'},
			callback:function(data){
				$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find('#data_grid').sldatagrid('loadData',data.obj);
			}
		});
	};
	cashFees_${param.type}_${param.prdNo}.appendRow =  function(){
		var rate=$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#executeRate").slnumberbox('getValue');
		var basic=conValue($("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#dayCounter").slcombobox('getValue'));
		var date=conValue($("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox('getValue'));
		var intFre=conValue($("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashIntFee").slcombobox('getValue'));
		var cashBank=conValue($("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashBank").sltextbox('getValue'));
		var start=new Date(Date.parse(vDate.replace('-','/'))).getTime();
		var end=new Date(Date.parse(mDate.replace('-','/'))).getTime();
		var dates=Math.abs((start-end)/(1000*60*60*24));
		var cash=amt*rate/basic*dates;
		if(!CommonUtil.isValid($("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id).find("#cashFees_${param.type}_${param.prdNo} #search_form"))){return false;}
		$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find('#data_grid').sldatagrid('appendRow',{
			cashBank: $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashBank").sltextbox('getValue'),
			//cashtDate: $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox('getValue'),
			dayCounter : $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#dayCounter").slcombobox('getValue'),
			cashIntfee : $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashIntFee").slcombobox('getValue'),
			feeType : $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feeType").slcombobox('getValue'),
			//feesAmt : $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feesAmt").slnumberbox('getValue'),
			executeRate : $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#executeRate").slnumberbox('getValue')			 
		});
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feeBasis").slcombobox('clear');
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feeType").slcombobox('clear');
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feeRate").sltextbox('clear');
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feesAmt").sltextbox('clear');		 
	}
	
	cashFees_${param.type}_${param.prdNo}.removeRow = function(){
		var rows = $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find('#data_grid').sldatagrid('getChecked');
		$.each(rows, function(i, n){
			$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find('#data_grid').sldatagrid('deleteRow',$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find('#data_grid').sldatagrid('getRowIndex',n));
		});
	}
	
	function conValue (value){
		if(value == "Actual/365"){
			return 365;
		}else if(value == "Actual/360"){
			return 360;
		}
	}
	
		
	$(document).ready(function(){

		$.parser.parse($("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id).find("#cashFees_${param.type}_${param.prdNo}").parent());
		/*******************************************************************
		 *组件定义
		 *******************************************************************/
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find('#data_grid').sldatagrid({
			fit:true,
			title:"费用维护",
			rownumbers:true,
			showFooter:true,
			columns:[[
				{field:'cashflowId',title:'费用流水号',hidden:true},   
				{field:'dealNo',title:'原交易流水号',hidden:true}, 
				{field:'feeType',title:'费率类型',width:80,dict:'rateCashTyp'},  
				{field:'cashBank',title:'托管行',width:80},
				{field:'dayCounter',title:'计息基础',width:80,dict:'DayCounter'},
				{field:'cashIntfee',title:'收取频率',width:120,dict:'CouponFrequently'},
				{field:'executeRate',title:'费率(%)',width:80,formatter:function(value){return CommonUtil.moneyFormat(value.mul(100),2);}}
				//{field:'cashtDate',title:'扣划日期',width:80},
				//{field:'feesAmt',title:'收取固定金额',width:100}
			]],
			onClickRow : function(index, row){
				dealIndex = index;
			}
		});
		
		
		
		/*******************************************************************
		 *按钮：事件定义
		 *******************************************************************/
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#add_btn").sllinkbutton({
			 onClick : function(){
				 cashFees_${param.type}_${param.prdNo}.appendRow();
			 }
		 });
		
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#delete_btn").sllinkbutton({
			 onClick : function(){
				 cashFees_${param.type}_${param.prdNo}.removeRow();
			 }
		 });
		 
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feeType").slcombobox({
			 onChange : function(newData,oldData){
				 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feesAmt").numberbox("clear");
				 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox("clear");
				 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#executeRate").slnumberbox("clear");
				 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#dayCounter").slcombobox("clear");
				 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashIntFee").slcombobox("clear");
				 if(newData == 1) {
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox({required:true});
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feesAmt").slnumberbox({required:true}); 
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feesAmt").slnumberbox("enable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox("enable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#executeRate").slnumberbox("disable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#dayCounter").slcombobox("disable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashIntFee").slcombobox("disable");
				 } else if(newData == 2) {
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#executeRate").slnumberbox({required:true});
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#dayCounter").slcombobox({required:true});
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashIntFee").slcombobox({required:true});
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#feesAmt").slnumberbox("disable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox("disable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#executeRate").slnumberbox("enable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#dayCounter").slcombobox("enable");
				 	 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashIntFee").slcombobox("enable");
				 }
			 }
		 });
		 
		 $("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox({
			 onChange : function(newData,oldData){
				 if(newData == '' || newData == null) {
				 	return false;
				 }
			 	 //起息日
				 var vDate = $("#" + ProductApprove_${param.type}_${param.prdNo}.tabOptions.id + " #productApprove_form").find("#vDate").sldatebox("getValue");
				 //到期日
				 var mDate = $("#" + ProductApprove_${param.type}_${param.prdNo}.tabOptions.id + " #productApprove_form").find("#mDate").sldatebox("getValue");
				 if(newData < vDate) {
				 	$.messager.alert("提示","划款日不能小于起息日","info");
				 	$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox("setValue",oldData);
				 	return false;
				 }
				 if(newData > mDate) {
				 	$.messager.alert("提示","划款日不能大于到期日","info");
				 	$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id + " #cashFees_${param.type}_${param.prdNo}").find("#cashtDate").sldatebox("setValue",oldData);
				 	return false;
				 }
			 }
		 });
		 
		 /**
		客户组件
		**/
		$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id).find("#cashFees_${param.type}_${param.prdNo}").find("#cashBankFuzzy").sltextbox({
			fuzzy:{
				dialog:{
					title:"客户选择",
				    urls:"./cp/CoreCustQueryEdit.jsp",
				    onCloseed : function(row){
					    if(row){
					    	$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id).find("#cashFees_${param.type}_${param.prdNo}").find("#cashBank").sltextbox("setValue",row.cifNo);
						}
					}
				}
			}
		});
		 
		/*******************************************************************
		 *数据初始化 
		 *******************************************************************/
		 cashFees_${param.type}_${param.prdNo}.loadDta();
		 if($.inArray(ProductApprove_${param.type}_${param.prdNo}.tabOptions.type,["detail","approve"]) > -1){
				$("#"+cashFees_${param.type}_${param.prdNo}.tabOptions.id).find("#cashFees_${param.type}_${param.prdNo}").layout("remove","north");
		 }
		 
		 
		 
	});
</script>
<div id="cashFees_${param.type}_${param.prdNo}" class="easyui-layout" data-options="fit:true,border:true"> 
	<div data-options="region:'north',split:true" style="height:280px;">
		<table id="search_form" class="form_table" width="100%">			
			<tr>
				<td class="red-star">费率类型</td>
				<td><input id="feeType" class="easyui-slcombobox" data-options="required:true,dict:'rateCashTyp',disabled:true,value:'2'" /></td>
				<td>托管行 </td>
				<td><input id="cashBank" class="easyui-sltextbox" data-options="required:true,height:22" />
				<input id="cashBankFuzzy" class="easyui-sltextbox" data-options="width:20" />
				</td>
			</tr>
			<tr>
			    <td>管理费率(%)</td>
				<td><input id="executeRate" class="easyui-slnumberbox" data-options="precision:6,min:0,max:100,required:true,textTimes:100" /></td>
				<td>计息基础</td>
				<td><input id="dayCounter" class="easyui-slcombobox" data-options="required:true,dict:'DayCounter',initValue:'Actual/360' "/></td>
			</tr>
			<tr>
				<td class="red-star">收取频率</td>
				<td><input type="hidden" id="dealNo">
				<input type="hidden" id="refNo">
				<input id="cashIntFee" class="easyui-slcombobox" data-options="required:true,dict:'CouponFrequently',value:'0'" /></td>
			</tr>
			<tr>
				
				<!-- <td>收取固定金额</td>
				<td><input id="feesAmt" class="easyui-slnumberbox" data-options="disabled:true,precision:6,groupSeparator:','" /></td> -->	
			</tr>
        </table>
        <div class="btn_div">
        	<a href="javascript:void(0)"  id="add_btn" class="easyui-linkbutton" data-options="iconCls:'icon-add'">新增</a>
        	<a href="javascript:void(0)"  id="delete_btn" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">删除</a>
        	<!-- <a href="javascript:void(0)"  id="dailyInterest_btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'">计提明细</a> -->
        </div>
	</div>
    <div data-options="region:'center'" >
    	<table id="data_grid"></table>
    </div>
</div>