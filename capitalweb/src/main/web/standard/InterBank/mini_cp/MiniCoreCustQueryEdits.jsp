<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  
	<body style="width:100%;height:100%;background:white">
		<fieldset class="mini-fieldset">
			<legend>客户查询</legend>
			<div style="width:100%;">
				<table id="search_form" width="100%" height="100%">
					<tr>
						<td>
							<input id="custName" name="custName" class="mini-textbox" labelField="true"  label="客户名称:" style="width:80%;" emptyText="请输入客户名称" />
						</td>
						<td>
							<input id="cifNo" name="cifNo" class="mini-textbox" labelField="true"  label="客户编号:" style="width:80%;" emptyText="请输入精准客户编号" />
						</td>
					</tr>
				</table>
			</div>

			<div id="toolbar1" class="mini-toolbar" style="margin-right: 80px;">
				<table style="width:100%;">
					<tr>
						<td style="width:100%;text-align:right;">
							<a class="mini-button" style="display: none"   onclick="searchCustPage('local')">本地客户查询</a>
							<a class="mini-button" style="display: none"   onclick="searchCustPage('cName')">核心客户名称查询</a>
							<a class="mini-button" style="display: none"   onclick="searchCustPage('cNo')">核心客户号查询</a>
						</td>
					</tr>
				</table>
			</div>
		</fieldset>
		<div class="mini-fit">
			<div id="datagrid1" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" height="100%"  
				onrowdblclick="onRowDblClick" sizeList="[10]" pageSize="10" allowAlternating="true" sortMode="client">
				<div property="columns">
					<div type="indexcolumn" width="60" align="center" headerAlign="center">序号</div>
					<div field="party_id" width="100" align="center" headerAlign="center">客户号</div>    
					<div field="party_name" width="180" headerAlign="center">客户名称</div>    
					<div field="custType" headerAlign="center">客户类型</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			mini.parse();
			var grid = mini.get("datagrid1");
			var type = 'local';

			grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				if(type == 'local'){
					searchCustLocalPage(pageSize,pageIndex);

				}else if(type == 'cName'){
					searchCustCoreByType(pageSize,pageIndex,type);
				
				}else{
					searchCustCoreByType(pageSize,pageIndex,type);
				}
			});

			function searchCustPage(type_1){
				type = type_1;
				//从核心按名称查询不显示最后一页分页
				$(".mini-pager-last").css("display","");
				$(".mini-pager-right").css("display","");
				$(".mini-pager-pages").css("display","");
				$(".mini-pager-num").removeAttr("disabled");
				$(".mini-pager-last").removeAttr("disabled");

				if(type == 'local'){
					grid.clearRows();
					grid.set({
						columns: [
							{ type: "indexcolumn", width: 50, headerAlign: "center", allowSort: true, header: "序号",align:"center"},
							{ field: "party_id", width: 90, headerAlign: "center", allowSort: true, header: "客户号",align:"center"},
							{ field: "party_name", width: 180, headerAlign: "center", allowSort: true, header: "客户名称"},
							{ field: "custType", width: 80, headerAlign: "center", allowSort: true, header: "客户类型"}
						]
					});
					grid.showPager = true;
					grid.showReloadButton = true;

					searchCustLocalPage(grid.pageSize,0);

				}else if(type == 'cName'){
					var param = new mini.Form("search_form");
					var data = param.getData();
					if(CommonUtil.isNull(data.custName))
					{
						mini.alert('请输入客户名称','消息提示');
						return;
					}

					grid.clearRows();
					grid.showPager = true;
					grid.showReloadButton = true;
					grid.set({
						columns: [
							{ type : "indexcolumn", width: 50, headerAlign: "center", allowSort: true, header: "序号",align:"center"},
							{ field: "cifNo", width: 90, headerAlign: "center", allowSort: true, header: "客户号",align:"center"},
							{ field: "custName", width: 180, headerAlign: "center", allowSort: true, header: "客户名称"},
							{ field: "custType", width: 80, headerAlign: "center", allowSort: true, header: "客户类型"}
						]
					});
					searchCustCoreByType(grid.pageSize,0,type);

				}else{
					var param = new mini.Form("search_form");
					var data = param.getData();
					if(CommonUtil.isNull(data.cifNo))
					{
						mini.alert('请输入客户号','消息提示');
						return;
					}

					grid.clearRows();
					grid.showPager = false;
					grid.showReloadButton = false;
					grid.set({
						columns: [
							{ type : "indexcolumn", width: 50, headerAlign: "center", allowSort: true, header: "序号",align:"center"},
							{ field: "cifNo", width: 90, headerAlign: "center", allowSort: true, header: "客户号",align:"center"},
							{ field: "custName", width: 180, headerAlign: "center", allowSort: true, header: "客户名称"},
							{ field: "custType", width: 80, headerAlign: "center", allowSort: true, header: "客户类型"}
						]
					});
					searchCustCoreByType(grid.pageSize,0,type);
				}
			}
			

			function searchCustLocalPage(pageSize,pageIndex) {
				var param = new mini.Form("search_form");
				var data = param.getData();
				data.party_id = data.cifNo;
				data.party_name = data.custName;
				data.pageNumber = pageIndex+1;
				data.pageSize = pageSize;
				var json = mini.encode(data);
				var url = '/CounterPartyController/searchCounterParty';
				CommonUtil.ajax({
					url : url,
					data : json,
					callback : function(data) {
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
					}
				});
			}
        
			function searchCustCoreByType(pageSize,pageIndex,type) {
				var param = new mini.Form("search_form");
				var data = param.getData();
				data.pageNumber = pageIndex+1;
				data.pageSize = pageSize;
				var json = mini.encode(data);

				var url = '/CIFController/toCifNbr';
				if(type == 'cName'){
					url = '/CIFController/toCifList';
				}
				CommonUtil.ajax({
					url : url,
					data : json,
					callback : function(data) {
						if(type == 'cName'){
							if(data.obj && data.obj.total){
								grid.setTotalCount(data.obj.total);
							}
							grid.setPageIndex(pageIndex);
							grid.setPageSize(pageSize);
							grid.setData(data.obj.rows);

							//隐藏最后一页分页
							if($(".mini-pager-last").length > 0){
								$(".mini-pager-last").css("display","none");
								$(".mini-pager-last").attr("disabled","disabled");
							}
							if($(".mini-pager-right").length > 0){
								$(".mini-pager-right").css("display","none");
							}
							if($(".mini-pager-pages").length > 0){
								$(".mini-pager-pages").css("display","none");
							}
							if($(".mini-pager-num").length > 0){
								$(".mini-pager-num").attr("disabled","disabled");
							}

						}else{
							grid.setTotalCount(data.obj.length);
							grid.setData(data.obj);

						}
					}
				});
			}

			/* 获取所选行的属性的值 */
			function GetData() 
			{
				var row = grid.getSelected();
				if(row.cifNo){
					row.party_id = row.cifNo;
					row.party_name = row.custName;
				}
				return row;
			}

			function onRowDblClick(e) {
				CloseWindow("ok");
			}
			
			function CloseWindow(action) {
				if (window.CloseOwnerWindow)
					return window.CloseOwnerWindow(action);
				else
					window.close();
			}

		</script>
	</body>
</html>