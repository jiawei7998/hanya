﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
	<title>客户管理</title>
</head>
<body style="width:100%;height:100%;background:white">
	<fieldset id="fd2" class="mini-fieldset">
		<legend><label>查询条件</label></legend>
		<div id="search_form">
			<input id="party_id" name="party_id" class="mini-textbox" labelField="true"  label="客户号：" labelStyle="text-align:right;" emptyText="请输入模糊客户号"/>
			<input id="custType" name="custType" class="mini-combobox" labelField="true"  label="客户类型：" labelStyle="text-align:right;"  emptyText="请选择客户类型"data = "CommonUtil.serverData.dictionary.cType"/>
			<input id="party_bigkind" name="party_bigkind" class="mini-combobox" labelField="true"  label="行业大类："  labelStyle="text-align:right;" emptyText="请选择行业大类"data = "CommonUtil.serverData.dictionary.GBT4754_2011"/>
			<input id="party_name" name="party_name" class="mini-textbox" labelField="true"  label="客户名称：" labelStyle="text-align:right;"  emptyText="请输入模糊客户全称"/>
			<span style="float:right;margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"     onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"     onClick="clear">清空</a>
			</span>
		</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit()">修改</a>
	</span>	
	<div id="cpManage" class="mini-fit" style="margin-top: 2px;">	
		<div id="cp_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
		idField="p_code" allowResize="true" pageSize="10" onRowdblclick="custDetail" sortMode="client"
		allowAlternating="true">
			<div property="columns" >
				<div type="indexcolumn" headerAlign="center"  align="center" width="40px">序号</div>
				<div field="party_id" headerAlign="center"  align="center" width="100px" allowSort="true">客户号</div>    
				<div field="party_name" headerAlign="center"width="200px">客户全称</div>                            
				<div field="custType"  headerAlign="center"width="120px"  renderer="CommonUtil.dictRenderer" data-options="{dict:'cType'}">客户类型</div>
				<div field="custAccty"   headerAlign="center"  width="100px"renderer="CommonUtil.dictRenderer" data-options="{dict:'custAccty'}">客户会计类型</div>                                
				<div field="party_bigkind_name"  headerAlign="center"width="160px" >行业大类</div>
				<div field="party_midkind_name"  headerAlign="center"width="120px" >行业中类</div>
				<div field="party_smallkind_name"   headerAlign="center"  width="100px">行业小类</div>
				<div field="party_organ_type"  headerAlign="center" width="120px">金融机构类型</div>
				<div field="party_organ_grade" headerAlign="center" width="120px">金融机构评级</div>
				<div field="p_party_id"   headerAlign="center" align="center" width="120px" allowSort="true">上级机构编号</div>
				<div field="idType"  headerAlign="center" align="center" width="100px">证件类型</div>                
				<div field="idOwnerCntry"  headerAlign="center" align="center" width="100px">证件发行国家</div>
				<div field="party_business_license" headerAlign="center"  width="160px" >证件号</div> 
				<div field="update_time"  headerAlign="center" align="center" width="160px" allowSort="true">更新时间</div>                                                                                               
			</div>
		</div>
	</div>
		
	
	<script>
		mini.parse();
		$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var grid = mini.get("cp_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				search(10, 0);
			});
		});

		top["cpManage"]=window;
        function getData(){
            var grid = mini.get("cp_grid");
            var record =grid.getSelected(); 
            return record;
        }
		//修改
        function modifyedit(){
            var grid = mini.get("cp_grid");
            var  record =grid.getSelected();
            if(record){
                var url = CommonUtil.baseWebPath() + "/mini_cp/MiniCPEdit.jsp?action=edit";
                var tab = { id: "cpManageEdit", name: "cpManageEdit", text: "客户修改", url: url ,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name};
                top['win'].showTab(tab);
            }else{
                mini.alert("请选中一条记录!","系统提示"); 
            }
        }

        //双击一行打开详情信息
        function custDetail(e){
            var grid = e.sender;
            var row = grid.getSelected();
            if(row){
                var url = CommonUtil.baseWebPath() + "/mini_cp/MiniCPEdit.jsp?action=detail";
                var tab = { id: "cpManageDetail", name: "cpManageDetail", text: "客户详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
                top['win'].showTab(tab);
            }
        }
		
		//查询(带分页)
		function search(pageSize,pageIndex){
			var form =new mini.Form("search_form");
			form.validate();
			if (form.isValid() == false) return;//表单验证
			var data =form.getData();//获取表单数据
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			var param = mini.encode(data); //序列化成JSON
	    	CommonUtil.ajax({
				url:"/CounterPartyController/searchCounterParty",
				data:param,
				callback:function(data){
						
					var grid =mini.get("cp_grid");
					//设置分页
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					//设置数据
					grid.setData(data.obj.rows);
				}
			});
		}
		
		//清空
		function clear(){
			var form =new mini.Form("search_form");
			form.clear();
			search(10,0);
		}

		
			
		
	</script>
</body>
</html>