<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<div id="Irseries" class="mini-fit">   
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" showPager="false"
	idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" >
		<div property="columns" >
			<div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
			<div field="i_code" width="150" align="center"  headerAlign="center" allowSort="true">基准利率代码</div>    
			<div field="ir_name" width="150" align="center"  headerAlign="center" allowSort="true">基准利率名称</div>  
			<div field="beg_date" width="100px" align="center"  headerAlign="center"  >市场数据日期</div>                                
			<div field="s_close" width="180" numberFormat="p4" align="center" headerAlign="center" allowSort="true" >收盘价(%)</div>
			<div field="s_high" width="180" numberFormat="p4" align="center" headerAlign="center" allowSort="true" >最高价(%)</div>
			<div field="imp_date" width="180" align="center" headerAlign="center" >导入日期</div>
		</div>
	</div>
</div> 
<script type="text/javascript">
	mini.parse();
	var url=window.location.search;
	var i_code=CommonUtil.getParam(url,"i_code");
	var _bizDate = '<%=__bizDate %>';
	var grid=mini.get("datagrid");
	//查询
	function search(){
		CommonUtil.ajax({
			url:"/QdTtMktIrController/selectTtMktSeriessBySize",
			data:mini.encode({"searchSize":10,"i_code":i_code,"beg_date":_bizDate}),
			callback : function(data) {
				if(data.obj.length == 0) {
					mini.alert("暂无基准利率行情！","系统提示");
				}
				grid.setData(data.obj);
			}
		});
	}


	function onRowDblClick(e){
		CloseWindow("ok");				  
	}
	function CloseWindow(action) {
	   if (window.CloseOwnerWindow) return window.CloseOwnerWindow(action);
	   else window.close();
	}
	function GetData() {
	    var row = grid.getSelected();
	    return row;
	}


	$(document).ready(function(){
		search();
	});
</script>
</body>
</html>