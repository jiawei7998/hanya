<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
	<legend>客户查询</legend>

<div id="CpManage">
	<table id="search_form" class="mini-form" style="width:100%;">
		<tr>
			<td><input id="party_name" name="party_name" class="mini-textbox"  labelField="true"   label="客户名称" style="width:100%;" /></td>
			<td><input id="party_shortname" name="party_shortname" class="mini-textbox" labelField="true"   label="客户代码" style="width:100%;" /></td>
		</tr>
	</table>
</div>
<div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
	<table style="width:100%;">
		<tr>
			<td colspan="3">
				<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
				<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
			</td>
		</tr>
	</table>
</div>
</fieldset>
<div id="CpManage" class="mini-fit">   
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
	idField="dealNo" allowResize="true" onrowdblclick="onRowDblClick" border="true" >
		<div property="columns" >
			<div field="party_id" width="150" align="center"  headerAlign="center" allowSort="true">客户代码</div>    
			<div field="party_name" width="150" align="center"  headerAlign="center" allowSort="true">CN客户名称</div>                            
			<div field="is_active" width="180" align="center" headerAlign="center" >GB客户名称</div>
			<div field="party_code"  width="180" align="center" headerAlign="center" allowSort="true">客户类型</div>                                
			<div field="core_custno" width="180" align="center" headerAlign="center" allowSort="true" >同业客户标识</div>
			<div field="sf_cpty_country" width="180" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CDDZ0012'}">国别</div>
			<div field="at_type" width="180" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'AT0001'}">准入级别</div>
			<div field="at_date" width="180" align="center" headerAlign="center" allowSort="true" >准入级别有效期</div>
			<div field="fcci_codeName" width="180" align="center" headerAlign="center" allowSort="true" >所属一级法人</div>
		</div>
	</div>
</div> 
<script type="text/javascript">
	mini.parse();
	var grid=mini.get("datagrid");
	var form=new mini.Form("#search_form");
	//清空
	function clear(){
		form.clear();
	}
	//查询
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误","系统提示");
			return ;
		}
		var data=form.getData();
		data['pageSize']=pageSize;
		data['pageNumber']=pageIndex+1;
		var params=mini.encode(data);
		CommonUtil.ajax({
			url:"/TdCustEcifController/searchTdCustEcifPaged",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	grid.on("beforeload",function(e){
		e.cancel = true;
		var pageSize=e.data.pageSize;
		var pageIndex=e.data.pageIndex;
		search(pageSize,pageIndex);
	});

	function onRowDblClick(e){
		CloseWindow("ok");				  
	}
	function CloseWindow(action) {
	   if (window.CloseOwnerWindow) return window.CloseOwnerWindow(action);
	   else window.close();
	}
	function GetData() {
	    var row = grid.getSelected();
	    return row;
	}


	$(document).ready(function(){
		search(10,0);
	});
</script>
</body>
</html>