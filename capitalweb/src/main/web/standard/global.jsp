<%@page language="java" pageEncoding="UTF-8"%>
<%@page import="com.singlee.capital.common.util.Constants"%>
<%@page import="com.singlee.capital.common.session.SlSession"%>
<%@page import="com.singlee.capital.system.session.impl.SlSessionHelper"%>
<%@page import="com.singlee.capital.common.session.SessionStaticUtil"%>
<%@page import="com.singlee.capital.system.model.TaUser"%>
<%@page import="com.singlee.capital.system.model.TtInstitution"%>
<%@page import="com.singlee.capital.dayend.service.DayendDateServiceProxy"%>
<%@page import="com.singlee.capital.system.model.TaBrcc"%>
<%@ page import="com.alibaba.fastjson.JSON" %>
<%@ page import="com.singlee.hrbcap.util.DateUtils" %>
<%
SlSession __slSession = SessionStaticUtil.getSlSessionByHttp(request);
TaUser __sessionUser = SlSessionHelper.getUser(__slSession)==null?new TaUser():SlSessionHelper.getUser(__slSession);
TtInstitution __sessionInstitution = SlSessionHelper.getInstitution(__slSession)==null?new TtInstitution():SlSessionHelper.getInstitution(__slSession);
TaBrcc __brcc = SlSessionHelper.getBrcc(__slSession)==null?new TaBrcc():SlSessionHelper.getBrcc(__slSession);

String __bizDate = DayendDateServiceProxy.getInstance().getSettlementDate();
String __bizYesterdayDate = DateUtils.getAfterTreatingDate(__bizDate,"subtract",1);
boolean __is_admin =SlSessionHelper.isAdmin(__slSession);
boolean __is_header_inst = SlSessionHelper.isInstitutionTypeHeader(__slSession);
String basePath = request.getContextPath();
String _isFta = SlSessionHelper.isInstitutionTypeFt(__slSession);
%>
<script type="text/javascript">
    var branchId ='<%=__sessionUser.getBranchId()%>';
    var branchModule ='<%=__brcc.getBranchModule()%>';
    var userId ='<%=__sessionUser.getUserId()%>';
    var opicsBr ='<%=__sessionUser.getOpicsBr()%>';//用户所属OPICS部门
    var instId ='<%=__sessionUser.getInstId()%>';
    var roleIds ='<%=JSON.toJSONString(__sessionUser.getRoleIdList())%>';
    var basePath ='<%=basePath%>';
    var sysDate ='<%=__bizDate%>';
</script>
<script src="<%=basePath%>/miniScript/boot.js" type="text/javascript"></script>
<script src="<%=basePath%>/miniScript/jquery.min.js" type="text/javascript"></script>
<script src="<%=basePath%>/miniScript/miniui/miniui.js" type="text/javascript" ></script>
<link href="<%=basePath%>/miniScript/miniui/themes/default/iconfont/iconfont.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>/miniScript/miniui/themes/default/miniui.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>/miniScript/miniui/themes/icons.css" rel="stylesheet" type="text/css" />
<link href="<%=basePath%>/miniScript/miniui/themes/xinli-red/skin-red.css" rel="stylesheet" type="text/css" />
<script src="<%=basePath%>/miniScript/sockjs.min-1.1.1.js" type="text/javascript" ></script>
<script src="<%=basePath%>/miniScript/stomp.min-2.3.3.js" type="text/javascript"/>
<script src="<%=basePath%>/miniScript/stomp.min-2.3.3.js" type="text/javascript" ></script>
<link href="<%=basePath%>/miniScript/jqueryToast/css/toast.style.min.css" rel="stylesheet"/>
<script src="<%=basePath%>/miniScript/jqueryToast/js/toast.script.js" type="text/javascript" ></script>

<script src="<%=basePath%>/miniScript/common_mini.js" type="text/javascript"></script>
<script src="<%=basePath%>/miniScript/approveFlowBaseUrl.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/jquery.json.min.js"></script>
<%-- <link rel="shortcut icon"  href="<%=basePath%>/miniScript/css/ico/favicon-harb.ico" type="image/x-icon"></link>
 --%><!--[if lt IE 9]>
    <script type="text/javascript" src="<%=basePath%>/miniScript/json2.js"></script>
<![endif]-->
<script type="text/javascript">
    //控制输入框输入后退键时页面不后退
    function keyDown(e){
        var currentKey;
        var obj;
        if(window.event) { //IE
            currentKey = window.event.keyCode;
            obj = window.event.srcElement;
        } else if(e.which) { //FF
            currentKey = e.which;
            obj = e.target;
        }
        var t = obj.type;
        var vRead = obj.readOnly;
        var vDisable = obj.disabled;
        vRead = (vRead == undefined)?false:vRead;
        vDisable = (vDisable == undefined)?false:vDisable;
        var flag1 = currentKey == 8 && (t == "password" || t=="text" || t == "textarea")&&(vRead==true||vDisable==true);
        var flag2 = currentKey == 8 && t != "password" && t != "text" && t != "textarea";
        if(flag2||flag1){
            return false;
        }
    }
    document.onkeydown=keyDown;
</script>
<style>
    /*body{*/
    /*     background:#fff!important;*/
    /*    }*/
    .mini-grid-cell {
        border-right: 1px dashed #d7d7d7;
    }
</style>