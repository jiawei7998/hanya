<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<head>
    <title>Portal 门户</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <script type="text/javascript" src="<%=basePath%>/miniScript/portal/Portal.js"></script>
    <link href="<%=basePath%>/miniScript/portal/portal.css" rel="stylesheet" type="text/css" />
    <style>
        .mini-panel.max
        {
            position:fixed !important;
            width:100% !important;
            height:100% !important;
            left:0 !important;
            top:0 !important;
            z-index:10000;
        }
    </style>
</head>
<body id="body1">
    <div id="button">
        <span style="float: left; margin-left: 0px"> 
            <a id="reset_btn" class="mini-button" style="display: none"   iconCls="layout-3-center" onclick="columnStyle('center',3)"></a>
            <a id="refresh_btn" class="mini-button" style="display: none"  iconCls="layout-2-right" onclick="columnStyle('left',2)"></a>
            <a id="refresh_btn" class="mini-button" style="display: none"  iconCls="layout-2" onclick="columnStyle('average',2)"></a>
            <a id="refresh_btn" class="mini-button" style="display: none"  iconCls="layout-1" onclick="columnStyle('big',1)"></a>
        </span>
        <span style="float: right; margin-right: 10px"> 
            <a id="reset_btn" class="mini-button" style="display: none"   onclick="reset()">重置</a>
            <a id="refresh_btn" class="mini-button" style="display: none"   onclick="refresh()">刷新</a>
        </span>
    </div>
    <div id="portal">

    </div>
   

</body>
</html>

<script type="text/javascript">
    mini.parse();

    $(document).ready(function(){
        //初始化
        var portal = new mini.ux.Portal();
        portal.set({
            style: "width: 100%;height:400px",                
            columns: ["64%", "34%"]   // 如果布局样式几行几列
        });
        searchPanels(portal,2); 
        
    });

    function searchPanels(portal,cols){
        var panelArray = [];
        var rowsArray = new Array();
        
        CommonUtil.ajax({
				url:'/PortalController/getAllPortal',
                data: {"branchId":branchId},
                showLoadingMsg:true,
				messageId:mini.loading('正在查询数据...', "请稍后"),
				callback : function(data) {
                    var rows = 1;
                    var panelIdsArray = new Array();
                    for(var index=0; index<data.length; index++){
                        var panel = {};
                        if(cols == 1){//  1 列
                            //panel.column = data[index].portalColumn;
                            panel.column = 0;
                        }else if(cols == 2){ //2列
                            if(index%cols == 0){
                                panel.column = 0;
                            }else{
                                panel.column = 1; 
                            }

                        }else if(cols == 3){ //3列
                            if(index%cols == 0){
                                panel.column = 0;
                            }else if(index%cols == 1){
                                panel.column = 1; 
                            }else if(index%cols == 2){
                                panel.column = 2; 
                            }
                        }
                         
                        panel.id =data[index].portalId;
                        

                        panel.title =data[index].portalTitle;
                        panel.url =data[index].portalBody;
                        panel.allowDrag =data[index].portalAllowDrag==1 ? true:false;
                        panel.height =data[index].portalHeight;
                        panel.buttons="collapse max close"; //按钮内容
                        panel.onbuttonclick= onbuttonclick //最大化
                        panelArray.push(panel);
                        rowsArray[index] = data[index].portalRows;//列宽
                    };
                    
                   
                    //portal.render(document.body);
                    portal.render(document.getElementById("portal"));
                    
                    portal.setPanels(panelArray);

                    // var bodyEl = portal.getPanelBodyEl("p1");document.body
                    // bodyEl.appendChild(document.getElementById("table1"));

                    //获取配置的panels信息
                    var panels = portal.getPanels();
				}
			});
    }

   


    
    
    //panel
    // portal.setPanels([
    //     {   
    //         column: 0, //列
    //         id: "p1", //id
    //         title: "panel1",//名称 
    //         // showCloseButton: true,//是否显示关闭按钮
    //         // showCollapseButton:true, //是否显示折叠框
    //         body: "", //body内容
    //         allowDrag: true, //是否运行拖拽
    //         buttons: "collapse max close", //按钮内容
    //         height:400,//高度
    //         onbuttonclick: onbuttonclick //最大化
            
    //     }
    // ]);
    

    //alert(panels.length);
    /////////////////////////////////////////////////////////////////////////////////

    function maxPanel(id) {
        var panel = mini.get(id);
        panel.maxed = true;
        $(panel.el).addClass("max");
        $(panel.el).find(".mini-tools-max").addClass("mini-tools-restore");
        mini.layout();
    }

    function restorePanel(id) {
        var panel = mini.get(id);
        panel.maxed = false;
        $(panel.el).find(".mini-tools-max").removeClass("mini-tools-restore");
        $(panel.el).removeClass("max");
        mini.layout();
    }

    //最大化
    function onbuttonclick(e) {
        var panel = this;

        if (e.name == "max") {
            if (panel.maxed) {
                restorePanel(panel);
            } else {
                maxPanel(panel);
            }
        }
    }

    //重置
    function reset(){
        CommonUtil.ajax({
            url: '/PortalVirtualController/searchPortalVirtual',
            data: {"branchId":branchId,"userId":userId,"roleId":roleId},
            callback: function (data){
                var portal = new mini.ux.Portal();
                portal.set({
                    style: "width: 100%;height:400px",                
                    columns: ["64%", "34%"]   // 如果布局样式几行几列
                });
                searchPanels(portal,2); 
                refresh();
            }
        });

    }

    //刷新
    function refresh(){
        window.location.reload();
    }

    //切换列表样式
    function columnStyle(style,cols){
        var portal = new mini.ux.Portal();
        //先清空id="portal"的div里的panels(面板)
        document.getElementById("portal").innerHTML = ""; 
        
        if(cols == 1){//每行1列
            portal.set({
                style: "width: 100%;height:400px",                
                columns: ["100%"]   // 如果布局样式几行几列
            });
            searchPanels(portal,1);
        }
        if(cols == 3){//每行3列
            portal.set({
                style: "width: 100%;height:400px",                
                columns: [250, "100%", 250]   // 如果布局样式几行几列
            });
            searchPanels(portal,3);
        }
        if(cols == 2){//每行2列
            if(style=="left"){//左面大，右面小
                portal.set({
                    style: "width: 100%;height:400px",                
                    columns: ["64%", "34%"]   // 如果布局样式几行几列
                });
                searchPanels(portal,2);
            }else if(style=="average"){//左右一样大
                portal.set({
                    style: "width: 100%;height:400px",                
                    columns: ["50%", "50%"]   // 如果布局样式几行几列
                });
                searchPanels(portal,2);
            }
        }
       
    }

</script>