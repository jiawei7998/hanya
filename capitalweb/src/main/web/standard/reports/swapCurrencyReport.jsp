<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>货币互换头寸敞口</legend>
		<div id="search_form" style="width: 100%;margin:5px 0 5px 0">
			<input id="br" name="br" class="mini-combobox mini-mustFill" labelField="true" label="部门：" vtype="maxLength:5" required="true"
	           emptyText="请填写部门" labelStyle="text-align:right;" width="280px" value='<%=__sessionUser.getOpicsBr()%>' data="CommonUtil.serverData.dictionary.opicsBr"/>
		    <input id="trad" name="trad" class="mini-textbox" labelField="true" label="交易员：" emptyText="请填写交易员" labelStyle="text-align:right;" width="280px"/>
	        <input id="ccy" name="ccy" class="mini-combobox"  data="CommonUtil.serverData.dictionary.Currency" width="280px" emptyText="请选择币种" labelField="true"  label="币种：" labelStyle="text-align:right;"/>
			<!-- <nobr>
			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期："
						ondrawdate="onDrawDateStart"  onvaluechanged='dateCalculation' labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			<span>~</span>
			<input id="endDate" name="endDate" class="mini-datepicker" 
						ondrawdate="onDrawDateEnd"  onvaluechanged='dateCalculation'  emptyText="结束日期" format="yyyy-MM-dd" value="<%=__bizDate%>"/>
			</nobr>
			 -->
			<span style="float: right; margin-left: 20px;margin-top:6px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="export_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
			</span>
		</div>
	</fieldset>
	
	<div class="mini-fit" style="margin-top: 2px;">
	<div id="grid1" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
		allowResize="true" border="true" sortMode="client" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			      <div field="br" width="60" headerAlign="center" align="center"allowSort="true">机构</div> 
                  <div field="dealno" width="120" headerAlign="center" align="center" allowSort="true">交易编号</div>
                  <div field="product" width="80" allowSort="false" align="center" headerAlign="center" >产品</div>
                  <div field="prodtype" width="60" headerAlign="center" align="center" allowSort="true" >产品类型</div>
                  <div field="cost" width="120" headerAlign="center" align="center" allowSort="true" >产品中心</div>
                  <div field="cmne" width="120" headerAlign="center"align="center" align="center" >交易对手</div>
                  <div field="trad" width="90" headerAlign="center"align="center" >交易员</div> 
                  <div field="dealdate" width="120" headerAlign="center" align="center" renderer='onDateRenderer'>交易日期</div> 
                  <div field="settledate" width="120" headerAlign="center" align="center" renderer='onDateRenderer'>起息日</div> 
                  <div field="matdate" width="120" headerAlign="center"align="center" renderer='onDateRenderer'>到期日</div> 
                  <div field="currency" width="80" headerAlign="center" align="center">互换币种</div>
                  <div field="side" width="60" headerAlign="center"align="center" >交易方向</div> 
                  <div field="ratecode" width="150" headerAlign="center" align="center">利率代码</div> 
                  <div field="principle" width="160" headerAlign="center" align="center" >本金交割方式</div> 
                  <div field="initexchdate" width="120" headerAlign="center"align="center" renderer='onDateRenderer'>期初交割日</div> 
                  <div field="iccy1" width="70" headerAlign="center" align="center">币种1</div>
                  <div field="initexchamtccy1" width="150" headerAlign="center"align="right" renderer='TypeChange'>币种1金额</div> 
                  <div field="iccy2" width="70" headerAlign="center" align="center">币种2</div> 
                  <div field="initexchamtccy2" width="150" headerAlign="center" align="right" renderer='TypeChange'>币种2金额</div> 
                  <div field="finexchdate" width="120" headerAlign="center"align="center" renderer='onDateRenderer'>期末交割日</div> 
                   <div field="fccy1" width="70" headerAlign="center" align="center">币种1</div>
                  <div field="finexchamtccy1" width="120" headerAlign="center"align="right" renderer='TypeChange'>币种1金额</div> 
                  <div field="fccy2" width="70" headerAlign="center" align="center">币种2</div> 
                  <div field="finexchamtccy2" width="150" headerAlign="center" align="right" renderer='TypeChange'>币种2金额</div> 
		</div>
	</div>
</div>
	
<script>
	mini.parse();
	//获取当前tab
	var grid = mini.get("grid1");
	grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
	$(document).ready(function() {
		//initDate();//初始化查询时间段
		query();
	});
	//日期格式转换
	function onDateRenderer(e) {
		if(e.value!=null){
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
	        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}
	}
	//amount类型转换
	function TypeChange(e){
		var num=parseFloat(e.value);
		return  num.toFixed(2);
	}
	// 初始化数据
	function query() {
    	search(grid.pageSize, 0);
    }
	function search(pageSize,pageIndex){
		var form = new mini.Form("#search_form");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		//dateCalculation();//检查查询时间段长度
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/ifsCurSwapCount/searchCurSwapCount";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				if(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			}
		});
	}
	
	function clear(){
        var form=new mini.Form("search_form");
        form.clear();
       // initDate();
       mini.get("br").setValue(opicsBr);//自贸区加br
        query();
	}
	function initDate(){
		var date=new Date(sysDate.replace(/-/g,'/'));
	    var year=date.getFullYear();
	    var month=date.getMonth()+1;
	    var start=new Array();
	    start.push(year);
	    start.push(month);
	    start.push("1");
	    mini.get("startDate").setValue(start.join("-"));
	    mini.get("endDate").setValue(getLastDay(year,month));
	}
	//获得每月最后一天
	 function getLastDay(year,month){         
         var new_year = year;    //取当前的年份
         var new_month = month++;//取下一个月的第一天，方便计算（最后一天不固定）
         if(month>12) {         
          new_month -=12;        //月份减
          new_year++;            //年份增
         }         
         var new_date = new Date(new_year,new_month,1);                //取当年当月中的第一天
         return (new Date(new_date.getTime()-1000*60*60*24));//获取当月最后一天日期
    } ;

	//日期
	function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate= mini.get("endDate").getValue();
        if(CommonUtil.isNull(endDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	
	function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if(CommonUtil.isNull(startDate)){
        	return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }
	//计算时间段长度
	function dateCalculation(){
		var start = mini.get("startDate").getValue();
		var end= mini.get("endDate").getValue();
		if(CommonUtil.isNull(start)||CommonUtil.isNull(end)){
			return;
		}
		var subDate= Math.abs(parseInt((end.getTime() - start.getTime())/1000/3600/24));
		if(subDate>30){
			mini.alert('查询时间大于一个月，请重新选择');
			return;
		}
	}
	//导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
					var form = new mini.Form("#search_form");
					var data=form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportCurSwapCount";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</body>
</html>