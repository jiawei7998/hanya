<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>台账查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="br" name="br" class="mini-combobox" labelField="true" required="true"  data="CommonUtil.serverData.dictionary.dp"  label="部门：" labelStyle="text-align:right;" labelStyle="width:100px" emptyText="请选择部门" width="280px" />
			<input id="rpdate" name="rpdate" class="mini-datepicker" labelField="true" label="日期：" labelStyle="text-align:right;" emptyText="请选择日期" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">导出</a>
			</span>
		</div>
	</div>
</fieldset> 	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
			 allowResize="true"  allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="30px" headerAlign="center">序号</div>
			<div type="expandcolumn"></div>
			<div field="br" width="180px" align="center"  headerAlign="center" >机构号</div>    
			<div field="dealno" width="80px" align="left"  headerAlign="center" >交易号</div>                            
			<div field="cno" width="180px" align="left"  headerAlign="center" >交易对手</div>                            
			<div field="cmne" width="100px" align="right"   headerAlign="center" >客户英文简称</div>
			<div field="cost" width="120px" align="center"  headerAlign="center" allowSort="true">成本中心</div>
			<div field="retcode"name="dealdate" width="150px" align="right"   headerAlign="center" allowSort="true">交易日期</div>
			<div name="startdate" field="retmsg" width="120px" align="center"  headerAlign="center" allowSort="true">开始日期</div>
			<div field="matdate" width="150px" align="right"   headerAlign="center" allowSort="true">到期日</div> 
			<div name="port" field="cnretmsg" width="120px" align="center"  headerAlign="center" allowSort="true">投资组合</div> 
			<div name="trad" field="errcode" width="120px" align="center"  headerAlign="center" allowSort="true">交易员</div>
			
			<div name="basis" field="errmsg" width="120px" align="center"  headerAlign="center" allowSort="true">固息计息基数代码</div>
			<div field="payrecind" width="100px" align="right"   headerAlign="center" >固息付款/收款标志</div>
			<div field="intpaycycle" width="120px" align="center"  headerAlign="center" allowSort="true">固息付息周期代码</div>
			<div name="notccyamt" field="retcode" width="150px" align="right"   headerAlign="center" allowSort="true">固息名义货币金额</div>
			<div name="ratecode" field="retmsg" width="120px" align="center"  headerAlign="center" allowSort="true">固息利率代码</div>
			<div field="intrate_8" width="150px" align="right"   headerAlign="center" allowSort="true">固息利率</div> 
			<div name="npvamt" field="cnretmsg" width="120px" align="center"  headerAlign="center" allowSort="true">固息净现值</div> 
			
			
			<div name="floatbasis" field="errcode" width="120px" align="center"  headerAlign="center" allowSort="true">浮息计息基数代码</div>
			<div name="floatnotccyamt" field="errmsg" width="120px" align="center"  headerAlign="center" allowSort="true">浮息名义货币金额</div>
			<div name="floatratecode" field="cnretmsg" width="120px" align="center"  headerAlign="center" allowSort="true">浮息利率代码</div> 
			<div name="floatintrate_8" field="errcode" width="120px" align="center"  headerAlign="center" allowSort="true">浮息利率</div>
			<div name="floatnpvamt" field="errmsg" width="120px" align="center"  headerAlign="center" allowSort="true">浮息净现值</div>
			<div name="mtm" field="errmsg" width="120px" align="center"  headerAlign="center" allowSort="true">收付息总金额</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
	}

	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/HsReportController/searchgageList",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
</script>
</body>
</html>
