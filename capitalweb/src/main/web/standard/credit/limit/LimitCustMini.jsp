<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户管理</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form">  
          
    <input id="cno" name="cno" class="mini-textbox" labelField="true"  label="交易对手编号："  width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入交易对手编号" />
    <input id="cliname" name="cliname" class="mini-textbox" labelField="true"  label="客户名称：" width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入中文名称" />
    <input id="cname" name="cname" class="mini-textbox" labelField="true"  label="交易对手代号：" width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入交易对手代号" />
    <input id="ctype" name="ctype" class="mini-combobox" data="CommonUtil.serverData.dictionary.CType" width="330px" emptyText="请选择交易对手类型" labelField="true"  label="交易对手类型：" labelStyle="text-align:right;width:140px"/>
 
        <span style="float:right;margin-right: 100px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
   
    <div class="mini-fit"  title="客户列表">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
		<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
        <div field="cliname" width="180"  headerAlign="center" align="left" >客户名称</div>
        <div field="cno" width="120px" align="center"  headerAlign="center" >交易对手编号</div>
        <div field="cname" width="120px"  headerAlign="center" align="center" >交易对手代号</div>
        <div field="acctngtype" width="190"  headerAlign="center" align="center" >会计类型</div>
        <div field="bic" width="120px" align="center" headerAlign="center"  >SWIFT编码</div>
        <div field="ccode" width="90px" align="center"  headerAlign="center"  >城市代码</div>                            
        <div field="sic" width="150px" align="center"  headerAlign="center" >工业标准代码</div>                            
        <div field="sn" width="120px" align="left"  headerAlign="center" >交易对手简称</div>
        <div field="cfn" width="250px" align="left"  headerAlign="center" >交易对手全称</div>                                
        <div field="ctype" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CType'}">交易对手类型</div>
        <div field="uccode" width="90px" align="center"  headerAlign="center" >国家代码</div>
        <div field="location" width="90px"  headerAlign="center" align="left">所在城市</div>
		</div>
	</div>
</div>   



<script>

mini.parse();


var url = window.parent.tabs.getActiveTab().url;
var prdNo = CommonUtil.getParam(url, "prdNo");
var prdName = CommonUtil.getParam(url, "prdName");
var isoccupy = CommonUtil.getParam(url, "isoccupy");
var form = new mini.Form("#search_form");
var grid=mini.get("datagrid");
var row="";

grid.on("beforeload", function (e) {
	e.cancel = true;
	var pageIndex = e.data.pageIndex; 
	var pageSize = e.data.pageSize;
	search(pageSize,pageIndex);
});



$(document).ready(function() {
    //控制按钮显示
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
        query();
    });
});

//查询按钮
function query(){
    search(grid.pageSize,0);
}

function search(pageSize, pageIndex) {
    var form = new mini.Form("#search_form");
    form.validate();
    if (form.isValid() == false) {
        mini.alert("表单填写错误,请确认!", "提示信息");
        return;
    }

    var data = form.getData();
    data['pageNumber'] = pageIndex + 1;
    data['pageSize'] = pageSize;
    data['branchId']=branchId;
    data['isoccupy']=isoccupy;
    var params = mini.encode(data);

    CommonUtil.ajax({
    	url :"/IfsOpicsCustController/searchPageOpicsCust" ,
        data : params,
        callback : function(data) { 
            grid.setTotalCount(data.obj.total);
            grid.setPageIndex(pageIndex);
            grid.setPageSize(pageSize);
            grid.setData(data.obj.rows);
        }
    });
}

	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}

    function GetData() {
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }

 

 
</script>
 </body>

</html>