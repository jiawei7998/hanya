<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户管理</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form">  
          
    <input id="cno" name="cno" class="mini-textbox" labelField="true"  label="客户编号："  width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入客户编号" />

        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
   
    <div class="mini-fit"  title="客户列表">
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
		<div field="dealNo" width="180" align="" headerAlign="center"  >流水号</div>
        <div field="approveStatus" width="140px" align=""  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div> 
        <div field="custName" width="120" align="" headerAlign="center"  >客户名称</div>
        <div field="productName" width="120" align="" headerAlign="center"  >业务品种名称</div>
        <div field="vdate" width="120px" align="center"  headerAlign="center" allowSort="true">占用日</div>                            
        <div field="mdate" width="120px" align="center"  headerAlign="center" allowSort="true" >到期日</div>                            
        <div field="amt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">占用金额(元)</div>
        
//        <div field="ioper" width="100px" align="center"  headerAlign="center" >经办人</div>
//        <div field="institution" width="150px" align=""  headerAlign="center" >经营单位</div>
   		</div>
	</div>
</div>   



<script>

mini.parse();

var url = window.location.search;
var prdNo = CommonUtil.getParam(url, "prdNo");
var prdName = CommonUtil.getParam(url, "prdName");
var form = new mini.Form("#search_form");
var grid=mini.get("datagrid");
var row="";

grid.on("beforeload", function (e) {
	e.cancel = true;
	var pageIndex = e.data.pageIndex; 
	var pageSize = e.data.pageSize;
	search(pageSize,pageIndex);
});



$(document).ready(function() {
	query();
});

//查询按钮
function query(){
    search(grid.pageSize,0);
}

function search(pageSize, pageIndex) {
    var form = new mini.Form("#search_form");
    form.validate();
    if (form.isValid() == false) {
        mini.alert("表单填写错误,请确认!", "提示信息");
        return;
    }

    var data = form.getData();
    data['pageNumber'] = pageIndex + 1;
    data['pageSize'] = pageSize;
    data['branchId']=branchId;

    var params = mini.encode(data);

    CommonUtil.ajax({
    	url :"/IfsOpicsCustController/searchPageOpicsCust" ,
        data : params,
        callback : function(data) { 
            grid.setTotalCount(data.obj.total);
            grid.setPageIndex(pageIndex);
            grid.setPageSize(pageSize);
            grid.setData(data.obj.rows);
        }
    });
}

	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}

    function GetData() {
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }

 

 
</script>
 </body>

</html>