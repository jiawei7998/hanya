<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>交易使用额度情况信息列表</title>
</head>
<div style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>交易使用额度情况查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="ecifNo" name="ecifNo" class="mini-textbox" labelField="true" label="客户号：" labelStyle="text-align:right;" width="300px" emptyText="请输入客户号" />
			<!-- <input id="ecifNo" name="ecifNo" class="mini-buttonedit" onbuttonclick="onButtonEdit"  label="客户名称：" labelField="true"    width="280px" labelStyle="text-align:right;"  emptyText="请输入客户名称" /> -->
			<input id="prdName" name="prdName" class="mini-textbox" labelField="true" label="额度品种：" labelStyle="text-align:right;" width="300px" emptyText="请输入业务品种" />
			<input id="dealNo" name="dealNo" class="mini-textbox"  label="审批单号：" labelField="true"    width="300px" labelStyle="text-align:right;"  emptyText="请输入审批单号" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset>
<div class="mini-splitter" vertical="true" handlerSize="2px" style="width:100%;height:80%;" allowResize="false">
    <div size="100%">
        <div class="mini-fit" style="width:100%;height:40%;" >
<%--            <div class="mini-panel" title="交易额度使用总览" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true" >--%>
                 <div id="credit_query_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" onrowdblclick="onRowDblClick" sortMode="client" allowAlternating="true"
                    idField="id" pageSize="10" multiSelect="false" sortMode="client" allowAlternating="true">
                    <div property="columns">
                        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
                        <%--                        <div field="custCreditId" width="120px" align="center" headerAlign="center" >额度编号</div>--%>
                        <div field="dealNo" width="200px" align="left"  headerAlign="center" >审批单号</div>
                        <div field="dealType" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'creditType'}">类型</div>
                        <div field="prdNo" width="100px" align="center"  headerAlign="center" >额度品种</div>
<%--                        <div field="creditId" width="100px" align="center"  headerAlign="center" renderer="onTypeRenderer" >额度类型</div>--%>
                        <div field="ecifNo" width="100px" align="center"  headerAlign="center" >客户编号</div>
                        <div field="cnName" width="200px" align="center"  headerAlign="center" >客户名称</div>
                        <div field="loanAmtEcif" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">客户授信额度(元)</div>
                        <div field="avlAmtEcif" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">客户授信可用额度(元)</div>
                        <%--                        <div field="edOpType" width="120px" align="center"  headerAlign="center" >额度操作类型</div>--%>
                        <div field="loanAmtCredit" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信品种额度(元)</div>
                        <div field="avlAmtCredit" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信品种可用额度(元)</div>
                        <div field="edOpAmt" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">使用金额SUM(元)</div>
                        <div field="vDate" width="120px" align="center"  headerAlign="center" >占用日</div>
                        <div field="mDate" width="120px" align="center"  headerAlign="center" >到期日</div>
                    </div>
                </div>
<%--            </div>--%>
        </div>
    </div>

</div>
</div>
<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var row="";

	var grid =mini.get("credit_query_grid");
    grid.on("beforeload",function(e){
        e.cancel = true;
        var pageIndex = e.data.pageIndex; 
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });
	
    
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            query();
        });
    });
	
	//清空
	function clear(){
		form.clear();
		query();
	}

	/* 查询 按钮事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
        data['branchId']=branchId;
        data['dealtype']="1";
		//var url="/edCustBatchStatusController/getTdCustBatchStatusPage";
		var url="/edCustBatchStatusController/selectEdCustLogForDealNoOrPrdNo";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	function onButtonEdit(){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/config/CPMiniManage.jsp",
            title: "选择客户列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cliname);
						btnEdit.setText(data.cliname);
						mini.get("partyId").setValue(party_id);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	
	  function GetData() {
	        var row = grid.getSelected();
	        return row;
	    }
	    
	
	
	  //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }

	
</script>
</body>
</html>
