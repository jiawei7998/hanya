<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<title>客户额度</title>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="FXD";
</script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset title">
		<div id="search_form" style="width:100%">
			<legend>客户额度查询</legend>
			<input id="custNo" name="custNo" class="mini-textbox" labelField="true" label="客户号:" labelSyle="text-align:right;" emptyText="请输入"></input>
			<input id="custName" name="custName" class="mini-textbox" labelField="true" label="客户名称:" labelSyle="text-align:right;" emptyText="请输入"></input>
			<span>
				<a class="mini-button" style="display: none"  id="search_btn" onclick="sea()">查询</a>
				<a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<span style="margin:2px;display: block;">
		<a class="mini-button" style="display: none"  id="add_btn" onClick="add()">新增</a>
		<a class="mini-button" style="display: none"  id="edit_btn" onClick="modify()">修改</a>
		<a class="mini-button" style="display: none"  id="delete_btn" onClick="del()">删除</a>
	</span>
	<div class="mini-fit" style="width:100%;height:100%;">
		<div id="cus_credit_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
			sortMode="client" allowAlternating="true" onrowdbclick="onRowDblClick"
			border="true" allowResize="true">
			<div property="columns">
				<div type="indexcolumn" headerAlign="center" align="center" width="30px">序列</div>
				<div field="custCreditId" headerAlign="center" align="center" width="0px">客户额度唯一编号</div>
				<div field="creditName" headerAlign="center" align="center" width="50px" 
					renderer="CommonUtil.dictRenderer" data-options="{dict:'CustCreditType'}">额度类型</div>
				<div field="custNo" headerAlign="center" align="center" width="50px">客户编号</div>
				<div field="custName" headerAlign="center" align="center" width="50px">客户名称</div>
				<div field="creditStartDate" headerAlign="center" align="center" width="50px">授信起始日期</div>
				<div field="expDate" headerAlign="center" align="center" width="50px">授信结束日期</div>
				<div field="custType" headerAlign="center" align="center" width="50px"
					renderer="CommonUtil.dictRenderer" data-options="{dict:'CType'}">客户类型</div>
				<div field="creditAmt" headerAlign="center" align="center" numberFormat="#,0.00" width="50px">额度总金额</div>
				<div field="quotaUsedAmt" headerAlign="center" align="center" numberFormat="#,0.00" width="50px">已用金额</div>
				<div field="occupyAmt" headerAlign="center" align="center" numberFormat="#,0.00" width="50px">预占用金额</div>
				<div field="quotaAvlAmt" headerAlign="center" align="center" numberFormat="#,0.00" width="50px">剩余额度</div>
			</div>
		</div>
	</div>
	<script>
		mini.parse();
		var grid = mini.get("cus_credit_grid");
		var form = new mini.Form("#search_form");
		var custCreditId = '';
		
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex; 
			var pageSize = e.data.pageSize;
			search(pageSize,pageIndex);
		});
		
	    $(document).ready(function() {
	    	sea();
		});
	
	    //查询按钮
		function sea(){
			search(10,0);
		}
	     
		function search(pageSize, pageIndex) {
			var searchUrl="/CustomerCreditController/search";
			 
			var data = form.getData(true);
			data['pageNumber'] = pageIndex + 1;
			data['pageSize'] = pageSize;
	
			var params = mini.encode(data);
	 
			CommonUtil.ajax({
				url:searchUrl,
				data:params,
				callback:function(data){
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
					grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		
		function add(){
			branchModule = '04';
			var url = CommonUtil.baseWebPath() + "/credit/CustCreditEdit.jsp?action=add&custCreditId="+custCreditId;
			var tab = { id: "rmbsptAdd", name: "rmbsptAdd", title: "客户额度新增",
			url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
			var paramData = {selectData:""};
			CommonUtil.openNewMenuTab(tab,paramData);
		}
		
		function modify() {
			var row = grid.getSelected();
			if(row == null) {
				mini.alert('请选择要修改的内容');
			} else {
				branchModule = '04';
				custCreditId = row.custCreditId;
				var url = CommonUtil.baseWebPath() + "/credit/CustCreditEdit.jsp?action=modify&custCreditId="+custCreditId;
				var tab = { id: "rmbsptAdd", name: "rmbsptAdd", title: "外汇即期补录",
				url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
				var paramData = {selectData:""};
				CommonUtil.openNewMenuTab(tab,paramData);
			}
		}
		
		function del() {
			var selections = grid.getSelecteds();
			if(selections.length>1){
				mini.alert("系统不支持多条数据进行删除","消息提示");
				return;
			}
			var row = grid.getSelected();
			if(row == null) {
				mini.alert('请选择要删除的内容',"消息提示");
				return;
			} else {
				var searchUrl="/CustomerCreditController/delete";
				CommonUtil.ajax({
					url:searchUrl,
					data:{'custCreditId':row.custCreditId},
					callback:function(data){
						if(data.code == 'error.common.0000') {
							mini.alert('操作成功');
							sea();
						} else {
							mini.alert('操作失败');
						}
					}
				});
			}
		}
		
		function clear() {
			form.clear();
			sea();
		}
		
	</script>
</body>
</html>