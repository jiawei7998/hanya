<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
    <title>额度占用维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <input id="institution" name="institution" class="mini-hidden" value='<%=__sessionUser.getInstId() %>'/>
            <input id="state" name="state" class="mini-hidden" value="2">
            <input id="ioper" name="ioper" class="mini-hidden" value="<%=__sessionUser.getUserId() %>">
            <input id="offLine" name="offLine" class="mini-hidden" value="Y">
                <!-- <div class="centerarea">
                    <fieldset>
                    <legend>审批信息</legend>
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="审批单号：" required="true"   enabled="false"    labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;保存后自动生成
                        <input id="ioper" name="ioper" class="mini-textbox" labelField="true"  label="审批发起人："   enabled="false" value='<%=__sessionUser.getUserId() %>'     labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;当前登录的客户经理
                        <input id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true"  label="审批发起机构："  enabled="false"  value='<%=__sessionInstitution.getInstName() %>' vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;客户经理所属的机构
                        <input id="inputDate" name="inputDate" class="mini-datepicker" labelField="true"  label="审批日期："   enabled="false" value='<%=__bizDate %>'   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;发起审批单据具体时间
                    </fieldset>   
                </div> -->
                <fieldset>
                    <legend>审批信息</legend>
                        <div class="leftarea">
                            <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="审批单号：" emptyText="保存后自动生成" required="true"   enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                            <input id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true"  label="审批发起机构："  enabled="false"  value='<%=__sessionInstitution.getInstName() %>' vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        </div>
                        <div class="rightarea">
                            <input id="ioper" name="ioper" class="mini-textbox" labelField="true"  label="审批发起人："   enabled="false" value='<%=__sessionUser.getUserId() %>'     labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                            <input id="inputDate" name="inputDate" class="mini-datepicker" labelField="true"  label="审批日期："   enabled="false" value='<%=__bizDate %>'   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        </div>
                </fieldset>   
                <fieldset>
                <legend>占用要素</legend>
                <div class="leftarea">
                    <!-- <input id="dealNo" name="dealNo" class="mini-textbox"   labelField="true"  label="审批编号：" emptyText="系统自动生成" enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  /> -->
                    <input id="custNo" name="custNo" class="mini-buttonedit"  onbuttonclick="onCustNoSearch" allowInput="false" labelField="true"  label="客户编号：" required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;"/>
                    <input id="productCode" name="productCode" class="mini-buttonedit"  onbuttonclick="onButtonEdit" allowInput="false" labelField="true"  label="产品编号：" required="true"   style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                    <input id="vdate" name="vdate" class="mini-datepicker" labelField="true"  label="起息日："  required="true"    labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    
                    <input id="institution" name="institution" class="mini-textbox" labelField="true"  label="经营单位："  enabled="false"  value="<%=__sessionUser.getInstId() %>"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                	<input style="width:100%;" id="weight" name="weight" class="mini-spinner mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="true"  label="权重%："  labelStyle="text-align:left;width:130px;" />
                </div>			
                <div class="rightarea">
                    <!-- <input id="remark1" name="remark1" class="mini-textbox" labelField="true"  label="上传附件："   value="" labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                    <a class='mini-button' onclick='openW'>资料影像管理</a> -->
                    
                    <input id="custName" name="custName"  class="mini-textbox" labelField="true"  label="客户名称：" enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
                    <input id="productName" name="productName" class="mini-textbox" labelField="true"  label="产品名称：" enabled="false" labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="mdate" name="mdate" class="mini-datepicker" labelField="true"  label="到期日："  required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="amt" name="amt" class="mini-spinner" labelField="true"  label="占用金额(元)：" maxValue="9999999999999" format="n2"  required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    
                </div>
              <!--   <div class="centerarea">
                    <a id="remark1" name="remark1" class="mini-button" style="display: none"  onclick='openW'>
                            <span class="l-btn-left ">资料影像管理 </span>
                    </a>
                </div>	 -->		
                <div class="centerarea">
                    <input id="reason" name="reason" class="mini-textarea" labelField="true"  label="调整说明：" vtype="maxLength:50"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                </div>
                </fieldset>  
                <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
        
            
        
            
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div> 
</div>
<script type="text/javascript">

    mini.parse();   
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field_form");

    var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;

    
    $(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){//审批、详情
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
            var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}

		if($.inArray(action,["edit","approve","detail"])>-1){//修改、审批、详情
            form.setData(row);
            //客户编号
			mini.get("custNo").setValue(row.custNo);
			mini.get("custNo").setText(row.custNo);
            //产品编号
			mini.get("productCode").setValue(row.productCode);
			mini.get("productCode").setText(row.productCode);
			
		}else{//增加 
			
		}
    });
    
    function close(){
		top["win"].closeMenuTab();
    }
    
    function save(){
        form.validate();
		if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

        var saveUrl="";
        if(action == "add"){
            saveUrl="/CustCreditDealController/addCustCreditDeal";
        }else if(action == "edit"){
            saveUrl="/CustCreditDealController/updateCreditDeal";
        }

        var data=form.getData(true);
        var params=mini.encode(data);

        CommonUtil.ajax({
            url:saveUrl,
            data:params,
            callback:function(data){
                mini.alert('保存成功','提示信息',function(){
                    top["win"].closeMenuTab();
                });
            }
		});

    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/operate/mini/CreditProductDialogMini.jsp",
            title: "查询产品",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.productCode);
                        btnEdit.setText(data.productCode);
                        mini.get("productName").setValue(data.productName);
                       
                        searchProductWeight(10,0,data.productCode); 
                        btnEdit.focus();
                    }
                }

            }
        });

	}

    //客户编号的查询
    function onCustNoSearch(){
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/config/CPMiniManage.jsp",
            title: "选择客户列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.clino);
						btnEdit.setText(data.clino);
						mini.get("custName").setValue(data.cliname);
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    
    function openW(){
        var prdNo = mini.get("productCode").getValue();
        var dealNo = mini.get("dealNo").getValue();



        window.open("../base/FileUpLoad.jsp?prdNo="+prdNo+"&dealNo="+dealNo+"&opType=1",'fileUpload','width='+(window.screen.availWidth-10)+',height='+(window.screen.availHeight-30)+ ',top=0,left=0,resizable=yes,status=yes,menubar=no,scrollbars=yes');
    }

</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>	
</body>
</html>