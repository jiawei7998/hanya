<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="./BaseMethod.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>手工释放</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>手工释放查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
            <input id="userId" name="userId" class="mini-hidden"  value="<%=__sessionUser.getUserId() %>">
            <input id="offLine" name="offLine" class="mini-hidden"  value="N">
			<input id="state" name="state" class="mini-hidden" value="3">
			<input id="partyId" name="partyId" class="mini-hidden"/>

			<input id="dealNo" name="dealNo" class="mini-textbox" label="流水号："  labelField="true"  width="280px" labelStyle="text-align:right;" emptyText="请输入审批流水号" />
			<input id="partyName" name="partyName" class="mini-buttonedit" onbuttonclick="onButtonEdit"  label="客户名称：" labelField="true"    width="280px" labelStyle="text-align:right;"  emptyText="请输入客户名称" />
			<input id="oldDealNo" name="oldDealNo" class="mini-textbox" label="原交易编号：" labelField="true"  width="280px" labelStyle="text-align:right;" emptyText="请输入原交易编号"    />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
	<a  id="approve_mine_commit_btn" class="mini-button" style="display: none"    onclick="commit()">提交审批</a>
	<a  id="approve_log" class="mini-button" style="display: none"     onclick="searchlog()">审批日志</a>
	<!-- <a  id="print_bk_btn" class="mini-button" style="display: none"    onclick="print()">打印</a> -->
	<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;" labelField="true" label="审批列表选择：" labelStyle="text-align:right;border:none; background-color:#fff;" 
				value="mine" textField="text" valueField="id" multiSelect="false"
				data="[{id:'mine',text:'我发起的'},{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'}]" onvaluechanged="checkBoxValuechanged">
	</div>
	</span>  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			 
            <div field="dealNo" width="180" align="" headerAlign="center"  >流水号</div>
            <div field="approveStatus" width="140px" align=""  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">审批状态</div> 
            <div field="custName" width="120" align="" headerAlign="center"  >客户名称</div>
            <div field="productCode" width="120" align="" headerAlign="center"  >业务品种</div>
            <div field="productName" width="120" align="" headerAlign="center"  >业务品种名称</div>
            <div field="vdate" width="120px" align="center"  headerAlign="center" allowSort="true">占用日</div>                            
            <div field="mdate" width="120px" align="center"  headerAlign="center" allowSort="true" >到期日</div>                            
            <div field="amt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">释放额度金额(元)</div>
            <div field="ioper" width="100px" align="center"  headerAlign="center" >经办人</div>
            <div field="institution" width="150px" align=""  headerAlign="center" >经营单位</div>
            <!-- <div field="state" width="100px" align="center"  headerAlign="center" >额度状态</div> --> 
            <div field="oldDealNo" width="180" align="" headerAlign="center"  >原交易编号</div> 
            <div field="reason" width="180px" align=""  headerAlign="center" >理由</div>
                
                

		</div>
	</div>
</div>   

<script>
    mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			initButton();
			query();
		});
    });

    //添加
	function add(){
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/operate/CreditTradeManage.jsp",
            title: "原交易选择",
            width: 700,
            height: 600,
            ondestroy: function (action) {

            }
        });

	}


	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
		var url = CommonUtil.baseWebPath() +"../../quota/operate/HandReleaseEdit.jsp?action=edit";
		var tab = {id: "HandReleaseEdit",name:"HandReleaseEdit",url:url,title:"手工释放修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
    }
    
    //查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"../../quota/operate/HandReleaseEdit.jsp?action=detail";
		var tab = {id: "HandReleaseDetail",name:"HandReleaseDetail",url:url,title:"手工释放详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
    }
    

    //清空
	function clear(){
		form.clear();
		query();
    }

    /* 查询 按钮事件 */
	function query(){
		search(grid.pageSize,0);
    }

    /* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
        data['branchId']=branchId;
        data['offLine']='N';
        data['state']='3';
       
		var url=null;

		var approveType = mini.get("approveType").getValue();
		if(approveType == "mine"){//我发起的
			url = "/CustCreditDealController/getCreditDealList";
		}else if(approveType == "approve"){//待审批
            url = "/CustCreditDealController/getCreditDealWaitApprove";
		}else{//已审批
			url = "/CustCreditDealController/getCreditDealApprovedPass";
		}
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
    }
    
    //删除
	function del(){
		var row=grid.getSelected();
		if(!row){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除<span style='color:#FF3333;font-weight:bold;'>" + row.dealNo +"</span>吗?","系统警告",function(value){   
			if (value=='ok'){   
				var data=row;
				params=mini.encode(data);

				CommonUtil.ajax( {
					url:"/CustCreditDealController/deleteCreditDeal",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
    }


    grid.on("select",function(e){
		var row=e.record;
		mini.get("approve_mine_commit_btn").setEnabled(false);
		mini.get("approve_commit_btn").setEnabled(false);
		mini.get("edit_btn").setEnabled(false);
		mini.get("delete_btn").setEnabled(false);
		if(row.approveStatus == "3"){//新建
			mini.get("approve_mine_commit_btn").setEnabled(true);
			mini.get("approve_commit_btn").setEnabled(false);
			mini.get("edit_btn").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
			mini.get("approve_log").setEnabled(false);
		}
		if( row.approveStatus == "6" || row.approveStatus == "5"){//审批通过：6    审批中:5
			mini.get("approve_mine_commit_btn").setEnabled(false);
			mini.get("approve_commit_btn").setEnabled(true);
			mini.get("edit_btn").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
		}
		if(row.approveStatus != null && row.approveStatus != "3") {
			mini.get("approve_log").setEnabled(true);
		}
	});

	function onButtonEdit(){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/config/CPMiniManage.jsp",
            title: "选择客户列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cliname);
						btnEdit.setText(data.cliname);
						mini.get("partyId").setValue(party_id);
                        btnEdit.focus();
                    }
                }

            }
        });
	}

/****************************************************************************************/$
//提交审批
function commit(){
	mini.get("approve_mine_commit_btn").setEnabled(false);
	var messageid = mini.loading("系统正在处理...", "请稍后");
	try {
		verify(grid.getSelecteds());
	} catch (error) {
		
	}
	mini.hideMessageBox(messageid);	
	mini.get("approve_mine_commit_btn").setEnabled(true);
}

//提交正式审批、待审批
function verify(selections){
		if(selections.length == 0){
			mini.alert("请选中一条记录！","消息提示");
			return false;
		}else if(selections.length > 1){
			mini.alert("暂不支持多笔提交","系统提示");
			return false;
		}
		if(selections[0]["approveStatus"] != "3" ){
			var url = CommonUtil.baseWebPath() +"../../quota/operate/HandReleaseEdit.jsp?action=approve&dealNo="+selections[0].dealNo;
			var tab = {"id": "repoApprove",name:"repoApprove",url:url,title:"手工释放审批",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:selections[0]};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			Approve.approveCommit(Approve.FlowType.QuotaAdjFlow,selections[0].dealNo,Approve.OrderStatus.New,function(){query();},function(){},'9006');
		}
	};

//审批
function approve(){
		mini.get("approve_commit_btn").setEnabled(false);
		var messageid = mini.loading("系统正在处理...", "请稍后");
		try {
			verify(grid.getSelecteds());
		} catch (error) {
			
		} 
		
		mini.hideMessageBox(messageid);	
		mini.get("approve_commit_btn").setEnabled(true);
	}

//审批日志
function searchlog(){
		appLog(grid.getSelecteds());
	}

//审批日志查看
function appLog(selections){
		
		if(selections.length <= 0){
			mini.alert("请选择要操作的数据","系统提示");
			return;
		}
		if(selections.length > 1){
			mini.alert("系统不支持多笔操作","系统提示");
			return;
		}
		Approve.approveLog(Approve.FlowType.QuotaAdjFlow,selections[0].dealNo);
		
	};












</script>
</body>
</html>