function checkBoxValuechanged(e){
    search(10,0);
    var approveType=this.getValue();
    if(approveType == "mine"){//我发起的
        initButton();
    }else if(approveType == "finished"){//已审批
        mini.get("add_btn").setVisible(false);
        mini.get("edit_btn").setVisible(false);
        mini.get("delete_btn").setVisible(false);
        mini.get("approve_commit_btn").setVisible(false);
        mini.get("approve_mine_commit_btn").setVisible(false);
        mini.get("approve_log").setVisible(true);//审批日志
        mini.get("approve_log").setEnabled(true);//审批日志高亮
        /* mini.get("print_bk_btn").setVisible(true);//打印
        mini.get("print_bk_btn").setEnabled(true);//打印高亮 */
    }else{//待审批
        mini.get("add_btn").setVisible(false);
        mini.get("edit_btn").setVisible(false);
        mini.get("delete_btn").setVisible(false);
        mini.get("approve_commit_btn").setVisible(true);//审批
        mini.get("approve_commit_btn").setEnabled(true);//审批高亮
        mini.get("approve_mine_commit_btn").setVisible(false);//提交审批
        mini.get("approve_log").setVisible(true);//审批日志
        mini.get("approve_log").setEnabled(true);//审批日志高亮
       /*  mini.get("print_bk_btn").setVisible(true);//打印
        mini.get("print_bk_btn").setEnabled(true);//打印高亮 */
    }
}


function initButton(){
    mini.get("add_btn").setVisible(true);
    mini.get("add_btn").setEnabled(true);
    mini.get("edit_btn").setVisible(true);
    mini.get("edit_btn").setEnabled(true);
    mini.get("delete_btn").setVisible(true);
    mini.get("delete_btn").setEnabled(true);
    mini.get("approve_commit_btn").setVisible(false);
    mini.get("approve_mine_commit_btn").setVisible(true);//提交审批显示
    mini.get("approve_mine_commit_btn").setEnabled(true);//提交审批高亮
    mini.get("approve_log").setVisible(true);//审批日志
    mini.get("approve_log").setEnabled(true);//审批日志高亮
   /*  mini.get("print_bk_btn").setVisible(true);//打印
    mini.get("print_bk_btn").setEnabled(true);//打印高亮 */
}

