<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>手工释放维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
                <input id="state" name="state"  class="mini-hidden" value="3">
                <input id="ioper"  name="ioper" class="mini-hidden" value="<%=__sessionUser.getUserId() %>">
                <!-- <div class="centerarea">
                    <fieldset>
                    <legend>审批信息</legend>
                        <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="审批单号：" required="true"   enabled="false"    labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;保存后自动生成
                        <input id="ioper" name="ioper" class="mini-textbox" labelField="true"  label="审批发起人："   enabled="false" value='<%=__sessionUser.getUserId() %>'     labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;当前登录的客户经理
                        <input id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true"  label="审批发起机构："  enabled="false"  value='<%=__sessionInstitution.getInstName() %>' vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;客户经理所属的机构
                        <input id="inputDate" name="inputDate" class="mini-datepicker" labelField="true"  label="审批日期："   enabled="false" value='<%=__bizDate %>'   labelStyle="text-align:left;width:130px;" style="width:50%;"/><span class='tips'>&nbsp;&nbsp;发起审批单据具体时间
                    </fieldset>   
                </div> -->
                <fieldset>
                    <legend>审批信息</legend>
                        <div class="leftarea">
                            <input id="dealNo" name="dealNo" class="mini-textbox" labelField="true"  label="审批单号：" emptyText="保存后自动生成" required="true"   enabled="false"    labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                            <input id="sponInstName" name="sponInstName" class="mini-textbox" labelField="true"  label="审批发起机构："  enabled="false"  value='<%=__sessionInstitution.getInstName() %>' vtype="maxLength:10"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        </div>
                        <div class="rightarea">
                            <input id="ioper" name="ioper" class="mini-textbox" labelField="true"  label="审批发起人："   enabled="false" value='<%=__sessionUser.getUserId() %>'     labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                            <input id="inputDate" name="inputDate" class="mini-datepicker" labelField="true"  label="审批日期："   enabled="false" value='<%=__bizDate %>'   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                        </div>
                </fieldset>   
                <fieldset>
                <legend>释放要素</legend>
                <div class="leftarea">
                    <!-- <input id="dealNo" name="dealNo" class="mini-textbox"   labelField="true"  label="交易编号："  required="true"   enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />  -->
                    <input id="oldDealNo" name="oldDealNo" class="mini-textbox"   labelField="true"  label="原交易编号："  required="true"   enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                    <input id="productCode" name="productCode" class="mini-textbox"    labelField="true"  label="产品编号：" required="true"   enabled="false" style="width:100%;"  labelStyle="text-align:left;width:130px;"  />
                    <input id="custNo" name="custNo" class="mini-textbox"  labelField="true"  label="客户编号：" required="true"  enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;"/>
                    <input id="vdate" name="vdate" class="mini-datepicker" labelField="true"  label="起息日："  required="true"  enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="oldAmt" name="oldAmt" class="mini-spinner" labelField="true"  label="已占用金额(元)：" maxValue="99999999999999999" format="n2" minValue = "-999999999999999999999" required="true"  enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    
                </div>			
                <div class="rightarea">
                    <input id="institution" name="institution" class="mini-textbox" labelField="true"  label="经营单位："  value="<%=__sessionUser.getInstId() %>" required="true"    enabled="false"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    
                    <input id="productName" name="productName" class="mini-combobox" labelField="true"  label="产品名称："  required="true"   enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="custName" name="custName"  class="mini-textbox" labelField="true"  label="客户名称："  enabled="false"  style="width:100%;"  labelStyle="text-align:left;width:130px;" />
                    <input id="mdate" name="mdate" class="mini-datepicker" labelField="true"  label="到期日："   required="true"  enabled="false"  labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                    <input id="amt" name="amt" class="mini-spinner" labelField="true"  label="释放金额(元)：" maxValue="9999999999999999999" minValue = "-999999999999999999999" format="n2" enabled="false" required="true"   labelStyle="text-align:left;width:130px;" style="width:100%;"   />
                </div>			
                <div class="centerarea">
                    <input id="reason" name="reason" class="mini-textarea" labelField="true"  label="调整说明：" required="true"    vtype="maxLength:50"   labelStyle="text-align:left;width:130px;" style="width:100%;"/>
                </div>
                </fieldset>  
                <%@ include file="../../Common/Flow/MiniApproveOpCommon.jsp"%>
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div> 
</div>
<script type="text/javascript">

    mini.parse();   
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row=params.selectData;
	var url=window.location.search;
	var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field_form");

    var tradeData={};
	
	tradeData.selectData=row;
	tradeData.operType=action;
	tradeData.serial_no=row.dealNo;
	tradeData.task_id=row.taskId;
    
    $(document).ready(function(){
		if($.inArray(action,["approve","detail"])>-1){//审批、详情
			mini.get("save_btn").setVisible(false);
			form.setEnabled(false);
            var form11=new mini.Form("#approve_operate_form");
			form11.setEnabled(true);
		}

		if($.inArray(action,["edit","approve","detail"])>-1){//修改、审批、详情
            form.setData(row);
            //额度中类编号
			//mini.get("creditId").setValue(row.creditId);
			//mini.get("creditId").setText(row.creditId);
            mini.get("amt").setValue(row.oldAmt);
			
		}else{//增加 
            /* mini.get("dealNo").setValue(row.dealNo);//交易编号 */
            mini.get("oldDealNo").setValue(row.dealNo);//原交易编号
            mini.get("productCode").setValue(row.prdNo);//产品编号
            mini.get("productName").setValue(row.prdName);//产品名称
            mini.get("custNo").setValue(row.ecifNo);//客户编号
            mini.get("custName").setValue(row.cnName);//客户名称
            mini.get("vdate").setValue(row.vDate);//起息日
            mini.get("mdate").setValue(row.mDate);//到期日
            mini.get("oldAmt").setValue(row.amt);//已占用金额
            mini.get("amt").setValue(row.amt);
			
		}
    });
    
    function close(){
		top["win"].closeMenuTab();
    }
    
    function save(){
        form.validate();
		if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

        
        var useAmt = mini.get("oldAmt").getValue();
        var releaseAmt =  mini.get("amt").getValue();
        if(parseFloat(releaseAmt) > parseFloat(useAmt)) {
            mini.alert("释放金额不能大于占用金额","提示");
            return false;
        }

        var saveUrl="/CustCreditDealController/addCustCreditDeal";

        var data=form.getData(true);
        var params=mini.encode(data);

        CommonUtil.ajax({
            url:saveUrl,
            data:params,
            callback:function(data){
                mini.alert('保存成功','提示信息',function(){
                    top["win"].closeMenuTab();
                });
            }
		});

    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/quota/operate/mini/DealCreditFrozenMini.jsp",
            title: "冻结交易选择",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.dealNo);
                        //btnEdit.setText(data.cfn);
                        mini.get("custNo").setValue(data.custNo);
                        mini.get("custName").setValue(data.custName);
                        mini.get("creditId").setValue(data.creditId);
                        mini.get("creditName").setValue(data.creditName);
                        mini.get("creditAmt").setValue(data.creditAmt);
                        mini.get("quotaAvlAmt").setValue(data.quotaAvlAmt);
                        mini.get("quotaFrozenAmt").setValue(data.quotaFrozenAmt);
                        mini.get("custCreditId").setValue(data.custCreditId);
                        mini.get("dueDate").setValue(data.dueDate);
                        mini.get("term").setValue(data.term);
                        mini.get("vdate").setValue(data.vdate);
                        mini.get("mdate").setValue(data.mdate);
                        mini.get("frozenAmt").setValue(data.frozenAmt);

                        mini.get("oldIoper").setValue(data.ioper);
                        mini.get("odlSponInstName").setValue(data.sponInstName);
                        mini.get("reason").setValue(data.reason);
                        mini.get("oldInstitution").setValue(data.institution);



                        btnEdit.focus();
                    }
                }

            }
        });

	}



</script>
<script type="text/javascript" src="../../Common/Flow/MiniApproveOpCommon.js"></script>	
</body>
</html>