<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<style type="text/css">
    .myrow
    {
        background-color:#fff8f0;font-weight:bold;
    }
</style>

<title>产品额度信息</title>
</head>

<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>产品额度查询</legend>   
    <div>
        <div id="search_form" style="width:100%" cols="6">
            <input id="ecifNum" name="ecifNum" class="mini-textbox"  label="客户编号：" labelField="true"    width="280px"
                   labelStyle="text-align:right;width:120px;"  emptyText="请输入客户编号" />
            <input id="customer" name="customer" class="mini-textbox"  label="客户名称：" labelField="true"    width="280px"
                labelStyle="text-align:right;width:120px;"  emptyText="请输入客户名称" />
            <span style="float: right; margin-right: 150px"> 
                <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
                <a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
            </span>
        </div>
    </div>
</fieldset>
<span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
</span>
<div class="mini-fit" >
    <div id="credit_query_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
     allowResize="true" sortMode="client" allowAlternating="true" onshowrowdetail="onShowRowDetail">
        <div property="columns">
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div type="expandcolumn" >操作</div>
            <div field="custCreditId" headerAlign="center" align="center" width="0px">客户额度唯一编号</div>
            <div field="ecifNum" width="120px" align="center"  headerAlign="center" >客户编号</div>
            <div field="customer" width="120px" align="center" headerAlign="center" >客户名称</div>
            <div field="currency" width="120px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
            <div field="creditType" width="120px" align="center" headerAlign="center"  renderer="onTypeRenderer" >额度种类</div>
            <div field="loanAmt" width="120px" align="right" headerAlign="center" allowSort="true" numberFormat="n2">授信额度（元 ）</div>
            <div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">剩余可用金额（元 ）</div>
            <div field="frozenAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">已使用额度（元 ）</div>
            <div field="dueDate" width="120px" align="center"  headerAlign="center" >起始日</div>
            <div field="dueDateLast" width="120px" align="center"  headerAlign="center" >到期日</div>
        </div>
    </div>
</div>   


<div id="detailGrid_Form" style="display:none;">
    <div id="detail_grid" class="mini-datagrid" style="width:100%;height:250px;" allowResize="true" sortMode="client" allowAlternating="true">
        <div property="columns">
            <div field="prodCreditId" width="0px" align="center"  headerAlign="center" >产品额度唯一编号</div>
            <div field="prodName" width="120px" align="center"  headerAlign="center" >品种名称</div>
            <div field="prodType" width="120px" align="center"  headerAlign="center" >品种编号</div>
            <div field="proLoanAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信额度（元）</div>
            <div field="proAvlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用金额（元 ）</div>
            <div field="proFrozenAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">已使用金额（元 ）</div>
        </div>
    </div>    
</div>

<script>
    /**************************************基本参数设置******************************/
    mini.parse();
    var form = new mini.Form("#search_form");
    var row="";
    var credit_query_grid = mini.get("credit_query_grid");
    var detail_grid = mini.get("detail_grid");
    var detailGrid_Form = document.getElementById("detailGrid_Form");
    var jsonData = new Array();

    /**************************************点击下面显示详情开始******************************/

    function onShowRowDetail(e) {
        var grid = e.sender;
        var row = e.record;
        var td = grid.getRowDetailCellEl(row);
        td.appendChild(detailGrid_Form);
        detailGrid_Form.style.display = "block";
        searchDetail(10,0,row.custCreditId);
    }
    function searchDetail(pageSize,pageIndex,custCreditId){
        var data = {};
        data["custCreditId"]=custCreditId;
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        
        var param = mini.encode(data);

        CommonUtil.ajax( {
            url:"/HrbCreditPrdRelationController/getHrbCreditQueryPage",
            data:param,
            callback : function(data) {
                
                //设置分页
                detail_grid.setTotalCount(data.obj.total);
                detail_grid.setPageIndex(pageIndex);
                detail_grid.setPageSize(pageSize);
                //设置数据
                detail_grid.setData(data.obj.rows);
                
            }
        });
    }
    
    credit_query_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex; 
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });
    
    detail_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex; 
        var pageSize = e.data.pageSize;
        var row = credit_query_grid.getSelected();
        searchDetail(pageSize,pageIndex,row.custCreditId);
    });
    
    //清空
    function clear(){
        form.clear();
        query();
    }

    /* 查询 按钮事件 */
    function query(){
        search(credit_query_grid.pageSize,0);
    }
    /* 查询 */
    function search(pageSize,pageIndex){
        form.validate();
        if(form.isValid()==false){
            mini.alert("信息填写有误，请重新填写","系统提示");
            return;
        }

        var data=form.getData(true);
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var url="/hrbCreditCustController/getHrbCreditQueryPage";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                credit_query_grid.setTotalCount(data.obj.total);
                credit_query_grid.setPageIndex(pageIndex);
                credit_query_grid.setPageSize(pageSize);
                credit_query_grid.setData(data.obj.rows);
            }
        });
    }
    
    
    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            queryCreditType();
            query();
        });
    });

    function onTypeRenderer(e) {
        var length = jsonData.length;
        for(var i=0;i<length;i++){
            if(e.value == jsonData[i].creType){
                return jsonData[i].creTypeName;
            }
        }
    }
    
    function add(){
        var row = credit_query_grid.getSelected();
        if(row == null) {
            mini.alert('请操作选择你要添加额度的客户');
            return;
        }
        
        branchModule = '04';
        var url = CommonUtil.baseWebPath() + "/credit/config/CreditPrdRelationEdit.jsp?action=add"
        var tab = { id: "prodCreditAdd", name: "prodCreditAdd", title: "产品额度新增",
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:row};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
    
    function modify() {
        var row = credit_query_grid.getSelected();
        var derow = detail_grid.getSelected();
        if(row == null || derow == null) {
            mini.alert('请操作选择你要修改客户向下的额度');
        } else {
            branchModule = '04';
            prodCreditId = derow.prodCreditId;
            var url = CommonUtil.baseWebPath() + "/credit/config/CreditPrdRelationEdit.jsp?action=edit&prodCreditId="+prodCreditId;
            var tab = { id: "prodCreditUpdate", name: "prodCreditUpdate", title: "产品额度修改",
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:row,selectDeData:derow};
            CommonUtil.openNewMenuTab(tab,paramData);
        }
    }
    function del() {
        var creselections = credit_query_grid.getSelecteds();
        var deselections = detail_grid.getSelecteds();
        if(creselections.length>1 || deselections.length>1){
            mini.alert("系统不支持多条数据进行删除","消息提示");
            return;
        }
        var row = detail_grid.getSelected();
        if(row == null) {
            mini.alert('请操作选择你要删除客户向下的额度',"消息提示");
            return;
        } else {
            var searchUrl="/HrbCreditPrdRelationController/deleteInfo";
            CommonUtil.ajax({
                url:searchUrl,
                data:{'prodCreditId':row.prodCreditId},
                callback:function(data){
                    mini.alert('操作成功');
                    query();
                }
            });
        }
    }
    //查询额度种类配置
    function queryCreditType(){
        CommonUtil.ajax({
            url:"/HrbCreditTypeController/queryAllCrediyType",
            data:{},
            callback:function(data){
                var length = data.length;
                for(var i=0;i<length;i++){
                    var creType = data[i].creType;
                    var creTypeName = data[i].creTypeName;
                    jsonData.add({'creType':creType,'creTypeName':creTypeName});
                };
            }
        });
    }

</script>
</body>
</html>
