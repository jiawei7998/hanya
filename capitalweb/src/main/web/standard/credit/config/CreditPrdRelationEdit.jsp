<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>产品额度信息</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="100%" showCollapseButton="false">
		<fieldset class="mini-fieldset title">
		    <legend>产品额度信息</legend>
		</fieldset>
		<div id="field_form" class="mini-fit area"  style="background:white">
		    <div class="mini-panel" title="客户额度信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
                <div class="leftarea">
                    <input id="creditType" name="creditType" class="mini-combobox mini-mustFill" labelField="true" label="额度种类" requiredErrorText="该输入项为必输项" required="true"
                         labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" textField="creTypeName" valueField="creType" />
                    <input id="ecifNum" name="ecifNum" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" label="客户编号" 
                        labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" requiredErrorText="该输入项为必输项" required="true" />
                    <input id="dueDate" name="dueDate" class="mini-datepicker" labelField="true" label="授信起始日期" onvaluechanged="dateCheck"
                        labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" requiredErrorText="该输入项为必输项" required="true" />
                    <input id="loanAmt" name="loanAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                        changeOnMousewheel="false" format="n2" labelField="true" label="额度总金额" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                        requiredErrorText="该项为必录项" required="true"  enabled="false" onvaluechanged="setAvlAmt" />
                    <input id="avlAmt" name="avlAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                        changeOnMousewheel="false" format="n2" labelField="true" label="剩余额度" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                        requiredErrorText="该项为必录项" requited="true" enabled="false"/>
                </div>
                <div class="rightarea">
                    <input id="currency" name="currency" class="mini-combobox mini-mustFill" labelField="true"  label="币种" requiredErrorText="该输入项为必输项"
                        required="true"   labelStyle="text-align:left;width:130px;" enabled="false" data="CommonUtil.serverData.dictionary.Currency" style="width:100%;"/>
                    <input id="customer" name="customer" class="mini-textbox mini-mustFill" labelField="true" label="客户名称" requiredErrorText="该输入项为必输项"  requited="true"
                        labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false"/>
                    <input id="dueDateLast" name="dueDateLast" class="mini-datepicker mini-mustFill" labelField="true" label="授信结束日期" labelStyle="text-align:left;width:130px;"
                        style="width:100%;" onvaluechanged="dateCheck" requiredErrorText="该输入项为必输项" required="true"  enabled="false"/>
                    <input id="frozenAmt" name="frozenAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                        changeOnMousewheel="false" format="n2" labelField="true" label="已用额度" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                        requiredErrorText="该项为必录项" required="true"  enabled="false" onvaluechanged="setAvlAmt" />
                    <input id="custCreditId" name="custCreditId" class="mini-textbox mini-mustFill" labelField="true" label="客户额度唯一编号" 
                        labelStyle="text-align:left;width:0px;" style="width:0%;" enabled="false" />
                </div>
            </div>
            
            <div class="mini-panel" title="产品额度信息" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
                <div id="search_detail_form">
                    <div class="leftarea">
                        <input id="prodType" name="prodType" class="mini-buttonedit mini-mustFill" labelField="true" label="产品编号" requiredErrorText="该输入项为必输项" 
                            required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" onbuttonclick="onButtonEditProd"/>

                        <input id="proLoanAmt" name="proLoanAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                            changeOnMousewheel="false" format="n2" labelField="true" label="额度总金额" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                            requiredErrorText="该项为必录项" required="true"  onvaluechanged="setAvlAmt" />
                        <input id="proAvlAmt" name="proAvlAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999"
                               changeOnMousewheel="false" format="n2" labelField="true" enabled="false" label="剩余额度" labelStyle="text-align:left;width:130px;" style="width:100%;"
                               requiredErrorText="该项为必录项" requited="true"/>
                    </div>
                    <div class="rightarea">
                        <input id="prodName" name="prodName" class="mini-textbox mini-mustFill" labelField="true" label="产品种类" requiredErrorText="该输入项为必输项"
                               required="true"  labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false">
                        <input id="proFrozenAmt" name="proFrozenAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                            changeOnMousewheel="false" format="n2" labelField="true" label="已用额度" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                            requiredErrorText="该项为必录项" required="true"  onvaluechanged="setAvlAmt" enabled="false" />

                    </div>
                </div>
                <div class="centerarea" style="margin-top:5px;">
                    <div class="mini-toolbar" style="padding:0px;border-bottom:0;" id="toolbar">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:100%;">
                                    <a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">添加</a>
                                    <a  id="edit_btn" class="mini-button" style="display: none"   onclick="add()">修改</a>
                                    <!-- <a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a> -->
                                    <!-- <a  id="saves_btn" class="mini-button" style="display: none"    onclick="saves()" enabled="false" >保存</a> -->
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="centerarea" style="height:350px;">
                    <div class="mini-fit" >
                        <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
                            fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true" onselectionchanged="onSelectionChanged" showpager="false">
                            <div property="columns">
                                <div field="prodCreditId" width="0px" align="center"  headerAlign="center" >产品额度唯一编号</div>
                                <div field="prodType" width="120px" align="center"  headerAlign="center" >品种编号</div>
                                <div field="prodName" width="120px" align="center"  headerAlign="center" >品种名称</div>
                                <div field="proLoanAmt" width="120px" align="right"  headerAlign="center" allowSort="true" 
                                    numberFormat="n2" onvaluechanged="setAvlAmt">授信额度（元）</div>
                                <div field="proAvlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用额度（元 ）</div>
                                <div field="proFrozenAmt" width="120px" align="right"  headerAlign="center" allowSort="true" 
                                    numberFormat="n2" onvaluechanged="setAvlAmt">已用额度（元 ）</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
    <!-- 
	<div id="functionIds"  style="padding-top:50px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div>
    -->
</div>
<script type="text/javascript">
/**************************************基本参数设置******************************/
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    var derow=params.selectDeData;
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");
    var prodCreditId=CommonUtil.getParam(url,"prodCreditId");
    var form=new mini.Form("#field_form");
    var detailForm = new mini.Form("#search_detail_form");
    var grid=mini.get("datagrid");
    
    /**************************************初始化******************************/
    $(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            queryCreditType();
            initData();
            searchDetail(10, 0);
            setChinese();
        });
    });
    
    //初始化数据
    function initData(){
        if($.inArray(action,["detail"])>-1){
//      mini.get("save_btn").setVisible(false);
    	    form.setEnabled(false);
    	}
        if($.inArray(action,["add","edit","detail"])>-1){
            form.setData(row);
            mini.get("ecifNum").setText(row.ecifNum);
    	}
        if($.inArray(action,["add","detail"])>-1){
            mini.get("edit_btn").setVisible(false);
        }
    	if($.inArray(action,["edit","detail"])>-1){
            mini.get("add_btn").setVisible(false);
            detailForm.setData(derow);
            mini.get("prodType").setText(derow.prodType);
            mini.get("prodType").setValue(derow.prodType);
        }
    }
    /**************************************按钮方法******************************/
    //添加
    function add() {
        form.validate();
        if(form.isValid() == false) {
            mini.alert("表单填写错误,请确认!"," 提示信息");
            return;
        }

        var proLoanAmt = mini.get("proLoanAmt").getValue();
        if(Number(proLoanAmt) <= 0) {
            mini.alert("额度总金额必须大于0"," 提示信息");
            return;
        }
        
        if(isOverLimit(grid.getData())) {
            return;
        }
        
        var data=detailForm.getData(true);
        if(action == 'edit') {
            //只有修改将prodCreditId传到后台
            data['prodCreditId'] = prodCreditId;
        }
        data['custCreditId'] = mini.get("custCreditId").getValue();
        data['custNum'] = mini.get("ecifNum").getValue();
        data['prodName'] = mini.get("prodName").getValue();

        var param=mini.encode(data);
        CommonUtil.ajax( {
            url:"/HrbCreditPrdRelationController/saveInfo",
            data:param,
            callback : function(data) {
                
                if(data.code == 'error.common.0000') {
                    if(action == 'edit') {
                        del();
                    }
                    addrow();
                    mini.alert(data.desc,"提示信息"
                    );//,function(){ top["win"].closeMenuTab();}
                }else {
                    mini.alert(data.desc,"提示信息");
                }
            }
        });
    }
    
    /**************************************方法实现******************************/
    //查询产品额度明细
    function searchDetail(pageSize,pageIndex){
        var data = {};
        data["custCreditId"]=mini.get("custCreditId").getValue();
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        var param = mini.encode(data);

        CommonUtil.ajax({
            url:"/HrbCreditPrdRelationController/getHrbCreditQueryList",
            data:param,
            callback : function(data) {
                
                grid.setData(data.obj);
            }
        });
    }
    
    //产品额度显示添加行
    function addrow() {
        var detailData = detailForm.getData(true);
        
        var row = {
                prodType:detailData.prodType,
                prodName:detailData.prodName,
                proLoanAmt:detailData.proLoanAmt,
                proAvlAmt:detailData.proAvlAmt,
                proFrozenAmt:detailData.proFrozenAmt
        };
        grid.addRow(row);
        detailForm.clear();
    }
    
    //
    function del() {
        
        var detailData = detailForm.getData(true);
        var row = grid.findRow(function(row){
            if(row.prodType == detailData.prodType) return true;
        });
        if(!row){
            mini.alert("请选中要修改的一行","提示");
            return;
        }
        grid.removeRow(row);
    }
    
    //生成剩余额度
    function setAvlAmt() {
        var loanAmt = mini.get("proLoanAmt").getValue();//总额度
        var usedAmt = mini.get("proFrozenAmt").getValue();//已用额度
        if(loanAmt < usedAmt) {
            mini.alert("已用额度不能超过总额度","提示信息");
            mini.get("proFrozenAmt").setValue('');
            usedAmt = 0.00;
        }
        var avlAmt = CommonUtil.accSubtr(loanAmt,usedAmt);//剩余额度
        mini.get("proAvlAmt").setValue(avlAmt);
        setChinese();
    }
    
    //计算修改后是否超过总额度
    function isOverLimit (rows){
        var LoanAmt = mini.get("loanAmt").getValue();
        var prodType = mini.get("prodType").getValue();
        var proLoanAmt = mini.get("proLoanAmt").getValue();
        var proLoanAmtSum = 0;
        
        for(var i = 0; i < rows.length ; i++) {
            //正在修改的行不计算
            if(prodType == rows[i].prodType) {
                continue;
            }
            proLoanAmtSum = proLoanAmtSum + parseFloat(rows[i].proLoanAmt);
        }
        //加上修改/添加的金额
        proLoanAmtSum = proLoanAmtSum + parseFloat(proLoanAmt);
        
        if(proLoanAmtSum > LoanAmt){
            mini.alert("产品额度总和不能超过总授信额度","提示信息");
            return true;
        }
        return false;
    }
    
    function onSelectionChanged(e) {
        var selgrid = e.sender;
        var record = selgrid.getSelected();
        if (record) {
            mini.get("prodType").setValue(record.prodType);
            mini.get("prodType").setText(record.prodType);
            mini.get("prodName").setValue(record.prodName);
            mini.get("proLoanAmt").setValue(record.proLoanAmt);
            mini.get("proFrozenAmt").setValue(record.proFrozenAmt);
            mini.get("proAvlAmt").setValue(record.proAvlAmt);
            prodCreditId = record.prodCreditId;
        }
        setChinese();
    }
    
    //生成中文金额
    function setChinese() {
        var fields = ['loanAmt','frozenAmt','avlAmt','proLoanAmt','proAvlAmt','proFrozenAmt'];
        for(element of fields) {
            doSetChinese(element);
        }
    }
    
    function doSetChinese(field) {
        var fieldMIni = mini.get(field);
        var amt =  CommonUtil.chineseNumber(fieldMIni.getValue());
        fieldMIni.setText(fieldMIni.getValue() + '      ' + amt);
    }
    
    
    function onButtonEditProd(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/operate/mini/CreditProductDialogMini.jsp",
            title: "查询产品",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.productCode);
                        btnEdit.setText(data.productCode);
                        mini.get("prodName").setValue(data.productName);
                        // searchProductWeight(10,0,data.productCode);
                        btnEdit.focus();
                    }
                }

            }
        });
    }

    //查询额度种类配置
    function queryCreditType(){
        CommonUtil.ajax({
            url:"/HrbCreditTypeController/queryAllCrediyType",
            data:{},
            callback:function(data){
                var length = data.length;
                var jsonData = new Array();
                for(var i=0;i<length;i++){
                    var creType = data[i].creType;
                    var creTypeName = data[i].creTypeName;
                    jsonData.add({'creType':creType,'creTypeName':creTypeName});
                };
                mini.get("creditType").setData(jsonData);
            }
        });
    }
    
</script>
</body>
</html>