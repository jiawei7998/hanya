<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>


<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>额度CRMS中类品种新增</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;" id="field" >
	<div size="90%" showCollapseButton="false">
		<fieldset class="mini-fieldset title">
		<legend>额度CRMS中类品种新增</legend>
		</fieldset>
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <div class="leftarea">
                <input id="creType" name="creType"  labelStyle="text-align:right;"class="mini-textbox" width="550px" labelField="true"  label="额度代码："    />
			 	<!-- <input id="riskLevel" name="riskLevel"  labelStyle="text-align:right;"class="mini-combobox" width="550px" labelField="true"  
	                   		label="风险等级："  value="1" required="true"   data="CommonUtil.serverData.dictionary.riskLevel" /> -->
			</div>			
			<div class="rightarea">
               <input id="creTypeName" name="creTypeName"  labelStyle="text-align:right;"class="mini-textbox" width="550px" labelField="true" label="额度名称："   required="true"   />
			</div>			
            </fieldset>  
		</div>
		
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div> 
</div>

<script type="text/javascript">

    mini.parse();

    
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field");
    //mini.get("riskLevel").setVisible(false);
    
    $(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if ($.inArray(action, ["approve", "detail"]) > -1) {
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
            }

            if ($.inArray(action, ["edit", "approve", "detail"]) > -1) {
                form.setData(row);
                //额度中类编号
                mini.get("creType").setValue(row.creType);
                mini.get("creType").setText(row.creType);
                mini.get("creType").setEnabled(false);
            }
        });
    });
    
    
    function save(){
        form.validate();
    	if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写","系统提示");
    		return;
    	}
    
        var saveUrl="";
        if(action == "add"){
            saveUrl="/HrbCreditTypeController/insertCrediyType";
        }else if(action == "edit"){
            saveUrl="/HrbCreditTypeController/updateCrediyType";
        }
    
        var data=form.getData(true);
        var params=mini.encode(data);
        CommonUtil.ajax({
            url:saveUrl,
            data:params,
            callback:function(data){
                mini.alert('保存成功','提示信息',function(){
                    top["win"].closeMenuTab();
                });
            }
    	});
    
    }
    
    
    //取消
    function cancel(){
        top["win"].closeMenuTab();
    }
    
    
    //关闭
    function close(){
        top["win"].closeMenuTab();
    }


</script>
</body>
</html>