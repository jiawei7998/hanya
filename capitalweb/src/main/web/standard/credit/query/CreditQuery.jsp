<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<style type="text/css">
	.myrow
    {
        background-color:#fff8f0;font-weight:bold;
    }
</style>

<title>额度信息列表</title>
</head>

<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>额度查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="customer" name="customer" class="mini-textbox"  label="客户名称：" labelField="true"    width="280px" labelStyle="text-align:right;width:120px;"  emptyText="请输入客户名称" />
			<input id="ecifNum" name="ecifNum" class="mini-textbox"  label="客户编号：" labelField="true"    width="280px" labelStyle="text-align:right;width:120px;"  emptyText="请输入客户编号" />
			<span style="float: right; margin-right: 150px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a id="expt_btn" class="mini-button"  style="display: none"  onclick="exportData()">导出</a>
			</span>
		</div>
	</div>
</fieldset>
<span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del();">删除</a>
        <a class="mini-button" style="display: none"  id="exp_btn"  onClick="exp();">导出</a>
    </span>
<div class="mini-fit" >
	<div id="credit_query_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true">
		<!-- onshowrowdetail="onShowRowDetail" -->
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="custCreditId" headerAlign="center" align="center" width="0px">客户额度唯一编号</div>
			<div field="ecifNum" width="120px" align="center"  headerAlign="center" >客户编号</div>
			<div field="customer" width="120px" align="center" headerAlign="center" >客户名称</div>
			<div field="currency" width="120px" align="center" headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'Currency'}">币种</div>
			<div field="creditType" width="120px" align="center" headerAlign="center"  renderer="onTypeRenderer" >额度种类</div>
			<div field="loanAmt" width="120px" align="right" headerAlign="center" allowSort="true" numberFormat="n2">授信额度（元 ）</div>
			<div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">剩余可用金额（元 ）</div>
			<div field="frozenAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">已使用额度（元 ）</div>
			<div field="dueDate" width="120px" align="center"  headerAlign="center" >起始日</div>
			<div field="dueDateLast" width="120px" align="center"  headerAlign="center" >到期日</div>
		</div>
	</div>
</div>   


<div id="detailGrid_Form" style="display:none;">
    <div id="detail_grid" class="mini-datagrid" style="width:80%;height:250px;" allowResize="true" sortMode="client" allowAlternating="true">
        <div property="columns">
			<div field="productType" width="120px" align="center"  headerAlign="center" >品种编号</div>
			<div field="creditName" width="120px" align="center"  headerAlign="center" >额度品种</div>
			<div field="loanAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信额度（元）</div>
			<div field="avlAmt" width="120px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">可用金额（元 ）</div>
			<div field="termName" width="120px" align="center"  headerAlign="center" >业务期限</div>
        </div>
    </div>    
</div>

<script>
	/**************************************基本参数设置******************************/
	mini.parse();

	var form = new mini.Form("#search_form");
	var row="";
	var credit_query_grid = mini.get("credit_query_grid");
	var jsonData = new Array();

	/**************************************点击下面显示详情开始******************************/
	credit_query_grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	//清空
	function clear(){
		form.clear();
		query();
	}

	/* 查询 按钮事件 */
	function query(){
		search(credit_query_grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var url="/hrbCreditCustController/getHrbCreditQueryPage";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				credit_query_grid.setTotalCount(data.obj.total);
				credit_query_grid.setPageIndex(pageIndex);
				credit_query_grid.setPageSize(pageSize);
				credit_query_grid.setData(data.obj.rows);
			}
		});
	}

	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			queryCreditType();
			query();
		});
	});

	function onTypeRenderer(e) {
		var length = jsonData.length;
		for(var i=0;i<length;i++){
			if(e.value == jsonData[i].creType){
				return jsonData[i].creTypeName;
			}
		}
	}

	function add(){
        branchModule = '04';
        var url = CommonUtil.baseWebPath() + "/credit/query/CreditQueryEdit.jsp?action=add&limit=1"
        var tab = { id: "creditAdd", name: "creditAdd", title: "客户额度新增",
        url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }
	
	function modify() {
        var row = credit_query_grid.getSelected();
        if(row == null) {
            mini.alert('请选择要修改的内容');
        } else {
            branchModule = '04';
            custCreditId = row.custCreditId;
            var url = CommonUtil.baseWebPath() + "/credit/query/CreditQueryEdit.jsp?action=modify&custCreditId="+custCreditId+"&limit=1";
            var tab = { id: "creditUpdate", name: "creditUpdate", title: "客户额度修改",
            url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
            var paramData = {selectData:""};
            CommonUtil.openNewMenuTab(tab,paramData);
        }
    }
    function del() {
        var selections = credit_query_grid.getSelecteds();
        if(selections.length>1){
            mini.alert("系统不支持多条数据进行删除","消息提示");
            return;
        }
        var row = credit_query_grid.getSelected();
        if(row == null) {
            mini.alert('请选择要删除的内容',"消息提示");
            return;
        } else {
			mini.confirm("您确认要删除选中记录?","系统警告",function(value){
				if (value=='ok'){
					var searchUrl="/hrbCreditCustController/deleteCreditCust";
					CommonUtil.ajax({
						url:searchUrl,
						data:{'custCreditId':row.custCreditId},
						callback:function(data){
							mini.alert('操作成功');
							query();
						}
					});
				}
			});
        }
    }

	//查询额度种类配置
	function queryCreditType(){
		CommonUtil.ajax({
			url:"/HrbCreditTypeController/queryAllCrediyType",
			data:{},
			callback:function(data){
				var length = data.length;
				for(var i=0;i<length;i++){
					var creType = data[i].creType;
					var creTypeName = data[i].creTypeName;
					jsonData.add({'creType':creType,'creTypeName':creTypeName});
				};
			}
		});
	}



	//导出
	function exportData() {
		if (credit_query_grid.totalCount == "0") {
			mini.alert("表中无数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?", "系统提示",
				function (action) {
					if (action == "ok") {
						var form = new mini.Form("#search_form");
						//var data = form.getData(true);
						var data = mini.encode(form.getData());
						var batchTime = JSON.parse(data).batchTime; //成交单编号
						var data = {batchTime: batchTime};
						var fields = null;
						for (var id in data) {
							fields += '<input type="hidden" id="' + id + '" name="' + id + '" value="' + data[id] + '">';
						}
						fields += "<input type='hidden' id='excelName' name='excelName' value='" + "creditcust.xls" + "'>";
						fields += "<input type='hidden' id='id' name='id' value='" + "creditcust" + "'>";
						var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
						$('<form action="' + urls + '" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
					}
				}
		);

	}
</script>
</body>
</html>
