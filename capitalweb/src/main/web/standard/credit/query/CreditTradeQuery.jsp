<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/excelExport.js"></script>

	<title>交易使用额度情况信息列表</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>交易使用额度情况查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="ecifNo" name="ecifNo" class="mini-textbox" labelField="true" label="客户编号：" labelStyle="text-align:right;" emptyText="请输入客户编号" />
			<!-- <input id="ecifNo" name="ecifNo" class="mini-buttonedit" onbuttonclick="onButtonEdit"  label="客户名称：" labelField="true"    width="280px" labelStyle="text-align:right;"  emptyText="请输入客户名称" /> -->
			<input id="prdNo" name="prdNo" class="mini-buttonedit" labelField="true" label="额度品种：" labelStyle="text-align:right;" emptyText="请输入业务品种" onbuttonclick="onButtonEditProd" />
			<input id="dealNo" name="dealNo" class="mini-textbox"  label="审批单号：" labelField="true"    width="280px" labelStyle="text-align:right;"  emptyText="请输入审批单号" />
<%--			<input id="batchTime" name="batchTime" class="mini-datepicker" labelField="true" label="查询日期："--%>
<%--							labelStyle="text-align:right;" emptyText="请选择起息日期" value = '<%=__bizDate %>'/>--%>
							<!-- value = "new Date()" -->
			<span style="float: right; margin-right: 100px">
				<a id="search_btn" class="mini-button"  style="display: none"  onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button"  style="display: none" onclick="clear()">清空</a>
				<a id="expt_btn" class="mini-button" style="display: none"  onclick="exportData()">导出</a>
			</span>
		</div>
	</div>
</fieldset>



<div class="mini-splitter" vertical="true" handlerSize="2px" style="width:100%;height:92%;" allowResize="false">
    <div size="100%">
        <div class="mini-fit" style="width:100%;height:40%;" >
<%--            <div class="mini-panel" title="交易额度使用总览" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true">--%>
                <!-- <div id="credit_query_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" onselectionchanged="onSelectionChangedSearchDetail"
                    idField="id" pageSize="20" multiSelect="false" sortMode="client" allowAlternating="true"> -->
                 <div id="credit_query_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true"  idField="id" pageSize="20"
					  multiSelect="false" sortMode="client" allowAlternating="true" sizeList="[10,20,50,100,-1]">
                    <div property="columns">
                        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
<%--                        <div field="custCreditId" width="120px" align="center" headerAlign="center" >额度编号</div>--%>
						<div field="dealNo" width="200px" align="left"  headerAlign="center" >审批单号</div>
						<div field="dealType" width="100px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'creditType'}">类型</div>
						<div field="prdNo" width="100px" align="center"  headerAlign="center" >额度品种</div>
						<div field="creditId" width="100px" align="center"  headerAlign="center" renderer="onTypeRenderer" >额度类型</div>
                        <div field="ecifNo" width="100px" align="center"  headerAlign="center" >客户编号</div>
						<div field="cnName" width="200px" align="center"  headerAlign="center" >客户名称</div>
						<div field="cfn" width="200px" align="center"  headerAlign="center" >客户全称</div>
                        <div field="loanAmtEcif" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">客户授信额度(元)</div>
                        <div field="avlAmtEcif" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">客户授信可用额度(元)</div>
<%--                        <div field="edOpType" width="120px" align="center"  headerAlign="center" >额度操作类型</div>--%>
                        <div field="loanAmtCredit" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信品种额度(元)</div>
                        <div field="avlAmtCredit" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">授信品种可用额度(元)</div>
						<div field="edOpAmt" width="140px" align="right"  headerAlign="center" allowSort="true" numberFormat="n2">使用金额SUM(元)</div>
						<div field="vDate" width="120px" align="center"  headerAlign="center" >占用日</div>
						<div field="mDate" width="120px" align="center"  headerAlign="center" >到期日</div>
					</div>
                </div>
<%--            </div>--%>
        </div>
    </div>
</div>


<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var row="";

	var grid =mini.get("credit_query_grid");
    grid.on("beforeload",function(e){
        e.cancel = true;
        var pageIndex = e.data.pageIndex; 
        var pageSize = e.data.pageSize;
        search(pageSize,pageIndex);
    });

	var jsonData = new Array();

    // var quota_detail_grid =mini.get("quota_detail_grid");
    // quota_detail_grid.on("beforeload",function(e){
    //      e.cancel = true;
    //      var pageIndex = e.data.pageIndex;
    //      var pageSize = e.data.pageSize;
    //      var row = grid.getSelected();
    //      searchDetail(pageSize,pageIndex,row.dealNo,row.batchTId);
    //  });
	
	
	//清空
	function clear(){
		form.clear();
		query();
	}

	/* 查询 按钮事件 */
	function query(){
		search(grid.pageSize,0);
	}
	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		//var url="/edCustBatchStatusController/getTdCustBatchStatusPage";
		var url="/edCustBatchStatusController/selectEdCustLogForDealNoOrPrdNo";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}

	function onTypeRenderer(e) {
		var length = jsonData.length;
		for(var i=0;i<length;i++){
			if(e.value == jsonData[i].creType){
				return jsonData[i].creTypeName;
			}
		}
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			queryCreditType();
			query();
		});
	});

	function onButtonEdit(){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../quota/config/CPMiniManage.jsp",
            title: "选择客户列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cliname);
						btnEdit.setText(data.cliname);
						mini.get("partyId").setValue(party_id);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
	
	//导出
	function exportData() {
		var data=grid.getData();
		console.log(data);
		if(data.totalCount=="0"){
			mini.alert("表中无数据");
			return;
		}
        //前台直接导出
		downloadExcel(ExportTable(grid.getData(true),grid.columns),"总交易额度查询");
		// mini.confirm("您确认要导出Excel吗?","系统提示",
		// 	function (action) {
		// 		if (action == "ok"){
		// 	 		var form = new mini.Form("#search_form");
		// 			//var data = form.getData(true);
		// 			var data = mini.encode(form.getData());
		// 			var batchTime = JSON.parse(data).batchTime; //成交单编号
		// 			var data = {batchTime:batchTime};
		// 			var fields = null;
		// 			for(var id in data){
		// 				fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
		// 			}
		// 			fields += "<input type='hidden' id='excelName' name='excelName' value='"+"tradequery.xls"+"'>";
		// 			fields += "<input type='hidden' id='id' name='id' value='"+"tradequery"+"'>";
		// 			var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcel";
		// 			$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
		// 		}
		// 	}
		// );
	}

	//额度品种查询
	function onButtonEditProd(e) {
		var btnEdit = this;
		mini.open({
			url: CommonUtil.baseWebPath() + "../../quota/operate/mini/CreditProductDialogMini.jsp",
			title: "查询产品",
			width: 700,
			height: 600,
			ondestroy: function (action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data);    //必须
					if (data) {
						btnEdit.setValue(data.productName);
						btnEdit.setText(data.productName);
						btnEdit.focus();
					}
				}

			}
		});
	}

	//查询额度种类配置
	function queryCreditType(){
		CommonUtil.ajax({
			url:"/HrbCreditTypeController/queryAllCrediyType",
			data:{},
			callback:function(data){
				var length = data.length;
				for(var i=0;i<length;i++){
					var creType = data[i].creType;
					var creTypeName = data[i].creTypeName;
					jsonData.add({'creType':creType,'creTypeName':creTypeName});
				};
			}
		});
	}
	
</script>
</body>
</html>
