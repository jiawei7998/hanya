<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" >
    var prdCode="CCREDIT";
</script>
<title>客户额度管理</title>
</head>
<body style="width: 100%; height: 100%; background: white">
    <div id="cust_credit_form" class="mini-panel" title="客户额度信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
        <div class="leftarea">
            <input id="creditType" name="creditType" class="mini-combobox mini-mustFill" labelField="true" label="额度种类" requiredErrorText="该输入项为必输项" required="true"
                 labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" textField="creTypeName" valueField="creType" />
            <input id="ecifNum" name="ecifNum" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" label="客户编号" 
                labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" requiredErrorText="该输入项为必输项" required="true" />
            <input id="dueDate" name="dueDate" class="mini-datepicker" labelField="true" label="授信起始日期" onvaluechanged="dateCheck"
                labelStyle="text-align:left;width:130px;" style="width:100%;" requiredErrorText="该输入项为必输项" required="true" />
            <input id="loanAmt" name="loanAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                changeOnMousewheel="false" format="n2" labelField="true" label="额度总金额" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                requiredErrorText="该项为必录项" required="true"  onvaluechanged="setAvlAmt" />
            <input id="avlAmt" name="avlAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                changeOnMousewheel="false" format="n2" labelField="true" label="剩余额度" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                requiredErrorText="该项为必录项" requited="true" enabled="false"/>
        </div>
        <div class="rightarea">
            <input id="currency" name="currency" class="mini-combobox mini-mustFill" labelField="true"  label="币种" requiredErrorText="该输入项为必输项"
                required="true"   labelStyle="text-align:left;width:130px;"  data="CommonUtil.serverData.dictionary.Currency" value="CNY" style="width:100%;"/>
            <input id="customer" name="customer" class="mini-textbox mini-mustFill" labelField="true" label="客户名称" requiredErrorText="该输入项为必输项"  requited="true"
                labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false"/>
            <input id="dueDateLast" name="dueDateLast" class="mini-datepicker mini-mustFill" labelField="true" label="授信结束日期" labelStyle="text-align:left;width:130px;" 
                style="width:100%;" onvaluechanged="dateCheck" requiredErrorText="该输入项为必输项" required="true" />
            <input id="frozenAmt" name="frozenAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
                changeOnMousewheel="false" format="n2" labelField="true" label="已用额度" labelStyle="text-align:left;width:130px;" style="width:100%;" 
                requiredErrorText="该项为必录项" required="true"  onvaluechanged="setAvlAmt" />
        </div>
    </div>
    <br/>
    <div>
        <div style="margin-bottom:10px;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a></div>
    </div>
    
    <script>
        mini.parse();

        var form = new mini.Form("cust_credit_form");
        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");
        var custCreditId = CommonUtil.getParam(url,"custCreditId");
        
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
                queryCreditType();
                init();
            });
        });
        
        function init() {
            if(action == 'modify') {
                var url = "/hrbCreditCustController/getHrbCreditQueryPage";
                CommonUtil.ajax({
                    url:url ,
                    data:{'custCreditId':custCreditId} ,
                    callback:function(data){
                        var bean = data.obj.rows[0];
                        mini.get("creditType").setValue(bean.creditType);
                        mini.get("currency").setValue(bean.currency);
                        mini.get("ecifNum").setValue(bean.ecifNum);
                        mini.get("ecifNum").setText(bean.ecifNum);
                        mini.get("dueDate").setValue(bean.dueDate);
                        mini.get("loanAmt").setValue(bean.loanAmt);
                        mini.get("customer").setValue(bean.customer);
                        mini.get("dueDateLast").setValue(bean.dueDateLast);
                        mini.get("frozenAmt").setValue(bean.frozenAmt);
                        mini.get("avlAmt").setValue(bean.avlAmt);
                        setChinese();
                    }
                });
            } else if(action == "add") {
                mini.get("creditType").setEnabled(true);
                mini.get("ecifNum").setEnabled(true);
                // mini.get("currency").setEnabled(true);
                var date = new Date();
                mini.get("dueDate").setValue(date);
            }
        }
        
        function save() {
            
            form.validate();
            if(form.isValid() == false) {
                mini.alert("表单填写错误,请确认!"," 提示信息");
                return;
            }
            
            var loanAmt = mini.get("loanAmt").getValue();
            if(Number(loanAmt) <= 0) {
                mini.alert("额度总金额必须大于0"," 提示信息");
                return;
            }
            
            var saveUrl="/hrbCreditCustController/insertAndUpdate";
            var data=form.getData(true);
            
            if(action == 'modify') {
                data['custCreditId'] = custCreditId;
            }
            
            var params=mini.encode(data);
            CommonUtil.ajax({
                url:saveUrl,
                data:params,
                callback:function(data){
                    if(data.code == 'error.common.0000') {
                        mini.get("save_btn").setEnabled(false);
                        mini.alert(data.desc,"提示信息",function(){
                            top["win"].closeMenuTab();
                        });
                    }
                },
                callerror:function(data){
//                  mini.alert(data.desc,"提示信息");
                }
            });
        }
        
        function onButtonEdit() {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "../../credit/limit/LimitCustMini.jsp?",
                title: "选择客户列表",
                width: 900,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);
                        if (data) {
                            mini.get("customer").setValue(data.cliname);
                            btnEdit.setValue(data.cno);
                            btnEdit.setText(data.cno);
                            btnEdit.focus();
                        }
                    }
                }
            });
        }
        
        //额度判断
        function setAvlAmt(e) {
            
            var loanAmt = mini.get("loanAmt").getValue();//总额度
            var usedAmt = mini.get("frozenAmt").getValue();//已用额度
            var avlAmt = mini.get("avlAmt").getValue();//剩余额度

            if(loanAmt < usedAmt) {
                mini.alert("已用额度不能超过总额度","提示信息");
                usedAmt = CommonUtil.accSubtr(loanAmt,avlAmt);
                mini.get("frozenAmt").setValue(usedAmt);

            }
            
            avlAmt = CommonUtil.accSubtr(loanAmt,usedAmt);//剩余额度
            mini.get("avlAmt").setValue(avlAmt);
            
            setChinese();
        }
        
        //生成中文金额
        function setChinese() {
            var fields = ['loanAmt','frozenAmt','avlAmt'];
            for(element of fields) {
                doSetChinese(element);
            }
        }
        
        function doSetChinese(field) {
            var fieldMIni = mini.get(field);
            var amt =  CommonUtil.chineseNumber(fieldMIni.getValue());
            fieldMIni.setText(fieldMIni.getValue() + '      ' + amt);
        }
        
        //起期和止期校验
        function dateCheck() {
            var startDate = mini.get("dueDate").getValue();
            var endDate = mini.get("dueDateLast").getValue();
            if(CommonUtil.isNull(startDate) || CommonUtil.isNull(endDate)) {
                return;
            }
            if(startDate > endDate) {
                mini.alert("结束日期应在起始日期之后","提示信息");
                mini.get("dueDateLast").setValue('');
            }
        }
        
        //查询额度种类配置
        function queryCreditType(){
            CommonUtil.ajax({
                url:"/HrbCreditTypeController/queryAllCrediyType",
                data:{},
                callback:function(data){
                    var length = data.length;
                    var jsonData = new Array();
                    for(var i=0;i<length;i++){
                        var creType = data[i].creType;
                        var creTypeName = data[i].creTypeName;
                        jsonData.add({'creType':creType,'creTypeName':creTypeName});
                    };
                    mini.get("creditType").setData(jsonData);
                }
            });
        }
        
    </script>
    <script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>