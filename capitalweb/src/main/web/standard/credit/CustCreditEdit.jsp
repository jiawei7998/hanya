<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" >
	/**prdCode用于页面投资组合的选择   该全局变量必须为prdCode*/
	var prdCode="CCREDIT";
</script>
<title></title>
</head>
<body style="width: 100%; height: 100%; background: white">
	<div id="cust_credit_form" class="mini-panel" title="客户额度信息" style="width:100%" allowResize="true" collapseOnTitleClick="false">
		<div class="leftarea">
			<input id="creditName" name="creditName" class="mini-combobox mini-mustFill" labelField="true" label="额度类型" labelStyle="text-align:left;width:130px;" 
				style="width:100%;" enabled="false" requiredErrorText="该输入项为必输项" requited="true"  data="CommonUtil.serverData.dictionary.CustCreditType" />
			<input id="custNo" name="custNo" class="mini-buttonedit mini-mustFill" onbuttonclick="onButtonEdit" labelField="true" label="客户编号" 
				labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" requiredErrorText="该输入项为必输项" required="true" />
			<input id="creditStartDate" name="creditStartDate" class="mini-datepicker" labelField="true" label="授信起始日期" onvaluechanged="dateCheck"
				labelStyle="text-align:left;width:130px;" style="width:100%;" requiredErrorText="该输入项为必输项" required="true" />
			<input id="creditAmt" name="creditAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
				changeOnMousewheel="false" format="n2" labelField="true" label="额度总金额" labelStyle="text-align:left;width:130px;" style="width:100%;" 
				requiredErrorText="该项为必录项" required="true"  onvaluechanged="setAvlAmt" />
			<input id="occupyAmt" name="occupyAmt" labelField="true" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" format="n2" 
					changeOnMousewheel="false" required="true"  onvaluechanged="setChinese"
				label="预占用金额" labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="true" />
		</div>
		<div class="rightarea">
			<input id="custType" name="custType" class="mini-combobox mini-mustFill" labelField="true"  label="客户类型" requiredErrorText="该输入项为必输项"  requited="true"
				labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false" data="CommonUtil.serverData.dictionary.CType" />
			<input id="custName" name="custName" class="mini-textbox mini-mustFill" labelField="true" label="客户名称" requiredErrorText="该输入项为必输项"  requited="true"
				labelStyle="text-align:left;width:130px;" style="width:100%;" enabled="false"/>
			<input id="expDate" name="expDate" class="mini-datepicker mini-mustFill" labelField="true" label="授信结束日期" labelStyle="text-align:left;width:130px;" 
				style="width:100%;" onvaluechanged="dateCheck" requiredErrorText="该输入项为必输项" required="true" />
			<input id="quotaUsedAmt" name="quotaUsedAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
				changeOnMousewheel="false" format="n2" labelField="true" label="已用金额" labelStyle="text-align:left;width:130px;" style="width:100%;" 
				requiredErrorText="该项为必录项" required="true"  onvaluechanged="setAvlAmt" />
			<input id="quotaAvlAmt" name="quotaAvlAmt" class="mini-spinner mini-mustFill input-text-strong" maxValue="999999999999" 
				changeOnMousewheel="false" format="n2" labelField="true" label="剩余额度" labelStyle="text-align:left;width:130px;" style="width:100%;" 
				requiredErrorText="该项为必录项" requited="true"/>
		</div>
	</div>
	<br/>
	<div>
		<div style="margin-bottom:10px;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a></div>
	</div>
	
	<script>
		mini.parse();
		var form = new mini.Form("cust_credit_form");
		var url = window.location.search;
		var action = CommonUtil.getParam(url,"action");
		var custCreditId = CommonUtil.getParam(url,"custCreditId");
		
	    $(document).ready(function() {
	    	init();
		});
	    
	    function init() {
	    	if(action == 'modify') {
	    		var url = "/CustomerCreditController/search";
	    		CommonUtil.ajax({
		    		url:url ,
		    		data:{'custCreditId':custCreditId} ,
		    		callback:function(data){
	    				var bean = data.obj.rows[0];
		    			mini.get("creditName").setValue(bean.creditName);
		    			mini.get("custNo").setValue(bean.custNo);
		    			mini.get("custNo").setText(bean.custNo);
		    			mini.get("creditStartDate").setValue(bean.creditStartDate);
		    			mini.get("creditAmt").setValue(bean.creditAmt);
		    			mini.get("occupyAmt").setValue(bean.occupyAmt);
		    			mini.get("custType").setValue(bean.custType);
		    			mini.get("custName").setValue(bean.custName);
		    			mini.get("expDate").setValue(bean.expDate);
		    			mini.get("quotaUsedAmt").setValue(bean.quotaUsedAmt);
		    			mini.get("quotaAvlAmt").setValue(bean.quotaAvlAmt);
		    			setChinese();
		    		}
		    	});
	    	} else if(action == "add") {
	    		mini.get("creditName").setEnabled(true);
	    		mini.get("custNo").setEnabled(true);
	    		//mini.get("occupyAmt").setEnabled(true);
	    		var date = new Date();
	    		mini.get("creditStartDate").setValue(date);
	    	}
	    }
		
		function save() {
			
			form.validate();
			if(form.isValid() == false) {
				mini.alert("表单填写错误,请确认!"," 提示信息");
				return;
			}
			
			var saveUrl="/CustomerCreditController/insertAndUpdate";
			var data=form.getData(true);
			
			if(action == 'modify') {
				data['custCreditId'] = custCreditId;
			}
			
			var params=mini.encode(data);
			CommonUtil.ajax({
				url:saveUrl,
				data:params,
				callback:function(data){
					if(data.code == 'error.common.0000') {
						mini.get("save_btn").setEnabled(false);
						mini.alert(data.desc,"提示信息",function(){
							top["win"].closeMenuTab();
						});
					}
				},
				callerror:function(data){
//					mini.alert(data.desc,"提示信息");
				}
			});
		}
		
		function onButtonEdit() {
			var btnEdit = this;
	        mini.open({
	            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
	            title: "选择对手方列表",
	            width: 900,
	            height: 600,
	            ondestroy: function (action) {
	                if (action == "ok") {
	                    var iframe = this.getIFrameEl();
	                    var data = iframe.contentWindow.GetData();
	                    data = mini.clone(data);
	                    if (data) {
	                    	mini.get("custType").setValue(data.ctype);
	                    	mini.get("custName").setValue(data.cliname);
	                        btnEdit.setValue(data.cno);
	                        btnEdit.setText(data.cno);
	                        btnEdit.focus();
	                    }
	                }
	            }
	        });
		}
		
		function setAvlAmt() {
			
			var creditAmt = mini.get("creditAmt").getValue();//总额度
			var quotaUsenAmt = mini.get("quotaUsedAmt").getValue();//已用额度
			
			if(creditAmt < quotaUsenAmt) {
				mini.alert("已用额度不能超过总额度","提示信息");
				mini.get("quotaUsedAmt").setValue('');
				quotaUsenAmt = 0.00;
			}
			
			var avlAmt = CommonUtil.accSubtr(creditAmt,quotaUsenAmt);//剩余额度
			mini.get("quotaAvlAmt").setValue(avlAmt);
			
			setChinese();
			
		}
		
		function setChinese() {
			var fields = ['creditAmt'];//'creditAmt','quotaUsedAmt','quotaAvlAmt','occupyAmt'
			for(element of fields) {
				doSetChinese(element);
			}
		}
		
		function doSetChinese(field) {
			var fieldMIni = mini.get(field);
			var amt =  CommonUtil.chineseNumber(fieldMIni.getValue());
			fieldMIni.setText(fieldMIni.getValue() + '      ' + amt);
		}
		
		function dateCheck() {
			var startDate = mini.get("creditStartDate").getValue();
			var endDate = mini.get("expDate").getValue();
			if(CommonUtil.isNull(startDate) || CommonUtil.isNull(endDate)) {
				return;
			}
			if(startDate > endDate) {
				mini.alert("结束日期应在起始日期之后","提示信息");
				mini.get("expDate").setValue('');
			}
		}
		
	</script>
	<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>