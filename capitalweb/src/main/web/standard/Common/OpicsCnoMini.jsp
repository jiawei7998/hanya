﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
  <head>
	<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <title></title>
  </head>
  
<body style="width:100%;height:100%;background:white">
	<div class="mini-panel" title="客户信息单据列表（已同步）" style="width:100%">
		<div style="width:100%;padding-top:10px;">
			<div id="search_form" style="width:100%" cols="6">
				<input id="cno" name="cno" class="mini-textbox" labelField="true"  label="交易对手编号："  width="330px"  labelStyle="text-align:center;width:140px"  emptyText="请输入交易对手编号" />
				<input id="cname" name="cname" class="mini-textbox" labelField="true"  label="交易对手简称："    width="330px"  labelStyle="text-align:center;width:140px"  emptyText="请输入交易对手简称" />
				<input id="ctype" name="ctype" class="mini-combobox"  data="CommonUtil.serverData.dictionary.CType" width="330px" emptyText="请选择交易对手类型" labelField="true"  label="交易对手类型：" labelStyle="text-align:center;width:140px"/>
				<span style="float: right; margin-right: 50px"> 
					<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				</span>
			</div>
		</div>
	</div>
	
	<div id="tradeManage" class="mini-fit" >
		<div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
			onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
			<div property="columns">
				<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
				<div field="cno" width="180px" align="center"  headerAlign="center" >交易对手编号</div>    
				<div field="cname" width="150"  headerAlign="center" align="center" >交易对手代号</div>
				<div field="sn" width="250px" align="center"  headerAlign="center" >交易对手简称</div>
				<div field="ctype" width="150px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" 
					 data-options="{dict:'CType'}">交易对手类型</div>
				<div field="clino" width="150"  headerAlign="center" align="center" >ECIF编号</div>
				<div field="localstatus" width="100"  headerAlign="center" align="center" 
				renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}">处理状态</div>
				<div field="gtype" width="150px" align="center"  headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'GType'}">证件类型</div>
				<div field="gid" width="150"  headerAlign="center" align="center" >证件号码</div>
				<div field="custclass" width="100"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'custclass'}">客户分类</div>
				<div field="cliname" width="150"  headerAlign="center" align="center" >客户中文名称</div>
				<div field="fintype" width="120"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'fintype'}">金融机构类型</div>
				<div field="license" width="140"  headerAlign="center" align="center" >客户金融许可证</div>
				<div field="flag" width="100"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">境内外标志</div>
				<div field="regplace" width="180"  headerAlign="center" align="center" >注册地/经营所在地</div>
				<div field="indtype" width="120"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'indtype'}">国民行业类型</div>
				<div field="ecotype" width="120"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ecotype'}">国民经济类型</div>
				<div field="ratype" width="100"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ratype'}">评级类型</div>
				<!-- <div field="extrat" width="100"  headerAlign="center" align="center" >外部评级</div> -->
				<div field="extrainfo" width="120"  headerAlign="center" align="center" >外部评级信息</div>
				<div field="ratresult1" width="100"  headerAlign="center" align="center" >外部评级结果</div>
				<div field="interinfo" width="120"  headerAlign="center" align="center" >内部评级信息</div>
				<div field="ratresult2" width="100"  headerAlign="center" align="center" >内部评级结果</div>				
				<div field="bic" width="120" align="center" headerAlign="center" 
					 renderer="CommonUtil.dictRenderer" data-options="{dict:'bicCode'}">SWIFT编码</div>
				<div field="ccode" width="150px" align="center"  headerAlign="center"
					 renderer="CommonUtil.dictRenderer" data-options="{dict:'cityCode'}">城市代码</div>                            
				<div field="sic" width="220px" align="center"  headerAlign="center"
				     renderer="CommonUtil.dictRenderer" data-options="{dict:'sicCode'}">工业标准代码</div>                            
				<div field="cfn" width="250px" align="center"  headerAlign="center" >交易对手全称</div>                                
				<div field="ca" width="190px" align="center"  headerAlign="center" >公司地址</div>
				<div field="cpost" width="110px" align="center"  headerAlign="center" >邮编</div>
				<div field="lstmntdate" width="160px" align="center"  headerAlign="center" >维护更新日期</div>
				<div field="uccode" width="90px" align="center"  headerAlign="center" >国家代码</div>
				<div field="taxid" width="100"  headerAlign="center" align="center">纳税人编号</div>
				<div field="acctngtype" width="190"  headerAlign="center" align="center"
				 	 renderer="CommonUtil.dictRenderer" data-options="{dict:'custAccty'}">会计类型</div>
				<div field="location" width="180"  headerAlign="center" align="center">所在城市</div>
				<div field="ratinfo" width="180"  headerAlign="center" align="center">备注</div>
				<div field="remark1" width="180"  headerAlign="center" align="center">备注1</div>
				<div field="remark2" width="180"  headerAlign="center" align="center">备注2</div>
			</div>
		</div>
	</div>   
	
	<script type="text/javascript">
		mini.parse();
		var form=new mini.Form("#search_form");
		var grid=mini.get("cust_grid");
		top["OpicsCnoMini"] = window;
		
		/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		url = "/IfsOpicsCustController/searchPageOpicsCust";
		data['localstatus']='1';
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:url,
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	//清空
	function clear() {
		var form=new mini.Form("search_form");
        form.clear();
        query();
      }
	
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	//选中一行将数据展示
	function onRowDblClick(){
		CloseWindow("ok");
	}
	function GetData() {
        var row = grid.getSelected();
        return row;
	}
	function SetData(data){
		rows=data.gridData;
		fundCode=data.fundCode;
	}
	function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
	}
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
	$(document).ready(function(){
		search(10,0);
	})
	</script>
</body>
</html>