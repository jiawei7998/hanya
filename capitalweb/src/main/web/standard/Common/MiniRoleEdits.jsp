<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../global.jsp"%>

<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<div id="RoleEdit_div" class="mini-panel" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
	<table id="role_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable" >
		<tr>
			<td style="width:50%">
				<input id="roleName" name="roleName" class="mini-textbox mini-mustFill" labelField="true" label="角色名称" emptyText="请输入角色名称" vtype="maxLength:25" style="width:100%"labelStyle="text-align:left;width:170px"
				 required="true"  />
			</td>
			<td style="width:50%">
				<input id="roleId" name="roleId" class="mini-textbox" labelField="true" label="角色代码" emptyText="自动生成" allowInput="false" style="width:100%"labelStyle="text-align:left;width:170px"/>
			</td>
		</tr>
		<tr>
			<td style="width:50%">
				<input id="instId" name="instId" valueField="id" labelField="true" label="所属机构" showFolderCheckBox="true" showCheckBox="true"
				 showTreeLines="true" showTreeIcon="true" textField="instName" parentField="parentId" class="mini-treeselect mini-mustFill"  expandOnLoad="true" resultAsTree="false"
				 valueFromSelect="true" multiSelect="true" emptyText="请选择机构" required="true"  style="width:100%"labelStyle="text-align:left;width:170px"/>
			</td>
			<td style="width:50%">
				<select id="isActive" name="isActive" class="mini-combobox mini-mustFill" labelField="true" label="是否启用" required="true"  emptyText="请选择启用状态" data="CommonUtil.serverData.dictionary.YesNo" style="width:100%"labelStyle="text-align:left;width:170px"></select>
			</td>
		</tr>
		<tr>
			<td style="width:50%">
				<input id="roleMemo" name="roleMemo" labelField="true" label="备注" emptyText="请输入备注" class="mini-textbox" vtype="maxLength:50" style="width:100%"labelStyle="text-align:left;width:170px"
				/>
			</td>
			<td style="width:50%">
				<input id="roleLevel" name="roleLevel" class="mini-combobox" labelField="true" label="角色级别" required="true"  emptyText="请选择角色级别" data="CommonUtil.serverData.dictionary.roleLevel"
					   style="width:100%"labelStyle="text-align:left;width:170px" value="3" enabled="false" visible="hiden"/>
			</td>
		</tr>
		<tr>
			<td style="width:50%">
				<input id="indexPage" name="indexPage" class="mini-textbox" labelField="true" label="首页类型" emptyText="请输入首页类型" visible="hiden" value="index_approve.jsp" vtype="maxLength:16" style="width:100%"labelStyle="text-align:left;width:170px"
				/>
			</td>
			<td style="width:50%">
				<input id="roleFlag" name="roleFlag" class="mini-combobox mini-mustFill" labelField="true" label="角色类型" required="true"  emptyText="请选择角色类型" data="CommonUtil.serverData.dictionary.RoleType"
					   style="width:100%"labelStyle="text-align:left;width:0px" value="0" visible="hiden"/>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align:center;">
					<a id="save_btn" class="mini-button" style="display: none"    onclick="save();">保存</a>
					<a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
			</td>
		</tr>
	</table>
</div>
<script>
mini.parse();
var grid = mini.get("instId");
mini.get("indexPage").setEnabled();
var url = window.location.search;
var action = CommonUtil.getParam(url, "action");
var roleEditId='';

$(document).ready(function(){
	//控制按钮显示
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
		if (action == 'edit') {
			mini.get("RoleEdit_div").setTitle("角色信息  >> 修改");
			mini.get("roleId").setEnabled(false);
			// mini.get("roleLevel").setEnabled(false);
			// mini.get("roleFlag").setEnabled(false);
			init();
		} else {
			mini.get("RoleEdit_div").setTitle("角色信息  >> 新增");
		}

		mini.get("roleId").setEnabled(false);
		institution();
	});
})
//修改
function init(){
	var form =new mini.Form("role_form");
	var record=top["roleManage"].getData();
	roleEditId =record.roleId;
	form.setData(record);
}
//加载机构树
function institution() {
	var instHaving='';
	if(action=='edit'){//根据角色Id查询该角色下的机构
				CommonUtil.ajax({
					url:"/RoleController/searchInstIdByRoleId",
					data:mini.encode({"roleId":roleEditId}),
					callback: function (data) {
						for(var i=0;i<data.obj.length;i++){
							instHaving=instHaving+data.obj[i]+","
						}
					}
				})
			}
	var param = mini.encode({branchId:branchId}); //序列化成JSON
	CommonUtil.ajax({
		url: "/InstitutionController/searchInstitutionByBranchId",
		data: param,
		callback: function (data) {
			grid.setData(data.obj);
			grid.setValue(instHaving);
		}
	});
}
//保存操作
function save() {
	var form = new mini.Form("RoleEdit_div");
	form.validate();
	if (form.isValid() == false) return;
	var paramObj=form.getData();
	paramObj.branchId=branchId;
	paramObj['instName'] = mini.get('instId').getText();
	var param = mini.encode(paramObj);
	var url='';
	if(action=='edit'){
		url='/RoleController/updateRole';
	}else{
		url='/RoleController/saveRole';
	}
	CommonUtil.ajax({
		url: url,
		data: param,
		callback: function (data) {
			if (data.code == 'error.common.0000') {
				mini.alert("操作成功","系统提示",function(){setTimeout(function(){
					top["win"].closeMenuTab()
				},10);
					
				});
			} else {
				mini.alert("操作失败","系统提示");
			}
		}
	});
}
//取消按钮
function cancel(){
	window.CloseOwnerWindow();
}
</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
	