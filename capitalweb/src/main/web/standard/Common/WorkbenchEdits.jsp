<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>产品限额维护页面</title>
</head>
<body style="width:100%;height:100%;background:white">

	<div id="UserEdit"  style="width: 100%; height: 100%;">
		<!-- <span id="labell"><a href="javascript:CommonUtil.activeTab();">用户添加</a> >> 新增</span> -->
		<div id="field" style="padding:5px;">
			
			<table  width:100%;" class="mini-sltable">
			<h1 style="text-align:left;" id="labell"><a href="javascript:CommonUtil.activeTab();">产品限额</a> >> 修改</h1>
				<tr>
					<td style="width:50%">
						<input id="product" name="product" class="mini-combobox" labelField="true" label="产品："   onValueChanged="initCondition" emptyText="请选择产品"  valueField="TABLE_NAME" textField="COMMENTS" required="true"  style="width:100%" labelStyle="text-align:left;width:170px" />
                        <input class="mini-hidden" id="id" name="id" />
                        <input class="mini-hidden" id="inputTime" name="inputTime" />
                    </td>
					<td style="width:50%">
						<input id="type" name="type" class="mini-combobox" labelField="true" label="限额类型："  emptyText="请选择限额类型" data="CommonUtil.serverData.dictionary.limitType" required="true"   style="width:100%" labelStyle="text-align:left;width:170px" />
					</td>
				</tr>
				<tr>
                    <td style="width:50%">
                        <input id="trader" name="trader" class="mini-buttonedit" onbuttonclick="onButtonEdit" labelField="true"  label="交易员："  emptyText="请选择交易员"  required="true"  style="width:100%" labelStyle="text-align:left;width:170px"  />
					</td>
					<td style="width:50%">
						<input id="quotaAmount" name="quotaAmount" class="mini-spinner" labelField="true"  label="限额金额：" onValueChanged="limitAmt" emptyText="请输入限额金额" maxValue="999999999999" changeOnMousewheel='false' format="n2"  onvaluechanged="getSellAmount" style="width:100%" labelStyle="text-align:left;width:170px" 
						 />
					</td>
				</tr>
				<tr>
					<td style="width:50%">
						<input id="availAmount" name="availAmount" class="mini-spinner" labelField="true"  label="可用金额：" enabled="false" emptyText="请输入可用金额" maxValue="999999999999" changeOnMousewheel='false' format="n2"  onvaluechanged="getSellAmount" style="width:100%" labelStyle="text-align:left;width:170px" 
						 />
					</td>
					<td style="width:50%">
						<input id="inputTrader" name="inputTrader" class="mini-textbox" labelField="true" label="录入人员：" enabled="false"  emptyText=""vtype="maxLength:50" style="width:100%" labelStyle="text-align:left;width:170px"  
						/>
					</td>
                </tr> 
            </table>
                <!-- <div class="centerarea">
                    <fieldset>
                        <legend>限额基本信息</legend>
                        <input id="product" name="product" class="mini-combobox" labelField="true" label="产品："   onValueChanged="initCondition" emptyText="请选择产品"  valueField="TABLE_NAME" textField="COMMENTS" required="true"   labelStyle="text-align:left" />
                        <input class="mini-hidden" id="id" name="id" />
                        <input class="mini-hidden" id="inputTime" name="inputTime" />
                        <input id="type" name="type" class="mini-combobox" labelField="true" label="限额类型：" emptyText="请选择限额类型" data="CommonUtil.serverData.dictionary.limitType" required="true"   labelStyle="text-align:left" />
                        <input id="trader" name="trader" class="mini-buttonedit" onbuttonclick="onButtonEdit" labelField="true"  label="交易员："  emptyText="请选择交易员"  required="true"   labelStyle="text-align:left"  />
                        <input id="quotaAmount" name="quotaAmount" class="mini-spinner" labelField="true"  label="限额金额：" emptyText="请输入限额金额" maxValue="999999999999" changeOnMousewheel='false' format="n2"  onvaluechanged="getSellAmount" labelStyle="text-align:left;" />
                        <input id="availAmount" name="availAmount" class="mini-spinner" labelField="true"  label="可用金额：" emptyText="请输入可用金额" maxValue="999999999999" changeOnMousewheel='false' format="n2"  onvaluechanged="getSellAmount"  labelStyle="text-align:left;" />
                        <input id="inputTrader" name="inputTrader" class="mini-textbox" labelField="true" label="录入人员：" enabled="false"  emptyText=""vtype="maxLength:50"  labelStyle="text-align:left"  />
                    </fieldset>
                </div> -->
                <table id="append_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                        <input id="limitCondition" name="limitCondition" class="mini-buttonedit" onbuttonclick="LimitConditEdit"  labelField="true" label="限额条件：" required="true"  vtype="maxLength:50" style="width:100%" labelStyle="text-align:left;width:170px"  />
                </table> 

				<table style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                    <tr>
                        <td colspan="3" style="text-align:center;">
                                <a class="mini-button" style="display: none"    onClick="save()" id="save_btn">保存</a>
                                <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                        </td>
                    </tr>
                </table>
			
		</div>
</div>
</body>
<script>
    mini.parse();

    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field");

    $(document).ready(function(){
        initData();
        loadProductList();
        
            
    }); 

    //初始化数据
    function initData(){
        if($.inArray(action,["edit","detail"])>-1){//修改、详情
            form.setData(row);
            mini.get("trader").setValue(row.trader);
            mini.get("trader").setText(row.userName);
            if(action == "detail"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品限额</a> >> 详情");
                form.setEnabled(false);
                mini.get("save_btn").setVisible(false);
            }
        }else{//增加 
            mini.get("inputTrader").setValue("<%=__sessionUser.getUserId()%>");
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>产品限额</a> >> 新增");
        }
    }
    //初始化限额条件
    function initCondition(e){
        CommonUtil.ajax({
            url:"/IfsFieldLimitController/searchLimitFieldAndDict",
            data:{prd:e.value},
            callback:function(data){
                mini.get("limitCondition").setValue(data.desc);
                
            }
        }); 
    }

   /*  function formTypeValueChange(e){
        var  conditionData ="";
        var length ="";
        var content; 

        CommonUtil.ajax({
            url:"/IfsFieldLimitController/searchLimitFieldAndDict",
            data:{prd:e.value},
            callback:function(data){
                conditionData = data.obj;
                length=data.obj.length;
                //for(var i=0;i<length;i++){
                    //$("#append_form").append(" <input id='+conditionData[i].colum+' name='' class='mini-combobox' labelField='true' label='限额条件' labelStyle='text-align:left;'");
                    content = '<tr><td><input id="' + conditionData[0].COLUM +'"  name="'+conditionData[0].COLUM+ '" class="mini-combobox"  labelField="true"  labelStyle="text-align:left" label="限额条件"   data="CommonUtil.serverData.dictionary.'+conditionData[0].DICT_ID+'"'  +'  </td></tr>';
                    
                
                //}
            }
        });
        return content;
    } */

    //保存
    function save(){
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写有误!","提示");
            return;
        }
        var saveUrl=null;
        if(action == "add"){
            saveUrl = "/IfsLimitController/LimitConditionAdd";
        }else if(action == "edit"){
            saveUrl = "/IfsLimitController/saveLimitConditionEdit";
        }

        var data=form.getData(true);
        var params=mini.encode(data);
        CommonUtil.ajax({
            url:saveUrl,
            data:params,
            callback:function(data){
                mini.alert("保存成功",'提示信息',function(){
                    top["win"].closeMenuTab();
                });
            }
        });
    }

    //取消
    function cancel(){
        top["win"].closeMenuTab();
    }

    //加载产品列表
    function loadProductList(){
        CommonUtil.ajax({
            data:{IFS_CFET:"IFS_CFET"},
            url:"/IfsFieldLimitController/searchTables",
            callback:function(data){
                var prdTypeNo = mini.get("product");
                prdTypeNo.setData(data.obj);
            }
        });	
    } 


    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/MiniUserManagesMini.jsp",
            title: "选择限额交易员列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.userId);
                        btnEdit.setText(data.userName);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
    function LimitConditEdit(e) {
    	 var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/WorkbenchLimitC.jsp",
            title: "选择限额字段",
            width: 1000,
            height: 600,
            ondestroy: function (action) {              
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    
                    data = mini.clone(data);                                  
                    if( data== undefined){return;}
        	    	var limitCondition = "";
        	    	if( data){
        	    		 
        	    		for(var i = 0; i <  data.length; i++){
        	    			if(i ==  data.length - 1){
        	    				limitCondition += data[i].colum+":"+data[i].dictId1;
        	    				
        	    			}else{
        	    				limitCondition += data[i].colum+":"+data[i].dictId1+ ",";
        	    				
        	    			}
        	    			
        	    		}
        	    	}
        	    	 limitCondition ="{"+limitCondition+"}";
        	    	 btnEdit.setValue(limitCondition);
                     btnEdit.setText(limitCondition);
                    
               
                }
            });

        }
    //
    function limitAmt(e){
        mini.get("availAmount").setValue(e.value);

    }


</script>     
</html>