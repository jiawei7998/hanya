<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title>交易对手</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>交易对手查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="cno" name="cno" class="mini-textbox" labelField="true"  label="客户号："  width="320px"
				labelStyle="text-align:center;width:140px"  emptyText="请输入客户号" />
			<input id="cname" name="cname" class="mini-textbox" labelField="true"  label="客户代码："  width="320px"
				labelStyle="text-align:center;width:140px"  emptyText="请输入客户代码" />
			<input id="cliname" name="cliname" class="mini-textbox" labelField="true"  label="客户名称："  width="320px"
				labelStyle="text-align:center;width:140px"  emptyText="请输入客户名称" />
			<input id="acctngtype" name="acctngtype" class="mini-textbox" labelField="true"  label="会计类型："  width="320px"
				labelStyle="text-align:center;width:140px"  emptyText="请输会计类型" />
			<input id="localstatus" name="localstatus" class="mini-combobox" labelField="true"  label="处理状态："  width="320px"
				labelStyle="text-align:center;width:140px"  emptyText="请输入处理状态" 
				 data="CommonUtil.serverData.dictionary.DealStatus"/>
			<input id="ctype" name="ctype" class="mini-combobox"  data="CommonUtil.serverData.dictionary.CType" width="320px"
				emptyText="请选择客户类型" labelField="true"  label="客户类型：" labelStyle="text-align:center;width:140px"/>
			<span style="float: right; margin-right: 50px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
	<span style="margin: 2px; display: block;">
		<a id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
		<a id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
		<a id="sync" class="mini-button" style="display: none"   onclick="sync()">同步至OPICS</a>
		<a id="calib" class="mini-button" style="display: none"  onClick="calibration()">批量校准信息</a>
		<a id="update" class="mini-button" style="display: none"  onClick="update()">更新核心客户信息</a>
		<a id="expt_btn" class="mini-button" style="display: none"   onclick="exportExcel()">导出报表</a>
	</span>
</fieldset> 

<div class="mini-fit" >
	<div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div field="ioper" width="50px" align="center"  headerAlign="center" >经办人</div>
			<div field="reviewer" width="50px" align="center"  headerAlign="center" >复核人</div>
			<div field="cno" width="120px" align="center"  headerAlign="center" >客户号</div>    
			<div field="cname" width="100px"  headerAlign="center" align="center" >客户代码</div>
			<div field="sn" width="250px" align="center"  headerAlign="center" >客户简称</div>
			<div field="cliname" width="200px"  headerAlign="center" align="center" >客户名称</div>
			<div field="linkman" width="120" align="center" headerAlign="center">联系人</div>
			<div field="contact" width="120" align="center" headerAlign="center">联系方式</div>
			<div field="localstatus" width="100"  headerAlign="center" align="center" 
			 	renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}">处理状态</div>
			<div field="shtype" width="200px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer"
				data-options="{dict:'GidType'}">唯一标识代码类型</div>
			<div field="shcode" width="200px"  headerAlign="center" align="center" >唯一标识代码</div>
			<div field="ctype" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" 
				 data-options="{dict:'CType'}">客户类型</div>
<%--			<div field="custclass" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" --%>
<%--				 data-options="{dict:'YesNo'}">是否为集团客户</div>--%>
			<div field="subjectType" width="200px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" 
			 	 data-options="{dict:'ztType'}">主体类型</div>
			<div field="acctngtype" width="120px"  headerAlign="center" align="center">会计类型</div>   
			<div field="bic" width="120" align="center" headerAlign="center">SWIFT编码</div>	 
			<div field="uccode" width="90px" align="center"  headerAlign="center" >国家代码</div>
			<div field="ccode" width="90px" align="center"  headerAlign="center">城市代码</div>                            
			<div field="sic" width="120px" align="center"  headerAlign="center">工业标准代码</div>   
			<div field="cfn" width="250px" align="center"  headerAlign="center" >客户全称</div>
		    <div field="location" width="90px"  headerAlign="center" align="center">所在城市</div>
			<div field="cfetsno" width="200px" headerAlign="center" align="center">CFETS本币机构编码（6位）</div>
			<div field="cfetscn" width="200px" headerAlign="center" align="center">CFETS外币机构编码（21位）</div>
			<div field="clino" width="120px"  headerAlign="center" align="center" >核心客户号</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid = mini.get("cust_grid");
	var row = "";

	grid.on("select",function(e){
		var row = e.record;
		if(row.localstatus == "0"){//未同步
			mini.get("sync").setEnabled(true);
			mini.get("delete_btn").setEnabled(true);
		}
		if(row.localstatus == "1"){//已同步
			mini.get("sync").setEnabled(false);
			mini.get("delete_btn").setEnabled(false);
		}
	}); 
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	//查看详情
	function onRowDblClick(e) {
		var url = CommonUtil.baseWebPath() +"/../Common/CustEdit.jsp?action=detail";
		var tab = {id: "CustDetail",name:"CustDetail",url:url,title:"交易对手详情",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:grid.getSelected()};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	

		
	//修改
	function edit(){
		var row=grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/../Common/CustEdit.jsp?action=edit&limit=1&handles=3";
			var tab = {id: "CustEdit",name:"CustEdit",url:url,title:"交易对手修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	//删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除该交易对手信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsOpicsCustController/deleteOpicsCust",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	//同步
	function sync(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要同步该交易对手信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsOpicsCustController/syncCustD",
					data:params,
					callback : function(data) {
						mini.alert(data.obj.retMsg,data.obj.retCode);
						search(10,0);
					}
				});
			}
		});
	}
	
	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['custclass']='0';
		data['handle']='3';
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsCustController/searchPageOpicsCust",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
	
	//批量校准信息
    function calibration() {
		var rows = grid.getSelecteds();
		if(rows.localstatus == "0"){
			mini.alert("请先同步再校准");
			return;
		}
    	if(rows.length > 0){ //选中行校准
    		var str = "";
    		if(rows.length == 1){
    			str = rows[0].cno;
    		} else {
    			for(var i=0;i<rows.length;i++){
    				if(i==0){
    					str = rows[i].cno + ",";
    				} else {
    					if(i==rows.length-1){
    						str = str + rows[i].cno;
    					} else {
    						str = str + rows[i].cno + ",";
    					}
    				}
    			}
    		}
    		mini.confirm("您确认要校准选中记录?","系统警告",function(value){
    			if(value=="ok"){
    				CommonUtil.ajax({
    		             url: "/IfsOpicsCustController/batchCalibration",
    		             data: {cno:str,type:1},
    		             callback: function (data) {
    		            	 mini.alert(data.obj.retMsg,data.obj.retCode);
    		            	 search(10,0);
    		             }
    		         });
    			}
    		});
    	} else { //未选中就全部校准
    		mini.confirm("您确认要校准全部记录?","系统警告",function(value){
    			if(value=="ok"){
    				CommonUtil.ajax({
    		             url: "/IfsOpicsCustController/batchCalibration",
    		             data: {type:2},
    		             callback: function (data) {
    		            	 mini.alert(data.obj.retMsg,data.obj.retCode);
    		            	 search(10,0);
    		             }
    		         });
    			}
    		});
    	}
    }
	
    /*ECIF信息更新 */
	function update(){
		var rows = grid.getSelecteds();
    	if(rows.length > 0){ //选中行校准
    		var ecifno = rows[0].clino;
			mini.confirm("您确认要更新选中客户信息吗?","系统警告",function(value){
				if(value=="ok"){
					CommonUtil.ajax({
						url:"/IfsOpicsCustController/updateCustInfo",
						data:ecifno,
						callback : function(data) {
							if(data == "000000"|| data == "AAAAAAAAAA"){
   		            			mini.alert("更新成功!");
   		            			search(10, 0);
	   		            	} else {
	   		            		mini.alert("更新失败!");
	   		            	}
						}
					});
				}
			});
    	} else {
    		mini.alert("请选中一行更新!");
    	}
    }
    
	// 导出
	function exportExcel(){
		var content = grid.getData();
		if(content.length == 0){
			mini.alert("请先查询数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsExportController/exportCustExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
				}
			}
		);
	}
</script>
</body>
</html>