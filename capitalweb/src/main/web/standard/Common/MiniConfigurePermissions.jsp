﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
	<head>
		<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	</head>

	<body style="width:100%;height:100%;background:white">
			<div id="institution_search_form" style="text-align: center;">
				<span style="font-size: medium;">角色名称  </span><input id="roleName" name="roleName" enabled="false" allowInput="false" class="mini-textbox" vtype="maxLength:50"/>
				<span>
					<a class="mini-button" style="display: none" id="add_btn" onclick="save()" >保存</a>
					<a class="mini-button" style="display: none" id="coll_btn" onclick="collapseAll()" >收缩所有</a>
					<a class="mini-button" style="display: none" id="expand_btn" onclick="expandAll()" >展开所有</a>
				</span>
			</div>

		<div class="mini-fit" style="overflow: hidden;">
			<div id="treegrid" class="mini-treegrid borderAll"  ondrawcell="onDrawCell" showTreeIcon="true" treeColumn="moduleName" idField="moduleId"
				 showCheckBox="true" checkRecursive="true"
				 parentField="modulePid" resultAsTree="false" allowResize="true" expandOnLoad="false" height="100%"  allowAlternating="true">
				<div property="columns" allowMove="true" allowResize="true">
					<div type="indexcolumn"  width="20px" headerAlign="center"></div>
					<div name="moduleId"  field="moduleId" width="40px" headerAlign="center">菜单编号</div>
					<div name="moduleName"  field="moduleName" width="120px" headerAlign="center">菜单名称</div>
					<div field="functions"  width="300px" headerAlign="center">权限功能表</div>
					<div field="selectedAll" align='center' headerAlign="center" width='80px'>权限功能选择操作</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			mini.parse();

			var form = new mini.Form("#institution_search_form");
			var grid = mini.get("treegrid");
			var InstitutionType =CommonUtil.serverData.dictionary.InstitutionType;
			var functionList = "";

			function search() {
				var form = new mini.Form("institution_search_form");
				form.validate();
				var data = form.getData();//获取表单多个控件的数据
				data.roleId = GetQueryString("roleId");
				data.branchId = branchId;
				var params = mini.encode(data); //序列化成JSON
				CommonUtil.ajax({
					url: '/ModuleController/searchDeskModuleList',
					data: params,
					callback: function (data) {
						grid.setData(data.obj);

						//初始化接节点
						var roledata = {};
						roledata.roleId = GetQueryString("roleId");
						var roleparams = mini.encode(roledata); //序列化成JSON
						CommonUtil.ajax({
							url: '/RoleModuleController/searchModuleByRoleIdList',
							data: roleparams,
							callback: function (data) {
								grid.setValue(data.obj.toString());
							}
						});
					}
				});
			}

			function save() {
				//获取菜单选中的内容
				var roleId = GetQueryString("roleId");
				var functionList = new Array();//功能点保存对象
				var deskMenuList = new Array();//保存台子对象
				var moduleList = new Array();//保存模块对象
				var paramObj = {};

				//获取选中的功能点
				var keys = grid.getValue(true).split(",");

				var treeAllNote = grid.getData();//台子节点 10
				for (var i = 0; i < treeAllNote.length; i++) {
					if (keys.indexOf (treeAllNote[i].moduleId) > -1) { //台子节点 选中  treeAllNote[i].checked
						var deskmenued = {};
						deskmenued.roleId = roleId;
						deskmenued.deskId = treeAllNote[i].moduleId;
						deskmenued.accessFlag = '1';
						deskmenued.deskUrl = './menu.jsp';
						deskMenuList.push(deskmenued);
					}
					paramObj.roleId = roleId;
					paramObj.branchId = branchId;
					var firstLayer = treeAllNote[i];//一级菜单节点 12
					if (firstLayer.children != null && firstLayer.children.length > 0) {
						var secondFloor = firstLayer.children;
						for (var j = 0; j < secondFloor.length; j++) {
							if (keys.indexOf (secondFloor[j].moduleId) > -1) { //一级菜单节点 选中
								var taRoleModule = {};
								taRoleModule.roleId = roleId;
								taRoleModule.moduleId = secondFloor[j].moduleId;
								taRoleModule.accessFlag = '1';
								moduleList.push(taRoleModule);
							}
							var secondFloorFunction = secondFloor[j].functions;//一级菜单节点 功能点  （没有数据）
							if (secondFloorFunction != null && secondFloorFunction.length > 0) {
								for (var n = 0; n < secondFloorFunction.length; n++) {
									if (secondFloorFunction[n].checked) {
										var funtionPoint = {};
										funtionPoint.roleId = roleId;
										funtionPoint.moduleId =secondFloor[j].id;
										funtionPoint.functionId = secondFloorFunction[n].functionId;
										functionList.push(funtionPoint);
									}
								}
							}
							if (secondFloor != null && secondFloor.length > 0) {
								var thirdLayer = secondFloor[j].children;//二级菜单节点
								if (thirdLayer != null && thirdLayer.length > 0) {
									for (var k = 0; k < thirdLayer.length; k++) {
										if (keys.indexOf (thirdLayer[k].moduleId) > -1) { //二级菜单节点 选中
											var taRoleModule = {};
											taRoleModule.roleId = roleId;
											taRoleModule.moduleId = thirdLayer[k].moduleId;
											taRoleModule.accessFlag = '1';
											moduleList.push(taRoleModule);
										}
										var attributesList = thirdLayer[k].functions;//二级菜单节点 功能点
										if (attributesList != null && attributesList.length > 0) {
											for (var v = 0; v < attributesList.length; v++) {
												var functionNote = attributesList[v];
												if (functionNote.checked) {
													var funtionPoint = {};
													funtionPoint.roleId = roleId;
													funtionPoint.moduleId=thirdLayer[k].id;
													funtionPoint.functionId = functionNote.functionId;
													functionList.push(funtionPoint);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				paramObj.deskMemuSelected = deskMenuList;
				paramObj.moduleSelected = moduleList;
				paramObj.functionSelected = functionList;
				if (paramObj) {
					if (paramObj.deskMemuSelected != null && paramObj.deskMemuSelected.length > 0) {
						//发送ajax
						mini.confirm("您确定分配吗？", "温馨提示", function (action) {
							if (action == 'ok') {
								CommonUtil.ajax({
									url: "/RoleController/savaAllData",
									data: mini.encode(paramObj),
									callback: function (data) {
										if (data.code == 'error.common.0000') {
											mini.alert("分配成功","系统提示",function(){
												onCancel();
											});
										} else {
											mini.alert("分配失败，请重试~","系统提示");
										}
									}
								});
							}
						})
					} else {
						mini.alert("请先选择菜单","系统提示");
					}
				} else {
					mini.alert("请先选择菜单列表","系统提示");
				}
			}

			function onCancel(e) {
				window.CloseOwnerWindow();
			}

			//全选功能
			function quanxuan(id,flag) {
				var record = grid.getRecord(id);
				if (record) {
					var funcs = record.functions;
					if (funcs != null && funcs.length > 0) {
						for (var i = 0; i < funcs.length; i++) {
							var funcsId = funcs[i].functionId;
							checkFunc(id, funcsId, flag);
							$("#" + funcsId).attr("checked", flag);
						}
					}
				}
			}

			function getData(action) {
				if (action != "add") {
					var row = null;
					row = grid.getSelected();
				}
				return row;
			}

			function expandAll() {
				grid.expandAll();
			}

			function collapseAll() {
				grid.collapseAll();
			}

			$(document).ready(function () {
				//控制按钮显示
				$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
					//loadResourceTree();
					showInstAndRoleName();
					search();
				});
			})

			//渲染机构名称 角色名称
			function showInstAndRoleName() {
				// var instName=mini.get("instName");
				// instName.setValue(decodeURI(GetQueryString("instName")));
				var roleName = mini.get("roleName");
				roleName.setValue(decodeURI(GetQueryString("roleName")));
			}

			//默认加载资源树
			function loadResourceTree() {
				var data = {};
				data.roleId = GetQueryString("roleId");
				data.branchId = branchId;
				var param = mini.encode(data);
				CommonUtil.ajax({
					url: "/RoleController/searchModuleAndFunctionByRoleId",
					data: param,
					async:false,
					callback: function (data) {
						functionList = data;
					}
				});
			}

			function GetQueryString(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
				var r = window.location.search.substr(1).match(reg);
				if (r != null)
					return unescape(r[2]);
				return null;
			}

			function onDrawCell(e) {
				var tree = e.sender,
					node = e.node,
					record = e.record,
					field = e.field,
					id = record[tree.getIdField()],
					funs = record.functions;

				if (field == 'functions') {
					if (!funs) return "";
					var html = "";
					for (var i = 0, l = funs.length; i < l; i++) {
						var fn = funs[i];
						var clickFn = 'checkFunc(\'' + id + '\',\'' + fn.functionId + '\', this.checked)';
						var checked = fn.checked ? 'checked' : '';
						html += '<label class="function-item"><input onclick="' + clickFn + '" ' + checked + ' type="checkbox" name="'
								+ fn.functionId + '" id='+fn.functionId+' hideFocus/>' + fn.functionName + '</label>';
						if(i!=0 && (i+1)%5 ==0){
							html +="<br>"
						}
					}
					e.cellHtml =  html;
				}
				if(field == 'selectedAll'){
					if (!funs) {
						return "";
					}else{
						if(funs.length>0){
							// e.cellHtml = "<a class='mini-button' height=22  onclick='quanxuan(\"" + id + "\",true)' >全选</a>" +
							// 		"  <a class='mini-button' height=22  onclick='quanxuan(\"" + id + "\",false)' >取消</a>";
							e.cellHtml ="<label class='function-item'><input onclick='quanxuan(\"" + id + "\",this.checked)' type='checkbox' name='operCkId' id='operCkId'/></label>";
						}
					}
				}
			}

			//检查按钮
			function checkFunc(id, functionId, checked) {
				var record = grid.getRecord(id);
				if (!record) return;
				var funs = record.functions;
				if (!funs) return;
				function getAction(functionId) {
					for (var i = 0, l = funs.length; i < l; i++) {
						var o = funs[i];
						if (o.functionId == functionId) return o;
					}
				}
				var obj = getAction(functionId);
				if (!obj) return;
				obj.checked = checked;
				var keys = grid.getValue();
				keys = keys + "," + record.moduleId;
				grid.setValue(keys);
			}

		</script>
	</body>
</html>