<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户准入</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>客户准入</legend>
    <div>
        <div id="search_form" style="width:100%" cols="6">
            <input id="coreClntNo" name="cno" class="mini-textbox" labelField="true" label="客户编号："
                   labelStyle="text-align:right;"
                   width="330px" emptyText="请输入客户编号"/>
            <input id="branchName" name="branchName" class="mini-textbox" labelField="true" label="客户中文名称："
                   labelStyle="text-align:right;"
                   width="330px" emptyText="请输入客户中文名称"/>
            <span style="float:right;margin-right: 100px">
	        	<a class="mini-button" style="display: none" id="search_btn" onclick="query(10,0)">查询</a>
<%--	            <a class="mini-button" style="display: none"  id="sync_btn"  onclick="sync()">导入同业数据</a>--%>
	            <a id="clear_btn" class="mini-button" style="display: none" onclick="clear()">清空</a>
<%--	            <a id="export_btn" class="mini-button" style="display: none"  onclick="exportData()">导出</a>--%>
<%--	            <a id="search_btn" class="mini-button" style="display: none"  onclick="searchData()">查看明细</a>--%>
	        </span>
        </div>
    </div>
</fieldset>

<span style="margin: 2px; display: block;"> 
	<a id="add_btn" class="mini-button" style="display: none" onclick="add()">新增</a>
	<a id="edit_btn" class="mini-button" style="display: none" onclick="edit()">修改</a>
	<a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>
<%--	<a  id="import_btn" class="mini-button" style="display: none"    onclick="ExcelImport()">导入</a>
	<a  id="export_btn" class="mini-button" style="display: none"    onclick="ExcelExport()">导出模板</a>--%>
</span>

<div class="mini-fit" style="width:100%;height:100%;">
    <div id="grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client"
         allowAlternating="true"
         idField="userId" onrowdblclick="onRowDblClick" onshowrowdetail="onShowRowDetail">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center">序号</div>
            <div type="expandcolumn">操作</div>
            <div field="coreClntNo" headerAlign="center" allowSort="true" width="80px" align="center">客户编号</div>
            <div field="conm" headerAlign="center" width="100px" align="center">客户英文名称</div>
            <div field="branchCode" headerAlign="center" width="80px" visible="false" align="center">机构码</div>
            <div field="branchName" headerAlign="center" width="150px" allowSort="true" align="center">客户中文名称</div>
            <div field="bussType" headerAlign="center" width="80px" visible="false" align="center">业务类型</div>
            <div field="bussTpnm" headerAlign="center" width="100px" visible="false" align="center">业务类型名称</div>
            <div field="dealType" width="80" headerAlign="center" visible="false" align="center"
                 renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'dealType2'}">交易类型
            </div>
            <%--    <div field="inclSubFlg" width="80" headerAlign="center" visible="false" align="center" renderer="CommonUtil.dictRenderer"
                    data-options="{dict:'inclflag'}">包含下级标志</div>--%>
            <div field="approvalDate" width="100" headerAlign="center" visible="false" align="center"
                 renderer="onDateRenderer">审批日期
            </div>

        </div>
    </div>
</div>


<div id="detailGrid_Form" style="display:none;">
    <div id="detail_grid" class="mini-datagrid" style="width:100%;height:400px;" allowResize="true" sortMode="client"
         allowAlternating="true"
    >
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center">序号</div>
            <div field="bussTpnm" width="120px" align="center" headerAlign="center">业务类型名称</div>
            <div field="bussType" headerAlign="center" width="80px" align="center">业务类型</div>
            <div field="dealType" width="80" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'dealType2'}">交易类型
            </div>
            <%-- <div field="inclSubFlg" width="80" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'inclflag'}">包含下级标志</div>--%>
            <div field="approvalDate" width="120px" align="center" headerAlign="center" renderer="onDateRenderer">审批日期
            </div>
        </div>
    </div>
</div>


</body>

<script>
    mini.parse();

    var form = new mini.Form("#search_form");
    var grid = mini.get("grid");
    var detail_grid = mini.get("detail_grid");
    var detailGrid_Form = document.getElementById("detailGrid_Form");


    function onShowRowDetail(e) {
        var grid = e.sender;
        var row = e.record;
        var td = grid.getRowDetailCellEl(row);
        td.appendChild(detailGrid_Form);
        detailGrid_Form.style.display = "block";
        searchDetail(10, 0, row);
    }

    function searchDetail(pageSize, pageIndex, row) {
        var data = {};

        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;

//        var coreClntNo = mini.get("productCode").getValue();
        data['coreClntNo'] = row.coreClntNo;

        var param = mini.encode(data);

        CommonUtil.ajax({
            url: "/IfsCustLimitController/searchAllBussTpnm",
            data: param,
            callback: function (data) {

                //设置分页
                detail_grid.setTotalCount(data.obj.total);
                detail_grid.setPageIndex(pageIndex);
                detail_grid.setPageSize(pageSize);
                //设置数据
                detail_grid.setData(data.obj.rows);

            }
        });
    }


    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    detail_grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        var row = grid.getSelected();
        searchDetail(pageSize, pageIndex, row);
    });


    // 初始化打开页面时就查询
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    /* 查询所有 */
    function search(pageSize, pageIndex) {
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
//		data['cno']= mini.get("cno").getValue();
//		data['sn']= mini.get("sn").getValue();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsCustLimitController/searchAllBuss",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    // 清空(查询所有)
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    // 新增
    function add() {
        var url = CommonUtil.baseWebPath() + "/../Common/limit/entryInterfaceEdit.jsp?action=add";
        var tab = {
            id: "entryInterfaceAdd", name: "entryInterfaceAdd", title: "客户准入新增", url: url,
            showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name
        };
        var paramData = {selectData: ""};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    // 修改
    function edit() {

        var row = detail_grid.getSelecteds();
        if (row[0]) {
            var approvalDate = new Date(row[0].approvalDate);

            row[0].approvalDate = approvalDate;
            var url = CommonUtil.baseWebPath() + "/../Common/limit/entryInterfaceEdit.jsp?action=edit";
            var tab = {
                id: "entryInterfaceEdit", name: "entryInterfaceEdit", url: url, title: "客户准入修改",
                parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
            };
            var paramData = {selectData: row[0]};
            CommonUtil.openNewMenuTab(tab, paramData);
        } else {
            mini.alert("请展开请选中一行业务类型再进行修改", "提示");
        }
    }

    // 删除
    function del() {
        var rows = grid.getSelecteds();
        var rows2 = detail_grid.getSelecteds();
        if (rows2.length == 0) {
            mini.alert("请展开请选中一行业务类型再进行删除", "提示");
            return;
        }
        mini.confirm("您确认要删除该客户准入信息?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows[0];
                var data2 = rows2[0]
                data['bussTpnm'] = data2.bussTpnm;
                data['inclSubFlg'] = data2.inclSubFlg;
                var value = new Date(data.approvalDate);
                if (value) data['approvalDate'] = mini.formatDate(value, 'yyyy-MM-dd');
                params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsCustLimitController/deleteBussInfo",
                    data: params,
                    callback: function (data) {
                        mini.alert("删除成功.", "系统提示");
                        search(10, 0);
                    }
                });
            }
        });
    }

    //查看详情
    function onRowDblClick(e) {
        var row = grid.getSelected();
        var approvalDate = new Date(row.approvalDate);
        row.approvalDate = approvalDate;
        if (row) {
            var url = CommonUtil.baseWebPath() + "/../Common/limit/entryInterfaceEdit.jsp?action=detail";
            var tab = {
                id: "entryInterfaceDetail", name: "entryInterfaceDetail", url: url, title: "客户准入详情",
                parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
            };
            var paramData = {selectData: grid.getSelected()};
            CommonUtil.openNewMenuTab(tab, paramData);
        }
    }

    /*	//客户准入信息导入
        function ExcelImport(){
            mini.open({
                showModal: true,
                allowResize: true,
                url: CommonUtil.baseWebPath() +"/../Common/limit/entryInterfaceImport.jsp",
                width: 420,
                height: 300,
                title: "Excel导入",
                ondestroy: function (action) {
                    search(10,0);//重新加载页面
                }
            });
        }*/

    // 查询本地数据
    function query(pageSize, pageIndex) {
        var coreClntNo = mini.get("coreClntNo").getValue();
        var branchName = mini.get("branchName").getValue();

        CommonUtil.ajax({
            url: "/IfsCustLimitController/searchAllBuss",
            data: {coreClntNo: coreClntNo, branchName: branchName},
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    // 查询同业数据
    function sync() {
        var cno = mini.get("cno").getValue();
        var sn = mini.get("sn").getValue();
        if (cno == "" && sn == "") {
            mini.alert("客户编号和客户英文名称选输其一");
            return;
        }
        if (cno != "" && sn != "") {
            mini.alert("客户编号和客户英文名称选输其一");
            return;
        }
        CommonUtil.ajax({
            url: "/IfsLimitController/searchBussInfo",
            data: {cno: cno, branchName: branchName},
            callback: function (data) {
                if (data.obj.length <= 1) {
                    grid.setData("");
                    mini.alert("无此客户准入信息");
                } else {
                    query(10, 0);
                }
            }
        });
    }

    //导出
    function exportData() {
        if (grid.totalCount == "0") {
            mini.alert("表中无数据");
            return;
        }
        mini.confirm("您确认要导出Excel吗?", "系统提示",
            function (action) {
                if (action == "ok") {
                    var form = new mini.Form("#search_form");
                    var data = form.getData(true);
                    var fields = null;
                    for (var id in data) {
                        fields += '<input type="hidden" id="' + id + '" name="' + id + '" value="' + data[id] + '">';
                    }
                    var urls = CommonUtil.pPath + "/sl/IfsLimitController/exportExcel";
                    $('<form action="' + urls + '" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }

    /*//导出模板
    function ExcelExport() {
//        if(grid.totalCount=="0"){
//            mini.alert("表中无数据");
//            return;
//        }
        mini.confirm("您确认要导出Excel吗?","系统提示", 
            function (action) {
                if (action == "ok"){
                    var form = new mini.Form("#search_form");
                    var data = form.getData(true);
                    var fields = null;
                    for(var id in data){
                        fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
                    }
                    var urls = CommonUtil.pPath + "/sl/IfsLimitController/templateExcel";
                    $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
                }
            }
        );
    }
*/

    //查看明细
    function searchData() {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath()
                + "../../Common/limit/entryInterfaceDetail.jsp",
            title: "客户准入明细",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.creditId);
                        btnEdit.setText(data.creditId);
                        mini.get("creditName").setValue(data.creditName);

                        btnEdit.focus();
                    }
                }
            }
        });
    }
</script>
</html>