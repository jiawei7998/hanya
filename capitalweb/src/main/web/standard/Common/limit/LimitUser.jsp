<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>审批成员新增</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset id="fd2" class="mini-fieldset">
    <legend><label>查询条件</label></legend>
    <div id="find" class="fieldset-body">
        <div id="search_form"   >

            <input id="userId" name="userId" labelField="true" label="用户代码："
                   class="mini-textbox" emptyText="请输入用户代码"labelStyle="text-align:right;" />
            <input id="isActive" value="1" class="mini-hidden" />

            <input id="userName" name="userName" labelField="true" label="用户名称："
                   class="mini-textbox" emptyText="请输入用户名称" labelStyle="text-align:right;"/>
            <input id="instIds" name="instIds" class="mini-buttonedit"
                   onbuttonclick="onInsQuery" labelField="true"  label="所属机构："
                   emptyText="请选择所属机构" labelStyle="text-align:right;"  allowInput="false"/>


        </div>

        <div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;">
                        <a id="search_btn" class="mini-button" onClick="searchBtn">查询</a>
                        <a id="clear_btn" class="mini-button" onClick="clear">清空</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</fieldset>

<div class="mini-fit" style="margin-top: 2px;">
    <div id="user_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client"
         allowAlternating="true" idField="userId" onrowdblclick="onRowDblClick" border="true"
         allowResize="true">
        <div property="columns" >
            <div type="indexcolumn" headerAlign="center"  align="center">序号</div>
            <div field="userId" headerAlign="center"  align="center" width="80px" allowSort="true">用户代码</div>
            <div field="userName" headerAlign="center"  align="center" width="80px">用户名称</div>
            <div field="instName" headerAlign="center"  align="center" width="120px">机构名称</div>
            <div field="isActive"  headerAlign="center"  align="center" width="80px" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否启用</div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var grid =mini.get("user_grid");
    $(document).ready(function(){


        var url = window.location.search;
        var instId = CommonUtil.getParam(url,"instId");
        var instIdName = CommonUtil.getParam(url,"instIdName");

        if(instId!=null&&instId!=''&&instId!='undefined'){
            mini.get("instIds").setValue(instId);
            mini.get("instIds").setText(instIdName);
        }


        grid.on("beforeload",function(e){
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize,pageIndex);
        });

        search(grid.pageSize,0);
    });

    //所属机构
    function onInsQuery(e) {
        var btnEdit = this;
        mini.open({
            url : CommonUtil.baseWebPath() +"/../Common/MiniInstitutionSelectManages.jsp",
            title : "机构选择",
            width : 900,
            height : 600,
            ondestroy : function(action) {
                if (action == "ok")
                {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.instId);
                        btnEdit.setText(data.instName);
                        btnEdit.focus();
                    }


                }

            }
        });
    }

    //双击行选择
    function onRowDblClick() {
        onOk();
    }

    function onOk() {
        CloseWindow("ok");
    }

    function searchBtn(){
        search(10,0);
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    //清空
    function clear(){
        var instId = mini.get("instIds").getValue()
        var instIdName = mini.get("instIds").getText()
        var grid =mini.get("user_grid");
        var form =new mini.Form("search_form");
        form.clear();
        mini.get("instIds").setValue(instId);
        mini.get("instIds").setText(instIdName);
        search(grid.pageSize,0);
    }


    //查询(带分页)
    function search(pageSize,pageIndex){
        var form =new mini.Form("search_form");
        form.validate();
        if (form.isValid() == false) return;//表单验证
        var data =form.getData();//获取表单数据
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['is_active']=1;
        data['branchId']= top["win"].branchId;
        var param = mini.encode(data); //序列化成JSON

        CommonUtil.ajax({
            url:"/UserController/selectUserWithInstIdsPage",
            data:param,
            callback:function(data){

                var grid =mini.get("user_grid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
            }
        });
    }

    function SetData(data) {
        search(10,0);
    }


    function GetData() {
        var row = grid.getSelected();
        return row;
    }

    function onCancel(e) {
        window.CloseOwnerWindow();
    }

</script>

</body>
</html>