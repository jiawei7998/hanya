<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户黑名单管理</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>客户黑名单管理</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">        
	        <input id="ecifno" name="ecifno" class="mini-textbox" labelField="true" label="ECIF编号：" labelStyle="text-align:right;"
	        	width="330px" emptyText="请输入ECIF编号"/>
	       	<input id="ecifname" name="ecifname" class="mini-textbox" labelField="true" label="客户名称：" labelStyle="text-align:right;"
	        	width="330px" emptyText="请输入客户名称" />
	        <span style="float:right;margin-right: 100px">
	        	<a class="mini-button" style="display: none"  id="search_btn"  onclick="query()">查询</a>
	            <a class="mini-button" style="display: none"  id="sync_btn"  onclick="sync()">同业数据同步</a>
	            <a id="clear_btn" class="mini-button" style="display: none"  onclick="clear()">清空</a>
	            <a id="export_btn" class="mini-button" style="display: none"  onclick="exportData()">导出</a>
	        </span>
	    </div>
	</div>
</fieldset> 
    
<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="import_btn" class="mini-button" style="display: none"    onclick="ExcelImport()">导入</a>
</span>

<div class="mini-fit" style="width:100%;height:100%;">
	<div id="grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" allowAlternating="true"
		idField="userId" onrowdblclick="onRowDblClick" >
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center">序号</div>
            <div field="cORE_CLNT_NO" headerAlign="center" allowSort="true" width="80px" align="center">核心客户号</div>
            <div field="cO_NM" headerAlign="center" width="100px" align="center" >客户名称</div>
            <!-- <div field="bUSS_TYPE" headerAlign="center"  width="80px" align="center">业务类型</div> -->
            <div field="bUSS_TP_NM" headerAlign="center"  width="100px" align="center">业务类型名称</div>
            <!-- <div field="bRANCH_CODE" headerAlign="center"  width="80px" align="center">机构码</div> -->
            <div field="bRANCH_NAME" headerAlign="center" width="150px" allowSort="true" align="center">机构名称</div>
            <div field="iNCL_SUB_FLG" width="80" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
            	data-options="{dict:'inclflag'}">包含下级标志</div>
            <div field="aPPROVAL_DATE" width="100" headerAlign="center" align="center" renderer="onDateRenderer">审批日期</div>
        </div>
    </div>
</div>
</body>

<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var grid = mini.get("grid");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	// 初始化打开页面时就查询
	$(document).ready(function() {
		search(10, 0);
    });
	
	/* 查询所有 */
	function search(pageSize,pageIndex){
		var data = form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsLimitController/searchAllBussBk",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	// 清空(查询所有)
	function clear(){
	    var form=new mini.Form("search_form");
	    form.clear();
	    search(10,0);
	}
	
	// 新增
	function add(){
		var url = CommonUtil.baseWebPath() + "/../Common/limit/entryInterfaceBKEdit.jsp?action=add";
		var tab = { id: "entryInterfaceAdd", name: "entryInterfaceAdd", title: "黑名单新增", url: url,
				showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	// 修改
	function edit(){
		var row = grid.getSelected();
		if(row){
			var aPPROVAL_DATE = new Date(row.aPPROVAL_DATE);
        	row.aPPROVAL_DATE = aPPROVAL_DATE;
			var url = CommonUtil.baseWebPath() +"/../Common/limit/entryInterfaceBKEdit.jsp?action=edit";
			var tab = {id: "entryInterfaceEdit",name:"entryInterfaceEdit",url:url,title:"黑名单修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	// 删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除该黑名单信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsLimitController/deleteBussInfoBk",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	//查看详情
	function onRowDblClick(e) {
		var row = grid.getSelected();
	 	var aPPROVAL_DATE = new Date(row.aPPROVAL_DATE);
	 	row.aPPROVAL_DATE = aPPROVAL_DATE;
        if(row){
        	var url = CommonUtil.baseWebPath() +"/../Common/limit/entryInterfaceEdit.jsp?action=detail";
    		var tab = {id: "entryInterfaceDetail",name:"entryInterfaceDetail",url:url,title:"黑名单详情",
    					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
    		var paramData = {selectData:grid.getSelected()};
    		CommonUtil.openNewMenuTab(tab,paramData);
        }
	}
	
	//客户准入信息导入
	function ExcelImport(){
		mini.open({
			showModal: true,
		    allowResize: true,
		    url: CommonUtil.baseWebPath() +"/../Common/limit/entryInterfaceImport.jsp",
			width: 420,
			height: 300,
			title: "Excel导入",
			ondestroy: function (action) {
				search(10,0);//重新加载页面
			}
		});
	}
	
	// 查询本地数据
	function query() {
		var ecifno = mini.get("ecifno").getValue();
		var ecifname = mini.get("ecifname").getValue();
		if(ecifno == "" && ecifname == ""){
			mini.alert("客户号和客户名称选输其一");
            return;
		}
		if(ecifno != "" && ecifname != ""){
			mini.alert("客户号和客户名称选输其一");
            return;
		}
        CommonUtil.ajax({
            url:"/IfsLimitController/searchLocalInfoBk",
            data:{ecifno:ecifno,ecifname:ecifname},
            callback:function(data){
            	grid.setTotalCount(data.length);
				grid.setPageIndex(0);
		        grid.setPageSize(10);
				grid.setData(data);
            }
 		 });
	}
	
	// 查询同业数据
	function sync() {
		/* var ecifno = mini.get("ecifno").getValue();
		var ecifname = mini.get("ecifname").getValue();
		if(ecifno == "" && ecifname == ""){
			mini.alert("客户号和客户名称选输其一");
            return;
		}
		if(ecifno != "" && ecifname != ""){
			mini.alert("客户号和客户名称选输其一");
            return;
		}
        CommonUtil.ajax({
            url:"/IfsLimitController/searchBussInfo",
            data:{ecifno:ecifno},
            callback:function(data){
            	grid.setTotalCount(data.obj.length);
				grid.setPageIndex(0);
		        grid.setPageSize(10);
				grid.setData(data.obj);
            }
 		 }); */
	}
	
	//导出
	function exportData() {
		if(grid.totalCount=="0"){
			mini.alert("表中无数据");
			return;
		}
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsLimitController/exportExcel";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</html>