<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>信用风险审查</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>单笔信用风险审查</legend>
	<div>
		<div id="search_form" style="width:100%" cols="6">        
	        <input id="product" name="product" class="mini-textbox" labelField="true" label="审批件编号："
	        	labelStyle="text-align:center;" width="330px" emptyText="请输入审批件编号" />
     		<input id="prodname" name="prodname" class="mini-textbox" labelField="true" label="产品名称：" labelStyle="text-align:center;"
        		width="330px" emptyText="请输入产品名称" />
	        <span style="float:right;margin-right: 150px">
	        	<a class="mini-button" style="display: none"  id="search_btn" onclick="query()">查询</a>
	            <a class="mini-button" style="display: none"  id="sync_btn"  onclick="sync()">导入同业数据</a>
	            <a class="mini-button" style="display: none"  id="clear_btn" onclick="clear()">清空</a>
	            <a class="mini-button" style="display: none"  id="export_btn" onclick="exportData()">导出</a>
	        </span>
	    </div>
	</div>
</fieldset> 

<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
	<a  id="import_btn" class="mini-button" style="display: none"    onclick="ExcelImport()">导入</a>
</span>

<div class="mini-fit" style="width:100%;height:100%;">
	<div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id" allowAlternating="true"
		allowResize="true" sortMode="client" allowAlternating="true" >
		<div property="columns">
			<div type="indexcolumn" width="30px" headerAlign="center">序号</div>
            <div field="aPPROVE_FL_NO" headerAlign="center" width="100px" align="center">审批件编号</div>
            <div field="oRDER_ID" headerAlign="center" width="100px" align="center">审批单号</div>
            <div field="tRAN_TYPE" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer"
            	data-options="{dict:'trantype'}" width="80px" align="center">交易类型</div>
            <div field="tRAN_STATUS" width="60px" align="center"  headerAlign="center"
     			renderer="CommonUtil.dictRenderer" data-options="{dict:'transtatus'}">交易状态</div>	
            <div field="iNPT_TELLER_NO" headerAlign="center" width="60px" align="center" >经办员</div>
            <div field="pRODUCT_NAME" headerAlign="center" width="100px" align="center" >产品名称</div>
            <div field="yLD_RATE" headerAlign="center" width="50px" allowSort="true" align="center"
            	numberFormat="#,0.00">年化收益率(%)</div>
            <div field="iVTNT_PLN" headerAlign="center"  width="100px" align="center">投资规模</div>
            <div field="pD_EXP_DT" width="80" headerAlign="center" align="center" renderer="onDateRenderer">有效期截止日</div>
		</div>
	</div>
</div>
</body>

<script>
	mini.parse();
	var form = new mini.Form("#search_form");
	var grid = mini.get("prod_grid");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	// 初始化打开页面时就查询
	$(document).ready(function() {
		search(10, 0);
    });
	
	/* 查询所有 */
	function search(pageSize,pageIndex){
		var data = form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsLimitController/searchAllProd",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	// 清空(查询所有)
	function clear(){
	    var form=new mini.Form("search_form");
	    form.clear();
	    search(10,0);
	}
	
	// 新增
	function add(){
		var url = CommonUtil.baseWebPath() + "/../Common/limit/singleLimitEdit.jsp?action=add";
		var tab = { id: "singleLimitAdd", name: "singleLimitAdd", title: "风险审查新增", url: url,
				showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
	}
	
	// 修改
	function edit(){
		var row = grid.getSelected();
		if(row){
			var pD_EXP_DT = new Date(row.pD_EXP_DT);
        	row.pD_EXP_DT = pD_EXP_DT;
			var url = CommonUtil.baseWebPath() +"/../Common/limit/singleLimitEdit.jsp?action=edit";
			var tab = {id: "singleLimitEdit",name:"singleLimitEdit",url:url,title:"风险审查修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
	}
	
	//查看详情
	function onRowDblClick(e) {
		var row = grid.getSelected();
	 	var aPPROVAL_DATE = new Date(row.aPPROVAL_DATE);
	 	row.aPPROVAL_DATE = aPPROVAL_DATE;
        if(row){
        	var url = CommonUtil.baseWebPath() +"/../Common/limit/singleLimitEdit.jsp?action=detail";
    		var tab = {id: "entryInterfaceDetail",name:"entryInterfaceDetail",url:url,title:"风险审查详情",
    					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
    		var paramData = {selectData:grid.getSelected()};
    		CommonUtil.openNewMenuTab(tab,paramData);
        }
	}
	
	// 删除
	function del(){
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		mini.confirm("您确认要删除该客户准入信息?","系统警告",function(value){   
			if (value=='ok'){   
				var data=rows[0];
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/IfsLimitController/deleteSingleLimit",
					data:params,
					callback : function(data) {
						mini.alert("删除成功.","系统提示");
						search(10,0);
					}
				});
			}
		});
	}
	
	// 查询本地数据
	function query() {
		var approveFlNo = mini.get("product").getValue();
		var productname = mini.get("prodname").getValue();
		if(approveFlNo == "" && productname == ""){
			mini.alert("审批件编号和产品名称选输其一");
            return;
		}
		if(approveFlNo != "" && productname != ""){
			mini.alert("审批件编号和产品名称选输其一");
            return;
		}
        CommonUtil.ajax({
            url:"/IfsLimitController/searchLocalData",
            data:{approveFlNo:approveFlNo,productname:productname},
            callback:function(data){
            	grid.setTotalCount(data.length);
				grid.setPageIndex(0);
		        grid.setPageSize(10);
				grid.setData(data);
            }
 		 });
	}
	
	// 查询同业数据
	function sync() {
      	var product = mini.get("product").getValue();
		var prodname = mini.get("prodname").getValue();
		if(product == "" && prodname == ""){
			mini.alert("审批件编号和产品名称必输其一");
            return;
		}
		if(product != "" && prodname != ""){
			mini.alert("审批件编号和产品名称必输其一");
            return;
		}
        CommonUtil.ajax({
            url:"/IfsLimitController/searchProdInfo",
            data:{product:product,prodname:prodname},
            callback:function(data){
            	if(data.obj.length <= 1){
            		grid.setData("");
            		mini.alert("无此审批件信息");
            	} else {
            		query();
            	}
            }
 		 });
	}
	
	//信用风险审查信息导入
	function ExcelImport(){
		mini.open({
			showModal: true,
		    allowResize: true,
		    url: CommonUtil.baseWebPath() +"/../Common/limit/singleLimitImport.jsp",
			width: 420,
			height: 300,
			title: "Excel导入",
			ondestroy: function (action) {
				search(10,0);//重新加载页面
			}
		});
	}
	
	//导出
	function exportData() {
		mini.confirm("您确认要导出Excel吗?","系统提示", 
			function (action) {
				if (action == "ok"){
			 		var form = new mini.Form("#search_form");
					var data = form.getData(true);
					var fields = null;
					for(var id in data){
						fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
					}
					var urls = CommonUtil.pPath + "/sl/IfsLimitController/exportExcelData";                                                                                                                         
					$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();   
				}
			}
		);
	}
</script>
</html>