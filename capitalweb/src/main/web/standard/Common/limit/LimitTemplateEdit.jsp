<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>额度调整维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <legend>外汇限额信息</legend>
            <div class="leftarea">
                <input id="limitType" name="limitType" class="mini-combobox" labelField="true" label="限额类型："  emptyText="请选择限额类型" data="CommonUtil.serverData.dictionary.limitType" required="true"   style="width:100%" labelStyle="align:left;width:130px" onvaluechanged="onCurrencyPairChange"/>
                <input id="prd" name="prd" class="mini-buttonedit" onbuttonclick="onPrdQuery" labelField="true" label="产品：" required="false"  emptyText="请选择产品" style="width:100%" labelStyle="align:left;width:130px" />
                <input id="quotaAmountU" name="quotaAmountU" class="mini-spinner" labelField="true"  label="美元限额下线(美元)：" enabled="true" emptyText="请输入可用金额" maxValue="999999999999999" minValue="-999999999999999" changeOnMousewheel='false' format="n2"   style="width:100%" labelStyle="align:left;width:130px" onvaluechanged="changeUSDToCNY"/>
                <input id="quotaAmount" name="quotaAmount" class="mini-spinner" labelField="true"  label="折人民币下线(元)：" enabled="false" emptyText="请输入可用金额" maxValue="9999999999999999" minValue="-9999999999999999" changeOnMousewheel='false' format="n2"   style="width:100%" labelStyle="align:left;width:130px"/>
                <input id="ccy" name="ccy" class="mini-buttonedit" onbuttonclick="onCcyQuery" labelField="true"  label="币种："  emptyText="请选择币种"  required="false" style="width:100%" labelStyle="align:left;width:130px"/>
                <input id="limitCount" name="limitCount" class="mini-textbox" labelField="true"  label="累计笔数：" enabled="true" emptyText="请输入需要限制的笔数" maxValue="999999999999"  changeOnMousewheel='false' style="width:100%" labelStyle="align:left;width:130px" />
                <input id="ctlType" name="ctlType" class="mini-combobox" labelField="true"  label="检测控制："  emptyText="请选择"  data="CommonUtil.serverData.dictionary.ctlType"  required="true"  style="width:100%" labelStyle="align:left;width:130px"/>
                <input id="ccyCode" name="ccyCode" class="mini-textbox" labelField="true"  label="币种代码："  style="width:100%" labelStyle="align:left;width:130px"/>
                <input id="prdTypeCode" name="prdTypeCode" class="mini-textbox" labelField="true"  label="类型代码："  style="width:100%" labelStyle="align:left;width:130px"/>
                <input id="rate" name="rate" class="mini-textbox" labelField="true"  label="汇率："  style="width:100%" labelStyle="align:left;width:130px"/>
				<input id="direction" name="direction" class="mini-combobox" labelField="true" label="交易方向："  emptyText="请选择交易方向" data="CommonUtil.serverData.dictionary.trading" required="false"  style="width:100%" labelStyle="align:left;width:130px" />
                <input id="dealSource" name="dealSource" class="mini-combobox" labelField="true"  label="交易来源："  emptyText="请选择交易来源"  data="CommonUtil.serverData.dictionary.TradeSource"  required="false" style="width:100%" labelStyle="align:left;width:130px"/>
			</div>			
			<div class="rightarea">
                <input id="tradName" name="tradName" class="mini-buttonedit" onbuttonclick="onPrdTradQuery" labelField="true"  label="用户名："  emptyText="请选择交易员"  required="false" style="width:100%" labelStyle="align:left;width:130px"  />
                <input id="trad" name="trad" class="mini-textbox" labelField="true"  label="用户名ID："  emptyText="请选择交易员"  required="false" style="width:100%" labelStyle="align:left;width:130px"  />
                <input id="prdType" name="prdType" class="mini-buttonedit" onbuttonclick="onPrdTypeQuery" labelField="true"  label="产品类型："  emptyText="请选择产品类型"  required="false" style="width:100%" labelStyle="align:left;width:130px"  />
                <input id="availAmountU" name="availAmountU" class="mini-spinner" labelField="true"  label="美元限额上线(美元)：" emptyText="请输入限额金额" maxValue="999999999999999" minValue="-999999999999999" changeOnMousewheel='false' format="n2"  style="width:100%" labelStyle="align:left;width:130px" onvaluechanged="changeUSDToCNY"/>
                <input id="availAmount" name="availAmount" class="mini-spinner" onvaluechanged="checkAmount" labelField="true" enabled="false" label="折人民币上线(元)：" emptyText="请输入限额金额" maxValue="9999999999999999" changeOnMousewheel='false' format="n2"  style="width:100%" labelStyle="align:left;width:130px" />
                <input id="term" name="term" class="mini-spinner" labelField="true"  label="期限："  emptyText="请输入期限" maxValue="999999999999" minValue="-9999999999999999" changeOnMousewheel='false' decimalplaces="2"   style="width:100%" labelStyle="align:left;width:130px" />
                <input id="costName" name="costName" class="mini-buttonedit" onbuttonclick="loadCostList" labelField="true"  label="成本中心："  emptyText="请选择成本中心"  required="false" style="width:100%" labelStyle="align:left;width:130px"  />
                <input id="cost" name="cost" class="mini-textbox" labelField="true"  label="成本中心："  required="false" style="width:100%" labelStyle="align:left;width:130px"  />
                <input id="prdCode" name="prdCode" class="mini-textbox" labelField="true"  label="产品代码："  style="width:100%" labelStyle="align:left;width:130px"/>
                <input id="limitId" name="limitId" class="mini-textbox" labelField="true"  label="流水号："  style="width:100%" labelStyle="align:left;width:130px"/>
				<input id="totalAmount" name="totalAmount" class="mini-textbox" labelField="true"  label="累计额度: "  style="width:100%" labelStyle="align:left;width:130px"/>
			</div>			
            </fieldset>  
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a></div>
        <div style="margin-bottom:10px; text-align: center;"><a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="close">关闭</a></div>
    </div> 
</div>
<script type="text/javascript">

mini.parse();
//获取当前tab
var currTab = top["win"].tabs.getActiveTab();
var params = currTab.params;
var row=params.selectData;
var url=window.location.search;
var action=CommonUtil.getParam(url,"action");
var form=new mini.Form("#field_form");
mini.get("ccyCode").setVisible(false);
mini.get("prdTypeCode").setVisible(false);
mini.get("prdCode").setVisible(false);
mini.get("limitId").setVisible(false);
mini.get("rate").setVisible(false);
mini.get("direction").setVisible(false);
mini.get("dealSource").setVisible(false);
mini.get("totalAmount").setVisible(false);
mini.get("trad").setVisible(false);
mini.get("cost").setVisible(false);
mini.get("limitCount").setVisible(false);

var rate = searchRate();
$(document).ready(function(){
    initData();
}); 



//初始化数据
function initData(){
    if($.inArray(action,["edit","detail"])>-1){//修改、详情
        form.setData(row);
        mini.get("ccy").setText(row.ccy);
        mini.get("prd").setText(row.prd);
        mini.get("prdType").setText(row.prdType);
        mini.get("tradName").setValue(row.tradName);
        mini.get("tradName").setText(row.tradName);
        mini.get("costName").setValue(row.costName);
        mini.get("costName").setText(row.costName);
        mini.get("limitCount").setValue(row.limitCount);
        onCurrencyPairChange();
        if(action == "detail"){
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>外汇限额</a> >> 详情");
            form.setEnabled(false);
            mini.get("save_btn").setVisible(false);
        }
    }
}
//初始化限额条件
function initCondition(e){
    CommonUtil.ajax({
        url:"/IfsFieldLimitController/searchLimitFieldAndDict",
        data:{prd:e.value},
        callback:function(data){
            mini.get("limitCondition").setValue(data.desc);
            
        }
    }); 
}

//保存
function save(){
    //表单验证！！！
    form.validate();
    if (form.isValid() == false) {
        mini.alert("表单填写有误!","提示");
        return;
    }
    var saveUrl=null;
    if(action == "add"){
        saveUrl = "/IfsLimitController/LimitTemplateAdd";
    }else if(action == "edit"){
        saveUrl = "/IfsLimitController/LimitTemplateEdit";
    }

    var data=form.getData(true);
    var params=mini.encode(data);
    CommonUtil.ajax({
        url:saveUrl,
        data:params,
        callback:function(data){
            mini.alert("保存成功",'提示信息',function(){
                top["win"].closeMenuTab();
            });
        }
    });
}

function close(){
	top["win"].closeMenuTab();
}

//取消
function cancel(){
    top["win"].closeMenuTab();
}

function onButtonEdit(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/MiniUserManagesMini.jsp",
        title: "选择限额交易员列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if (data) {
                    btnEdit.setValue(data.userId);
                    btnEdit.setText(data.userName);
                    btnEdit.focus();
                }
            }
        }
    });

}
//选择产品
function onPrdQuery(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplatePrd.jsp",
        title: "选择产品列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if( data== undefined){return;}
    	    	var limitTemplate = "";
    	    	var limitTemplateCode = "";
                if (data) {
                	for(var i = 0; i <  data.length; i++){
    	    			if(i ==  data.length - 1){
    	    				limitTemplate += data[i].pdesccn;
    	    				limitTemplateCode += data[i].pcode+",";
    	    			}else{
    	    				limitTemplate += data[i].pdesccn+ ",";
    	    				limitTemplateCode += data[i].pcode+ ",";
    	    			}
    	    			
    	    		}
                }
                //limitTemplate ="{"+limitTemplate+"}";
   	    	 	btnEdit.setValue(limitTemplateCode);//产品中文描述
                btnEdit.setText(limitTemplateCode);
                mini.get("prdCode").setValue(limitTemplateCode);//产品代码
                btnEdit.focus();
            }
        }
    });
}

//选择产品类型
function onPrdTypeQuery(e) {
    var btnEdit = this;
    var prd =mini.get("prdCode").getValue();
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateType.jsp?action="+prd,
        title: "选择产品类型列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if( data== undefined){return;}
    	    	var limitTemplate = "";
    	    	var limitTemplateCode = "";
                if (data) {
                	for(var i = 0; i <  data.length; i++){
    	    			if(i ==  data.length - 1){
    	    				limitTemplate += data[i].type+",";
    	    				limitTemplateCode += data[i].prodcode+",";
    	    			}else{
    	    				limitTemplate += data[i].type+ ",";
    	    				limitTemplateCode += data[i].prodcode+ ",";
    	    			}
    	    			
    	    		}
                }
                mini.get("prdTypeCode").setValue(limitTemplateCode);
                //limitTemplate ="{"+limitTemplate+"}";
   	    	 	btnEdit.setValue(limitTemplate);
                btnEdit.setText(limitTemplate);
                btnEdit.focus();
            }
        }
    });
}

//选择币种
function onCcyQuery(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateCcy.jsp",
        title: "选择币种列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if( data== undefined){return;}
    	    	var limitTemplate = "";
    	    	var limitTemplateCode = "";
                if (data) {
                	for(var i = 0; i <  data.length; i++){
    	    			if(i ==  data.length - 1){
    	    				//limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
    	    				limitTemplate += data[i].dictValue;
    	    				limitTemplateCode += data[i].dictKey+",";
    	    			}else{
    	    				//limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
    	    				limitTemplate += data[i].dictValue+ ",";
    	    				limitTemplateCode += data[i].dictKey+ ",";
    	    			}
    	    			
    	    		}
                }
                //limitTemplate ="{"+limitTemplate+"}";
   	    	 	btnEdit.setValue(limitTemplate);
                btnEdit.setText(limitTemplate);
                mini.get("ccyCode").setValue(limitTemplateCode);
                btnEdit.focus();
            }
        }
    });
}

//交易员信息
function onPrdTradQuery(){
	 var btnEdit = this;
	    mini.open({
	    	url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateTrad.jsp",
	        title: "选择交易员信息列表",
	        width: 900,
	        height: 600,
	        ondestroy: function (action) {
	            if (action == "ok") {
	                var iframe = this.getIFrameEl();
	                var data = iframe.contentWindow.GetData();
	                data = mini.clone(data);    //必须
	                if( data== undefined){return;}
	    	    	var limitTemplate = "";
	    	    	var limitTemplateCode = "";
	                if (data) {
	                	for(var i = 0; i <  data.length; i++){
	    	    			if(i ==  data.length - 1){
	    	    				//limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
	    	    				limitTemplate += data[i].dictValue+",";
	    	    				limitTemplateCode += data[i].dictKey+",";
	    	    			}else{
	    	    				//limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
	    	    				limitTemplate += data[i].dictValue+ ",";
	    	    				limitTemplateCode += data[i].dictKey+ ",";
	    	    			}
	    	    			
	    	    		}
	                }
	                //limitTemplate ="{"+limitTemplate+"}";
	   	    	 	btnEdit.setValue(limitTemplate);
	                btnEdit.setText(limitTemplate);
	                mini.get("trad").setValue(limitTemplateCode);
	                btnEdit.focus();
	            }
	        }
	    });
}


//
function onCostQuery(e) {
	var btnEdit = this;
    mini.open({
    	url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateCost.jsp",
        title: "选择产品中心列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
        	if (action == "ok") {
        		
        	}
        }
    });
}

//加载成本中心列表
function loadCostList(){
    var btnEdit = this;
    var prd =mini.get("prdCode").getValue();
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/limit/LimitTemplateCost.jsp",
        title: "选择成本中心列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if( data== undefined){return;}
    	    	var limitTemplate = "";
    	    	var limitTemplateCode = "";
    	    	if (data) {
                	for(var i = 0; i <  data.length; i++){
    	    			if(i ==  data.length - 1){
    	    				//limitTemplate += data[i].dictValue+"||"+data[i].dictKey;
    	    				limitTemplate += data[i].dictValue+",";
    	    				limitTemplateCode += data[i].dictKey+",";
    	    			}else{
    	    				//limitTemplate += data[i].dictValue+"||"+data[i].dictKey+ ",";
    	    				limitTemplate += data[i].dictValue+ ",";
    	    				limitTemplateCode += data[i].dictKey+ ",";
    	    			}
    	    			
    	    		}
                }
                //limitTemplate ="{"+limitTemplate+"}";
   	    	 	btnEdit.setValue(limitTemplate);
                btnEdit.setText(limitTemplate);
                mini.get("cost").setValue(limitTemplateCode);
                btnEdit.focus();
            }
        }
    });
}

//校验上限数据比下限高
function checkAmount(){
	//下限
	var quotaAmount = mini.get("quotaAmount").getValue();
	//上限
	var availAmount = mini.get("availAmount").getValue();
	if(quotaAmount>availAmount){
		 mini.alert("额度上限低于额度下限,请核实","提示");
		 mini.get("availAmount").setValue("0.00");
		 mini.get("quotaAmount").setValue("0.00");
	}
}
function onCurrencyPairChange(e){
	var limitType = mini.get("limitType").getValue();
	/* if(limitType=='1'){//单笔
		mini.get("availAmount").setValue("0");
		mini.get("quotaAmountU").setEnabled(false);
	}else{//累计
		mini.get("quotaAmountU").setEnabled(true);
	} */
	if(limitType=='4'){
		mini.get("limitCount").setVisible(true);
	}else{
		mini.get("limitCount").setVisible(false);
	}
}
function searchRate(e){
	CommonUtil.ajax({
        data:{},
    	url:'/IfsLimitController/searchRate',
        callback:function(data){
            mini.get("rate").setValue(data);
        }
    });	
}

function changeUSDToCNY(e){
	var availAmountU = mini.get("availAmountU").getValue();
	var quotaAmountU = mini.get("quotaAmountU").getValue();
	var rate = mini.get("rate").getValue();
	if(availAmountU!=null && availAmountU!="" && rate!=null && rate!="" && Number(availAmountU)!=0){
		var availAmount=Number(availAmountU)*Number(rate);
		mini.get("availAmount").setValue(availAmount);
	}
	if(quotaAmountU!=null && quotaAmountU!="" && rate!=null && rate!="" && Number(quotaAmountU)!=0){
		var quotaAmount=Number(quotaAmountU)*Number(rate);
		mini.get("quotaAmount").setValue(quotaAmount);
	}
	if(Number(availAmountU)!=0 && availAmountU < quotaAmountU){
		 mini.alert("限额上线要大于限额下线!","提示");
	        return;
	}
}

</script>
</body>
</html>