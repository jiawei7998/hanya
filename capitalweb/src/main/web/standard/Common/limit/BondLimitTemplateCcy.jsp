<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>币种页面</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>条件查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="colum" name="colum" class="mini-textbox" labelField="true"  label="币种："    width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入限额字段" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<span style="margin: 2px; display: block;"> 
	<a  id="save_btn" class="mini-button" style="display: none"   onclick="save">保存</a>
	
</span>   
<div class="mini-fit">
		<div id="limit_grid" class="mini-datagrid "  style="width:100%;height:100%;" idField="id" allowResize="true"	multiSelect="true"	   >
			<div property="columns">
			 <div type="checkcolumn" ></div> 
			  	<div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
			  	<div field="dictKey" width="120" align="center" headerAlign="center"  >产品代码</div>
				<div field="dictValue" width="180px" align="center"  headerAlign="center" >产品类型代码</div>descrcn
				<div field="descrcn" width="180px" align="center"  headerAlign="center" >产品中文描述</div>   
			</div>   
		</div>
	</div>  
<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("limit_grid");
	var row="";
	var action = CommonUtil.getParam(url,"action");
	
    
	grid.on("beforeload", function (e) {
		//隐藏字典具体值
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	$(document).ready(function(){
	    search(10,0,action); 
	});

	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	
	/* 按钮 查询事件 */
	
	function query(){
		search(grid.pageSize,0,action);
	} 

	/* 查询 */
	function search(pageSize,pageIndex,action){		
		form.validate();
		//数据验证	
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['type']=action;
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsFieldLimitController/searchCcyInfo",
			data:params,
   			callback : function(data) {
				var grid=mini.get("limit_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	  function GetData() {
          var grid1= mini.get("limit_grid");
          var row1=grid1.getSelecteds();
          return row1;
      }

	 function save(){
    	 onOk();
    }
	 
    function onOk() {
        CloseWindow("ok");
    }
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }
	
</script>
</body>

</html>
