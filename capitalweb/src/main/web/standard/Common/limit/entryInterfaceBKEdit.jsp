<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户准入维护</title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <legend>黑名单信息</legend>
            <div class="leftarea">
                <input id="cORE_CLNT_NO" name="cORE_CLNT_NO" class="mini-textbox" labelField="true" label="核心客户号：" style="width:100%"
                	emptyText="请输入核心客户号" required="true"  labelStyle="text-align:center;width:150px" onvaluechanged="checkCoreNo"/>
               	<input id="bUSS_TP_NM" name="bUSS_TP_NM" class="mini-combobox" labelField="true" label="业务类型名称：" multiSelect="true" 
               		data="CommonUtil.serverData.dictionary.bussTpNm" emptyText="请输入业务类型名称" required="true"  style="width:100%"
               		labelStyle="text-align:center;width:150px" allowInput="true" onvalidation="onComboValidation"/>
                <input id="iNCL_SUB_FLG" name="iNCL_SUB_FLG" class="mini-combobox" label="包含下级标志：" emptyText="请选择包含下级标志"
               		labelField="true" required="true"  style="width:100%" data="CommonUtil.serverData.dictionary.inclflag"
               		labelStyle="text-align:center;width:150px"/>
			</div>			
			<div class="rightarea">
                <input id="cO_NM" name="cO_NM" class="mini-textbox" labelField="true" label="客户名称：" style="width:100%" enabled="false"
                	onvalidation="onComboValidation" emptyText="请输入客户名称" labelStyle="text-align:center;width:150px" />
                <input id="bRANCH_NAME" name="bRANCH_NAME" class="mini-textbox" labelField="true" label="机构名称：" required="true"
                	emptyText="请输入机构名称" style="width:100%" labelStyle="text-align:center;width:150px" />
                <input id="aPPROVAL_DATE" name="aPPROVAL_DATE" class="mini-datepicker" label="审批日期：" emptyText="请选择审批日期" required="true"
					labelField="true" style="width:100%" labelStyle="text-align:center;width:150px"  renderer="onDateRenderer"/>
			</div>			
            </fieldset>  
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>
    </div> 
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row = params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url,"action"); // 增删改
	var form = new mini.Form("#field_form");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	$(document).ready(function(){
	    initData();
	}); 
	
	//初始化数据
	function initData(){
		/* $(document).ready(function(){
    		queryCustNm();
    	}); */
	    if($.inArray(action,["edit","detail"])>-1){//修改、详情
	    	mini.get("cORE_CLNT_NO").setEnabled(false); // 客户号不允许修改
	    	mini.get("bUSS_TP_NM").setEnabled(false); // 业务类型名称不允许修改
	        form.setData(row);
	        if(action == "detail"){
	            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>客户准入</a> >> 详情");
	            form.setEnabled(false);
	            mini.get("save_btn").setVisible(false);
	        }
	    }
	}
	
	//查询客户名称
	/* function queryCustNm() {
		CommonUtil.ajax({
    		url:"/IfsLimitController/queryCustNm",
    		data:{},
    		callback:function(data){
   				var length = data.length;
               	var jsonData = new Array();
   			   	for(var i=0;i<length;i++){
   			   		if(data[i].cliname != null){
   			   			var tyId = data[i].cliname;
						var tyValue = data[i].cliname;
 			   			jsonData.add({'typeId':tyId,'typeValue':tyValue});
   			   		}
   			   	};
   			   	mini.get("cO_NM").setData(jsonData);
    		}
    	});
	} */
	
	//保存
	function save(){
	    //表单验证！！！
	    form.validate();
	    if (form.isValid() == false) {
	        mini.alert("表单填写有误!","提示");
	        return;
	    }
	    var saveUrl = null;
	    if(action == "add"){
	        saveUrl = "/IfsLimitController/bussInfoAddBk";
	    }else if(action == "edit"){
	        saveUrl = "/IfsLimitController/bussInfoEditBk";
	    }
	    var data = form.getData(true);
	    data['bUSS_TYPE']=mini.get('bUSS_TP_NM').getValue();
	    data['bUSS_TP_NM']=mini.get('bUSS_TP_NM').getText();
	    data['cO_NM']=mini.get('cO_NM').getText();
	    var params = mini.encode(data);
	    CommonUtil.ajax({
	        url:saveUrl,
	        data:params,
	        callback:function(data){
	            mini.alert("保存成功",'提示信息',function(){
	                top["win"].closeMenuTab();
	            });
	        }
	    });
	}
	
	function close(){
		top["win"].closeMenuTab();
	}
	
	// 检查该核心客户号是否存在已经根据客户号查询客户名称
    function checkCoreNo(){
    	var ecifno = mini.get("cORE_CLNT_NO").getValue();
    	CommonUtil.ajax({
    		url:"/IfsLimitController/searchBuss",
    		data:ecifno,
    		callback:function(data){
    			if(data.name == null){ // 查询不到客户信息
    				mini.alert("无此客户");
    				mini.get("cORE_CLNT_NO").setValue("");
    				return;
    			}
    			/* if(data.flag == "1"){ // 表中有此客户号
 					mini.alert("此核心客户号'"+ecifno+"'已存在!请重新填写!",'提示信息',function(){
 						mini.get("cORE_CLNT_NO").setValue("");
 					});
     			} else {
     				mini.get("cO_NM").setValue(data.name);
     			} */
    			mini.get("cO_NM").setValue(data.name);
    		}
    	});
    }
	
	// 根据客户号查询客户名称
	function queryName(){
    	
	}
	
 	// 新增时下拉值校验
    function onComboValidation(e) {
 		if($.inArray(action,["edit","detail"])==-1){
 			var items = this.findItems(e.value);
 	        if (!items || items.length == 0) {
 	            e.isValid = false;
 	            e.errorText = "输入值不在下拉数据中";
 	        }
 		}
    }
</script>
</body>
</html>