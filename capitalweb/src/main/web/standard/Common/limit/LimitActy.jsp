<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>opics系统会计分类参数表</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript">
    </script>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset title">
    <legend>opics系统会计类型信息查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="acctngtype" name="acctngtype" class="mini-textbox" labelField="true" label="会计类型："
               labelStyle="text-align:right;" emptyText="请输入会计类型" />
        <input id="type1" name="type1"  class="mini-hidden"/>
        <input id="type2" name="type2"  class="mini-hidden"/>
        <span style="float:right;margin-right: 150px">
            <a class="mini-button"   id="search_btn"  onclick="search()">查询</a>
            <a class="mini-button"   id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
</fieldset>
<span style="margin: 2px; display: block;">
	<a  id="save_btn" class="mini-button"  onclick="save">保存</a>

</span>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="prod_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" idField="userId"
         onrowdblclick="onRowDblClick" border="true" allowResize="true" multiSelect="true">
        <div property="columns">
            <div type="checkcolumn" ></div>
            <div type="indexcolumn" headerAlign="center" align="center" width="40px">序列</div>
            <div field="acctngtype" headerAlign="center"  align="center" width="180px" >会计类型</div>
            <div field="acctdesc" headerAlign="center"  align="center" width="180px" >
                会计类型描述
            </div>
            <div field="acctdesccn" headerAlign="center" width="120px">会计类型中文描述</div>
            <div field="type" headerAlign="center" width="120px" renderer="CommonUtil.dictRenderer" data-options="{dict:'actyType'}" >会计归属类型</div>
            <div field="status" headerAlign="center" width="120px"
                 renderer="CommonUtil.dictRenderer" data-options="{dict:'DealStatus'}"
                 align="center">处理状态
            </div>
            <div field="operator" headerAlign="center"  width="120px" align="center">操作人</div>
            <div field="remark" headerAlign="center"  width="200px">备注</div>
            <div field="lstmntdate" headerAlign="center"  align="center" width="160px" renderer="onDateRenderer">最后维护时间</div>
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();

    var grid = mini.get("prod_grid");

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    /*  grid.on("select",function(e){
         var row=e.record;
         if(row.status == "0"){//未处理
             mini.get("sync").setEnabled(true);
             mini.get("modify").setEnabled(true);
             mini.get("delete").setEnabled(true);
             mini.get("verify").setEnabled(true);
         }
         if(row.status == "1"){//已处理
             mini.get("sync").setEnabled(false);
             mini.get("modify").setEnabled(false);
             mini.get("delete").setEnabled(false);
             mini.get("verify").setEnabled(false);
         }
     });  此处不需要*/

    $(document).ready(function() {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {

        });
	});

    function SetData(data) {
        mini.get("type1").setValue(data.type1);
        mini.get("type2").setValue(data.type2);
        search();

    }

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    //查询IFS_OPICS_ACTY表中的数据
    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }
        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;
        data['status']="1";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url : "/IfsOpicsActyController/searchPageActy",
            data : params,
            callback : function(data) {
                var grid = mini.get("prod_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear(){
        /* var form=new mini.Form("search_form");
        form.clear(); 注掉原因：不全部清空，不要清空隐藏值 type1 type2 ，因为两者存有从大页面传过来的初始值*/
        mini.get("acctngtype").setValue("");
        search();
    }

    function GetData() {
        var row = grid.getSelecteds();
        return row;
    }

    function save(){
        onOk();
    }


    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
</script>
</html>