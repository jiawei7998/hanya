<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title>外汇限额管理</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>外汇限额信息</legend>
    <div id="search_form">        
        <input id="product" name="product" class="mini-textbox" labelField="true" label="产品：" labelStyle="text-align:right;" width="300px" emptyText="请输入产品" />
        <input id="type" name="type" class="mini-combobox" labelField="true" label="限额类型：" data="CommonUtil.serverData.dictionary.limitType" width="300px" labelStyle="text-align:right;" emptyText="请选择限额类型" />
        <input id="trader" name="trader" class="mini-buttonedit" onbuttonclick="onButtonEdit" emptyText="请选择交易员" labelField="true" allowInput="false" width="300px" label="交易员：" labelStyle="text-align:right;" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del">删除</a>
    </span>
    
<div class="mini-splitter" vertical="true" style="width:100%;height:100%;" handlerSize="2px" allowResize="false">
    <div size="40%" >
	    <div class="mini-fit" style="width:100%;height:100%;">
	        <div  id="grid"  class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client"  allowAlternating="true"   
	        idField="userId" onrowdblclick="onRowDblClick" onselectionchanged="onSelectionChanged">
	            <div property="columns">
	                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
	                <div field="limitId" headerAlign="center" allowSort="true" width="150px" align="center">业务流水号</div>
	                <div field="limitType" headerAlign="center"  width="100px" align="center" data-options="{dict:'limitType'}" renderer="CommonUtil.dictRenderer" >限额类型</div>
	                <div field="tradName" headerAlign="center" width="100px" align="center" visible="false">经办人</div>
	                <div field="trad" headerAlign="center" width="100px" align="center" >经办人</div>
	                <div field="prd" headerAlign="center"  width="100px">产品</div>
	                <div field="prdType" headerAlign="center"  width="100px" align="center">产品类型</div>
	                <div field="quotaAmountU" headerAlign="center" allowSort="true" align="center" numberFormat="#,0.00">限额下线(美元)</div>
	                <div field="availAmountU" headerAlign="center"  allowSort="true" align="center" numberFormat="#,0.00">限额上线(美元)</div>
	                <div field="ccy" width="100" headerAlign="center" align="center">币种</div>
	                <!-- <div field="direction" width="100" headerAlign="center" align="center">交易方向</div> -->
	                <div field="term" width="100" headerAlign="center" align="center">期限</div>
	                <!-- <div field="dealSource" width="100" headerAlign="center" align="center">交易来源</div>
	                <div field="cost" width="100" headerAlign="center" align="center">成本中心</div> -->
	                <div field="ctlType" headerAlign="center"  width="100px" align="center" data-options="{dict:'ctlType'}" renderer="CommonUtil.dictRenderer" >检测控制</div>
	                <div field="itime" headerAlign="center" align="center"  allowSort="true" width="150px">录入时间</div>
	                <div field="totalAmount" headerAlign="center" align="center"  allowSort="true" width="150px" numberFormat="#,0.00">今日敞口(美元)</div>
	                <div field="maxToday" headerAlign="center" align="center"  allowSort="true" width="150px" numberFormat="#,0.00">今日敞口最大(美元)</div>
	                <!-- <div field="lastAmount" headerAlign="center" align="center"  allowSort="true" width="150px" numberFormat="#,0.00">昨日敞口</div>
	                <div field="maxLast" headerAlign="center" align="center"  allowSort="true" width="150px" numberFormat="#,0.00">昨日累计最大</div> -->
	                <div field="limitCount" headerAlign="center" align="center"  allowSort="true" width="150px">累计笔数</div>
	                <div field="limitCountSum" headerAlign="center" align="center"  allowSort="true" width="150px">今日笔数统计</div>
	            </div>
	        </div>
	    </div>
    </div>
    
    <div>
        <div class="mini-fit" style="width:100%;height:100%;" >
            <div class="mini-panel" title="外汇限额累计明细" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true">
				
				<fieldset id="fd1" class="mini-fieldset">
					<legend>
						<label>查询条件</label>
					</legend>
					<div id="search_formW" style="width: 100%;">
					<table id="search_formW" style="width: 100%;">
						<input id="limitId2" name="limitId2" class="mini-textbox" labelField="true" label="业务流水号："
							labelStyle="text-align:right;" emptyText="请输入业务流水号"/>
						<input id="cname" name="cname" class="mini-buttonedit" onbuttonclick="onButtonEdit" emptyText="请选择交易员" 
						labelField="true" allowInput="false" width="300px" label="交易员：" labelStyle="text-align:right;" />
						<input id="startVdate" name="startVdate" class="mini-datepicker" labelField="true" label="查询日期："
							labelStyle="text-align:right;" emptyText="请选择起息日期" value = "new Date()" />
						<span style="float: right; margin-right: 50px">
							<a id="search_btn" class="mini-button" style="display: none"  onclick="queryW()">查询</a>
							<a id="clear_btn" class="mini-button" style="display: none"  onClick="clearW()">清空</a>
						</span>
					</table>
					</div>
				</fieldset>
				
				
                <div id="quota_detail_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true"
                    idField="id" pageSize="10" multiSelect="false" sortMode="client" allowAlternating="true">
					<div property="columns">
                        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
						<div field="limitId" width="100px" align=""  headerAlign="center" >业务流水号</div>    
						<div field="prdNo" width="120px" align="center" headerAlign="center" >业务品种</div>
						<div field="trad" width="60px" align="center" headerAlign="center" >经办人</div>
						<div field="amt" width="120px" align="center" headerAlign="center" numberFormat="#,0.00">金额(美元)</div>
						<div field="loadTime" width="100px" align="center" headerAlign="center" >限额时间</div>
						<div field="todayTotal" width="120px" align="center" headerAlign="center" numberFormat="#,0.00">当日敞口(美元)</div>
						<div field="maxToday" width="120px" align="center" headerAlign="center" numberFormat="#,0.00">当日敞口最大(美元)</div>
						<!-- <div field="lastdayTotal" width="120px" align="center" headerAlign="center" numberFormat="#,0.00">昨日敞口(元)</div>
						<div field="maxLast" width="120px" align="center" headerAlign="center" numberFormat="#,0.00">昨日最大(元)</div> -->
						<div field="ccy" width="60px" align="center" headerAlign="center" data-options="{dict:'Currency'}" renderer="CommonUtil.dictRenderer">交易币种</div>
						<div field="prd" width="100px" align="center" headerAlign="center" >产品</div>
						<div field="prdType" width="60px" align="center" headerAlign="center" >产品类型</div>
						<div field="cost" width="100px" align="center" headerAlign="center" >成本中心</div>
						<div field="direction" width="60px" align="center" headerAlign="center" data-options="{dict:'trading'}" renderer="CommonUtil.dictRenderer">交易方向</div>
						<div field="ticketId" width="120px" align="center" headerAlign="center" >审批单号</div>
					</div>
				</div>
            </div>
        </div>
    </div>
 </div>   
</body>

<script>

mini.parse();
var grid = mini.get("grid");
$(document).ready(function () {
	//控制按钮显示
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		search(grid.pageSize, grid.pageIndex);

		var quota_detail_grid = mini.get("quota_detail_grid");
		quota_detail_grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			searchDetail(pageSize, pageIndex);
		});
	});
});

//增删改查
function add(){
	var url = CommonUtil.baseWebPath() + "/../Common/limit/LimitTemplateEdit.jsp?action=add";
	var tab = { id: "LimitTemplateAdd", name: "LimitTemplateAdd", title: "外汇限额新增", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
	var paramData = {selectData:""};
	CommonUtil.openNewMenuTab(tab,paramData);
}
function query() {
    search(grid.pageSize, 0);
}
function clear(){
    var form=new mini.Form("search_form");
    form.clear();
    search(10,0);

}


//修改
function modify() {
   var row = grid.getSelected(); 
    if (row) {
        var url = CommonUtil.baseWebPath() + "/../Common/limit/LimitTemplateEdit.jsp?action=edit";
        var tab = { id: "LimitTemplateEdit", name: "LimitTemplateEdit", title: "外汇限额修改", url: url, parentId: top["win"].tabs.getActiveTab().name,showCloseButton:true };
        var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
    } else {
        mini.alert("请选择一条数据");
    }
}


//搜索
function search(pageSize, pageIndex) {
    var form = new mini.Form("search_form");
    var data = form.getData();
    form.validate();
    if(form.isValid()==false){
        return;
    }
    data.branchId =branchId;
    data.pageNumber = pageIndex + 1;
    data.pageSize = pageSize;
    var param = mini.encode(data); //序列化成JSON
    CommonUtil.ajax({
        url: "/IfsLimitController/searchTemplateLimit",
        data: param,
        callback: function (data) {
        	//console.log(data);
            grid.setTotalCount(data.obj.total);
            grid.setPageIndex(pageIndex);
            grid.setPageSize(pageSize);
            grid.setData(data.obj.rows);
        }
    });
}

    //删除
    function del(){
        var rows=grid.getSelecteds();
        if(rows.length==0){
            mini.alert("请选中一行","提示");
            return;
        }
        mini.confirm("您确认要删除该限额?","系统警告",function(value){   
            if (value=='ok'){   
                var data=rows[0];
                params=mini.encode(data);
                CommonUtil.ajax( {
                    url:"/IfsLimitController/deleteIfsLimit",
                    data:params,
                    callback : function(data) {
                        mini.alert("删除成功.","系统提示");
                        search(10,0);
                    }
                });
            }
        });
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/MiniUserManagesMini.jsp",
            title: "选择限额交易员列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.userId);
                        btnEdit.setText(data.userName);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
    
    function onRowDblClick(e) {
		var grid = mini.get("grid");
		var row = grid.getSelected();
		if(row){
				var url = CommonUtil.baseWebPath() + "/../Common/limit/LimitTemplateEdit.jsp?action=detail&limitId="+row.limitId;
				var tab = { id: "LimitTemplateDetail", name: "LimitTemplateDetail", title: "外汇限额详情", url: url ,showCloseButton:true};
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);

		} else {
			mini.alert("请选中一条记录！","消息提示");
		}
	}
    
    function onSelectionChanged(e){
    	var credit_query_grid = e.sender;
        var record = credit_query_grid.getSelected();
        if(!record){
		     return false;
	    }
        row=record;
        mini.get("limitId2").setValue(row.limitId);
        searchDetail(10,0);
    }
    
    function queryW(){
    	searchDetail(10,0);
    }
    
    function searchDetail(pageSize,pageIndex){
    	
    	var form = new mini.Form("#search_formW");
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		
        data['LimitTemplate']=data.limitId2;
        data['loadTime'] = data.startVdate;
        data['trad']=data.cname;
        var url=null;
        var param = mini.encode(data);
        CommonUtil.ajax( {
        	url: "/IfsLimitController/searchLimitDetail",
            data:param,
            callback : function(data) {
                
                var grid =mini.get("quota_detail_grid");
                //设置分页
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                //设置数据
                grid.setData(data.obj.rows);
                
            }
        });
    }
    
	function clearW() {
		var form = new mini.Form("#search_formW");
		form.clear();
	}
    
</script>
</html>