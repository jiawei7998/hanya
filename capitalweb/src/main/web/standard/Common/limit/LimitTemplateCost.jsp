<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>成本中心页面</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>条件查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="cost" name="cost" class="mini-textbox" labelField="true"  label="成本中心编号："    width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入限额字段" />
			<input id="costName" name="costName" class="mini-textbox" labelField="true"  label="成本中心描述："    width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入限额字段" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button"     onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button"     onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
<span style="margin: 2px; display: block;"> 
	<a  id="save_btn" class="mini-button"     onclick="save">保存</a>
	
</span>   
<div class="mini-fit">
		<div id="limit_grid" class="mini-datagrid "  style="width:100%;height:100%;" idField="id" allowResize="true"	multiSelect="true"	   >
			<div property="columns">
			 <div type="checkcolumn" ></div> 
			  	<div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
				<div field="dictKey" width="180px" align="center"  headerAlign="center" >成本中心</div>
				<div field="dictValue" width="180px" align="center"  headerAlign="center" >成本中心描述</div>   
			</div>   
		</div>
	</div>  
<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("limit_grid");
	var row="";
	
	
    
	grid.on("beforeload", function (e) {
		//隐藏字典具体值
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	// $(document).ready(function(){
	//     search(10,0);
	// });

	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	
	/* 按钮 查询事件 */
	
	function query(){
		search(grid.pageSize,0);
	} 

	/* 查询 */
	function search(pageSize,pageIndex){		
		form.validate();
		//数据验证	
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params = mini.encode(data);
		//console.log(params);
		CommonUtil.ajax({
			url:"/IfsFieldLimitController/searchCostInfo",
			data:params,
   			callback : function(data) {
				var grid=mini.get("limit_grid");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	  function GetData() {
          var grid1= mini.get("limit_grid");
          var row1=grid1.getSelecteds();
          return row1;
      }

	 function save(){
    	 onOk();
    }
	 
    function onOk() {
        CloseWindow("ok");
    }
    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

	function SetData(data1) {
		prd = data1.prd;
		if(prd =="FXD," ){
			mini.get("costName").setValue("FX");
		}else if (prd == "MM,") {
			mini.get("costName").setValue("MM");
		}else  if (prd == "SWAP,") { //利率掉期  利率掉期
			mini.get("costName").setValue("利率掉期");
		}else  if (prd == "REPO,") {//回购 REPO
			mini.get("costName").setValue("REPO");
		}else if (prd == "SECLEN,") {//债券借贷 SECLEN
			mini.get("costName").setValue("SECLEN");
		}else if (prd == "SECUR,") {//债券借贷 SECLEN
			mini.get("costName").setValue("SECUR");
		}else if(prd == "FRA,") {//远期利率协议 FRA
			mini.get("costName").setValue("FRA");
		}
		search(10,0);
	}
</script>
</body>

</html>
