<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户准入明细</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>客户准入明细</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="ecifNameQry"  class="mini-textbox"   label="客户名称："  labelField="true" labelStyle="text-align:right;" width="280px" />
        	<input id="bussTpnmQry" class="mini-textbox"   label="业务类型：" labelField="true"   width="280px" labelStyle="text-align:right;" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		 allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>

			<div field="coreClntNo" headerAlign="center" allowSort="true" width="80px" align="center">核心客户号</div>
            <div field="conm" headerAlign="center" width="140px" align="center" >客户名称</div>
            <!-- <div field="bUSS_TYPE" headerAlign="center"  width="80px" align="center">业务类型</div> -->
            <div field="bussTpnm" headerAlign="center"  width="100px" align="center">业务类型名称</div>
            <!-- <div field="bRANCH_CODE" headerAlign="center"  width="80px" align="center">机构码</div> -->
            <div field="branchName" headerAlign="center" width="150px" allowSort="true" align="center">机构名称</div>
            <div field="inclSubFlg" width="80" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
            	data-options="{dict:'inclflag'}">包含下级标志</div>
            <div field="approvalDate" width="100" headerAlign="center" align="center" renderer="onDateRenderer">审批日期</div>
			<div field="oprType" width="100" headerAlign="center" align="center">操作状态</div>
			<div field="oprTime" width="100" headerAlign="center" align="center" renderer="onDateRenderer">操作日期</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("cust_grid");
	

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}

	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['ecifName'] = mini.get("ecifNameQry").getValue();
		data['bussTpnm'] = mini.get("bussTpnmQry").getValue();
		var params = mini.encode(data);

		CommonUtil.ajax({
			//url:"/edRiskController/getTcEdRisksPage",
			url:"/IfsLimitController/searchLocalDetail",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
    });
    

    function GetData() {
        
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
</script>
</body>
</html>
