<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>信用风险审查维护</title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
            <fieldset>
            <legend>风险审查信息</legend>
            <div class="leftarea">
                <input id="aPPROVE_FL_NO" name="aPPROVE_FL_NO" class="mini-textbox" labelField="true" label="审批件编号：" 
                	emptyText="请输入审批件编号" data="CommonUtil.serverData.dictionary.limitType" required="true"
                	style="width:100%" labelStyle="text-align:center;width:150px" onvaluechanged="checkProd"/>
               	<input id=tRAN_TYPE name="tRAN_TYPE" class="mini-combobox" labelField="true" label="交易类型："
               		data="CommonUtil.serverData.dictionary.trantype" emptyText="请选择交易类型" required="true"
               		style="width:100%" labelStyle="text-align:center;width:150px" />
                <input id="iNPT_TELLER_NO" name="iNPT_TELLER_NO" class="mini-textbox" labelField="true" label="经办员："
                	required="true"  emptyText="请输入经办员" style="width:100%" labelStyle="text-align:center;width:150px" />
                <input id="yLD_RATE" name="yLD_RATE" class="mini-spinner" label="年化收益率(%)：" emptyText="请输入年化收益率"
               		labelField="true" required="true"  style="width:100%" labelStyle="text-align:center;width:150px"
               		format="n2"/>
              	<input id="pD_EXP_DT" name=pD_EXP_DT class="mini-datepicker" label="有效期截止日：" emptyText="请选择有效期截止日"
					labelField="true" style="width:100%" labelStyle="text-align:center;width:150px" renderer="onDateRenderer"/>
			</div>			
			<div class="rightarea">
                <input id="oRDER_ID" name="oRDER_ID" class="mini-textbox" labelField="true" label="审批单号：" required="true"
                	emptyText="请输入审批单号" style="width:100%" labelStyle="text-align:center;width:150px" />
                <input id="tRAN_STATUS" name="tRAN_STATUS" class="mini-combobox" labelField="true"  label="交易状态："
                	data="CommonUtil.serverData.dictionary.transtatus" emptyText="请选择交易状态" required="true"
                	style="width:100%" labelStyle="text-align:center;width:150px"/>
                <input id="pRODUCT_NAME" name="pRODUCT_NAME" class="mini-textbox" labelField="true" label="产品名称：" required="true"
                	emptyText="请输入产品名称" style="width:100%" labelStyle="text-align:center;width:150px" />
                <input id="iVTNT_PLN" name="iVTNT_PLN" class="mini-textbox" label="投资规模：" emptyText="请输入投资规模"
					labelField="true" style="width:100%" labelStyle="text-align:center;width:150px"/>
			</div>			
            </fieldset>  
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>    
    </div> 
</div>
<script type="text/javascript">
	mini.parse();
	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var row = params.selectData;
	var url = window.location.search;
	var action = CommonUtil.getParam(url,"action"); // 增删改
	var form = new mini.Form("#field_form");
	
	function onDateRenderer(e) {
		var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
		if (value) return mini.formatDate(value, 'yyyy-MM-dd');
	}
	
	$(document).ready(function(){
	    initData();
	}); 
	
	//初始化数据
	function initData(){
	    if($.inArray(action,["edit","detail"])>-1){//修改、详情
	    	mini.get("aPPROVE_FL_NO").setEnabled(false); // 审批件编号不允许修改
	        form.setData(row);
	        if(action == "detail"){
	            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>客户准入</a> >> 详情");
	            form.setEnabled(false);
	            mini.get("save_btn").setVisible(false);
	        }
	    }
	}
	
	//保存
	function save(){
	    //表单验证！！！
	    form.validate();
	    if (form.isValid() == false) {
	        mini.alert("表单填写有误!","提示");
	        return;
	    }
	    var saveUrl = null;
	    if(action == "add"){ // 新增时保存
	        saveUrl = "/IfsLimitController/singleLimitAdd";
	    }else if(action == "edit"){ // 修改时保存
	        saveUrl = "/IfsLimitController/singleLimitEdit";
	    }
	    var data = form.getData(true);
	    var params = mini.encode(data);
	    CommonUtil.ajax({
	        url:saveUrl,
	        data:params,
	        callback:function(data){
	            mini.alert("保存成功",'提示信息',function(){
	                top["win"].closeMenuTab();
	            });
	        }
	    });
	}
	
	function close(){
		top["win"].closeMenuTab();
	}
	
	// 检查该审批件编号是否存在
    function checkProd(){
    	var approveFlNo = mini.get("aPPROVE_FL_NO").getValue();
    	CommonUtil.ajax({
    		url:"/IfsLimitController/searchProd",
    		data:approveFlNo,
    		callback:function(data1){
    			if(data1 == "1"){ // 表中有此编号
   					mini.alert("此审批件编号'"+approveFlNo+"'已存在!请重新填写!",'提示信息',function(){
   						mini.get("aPPROVE_FL_NO").setValue("");
   					});
    			}
    		}
    	});
    }
</script>
</body>
</html>