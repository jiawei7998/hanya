<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户号  用于客户号选择</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>客户查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
		 <input id="cno" name="cno" class="mini-textbox" labelField="true" label="客户号：" labelStyle="text-align:right;"
         width="330px" emptyText="请输入客户号"/>
         <input id="sn" name="sn" class="mini-textbox" labelField="true" label="客户简称：" labelStyle="text-align:right;"
         width="330px" emptyText="请输入客户简称" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="cno" width="120px" align="center"  headerAlign="center" >客户号</div>
            <div field="sn" width="120px" align="left"  headerAlign="center" >客户简称</div>
            <div field="cfn" width="250px" align="left"  headerAlign="center" >客户全称</div>
            <div field="ctype" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'CType'}">客户类型</div>
            <div field="uccode" width="90px" align="center"  headerAlign="center" >国家代码</div>
            <div field="location" width="90px"  headerAlign="center" align="left">所在城市</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("cust_grid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	var action ="edit";
	function SetData(data) {
		action=data.action;
    }
	
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	
	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['cno']= mini.get("cno").getValue();
        data['sn']= mini.get("sn").getValue();
		var params = mini.encode(data);

		CommonUtil.ajax({
			url:"/IfsCustLimitController/searchAllCust",
			data:params,
			callback : function(data) {
		        grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			search(10, 0);
		});
    });
    

    function GetData() {
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
    
        onOk();
    }
    function checkNull(val){
    	var flag = false;
    	if(val==null||val==""){
    		flag=true;
    	}
    	return flag;
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
</script>
</body>
</html>
