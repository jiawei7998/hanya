<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>客户准入维护</title>
</head>

<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
    <div size="90%" showCollapseButton="false">
        <div id="field_form" class="mini-fit area" style="background:white">
            <fieldset>
                <legend>客户准入信息</legend>
                <div class="leftarea">
                    <input id="coreClntNo" name="coreClntNo" class="mini-buttonedit mini-mustFill" labelField="true"
                           allowInput="false" label="客户号：" style="width:100%" emptyText="请输入客户号" required="true"
                           onbuttonclick="onButtonEdit" labelStyle="text-align:center;width:150px"/>
                    <div id="bussTpnm" name="bussTpnm" class="mini-combobox mini-mustFill" label="业务类型名称："
                         emptyText="请选择业务类型" labelField="true" required="true" style="width:100%"
                         labelStyle="text-align:center;width:150px" textField="busspnm" valueField="bussType"
                         multiSelect="true" showClose="true" onvalidation="onComboValidation"
                         onvaluechanged="bussTypeChange">   <%--onclick="onCloseClick"--%>
                        <div property="columns">
                            <div header="类型编号" field="bussType"></div>
                            <div header="类型名称" field="busspnm"></div>
                        </div>
                    </div>
                    <%--                <input id="bussTpnm" name="bussTpnm" textField="busspnm" valueField="bussType"  class="mini-combobox mini-mustFill" labelField="true" label="业务类型名称：" multiSelect="true"  required="true"  style="width:100%"  labelStyle="text-align:center;width:150px" allowInput="true" onvalidation="onComboValidation" onvaluechanged="bussTypeChange"/>--%>
                    <input id="dealType" name="dealType" class="mini-combobox mini-mustFill" label="交易类型："
                           emptyText="请选择交易类型" labelField="true" required="true" style="width:100%"
                           data="CommonUtil.serverData.dictionary.dealType2"
                           labelStyle="text-align:center;width:150px"/>
                    <input id="approvalDate" name="approvalDate" class="mini-datepicker mini-mustFill" label="审批日期："
                           emptyText="请选择审批日期" required="true" labelField="true" style="width:100%"
                           labelStyle="text-align:center;width:150px" renderer="onDateRenderer"/>
                    <%--   <input id="inclSubFlg" name="inclSubFlg" class="mini-combobox" label="包含下级标志：" emptyText="请选择包含下级标志" labelField="true" required="true"  style="width:100%" data="CommonUtil.serverData.dictionary.inclflag"  labelStyle="text-align:center;width:150px"/>--%>
                    <!--   <input id="bussType" name="bussType" class="mini-datepicker" label="业务类型名称编号：" emptyText="请输入业务类型名称编号"  labelField="true" style="width:100%" labelStyle="text-align:center;width:150px" renderer="onDateRenderer"/> -->
                </div>
                <div class="rightarea">
                    <input id="conm" name="conm" class="mini-textbox" labelField="true" label="客户名称：" style="width:100%"
                           enabled="false" onvalidation="onComboValidation" emptyText="请输入客户名称"
                           labelStyle="text-align:center;width:150px"/>
                    <input id="bussType" name="bussType" class="mini-textbox" labelField="true" label="业务类型名称编号："
                           enabled="false" emptyText="请输入业务类型名称编号" style="width:100%"
                           labelStyle="text-align:center;width:150px"/>
                    <input id="branchName" name="branchName" class="mini-textbox" labelField="true" label="机构名称："
                           enabled="false" emptyText="请输入机构名称" style="width:100%"
                           labelStyle="text-align:center;width:150px"/>
                </div>
            </fieldset>
        </div>
    </div>
    <div id="functionIds" style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
            <a class="mini-button" style="display: none" style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
            <a class="mini-button" style="display: none" style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    mini.parse();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row = params.selectData;
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    // 增删改
    var form = new mini.Form("#field_form");

    function onDateRenderer(e) {
        var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value) == null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g, '/'));
        if (value) return mini.formatDate(value, 'yyyy-MM-dd');
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            initData();
            loadProductList();
        });
    });

    //加载产品列表
    function loadProductList() {
        CommonUtil.ajax({
            data: {branchId: branchId, isTemplate: "0"},
            url: "/ProductController/searchProduct",
            callback: function (data) {

                var length = data.obj.length;
                var jsonData = new Array();
                for (var i = 0; i < length; i++) {
                    var bussType = data.obj[i].prdNo;
                    var bussTpnm = data.obj[i].prdName;
                    jsonData.add({'bussType': bussType, 'busspnm': bussTpnm});
                }
                ;
                mini.get("bussTpnm").setData(jsonData);
                //设置默认全选
                var bussTypeList=jsonData.map(e=>{
                    return e.bussType;
                })
                var values=bussTypeList.join(",");
                mini.get("bussTpnm").setValue(values);
                mini.get("bussType").setText(values);
            }
        });
    }

    function bussTypeChange(e) {
        var bussType = mini.get("bussTpnm").getValue();
        mini.get("bussType").setText(bussType);

    }

    function onCloseClick(e) {
        var obj = e.sender;
        obj.setText("");
        obj.setValue("");
    }

    //初始化数据
    function initData() {
        /* $(document).ready(function(){
            queryCustNm();
        }); */
        if ($.inArray(action, ["edit", "detail"]) > -1) {//修改、详情
            mini.get("coreClntNo").setEnabled(false); // 客户号不允许修改
            mini.get("bussTpnm").setEnabled(false); // 业务类型名称不允许修改
            form.setData(row);
            mini.get("coreClntNo").setText(row.coreClntNo);
            mini.get("bussTpnm").setValue(row.bussType);
            mini.get("bussType").setValue(row.bussType);

            if (action == "detail") {
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>客户准入</a> >> 详情");
                form.setEnabled(false);
                mini.get("save_btn").setVisible(false);
            }
        }


    }

    //
    //   function checkCoreNo(){
    //          var coreClntNo = mini.get("coreClntNo").getValue();
    //          CommonUtil.ajax({
    //              url:"/IfsCustLimitController/searchBuss",
    //              data:coreClntNo,
    //              callback:function(data){
    //                  if(data.sn == null){ // 查询不到客户信息
    //                      mini.alert("无此客户");
    //                      mini.get("coreClntNo").setValue("");
    //                      return;
    //                  }
    //
    //                  mini.get("conm").setValue(data.sn);
    //              }
    //          });
    //      }
    //

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/limit/CustLimit.jsp",
            title: "选择客户号列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cno);
                        btnEdit.focus();
                    }
                    if (data.cno != null) {
                        mini.get("conm").setValue(data.cname);

                        mini.get("branchName").setValue(data.sn);

                    }

                }

            }
        });
    }


    //保存
    function save() {
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写有误!", "提示");
            return;
        }
        var saveUrl = null;
        var data = form.getData(true);
        data['bussTpnm'] = mini.get('bussTpnm').getText();
        if (action == "add") {
            data['bussType'] = mini.get('bussTpnm').getValue();
            saveUrl = "/IfsCustLimitController/bussInfoAdd";
        } else if (action == "edit") {
            saveUrl = "/IfsCustLimitController/bussInfoEdit";
        }

        data['conm'] = mini.get('conm').getText();
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: saveUrl,
            data: params,
            callback: function (data) {
                mini.alert(data.desc, '提示信息', function () {
                    top["win"].closeMenuTab();
                });
            }
        });
    }

    function close() {
        top["win"].closeMenuTab();
    }


    // 根据客户号查询客户名称
    function queryName() {

    }

    // 新增时下拉值校验
    function onComboValidation(e) {
        if ($.inArray(action, ["edit", "detail"]) == -1) {
            var items = this.findItems(e.value);
            if (!items || items.length == 0) {
                e.isValid = false;
                e.errorText = "输入值不在下拉数据中";
            }
        }
    }
</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>