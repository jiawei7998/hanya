<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <title>客户信息经办</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>经办客户查询</legend>
    <div>
        <div id="search_form" style="width:100%" cols="6">
            <input id="cno" name="cno" class="mini-textbox" labelField="true" label="客户号：" width="320px"
                   labelStyle="text-align:center;width:140px" emptyText="请输入客户号"/>
            <input id="cname" name="cname" class="mini-textbox" labelField="true" label="客户代码：" width="320px"
                   labelStyle="text-align:center;width:140px" emptyText="请输入客户代码"/>
            <input id="cliname" name="cliname" class="mini-textbox" labelField="true" label="客户名称：" width="320px"
                   labelStyle="text-align:center;width:140px" emptyText="请输入客户名称"/>
            <input id="acctngtype" name="acctngtype" class="mini-textbox" labelField="true" label="会计类型：" width="320px"
                   labelStyle="text-align:center;width:140px" emptyText="请输会计类型"/>
            <input id="ctype" name="ctype" class="mini-combobox" data="CommonUtil.serverData.dictionary.CType"
                   width="320px"
                   emptyText="请选择客户类型" labelField="true" label="客户类型：" labelStyle="text-align:center;width:140px"/>
            <span style="float: right; margin-right: 50px">
				<a id="search_btn" class="mini-button" style="display: none" onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none" onclick="clear()">清空</a>
			</span>
        </div>
    </div>
    <span style="margin: 2px; display: block;">
        <a id="add_btn" class="mini-button" style="display: none" onclick="add()">新增</a>
		<a id="send_btn" class="mini-button" style="display: none" onclick="send()">经办</a>
		<a id="edit_btn" class="mini-button" style="display: none" onclick="edit()">修改</a>
		<a id="delete_btn" class="mini-button" style="display: none" onclick="del()">删除</a>

	</span>
</fieldset>

<div class="mini-fit">
    <div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"
         allowAlternating="true"
         onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true"
         frozenStartColumn="0" frozenEndColumn="1">
        <div property="columns">
            <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
            <div field="handle" width="120px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'Handletype'}">经办状态
            </div>
            <div field="cno" width="120px" align="center" headerAlign="center">客户号</div>
            <div field="cname" width="100px" headerAlign="center" align="center">客户代码</div>
            <div field="sn" width="250px" align="center" headerAlign="center">客户简称</div>
            <div field="cliname" width="200px" headerAlign="center" align="center">客户名称</div>
            <div field="linkman" width="120" align="center" headerAlign="center">联系人</div>
            <div field="contact" width="120" align="center" headerAlign="center">联系方式</div>
            <div field="shtype" width="200px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'GidType'}">唯一标识代码类型
            </div>
            <div field="shcode" width="200px" headerAlign="center" align="center">唯一标识代码</div>
            <div field="ctype" width="120px" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'CType'}">客户类型
            </div>
            <%--			<div field="custclass" width="120px" align="center"  headerAlign="center" renderer="CommonUtil.dictRenderer" --%>
            <%--				 data-options="{dict:'YesNo'}">是否为集团客户</div>--%>
            <div field="subjectType" width="200px" align="center" headerAlign="center"
                 renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'ztType'}">主体类型
            </div>
            <div field="acctngtype" width="120px" headerAlign="center" align="center">会计类型</div>
            <div field="bic" width="120" align="center" headerAlign="center">SWIFT编码</div>
            <div field="uccode" width="90px" align="center" headerAlign="center">国家代码</div>
            <div field="ccode" width="90px" align="center" headerAlign="center">城市代码</div>
            <div field="sic" width="120px" align="center" headerAlign="center">工业标准代码</div>
            <div field="cfn" width="250px" align="center" headerAlign="center">客户全称</div>
            <div field="location" width="90px" headerAlign="center" align="center">所在城市</div>
            <div field="cfetsno" width="200px" headerAlign="center" align="center">CFETS本币机构编码（6位）</div>
            <div field="cfetscn" width="200px" headerAlign="center" align="center">CFETS外币机构编码（21位）</div>
            <div field="clino" width="120px" headerAlign="center" align="center">核心客户号</div>
        </div>
    </div>
</div>

<script>
    mini.parse();
    var url = window.location.search;
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var prdName = CommonUtil.getParam(url, "prdName");
    var form = new mini.Form("#search_form");
    var grid = mini.get("cust_grid");
    var row = "";

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    //查看详情
    function onRowDblClick(e) {
        var url = CommonUtil.baseWebPath() + "/../Common/CustEdit.jsp?action=detail";
        var tab = {
            id: "CustDetail", name: "CustDetail", url: url, title: "交易对手详情",
            parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
        };
        var paramData = {selectData: grid.getSelected()};
        CommonUtil.openNewMenuTab(tab, paramData);
    }

    //清空
    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        query();
    }


    //修改
    function edit() {
        var row = grid.getSelected();
        if (row) {
            var url = CommonUtil.baseWebPath() + "/../Common/CustEdit.jsp?action=edit&limit=1&handles=1";
            var tab = {
                id: "CustEdit", name: "CustEdit", url: url, title: "客户信息修改",
                parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
            };
            var paramData = {selectData: row};
            CommonUtil.openNewMenuTab(tab, paramData);
        } else {
            mini.alert("请选择一条记录", "提示");
        }
    }

    //添加
    function add(){
        var url = CommonUtil.baseWebPath() +"/../Common/CustEdit.jsp?action=add&limit=1";
        var tab = {id: "CustAdd",name:"CustAdd",url:url,title:"交易对手新增",
            parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
        var paramData = {selectData:""};
        CommonUtil.openNewMenuTab(tab,paramData);
    }

    //经办
    function send() {
        var rows = grid.getSelected();
        if (rows.length == 0) {
            mini.alert("请选中一行", "提示");
            return;
        }
        mini.confirm("您确认要经办该客户信息?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows;
                data['handle'] = '2';
                params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsOpicsCustController/sendCust",
                    data: params,
                    callback: function (data) {
                        mini.alert("经办成功.", "系统提示");
                        search(10, 0);
                    }
                });
            }
        });
    }

    //删除
    function del() {
        var rows = grid.getSelected();
        if (rows.length == 0) {
            mini.alert("请选中一行", "提示");
            return;
        }
        mini.confirm("您确认要删除该交易对手信息?", "系统警告", function (value) {
            if (value == 'ok') {
                var data = rows;
                params = mini.encode(data);
                CommonUtil.ajax({
                    url: "/IfsOpicsCustController/deleteOpicsCust",
                    data: params,
                    callback: function (data) {
                        mini.alert("删除成功.", "系统提示");
                        search(10, 0);
                    }
                });
            }
        });
    }


    /* 按钮 查询事件 */
    function query() {
        search(grid.pageSize, 0);
    }

    /* 查询 */
    function search(pageSize, pageIndex) {
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['custclass'] = '0';
        data['handle'] = '1';
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: "/IfsOpicsCustController/searchPageOpicsCust",
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });


    // 导出
    function exportExcel() {
        var content = grid.getData();
        if (content.length == 0) {
            mini.alert("请先查询数据");
            return;
        }
        mini.confirm("您确认要导出Excel吗?", "系统提示",
            function (action) {
                if (action == "ok") {
                    var form = new mini.Form("#search_form");
                    var data = form.getData(true);
                    var fields = null;
                    for (var id in data) {
                        fields += '<input type="hidden" id="' + id + '" name="' + id + '" value="' + data[id] + '">';
                    }
                    var urls = CommonUtil.pPath + "/sl/IfsExportController/exportCustExcel";
                    $('<form action="' + urls + '" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }
</script>
</body>
</html>