<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<%-- <script type="text/javascript" src="<%=basePath%>/standard/Common/opics.js"></script> --%>
<div id="field_form">
	<div class="centerarea">
		<div class="leftarea" style="float: left; margin-left: 15px;margin-top: 50px;width: 45%;">
			<!-- <input id="dealSource" name="dealSource" class="mini-combobox" labelField="true"  style="width:100%;"  label="交易来源：" required="true"  data="CommonUtil.serverData.dictionary.TradeSource" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  > -->
         	<input id="product" name="product" onbuttonclick="onPrdEdit" allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true"  style="width:100%;"  label="产品代码：" required="true"  maxLength="10"  labelStyle="text-align:left;width:130px;" >
	    	<input id="port" name="port" class="mini-buttonedit mini-mustFill" onbuttonclick="onPortEdit"  style="width:100%;"  labelField="true"  label="投资组合：" required="true"  allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:130px;" >
        </div>
        <div class="rightarea" style="float: right; margin-right: 15px;margin-top: 50px;width: 45%;">
        	<input id="prodType" name="prodType" onbuttonclick="onTypeEdit" allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true" style="width:100%;" label="产品类型：" required="true"  vtype="maxLength:2" labelStyle="text-align:left;width:130px;" >
        	<input id="cost" name="cost" onbuttonclick="onCostEdit" allowInput="false" class="mini-buttonedit mini-mustFill" labelField="true"  label="成本中心：" style="width:100%;"  required="true"  vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  >
        </div>
	</div>
</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-top: 30%; text-align: center;">
			<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"   onclick="save">保存</a>
			<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"   onclick="onCancel">取消</a>
		</div>
</div>
<script>
	mini.parse();
	function GetData() {
		var form=new mini.Form("#field_form");
		var data=form.getData(true);
		return data;
	}

	function SetData(data){
		prdName = data.prdName;
		if(prdName=="外汇即期"){
			prdName="FXD-SPOT";
		}
		if(prdName=="外汇远期"){
			prdName="FXD-FWD";
		}
		if(prdName=="外汇掉期"){
			prdName="FXD-SWAP";
		}
		if(prdName=="外币拆借"){
			prdName="MM";
		}
		if(prdName=="黄金即期"){
			prdName="GOLD-SPOT";
		}
		if(prdName=="黄金远期"){
			prdName="GOLD-FWD";
		}
		if(prdName=="黄金掉期"){
			prdName="GOLD-SWAP";
		}
	}

	function save() {
		onOk();
	}
	
	function CloseWindow(action) {
		if (window.CloseOwnerWindow)
			return window.CloseOwnerWindow(action);
		else
			window.close();
	}

	function onOk() {
		CloseWindow("ok");
	}
	
	function onCancel() {
		CloseWindow("cancel");
	}
	
	/* 产品代码的选择 */
    function onPrdEdit(){
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 700,
            height: 500,
            onload: function () { // 弹出页面加载完成
                var iframe = this.getIFrameEl();
            	var data = {"prdName":prdName};
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pcode);
                        onPrdChanged();
                        btnEdit.focus();
                    }
                }
            }
        });
    }
	
    function onPrdChanged(){
    	if(mini.get("prodType").getValue()!=""){
    		mini.get("prodType").setValue("");
        	mini.get("prodType").setText("");
        	mini.alert("产品代码已改变，请重新选择产品类型!");
    	}
    }
    
    /* 产品类型的选择 */
    function onTypeEdit(){
    	var prd = mini.get("product").getValue();
    	if(prd == null || prd == ""){
    		mini.alert("请先选择产品代码!");
    		return;
    	}
    	var data={prd:prd,prdName:prdName};
    	var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.type);
                        btnEdit.focus();
                    }
                }
            }
        });
    }
    
    /* 成功中心的选择 */
    function onCostEdit(){
    	var btnEdit = this;
    	
    	var port=mini.get("port").getValue();
    	var type=null;
    	if(port=="FXZY"){
    		type="ZI YING";
    	}
    	if(port=="FXDK"){
    		type="DAI KE";
    	}
    	if(port=="FXZS"){
    		type="ZI SHEN";
    	}
    	var data={prdName:prdName,type:type};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costcent);
                        btnEdit.focus();
                    }
                }

            }
        });
    }
    
    /* 投资组合的选择 */
    function onPortEdit(){
 	   var btnEdit = this;
 	   var data={prdName:prdName};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/portMiniLess.jsp",
            title: "选择投资组合",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.portfolio);
                        btnEdit.setText(data.portfolio);
                        btnEdit.focus();
                    }
                }

            }
        });
     	
     }
</script>