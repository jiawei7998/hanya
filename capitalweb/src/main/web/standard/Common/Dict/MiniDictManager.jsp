<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp" %>
<script src="<%=basePath %>/sl/TaDictController/dictionary.js" type="text/javascript"></script>

<body style="width:100%;height:100%;background:white">
<fieldset id="fd2" class="mini-fieldset title">
    <legend>查询</legend>
    <div id="search_form">
        <input id="dict_id" name="dict_id" class="mini-textbox" width="320px" labelField="true" label="代码编号："
               labelStyle="text-align:right;" emptyText="请输入代码编号"/>
        <input id="dict_name" name="dict_name" class="mini-textbox" width="320px" labelField="true" label="代码名称："
               labelStyle="text-align:right;" emptyText="请输入代码名称"/>
        <input id="dict_module" name="dict_module" class="mini-textbox" width="320px" labelField="true" label="所属主题："
               labelStyle="text-align:right;" emptyText="请输入所属主题"/>
        <span style="float:right;margin-right: 150px">
						<a class="mini-button" style="display: none" id="search_btn" onclick="query()">查询</a>
						<a class="mini-button" style="display: none" id="clear_btn" onclick="clear()">清空</a>
				</span>
    </div>
    <span style="margin:2px;display: block;">
				<a class="mini-button" style="display: none" id="refresh_btn" name="refresh_btn" onclick="refresh">刷新字典缓存</a>
				<a class="mini-button" style="display: none" id="add_btn" name="add_btn" onclick="add();">新增</a>
				<a class="mini-button" style="display: none" id="edit_btn" name="edit_btn" onclick="edit">修改</a>
				<a class="mini-button" style="display: none" id="delete_btn" name="delete_btn" onclick="del();">删除</a>
			<span style="color: red;"> 【新增】、【修改】、【删除】 操作后，请手动执行【刷新字典缓存】，否则下拉框选项值不生效！</span>
		</span>
</fieldset>

<div class="mini-fit">
    <div id="dictManagerGrid" class="mini-datagrid borderAll" style="width:100%;height:50%;" idField="dict_id"
         onselectionchanged="onSelectionChanged" sortMode="client" allowAlternating="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
            <div field="dict_id" width="100" headerAlign="center">代码编号</div>
            <div field="dict_name" width="100" headerAlign="center">代码名称</div>
            <div field="dict_module" width="100" headerAlign="center">所属主题</div>
        </div>
    </div>
    <div id="dictManagerGrid_children" class="mini-datagrid borderAll" idField="dict_id" sortMode="client"
         style="width:100%;height:50%;" allowResize="true" showPager="false" multiSelect="true" virtualScroll="true"
         allowAlternating="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="50">序号</div>
            <div field="dict_id" width="120" headerAlign="center">代码编号</div>
            <div field="dict_name" width="100" headerAlign="center">代码名称</div>
            <div field="dict_key" width="100" headerAlign="center" align="center">代码值</div>
            <div field="dict_value" width="100" headerAlign="center">代码名称</div>
            <div field="dict_desc" width="100" headerAlign="center">代码描述</div>
            <%--				<div field="dict_parentid" width="120" headerAlign="center" align="center">上级代码</div>                --%>
            <div field="dict_level" width="100" headerAlign="center" align="center"> 代码级别</div>
            <div field="is_active" width="100" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer"
                 data-options="{dict:'YesNo'}">是否启用
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    mini.parse();

    var form = new mini.Form("#search_form")
    var grid2 = mini.get("dictManagerGrid_children");
    var grid = mini.get("dictManagerGrid");
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search(10, 0);
        });
    });

    function query() {
        search(grid.pageSize, 0);
    }

    function refresh() {
        CommonUtil.ajax({
            url: "/TaDictController/refreshDictionary",
            data: {},
            callback: function (data) {
                mini.alert("刷新成功.", "系统提示");
            }
        });
    }

    //删除的时候调用-->为了不影响其他功能
    function updateDel() {
        var row = grid.getSelected();
        if (row) {
            var param = mini.encode(row);
            CommonUtil.ajax({
                url: "/TaDictController/getTaDictByDictId",
                data: param,
                callback: function (data) {
                    grid2.setData(data.obj);
                    grid2.setTotalCount(data.obj.total);
                    query();
                }
            });
        }
    }

    //子节点
    function onSelectionChanged() {
        var row = grid.getSelected();
        if (row) {
            var param = mini.encode(row);
            CommonUtil.ajax({
                url: "/TaDictController/getTaDictByDictId",
                data: param,
                callback: function (data) {
                    grid2.setData(data.obj);
                    grid2.setTotalCount(data.obj.total);
                }
            });
        }
    }

    //父节点查询
    function search(pageSize, pageIndex) {
        //数据验证
        form.validate();
        if (form.isValid == false) return;
        //提交数据
        var data = form.getData();// 获取表单多个控件数据
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var param = mini.encode(data); // 序列化json
        CommonUtil.ajax({
            url: '/TaDictController/getTaDictParent',
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }

        });
    }

    //清空
    function clear() {
        mini.get("dict_id").setValue("");
        mini.get("dict_name").setValue("");
        mini.get("dict_module").setValue("");
        query();
        clearGrid2();
    }

    function clearGrid2() {
        grid2.setData();
    }

    //新增按钮
    function add() {
        var row = grid.getSelected();
        if (row) {
            //选择父级一条数据新增
            var url = '<%=basePath%>' + "/standard/Common/Dict/MiniDictEdit.jsp?action=add";
            var tab = {
                id: "dictAdd",
                name: "dictAdd",
                text: "数据字典新增",
                url: url,
                parentId: top["win"].tabs.getActiveTab().name
            };
            top['win'].showTab(tab);
            onSelectionChanged();
        } else {
            //直接点击新增按钮
            var url = '<%=basePath%>' + "/standard/Common/Dict/MiniDictEdit.jsp?action=add2";
            var tab = {
                id: "dictAdd",
                name: "dictAdd",
                text: "数据字典新增",
                url: url,
                parentId: top["win"].tabs.getActiveTab().name
            };
            top['win'].showTab(tab);
            onSelectionChanged();
        }

    }

    top['DictEdit'] = window;

    //修改
    function edit() {
        var record = grid2.getSelected();
        if (record) {
            var url = '<%=basePath%>' + "/standard/Common/Dict/MiniDictEdit.jsp?action=edit";
            var tab = {
                id: "dictEdit",
                name: "dictEdit",
                text: "数据字典修改",
                url: url,
                parentId: top['win'].tabs.getActiveTab().name
            };
            top['win'].showTab(tab);
        } else {
            mini.alert("请选中一条记录", "系统提示");
        }
    }

    //删除
    function del() {
        var grid = mini.get("dictManagerGrid_children");
        var row = grid.getSelected();
        if (row) {
            mini.confirm("您确定要删除吗？", "温馨提示", function (action) {
                if (action == 'ok') {
                    CommonUtil.ajax({
                        url: "/TaDictController/deleteTaDict",
                        data: {dict_id: row.dict_id, dict_key: row.dict_key},
                        callback: function (data) {
                            if (data.code == 'error.common.0000') {
                                mini.alert("操作成功", "系统提示", function () {
                                    updateDel();
                                });
                            } else {
                                mini.alert("操作失败");
                            }
                        }
                    });
                }
            })
        } else {
            mini.alert("请先选择一条数据操作", "操作提示");
        }
    }

    function getData(action) {
        var grid = mini.get("dictManagerGrid_children");
        var grid2 = mini.get("dictManagerGrid")
        if (action == "edit") {
            var row = null;
            //var row2=null;
            row = grid.getSelected();
            //row=grid2.getSelected();

        } else if (action == "add") {
            var row = null;
            row = grid2.getSelected();
        }
        return row;
        //return row2;
    }

    top['dictManagerList'] = window;
</script>