<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title>交易对手</title>
</head>
<body style="width:100%;height:100%;background:white">

<div style="width:100%;height:100%;">
    <div showCollapseButton="true">
        <div id="tabs" class="mini-panel" title="现金流信息" style="width:100%;height:100%;"collapseOnTitleClick="true">
            <div id="panel1" class="mini-panel" title="本金计划信息" style="width:50%;height:98%;float: left"  allowResize="false" collapseOnTitleClick="false">
                <span style="margin: 2px; display: block;">
                    <a id="amt_add_btn" class="mini-button" onclick="addAmtRow()">新增</a>
                    <a id="amt_remove_btn" class="mini-button" onclick="removeAmtRow()">删除</a>
                </span>
                <div id="amt_datagrid" class="mini-datagrid" style="width: 100%; height: 400px;" showPager="false" allowResize="true"
                     sortMode="client" allowAlternating="true" virtualScroll="true" allowCellEdit="true" allowCellSelect="true"
                     multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true"
                     oncellvalidation="onCellValidation" allowCellValid="true">
                    <div property="columns">
                        <div field="seqNumber" headerAlign="center" align="center" width="50px" >期号</div>
                        <div name="repaymentSdate" field="repaymentSdate" width="200px" dateFormat="yyyy-MM-dd" headerAlign="center" align="center" >期末日期
                            <input property="editor" class="mini-datepicker" style="width:100%;"  format='yyyy-MM-dd' onValueChanged="sortByEndDate"/> <%--ondrawdate="onDrawDate"--%>
                        </div>
                        <div name="repaymentSamt" field="repaymentSamt" width="200px" numberFormat="#,0.00" headerAlign="center" align="center" >当期归还本金
                            <input property="editor" class="mini-spinner input-text-strong" minValue="0" maxValue="9999999999999999999.99" format="n2" style="width:100%;" onValueChanged="reCalBackAmt"/>
                        </div>
                    </div>
                </div>
            </div>
            <div id="panel1" class="mini-panel" title="利息计划信息" style="width:50%;height:98%;float: left"  allowResize="false" collapseOnTitleClick="false">
                <span style="margin: 2px; display: block;">
                    <a id="int_add_btn" class="mini-button" onclick="addIntRow()">新增</a>
                    <a id="int_remove_btn" class="mini-button" onclick="removeIntRow()">删除</a>
                </span>
                <div id="int_datagrid" class="mini-datagrid" style="width: 100%; height: 400px;" showPager="false" allowResize="true"
                     sortMode="client" allowAlternating="true" virtualScroll="true" allowCellEdit="true" allowCellSelect="true"
                     multiSelect="false" editNextOnEnterKey="true"  editNextRowCell="true"
                     oncellvalidation="onCellValidation" allowCellValid="true">
                    <div property="columns">
                        <div field="seqNumber" headerAlign="center" align="center" width="50px" >期号</div>
                        <div name="refEndDate" field="refEndDate" width="120px" dateFormat="yyyy-MM-dd" headerAlign="center" align="center" >计息到期日期
                            <input property="editor" class="mini-datepicker" style="width:100%;" format='yyyy-MM-dd' onValueChanged="sortByPayEndDate" /><%--ondrawdate="onDrawDate"--%>
                        </div>
                        <div name="interestAmt" field="interestAmt" width="120px" numberFormat="#,0.00" headerAlign="center" align="center" >区间计提利息总额
                            <input property="editor" class="mini-spinner input-text-strong" minValue="0" maxValue="9999999999999999999.99" format="n2" style="width:100%;" enabled="false"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span>
            <a id="save_btn" class="mini-button" onclick="save()">保存</a>
        </span>
    </div>
</div>
<script>
    /*******************************初始化******************************************/
    mini.parse();
    var url = window.location.search;
    var prdNo = CommonUtil.getParam(url, "prdNo");
    var prdName = CommonUtil.getParam(url, "prdName");
    var ticketId = CommonUtil.getParam(url, "ticketId");
    var action = CommonUtil.getParam(url, "action");

    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    // var vdate = params.vdate;
    // var mdate = params.mdate;
    var amtgrid = mini.get("amt_datagrid");
    var intgrid = mini.get("int_datagrid");
    var row = "";

    $(document).ready(function() {
        //控制按钮显示
        // $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            initCashFlow();
            query();
        // });
    });

    amtgrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchAmt(pageSize,pageIndex);
    });
    intgrid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchInt(pageSize,pageIndex);
    });

    //按钮和input框初始化
    function initCashFlow(){
        if(action=="add" || action=="edit" ){

        } else {
            mini.get("save_btn").hide();
            mini.get("amt_add_btn").hide();
            mini.get("amt_remove_btn").hide();
            mini.get("int_add_btn").hide();
            mini.get("int_remove_btn").hide();
        }
    }

    /* 按钮 查询事件 */
    function query(){
        searchAmt(20,0);
        searchInt(20,0);
    }

    /* 查询本金计划 */
    function searchAmt(pageSize,pageIndex){
        CommonUtil.ajax({
            url:"/IfsRmbDepositOutController/searchAmt",
            data: {dealNo:ticketId},
            callback : function(data) {
                // amtgrid.setTotalCount(data.obj.total);  ,pageNumber:pageIndex+1,pageSize:pageSize
                // amtgrid.setPageIndex(pageIndex);
                // amtgrid.setPageSize(pageSize);
                amtgrid.setData(data.obj);
            }
        });
    }

    /* 查询利息计划 */
    function searchInt(pageSize,pageIndex){
        CommonUtil.ajax({
            url:"/IfsRmbDepositOutController/searchInt",
            data: {dealNo:ticketId},
            callback : function(data) {
                // intgrid.setTotalCount(data.obj.total); ,pageNumber:pageIndex+1,pageSize:pageSize
                // intgrid.setPageIndex(pageIndex);
                // intgrid.setPageSize(pageSize);
                intgrid.setData(data.obj);
            }
        });
    }

    /*******************************按钮方法******************************************/
    //保存
    function save(){
        var intTotalRows = intgrid.getData();
        var amtTotalRows = amtgrid.getData();

        amtgrid.validate();
        if (amtgrid.isValid() == false) {
           mini.alert("必须输入有效的数据！","系统提示");
            return;
        }
        //
        // var actualIntRows = new Array();
        // var intNum = 0;
        // for (var i = 0; i < intTotalRows.length; i++) {
        //     if (intTotalRows[i].actualInt != null) {
        //         actualIntRows[intNum++] = intTotalRows[i];
        //     }
        // }
        // var originalActualIntSize = actualIntRows.length;

        var data = {};
        data['amtPlan'] = amtTotalRows;
        data['intPlan'] = intTotalRows;
        data['dealNo'] = ticketId;
        var params=mini.encode(data);
        CommonUtil.ajax({
            url:'/IfsRmbDepositOutController/saveCashFlow',
            data:params,
            callback : function(data) {
                mini.alert(data.desc, '提示信息', function () {//提示弹框并执行回调
                    if (data.code == 'error.common.0000') {
                        top["win"].closeMenuTab();//关闭并刷新当前界面
                    }
                });
            }
        });
    }

    //新增一行
    function addIntRow() {
        var totalRows = intgrid.getData().length;

        for (var i = 0; i < totalRows; i++) {
            if (intgrid.getData()[i].refEndDate == null) {
                return;
            }
        }

        var newRow = { seqNumber: 0 };
        intgrid.addRow(newRow, 0);
    }

    //新增一行
    function addAmtRow() {
        var totalRows = amtgrid.getData().length;
        for (var i = 0; i < totalRows; i++) {
            if (amtgrid.getData()[i].repaymentSdate == null) {
                return;
            }
        }

        var newRow = { seqNumber: 0};
        amtgrid.addRow(newRow, 0);
    }

    //删除一行
    function removeIntRow() {
        var totalRows = intgrid.getData().length;
        var rows = intgrid.getSelecteds();
        var row = rows[0];
        if (row.actualInt != null) {
            mini.alert("有实际还款的不允许删除","提示");
            return;
        }
        if (rows.length == 0) {
            mini.alert("请选择需要删除的数据","提示");
            return;
        }
        intgrid.removeRows(rows, true);
        sortByPayEndDate("null");
    }

    //删除一行
    function removeAmtRow() {
        var totalRows = amtgrid.getData().length;
        var rows = amtgrid.getSelecteds();
        var row = rows[0];
        if (row.actualAmt != null) {
            mini.alert("有实际还款的不允许删除","提示");
            return;
        }
        if (rows.length == 0) {
            mini.alert("请选择需要删除的数据","提示");
            return;
        }
        amtgrid.removeRows(rows, true);
        sortByEndDate("null");
    }

    // function del(){
    //     var rows=grid.getSelecteds();
    //     if(rows.length==0){
    //         mini.alert("请选中一行","提示");
    //         return;
    //     }
    //     mini.confirm("您确认要删除该交易对手信息?","系统警告",function(value){
    //         if (value=='ok'){
    //             var data=rows[0];
    //             params=mini.encode(data);
    //             CommonUtil.ajax( {
    //                 url:"/IfsOpicsCustController/deleteOpicsCust",
    //                 data:params,
    //                 callback : function(data) {
    //                     mini.alert("删除成功.","系统提示");
    //                     search(10,0);
    //                 }
    //             });
    //         }
    //     });
    // }

    /*******************************事件触发******************************************/
    //禁选日期
    // function onDrawDate(e) {
    //     var date = e.date;
    //     var adate = vdate;//交易起期
    //     var a = mini.parseDate(adate)
    //     if (date.getTime() < a.getTime()) {
    //         e.allowSelect = false;
    //     }
    //     var mdate = mdate;//交易止期
    //     var m = mini.parseDate(mdate)
    //     if (date.getTime() > m.getTime()) {
    //         e.allowSelect = false;
    //     }
    // }

    //利息按期末日期排序
    function sortByPayEndDate(e){
        var row = intgrid.getSelected();
        var payOrder = 1;
        var totalRows = intgrid.getData();
        var sameNumber = 0;

        if (e != 'null') {
            for (var i = 0; i < totalRows.length; i++) {
                if (totalRows[i].seqNumber == row.seqNumber) {
                    totalRows[i].refEndDate = mini.formatDate(e.value,'yyyy-MM-dd');//转换时间格式设置时间
                }
                if (totalRows[i].refEndDate.length != 10) {
                    totalRows[i].refEndDate = mini.formatDate(totalRows[i].refEndDate,'yyyy-MM-dd');//转换时间格式设置时间
                }
            }

            for (var i = 0; i < totalRows.length; i++) {
                if (totalRows[i].refEndDate == mini.formatDate(e.value,'yyyy-MM-dd')) {
                    sameNumber++;
                }
            }
        }


        for (var i = 0; i < totalRows.length; i++) {
            if (totalRows[i].refEndDate.length != 10) {
                totalRows[i].refEndDate = mini.formatDate(totalRows[i].refEndDate,'yyyy-MM-dd');//转换时间格式设置时间
            }
        }

        if (sameNumber > 1) {
            mini.alert("计息结束日期不能相同","提示");
            return;
        }

        totalRows.sort(function(a, b){
            return a.refEndDate > b.refEndDate ? 1 : -1; // 这里改为大于号
        });

        for (var i = 0; i < totalRows.length; i++) {
            totalRows[i].seqNumber = payOrder++;
        }

        intgrid.clearRows();
        intgrid.setData(totalRows);
        calTotalIntByPayEndDate();
    }

    //本金按期末日期排序
    function sortByEndDate(e){
        var row = amtgrid.getSelected();
        var payOrder = 1;
        var totalRows = amtgrid.getData();
        var sameNumber = 0;

        if (e != 'null') {
            for (var i = 0; i < totalRows.length; i++) {
                if (totalRows[i].seqNumber == row.seqNumber) {
                    totalRows[i].repaymentSdate = mini.formatDate(e.value,'yyyy-MM-dd');//转换时间格式设置时间
                }
                if (totalRows[i].repaymentSdate.length != 10) {
                    totalRows[i].repaymentSdate = mini.formatDate(totalRows[i].repaymentSdate,'yyyy-MM-dd');//转换时间格式设置时间
                }
            }

            for (var i = 0; i < totalRows.length; i++) {
                if (totalRows[i].repaymentSdate == mini.formatDate(e.value,'yyyy-MM-dd')) {
                    sameNumber++;
                }
            }
        }

        for (var i = 0; i < totalRows.length; i++) {
            if (totalRows[i].repaymentSdate.length != 10) {
                totalRows[i].repaymentSdate = mini.formatDate(totalRows[i].repaymentSdate,'yyyy-MM-dd');//转换时间格式设置时间
            }
        }

        if (sameNumber > 1) {
            mini.alert("期末日期不能相同","提示");
            return;
        }

        totalRows.sort(function(a, b){
            return a.endDate > b.endDate ? 1 : -1; // 这里改为大于号
        });

        for (var i = 0; i < totalRows.length; i++) {
            totalRows[i].seqNumber = payOrder++;
        }

        amtgrid.clearRows();
        amtgrid.setData(totalRows);
        calTotalIntByPayEndDate();
    }

    //修改本金金额，重新生成利息计划
    function reCalBackAmt(){

        var grid = mini.get("amt_datagrid");
        var row = grid.getSelected();
        var payOrder = 1;
        var totalRows = grid.getData();
        var sameNumber = 0;
        for (var i = 0; i < totalRows.length; i++) {
            if (totalRows[i].payOrder == row.payOrder) {
                totalRows[i].backAmt = e.value;//转换时间格式设置时间
            }
            if (totalRows[i].endDate && totalRows[i].endDate.length != 10) {
                totalRows[i].endDate = mini.formatDate(totalRows[i].endDate,'yyyy-MM-dd');//转换时间格式设置时间
            }
        }


        grid.clearRows();
        grid.setData(totalRows);
        calTotalIntByPayEndDate();

    }

    //重新生成利息计划
    function calTotalIntByPayEndDate(){
        var intTotalRows = intgrid.getData();
        var amtTotalRows = amtgrid.getData();

        amtgrid.validate();
        if (amtgrid.isValid() == false) {
//           mini.alert("必须输入有效的数据！","系统提示");
            return;
        }

        var actualIntRows = new Array();
        var intNum = 0;
        for (var i = 0; i < intTotalRows.length; i++) {
            if (intTotalRows[i].actualInt != null) {
                actualIntRows[intNum++] = intTotalRows[i];
            }
        }
        var originalActualIntSize = actualIntRows.length;

        var data = {};
        data['amtPlan'] = amtTotalRows;
        data['intPlan'] = intTotalRows;
        data['dealNo'] = ticketId;
        var params=mini.encode(data);
        CommonUtil.ajax({
            url:'/IfsRmbDepositOutController/getInterest',
            data:params,
            callback : function(data) {
                // var payOrderNumber = originalActualIntSize;
                for (var i = 0; i < data.obj.length; i++) {
                    actualIntRows[originalActualIntSize++] = data.obj[i];
                }
                intgrid.setData(actualIntRows);
            }
        });
    }

    //字段必填校验
    function onCellValidation(e) {
        if (e.field == "repaymentSdate") {
            var name=e.value;
            if (name == null || name == '') {
                e.isValid = false;
                e.errorText = "日期必须输入";
            }
        }
        if (e.field == "repaymentSdate") {
            var name=e.value;
            if (name == null || name == '') {
                e.isValid = false;
                e.errorText = "日期必须输入";
            }
        }
        if (e.field == "repaymentSamt") {
            var name=e.value;
            if (name == null || name == '') {
                e.isValid = false;
                e.errorText = "当期还款本金必须输入";
            }
        }
    }

</script>
</body>
</html>