<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
    	<div id="find2" class="fieldset-body">
		<table id="field_form" class="form-table" width="100%">
			<tr>
				<td>opics编号：</td>
				<td>
					<input id="dealno" name="dealno" readonly="true" class="mini-textbox" enabled="false" style="width:90%" />
				</td>
				<td>生成流水号：</td>
				<td>
					<input id="fedealno" name="fedealno" class="mini-textbox" required="true"  enabled="false"
						style="width: 90%" allowInput="true"/>
				</td>
			</tr>
			<tr>
				<td>付款人账号：</td>
				<td>
					<input id="payUserid" name="payUserid" style="width:90%" class="mini-textbox" enabled="false" />
				</td>
				<td>收款人账号：</td>
				<td><input id="recUserid" name="recUserid" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
						onbuttonclick="recUserEdit" allowInput="true" maxLength="32" style="width:90%" class="mini-textbox"
						required="true"  enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>付款人名称</td>
				<td>
				<input id="payUsername" name="payUsername" style="width:90%" readonly="true" class="mini-textbox"
					enabled="false"/>
				</td>
				<td>收款人名称：</td>
				<td>
					<input id="recUsername" name="recUsername" style="width:90%" class="mini-textbox" required="true"
						vtype="maxLength:50" enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>付款行行号：</td>
				<td>
				<input id="payBankid" name="payBankid" style="width: 90%" readonly="true" class="mini-textbox"
					enabled="false" />
				</td>
				<td>收款行行号：</td>
				<td>
					<input id="recBankid" name="recBankid" style="width:90%" required="true"  class="mini-textbox"
						maxLength="14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>付款行名称</td>
				<td>
				<input id="payBankname" name="payBankname" style="width: 90%" readonly="true" class="mini-textbox"
					enabled="false" />
				</td>
				<td>收款行名称：</td>
				<td>
					<input id="recBankname" name="recBankname" style="width:90%" class="mini-textbox" required="true"
                       	vtype="maxLength:50" enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>业务类型：</td>
				<td>
					<input id="hvpType" name="hvpType" style="width:90%" class="mini-combobox" enabled="false"
						data="[{text:'A200-行间划拨',id:'A200'},{text:'A100-普通汇兑',id:'A100'}]"/>
				</td>
				<td>币种代码：</td>
				<td>
					<input id="ccy" name="ccy" readonly="true" style="width:90%" value="RMB=人民币" class="mini-textbox"
						enabled="false" />
				</td>
			</tr>
			<tr>
				<td>金额：</td>
				<td>
					<input id="amount" name="amount" style="width:90%" format="#,0.00" class="mini-textbox" required="true"  enabled="false"/>
				</td>
				<td>手续费：</td>
				<td>
					<input id="amount_1" name="amount_1" readonly="true" style="width:90%" value="0.00"
						class="mini-spinner" changeOnMousewheel="false" enabled="false" />
				</td>
			</tr>
			<tr>
				<td>支付优先级：</td>
				<td>
					<input id="level" name="level" style="width:90%" value="HIGH-紧急" readonly="true" class="mini-textbox"
						enabled="false" />
				</td>
				<td>附言：</td>
				<td>
					<input id="remarkFy" name="remarkFy" style="width:90%" class="mini-textbox" vtype="maxLength:50"
						enabled="false"/>
				</td>
			</tr>
			<tr>
				<td>经办用户：</td>
				<td>
					<input id="ioper" name="ioper" style="width:90%"  class="mini-textbox" enabled="false"/>
				</td>
				<td>复核用户：</td>
				<td>
					<input id="voper" name="voper" style="width:90%" class="mini-textbox"  enabled="false"/>
				</td>
			</tr>
		</table>
	</div>
    
    
    
    <script type="text/javascript">
        mini.parse();
        var url = window.location.search;
        var fedealno=CommonUtil.getParam(url,"fedealno");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            initData();
        });

        //初始化数据
        function initData(){
			CommonUtil.ajax({
				url : '/TrdSettleController/getSettleCnaps',
				data : {
					fedealno : fedealno
				},
				callback : function(text) {
					//资金编号
					var dealno=mini.get("dealno");
					dealno.setValue(text.obj.dealno);
					dealno.setText(text.obj.dealno);
					//审批单编号
					var fedealno=mini.get("fedealno");
					fedealno.setValue(text.obj.fedealno);
					fedealno.setText(text.obj.fedealno);
					//付款人账号
					var payUserid = mini.get("payUserid");
					payUserid.setValue(text.obj.payUserid);
					payUserid.setText(text.obj.payUserid);
					//付款人名称
					var payUsername = mini.get("payUsername");
					payUsername.setValue(text.obj.payUsername);
					payUsername.setText(text.obj.payUsername);
					//付款行行号
					var payBankid = mini.get("payBankid");
					payBankid.setValue(text.obj.payBankid);
					payBankid.setText(text.obj.payBankid);
					//付款行名称
					var payBankname = mini.get("payBankname");
					payBankname.setValue(text.obj.payBankname);
					payBankname.setText(text.obj.payBankname);
					//收款人账号
					var recUserid=mini.get("recUserid");
					recUserid.setValue(text.obj.recUserid);
					recUserid.setText(text.obj.recUserid);
					//收款人名称
					var recUsername=mini.get("recUsername");
					recUsername.setValue(text.obj.recUsername);
					recUsername.setText(text.obj.recUsername); 
					//收款行行号
					var recBankid=mini.get("recBankid");
					recBankid.setValue(text.obj.recBankid);
					recBankid.setText(text.obj.recBankid);
					//收款行名称
					var recBankname=mini.get("recBankname");
					recBankname.setValue(text.obj.recBankname);
					recBankname.setText(text.obj.recBankname);
					//业务类型
					var hvpType=mini.get("hvpType");
					hvpType.setValue(text.obj.hvpType);
					//金额
					var amount=mini.get("amount");
					amount.setValue(text.obj.amount);
					amount.setText(text.obj.amount);
					//附言
					var remarkFy=mini.get("remarkFy");
					remarkFy.setValue(text.obj.remarkFy);
					remarkFy.setText(text.obj.remarkFy);
					
					var remarkFy=mini.get("ioper");
					remarkFy.setValue(text.obj.iopername);
					remarkFy.setText(text.obj.iopername);
					
					var remarkFy=mini.get("voper");
					remarkFy.setValue(text.obj.vopername);
					remarkFy.setText(text.obj.vopername);
					
				}
			}); 
        }
        

    </script>
</body>
</html>