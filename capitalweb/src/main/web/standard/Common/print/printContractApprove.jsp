<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
	<style>
	table th{
	text-align:left;
	padding-left:10px;
	width:150px;
    border-right: 0;
    }
 .header {
	    height: 35px;
	    /* background: #95B8E7; */
	    color: #404040;
	    background: linear-gradient(to bottom,#EFF5FF 0,#E0ECFF 100%);
	    background-repeat: repeat-x;
	    line-height:35px; /*设置line-height与父级元素的height相等*/
	    text-align: center; /*设置文本水平居中*/
	    font-size:18px;
		}
		 @page { margin: 0; }
	</style>
</head>
<body style="width:100%;height:100%;background:white">
<div id="layout1" class="mini-layout" style="width:100%;height:100%;">
    <div title="center" region="center" bodyStyle="overflow:hidden;">
        <iframe id="mainframe" frameborder="0" name="main" style="width:100%;height:100%;" border="0">
        </iframe>
    </div>	
	<div title="south" region="south" showSplit="false" showHeader="false" height='39px'   style="line-height:32px;text-align: center;background: linear-gradient(to bottom,#EFF5FF 0,#f9f4d9 100%);">
       <div id="functionIds" style="padding-top:5px;">
			<a class="mini-button" style="display: none"  style="width:120px;" id="print_btn" onClick="printdiv();">打印单据</a>
		<!-- <a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a> -->
		</div>
    </div>
  </div>
	<script type="text/javascript">
	mini.parse();
    var decode_url = decodeURI(location.href);
	var afterUrl= decode_url.substring(1);
	var params= (afterUrl.substring(afterUrl.indexOf('=')+1)).split('|');
	var ticketId=params[0];
	var prdNo=params[1];
	var prdName=params[2];
	
	
    var iframe = document.getElementById("mainframe");
  	//获取当前活动ID
  	if(prdNo=='411'){//外汇即期
  		iframe.src =CommonUtil.baseWebPath() +'/cfetsfx/rmbsptEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='391'){//外汇远期
  		iframe.src =CommonUtil.baseWebPath() +'/cfetsfx/rmfwdEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='432'){//外汇掉期
  		iframe.src =CommonUtil.baseWebPath() +'/cfetsfx/rmswapEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='438'){//外币拆借
  		iframe.src =CommonUtil.baseWebPath() +'/cfetsfx/ccyLendingEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='447'){//黄金即期
  		iframe.src =CommonUtil.baseWebPath() +'/cfetsmetal/goldsptEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='448'){//黄金远期
  		iframe.src =CommonUtil.baseWebPath() +'/cfetsmetal/goldfwdEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='449'){//黄金掉期
  		iframe.src =CommonUtil.baseWebPath() +'/cfetsmetal/goldswapEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='446'){//质押式回购
        iframe.src =CommonUtil.baseWebPath() +'/cfetsrmb/pledgeRepoEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
    }else if(prdNo=='442'){//买断式回购
        iframe.src =CommonUtil.baseWebPath() +'/cfetsrmb/repoEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
    }else if(prdNo=='467'){//国库定期存款
  	    iframe.src =CommonUtil.baseWebPath() +'/cfetsrmb/nationalDepositEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='468'){//交易所回购
  	    iframe.src =CommonUtil.baseWebPath() +'/cfetsrmb/exchangeRepoEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='469'){//常备借贷便利
  	    iframe.src =CommonUtil.baseWebPath() +'/cfetsrmb/standingLendingEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
  	}else if(prdNo=='470'){//中期借贷便利
        iframe.src =CommonUtil.baseWebPath() +'/cfetsrmb/MediumLendingEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
    }else if(prdName=='afp'){//基金申购
        iframe.src =CommonUtil.baseWebPath() +'/refund/CFundAfpEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
    }else if(prdName=='rdp'){//基金赎回
        iframe.src =CommonUtil.baseWebPath() +'/refund/CFundRdpEdit.jsp?action=print&prdNo='+prdNo+'&ticketid='+ticketId;
    }
  	
  	
	 /***去除页眉页脚***/
	  function remove_ie_header_and_footer() {
		  var hkey_path;
		  hkey_path = "HKEY_CURRENT_USER\\Software\\Microsoft\\Internet Explorer\\PageSetup\\";
		  try {
		   var RegWsh = new ActiveXObject("WScript.Shell");
		   RegWsh.RegWrite(hkey_path + "header", "");
		   RegWsh.RegWrite(hkey_path + "footer", "");
		  } catch (e) {
		  }
		 }
	  
	/***打印***/
	function printdiv(){
		var iframe = $('#mainframe').contents().find("#field_form");
		var elements=iframe[0].getElementsByTagName("input");
		var text=iframe[0].getElementsByTagName("textarea");
		for(var i=0; i< elements.length; i++){
             elements[i].setAttribute("value",elements[i].value);
        }
		for(var i=0; i< text.length; i++){
			text[i].innerHTML=text[i].value;
			// var id=(text[i].id).substring(0,(text[i].id).indexOf('$'));
		}
	   document.body.innerHTML='<br/><h2 style="text-align:center">哈尔滨银行'+prdName+'业务审批单</h2>'+iframe.html();
		
	    if (!!window.ActiveXObject || "ActiveXObject" in window) { //是否ie
				remove_ie_header_and_footer();
			}
		window.print(); 
		window.location.reload(true);//刷新页面使页面功能正常
		return false;
	};
	
</script>
</body>
</html>