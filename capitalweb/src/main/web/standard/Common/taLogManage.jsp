<%@ page import="java.util.Date" %>
<%@ page import="com.singlee.capital.common.util.DateUtil" %>
<%@ page import="org.apache.commons.lang3.time.DateUtils" %>
<%@ page import="java.util.Calendar" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title>操作日志</title>
</head>
<body style="width: 100%; height: 100%; background: white">
<fieldset class="mini-fieldset title">
    <legend>记录查询</legend>
    <div id="search_form">
        <nobr>
            <input id="startDate" name="startDate" class="mini-datepicker" width="320px" labelField="true" label="操作时间："
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
            <input id="endDate" name="endDate" class="mini-datepicker" width="320px" labelField="true" label="～"
                   labelStyle="text-align:center;"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
        </nobr>
        <input id="reqDesc" name="reqDesc" class="mini-combobox" width="320px" textField="busspnm" valueField="bussType"
               width="280px" emptyText="请选择操作类型" labelField="true" label="操作类型：" labelStyle="text-align:right;"/>
        <input id="reqUserId" name="reqUserId" class="mini-textbox" width="320px" labelField="true" label="请求人ID："
               emptyText="请填写请求人ID"
               labelStyle="text-align:right;"/>

        <span style="float:right;margin-right: 150px">
            <a class="mini-button" id="search_btn" onclick="search(10,0)">查询</a>
            <a class="mini-button" id="clear_btn" onclick="clear()">清空</a>
            <a class="mini-button" id="print_btn" onclick="printView()">打印预览</a>
        </span>
    </div>
</fieldset>

<div id="mainGrid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
     allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true"
     onshowrowdetail="onShowRowDetail">
    <div property="columns">
        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
        <div field="reqUserId" headerAlign="center" align="center" allowSort="true" width="100px">请求人ID</div>
        <div field="reqUserName" headerAlign="center" align="center" allowSort="true" width="100px">请求人姓名</div>
        <div field="rspDesc" headerAlign="center" align="center" allowSort="true" width="100px">操作类型</div>
        <div field="reqTime" headerAlign="reqTime" allowSort="true" width="120px">操作时间</div>
        <div field="reqIp" headerAlign="center" allowSort="true" width="100px">请求IP</div>
        <div field="rspObject" headerAlign="center" allowSort="true" width="200px">操作内容</div>
        <div field="reqUrl" headerAlign="center" allowSort="true" width="200px">请求URL</div>
        <div field="reqMethod" headerAlign="reqMethod" allowSort="true" width="150px">请求方法</div>
    </div>
</div>
<script>
    mini.parse();
    var grid = mini.get("mainGrid");
    $(document).ready(function () {
        search(10, 0);
        loadreqDesc();
        mini.get("startDate").setValue(new Date());
        mini.get("endDate").setValue(new Date());
    });

    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    function search() {
        search(grid.pageSize, 0);
    }

    function search(pageSize, pageIndex) {
        let form = new mini.Form("search_form");
        let data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        let url = "/LogController/searchPageLog";
        let params = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: params,
            callback: function (data) {
                //设置主页面信息
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    function loadreqDesc() {
        CommonUtil.ajax({
            data: {},
            url: "/InterfaceController/queryInterfaceInfo",
            callback: function (data) {

                var length = data.obj.length;
                var jsonData = new Array();
                for (var i = 0; i < length; i++) {
                    var bussType = data.obj[i].infaceName;
                    var bussTpnm = data.obj[i].infaceName;
                    jsonData.add({'bussType': bussType, 'busspnm': bussTpnm});
                }
                mini.get("reqDesc").setData(jsonData);
            }
        });

    }

    function clear() {
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("endDate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }


</script>
<script type="text/javascript" src="<%=basePath%>/miniScript/lodop/LodopFuncs.js"></script>
<script language="javascript" type="text/javascript">
    var LODOP; //声明为全局变量
    function printView() {
        var print = {
            "paybankname": "1",
            "recvaccname": "2",
            "paybankno": "3",
            "recvaccno": "4",
            "recvbankname": "5",
            "mbfememo": "6",
            "dealno": "7",
            "coreflow": "8",
            "ioper": "9",
            "voper": "10",
            "payoper": "11",
            "payauthoper": "12",
            "authorization": "13",
            "recvbankno": "14"
        }
        var amount = 9123456789.12;
        var amounts = 0;
        var amountint = 0;
        var amountdecimal = 0;
        if (amount != 0) {
            amounts = Math.abs(amount);
            if (amounts.toString().indexOf(".") > 0) {
                var amounttemp = amounts.toString().split(".");
                amountint = amounttemp[0];
                amountdecimal = amounttemp[1];
            } else {
                amountint = amounts;
            }
        }
        var temp = CommonUtil.chineseNumber(amount);

        CreateFullDvp(print, amountint, amountdecimal, temp);
        LODOP.PREVIEW();
        // LODOP.PRINT_DESIGN();
    };

    /**
     *
     **/
    function CreateFullDvp(print, amountint, amountdecimal, temp) {
        //开始打印处理
        LODOP = getLodop();
        //Top,Left,Width,Height
        LODOP.PRINT_INITA(-2, -48, 830, 700, "套打EMS的模板");
        LODOP.SET_LICENSES("哈尔滨银行", "92EC9F078EE17DE9F89608BCA683CAEA", "", "");
        LODOP.ADD_PRINT_SETUP_BKIMG("<IMG SRC='<%=basePath%>/miniScript/images/print_fm.jpg'/>");
        LODOP.SET_SHOW_MODE("BKIMG_LEFT", 10);
        LODOP.SET_SHOW_MODE("BKIMG_TOP", -4);
        LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW", true);
        //委托人-全称
        LODOP.ADD_PRINT_TEXT(372, 240, 203, 24, print.paybankname);
        //收款人-全称
        LODOP.ADD_PRINT_TEXT(372, 495, 225, 24, print.recvaccname);
        //委托人-帐户或地址
        LODOP.ADD_PRINT_TEXT(400, 240, 158, 24, print.paybankno);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
        //收款人-帐户或地址
        LODOP.ADD_PRINT_TEXT(400, 495, 225, 24, print.recvaccno);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
        //委托人-开户行名称
        LODOP.ADD_PRINT_TEXT(430, 240, 149, 24, "哈尔滨银行结算中心");
        //委托人-开户行名称
        LODOP.ADD_PRINT_TEXT(430, 495, 225, 24, print.recvbankname);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);
        //用途
        LODOP.ADD_PRINT_TEXT(514, 145, 287, 28, print.mbfememo);
        //大额支付系统行号
        LODOP.ADD_PRINT_TEXT(514, 567, 147, 25, print.recvbankno);
        //交易号
        LODOP.ADD_PRINT_TEXT(316, 120, 153, 24, "交易号: " + print.dealno);
        //交易流水
        LODOP.ADD_PRINT_TEXT(316, 500, 175, 24, "交易流水号: " + print.coreflow);
        //初审
        LODOP.ADD_PRINT_TEXT(585, 103, 105, 38, print.ioper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //复审
        LODOP.ADD_PRINT_TEXT(586, 228, 114, 38, print.voper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //副总经理(或助力)审
        LODOP.ADD_PRINT_TEXT(588, 353, 102, 38, print.payoper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //总经理审批
        LODOP.ADD_PRINT_TEXT(585, 485, 98, 38, print.payauthoper);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //主管行长审批
        LODOP.ADD_PRINT_TEXT(587, 612, 101, 38, print.authorization);
        LODOP.SET_PRINT_STYLEA(0, "Alignment", 2);
        //金额
        LODOP.ADD_PRINT_TEXT(479, 682, 14, 28, AmountAdjustment(amountint, 1));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 668, 14, 28, AmountAdjustment(amountint, 2));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 654, 14, 28, AmountAdjustment(amountint, 3));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 640, 14, 28, AmountAdjustment(amountint, 4));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 626, 14, 28, AmountAdjustment(amountint, 5));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 612, 14, 28, AmountAdjustment(amountint, 6));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 597, 14, 28, AmountAdjustment(amountint, 7));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 582, 14, 28, AmountAdjustment(amountint, 8));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 569, 14, 28, AmountAdjustment(amountint, 9));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 553, 14, 28, AmountAdjustment(amountint, 10));
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 697, 14, 28, amountdecimal.toString().charAt(0) / 1);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(479, 710, 14, 28, amountdecimal.toString().charAt(1) / 1);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 12);
        LODOP.ADD_PRINT_TEXT(465, 246, 271, 36, temp);
        LODOP.SET_PRINT_STYLEA(0, "FontSize", 10);

    }

    /**
     * 金额拆分
     * @param amount
     * @param number
     * @returns {number|string}
     * @constructor
     */
    function AmountAdjustment(amount, number) {
        var value;
        var amounts = amount.toString().charAt(amount.toString().length - number) / 1;
        var amounttop = amount.toString().substring(0, amount.toString().length - number + 1) / 1;
        if (amounttop == 0) {
            if (amounts != 0) {
                value = amounts;
            } else {
                value = "";
            }
        } else {
            if (amounts != 0) {
                value = amounts;
            } else {
                value = 0;
            }
        }
        return value;
    }
</script>
