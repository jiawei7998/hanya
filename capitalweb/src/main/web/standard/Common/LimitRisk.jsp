<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>风险审查单号选择</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>风险审查单号查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="applyProd" name="applyProd" class="mini-textbox" labelField="true"  label="产品名称："    width="330px"  labelStyle="text-align:right;width:140px"  emptyText="请输入产品名称" />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="cust_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>

			<div field="approveFlno" width="160px" align="center"  headerAlign="center" >审批件编号</div>
			<div field="orderId" width="180px" align="center"  headerAlign="center" >产品编码</div>    
			<div field="productName" width="100px" align="center"  headerAlign="center" >产品名称</div>
			<div field="tranType" width="150px" align="center"  headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'tranType'}">交易类型</div> 
			<div field="tranStatus" width="150px" align="center"  headerAlign="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'tranStatus'}">交易状态</div>                            
			<div field="inputTelNo" width="150px" align="center"  headerAlign="center" >录入柜员编号</div>                            
			<div field="yearRate" width="150px" align="center"  headerAlign="center" >年化收益率</div>                                
			<div field="investPlan" width="190px" align="center"  headerAlign="center" >投资计划</div>
			<div field="prodExpDate" width="220px" align="center"  headerAlign="center" >产品到期日期</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("cust_grid");
	var row="";

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}

	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}
		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['localstatus']="1";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:"/IfsOpicsRiskController/searchPageOpicsRisk",
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	
	$(document).ready(function() {
		search(10, 0);
    });
    

    function GetData() {
        var row = grid.getSelected();
        return row;
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
</script>
</body>
</html>
