<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>代审批信息</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
	<fieldset class="mini-fieldset">
		<legend>基本信息</legend>
        <table id="auth_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
		    <tr>
	            <td style="width:50%;vertical-align: top;">
		        	<input id="auth_userid" name="auth_userid" class="mini-combobox" style="width:100%" label="代审批人"
		        	labelStyle="text-align:left;width:200px" labelField="true" required="true"  validateOnLeave="true"
					requiredErrorText="代审批人不能为空"/>
	            </td>
		        <td style="width:50%">
		        	<input id="role" name="role" class="mini-combobox" style="width:100%" label="审批组"
		        	labelStyle="text-align:left;width:200px" labelField="true" multiSelect="true"/>
	            </td>
	        </tr>
	        <tr>
	            <td style="width:50%;vertical-align: top;">&nbsp;</td>
		        <td style="width:50%"><span style="color:#ff3333">*审批组为空则默认授权您名下的所有审批组</span></td>
	        </tr>
            <tr>
	            <td style="width:50%;vertical-align: top;">
		            <input id="auth_begdate" name="auth_begdate" class="mini-datepicker" style="width:100%" label="开始日期"
		            labelStyle="text-align:left;width:200px" labelField="true" required="true"  validateOnLeave="true"
							requiredErrorText="开始日期不能为空"/>
		        </td>
		        <td style="width:50%">
		        	<input id="auth_enddate" name="auth_enddate" class="mini-datepicker" style="width:100%" label="结束日期"
		        	labelStyle="text-align:left;width:200px" labelField="true" required="true"  validateOnLeave="true"
							requiredErrorText="结束日期不能为空"/>
	            </td>
	        </tr>
        </table>
    </fieldset>
	<table style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
	    <tr>
	        <td colspan="2" style="text-align:center;">
	            <a id="save_btn" class="mini-button" style="display: none"   onclick="save">保存</a>
	            <a id="cancel_btn" class="mini-button" style="display: none"   onclick="cancel">取消</a>
	        </td>
	    </tr>
	</table>
    <script>
        $(document).ready(function(){
        	getAuthUser();
        	getRoleList();
        });

        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");

        //获取可授权人列表
        function getAuthUser(){
     	   CommonUtil.ajax({
  			  url:"/UserController/selectUserWithInstIds",
                data:{"instIds":'<%=__sessionUser.getInstId()%>',"branchId":'<%=__sessionUser.getBranchId()%>'},
             	  callback:function(data){
                 	  var combo = mini.get("auth_userid");
 	                   var result = new Array();
 	                   $.each(data.obj,function(i,item){
 	                	   if(item.userId == '<%=__sessionUser.getUserId()%>'){
 	                		   return true;
 	                	   }
 	            		   result.push({"id":item.userId, "text":item.userName + "(" + item.userId + ")"});
 	                   });
                 	  combo.setData(result);
                }
  		   });
        }
        
        //当前用户拥有的所有审批组
        function getRoleList(){
        	CommonUtil.ajax( {
        		url:'/FlowRoleController/listFlowRoleUserMap',
        		data:{}, 
        		callback : function(data) {
        			var combo = mini.get("role");
        			var roleList = new Array();
        			$.each(data.obj,function(i,item){
        				roleList.push({"id":item.role_id,"text":item.role_name});
        			});
        			//组件:审批组列表
        			combo.setData(roleList);
        		}
        	});
        }
        
        //保存
    	function save(){
            var form = new mini.Form("auth_form");
            form.validate();
 	        if (form.isValid() == false){
                mini.alert("请输入有效的数据!","系统提示");
                return;//表单验证
            }
    		var urls = "/FlowRoleController/saveFlowRoleUserMap";
    		var param = new Array();
    		var cpObj = form.getData();
    		cpObj.auth_begdate = mini.get("auth_begdate").getFormValue();
    		cpObj.auth_enddate = mini.get("auth_enddate").getFormValue();
    		if(CommonUtil.isNull(cpObj.role)){
    			cpObj.role_id = '*'; //注明是统一授权
    			param.push(cpObj);
    		}else{
    			var role = cpObj.role.split(",");
    			$.each(role,function(i,item){
    				var obj = {
    					"auth_begdate":cpObj.auth_begdate,
    					"auth_enddate":cpObj.auth_enddate,
    					"auth_userid":cpObj.auth_userid,
    					"user_id":'<%=__sessionUser.getUserId() %>',
    					"role_id":item
    				};
    				param.push(obj);
    			});
    		}
    		CommonUtil.ajax( {
    			url:urls,
    			data:param, 
    			callback : function(data) {
				   if('error.common.0000' == data.code){
			           mini.alert("保存成功!","系统提示",function(value){
                           //关闭并刷新
					       setTimeout(function(){ top["win"].closeMenuTab() },100);
                       });
                   }else{
                       mini.alert("保存失败！","系统提示");
                   }
    			}
    		});
    	}
    </script>
</body>
</html>