<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
	<head>
	    <title>代审批授权</title>
	    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 	
	</head>
	<body style="width:100%;height:100%;background:white">
		<div class="mini-panel" title="代审批信息" style="width:100%">
		<div style="width:100%;padding-top:10px;">
			<div id="search_form" class="mini-form" width="80%">
				<input id="user_id" name="user_id" class="mini-textbox" readonly value="<%=__sessionUser.getUserId() %>"  labelField="true"  label="用户编号："/>
				<input id="user_name" name="user_name"  class="mini-textbox" readonly value="<%=__sessionUser.getUserName() %>"  labelField="true"  label="用户名称："/>
				<span style="float:right;margin-right: 150px">
					<a id="search_btn" class="mini-button" style="display: none"  onclick="search()">刷新</a>
				</span>
			</div>
		</div>
	
		<table width="100%">
			<tr>
				<td width="60%" style="padding:10px 0 5px 5px">
	                <a id="add_btn" class="mini-button" style="display: none"   onclick="add">代审批</a>
	                <a id="delete_btn" class="mini-button" style="display: none"    onclick="clear">取消</a>
				</td>
				<td width="40%">
					<div id = "roleUserMapType" name = "approveType" class="mini-checkboxlist" style="float:right;width:100%; " 
						labelField="true" label="代审批列表选择：" labelStyle="text-align:right;" =value="approve" textField="text" 
						valueField="id" multiSelect="false" data="[{id:'authority',text:'代审批信息'}, {id:'history',text:'代审批历史'}]" 
						onvaluechanged="checkBoxValuechanged"></div>
				</td>
			</tr>
		</table>
		</div>
		<div class="mini-fit">      
			<div id="auth_grid"  class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
			allowAlternating="true" allowResize="true" border="true"  showPager="false"
			multiSelect="false" sortMode="client">
				<div property="columns">
				<div type="indexcolumn" align="center" headerAlign="center">序号</div>
                <div field="role_id" width="100" align="center" headerAlign="center" allowSort=nihj"true">审批组编号</div>
                <div field="role_name" width="130" align="center" headerAlign="center" allowSort="true">审批组名称</div>
                <div field="auth_userid" width="130" align="center" headerAlign="center" allowSort="true">代审批用户</div> 
                <div field="auth_begdate" width="130" align="center" headerAlign="center" allowSort="true">代审批开始日期</div>   
                <div field="auth_enddate" width="130" align="center" headerAlign="center" allowSort="true">代审批结束日期</div>
                <div field="operate_type" width="130" align="center" headerAlign="center" allowSort="true" data-options="{dict:'OperationType'}">操作类型</div>   
                <div field="operate_time" width="130" align="center" headerAlign="center" allowSort="true">操作时间</div>
			</div>  
		</div>
	</body>
	<script>
	    mini.parse();
	    top["roleUserMap"]=window;
	    
	    $(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var grid = mini.get("auth_grid");
				grid.on("beforeload", function (e) {
					search();
				});

				grid.hideColumn(6);
				grid.hideColumn(7);
				search();
			});
	    });
	   
	    //查询代审批
	    function search(){
            var param = {};
            var roleUserMapType=mini.get("roleUserMapType").getValue();
            if(roleUserMapType == "history"){//代审批历史
            	var urls = "/FlowRoleController/pageFlowRoleUserMapHis";
            	param['pageNumber']=1;
            	param['pageSize']=1000000000;
            	param['user_id']="<%=__sessionUser.getUserId() %>";
            }else{
            	var urls = "/FlowRoleController/listFlowRoleUserMap";
            }
            param['branchId']='<%=__sessionUser.getBranchId()%>';
            CommonUtil.ajax({
                url:urls,
                data:param,
                callback:function(data){
                    var grid =mini.get("auth_grid");
                    //设置数据
                    if(roleUserMapType == "history"){//代审批历史
                    	grid.setData(data.obj.rows);
                    }else{
                		grid.setData(data.obj);
                	}
                }
            });
	    }
	    
	    //切换模式
		function checkBoxValuechanged(e){
			search();
			var roleUserMapType=this.getValue();
			var grid =mini.get("auth_grid");
			if(roleUserMapType == "history"){//历史
				mini.get("add_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				grid.showColumn(6);
				grid.showColumn(7);
			}else{//代审批信息
				mini.get("add_btn").setVisible(true);
				mini.get("add_btn").setEnabled(true);
				mini.get("delete_btn").setVisible(true);
				mini.get("delete_btn").setEnabled(true);
				grid.hideColumn(6);
				grid.hideColumn(7);
			}
		}
		
		//弹出 统一代审批 页面
		function add(){
	        var url = CommonUtil.baseWebPath() + '/../Common/Flow/MiniRoleUserMapEdit.jsp';
	        var tab = { id: "roleUserMapManageAdd", name: "roleUserMapManageAdd", text: "代审批新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
	        top['win'].showTab(tab);
		}
		
		//清除代审批
		function clear(){
			//编辑模式，必须选中一条记录
			var selected = mini.get("auth_grid").getSelected();
			var str;
			if(!selected){
				mini.alert("请选择一条数据");
				return false;
			}
			str=selected.role_name;
			//提示
            mini.confirm('您确认要取消代审批吗？','系统提示', function (action) {
                if (action == 'ok') {
				    CommonUtil.ajax({
						url:"/FlowRoleController/resetFlowRoleUserMap",
						data:selected,
						callback : function(data) {
							mini.alert("取消代审批成功.","系统提示");
							search();
						}
					});   
			    }
			});
		}
	</script>
</html>