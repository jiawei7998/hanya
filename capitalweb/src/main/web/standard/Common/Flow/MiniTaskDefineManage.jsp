<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>审批组定义</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-fit" style="width:100%;height:100%;">
    <div class="mini-splitter" style="width:100%;height:100%;" allowResize="false" borderStyle="border:solid 1px #aaa;">
    <div size="50%">
	    <fieldset class="mini-fieldset" style="height:15%">
			<legend>流程查询</legend>	
			<div>
				<div id="search_form" style="width:100%!important; ">
					<input id="prd_no" name="prd_no" required="true"  class="mini-combobox" labelField="true"  label="产品"  width="330px"  labelStyle="text-align:left;width:140px" onValueChanged="getFlowType" />
					<input id="flow_type" name="flow_type" required="true"  class="mini-combobox" labelField="true"  label="流程类型" width="330px"  labelStyle="text-align:left;width:140px"  />
					<span style="float: right; margin-right: 60px"> 
						<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
						<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
					</span>
				</div>
			</div>
		</fieldset> 
		<div class="mini-fit"  style="height:45%">
			<div id="flow_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
				onrowclick="showTaskDef" allowResize="true" sortMode="client" allowAlternating="true">
				<div property="columns">
					<div field="flow_id" width="150" align="center"  headerAlign="center" >流程ID</div>    
					<div field="flow_name" width="180"  headerAlign="center" align="center" >流程名称</div>
					<div field="version" width="120" align="center" headerAlign="center"  >版本</div>
				</div>
			</div>
		</div>  
        <div class="mini-fit" style="height:40%">
	        <div id="task_def_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
	            idField="p_code" allowResize="true"  allowAlternating="true"
	            onrowclick="showDefInfo" sortMode="client">
	                <div property="columns" >
	                    <div type="indexcolumn" align="center" headerAlign="center">序号</div>
	                    <div field="Id" width="150" align="center" headerAlign="center" allowSort="true">节点ID</div>    
	                    <div field="Name.expressionText"  width="100"  align="center" headerAlign="center" >节点名称</div>                                                                                                                        
	                </div>
	        </div>
	    </div>
    </div>
    <div size="50%">
        <fieldset style="height:100%">
            <legend><label>审批界面url</label></legend>
            <div>
                <a id="add_btn" class="mini-button" style="display: none"   onclick="editUrl">新增</a>
                <a id="delete_btn" class="mini-button" style="display: none"    onclick="deleteUrl">删除</a>
            </div>
            <div class="mini-fit">
	            <div id="url_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true"
	                idField="p_code" allowResize="true" pageSize="10" multiSelect="true" sortMode="client">
	                <div property="columns" >
	                    <div type="indexcolumn" align="center" headerAlign="center">序号</div>
	                    <div field="urls" width="100" headerAlign="center">界面地址</div>
	                </div>
	            </div>
	        </div>
        </fieldset>
    </div>
    </div>
</div>
</body>
<script>
    mini.parse();
    top["taskDefine"]=window;


     //查询
    function search(){
	    var form =new mini.Form("search_form");
	    form.validate();
	    if (form.isValid() == false) return;//表单验证
   		var params = {
			"productType":mini.get('#prd_no').getValue(),
			"flowType":mini.get('#flow_type').getValue()
		};
   		
		CommonUtil.ajax({
			url:'/FlowController/searchProcDefByPrdType',
			data:params,
			callback:function(data){
				mini.get('#flow_grid').setData(data.obj);
			}
		});
	} 
    
    //清空
    function clear(){
       var form=new mini.Form("search_form");
       form.clear();
       search(10,0);
    }
    
    //查询条件 产品
	function searchPrd(){
		CommonUtil.ajax({
			url:"/ProductController/searchProduct",
			data:{"branchId":"<%=__sessionUser.getBranchId()%>","prdInst":"<%=__sessionUser.getInstId()%>"},
			callback : function(data) {
				var prd_no=mini.get("prd_no");
				var arr = new Array();
				$.each(data.obj,function(i,item){
					arr.push({"id":item.prdNo, "text":item.prdName});
				});
				prd_no.setData(arr);
			}
		});
	}
    
	//查询条件 流程类型
    function getFlowType(){
		var prd = mini.get("prd_no").getValue();
		var flowType = CommonUtil.serverData.dictionary.FlowType;
		mini.get("flow_type").setData([]);
		//加载流程类型信息
		CommonUtil.ajax({
			url:"/ProductTypeController/getFlowTypeByProductType",
			data:{"prdTypeNo":prd,"branchId":"<%=__sessionUser.getBranchId()%>","prdInst":"<%=__sessionUser.getInstId()%>"},
			callback:function(data){
				if(!data.obj){
					return false;
				}
				var flowTypeSelect=[];
				$.each(flowType, function(i, itemf) {
					$.each(data.obj, function(j, item) {
						if(itemf.id == item.flowTypeNo) {
							var info ={id:itemf.id,text:itemf.text};
							flowTypeSelect.push(info);
						}
					});
				});
				mini.get("flow_type").setData(flowTypeSelect);
			}
		});
	}
    
    function showTaskDef(){
        var selected = mini.get("flow_grid").getSelected();
        if(!selected){
        	return false;
        }
        CommonUtil.ajax({
			url:'/FlowController/getFlowTaskListFlowId',
			data:{flowId:selected.flow_id},
			callback:function(data){
				mini.get('#task_def_grid').setData(data.obj);
			}
		});
    }
    
    function showDefInfo(){
        var prd_no = mini.get("prd_no").getValue();
		var flow = mini.get("flow_grid").getSelected();
		var row = mini.get("task_def_grid").getSelected();
		if(row){
			showUrl(flow.flow_id,row.Id,prd_no);
		}
    }
    
    function getData(){
    	var param = {
    		"prd_no": mini.get("prd_no").getValue(),
    		"flow_id": mini.get("flow_grid").getSelected().flow_id,
    		"task_def_key":mini.get("task_def_grid").getSelected().Id
    	};
        return param;
    }
    
    function showUrl(flowId,taskId,prdCode){
        CommonUtil.ajax({
			url:'/FlowController/getUserTaskUrl',
			data:{"procDefId":flowId,"taskDefKey":taskId,"prdCode":prdCode},
			callback:function(data){
				var result = new Array();
				if(data.obj){
					result.push(data.obj);
				}
				mini.get('#url_grid').setData(result);
			}
		});
    }
    
    function deleteUrl(flowId,taskId,prdCode){
    	var selected = mini.get('#url_grid').getSelected();
    	if(!selected){
    		return false;
    	}
    	mini.confirm("您确认要删除选中的?","系统警告",function(value){
			if(value=="ok"){
		        CommonUtil.ajax({
					url:'/FlowController/deleteFlowTaskUrl',
					data:selected,
					callback:function(data){
		        		showDefInfo();
					}
				});
			}
    	});
    }
    
    function editUrl(){
	    mini.open({
	        url: CommonUtil.baseWebPath() + "../../Common/Flow/MiniTaskFormUrlEdit.jsp",
	        title: "url编辑",
	        width: 500,
	        height: 300,
	        ondestroy: function (action) {
	    		if(action && action == "ok"){
	    			showDefInfo();
	    		}
	        }
	    });
    }
    
    $(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			searchPrd();
		});
    });

</script>

</html>