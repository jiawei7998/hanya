<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>授权信息维护</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
	<fieldset class="mini-fieldset">
		<legend>基本信息</legend>	
        <table id="auth_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
            <tr>
	            <td style="width:100%;vertical-align: top;" colspan = '2'>
		            <input id="from_user_id" name="from_user_id" class="mini-combobox" labelField="true" label="被授权人" onvalueChanged="onUserChanged()"
		            popupWidth="100%" required="true"  style="width:100%" labelStyle="text-align:left;width:200px"/>
		            <input id="to_user_id" name="to_user_id" class="mini-textbox" labelField="true" label="申请人"
		            popupWidth="100%" required="true"  style="width:100%" labelStyle="text-align:left;width:200px"/>
		        </td>
		    </tr>
		    <tr>
	            <td style="width:50%;vertical-align: top;">
	            	<input id="flow_id" name="flow_id" class="mini-buttonedit mini-mustFill" onbuttonclick="onFlowClick" 
	            	labelField="true"  label="流程" style="width:100%;"  labelStyle="text-align:left;width:200px;" />
	            </td>
		        <td style="width:50%">
		        	<input id="task_def_key" name="task_def_key" class="mini-combobox" style="width:100%" label="节点"
		        	labelStyle="text-align:left;width:200px" labelField="true"/>
	            </td>
	        </tr>
            <tr>
	            <td style="width:50%;vertical-align: top;">
		            <input id="auth_start_date" class="mini-datepicker" style="width:100%" label="开始日期"
		            labelStyle="text-align:left;width:200px" labelField="true"/>
		        </td>
		        <td style="width:50%">
		        	<input id="auth_end_date" class="mini-datepicker" style="width:100%" label="结束日期"
		        	labelStyle="text-align:left;width:200px" labelField="true"/>
	            </td>
	        </tr>
        </table>
    </fieldset>
    <fieldset class="mini-fieldset">
		<legend>规则信息</legend>	
	    <table id="rule_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
	        <tr>
	            <td style="width:50%;vertical-align: top;">
		            <input id="amt" name="amt" class="mini-textbox" labelField="true" label="限额"
		            popupWidth="100%" style="width:100%" labelStyle="text-align:left;width:200px"/>
		        </td>
		        <td style="width:50%;vertical-align: top;">
	            	<input id="ccy" name="ccy" class="mini-combobox" labelField="true"  label="币种" 
	            	data="CommonUtil.serverData.dictionary.Currency"
	            	style="width:100%;"  labelStyle="text-align:left;width:200px;" />
	            </td>
		    </tr>
		    <tr>
	            <td style="width:50%;vertical-align: top;">
	            	<input id="prd_no" name="prd_no" class="mini-combobox" labelField="true" multiSelect="true"
	            	label="业务类型" style="width:100%;"  labelStyle="text-align:left;width:200px;" />
	            </td>
		        <td style="width:50%">
	            </td>
	        </tr>
	        <tr>
	            <td style="width:50%;vertical-align: top;">
		        </td>
		        <td style="width:50%">
	            </td>
	        </tr>
	    </table>
	</fieldset>
	<table style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
	    <tr>
	        <td colspan="2" style="text-align:center;">
	            <a id="save_btn" class="mini-button" style="display: none"   onclick="save">保存</a>
	            <a id="cancel_btn" class="mini-button" style="display: none"   onclick="cancel">取消</a>
	            <a id="pass_btn" class="mini-button" style="display: none"   onclick="approve(6)">同意</a>
	            <a id="reject_btn" class="mini-button" style="display: none"   onclick="approve(7)">拒绝</a>
	            <a id="key_btn" class="mini-button" style="display: none"   onclick="getKey()">获得key</a>
	        </td>
	    </tr>
	</table>
    <script>
        $(document).ready(function(){
            mini.parse();
            if(action == "new"){
	            mini.get("auth_start_date").setValue("<%=__bizDate%>");
	            mini.get("auth_end_date").setValue("<%=__bizDate%>");
            }
            getFromUser();
        	var data=top["flowAuth"].getData();
            if(action == "approve"){
            	document.getElementById("from_user_id").style.display="none";
                document.getElementById("save_btn").style.display="none";
                document.getElementById("cancel_btn").style.display="none";
                document.getElementById("key_btn").style.display="none";
            }else if(action == "detail"){
            	document.getElementById("from_user_id").style.display="none";
                document.getElementById("pass_btn").style.display="none";
                document.getElementById("reject_btn").style.display="none";
                document.getElementById("save_btn").style.display="none";
                document.getElementById("cancel_btn").style.display="none";
                if(data.auth_status != "6"){
                    document.getElementById("key_btn").style.display="none";
                }
            }else{
            	document.getElementById("to_user_id").style.display="none";
                document.getElementById("pass_btn").style.display="none";
                document.getElementById("reject_btn").style.display="none";
                document.getElementById("key_btn").style.display="none";
            }
            init();
            searchPrd();
        });

        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");

        function init(){
            var title = action=="new"?"新增":action=="detail"?"详情":action=="approve"?"审批":action=="edit"?"修改":"";
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>授权</a> >>"+title); 
            if(action == "edit" || action == "approve" || action == "detail"){
                var form1 = new mini.Form("auth_form");
                var form2 = new mini.Form("rule_form");
                var data = top["flowAuth"].getData();
                if(action == "edit"){
                	getTaskDef(data.flow_id, data.from_user_id);
                }
                form1.setData(data);
                form2.setData(JSON.parse(data.authority_rule));
                mini.get("to_user_id").setValue(data.to_user_name + "(" + data.to_user_id + ")");
                mini.get("auth_start_date").setValue(data.auth_start_date);
                mini.get("auth_end_date").setValue(data.auth_end_date);
                mini.get("flow_id").setValue(data.flow_Id);
                mini.get("flow_id").setText(data.flow_name);
                mini.get("task_def_key").setValue(data.task_def_key);
                mini.get("task_def_key").setText(data.task_def_name);
                if(action == "detail"){
                    form1.setEnabled(false);
                    form2.setEnabled(false);
                }
            }
        }

        //保存
        function save(){
           var form = new mini.Form("auth_form");
           form.validate();
	       if (form.isValid() == false){
               mini.alert("请输入有效的数据!","系统提示");
               return;//表单验证
           }
	       var data = form.getData();
	       data.to_user_id = "<%=__sessionUser.getUserId()%>";
	       data.is_active = 1;
	       data.auth_status = 3;
	       data.auth_start_date = mini.get("auth_start_date").getFormValue();
	       data.auth_end_date = mini.get("auth_end_date").getFormValue();
	       data.task_def_key = mini.get("task_def_key").getValue();
	       data.authority_rule = mini.encode({
	    		  "prd_no":mini.get("prd_no").getValue(),
	    		  "amt":mini.get("amt").getValue(),
	    		  "ccy":mini.get("ccy").getValue()
	       });
           var saveUrl="";
           if(action == "new"){
               saveUrl = "/FlowSublicenseController/addUserSublicenseDetail";
           }else if(action == "edit"){
        	   data.id = top["flowAuth"].getData().id;
               saveUrl = "/FlowSublicenseController/updateUserSublicenseDetail";
           }
           var param = mini.encode(data); //序列化成JSON
           CommonUtil.ajax({
		       url:saveUrl,
               data:param,
               callback:function(data){
				   if('error.common.0000' == data.code){
			           mini.alert("保存成功!","系统提示",function(value){
                           //关闭并刷新
					       setTimeout(function(){ top["win"].closeMenuTab() },100);
                       });
                   }else{
                       mini.alert("保存失败！","系统提示");
                   }	
               }
		   });
       }
        
       //取消按钮
       function cancel(){
           window.CloseOwnerWindow();
       }
       
       //被授权人
       function getFromUser(){
    	   CommonUtil.ajax({
 			  url:"/UserController/selectUserWithInstIds",
               data:{"instIds":'<%=__sessionUser.getInstId()%>',"branchId":'<%=__sessionUser.getBranchId()%>'},
            	  callback:function(data){
                	  var combo = mini.get("from_user_id");
	                   var result = new Array();
	                   $.each(data.obj,function(i,item){
	                	   if(item.userId == '<%=__sessionUser.getUserId()%>'){
	                		   return true;
	                	   }
	            		   result.push({"id":item.userId, "text":item.userName + "(" + item.userId + ")"});
	                   });
                	  combo.setData(result);
               }
 		   });
       }
       
       //提交
       function approve(status){
    	   var data=top["flowAuth"].getData();
           CommonUtil.ajax({ 
               url:"/FlowSublicenseController/approve",
               data:{"id":data.id,"status":status},
               callback:function(data){
                   mini.alert("提交成功!","系统提示");
                   cancel();
               }
           });
       }
       
       function onFlowClick(e) {
           var btnEdit = this;
           mini.open({
               url: CommonUtil.baseWebPath() + "../../Common/Flow/FlowMini.jsp",
               title: "选择流程",
               width: 700,
               height: 600,
               ondestroy: function (action) {
                   if (action && action != "close") {
                	   btnEdit.setValue(action.flow_id);
                	   btnEdit.setText(action.flow_name);
                   }
                   getTaskDef(action.flow_id,mini.get("from_user_id").getSelected().id);
               }
           });
       }
       function onUserChanged (){
    	   getTaskDef(mini.get("flow_id").getValue(), mini.get("from_user_id").getSelected().id);
       }
       
       function getTaskDef(flowId, userId){
    		if(!flowId || !userId){
    			return false;
    		}
    		CommonUtil.ajax({
    			url:'/FlowController/getUserRoleListByFlow',
    			data:{flowId:flowId,userId:userId},
    			callback:function(data){
    				mini.get('task_def_key').setData([]);
    				if(!data.obj){
    					return false;
    				}
    				var sidObj = new Array();
    				$.each(data.obj, function(i, item){
    					sidObj.push({"id": item.Id, "text": item.Name.expressionText});
    				});	
    				//加载节点
    				mini.get('#task_def_key').setData(sidObj);
    			}
    		});
    	};
    	
    	//key
        function getKey(){
        	var data=top["flowAuth"].getData();
        	CommonUtil.ajax({
    			url:'/FlowSublicenseController/sendKeyToJsp',
    			data:{id:data.id},
    			callback:function(data){
    				if(data.desc){
    					mini.alert("授权验证码为：<span style='color:#ff3333;font-size:25px;'>"+data.desc+"</span>", "系统提示");
    				}else{
    					mini.alert("该记录查无授权验证码.", "系统提示");
    				}
    			}
    		});
        };
        
		/* 查询产品*/
		function searchPrd(){
			CommonUtil.ajax({
				url:"/ProductController/searchProduct",
				data:{"branchId":"<%=__sessionUser.getBranchId()%>","prdInst":"<%=__sessionUser.getInstId()%>"},
				callback : function(data) {
					var arr = new Array();
					$.each(data.obj,function(i,item){
						arr.push({"id":item.prdNo, "text":item.prdName});
					});
					mini.get("prd_no").setData(arr);
				}
			});
		}
    </script>
</body>
</html>