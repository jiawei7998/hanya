<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>审批页面url</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
    <div>
        <div id="field" class="fieldset-body">
	        <table id="url_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
	            <tr>
	                <td style="width:100%;vertical-align: top;">
	                	<input id="urls" name="urls" class="mini-combobox" data="CommonUtil.serverData.dictionary.approveUrl"
	                        labelField="true"  label="url：" required="true"  vtype="maxLength:32"
	                        style="width:100%" labelStyle="text-align:left;width:170px" allowInput="true"/>
	                </td>
	            </tr>
	            <tr>
	                <td colspan="2" style="text-align:center;">
	                    <a  id="save_btn" class="mini-button" style="display: none"   onclick="save">保存</a>
	                </td>
	            </tr>
	        </table>
        </div>
    </div>

    <script>
         //保存
         function save(){
        	var data=top["taskDefine"].getData();
            var form = new mini.Form("url_form");
            form.validate();
	        if (form.isValid() == false){
                mini.alert("请输入有效的数据!","系统提示");
                return false;//表单验证
            }
	        data.urls = mini.get("urls").getValue();
            var param = mini.encode(data); //序列化成JSON
            var saveUrl="/FlowController/addFlowTaskDefineUrl";
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
					if('error.common.0000' == data.code){
				        mini.alert("保存成功!","系统提示",function(value){
                            //关闭并刷新
				            CloseWindow("ok");
                        });
				
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
			});
        }
         
         function CloseWindow(action) {
             if (window.CloseOwnerWindow)
                 return window.CloseOwnerWindow(action);
             else
                 window.close();
         }
    </script>
</body>
</html>