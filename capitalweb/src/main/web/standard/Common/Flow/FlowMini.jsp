<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>流程 mini 用于对方流程选择</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>流程查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<input id="prd_no" name="prd_no" class="mini-combobox" labelField="true"  label="产品"  width="330px"  labelStyle="text-align:left;width:140px" onValueChanged="getFlowType" />
			<input id="flow_type" name="flow_type" class="mini-combobox" labelField="true"  label="流程类型"    width="330px"  labelStyle="text-align:left;width:140px"  />
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 

<div class="mini-fit" >
	<div id="flow_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="flow_id" width="150" align="center"  headerAlign="center" >流程ID</div>    
			<div field="flow_name" width="180"  headerAlign="center" align="center" >流程名称</div>
			<div field="version" width="120" align="center" headerAlign="center"  >版本</div>
		</div>
	</div>
</div>   

<script>
	mini.parse();
	var url = window.location.search;
	var prdNo = CommonUtil.getParam(url, "prdNo");
	var prdName = CommonUtil.getParam(url, "prdName");
	var form = new mini.Form("#search_form");
	var grid=mini.get("flow_grid");
	var prd_no=mini.get("prd_no");
	var row="";
	searchPrd();
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
	});

	
	
	//清空
	function clear(){
		var form=new mini.Form("search_form");
		form.clear();
		query();
	}
	

	/* 按钮 查询事件 */
	function query(){
		search(grid.pageSize,0);
	}

	/* 查询 */
	function search(){
		var params = {
			"productType":mini.get('#prd_no').getValue(),
			"flowType":mini.get('#flow_type').getValue()
		};
		CommonUtil.ajax({
			url:'/FlowController/searchProcDefByPrdType',
			data:params,
			callback:function(data){
				mini.get('#flow_grid').setData(data.obj);
			}
		});
	}
	
	/* 查询 */
	function searchPrd(){
		CommonUtil.ajax({
			url:"/ProductController/searchProduct",
			data:{"branchId":"<%=__sessionUser.getBranchId()%>","prdInst":"<%=__sessionUser.getInstId()%>"},
			callback : function(data) {
				var arr = new Array();
				$.each(data.obj,function(i,item){
					arr.push({"id":item.prdNo, "text":item.prdName});
				});
				prd_no.setData(arr);
			}
		});
	}
	
	function getFlowType(){
		var prd = mini.get("prd_no").getValue();
		var flowType = CommonUtil.serverData.dictionary.FlowType;
		mini.get("flow_type").setData([]);
		//加载流程类型信息
		CommonUtil.ajax({
			url:"/ProductTypeController/getFlowTypeByProductType",
			data:{"prdTypeNo":prd,"branchId":"<%=__sessionUser.getBranchId()%>","prdInst":"<%=__sessionUser.getInstId()%>"},
			callback:function(data){
				var flowTypeSelect=[];
				$.each(flowType, function(i, itemf) {
					$.each(data.obj, function(j, item) {
						if(itemf.id == item.flowTypeNo) {
							var info ={id:itemf.id,text:itemf.text};
							flowTypeSelect.push(info);
						}
					});
				});
				mini.get("flow_type").setData(flowTypeSelect);
			}
		});
	}
	
	$(document).ready(function() {
    });
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
    	
        CloseWindow(mini.get("flow_grid").getSelected());
    }

    //关闭窗口
    function onCancel() {
        CloseWindow(null);
    }
</script>
</body>
</html>
