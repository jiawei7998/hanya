<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>用户流程特权维护</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area"  style="background:white">
		<input id="ticketId" name="ticketId" style="display:none;"   class="mini-textbox" />
            <fieldset>
            <legend>用户流程特权信息</legend>
            <div class="leftarea">
                <input id="userId" name="userId" class="mini-textbox mini-mustFill" labelField="true" label="用户：" emptyText="请输入用户" required="true"  style="width:100%" labelStyle="text-align:left;width:170px"
						  requiredErrorText="用户不能为空" />
				<input id="condition" name="condition" class="mini-combobox mini-mustFill" labelField="true" label="条件：" style="width:100%" labelStyle="text-align:left;width:170px" required="true"  validateOnLeave="true"
						 requiredErrorText="条件不能为空" vtype="maxLength:32" 
						 data="[{'id':'1','text':'小于'},{'id':'2','text':'小于等于'},{'id':'3','text':'大于'},{'id':'4','text':'大于等于'}]"/>
			</div>	
			<div class="rightarea">
				<input style="width:100%" id="productType" name="productType" class="mini-combobox mini-mustFill" allowInput="false" 
					labelField="true"  label="产品类型：" labelStyle="text-align:left;width:170px;" required="true"
					data = "CommonUtil.serverData.dictionary.bussTpNm"/>
				<input id="amt" name="amt" labelField="true" label="金额：" style="width:100%" labelStyle="text-align:left;width:170px" emptyText="金额" class="mini-spinner mini-mustFill" required="true"
						 minValue="0" maxValue="999999999999999.99" format="n2"/>
			</div>
            </fieldset>
		</div>
	</div>		
    <div id="functionIds"  style="padding-top:30px;text-align:center;">
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn" onclick="save">保存</a>
        </div>
        <div style="margin-bottom:10px; text-align: center;">
        	<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn" onclick="close">关闭</a>
        </div>
    </div> 
</div>
</body>
<script>
mini.parse();
//获取当前tab
var currTab = top["win"].tabs.getActiveTab();
var params = currTab.params;
var row = params.selectData;
var url = window.location.search;
var action = CommonUtil.getParam(url,"action"); // 增删改
var form = new mini.Form("#field_form");
var productTypeOld="";

$(document).ready(function(){
	var appendRedstar="<i class='red-star'></i>";
    $(".mini-mustFill label").append(appendRedstar);
	if($.inArray(action,["edit","detail"])>-1){//修改、详情
        form.setData(row);
        productTypeOld=row.productType;
        if($.inArray(action,["detail"])>-1){
        	form.setEnabled(false);
        }
    }
});

//保存
function save(){
    //表单验证！！！
    form.validate();
    if (form.isValid() == false) {
        mini.alert("表单填写有误!","提示");
        return;
    }
    var saveUrl = "/IfsCfetsFlowUserManageController/AddFlowUser";
    var data = form.getData(true);
    data['action']=action;
    data['productTypeOld']=productTypeOld;
    var params = mini.encode(data);
    CommonUtil.ajax({
        url:saveUrl,
        data:params,
        callback:function(data){
            mini.alert("保存成功",'提示信息',function(){
                top["win"].closeMenuTab();
            });
        }
    });
}

function close(){
	top["win"].closeMenuTab();
}
</script>
</html>