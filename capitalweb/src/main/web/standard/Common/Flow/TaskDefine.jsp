<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
</head>
<body style="width:100%;height:99%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">

    <div size="50%" showCollapseButton="false" >
        <fieldset class="mini-fieldset">
            <legend>流程分配</legend>
            <div id="search_form" class="mini-form" style="width:100%;height:15%;margin-top:5px">
                <div>
                    <input id="productType" name="productType" class="mini-combobox" width="320px" label="产品类型：" onvaluechanged="searchFlowType" labelField="true" labelStyle="text-align:left;width:60px;"  />
                    <input id="flowType" name="flowType" class="mini-combobox" width="320px"  label="流程类型：" labelField="true" labelStyle="text-align:left;width:60px;"  onvaluechanged="search" />
                </div>
                <div class="btn_div" style="margin-left: 440px;">
                    <a id="search_btn" class="mini-button"  iconCls="iconfont icon-fsearch" onclick="search()">查询</a>
                    <a id="clear_btn" class="mini-button"   iconCls="iconfont icon-fclose" onclick="clear">清空</a>
                    <a id="save_btn" class="mini-button" style=""  iconCls="iconfont icon-baocun" onclick="save">保存信息</a>
                    <a id="close_btn" class="mini-button" style=""  iconCls="iconfont icon-fclose" onclick="close">关闭界面</a>
                </div>
            </div>
        </fieldset>

        <div id="panel1" class="mini-panel" title="流程列表" style="width:100%;height:40%;"  allowResize="true" collapseOnTitleClick="false">
            <div id="flow_table" class="mini-datagrid" style="width:99.9%;" onselectionchanged="onSelectionChanged" multiSelect="true" showPager="false"  allowResize="true">
                <div property="columns">
                    <div type="indexcolumn" align="center" width="30"></div>
                    <div field="flow_id" align="left" headerAlign="center" width="140px">流程Id</div>
                    <div field="flow_name" align="center"  headerAlign="center" width="120px" >流程名称</div>
                    <div field="version"  align="center" headerAlign="center" width="50px">版本号</div>
                </div>
            </div>
        </div>

        <div id="panel2" class="mini-panel" title="任务节点列表" style="width:100%;height:40%;"  allowResize="true" collapseOnTitleClick="false">
            <div id="task_table" class="mini-datagrid" style="width:99.9%;" multiSelect="true" showPager="false"  allowResize="true" onselectionchanged="onSelectionChanged2">
                <div property="columns">
                    <div type="indexcolumn" width="30"></div>
                    <div field="Id"  headerAlign="center" align="left" width="220px">节点Id</div>
                    <div field="Name.expressionText" align="center" headerAlign="center" width="90px">节点名称</div>
                </div>
            </div>
        </div>
    </div>

    <div showCollapseButton="false">

        <div   style="width:100%;height:30%;" >

            <div id="panel31" class="mini-panel" title="已选表单列表" visible="false" style="width: 46%;height: 100%;display: none;">
                <div id="form_selected_grid" class="mini-datagrid"  fitColumns="true"  idField="id" pageSize="10" multiSelect="false" showPager="false"  allowResize="true">
                    <div property="columns">
                        <div type="checkcolumn"></div>
                        <div type="indexcolumn" width="30"></div>
                        <div field="formName"  headerAlign="center" >表单名称</div>
                        <div field="formNo" headerAlign="center"   >表单编号</div>
                    </div>
                </div>
            </div>
            <div style="width:7%;height: 100%;display: inline-block;text-align:center;vertical-align: top" >
                <div style="margin-bottom:10px; margin-top:20px" ><a class="mini-button" style="display: none"  id="form_select_btn" onclick="formSelect" style="width:50px;">&lt;</a></div>
                <div style="margin-bottom:10px;" ><a class="mini-button" style="display: none"   id="form_remove_btn"  onclick="formRemove" style="width:50px;">&gt;</a></div>
            </div>
            <div id="panel32" class="mini-panel"  title="可选表单列表"  style="width: 46%;height: 100%;display: none;">
                <div id="form_optional_grid" class="mini-datagrid"  fitColumns="true"  idField="id" pageSize="10"
                     multiSelect="false" showPager="false"  allowResize="true">
                    <div property="columns">
                        <div type="checkcolumn"></div>
                        <div type="indexcolumn" width="30"></div>
                        <div field="formName"  headerAlign="center" >表单名称</div>
                        <div field="formNo" headerAlign="center" >表单编号</div>
                    </div>
                </div>
            </div>
        </div>

        <div id="pane4" style="width:100%;height:30%;display: none;" >
            <div id="pane41"  title="已选检查列表" class="mini-panel" style="width: 46%;height: 100%;display: none;" >
                <div id="check_selected_grid" class="mini-datagrid" style="width:99.9%;" fitColumns="true"
                     multiSelect="true" showPager="false"   allowResize="true">
                    <div property="columns">
                        <div type="checkcolumn"></div>
                        <div type="indexcolumn" width="30"></div>
                        <div field="prdName"  headerAlign="center">产品类型</div>
                        <div field="ckType" headerAlign="center">审查类型</div>
                    </div>
                </div>
            </div>
            <div style="width:7%;height: 100%;display: inline-block;text-align:center;vertical-align: top">
                <div style="margin-bottom:10px; margin-top:20px"><a class="mini-button" style="display: none"     onclick="add" style="width:50px;">&gt;</a></div>
                <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"     onclick="removes" style="width:50px;">&lt;</a></div>
            </div>
            <div id="pane42"  title="可选检查列表" class="mini-panel" style="width: 46%;height: 100%;display: none;">
                <div id="check_optional_grid" class="mini-datagrid" style="width:99.9%;" fitColumns="true"
                     multiSelect="true" showPager="false"   allowResize="true">
                    <div property="columns">
                        <div type="checkcolumn"></div>
                        <div type="indexcolumn" width="30"></div>
                        <div field="prdName"  headerAlign="center">产品类型</div>
                        <div field="ckType" headerAlign="center">审查类型</div>
                    </div>
                </div>
            </div>
        </div>

        <div id="pane5" style="width:100%;height:50%;"  >
            <div id="pane51" title="已选事件列表" class="mini-panel" style="width: 46%;height: 100%;display: inline-block;">
                <div id="event_selected_grid" class="mini-datagrid" style="width:99.9%;" allowResize="true" showPager="false" multiSelect="true">
                    <div property="columns">
                        <div type="checkcolumn"></div>
                        <div type="indexcolumn" width="30"></div>
                        <div field="eventName"  headerAlign="center">事件名称</div>
                        <div field="eventCode" headerAlign="center">事件编码</div>
                    </div>
                </div>
            </div>
            <div style="width:7%;height: 100%;display: inline-block;text-align:center;vertical-align: top" >
                <div style="margin-bottom:10px; margin-top:100px"><a class="mini-button"     onclick="eventSelect" style="width:50px;">＞</a></div>
                <div style="margin-bottom:10px; "><a class="mini-button"     onclick="eventRemove" style="width:50px;">＜</a></div>
                <span style="color:red">配置事件后请保存信息，否则配置不生效。</span>
            </div>
            <div id="pane52" title="可选事件列表" class="mini-panel" style="width: 46%;height: 100%;display: inline-block;" >
                <div id="event_optional_grid" class="mini-datagrid" style="width:99.9%;" allowResize="true" showPager="false" multiSelect="true">
                    <div property="columns">
                        <div type="checkcolumn"></div>
                        <div type="indexcolumn" width="30"></div>
                        <div field="eventName"  headerAlign="center">事件名称</div>
                        <div field="eventCode" headerAlign="center">事件编码</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    mini.parse();
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var form = new mini.Form("#search_form");
    var flowTable = mini.get("flow_table");
    var taskTable = mini.get("task_table");
    var check_selected_grid  = mini.get("check_selected_grid");//已选检查列表
    var check_optional_grid  = mini.get("check_optional_grid");//可选检查列表
    var form_selected_grid  = mini.get("form_selected_grid");//已选表单列表
    var form_optional_grid  = mini.get("form_optional_grid");//可选表单列表
    var event_selected_grid  = mini.get("event_selected_grid");//已选表单列表
    var event_optional_grid  = mini.get("event_optional_grid");//可选事件列表

    var TaskDefine={};
    TaskDefine.flowType = CommonUtil.getDictList("FlowType");

    function save(){
        var flow   = flowTable.getSelected();
        var task   = taskTable.getSelected();
        var events = event_selected_grid.getData();
        var checks = check_selected_grid.getData();
        var form   = form_selected_grid.getData();
        var params = {};
        if(task == null){
            mini.alert("请先选择一条流程节点数据","系统提示");
            return false;
        }else{
            params.flowId = flow.flow_id;
            params.taskId = task.Id;
            params.version = flow.version;
            params.events = "";
            params.checks = "";
            params.prdCode = mini.get("productType").getValue();
            $.each(events, function(i, item) {
                params.events += item.eventId;
                params.events += i==events.length-1?"":",";
            });
            $.each(checks, function(i, item) {
                params.prdCode = item.ckPrd;
                params.checks += item.ckType;
                params.checks += i==checks.length-1?"":",";
            });

            if(form.length > 0){
                params.formNo = form[0].formNo;
                params.form_fields_json = form[0].form_fields_json;
            }

            CommonUtil.ajax({
                url:'/FlowController/addFlowTaskDefine',
                data:params,
                callback:function(data){
                    mini.alert("节点定义保存成功","系统提示");
                }
            });
        }
    }
    //加载产品类型信息
    function searchProduct(){
        CommonUtil.ajax({
            url:"/ProductController/searchProduct",
            data:{},
            callback:function(data){
                var Product=[];
                for(var i = 0;i<data.obj.length;i++){
                    var info = {id :data.obj[i].prdNo,text:data.obj[i].prdName};
                    Product.push(info);
                }
                mini.get("productType").setData(Product);
            }
        });
    }
    //加载流程类型信息
    function searchFlowType(e){
        var prdTypeNo = e.value;
        var data = {};
        data['prdTypeNo'] = prdTypeNo;
        CommonUtil.ajax({
            url:"/ProductTypeController/getFlowTypeByProductType",
            data:data,
            callback:function(data){
                var flowTypeSelect=[];
                $.each(TaskDefine.flowType, function(i, itemf) {
                    $.each(data.obj, function(j, item) {
                        if(itemf.value == item.flowTypeNo) {
                            var info ={id:itemf.value,text:itemf.text};
                            flowTypeSelect.push(info);
                        }
                    });
                });
                mini.get("flowType").setData(flowTypeSelect);
            }
        });
    }
    /* 查询 */
    function search(){
        flowTable.clearRows();
        form.validate();
        if(form.isValid()==false){
            mini.alert("信息填写有误，请重新填写","系统也提示");
            return;
        }
        var data=form.getData(true);
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:'/FlowController/searchProcDefByPrdType',
            data:params,
            callback : function(data) {
                for(var i = 0; i < data.obj.length;i++){
                    flowTable.addRow(data.obj[i]);
                }
            }
        });
    }

    //加载任务节点信息
    function onSelectionChanged(){
        var row = flowTable.getSelected();
        if (row) {
            CommonUtil.ajax({
                url:'/FlowController/getFlowTaskListFlowId',
                data:{flowId:row.flow_id},
                callback:function(data){
                    taskTable.setData(data.obj);
                }
            });
        }
        var productType = mini.get("productType").getValue();
        //loadCheckInfo(row.flow_id,row.Id,productType);
        loadFormInfo(row.flow_id,row.Id,productType);
        loadEventInfo(row.flow_id,row.Id,productType);



    }

    //加载任务节点信息
    function onSelectionChanged2(){
        var row = flowTable.getSelected();
        if (row) {
            /* CommonUtil.ajax({
                url:'/FlowController/getFlowTaskListFlowId',
                data:{flowId:row.flow_id},
                callback:function(data){
                 taskTable.setData(data.obj);
                }
            }); */
        }
        var productType = mini.get("productType").getValue();

        var row2 =taskTable.getSelected();
        //loadCheckInfo(row.flow_id,row.Id,productType);
        loadFormInfo(row.flow_id,row.Id,productType);
        if (row2) {
            loadEventInfo(row.flow_id,row2.Id,productType);
        }



    }

    //加载检查框信息
    function loadCheckInfo(flowId,taskId,ckPrd){
        CommonUtil.ajax({
            url:'/TcLoanCheckboxController/getTaskCheckboxSelect',
            data:{"flowId":flowId,"task_def_key":taskId,"ckPrd":ckPrd},
            callback:function(data){
                check_selected_grid.setData(data.obj);
            }
        });
        CommonUtil.ajax({
            url:'/TcLoanCheckboxController/getTaskCheckboxUnselect',
            data:{"flowId":flowId,"task_def_key":taskId,"ckPrd":ckPrd},
            callback:function(data){
                check_optional_grid.setData(data.obj);
            }
        });
    }

    //加载表单信息
    function loadFormInfo(flowId,taskId,prdCode){
        CommonUtil.ajax({
            url:'/ProductFormController/searchFormList',
            data:{},
            callback:function(data){
                form_optional_grid.setData(data.obj);
                form_selected_grid.setData();
                //$("#TaskDefine").find('#form_select_btn').linkbutton("enable");
            }
        });
        CommonUtil.ajax({
            url:'/FlowController/getUserTaskForm',
            data:{"procDefId":flowId,"taskDefKey":taskId,"prdCode":prdCode},
            callback:function(data){
                if(data.obj){
                    var optional = form_optional_grid.getData();
                    $.each(optional,function(i,item){
                        if(data.obj.form_no == item.formNo){
                            //根据已选表单id将表单项目从待选移入已选，并且禁用<-按钮
                            item.form_fields_json = JSON.parse(data.obj.form_fields_json);
                            //TaskDefine.form_selected_grid.datagrid("appendRow", item);
                            //TaskDefine.form_optional_grid.datagrid("deleteRow", TaskDefine.form_optional_grid.datagrid("getRowIndex", item));
                            //$("#TaskDefine").find('#form_select_btn').linkbutton("disable");
                            form_selected_grid.addRow(item);
                            form_optional_grid.moveRow(form_optional_grid.indexOf(item));
                            //跳出循环
                            return false;
                        }
                    });
                }
            }
        });
    }

    //加载事件信息
    function loadEventInfo(flowId,taskId,prdCode){
        CommonUtil.ajax({
            url:'/FlowController/getFlowTaskEventSelect',
            data:{"flow_id":flowId,"task_def_key":taskId,"prd_code":prdCode},
            callback:function(data){
                event_selected_grid.setData(data.obj);
            }
        });
        CommonUtil.ajax({
            url:'/FlowController/getFlowTaskEventUnselect',
            data:{"flow_id":flowId,"task_def_key":taskId,"prd_code":prdCode},
            callback:function(data){
                event_optional_grid.setData(data.obj);
            }
        });
    }


    //事件选择
    function eventSelect(){
        var rows = event_optional_grid.getSelecteds();
        event_optional_grid.removeRows(rows);
        event_selected_grid.addRows(rows);
    }
    //事件去选
    function eventRemove(){
        var rows = event_selected_grid.getSelecteds();
        event_selected_grid.removeRows(rows);
        event_optional_grid.addRows(rows);
    }


    //表单选择
    function formSelect(){
        var rows = form_optional_grid.getSelecteds();
        if(!rows){
            return false;
        }
        form_optional_grid.removeRows(rows);
        form_selected_grid.addRows(rows);
        //选中一条后禁用选择按钮
        mini.get("form_select_btn").setVisible(false);
    }
    //表单去选
    function formRemove(){
        var rows = form_selected_grid.getSelecteds();
        if(!rows){
            return false;
        }
        //重置自定义表单设置
        if(rows.form_fields_json){

        }
        form_selected_grid.removeRows(rows);
        form_optional_grid.addRows(rows);
        //取消选中一条后重启选择按钮
        mini.get("form_select_btn").setVisible(true);
    }


    //清空
    function clear(){
        var form=new mini.Form("search_form");
        form.clear();
        query();
    }
    $(document).ready(function(){
        searchProduct();

    });
    function close(){
        top["win"].closeMenuTab();
    }


</script>
</body>
</html>