	//样式   必输项 出现 红星
	var appendRedstar="<i class='red-star'></i>";
    $(".mini-mustFill label").append(appendRedstar);

	var ApproveOperate = {};
	ApproveOperate.isLogPage = null;
	if(CommonUtil.isNotNull(tradeData.serial_no)){
		ApproveOperate.serial_no = tradeData.serial_no;
	}

	if(CommonUtil.isNotNull(tradeData.task_id)){
		ApproveOperate.task_id = tradeData.task_id;
	}
	
	ApproveOperate.operType = tradeData.operType;
	ApproveOperate.flow_type = Approve.FlowType.SettleApproveFlow;
	ApproveOperate.isNeedAuth = null;
	ApproveOperate.authType = null;
	ApproveOperate.temp_flow_info = null; 
	ApproveOperate.temp_log_data = null;
	
	//根据英文节点类型返回相应中文
	ApproveOperate.ActivityType = function(type){
		var result = null;
		switch(type){
			case "startEvent":
				result = "开始节点";
				break;
			case "exclusiveGateway":
				result = "决策节点";
				break;
			case "userTask":
				result = "人工节点";
				break;	
			case "endEvent":
				result = "结束节点";
				break;	
			default:
				result = type;
		}
		return result;
	};
	
	//获取下一人工任务节点审批人
	//edit by wangchen on 2017/5/23
	ApproveOperate.getUserNextStep = function(flow_id,task_id,serial_no){
		CommonUtil.ajax({
			url:"/FlowController/getNextStepCandidatesList",
  			data:{
  				flow_id:flow_id,
  				step_id:task_id,
  				serial_no:serial_no
  			},
  			callback : function(data) {
  				if(data.obj){
  					if (data.obj.length > 0) {				
	  					//将用户格式转化为tree可接受的格式
	  					var userData = new Array();
						userData.push({"id":"","text":"---不指定下一步审批人---"});
						var item = null;
						for(var i = 0; i < data.obj.length; i++)
						{
							item = data.obj[i];
							userData.push({"id":item.userId,"text":item.userName});
						}
						//载入combotree
						mini.get("user").setData(userData);

	  				} else {
						//未查询到待指派人则视为下个节点非人工审批节点，无法指派待审批人
						mini.get("user").setVisible(false);
	  					$("#ApproveOperate_div #td_user_text").append("<span style='color:#ff3333;font-weight:bold;'>*&nbsp;下一个节点非人工审批节点，因此无法指定审批人。");
	  				}
  				} else {
					//未查询到待指派人则视为下个节点非人工审批节点，无法指派待审批人
					mini.get("user").setVisible(false);
					$("#ApproveOperate_div #td_user_text").append("<span style='color:#ff3333;font-weight:bold;'>*&nbsp;下一个节点非人工审批节点，因此无法指定审批人。");
				}
  			}
  		});
	};

	function loadFlow(){
		var messageid = mini.loading("正在获取流程信息……", "请稍后");
		//var temp_json_data = null;
		CommonUtil.ajax( {
			url:"/FlowController/getOneFlowDefineBaseInfo",
			data:{serial_no:ApproveOperate.serial_no},
			callerror: function(data){
				mini.hideMessageBox(messageid);
				//ApproveOperate.loadLogInfo();
			},
			callback : function(data) {
				mini.hideMessageBox(messageid);
				temp_flow_info = data.obj;
				var innerInterval;
				innerInitFunction = function(){
					clearInterval(innerInterval);
					ApproveOperate.getUserNextStep(temp_flow_info.ActivitiId,ApproveOperate.task_id,ApproveOperate.serial_no);

					if(typeof(FlowDesigner) != 'undefined'){
						/***
						if($("#"+ApproveOperate.tabOptions.id).find("#log_tabs").tabs('getSelected').find('#FlowDesigner_div').length == 1){
							FlowDesigner.createAllNode($.parseJSON(data.obj.json_data));
						}
						else{
							temp_json_data = $.parseJSON(data.obj.json_data);
						}
						***/
					}
				},
				innerInterval = setInterval("innerInitFunction()",100);
			}
		});
	}

	//校验审批用户是否超过审批限额
	function checkUserCreditLimit(){
		var flag = false;
		if(tradeData.isTrade){
			var amt = mini.get("amt").getValue();
			CommonUtil.ajax({
				url:"/qdCreditController/checkUserCreditLimit",
				async:false,
				data:{'amt' : amt},
				callback : function(data) {
					if(data && data.desc && data.desc == '999'){
						flag = true;
					}else{
						flag = false;
					}
				}// end function
			});// end ajax

		}else{
			flag = true;
		}
		return flag;
	}
	
$(function(){
	if(Approve.FlowType.SettleApproveFlow == ApproveOperate.flow_type){
		 //结算不允许拒绝
		 mini.get("nopass_btn").setVisible(false);
	}

	mini.get("pass_btn").setVisible(false);

	// add by wangchen on 2017/5/22 
	//审批权限控制按钮显示
	//start
	if(!ApproveOperate.isLogPage){
		CommonUtil.ajax({
			data:{"task_id":ApproveOperate.task_id},
			url:"/FlowRoleController/getUserFlowOperations",
			callback:function(data){
				if(data.obj){
					//可视的审批权限为：4.审批;7.加签;8.退回;9.驳回;10.撤销;11.挂起.
					$.each(data.obj,function(i,item){
						switch(item){
							case "4":
								//4.审批
								mini.get("continue_btn").setVisible(true);
								break;
							case "7":
								//7.加签
								mini.get("skip_btn").setVisible(true);
								break;
							case "8":
								//8.退回
								mini.get("redo_btn").setVisible(true);
								break;
							case "9":
								//9.驳回
								mini.get("back_btn").setVisible(true);
								break;
							case "10":
								//10.拒绝
								mini.get("nopass_btn").setVisible(true);
								break;
						}
					});
				}
			}
		});
	}
	//end
	if(ApproveOperate.operType == "approve"){
		loadFlow();
	}else{
		$("#ApproveOperate_div").hide();
	}
	
	/*----------------事件注册    开始--------------------*/
	//事件注册： 通过
	mini.get("pass_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		if(checkUserCreditLimit() == false){
			mini.alert("审批失败,该笔交易金额超过审批限额.","提示");
			return;
		}

		mini.confirm("确认通过该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			if(ApproveOperate.isNeedAuth){
				var authPageUrl = CommonUtil.baseWebPath() + '/mini_settle/MiniSettleAuthEdit.jsp?authKey1=' + ApproveOperate.serial_no + '&authType=' + ApproveOperate.authType;
				mini.open({
					url : authPageUrl,
					title : "请授权",
					width:300,
					height:150,
					onload : function(e){
						var param = {
							authKey1:ApproveOperate.serial_no,
							authType:ApproveOperate.authType
						};
						var iframe = this.getIFrameEl();
						try{
							if(iframe.contentWindow.init && typeof(iframe.contentWindow.init)=="function"){
								iframe.contentWindow.init(param);
							}
						}catch(e){
							
						}

					},
					ondestroy : function(action) {		
						if (action == "ok") 
						{
							Approve.approveOperator({
								task_id : ApproveOperate.task_id,
								specific_user : mini.get("user").getValue(),
								msg : mini.get("approve_msg").getValue(),
								op_type : Approve.OperatorType.Pass,
								success:function(){
									CommonUtil.closeMenuTab("task_" + ApproveOperate.task_id,null);
									//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
								}
							});
						}

					}
				});

			}else{
				Approve.approveOperator({
					task_id : ApproveOperate.task_id,
					specific_user : mini.get("user").getValue(),
					msg : mini.get("approve_msg").getValue(),
					op_type : Approve.OperatorType.Pass,
					success:function(){
						CommonUtil.closeMenuTab("task_" + ApproveOperate.task_id,null);
						//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
					}
				});
			}
		}); // end confirm
	});// end pass_btn

	ApproveOperate.continue_btn = function(){
		var authPageUrl = CommonUtil.baseWebPath() + '/mini_settle/MiniSettleAuthEdit.jsp?authKey1=' + ApproveOperate.serial_no + '&authType=' + ApproveOperate.authType;
		mini.open({
			url : authPageUrl,
			title : "请授权",
			width:300,
			height:150,
			onload : function(e){
				var param = {
					authKey1:ApproveOperate.serial_no,
					authType:ApproveOperate.authType
				};
				var iframe = this.getIFrameEl();
				try{
					if(iframe.contentWindow.init && typeof(iframe.contentWindow.init)=="function"){
						iframe.contentWindow.init(param);
					}
				}catch(e){
					
				}

			},
			ondestroy : function(action) {	
				if (action == "ok") 
				{
					Approve.approveOperator({
						task_id : ApproveOperate.task_id,
						specific_user : mini.get("user").getValue(),
						msg : mini.get("approve_msg").getValue(),
						op_type : Approve.OperatorType.Continue,
						success:function(){
							CommonUtil.closeMenuTab("task_" + ApproveOperate.task_id,null);
							//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
						}
					});
				}

			}
		});
	};

	//事件注册： 同意继续
	mini.get("continue_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		
		var psettmeans = mini.get("psettmeans");
		var psetacct = mini.get("psetacct");
		var rsettmeans = mini.get("rsettmeans");
		var rsetacct = mini.get("rsetacct");
		
		// 现券买卖，单边校验
		var prdname = mini.get("prdname").getValue(); // 产品名称
		if(prdname == "现券买卖"){
			var mydircn = mini.get("mydircn").getValue(); // 本方方向
			if(mydircn == "卖出"){
				rsettmeans.validate();
				if(!rsettmeans.isValid()){
					return false;
				}
				rsetacct.validate();
				if(!rsetacct.isValid()){
					return false;
				}
			} else if (mydircn == "买入"){
				psettmeans.validate();
				if(!psettmeans.isValid()){
					return false;
				}
				psetacct.validate();
				if(!psetacct.isValid()){
					return false;
				}
			}
		}
		
		var nettingstatus = mini.get("nettingstatus").getValue(); // 净额清算状态
		// 外汇即、远、掉+净额清算状态为1,不做校验
		if(nettingstatus == 1 && (prdname == "外汇即期" || prdname == "外汇远期" || prdname == "外汇掉期")){
		} else if(prdname != "现券买卖") {
			// 非现券买卖，非外汇，都要校验
			psettmeans.validate();
			if(!psettmeans.isValid()){
				return false;
			}
			psetacct.validate();
			if(!psetacct.isValid()){
				return false;
			}
			rsettmeans.validate();
			if(!rsettmeans.isValid()){
				return false;
			}
			rsetacct.validate();
			if(!rsetacct.isValid()){
				return false;
			}
		}
		if(prdname == "外汇掉期"){
			var fpsettmeans = mini.get("fpsettmeans");
			var fpsetacct = mini.get("fpsetacct");
			var frsettmeans = mini.get("frsettmeans");
			var frsetacct = mini.get("frsetacct");
			fpsettmeans.validate();
			if(!fpsettmeans.isValid()){
				return false;
			}
			fpsetacct.validate();
			if(!fpsetacct.isValid()){
				return false;
			}
			frsettmeans.validate();
			if(!frsettmeans.isValid()){
				return false;
			}
			frsetacct.validate();
			if(!frsetacct.isValid()){
				return false;
			}
			
		}	
		
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		if(checkUserCreditLimit() == false){
			mini.alert("审批失败,该笔交易金额超过审批限额.","提示");
			return;
		}

		mini.confirm("确认通过该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			}
			if(ApproveOperate.isNeedAuth){
				if(ApproveOperate.doApprove){
					var checkListParam = ApproveOperate.doApprove();
					var checkList = checkListParam.checkList;
					if(checkListParam.checkLoan == "block"){
						if(!checkList){return;}
						CommonUtil.ajax({
							url:"/TcLoanCheckboxDealController/insertTcLoanCheckboxDeal",
							data:{'jsonStr':JSON.stringify(checkList),'taskId':ApproveOperate.taskId},
							callback:function(data){
								ApproveOperate.continue_btn();
							}
						});
					}else{
						ApproveOperate.continue_btn();
					}
				}else{
					ApproveOperate.continue_btn();
				}
			}else{
				if(ApproveOperate.doApprove){
					var checkListParam = ApproveOperate.doApprove();
					var checkList = checkListParam.checkList;
					if(checkListParam.checkLoan == "block"){
						if(!checkList){return;}
						CommonUtil.ajax({
							url:"/TcLoanCheckboxDealController/insertTcLoanCheckboxDeal",
							data:{'jsonStr':JSON.stringify(checkList),'taskId':ApproveOperate.taskId},
							callback:function(data){
								Approve.approveOperator({
									task_id : ApproveOperate.task_id,
									specific_user : mini.get("user").getValue(),
									msg : mini.get("approve_msg").getValue(),
									op_type : Approve.OperatorType.Continue,
									success:function(){
										CommonUtil.closeMenuTab("task_"+ApproveOperate.task_id,null);
										//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
									}
								});
								}
						});
					}else{
						Approve.approveOperator({
							task_id : ApproveOperate.task_id,
							specific_user : mini.get("user").getValue(),
							msg : mini.get("approve_msg").getValue(),
							op_type : Approve.OperatorType.Continue,
							success:function(){
								CommonUtil.closeMenuTab("task_"+ApproveOperate.task_id,null);
								//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
							}
						});
					}

				}else{

					Approve.approveOperator({
						task_id : ApproveOperate.task_id,
						specific_user : mini.get("user").getValue(),
						msg : mini.get("approve_msg").getValue(),
						op_type : Approve.OperatorType.Continue,
						success:function(){
							CommonUtil.closeMenuTab("task_"+ApproveOperate.task_id,null);
							//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
						}
					});
				}
			}
		
		}); // end confirm
	});// end continue_btn

	//事件注册： 拒绝
	mini.get("nopass_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		if(checkUserCreditLimit() == false){
			mini.alert("审批失败,该笔交易金额超过审批限额.","提示");
			return;
		}

		mini.confirm("确认拒绝该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			Approve.approveOperator({
				task_id : ApproveOperate.task_id,
				specific_user : "",
				msg : mini.get("approve_msg").getValue(),
				op_type : Approve.OperatorType.NoPass,
				success:function(){
					CommonUtil.closeMenuTab("task_" + ApproveOperate.task_id,null);
					//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
				}
			});
		}); // end confirm
	});// end nopass_btn

	//事件注册： 驳回
	mini.get("back_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		if(checkUserCreditLimit() == false){
			mini.alert("审批失败,该笔交易金额超过审批限额.","提示");
			return;
		}

		mini.confirm("确认驳回该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			Approve.approveOperator({
				task_id : ApproveOperate.task_id,
				specific_user : "",
				msg : mini.get("approve_msg").getValue(),
				op_type : Approve.OperatorType.Back,
				success:function(){
					CommonUtil.closeMenuTab("task_" + ApproveOperate.task_id,null);
					//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
				}
			});
		}); // end confirm
	});// end back_btn

	//事件注册： 退回发起
	mini.get("redo_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		if(checkUserCreditLimit() == false){
			mini.alert("审批失败,该笔交易金额超过审批限额.","提示");
			return;
		}

		mini.confirm("确认退回发起该笔审批吗？","确认",function (action) {
			if (action != "ok") {
				return;
			} 
			Approve.approveOperator({
				task_id : ApproveOperate.task_id,
				specific_user : "",
				msg : mini.get("approve_msg").getValue(),
				op_type : Approve.OperatorType.Redo,
				success:function(){
					CommonUtil.closeMenuTab("task_"+ApproveOperate.task_id,null);
					//ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
				}
			});
		}); // end confirm
	});// end redo_btn
	
	//添加快捷信息
	mini.get("add_text_btn").on("click",function(e){
		var value = mini.get("approve_msg").getValue() + mini.get("add_text_combobox").getText();
		mini.get("approve_msg").setValue(value);
	});
	
	
	//事件注册： 查看审批日志
	/**
	mini.get("log_btn").on("click",function(e){
		Approve.approveLog(flow_type,serial_no);
	});
	**/

	//加签
	mini.get("skip_btn").on("click",function(){
		var approve_msg = mini.get("approve_msg");
		approve_msg.validate();
		if(!approve_msg.isValid()){
			return false;
		}

		mini.open({
			url : CommonUtil.baseWebPath() + '/../Common/Flow/MiniApproveOperateSkip.jsp',
			title : "指派加签人",
			width : 660,
			height : 450,
			onload : function(e){
				var param = {
					authKey1:ApproveOperate.serial_no,
					authType:ApproveOperate.authType
				};
				var iframe = this.getIFrameEl();
				try{
					if(iframe.contentWindow.init && typeof(iframe.contentWindow.init)=="function"){
						iframe.contentWindow.init(param);
					}
				}catch(e){
					
				}

			},
			ondestroy : function(action) {
				if (action == "ok") 
				{
					var iframe = this.getIFrameEl();
					var record = null;
					try{
						if(iframe.contentWindow.getData && typeof(iframe.contentWindow.getData)=="function"){
							record = iframe.contentWindow.getData();
						}
					}catch(e){
						
					}
					CommonUtil.ajax({
						data:{"task_id":ApproveOperate.task_id,"user_id":record},
						url:"/FlowController/invitationWithTask",
						callback:function(data){
							$.messager.alert("提示","操作成功");
							CommonUtil.closeMenuTab("task_"+ApproveOperate.task_id,null);
							//CommonUtil.closeTab(null,"task_" + ApproveOperate.task_id);
							ApproveOperate.tabOptions.parentObj.loadData("待审批列表");
						}
					});
				}
			}
		});

		mini.get("skip_btn").setEnabled(false);
		mini.get("continue_btn").setEnabled(false);
		mini.get("redo_btn").setEnabled(false);
		mini.get("back_btn").setEnabled(false);
	});// end skip_btn
});