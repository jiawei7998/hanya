<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>

<body style="width:100%;height:100%;background:white">

<div id="FlowDistributionManage" class="mini-fit">
	<div class="mini-splitter" style="width:100%;height:100%;" allowResize="false" borderStyle="border:solid 1px #aaa;">
		<div size="55%" >
			<fieldset class="mini-fieldset">
				<legend>产品流程列表</legend>
				<div id="searchForm" style="width:100%;">
					<table width="100%">
						<tr>
							<td width="100%"><input id="prdTypeNo" name="prdTypeNo" class="mini-combobox" valueField="prdNo" textField="text" labelField="true"  label="产品类型" style="width:80%;" emptyText="请选择产品类型"></td>
						</tr>
					</table>
				</div>
			</fieldset>
			<div style="height:40%">
			<div id="flowGrid" class="mini-treegrid" showTreeIcon="true" treeColumn="text" idField="id" parentField="parentId" resultAsTree="false"
				allowResize="true" expandOnLoad="true" showCheckBox="false" checkRecursive="true">
				<div property="columns" allowMove="true" allowResize="true">
					<div type="indexcolumn" width="50" headerAlign="center">序号</div>
					<div name="text" field="text" width="250" headerAlign="center">审批流程</div>
				</div>
			</div>
			</div>
			<div id="datagrid" class="mini-datagrid borderAll" idField="id"  allowAlternating="true" style="height:48%;" showPager="false"
	  			allowResize="true" sortMode="client" allowAlternating="true" onrowclick="loadEventInfo">
				<div property="columns"> 
					<div header="流程节点列表">
						<div property="columns">
							<div type="indexcolumn" width="50px" align="center" headerAlign="center">序号</div>
							<div field="Id" width="150px" align="left"  headerAlign="center" >节点ID</div>
							<div field="Name.expressionText" width="150px" align="left"  headerAlign="center" >节点名称</div>
						</div>
					</div>
				</div>
		  </div>
		</div>
		<div size="45%">
			 <div id="datagrid1" class="mini-datagrid borderAll" style="height:50%;float:right"  idField="id"  allowAlternating="true" showPager="false"
		  			allowResize="true" sortMode="client" allowAlternating="true"  multiSelect="true">
				<div property="columns"> 
					<div header="可选事件列表">
						<div property="columns"> 
							<div type="indexcolumn" width="50px" align="center" headerAlign="center">序号</div>
							<div type="checkcolumn"></div>
							<div field="eventName" width="130px" align="left"  headerAlign="center" >事件名称</div>
							<div field="eventCode" width="130px" align="left"  headerAlign="center" >事件编码</div>                                
						</div>
					</div>
				</div>
			 </div>
			 <div style="width:800px;">
 
            <table style="width:100%;">
                <tr>
                    <td style="width:100%;">
                        <a class="mini-button" style="display: none"   onclick="adds()" plain="true">可选事件选择</a>
                        <a class="mini-button" style="display: none"   onclick="removes()" plain="true">已选事件移除</a>
                        <a class="mini-button" style="display: none"   onclick="saveEvent()" plain="true">事件保存</a>
                    </td>
                </tr>
            </table>           
        </div>
 
			<div id="datagrid2" class="mini-datagrid borderAll" style="height:40%;float:right"  idField="id"  allowAlternating="true" showPager="false"
			  allowResize="true" sortMode="client" allowAlternating="true"  multiSelect="true">
				<div property="columns"> 
					<div header="已选事件列表">
						<div property="columns"> 
							<div type="indexcolumn" width="50px" align="center" headerAlign="center">序号</div>
							<div type="checkcolumn"></div>							
							<div field="eventName" width="130px" align="left"  headerAlign="center" >事件名称</div>
							<div field="eventCode" width="130px" align="left"  headerAlign="center" >事件编码</div>                                
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

<script type="text/javascript">
	mini.parse();

	top["FlowSuspendProcessManage"] = window;

	var flowGrid = mini.get("flowGrid");
	var grid1=mini.get("datagrid1");
	var grid2=mini.get("datagrid2");
	var grid=mini.get("datagrid");
	/***********************************事件区域*******************************************/
	 //可选事件的选择
	function adds() {
        
            var items = grid1.getSelecteds();
            grid1.removeRows(items);
            grid2.addRows(items);
        }
	 //可已选事件的移除
        function removes() {
            var items = grid2.getSelecteds();
            grid2.removeRows(items);
            grid1.addRows(items);
        }
	 //事件保存
	 function saveEvent(){
			var flow = flowGrid.getSelected();
			var task   = grid.getSelected();
			var events = grid2.getData();
			var params = {};
			if(task==null){
				mini.alert("请先选择一条流程节点数据","提示");
				return false;
			}else{
				params.flowId = flow.id;
				params.taskId = task.Id;
				params.events = "";
				params.prdCode = mini.get("prdTypeNo").getValue();
				
				$.each(events, function(i, item) { 
					params.events += item.eventId;
					params.events += i==events.length-1?"":",";
				});
				CommonUtil.ajax({
					url:'/FlowController/addFlowTaskDefine',
					data:params,
					callback:function(data){
						mini.alert("节点定义保存成功","提示");
					}
				});
			} 
	 }
	//加载产品列表
	function loadProductList(){
		CommonUtil.ajax({
			data:{branchId:branchId},
			url:"/ProductController/searchProduct",
			callback:function(data){
				var prdTypeNo = mini.get("prdTypeNo");
				prdTypeNo.setData(data.obj);
			}
		});	
	}
	function queryFlowData(){
		var messageid = mini.loading("正在操作", "请稍后");
		var prdTypeNo = mini.get("prdTypeNo").getValue();
		var dict = CommonUtil.getDictList("FlowType");
		var result = new Array();
		CommonUtil.ajax({
			url:"/ProductTypeController/getFlowTypeByProductType",
			data:{"prdTypeNo":prdTypeNo},
			callback : function(data) {
				for(var item = 0; item < dict.length; item++)
				{
					for(var item2 = 0; item2 < data.obj.length; item2++)
					{
						if(dict[item].value == data.obj[item2].flowTypeNo){
							result.push(dict[item]);
						}
					}
				}
				flowGrid.setData(result);

				CommonUtil.ajax({
					url:"/FlowController/searchProcDefByPrdType",
					data:{"product_type":prdTypeNo},
					callback:function(data){
						mini.hideMessageBox(messageid);
						$.each(data.obj,function(i,item){
							var row = flowGrid.findRow(function(row){
								//返回当前流程类型下的所有流程
								if(row.id == item.flow_type) return true;
							});
							if(row){
								//增加子节点
								flowGrid.addNode({"id":item.flow_id,parentId:row.id,"text":item.flow_name + "(编号：" + item.flow_id + "；版本：" + item.version + ")" },0,row);				
							}
						});
					}
				});// end callback

			}// end function
		});
	}// end function

	/***********************************页面初始化*******************************************/
	$(document).ready(function(){
//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			/*********************事件定义********************/
			//组件:流程树
			flowGrid.on("rowclick", function () {
				var prdTypeNo = mini.get("prdTypeNo").getValue();
				var record = flowGrid.getSelected();
				if (!flowGrid.isLeaf(record)) {//如果不是子节点返回
					return;
				}
				//根据所选择的流程，查询该流程下的所有节点
				CommonUtil.ajax({
					url: '/FlowController/getFlowTaskListFlowId',
					data: {"flowId": record.id},
					callback: function (data) {
						grid.setData(data.obj);
					}
				});
			});

			//prdTypeNo值变动时调用
			mini.get("prdTypeNo").on("valuechanged", function () {
				queryFlowData();
			});

			loadProductList();
		});
	});
	//根据选择的 节点id查询该节点的可选事件名称与已选事件名称
	function loadEventInfo(e){
		var prdCode = mini.get("prdTypeNo").getValue();
		var flow = flowGrid.getSelected();
		if(!flowGrid.isLeaf(flow)){
			return;
		}
		CommonUtil.ajax({
			url:'/FlowController/getFlowTaskEventSelect',
			data:{"flow_id":flow.id,"task_def_key":e.row.Id,"prd_code":prdCode},
			callback:function(data){
				grid2.setData(data.obj);
			}
		});
		CommonUtil.ajax({
			url:'/FlowController/getFlowTaskEventUnselect',
			data:{"flow_id":flow.id,"task_def_key":e.row.Id,"prd_code":prdCode},
			callback:function(data){
				grid1.setData(data.obj);
			}
		});
	}
</script>