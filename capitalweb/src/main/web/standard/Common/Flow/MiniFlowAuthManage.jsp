<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
	<head>
	    <title>审批授权</title>
	    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 	
	</head>
	<body style="width:100%;height:100%;background:white">
		<div class="mini-panel" title="授权信息" style="width:100%">
		<div style="width:100%;padding-top:10px;">
			<div id="search_form" class="mini-form" width="80%">
				<input id="auth_start_date" name="auth_start_date" class="mini-datepicker" labelField="true"  label="开始日期：" labelStyle="text-align:right;" />
				<input id="auth_end_date" name="auth_end_date" class="mini-datepicker" labelField="true"  label="结束日期：" labelStyle="text-align:right;" />
				<span style="float:right;margin-right: 150px">
					<a id="search_btn" class="mini-button" style="display: none"  onclick="search(10,0)">查询</a>
					<a id="clear_btn" class="mini-button" style="display: none"  >清空</a>
				</span>
			</div>
		</div>
	
		<table width="100%">
			<tr>
				<td width="60%" style="padding:10px 0 5px 5px">
		            <a id="approve_commit_btn" class="mini-button" style="display: none"    onclick="approve()">审批</a>
		            <a id="destroy_btn" class="mini-button" style="display: none"    onclick="modifyedit()">销毁</a>
	                <a id="submit_btn" class="mini-button" style="display: none"   onclick="submit(4)">提交</a>
	                <a id="add_btn" class="mini-button" style="display: none"   onclick="add">新增</a>
	                <a id="edit_btn" class="mini-button" style="display: none"    onclick="modifyedit">修改</a>
	                <a id="delete_btn" class="mini-button" style="display: none"    onclick="deleteRow">删除</a>
				</td>
				<td width="40%">
					<div id = "approveType" name = "approveType" class="mini-checkboxlist" style="float:right;width:100%; " 
						labelField="true" label="审批列表选择：" labelStyle="text-align:right;" =value="approve" textField="text" 
						valueField="id" multiSelect="false" data="[{id:'approve',text:'待审批列表'}, {id:'mine',text:'我发起的'}]" 
						onvaluechanged="checkBoxValuechanged"></div>
				</td>
			</tr>
		</table>
		</div>
		<div class="mini-fit">      
			<div id="auth_grid"  class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="dealNo" 
			allowAlternating="true" allowResize="true" onrowdblclick="custDetail" border="true" 
			multiSelect="false" sortMode="client">
				<div property="columns">
				<div type="indexcolumn" align="center" headerAlign="center">序号</div>
                <div field="id" width="25" align="center" headerAlign="center" allowSort=nihj"true">授权ID</div> 
                <div field="from_user_id" width="40" align="center" headerAlign="center" allowSort="true">被申请人ID</div> 
                <div field="from_user_name" width="40" align="center" headerAlign="center" allowSort="true">被申请人姓名</div> 
                <div field="to_user_id" width="40" align="center" headerAlign="center" allowSort="true">申请人ID</div>   
                <div field="to_user_name" width="40" align="center" headerAlign="center" allowSort="true">申请人姓名</div>   
                <div field="flow_id"  width="60" align="center" headerAlign="center">流程ID</div>   
                <div field="flow_name"  width="50" align="center" headerAlign="center">流程名称</div> 
                <div field="task_def_name"  width="50" align="center" headerAlign="center">节点名称</div>       
                <div field="auth_start_date"  width="50" align="center" headerAlign="center">开始日期</div>    
                <div field="auth_end_date"  width="50" align="center" headerAlign="center">结束日期</div>
                <div field="auth_status"  headerAlign="center"  align="center" width="40" renderer="CommonUtil.dictRenderer" data-options="{dict:'ApproveStatus'}">状态</div> 
                <div field="is_active"  headerAlign="center"  align="center" width="40" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否启用</div>
				</div>
			</div>  
		</div>
	</body>
	<script>
	    mini.parse();

	    top["flowAuth"]=window;
	    function getData(){
	        var grid = mini.get("auth_grid");
	        var record =grid.getSelected(); 
	        return record;
	    }
	
	    //新增审批组
	    function add(){
	        var url = CommonUtil.baseWebPath() + "/../Common/Flow/MiniFlowAuthEdit.jsp?action=new";
	        var tab = { id: "floAuthManageAdd", name: "flowAuthManageAdd", text: "授权新增", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
	        top['win'].showTab(tab);
	    }
	    //修改
	    function modifyedit(){
	        var grid = mini.get("auth_grid");
	        var  record =grid.getSelected();
            if(record.auth_status!=3){
                mini.alert("只有“新建”状态的记录才能修改!","系统提示");
                return; 
            }
	        if(record){
	            var url = CommonUtil.baseWebPath() + "/../Common/Flow/MiniFlowAuthEdit.jsp?action=edit";
	            var tab = { id: "flowAuthManageEdit", name: "flowAuthManageEdit", text: "授权信息修改", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
	            top['win'].showTab(tab);
	        }else{
	            mini.alert("请选中一条记录","系统提示"); 
	        }
	    }
	    
	    //审批
	    function approve(){
	        var grid = mini.get("auth_grid");
			var row = grid.getSelected();
			if(row){
				var url = CommonUtil.baseWebPath() + "/../Common/Flow/MiniFlowAuthEdit.jsp?action=approve";
				var tab = {"id": "flowAuthManageEdit",name:"flowAuthManageEdit",url:url,title:"授权信息审批",
							parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
				var paramData = {selectData:row};
				CommonUtil.openNewMenuTab(tab,paramData);
			} else {
				mini.alert("请选中一条记录！","消息提示");
			}

	    }
	
	    //双击一行打开详情信息
	    function custDetail(e){
	        var grid = e.sender;
	        var row = grid.getSelected();
	        if(row){
	            var url = CommonUtil.baseWebPath() + "/../Common/Flow/MiniFlowAuthEdit.jsp?action=detail";
	            var tab = { id: "flowAuthManageDetail", name: "flowAuthManageDetail", text: "授权信息详情", url: url,showCloseButton:true,parentId:top['win'].tabs.getActiveTab().name };
	            top['win'].showTab(tab);
	        }
	    }
	
	    
	
	     //查询(带分页)
	     function search(pageSize,pageIndex){
	            var form =new mini.Form("search_form");
	            form.validate();
	            if (form.isValid() == false) return;//表单验证
	            var data =form.getData();//获取表单数据
	            data['pageNumber']=pageIndex+1;
	            data['pageSize']=pageSize;
	            data['branchId']='<%=__sessionUser.getBranchId()%>';
	            var param = mini.encode(data); //序列化成JSON
	            var urls = "";
	            var approveType=mini.get("approveType").getValue();
	            if(approveType == "mine"){//我发起的
	            	urls = "/FlowSublicenseController/getUserSublicenseListMine";
	            }else{
	            	urls = "/FlowSublicenseController/getUserSublicenseListApprove";
	            }
	            CommonUtil.ajax({
	                url:urls,
	                data:param,
	                callback:function(data){
	                        
	                    var grid =mini.get("auth_grid");
	                    //设置分页
	                    grid.setTotalCount(data.obj.total);
	                    grid.setPageIndex(pageIndex);
	                    grid.setPageSize(pageSize);
	                    //设置数据
	                    grid.setData(data.obj.rows);
	                }
	            });
	        }
	        var row="";
	        function searchUser(pageSize,pageIndex){
	            var auth_grid =mini.get("auth_grid");
	            var form =new mini.Form("search_inst");
	            var data = form.getData();//获取机构数据
	            data['pageNumber']=pageIndex+1;
	            data['pageSize']=pageSize;
	            data['branchId']='<%=__sessionUser.getBranchId()%>';
	            
	            var param = mini.encode(data);
	
	            CommonUtil.ajax( {
	                url:"/FlowSublicenseController/getUserSublicenseListMine",
	                data:param,
	                callback : function(data) {
	                    var grid =mini.get("auth_grid");
	                    //设置分页
	                    grid.setTotalCount(data.obj.total);
	                    grid.setPageIndex(pageIndex);
	                    grid.setPageSize(pageSize);
	                    //设置数据
	                    grid.setData(data.obj.rows);
	                }
	            });
	
	        }
	
	         //清空
	         function clear(){
	            var form=new mini.Form("search_form");
	            form.clear();
	            search(10,0);
	            
	        }
	
	        //提交
	        function submit(status){
	        	var grid = mini.get("auth_grid"); 
	            var rows =grid.getSelected();
	            if(rows.length<=0){
	                mini.alert("请至少选择一条记录!","系统提示");
	                return; 
	            }
	            if(rows.auth_status!=3){
	                mini.alert("只有“新建”状态的记录才能提交!","系统提示");
	                return; 
	            }
	        	CommonUtil.ajax({ 
	                url:"/FlowSublicenseController/approve",
	                data:{"id":rows.id,"status":status},
	                callback:function(data){
	                	mini.alert("提交成功!","系统提示");
	                	searchUser(grid.pageSize,0);
	                }
	            });
	        }
	        
	        //删除
	        function deleteRow(){
	            var grid = mini.get("auth_grid"); 
	            var rows =grid.getSelected();
	            if(rows.length<=0){
	                mini.alert("请至少选择一条记录!","系统提示");
	                return; 
	            }
	            mini.confirm("您确定要删除吗？", "温馨提示", function (action) {
                    if (action == 'ok') {
			            CommonUtil.ajax({ 
			                url:"/FlowSublicenseController/deleteUserSublicenseById",
			                data:{"id":rows.id},
			                callback:function(data){
			                	mini.alert("删除成功!","系统提示");
			                	searchUser(grid.pageSize,0);
			                }
			            });
                    }
	            });
	        }
	
	    $(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				var grid = mini.get("auth_grid");
				grid.on("beforeload", function (e) {
					e.cancel = true;
					var pageIndex = e.data.pageIndex;
					var pageSize = e.data.pageSize;
					search(pageSize, pageIndex);
				});
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("submit_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(true);
				mini.get("approve_commit_btn").setEnabled(true);
				search(grid.pageSize, 0);
			});
	    });
	    
		function checkBoxValuechanged(e){
			search(10,0);
			var approveType=this.getValue();
			if(approveType == "approve"){//待审批
				mini.get("add_btn").setVisible(false);
				mini.get("edit_btn").setVisible(false);
				mini.get("delete_btn").setVisible(false);
				mini.get("submit_btn").setVisible(false);
				mini.get("approve_commit_btn").setVisible(true);
				mini.get("approve_commit_btn").setEnabled(true);
			}else{//我发起的
				mini.get("approve_commit_btn").setVisible(false);
				mini.get("add_btn").setVisible(true);
				mini.get("add_btn").setEnabled(true);
				mini.get("edit_btn").setVisible(true);
				mini.get("edit_btn").setEnabled(true);
				mini.get("delete_btn").setVisible(true);
				mini.get("delete_btn").setEnabled(true);
				mini.get("submit_btn").setVisible(true);
				mini.get("submit_btn").setEnabled(true);
			}
		}
	</script>

</html>