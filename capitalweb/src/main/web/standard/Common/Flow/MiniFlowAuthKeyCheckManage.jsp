<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
	<head>
	    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	    <title>授权验证</title>
	</head>
	<body style="width:100%;height:100%;background:white">
		<fieldset class="mini-fieldset">
		<legend>请输入12位授权验证码</legend>	
		<div>
			<div id="checkform" style="width:100%" cols="6">
				<input id="auth_key" name="auth_key" class="mini-textbox" style="width:100%" label="授权验证码"
		    	labelStyle="text-align:left;width:120px" labelField="true"/>
			</div>
			<a id="submit_btn" class="mini-button" style="display: none"   onclick="save" Style="text-align:center">提交</a>
		</div>
		</fieldset> 
		<script>
			function save() {
			    CloseWindow(mini.get("auth_key").getValue());
			}
			function CloseWindow(action) {
			    if (window.CloseOwnerWindow)
			        return window.CloseOwnerWindow(action);
			    else
			        window.close();
			}
		</script>
	</body>
</html>
