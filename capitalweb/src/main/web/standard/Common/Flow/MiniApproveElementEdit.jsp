<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>审批要素组合维护页面</title>
</head>
<body style="width:100%;height:100%;background:white">
	<div id="UserEdit"  style="width: 100%; height: 100%;">
		<div id="field" style="padding:5px;">
			<table   id="field_form" style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
			<h1 style="text-align:left;" id="labell"><a href="javascript:CommonUtil.activeTab();">要素组合</a> >> 修改</h1>
				<tr>
					<td style="width:50%">
						<input id="type" name="type" class="mini-combobox" labelField="true" label="操作类型："  emptyText="请选择操作类型" onValueChanged="queryProduct" data="CommonUtil.serverData.dictionary.OperatorType" required="true"   style="width:100%" labelStyle="text-align:left;width:170px" />
					</td>
					<td style="width:50%">
						<input id="product" name="product" class="mini-combobox" labelField="true" label="产品："   emptyText="请选择产品"  valueField="TABLE_NAME" onValueChanged="queryElement" textField="PRODUCTSCOMMENTS" required="true"  style="width:100%" labelStyle="text-align:left;width:170px" />
                        <input class="mini-hidden" id="id" name="id" />
                        <input class="mini-hidden" id="inputTime" name="inputTime" />
                    </td>
				</tr>
				<tr>
					<td >
						<input id="tradeGroup" name="tradeGroup" class="mini-combobox" labelField="true" label="要素组合：" multiselect="true" emptyText="请选择要素"  valueField="COLUMN_NAME" textField="COMMENTSGROUPS" required="true"  style="width:100%" labelStyle="text-align:left;width:170px" />
					</td>
				</tr>
            </table>
				<table style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                    <tr>
                        <td colspan="3" style="text-align:center;">
                                <a class="mini-button" style="display: none"    onClick="save()" id="save_btn">保存</a>
                                <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                        </td>
                    </tr>
                </table>
			
		</div>
</div>
</body>
<script>
    mini.parse();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field");

    $(document).ready(function(){
        initData();
    }); 

    //初始化数据
    function initData(){
        if($.inArray(action,["edit","detail"])>-1){//修改、详情
            form.setData(row);
            if(action == "detail"){
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>要素组合</a> >> 详情");
                form.setEnabled(false);
                mini.get("save_btn").setVisible(false);
            }
        }else{//增加 
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>要素组合</a> >> 新增");
        }
    }

    //保存
    function save(){
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写有误!","提示");
            return;
        }
        var saveUrl=null;
        if(action == "add"){
            saveUrl = "/IfsApproveElementController/LimitConditionAdd";
        }else if(action == "edit"){
            saveUrl = "/IfsApproveElementController/saveLimitConditionEdit";
        }

        var data=form.getData(true);
        data['productsComments']=mini.get("product").getText();
        data['commentsGroup']=mini.get("tradeGroup").getText();
        data['inputTrader']="<%=__sessionUser.getUserId()%>";
        var params=mini.encode(data);
        CommonUtil.ajax({
            url:saveUrl,
            data:params,
            callback:function(data){
                mini.alert("保存成功",'提示信息',function(){
                    top["win"].closeMenuTab();
                });
            }
        });
    }

    //取消
    function cancel(){
        top["win"].closeMenuTab();
    }

    //加载产品列表
    function queryProduct(){
    	var params=mini.get("type").getValue();
        CommonUtil.ajax({
            data:{tables:params},
            url:"/IfsApproveElementController/searchTables",
            callback:function(data){
                var prdTypeNo = mini.get("product");
                prdTypeNo.setData(data.obj);
            }
        });	
    } 
  //加载要素列表
    function queryElement(){
    	var params=mini.get("product").getValue();
        CommonUtil.ajax({
            data:{tableName:params},
            url:"/IfsApproveElementController/searchColumns",
            callback:function(data){
                var prdTypeNo = mini.get("tradeGroup");
                prdTypeNo.setData(data.obj);
            }
        });	
    } 
</script>     
</html>