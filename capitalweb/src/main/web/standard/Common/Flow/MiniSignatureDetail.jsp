<%--
  Created by IntelliJ IDEA.
  User: Luozb
  Date: 2021/5/28 0028
  Time: 10:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>电子签名</title>
    <style type="text/css">
        #canvas {
            border: 1px solid #ccc;
        }
        .button1
        {
            background: #ff5d48;
            border-color: #ff5d48;
            color:#fff;
            font-family: 微软雅黑,宋体,Arial,Helvetica,Verdana,sans-serif;
            font-size:13px;
            line-height:16px;
        }
    </style>
</head>
<body>
<div id="canvasDiv"></div>


<%--<button id="btn_ret" class="button1">擦除</button>--%>
<button id="btn_clear" class="button1">重写</button>

<button id="btn_submit" class="button1">提交</button>
<form action="" id = "form1" method="post">
    <input
        type="hidden" name="imageData" id="imageData" /></form>

<img id="tempImage" src="" style="display: none;" alt="临时图片文件" />

<script language="javascript">
    var canvasDiv = document.getElementById('canvasDiv');
    var canvas = document.createElement('canvas');
    var canvasWidth = 800, canvasHeight = 300;
    var form1 = document.getElementById('form1');
    form1.action  = CommonUtil.pPath + "/sl/FlowSignatureController/base64ToImages";
    var point = {};
    var flag = 1;//1-写入，2-擦除
    point.notFirst = false;

    canvas.setAttribute('width', canvasWidth);
    canvas.setAttribute('height', canvasHeight);
    canvas.setAttribute('id', 'canvas');
    canvasDiv.appendChild(canvas);

    if (typeof G_vmlCanvasManager != 'undefined') {

        canvas = G_vmlCanvasManager.initElement(canvas);
    }
    var context = canvas.getContext("2d");

    canvas.addEventListener("mousedown", function(e) {
        var mouseX = e.pageX - this.offsetLeft;
        var mouseY = e.pageY - this.offsetTop;
        paint = true;
        addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
        redraw();
    });

    canvas.addEventListener("mousemove",
        function(e) {
            if (paint) {
                addClick(e.pageX - this.offsetLeft, e.pageY
                    - this.offsetTop, true);
                redraw();
            }
        });

    canvas.addEventListener("mouseup", function(e) {
        paint = false;
    });

    canvas.addEventListener("mouseleave", function(e) {
        paint = false;
    });

    var clickX = new Array();
    var clickY = new Array();
    var clickDrag = new Array();
    var paint;

    function addClick(x, y, dragging) {
        clickX.push(x);
        clickY.push(y);
        clickDrag.push(dragging);
    }

    function redraw() {
        //canvas.width = canvas.width; // Clears the canvas

        context.strokeStyle = "#df4b26";
        context.lineJoin = "round";
        context.lineWidth = 5;

        while (clickX.length > 0) {
            point.bx = point.x;
            point.by = point.y;
            point.x = clickX.pop();
            point.y = clickY.pop();
            point.drag = clickDrag.pop();
            context.beginPath();
            if (point.drag && point.notFirst) {
                context.moveTo(point.bx, point.by);
            } else {
                point.notFirst = true;
                context.moveTo(point.x - 1, point.y);
            }
            context.lineTo(point.x, point.y);
            context.closePath();
            context.stroke();
        }
        /*
         for(var i=0; i < clickX.length; i++)
         {
         context.beginPath();
         if(clickDrag[i] && i){
         context.moveTo(clickX[i-1], clickY[i-1]);
         }else{
         context.moveTo(clickX[i]-1, clickY[i]);
         }
         context.lineTo(clickX[i], clickY[i]);
         context.closePath();
         context.stroke();
         }
         */
    }
    var clear = document.getElementById("btn_clear");
    var submit = document.getElementById("btn_submit");
    clear.addEventListener("click", function() {
        canvas.width = canvas.width;
    });

    submit.addEventListener("click", function() {
        //获取当前页面的信息，在当前页面的img下展示
        var image = document.getElementById("tempImage");
        image.src = canvas.toDataURL("image/png");
        document.getElementById("imageData").value = canvas
            .toDataURL("image/png");

        image.style = "display:block;";
        //获取canvas的数据格式如下
        alert(canvas.toDataURL("image/png"));
        //提交表单数据信息
        document.forms[0].submit();
    });
</script>

</body>
</html>
