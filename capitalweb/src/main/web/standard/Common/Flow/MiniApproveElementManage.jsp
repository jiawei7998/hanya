<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>审批要素组合管理页面</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form">        
        <input id="type" name="type" class="mini-combobox" labelField="true" label="操作类型："  onValueChanged="queryProducts" data="CommonUtil.serverData.dictionary.OperatorType" width="330px" labelStyle="text-align:right;" emptyText="请选择操作类型" 
        />
        <input id="product" name="product" class="mini-combobox" labelField="true" label="产品："   emptyText="请选择产品"  valueField="TABLE_NAME"  textField="PRODUCTSCOMMENTS" width="330px" labelStyle="text-align:right;" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search"  onclick="search(10,0)">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add"  onClick="add();">新增</a>
        <!-- <a class="mini-button" style="display: none"  id="modify"  onClick="modify();">修改</a> -->
        <a class="mini-button" style="display: none"  id="deleteUser"  onClick="del">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div  id="grid"  class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" 
        allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="productsComments" headerAlign="center"  width="150px">产品</div>
                <div field="type" headerAlign="center" width="100px" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'OperatorType'}">操作类型</div>
                <div field="commentsGroup" headerAlign="center" width="180px" >审批要素组合</div>
                <div field="inputTrader" headerAlign="center" align="center" width="100px">录入人员</div>
                <div field="inputTime" headerAlign="center" align="center"  allowSort="true" width="150px">录入时间</div>                
            </div>
        </div>
    </div>
</body>

<script>

mini.parse();

var grid = mini.get("grid");
$(document).ready(function () {
    //控制按钮显示
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        });
        search(grid.pageSize, grid.pageIndex);
    });
})

//增删改查
function add(){
	var url = CommonUtil.baseWebPath() + "/../Common/Flow/MiniApproveElementEdit.jsp?action=add";
	var tab = { id: "ApproveElementAdd", name: "ApproveElementAdd", title: "要素组合新增", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
	var paramData = {selectData:""};
	CommonUtil.openNewMenuTab(tab,paramData);
}
function query() {
    search(grid.pageSize, 0);
}
function clear(){
    var form=new mini.Form("search_form");
    form.clear();
    search(10,0);

}


//用户的修改
function modify() {
   var row = grid.getSelected(); 
    if (row) {
        var url = CommonUtil.baseWebPath() + "/../Common/Flow/MiniApproveElementEdit.jsp?action=edit";
        var tab = { id: "ApproveElementEdit", name: "ApproveElementEdit", title: "要素组合修改", url: url, showCloseButton: true, parentId: top["win"].tabs.getActiveTab().name };
        var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
    } else {
        mini.alert("请选择一条数据");
    }
}


//搜索
function search(pageSize, pageIndex) {
    var form = new mini.Form("search_form");
    var data = form.getData();
    form.validate();
    if(form.isValid()==false){
        return;
    }
    data.branchId =branchId;
    data.pageNumber = pageIndex + 1;
    data.pageSize = pageSize;
    var param = mini.encode(data); //序列化成JSON
    CommonUtil.ajax({
        url: "/IfsApproveElementController/searchPageLimit",
        data: param,
        callback: function (data) {
            grid.setTotalCount(data.obj.total);
            grid.setPageIndex(pageIndex);
            grid.setPageSize(pageSize);
            grid.setData(data.obj.rows);
        }
    });
}
//加载产品列表
function queryProducts(){
	var params=mini.get("type").getValue();
    CommonUtil.ajax({
        data:{tables:params},
        url:"/IfsApproveElementController/searchTables",
        callback:function(data){
            var prdTypeNo = mini.get("product");
            prdTypeNo.setData(data.obj);
        }
    });	
} 
    //删除
    function del(){
        var rows=grid.getSelecteds();
        if(rows.length==0){
            mini.alert("请选中一行","提示");
            return;
        }
        mini.confirm("您确认要删除该产品所对应的限额?","系统警告",function(value){   
            if (value=='ok'){   
                var data=rows[0];
                params=mini.encode(data);
                CommonUtil.ajax( {
                    url:"/IfsApproveElementController/deleteLimit",
                    data:params,
                    callback : function(data) {
                        mini.alert("删除成功.","系统提示");
                        search(10,0);
                    }
                });
            }
        });
    }
</script>
</html>