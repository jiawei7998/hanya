<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <title>批量审批</title>
    
</head>
<body>
<div class="" id="ApproveOperate_div"  style='background:white;' width="100%">
	
        <div class="centerarea" style="margin:10px;">
            <input id="add_text_combobox" class="mini-combobox" style='width:70%;' labelField="true"  label="常用语："  
                data="[{'id':'1','text':'同意'},{'id':'2','text':'已阅'},{'id':'3','text':'不同意'},{'id':'4','text':'退回修改'}]"/> 
            <a  id="add_text_btn" class="mini-button" style="display: none"  onclick="add()">添加</a>
        </div>
        <div class="centerarea"  style="margin:10px;">
            <input id="approve_msg" class='mini-textarea' labelField="true"  label="审批意见："  required='true'  vtype="maxLength:500" style="width:100%;height:100px;" value="同意"/>  
        </div>
        <div class="centerarea" style="width:100%;text-align: center;margin:10px;"  >
            <a id="continue_btn" class="mini-button"  visible="true"  onclick="passBatch()">通过</a>
            <a id="skip_btn"   class="mini-button"   visible="true" onclick="backBatch()">退回</a>
        </div>
        
</div>
</body>
<script>
mini.parse();
var rows;
var batchParams=[];

function SetData(data){
    rows=mini.clone(data.rows);
}

//添加
function add(){
    var value = mini.get("approve_msg").getValue() + mini.get("add_text_combobox").getText();
	mini.get("approve_msg").setValue(value);
}

//通过
function passBatch(){
 var approve_msg = mini.get("approve_msg");
    approve_msg.validate();
    if(!approve_msg.isValid()){
        mini.alert("请填写审批意见!");
        return false;
    }
    var success=0;
    var flagMsg="";
	//批量组装数据
	for (var i = 0; i < rows.length; i++) {
		batchParams[i]={
			"task_id":rows[i].taskId,
			"op_type":Approve.OperatorType.Continue,
			"specific_user":"",
			"msg":approve_msg.getValue(),
            "serialNo":rows[i].ticketId,
            "orderStatus":rows[i].taskName,
            "productType":rows[i].prdNo,
			"back_to":""
		}
	}
	window.CloseOwnerWindow(batchParams);//关闭进入approve.jsp
}
// 退回
function backBatch(){
    var approve_msg = mini.get("approve_msg");
    approve_msg.validate();
    if(!approve_msg.isValid()){
        mini.alert("请填写审批意见!");
        return false;
    }
    var success=0;
    var flagMsg="";
    //批量组装数据
    for (var i=0;i<rows.length;i++){
        batchParams[i]={
            "task_id":rows[i].taskId,
            "op_type":Approve.OperatorType.Redo,//退回发起
            "specific_user":"",
            "msg":approve_msg.getValue(),
            "serialNo":rows[i].ticketId,
            "orderStatus":rows[i].taskName,
            "productType":rows[i].prdNo,
            "back_to":""
        }
    }
    window.CloseOwnerWindow(batchParams);//关闭进入approve.jsp
}


//通过【已过时】
function pass(){
    var approve_msg = mini.get("approve_msg");
    approve_msg.validate();
    if(!approve_msg.isValid()){
        mini.alert("请填写审批意见!");
        return false;
    }
    var success=0;
    var flagMsg="";
    var messageid = mini.loading("正在提交审批", "请稍后");
    for(var i=0;i<rows.length;i++){
        var param={};
        param.task_id=rows[i].taskId;
        param.specific_user="";
        param.msg=mini.get("approve_msg").getValue();
        param.op_type=Approve.OperatorType.Continue;
        
        if(rows[i].taskName=="待结算" && rows[i].prdNo!='sl'){
       		var data1={"ticketId":rows[i].ticketId,"prdNo":rows[i].prdNo,"indexVal":i};
       		CommonUtil.ajax({
       			url:"/IfsOpicsSettleManageController/checkSearchSettInfoAll",
       			data:data1,
       			callback : function(datac) {
       				if(datac.msg==""){
       					var param1={};
       			        param1.task_id=rows[datac.indexVal].taskId;
       			        param1.specific_user="";
       			        param1.msg=mini.get("approve_msg").getValue();
       			        param1.op_type=Approve.OperatorType.Continue;
				        	(function(n,m,opParam){
				                var defaultParam={
				                    task_id:"",
				                    specific_user:"",
				                    msg:"",
				                    op_type:"",
				                    back_to:"",
				                    success:function(){}
				                };
				                var _opParam = $.extend({},defaultParam,opParam);
				                var params = {};
				                params.task_id = _opParam.task_id;
				                params.msg = _opParam.msg;
				                params.specific_user = _opParam.specific_user;
				                params.op_type = ""+_opParam.op_type;
				                params.back_to=_opParam.back_to;
				                params.data = _opParam.data;
				                
				                CommonUtil.ajax({
				                    url:"/IfsFlowController/approve",
				                    data:params,
				                    callComplete:function(){},
				                    callerror: function(data){
				                    },
				                    callback : function(data) {
				                        if(data.code == "error.common.0000"){
				                            success++;
				                        }
// 				                        if(n==m){
// 				                            mini.alert("审批成功","消息提示",function(){
// 				                                window.CloseOwnerWindow();
// 				                            });
// 				                            mini.alert("您审批成功"+success+"笔,审批失败"+(rows.length-success)+"笔!","消息提示",function(){
// 				                                window.CloseOwnerWindow();
// 				                            });
// 				                        }
				                    }
				                });
				
				            })(i,rows.length-1,param1);
       				}else{
       					flagMsg+=datac.msg;
       				}
       			}
       		});
        }else{
        	(function(n,m,opParam){
                var defaultParam={
                    task_id:"",
                    specific_user:"",
                    msg:"",
                    op_type:"",
                    back_to:"",
                    success:function(){}
                };
                var _opParam = $.extend({},defaultParam,opParam);
                var params = {};
                params.task_id = _opParam.task_id;
                params.msg = _opParam.msg;
                params.specific_user = _opParam.specific_user;
                params.op_type = ""+_opParam.op_type;
                params.back_to=_opParam.back_to;
                params.data = _opParam.data;
                
                
                CommonUtil.ajax({
                    url:"/IfsFlowController/approve",
                    data:params,
                    callComplete:function(){},
                    callerror: function(data){
                    },
                    callback : function(data) {
                        if(data.code == "error.common.0000"){
                            success++;
                        }
                    }
                });

            })(i,rows.length-1,param);
        }
        if(i==rows.length-1){
        	setTimeout(function(){
        			mini.hideMessageBox(messageid);
					var fail=i-success;
					mini.alert("您审批成功"+success+"笔,审批失败"+fail+"笔!","消息提示",function(){
						window.CloseOwnerWindow();
					});
				},2000);
       }
    }

}

//退回【已过时】
function back(){
    var approve_msg = mini.get("approve_msg");
    approve_msg.validate();
    if(!approve_msg.isValid()){
        mini.alert("请填写审批意见!");
        return false;
    }
    var success=0;
    var messageid = mini.loading("正在提交审批", "请稍后");
    for(var i=0;i<rows.length;i++){
        var param={};
        param.task_id=rows[i].taskId;
        param.specific_user="";
        param.msg=mini.get("approve_msg").getValue();
        param.op_type=Approve.OperatorType.Redo;

		(function(n,m,opParam){
            var defaultParam={
                task_id:"",
                specific_user:"",
                msg:"",
                op_type:"",
                back_to:"",
                success:function(){}
            };
            var _opParam = $.extend({},defaultParam,opParam);
            var params = {};
            params.task_id = _opParam.task_id;
            params.msg = _opParam.msg;
            params.specific_user = _opParam.specific_user;
            params.op_type = ""+_opParam.op_type;
            params.back_to=_opParam.back_to;
            params.data = _opParam.data;
            
            CommonUtil.ajax({
                url:"/IfsFlowController/approve",
                data:params, 
                callComplete:function(){
                    
                },
                callerror: function(data){
                },
                callback : function(data) {
                    if(data.code == "error.common.0000"){
                        success++;
                    }
//                     if(n==m){
//                          setTimeout(function(){
//                         	mini.hideMessageBox(messageid);
//          					var fail=m-success;
//          					mini.alert("您退回成功"+success+"笔,退回失败"+fail+"笔!","消息提示",function(){
//          	    				 window.CloseOwnerWindow();
//          					});
//          				},2000);
//                     }
                }
            });

        })(i,rows.length-1,param);
		
		if(i==rows.length-1){
        	setTimeout(function(){
        			mini.hideMessageBox(messageid);
					var fail=i-success;
					mini.alert("您退回成功"+success+"笔,退回失败"+fail+"笔!","消息提示",function(){
						window.CloseOwnerWindow();
					});
				},2000);
       }
    }  
}

 /* function approveOperator(opParam,success){
	var defaultParam={
		task_id:"",
		specific_user:"",
		msg:"",
		op_type:"",
		back_to:"",
		success:function(){}
	};
	var _opParam = $.extend({},defaultParam,opParam);
	var params = {};
	params.task_id = _opParam.task_id;
	params.msg = _opParam.msg;
	params.specific_user = _opParam.specific_user;
	params.op_type = ""+_opParam.op_type;
	params.back_to=_opParam.back_to;
	params.data = _opParam.data;
	
	var messageid = mini.loading("正在提交审批", "请稍后");
	
	CommonUtil.ajax({
		url:"/IfsFlowController/approve",
		data:params, 
		callComplete:function(){
			//loadSystemMessage();
			//mini.hideMessageBox(messageid);
		},
		callerror: function(data){
			mini.hideMessageBox(messageid);
		},
		callback : function(data) {
            if(data.code == "error.common.0000"){
                success++;
            }
			mini.hideMessageBox(messageid);
			
		}
    });
    
    return success;
} 
 */
</script>

</html>