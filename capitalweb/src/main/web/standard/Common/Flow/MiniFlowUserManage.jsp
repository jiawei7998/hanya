<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>用户流程特权</legend>
    <div id="UserManageGrid">
        <input id="userId" name="userId" class="mini-combobox" labelField="true" label="交易状态：" labelStyle="text-align:right;" emptyText="请输入交易状态" 
        />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search"  onclick="search(10,0)">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="deleteBeforeDeal()">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" allowAlternating="true" id="grid"  >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="userId" headerAlign="center" allowSort="true" >用户</div>
                <div field="productType" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'bussTpNm'}">产品类型</div>
                <div field="condition" headerAlign="center" allowSort="true" renderer="onRenderer">条件</div>
                <div field="amt" headerAlign="center" allowSort="true" >金额</div>
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("grid");
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            grid.on("beforeload", function (e) {
                e.cancel = true;
                var pageIndex = e.data.pageIndex;
                var pageSize = e.data.pageSize;
                search(pageSize, pageIndex);
            });
            search(grid.pageSize, grid.pageIndex);
        });
    });
    function add() {
        var url = CommonUtil.baseWebPath() +"/../Common/Flow/MiniFlowUserManageEdit.jsp?action=add";
		var tab = { id: "MiniFlowUserManageAdd", name: "MiniFlowUserManageAdd", title: "新增", url: url,
				showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
		var paramData = {selectData:""};
		CommonUtil.openNewMenuTab(tab,paramData);
     }
    function modify() {
    	var row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/../Common/Flow/MiniFlowUserManageEdit.jsp?action=edit";
			var tab = {id: "MiniFlowUserManageEdit",name:"MiniFlowUserManageEdit",url:url,title:"修改",
						parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
			var paramData = {selectData:row};
			CommonUtil.openNewMenuTab(tab,paramData);
		}else{
			mini.alert("请选择一条记录","提示");
		}
    }
    function deleteBeforeDeal() {
        var record = grid.getSelected();
        if (record) {
        	var saveUrl = "/IfsCfetsFlowUserManageController/deleteFlowUser";
            CommonUtil.ajax({
                url:saveUrl,
                data:{"userId":record.userId,"productType":record.productType},
                callback:function(data){
                    mini.alert("删除成功",'提示信息',function(){
                    	 search(grid.pageSize, grid.pageIndex);
                    });
                }
            });
        } else {
            mini.alert("请选择要删除的记录", "系统提示");
        }
    }
    
    //搜索
    function search(pageSize, pageIndex) {
        var form = new mini.Form("UserManageGrid");
        var data = form.getData();
        form.validate();
        if(form.isValid()==false){
            return;
        }
        data.branchId =branchId;
        data.pageNumber = pageIndex + 1;
        data.pageSize = pageSize;
        var param = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: "/IfsCfetsFlowUserManageController/searchCfetsFlowUserManage",
            data: param,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }
    function clear() {
        var form = new mini.Form("#UserManageGrid");
        form.clear();
        }    
    function onRenderer(e) {
        if (e.value == 1) return "小于";
        else if (e.value == 2) return "小于等于";
        else if (e.value == 3) return "大于";
        else if (e.value == 4) return "大于等于";
    }
    </script>
    </html>