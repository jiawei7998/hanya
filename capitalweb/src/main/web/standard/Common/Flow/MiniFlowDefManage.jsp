<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>审批流程</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-fit" style="width:100%;height:100%;">
    <div class="mini-splitter" style="width:100%;height:100%;" allowResize="false" borderStyle="border:solid 1px #aaa;">
    <div size="50%">
        <fieldset>
            <legend><label>流程定义列表</label></legend>
	        <span style="margin:2px;display: block;">
	        	<a id="search_btn" class="mini-button" style="display: none"    onclick="search(10,0)">刷新</a>
	            <a id="delete_btn" class="mini-button" style="display: none"    onclick="deleteDef()">删除</a>
	        </span>
		</fieldset>
	        <div class="mini-fit">
	            <div id="flow_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
	                idField="p_code" allowResize="true" showpager="false"  allowAlternating="true"
	                onselectionchanged="SearchPrd()"
	                onRowdblclick="custDetail" sortMode="client">
	                    <div property="columns" >
	                        <div type="indexcolumn" align="center" headerAlign="center">序号</div>
	                        <div field="flow_id" width="100" align="center" headerAlign="center" allowSort="true">流程ID</div>    
	                        <div field="flow_name"  width="120" headerAlign="center">流程名称</div>
	                        <div field="version" width="50" align="center" headerAlign="center" allowSort="true">版本</div>    
	                        <div field="model_id" width="80" align="center" headerAlign="center" allowSort="true">来源模型ID</div>    
	                        <div field="model_name"  width="120" headerAlign="center">模型名称</div>
	                    </div>
	            </div>
	        </div>
    </div>
    <div size="50%">
        <fieldset>
            <legend><label>关联产品</label></legend>
        </fieldset>
        <div class="mini-fit">
            <div id="prd_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowAlternating="true"
                idField="p_code" allowResize="true" showpager="false" sortMode="client">
                <div property="columns" >
                    <div type="indexcolumn" align="center" headerAlign="center">序号</div>
                    <div field="product_type" width="70" align="center" headerAlign="center" allowSort="true">产品编号</div>    
                    <div field="product_name" width="120" headerAlign="center">产品名称</div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</body>
<script>
    mini.parse();

    //删除
    function deleteDef(){
        var flowgrid = mini.get("flow_grid");
        var prodgrid = mini.get("prd_grid");
        var flowSelectd = flowgrid.getSelected();
        if(!flowSelectd){
        	return false;
        }
        CommonUtil.ajax( {
            url:"/FlowController/getPrdInfoByFlow",
            data:{"flow_id":flowSelectd.flow_id},
            callback : function(data) {
            	if(data.obj && data.obj.length > 0){
            		mini.alert("该流程定义含有关联产品，无法删除。","提示");
            		return false;
            	}
            	mini.confirm("确认要删除该笔流程定义吗？","确认",function (action) {
           			if (action != "ok") {
           				return;
           			}
	            	CommonUtil.ajax( {
	                    url:"/FlowController/deleteFlowDef",
	                    data:{"old_flow_id":flowSelectd.flow_id,"old_version":flowSelectd.version},
	                    callback : function(data) {
                    		mini.alert("删除成功。");
                    		search();
	                    }
	                });
            	});
            }
        });
    }

    //查询(带分页)
    function search(){
		CommonUtil.ajax({
		  	url:"/FlowController/searchProcDefByType",
			data:{"is_active":1},
			callback:function(data){
				var grid =mini.get("flow_grid");
                //设置数据
                grid.setData(data.obj);
			}
		});
    	var prdgrid =mini.get("prd_grid");
		prdgrid.setData([]);
    }    

    function SearchPrd(){
    	var grid =mini.get("flow_grid");
    	var selected = grid.getSelected();
    	if(!selected){
    		return false;
    	}
        CommonUtil.ajax( {
            url:"/FlowController/getPrdInfoByFlow",
            data:{"flow_id":selected.flow_id},
            callback : function(data) {
                //设置数据
            	var prdgrid =mini.get("prd_grid");
            	prdgrid.setData(data.obj);
            }
        });

    }
    
    $(document).ready(function(){
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
            search();
        });
    });

</script>

</html>