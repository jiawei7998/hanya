<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>审批组维护</title>
</head>
<body>
    <div id="flowRoleEdit" class="mini-panel" style="width:100%"  allowResize="true" collapseOnTitleClick="false">
        <div id="field" class="fieldset-body">
	        <table id="role_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
	            <tr>
	                <td style="width:50%;vertical-align: top;">
	                        <input id="role_name" name="role_name" class="mini-textbox mini-mustFill"  
	                        labelField="true"  label="审批组名称：<i class='red-star'></i>" emptyText="请输入审批组名称"
	                        required="true"  vtype="maxLength:32" style="width:100%"
	                        labelStyle="text-align:left;width:170px"/>
	                        <input id="role_id" name="role_id" class="mini-hidden"/>  
	                </td>
	                <td style="width:50%">
	                        <input id="memo" name="memo" class="mini-textarea" emptyText="请输入备注"
	                        labelField="true"  label="备注：" style="width:100%" labelStyle="text-align:left;width:170px"
	                        vtype="maxLength:50"/>
	                </td>
	            </tr>
	            <tr>
	                <td colspan="2" style="text-align:center;">
	                    <a  id="save_btn" class="mini-button" style="display: none"   onclick="save">保存</a>
	                    <a id="cancel_btn" class="mini-button" style="display: none"   onclick="cancel">取消</a>
	                </td>
	            </tr>
	        </table>
        </div>
    </div>

    <script>
		mini.parse();
        $(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
				init();
			});
        });

        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");

        function init(){
            
            if(action == "new"){
            	mini.get("flowRoleEdit").setTitle("审批组 >> 新增");
                
            }else if(action == "edit" || action == "detail"){
                var form1 = new mini.Form("role_form");
                var data=top["flowRole"].getData();
                form1.setData(data);
                
                if(action == "detail"){
                	mini.get("flowRoleEdit").setTitle("审批组 >> 详情");
                    form1.setEnabled(false);
                    document.getElementById("save_btn").style.display="none";
                    document.getElementById("cancel_btn").style.display="none";
                    
                }else{
                	mini.get("flowRoleEdit").setTitle("审批组 >> 修改");
                }
            }
        }

         //保存
         function save(){
            var form = new mini.Form("role_form");
            form.validate();
	        if (form.isValid() == false){
                mini.alert("请输入有效的数据!","系统提示");
                return;//表单验证
            }
	        var data = form.getData();
            var param = mini.encode(data); //序列化成JSON
            var saveUrl="";
            if(action == "new"){
                saveUrl = "/FlowRoleController/roleEdit";
            }else if(action == "edit"){
                saveUrl = "/FlowRoleController/roleEdit";
            }
            CommonUtil.ajax({
				url:saveUrl,
                data:param,
            	callback:function(data){
					if('error.common.0000' == data.code){
				        mini.alert("保存成功!","系统提示",function(value){
                            //关闭并刷新
						    setTimeout(function(){ top["win"].closeMenuTab() },100);
                        });
				
                    }else{
                        mini.alert("保存失败！","系统提示");
                    }	
                }
			});
        }
        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();

        }
    </script>
    
</body>
</html>