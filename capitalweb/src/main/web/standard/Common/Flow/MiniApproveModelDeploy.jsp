<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%> 
<html>
<head>
    <title>审批模型部署</title>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
</head>
<body>
    <fieldset id="fd2" class="mini-fieldset" width="100%" height="400px">
        <legend><label id="labell">审批模型>>部署</label></legend>
        <div id="field" class="fieldset-body">
            <table id="field_form"  width="100%" cols="4">
                <tr>
                    <td width="100%"><input id="delpoy_model_id" name="delpoy_model_id" class="mini-textbox"  
                        labelField="true"  label="流程模型ID："  enabled="false"
                        width="50%" labelStyle="width:120px"/>
                    </td>
                </tr>
                <tr>
                    <td width="100%"><input id="deploy_name" name="deploy_name" class="mini-textbox"  
                        labelField="true"  label="流程模型名称：" emptyText="请输入流程名称"
                        required="true"  vtype='maxLength:32'
                        width="50%" labelStyle="width:120px"/>
                    </td>
                </tr>
                <tr>
                    <td width="100%"><input id="deploy_type" name="deploy_type" class="mini-combobox"  
                        labelField="true"  label="流程模型类型：" emptyText="请选择流程类型"
                        data="CommonUtil.serverData.dictionary.FlowType"
                        width="50%" labelStyle="width:120px"/>
                    </td>
                </tr>
                <tr>
                    <td width="100%"><input id="deploy_is_active" name="deploy_is_active" class="mini-combobox"  
                        labelField="true"  label="是否启用：" emptyText="请选择是否启用" 
                        data="CommonUtil.serverData.dictionary.YesNo"
                        width="50%" labelStyle="width:120px"/>
                    </td>
                </tr>
            </table>
        </div>
    </fieldset>
    <div class="btn_div" style="float:right;margin:15px">
        <a  id="deploy_btn" class="mini-button"   onclick="deployModel">点击部署</a>
    </div>

    <script>
        $(document).ready(function(){
            mini.parse();
            init();
        });

        var url = window.location.search;
        var modelId = CommonUtil.getParam(url,"modelid");
        var modelName = CommonUtil.getParam(url,"modelname");

        function init(){
            var id=mini.get("delpoy_model_id");
            id.setValue(modelId);
            var name=mini.get("deploy_name");
            name.setValue(modelName);
        }

        //部署模型
        function deployModel(){
            var deployName = mini.get("deploy_name").getValue().trim();
	        var deployType = mini.get("deploy_type").getValue();
            var deployIsActive = mini.get("deploy_is_active").getValue();
            if(deployName==null || deployName==''){
                mini.alert("流程名称未填写");
                return;
            }else if(deployType==null || deployType==''){
                mini.alert("流程类型未选择");
                return;
            }
            var form = new mini.Form("field_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            } 
            
                var param = {};
                var proc_def_info = {
                    "flow_name":deployName,
                    "flow_type":deployType,
                    "is_active":deployIsActive
                };
                param = {
                    "model_id":modelId,
                    "proc_def_info":proc_def_info,
                    "override": false
                };
                CommonUtil.ajax({
                    url: "/FlowModelController/deployModel",
                    data: param,
                    callback: function(result) {
                        
                        if(result.need_override) {
                            
                            mini.confirm(result.message,"提示信息",  function(r) {
                                if(r == "ok"){	
                                    param.override = true;							
                                    CommonUtil.ajax({
                                        url: "/FlowModelController/deployModel",
                                        data: param,
                                        callback: function(data){
                                            mini.alert(data.message,"提示信息",function(value){
                                               if(value == 'ok'){
                                                    //关闭并刷新
						                            setTimeout(function(){ top["win"].closeMenuTab() },100);
                                               }
                                                
                                            });
                                           
                                        }
                                    });
                                }
                            });
                        }else {
                            mini.alert(result.message,"提示信息",function(value){
                                if(value == 'ok'){
                                    //关闭并刷新
						            setTimeout(function(){ top["win"].closeMenuTab() },100);
                                }
                                                
                            });
                            
                        }
                    }
                });
            


        }
    </script>
</body>
</html>