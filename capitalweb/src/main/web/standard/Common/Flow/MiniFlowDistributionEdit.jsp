<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>

<body style="width:100%;height:100%;background:white">

<div id="MiniFlowDistributionEdit" class="mini-fit">
	<div class="mini-splitter" style="width:100%;height:100%;"  borderStyle="border:solid 1px #aaa;">
		<div size="60%">
			<fieldset class="mini-fieldset">
				<legend>产品流程分配</legend>
				<input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
				<input id="prdTypeNo" name="prdTypeNo" class="mini-hidden">
				<div id="searchForm" style="width:100%;">
					<table width="100%">
						<tr>
							<td width="100%">
								<input id="prdTypeName" name="prdTypeName" class="mini-textbox" labelField="true"  label="产品类型" enabled="false" style="width:80%;"/>
							</td>
						</tr>
						<tr>
							<td width="100%">
								<input id="flow_type" name="flow_type" class="mini-combobox" labelField="true"  label="流程类型" style="width:80%;" emptyText="请选择流程类型" required="true" />
							</td>
						</tr>
					</table>
					<div id="toolbar1" class="mini-toolbar" style="margin-left: 20px;">
						<table style="width:100%;">
							<tr>
							<td style="width:100%;">
								<a id="save_btn" class="mini-button" style="display: none"  >保存产品流程</a>
								<a id="clear_btn" class="mini-button" style="display: none"  >清空机构选择项</a>
							</td>
							</tr>
						</table>
					</div>
				</div>
			</fieldset>

			<div id="flowGrid" class="mini-datagrid borderAll" style="width:100%;height:70%;" idField="dealNo" 
					allowAlternating="true" allowResize="true" border="true" allowSortColumn="false"
					multiSelect="false" onrowdblclick="onDblClickRow" showPager="false">
				<div property="columns">
					<div type="indexcolumn" headerAlign="center" width="50">序号</div>
					<div field="flow_id" width="120" headerAlign="center" allowSort="true">流程编号</div> 
					<div field="version" width="50" headerAlign="center" allowSort="true">流程版本</div>
					<div field="flow_name" width="150" headerAlign="center" allowSort="true">流程名称</div>
					<div field="flow_type" width="120" headerAlign="center" allowSort="true" renderer="CommonUtil.dictRenderer" data-options="{dict:'FlowType'}" >流程类型</div>
					<div field="model_id" width="120" headerAlign="center" allowSort="true">流程模型ID</div>
					<div field="model_name" width="120" headerAlign="center" allowSort="true">流程模型名称</div>
				</div>
			</div>
		
		</div>
	
		<div size="40%">
			<div id="instGrid" class="mini-treegrid" showTreeIcon="true" treeColumn="instName" idField="id" parentField="parentId" resultAsTree="false"
			allowResize="true" expandOnLoad="true" height="100%" showCheckBox="true" checkRecursive="true">
				<div property="columns" allowMove="true" allowResize="true">
					<div type="indexcolumn" width="50" headerAlign="center">序号</div>
					<div name="instName" field="instName" width="250" headerAlign="center">机构简称</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<script type="text/javascript">
	var FlowDistributionEdit = {};
	mini.parse();

	//获取当前tab
	var currTab = top["win"].tabs.getActiveTab();
	var params = currTab.params;
	var prdTypeNo = params.prdTypeNo;
	mini.get("prdTypeNo").setValue(prdTypeNo);
	mini.get("prdTypeName").setValue(params.prdTypeName);

	var flowGrid = mini.get("flowGrid");
	var instGrid = mini.get("instGrid");

	//加载机构树
	function loadInstitutions() {
		var param = {branchId:mini.get("branchId").getValue()};
		CommonUtil.ajax({
			url: '/InstitutionController/searchInstitutions',
			data: param,
			callback: function (data) {
				instGrid.setData(data.obj);
			}
		});
	}

	function loadFlowType(){
		var dict = CommonUtil.getDictList("FlowType");
		var result = new Array();
		CommonUtil.ajax({
			url:"/ProductTypeController/getFlowTypeByProductType",
			data:{"prdTypeNo":prdTypeNo},
			callback : function(data) {
				for(var item = 0; item < dict.length; item++)
				{
					for(var item2 = 0; item2 < data.obj.length; item2++)
					{
						if(dict[item].value == data.obj[item2].flowTypeNo){
							result.push(dict[item]);
						}
					}
				}

				mini.get("flow_type").setData(result);
			}// end function
		});
	}

	function onDblClickRow()
	{
		var row = flowGrid.getSelected();
		if(CommonUtil.isIE(6) || CommonUtil.isIE(7) || CommonUtil.isIE(8) || CommonUtil.isIE(9)){
			window.open(baseUrl+'slbpm/processDefinition_temp/showProcessDefinitionImage?processDefinitionId='+row.flow_id);
		}else{
			window.open(baseUrl+'slbpm/diagram-viewer/definition_index.html?processDefinitionId='+row.flow_id);
		}
	}

	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
			mini.get("flow_type").on("valuechanged", function () {
				var flow_type = mini.get("flow_type").getValue();
				CommonUtil.ajax({
					url: "/FlowController/searchProcDefByType",
					data: {"flow_type": flow_type},
					callback: function (data) {
						flowGrid.setData(data.obj);
						instGrid.expandAll();
					}
				});

				/****
				 CommonUtil.ajax({
				url:"/FlowController/getProcDefPropMap",
				data:{"flow_type" : flow_type , "product_type" : prdTypeNo},
				callback:function(data){
					if(data.obj){
						instGrid.uncheckAllNodes();
						//禁选已配置流程的机构
						$.each(data.obj,function(i,item){
							var node = instGrid.findRow(function(row){
								//返回当前流程类型下的所有流程
								if(row.id == item.inst_code) return true;
							});
							if(node){
								instGrid.checkNode(node);
							}
						});
					}
				}
			});
				 ***/
			}); // end flow_type function


			//事件：保存按钮
			mini.get("save_btn").on("click", function () {
				//流程类型
				var flow_type = mini.get("flow_type").getValue();
				//流程实例
				var selected = flowGrid.getSelected();
				if (!selected) {
					mini.alert("请选择一条流程.", "警告");
					return false;
				}
				//机构id
				var instTree = instGrid.getCheckedNodes();
				var instIds = "";
				for (var i = 0; i < instTree.length; i++) {
					instIds += instTree[i].id;
					instIds += (i == instTree.length - 1 ? "" : ",");
				}
				if (CommonUtil.isNull(instIds)) {
					mini.alert("请选择至少一个机构.", "警告");
					return false;
				}
				var messageid = mini.loading("正在操作", "请稍后");
				CommonUtil.ajax({
					url: "/FlowController/bindProcDefProps",
					data: {
						"product_type": prdTypeNo,
						"instIds": instIds,
						"flow_id": selected.flow_id,
						"version": selected.version,
						"flow_type": flow_type
					},
					callback: function (data) {
						mini.hideMessageBox(messageid);
						if (data.code = "error.common.0000") {
							mini.alert("保存成功", "提示");
						}
					}, callerror: function (data) {
						mini.hideMessageBox(messageid);
					}
				});
			});// end function

			mini.get("clear_btn").on("click", function () {
				instGrid.uncheckAllNodes();
			});


			//加载机构
			loadInstitutions();
			//加载流程类型
			loadFlowType();
		});
	});
</script>