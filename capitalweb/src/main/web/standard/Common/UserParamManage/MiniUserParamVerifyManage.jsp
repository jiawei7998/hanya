<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <title>用户参数复核</title>   
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
	<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>

</head>
<body style="width:100%;height:100%;background:white">
	
    <fieldset id="fd2" class="mini-fieldset title">
        <legend><label>查询条件</label></legend>
        <div id="find" >
            <div id="search_form"  >
                <input id="p_id" name="p_id" class="mini-textbox" labelField="true"  
                label="参数编号："  emptyText="请输入参数编号" labelStyle="text-align:right;"/>
                <input id="p_name" name="p_name" class="mini-textbox" labelField="true" 
                    label="参数名称："  emptyText="请输入参数名称" labelStyle="text-align:right;"/>
                <input id="p_value" name="p_value" class="mini-textbox" labelField="true" 
                    label="参数值："  emptyText="请输入参数值" labelStyle="text-align:right;"/>
                <input id="p_sub_type" name="p_sub_type" class="mini-combobox" 
                emptyText="请选择参数小类" data="CommonUtil.serverData.dictionary.UserParameterType" 
                labelField="true"  label="参数小类："  labelStyle="text-align:right;"/>
                <input id="p_sort" name="p_sort" class="mini-textbox" labelField="true"  
                label="参数排序："  emptyText="请输入参数排序" labelStyle="text-align:right;"/>
                <input id="p_sub_name" name="p_sub_name" class="mini-textbox" 
                labelField="true"  label="参数小类描述："  emptyText="请输入参数小类描述" labelStyle="text-align:right;"/>
                <span style="float:right;margin-right: 150px">
                    <a id="search_btn" class="mini-button" style="display: none"   onclick="search1()" >查询</a>
                    <a id="clear_btn" class="mini-button" style="display: none"    onClick="clear">清空</a>
                </span>
                
            </div>
            
        </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a id="pass_btn" class="mini-button" style="display: none"    onclick="pass">复核通过</a>
        <a id="refuse_btn" class="mini-button" style="display: none"    onclick="refuse">复核拒绝</a>
    </span>
    <div id="UserParamVerify" class="mini-fit" style="margin-top: 2px;"> 
        <div id="UserParamVerifyManage" class="mini-datagrid borderAll" style="width:100%;height:100%;" 
            idField="p_id"  allowResize="true" multiSelect="false" sortMode="client" allowAlternating="true">
            <div property="columns" >
                <div type="indexcolumn" align="center" headerAlign="center" width="40">序号</div>
                <div field="p_id" width="80" headerAlign="center" align="center" allowSort="true" dataType="int">参数编号</div>    
                <div field="p_type" width="80" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'ParameterBigKind'}">参数大类</div>
                <div field="p_sub_type"   width="120" align="center" headerAlign="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'UserParameterType'}" >参数小类</div>                                
                <div field="p_sub_name" width="180" headerAlign="center">参数小类描述</div>
                <div field="p_name" width="180" headerAlign="center">参数名称</div>
                <div field="status"   headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'status'}">状态</div>
                <div field="p_value" width="180" headerAlign="center">参数值</div>
                <div field="p_sort" width="100" headerAlign="center" align="center" allowSort="true" dataType="int">排序</div>
                <div field="p_code" width="100" headerAlign="center" align="center">参数子节点</div>
                <div field="p_parent_code" width="100" headerAlign="center" align="center">参数父节点</div>
                <div field="ioper" width="100" headerAlign="center" align="center">经办人</div>
                <div field="itime" width="160" headerAlign="center" align="center" allowSort="true" dataType="date">经办时间</div>
            </div>
        </div>
    </div>
	
	<script>
        

        mini.parse();
		$(document).ready(function(){
			var grid =mini.get("UserParamVerifyManage");
			grid.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex; 
				var pageSize = e.data.pageSize;
				search(pageSize,pageIndex);
			});
			search(grid.pageSize,0);
    	
		});

        function search(pageSize,pageIndex){
			var form =new mini.Form("find");
			form.validate();
			if (form.isValid() == false) return;//表单验证
			var data =form.getData();//获取表单数据
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			var param = mini.encode(data); //序列化成JSON
			CommonUtil.ajax({
				url:"/UserParamController/searchUserParamWaitingVerify",
				data:param,
				callback:function(data){
						var grid =mini.get("UserParamVerifyManage");
						//设置分页
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
						grid.setPageSize(pageSize);
						//设置数据
						grid.setData(data.obj.rows);
					}
				});
		}

        
		

		//复核通过
		function pass(){
            var grid =mini.get("UserParamVerifyManage");
            var row = grid.getSelected();
            if(row){
                CommonUtil.ajax({
                    url:"/UserParamController/verifyOperation",
                    data:row,
                    callback:function(data){
                        if(data && data.code == "error.common.0000"){
							mini.alert("复核通过！");
						}else{
							mini.alert("复核失败！");
						}
                        var grid =mini.get("UserParamVerifyManage");
						search(grid.pageSize,0);
                    }
                });
            }else{
                mini.alert("请先选择一条记录！");
            }
		}

		//复核拒绝
		function refuse(){
            var grid =mini.get("UserParamVerifyManage");
            var row = grid.getSelected();
            if(row){
                CommonUtil.ajax({
                    url:"/UserParamController/verifyRefuse",
                    data:row,
                    callback:function(data){
                        if(data && data.code == "error.common.0000"){
							mini.alert("复核拒绝成功！");
						}else{
							mini.alert("复核拒绝失败！");
						}
                        var grid =mini.get("UserParamVerifyManage");
						search(grid.pageSize,0);
                    }
                });
            }else{
                mini.alert("请先选择一条记录！");
            }
		}

         //清空
         function clear(){
           // var grid =mini.get("UserParamVerifyManage");
            var form =new mini.Form("find");
            form.clear();
           // search(grid.pageSize,0);
        }

        function search1(){
            var grid =mini.get("UserParamVerifyManage");
            search(grid.pageSize,0);
        }


		
       
	</script>

</body>
</html>