<%@ page language="java" pageEncoding="UTF-8" %>
<div class="centerarea">
    <div id="panel5" class="mini-panel" title="OPICS 管理要素" style="width:100%" allowResize="true"
         collapseOnTitleClick="false">
        <input id="pcode" name="pcode" type="hidden"/>
        <input id="ptype" name="ptype" type="hidden"/>
        <input id="costcent" name="costcent" type="hidden"/>
        <input id="portfolio" name="portfolio" type="hidden"/>
        <div class="leftarea">
            <input id="dealSource" name="dealSource" class="mini-combobox mini-mustFill" labelField="true"
                   style="width:100%;" label="交易来源：" required="true"
                   data="CommonUtil.serverData.dictionary.TradeSource" vtype="maxLength:10"
                   labelStyle="text-align:left;width:130px;">
            <input id="prodType" name="prodType" onbuttonclick="onTypeEdit" allowInput="false"
                   class="mini-buttonedit mini-mustFill" labelField="true" style="width:100%;" label="产品类型："
                   required="true" vtype="maxLength:2" labelStyle="text-align:left;width:130px;">
        </div>
        <div class="rightarea">
            <input id="product" name="product" onbuttonclick="onPrdEdit" allowInput="false"
                   class="mini-buttonedit mini-mustFill" labelField="true" style="width:100%;" label="产品代码："
                   required="true" maxLength="10" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                   labelStyle="text-align:left;width:130px;">
            <input id="cost" name="cost" onbuttonclick="onCostEdit" allowInput="false"
                   class="mini-buttonedit mini-mustFill" labelField="true" label="成本中心：" style="width:100%;"
                   required="true" vtype="maxLength:15" labelStyle="text-align:left;width:130px;">

        </div>
        <input id="ccysmeans" name="ccysmeans" class="mini-buttonedit" labelField="true" label="付款方式："
               required="false" vtype="maxLength:7" labelStyle="text-align:left;width:130px;"
               style="width:100%;display:none;" onbuttonclick="settleMeans1()" allowInput="false">
        <input id="ccysacct" name="ccysacct" class="mini-textbox" labelField="true" label="付款账户：" required="false"
               vtype="maxLength:15" labelStyle="text-align:left;width:130px;" style="width:100%;display:none;"
               enabled="false">
        <input id="ctrsmeans" name="ctrsmeans" class="mini-buttonedit" labelField="true" label="收款方式："
               required="false" vtype="maxLength:7" labelStyle="text-align:left;width:130px;"
               style="width:100%;display:none;" onbuttonclick="settleMeans2()" allowInput="false">
        <input id="ctrsacct" name="ctrsacct" class="mini-textbox" labelField="true" label="收款账户：" required="false"
               vtype="maxLength:15" labelStyle="text-align:left;width:130px;" style="width:100%;display:none;"
               enabled="false">

    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        mini.get("dealSource").setValue(CommonUtil.serverData.dictionary.TradeSource[0].id);
        InitTcProductOpics();
    });

    //初始化值
    function InitTcProductOpics() {
        CommonUtil.ajax({
            data: {"fprdCode": fPrdCode},
            url: "/TcProductOpicsController/selectByPrdCode",
            callback: function (data) {
                $('#pcode').val(data.obj.pcode);
                $('#ptype').val(data.obj.ptype);
                $('#costcent').val(data.obj.costcent);
                $('#portfolio').val(data.obj.portfolio);
            }
        });
    }

    /* 产品代码的选择 */
    function onPrdEdit() {
        var btnEdit = this;
        var pcode = $('#pcode').val();
        var data = {"pcodelst": pcode.split(",")};//配置表信息动态传入
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/prodMiniLess.jsp",
            title: "选择产品代码",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.pcode);
                        btnEdit.setText(data.pdesc);
                        onPrdChanged();
                        btnEdit.focus();
                    }
                }

            }
        });

    }

    /* 产品类型的选择 */
    function onTypeEdit() {
        var prd = mini.get("product").getValue();
        var direction = "";
        var al = "";
        if (prd == null || prd == "") {
            mini.alert("请先选择产品代码!");
            return;
        }
        if (prd == "REPO") {
            if (mini.get("myDir") == null) {
                direction = mini.get("direction").getValue();
            } else {
                direction = mini.get("myDir").getValue();
            }
            if (direction != "" && direction == 'S') {//拆出-资产
                al = 'L';
            } else if (direction != "" && direction == 'P') {
                al = 'A';
            } else {
                al = "";
            }
        }
        var ptype = $('#ptype').val();
        var data = {"prd": prd, "al": al, "typelst": ptype.split(",")};
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/typeMiniLess.jsp",
            title: "选择产品类型",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.type);
                        btnEdit.setText(data.descr);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    /* 成功中心的选择 */
    function onCostEdit() {
        var btnEdit = this;
        var costcent = $('#costcent').val();
        var data = {"prdName": prdCode, "costcentlst": costcent.split(",")};
        mini.open({
            url: CommonUtil.baseWebPath() + "/opics/costMiniLess.jsp",
            title: "选择成本中心",
            width: 700,
            height: 500,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.costcent);
                        btnEdit.setText(data.costdesc);
                        btnEdit.focus();
                    }
                }
            }
        });
    }

    function onPrdChanged() {
        if (mini.get("prodType").getValue() != "") {
            mini.get("prodType").setValue("");
            mini.get("prodType").setText("");
            mini.alert("产品代码已改变，请重新选择产品类型!");
        }
    }
</script>