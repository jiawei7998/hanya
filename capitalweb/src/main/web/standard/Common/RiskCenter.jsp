<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=basePath%>/standard/Common/RiskCenter.js"></script>
<div class="centerarea">
	<div visible="false" id="panel2" class="mini-panel" title="风险中台" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
			<input   style="width:100%;"  id="applyNo" name="applyNo" class="mini-buttonedit mini-mustFill" onbuttonclick="onLimitRisk" allowInput="false" labelField="true"  label="信用风险审查单号："  style="width:100%;"  labelStyle="text-align:left;width:130px;" vtype="maxLength:16" requiredErrorText="该输入项为必输项" required="false"/>
			<input id="custNo" name="custNo" class="mini-buttonedit " labelField="true"  label="占用授信主体："  style="width:100%;"  labelStyle="text-align:left;width:130px;"  vtype="maxLength:16" enabled="false"/>
			<!-- <input id="tradeLimit" name="tradeLimit" class="mini-textbox "  labelField="true"  label="交易限额："  style="width:100%;"  labelStyle="text-align:left;width:130px" vtype="maxLength:16" enabled="false"/> -->
			<input id="DV01Risk" name="DV01Risk" class="mini-textbox "  labelField="true"  label="DV01："  style="width:100%;"  labelStyle="text-align:left;width:130px" minValue="-999999999999.9999999999" maxValue="9999999999999999.99999999" format="n4" vtype="maxLength:16" enabled="false"/>
			<input id="longLimit" name="longLimit" class="mini-textbox " labelField="true"  label="久期限："  style="width:100%;"  labelStyle="text-align:left;width:130px;"  minValue="-999999999999.9999999999" maxValue="9999999999999999.99999999" format="n4" vtype="maxLength:16" enabled="false"/>
			<input id="riskDegree" name="riskDegree" class="mini-combobox mini-mustFill" required="false" labelField="true"  label="风险程度："  style="width:100%;"  labelStyle="text-align:left;width:130px" vtype="maxLength:16"  showNullItem="true" data="CommonUtil.serverData.dictionary.RiskDegree" visible = "false"/>
		</div>
        <div class="rightarea">
        	<input style="width:100%;" id="applyProd" name="applyProd" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="false" label="产品名称："  labelStyle="text-align:left;width:130px;" enabled="false"/>
        	<input style="width:100%;" id="weight" name="weight" class="mini-textbox mini-mustFill" labelField="true" requiredErrorText="该输入项为必输项" required="false" label="授信占用权重%：" value = "100" minValue="0" maxValue="100"  labelStyle="text-align:left;width:130px;" changeOnMousewheel="false" / >
        	<input id="lossLimit" name="lossLimit" class="mini-textbox " labelField="true"  label="止损："  style="width:100%;"  labelStyle="text-align:left;width:130px;"  required="false" minValue="-999999999999.9999999999" maxValue="9999999999999999.99999999" format="n4" vtype="maxLength:16" enabled="false"/>
        	<input id="profitLoss" name="profitLoss" class="mini-textbox " labelField="true"  label="未交割头寸损益："  style="width:100%;"  labelStyle="text-align:left;width:130px;"  required="false" vtype="maxLength:16" enabled="false" visible="false"/>
        	<input id="priceDeviation" name="priceDeviation" class="mini-textbox" enabled="false" visible="false" labelField="true"  label="偏离度：" style="width:100%;" labelStyle="text-align:left;width:130px;"/ >
        </div>
	</div>
</div>


