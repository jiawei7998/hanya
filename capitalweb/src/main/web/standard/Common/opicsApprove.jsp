<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=basePath%>/standard/Common/opics.js"></script>
<div class="centerarea">
	<fieldset>
		<legend>OPICS管理要素</legend>
        <input id="port" name="port" class="mini-buttonedit" onbuttonclick="onPortQuery"  labelField="true"  label="投资组合：" required="true"  allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;" >
        <input id="cost" name="cost" class="mini-textbox" labelField="true"  label="成本中心：" required="true"  enabled="false" vtype="maxLength:15" labelStyle="text-align:left;"  >
        <input id="product" name="product" class="mini-textbox" labelField="true"  label="产品代码：" required="true"  enabled="false"  maxLength="10" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;" >
        <input id="prodType" name="prodType" class="mini-textbox" labelField="true"  label="产品类型：" required="true"  enabled="false"  vtype="maxLength:2" labelStyle="text-align:left;" >
	</fieldset>
</div>


