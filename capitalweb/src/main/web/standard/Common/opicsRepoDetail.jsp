<%@ page language="java" pageEncoding="UTF-8" %>
<div class="centerarea">
    <div id="panel1" class="mini-panel" title="OPICS 管理要素" style="width:100%" allowResize="true"
         collapseOnTitleClick="true">
        <div class="leftarea">
            <input id="dealSource" name="dealSource" class="mini-combobox mini-mustFill" labelField="true"
                   style="width:100%;" label="交易来源：" required="true" data="CommonUtil.serverData.dictionary.TradeSource"
                   vtype="maxLength:10" labelStyle="text-align:left;width:130px;">
            <input id="prodType" name="prodType" onbuttonclick="onTypeEdit" allowInput="false"
                   class="mini-textbox mini-mustFill" labelField="true" style="width:100%;" label="产品类型："
                   required="true" vtype="maxLength:2" labelStyle="text-align:left;width:130px;">

        </div>
        <div class="rightarea">
            <input id="product" name="product" onbuttonclick="onPrdEdit" allowInput="false"
                   class="mini-textbox mini-mustFill" labelField="true" style="width:100%;" label="产品代码："
                   required="true" maxLength="10" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])"
                   labelStyle="text-align:left;width:130px;">
            <input id="cost" name="cost" onbuttonclick="onCostEdit" allowInput="false"
                   class="mini-textbox mini-mustFill" labelField="true" label="成本中心：" style="width:100%;"
                   required="true" vtype="maxLength:15" labelStyle="text-align:left;width:130px;">

        </div>
        <input id="ccysmeans" name="ccysmeans" class="mini-textbox" labelField="true" label="付款方式：" required="false"
               vtype="maxLength:7" labelStyle="text-align:left;width:130px;" style="width:100%;display:none;"
               onbuttonclick="settleMeans1()" allowInput="false">
        <input id="ccysacct" name="ccysacct" class="mini-textbox" labelField="true" label="付款账户：" required="false"
               vtype="maxLength:15" labelStyle="text-align:left;width:130px;" style="width:100%;display:none;"
               enabled="false">
        <input id="ctrsmeans" name="ctrsmeans" class="mini-textbox" labelField="true" label="收款方式：" required="false"
               vtype="maxLength:7" labelStyle="text-align:left;width:130px;" style="width:100%;display:none;"
               onbuttonclick="settleMeans2()" allowInput="false">
        <input id="ctrsacct" name="ctrsacct" class="mini-textbox" labelField="true" label="收款账户：" required="false"
               vtype="maxLength:15" labelStyle="text-align:left;width:130px;" style="width:100%;display:none;"
               enabled="false">

    </div>
</div>