<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<%
    String branchId = __sessionUser.getBranchId();
    String adminUserId = __sessionUser.getUserId();
%>
<html>
<head>
</head>

<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>机构查询</legend>
    <div showCollapseButton="false" showHeader="true">
        <table id="institution_search_form" style="width:100%;">
            <tr>
                <td>
                    <input id="instName" name="instName" class="mini-textbox" labelField="true" label="机构简称:"
                           style="width:90%;" emptyText="请输入机构简称"/>
                </td>
                <td>
                    <a class="mini-button" style="display: none" id="search_btn" onclick="search();">查询</a>
                </td>
            </tr>
        </table>
    </div>
</fieldset>
<div class="mini-fit">
    <div id="treegrid" class="mini-datagrid" showTreeIcon="true" treeColumn="instName" idField="id"
         parentField="parentId"
         resultAsTree="true" allowResize="false" expandOnLoad="true" onrowdblclick="onRowDblClick"
         allowAlternating="true" sortMode="client">
        <div property="columns">
            <div type="indexcolumn" width="50">序号</div>
            <div name="dict_value" field="dict_value" width="200">机构简称</div>
            <div field="dict_key" width="60">机构代码</div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            search();
        });
    });


    mini.parse();

    function search() {
        var form = new mini.Form("institution_search_form");
        form.validate();
        //提交数据
        var data = form.getData();//获取表单多个控件的数据
        data.dict_id = "instId";
        data.dict_value = data.instName;
        var params = mini.encode(data); //序列化成JSON
        CommonUtil.ajax({
            url: '/TaDictController/getTaDictByDictIdForList',
            data: params,
            callback: function (data) {
                var grid = mini.get("treegrid");
                grid.setData(data.obj);
            }
        });
    }

    function GetData() {
        var grid = mini.get("treegrid");
        var row = grid.getSelected();
        return row;
    }

    function onRowDblClick(e) {
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    function onCancel() {
        CloseWindow("cancel");
    }

</script>
</body>
</html>