<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">



<div class="mini-fit" >
	<div style="width:100%;height:100%; ">
		<div id="p1" class="mini-panel" title="已选节点" style="width:47%;height:100%;float: left">
   				<div id="datagrid1" class="mini-datagrid borderAll"  idField="id"   showCheckBox="true" multiSelect="true" showPager="false">
            <div property="columns">
            	<div type="checkcolumn"></div>
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="parId" header="附件树ID" headerAlign="center" align="center" width="50px"></div>
                <div field="treeName" header="附件树名称" headerAlign="center" align="center"></div>
                <div field="attachId" header="节点ID"  headerAlign="center" align="center" width="50px"></div>
                <div field="nodeName"  header="节点名称" headerAlign="center" align="center"></div>
            </div>
        </div>
    
		</div>
        
        <div style="float: left;width: 5%;text-align:center; ">
            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button" style="display: none"  style="width: 40px;"  onclick="add">&gt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="addAll">&gt;&gt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="removeAll">&lt;&lt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="removes">&lt;</a></div>
        </div>
        
        <div id="p2" class="mini-panel" title="可选节点" style="width:47%;height:100%;float: left;">
			
			<div id="search_form" class="mini-form" style="border-bottom:0;padding:0px;">
	            <table style="width:100%;">
	                <tr>
	                    <td style="width:100%;">
	                        <input id="filterList" name="filterList" value="" class="mini-hidden" />
							<input id="useFlag" name="useFlag" value="1" class="mini-hidden" />
							<input id="paramName" name="nodName" class="mini-textbox" labelField="true"  label="节点名称："  width="300px"  labelStyle="text-align:right;width:80px"  emptyText="请输入节点名称" />
	                    </td>
	                    <td style="white-space:nowrap;">
	                        <a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
							<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
	                    </td>
	                </tr>
	            </table>           
        	</div>
			  
			<div class="mini-fit" >
	        	<div id="datagrid2" class="mini-datagrid borderAll" idField="id" showCheckBox="true" multiSelect="true" style="width:100%;height:100%;" >
		            <div property="columns">
		            	<div type="checkcolumn"></div>
		                <div type="indexcolumn" headerAlign="center" align="center" >序列</div>
		                <div field="id" header="节点ID" headerAlign="center" align="center" width="50px"></div>
		                <div field="name" header="节点名称" headerAlign="center"></div>
		                <div field="type" header="节点类型" headerAlign="center" align="center" width="50px"></div>
		            </div>
	        	</div> 
        	</div>
        </div>
        
    </div>
</div>   

<script type="text/javascript">
mini.parse();
//获取当前tab
var currTab = top["win"].tabs.getActiveTab();
var params = currTab.params;
var row=params.selectData;
var datagrid1 = mini.get("datagrid1");
var datagrid2 = mini.get("datagrid2");
var url=window.location.search;
var action=CommonUtil.getParam(url,"action");

datagrid2.on("beforeload", function (e) {
	e.cancel = true;
	var pageIndex = e.data.pageIndex; 
	var pageSize = e.data.pageSize;
	search(pageSize,pageIndex);
}); 

$(document).ready(function(){
	init();
   
});

function init(){
	var data = {};
	data = {treeId:row.treeId};
	var params=mini.encode(data);
	CommonUtil.ajax({
        url:"/AttachNodeController/searchAttachNodeRelationList",
        data:params,
        callback:function(data){
        	datagrid1.setData(data.obj);
        	var filterList=new Array();
        	$.each(data.obj, function(i, n){
    			filterList.push("'"+n.attachId+"'");
    		 });
        	mini.get("filterList").setValue(filterList.toString());
        	CommonUtil.ajax({
    			url:"/AttachTreeController/getTcAttachTreeNodeVo",
    			data:{filterList:filterList.toString()},
    			callback:function(data) {
    				datagrid2.setTotalCount(data.obj.total);
    				datagrid2.setPageIndex(0);
    				datagrid2.setPageSize(10);
    				datagrid2.setData(data.obj.rows);
    			}
    		});  
        }
    });
}






/* 查询 */
function search(pageSize,pageIndex){
	var form = new mini.Form("search_form");
	form.validate();
	if(form.isValid()==false){
		mini.alert("信息填写有误，请重新填写","系统提示");
		return;
	}

	var data=form.getData();
	data['pageNumber']=pageIndex+1;
	data['pageSize']=pageSize;
	var params = mini.encode(data);

	CommonUtil.ajax({
		url:"/AttachTreeController/getTcAttachTreeNodeVo",
		data:params,
		callback : function(data) {
			datagrid2.setTotalCount(data.obj.total);
			datagrid2.setPageIndex(pageIndex);
			datagrid2.setPageSize(pageSize);
			datagrid2.setData(data.obj.rows);
		}
	});
}

/* 按钮 查询事件 */
function query(){
	search(datagrid2.pageSize,0);
}



//清空
function clear(){
	var form=new mini.Form("search_form");
	form.clear();
	query();
}

//向右移动一个（datagrid1==》datagrid2）
function add() {
    var items = datagrid1.getSelecteds();
    
    if(items!=null&&items.length>0){
    	datagrid1.removeRows(items);
    	var array_id = new Array();
    	$.each(items, function(i, n) {
    		array_id.push("'"+items[i].attachId+"'");
    	});
    	CommonUtil.ajax({
			url:"/AttachNodeController/delAttachRelation",
			data:{treeId:row.treeId, 
				filterList:array_id.toString()
			},
			callback:function(data) {
				init();
			}
		});  
    }else{
        mini.alert("请选择一个已选节点");
    }
}


//向右移动全部（datagrid1==》datagrid2） 
function addAll() {
    var items = datagrid1.getData();
    datagrid1.removeRows(items);
    var array_id = new Array();
	$.each(items, function(i, n) {
		array_id.push("'"+items[i].attachId+"'");
	});
	 CommonUtil.ajax({
			url:"/AttachNodeController/delAttachRelation",
			data:{treeId:row.treeId, 
				filterList:array_id.toString()
			},
			callback:function(data) {
				init();
			}
	});  
}
//向左移动一个（datagrid1《==datagrid2） 
function removes() {
    var items = datagrid2.getSelecteds();
    if(items!=null && items.length>0){
    	datagrid2.removeRows(items);
    	var array_id = new Array();
		var attachType_arr = new Array();
		$.each(items, function(i, n) {
			array_id.push(items[i].id);
			attachType_arr.push(items[i].type);
		});
		CommonUtil.ajax({
				url:"/AttachNodeController/addAttachRelation",
				data:{treeId:row.treeId, 
					filterList:array_id.toString(),attachTypes:attachType_arr.toString()
				},
				callback:function(data) {
					init();
				}
		});  
        
    }else{
        mini.alert("请选择一个可选节点");
    }
    
}
//向左移动全部（datagrid1《==datagrid2） 
function removeAll() {
    var items = datagrid2.getData();
    datagrid2.removeRows(items);
    var array_id = new Array();
	var attachType_arr = new Array();
	$.each(items, function(i, n) {
		array_id.push(items[i].id);
		attachType_arr.push(items[i].type);
	});
	 CommonUtil.ajax({
			url:"/AttachNodeController/addAttachRelation",
			data:{treeId:row.treeId, 
				filterList:array_id.toString(),attachTypes:attachType_arr.toString()
			},
			callback:function(data) {
				init();
			}
	});  
    
}


</script>
</body>
</html>