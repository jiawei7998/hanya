<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>文档节点查询</legend>

<div>
    <div id="search_form" class="mini-form" >
		<input id="paramName" name="nodeName" class="mini-textbox" labelField="true"  label="节点名称："  width="330px"  labelStyle="text-align:right;width:100px"  emptyText="请输入节点名称" />
		<input id="paramId" name="nodeId" class="mini-textbox" labelField="true"  label="节点编号："    width="330px"  labelStyle="text-align:right;width:100px"  emptyText="请输入节点编号" />
			
		<span style="float: right; margin-right: 150px"> 
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		</span>
			
	</div>  
</div>
</fieldset> 
<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
</span> 
<div class="mini-fit" >
	<div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>

			<div field="nodeName" width="200px" align="center"  headerAlign="center" >节点名称</div>    
			<div field="nodeMustFlag" width="60"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否必须</div>
			<div field="nodeAttsize" width="100" align="center" headerAlign="center"  >文档大小</div>
			<div field="nodeAtttype" width="100px" align="center"  headerAlign="center" >文档类型</div>
			<div field="nodeAttmark" width="200px" align="center"  headerAlign="center" >文档注释</div>  
		</div>
	</div>
</div>   

<script type="text/javascript">
mini.parse();
var url = window.location.search;
var form = new mini.Form("#search_form");
var grid=mini.get("data_grid");
var row="";

grid.on("beforeload", function (e) {
	e.cancel = true;
	var pageIndex = e.data.pageIndex; 
	var pageSize = e.data.pageSize;
	search(pageSize,pageIndex);
});


//查看详情
function onRowDblClick(e) {
	var url = CommonUtil.baseWebPath() +"/../Common/File/AttachNodeEdit.jsp?action=detail";
	var tab = {id: "AttachNodeDetail",name:"AttachNodeDetail",url:url,title:"文档节点详情",
				parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
	var paramData = {selectData:grid.getSelected()};
	CommonUtil.openNewMenuTab(tab,paramData);
}
//清空
function clear(){
	var form=new mini.Form("search_form");
	form.clear();
	query();
}

//添加
function add(){
	var url = CommonUtil.baseWebPath() +"/../Common/File/AttachNodeEdit.jsp?action=add";
	var tab = {id: "AttachNodeAdd",name:"AttachNodeAdd",url:url,title:"文档节点新增",
				parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
	var paramData = {selectData:""};
	CommonUtil.openNewMenuTab(tab,paramData);
}
//修改
function edit(){
	var row=grid.getSelected();
	if(row){
		var url = CommonUtil.baseWebPath() +"/../Common/File/AttachNodeEdit.jsp?action=edit";
		var tab = {id: "AttachNodeEdit",name:"AttachNodeEdit",url:url,title:"文档节点修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
	}else{
		mini.alert("请选择一条记录","提示");
	}
}
//删除
function del(){
	var rows=grid.getSelecteds();
	if(rows.length==0){
		mini.alert("请选中一行","提示");
		return;
	}
	mini.confirm("您确认要删除该文档节点信息?","系统警告",function(value){   
		if (value=='ok'){   
			var data=rows[0];
			params=mini.encode(data);
			CommonUtil.ajax( {
				url:"/AttachNodeController/deleteAttachNode",
				data:params,
				callback : function(data) {
					mini.alert("删除成功.","系统提示");
					search(10,0);
				}
			});
		}
	});
}




/* 按钮 查询事件 */
function query(){
	search(grid.pageSize,0);
}

/* 查询 */
function search(pageSize,pageIndex){
	form.validate();
	if(form.isValid()==false){
		mini.alert("信息填写有误，请重新填写","系统提示");
		return;
	}

	var data=form.getData(true);
	data['pageNumber']=pageIndex+1;
	data['pageSize']=pageSize;
	var params = mini.encode(data);

	CommonUtil.ajax({
		url:"/AttachNodeController/searchAttachNode",
		data:params,
		callback : function(data) {
			grid.setTotalCount(data.obj.total);
			grid.setPageIndex(pageIndex);
	        grid.setPageSize(pageSize);
			grid.setData(data.obj.rows);
		}
	});
}

$(document).ready(function() {
	search(10, 0);
});
</script>
</body>
</html>