<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>附件树查询</legend>

<div>
    <div id="search_form" class="mini-form" >
		<input id="treeId" name="treeId" class="mini-textbox" labelField="true"  label="附件树编号："  width="300px"  labelStyle="text-align:right;width:100px"  emptyText="请输入节点名称" />
		<input id="treeName" name="treeName" class="mini-textbox" labelField="true"  label="附件树名称："    width="300px"  labelStyle="text-align:right;width:100px"  emptyText="请输入节点编号" />
		<input id="refType" name="refType" class="mini-combobox"  data="CommonUtil.serverData.dictionary.refType" width="300px" emptyText="请选择关联类型" labelField="true"  label="关联类型：" labelStyle="text-align:right;width:100px"/>	
		<span style="float: right; margin-right: 150px"> 
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		</span>
			
	</div>  
</div>
</fieldset> 
<span style="margin: 2px; display: block;"> 
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">新增</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>
</span> 
<div class="mini-fit" >
	<div id="data_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"  allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true" multiSelect="true">
		<div property="columns">
			<div type="indexcolumn" width="50px" headerAlign="center">序号</div>
			<div type="checkcolumn"></div>
			<div field="treeId" width="120px" align="center"  headerAlign="center" >附件树编号</div>  
			<div field="treeName" width="200px" align="center"  headerAlign="center" >附件树名称</div> 
			<div field="refType" width="60"  headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'refType'}">关联类型</div>
			<div field="treeDesc" width="200px" align="center"  headerAlign="center" >附件树描述</div> 
			<div field="createUserName" width="150px" align="center"  headerAlign="center" >创建用户</div> 
			<div field="createInstName" width="200px" align="center"  headerAlign="center" >创建机构</div> 
			<div field="createTime" width="150" align="center" headerAlign="center"  >创建时间</div>
			<div field="updateUserName" width="100px" align="center"  headerAlign="center" >更新用户</div>
			<div field="updateInstName" width="200px" align="center"  headerAlign="center" >更新机构</div>
			<div field="updateTime" width="150px" align="center"  headerAlign="center" >更新时间</div>
		</div>
	</div>
</div>   

<script type="text/javascript">
mini.parse();
var url = window.location.search;
var form = new mini.Form("#search_form");
var grid=mini.get("data_grid");
var row="";

grid.on("beforeload", function (e) {
	e.cancel = true;
	var pageIndex = e.data.pageIndex; 
	var pageSize = e.data.pageSize;
	search(pageSize,pageIndex);
}); 

//清空
function clear(){
	var form=new mini.Form("search_form");
	form.clear();
	query();
}

//添加
function add(){
	var url = CommonUtil.baseWebPath() +"/../Common/File/AttachTreeEdit.jsp?action=add";
	var tab = {id: "AttachTreeAdd",name:"AttachTreeAdd",url:url,title:"附件树新增",
				parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
	var paramData = {selectData:""};
	CommonUtil.openNewMenuTab(tab,paramData);
}
//修改
function edit(){
	var row=grid.getSelected();
	if(row){
		var url = CommonUtil.baseWebPath() +"/../Common/File/AttachTreeRelation.jsp?action=edit";
		var tab = {id: "AttachTreeEdit",name:"AttachTreeEdit",url:url,title:"附件树修改",
					parentId:top["win"].tabs.getActiveTab().name,showCloseButton:true};
		var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
	}else{
		mini.alert("请选择一条记录","提示");
	}
}
//删除
function del(){
	var rows=grid.getSelecteds();
	if(rows.length==0){
		mini.alert("请选中一行","提示");
		return;
	}
	mini.confirm("您确认要删除该附件树信息?","系统警告",function(value){   
		if (value=='ok'){   
			var data = new Array();
			$.each(rows, function(i, n){
				data.push(n.treeId);
			});
			params=mini.encode(data);
			CommonUtil.ajax( {
				url:"/AttachTreeController/removeAttachTree",
				data:params,
				callback : function(data) {
					mini.alert("删除成功.","系统提示");
					search(10,0);
				}
			});
		}
	});
}




/* 按钮 查询事件 */
function query(){
	search(grid.pageSize,0);
}

/* 查询 */
function search(pageSize,pageIndex){
	form.validate();
	if(form.isValid()==false){
		mini.alert("信息填写有误，请重新填写","系统提示");
		return;
	}

	var data=form.getData(true);
	data['pageNumber']=pageIndex+1;
	data['pageSize']=pageSize;
	var params = mini.encode(data);

	CommonUtil.ajax({
		url:"/AttachTreeController/searchAttachTreeList",
		data:params,
		callback : function(data) {
			grid.setTotalCount(data.obj.total);
			grid.setPageIndex(pageIndex);
	        grid.setPageSize(pageSize);
			grid.setData(data.obj.rows);
		}
	});
}

$(document).ready(function() {
	search(10, 0);
});


function GetData() {
    var row = grid.getSelected();
    return row;
}

	//双击行选择
function onRowDblClick(){
    onOk();
}

function CloseWindow(action) {
    if (window.CloseOwnerWindow)
        return window.CloseOwnerWindow(action);
    else
        window.close();
}

function onOk() {
    CloseWindow("ok");
}

//关闭窗口
function onCancel() {
    CloseWindow("cancel");
}
</script>
</body>
</html>