<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>附件树  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">附件树</a> >> 新增</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    
                    <td style="width:50%">
                        <input id="treeId" name="treeId" class="mini-textbox" label="附件树编号"  labelField="true"   style="width:100%" labelStyle="text-align:left;width:170px"  required="true"  enabled="false"/>
                    </td>
                    <td style="width:50%">
                        <input id="treeName" name="treeName" class="mini-textbox"  label="附件树名称" emptyText="请输入附件树名称" labelField="true"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:12" required="true" />
                    </td>
                    
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="refType" name="refType" class="mini-combobox"  label="关联类型" emptyText="请选择关联类型" labelField="true"  data="CommonUtil.serverData.dictionary.refType" style="width:100%" labelStyle="text-align:left;width:170px" required="true" />
                    </td>
                    <td style="width:50%">
                        <input id="treeDesc" name="treeDesc" class="mini-textbox"  label="附件树描述" labelField="true"  emptyText="请输入附件树描述"  vtype="maxLength:8" style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
                </tr>
               
                
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
        	var data = {};
        	data = {refId:null, refType:null};
        	var params=mini.encode(data);
        	CommonUtil.ajax({
                url:"/AttachTreeController/searchAttachTree",
                data:params,
                callback:function(data){
                	mini.get("treeId").setValue(data.obj[0].treeId);
                }
		    });
           
        });

        
        //保存
         function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }
            var saveUrl = "/AttachTreeController/createAttachTree";
                var data=form.getData(true);
                
                var params=mini.encode(data);
                CommonUtil.ajax({
                    url:saveUrl,
                    data:params,
                    callback:function(data){
                    	if (data.code == 'error.common.0000') {
    						mini.alert("保存成功",'提示信息',function(){
    							top["win"].closeMenuTab();
    						});
    					} else {
    						mini.alert("保存失败");
    			    	}
                    }
    		    });
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        

    </script>
</body>
</html>