<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlow.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>附件树关联配置</legend>

<div>
    <div id="search_form" class="mini-form" >
		<input id="prtype" name="prtype" class="mini-combobox" value="1" labelField="true"  label="关联类型选择："  width="200px"  labelStyle="text-align:right;width:100px" 
		data="CommonUtil.serverData.dictionary.treeRefType" onvaluechanged="onchange"/>
		<input id="refType" class="mini-hidden" value="1"/>
		<input id="refName" class="mini-hidden"/>
		<input id="refId" class="mini-hidden"/>
		
		
		
		
		<input id="cno" name="cno" class="mini-buttonedit" onbuttonclick="onButtonEdit" labelField="true"  label="客户选择："    width="330px"  labelStyle="text-align:right;width:100px"/>
		
		<input id="supPrdTypeNo" name="supPrdTypeNo" class="mini-buttonedit"  labelField="true"  label="产品选择："    width="330px"  labelStyle="text-align:right;width:100px"/>
		
		<input id="treeId" name="treeId" class="mini-buttonedit" onbuttonclick="onButtonEdit2" labelField="true"  label="附件树选择："    width="330px"  labelStyle="text-align:right;width:100px" />
			
		<span style="float: right; margin-right: 150px"> 
			<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
			<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
		</span>
			
	</div>  
</div>
</fieldset> 


<div class="mini-fit" >
	<fieldset class="mini-fieldset">
	<legend>对应附件树信息</legend>

	<div>
	     <ul id="tree1" class="mini-tree" style="width:300px;padding:5px;" 
                    showTreeIcon="true" textField="text" idField="id" value="base" expandOnNodeClick="true">        
         </ul>
	</div>
	</fieldset> 
</div>   

<script type="text/javascript">
mini.parse();
var url = window.location.search;
var form = new mini.Form("#search_form");
var row="";



//清空
function clear(){
	var form=new mini.Form("search_form");
	form.clear();
	query();
}





/* 按钮 查询事件 */
function query(){
}

/* 查询 */
function search(pageSize,pageIndex){
	form.validate();
	if(form.isValid()==false){
		mini.alert("信息填写有误，请重新填写","系统提示");
		return;
	}

	var data=form.getData(true);
	data['pageNumber']=pageIndex+1;
	data['pageSize']=pageSize;
	var params = mini.encode(data);

	CommonUtil.ajax({
		url:"/AttachNodeController/searchAttachNode",
		data:params,
		callback : function(data) {
			grid.setTotalCount(data.obj.total);
			grid.setPageIndex(pageIndex);
	        grid.setPageSize(pageSize);
			grid.setData(data.obj.rows);
		}
	});
}

$(document).ready(function() {
	mini.get("cno").setVisible(true);
	mini.get("supPrdTypeNo").setVisible(false);
	search(10, 0);
});

function onchange(e){
	if(e.value == '1'){//选择客户
		mini.get("refType").setValue("1");
		mini.get("cno").setVisible(true);
		mini.get("supPrdTypeNo").setVisible(false);
		
	}
	if(e.value == '2'){//选择产品
		mini.get("refType").setValue("2");
		mini.get("supPrdTypeNo").setVisible(true);
		mini.get("cno").setVisible(false);
	}	
}







function onButtonEdit(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
        title: "选择对手方列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if (data) {
                    btnEdit.setValue(data.cno);
                    btnEdit.setText(data.cfn);
                    btnEdit.focus();
                }
            }

        }
    });
}

function onButtonEdit2(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/File/AttachTreeMini.jsp",
        title: "选择附件树列表",
        width: 700,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if (data) {
                    btnEdit.setValue(data.treeId);
                    btnEdit.setText(data.treeName);
                    btnEdit.focus();
                }
            }

        }
    });
}

</script>
</body>
</html>