<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
    <head>
        <title>opics交易要素管理</title>
        <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    </head>
<body style="width:99%;height:99%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>管理要素查询-选择</legend>
    <div id="search_form" style="width: 99%;">
       <input id="groupMod" name="groupMod" class="mini-textbox"   labelField="true" label="模块：" labelStyle="text-align:right;" emptyText="请输入模块"/> 
        <input id="groupName" name="groupName" class="mini-textbox" labelField="true" label="组合名称：" labelStyle="text-align:right;" emptyText="请输入组合名称" />
        <input id="product" name="product" class="mini-textbox" labelField="true" label="产品代码：" labelStyle="text-align:right;" emptyText="请输入产品代码" />
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search"  onclick="search()">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    
    <div class="mini-fit" style="width:99%;height:99%;">
        <div id="tradeParam_grid" class="mini-datagrid borderAll" style="width:99%;height:99%;" 
            sortMode="client" allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" 
            border="true" allowResize="true" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center" width="40">序列</div>
                <!-- <div field="groupId" headerAlign="center"  align="center" width="180px" >分组ID</div>-->
                <div field="groupMod" headerAlign="center"  width="120px">模块</div>
                <div field="groupName" headerAlign="center" width="120">组合名称</div>
                <div field="port" headerAlign="center"  width="120">投资组合</div>
                <div field="product" headerAlign="center" width="120">产品代码</div>
                <div field="prodType" headerAlign="center" width="120" >产品类型</div>
                <div field="cost" headerAlign="center" width="120">成本中心</div>
                <div field="trader" headerAlign="center"  width="120">交易员</div>
                <!-- <div field="note" headerAlign="center"  width="200px">备注</div>
                <div field="inputTime" headerAlign="center" align="center" width="160px">录入时间</div>
                <div field="lastTime" headerAlign="center"  align="center" width="160px">最后修改时间</div> -->
            </div>
        </div>
    </div>
</body>
<script>
    mini.parse();

    var grid = mini.get("tradeParam_grid");
    grid.on("beforeload", function(e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        searchs(pageSize, pageIndex);
    });

    /* $(document).ready(function() {
		search();
	}); */

    //查询按钮
    function search(){
        searchs(grid.pageSize,0);
    }

    function searchs(pageSize, pageIndex) {
        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("表单填写错误,请确认!", "提示信息");
            return;
        }

        var data = form.getData();
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['branchId']=branchId;

        var params = mini.encode(data);

        CommonUtil.ajax({
            url : "/IfsTradeParamController/searchPageTradeParam",
            data : params,
            callback : function(data) {
                var grid = mini.get("tradeParam_grid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空
    function clear(){
    	mini.get("groupName").setValue("");
        search();
    }

    function GetData() {
        var grid = mini.get("tradeParam_grid");
        var row = grid.getSelected();
        return row;
    }
    function SetData(data1){
    	data = mini.clone(data1);
    	mini.get("groupMod").setValue(data.prdCode);
    	search();
    	
    }
    
    //双击行选择
    function onRowDblClick(){
        onOk();
    }

    function CloseWindow(action) {
        if (window.CloseOwnerWindow)
            return window.CloseOwnerWindow(action);
        else
            window.close();
    }

    function onOk() {
        CloseWindow("ok");
    }

    //关闭窗口
    function onCancel() {
        CloseWindow("cancel");
    }
    

    
    


   




</script>
</html>