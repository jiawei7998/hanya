<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body style="width:100%;height:100%;background:white">
	<div id="RoleSearch" style="width: 70%">
		<fieldset class="mini-fieldset title">
			<legend>角色信息查询</legend>
			<div id="search_form">
					<input id="instId" name="instId" class="mini-buttonedit" width="320px" allowInput='false' onbuttonclick="onInsQuery" labelField="true" labelStyle="text-align:right;"  label="所属机构："  emptyText="请选择机构" />
					<input id="roleName" name="roleName" labelField="true" labelStyle="text-align:right;"  label="角色名称：" emptyText="请输入角色名称" class="mini-textbox" width="320px" vtype="maxLength:50" />
					<span style="float:right;margin-right: 150px">
						<a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
						<a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
					</span>
			</div>
			<span style="margin:2px;display: block;">
			<a id="add_btn" class="mini-button" style="display: none"  onclick="add()">新增</a>
			<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
<%--			<a id="delete_btn" class="mini-button" style="display: none"    onclick="removeObj()">删除</a>--%>
			<a id="access_btn" class="mini-button" style="display: none"   onclick="AccessConfiguration()">权限配置</a>
		</span>
		</fieldset>
	</div>
	<div class="mini-fit">
		<div id="RoleManageDatagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" allowAlternating="true"
			idField="id" pageSize="10" multiSelect="false" sortMode="client">
			<div property="columns">
				<div type="indexcolumn" field="deskId" width="10px">序号</div>
				<div field="roleId" headerAlign="center" allowSort="true" width="20px" >角色代码</div>
				<div field="roleName" headerAlign="center" allowSort="true" width="50px">角色名称</div>
				<div field="isActive" headerAlign="center" allowSort="true" align="center"  renderer="onYesOrNoRenderer" width="20px">启用状态</div>
				<div field="roleLevel" visible="false" headerAlign="center" allowSort="true" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'roleLevel'}"  width="30px">角色级别</div>
				<div field="instName" headerAlign="center" allowSort="true">所属机构</div>
				<div field="branchId" headerAlign="center" visible="false" allowSort="true">银行号</div>
			</div>
		</div>
	</div>
</body>
<script>
mini.parse();

var grid = mini.get("RoleManageDatagrid");
var inst =mini.get("instId");
$(document).ready(function(){
	//控制按钮显示
	$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
		grid.on("beforeload", function (e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		search(10, 0);
	});
})
//获取机构
function onInsQuery(e) {
	var btnEdit = this;
	mini.open({
		url: CommonUtil.baseWebPath() + "/../Common/MiniInstitutionSelectManages.jsp",
		title: "机构选择",
		width: 700,
		height: 600,
		ondestroy: function (action) {
			if (action == "ok") {
				var iframe = this.getIFrameEl();
				var data = iframe.contentWindow.GetData();
				data = mini.clone(data); //必须
				if (data) {
					btnEdit.setValue(data.instId);
					btnEdit.setText(data.instName);
					//btnEdit.setEnabled(false);
					search(10,0);
				}
			}
		}
	});
}
//删除角色
function removeObj() {
	var record = grid.getSelected();
	var param = {};
	if (record.roleId) {
		param.roleId = record.roleId;
		param.branchId=branchId;
		var roleId = mini.encode(param);
		mini.confirm("会删除该角色绑定关系--确认删除？", "警告！", function (action) {
			if (action == 'ok') {
				CommonUtil.ajax({
					url: "/RoleController/deleteRole",
					data: roleId,
					callback: function (data) {
						if (data.code == 'error.common.0000') {
							mini.alert("操作成功","温馨提示",function(){
								search(10, 0);
							});
						} else {
							mini.alert("操作失败");
						}
					}
				});
			}
		})
	} else {
		mini.alert("请选中一条记录");
	}
}
function clear() {
    var form = new mini.Form("search_form");
    form.clear();
	mini.get("instId").setEnabled(true);
}
top["roleManage"]=window;
function getData(){
	var row = grid.getSelected();
	if(row){
		return row;
	}
	return null;
}
//===修改===
function edit() {
	var row = grid.getSelected();
	if (row) {
		var url = CommonUtil.baseWebPath() + "/../Common/MiniRoleEdits.jsp?action=edit";
		var tab = { id: "RoleEdit", name: "RoleEdit", text: "角色修改", url: url ,parentId:top["win"].tabs.getActiveTab().name};
		top['win'].showTab(tab);
	} else {
		mini.alert("请选中一条记录");
	}
}
//机构字典
var InstitutionType = [{ "id": "2", "text": "支行" }, { "id": "1", "text": "分行" }, { "id": "0", "text": "总行" }, { "id": "9", "text": "部门" }];
//渲染机构字段
function onInstitutionTypeRenderer(e) {
	for (var i = 0, l = InstitutionType.length; i < l; i++) {
		var g = InstitutionType[i];
		if (g.id == e.value) return g.text;
	}
	return "";
}
//启用状态字典
var YesOrNo = [{ "id": "1", "text": "是" }, { "id": "0", "text": "否" }];
//渲染启用状态
function onYesOrNoRenderer(e) {
	for (var i = 0, l = YesOrNo.length; i < l; i++) {
		var g = YesOrNo[i];
		if (g.id == e.value) return g.text;
	}
	return "";
}

//跳转到权限配置界面
function AccessConfiguration(){
	// if(inst.getValue()!='' && inst.getText()!=''){
		var record = grid.getSelected();
		if(record){
			var url = CommonUtil.baseWebPath() + "/../Common/MiniConfigurePermissions.jsp?roleId=" + record.roleId + "&roleName=" + encodeURI(encodeURI(record.roleName, "utf-8")) + "&instName=" + encodeURI(encodeURI(inst.getText(), "utf-8")) + "&instId=" + inst.getValue();
			var tab = { id: "ShowMiniConfigurePermissions", name: "ShowMiniConfigurePermissions", text: "权限配置", url: url };
			top['win'].showTab(tab);
			mini.get("instId").setEnabled(true);
		}else{
			mini.alert("请选择角色分配");
		}
	// }else{
	// 	mini.alert("请根据机构查询之后再分配");
	// }
}
//新增角色
function add() {
		var url = CommonUtil.baseWebPath() + "/../Common/MiniRoleEdits.jsp";
		var tab = { id: "RoleAdd", name: "RoleAdd", text: "角色新增", url: url ,parentId:top["win"].tabs.getActiveTab().name};
		top['win'].showTab(tab);
	}
//搜索角色列表  根据银行Id 查询角色
function search(pageSize, pageIndex) {
	var form = new mini.Form("RoleSearch");
	var data = form.getData();
	data.pageSize=pageSize;
	data.pageNumber=pageIndex + 1;
	data.branchId=branchId;
	var param = mini.encode(data); //序列化成JSON
	CommonUtil.ajax({
		url: "/RoleController/pageAllRoleBranchId",
		data:param,
		callback: function (data) {
			grid.setTotalCount(data.obj.total);
			grid.setPageIndex(pageIndex);
			grid.setPageSize(pageSize);
			grid.setData(data.obj.rows);
		}
	});
}
</script>
