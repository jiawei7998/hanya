<%@ page language="java" pageEncoding="UTF-8"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<%@ include file="../../global.jsp"%>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>菜单列表信息查询</legend>
    <div>
	    <div id="MenuSearch" style="width:70%">
	        <input id="deskId" name="deskId" class="mini-textbox" width="320px" labelField="true" label="工作台Id：" labelStyle="text-align:right;" emptyText="请输入工作台Id" vtype="maxLength:7"/>
	        <input id="deskName" name="deskName" labelField="true" label="工作台名称：" class="mini-textbox" width="320px" labelStyle="text-align:right;" emptyText="请输入工作台名称"   vtype="maxLength:10"/>
	        <span style="float: right; margin-right: 50px">
	            <a class="mini-button" style="display: none"  id="search_btn"  onclick="deskMenu(10,0)">查询</a>
	            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
	        </span>
	    </div>
    </div>
    <span style="margin:2px;display: block;">
		    <a class="mini-button" style="display: none"  id="add_btn"  onclick="add()">新增</a>
		    <a class="mini-button" style="display: none"  id="edit_btn"  onclick="edit()">修改</a>
		    <a class="mini-button" style="display: none"  id="update_btn"   onclick="update()">刷新</a>
		    <a class="mini-button" style="display: none"  id="add_func"   onclick="addFunc()">新增功能点</a>
		    <a class="mini-button" style="display: none"  id="edit_func"   onclick="updateFunc()">修改功能点</a>
		    <a class="mini-button" style="display: none"  id="save_memu_relation"   onclick="savaMenuBinding()">保存菜单绑定</a>
		</span>
</fieldset>
<div class="mini-fit">
<div class="mini-splitter" handlerSize="2px" style="width:100%;height:100%;">
    <div size="30%" showCollapseButton="true">
        <!-- <div class="mini-fit" style="width:100%;height:100%;" > -->
            <div class="mini-panel" title="菜单列表" style="width:100%;height:100%;" showToolbar="true" showCloseButton="false" showFooter="true">
                <div class="mini-fit" style="width:100%;height:100%;" >
                <div id="MemuGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" onrowclick="showRoleResouce"
                    idField="deskId" pageSize="10" multiSelect="false" sortMode="client" allowAlternating="true">
                    <div property="columns">
                    	<div type="checkcolumn"></div>
                        <div type="indexcolumn" headerAlign="center" field="deskId" width="40px">序号</div>
                        <div field="deskId" headerAlign="center" allowSort="true" width="80px">工作台Id</div>
                        <div field="deskName" headerAlign="center" allowSort="true" width="100px">工作台名称</div>
                        <div field="branchId" headerAlign="center" visible="false" allowSort="true">银行归属</div>
                        <div field="deskUrl" headerAlign="center" visible="false" allowSort="true">URL地址</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div showCollapseButton="true">
            <div class="mini-fit" style="width:100%;height:100%;">
                <div class="mini-panel" title="菜单资源" style="width:50%;height:100%;float:left;" showToolbar="true" showCloseButton="false"
                    showFooter="false">
            
                    <div id="treeGridMenu" class="mini-treegrid"   expandOnLoad="true" showCheckBox="true" autoCheckParent="true" treeColumn="text"
                        idField="id" parentField="pid" resultAsTree="false" allowResize="1" showTreeIcon="true" allowSelect="true" onnodeclick="showFuncs()"
                        allowAlternating="true" checkRecursive="true">
                        <div property="columns">
                            <div type="indexcolumn" width="12px"></div>
                            <div field="deskId" name="deskId" headerAlign="center" align="center" width="30px">归属工作台</div>
                            <div name="text" field="text" headerAlign="center">模块名称</div>
                        </div>
                    </div>
                </div>
                <div class="mini-panel" title="功能点列表 &nbsp;&nbsp;&nbsp;&nbsp;【出现在列表中的代表拥有该按钮权限，请通过是否可用进行控制】" style="width:50%;height:100%;"
                     showToolbar="true" showCloseButton="false" showFooter="true">
                    <div id="functionGrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" allowResize="true" idField="functionId"
                        showPager="true" pageSize="10" multiSelect="false" allowAlternating="true">
                        <div property="columns">
                            <div type="checkcolumn" field="deskId"></div>
                            <div field="moduleId" headerAlign="center" allowSort="true" width="50px">模块Id</div>
                            <div field="functionId" headerAlign="center" allowSort="true" width="60px">功能点Id</div>
                            <div field="functionName" headerAlign="center" allowSort="true" width="100px">功能点名称</div>
                            <div field="functionUrl" headerAlign="center" allowSort="true">功能点url</div>
                            <div field="isActive" headerAlign="center" allowSort="true" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}">是否启用</div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
</body>
<script>
  mini.parse();

  var grid = mini.get("MemuGrid");
  var func =mini.get("functionGrid");
  var treeValue=mini.get("treeGridMenu");
  grid.on("beforeload", function (e) {
      e.cancel = true;
      var pageIndex = e.data.pageIndex;
      var pageSize = e.data.pageSize;
      deskMenu(pageSize, pageIndex);
  });
func.on("beforeload", function (e) {
    e.cancel = true;
    var pageIndex = e.data.pageIndex;
    var pageSize = e.data.pageSize;
    functionHaving(pageSize, pageIndex);
}); 
  $(document).ready(function(){
      //控制按钮显示
      $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
          deskMenu(10, 0);
          loadResourceTree();
      });
  })
  
  
  //增加search方法，用于自动刷新
  function search(){
	  deskMenu(10, 0);
      loadResourceTree();  
  }
  
  
  
  function update(){
    deskMenu(20, 0);
    loadResourceTree();
  }
  function showFuncs(){
    functionHaving(10,0);
  }
//绑定模块与功能点
function saveModuleFucn(){
    var param={};
    var funcList="";
    var treeValueSelect=treeValue.getSelected();
    if(treeValueSelect){
       param.moduleId=treeValueSelect.id;
       var functionSelects=func.getSelecteds();//对象
       if(functionSelects !=null && functionSelects.length>0){
           for(var i=0;i<functionSelects.length;i++){
            funcList=funcList+functionSelects[i].functionId+",";
              
           }
           param.functionId=funcList;
          
           mini.confirm("你确定要绑定吗？","温馨提示",function(r){
               if(r=='ok'){
                CommonUtil.ajax({
                    url: "/ModuleController/saveModuleFunction",
                    data: mini.encode(param),
                    callback: function (data) {
                        if (data.code == 'error.common.0000') {
                            mini.alert("操作成功","系统提示",function(){
                                loadResourceTree();
                                func.setData();
                            });
                        } else {
                            mini.alert("操作失败","系统提示");
                        }

                    }
                });


               }
           });

       }else{
           mini.alert("请选择要绑定的功能点","温馨提示");
       }
       

    }else{
        mini.alert("请选择要绑定的模块","温馨提示");
    }
}
//判断字符串str中出现了几次‘-’
function checkStr(str){
	
	var str1=str.replace(/-/g,""); //将字符串中'-'替换为空,创建新的字符串
	var count = str.length-str1.length;
	return count;
	
}

        
//菜单的绑定保存
function savaMenuBinding(){
	
	
    var record =grid.getSelected();
    if(record !=null){
    	
    	
        param ={};
        param.deskId =record.deskId;
        var str =treeValue.getValue(true);
        
        var firstList = new Array();
       
        if(str!=null && str!=''){
        	var pnode="";//父节点
        	var strArray=str.split(",");//父节点、子节点数组
        	for(var i=0;i<strArray.length;i++){
        		if(checkStr(strArray[i])==1){//'-'出现一次，说明是父节点
        			pnode=strArray[i];
        		}
        		
            } 
			var node = treeValue.getNode(pnode);
            var childs=node.children;
            for(var i=0;i<childs.length;i++){
            	firstList.push(childs[i].id);
            }
            firstList.push(pnode);
            param.pnode=pnode;
             
            /* for(var i=0;i<strArray.length;i++){
                if(strArray[i].length==3 || strArray[i].length==4){
                    firstList.push(strArray[i]);
                }
            }  */
        }else{
        	mini.alert("请选择要绑定的菜单资源!","系统提示");
        	return;
        }
        
        param.moduleList=firstList;
        param.deskId=record.deskId;
        param.branchId=branchId;
        
        mini.confirm("您确定要分配吗？", "温馨提示", function (action) {
            if (action == 'ok') {
                CommonUtil.ajax({
                    url: "/ModuleController/updateModuleDesk",
                    data: mini.encode(param),
                    callback: function (data) {
                        if (data.code == 'error.common.0000') {
                            mini.alert("操作成功","系统提示",function(){
                                loadResourceTree();
                            });
                        } else {
                            mini.alert("操作失败","系统提示");
                        }

                    }
                });
            }
        });
        
    }else{
        mini.alert("请选择一个菜单进行绑定");
    }
}
            //双击列表显示该列表的资源
function showRoleResouce() {
    
    
    var record = grid.getSelected();
    treeValue.setValue();
    //台子Id  银行Id
    var paramObj = {};
    paramObj.deskId = record.deskId;
    paramObj.branchId =branchId;
    var param = mini.encode(paramObj);
    CommonUtil.ajax({
        url: "/RoleController/searchModuleDetailByAndDeskIdAndBranchId",
        data: param,
        callback: function (data) {
            treeValue.setValue(data);
        }
    });
}
        
//默认加载资源树
function loadResourceTree() {
    var data = {};
    data.branchId = branchId;
    var param = mini.encode(data);
    CommonUtil.ajax({
        url: "/RoleController/getAllResourceByBranchId",
        data: param,
        callback: function (data) {
            treeValue.setData(data.obj);
        }
    });
}
//根据银行Id 刷出改银行下所有的菜单列表
function deskMenu(pageSize, pageIndex) {
    var memuObj = {};
    var form = new mini.Form("MenuSearch");
    form.validate();
    if (form.isValid() == false) {
        return;
    }
    var deskName =mini.get("deskName");
    memuObj.deskName =deskName.getValue();
    var deskId=mini.get("deskId");
    memuObj.deskId=deskId.getValue();
    memuObj.branchId = branchId;
    memuObj.pageSize = pageSize;
    memuObj.pageNumber = pageIndex + 1;
    if (branchId) {
        CommonUtil.ajax({
            url: "/deskMenuController/getAllMenuByBranchId",
            data: mini.encode(memuObj),
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });

    }
}    
function onBeforeNodeCheck(e) {
    var tree = e.sender;
    var node = e.node;
    if (tree.hasChildren(node)) {
    }
}
            //新增菜单
function add(){
    var url = CommonUtil.baseWebPath() + "/../Common/system/MiniMenuEdit.jsp";
    var tab = { id: "MenuAdd", name: "MenuAdd", text: "菜单新增", url: url,parentId: top["win"].tabs.getActiveTab().name  };
    top['win'].showTab(tab);
}
            //修改数据
function edit(){
    var record=grid.getSelected();
    var node=treeValue.getSelectedNode ();
    if(record!=null && node!=null){
        mini.alert("【菜单列表】、【资源资源】、【功能点列表】只能选一个修改");
    }else{
        if(record!=null && record!=''){
            //修改菜单台子
            var url = CommonUtil.baseWebPath() + "/../Common/system/MiniMenuEdit.jsp?deskId="+record.deskId;
            var tab = { id: "MenuEdit", name: "MenuEdit", text: "菜单修改", url: url,parentId:top["win"].tabs.getActiveTab().name };
            top['win'].showTab(tab);
        }else if(node!=null && node!=''){
            //修改菜单资源
            var url = CommonUtil.baseWebPath() + "/../Common/system/MiniMenuEdit.jsp?moduleId="+node.id;
            var tab = { id: "MenuEdit", name: "MenuEdit", text: "资源修改", url: url ,parentId:top["win"].tabs.getActiveTab().name};
            top['win'].showTab(tab);

        }else{
            mini.alert("请选择修改内容");
        }
    }
}
            //查询该节点所拥有的功能点
function functionHaving(pageSize,pageIndex){
    var node =treeValue.getSelectedNode();
    if(node){
        func.setData();
        var funcData = {};
        funcData.pageSize = pageSize;
        funcData.pageNumber = pageIndex + 1;
        funcData.moduleId = node.id;
        funcData.branchId = branchId;
        CommonUtil.ajax({
            url: "/RoleController/getFunctionByBranchId",
            data: mini.encode(funcData),
            callback: function (data) {
                var grid = mini.get("functionGrid");
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
                //渲染已经拥有的功能点绑定关系
                showModuleFunctions();
            }
        });
    }else{
        mini.alert("请选择模块节点查询","系统提示");
    }
    
}
//显示已经有的功能点
function showModuleFunctions() {
    var node = treeValue.getSelectedNode();
    var param = {};
    param.functionId = node.id;
    if (param) {
        CommonUtil.ajax({
            url: "/ModuleController/getHavingFuncId",
            data: mini.encode({ "moduleId": node.id }),
            callback: function (data) {
                func.selects(data.obj, true);
            }
        });
    }
}
            //新增功能点
function addFunc(){
    var node =treeValue.getSelectedNode();
    if(node){
        var url = CommonUtil.baseWebPath() + "/../Common/system/MiniAddFunc.jsp?action=add&moduleId="+node.id;
        var tab = { id: "funcAdd", name: "funcAdd", text: "功能点新增", url: url, parentId: top["win"].tabs.getActiveTab().name };
        top['win'].showTab(tab);
        functionHaving(10,0);
    }else{
        mini.alert("请选择【资源菜单】作为功能点的父级","温馨提示")
    }
}

  function updateFunc(){
      var node =func.getSelected();
      if(node){
          var url = CommonUtil.baseWebPath() + "/../Common/system/MiniAddFunc.jsp?action=edit&functionId="+node.functionId+"&moduleId="+node.moduleId;
          var tab = { id: "funcEdit", name: "funcEdit", text: "功能点修改", url: url, parentId: top["win"].tabs.getActiveTab().name };
          top['win'].showTab(tab);
          functionHaving(10,0);
      }else{
          mini.alert("请选择功能点的父级","温馨提示")
      }
  }

function clear() {
    var form = new mini.Form("#MenuSearch");
    form.clear();
    deskMenu(10, 0);
}         
</script>
 
