<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
    <legend>用户列表</legend>
    <div id="InstRoleManageGrid">
        <input id="adminUserId" name="adminUserId" type="hidden" value="<%=__sessionUser.getUserId() %>" />
        <input id="instId" name="instId" class="mini-textbox" labelField="true" label="机构Id：" enabled="false" labelStyle="text-align:right;"   />
        <input id="instName" name="instName" class="mini-textbox" labelField="true" label="机构名称：" enabled="false" labelStyle="text-align:right;"  />
    </div>
</fieldset>
<span style="margin:2px;display: block;">
    <a class="mini-button"  id="save_btn" onclick="save()">保存</a>
</span>
<div class="mini-fit" style="width:100%; " title="角色列表" >
    <div class="mini-datagrid borderAll" style="width: 100%; height: 48%;" onselectionchanged="onSelectionChangedSearchRole" id="InstRoleManageGridColum" allowResize="true"
         pageSize="10" allowAlternating="true"  sortMode="client" >
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" width="10px">序列</div>
            <div field="instId" headerAlign="center" allowSort="true"  width="30px">机构号</div>
            <div field="instName" headerAlign="center" allowSort="true"  width="30px">机构名称</div>
        </div>
    </div>
    <div style="width:100%;height:50%;" title="角色列表" >
        <div class="mini-panel noneflowpanel" title="未分配角色列表" style="width:47%;height:100%;float: left;" allowResize="true" collapseOnTitleClick="false">
            <div id="listbox1" class="mini-listbox" title="全部"  textField="roleName" valueField="roleId"   showCheckBox="true" multiSelect="true">
                <div property="columns">
                    <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                    <div field="roleId" header="角色编号" headerAlign="center" ></div>
                    <div field="roleName" header="角色名称" headerAlign="center" ></div>
                    <div field="roleFlag" header="角色类别" renderer="onRoleType" headerAlign="center" align="center"></div>
                    <div field="isActive" renderer="onYesOrNoRenderer" header="启用状态" headerAlign="center" align="center"></div>
                    <div field="roleLevel" renderer="onroleLevelRenderer" header="角色级别" headerAlign="center" align="center"></div>
                </div>
            </div>
        </div>
        <div style="float: left;width: 5%;text-align:center; ">
            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button"   style="width: 40px;"  onclick="add">&gt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button"  style="width: 40px;" onclick="addAll">&gt;&gt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button"   style="width: 40px;" onclick="removeAll">&lt;&lt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button"   style="width: 40px;" onclick="removes">&lt;</a></div>
        </div>
        <div class="mini-panel noneflowpanel" title="已分配角色列表" style="width:47%;height:100%;float: left;" allowResize="true" collapseOnTitleClick="false">
            <div id="listbox2" class="mini-listbox"  textField="roleName" valueField="roleId" showCheckBox="true" multiSelect="true">
                <div property="columns">
                    <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                    <div field="roleId" header="角色编号" headerAlign="center"></div>
                    <div field="roleName" header="角色名称" headerAlign="center"></div>
                    <div field="roleFlag" header="角色类别"  renderer="onRoleType" headerAlign="center" align="center"></div>
                    <div field="isActive" renderer="onYesOrNoRenderer" header="启用状态" headerAlign="center" align="center"></div>
                    <div field="roleLevel" renderer="onroleLevelRenderer" header="角色级别" headerAlign="center" align="center" ></div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    var grid =mini.get("InstRoleManageGridColum");
    var listbox2 = mini.get("listbox2");
    var listbox1 = mini.get("listbox1");
    mini.get("instId").setValue(row.instId);
    mini.get("instName").setValue(row.instName);
    var data1 =[];
    data1.push({instId:row.instId,instName:row.instName})
    grid.setData(data1);

    $(document).ready(function(){
        //1、设置机构
    })

    //保存按钮
    function save() {
        var saveObj = {};
        var instRecord = grid.getSelected();
        if (instRecord) {
            saveObj.instId = instRecord.instId;
        } else {
            mini.alert("请选择用户进行分配权限","系统提示");
            return;
        }
        if (listbox2.getData().length != 0) {
            var recordList = listbox2.getData();
            var str = "";
            $.each(recordList, function (i, n) {
                str += n.roleId + ",";
            });
            saveObj.roleId = str;
        } else {
            // mini.alert("请选择一个权限","系统提示");return;
        }
        //发送保存操作
        var param = mini.encode(saveObj);
        mini.confirm("您确定要保存<span style='color:#FF3333;font-weight:bold;'>" + instRecord.instName + "</span>权限吗?", "系统提示", function (r) {
            if (r == 'ok') {
                CommonUtil.ajax({
                    url: '/RoleController/saveInstRole',
                    data: param,
                    callback: function (data) {
                        if ("error.common.0000" == data.code) {
                            mini.alert("操作成功","温馨提示",function(){
                                listbox1.setData();
                                listbox2.setData();
                            });
                        } else {
                            mini.alert("操作失败","系统提示");
                        }
                    }
                });
            }
        })
    }
    //点击用户列表加载角色
    function onSelectionChangedSearchRole(e) {
        listbox1.setData();
        listbox2.setData();
        var grid = e.sender;
        var record = grid.getSelected();
        var param={};
        param.adminUserId="<%=__sessionUser.getUserId() %>";//登录人的Id
        // param.userId=record.userId;//用户的ID
        param.instId=record.instId;//机构Id
        param.branchId=branchId;
        if (record) {
            //加载权限分配listBox
            CommonUtil.ajax({
                url: '/RoleController/searchAllRoleByInstID',
                data: mini.encode(param),
                callback: function (data) {
                    var InstRoleList = data.obj.right;
                    listbox1.setData(data.obj.left);
                    $.each(InstRoleList, function (i, n) {
                        listbox1.select(n.roleId);
                        var items = listbox1.getSelecteds();
                        if(items.length>0){
                            listbox1.removeItems(items);
                            listbox2.addItems(items);
                        }
                    });
                }
            });
        }
    }

    //向右移动一个（listbox1==》listbox2）
    function add() {
        var items = listbox1.getSelecteds();
        if(items!=null&&items.length>0){
            listbox1.removeItems(items);
            listbox2.addItems(items);
        }else{
            mini.alert("请选择一个角色分配");
        }
    }
    //向右移动全部（listbox1==》listbox2）
    function addAll() {
        var items = listbox1.getData();
        listbox1.removeItems(items);
        listbox2.addItems(items);
    }
    //向左移动一个（listbox1《==listbox2）
    function removes() {
        var items = listbox2.getSelecteds();
        if(items!=null && items.length>0){
            listbox2.removeItems(items);
            listbox1.addItems(items);
        }else{
            mini.alert("请选择一个角色移除");
        }

    }
    //向左移动全部（listbox1《==listbox2）
    function removeAll() {
        var items = listbox2.getData();
        listbox2.removeItems(items);
        listbox1.addItems(items);
    }



    var RoleType =CommonUtil.serverData.dictionary.RoleType;
    function onRoleType(e) {
        for (var i = 0, l = RoleType.length; i < l; i++) {
            var g = RoleType[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }

    //角色级别字典
    var roleLevel =CommonUtil.serverData.dictionary.roleLevel;
    //渲染级别字典
    function onroleLevelRenderer(e) {
        for (var i = 0, l = roleLevel.length; i < l; i++) {
            var g = roleLevel[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }
    //机构字典
    var InstitutionType =CommonUtil.serverData.dictionary.InstitutionType;
    //渲染机构字段
    function onInstitutionTypeRenderer(e) {
        for (var i = 0, l = InstitutionType.length; i < l; i++) {
            var g = InstitutionType[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }
    //启用状态字典
    var YesOrNo = [{ "id": "1", "text": "是" }, { "id": "0", "text": "否" }];
    //渲染启用状态
    function onYesOrNoRenderer(e) {
        for (var i = 0, l = YesOrNo.length; i < l; i++) {
            var g = YesOrNo[i];
            if (g.id == e.value) return g.text;
        }
        return "";
    }

</script>