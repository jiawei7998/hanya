<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
    <script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
	<title>打印模板维护</title>
</head>
<body>
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">模板</a> >> 修改</span>
            <table id="print_form" style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%"><input id="templateName" name="templateName" class="mini-textbox" 
                        labelField="true"  label="模板名称："  emptyText="请输入模板名称"
                        required="true"   style="width:100%" vtype="maxLength:50" labelStyle="text-align:left;width:170px"/>
                        <input id="tempCode" name="tempCode"  class="mini-hidden"/>
                    </td>
                    <td style="width:50%"><input id="beanName" name="beanName" class="mini-textbox" 
                        labelField="true"  label="数据加载类bean名："  emptyText="请输入数据加载类bean名"
                        required="true"  style="width:100%" vtype="maxLength:32" onvalidation="onEnglishValidation" labelStyle="text-align:left;width:170px"/>
                    </td>
                    
                    
                </tr>
                
                <tr>
                    <td style="width:50%;vertical-align: top;"><input class="mini-htmlfile" name="Fdata"  id="file"   
                        labelField="true"  label="模板文件："  emptyText="请选择模板文件" 
                        style="width:100%" labelStyle="text-align:left;width:170px"/>
                    </td>
                    <td style="width:50%"><input id="tempDesc" name="tempDesc" class="mini-textarea" 
                        labelField="true"  label="描述："  emptyText="请输入描述"
                        vtype="maxLength:100" style="width:100%" labelStyle="text-align:left;width:170px"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    

    <script>
        $(document).ready(function(){
            mini.parse();
            init();
        });


        var url = window.location.search;
        var action = CommonUtil.getParam(url,"action");

        function init(){
            if(action == "new"){

            }else if(action == "edit" || action == "detail"){
                var form1 = new mini.Form("print_form");
                var data=top["printTemplateManage"].getData();
                form1.setData(data);

                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>模板</a> >> 详情");
                    form1.setEnabled(false);
                    document.getElementById("save_btn").style.display="none";
                    document.getElementById("cancel_btn").style.display="none";
                    
                }

            }
        }
        
        

        //保存
        function save(){
            var inputFile = $("#file > input:file")[0];
            var param = {};
            var form = new mini.Form("print_form");
            form.validate();
	        if (form.isValid() == false){//表单验证
                mini.alert("请输入有效数据！","系统提示");
                return;
            } 
            param = form.getData();


            $.ajaxFileUpload({
                
                url: '<%=basePath%>/sl/PrintController/uploadPrintTemplate', //用于文件上传的服务器端请求地址
                fileElementId: inputFile, 
                data:param,  //文件上传域的ID
                dataType: 'text', //返回值类型 一般设置为json
                success: function (data, status) //服务器成功响应处理函数
                {
                    mini.alert("保存成功！","消息",function(){
                        //关闭自己
                        window.CloseOwnerWindow("ok");
                    });

                },
                error: function (data, status, e) //服务器响应失败处理函数
                {
                    mini.alert("保存失败："+e,"消息");
                },
                complete: function () {
                    var jq = $("#file > input:file");
                    jq.before(inputFile);
                    jq.remove();
                }
            });

        }
        //取消按钮
        function cancel(){
            window.CloseOwnerWindow();
        }

        function onEnglishValidation(e) {
            if (e.isValid) {
                if (isEnglish(e.value) == false) {
                    e.errorText = "必须输入英文";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文 */
        function isEnglish(v) {
            var re = new RegExp("^[a-zA-Z\_]+$");
            if (re.test(v)) return true;
            return false;
        }
        

    

    </script>

</body>
</html>