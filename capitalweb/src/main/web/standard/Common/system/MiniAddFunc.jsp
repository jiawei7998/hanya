<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<div id="func">  
<table id="field_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
	<h1 style="text-align:left;" id="labell"><a href="javascript:CommonUtil.activeTab();">菜单配置</a> >>  新增功能点</h1>
    <tr>
       <td style="width:50%">
            <input id="moduleId" name="moduleId" class="mini-textbox" labelField="true" label="功能点父级：" enabled="false" required="true"  validateOnLeave="true" enable="true" style="width:100%" labelStyle="text-align:left;width:170px"/>
        </td>
       <td style="width:50%">
            <input id="functionId" name="functionId" required="true"  labelField="true" label="功能点Id：" emptyText="功能点Id自动生成" class="mini-textbox" style="width:100%" labelStyle="text-align:left;width:170px"/>
        </td>
          </tr>
        <tr>
       <td style="width:50%">
            <input id="functionName" name="functionName" required="true"  labelField="true" label="功能点名称：" class="mini-textbox mini-mustFill" vtype="maxLength:10"
                 requiredErrorText="功能点名不能为空" emptyText="请输入功能点名称" style="width:100%" labelStyle="text-align:left;width:170px" />
        </td>
  
    
       <td style="width:50%">
            <input id="isActive" name="isActive" labelField="true" required="true"  emptyText="请选择是否启用" label="是否启用：" class="mini-combobox mini-mustFill" data="CommonUtil.serverData.dictionary.YesNo"
            style="width:100%" labelStyle="text-align:left;width:170px"/>
        </td>
        
        </tr>
        <tr>
       <td style="width:50%">
            <input id="functionUrl" name="functionUrl" labelField="true" required="true"  emptyText="请选择功能点"
                label="功能点：" class="mini-combobox mini-mustFill"  data="CommonUtil.serverData.dictionary.buttonType" style="width:100%" labelStyle="text-align:left;width:170px"/>
        </td>
        <td>
        
        </td>
    </tr>
    <tr>
    <td colspan="3" style="text-align:center;">
        <a class="mini-button" style="display: none"    id="save_btn" onClick="save();">保存</a>
    </td>
    <td>

    </td>
    <td>

    </td>
        
    </tr>
</table>
</div>
<script>
    mini.parse();
    var url = window.location.search;
    var action = CommonUtil.getParam(url, "action");
    mini.get("moduleId").setValue(GetQueryString("moduleId"));
    mini.get("functionId").setEnabled(false);
    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return unescape(r[2]);
        return null;
    }

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
            if (action == "edit") {
                var functionId = CommonUtil.getParam(url, "functionId");
                var moduleId = CommonUtil.getParam(url, "moduleId")
                initFunction(functionId, moduleId);
            }
        });
    });

    function initFunction(functionId,moduleId){
        var data = {};
        data['functionId']=functionId;
        data['moduleId']=moduleId;
        CommonUtil.ajax({
            url: "/RoleController/getFunc",
            data : data,
            callback:function (data) {
                if (data && data.obj) {
                    new mini.Form("field_form").setData(data.obj);
                }
            }
        });
    }


    //保存操作
    function save(){
        var form = new mini.Form("func");
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        var url=null;
        if(action == "add"){
            url="/RoleController/saveFunc";
        }else if(action = "edit"){
            url="/RoleController/updateFunc";
        }
        var data = form.getData();
        data.branchId=branchId;
        mini.confirm("您确定要保存吗？", "温馨提示", function (action) {
            if (action == 'ok') {
                CommonUtil.ajax({
                    url: url,
                    data: mini.encode(data),
                    callback: function (data) {
                        if ("error.common.0000" == data.code) {
                            mini.alert("操作成功","温馨提示",function(){
                                onCancel('ok');
                            });
                        } else {
                            mini.alert("操作失败");
                        }
                    }
                });

            }

        });
    }
    

function onCancel(e) {
window.CloseOwnerWindow();
}

</script>
<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>