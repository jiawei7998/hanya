<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%> 
<html>
<head>
<title>Insert title here</title>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body>
    <div  id="editForm"  class="fieldset-body">
        <table id="field_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
        <h1  id="labell"><a href="javascript:CommonUtil.activeTab();">机构清算信息</a> >> 修改</h1>
            <tr>
                <td style="width:50%"><input id="instId" name="instId"   allowInput="false"  labelField="true" label="机构："  class="mini-buttonedit mini-mustFill" onbuttonclick="onInsQuery"  required="true"    emptyText="请选择机构..." style="width:100%" labelStyle="text-align:left;width:170px"/></td>
                <td style="width:50%"><input id="busiType" name="busiType"  class="mini-combobox mini-mustFill"    labelField="true" label="业务类别：" emptyText="请输入业务类别" required="true"   data="CommonUtil.serverData.dictionary.busiType" style="width:100%" labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
                <td style="width:50%"><input id="ccy" name="ccy"  class="mini-combobox" labelField="true"    label="币种代码：" emptyText=""  required="true"   data="CommonUtil.serverData.dictionary.Currency" value="CNY" enabled = "false" style="width:100%" labelStyle="text-align:left;width:170px"/></td>
                <td style="width:50%"><input  id="instAcctNo" name="instAcctNo"  class="mini-textbox mini-mustFill"    labelField="true" label="账户号：" emptyText="请输入账号" required="true"  maxLength="32" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" style="width:100%" labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
                <td style="width:50%"><input  id="instAcctNm" name="instAcctNm"  class="mini-textbox mini-mustFill"    labelField="true" label="账户名：" emptyText="请输入户名"   required="true"  vtype="maxLength:26" style="width:100%" labelStyle="text-align:left;width:170px"/></td>
                <td style="width:50%"><input  id="mediumType" name="mediumType"     class="mini-combobox" labelField="true" label="账号类型：" emptyText="请输入账号类型"   required="true"  data="CommonUtil.serverData.dictionary.mediumType" value="A" enabled="false" style="width:100%" labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
                <td style="width:50%"><input id="instAcctPbocNo" name="instAcctPbocNo"     required="true"  class="mini-textbox mini-mustFill" labelField="true" label="开户行号：" emptyText="请输入开户行号" maxLength="14" onvalidation="CommonUtil.onValidation(e,'alphanum',[null]) " style="width:100%" labelStyle="text-align:left;width:170px"/></td>
                <td style="width:50%"><input id="instAcctBknm" name="instAcctBknm"  class="mini-textbox mini-mustFill"    required="true"  labelField="true" label="开户行名：" emptyText="请输入开户行名"   vtype="maxLength:26" style="width:100%" labelStyle="text-align:left;width:170px"/></td>
            </tr>
            <tr>
               
                <td style="width:50%"><input id="payerAccType" name="payerAccType"      class="mini-combobox" labelField="true" label="大额支付账号类型：" emptyText="请输入大额支付账号类型" required="true"  data="CommonUtil.serverData.dictionary.payerAccType" value="1"  enabled="false" style="width:100%" labelStyle="text-align:left;width:170px"/></td>
                <td style="width:50%"><input id="isActive" name="isActive"  style="width:100%" labelStyle="text-align:left;width:170px"   class="mini-combobox" labelField="true" label="是否启用：" emptyText="" value="1" required="true"  data="CommonUtil.serverData.dictionary.isActive"/></td>
            </tr>
            <tr>
                
                <td style="width:50%"><input id="instRemark" name="instRemark"  style="width:100%" labelStyle="text-align:left;width:170px"   class="mini-textbox"  labelField="true" label="备注：" emptyText="请输入备注"   vtype="maxLength:100"/></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:center;">
                    <a id="save_btn" class="mini-button" style="display: none"    onclick="add">保存</a>
                    <a id="cancel_btn" class="mini-button" style="display: none"    onclick="close()">取消</a>
                </td>
            </tr>
        </table>
    </div>
    <script>
        mini.parse();
        var url=window.location.search;
	    var action=CommonUtil.getParam(url,"action");
        var param=top['InsitutionSettles'].getData(action);
        var form=new mini.Form("#field_form");
        //关闭页面
        function close(){
            top['win'].closeMenuTab();
        }

        $(document).ready(function(){
			initForm();
            if (action=="detail") {
                $("#labell").html("<a href=javascript:CommonUtil.activeTab();>机构清算信息</a>>>详情");
                mini.get("save_btn").setVisible(false);
                form.setEnabled(false);
            }else if(action=="add"){
                $("#labell").html("<a href=javascript:CommonUtil.activeTab();>机构清算信息</a>>>新增")
            }
        });
        //赋值给form表单
        function initForm(){
            if ($.inArray(action,['edit','detail']) > -1) {
             form.setData(param);
             var instId =mini.get("instId");
             instId.setValue(param.instId);
             instId.setText(param.instFullname);
            }
        }
        //保存
        function add(){
            form.validate();
            if(form.isValid() == false){
                mini.alert("表单填写错误,请确认!","提示信息")
                return;
            }
            var params=form.getData();
            
            var saveUrl =$.inArray(action, ["add"]) > -1 ? "/InstitutionSettlsController/addInstitutionSettl" : "/InstitutionSettlsController/updateInstitutionSettl";
            if ($.inArray(action,['edit'])>-1) {
               params.settlId=param.settlId;
            }
            var data=mini.encode(params);
            CommonUtil .ajax({
                url:saveUrl,
                data:data,
                callback : function(data){
                    if('error.common.0000' == data.code){
                        mini.alert("保存成功","提示",function(){
                            setTimeout(function(){top["win"].closeMenuTab()},10);
                        });
                
                    }else{
                        mini.alert("保存失败");
                    }
                }
            });
        }
        // 查询机构的方法
        function onInsQuery(e) {
            var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/../Common/MiniInstitutionSelectManages.jsp",
                title: "机构选择",
                width: 900,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data); //必须
                        if (data) {
                            btnEdit.setValue(data.instId);
                            btnEdit.setText(data.instName);
                            btnEdit.focus();
                        }
                    }

                }
            });
        } 
        
    </script>
	<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
</body>
</html>