<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
	<%@ include file="../../global.jsp"%>
		<html>
		<head>
			<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
			<title></title>
		</head>
		<body style="width:100%;height:100%;background:white">
			<div id="form1">
					<div id="MiniInstitutionAddEdits" class="mini-panel" style="width:100%"  allowResize="true" collapseOnTitleClick="false">   
					<table id="field_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
					<tr>
						<td style="width:50%">
							<input id="instId" name="instId" class="mini-textbox"
								   labelField="true" label="机构代码："  emptyText="系统自动生成" style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
						<td style="width:50%">
							<input id="instFullname" name="instFullname" onvaluechanged="trimSelf" class="mini-textbox mini-mustFill" requiredErrorText="该输入项为必输项" required="true"  vtype="rangeLength:1,125"
							 labelField="true" label="机构全称："   emptyText="请输入机构全称" style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
					</tr>
					<tr>
						<td style="width:50%">
							<input id="instName" name="instName" onvaluechanged="trimSelf" class="mini-textbox mini-mustFill" requiredErrorText="该输入项为必输项" required="true"  vtype="rangeLength:1,32"
							 labelField="true" label="机构简称：" emptyText="请输入机构简称" style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
						<td style="width:50%">
							<input id="pInstName" name="pInstName" class="mini-hidden"/>
							<input id="pinstId" name="pinstId" class="mini-buttonedit searchbox" onbuttonclick="onInsQuery"  labelField="true" label="上级机构："
							 emptyText="请选择上级机构..." allowinput='false' style="width:100%" labelStyle="text-align:left;width:170px"
							 />
							<div class="mini-hidden" id="pinstLevel"></div>
						</td>
						</tr>
					<tr>
						<td style="width:50%">
							<input id="instType" name="instType" class="mini-ComboBox mini-mustFill" vtype="" requiredErrorText="该输入项为必输项" data="CommonUtil.serverData.dictionary.InstitutionType"
							 required="true"  emptyText="请选择机构类型..." labelField="true" label="机构类型：" style="width:100%" labelStyle="text-align:left;width:170px" onvaluechanged="checkInst"/>
						</td>
						<td style="width:50%">
							<input id="instStatus" name="instStatus" class="mini-ComboBox mini-mustFill" vtype="" requiredErrorText="该输入项为必输项" data="CommonUtil.serverData.dictionary.AccStatus"
							 required="true"  emptyText="请选择机构状态..." labelField="true" label="机构状态：" value="2"  emptyText="请选择机构状态..." style="width:100%" labelStyle="text-align:left;width:170px"
							/>
						</td>
					</tr>
					<tr>
						<td style="width:50%">
							<input id="onlineDate" name="onlineDate" class="mini-datepicker" requiredErrorText="该输入项为必输项"  labelField="true"
							 label="创建日期："  emptyText="请选择创建日期..."  style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
						<td style="width:50%">
							<input id="thirdPartyId" name="thirdPartyId" class="mini-ComboBox" data="CommonUtil.serverData.dictionary.instMapper" value="01" labelField="true" label="交易映射机构[OPICS]：" vtype="rangeLength:1,16" emptyText="交易映射机构"
								   style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>

						</tr>
					<tr>
						<td style="width:50%">
							<input id="bInstId" name="bInstId" class="mini-textbox" labelField="true" vtype="rangeLength:1,16" label="记账机构号：" emptyText="请输入记账机构号"
							style="width:100%" labelStyle="text-align:left;width:170px" visible="false"/>
						</td>

						<td style="width:50%">
							<input id="instLrInstCode" name="instLrInstCode" class="mini-textbox"  requiredErrorText="该输入项为必输项"  visible="false"
								   vtype="rangeLength:1,32" labelField="true" label="机构代码证："  emptyText="请输入机构代码证" style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>

						<td style="width:50%">
							<input id="isFta" name="isFta" class="mini-hidden" class="mini-ComboBox" data="CommonUtil.serverData.dictionary.YesNo" labelField="true" label="是否自贸区机构：" value="1"
								   emptyText="请选择是否自贸区机构..." style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
					<%--	<td style="width:50%">
							<input id="instSimpleName" name="instSimpleName" class="mini-textbox" labelField="true" vtype="rangeLength:1,100" label="简名：" 
							 emptyText="请输入简名" style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>--%>
                    </tr>
			<%--		<tr>
						<td style="width:50%">
							<input id="linkMan" name="linkMan" class="mini-textbox" labelField="true" vtype="rangeLength:1,15" label="联系人："  emptyText="请输入联系人"
							style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
						<td style="width:50%">
							<input id="telephone" name="telephone" class="mini-textbox" labelField="true" label="手机："  emptyText="请输入手机号码"
							vtype="maxLength:50;" onvalidation="CommonUtil.onValidation(e,'mobile',[null])" style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
					</tr>--%>
					<%--<tr>
						<td style="width:50%">
							<input id="businessUnit" name="businessUnit" class="mini-textbox"  vtype="rangeLength:1,2" labelField="true"
							 requiredErrorText="该输入项为必输项" label="业务单元：" emptyText="请输入业务单元" style="width:100%" labelStyle="text-align:left;width:170px"/>
						</td>
						</tr>
					<tr>
						<td style="width:50%">
							<input id="printInstId" name="printInstId" class="mini-textbox" labelField="true" label="打票机构：" vtype="rangeLength:1,16" emptyText="请输入打票机构"
							style="width:100%" labelStyle="text-align:left;width:170px"/>
							
						</td>
						<td>
							<input id="branchId" class="mini-hidden" name="branchId" value="<%=__sessionUser.getBranchId()%>" />
							<input id="pinstType" name="pinstType" class="mini-hidden" />
						</td>
					</tr>--%>
					<tr>
						<td colspan="3" style="text-align:center;">
							<a id="save_btn" class="mini-button" style="display: none"    onclick="save()">保存</a>
							<a id="cancel_btn" class="mini-button" style="display: none"    onclick="close">取消</a>
						</td>
					</tr>
				</table>
			</div>
			</div>
			<script type="text/javascript">
				mini.parse();
				var url = window.location.search;
				var action = CommonUtil.getParam(url, "action");
				var param = top["InstitutionManger"].getData(action);
				var form = new mini.Form("#field_form");
				var instIdValue =mini.get("instId");
				var instType = mini.get("instType");//机构类型
				//验证机构代码
				
				var saveUrl = "";
					if (action == "add") {
						saveUrl = "/InstitutionController/addInstitution";
					} else if (action == "edit") {
						saveUrl = "/InstitutionController/updateInstitution";
					}
				function save() {
					form.validate();
					if (form.isValid() == false)
					// mini.alert("信息填写有误，请重新填写!", "消息提示")
						return;
					//提交数据
					var data = form.getData(true); //获取表单多个控件的数据  
					data.branchId=branchId ;
					var param = mini.encode(data); //序列化成JSON
					CommonUtil.ajax({
						url: saveUrl, 
						data: param,
						callback: function (data) {
							if (data.code == 'error.common.0000') {
								mini.alert("保存成功",'提示信息',function(){
									setTimeout(function(){top["win"].closeMenuTab()},10);
								});
							} else {
								mini.alert("保存成功");
							}
						}
					});
				}
				function close() {
							top["win"].closeMenuTab();
						}
				function initform() {
						if (action == "add") {
							mini.get("MiniInstitutionAddEdits").setTitle("机构信息  >> 新增");
							mini.get("onlineDate").setValue("<%=__bizDate %>");
							mini.get("instStatus").setEnabled(false);
							mini.get("instId").setEnabled(false);
						}

						else if (action == "edit") {
							mini.get("MiniInstitutionAddEdits").setTitle("机构信息  >> 修改");
							form.setData(param);
							mini.get("instId").setEnabled(false);
							mini.get("onlineDate").setEnabled(false);

							mini.get("pinstId").setValue(param.pInstId);
							if(param.pInstId=='-1'){
								mini.get("pinstId").setText(param.pInstId);
							}else{
								mini.get("pinstId").setText(param.pInstName);
							}

						} else if (action == "detail") {
							mini.get("MiniInstitutionAddEdits").setTitle("机构信息  >> 详情");
							form.setData(param);
							mini.get("pinstId").setValue(param.pInstId);
							mini.get("pinstId").setText(param.pInstName);
							form.setEnabled(false);
							mini.get("save_btn").setEnabled(false);
							mini.get("cancel_btn").setEnabled(false);
							if(param.pInstId=='-1'){
								mini.get("pinstId").setText(param.pInstId);
							}else{
								mini.get("pinstId").setText(param.pInstName);
							}
						}

				
					}
			
				// 查询机构的方法
				function onInsQuery(e) {
					var btnEdit = this;
					mini.open({
						url: CommonUtil.baseWebPath() + "/../Common/MiniInstitutionSelectManages.jsp",
						title: "机构选择",
						width: 900,
						height: 600,
						ondestroy: function (action) {
							if (action == "ok") {
								var iframe = this.getIFrameEl();
								var data = iframe.contentWindow.GetData();
								data = mini.clone(data); //必须
								if (data) {
									btnEdit.setValue(data.instId);
									btnEdit.setText(data.instName);
									mini.get("pInstName").setValue(data.instName);
									mini.get("pinstLevel").setValue(data.instType);
									btnEdit.focus();
									checkInst();
								}
							}
						}
					});
				}

				$(document).ready(function () {
					//控制按钮显示
					$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn) {
						initform();
						instIdValue.on("validation", function (e) {
							if (e.isValid) {
								if (EnglistAndNumberValidation(e.value) == false) {
									e.errorText = "请输入字母或者数字";
									e.isValid = false;
								}

							}
						});
					});
				})
				//验证只能输入字母数字
				function EnglistAndNumberValidation(userId) {
						var reg = new RegExp("^[0-9a-zA-Z]+$");
						if (!reg.test(userId)) {
							return false;
						}
						return true;
					}
				function trimSelf(e){
					mini.get(e.sender).setValue($.trim(e.value));
				}

				function checkInst() {
					//上级机构是总行的,机构类型不能选总行
					instTypeValue = instType.getValue();
					var pinstType = mini.get("pinstLevel").getValue();
					var pinstId = mini.get("pinstId");
					if(pinstId.getValue() == "00000" && instTypeValue == "0"){
						instType.setValue("");
						instType.setText("");
						mini.alert('上级机构是哈尔滨银行,机构类型不能选总行','提示信息');
					}
					if( instTypeValue  && pinstType && instTypeValue <= pinstType){
						mini.alert('机构类型不能高于上级机构','提示信息');
					}
				}

			</script>
			<script src="<%=basePath%>/miniScript/miniMustFill.js"></script>
		</body>
		</html>