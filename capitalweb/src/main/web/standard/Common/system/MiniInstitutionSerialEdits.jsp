<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../../global.jsp"%> 
<html>
	<head>
	</head>
	<body>
	    <div class="mini-fit">
			<div id="inst_ul" class="mini-treegrid" showTreeIcon="true" treeColumn="instName" idField="id" parentField="parentId" resultAsTree="false"
				 allowResize="true" expandOnLoad="true" height="100%" allowAlternating="true" multiSelect="true" allowUnselect="true">
				<div property="columns" allowMove="true" allowResize="true">
					<div type="checkcolumn"></div>
					<div type="indexcolumn" width="50" headerAlign="center"></div>
					<div field="instId" width="100" headerAlign="center">机构代码</div>
					<div name="instName" field="instName" width="250" headerAlign="center">机构简称</div>
					<div field="instFullname" width="300" headerAlign="center">机构全称</div>
					<div field="instType" width="100" renderer="onInstitutionTypeRenderer" headerAlign="center" align="center">机构类型</div>
				</div>
			</div>
		</div>
		<a id="save_btn" class="mini-button" style="display: none"    onclick="save()">保存</a>
	</body>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script> 
	<script type="text/javascript">
		mini.parse();
		var url=window.location.search;
	    var action=CommonUtil.getParam(url,"action");
	    var param=top['InstitutionManger'].getData(action);
	    var InstitutionType =CommonUtil.serverData.dictionary.InstitutionType;
	    
		$(document).ready(function(){
			searchSerialChildren();
		});
	
		//渲染机构字段
		function onInstitutionTypeRenderer(e) {
			for (var i = 0; i < InstitutionType.length; i++) {
				var g = InstitutionType[i];
				if (e != null && g.id == e.value) return g.text;
			}
			return "";
		}
		
		function searchSerialChildren(){

			CommonUtil.ajax({
				url: '/InstitutionController/searchInstitutions',
				data: {"branchId":"<%=__sessionUser.getBranchId()%>"},
				callback: function (data) {
					if(data.obj){
						var grid = mini.get("inst_ul");
						grid.setData(data.obj);
						CommonUtil.ajax( {
							url:"/InstitutionSerialController/selectSerialChildrenInst",
							data:{"instId":param.instId},
							callback : function(data2) {
								var nodes=new Array();
								if(data2.obj){
									//循环判断并还原机构选择

									$.each(data2.obj, function(i,item){
										var node = grid.findRow(function(row){
											//返回当前流程类型下的所有流程
											if(row.id == item) return true;
										});
										nodes.push(node);
									});
									grid.selects(nodes);
								}

							}
						});
					}
				}
			});
		};
		
		function save(){
			var instTree = mini.get("inst_ul").getSelecteds();
			var instIds = "";
			$.each(instTree, function(i,item){
				instIds += item.instId;
				if(i != instTree.length - 1){
					instIds += ",";
				}
			});
			CommonUtil.ajax( {
				url:"/InstitutionSerialController/saveSerialChildrenInst",
				data:{"instId":param.instId,"childrenInstIds":instIds},
				callback : function(data) {
					if (data.code == 'error.common.0000') {
						mini.alert("操作成功","系统提示",function(){setTimeout(function(){
							top["win"].closeMenuTab()
						},10);
							
						});
					} else {
						mini.alert("操作失败","系统提示");
					}
				}
			});
		};
	</script>
</html>