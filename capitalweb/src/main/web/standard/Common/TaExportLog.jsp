<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../global.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/exportHrbExcel.js"></script>
    <title>报表日志查询</title>
</head>
<body>
<fieldset class="mini-fieldset title">
    <div id="search_form" style="width:100%">
        <legend>报表查询</legend>
        <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="交易日期："
               ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
        <span>~</span>
        <input id="endDate" name="endDate" class="mini-datepicker"
               ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>

        <input id="userName" name="userName" class="mini-textbox" labelField="true" label="用户名称："
               labelSyle="text-align:right;" emptyText="请输入"/>
        <input id="exportInst" name="exportInst" class="mini-textbox" labelField="true" label="导出机构："
               labelSyle="text-align:right;" emptyText="请输入"/>
        <span style="float: right; margin-right: 150px">
                <a class="mini-button" id="search_btn" onclick="query()">查询</a>
                <a class="mini-button" id="clear_btn" onclick="clear()">清空</a>
<%--                <a id="export_btn" class="mini-button" style="display: none" onclick="exportExcel()">导出报表</a>--%>
            </span>
    </div>
</fieldset>
<div class="mini-fit" style="width:100%;height:100%;">
    <div id="repo_grid" class="mini-datagrid borderAll" style="width:100%;height:100%;"
         sortMode="client" allowAlternating="true" border="true" allowResize="true">
        <div property="columns">
            <div type="indexcolumn" headerAlign="center" align="center" width="30px">序号</div>
            <div field="userId" headerAlign="center" align="center">用户id</div>
            <div field="userName" headerAlign="center" align="center">用户名称</div>
            <div field="exportName" headerAlign="center" align="center">导出报表名称</div>
            <div field="exportInst" headerAlign="center" align="center">导出机构</div>
            <div field="exportTime" headerAlign="center" align="center" width="150px">导出时间</div>
            <div field="exportStatus" headerAlign="center" align="center" renderer="exportStatus">状态</div>
            <div field="remark" headerAlign="center" align="center">备注</div>
            <div field="timeCost" headerAlign="center" align="center">耗时</div>
            <div field="ip" headerAlign="center" align="center">IP地址</div>
        </div>
    </div>
</div>
<script>

    mini.parse();

    var grid = mini.get("repo_grid");
    var form = new mini.Form("#search_form");
    var currTab = top["win"].tabs.getActiveTab();
    grid.on("beforeload", function (e) {
        e.cancel = true;
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        search(pageSize, pageIndex);
    });

    $(document).ready(function () {
        //控制按钮显示
        $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function (visibleBtn) {
            query();
        });
    });

    //查询按钮
    function query() {
        search(10, 0);
    }

    function search(pageSize, pageIndex) {
        var searchUrl = "/TaReportLogController/getPageData";
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        var params = mini.encode(data);
        CommonUtil.ajax({
            url: searchUrl,
            data: params,
            callback: function (data) {
                grid.setTotalCount(data.obj.total);
                grid.setPageIndex(pageIndex);
                grid.setPageSize(pageSize);
                grid.setData(data.obj.rows);
            }
        });
    }

    //清空按钮
    function clear() {
        form.clear();
        query();
    }

    function exportStatus(status) {
        if (status.value) {
            if (status.value == '1') {
                return "成功";
            } else {
                return "失败";
            }
        }
        return "";
    }

    //交易日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("endDate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

</script>
</body>
</html>
