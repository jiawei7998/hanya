<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=basePath%>/standard/Common/opics.js"></script>
<div class="centerarea">
	<div id="panel1" class="mini-panel" title="OPICS 管理要素" style="width:100%"  allowResize="true" collapseOnTitleClick="true">
		<div class="leftarea">
		<input id="dealSource" name="dealSource" class="mini-combobox" labelField="true"  style="width:100%;"  label="交易来源：" required="true"  data="CommonUtil.serverData.dictionary.TradeSource" vtype="maxLength:10"  labelStyle="text-align:left;width:130px;"  >
        <input id="port" name="port" class="mini-textbox" onbuttonclick="onPortQuery"  style="width:100%;"  labelField="true"  label="投资组合：" required="true"  allowInput="false" vtype="maxLength:4" labelStyle="text-align:left;width:130px;" >
        <input id="cost" name="cost" class="mini-textbox" labelField="true"  label="成本中心：" style="width:100%;"  required="true"  enabled="false" vtype="maxLength:15" labelStyle="text-align:left;width:130px;"  >
       	<input id="product" name="product" class="mini-textbox" labelField="true"  style="width:100%;"  label="产品代码：" required="true"  enabled="false"  maxLength="10" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" >
	    <input id="prodType" name="prodType" class="mini-textbox" labelField="true" style="width:100%;" label="产品类型：" required="true"  enabled="false"  vtype="maxLength:2" labelStyle="text-align:left;width:130px;" >
        </div>
        <div class="rightarea">
	        <!-- <input id="verifySign" name="verifySign" class="mini-combobox" labelField="true"  label="复核标识：" required="true"  data="CommonUtil.serverData.dictionary.YesNo" maxLength="2" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" >
	        <input id="grantSign" name="grantSign" class="mini-combobox" labelField="true"  label="授权标识：" required="true"  data="CommonUtil.serverData.dictionary.YesNo" maxLength="2" onvalidation="CommonUtil.onValidation(e,'alphanum',[null])" labelStyle="text-align:left;width:130px;" >
	        <input id="settleMeans" name="settleMeans" class="mini-textbox" labelField="true"  label="清算方式：" required="true"  vtype="maxLength:4" labelStyle="text-align:left;width:130px;" >
	        <input id="settleAcct" name="settleAcct" class="mini-textbox" labelField="true"  label="清算账户：" required="true"   vtype="maxLength:10" labelStyle="text-align:left;width:130px;" > -->
        	<input id="ccysmeans" name="ccysmeans" class="mini-textbox" labelField="true"  label="付款路径：" required="true"  vtype="maxLength:7" labelStyle="text-align:left;width:130px;" style="width:100%;">
	        <input id="ccysacct" name="ccysacct" class="mini-textbox" labelField="true"  label="付款账户：" required="true"   vtype="maxLength:15" labelStyle="text-align:left;width:130px;" style="width:100%;">
        	<input id="ctrsmeans" name="ctrsmeans" class="mini-textbox" labelField="true"  label="收款路径：" required="true"  vtype="maxLength:7" labelStyle="text-align:left;width:130px;" style="width:100%;">
	        <input id="ctrsacct" name="ctrsacct" class="mini-textbox" labelField="true"  label="收款账户：" required="true"   vtype="maxLength:15" labelStyle="text-align:left;width:130px;" style="width:100%;">
        	
        </div>
	</div>
</div>


