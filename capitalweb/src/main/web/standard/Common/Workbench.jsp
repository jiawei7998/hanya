<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title>字段限额管理页面</title>
</head>
<body style="width:100%;height:100%;background:white">
    <fieldset class="mini-fieldset title">
    <legend>信息查询</legend>
    <div id="search_form">        
        <input id="product" name="product" class="mini-textbox" labelField="true" label="产品：" labelStyle="text-align:right;" width="300px" emptyText="请输入产品" 
        />
        <input id="type" name="type" class="mini-combobox" labelField="true" label="限额类型：" data="CommonUtil.serverData.dictionary.limitType" width="300px" labelStyle="text-align:right;" emptyText="请选择限额类型" 
        />
        <input id="trader" name="trader" class="mini-buttonedit" onbuttonclick="onButtonEdit" emptyText="请选择交易员" labelField="true" allowInput="false" width="300px"
            label="交易员：" labelStyle="text-align:right;" />
        
        <span style="float:right;margin-right: 150px">
            <a class="mini-button" style="display: none"  id="search_btn"  onclick="search(10,0)">查询</a>
            <a class="mini-button" style="display: none"  id="clear_btn"  onclick="clear()">清空</a>
        </span>
    </div>
    </fieldset>
    <span style="margin:2px;display: block;">
        <a class="mini-button" style="display: none"  id="add_btn"  onClick="add();">新增</a>
        <a class="mini-button" style="display: none"  id="edit_btn"  onClick="modify();">修改</a>
        <a class="mini-button" style="display: none"  id="delete_btn"  onClick="del">删除</a>
    </span>
    <div class="mini-fit" style="width:100%;height:100%;">
        <div  id="grid"  class="mini-datagrid borderAll" style="width:100%;height:100%;" sortMode="client" 
        allowAlternating="true"   idField="userId" onrowdblclick="onRowDblClick" >
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="id" headerAlign="center" allowSort="true" width="150px">产品限额编号</div>
                <div field="product" headerAlign="center"  width="150px">产品</div>
                <div field="type" headerAlign="center" width="100px" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'limitType'}">限额类型</div>
                <div field="trader" headerAlign="center"  width="100px">交易员</div>
                <div field="limitCondition" headerAlign="center" width="180px" >限额条件</div>
                <div field="quotaAmount" headerAlign="center"  allowSort="true" align="right" numberFormat="#,0.00">限额金额</div>
                <div field="availAmount" headerAlign="center" allowSort="true" align="right" numberFormat="#,0.00">可用金额</div>
                <div field="inputTrader" headerAlign="center" align="center" width="100px">录入人员</div>
                <div field="inputTime" headerAlign="center" align="center"  allowSort="true" width="150px">录入时间</div>                
            </div>
        </div>
    </div>
</body>

<script>

mini.parse();

var grid = mini.get("grid");
$(document).ready(function () {
    //控制按钮显示
    $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
        grid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex;
            var pageSize = e.data.pageSize;
            search(pageSize, pageIndex);
        });
        search(grid.pageSize, grid.pageIndex);
    });
})

//增删改查
function add(){
	var url = CommonUtil.baseWebPath() + "/../Common/WorkbenchEdits.jsp?action=add";
	var tab = { id: "WorkbenchAdd", name: "WorkbenchAdd", title: "产品限额新增", url: url, showCloseButton: true,parentId:top["win"].tabs.getActiveTab().name };
	var paramData = {selectData:""};
	CommonUtil.openNewMenuTab(tab,paramData);
}
function query() {
    search(grid.pageSize, 0);
}
function clear(){
    var form=new mini.Form("search_form");
    form.clear();
    search(10,0);

}


//修改
function modify() {
   var row = grid.getSelected(); 
    if (row) {
        var url = CommonUtil.baseWebPath() + "/../Common/WorkbenchEdits.jsp?action=edit";
        var tab = { id: "WorkbenchEdit", name: "WorkbenchEdit", title: "产品限额修改", url: url, parentId: top["win"].tabs.getActiveTab().name,showCloseButton:true };
        var paramData = {selectData:row};
		CommonUtil.openNewMenuTab(tab,paramData);
    } else {
        mini.alert("请选择一条数据");
    }
}


//搜索
function search(pageSize, pageIndex) {
    var form = new mini.Form("search_form");
    var data = form.getData();
    form.validate();
    if(form.isValid()==false){
        return;
    }
    data.branchId =branchId;
    data.pageNumber = pageIndex + 1;
    data.pageSize = pageSize;
    var param = mini.encode(data); //序列化成JSON
    CommonUtil.ajax({
        url: "/IfsLimitController/searchPageLimit",
        data: param,
        callback: function (data) {
            grid.setTotalCount(data.obj.total);
            grid.setPageIndex(pageIndex);
            grid.setPageSize(pageSize);
            grid.setData(data.obj.rows);
        }
    });
}

    //删除
    function del(){
        var rows=grid.getSelecteds();
        if(rows.length==0){
            mini.alert("请选中一行","提示");
            return;
        }
        mini.confirm("您确认要删除该产品所对应的限额?","系统警告",function(value){   
            if (value=='ok'){   
                var data=rows[0];
                params=mini.encode(data);
                CommonUtil.ajax( {
                    url:"/IfsLimitController/deleteLimit",
                    data:params,
                    callback : function(data) {
                        mini.alert("删除成功.","系统提示");
                        search(10,0);
                    }
                });
            }
        });
    }

    function onButtonEdit(e) {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/MiniUserManagesMini.jsp",
            title: "选择限额交易员列表",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.userId);
                        btnEdit.setText(data.userName);
                        btnEdit.focus();
                    }
                }

            }
        });

    }
</script>
 

</html>