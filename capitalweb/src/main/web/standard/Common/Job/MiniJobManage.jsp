<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
    <title></title>
  </head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>定时任务查询</legend>

<div id="JobManage">
    <div id="search_form" class="mini-form" >
		<input class="mini-textbox" width="320px" id="jobId" name="jobId" labelField="true" label="任务编号：" labelStyle="text-align:right;" emptyText="请输入任务编号" >
		<input class="mini-textbox" width="320px" id="jobName" name="jobName" labelField="true" label="任务名称：" labelStyle="text-align:right;" emptyText="请输入任务名称" >
		<span style="float:right;margin-right: 150px">
			<a  id="search_btn" class="mini-button" style="display: none"   onclick="search(10,0)">查询</a>
			<a  id="clear_btn" class="mini-button" style="display: none"   onclick='clear()'>清空</a>
		</span>	
			
	</div>  
</div>
</fieldset> 
<span style="margin:2px;display: block;">
	<!-- <a id="search_queue" onclick="searchque()" class="mini-button" style="display: none"   >查询任务队列</a> -->
	<a id="add_btn" class="mini-button" style="display: none"  onclick="addJob()"  >新增</a>
	<a id="edit_btn" class="mini-button" style="display: none"  onclick="editJob()"  >修改</a>
	<a id="delete_btn" onclick="deleteJob()" class="mini-button" style="display: none"   >删除</a>
	<a id="start_btn" class="mini-button" style="display: none"  onclick="start()"  >启用</a>
	<a id="stop_btn" class="mini-button" style="display: none"  onclick="stop()"  >停用</a>
	<span style="color: red">定时任务执行与编号无关，定时任务谨慎修改</span>

</span>
<div class="mini-fit">
	<div id="datagrid" class="mini-datagrid borderAll" style="height:100%;" 
			idField="jobId" allowResize="true" multiSelect="true"  allowCellEdit="true"  
			border="true" onshowrowdetail="onShowRowDetail" >
		<div property="columns">
			<div type="indexcolumn" width="50" headerAlign="center" align="left">序号</div>
			<div type="expandcolumn"></div>
			<div field="jobId" width="60" headerAlign="center" align="center">任务编号</div>    
			<div field="execJob" width="60" headerAlign="center" align="center" renderer="execJob">手动执行</div>                         
			<div field="isDisabled" name="isDisabled" width="60"  headerAlign="center" align="center"  renderer="CommonUtil.dictRenderer" data-options="{dict:'YesNo'}" >是否启用</div>
			<div field="resetStatus"   headerAlign="center" align="center" visible=('undefined' != typeof debug && debug == true ? false : true) >状态重置</div>                                
			<div field="terminateReset" width="100"  headerAlign="center" align="center" visible=('undefined' != typeof debug && debug == true ? false : true)>是否终止</div>
			<div field="jobName" width="130"  headerAlign="center" align="left">任务名称</div>
			<div field="schedule" width="100" headerAlign="center" align="left">时间表</div>
			<div field="manRunDate" width="140" headerAlign="center" align="center">上次手动执行时间</div>
			<div field="lastRunDate" width="140" headerAlign="center" align="center">上次执行时间</div>
			<div field="nextRunDate" width="140" headerAlign="center" align="center">下次执行时间</div>
			<div field="jobStatus" width="100" headerAlign="center" align="center" renderer="jobStatus">任务状态</div>
			<div field="isStartup" width="100" headerAlign="center" align="center" renderer="yesNo">是否立即启动</div>
			<div field="isSingleton" width="100" headerAlign="center" align="center" renderer="yesNo">是否单例执行</div>
			<div field="version" width="100" headerAlign="center" align="left">执行版本</div>
		</div>
	</div>
	<div id="jobLog" class="mini-datagrid borderAll" style="display: none;" >
		<div property="columns">
			<div type="indexcolumn"></div>
			<div field="jobId" width="70" headerAlign="center" align="center">任务编号</div>    
			<div field="startTime" width="80" headerAlign="center" align="center">开始时间</div>                            
			<div field="endTime" width="100" headerAlign="center" align="center">结束时间</div>
			<div field="execTime" width="70"  headerAlign="center" align="center" >执行时间（ms）</div>
			<div field="dbTime" width="100"  headerAlign="center" align="center" >数据库时间</div>
			<div field="execUser" width="70"  headerAlign="center" align="center">执行人</div>
			<div field="execIp" width="70" headerAlign="center" align="center">执行IP</div>
			<div field="result" width="70" headerAlign="center" align="center">执行结果</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	mini.parse();
	top['miniJobManage']=window;
	var grid=mini.get("datagrid");
	//查询任务列表
	function search(pageSize,pageIndex){
		var form=new mini.Form("#search_form");
		form.validate();
		if(form.isValid==false) return;
		var data=form.getData();
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		var params=mini.encode(data);
		CommonUtil.ajax( {
			url:"/JobController/searchPageJob",
			data:params,
			callback : function(data) {
				
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
		
	}
	// 传递参数
	function getData(){
		var grid=mini.get("datagrid");
		var row = grid.getSelected();
		return row;
	}
	//清空
	function clear(){
		var form=new mini.Form("#search_form");
		form.clear();
		search(10,0);
	}
	//添加
	function addJob(){
		var url = CommonUtil.baseWebPath() +"/../Common/Job/MiniJobEdit.jsp?action=add";
		var tab = {"id": "MiniJobEdit",name:"MiniJobEdit",url:url,text:"定时任务添加",parentId:top["win"].tabs.getActiveTab().name};
		top["win"].showTab(tab);
	}
	//修改
	function editJob(){
		var grid=mini.get("datagrid");
		var row = grid.getSelected();
		if(row){
			var url = CommonUtil.baseWebPath() +"/../Common/Job/MiniJobEdit.jsp?action=edit";
			var tab = {"id": "MiniJobEdit",name:"MiniJobEdit",url:url,text:"定时任务修改",parentId:top["win"].tabs.getActiveTab().name};
			top["win"].showTab(tab);
		} else {
			mini.alert("请选中一条记录！","消息提示");
		}

	}
	//删除
	function deleteJob(){
		var grid=mini.get("datagrid");
		var rows=grid.getSelecteds();
		if(rows.length==0){
			mini.alert("请选中一行","提示");
			return;
		}
		var jobIds = new Array();
		$.each(rows, function(i, n){
			jobIds.push(n.jobId);
		});
		mini.confirm("您确认要删除选中记录？","确认", function(r){
			if (r=="ok"){
				var data=jobIds;
				params=mini.encode(data);
				CommonUtil.ajax( {
					url:"/JobController/removeJob",
					data:params,
					callback : function(data) {
						mini.alert("删除成功","系统提示");
						search(10,0);
					}
				});	
			}
		});

	}
	//分页查询任务日志
	function searchLog(pageSize,pageIndex,jobId){
		var data={};
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['jobId']=jobId;
		var params=mini.encode(data);
		//发送ajax请求
		CommonUtil.ajax({
			url:'/JobController/searchPageJobLogs',
			data:params,
			
			callback : function(data) {
				var grid=mini.get("jobLog");
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
				grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	//任务日志
	function onShowRowDetail(e){
		var grid=e.sender;
		var row=e.record;
		var td = grid.getRowDetailCellEl(row);
		var childGrid=mini.get("jobLog");
		var gridEl=childGrid.getEl();
		td.appendChild(gridEl);
		gridEl.style.display = "block";
		searchLog(10,0,row.jobId);
	}
	//查询任务队列
	function searchque(){
		var data={};
		params=mini.encode(data);
		CommonUtil.ajax( {
			url:"/JobController/searchJobQueue",
			data:params,
			callback : function(data) {
				mini.alert(data.desc,"系统提示");
			}
		});
	}
	//手动执行
	function execute(jobId){
		var record =grid.getSelected();
		if(record.isDisabled =='1'){
			if (jobId) {
				mini.confirm('您确定要手动执行任务吗？', '确认', function (r) {
					if (r == "ok") {
						var data = { "jobId": jobId };
						params = mini.encode(data);
						CommonUtil.ajax({
							url: "/JobController/execJob/",
							data: params,
							callback: function (data) {
								mini.alert(data.desc, "系统提示");
							}
						});
					}
				});
			}
		}else{
			mini.alert("停用状态不能启用","系统提示");
		}
		
		
	}
	//是否停用
	function isStop(jobId,bool){
		var data={checked:bool,jobId:jobId};
		params = mini.encode(data);
		CommonUtil.ajax( {
			url:"/JobController/switchJob/",
			data:params,
			callback : function(data) {
				mini.alert(data.desc,"系统提示");
			}
		});
	}
	/***************显示设置******************************/	
	function execJob(e){
		var row=e.record;
		var jobId=row.jobId;
		var s="<a class='mini-button' height=22 inabled=" + (row.isManual == "1" ? "true" : "false") + " onclick='execute(\"" + jobId + "\")' >执行</a>"
		return s;
	}
	function jobStatus(e){
		if("1"==e.value) return "执行中";
		return "待执行";
	}
	function yesNo(e){
		if("1"==e.value) return "是";
		return "否";
	}
	//启用
	function start(){
		var record =grid.getSelected();
		if(record){
			
			var data = { checked: true, jobId: record.jobId };
			params = mini.encode(data);
			CommonUtil.ajax({
				url: "/JobController/switchJob/",
				data: params,
				callback: function (data) {
					mini.alert(data.desc, "系统提示",function(){
						search(10, 0);
					});
				}
			});
		}else{
			mini.alert("请选择一条记录");
		}
		
	}
	//停用
	function stop(){
		var record =grid.getSelected();
		if(record){
			var data = { checked: false, jobId: record.jobId };
			params = mini.encode(data);
			CommonUtil.ajax({
				url: "/JobController/switchJob/",
				data: params,
				callback: function (data) {
					mini.alert(data.desc, "系统提示",function(){
						search(10, 0);
					});
				}
			});
		}else{
			mini.alert("请选择一条记录");
		}
	}
	
	
	$(document).ready(function(){
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
			//任务日志加载事件
			var grid = mini.get("jobLog");
			grid.on("beforeload", function (e) {
				var row = grid.getRow(0);
				e.cancel = true;
				var pageIndex = e.data.pageIndex;
				var pageSize = e.data.pageSize;
				searchLog(pageSize, pageIndex, row.jobId);
			});
			//任务列表加载事件
			var grid1 = mini.get("datagrid");
			grid1.on("beforeload", function (e) {
				e.cancel = true;
				var pageIndex = e.data.pageIndex;
				var pageSize = e.data.pageSize;
				search(pageSize, pageIndex);
			});
		});
	})
</script>
</body>
</html>