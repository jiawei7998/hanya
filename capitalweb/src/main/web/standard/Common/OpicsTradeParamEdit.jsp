<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>交易要素  维护</title>
  </head>

<body style="width:100%;height:100%;background:white">
    <div>
        <span id="labell"><a href="javascript:CommonUtil.activeTab();">交易要素</a> >> 修改</span>
        <div id="field" class="fieldset-body">
            <table id="field_form"  style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <tr>
                    <td style="width:50%">
                        <input id="groupMod" name="groupMod" class="mini-combobox"  
                        labelField="true"  label="模块" emptyText="请输入模块名称"
                        required="true"  vtype="maxLength:10" style="width:100%"
                        labelStyle="text-align:left;width:170px"onvalidation="onEnglishAndNumberValidation"
                        data="CommonUtil.serverData.dictionary.model"/>
                        <input id="groupId" name="groupId" class="mini-hidden"/>
                        <input id="inputTime" name="inputTime" class="mini-hidden"/>
                        <input id="lastTime" name="lastTime" class="mini-hidden"/>
                    </td>
                    <td style="width:50%">
                        <input id="groupName" name="groupName" class="mini-textbox" emptyText="请输入组合名称"
                        labelField="true"  label="组合名称" required="true"  style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:20"/>
                    </td>
                    
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="port" name="port" class="mini-buttonedit" emptyText="请输入投资组合名称"
                        labelField="true"  label="投资组合" required="true"  style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:4" onvalidation="onEnglishAndNumberValidation"
                        onbuttonclick="onPortEdit"/>
                    </td>
                    <td style="width:50%">
                        <input id="product" name="product" class="mini-buttonedit" 
                        labelField="true"  label="产品代码"  emptyText="请输入产品代码"
                        required="true"  vtype="maxLength:10" style="width:100%"
                        labelStyle="text-align:left;width:170px"onvalidation="onEnglishAndNumberValidation"
                        onbuttonclick="onPrdEdit" onValueChanged="onPrdChanged"/>
                    </td>
                </tr>
                <tr>
                    
                    <td style="width:50%">
                        <input id="prodType" name="prodType" class="mini-buttonedit"  emptyText="请输入产品类型"
                        labelField="true"  label="产品类型" required="true"  style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:2" onvalidation="onEnglishAndNumberValidation"
                        onbuttonclick="onTypeEdit" />
                    </td>
                    <td style="width:50%">
                        <input id="cost" name="cost" class="mini-buttonedit" emptyText="请输入成本中心"
                        labelField="true"  label="成本中心" required="true"  style="width:100%" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:15" onvalidation="onEnglishAndNumberValidation"
                        onbuttonclick="onCostEdit" />
                    </td>
                </tr>
                <tr>
                    <td style="width:50%">
                        <input id="trader" name="trader" class="mini-textbox"  
                        labelField="true"  label="交易员" emptyText="请输入交易员"
                        required="true"  vtype="maxLength:20" style="width:100%"
                        labelStyle="text-align:left;width:170px"onvalidation="onEnglishAndNumberValidation"/>
                    </td>
                    <td style="width:50%">
                        <input id="note" name="note" class="mini-textarea" emptyText="请输入备注"
                        labelField="true"  label="备注" required="true" style="width:100%;height:29px" labelStyle="text-align:left;width:170px"
                        vtype="maxLength:50"/>
                    </td>
                    
                </tr>
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        mini.parse();
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");

        $(document).ready(function(){
            initData();
        }); 

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){//修改、详情
            	mini.get("port").setValue(row.port);
            	mini.get("port").setText(row.port);
            	mini.get("product").setValue(row.product);
            	mini.get("product").setText(row.product);
            	mini.get("prodType").setValue(row.prodType);
            	mini.get("prodType").setText(row.prodType);
            	mini.get("cost").setValue(row.cost);
            	mini.get("cost").setText(row.cost);
            	
            	
                form.setData(row);
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易要素</a> >> 详情");
                }
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易要素</a> >> 新增");
            }
        }
        //保存
        function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                return;
            }

            var data=form.getData();
            var params=mini.encode(data);
            CommonUtil.ajax({
                url:"/IfsTradeParamController/saveTradeParam",
                data:params,
                callback:function(data){
                    mini.alert("保存成功",'提示信息',function(){
                        top["win"].closeMenuTab();
                    });
                }
		    });
        }

        function cancel(){
            top["win"].closeMenuTab();
        }

        //英文、数字、下划线 的验证
        function onEnglishAndNumberValidation(e) {
            if(e.value == "" || e.value == null){//值为空，就不做校验
                return;
            }
            if (e.isValid) {
                if (isEnglishAndNumber(e.value) == false) {
                    e.errorText = "必须输入英文或数字";
                    e.isValid = false;
                }
            }
        }

        /* 是否英文、数字、下划线 */
        function isEnglishAndNumber(v) {
            var re = new RegExp("^[0-9a-zA-Z\_\-]+$");
            if (re.test(v)) return true;
            return false;
        }
        
        /* 产品代码的选择 */
        function onPrdEdit(){
        	var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/prodMini.jsp",
                title: "选择产品代码",
                width: 700,
                height: 600,
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.pcode);
                            btnEdit.setText(data.pcode);
                            btnEdit.focus();
                        }
                    }

                }
            });
        	
        }
        
        function onPrdChanged(){
        	mini.get("prodType").setValue("");
        	mini.alert("产品代码已改变，请重新选择产品类型!");
        	
        }
        
        /* 产品类型的选择 */
        function onTypeEdit(){
        	var prd = mini.get("product").getValue();
        	if(prd == null || prd == ""){
        		mini.alert("请先选择产品代码!");
        		return;
        	}
        	var data={prd:prd};
        	var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/typeMini.jsp",
                title: "选择产品类型",
                width: 700,
                height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.type);
                            btnEdit.setText(data.type);
                            btnEdit.focus();
                        }
                    }

                }
            });
        	
        	
        }
        
        /* 成功中心的选择 */
        function onCostEdit(){
        	var btnEdit = this;
            mini.open({
                url: CommonUtil.baseWebPath() + "/opics/costMini.jsp",
                title: "选择成本中心",
                width: 700,
                height: 600,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    iframe.contentWindow.SetData({});
                },
                ondestroy: function (action) {
                    if (action == "ok") {
                        var iframe = this.getIFrameEl();
                        var data = iframe.contentWindow.GetData();
                        data = mini.clone(data);    //必须
                        if (data) {
                            btnEdit.setValue(data.costcent);
                            btnEdit.setText(data.costcent);
                            btnEdit.focus();
                        }
                    }

                }
            });
        	
        }
        
        /* 投资组合的选择 */
       function onPortEdit(){
    	   var btnEdit = this;
           mini.open({
               url: CommonUtil.baseWebPath() + "/opics/portMini.jsp",
               title: "选择投资组合",
               width: 700,
               height: 600,
               onload: function () {
                   var iframe = this.getIFrameEl();
                   iframe.contentWindow.SetData({});
               },
               ondestroy: function (action) {
                   if (action == "ok") {
                       var iframe = this.getIFrameEl();
                       var data = iframe.contentWindow.GetData();
                       data = mini.clone(data);    //必须
                       if (data) {
                           btnEdit.setValue(data.portfolio);
                           btnEdit.setText(data.portfolio);
                           btnEdit.focus();
                       }
                   }

               }
           });
        	
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

    </script>
</body>
</html>