<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../global.jsp"%>
<html>
  <head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>字段限额  维护</title>
  </head>
  <body style="width:100%;height:100%;background:white">
    <div>
        <div id="field" class="fieldset-body">
            <table id="field_form"   style="text-align:left;margin:auto;width:100%;" class="mini-sltable">
                <h1 style="text-align:left;" id="labell"><a href="javascript:CommonUtil.activeTab();">字段限额</a> >> 修改</h1>
                <tr>
                    <td style="width:25%">
                      <input id="type" name="type" class="mini-combobox" labelField="true" label="操作类型："  emptyText="请选择操作类型" onValueChanged="queryProduct" data="CommonUtil.serverData.dictionary.OperatorType" required="true"   style="width:100%" labelStyle="text-align:left;width:170px" />
                    </td>
                    <td style="width:25%">
                        <input id="prd" name="prd" class="mini-combobox" label="产品类型" onvaluechanged="onPrdChanged()" emptyText="请选择产品类型" labelField="true"   valueField="TABLE_NAME" textField="COMMENTS"  style="width:100%" labelStyle="text-align:left;width:170px" vtype="maxLength:100" required="true" />
                    </td>
                </tr>
                <tr>  
                    <td style="width:50%" id="append">
                        <input id="dictId" name="dictId" class="mini-combobox"  label="字典值" labelField="true"  emptyText="请输入字典值" onvaluechanged="dictIdChanged" valueField="DICT_ID" textField="DICT_NAME" vtype="maxLength:100" required="true"   style="width:100%"labelStyle="text-align:left;width:170px"/>
                        <input id="inputTime" name="inputTime" class="mini-hidden"  label="录入时间" labelField="true"  />
                    </td>
                    <td style="width:50%" >
                        <input id="limitId" name="limitId" class="mini-hidden"  label="字段限额编号" labelField="true"   emptyText="系统自动生成" enabled="false"  vtype="maxLength:20" style="width:100%"labelStyle="text-align:left;width:170px"/>
                        <input id="colum" name="colum" class="mini-combobox"  label="限额字段" enabled="false" emptyText="请选择限额字段" labelField="true"  valueField="COLUMN_NAME" textField="COMMENTS"   required="true"    style="width:100%"labelStyle="text-align:left;width:170px" />
                    </td>
               </tr>
               
               <tr>
                    <td style="width:50%" >
                        <input id="dictTran" name="dictTran" class="mini-textbox" labelField="true"  label="字典值中文名称"   emptyText="请输入字典值中文名称"  vtype="maxLength:100" required="true"    style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
                    <td style="width:50%">
                        <input id="lastTime" name="lastTime" class="mini-hidden"  label="上次修改时间" labelField="true"  />        
                        <input id="dictId1Tran" name="dictId1Tran" class="mini-textbox"  label="字典具体值中文名称" labelField="true"  emptyText="请输入字典具体值中文名称" data="" vtype="maxLength:100" required="true"    style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
               </tr> 
               <tr>   
                    <td style="width:50%">
                        <input id="dictId1" name="dictId1" class="mini-textbox"  label="字典具体值" labelField="true"  emptyText="请输入字典具体值" data="" vtype="maxLength:100" required="true"    style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
                    <td style="width:50%">
                        <input id="remark" name="remark" class="mini-textbox"  label="备注" labelField="true"  emptyText="输入备注" data="" vtype="maxLength:100" required="true"    style="width:100%"labelStyle="text-align:left;width:170px"/>
                    </td>
               </tr> 
               
                <tr>
                    <td colspan="2" style="text-align:center;">
                        <a  id="save_btn" class="mini-button" style="display: none"    onclick="save">保存</a>
                        <a  id="cancel_btn" class="mini-button" style="display: none"    onclick="cancel">取消</a>
                    </td>
                </tr> 
            </table>
           
        </div> 
    </div>
  </body>
  <script type="text/javascript">
        mini.parse();
        var branchId ='<%=__sessionUser.getBranchId()%>';
        //获取当前tab
        var currTab = top["win"].tabs.getActiveTab();
        var params = currTab.params;
        var row=params.selectData;
        var url=window.location.search;
        var action=CommonUtil.getParam(url,"action");
        var form=new mini.Form("#field_form");
  
        $(document).ready(function(){
            initData();
            loadDictList();
        }); 

        //初始化数据
        function initData(){
            if($.inArray(action,["edit","detail"])>-1){//修改、详情
                form.setData(row);
                //mini.get("colum").setEnabled("true");
                if(action == "detail"){
                    $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>限额字段</a> >> 详情");
                    form.setEnabled(false);
                    mini.get("save_btn").setVisible(false);
                }
            }else{//增加 
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>限额字段</a> >> 新增");
            }
        }

        //保存
        function save(){
            //表单验证！！！
            form.validate();
            if (form.isValid() == false) {
                mini.alert("表单填写有误!","提示");
                return;
            }
            var saveUrl=null;
            if(action == "add"){
                saveUrl = "/IfsFieldLimitController/saveFieldLimitAdd";
            }else if(action == "edit"){
                saveUrl = "/IfsFieldLimitController/saveFieldLimitEdit";
            }

            var data=form.getData(true);
            var params=mini.encode(data);
            CommonUtil.ajax({
                url:saveUrl,
                data:params,
                callback:function(data){
                    mini.alert("保存成功",'提示信息',function(){
                        top["win"].closeMenuTab();
                    });
                }
		    });
        }

        //取消
        function cancel(){
            top["win"].closeMenuTab();
        }

        //加载产品列表
        
        function queryProduct(){
        	var params = mini.get("type").getValue();
        	window.alert(params);
        	 var proUrl=null;
        	if(params =="IFS_CFET"){
        	  proUrl="/IfsFieldLimitController/searchTables";
        	}else if(params =="IFS_APPRO"){
              //proUrl="/IfsLimitController/likeSearchTables";
        	  proUrl="/IfsFieldLimitController/searchTables";
        	}
        	
            CommonUtil.ajax({
            	data:{table:params,branchId:branchId},
                url:proUrl,
                callback:function(data){
                    var prdTypeNo = mini.get("prd");
                    prdTypeNo.setData(data.obj);
                }
            });	
        }

        //产品类型改变时发生
        function onPrdChanged(e){
            mini.get("colum").setEnabled("true");
            var t = e.value;
           /*  var tableName = "";
            if(value=="441"){ //利率互换
                tableName = "IFS_CFETSRMB_IRS"
            }else if(value=="411"){//外汇即期
                tableName = ""
            }else if(value=="391"){//外汇远期
                tableName = ""
            }else if(value=="432"){//外汇掉期
                tableName = ""
            }else if(value=="431"){//货币掉期
                tableName = ""
            }else if(value=="433"){//人民币期权
                tableName = ""
            }else if(value=="438"){//外币拆借
                tableName = ""
            }else if(value=="439"){//外币头寸调拨
                tableName = ""
            }else if(value=="440"){//外汇对即期
                tableName = ""
            }else if(value=="442"){//买断式回购
                tableName = ""
            }else if(value=="443"){//现券买卖
                tableName = ""
            }else if(value=="444"){//信用拆借
                tableName = ""
            }else if(value=="445"){//债券借贷
                tableName = ""
            }else if(value=="446"){//质押式回购
                tableName = ""
            }else if(value=="447"){//黄金即期
                tableName = ""
            }else if(value=="448"){//黄金远期
                tableName = ""
            }else if(value=="449"){//黄金掉期
                tableName = ""
            }else if(value=="450"){//黄金拆借
                tableName = ""
            }else if(value=="451"){//外币债
                tableName = ""
            } */

            CommonUtil.ajax({
                data:{tableName:value},
                url:"/IfsFieldLimitController/searchColumns",
                callback:function(data){
                    var colum = mini.get("colum");
                    colum.setData(data.obj);
                }
            });	
            


        }

        //加载字典表
        function loadDictList(){
            CommonUtil.ajax({
                data:{},
                url:"/IfsFieldLimitController/searchDict",
                callback:function(data){
                    var dict_Id = mini.get("dictId");
                    dict_Id.setData(data.obj);
                }
            });	
        }

        function dictIdChanged(e){
            var data =mini.get("dictId1");
            data.setData("CommonUtil.serverData.dictionary."+e.value);
            //$("#dictId1").attr("data","CommonUtil.serverData.dictionary."+e.value ); $("#dictId1").

        }

        
  
  
  
  </script>


</html>