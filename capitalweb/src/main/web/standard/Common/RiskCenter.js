function onLimitRisk(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/LimitRisk.jsp",
        title: "审查单号列表",
        width: 700,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if (data) {
                    btnEdit.setValue(data.approveFlno);
                    btnEdit.setText(data.approveFlno);
                    mini.get("applyProd").setValue(data.productName);
                    btnEdit.focus();
                }
            }

        }
    });
}

//查询权重值
function searchProductWeight(pageSize,pageIndex,prdNo,mDate){
	var form = new mini.Form("field_form");
	var data=form.getData(true);
	data['pageNumber']=pageIndex+1;
	data['pageSize']=pageSize;
	//data['branchId']=branchId;
	data['productCode']=prdNo;
	var params = mini.encode(data);	
	
	CommonUtil.ajax({
		url:"/TdCustEcifController/searchSingleProtectName",
		data:params,
		callback : function(data) {
			//grid.setData(data.obj.rows);
			var totalNum = data.obj.rows;
			var weight = '100';
			if(totalNum.length != 0){
				var curDate = totalNum[0].curDate;//表TT_DAYEND_DATE当前日期
				var totalDay;
				var containsMax;
				var containsMin;
				var ruleMax;
				var ruleMin;
				
				
				for(var i=0,l=totalNum.length;i<l;i++){
					
					ruleMax = parseFloat(totalNum[i].ruleMax);
					ruleMin = parseFloat(totalNum[i].ruleMin);
					containsMax = totalNum[i].containsMax;
					containsMin = totalNum[i].containsMin;
					//dueDate = totalNum[0].dueDate;					
					//totalDay = parseFloat(getDays(dueDate,  curDate));
					totalDay = parseFloat(getDays(mDate,curDate));
					
					if(ruleMax == 0){
						if(totalDay > ruleMin){
							weight = totalNum[i].weight;
							break;
						}else if(totalDay == ruleMin && containsMin == '1'){
							weight = totalNum[i].weight;
							break;
						}
					}
					if(ruleMin == 0){
						if(totalDay < ruleMax){
							weight = totalNum[i].weight;
							break;
						}else if(totalDay == ruleMax && containsMax == '1'){
							weight = totalNum[i].weight;
							break;
						}
					}
					if(totalDay < ruleMax && totalDay > ruleMin){
						weight = totalNum[i].weight;
						break;
					}
					if(totalDay == ruleMin && containsMin == '1'){
						weight = totalNum[i].weight;
						break;
					}
					if(totalDay == ruleMax && containsMax == '1'){
						weight = totalNum[i].weight;
						break;
					}
				}
			}
			mini.get("weight").setValue(weight);
		}
	});
}

//计算两个日期之间相差天数
function getDays(date1 , date2){
	var date1Str = date1.split("-");//将日期字符串分隔为数组,数组元素分别为年-月-日
	//根据年 . 月 . 日的值创建Date对象
	var date1Obj = new Date(date1Str[0],(date1Str[1]-1),date1Str[2]);
	var date2Str = date2.split("-");
	var date2Obj = new Date(date2Str[0],(date2Str[1]-1),date2Str[2]);
	var t1 = date1Obj.getTime();
	var t2 = date2Obj.getTime();
	var dateTime = 1000*60*60*24; //每一天的毫秒数
	var minusDays = Math.floor(((t2-t1)/dateTime));//计算出两个日期的天数差
	var days = Math.abs(minusDays);//取绝对值
	return days;
}

//查询占用授信主体信息
function searchCustNoEdit(e) {
    var btnEdit = this;
    mini.open({
        url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
        title: "选择对手方列表",
        width: 900,
        height: 600,
        ondestroy: function (action) {
            if (action == "ok") {
                var iframe = this.getIFrameEl();
                var data = iframe.contentWindow.GetData();
                data = mini.clone(data);    //必须
                if (data) {
                    btnEdit.setValue(data.cno);
                    btnEdit.setText(data.cfn);
                    searchProductWeight(20,0,prdNo);
                    btnEdit.focus();
                }
            }

        }
    });
}
//获取授信主体名称
function queryCustNo(cno){
	CommonUtil.ajax({
	    url: "/IfsOpicsCustController/searchIfsOpicsCust",
	    data : {'cno' : cno},
	    callback:function (data) {
	    	if (data && data.obj) {
	    		mini.get("custNo").setValue(data.obj.cno);//占用授信主体
    			mini.get("custNo").setText(data.obj.cliname);//占用授信主体
			} else {
				mini.get("custNo").setValue("");
				mini.get("custNo").setText("");
	    }
	    }
	});
}

//获取DV01，久期限，止损值查询
function queryMarketData(code,costCent){
	CommonUtil.ajax({
	    url: "/IfsOpicsCustController/queryMarketData",
	    data : {'code':code,'costCent':costCent},
	    callback:function (data) {
	    	if (data && data.obj) {
	    		mini.get("longLimit").setValue(data.obj.duration);//久期限
    			mini.get("lossLimit").setValue(data.obj.convexity);//止损
    			mini.get("DV01Risk").setValue(data.obj.delta);//DV01
	    	} else {
	    		mini.get("longLimit").setValue();//久期限
    			mini.get("lossLimit").setValue();//止损
    			mini.get("DV01Risk").setValue();//DV01
	    }
	    }
	});
}
