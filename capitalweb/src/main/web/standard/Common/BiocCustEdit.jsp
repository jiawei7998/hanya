<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%-- <%@ page language="java" pageEncoding="UTF-8"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="../global.jsp"%>
<%
	SlSession slSession = SessionStaticUtil.getSlSessionByHttp(request);
	TaUser sessionUser = SlSessionHelper.getUser(slSession)==null?new TaUser():SlSessionHelper.getUser(slSession);
%>

<html>
<head>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <title>集团客户  维护</title>
</head>
<body style="width:100%;height:100%;background:white">
<div class="mini-splitter" style="width:100%;height:100%;">
	<div size="90%" showCollapseButton="false">
		<div id="field_form" class="mini-fit area" style="background:white">
			<div id="ecif" class="mini-panel" title="基本信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">

					<%--<input id="clino" name="clino" class="mini-textbox  mini-mustFill" label="核心客户号" emptyText="请输入核心客户号" required="true"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:10"
						onvalidation="onEnglishAndNumberValidation"/>
					<a id="query" class="mini-button" style="display: none"  style="width:40px;" onclick="queryCustInfo()">同步</a>
					<a id="build" class="mini-button" style="display: none"  style="width:50px;" onclick="build()">新建</a>
					<a id="alter" class="mini-button" style="display: none"  style="width:50px;" onclick="alter()">修改</a>
					<input id="tellerno" name="tellerno" class="mini-textbox"  label="客户建档柜员号" emptyText="请输入客户建档柜员号" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"
						onvalidation="onEnglishAndNumberValidation"/>

					<input id="regplace" name="regplace" class="mini-combobox" label="注册国家" emptyText="请输入注册国家" allowInput="true"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40" showNullItem="true"
						data="CommonUtil.serverData.dictionary.newRegplace"/>--%>

					
					<input id="shtype" name="shtype" class="mini-combobox mini-mustFill" label="唯一标识代码类型" emptyText="请选择唯一标识代码类型" labelField="true" 
						data="CommonUtil.serverData.dictionary.GidType" style="width:100%" labelStyle="text-align:center;width:170px"  required="true"
						vtype="maxLength:12" onvalidation="onEnglishAndNumberValidation" showNullItem="true"/>
					<input id="institucode" name="institucode" class="mini-textbox mini-mustFill"  label="客户金融机构编码" emptyText="请输入客户金融机构编码" required="true"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"  />
					<input id="indtype" name="indtype" class="mini-combobox" label="行业类型" emptyText="请选择行业类型" labelField="true" 
						data="CommonUtil.serverData.dictionary.industtype" style="width:100%" labelStyle="text-align:center;width:170px" 
						vtype="maxLength:12" onvalidation="onEnglishAndNumberValidation"/>
					<input id="flag" name="flag" class="mini-combobox" label="境内外标识" emptyText="请选择境内外标识" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.YesNo"/>
					<input id="cliname" name="cliname" class="mini-textbox mini-mustFill"  label="客户中文名称" emptyText="请输入客户中文名称"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"  required="true" />
						
				</div>
				<div class="rightarea">

					<%--<input id="orgno" name="orgno" class="mini-textbox"  label="客户建档机构号" emptyText="请输入客户建档机构号"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"
						onvalidation="onEnglishAndNumberValidation"/>--%>

						
					<input id="shcode" name="shcode" class="mini-textbox mini-mustFill" label="唯一标识代码"  emptyText="请输入唯一标识代码"  required="true"
						labelField="true"  style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40" 
						onvalidation="onEnglishAndNumberValidation"/>
					<input id="custclass" name="custclass" class="mini-combobox" label="是否为集团客户" emptyText="请选择是否为集团客户" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.YesNo" value='1' enabled="false"/>
					<input id="fintype" name="fintype" class="mini-combobox" label="同业客户类型" emptyText="请选择同业客户类型" labelField="true"  
						data="CommonUtil.serverData.dictionary.newFintype" style="width:100%" labelStyle="text-align:center;width:170px" 
						vtype="maxLength:12" onvalidation="onEnglishAndNumberValidation" showNullItem="true"/> 
					<input id="subjectType" name="subjectType" class="mini-combobox mini-mustFill" label="主体类型" emptyText="请选择主体类型"
						data="CommonUtil.serverData.dictionary.ztType" labelField="true" style="width:100%" required="true"
						labelStyle="text-align:center;width:170px" vtype="maxLength:12"/>
					<input id="clitype" name="clitype" class="mini-combobox mini-mustFill" label="客户类型" emptyText="请选择客户类型" labelField="true"
						   data="CommonUtil.serverData.dictionary.clitype" style="width:100%" labelStyle="text-align:center;width:170px" required="true"
						   vtype="maxLength:12" onvalidation="onEnglishAndNumberValidation" showNullItem="true"/> <%--onitemclick="judge()"--%>
				</div>
			</div>
			<div id="opponent" class="mini-panel" title="OPICS信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="cno" name="cno" class="mini-textbox mini-mustFill"  label="交易对手编号" labelField="true"
						emptyText="系统自动生成"  vtype="maxLength:10" style="width:100%" Enabled="false"
						labelStyle="text-align:center;width:170px" onvaluechanged="checkcust" onvalidation="onEnglishAndNumberValidation"/>
					<input id="cfn" name="cfn" class="mini-textbox mini-mustFill"  label="交易对手全称" labelField="true" required="true"
						emptyText="请输入交易对手全称"  vtype="maxLength:35" style="width:100%" labelStyle="text-align:center;width:170px"/>
					<input id="bic" name="bic" class="mini-combobox mini-mustFill"  label="SWIFT编码" emptyText="请选择SWIFT编码"  labelField="true" 
						textField="typeValue" valueField="typeId" value="typeId" style="width:100%" required="true"
						labelStyle="text-align:center;width:170px" allowInput="true" onvalidation="onComboValidation"/>
					<input id="uccode" name="uccode" class="mini-combobox mini-mustFill" label="国家代码" emptyText="请输入国家代码" labelField="true"
					 	vtype="maxLength:12" required="true"  textField="typeValue" valueField="typeId" value="typeId" style="width:100%"
					 	labelStyle="text-align:center;width:170px" allowInput="true" onvalidation="onComboValidation"/>
					<input id="location" name="location" class="mini-textbox mini-mustFill" label="经营地城市代码" emptyText="请输入经营地城市代码" labelField="true"
						required="true"   style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:35"/>
					<input id="ctype" name="ctype" class="mini-combobox mini-mustFill" required="true"  label="opics客户类型" emptyText="请选择opics客户类型" labelField="true"
						data="CommonUtil.serverData.dictionary.CType" style="width:100%" labelStyle="text-align:center;width:170px"
						allowInput="false" onvalidation="onComboValidation" vtype="maxLength:60"/>
					<input id="lstmntdate" name="lstmntdate" class="mini-datepicker" label="维护更新日期" emptyText="请选择维护更新日期"
						labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px"/>
					<input id="ratinfo" name="ratinfo" class="mini-textbox"  label="备注" emptyText="请输入备注" labelField="true"
						   style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:33"/>
					<input id="remark2" name="remark2" class="mini-textarea" label="备注2" emptyText="请输入备注2" labelField="true"
						   vtype="maxLength:100" style="width:100%;height:29px" labelStyle="text-align:center;width:170px"/>
				</div>
				<div class="rightarea">
					<input id="cname" name="cname" class="mini-textbox mini-mustFill" label="交易对手代码" emptyText="请输入交易对手代码" 
						labelField="true" labelStyle="text-align:center;width:170px" vtype="maxLength:10" 
						style="width:100%" onvalidation="onEnglishAndNumberValidation" required="true" />
					<input id="sn" name="sn" class="mini-textbox mini-mustFill" label="交易对手简称" required="true"  emptyText="请输入交易对手简称"
						labelField="true"  style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40" />
					<input id="sic" name="sic" class="mini-combobox mini-mustFill" label="工业标准代码" emptyText="请选择工业标准代码" labelField="true" 
						style="width:100%" labelStyle="text-align:center;width:170px" textField="typeValue" valueField="typeId"
						onvalidation="onComboValidation" allowInput="true" vtype="maxLength:12" required="true"
						onvalidation="onEnglishAndNumberValidation"/>
					<input id="ccode" name="ccode" class="mini-combobox mini-mustFill"  label="注册地代码" labelField="true"  emptyText="请输入注册地代码" 
						vtype="maxLength:12" required="true"  textField="typeValue" valueField="typeId" allowInput="true"
						onvalidation="onComboValidation" style="width:100%" labelStyle="text-align:center;width:170px"/>
					<input id="acctngtype" name="acctngtype" class="mini-buttonedit mini-mustFill" onbuttonclick="onActyQuery" allowInput="false"
						required="true"  label="会计类型" emptyText="请选择会计类型" textField="typeValue" valueField="typeId" labelField="true"
						style="width:100%" labelStyle="text-align:center;width:170px"/>
					<input id="taxid" name="taxid" class="mini-textbox"  label="纳税人编号" labelField="true"  emptyText="请输入纳税人编号"
						vtype="maxLength:15" style="width:100%"labelStyle="text-align:center;width:170px"
						onvalidation="onEnglishAndNumberValidation" visible="false"/>
					<input id="cpost" name="cpost" class="mini-textbox"  label="邮编" labelField="true"  emptyText="请输入邮编"  
						vtype="maxLength:10" onvalidation="onEnglishAndNumberValidation" style="width:100%"
						labelStyle="text-align:center;width:170px"/>
					<input id="ca" name="ca" class="mini-textbox" label="公司地址" emptyText="请输入公司地址" labelField="true" style="width:100%"
						   labelStyle="text-align:center;width:170px" vtype="maxLength:70"/>
					<input id="remark1" name="remark1" class="mini-textarea"  label="备注1" labelField="true"  emptyText="请输入备注1"
						   vtype="maxLength:20" style="width:100%;height:29px"labelStyle="text-align:center;width:170px"/>
				</div>
			</div>
			<div id="credit" class="mini-panel" title="授信主体信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="creditsub" name="creditsub" class="mini-combobox" label="是否为授信主体" emptyText="请选择是否为授信主体"
						   labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true" onitemclick="creditChange()"
						   data = "CommonUtil.serverData.dictionary.YesNo"/>
					<input id="creditsubnm" name="creditsubnm" class="mini-textbox" label="授信主体名称" emptyText="请输入授信主体名称" enabled="false"
						   labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:12"/>
				</div>
				<div class="rightarea">
					<input id="creditsubno" name="creditsubno" class="mini-buttonedit" allowInput="false"
						   label="授信主体编号" emptyText="请选择授信主体编号"  labelField="true" onbuttonclick="onButtonEdit" enabled="false"
						   style="width:100%" labelStyle="text-align:center;width:170px"/>
				</div>
			</div>
			<div id="other" class="mini-panel" title="其他信息" style="width:100%" allowResize="true" collapseOnTitleClick="true">
				<div class="leftarea">
					<input id="founddate" name="founddate" class="mini-datepicker" label="成立日期" emptyText="请选择成立日期"
						labelField="true"   style="width:100%" labelStyle="text-align:center;width:170px"/>
					<input id="ecing" name="ecing" class="mini-combobox" label="客户经济成分" emptyText="请选择客户经济成分" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.CustEcing"/>
					<input id="cre" name="cre" class="mini-spinner mini-mustFill input-text-strong"  label="客户信用级别总等级数" emptyText="请输入客户信用级别总等级数" required="true"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"  />
					<input id="gtype" name="gtype" class="mini-combobox" label="证件类型" emptyText="请选择证件类型" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.GType"/>
					<input id="comscale" name="comscale" class="mini-combobox" label="客户企业规模" emptyText="请选择客户企业规模" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.CustComScale"/>
					<input id="ratype" name="ratype" class="mini-combobox" label="评级类型" emptyText="请选择评级类型" labelField="true" 
						data="CommonUtil.serverData.dictionary.ratype" style="width:100%" labelStyle="text-align:center;width:170px" 
						vtype="maxLength:12" onvalidation="onEnglishAndNumberValidation" onitemclick="change()"/>
					<input id="extrainfo" name="extrainfo" class="mini-combobox" label="外部评级公司" emptyText="请选择外部评级公司"
						data="CommonUtil.serverData.dictionary.outCompany" labelField="true" style="width:100%"
						labelStyle="text-align:center;width:170px" vtype="maxLength:12"/>
					<input id="interinfo" name="interinfo" class="mini-combobox" label="内部评级公司" emptyText="请选择内部评级公司"
						data="CommonUtil.serverData.dictionary.outCompany" labelField="true" style="width:100%"
						labelStyle="text-align:center;width:170px" vtype="maxLength:12"/>
					<input id="cfetsno" name="cfetsno" class="mini-textbox" label="CFETS本币机构编码（6位）" emptyText="请输入CFETS本币机构编码"
						onvalidation="onEnglishAndNumberValidation" labelField="true" style="width:100%"
						labelStyle="text-align:center;width:170px" vtype="maxLength:21"/>
					<input id="holdtype" name="holdtype" class="mini-combobox" label="控股类型" emptyText="请选择控股类型" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.CustHoldType"/>
					<input id="prdManager" name="prdManager" class="mini-textbox" label="产品管理人" emptyText="请输入产品管理人"
						   labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:12"/>
					<input id="transType" name="transType" class="mini-combobox" label="客户来源渠道" emptyText="请选择客户来源渠道"
						   labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						   data = "CommonUtil.serverData.dictionary.custTransType"/>
				</div>
				<div class="rightarea">
					<input id="isacc" name="isacc" class="mini-combobox" label="是否关联方" emptyText="请选择是否关联方" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.YesNo"/>
					<input id="ecotype" name="ecotype" class="mini-combobox" label="客户国民经济部门类型" emptyText="请选择客户国民经济部门类型" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.CustEcotype"/>
					<input id="crelevel" name="crelevel" class="mini-spinner mini-mustFill input-text-strong"  label="客户信用评级" emptyText="请输入客户信用评级" required="true"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"  />
					<input id="gid" name="gid" class="mini-textbox mini-mustFill"  label="客户证件代码" emptyText="请输入客户证件代码" required="true"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"  />
					<input id="ruciflag" name="ruciflag" class="mini-combobox" label="农村城市标志" emptyText="请选择农村城市标志" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.CustRuciFlag"/>
					<input id="license" name="license" class="mini-textbox mini-mustFill"  label="客户金融许可证号" emptyText="请输入客户金融许可证号" required="true"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:40"  />
					<input id="ratresult1" name="ratresult1" class="mini-combobox" showNullItem="true"  label="外部评级结果"
						emptyText="请输入外部评级结果" labelField="true" style="width:100%" labelStyle="text-align:center;width:170px"
						data = "CommonUtil.serverData.dictionary.Rating"/>
					<input id="ratresult2" name="ratresult2" class="mini-combobox" label="内部评级结果" emptyText="请输入内部评级结果" 
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" showNullItem="true"
						data = "CommonUtil.serverData.dictionary.Rating"/>
					<input id="cfetscn" name="cfetscn" class="mini-textbox" label="CFETS外币机构编码（21位）" emptyText="请输入CFETS外币机构编码"
						labelField="true" style="width:100%" labelStyle="text-align:center;width:170px"
						onvalidation="onEnglishAndNumberValidation" vtype="maxLength:12"/>
					<input id="remark3" name="remark3" class="mini-textbox" label="备注" emptyText="请输入备注"
						   labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:12"/>
					<input id="trustee" name="trustee" class="mini-textbox" label="托管人信息" emptyText="请输入托管人信息"
						   labelField="true" style="width:100%" labelStyle="text-align:center;width:170px" vtype="maxLength:12"/>
				</div>
			</div>
		</div>
	</div>
<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
	<div style="margin-bottom:10px; text-align: center;">
		<a id="save_btn" class="mini-button" style="display: none"  style="width:125px;" onclick="save(1)">保&nbsp;&nbsp;&nbsp;&nbsp;存</a>
	</div>
	<div style="margin-bottom:10px; text-align: center;">
		<a id="saveAndSync_btn" class="mini-button" style="display: none"  width="125px" onclick="save(2)">保存并同步至OPICS</a>
	</div>
	<div style="margin-bottom:10px; text-align: center;">
		<a id="cancel_btn" class="mini-button" style="display: none"  style="width:125px;" onclick="cancel">取&nbsp;&nbsp;&nbsp;&nbsp;消</a>
	</div>
  </div>
</div>	
<script type="text/javascript">
    mini.parse();
    //获取当前tab
    var currTab = top["win"].tabs.getActiveTab();
    var params = currTab.params;
    var row=params.selectData;
    var url=window.location.search;
    var action=CommonUtil.getParam(url,"action");
    var form=new mini.Form("#field_form");

    // 动态加载金融机构类型
    $(document).ready(function(){
        var appendRedstar="<i class='red-star'></i>";
        $(".mini-mustFill label").append(appendRedstar);
        var fintype="fintype";
        var param = mini.encode({"dictId":fintype}); //序列化成JSON
        CommonUtil.ajax({
            url:"/TaDictController/getTadictTree",
            data:param,
            callback:function(data){
            	/* mini.get("fintype").loadList(data, "id", "pid");
            	if(($.inArray(action,["edit","detail"])>-1)){ // 修改以及查看详情时，将金融机构类型带入
            		mini.get("fintype").setValue(row.fintype);
            	} */
            }
 		 });
        initData();
    });
    
    //初始化数据
    function initData(){
    	$(document).ready(function(){
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().parentId)).done(function(visibleBtn){
				queryUCcode();
				querySic();
				querySico();
			});
    	});
//     	mini.get("ctype").setValue("B"); //交易对手默认银行
    	mini.get("lstmntdate").setValue(new Date()); //默认当前日期
    	var tellerno ='<%=sessionUser.getEmpId()%>';
    	var orgno ='<%=sessionUser.getInstId()%>';
    	//mini.get("tellerno").setValue(tellerno); // 默认柜员号
    	//mini.get("orgno").setValue(orgno); // 默认机构号
        if($.inArray(action,["edit","detail"])>-1){ //修改、详情
        	//mini.get("build").setVisible(false);
        	//mini.get("clino").setWidth("80%");
        	mini.get("cno").setEnabled(false); // 交易对手编号不允许修改
            form.setData(row);
			mini.get("creditsubno").setValue(row.creditsubno);
			mini.get("creditsubno").setText(row.creditsubno);
/*         	var clitype = row.clitype;
        	if(clitype=="0"){ // 同业客户金融机构类型和许可证必填
        		mini.get("fintype").setEnabled(true);
        		mini.get("license").setEnabled(true);
        	} else {
        		mini.get("fintype").setEnabled(false);
        		mini.get("license").setEnabled(false);
        	} */
            //mini.get("cfn").setValue(row.cliname); // 交易对手全称取客户中文名
            if(row.localstatus == "1"){
            	//已同步的OPICS信息不能修改
                 mini.get("cfn").setEnabled(false);
                 mini.get("bic").setEnabled(false);
                 mini.get("uccode").setEnabled(false);
                 mini.get("location").setEnabled(false);
                 mini.get("lstmntdate").setEnabled(false);
                 mini.get("ctype").setEnabled(false);
                 mini.get("taxid").setEnabled(false);
                 mini.get("remark1").setEnabled(false);
                 mini.get("cname").setEnabled(false);
                 mini.get("sn").setEnabled(false);
                 mini.get("sic").setEnabled(false);
                 mini.get("ccode").setEnabled(false);
                 mini.get("ca").setEnabled(false);
                 mini.get("cpost").setEnabled(false);
                 mini.get("acctngtype").setEnabled(false);
                 mini.get("ratinfo").setEnabled(false);
                 mini.get("remark2").setEnabled(false);
                 mini.get("saveAndSync_btn").setEnabled(false);
            }
            mini.get("acctngtype").setText(row.acctngtype);
            if(action == "detail"){
            	mini.get("query").setVisible(false);
            	// mini.get("alter").setVisible(false);
            	mini.get("clino").setWidth("100%");
                $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易对手</a> >> 详情");
                form.setEnabled(false);
                mini.get("save_btn").setVisible(false);
            }
        }else{//增加 
        	//mini.get("alter").setVisible(false);
            $("#labell").html("<a href='javascript:CommonUtil.activeTab();'>交易对手</a> >> 新增");
        }
    }
    
    //客户信息查询
  	function queryCustInfo() {
  		var clino = mini.get("clino").getValue();
  		if(clino == ""){
  			mini.alert("请输入客户号");
  			return;
  		}
  		CommonUtil.ajax({
            url:"/IfsOpicsCustController/queryCustInfo",
            data:clino,
            callback:function(data){
            	if(data.obj.msg == "查询成功"){
            		mini.alert("查询成功",'提示信息');
            		//mini.get("build").setEnabled(false);
            		mini.get("cliname").setValue(data.obj.cLIENT_NAME);  // 客户中文名称
            	    mini.get("clitype").setValue(data.obj.cLIENT_TYPE);  //客户类型
            		mini.get("gtype").setValue(data.obj.gLOBAL_TYPE);  // 证件类型
            		mini.get("gid").setValue(data.obj.gLOBAL_ID);  //证件号
          		    mini.get("license").setValue(data.obj.fIN_LICENSE_CODE); //地区名称
            		mini.get("fintype").setValue(data.obj.fIN_ORG_TYPE);  //同业客户类型
            		mini.get("regplace").setValue(data.obj.mNGE_SCOP);   // 国别代码
            		mini.get("tellerno").setValue(data.obj.custTelNo); //客户建档柜员号
            		mini.get("orgno").setValue(data.obj.custOrgNo);  //客户建档机构号
            	} else {
            		if(data.obj.msg==null){
            			mini.alert("请求异常，请检查系统间通讯是否正常","系统提示");
            		} else {
            			mini.alert(data.obj.msg,"系统提示");
            		}
            		//mini.get("build").setEnabled(true);
            		return;
            	}
            }
 		 });
  		if(mini.get("clino").value != ""){
  			mini.get("cliname").setEnabled(false);
  			mini.get("clitype").setEnabled(false);
  	  		mini.get("gtype").setEnabled(false);
  	  		mini.get("gid").setEnabled(false);
  	  		mini.get("license").setEnabled(false);
  	  		mini.get("fintype").setEnabled(false);
  	  		mini.get("regplace").setEnabled(false);
  	  		mini.get("tellerno").setEnabled(false);
  	  		mini.get("orgno").setEnabled(false);
  		} else {
  			mini.get("cliname").setEnabled(true);
  			mini.get("clitype").setEnabled(true);
  	  		mini.get("gtype").setEnabled(true);
  	  		mini.get("gid").setEnabled(true);
  	  		mini.get("license").setEnabled(true);
  	  		mini.get("fintype").setEnabled(true);
  	  		mini.get("regplace").setEnabled(true);
  	  		mini.get("tellerno").setEnabled(true);
  	  		mini.get("orgno").setEnabled(true);
  		}
  	}
    
  	/* //新建客户
    function build(){
    	//ECIF信息验证
    	var ecif = new mini.Form("#ecif");
    	ecif.validate();
        if (ecif.isValid() == false) {
        	mini.alert("ECIF信息有误,请确认!","提示信息");
            return;
        }
    	var data = form.getData(true);
        var param = mini.encode(data);
        CommonUtil.ajax({
            url:"/IfsOpicsCustController/buildCust",
            data:param,
            callback:function(data){
            	if(data.obj.code == "000000"){
            		mini.alert("新建成功",'提示信息');
            		mini.get("query").hide();
            		mini.get("clino").setWidth(550);
            		mini.get("clino").setValue(data.obj.clino);
            		mini.get("clino").setEnabled(false); //客户号置灰
            		mini.get("build").setEnabled(false); // 按钮置灰
            	} else {
            		mini.alert(data.obj.msg,"系统提示");
            		mini.get("clino").setEnabled(true);
            		return;
            	}
            }
 		 });
    } */
    //查询SWIFT代码
    function querySic(){
    	CommonUtil.ajax({
            url:"/IfsOpicsBicoController/searchAllOpicsBico",
            data:{},
            callback:function(data){
            	var length = data.length;
            	var jsonData = new Array();
			   	for(var i=0;i<length;i++){
			   		var status = data[i].status;
			   		var tyId = data[i].bic;
			   		if(status=="1"){ //已同步的插入
			   			jsonData.add({'typeId':tyId,'typeValue':tyId});
			   		}
			   	};
			   	mini.get("bic").setData(jsonData);
            }
  		});
    }
    //查询国家代码
    function queryUCcode(){
    	CommonUtil.ajax({
            url:"/IfsOpicsCounController/searchAllOpicsCoun",
            data:{},
            callback:function(data){
            	var length = data.length;
            	var jsonData = new Array();
			   	for(var i=0;i<length;i++){
			   		var status = data[i].status;
			   		var tyId = data[i].ccode;
			   		if(status=="1"){ //已同步的插入
			   			jsonData.add({'typeId':tyId,'typeValue':tyId});
			   		}
			   	};
			   	mini.get("uccode").setData(jsonData);
			   	mini.get("ccode").setData(jsonData);
            }
  		});
    }
    
    //查询工业标准代码
   	function querySico(){
   	 CommonUtil.ajax({
             url:"/IfsOpicsSicoController/searchAllOpicsSico",
             data:{},
             callback:function(data){
             	var length = data.length;
             	var jsonData = new Array();
 			   	for(var i=0;i<length;i++){
 			   		var status = data[i].status;
 			   		if(data[i].sic != null){
 			   			var tyId = data[i].sic;
 			   			var tyValue = data[i].sic;
	 			   		if(status=="1"){ //已同步的插入
	 			   			jsonData.add({'typeId':tyId,'typeValue':tyValue});
	 			   		}
 			   		}
 			   	};
 			   	mini.get("sic").setData(jsonData);
             }
   		});
    }

	function onButtonEdit() {
		var btnEdit = this;
		mini.open({
			url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp?",
			title: "选择客户列表",
			width: 900,
			height: 600,
			ondestroy: function (action) {
				if (action == "ok") {
					var iframe = this.getIFrameEl();
					var data = iframe.contentWindow.GetData();
					data = mini.clone(data);
					if (data) {
						mini.get("creditsubnm").setValue(data.sn);
						btnEdit.setValue(data.cno);
						btnEdit.setText(data.cno);
						btnEdit.focus();
					}
				}
			}
		});
	}
    
    //保存
    function save(flag){
        //表单验证！！！
        form.validate();
        if (form.isValid() == false) {
            return;
        }
        /* var clino = mini.get("clino").getValue(); // 客户号
        var clitype = mini.get("clitype").getValue(); // 客户类型
    	if(clitype != "1" && clino == ""){ // 客户类型不是其他并且客户号为空，不能新增
        	mini.alert("请输入客户号");
        	return;
        } */
        var saveUrl=null;
        if(action == "add"){
            saveUrl = "/IfsOpicsCustController/saveOpicsCustAdd";
            var data=form.getData(true);
			initCredit(data);
            var params=mini.encode(data);
            CommonUtil.ajax({
                url:saveUrl,
                data:params,
                callback:function(data2){
                	if (data2.desc == "error.common.0000") {
                		if(flag==1){ //弹出保存成功
                			mini.alert("保存成功!",'提示信息',function(){
                				top["win"].closeMenuTab();
                			});
                		} else if(flag==2){//弹出保存并同步到OPICS成功
                			CommonUtil.ajax({
                				url:"/IfsOpicsCustController/syncCustD",
                				data:{cno:data.cno},
                				callback:function(data1){
                					if(data1.obj.retCode == "SL-00000"){
                						mini.alert("保存并同步成功!",'提示信息',function(){
                            				top["win"].closeMenuTab();
                            			});
                					} else {
                						mini.alert("保存成功但同步失败!",'提示信息',function(){
                            				top["win"].closeMenuTab();
                            			});
                					}
                				}
                			});
                		}
					} else if (data2.desc == "0") {
						mini.alert("该客户号已存在");
					} 
                	else {
						mini.alert("保存失败");
			    	}
                }
		    });
        } else if(action == "edit"){ //修改时保存
            saveUrl = "/IfsOpicsCustController/saveOpicsCustEdit";
            var data2=form.getData(true);
			initCredit(data2);
            var params2=mini.encode(data2);
            CommonUtil.ajax({
                url:saveUrl,
                data:params2,
                callback:function(data3){
                	if (data3.desc == "error.common.0000") {
                		if(flag==1){ //弹出保存成功
                			mini.alert("保存成功!","提示信息",function(){
                				top["win"].closeMenuTab();
                			});
                		} else if(flag==2){//弹出保存并同步到OPICS成功
                			CommonUtil.ajax({
                				url:"/IfsOpicsCustController/syncCustD",
                				data:{cno:data2.cno},
                				callback:function(data1){
                					if(data1.obj.retCode == "SL-00000"){
                						mini.alert("保存并同步成功!","提示信息",function(){
                            				top["win"].closeMenuTab();
                            			});
                					} else {
                						mini.alert("保存成功但同步失败!","提示信息",function(){
                            				top["win"].closeMenuTab();
                            			});
                					}
                				}
                			});
                		}
					} else {
						mini.alert("修改失败");
			    	}
                }
		    });
        }
    }

	function initCredit(data){
		if(data.creditsub == "1"){
			data['creditsubno']= data.cno;
			data['creditsubnm']= data.sn;
		}
	}

    function cancel(){
        top["win"].closeMenuTab();
    }

    //英文、数字、下划线 的验证
    function onEnglishAndNumberValidation(e) {
        if(e.value == "" || e.value == null){//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isEnglishAndNumber(e.value) == false) {
                e.errorText = "必须输入大写英文或数字";
                e.isValid = false;
            }
        }
    }

    /* 是否大写英文、数字、下划线 */
    function isEnglishAndNumber(v) {
        var re = new RegExp("^[0-9A-Z\_\-]+$");
        if (re.test(v)) return true;
        return false;
    }

 	//中文校验
   /*  function onChineseValidation(e) {
        if(e.value == "" || e.value == null){//值为空，就不做校验
            return;
        }
        if (e.isValid) {
            if (isChinese(e.value) == false) {
                e.errorText = "必须输入中文";
                e.isValid = false;
            }
        }
    }
    function isChinese(v) {
    	var ch = new RegExp("^[\u4e00-\u9fa5]+$");
        if (ch.test(v)) 
        	return true;
        return false;
    } */
    
    /* 根据评级类型设置是否可用 */
    function change() {
    	var flg = mini.get("ratype").getValue();
    	if(flg==1){ //内部评级
    		mini.get("extrainfo").setValue("");
    		mini.get("ratresult1").setValue("");
    		mini.get("extrainfo").setEnabled(false);
    		mini.get("ratresult1").setEnabled(false);
			mini.get("interinfo").setEnabled(true);
    		mini.get("ratresult2").setEnabled(true);
    	} else if(flg==2){ //外部评级
			mini.get("interinfo").setValue("");
			mini.get("ratresult2").setValue("");
			mini.get("interinfo").setEnabled(false);
			mini.get("ratresult2").setEnabled(false);
			mini.get("extrainfo").setEnabled(true);
			mini.get("ratresult1").setEnabled(true);
    	}
    }
    
    function onActyQuery(e){
		var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Ifs/opics/actyMini.jsp",
            title: "选择会计类型",
            width: 700,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                var data = {type1:"1",type2:"1"};//把会计归属类型传到小页面，"2"表示归属债券类，"3"表示归属客户及债券类
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);    //必须
                    if (data) {
                        btnEdit.setValue(data.acctngtype);
                        btnEdit.setText(data.acctngtype);
                        btnEdit.focus();
                    }
                }

            }
        });
	}
    
  	//验证交易对手是否存在
    function checkcust(){
    	var cno = mini.get("cno").getValue();
    	CommonUtil.ajax({
    		url:"/IfsOpicsCustController/searchIfsOpicsCust",
    		data:{cno:cno},
    		callback:function(data1){
    			if(data1.code == 'error.common.0000'){
    				if(data1.obj != null){
    					mini.alert("此交易对手'"+cno+"'已存在!请重新填写!",'提示信息',function(){
    						mini.get("cno").setValue("");
    					});
    				}
    			}
    		}
    	});
    }
  	
  	// 下拉值校验
    function onComboValidation(e) {
    	if(e.value == "" || e.value == null){ //值为空，就不做校验
            return;
        }
        var items = this.findItems(e.value);
        if (!items || items.length == 0) {
            e.isValid = false;
            e.errorText = "输入值不在下拉数据中";
        }
    }
  	
 	// 查询授信占用主体客户名称
    function searchCustNoEdit(e) {
        var btnEdit = this;
        var data={"action":'add'};
        mini.open({
            url: CommonUtil.baseWebPath() + "../../Common/CustMini.jsp",
            title: "选择对手方列表",
            width: 900,
            height: 600,
            onload: function () {
                var iframe = this.getIFrameEl();
                iframe.contentWindow.SetData(data);
            },
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cliname);
                        mini.get("creditsubno").setValue(data.clino);
                        searchProductWeight(20,0,prdNo);
                        btnEdit.focus();
                    }
                }
            }
        });
    }
 	
    /* 根据客户类型设置是否必输 */
    function judge() {
    	var clitype = mini.get("clitype").getValue();
    	if(clitype == "1"){ // 其他
    		mini.get("gtype").setEnabled(false);
    		mini.get("tellerno").setEnabled(false);
            mini.get("fintype").setEnabled(false);
    		mini.get("regplace").setEnabled(false);
    		mini.get("gid").setEnabled(false);
    		mini.get("orgno").setEnabled(false);
    		mini.get("license").setEnabled(false);
    	} else {
    		mini.get("gtype").setEnabled(true);
    		mini.get("tellerno").setEnabled(true);
    		mini.get("fintype").setEnabled(true);
    		mini.get("regplace").setEnabled(true);
    		mini.get("gid").setEnabled(true);
    		mini.get("orgno").setEnabled(true);
    		mini.get("license").setEnabled(true);
    	}
    	/* if(clitype == "0"){ //同业
    		mini.get("fintype").setEnabled(true);
    		mini.get("license").setEnabled(true);
    	}
    	if(clitype == "2"){ //企事业
    		mini.get("fintype").setValue("");
    		mini.get("license").setValue("");
    		mini.get("fintype").setEnabled(false);
    		mini.get("license").setEnabled(false);
    	} */
    }

	function creditChange(){
		var creditsub = mini.get("creditsub").getValue();
		if(creditsub == "1"){
			mini.get("creditsubno").setEnabled(false);
			mini.get("creditsubno").setText(null);
			mini.get("creditsubnm").setText(null);
		}else{
			mini.get("creditsubno").setEnabled(true);
			mini.get("creditsubno").setRequired(true);
		}

	}
    
/*     //客户信息修改
    function alter(){
    	/* var ecif = new mini.Form("#ecif");
    	ecif.validate();
        if (ecif.isValid() == false) {
        	mini.alert("ECIF信息有误,请确认!","提示信息");
            return;
        } 
    	var data = form.getData(true);
        var param = mini.encode(data);
        CommonUtil.ajax({
            url:"/IfsOpicsCustController/alterCust",
            data:param,
            callback:function(data){
            	if(data.obj.code == "000000"){
            		mini.alert("修改成功",'提示信息');
            		mini.get("query").hide();
            		mini.get("clino").setWidth(550);
            		mini.get("clino").setValue(data.obj.clino);
            		mini.get("clino").setEnabled(false); //客户号置灰
            		//mini.get("build").setEnabled(false); // 按钮置灰
            		mini.get("clino").setWidth("90%");
            	} else {
            		mini.alert(data.obj.msg,"系统提示");
            		mini.get("clino").setEnabled(true);
            		return;
            	}
            }
 		 });
    } */
</script>
</body>
</html>