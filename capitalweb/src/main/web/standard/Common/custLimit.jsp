<%@ page language="java" pageEncoding="UTF-8" %>
<div class="centerarea">
    <div id="panel1" class="mini-panel" title="授信主体信息 " style="width:100%" allowResize="true"
         collapseOnTitleClick="true">
        <div id="search_credit_form">
            <div class="leftarea">
                <input id="creditCno" name="custNo" class="mini-buttonedit mini-mustFill" labelField="true"
                       onbuttonclick="onCustEdit" label="授信主体编号：" required="true" style="width:100%;"
                       labelStyle="text-align:left;width:130px;" onvaluechanged="creditsubvalue"/>
                <input id="creditWeight" name="weight" class="mini-spinner mini-mustFill input-text-strong" minValue="0"
                       maxValue="100" value="100" changeOnMousewheel='false' labelField="true" label="授信系数(%)："
                       required="true" style="width:100%;" labelStyle="text-align:left;width:130px;"/>
                <input id="creditBamt" name="creditBamt" class="mini-spinner mini-mustFill input-text-strong"
                       maxValue="999999999999999" format="n2" labelField="true" label="授信主体剩余额度：" enabled="false"
                       style="width:100%;" labelStyle="text-align:left;width:130px;"/>
            </div>
            <div class="rightarea">
                <input id="creditSubnm" name="creditSubnm" class="mini-textbox" labelField="true" label="授信主体名称："
                       enabled="false" style="width:100%;" labelStyle="text-align:left;width:130px;"/>
                <input id="creditType" name="custType" class="mini-combobox mini-mustFill" labelField="true"
                       label="额度类型" required="true" labelStyle="text-align:left;width:130px;" style="width:100%;"
                       value="1" textField="creTypeName" valueField="creType" onvaluechanged="onCreditTypeChange"/>
                <input id="creditProdamt" name="creditProdamt" class="mini-spinner mini-mustFill input-text-strong"
                       maxValue="999999999999999" format="n2" labelField="true" label="授信主体产品剩余额度：" enabled="false"
                       style="width:100%;" labelStyle="text-align:left;width:130px;"/>
            </div>
        </div>
    </div>
    <div visible="false" id="panel12" class="mini-panel"
         title="授信主体信息 <span style='color: red;margin-left: 62px'><strong>债券类,回购类交易占用债券发行人额度</strong></span>"
         style="width:100%;height:150px;" allowResize="true" collapseOnTitleClick="true">
        <div class="mini-fit">
            <div id="panel13" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id"
                 allowAlternating="true" fitColumns="false" allowResize="true" sortMode="client" allowAlternating="true"
                 showpager="false">
                <div property="columns">
                    <div type="indexcolumn" width="50px" align="center" headerAlign="center">序号</div>
                    <div field="bondCode" width="150" align="center" headerAlign="center">债券代码</div>
                    <div field="custNo" width="150" align="center" headerAlign="center">授信主体编号</div>
                    <div field="creditSubnm" width="230px" align="center" headerAlign="center">授信主体名称</div>
                    <div field="weight" width="230px" align="center" headerAlign="center">授信系数</div>
                    <div field="custType" width="230px" align="center" headerAlign="center" renderer="onTypeRenderer">
                        额度类型
                    </div>
                    <div field="creditBamt" width="230px" align="right" headerAlign="center" allowSort="true"
                         numberFormat="n4">授信主体剩余额度
                    </div>
                    <div field="creditProdamt" width="230px" align="right" headerAlign="center" allowSort="true"
                         numberFormat="n4">授信主体产品剩余额度
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var jsonData = new Array();

    $(document).ready(function () {
        queryCreditType();
    });

    function onCustEdit() {
        var btnEdit = this;
        mini.open({
            url: "Common/CustCredit.jsp?",
            title: "选择客户列表",
            width: 900,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data);
                    if (data) {
                        btnEdit.setValue(data.cno);
                        btnEdit.setText(data.cno);
                        btnEdit.focus();
                        var cno = data.cno;
                        var creditType = mini.get("creditType").getValue();

                        if (prdNo == "") {
                            prdNo = mini.get("productCode").getValue();
                        }

                        CommonUtil.ajax({
                            url: "/HrbCreditLimitOccupyController/checklimit",
                            data: {custName: cno, prodType: prdNo, creditType: creditType},
                            callback: function (data) {
                                if (data.obj.length == 0) {
                                    mini.get("creditCno").setValue(null);
                                    mini.get("creditCno").setText(null);
                                    mini.get("creditSubnm").setValue(null);
                                    //授信主体剩余额度
                                    mini.get("creditBamt").setValue(null);
                                    //授信主体产品的剩余额度
                                    mini.get("creditProdamt").setValue(null);
                                } else {
                                    mini.get("creditCno").setValue(data.obj[0].custNum);
                                    mini.get("creditCno").setText(data.obj[0].custNum);
                                    mini.get("creditSubnm").setValue(data.obj[0].creditsubnm);
                                    //授信主体剩余额度
                                    mini.get("creditBamt").setValue(data.obj[0].avlAmt);
                                    //授信主体产品的剩余额度
                                    mini.get("creditProdamt").setValue(data.obj[0].proAvlAmt);
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    //获取授信主体信息
    function creditsubvalue(cno, prdNo, bondCode) {
        var creditType = mini.get("creditType").getValue();
        if (((cno == null || cno == "") && (bondCode == null || bondCode == "")) || prdNo == null || (creditType == null || creditType == "")) {
            return;
        }
        CommonUtil.ajax({
            url: "/HrbCreditLimitOccupyController/checklimit",
            data: {custName: cno, prodType: prdNo, bondCode: bondCode, creditType: creditType},
            callback: function (data) {
                if (data.obj.length == 0) {
                    mini.get("creditCno").setValue(null);
                    mini.get("creditCno").setText(null);
                    mini.get("creditSubnm").setValue(null);
                    //授信主体剩余额度
                    mini.get("creditBamt").setValue(null);
                    //授信主体产品的剩余额度
                    mini.get("creditProdamt").setValue(null);
                } else {
                    mini.get("creditCno").setValue(data.obj[0].custNum);
                    mini.get("creditCno").setText(data.obj[0].custNum);
                    mini.get("creditSubnm").setValue(data.obj[0].creditsubnm);
                    //授信主体剩余额度
                    mini.get("creditBamt").setValue(data.obj[0].avlAmt);
                    //授信主体产品的剩余额度
                    mini.get("creditProdamt").setValue(data.obj[0].proAvlAmt);
                }
            }
        });
    }

    //查询额度种类配置
    function queryCreditType() {
        CommonUtil.ajax({
            url: "/HrbCreditTypeController/queryAllCrediyType",
            data: {},
            callback: function (data) {
                var length = data.length;
                for (var i = 0; i < length; i++) {
                    var creType = data[i].creType;
                    var creTypeName = data[i].creTypeName;
                    jsonData.add({'creType': creType, 'creTypeName': creTypeName});
                }
                ;
                mini.get("creditType").setData(jsonData);
            }
        });
    }

    function onTypeRenderer(e) {
        var length = jsonData.length;
        for (var i = 0; i < length; i++) {
            if (e.value == jsonData[i].creType) {
                return jsonData[i].creTypeName;
            }
        }
    }

    //添加
    function addCredit(detailData) {
        var myDir = mini.get("myDir").getValue();
        if (myDir == 'S') {
            return;
        }
        var row = {
            bondCode: detailData.bondCode,
            custNo: detailData.custNo,
            creditSubnm: detailData.creditSubnm,
            weight: detailData.weight,
            custType: detailData.custType,
            creditBamt: detailData.creditBamt,
            creditProdamt: detailData.creditProdamt
        };
        panel13.addRow(row);
        var creditFormx = new mini.Form("#search_credit_form");
        creditFormx.clear();
        mini.get("creditWeight").setValue(100);
        mini.get("creditType").setValue("1");
        mini.get("creditBamt").setValue(0);
        mini.get("creditProdamt").setValue(0);

    }

    //修改
    function editCredit(bondCode) {
        var myDir = mini.get("myDir").getValue();
        if (myDir == 'S') {
            return;
        }
        var row = null;
        var rows = panel13.getData();
        for (var i = 0; i < rows.length; i++) {
            if (rows[i].bondCode == bondCode) {
                row = rows[i];
            }
        }
        if(row == null){
            return;
        }
        var creditForm = new mini.Form("#search_credit_form");
        creditForm.setData(row);
        mini.get("creditCno").setValue(row.custNo);
        mini.get("creditCno").setText(row.custNo);
    }

    //删除
    function delCredit(bondCode) {
        var myDir = mini.get("myDir").getValue();
        if (myDir == 'S') {
            return;
        }
        var row = null;
        var rows = panel13.getData();
        for (var i = 0; i < rows.length; i++) {
            if (rows[i].bondCode == bondCode) {
                row = rows[i];
            }
        }
        panel13.removeRow(row);
    }

    function hebing(rows, crets) {
        for (var i = 0; i < rows.length; i++) {
            for (var j = 0; i < crets.length; i++) {
                if (rows[i].bondCode == crets[i].bondCode) {
                    rows[i]['custNo'] = crets[i].custNo;
                    rows[i]['weight'] = crets[i].weight;
                    rows[i]['custType'] = crets[i].custType;
                }
            }
        }
    }

    function fenkai(rows) {
        for (var i = 0; i < rows.length; i++) {
            var row = {
                bondCode: rows[i].bondCode,
                custNo: rows[i].custNo,
                creditSubnm: rows[i].creditSubnm,
                weight: rows[i].weight,
                custType: rows[i].custType,
                creditBamt: rows[i].creditBamt,
                creditProdamt: rows[i].creditProdamt
            };
            panel13.addRow(row);
        }
    }

</script>