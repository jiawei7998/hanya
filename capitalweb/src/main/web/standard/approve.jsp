﻿﻿﻿﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="./global.jsp" %>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
    <link href="<%=basePath%>/miniScript/css/firstpage.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<fieldset class="mini-fieldset">
    <legend>查询</legend>
    <div id="search_form" style="width: 100%;">
        <input id="contractId" name="contractId" class="mini-textbox" labelField="true" label="成交单编号："
               emptyText="请填写成交单编号"
               labelStyle="text-align:right;" width="350px"/>
        <input id="prdName" name="prdName" class="mini-textbox" labelField="true" label="产品名称：" emptyText="请填写产品名称"
               labelStyle="text-align:right;" width="350px"/>
        <nobr>
            <input id="startDate" name="startDate" class="mini-datepicker" labelField="true" label="审批日期："
                   ondrawdate="onDrawDateStart" labelStyle="text-align:right;" emptyText="起始日期" format="yyyy-MM-dd"/>
            <span>~</span>
            <input id="endDate" name="endDate" class="mini-datepicker"
                   ondrawdate="onDrawDateEnd" emptyText="结束日期" format="yyyy-MM-dd"/>
        </nobr>

        <input id="custName" name="custName" class="mini-textbox" labelField="true" label="对方机构：" emptyText="请填写对方机构"
               labelStyle="text-align:right;" width="350px"/>
        <input id="tradeDate" name="tradeDate" class="mini-datepicker" labelField="true" label="交易日期："
               labelStyle="text-align:right;" width="350px" emptyText="请选择交易日期"/>
        <!-- <input id="approveDate" name="approveDate" class="mini-datepicker" labelField="true" label="审批日期："
                labelStyle="text-align:center;" emptyText="已审列表有效" /> -->
        <input id="dealno" name="dealno" class="mini-textbox" labelField="true" label="OPICS编号：" emptyText="请填写OPICS编号"
               labelStyle="text-align:right;" width="350px"/>


        <span style="float: right; margin-right: 100px">
				<a id="search_btn" class="mini-button" onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" onclick="clear()">清空</a>
			</span>
    </div>
</fieldset>

<div class="mini-fit">
    <div style="height:100%;">
        <div class="mini-toolbar" width="80%">
            <table style="width:100%;">
                <tr>
                    <td>
                        <a id="verify_commit_btn" class="mini-button">审批</a>
                        <a id="batch_approve_btn" class="mini-button" onclick="batchApprove()">批量审批</a>
                        <a id="approve_log" class="mini-button" onclick="searchlog()">审批日志</a>
                        <a id="refresh_btn" class="mini-button" onclick="refresh">刷新</a>
                        <a id="export" class="mini-button" onclick="exportExcel">导出报表</a>
                        <a id="print_btn" class="mini-button" onclick="print">打印</a>
                        <a id="look_btn" class="mini-button">查看</a>
                        <div id="approveType" name="approveType" class="mini-checkboxlist" style="float:right;"
                             labelStyle="text-align:right;background: #fff; border: none;"
                             textField="text" valueField="id" multiSelect="false" onvaluechanged="checkBoxValuechanged"
                             data="[{id:'approve',text:'待审批列表'},{id:'finished',text:'已审批列表'}]"
                             value="approve">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="mini-fit">
            <div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" multiSelect="true"
                 sortMode="client"
                 allowAlternating="true" allowResize="true" allowCellWrap="true" border="true" multiSelect="false"
                 sizeList="[10,20,50,100]">
                <div property="columns">
                    <div type="checkcolumn"></div>
                    <div type="indexcolumn" headerAlign="center" width="40">序号</div>
                    <div field="ticketId" width="150" headerAlign="center" align="left" allowSort="true">审批单编号</div>
                    <div field="taskName" width="80" allowSort="false" headerAlign="center" align="">审批状态</div>
                    <%--                    <div field="processMsg" width="100" headerAlign="center">状态</div>--%>
                    <div field="prdName" width="150" headerAlign="center">产品名称</div>
                    <div field="ccyPair" width="80" headerAlign="center">货币/货币</div>
                    <div field="mydircn" width="80" headerAlign="center">本方方向</div>
                    <div field="tccy" width="80" headerAlign="center">交易货币</div>
                    <!-- <div field="amt" width="80" headerAlign="center">交易金额</div>
                    <div field="rate" width="80" headerAlign="center"numberFormat="#,0.00000000">成交汇率</div> -->
                    <div field="vamt" width="80" headerAlign="center">交易金额</div>
                    <div field="vrate" width="100" headerAlign="center" numberFormat="#,0.00000000">成交汇率</div>
                    <!--  <div field="tradeType" width="100" headerAlign="center" align="center" allowSort="true"
                         renderer="CommonUtil.dictRenderer" data-options="{dict:'TradeFlowType'}" >操作类型</div> -->
                    <div field="vDate" width="80" headerAlign="center" align="center" allowSort="true">起息日期</div>
                    <div field="tradeDate" width="80" headerAlign="center" align="center" allowSort="true">交易日期</div>
                    <!--                     <div field="myInstId" width="100" headerAlign="center">本方机构</div> -->
                    <div field="custName" width="200" headerAlign="center">对方机构</div>
                    <!--                     <div field="mydealer" width="100" headerAlign="center">本方交易员</div> -->
                    <div field="approveDate" width="180" headerAlign="center" align="center" allowSort="true">审批日期</div>
                    <div field="contractId" width="180" headerAlign="center" align="left" allowSort="true">成交单编号</div>
                    <div field="statcode" width="120" headerAlign="center" align="left" allowSort="true"
                         renderer="CommonUtil.dictRenderer" data-options="{dict:'statcode'}">opics处理状态
                    </div>
                    <div field="dealno" width="150" headerAlign="center" align="left" allowSort="true">opics编号</div>
                    <div field="sponsor" width="60" headerAlign="center">审批发起人</div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    mini.parse();
    var flashFlag = "1";//是否从0开始  ,0是，1不是
    $(document).ready(function () {
        var approveType = mini.get("approveType").getValue();
        if (approveType == "finished") {
            mini.get("print_btn").setVisible(true);
        } else {
            mini.get("print_btn").setVisible(false);
        }
        //search(grid.pageSize, 0);
       // tips();
    });

    top["qdfirstpage"] = window;
    var type = "trade";
    var grid = mini.get("datagrid");

    grid.on("beforeload", function (e) {
        e.cancel = true;
        flashFlag = "0";
        var pageIndex = e.data.pageIndex;
        var pageSize = e.data.pageSize;
        //search(pageSize, pageIndex);
    });

    function query() {

        flashFlag = "0";
        search(grid.pageSize, 0);
    }

    function clear() {
        flashFlag = "0";
        var form = new mini.Form("search_form");
        form.clear();
        search(10, 0);
    }

    //审批日志
    function searchlog() {
        appLog(grid.getSelecteds());
    }

    //审批日志查看
    function appLog(selections) {
        var flow_type = Approve.FlowType.VerifyApproveFlow;
        if (selections.length <= 0) {
            mini.alert("请选择要操作的数据", "系统提示");
            return;
        }
        if (selections.length > 1) {
            mini.alert("系统不支持多笔操作", "系统提示");
            return;
        }
        if (selections[0].tradeSource == "3") {
            mini.alert("初始化导入的业务没有审批日志", "系统提示");
            return false;
        }
        Approve.approveLog(flow_type, selections[0].ticketId);
    };

    //审批日期
    function onDrawDateStart(e) {
        var startDate = e.date;
        var endDate = mini.get("endDate").getValue();
        if (CommonUtil.isNull(endDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function onDrawDateEnd(e) {
        var endDate = e.date;
        var startDate = mini.get("startDate").getValue();
        if (CommonUtil.isNull(startDate)) {
            return;
        }
        if (endDate.getTime() < startDate.getTime()) {
            e.allowSelect = false;
        }
    }

    function search(pageSize, pageIndex) {
        /*  var param = {};
      param.pageNumber = pageIndex+1;
      param.pageSize = pageSize;
         param.userId=userId; */

        var form = new mini.Form("#search_form");
        form.validate();
        if (form.isValid() == false) {
            mini.alert("信息填写有误，请重新填写", "系统提示");
            return;
        }
        if (flashFlag == "0") {
            //是否从0开始  ,0是，1不是
            flashFlag = "1";
        } else {
            pageIndex = grid.pageIndex;
        }
        var data = form.getData(true);
        data['pageNumber'] = pageIndex + 1;
        data['pageSize'] = pageSize;
        data['userId'] = userId;
        var url = null;
        var approveType = mini.get("approveType").getValue();
        if (approveType == "approve") {
            url = "/IfsFlowMonitorController/searchPageFlowMonitor";
        } else if (approveType == "finished") {
            url = "/IfsFlowCompletedController/searchPageFlowCompleted";
        } else {
            //modify by shenzl20190708
            //url = "/IfsFlowMonitorController/searchPageAllFlowMonitor";
        }
        var param = mini.encode(data);
        CommonUtil.ajax({
            url: url,
            data: param,
            showLoadingMsg: true,
            messageId: mini.loading('正在查询数据...', "请稍后"),
            callback: function (data) {
                if (data) {
                    var dataArray = data.obj.rows;
                    grid.setTotalCount(data.obj.total);
                    grid.setPageIndex(pageIndex);
                    grid.setPageSize(pageSize);
                    grid.setData(dataArray);
                }
            }
        });
    }// end search

    function checkBoxValuechanged(e) {
        flashFlag = "0";
        search(10, 0);
        var approveType = mini.get("approveType").getValue();
        if (approveType == "approve") {
            mini.get("approve_log").setEnabled(true);
            mini.get("verify_commit_btn").setEnabled(true);
            mini.get("approve_log").setVisible(true);
            mini.get("verify_commit_btn").setVisible(true);
            mini.get("batch_approve_btn").setVisible(true);//批量审批显示
            mini.get("batch_approve_btn").setEnabled(true);//批量审批高亮
        } else if (approveType == "finished" || approveType == "all") {
            mini.get("verify_commit_btn").setEnabled(false);
            mini.get("approve_log").setEnabled(true);
            mini.get("approve_log").setVisible(true);
            mini.get("verify_commit_btn").setVisible(false);
            mini.get("batch_approve_btn").setVisible(false);//批量审批不显示
            mini.get("batch_approve_btn").setEnabled(false);//批量审批不高亮
        }
        var approveType = mini.get("approveType").getValue();
        if (approveType == "finished") {
            mini.get("print_btn").setVisible(true);
        } else {
            mini.get("print_btn").setVisible(false);
        }
    }

    //正式交易 审批
    mini.get("verify_commit_btn").on("click", function () {
        var selections = grid.getSelecteds();
        if (selections.length == 0) {
            mini.alert("请选中一条记录！", "消息提示");
            return false;
        } else if (selections.length > 1) {
            mini.alert("暂不支持多笔提交", "系统提示");
            return false;
        }
        verify_trade(selections);
        mini.get("verify_commit_btn").setEnabled(true);
    });

    function verify_trade(selections) {
        var fPrdCode = selections[0]["fPrdCode"];
        var prdNo = selections[0]["prdNo"];
        var tradeId = selections[0]["ticketId"];
        var tradeType = selections[0]["tradeType"];
        if (prdNo == null || prdNo == "") {
            mini.alert("抱歉,该笔交易暂时无法在此审批，请进入具体交易界面进行审批！");
            return;
        }
        productType(tradeType, prdNo, tradeId, fPrdCode);
    }

    function productType(tradeType, prdNo, tradeId, fPrdCode) {
        var searchByIdUrl = "";
        var openJspUrl = "";
        var id = "";
        var title = "";
        var datas = {};
        datas['ticketId'] = tradeId;
        var selected = mini.get("datagrid").getSelected();
        if (tradeType == "1") {//正式交易

            if (prdNo == "irs") {//利率互换
                searchByIdUrl = "/IfsCfetsrmbIrsController/searchCfetsrmbIrsAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "441", "/cfetsrmb/IRSEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "IRSApprove";
                title = "利率互换审批";
            } else if (prdNo == "irsDerive") {//结构衍生
                searchByIdUrl = "/IfsCfetsrmbIrsController/searchCfetsrmbIrsAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "400", "/cfetsrmb/IrsDeriveEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "IRSDeriveApprove";
                title = "结构衍生审批";
            } else if (prdNo == "or") {//买断式回购
                searchByIdUrl = "/IfsCfetsrmbOrController/searchCfetsrmbOrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "442", "/cfetsrmb/repoEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "repoApprove";
                title = "买断式回购审批";
            } else if (prdNo == "cbt") {//现券买卖
                searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "443", "/cfetsrmb/bondTradingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "bondTradingApprove";
                title = "现券买卖审批";
            } else if (prdNo == "mbs") {//MBS
                searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "471", "/cfetssecur/MBSEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "MBSApprove";
                title = "MBS审批";
            } else if (prdNo == "bfwd") {//债券远期
                searchByIdUrl = "/IfsCfetsrmbBondfwdController/searchCfetsrmbBondfwdAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "447", "/cfetsrmb/bondFwdEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "bondTradingApprove";
                title = "债券远期审批";
            } else if (prdNo == "ibo") {//信用拆借
                searchByIdUrl = "/IfsCfetsrmbIboController/searchCfetsrmbIboAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "444", "/cfetsrmb/creditLoanEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "creditLoanApprove";
                title = "信用拆借审批";
            } else if (prdNo == "tyibo") {//同业借款
                searchByIdUrl = "/IfsCfetsrmbIboController/searchCfetsrmbIboAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "544", "/cfetsrmb/creditTYLoanEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "creditTYLoanApprove";
                title = "同业借款审批";
            } else if (prdNo == "sl") {//债券借贷
                datas['execId'] = tradeId;
                searchByIdUrl = "/IfsCfetsrmbSlController/searchCfetsrmbSlAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "445", "/cfetsrmb/bondLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "bondLendingApprove";
                title = "债券借贷审批";
            } else if (prdNo == "cr") {//质押式回购
                searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "446", "/cfetsrmb/pledgeRepoEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "pledgeRepoApprove";
                title = "质押式回购审批";
            } else if (prdNo == "goldspt") {//黄金即期
                searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
                Approve.goToApproveJsp(selected.taskId, "447", "/cfetsmetal/goldsptEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "IrsSptApprove";
                title = "黄金即期审批";
            } else if (prdNo == "goldfwd") {//黄金远期
                searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
                Approve.goToApproveJsp(selected.taskId, "448", "/cfetsmetal/goldfwdEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "IrsFwdApprove";
                title = "黄金远期审批";
            } else if (prdNo == "goldswap") {//黄金掉期
                searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
                Approve.goToApproveJsp(selected.taskId, "449", "/cfetsmetal/goldswapEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "IrsSwapApprove";
                title = "黄金掉期审批";
            } else if (prdNo == "goldLend") {//黄金拆借
                searchByIdUrl = "/IfsGoldLendController/searchCfetsGoldLendAndFlowById";
                Approve.goToApproveJsp(selected.taskId, "450", "/cfetsmetal/GoldLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "goldLendingApprove";
                title = "黄金拆借审批";
            } else if (prdNo == "debt") {//外币债
                searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "451", "/bloomberg/fxccydebtEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "fxccydebtApprove";
                title = "外币债审批";
            } else if (prdNo == "rmbspt") {//外汇即期
                searchByIdUrl = "/IfsForeignController/searchSpotAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "411", "/cfetsfx/rmbsptEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "RmbSptApprove";
                title = "外汇即期审批";
            } else if (prdNo == "rmbfwd") {//外汇远期
                searchByIdUrl = "/IfsForeignController/searchFwdAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "391", "/cfetsfx/rmfwdEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "rmfwdApprove";
                title = "外汇远期审批";
            } else if (prdNo == "rmbswap") {//外汇掉期
                searchByIdUrl = "/IfsForeignController/searchRmbSwapAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "432", "/cfetsfx/rmswapEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "rmsSwapApprove";
                title = "外汇掉期审批";
            } else if (prdNo == "ccyswap") {//货币掉期
                searchByIdUrl = "/IfsCurrencyController/searchCcySwapAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "431", "/cfetsfx/ccyswapEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "CcySwapApprove";
                title = "货币掉期审批";
            } else if (prdNo == "rmbopt") {//人民币期权
                searchByIdUrl = "/IfsForeignController/searchRmbOptAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "433", "/cfetsfx/rmboptEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceRmBoptApprove";
                title = "外汇期权审批";
            } else if (prdNo == "ccyLending") {//外币拆借
                searchByIdUrl = "/IfsForeignController/searchRmbCcylendingAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "438", "/cfetsfx/ccyLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceCcyLendingApprove";
                title = "外币拆借审批";
            } else if (prdNo == "ccyDepositIn") {//同业存放(外币)
                searchByIdUrl = "/IfsCCYDepositInController/searchDepositInFXFlowByTicketId";
                Approve.goToApproveJsp(selected.taskId, "436", "/cfetsfx/ccyDepositInEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceCcyDepositInApprove";
                title = "同业存放(外币)审批";
            } else if (prdNo == "ccyDepositout") {//存放同业(外币)
                searchByIdUrl = "/IfsCCYDepositOutController/searchFlowByTicketId";
                Approve.goToApproveJsp(selected.taskId, "437", "/cfetsfx/ccyDepositOutEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceCcyDepositoutApprove";
                title = "存放同业(外币)审批";
            }  else if (prdNo == "fxtyibo") {//同业拆放
                searchByIdUrl = "/IfsCfetsrmbIboController/searchCfetsrmbIboAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "437", "/cfetsrmb/creditTYLoanRuaEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target +"?action=approve&ticketid=" + tradeId;
                id = "creditTYLoanRuaDetail";
                title = "同业拆放审批";
            } else if (prdNo == "rmbDepositIn") {//同业存放
                searchByIdUrl = "/IfsRmbDepositInController/searchFlowByTicketId";
                Approve.goToApproveJsp(selected.taskId, "790", "/cfetsrmb/rmbDepositInEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceRmbDepositInApprove";
                title = "同业存放审批";
            } else if (prdNo == "rmbDepositOut") {//存放同业
                searchByIdUrl = "/IfsRmbDepositOutController/searchFlowDepositOutRmbByTicketId";
                Approve.goToApproveJsp(selected.taskId, "791", "/cfetsrmb/rmbDepositOutEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceRmbDepositOutApprove";
                title = "存放同业审批";
            } else if (prdNo == "ccypa") {//外币头寸调拨
                searchByIdUrl = "/IfsForeignController/searchRmbCcypaAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "439", "/cfetsfx/ccyPositionAllocationEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceAllcationApprove";
                title = "外币头寸调拨审批";
            } else if (prdNo == "ccypspot") {//外汇对即期
                searchByIdUrl = "/IfsForeignController/searchRmbSpotAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "440", "/cfetsfx/ccypspotEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AdvanceCcySpotApprove";
                title = "外汇对即期审批";
            } else if (prdNo == "dp") {//债券发行
                searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "452", "/cfetsrmb/depositPublishEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "depositPublishApprove";
                title = "债券发行审批";
            } else if (prdNo == "dpcd") {//存单发行
                searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "452", "/cfetsrmb/cdPublishEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "cdPublishApprove";
                title = "存单发行审批";
            } else if (prdNo == "IBD") {//同业存款(线上)
                searchByIdUrl = "/IfsApprovermbDepositController/searchDepositByDealNo";
                Approve.goToApproveJsp(selected.taskId, "800", "/cfetsrmb/depositEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?approveflag=1&action=approve&ticketid=" + tradeId;
                id = "depositApprove";
                title = "同业存款(线上)";
            } else if (prdNo == "IBDOFF") {//同业存款(线下)
                searchByIdUrl = "/IfsApprovermbDepositController/searchDepositByDealNo";
                Approve.goToApproveJsp(selected.taskId, "799", "/cfetsrmb/depositOfflineEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?approveflag=1&action=approve&ticketid=" + tradeId;
                id = "depositOffApprove";
                title = "同业存款(线下)";
            } else if (prdNo == "CRGD") {//国库定期存款
                searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "467", "/cfetsrmb/nationalDepositEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "nationalDepositApprove";
                title = "国库定期存款";
            } else if (prdNo == "CRJY") {//交易所回购
                searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "468", "/cfetsrmb/exchangeRepoEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "exchangeRepoApprove";
                title = "交易所回购";
            } else if (prdNo == "CRCB") {//常备借贷便利
                searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "469", "/cfetsrmb/standingLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "standingLendingApprove";
                title = "常备借贷便利";
            } else if (prdNo == "CRZQ") {//中期借贷便利
                searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "470", "/cfetsrmb/MediumLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "MediumLendingApprove";
                title = "中期借贷便利";
            } else if (prdNo == "CRZD") {//线下押券
                searchByIdUrl = "/IfsCfetsrmbCrController/searchCfetsrmbCrAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "472", "/cfetsrmb/MicroLendingEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "MicroLendingApprove";
                title = "线下押券";
            } else if (prdNo == "tyck") {//同业存款(线下)
                earchByIdUrl = "/IfsCfetsrmbCrController/searchDepositByDealNo";
                Approve.goToApproveJsp(selected.taskId, "803", "/cfetsrmb/depositOfflineEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "depoistOfflineApprove";
                title = "同业存款(线下)";
            }

        } else if (tradeType == "0") {//事前交易

            if (prdNo == "irs") {//利率互换
                searchByIdUrl = "/IfsApprovermbIrsController/searchCfetsrmbIrsAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvermb/IRSEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "IRSApprove";
                title = "利率互换审批";
            } else if (prdNo == "irsDerive") {//结构衍生
                searchByIdUrl = "/IfsCfetsrmbIrsController/searchCfetsrmbIrsAndFlowIdById";
                Approve.goToApproveJsp(selected.taskId, "400", "/cfetsrmb/IrsDeriveEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "IRSDeriveApprove";
                title = "结构衍生审批";
            } else if (prdNo == "or") {//买断式回购
                searchByIdUrl = "/IfsApprovermbOrController/searchCfetsrmbOrAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvermb/repoEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "repoApprove";
                title = "买断式回购审批";
            } else if (prdNo == "cbt") {//现券买卖
                searchByIdUrl = "/IfsApprovermbCbtController/searchCfetsrmbCbtAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvermb/bondTradingEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "bondTradingApprove";
                title = "现券买卖审批";
            } else if (prdNo == "ibo") {//信用拆借
                searchByIdUrl = "/IfsApprovermbIboController/searchCfetsrmbIboAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvermb/creditLoanEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "creditLoanApprove";
                title = "信用拆借审批";
            } else if (prdNo == "sl") {//债券借贷
                searchByIdUrl = "/IfsApprovermbSlController/searchCfetsrmbSlAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvermb/bondLendingEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "bondLendingApprove";
                title = "债券借贷审批";
            } else if (prdNo == "cr") {//质押式回购
                searchByIdUrl = "/IfsApprovermbCrController/searchCfetsrmbCrAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvermb/pledgeRepoEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "pledgeRepoApprove";
                title = "质押式回购审批";
            } else if (prdNo == "goldspt") {//黄金即期
                searchByIdUrl = "/IfsApprovemetalGoldController/searchCfetsmetalGoldAndFlowById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvemetal/goldsptEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "IrsSptApprove";
                title = "黄金即期审批";
            } else if (prdNo == "goldfwd") {//黄金远期
                searchByIdUrl = "/IfsApprovemetalGoldController/searchCfetsmetalGoldAndFlowById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvemetal/goldfwdEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "IrsFwdApprove";
                title = "黄金远期审批";
            } else if (prdNo == "goldswap") {//黄金掉期
                searchByIdUrl = "/IfsApprovemetalGoldController/searchCfetsmetalGoldAndFlowById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvemetal/goldswapEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "IrsSwapApprove";
                title = "黄金掉期审批";
            } else if (prdNo == "goldLend") {//黄金拆借
                searchByIdUrl = "/IfsApproveGoldLendController/searchCfetsGoldLendAndFlowById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvesge/GoldLendingEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "goldLendingApprove";
                title = "黄金拆借审批";
            } else if (prdNo == "debt") {//外币债
                searchByIdUrl = "/IfsApproveCurrencyController/searchCurrencyAndFlowById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/fxccydebtEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "fxccydebtApprove";
                title = "外币债审批";
            } else if (prdNo == "rmbspt") {//外汇即期
                searchByIdUrl = "/IfsApproveForeignController/searchSpotAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/rmbsptEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "RmbSptApprove";
                title = "外汇即期审批";
            } else if (prdNo == "rmbfwd") {//外汇远期
                searchByIdUrl = "/IfsApproveForeignController/searchFwdAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/rmfwdEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "rmfwdApprove";
                title = "外汇远期审批";
            } else if (prdNo == "rmbswap") {//外汇掉期
                searchByIdUrl = "/IfsApproveForeignController/searchRmbSwapAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/rmswapEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "rmsSwapApprove";
                title = "外汇掉期审批";
            } else if (prdNo == "ccyswap") {//货币掉期
                searchByIdUrl = "/IfsApproveCurrencyController/searchCcySwapAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/ccyswapEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "CcySwapApprove";
                title = "货币掉期审批";
            } else if (prdNo == "rmbopt") {//人民币期权
                searchByIdUrl = "/IfsApproveForeignController/searchRmbOptAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/rmboptEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "AdvanceRmBoptApprove";
                title = "外汇期权审批";
            } else if (prdNo == "ccyLending") {//外币拆借
                searchByIdUrl = "/IfsApproveForeignController/searchRmbCcylendingAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/ccyLendingEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "AdvanceCcyLendingApprove";
                title = "外币拆借审批";
            } else if (prdNo == "ccypa") {//外币头寸调拨
                searchByIdUrl = "/IfsApproveForeignController/searchRmbCcypaAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/ccyPositionAllocationEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "AdvanceAllcationApprove";
                title = "外币头寸调拨审批";
            } else if (prdNo == "ccypspot") {//外汇对即期
                searchByIdUrl = "/IfsApproveForeignController/searchRmbSpotAndFlowIdById";
                openJspUrl = CommonUtil.baseWebPath() + "/approvefx/ccypspotEdit.jsp?action=approve&ticketid=" + tradeId;
                id = "AdvanceCcySpotApprove";
                title = "外汇对即期审批";
            }
        } else if (tradeType == "30") {//基金申购审批
            if (prdNo == "CURYAFP") {
                searchByIdUrl = "/CFtAfpController/searchCFtAfpByDealNo";
                Approve.goToApproveJsp(selected.taskId, "801", "/refund/CCurrencyFundAfpEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "CuryAfpApprove";
                title = "货币基金申购审批";
            }
            if (prdNo == "BDAFP") {
                searchByIdUrl = "/CFtAfpController/searchCFtAfpByDealNo";
                Approve.goToApproveJsp(selected.taskId, "802", "/refund/CBondFundAfpEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "BdAfpApprove";
                title = "债券基金申购审批";
            }
            if (prdNo == "AFP") {
                searchByIdUrl = "/CFtAfpController/searchCFtAfpByDealNo";
                Approve.goToApproveJsp(selected.taskId, "803", "/refund/CFundAfpEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AfpApprove";
                title = "专户基金申购审批";
            }
        } else if (tradeType == "31") {//基金申购份额确认审批
            if (prdNo == "CURYAFPCON") {
                searchByIdUrl = "/CFtAfpConfController/searchCFtAfpConfByDealNo";
                Approve.goToApproveJsp(selected.taskId, "801", "/refund/CCurrencyFundAfpConfEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "CuryAfpConfApprove";
                title = "债券基金申购份额确认审批";
            }
            if (prdNo == "BDAFPCON") {
                searchByIdUrl = "/CFtAfpConfController/searchCFtAfpConfByDealNo";
                Approve.goToApproveJsp(selected.taskId, "802", "/refund/CBondFundAfpConfEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "BdAfpConfApprove";
                title = "债券基金申购份额确认审批";
            }
            if (prdNo == "AFPCON") {
                searchByIdUrl = "/CFtAfpConfController/searchCFtAfpConfByDealNo";
                Approve.goToApproveJsp(selected.taskId, "803", "/refund/CFundAfpConfEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "AfpConfApprove";
                title = "专户基金申购份额确认审批";
            }
        } else if (tradeType == "32") {//基金赎回审批
            if (prdNo == "CURYRDP") {
                searchByIdUrl = "/CFtRdpController/searchCFtRdpByDealNo";
                Approve.goToApproveJsp(selected.taskId, "801", "/refund/CCurrencyFundRdpEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "CuryRdpApprove";
                title = "货币基金赎回审批";
            }
            if (prdNo == "BDRDP") {
                searchByIdUrl = "/CFtRdpController/searchCFtRdpByDealNo";
                Approve.goToApproveJsp(selected.taskId, "802", "/refund/CBondFundRdpEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "BdRdpApprove";
                title = "债券基金赎回审批";
            }
            if (prdNo == "RDP") {
                searchByIdUrl = "/CFtRdpController/searchCFtRdpByDealNo";
                Approve.goToApproveJsp(selected.taskId, "803", "/refund/CFundRdpEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "RdpApprove";
                title = "专户基金赎回审批";
            }
        } else if (tradeType == "33") {//基金赎回份额确认审批
            if (prdNo == "CURYRDPCON") {
                searchByIdUrl = "/CFtRdpConfController/searchCFtRdpConfByDealNo";
                Approve.goToApproveJsp(selected.taskId, "801", "/refund/CCurrencyFundRdpConfEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "CuryRdpConfApprove";
                title = "货币基金申购份额确认审批";
            }
            if (prdNo == "BDRDPCON") {
                searchByIdUrl = "/CFtRdpConfController/searchCFtRdpConfByDealNo";
                Approve.goToApproveJsp(selected.taskId, "802", "/refund/CBondFundRdpConfEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "BdRdpConfApprove";
                title = "债券基金申购份额确认审批";
            }
            if (prdNo == "RDPCON") {
                searchByIdUrl = "/CFtRdpConfController/searchCFtRdpConfByDealNo";
                Approve.goToApproveJsp(selected.taskId, "803", "/refund/CFundRdpConfEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "RdpConfApprove";
                title = "专户基金申购份额确认审批";
            }
        } else if (tradeType == "34") {//基金现金分红审批
            if (prdNo == "REDD") {
                searchByIdUrl = "/CFtReddController/searchCFtReddByDealNo";
                Approve.goToApproveJsp(selected.taskId, "805", "/refund/CFundReddEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "depositPublishApprove";
                title = "基金现金分红";
            } else if (prdNo == "REIN") {
                searchByIdUrl = "/CFtReinController/searchCFtReinByDealNo";
                Approve.goToApproveJsp(selected.taskId, "806", "/refund/CFundReinEdit.jsp", target);
                var target = Approve.approvePage;
                openJspUrl = CommonUtil.baseWebPath() + target + "?action=approve&ticketid=" + tradeId;
                id = "depositPublishApprove";
                title = "基金红利再投";
            }
        }
        openJspUrl = openJspUrl + "&prdNo=" + prdNo + "&fPrdCode=" + fPrdCode;
        datas.userId = userId;
        datas.branchId = branchId;
        var params = mini.encode(datas);
        //外汇单笔校验其他信息
        if (selected.taskName == "待结算" && (prdNo == "rmbspt" || prdNo == "rmbfwd" || prdNo == "rmbswap" || prdNo == "rmbopt")) {
            datas.prdNo = prdNo;
            var paramsFx = mini.encode(datas);
            CommonUtil.ajax({
                url: "/IfsOpicsSettleManageController/checkFxFlagSinglee",
                data: paramsFx,
                callback: function (fxData) {
                    if (fxData == "") {
                        CommonUtil.ajax({
                            url: searchByIdUrl,
                            data: params,
                            callback: function (data) {
                                var url = openJspUrl;
                                var tab = {
                                    "id": id, name: id, url: url, title: title,
                                    parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
                                };
                                var paramData = {selectData: data.obj};
                                CommonUtil.openNewMenuTab(tab, paramData);
                            }
                        });
                    } else {
                        mini.confirm(fxData + ",确认要审批吗？", "确认", function (action) {
                            if (action != "ok") {
                                return;
                            }
                            CommonUtil.ajax({
                                url: searchByIdUrl,
                                data: params,
                                callback: function (data) {
                                    var url = openJspUrl;
                                    var tab = {
                                        "id": id, name: id, url: url, title: title,
                                        parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
                                    };
                                    var paramData = {selectData: data.obj};
                                    CommonUtil.openNewMenuTab(tab, paramData);
                                }
                            });
                        }); // end confirm
                    }
                }
            });
        } else {
            CommonUtil.ajax({
                url: searchByIdUrl,
                data: params,
                callback: function (data) {
                    var url = openJspUrl;
                    var tab = {
                        "id": id, name: id, url: url, title: title,
                        parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
                    };
                    var paramData = {selectData: data.obj};
                    CommonUtil.openNewMenuTab(tab, paramData);
                }
            });
        }
    }

    //刷新
    function refresh() {
        search(grid.pageSize, 0);
    }

    //查看
    mini.get("look_btn").on("click", function () {
        var selections = grid.getSelecteds();
        if (selections.length != 1) {
            mini.alert("请选中一条记录！", "消息提示");
            return false;
        }
        queryDealTail(selections);
    });

    function queryDealTail(selections) {
        var fPrdCode = selections[0]["fPrdCode"];
        var tradeId = selections[0]["ticketId"];
        var prdNo = selections[0]["prdNo"];
        var searchByIdUrl = "";
        var openJspUrl = "";
        var id = "";
        var title = "";
        var datas = {};
        datas['ticketId'] = tradeId;
        if (prdNo == "rmbspt") {//外汇即期
            searchByIdUrl = "/IfsForeignController/searchSptDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmbsptEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "RmbSptApproveDetail";
            title = "外汇即期详情";
        } else if (prdNo == "rmbfwd") {//外汇远期
            searchByIdUrl = "/IfsForeignController/searchFwdDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmfwdEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "rmfwdApproveDetail";
            title = "外汇远期详情";
        } else if (prdNo == "rmbswap") {//外汇掉期
            searchByIdUrl = "/IfsForeignController/searchSwapDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmswapEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "rmsSwapApproveDetail";
            title = "外汇掉期详情";
        } else if (prdNo == "ccyLending") {//外币拆借
            searchByIdUrl = "/IfsForeignController/searchLendDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyLendingEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceCcyLendingApproveDetail";
            title = "外币拆借详情";
        } else if (prdNo == "ibo") {//信用拆借
            searchByIdUrl = "/IfsCfetsrmbIboController/searchIboDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/creditLoanEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "creditLoanApproveDetail";
            title = "信用拆借详情";
        } else if (prdNo == "tyibo") {//同业借款
            searchByIdUrl = "/IfsCfetsrmbIboController/searchIboDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/creditTYLoanEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "creditTYLoanDetail";
            title = "同业借款详情";
        } else if (prdNo == "fxtyibo") {//同业拆放
            searchByIdUrl = "/IfsCfetsrmbIboController/searchIboDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/creditTYLoanRuaEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "creditTYLoanRuaDetail";
            title = "同业拆放详情";
        } else if (prdNo == "ccyDepositIn") {//同业存放(外币)
            searchByIdUrl = "/IfsCCYDepositInController/searchDepositInFXFlowByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyDepositInEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceCcyDepositInDetail";
            title = "同业存放(外币)详情";
        } else if (prdNo == "ccyDepositout") {//存放同业(外币)
            searchByIdUrl = "/IfsCCYDepositOutController/searchFlowByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyDepositOutEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceCcyDepositoutDetail";
            title = "存放同业(外币)详情";
        } else if (prdNo == "rmbDepositIn") {//同业存放
            searchByIdUrl = "/IfsRmbDepositInController/searchFlowByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/rmbDepositInEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceRmbDepositInDetail";
            title = "同业存放详情";
        } else if (prdNo == "rmbDepositOut") {//存放同业
            searchByIdUrl = "/IfsRmbDepositOutController/searchFlowDepositOutRmbByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/rmbDepositOutEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceRmbDepositOutDetail";
            title = "存放同业详情";
        } else if (prdNo == "IBD") {//同业存款(线上)
            searchByIdUrl = "/IfsApprovermbDepositController/searchApprovermbDepositByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/depositEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "depositDetail";
            title = "同业存款(线上)详情";
        } else if (prdNo == "IBDOFF") {//同业存款(线下)
            searchByIdUrl = "/IfsApprovermbDepositController/searchApprovermbDepositByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/depositOfflineEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "depositOffDetail";
            title = "同业存款(线下)详情";
        } else if (prdNo == "cbt") {//现券买卖
            searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/bondTradingEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "bondTradingApproveDetail";
            title = "现券买卖详情";
        } else if (prdNo == "sl") {//债券借贷
            datas['execId'] = tradeId;
            searchByIdUrl = "/IfsCfetsrmbSlController/searchCfetsrmbSlByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/bondLendingEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "bondLendingApproveDetail";
            title = "债券借贷详情";
        } else if (prdNo == "dp") {//债券发行
            searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/depositPublishEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "depositPublishDetail";
            title = "债券发行详情";
        } else if (prdNo == "dpcd") {//存单发行
            searchByIdUrl = "/IfsCfetsrmbDpController/searchCfetsrmbDpByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/cdPublishEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "cdPublishApproveDetail";
            title = "存单发行详情";
        } else if (prdNo == "mbs") {//提前偿还(MBS)
            searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetssecur/MBSEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "MBSDetail";
            title = "提前偿还详情";
        } else if (prdNo == "cr") {//质押式回购
            searchByIdUrl = "/IfsCfetsrmbCrController/searchCrDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/pledgeRepoEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "pledgeRepoApproveDetail";
            title = "质押式回购详情";
        } else if (prdNo == "or") {//买断式回购
            searchByIdUrl = "/IfsCfetsrmbOrController/searchOrDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/repoEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "repoApproveDetail";
            title = "买断式回购详情";
        } else if (prdNo == "CRGD") {//国库定期存款
            searchByIdUrl = "/IfsCfetsrmbCrController/searchCrDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/nationalDepositEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "nationalDepositDetail";
            title = "国库定期存款详情";
        } else if (prdNo == "CRJY") {//交易所回购
            searchByIdUrl = "/IfsCfetsrmbCrController/searchCrDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/exchangeRepoEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "exchangeRepoDetail";
            title = "交易所回购详情";
        } else if (prdNo == "CRCB") {//常备借贷便利
            searchByIdUrl = "/IfsCfetsrmbCrController/searchCrDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/standingLendingEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "standingLendingDetail";
            title = "常备借贷便利详情";
        } else if (prdNo == "CRZQ") {//中期借贷便利
            searchByIdUrl = "/IfsCfetsrmbCrController/searchCrDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/MediumLendingEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "MediumLendingDetail";
            title = "中期借贷便利详情";
        } else if (prdNo == "CRZD") {//线下押券
            searchByIdUrl = "/IfsCfetsrmbCrController/searchCrDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/MicroLendingEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "MicroLendingDetail";
            title = "线下押券详情";
        } else if (prdNo == "irs") {//利率互换
            searchByIdUrl = "/IfsCfetsrmbIrsController/searchIrsDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/IRSEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "IRSApproveDetail";
            title = "利率互换详情";
        } else if (prdNo == "bfwd") {//债券远期
            searchByIdUrl = "/IfsCfetsrmbBondfwdController/searchCfetsrmbBondfwdByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/bondFwdEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "bondTradingDetail";
            title = "债券远期详情";
        } else if (prdNo == "CURYAFP") {//货基申购
            searchByIdUrl = "/CFtAfpController/searchCFtAfpById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CCurrencyFundAfpEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "CuryAfpDetail";
            title = "货币基金申购详情";
        } else if (prdNo == "CURYAFPCON") {//货基申购确认
            searchByIdUrl = "/CFtAfpConfController/searchCFtAfpConfById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CCurrencyFundAfpConfEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "CuryAfpConfDetail";
            title = "货币基金申购确认详情";
        } else if (prdNo == "CURYRDP") {//货基赎回
            searchByIdUrl = "/CFtRdpController/searchCFtRdpById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CCurrencyFundRdpEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "CuryRdpDetail";
            title = "货币基金赎回详情";
        } else if (prdNo == "CURYRDPCON") {//货基赎回确认
            searchByIdUrl = "/CFtRdpConfController/searchCFtRdpConfById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CCurrencyFundRdpConfEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "CuryRdpConfDetail";
            title = "货币基金赎回确认详情";
        } else if (prdNo == "BDAFP") {//债基申购
            searchByIdUrl = "/CFtAfpController/searchCFtAfpById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CBondFundAfpEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "BondAfpDetail";
            title = "债券基金申购详情";
        } else if (prdNo == "BDAFPCON") {//债基申购确认
            searchByIdUrl = "/CFtAfpConfController/searchCFtAfpConfById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CBondFundAfpConfEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "BondAfpConfDetail";
            title = "债券基金申购确认详情";
        } else if (prdNo == "BDRDP") {//债基赎回
            searchByIdUrl = "/CFtRdpController/searchCFtRdpById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CBondFundRdpEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "BondRdpDetail";
            title = "债券基金赎回详情";
        } else if (prdNo == "BDRDPCON") {//债基赎回确认
            searchByIdUrl = "/CFtRdpConfController/searchCFtRdpConfById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CBondFundRdpConfEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "BondRdpConfDetail";
            title = "债券基金赎回确认详情";
        } else if (prdNo == "AFP") {//专基申购
            searchByIdUrl = "/CFtAfpController/searchCFtAfpById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CFundAfpEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AfpDetail";
            title = "专户基金申购详情";
        } else if (prdNo == "AFPCON") {//专基申购确认
            searchByIdUrl = "/CFtAfpConfController/searchCFtAfpConfById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CFundAfpConfEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AfpConfDetail";
            title = "专户基金申购确认详情";
        } else if (prdNo == "RDP") {//专基赎回
            searchByIdUrl = "/CFtRdpController/searchCFtRdpById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CFundRdpEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "RdpDetail";
            title = "专户基金赎回详情";
        } else if (prdNo == "RDPCON") {//专基赎回确认
            searchByIdUrl = "/CFtRdpConfController/searchCFtRdpConfById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CFundRdpConfEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "RdpConfDetail";
            title = "专户基金赎回确认详情";
        } else if (prdNo == "REDD") {//现金分红
            searchByIdUrl = "/CFtReddController/searchCFtReddById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CFundReddEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "RdpDetail";
            title = "现金分红详情";
        } else if (prdNo == "REIN") {//红利再投
            searchByIdUrl = "/CFtReddController/searchCFtReinById";
            openJspUrl = CommonUtil.baseWebPath() + "/refund/CFundReinEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "RdpConfDetail";
            title = "红利再投详情";
        } else if (prdNo == "goldspt") {//黄金即期
            searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsmetal/goldsptEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "IrsSptApproveDetail";
            title = "黄金即期详情";
        } else if (prdNo == "goldfwd") {//黄金远期
            searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsmetal/goldfwdEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "IrsFwdApproveDetail";
            title = "黄金远期详情";
        } else if (prdNo == "goldswap") {//黄金掉期
            searchByIdUrl = "/IfsCfetsmetalGoldController/searchCfetsmetalGoldAndFlowById";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsmetal/goldswapEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "IrsSwapApproveDetail";
            title = "黄金掉期详情";
        } else if (prdNo == "goldLend") {//黄金拆借
            searchByIdUrl = "/IfsGoldLendController/searchCfetsGoldLendAndFlowById";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsmetal/GoldLendingEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "goldLendingApproveDetail";
            title = "黄金拆借详情";
        } else if (prdNo == "debt") {//外币债
            searchByIdUrl = "/IfsCfetsrmbCbtController/searchCfetsrmbCbtByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/bloomberg/fxccydebtEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "fxccydebtApproveDetail";
            title = "外币债详情";
        } else if (prdNo == "ccyswap") {//货币掉期
            searchByIdUrl = "/IfsCurrencyController/searchCcySwapAndFlowIdById";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyswapEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "CcySwapApproveDetail";
            title = "货币掉期详情";
        } else if (prdNo == "rmbopt") {//人民币期权
            searchByIdUrl = "/IfsForeignController/searchOptionDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/rmboptEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceRmBoptApproveDetail";
            title = "人民币期权详情";
        } else if (prdNo == "ccypa") {//外币头寸调拨
            searchByIdUrl = "/IfsForeignController/searchRmbCcypaAndFlowIdById";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccyPositionAllocationEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceAllcationApproveDetail";
            title = "外币头寸调拨详情";
        } else if (prdNo == "ccypspot") {//外汇对即期
            searchByIdUrl = "/IfsForeignController/searchRmbSpotAndFlowIdById";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsfx/ccypspotEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "AdvanceCcySpotApproveDetail";
            title = "外汇对即期详情";
        } else if (prdNo == "irsDerive") {//结构衍生
            searchByIdUrl = "/IfsCfetsrmbIrsController/searchIrsDetailByTicketId";
            openJspUrl = CommonUtil.baseWebPath() + "/cfetsrmb/IrsDeriveEdit.jsp?action=detail&ticketid=" + tradeId;
            id = "irsDeriveApproveDetail";
            title = "结构衍生详情";
        }
        openJspUrl = openJspUrl + "&prdNo=" + prdNo + "&fPrdCode=" + fPrdCode;
        datas.userId = userId;
        datas.branchId = branchId;
        var params = mini.encode(datas);
        CommonUtil.ajax({
            url: searchByIdUrl,
            data: params,
            callback: function (data) {
                var url = openJspUrl;
                var tab = {
                    "id": id, name: id, url: url, title: title,
                    parentId: top["win"].tabs.getActiveTab().name, showCloseButton: true
                };
                var paramData = {selectData: data.obj};
                CommonUtil.openNewMenuTab(tab, paramData);
            }
        });
    }

    /***
     * 批量审批通过
     *
     * @author lij
     *
     */

    function batchApprove() {
        var grid = mini.get("datagrid");
        var rows = grid.getSelecteds();
        if (rows.length == 0) {
            mini.alert("请至少选中一行要审批的数据!", "提示");
            return;
        }
        mini.confirm("确认要批量审批吗？", "确认", function (action) {
            if (action != "ok") {
                return;
            }
            mini.open({
                targetWindow: window,
                url: "./Common/Flow/BatchApprove.jsp",
                title: "批量审批",
                width: 600,
                height: 400,
                onload: function () {
                    var iframe = this.getIFrameEl();
                    var data = {rows: rows};
                    iframe.contentWindow.SetData(data);
                },
                ondestroy: function (action) {
                    if (action == "close" || action == undefined) {
                        grid.reload();
                        return;
                    }
                    var iframe = this.getIFrameEl();
                    var batchParams = mini.clone(action);    //必须。克隆数据。
                    //流程审批
                    CommonUtil.ajax({
                        url: "/IfsFlowController/approveBatchPass",
                        data: {"batchParams": batchParams},
                        loadingMsg: "批量提交审批中...<br>逐笔处理中,请勿关闭刷新!",
                        callback: function (data) {
                            if (data.code != "RuntimeException") {
                                setTimeout(function () {
                                    mini.alert(data.desc, "系统提示", function () {
                                        grid.reload();
                                    });
                                }, 200);
                            }
                        }
                    });
                }
            });
        }); // end confirm
    }

    // 导出报表
    function exportExcel() {
        var content = grid.getData();
        if (content.length == 0) {
            mini.alert("请先查询数据");
            return;
        }
        var form = new mini.Form("search_form");
        var data = form.getData(true);
        data['userId'] = userId;
        data['approveType'] = mini.get("approveType").getValue();

//    		var approveType = mini.get("approveType").getValue();
//    		var contractId = mini.get("contractId").getValue();
//    		var tradeType = mini.get("tradeType").getValue();
//    		var prdName = mini.get("prdName").getValue();
//    		var custName = mini.get("custName").getValue();

//    		var tradeDate = data['tradeDate'];
//    		var approveDate=data['approveDate'];
        mini.confirm("您确认要导出Excel吗?", "系统提示",
            function (action) {
                if (action == "ok") {
//    					var data = {approveType:approveType,userId:userId,contractId:contractId,tradeType:tradeType,prdName:prdName,custName:custName,
//    							tradeDate:tradeDate,approveDate:approveDate};
                    var fields = null;
                    for (var id in data) {
                        fields += '<input type="hidden" id="' + id + '" name="' + id + '" value="' + data[id] + '">';
                    }
                    var urls = CommonUtil.pPath + "/sl/IfsExportController/exportExcel";
                    $('<form action="' + urls + '" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
                }
            }
        );
    }

    //加载产品列表
    function queryProducts() {
        var tradeType = mini.get("tradeType").getValue();
        if (tradeType == "0") {
            params = "IFS_APPRO";
        } else if (tradeType == "1") {
            params = "IFS_CFET";
        }
        CommonUtil.ajax({
            data: {tables: params},
            url: "/IfsApproveElementController/searchTables",
            callback: function (data) {
                var prdTypeNo = mini.get("prdName");
                prdTypeNo.setData(data.obj);
            }
        });
    }

    function itemclick() {
        var tradeType = mini.get("tradeType").getValue();
        if (tradeType == null || tradeType == "") {
            mini.alert("请先选择操作类型");
        }
    }

    //打印
    /* function print(){
		var selections = grid.getSelecteds();
		if(selections == null || selections.length == 0){
			mini.alert('请选择一条要打印的数据！','系统提示');
			return false;
		}else if(selections.length>1){
			mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！','系统提示');
			return false;
		}
		var canPrint = selections[0].ticketId;
		var prdName = selections[0].prdName; // 产品名称
		if(!CommonUtil.isNull(canPrint)){
			if(prdName == "外汇即期"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.rmbspt +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "外汇远期"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.rmfwd +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "外汇掉期"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.rmswap +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "人民币期权"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.rmbopt +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "外币拆借"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.ccyLending +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "利率互换"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.IRS +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "结构衍生"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.irsDerive +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "买断式回购"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.repo +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "现券买卖"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.bondTrading +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "信用拆借"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.creditLoan +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "债券借贷"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.bondLending +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "质押式回购"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.pledgeRepo +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "债券发行"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.depositPub +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "外币债"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.fxccydept +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "黄金即期"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.fxccydept +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "黄金远期"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.fxccydept +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
			if(prdName == "黄金掉期"){
				var actionStr = CommonUtil.pPath + "/sl/IfsPrintUtilController/exportload/print/"+PrintNo.fxccydept +"/" + canPrint;
				$('<form target="_blank" action=' + actionStr + ' method="post"></form>').appendTo('body').submit().remove();
			}
		};
	} */



    //打印
    function print() {
        var selections = grid.getSelecteds();
        if (selections == null || selections.length == 0) {
            mini.alert('请选择一条要打印的数据！', '系统提示');
            return false;
        } else if (selections.length > 1) {
            mini.alert('暂不支持多条数据同时打印，请选择一条要打印的数据！', '系统提示');
            return false;
        }
        var ticketId = selections[0].ticketId;
        var prdName = selections[0].prdName; // 产品名称
        if (!CommonUtil.isNull(ticketId)) {
            if (prdName == "外汇即期") {
                var canPrint = ticketId + '|' + '411' + '|' + prdName;
                if (!CommonUtil.isNull(canPrint)) {
                    openFullScreen("./Common/print/printContractApprove.jsp?ticketId=" + canPrint);
                }
                ;
            }
            if (prdName == "外汇远期") {
                var canPrint = ticketId + '|' + '391' + '|' + prdName;
                if (!CommonUtil.isNull(canPrint)) {
                    openFullScreen("./Common/print/printContractApprove.jsp?ticketId=" + canPrint);
                }
                ;
            }
            if (prdName == "外汇掉期") {
                var canPrint = ticketId + '|' + '432' + '|' + prdName;
                if (!CommonUtil.isNull(canPrint)) {
                    openFullScreen("./Common/print/printContractApprove.jsp?ticketId=" + canPrint);
                }
                ;
            }
            if (prdName == "外币拆借") {
                var canPrint = ticketId + '|' + '438' + '|' + prdName;
                if (!CommonUtil.isNull(canPrint)) {
                    openFullScreen("./Common/print/printContractApprove.jsp?ticketId=" + canPrint);
                }
                ;
            }
            if (prdName == "黄金即期") {
                var canPrint = ticketId + '|' + '447' + '|' + prdName;
                if (!CommonUtil.isNull(canPrint)) {
                    openFullScreen("./Common/print/printContractApprove.jsp?ticketId=" + canPrint);
                }
                ;
            }
            if (prdName == "黄金远期") {
                var canPrint = ticketId + '|' + '448' + '|' + prdName;
                if (!CommonUtil.isNull(canPrint)) {
                    openFullScreen("./Common/print/printContractApprove.jsp?ticketId=" + canPrint);
                }
                ;
            }
            if (prdName == "黄金掉期") {
                var canPrint = ticketId + '|' + '449' + '|' + prdName;
                if (!CommonUtil.isNull(canPrint)) {
                    openFullScreen("./Common/print/printContractApprove.jsp?ticketId=" + canPrint);
                }
                ;
            }

        }
    }

    function tips() {
        CommonUtil.ajax({
            url: "/IfsMessageTopicController/sendhbsxtx",
            data: {"userId":userId,"roleIds": roleIds, "moduleId": "302-1-1"},//还本收息提醒菜单
            showLoadingMsg: true,
            callback: function (data) {}
        });
    }
</script>
</html>