<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>
<html>
<head>
    <script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>

<body style="width: 100%; height: 100%; background: white">

<div id="mainGrid" class="mini-datagrid borderAll" style="width:100%;height:80%;" idField="id" multiSelect="true"
     allowAlternating="true" allowResize="true" sortMode="client" allowAlternating="true" onshowrowdetail="onShowRowDetail">
    <div property="columns">
        <div type="indexcolumn" width="50px" headerAlign="center">序号</div>
        <div type="expandcolumn"></div>
        <div field="name" width="300px" align="left"  headerAlign="center" >缓存名称</div>
        <div field="hitRatio" width="150"  headerAlign="center" align="center" >命中率(%)</div>
        <div field="maxSize" width="120"  headerAlign="center" align="center" >最大阀值</div>
        <div field="size" width="120"  headerAlign="center" align="center" >当前大小值</div>
        <div field="stust" width="300px" align="left"  headerAlign="center" >缓存状态</div>
        <div field="tTldles" width="150"  dateFormat="yyyy-MM-dd HH:mm:ss" headerAlign="center" align="center" >闲置时间</div>
        <div field="tTLives" width="120"  dateFormat="yyyy-MM-dd HH:mm:ss" headerAlign="center" align="center" >存活时间</div>
        <div field="hit" width="120"  headerAlign="center" align="center" >命中记录数</div>
        <div field="expire" width="300px" align="left"  headerAlign="center" >失效记录数</div>
        <div field="notFound" width="150"  headerAlign="center" align="center" >未找到记录数</div>

    </div>
</div>
<div id="childGrid" class="mini-datagrid borderAll" style="display: none;" >
		<div property="columns">
			<div type="indexcolumn"></div>
			<div field="key" width="70" headerAlign="center" align="center">缓存键</div>    
            <div field="value" width="300px" align="left"  headerAlign="center" >缓存值</div>
            <div field="creationTime" width="300px"  renderer="ondayRenderer" align="left"  headerAlign="center" >创建时间</div>
            <div field="lastUpdateTime" width="300px"  renderer="ondayRenderer" align="left"  headerAlign="center" >最后更新时间</div>
		</div>
	</div>
<script>
    mini.parse();
    //获取当前tab
    var mainGrid=mini.get("mainGrid");
    var childGrid=mini.get("childGrid");
  

    $(document).ready(function() {
        search(10, 0);
        //设置详情信息
        childGrid.on("beforeload", function (e) {
            var row=mainGrid.getSelected();
            e.cancel = true;
            childGrid.setData(row.cacheElementLst);
        });
        //设置主页面信息
        mainGrid.on("beforeload", function (e) {
            e.cancel = true;
            var pageIndex = e.data.pageIndex; 
            var pageSize = e.data.pageSize;
            mainGrid.setTotalCount(data.obj.length);
            mainGrid.setPageIndex(pageIndex);
            mainGrid.setPageSize(data.obj.length);
            mainGrid.setData(data.obj);
        });
       
    });


    // 初始化数据
    function query(){
        search(mainGrid.pageSize, 0);
    }
    //查询
    function search(pageSize,pageIndex){
        var data= {};
        data['pageNumber']=pageIndex+1;
        data['pageSize']=pageSize;
        data['branchId']=branchId;
        var url="/CacheManageController/getEhCache";
        var params = mini.encode(data);
        CommonUtil.ajax({
            url:url,
            data:params,
            callback : function(data) {
                 //设置主页面信息
                 mainGrid.setTotalCount(data.obj.length);
                 mainGrid.setPageIndex(pageIndex);
                 mainGrid.setPageSize(data.obj.length);
                 mainGrid.setData(data.obj);
            }
        });
    }
    
	function onShowRowDetail(e){
		var grid=e.sender;
		var row=e.record;
		var td = grid.getRowDetailCellEl(row);
		var childGrid=mini.get("childGrid");
        childGrid.setData(row.cacheElementLst);
		var gridEl=childGrid.getEl();
		td.appendChild(gridEl);
		gridEl.style.display = "block";
	}

    function ondayRenderer(e) {
        var value = new Date(e.value);
        if (value) return mini.formatDate(value,"yyyy-MM-dd HH:mm:ss");
    }
</script>
</body>
</html>
