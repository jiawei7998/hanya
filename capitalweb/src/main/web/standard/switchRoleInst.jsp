<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./global.jsp"%>


<div id="PasswordChange" class="mini-fit">
    <table >
        <tr>
            <td>
                <input class="mini-combobox" onvaluechanged="onDeptChanged" emptyText="请选择机构" id="instId" style="width:200px" name="instId"  textField="instName" valueField="instId" emptyText="请选择机构名称" errorMode="border" required="true" />
            </td>
        </tr>
        <tr>
            <td>
                <input class="mini-combobox" emptyText="请选择角色" id="roleId" style="width:200px" name="roleId"  textField="roleName" valueField="roleId"  emptyText="请选择角色名称" errorMode="border" required="true" />
            </td>
        </tr>
        <tr>
            <td>
                <a id="confirm_btn" class="mini-button" style="display: none"  >确认</a>
            </td>
        </tr>
    </table>
</div>
<script>
    mini.parse();

    CommonUtil.ajax({
        url:"/InstitutionController/getInstByUser",
        data:{'branchId':branchId,'userId':userId},
        callback:function(data){
            mini.get('instId').setData(data.obj);
            }
        });

    function onDeptChanged(e){
        
        CommonUtil.ajax({
            url:"/RoleController/getRoleByUserIdAndInstId",
            data:{'branchId':branchId,'userId':userId,'instId':e.value},
            callback:function(data){
                mini.get('roleId').setData(data.obj);
                }
            });
      } 

    //切换session
    mini.get("confirm_btn").on("click",function(){
        mini.get("confirm_btn").setEnabled(false);

        var form = new mini.Form("#PasswordChange");
			form.validate();
			if (form.isValid() == false) return;
				var param = form.getData(); //获取表单多个控件的数据
        var messageid = mini.loading("系统正在处理...", "请稍后");
        try {
            CommonUtil.ajax({
                url:'/UserController/swithRoleInst',
                data:mini.encode(param),
                relbtn:"confirm_btn",
                callback:function(data){
                    mini.alert("切换成功.","提示",function(){
                        //关闭自己
                        window.CloseOwnerWindow("ok");
                        
                    });
                    
                }
            });

        } catch (error) {
            
        }
        mini.hideMessageBox(messageid);
        mini.get("confirm_btn").setEnabled(true);
    });




</script>