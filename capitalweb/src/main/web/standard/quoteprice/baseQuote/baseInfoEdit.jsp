<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div style="width:70%;height:70%;" >
	<div  id="field_form"  class="mini-fit area" style="background:white">
			
		
			<fieldset>
		  	<legend>工作时间设置</legend>
			<input id="startTime" name="startTime" class="mini-timespinner" labelField="true"  label="开始时间：" format="H:mm:ss" style="width:50%;"  labelStyle="text-align:left;" />
			<input id="endTime" name="endTime" class="mini-timespinner" labelField="true"  label="结束时间：" format="H:mm:ss" style="width:50%;"  labelStyle="text-align:left;" />
			</fieldset>

			<fieldset>
		  	<legend>系统内平盘方式设置</legend>
			<input id="rmbpType" name="rmbpType" class="mini-combobox" labelField="true"  label="系统内平盘方式设置："  style="width:50%;" data="CommonUtil.serverData.dictionary.rmbpType" labelStyle="text-align:left;" />
			</fieldset>
			

			<fieldset>
		  	<legend>中间价策略设置</legend>
			<input id="mpType" name="mpType" class="mini-combobox" labelField="true"  label="中间价策略设置："  style="width:50%;" data="CommonUtil.serverData.dictionary.mpType" labelStyle="text-align:left;" />
			</fieldset>	
    
			<fieldset>
		  	<legend>其他设置</legend>
			<input id="msgCount" name="msgCount" class="mini-spinner" labelField="true"  label="同类型异常短信发送条数："  style="width:50%;" changeOnMousewheel='false' labelStyle="text-align:left;" />
			</fieldset>	
		</div>
		</div>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;" width="70%" align="center">
		<div style="margin-bottom:10px;" width="100%">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">保存交易</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭界面</a>
		</div>
	</div>
	
		<script type="text/javascript">
			mini.parse();
			function inner(){
				var from = new mini.Form("field_form");
				CommonUtil.ajax({
					url:'/baseQuoteController/sercherBaseInfo',
					data:{},
					callback : function(data) {
						from.setData(data.obj);
					}
				});
			}
			function save(){
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true);
				var json = mini.encode(data); 
				CommonUtil.ajax({
					url:'/baseQuoteController/updateBaseInfo',
					data:json,
					callback : function(data) {
						mini.alert("保存成功");
						form.setData(data.obj);
					}
				});
				
			}
			function close() {
				top["win"].closeMenuTab();
			}
			$(document).ready(function() {
				//控制按钮显示
				$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
					inner();
				});
			});
		</script>		
</body>
</html>
