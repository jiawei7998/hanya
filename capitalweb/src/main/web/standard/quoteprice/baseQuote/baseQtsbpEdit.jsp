<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div style="width:100%;height:100%;" showCollapseButton="false">
				
<h1 style="text-align:center"></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<table >
		<tr>
			<input id="id" name="id" class="mini-textbox" labelField="true"  label="编号："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="sid" name="sid" class="mini-textbox" labelField="true"  label="报价源："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="ccyp" name="ccyp" class="mini-textbox" labelField="true"  label="货币对："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="pop" name="pop" class="mini-textbox" labelField="true"  label="价差方式："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="" name="" class="mini-textbox" labelField="true"  label="数据源报价单位："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="cbuybp" name="cbuybp" class="mini-textbox" labelField="true"  label="过滤钞买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="csalebp" name="csalebp" class="mini-textbox" labelField="true"  label="过滤钞卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="rbuybp" name="rbuybp" class="mini-textbox" labelField="true"  label="过滤汇买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="rsalebp" name="rsalebp" class="mini-textbox" labelField="true"  label="过滤汇卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="cbuybpw" name="cbuybpw" class="mini-textbox" labelField="true"  label="过滤钞买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="csalebpw" name="csalebpw" class="mini-textbox" labelField="true"  label="过滤钞卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="rbuybpw" name="rbuybpw" class="mini-textbox" labelField="true"  label="过滤汇买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="rsalebpw" name="rsalebpw" class="mini-textbox" labelField="true"  label="过滤汇卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="note" name="note" class="mini-textbox" labelField="true"  label="备注："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
	</table>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">保存</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭</a>
		</div>
	</div>
		
</div>	


	</div>
		<script type="text/javascript">
			mini.parse();
			var Data = "";
			 function SetData(data) {
		            if (data.action == "edit") {
		                //跨页面传递的数据对象，克隆后才可以安全使用
		                data = mini.clone(data);
						var form =new mini.Form("field_form");
						form.setData(data.row);
						Data = data.row;
						mini.get("id").setEnabled(false);
						mini.get("sid").setEnabled(false);
						mini.get("ccyp").setEnabled(false);
		            }
		           
		        }
			function save(){
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var url = "";
				if(Data==""){
					url = '/baseQuoteQtsbpController/saveQtsbp'
				}else {
					url = '/baseQuoteQtsbpController/updateQtsbp'
				}
				var form = new mini.Form("field_form");
				var data = form.getData(true);
				var json = mini.encode(data); 
				CommonUtil.ajax({
					url : url,
					data : json,
					callback : function(data) {
						mini.alert("保存成功",'提示信息',function(){
							CloseOwnerWindow();
						});
					}
				});
			}

			function close() {
				CloseOwnerWindow();
			}
	
			 

		</script>
</body>
</html>
