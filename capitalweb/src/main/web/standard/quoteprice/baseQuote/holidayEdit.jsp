<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
	<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
</head>
<body style="width: 100%; height: 100%; background: white">
	<table id="field_form" class="mini-table" align="center">
	<tr>
		<td>
			<input id="name" name="name" class="mini-textbox" labelField="true"  label="假期名称："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" required="true" />
		</td>
	</tr>
	<tr>
		<td>
 			<input id="startDate" name="startDate" class="mini-datepicker" labelField="true"  label="开始日期："  style="width:100%;" labelStyle="text-align:left;" required="true" />
		</td>
	</tr>
	<tr>
		<td>
			<input id="endDate" name="endDate" class="mini-datepicker" labelField="true"  label="结束日期："  style="width:100%;"  labelStyle="text-align:left;" required="true" />
		</td>
	</tr>
	</table>
	<div id="functionIds" showCollapseButton="true" style="padding-top:30px;" width="70%">
		<div style="margin-bottom:10px;" width="100%">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">保存交易</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭界面</a>
		</div>
	</div>
	<script type="text/javascript">
			mini.parse();
			var form =new mini.Form("field_form");
			var Data = "";
			
			 function SetData(data) {
		            if (data.action == "edit") {
		                //跨页面传递的数据对象，克隆后才可以安全使用
		                data = mini.clone(data);
						var form =new mini.Form("field_form");
						Data = data.row;
						mini.get("name").setEnabled(false);
						form.setData(data.row);
		            }
		           
		        }
			function save(){
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var url = "";
				
				var form = new mini.Form("field_form");
				var data = form.getData(true);
				data['updateUser'] = userId;
				data['updateDate'] = sysDate;
				var json = mini.encode(data); 
				if(Data==""){
					/* CommonUtil.ajax({
						url : '/baseQuoteController/checkHoliday',
						data : json,
						callback : function(data) {
							if(data.obj){
							mini.alert("已存在数据，请检查",'提示信息',function(){
								return;
							});
							
							}else{
								CommonUtil.ajax({
									url : '/baseQuoteController/saveHoliday',
									data : json,
									callback : function(data) {
										mini.alert("保存成功",'提示信息',function(){
											CloseOwnerWindow();
										});
									}
								});
							}
						}
					}); */
					url = '/baseQuoteController/saveHoliday'
				}else {
					url = '/baseQuoteController/updateHoliday'
				}

				CommonUtil.ajax({
					url : url,
					data : json,
					callback : function(data) {
						mini.alert("保存成功",'提示信息',function(){
							CloseOwnerWindow();
						});
					}
				});
			}
			function close() {
				CloseOwnerWindow();
			}
			</script>
</body>

</html>