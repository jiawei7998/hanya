<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title></title>

</head>
<body style="width:100%;height:100%;background:white;text-align:center;">
    <h1></h1>
        <div style="width:100%;height:70%; " title="角色列表" >
        <div id="listbox1" class="mini-listbox" title="全部"  textField="roleName" valueField="roleId" style="width:47%;height:100%;float: left"  showCheckBox="true" multiSelect="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="ccyp" header="未关注的货币对" headerAlign="center" ></div>

            </div>
        </div>
        <div style="float: left;width: 5%;text-align:center; ">
            <div style="margin-bottom:10px; margin-top:50px"><a class="mini-button" style="display: none"  style="width: 40px;"  onclick="add">&gt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="addAll">&gt;&gt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="removeAll">&lt;&lt;</a></div>
            <div style="margin-bottom:10px; "><a class="mini-button" style="display: none"  style="width: 40px;" onclick="removes">&lt;</a></div>
        </div>
        <div id="listbox2" class="mini-listbox" style="width:47%;height:100%;float: left"  textField="roleName" valueField="roleId" showCheckBox="true" multiSelect="true">
            <div property="columns">
                <div type="indexcolumn" headerAlign="center" align="center">序列</div>
                <div field="ccyp" header="关注的货币对" headerAlign="center"></div>

            </div>
        </div> 
    </div>  
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">保存</a>
    <script type="text/javascript">
        mini.parse();
        var listbox2 = mini.get("listbox2");
        var listbox1 = mini.get("listbox1");
		function search(){
			var data = {};
		    listbox1.setData();
		     listbox2.setData();
			data['branchId']=branchId;
			data['userId']=userId;
			var json = mini.encode(data);
			 CommonUtil.ajax({
		            url: '/baseQuoteUccypController/searchAllUccyp',
		            data: json,
		            callback: function (data) {
		                listbox1.setData(data.obj.left);
		                listbox2.setData(data.obj.right);
		            }
		        });
		}
        function add() {
            var items = listbox1.getSelecteds();
            listbox1.removeItems(items);
            listbox2.addItems(items);
        }
        function addAll() {
            var items = listbox1.getData();       
            listbox1.removeItems(items);
            listbox2.addItems(items);
        }
        function removes() {
            var items = listbox2.getSelecteds();
            listbox2.removeItems(items);
            listbox1.addItems(items);
        }
        function removeAll() {
            var items = listbox2.getData();
            listbox2.removeItems(items);
            listbox1.addItems(items);
        }
       
        function save() {
            var data1 = listbox1.getData();
            var data2 = listbox2.getData();
            //data2['uid1'] = userId;
                var recordList = listbox2.getData();
                var str = "";
                $.each(recordList, function (i, n) {
                    str += n.ccyp + ",";
                });
                CommonUtil.ajax({
    	            url: '/baseQuoteUccypController/saveAllUccyp',
    	            data: {ccyp:str,userid:userId,branchid:branchId},
    	            callback: function (data) {
    	            	mini.alert("保存成功",'提示信息');
    	            }
    	        });
           
        }
        $(document).ready(function() {
            //控制按钮显示
            $.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
                search();
            });
		});
    </script>

   
</body>
</html>