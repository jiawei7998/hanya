<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/miniui/res/ajaxfileupload.js"></script>
<title></title>

</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>外汇远掉点查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="ccy" name="ccy" class="mini-textbox" width="320px" labelField="true" width="280px" label="货币对：" emptyText="货币对" labelStyle="text-align:center;" />
			<input id="postdate" name="postdate" class="mini-datepicker" width="320px" labelField="true" width="280px" label="汇率日期：" emptyText="汇率日期" labelStyle="text-align:center;" format="yyyy-MM-dd"  />
			<input id="isBuy" name="isBuy" class="mini-combobox" width="320px" labelField="true" width="280px" data="CommonUtil.serverData.dictionary.isBuy" label="汇率类型：" emptyText="汇率类型"  />

			<span style="float: right; margin-center: 150px">
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
				<a  id="export_btn" class="mini-button"   style="display: none"  onclick="ExcelExport()">下载模板</a>
				<a  id="upload_btn" class="mini-button"  style="display: none"  onclick="ExcelImport()">上传市场数据</a>
			</span>
		</div>
	</fieldset>
</span>	
	<div id="MiniSettleForeignSpot" style="margin-top: 2px;height:90%">
			<div id="datagrid" class="mini-datagrid borderAll" allowAlternating="true" style="width:100%;height:97.5%;" sortMode="client" fitColumns="false" frozenStartColumn="0" multiSelect="true"  frozenEndColumn="0">
			<div property="columns">
				<div field="ccy" width="100" allowSort="false" headerAlign="center" align="center">货币对</div>
				<div field="postdate" width="120" allowSort="false" headerAlign="center" align="center" renderer="onDateRenderer" >汇率日期</div>
				<div field="isBuy" width="120" allowSort="false" headerAlign="center" align="center" renderer="CommonUtil.dictRenderer" data-options="{dict:'isBuy'}" >汇率类型</div>
				<div field="spotRate" width="120" align="center" headerAlign="center" >SPOT（即期汇率）</div>
				<div field="period1Rate" width="120" align="center" headerAlign="center" >T（交易日）</div>
				<div field="period2Rate" width="120" allowSort="false" headerAlign="center" align="center">T+1（1天）</div>
				<div field="period3Rate" width="120" allowSort="false" headerAlign="center" align="center">T+3（3天）</div>
				<div field="period4Rate" width="120" allowSort="false" headerAlign="center" align="center" >1W（1周）</div>
				<div field="period5Rate" width="120" allowSort="false" headerAlign="center" align="center" >2W（2周）</div>
				<div field="period6Rate" width="120" allowSort="false" headerAlign="center" align="center">1M（1月）</div>
				<div field="period7Rate" width="120" allowSort="false" headerAlign="center" align="center">2M（2月）</div>
				<div field="period8Rate" width="120" allowSort="false" headerAlign="center" align="center" >3M（3月）</div>
				<div field="period9Rate" width="120" allowSort="false" headerAlign="center" align="center" >6M（6月）</div>
				<div field="period10Rate" width="120" allowSort="false" headerAlign="center" align="center" >9M（9月）</div>
				<div field="period11Rate" width="120" allowSort="false" headerAlign="center" align="center" >1Y（1年）</div>
				<div field="period12Rate" width="120" allowSort="false" headerAlign="center" align="center" >2Y（2年）</div>
				<div field="period13Rate" width="120" allowSort="false" headerAlign="center" align="center" >3Y（3年）</div>
				<div field="period14Rate" width="120" allowSort="false" headerAlign="center" align="center" >4Y（4年）</div>
				<div field="period15Rate" width="120" allowSort="false" headerAlign="center" align="center" >5Y（5年）</div>
			</div>
		</div>
	</div>
<%--	<div id="fileMsg" class="mini-panel" title="市场数据信息上传" style="width:100%"  allowResize="true" collapseOnTitleClick="true">	--%>
<%--	<div class="leftarea">--%>
<%--		<input class="mini-htmlfile" name="Fdata"  id="Fdata" labelField="true"  label="选择上传文件："  emptyText="请选择要上传的文件" --%>
<%--        style="width:100%" labelStyle="text-align:left;width:170px"/>--%>
<%--	</div>--%>
<%--	<div class="centerarea">--%>
<%--		<a class="mini-button" style="display: none"    onclick="upload()" style="margin-left: 30px;" id="upload_btn">上传</a>--%>
<%--	</div>	--%>
</div>
	<script>
		mini.parse();

		var grid = mini.get("datagrid");
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		
		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}

			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
	
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/IfsForwardExchangeRateController/searchPageRate',
				data:params,
				callback : function(data) {
					if(data.obj){
						grid.setTotalCount(data.obj.total);
						grid.setPageIndex(pageIndex);
				        grid.setPageSize(pageSize);
						grid.setData(data.obj.rows);
					}
				}
			});
		}
		function clear(){
            var form=new mini.Form("search_form");
            form.clear();
            search(10,0);

		}
		function query (){
			search(10,0);
		}
		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				query();
				var innerInterval;
				innerInitFunction = function () {
					clearInterval(innerInterval);
					search(10, 0);
				}, innerInterval = setInterval("innerInitFunction()", 5 * 60 * 1000);
			});
		});
		//上传
	/*	   function upload(){
		       var fdata = mini.get("Fdata"), value =fdata.getValue(),file = $("input[name='Fdata']")[0];
		       if(!value||value==""){
		       	mini.alert("请选择上传文件", "消息");
		       	return;
		       }
		       var messageid = mini.loading("加载中,请稍后... ", "提示信息");
		       $.ajaxFileUpload({
		           url: CommonUtil.pPath + '/sl/IfsForwardExchangeRateController/ImportRateByExcel',         //用于文件上传的服务器端请求地址
		           fileElementId:file,
		           //文件上传域的ID
		           // data:{},
		           dataType: 'text',                                     //返回值类型 一般设置为json
		           success: function (data, status)                     //服务器成功响应处理函数
		           {
		        	   data=jQuery(data).text();
		        	   mini.hideMessageBox(messageid);
		        	   if(data=="操作成功"){
		        		 //上传成功触发牌价更新插入事件
				        	 CommonUtil.ajax({
								url:'/IfsForwardExchangeRateController/ImportRateByExcel',
								data:{},
								callback : function(data) {
									mini.alert(data.obj.retMsg, "消息", function() {
										//fdata.setText("");

									});
								}
							});
		        	   }else{
		        		   mini.alert(data, "消息", function() {
								//fdata.setText("");
								window.location.reload();
							});
		        	   }
					},
					error : function(data, status, e) //服务器响应失败处理函数
					{
						mini.hideMessageBox(messageid);
						mini.alert("上传失败：" + e, "消息",function(){
							fdata.setText("");
							//window.location.reload();
						});
					}
				});
		   }

    */

		//市场数据文件导入
		function ExcelImport(){
			mini.open({
				showModal: true,
				allowResize: true,
				url: CommonUtil.baseWebPath() +"/../quoteprice/baseQuote/queryQuotePriceImport.jsp",
				width: 420,
				height: 300,
				title: "Excel导入",
				ondestroy: function (action) {
					search(10,0);//重新加载页面
				}
			});
		}

		function onDateRenderer(e) {
			var value = new Date(/\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)==null ? e.value : /\d{4}-\d{1,2}-\d{1,2}/g.exec(e.value)[0].replace(/-/g,'/'));
			if (value) return mini.formatDate(value, 'yyyy-MM-dd');
		}



		//导出模板
		function ExcelExport() {
//        if(grid.totalCount=="0"){
//            mini.alert("表中无数据");
//            return;
//        }
			mini.confirm("您确认要导出Excel吗?","系统提示",
					function (action) {
						if (action == "ok"){
							var form = new mini.Form("#search_form");
							var data = form.getData(true);
							var fields = null;
							for(var id in data){
								fields += '<input type="hidden" id="' + id + '" name="'+ id +'" value="' + data[id] + '">';
							}
							var urls = CommonUtil.pPath + "/sl/IfsForwardExchangeRateController/forwardRateExcel  ";
							$('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
						}
					}
			);
		}


	</script>
</body>
</html>