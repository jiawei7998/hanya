<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div style="width:100%;height:100%;"  showCollapseButton="false">
				
<h1 style="text-align:center"></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<table  width="100%" class="mini-table">
		<tr>
			<input id="exptcode" name="exptcode" class="mini-textbox" labelField="true"  label="异常代码："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</tr>
		<tr>
			<input id="exptname" name="exptname" class="mini-textbox" labelField="true"  label="异常名称："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="dealtype" name="dealtype" class="mini-checkboxlist" labelField="true"  label="异常类型：" data="CommonUtil.serverData.dictionary.expt" style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		<tr>
			<input id="muser" name="muser" class="mini-textbox" labelField="true"  label="角色接收："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
	</table>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">保存</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭</a>
		</div>
	</div>
		
</div>	


	</div>
		<script type="text/javascript">
			mini.parse();
			var Data = "";
			 function SetData(data) {
		            if (data.action == "edit") {
						var form =new mini.Form("field_form");
						Data = data.row;
						form.setData(data.row);
						mini.get("exptcode").setEnabled(false);
						mini.get("exptname").setEnabled(false);
		            }
		           
		        }
			function save(){
				
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var data = form.getData(true);
				var json = mini.encode(data);
				CommonUtil.ajax({
					url : '/baseQuoteExptController/updateExpt',
					data : json,
					callback : function(data) {
						mini.alert("保存成功",'提示信息',function(){
							CloseOwnerWindow();
						});
					}
				});
			}

			function close() {
				CloseOwnerWindow();
			}
	
			 

		</script>
</body>
</html>
