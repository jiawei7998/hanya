<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div style="width:100%;height:100%;"  showCollapseButton="false">
				
<h1 style="text-align:center"></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<table  width="100%" class="mini-table">
		<tr>
		<td>
			<input id="ccyp" name="ccyp" class="mini-textbox" labelField="true"  label="名称："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="quoteunits" name="quoteunits" class="mini-textbox" labelField="true"  label="报价单位："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="peoplepriceunit" name="peoplepriceunit" class="mini-textbox" labelField="true"  label="最大波动价差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="wgjrxzjc" name="wgjrxzjc" class="mini-textbox" labelField="true"  label="外管局现限制价差（%）："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		<td>
			<input id="ccyname" name="ccyname" class="mini-textbox" labelField="true"  label="波动价差方式："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="pricenum" name="pricenum" class="mini-textbox" labelField="true"  label="报价精度："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="maxwfb" name="maxwfb" class="mini-textbox" labelField="true"  label="最大波动万分比：" data="CommonUtil.serverData.dictionary.qtsStatus" style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="wgjcxzjc" name="wgjcxzjc" class="mini-textbox" labelField="true"  label="外管局汇限制价差（%）："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		</tr>
		<tr>
					<input id="note" name="note" class="mini-textbox" labelField="true"  label="备注："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
		</tr>
		
	</table>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">保存</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭</a>
		</div>
	</div>
		
</div>	


	</div>
		<script type="text/javascript">
			mini.parse();
			 function SetData(data) {
		            if (data.action == "edit") {
						var form =new mini.Form("field_form");
						form.setData(data.row);
						mini.get("ccyp").setEnabled(false);
		            }
		           
		        }
			function save(){
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var data = form.getData(true);
				data['isused']="1";
				var json = mini.encode(data);
				CommonUtil.ajax({
					url : '/baseQuoteCcypairController/updateCcypair',
					data : json,
					callback : function(data) {
						mini.alert("保存成功",'提示信息',function(){
							CloseOwnerWindow();
						});
					}
				});
			}

			function close() {
				CloseOwnerWindow();
			}
	
			 

		</script>
</body>
</html>
