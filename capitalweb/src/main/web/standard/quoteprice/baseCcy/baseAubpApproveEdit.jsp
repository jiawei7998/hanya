<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div style="width:100%;height:100%;"  showCollapseButton="false">
				
<h1 style="text-align:center"></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<table  width="100%" class="mini-table">
	<tr>
		<td>
			<input id="auser" name="auser"   class="mini-textbox" labelField="true"  label="申请人："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="ccyp" name="ccyp" class="mini-textbox"   labelField="true"  label="货币对/系统源："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			
			<input id="cbuybp" name="cbuybp" class="mini-textbox"   labelField="true"  label="钞买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuybp" name="rbuybp" class="mini-textbox"   labelField="true"  label="汇买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			
			<input id="cbuywfb" name="cbuywfb" class="mini-textbox"   labelField="true"  label="钞买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuywfb" name="rbuywfb" class="mini-textbox"   labelField="true"  label="汇买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="shnote" name="shnote" class="mini-textbox"   labelField="true"  label="审核说明："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			
		</td>
		<td>
			<input id="atime" name="atime" class="mini-combobox"   labelField="true"  label="申请时间："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="webpage" name="webpage" class="mini-textbox"   labelField="true"  label="点差方式："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />

			<input id="csalebp" name="csalebp" class="mini-textbox"   labelField="true"  label="钞卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			
			<input id="rsalebp" name="rsalebp" class="mini-textbox"   labelField="true"  label="汇卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="csalewfb" name="csalewfb" class="mini-textbox"   labelField="true"  label="钞卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rsalewfb" name="rsalewfb" class="mini-textbox"   labelField="true"  label="汇卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="shresult" name="shresult" class="mini-textbox"   labelField="true"  label="审核结果：" style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
	</tr>
	</table>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">提交</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭</a>
		</div>
	</div>
		
</div>	


	</div>
		<script type="text/javascript">
			mini.parse();
			var Data = "";
			 function SetData(data) {
						var form =new mini.Form("field_form");
						Data = data.row;
						form.setData(data.row);
		           
		        }
			function save(){
				
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				if(userId==Data.auser){
					mini.alert("不能审批自己提交的数据",'提示信息',function(){
						CloseOwnerWindow();
					});
					return;
				}
				var data = form.getData(true);
				data['shuser']=userId;
				data['shtime']=sysDate;
				var json = mini.encode(data);
				CommonUtil.ajax({
					url : '/baseQuoteAubpController/updateAubp',
					data : json,
					callback : function(data) {
						mini.alert("提交成功",'提示信息',function(){
							CloseOwnerWindow();
						});
					}
				});
			}

			function close() {
				CloseOwnerWindow();
			}
	
			 

		</script>
</body>
</html>
