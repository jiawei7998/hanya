<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title></title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend></legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
	<span style="margin: 2px; display: block;"> 
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<div id = "type" name = "type" class="mini-checkboxlist" style="float:right;" labelField="true" label="选择源：" labelStyle="text-align:right;border:none; background-color:#fff;" 
				value="webStart" textField="text" valueField="id" multiSelect="false"
				data="[{id:'webStart',text:'网页源日间'},{id:'webFinished',text:'网页源日终'}]" onvaluechanged="checkBoxValuechanged">
	</div>
	</span>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id" hidden="false" allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="ccyp" width="180px"  align="center"  headeralign="center" >货币对/网页源</div>    
	
			<div field="cbuybpweb" name="cbuybpweb" width="180px"  align="center"  headeralign="center" >钞买点差</div>    
			<div field="csalebpweb" name="csalebpweb" width="120"  align="center" headeralign="center"  >钞卖点差</div>
			<div field="rbuybpweb" name="rbuybpweb" width="120"  align="center" headeralign="center"  >汇买点差</div>
			<div field="rsalebpweb" name="rsalebpweb" width="150px"  align="center"  headeralign="center" >汇卖点差</div>                            
			<div field="cbuywfbweb" name="cbuywfbweb" width="150px"  align="center"  headeralign="center" >钞买万分比</div>                            
			<div field="csalewfbweb" name="csalewfbweb" width="180px"  align="left"  headeralign="center" >钞卖万分比</div>
			<div field="rbuywfbweb" name="rbuywfbweb" width="180px"  align="right"  headeralign="center" allowsort="true" >汇买万分比</div>                                
			<div field="rsalewfbweb" name="rsalewfbweb" width="150px"  align="center"  headeralign="center" allowsort="true">汇卖万分比</div>
	
			<div field="cbuybpwebdate" name="cbuybpwebdate" width="180px"  align="center"  headeralign="center" >日终钞买点差</div>    
			<div field="csalebpwebdate" name="csalebpwebdate" width="120"  align="center" headeralign="center"  >日终钞卖点差</div>
			<div field="rbuybpwebdate" name="rbuybpwebdate" width="120"  align="center" headeralign="center"  >日终汇买点差</div>
			<div field="rsalebpwebdate" name="rsalebpwebdate" width="150px"  align="center"  headeralign="center" >日终汇卖点差</div>                            
			<div field="cbuywfbwebdate" name="cbuywfbwebdate" width="150px"  align="center"  headeralign="center" >日终钞买万分比</div>                            
			<div field="csalewfbwebdate" name="csalewfbwebdate" width="180px"  align="left"  headeralign="center" >日终钞卖万分比</div>
			<div field="rbuywfbwebdate" name="rbuywfbwebdate" width="180px"  align="right"  headeralign="center" allowsort="true" >日终汇买万分比</div>                                
			<div field="rsalewfbwebdate" name="rsalewfbwebdate" width="150px"  align="center"  headeralign="center" allowsort="true">日终汇卖万分比</div>	
	</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");

	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function onRowDblClick(e) {
		edit();
	}
			 function edit(){
					var row = grid.getSelected();
					var type = mini.get("type").getValue();
					if(row){
		            mini.open({
		                targetWindow: window,
		                url:CommonUtil.baseWebPath() + "/../quoteprice/baseCcy/baseAubpEdit.jsp",
		                title: "货币对价差修改", width: 600, height: 400,
		                onload: function () {
		                    var iframe = this.getIFrameEl();
		                    var data = {type: type,row : row };
		                    iframe.contentWindow.SetData(data);
		                },
		                ondestroy: function (action) {
		                    grid.reload();
		                }
		            });
					}else{
						mini.alert("请选中一条记录！","消息提示");
					}
					}

	function checkBoxValuechanged(e){
		search(10,0);
	}
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		var type = mini.get("type").getValue();
		 if(type == "webStart"){
			 	 mini.get("datagrid").hideColumn("cbuybpwebdate");
				mini.get("datagrid").hideColumn("csalebpwebdate");
				mini.get("datagrid").hideColumn("rbuybpwebdate");
				mini.get("datagrid").hideColumn("rsalebpwebdate");
				mini.get("datagrid").hideColumn("cbuywfbwebdate");
				mini.get("datagrid").hideColumn("csalewfbwebdate");
				mini.get("datagrid").hideColumn("rbuywfbwebdate");
				mini.get("datagrid").hideColumn("rsalewfbwebdate");
				mini.get("datagrid").showColumn("cbuybpweb");
				mini.get("datagrid").showColumn("csalebpweb");
				mini.get("datagrid").showColumn("rbuybpweb");
				mini.get("datagrid").showColumn("rsalebpweb");
				mini.get("datagrid").showColumn("cbuywfbweb");
				mini.get("datagrid").showColumn("csalewfbweb");
				mini.get("datagrid").showColumn("rbuywfbweb");
				mini.get("datagrid").showColumn("rsalewfbweb");
			 	 mini.get("datagrid").showColumn("cbuybpwebdate");
		}else{
				 mini.get("datagrid").hideColumn("cbuybpweb");
				mini.get("datagrid").hideColumn("csalebpweb");
				mini.get("datagrid").hideColumn("rbuybpweb");
				mini.get("datagrid").hideColumn("rsalebpweb");
				mini.get("datagrid").hideColumn("cbuywfbweb");
				mini.get("datagrid").hideColumn("csalewfbweb");
				mini.get("datagrid").hideColumn("rbuywfbweb");
				mini.get("datagrid").hideColumn("rsalewfbweb");
			 	 mini.get("datagrid").showColumn("cbuybpwebdate");
					mini.get("datagrid").showColumn("csalebpwebdate");
					mini.get("datagrid").showColumn("rbuybpwebdate");
					mini.get("datagrid").showColumn("rsalebpwebdate");
					mini.get("datagrid").showColumn("cbuywfbwebdate");
					mini.get("datagrid").showColumn("csalewfbwebdate");
					mini.get("datagrid").showColumn("rbuywfbwebdate");
					mini.get("datagrid").showColumn("rsalewfbwebdate");
		}
		data['isused']="1";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:'/baseQuoteCcypairController/searchCcypairpage',
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
</script>
</body>
</html>
