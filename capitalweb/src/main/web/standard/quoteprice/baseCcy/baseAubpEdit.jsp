<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div style="width:100%;height:100%;"  showCollapseButton="false">
				
<h1 style="text-align:center"></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<table  width="100%" class="mini-table">
		<tr>
			<input id="syspop" name="syspop" class="mini-textbox" visible="false" labelField="true"  label="系统日间价差方式："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="syspopdate" name="syspopdate" class="mini-textbox" visible="false" labelField="true"  label="系统日终价差方式："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="webpop" name="webpop" class="mini-textbox" visible="false" labelField="true"  label="网页日间价差方式："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="webpopdate" name="webpopdate" class="mini-textbox" visible="false" labelField="true"  label="网页日终价差方式："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</tr>
		<tr>
		<td>
			<input id="cbuybp" name="cbuybp" class="mini-textbox" visible="false" labelField="true"  label="钞买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuybp" name="rbuybp" class="mini-textbox" visible="false"  labelField="true"  label="汇买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="cbuywfb" name="cbuywfb" class="mini-textbox"  visible="false" labelField="true"  label="钞买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuywfb" name="rbuywfb" class="mini-textbox" visible="false"  labelField="true"  label="汇买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		<td>
			<input id="csalebp" name="csalebp" class="mini-textbox" visible="false"  labelField="true"  label="钞卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="rsalebp" name="rsalebp" class="mini-textbox"  visible="false" labelField="true"  label="汇卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="csalewfb" name="csalewfb" class="mini-textbox"  visible="false" labelField="true"  label="钞卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rsalewfb" name="rsalewfb" class="mini-textbox"  visible="false" labelField="true"  label="汇卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		</tr>
		<tr>
		<td>
			<input id="cbuybpdate" name="cbuybpdate"  visible="false" class="mini-textbox" labelField="true"  label="日终钞买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="rbuybpdate" name="rbuybpdate" class="mini-textbox"  visible="false" labelField="true"  label="日终汇买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="cbuywfbdate" name="cbuywfbdate" class="mini-textbox" visible="false"  labelField="true"  label="日终钞买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuywfbdate" name="rbuywfbdate" class="mini-textbox"  visible="false" labelField="true"  label="日终汇买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		<td>
			<input id="csalebpdate" name="csalebpdate" class="mini-combobox"  visible="false" labelField="true"  label="日终钞卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="rsalebpdate" name="rsalebpdate" class="mini-textbox" visible="false"  labelField="true"  label="日终汇卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="csalewfbdate" name="csalewfbdate" class="mini-textbox" visible="false"  labelField="true"  label="日终钞卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rsalewfbdate" name="rsalewfbdate" class="mini-textbox"  visible="false" labelField="true"  label="日终汇卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		</tr>
		<td>
			<input id="cbuybpweb" name="cbuybpweb" class="mini-textbox" visible="false" labelField="true"  label="钞买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuybpweb" name="rbuybpweb" class="mini-textbox" visible="false"  labelField="true"  label="汇买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="cbuywfbweb" name="cbuywfbweb" class="mini-textbox"  visible="false" labelField="true"  label="钞买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuywfbweb" name="rbuywfbweb" class="mini-textbox" visible="false"  labelField="true"  label="汇买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		<td>
			<input id="csalebpweb" name="csalebpweb" class="mini-textbox" visible="false"  labelField="true"  label="钞卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="rsalebpweb" name="rsalebpweb" class="mini-textbox"  visible="false" labelField="true"  label="汇卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="csalewfbweb" name="csalewfbweb" class="mini-textbox"  visible="false" labelField="true"  label="钞卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rsalewfbweb" name="rsalewfbweb" class="mini-textbox"  visible="false" labelField="true"  label="汇卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		</tr>
		<tr>
		<td>
			<input id="cbuybpwebdate" name="cbuybpwebdate"  visible="false" class="mini-textbox" labelField="true"  label="日终钞买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="rbuybpwebdate" name="rbuybpwebdate" class="mini-textbox"  visible="false" labelField="true"  label="日终汇买点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="cbuywfbwebdate" name="cbuywfbwebdate" class="mini-textbox" visible="false"  labelField="true"  label="日终钞买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rbuywfbwebdate" name="rbuywfbwebdate" class="mini-textbox"  visible="false" labelField="true"  label="日终汇买万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		<td>
			<input id="csalebpwebdate" name="csalebpwebdate" class="mini-combobox"  visible="false" labelField="true"  label="日终钞卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;" />
			<input id="rsalebpwebdate" name="rsalebpwebdate" class="mini-textbox" visible="false"  labelField="true"  label="日终汇卖点差："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="csalewfbwebdate" name="csalewfbwebdate" class="mini-textbox" visible="false"  labelField="true"  label="日终钞卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
			<input id="rsalewfbwebdate" name="rsalewfbwebdate" class="mini-textbox"  visible="false" labelField="true"  label="日终汇卖万分比："  style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</td>
		</tr>
	</table>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="commit">提交审批</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭</a>
		</div>
	</div>
		
</div>	


	</div>
		<script type="text/javascript">
			mini.parse();
			//mini.get("ccyp").show();
			var Data = "";
			var Type = "";
			 function SetData(data) {
					var form =new mini.Form("field_form");
	            	Data = data.row;
	            	form.setData(data.row);
		            if (data.type == "sysStart") {
						mini.get("syspop").show();
						mini.get("cbuybp").show();
						mini.get("csalebp").show();
						mini.get("rbuybp").show();
						mini.get("rsalebp").show();
						mini.get("cbuywfb").show();
						mini.get("csalewfb").show();
						mini.get("rbuywfb").show();
						mini.get("rsalewfb").show();
						Type = "1";
		            }else if(data.type == "sysFinished"){
						mini.get("syspopdate").show();
						mini.get("cbuybpdate").show();
						mini.get("csalebpdate").show();
						mini.get("rbuybpdate").show();
						mini.get("rsalebpdate").show();
						mini.get("cbuywfbdate").show();
						mini.get("csalewfbdate").show();
						mini.get("rbuywfbdate").show();
						mini.get("rsalewfbdate").show();
						Type = "2";
		            }else if(data.type == "webStart"){
						mini.get("webpop").show();
						mini.get("cbuybpweb").show();
						mini.get("csalebpweb").show();
						mini.get("rbuybpweb").show();
						mini.get("rsalebpweb").show();
						mini.get("cbuywfbweb").show();
						mini.get("csalewfbweb").show();
						mini.get("rbuywfbweb").show();
						mini.get("rsalewfbweb").show();
						Type = "3";
		            }else if(data.type == "webFinished"){
						mini.get("webpopdate").show();
						mini.get("cbuybpwebdate").show();
						mini.get("csalebpwebdate").show();
						mini.get("rbuybpwebdate").show();
						mini.get("rsalebpwebdate").show();
						mini.get("cbuywfbwebdate").show();
						mini.get("csalewfbwebdate").show();
						mini.get("rbuywfbwebdate").show();
						mini.get("rsalewfbwebdate").show();
						Type = "4";
		            }
		           
		        }
			
				function commit(){
					var form = new mini.Form("#field_form");
					form.validate();
					if(form.isValid()==false){
						mini.alert("信息填写有误，请重新填写","系统提示");
						return;
					}

					var data=form.getData(true);
					data['updateuser']=userId;
					data['updatedate']=sysDate;
					data['type']=Type;
					data['ccyp']=Data.ccyp;
					if(Type="1"){
						data['pop']=mini.get("syspop").getValue();
					}else if(Type="2"){
						data['pop']=mini.get("syspopdate").getValue();
					}else if(Type="3"){
						data['pop']=mini.get("webpop").getValue();
					}else {
						data['pop']=mini.get("webpopdate").getValue();
					}
					var params = mini.encode(data);
					CommonUtil.ajax({
						url:'/baseQuoteAubpController/saveAubp',
						data:params,
						callback : function(data) {
							mini.alert("保存成功",'提示信息',function(){
								CloseOwnerWindow();
							});
						}
					});
				}
				
				
			function close() {
				CloseOwnerWindow();
			}
	
			 

		</script>
</body>
</html>
