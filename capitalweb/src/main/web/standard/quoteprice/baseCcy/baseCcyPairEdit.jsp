<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<script type="text/javascript" src="<%=basePath%>/sl/TaDictController/dictionary.js"></script>
<html>
  <head>
    <title></title>
  </head>

<body style="width:100%;height:100%;background:white">
		<div style="width:100%;height:100%;"  showCollapseButton="false">
				
<h1 style="text-align:center"></h1>
	<div  id="field_form"  class="mini-fit area" style="background:white" >
	<table  width="100%" class="mini-table">
		<tr>
			<input id="ccyp1" name="ccyp1" class="mini-combobox" labelField="true"  label="基准货币：" style="width:100%;" maxLength="25" labelStyle="text-align:left;"  required="true" />
		</tr>
		<tr>
			<input id="ccyp2" name="ccyp2" class="mini-combobox" labelField="true"  label="参照货币："  style="width:100%;" maxLength="25"  labelStyle="text-align:left;"  required="true" />
		</tr>
	</table>
		<div id="functionIds" showCollapseButton="true" style="padding-top:30px;">
		<div style="margin-bottom:10px;">
		<a class="mini-button" style="display: none"  style="width:120px;" id="save_btn"  text-align: left;  onclick="save">保存</a>
		<a class="mini-button" style="display: none"  style="width:120px;" id="close_btn"  text-align: right; onclick="close">关闭</a>
		</div>
	</div>
		
</div>	


	</div>
		<script type="text/javascript">
			mini.parse();
			var ccy1 = mini.get("ccyp1");
			var ccy2 = mini.get("ccyp2")
			function inner(){
				CommonUtil.ajax({
					url : '/baseQuoteCcycodeController/saveCcyMap',
					data : {},
					callback : function(data) {
						var ss = eval("("+data+")");
						var arr = [];
						
						for(var i=0;i<ss.length;i++){
							var dat = {};
							dat['id']=ss[i];
							dat['text']=ss[i];
							arr.push(dat);
							////console.log(dat);
							////console.log(arr);
						}
						ccy1.load(arr)
						ccy2.load(arr)
					}
					});
				
			}
			function onRowDblClick(e) {
				return;
			}
			function save(){
				
				var form = new mini.Form("field_form");
				form.validate();
				if (form.isValid() == false) {
					return;
				}
				var str1 = mini.get("ccyp1").getText();
				var str2 = mini.get("ccyp2").getText();
					if(str1==str2){
						mini.alert("二个币种不能相同","提示信息")
					return;
				}
				var str = str1+"/"+str2;
				var data = {};
				data['inputuser'] = userId;
				data['inputdate'] = sysDate;
				data['ccyp'] = str;
				var json = mini.encode(data);
				CommonUtil.ajax({
					url : '/baseQuoteCcypairController/saveCcypair',
					data : json,
					callback : function(data) {
						mini.alert("保存成功",'提示信息',function(){
							CloseOwnerWindow();
						});
					}
				});
			}

			function close() {
				CloseOwnerWindow();
			}
			
			$(document).ready(function() {
				inner();
			});

		</script>
</body>
</html>
