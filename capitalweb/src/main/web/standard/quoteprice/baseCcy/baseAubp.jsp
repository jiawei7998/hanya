<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>债券借贷</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>债券借贷成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
	<span style="margin: 2px; display: block;"> 
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<div id = "type" name = "type" class="mini-checkboxlist" style="float:right;" labelField="true" label="选择源：" labelStyle="text-align:right;border:none; background-color:#fff;" 
				value="sysStart" textField="text" valueField="id" multiSelect="false"
				data="[{id:'sysStart',text:'系统源日间'},{id:'sysFinished',text:'系统源日终'}]" onvaluechanged="checkBoxValuechanged">
	</div>
	</span>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id" hidden="false" allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="ccyp"  width="180px"  align="center"  headeralign="center" >货币对/系统源</div>    
	
			<div field="cbuybp" name="cbuybp" width="180px"  align="center"  headeralign="center" >钞买点差</div>    
			<div field="csalebp" name="csalebp" width="120"  align="center" headeralign="center"  >钞卖点差</div>
			<div field="rbuybp" name="rbuybp" width="120"  align="center" headeralign="center"  >汇买点差</div>
			<div field="rsalebp" name="rsalebp" width="150px"  align="center"  headeralign="center" >汇卖点差</div>                            
			<div field="cbuywfb" name="cbuywfb" width="150px"  align="center"  headeralign="center" >钞买万分比</div>                            
			<div field="csalewfb" name="csalewfb" width="180px"  align="left"  headeralign="center" >钞卖万分比</div>
			<div field="rbuywfb" name="rbuywfb" width="180px"  align="right"  headeralign="center" allowsort="true" >汇买万分比</div>                                
			<div field="rsalewfb" name="rsalewfb" width="150px"  align="center"  headeralign="center" allowsort="true">汇卖万分比</div>
	
			<div field="cbuybpdate" name="cbuybpdate" width="180px"  align="center"  headeralign="center" >日终钞买点差</div>    
			<div field="csalebpdate" name="csalebpdate" width="120"  align="center" headeralign="center"  >日终钞卖点差</div>
			<div field="rbuybpdate" name="rbuybpdate" width="120"  align="center" headeralign="center"  >日终汇买点差</div>
			<div field="rsalebpdate" name="rsalebpdate" width="150px"  align="center"  headeralign="center" >日终汇卖点差</div>                            
			<div field="cbuywfbdate" name="cbuywfbdate" width="150px"  align="center"  headeralign="center" >日终钞买万分比</div>                            
			<div field="csalewfbdate" name="csalewfbdate" width="180px"  align="left"  headeralign="center" >日终钞卖万分比</div>
			<div field="rbuywfbdate" name="rbuywfbdate" width="180px"  align="right"  headeralign="center" allowsort="true" >日终汇买万分比</div>                                
			<div field="rsalewfbdate" name="rsalewfbdate" width="150px"  align="center"  headeralign="center" allowsort="true">日终汇卖万分比</div>
		</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function onRowDblClick(e) {
		edit();
	}
	function edit(){
					var row = grid.getSelected();
					var type = mini.get("type").getValue();
					if(row){
		            mini.open({
		                targetWindow: window,
		                url:CommonUtil.baseWebPath() + "/../quoteprice/baseCcy/baseAubpEdit.jsp",
		                title: "货币对价差修改", width: 600, height: 400,
		                onload: function () {
		                    var iframe = this.getIFrameEl();
		                    var data = {type: type,row : row };
		                    iframe.contentWindow.SetData(data);
		                },
		                ondestroy: function (action) {
		                    grid.reload();
		                }
		            });
					}else{
						mini.alert("请选中一条记录！","消息提示");
					}
		}
			 
	function checkBoxValuechanged(e){
		search(10,0);
	}
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统也提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;

		var type = mini.get("type").getValue();
		if(type == "sysStart"){
			 mini.get("datagrid").hideColumn("cbuybpdate");
			mini.get("datagrid").hideColumn("csalebpdate");
			mini.get("datagrid").hideColumn("rbuybpdate");
			mini.get("datagrid").hideColumn("rsalebpdate");
			mini.get("datagrid").hideColumn("cbuywfbdate");
			mini.get("datagrid").hideColumn("csalewfbdate");
			mini.get("datagrid").hideColumn("rbuywfbdate");
			mini.get("datagrid").hideColumn("rsalewfbdate");
			mini.get("datagrid").hideColumn("cbuybp");
			mini.get("datagrid").showColumn("csalebp");
			mini.get("datagrid").showColumn("rbuybp");
			mini.get("datagrid").showColumn("rsalebp");
			mini.get("datagrid").showColumn("cbuywfb");
			mini.get("datagrid").showColumn("csalewfb");
			mini.get("datagrid").showColumn("rbuywfb");
			mini.get("datagrid").showColumn("rsalewfb");
		}else{
				 mini.get("datagrid").hideColumn("cbuybp");
				mini.get("datagrid").hideColumn("csalebp");
				mini.get("datagrid").hideColumn("rbuybp");
				mini.get("datagrid").hideColumn("rsalebp");
				mini.get("datagrid").hideColumn("cbuywfb");
				mini.get("datagrid").hideColumn("csalewfb");
				mini.get("datagrid").hideColumn("rbuywfb");
				mini.get("datagrid").hideColumn("rsalewfb");
				 mini.get("datagrid").showColumn("cbuybpdate");
					mini.get("datagrid").showColumn("csalebpdate");
					mini.get("datagrid").showColumn("rbuybpdate");
					mini.get("datagrid").showColumn("rsalebpdate");
					mini.get("datagrid").showColumn("cbuywfbdate");
					mini.get("datagrid").showColumn("csalewfbdate");
					mini.get("datagrid").showColumn("rbuywfbdate");
					mini.get("datagrid").showColumn("rsalewfbdate");
		}
		data['isused']="1";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:'/baseQuoteCcypairController/searchCcypairpage',
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
</script>
</body>
</html>
