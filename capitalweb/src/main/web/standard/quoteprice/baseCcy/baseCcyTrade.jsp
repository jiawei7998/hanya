<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>

<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title></title>

</head>
<body style="width: 100%; height: 100%; background: white">
	<fieldset class="mini-fieldset">
		<legend>查询</legend>
		<div id="search_form" style="width: 100%;">
			<input id="contractId" name="contractId" class="mini-textbox" labelField="true" width="280px" label="英文名称：" emptyText="请输入英文名称" labelStyle="text-align:right;" /> 
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="query()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</fieldset>
	<span style="margin:2px;display: block;">
	<a  id="add_btn" class="mini-button" style="display: none"   onclick="add()">增加</a>
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	<a  id="delete_btn" class="mini-button" style="display: none"   onclick="del()">删除</a>

</span>
	<div id="MiniSettleForeignSpot" class="mini-fit"
		style="margin-top: 2px;">
		<div id="datagrid" class="mini-datagrid borderAll" style="width: 100%; height: 100%;" allowAlternating="true"
			allowResize="true" onrowdblclick="onRowDblClick" border="true" sortMode="client">
			<div property="columns">
				<div field="ccyp" width="200" allowSort="false" headerAlign="center" align="">英文名称</div>
				<div field="quoteunits" width="200" align="center" headerAlign="center" >报价单位</div>
				<div field="pricenum" width="200" align="center" headerAlign="center" >报价精度</div>
				<div field="peoplepriceunit" width="200" allowSort="false" headerAlign="center" align="center">最大波动价差</div>				
				<div field="maxwfb" width="200" allowSort="false" headerAlign="center" align="center">最大波动万分比</div>				
				<div field="wgjrxzjc" width="200" allowSort="false" headerAlign="center" align="center">外管局现现限制价差（%）</div>
				<div field="wgjcxzjc" width="200" allowSort="false" headerAlign="center" align="">外管局汇限制价差（%）</div>
				<div field="ccyname" width="200" align="center" headerAlign="center" >波动价差方式</div>
				<div field="note" width="200" align="center" headerAlign="center" >备注</div>
			</div>
		</div>
	</div>
	<script>
		mini.parse();

		var grid = mini.get("datagrid");
		
		grid.on("beforeload", function(e) {
			e.cancel = true;
			var pageIndex = e.data.pageIndex;
			var pageSize = e.data.pageSize;
			search(pageSize, pageIndex);
		});
		
		function search(pageSize,pageIndex){
			var form = new mini.Form("#search_form");
			form.validate();
			if(form.isValid()==false){
				mini.alert("信息填写有误，请重新填写","系统提示");
				return;
			}

			var data=form.getData(true);
			data['pageNumber']=pageIndex+1;
			data['pageSize']=pageSize;
			data['isused']="1";
			var params = mini.encode(data);
			CommonUtil.ajax({
				url:'/baseQuoteCcypairController/searchCcypairpage',
				data:params,
				callback : function(data) {
					grid.setTotalCount(data.obj.total);
					grid.setPageIndex(pageIndex);
			        grid.setPageSize(pageSize);
					grid.setData(data.obj.rows);
				}
			});
		}
		function clear(){
            var form=new mini.Form("search_form");
            form.clear();
            search(10,0);

		}
		//增删改查
					function del() {
				var grid = mini.get("datagrid");
				var row = grid.getSelected();
				if (row) {
					mini.confirm("您确认要删除选中记录?","系统警告",function(value){
						if(value=="ok"){
						CommonUtil.ajax({
							url: "/baseQuoteCcypairController/updateCcypairIsused",
							data: {ccyp:row.ccyp},
							callback: function (data) {
								if (data.code == 'error.common.0000') {
									mini.alert("删除成功");
									grid.reload();
									//search(grid.pageSize,grid.pageIndex);
								} else {
									mini.alert("删除失败");
								}
							}
						});
						}
					});
				}
				else {
					mini.alert("请选中一条记录！", "消息提示");
				}

			}
		 function add() {

	            mini.open({
	                targetWindow: window,
	                url:CommonUtil.baseWebPath() + "/../quoteprice/baseCcy/baseCcyTradeEdit.jsp",
	                title: "基础货币新增", width: 600, height: 400,
	                onload: function () {
	                    var iframe = this.getIFrameEl();
	                    var data = { action: "add" };
	                    iframe.contentWindow.SetData(data);
	                },
	                ondestroy: function (action) {
	                    grid.reload();
	                }
	            });
	        }

		function onRowDblClick(e) {
			edit();
		}
				 function edit() {
						var row = grid.getSelected();
						if(row){
			            mini.open({
			                targetWindow: window,
			                url:CommonUtil.baseWebPath() + "/../quoteprice/baseCcy/baseCcyTradeEdit.jsp",
			                title: "基础货币修改", width: 600, height: 400,
			                onload: function () {
			                    var iframe = this.getIFrameEl();
			                    var data = { action: "edit",row : row };
			                    iframe.contentWindow.SetData(data);
			                },
			                ondestroy: function (action) {
			                    grid.reload();
			                }
			            });
						}else{
							mini.alert("请选中一条记录！","消息提示");
						}
						}

		$(document).ready(function() {
			//控制按钮显示
			$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
				search(10, 0);
			});
		});

	</script>
</body>
</html>