<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../global.jsp"%>
<html>
<head>
<script src="<%=basePath%>/sl/TaDictController/dictionary.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/approveFlowForIfs.js"></script>
<script type="text/javascript" src="<%=basePath%>/miniScript/hideBtn.js"></script>
<title>债券借贷</title>
</head>
<body style="width:100%;height:100%;background:white">
<fieldset class="mini-fieldset">
	<legend>债券借贷成交单查询</legend>	
	<div>
		<div id="search_form" style="width:100%" cols="6">
			<span style="float: right; margin-right: 150px"> 
				<a id="search_btn" class="mini-button" style="display: none"   onclick="search()">查询</a>
				<a id="clear_btn" class="mini-button" style="display: none"   onclick="clear()">清空</a>
			</span>
		</div>
	</div>
</fieldset> 
	<span style="margin: 2px; display: block;"> 
	<a  id="edit_btn" class="mini-button" style="display: none"   onclick="edit()">修改</a>
	
	</span>	  
<div class="mini-fit" >
	<div id="datagrid" class="mini-datagrid borderAll" style="width:100%;height:100%;" idField="id" hidden="false" allowAlternating="true"
		onrowdblclick="onRowDblClick" allowResize="true" sortMode="client" allowAlternating="true">
		<div property="columns">
			<div field="auser"  width="150px"  align="center"  headeralign="center" allowsort="true">申请人</div>
			<div field="atime"  width="150px"  align="center"  headeralign="center" allowsort="true">申请时间</div>
			<div field="ccyp"  width="180px"  align="center"  headeralign="center" >货币对/系统源</div>
			<div field="webpage"  width="150px"  align="center"  headeralign="center" allowsort="true">点差方式</div>
			<div field="pop"  width="150px"  align="center"  headeralign="center" allowsort="true">价差类型</div>    
			<div field="cbuybp"  width="180px"  align="center"  headeralign="center" >钞买点差</div>    
			<div field="csalebp"  width="120"  align="center" headeralign="center"  >钞卖点差</div>
			<div field="rbuybp"  width="120"  align="center" headeralign="center"  >汇买点差</div>
			<div field="rsalebp"  width="150px"  align="center"  headeralign="center" >汇卖点差</div>                            
			<div field="cbuywfb"  width="150px"  align="center"  headeralign="center" >钞买万分比</div>                            
			<div field="csalewfb" width="180px"  align="left"  headeralign="center" >钞卖万分比</div>
			<div field="rbuywfb"  width="180px"  align="right"  headeralign="center" allowsort="true" >汇买万分比</div>                                
			<div field="rsalewfb"  width="150px"  align="center"  headeralign="center" allowsort="true">汇卖万分比</div>
			<div field="shresult"  width="180px"  align="left"  headeralign="center" >审批状态</div>
			

		</div>
</div>   

<script>
	mini.parse();

	var form = new mini.Form("#search_form");
	var grid=mini.get("datagrid");
	
	grid.on("beforeload", function (e) {
		e.cancel = true;
		var pageIndex = e.data.pageIndex; 
		var pageSize = e.data.pageSize;
		search(pageSize,pageIndex);
	});

	function onRowDblClick(e) {
		edit();
	}
	function edit(){
					var row = grid.getSelected();
					if(row){
		            mini.open({
		                targetWindow: window,
		                url:CommonUtil.baseWebPath() + "/../quoteprice/baseCcy/baseAubpApproveEdit.jsp",
		                title: "货币对价差审批", width: 600, height: 400,
		                onload: function () {
		                    var iframe = this.getIFrameEl();
		                    var data = {row : row };
		                    iframe.contentWindow.SetData(data);
		                },
		                ondestroy: function (action) {
		                    grid.reload();
		                }
		            });
					}else{
						mini.alert("请选中一条记录！","消息提示");
					}
		}
			 
	function checkBoxValuechanged(e){
		search(10,0);
	}
	function search(pageSize,pageIndex){
		form.validate();
		if(form.isValid()==false){
			mini.alert("信息填写有误，请重新填写","系统提示");
			return;
		}

		var data=form.getData(true);
		data['pageNumber']=pageIndex+1;
		data['pageSize']=pageSize;
		data['branchId']=branchId;
		data['isused']="1";
		var params = mini.encode(data);
		CommonUtil.ajax({
			url:'/baseQuoteAubpController/searchAubp',
			data:params,
			callback : function(data) {
				grid.setTotalCount(data.obj.total);
				grid.setPageIndex(pageIndex);
		        grid.setPageSize(pageSize);
				grid.setData(data.obj.rows);
			}
		});
	}
	$(document).ready(function() {
		//控制按钮显示
		$.when(CommonUtil.hideBtn(top["win"].tabs.getActiveTab().id)).done(function(visibleBtn) {
			search(10, 0);
		});
	});
</script>
</body>
</html>
