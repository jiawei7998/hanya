/*
 *	 审批相关操作
 *  @auth : libo 
 */
var Approve = {
	config:{
		supportType:[
			//"decision",		//判断节点
			//"fork",			//分支节点
			//"skip",			//审批任意跳转
			""
		]
		
	}
};
Approve.loading=function(){
	$("<div class=\"datagrid-mask\"></div>").css({
		display:"block",
		width:"100%",
		height:$(window).height()
	}).appendTo("body");
	
	$("<div class=\"datagrid-mask-msg\"></div>").html(
		"正在提交审批，请稍候...").appendTo("body").css({
		display:"block",
		left:($(document.body).outerWidth(true)-190)/2,
		top:($(window).height()-45)/2
		}); 	
}

Approve.loadingCancel=function(){
	$(".datagrid-mask").remove();
	$(".datagrid-mask-msg").remove();
}
/**  审批单状态  1:就绪	2:计算中	3:新建	4:待审批	5:审批中	6:审批通过	7:审批拒绝	8:注销	9:执行中	10:执行完成	*/
Approve.OrderStatus={Ready:'1',Calculating:'2',New:'3',WaitApprove:'4',Approving:'5',ApprovedPass:'6',ApprovedNoPass:'7',Cancle:'8',Executing:'9',Executed:'10',Verifying:'11',Verified:'12'};

/**  审批流程类型  1:业务审批流程  2：风险预审  3：预授权审批流程 4:核实流程 5：结算流程 6:额度申请流程  7:清算路径流程  8:开户销户流程  9:投后报告流程
 * 				  20:额度切分  22:额度调整   23:额度冻结   24:额度占用 35:手工调帐   36:年终结算    40：事前审批*/
Approve.FlowType={BusinessApproveFlow:'1',RiskApproveFlow:'2',PreAuthApproveFlow:'3',VerifyApproveFlow:'4',SettleApproveFlow:'5',QuotaApproveFlow:'6',PathApproveFlow:'7',AccountApproveFlow:'8'
					,investFollowupFlow:'9',QuotaSegFlow:'20',tdCustAtFlow:'21',QuotaAdjFlow:'22',QuotaFrozenFlow:'23',QuotaUsedFlow:'24',FiveType:'32',ManualAdjFlow:'35',YeanEndFlow:'36',
					rdpApproveFLow:'11',rdpVerifyFLow:'44',rddApproveFlow:'12',rdiApproveFLow:'13',preApprove:'40'};

/**  审批操作类型  0:通过  1:同意继续   2:跳转    -1:不通过   -2:(Back)驳回    99：(Redo)退回发起  */
Approve.OperatorType={Pass:'0',NoPass:'-1',Continue:'1',Back:'-2',Redo:'99',Skip:'2'};

/**  结算状态 0:未处理 1：待结算 2：结算中 3：等待付款 4：等待收款 5：核算完成 6：结算完成 7：暂停 8：已撤销 9：结算失败 13：待发起 14：待复核 15：待授权 **/
Approve.SettleInstState = {NEW:'0',WAITSETTLE:'1',SETTLING:'2',WAITPAY:'3',WAITREC:'4',ACCOUNTINGDONE:'5',SETTLECOMPLETE:'6',PAUSE:'7',CANCEL:'8',FAILED:'9',WAITSTART:'13',WAITVERIFY:'14',WAITAHTH:'15'};

/** 扩展虚拟产品  **/
Approve.ProductExt = {FiveType:'9013'};


/** 
	打开审批操作页面，
	@param flow_type	流程类型
	@param task_id		任务id
	@param serial_no	外部序号
	@param isNeedAuth	是否需要授权
	@param authType		授权类型
*/
Approve.openApproveOperator = function(flow_type,task_id,serial_no,closedFun,isNeedAuth,authType,parentObj){
	var tab = {
		id:"task_"+task_id,
		name:"task_"+task_id,
		iconCls:null,
		title:"审批",
		url:CommonUtil.baseWebPath() + "/mini_base/mini_flow/MiniApproveOperate.jsp",
		showCloseButton:true
	};
	var param = {
		isLogPage:false,
		task_id:task_id,
		flow_type:flow_type,
		serial_no:serial_no,
		isNeedAuth:isNeedAuth,
		authType:authType,
		closedFun:closedFun
	};

	CommonUtil.openNewMenuTab(tab,param);
}

/**
 *	提交审批
 * @param 	flow_type		 流程类型
 * @param 	serial_no		 外部序号（审批单号）
 * @param 	order_status    审批单状态
 * @param 	closedFun		close回调
 * @param 	closedFun		close回调
 * @param   callBackService 回调java方法（可为空）!该参数已作废,固定传null即可
 * @param   product_type    产品类型（可为空）
 */
Approve.approveCommit = function(flow_type,serial_no,order_status,closedFun,callBackService,product_type){
	var params = {};
	params.serial_no = serial_no;
	params.flow_type = flow_type;
	params.product_type = product_type;

	var messageid = mini.loading("正在提交审批", "请稍后");

	CommonUtil.ajax({
		url:"/FlowController/approveLog",
		data:params,
		async:false,
		callback : function(data) {
			if(data.obj){
				//若有日志则说明该交易是退回发起的
				//从审批日志获取最新taskid
				var unfinishTask = new Array();
				$.each(data.obj, function(i, item){
					if(item.EndTime == "" || item.EndTime == null){
						unfinishTask.push(item);
					}
				});
				if(unfinishTask){
					var task_id =  unfinishTask[0].TaskId;
				}else{
					
				}
				//组装参数对象
				var defaultParam={
			    	task_id:task_id,
			    	specific_user:"",
			    	msg:"",
			    	op_type:Approve.OperatorType.Continue,
			    	back_to:"",
			    	success:closedFun
				};
				Approve.approveOperator(defaultParam);
			}else{
				//不能查到审批日志的新建状态则按一般步骤提交
				CommonUtil.ajax( {
					url:"/FlowController/getAvailableFlowList",
					data:params,
					async:false,
					callback : function(data) {
						if(typeof(data) == "undefined" || data.length != 1){
							mini.hideMessageBox(messageid);
							mini.alert("流程匹配失败.匹配到的审批流程有" + data.length +"笔","系统提示");
							return false;
						}
						params.flow_id = data[0].flow_id;
						CommonUtil.ajax( {
							url:"/FlowController/approveCommit",
							data:params,
							async:false,
							callback : function(data) {
								mini.alert("提交成功","系统提示");
							},
							callerror: function(data){
							},
							callComplete:closedFun
						});
					}
				});
			}
		}
	});
	
	mini.hideMessageBox(messageid);
}

/**
 *	提交审批
 * @param 	flow_type		 流程类型
 * @param 	serial_no		 外部序号（审批单号）
 * @param 	order_status    审批单状态
 * @param 	closedFun		close回调
 * @param 	closedFun		close回调
 * @param   callBackService 回调java方法（可为空）
 * @param   product_type    产品类型（可为空）
 */
Approve.approveCommitOld = function(flow_type,serial_no,order_status,closedFun,callBackService,product_type){
	if(Approve.OrderStatus.New != order_status){
		mini.alert("该状态不允许提交审批","错误信息");
		return;
	}
	var urls="base/flow/ApproveCommit.jsp?";
	urls += "&approve_type=commit";
	urls += "&flow_type="+flow_type;
	urls += "&serial_no="+serial_no;
	if(callBackService){
		urls += "&callBackService="+callBackService;
	}
	if(product_type){
		urls += "&product_type="+product_type;
	}
	if(!closedFun){
		closedFun=function(){};
	}
	CommonUtil.dialog({
		title:"提交审批 - 流程选择",
		modal:true,
		resizable:true,
		height:300,
		width:600,
		urls:urls,
		param:{
			approve_type:'commit',
			flow_type:flow_type,
			serial_no:serial_no,
			product_type:product_type,
			callBackService:callBackService
		},
		onCloseed:closedFun
	});
}


/**
 *	撤销审批
 * @param 	orders:
				flow_type:流程类型,
				order_id:审批单号（外部序号,
				order_status:审批单状态
 * @param 	successCallBack:操作成功回调
 */
Approve.approveCancel = function(orders,successCallBack){
	var err_ids = "<br />";
	var err_count = 0;
	for (var i=0;i<orders.length;i++){
		if(Approve.OrderStatus.WaitApprove != orders[i]["order_status"]){
			err_ids += "<br />[" + orders[i]["order_id"] + "]";
			err_count++;
		}	
	}
	if(err_count > 0){
		mini.alert("<br /><span style='font-weight:bold;color:red;'>" + err_count + "</span>&nbsp;笔审批单不允许撤销审批" + err_ids,"错误信息");
		return;
	}
	
	mini.confirm("确认撤销" + orders.length +"笔审批吗?", "系统提示",
		function (action) {
			if (action == "ok") {
				if(!successCallBack){
					successCallBack = function(obj){};
				}

				var messageid = mini.loading("正在撤销审批审批", "请稍后");
				CommonUtil.ajax( {
					url:"/FlowController/approveCancel",
					data:{"serials":orders},
					callComplete:function(){
					},
					callback : function(data) {
						successCallBack(data);
					}
				});
				mini.hideMessageBox(messageid);

			}else{
				return false;
			}
		}
	);
}

/**
 *	获取个人任务列表
 * @param 	callbackFun		 回调
 */
Approve.getPersonalTasks = function(callbackFun){
	var messageid = mini.loading("正在获取个人审批列表信息", "请稍后");
	CommonUtil.ajax( {
		url:"/FlowController/getPersonalTask",
		data:{},
		callerror: function(data){
			mini.hideMessageBox(messageid);
			mini.alert(data.errorMsg,"错误信息");
		},
		callback : function(data) {
			mini.hideMessageBox(messageid);
			callbackFun(data);
		}
	});
}

/**
 *	查看审批日志
 * @param 	flow_type		 流程类型
 * @param 	serial_no		 外部序号（审批单号）
 * @param 	closedFun		 close回调
 */
Approve.approveLog = function(flow_type,serial_no,closedFun){
	var tab = {
		id:"flow_log_" + serial_no,
		name:"flow_log_" + serial_no,
		iconCls:null,
		title:"审批日志("+serial_no+")",
		url:CommonUtil.baseWebPath() + "../../Common/Flow/MiniApproveFlowLog.jsp",
		showCloseButton:true
	};
	var param ={
		isLogPage:true,
		task_id:'',
		flow_type:flow_type,
		serial_no:serial_no
	};
	CommonUtil.openNewMenuTab(tab,param);
}

/**
 *	审批操作：通过、不通过、同意继续
 *  @param  task_id    任务id
 *  @param  specific_user 指定用户
 *  @param  msg        意见信息
 *  @param  op_type    操作类型
 *	@param  back_to    回退的目标节点名称
 *  @param  success    操作成功回调
 */
Approve.approveOperator = function(opParam){
	var defaultParam={
		task_id:"",
		specific_user:"",
		msg:"",
		op_type:"",
		back_to:"",
		success:function(){}
	};
	var _opParam = $.extend({},defaultParam,opParam);
	var params = {};
	params.task_id = _opParam.task_id;
	params.msg = _opParam.msg;
	params.specific_user = _opParam.specific_user;
	params.op_type = ""+_opParam.op_type;
	params.back_to=_opParam.back_to;
	params.data = _opParam.data;
	params.serial_no = _opParam.serial_no;
	
	var messageid = mini.loading("正在提交审批", "请稍后");
	
	CommonUtil.ajax({
		url:"/FlowController/approve",
		data:params, 
		callComplete:function(){
			//loadSystemMessage();
			//mini.hideMessageBox(messageid);
		},
		callerror: function(data){
			mini.hideMessageBox(messageid);
		},
		callback : function(data) {
			mini.hideMessageBox(messageid);
			mini.alert("操作成功","系统提示",function(){
				_opParam.success(data);
			});
		}
	});
}

/**
 *	跳转审批页面
 *  @param  taskId        任务id
 *  @param  prdNo         产品编号
 *  @param  defaultUrl    默认url
 */
Approve.goToApproveJsp = function(taskId, prdNo, defaultUrl, targetUrl){
	CommonUtil.ajax({
		url:'/FlowController/getUserTaskUrl',
		data:{"taskId":taskId,"prdCode":prdNo},
		async:false,
		callback:function(data){
			if(data.obj){
				targetUrl = data.obj.urls;
			}else{
				targetUrl = defaultUrl;
			}
			Approve.approvePage = targetUrl;
			return targetUrl;
		}
	});
}
