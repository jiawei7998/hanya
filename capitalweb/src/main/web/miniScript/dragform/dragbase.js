﻿
var DragBase = function (element, options) {
    this.element = $(element);
    this.options = $.extend({}, this.options, options);
    this.init();
}
DragBase.prototype = {

    options: {
        
        mousedown: function (event) { },
        beforedrag: function (event, ui) { },
        dragstart: function (event, ui) { },
        drag: function (event, ui) { },
        dragend: function (event, ui) { }
    },

    _started: false,
    _startEvent: null,

    init: function () {
        var me = this,
            el = me.element,
            opts = me.options;

        me._mousedownBind = me._mousedown.bind(me);
        me._mousemoveBind = me._mousemove.bind(me);
        me._mouseupBind = me._mouseup.bind(me);
        el.on("mousedown", me._mousedownBind);
    },

    destroy: function () {
        this.element.off("mousedown", this._mousedownBind);
    },

    enable: function () {
        this.options.disabled = false;
    },

    disable: function () {
        this.options.disabled = true;
    },

    _mousedown: function (event) {
        var me = this,
            el = me.element,
            opts = me.options;

        if (me.options.disabled) return;

        me._started = false;
        me._startEvent = event;
        me._startTime = new Date();

        var ui = me._ui = { event: me._startEvent, time: me._startTime, offset: { left: 0, top: 0} };

        if (opts.beforedrag.call(me, event, ui) !== false) {

            event.preventDefault();
            $(document).on("mousemove", me._mousemoveBind);
            $(document).on("mouseup", me._mouseupBind);
        }
    },

    _mousemove: function (event) {
        var me = this,
            opts = me.options,
            xOffset = event.pageX - me._startEvent.pageX,
            yOffset = event.pageY - me._startEvent.pageY,
            helper = me._helper,
            started = me._started;

        event.preventDefault();

        me._ui.offset = { left: xOffset, top: yOffset };
        var ui = me._ui;

        if (!started) {
            if (opts.beforedrag.call(this, event, ui) === false) return;
            me._started = true;
            //Droppable.draggable = me;

        }

        if (!started) {
            opts.dragstart.call(this, event, ui);
        }

        opts.drag.call(this, event, ui);

    },

    _mouseup: function (event) {
        var me = this,
            el = me._dragElement,
            opts = me.options;

        var ui = me._ui;

        setTimeout(function () {
            
            $(document).off("mousemove", me._mousemoveBind);
            $(document).off("mouseup", me._mouseupBind);
            if (me._started) {
                opts.dragend.call(me, event, ui);
            }
            me._started = false;
            me._startEvent = null;
            //Droppable.draggable = null;
        }, 1);
    }

};

/////////////////////////////////////////////////////////////////////////////////////////


//只放置原生JS已支持的方法，帮助在IE6下也运行正常。


////////////////////////////////////////////////////////////////////////////////////////
// javascrpit
////////////////////////////////////////////////////////////////////////////////////////

//if (!String.format) {
//    //var s = String.format("{0} ... {1} ... ", 'aaa', 'bbb');
//    String.format = function (format) {
//        var args = Array.prototype.slice.call(arguments, 1);
//        format = format || "";
//        return format.replace(/\{(\d+)\}/g, function (m, i) {
//            return args[i];
//        });
//    }
//}

//trim
//if (!String.prototype.trim) {
//    String.prototype.trim = function () {
//        var re = /^\s+|\s+$/g;
//        return function () { return this.replace(re, ""); };
//    } ();
//}

//bind
if (!Function.prototype.bind) {
    Function.prototype.bind = function (scope) {
        var fn = this;
        return function () {
            return fn.apply(scope, arguments);
        }
    }
}

//now
if (!Date.now) {
    Date.now = function () {
        return new Date().getTime();
    }
}

////////////////////////////////////////////////////////////////////////////////////////
// Array
////////////////////////////////////////////////////////////////////////////////////////
var ap = Array.prototype;
$.extend(ap, {
    add: ap.enqueue = function (item) {
        this[this.length] = item;
        return this;
    },
    addRange: function (array) {
        for (var i = 0, j = array.length; i < j; i++) this[this.length] = array[i];
        return this;
    },
    clear: function () {
        this.length = 0;
        return this;
    },
    clone: function () {
        if (this.length === 1) {
            return [this[0]];
        }
        else {
            return Array.apply(null, this);
        }
    },
    contains: function (item) {
        return (this.indexOf(item) >= 0);
    },
    indexOf: ap.indexOf || function (item, from) {
        var len = this.length;
        for (var i = (from < 0) ? Math.max(0, len + from) : from || 0; i < len; i++) {
            if (this[i] === item) return i;
        }
        return -1;
    },
    dequeue: function () {
        return this.shift();
    },
    insert: function (index, item) {
        this.splice(index, 0, item);
        return this;
    },
    insertRange: function (index, items) {
        if (index < 0 || index > this.length) index = this.length;
        for (var i = items.length - 1; i >= 0; i--) {
            var item = items[i];
            this.splice(index, 0, item);
        }
        return this;
    },
    remove: function (item) {
        var index = this.indexOf(item);
        if (index >= 0) {
            this.splice(index, 1);
        }
        return (index >= 0);
    },
    removeAt: function (index) {
        var ritem = this[index];
        this.splice(index, 1);
        return ritem;
    },
    removeRange: function (items) {
        items = items.clone();
        //if (items == this) items = items.clone();
        for (var i = 0, l = items.length; i < l; i++) {
            this.remove(items[i]);
        }
    },
    filter: ap.filter || function (fn, scope) {
        var scope = scope || window,
        i = 0,
        l = this.length,
        results = [],
        o;
        for (; i < l; i++) {
            o = this[i];
            if (fn.call(scope, o, i, this)) {
                results.push(o);
            }
        }
        return results;
    },
    map: ap.map || function (fn, scope) {
        var scope = scope || window,
        i = 0,
        l = this.length,
        results = [];
        for (; i < l; i++) {
            results[i] = fn.call(scope, this[i], i, this);
        }
        return results;
    },
    some: ap.some || function (fn, scope) {
        var scope = scope || window,
        i = 0,
        l = this.length;
        for (; i < l; i++) {
            if (fn.call(scope, this[i], i, this)) return true;
        }
        return false;
    },
    every: ap.every || function (fn, scope) {
        var scope = scope || window,
        i = 0,
        l = this.length;
        for (; i < l; i++) {
            if (!fn.call(scope, this[i], i, this)) return false;
        }
        return true;
    },
    forEach: ap.forEach || function (fn, scope) {
        var scope = scope || window,
            i = 0,
            l = this.length;
        for (; i < l; i++) {
            fn.call(scope, this[i], i, this);           //Array.prototype.forEach是无法跳出循环的。return false也不行。
        }
    }
});

//
if (!window.requestAnimationFrame) {
    var lastTime = 0;
    window.requestAnimationFrame = function (callback) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16.7 - (currTime - lastTime));
        var id = window.setTimeout(function () {
            callback(currTime + timeToCall);
        }, timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    }
}
if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
        clearTimeout(id);
    };
}

