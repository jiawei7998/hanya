﻿
//var FieldUid = 1;

var FieldContainer = function (element, options) {
    this.element = $(element);
    this.options = $.extend({}, this.options, options);
    this.init();
}

FieldContainer.prototype = {

    options: {
        draggble: true,
        fieldsetDraggable: false,                   //true
        fieldset: false,
        fieldsetCls: "fieldset-item",
        fieldCls: 'field-item',
        ghostCls: "item-ghost",
        fieldsetGhostCls: "fieldset-item-ghost",
        fieldsCls: 'fieldset-fields',
        data: null,

        onFieldClick: null,
        onFieldDblClick: null
    },

    init: function () {
        var me = this,
            el = me.element,
            opts = me.options;

        if (!opts.data) opts.data = [];

        el.addClass("fieldcontainer");

        el.data("fieldcontainer", me);

        me._dragdrop();

        if (opts.fieldsetDraggable) {
            el.addClass("fieldcontainer-draggable");
            me._fieldsetDragDrop();
        }

        ///////////////////////////////////////////////////////////////////

        el.on("click dblclick", ".field-item", function (event) {

            //if (opts.draggable) return;

            var jq = $(event.currentTarget);
            //alert(jq[0].outerHTML);

            var uid = jq.attr("data-uid");
            var field = me.getByUid(uid);
            //alert(field.name);

            var jq = jq.closest(".fieldset-item");
            var uid = jq.attr("data-uid");
            var fieldset = me.getByUid(uid);
            //alert(fieldset.name);

            var e = { field: field, fieldset: fieldset, event: event };

            var eventName = event.type == "click" ? "onFieldClick" : "onFieldDblClick";

            if (me.options[eventName]) me.options[eventName].call(me, e);

        });


        me.refresh();
    },

    refresh: function () {
        var me = this,
            el = me.element,
            opts = me.options,
            data = opts.data,
            sb = [];



        me._uids = {};

        function createField(index, item) {
            if (!item.uid) item.uid = item.fldNo;
            me._uids[item.uid] = item;
            sb.push('<div data-uid="' + item.uid + '" class="' + opts.fieldCls + (item.full ? ' full' : '') + '">' + item.formName + '</div>');
        }

        function createFieldset(index, item) {

            if (!item.uid) item.uid = item.fdsNo;
            me._uids[item.uid] = item;
            if (!item.fields) item.fields = [];

            sb.push('<div data-uid="' + item.uid + '" data-index="' + index + '" class="' + opts.fieldsetCls + '"><div class="fieldset-item-header">' + item.fdsName + '</div>');
            sb.push('<div class="' + opts.fieldsCls + '">');
            $.each(item.fields, createField);
            sb.push('</div>');
            sb.push('</div>');
        }

        if (opts.fieldset) {
            $.each(data, createFieldset);
        } else {
            $.each(data, createField);
        }
        var html = sb.join('');
        el.html(html);
    },

    getData: function () {
        return this.options.data;
    },
    setData: function (data) {
        this.options.data = data;
        this.refresh();
    },
    getByUid: function (uid) {
        return this._uids[uid];
    },
    updateItem: function (id, fields) {
        var me = this,
            opts = me.options,
            data = opts.data;
        function loop(items, index) {
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.fields) {
                    loop(item.fields, i);
                }
                else {
                    if (item.fldNo == id) {
                        item = $.extend({}, item, fields);
                        opts.data[index].fields[i] = item;
                    }
                }
            }
        }

        loop(data);
        me.refresh();
    },
    removeItem: function (item) {
        function each(items) {
            for (var i = 0, l = items.length; i < l; i++) {
                var o = items[i];
                if (o == item) {
                    items.removeAt(i);
                    break;
                }
                if (o.fields) each(o.fields);
            }
        }
        each(this.options.data);
    },

    _dragdrop: function () {

        var me = this,
            el = me.element,
            opts = me.options,
            data = opts.data,
            selector = "." + opts.fieldCls,
            helper, box, cssPosition,
            ghost;

        function doEnd() {

            var ct = ghost.closest(".fieldcontainer").data("fieldcontainer");
            if (!ct) {
                me.refresh()
                return;
            }

            var uid = parseInt(helper.attr("data-uid")),
                item = me.getByUid(uid);
            //data.splice(index, 1);
            me.removeItem(item);

            ;
            if (ct.options.fieldset) {
                var jq = ghost.closest(".fieldset-item");
                index = jq.find(".fieldset-fields").children().index(ghost);

                var fieldset = ct.getByUid(jq.attr("data-uid"));

                fieldset.fields.splice(index, 0, item);


            } else {
                var jq = ct.element.children()
                index = jq.index(ghost);
                ct.options.data.splice(index, 0, item);
            }


            ct.refresh();
            if (ct != me) me.refresh();
        }

        //////////////////////////////////////////////////////////////////

        function handleGhost(event, jq) {
            var x = event.pageX,
                y = event.pageY;
            //data = ct.options.data,
            //jq = ct.element;
            var action = "no", node;

            //var nodes = jq.children("." + opts.itemCls);
            var nodes = jq.find(selector);
            var lastNode = nodes[nodes.length - 1];
            if (lastNode) {
                var lastBox = getBox(lastNode);
                if (y > lastBox.bottom) action = 'append';
            }
            if (action != 'append') {
                if (nodes.length == 0) {
                    action = "append";
                } else {
                    nodes.each(function () {

                        var box = getBox(this);
                        var inY = box.top <= y && y <= box.bottom;
                        var inX = box.left <= x && x <= box.right;
                        if (inY && inX) {

                            if (this == ghost[0]) {
                                action = "no";
                                return;
                            }
                            node = $(this);
                            if (y <= box.top + box.height / 2) {
                                action = "before";
                            } else {
                                action = "after";
                            }

                            //                        if (x <= box.left + box.width / 2) {
                            //                            action = "before";
                            //                        } else {
                            //                            action = "after";
                            //                        }

                        }
                    });
                }
            }

            if (action == "before") {
                ghost.insertBefore(node);
            } else if (action == 'after') {
                ghost.insertAfter(node);
            } else if (action == "append") {
                ghost.appendTo(jq);
            }

            document.title = action + ":" + Date.now();
        }

        function handleFieldsetGhost(event) {
            var x = event.pageX,
                y = event.pageY;
            var action = "no", node;

            var el = getFieldsetByEvent(event);

            if (!el) {

                ghost.remove()
                return;
            }

            handleGhost(event, $(el).find(".fieldset-fields"));
            //document.title = Date.now();
        }

        function doDrag(event) {
            var x = event.pageX,
                y = event.pageY,
                ct = getFieldContainerByEvent(event);

            //document.title = ct + "," + x + "," + y;

            if (!ct) {
                ghost.remove()
                return;
            }

            if (ct.options.fieldset) {
                handleFieldsetGhost(event);
            } else {
                handleGhost(event, ct.element);
            }

        }
        //        function _onItemClick(event) {
        //            $(".field-item").on("click", function (event) {
        //                alert(1);
        //            })
        //        }
        /////////////////////////////////////////////////////////////////////

        var drag = new DragBase(el, {

            beforedrag: function (event, ui) {
                if (!me.options.draggble) return false

                //                if (!me.options.draggble) { me._onItemClick(event); return false }

                helper = $(event.target).closest(selector);
                if (!helper[0]) return false;
            },

            dragstart: function (event, ui) {
                box = getBox(helper);
                ghost = $('<div class="' + opts.ghostCls + '"></div>')
                ghost.insertAfter(helper);
                if (helper.hasClass("full")) ghost.addClass("full");

                helper.appendTo("body");
                helper.css({ position: "absolute", opacity: 1 });
                helper.offset(box);
                helper.width(box.width);
                cssPosition = helper.position();
            },

            drag: function (event, ui) {

                var xOffset = event.pageX - ui.event.pageX,
                    yOffset = event.pageY - ui.event.pageY;

                helper.css({
                    left: cssPosition.left + xOffset,
                    top: cssPosition.top + yOffset
                });

                doDrag(event);
            },

            dragend: function (event, ui) {

                doEnd();

                helper.remove();
                helper = null;

                ghost.remove();
                ghost = null;

            }
        });

    },

    _fieldsetDragDrop: function () {
        var me = this,
            el = me.element,
            opts = me.options,
            data = opts.data,
            selector = "." + opts.fieldsetCls,
            helper, box, cssPosition,
            ghost;

        function doEnd() {

            var ct = ghost.closest(".fieldcontainer").data("fieldcontainer");
            if (!ct) {
                me.refresh()
                return;
            }

            var index = parseInt(helper.attr("data-index")),
                item = data[index];
            data.splice(index, 1);


            var jq = ct.element.children(),
                index = jq.index(ghost);

            ct.options.data.splice(index, 0, item);
            ct.refresh()

            if (ct != me) me.refresh();
        }

        function doDrag(event) {
            var x = event.pageX,
                y = event.pageY,
                ct;

            //var jq = $(event.target);
            //if (!jq.closest(".fieldcontainer-draggable")[0]) return;

            $(".fieldcontainer").each(function () {
                if (!$(this).hasClass("fieldcontainer-draggable")) return;

                var box = getBox(this);
                if (box.top <= y && y <= box.bottom
                    && box.left <= x && x <= box.right
                ) {
                    ct = $(this).data("fieldcontainer");
                }
            });

            //document.title = ct + "," + x + "," + y;

            if (!ct) {

                ghost.remove()
                return;
            }

            //document.title = new Date().getTime();

            var data = ct.options.data,
                jq = ct.element;

            var action = "append", node;

            //var nodes = jq.children("." + opts.itemCls);
            var nodes = jq.children();
            nodes.each(function () {

                var box = getBox(this);
                if (box.top <= y && y <= box.bottom) {
                    if (this == ghost[0]) {
                        action = "no";
                        return;
                    }
                    node = $(this);
                    if (y <= box.top + box.height / 2) {
                        //if(node.attr("data-index")=="0") debugger
                        action = "before";
                    } else {
                        action = "after";
                    }
                }
            });

            if (action == "before") {
                ghost.insertBefore(node);
            } else if (action == 'after') {

                ghost.insertAfter(node);
            } else if (action == "append") {
                ghost.appendTo(jq);
            }

            //document.title = Date.now() + "" + action;

        }

        /////////////////////////////////////////////////////////////////////

        var drag = new DragBase(el, {

            beforedrag: function (event, ui) {
                if (!me.options.draggble) return false
                if (!$(event.target).closest(".fieldset-item-header")[0]) return false;
                helper = $(event.target).closest(selector);
                if (!helper[0]) return false;
            },

            dragstart: function (event, ui) {

                box = helper.offset();

                ghost = $('<div class="' + opts.fieldsetGhostCls + '"></div>')
                ghost.insertAfter(helper);


                helper.appendTo("body");
                helper.css({ position: "absolute", opacity: 1 });
                helper.offset(box);
                cssPosition = helper.position()
            },

            drag: function (event, ui) {
                var xOffset = event.pageX - ui.event.pageX,
                    yOffset = event.pageY - ui.event.pageY;

                helper.css({
                    left: cssPosition.left + xOffset,
                    top: cssPosition.top + yOffset
                });

                doDrag(event);
            },

            dragend: function (event, ui) {

                doEnd();

                helper.remove();
                helper = null;

                ghost.remove();
                ghost = null;

            }
        });
    }

};

function getBox(element) {
    var jq = $(element);
    var box = jq.offset()
    box.width = jq.outerWidth();
    box.height = jq.outerHeight();
    box.right = box.left + box.width;
    box.bottom = box.top + box.height;
    return box;
}

function getFieldContainerByEvent(event) {
    var x = event.pageX,
        y = event.pageY;
    var ct;
    $(".fieldcontainer").each(function () {
        var box = getBox(this);
        if (box.top <= y && y <= box.bottom
                    && box.left <= x && x <= box.right
                ) {
            ct = $(this).data("fieldcontainer");
        }
    });
    return ct;
}

function getFieldsetByEvent(event) {
    var x = event.pageX,
        y = event.pageY;
    var ct;
    $(".fieldset-item").each(function () {
        var box = getBox(this);
        if (box.top <= y && y <= box.bottom
                    && box.left <= x && x <= box.right
                ) {
            ct = this;
        }
    });
    return ct;
}

