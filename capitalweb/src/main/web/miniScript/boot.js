﻿__CreateJSPath = function (js) {
    var scripts = document.getElementsByTagName("script");
    var path = "";
    for (var i = 0, l = scripts.length; i < l; i++) {
        var src = scripts[i].src;
        if (src.indexOf(js) != -1) {
            var ss = src.split(js);
            path = ss[0];
            break;
        }
    }
    var href = location.href;
    href = href.split("#")[0];
    href = href.split("?")[0];
    var ss = href.split("/");
    ss.length = ss.length - 1;
    href = ss.join("/");
    if (path.indexOf("https:") == -1 && path.indexOf("http:") == -1 && path.indexOf("file:") == -1 && path.indexOf("\/") != 0) {
        path = href + "/" + path;
    }
    return path;
};

var bootPATH = __CreateJSPath("boot.js");

mini_debugger = false;

//miniui
// document.write('<script src="' + bootPATH + 'jquery.min.js" type="text/javascript"></sc' + 'ript>');
// document.write('<script src="' + bootPATH + 'miniui/miniui.js" type="text/javascript" ></sc' + 'ript>');
// document.write('<link href="' + bootPATH + 'miniui/themes/default/iconfont/iconfont.css" rel="stylesheet" type="text/css" />');
// document.write('<link href="' + bootPATH + 'miniui/themes/default/miniui.css" rel="stylesheet" type="text/css" />');
//document.write('<link href="' + bootPATH + 'miniui/local/zh_EN.js" type="text/javascript"></sc' + 'ript>');
//font-awesome.min.css不支持ie8以下，ie11以上可以运行
//document.write('<link href="' + bootPATH + 'miniui/res/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />');

//common
// document.write('<link href="' + bootPATH + 'miniui/themes/icons.css" rel="stylesheet" type="text/css" />');
//document.write('<link href="' + bootPATH + 'miniui/themes/learun/skin.css" rel="stylesheet" type="text/css" />');
//document.write('<link href="' + bootPATH + 'miniui/themes/xinli/skin.css" rel="stylesheet" type="text/css" />');
//document.write('<link href="' + bootPATH + 'miniui/themes/xinli-Jiangsu/skin.css" rel="stylesheet" type="text/css" />');
// document.write('<link href="' + bootPATH + 'miniui/themes/xinli-red/skin-red.css" rel="stylesheet" type="text/css" />');
// document.write('<script src="' + bootPATH + 'sockjs.min-1.1.1.js" type="text/javascript" ></sc' + 'ript>');
// document.write('<script src="' + bootPATH + 'stomp.min-2.3.3.js" type="text/javascript"/>');
// document.write('<script src="' + bootPATH + 'stomp.min-2.3.3.js" type="text/javascript" ></sc' + 'ript>');
// document.write('<link href="' + bootPATH + 'jqueryToast/css/toast.style.min.css" rel="stylesheet"/>');
// document.write('<script src="' + bootPATH + 'jqueryToast/js/toast.script.js" type="text/javascript" ></sc' + 'ript>');
////后续皮肤切换会使用,目前暂时无用
//var mode = getCookie("miniuiMode");
//if (mode) {
//    document.write('<link href="' + bootPATH + 'miniui/themes/default/' + mode + '-mode.css" rel="stylesheet" type="text/css" />');
//}
//
////skin
//var skin = getCookie("miniuiSkin");
//if (skin) {
//    document.write('<link href="' + bootPATH + 'miniui/themes/' + skin + '/skin.css" rel="stylesheet" type="text/css" />');
//}


////////////////////////////////////////////////////////////////////////////////////////
function getCookie(sName) {
    var aCookie = document.cookie.split("; ");
    var lastMatch = null;
    for (var i = 0; i < aCookie.length; i++) {
        var aCrumb = aCookie[i].split("=");
        if (sName == aCrumb[0]) {
            lastMatch = aCrumb;
        }
    }
    if (lastMatch) {
        var v = lastMatch[1];
        if (v === undefined) return v;
        return unescape(v);
    }
    return null;
}