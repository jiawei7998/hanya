﻿
var MenuItemUid = 1;

var Menu = function (element, options) {

    this.element = $(element);
    this.options = $.extend(true, {}, this.options, options);
    this.init();

}
Menu.prototype = {
    name: 'menu',

    options: {
        mode: 'vertical',           //vertical, horizontal, inline
        theme: 'light',             //light|dark  
        multiple: false,            //是否允许多选
        inlineCollapsed: false,     //inline时菜单是否收起状态
        selectable: true,           //是否允许选中
        iconfont:true                   //是否显示菜单图片
    },

    init: function () {
        var me = this,
            el = me.element,
            opts = me.options,
            mode = opts.mode;

        if (!opts.items) opts.items = [];



        this.cascade(function (item) {
            item.uid = MenuItemUid++;
        });

        var html = '<ul class="ant-menu">' + me._createItems(me.options.items,opts.iconfont) + '</ul>';
        var menu = $(html);
        menu.addClass("ant-menu-" + mode);
        menu.addClass("ant-menu-" + opts.theme);
        menu.addClass("ant-menu-root");

        var li = menu.find(".ant-menu-submenu"),
            ul = menu.find(".ant-menu-sub");

        li.addClass("ant-menu-submenu-" + mode);
        if (mode == 'horizontal') {
            ul.addClass("ant-menu-vertical");
        } else if (mode == 'inline') {
            ul.addClass("ant-menu-inline");
        } else {
            ul.addClass("ant-menu-vertical");
        }

        me.menu = menu;

        this.cascade(function (item) {
            if (item.expanded) me.open(item);
        });

        el.on("mouseenter", ".ant-menu-submenu", function (event) {
            if (mode == 'inline' && !opts.inlineCollapsed) return;
            var item = me.getSubMenuByEvent(event);
            if (!item) return;
            clearTimeout(me.timeout);
            me.timeout = setTimeout(function () {
                me.open(item);
            }, 100);
        });

        el.on("mouseleave", ".ant-menu-submenu, .ant-menu-sub", function (event) {
            if (mode == 'inline' && !opts.inlineCollapsed) return;
            var item = me.getSubMenuByEvent(event);
            if (!item) return;
            clearTimeout(me.timeout);
            me.timeout = setTimeout(function () {
                me.close(item);
            }, 150);
        });

        el.on("click", ".ant-menu-submenu-title", function (event) {
            if (mode != 'inline') return;
            var item = me.getItemByEvent(event);
            if (!item) return;
            me.toggle(item);
        });

        ////////////////////////////////////////////////////////////////////////////////////////
        var scrollElement = $('<div class="ant-menu-scrollable"></div>').appendTo(el);
        menu.appendTo(scrollElement);
        $('<div class="ant-menu-arrows"><div class="ant-menu-arrow-left"></div><div class="ant-menu-arrow-right"></div></div>').appendTo(el);


        var scrollOffset = 10;
        function startScroll(value) {

            function step() {
                scrollElement[0].scrollLeft += value;
                timer = setTimeout(step, 30);
            }

            var timer;

            $(document).mouseup(function () {
                if (timer) clearTimeout(timer);
            });

            timer = setTimeout(step, 30);
        }
        el.find(".ant-menu-arrow-left").mousedown(function (event) {
            startScroll(-scrollOffset);
        });
        el.find(".ant-menu-arrow-right").mousedown(function (event) {
            startScroll(scrollOffset);
        });

        $(window).resize(function () {
            me.doLayout();
        });
        me.doLayout();
    },

    ///////////////////////////////////////////////////////////////////////////////////////

    doLayout: function () {
        var me = this,
            el = me.element,
            opts = me.options,
            menu = me.menu;

        el.addClass("ant-menu-no-arrows");
        if (opts.mode != 'horizontal') return;

        var scrollable = el.find(".ant-menu-scrollable"),
            outWidth = scrollable[0].scrollWidth > scrollable.width();

        if (outWidth) {
            el.removeClass("ant-menu-no-arrows");
        }

    },

    _createItems: function (items,iconfont) {
        if (!items) return '';
        var me = this,
            sb = [];
        for (var i = 0, l = items.length; i < l; i++) {
            var item = items[i];
            var str;
            if (item.type == 'menugroup') {
                str = me._createMenuGroup(item,iconfont);
            } else if (item.type == 'submenu' || item.items) {
                str = me._createSubMenu(item,iconfont);
            } else {
                str = me._createMenuItem(item,iconfont);
            }
            sb.push(str);
        }
        return sb.join('');
    },

    _createItemTitle: function (item, prefixCls, arrow,iconfont) {
        var icon = item.icon&&iconfont ? '<i class="sl-icon iconfont ' + item.icon + ' " style="padding-left:2px"> </i>' : '<i class="sl-icon" style="padding-left:20px"> </i>',
            text = item.text&&iconfont ? '<span class="' + prefixCls + '-text">' + item.text + '</span>' : '<span class="' + prefixCls + '-text" style="font-size: 15px">' + item.text + '</span>',
            href = item.href ? 'href="' + item.href + '" target="' + (item.hrefTarget || '') + '"' : '',
            arrow = arrow ? '<i class="' + prefixCls + '-arrow"></i>' : '';

        return '<a class="' + prefixCls + '-title" ' + href + '>' + icon + text + arrow + '</a>';
    },

    _createMenuItem: function (item,iconfont) {
        var html = '<li title="' + item.text + '" uid="' + item.uid + '" name="' + item.name + '"  url="' + item.url + '" class="ant-menu-item ' + (item.cls || '') + '">' + this._createItemTitle(item, 'ant-menu-item',false,iconfont) + '</li>';
        return html;
    },

    _createSubMenu: function (item,iconfont) {
        var me = this,
            html = '<li uid="' + item.uid + '" url="' + item.url + '" class="ant-menu-submenu ' + (item.cls || '') + '">';
        html += this._createItemTitle(item, 'ant-menu-submenu', true,iconfont);
        html += '<ul class="ant-menu ant-menu-sub ant-menu-hidden">' + this._createItems(item.items) + '</ul>';
        html += '</li>';
        return html;
    },

    _createMenuGroup: function (item,iconfont) {
        var me = this,
            html = '<li uid="' + item.uid + '" class="ant-menu-item-group ' + (item.cls || '') + '">';
        html += '<div class="ant-menu-item-group-title">' + item.text + '</div>';
        html += '<ul class="ant-menu-item-group-list">' + this._createItems(item.items,iconfont) + '</ul>';
        html += '</li>';
        return html;
    },

    /////////////////////////////////////////////////////////////////////////////////////////////

    cascade: function (fn, scope) {
        var isBreak = false;
        function each(items) {
            if (isBreak) return;
            for (var i = 0, l = items.length; i < l; i++) {
                var item = items[i];
                if (fn.call(scope || this, item) === false) {
                    isBreak = true;
                    break;
                }
                if (item.items) each(item.items);
            }
        }
        each(this.options.items);
    },

    getItemByName: function (name) {
        var found;
        this.cascade(function (item) {
            if (item.name == name) {
                found = item;
                return false;
            }
        });
        return found;
    },

    getItemByUid: function (uid) {
        var found;
        this.cascade(function (item) {
            if (item.uid == uid) {
                found = item;
                return false;
            }
        });
        return found;
    },

    getItemEl: function (item) {
        var uid = typeof item == 'object' ? item.uid : item;
        return this.element.find("[uid=" + uid + "]");
    },

    getItemByEvent: function (event) {
        var el = $(event.target).closest('.ant-menu-item');
        if (!el[0]) el = $(event.target).closest('.ant-menu-submenu');
        var uid = el.attr("uid");
        return this.getItemByUid(uid);
    },

    getSubMenuByEvent: function (event) {
        var el = $(event.target).closest('.ant-menu-submenu');
        var uid = el.attr("uid");
        return this.getItemByUid(uid);
    },

    open: function (item) {
        var el = this.getItemEl(item);
        item.opened = true;

        //if (item.uid == 12) debugger

        var ul = el.children("ul.ant-menu");
        ul.removeClass("ant-menu-hidden");
        el.addClass("ant-menu-submenu-open");
        if (this.onOpenChange) this.onOpenChange(item);
    },

    close: function (item) {
        var el = this.getItemEl(item);
        item.opened = false;

        var ul = el.children("ul.ant-menu");
        ul.addClass("ant-menu-hidden");
        el.removeClass("ant-menu-submenu-open");
        if (this.onOpenChange) this.onOpenChange(item);
    },

    toggle: function (item) {
        if (item.opened) this.close(item);
        else this.open(item);
    },

    getSelected: function () {
        var found;
        this.cascade(function (item) {
            if (item.selected) {
                found = item;
                return false;
            }
        });
        return found;
    },

    select: function (item) {
        var el = this.getItemEl(item);

        var selected = this.getSelected();
        if (selected) this.deselect(selected);

        item.selected = true;

        el.addClass("ant-menu-item-selected");
        this.trigger("select", { item: item });
    },

    deselect: function (item) {
        var el = this.getItemEl(item);
        item.selected = false;

        el.removeClass("ant-menu-item-selected");
        this.trigger("deselect", { item: item });
    },

    setTheme: function (value) {
        var el = this.menu;
        this.options.theme = value;
        el.removeClass("ant-menu-dark ant-menu-light").addClass("ant-menu-" + value);
    },

    setMode: function (value) {
        var el = this.menu;
        this.options.mode = value;
        el.removeClass("ant-menu-vertical ant-menu-horizontal").addClass("ant-menu-" + value);
        this.doLayout();
    },

    setInlineCollapsed: function (value) {
        this.options.inlineCollapsed = value;

        var menu = this.menu;
        var li = menu.find(".ant-menu-submenu"),
            ul = menu.find(".ant-menu-sub");

        if (value) {
            menu.removeClass("ant-menu-inline").addClass("ant-menu-vertical ant-menu-inline-collapsed");
            li.removeClass("ant-menu-submenu-inline").addClass("ant-menu-submenu-vertical");
            ul.removeClass("ant-menu-inline").addClass("ant-menu-vertical");
        } else {
            menu.addClass("ant-menu-inline").removeClass("ant-menu-vertical ant-menu-inline-collapsed");
            li.addClass("ant-menu-submenu-inline").removeClass("ant-menu-submenu-vertical");
            ul.addClass("ant-menu-inline").removeClass("ant-menu-vertical");
        }

    }

};




$.fn.menu = function (options) {

    var isSTR = typeof options == "string",
        args, ret;

    if (isSTR) {
        args = $.makeArray(arguments)
        args.splice(0, 1);
    }

    var name = "menu",
        type = Menu;

    var jq = this.each(function () {
        var ui = $.data(this, name);

        if (!ui) {
            ui = new type(this, options);
            $.data(this, name, ui);
        }
        if (isSTR) {
            ret = ui[options].apply(ui, args);
        }
    });

    return isSTR ? ret : jq;
};