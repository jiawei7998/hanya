﻿
var Calendar = function (element, options) {

    this.el = $(element);
    this.options = $.extend(true, {}, this.options, options);
    this.init();
}
Calendar.prototype = {

    options: {

        calendar: ["日", "一", "二", "三", "四", "五", "六"],
        newDate: new Date(),
        getHoliDay: null,
    },

    init: function () {
        var me = this,
            el = me.el,
            opts = me.options;

        me.holiDays = new Object();

        me.focusDays = [];

        el.addClass("calendar-content");

        me._createToolbar();

        me._createCalendar();


        $(document.body).on("click", ".month-prev", function () {

            me.prevMonth(opts.newDate);
        })

        $(document.body).on("click", ".month-next", function () {

            me.nextMonth(opts.newDate);
        })
        //触发选择年份
        $(document.body).on("click", ".calendar-toolbar-year", function () {

            $(".dropdown-year").toggleClass("open");

            //创建下拉数据
            var yearText = opts.newDate.getFullYear();
            var sb = [];
            for (var i = 0; i < 11; i++) {

                if (i == 5) {
                    sb[sb.length] = '<li class="year-item active">'
                }
                else {
                    sb[sb.length] = '<li class="year-item">'
                }

                sb[sb.length] = '<span class="year-check">' + (yearText - 5 + i) + '</span>'
                sb[sb.length] = '<span >年</span>'
                sb[sb.length] = '</li>'
            }

            document.getElementById("dropdown-year").innerHTML = sb.join("");

        })
        //日期拖选
        $(el).on("mousedown", ".calendar-daynumber", function (e) {
            var startDate = me._cloneDate(opts.newDate)

            var dateText = parseInt($(this).find(".solar")[0].innerText);
            if ($(this).hasClass("calendar-othermonth")) {
                if (dateText > 15) {
                    dateText = 1;
                }
                else {
                    dateText = me.getMonthCount(startDate);
                }
            }

            startDate.setDate(dateText);

            me.startDate = startDate;

        })
        //$(el).on("mouseup", ".calendar-daynumber", function (e) {

        //    if (!me.startDate) return;
        //    var endDate = me._cloneDate(opts.newDate)

        //    var dateText = parseInt($(this).find(".solar")[0].innerText);
        //    if ($(this).hasClass("calendar-othermonth")) {
        //        if (dateText > 15) {
        //            dateText = 1;
        //        }
        //        else {
        //            dateText = me.getMonthCount(endDate);
        //        }
        //    }
        //    endDate.setDate(dateText);
        //    me.endDate = endDate;

        //    me.createRange();
        //})


        //年份check事件
        $(document.body).on("click", ".year-item", function (e) {

            me.focusDays = [];
            me.holiDays = [];

            var yearText = parseInt($(e.currentTarget).find(".year-check")[0].innerText);

            var thisDate = me._cloneDate(me.options.newDate);

            thisDate.setFullYear(yearText);

            me.options.newDate = thisDate;

            me.holiDays.date = me._refreshCalendar(thisDate);

            $(".toolbar-year")[0].innerHTML = thisDate.getFullYear();


       
        })

        //触发选择月份
        $(document.body).on("click", ".calendar-toolbar-month", function () {

            $(".dropdown-month").toggleClass("open");
        })
        //月份check事件
        $(document.body).on("click", ".month-item", function (e) {

            me.focusDays = [];
            me.holiDays = [];


            var monthText = parseInt($(e.currentTarget).find(".month-check")[0].innerText);

            var newDate = me._cloneDate(me.options.newDate);
            newDate.setMonth(monthText - 1);

            me.options.newDate = newDate;

            me.holiDays.date = me._refreshCalendar(newDate);


            $(".toolbar-month")[0].innerHTML = newDate.getMonth() + 1;

        })
        //今天
        $(document.body).on("click", ".totoday", function () {

            me.options.newDate = me.newDate = new Date();

            me.holiDays.date = me._refreshCalendar(me.newDate);

            $(".toolbar-year")[0].innerHTML = me.newDate.getFullYear();
            $(".toolbar-month")[0].innerHTML = me.newDate.getMonth() + 1;
        })

        //选中
        $(document.body).on("click", ".calendar-daynumber.calendar-month", function (e) {

            var event = e.currentTarget;

            $(event).toggleClass("focus")

            //获取方式是刷新获取所有focus还是判断有没有

            var newFocusDays = [];
            $(".calendar-daynumber.focus").each(function () {
                var innerText = $(this).find(".solar")[0].innerText;


                if (innerText.length == 1) {
                    innerText = 0 + innerText;
                }

                var monthText = opts.newDate.getMonth() + 1;

                monthText = String(monthText);
                if (monthText.length == 1) {
                    monthText = 0 + monthText;
                }

                newFocusDays.push(opts.newDate.getFullYear() + "-" + monthText + "-" + innerText)
            })
            me.focusDays = newFocusDays;

        })

    },
    prevMonth: function (newDate) {

        var me = this;
        me.focusDays = [];
        me.holiDays = [];

        me.newDate = new Date(newDate.setMonth(newDate.getMonth() - 1));

        me.holiDays.date = me._refreshCalendar(me.newDate);
        $(".toolbar-year")[0].innerHTML = me.newDate.getFullYear();
        $(".toolbar-month")[0].innerHTML = me.newDate.getMonth() + 1;

        //me.focusDays = [];

    },
    nextMonth: function (newDate) {

        var me = this;

        me.focusDays = [];
        me.holiDays = [];

        me.newDate = new Date(newDate.setMonth(newDate.getMonth() + 1));

        me.holiDays.date = me._refreshCalendar(me.newDate);

        $(".toolbar-year")[0].innerHTML = me.newDate.getFullYear();
        $(".toolbar-month")[0].innerHTML = me.newDate.getMonth() + 1;

        //me.focusDays = [];

    },

    _createToolbar: function () {
        var me = this;
        var newDate = me.options.newDate;
        var sb = [];
        sb[sb.length] = '<div class="calendar-toolbar">'

	sb[sb.length] = '<div class="calendar-toolbar-color"></div><div class="calendar-toolbar-text">节假日</div>'
        sb[sb.length] = '<div class="calendar-toolbar-centerbox">'
        sb[sb.length] = '<div class="month-prev"><a class="sl-icon sl-left-black"></a></div>'
        sb[sb.length] = '<div class="btn-group">'
        sb[sb.length] = '<div class="btn calendar-toolbar-year">'

        sb[sb.length] = '<span class="toolbar-year">' + newDate.getFullYear() + '</span>'
        sb[sb.length] = '<span>年</span>'

        sb[sb.length] = '<span class="fa fa-caret-down"></span>'

        //创建年份下拉(动态创建)
        sb[sb.length] = '<ul id="dropdown-year" class="dropdown-year">'
        sb[sb.length] = '</ul>'

        sb[sb.length] = '</div>'

        sb[sb.length] = '<div class="btn calendar-toolbar-month">'
        sb[sb.length] = '<span class="toolbar-month">' + (newDate.getMonth() + 1) + '</span>'
        sb[sb.length] = '<span>月</span>'
        sb[sb.length] = '<span class="sl-icon sl-location-black"></span>'

        //创建月份下拉(写死)
        sb[sb.length] = '<ul class="dropdown-month">'
        for (var i = 1; i <= 12; i++) {
            sb[sb.length] = '<li class="month-item">'
            sb[sb.length] = '<span class="month-check">' + i + '</span>'
            sb[sb.length] = '<span >月</span>'
            sb[sb.length] = '</li>'
        }
        sb[sb.length] = '</ul>'
        sb[sb.length] = '</div>'
        sb[sb.length] = '</div>'
        sb[sb.length] = '<div class="month-next"><a class="sl-icon sl-right-black"></a></div>'
        sb[sb.length] = '</div>'

        sb[sb.length] = '<div class="calendar-toolbar-rightbox"><span class="btn totoday">今天</span></div >'
        sb[sb.length] = '</div>'
        $(sb.join('')).appendTo(me.el);
    },
    _createCalendar: function () {

        var me = this;
        var newDate = me.options.newDate;
        var sb = [];
        sb[sb.length] = '<div class="calendar">'

        //sb[sb.length] = '<colgroup><colwidth="1"/><colwidth="1"/><colwidth="1"/><colwidth="1"/><colwidth="1"/><colwidth="1"/><colwidth="1"/></colgroup>'
        me._createCalendarHeader(sb);

        me._createCalendarMain(sb, newDate);


        sb[sb.length] = '</div>'


        $(sb.join('')).appendTo(me.el);

        me.holiDays.date = me._refreshCalendar(newDate);
    },
    _createCalendarHeader: function (sb) {
        var me = this;
        var cal = me.options.calendar;
        sb[sb.length] = '<table cellspacing="0" cellpadding="0" style="width:100%">'
        sb[sb.length] = '<thead>'
        sb[sb.length] = '<tr class="calendar-header">'
        sb[sb.length] = ' <td class="calendar-weekend">' + cal[0] + '</td>'
        sb[sb.length] = ' <td>' + cal[1] + '</td>'
        sb[sb.length] = ' <td>' + cal[2] + '</td>'
        sb[sb.length] = ' <td>' + cal[3] + '</td>'
        sb[sb.length] = ' <td>' + cal[4] + '</td>'
        sb[sb.length] = ' <td>' + cal[5] + '</td>'
        sb[sb.length] = ' <td class="calendar-weekend">' + cal[6] + '</td>'
        sb[sb.length] = '</tr>'
        sb[sb.length] = '</thead>'
        sb[sb.length] = '</table>'
    },
    _createCalendarMain: function (sb, newDate) {
        sb[sb.length] = '<div id="calendar-body">'
        sb[sb.length] = '</div>'
    },
    _refreshCalendar: function (newDate) {

        var sb = [];
        var me = this;

        var _newDate = me._cloneDate(newDate);

        //当前date
        var nowNum = _newDate.getDate();

        //第一天周几
        _newDate.setDate(1);

        var weekDay = _newDate.getDay();

        //视图第一天（的前一天)
        var viewDate = me._cloneDate(_newDate);

        viewDate.setDate(viewDate.getDate() - weekDay - 1);

        //获取当前视图holiDay
        if (me.options.getHoliDay) {
            var holiDays = me.options.getHoliDay(newDate)
        }
        else {
            var holiDays = me.getHoliDay(newDate);
        }


        //月视图
        var trNum = 6;

        sb[sb.length] = '<table cellspacing="0" cellpadding="0" style="width:100%">'
        for (var i = 0; i < trNum; i++) {
            sb[sb.length] = '<tr>'

            for (var l = 0; l < 7; l++) {
                sb[sb.length] = '<td>'
                viewDate.setDate(viewDate.getDate() + 1);

                if (viewDate.getMonth() != newDate.getMonth()) {
                    sb[sb.length] = '<div class="calendar-daynumber calendar-othermonth '
                }
                else {
                    if (viewDate.getDate() == newDate.getDate()) {
                        sb[sb.length] = '<div class="calendar-daynumber calendar-month  today '
                    }
                    else {
                        sb[sb.length] = '<div class="calendar-daynumber calendar-month  '
                    }
                }

                if (!Array.prototype.indexOf) {
                    Array.prototype.indexOf = function (val) {
                        var value = this;
                        for (var i = 0; i < value.length; i++) {
                            if (value[i] == val) return i;
                        }
                        return -1;
                    };
                }
                if (holiDays.indexOf(viewDate.getTime()) > -1) {
                    sb[sb.length] = 'calendar-weekend "'
                    sb[sb.length] = '>'
                }
                else {
                    sb[sb.length] = '">'
                }


                sb[sb.length] = '<span class="solar">' + viewDate.getDate() + '</span>'
                //var lunar = me.solar2lunar(viewDate);
                var lunar = calendarChange.solar2lunar(viewDate.getFullYear(), (viewDate.getMonth() + 1), viewDate.getDate());

                //alert(JSON.stringify(lunar))
                if (lunar.isTerm === true) {
                    sb[sb.length] = '<span class="lunar">' + lunar.Term + '</span >'
                }
                else {
                    sb[sb.length] = '<span class="lunar">' + lunar.IDayCn + '</span >'
                }

                sb[sb.length] = '</div>'
                sb[sb.length] = '</td>'
            }
            sb[sb.length] = '</tr>'
        }
        sb[sb.length] = '</table">'
        document.getElementById('calendar-body').innerHTML = sb.join("");

        return holiDays;
    },
    getMonthCount: function (nowDate) {

        var nowDate = new Date(nowDate.getTime());
        var nowMonth = nowDate.getMonth();

        nowDate.setMonth(nowMonth + 1);

        nowDate.setDate(0);

        return nowDate.getDate();
    },


    getHoliDay: function (newDate) {
        var me = this;
        if ((me.holiDays.date && me.holiDays.date.length > 0)) {
            return me.holiDays.date
        }
        var holiDays = []

        var _newDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());


        //当前月天数 不要要获取
        var monthCount = me.getMonthCount(_newDate);

        _newDate.setDate(1);

        _newDate.setDate(_newDate.getDate() - 1);

        for (var i = 0; i < monthCount; i++) {
            _newDate.setDate(_newDate.getDate() + 1);
            if (_newDate.getDay() == 0 || _newDate.getDay() == 6) {
                holiDays.push(me._cloneDate(_newDate).getTime())
            }
        }

        return holiDays;
    },
    _cloneDate: function (date) {
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    },
    validate: function (date) {


    },
    //createRange: function () {

    //    var me = this,
    //        opts = me.options;

    //    if (me.startDate > me.endDate) {
    //        _date = me._cloneDate(me.startDate);

    //        me.startDate = me._cloneDate(me.endDate)
    //        me.endDate = me._cloneDate(_date)
    //    }

    //    var newDate = me._cloneDate(me.options.newDate),
    //        startDateText = me.startDate.getDate(),
    //        endDateText = me.endDate.getDate();

    //    $(".calendar-daynumber.calendar-month").each(function (e) {
    //        var thisDateText = $(this).find(".solar")[0].innerText;
    //        if (startDateText <= thisDateText && endDateText >= thisDateText) {

    //            $(this).toggleClass("focus");

    //        }
    //    })

    //    $(".calendar-daynumber.focus").each(function () {
    //        var innerText = $(this).find(".solar")[0].innerText;


    //        if (innerText.length == 1) {
    //            innerText = 0 + innerText;
    //        }

    //        var monthText = opts.newDate.getMonth() + 1;

    //        monthText = String(monthText);
    //        if (monthText.length == 1) {
    //            monthText = 0 + monthText;
    //        }

    //        me.focusDays.push(opts.newDate.getFullYear() + "-" + monthText + "-" + innerText)
    //    })

    //},
    //外部方法,
    getFocusDays: function () {
        var me = this;
        return me.focusDays;
    },
    setUnHolidays: function (date) {
        var me = this;
        for (var i = 0; i < date.date.length; i++) {
            thisYear = date.date[i].slice(0, 4);
            thisMonth = date.date[i].slice(5, 7);
            thisDate = date.date[i].slice(8, 10);
            
            var index = me.holiDays.date.indexOf(new Date(new Date(thisYear, (thisMonth - 1), thisDate)).getTime())
            if (index >= 0) {
                me.holiDays.date.splice(index,1)
            }
        }
        me.holiDays.date = me._refreshCalendar(me.options.newDate);
        me.focusDays = [];
    },

    getHolidays: function () {

        var me = this;
        var obj = new Object();
        var newHoliDays = [];

        for (var i = 0; i < me.holiDays.date.length; i++) {
            var holiDay = new Date(me.holiDays.date[i]);

            newHoliDays.push(holiDay.getFullYear() + "-" + (holiDay.getMonth() + 1) + "-" + holiDay.getDate())
        }

        obj.date = newHoliDays;

        return obj;
    },

    setHolidays: function (date) {
        var me = this;
        var newHoliDays = [];
        for (var i = 0; i < date.date.length; i++) {
            thisYear = date.date[i].slice(0, 4);
            thisMonth = date.date[i].slice(5, 7);
            thisDate = date.date[i].slice(8, 10);

            var dateTime = new Date(new Date(thisYear, (thisMonth - 1), thisDate)).getTime();
            var index = me.holiDays.date.indexOf(dateTime)
            if (index < 0) {
                me.holiDays.date.push(dateTime);
            }
        }
        me.holiDays.date = me._refreshCalendar(me.options.newDate);
        me.focusDays = [];
    }

};


$.fn.calendar = function (options) {

    var isSTR = typeof options == "string",
        args, ret;

    if (isSTR) {
        args = $.makeArray(arguments)
        args.splice(0, 1);
    }

    var name = "calendar",
        type = Calendar;

    var jq = this.each(function () {
        var ui = $.data(this, name);

        if (!ui) {
            ui = new type(this, options);
            $.data(this, name, ui);
        }
        if (isSTR) {
            ret = ui[options].apply(ui, args);
        }
    });

    return isSTR ? ret : jq;
};

