/**
 * 公共导出
 *
 *
 *  var  fields = "<input type='hidden' id='queryDate' name='queryDate' value='"+data.queryDate+"'>";
    fields += "<input type='hidden' id='excelName' name='excelName' value='ywmx.xls'>";
    fields += "<input type='hidden' id='id' name='id' value='ywmx'>";
 * @param fields
 */
function exportHrbExcel(fields){
    mini.confirm("您确认要导出Excel吗?","系统提示",
        function (action) {
            if (action == "ok"){
                var urls = CommonUtil.pPath + "/sl/HrbReportManageController/exportExcelReport"
                $('<form action="'+ urls +'" method="post"> ' + fields + '</form>').appendTo('body').submit().remove();
            }
        }
    );
}