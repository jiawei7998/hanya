var localObj = window.location;
var contextPath = localObj.pathname.split("/")[1];
var pPath = localObj.protocol + "//" + localObj.host + "/" + contextPath;
var ApproveTrdType = {
    config: {}
};
ApproveTrdType.FundTrdType = {Afp: '0101', Rdp: '0102', Rdi: '0103', Rdd: '0104'};
/**
 * @namespace 通用工具函数  CommonUtil
 * @version 0.1
 */
CommonUtil = {
    /**
     服务端数据
     dictionary  字典项
     fullInstitutionInfo 全量机构信息
     fullTaModuleInfo  菜单信息
     productType  产品类型
     bizDate  业务日期
     */
    serverData: {
        dictionary: {},
        fullInstitutionInfo: {},
        fullTaModuleInfo: {},
        fullTtTrdCostInFo: {},
        TtMktCalendarDateInFo: {},
        currentInstitutionInfo: {},
        productType: [],
        bizDate: '',
        partyKindDict: {}
    },

    /**
     * 取对话框参数，仅仅为了兼容老页面，不推荐使用，后续会删除该方法
     * @param  {url}
     * @param  {paramName}
     * @return {String}
     */
    dialogParam: function (url, paramName) {
        try {
            return $.getDialogOptions()["param"][paramName];
        } catch (e) {
            return "";
        }
    },


    /**
     * ajax提交(默认post)，回调方法使用callback属性
     * @param  {param} ajax参数
     * @return {Void}
     @example 示例：
     CommonUtil.ajax({
			url:"/UserController/login", //请求地址
			data:param,	//请求参数
			relbtn:'login_btn',//关联的按钮，ajax请求期间会显示等待图标，无法重复点击
			showLoadingMsg:true,//是否显示进度条 ,可不传,默认为true
			loadingMsg:'正在加载中...',//进度条中的提示信息,可不传,不传默认为"正在处理..."
			messageId:mini.loading('正在加载中...', "请稍后"),//可不传,当设置 showLoadingMsg:false 时,传递自定义的进度条的messageId
			callback:function(data){	//回调方法
				alert('请求成功啦~');
			}
		});
     */
    ajax: function (param) {
        //默认显示加载进度条
        param.showLoadingMsg = CommonUtil.isNull(param.showLoadingMsg) ? true : param.showLoadingMsg;
        if (CommonUtil.isNull(param.messageId) && param.showLoadingMsg) {
            if (param.loadingMsg) {
                param.messageId = mini.loading(param.loadingMsg, "请稍后");
            } else {
                param.messageId = mini.loading("正在处理...", "请稍后");
            }
        }
        var defautlParam = {
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            cache: false,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("__module_id", CommonUtil.pageModuleIdForPage);
                if (param.relbtn) {
                    CommonUtil.setBtnLoading(param.relbtn);
                }
            },
            complete: function (XMLHttpRequest) {
                if (XMLHttpRequest.getResponseHeader("Biz-Date")) {
                    if ("" != CommonUtil.serverData.bizDate && typeof (XMLHttpRequest.getResponseHeader("Biz-Date")) != "undefined") {
                        if (CommonUtil.serverData.bizDate != XMLHttpRequest.getResponseHeader("Biz-Date")) {
                            mini.confirm("系统日期发生变化，请刷新页面或重新登陆", '系统提示', function (r) {
                                if (r) {
                                    if (window.top == window.self) {
                                        window.location.reload();
                                    } else {
                                        window.top.location.reload();
                                    }
                                }
                            });
                        }
                    }
                    CommonUtil.serverData.bizDate = XMLHttpRequest.getResponseHeader("Biz-Date");
                }
                if (param.relbtn) {
                    CommonUtil.cancelBtnLoading(param.relbtn);
                }
                if (param.callComplete) {
                    param.callComplete(XMLHttpRequest);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                mini.hideMessageBox(param.messageId);

                mini.alert("status: " + XMLHttpRequest.status + "<br/> statusText: " + XMLHttpRequest.statusText + "<br/> Info:" + $(XMLHttpRequest.responseText).text(), "error");
            },
            success: function (data) {
                mini.hideMessageBox(param.messageId);

                if (data.code && data.code != "error.common.0000") {
                    if (data.code == 'error.system.0001') {
                        mini.alert(data.desc, "info", function () {
                            if (window.top == window.self) {
                                window.location.href = pPath;
                            } else {
                                window.top.location.href = pPath;
                            }
                        });
                    } else {
                        mini.alert(data.desc, "info");
                    }
                    //增加异常时，回调操作
                    if (param.callerror) {
                        param.callerror(data);
                    }
                } else {
                    if (param.callback) {
                        param.callback(data);
                    }
                }
            }
        };
        // 当传入data为json对象时，将其转为字符串格式
        if (typeof param.data != "string" && typeof param.data != "undefined") {
            param.data = JSON.stringify(param.data);//$.toJSON(param.data);
        }

        //采用完整路径
        param.url = pPath + "/sl" + param.url;
        defautlParam = $.extend({data: {}}, defautlParam, param);
        $.ajax(defautlParam);
    },

    ajax2: function (param) {
        //默认显示加载进度条
        param.showLoadingMsg = CommonUtil.isNull(param.showLoadingMsg) ? true : param.showLoadingMsg;
        if (CommonUtil.isNull(param.messageId) && param.showLoadingMsg) {
            if (param.loadingMsg) {
                param.messageId = mini.loading(param.loadingMsg, "请稍后");
            } else {
                param.messageId = mini.loading("正在处理...", "请稍后");
            }
        }


        var defautlParam = {
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            cache: false,
            beforeSend: function (XMLHttpRequest) {
                XMLHttpRequest.setRequestHeader("__module_id", CommonUtil.pageModuleIdForPage);
                if (param.relbtn) {
                    CommonUtil.setBtnLoading(param.relbtn);
                }
            },
            complete: function (XMLHttpRequest) {
                if (XMLHttpRequest.getResponseHeader("Biz-Date")) {
                    if ("" != CommonUtil.serverData.bizDate && typeof (XMLHttpRequest.getResponseHeader("Biz-Date")) != "undefined") {
                        if (CommonUtil.serverData.bizDate != XMLHttpRequest.getResponseHeader("Biz-Date")) {
                            mini.confirm('系统提示', "系统日期发生变化，请刷新页面或重新登陆", function (r) {
                                if (r) {
                                    if (window.top == window.self) {
                                        window.location.reload();
                                    } else {
                                        window.top.location.reload();
                                    }
                                }
                            });
                        }
                    }
                    CommonUtil.serverData.bizDate = XMLHttpRequest.getResponseHeader("Biz-Date");
                }
                if (param.relbtn) {
                    CommonUtil.cancelBtnLoading(param.relbtn);
                }
                if (param.callComplete) {
                    param.callComplete(XMLHttpRequest);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                mini.hideMessageBox(param.messageId);

                mini.alert("status: " + XMLHttpRequest.status + "<br/> statusText: " + XMLHttpRequest.statusText + "<br/> Info:" + $(XMLHttpRequest.responseText).text(), "error");
            },
            success: function (data) {
                mini.hideMessageBox(param.messageId);

                if (data.code && data.code != "error.common.0000") {
                    if (data.code == 'error.system.0001') {
                        mini.alert(data.desc, "info", function () {
                            if (window.top == window.self) {
                                window.location.href = pPath + "/standard";
                            } else {
                                window.top.location.href = pPath + "/standard";
                            }
                        });
                    } else {
                        mini.alert(data.desc, "info");
                    }
                    //增加异常时，回调操作
                    if (param.callerror) {
                        param.callerror(data);
                    }
                } else {
                    if (param.callback) {
                        param.callback(data);
                    }
                }
            }
        };
        // 当传入data为json对象时，将其转为字符串格式
        if (typeof param.data != "string" && typeof param.data != "undefined") {
            param.data = JSON.stringify(param.data);//$.toJSON(param.data);
        }
        param.url = pPath + "/" + param.url;
        defautlParam = $.extend({data: {}}, defautlParam, param);
        $.ajax(defautlParam);
    },

    /**
     * 设置按钮为加载中
     * @param  {_id} 按钮id
     * @return {Void}
     @example 示例：
     */
    setBtnLoading: function (_id) {
        if ($("#" + _id).length == 0) return false;
        var _zindex = 999;
        if ($("#" + _id).css('z-index')) {
            _zindex = $("#" + _id).css('z-index') + 1;
        }
        var _width = $("#" + _id).outerWidth();
        var _height = $("#" + _id).outerHeight();
        var _imgHeight = _height < _width ? _height : _width;
        var _img = $("<img>");
        _img.css("width", _imgHeight);
        _img.css("height", _imgHeight);
        _img.attr("src", "./css/images/loading.gif");
        var _loading_mask = $("<span id='mask_4_" + _id + "'></span>");
        //_loading_mask.addAttr("id","mask_4_"+id);
        _loading_mask.append(_img);
        $("body").append(_loading_mask);
        _loading_mask.css("background-color", "#DDDDDD");
        _loading_mask.css("opacity", "0.5");
        _loading_mask.css("text-align", "center");
        _loading_mask.css("position", "absolute");
        _loading_mask.css("z-index", _zindex);
        _loading_mask.css("top", $("#" + _id).offset().top + "px");
        _loading_mask.css("left", $("#" + _id).offset().left + "px");
        _loading_mask.css("width", _width + "px");
        _loading_mask.css("height", _height + "px");
        _img.css("margin-left", (_width / 2 - 18) + "px");
    },
    /**
     * 取消按钮loading
     * @param  {_id} 按钮id
     * @return {Void}
     @example 示例：
     */
    cancelBtnLoading: function (_id) {
        $("#mask_4_" + _id).remove();
    },
    /**
     *判断对象是否为null
     * @param  {obj}  判断的对象
     * @return {boolean}    True/False
     @example 示例：
     */
    isNull: function (obj) {
        if (obj == null || typeof (obj) == 'undefind' || obj.toString() == "" || obj.toString() == "NaN") {
            return true;
        }
        return false;
    },
    /**
     *判断对象是否为null
     * @param  {obj}  判断的对象
     * @return {boolean}    True/False
     @example 示例：
     */
    isNotNull: function (obj) {
        return !this.isNull(obj);
    },

    /**
     *根据URL取对应参数
     * @param  {paramName}  参数名称
     * @param  {url}  指定url字符串，为空则取地址栏参数
     * @return {String}
     @example 示例：
     */
    getUrlParam: function (paramName, url) {
        if (!url) {
            url = window.location.href;
        }
        url = url.substring(url.indexOf("?"));
        var reg = new RegExp("(^|&)" + paramName + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
        var r = url.substr(1).match(reg);  //匹配目标参数
        if (r != null) return unescape(r[2]);
        return ""; //返回参数值
    },


    /**
     * 金额千分位格式化
     */
    moneyFormat: function (val, pos) {
        if (isNaN(val) || CommonUtil.isNull(val)) {
            return val;
        }
        //如果存在个格式化的小数位时，先将数字格式化后再进行千分位格式化
        if (pos) {
            val = CommonUtil.fomatFloat(val, pos);
        } else {
            val = parseFloat(val);
        }
        var s = val + "";
        if (s.indexOf(".") == -1) s += ".0";
        //如果没有小数点，在后面补个小数点和0
        if (/\.\d$/.test(s)) s += "0";
        //正则判断
        while (/\d{4}(\.|,)/.test(s)) {
            //符合条件则进行替换
            s = s.replace(/(\d)(\d{3}(\.|,))/, "$1,$2");
        }
        //每隔3位添加一个，
        return s;
    },
    /**
     * 去除千分位
     *@param{Object}num
     */
    moneyRevert: function (num) {
        if ($.trim(num + "") == "") {
            return "";
        }
        num = num.toString().replace(/,/gi, '');
        return parseFloat(num);
    },

    /**
     * 小数到百分比0.015 => 1.5
     */
    rateToPercent: function (rate, precision) {
        if (this.isNull(precision)) precision = 2;
        if (rate == 0) {
            var a = "0.";
            for (var i = 0; i < precision; i++) {
                a += "0";
            }
            return a;
        }
        if (rate == "") return "";
        if (isNaN(rate)) return rate;
        return parseFloat(rate).mul(100).toFixed(precision);
    },

    /**
     * 百分比还原 1.5 => 0.015
     */
    rateReset: function (rate) {
        if (rate == "") return "";
        if (isNaN(rate)) return rate;
        return parseFloat(rate).mul(0.01);
    },

    /**
     * 数字转中文
     */
    chineseNumber: function (n) {
        n = CommonUtil.moneyRevert(n);
        if (isNaN(n) || n > Math.pow(10, 12)) return "";
        if (!/^(0|[1-9]\d*)(\.\d+)?$/.test(n))
            return "数据非法";
        var unit = "万仟佰拾亿仟佰拾万仟佰拾元角分", str = "";
        n += "00";
        var p = n.indexOf('.');
        if (p >= 0)
            n = n.substring(0, p) + n.substr(p + 1, 2);
        unit = unit.substr(unit.length - n.length);
        for (var i = 0; i < n.length; i++)
            str += '零壹贰叁肆伍陆柒捌玖'.charAt(n.charAt(i)) + unit.charAt(i);
        return str.replace(/零(仟|佰|拾|角)/g, "零").replace(/(零)+/g, "零").replace(/零(万|亿|元)/g, "$1").replace(/(亿)万|壹(拾)/g, "$1$2").replace(/^元零?|零分/g, "").replace(/元$/g, "元整");
    },

    /**
     * 小数转换
     */
    parseFloat: function (num) {
        var nm = CommonUtil.moneyRevert(num);
        return parseFloat(nm);
    },

    /**
     * 小数位格式化
     */
    fomatFloat: function (src, pos) {
        var float = Math.round(src * Math.pow(10, pos)) / Math.pow(10, pos);
        return float.toFixed(pos);
    },
    /**
     * js除法运算
     * @param arg1
     * @param arg2
     * @returns {Number}
     */
    accDiv: function (arg1, arg2) {
        var t1 = 0, t2 = 0, r1, r2;
        try {
            t1 = arg1.toString().split(".")[1].length;
        } catch (e) {
        }
        try {
            t2 = arg2.toString().split(".")[1].length;
        } catch (e) {
        }
        with (Math) {
            r1 = Number(arg1.toString().replace(".", ""));
            r2 = Number(arg2.toString().replace(".", ""));
            var val = (r1 / r2) * pow(10, t2 - t1);
            val = CommonUtil.fomatFloat(val, 8);
            return parseFloat(val);
        }
    },

    /**
     * js乘法运算
     * @param arg1
     * @param arg2
     * @returns {Number}
     */
    accMul: function (arg1, arg2) {
        var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
        try {
            m += s1.split(".")[1].length;
        } catch (e) {
        }
        try {
            m += s2.split(".")[1].length;
        } catch (e) {
        }
        return parseFloat(Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m));
    },

    /**
     * js加法运算
     * @param arg1
     * @param arg2
     * @returns {Number}
     */
    accAdd: function (arg1, arg2) {
        var r1, r2, m;
        try {
            r1 = arg1.toString().split(".")[1].length;
        } catch (e) {
            r1 = 0;
        }
        ;
        try {
            r2 = arg2.toString().split(".")[1].length;
        } catch (e) {
            r2 = 0;
        }
        ;
        m = Math.pow(10, Math.max(r1, r2));
        return parseFloat((arg1 * m + arg2 * m) / m);
    },

    /**
     * js减法运算
     * @param arg1
     * @param arg2
     * @returns {Number}
     */
    accSubtr: function (arg1, arg2) {
        var r1, r2, m, n;
        try {
            r1 = arg1.toString().split(".")[1].length;
        } catch (e) {
            r1 = 0;
        }
        try {
            r2 = arg2.toString().split(".")[1].length;
        } catch (e) {
            r2 = 0;
        }
        m = Math.pow(10, Math.max(r1, r2));
        //last modify by deeka
        //动态控制精度长度
        n = (r1 >= r2) ? r1 : r2;
        return parseFloat(((arg1 * m - arg2 * m) / m).toFixed(n));
    },


    /*
    判断字符类型
    */
    CharMode: function (iN) {
        if (iN >= 48 && iN <= 57) //数字
            return 1;
        if (iN >= 65 && iN <= 90) //大写字母
            return 2;
        if (iN >= 97 && iN <= 122) //小写
            return 2;
        if (iN == 95) //下划线
            return 4;
        else
            return 0; //其他字符
    },

    //统计字符类型
    bitTotal: function (num) {
        modes = 0;
        for (var i = 0; i < 4; i++) {
            if (num & 1) modes++;
            num >>>= 1;
        }
        return modes;
    },

    /**
     *计算天数差的函数，通用
     * @param  {sDate1}
     * @param  {sDate2}
     * @return {iDays}
     @example 示例：
     */
    dateDiff: function (sDate1, sDate2) { // sDate1和sDate2是2014-02-13格式
        var aDate, oDate1, oDate2, iDays;
        aDate = sDate1.split("-");
        oDate1 = new Date((aDate[1]) + '/' + aDate[2] + '/' + aDate[0]); // 转换为02-13-2014格式
        // 兼容IE浏览器
        if (oDate1.toString() === "Invalid Date" || typeof (oDate1) == "undefined") {
            var d12 = new Date();
            d12.setFullYear(aDate[0]);
            d12.setMonth(aDate[1] - 1);
            d12.setDate(parseInt(aDate[2])); // 这里必须为 number
            oDate1 = d12;
        }

        aDate = sDate2.split("-");
        oDate2 = new Date((aDate[1]) + '/' + aDate[2] + '/' + aDate[0]);

        if (oDate2.toString() === "Invalid Date" || typeof (oDate2) == "undefined") {
            var d12 = new Date();
            d12.setFullYear(aDate[0]);
            d12.setMonth(aDate[1] - 1);
            d12.setDate(parseInt(aDate[2])); // 这里必须为 number
            oDate2 = d12;
        }

        iDays = parseInt((oDate2 - oDate1) / 1000 / 60 / 60 / 24); // 把相差的毫秒数转换为天数
        return iDays;
    },
    dateCompare: function (sDate1, sDate2) { // sDate1和sDate2是2014-02-13格式
        var aDate, oDate1, oDate2, iDays;
        aDate = sDate1.split("-");
        oDate1 = new Date((aDate[1]) + '/' + aDate[2] + '/' + aDate[0]); // 转换为02-13-2014格式

        // 兼容IE浏览器
        if (oDate1.toString() === "Invalid Date" || typeof (oDate1) == "undefined") {
            var d12 = new Date();
            d12.setFullYear(aDate[0]);
            d12.setMonth(aDate[1] - 1);
            d12.setDate(parseInt(aDate[2])); // 这里必须为 number
            oDate1 = d12;
        }

        aDate = sDate2.split("-");
        oDate2 = new Date((aDate[1]) + '/' + aDate[2] + '/' + aDate[0]);

        if (oDate2.toString() === "Invalid Date" || typeof (oDate2) == "undefined") {
            var d12 = new Date();
            d12.setFullYear(aDate[0]);
            d12.setMonth(aDate[1] - 1);
            d12.setDate(parseInt(aDate[2])); // 这里必须为 number
            oDate2 = d12;
        }
        if (oDate1 >= oDate2) {
            return true;
        }
        return false;
    },

    isComboContainsValue: function (value, fieldname, data) {
        if (CommonUtil.isNull(data) || CommonUtil.isNull(value))
            return false;
        for (var i = 0; i < data.length; i++) {
            if (data[i][fieldname] == value) {
                return true;
            }
        }
        return false;
    },

    isDate: function (val) {
        if (!val || val.length != 10) {
            return false;
        }
        var a = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})/;
        return a.test(val);
    },
    moneyToChineseNumber: function (jqObjOrId) {

        var jqObj = null;
        if ($.type(jqObjOrId) == "string") {
            if (jqObjOrId.indexOf('#') == 0) {
                jqObj = $(jqObjOrId);
            } else {
                jqObj = $('#' + jqObjOrId);
            }
        } else if ($.type(jqObjOrId) == "object") {
            jqObj = jqObjOrId;
        }
        if (jqObj.length == 1) {
            jqObj.next("span").find("input[type='text']").on("keyup", function () {
                jqObj.parent().find(".money").remove();
                var maxValue = jqObjOrId.numberbox("options").max;
                var currenValue = $(this).val();
                var n = CommonUtil.moneyRevert(currenValue);
                if (n > maxValue) {
                    return false;
                }
                //本金转大写
                jqObj.parent().append("<span class='money'>&nbsp;" + CommonUtil.chineseNumber($(this).val()) + "</span>");
            });
        }
    },
    /*****
     * 组件点击事件
     * type = 1 根据组件的dict属性加载组件字典项
     */
    onclickOption: function onclickOption(param) {
        if (!param || !param.type) {
            return;
        }
        if (param.type == '1') {
            if (param.obj && param.obj.dictStatus && (param.obj.dictStatus == 'init')) {
                var val = param.obj.getValue();
                var data = CommonUtil.serverData.dictionary[param.obj.dict];
                param.obj.setData(data);
                param.obj.setValue(val);
                param.obj.dictStatus = 'load';
            }
        }
    },
    dictRenderer: function dictRenderer(e) {
        var dict = e.column.dict;
        if (CommonUtil.isNotNull(dict)) {
            var data = CommonUtil.serverData.dictionary[dict];
            if (CommonUtil.isNotNull(data)) {
                for (var i = 0, l = data.length; i < l; i++) {
                    var g = data[i];
                    if (g.id == e.value) return g.text;
                }
            }
        }
        return "";
    },
    getParam: function getParam(url, paramName) {
        var reg = new RegExp("(^|&)" + paramName + "=([^&]*)(&|$)");
        var r = url.substr(1).match(reg);
        if (r != null)
            return decodeURI(r[2]);   //对参数进行decodeURI解码
        return null;
    },
    pPath: pPath,
    baseWebPath: function () {
        switch (branchModule) {
            case '01':
                return pPath + "/standard/InterBank";
                break;
            case '02':
                return pPath + "/standard/Ifs";
                break;
            case '03':
                return pPath + "/standard/Derivative";
                break;
            case '04':
                return pPath + "/standard/";
                break;
            default:
                return pPath + "/standard/";
                break;
        }
    },
    stringFormat: function stringFormat(value, param) {
        if (CommonUtil.isNull(param) || param.length == 0) {
            return value;
        }

        for (var i = 0; i < param.length; i++) {
            var re = new RegExp('\\{' + i + '\\}', 'gm');
            value = value.replace(re, param[i]);
        }
        return value;
    },

    validations: {
        regularExpression: /[^\u0000-\u00ff]|[(\u000d\u000a)]|[(\u00c2\u00b7)]/g,

        fixLength: {
            validator: function (value, param) {
                param[1] = $.trim(value).replace(CommonUtil.validations.regularExpression, "aa").length;
                return param[1] == param[0];
            },
            message: '必须输入{0}字符,实际长度为{1}.'
        },
        length: {
            validator: function (value, param) {
                param[2] = $.trim(value).replace(CommonUtil.validations.regularExpression, "aa").length;
                return param[2] >= param[0] && param[2] <= param[1];
            },
            message: "输入内容长度必须介于{0}和{1}之间,实际长度为{2}."
        },
        minLength: {
            validator: function (value, param) {
                param[1] = $.trim(value).replace(CommonUtil.validations.regularExpression, "aa").length;
                return param[1] >= param[0];
            },
            message: '至少输入{0}字符,实际长度为{1}.'
        },
        maxLength: {
            validator: function (value, param) {
                param[1] = $.trim(value).replace(CommonUtil.validations.regularExpression, "aa").length;
                return param[1] <= param[0];
            },
            message: '最多输入{0}字符,实际长度为{1}.'
        },
        eqPwd: {
            validator: function (value, param) {
                return value == $(param[0]).val();
            },
            message: '确认密码和新密码不一致.'
        },
        uneqPwd: {
            validator: function (value, param) {
                return value != $(param[0]).val();
            },
            message: '新密码和原密码一致.'
        },
        unnormal: {
            validator: function (value) {
                return /.+/i.test(value);
            },
            message: '输入值不能为空和包含其他非法字符.'
        },
        alpha: {
            validator: function (value, param) {
                if (value) {
                    return /^[a-zA-Z\u00A1-\uFFFF]*$/.test(value);
                } else {
                    return true;
                }
            },
            message: '只能输入字母.'
        },
        alphanum: {
            validator: function (value, param) {
                if (value) {
                    return /^([a-zA-Z0-9])*$/.test(value);
                } else {
                    return true;
                }
            },
            message: '只能输入字母和数字.'
        },
        positiveInt: {
            validator: function (value, param) {
                if (value) {
                    return /^[0-9]*[1-9][0-9]*$/.test(value);
                } else {
                    return true;
                }
            },
            message: '只能输入正整数.'
        },
        chsNoSymbol: {
            validator: function (value, param) {
                return /^[\u4E00-\u9FA5]+$/.test(value);
            },
            message: '只能输入中文.'
        },
        noChs: {
            validator: function (value, param) {
                return /^[^\u4e00-\u9fa5]{0,}$/.test(value);
            },
            message: '不能输入中文.'
        },
        chs: {
            validator: function (value, param) {
                return /^[\u0391-\uFFE5]+$/.test(value);
            },
            message: '请输入汉字.'
        },
        zip: {
            validator: function (value, param) {
                return /^[1-9]\d{5}$/.test(value);
            },
            message: '邮政编码不存在.'
        },
        faxno: {
            validator: function (value) {
                return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value);
            },
            message: '传真号码不正确.'
        },
        qq: {
            validator: function (value, param) {
                return /^[1-9]\d{4,10}$/.test(value);
            },
            message: 'QQ号码不正确.'
        },
        phone: {
            validator: function (value) {
                return /^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/i.test(value);
            },
            message: '电话号码不正确，请使用下面格式:020-88888888.'
        },
        mobile: {
            validator: function (value, param) {
                return /^((\(\d{2,3}\))|(\d{3}\-))?1\d{10}$/.test(value);
            },
            message: '手机号码不正确.'
        },
        ip: {
            validator: function (value) {
                return /d+.d+.d+.d+/i.test(value);
            },
            message: 'IP地址格式不正确.'
        },
        age: {
            validator: function (value) {
                return /^(?:[1-9][0-9]?|1[01][0-9]|120)$/i.test(value);
            },
            message: '年龄必须是0到120之间的整数.'
        },
        number: {
            validator: function (value, param) {
                return /^\d+$/.test(value);
            },
            message: '请输入数字.'
        },
        numberRange: {
            validator: function (value, param) {
                return parseFloat(value) >= parseFloat(param[0]) && parseFloat(value) <= parseFloat(param[1]);
            },
            message: '请输入从{0}到{1}之间的数字.'
        },
        idcard: {
            validator: function (value) {
                return /^\d{15}(\d{2}[A-Za-z0-9])?$/i.test(value);
            },
            message: '身份证号码格式不正确.'
        },
        stockId: {
            validator: function (value) {
                return /^([a-zA-Z0-9.])*$/.test(value);
            },
            message: '只能输入字母数字和小数点.'
        }
    },
    onValidation: function onValidation(e, type, param) {
        if (!e.sender) {
            return;
        }
        if (CommonUtil.isNotNull(e.sender.required) && CommonUtil.isNull(e.value)) {
            if (e.sender.required) {
                e.isValid = false;
                return;
            } else {
                e.isValid = true;
                return;
            }
        }
        var valid = CommonUtil.validations[type];
        if (CommonUtil.isNotNull(valid)) {
            if (valid.validator(e.value, param) == false) {
                e.isValid = false;
                e.errorText = CommonUtil.stringFormat(valid.message, param);
            } else {
                e.isValid = true;
            }
        }
    },
    openNewMenuTab: function openNewTab(tab, params) {
        var tab = {
            id: tab.id,
            name: tab.name,
            iconCls: tab.iconCls,
            title: tab.title,
            url: tab.url,
            showCloseButton: tab.showCloseButton,
            parentId: tab.parentId
        };
        top["win"].openNewTab(tab, params);
    },
    getUrlParms: function getUrlParms(params, url) {
        var index = url.indexOf("?");
        if (index != -1) {
            var query = url.substr(index + 1);//获取查询串
            var pairs = query.split("&");//在逗号处断开
            for (var i = 0; i < pairs.length; i++) {
                var pos = pairs[i].indexOf('=');//查找name=value
                if (pos == -1) continue;//如果没有找到就跳过
                var argname = pairs[i].substring(0, pos);//提取name
                var value = pairs[i].substring(pos + 1);//提取value
                params[argname] = decodeURI(value);//对参数进行decodeURI解码
            }
        }
    },
    closeMenuTab: function closeMenuTab() {
        top["win"].closeMenuTab();
    },
    /**
     *获取字典项列表，用于下拉框组件
     * @param  {dictId}  字典ID
     * @param  {folder}  folder 可为空
     * @return {String}
     @example 示例：
     */
    getDictList: function (dictId, folder) {
        var dataArray = new Array();
        var datas = CommonUtil.serverData.dictionary[dictId];
        for (var key = 0; key < datas.length; key++) {
            if (CommonUtil.isNull(folder) || $.inArray(folder, CommonUtil.serverData.dictionary[dictId][key]["folder"]) > -1) {
                dataArray.push({"id": datas[key].id, "value": datas[key].id, "text": datas[key].text});
            }
        }
        return dataArray;
    },
    /**
     * 主要用于任何mini-tab，打开自己的tab
     * modify by shenzl
     * @param tabs 主tabs(mini-tab组件)
     * @param tab 需要现在tabs上的tab
     * @param params 参数
     *根据tabs打开可以传递参数的tab
     **/
    openOwnTab: function openTab(tabs, tab, params) {
        if (!tabs) {
            return;
        }
        if (CommonUtil.isNull(tabs.getTab(tab.name))) {
            tab.params = params;
            tabs.addTab(tab);
        }
        tabs.activeTab(tabs.getTab(tab.name));
    },
    //判断IE版本
    isIE: function (ver) {
        var b = document.createElement('b');
        b.innerHTML = '<!--[if IE ' + ver + ']><i></i><![endif]-->';
        return b.getElementsByTagName('i').length === 1;
    },
    //个位数字格式转换   1->01
    singleNumberFormatter: function (single) {
        return parseInt(single) < 10 ? "0" + single : single;
    },

    //时间戳转换成yyyy-mm-dd hh24:mi:ss
    stampToTime: function (stamp) {
        if (!stamp) return null;
        var time = new Date(stamp);
        var result = "";
        result += time.getFullYear() + "-";
        result += CommonUtil.singleNumberFormatter(time.getMonth() + 1) + "-";
        result += CommonUtil.singleNumberFormatter(time.getDate()) + " ";
        result += CommonUtil.singleNumberFormatter(time.getHours()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getMinutes()) + ":";
        result += CommonUtil.singleNumberFormatter(time.getSeconds());
        return result;
    },

    //时间戳转换成yyyy-mm-dd
    stampToDate: function (stamp) {
        if (!stamp) return null;
        var time = null;
        if (stamp instanceof Date) {
            time = stamp;
        } else {
            if (stamp.length > 10) {
                stamp = stamp.substr(0, 10);
            }
            var aDate = stamp.split("-");
            var time = new Date((aDate[1]) + '/' + aDate[2] + '/' + aDate[0]); // 转换为02-13-2014格式
            // 兼容IE浏览器
            if (time.toString() === "Invalid Date" || typeof (time) == "undefined") {
                var d12 = new Date();
                d12.setFullYear(aDate[0]);
                d12.setMonth(aDate[1] - 1);
                d12.setDate(parseInt(aDate[2])); // 这里必须为 number
                time = d12;
            }
        }

        var result = "";
        result += time.getFullYear() + "-";
        result += CommonUtil.singleNumberFormatter(time.getMonth() + 1) + "-";
        result += CommonUtil.singleNumberFormatter(time.getDate());
        return result;
    },

    //根据参数秒转化为X天X小时X分X秒
    secondFormatter: function (second_time) {
        var time = parseInt(second_time) + "秒";
        if (parseInt(second_time) > 60) {
            var second = parseInt(second_time) % 60;
            var min = parseInt(second_time / 60);
            time = min + "分" + second + "秒";
            if (min > 60) {
                min = parseInt(second_time / 60) % 60;
                var hour = parseInt(parseInt(second_time / 60) / 60);
                time = hour + "小时" + min + "分" + second + "秒";
                if (hour > 24) {
                    hour = parseInt(parseInt(second_time / 60) / 60) % 24;
                    var day = parseInt(parseInt(parseInt(second_time / 60) / 60) / 24);
                    time = day + "天" + hour + "小时" + min + "分" + second + "秒";
                }
            }
        }
        return time;
    },

    //获取机构列表
    getInstList: function () {
        var btnEdit = this;
        mini.open({
            url: CommonUtil.baseWebPath + "/mini_system/MiniInstitutionSelectManages.jsp",
            title: "机构选择",
            width: 700,
            height: 600,
            ondestroy: function (action) {
                if (action == "ok") {
                    var iframe = this.getIFrameEl();
                    var data = iframe.contentWindow.GetData();
                    data = mini.clone(data); //必须
                    if (data) {
                        btnEdit.setValue(data.instId);
                        btnEdit.setText(data.instName);
                        btnEdit.focus();
                    }
                }
            }
        });
    },
    //激活tab
    activeTab: function () {
        var activeTab = top["win"].tabs.getActiveTab();
        if (activeTab.parentId && top['win'].tabs.getTab(activeTab.parentId))
            top['win'].tabs.activeTab(activeTab.parentId);
    },
    /***
     * 行业类型数据字典
     * id : 控件id
     * key : 行业类型父节点代码
     */
    loadPartyKindDict: function (id, key) {
        if (CommonUtil.isNull(CommonUtil.serverData.partyKindDict.data)) {
            CommonUtil.ajax({
                url: "/CounterPartyController/getAllCounterPartyKind",
                showLoadingMsg: false,
                data: {},
                callback: function (data) {
                    CommonUtil.serverData.partyKindDict.data = data.obj;
                    if (CommonUtil.isNotNull(mini.get(id)) && key != null) {
                        mini.get(id).setData(CommonUtil.serverData.partyKindDict.data[key]);
                    }
                }
            });
        } else {
            if (CommonUtil.isNotNull(mini.get(id)) && key != null) {
                mini.get(id).setData(CommonUtil.serverData.partyKindDict.data[key]);
            }
        }
    }, hideBtn: function (btnId) {
        var defer = $.Deferred();
        // 控制按钮权限
        CommonUtil.ajax({
            url: "/RoleController/getFunctionByMid",
            data: {moduleId: btnId, branchId: branchId, roleIds: roleIds},
            callback: function (data) {
                //获取所有mini-button控件
                var controls = mini.findControls(function (control) {
                    var id = new RegExp('^mini');
                    if (control.type == "button" && !id.test(control.id)) return true;
                });
                var btns = {};
                for (var i = 0; i < controls.length; i++) {
                    if (CommonUtil.isNotNull(controls[i].id)) {
                        if ($.inArray(controls[i].id, data.obj) >= 0) {
                            btns[controls[i].id] = true;
                            mini.get(controls[i].id).setVisible(true);
                        } else {
                            mini.get(controls[i].id).setVisible(false);
                        }
                    }
                }
                defer.resolve(btns);
            }
        });
        return defer.promise();
    },
    //链接websocket
    connectWs: function connectWs(url) {
        //不管是否登录
        let socket = new SockJS(url);//创建SockJs链接,可以相对路径
        let stompClient = Stomp.over(socket);//创建TOMP客户端,一层一层封装
        stompClient.heartbeat.outgoing = 30 * 1000;
        stompClient.heartbeat.ingoing = 0;
        stompClient.connect({}, function (frame) {//连接STOPM断点
            //订阅地址/topic/response
            stompClient.subscribe('/topic/response', function (message) {
                //调用toast消息框
                CommonUtil.addToastMessage("新消息", JSON.parse(message.body).content, "success", 30 * 1000 , function () {
                    if (JSON.parse(message.body).call !=''){
                        window.location.href = JSON.parse(message.body).call;
                    }
                });
            });
            //订阅地址/user/' + userId + '/message
            stompClient.subscribe('/user/' + userId + '/message', function (message) {
                //调用toast消息框
                CommonUtil.addToastMessage("新消息", JSON.parse(message.body).content, "success", 30 * 1000, function () {
                    if (JSON.parse(message.body).call !=''){
                        window.location.href = JSON.parse(message.body).call;
                    }
                });
            });
        });
    },
    //  * 添加推送消息模板
    //  * @title 消息标题
    //  * @message 消息内容
    //  * @type 消息类型
    //  * @timeout 消息停留时间ns
    //  */
    addToastMessage: function (title, message, type, timeout, callback) {
        $.Toast(title, message, type, {
            stack: true,
            has_icon: true,
            has_close_btn: true,
            fullscreen: false,
            timeout: timeout,
            sticky: false,
            has_progress: true,
            rtl: false,
        });
        setTimeout(callback, timeout);
    }
};
/////////////////////////////////////////////////////////////////
 




