/*
 *	 审批相关操作
 *  @auth : libo 
 */
var Approve = {
	config:{
		supportType:[
			//"decision",		//判断节点
			//"fork",			//分支节点
			//"skip",			//审批任意跳转
			""
		]
	}
};


/**  审批单状态  1:就绪	2:计算中	3:新建	4:待审批	5:审批中	6:审批通过	7:审批拒绝	8:注销	9:执行中	10:执行完成	*/
Approve.OrderStatus={Ready:'1',Calculating:'2',New:'3',WaitApprove:'4',Approving:'5',ApprovedPass:'6',ApprovedNoPass:'7',Cancle:'8',Executing:'9',Executed:'10',Verifying:'11',Verified:'12'};

/**  审批流程类型  1:业务审批流程  2：风险预审  3：预授权审批流程 4:核实流程 5：结算流程 6:额度申请流程  7:清算路径流程  8:开户销户流程  9:投后报告流程
 * 				  20:额度切分  22:额度调整   23:额度冻结   24:额度占用 35:手工调帐   36:年终结算  99:交易冲销
 * 				30:基金申购审批  31:基金申购确认审批  32:基金赎回审批 33:基金赎回确认审批 34:基金分红审批  40:事前审批
*/
Approve.FlowType={BusinessApproveFlow:'1',RiskApproveFlow:'2',PreAuthApproveFlow:'3',VerifyApproveFlow:'4',SettleApproveFlow:'5',QuotaApproveFlow:'6',PathApproveFlow:'7',AccountApproveFlow:'8'
					,investFollowupFlow:'9',QuotaSegFlow:'20',tdCustAtFlow:'21',QuotaAdjFlow:'22',QuotaFrozenFlow:'23',QuotaUsedFlow:'24',FiveType:'32',ManualAdjFlow:'35',YeanEndFlow:'36',
					rdpApproveFLow:'11',rdpVerifyFLow:'44',rddApproveFlow:'12',rdiApproveFLow:'13',RevApproveFlow:'99',FundAfpFlow:'30',FundAfpConfFlow:'31',FundRdpFlow:'32',FundRdpConfFlow:'33',FundReddinFlow:'34',preApprovse:'40'};

/**  审批操作类型  0:通过  1:同意继续   2:跳转    -1:不通过   -2:(Back)驳回    99：(Redo)退回发起  */
Approve.OperatorType={Pass:'0',NoPass:'-1',Continue:'1',Back:'-2',Redo:'99',Skip:'2'};

/**  结算状态 0:未处理 1：待结算 2：结算中 3：等待付款 4：等待收款 5：核算完成 6：结算完成 7：暂停 8：已撤销 9：结算失败 13：待发起 14：待复核 15：待授权 **/
Approve.SettleInstState = {NEW:'0',WAITSETTLE:'1',SETTLING:'2',WAITPAY:'3',WAITREC:'4',ACCOUNTINGDONE:'5',SETTLECOMPLETE:'6',PAUSE:'7',CANCEL:'8',FAILED:'9',WAITSTART:'13',WAITVERIFY:'14',WAITAHTH:'15'};

/** 扩展虚拟产品  **/
Approve.ProductExt = {FiveType:'9013'};



/*
 *	ifs 打印模板编号
 *  @auth : lijing 
 */
var PrintNo = {};
/**打印模板编号  SLAPPROVE:债券借贷事前审批
 * REPOPRV 质押式回购
 * REPOBRV 买断式回购
 * BONDCBT 现券买卖
 * BONDSL 债券借贷
 * IBOFX 外币拆借
 * IBORMB 信用拆借
 * IBD 同业存款
 * bondFwd 债券远期
*/
PrintNo = {bondLendingApprove:'SLAPPROVE',
	IRS:"IRSRMB",
	CFUND:"CFUND",
	CFUNDSHARE:"CFUNDSHARE",
	CFUNDRDP:"CFUNDRDP",
	RDPSHARE:"RDPSHARE",
	CFUNDREDD:"CFUNDREDD",
	CFUNDREIN:"CFUNDREIN",
	ccyLending:"IBOFX",
	creditTYLoan:"TYIBOFX",
	creditLoan:"IBORMB",
	deposit:"IBD",
	bondTrading:"BONDCBT",
	bondLending:"BONDSL",
	depositPublish:"SDIS",
	SPT:"SPT",
	FWD:"FWD",
	SWAP:"SWAP",
	bondFwd:'bondFwd',
	cdPublish:"CDIS",
	MBS:"BONDMBS",
	pledgeRepo:"REPOPRV",
	repo:"REPOBRV",
	nationalDeposit:"REPOGD",
	exchangeRepo:"REPOJY",
	standingLending:"REPOCB",
	MediumLending:"REPOZQ",
	MicroLending:"REPOZD",
	ccyDepositIn:"depIFX",
	DVP:"DVP"
};

/**
 *	提交审批
 * @param 	flow_type		 流程类型
 * @param 	serial_no		 外部序号（审批单号）
 * @param 	order_status    审批单状态
 * @param 	closedFun		close回调
 * @param 	closedFun		close回调
 * @param   callBackService 回调java方法（可为空）!该参数已作废,固定传null即可
 * @param   product_type    产品类型（可为空）
 * trd_type 操作类型，可以为空
 */
Approve.approveCommit = function(flow_type,serial_no,order_status,callBackFun,product_type,closedFun){
	var params = {};
	params.serial_no = serial_no;
	params.flow_type = flow_type;
	params.product_type = product_type;
	params.callBackFun = callBackFun;
	
	CommonUtil.ajax({
		url:"/IfsFlowController/approveLog",
		data:params,
		callback : function(data) {
			if(data.obj){
				//若有日志则说明该交易是退回发起的
				//从审批日志获取最新taskid
				var unfinishTask = new Array();
				$.each(data.obj, function(i, item){
					if(item.EndTime == "" || item.EndTime == null){
						unfinishTask.push(item);
					}
				});
				if(unfinishTask){
					var task_id =  unfinishTask[0].TaskId;
				}else{
					
				}
				//组装参数对象
				var defaultParam={
			    	task_id:task_id,
			    	specific_user:"",
			    	msg:"",
			    	op_type:Approve.OperatorType.Continue,
			    	back_to:"",
			    	success:closedFun
				};
				Approve.approveOperator(defaultParam);
			}else{
				//不能查到审批日志的新建状态则按一般步骤提交
				CommonUtil.ajax( {
					url:"/IfsFlowController/getAvailableFlowList",
					data:params,
					callback : function(data) {
						if(typeof(data) == "undefined" || data.length != 1){
							//mini.hideMessageBox(messageid);
							mini.alert("流程匹配失败.匹配到的审批流程有" + data.length +"笔","系统提示");
							return false;
						}
						params.flow_id = data[0].flow_id;
						CommonUtil.ajax( {
							url:"/IfsFlowController/approveCommit",
							data:params,
							callback : function(data) {
								mini.alert("提交成功","系统提示");
							},
							callerror: function(data){
							},
							callComplete:closedFun
						});
					}
				});
			}
		}
	});
}
/**
 *	批量提交审批
 * @param 	flow_type		 流程类型
 * @param 	serial_no		 外部序号（审批单号）
 * @param 	order_status    审批单状态
 * @param 	closedFun		close回调
 * @param 	closedFun		close回调
 * @param   callBackService 回调java方法（可为空）!该参数已作废,固定传null即可
 * @param   product_type    产品类型（可为空）
 * trd_type 操作类型，可以为空
 */
Approve.approveCommitAll = function(flow_type,serial_no,order_status,callBackFun,product_type,closedFun){
	var params = {};
	params.serial_no = serial_no;
	params.flow_type = flow_type;
	params.product_type = product_type;
	params.callBackFun = callBackFun;
	
	var messageid = mini.loading("正在提交审批", "请稍后");
	CommonUtil.ajax({
		url:"/IfsFlowController/approveLog",
		data:params,
		callback : function(data) {
			if(data.obj){
				//若有日志则说明该交易是退回发起的
				//从审批日志获取最新taskid
				var task_id = data.obj[data.obj.length -1].TaskId;
				//组装参数对象
				var defaultParam={
			    	task_id:task_id,
			    	specific_user:"",
			    	msg:"",
			    	op_type:Approve.OperatorType.Continue,
			    	back_to:"",
			    	success:closedFun
				};
				Approve.approveOperatorAll(defaultParam);
			}else{
				//不能查到审批日志的新建状态则按一般步骤提交
				CommonUtil.ajax( {
					url:"/IfsFlowController/getAvailableFlowList",
					data:params,
					callback : function(data) {
						if(typeof(data) == "undefined" || data.length != 1){
							mini.hideMessageBox(messageid);
							mini.alert("流程匹配失败.匹配到的审批流程有" + data.length +"笔","系统提示");
							return false;
						}
						params.flow_id = data[0].flow_id;
						CommonUtil.ajax( {
							url:"/IfsFlowController/approveCommit",
							data:params,
							callback : function(data) {
							},
							callerror: function(data){
							},
							callComplete:closedFun
						});
					}
				});
			}
		}
	});
	
	mini.hideMessageBox(messageid);
}

/**
 *	审批操作：通过、不通过、同意继续(单笔)
 *  @param  task_id    任务id
 *  @param  specific_user 指定用户
 *  @param  msg        意见信息
 *  @param  op_type    操作类型
 *	@param  back_to    回退的目标节点名称
 *  @param  success    操作成功回调
 */
Approve.approveOperator = function(opParam){
	var defaultParam={
		task_id:"",
		specific_user:"",
		msg:"",
		op_type:"",
		back_to:"",
		success:function(){}
	};
	var _opParam = $.extend({},defaultParam,opParam);
	var params = {};
	params.task_id = _opParam.task_id;
	params.msg = _opParam.msg;
	params.specific_user = _opParam.specific_user;
	params.op_type = ""+_opParam.op_type;
	params.back_to=_opParam.back_to;
	params.data = _opParam.data;
	params.serial_no = _opParam.serial_no;
	
	var messageid = mini.loading("正在提交审批", "请稍后");
	
	CommonUtil.ajax({
		url:"/IfsFlowController/approve",
		data:params, 
		callComplete:_opParam.success,
		callerror: function(data){
			mini.hideMessageBox(messageid);
		},
		callback : function(data) {
			mini.hideMessageBox(messageid);
			mini.alert("操作成功","系统提示",function(){
				_opParam.success(data);
			});
		}
	});
}


/**
 *	审批操作：通过、不通过、同意继续(批量)
 *  @param  task_id    任务id
 *  @param  specific_user 指定用户
 *  @param  msg        意见信息
 *  @param  op_type    操作类型
 *	@param  back_to    回退的目标节点名称
 *  @param  success    操作成功回调
 */
Approve.approveOperatorAll = function(opParam){
	var defaultParam={
		task_id:"",
		specific_user:"",
		msg:"",
		op_type:"",
		back_to:"",
		success:function(){}
	};
	var _opParam = $.extend({},defaultParam,opParam);
	var params = {};
	params.task_id = _opParam.task_id;
	params.msg = _opParam.msg;
	params.specific_user = _opParam.specific_user;
	params.op_type = ""+_opParam.op_type;
	params.back_to=_opParam.back_to;
	params.data = _opParam.data;
	
	
	var messageid = mini.loading("正在提交审批", "请稍后");
	
	CommonUtil.ajax({
		url:"/IfsFlowController/approve",
		data:params, 
		callComplete:_opParam.success,
		callerror: function(data){
			mini.hideMessageBox(messageid);
		},
		callback : function(data) {
			mini.hideMessageBox(messageid);
			/*mini.alert("操作成功","系统提示",function(){
				_opParam.success(data);
			});*/
		}
	});
}



/**
 *	查看审批日志
 * @param 	flow_type		 流程类型
 * @param 	serial_no		 外部序号（审批单号）
 * @param 	closedFun		 close回调
 */
Approve.approveLog = function(flow_type,serial_no,closedFun){
	var tab = {
		id:"flow_log_" + serial_no,
		name:"flow_log_" + serial_no,
		iconCls:null,
		title:"审批日志("+serial_no+")",
		url:CommonUtil.baseWebPath() + "/../Common/Flow/MiniApproveFlowLog.jsp",
		showCloseButton:true
	};
	var param ={
		isLogPage:true,
		task_id:'',
		flow_type:flow_type,
		serial_no:serial_no
	};
	CommonUtil.openNewMenuTab(tab,param);
}

/**
 *	跳转审批页面
 *  @param  taskId        任务id
 *  @param  prdNo         产品编号
 *  @param  defaultUrl    默认url
 */
Approve.goToApproveJsp = function(taskId, prdNo, defaultUrl, targetUrl){
	CommonUtil.ajax({
		url:'/FlowController/getUserTaskUrl',
		data:{"taskId":taskId,"prdCode":prdNo},
		async:false,
		callback:function(data){
			if(data.obj){
				targetUrl = data.obj.urls;
			}else{
				targetUrl = defaultUrl;
			}
			Approve.approvePage = targetUrl;
			return targetUrl;
		}
	});
}



function openFullScreen (url) {
    var name = arguments[1] ? arguments[1] : "_blank";
    var feature = "fullscreen=yes,channelmode=yes, titlebar=no, toolbar=no, scrollbars=no," +
         "resizable=yes, status=no, copyhistory=no, location=no, menubar=no,width="+(screen.availWidth) +
         "height="+(screen.availHeight);
     window.open(url, name, feature);
} 





























































