﻿--prompt
--prompt Creating sequence ACT_EVT_LOG_SEQ
--prompt =================================
--prompt
create sequence ACT_EVT_LOG_SEQ
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence ITS_TRAD_SEQ
--maxvalue 999999 不可修改!
--prompt ==============================
--prompt
create sequence ITS_TRAD_SEQ
minvalue 1
maxvalue 999999
start with 1
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence PRODUCT_WEIGHT_SQL
--prompt ====================================
--prompt
create sequence PRODUCT_WEIGHT_SQL
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
nocache;

--prompt
--prompt Creating sequence SEQ_ACCOUNTING_TEST
--prompt =====================================
--prompt
create sequence SEQ_ACCOUNTING_TEST
minvalue 1000
maxvalue 99999999999999999
start with 1000
increment by 1
cache 10;

--prompt
--prompt Creating sequence SEQ_ACCT_ID
--prompt =============================
--prompt
create sequence SEQ_ACCT_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_ATTACH_NODE
--prompt =================================
--prompt
create sequence SEQ_ATTACH_NODE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle
order;

--prompt
--prompt Creating sequence SEQ_ATTACH_SERIALNO
--prompt =====================================
--prompt
create sequence SEQ_ATTACH_SERIALNO
minvalue 1
maxvalue 999999999999999999
start with 1000
increment by 1
cache 20
cycle
order;

--prompt
--prompt Creating sequence SEQ_AUTH_LOG
--prompt ==============================
--prompt
create sequence SEQ_AUTH_LOG
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_BASE_RISK
--prompt ===============================
--prompt
create sequence SEQ_BASE_RISK
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
order;

--prompt
--prompt Creating sequence SEQ_BOND_DETAIL
--prompt =================================
--prompt
create sequence SEQ_BOND_DETAIL
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_BOND_TEMPLATE
--prompt ===================================
--prompt
create sequence SEQ_BOND_TEMPLATE
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_CASHFLOW_FEE
--prompt ==================================
--prompt
create sequence SEQ_CASHFLOW_FEE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_CUST_CREDIT
--prompt =================================
--prompt
create sequence SEQ_CUST_CREDIT
minvalue 10000
maxvalue 99999999999999999
start with 10000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_ED_BATCH_ID
--prompt =================================
--prompt
create sequence SEQ_ED_BATCH_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_ED_CUST_ID
--prompt ================================
--prompt
create sequence SEQ_ED_CUST_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_ED_CUST_LOG_ID
--prompt ====================================
--prompt
create sequence SEQ_ED_CUST_LOG_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_ENTRY_FLOWID
--prompt ==================================
--prompt
create sequence SEQ_ENTRY_FLOWID
minvalue 1000
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_FTP_TEST
--prompt ==============================
--prompt
create sequence SEQ_FTP_TEST
minvalue 1000
maxvalue 99999999999999999
start with 1000
increment by 1
cache 10;

--prompt
--prompt Creating sequence SEQ_IFS_REV_BRED
--prompt ==================================
--prompt
create sequence SEQ_IFS_REV_BRED
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_IFS_REV_IFXD
--prompt ==================================
--prompt
create sequence SEQ_IFS_REV_IFXD
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_IFS_REV_IOTD
--prompt ==================================
--prompt
create sequence SEQ_IFS_REV_IOTD
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_IFS_REV_IRVV
--prompt ==================================
--prompt
create sequence SEQ_IFS_REV_IRVV
minvalue 1
maxvalue 999999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_IFS_REV_ISLD
--prompt ==================================
--prompt
create sequence SEQ_IFS_REV_ISLD
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_IFS_REV_ISWH
--prompt ==================================
--prompt
create sequence SEQ_IFS_REV_ISWH
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_IFS_TEMPLATE
--prompt ==================================
--prompt
create sequence SEQ_IFS_TEMPLATE
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_IFS_TRDPARAM
--prompt ==================================
--prompt
create sequence SEQ_IFS_TRDPARAM
minvalue 2
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_INVEST_FOLLOWUP_ID
--prompt ========================================
--prompt
create sequence SEQ_INVEST_FOLLOWUP_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_IR_SERIES
--prompt ===============================
--prompt
create sequence SEQ_IR_SERIES
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_LIMIT_DETAIL
--prompt ==================================
--prompt
create sequence SEQ_LIMIT_DETAIL
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_LIMIT_LOG
--prompt ===============================
--prompt
create sequence SEQ_LIMIT_LOG
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_LOAN_CHECKBOX
--prompt ===================================
--prompt
create sequence SEQ_LOAN_CHECKBOX
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_ODS_ASSETS
--prompt ================================
--prompt
create sequence SEQ_ODS_ASSETS
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_ODS_LIABILITIES
--prompt =====================================
--prompt
create sequence SEQ_ODS_LIABILITIES
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_PARAMS_MAP
--prompt ================================
--prompt
create sequence SEQ_PARAMS_MAP
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_PUBLIC
--prompt ============================
--prompt
create sequence SEQ_PUBLIC
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_RISK_MANAGER
--prompt ==================================
--prompt
create sequence SEQ_RISK_MANAGER
minvalue 1000000001
maxvalue 99999999999
start with 1000000001
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_SPECIFIC_BOND
--prompt ===================================
--prompt
create sequence SEQ_SPECIFIC_BOND
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
order;

--prompt
--prompt Creating sequence SEQ_TA_BASE
--prompt =============================
--prompt
create sequence SEQ_TA_BASE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TA_LOG
--prompt ============================
--prompt
create sequence SEQ_TA_LOG
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TA_MESSAGE
--prompt ================================
--prompt
create sequence SEQ_TA_MESSAGE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_TA_ROLE_MODULE_MAP_COPY
--prompt =============================================
--prompt
create sequence SEQ_TA_ROLE_MODULE_MAP_COPY
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 10;

--prompt
--prompt Creating sequence SEQ_TA_USER_FLOW
--prompt ==================================
--prompt
create sequence SEQ_TA_USER_FLOW
minvalue 1
maxvalue 99999999999
start with 1000
increment by 1
cache 10;

--prompt
--prompt Creating sequence SEQ_TA_USER_ROLE_MAP_COPY
--prompt ===========================================
--prompt
create sequence SEQ_TA_USER_ROLE_MAP_COPY
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 10;

--prompt
--prompt Creating sequence SEQ_TA_WARN_MESSAGE
--prompt =====================================
--prompt
create sequence SEQ_TA_WARN_MESSAGE
minvalue 1
maxvalue 99999999999999999
start with 10000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TBK_CONFIG_DB
--prompt ===================================
--prompt
create sequence SEQ_TBK_CONFIG_DB
minvalue 1
maxvalue 99999999999999999
start with 2000
increment by 1
cache 20
order;

--prompt
--prompt Creating sequence SEQ_TBK_ENTRY_GROUP
--prompt =====================================
--prompt
create sequence SEQ_TBK_ENTRY_GROUP
minvalue 1
maxvalue 999999999999999999
start with 2000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TBK_ENTRY_SCENE
--prompt =====================================
--prompt
create sequence SEQ_TBK_ENTRY_SCENE
minvalue 1
maxvalue 999999999999999
start with 30
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TBK_ENTRY_SETTING
--prompt =======================================
--prompt
create sequence SEQ_TBK_ENTRY_SETTING
minvalue 1
maxvalue 99999999999999999
start with 30
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TBK_INST_SUBJECT_PL
--prompt =========================================
--prompt
create sequence SEQ_TBK_INST_SUBJECT_PL
minvalue 1
maxvalue 999999999999999
start with 20
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TBK_PRODUCT_EVENT
--prompt =======================================
--prompt
create sequence SEQ_TBK_PRODUCT_EVENT
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TBK_SCENCE_SUBJECT
--prompt ========================================
--prompt
create sequence SEQ_TBK_SCENCE_SUBJECT
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
order;

--prompt
--prompt Creating sequence SEQ_TBK_SUBJECT_BALANCE
--prompt =========================================
--prompt
create sequence SEQ_TBK_SUBJECT_BALANCE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TBK_YEAR_BAL
--prompt ==================================
--prompt
create sequence SEQ_TBK_YEAR_BAL
minvalue 1
maxvalue 999999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_ATTACH
--prompt ===============================
--prompt
create sequence SEQ_TC_ATTACH
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_ATTACH_NODE
--prompt ====================================
--prompt
create sequence SEQ_TC_ATTACH_NODE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_TC_CUST_CREDIT_COMPANY
--prompt ============================================
--prompt
create sequence SEQ_TC_CUST_CREDIT_COMPANY
minvalue 10000
maxvalue 999999999999999999
start with 10000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_FIELD
--prompt ==============================
--prompt
create sequence SEQ_TC_FIELD
minvalue 101
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_FIELDSET
--prompt =================================
--prompt
create sequence SEQ_TC_FIELDSET
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_FORM
--prompt =============================
--prompt
create sequence SEQ_TC_FORM
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_INST_CREDIT
--prompt ====================================
--prompt
create sequence SEQ_TC_INST_CREDIT
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_TC_PRODUCT
--prompt ================================
--prompt
create sequence SEQ_TC_PRODUCT
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_PRODUCT_TYPE
--prompt =====================================
--prompt
create sequence SEQ_TC_PRODUCT_TYPE
minvalue 30
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TC_PROJECT
--prompt ================================
--prompt
create sequence SEQ_TC_PROJECT
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_AMT_RANGE
--prompt ==================================
--prompt
create sequence SEQ_TD_AMT_RANGE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_CUST_ACC
--prompt =================================
--prompt
create sequence SEQ_TD_CUST_ACC
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_FEES_BROKER
--prompt ====================================
--prompt
create sequence SEQ_TD_FEES_BROKER
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_FEES_PASSAGEWAY
--prompt ========================================
--prompt
create sequence SEQ_TD_FEES_PASSAGEWAY
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_FEES_PASSAGEWAY_DAILY
--prompt ==============================================
--prompt
create sequence SEQ_TD_FEES_PASSAGEWAY_DAILY
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_FEES_PLAN
--prompt ==================================
--prompt
create sequence SEQ_TD_FEES_PLAN
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_FLOW_TASK_EVENT
--prompt ========================================
--prompt
create sequence SEQ_TD_FLOW_TASK_EVENT
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_FORM_APPROVE
--prompt =====================================
--prompt
create sequence SEQ_TD_FORM_APPROVE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_FUND_TPOS
--prompt ==================================
--prompt
create sequence SEQ_TD_FUND_TPOS
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_TD_PRODUCT_APPROVE
--prompt ========================================
--prompt
create sequence SEQ_TD_PRODUCT_APPROVE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_PROJECT_APPROVE
--prompt ========================================
--prompt
create sequence SEQ_TD_PROJECT_APPROVE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_RATE_RANGE
--prompt ===================================
--prompt
create sequence SEQ_TD_RATE_RANGE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TD_SETTLE
--prompt ===============================
--prompt
create sequence SEQ_TD_SETTLE
minvalue 1
maxvalue 999999
start with 2
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_TD_VERSION
--prompt ================================
--prompt
create sequence SEQ_TD_VERSION
minvalue 1000
maxvalue 99999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TM_CASHFLOW_FEE
--prompt =====================================
--prompt
create sequence SEQ_TM_CASHFLOW_FEE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_ACCOUNT
--prompt ================================
--prompt
create sequence SEQ_TT_ACCOUNT
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_ACC_IN_CASH
--prompt ====================================
--prompt
create sequence SEQ_TT_ACC_IN_CASH
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_ACC_IN_SECU
--prompt ====================================
--prompt
create sequence SEQ_TT_ACC_IN_SECU
minvalue 100
maxvalue 999999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_ACC_IN_SECU_VISIT
--prompt ==========================================
--prompt
create sequence SEQ_TT_ACC_IN_SECU_VISIT
minvalue 100
maxvalue 99999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_ACC_OUT_CASH
--prompt =====================================
--prompt
create sequence SEQ_TT_ACC_OUT_CASH
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_ACC_OUT_CASH_FIXED
--prompt ===========================================
--prompt
create sequence SEQ_TT_ACC_OUT_CASH_FIXED
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_ACC_OUT_SECU
--prompt =====================================
--prompt
create sequence SEQ_TT_ACC_OUT_SECU
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_CASHFLOW_CAPITAL
--prompt =========================================
--prompt
create sequence SEQ_TT_CASHFLOW_CAPITAL
minvalue 100
maxvalue 999999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_CASHFLOW_DAILY_INTEREST
--prompt ================================================
--prompt
create sequence SEQ_TT_CASHFLOW_DAILY_INTEREST
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_CASHFLOW_FEE
--prompt =====================================
--prompt
create sequence SEQ_TT_CASHFLOW_FEE
minvalue 100
maxvalue 999999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_CASHFLOW_INTEREST
--prompt ==========================================
--prompt
create sequence SEQ_TT_CASHFLOW_INTEREST
minvalue 100
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_CASHFLOW_ON_INTEREST
--prompt =============================================
--prompt
create sequence SEQ_TT_CASHFLOW_ON_INTEREST
minvalue 100
maxvalue 999999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_CFETS_ID
--prompt =================================
--prompt
create sequence SEQ_TT_CFETS_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_COUNTERPARTY_KIND
--prompt ==========================================
--prompt
create sequence SEQ_TT_COUNTERPARTY_KIND
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20
cycle;

--prompt
--prompt Creating sequence SEQ_TT_COUNTERPARTY_PARTY_ID
--prompt ==============================================
--prompt
create sequence SEQ_TT_COUNTERPARTY_PARTY_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_COUNTERPARTY_PARTY_NO
--prompt ==============================================
--prompt
create sequence SEQ_TT_COUNTERPARTY_PARTY_NO
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_DAYEND_ROLLING_LOG
--prompt ===========================================
--prompt
create sequence SEQ_TT_DAYEND_ROLLING_LOG
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_G_FLOW
--prompt ===============================
--prompt
create sequence SEQ_TT_G_FLOW
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_G_FLOW_STEP
--prompt ====================================
--prompt
create sequence SEQ_TT_G_FLOW_STEP
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_INST_SETTL
--prompt ===================================
--prompt
create sequence SEQ_TT_INST_SETTL
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 10;

--prompt
--prompt Creating sequence SEQ_TT_ORDER
--prompt ==============================
--prompt
create sequence SEQ_TT_ORDER
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_PACKAGESERNO
--prompt =====================================
--prompt
create sequence SEQ_TT_PACKAGESERNO
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_SETTLE_INST_ID
--prompt =======================================
--prompt
create sequence SEQ_TT_SETTLE_INST_ID
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_SETTLE_LARGE
--prompt =====================================
--prompt
create sequence SEQ_TT_SETTLE_LARGE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_TRADE
--prompt ==============================
--prompt
create sequence SEQ_TT_TRADE
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_TRD_ACC_ADJUST
--prompt =======================================
--prompt
create sequence SEQ_TT_TRD_ACC_ADJUST
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_TRD_ACC_CURRENT
--prompt ========================================
--prompt
create sequence SEQ_TT_TRD_ACC_CURRENT
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_TRD_ACC_DEPOSIT
--prompt ========================================
--prompt
create sequence SEQ_TT_TRD_ACC_DEPOSIT
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_TRD_QUOTA
--prompt ==================================
--prompt
create sequence SEQ_TT_TRD_QUOTA
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_TRD_QUOTA_APP
--prompt ======================================
--prompt
create sequence SEQ_TT_TRD_QUOTA_APP
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence SEQ_TT_TRUST
--prompt ==============================
--prompt
create sequence SEQ_TT_TRUST
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence S_TT_PROC_ACTIVITY_CREATION
--prompt =============================================
--prompt
create sequence S_TT_PROC_ACTIVITY_CREATION
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence TI_INTERFACE_INFO_S
--prompt =====================================
--prompt
create sequence TI_INTERFACE_INFO_S
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 5;

--prompt
--prompt Creating sequence TI_JOBEXCU_LOG_S
--prompt ==================================
--prompt
create sequence TI_JOBEXCU_LOG_S
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 20;

--prompt
--prompt Creating sequence TI_JOBEXCU_RECORD_S
--prompt =====================================
--prompt
create sequence TI_JOBEXCU_RECORD_S
minvalue 1
maxvalue 99999999999999999
start with 1000
increment by 1
cache 10;


-- Create sequence 
create sequence SEQ_SESSION
minvalue 1
maxvalue 9999
start with 1
increment by 1
cache 20
cycle;


-- Create sequence 
create sequence seq_Sequence
minvalue 1
maxvalue 9999
start with 1
increment by 1
cache 20
cycle;


-- Create sequence 
create sequence seq_FTP_MANAGE
minvalue 1
maxvalue 999999
start with 1
increment by 1
cache 20
cycle;

create sequence SEQ_TB_DIMENSION
minvalue 1
maxvalue 99999999999
start with 12841
increment by 1
cache 20
cycle;

create sequence SEQ_TB_PRODUCT_EVENT
minvalue 1
maxvalue 99999999999
start with 1741
increment by 1
cache 20
cycle;

create sequence SEQ_TB_DIMENSION_VALUE
minvalue 1
maxvalue 99999999999
start with 1081
increment by 1
cache 20
cycle;

create sequence SEQ_CASH_FLOW
minvalue 1
maxvalue 99999999999
start with 1081
increment by 1
cache 20
cycle;

create sequence SEQ_TB_ENTRY_FLOWID
minvalue 1
maxvalue 999999
start with 1
increment by 1
cache 20
cycle;

create sequence SEQ_IFS_LIMIT_TOTAL
minvalue 1000000001
maxvalue 9999999999
start with 1000000001
increment by 1
cache 20
cycle;

create sequence seq_ifs_taxes
minvalue 1000000001
maxvalue 9999999999
start with 1000000001
increment by 1
cache 20
cycle;


create sequence seq_ifs_trade_limit
minvalue 1000000001
maxvalue 9999999999
start with 1000000001
increment by 1
cache 20
cycle;


create sequence seq_ifs_guozhaimianshui
minvalue 1000000001
maxvalue 9999999999
start with 1000000001
increment by 1
cache 20
cycle;


create sequence seq_k_ygz
minvalue 1000000001
maxvalue 9999999999
start with 1000000001
increment by 1
cache 20
cycle;