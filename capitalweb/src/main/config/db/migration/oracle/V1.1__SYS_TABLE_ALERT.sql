ALTER TABLE IFS_CFETSFX_FWD ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSFX_SPT ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSFX_LEND ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSFX_SWAP ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_FX_DEPOSITIN ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_FX_DEPOSITOUT ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_BONDFWD ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_CBT ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_CR ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_DP ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_IBO ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_IRS ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_SL ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_CFETSRMB_OR ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_RMB_DEPOSITIN ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');
ALTER TABLE IFS_RMB_DEPOSITOUT ADD (F_PRD_CODE VARCHAR2(10) DEFAULT '');


alter table IFS_FX_DEPOSITIN  add(account_nature char(1));
alter table IFS_FX_DEPOSITIN  add(account_use varchar2(50));
alter table IFS_FX_DEPOSITOUT  add(account_nature char(1));
alter table IFS_FX_DEPOSITOUT  add(account_use varchar2(50));
alter table IFS_CFETSRMB_IBO  add(account_nature char(1));
alter table IFS_CFETSRMB_IBO  add(account_use varchar2(50));
alter table IFS_RMB_DEPOSITIN  add(account_nature char(1));
alter table IFS_RMB_DEPOSITIN  add(account_use varchar2(50));
alter table IFS_RMB_DEPOSITOUT  add(account_nature char(1));
alter table IFS_RMB_DEPOSITOUT  add(account_use varchar2(50));
comment on column IFS_FX_DEPOSITIN.account_nature  is '账户性质';
comment on column IFS_FX_DEPOSITIN.account_use  is '账户用途';
comment on column IFS_FX_DEPOSITOUT.account_nature  is '账户性质';
comment on column IFS_FX_DEPOSITOUT.account_use  is '账户用途';
comment on column IFS_CFETSRMB_IBO.account_nature  is '账户性质';
comment on column IFS_CFETSRMB_IBO.account_use  is '账户用途';
comment on column IFS_RMB_DEPOSITIN.account_nature  is '账户性质';
comment on column IFS_RMB_DEPOSITIN.account_use  is '账户用途';
comment on column IFS_RMB_DEPOSITOUT.account_nature  is '账户性质';
comment on column IFS_RMB_DEPOSITOUT.account_use  is '账户用途';

alter table IFS_OPICS_BOND  add(financing_type char(1));
comment on column IFS_OPICS_BOND.financing_type  is '融资类型';
alter table IFS_OPICS_BOND  add(debtor_level char(1));
comment on column IFS_OPICS_BOND.debtor_level  is '债务人层级';

alter table IFS_CFETSRMB_DP  add(receivable number(19,4));
comment on column IFS_CFETSRMB_DP.receivable  is '应收款项';

alter table FT_INFO  add(earliest_redemption_date varchar2(50));
comment on column FT_INFO.earliest_redemption_date  is '最早可赎回日期';
alter table IFS_CFETSRMB_CR  add(handle_amt number(19,4));
comment on column IFS_CFETSRMB_CR.handle_amt  is '手续费';

alter table IFS_CFETSRMB_IBO  add(pricing_standard_type varchar2(2));
comment on column IFS_CFETSRMB_IBO.pricing_standard_type  is '定价基准类型';
alter table IFS_CFETSRMB_IBO  add(rate_float_frequency varchar2(2));
comment on column IFS_CFETSRMB_IBO.rate_float_frequency  is '利率浮动频率';

alter table IFS_FX_DEPOSITIN  add(pricing_standard_type varchar2(2));
comment on column IFS_FX_DEPOSITIN.pricing_standard_type  is '定价基准类型';
alter table IFS_FX_DEPOSITIN  add(rate_float_frequency varchar2(2));
comment on column IFS_FX_DEPOSITIN.rate_float_frequency  is '利率浮动频率';

alter table IFS_RMB_DEPOSITIN  add(pricing_standard_type varchar2(2));
comment on column IFS_RMB_DEPOSITIN.pricing_standard_type  is '定价基准类型';
alter table IFS_RMB_DEPOSITIN  add(rate_float_frequency varchar2(2));
comment on column IFS_RMB_DEPOSITIN.rate_float_frequency  is '利率浮动频率';

alter table TA_HRB_REPORT  add(report_code varchar2(30));
comment on column TA_HRB_REPORT.report_code  is '报表编号';


alter table IFS_RMB_DEPOSITIN add (DEPOSITACCOUNT varchar2(40));
comment on column  IFS_RMB_DEPOSITIN.DEPOSITACCOUNT  is '存款账号';
alter table IFS_FX_DEPOSITIN add (DEPOSITACCOUNT varchar2(40));
comment on column  IFS_FX_DEPOSITIN.DEPOSITACCOUNT  is '存款账号';
alter table IFS_CFETSRMB_CBT add (DEPOSITACCOUNT varchar2(40));
comment on column  IFS_CFETSRMB_CBT.DEPOSITACCOUNT  is '存款账号';
alter table IFS_CFETSRMB_DP add (DEPOSITACCOUNT varchar2(40));
comment on column  IFS_CFETSRMB_DP.DEPOSITACCOUNT  is '存款账号';
alter table IFS_OPICS_CUST add (BASICDEPOSITACCOUNT varchar2(40));
comment on column  IFS_OPICS_CUST.BASICDEPOSITACCOUNT  is '基本存款账号';
alter table IFS_OPICS_CUST add (BANKACCOUNTNAME varchar2(100));
comment on column  IFS_OPICS_CUST.BANKACCOUNTNAME  is '基本账号开户行名称';


alter table IFS_CFETSRMB_CR  add(business_type varchar2(1));
comment on column IFS_CFETSRMB_CR.business_type  is '业务类型';

alter table IFS_RMB_DEPOSITOUT add (DEPOSITACCOUNT varchar2(40));
comment on column  IFS_RMB_DEPOSITOUT.DEPOSITACCOUNT  is '存款账号';
alter table IFS_FX_DEPOSITOUT add (DEPOSITACCOUNT varchar2(40));
comment on column  IFS_FX_DEPOSITOUT.DEPOSITACCOUNT  is '存款账号';

alter table IFS_CFETSRMB_IBO  add(ccy char(3));
comment on column IFS_CFETSRMB_IBO.ccy  is '币种';

alter table IFS_CFETSFX_FWD add (tenor_code VARCHAR2(10));
comment on column IFS_CFETSFX_FWD.tenor_code
  is '期限代码';

alter table IFS_CFETSFX_SWAP add (tenor_code VARCHAR2(10));
alter table IFS_CFETSFX_SWAP add (fwd_tenor_code VARCHAR2(10));
comment on column IFS_CFETSFX_SWAP.tenor_code
  is '近端期限代码';
comment on column IFS_CFETSFX_SWAP.fwd_tenor_code
  is '远端期限代码';

alter table IFS_CFETSFX_LEND add (tenor_code VARCHAR2(10));
alter table IFS_CFETSFX_LEND add (trading_type VARCHAR2(12));
comment on column IFS_CFETSFX_LEND.tenor_code
  is '期限代码';
comment on column IFS_CFETSFX_LEND.trading_type
  is '交易方式';

alter table IFS_CFETSRMB_SL  add(approval_form_name varchar2(100));
comment on column IFS_CFETSRMB_SL.approval_form_name  is '审批单表头';

alter table IFS_OPICS_CUST modify SN varchar2(80);



alter table IFS_RMB_DEPOSITOUT add (occupancy_days VARCHAR2(10));
comment on column IFS_RMB_DEPOSITOUT.occupancy_days
  is '实际占款天数';
alter table IFS_RMB_DEPOSITOUT add (first_settl_day VARCHAR2(10));
comment on column IFS_RMB_DEPOSITOUT.first_settl_day
  is '首次结息日';
alter table IFS_RMB_DEPOSITOUT add (first_pay_day VARCHAR2(10));
comment on column IFS_RMB_DEPOSITOUT.first_pay_day
  is '首次付息日';
alter table IFS_RMB_DEPOSITOUT add (settl_day_agreement VARCHAR2(5));
comment on column IFS_RMB_DEPOSITOUT.settl_day_agreement
  is '结息日约定';
alter table IFS_RMB_DEPOSITOUT add (pay_day_agreement VARCHAR2(5));
comment on column IFS_RMB_DEPOSITOUT.pay_day_agreement
  is '付息日约定';
alter table IFS_RMB_DEPOSITOUT add (adv_rate NUMBER(12,7));
comment on column IFS_RMB_DEPOSITOUT.adv_rate
  is '提前支取预期利率（%）';
alter table IFS_RMB_DEPOSITOUT add (adv_agreement VARCHAR2(5));
comment on column IFS_RMB_DEPOSITOUT.adv_agreement
  is '提前支取约定';

alter table IFS_RMB_DEPOSITIN add (occupancy_days VARCHAR2(10));
comment on column IFS_RMB_DEPOSITIN.occupancy_days
  is '实际占款天数';
alter table IFS_RMB_DEPOSITIN add (first_settl_day VARCHAR2(10));
comment on column IFS_RMB_DEPOSITIN.first_settl_day
  is '首次结息日';
alter table IFS_RMB_DEPOSITIN add (first_pay_day VARCHAR2(10));
comment on column IFS_RMB_DEPOSITIN.first_pay_day
  is '首次付息日';
alter table IFS_RMB_DEPOSITIN add (settl_day_agreement VARCHAR2(5));
comment on column IFS_RMB_DEPOSITIN.settl_day_agreement
  is '结息日约定';
alter table IFS_RMB_DEPOSITIN add (pay_day_agreement VARCHAR2(5));
comment on column IFS_RMB_DEPOSITIN.pay_day_agreement
  is '付息日约定';
alter table IFS_RMB_DEPOSITIN add (adv_rate NUMBER(12,7));
comment on column IFS_RMB_DEPOSITIN.adv_rate
  is '提前支取预期利率（%）';
alter table IFS_RMB_DEPOSITIN add (adv_agreement VARCHAR2(5));
comment on column IFS_RMB_DEPOSITIN.adv_agreement
  is '提前支取约定';
alter table IFS_RMB_DEPOSITIN add (ACCURED_INTEREST NUMBER(25,8));
comment on column IFS_RMB_DEPOSITIN.ACCURED_INTEREST
  is '到期利息(元)';
alter table IFS_RMB_DEPOSITIN add (SETTLEMENT_AMOUNT NUMBER(25,8));
comment on column IFS_RMB_DEPOSITIN.SETTLEMENT_AMOUNT
  is '到期金额(元)';

-- Add/modify columns
alter table IFS_CFETSRMB_DETAIL_CR add par_value NUMBER(25,8);
-- Add comments to the columns
comment on column IFS_CFETSRMB_DETAIL_CR.par_value
  is '券面总额*折算比例的值';