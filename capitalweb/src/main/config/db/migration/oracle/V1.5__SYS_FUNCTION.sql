
--prompt
--prompt Creating type TY_STR_SPLIT
--prompt ==========================
--prompt
CREATE OR REPLACE TYPE "TY_STR_SPLIT"                                                                                                                                                      IS TABLE OF VARCHAR2 (4000)
/

--prompt
--prompt Creating package BASEUTIL
--prompt =========================
--prompt
CREATE OR REPLACE PACKAGE BASEUTIL IS

  -- Author  : gm
  -- Created : 2012/11/5 10:18:24
  -- Purpose : 基础信息维护

  -- 生成流水编号函数,传入表名如果 ta_role
  FUNCTION SELECTSEQUENCES(TABLENAME VARCHAR2) RETURN VARCHAR2;

  --用于生成MD5编码
  FUNCTION MD5(PASSWD IN VARCHAR2) RETURN VARCHAR2;

  -- 模糊查询 sql 拼装 默认前后均有%
  --FUNCTION TOSQLBLUR(KEY  VARCHAR2,
  --                   LCHR VARCHAR2 DEFAULT ('%'),
  --                   RCHR VARCHAR2 DEFAULT ('%')) RETURN VARCHAR2;

  -- 数字类型转换 防止出现0.33 变成  .33
  FUNCTION TO_NUMBER(NUM VARCHAR2) RETURN VARCHAR2;

  -- 根据业务类型生成审批单编号 order_id
  FUNCTION GETORDERID(BIZTYPE VARCHAR2) RETURN VARCHAR2;

  -- 根据业务类型生成审批单编号 trust_id
  FUNCTION GETTRUSTID(BIZTYPE VARCHAR2) RETURN VARCHAR2;

  -- 根据业务类型生成成交单编号 trade_id
  FUNCTION GETTRADEID(BIZTYPE VARCHAR2) RETURN VARCHAR2;

  -- 将‘元’格式化为‘万元’
  FUNCTION FORMATTERTENTHOUSAND(MONEY VARCHAR2) RETURN VARCHAR2;

  -- 读取TT_ACC_IN_SECU_PACKAGE中节点的内部证券账户节点
  /*FUNCTION GETACCINSECUBYPACKAGEID(STR_PACKAGE_ID VARCHAR2,
                                   PACKAGE_TYPE   VARCHAR2) RETURN VARCHAR2;*/

  --格式化数组数据
  FUNCTION FN_SPLIT(P_STR IN VARCHAR2, P_DELIMITER IN VARCHAR2)
    RETURN TY_STR_SPLIT;


  function dateConvert(o in Varchar2, f in varchar2, d in boolean) return varchar2 ;

  --期限天数 范围
  FUNCTION TERM_DAYS_RANGE(TERM_DAYS IN INTEGER) RETURN INTEGER;

  --
  FUNCTION SPLIT(P_LIST IN VARCHAR2, P_DEL IN VARCHAR2 DEFAULT (','))
    RETURN TY_STR_SPLIT
    PIPELINED;
  --
  FUNCTION GET_INSTS_STR(STR_INST_ID IN VARCHAR2) RETURN VARCHAR2;


  --判断日期END_DATE是否在期限YEARS内
  FUNCTION ISBETWEENYEARS(END_DATE IN VARCHAR2,YEARS IN VARCHAR2) RETURN NUMBER;
    -- 根据FEE类型生成审批单编号 fee_id
  FUNCTION GETFEEID RETURN VARCHAR2;
  FUNCTION GETEDID RETURN VARCHAR2;

  FUNCTION GET_SETTLE_ID(S_STR VARCHAR2,BIZTYPE VARCHAR2) RETURN VARCHAR2;

END BASEUTIL;
/

--prompt
--prompt Creating type EN_CONCAT_IM
--prompt ==========================
--prompt
CREATE OR REPLACE TYPE en_concat_im AUTHID CURRENT_USER AS OBJECT
(
  CURR_STR VARCHAR2(32767),
  STATIC FUNCTION ODCIAGGREGATEINITIALIZE(SCTX IN OUT en_concat_im)
    RETURN NUMBER,
  MEMBER FUNCTION ODCIAGGREGATEITERATE(SELF IN OUT en_concat_im,
                                       P1   IN VARCHAR2) RETURN NUMBER,
  MEMBER FUNCTION ODCIAGGREGATETERMINATE(SELF        IN en_concat_im,
                                         RETURNVALUE OUT VARCHAR2,
                                         FLAGS       IN NUMBER)
    RETURN NUMBER,
  MEMBER FUNCTION ODCIAGGREGATEMERGE(SELF  IN OUT en_concat_im,
                                     SCTX2 IN en_concat_im) RETURN NUMBER
)
/

--prompt
--prompt Creating function EN_CONCAT
--prompt ===========================
--prompt
CREATE OR REPLACE FUNCTION en_concat(P1 VARCHAR2) RETURN VARCHAR2
  AGGREGATE USING en_concat_im;
/

--prompt
--prompt Creating package body BASEUTIL
--prompt ==============================
--prompt
CREATE OR REPLACE PACKAGE BODY BASEUTIL IS

  -- 生成流水编号函数,传入表名如果 ta_role
  FUNCTION SELECTSEQUENCES(TABLENAME VARCHAR2) RETURN VARCHAR2 IS
    STRSQL VARCHAR2(100);
    STRSEQ VARCHAR2(50);
  BEGIN
    --账户树表序列规则  PACK + seq
    IF UPPER(TABLENAME) = 'TT_COUNTERPARTY_PARTY_NO' THEN
      STRSQL := 'SELECT TRIM(TO_CHAR(SEQ_TT_COUNTERPARTY_PARTY_NO.NEXTVAL, ''0000'')) FROM DUAL ';
      --交易对手分类 编码 格式化000000 补齐
    ELSIF UPPER(TABLENAME) = 'TT_COUNTERPARTY_KIND' THEN
      STRSQL := 'SELECT TRIM(TO_CHAR(SEQ_TT_COUNTERPARTY_KIND.NEXTVAL, ''0000'')) FROM DUAL ';
    ELSE
      STRSQL := 'select seq_' || LOWER(TABLENAME) || '.nextval from dual';
    END IF;

    EXECUTE IMMEDIATE (STRSQL)
      INTO STRSEQ;
    RETURN STRSEQ;
  END;


  FUNCTION MD5(PASSWD IN VARCHAR2) RETURN VARCHAR2 IS
    RETVAL VARCHAR2(32);
  BEGIN
    RETVAL := UTL_RAW.CAST_TO_RAW(DBMS_OBFUSCATION_TOOLKIT.MD5(INPUT_STRING => PASSWD));
    RETURN RETVAL;
  END;

  -- 模糊查询 sql 拼装 默认前后均有%
  /*
  FUNCTION TOSQLBLUR(KEY  VARCHAR2,
                     LCHR VARCHAR2 DEFAULT ('%'),
                     RCHR VARCHAR2 DEFAULT ('%')) RETURN VARCHAR2 IS
  BEGIN
    RETURN LCHR || KEY || RCHR;
  END;*/

  -- 数字类型转换 防止出现0.33 变成  .33
  FUNCTION TO_NUMBER(NUM VARCHAR2) RETURN VARCHAR2 IS
  BEGIN
    RETURN RTRIM(TO_CHAR(NUM, 'fm99999999999999990.9999999'), '.');
  END;

  -- 根据业务类型生成审批单编号 order_id
  FUNCTION GETORDERID(BIZTYPE VARCHAR2) RETURN VARCHAR2 IS
    RET VARCHAR2(100);
    BIZ_INDEX INTEGER;
    BIZTYPE_IN VARCHAR2(100);
  BEGIN
    BIZ_INDEX := INSTR(BIZTYPE,'_');
    BIZTYPE_IN := CASE WHEN biz_index > 0 THEN SUBSTR(BIZTYPE,0,biz_index-1) else BIZTYPE end;
    SELECT 'ORD' || TO_CHAR(SYSDATE, 'YYYYMMDD') || BIZTYPE_IN ||
           TRIM(TO_CHAR(SEQ_TT_ORDER.NEXTVAL, '000000'))
      INTO RET
      FROM DUAL;
    RETURN RET;
  END;

  -- 根据业务类型生成委托单编号 trust_id
  FUNCTION GETTRUSTID(BIZTYPE VARCHAR2) RETURN VARCHAR2 IS
    RET VARCHAR2(100);
  BEGIN
    SELECT 'TRS' || TO_CHAR(SYSDATE, 'YYYYMMDD') || BIZTYPE ||
           TRIM(TO_CHAR(SEQ_TT_TRUST.NEXTVAL, '000000'))
      INTO RET
      FROM DUAL;
    RETURN RET;
  END;

  -- 根据业务类型生成成交单编号 trade_id
  FUNCTION GETTRADEID(BIZTYPE VARCHAR2) RETURN VARCHAR2 IS
    RET VARCHAR2(100);
    BIZ_INDEX INTEGER;
    BIZTYPE_IN VARCHAR2(100);
  BEGIN
    BIZ_INDEX := INSTR(BIZTYPE,'_');
    BIZTYPE_IN := CASE WHEN biz_index > 0 THEN SUBSTR(BIZTYPE,0,biz_index-1) else BIZTYPE end;
    SELECT 'TRD' || TO_CHAR(SYSDATE, 'YYYYMMDD') || BIZTYPE_IN ||
           TRIM(TO_CHAR(SEQ_TT_TRADE.NEXTVAL, '000000'))
      INTO RET
      FROM DUAL;
    RETURN RET;
  END;

  -- 将‘元’格式化为‘万元’
  FUNCTION FORMATTERTENTHOUSAND(MONEY VARCHAR2) RETURN VARCHAR2 IS
    RET VARCHAR2(100);
  BEGIN
    SELECT TO_NUMBER(MONEY) / 10000 INTO RET FROM DUAL;
    RETURN RET;
  END;

  -- 读取TT_ACC_IN_SECU_PACKAGE中节点的内部证券账户节点
   /*FUNCTION GETACCINSECUBYPACKAGEID(STR_PACKAGE_ID VARCHAR2,
                                   PACKAGE_TYPE   VARCHAR2) RETURN VARCHAR2 IS
    RET     VARCHAR2(1000);
    PACKROW TT_ACC_IN_SECU_PACKAGE%ROWTYPE;
    CURSOR PACKROWS IS
      SELECT T.*
        FROM TT_ACC_IN_SECU_PACKAGE T
       WHERE T.PACKAGE_TYPE = '1'
       START WITH T.PACKAGE_ID = STR_PACKAGE_ID
              AND T.PACKAGE_TYPE = PACKAGE_TYPE
      CONNECT BY PRIOR T.PACKAGE_ID = T.PACKAGE_PID;
  BEGIN

    IF PACKAGE_TYPE = '1' THEN
      RET := STR_PACKAGE_ID;
      RETURN RET;
    END IF;

    RET := '''';
    FOR PACKROW IN PACKROWS LOOP
      RET := RET || PACKROW.PACKAGE_ID || ''',''';
    END LOOP;

    RETURN SUBSTR(RET, 0, LENGTH(RET) - 2);

  END;*/
  --格式化数组数据
  FUNCTION FN_SPLIT(P_STR IN VARCHAR2, P_DELIMITER IN VARCHAR2)
    RETURN TY_STR_SPLIT IS
    J         INT := 0;
    I         INT := 1;
    LEN       INT := 0;
    LEN1      INT := 0;
    STR       VARCHAR2(4000);
    STR_SPLIT TY_STR_SPLIT := TY_STR_SPLIT();
  BEGIN
    LEN  := LENGTH(P_STR);
    LEN1 := LENGTH(P_DELIMITER);

    WHILE J < LEN LOOP
      J := INSTR(P_STR, P_DELIMITER, I);

      IF J = 0 THEN
        J   := LEN;
        STR := SUBSTR(P_STR, I);
        STR_SPLIT.EXTEND;
        STR_SPLIT(STR_SPLIT.COUNT) := STR;

        IF I >= LEN THEN
          EXIT;
        END IF;
      ELSE
        STR := SUBSTR(P_STR, I, J - I);
        I   := J + LEN1;
        STR_SPLIT.EXTEND;
        STR_SPLIT(STR_SPLIT.COUNT) := STR;
      END IF;
    END LOOP;

    RETURN STR_SPLIT;
  END;

  function dateConvert(o in Varchar2, f in varchar2, d in boolean) return varchar2 is
    begin
       if f = '1Y' then
          if d then
          return to_char(add_months(to_date(o,'yyyyMMdd'), 12),'yyyyMMdd');
          else
            return to_char(add_months(to_date(o,'yyyyMMdd'), -12),'yyyyMMdd');
         end if;
      end if;
      if f = '1M' then
       if d then
          return to_char(add_months(to_date(o,'yyyyMMdd'), 1),'yyyyMMdd');
          else
            return to_char(add_months(to_date(o,'yyyyMMdd'), -1),'yyyyMMdd');
         end if;
      end if;

      if f = '2M' then
       if d then
          return to_char(add_months(to_date(o,'yyyyMMdd'), 2),'yyyyMMdd');
          else
            return to_char(add_months(to_date(o,'yyyyMMdd'), -2),'yyyyMMdd');
            end if;
      end if;


      if f = '3M' then
       if d then
          return to_char(add_months(to_date(o,'yyyyMMdd'), 3),'yyyyMMdd');
          else
            return to_char(add_months(to_date(o,'yyyyMMdd'), -3),'yyyyMMdd');
            end if;
      end if;


      if f = '4M' then
       if d then
          return to_char(add_months(to_date(o,'yyyyMMdd'), 4),'yyyyMMdd');
          else
            return to_char(add_months(to_date(o,'yyyyMMdd'), -4),'yyyyMMdd');
            end if;
      end if;


      if f = '6M' then
       if d then
          return to_char(add_months(to_date(o,'yyyyMMdd'), 6),'yyyyMMdd');
          else
            return to_char(add_months(to_date(o,'yyyyMMdd'), -6),'yyyyMMdd');
           end if;
      end if;


      return o;
     end;
  --期限天数 范围
  FUNCTION TERM_DAYS_RANGE(TERM_DAYS IN INTEGER) RETURN INTEGER IS
  BEGIN
    IF TERM_DAYS = 0 THEN
      RETURN -1;
    END IF;
    IF TERM_DAYS > 0 AND TERM_DAYS <= 1 THEN
      RETURN 0;
    END IF;
    IF TERM_DAYS > 1 AND TERM_DAYS <= 7 THEN
      RETURN 1;
    END IF;
    IF TERM_DAYS > 7 AND TERM_DAYS <= 14 THEN
      RETURN 7;
    END IF;
    IF TERM_DAYS > 14 AND TERM_DAYS <= 30 THEN
      RETURN 14;
    END IF;
    IF TERM_DAYS > 30 AND TERM_DAYS <= 90 THEN
      RETURN 30;
    END IF;
    IF TERM_DAYS > 90 AND TERM_DAYS <= 180 THEN
      RETURN 90;
    END IF;
    IF TERM_DAYS > 180 AND TERM_DAYS <= 365 THEN
      RETURN 180;
    END IF;
    IF TERM_DAYS > 365 AND TERM_DAYS <= 365 * 3 THEN
      RETURN 365;
    END IF;
    RETURN 365 * 3;

  END;

  FUNCTION SPLIT(P_LIST IN VARCHAR2, P_DEL IN VARCHAR2 DEFAULT (','))
    RETURN TY_STR_SPLIT
    PIPELINED IS
    L_IDX  PLS_INTEGER;
    L_LIST VARCHAR2(32767) := P_LIST;
  BEGIN
    LOOP
      L_IDX := INSTR(L_LIST, P_DEL);
      IF L_IDX > 0 THEN
        PIPE ROW(SUBSTR(L_LIST, 1, L_IDX - 1));
        L_LIST := SUBSTR(L_LIST, L_IDX + LENGTH(P_DEL));
      ELSE
        PIPE ROW(L_LIST);
        EXIT;
      END IF;
    END LOOP;
    RETURN;
  END;

  --
  FUNCTION GET_INSTS_STR(STR_INST_ID IN VARCHAR2) RETURN VARCHAR2 IS
    RET VARCHAR2(1000);
  BEGIN
    FOR INSTROW IN (SELECT T.INST_FULLNAME
                      FROM TT_INSTITUTION T, TABLE(SPLIT(STR_INST_ID)) TMP
                     WHERE TMP.COLUMN_VALUE = T.INST_ID) LOOP
      RET := RET || INSTROW.INST_FULLNAME || ',';
      if length(RET) > 200 then
          RETURN RTRIM(RET, ',');
       end if;
    END LOOP;

    RETURN RTRIM(RET, ',');
  END;


  FUNCTION ISBETWEENYEARS(END_DATE IN VARCHAR2,YEARS IN VARCHAR2) RETURN NUMBER IS
    VALUE_DATE DATE;
    DOWN_DATE DATE;
    UP_DATE DATE;
    MATURITY_DATE DATE;
  BEGIN
    MATURITY_DATE := TO_DATE(END_DATE,'YYYY-MM-DD');
    SELECT TO_DATE(D.CUR_DATE,'YYYY-MM-DD') INTO VALUE_DATE FROM TT_DAYEND_DATE D;
    IF YEARS = 'BelowOneYear' THEN
      DOWN_DATE := VALUE_DATE;
      UP_DATE := ADD_MONTHS(VALUE_DATE, 12);
      IF MATURITY_DATE >= DOWN_DATE AND MATURITY_DATE < UP_DATE THEN
        RETURN 1;
      END IF;
    ELSIF YEARS = 'OneToFiveYear' THEN
      DOWN_DATE := ADD_MONTHS(VALUE_DATE, 12);
      UP_DATE := ADD_MONTHS(VALUE_DATE, 60);
      IF MATURITY_DATE >= DOWN_DATE AND MATURITY_DATE < UP_DATE THEN
        RETURN 1;
      END IF;
    ELSIF YEARS = 'FiveToTenYear' THEN
      DOWN_DATE := ADD_MONTHS(VALUE_DATE, 60);
      UP_DATE := ADD_MONTHS(VALUE_DATE, 120);
      IF MATURITY_DATE >= DOWN_DATE AND MATURITY_DATE < UP_DATE THEN
        RETURN 1;
      END IF;
    ELSIF YEARS = 'OverTenYear' THEN
      DOWN_DATE := ADD_MONTHS(VALUE_DATE, 120);
      IF MATURITY_DATE >= DOWN_DATE THEN
        RETURN 1;
      END IF;
    END IF;
    RETURN 0;
  END;


  -- 根据业务类型生成审批单编号 order_id
  FUNCTION GETFEEID RETURN VARCHAR2 IS
    RET VARCHAR2(100);
  BEGIN
    SELECT 'CF' || TO_CHAR(SYSDATE, 'YYYYMMDD') ||
           TRIM(TO_CHAR(SEQ_CASHFLOW_FEE.NEXTVAL, '000000'))
      INTO RET
      FROM DUAL;
    RETURN RET;
  END;

  FUNCTION GETEDID RETURN VARCHAR2 IS
    RET VARCHAR2(100);
  BEGIN
    SELECT 'CF' || TO_CHAR(SYSDATE, 'YYYYMMDD') ||
           TRIM(TO_CHAR(SEQ_ED_BATCH_ID.NEXTVAL, '000000'))
      INTO RET
      FROM DUAL;
    RETURN RET;
  END;


  -- 根据业务类型生成交易编号 trade_id
  FUNCTION GET_SETTLE_ID(S_STR VARCHAR2,BIZTYPE VARCHAR2) RETURN VARCHAR2 IS
    RET VARCHAR2(100);
    BIZ_INDEX INTEGER;
    BIZTYPE_IN VARCHAR2(100);
  BEGIN
    BIZ_INDEX := INSTR(BIZTYPE,'_');
    BIZTYPE_IN := CASE WHEN biz_index > 0 THEN SUBSTR(BIZTYPE,0,biz_index-1) else BIZTYPE end;
    SELECT S_STR || TO_CHAR(SYSDATE, 'YYYYMMDD') || BIZTYPE_IN ||
           TRIM(TO_CHAR(SEQ_TD_SETTLE.NEXTVAL, '000000'))
      INTO RET
      FROM DUAL;
    RETURN RET;
  END;

END BASEUTIL;
/

--prompt
--prompt Creating type body EN_CONCAT_IM
--prompt ===============================
--prompt
CREATE OR REPLACE TYPE BODY en_concat_im IS
  STATIC FUNCTION ODCIAGGREGATEINITIALIZE(SCTX IN OUT en_concat_im)
    RETURN NUMBER IS
  BEGIN
    SCTX := en_concat_im(NULL);
    RETURN ODCICONST.SUCCESS;
  END;
  MEMBER FUNCTION ODCIAGGREGATEITERATE(SELF IN OUT en_concat_im,
                                       P1   IN VARCHAR2) RETURN NUMBER IS
  BEGIN
    IF (CURR_STR IS NOT NULL) THEN
      CURR_STR := CURR_STR || ',' || P1;
    ELSE
      CURR_STR := P1;
    END IF;
    RETURN ODCICONST.SUCCESS;
  END;
  MEMBER FUNCTION ODCIAGGREGATETERMINATE(SELF        IN en_concat_im,
                                         RETURNVALUE OUT VARCHAR2,
                                         FLAGS       IN NUMBER) RETURN NUMBER IS
  BEGIN
    RETURNVALUE := CURR_STR;
    RETURN ODCICONST.SUCCESS;
  END;
  MEMBER FUNCTION ODCIAGGREGATEMERGE(SELF  IN OUT en_concat_im,
                                     SCTX2 IN en_concat_im) RETURN NUMBER IS
  BEGIN
    IF (SCTX2.CURR_STR IS NOT NULL) THEN
      SELF.CURR_STR := SELF.CURR_STR || ',' || SCTX2.CURR_STR;
    END IF;
    RETURN ODCICONST.SUCCESS;
  END;
END;
/


--跳转到下一个节假日
CREATE OR REPLACE FUNCTION SL_FUN_AFTERWORKDATE(I_POSTDATE IN DATE,
                                               I_CCY       IN CHAR)
  RETURN DATE IS
  O_BEFOREWORK  DATE;
  VS_COUNT    INT;
  VS_TEMPDATE DATE;
  VS_ENDCOUNT INT;
BEGIN
  SELECT COUNT(*)
    INTO VS_ENDCOUNT
    FROM IFS_OPICS_HLDY
   WHERE HOLIDATE = I_POSTDATE
     AND CALENDARID = I_CCY;
  IF (VS_ENDCOUNT > 0) THEN
    BEGIN
      VS_TEMPDATE := I_POSTDATE;
      LOOP
        SELECT COUNT(*)
          INTO VS_COUNT
          FROM IFS_OPICS_HLDY
         WHERE HOLIDATE = VS_TEMPDATE
           AND CALENDARID = I_CCY;
        EXIT WHEN VS_COUNT = 0;
        IF (VS_COUNT > 0) THEN
          VS_TEMPDATE := VS_TEMPDATE + 1;
        END IF;
      END LOOP;
      O_BEFOREWORK := VS_TEMPDATE;
    END;

  ELSE
    O_BEFOREWORK := I_POSTDATE ;
  END IF;
  RETURN O_BEFOREWORK;
END SL_FUN_AFTERWORKDATE;
/
