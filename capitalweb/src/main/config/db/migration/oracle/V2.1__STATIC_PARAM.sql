
--系统参数
DELETE ta_sys_param WHERE 1=1;
insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Bankid', '9001000113901410', 'HRBCBFX', '国际业务二代报文清算路径', '二代付款行号，不能修改', '0', '00', '00', 'FX-国际业务部付款行号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Userid', '9001000113901410', 'HRBCBFX', '国际业务二代报文清算路径', '二代付款账号，不能修改', '0', '00', '00', 'FX-国际业务部付款账号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Bankname', '哈尔滨银行股份有限公司', 'HRBCBFX', '国际业务二代报文清算路径', '二代付款行行名，不能修改', '0', '00', '00', 'FX-国际业务部付款行名');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Username', '哈尔滨银行股份有限公司', 'HRBCBFX', '国际业务二代报文清算路径', '二代付款账户名称，不能修改', '0', '00', '00', 'FX-国际业务部付款账户名称');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Bankid', '9001000113901401', 'HRBCB', '二代报文清算路径', '二代付款行行号，不能修改', '0', '00', '00', 'FM-金融市场部付款行号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Userid', '9001000113901401', 'HRBCB', '二代报文清算路径', '二代付款账号，不能修改', '0', '00', '00', 'FM-金融市场部付款账号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Bankname', '投资类业务', 'HRBCB', '二代报文清算路径', '二代付款行行名，不能修改', '0', '00', '00', 'FM-金融市场部付款行名');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Username', '投资类业务', 'HRBCB', '二代报文清算路径', '二代付款账户名称，不能修改', '0', '00', '00', 'FM-金融市场部付款账户名称');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('VerNo', '20170412ESB', 'ESB', 'ESB报文要素', 'ESB报文--报文版本号， 不能修改', '0', '00', '00', '版本号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('ReqSysCd', '174', 'ESB', 'ESB报文要素', 'ESB报文--请求方系统代码（opics系统-174）,不能修改', '0', '00', '00', '请求方系统代码');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('TxnTyp', 'RQ', 'ESB', 'ESB报文要素', 'ESB报文--交易类型,不能修改', '0', '00', '00', '交易类型');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('TxnMod', '0', 'ESB', 'ESB报文要素', 'ESB报文--交易模式,不能修改', '0', '00', '00', '交易模式');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('TerminalType', '0', 'ESB', 'ESB报文要素', 'ESB报文--终端类型,不能修改', '0', '00', '00', '终端类型');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Flag1', '0', 'ESB', 'ESB报文要素', 'ESB报文--标识1,不能修改', '0', '00', '00', '标识1');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Flag2', '0', 'ESB', 'ESB报文要素', 'ESB报文--标识2,不能修改', '0', '00', '00', '标识2');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Flag4', '5', 'ESB', 'ESB报文要素', 'ESB报文--标识4,不能修改', '0', '00', '00', '标识4');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('TrmNo', '000', 'ESB', 'ESB报文要素', 'ESB报文--终端号,不能修改', '0', '00', '00', '终端号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('BancsSeqNo', '0', 'ESB', 'ESB报文要素', 'ESB报文--流水号,不能修改', '0', '00', '00', '流水号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('SupervisorID', '0000000', 'ESB', 'ESB报文要素', 'ESB报文--主管id号,不能修改', '0', '00', '00', '主管id号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('ChnlNo', '174', 'ESB', 'ESB报文要素', 'ESB报文--渠道号,不能修改', '0', '00', '00', '渠道号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('TlrNo', '9001351', 'ESB', 'ESB报文要素', 'ESB报文--柜员号,不能修改', '0', '00', '00', '柜员号');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('OldAcctFlag', '0', 'ESB', 'ESB报文要素', 'ESB报文--旧账号标识,不能修改', '0', '00', '00', '旧账号标识');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Syscd', 'E1', 'ESBMS5702', '二代支付报文要素', '二代支付--通道,不能修改', '0', '00', '00', '通道');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Procuctflag', '0', 'ESBMS5702', '二代支付报文要素', '二代支付--强制执行标志,不能修改', '0', '00', '00', '强制执行标志');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Paytyp', '0101', 'ESBMS5702', '二代支付报文要素', '二代支付--支付类型,不能修改', '0', '00', '00', '支付类型');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Feemod', '0', 'ESBMS5702', '二代支付报文要素', '二代支付--手续费收费方式,不能修改', '0', '00', '00', '手续费收费方式');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Priority', '2', 'ESBMS5702', '二代支付报文要素', '二代支付--优先级,不能修改', '0', '00', '00', '优先级');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Ctgypurpcd', '02102', 'ESBMS5702', '二代支付报文要素', '二代支付--业务种类,不能修改', '0', '00', '00', '业务种类');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Actflg', '1', 'ESBMS5702', '二代支付报文要素', '二代支付--现转标识,不能修改', '0', '00', '00', '现转标识');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('TxCode', '700105', 'ESBMS700105', '二代支付报文要素', '二代支付--交易码,不能修改', '0', '00', '00', '交易码');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('SysFlag', 'OPCS', 'ESBMS700105', '二代支付报文要素', '二代支付--应用系统标志,不能修改', '0', '00', '00', '应用系统标志');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('RetuFlag', 'I', 'ESBMS700105', '二代支付报文要素', '二代支付--文件响应方式,不能修改', '0', '00', '00', '文件响应方式');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('Flag', '1', 'ESBMS700105', '二代支付报文要素', '二代支付--柜面外围标志,不能修改', '0', '00', '00', '柜面外围标志');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('NotiType', 'E', 'ESBMS700105', '二代支付报文要素', '二代支付--通知处理类型,不能修改', '0', '00', '00', '通知处理类型');

insert into ta_sys_param (P_CODE, P_VALUE, P_TYPE, P_TYPE_NAME, P_MEMO, P_EDITABLED, P_PROP1, P_PROP2, P_NAME)
values ('000002', 'ifJorP', 'JAVA', '前置访问OPICS系统标识', '前置访问OPICS系统标识', '0', '00', '00', '调用java代码');

commit;


-- prompt Importing table TC_PRODUCT...
-- select * from TC_PRODUCT t order by t.prd_no ;
DELETE TC_PRODUCT WHERE 1=1;
insert into TC_PRODUCT values('391','00000','外汇远期','5Y','7000','4','0','1','2021-06-21','7000-37','7000','外汇远期','37','trade/ProudctApproveManage.jsp?prdNo=391=外汇远期=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('411','00000','外汇即期','7Y','7000','4','0','0','2021-06-21','7000-36','7000','外汇即期','36','trade/ProudctApproveManage.jsp?prdNo=411=外汇即期=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('425','00000','存单发行','5Y','7000','4','0','1','2021-06-21','555-2','7000','存单发行','45','trade/ProudctApproveManage.jsp?prdNo=425'||chr(38)||'prdName=存单发行'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('432','00000','外汇掉期','5Y','555','4','0','1','2021-06-21','555-2','555','外汇掉期','2','trade/ProudctApproveManage.jsp?prdNo=432=外汇掉期=4=555','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('435','00000','同业拆放','5Y','7000','4','0','1','2021-06-21','7000-27','7000','同业拆放','30','trade/ProudctApproveManage.jsp?prdNo=435'||chr(38)||'prdName=同业拆放'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('436','00000','同业存放(外币)','5Y','7000','4','0','1','2021-06-21','7000-30','7000','同业存放(外币)','30','trade/ProudctApproveManage.jsp?prdNo=436'||chr(38)||'prdName=同业存放(外币)'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('437','00000','存放同业(外币)','5Y','7000','4','0','1','2021-06-21','7000-29','7000','存放同业(外币)','30','trade/ProudctApproveManage.jsp?prdNo=437'||chr(38)||'prdName=存放同业(外币)'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('438','00000','外币拆借','5Y','555','4','0','1','2021-06-21','7000-30','555','外币拆借','2','trade/ProudctApproveManage.jsp?prdNo=438=外币拆借=4=555','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('441','00000','利率互换','5Y','7000','4','0','1','2021-06-21','7000-38','7000','利率互换','27','trade/ProudctApproveManage.jsp?prdNo=441'||chr(38)||'prdName=利率互换'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('442','00000','买断式回购','5Y','7000','4','0','1','2021-06-21','7000-28','7000','买断式回购','30','trade/ProudctApproveManage.jsp?prdNo=442=买断式回购=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('443','00000','现券买卖','5Y','7000','4','0','1','2021-06-21','7000-28','7000','现券买卖','29','trade/ProudctApproveManage.jsp?prdNo=443'||chr(38)||'prdName=现券买卖'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('444','00000','信用拆借','5Y','7000','4','0','1','2021-06-21','7000-45','7000','信用拆借','30','trade/ProudctApproveManage.jsp?prdNo=444'||chr(38)||'prdName=信用拆借'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('445','00000','债券借贷','5Y','3000','1','0','1','2021-06-21','7000-45','7000','债券借贷','2','trade/ProudctApproveManage.jsp?prdNo=455'||chr(38)||'prdName=债券借贷'||chr(38)||'prdProp=1'||chr(38)||'prdRootType=3000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('446','00000','质押式回购','5Y','7000','4','0','1','2021-06-21','7000-30','7000','质押式回购','28','trade/ProudctApproveManage.jsp?prdNo=446'||chr(38)||'prdName=质押式回购'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('447','00000','债券远期','5Y','7000','4','0','1','2021-06-21','7000-30','7000','债券远期','28','trade/ProudctApproveManage.jsp?prdNo=447'||chr(38)||'prdName=债券远期'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('452','00000','债券发行','5Y','7000','4','0','1','2021-06-21','7000-30','7000','债券发行','45','trade/ProudctApproveManage.jsp?prdNo=452'||chr(38)||'prdName=债券发行'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('467','00000','国库定期存款','5Y','7000','4','0','1','2021-06-21','7000-30','7000','国库定期存款','30','trade/ProudctApproveManage.jsp?prdNo=544=国库定期存款=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('468','00000','交易所回购','5Y','7000','4','0','1','2021-06-21','7000-30','7000','交易所回购','30','trade/ProudctApproveManage.jsp?prdNo=544=交易所回购=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('469','00000','常备借贷便利','5Y','7000','4','0','1','2021-06-21','7000-30','7000','常备借贷便利','30','trade/ProudctApproveManage.jsp?prdNo=544=常备借贷便利=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('470','00000','中期借贷便利','5Y','7000','4','0','1','2021-06-21','7000-31','7000','中期借贷便利','30','trade/ProudctApproveManage.jsp?prdNo=544=中期借贷便利=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('471','00000','提前偿还','7Y','7000','4','0','1','2021-06-21','7000-44','7000','提前偿还','42','trade/ProudctApproveManage.jsp?prdNo=471'||chr(38)||'prdName=提前偿还'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('472','00000','线下押券','5Y','7000','4','0','1','2021-06-21','7000-33','7000','线下押券','30','trade/ProudctApproveManage.jsp?prdNo=544=线下押券=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('510','00000','拆借提前还款','7Y','7000','4','1','1','2021-06-21','7000-44','7000','拆借提前还款','33','trade/ProudctApproveManage.jsp?prdNo=510=拆借提前还款=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('544','00000','同业借款','5Y','7000','4','0','1','2021-06-21','7000-44','7000','同业借款','30','trade/ProudctApproveManage.jsp?prdNo=544=同业借款=4=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('666','00000','额度占用','7Y','3000','1','1','1','2021-06-21','7000-33','7000','额度占用','2','trade/ProudctApproveManage.jsp?prdNo=666'||chr(38)||'prdName=额度占用'||chr(38)||'prdProp=1'||chr(38)||'prdRootType=3000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('667','00000','额度释放','7Y','3000','1','1','1','2021-06-21','7000-30','7000','额度释放','2','trade/ProudctApproveManage.jsp?prdNo=667'||chr(38)||'prdName=额度释放'||chr(38)||'prdProp=1'||chr(38)||'prdRootType=3000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('790','00000','同业存放','5Y','7000','4','0','1','2021-06-21','7000-38','7000','同业存放','30','trade/ProudctApproveManage.jsp?prdNo=790'||chr(38)||'prdName=同业存放'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('791','00000','存放同业','5Y','7000','4','0','1','2021-06-21','7000-38','7000','存放同业','30','trade/ProudctApproveManage.jsp?prdNo=791'||chr(38)||'prdName=存放同业'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('799','00000','同业存款(线下)','7Y','7000','4','0','1','2021-06-21','7000-38','7000','同业存款(线下)','41','trade/ProudctApproveManage.jsp?prdNo=799'||chr(38)||'prdName=同业存款(线下)'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('800','00000','同业存款(线上)','7Y','7000','4','0','1','2021-06-21','7000-40','7000','同业存款(线上)','41','trade/ProudctApproveManage.jsp?prdNo=800'||chr(38)||'prdName=同业存款(线上)'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('801','00000','货币基金','7Y','7000','4','0','1','2021-06-21','7000-41','7000','货币基金','41','trade/ProudctApproveManage.jsp?prdNo=801'||chr(38)||'prdName=货币基金'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('802','00000','债券基金','7Y','7000','4','0','1','2021-06-21','7000-42','7000','债券基金','42','trade/ProudctApproveManage.jsp?prdNo=802'||chr(38)||'prdName=债券基金'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
insert into TC_PRODUCT values('803','00000','专户基金','7Y','7000','4','0','1','2021-06-21','7000-40','7000','专户基金','42','trade/ProudctApproveManage.jsp?prdNo=803'||chr(38)||'prdName=专户基金'||chr(38)||'prdProp=4'||chr(38)||'prdRootType=7000','','','','HRBBANK','','20107050100');
commit;

DELETE TC_PRODUCT_OPICS WHERE 1=1;
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('411','FXD','FX','DEFAULT','1010,1020,102010,1030,104010,105010,107010','FXZY,FXBK,FXDK,FXZS,POSP,POFW','外汇即期');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('391','FXD','FX','DEFAULT','102020,1040,104020,105020,106020,107020','FXZY,FXBK,FXDK,FXZS,POSP,POFW','外汇远期');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('432','FXD','FX,SW','DEFAULT','1040,104020,105020,106020,107020','FXZY,FXBK,FXDK,FXZS,POSP,POFW','外汇掉期');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('444','MM','CC,CR','COMMBK-D,OTHERDEFIN,OTHNODEFIN,ZGS,POLICYBK','8500000041,8500000042,8500000031,8500000032,8500000033','CJLD,CJTZ','信用拆借-拆出|信用拆借-拆入');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('435','MM','LD,BR','DEFAULT','504020,503020','MMZY','拆放同业-对俄|同业拆放-对俄');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('436','MM','DP','DEFAULT','501020','MMZY','同业存放-对俄');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('437','MM','LN','DEFAULT','502020','MMZY','存放同业-对俄');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('790','MM','TC','COMMBK-D,OTHERDEFIN,OTHNODEFIN,POLICYBK,ZGS,ZGS-BK,ZGS-FIN','8500000011,8500000012','MMZY','同业存放');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('791','MM','CT','COMMBK-D,OTHERDEFIN,OTHNODEFIN,POLICYBK,ZGS,ZGS-BK,ZGS-FIN','8500000021,8500000022','MMZY','存放同业');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('438','MM','BR,LD','DEFAULT','504010,504020,503010,503020,502010,502020','MMZY','外币拆借-拆出|外币拆借-拆入');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('467','REPO','GD','GOVERNMENT','8400000001','ZQJY,ZQTZ','国库定期存款');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('468','REPO','JY','CTRLBK,POLICYBK,COMMBK-D,OTHERDEFIN,OTHNODEFIN,ZGS,DOMOTHER','8400000000','ZQJY,ZQTZ','交易所回购-逆回购');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('469','REPO','CB','GOVERNMENT','8400000002','ZQJY,ZQTZ','常备借贷便利');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('470','REPO','ZQ','GOVERNMENT','8400000003','ZQJY,ZQTZ','中期借贷便利');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('472','REPO','ZD','GOVERNMENT','8400000004','ZQJY,ZQTZ','支小再贷款');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('544','MM','CL,LC','COMMBK-D,OTHERDEFIN,OTHNODEFIN,ZGS,POLICYBK','8500000031,8500000032,8500000041,8500000042','CJLD,CJTZ','同业借款-借入|同业借款-借出');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('446','REPO','VP,RP','CTRLBK,POLICYBK,COMMBK-D,OTHERDEFIN,OTHNODEFIN,ZGS,DOMOTHER','8400000011,8400000012,8400000021,8400000022','ZQJY,ZQTZ','质押式-逆回购|质押式-正回购');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('447','SECUR','SD,IM','DOMCENBKSE,DOMFINSE,DOMGOVSE,DOMLOGOVSE,DOMNOFINSE,DOMPOLICSE,DOMNBFINSE,DOMCD,DOMYXZ','8200001011,8300001041,8200001044,8300001011,8200001021,8200001031,8200001041,8200001043','ZQJY,ZQTZ,DLYW,ZYCH,ZYFX','债券远期');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('442','REPO','VB,RB','CTRLBK,POLICYBK,COMMBK-D,OTHERDEFIN,OTHNODEFIN,ZGS,DOMOTHER','8400000041,8400000042,8400000031,8400000032','ZQJY,ZQTZ','买断式-逆回购|买断式-正回购');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('445','SECLEN','SB,SL','DEFAULT','9000010000,9000020000','SLBS,SLLS','债券借贷');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('452','SECUR','SD','SDIS','8300002001,8300002002,8300002003','ZQJY,ZQTZ','债券发行');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('425','SECUR','CD','CDIS','8300003001,8300003002,8300003003','ZQJY,ZQTZ','存单发行');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('441','SWAP','SP','DEFAULT','201010,202020,202010','SWZY','利率互换');
insert into TC_PRODUCT_OPICS (PRD_CODE,PCODE,PTYPE,ACTY,COSTCENT,PORTFOLIO,REMARK) values('443','SECUR','SD,IM','DOMCENBKSE,DOMFINSE,DOMGOVSE,DOMLOGOVSE,DOMNOFINSE,DOMPOLICSE,DOMNBFINSE,DOMCD,DOMYXZ','8200001011,8300001041,8200001044,8300001011,8200001021,8200001031,8200001041,8200001043','ZQJY,ZQTZ,DLYW,ZYCH,ZYFX','债券买卖');
commit;

--用户限额数据
insert into IFS_QUOTA (USER_ID, INST_ID, PRODUCT, PROD_TYPE, COST, PATH, LEV, QUOTA)
values ('000142', '999999999', 'FXD', 'FX', '1050110000', '200', '1', 0.01);
insert into IFS_QUOTA (USER_ID, INST_ID, PRODUCT, PROD_TYPE, COST, PATH, LEV, QUOTA)
values ('005968', '999999999', 'FXD', 'FX', '1050110000', '200', '3', 0.10);

--报表数据
delete from ta_hrb_report;
commit;

insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('bjjt','本金计提变动情况表','','1','hrbReport/accrualPrincipalChangeReport.jsp','accrualPriChgServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('bwbty','哈尔滨银行本外币同业台账','','1','Ifs/report/TheForeignCurrency.jsp','theForeignCurrencyReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('cdfx','2020年黑龙江省法人金融机构同业存单发行情况统计表','','1','hrbReport/slCdBalanceReport.jsp','slCdBalanceServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('cktyfh','存款含同业-分行上传','','1','hrbReport/depositAndBorrowingBranchUploadReport.jsp','depAndBorrServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('cktyhz','存款含同业-汇总','','1','hrbReport/depositAndBorrowingSummarizingReport.jsp','depAndBorrServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('cktyjw','存款含同业-境外机构','','1','hrbReport/depositAndBorrowingForeInvestorReport.jsp','depAndBorrServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('csh','城商行/民营银行主要监管指标情况表','','0','','supervisionIndexServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('dffr','27. 附件1：地方法人金融机构月度监测表','','0','','legalMonitoringServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('dfzfz','地方政府债明细','','1','Ifs/report/LocalGovernmentBond.jsp','localGovernmentBondServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('dkcf','贷款拆放含拆放银行同业及联行资产表','','1','hrbReport/loanIncludeMoneyMarket.jsp','loanInSameServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('dldttz','拆借交易台账','','1','hrbReport/dldtLedgerReport.jsp','dldtReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('fdcjk','房地产融资风险监测表','','1','hrbReport/estateComReport.jsp','estateComSecServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g01','G01资产负债项目统计表','','0','','g01ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g1102','G11 第Ⅱ部分：资产质量及准备金 ','','0','','g1102ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g14','G14 大额风险暴露统计表','','0','','g1401ReportService.javaImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g14o','G14 大额风险暴露统计表-客户情况','','0','','g14OtherReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g14zt','G14台账明细-金融市场部','','1','Ifs/report/G14BookReport.jsp','g14BookReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g15xz','15.新债券投资情况表','','0','','newBondInvestReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g15zdsj','G15最大十家关联方关联交易情况表','','0','','g15ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g18dfzf','G18_III地方政府自主发行债券持有情况统计表','','0','','g18NewReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g21','G21流动性期限缺口统计表','','0','','g21NewReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g22','G22流动性比例监测表','','0','','g22ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g24','G24最大百家金额机构同业融入情况表','','0','','g24ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g2501','G25 第I部分 流动性覆盖率','','0','','g2501ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g2502','G25流动性覆盖率和净稳定资金比例情况表','','0','','g2502ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g31','G31有价证券及投资情况表','','0','','g31ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g331','G331利率重新定价风险情况报表第一部分：交易账户','','0','','g331ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g332','G332利率重新定价风险情况报表第二部分：银行账户','','0','','g332ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g42','G42表内加权风险资产计算表','','0','','g42ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('g52','G52地方政府融资平台及支出责任债务持有情况统计表','','0','','g52ReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('jyshg','交易所回购整理','','1','hrbReport/exchangeRepoReport.jsp','exchangeRepoServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('lrfdc','其他形式流入房地产领域得资金','','1','hrbReport/estSecPosReport.jsp','estSecPosServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('mrfszy','买入返售质押','','1','hrbReport/rvRepoPladgeReport.jsp','rvRepoPladgeServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('repotz','回购交易台账','','1','hrbReport/repoLedgerReport.jsp','repoReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('rmbtz','本币交易台账','','1','hrbReport/rmbReport.jsp','rmbReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('tpos','【含逾期】合并持仓分析报表','','1','hrbReport/tposReport.jsp','tposReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('tyxx','同业客户-基本信息表','','1','hrbReport/sameBasicInfoReport.jsp','sameBasicInfoServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('wzqy','外债签约','','1','Ifs/report/ForeDebtContractReport.jsp','foreDebtContractReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('wzye','外债余额','','1','hrbReport/foreDebtBalanceReport.jsp','foreDebtBalanceReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('xb1411','金融机构资产负债项目月报表','','0','','assetandLoanServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('yjty','【106号文-银监局】同业业务管理报告','','1','hrbReport/InBankDealReport.jsp','inBankDealServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('ywmx','202X.XX-业务明细-人行','','1','hrbReport/dealDetail.jsp','dealDetailServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('yzldzc','哈尔滨银行优质流动性资产台账','','1','hrbReport/curAssetReport.jsp','curAssetServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('zebb','中俄月报表','','0','','chinaRussiaReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('zfzfzsb','地方政府债情况报送','','0','Ifs/report/LocalGovernmentBondBill.jsp','localGovernmentBondBillServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('zjjy','自营资金交易信息表','','1','Ifs/report/CapitalTransaction.jsp','capitalTransactionServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('zjtc','哈尔滨银行资金头寸日报表','','0','','positionReportServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('zjyw','主要资产和负债业务周计划表','','1','Ifs/report/CapitalTurnover.jsp','capitalTurnoverServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('zyzjjy','自营资金交易信息表','','1','Ifs/report/PCapitalTransaction.jsp','pCapitalTransactionServiceImpl','1');
insert into ta_hrb_report (ID,REPORT_NMAE,REPORT_INST,REPORT_TYPE,MODULE_URL,REPORT_SERVER,ACTIVE_FLAG)
values('zyzjjyye','自营资金业务余额表','','1','Ifs/report/PCapitalTransactionAmt.jsp','pCapitalTransactionAmtServiceImpl','1');

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('pbfd', '人行金数', null, '1', 'hrbReport/PeopleBankReports/peopleBankFinaData.jsp', null, '1', null, null);
insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('imas', '人行利率报备', null, '1', 'hrbReport/PeopleBankReports/peopleBankIMAS.jsp', null, '1', null, null);
insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('g02', 'G02银行业金融机构衍生产品交易业务情况表', null, '1', 'hrbReport/g02Report.jsp', 'g02ReportServiceImpl', '1', 'g02.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('rmbSpot', '金融机构人民币外汇即期交易季报表', null, '1', 'hrbReport/rmbSpotSeasonReport.jsp', 'rmbSpotSeasonServiceImpl', '1', 'rmbSpot.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('sell2h', '原‘卖出兑付‘报表内容调整', null, '1', 'hrbReport/sellToHonorReport.jsp', 'sellToHonorServiceImpl', '1', 'sell2h.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('manamon', '累计经营月报', null, '1', 'hrbReport/manageMonthReport.jsp', 'manageMonthServiceImpl', '1', 'manamon.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('gainloss', '损益分析', null, '1', 'hrbReport/gainOrLossReport.jsp', 'gainOrLossServiceImpl', '1', 'gainloss.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('bonddeal', '现券买卖(内部)成交单', null, '1', 'hrbReport/bondTradingReportReport.jsp', 'bondTradingReportServiceImpl', '1', 'bonddeal.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('ibotra', '同业拆借业务交易情况统计表', null, '1', 'hrbReport/inBankiboTradingReport.jsp', 'inBankiboTradingServiceImpl', '1', 'ibotra.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('acctloss', '交易帐户浮亏止损限额表', null, '1', 'hrbReport/acctLossLimitReport.jsp', 'acctLossLimitServiceImpl', '1', 'acctloss.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('deallimit', '交易限额情况表', null, '1', 'hrbReport/dealLimitQuotaReport.jsp', 'dealLimitQuotaServiceImpl', '1', 'deallimit.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('pv01', '交易帐户组合PV01和组合久期限额情况表', null, '1', 'hrbReport/acctPV01Report.jsp', 'acctPV01ServiceImpl', '1', 'pv01.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('custcre', '同业授信报表', null, '1', 'hrbReport/bankCustCreditReport.jsp', 'bankCustCreditServiceImpl', '1', 'custcre.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('sectpos', '债券头寸情况表', null, '1', 'hrbReport/secTposReport.jsp', 'secTposServiceImpl', '1', 'sectpos.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('exposfxs', '外汇自营敞口情况', null, '1', 'hrbReport/exposureFxSelfReport.jsp', 'exposureFxSelfServiceImpl', '1', 'exposfxs.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('palfx', '外汇自营业务浮盈亏情况表', null, '1', 'hrbReport/profitAndLossFXReport.jsp', 'profitAndLossFXServiceImpl', '1', 'palfx.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('exposfx', '外汇敞口情况表', null, '1', 'hrbReport/exposureFxReport.jsp', 'exposureFxServiceImpl', '1', 'exposfx.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('ssth', '债券卖出兑付明细', null, '1', 'hrbReport/secSellToHonorReport.jsp', 'secSellToHonorServiceImpl', '1', 'ssth.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('rsrr', '利率特定风险计算表', null, '1', 'hrbReport/rateSpecialRiskReport.jsp', 'rateSpecialRiskServiceImpl', '1', 'rsrr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('rgrr', '一般利率风险-到期日法计算表', null, '1', 'hrbReport/rateGeneralRiskReport.jsp', 'rateGeneralRiskServiceImpl', '1', 'rgrr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('sagolr', '交易帐户债券业务已实现损益情况', null, '1', 'hrbReport/secAcctGainOrLossReport.jsp', 'secAcctGainOrLossServiceImpl', '1', 'sagolr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('spr', '债券投资结构表', null, '1', 'hrbReport/secPortReport.jsp', 'secPortServiceImpl', '1', 'spr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('sapr', '交易帐户债券投资结构情况', null, '1', 'hrbReport/secAcctPortReport.jsp', 'secAcctPortServiceImpl', '1', 'sapr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('dbcr', '本币交易情况表', null, '1', 'hrbReport/dealByCCYReport.jsp', 'dealByCCYServiceImpl', '1', 'dbcr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('dsflr', '外汇自营交易限额执行情况', null, '1', 'hrbReport/dealSelfFXLimitReport.jsp', 'dealSelfFXLimitServiceImpl', '1', 'dsflr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('sposr', '持有债券结构情况表', null, '1', 'hrbReport/secPositionReport.jsp', 'secPositionServiceImpl', '1', 'sposr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('swapdr', '利率互换业务明细表', null, '1', 'hrbReport/swapDealReport.jsp', 'swapDealServiceImpl', '1', 'swapdr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('pcr', '利润测算表', null, '1', 'hrbReport/profitCountReport.jsp', 'profitCountServiceImpl', '1', 'pcr.xml', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('crnr', 'Opics-新版客户风险需求', null, '1', 'hrbReport/custRiskNewReport.jsp', 'custRiskNewServiceImpl', '1', 'crnr.xml', null);


insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('creditcust', '客户额度', null, '1', '', 'hrbCreditCustServiceImpl', '1', 'creditcust.xls', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR)
values ('tradequery', '交易额度查询', null, '1', '', 'tdEdCustBatchStatusServiceImpl', '1', 'tradequery.xls', null);

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR, REPORT_ROLE, REPORT_CODE)
values ('g1405report', 'G1405-大额风险暴露统计表', null, '1', 'hrbReport/ifsReportG1405.jsp', 'ifsReportG1405ServiceImpl', '1', 'g1405.xls', null, null, 'Repoet-142');

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR, REPORT_ROLE, REPORT_CODE)
values ('g1406', 'G1406-大额风险暴露统计表', null, '1', 'hrbReport/ifsReportG1406.jsp', 'ifsReportG1406ServiceImpl', '1', 'g1406.xls', null, null, 'Repoet-143');

insert into ta_hrb_report (ID, REPORT_NMAE, REPORT_INST, REPORT_TYPE, MODULE_URL, REPORT_SERVER, ACTIVE_FLAG, EXECL_NAME, EXECL_DIR, REPORT_ROLE, REPORT_CODE)
values ('xefx', '限额预风险', null, '0', 'hrbReport/LimitRisk.jsp', 'limitRiskServiceImpl', '0', 'WEB-INF/classes/template/hrbreport/xefx.xls', null, null, 'Repoet-2');


insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2000850', '中华人民共和国财政部', '8000000001 ', '财政部', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4J2816CE8A', '江苏省人民政府', '8000001827 ', '江苏省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'fFJyb7OEI9', '甘肃省公路航空旅游投资集团有限公司', '8000001435 ', '甘肃省公路航空旅游投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hD62FE935', '三一集团有限公司', '8000000119 ', '三一集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2008251', '巨化集团有限公司', '8000001488 ', '巨化集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502040967', '中国铝业公司', '8000001533 ', '中国铝业公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2N9DDEE0DF', '光明食品(集团)有限公司', '8000001710 ', '光明食品集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0486B09311', '锦州银行股份有限公司', '8000001370 ', '锦州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'MHq1HMD5M3', '辽阳农村商业银行股份有限公司', '8000003134 ', '辽阳农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2026885', '海宁市资产经营公司', '8000000180 ', '海宁市资产经营公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502039149', '北京银行股份有限公司', '8000000009 ', '北京银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0436BE9E81', '成都银行股份有限公司', '8000001356 ', '成都银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hG81445F0', '山西省人民政府', '8000003741 ', '山西省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'hCqVOaESpx', '中国工艺集团有限公司', '8000001558 ', '中国工艺（集团）公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PE802FDF2', '重庆银行股份有限公司', '8000001426 ', '重庆银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502038506', '乌鲁木齐银行股份有限公司', '8000001369 ', '乌鲁木齐银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04D884D908', '恒丰银行股份有限公司', '8000001367 ', '恒丰银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04M9918AA9', '中信建投证券股份有限公司', '8000000089 ', '中信建投证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF3E93744', '天津滨海农村商业银行股份有限公司', '8000001437 ', '天津滨海农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'iwL7bTLILI', '张家口通泰控股集团有限公司', '8000001342 ', '张家口通泰控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hQ9993FE7', '四川省人民政府', '8000001826 ', '四川省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SFDB97719', '青岛国信实业有限公司', '8000000158 ', '青岛国信实业有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '752813653', '成都东方广益投资有限公司', '800003934  ', '成都东方广益投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF4E043EB', '广汇汽车服务有限责任公司', '8000000141 ', '广汇汽车服务股份公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'F42B8', '九州通医药集团股份有限公司', '8000001511 ', '德邦德利货币市场基金', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0KR85BDBE1', '西藏自治区人民政府', '8000003840 ', '西藏自治区政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'aTakIns9yl', '北京兴展投资控股有限公司', '8000000177 ', '北京兴展国有资产经营公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002178', '中国银行股份有限公司', '8000001345 ', '中国银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'NYqiXLbgqn', '大庆市城市建设投资开发有限公司', '8000003736 ', '大庆市城市建设投资开发有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'QDObHxkpSX', '重庆市涪陵国有资产投资经营集团有限公司', '8000001623 ', '重庆市涪陵国有资产投资经营集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2020372', '江苏汇鸿国际集团有限公司', '8000001414 ', '江苏汇鸿国际集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'NTcNXWC5Zi', '江苏华东有色投资控股有限公司', '8000000118 ', '江苏华东有色投资控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '484989900C', '伊春市城市建设投资开发有限责任公司', '8000000163 ', '伊春市城市建设投资开发有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000966', '国电长源电力股份有限公司', '8000001546 ', '国电长源电力股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3IPB05761F', '江苏银行股份有限公司', '8000001374 ', '江苏银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16H05DEA7F', '青岛市人民政府', '8000004161 ', '青岛市人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'jYtVwlaP88', '新沂经济开发区建设发展有限公司', '8000002973 ', '新沂经济开发区建设发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4JR81A687D', '福建省人民政府', '8000003965 ', '福建省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1H64563A1A', '山东省国有资产投资控股有限公司', '8000001607 ', '山东省国有资产投资控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF4DE003E', '北京大北农科技集团股份有限公司', '8000001410 ', '北京大北农科技集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1fcazfLASx', '中国中信有限公司', '8000000151 ', '中国中信集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04T4E6EBA9', '郑州银行股份有限公司', '8000000844 ', '郑州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08Q898C166', '中国华电集团公司', '8000001344 ', '中国华电集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF4DDFE3B', '大全集团有限公司', '8000000138 ', '大全集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'GvSoc2mHsQ', '双鸭山市大地城市建设开发投资有限公司', '8000003160 ', '双鸭山市大地城市建设开发投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '15BD8C', '河南省人民政府', '8000003836 ', '河南省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '044A60D6B0', '渤海银行股份有限公司', '8000000013 ', '渤海银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '标普信用评级(中国)有限公司', '标普信用评级(中国)有限公司', '1', '标普信用评级(中国)有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '穆迪公司', '穆迪公司', '2', '穆迪公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '惠誉国际信用评级有限公司', '惠誉国际信用评级有限公司', '3', '惠誉国际信用评级有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '大公国际资信评估有限公司', '大公国际资信评估有限公司', '4', '大公国际资信评估有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '东方金诚国际信用评估有限公司', '东方金诚国际信用评估有限公司', '5', '东方金诚国际信用评估有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '联合资信评估股份有限公司', '联合资信评估股份有限公司', '6', '联合资信评估股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '联合资信评估有限公司', '联合资信评估有限公司', '6', '联合资信评估股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '联合信用评级有限公司', '联合信用评级有限公司', '6', '联合资信评估股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '上海新世纪资信评估投资服务有限公司', '上海新世纪资信评估投资服务有限公司', '7', '上海新世纪资信评估投资服务有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '中国诚信信用管理股份有限公司', '中国诚信信用管理股份有限公司', '8', '中国诚信信用管理股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '鹏元资信评估有限公司', '鹏元资信评估有限公司', '9', '中证鹏元资信评估股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '中证鹏元资信评估股份有限公司', '中证鹏元资信评估股份有限公司', '9', '中证鹏元资信评估股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '中债资信评估有限责任公司', '中债资信评估有限责任公司', '10', '中债资信评估有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '远东资信评估有限公司', '远东资信评估有限公司', '11', '远东资信评估有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '中诚信国际信用评级有限责任公司', '中诚信国际信用评级有限责任公司', '12', '中诚信国际信用评级有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '贵州博远信用管理评估有限公司', '贵州博远信用管理评估有限公司', '13', '贵州博远信用管理评估有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('OUTCOMPANY', 'TYPE-TYPE', '中诚信证券评估有限公司', '中诚信证券评估有限公司', '14', '中诚信证券评估有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'AAA', 'AAA', '02', 'AAA', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'AA+', 'AA+', '03', 'AA+', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'AA', 'AA', '04', 'AA', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'AA-', 'AA-', '05', 'AA-', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'A+', 'A+', '06', 'A+', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'A', 'A', '07', 'A', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'A-', 'A-', '08', 'A-', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'A2', 'A2', '24', 'A2', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'BBB+', 'BBB+', '10', 'BBB+', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'BBB', 'BBB', '11', 'BBB', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'BBB-', 'BBB-', '12', 'BBB-', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'BB+', 'BB+', '15', 'BB+', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'BB', 'BB', '16', 'BB', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'BB-', 'BB-', '17', 'BB-', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'B+', 'B+', '18', 'B+', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'B', 'B', '19', 'B', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'B-', 'B-', '20', 'B-', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'CCC', 'CCC', '21', 'CCC', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'CC', 'CC', '22', 'CC', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('RATING', 'TYPE-TYPE', 'C', 'AAA', '23', 'C', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600518', '康美药业股份有限公司', '8000001458 ', '康美药业股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'ktQ76bEuBR', '红星美凯龙家居集团股份有限公司', '8000000133 ', '红星美凯龙家居集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'wgfrOB3VBW', '无锡灵山文化旅游集团有限公司', '8000001451 ', '无锡灵山文化旅游集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '6LvlsbbckL', '中航发动机控股有限公司', '8000001574 ', '中航发动机控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502038477', '齐鲁银行股份有限公司', '8000000719 ', '齐鲁银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1dza2ZYWjK', '重庆市铜梁区金龙城市建设投资(集团)有限公司', '8000003105 ', '重庆市铜梁区金龙城市建设投资（集团）有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502036503', '中信国安集团有限公司', '8000003701 ', '中信国安集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'P75E82FA', '天津市西青经济开发总公司', '8000004702 ', '天津市西青经济开发总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2020987', '上海华谊(集团)公司', '8000001561 ', '上海华谊（集团）公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'yHZlUlZab2', '江苏新扬子造船有限公司', '8000000126 ', '江苏新扬子造船有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0DM8A7BDC4', '富滇银行股份有限公司', '8000001395 ', '富滇银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3xSC2A9A36', '重庆渝垫国有资产经营有限责任公司', '8000000185 ', '重庆渝垫国有资产经营有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'BG9JBEdBe8', '冀银金融租赁股份有限公司', '8000008789 ', '冀银金融租赁股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002233', '中国长江三峡集团有限公司', '8000000153 ', '中国长江三峡工程开发总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1683C9F299', '天津城市基础设施建设投资集团有限公司', '8000000157 ', '天津城市基础设施建设投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1uI61F44C3', '东兴证券股份有限公司', '8000001385 ', '东兴证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'BJgKTVUjnV', '新疆维吾尔自治区人民政府', '8000003661 ', '新疆维吾尔自治区政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04F4FD7BFC', '宁波鄞州农村商业银行股份有限公司', '8000001567 ', '宁波鄞州农村合作银行', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'ky1Y0SdzHz', '哈银金融租赁有限责任公司', '8000003616 ', '哈银金融租赁有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600000', '上海浦东发展银行股份有限公司', '8000000135 ', '上海浦东发展银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1AD501EDED', '电信科学技术研究院', '8000000087 ', '电信科学技术研究院', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2011458', '武汉国有资产经营有限公司', '8000001543 ', '武汉国有资产经营公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hRA51FB72', '贵州省人民政府', '8000003716 ', '贵州省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0D73AF3A4A', '中国南方航空股份有限公司', '8000001711 ', '中国南方航空股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '15F577', '遂宁发展投资集团有限公司', '8000003981 ', '遂宁发展投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0LEB921B54', '天津市人民政府', '8000003879 ', '天津市人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502038032', '上海银行股份有限公司', '8000000154 ', '上海银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0FU4FB0008', '上饶银行股份有限公司', '8000001600 ', '上饶银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'qzc6agrEHE', '中国铁路物资股份有限公司', '8000001433 ', '中国铁路物资股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'mb9aNeMhQC', '成都经开产业投资集团有限公司', '8000004060 ', '成都经济技术开发区建设发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600509', '新疆天富能源股份有限公司', '8000000156 ', '新疆天富热电股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4WG5B46624', '张家港市直属公有资产经营有限公司', '8000001434 ', '张家港市直属公有资产经营有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16G3B2175F', '中国中材集团有限公司', '8000001405 ', '中国中材集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04F5A88E54', '财通证券股份有限公司', '8000001466 ', '财通证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000860', '北京顺鑫农业股份有限公司', '8000001461 ', '北京顺鑫农业发展集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502041190', '中银国际证券有限责任公司', '8000001361 ', '中银国际证券', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '689024757', '四川成阿发展实业有限公司', '8000003029 ', '四川成阿发展实业有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3v961DA2B8', '天津市北辰区建设开发公司', '8000004738 ', '天津市北辰区建设开发公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3IT9CE6E72', '岳阳市城市建设投资有限公司', '8000000166 ', '岳阳市城市建设投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SR02012B1', '泰格林纸集团股份有限公司', '8000000159 ', '湖南泰格林纸集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08D39A0C84', '三一重工股份有限公司', '8000000119 ', '三一集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'IPWb5u9jpG', '贵阳观城产业建设投资发展有限公司', '8000003069 ', '贵阳观城产业建设投资发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600016', '中国民生银行股份有限公司', '8000000221 ', '中国民生银行股份公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3BA6B0C0B1', '天津生态城投资开发有限公司', '8000004712 ', '天津生态城投资开发有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16PEBB1A49', '陕西省高速公路建设集团公司', '8000001436 ', '陕西省高速公路建设集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '07R99F739B', '江苏南通农村商业银行股份有限公司', '8000001630 ', '江苏南通农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08S9422A03', '重庆三峡银行股份有限公司', '8000001974 ', '重庆三峡银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4UO92E0C27', '天津保税区投资控股集团有限公司', '8000000112 ', '天津保税区投资控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'Qcfa20RVbx', '鹤岗市开源城市投资开发有限责任公司', '8000000175 ', '鹤岗市开源城市投资开发有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0FM900C66C', '浙江泰隆商业银行股份有限公司', '8000001903 ', '浙江泰隆商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'JzJVavrYqq', '成都新开元城市建设投资有限公司', '8000004716 ', '成都新开元城市建设投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502039388', '宁波银行股份有限公司', '8000000395 ', '宁波银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007607', '上海仪电(集团)有限公司', '8000000122 ', '上海仪电控股（集团）公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hS7DE4A10', '湖北省人民政府', '8000003793 ', '湖北省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502038385', '福建海峡银行股份有限公司', '8000001569 ', '福建海峡银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'F857F', '黑龙江省建设集团有限公司', '8000001730 ', '黑龙江省建设集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000009', '中国宝安集团股份有限公司', '8000000109 ', '中国宝安集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3IQ91D4ECF', '绍兴市交通投资集团有限公司', '8000000179 ', '绍兴市交通投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007907', '首钢集团有限公司', '8000001549 ', '首钢总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0425780707', '中铁信托有限责任公司', '8000001608 ', '中铁信托有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0x986C414E', '广东省人民政府', '8000001825 ', '广东省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502037343', '中信银行股份有限公司', '8000001354 ', '中信银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000027', '深圳能源集团股份有限公司', '8000000068 ', '深圳能源集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '600404', '红豆集团有限公司', '8000000137 ', '红豆集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2026874', '龙源电力集团股份有限公司', '8000001536 ', '龙源电力集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2006608', '河北银行股份有限公司', '8000001444 ', '河北银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0M77D1109A', '长沙银行股份有限公司', '8000001601 ', '长沙银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SA4G00111', '杭州联合农村商业银行股份有限公司', '8000001616 ', '杭州联合农村商业银行', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'oDTPW76PwW', '华融湘江银行股份有限公司', '8000001353 ', '华融湘江银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600713', '南京医药股份有限公司', '8000001565 ', '南京医药股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '6lJ0mUWHWb', '黑龙江省人民政府', '8000001620 ', '黑龙江省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A4E36D9A', '华商银行', '8000001425 ', '华商银行', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002209', '中国国家铁路集团有限公司', '8000000182 ', '中国铁路总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2o251BE817', '中国建筑股份有限公司', '8000001431 ', '中国建筑股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PI9B3200E', '淮南矿业(集团)有限责任公司', '8000001576 ', '淮南矿业（集团）有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04F99A4755', '山东省人民政府', '8000001333 ', '山东省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PI470183C', '陕西煤业化工集团有限责任公司', '8000000111 ', '陕西煤业化工集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'F6F37', '吉视传媒股份有限公司', '8000000105 ', '吉视传媒股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04J5B4A204', '辽宁省人民政府', '8000002994 ', '辽宁省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'h7dmHN4CGO', '厦门轨道交通集团有限公司', '8000003740 ', '厦门轨道交通集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '11B250', '云南白药控股有限公司', '8000001579 ', '云南白药控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0K83DF0200', '联想控股股份有限公司', '8000001428 ', '联想控股股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A57793C9', '九江银行股份有限公司', '8000001739 ', '九江银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002176', '中国工商银行股份有限公司', '8000000055 ', '中国工商银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007994', '深圳华强集团有限公司', '8000000127 ', '深圳华强集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0438094B1B', '温州银行股份有限公司', '8000001351 ', '温州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2027077', '云南省人民政府', '8000003861 ', '云南省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'XUiVDvVOH1', '大连市城乡建设投资集团有限公司', '8000008801 ', '大连市城乡建设投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'G9asDjAwnb', '浙江省人民政府', '8000003750 ', '浙江省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0S65FAC969', '青海省国有资产投资管理有限公司', '8000001606 ', '青海省国有资产投资管理有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'F609C', '中电建水电开发集团有限公司', '8000000142 ', '中国水电建设集团四川电力开发有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2017036', '三环集团有限公司', '8000001517 ', '三环集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'mLyFncue9e', '青岛农村商业银行股份有限公司', '8000001399 ', '青岛农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002174', '交通银行股份有限公司', '8000000057 ', '交通银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0MA8990ABB', '广州万宝集团有限公司', '8000001440 ', '广州万宝集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16N5FCC2BB', '潍坊银行股份有限公司', '8000001975 ', '潍坊银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'XOWNXyKJam', '重庆万州经济技术开发(集团)有限公司', '8000004673 ', '重庆万州经济技术开发(集团)有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600588', '用友网络科技股份有限公司', '8000000146 ', '用友软件股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2024606', '国联证券股份有限公司', '8000001571 ', '国联证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SE879D79B', '厦门银行股份有限公司', '8000001363 ', '厦门银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'w8LpeVi5q6', '保利能源控股有限公司', '8000001701 ', '保利能源控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0P37CC0504', '赣州银行股份有限公司', '8000000035 ', '赣州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SK3CF860A', '北京顺鑫控股集团有限公司', '8000001461 ', '北京顺鑫农业发展集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0xP590719E', '中国重型汽车集团有限公司', '8000001580 ', '中国重型汽车集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '602213', '北京首都旅游集团有限责任公司', '8000001854 ', '北京首都旅游集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002219', '大连市建设投资集团有限公司', '8000004681 ', '大连市建设投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'TQAV3iDS9V', '四川仁寿视高天府投资有限公司', '8000008803 ', '四川仁寿视高天府投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502030478', '西部证券股份有限公司', '8000001450 ', '西部证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502044702', '金华银行股份有限公司', '8000000395 ', '宁波银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'kNOrDfdhpZ', '天津滨海旅游区投资控股有限公司', '8000008808 ', '天津滨海旅游区投资控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4828ACF46C', '丹阳投资集团有限公司', '8000000168 ', '丹阳市城建交通投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2012219', '广西柳工集团有限公司', '8000000078 ', '广西柳工集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'XFWK4aBKP6', '重庆西永微电子产业园区开发有限公司', '8000001598 ', '重庆西永微电子产业园区开发有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A57782C5', '广西北部湾银行股份有限公司', '8000002186 ', '广西北部湾银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '687726050', '天津市静海城市基础设施建设投资集团有限公司', '8000008802 ', '天津市静海城市基础设施建设投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002700', '国家开发银行', '8000000098 ', '国家开发银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'rHk2wrs4Yf', '江苏飞达控股集团有限公司', '8000000176 ', '江苏飞达控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '683387418', '长春润德投资集团有限公司', '8000008804 ', '长春润德投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04M5F620A3', '中国农业发展银行', '8000000099 ', '中国农业发展银行', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T01', '央行票据', 'SE-YP', '央行票据', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T02', '商业银行债券', 'SE-SYYHZ', '商业银行债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T03', '证券公司债', 'SE-ZQGSZ', '证券公司债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T04', '非银行金融机构债', 'SE-QTHRJGZ', '其他金融机构债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T05', '储蓄国债', 'SE-GZ', '国债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T06', '记账式国债', 'SE-GZ', '国债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T07', '地方政府债', 'SE-DFZFZ', '地方政府债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T08', '地方企业债', 'SE-QYZ', '企业债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T09', '中期票据', 'SE-YP', '央行票据', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T10', '集合票据', 'SE-YP', '央行票据', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T11', '集合企业债', 'SE-QYZ', '企业债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T12', '短期融资券', 'SE-DR', '一般短期融资券', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T13', '超短期融资券', 'SE-CDR', '超短期融资券', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T14', '资产支持证券', null, null, null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T15', '政府支持机构债', 'SE-ZFZCJGZ', '政府支持机构债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T16', '政策性银行债券', 'SE-ZCXYHZ', '政策性银行债', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T17', '国际机构债券', null, null, null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('BONDPRO', 'TYPE-TYPE', 'T18', '其他', null, null, null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502039214', '中国普天信息产业股份有限公司', '8000001564 ', '中国普天信息产业股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '771208045', '成都市蜀州城市建设投资有限责任公司', '8000008677 ', '成都市蜀州城市建设投资有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '75005362X', '重庆市长寿生态旅业开发集团有限公司', '8000004706 ', '重庆市长寿生态旅业开发集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'pkjsPdA4nq', '锦州市城市建设投资发展(集团)有限公司', '8000001402 ', '锦州市城市建设投资发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PJ454596C', '长春城市开发(集团)有限公司', '8000004125 ', '长春城市开发(集团)有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2004450', '长江经济联合发展(集团)股份有限公司', '8000000106 ', '长江经济联合发展（集团）股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502038495', '汉口银行股份有限公司', '8000000101 ', '汉口银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002075', '国信证券股份有限公司', '8000000054 ', '国信证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2015450', '四川高速公路建设开发总公司', '8000001575 ', '四川高速公路建投开发总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'e55gsxIfmP', '厦门市人民政府', '8000003894 ', '厦门市人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3cP6A5F800', '龙江银行股份有限公司', '8000000024 ', '龙江银行股分有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2020299', '浙江英特药业有限责任公司', '8000000077 ', '浙江英特药业有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16U524A4E5', '天津保税区投资有限公司', '8000000112 ', '天津保税区投资控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16L080053F', '晨鸣控股有限公司', '8000000173 ', '寿光晨鸣控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600122', '江苏宏图高科技股份有限公司', '8000000144 ', '江苏宏图高科技股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SP880CBB5', '长安银行股份有限公司', '8000000003 ', '长安银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '759267085', '重庆市南川区城市建设投资(集团)有限公司', '8000004440 ', '重庆市南川区城市建设投资（集团）有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3ISA4157BE', '桐乡市城市建设投资有限公司', '8000001595 ', '桐乡市城市建设投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF4DA7077', '北京昊华能源股份有限公司', '8000001535 ', '北京昊华能源股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '07U6A73FDC', '江苏常熟农村商业银行股份有限公司', '8000002051 ', '江苏常熟农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF3DBF707', '浙江吉利控股集团有限公司', '8000000174 ', '浙江吉利控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2013708', '兴业银行股份有限公司', '8000001376 ', '兴业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2011313', '国家开发投资集团有限公司', '8000001570 ', '国家开发投资公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600894', '广州广日股份有限公司', '8000000110 ', '广州广日股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'yBJQObb1ZA', '本溪市城市建设投资发展有限公司', '8000003146 ', '本溪市城市建设投资发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'G4RQm24XOk', '成都市新益州城市建设发展有限公司', '8000003627 ', '成都市新益州城市建设发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08C399ED78', '方大炭素新材料科技股份有限公司', '8000000147 ', '方大炭素新材料科技股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'u0qxYnp8Kk', '南京雨润食品有限公司', '8000000140 ', '南京雨润食品有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502039907', '富邦华一银行有限公司', '8000001481 ', '富邦华一银行有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04L477480E', '江苏省国信资产管理集团有限公司', '8000001409 ', '江苏省国信资产管理集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0S54323D9A', '国家电力投资集团有限公司', '8000000149 ', '中国电力投资集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16D434D713', '金光纸业(中国)投资有限公司', '8000000116 ', '金光纸业（中国）投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'qVmfgfScpD', '中国证券金融股份有限公司', '8000000088 ', '中国证券金融股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0gF69038FD', '天津高速公路集团有限公司', '8000001602 ', '天津高速公路集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2KJA3F5BC1', '北京信威通信技术股份有限公司', '8000001717 ', '北京信威通信技术股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'BhXuQhKEIj', '现代牧业(集团)有限公司', '8000000073 ', '现代牧业（集团）有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000701', '厦门信达股份有限公司', '8000001621 ', '厦门信达股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0F27CA9D8B', '苏州银行股份有限公司', '8000001453 ', '苏州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04DA4A6D76', '浙江恒逸集团有限公司', '8000001622 ', '浙江恒逸集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502036579', '国都证券股份有限公司', '8000001463 ', '国都证券有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A5776654', '营口银行股份有限公司', '8000001789 ', '营口银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16O13B75C9', '黑龙江省鹤城建设投资发展集团有限公司', '8000002116 ', '黑龙江省鹤城建设投资发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4JR7DE7B51', '大连德泰控股有限公司', '8000004701 ', '大连德泰控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'huuIVAo6er', '襄阳市住房投资有限公司', '8000003067 ', '襄阳市住房投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0MT64BBFB6', '中国进出口银行', '8000000100 ', '中国进出口银行', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'O4q4clxWA4', '陕西省交通建设集团公司', '8000001467 ', '陕西省交通建设集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502045906', '包商银行股份有限公司', '8000001352 ', '包商银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'FCB72', '黑龙江龙煤矿业控股集团有限责任公司', '8000001665 ', '黑龙江龙煤矿业控股集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A57767C7', '辽阳银行股份有限公司', '8000001803 ', '辽阳银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '28H4D48E21', '张家口银行股份有限公司', '8000001833 ', '张家口银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'C6410D75', '安徽新华发行(集团)控股有限公司', '8000001603 ', '安徽新华发行（集团）控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0bK7EB27E0', '新疆生产建设兵团国有资产经营有限责任公司', '8000000083 ', '新疆生产建设兵团国有资产经营公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '48UA33189C', '平湖市城市发展投资(集团)有限公司', '8000000170 ', '平湖市城市发展投资（集团）有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0g357B19C5', '大连市人民政府', '8000003774 ', '大连市人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A57605C8', '四川天府银行股份有限公司', '8000001486 ', '南昌银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hAB921545', '宁夏回族自治区人民政府', '8000003792 ', '宁夏回族自治区人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002157', '瑞银证券有限责任公司', '8000001418 ', '瑞银证券有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '29O7F8E349', '淄博市城市资产运营有限公司', '8000000164 ', '淄博市城市资产运营有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600811', '东方集团股份有限公司', '8000001537 ', '东方集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'uv748uSFaN', '青海省人民政府', '800003835  ', '青海省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SC5A448B1', '四川省盐业总公司', '8000001551 ', '庞大汽贸集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'kofYJ4Sfq7', '重庆三峡产业投资有限公司', '8000004724 ', '重庆三峡产业投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'cb9XLPnazA', '河南众品食业股份有限公司', '8000000085 ', '河南众品食业股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0DR4F7BC7E', '北京市首都公路发展集团有限公司', '8000001528 ', '北京市首都公路发展集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3c3A5AD079', '中国旅游集团有限公司', '8000001559 ', '中国港中旅集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A578DDCD', '武汉农村商业银行股份有限公司', '8000002131 ', '武汉农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'FJD6HDDZww', '天津新技术产业园区武清开发区总公司', '8000004050 ', '天津新技术产业园区武清开发区总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '28Q5AA30FC', '广东顺德农村商业银行股份有限公司', '8000001424 ', '广东顺德农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000826', '启迪桑德环境资源股份有限公司', '8000000075 ', '桑德环境资源股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'jYVfFTjjKY', '河南省中小企业', '8000000172 ', '河南省中小企业服务局', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600100', '同方股份有限公司', '8000001465 ', '同方股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08544634FD', 'TCL集团股份有限公司', '8000001550 ', 'TCL集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hAA3D9C44', '重庆市人民政府', '8000003755 ', '重庆市政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0yA47613A2', '传化集团有限公司', '8000000131 ', '传化集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'opKub8M1Oa', '协鑫智慧能源(苏州)有限公司', '8000000130 ', '保利协鑫有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002081', '长城证券股份有限公司', '8000000094 ', '长城证券有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2024267', '广发银行股份有限公司', '8000001203 ', '广发银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'cyBjfRuof8', '天津市武清区国有资产经营投资公司', '8000004642 ', '天津市武清区国有资产经营投资公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '48V9D8CF1F', '湘潭市城市建设投资经营有限责任公司', '8000000167 ', '湘潭市城市建设投资经营有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'tin5fIdvRm', '衢州市国有资产经营有限公司', '8000001605 ', '衢州市国有资产经营有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2029290', '内蒙古鄂尔多斯羊绒集团有限责任公司', '8000000121 ', '内蒙古鄂尔多斯羊绒集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '552032764', '重庆两山建设投资有限公司', '8000002989 ', '重庆两山建设投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'wRxrpy5lGW', '中国林业集团有限公司', '8000003772 ', '中国林业集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A571E5E6', '上海农村商业银行股份有限公司', '8000001387 ', '上海农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2015743', '中融国际信托有限公司', '8000001584 ', '中融国际信托有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PH6B96491', '广州农村商业银行股份有限公司', '8000001391 ', '广州农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '202753510', '都江堰新城建设投资有限责任公司', '8000003605 ', '都江堰新城建设投资有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'VeomRMmi9a', '重庆市南川区惠农文化旅游发展集团有限公司', '8000008805 ', '重庆市南川区惠农文化旅游发展集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2NG469A325', '安顺市国有资产管理有限公司', '8000000171 ', '安顺市国有资产管理有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0LS24E9113', '法尔胜泓昇集团有限公司', '8000000139 ', '江苏法尔胜泓昇集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2009616', '中国南山开发(集团)股份有限公司', '8000000072 ', '中国南山开发（集团）股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0437E7D485', '大连银行股份有限公司', '8000000023 ', '大连银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PF4511D33', '上海世博土地控股有限公司', '8000000155 ', '上海世博土地控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0L789EAD9F', '徽商银行股份有限公司', '8000001400 ', '徽商银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '45280FA7DE', '南京中电熊猫信息产业集团有限公司', '8000000124 ', '南京中电熊猫信息产业集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0486D9551E', '盛京银行股份有限公司', '8000000165 ', '盛京银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '15H51A3E31', '中国邮政储蓄银行股份有限公司', '8000001408 ', '中国邮政储蓄银行', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2NA02229D5', '厦门象屿集团有限公司', '8000001562 ', '厦门象屿集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04D6AF5912', '浙商银行股份有限公司', '8000000217 ', '浙商银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16981DE001', '北京金隅股份有限公司', '8000000070 ', '北京金隅股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002077', '华泰证券股份有限公司', '8000000092 ', '华泰证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '721384235', '牡丹江市城市投资集团有限公司', '8000001777 ', '牡丹江市城市投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04R6BE23E6', '阳泉煤业(集团)有限责任公司', '8000001703 ', '阳泉煤业(集团)有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'lqhWzXobnG', '成都临江田园园林有限公司', '8000003733 ', '成都临江田园园林有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08D39EC855', '中信证券股份有限公司', '8000000152 ', '中信证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04994570F2', '天津农村商业银行股份有限公司', '8000001828 ', '天津农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hP9962037', '内蒙古自治区人民政府', '8000001855 ', '内蒙古自治区人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502044367', '青海银行股份有限公司', '8000002021 ', '青海银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3ECYAeuQNu', '四川阳安交通投资有限公司', '8000004583 ', '四川阳安交通投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007564', '湖南省高速公路建设开发总公司', '8000001403 ', '湖南省高速公路建设开发总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'bol9fPjfff', '眉山岷东开发投资有限公司', '8000004695 ', '眉山岷东开发投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A5778AC0', '廊坊银行股份有限公司', '8000001398 ', '廊坊银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A5778076', '广东南粤银行股份有限公司', '8000001407 ', '广东南粤银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007627', '华侨城集团公司', '8000000113 ', '华侨城集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PM5B68EE9', '金川集团股份有限公司', '8000000145 ', '金川集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0S45FDEF7F', '华融证券股份有限公司', '8000001377 ', '华融证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '157307', '西王集团有限公司', '8000000071 ', '西王集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000993', '福建闽东电力股份有限公司', '8000000086 ', '福建闽东电力股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502042173', '安徽省交通控股集团有限公司', '8000001499 ', '安徽省高速公路控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0FL5F82BEC', '昆仑银行股份有限公司', '8000001383 ', '昆仑银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4W39E3CA00', '中国邮电器材集团公司', '8000001415 ', '中国邮电器材集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0FN7ECAE10', '青岛银行股份有限公司', '8000001624 ', '青岛银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16VB1210B9', '新希望集团有限公司', '8000001339 ', '新希望集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '6X2IXEqS0K', '北京京能新能源有限公司', '8000000129 ', '北京京能新能源有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '758771087', '成都市西汇投资有限公司', '8000004676 ', '成都市西汇投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2017301', '天津钢管集团股份有限公司', '8000001538 ', '天津钢管集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04863CDE01', '广州银行股份有限公司', '8000000942 ', '广州银行', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'eUhFhgz4HU', '中原银行股份有限公司', '8000001851 ', '中原银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'NJ0wiSbyq3', '超威电源有限公司', '8000000067 ', '超威电源有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0I95AE5176', '浙江省铁路投资集团有限公司', '8000001566 ', '浙江省铁路投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0437FB89D0', '杭州银行股份有限公司', '8000000562 ', '杭州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '485A535EF0', '镇江城市建设产业集团有限公司', '8000000181 ', '镇江市城市建设投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002175', '中国建设银行股份有限公司', '8000000084 ', '中国建设银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000898', '鞍钢股份有限公司', '8000000148 ', '鞍钢股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08F7E6ABFD', '重庆机电控股(集团)公司', '8000000107 ', '重庆机电控股（集团）公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600256', '广汇能源股份有限公司', '8000001452 ', '广汇能源股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'CNJlaa3kip', '万达集团股份有限公司', '8000001500 ', '万达集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PN7664A78', '中远海运控股股份有限公司', '8000001429 ', '中国远洋控股股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502039354', '天津银行股份有限公司', '8000000178 ', '天津银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08M399B152', '华夏银行股份有限公司', '8000000076 ', '华夏银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'Y3e4l1R7zz', '新誉集团有限公司', '8000000103 ', '新誉集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3IL63F90C6', '广东粤海控股集团有限公司', '8000001404 ', '广东粤海控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04L571BC31', '金元证券股份有限公司', '8000001504 ', '金元证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '15D070', '江河创建集团股份有限公司', '8000003748 ', '江河创建集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0F26AC4E10', '内蒙古银行股份有限公司', '8000001802 ', '内蒙古银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16E40E9D7F', '深圳茂业商厦有限公司', '8000001459 ', '深证茂业商厦有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002098', '第一创业证券股份有限公司', '8000000027 ', '第一创业证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0479264552', '吉林银行股份有限公司', '8000001518 ', '吉林银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'zcWfsrE2V6', '河北省人民政府', '8000003767 ', '河北省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'alySwvIfZN', '大连农村商业银行股份有限公司', '8000001668 ', '大连农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF3CC9E78', '重庆农村商业银行股份有限公司', '8000001698 ', '重庆农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502038360', '南京银行股份有限公司', '8000000056 ', '南京银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0h45872E23', '深圳市人民政府', '8000004636 ', '深圳市人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2WD5B15F50', '东莞玖龙纸业有限公司', '8000000125 ', '东莞玖龙纸业有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000001', '平安银行股份有限公司', '8000000845 ', '平安银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '602406', '华安证券股份有限公司', '8000001507 ', '华安证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'DTC2YwvdWe', '陕西省人民政府', '8000001886 ', '陕西省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16S5800E63', '甘肃银行股份有限公司', '8000002069 ', '甘肃银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0435A0E03A', '兴业国际信托有限公司', '8000001370 ', '锦州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SO89A93EE', '朝阳银行股份有限公司', '8000001637 ', '朝阳银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hS51AE84B', '广西壮族自治区人民政府', '8000003681 ', '广西壮族自治区政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4JG629FE8B', '天津临港投资控股有限公司', '8000004720 ', '天津临港投资控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3vD86E5DD6', '深圳市地铁集团有限公司', '8000000064 ', '深圳市地铁集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'QYX6JmpzR2', '庞大汽贸集团股份有限公司', '8000001552 ', '四川省盐业总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0F24ECA058', '泉州银行股份有限公司', '8000001506 ', '泉州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '684880626', '牡丹江龙盛投资有限公司', '8000003775 ', '牡丹江龙盛投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '15BD8D', '河南交通投资集团有限公司', '8000001468 ', '河南交通投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04564566BB', '安徽国元信托有限责任公司', '8000000395 ', '宁波银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '745323568', '重庆市德感工业园区建设有限公司', '8000008788 ', '重庆市德感工业园区建设有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '103981331', '天津北辰科技园区总公司', '8000008791 ', '天津北辰科技园区总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502041619', '上海复星高科技(集团)有限公司', '8000001548 ', '上海复星高科技（集团）有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04HA73B907', '台州银行股份有限公司', '8000001332 ', '台州银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '48KA591548', '丹东港集团有限公司', '8000001484 ', '丹东港集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502036152', '上海国际集团有限公司', '8000001430 ', '上海国际集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16R58EC6BF', '天津港(集团)有限公司', '8000001438 ', '天津港（集团）有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'ovstczD6Cr', '重庆市永川区兴永建设发展有限公司', '8000004703 ', '重庆市永川区兴永建设发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2NQ6A3DA92', '中国城市建设控股集团有限公司', '8000000066 ', '中国城市建设控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '673976227', '杭州富阳开发区建设投资集团有限公司', '8000001596 ', '浙江富春山居集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'cuKyuBYn8v', '上海南汇城乡建设开发投资总公司', '8000001604 ', '上海南汇城乡建设开发投资总公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'A64848A2', '正泰集团股份有限公司', '8000000143 ', '正泰集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1FaobzT2ga', '国家电投集团东北电力有限公司', '8000000081 ', '中电投东北电力有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'QAGj5eX2E9', '鸿达兴业集团有限公司', '8000001340 ', '鸿达兴业集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16R58ECDAB', '招金矿业股份有限公司', '8000001578 ', '招金矿业股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hB50F7982', '上海市人民政府', '8000003764 ', '上海市人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '06K4F027E5', '厦门国际银行股份有限公司', '8000001362 ', '厦门国际银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08S39D0316', '凌云工业股份有限公司', '8000000108 ', '凌云工业股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2024269', '中国光大银行股份有限公司', '8000000038 ', '中国光大银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2024670', '渤海证券股份有限公司', '8000001443 ', '渤海证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PC60B74DD', '灵宝黄金股份有限公司', '8000001542 ', '灵宝黄金股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF46817C6', '奇瑞汽车股份有限公司', '8000001464 ', '奇瑞汽车股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'F8072', '山西煤炭运销集团有限公司', '8000001427 ', '山西煤炭运销集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0835762A25', '中国国电集团公司', '8000001702 ', '中国国电集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3IA56A2C6A', '哈尔滨市城市建设投资集团有限公司', '8000003133 ', '哈尔滨市城市建设投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3AR4DEAA00', '中国中车股份有限公司', '8000000058 ', '中国南车股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '08I60EECCE', '山东省商业集团有限公司', '8000001582 ', '山东省商业集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'Rr6uNb4GZs', '甘肃省人民政府', '8000003786 ', '甘肃省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3I59DB09BC', '鞍山市城市建设投资发展有限公司', '8000001557 ', '鞍山市城市建设投资发展有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000776', '广发证券股份有限公司', '8000001378 ', '广发证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0SA0A977D0', '合肥兴泰金融控股(集团)有限公司', '8000001553 ', '合肥兴泰控股集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600036', '招商银行股份有限公司', '8000000216 ', '招商银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'ii6hHAcIX5', '沈阳盛京能源发展集团有限公司', '8000003728 ', '沈阳城市公用集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1600873', '梅花生物科技集团股份有限公司', '8000000079 ', '梅花生物科技集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2019282', '杉杉集团有限公司', '8000000115 ', '杉杉集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0h25AD2E01', '北京市人民政府', '8000003763 ', '北京市人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'N6P2rFsNwt', '湖南省人民政府', '8000003725 ', '湖南省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2029385', '海南省人民政府', '8000003841 ', '海南省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '8mTL2nnDKS', '宁波市人民政府', '8000003857 ', '宁波市政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2029149', '浙江省手工业合作社联合社', '8000000080 ', '浙江省手工业合作社联合社', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0F3991E17D', '浙江民泰商业银行股份有限公司', '8000008670 ', '浙江民泰商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'A17vQMqdtG', '贵阳农村商业银行股份有限公司', '8000003068 ', '贵阳农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2qB4495556', '安徽国贸集团控股有限公司', '8000001588 ', '安徽国贸集团控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0hQ995E508', '江西省人民政府', '8000003737 ', '江西省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000690', '广东宝丽华新能源股份有限公司', '8000000114 ', '广东宝丽华新能源股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502036646', '中泰证券股份有限公司', '8000001375 ', '齐鲁证券有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4JVA28AF7F', '吉林省人民政府', '8000003742 ', '吉林省政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A574EF71', '阜新银行股份有限公司', '8000002938 ', '阜新银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '3IP86C7CA7', '天津临港建设开发有限公司', '8000004751 ', '天津临港建设开发有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0ST43BAC97', '国家电网公司', '8000001406 ', '国家电网公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0486BB5003', '洛阳银行股份有限公司', '8000001386 ', '洛阳银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '502040647', '泸州老窖集团有限责任公司', '8000001480 ', '泸州老窖集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '0PF9C3A93A', '甘肃省电力投资集团有限责任公司', '8000001589 ', '甘肃省电力投资集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'GKW5MJz3H8', '成都九联投资集团有限公司', '8000004423 ', '成都九联投资有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007535', '中国中化集团公司', '8000000102 ', '中国中化集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF46651B6', '内蒙古东达蒙古王集团有限公司', '8000000069 ', '内蒙古东达蒙古王集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '48O9B4270D', '温岭市国有资产投资集团有限公司', '8000001343 ', '温岭市国有资产投资集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '7ZWtchX3PH', '北京粮食集团有限责任公司', '8000001590 ', '北京粮食集团有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'yIyT8qTAhO', '牡丹江市国有资产投资控股有限公司', '8000001812 ', '牡丹江市国有资产投资控股有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04J4CDF5B3', '北京农村商业银行股份有限公司', '8000000007 ', '北京农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '06H7C7BBF3', '重庆化医控股(集团)公司', '8000000117 ', '重庆化医控股（集团）公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2024609', '东吴证券股份有限公司', '8000001482 ', '东吴证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '48NA4EB9F7', '临沂市城市资产经营开发有限公司', '8000000169 ', '临沂市城市建设投资开发有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04P59F85E8', '安信证券股份有限公司', '8000000052 ', '安信证券股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '4JQ8A99140', '安徽省人民政府', '8000001813 ', '安徽省人民政府', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '16V9E4ED51', '沈阳煤业(集团)有限责任公司', '8000001577 ', '沈阳煤业（集团）有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1000973', '佛山佛塑科技集团股份有限公司', '8000000082 ', '佛山佛塑科技集团股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007457', '北京金融街投资(集团)有限公司', '8000000120 ', '北京金融街投资（集团）有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2013546', '中国华能集团公司', '8000000150 ', '中国华能集团公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '601524', '包头钢铁(集团)有限责任公司', '8000001534 ', '包头钢铁（集团）有限责任公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A57770DA', '威海市商业银行股份有限公司', '8000001346 ', '威海市商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2AF3E94AEB', '天瑞集团水泥有限公司', '8000000136 ', '天瑞集团水泥有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2007687', '江苏阳光集团有限公司', '8000001338 ', '江苏阳光集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2012295', '华信信托股份有限公司', '8000000165 ', '盛京银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '2002177', '中国农业银行股份有限公司', '8000000222 ', '中国农业银行股份公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '1HVEC4EC99', '四川省水电投资经营集团有限公司', '8000001560 ', '四川省水电投资经营集团有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '04A577CDAE', '广东南海农村商业银行股份有限公司', '8000001392 ', '广东南海农村商业银行股份有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', 'ciUkvIwWBx', '天津滨海高新区资产管理有限公司', '8000004739 ', '天津滨海高新区资产管理有限公司', null, null);

insert into IFS_WD_OPICS_SECMAP (MAPID, MAPNAME, WDID, WDNAME, OPICSID, OPICSNAME, REMARK1, REMARK2)
values ('CUST', 'ISSUER-CUST', '5osNdLSfxG', '贵州银行股份有限公司', '8000001671 ', '贵州银行股份有限公司', null, null);



insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('国债',10.0,11.0,12.0,13.0,14.0,15.0,'1');

insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('央行票据',10.0,11.0,12.0,13.0,14.0,15.0,'2');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('政策性银行金融债',10.0,11.0,12.0,13.0,14.0,15.0,'3');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('非银行金融机构债',10.0,11.0,12.0,13.0,14.0,15.0,'4');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('商业银行金融债',10.0,11.0,12.0,13.0,14.0,15.0,'5');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('企业债',10.0,11.0,12.0,13.0,14.0,15.0,'6');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('铁道债',10.0,11.0,12.0,13.0,14.0,15.0,'7');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('公司债',10.0,11.0,12.0,13.0,14.0,15.0,'8');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('短期融资债',10.0,11.0,12.0,13.0,14.0,15.0,'9');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('地方债',10.0,11.0,12.0,13.0,14.0,15.0,'10');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('中期票据',10.0,11.0,12.0,13.0,14.0,15.0,'11');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('资产支持证券',10.0,11.0,12.0,13.0,14.0,15.0,'12');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('抵押支持证券',10.0,11.0,12.0,13.0,14.0,15.0,'13');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('次级债',10.0,11.0,12.0,13.0,14.0,15.0,'14');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('集合票据',10.0,11.0,12.0,13.0,14.0,15.0,'15');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('国际机构债',10.0,11.0,12.0,13.0,14.0,15.0,'16');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('混合资本债',10.0,11.0,12.0,13.0,14.0,15.0,'17');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('可分离债',10.0,11.0,12.0,13.0,14.0,15.0,'18');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('可转债',10.0,11.0,12.0,13.0,14.0,15.0,'19');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('同业存单',10.0,11.0,12.0,13.0,14.0,15.0,'20');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('外债',10.0,11.0,12.0,13.0,14.0,15.0,'21');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('其他',10.0,11.0,12.0,13.0,14.0,15.0,'22');
insert into LIMIT_RISK (SECM_TYPE, DURATION_TEST, DURATION_LIMIT,
                        BP_TEST, BP_LMIT, PV_TEST,
                        PV_LIMIT,SEQUE)
values('总限额',10.0,11.0,12.0,13.0,14.0,15.0,'23');


INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('feeprodtype','FE','FEE','交易所费用参数','交易所费用参数','0','00','00','费用产品类型');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('feeprod','FEES','FEE','交易所费用参数','交易所费用参数','0','00','00','费用产品');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('cost','704060','FEE','交易所费用参数','交易所费用参数','0','00','00','费用成本中心');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('tenor','99','FEE','交易所费用参数','交易所费用参数','0','00','00','期限');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('al','A','FEE','交易所费用参数','交易所费用参数','0','00','00','al');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('acctgmethod','A','FEE','交易所费用参数','交易所费用参数','0','00','00','acctgmethod');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('verind','1','FEE','交易所费用参数','交易所费用参数','0','00','00','verind');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('settmeans','CNAPS','FEE','交易所费用参数','交易所费用参数','0','00','00','settmeans');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('settacct','CNAPS','FEE','交易所费用参数','交易所费用参数','0','00','00','settacct');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('settauthind','Y','FEE','交易所费用参数','交易所费用参数','0','00','00','settauthind');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('feesind','Y','FEE','交易所费用参数','交易所费用参数','0','00','00','feesind');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('feeper_8','0','FEE','交易所费用参数','交易所费用参数','0','00','00','feeper_8');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('port','0','FEE','交易所费用参数','交易所费用参数','0','00','00','port');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('siind','Y','FEE','交易所费用参数','交易所费用参数','0','00','00','siind');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('suppconfind','Y','FEE','交易所费用参数','交易所费用参数','0','00','00','suppconfind');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('suppayind','Y','FEE','交易所费用参数','交易所费用参数','0','00','00','suppayind');

INSERT INTO TA_SYS_PARAM(P_CODE,P_VALUE,P_TYPE,P_TYPE_NAME,P_MEMO,P_EDITABLED,P_PROP1,P_PROP2,P_NAME)
VALUES('suprecind','Y','FEE','交易所费用参数','交易所费用参数','0','00','00','suprecind');

commit;