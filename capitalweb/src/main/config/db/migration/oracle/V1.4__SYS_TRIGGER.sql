--prompt
--prompt Creating trigger PRODUCT_WEIGHT_TRIGGER
--prompt =======================================
--prompt
create or replace trigger product_weight_trigger
before insert on IFS_PROTECT_WEIGHT
for each row
begin
select product_weight_sql.nextval into:new.product_weight_id from sys.dual;
end product_weight_trigger;
/

--prompt
--prompt Creating trigger tib_TT_PROC_ACTIVITY_CREATION
--prompt ==============================================
--prompt
create or replace trigger "tib_TT_PROC_ACTIVITY_CREATION" before insert
on TT_PROC_ACTIVITY_CREATION for each row
declare
    integrity_error  exception;
    errno            integer;
    errmsg           char(200);
    dummy            integer;
    found            boolean;

begin
    --  Column "ID" uses sequence S_TT_PROC_ACTIVITY_CREATION
    select S_TT_PROC_ACTIVITY_CREATION.NEXTVAL INTO :new.ID from dual;

--  Errors handling
exception
    when integrity_error then
       raise_application_error(errno, errmsg);
end;
/