
--账套
delete from TAC_TASK t where t.task_id='DEFAULT';
insert into TAC_TASK (TASK_ID, TASK_TYPE, TASK_NAME, TASK_CURRENT_DATE, DEFAULT_TASK, CREATE_DATE, CREATE_TIME, UPDATE_TIME, BK_MAIN, INST_ID, BK_MAIN_NM, INST_ID_NM, BKKPG_ORG_ID)
values ('DEFAULT', '1', '默认账套', '2017-09-25', '1', '2017-09-25', '18:21:36', '2017-09-25 18:21:36', null, null, null, null, null);

--excel 约束
delete from tb_excel_config t;
insert into tb_excel_config (PRD_NO, PRODUCT_ID_COL, BEGIN_READ_ROW, BEGIN_DIMESION_COL, END_DIMESION_COL)
values ('801', 0, 1, 4, 7);
insert into tb_excel_config (PRD_NO, PRODUCT_ID_COL, BEGIN_READ_ROW, BEGIN_DIMESION_COL, END_DIMESION_COL)
values ('802', 0, 1, 4, 5);
insert into tb_excel_config (PRD_NO, PRODUCT_ID_COL, BEGIN_READ_ROW, BEGIN_DIMESION_COL, END_DIMESION_COL)
values ('803', 0, 1, 4, 6);
insert into tb_excel_config (PRD_NO, PRODUCT_ID_COL, BEGIN_READ_ROW, BEGIN_DIMESION_COL, END_DIMESION_COL)
values ('799', 0, 1, 4, 4);
--设置产品表约束
update tc_product set is_acup ='1' where prd_no in ('801','802','803','799');

--会计维度
delete from ta_dict t where t.dict_id = 'IFRS9';
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('IFRS9', 'IFRS9会计引擎控制器', 'IFRS9', 'IFRS9会计引擎控制器', '-', 'OPEN', '1', '1', '1', 1);

delete from ta_dict t where t.dict_id = 'dimension';
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('dimension', '核算方式', '会计核算', '核算方式', '-', 'tbAccountType', '核算方式', '1', '1', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('dimension', '风险类型', '会计核算', '风险类型', '-', 'riskType', '风险类型', '1', '2', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('dimension', '赎回类型', '会计核算', '赎回类型', '-', 'rdpType', '赎回类型', '1', '3', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('dimension', '交易对手类型', '会计核算', '交易对手类型', '-', 'accountCustType', '交易对手类型', '1', '4', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('dimension', '净值类型', '会计核算', '净值类型', '-', 'priceType', '净值类型', '1', '5', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('dimension', '分红方式', '会计核算', '分红方式', '-', 'BonusWay', '分红方式', '1', '6', 1);

delete from ta_dict t where t.dict_id = 'tbAccountType';
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('tbAccountType', '核算方式', '会计核算', '核算方式', '-', '0', '以摊余成本计量', '1', '1', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('tbAccountType', '核算方式', '会计核算', '核算方式', '-', '2', '以公允价值计量且其变动计入当期损益', '1', '2', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('tbAccountType', '核算方式', '会计核算', '核算方式', '-', '3', '以公允价值计量且其变动计入其他综合收益', '1', '3', 1);

delete from ta_dict t where t.dict_id = 'riskType';
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('riskType', '风险类型', '会计核算', '风险类型', '-', '1', '保本', '1', '1', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('riskType', '风险类型', '会计核算', '风险类型', '-', '0', '非保本', '1', '2', 1);

delete from ta_dict t where t.dict_id = 'rdpType';
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('rdpType', '赎回类型', '会计核算', '赎回类型', '-', '1', '全部赎回', '1', '1', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('rdpType', '赎回类型', '会计核算', '赎回类型', '-', '0', '部分赎回', '1', '2', 1);

delete from ta_dict t where t.dict_id = 'accountCustType';
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('accountCustType', '交易对手类型', '会计核算', '交易对手类型', '-', '0', '银行同业', '1', '1', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('accountCustType', '交易对手类型', '会计核算', '交易对手类型', '-', '1', '集团内村镇银行', '1', '2', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('accountCustType', '交易对手类型', '会计核算', '交易对手类型', '-', '2', '集团内哈租赁/哈消金', '1', '3', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('accountCustType', '交易对手类型', '会计核算', '交易对手类型', '-', '3', '非银同业', '1', '4', 1);

delete from ta_dict t where t.dict_id = 'priceType';
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('priceType', '净值类型', '会计核算', '净值类型', '-', '0', '净值型', '1', '1', 1);
insert into ta_dict (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('priceType', '净值类型', '会计核算', '净值类型', '-', '1', '非净值型', '1', '2', 1);

--会计金额码表
delete from tb_amount;
insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_assmt', '估值金额', '1*amtMap.get("loan_assmt")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_Bond', '分红金额', '1*amtMap.get("loan_Bond")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('amt_Int', '每日计提', '1*amtMap.get("amt_Int")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_Amt-fee-loan_Vamt', '合同金额-手续费-确认金额', '1*amtMap.get("loan_Amt")-amtMap.get("fee")-amtMap.get("loan_Vamt")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_Amt+amt_Int', '合同金额+利息', '1*amtMap.get("loan_Amt")+amtMap.get("amt_Int")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_Drt', '红利金额', '1*amtMap.get("loan_Drt")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('income_Int', '利息', '1*amtMap.get("income_Int")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_Amt+income_Int', '合同金额+利息', 'amtMap.get("loan_Amt")+amtMap.get("income_Int")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_Amt', '合同金额', '1*amtMap.get("loan_Amt")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('fee', '手续费', '1*amtMap.get("fee")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('income_Int-sys_Int', '到期利息-系统计算利息', 'amtMap.get("income_Int")-amtMap.get("sys_Int")', null);

insert into tb_amount (AMOUNT_ID, AMOUNT_NAME, AMOUNT_FORMULA, AMOUNT_DESC)
values ('loan_assmt_last', '前一日估值', 'amtMap.get("loan_assmt_last")', null);

delete from tb_dimension_value; 
