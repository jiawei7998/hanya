--流程删除
insert into ACT_GE_PROPERTY values ('schema.version', '5.22.0.0', 1);
insert into ACT_GE_PROPERTY values ('schema.history', 'create(5.22.0.0)', 1);
insert into ACT_GE_PROPERTY values ('next.dbid', '1', 1);

--审批角色表
DELETE FROM TT_FLOW_ROLE WHERE 1=1;
insert into TT_FLOW_ROLE values ('SP0000001','交易员','审批角色','00014');
insert into TT_FLOW_ROLE values ('SP0000002','金融部领导','审批角色','00014');
insert into TT_FLOW_ROLE values ('SP0000003','小中台','审批角色','00014');
insert into TT_FLOW_ROLE values ('SP0000004','理财部领导','审批角色','00014');
insert into TT_FLOW_ROLE values ('SP0000005','行长','审批角色','00014');
commit;

--审批权限初始表
DELETE FROM TT_FLOW_OPERATION WHERE 1=1;
insert into TT_FLOW_OPERATION (OPERATION_ID, OPERATION_NAME, COMMENTS) values (4, '通过', null);
insert into TT_FLOW_OPERATION (OPERATION_ID, OPERATION_NAME, COMMENTS) values (8, '退回', null);
insert into TT_FLOW_OPERATION (OPERATION_ID, OPERATION_NAME, COMMENTS) values (10, '拒绝', null);
commit;

--审批角色权限表，导不导数据看具体情况
DELETE FROM TT_FLOW_ROLE_AUTH WHERE 1=1;
insert into TT_FLOW_ROLE_AUTH values ('SP0000001','4');
insert into TT_FLOW_ROLE_AUTH values ('SP0000001','8');
insert into TT_FLOW_ROLE_AUTH values ('SP0000001','10');
insert into TT_FLOW_ROLE_AUTH values ('SP0000002','4');
insert into TT_FLOW_ROLE_AUTH values ('SP0000002','8');
insert into TT_FLOW_ROLE_AUTH values ('SP0000002','10');
insert into TT_FLOW_ROLE_AUTH values ('SP0000003','4');
insert into TT_FLOW_ROLE_AUTH values ('SP0000003','8');
insert into TT_FLOW_ROLE_AUTH values ('SP0000003','10');
insert into TT_FLOW_ROLE_AUTH values ('SP0000004','4');
insert into TT_FLOW_ROLE_AUTH values ('SP0000004','8');
insert into TT_FLOW_ROLE_AUTH values ('SP0000004','10');
insert into TT_FLOW_ROLE_AUTH values ('SP0000005','4');
insert into TT_FLOW_ROLE_AUTH values ('SP0000005','8');
insert into TT_FLOW_ROLE_AUTH values ('SP0000005','10');
commit;

--审批角色与用户关联表
DELETE FROM TT_FLOW_ROLE_USER_MAP WHERE 1=1;
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','XZF','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','QGD','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','ZBJ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','LSL','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','ZSS','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','GXX','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','SY','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','TXM','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','JYM','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','FBZ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','ZYX','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','LT','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','WX','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','LTY','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','YJ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','GYT','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','GYL','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','CH','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','ZJ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','SK','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','WHQ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','YLS','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','SYF','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','WMF','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','LYJ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','LDD','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','LQL','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','LY','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','SXW','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','SX','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','ZQR','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','ZM','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','GTM','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','SBM','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','ZHW','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','JCY','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','WQ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','CY','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','ZD','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','LLY','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','LH','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000002','CYM','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000003','WJJ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000005','GAOJIN','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000004','YDZ','','','');
insert into TT_FLOW_ROLE_USER_MAP values ('SP0000001','YWZX','','','');
commit;

--审批类型配置表
DELETE FROM TT_PRODUCT_FLOW_TYPE_MAP WHERE 1=1;
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('391','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('411','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('425','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('432','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('438','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('441','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('442','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('443','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('444','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('445','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('445','40');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('446','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('447','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('448','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('449','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('452','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('467','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('468','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('469','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('470','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('471','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('472','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('500','99');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('501','99');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('502','99');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('504','99');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('505','99');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('510','99');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('544','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('666','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('667','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('799','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('800','4');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('801','30');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('801','31');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('801','32');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('801','33');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('801','34');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('802','30');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('802','31');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('802','32');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('802','33');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('802','34');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('803','30');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('803','31');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('803','32');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('803','33');
insert into TT_PRODUCT_FLOW_TYPE_MAP values ('803','34');
commit;

--审批流程表
DELETE FROM TT_PROC_DEF_PROP_MAP WHERE 1=1;
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','452','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','452','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','452','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','467','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','452','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','452','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','449','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','449','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','449','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','452','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','449','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','449','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','467','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','468','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','468','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','468','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','469','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','469','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','468','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','467','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','467','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','467','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','468','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','468','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','467','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','446','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','446','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','446','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','446','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','446','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','446','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','445','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','445','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','445','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','445','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','445','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','445','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','447','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','448','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','448','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','448','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','449','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','448','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','448','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','447','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','447','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','447','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','448','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','447','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','447','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','666','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','666','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','666','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','667','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','667','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','667','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','544','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','544','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','544','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','666','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','666','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','666','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','667','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','800','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','800','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','799','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','800','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','800','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','800','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','799','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','667','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','667','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','799','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','799','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','799','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','470','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','470','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','470','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','471','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','471','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','470','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','469','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','469','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','469','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','470','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','470','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','469','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','471','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','472','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','472','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','472','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','544','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','544','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','544','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','471','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','471','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','471','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','472','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','472','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','472','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','444','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','425','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','425','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','432','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','425','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','425','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','425','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','432','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','432','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','438','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','438','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','432','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','432','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','432','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','391','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','391','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','391','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','391','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','391','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','391','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','411','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','411','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','411','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','425','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','411','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','411','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','411','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','438','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','443','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','443','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','443','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','442','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','442','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','443','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','443','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','444','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','444','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','444','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','443','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','444','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','444','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','441','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','441','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','441','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','438','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','438','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','438','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','441','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','442','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','442','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','442','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','441','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','441','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:1:8','1','442','00003');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','803','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','803','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','803','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','803','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','803','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','801','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','801','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','801','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','801','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','801','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','802','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','802','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','802','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','802','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:3:16','3','802','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','802','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','802','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','802','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','803','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','802','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','801','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','801','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','801','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','801','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','801','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','803','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','802','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','803','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','803','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:4:20','4','803','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','802','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','802','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','802','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','802','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','802','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','803','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','803','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','803','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','801','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','803','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','803','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','801','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','801','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','801','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:5:24','5','801','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','801','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','801','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','801','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','803','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','803','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','801','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','803','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','803','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','803','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','802','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','802','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','802','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','802','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','801','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:6:28','6','802','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','801','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','801','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','801','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','801','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','801','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','803','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','803','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','803','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','803','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','803','00014');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','802','00008');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','802','00005');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','802','00012');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','802','00022');
insert into TT_PROC_DEF_PROP_MAP values ('process:7:32','7','802','00014');
commit;

--流程版本信息
insert into ACT_RE_MODEL (ID_, REV_, NAME_, KEY_, CATEGORY_, CREATE_TIME_, LAST_UPDATE_TIME_, VERSION_, META_INFO_, DEPLOYMENT_ID_, EDITOR_SOURCE_VALUE_ID_, EDITOR_SOURCE_EXTRA_VALUE_ID_, TENANT_ID_)
values ('1', 9, '本币交易审批流程', 'CNYAPPROVFLOW', null, '29-6月 -21 03.49.19.534000 下午', '30-6月 -21 11.37.05.195000 上午', 1, '{"name":"本币交易审批流程","revision":1,"description":"用于本币交易审批的流程"}', null, '2', '3', null);
insert into ACT_RE_MODEL (ID_, REV_, NAME_, KEY_, CATEGORY_, CREATE_TIME_, LAST_UPDATE_TIME_, VERSION_, META_INFO_, DEPLOYMENT_ID_, EDITOR_SOURCE_VALUE_ID_, EDITOR_SOURCE_EXTRA_VALUE_ID_, TENANT_ID_)
values ('47501', 4, '基金申购确认审批', '1', null, '01-7月 -21 10.54.21.974000 上午', '01-7月 -21 11.01.49.770000 上午', 1, '{"name":"基金申购确认审批","revision":1,"description":"基金申购确认审批"}', null, '47502', '47503', null);
insert into ACT_RE_MODEL (ID_, REV_, NAME_, KEY_, CATEGORY_, CREATE_TIME_, LAST_UPDATE_TIME_, VERSION_, META_INFO_, DEPLOYMENT_ID_, EDITOR_SOURCE_VALUE_ID_, EDITOR_SOURCE_EXTRA_VALUE_ID_, TENANT_ID_)
values ('47504', 4, '基金申购审批', '1', null, '01-7月 -21 11.02.32.532000 上午', '01-7月 -21 11.06.03.670000 上午', 1, '{"name":"基金申购审批","revision":1,"description":"基金申购审批"}', null, '47505', '47506', null);
insert into ACT_RE_MODEL (ID_, REV_, NAME_, KEY_, CATEGORY_, CREATE_TIME_, LAST_UPDATE_TIME_, VERSION_, META_INFO_, DEPLOYMENT_ID_, EDITOR_SOURCE_VALUE_ID_, EDITOR_SOURCE_EXTRA_VALUE_ID_, TENANT_ID_)
values ('47507', 4, '基金赎回审批', '1', null, '01-7月 -21 11.06.53.664000 上午', '01-7月 -21 11.13.18.818000 上午', 1, '{"name":"基金赎回审批","revision":1,"description":"基金赎回审批"}', null, '47508', '47509', null);
insert into ACT_RE_MODEL (ID_, REV_, NAME_, KEY_, CATEGORY_, CREATE_TIME_, LAST_UPDATE_TIME_, VERSION_, META_INFO_, DEPLOYMENT_ID_, EDITOR_SOURCE_VALUE_ID_, EDITOR_SOURCE_EXTRA_VALUE_ID_, TENANT_ID_)
values ('47510', 4, '基金赎回确认审批', '1', null, '01-7月 -21 11.13.41.321000 上午', '01-7月 -21 11.16.21.603000 上午', 1, '{"name":"基金赎回确认审批","revision":1,"description":"基金赎回确认审批"}', null, '47511', '47512', null);
insert into ACT_RE_MODEL (ID_, REV_, NAME_, KEY_, CATEGORY_, CREATE_TIME_, LAST_UPDATE_TIME_, VERSION_, META_INFO_, DEPLOYMENT_ID_, EDITOR_SOURCE_VALUE_ID_, EDITOR_SOURCE_EXTRA_VALUE_ID_, TENANT_ID_)
values ('47513', 4, '基金分红审批', '1', null, '01-7月 -21 11.16.56.411000 上午', '01-7月 -21 11.20.19.303000 上午', 1, '{"name":"基金分红审批","revision":1,"description":"基金分红审批"}', null, '47514', '47515', null);

-- 审批流程表
DELETE FROM TT_PROC_DEF WHERE 1=1;
insert into TT_PROC_DEF (FLOW_ID, FLOW_NAME, FLOW_TYPE, VERSION, IS_ACTIVE, MODEL_ID) values ('process:3:16', '基金申购审批', '30', '3', '1', '47504');
insert into TT_PROC_DEF (FLOW_ID, FLOW_NAME, FLOW_TYPE, VERSION, IS_ACTIVE, MODEL_ID) values ('process:4:20', '基金申购确认审批', '31', '4', '1', '47501');
insert into TT_PROC_DEF (FLOW_ID, FLOW_NAME, FLOW_TYPE, VERSION, IS_ACTIVE, MODEL_ID) values ('process:7:32', '基金分红审批', '34', '7', '1', '47513');
insert into TT_PROC_DEF (FLOW_ID, FLOW_NAME, FLOW_TYPE, VERSION, IS_ACTIVE, MODEL_ID) values ('process:5:24', '基金赎回审批', '32', '5', '1', '47507');
insert into TT_PROC_DEF (FLOW_ID, FLOW_NAME, FLOW_TYPE, VERSION, IS_ACTIVE, MODEL_ID) values ('process:1:8', '本币交易审批流程', '4', '1', '1', '1');
insert into TT_PROC_DEF (FLOW_ID, FLOW_NAME, FLOW_TYPE, VERSION, IS_ACTIVE, MODEL_ID) values ('process:6:28', '基金赎回确认审批', '33', '6', '1', '47510');
commit;

--审批流程节点关联表   42 rows
DELETE FROM TT_PROC_TASK_URL WHERE 1=1;
commit;

--审批事件
delete from TD_FLOW_TASK_EVENT;
insert into TD_FLOW_TASK_EVENT (EVENT_ID, EVENT_NAME, EVENT_CODE)
values (1, '限额控制', 'qutoLimit');
commit;

--流程字典表
delete from TT_FLOW_DICT;
insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('optionConfig', '是否', '1', '是', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('optionConfig', '是否', '0', '否', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('riskLevelConfig', '风险程度', '1', '低风险', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('riskLevelConfig', '风险程度', '2', '较低风险', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('riskLevelConfig', '风险程度', '3', '低风险和较低风险', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('riskLevelConfig', '风险程度', '4', '不适用', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '货币代码', '1', '人民币', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '2', '美元', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '3', '欧元', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '4', '日元', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '5', '港币', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '6', '英镑', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '7', '卢布', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '8', '林吉特', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '9', '加元', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '10', '澳元', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '11', '瑞士法郎', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('currencyConfig', '币种', '12', '新加坡元', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '1', 'AAA+', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '2', 'AAA', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '3', 'AA+', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '4', 'AA', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '5', 'AA-', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '6', 'A+', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '7', 'A', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '8', 'A-', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '9', 'A-1', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '10', 'BBB+', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '11', 'BBB', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '12', 'BBB-', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '13', 'BBB-以下', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('mainLevelConfig', '主体评级', '14', '无评级', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('bondTypeConfig', '债券类型', '1', '利率债', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('bondTypeConfig', '债券类型', '2', '非金融机构类信用债', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('bondTypeConfig', '债券类型', '3', '金融机构债券', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('bondTypeConfig', '债券类型', '4', '同业存单', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('bondTypeConfig', '债券类型', '5', '资产支持证券/ABN', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('bondTypeConfig', '债券类型', '6', '其他', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('tradeDirectionConfig', '交易方向', '1', '买入/借入/拆入/正回购', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('tradeDirectionConfig', '交易方向', '2', '卖出/借出/拆出/逆回购', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('productConfig', '产品代码', '1', '外汇即期', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('productConfig', '产品代码', '2', '外汇远期', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('productConfig', '产品代码', '3', '外汇掉期', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('productConfig', '产品代码', '4', '外币拆借', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('prodTypeConfig', '产品类型', '1', '自营', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('prodTypeConfig', '产品类型', '2', '代客', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('costConfig', '成本中心', '1', '平盘', '1');

insert into TT_FLOW_DICT (DICT_ID, DICT_NAME, DICT_KEY, DICT_VALUE, IS_ACTIVE)
values ('costConfig', '成本中心', '2', '平补', '1');
commit;