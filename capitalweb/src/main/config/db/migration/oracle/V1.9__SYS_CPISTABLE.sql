-- Create table
create table IFS_CPIS_RECINFO
(
    CONFIRMID       VARCHAR2(30),
    EXECID          VARCHAR2(30),
    AFFIRMSTATUS    VARCHAR2(30),
    TRADEDATE       VARCHAR2(8),
    MARKETID        VARCHAR2(8),
    MARKETINDICATOR VARCHAR2(8),
    QUERYREQUESTID  VARCHAR2(30),
    MATCHSTATUS     VARCHAR2(1),
    TEXT            VARCHAR2(1000),
    CONFIRMSTATUS   VARCHAR2(30),
    execType        VARCHAR2(10)
);
comment on table IFS_CPIS_RECINFO
    is 'CFETS返回信息主要信息表';
comment on column IFS_CPIS_RECINFO.CONFIRMID
  is '确认编号';
comment on column IFS_CPIS_RECINFO.EXECID
  is '成交编号';
comment on column IFS_CPIS_RECINFO.AFFIRMSTATUS
  is '校验状态  ';
comment on column IFS_CPIS_RECINFO.TRADEDATE
  is '成交日期';
comment on column IFS_CPIS_RECINFO.MARKETID
  is '交易场所  ';
comment on column IFS_CPIS_RECINFO.MARKETINDICATOR
  is '市场类型  ';
comment on column IFS_CPIS_RECINFO.QUERYREQUESTID
  is '查询ID';
comment on column IFS_CPIS_RECINFO.MATCHSTATUS
  is '匹配状态';
comment on column IFS_CPIS_RECINFO.TEXT
  is '未匹配字段';
comment on column IFS_CPIS_RECINFO.CONFIRMSTATUS
  is '确认状态';
comment on column IFS_CPIS_RECINFO.execType
  is '成交状态';

-- Create/Recreate primary, unique and foreign key constraints
alter table IFS_CPIS_RECINFO
    add constraint PK_LS primary key (EXECID);


create table IFS_CPISFX_MM_REC
(
    EXECID                  VARCHAR2(32),
    CONFIRMID               VARCHAR2(32) not null,
    MARKETINDICATOR         VARCHAR2(2),
    TRADEDATE               VARCHAR2(8),
    NETGROSSIND             VARCHAR2(1),
    SETTLDATE               VARCHAR2(8),
    SETTLDATE2              VARCHAR2(8),
    CURRENCY                VARCHAR2(3),
    LASTPX                  VARCHAR2(16),
    DAYCOUNT                VARCHAR2(10),
    SIDE                    VARCHAR2(1),
    ACCRUEDINTERESTTOTALAMT VARCHAR2(20),
    LASTQTY                 NUMBER(19,4),
    SETTLECURRAMT2          VARCHAR2(20),
    OTHPARTYID              VARCHAR2(35),
    OWNPARTYID              VARCHAR2(35),
    OWNCAPITALACCOUNTNAME   VARCHAR2(150),
    OWNPAYBANKNO            VARCHAR2(35),
    OWNCAPITALACCOUNTBIC    VARCHAR2(35),
    OWNCAPITALBAKENAME      VARCHAR2(150),
    OWNCAPITALACCOUNT       VARCHAR2(150),
    OWNCNNAME               VARCHAR2(150),
    OWNENNAME               VARCHAR2(150),
    OWNREMARK               VARCHAR2(150),
    OWNPARTYROLE            NUMBER,
    OTHPARTYROLE            NUMBER,
    OWNOPENACCOUNT          VARCHAR2(150),
    OTHCAPITALBAKENAME      VARCHAR2(150),
    OTHCAPITALACCOUNTNAME   VARCHAR2(150),
    OTHCAPITALACCOUNT       VARCHAR2(35),
    OTHPAYBANKNO            VARCHAR2(35),
    EXECTYPE                VARCHAR2(10),
    DEALTRANSTYPE           VARCHAR2(10),
    CONFIRMSTATUS           VARCHAR2(10),
    OWNCNFMTIMETYPE         NUMBER,
    OWNCNFMTIME             VARCHAR2(50),
    OTHCNFMTIMETYPE         NUMBER,
    OTHCNFMTIME             VARCHAR2(50),
    OTHREMARK               VARCHAR2(150),
    MIDCNNAME               VARCHAR2(150),
    MIDBANKBIC              VARCHAR2(35),
    MIDBANKACCOUNT          VARCHAR2(35),
    OTHCNNAME               VARCHAR2(200),
    OTHENNAME               VARCHAR2(200),
    CURRENCY2               VARCHAR2(35),
    CALCULATEDCCYLASTQTY    VARCHAR2(150)
)
;
comment on table IFS_CPISFX_MM_REC
    is '外币拆借交易确认信息返回';
comment on column IFS_CPISFX_MM_REC.EXECID
    is '成交编号';
comment on column IFS_CPISFX_MM_REC.CONFIRMID
    is '报文编号';
comment on column IFS_CPISFX_MM_REC.MARKETINDICATOR
    is '交易市场类型 ';
comment on column IFS_CPISFX_MM_REC.TRADEDATE
    is '成交日期';
comment on column IFS_CPISFX_MM_REC.NETGROSSIND
    is '清算方式';
comment on column IFS_CPISFX_MM_REC.SETTLDATE
    is '起息日';
comment on column IFS_CPISFX_MM_REC.SETTLDATE2
    is '到期日';
comment on column IFS_CPISFX_MM_REC.CURRENCY
    is '拆借币种';
comment on column IFS_CPISFX_MM_REC.LASTPX
    is '拆借利率';
comment on column IFS_CPISFX_MM_REC.DAYCOUNT
    is '计息基准';
comment on column IFS_CPISFX_MM_REC.SIDE
    is '本方交易方向';
comment on column IFS_CPISFX_MM_REC.ACCRUEDINTERESTTOTALAMT
    is '应计利息 ';
comment on column IFS_CPISFX_MM_REC.LASTQTY
    is '拆借金额';
comment on column IFS_CPISFX_MM_REC.SETTLECURRAMT2
    is '到期还款金额';
comment on column IFS_CPISFX_MM_REC.OTHPARTYID
    is '对方21位机构码';
comment on column IFS_CPISFX_MM_REC.OWNPARTYID
    is '本方21位机构码';
comment on column IFS_CPISFX_MM_REC.OWNCAPITALACCOUNTNAME
    is '本方 资金账户户名';
comment on column IFS_CPISFX_MM_REC.OWNPAYBANKNO
    is '本方 支付系统行号';
comment on column IFS_CPISFX_MM_REC.OWNCAPITALACCOUNTBIC
    is '本方开户行BICCODE';
comment on column IFS_CPISFX_MM_REC.OWNCAPITALBAKENAME
    is '本方 资金开户行';
comment on column IFS_CPISFX_MM_REC.OWNCAPITALACCOUNT
    is '本方 资金账号';
comment on column IFS_CPISFX_MM_REC.OWNCNNAME
    is '本方 中文全称';
comment on column IFS_CPISFX_MM_REC.OWNENNAME
    is '本方 英文全称';
comment on column IFS_CPISFX_MM_REC.OWNREMARK
    is '本方 付言';
comment on column IFS_CPISFX_MM_REC.OWNOPENACCOUNT
    is '本方 开户行账号';
comment on column IFS_CPISFX_MM_REC.OTHCAPITALBAKENAME
    is '对手方 资金开户行';
comment on column IFS_CPISFX_MM_REC.OTHCAPITALACCOUNTNAME
    is '对手方 资金账户户名';
comment on column IFS_CPISFX_MM_REC.OTHCAPITALACCOUNT
    is '对手方 资金账号';
comment on column IFS_CPISFX_MM_REC.OTHPAYBANKNO
    is '对手方 支付系统行号';
comment on column IFS_CPISFX_MM_REC.CONFIRMSTATUS
    is '成交状态';
comment on column IFS_CPISFX_MM_REC.OWNCNFMTIME
    is '本方 确认时间';
comment on column IFS_CPISFX_MM_REC.OTHCNFMTIME
    is '对手方 确认时间';
comment on column IFS_CPISFX_MM_REC.OTHREMARK
    is '对手方 附言';
comment on column IFS_CPISFX_MM_REC.MIDCNNAME
    is ' 本方 中间行名称';
comment on column IFS_CPISFX_MM_REC.MIDBANKBIC
    is ' 本方 中间行SWIFT CODE/中间行行号';
comment on column IFS_CPISFX_MM_REC.MIDBANKACCOUNT
    is ' 本方 中间行在开户行的资金账号';
comment on column IFS_CPISFX_MM_REC.OTHCNNAME
    is '对手方机构中文全称';
comment on column IFS_CPISFX_MM_REC.OTHENNAME
    is '对手方机构英文全称';
comment on column IFS_CPISFX_MM_REC.CURRENCY2
    is '卖出币种';
comment on column IFS_CPISFX_MM_REC.CALCULATEDCCYLASTQTY
    is '卖出金额';

create table IFS_CPISFX_SPTFWD_REC
(
    EXECID                VARCHAR2(32),
    TRADEDATE             VARCHAR2(8),
    CONFIRMID             VARCHAR2(32),
    NETGROSSIND           VARCHAR2(20),
    MARKETINDICATOR       VARCHAR2(2),
    SETTLDATE             VARCHAR2(8),
    LASTPX                NUMBER(18,8),
    CURRENCY1             VARCHAR2(3),
    LASTQTY               NUMBER(19,4),
    CURRENCY2             VARCHAR2(3),
    CALCULATEDCCYLASTQTY  NUMBER(19,4),
    OTHPARTYID            VARCHAR2(25),
    OWNCAPITALBAKENAME    VARCHAR2(150),
    OWNCAPITALACCOUNTNAME VARCHAR2(150),
    OWNCAPITALACCOUNT     VARCHAR2(60),
    OWNCAPITALACCOUNTBIC  VARCHAR2(32),
    OWNPAYBANKNO          VARCHAR2(20),
    OTHCAPITALBANKNAME    VARCHAR2(150),
    OTHCAPITALACCOUNTNAME VARCHAR2(150),
    OTHCAPITALACCOUNT     VARCHAR2(60),
    MIDCNNAME             VARCHAR2(150),
    MIDBANKBIC            VARCHAR2(60),
    MIDBANKACCOUNT        VARCHAR2(60),
    OTHPAYBANKNO          VARCHAR2(20),
    OTHREMARK             VARCHAR2(150),
    OWNREMARK             VARCHAR2(150),
    OTHCNNAME             VARCHAR2(200),
    OWNPARTYID            VARCHAR2(25),
    CONFIRMSTATUS         VARCHAR2(10),
    DEALTRANSTYPE         VARCHAR2(10),
    EXECTYPE              VARCHAR2(10),
    OWNCNFMTIME           VARCHAR2(20),
    OTHCNFMTIME           VARCHAR2(20),
    OWNCNNAME             VARCHAR2(200),
    OWNOPENACCOUNT        VARCHAR2(60),
    SECURITYID            VARCHAR2(10)
)
;
comment on table IFS_CPISFX_SPTFWD_REC
  is '外汇即远期交易确认信息返回';
comment on column IFS_CPISFX_SPTFWD_REC.EXECID
  is '成交编号';
comment on column IFS_CPISFX_SPTFWD_REC.TRADEDATE
  is '成交日期';
comment on column IFS_CPISFX_SPTFWD_REC.CONFIRMID
  is '确认报文ID';
comment on column IFS_CPISFX_SPTFWD_REC.NETGROSSIND
  is '清算方式';
comment on column IFS_CPISFX_SPTFWD_REC.MARKETINDICATOR
  is '交易类型';
comment on column IFS_CPISFX_SPTFWD_REC.SETTLDATE
  is '起息日';
comment on column IFS_CPISFX_SPTFWD_REC.LASTPX
  is '成交价格';
comment on column IFS_CPISFX_SPTFWD_REC.CURRENCY1
  is '买入币种';
comment on column IFS_CPISFX_SPTFWD_REC.LASTQTY
  is '买入金额';
comment on column IFS_CPISFX_SPTFWD_REC.CURRENCY2
  is '卖出币种';
comment on column IFS_CPISFX_SPTFWD_REC.CALCULATEDCCYLASTQTY
  is '卖出金额';
comment on column IFS_CPISFX_SPTFWD_REC.OTHPARTYID
  is '对手方CFETS编号';
comment on column IFS_CPISFX_SPTFWD_REC.OWNCAPITALBAKENAME
  is '本方资金开户名称             ';
comment on column IFS_CPISFX_SPTFWD_REC.OWNCAPITALACCOUNTNAME
  is '本方资金账户户名';
comment on column IFS_CPISFX_SPTFWD_REC.OWNCAPITALACCOUNT
  is '本方资金账号';
comment on column IFS_CPISFX_SPTFWD_REC.OWNCAPITALACCOUNTBIC
  is '本方开户行BICCODE';
comment on column IFS_CPISFX_SPTFWD_REC.OWNPAYBANKNO
  is '本方支付系统行号';
comment on column IFS_CPISFX_SPTFWD_REC.OTHCAPITALBANKNAME
  is '对手方开户行名称  (资金开户名称 CNY)';
comment on column IFS_CPISFX_SPTFWD_REC.OTHCAPITALACCOUNTNAME
  is '对手方收款行名称  (资金账户户名  CNY    )';
comment on column IFS_CPISFX_SPTFWD_REC.OTHCAPITALACCOUNT
  is '对手方收款行账号  (资金账号 CNY)';
comment on column IFS_CPISFX_SPTFWD_REC.MIDCNNAME
  is '对手方中间行名称';
comment on column IFS_CPISFX_SPTFWD_REC.MIDBANKBIC
  is '对手方中间行BICCODE';
comment on column IFS_CPISFX_SPTFWD_REC.MIDBANKACCOUNT
  is '对手方中间行账号';
comment on column IFS_CPISFX_SPTFWD_REC.OTHPAYBANKNO
  is '对手方开户行账号  (支付系统行号 CNY)';
comment on column IFS_CPISFX_SPTFWD_REC.OTHREMARK
  is '对手方附言';
comment on column IFS_CPISFX_SPTFWD_REC.OWNREMARK
  is '本方附言';
comment on column IFS_CPISFX_SPTFWD_REC.OTHCNNAME
  is '对手方机构中文全称';
comment on column IFS_CPISFX_SPTFWD_REC.OWNPARTYID
  is '本方CFETS编号';
comment on column IFS_CPISFX_SPTFWD_REC.CONFIRMSTATUS
  is '确认状态';
comment on column IFS_CPISFX_SPTFWD_REC.DEALTRANSTYPE
  is '成交单录入0-dealentry';
comment on column IFS_CPISFX_SPTFWD_REC.EXECTYPE
  is '成交状态F-已成交';
comment on column IFS_CPISFX_SPTFWD_REC.OWNCNFMTIME
  is '本方确认时间';
comment on column IFS_CPISFX_SPTFWD_REC.OTHCNFMTIME
  is '对手方确认时间';
comment on column IFS_CPISFX_SPTFWD_REC.OWNCNNAME
  is '本方机构中文全称';
comment on column IFS_CPISFX_SPTFWD_REC.OWNOPENACCOUNT
  is '开户行账号';
comment on column IFS_CPISFX_SPTFWD_REC.SECURITYID
  is '货币对';

create table IFS_CPISFX_SWAP_REC
(
    EXECID                       VARCHAR2(32),
    TRADEDATE                    VARCHAR2(8),
    CONFIRMID                    VARCHAR2(32),
    NETGROSSIND                  VARCHAR2(20),
    MARKETINDICATOR              VARCHAR2(2),
    NEARENDVALUEDATE             VARCHAR2(8),
    NEARENDPRICE                 VARCHAR2(20),
    NEARENDSELLAMOUNT            NUMBER(19,4),
    NEARENDBUYAMOUNT             NUMBER(19,4),
    NEARENDSELLCCY               VARCHAR2(3),
    NEARENDBUYCCY                VARCHAR2(3),
    FARENDVALUEDATE              VARCHAR2(8),
    FARENDPRICE                  VARCHAR2(20),
    FARENDSELLAMOUNT             NUMBER(19,4),
    FARENDBUYAMOUNT              NUMBER(19,4),
    FARENDSELLCCY                VARCHAR2(3),
    FARENDBUYCCY                 VARCHAR2(3),
    OWNPARTYID                   VARCHAR2(25),
    OTHPARTYID                   VARCHAR2(25),
    NEARENDOWNCAPITALACCOUNTNAME VARCHAR2(150),
    NEARENDOWNCAPITALACCOUNT     VARCHAR2(35),
    NEARENDOWNCAPITALBAKENAME    VARCHAR2(150),
    NEARENDOWNPAYBANKNO          VARCHAR2(35),
    NEARENDOWNREMARK             VARCHAR2(150),
    FARENDOWNRECEIPTBANKNAME     VARCHAR2(150),
    FARENDOWNRECEIPTBANKBIC      VARCHAR2(35),
    FARENDOWNRECEIPTBANKACCOUNT  VARCHAR2(35),
    FARENDOWNCAPITALBAKENAME     VARCHAR2(150),
    FARENDOWNCAPITALACCOUNTBIC   VARCHAR2(35),
    FARENDOWNCAPITALACCOUNT      VARCHAR2(35),
    FARENDMIDCNNAME              VARCHAR2(150),
    FARENDMIDBANKBIC             VARCHAR2(35),
    FARENDMIDBANKACCOUNT         VARCHAR2(35),
    FARENDOWNREMARK              VARCHAR2(150),
    NEARENDOTHRECEIPTBANKNAME    VARCHAR2(150),
    NEARENDOTHRECEIPTBANKBIC     VARCHAR2(35),
    NEARENDOTHCAPITALBAKENAME    VARCHAR2(150),
    NEARENDOTHCAPITALACCOUNTBIC  VARCHAR2(35),
    NEARENDOTHCAPITALACCOUNT     VARCHAR2(35),
    NEARENDMIDCNNAME             VARCHAR2(150),
    NEARENDMIDBANKBIC            VARCHAR2(35),
    NEARENDMIDBANKACCOUNT        VARCHAR2(35),
    NEARENDOTHREMARK             VARCHAR2(150),
    FARENDOTHCAPITALACCOUNTNAME  VARCHAR2(150),
    FARENDOTHCAPITALACCOUNT      VARCHAR2(35),
    FARENDOTHCAPITALBAKENAME     VARCHAR2(150),
    FARENDOTHPAYBANKNO           VARCHAR2(35),
    OTHCNNAME                    VARCHAR2(200),
    FARENDOTHREMARK              VARCHAR2(200),
    EXECTYPE                     VARCHAR2(10),
    DEALTRANSTYPE                VARCHAR2(10),
    CONFIRMSTATUS                VARCHAR2(10),
    OWNCNFMTIME                  VARCHAR2(20),
    OTHCNFMTIME                  VARCHAR2(20),
    OWNCNNAME                    VARCHAR2(200),
    LEGSIDE_1                    VARCHAR2(3),
    LEGSIDE_2                    VARCHAR2(3),
    SECURITYID                   VARCHAR2(10)
)
;
comment on table IFS_CPISFX_SWAP_REC
  is '外汇掉期交易确认信息返回';
comment on column IFS_CPISFX_SWAP_REC.EXECID
  is '成交编号';
comment on column IFS_CPISFX_SWAP_REC.TRADEDATE
  is '成交日期';
comment on column IFS_CPISFX_SWAP_REC.CONFIRMID
  is '确认报文ID';
comment on column IFS_CPISFX_SWAP_REC.NETGROSSIND
  is '清算方式';
comment on column IFS_CPISFX_SWAP_REC.MARKETINDICATOR
  is '11-外汇掉期';
comment on column IFS_CPISFX_SWAP_REC.NEARENDVALUEDATE
  is '近端起息日';
comment on column IFS_CPISFX_SWAP_REC.NEARENDPRICE
  is '近端成交价格';
comment on column IFS_CPISFX_SWAP_REC.NEARENDSELLAMOUNT
  is '近端卖出金额';
comment on column IFS_CPISFX_SWAP_REC.NEARENDBUYAMOUNT
  is '近端买入金额';
comment on column IFS_CPISFX_SWAP_REC.NEARENDSELLCCY
  is '近端卖出币种';
comment on column IFS_CPISFX_SWAP_REC.NEARENDBUYCCY
  is '近端买入币种';
comment on column IFS_CPISFX_SWAP_REC.FARENDVALUEDATE
  is '远端起息日';
comment on column IFS_CPISFX_SWAP_REC.FARENDPRICE
  is '远端成交价格';
comment on column IFS_CPISFX_SWAP_REC.FARENDSELLAMOUNT
  is '远端买入金额';
comment on column IFS_CPISFX_SWAP_REC.FARENDBUYAMOUNT
  is '远端卖出金额';
comment on column IFS_CPISFX_SWAP_REC.FARENDSELLCCY
  is '远端买入币种';
comment on column IFS_CPISFX_SWAP_REC.FARENDBUYCCY
  is '远端卖出币种';
comment on column IFS_CPISFX_SWAP_REC.OWNPARTYID
  is '本方21位机构号';
comment on column IFS_CPISFX_SWAP_REC.OTHPARTYID
  is '对手方21位机构号';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOWNCAPITALACCOUNTNAME
  is ' 本方近端 收款行名称   /资金账户户名  ';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOWNCAPITALACCOUNT
  is '本方近端 收款行账号  /资金账号';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOWNCAPITALBAKENAME
  is '本方近端 开户行名称  /资金开户名称 ';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOWNPAYBANKNO
  is '本方近端 开户行账号   /支付系统行号 ';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOWNREMARK
  is ' 本方近端 付言';
comment on column IFS_CPISFX_SWAP_REC.FARENDOWNRECEIPTBANKNAME
  is '本方远端 收款行名称   /资金账户户名';
comment on column IFS_CPISFX_SWAP_REC.FARENDOWNRECEIPTBANKBIC
  is '本方远端 收款行SWIFTCODE ';
comment on column IFS_CPISFX_SWAP_REC.FARENDOWNRECEIPTBANKACCOUNT
  is '本方远端 款行账号  /资金账号';
comment on column IFS_CPISFX_SWAP_REC.FARENDOWNCAPITALBAKENAME
  is '本方远端 开户行名称  /资金开户名称 ';
comment on column IFS_CPISFX_SWAP_REC.FARENDOWNCAPITALACCOUNTBIC
  is '本方远端资 金开户行SWIFT CODE ';
comment on column IFS_CPISFX_SWAP_REC.FARENDOWNCAPITALACCOUNT
  is '本方远端 开户行账号   /支付系统行号';
comment on column IFS_CPISFX_SWAP_REC.FARENDMIDCNNAME
  is ' 本方远端 中间行名称-209';
comment on column IFS_CPISFX_SWAP_REC.FARENDMIDBANKBIC
  is ' 本方远端 中间行SWIFT CODE/中间行行号-211';
comment on column IFS_CPISFX_SWAP_REC.FARENDMIDBANKACCOUNT
  is ' 本方远端 中间行在开户行的资金账号-213';
comment on column IFS_CPISFX_SWAP_REC.FARENDOWNREMARK
  is ' 本方远端 付言-139';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOTHRECEIPTBANKNAME
  is '对手方近端 收款行名称   /资金账户户名 ';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOTHRECEIPTBANKBIC
  is '对手方近端 收款行SWIFTCODE';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOTHCAPITALBAKENAME
  is '对手方近端 开户行名称  /资金开户名称';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOTHCAPITALACCOUNTBIC
  is '对手方近端 金开户行SWIFT CODE';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOTHCAPITALACCOUNT
  is '对手方近端 开户行账号   /支付系统行号';
comment on column IFS_CPISFX_SWAP_REC.NEARENDMIDCNNAME
  is '对手方近端 中间行名称-209';
comment on column IFS_CPISFX_SWAP_REC.NEARENDMIDBANKBIC
  is '对手方近端 中间行SWIFT CODE/中间行行号-211';
comment on column IFS_CPISFX_SWAP_REC.NEARENDMIDBANKACCOUNT
  is '对手方近端 中间行在开户行的资金账号-213';
comment on column IFS_CPISFX_SWAP_REC.NEARENDOTHREMARK
  is '对手方近端 付言-139';
comment on column IFS_CPISFX_SWAP_REC.FARENDOTHCAPITALACCOUNTNAME
  is '对手方远端 收款行名称   /资金账户户名 ';
comment on column IFS_CPISFX_SWAP_REC.FARENDOTHCAPITALACCOUNT
  is '对手方远端 款行账号  /资金账号';
comment on column IFS_CPISFX_SWAP_REC.FARENDOTHCAPITALBAKENAME
  is '对手方远端 开户行名称  /资金开户名称';
comment on column IFS_CPISFX_SWAP_REC.FARENDOTHPAYBANKNO
  is '对手方远端 开户行账号   /支付系统行号';
comment on column IFS_CPISFX_SWAP_REC.OTHCNNAME
  is '对手方机构中文全称';
comment on column IFS_CPISFX_SWAP_REC.FARENDOTHREMARK
  is '对手方远端 附言';
comment on column IFS_CPISFX_SWAP_REC.EXECTYPE
  is '成交状态';
comment on column IFS_CPISFX_SWAP_REC.DEALTRANSTYPE
  is '0-成交单录入';
comment on column IFS_CPISFX_SWAP_REC.CONFIRMSTATUS
  is '确认状态';
comment on column IFS_CPISFX_SWAP_REC.OWNCNFMTIME
  is '本方确认时间';
comment on column IFS_CPISFX_SWAP_REC.OTHCNFMTIME
  is '对手方确认时间';
comment on column IFS_CPISFX_SWAP_REC.OWNCNNAME
  is '对手方机构中文全称';
comment on column IFS_CPISFX_SWAP_REC.LEGSIDE_1
  is '近端交易方向';
comment on column IFS_CPISFX_SWAP_REC.LEGSIDE_2
  is '远端交易方向';
comment on column IFS_CPISFX_SWAP_REC.SECURITYID
  is '货币对';

-- Create table
create table IFS_CPIS_RECDUENOT
(
    trade_instrument VARCHAR2(20),
    market_indicator VARCHAR2(10),
    confirm_type     VARCHAR2(10),
    transaction_num  VARCHAR2(10),
    message          CLOB,
    updatetime       DATE
);
-- Add comments to the table
comment on table IFS_CPIS_RECDUENOT
  is '交易后-接收到期未推送';
-- Add comments to the columns
comment on column IFS_CPIS_RECDUENOT.trade_instrument
  is '市场类型';
comment on column IFS_CPIS_RECDUENOT.market_indicator
  is '交易类型';
comment on column IFS_CPIS_RECDUENOT.confirm_type
  is '类型';
comment on column IFS_CPIS_RECDUENOT.transaction_num
  is '到期未确认笔数';
comment on column IFS_CPIS_RECDUENOT.message
  is '报文信息';
comment on column IFS_CPIS_RECDUENOT.updatetime
  is '更新时间';


-- Create table
create table IFS_CPIS_FAILREC
(
    affirm_status    VARCHAR2(10),
    match_status     VARCHAR2(10),
    query_request_id VARCHAR2(50),
    text             VARCHAR2(100),
    message          CLOB,
    updatetime       DATE
);
-- Add comments to the table
comment on table IFS_CPIS_FAILREC
  is '交易后-接收失败';
-- Add comments to the columns
comment on column IFS_CPIS_FAILREC.affirm_status
  is '0-数据匹配  其他-对应的问题编码';
comment on column IFS_CPIS_FAILREC.match_status
  is '查询匹配状态 1-数据不匹配';
comment on column IFS_CPIS_FAILREC.query_request_id
  is '查询请求编号 ';
comment on column IFS_CPIS_FAILREC.text
  is '未匹配成功字段';
comment on column IFS_CPIS_FAILREC.message
  is '报文信息';
comment on column IFS_CPIS_FAILREC.updatetime
  is '更新时间';
