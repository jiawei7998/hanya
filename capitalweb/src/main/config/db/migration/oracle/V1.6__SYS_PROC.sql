
--prompt
--prompt Creating procedure ACT_TABLE_CLEAR
--prompt ==================================
--prompt
create or replace procedure ACT_TABLE_CLEAR(
       D_ID    IN VARCHAR
) is
begin
  delete from ACT_RE_PROCDEF where DEPLOYMENT_ID_ = D_ID;
  delete from ACT_GE_BYTEARRAY where DEPLOYMENT_ID_ = D_ID;
  delete from ACT_RE_DEPLOYMENT where ID_ = D_ID;
commit;
end ACT_TABLE_CLEAR;
/

--prompt
--prompt Creating procedure FUNDSYSTEMDATADEL
--prompt ====================================
--prompt
CREATE OR REPLACE PROCEDURE FUNDSYSTEMDATADEL( VS_BRANCHID   IN VARCHAR, --系统分支ID,一般采用SWIFT
                                               O_RETCODE    OUT VARCHAR,
                                               O_RETMSG     OUT VARCHAR) IS

  VS_ROLEID   VARCHAR2(2);

BEGIN
  O_RETCODE:='999';
  O_RETMSG:='成功';

SELECT BRANCH_CN INTO VS_ROLEID  FROM TA_BRCC WHERE BRANCH_ID = VS_BRANCHID;
--删除TA_BRCC
DELETE FROM TA_BRCC WHERE BRANCH_ID <> VS_BRANCHID;
--删除关联关系表
DELETE FROM TA_USER_INST_MAP WHERE USER_ID IN(SELECT USER_ID FROM TA_USER WHERE BRANCH_ID <> VS_BRANCHID ) AND INST_ID IN (SELECT INST_ID FROM TT_INSTITUTION WHERE BRANCH_ID <> VS_BRANCHID);
--删除用户
DELETE FROM TA_USER WHERE BRANCH_ID <> VS_BRANCHID;
--删除机构表
DELETE FROM TT_INSTITUTION WHERE BRANCH_ID <> VS_BRANCHID;
--删除用户角色关联关系表
DELETE FROM TA_USER_ROLE_MAP WHERE ROLE_ID NOT LIKE '%'||VS_ROLEID||'%';
--删除角色表
DELETE FROM TA_ROLE WHERE BRANCH_ID <> VS_BRANCHID;
--删除角色机构关联关系表
DELETE FROM TA_ROLE_INST_MAP WHERE ROLE_ID NOT LIKE   '%'||VS_ROLEID||'%';
--删除台子表
DELETE FROM TA_DESK_MENU WHERE BRANCH_ID <> VS_BRANCHID ;
--删除角色台子关系表
DELETE FROM TA_ROLE_DESK_MENU_MAP WHERE  ROLE_ID NOT LIKE '%'||VS_ROLEID||'%';
--删除菜单表
DELETE FROM TA_MODULE WHERE  BRANCH_ID <> VS_BRANCHID;
--删除角色菜单关系表
DELETE FROM TA_ROLE_MODULE_MAP WHERE  ROLE_ID NOT LIKE '%'||VS_ROLEID||'%';
--删除功能菜单表
DELETE FROM TA_FUNCTION WHERE  BRANCH_ID <> VS_BRANCHID;
--删除关联关系表
DELETE FROM TA_ROLE_FUNCTION_MAP WHERE ROLE_ID NOT LIKE '%'||VS_ROLEID||'%';
--删除产品表
DELETE FROM  TC_PRODUCT WHERE  BRANCH_ID <> VS_BRANCHID;
 COMMIT;
END FUNDSYSTEMDATADEL;
/

--prompt
--prompt Creating procedure FUNDSYSTEMDATADELBYID
--prompt ========================================
--prompt
CREATE OR REPLACE PROCEDURE FUNDSYSTEMDATADELBYID( VS_BRANCHID   IN VARCHAR, --系统分支ID,一般采用SWIFT
                                               O_RETCODE    OUT VARCHAR,
                                               O_RETMSG     OUT VARCHAR) IS

  VS_ROLEID   VARCHAR2(2);

BEGIN
  O_RETCODE:='999';
  O_RETMSG:='成功';

SELECT BRANCH_CN INTO VS_ROLEID  FROM TA_BRCC WHERE BRANCH_ID = VS_BRANCHID;
--删除TA_BRCC
DELETE FROM TA_BRCC WHERE BRANCH_ID = VS_BRANCHID;
--删除关联关系表
DELETE FROM TA_USER_INST_MAP WHERE USER_ID IN(SELECT USER_ID FROM TA_USER WHERE BRANCH_ID = VS_BRANCHID ) AND INST_ID IN (SELECT INST_ID FROM TT_INSTITUTION WHERE BRANCH_ID = VS_BRANCHID);
--删除用户
DELETE FROM TA_USER WHERE BRANCH_ID = VS_BRANCHID;
--删除机构表
DELETE FROM TT_INSTITUTION WHERE BRANCH_ID  = VS_BRANCHID;
--删除用户角色关联关系表
DELETE FROM TA_USER_ROLE_MAP WHERE ROLE_ID  LIKE '%'||VS_ROLEID||'%';
--删除角色表
DELETE FROM TA_ROLE WHERE BRANCH_ID = VS_BRANCHID;
--删除角色机构关联关系表
DELETE FROM TA_ROLE_INST_MAP WHERE ROLE_ID  LIKE   '%'||VS_ROLEID||'%';
--删除台子表
DELETE FROM TA_DESK_MENU WHERE BRANCH_ID = VS_BRANCHID ;
--删除角色台子关系表
DELETE FROM TA_ROLE_DESK_MENU_MAP WHERE  ROLE_ID  LIKE '%'||VS_ROLEID||'%';
--删除菜单表
DELETE FROM TA_MODULE WHERE  BRANCH_ID = VS_BRANCHID;
--删除角色菜单关系表
DELETE FROM TA_ROLE_MODULE_MAP WHERE  ROLE_ID  LIKE '%'||VS_ROLEID||'%';
--删除功能菜单表
DELETE FROM TA_FUNCTION WHERE  BRANCH_ID = VS_BRANCHID;
--删除关联关系表
DELETE FROM TA_ROLE_FUNCTION_MAP WHERE ROLE_ID  LIKE '%'||VS_ROLEID||'%';
--删除产品表
DELETE FROM  TC_PRODUCT WHERE  BRANCH_ID = VS_BRANCHID;
 COMMIT;
END FUNDSYSTEMDATADELBYID;
/

--prompt
--prompt Creating procedure FUNDSYSTEMDATAINIT
--prompt =====================================
--prompt
CREATE OR REPLACE PROCEDURE FUNDSYSTEMDATAINIT(VS_BRANCHID   IN VARCHAR, --系统分支ID,一般采用SWIFT
                                               VS_BRANCHNM   IN VARCHAR,--系统分支名称，一般银行名称
                                               VS_SYSTEMNM   IN VARCHAR,--系统名称
                                               VS_SYSTEMVERSION IN VARCHAR,--系统版本号
                                               O_RETCODE    OUT VARCHAR,
                                               O_RETMSG     OUT VARCHAR) IS

  VS_ROLEID   VARCHAR2(20);

BEGIN
  O_RETCODE:='999';
  O_RETMSG:='成功';

VS_ROLEID:=SUBSTR(VS_BRANCHID, 1, 2)||'0000000';
---插入TA_BRCC
DELETE FROM TA_BRCC WHERE BRANCH_ID = VS_BRANCHID;

--插入人员
DELETE FROM TA_USER WHERE BRANCH_ID =VS_BRANCHID AND USER_ID='singlee';

DELETE FROM TA_USER WHERE BRANCH_ID =VS_BRANCHID AND USER_ID='admin';


--插入机构
DELETE FROM TT_INSTITUTION WHERE  BRANCH_ID =VS_BRANCHID AND INST_ID ='99999999';

--插入角色
DELETE FROM TA_ROLE WHERE  BRANCH_ID =VS_BRANCHID AND ROLE_ID= VS_ROLEID;


--插入TA_DESK
DELETE FROM TA_DESK_MENU WHERE BRANCH_ID = VS_BRANCHID AND DESK_ID='M999999';
--新增系统台子 --JSBANK
DELETE FROM TA_ROLE_DESK_MENU_MAP WHERE  DESK_ID='M999999'AND ROLE_ID=VS_ROLEID;

--插入MODULE
DELETE FROM TA_MODULE WHERE MODULE_ID IN('99-2','99-4','99-3','99-1','99-2-1','99-3-1','99-3-2','99-4-1','99-4-2','99-4-3','99-4-5','99-4-8','99-1-1','99-1-2') AND   BRANCH_ID =VS_BRANCHID;

DELETE FROM TA_ROLE_MODULE_MAP WHERE MODULE_ID IN('99-2','99-4','99-3','99-1','99-2-1','99-3-1','99-3-2','99-4-1','99-4-2','99-4-3','99-4-5','99-4-8','99-1-1','99-1-2') AND ROLE_ID=VS_ROLEID;




DELETE FROM TA_ROLE_FUNCTION_MAP WHERE MODULE_ID IN('99-2','99-4','99-3','99-1','99-2-1','99-3-1','99-3-2','99-4-1','99-4-2','99-4-3','99-4-5','99-4-8','99-1-1','99-1-2') AND ROLE_ID=VS_ROLEID;


  COMMIT;
END FUNDSYSTEMDATAINIT;
/



--prompt
--prompt Creating procedure SL_SP_DELETE
--prompt ===============================
--prompt
CREATE OR REPLACE PROCEDURE SL_SP_DELETE AS
  TYPE name_list IS TABLE OF VARCHAR2(140);
  TYPE type_list IS TABLE OF VARCHAR2(120);
  Tab_name name_list := name_list();
  Tab_type type_list := type_list();
  sql_str  VARCHAR2(500);

BEGIN
  begin
    for item in (select *
                   from user_constraints a
                  where a.constraint_type = 'R') loop
      execute immediate 'alter table ' || item.table_name ||
                        ' drop constraint ' || item.constraint_name;
    end loop;
  end;
  sql_str := 'select uo.object_name,uo.object_type from user_objects uo where uo.object_type not in(''INDEX'',''LOB'') order by uo.object_type desc';
  EXECUTE IMMEDIATE sql_str BULK COLLECT
    INTO tab_name, tab_type;
  FOR i IN Tab_name.FIRST .. Tab_name.LAST LOOP
    sql_str := 'DROP ' || Tab_type(i) || ' ' || Tab_name(i);
    IF (Tab_name(i) != 'SL_SP_DELETE' and
       Tab_name(i) != 'tib_TT_PROC_ACTIVITY_CREATION') then
      --dbms_output.put_line(sql_str);
      EXECUTE IMMEDIATE sql_str;
    end if;
  END LOOP;
END SL_SP_DELETE;
/

