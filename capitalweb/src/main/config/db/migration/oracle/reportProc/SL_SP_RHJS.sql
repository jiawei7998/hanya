-- 存量同业借贷
DROP table TB_RH_CLTYJD ;

create table TB_RH_CLTYJD
(	
  BANKINISTCODE VARCHAR2(20),
  INSIDEINISTCODE VARCHAR2(20),
  shcode       VARCHAR2(20),
  shtype       VARCHAR2(20),
  DEALNO       VARCHAR2(20),
  AL 		   VARCHAR2(20),
  PRODUCT      VARCHAR2(20),
  VDATE	       VARCHAR2(20),
  MDATE        VARCHAR2(20),
  CCY          VARCHAR2(20),
  CCYAMT       NUMBER(19,4),
  SPOTRATE_8   NUMBER(19,4),
  ISFIXED      VARCHAR2(10),
  INTRATE      NUMBER(19,4),
  INTRATETYPE  VARCHAR2(10),
  ACRATE       NUMBER(19,4),
  INTPAYCYCLE  VARCHAR2(10)
);
comment on table TB_RH_CLTYJD
  is '人行金数-存量同业借贷';
-- Add comments to the columns 
comment on column TB_RH_CLTYJD.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_CLTYJD.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_CLTYJD.shcode
  is '交易对手代码';
comment on column TB_RH_CLTYJD.shtype
  is '交易对手代码类型';
comment on column TB_RH_CLTYJD.DEALNO
  is '合同编码';
comment on column TB_RH_CLTYJD.AL
  is '资产负债类型';
comment on column TB_RH_CLTYJD.PRODUCT
  is '产品类型';
comment on column TB_RH_CLTYJD.VDATE
  is '合同起始日期';
 comment on column TB_RH_CLTYJD.MDATE
  is '合同到期日期';
 comment on column TB_RH_CLTYJD.CCY
  is '币种';
 comment on column TB_RH_CLTYJD.CCYAMT
  is '合同余额';
 comment on column TB_RH_CLTYJD.SPOTRATE_8
  is '合同余额折人名币';
 comment on column TB_RH_CLTYJD.ISFIXED
  is '利率是否固定';
  comment on column TB_RH_CLTYJD.INTRATE
  is '利率水平';
  comment on column TB_RH_CLTYJD.INTRATETYPE
  is '定价基准类型';
  comment on column TB_RH_CLTYJD.ACRATE
  is '基准利率';
  comment on column TB_RH_CLTYJD.INTPAYCYCLE
  is '计息方式';

--同业借贷发生额
DROP table TB_RH_TYJDFSE;

create table TB_RH_TYJDFSE
(
  BANKINISTCODE VARCHAR2(20),
  INSIDEINISTCODE VARCHAR2(20),
  shcode       VARCHAR2(20),
  shtype       VARCHAR2(20),
  DEALNO       VARCHAR2(20),
  FEDEALNO     VARCHAR2(20),
  AL 		   VARCHAR2(20),
  PRODUCT      VARCHAR2(20),
  VDATE	       VARCHAR2(20),
  MDATE        VARCHAR2(20),
  AMDATE 	   VARCHAR2(20),
  CCY          VARCHAR2(20),
  CCYAMT       NUMBER(19,4),
  SPOTRATE     NUMBER(19,4),
  ISFIXED      VARCHAR2(10),
  INTRATE      NUMBER(19,4),
  INTRATETYPE  VARCHAR2(10),
  ACRATE       NUMBER(19,4),
  INTPAYCYCLE  VARCHAR2(10),
  logo         VARCHAR2(10)
);
comment on table TB_RH_TYJDFSE
  is '人行金数-同业借贷发生额';
-- Add comments to the columns 
comment on column TB_RH_TYJDFSE.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_TYJDFSE.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_TYJDFSE.shcode
  is '交易对手代码';
comment on column TB_RH_TYJDFSE.shtype
  is '交易对手代码类型';
comment on column TB_RH_TYJDFSE.DEALNO
  is '合同编码';
comment on column TB_RH_TYJDFSE.FEDEALNO
  is '交易流水号';
comment on column TB_RH_TYJDFSE.AL
  is '资产负债类型';
comment on column TB_RH_TYJDFSE.PRODUCT
  is '产品类型';
comment on column TB_RH_TYJDFSE.VDATE
  is '合同起始日期';
comment on column TB_RH_TYJDFSE.MDATE
  is '合同到期日期';
comment on column TB_RH_TYJDFSE.AMDATE
  is '合同实际终止日期';
comment on column TB_RH_TYJDFSE.CCY
  is '币种';
comment on column TB_RH_TYJDFSE.CCYAMT
  is '发生金额';
comment on column TB_RH_TYJDFSE.SPOTRATE
  is '发生金额折人名币';
comment on column TB_RH_TYJDFSE.ISFIXED
  is '利率是否固定';
comment on column TB_RH_TYJDFSE.INTRATE
  is '利率水平';
comment on column TB_RH_TYJDFSE.INTRATETYPE
  is '定价基准类型';
comment on column TB_RH_TYJDFSE.ACRATE
  is '基准利率';
comment on column TB_RH_TYJDFSE.INTPAYCYCLE
  is '计息方式';
comment on column TB_RH_TYJDFSE.logo
  is '发生结清标识';

--存量同业存款
DROP table TB_RH_CLTYCK;
create table TB_RH_CLTYCK
(
  BANKINISTCODE VARCHAR2(20),
  INSIDEINISTCODE VARCHAR2(20),
  prodtype       VARCHAR2(32) ,
  shtype         VARCHAR2(32) ,
  shcode  	     VARCHAR2(20) ,
  accountcode    VARCHAR2(20) ,
  protocolcode   VARCHAR2(32) ,
  VDATE	         VARCHAR2(20),
  MDATE          VARCHAR2(20),
  CCY            VARCHAR2(10),
  CCYAMT         NUMBER(19,4),
  spotrate       NUMBER(19,4),
  intrate        NUMBER(19,4),
  payway         VARCHAR2(40)
);
comment on table TB_RH_CLTYCK
  is '人行金数-存量同业存款';
-- Add comments to the columns 
comment on column TB_RH_CLTYCK.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_CLTYCK.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_CLTYCK.prodtype
  is '业务类型';
comment on column TB_RH_CLTYCK.shtype
  is '交易对手证件类型';
comment on column TB_RH_CLTYCK.shcode
  is '交易对手代码';
comment on column TB_RH_CLTYCK.ACCOUNTCODE
  is '存款账户编码';
comment on column TB_RH_CLTYCK.protocolcode
  is '存款协议代码';
comment on column TB_RH_CLTYCK.VDATE
  is '协议起始日期';
comment on column TB_RH_CLTYCK.MDATE
  is '协议到期日期';
comment on column TB_RH_CLTYCK.CCY
  is '币种';
comment on column TB_RH_CLTYCK.CCYAMT
  is '存款余额';
comment on column TB_RH_CLTYCK.spotrate
  is '存款余额折人名币';
comment on column TB_RH_CLTYCK.intrate
  is '利率水平';
comment on column TB_RH_CLTYCK.payway
  is '缴存准备进方式';


--同业存款发生额
DROP TABLE  TB_RH_TYCKFSE;
  
create table TB_RH_TYCKFSE
(
  BANKINISTCODE   VARCHAR2(20),
  INSIDEINISTCODE VARCHAR2(20),
  prodtype       VARCHAR2(32) ,
  shtype         VARCHAR2(32) ,
  shcode  	     VARCHAR2(20) ,
  accountcode    VARCHAR2(20) ,
  protocolcode   VARCHAR2(32) ,
  VDATE	         VARCHAR2(20),
  MDATE          VARCHAR2(20),
  CCY            VARCHAR2(10),
  CCYAMT         NUMBER(19,4),
  spotrate       NUMBER(19,4),
  DEALDATE       VARCHAR2(20),
  intrate        NUMBER(19,4),
  PAY_USERID     VARCHAR2(32),
  PAY_BANKID     VARCHAR2(32),
  REC_USERID     VARCHAR2(32),
  logo           VARCHAR2(40)
);
comment on table TB_RH_TYCKFSE
  is '人行金数-同业存款发生额';
-- Add comments to the columns 
comment on column TB_RH_TYCKFSE.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_TYCKFSE.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_TYCKFSE.prodtype
  is '业务类型';
comment on column TB_RH_TYCKFSE.shtype
  is '交易对手证件类型';
comment on column TB_RH_TYCKFSE.shcode
  is '交易对手代码';
comment on column TB_RH_TYCKFSE.accountcode
  is '存款账户编码';
comment on column TB_RH_TYCKFSE.protocolcode
  is '存款协议编码';
comment on column TB_RH_TYCKFSE.VDATE
  is '协议起始日期';
comment on column TB_RH_TYCKFSE.MDATE
  is '协议到期日期';
comment on column TB_RH_TYCKFSE.CCY
  is '币种';
comment on column TB_RH_TYCKFSE.CCYAMT
  is '交易余额';
comment on column TB_RH_TYCKFSE.spotrate
  is '交易余额折人名币';
comment on column TB_RH_TYCKFSE.DEALDATE
  is '交易日期';
comment on column TB_RH_TYCKFSE.intrate
  is '利率水平';
comment on column TB_RH_TYCKFSE.PAY_USERID
  is '交易账户号';
comment on column TB_RH_TYCKFSE.PAY_BANKID
  is '交易账户开户行号';
comment on column TB_RH_TYCKFSE.REC_USERID
  is '交易对手账户号';
comment on column TB_RH_TYCKFSE.logo
  is '交易方向';
  
  
 --同业客户基本信息
DROP TABLE  TB_RH_CUST;
  
create table TB_RH_CUST
(
  BANKINISTCODE   	VARCHAR2(20),
  cfn			 	VARCHAR2(40),
  SHCODE         	VARCHAR2(32) ,
  INSTITUCODE    	VARCHAR2(32) ,
  cno  	         	VARCHAR2(40) ,
  basicAccount    	VARCHAR2(40) ,
  basicAccountName  VARCHAR2(32) ,
  CA	        	 VARCHAR2(40),
  CCODE         	 VARCHAR2(40),
  fintype            VARCHAR2(40),
  FOUNDDATE         VARCHAR2(32),
  ISACC       		VARCHAR2(32),
  ECING       		VARCHAR2(20),
  ECOTYPE       	VARCHAR2(32),
  CRE    			VARCHAR2(32),
  CRELEVEL     		VARCHAR2(32)
);
comment on table TB_RH_CUST
  is '人行金数-同业客户基本信息';
-- Add comments to the columns 
comment on column TB_RH_CUST.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_CUST.cfn
  is '客户名称';
comment on column TB_RH_CUST.SHCODE
  is '客户代码';
comment on column TB_RH_CUST.INSTITUCODE
  is '客户金融机构编码';
comment on column TB_RH_CUST.cno
  is '客户内部编码';
comment on column TB_RH_CUST.basicAccount
  is '基本存款账号';
comment on column TB_RH_CUST.basicAccountName
  is '基本账户开户行名称';
comment on column TB_RH_CUST.CA
  is '注册地址';
comment on column TB_RH_CUST.CCODE
  is '地区代码';
comment on column TB_RH_CUST.fintype
  is '客户类别';
comment on column TB_RH_CUST.FOUNDDATE
  is '成立日期';
comment on column TB_RH_CUST.ISACC
  is '是否关联方';
comment on column TB_RH_CUST.ECING
  is '客户经济成分';
comment on column TB_RH_CUST.ECOTYPE
  is '客户国民经济部门';
comment on column TB_RH_CUST.CRE
  is '客户信用级别总等级数';
comment on column TB_RH_CUST.CRELEVEL
  is '客户信用评级';

--人行金数-存量债券投资信息
DROP TABLE TB_RH_CLZQTZ;

create table TB_RH_CLZQTZ
(
  BANKINISTCODE   	VARCHAR2(20),
  INSIDEINISTCODE 	VARCHAR2(20),
  BNDCD	        	VARCHAR2(32) ,
  ACCOUNTNO         VARCHAR2(32) ,
  BONDPROPERTIES  	VARCHAR2(32) ,
  BONDRATING   		VARCHAR2(32) ,
  CCY  	  			VARCHAR2(32) ,
  PRINAMT  	  		NUMBER(19,4) ,
  SPOTRATE  	 	NUMBER(19,4) ,
  BOND_RIGHT_DEBT_DATE  	VARCHAR2(40),
  VDATE  	  				VARCHAR2(40),
  MDATE   				    VARCHAR2(40),
  COUPRATE_8	          	NUMBER(19,4),
  SHTYPE           			VARCHAR2(40),
  CCODE             		VARCHAR2(40),
  INDTYPE         		    VARCHAR2(32),
  COMSCALE      			VARCHAR2(32),
  ECING        				VARCHAR2(40),
  ECOTYPE          			VARCHAR2(40)
 
);
comment on table TB_RH_CLZQTZ
  is '人行金数-存量债券投资信息';
-- Add comments to the columns 
comment on column TB_RH_CLZQTZ.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_CLZQTZ.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_CLZQTZ.BNDCD
  is '债券代码';
comment on column TB_RH_CLZQTZ.ACCOUNTNO
  is '债券总托管机构';
comment on column TB_RH_CLZQTZ.BONDPROPERTIES
  is '债券品种';
comment on column TB_RH_CLZQTZ.BONDRATING
  is '债券信用级别';
comment on column TB_RH_CLZQTZ.CCY
  is '币种';
comment on column TB_RH_CLZQTZ.PRINAMT
  is '债券余额';
comment on column TB_RH_CLZQTZ.SPOTRATE
  is '债券余额折人名币';
comment on column TB_RH_CLZQTZ.BOND_RIGHT_DEBT_DATE
  is '债权债务登记日';
comment on column TB_RH_CLZQTZ.VDATE
  is '起息日';
comment on column TB_RH_CLZQTZ.MDATE
  is '兑付日期';
comment on column TB_RH_CLZQTZ.COUPRATE_8
  is '票面利率';
comment on column TB_RH_CLZQTZ.SHTYPE
  is '发行人证件代码';
comment on column TB_RH_CLZQTZ.CCODE
  is '发行人地区代码';
comment on column TB_RH_CLZQTZ.INDTYPE
  is '发行人行业';
comment on column TB_RH_CLZQTZ.COMSCALE
  is '发行人企业规模';
comment on column TB_RH_CLZQTZ.ECING
  is '发行人经济成分';
comment on column TB_RH_CLZQTZ.ECOTYPE
  is '发行人国民经济部门';
  
 --债券投资发生额信息
 DROP TABLE TB_RH_ZQTZFSE;
 create table TB_RH_ZQTZFSE
(
  BANKINISTCODE   	VARCHAR2(20),
  INSIDEINISTCODE 	VARCHAR2(20),
  BNDCD	        	VARCHAR2(32) ,
  ACCOUNTNO         VARCHAR2(32) ,
  BONDPROPERTIES  	VARCHAR2(32) ,
  BONDRATING   		VARCHAR2(32) ,
  CCY  	  			VARCHAR2(32) ,
  PRINAMT  	  		NUMBER(19,4) ,
  SPOTRATE  	 	NUMBER(19,4) ,
  BOND_RIGHT_DEBT_DATE  	VARCHAR2(40),
  VDATE  	  				VARCHAR2(40),
  MDATE   				    VARCHAR2(40),
  COUPRATE_8	          	NUMBER(19,4),
  SHTYPE           			VARCHAR2(40),
  CCODE             		VARCHAR2(40),
  INDTYPE         		    VARCHAR2(32),
  COMSCALE      			VARCHAR2(32),
  ECING        				VARCHAR2(40),
  ECOTYPE          			VARCHAR2(40),
  DEALDATE					VARCHAR2(40),
  FEDEALNO					VARCHAR2(40),
  LOGO						VARCHAR2(10)
 
);
comment on table TB_RH_ZQTZFSE
  is '人行金数-债券投资信息发生额';
-- Add comments to the columns 
comment on column TB_RH_ZQTZFSE.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_ZQTZFSE.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_ZQTZFSE.BNDCD
  is '债券代码';
comment on column TB_RH_ZQTZFSE.ACCOUNTNO
  is '债券总托管机构';
comment on column TB_RH_ZQTZFSE.BONDPROPERTIES
  is '债券品种';
comment on column TB_RH_ZQTZFSE.BONDRATING
  is '债券信用级别';
comment on column TB_RH_ZQTZFSE.CCY
  is '币种';
comment on column TB_RH_ZQTZFSE.PRINAMT
  is '成交金额';
comment on column TB_RH_ZQTZFSE.SPOTRATE
  is '成交金额折人名币';
comment on column TB_RH_ZQTZFSE.BOND_RIGHT_DEBT_DATE
  is '债权债务登记日';
comment on column TB_RH_ZQTZFSE.VDATE
  is '起息日';
comment on column TB_RH_ZQTZFSE.MDATE
  is '兑付日期';
comment on column TB_RH_ZQTZFSE.COUPRATE_8
  is '票面利率';
comment on column TB_RH_ZQTZFSE.SHTYPE
  is '发行人证件代码';
comment on column TB_RH_ZQTZFSE.CCODE
  is '发行人地区代码';
comment on column TB_RH_ZQTZFSE.INDTYPE
  is '发行人行业';
comment on column TB_RH_ZQTZFSE.COMSCALE
  is '发行人企业规模';
comment on column TB_RH_ZQTZFSE.ECING
  is '发行人经济成分';
comment on column TB_RH_ZQTZFSE.ECOTYPE
  is '发行人国民经济部门';
comment on column TB_RH_ZQTZFSE.DEALDATE
  is '交易日期';
comment on column TB_RH_ZQTZFSE.FEDEALNO
  is '交易流水号';
comment on column TB_RH_ZQTZFSE.LOGO
  is '买入卖出标记';
  
  
 --存量债券投资信息
 drop table TB_RH_CLZQFX ;
 create table TB_RH_CLZQFX
(
  BANKINISTCODE   	VARCHAR2(20),
  INSIDEINISTCODE 	VARCHAR2(20),
  BNDCD	        	VARCHAR2(32) ,
  ACCOUNTNO         VARCHAR2(32) ,
  BONDPROPERTIES  	VARCHAR2(32) ,
  BONDRATING   		VARCHAR2(32) ,
  REISSUE_FREQUENCY VARCHAR2(32) ,
  CCY  	  			VARCHAR2(32) ,
  PRINAMT  	  		NUMBER(19,4) ,
  SPOTRATE1  	 	NUMBER(19,4) ,
  SETTAVGCOST  	  	NUMBER(19,4) ,
  SPOTRATE2  	 	NUMBER(19,4) ,
  BOND_RIGHT_DEBT_DATE  	VARCHAR2(40),
  VDATE  	  				VARCHAR2(40),
  MDATE   				    VARCHAR2(40),
  INTCALCRULE				VARCHAR2(40),
  COUPRATE_8	          	NUMBER(19,4)
 
);
comment on table TB_RH_CLZQFX
  is '人行金数-存量债券发行信息';
-- Add comments to the columns 
comment on column TB_RH_CLZQFX.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_CLZQFX.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_CLZQFX.BNDCD
  is '债券代码';
comment on column TB_RH_CLZQFX.ACCOUNTNO
  is '债券总托管机构';
comment on column TB_RH_CLZQFX.BONDPROPERTIES
  is '债券品种';
comment on column TB_RH_CLZQFX.BONDRATING
  is '债券信用级别';
 comment on column TB_RH_CLZQFX.REISSUE_FREQUENCY
  is '续发次数';
comment on column TB_RH_CLZQFX.CCY
  is '币种';
comment on column TB_RH_CLZQFX.PRINAMT
  is '期末债券面值';
comment on column TB_RH_CLZQFX.SPOTRATE1
  is '期末债券面值折人民币(元)';
comment on column TB_RH_CLZQFX.SETTAVGCOST
  is '期末债券余额';
comment on column TB_RH_CLZQFX.SPOTRATE2
  is '期末债券余额折人民币(元)';
comment on column TB_RH_CLZQFX.BOND_RIGHT_DEBT_DATE
  is '债权债务登记日';
comment on column TB_RH_CLZQFX.VDATE
  is '起息日';
comment on column TB_RH_CLZQFX.MDATE
  is '兑付日期';
comment on column TB_RH_CLZQFX.INTCALCRULE
  is '付息方式';
comment on column TB_RH_CLZQFX.COUPRATE_8
  is '票面利率';

--债券发行发生额信息
DROP table TB_RH_ZQFXFSE;

create table TB_RH_ZQFXFSE
(
  BANKINISTCODE   	VARCHAR2(20),
  BNDCD	        	VARCHAR2(32) ,
  ACCOUNTNO         VARCHAR2(32) ,
  BONDPROPERTIES  	VARCHAR2(32) ,
  BONDRATING   		VARCHAR2(32) ,
  REISSUE_FREQUENCY VARCHAR2(32) ,
  CCY  	  			VARCHAR2(32) ,
  PRINAMT  	  		NUMBER(19,4) ,
  SPOTRATE1  	 	NUMBER(19,4) ,
  PROCEEDAMT  	  	NUMBER(19,4) ,
  SPOTRATE2  	 	NUMBER(19,4) ,
  BOND_RIGHT_DEBT_DATE  	VARCHAR2(40),
  VDATE  	  				VARCHAR2(40),
  MDATE   				    VARCHAR2(40),
  dealdate   			    VARCHAR2(40),
  COUPRATE_8	          	NUMBER(19,4),
  logo						VARCHAR2(10)
);
comment on table TB_RH_ZQFXFSE
  is '人行金数-债券发行发生额信息';
-- Add comments to the columns 
comment on column TB_RH_ZQFXFSE.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_ZQFXFSE.BNDCD
  is '债券代码';
comment on column TB_RH_ZQFXFSE.ACCOUNTNO
  is '债券总托管机构';
comment on column TB_RH_ZQFXFSE.BONDPROPERTIES
  is '债券品种';
comment on column TB_RH_ZQFXFSE.BONDRATING
  is '债券信用级别';
 comment on column TB_RH_ZQFXFSE.REISSUE_FREQUENCY
  is '续发次数';
comment on column TB_RH_ZQFXFSE.CCY
  is '币种';
comment on column TB_RH_ZQFXFSE.PRINAMT
  is '发行/兑付债券面值';
comment on column TB_RH_ZQFXFSE.SPOTRATE1
  is '发行/兑付债券面值折人民币(元)';
comment on column TB_RH_ZQFXFSE.PROCEEDAMT
  is '发行/兑付债券金额';
comment on column TB_RH_ZQFXFSE.SPOTRATE2
  is '发行/兑付债券金额折人民币(元)';
comment on column TB_RH_ZQFXFSE.BOND_RIGHT_DEBT_DATE
  is '债权债务登记日';
comment on column TB_RH_ZQFXFSE.VDATE
  is '起息日';
comment on column TB_RH_ZQFXFSE.MDATE
  is '兑付日期';
comment on column TB_RH_ZQFXFSE.dealdate
  is '交易日期';
comment on column TB_RH_ZQFXFSE.COUPRATE_8
  is '票面利率';
comment on column TB_RH_ZQFXFSE.logo
  is '发行兑付标识';

 --存量特定目的载体投资信息
 DROP TABLE TB_RH_CLFUND;
 create table TB_RH_CLFUND
( 
  BANKINISTCODE 	VARCHAR2(20),
  INSIDEINISTCODE 	VARCHAR2(20),
  fundtypes   		VARCHAR2(20),
  MANAGE_PRODUCT_TYPE 	VARCHAR2(20),
  FUND_CODE	        	VARCHAR2(32) ,
  issuercode         	VARCHAR2(32) ,
  issuerarea  			VARCHAR2(32) ,
  NET_WORTH_TYPE   		VARCHAR2(32) ,
  AFP_DATE 				VARCHAR2(32) ,
  EARLIEST_REDEMPTION_DATE  	VARCHAR2(32) ,
  ccy  	  				VARCHAR2(10) ,
  totalamt  	 		NUMBER(19,4) , 
  convertamt    		 NUMBER(19,4) 
);
comment on table TB_RH_CLFUND
  is '人行金数-存量特定目的载体投资信息';
-- Add comments to the columns 
comment on column TB_RH_CLFUND.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_CLFUND.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_CLFUND.fundtypes
  is '特定目的载体类型';
comment on column TB_RH_CLFUND.MANAGE_PRODUCT_TYPE
  is '资管产品统计编码';
comment on column TB_RH_CLFUND.FUND_CODE
  is '特定目的载体代码';
comment on column TB_RH_CLFUND.issuercode
  is '发行人代码';
 comment on column TB_RH_CLFUND.issuerarea
  is '发行人地区代码';
comment on column TB_RH_CLFUND.NET_WORTH_TYPE
  is '运行方式';
comment on column TB_RH_CLFUND.AFP_DATE
  is '认购日期';
comment on column TB_RH_CLFUND.EARLIEST_REDEMPTION_DATE
  is '到期日期';
comment on column TB_RH_CLFUND.ccy
  is '币种';
comment on column TB_RH_CLFUND.totalamt
  is '投资余额';
comment on column TB_RH_CLFUND.convertamt
  is '投资余额折人民币';

--特定目的载体投资发生额信息
 DROP TABLE TB_RH_FUNDFSE;
  create table TB_RH_FUNDFSE
( 
  BANKINISTCODE 	VARCHAR2(20),
  INSIDEINISTCODE 	VARCHAR2(20),
  fundtypes   		VARCHAR2(40),
  MANAGE_PRODUCT_TYPE 	VARCHAR2(20),
  FUND_CODE	        	VARCHAR2(32) ,
  issuercode         	VARCHAR2(32) ,
  issuerarea  			VARCHAR2(32) ,
  NET_WORTH_TYPE   		VARCHAR2(32) ,
  EST_DATE 				VARCHAR2(32) ,
  TDATE					VARCHAR2(32) ,
  EARLIEST_REDEMPTION_DATE  	VARCHAR2(32) ,
  ccy  	  				VARCHAR2(10) ,
  amt  	 				 NUMBER(19,4) , 
  convertamt    		 NUMBER(19,4) ,
  tradedic  			 VARCHAR2(10) 
);
comment on table TB_RH_FUNDFSE
  is '人行金数-特定目的载体投资发生额信息';
-- Add comments to the columns 
comment on column TB_RH_FUNDFSE.BANKINISTCODE
  is '金融机构代码';
comment on column TB_RH_FUNDFSE.INSIDEINISTCODE
  is '内部机构号';
comment on column TB_RH_FUNDFSE.fundtypes
  is '特定目的载体类型';
comment on column TB_RH_FUNDFSE.MANAGE_PRODUCT_TYPE
  is '资管产品统计编码';
comment on column TB_RH_FUNDFSE.FUND_CODE
  is '特定目的载体代码';
comment on column TB_RH_FUNDFSE.issuercode
  is '发行人代码';
 comment on column TB_RH_FUNDFSE.issuerarea
  is '发行人地区代码';
comment on column TB_RH_FUNDFSE.NET_WORTH_TYPE
  is '运行方式';
comment on column TB_RH_FUNDFSE.EST_DATE
  is '认购日期';
comment on column TB_RH_FUNDFSE.TDATE
  is '交易日期';
comment on column TB_RH_FUNDFSE.EARLIEST_REDEMPTION_DATE
  is '到期日期';
comment on column TB_RH_FUNDFSE.ccy
  is '币种';
comment on column TB_RH_FUNDFSE.amt
  is '交易余额';
comment on column TB_RH_FUNDFSE.convertamt
  is '交易余额折人民币';
comment on column TB_RH_FUNDFSE.tradedic
  is '交易方向';
  
 
CREATE OR REPLACE PROCEDURE SL_SP_RHJS(datadate     IN VARCHAR2,
                                       businessType IN varchar2,
                                       retcode      OUT VARCHAR2,
                                       retmsg       OUT VARCHAR2) IS

  v_datadate     DATE; --当前日期
  v_businessType varchar2(10); --报表类别
  v_monthyear    varchar2(10); --年月
BEGIN
  --数据日期
  IF datadate IS NULL THEN
    BEGIN
      SELECT prevbranprcdate
        INTO v_datadate
        FROM brps@opics
       WHERE br = '01';
    EXCEPTION
      WHEN no_data_found THEN
        retcode := '100';
        retmsg  := 'QUERY BRPS DATA NOT FOUND';
        RETURN;
    END;
  ELSE
    select max(POSTDATE) into v_datadate FROM SL_TPOS_H@opics
    WHERE POSTDATE <= LAST_DAY(TO_DATE(datadate, 'yyyyMM'));
    
  END IF;
  v_monthyear  := datadate;
  v_businessType := businessType;
  
   
  if v_businessType = '1' then 
    begin 
  --删除表数据  存量同业借贷
    delete from TB_RH_CLTYJD ;
  -- 插入数据
   INSERT INTO TB_RH_CLTYJD(
                      BANKINISTCODE,
                      INSIDEINISTCODE,       
                      shcode,
                      shtype,
                      DEALNO,
                      AL,
                      PRODUCT,
                      VDATE,
                      MDATE,
                      CCY,
                      CCYAMT,
                      SPOTRATE_8,
                      ISFIXED,
                      INTRATE,
                      INTPAYCYCLE)
                      SELECT DISTINCT
                           '912301001275921118' as BANKINISTCODE ,           
                            '00100' as INSIDEINISTCODE ,
                            c.shcode,
                            c.shtype,
                            r.DEALNO,
                            CASE
                              WHEN r.PS LIKE '%S%' THEN
                               'AL01'
                              ELSE
                               'AL02'
                            END AS AL,
                            r.PRODUCT,
                            to_char(r.VDATE,'yyyy-MM-dd')AS VDATE ,
                            to_char(r.MATDATE,'yyyy-MM-dd') AS MDATE,
                            r.CCY,
                            r.NOTAMT AS CCYAMT,
                            case when sp.spotrate != 0 then sp.spotrate * r.NOTAMT
                              else p.SPOTRATE_8 * r.NOTAMT end  SPOTRATE_8,
                            '是' AS ISFIXED,
                            TO_CHAR(r.REPORATE_8) AS INTRATE,
                            r.INTPAYCYCLE
              FROM RPRH@opics r
              LEFT JOIN ifs_opics_cust c
                ON c.CNO = r.CNO
              LEFT JOIN SRHR@opics s
                ON s.CCY = r.CCY
              LEFT JOIN (SELECT
                          ccy,
                          CASE
                            WHEN spotrate31_8 <> 0 THEN spotrate31_8
                            WHEN spotrate30_8 <> 0 THEN spotrate30_8
                            WHEN spotrate29_8 <> 0 THEN spotrate29_8
                            WHEN spotrate28_8 <> 0 THEN spotrate28_8
                            WHEN spotrate27_8 <> 0 THEN spotrate27_8
                          END spotrate
                        FROM
                          SRHR@opics
                        WHERE
                          BR = '01'
                          AND YRMONTH =v_monthyear ) sp
                ON sp.CCY = r.CCY
              left join REVP@opics p on 
              p.ccy = r.ccy
             WHERE r.VDATE <= v_datadate
               AND r.MATDATE > v_datadate
               AND r.REVDATE IS NULL
            
            UNION ALL
            
            SELECT DISTINCT 
                             '912301001275921118' as BANKINISTCODE ,           
                              '00100' as INSIDEINISTCODE ,
                             c.shcode,
                            c.shtype,
                            d.DEALNO,
                            CASE
                              WHEN d.AL LIKE '%A%' THEN
                               'AL01'
                              ELSE
                               'AL02'
                            END AS AL,
                            d.PRODUCT,
                            to_char(d.VDATE,'yyyy-MM-dd')AS VDATE,
                            to_char(d.MDATE,'yyyy-MM-dd'),
                            d.CCY,
                            d.CCYAMT,
                           case when sp.spotrate != 0 then sp.spotrate * d.CCYAMT
                              else p.SPOTRATE_8 * d.CCYAMT end  SPOTRATE_8,
                            CASE
                              WHEN d.RATECODE LIKE '%FIXED%' THEN
                               '是'
                              ELSE
                               '否'
                            END AS ISFIXED,
                            d.INTRATE,
                            d.INTPAYCYCLE
              FROM DLDT@opics d
              LEFT JOIN ifs_opics_cust c
                ON c.CNO = d.CNO
              LEFT JOIN SRHR@opics s
                ON s.CCY = d.CCY
              LEFT JOIN (SELECT
                          ccy,
                          CASE
                            WHEN spotrate31_8 <> 0 THEN spotrate31_8
                            WHEN spotrate30_8 <> 0 THEN spotrate30_8
                            WHEN spotrate29_8 <> 0 THEN spotrate29_8
                            WHEN spotrate28_8 <> 0 THEN spotrate28_8
                            WHEN spotrate27_8 <> 0 THEN spotrate27_8
                          END spotrate
                        FROM
                          SRHR@opics
                        WHERE
                          BR = '01'
                          AND YRMONTH =v_monthyear ) sp
                ON sp.CCY = d.CCY
              left join REVP@opics p on 
              p.ccy = d.ccy
             WHERE d.VDATE <= v_datadate
               AND d.MDATE > v_datadate
               AND d.REVDATE IS NULL;
               commit;
     end ;
     
     elsif v_businessType = '2' then 
      
      begin
     --删除表数据  同业借贷发生额
     delete from TB_RH_TYJDFSE ;
     -- 插入数据
     INSERT INTO TB_RH_TYJDFSE(
        BANKINISTCODE,
        INSIDEINISTCODE,
        shcode,
        shtype,
        DEALNO,
        FEDEALNO,
        AL,
        PRODUCT,
        VDATE,
        MDATE,
        AMDATE,
        CCY,
        CCYAMT,
        SPOTRATE,
        ISFIXED,
        INTRATE,
        INTPAYCYCLE,
        logo)
        SELECT DISTINCT 
         '912301001275921118' as BANKINISTCODE ,           
        '00100' as INSIDEINISTCODE ,
        c.shcode,
        c.shtype,
        r.DEALNO,
        i.FEDEALNO,
        CASE
        WHEN r.PS LIKE '%S%' THEN 'AL01'
        ELSE 'AL02'
        END AS AL,
        r.PRODUCT,
        to_char(r.VDATE,'yyyy-MM-dd') AS VDATE,
        to_char(r.MATDATE,'yyyy-MM-dd') AS MDATE,
        to_char(r.MATDATE,'yyyy-MM-dd') AS AMDATE,
        r.CCY,
        r.NOTAMT AS CCYAMT,
        case when sp.spotrate != 0 
        then sp.spotrate * r.NOTAMT
        else p.SPOTRATE_8 * r.NOTAMT end SPOTRATE,
        '是' AS ISFIXED,
        TO_CHAR(r.REPORATE_8) AS INTRATE,
        r.INTPAYCYCLE,
        CASE WHEN
        r.MATDATE< TO_DATE(v_datadate, 'yyyy-MM-dd') THEN '结清'
        ELSE '发生' END  logo
        FROM
             RPRH@OPICS r
        LEFT JOIN ifs_opics_cust c ON
        c.CNO = r.CNO
    LEFT JOIN INTH@OPICS i ON 
    TRIM(i.dealno)=TRIM(r.dealno)
        LEFT JOIN SRHR@OPICS s ON
        s.CCY = r.CCY
        LEFT JOIN (
        SELECT
        ccy,
        CASE
        WHEN spotrate31_8 <> 0 THEN spotrate31_8
        WHEN spotrate30_8 <> 0 THEN spotrate30_8
        WHEN spotrate29_8 <> 0 THEN spotrate29_8
        WHEN spotrate28_8 <> 0 THEN spotrate28_8
        WHEN spotrate27_8 <> 0 THEN spotrate27_8
        END spotrate
      FROM
        SRHR@opics
      WHERE
        BR = '01'
        AND YRMONTH =v_monthyear ) sp
        ON sp.CCY = r.CCY
        left join REVP@opics p on 
        p.ccy = r.ccy
        WHERE
        r.VDATE >= TRUNC(v_datadate, 'MM')
        AND r.VDATE <= v_datadate
        AND r.REVDATE IS NULL

        UNION ALL

        SELECT
        DISTINCT
         '912301001275921118' as BANKINISTCODE ,           
        '00100' as INSIDEINISTCODE ,
         c.shcode,
        c.shtype,
        d.DEALNO,
        i.FEDEALNO,
        CASE
        WHEN d.AL LIKE '%A%' THEN 'AL01'
        ELSE 'AL02'
        END AS AL,
        d.PRODUCT,
        to_char(d.VDATE,'yyyy-MM-dd')as VDATE,
        to_char(d.MDATE,'yyyy-MM-dd')as MDATE,
        to_char(d.MDATE,'yyyy-MM-dd')as AMDATE,
        d.CCY,
        d.CCYAMT,
        case when sp.spotrate != 0 then sp.spotrate * d.CCYAMT
        else p.SPOTRATE_8 * d.CCYAMT end  SPOTRATE,
        CASE
        WHEN d.RATECODE LIKE '%FIXED%' THEN '是'
        ELSE '否'
        END AS ISFIXED,
        d.INTRATE,
        d.INTPAYCYCLE,
        CASE WHEN
        d.MDATE < TO_DATE(v_datadate, 'yyyy-MM-dd') THEN '1'
        ELSE '0' END  logo
        FROM
             DLDT@OPICS d
        LEFT JOIN ifs_opics_cust c ON
        c.CNO = d.CNO
    LEFT JOIN INTH@OPICS i ON 
    TRIM(i.dealno)=TRIM(d.dealno)
        LEFT JOIN SRHR@OPICS s ON
        s.CCY = d.CCY
        LEFT JOIN (
        SELECT
        ccy,
        CASE
        WHEN spotrate31_8 <> 0 THEN spotrate31_8
        WHEN spotrate30_8 <> 0 THEN spotrate30_8
        WHEN spotrate29_8 <> 0 THEN spotrate29_8
        WHEN spotrate28_8 <> 0 THEN spotrate28_8
        WHEN spotrate27_8 <> 0 THEN spotrate27_8
        END spotrate
      FROM
        SRHR@opics
      WHERE
        BR = '01'
        AND YRMONTH =v_monthyear ) sp
                ON sp.CCY = d.CCY
      left join REVP@opics p on 
      p.ccy = d.ccy
        WHERE
        d.VDATE >= TRUNC(v_datadate, 'MM')
        AND d.VDATE <= v_datadate
        AND d.REVDATE IS NULL ;
        commit;
     end;
     
     elsif v_businessType = '3' then 
     begin
     --删除表数据  存量同业存款
     delete from TB_RH_CLTYCK ;
     -- 插入数据
     INSERT INTO TB_RH_CLTYCK(
     BANKINISTCODE,
     INSIDEINISTCODE,
     prodtype,
     shtype,
     shcode,
     accountcode,
     protocolcode,
     vdate,
     mdate,
     ccy,
     ccyamt,
     spotrate,
     intrate,
     payway )
     select
      '912301001275921118' as BANKINISTCODE ,           
      '00100' as INSIDEINISTCODE ,
     prodtype,
     shtype,
     shcode,
     accountcode,
     protocolcode,
     vdate,
     mdate,
     ccy,
     ccyamt,
     spotrate,
     intrate,
     payway
     from(
        SELECT
            '存放同业' as prodtype,
            c.shtype,
            c.shcode ,
            '' AS accountcode,
            '' AS protocolcode,
            TO_CHAR(d.vdate,'yyyy-MM-dd') AS vdate ,
            TO_CHAR(d.mdate,'yyyy-MM-dd') AS mdate ,
            d.ccy,
            d.ccyamt,
            case when sp.spotrate != 0 
        then sp.spotrate * d.ccyamt
        else p.SPOTRATE_8 * d.ccyamt end spotrate,
            TO_NUMBER(d.intrate) AS intrate,
            '不涉及' AS payway
        FROM
                DLDT@opics d
                        LEFT JOIN ifs_opics_cust c ON
                        c.CNO = d.CNO
                        LEFT JOIN (
                        SELECT
                          ccy,
                          CASE
                            WHEN spotrate31_8 <> 0 THEN spotrate31_8
                            WHEN spotrate30_8 <> 0 THEN spotrate30_8
                            WHEN spotrate29_8 <> 0 THEN spotrate29_8
                            WHEN spotrate28_8 <> 0 THEN spotrate28_8
                            WHEN spotrate27_8 <> 0 THEN spotrate27_8
                          END spotrate
                        FROM
                          SRHR@opics
                        WHERE
                          BR = '01'
                          AND YRMONTH =v_monthyear ) sp
                ON sp.CCY = d.CCY
              left join REVP@opics p on 
              p.ccy = d.ccy
        WHERE d.prodtype IN ('CT')
          AND d.REVDATE IS NULL
          AND d.VDATE <= v_datadate
          AND d.MDATE > v_datadate

        UNION ALL
        SELECT
                '同业存放' AS prodtype,
                c.shtype,
                c.shcode ,
                '' AS accountcode,
                '' AS protocolcode,
                ird.FIRST_SETTLEMENT_DATE AS vdate,
                ird.SECOND_SETTLEMENT_DATE AS mdate,
                ird.CURRENCY AS ccy ,
                ird.AMT AS ccyamt,
                ird.AMT AS spotrate,
                ird.RATE AS intrate ,
                '' AS payway
        FROM IFS_RMB_DEPOSITIN ird LEFT JOIN ifs_opics_cust c ON
                c.CNO = ird.CUST_ID
                LEFT JOIN (
                SELECT
                          ccy,
                          CASE
                            WHEN spotrate31_8 <> 0 THEN spotrate31_8
                            WHEN spotrate30_8 <> 0 THEN spotrate30_8
                            WHEN spotrate29_8 <> 0 THEN spotrate29_8
                            WHEN spotrate28_8 <> 0 THEN spotrate28_8
                            WHEN spotrate27_8 <> 0 THEN spotrate27_8
                          END spotrate
                        FROM
                          SRHR@opics
                        WHERE
                          BR = '01'
                          AND YRMONTH = v_monthyear ) sp
               ON sp.CCY = ird.CURRENCY
              left join REVP@opics p on 
              p.ccy = ird.CURRENCY
        WHERE  ird.APPROVE_STATUS = '6'
        AND TO_DATE(ird.FIRST_SETTLEMENT_DATE,'yyyy-MM-dd')<= v_datadate
        AND TO_DATE(ird.SECOND_SETTLEMENT_DATE,'yyyy-MM-dd') >  v_datadate

        union all

        SELECT
        '同业存单发行' AS prodtype,
        c.shtype,
        c.shcode ,
        '' AS accountcode,
        '' AS protocolcode,
        TO_CHAR(s2.vdate,'yyyy-MM-dd') AS vdate ,
        TO_CHAR(s2.mdate,'yyyy-MM-dd') AS mdate ,
        s1.ccy,
        s1.FACEAMT  AS ccyamt,
        case when sp.spotrate != 0 
        then sp.spotrate * s1.FACEAMT
        else p.SPOTRATE_8 * s1.FACEAMT end spotrate,
        s2.COUPRATE_8 AS intrate,
        '' AS payway
        FROM SPSH@opics s1 LEFT JOIN IFS_OPICS_CUST c ON c.CNO = s1.CNO
        LEFT JOIN SECM@opics s2 ON s2.SECID =s1.SECID
        LEFT JOIN (
        SELECT
                  ccy,
                  CASE
                    WHEN spotrate31_8 <> 0 THEN spotrate31_8
                    WHEN spotrate30_8 <> 0 THEN spotrate30_8
                    WHEN spotrate29_8 <> 0 THEN spotrate29_8
                    WHEN spotrate28_8 <> 0 THEN spotrate28_8
                    WHEN spotrate27_8 <> 0 THEN spotrate27_8
                  END spotrate
                FROM
                  SRHR@opics
                WHERE
                  BR = '01'
                  AND YRMONTH = v_monthyear ) sp
                ON sp.CCY = s1.ccy
              left join REVP@opics p on 
              p.ccy = s1.ccy
        WHERE  s1.PRODTYPE = 'CD'
        AND s1.INVTYPE='I'
        AND s1.REVDATE IS NULL
        AND s2.vdate <= v_datadate
        AND s2.mdate > v_datadate

        UNION ALL

        SELECT
        '同业存单投资' AS prodtype,
        c.shtype,
        c.shcode ,
        '' AS accountcode,
        '' AS protocolcode,
        TO_CHAR(s2.vdate,'yyyy-MM-dd') AS vdate ,
        TO_CHAR(s2.mdate,'yyyy-MM-dd') AS mdate ,
        s1.ccy,
        s1.FACEAMT  AS ccyamt,
        case when sp.spotrate != 0 
        then sp.spotrate * s1.FACEAMT
        else p.SPOTRATE_8 * s1.FACEAMT end spotrate,
        s2.COUPRATE_8 AS intrate,
        '' AS payway
        FROM SPSH@opics s1 LEFT JOIN IFS_OPICS_CUST c ON c.CNO = s1.CNO
        LEFT JOIN SECM@opics s2 ON s2.SECID =s1.SECID
        LEFT JOIN (
         SELECT
                  ccy,
                  CASE
                    WHEN spotrate31_8 <> 0 THEN spotrate31_8
                    WHEN spotrate30_8 <> 0 THEN spotrate30_8
                    WHEN spotrate29_8 <> 0 THEN spotrate29_8
                    WHEN spotrate28_8 <> 0 THEN spotrate28_8
                    WHEN spotrate27_8 <> 0 THEN spotrate27_8
                  END spotrate
                FROM
                  SRHR@opics
                WHERE
                  BR = '01'
                  AND YRMONTH = v_monthyear ) sp
                ON sp.CCY = s1.ccy
              left join REVP@opics p on 
              p.ccy = s1.ccy
        WHERE c.ACCTNGTYPE = 'DOMCD'
        AND s1.REVDATE IS NULL
        AND s2.vdate <= v_datadate
        AND s2.mdate > v_datadate
     );
     commit;
     end;
     elsif v_businessType = '4' then 
      begin
     --删除表数据 同业存款发生额
     delete from TB_RH_TYCKFSE ;
     -- 插入数据
     INSERT INTO TB_RH_TYCKFSE(
     BANKINISTCODE,
     INSIDEINISTCODE,
     prodtype,
     shtype,
     shcode,
     accountcode,
     protocolcode,
     vdate,
     mdate,
     ccy,
     ccyamt,
     spotrate,
     DEALDATE,
     intrate,
     PAY_USERID,
     PAY_BANKID,
     REC_USERID,
     logo )
     select
      '912301001275921118' as BANKINISTCODE ,           
      '00100' as INSIDEINISTCODE ,
     prodtype,
     shtype,
     shcode,
     accountcode,
     protocolcode,
     vdate,
     mdate,
     ccy,
     ccyamt,
     spotrate,
     DEALDATE,
     intrate,
     PAY_USERID,
     PAY_BANKID,
     REC_USERID,
     logo
     from(
     SELECT
        '存放同业' as prodtype,
        c.shtype,
        c.shcode ,
        '' AS accountcode,
        '' AS protocolcode,
        TO_CHAR(d.vdate,'yyyy-MM-dd') AS vdate ,
        TO_CHAR(d.mdate,'yyyy-MM-dd') AS mdate ,
        d.ccy,
        d.ccyamt,
        case when sp.spotrate != 0 
        then sp.spotrate * d.ccyamt
        else p.SPOTRATE_8 * d.ccyamt end spotrate,
        TO_CHAR(d.DEALDATE,'yyyy-MM-dd') AS DEALDATE,
        TO_NUMBER(d.intrate) AS intrate,
        ics.PAY_USERID,
        ics.PAY_BANKID,
        ics.REC_USERID,
        CASE WHEN
        d.mdate > v_datadate THEN '1'
        ELSE '0' END AS logo
        FROM
        DLDT@opics d
        LEFT JOIN ifs_opics_cust c ON
        c.CNO = d.CNO
        LEFT JOIN IFS_CUST_SETTLE ics ON
        ics.DEALNO =d.DEALNO
        LEFT JOIN (
        SELECT
              ccy,
              CASE
                WHEN spotrate31_8 <> 0 THEN spotrate31_8
                WHEN spotrate30_8 <> 0 THEN spotrate30_8
                WHEN spotrate29_8 <> 0 THEN spotrate29_8
                WHEN spotrate28_8 <> 0 THEN spotrate28_8
                WHEN spotrate27_8 <> 0 THEN spotrate27_8
              END spotrate
                FROM
                  SRHR@opics
                WHERE
                  BR = '01'
                  AND YRMONTH =v_monthyear ) sp
                ON sp.CCY = d.CCY
              left join REVP@opics p on 
              p.ccy = d.ccy
        WHERE d.prodtype IN ('CT')
        AND d.REVDATE IS NULL
        AND d.DEALDATE >= TRUNC(v_datadate,'MM') 
        AND d.DEALDATE <= v_datadate

        union all

        SELECT
        '同业存放' AS prodtype,
        c.shtype,
        c.shcode ,
        '' AS accountcode,
        '' AS protocolcode,
        ird.FIRST_SETTLEMENT_DATE AS vdate,
        ird.SECOND_SETTLEMENT_DATE AS mdate,
        ird.CURRENCY AS ccy ,
        ird.AMT AS ccyamt,
        ird.AMT AS spotrate,
        ird.FOR_DATE AS DEALDATE,
        ird.RATE AS intrate ,
        ird.SELF_ACCCODE AS PAY_USERID,
        ird.SELF_BANKCODE AS PAY_BANKID,
        ird.PARTY_ACCCODE AS REC_USERID,
        CASE WHEN
        TO_DATE(ird.SECOND_SETTLEMENT_DATE,'yyyy-MM-dd') > v_datadate THEN '1'
        ELSE '0' END AS logo
        FROM IFS_RMB_DEPOSITIN ird LEFT JOIN ifs_opics_cust c ON
        c.CNO = ird.CUST_ID
        LEFT JOIN (
        SELECT
              ccy,
              CASE
                WHEN spotrate31_8 <> 0 THEN spotrate31_8
                WHEN spotrate30_8 <> 0 THEN spotrate30_8
                WHEN spotrate29_8 <> 0 THEN spotrate29_8
                WHEN spotrate28_8 <> 0 THEN spotrate28_8
                WHEN spotrate27_8 <> 0 THEN spotrate27_8
              END spotrate
            FROM
              SRHR@opics
            WHERE
              BR = '01'
              AND YRMONTH = v_monthyear ) sp
              ON sp.CCY = ird.CURRENCY
              left join REVP@opics p on 
              p.ccy = ird.CURRENCY
        WHERE  ird.APPROVE_STATUS = '6'
        AND TO_DATE(ird.FOR_DATE,'yyyy-MM-dd') >= trunc(v_datadate,'MM')
        AND TO_DATE(ird.FOR_DATE,'yyyy-MM-dd') <= v_datadate

        union all
        SELECT
        '同业存单发行' AS prodtype,
        c.shtype,
        c.shcode ,
        '' AS accountcode,
        '' AS protocolcode,
        TO_CHAR(s2.vdate,'yyyy-MM-dd') AS vdate ,
        TO_CHAR(s2.mdate,'yyyy-MM-dd') AS mdate ,
        s1.ccy,
        s1.FACEAMT  AS ccyamt,
        case when sp.spotrate != 0 
        then sp.spotrate * s1.FACEAMT
        else p.SPOTRATE_8 * s1.FACEAMT end spotrate,
        TO_CHAR(s1.DEALDATE,'yyyy-MM-dd') AS DEALDATE ,
        s2.COUPRATE_8 AS intrate,
        ics.PAY_USERID,
        ics.PAY_BANKID,
        ics.REC_USERID,
        CASE WHEN
        s2.mdate > v_datadate THEN '1'
        ELSE '0' END AS logo
        FROM SPSH@opics s1 LEFT JOIN IFS_OPICS_CUST c ON c.CNO = s1.CNO
        LEFT JOIN SECM@opics s2 ON s2.SECID =s1.SECID
        LEFT JOIN IFS_CUST_SETTLE ics ON ics.DEALNO = s1.DEALNO
        LEFT JOIN (
        SELECT
                  ccy,
                  CASE
                    WHEN spotrate31_8 <> 0 THEN spotrate31_8
                    WHEN spotrate30_8 <> 0 THEN spotrate30_8
                    WHEN spotrate29_8 <> 0 THEN spotrate29_8
                    WHEN spotrate28_8 <> 0 THEN spotrate28_8
                    WHEN spotrate27_8 <> 0 THEN spotrate27_8
                  END spotrate
                FROM
                  SRHR@opics
                WHERE
                  BR = '01'
                  AND YRMONTH = v_monthyear ) sp
                ON sp.CCY = s1.ccy
              left join REVP@opics p on 
              p.ccy = s1.ccy
        WHERE  s1.PRODTYPE = 'CD'
        AND s1.INVTYPE='I'
        AND s1.REVDATE IS NULL
        AND s1.DEALDATE >= TRUNC(v_datadate,'MM') 
        AND s1.DEALDATE <= v_datadate

        union all

        SELECT
        '同业存单投资' AS prodtype,
        c.shtype,
        c.shcode ,
        '' AS accountcode,
        '' AS protocolcode,
        TO_CHAR(s2.vdate,'yyyy-MM-dd') AS vdate ,
        TO_CHAR(s2.mdate,'yyyy-MM-dd') AS mdate ,
        s1.ccy,
        s1.FACEAMT  AS ccyamt,
        case when sp.spotrate != 0 
        then sp.spotrate * s1.FACEAMT
        else p.SPOTRATE_8 * s1.FACEAMT end spotrate,
        TO_CHAR(s1.DEALDATE,'yyyy-MM-dd') AS DEALDATE,
        s2.COUPRATE_8 AS intrate,
        ics.PAY_USERID,
        ics.PAY_BANKID,
        ics.REC_USERID,
        CASE WHEN
        s2.mdate > v_datadate THEN '1'
        ELSE '0' END AS logo
        FROM SPSH@OPICS s1 LEFT JOIN IFS_OPICS_CUST c ON c.CNO = s1.CNO
        LEFT JOIN SECM@opics s2 ON s2.SECID =s1.SECID
        LEFT JOIN IFS_CUST_SETTLE ics ON ics.DEALNO = s1.DEALNO
        LEFT JOIN (
         SELECT
                  ccy,
                  CASE
                    WHEN spotrate31_8 <> 0 THEN spotrate31_8
                    WHEN spotrate30_8 <> 0 THEN spotrate30_8
                    WHEN spotrate29_8 <> 0 THEN spotrate29_8
                    WHEN spotrate28_8 <> 0 THEN spotrate28_8
                    WHEN spotrate27_8 <> 0 THEN spotrate27_8
                  END spotrate
                FROM
                  SRHR@opics
                WHERE
                  BR = '01'
                  AND YRMONTH = v_monthyear ) sp
                ON sp.CCY = s1.ccy
              left join REVP@opics p on 
              p.ccy = s1.ccy
        WHERE  c.ACCTNGTYPE = 'DOMCD'
        AND s1.REVDATE IS NULL
        AND s1.SETTDATE >= TRUNC(v_datadate,'MM')
        AND s1.SETTDATE <= v_datadate
       );
       commit;
       end;
      elsif v_businessType = '5' then 
      begin
     --删除表数据 同业客户基本信息
     delete from TB_RH_CUST ;
     -- 插入数据
     INSERT INTO TB_RH_CUST(
           BANKINISTCODE,
           cfn,
           SHCODE,
           INSTITUCODE,
           cno,
           basicAccount,
           basicAccountName,
           CA,
           CCODE,
           fintype,
           FOUNDDATE,
           ISACC,
           ECING,
           ECOTYPE,
           CRE,
           CRELEVEL)
       SELECT
            '912301001275921118'as BANKINISTCODE,
            ioc.cfn,
            ioc.SHCODE ,
            ioc.INSTITUCODE ,
            ioc.cno,
            ioc.CA ,
           '' AS basicAccount,
           '' AS basicAccountName,
            ioc.CCODE,
            ioc.fintype,
            ioc.FOUNDDATE,
            ioc.ISACC,
            ioc.ECING,
            ioc.ECOTYPE,
            ioc.CRE,
            ioc.CRELEVEL
        FROM IFS_OPICS_CUST ioc ;
        commit;
     end;
     
     --存量债券投资信息
     
     elsif v_businessType = '6' then 
       
     begin
       --删除表数据
       delete from TB_RH_CLZQTZ;
       --插入数据
       insert into TB_RH_CLZQTZ(
       BANKINISTCODE,
       INSIDEINISTCODE,
       BNDCD,
       ACCOUNTNO,
       BONDPROPERTIES,
       BONDRATING,
       CCY,
       PRINAMT,
       SPOTRATE,
       BOND_RIGHT_DEBT_DATE,
       VDATE,
       MDATE,
       COUPRATE_8,
       SHTYPE,
       CCODE,
       INDTYPE,
       COMSCALE,
       ECING,
       ECOTYPE
       ) 
       SELECT
        DISTINCT 
        '912301001275921118' AS  BANKINISTCODE,
       '00100' AS INSIDEINISTCODE,
        iwb.BNDCD ,
        iwb.ACCOUNTNO ,
        iwb.BONDPROPERTIES,
        iwb.BONDRATING ,
        s1.CCY ,
        m.PRINAMT ,
        case when sp.spotrate != 0 
          then sp.spotrate * m.PRINAMT
          else p.SPOTRATE_8 * m.PRINAMT end spotrate,
        iwb.BOND_RIGHT_DEBT_DATE ,
        to_char(s2.VDATE,'yyyy-MM-dd') as vdate ,
        to_char(s2.MDATE,'yyyy-MM-dd') as MDATE ,
        s2.COUPRATE_8 ,
        ioc.SHTYPE ,
        ioc.CCODE ,
        ioc.INDTYPE ,
        ioc.COMSCALE ,
        ioc.ECING ,
        ioc.ECOTYPE
      FROM
        sl_tpos_h@opics s1
      LEFT JOIN IFS_OPICS_BOND iwb ON
        trim(iwb.BNDCD) = trim(s1.SECID)
      LEFT JOIN SECM@opics s2 ON
        s1.SECID = s2.SECID
      LEFT JOIN IFS_OPICS_CUST ioc ON
        ioc.CNO = s2.ISSUER
      LEFT JOIN (
        SELECT
          ccy,
          CASE
            WHEN spotrate31_8 <> 0 THEN spotrate31_8
            WHEN spotrate30_8 <> 0 THEN spotrate30_8
            WHEN spotrate29_8 <> 0 THEN spotrate29_8
            WHEN spotrate28_8 <> 0 THEN spotrate28_8
            WHEN spotrate27_8 <> 0 THEN spotrate27_8
          END spotrate
        FROM
          SRHR@opics
        WHERE
          BR = '01'
          AND YRMONTH = v_monthyear ) sp
        ON sp.CCY = s1.ccy
      left join REVP@opics p on 
      p.ccy = s1.ccy
     LEFT JOIN (SELECT
        ccy,
        (sum(CASE WHEN trim(AT) IS NULL THEN 0 ELSE AT END) +
        sum(CASE WHEN trim(aa) IS NULL THEN 0 ELSE aa END) +
        sum(CASE WHEN trim(aH) IS NULL THEN 0 ELSE ah END) +
        sum(CASE WHEN trim(aH1) IS NULL THEN 0 ELSE aH1 END) +
        sum(CASE WHEN trim(aa1) IS NULL THEN 0 ELSE aa1 END) )
        AS
        PRINAMT
        FROM
        (
        SELECT
        t.ccy,
        CASE
        WHEN trim(t.invtype) = 'T' THEN (abs(t.settavgcost) + t.tdymtm)
        END AT,
        CASE
        WHEN trim(t.invtype) = 'A'
        AND s.prodtype = 'IM' THEN abs(t.prinamt) + abs(t.accroutst) + t.unamortamt + t.tdymtm
        END aa,
        CASE
        WHEN trim(t.invtype) = 'H'
        AND s.prodtype = 'IM' THEN abs(t.prinamt) + abs(t.accroutst) - (t.unamortamt * -1)
        END aH,
        CASE
        WHEN trim(t.invtype) = 'A'
        AND s.prodtype != 'IM' THEN abs(t.prinamt) + t.unamortamt + t.tdymtm
        END aa1,
        CASE
        WHEN trim(t.invtype) = 'H'
        AND s.prodtype != 'IM' THEN abs(t.prinamt) - (t.unamortamt * -1)
        END aH1
        FROM
        SL_TPOS_H@Opics t
        LEFT JOIN secm@Opics s on
        t.secid = s.secid
        WHERE
        t.Cost LIKE '83%'
        AND t.br = '01'
        AND t.INVTYPE != 'I'
        AND t.postdate = v_datadate ) GROUP BY CCY) m on m.ccy=s1.ccy
        WHERE s1.INVTYPE != 'I' ;
       commit;     
     end;
      --债券投资发生额信息
     
     elsif v_businessType = '7' then 
       
     begin
       --删除表数据
       delete from TB_RH_ZQTZFSE;
       --插入数据
       insert into TB_RH_ZQTZFSE(
       BANKINISTCODE,
       INSIDEINISTCODE,
       BNDCD,
       ACCOUNTNO,
       BONDPROPERTIES,
       BONDRATING,
       CCY,
       PRINAMT,
       SPOTRATE,
       BOND_RIGHT_DEBT_DATE,
       VDATE,
       MDATE,
       COUPRATE_8,
       SHTYPE,
       CCODE,
       INDTYPE,
       COMSCALE,
       ECING,
       ECOTYPE,
       DEALDATE,
       FEDEALNO,
       LOGO
       )
       SELECT
        DISTINCT 
         '912301001275921118' AS  BANKINISTCODE,
       '00100' AS INSIDEINISTCODE,
        iwb.BNDCD ,
        iwb.ACCOUNTNO ,
        iwb.BONDPROPERTIES,
        iwb.BONDRATING ,
        iwb.CCY ,
        s1.PRINAMT ,
        case when sp.spotrate != 0 
        then sp.spotrate * s1.PRINAMT
        else p.SPOTRATE_8 *s1.PRINAMT end spotrate,
        iwb.BOND_RIGHT_DEBT_DATE ,
        to_char( s2.VDATE,'yyyy-MM-dd')as VDATE ,
        to_char( s2.MDATE,'yyyy-MM-dd')as MDATE ,
        s2.COUPRATE_8 ,
        ioc.SHTYPE ,
        ioc.CCODE ,
        ioc.INDTYPE ,
        ioc.COMSCALE ,
        ioc.ECING ,
        ioc.ECOTYPE,
        s1.DEALDATE,
        ioc.FEDEALNO,
        case when s1.PS ='P' THEN '买入'
        ELSE '卖出' END AS logo
        FROM
          SPSH@opics s1
        LEFT JOIN IFS_OPICS_BOND iwb ON
        trim(iwb.BNDCD) = trim(s1.SECID)
        LEFT JOIN SECM@opics s2 ON
        s1.SECID = s2.SECID
        LEFT JOIN IFS_OPICS_CUST ioc ON
        ioc.CNO = s1.CNO
        LEFT JOIN (
        SELECT
          ccy,
          CASE
            WHEN spotrate31_8 <> 0 THEN spotrate31_8
            WHEN spotrate30_8 <> 0 THEN spotrate30_8
            WHEN spotrate29_8 <> 0 THEN spotrate29_8
            WHEN spotrate28_8 <> 0 THEN spotrate28_8
            WHEN spotrate27_8 <> 0 THEN spotrate27_8
          END spotrate
        FROM
          SRHR@opics
        WHERE
          BR = '01'
          AND YRMONTH = v_monthyear ) sp
        ON sp.CCY = s1.ccy
      left join REVP@opics p on 
       p.ccy = s1.ccy
        WHERE
        s1.INVTYPE != 'I'
        AND s1.REVDATE IS NULL
        AND s1.SETTDATE >= TRUNC(v_datadate,'MM')
        AND s1.SETTDATE <= v_datadate ;
       commit;
       end;
     --存量债券发行信息
     
     elsif v_businessType = '8' then 
       
     begin
       --删除表数据
       delete from TB_RH_CLZQFX;
       --插入数据
       insert into TB_RH_CLZQFX(
       BANKINISTCODE,
       INSIDEINISTCODE,
       BNDCD,
       ACCOUNTNO,
       BONDPROPERTIES,
       BONDRATING,
       REISSUE_FREQUENCY,
       CCY,
       PRINAMT,
       SPOTRATE1,
       SETTAVGCOST,
       SPOTRATE2,
       BOND_RIGHT_DEBT_DATE,
       VDATE,
       MDATE,
       INTCALCRULE,
       COUPRATE_8
       )
       SELECT
       '912301001275921118' AS  BANKINISTCODE,
       '00100' AS INSIDEINISTCODE,
        iwb.BNDCD ,
        iwb.ACCOUNTNO ,
        iwb.BONDPROPERTIES,
        iwb.BONDRATING ,
        iwb.REISSUE_FREQUENCY,
        iwb.CCY ,
        s1.PRINAMT,
        case when sp.spotrate != 0 
        then sp.spotrate * s1.PRINAMT
        else p.SPOTRATE_8 *s1.PRINAMT end spotrate1,
        s1.settavgcost ,
        case when sp.spotrate != 0 
        then sp.spotrate * s1.settavgcost
        else p.SPOTRATE_8 *s1.settavgcost end spotrate2,
        iwb.BOND_RIGHT_DEBT_DATE ,
        to_CHAR(s2.VDATE,'yyyy-MM-dd') as VDATE ,
        to_CHAR(s2.MDATE,'yyyy-MM-dd') as MDATE ,
        CASE WHEN iwb.INTCALCRULE ='DIS' THEN '贴现式'
        	WHEN iwb.INTCALCRULE ='IAM' THEN '利随本清式'
        	WHEN iwb.INTCALCRULE ='NVP' AND ior.RATETYPE ='FIXED' THEN '附息式固定利率'
           WHEN iwb.INTCALCRULE ='NVP' AND ior.RATETYPE !='FIXED' THEN '附息式浮动利率'
           ELSE '' END  INTCALCRULE ,
        s2.COUPRATE_8
        FROM
           sl_tpos_h@opics s1
        LEFT JOIN IFS_OPICS_BOND iwb ON
        trim(iwb.BNDCD) = trim(s1.SECID)
        LEFT JOIN SECM@opics s2 ON
        s1.SECID = s2.SECID
        LEFT JOIN IFS_OPICS_RATE ior ON 
        ior.RATECODE = iwb.floatratemark
        LEFT JOIN (
       SELECT
          ccy,
          CASE
            WHEN spotrate31_8 <> 0 THEN spotrate31_8
            WHEN spotrate30_8 <> 0 THEN spotrate30_8
            WHEN spotrate29_8 <> 0 THEN spotrate29_8
            WHEN spotrate28_8 <> 0 THEN spotrate28_8
            WHEN spotrate27_8 <> 0 THEN spotrate27_8
          END spotrate
        FROM
          SRHR@opics
        WHERE
          BR = '01'
          AND YRMONTH = v_monthyear ) sp
          ON sp.CCY = s1.ccy
        left join REVP@opics p on 
         p.ccy = s1.ccy
        WHERE
        s1.INVTYPE = 'I'
        AND s1.PRODTYPE != 'CD'
        AND s1.POSTDATE = v_datadate ;
       commit;
       end;
        --债券发行信息发生额
     
     elsif v_businessType = '9' then 
       
     begin
       --删除表数据
       delete from TB_RH_ZQFXFSE;
       --插入数据
       insert into TB_RH_ZQFXFSE(
       BANKINISTCODE,
       BNDCD,
       ACCOUNTNO,
       BONDPROPERTIES,
       BONDRATING,
       REISSUE_FREQUENCY,
       CCY,
       PRINAMT,
       SPOTRATE1,
       PROCEEDAMT,
       SPOTRATE2,
       BOND_RIGHT_DEBT_DATE,
       VDATE,
       MDATE,
       dealdate,
       COUPRATE_8,
       logo)
       SELECT
            distinct
            '912301001275921118' AS  BANKINISTCODE,
            iwb.BNDCD ,
            iwb.ACCOUNTNO ,
            iwb.BONDPROPERTIES,
            iwb.BONDRATING ,
            iwb.REISSUE_FREQUENCY ,
            iwb.CCY ,
            s1.PRINAMT ,
            case when sp.spotrate != 0 
            then sp.spotrate * s1.PRINAMT
            else p.SPOTRATE_8 *s1.PRINAMT end spotrate1,
            s1.PROCEEDAMT ,
            case when sp.spotrate != 0 
        	then sp.spotrate * s1.PROCEEDAMT
        	else p.SPOTRATE_8 *s1.PROCEEDAMT end spotrate2,
            iwb.BOND_RIGHT_DEBT_DATE ,
            TO_CHAR(s2.VDATE,'yyyy-MM-dd')  ,
            TO_CHAR(s2.MDATE,'yyyy-MM-dd') ,
            TO_CHAR(s1.dealdate,'yyyy-MM-dd'),
            s2.COUPRATE_8,
            CASE WHEN s2.MDATE < v_datadate THEN '兑付'
            ELSE '发行' END AS logo
        FROM
          spsh@opics s1
        LEFT JOIN IFS_OPICS_BOND iwb ON
        trim(iwb.BNDCD) = trim(s1.SECID)
        LEFT JOIN SECM@opics s2 ON
        s1.SECID = s2.SECID
        LEFT JOIN (
        SELECT
          ccy,
          CASE
            WHEN spotrate31_8 <> 0 THEN spotrate31_8
            WHEN spotrate30_8 <> 0 THEN spotrate30_8
            WHEN spotrate29_8 <> 0 THEN spotrate29_8
            WHEN spotrate28_8 <> 0 THEN spotrate28_8
            WHEN spotrate27_8 <> 0 THEN spotrate27_8
          END spotrate
        FROM
          SRHR@opics
        WHERE
          BR = '01'
          AND YRMONTH = v_monthyear ) sp
          ON sp.CCY = s1.ccy
        left join REVP@opics p on 
         p.ccy = s1.ccy
        WHERE
        s1.INVTYPE = 'I'
        AND s1.PRODTYPE != 'CD'
        AND s1.REVDATE IS NULL
        AND s1.SETTDATE >= TRUNC(v_datadate,'MM')
        AND s1.SETTDATE<= v_datadate ;
       commit;
       end;
       --存量特定目的载体投资信息
        elsif v_businessType = '10' then       
        begin
       --删除表数据
       delete from TB_RH_CLFUND;
       --插入数据
       insert into TB_RH_CLFUND(
                    BANKINISTCODE ,         
                    INSIDEINISTCODE ,      
                    fundtypes  ,           
                    MANAGE_PRODUCT_TYPE ,    
                    FUND_CODE  ,             
                    issuercode ,            
                    issuerarea ,             
                    NET_WORTH_TYPE  ,
                    AFP_DATE ,
                    EARLIEST_REDEMPTION_DATE  ,
                    ccy,
                    totalamt             
                    )SELECT 
                     '912301001275921118' AS  BANKINISTCODE,
                     '00100' AS INSIDEINISTCODE,
                    CASE WHEN t.PRD_NO = '803' THEN '基金管理公司及其子公司专户'
                    ELSE  '公募基金' END AS  fundtypes,
                    i.MANAGE_PRODUCT_TYPE,
                    t.FUND_CODE ,
                    '' AS issuercode,
                    '' AS issuerarea,
                    i.NET_WORTH_TYPE,
                    t.AFP_DATE ,
                    i.EARLIEST_REDEMPTION_DATE,
                    t.ccy,
                    CASE
                      WHEN i.NET_WORTH_TYPE = '0' THEN NVL((t.UT_QTY), 0)
                      ELSE NVL(t.UT_QTY*d.FUND_PRICE, 0)
                    END AS totalamt     
                    FROM FT_TPOS t LEFT JOIN FT_INFO i ON t.FUND_CODE=i.FUND_CODE 
                    LEFT JOIN FT_PRICE_DAILY d ON
                    t.FUND_CODE = d.FUND_CODE
                    WHERE to_date(t.AFP_DATE,'yyyy-MM-dd') = v_datadate ;
              commit;
              end;
     elsif v_businessType = '11' then       
        begin
       --删除表数据 特定目的载体发生额信息
       delete from TB_RH_FUNDFSE;
       --插入数据
       insert into TB_RH_FUNDFSE(
                    BANKINISTCODE ,         
                    INSIDEINISTCODE ,      
                    fundtypes  ,           
                    MANAGE_PRODUCT_TYPE ,    
                    FUND_CODE  ,             
                    issuercode ,            
                    issuerarea ,             
                    NET_WORTH_TYPE  ,
                    EST_DATE ,
                    TDATE,
                    EARLIEST_REDEMPTION_DATE  ,
                    ccy     ,
                    amt   , 
                    convertamt ,
                    tradedic  )
          SELECT 
                '912301001275921118' AS  BANKINISTCODE,
                 '00100' AS INSIDEINISTCODE,
              CASE WHEN t.PRD_NO = '803' THEN '基金管理公司及其子公司专户'
              ELSE  '公募基金' END AS  fundtypes,
              i.MANAGE_PRODUCT_TYPE,
              t.FUND_CODE ,
              '' AS issuercode,
              '' AS issuerarea,
              i.NET_WORTH_TYPE,
              i.EST_DATE ,
              t.TDATE,
              i.EARLIEST_REDEMPTION_DATE,
              t.ccy,
              t.AMT,
              t.AMT AS convertamt,
              '赎回' AS tradedic
              FROM FT_AFP_CONF t LEFT JOIN FT_INFO i ON t.FUND_CODE=i.FUND_CODE  
              WHERE TO_DATE(t.TDATE,'yyyy-MM-dd') <= v_datadate AND TO_DATE(t.TDATE,'yyyy-MM-dd') >= TRUNC( v_datadate,'MM')
              
              UNION ALL 
              
              SELECT 
               '912301001275921118' AS  BANKINISTCODE,
               '00100' AS INSIDEINISTCODE,
              CASE WHEN t.PRD_NO = '803' THEN '基金管理公司及其子公司专户'
              ELSE  '公募基金' END AS  fundtypes,
              i.MANAGE_PRODUCT_TYPE,
              t.FUND_CODE ,
              '' AS issuercode,
              '' AS issuerarea,
              i.NET_WORTH_TYPE,
              i.EST_DATE ,
              t.TDATE,
              i.EARLIEST_REDEMPTION_DATE,
              t.ccy,
              t.AMT,
              t.AMT AS convertamt,
              '申购' AS tradedic
              FROM FT_RDP_CONF t LEFT JOIN FT_INFO i ON t.FUND_CODE=i.FUND_CODE            
               WHERE TO_DATE(t.TDATE,'yyyy-MM-dd') <= v_datadate AND TO_DATE(t.TDATE,'yyyy-MM-dd') >= TRUNC( v_datadate,'MM') ;     
               commit;
          end;
                    
                     
     end if ;
  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
END SL_SP_RHJS;
  
  