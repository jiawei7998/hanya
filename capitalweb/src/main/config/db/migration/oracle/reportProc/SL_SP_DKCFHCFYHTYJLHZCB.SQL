--DROP TABLE;
DROP TABLE IFS_REPORT_DKCFHCFYHTYJLHZCB;
-- CREATE TABLE
CREATE TABLE IFS_REPORT_DKCFHCFYHTYJLHZCB
-- 贷款拆放含拆放银行同业及联行资产表


(
	SJZBM			VARCHAR2(50),
	TBJGDM			VARCHAR2(50),
	BGQ				VARCHAR2(50),	
	SFWTDK			VARCHAR2(50),
	WTSSBM			VARCHAR2(50),
	DFGJDQ			VARCHAR2(50),
	DFBM			VARCHAR2(50),
	DFYBFGX			VARCHAR2(50),
	YSQX			VARCHAR2(50),
	YSBZ			VARCHAR2(50),
	SYMBJYE			NUMBER(19,4),
	SYMYSLXYE		NUMBER(19,4),	
	BYMBJYE			NUMBER(19,4),	
	BYMBJYEQXYNYX	NUMBER(19,4),
	BYMYSLXYE		NUMBER(19,4),	
	BYFJYBD			NUMBER(19,4),
	BYJFSE			NUMBER(19,4),
	BYLXSR			NUMBER(19,4),
	BZ				VARCHAR2(50)
);



-- Add comments to the table 
comment on table IFS_REPORT_DKCFHCFYHTYJLHZCB
  is '贷款拆放含拆放银行同业及联行资产表';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.SJZBM is '数据自编码	';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.TBJGDM is '填报机构代码';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BGQ is '报告期';	
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.SFWTDK is '是否委托贷款';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.WTSSBM is '委托所属部门';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.DFGJDQ is '对方国家/地区';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.DFBM is '对方部门';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.DFYBFGX is '对方与本机构/委托人关系';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.YSQX is '原始期限';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.YSBZ is '原始币种';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.SYMBJYE is '上月末本金余额';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.SYMYSLXYE is '上月末应收利息余额';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BYMBJYE is '本月末本金余额';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BYMBJYEQXYNYX is '本月末本金余额:其中剩余期限在一年及以下';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BYMYSLXYE is '本月末应收利息余额';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BYFJYBD is '本月非交易变动';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BYJFSE is '本月净发生额';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BYLXSR is '本月利息收入';
-- Add comments to the columns 
comment on column IFS_REPORT_DKCFHCFYHTYJLHZCB.BZ is '备注';



CREATE OR REPLACE PROCEDURE SL_SP_DKCFHCFYHTYJLHZCB(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
	/*  **************************************************************************
	*  贷款拆放含拆放银行同业及联行资产表
	**************************************************************************  */
	V_DATADATE     		DATE; --当前日期
	V_POSTDATE          DATE; --账务日期
  
            

BEGIN
	--获取账务日期
	BEGIN
		SELECT prevbranprcdate INTO V_POSTDATE FROM brps@opics WHERE br = '01';
	EXCEPTION
		WHEN no_data_found THEN
		retcode := '100';
		retmsg  := 'QUERY BRPS DATA NOT FOUND';
		RETURN;
	END;

	--获取当前日期
	IF datadate IS NULL THEN
		V_DATADATE := V_POSTDATE;
	ELSE
		V_DATADATE := to_date(datadate, 'YYYY-MM-DD');
	END IF;	
	
	
	INSERT INTO IFS_REPORT_DKCFHCFYHTYJLHZCB (SJZBM,TBJGDM,BGQ,SFWTDK,WTSSBM,DFGJDQ,DFBM,DFYBFGX,YSQX,YSBZ,SYMBJYE,SYMYSLXYE,BYMBJYE,BYMBJYEQXYNYX,BYMYSLXYE,BYFJYBD,BYJFSE,BYLXSR,BZ) 
	VALUES ('1','2','3','4','5','6','7','8','9','10',11,12,13,14,15,16,17,18,'19');


  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
END SL_SP_DKCFHCFYHTYJLHZCB;		