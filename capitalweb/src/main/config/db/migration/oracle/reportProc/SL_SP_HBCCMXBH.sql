--DROP table;
drop table IFS_REPORT_HBCCMXBH;
-- Create table
create table IFS_REPORT_HBCCMXBH
(
  invType         varchar2(40),
  sd              varchar2(40),
  issuer          varchar2(40),
  cost            varchar2(40),
  secId           varchar2(40), 
  port            varchar2(40),
  jiuQi           varchar2(40),
  pvbpx           varchar2(40),
  pv01x           varchar2(40),
  varx            varchar2(40),
  fxzl            varchar2(40),
  sshy            varchar2(40),
  ztLevel         varchar2(40),
  zxLevel         varchar2(40),
  fxZtLevel       varchar2(40),
  fxZxLevel       varchar2(40),
  sName           varchar2(40),
  vDate           varchar2(40),
  tenor           varchar2(40),
  mDate           varchar2(40),
  prinAmt         varchar2(40),
  coupRate        varchar2(40),
  acctngType      varchar2(40),
  cccb            varchar2(40),
  yslx            varchar2(40),
  zmjz            varchar2(40),
  yjlx            varchar2(40),
  dnljlxsr        varchar2(40),
  syljlxsr        varchar2(40),
  tdymtm          varchar2(40),
  tjpmtm          varchar2(40),
  avgCost         varchar2(40),
  settAvgCost     varchar2(40),
  gz              varchar2(40),
  opicsJjsz       varchar2(40),
  jjsz            varchar2(40),
  yzj             varchar2(40),
  windYzj         varchar2(40),
  lxtz            varchar2(40),
  syl             varchar2(40),
  jqcbPer         varchar2(40),
  jjcbPer         varchar2(40),
  yslxPer         varchar2(40),
  yjlxPer         varchar2(40),
  tyjj            varchar2(40),
  jjfy            varchar2(40),
  windJjfy        varchar2(40),
  synx            varchar2(40),
  fxSyts          varchar2(40),
  cdjr            varchar2(40),
  nx              varchar2(40),
  fxrq            varchar2(40),
  fxl             varchar2(40),
  fxpl            varchar2(40),
  fdlx            varchar2(40),
  jzll            varchar2(40),
  fdjd            varchar2(40),
  lc              varchar2(40),
  hqz             varchar2(40),
  ifName          varchar2(40),
  ptz             varchar2(40),
  db              varchar2(40),
  dbfs            varchar2(40),
  dbName          varchar2(40),
  rbaMount        varchar2(40),
  rpaMount        varchar2(40),
  rdaMount        varchar2(40),
  sbaMount        varchar2(40),
  slaMount        varchar2(40),
  cbaMount		  varchar2(40),
  zqaMount		  varchar2(40),
  yzaMount		  varchar2(40),
  me              varchar2(40),
  yuqi			  varchar2(40),
  bz              varchar2(40)
);

comment on table IFS_REPORT_HBCCMXBH
  is '合并持仓分析表-明细变化';
comment on column IFS_REPORT_HBCCMXBH.invType is '账户类型';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.sd is '债券类型';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.issuer is '债券发行人';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.cost is '成本中心';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.secId is '债券代码';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.port is '投资组合';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.jiuQi is '久期';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.pvbpx is 'PVBP值';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.pv01x is 'PV01值';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.varx is 'VAR值';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fxzl is '债券发行总量(万)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.sshy is '债券所属行业';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.ztLevel is '最新主体评级';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.zxLevel is '最新债项评级';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fxZtLevel is '发行主体评级';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fxZxLevel is '发行债项评级';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.sName is '债券名称';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.vDate is '起息日';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.tenor is '期限';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.mDate is '到期日';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.prinAmt is '债券面值';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.coupRate is '票面利率';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.acctngType is '持仓类别';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.cccb is '持仓成本(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.yslx is '应收利息(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.zmjz is '账面价值(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.yjlx is '应计利息(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.dnljlxsr is '计算期利息收入(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.syljlxsr is '持有期利息收入(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.tdymtm is '公允价值变动(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.tjpmtm is '公允价值变动损益(元)';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.avgCost is '债券买入成本';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.settAvgCost is '债券投资成本' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.gz is '估值' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.opicsJjsz is '账面净价市值(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.jjsz is '净价市值(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.yzj is 'OPICS_溢折价(元）' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.windYzj is '溢折价(元）' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.lxtz is '利息调整(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.syl is '到期收益率' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.jqcbPer is '百元加权成本' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.jjcbPer is '百元净价成本' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.yslxPer is '百元应收利息' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.yjlxPer is '百元应计利息' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.tyjj is '摊余净价(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.jjfy is 'OPICS_净价浮赢(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.windJjfy is '净价浮赢(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.synx is '剩余年限' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fxSyts is '付息剩余天数' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.cdjr is '最近利率重定价日' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.nx is '年限' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fxrq is '发行日期' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fxl is '付息类' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fxpl is '付息频率' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fdlx is '浮动类型' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.jzll is '基准利率' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.fdjd is '浮动基点' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.lc is '利差' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.hqz is '含权类' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.ifName is '发行人' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.ptz is '是否平台债' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.db is '是否担保' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.dbfs is '担保方式' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.dbName is '担保人' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.rbaMount is '买断抵押面额(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.rpaMount is '质押面额(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.rdaMount is '国库定期存款质押面额' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.sbaMount is '债券借贷借入面额';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.slaMount is '债券借贷借出面额';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.cbaMount is '常备借贷便利质押面额';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.zqaMount is '中期借贷便利质押面额';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.yzaMount is '压债业务质押面额';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.me is '可用面额(元)' ;
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.yuqi is '是否逾期';
-- Add comments to the columns 
comment on column IFS_REPORT_HBCCMXBH.bz is '备注';

CREATE OR REPLACE PROCEDURE SL_SP_HBCCMXBH(DATADATE IN VARCHAR2,
                                        RETCODE  OUT VARCHAR2,
                                        RETMSG   OUT VARCHAR2 --错误信息
) IS
 

  V_NOWDATE       DATE;  --本月末日期
  V_LASTDATE        DATE; --上月末日期
  V_DATADATE           DATE;

BEGIN
  
    --数据日期
    BEGIN
        IF DATADATE IS NULL THEN
          BEGIN
            SELECT PREVBRANPRCDATE INTO V_DATADATE FROM BRPS@OPICS WHERE BR = '01';
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              RETCODE := '100';
              RETMSG  := 'QUERY BRPS DATA NOT FOUND';
              RETURN;
          END;
        ELSE
          V_DATADATE := TO_DATE(DATADATE, 'YYYY-MM-DD');
        END IF;
    END;




    --合并持仓明细变化表 不保留历史数据
                
    BEGIN 
      DELETE FROM IFS_REPORT_HBCCMXBH WHERE 1 = 1;
    END;
    
    --	              
    INSERT INTO IFS_REPORT_HBCCMXBH(
          INVTYPE,
          SD,
          ISSUER,
          COST,
          SECID,
          PORT,
          JIUQI,
          PVBPX,
          PV01X,
          VARX,
          FXZL,
          SSHY,
          ZTLEVEL,
          ZXLEVEL,
          FXZTLEVEL,
          FXZXLEVEL,
          SNAME,
          VDATE,
          TENOR,
          MDATE,
          PRINAMT,
          COUPRATE,
          ACCTNGTYPE,
          CCCB,
          YSLX,
          ZMJZ,
          YJLX,
          DNLJLXSR,
          SYLJLXSR,
          TDYMTM,
          TJPMTM,
          AVGCOST,
          SETTAVGCOST,
          GZ,
          OPICSJJSZ,
          JJSZ,
          YZJ,
          WINDYZJ,
          LXTZ,
          SYL,
          JQCBPER,
          JJCBPER,
          YSLXPER,
          YJLXPER,
          TYJJ,
          JJFY,
          WINDJJFY,
          SYNX,
          FXSYTS,
          CDJR,
          NX,
          FXRQ,
          FXL,
          FXPL,
          FDLX,
          JZLL,
          FDJD,
          LC,
          HQZ,
          IFNAME,
          PTZ,
          DB,
          DBFS,
          DBNAME,
          RBAMOUNT,
          RPAMOUNT,
          RDAMOUNT,
          SBAMOUNT,
          SLAMOUNT,
          CBAMOUNT,
          ZQAMOUNT,
          YZAMOUNT,
          ME,
          YUQI,
          BZ)
SELECT INVTYPE,
          SD,
          ISSUER,
          COST,
          SECID,
          PORT,
          JIUQI,
          PVBP,
          PV01,
          VARX,
          FXZL,
          SSHY,
          ZTLEVEL,
          ZXLEVEL,
          FXZTLEVEL,
          FXZXLEVEL,
          SNAME,
          VDATE,
          TENOR,
          MDATE,
          PRINAMT,
          COUPRATE_8,
          ACCTNGTYPE,
          CCCB,
          YSLX,
          ZMJZ,
          YJLX,
          DNLJLXSR,
          SYLJLXSR,
          TDYMTM,
          TJPMTM,
          AVGCOST,
          SETTAVGCOST,
          GZ,
          OPICS_JJSZ,
          JJSZ,
          YZJ,
          WIND_YZJ,
          LXTZ,
          SYL,
          JQCB_PER,
          JJCB_PER,
          YSLX_PER,
          YJLX_PER,
          TYJJ,
          JJFY,
          WIND_JJFY,
          SYNX,
          FXSYTS,
          CDJR,
          NX,
          FXRQ,
          FXL,
          FXPL,
          FDLX,
          JZLL,
          FDJD,
          LC,
          HQZ,
          IFNAME,
          PTZ,
          DB,
          DBFS,
          DBNAME,
          RBAMOUNT,
          RPAMOUNT,
          RDAMOUNT,
          SBAMOUNT,
          SLAMOUNT,
          CBAMOUNT,
          ZQAMOUNT,
          YZAMOUNT,
          ME,
          YUQI,
          BZ
  FROM (
        --明细数据
         SELECT CASE
                 WHEN TPOS.INVTYPE = 'A' THEN
                  'A可供出售类'
                 WHEN TPOS.INVTYPE = 'H' THEN
                  'C持有至到期类'
                 WHEN TPOS.INVTYPE = 'T' THEN
                  'E交易类'
               END AS INVTYPE
              ,TRIM(SICO.SD) AS SD
              ,TRIM(D.ISSUER) AS ISSUER
              ,COST.COSTDESC AS COST
              ,SECM.SECID AS SECID
              ,TPOS.PORT AS PORT
              ,KBB.JIUQI * 1 AS JIUQI
              ,KBB.PV * 1 AS PVBP
              ,CASE
                 WHEN SECM.INTCALCRULE <> 'DIS' THEN
                  KBBP.CLEAN_PRICE * TPOS.SETTQTY * 100 * KBB.PV
                 ELSE
                  KBBP.DIRTY_PRICE * TPOS.SETTQTY * 100 * KBB.PV
               END AS PV01
              ,VARDETAIL.VARX AS VARX
               ,D.NEW_QTY * 1 AS FXZL
              ,TRIM(D.INDUSTRY) AS SSHY
              ,B.ZX_ZT_LEVEL AS ZTLEVEL
              ,B.ZX_ZX_LEVEL AS ZXLEVEL
              ,B.FX_ZT_LEVEL AS FXZTLEVEL
              ,B.FX_ZX_LEVEL AS FXZXLEVEL
              ,TRIM(SECM.DESCR) AS SNAME
              ,SECM.VDATE AS VDATE
              ,CASE
                 WHEN SECM.MDATE - SECM.VDATE < 365 THEN
                  ROUND(SECM.MDATE - SECM.VDATE) || '天'
                 WHEN SECM.MDATE - SECM.VDATE >= 365 THEN
                  ROUND((SECM.MDATE - SECM.VDATE) / 365) || '年'
                 ELSE
                  NULL
               END AS TENOR
              ,SECM.MDATE AS MDATE
              ,ABS(TPOS.PRINAMT) AS PRINAMT
              ,SECM.COUPRATE_8 AS COUPRATE_8
              ,ACTY.ACCTDESC AS ACCTNGTYPE
              ,(CASE
                 WHEN TPOS.INVTYPE = 'T' THEN
                  ABS(TPOS.SETTAVGCOST)
                 WHEN TPOS.INVTYPE IN ('A', 'H') AND TPOS.PRODTYPE = 'SD' THEN
                  ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT
                 WHEN TPOS.INVTYPE IN ('A', 'H') AND TPOS.PRODTYPE = 'IM' THEN
                  ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT
               END) AS CCCB
              ,(CASE
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                  0
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                  ABS(TPOS.ACCROUTST)
                 WHEN TPOS.INVTYPE IN ('T') THEN
                  ABS(TPOS.ACCROUTST)
               END) AS YSLX
              ,(CASE
                 WHEN TPOS.INVTYPE = 'T' THEN
                  ABS(TPOS.SETTAVGCOST) + TPOS.TDYMTM
                 WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE = 'IM' THEN
                  ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT + TPOS.TDYMTM
                 WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE = 'SD' THEN
                  ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT + TPOS.TDYMTM
                 WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE = 'IM' THEN
                  ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT
                 WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE = 'SD' THEN
                  ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT
               END) AS ZMJZ
              ,(CASE
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                  ABS(TPOS.ACCROUTST)
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                  0
                 WHEN TPOS.INVTYPE = 'T' THEN
                  0
               END) AS YJLX
              ,- (TPOS.TDYINTINCEXP - TPOS.PYEINTINCEXP) + (TDYDSCINCEXP - PYEDSCINCEXP) AS DNLJLXSR
              ,-TPOS.TDYINTINCEXP AS SYLJLXSR
              ,CASE
                 WHEN TPOS.INVTYPE = 'H' THEN
                  0
                 ELSE
                  TPOS.TDYMTM
               END AS TDYMTM
              ,CASE
                 WHEN TPOS.INVTYPE IN ('H', 'A') THEN
                  0
                 ELSE
                  TPOS.TDYMTM - TPOS.PYEMTM
               END AS TJPMTM
              ,ABS(TPOS.AVGCOST) AS AVGCOST
              ,ABS(TPOS.SETTAVGCOST) AS SETTAVGCOST
              ,CASE
                 WHEN SECM.INTCALCRULE <> 'DIS' THEN
                  ROUND(KBBP.CLEAN_PRICE, 4)
                 ELSE
                  ROUND(KBBP.DIRTY_PRICE, 4)
               END AS GZ
              ,ROUND(-TPOS.MKTVAL, 4) AS OPICS_JJSZ
              ,CASE
                 WHEN SECM.INTCALCRULE <> 'DIS' THEN
                  KBBP.CLEAN_PRICE * TPOS.SETTQTY * 100
                 ELSE
                  KBBP.DIRTY_PRICE * TPOS.SETTQTY * 100
               END AS JJSZ
              ,TPOS.TDYMTM AS YZJ
              ,CASE
                 WHEN SECM.INTCALCRULE <> 'DIS' THEN
                  SETTQTY * 100 * KBBP.CLEAN_PRICE + SETTAVGCOST
                 ELSE
                  SETTQTY * 100 * KBBP.DIRTY_PRICE + SETTAVGCOST
               END AS WIND_YZJ
              ,CASE
                 WHEN TPOS.INVTYPE = 'T' THEN
                  0
                 ELSE
                  TPOS.UNAMORTAMT
               END AS LXTZ
              ,ROUND(TPOS.CLSGPRICE_8, 2) AS SYL
              ,ABS(ROUND(TPOS.AVGCOST / TPOS.SETTQTY / 100, 2)) AS JQCB_PER
              ,ABS(ROUND(TPOS.SETTAVGCOST / TPOS.SETTQTY / 100, 2)) AS JJCB_PER
              ,(CASE
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                  0
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                  ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                 WHEN TPOS.INVTYPE IN ('T') THEN
                  ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
               END) AS YSLX_PER
              ,(CASE
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                  ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                 WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                  0
                 WHEN TPOS.INVTYPE = 'T' THEN
                  0
               END) AS YJLX_PER
              ,ABS(TPOS.SETTAVGCOST) AS TYJJ
              ,TPOS.TDYMTM AS JJFY
              ,CASE
                 WHEN SECM.INTCALCRULE <> 'DIS' THEN
                  SETTQTY * 100 * KBBP.CLEAN_PRICE + SETTAVGCOST
                 ELSE
                  SETTQTY * 100 * KBBP.DIRTY_PRICE + SETTAVGCOST
               END AS WIND_JJFY
              ,ROUND((SECM.MDATE - V_DATADATE) / 365, 2) AS SYNX
              ,(SELECT MIN(IPAYDATE) - V_DATADATE
                  FROM SECS@OPICS
                 WHERE IPAYDATE > V_DATADATE
                   AND SECID = TPOS.SECID) AS FXSYTS
              ,CASE
                 WHEN (SECM.INTCALCRULE = 'NPV' AND SECM.RATECODE IS NOT NULL) OR SECM.INTCALCRULE = 'IAM' THEN
                  (SELECT MIN(IPAYDATE)
                     FROM SECS@OPICS
                    WHERE IPAYDATE > V_DATADATE
                      AND SECID = TPOS.SECID)
                 ELSE
                  NULL
               END AS CDJR
              ,ROUND((SECM.MDATE - SECM.VDATE) / 365) AS NX
              ,SECM.ISSDATE AS FXRQ
              ,CASE
                 WHEN SECM.INTCALCRULE = 'IAM' THEN
                  '一次性'
                 WHEN SECM.INTCALCRULE = 'DIS' THEN
                  '贴现'
                 WHEN SECM.INTCALCRULE = 'NPV' AND SECM.RATECODE IS NOT NULL THEN
                  '浮动'
                 ELSE
                  '固定'
               END AS FXL
              ,CASE
                 WHEN SECM.INTPAYCYCLE = 'A' THEN
                  '一年'
                 WHEN SECM.INTPAYCYCLE = 'S' THEN
                  '半年'
                 WHEN SECM.INTPAYCYCLE = 'Q' THEN
                  '季度'
                 ELSE
                  NULL
               END AS FXPL
              ,RATE.DESCR AS FDLX
              ,SECS.INTRATE_8 AS JZLL
              ,SECS.SPREADRATE_8 AS FDJD
              ,(SECS.INTRATE_8 - SECS.SPREADRATE_8) AS LC
              ,(CASE
                   WHEN D.RIGHT_TYPE = '1' THEN
                        '发行可赎回'
                   WHEN  D.RIGHT_TYPE = '2' THEN
                        '投资人可回售券'
                   ELSE
                         '普通债'
                 END) AS HQZ
              ,D.ISSUER  AS IFNAME
              ,(CASE
                 WHEN A.BATNO IS NOT NULL THEN
                 --WHEN 'YES' IS NOT NULL THEN
                  '平台债'
                 ELSE
                  '非平台债'
               END) AS PTZ
              ,(CASE
                 WHEN E.BOND_NAME IS NOT NULL THEN
                  '是'
                 ELSE
                  '否'
               END) AS DB
              ,(CASE
                 WHEN E.DB_TYPE  IS NOT NULL THEN
                  E.DB_TYPE 
                 ELSE
                  ' '
               END) AS DBFS
              ,(CASE
                 WHEN E.BOND_NAME IS NOT NULL THEN
                  E.BOND_NAME
                 ELSE
                  ' '
               END) AS DBNAME
              ,RBAMOUNT
              ,RPAMOUNT
              ,RDAMOUNT
              ,10 AS SBAMOUNT 
              ,10 AS SLAMOUNT 
              ,CBAMOUNT 
              ,ZQAMOUNT 
              ,YZAMOUNT 
              ,ABS(TPOS.PRINAMT) + DECODE(RBAMOUNT, NULL, 0, RBAMOUNT) + DECODE(RPAMOUNT, NULL, 0, RPAMOUNT) +
               DECODE(RDAMOUNT, NULL, 0, RDAMOUNT) AS ME
               ,(CASE WHEN S.SECID IS NULL THEN
                     ''
                   ELSE
                     '已逾期' 
                 END) AS  YUQI
              ,NULL AS BZ
              
          FROM SL_TPOS_H@OPICS TPOS
          LEFT JOIN SECM@OPICS
            ON SECM.SECID = TPOS.SECID
          LEFT JOIN SECS@OPICS
            ON SECS.INTSTRTDTE < V_DATADATE
           AND SECS.INTENDDTE >= V_DATADATE
           AND SECS.SECID = TPOS.SECID
          LEFT JOIN RATE@OPICS
            ON RATE.RATECODE = SECM.RATECODE
           AND TPOS.BR = RATE.BR
          LEFT JOIN COST@OPICS
            ON COST.COSTCENT = TPOS.COST
          LEFT JOIN SICO@OPICS
            ON SECM.SECMSIC = SICO.SIC
          LEFT JOIN ACTY@OPICS
            ON SECM.ACCTNGTYPE = ACTY.ACCTNGTYPE
          LEFT JOIN (SELECT MAX(RUNID) AS RUNID
                      FROM RESSTAT@OPICS
                     WHERE RUNID IN (SELECT RUNID
                                       FROM RESAGGR@OPICS
                                      WHERE --CREATEDATE = TO_DATE('2021-01-14', 'YYYY-MM-DD') AND
                                         AGGR2 = 'SECUR'
                                        AND AGGR9 IS NOT NULL
                                      GROUP BY RUNID)) VARMAXRUNID
            ON 1 = 1
          LEFT JOIN RESAGGR@OPICS RSA
            ON RSA.RUNID = VARMAXRUNID.RUNID
           AND RSA.AGGR9 IS NOT NULL
           AND RSA.AGGR1 = TPOS.BR
           AND RSA.AGGR6 = TPOS.COST
           AND TRIM(RSA.AGGR7) = TRIM(TPOS.SECID)
           AND TRIM(RSA.AGGR8) = TRIM(TPOS.PORT)
           AND SUBSTR(TRIM(RSA.AGGR9), 1, 1) = TRIM(TPOS.INVTYPE)
          LEFT JOIN (SELECT MAX(NODEID) AS NODEID
                          ,RUNID AS RUNID
                      FROM RESSTAT@OPICS
                     WHERE VARX <> 0
                       AND RUNID = (SELECT MAX(RUNID) AS RUNID
                                      FROM RESSTAT@OPICS
                                     WHERE VARX <> 0
                                       AND RUNID IN (SELECT RUNID
                                                       FROM RESAGGR@OPICS
                                                      WHERE --CREATEDATE = TO_DATE('2021-01-14', 'YYYY-MM-DD') AND
                                                         AGGR2 = 'SECUR'
                                                        AND AGGR9 IS NOT NULL
                                                      GROUP BY RUNID))
                     GROUP BY RUNID) RTR
            ON 1 = 1
          LEFT JOIN RESSTAT@OPICS VARDETAIL
            ON VARDETAIL.RUNID = VARMAXRUNID.RUNID
           AND VARDETAIL.AGGRID = RSA.AGGRID
           AND VARDETAIL.NODEID = RTR.NODEID
           AND VARDETAIL.VARX <> 0
          LEFT JOIN IFS_WIND_SECL KBBP
            ON TRIM(KBBP.SEC_CODE) = TRIM(TPOS.SECID)
          LEFT JOIN IFS_WIND_BOND_LEVEL B
            ON TRIM(B.BOND_CODE) = TRIM(TPOS.SECID)
          LEFT JOIN IFS_WIND_RISK KBB
            ON TRIM(KBB.BOND_CODE) = TRIM(TPOS.SECID)
           AND KBB.RDATE = '2021-01-14'
          LEFT JOIN (SELECT RPDT.BR
                          ,RPDT.SECID
                          ,SUM(RPDT.PRINAMT) RBAMOUNT
                          ,RPDT.INVTYPE
                          ,RPDT.COST
                          ,RPDT.PORT
                      FROM RPDT@OPICS
                     WHERE (V_DATADATE >= RPDT.VDATE AND V_DATADATE < RPDT.MDATE)
                       AND RPDT.PRODTYPE = 'RB'
                       AND RPDT.VERDATE IS NOT NULL
                       AND RPDT.REVDATE IS NULL
                     GROUP BY RPDT.BR
                             ,RPDT.SECID
                             ,RPDT.INVTYPE
                             ,RPDT.COST
                             ,RPDT.PORT) RB
            ON RB.SECID = TPOS.SECID
           AND RB.INVTYPE = TPOS.INVTYPE
           AND RB.COST = TPOS.COST
           AND RB.PORT = TPOS.PORT
           AND RB.BR = TPOS.BR
          LEFT JOIN (SELECT RPDT.BR
                          ,RPDT.SECID
                          ,SUM(RPDT.PRINAMT) RPAMOUNT
                          ,RPDT.INVTYPE
                          ,RPDT.COST
                          ,RPDT.PORT
                      FROM RPDT@OPICS
                     WHERE (V_DATADATE >= RPDT.VDATE AND V_DATADATE < RPDT.MDATE)
                       AND RPDT.PRODTYPE = 'RP'
                       AND RPDT.VERDATE IS NOT NULL
                       AND RPDT.REVDATE IS NULL
                     GROUP BY RPDT.BR
                             ,RPDT.SECID
                             ,RPDT.INVTYPE
                             ,RPDT.COST
                             ,RPDT.PORT) RP
            ON RP.SECID = TPOS.SECID
           AND RP.INVTYPE = TPOS.INVTYPE
           AND RP.COST = TPOS.COST
           AND RP.PORT = TPOS.PORT
           AND RP.BR = TPOS.BR
          LEFT JOIN (SELECT RPDT.BR
                          ,RPDT.SECID
                          ,SUM(RPDT.PRINAMT) RDAMOUNT
                          ,RPDT.INVTYPE
                          ,RPDT.COST
                          ,RPDT.PORT
                      FROM RPDT@OPICS
                     WHERE (V_DATADATE >= RPDT.VDATE AND V_DATADATE < RPDT.MDATE)
                       AND RPDT.PRODTYPE = 'GD'
                       AND RPDT.VERDATE IS NOT NULL
                       AND RPDT.REVDATE IS NULL
                     GROUP BY RPDT.BR
                             ,RPDT.SECID
                             ,RPDT.INVTYPE
                             ,RPDT.COST
                             ,RPDT.PORT) RD
            ON RD.SECID = TPOS.SECID
           AND RD.INVTYPE = TPOS.INVTYPE
           AND RD.COST = TPOS.COST
           AND RD.PORT = TPOS.PORT
           AND RD.BR = TPOS.BR
          --常备借贷便利
          LEFT JOIN (SELECT RPDT.BR
                          ,RPDT.SECID
                          ,SUM(RPDT.PRINAMT) CBAMOUNT
                          ,RPDT.INVTYPE
                          ,RPDT.COST
                          ,RPDT.PORT
                      FROM RPDT@OPICS
                     WHERE (V_DATADATE >= RPDT.VDATE AND
                           V_DATADATE < RPDT.MDATE)
                       AND RPDT.PRODTYPE = 'CB'
                       AND RPDT.VERDATE IS NOT NULL
                       AND RPDT.REVDATE IS NULL
                     GROUP BY RPDT.BR
                             ,RPDT.SECID
                             ,RPDT.INVTYPE
                             ,RPDT.COST
                             ,RPDT.PORT) CB
            ON CB.SECID = TPOS.SECID
           AND CB.INVTYPE = TPOS.INVTYPE
           AND CB.COST = TPOS.COST
           AND CB.PORT = TPOS.PORT
           AND CB.BR = TPOS.BR 
           LEFT JOIN (SELECT RPDT.BR
                          ,RPDT.SECID
                          ,SUM(RPDT.PRINAMT) ZQAMOUNT
                          ,RPDT.INVTYPE
                          ,RPDT.COST
                          ,RPDT.PORT
                      FROM RPDT@OPICS
                     WHERE (V_DATADATE >= RPDT.VDATE AND
                           V_DATADATE < RPDT.MDATE)
                       AND RPDT.PRODTYPE = 'ZQ'
                       AND RPDT.VERDATE IS NOT NULL
                       AND RPDT.REVDATE IS NULL
                     GROUP BY RPDT.BR
                             ,RPDT.SECID
                             ,RPDT.INVTYPE
                             ,RPDT.COST
                             ,RPDT.PORT) ZQ
            ON ZQ.SECID = TPOS.SECID
           AND ZQ.INVTYPE = TPOS.INVTYPE
           AND ZQ.COST = TPOS.COST
           AND ZQ.PORT = TPOS.PORT
           AND ZQ.BR = TPOS.BR  
          LEFT JOIN (SELECT RPDT.BR
                          ,RPDT.SECID
                          ,SUM(RPDT.PRINAMT) YZAMOUNT
                          ,RPDT.INVTYPE
                          ,RPDT.COST
                          ,RPDT.PORT
                      FROM RPDT@OPICS
                     WHERE (V_DATADATE >= RPDT.VDATE AND
                           V_DATADATE < RPDT.MDATE)
                       AND RPDT.PRODTYPE = 'ZD'
                       AND RPDT.VERDATE IS NOT NULL
                       AND RPDT.REVDATE IS NULL
                     GROUP BY RPDT.BR
                             ,RPDT.SECID
                             ,RPDT.INVTYPE
                             ,RPDT.COST
                             ,RPDT.PORT) ZD
            ON ZD.SECID = TPOS.SECID
           AND ZD.INVTYPE = TPOS.INVTYPE
           AND ZD.COST = TPOS.COST
           AND ZD.PORT = TPOS.PORT
           AND ZD.BR = TPOS.BR    
          LEFT JOIN IFS_WIND_BOND D
            ON TRIM(D.BOND_CODE) = TRIM(TPOS.SECID)
           AND TRIM(D.CIRCUPLACE) = '银行间'
          LEFT JOIN  K_CUST_BLACKFORMS@OPICS A
            ON TRIM(D.MAINCORPID) = TRIM(A.BATNO)
          LEFT JOIN IFS_WIND_BOND_ADD  E
            ON TRIM(E.BOND_CODE) = TRIM(TPOS.SECID)
          LEFT JOIN SL_FI_DEFAULT@OPICS S 
            ON TRIM(S.SECID) = TRIM(TPOS.SECID) AND S.STATUS = '1'    
         WHERE TPOS.SETTQTY > 0
           AND TRIM(TPOS.BR) = '01'
           AND TPOS.POSTDATE = V_DATADATE   
        
        UNION ALL
        
        --小计统计
        SELECT INVTYPE
              ,NULL AS SD
              ,NULL AS ISSUER
              ,NULL AS COST
              ,NULL AS SECID
              ,NULL AS PORT
              ,SUM(JIUQI * JJSZ) / SUM(JJSZ) AS JIUQI
              ,SUM(PVBP * JJSZ) / SUM(JJSZ) AS PVBP
              ,SUM(PV01) AS PV01
              ,SUM(VARX) AS VARX
              ,SUM(FXZL) AS FXZL
              ,NULL AS SSHY
              ,NULL AS ZTLEVEL
              ,NULL AS ZXLEVEL
              ,NULL AS FXZTLEVEL
              ,NULL AS FXZXLEVEL
              ,NULL AS SNAME
              ,NULL AS VDATE
              ,NULL AS TENOR
              ,NULL AS MDATE
              ,SUM(PRINAMT) AS PRINAMT
              ,NULL AS COUPRATE_8
              ,NULL AS ACCTNGTYPE
              ,SUM(CCCB) AS CCCB
              ,SUM(YSLX) AS YSLX
              ,SUM(ZMJZ) AS ZMJZ
              ,SUM(YJLX) AS YJLX
              ,SUM(DNLJLXSR) AS DNLJLXSR
              ,SUM(SYLJLXSR) AS SYLJLXSR
              ,SUM(TDYMTM) AS TDYMTM
              ,SUM(TJPMTM) AS TJPMTM
              ,SUM(AVGCOST) AS AVGCOST
              ,SUM(SETTAVGCOST) AS SETTAVGCOST
              ,NULL AS GZ
              ,SUM(JJSZ) AS JJSZ
              ,SUM(OPICS_JJSZ) AS OPICS_JJSZ
              ,SUM(YZJ) AS YZJ
              ,SUM(WIND_YZJ) AS WIND_YZJ
              ,SUM(LXTZ) AS LXTZ
              ,NULL AS SYL
              ,NULL AS JQCB_PER
              ,NULL AS JJCB_PER
              ,NULL AS YSLX_PER
              ,NULL AS YJLX_PER
              ,SUM(TYJJ) AS TYJJ
              ,SUM(JJFY) AS JJFY
              ,SUM(WIND_JJFY) AS WIND_JJFY
              ,NULL AS SYNX
              ,NULL AS FXSYTS
              ,NULL AS CDJR
              ,NULL AS NX
              ,NULL AS FXRQ
              ,NULL AS FXL
              ,NULL AS FXPL
              ,NULL AS FDLX
              ,NULL AS JZLL
              ,NULL AS FDJD
              ,NULL AS LC
              ,NULL AS HQZ
              ,NULL AS IFNAME
              ,NULL AS PTZ
              ,NULL AS DB
              ,NULL AS DBFS
              ,NULL AS DBNAME
              ,SUM(RBAMOUNT) AS RBAMOUNT
              ,SUM(RPAMOUNT) AS RPAMOUNT
              ,SUM(RDAMOUNT) AS RDAMOUNT
              ,SUM(SBAMOUNT) AS SBAMOUNT 
              ,SUM(SLAMOUNT) AS SLAMOUNT 
              ,SUM(CBAMOUNT) AS CBAMOUNT 
              ,SUM(ZQAMOUNT) AS ZQAMOUNT 
              ,SUM(YZAMOUNT) AS YZAMOUNT 
              ,SUM(ME) AS MZ
              ,NULL AS YUQI
              ,NULL AS BZ
          FROM (
          
                        SELECT CASE
                             WHEN TPOS.INVTYPE = 'A' THEN
                              'B可供出售类小计'
                             WHEN TPOS.INVTYPE = 'H' THEN
                              'D持有至到期类小计'
                             WHEN TPOS.INVTYPE = 'T' THEN
                              'F交易类小计'
                           END AS INVTYPE
                          ,TRIM(SICO.SD) AS SD
                          ,TRIM(D.ISSUER) AS ISSUER
                          ,COST.COSTDESC AS COST
                          ,SECM.SECID AS SECID
                          ,TPOS.PORT AS PORT
                          ,KBB.JIUQI * 1 AS JIUQI
                          ,KBB.PV * 1 AS PVBP
                          ,CASE
                             WHEN SECM.INTCALCRULE <> 'DIS' THEN
                              KBBP.CLEAN_PRICE * TPOS.SETTQTY * 100 * KBB.PV
                             ELSE
                              KBBP.DIRTY_PRICE * TPOS.SETTQTY * 100 * KBB.PV
                           END AS PV01
                          ,VARDETAIL.VARX AS VARX
                          ,D.NEW_QTY * 1 AS FXZL
                          ,TRIM(D.INDUSTRY) AS SSHY                    
                          ,B.ZX_ZT_LEVEL AS ZTLEVEL
                          ,B.ZX_ZX_LEVEL AS ZXLEVEL
                          ,B.FX_ZT_LEVEL AS FXZTLEVEL
                          ,B.FX_ZX_LEVEL AS FXZXLEVEL
                          ,TRIM(SECM.DESCR) AS SNAME
                          ,SECM.VDATE AS VDATE
                          ,CASE
                             WHEN SECM.MDATE - SECM.VDATE < 365 THEN
                              ROUND(SECM.MDATE - SECM.VDATE) || '天'
                             WHEN SECM.MDATE - SECM.VDATE >= 365 THEN
                              ROUND((SECM.MDATE - SECM.VDATE) / 365) || '年'
                             ELSE
                              NULL
                           END AS TENOR
                          ,SECM.MDATE AS MDATE
                          ,ABS(TPOS.PRINAMT) AS PRINAMT
                          ,SECM.COUPRATE_8 AS COUPRATE_8
                          ,ACTY.ACCTDESC AS ACCTNGTYPE
                          ,(CASE
                             WHEN TPOS.INVTYPE = 'T' THEN
                              ABS(TPOS.SETTAVGCOST)
                             WHEN TPOS.INVTYPE IN ('A', 'H') AND TPOS.PRODTYPE = 'SD' THEN
                              ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT
                             WHEN TPOS.INVTYPE IN ('A', 'H') AND TPOS.PRODTYPE = 'IM' THEN
                              ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT
                           END) AS CCCB
                          ,(CASE
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                              0
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                              ABS(TPOS.ACCROUTST)
                             WHEN TPOS.INVTYPE IN ('T') THEN
                              ABS(TPOS.ACCROUTST)
                           END) AS YSLX
                          ,(CASE
                             WHEN TPOS.INVTYPE = 'T' THEN
                              ABS(TPOS.SETTAVGCOST) + TPOS.TDYMTM
                             WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE = 'IM' THEN
                              ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT + TPOS.TDYMTM
                             WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE = 'SD' THEN
                              ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT + TPOS.TDYMTM
                             WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE = 'IM' THEN
                              ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT
                             WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE = 'SD' THEN
                              ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT
                           END) AS ZMJZ
                          ,(CASE
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                              ABS(TPOS.ACCROUTST)
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                              0
                             WHEN TPOS.INVTYPE = 'T' THEN
                              0
                           END) AS YJLX
                          ,- (TPOS.TDYINTINCEXP - TPOS.PYEINTINCEXP) + (TDYDSCINCEXP - PYEDSCINCEXP) AS DNLJLXSR
                          ,-TPOS.TDYINTINCEXP AS SYLJLXSR
                          ,CASE
                             WHEN TPOS.INVTYPE = 'H' THEN
                              0
                             ELSE
                              TPOS.TDYMTM
                           END AS TDYMTM
                          ,CASE
                             WHEN TPOS.INVTYPE IN ('H', 'A') THEN
                              0
                             ELSE
                              TPOS.TDYMTM - TPOS.PYEMTM
                           END AS TJPMTM
                          ,ABS(TPOS.AVGCOST) AS AVGCOST
                          ,ABS(TPOS.SETTAVGCOST) AS SETTAVGCOST
                          ,CASE
                             WHEN SECM.INTCALCRULE <> 'DIS' THEN
                              ROUND(KBBP.CLEAN_PRICE, 4)
                             ELSE
                              ROUND(KBBP.DIRTY_PRICE, 4)
                           END AS GZ
                          ,ROUND(-TPOS.MKTVAL, 4) AS OPICS_JJSZ
                          ,CASE
                             WHEN SECM.INTCALCRULE <> 'DIS' THEN
                              KBBP.CLEAN_PRICE * TPOS.SETTQTY * 100
                             ELSE
                              KBBP.DIRTY_PRICE * TPOS.SETTQTY * 100
                           END AS JJSZ
                          ,TPOS.TDYMTM AS YZJ
                          ,CASE
                             WHEN SECM.INTCALCRULE <> 'DIS' THEN
                              SETTQTY * 100 * KBBP.CLEAN_PRICE + SETTAVGCOST
                             ELSE
                              SETTQTY * 100 * KBBP.DIRTY_PRICE + SETTAVGCOST
                           END AS WIND_YZJ
                          ,CASE
                             WHEN TPOS.INVTYPE = 'T' THEN
                              0
                             ELSE
                              TPOS.UNAMORTAMT
                           END AS LXTZ
                          ,ROUND(TPOS.CLSGPRICE_8, 2) AS SYL
                          ,ABS(ROUND(TPOS.AVGCOST / TPOS.SETTQTY / 100, 2)) AS JQCB_PER
                          ,ABS(ROUND(TPOS.SETTAVGCOST / TPOS.SETTQTY / 100, 2)) AS JJCB_PER
                          ,(CASE
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                              0
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                              ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                             WHEN TPOS.INVTYPE IN ('T') THEN
                              ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                           END) AS YSLX_PER
                          ,(CASE
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                              ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                             WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                              0
                             WHEN TPOS.INVTYPE = 'T' THEN
                              0
                           END) AS YJLX_PER
                          ,ABS(TPOS.SETTAVGCOST) AS TYJJ
                          ,TPOS.TDYMTM AS JJFY
                          ,CASE
                             WHEN SECM.INTCALCRULE <> 'DIS' THEN
                              SETTQTY * 100 * KBBP.CLEAN_PRICE + SETTAVGCOST
                             ELSE
                              SETTQTY * 100 * KBBP.DIRTY_PRICE + SETTAVGCOST
                           END AS WIND_JJFY
                          ,ROUND((SECM.MDATE - V_DATADATE) / 365, 2) AS SYNX
                          ,(SELECT MIN(IPAYDATE) - V_DATADATE
                              FROM SECS@OPICS
                             WHERE IPAYDATE > V_DATADATE
                               AND SECID = TPOS.SECID) AS FXSYTS
                          ,CASE
                             WHEN (SECM.INTCALCRULE = 'NPV' AND SECM.RATECODE IS NOT NULL) OR SECM.INTCALCRULE = 'IAM' THEN
                              (SELECT MIN(IPAYDATE)
                                 FROM SECS@OPICS
                                WHERE IPAYDATE > V_DATADATE
                                  AND SECID = TPOS.SECID)
                             ELSE
                              NULL
                           END AS CDJR
                          ,ROUND((SECM.MDATE - SECM.VDATE) / 365) AS NX
                          ,SECM.ISSDATE AS FXRQ
                          ,CASE
                             WHEN SECM.INTCALCRULE = 'IAM' THEN
                              '一次性'
                             WHEN SECM.INTCALCRULE = 'DIS' THEN
                              '贴现'
                             WHEN SECM.INTCALCRULE = 'NPV' AND SECM.RATECODE IS NOT NULL THEN
                              '浮动'
                             ELSE
                              '固定'
                           END AS FXL
                          ,CASE
                             WHEN SECM.INTPAYCYCLE = 'A' THEN
                              '一年'
                             WHEN SECM.INTPAYCYCLE = 'S' THEN
                              '半年'
                             WHEN SECM.INTPAYCYCLE = 'Q' THEN
                              '季度'
                             ELSE
                              NULL
                           END AS FXPL
                          ,RATE.DESCR AS FDLX
                          ,SECS.INTRATE_8 AS JZLL
                          ,SECS.SPREADRATE_8 AS FDJD
                          ,(SECS.INTRATE_8 - SECS.SPREADRATE_8) AS LC
                          ,(CASE
                               WHEN D.RIGHT_TYPE = '1' THEN
                                    '发行可赎回'
                               WHEN  D.RIGHT_TYPE = '2' THEN
                                    '投资人可回售券'
                               ELSE
                                     '普通债'
                             END) AS HQZ
                          ,D.ISSUER AS IFNAME
                          ,(CASE
                             WHEN A.BATNO IS NOT NULL THEN
                             --WHEN 'YES' IS NOT NULL THEN
                              '平台债'
                             ELSE
                              '非平台债'
                           END) AS PTZ
                          ,(CASE
                             WHEN E.BOND_NAME IS NOT NULL THEN
                              '是'
                             ELSE
                              '否'
                           END) AS DB
                          ,(CASE
                             WHEN E.DB_TYPE  IS NOT NULL THEN
                                  E.DB_TYPE
                             ELSE
                              ' '
                           END) AS DBFS
                          ,(CASE
                             WHEN E.BOND_NAME IS NOT NULL THEN
                              E.BOND_NAME
                             ELSE
                              ' '
                           END) AS DBNAME
                          ,RBAMOUNT
                          ,RPAMOUNT
                          ,RDAMOUNT
                          ,10 AS SBAMOUNT 
                          ,10 AS SLAMOUNT 
                          ,CBAMOUNT 
                          ,ZQAMOUNT 
                          ,YZAMOUNT 
                          ,ABS(TPOS.PRINAMT) + DECODE(RBAMOUNT, NULL, 0, RBAMOUNT) + DECODE(RPAMOUNT, NULL, 0, RPAMOUNT) +
                           DECODE(RDAMOUNT, NULL, 0, RDAMOUNT) AS ME
                          ,'Y' AS YUQI
                          ,NULL AS BZ
                          
                      FROM SL_TPOS_H@OPICS TPOS
                      LEFT JOIN SECM@OPICS
                        ON SECM.SECID = TPOS.SECID
                      LEFT JOIN SECS@OPICS
                        ON SECS.INTSTRTDTE < V_DATADATE
                       AND SECS.INTENDDTE >= V_DATADATE
                       AND SECS.SECID = TPOS.SECID
                      LEFT JOIN RATE@OPICS
                        ON RATE.RATECODE = SECM.RATECODE
                       AND TPOS.BR = RATE.BR
                      LEFT JOIN COST@OPICS
                        ON COST.COSTCENT = TPOS.COST
                      LEFT JOIN SICO@OPICS
                        ON SECM.SECMSIC = SICO.SIC
                      LEFT JOIN ACTY@OPICS
                        ON SECM.ACCTNGTYPE = ACTY.ACCTNGTYPE
                      LEFT JOIN (SELECT MAX(RUNID) AS RUNID
                                  FROM RESSTAT@OPICS
                                 WHERE RUNID IN (SELECT RUNID
                                                   FROM RESAGGR@OPICS
                                                  WHERE --CREATEDATE = V_DATADATE AND
                                                     AGGR2 = 'SECUR'
                                                    AND AGGR9 IS NOT NULL
                                                  GROUP BY RUNID)) VARMAXRUNID
                        ON 1 = 1
                      LEFT JOIN RESAGGR@OPICS RSA --bak
                        ON RSA.RUNID = VARMAXRUNID.RUNID
                       AND RSA.AGGR9 IS NOT NULL
                       AND RSA.AGGR1 = TPOS.BR
                       AND RSA.AGGR6 = TPOS.COST
                       AND TRIM(RSA.AGGR7) = TRIM(TPOS.SECID)
                       AND TRIM(RSA.AGGR8) = TRIM(TPOS.PORT)
                       AND SUBSTR(TRIM(RSA.AGGR9), 1, 1) = TRIM(TPOS.INVTYPE)
                      LEFT JOIN (SELECT MAX(NODEID) AS NODEID
                                      ,RUNID AS RUNID
                                  FROM RESSTAT@OPICS
                                 WHERE VARX <> 0
                                   AND RUNID = (SELECT MAX(RUNID) AS RUNID
                                                  FROM RESSTAT@OPICS
                                                 WHERE VARX <> 0
                                                   AND RUNID IN (SELECT RUNID
                                                                   FROM RESAGGR@OPICS
                                                                  WHERE --CREATEDATE = V_DATADATE AND
                                                                     AGGR2 = 'SECUR'
                                                                    AND AGGR9 IS NOT NULL
                                                                  GROUP BY RUNID))
                                 GROUP BY RUNID) RTR
                        ON 1 = 1
                      LEFT JOIN RESSTAT@OPICS VARDETAIL
                        ON VARDETAIL.RUNID = VARMAXRUNID.RUNID
                       AND VARDETAIL.AGGRID = RSA.AGGRID
                       AND VARDETAIL.NODEID = RTR.NODEID
                       AND VARDETAIL.VARX <> 0
                      LEFT JOIN IFS_WIND_SECL  KBBP
                        ON TRIM(KBBP.SEC_CODE) = TRIM(TPOS.SECID)
                      LEFT JOIN IFS_WIND_BOND_LEVEL  B
                        ON TRIM(B.BOND_CODE) = TRIM(TPOS.SECID)
                      LEFT JOIN IFS_WIND_RISK  KBB
                        ON TRIM(KBB.BOND_CODE) = TRIM(TPOS.SECID)
                       AND KBB.RDATE = '2021-01-14'
                      LEFT JOIN (SELECT RPDT.BR
                                      ,RPDT.SECID
                                      ,SUM(RPDT.PRINAMT) RBAMOUNT
                                      ,RPDT.INVTYPE
                                      ,RPDT.COST
                                      ,RPDT.PORT
                                  FROM RPDT@OPICS
                                 WHERE (V_DATADATE >= RPDT.VDATE AND
                                       V_DATADATE < RPDT.MDATE)
                                   AND RPDT.PRODTYPE = 'RB'
                                   AND RPDT.VERDATE IS NOT NULL
                                   AND RPDT.REVDATE IS NULL
                                 GROUP BY RPDT.BR
                                         ,RPDT.SECID
                                         ,RPDT.INVTYPE
                                         ,RPDT.COST
                                         ,RPDT.PORT) RB
                        ON RB.SECID = TPOS.SECID
                       AND RB.INVTYPE = TPOS.INVTYPE
                       AND RB.COST = TPOS.COST
                       AND RB.PORT = TPOS.PORT
                       AND RB.BR = TPOS.BR
                      LEFT JOIN (SELECT RPDT.BR
                                      ,RPDT.SECID
                                      ,SUM(RPDT.PRINAMT) RPAMOUNT
                                      ,RPDT.INVTYPE
                                      ,RPDT.COST
                                      ,RPDT.PORT
                                  FROM RPDT@OPICS
                                 WHERE (V_DATADATE >= RPDT.VDATE AND
                                       V_DATADATE < RPDT.MDATE)
                                   AND RPDT.PRODTYPE = 'RP'
                                   AND RPDT.VERDATE IS NOT NULL
                                   AND RPDT.REVDATE IS NULL
                                 GROUP BY RPDT.BR
                                         ,RPDT.SECID
                                         ,RPDT.INVTYPE
                                         ,RPDT.COST
                                         ,RPDT.PORT) RP
                        ON RP.SECID = TPOS.SECID
                       AND RP.INVTYPE = TPOS.INVTYPE
                       AND RP.COST = TPOS.COST
                       AND RP.PORT = TPOS.PORT
                       AND RP.BR = TPOS.BR
                      LEFT JOIN (SELECT RPDT.BR
                                      ,RPDT.SECID
                                      ,SUM(RPDT.PRINAMT) RDAMOUNT
                                      ,RPDT.INVTYPE
                                      ,RPDT.COST
                                      ,RPDT.PORT
                                  FROM RPDT@OPICS
                                 WHERE (V_DATADATE >= RPDT.VDATE AND
                                       V_DATADATE < RPDT.MDATE)
                                   AND RPDT.PRODTYPE = 'GD'
                                   AND RPDT.VERDATE IS NOT NULL
                                   AND RPDT.REVDATE IS NULL
                                 GROUP BY RPDT.BR
                                         ,RPDT.SECID
                                         ,RPDT.INVTYPE
                                         ,RPDT.COST
                                         ,RPDT.PORT) RD
                        ON RD.SECID = TPOS.SECID
                       AND RD.INVTYPE = TPOS.INVTYPE
                       AND RD.COST = TPOS.COST
                       AND RD.PORT = TPOS.PORT
                       AND RD.BR = TPOS.BR
                      --常备借贷便利
                      LEFT JOIN (SELECT RPDT.BR
                                      ,RPDT.SECID
                                      ,SUM(RPDT.PRINAMT) CBAMOUNT
                                      ,RPDT.INVTYPE
                                      ,RPDT.COST
                                      ,RPDT.PORT
                                  FROM RPDT@OPICS
                                 WHERE (V_DATADATE >= RPDT.VDATE AND
                                       V_DATADATE < RPDT.MDATE)
                                   AND RPDT.PRODTYPE = 'CB'
                                   AND RPDT.VERDATE IS NOT NULL
                                   AND RPDT.REVDATE IS NULL
                                 GROUP BY RPDT.BR
                                         ,RPDT.SECID
                                         ,RPDT.INVTYPE
                                         ,RPDT.COST
                                         ,RPDT.PORT) CB
                        ON CB.SECID = TPOS.SECID
                       AND CB.INVTYPE = TPOS.INVTYPE
                       AND CB.COST = TPOS.COST
                       AND CB.PORT = TPOS.PORT
                       AND CB.BR = TPOS.BR 
                      LEFT JOIN (SELECT RPDT.BR
                                      ,RPDT.SECID
                                      ,SUM(RPDT.PRINAMT) ZQAMOUNT
                                      ,RPDT.INVTYPE
                                      ,RPDT.COST
                                      ,RPDT.PORT
                                  FROM RPDT@OPICS
                                 WHERE (V_DATADATE >= RPDT.VDATE AND
                                       V_DATADATE < RPDT.MDATE)
                                   AND RPDT.PRODTYPE = 'ZQ'
                                   AND RPDT.VERDATE IS NOT NULL
                                   AND RPDT.REVDATE IS NULL
                                 GROUP BY RPDT.BR
                                         ,RPDT.SECID
                                         ,RPDT.INVTYPE
                                         ,RPDT.COST
                                         ,RPDT.PORT) ZQ
                        ON ZQ.SECID = TPOS.SECID
                       AND ZQ.INVTYPE = TPOS.INVTYPE
                       AND ZQ.COST = TPOS.COST
                       AND ZQ.PORT = TPOS.PORT
                       AND ZQ.BR = TPOS.BR  
                      LEFT JOIN (SELECT RPDT.BR
                                      ,RPDT.SECID
                                      ,SUM(RPDT.PRINAMT) YZAMOUNT
                                      ,RPDT.INVTYPE
                                      ,RPDT.COST
                                      ,RPDT.PORT
                                  FROM RPDT@OPICS
                                 WHERE (V_DATADATE >= RPDT.VDATE AND
                                       V_DATADATE < RPDT.MDATE)
                                   AND RPDT.PRODTYPE = 'ZD'
                                   AND RPDT.VERDATE IS NOT NULL
                                   AND RPDT.REVDATE IS NULL
                                 GROUP BY RPDT.BR
                                         ,RPDT.SECID
                                         ,RPDT.INVTYPE
                                         ,RPDT.COST
                                         ,RPDT.PORT) ZD
                        ON ZD.SECID = TPOS.SECID
                       AND ZD.INVTYPE = TPOS.INVTYPE
                       AND ZD.COST = TPOS.COST
                       AND ZD.PORT = TPOS.PORT
                       AND ZD.BR = TPOS.BR    
                      LEFT JOIN IFS_WIND_BOND  D
                        ON TRIM(D.BOND_CODE) = TRIM(TPOS.SECID)
                       AND TRIM(D.CIRCUPLACE) = '银行间'
                      LEFT JOIN K_CUST_BLACKFORMS@OPICS A
                        ON TRIM(D.MAINCORPID) = TRIM(A.BATNO)
                      LEFT JOIN IFS_WIND_BOND_ADD  E
                        ON TRIM(E.BOND_CODE) = TRIM(TPOS.SECID)
                      LEFT JOIN SL_FI_DEFAULT@OPICS S 
                        ON TRIM(S.SECID) = TRIM(TPOS.SECID) AND S.STATUS = '1' 
                     WHERE TPOS.SETTQTY > 0
                       AND TRIM(TPOS.BR) = '01'
                       AND TPOS.POSTDATE = V_DATADATE
                       
                   )
         GROUP BY INVTYPE
         
         
         
         
        UNION ALL
        
        
        --汇总统计
        SELECT INVTYPE
              ,NULL AS SD
              ,NULL AS ISSUER
              ,NULL AS COST
              ,NULL AS SECID
              ,NULL AS PORT
              ,SUM(JIUQI * JJSZ) / SUM(JJSZ) AS JIUQI
              ,SUM(PVBP * JJSZ) / SUM(JJSZ) AS PVBP
              ,SUM(PV01) AS PV01
              ,SUM(VARX) AS VARX
              ,SUM(FXZL) AS FXZL
              ,NULL AS SSHY
              ,NULL AS ZTLEVEL
              ,NULL AS ZXLEVEL
              ,NULL AS FXZTLEVEL
              ,NULL AS FXZXLEVEL
              ,NULL AS SNAME
              ,NULL AS VDATE
              ,NULL AS TENOR
              ,NULL AS MDATE
              ,SUM(PRINAMT) AS PRINAMT
              ,NULL AS COUPRATE_8
              ,NULL AS ACCTNGTYPE
              ,SUM(CCCB) AS CCCB
              ,SUM(YSLX) AS YSLX
              ,SUM(ZMJZ) AS ZMJZ
              ,SUM(YJLX) AS YJLX
              ,SUM(DNLJLXSR) AS DNLJLXSR
              ,SUM(SYLJLXSR) AS SYLJLXSR
              ,SUM(TDYMTM) AS TDYMTM
              ,SUM(TJPMTM) AS TJPMTM
              ,SUM(AVGCOST) AS AVGCOST
              ,SUM(SETTAVGCOST) AS SETTAVGCOST
              ,NULL AS GZ
              ,SUM(JJSZ) AS JJSZ
              ,SUM(OPICS_JJSZ) AS OPICS_JJSZ
              ,SUM(YZJ) AS YZJ
              ,SUM(WIND_YZJ) AS WIND_YZJ
              ,SUM(LXTZ) AS LXTZ
              ,NULL AS SYL
              ,NULL AS JQCB_PER
              ,NULL AS JJCB_PER
              ,NULL AS YSLX_PER
              ,NULL AS YJLX_PER
              ,SUM(TYJJ) AS TYJJ
              ,SUM(JJFY) AS JJFY
              ,SUM(WIND_JJFY) AS WIND_JJFY
              ,NULL AS SYNX
              ,NULL AS FXSYTS
              ,NULL AS CDJR
              ,NULL AS NX
              ,NULL AS FXRQ
              ,NULL AS FXL
              ,NULL AS FXPL
              ,NULL AS FDLX
              ,NULL AS JZLL
              ,NULL AS FDJD
              ,NULL AS LC
              ,NULL AS HQZ
              ,NULL AS IFNAME
              ,NULL AS PTZ
              ,NULL AS DB
              ,NULL AS DBFS
              ,NULL AS DBNAME
              ,SUM(RBAMOUNT) AS RBAMOUNT
              ,SUM(RPAMOUNT) AS RPAMOUNT
              ,SUM(RDAMOUNT) AS RDAMOUNT
              ,SUM(SBAMOUNT) AS SBAMOUNT 
              ,SUM(SLAMOUNT)  AS SLAMOUNT 
              ,SUM(CBAMOUNT)  AS CBAMOUNT 
              ,SUM(ZQAMOUNT)  AS ZQAMOUNT 
              ,SUM(YZAMOUNT)  AS YZAMOUNT 
              ,SUM(ME) AS MZ
              ,NULL AS YUQI
              ,NULL AS BZ
                FROM (
                       
                       SELECT 'G总计' AS INVTYPE
                            ,TRIM(SICO.SD) AS SD
                            ,TRIM(D.ISSUER) AS ISSUER
                            ,COST.COSTDESC AS COST
                            ,SECM.SECID AS SECID
                            ,TPOS.PORT AS PORT
                            ,KBB.JIUQI * 1 AS JIUQI
                            ,KBB.PV * 1 AS PVBP
                            ,CASE
                               WHEN SECM.INTCALCRULE <> 'DIS' THEN
                                KBBP.CLEAN_PRICE * TPOS.SETTQTY * 100 * KBB.PV
                               ELSE
                                KBBP.DIRTY_PRICE * TPOS.SETTQTY * 100 * KBB.PV
                             END AS PV01
                            ,VARDETAIL.VARX AS VARX
                            ,D.NEW_QTY * 1 AS FXZL
                            ,TRIM(D.INDUSTRY) AS SSHY
                            ,B.ZX_ZT_LEVEL AS ZTLEVEL
                            ,B.ZX_ZX_LEVEL AS ZXLEVEL
                            ,B.FX_ZT_LEVEL AS FXZTLEVEL
                            ,B.FX_ZX_LEVEL AS FXZXLEVEL
                            ,TRIM(SECM.DESCR) AS SNAME
                            ,SECM.VDATE AS VDATE
                            ,CASE
                               WHEN SECM.MDATE - SECM.VDATE < 365 THEN
                                ROUND(SECM.MDATE - SECM.VDATE) || '天'
                               WHEN SECM.MDATE - SECM.VDATE >= 365 THEN
                                ROUND((SECM.MDATE - SECM.VDATE) / 365) || '年'
                               ELSE
                                NULL
                             END AS TENOR
                            ,SECM.MDATE AS MDATE
                            ,ABS(TPOS.PRINAMT) AS PRINAMT
                            ,SECM.COUPRATE_8 AS COUPRATE_8
                            ,ACTY.ACCTDESC AS ACCTNGTYPE
                            ,(CASE
                               WHEN TPOS.INVTYPE = 'T' THEN
                                ABS(TPOS.SETTAVGCOST)
                               WHEN TPOS.INVTYPE IN ('A', 'H') AND TPOS.PRODTYPE = 'SD' THEN
                                ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT
                               WHEN TPOS.INVTYPE IN ('A', 'H') AND TPOS.PRODTYPE = 'IM' THEN
                                ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT
                             END) AS CCCB
                            ,(CASE
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                                0
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                                ABS(TPOS.ACCROUTST)
                               WHEN TPOS.INVTYPE IN ('T') THEN
                                ABS(TPOS.ACCROUTST)
                             END) AS YSLX
                            ,(CASE
                               WHEN TPOS.INVTYPE = 'T' THEN
                                ABS(TPOS.SETTAVGCOST) + TPOS.TDYMTM
                               WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE = 'IM' THEN
                                ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT + TPOS.TDYMTM
                               WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE = 'SD' THEN
                                ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT + TPOS.TDYMTM
                               WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE = 'IM' THEN
                                ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT
                               WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE = 'SD' THEN
                                ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT
                             END) AS ZMJZ
                            ,(CASE
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                                ABS(TPOS.ACCROUTST)
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                                0
                               WHEN TPOS.INVTYPE = 'T' THEN
                                0
                             END) AS YJLX
                            ,- (TPOS.TDYINTINCEXP - TPOS.PYEINTINCEXP) + (TDYDSCINCEXP - PYEDSCINCEXP) AS DNLJLXSR
                            ,-TPOS.TDYINTINCEXP AS SYLJLXSR
                            ,CASE
                               WHEN TPOS.INVTYPE = 'H' THEN
                                0
                               ELSE
                                TPOS.TDYMTM
                             END AS TDYMTM
                            ,CASE
                               WHEN TPOS.INVTYPE IN ('H', 'A') THEN
                                0
                               ELSE
                                TPOS.TDYMTM - TPOS.PYEMTM
                             END AS TJPMTM
                            ,ABS(TPOS.AVGCOST) AS AVGCOST
                            ,ABS(TPOS.SETTAVGCOST) AS SETTAVGCOST
                            ,CASE
                               WHEN SECM.INTCALCRULE <> 'DIS' THEN
                                ROUND(KBBP.CLEAN_PRICE, 4)
                               ELSE
                                ROUND(KBBP.DIRTY_PRICE, 4)
                             END AS GZ
                            ,ROUND(-TPOS.MKTVAL, 4) AS OPICS_JJSZ
                            ,CASE
                               WHEN SECM.INTCALCRULE <> 'DIS' THEN
                                KBBP.CLEAN_PRICE * TPOS.SETTQTY * 100
                               ELSE
                                KBBP.DIRTY_PRICE * TPOS.SETTQTY * 100
                             END AS JJSZ
                            ,TPOS.TDYMTM AS YZJ
                            ,CASE
                               WHEN SECM.INTCALCRULE <> 'DIS' THEN
                                SETTQTY * 100 * KBBP.CLEAN_PRICE + SETTAVGCOST
                               ELSE
                                SETTQTY * 100 * KBBP.DIRTY_PRICE + SETTAVGCOST
                             END AS WIND_YZJ
                            ,CASE
                               WHEN TPOS.INVTYPE = 'T' THEN
                                0
                               ELSE
                                TPOS.UNAMORTAMT
                             END AS LXTZ
                            ,ROUND(TPOS.CLSGPRICE_8, 2) AS SYL
                            ,ABS(ROUND(TPOS.AVGCOST / TPOS.SETTQTY / 100, 2)) AS JQCB_PER
                            ,ABS(ROUND(TPOS.SETTAVGCOST / TPOS.SETTQTY / 100, 2)) AS JJCB_PER
                            ,(CASE
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                                0
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                                ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                               WHEN TPOS.INVTYPE IN ('T') THEN
                                ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                             END) AS YSLX_PER
                            ,(CASE
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'IM' THEN
                                ABS(ROUND(TPOS.ACCROUTST / TPOS.SETTQTY / 100, 2))
                               WHEN TPOS.INVTYPE IN ('H', 'A') AND TPOS.PRODTYPE = 'SD' THEN
                                0
                               WHEN TPOS.INVTYPE = 'T' THEN
                                0
                             END) AS YJLX_PER
                            ,ABS(TPOS.SETTAVGCOST) AS TYJJ
                            ,TPOS.TDYMTM AS JJFY
                            ,CASE
                               WHEN SECM.INTCALCRULE <> 'DIS' THEN
                                SETTQTY * 100 * KBBP.CLEAN_PRICE + SETTAVGCOST
                               ELSE
                                SETTQTY * 100 * KBBP.DIRTY_PRICE + SETTAVGCOST
                             END AS WIND_JJFY
                            ,ROUND((SECM.MDATE - V_DATADATE) / 365, 2) AS SYNX
                            ,(SELECT MIN(IPAYDATE) - V_DATADATE
                                FROM SECS@OPICS
                               WHERE IPAYDATE > V_DATADATE
                                 AND SECID = TPOS.SECID) AS FXSYTS
                            ,CASE
                               WHEN (SECM.INTCALCRULE = 'NPV' AND SECM.RATECODE IS NOT NULL) OR SECM.INTCALCRULE = 'IAM' THEN
                                (SELECT MIN(IPAYDATE)
                                   FROM SECS@OPICS
                                  WHERE IPAYDATE > V_DATADATE
                                    AND SECID = TPOS.SECID)
                               ELSE
                                NULL
                             END AS CDJR
                            ,ROUND((SECM.MDATE - SECM.VDATE) / 365) AS NX
                            ,SECM.ISSDATE AS FXRQ
                            ,CASE
                               WHEN SECM.INTCALCRULE = 'IAM' THEN
                                '一次性'
                               WHEN SECM.INTCALCRULE = 'DIS' THEN
                                '贴现'
                               WHEN SECM.INTCALCRULE = 'NPV' AND SECM.RATECODE IS NOT NULL THEN
                                '浮动'
                               ELSE
                                '固定'
                             END AS FXL
                            ,CASE
                               WHEN SECM.INTPAYCYCLE = 'A' THEN
                                '一年'
                               WHEN SECM.INTPAYCYCLE = 'S' THEN
                                '半年'
                               WHEN SECM.INTPAYCYCLE = 'Q' THEN
                                '季度'
                               ELSE
                                NULL
                             END AS FXPL
                            ,RATE.DESCR AS FDLX
                            ,SECS.INTRATE_8 AS JZLL
                            ,SECS.SPREADRATE_8 AS FDJD
                            ,(SECS.INTRATE_8 - SECS.SPREADRATE_8) AS LC
                            ,(CASE
                               WHEN D.RIGHT_TYPE = '1' THEN
                                    '发行可赎回'
                               WHEN  D.RIGHT_TYPE = '2' THEN
                                    '投资人可回售券'
                               ELSE
                                     '普通债'
                             END) AS HQZ
                            ,D.ISSUER AS IFNAME
                            ,(CASE
                               WHEN A.BATNO IS NOT NULL THEN
                               --WHEN 'YES' IS NOT NULL THEN
                                '平台债'
                               ELSE
                                '非平台债'
                             END) AS PTZ
                            ,(CASE
                               WHEN E.BOND_NAME IS NOT NULL THEN
                                '是'
                               ELSE
                                '否'
                             END) AS DB
                            ,(CASE
                               WHEN E.DB_TYPE IS NOT NULL THEN
                                 E.DB_TYPE
                               ELSE
                                ' '
                             END) AS DBFS
                            ,(CASE
                               WHEN E.BOND_NAME IS NOT NULL THEN
                                E.BOND_NAME
                               ELSE
                                ' '
                             END) AS DBNAME
                            ,RBAMOUNT
                            ,RPAMOUNT
                            ,RDAMOUNT
                            ,10 AS SBAMOUNT 
                            ,10 AS SLAMOUNT 
                            ,CBAMOUNT 
                            ,ZQAMOUNT 
                            ,YZAMOUNT 
                            ,ABS(TPOS.PRINAMT) + DECODE(RBAMOUNT, NULL, 0, RBAMOUNT) + DECODE(RPAMOUNT, NULL, 0, RPAMOUNT) +
                             DECODE(RDAMOUNT, NULL, 0, RDAMOUNT) AS ME
                             ,'' AS YUQI
                            ,NULL AS BZ
                            
                        FROM SL_TPOS_H@OPICS TPOS
                        LEFT JOIN SECM@OPICS
                          ON SECM.SECID = TPOS.SECID
                        LEFT JOIN SECS@OPICS
                          ON SECS.INTSTRTDTE < V_DATADATE
                         AND SECS.INTENDDTE >= V_DATADATE
                         AND SECS.SECID = TPOS.SECID
                        LEFT JOIN RATE@OPICS
                          ON RATE.RATECODE = SECM.RATECODE
                         AND TPOS.BR = RATE.BR
                        LEFT JOIN COST@OPICS
                          ON COST.COSTCENT = TPOS.COST
                        LEFT JOIN SICO@OPICS
                          ON SECM.SECMSIC = SICO.SIC
                        LEFT JOIN ACTY@OPICS
                          ON SECM.ACCTNGTYPE = ACTY.ACCTNGTYPE
                        LEFT JOIN (SELECT MAX(RUNID) AS RUNID
                                    FROM RESSTAT@OPICS
                                   WHERE RUNID IN (SELECT RUNID
                                                     FROM RESAGGR@OPICS
                                                    WHERE --CREATEDATE = V_DATADATE AND
                                                       AGGR2 = 'SECUR'
                                                      AND AGGR9 IS NOT NULL
                                                    GROUP BY RUNID)) VARMAXRUNID
                          ON 1 = 1
                        LEFT JOIN RESAGGR@OPICS RSA
                          ON RSA.RUNID = VARMAXRUNID.RUNID
                         AND RSA.AGGR9 IS NOT NULL
                         AND RSA.AGGR1 = TPOS.BR
                         AND RSA.AGGR6 = TPOS.COST
                         AND TRIM(RSA.AGGR7) = TRIM(TPOS.SECID)
                         AND TRIM(RSA.AGGR8) = TRIM(TPOS.PORT)
                         AND SUBSTR(TRIM(RSA.AGGR9), 1, 1) = TRIM(TPOS.INVTYPE)
                        LEFT JOIN (SELECT MAX(NODEID) AS NODEID
                                        ,RUNID AS RUNID
                                    FROM RESSTAT@OPICS
                                   WHERE VARX <> 0
                                     AND RUNID = (SELECT MAX(RUNID) AS RUNID
                                                    FROM RESSTAT@OPICS
                                                   WHERE VARX <> 0
                                                     AND RUNID IN (SELECT RUNID
                                                                     FROM RESAGGR@OPICS
                                                                    WHERE --CREATEDATE = V_DATADATE AND
                                                                       AGGR2 = 'SECUR'
                                                                      AND AGGR9 IS NOT NULL
                                                                    GROUP BY RUNID))
                                   GROUP BY RUNID) RTR
                          ON 1 = 1
                        LEFT JOIN RESSTAT@OPICS VARDETAIL
                          ON VARDETAIL.RUNID = VARMAXRUNID.RUNID
                         AND VARDETAIL.AGGRID = RSA.AGGRID
                         AND VARDETAIL.NODEID = RTR.NODEID
                         AND VARDETAIL.VARX <> 0
                        LEFT JOIN IFS_WIND_SECL KBBP
                          ON TRIM(KBBP.SEC_CODE) = TRIM(TPOS.SECID)
                        LEFT JOIN IFS_WIND_BOND_LEVEL B
                          ON TRIM(B.BOND_CODE) = TRIM(TPOS.SECID)
                        LEFT JOIN IFS_WIND_RISK KBB
                          ON TRIM(KBB.BOND_CODE) = TRIM(TPOS.SECID)
                         AND KBB.RDATE = '2021-01-14'
                        LEFT JOIN (SELECT RPDT.BR
                                        ,RPDT.SECID
                                        ,SUM(RPDT.PRINAMT) RBAMOUNT
                                        ,RPDT.INVTYPE
                                        ,RPDT.COST
                                        ,RPDT.PORT
                                    FROM RPDT@OPICS
                                   WHERE (V_DATADATE >= RPDT.VDATE AND
                                         V_DATADATE < RPDT.MDATE)
                                     AND RPDT.PRODTYPE = 'RB'
                                     AND RPDT.VERDATE IS NOT NULL
                                     AND RPDT.REVDATE IS NULL
                                   GROUP BY RPDT.BR
                                           ,RPDT.SECID
                                           ,RPDT.INVTYPE
                                           ,RPDT.COST
                                           ,RPDT.PORT) RB
                          ON RB.SECID = TPOS.SECID
                         AND RB.INVTYPE = TPOS.INVTYPE
                         AND RB.COST = TPOS.COST
                         AND RB.PORT = TPOS.PORT
                         AND RB.BR = TPOS.BR
                        LEFT JOIN (SELECT RPDT.BR
                                        ,RPDT.SECID
                                        ,SUM(RPDT.PRINAMT) RPAMOUNT
                                        ,RPDT.INVTYPE
                                        ,RPDT.COST
                                        ,RPDT.PORT
                                    FROM RPDT@OPICS
                                   WHERE (V_DATADATE >= RPDT.VDATE AND
                                         V_DATADATE < RPDT.MDATE)
                                     AND RPDT.PRODTYPE = 'RP'
                                     AND RPDT.VERDATE IS NOT NULL
                                     AND RPDT.REVDATE IS NULL
                                   GROUP BY RPDT.BR
                                           ,RPDT.SECID
                                           ,RPDT.INVTYPE
                                           ,RPDT.COST
                                           ,RPDT.PORT) RP
                          ON RP.SECID = TPOS.SECID
                         AND RP.INVTYPE = TPOS.INVTYPE
                         AND RP.COST = TPOS.COST
                         AND RP.PORT = TPOS.PORT
                         AND RP.BR = TPOS.BR
                        LEFT JOIN (SELECT RPDT.BR
                                        ,RPDT.SECID
                                        ,SUM(RPDT.PRINAMT) RDAMOUNT
                                        ,RPDT.INVTYPE
                                        ,RPDT.COST
                                        ,RPDT.PORT
                                    FROM RPDT@OPICS
                                   WHERE (V_DATADATE >= RPDT.VDATE AND
                                         V_DATADATE < RPDT.MDATE)
                                     AND RPDT.PRODTYPE = 'GD'
                                     AND RPDT.VERDATE IS NOT NULL
                                     AND RPDT.REVDATE IS NULL
                                   GROUP BY RPDT.BR
                                           ,RPDT.SECID
                                           ,RPDT.INVTYPE
                                           ,RPDT.COST
                                           ,RPDT.PORT) RD
                          ON RD.SECID = TPOS.SECID
                         AND RD.INVTYPE = TPOS.INVTYPE
                         AND RD.COST = TPOS.COST
                         AND RD.PORT = TPOS.PORT
                         AND RD.BR = TPOS.BR
                        --常备借贷便利
                        LEFT JOIN (SELECT RPDT.BR
                                        ,RPDT.SECID
                                        ,SUM(RPDT.PRINAMT) CBAMOUNT
                                        ,RPDT.INVTYPE
                                        ,RPDT.COST
                                        ,RPDT.PORT
                                    FROM RPDT@OPICS
                                   WHERE (V_DATADATE >= RPDT.VDATE AND
                                         V_DATADATE < RPDT.MDATE)
                                     AND RPDT.PRODTYPE = 'CB'
                                     AND RPDT.VERDATE IS NOT NULL
                                     AND RPDT.REVDATE IS NULL
                                   GROUP BY RPDT.BR
                                           ,RPDT.SECID
                                           ,RPDT.INVTYPE
                                           ,RPDT.COST
                                           ,RPDT.PORT) CB
                          ON CB.SECID = TPOS.SECID
                         AND CB.INVTYPE = TPOS.INVTYPE
                         AND CB.COST = TPOS.COST
                         AND CB.PORT = TPOS.PORT
                         AND CB.BR = TPOS.BR 
                        LEFT JOIN (SELECT RPDT.BR
                                        ,RPDT.SECID
                                        ,SUM(RPDT.PRINAMT) ZQAMOUNT
                                        ,RPDT.INVTYPE
                                        ,RPDT.COST
                                        ,RPDT.PORT
                                    FROM RPDT@OPICS
                                   WHERE (V_DATADATE >= RPDT.VDATE AND
                                         V_DATADATE < RPDT.MDATE)
                                     AND RPDT.PRODTYPE = 'ZQ'
                                     AND RPDT.VERDATE IS NOT NULL
                                     AND RPDT.REVDATE IS NULL
                                   GROUP BY RPDT.BR
                                           ,RPDT.SECID
                                           ,RPDT.INVTYPE
                                           ,RPDT.COST
                                           ,RPDT.PORT) ZQ
                          ON ZQ.SECID = TPOS.SECID
                         AND ZQ.INVTYPE = TPOS.INVTYPE
                         AND ZQ.COST = TPOS.COST
                         AND ZQ.PORT = TPOS.PORT
                         AND ZQ.BR = TPOS.BR  
                        LEFT JOIN (SELECT RPDT.BR
                                        ,RPDT.SECID
                                        ,SUM(RPDT.PRINAMT) YZAMOUNT
                                        ,RPDT.INVTYPE
                                        ,RPDT.COST
                                        ,RPDT.PORT
                                    FROM RPDT@OPICS
                                   WHERE (V_DATADATE >= RPDT.VDATE AND
                                         V_DATADATE < RPDT.MDATE)
                                     AND RPDT.PRODTYPE = 'ZD'
                                     AND RPDT.VERDATE IS NOT NULL
                                     AND RPDT.REVDATE IS NULL
                                   GROUP BY RPDT.BR
                                           ,RPDT.SECID
                                           ,RPDT.INVTYPE
                                           ,RPDT.COST
                                           ,RPDT.PORT) ZD
                          ON ZD.SECID = TPOS.SECID
                         AND ZD.INVTYPE = TPOS.INVTYPE
                         AND ZD.COST = TPOS.COST
                         AND ZD.PORT = TPOS.PORT
                         AND ZD.BR = TPOS.BR   
                        LEFT JOIN IFS_WIND_BOND D
                          ON TRIM(D.BOND_CODE) = TRIM(TPOS.SECID)
                         AND TRIM(D.CIRCUPLACE) = '银行间'
                        LEFT JOIN K_CUST_BLACKFORMS@OPICS A
                          ON TRIM(D.MAINCORPID) = TRIM(A.BATNO)                          
                        LEFT JOIN IFS_WIND_BOND_ADD E
                          ON TRIM(E.BOND_CODE) = TRIM(TPOS.SECID)
                        LEFT JOIN SL_FI_DEFAULT@OPICS S 
                          ON TRIM(S.SECID) = TRIM(TPOS.SECID) AND S.STATUS = '1' 
                       WHERE TPOS.SETTQTY > 0
                         AND TRIM(TPOS.BR) = '01'
                         AND TPOS.POSTDATE = V_DATADATE                         
                   )
         GROUP BY INVTYPE)
 ORDER BY INVTYPE;
		
  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';   
  RETURN;
END SL_SP_HBCCMXBH;

