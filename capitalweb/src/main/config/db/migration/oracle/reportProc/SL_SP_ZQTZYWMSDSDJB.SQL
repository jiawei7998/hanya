--DROP TABLE;
DROP TABLE IFS_REPORT_ZQTZYWMSDSDJB;
-- CREATE TABLE
CREATE TABLE IFS_REPORT_ZQTZYWMSDSDJB
-- 债券投资业务免所得税登记表

(
	JJXH			VARCHAR2(50),
	ZHLX			VARCHAR2(50),
	ZQLX			VARCHAR2(50),
	ZQDM			VARCHAR2(50),
	ZQMC			VARCHAR2(50),
	QX				VARCHAR2(50),
	ZQQXR			VARCHAR2(50),
	ZQDQR			VARCHAR2(50),
	PMLV_GDLV		NUMBER(19,4),
	PMLV_FDLV		NUMBER(19,4),	
	FXPL			VARCHAR2(50),
	FXRQ			VARCHAR2(50),
	MRRQ			VARCHAR2(50),
	MRPMJE			NUMBER(19,4),
	MRQJ			NUMBER(19,4),
	MRJJ			NUMBER(19,4),
	MRYJLX			NUMBER(19,4),
	MCRQ			VARCHAR2(50),
	MCPMJE			NUMBER(19,4),	
	MCQJ			NUMBER(19,4),
	MCJJ			NUMBER(19,4),
	MCYJLX			NUMBER(19,4),
	SJSXRQ			VARCHAR2(50),
	SJSXJE			NUMBER(19,4),	
	DNSSLX			NUMBER(19,4),
	MSDSGZLXSR		NUMBER(19,4),
	SJMLXSR			NUMBER(19,4)
);

-- Add comments to the table 
comment on table IFS_REPORT_ZQTZYWMSDSDJB
  is '债券投资业务免所得税登记表';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.JJXH is '交易序号';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.ZHLX is '账户类型';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.ZQLX is '债券类型';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.ZQDM is '债券代码';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.ZQMC is '债券名称';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.QX is '期限';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.ZQQXR is '债券起息日';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.ZQDQR is '债券到期日';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.PMLV_GDLV is '票面利率_固定利率';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.PMLV_FDLV is '票面利率_浮动利率';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.FXPL is '付息频率';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.FXRQ is '付息日期';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MRRQ is '买入日期';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MRPMJE is '买入票面金额';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MRQJ is '买入全价';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MRJJ is '买入净价';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MRYJLX is '买入应计利息';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MCRQ is '卖出日期';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MCPMJE is '卖出票面金额';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MCQJ is '卖出全价';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MCJJ is '卖出净价';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MCYJLX is '卖出应计利息';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.SJSXRQ is '实际收息日期';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.SJSXJE is '实际收息金额';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.DNSSLX is '当年实收利息';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.MSDSGZLXSR is '免所得税国债利息收入';
-- Add comments to the columns 
comment on column IFS_REPORT_ZQTZYWMSDSDJB.SJMLXSR is '实际免利息收入';


CREATE OR REPLACE PROCEDURE SL_SP_ZQTZYWMSDSDJB(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
	/*  **************************************************************************
	*  债券投资业务免所得税登记表
	**************************************************************************  */
	V_DATADATE     		DATE; --当前日期
	V_POSTDATE          DATE; --账务日期
  
            

BEGIN
	--获取账务日期
	BEGIN
		SELECT prevbranprcdate INTO V_POSTDATE FROM brps@opics WHERE br = '01';
	EXCEPTION
		WHEN no_data_found THEN
		retcode := '100';
		retmsg  := 'QUERY BRPS DATA NOT FOUND';
		RETURN;
	END;

	--获取当前日期
	IF datadate IS NULL THEN
		V_DATADATE := V_POSTDATE;
	ELSE
		V_DATADATE := to_date(datadate, 'YYYY-MM-DD');
	END IF;	
	

	
	INSERT INTO IFS_REPORT_ZQTZYWMSDSDJB (JJXH,ZHLX,ZQLX,ZQDM,ZQMC,QX,ZQQXR,ZQDQR,PMLV_GDLV,PMLV_FDLV,FXPL,FXRQ,MRRQ,MRPMJE,MRQJ,MRJJ,MRYJLX,MCRQ,MCPMJE,MCQJ,MCJJ,MCYJLX,SJSXRQ,SJSXJE,DNSSLX,MSDSGZLXSR,SJMLXSR) 
	VALUES ('1','2','3','4','5','6','7','8',9,10,'11','12','13',14,15,16,17,'18',19,20,21,22,'23',24,25,26,27);



  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
END SL_SP_ZQTZYWMSDSDJB;																									