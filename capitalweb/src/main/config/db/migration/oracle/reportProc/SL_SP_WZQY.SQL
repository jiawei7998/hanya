--DROP TABLE;
DROP TABLE IFS_REPORT_WZQY;
-- CREATE TABLE
CREATE TABLE IFS_REPORT_WZQY
-- 外债签约信息报送表

(
	WZRDM			VARCHAR2(50),
	ZWLX			VARCHAR2(50),	
	EJZWLX			VARCHAR2(50),
	HBLX			VARCHAR2(50),
	QXR				VARCHAR2(50),
	ZQRZBGJDM		VARCHAR2(50),
	ZQRJYDGJDM		VARCHAR2(50),	
	ZQRLXDM			VARCHAR2(50),
	ZQRLXEJDM		VARCHAR2(50),	
	BIZ				VARCHAR2(50),
	SWIFTCODE		VARCHAR2(50),
	ZQRDM			VARCHAR2(50),
	ZQRZWM			VARCHAR2(50),
	ZQRYWM			VARCHAR2(50),
	SFBNRFXJQYEJS	VARCHAR2(50),
	SFFDLL			VARCHAR2(50),
	NHLLZ			NUMBER(19,4),	
	CKYWLX			VARCHAR2(50),
	DFYBFGX			VARCHAR2(50),
	YSQX			VARCHAR2(50),	
	BEIZ			VARCHAR2(50)
);


-- Add comments to the table 
comment on table IFS_REPORT_WZQY
  is '外债签约信息报送表';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.WZRDM is '外债人代码';
-- Add comments to the columns 	
comment on column IFS_REPORT_WZQY.ZWLX is '债务类型';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.EJZWLX is '二级债务类型';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.HBLX is '货币类型';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.QXR is '起息日';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.ZQRZBGJDM is '债权人总部所在国家(地区)代码';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.ZQRJYDGJDM is '债权人经营地所在国家(地区)代码';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.ZQRLXDM is '债权人类型代码';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.ZQRLXEJDM is '债权人类型二级代码';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.BIZ is '币种';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.SWIFTCODE is 'SWIFTCODE';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.ZQRDM is '债权人代码';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.ZQRZWM is '债权人中文名';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.ZQRYWM is '债权人英文名';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.SFBNRFXJQYEJS is '是否不纳入跨境融资风险加权余额计算';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.SFFDLL is '是否浮动利率';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.NHLLZ is '年化利率值';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.CKYWLX is '存款业务类型';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.DFYBFGX is '对方与本方的关系';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.YSQX is '原始期限';
-- Add comments to the columns 
comment on column IFS_REPORT_WZQY.BEIZ is '备注';



CREATE OR REPLACE PROCEDURE SL_SP_WZQY(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
	/*  **************************************************************************
	*  债券投资业务免所得税登记表
	**************************************************************************  */
	V_DATADATE     		DATE; --当前日期
	V_POSTDATE          DATE; --账务日期
  
            

BEGIN
	--获取账务日期
	BEGIN
		SELECT prevbranprcdate INTO V_POSTDATE FROM brps@opics WHERE br = '01';
	EXCEPTION
		WHEN no_data_found THEN
		retcode := '100';
		retmsg  := 'QUERY BRPS DATA NOT FOUND';
		RETURN;
	END;

	--获取当前日期
	IF datadate IS NULL THEN
		V_DATADATE := V_POSTDATE;
	ELSE
		V_DATADATE := to_date(datadate, 'YYYY-MM-DD');
	END IF;	
	
				
	
	INSERT INTO IFS_REPORT_WZQY (WZRDM,ZWLX,EJZWLX,HBLX,QXR,ZQRZBGJDM,ZQRJYDGJDM,ZQRLXDM,ZQRLXEJDM,BIZ,SWIFTCODE,ZQRDM,ZQRZWM,ZQRYWM,SFBNRFXJQYEJS,SFFDLL,NHLLZ,CKYWLX,DFYBFGX,YSQX,BEIZ) 
	VALUES ('1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16',17,'18','19','20','21');



  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
END SL_SP_WZQY;																										