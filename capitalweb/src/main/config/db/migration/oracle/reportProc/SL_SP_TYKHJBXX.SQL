--DROP table;
drop table IFS_REPORT_TYKHJBXX;
-- Create table
create table IFS_REPORT_TYKHJBXX
-- 同业客户-基本信息表

(

	KHMC        	VARCHAR2(50),
	KHDM			VARCHAR2(50),
	KHLB			VARCHAR2(50),
	SHDM			VARCHAR2(50),
	GBJDQ			VARCHAR2(50),
	NBPJ			VARCHAR2(50),
	WBPJ			VARCHAR2(50),
	CHAFTY			NUMBER(19,4),
	CUNFTY			NUMBER(19,4),
	ZQHGMRFS		NUMBER(19,4),
	GPZYDK			NUMBER(19,4),
	MRFSZC			NUMBER(19,4),
	MDSZTX			NUMBER(19,4),
	CYZQ			NUMBER(19,4),
	GQTZ			NUMBER(19,4),	
	TYDF			NUMBER(19,4),
	MCHGZC			NUMBER(19,4),
	QTBNYWCDTZ		NUMBER(19,4)	
);



-- Add comments to the table 
comment on table IFS_REPORT_TYKHJBXX
  is '同业客户-基本信息表';
-- Add comments to the columns 
comment on column IFS_REPORT_TYKHJBXX.KHMC is '客户名称';
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.KHDM is '客户代码';
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.KHLB is '客户类别';
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.SHDM is '统一信用社会代码/组织机构代码证代码';	
-- Add comments to the columns 
comment on column IFS_REPORT_TYKHJBXX.GBJDQ is '国别及地区';
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.NBPJ is '内部评级';
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.WBPJ is '外部评级';
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.CHAFTY is '拆放同业（元）';
-- Add comments to the columns 		
comment on column IFS_REPORT_TYKHJBXX.CUNFTY is '存放同业（元）';
-- Add comments to the columns 		
comment on column IFS_REPORT_TYKHJBXX.ZQHGMRFS is '债券回购（元）-买入返售';	
-- Add comments to the columns 
comment on column IFS_REPORT_TYKHJBXX.GPZYDK is '股票质押贷款（元）';
-- Add comments to the columns 		
comment on column IFS_REPORT_TYKHJBXX.MRFSZC is '买入返售资产（元）';	
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.MDSZTX is '买断式转贴现（元）';
-- Add comments to the columns 		
comment on column IFS_REPORT_TYKHJBXX.CYZQ is '持有债券（元）';	
-- Add comments to the columns 
comment on column IFS_REPORT_TYKHJBXX.GQTZ is '股权投资（元）';	
-- Add comments to the columns 
comment on column IFS_REPORT_TYKHJBXX.TYDF is '同业代付（元）';	
-- Add comments to the columns 
comment on column IFS_REPORT_TYKHJBXX.MCHGZC is '卖出回购资产（元）';	
-- Add comments to the columns 	
comment on column IFS_REPORT_TYKHJBXX.QTBNYWCDTZ is '其他表内业务-存单投资';

CREATE OR REPLACE PROCEDURE SL_SP_TYKHJBXX(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
	/*  **************************************************************************
	*  同业客户-基本信息表
	**************************************************************************  */
	V_DATADATE     		DATE; --当前日期
	V_POSTDATE          DATE; --账务日期
  
            

BEGIN
	--获取账务日期
	BEGIN
		SELECT prevbranprcdate INTO V_POSTDATE FROM brps@opics WHERE br = '01';
	EXCEPTION
		WHEN no_data_found THEN
		retcode := '100';
		retmsg  := 'QUERY BRPS DATA NOT FOUND';
		RETURN;
	END;

	--获取当前日期
	IF datadate IS NULL THEN
		V_DATADATE := V_POSTDATE;
	ELSE
		V_DATADATE := to_date(datadate, 'YYYY-MM-DD');
	END IF;	
	
	
	
	INSERT INTO IFS_REPORT_TYKHJBXX (KHMC,KHDM,KHLB,SHDM,GBJDQ,NBPJ,WBPJ,CHAFTY,CUNFTY,ZQHGMRFS,GPZYDK,MRFSZC,MDSZTX,CYZQ,GQTZ,TYDF,MCHGZC,QTBNYWCDTZ) 
	VALUES ('1','2','3','4','5','6','7',8,9,10,11,12,13,14,15,16,17,18);



  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
END SL_SP_TYKHJBXX;