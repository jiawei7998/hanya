--DROP table;
drop table IFS_REPORT_JRSCBYWGL_ZQTZZHAFX;
-- Create table
create table IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
-- 债券投资账户结构表按风险账户

(
  ZQTYPE        	varchar2(50),
  YSQX        		varchar2(50),
  JYZH_YE		 	number(19,4),
  JYZH_ZB		 	number(19,4),
  YHZH_YE		 	number(19,4),
  YHZH_ZB		 	number(19,4),
  HJ_YE		 		number(19,4),
  HJ_ZB		 		number(19,4)
);


-- Add comments to the table 
comment on table IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
  is '债券投资账户结构表按风险账户';
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.ZQTYPE is '债券类型';
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YSQX is '原始期限';  
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.JYZH_YE is '交易账户_余额';
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.JYZH_ZB is '交易账户_占比';
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YHZH_YE is '银行账户_余额';
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YHZH_ZB is '银行账户_占比';
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.HJ_YE is '合计_余额';	
-- Add comments to the columns 
comment on column IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.HJ_ZB is '合计_占比';	

--初始化数据
--初始化
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('国债',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('地方政府债',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('政策性金融债',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('非银行金融机构债',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('商业银行金融债','3个月以内（含）',NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('商业银行金融债','3个月以上',NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('商业银行次级债',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('非金融债',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('铁道债',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('同业存单','3个月以内（含）',NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('同业存单','3个月以上',NULL,NULL,NULL,NULL,NULL,NULL);
insert into IFS_REPORT_JRSCBYWGL_ZQTZZHAFX (ZQTYPE,YSQX,JYZH_YE,JYZH_ZB,YHZH_YE,YHZH_ZB,HJ_YE,HJ_ZB)
values ('合计',NULL,NULL,NULL,NULL,NULL,NULL,NULL);




CREATE OR REPLACE PROCEDURE sl_sp_jrscbywgl_zqtzzhafx(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
	/*  **************************************************************************
	*   金融市场部业务管理报表 债券投资账户结构表按风险账户
	**************************************************************************  */

	V_JYZH_YE		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.JYZH_YE%TYPE;--交易账户_余额	
    V_YHZH_YE		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YHZH_YE%TYPE;--银行账户_余额	
    V_HJ_YE			IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.HJ_YE%TYPE;--合计_余额	
	
	V_JYZH_YE_HJ	IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.JYZH_YE%TYPE;--交易账户_余额	合计
    V_YHZH_YE_HJ	IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YHZH_YE%TYPE;--银行账户_余额	合计	
    V_HJ_YE_HJ		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.HJ_YE%TYPE;--合计_余额	合计

	--计算比例
	H_ZQTYPE 		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.ZQTYPE%TYPE; 	   
    H_YSQX   		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YSQX%TYPE;   	   
    H_JYZH_YE		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.JYZH_YE%TYPE;	
    H_JYZH_ZB		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.JYZH_ZB%TYPE;	
    H_YHZH_YE		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YHZH_YE%TYPE;	
    H_YHZH_ZB		IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.YHZH_ZB%TYPE;		
    H_HJ_YE			IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.HJ_YE%TYPE;		
    H_HJ_ZB			IFS_REPORT_JRSCBYWGL_ZQTZZHAFX.HJ_ZB%TYPE;	

	YE				NUMBER(19, 4); 
	COST			VARCHAR2(10);
	PRODTYPE		VARCHAR2(10);	
	ACCTNGTYPE		VARCHAR2(10);
	SECMSIC			VARCHAR2(10);
	TREN			NUMBER(19, 4); 

	V_DATADATE     	DATE; --当前日期
	V_POSTDATE      DATE; --账务日期
	

	-- 获取数据
	CURSOR ZQTZZHAFX_CUR IS	
		SELECT CASE WHEN TPOS.INVTYPE = 'T' THEN ABS(TPOS.SETTAVGCOST) + TDYMTM
					WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE = 'IM' THEN ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT + TPOS.TDYMTM
					WHEN TPOS.INVTYPE = 'A' AND TPOS.PRODTYPE != 'IM'THEN ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT + TPOS.TDYMTM
					WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE = 'IM' THEN ABS(TPOS.PRINAMT) + ABS(TPOS.ACCROUTST) + TPOS.UNAMORTAMT
					WHEN TPOS.INVTYPE = 'H' AND TPOS.PRODTYPE != 'IM'THEN ABS(TPOS.PRINAMT) + TPOS.UNAMORTAMT END AS YE
		,CASE WHEN COST LIKE '82%' THEN '1' WHEN COST LIKE '83%' THEN '2' END AS COST
		,TRIM(TPOS.PRODTYPE)
		,TRIM(M.ACCTNGTYPE)
		,TRIM(M.SECMSIC)
		,(M.MDATE-M.ISSDATE)
		FROM (
			SELECT T.* ,V_POSTDATE POSTDATE,V_POSTDATE INPUTTIME  FROM TPOS@OPICS T
			UNION ALL
			SELECT * FROM SL_TPOS_H@OPICS
		) TPOS
		LEFT JOIN  SECM@OPICS M ON  TRIM(TPOS.SECID) = TRIM(M.SECID)
		WHERE  (TRIM(TPOS.COST) LIKE '82%' OR TRIM(TPOS.COST) LIKE '83%') 
			AND TPOS.INVTYPE != 'I' 
			AND TPOS.QTY >0
			AND TPOS.POSTDATE = V_DATADATE AND M.MDATE > V_DATADATE AND M.VDATE <= V_DATADATE; 
	
	CURSOR TABLE_CUR  IS
		SELECT ZQTYPE,YSQX,JYZH_YE,YHZH_YE,HJ_YE FROM IFS_REPORT_JRSCBYWGL_ZQTZZHAFX;

		
	
BEGIN
	--获取账务日期
	BEGIN
		SELECT prevbranprcdate INTO V_POSTDATE FROM brps@opics WHERE br = '01';
	EXCEPTION
		WHEN no_data_found THEN
		retcode := '100';
		retmsg  := 'QUERY BRPS DATA NOT FOUND';
		RETURN;
	END;

	--获取当前日期
	IF datadate IS NULL THEN
		V_DATADATE := V_POSTDATE;
	ELSE
		V_DATADATE := to_date(datadate, 'YYYY-MM-DD');
	END IF;	
	
	/*  **************************************************************************
	*    国债
	**************************************************************************  */
	V_JYZH_YE_HJ	:= 0;
	V_YHZH_YE_HJ	:= 0;
	V_HJ_YE_HJ		:= 0;
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND ACCTNGTYPE = 'DOMGOVSE'  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '国债';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    地方政府债
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND ACCTNGTYPE = 'DOMLOGOVSE'  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '地方政府债';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    政策性金融债
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND ACCTNGTYPE = 'DOMPOLICSE'  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '政策性金融债';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    非银行金融机构债
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND ACCTNGTYPE = 'DOMNBFINSE'  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '非银行金融机构债';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    商业银行金融债 3个月以内（含）
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND ACCTNGTYPE = 'DOMFINSE' AND TREN <= 92  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '商业银行金融债' AND YSQX='3个月以内（含）';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    商业银行金融债 3个月以上
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND ACCTNGTYPE = 'DOMFINSE' AND TREN > 92  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '商业银行金融债' AND YSQX='3个月以上';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	
	/*  **************************************************************************
	*    商业银行次级债
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND  (SECMSIC = 'SE-SYYHCJZ' OR SECMSIC = 'SE-ZCYHCJZ')  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '商业银行次级债';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    非金融债
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND ACCTNGTYPE = 'DOMNOFINSE'  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '非金融债';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    铁道债
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF PRODTYPE != 'CD' AND SECMSIC = 'SE-TDZ'  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '铁道债';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    同业存单 3个月以内（含）
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF  PRODTYPE = 'CD' AND TREN <= 92  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '同业存单'  AND YSQX='3个月以内（含）';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	/*  **************************************************************************
	*    同业存单 3个月以上
	**************************************************************************  */
	
	V_JYZH_YE		:= 0;
	V_YHZH_YE		:= 0;
	V_HJ_YE			:= 0;

	
	BEGIN
	OPEN ZQTZZHAFX_CUR;
	FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		WHILE (ZQTZZHAFX_CUR%FOUND) LOOP
		BEGIN
			IF  PRODTYPE = 'CD' AND TREN > 92  THEN 								
				IF COST = '1' THEN 
					V_JYZH_YE := V_JYZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;				
				ELSIF COST = '2'  THEN 
					V_YHZH_YE := V_YHZH_YE + YE;
					V_HJ_YE := V_HJ_YE + YE;									
				END IF;				
			END IF;	
		FETCH ZQTZZHAFX_CUR INTO YE,COST,PRODTYPE,ACCTNGTYPE,SECMSIC,TREN;
		END;
		END LOOP;
	CLOSE ZQTZZHAFX_CUR;
	END;
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE/100000000,		
			YHZH_YE = V_YHZH_YE/100000000,		
			HJ_YE = V_HJ_YE/100000000
	WHERE ZQTYPE = '同业存单'  AND YSQX='3个月以上';
	
	
	V_JYZH_YE_HJ := V_JYZH_YE_HJ + V_JYZH_YE;	
	V_YHZH_YE_HJ := V_YHZH_YE_HJ + V_YHZH_YE;	
	V_HJ_YE_HJ := V_HJ_YE_HJ + V_HJ_YE;
	
	
	/*  **************************************************************************
	*    合计
	**************************************************************************  */
	
	UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
		SET JYZH_YE = V_JYZH_YE_HJ/100000000,		
			YHZH_YE = V_YHZH_YE_HJ/100000000,		
			HJ_YE = V_HJ_YE_HJ/100000000
	WHERE ZQTYPE = '合计';
	
	
	/*  **************************************************************************
	*    计算占比修改
	**************************************************************************  */
			
	H_ZQTYPE	    := '';
	H_YSQX      	:= '';
	H_JYZH_YE		:= 0;	
	H_JYZH_ZB		:= 0;	
	H_YHZH_YE		:= 0;		
	H_YHZH_ZB		:= 0;
	H_HJ_YE			:= 0;		
	H_HJ_ZB			:= 0;
	
	
	BEGIN
	OPEN TABLE_CUR;
	FETCH TABLE_CUR INTO H_ZQTYPE,H_YSQX,H_JYZH_YE,H_YHZH_YE,H_HJ_YE;
		WHILE (TABLE_CUR%FOUND) LOOP
		BEGIN
			H_JYZH_ZB := H_JYZH_YE/V_HJ_YE_HJ/100000000*100;
			H_YHZH_ZB := H_YHZH_YE/V_HJ_YE_HJ/100000000*100;
			H_HJ_ZB := H_HJ_YE/V_HJ_YE_HJ/100000000*100;
			
			IF H_YSQX IS NULL THEN 
				UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
					SET JYZH_ZB = H_JYZH_ZB,		
						YHZH_ZB = H_YHZH_ZB,		
						HJ_ZB = H_HJ_ZB
				WHERE ZQTYPE = H_ZQTYPE AND YSQX = H_YSQX;
			ELSE
				UPDATE IFS_REPORT_JRSCBYWGL_ZQTZZHAFX
					SET JYZH_ZB = H_JYZH_ZB,		
						YHZH_ZB = H_YHZH_ZB,		
						HJ_ZB = H_HJ_ZB
				WHERE ZQTYPE = H_ZQTYPE;
			END IF;
	
		FETCH TABLE_CUR INTO H_ZQTYPE,H_YSQX,H_JYZH_YE,H_YHZH_YE,H_HJ_YE;
		END;
		END LOOP;
	CLOSE TABLE_CUR;
	END;
	
	
	retcode := '999';
	retmsg  := 'PROCESSED SUCCESSFUL!';
	RETURN;
END sl_sp_jrscbywgl_zqtzzhafx;	

	