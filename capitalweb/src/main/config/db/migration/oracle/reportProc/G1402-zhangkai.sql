--DROP table;
drop table IFS_REPORT_G1402;
-- Create table
create table IFS_REPORT_G1402
(
 cust_type           varchar2(105),  --客户类型
 cust_name           varchar2(120), --客户名称
 cust_orgcode        varchar2(180),  --客户代码(组织机构代码)
 risk_exp_sum        number(25,4),  --风险暴露总和-合计
 risk_exp            number(19,4),  --风险暴露总和-其中:不可豁免风险暴露(三家政策性银行是0)
 net_cap_sum		 number(25,4),  --占一级资本净额比例-合计
 net_cap			 number(19,4),  --占一级资本净额比例-其中不可豁免风险暴露
 gen_risk_sum        number(25,4),  --一般风险暴露
 spec_risk_sum       number(25,4),  --特定风险暴露
 trade_risk_exp      number(19,4),  --交易账簿风险暴露
 cust_risk_sum       number(25,4),  --交易对手信用风险暴露
 qz_risk_exp         number(19,4),  --潜在风险暴露
 qt_risk_exp         number(19,4),  --其他风险暴露
 rev_risk_exp        number(19,4),  --风险缓释转出的风险暴露（转入为负数）
 none_rev_risk       number(19,4),  --不考虑风险缓释作用的风险暴露-风险暴露总和
 none_rev_risk_per   number(19,4)   --不考虑风险缓释作用的风险暴露-占一级资本净额比例
);
-- Add comments to the table 
comment on table IFS_REPORT_G1402
  is 'G1402大额风险暴露统计报表[G14_II不限制,G14_V同业单一客户100,G14_VI集团客户30]';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.cust_type is '客户类型';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.cust_name is '客户名称';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.cust_orgcode is '客户代码(组织机构代码)';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.risk_exp_sum is '风险暴露总和-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.risk_exp is '风险暴露总和-其中:不可豁免风险暴露(三家政策性银行是0)';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.net_cap_sum is '占一级资本净额比例-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.net_cap is '占一级资本净额比例-其中不可豁免风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.gen_risk_sum is '一般风险暴露 (商业银行大额风险暴露管理办法)-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.spec_risk_sum is '特定风险暴露-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.trade_risk_exp is '交易账簿风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.cust_risk_sum is '交易对手信用风险暴露-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.qz_risk_exp is '潜在风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.qt_risk_exp is '其他风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.rev_risk_exp is '风险缓释转出的风险暴露（转入为负数）';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.none_rev_risk is '不考虑风险缓释作用的风险暴露总和';
-- Add comments to the columns 
comment on column IFS_REPORT_G1402.none_rev_risk_per is '不考虑风险缓释作用的风险暴露-占一级资本净额比例);';

--初始化
--无



--执行存储过程
CREATE OR REPLACE PROCEDURE sl_sp_G1402(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
  /*  **************************************************************************
  *1.根据客户信息查询基础交易数据
  *2.计算合计部分
  **************************************************************************  */
    
  v_cust_type			IFS_REPORT_G1402.cust_type%TYPE;
  v_cust_name			IFS_REPORT_G1402.cust_name%TYPE;
  v_cust_orgcode		IFS_REPORT_G1402.cust_orgcode%TYPE;
  v_risk_exp_sum		IFS_REPORT_G1402.risk_exp_sum%TYPE;
  v_gen_risk_sum		IFS_REPORT_G1402.gen_risk_sum%TYPE;
  v_trade_risk_exp      IFS_REPORT_G1402.trade_risk_exp%TYPE;
  v_rev_risk_exp        IFS_REPORT_G1402.rev_risk_exp%TYPE;
  v_none_rev_risk       IFS_REPORT_G1402.none_rev_risk_per%TYPE;
  
  
  v_cno 	    CHAR(11);
  v_datadate  DATE;
  v_count     INT;

  
BEGIN
  
  --数据日期
  IF datadate IS NULL THEN
    BEGIN
      SELECT prevbranprcdate INTO v_datadate FROM brps@opics WHERE br = '01';
    EXCEPTION
      WHEN no_data_found THEN
        retcode := '100';
        retmsg  := 'QUERY BRPS DATA NOT FOUND';
        RETURN;
    END;
  ELSE
    v_datadate := to_date(datadate, 'YYYY-MM-DD');
  END IF;

  BEGIN
	  
	  execute immediate 'TRUNCATE TABLE IFS_REPORT_G1402';

    BEGIN    
    INSERT INTO IFS_REPORT_G1402
      (
      cust_type,
      cust_name,
      cust_orgcode,
      risk_exp_sum,
      risk_exp,
      net_cap_sum,
      net_cap,
      gen_risk_sum,
      spec_risk_sum,
      trade_risk_exp,
      cust_risk_sum,
      qz_risk_exp,
      qt_risk_exp,
      rev_risk_exp,
      none_rev_risk,
      none_rev_risk_per
      )
      SELECT
      cust_type,
      cust_name,
      cust_orgcode,
      risk_exp_sum,
      risk_exp,
      net_cap_sum,
      net_cap,
      gen_risk_sum,
      spec_risk_sum,
      trade_risk_exp,
      cust_risk_sum,
      qz_risk_exp,
      qt_risk_exp,
      rev_risk_exp,
      none_rev_risk,
      0.00
      FROM IFS_REPORT_G1405
      WHERE cust_type <> '列表客户合计' and risk_exp_sum > 0
      union all
      SELECT
      cust_type,
      cust_name,
      cust_orgcode,
      risk_exp_sum,
      risk_exp,
      net_cap_sum,
      net_cap,
      gen_risk_sum,
      spec_risk_sum,
      trade_risk_exp,
      cust_risk_sum,
      qz_risk_exp,
      qt_risk_exp,
      rev_risk_exp,
      none_rev_risk,
      0.00
      FROM IFS_REPORT_G1406
      WHERE cust_type <> '列表客户合计' and risk_exp_sum > 0;
    END;

    BEGIN
      SELECT COUNT(0) INTO v_count FROM IFS_REPORT_G1402;
      IF v_count <= 20 THEN

        INSERT INTO IFS_REPORT_G1402
          (
          cust_type,
          cust_name,
          cust_orgcode,
          risk_exp_sum,
          risk_exp,
          net_cap_sum,
          net_cap,
          gen_risk_sum,
          spec_risk_sum,
          trade_risk_exp,
          cust_risk_sum,
          qz_risk_exp,
          qt_risk_exp,
          rev_risk_exp,
          none_rev_risk,
          none_rev_risk_per
          )
          SELECT
          cust_type,
          cust_name,
          cust_orgcode,
          risk_exp_sum,
          risk_exp,
          net_cap_sum,
          net_cap,
          gen_risk_sum,
          spec_risk_sum,
          trade_risk_exp,
          cust_risk_sum,
          qz_risk_exp,
          qt_risk_exp,
          rev_risk_exp,
          none_rev_risk,
          0.00
          FROM IFS_REPORT_G1405
          WHERE cust_type <> '列表客户合计' and risk_exp_sum = 0;

      END IF;
    END;

    BEGIN
      INSERT INTO IFS_REPORT_G1402
        (
        cust_type,
        cust_name,
        cust_orgcode,
        risk_exp_sum,
        risk_exp,
        net_cap_sum,
        net_cap,
        gen_risk_sum,
        spec_risk_sum,
        trade_risk_exp,
        cust_risk_sum,
        qz_risk_exp,
        qt_risk_exp,
        rev_risk_exp,
        none_rev_risk,
        none_rev_risk_per
        )
        SELECT 
        '合计',
        '',
        '',
        sum(risk_exp_sum),
        sum(risk_exp),
        sum(net_cap_sum),
        sum(net_cap),
        sum(gen_risk_sum),
        sum(spec_risk_sum),
        sum(trade_risk_exp),
        sum(cust_risk_sum),
        sum(qz_risk_exp),
        sum(qt_risk_exp),
        sum(rev_risk_exp),
        sum(none_rev_risk),
        sum(none_rev_risk_per)
        FROM IFS_REPORT_G1402;
    END;
  END;
  
  COMMIT;
  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
END sl_sp_G1402;  