--DROP table;
drop table IFS_REPORT_XZQTZ;
-- Create table
create table IFS_REPORT_XZQTZ
-- 新债券投资情况表(含逾期)

(
  QZ        		varchar2(150),
  FXQZ        		varchar2(50),
  JYZH_YE		 	number(19,4),
  JYZH_YSLX		 	number(19,4),
  YHZH_YE		 	number(19,4),
  YHZH_YSLX		 	number(19,4)
);

-- Add comments to the table 
comment on table IFS_REPORT_XZQTZ
  is '新债券投资情况表(含逾期)';
-- Add comments to the columns 
comment on column IFS_REPORT_XZQTZ.QZ is '券种';       
-- Add comments to the columns 
comment on column IFS_REPORT_XZQTZ.FXQZ is '风险权重';        
-- Add comments to the columns 
comment on column IFS_REPORT_XZQTZ.JYZH_YE is '交易账户_余额';		
-- Add comments to the columns 
comment on column IFS_REPORT_XZQTZ.JYZH_YSLX is '交易账户_应收利息';		
-- Add comments to the columns 
comment on column IFS_REPORT_XZQTZ.YHZH_YE is '银行账户_余额';		 	
-- Add comments to the columns 
comment on column IFS_REPORT_XZQTZ.YHZH_YSLX is '银行账户_应收利息';	


--初始化数据
--初始化
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（一）国债','0%',NULL,NULL,NULL,NULL);	 		
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（二）地方政府债','20%',NULL,NULL,NULL,NULL);	 			
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（三）央票','0%',NULL,NULL,NULL,NULL);	 			
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_政策性银行债（不含次级债）','0%',NULL,NULL,NULL,NULL);	 		
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_商业银行债_原始期限3个月以内（不含次级债）','20%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_商业银行债_原始期限3个月以上（不含次级债）','25%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_商业银行发行的同业存单_原始期限3个月以内（不含次级债）','20%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_商业银行发行的同业存单_原始期限3个月以上（不含次级债）','25%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_其他金融机构发行的同业存单_原始期限3个月以内（不含次级债）','20%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_其他金融机构发行的同业存单_原始期限3个月以上（不含次级债）','25%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_次级债、二级资本债_政策性银行发行','100%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_次级债、二级资本债_商业银行发行','100%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_次级债、二级资本债_其他金融机构发行的债券','100%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_权益工具（包括优先股、永续债）待确认_政策性银行发行','250%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_权益工具（包括优先股、永续债）待确认_商业银行发行','250%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_权益工具（包括优先股、永续债）待确认_其他金融机构发行的债券','250%',NULL,NULL,NULL,NULL);	 	
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_我国中央政府投资的金融资产管理公司发行的债券','100%',NULL,NULL,NULL,NULL);	 		
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_我国中央政府投资的金融资产管理公司为收购国有银行不良贷款而定向发行的债券','0%',NULL,NULL,NULL,NULL);	 		
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（四）金融债_其他金融机构发行的债券','100%',NULL,NULL,NULL,NULL);	 		
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（五）公用企业债 政府支持机构债券','20%',NULL,NULL,NULL,NULL);	 			
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（六）一般企业发行的权益工具','1250%',NULL,NULL,NULL,NULL);	 			
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('（七）其他企业债 除上面统计之外企业发行的债券','100%',NULL,NULL,NULL,NULL);	 			
insert into IFS_REPORT_XZQTZ (QZ,FXQZ,JYZH_YE,JYZH_YSLX,YHZH_YE,YHZH_YSLX)
values ('合计',NULL,NULL,NULL,NULL,NULL);





CREATE OR REPLACE PROCEDURE sl_sp_xzqtz(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
 /*  **************************************************************************
  *   新债券投资情况表(含逾期)  
  **************************************************************************  */
  V_JYZH_YE      IFS_REPORT_XZQTZ.JYZH_YE%TYPE; --交易账户余额
  V_JYZH_YSLX    IFS_REPORT_XZQTZ.JYZH_YSLX%TYPE; --交易账户应收利息
  V_YHZH_YE      IFS_REPORT_XZQTZ.YHZH_YE%TYPE; --银行账户余额
  V_YHZH_YSLX    IFS_REPORT_XZQTZ.YHZH_YSLX%TYPE; --银行账户应收利息
  
  V_TJ_JYZH_YE      IFS_REPORT_XZQTZ.JYZH_YE%TYPE; --交易账户余额 统计
  V_TJ_JYZH_YSLX    IFS_REPORT_XZQTZ.JYZH_YSLX%TYPE; --交易账户应收利息 统计
  V_TJ_YHZH_YE      IFS_REPORT_XZQTZ.YHZH_YE%TYPE; --银行账户余额 统计
  V_TJ_YHZH_YSLX    IFS_REPORT_XZQTZ.YHZH_YSLX%TYPE; --银行账户应收利息 统计


  V_DATADATE     DATE; --当前日期
  V_ACCTNGTYPE   VARCHAR2(20);  --客户 会计类型
  V_COST         NUMBER(5, 1);  --成本中心
  V_SECMSIC      VARCHAR2(20);  --债券 会计类型
  V_BM           NUMBER(5, 1);  --债券期限
  V_SD           VARCHAR2(100); --
  V_SETTQTY      NUMBER(19, 4); --
  V_YE           NUMBER(19, 4); --余额
  V_LX           NUMBER(19, 4); --利息收益   
  
  -- 获取数据
  CURSOR XZQTZ_CUR IS
       SELECT ACCTNGTYPE,COST,SECMSIC,BM,SD,SETTQTY,ABS(YE) YE,ABS(LX) LX FROM     (
          SELECT PRINAMT AS MZ
                ,SETTAVGCOST
                ,TDYMTM
                ,UNAMORTAMT
                ,TRIM(ACCTNGTYPE) ACCTNGTYPE
                ,SETTQTY
                ,TRIM(SECMSIC) SECMSIC
                ,SI.SD AS SD
                ,MONTHS_BETWEEN(SE.MDATE, SE.VDATE) AS BM
                ,INVTYPE
                ,KTP.SECID AS SECID
                ,KTP.ACCROUTST
                ,SE.PRODTYPE
                ,KTP.PRODTYPE
                ,CASE WHEN COST LIKE '82%' THEN '1' WHEN COST LIKE '83%' THEN '2' END AS COST
                ,CASE  WHEN INVTYPE = 'T' THEN ABS(SETTAVGCOST) + TDYMTM
                       WHEN INVTYPE = 'A' AND KTP.PRODTYPE = 'IM' THEN ABS(PRINAMT) + ABS(ACCROUTST) + UNAMORTAMT + TDYMTM
                       WHEN INVTYPE = 'A' AND KTP.PRODTYPE != 'IM'THEN ABS(PRINAMT) + UNAMORTAMT + TDYMTM
                       WHEN INVTYPE = 'H' AND KTP.PRODTYPE = 'IM' THEN ABS(PRINAMT) + ABS(ACCROUTST) + UNAMORTAMT
                       WHEN INVTYPE = 'H' AND KTP.PRODTYPE != 'IM'THEN ABS(PRINAMT) + UNAMORTAMT END AS YE
                ,CASE  WHEN INVTYPE = 'T' OR ((INVTYPE = 'A' OR INVTYPE = 'H') AND KTP.PRODTYPE = 'SD') THEN KTP.ACCROUTST ELSE 0 END AS LX
          FROM SL_TPOS_H@opics KTP ,SECM@opics SE ,SICO@opics SI
          WHERE TRIM(KTP.SECID) = TRIM(SE.SECID)
                AND TRIM(SE.SECMSIC) = TRIM(SI.SIC)
                AND (TRIM(KTP.COST) LIKE '82%' OR TRIM(KTP.COST) LIKE '83%')
                AND POSTDATE = V_DATADATE
        ) WHERE  SETTQTY != 0;               

BEGIN
  --数据日期
  IF datadate IS NULL THEN
    BEGIN
      SELECT prevbranprcdate INTO V_DATADATE FROM brps@opics WHERE br = '01';
    EXCEPTION
      WHEN no_data_found THEN
        retcode := '100';
        retmsg  := 'QUERY BRPS DATA NOT FOUND';
        RETURN;
    END;
  ELSE
    V_DATADATE := to_date(datadate, 'YYYY-MM-DD');
  END IF;

  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  
  /*  **************************************************************************
  *    （一）国债 
  **************************************************************************  */
  V_TJ_JYZH_YE     := 0;
  V_TJ_JYZH_YSLX   := 0;
  V_TJ_YHZH_YE     := 0;
  V_TJ_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_ACCTNGTYPE='DOMGOVSE' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（一）国债';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
   
   
   /*  **************************************************************************
  *    （二）地方政府债
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_ACCTNGTYPE='DOMLOGOVSE' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（二）地方政府债';

   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;

    /*  **************************************************************************
  *    （三）央票
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_ACCTNGTYPE='DOMCENBKSE' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（三）央票';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;

  /*  **************************************************************************
    *  （四）金融债_政策性银行债（不含次级债）
    **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_ACCTNGTYPE='DOMPOLICSE'AND V_SECMSIC != 'SE-ZCYHCJZ' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_政策性银行债（不含次级债）';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
    
    /*  **************************************************************************
  *    （四）金融债_商业银行债_原始期限3个月以内（不含次级债）
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  (V_ACCTNGTYPE = 'DOMFINSE' AND V_SECMSIC != 'SE-SYYHCJZ') AND V_BM <= 3 THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_商业银行债_原始期限3个月以内（不含次级债）';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_商业银行债_原始期限3个月以上（不含次级债）
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  (V_ACCTNGTYPE = 'DOMFINSE' AND V_SECMSIC != 'SE-SYYHCJZ') AND V_BM > 3 THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_商业银行债_原始期限3个月以上（不含次级债）';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_商业银行发行的同业存单_原始期限3个月以内（不含次级债）
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  (V_ACCTNGTYPE = 'DOMFINSE'AND V_SECMSIC = 'SE-TYCD' ) AND V_BM <= 3 THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_商业银行发行的同业存单_原始期限3个月以内（不含次级债）';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_商业银行发行的同业存单_原始期限3个月以上（不含次级债）
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  (V_ACCTNGTYPE = 'DOMFINSE' AND V_SECMSIC = 'SE-TYCD' ) AND V_BM > 3 THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_商业银行发行的同业存单_原始期限3个月以上（不含次级债）';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_其他金融机构发行的同业存单_原始期限3个月以内（不含次级债）
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  (V_ACCTNGTYPE = 'DOMNBFINSE' AND V_SECMSIC = 'SE-TYCD' ) AND V_BM <= 3 THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_其他金融机构发行的同业存单_原始期限3个月以内（不含次级债）';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_其他金融机构发行的同业存单_原始期限3个月以上（不含次级债）
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  (V_ACCTNGTYPE = 'DOMNBFINSE' AND V_SECMSIC = 'SE-TYCD' ) AND V_BM > 3 THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_其他金融机构发行的同业存单_原始期限3个月以上（不含次级债）';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_次级债、二级资本债_政策性银行发行
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_SECMSIC = 'SE-ZCYHCJZ' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_次级债、二级资本债_政策性银行发行';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_次级债、二级资本债_商业银行发行
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_SECMSIC = 'SE-SYYHCJZ' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_次级债、二级资本债_商业银行发行';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_次级债、二级资本债_其他金融机构发行的债券
  **************************************************************************  */
  
  /*  **************************************************************************
  *    （四）金融债_权益工具（包括优先股、永续债）待确认_政策性银行发行
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_SECMSIC = 'SE-ZCYHYXZ' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_权益工具（包括优先股、永续债）待确认_政策性银行发行';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_权益工具（包括优先股、永续债）待确认_商业银行发行
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_SECMSIC = 'SE-SYYHYXZ' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_权益工具（包括优先股、永续债）待确认_商业银行发行';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （四）金融债_权益工具（包括优先股、永续债）待确认_其他金融机构发行的债券
  **************************************************************************  */
  
  /*  **************************************************************************
  *    （四）金融债_我国中央政府投资的金融资产管理公司发行的债券
  **************************************************************************  */
  
  /*  **************************************************************************
  *    （四）金融债_我国中央政府投资的金融资产管理公司为收购国有银行不良贷款而定向发行的债券
  **************************************************************************  */
  
  /*  **************************************************************************
  *    （四）金融债_其他金融机构发行的债券
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_ACCTNGTYPE  = 'DOMNBFINSE' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（四）金融债_其他金融机构发行的债券';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （五）公用企业债 政府支持机构债券
  **************************************************************************  */
  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
          IF  V_SD LIKE '%公共部门实体（收入来源于中央财政）%' OR V_SD  LIKE '%中央政府投资的公用事业企业%' THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
          END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '（五）公用企业债 政府支持机构债券';
   
   V_TJ_JYZH_YE := V_TJ_JYZH_YE + V_JYZH_YE;
   V_TJ_JYZH_YSLX := V_TJ_JYZH_YSLX + V_JYZH_YSLX;
   V_TJ_YHZH_YE := V_TJ_YHZH_YE + V_YHZH_YE;
   V_TJ_YHZH_YSLX := V_TJ_YHZH_YSLX + V_YHZH_YSLX;
  
  /*  **************************************************************************
  *    （六）一般企业发行的权益工具
  **************************************************************************  */
  
  /*  **************************************************************************
  *    合计
  **************************************************************************  */

  V_JYZH_YE     := 0;
  V_JYZH_YSLX   := 0;
  V_YHZH_YE     := 0;
  V_YHZH_YSLX   := 0;

  BEGIN
    OPEN XZQTZ_CUR;
    FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
    WHILE (XZQTZ_CUR%FOUND) LOOP
      BEGIN
        --  IF  ((V_ACCTNGTYPE = 'DOMFINSE' AND V_SECMSIC != 'SE-SYYHCJZ') OR V_SECMSIC = 'SE-TYCD') AND V_BM > 3 THEN
              IF  V_COST = 1  THEN
                V_JYZH_YE := V_JYZH_YE + V_YE;
                V_JYZH_YSLX := V_JYZH_YSLX + V_LX;
              ELSE
                V_YHZH_YE := V_YHZH_YE + V_YE;
                V_YHZH_YSLX := V_YHZH_YSLX + V_LX;
              END IF;
         -- END IF;
          FETCH XZQTZ_CUR INTO V_ACCTNGTYPE ,V_COST ,V_SECMSIC,V_BM,V_SD,V_SETTQTY,V_YE,V_LX;
      END;
    END LOOP;
    CLOSE XZQTZ_CUR;
  END;

  UPDATE IFS_REPORT_XZQTZ
   SET JYZH_YE  = V_JYZH_YE,
       JYZH_YSLX = V_JYZH_YSLX,
       YHZH_YE  = V_YHZH_YE,
       YHZH_YSLX = V_YHZH_YSLX
   WHERE QZ = '合计';


   /*  **************************************************************************
  *    （七）其他企业债 除上面统计之外企业发行的债券
  **************************************************************************  */
  UPDATE IFS_REPORT_XZQTZ
     SET JYZH_YE  = V_JYZH_YE - V_TJ_JYZH_YE,
         JYZH_YSLX = V_JYZH_YSLX - V_TJ_JYZH_YSLX,
         YHZH_YE  = V_YHZH_YE - V_TJ_YHZH_YE,
         YHZH_YSLX = V_YHZH_YSLX -V_TJ_YHZH_YSLX
     WHERE QZ = '（七）其他企业债 除上面统计之外企业发行的债券';
   

  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
END sl_sp_xzqtz;





	