--DROP table;
drop table IFS_REPORT_DPTOTAL;
-- Create table
create table IFS_REPORT_DPTOTAL
(
 pub_month        	    varchar2(20),
 total_lose_amount		number(19,4),
 same_lose_amount		number(19,4),
 same_dpsum				number(19,4),
 same_dpmoney			number(19,4),
 same_dpbalance			number(19,4)
);
comment on table IFS_REPORT_DPTOTAL
  is '金融机构同业存单发行情况统计表';
-- Add comments to the columns 
comment on column IFS_REPORT_DPTOTAL.pub_month is '月份';
-- Add comments to the columns 
comment on column IFS_REPORT_DPTOTAL.total_lose_amount is '总负债';
-- Add comments to the columns 
comment on column IFS_REPORT_DPTOTAL.same_lose_amount is '同业负债';
-- Add comments to the columns 
comment on column IFS_REPORT_DPTOTAL.same_dpsum is '同业存单发行笔数';
-- Add comments to the columns 
comment on column IFS_REPORT_DPTOTAL.same_dpmoney is '同业存单发行金额';
-- Add comments to the columns 
comment on column IFS_REPORT_DPTOTAL.same_dpbalance is '同业存单余额';

--初始化数据
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('1月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('2月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('3月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('4月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('5月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('6月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('7月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('8月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('9月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('10月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('11月', NULL, NULL, NULL, NULL, NULL );
INSERT INTO IFS_REPORT_DPTOTAL (pub_month, total_lose_amount, same_lose_amount, same_dpsum,same_dpmoney,same_dpbalance) VALUES ('12月', NULL, NULL, NULL, NULL, NULL );


CREATE OR REPLACE PROCEDURE SL_SP_Dptotal(DATADATE IN VARCHAR2,
                                          retcode  OUT VARCHAR2,
                                          retmsg   OUT VARCHAR2) is

  v_PUB_MONTH         Ifs_Report_Dptotal.PUB_MONTH%TYPE;
  v_TOTAL_LOSE_AMOUNT Ifs_Report_Dptotal.TOTAL_LOSE_AMOUNT%TYPE;
  v_SAME_LOSE_AMOUNT  Ifs_Report_Dptotal.SAME_LOSE_AMOUNT%TYPE;
  v_SAME_DPSUM        Ifs_Report_Dptotal.SAME_DPSUM%TYPE;
  v_SAME_DPMONEY      Ifs_Report_Dptotal.SAME_DPMONEY%TYPE;
  v_SAME_DPBALANCE    Ifs_Report_Dptotal.SAME_DPBALANCE%TYPE;

  v_datadate varchar2(20);

  v_num      varchar2(20);


begin
  for i in 1 .. 12 loop
    V_DATADATE := substr(DATADATE, 1, 4);
    v_num := i;
    if v_num <= 9 then
      v_num := '-0' || v_num;
    elsif v_num > 9 then
      v_num := '-' || v_num;
    end if;
    V_DATADATE := V_DATADATE || v_num;
    SELECT
           count(1) as SAME_DPSUM,
           nvl(sum(d.ACTUAL_AMOUNT)/10000,0) as SAME_DPMONEY ,
           nvl(SUM(t.PRINAMT)/10000,0) as SAME_DPBALANCE
      into v_SAME_DPSUM,v_SAME_DPMONEY,v_SAME_DPBALANCE
      FROM ifs_cfetsrmb_dp d
      LEFT JOIN TPOS@Opics t
        ON d.INSTRUMENT_TYPE = trim(t.prodtype)
       AND d.deposit_code = trim(t.secid)
     WHERE SUBSTR(d.PUBLISH_DATE, 1, 7) =
           V_DATADATE
       and t.invtype = 'I'
       and d.INSTRUMENT_TYPE = 'CD';
    update Ifs_Report_Dptotal
       set SAME_DPSUM     = v_SAME_DPSUM,
           Same_Dpmoney   = v_SAME_DPMONEY,
           same_dpbalance = v_SAME_DPBALANCE

     where pub_month = i || '月';
  end loop;
  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
end SL_SP_Dptotal;