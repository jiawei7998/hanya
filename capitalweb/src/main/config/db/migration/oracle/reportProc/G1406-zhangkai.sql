--DROP table;
drop table IFS_REPORT_G1406;
-- Create table
create table IFS_REPORT_G1406
(
 cust_type           varchar2(105),  --客户类型
 cust_name           varchar2(120), --客户名称
 cust_orgcode        varchar2(180),  --客户代码(组织机构代码)
 risk_exp_sum        number(25,4),  --风险暴露总和-合计
 risk_exp            number(19,4),  --风险暴露总和-其中:不可豁免风险暴露(三家政策性银行是0)
 net_cap_sum		 number(25,4),  --占一级资本净额比例-合计
 net_cap			 number(19,4),  --占一级资本净额比例-其中不可豁免风险暴露
 gen_risk_sum        number(25,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-合计
 gen_risk_cafty      number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：拆放同业
 gen_risk_tycj       number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业拆借
 gen_risk_tyjk       number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业借款
 gen_risk_cufty      number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：存放同业
 gen_risk_mrfszy     number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：买入返售(质押式)
 gen_risk_jqzq       number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业债券（发行人主体类型是金融机构都算）
 gen_risk_zcxzq      number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：政策性金融债
 gen_risk_tycd       number(19,4),  --一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业存单
 spec_risk_sum       number(25,4),  --特定风险暴露-合计
 spec_risk_zcgl      number(19,4),  --特定风险暴露-资产管理产品
 spec_risk_xt        number(19,4),  --特定风险暴露-其中：信托产品
 spec_risk_fblc      number(19,4),  --特定风险暴露-其中：非保本理财
 spec_risk_zqzg      number(19,4),  --特定风险暴露-其中：证券业资管产品
 spec_risk_zczqh     number(19,4),  --特定风险暴露-其中：资产证券化产品
 trade_risk_exp      number(19,4),  --交易账簿风险暴露
 cust_risk_sum       number(25,4),  --交易对手信用风险暴露-合计
 cust_risk_cwysp     number(19,4),  --交易对手信用风险暴露-其中：场外衍生工具
 cust_risk_zqrz      number(19,4),  --交易对手信用风险暴露-其中：证券融资交易
 qz_risk_exp         number(19,4),  --潜在风险暴露
 qt_risk_exp         number(19,4),  --其他风险暴露
 rev_risk_exp        number(19,4),  --风险缓释转出的风险暴露（转入为负数）
 none_rev_risk       number(19,4)   --不考虑风险缓释作用的风险暴露总和
);
-- Add comments to the table 
comment on table IFS_REPORT_G1406
  is 'G1406大额风险暴露统计报表[G14_II不限制,G14_V同业单一客户100,G14_VI集团客户30]';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.cust_type is '客户类型';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.cust_name is '客户名称';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.cust_orgcode is '客户代码(组织机构代码)';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.risk_exp_sum is '风险暴露总和-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.risk_exp is '风险暴露总和-其中:不可豁免风险暴露(三家政策性银行是0)';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.net_cap_sum is '占一级资本净额比例-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.net_cap is '占一级资本净额比例-其中不可豁免风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_sum is '一般风险暴露 (商业银行大额风险暴露管理办法)-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_cafty is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：拆放同业';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_tycj is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业拆借';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_tyjk is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业拆借';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_cufty is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：存放同业';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_mrfszy is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：买入返售(质押式)';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_jqzq is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业债券（发行人主体类型是金融机构都算）';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_zcxzq is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：政策性金融债';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.gen_risk_tycd is '一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业存单';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.spec_risk_sum is '特定风险暴露-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.spec_risk_zcgl is '特定风险暴露-资产管理产品';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.spec_risk_xt is '特定风险暴露-其中：信托产品';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.spec_risk_fblc is '特定风险暴露-其中：非保本理财';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.spec_risk_zqzg is '特定风险暴露-其中：证券业资管产品';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.spec_risk_zczqh is '特定风险暴露-其中：资产证券化产品';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.trade_risk_exp is '交易账簿风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.cust_risk_sum is '交易对手信用风险暴露-合计';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.cust_risk_cwysp is '交易对手信用风险暴露-其中：场外衍生工具';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.cust_risk_zqrz is '交易对手信用风险暴露-其中：证券融资交易';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.qz_risk_exp is '潜在风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.qt_risk_exp is '其他风险暴露';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.rev_risk_exp is '风险缓释转出的风险暴露（转入为负数）';
-- Add comments to the columns 
comment on column IFS_REPORT_G1406.none_rev_risk is '不考虑风险缓释作用的风险暴露总和';

--初始化
--无



--执行存储过程
CREATE OR REPLACE PROCEDURE sl_sp_g1406(datadate IN VARCHAR2
                                       ,retcode  OUT VARCHAR2 --999-成功 其他均为失败
                                       ,retmsg   OUT VARCHAR2 --错误信息
                                        ) IS
  /*  **************************************************************************
  *1.根据客户信息查询基础交易数据
  *2.计算合计部分
  **************************************************************************  */
    
  v_cust_type			IFS_REPORT_G1406.cust_type%TYPE;
  v_cust_name			IFS_REPORT_G1406.cust_name%TYPE;
  v_cust_orgcode		IFS_REPORT_G1406.cust_orgcode%TYPE;
  v_risk_exp_sum		IFS_REPORT_G1406.risk_exp_sum%TYPE;
  v_gen_risk_sum		IFS_REPORT_G1406.gen_risk_sum%TYPE;
  v_gen_risk_cafty		IFS_REPORT_G1406.gen_risk_cafty%TYPE;
  v_gen_risk_tycj       IFS_REPORT_G1406.gen_risk_tycj%TYPE;
  v_gen_risk_tyjk       IFS_REPORT_G1406.gen_risk_tyjk%TYPE;
  v_gen_risk_cufty      IFS_REPORT_G1406.gen_risk_cufty%TYPE;
  v_gen_risk_mrfszy     IFS_REPORT_G1406.gen_risk_mrfszy%TYPE;
  v_gen_risk_jqzq       IFS_REPORT_G1406.gen_risk_jqzq%TYPE;
  v_gen_risk_zcxzq      IFS_REPORT_G1406.gen_risk_zcxzq%TYPE;
  v_gen_risk_tycd       IFS_REPORT_G1406.gen_risk_tycd%TYPE;
  v_spec_risk_sum		IFS_REPORT_G1406.spec_risk_sum%TYPE;
  v_spec_risk_zqzg		IFS_REPORT_G1406.spec_risk_zqzg%TYPE;
  v_trade_risk_exp      IFS_REPORT_G1406.trade_risk_exp%TYPE;
  v_rev_risk_exp        IFS_REPORT_G1406.rev_risk_exp%TYPE;
  
  
  v_cno 	   CHAR(11);
  v_datadate   DATE;
  
  
  CURSOR  g1406_cust IS 
  SELECT 
  CU.CNO,--客户号
  '同业集团客户',--客户类型
  cu.sn, --客户名称
  OC.SHCODE --客户代码(组织机构代码)
  FROM cust@opics cu 
  join IFS_OPICS_CUST OC ON OC.CNO = TRIM(CU.CNO) and OC.CUSTCLASS = '1';
  
BEGIN
  
  --数据日期
  IF datadate IS NULL THEN
    BEGIN
      SELECT prevbranprcdate INTO v_datadate FROM brps@opics WHERE br = '01';
    EXCEPTION
      WHEN no_data_found THEN
        retcode := '100';
        retmsg  := 'QUERY BRPS DATA NOT FOUND';
        RETURN;
    END;
  ELSE
    v_datadate := to_date(datadate, 'YYYY-MM-DD');
  END IF;

  BEGIN
	  
	  execute immediate 'TRUNCATE TABLE IFS_REPORT_G1406';
	  
	  OPEN g1406_cust;
		FETCH g1406_cust
		  INTO v_cno
			  ,v_cust_type
			  ,v_cust_name
			  ,v_cust_orgcode;
		WHILE (g1406_cust%FOUND) LOOP
	  --1.根据客户信息查询基础交易数据

	  --1.1 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业拆借
		  SELECT nvl((SELECT ABS(SUM(CCYAMT)) FROM DLDT@opics d WHERE v_cno = d.cno AND d.prodtype = 'CC'AND v_datadate BETWEEN d.VDATE AND d.MDATE ),0)/10000
		  INTO v_gen_risk_tycj
		  FROM dual;
	  
	  --1.2 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业借款
		  SELECT nvl((SELECT ABS(SUM(CCYAMT)) FROM DLDT@opics D WHERE v_cno = d.cno AND d.prodtype = 'LC'AND v_datadate BETWEEN D.VDATE AND D.MDATE ),0)/10000 
		  INTO v_gen_risk_tyjk
		  FROM dual;
	  
	  --1.3 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：存放同业
		  SELECT nvl((SELECT ABS(SUM(CCYAMT)) FROM DLDT@opics D WHERE v_cno = d.cno AND d.prodtype = 'CT'AND v_datadate BETWEEN D.VDATE AND D.MDATE ),0)/10000 
		  INTO v_gen_risk_cufty 
		  FROM dual;
	  
	  --1.4 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：买入返售(质押式)
		  SELECT nvl((SELECT ABS(SUM(NOTAMT)) FROM RPRH@opics R WHERE v_cno = R.cno AND R.PRODTYPE = 'VP' AND v_datadate BETWEEN R.VDATE AND R.MATDATE ),0)/10000
		  INTO v_gen_risk_mrfszy  
		  FROM DUAL;
	  
	  --1.5 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业债券（发行人主体类型是金融机构都算）(银行账户)
		  SELECT  
		  nvl((SELECT 
			(sum(CASE WHEN trim(AT) IS NULL THEN 0 ELSE AT END) 
			+ sum(CASE WHEN trim(aa) IS NULL THEN 0 ELSE aa END) 
			+ sum(CASE WHEN trim(aH)IS NULL THEN 0 ELSE ah END) 
			+ sum(CASE WHEN trim(aH1) IS NULL THEN 0 ELSE aH1 END) 
			+ sum(CASE WHEN trim(aa1)IS NULL THEN 0 ELSE aa1 END)) AS  amt
			FROM 
			 (
			SELECT
			  CASE
				WHEN trim(t.invtype)= 'T' THEN abs(t.settavgcost)+ t.tdymtm
			  END AT,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)+ t.unamortamt + t.tdymtm
			  END aa,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)-(t.unamortamt*-1)
			  END aH,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)+ t.unamortamt + t.tdymtm
			  END aa1,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)-(t.unamortamt*-1)
			  END aH1
			FROM
			  OPICS47.SL_TPOS_H@opics t
			LEFT JOIN OPICS47.SECM@opics s ON
			  t.secid = s.secid
			WHERE
			  t.Cost LIKE '82%'
			  AND T.BR = '01'
			  AND T.CCY = 'CNY'
			  AND S.ACCTNGTYPE IN ('DOMPOLICSE','DOMFINSE','DOMNBFINSE')
			  AND S.issuer = trim(v_cno)
			  AND T.POSTDATE = v_datadate
			  AND T.QTY>0) 
		  ),0)/10000
		  INTO v_gen_risk_jqzq
		  FROM dual;
	  
	  --1.6 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：政策性金融债(银行账户)
		  SELECT 
			nvl((SELECT 
			(sum(CASE WHEN trim(AT) IS NULL THEN 0 ELSE AT END) 
			+ sum(CASE WHEN trim(aa) IS NULL THEN 0 ELSE aa END) 
			+ sum(CASE WHEN trim(aH)IS NULL THEN 0 ELSE ah END) 
			+ sum(CASE WHEN trim(aH1) IS NULL THEN 0 ELSE aH1 END) 
			+ sum(CASE WHEN trim(aa1)IS NULL THEN 0 ELSE aa1 END)) AS  amt
			FROM 
			 (
			SELECT
			  CASE
				WHEN trim(t.invtype)= 'T' THEN abs(t.settavgcost)+ t.tdymtm
			  END AT,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)+ t.unamortamt + t.tdymtm
			  END aa,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)-(t.unamortamt*-1)
			  END aH,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)+ t.unamortamt + t.tdymtm
			  END aa1,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)-(t.unamortamt*-1)
			  END aH1
			FROM
			  OPICS47.SL_TPOS_H@opics t
			LEFT JOIN OPICS47.SECM@opics s ON
			  t.secid = s.secid
			WHERE
			  t.Cost LIKE '82%'
			  AND T.BR = '01'
			  AND T.CCY = 'CNY'
			  AND S.ACCTNGTYPE = 'DOMPOLICSE'
			  AND S.issuer = trim(v_cno)
			  AND T.POSTDATE = v_datadate
			  AND T.QTY>0) 
		  ),0)/10000
		  INTO v_gen_risk_zcxzq 
		  FROM DUAL;
	  
	  --1.7 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业存单(银行账户)
		  SELECT 
			nvl((SELECT 
			(sum(CASE WHEN trim(AT) IS NULL THEN 0 ELSE AT END) 
			+ sum(CASE WHEN trim(aa) IS NULL THEN 0 ELSE aa END) 
			+ sum(CASE WHEN trim(aH)IS NULL THEN 0 ELSE ah END) 
			+ sum(CASE WHEN trim(aH1) IS NULL THEN 0 ELSE aH1 END) 
			+ sum(CASE WHEN trim(aa1)IS NULL THEN 0 ELSE aa1 END)) AS  amt
			FROM 
			 (
			SELECT
			  CASE
				WHEN trim(t.invtype)= 'T' THEN abs(t.settavgcost)+ t.tdymtm
			  END AT,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)+ t.unamortamt + t.tdymtm
			  END aa,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)-(t.unamortamt*-1)
			  END aH,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)+ t.unamortamt + t.tdymtm
			  END aa1,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)-(t.unamortamt*-1)
			  END aH1
			FROM
			  OPICS47.SL_TPOS_H@opics t
			LEFT JOIN OPICS47.SECM@opics s ON
			  t.secid = s.secid
			WHERE
			  t.Cost LIKE '82%'
			  AND T.BR = '01'
			  AND T.CCY = 'CNY'
			  AND S.ACCTNGTYPE = 'DOMCD'
			  AND S.issuer = trim(v_cno)
			  AND T.POSTDATE = v_datadate
			  AND T.QTY>0) 
			),0)/10000
		  INTO v_gen_risk_tycd
		  FROM DUAL;
	  
	  --1.8 交易账簿风险暴露 (交易账户 同业债券)
		  SELECT  
			nvl((SELECT 
			(sum(CASE WHEN trim(AT) IS NULL THEN 0 ELSE AT END) 
			+ sum(CASE WHEN trim(aa) IS NULL THEN 0 ELSE aa END) 
			+ sum(CASE WHEN trim(aH)IS NULL THEN 0 ELSE ah END) 
			+ sum(CASE WHEN trim(aH1) IS NULL THEN 0 ELSE aH1 END) 
			+ sum(CASE WHEN trim(aa1)IS NULL THEN 0 ELSE aa1 END)) AS  amt
			FROM 
			 (
			SELECT
			  CASE
				WHEN trim(t.invtype)= 'T' THEN abs(t.settavgcost)+ t.tdymtm
			  END AT,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)+ t.unamortamt + t.tdymtm
			  END aa,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype = 'IM' THEN abs(t.prinamt)+ abs(t.accroutst)-(t.unamortamt*-1)
			  END aH,
			  CASE
				WHEN trim(t.invtype)= 'A'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)+ t.unamortamt + t.tdymtm
			  END aa1,
			  CASE
				WHEN trim(t.invtype)= 'H'
				AND s.prodtype != 'IM' THEN abs(t.prinamt)-(t.unamortamt*-1)
			  END aH1
			FROM
			  OPICS47.SL_TPOS_H@opics t
			LEFT JOIN OPICS47.SECM@opics s ON
			  t.secid = s.secid
			WHERE
			  t.Cost LIKE '83%'
			  AND T.BR = '01'
			  AND T.CCY = 'CNY'
			  AND S.ACCTNGTYPE IN ('DOMPOLICSE','DOMFINSE','DOMNBFINSE')
			  AND S.issuer = trim(v_cno)
			  AND T.POSTDATE = v_datadate
			  AND T.QTY>0) 
			),0)/10000
		  INTO v_trade_risk_exp 
		  FROM DUAL;
	  
	  --1.9 风险缓释转出的风险暴露（转入为负数）
		  SELECT  
		  nvl((SELECT ABS(SUM(NOTAMT)) FROM RPRH@opics R WHERE v_cno = R.cno AND R.PRODTYPE = 'VP' AND v_datadate BETWEEN R.VDATE AND R.MATDATE ),0)/10000
		  INTO v_rev_risk_exp 
		  FROM DUAL;
	  
	  --1.10 特定风险暴露-其中:证券业资管产品
	  --TODO 专户基金穿透后的资产
		v_spec_risk_zqzg := 0;
	  
	  --2.计算合计部分
	  --更新 一般风险暴露 (商业银行大额风险暴露管理办法)-其中：拆放同业 = 其中：同业拆借 + 其中：同业借款
		v_gen_risk_cafty := v_gen_risk_tycj + v_gen_risk_tyjk;
	  
	  --更新一般风险暴露-合计 = 其中：拆放同业 + 其中：存放同业 + 其中:买入返售(质押式) + 其中:同业债券 + 其中:同业存单
		v_gen_risk_sum := v_gen_risk_cafty + v_gen_risk_cufty + v_gen_risk_mrfszy + v_gen_risk_jqzq + v_gen_risk_tycd;
	  
	  --更新特定风险暴露-合计 =  资管管理产品(只有 其中:证券业资管产品) + 资产证券化产品
		v_spec_risk_sum := v_spec_risk_zqzg;
	  
	  --更新风险暴露总和-合计 = 一般风险暴露-合计 + 特定风险暴露-合计 + 交易账簿风险暴露 + 交易对手信用风险暴露-合计 + 潜在风险暴露 + 其他风险暴露
		v_risk_exp_sum := v_gen_risk_sum + v_spec_risk_sum + v_trade_risk_exp + 0 + 0 + 0;
	  
	  
	  --插入信息
		  INSERT INTO IFS_REPORT_G1406
		  (
		  cust_type,
		  cust_name,
		  cust_orgcode,
		  risk_exp_sum,
		  risk_exp,
		  net_cap_sum,
		  net_cap,
		  gen_risk_sum,
		  gen_risk_cafty,
		  gen_risk_tycj,
		  gen_risk_tyjk,
		  gen_risk_cufty,
		  gen_risk_mrfszy,
		  gen_risk_jqzq,
		  gen_risk_zcxzq,
		  gen_risk_tycd,
		  spec_risk_sum,
		  spec_risk_zcgl,
		  spec_risk_xt,
		  spec_risk_fblc,
		  spec_risk_zqzg,
		  spec_risk_zczqh,
		  trade_risk_exp,
		  cust_risk_sum,
		  cust_risk_cwysp,
		  cust_risk_zqrz,
		  qz_risk_exp,
		  qt_risk_exp,
		  rev_risk_exp,
		  none_rev_risk
		  )
		  VALUES
		  (
		  v_cust_type,--客户类型
		  v_cust_name,--客户名称
		  v_cust_orgcode,--客户代码(组织机构代码)
		  v_risk_exp_sum,--风险暴露总和-合计
		  0,--风险暴露总和-其中:不可豁免风险暴露(三家政策性银行是0)
		  0,--占一级资本净额比例-合计
		  0,--占一级资本净额比例-其中不可豁免风险暴露
		  v_gen_risk_sum,--一般风险暴露 (商业银行大额风险暴露管理办法)-合计
		  v_gen_risk_cafty,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：拆放同业
		  v_gen_risk_tycj,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业拆借
		  v_gen_risk_tyjk,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业借款
		  v_gen_risk_cufty,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：存放同业
		  v_gen_risk_mrfszy,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：买入返售(质押式)
		  v_gen_risk_jqzq,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业债券（发行人主体类型是金融机构都算）
		  v_gen_risk_zcxzq,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：政策性金融债
		  v_gen_risk_tycd,--一般风险暴露 (商业银行大额风险暴露管理办法)-其中：同业存单
		  v_spec_risk_sum,--特定风险暴露-合计
		  0,--特定风险暴露-资产管理产品
		  0,--特定风险暴露-其中：信托产品
		  0,--特定风险暴露-其中：非保本理财
		  v_spec_risk_zqzg,--特定风险暴露-其中：证券业资管产品
		  0,--特定风险暴露-其中：资产证券化产品
		  v_trade_risk_exp,--交易账簿风险暴露
		  0,--交易对手信用风险暴露-合计
		  0,--交易对手信用风险暴露-其中：场外衍生工具
		  0,--交易对手信用风险暴露-其中：证券融资交易
		  0,--潜在风险暴露
		  0,--其他风险暴露
		  v_rev_risk_exp,--风险缓释转出的风险暴露（转入为负数）
		  0--不考虑风险缓释作用的风险暴露总和
		  );
	  
		FETCH g1406_cust
		  INTO v_cno
			  ,v_cust_type
			  ,v_cust_name
			  ,v_cust_orgcode;
	  END LOOP;
	  CLOSE g1406_cust;

	INSERT INTO IFS_REPORT_G1406
      (
      cust_type,
      cust_name,
      cust_orgcode,
      risk_exp_sum,
      risk_exp,
      net_cap_sum,
      net_cap,
      gen_risk_sum,
      gen_risk_cafty,
      gen_risk_tycj,
      gen_risk_tyjk,
      gen_risk_cufty,
      gen_risk_mrfszy,
      gen_risk_jqzq,
      gen_risk_zcxzq,
      gen_risk_tycd,
      spec_risk_sum,
      spec_risk_zcgl,
      spec_risk_xt,
      spec_risk_fblc,
      spec_risk_zqzg,
      spec_risk_zczqh,
      trade_risk_exp,
      cust_risk_sum,
      cust_risk_cwysp,
      cust_risk_zqrz,
      qz_risk_exp,
      qt_risk_exp,
      rev_risk_exp,
      none_rev_risk
      )
      SELECT 
      '列表客户合计',
      '',
      '',
      sum(risk_exp_sum),
      sum(risk_exp),
      sum(net_cap_sum),
      sum(net_cap),
      sum(gen_risk_sum),
      sum(gen_risk_cafty),
      sum(gen_risk_tycj),
      sum(gen_risk_tyjk),
      sum(gen_risk_cufty),
      sum(gen_risk_mrfszy),
      sum(gen_risk_jqzq),
      sum(gen_risk_zcxzq),
      sum(gen_risk_tycd),
      sum(spec_risk_sum),
      sum(spec_risk_zcgl),
      sum(spec_risk_xt),
      sum(spec_risk_fblc),
      sum(spec_risk_zqzg),
      sum(spec_risk_zczqh),
      sum(trade_risk_exp),
      sum(cust_risk_sum),
      sum(cust_risk_cwysp),
      sum(cust_risk_zqrz),
      sum(qz_risk_exp),
      sum(qt_risk_exp),
      sum(rev_risk_exp),
      sum(none_rev_risk) 
      FROM IFS_REPORT_G1406;

  END;
  
  COMMIT;
  retcode := '999';
  retmsg  := 'PROCESSED SUCCESSFUL!';
  RETURN;
END sl_sp_g1406;  