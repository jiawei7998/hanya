import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    /**
     *  返回当前日期  Date 类型
     * @return
     */
    public static Date getToday() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public static Timestamp getTimeStamp()
    {
        Calendar cal = Calendar.getInstance();
        return  new Timestamp(cal.getTime().getTime());
    }

    public static String getToday(String format) {
        Date today = getToday();
        return new SimpleDateFormat(format).format(today);
    }

    public static Date getNowWithYYYYMMHH() throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(getTodayWithYYYYMMHH());
    }

    public static String getTodayWithYYYYMMHH( ) {

        return getToday("yyyy-MM-dd");
    }
    public static String  getNow(String format)
    {
        Date today = getToday();
        return new SimpleDateFormat(format).format(today);
    }
    public static String getNowDefault()
    {
        return getNow("yyyyMMdd HH:mm:ss");
    }
    public static long geCurrentTime() {
        return System.currentTimeMillis();
    }

    public static long  calTime(Date begin, Date end)
    {
        return end.getTime()-begin.getTime();
    }

    public static String getDate() {
        Date now = Calendar.getInstance().getTime();
        return new SimpleDateFormat("yyyyMMdd").format(now);
    }

    public static String getTime() {
        Date now = Calendar.getInstance().getTime();
        return new SimpleDateFormat("HHmmss").format(now);
    }

    public static  Date parserDate(String date,String format) throws ParseException
    {
        return  new SimpleDateFormat(format).parse(date);


    }

    public static String  moveDate(String date ,String format ,int day) throws Exception
    {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, day);
        return  new SimpleDateFormat(format).format(cal.getTime());

    }


    /**
     * 不超过当前日期
     * @param date
     * @param format
     * @param today
     * @return
     * @throws Exception
     */
    public static String  moveDateNotOverToday(String date ,String format ,int day) throws Exception
    {
        Date d=parserDate(date,format);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.DAY_OF_YEAR, day);
        Date today=getToday();
        if(cal.after(today))
        {
            return new SimpleDateFormat(format).format(cal.getTime());
        }
        else
        {
            return new SimpleDateFormat(format).format(today.getTime());
        }





    }
    public static  boolean isAfterToday(String date ,String format) throws Exception
    {
        Date d=parserDate(date,format);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        Date today=getToday();
        return cal.after(today);

    }

    public static String  getYesterDay(String format)
    {
        Date today = getToday();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_YEAR, -1);

        return new SimpleDateFormat(format).format(cal.getTime());

    }

    public static String changeDateFormat(String date, String srcFormat,String desFormat) throws Exception
    {

        Date d=new SimpleDateFormat(srcFormat).parse(date);
        return new SimpleDateFormat(desFormat).format(d);

    }

    /**
     * 将日期转成format格式的字段串
     * @param date
     * @param format
     * @return
     */
    public static String parserDate(Date date,String format)
    {
        return new SimpleDateFormat(format).format(date);
    }

    /**(20140422新增该方法,主要是导入市场数据接口所用)
     * Tests if this date is before the specified date
     * 判断第一个日期是否在第二个日期之前
     *
     * @param date1
     * @param date2
     * @return
     * @throws Exception
     */
    public static boolean isHourBefore(String date1, String date2) throws Exception {
        //DateFormat df = DateFormat.getTimeInstance();
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("HH:mm:ss");
        return df.parse(date1).before(df.parse(date2));
    }


    /**(20140422新增该方法,用来判断处理时间是否在规定的时间范围内)
     * 如果在startTime与endTime之间,返回true,否则返回false
     * @param startTime "HH:mm:ss"
     * @param endTime "HH:mm:ss"
     * @return
     * @throws Exception
     */
    public static boolean isIntime(String startTime,String endTime) throws Exception
    {
        java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("HH:mm:ss");
        java.util.Date currentTime = new java.util.Date();
        String hour = formatter.format(currentTime);

        if ((isHourBefore(startTime, hour)&& isHourBefore(hour, endTime))) {
            //System.out.println("时间不在07:00和16:35之间!");
            return true;
        }else
            return false;
    }

    /**(20140425新增该方法，主要是在将债券信息导入接口表的时候所用)
     * 将字符日期加入分隔符号,如20120101,返回2012-01-01
     * @param date
     * @return
     */
    public static String getAddSeparateDate(String date)
    {
        if(date.length()<=8)
        {
            date = date.substring(0, 4)+"-"+date.substring(4, 6)+"-"+date.substring(6);
        }
        return date;
    }

    /**
     * 获取某年某月的第一天
     * @param year
     * @param month
     * @return
     */
    public static String getFirstDayOfMonth(int year, int month) {//月份从零开始，零代表一月
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        // 某年某月的第一天
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        String first = parserDate(cal.getTime(),"yyyy-MM-dd");
        int sum = cal.getActualMaximum(Calendar.DATE);   //得到对应月份的天数
        return first;
    }

    /**
     * 获取某年某月的最后一天
     * @param year
     * @param month
     * @return
     */
    public static String getLastDayOfMonth(int year, int month) {//月份从零开始，零代表一月
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        // 某年某月的最后一天
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        String last = parserDate(cal.getTime(),"yyyy-MM-dd");
        int sum = cal.getActualMaximum(Calendar.DATE);   //得到对应月份的天数
        return last;
    }

    /**
     * 取到给定日期的下一天
     * @param date
     * @param format
     * @return
     * @throws ParseException
     */
    public static String getNextDayOfMonth(String date,String format) throws ParseException {
        Calendar cal = Calendar.getInstance();
        Date date1 = parserDate(date,format);
        cal.setTime(date1);
        cal.add(Calendar.DAY_OF_YEAR,1);
        String next = DateUtil.parserDate(cal.getTime(),format);
        return next;
    }

    /**
     * 取到给定日期的前一天
     * @param date
     * @param format
     * @return
     * @throws ParseException
     */
    public static String getBeforeDayOfMonth(String date,String format) throws ParseException {
        Calendar cal = Calendar.getInstance();
        Date date1 = parserDate(date,format);
        cal.setTime(date1);
        cal.add(Calendar.DAY_OF_YEAR,-1);
        String next = DateUtil.parserDate(cal.getTime(),format);
        return next;
    }

    public static Timestamp getTimestamp(String dateTime,String sformat){
        DateFormat format = new SimpleDateFormat(sformat);
        format.setLenient(false);
        Timestamp ts = null;
        try {
            ts = new Timestamp(format.parse(dateTime).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ts;
    }

    public static Timestamp getTimestamp(String dateTime){
        return getTimestamp(dateTime,"yyyy-MM-dd");
    }

    public static String getDateTime(){
        return getNow("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到两个日期的天数据差
     * @param startDate  开始日期
     * @param endDate	 结束日期
     * @return
     * @throws ParseException
     */
    public static long getDifferenceDays(String startDate,String endDate) throws ParseException{

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);
        Date date1 = format.parse(startDate);
        Date date2 = format.parse(endDate);
        //计算差值，天数
        long days=(date2.getTime()-date1.getTime())/(1000*60*60*24);

        return days;
    }


    /**
     * 得到两个日期的天数据差
     * @param startDate  开始日期
     * @param endDate	 结束日期
     * @return
     * @throws ParseException
     */
    public static long getDifferenceDays(Date startDate,Date endDate) throws ParseException{

        //计算差值，天数
        long days=(endDate.getTime()-startDate.getTime())/(1000*60*60*24);

        return days;
    }

//	public static void main(String[] args) throws ParseException {
//		System.out.println(getNextDayOfMonth("2014-01-05","yyyy-MM-dd")+"");
//
//		System.out.println(getDifferenceDays("2014-01-05","2014-01-09"));
//	}
}
