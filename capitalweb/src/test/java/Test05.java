/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName Test05.java
 * @Description 金融机构
 * @createTime 2021年11月19日 15:47:00
 */
public class Test05 {

    public static void main(String[] args) {
        StringBuffer tposSql = new StringBuffer();
        String opicsName = "opics31plus";
        tposSql.append(" SELECT type,sum(prinamt) as amt FROM ( ");
        tposSql.append(" Select  ");
        tposSql.append(" KTP.PRODTYPE, ");
        tposSql.append(" KTP.INVTYPE, ");
        tposSql.append(" KTP.createdate, ");
        tposSql.append(" KTP.prinamt , ");
        tposSql.append(" CASE WHEN ");
        tposSql.append(" KTP.INVTYPE = 'T' ");
        tposSql.append("  THEN ABS(KTP.SETTAVGCOST) + KTP.TDYMTM ");
        tposSql.append(" WHEN ");
        tposSql.append(" KTP.INVTYPE = 'A' AND KTP.PRODTYPE = 'IM' ");
        tposSql.append(" THEN abs(prinamt) + abs(accroutst) + unamortamt + tdymtm  ");
        tposSql.append(" WHEN ");
        tposSql.append(" KTP.INVTYPE = 'A' AND KTP.PRODTYPE != 'IM' ");
        tposSql.append(" THEN abs(prinamt) + unamortamt + tdymtm ");
        tposSql.append(" WHEN ");
        tposSql.append(" KTP.INVTYPE = 'H' AND KTP.PRODTYPE = 'IM' ");
        tposSql.append(" THEN ABS(PRINAMT) + abs(accroutst) + unamortamt ");
        tposSql.append(" WHEN ");
        tposSql.append(" KTP.INVTYPE = 'H' AND KTP.PRODTYPE != 'IM' ");
        tposSql.append(" THEN Abs(Prinamt) + Unamortamt  ");
        tposSql.append(" ELSE 0  ");
        tposSql.append(" END AS AMT, ");
        tposSql.append(" CASE WHEN ");
        tposSql.append(" CU.SIC = 'FI31' ");
        tposSql.append(" THEN 'a' ");
        tposSql.append(" WHEN ");
        tposSql.append(" CU.ACCTNGTYPE = 'CTRLBK' ");
        tposSql.append(" THEN 'b' ");
        tposSql.append(" WHEN ");
//			tposSql.append(" CU.ACCTNGTYPE = 'POLICYBK' OR CU.ACCTNGTYPE = 'COMMBK-D' OR CU.ACCTNGTYPE = 'OTHERDEFIN' ");
        tposSql.append(" CU.ACCTNGTYPE = 'COMMBK-D' OR CU.ACCTNGTYPE = 'OTHERDEFIN' ");
        tposSql.append(" THEN 'c' ");
        tposSql.append(" WHEN ");
        tposSql.append(" Cu.Sic='FI02' or Cu.Sic='FI13' or Cu.Sic='FI15' Or Cu.Sic='FI17' Or Cu.Sic='FI20' Or Cu.Sic='FI21' ");
        tposSql.append(" OR CU.SIC='FI22' OR CU.SIC='FI23' OR CU.SIC = 'FI25' OR CU.SIC='FI24' ");
        tposSql.append(" THEN 'd' ");
        tposSql.append(" WHEN ");
        tposSql.append(" CU.SIC = 'FI13' OR CU.SIC='FI14' ");
        tposSql.append(" THEN 'e' ");
        tposSql.append(" WHEN ");
        tposSql.append(" CU.SIC='FI16' ");
        tposSql.append(" THEN 'f' ");
        tposSql.append(" WHEN ");
        tposSql.append(" CU.SIC = 'FI34' OR CU.SIC = 'FI35' OR CU.SIC = 'FI36' ");
        tposSql.append(" THEN  'g' ");
        tposSql.append(" WHEN ");
        tposSql.append(" CU.SIC = 'FI11' ");
        tposSql.append(" THEN  'h' ");
        tposSql.append(" WHEN  ");
        tposSql.append(" CU.SIC = 'FI12' ");
        tposSql.append(" THEN 'i' ");
        tposSql.append(" WHEN  ");
        tposSql.append(" CU.SIC = 'FI42' or  CU.SIC = 'FI26' or CU.SIC = 'FI27' or CU.SIC = 'FI28' or CU.SIC = 'FI29' or CU.SIC = 'FI43' or CU.SIC = 'FI44' or CU.SIC = 'FI30'");
        tposSql.append(" THEN 'j' ");
        tposSql.append(" WHEN  ");
        tposSql.append(" CU.SIC = 'FI18' OR CU.SIC = 'FI39' OR CU.SIC = 'FI40' OR CU.SIC = 'FI41' ");
        tposSql.append(" Then 'k' ");
        tposSql.append(" ELSE 'l' ");
        tposSql.append(" END AS TYPE ");
        tposSql.append(" FROM K_TPOSBACKUP KTP  ");
        tposSql.append(" Left Join ");
        tposSql.append(opicsName+".SECM SE ON ");
        tposSql.append(" TRIM(KTP.SECID) = TRIM(SE.SECID) ");
        tposSql.append(" Left Join  ");
        tposSql.append(opicsName+".CUST CU  ");
        tposSql.append(" ON  ");
        tposSql.append(" TRIM(SE.ISSUER) = TRIM(CU.CNO) ");
        tposSql.append(" WHERE KTP.SETTQTY > 0 ");
        tposSql.append(" ) ");

        System.out.println(tposSql);
    }
}
