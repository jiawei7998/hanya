import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.List;

public class CDCMessage {
    private Document doc = null;

    public CDCMessage(Document document) {
        this.doc = document;
    }

    private static CDCMessage createCDCMessage(String path) {
        Document document = null;
        try{
            File f = new File(path);
            SAXReader reader = new SAXReader();
            document = reader.read(f);
        } catch(Exception e) {
            System.out.println("Fail to create CDC message with the given message code or wrong template content." + e.getMessage());
            e.printStackTrace();
        }
        return new CDCMessage(document);
    }

    public static CDCMessage createReqCDCMessage(String msgCode) {
        String loc = CDCMessage.class.getResource("").getFile()+ msgCode + ".xml";
        return createCDCMessage(loc);
    }

    public static CDCMessage createPushCDCMessage(String filePath) {
        return createCDCMessage(filePath);
    }

    public void setMessageContent(String path, String content) {
        String fixedPath = path.replace(".", "/");
        try{
            this.doc.selectSingleNode(fixedPath).setText(content);
        } catch(Exception e) {
            System.out.println("Failed to set the content with path " + fixedPath + ", content " + content + ".");
            e.printStackTrace();
        }
    }

    public void setMessageContentCheck(String path,String content){
        if(content == null){
            return;
        }else{
            setMessageContent(path,content.trim());
        }
    }

    public String getXMLContent()  {
        String xmlContent = null;
        try{
            processEmptyLeaf(this.doc.getRootElement());
            OutputFormat format = OutputFormat.createPrettyPrint();
            //OutputFormat format = new OutputFormat();
            format.setTrimText(true);
            format.setEncoding("GBK");
            OutputStream outputStream = new ByteArrayOutputStream();
            XMLWriter writer = new XMLWriter(outputStream, format);
            writer.write(doc);
            writer.close();
            xmlContent = outputStream.toString();
        } catch (Exception e) {
            System.out.println("Message content encoding excetion." + e.getMessage());
            e.printStackTrace();
        }
        return xmlContent;
    }

    private static void processEmptyLeaf(Element element) {
        List<Element> eList = element.elements();
        if(eList.isEmpty()) {
            if( element.getText().equals(""))
                element.getParent().remove(element);
        } else {
            for(Element e : eList) {
                processEmptyLeaf(e);
            }
            if(element.isRootElement() == false && element.elements().isEmpty())
                element.getParent().remove(element);
        }
    }

    public String getMessageContent(String path) {
        String fixedPath = path.replace(".", "/");
        String content = null;
        try{
            content = this.doc.selectSingleNode(fixedPath).getText();
        } catch(Exception e) {
            System.out.println("Failed to set the content with path " + fixedPath + ", content " + content + ".");
            e.printStackTrace();
        }
        return content;
    }

    /**
     * 在获取节点内容时，先检查是否存在该节点，
     * 如果存在则获取其内容，否则返回空
     * @param path
     * @return
     */
    public String getMessageContentCheck(String path){
        String fixedPath = path.replace(".", "/");
        String content = null;
        List a = null;
        a = this.doc.selectNodes(fixedPath);
        if(a.size() > 0){
            for(int i=0;i<a.size();i++){
                content = this.getMessageContent(path);
            }
        }
        else{
            content = null;
        }
        return content;
    }

    /**
     * 检查该报文节点是点存在
     * 存在返回true
     * 不存在返回false
     * @param path
     * @return
     */
    public boolean isExistNode(String path){
        String fixedPath = path.replace(".", "/");
        List a = null;
        boolean flag = true;
        a = this.doc.selectNodes(fixedPath);
        if(a.size() > 0){
            flag = true;
        }else{
            flag = false;
        }
        return flag;
    }

    /**该方法将会返回相同节点个数的
     * @param path
     * @return
     */
    public String getMultMessageContent(String path){
        String fixedPath = path.replace(".", "/");
        String content = null;
        String nodeText = null;
        List a = null;
        a = this.doc.selectNodes(fixedPath);
        if(a.size() > 0){
            for(int i = 0 ; i < a.size();i++){
                nodeText = ((Node)a.get(i)).getText();
                if(content != null){
                    content = content + "," + nodeText;
                }
                else{
                    content = nodeText;
                }
            }
        }
        else{
            content = null;
        }
        return content;
    }

    /*public List<Map<String,String>> getMessage(String path){
        String fixedPath = path.replace(".", "/");
        String BdId = null;
        String BdShrtNm = null;
        String kemu = null;
        List a = null;
        a = this.doc.selectNodes(fixedPath);

    }
    public Integer getNodeSize(String path){
        String fixedPath = path.replace(".", "/");
        List a = null;
        a = this.doc.selectNodes(fixedPath);
        return a.size();

    }*/
    public void setMultMessageContent(String path,String childNode,String amtNode,String content,String amtContent){
        String fixedPath = path.replace(".", "/");
        String childPath = childNode.replace(".", "/");
        String amtPath = amtNode.replace(".", "/");
        String[] contentSimple = content.split(",");
        String[] amtContentSimple = amtContent.split(",");
        try{
            for(int i=0;i<contentSimple.length;i++){
                createNode("/Document/SttlmDtl",childPath,amtPath,contentSimple[i],amtContentSimple[i]);
            }
        } catch(Exception e) {
            System.out.println("Failed to set the content with path " + fixedPath + ", content " + content + ".");
            e.printStackTrace();
        }
    }
    public void createNode(String path,String childNode,String amtNode,String content,String amtContent){
        String[] childElement = childNode.split("/");
        String[] amtElement = amtNode.split("/");
        List<Node> BdList = this.doc.selectNodes(path);
        for(java.util.Iterator it = BdList.iterator();it.hasNext();){
            Element Bd = (Element)it.next();
            Element bd1 = Bd.addElement(childElement[0]);
            bd1.addElement(childElement[1]).setText(content);
            bd1.addElement(amtElement[1]).setText(amtContent);

        }
    }
    public void setMultMessageContentTest(String path,String childNode,String amtNode,String content,String amtContent){
        String fixedPath = path.replace(".", "/");
        String[] contentSimple = content.split(",");
        String[] amtContentSimple = amtContent.split(",");
        String ch1 = childNode.replace(".", "/");
        String ch2 = amtNode.replace(".", "/");
        String[] childElement = ch1.split("/");
        String[] amtElement = ch2.split("/");
        System.out.println(childElement[0]);
        System.out.println(amtElement);
        for(int i=0;i<contentSimple.length;i++){
            Element node = (Element) this.doc.selectSingleNode(fixedPath);
            Element bd1 = node.addElement(childElement[0]);
            bd1.addElement(childElement[1]).setText(contentSimple[i]);
            bd1.addElement(amtElement[1]).setText(amtContentSimple[i]);
        }
    }

    public static CDCMessage stringChangeXml(String msg){
        Document doc = null;
        try {
            doc = DocumentHelper.parseText(msg);
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return new CDCMessage(doc);
    }

}
