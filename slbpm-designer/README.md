# 框架版本

* Activiti: **5.22.0**

* Spring: **4.3.18.RELEASE**

# Changelog

### Ver 1.0.1 - 2017/9/21

* 数据库密码加密

### Ver 1.0.0 - 2017/4/22

* 第一个原始版本

# 注意：
因为Oracle驱动需要官方授权，所以在pom.xml文件里直接配置无法成功，需要将驱动包安装到本地maven库。

### 例如：
将驱动(如ojdbc14.jar)直接复制到"C:\Users\Administrator"，再在命令行下进入该位置，执行以下命令

    mvn install:install-file -DgroupId=com.oracle -DartifactId=ojdbc14 -Dversion=10.2.0.1.0 -Dpackaging=jar -Dfile=ojdbc14.jar
    
    mvn install:install-file -DgroupId=mysql -DartifactId=mysql-connector-Java -Dversion=5.1.34 -Dpackaging=jar -Dfile=mysql-connector-java-5.1.7-bin.jar
    
    mvn install:install-file -DgroupId=com.singlee.capital -DartifactId=common -Dversion=1.0 -Dpackaging=jar -Dfile=common-1.0.jar
    
    mvn install:install-file -DgroupId=com.singlee.capital -DartifactId=system -Dversion=1.0 -Dpackaging=jar -Dfile=system-1.0.jar
