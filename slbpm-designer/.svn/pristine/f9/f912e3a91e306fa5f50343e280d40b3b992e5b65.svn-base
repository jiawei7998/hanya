package com.singlee.slpmd.controller.identity;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slpmd.util.UserUtil;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created by rapix on 2017/4/23.
 * 用户登录操作
 */
@Controller
@RequestMapping("/user")
public class LoginController {

    private static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Resource
    private IdentityService identityService;

    /**
     * 用户登录
     * @param params {"username":xxx,"password":xxx}
     * @param httpsession http session
     * @return json {"loginuser":xxx} or {"errorMsg":"登录失败"}
     */
    @RequestMapping(value = "/login", method=RequestMethod.POST)
    @ResponseBody
    public JSONObject Login(@RequestBody JSONObject params, HttpSession httpsession) throws Exception {

        String userName = params.getString("username");
        String password = params.getString("password");
        logger.info("用户登录: {username={}, password={}}", userName, password);

        JSONObject result = new JSONObject();
        boolean checkPassword = identityService.checkPassword(userName, password);
        if (checkPassword) {
            // read user from database
            User user = identityService.createUserQuery().userId(userName).singleResult();
            UserUtil.saveUserToSession(httpsession, user);
            result.put("loginuser", user);
        }
        else {
            result.put("errorMsg", "登录失败");
        }

        return result;
    }

    /**
     * 用户登出
     * @param httpsession http session
     * @return json {"msg":xxx}
     */
    @RequestMapping(value = "/logout", method=RequestMethod.POST)
    @ResponseBody
    public JSONObject Logout(HttpSession httpsession) {
        httpsession.removeAttribute("user");
        JSONObject result = new JSONObject();
        result.put("msg", "登出成功");
        return result;
    }

    /**
     * 查看当前登录用户
     * @param httpsession http session
     * @return json {"currentUser":xxx}
     */
    @RequestMapping(value = "/currentUser")
    @ResponseBody
    public JSONObject CurrentUser(HttpSession httpsession) {
        User user = UserUtil.getUserFromSession(httpsession);
        JSONObject result = new JSONObject();
        result.put("currentUser", user);
        return result;
    }
}
