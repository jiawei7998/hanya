package com.singlee.slpmd.controller.management;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.activiti.engine.ManagementService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Created by rapix on 2017/4/22.
 * 引擎信息
 */
@Controller
@RequestMapping(value = "/management/engine")
public class ProcessEngineInfoController {

    @Resource
    private ManagementService managementService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public JSONObject info() throws Exception {
        Map<String,String> engineProperties = managementService.getProperties();

        Map<String,String> systemProperties = new HashMap<String, String>();
        Properties systemProperties11 = System.getProperties();
        Set<Object> objects = systemProperties11.keySet();
        for (Object object : objects) {
            systemProperties.put(object.toString(), systemProperties11.get(object.toString()).toString());
        }

        JSONObject result = new JSONObject();
        result.put("engineProperties", JSON.toJSONString(engineProperties));
        result.put("systemProperties", JSON.toJSONString(engineProperties));
        return result;
    }
}
