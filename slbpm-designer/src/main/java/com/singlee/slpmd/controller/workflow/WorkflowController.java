package com.singlee.slpmd.controller.workflow;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slpmd.service.WorkflowService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by rapix on 2017/4/23.
 * 流程实例操作
 */
@Controller
@RequestMapping("/ProcessInstance")
public class WorkflowController {

    private static Logger logger = LoggerFactory.getLogger(WorkflowController.class);

    @Resource
    private RuntimeService runtimeService;

    @Resource
    private WorkflowService workflowService;

    /**
     * 创建新流程实例
     * @param params {"bizId": xxx} 数据对象id
     * @return json
     */
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject startApply(@RequestBody JSONObject params) {
        String bizId = params.getString("bizId");
        return workflowService.startApply(new Integer(bizId));
    }

    /**
     * 挂起、激活流程实例
     * @param state active|suspend 激活|挂起
     * @param processInstanceId 流程实例id
     * @return json
     */
    @RequestMapping(value = "/update/{state}/{processInstanceId}", method= RequestMethod.POST)
    @ResponseBody
    public JSONObject updateState(@PathVariable("state") String state,
                              @PathVariable("processInstanceId") String processInstanceId) {

        JSONObject result = new JSONObject();
        if ("active".equals(state)) {
            runtimeService.activateProcessInstanceById(processInstanceId);
            result.put("message", "已激活ID为[" + processInstanceId + "]的流程实例。");
        } else if ("suspend".equals(state)) {
            runtimeService.suspendProcessInstanceById(processInstanceId);
            result.put("message", "已挂起ID为[" + processInstanceId + "]的流程实例。");
        }
        return result;
    }

    /**
     * 查看所有运行中的流程实例
     * @return json
     */
    @RequestMapping(value = "/running", method= RequestMethod.POST)
    @ResponseBody
    public JSONObject running() {

        List<ProcessInstance> lstRunning = runtimeService.createProcessInstanceQuery().list();
        JSONObject result = new JSONObject();
        result.put("runningList", lstRunning);
        return result;
    }
}
