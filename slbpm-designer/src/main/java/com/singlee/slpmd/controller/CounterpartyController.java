package com.singlee.slpmd.controller;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slpmd.service.CounterPartyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by rapix on 2017/5/18.
 * 机构列表
 */
@Controller
@RequestMapping("/counterparty")
public class CounterpartyController {

    private static Logger logger = LoggerFactory.getLogger(CounterpartyController.class);

    @Resource
    private CounterPartyService counterPartyService;

    @RequestMapping(value = "/fullList", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject fullList() {

        JSONObject result = new JSONObject();

        try {
            result.put("result", counterPartyService.getCounterPartyList());
        }
        catch (Exception e) {
            logger.error("获取对手列表失败：", e);
        }

        return result;
    }

}
