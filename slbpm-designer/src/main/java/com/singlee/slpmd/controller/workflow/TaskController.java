package com.singlee.slpmd.controller.workflow;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.StringUtil;
import com.singlee.slpmd.service.impl.BizDataServiceImpl;
import com.singlee.slpmd.util.UserUtil;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.User;
import org.activiti.engine.task.DelegationState;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by rapix on 2017/4/23.
 * 任务操作
 */
@Controller
@RequestMapping("/task")
public class TaskController {

    @Resource
    private TaskService taskService;

    @Resource
    private RuntimeService runtimeService;

    @Resource
    private BizDataServiceImpl bizDataService;

    @Resource
    private HistoryService historyService;

    @Resource
    private RepositoryService repositoryService;

//    @Resource
//    private TaskUtil taskUtil;
    
    /**
     * 查询用户待办任务列表
     * @return json
     */
    @RequestMapping(value = "/todoList", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject TodoList(HttpSession httpsession) {
        User user = UserUtil.getUserFromSession(httpsession);
//        List<Task> list = taskService.createTaskQuery().taskCandidateOrAssigned(user.getId()).list();
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().taskAssignee(user.getId()).unfinished().list();
        JSONObject result = new JSONObject();
        result.put("groups", list);
        return result;
    }

    /**
     * 查询用户已经完成的任务列表
     * @return json
     */
    @RequestMapping(value ="/finishedList", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject finishedList(HttpSession httpsession) {
        User user = UserUtil.getUserFromSession(httpsession);
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().taskAssignee(user.getId()).finished().list();
//        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().taskAssignee(user.getId()).list();
//        List<Task> todoList = taskService.createTaskQuery().taskCandidateOrAssigned(user.getId()).active().list();
//        List<HistoricTaskInstance> hisList = list;
//        for(int i=0;i<hisList.size();i++){
//            for(int j=0;j<todoList.size();j++){
//                if(Integer.parseInt(hisList.get(i).getId()) == Integer.parseInt(todoList.get(j).getId())){
//                    list.remove(hisList.get(i));
//                    break;
//                }
//            }
//        }

        JSONObject result = new JSONObject();
        result.put("groups", list);
        return result;
    }

    /**
     * 完成（审批）任务
     * @return json 
     */
    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject Approve(@RequestBody JSONObject params, Map<String, Object> variables, HttpSession httpsession) {
        String taskId = params.getString("task_id");
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        JSONObject result = new JSONObject();
        try{
            // 判断该任务是否是被委派的任务，如果是则返回到委派人处
            if (task.getDelegationState() == DelegationState.PENDING) {
                taskService.resolveTask(taskId);
            }
            else {
                taskService.complete(taskId, variables);
            }
        	result.put("groups", "SUCCESS");
        }catch(Exception e){
        	System.out.println(e.getMessage());
        	result.put("groups", e.getCause());
        	return result;
        }
        return result;
    }

    /**
     * 委派任务
     * 被委派人处理完任务后应流转到委派人处
     * @param params {"user_id"：xxx, "task_id": xxx}
     * @return json
     */
    @RequestMapping(value = "/delegate", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject delegate(@RequestBody JSONObject params) {
        String delegateUserId = params.getString("user_id");
        String taskId = params.getString("task_id");
        taskService.delegateTask(taskId, delegateUserId);
        JSONObject result = new JSONObject();
        result.put("return", "SUCCESS");
        return result;
    }
    
    /**
     * 驳回上一节点
     * @return json 
     */
    @RequestMapping(value = "/back", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject back(@RequestBody JSONObject params, Map<String, Object> variables, HttpSession httpsession) {
        String activityId = params.getString("activiti_id");
        String taskId = params.getString("task_id");
    	if (StringUtil.isEmpty(activityId)) {  
            try {
				throw new Exception("驳回目标节点ID为空！");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
        }  
   
         // 带有会签的节点不得驳回
        List<Task> tasks = taskService.createTaskQuery().taskId(taskId).taskDescription("jointProcess").list();  
        if(tasks.size() > 0){
        	try {
 				throw new Exception("带有会签的任务节点无法单方面驳回！");
 			} catch (Exception e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}  
        }
         
        try {
//			taskUtil.turnTransition(taskId, activityId, variables);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
        JSONObject result = new JSONObject();
        result.put("groups", "SUCCESS");
        return result;
    }
    
    /** 
     * 转办流程 
     *  
     * @param taskId 
     * @param userCode 
     */  
    @RequestMapping(value = "/assignee", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject assignee(@RequestBody JSONObject params, HttpSession httpsession) {
        String userId = params.getString("user_id");
        String taskId = params.getString("task_id");
        taskService.setAssignee(taskId, userId);
        JSONObject result = new JSONObject();
        result.put("groups", "SUCCESS");
        return result;
    }

    /**
     * 动态显示任务表单
     */
    @RequestMapping("/view/{taskId}")
    public ModelAndView showTaskView(@PathVariable("taskId") String taskId) {

//        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
//
//        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
//                .processInstanceId(task.getProcessInstanceId()).singleResult();
//
//        BizData bizData = bizDataService.findById(new Integer(processInstance.getBusinessKey()));
//
//        // 动态拼接视图名称 - todo
//        ModelAndView mav = new ModelAndView("" + task.getTaskDefinitionKey());
//        mav.addObject("bizData", bizData);
//        mav.addObject("task", task);
//        return mav;
        return null;
    }
}
