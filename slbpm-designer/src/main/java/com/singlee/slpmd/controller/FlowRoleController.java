package com.singlee.slpmd.controller;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slpmd.service.FlowRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by rapix on 2017/5/18.
 * 机构列表
 */
@Controller
@RequestMapping("/flowRole")
public class FlowRoleController {

    private static Logger logger = LoggerFactory.getLogger(FlowRoleController.class);

    @Resource
    private FlowRoleService flowRoleService;

    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject list() {

        JSONObject result = new JSONObject();

        try {
            result.put("result", flowRoleService.getFlowRoleList());
        }
        catch (Exception e) {
            logger.error("获取角色列表失败：", e);
        }

        return result;
    }

}
