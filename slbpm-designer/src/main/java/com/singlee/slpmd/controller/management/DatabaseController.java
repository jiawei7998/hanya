package com.singlee.slpmd.controller.management;

import com.alibaba.fastjson.JSONObject;
import org.activiti.engine.ManagementService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.*;


/**
 * Created by rapix on 2017/4/23.
 * 数据库信息查询
 */
@Controller
@RequestMapping("/management/database")
public class DatabaseController {

    @Resource
    private ManagementService managementService;

    @RequestMapping("")
    @ResponseBody
    public JSONObject index() throws Exception {

        Map<String, Long> tableCount = managementService.getTableCount();
        List<String> keys = new ArrayList<String>();
        keys.addAll(tableCount.keySet());
        Collections.sort(keys);

        TreeMap<String, Long> sortedTableCount = new TreeMap<String, Long>();

        for (String key : keys) {
            sortedTableCount.put(key, tableCount.get(key));
        }

        JSONObject result = new JSONObject();
        result.put("tableCount", sortedTableCount);
        return result;
    }
}
