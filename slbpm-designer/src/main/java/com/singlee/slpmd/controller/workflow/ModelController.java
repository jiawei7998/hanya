package com.singlee.slpmd.controller.workflow;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by wangfp on 2017/4/25.
 */
@Controller
@RequestMapping("/model")
public class ModelController implements ModelDataJsonConstants{
    // 注入activiti自带的RepositoryService接口
    @Resource
    private RepositoryService repositoryService;

    @Autowired
    private ObjectMapper objectMapper;

    private Logger logger = LoggerFactory.getLogger(ProcessDefinitionController.class);

    /**
     * 模型创建
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/createModel")
    public void create(@RequestParam("name") String name, @RequestParam("key") String key, @RequestParam("description") String description,
                       HttpServletRequest request, HttpServletResponse response) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ObjectNode editorNode = objectMapper.createObjectNode();
            editorNode.put("id", "canvas");
            editorNode.put("resourceId", "canvas");
            ObjectNode stencilSetNode = objectMapper.createObjectNode();
            stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
            editorNode.put("stencilset", stencilSetNode);
            Model modelData = repositoryService.newModel();

            ObjectNode modelObjectNode = objectMapper.createObjectNode();
            modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, name);
            modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
            description = StringUtils.defaultString(description);
            modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, description);
            modelData.setMetaInfo(modelObjectNode.toString());
            modelData.setName(name);
            modelData.setKey(StringUtils.defaultString(key));

            repositoryService.saveModel(modelData);
            repositoryService.addModelEditorSource(modelData.getId(), editorNode.toString().getBytes("UTF-8"));
            response.sendRedirect(request.getContextPath() + "/modeler.jsp?modelId=" + modelData.getId());
        }
        catch (Exception e) {
            logger.error("创建模型失败：", e);
        }
    }

    /**
     * 获取模型列表
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/getModelList", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject getModelList(HttpServletRequest request, HttpServletResponse response) {
        JSONObject result = new JSONObject();
        try {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            // 表明它允许"http://xxx"发起跨域请求
            httpResponse.setHeader("Access-Control-Allow-Origin", "*");
            String message = request.getParameter("message");
            List<Model> list = repositoryService.createModelQuery().list();
            result.put("modelList", list);
            result.put("message", message);
        } catch (Exception e) {
            logger.error("获取模型列表失败：", e);
            result.put("message", "获取模型列表失败");
        }
        return result;
    }

    /**
     * 删除模型
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/deleteModel", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject deleteModel(HttpServletRequest request, HttpServletResponse response) {
        JSONObject result = new JSONObject();
        try {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            // 表明它允许"http://xxx"发起跨域请求
            httpResponse.setHeader("Access-Control-Allow-Origin", "*");
            String modelId = request.getParameter("modelId");
            repositoryService.deleteModel(modelId);
            result.put("message", "删除成功，模型ID=" + modelId);
        } catch (Exception e) {
            logger.error("删除模型失败：", e);
            result.put("message", "删除模型失败");
        }
        return result;
    }

    /**
     * 模型部署
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/deployModel", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject deployModel(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        try {
            String modelId = request.getParameter("modelId");
            Model modelData = repositoryService.getModel(modelId);
            ObjectNode modelNode = (ObjectNode) new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelData.getId()));
            byte[] bpmnBytes = null;

            try{
                BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
                bpmnBytes = new BpmnXMLConverter().convertToXML(model);
            }catch (Exception e){
                logger.error("convertToXML Fail");
                result.put("message", "流程不符合规则，部署失败");
            }

            String processName = modelData.getName() + ".bpmn20.xml";
            Deployment deployment = repositoryService.createDeployment().name(modelData.getName()).addString(processName, new String(bpmnBytes)).deploy();
            result.put("message", "部署成功，部署ID=" + deployment.getId());
            result.put("deploymentId", deployment.getId());
        } catch (Exception e) {
            logger.error("模型部署失败：", e);
            result.put("message", "模型部署失败");
        }
        return result;
    }

    /**
     * 模型编辑列表
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/showModel")
    public void showmodel(@RequestParam("modelId") String modelId,
                          HttpServletRequest request, HttpServletResponse response) {
        try {
            response.sendRedirect(request.getContextPath() + "/modeler.jsp?modelId=" + modelId);
        } catch (Exception e) {
            logger.error("创建模型失败：", e);
        }
    }

    /**
     * 保存模型json数据
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value="/{modelId}/save", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public void saveModel(@PathVariable String modelId, @RequestBody MultiValueMap<String, String> values) {
        try {

            Model model = repositoryService.getModel(modelId);
            ObjectNode modelJson = (ObjectNode) objectMapper.readTree(model.getMetaInfo());

            modelJson.put(MODEL_NAME, values.getFirst("name"));
            modelJson.put(MODEL_DESCRIPTION, values.getFirst("description"));
            model.setMetaInfo(modelJson.toString());
            model.setName(values.getFirst("name"));

            repositoryService.saveModel(model);
            repositoryService.addModelEditorSource(model.getId(), values.getFirst("json_xml").getBytes("UTF-8"));

            BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
            JsonNode editorNode = new ObjectMapper().readTree(repositoryService.getModelEditorSource(model.getId()));
            BpmnModel bpmnModel = jsonConverter.convertToBpmnModel(editorNode);
            BpmnXMLConverter xmlConverter = new BpmnXMLConverter();
            byte[] bpmnBytes = xmlConverter.convertToXML(bpmnModel);
            repositoryService.addModelEditorSourceExtra(modelId, bpmnBytes);

//            String processName = model.getName() + ".bpmn20.xml";
//            Deployment deployment = repositoryService.createDeployment().name(model.getName()).addString(processName, new String(bpmnBytes)).deploy();
//            System.out.println("部署模型成功，部署ID=" + deployment.getId());
        } catch (Exception e) {
            logger.error("Error saving model", e);
            throw new ActivitiException("Error saving model", e);
        }
    }

    /**
     * 获取模型json数据
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value="/{modelId}/json", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ObjectNode getEditorJson(@PathVariable String modelId) {
        ObjectNode modelNode = null;

        Model model = repositoryService.getModel(modelId);

        if (model != null) {
            try {
                if (StringUtils.isNotEmpty(model.getMetaInfo())) {
                    modelNode = (ObjectNode) objectMapper.readTree(model.getMetaInfo());
                } else {
                    modelNode = objectMapper.createObjectNode();
                    modelNode.put(MODEL_NAME, model.getName());
                }
                modelNode.put(MODEL_ID, model.getId());
                ObjectNode editorJsonNode = (ObjectNode) objectMapper.readTree(
                        new String(repositoryService.getModelEditorSource(model.getId()), "utf-8"));
                modelNode.put("model", editorJsonNode);
            } catch (Exception e) {
                logger.error("Error creating model JSON", e);
                throw new ActivitiException("Error creating model JSON", e);
            }
        }
        return modelNode;
    }

    /**
     * 导出model的xml文件
     */
    @RequestMapping(value = "/export/{modelId}")
    public void exportModel(@PathVariable("modelId") String modelId, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            Model modelData = repositoryService.getModel(modelId);
            BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
            //获取节点信息
            byte[] arg0 = repositoryService.getModelEditorSource(modelData.getId());
            JsonNode editorNode = new ObjectMapper().readTree(arg0);
            //将节点信息转换为xml
            
            BpmnModel bpmnModel = jsonConverter.convertToBpmnModel(editorNode);
            BpmnXMLConverter xmlConverter = new BpmnXMLConverter();
            byte[] bpmnBytes = xmlConverter.convertToXML(bpmnModel);

            ByteArrayInputStream in = new ByteArrayInputStream(bpmnBytes);
            String filename = modelData.getName() + ".bpmn20.xml";
            response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(filename, "UTF-8"));
            IOUtils.copy(in, response.getOutputStream());
//                String filename = bpmnModel.getMainProcess().getId() + ".bpmn20.xml";
            response.flushBuffer();
        } catch (Exception e){
            PrintWriter out = null;
            try {
                out = response.getWriter();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            out.write("未找到对应数据");
            e.printStackTrace();
        }
    }

    /**
     * 导入model的xml文件
     */
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    @ResponseBody
    public String importModel(HttpServletRequest request) throws Exception {

    	JSONObject result = new JSONObject();

        try {
            MultipartHttpServletRequest mhs = (MultipartHttpServletRequest)request;
            MultipartFile mf = mhs.getFile("file");
            String fileName = mf.getOriginalFilename();
            InputStream is = mf.getInputStream();

            if (mf != null) {
            	BpmnXMLConverter xmlConverter = new BpmnXMLConverter();
                BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLStreamReader reader = factory.createXMLStreamReader(is);
                BpmnModel bpmnModel = xmlConverter.convertToBpmnModel((XMLStreamReader) reader);
            		
                ObjectMapper objectMapper = new ObjectMapper();
                ObjectNode editorNode = jsonConverter.convertToJson(bpmnModel);
                editorNode.put("id", "canvas");
                editorNode.put("resourceId", "canvas");
                ObjectNode stencilSetNode = objectMapper.createObjectNode();
                stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
                editorNode.put("stencilset", stencilSetNode);
                Model newModel = repositoryService.newModel();

                ObjectNode modelObjectNode = objectMapper.createObjectNode();
                modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, StringUtils.defaultString(fileName+"*upload"));
                modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, 1);
                modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, StringUtils.defaultString(fileName+"*upload"));
                newModel.setMetaInfo(modelObjectNode.toString());
                newModel.setName(fileName+"*upload");
                newModel.setKey(StringUtils.defaultString("model"));

                repositoryService.saveModel(newModel);
                repositoryService.addModelEditorSource(newModel.getId(), editorNode.toString().getBytes("UTF-8"));
                repositoryService.addModelEditorSourceExtra(newModel.getId(), mf.getBytes());

                result.put("message", "id: " + newModel.getId());
                result.put("desc", true);
            }
            return result.toJSONString();
        } catch (Exception e) {
            e.printStackTrace();
            result.put("message", e.getMessage());
            result.put("desc", false);
        }
        return result.toJSONString();
    }
}
