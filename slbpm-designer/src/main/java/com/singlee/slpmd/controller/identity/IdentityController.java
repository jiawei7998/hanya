package com.singlee.slpmd.controller.identity;

import com.alibaba.fastjson.JSONObject;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by rapix on 2017/4/23.
 * 用户管理
 * 角色（用户组）管理
 * 转授权管理
 */
@Controller
@RequestMapping("/management/identity")
public class IdentityController {

    @Resource
    private IdentityService identityService;

    private static final String[] INNER_GROUPS = {"admin", "user", "hr", "deptLeader"};
    private static final String[] INNER_USERS = {"admin", "employee", "hruser", "leaderuser"};

    /**
     * 组列表
     * @return json {"groups":[xxx]}
     */
    @RequestMapping("group/list")
    @ResponseBody
    public JSONObject groupList() {

        List<Group> groupList = identityService.createGroupQuery().list();

        JSONObject result = new JSONObject();
        result.put("groups", groupList);
        return result;
    }

    /**
     * 保存Group
     * @param params {"groupId":xxx,"groupName":xxx,"type":xxx}
     * @return json {"message":xxx}
     */
    @RequestMapping(value = "group/save", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject saveGroup(@RequestBody JSONObject params) {

        String groupId = params.getString("groupId");
        String groupName = params.getString("groupName");
        String type = params.getString("type");

        Group group = identityService.createGroupQuery().groupId(groupId).singleResult();
        if (group == null) {
            group = identityService.newGroup(groupId);
        }
        group.setName(groupName);
        group.setType(type);
        identityService.saveGroup(group);

        JSONObject result = new JSONObject();
        result.put("message", "成功添加组[" + groupName + "]");
        return result;
    }

    /**
     * 删除Group
     * @param groupId 用户组id
     * @return json {"message":xxx}
     */
    @RequestMapping(value = "group/delete/{groupId}")
    @ResponseBody
    public JSONObject deleteGroup(@PathVariable("groupId") String groupId) {

        JSONObject result = new JSONObject();

        if (ArrayUtils.contains(INNER_GROUPS, groupId)) {
            result.put("errorMsg", "组[" + groupId + "]属于Demo固定数据不可删除!");
            return result;
        }

        identityService.deleteGroup(groupId);
        result.put("message", "成功删除组[" + groupId + "]");
        return result;
    }

    /**
     * 用户列表
     * @return json {"userList":[xxx]}
     */
    @RequestMapping("user/list")
    @ResponseBody
    public JSONObject userList() throws Exception {

        UserQuery userQuery = identityService.createUserQuery();
        List<User> userList = userQuery.list();

        JSONObject result = new JSONObject();
        result.put("userList", userList);
        return result;
    }

    /**
     * 保存User
     * @param params {"userId":xxx,"firstName":xxx,"lastName":xxx,"password":xxx,"email":xxx}
     * @return json {"message":xxx}
     */
    @RequestMapping(value = "user/save", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject saveUser(@RequestBody JSONObject params) {

        String userId = params.getString("userId");
        String firstName = params.getString("firstName");
        String lastName = params.getString("lastName");
        String password = params.getString("password");
        String email = params.getString("email");
        User user = identityService.createUserQuery().userId(userId).singleResult();
        if (user == null) {
            user = identityService.newUser(userId);
        }
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        if (StringUtils.isNotBlank(password)) {
            user.setPassword(password);
        }
        identityService.saveUser(user);
        JSONObject result = new JSONObject();
        result.put("message", "成功添加用户[" + firstName + " " + lastName + "]");
        return result;
    }

    /**
     * 删除User
     * @param userId 用户名username
     * @return json {"message":xxx}
     */
    @RequestMapping(value = "user/delete/{username}")
    @ResponseBody
    public JSONObject deleteUser(@PathVariable("username") String userId) {

        JSONObject result = new JSONObject();
        if (ArrayUtils.contains(INNER_USERS, userId)) {
            result.put("errorMsg", "用户[" + userId + "]属于Demo固定数据不可删除!");
            return result;
        }

        identityService.deleteUser(userId);
        result.put("message", "成功删除用户[" + userId + "]");
        return result;
    }

    /**
     * 为用户设置所属组
     * @param params {"userId":xxx,"groupIds":[xxx]}
     * @return json {"message":xxx}
     */
    @RequestMapping(value = "group/set", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject groupForUser(@RequestBody JSONObject params) {

        String userId = params.getString("userId");
        String groupIds = params.getString("groupIds");
        JSONObject result = new JSONObject();
        if (ArrayUtils.contains(INNER_USERS, userId)) {
            result.put("errorMsg", "用户[" + userId + "]属于Demo固定数据不可更改!");
            return result;
        }

        List<Group> groupInDb = identityService.createGroupQuery().groupMember(userId).list();
        for (Group group : groupInDb) {
            identityService.deleteMembership(userId, group.getId());
        }

        List<String> lstGroupIds = JSONObject.parseArray(groupIds, String.class);

        for (String group : lstGroupIds) {
            identityService.createMembership(userId, group);
        }
        result.put("message", "为用户设置所属组成功");
        return result;
    }

    //todo - 转授权管理

    //todo - 检查用户查看流程图的权限
    @RequestMapping(value = "/checkUserRight", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject checkRight(HttpServletRequest request) {

        JSONObject result = new JSONObject();
        String user = request.getParameter("user");
        result.put("flag", true);

        return result;
    }

}
