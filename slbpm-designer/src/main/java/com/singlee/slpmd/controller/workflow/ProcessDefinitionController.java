package com.singlee.slpmd.controller.workflow;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slpmd.controller.services.BaseProcessDefinitionDiagramLayoutResource;
import com.singlee.slpmd.entity.bo.ProcSignSetting;
import com.singlee.slpmd.service.ProcessDefinitionService;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by rapix on 2017/4/23.
 * 流程定义管理
 */
@Controller
@RequestMapping("/processDefinition")
public class ProcessDefinitionController extends BaseProcessDefinitionDiagramLayoutResource {

    // 注入activiti自带的RepositoryService接口
    @Resource
    private RepositoryService repositoryService;

    @Resource
    private ProcessDefinitionService procDefService;

    private Logger logger = LoggerFactory.getLogger(ProcessDefinitionController.class);

     /**
     * 流程定义列表
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/listProcDef")
    public void listProcDef(HttpServletRequest request, HttpServletResponse response) {
        try {
            ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

            RepositoryService repositoryService = processEngine.getRepositoryService();

            List<ProcessDefinition> procDefList = repositoryService.createProcessDefinitionQuery()
                    .orderByProcessDefinitionId().desc().list();
            request.setAttribute("procDefList", procDefList);
            request.getRequestDispatcher("/procdef.jsp").forward(request, response);
        } catch (Exception e) {
            logger.error("获取流程定义列表失败：", e);
        }
    }

    /**
     * 挂起、激活流程定义
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/updateProcessDefinitionState", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject updateProcessDefinitionState(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        try {
            String state = request.getParameter("state");
            String processDefinitionId = request.getParameter("processDefinitionId");

            if ("active".equals(state)) {
                repositoryService.activateProcessDefinitionById(processDefinitionId, true, null);
                result.put("message", "已激活ID为[" + processDefinitionId + "]的流程定义。");
            } else if ("suspend".equals(state)) {
                repositoryService.suspendProcessDefinitionById(processDefinitionId, true, null);
                result.put("message", "已挂起ID为[" + processDefinitionId + "]的流程定义。");
            }
        } catch (Exception e) {
            logger.error("挂起、激活流程定义失败：", e);
            result.put("message", "挂起、激活流程定义失败");
        }
        return result;
    }

    /**
     * 删除流程定义
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/deleteProcessDefinitionState", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject deleteProcessDefinitionState(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        try {
            String deploymentId = request.getParameter("deploymentId");
            repositoryService.deleteDeployment(deploymentId, true);
            result.put("message", "已删除该流程定义");
        } catch (Exception e) {
            logger.error("删除流程定义失败：", e);
            result.put("message", "删除流程定义失败");
        }
        return result;
    }


    /**
     * 开始（审批）任务
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/startProcess", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject startProcess(HttpServletRequest request) {
        JSONObject result = new JSONObject();
        try {
            String processDefinitionId = request.getParameter("processDefinitionId");
            ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
            RuntimeService runtimeService = processEngine.getRuntimeService();
            runtimeService.startProcessInstanceById(processDefinitionId);
            result.put("message", "开始流程实例成功");
        } catch (Exception e) {
            logger.error("开始流程实例失败：", e);
            result.put("message", "开始流程实例失败");
        }
        return result;
    }

    /**
     * 流程实例列表
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/listProcInst")
    private void listProcInst(HttpServletRequest request,
                              HttpServletResponse response) throws ServletException, IOException {
        String processDefinitionId = new String(request.getParameter("pdfid").getBytes("iso-8859-1"), "utf-8");

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        HistoryService historyService = processEngine.getHistoryService();

        List<HistoricProcessInstance> procInstList = historyService.createHistoricProcessInstanceQuery()
                .processDefinitionId(processDefinitionId).list();

        request.setAttribute("procInstList", procInstList);
        request.getRequestDispatcher("/procinst.jsp").forward(request, response);
    }

    /**
     * 流程跟踪
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/showDiagramViewer")
    public void showdiagramviewer(HttpServletRequest request, HttpServletResponse response) {
        try {
            String processDefinitionId = request.getParameter("processDefinitionId");
            String processInstanceId = request.getParameter("processInstanceId");
            response.sendRedirect(request.getContextPath() + "/diagram-viewer/index.html?processDefinitionId="+processDefinitionId+"&processInstanceId="+processInstanceId);
        } catch (Exception e) {
            logger.error("流程跟踪失败：", e);
        }
    }

    @RequestMapping(value = "/setSignSetting", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject setSignSetting(@RequestBody JSONObject params) {

        JSONObject result = new JSONObject();

        try {

            String task_def_key = params.getString("task_def_key");
            String decision_type = params.getString("decision_type");
            String vote_type = params.getString("vote_type");
            String condition = params.getString("condition");
            procDefService.setSignSetting(task_def_key, decision_type, vote_type, condition);
            result.put("message", "流程会签节点设置成功");
        }
        catch (Exception e) {

            logger.error("流程会签节点设置失败：", e);
            result.put("message", "流程会签节点设置失败");
        }
        return result;
    }

    @RequestMapping(value = "/getSignSetting", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject getSignSetting(@RequestBody JSONObject params) {

        JSONObject result = new JSONObject();

        try {
            String task_def_key = params.getString("task_def_key");
            ProcSignSetting procSignSetting = procDefService.getSignSetting(task_def_key);
            if (procSignSetting != null) {
                result.put("result", procSignSetting);
            }
            result.put("message", "获取流程会签节点设置成功");
        }
        catch (Exception e) {

            logger.error("获取流程会签节点设置失败：", e);
            result.put("message", "获取流程会签节点设置失败");
        }
        return result;
    }
}
