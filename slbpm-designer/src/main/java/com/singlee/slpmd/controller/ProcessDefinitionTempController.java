package com.singlee.slpmd.controller;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.singlee.slpmd.controller.services.BaseProcessDefinitionDiagramLayoutResource;
import com.singlee.slpmd.entity.vo.ProcDefInfoVo;
import com.singlee.slpmd.service.ProcessDefinitionService;
import org.activiti.bpmn.exceptions.XMLException;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 杨阳 on 2017/5/3. 流程定义管理
 */
@Controller
@RequestMapping("/processDefinition_temp")
public class ProcessDefinitionTempController extends BaseProcessDefinitionDiagramLayoutResource {

	private static Logger logger = LoggerFactory.getLogger(ProcessDefinitionTempController.class);

	@Resource
	private ProcessDefinitionService processDefinitionService;

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	ProcessEngineConfiguration processEngineConfiguration;

	@Autowired
	ProcessEngineFactoryBean processEngine;

	/**
	 * 新建流程定义（模型部署）
	 */
	@RequestMapping(value = "/deployModel", method = RequestMethod.POST)
	@ResponseBody
	public JSONObject deployModel(HttpServletRequest request, HttpServletResponse response) {

		HttpServletResponse httpResponse = (HttpServletResponse) response;
		// 表明它允许"http://xxx"发起跨域请求
		httpResponse.setHeader("Access-Control-Allow-Origin", "*");

		String model_id = request.getParameter("model_id");
		String proc_def_info = request.getParameter("proc_def_info");

		ProcDefInfoVo procDefInfoVo = JSONObject.parseObject(proc_def_info, ProcDefInfoVo.class);

		JSONObject obj = new JSONObject();

		try {
			obj = processDefinitionService.addNewProcDef(model_id, procDefInfoVo);
		} catch (XMLException xe) {
			obj.put("message", "模型不符合设计规则 - " + xe.getMessage());
		} catch (Exception e) {
			obj.put("message", "模型部署失败 - " + e.getMessage());
		}

		return obj;
	}

	/**
	 * 修改流程定义
	 */
	@RequestMapping(value = "/modifyProcDef", method = RequestMethod.POST)
	@ResponseBody
	public JSONObject modifyProcDef(HttpServletRequest request, HttpServletResponse response) {
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		// 表明它允许"http://xxx"发起跨域请求
		httpResponse.setHeader("Access-Control-Allow-Origin", "*");
		String proc_def_info = request.getParameter("proc_def_info");
		ProcDefInfoVo procDefInfoVo = JSONObject.parseObject(proc_def_info, ProcDefInfoVo.class);

		return processDefinitionService.modifyProcDef(procDefInfoVo);
	}

	/**
	 * 流程定义列表
	 */
	@RequestMapping(value = "/listProcDef", method = RequestMethod.POST)
	@ResponseBody
	public JSONObject listProcDef(HttpServletRequest request, HttpServletResponse response) {

		JSONObject result = new JSONObject();
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		// 表明它允许"http://xxx"发起跨域请求
		httpResponse.setHeader("Access-Control-Allow-Origin", "*");

		try {
			String flow_name = request.getParameter("flow_name");
			String flow_type = request.getParameter("flow_type");
			String flow_belong = request.getParameter("flow_belong");
			String product_type = request.getParameter("product_type");
			String trd_type = request.getParameter("trd_type");
			String is_active = request.getParameter("is_active");

			List<ProcDefInfoVo> procDefList = processDefinitionService.listProcDef(flow_name, flow_type, flow_belong,
					product_type, trd_type, is_active);

			result.put("result", procDefList);
		} catch (Exception e) {
			logger.error("获取流程定义列表失败：", e);
		}

		return result;
	}

	/**
	 * 流程定义
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "showProcessDefinitionImage")
	public void showProcessDefinitionImage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String processDefinitionId = request.getParameter("processDefinitionId");
		request.setAttribute("processDefinitionId", processDefinitionId);
		request.getRequestDispatcher("/jsp/process/showProcessDefinitionImage.jsp").forward(request, response);
	}

	/**
	 * 读取带跟踪的图片信息
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/getProcessDefinitionInfo")
	public void getProcessDefinitionInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// processDefinitionId
		String processDefinitionId = request.getParameter("processDefinitionId");
		// 获取流程图
		BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);
		processEngineConfiguration = processEngine.getProcessEngineConfiguration();
		Context.setProcessEngineConfiguration((ProcessEngineConfigurationImpl) processEngineConfiguration);

		ProcessDiagramGenerator diagramGenerator = processEngineConfiguration.getProcessDiagramGenerator();

		// 高亮环节id集合
		List<String> highLightedActivitis = new ArrayList<String>();
		// 高亮线路id集合
		List<String> highLightedFlows = new ArrayList<String>();
		// 中文显示的是口口口，设置字体就好了
		InputStream imageStream = diagramGenerator.generateDiagram(bpmnModel, "png", highLightedActivitis,
				highLightedFlows, "宋体", "宋体", "宋体", null, 1.0);
		// 输出资源内容到相应对象
		byte[] b = new byte[1024];
		int len;
		while ((len = imageStream.read(b, 0, 1024)) != -1) {
			response.getOutputStream().write(b, 0, len);
		}
	}

	/**
	 * 流程跟踪
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "showProcessInstanceImage")
	public void showProcessInstanceImage(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String processInstanceId = request.getParameter("processInstanceId");
		String processDefinitionId = request.getParameter("processDefinitionId");
		request.setAttribute("processInstanceId", processInstanceId);
		request.setAttribute("processDefinitionId", processDefinitionId);
		request.getRequestDispatcher("/jsp/process/showProcessInstanceImage.jsp").forward(request, response);
	}

	/**
	 * 读取带跟踪的图片信息
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/getProcessInstanceInfo")
	public void getProcessInstanceInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// processInstanceId
		String processInstanceId = request.getParameter("processInstanceId");
		// 获取历史流程实例
		HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
				.processInstanceId(processInstanceId).singleResult();
		// 获取流程图
		BpmnModel bpmnModel = repositoryService.getBpmnModel(processInstance.getProcessDefinitionId());
		processEngineConfiguration = processEngine.getProcessEngineConfiguration();
		Context.setProcessEngineConfiguration((ProcessEngineConfigurationImpl) processEngineConfiguration);

		ProcessDiagramGenerator diagramGenerator = processEngineConfiguration.getProcessDiagramGenerator();

		JSONObject result = processDefinitionService.getProcessInstanceInfo(processInstanceId, processInstance);
		// 中文显示的是口口口，设置字体就好了
		InputStream imageStream = diagramGenerator.generateDiagram(bpmnModel, "png",
				(List<String>) result.get("highLightedActivitis"), (List<String>) result.get("highLightedFlows"), "宋体",
				"宋体", "宋体", null, 1.0);
		// 单独返回流程图，不高亮显示
//        InputStream imageStream = diagramGenerator.generatePngDiagram(bpmnModel);
		// 输出资源内容到相应对象
		byte[] b = new byte[1024];
		int len;
		while ((len = imageStream.read(b, 0, 1024)) != -1) {
			response.getOutputStream().write(b, 0, len);
		}
	}

	/**
	 * 获取流程节点详细信息
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getActivitisAndFlows", method = RequestMethod.POST)
	@ResponseBody
	public JSONObject getActivitisAndFlows(HttpServletRequest request) {

		JSONObject result = new JSONObject();
		String processDefinitionId = request.getParameter("processDefinitionId");
		ObjectNode objectNode = getDiagramNode(null, processDefinitionId);
		ArrayList<JsonNode> activityList = new ArrayList<JsonNode>();
		for (JsonNode node : objectNode.get("activities")) {
			activityList.add(node);
		}
		result.put("activitiList", activityList);
		result.put("sequenceFlows", objectNode.get("sequenceFlows"));
		return result;

	}

	@RequestMapping(value = "/flowDictList", method = RequestMethod.POST)
	@ResponseBody
	public JSONObject getFlowDictList(HttpServletRequest request, HttpServletResponse response) {
		JSONObject result = new JSONObject();
		result = processDefinitionService.getFlowDictList();
		return result;
	}
}
