package com.singlee.slpmd.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slpmd.entity.BizData;
import com.singlee.slpmd.service.BizDataService;
import com.singlee.slpmd.service.WorkflowService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rapix on 2017/4/24.
 * 流程实例管理业务层
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class WorkflowServiceImpl implements WorkflowService {

    private static Logger logger = LoggerFactory.getLogger(WorkflowServiceImpl.class);

    @Resource
    private BizDataService bizDataService;

    @Resource
    private IdentityService identityService;

    @Resource
    private RuntimeService runtimeService;

    @Resource
    private TaskService taskService;

    @Override
    public JSONObject startApply(Integer bizId) {

        Map<String,Object> variables = new HashMap<String,Object>();
        variables.put("bizId", bizId);

        // 启动流程
        ProcessInstance pi= runtimeService.startProcessInstanceByKey("process", variables);

        // 根据流程实例Id查询任务
        Task task=taskService.createTaskQuery().processInstanceId(pi.getProcessInstanceId()).singleResult();

        // 完成 填写审批单任务
        taskService.complete(task.getId());

        BizData bizData = bizDataService.findById(bizId);

        //修改状态
        bizData.setState("审核中");
        bizData.setProcessInstanceId(pi.getProcessInstanceId());
        // 修改请假单状态
        bizDataService.update(bizData);

        JSONObject result=new JSONObject();
        result.put("success", true);
        return result;
    }
}
