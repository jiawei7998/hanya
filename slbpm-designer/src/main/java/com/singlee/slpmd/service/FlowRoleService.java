package com.singlee.slpmd.service;


import com.singlee.slpmd.entity.bo.TtFlowRole;

import java.util.List;

/**
 * 审批角色管理，包括预授权管理
 * @author Libo
 *
 */
public interface FlowRoleService {
	List<TtFlowRole> getFlowRoleList();
}
