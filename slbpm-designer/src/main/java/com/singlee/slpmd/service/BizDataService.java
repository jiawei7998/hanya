package com.singlee.slpmd.service;

import com.singlee.slpmd.entity.BizData;

/**
 * Created by rapix on 2017/4/26.
 * 业务表单管理接口
 */
public interface BizDataService {

    BizData findById(Integer id);

    void save(BizData entity);

    void update(BizData entity);

}
