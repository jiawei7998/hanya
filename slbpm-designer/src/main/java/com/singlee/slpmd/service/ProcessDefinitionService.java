package com.singlee.slpmd.service;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slpmd.entity.bo.ProcSignSetting;
import com.singlee.slpmd.entity.vo.ProcDefInfoVo;
import org.activiti.engine.history.HistoricProcessInstance;

import java.util.List;

/**
 * Created by 杨阳 on 2017/5/3. 流程定义管理接口 - DEMO
 */
public interface ProcessDefinitionService {

	/**
	 * 新增流程定义（部署模型） - 演示用
	 * 
	 * @param model_id     模型ID
	 * @param flow_name    流程名称
	 * @param flow_type    流程类型
	 * @param flow_belong  机构
	 * @param product_type 产品类型
	 * @param trd_type     业务类型
	 * @param is_active    是否启用
	 * @return JSONObject
	 */
	JSONObject addNewProcDef(String model_id, ProcDefInfoVo pdInfoVo) throws Exception;

	/**
	 * 修改流程定义 - 演示用
	 * 
	 * @param pd_id        流程定义ID
	 * @param flow_name    流程名称
	 * @param flow_type    流程类型
	 * @param flow_belong  机构
	 * @param product_type 产品类型
	 * @param trd_type     业务类型
	 * @param is_active    是否启用
	 * @return JSONObject
	 */
	JSONObject modifyProcDef(ProcDefInfoVo procDefInfoVo);

	/**
	 * 查询流程定义列表 - 演示用
	 * 
	 * @param flow_name    流程名称
	 * @param flow_type    流程类型
	 * @param flow_belong  机构
	 * @param product_type 产品类型
	 * @param trd_type     业务类型
	 * @param is_active    是否启用
	 * @return 流程定义对象列表
	 */
	List<ProcDefInfoVo> listProcDef(String flow_name, String flow_type, String flow_belong, String product_type,
                                    String trd_type, String is_active);

	/**
	 * 读取带跟踪的图片信息
	 * 
	 * @param processInstanceId
	 */
	JSONObject getProcessInstanceInfo(String processInstanceId, HistoricProcessInstance processInstance);

	/**
	 * 保存流程会签节点设置
	 * 
	 * @param task_def_key  节点定义KEY
	 * @param decision_type 决策方式 0-拒绝 1-通过
	 * @param vote_type     投票类型 0-绝对票数 1-百分比
	 * @param condition     通过条件
	 */
	void setSignSetting(String task_def_key, String decision_type, String vote_type, String condition);

	/**
	 * 获得流程会签节点设置
	 * 
	 * @param task_def_key 节点定义KEY
	 * @return {task_def_key, decision_type, vote_type, condition}
	 */
	ProcSignSetting getSignSetting(String task_def_key);

	/**
	 * 获取流程中静态参数字典
	 * 
	 * @return
	 */
	JSONObject getFlowDictList();
}
