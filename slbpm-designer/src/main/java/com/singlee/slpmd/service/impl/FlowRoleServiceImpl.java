package com.singlee.slpmd.service.impl;

import com.singlee.slpmd.dao.TaUserMapper;
import com.singlee.slpmd.dao.TtFlowRoleMapper;
import com.singlee.slpmd.dao.TtFlowRoleUserMapMapper;
import com.singlee.slpmd.entity.bo.TaUser;
import com.singlee.slpmd.entity.bo.TtFlowRole;
import com.singlee.slpmd.entity.bo.TtFlowRoleUserMap;
import com.singlee.slpmd.service.FlowRoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * 流程角色管理Service
 * @author Libo
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FlowRoleServiceImpl implements FlowRoleService {

	@Resource
	private TtFlowRoleMapper ttFlowRoleMapper;

    @Resource
    private TtFlowRoleUserMapMapper ttFlowRoleUserMapMapper;

    @Resource
    private TaUserMapper taUserMapper;
    
    @Override
	public List<TtFlowRole> getFlowRoleList() {

        List<TtFlowRole> lstFlowRole = ttFlowRoleMapper.selectAll();
        List<TtFlowRoleUserMap> lstFlowRoleUserMap = ttFlowRoleUserMapMapper.selectAll();
        List<TaUser> lstUser = taUserMapper.selectAll();

        for (TtFlowRole flowRole: lstFlowRole) {

            String flowRoleId = flowRole.getRole_id();

            List<String> lstUserId = new ArrayList<String>();
            List<String> lstUserName = new ArrayList<String>();

            for (TtFlowRoleUserMap flowRoleUserMap: lstFlowRoleUserMap) {

                if (flowRoleId.equals(flowRoleUserMap.getRole_id()))
                {
                    String user_id = flowRoleUserMap.getUser_id();
                    lstUserId.add(user_id);

                    for (TaUser user : lstUser) {

                        if (user_id.equals(user.getUserId()))
                        {
                            lstUserName.add(user.getUserName());
                        }
                    }
                }
            }

            flowRole.setUser_list(StringUtils.join(lstUserId, ","));
            flowRole.setUser_name_list(StringUtils.join(lstUserName, ","));
        }

        return lstFlowRole;
	}

}
