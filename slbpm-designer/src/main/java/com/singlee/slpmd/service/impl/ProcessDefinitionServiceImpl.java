package com.singlee.slpmd.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.slpmd.dao.ProcessDefinitionInfoMapper;
import com.singlee.slpmd.dao.SignSettingDao;
import com.singlee.slpmd.dao.TtFlowDefinePropertyMapper;
import com.singlee.slpmd.entity.bo.FlowDict;
import com.singlee.slpmd.entity.bo.ProcDefInfo;
import com.singlee.slpmd.entity.bo.ProcSignSetting;
import com.singlee.slpmd.entity.bo.TtFlowDefineProperty;
import com.singlee.slpmd.entity.vo.ProcDefInfoVo;
import com.singlee.slpmd.service.ProcessDefinitionService;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 杨阳 on 2017/5/3. 流程定义管理实现 - DEMO
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProcessDefinitionServiceImpl implements ProcessDefinitionService {

	private static Logger logger = LoggerFactory.getLogger(ProcessDefinitionServiceImpl.class);

	/** 注入activiti自带的RepositoryService接口 */
	@Resource
	private RepositoryService repositoryService;

	@Resource
	private HistoryService historyService;

	/** 流程定义 dao */
	@Resource
	private ProcessDefinitionInfoMapper processDefinitionInfoMapper;

	@Resource
	private TtFlowDefinePropertyMapper ttFlowDefinePropertyMapper;

	@Resource
	ProcessEngine processEngine;

	/** 批量 dao */
	@Resource
	private BatchDao batchDao;

	@Resource
	private SignSettingDao signSettingDao;

	/**
	 * 新增流程定义（部署模型） - 演示用
	 * 
	 * @param model_id     模型ID
	 * @param pdInfoVo    流程名称
	 * @return JSONObject
	 */
	@Override
	public JSONObject addNewProcDef(String model_id, ProcDefInfoVo pdInfoVo) throws Exception {

		JSONObject result = new JSONObject();

		Model modelData = repositoryService.getModel(model_id);
		ObjectNode modelNode = (ObjectNode) new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelData.getId()));

		BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
		byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);
		String bpmnString = new String(bpmnBytes, "UTF-8");
		String processName = modelData.getName() + ".bpmn20.xml";

		Deployment deployment = repositoryService.createDeployment().name(modelData.getName())
				.addString(processName, bpmnString).deploy();

		ProcessDefinition proc_def = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId())
				.singleResult();

		String pd_id = proc_def.getId();
		String pd_version = String.valueOf(proc_def.getVersion());

		// 挂起、激活流程定义
		System.out.println(proc_def.isSuspended());
		if ("1".equals(pdInfoVo.getIs_active()) && proc_def.isSuspended()) {
			repositoryService.activateProcessDefinitionById(pd_id, true, null);
//            result.put("message", "已激活ID为[" + pd_id + "]的流程定义。");
		} else if ("0".equals(pdInfoVo.getIs_active()) && !proc_def.isSuspended()) {
			repositoryService.suspendProcessDefinitionById(pd_id, true, null);
//            result.put("message", "已挂起ID为[" + pd_id + "]的流程定义。");
		}

		// 保存流程定义配置信息到业务表
		ProcDefInfo pd_info = new ProcDefInfo();
		pd_info.setFlow_id(pd_id);
		pd_info.setVersion(pd_version);
		pd_info.setFlow_name(pdInfoVo.getFlow_name());
		pd_info.setFlow_type(pdInfoVo.getFlow_type());
		pd_info.setIs_active(pdInfoVo.getIs_active());
		processDefinitionInfoMapper.insert(pd_info);

		pdInfoVo.setFlow_id(pd_id);
		pdInfoVo.setVersion(pd_version);

		result.put("message", "部署成功，部署ID=" + deployment.getId());
		return result;
	}

	/**
	 * 添加流程维度
	 */
	private void saveFlowProperty(ProcDefInfoVo pdInfoVo) {

		String flow_id = pdInfoVo.getFlow_id();
		String version = pdInfoVo.getVersion();
		List<TtFlowDefineProperty> list = new ArrayList<TtFlowDefineProperty>();

		// 机构维度
		this.addFlowPropertyList(list, pdInfoVo.getFlow_belong(), pdInfoVo.getFlow_belong_text(), flow_id, version,
				"flow_belong");

		// 交易类型维度
		this.addFlowPropertyList(list, pdInfoVo.getTrd_type(), pdInfoVo.getTrd_type_text(), flow_id, version,
				"trd_type");

		// 产品类型维度
		this.addFlowPropertyList(list, pdInfoVo.getProduct_type(), pdInfoVo.getProduct_type_text(), flow_id, version,
				"product_type");

		// 批量添加
		batchDao.batch("com.singlee.slpmd.dao.TtFlowDefinePropertyMapper.insert", list);
	}

	/**
	 * 添加流程维度
	 * 
	 * @param  list 存放值的List
	 * @param  propertyNameEn 维度英文名
	 * @param  propertyNameCn 维度中文名
	 * @param  flowId 流程ID
	 * @param  version 版本
	 * @param  propertyType 类型
	 */
	private void addFlowPropertyList(List<TtFlowDefineProperty> list, String propertyNameEn, String propertyNameCn,
                                     String flowId, String version, String propertyType) {

		String[] propertyNameEnArray = propertyNameEn.split(",");
		String[] propertyNameCnArray = propertyNameCn.split(",");

		for (int i = 0; i < propertyNameEnArray.length; i++) {

			TtFlowDefineProperty ttFlowDefineProperty = new TtFlowDefineProperty();
			ttFlowDefineProperty.setFlow_id(flowId);
			ttFlowDefineProperty.setVersion(version);
			ttFlowDefineProperty.setProperty_type(propertyType);

			if (propertyNameEnArray.length == 1 && StringUtil.isEmpty(propertyNameEnArray[0])) {
				ttFlowDefineProperty.setProperty_code("*");
				ttFlowDefineProperty.setProperty_name("全部");
			} else {
				ttFlowDefineProperty.setProperty_code(propertyNameEnArray[i]);
				ttFlowDefineProperty.setProperty_name(propertyNameCnArray[i]);
			}

			list.add(ttFlowDefineProperty);
		}
	}

	/**
	 * 修改流程定义 - 演示用
	 * 
	 * @param procDefInfoVo        流程定义ID
	 * @return JSONObject
	 */
	@Override
	public JSONObject modifyProcDef(ProcDefInfoVo procDefInfoVo) {
		JSONObject result = new JSONObject();
		try {
			String pd_id = procDefInfoVo.getFlow_id();
			String flow_name = procDefInfoVo.getFlow_name();
			String flow_type = procDefInfoVo.getFlow_type();
			String version = procDefInfoVo.getVersion();
			String is_active = procDefInfoVo.getIs_active();
			ProcessDefinition proc_def = repositoryService.createProcessDefinitionQuery().processDefinitionId(pd_id)
					.singleResult();
			// 挂起、激活流程定义
			if ("1".equals(is_active) && proc_def.isSuspended()) {
				repositoryService.activateProcessDefinitionById(pd_id, true, null);
				result.put("message", "已激活ID为[" + pd_id + "]的流程定义。");
			} else if ("0".equals(is_active) && !proc_def.isSuspended()) {
				repositoryService.suspendProcessDefinitionById(pd_id, true, null);
				result.put("message", "已挂起ID为[" + pd_id + "]的流程定义。");
			}
			ProcDefInfo pd_info = new ProcDefInfo();
			pd_info.setFlow_id(pd_id);
			pd_info.setFlow_name(flow_name);
			pd_info.setFlow_type(flow_type);
			pd_info.setVersion(version);
			pd_info.setIs_active(is_active);
			processDefinitionInfoMapper.updateByPrimaryKeySelective(pd_info);
			// 批量更新
//            Map<String, String> map = new HashMap<String, String>();
//            map.put("flow_id", pd_id);
//            map.put("version", version);
//            ttFlowDefinePropertyMapper.batchDeleteProps(map);
//            this.saveFlowProperty(procDefInfoVo);

			result.put("message", "更新成功，流程定义ID=" + pd_id);
		} catch (Exception e) {
			logger.error("更新流程定义失败：", e);
			result.put("message", "更新流程定义失败");
		}

		return result;
	}

	/**
	 * 查询流程定义列表 - 演示用
	 * 
	 * @param flow_name    流程名称
	 * @param flow_type    流程类型
	 * @param flow_belong  机构
	 * @param product_type 产品类型
	 * @param trd_type     业务类型
	 * @param is_active    是否启用
	 * @return 流程定义对象列表
	 */
	@Override
	public List<ProcDefInfoVo> listProcDef(String flow_name, String flow_type, String flow_belong, String product_type,
                                           String trd_type, String is_active) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("flow_name", flow_name);
		map.put("flow_type", flow_type);
		map.put("is_active", is_active);
		map.put("flow_belong", flow_belong);
		map.put("product_type", product_type);
		map.put("trd_type", trd_type);

		List<ProcDefInfoVo> lst_procDefInfo = processDefinitionInfoMapper.searchProcDef(map);

		for (int i = 0; i < lst_procDefInfo.size(); i++) {

			TtFlowDefineProperty ttFlowDefineProperty = new TtFlowDefineProperty();
			ttFlowDefineProperty.setFlow_id(lst_procDefInfo.get(i).getFlow_id());
			ttFlowDefineProperty.setVersion(lst_procDefInfo.get(i).getVersion());

			List<TtFlowDefineProperty> list = ttFlowDefinePropertyMapper.select(ttFlowDefineProperty);

			String flowBelong = "";
			String flowBelongText = "";
			String trdType = "";
			String trdTypeText = "";
			String productType = "";
			String productTypeText = "";

			for (TtFlowDefineProperty item : list) {
				if ("trd_type".equals(item.getProperty_type())) {
					trdType = trdType + item.getProperty_code() + ",";
					trdTypeText = trdTypeText + item.getProperty_name() + ",";
				} else if ("product_type".equals(item.getProperty_type())) {
					productType = productType + item.getProperty_code() + ",";
					productTypeText = productTypeText + item.getProperty_name() + ",";
				} else if ("flow_belong".equals(item.getProperty_type())) {
					flowBelong = flowBelong + item.getProperty_code() + ",";
					flowBelongText = flowBelongText + item.getProperty_name() + ",";
				}
			}
			lst_procDefInfo.get(i).setTrd_type(trdType);
			lst_procDefInfo.get(i).setTrd_type_text(trdTypeText);
			lst_procDefInfo.get(i).setProduct_type(productType);
			lst_procDefInfo.get(i).setProduct_type_text(productTypeText);
			lst_procDefInfo.get(i).setFlow_belong(flowBelong);
			lst_procDefInfo.get(i).setFlow_belong_text(flowBelongText);
			lst_procDefInfo.get(i).setPropertyList(list);
		}

		return lst_procDefInfo;
	}

	/**
	 * 读取带跟踪的图片信息
	 * 
	 * @param processInstanceId
	 */
	@Override
	public JSONObject getProcessInstanceInfo(String processInstanceId, HistoricProcessInstance processInstance) {
		JSONObject result = new JSONObject();
		ProcessDefinitionEntity definitionEntity = (ProcessDefinitionEntity) repositoryService
				.getProcessDefinition(processInstance.getProcessDefinitionId());
		List<HistoricActivityInstance> highLightedActivitList = historyService.createHistoricActivityInstanceQuery()
				.processInstanceId(processInstanceId).list();
		// 高亮环节id集合
		List<String> highLightedActivitis = new ArrayList<String>();
		// 高亮线路id集合
		List<String> highLightedFlows = getHighLightedFlows(definitionEntity, highLightedActivitList);
		for (HistoricActivityInstance tempActivity : highLightedActivitList) {
			String activityId = tempActivity.getActivityId();
			highLightedActivitis.add(activityId);
		}
		result.put("highLightedActivitis", highLightedActivitis);
		result.put("highLightedFlows", highLightedFlows);
		return result;
	}

	/**
	 * 获取需要高亮的线
	 * 
	 * @param processDefinitionEntity
	 * @param historicActivityInstances
	 * @return
	 */
	private List<String> getHighLightedFlows(ProcessDefinitionEntity processDefinitionEntity,
			List<HistoricActivityInstance> historicActivityInstances) {
		List<String> highFlows = new ArrayList<String>();// 用以保存高亮的线flowId
		for (int i = 0; i < historicActivityInstances.size() - 1; i++) {// 对历史流程节点进行遍历
			ActivityImpl activityImpl = processDefinitionEntity
					.findActivity(historicActivityInstances.get(i).getActivityId());// 得到节点定义的详细信息
			List<ActivityImpl> sameStartTimeNodes = new ArrayList<ActivityImpl>();// 用以保存后需开始时间相同的节点
			ActivityImpl sameActivityImpl1 = processDefinitionEntity
					.findActivity(historicActivityInstances.get(i + 1).getActivityId());
			// 将后面第一个节点放在时间相同节点的集合里
			sameStartTimeNodes.add(sameActivityImpl1);
			for (int j = i + 1; j < historicActivityInstances.size() - 1; j++) {
				HistoricActivityInstance activityImpl1 = historicActivityInstances.get(j);// 后续第一个节点
				HistoricActivityInstance activityImpl2 = historicActivityInstances.get(j + 1);// 后续第二个节点
				if (activityImpl1.getStartTime().equals(activityImpl2.getStartTime())) {
					// 如果第一个节点和第二个节点开始时间相同保存
					ActivityImpl sameActivityImpl2 = processDefinitionEntity
							.findActivity(activityImpl2.getActivityId());
					sameStartTimeNodes.add(sameActivityImpl2);
				} else {
					// 有不相同跳出循环
					break;
				}
			}
			List<PvmTransition> pvmTransitions = activityImpl.getOutgoingTransitions();// 取出节点的所有出去的线
			for (PvmTransition pvmTransition : pvmTransitions) {
				// 对所有的线进行遍历
				ActivityImpl pvmActivityImpl = (ActivityImpl) pvmTransition.getDestination();
				// 如果取出的线的目标节点存在时间相同的节点里，保存该线的id，进行高亮显示
				if (sameStartTimeNodes.contains(pvmActivityImpl)) {
					highFlows.add(pvmTransition.getId());
				}
			}
		}
		return highFlows;
	}

	/**
	 * 流程会签节点设置
	 * 
	 * @param task_def_key  节点定义KEY
	 * @param decision_type 决策方式 0-拒绝 1-通过
	 * @param vote_type     投票类型 0-绝对票数 1-百分比
	 * @param condition     通过条件
	 */
	@Override
	public void setSignSetting(String task_def_key, String decision_type, String vote_type, String condition) {
		ProcSignSetting procSignSetting = signSettingDao.selectOneByTaskDefKey(task_def_key);
		if (procSignSetting == null) {
			procSignSetting = new ProcSignSetting();
			procSignSetting.setTask_def_key(task_def_key);
			procSignSetting.setDecision_type(decision_type);
			procSignSetting.setVote_type(vote_type);
			procSignSetting.setCondition(condition);
			signSettingDao.save(procSignSetting);
		} else {
			procSignSetting.setDecision_type(decision_type);
			procSignSetting.setVote_type(vote_type);
			procSignSetting.setCondition(condition);
			signSettingDao.update(procSignSetting);
		}
	}

	/**
	 * 获得流程会签节点设置
	 * 
	 * @param task_def_key 节点定义KEY
	 * @return ProcSignSetting
	 */
	@Override
	public ProcSignSetting getSignSetting(String task_def_key) {
		return signSettingDao.selectOneByTaskDefKey(task_def_key);
	}

	@Override
	public JSONObject getFlowDictList() {
		JSONObject result = new JSONObject();
		List<FlowDict> flowDictLst = ttFlowDefinePropertyMapper.getFlowDictList();
		result.put("result", flowDictLst);
		return result;
	}
}
