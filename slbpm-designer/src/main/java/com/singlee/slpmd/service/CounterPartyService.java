package com.singlee.slpmd.service;

import com.singlee.slpmd.entity.bo.TtCounterPartyKind;

import java.util.List;

/**
 * Created by rapix on 2017/5/18.
 * 机构业务接口
 */
public interface CounterPartyService {

    List<TtCounterPartyKind> getCounterPartyList();

}
