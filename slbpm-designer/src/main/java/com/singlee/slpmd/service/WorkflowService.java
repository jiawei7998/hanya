package com.singlee.slpmd.service;

import com.alibaba.fastjson.JSONObject;

/**
 * Created by rapix on 2017/4/26.
 * 流程实例管理接口
 */
public interface WorkflowService {

    JSONObject startApply(Integer bizId);

}
