package com.singlee.slpmd.service.impl;

import com.singlee.slpmd.dao.CounterPartyDao;
import com.singlee.slpmd.entity.bo.TtCounterPartyKind;
import com.singlee.slpmd.service.CounterPartyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by rapix on 2017/5/18.
 * 机构业务实现
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CounterPartyServiceImpl implements CounterPartyService {

    @Resource
    private CounterPartyDao counterPartyDao;

    @Override
    public List<TtCounterPartyKind> getCounterPartyList() {
        return counterPartyDao.selectAll();
    }
}
