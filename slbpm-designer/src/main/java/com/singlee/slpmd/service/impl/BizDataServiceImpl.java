package com.singlee.slpmd.service.impl;

import com.singlee.slpmd.dao.BizDataDao;
import com.singlee.slpmd.entity.BizData;
import com.singlee.slpmd.service.BizDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by rapix on 2017/4/24.
 * 业务表单管理
 */
@Service
public class BizDataServiceImpl implements BizDataService {

    @Resource
    private BizDataDao bizDataDao;

    @Override
    public BizData findById(Integer id) {
        return bizDataDao.findById(id);
    }

    @Override
    public void save(BizData entity) {
        bizDataDao.save(entity);
    }

    @Override
    public void update(BizData entity) {
        bizDataDao.update(entity);
    }

}
