package com.singlee.slpmd.dao;

import com.github.pagehelper.Page;
import com.singlee.slpmd.entity.bo.TaUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 用户持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-21 上午09:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TaUserMapper extends Mapper<TaUser> {
	
	/**
	 * 根据对象查询列表
	 * 
	 * @param user - 用户对象
	 * @param rb - 分页对象
	 * @return 用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	Page<TaUser> pageUser(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 修改用户状态
	 * 
	 * @param user - 用户对象
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void updateUserState(TaUser user);
	
	/**
	 * 修改用户密码
	 * 
	 * @param userId - 用户主键
	 * @param userPwd - 用户密码
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void updateUserPwd(@Param("userId") String userId, @Param("userPwd") String userPwd);

	/**
	 * 根据角色Id 读取拥有该角色的用户信息
	 * @param role_id
	 * @return
	 */
	List<TaUser> searchUserByRoleId(String roleId);

	/**
	 *
	 * @param flowRoleId
	 * @return
	 */
	List<TaUser> selectUserWithRoleName(@Param("flowRoleId") String flowRoleId);

	/**
	 * 获得用户信息
	 * @param taUserBo
	 * @return
	 */
	TaUser selectUser(String userId);

	/**
	 * 根据map参数查询用户列表
	 * @param map
	 * @return
	 */
	List<TaUser> searchUser(Map<String, Object> map);

	/**
	 * 查询用户个人信息
	 * @param map
	 * @return
	 */
	TaUser pageUser(Map<String, Object> map);
	
	//
//	/**
//	 * 根据主键查询对象
//	 * 
//	 * @param userId - 用户主键
//	 * @return 用户对象
//	 * @author Hunter
//	 * @date 2016-7-21
//	 */
//	TaUser selectUserByUserId(String userId);
	/**
	 * 根据主键查询对象
	 * @param map
	 * @return
	 */
	TaUser queryUser(String USER);
}