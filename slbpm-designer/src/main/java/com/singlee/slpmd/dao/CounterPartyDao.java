package com.singlee.slpmd.dao;

import com.singlee.slpmd.entity.bo.TtCounterPartyKind;
import tk.mybatis.mapper.common.Mapper;

public interface CounterPartyDao extends Mapper<TtCounterPartyKind> {
}