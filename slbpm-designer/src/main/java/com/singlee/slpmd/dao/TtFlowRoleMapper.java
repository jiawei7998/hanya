package com.singlee.slpmd.dao;

import com.singlee.slpmd.entity.bo.TtFlowRole;
import tk.mybatis.mapper.common.Mapper;

/**
 * 审批角色对应mapper类
 * @author Libo
 *
 */
public interface TtFlowRoleMapper extends Mapper<TtFlowRole> {
}