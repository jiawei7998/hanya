package com.singlee.slpmd.dao;

import com.singlee.slpmd.entity.bo.ProcDefInfo;
import com.singlee.slpmd.entity.vo.ProcDefInfoVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by 杨阳 on 2017/5/3.
 * 流程定义dao
 */
public interface ProcessDefinitionInfoMapper extends Mapper<ProcDefInfo> {

    /**
     * 模糊查询流程定义列表
     * map参数:
     * flow_name	:流程名称
     * flow_type	:流程类型
     * is_active	:是否启用
     * flow_belong	:机构
     * product_type	:产品类型
     * trd_type	    :业务类型
     */
    List<ProcDefInfoVo> searchProcDef(Map<String, Object> map);

}