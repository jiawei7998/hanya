package com.singlee.slpmd.dao;

import com.singlee.slpmd.entity.bo.FlowDict;
import com.singlee.slpmd.entity.bo.TtFlowDefineProperty;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtFlowDefinePropertyMapper extends Mapper<TtFlowDefineProperty> {

	/**
	 * 批量删除流程维度属性
	 */
	void batchDeleteProps(Map<String, String> map);

	/**
	 * 获取流程字典表
	 * 
	 * @return
	 */
	List<FlowDict> getFlowDictList();
}
