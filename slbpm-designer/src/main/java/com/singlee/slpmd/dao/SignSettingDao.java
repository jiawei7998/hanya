package com.singlee.slpmd.dao;

import com.singlee.slpmd.entity.bo.ProcSignSetting;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

/**
 * 会签节点自定义设置Dao
 * Created by rapix on 2017/7/5.
 */
@Component
public interface SignSettingDao {

    @Select("SELECT * FROM TT_PROC_SIGN_SETTING " +
            "WHERE TASK_DEF_KEY = #{task_def_key} AND ROWNUM <= 1")
    ProcSignSetting selectOneByTaskDefKey(@Param("task_def_key") String task_def_key);

    @Insert("INSERT INTO TT_PROC_SIGN_SETTING " +
            "VALUES(#{task_def_key}, #{decision_type}, #{vote_type}, #{condition})")
    void save(ProcSignSetting entity);

    @Update("UPDATE TT_PROC_SIGN_SETTING " +
            "SET DECISION_TYPE = #{decision_type}, " +
            "VOTE_TYPE = #{vote_type}, " +
            "CONDITION = #{condition} " +
            "WHERE TASK_DEF_KEY = #{task_def_key}")
    void update(ProcSignSetting entity);
}
