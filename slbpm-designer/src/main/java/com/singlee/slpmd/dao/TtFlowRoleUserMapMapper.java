package com.singlee.slpmd.dao;

import com.singlee.slpmd.entity.bo.TtFlowRoleUserMap;
import tk.mybatis.mapper.common.Mapper;

/**
 * 流程角色 用户 关系对象
 */
public interface TtFlowRoleUserMapMapper extends Mapper<TtFlowRoleUserMap> {
}