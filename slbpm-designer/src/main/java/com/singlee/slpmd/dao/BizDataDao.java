package com.singlee.slpmd.dao;

import com.singlee.slpmd.entity.BizData;
import org.springframework.stereotype.Component;

/**
 * Created by rapix on 2017/4/24.
 * 业务表单实体管理接口
 */
@Component
public interface BizDataDao {

    BizData findById(Integer id);

    void save(BizData entity);

    void update(BizData entity);

}
