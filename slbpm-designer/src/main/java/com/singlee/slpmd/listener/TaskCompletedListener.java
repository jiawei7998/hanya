package com.singlee.slpmd.listener;

import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.delegate.event.impl.ActivitiEntityEventImpl;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rapix on 2017/4/21.
 * 任务完成事件实现类
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaskCompletedListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger(TaskCompletedListener.class);

    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {

            case TASK_COMPLETED:
                ActivitiEntityEventImpl eventImpl=(ActivitiEntityEventImpl)event;
                TaskEntity taskEntity=(TaskEntity)eventImpl.getEntity();
                logger.info("task is "+taskEntity.getName()+" key is:"+taskEntity.getTaskDefinitionKey());
                logger.info("进入任务完成事件监听器 ---->" + event.getType().name());
                break;

            default:
                logger.info("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}