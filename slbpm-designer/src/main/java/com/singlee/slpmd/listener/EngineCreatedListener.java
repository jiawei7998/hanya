package com.singlee.slpmd.listener;

import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by rapix on 2017/4/22.
 * 引擎启动事件实现类
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EngineCreatedListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger(EngineCreatedListener.class);

    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {

            case ENGINE_CREATED:
                logger.info("进入引擎启动事件监听器 ---->" + event.getType().name());
                break;

            default:
                logger.info("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}