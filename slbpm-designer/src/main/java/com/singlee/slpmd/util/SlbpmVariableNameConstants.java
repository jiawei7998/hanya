package com.singlee.slpmd.util;

/**
 * 流程变量名常量
 * @author Libo
 *
 */
public class SlbpmVariableNameConstants {
	/** 外部序号  */
	public final static String serialNo="serialNo";
	/** 回调的Ban  */
	public final static String callBackBeanName="callBackBeanName";
	/** 最后操作人 */
	public final static String lastOperator="lastOperator";
	/** 指定用户 */
	public final static String specificUser="specificUser";
	/** 角色 */
	public final static String roles="roles";
	/** 决策类型 */
	public final static String voteType="vote";
	/** 审批步骤的路径 */
	public final static String approveStepPath="approveStepPath";
	/**子流程信息 */
	public final static String subTaskInfos="subTaskInfos";
	/**通过数量 */
	public final static String passCount="passCount";
	/**不通过数量 */
	public final static String nopassCount="nopassCount";
	/**继续的数量 */
	public final static String continueCount="continueCount";
	/**所有数量 */
	public final static String totalCount="totalCount";
	/**节点名称*/
	public final static String nodeName="nodeName";
	
}
