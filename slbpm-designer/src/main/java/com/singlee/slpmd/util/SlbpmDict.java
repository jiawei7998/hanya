package com.singlee.slpmd.util;

public class SlbpmDict {
	public final static class YesNo{
	    /** 是 */
	  public final static String  YES = "1";
	    /** 否 */
	  public final static String  NO = "0";
	}

	/**
	 *   FlowCompleteType 审批流程 处理方式 处理方式
	 */
	public final static class FlowCompleteType{
	    /** 作废 */
	  public final static String  Refuse = "-1";
	    /** 通过 */
	  public final static String  Pass = "0";
	    /** 继续 */
	  public final static String  Continue = "1";
	    /** 驳回 */
	  public final static String  Back = "-2";
	    /** 退回发起 */
	  public final static String  Redo = "99";
	 }
	
	/**
	 *   VoteType 审批流程 决策类型 决策类型
	 */
	public final static class VoteType{
	    /** 一票制 */
	  public final static String  One = "0";
	    /** 全票制 */
	  public final static String  All = "1";
	    /** 按比例 */
	  public final static String  Rate = "2";
	   public final static String getDictKey(){  return "VoteType"; } 
	 }
	/**
	 *   000048 审批流程 审批状态 审批状态
	 */
	public final static class ApproveStatus{
	    /** 就绪 */
	  public final static String  Ready = "1";
	    /** 计算中 */
	  public final static String  Calculating = "2";
	    /** 新建 */
	  public final static String  New = "3";
	    /** 待审批 */
	  public final static String  WaitApprove = "4";
	    /** 审批中 */
	  public final static String  Approving = "5";
	    /** 审批通过 */
	  public final static String  ApprovedPass = "6";
	    /** 审批拒绝 */
	  public final static String  ApprovedNoPass = "7";
	    /** 审批注销 */
	  public final static String  Cancle = "8";
	    /** 执行中 */
	  public final static String  Executing = "9";
	    /** 执行完成 */
	  public final static String  Executed = "10";
	    /** 待核实 */
	  public final static String  WaitVerify = "11";
	    /** 核实中 */
	  public final static String  Verifying = "15";
	    /** 核实完成 */
	  public final static String  Verified = "12";
	    /** 核实拒绝 */
	  public final static String  VerifyRefuse = "16";
	    /** 出账中 */
	  public final static String  Accounting = "13";
	    /** 出账完成 */
	  public final static String  Accounted = "14";
	    /** 交易冲正 */
	  public final static String  TradeOffset = "17";
	   public final static String getDictKey(){  return "000048"; } 
	 }
}
