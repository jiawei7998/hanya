package com.singlee.slpmd.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by rapix on 2017/4/23.
 * 采用AOP的方式拦截Controller调用
 */
@Component
@Aspect
public class GeneralLogAop {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Pointcut("execution(* com.singlee.slpmd.controller..*(..))")
    public void aopMethod(){}

    @Around("aopMethod()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("@调用方法 -- " + joinPoint.getSignature().getName());
        return joinPoint.proceed();
    }
}
