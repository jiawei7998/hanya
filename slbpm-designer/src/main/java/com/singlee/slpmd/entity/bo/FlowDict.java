package com.singlee.slpmd.entity.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "TT_FLOW_DICT")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class FlowDict implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 字典编码
	private String dictId;
	// 字典描述
	private String dictName;

	private List<FlowDictItem> dictItems;

	public List<FlowDictItem> getDictItems() {
		return dictItems;
	}

	public void setDictItems(List<FlowDictItem> dictItems) {
		this.dictItems = dictItems;
	}

	public String getDictId() {
		return dictId;
	}

	public void setDictId(String dictId) {
		this.dictId = dictId;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

}
