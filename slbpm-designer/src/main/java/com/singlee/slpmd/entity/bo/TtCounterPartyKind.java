package com.singlee.slpmd.entity.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_TRD_COUNTERPARTYKIND")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtCounterPartyKind {

    private String kind_code  ;// 编码
    private String kind_name  ;// 名称
    private String kind_grade ;// 类别
    public String getKind_code() {
        return kind_code;
    }
    public void setKind_code(String kind_code) {
        this.kind_code = kind_code;
    }
    public String getKind_name() {
        return kind_name;
    }
    public void setKind_name(String kind_name) {
        this.kind_name = kind_name;
    }
    public String getKind_grade() {
        return kind_grade;
    }
    public void setKind_grade(String kind_grade) {
        this.kind_grade = kind_grade;
    }
    public TtCounterPartyKind() {
    }
    @Override
    public String toString() {
        return String.format("TtCounterPartyKind [kind_code=%s, kind_name=%s, kind_grade=%s]", kind_code, kind_name, kind_grade);
    }

}