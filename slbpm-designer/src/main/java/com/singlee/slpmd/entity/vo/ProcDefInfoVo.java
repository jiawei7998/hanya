package com.singlee.slpmd.entity.vo;

import com.singlee.slpmd.entity.bo.ProcDefInfo;

/**
 * Created by 杨阳 on 2017/5/3.
 * 流程定义的Vo类 - DEMO
 */
public class ProcDefInfoVo extends ProcDefInfo {

    private String flow_belong;
    private String flow_belong_text;
    private String product_type;
    private String product_type_text;
    private String trd_type;
    private String trd_type_text;

    public String getFlow_belong() {
        return flow_belong;
    }
    public void setFlow_belong(String flow_belong) {
        this.flow_belong = flow_belong;
    }

    public String getFlow_belong_text() {
        return flow_belong_text;
    }
    public void setFlow_belong_text(String flow_belong_text) {
        this.flow_belong_text = flow_belong_text;
    }

    public String getTrd_type() {
        return trd_type;
    }
    public void setTrd_type(String trd_type) {
        this.trd_type = trd_type;
    }

    public String getTrd_type_text() {
        return trd_type_text;
    }
    public void setTrd_type_text(String trd_type_text) {
        this.trd_type_text = trd_type_text;
    }

    public String getProduct_type() {
        return product_type;
    }
    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_type_text() {
        return product_type_text;
    }
    public void setProduct_type_text(String product_type_text) {
        this.product_type_text = product_type_text;
    }

}
