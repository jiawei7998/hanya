package com.singlee.slpmd.entity.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by 杨阳 on 2017/7/5.
 * 会签节点自定义设置实体类
 */
@Entity
@Table(name = "TT_PROC_SIGN_SETTING")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcSignSetting {
	
    private String task_def_key;	// 节点定义KEY
    private String decision_type;	// 决策方式 0-拒绝 1-通过
    private String vote_type;		// 投票类型 0-绝对票数 1-百分比
    private String condition;		// 通过条件
    
    public String getTask_def_key() {
        return task_def_key;
    }
    public void setTask_def_key(String task_def_key) {
        this.task_def_key = task_def_key;
    }
    
    public String getDecision_type() {
        return decision_type;
    }
    public void setDecision_type(String decision_type) {
        this.decision_type = decision_type;
    }
    
    public String getVote_type() {
        return vote_type;
    }
    public void setVote_type(String vote_type) {
        this.vote_type = vote_type;
    }
    
    public String getCondition() {
        return condition;
    }
    public void setCondition(String condition) {
        this.condition = condition;
    }
}
