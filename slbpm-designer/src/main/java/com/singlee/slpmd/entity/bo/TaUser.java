package com.singlee.slpmd.entity.bo;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * 柜员 用户
 * 
 * @author LyonChen
 * 
 */

@Entity
@Table(name = "TA_USER")
public class TaUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String userId;// 用户ID
	private String userName;// 用户姓名
	private String userPwd;// 密码
	private String isActive;// 状态 1-启用 0-停用 字典项 000031
	private String userCreatetime;// 用户创建时间
	private String userStoptime;// 用户停用时间
	private String userPwdchgtime;// 上次密码更改时间
	private String userMemo;// 备注
	private String userEmail;// 用户EMAIL
	private String userFixedphone;// 固定电话
	private String userCellphone;// 移动电话
	private String instId;// 机构号
	private String userFlag;// 标识 字典项 000003
	private String userLogontype;// 登陆方式 0000005
	private String isOnline;// 是否在线
	private String latestIp;// 最后一次ip地址
	private String latestLoginTime;// 最后一次登录时间
	private String latestTime;// 最后访问时间
	
	@Transient
	private List<String> roleIdList; //角色 的id 列表
	@Transient
	private List<String> roleNameList; //角色 的名称 列表
	
	@Transient
	private String instName; // 机构名称
	private String empId;//员工编号

	public List<String> getRoleNameList() {
		return roleNameList;
	}

	public void setRoleNameList(List<String> roleNameList) {
		this.roleNameList = roleNameList;
	}

	public List<String> getRoleIdList() {
		return roleIdList;
	}

	public void setRoleIdList(List<String> roleList) {
		this.roleIdList = roleList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getUserCreatetime() {
		return userCreatetime;
	}

	public void setUserCreatetime(String userCreatetime) {
		this.userCreatetime = userCreatetime;
	}

	public String getUserStoptime() {
		return userStoptime;
	}

	public void setUserStoptime(String userStoptime) {
		this.userStoptime = userStoptime;
	}

	public String getUserPwdchgtime() {
		return userPwdchgtime;
	}

	public void setUserPwdchgtime(String userPwdchgtime) {
		this.userPwdchgtime = userPwdchgtime;
	}

	public String getUserMemo() {
		return userMemo;
	}

	public void setUserMemo(String userMemo) {
		this.userMemo = userMemo;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserFixedphone() {
		return userFixedphone;
	}

	public void setUserFixedphone(String userFixedphone) {
		this.userFixedphone = userFixedphone;
	}

	public String getUserCellphone() {
		return userCellphone;
	}

	public void setUserCellphone(String userCellphone) {
		this.userCellphone = userCellphone;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(String userFlag) {
		this.userFlag = userFlag;
	}

	public String getUserLogontype() {
		return userLogontype;
	}

	public void setUserLogontype(String userLogontype) {
		this.userLogontype = userLogontype;
	}

	public String getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}

	public String getLatestIp() {
		return latestIp;
	}

	public void setLatestIp(String latestIp) {
		this.latestIp = latestIp;
	}

	public String getLatestLoginTime() {
		return latestLoginTime;
	}

	public void setLatestLoginTime(String latestLoginTime) {
		this.latestLoginTime = latestLoginTime;
	}

	public String getLatestTime() {
		return latestTime;
	}

	public void setLatestTime(String latestTime) {
		this.latestTime = latestTime;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}
	
	/**
	 * 检查密码 送过来的 pass 参数已经是 由js加密成 md5
	 * @return
	 */
	public boolean checkPassword(String pass){
		// 是否状态合法
		return !StringUtils.isEmpty(pass) && pass.equalsIgnoreCase(this.userPwd);
	}

	@Override
	public String toString() {
		return String
				.format("TaUser [userId=%s, userName=%s, userPwd=%s, isActive=%s, userCreatetime=%s, userStoptime=%s, userPwdchgtime=%s, userMemo=%s, userEmail=%s, userFixedphone=%s, userCellphone=%s, instId=%s, userFlag=%s, userLogontype=%s, isOnline=%s, latestIp=%s, latestLoginTime=%s, latestTime=%s]",
						userId, userName, userPwd, isActive, userCreatetime,
						userStoptime, userPwdchgtime, userMemo, userEmail,
						userFixedphone, userCellphone, instId, userFlag,
						userLogontype, isOnline, latestIp, latestLoginTime,
						latestTime,empId);
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}
}
