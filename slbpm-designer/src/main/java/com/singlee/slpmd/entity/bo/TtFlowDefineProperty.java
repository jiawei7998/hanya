package com.singlee.slpmd.entity.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_FLOW_DEFINE_PROPERTY")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtFlowDefineProperty {
	
	private String flow_id;	// 流程定义ID
	private String version;// 版本号
	private String property_type;// 类型
	private String property_code;// 代码
	private String property_name;// 名称
	
	public String getFlow_id() {
		return flow_id;
	}
	public void setFlow_id(String flow_id) {
		this.flow_id = flow_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getProperty_type() {
		return property_type;
	}
	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}
	public String getProperty_code() {
		return property_code;
	}
	public void setProperty_code(String property_code) {
		this.property_code = property_code;
	}
	public String getProperty_name() {
		return property_name;
	}
	public void setProperty_name(String property_name) {
		this.property_name = property_name;
	}
	
}
