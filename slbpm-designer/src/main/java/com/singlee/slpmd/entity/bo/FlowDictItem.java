package com.singlee.slpmd.entity.bo;

import java.io.Serializable;

public class FlowDictItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 字典KEY
	private String value;
	// 字典VALYE
	private String text;
	// 是否可用
	private String isActive;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
}
