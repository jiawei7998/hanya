DROP TABLE IF EXISTS `t_biz_data`;
CREATE TABLE `t_biz_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` varchar(64) DEFAULT NULL,
  `applyTime` datetime DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `processInstanceId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `t_biz_data` VALUES (1,
                                 'employee',
                                 '2017-03-16 11:25:21',
                                 '111',
                                 '222',
                                 null);