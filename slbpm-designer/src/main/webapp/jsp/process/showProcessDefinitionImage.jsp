<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>流程定义图</title>
    <script type="text/javascript" src="../js/jquery.min.js" charset="utf-8"></script>
    <!-- 引入Jquery_easyui -->
    <script type="text/javascript" src="../js/jquery.easyui.min.js" charset="utf-8"></script>
    <!-- 引入easyUi默认的CSS格式--蓝色 -->
    <link rel="stylesheet" type="text/css" href="../css/easyui.css" />
    <!-- 引入easyUi小图标 -->
    <link rel="stylesheet" type="text/css" href="../css/icon.css" />
</head>
<body>
<input type="hidden" id="processDefinitionId" value="${processDefinitionId}">
<div>
    <img id="processImg" src="" style="position:absolute; left:0px; top:0px;">
</div>
</body>
<script  type="text/javascript" src="../js/baseUrl.js" charset="utf-8"></script>
<script>
    var storage = window.sessionStorage;
    var user = storage.getItem("user");
    $.post(baseUrl+"/slbpm/management/identity/checkUserRight",{"user":user},function(result) {
        if (result.flag) {
            document.getElementById('processImg').src = baseUrl+'/slbpm/processDefinition_temp/getProcessDefinitionInfo?processDefinitionId=' + $('#processDefinitionId').val();
        } else {
            $.messager.alert("操作提示", "您尚未登录！", "info", function () {
                window.location.href = baseUrl + '/standard/login.jsp';
            });
        }
    });
</script>
<script type="text/javascript" src="../js/process/getActivitisAndFlows.js" charset="utf-8"></script>
</html>