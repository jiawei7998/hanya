<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>我的流程列表</title>
    <script type="text/javascript" src="../../js/jquery.min.js" charset="utf-8"></script>
    <!-- 引入Jquery_easyui -->
    <script type="text/javascript" src="../../js/jquery.easyui.min.js" charset="utf-8"></script>
    <!-- 引入easyUi默认的CSS格式--蓝色 -->
    <link rel="stylesheet" type="text/css" href="../../css/easyui.css" />
    <!-- 引入easyUi小图标 -->
    <link rel="stylesheet" type="text/css" href="../../css/icon.css" />
</head>
<script  type="text/javascript" src="../js/baseUrl.js" charset="utf-8"></script>
<body>
<h3>我的流程列表（用户：<span id="userid"></span>）</h3>
    <hr>
    <div class="easyui-tabs" style="width:100%;height:500px;" id="tableTab">
        <div title="待办任务" style="padding:10px;">
            <table id="tt0" class="easyui-datagrid" style="width:100%;height:350px">
                <thead>
                <tr>
                    <th data-options="field:'id'">任务ID</th>
                    <th data-options="field:'name'">任务名称</th>
                    <th data-options="field:'createTime'">开始时间</th>
                    <th data-options="field:'endTime'">结束时间</th>
                    <th data-options="field:'processDefinitionId'">查看流程状态</th>
                </tr>
                </thead>
            </table>
        </div>
        <div title="已完成任务" closable="true" style="padding:10px;">
            <table id="tt1" class="easyui-datagrid" style="width:100%;height:350px">
                <thead>
                <tr>
                    <th data-options="field:'id'">任务ID</th>
                    <th data-options="field:'name'">任务名称</th>
                    <th data-options="field:'createTime'">开始时间</th>
                    <th data-options="field:'endTime'">结束时间</th>
                    <th data-options="field:'processDefinitionId'">查看流程状态</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</body>
<script>

    $('#userid').html(window.sessionStorage.getItem('name'));

    $('#tableTab').tabs({
        border:false,
        onSelect:function(title,index){
            if(title=='待办任务'){
                $.post(baseUrl+"/slbpm/task/todoList",{},function(result){
                    var data = result.groups;
                    var groupList = [];
                    console.log(data);
                    for(var i=0;i<data.length;i++){
                        var group = data[i];
                        var a = {
                            "id":group.id,
                            "name":group.name,
                            "createTime":formatDateTime(group.createTime),
                            "endTime":formatDateTime(group.endTime),
                            "processDefinitionId":'<a href="'+baseUrl+'/slbpm/processDefinition/showDiagramViewer?processDefinitionId='+group.processDefinitionId+'&processInstanceId='+group.processInstanceId+'">查看流程执行</a>',

                        };
                        groupList.push(a);
                    }
                    $("#tt0").datagrid("loadData",groupList);  //动态取数据
                });
            }else if(title=='已完成任务'){
                $.post(baseUrl+"/slbpm/task/finishedList",{},function(result){
                    var data = result.groups;
                    var groupList = [];
                    console.log(data);
                    for(var i=0;i<data.length;i++){
                        var group = data[i];
                        var a = {
                            "id":group.id,
                            "name":group.name,
                            "createTime":formatDateTime(group.createTime),
                            "endTime":formatDateTime(group.endTime),
                            "processDefinitionId":'<a href="'+baseUrl+'/slbpm/processDefinition/showDiagramViewer?processDefinitionId='+group.processDefinitionId+'&processInstanceId='+group.processInstanceId+'">查看流程执行</a>',

                        };
                        groupList.push(a);
                    }
                    $("#tt1").datagrid("loadData",groupList);  //动态取数据
                });
            }
        }
    });

    function formatDateTime(inputTime) {
        if(inputTime){
            var date = new Date(inputTime);
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            m = m < 10 ? ('0' + m) : m;
            var d = date.getDate();
            d = d < 10 ? ('0' + d) : d;
            var h = date.getHours();
            h = h < 10 ? ('0' + h) : h;
            var minute = date.getMinutes();
            var second = date.getSeconds();
            minute = minute < 10 ? ('0' + minute) : minute;
            second = second < 10 ? ('0' + second) : second;
            return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second;
        }else{
            return "";
        }
    };
</script>
</html>