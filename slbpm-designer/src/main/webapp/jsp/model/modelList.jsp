<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>流程模型列表</title>
    <script type="text/javascript" src="../../js/jquery.min.js" charset="utf-8"></script>
    <!-- 引入Jquery_easyui -->
    <script type="text/javascript" src="../../js/jquery.easyui.min.js" charset="utf-8"></script>
    <!-- 引入easyUi默认的CSS格式--蓝色 -->
    <link rel="stylesheet" type="text/css" href="../../css/easyui.css" />
    <!-- 引入easyUi小图标 -->
    <link rel="stylesheet" type="text/css" href="../../css/icon.css" />
    <script  type="text/javascript" src="../js/baseUrl.js" charset="utf-8"></script>
</head>
<body>
    <h3>流程模型工作区</h3>
    <hr>
    <div id="tb">
        <a href="javascript:showaddmodel();" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true">添加流程模型</a>
        <a href=baseUrl+"/slbpm/processDefinition/listProcDef" class="easyui-linkbutton" data-options="iconCls:'icon-redo',plain:true">已部署流程列表</a>
        <a href="javascript:showMyProcess()" class="easyui-linkbutton" data-options="iconCls:'icon-man',plain:true">我的流程列表</a>
    </div>
    <table id="dg" class="easyui-datagrid" style="width:100%;height:500px;" data-option="toolbar:'#tb', singleSelect:true">
        <thead>
        <tr>
            <th data-options="field:'id'">ID</th>
            <th data-options="field:'name'">NAME</th>
            <th data-options="field:'key'">KEY</th>
            <th data-options="field:'version'">VERSION</th>
            <th data-options="field:'createTime'">创建时间</th>
            <th data-options="field:'lastUpdateTime'">更新时间</th>
            <th data-options="field:'metaInfo'">元数据</th>
            <th data-options="field:'option'">操作</th>
        </tr>
        </thead>
    </table>

    <div id="dd" class="easyui-dialog" title="新建流程 " style="width:300px;height:350px;"
         data-options="iconCls:'icon-save',buttons:'#bb',modal:true,minimizable:false,collapsible:false,maximizable:false,closed:true">
        <div>
            流程的名称  <input id="modelname" class="easyui-validatebox" data-options="required:true">
        </div>
        <div>
            流程关键字  <input id="modelkey" class="easyui-validatebox" data-options="required:true">
        </div>
        <div>
            流程的描述  <input id="modeldes" class="easyui-validatebox" data-options="required:true">
        </div>
    </div>
    <div id="bb">
        <a href="javascript:addmodel();" class="easyui-linkbutton">Save</a>
        <a href="javascript:$('#dd').dialog('close');" class="easyui-linkbutton">Close</a>
    </div>
</body>
<script>

    $.post(baseUrl+"/slbpm/model/getModelList",{},function(result){
        var data = result.modelList;
        var groupList = [];
        for(var i=0;i<data.length;i++){
            var group = data[i];
            var a = {
                "id":group.id,
                "name":group.name,
                "key":group.key,
                "version":group.version,
                "createTime":formatDateTime(group.createTime),
                "lastUpdateTime":formatDateTime(group.lastUpdateTime),
                "metaInfo":group.metaInfo,
                "option":'<a href="'+baseUrl+'/slbpm/model/showModel?modelId='+group.id+'" class="easyui-linkbutton" data-options="iconCls:\'icon-edit\',plain:true">编辑</a>'+
                        '&nbsp;&nbsp;<a href="javascript:deployModel("'+group.id+'")" class="easyui-linkbutton" data-options="iconCls:\'icon-redo\',plain:true">部署</a>'+
                        '&nbsp;&nbsp;<a href="javascript:deleteModel("'+group.id+'")" class="easyui-linkbutton" data-options="iconCls:\'icon-remove\',plain:true">删除</a>',

            };
            groupList.push(a);
        }
        $("#dg").datagrid("loadData",groupList);  //动态取数据
    });

    function showaddmodel(){
        $('#dd').dialog('open');
    }
    function addmodel(){
        if($('#modelname').val().trim() == '' || $('#modelname').val().trim()==null){
            alert('请填写模型的名称');
        }else if($('#modelkey').val().trim() == '' || $('#modelkey').val().trim()==null){
            alert('请填写模型关键字');
        }else if($('#modeldes').val().trim() == '' || $('#modeldes').val().trim()==null){
            alert('请填写模型的描述');
        }else{
            window.location.href=baseUrl+'/slbpm/model/createModel?name='+$('#modelname').val().trim()+'&key='+$('#modelkey').val().trim()
                +'&description='+$('#modeldes').val().trim();
        }
    }

    function deployModel(id){
        $.post(baseUrl+"/slbpm/model/deployModel",{"modelId":id},function(result){
            alert(result.message);
            window.location.href=baseUrl+'/slbpm/editDefinition.jsp?deploymentId='+result.deploymentId;
        });
    }

    function deleteModel(id){
        $.post(baseUrl+"/slbpm/model/deleteModel",{"modelId":id},function(result){
            alert(result.message);
            window.location.reload();
        });
    }

    function showMyProcess(){
        window.location = baseUrl+"/slbpm/jsp/process/myProcess.jsp";
    }

    function formatDateTime(inputTime) {
        if(inputTime){
            var date = new Date(inputTime);
            var y = date.getFullYear();
            var m = date.getMonth() + 1;
            m = m < 10 ? ('0' + m) : m;
            var d = date.getDate();
            d = d < 10 ? ('0' + d) : d;
            var h = date.getHours();
            h = h < 10 ? ('0' + h) : h;
            var minute = date.getMinutes();
            var second = date.getSeconds();
            minute = minute < 10 ? ('0' + minute) : minute;
            second = second < 10 ? ('0' + second) : second;
            return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second;
        }else{
            return "";
        }
    };
</script>
</html>