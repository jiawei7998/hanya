<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>流程定义及部署</title>
<script type="text/javascript" src="../js/jquery.min.js" charset="utf-8"></script>
<!-- 引入Jquery_easyui -->  
<script type="text/javascript" src="../js/jquery.easyui.min.js" charset="utf-8"></script>
<!-- 引入easyUi默认的CSS格式--蓝色 -->  
<link rel="stylesheet" type="text/css" href="../css/easyui.css" />
<!-- 引入easyUi小图标 -->  
<link rel="stylesheet" type="text/css" href="../css/icon.css" />
<script  type="text/javascript" src="./js/baseUrl.js" charset="utf-8"></script>
</head>

<body>
	<h3>流程定义及部署</h3>
	<hr>
	<div id="tb">
		<%--<a href="/processDefinition/listProcDef" class="easyui-linkbutton" data-options="iconCls:'icon-redo',plain:true">流程列表</a>--%>
	</div>
	<table id="dg" class="easyui-datagrid" style="width:100%;height:500px;" data-option="toolbar:'#tb'">
		<thead>
			<tr>
				<th data-options="field:'ProcessDefinitionId'">ProcessDefinitionId</th>
				<th data-options="field:'DeploymentId'">DeploymentId</th>
				<th data-options="field:'名称'">名称</th>
				<th data-options="field:'KEY'">KEY</th>
				<th data-options="field:'版本号'">版本号</th>
				<th data-options="field:'XML'">XML</th>
				<th data-options="field:'图片'">图片</th>
				<%--<th data-options="field:'部署时间'">部署时间</th>--%>
				<th data-options="field:'是否挂起'">是否挂起</th>
				<th data-options="field:'操作'">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${procDefList }" var="va">
			<tr>
				<td>${va.id }</td>
				<td>${va.deploymentId }</td>
				<td>${va.name }</td>
				<td>${va.key }</td>
				<td>${va.version }</td>
				<td>${va.resourceName }</td>
				<td>${va.diagramResourceName }</td>
				<%--<td>${va. }</td>--%>
				<td>
					<c:choose>
						<c:when test="${va.suspensionState== 2}">
							true&nbsp;&nbsp;|<a href="javascript:updateProcessDefinition('active','${va.id }')" style="color:blue">激活</a>
						</c:when>
						<c:when test="${va.suspensionState== 1}">
							false&nbsp;|<a href="javascript:updateProcessDefinition('suspend','${va.id }')" style="color:blue">挂起</a>
						</c:when>
					</c:choose>
				</td>
				<td>
					<a href="javascript:startProcess('${va.id }')">开启新的流程实例</a>
					<a href="'+baseUrl+'/slbpm/processDefinition/listProcInst?pdfid=${va.id }">查看流程实例</a>
					<a href="javascript:deleteProcessDefinitionState('${va.deploymentId }')" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true">删除</a>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
<script>

	function startProcess(id){
        $.post(baseUrl+"/slbpm/processDefinition/startProcess",{"processDefinitionId":id},function(result){
            alert(result.message);
        });
	}

    function updateProcessDefinition(state,id){
        $.post(baseUrl+"/slbpm/processDefinition/updateProcessDefinitionState",{"state":state,"processDefinitionId":id},function(result){
            alert(result.message);
            window.location.reload();
        });
    }

	function deleteProcessDefinitionState(deploymentId){
        $.post(baseUrl+"/slbpm/processDefinition/deleteProcessDefinitionState",{"deploymentId":deploymentId},function(result){
			alert(result.message);
			window.location.reload();
        });
	}

</script>
</html>