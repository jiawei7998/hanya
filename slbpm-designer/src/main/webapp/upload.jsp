﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	    <title>File Upload</title>
	    <link id="easyuiTheme" rel="stylesheet" type="text/css" href="./css/easyui.css" />
	    <link rel="stylesheet" type="text/css" href="./css/icon.css" />
	    <script type="text/javascript" src="./js/jquery.min.js"></script>
	    <script type="text/javascript" src="./js/jquery.easyui.min.js"></script>
	    <script  type="text/javascript" src="./js/baseUrl.js" charset="utf-8"></script>
	    <style type="text/css">
		    table.form_table {border-collapse:collapse;border:none;width:100%;} 
		    table.form_table td{border:1px solid #99BBE8;padding:5px;}
		    table.form_table td:nth-child(odd){background:#fafafa;}
		    table.form_table tfoot td, tfoot th {font-weight: bold;}
		    table.form_table caption {font-size: 25px;}
		    table[cols="2"] td{width:50%;}
		    table[cols="4"] td{width:25%;min-width:200px;max-width:200px;height:22px;}
		    table[cols="4"] td:nth-child(odd) { width:15%;} 
		    table[cols="4"] td:nth-child(even) { width:35%;}
		    
		    .btn_div {
		        height: 30px;
		        line-height: 30px;
		        padding: 5px 18px 0px 0px;
		        text-align: center;
		        position: relative;
		    }
	    </style>
	    <script type="text/javascript">
		    $(document).ready(function(){
			    //上传文件
				$('#save_btn').click(function() {
					$.messager.progress({
						title : '提示信息',
						msg : '正在上传,请稍后...'
					});
					$('#frm').form('submit', {
						onSubmit : function() {
							return true;
						},
						success : function(data) {
							data = JSON.parse(data);
							$.messager.progress('close');
							var msg = data.desc?"上传成功.":"上传失败.";
							$.messager.alert("提示信息", msg + data.message , 'info'); 
						}
					});
				});
				
				$('#close_btn').click(function() {
					window.close();
				});
		    });
	    </script>
	</head>
	<body>
		<div id="upload" class="easyui-panel" autoWidth="true" title="" border="false" fit="true">
			<form id="frm" method="post" enctype="multipart/form-data" action="./model/import">
				<table id="fileUploadTable" class="form_table" cols="4">
					<caption>流程模型上传</caption>
					<tr>
					  <td>模型文件</td>
					  <td><input type="file" name="file" id="file" style="width:650px;"/></td>
					</tr>
				</table>
			</form>
			<div class="btn_div">
				<a href="javascript:void(0)" id="save_btn" class="easyui-linkbutton" iconCls="icon-save">上传</a>
				<a href="javascript:void(0)" id="close_btn" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
		  	</div>
		</div>
	</body>
</html>
