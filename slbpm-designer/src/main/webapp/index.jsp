<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="js/jquery.min.js" charset="utf-8"></script>
<!-- 引入Jquery_easyui -->
<script type="text/javascript" src="js/jquery.easyui.min.js" charset="utf-8"></script>
<!-- 引入easyUi默认的CSS格式--蓝色 -->
<link rel="stylesheet" type="text/css" href="css/easyui.css" />
<!-- 引入easyUi小图标 -->
<link rel="stylesheet" type="text/css" href="css/icon.css" />
<script  type="text/javascript" src="./js/baseUrl.js" charset="utf-8"></script>
<title></title>

</head>
<body class="easyui-layout">
	<div id="loginWin" class="easyui-window" title="登录" style="width:350px;height:188px;padding:5px;"
		 minimizable="false" maximizable="false" resizable="false" collapsible="false">
		<div class="easyui-layout" fit="true">
			<div region="center" border="false" style="padding:5px;background:#fff;border:1px solid #ccc;">
				<form id="loginForm" method="post">
					<div style="padding:5px 0;">
						<label for="username">帐号:</label>
						<input type="text" name="username" style="width:260px;"></input>
					</div>
					<div style="padding:5px 0;">
						<label for="password">密码:</label>
						<input type="password" name="password" style="width:260px;"></input>
					</div>
					<div style="padding:5px 0;text-align: center;color: red;" id="showMsg"></div>
				</form>
			</div>
			<div region="south" border="false" style="text-align:right;padding:5px 0;">
				<a class="easyui-linkbutton" iconCls="icon-ok" href="javascript:void(0)" onclick="login()">登录</a>
				<a class="easyui-linkbutton" iconCls="icon-cancel" href="javascript:void(0)" onclick="cleardata()">重置</a>
			</div>
		</div>
	</div>
</body>
<script>
   
    $(document).ready(function(){
        $("input[name='username']").focus();
    });
    function cleardata(){
        $('#loginForm').form('clear');
    }
    function login(){
        if($("input[name='username']").val()=="" || $("input[name='password']").val()==""){
            $("#showMsg").html("用户名或密码为空，请输入");
            $("input[name='username']").focus();
        }else{
            //ajax异步提交
            $.ajax({
                type:"POST",   //post提交方式默认是get
                url:baseUrl+"/slbpm/user/login",
                dataType:"json",
                contentType:"application/json;charset=utf-8",
                data: JSON.stringify({"username":$("input[name='username']").val(),"password":$("input[name='password']").val()}),
                error:function(request) {      // 设置表单提交出错
                    $("#showMsg").html(request);  //登录错误提示信息
                },
                success:function(data) {
                    var storage = window.sessionStorage;
                    storage.clear();
                    storage.setItem("id",data.loginuser.id);
                    storage.setItem("name",data.loginuser.lastName+data.loginuser.firstName);
                    if(data.loginuser.id=="admin"){
                        window.location = baseUrl+"/slbpm/jsp/model/modelList.jsp";
					}else{
                        window.location = baseUrl+"/slbpm/jsp/process/myProcess.jsp";
					}
                }
            });
        }
    }
</script>
</html>