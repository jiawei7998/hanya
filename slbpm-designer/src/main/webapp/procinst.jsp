<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>流程实例</title>
<script type="text/javascript" src="./js/jquery.min.js" charset="utf-8"></script>
<!-- 引入Jquery_easyui -->  
<script type="text/javascript" src="./js/jquery.easyui.min.js" charset="utf-8"></script>
<!-- 引入easyUi默认的CSS格式--蓝色 -->  
<link rel="stylesheet" type="text/css" href="./css/easyui.css" />
<!-- 引入easyUi小图标 -->  
<link rel="stylesheet" type="text/css" href="./css/icon.css" />
<script  type="text/javascript" src="./js/baseUrl.js" charset="utf-8"></script>
</head>

<body>
	<h3>流程实例</h3>
	<hr>
	<h5><a href="' + baseUrl + '/slbpm/unittest.jsp" target="_blank">审批页面</a></h5>
	<table class="easyui-datagrid" style="width:100%;height:500px;">
		<thead>
			<tr>
				<th data-options="field:'ID'">ID</th>
				<th data-options="field:'流程定义ID'">流程定义ID</th>
				<th data-options="field:'BUSINESS_KEY'">BUSINESS_KEY</th>
				<th data-options="field:'ENDED'">ENDED</th>
				<%--<th data-options="field:'是否挂起'">是否挂起</th>--%>
				<th data-options="field:'操作'">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${procInstList }" var="va">
			<tr>
				<td>${va.id }</td>
				<td>${va.processDefinitionId }</td>
				<td>${va.businessKey }</td>
				<%--<td>--%>
					<%--<c:choose>--%>
						<%--<c:when test="${va.suspensionState== 2}">--%>
							<%--true&nbsp;&nbsp;|<a href="javascript:updateProcessInstance('active','${va.id }')" style="color:blue">激活</a>--%>
						<%--</c:when>--%>
						<%--<c:when test="${va.suspensionState== 1}">--%>
							<%--false&nbsp;|<a href="javascript:updateProcessInstance('suspend','${va.id }')" style="color:blue">挂起</a>--%>
						<%--</c:when>--%>
					<%--</c:choose>--%>
				<%--</td>--%>
				<td>
					<a href="'+baseUrl+'/slbpm/processDefinition/showDiagramViewer?processDefinitionId=${va.processDefinitionId }&processInstanceId=${va.id }">查看流程执行</a>
				</td>
				<td></td>
			</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
<script>
    function updateProcessInstance(state,id){
        $.post(baseUrl+"/slbpm/ProcessInstance/updateProcessInstance",{"state":state,"processDefinitionId":id},function(result){
            alert(result.message);
            window.location.reload();
        });
    }

</script>
</html>