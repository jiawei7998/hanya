/*
 * Activiti Modeler component part of the Activiti project
 * Copyright 2005-2014 Alfresco Software, Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * ServiceTaskExpression
 */

var KisBpmServiceTaskExpressionCtrl = [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        backdrop: "static",
        keyboard: false,
        template:  'editor-app/configuration/properties/servicetaskexpression-property-pop.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];

var KisBpmServiceTaskExpressionPopCtrl = [ '$scope', function($scope) {

    if ($scope.property.value == undefined && $scope.property.value == null)
    {
        $scope.property.value = '';
    }

    $scope.save = function() {

        $scope.updatePropertyInModel($scope.property);

        if($scope.property_variable){
            if($scope.property.value == '${CreditManagement.UseCredit(execution)}'){
                $scope.property_variable.value = 'CreditUsed';
            }else if($scope.property.value == '${CreditManagement.ReleaseCredit(execution)}'){
                $scope.property_variable.value = 'CreditReleased';
            }
            $scope.updatePropertyInModel($scope.property_variable);
        }

        $scope.close();
    };

    // Close button handler
    $scope.close = function() {
        $scope.property.mode = 'read';
        $scope.$hide();
    };

    $scope.updatePropertyInModel = function (property, shapeId) {

        var shape = $scope.selectedShape;
        // Some updates may happen when selected shape is already changed, so when an additional
        // shapeId is supplied, we need to make sure the correct shape is updated (current or previous)
        if (shapeId) {
            if (shape.id != shapeId && $scope.previousSelectedShape && $scope.previousSelectedShape.id == shapeId) {
                shape = $scope.previousSelectedShape;
            } else {
                shape = null;
            }
        }

        if (!shape) {
            // When no shape is selected, or no shape is found for the alternative
            // shape ID, do nothing
            return;
        }
        var key = property.key;
        var newValue = property.value;
        var oldValue = shape.properties[key];

        if (newValue != oldValue) {
            var commandClass = ORYX.Core.Command.extend({
                construct: function () {
                    this.key = key;
                    this.oldValue = oldValue;
                    this.newValue = newValue;
                    this.shape = shape;
                    this.facade = $scope.editor;
                },
                execute: function () {
                    this.shape.setProperty(this.key, this.newValue);
                    this.facade.getCanvas().update();
                    this.facade.updateSelection();
                },
                rollback: function () {
                    this.shape.setProperty(this.key, this.oldValue);
                    this.facade.getCanvas().update();
                    this.facade.updateSelection();
                }
            });
            // Instantiate the class
            var command = new commandClass();

            // Execute the command
            $scope.editor.executeCommands([command]);
            $scope.editor.handleEvents({
                type: ORYX.CONFIG.EVENT_PROPWINDOW_PROP_CHANGED,
                elements: [shape],
                key: key
            });

            // Switch the property back to read mode, now the update is done
            property.mode = 'read';

            // Fire event to all who is interested
            // Fire event to all who want to know about this
            var event = {
                type: KISBPM.eventBus.EVENT_TYPE_PROPERTY_VALUE_CHANGED,
                property: property,
                oldValue: oldValue,
                newValue: newValue
            };
            KISBPM.eventBus.dispatch(event.type, event);
        } else {
            // Switch the property back to read mode, no update was needed
            property.mode = 'read';
        }
    }
}];