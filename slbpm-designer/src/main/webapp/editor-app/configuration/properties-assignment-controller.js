/*
 * Activiti Modeler component part of the Activiti project
 * Copyright 2005-2014 Alfresco Software, Ltd. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * Assignment
 */

var KisBpmAssignmentCtrl = [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        backdrop: "static",
        keyboard: false,
        template:  'editor-app/configuration/properties/assignment-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];

var KisBpmAssignmentPopupCtrl = [ '$scope','$translate','$http', function($scope,$translate,$http) {

    $scope.groupConfig = {
        data: [],
        placeholder: '尚无数据'
    };
    // getCandidateGroups
    $http({
        method: 'POST',
        data: {},
        ignoreErrors: true,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        transformRequest: function (obj) {
            var str = [];
            for (var p in obj) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        },
        url: KISBPM.URL.getCandidateGroups()
    }).success(function (data, status, headers, config) {
        // console.log(data);
        var data = data.result;
        for(var i=0;i<data.length;i++){
            var item = {};
            item.id = data[i].role_id;
            item.text = data[i].role_name;
            if(data[i].user_name_list != '' && data[i].user_name_list != null){
                item.text += "("+data[i].user_name_list+")";
            }
            $scope.groupConfig.data.push(item);
        }
    }).error(function (data, status, headers, config) {
        console.log('Something went wrong when get candidateGroups:' + JSON.stringify(data));
    }).then(function(){

        //避免执行顺序错误
        $scope.sign_type = $scope.property_multiinstance_type.value ? $scope.property_multiinstance_type.value : "None";

        // Put json representing assignment on scope
        if ($scope.property.value !== undefined && $scope.property.value !== null
            && $scope.property.value.assignment !== undefined
            && $scope.property.value.assignment !== null)
        {
            $scope.assignment = $scope.property.value.assignment;
        } else {
            $scope.assignment = {};
        }

        if ($scope.assignment.candidateUsers == undefined || $scope.assignment.candidateUsers.length == 0)
        {
            $scope.assignment.candidateUsers = [{value: ''}];
        }

        // Click handler for + button after enum value
        var userValueIndex = 1;
        $scope.addCandidateUserValue = function(index) {
            $scope.assignment.candidateUsers.splice(index + 1, 0, {value: 'value ' + userValueIndex++});
        };

        // Click handler for - button after enum value
        $scope.removeCandidateUserValue = function(index) {
            $scope.assignment.candidateUsers.splice(index, 1);
        };

        if ($scope.assignment.candidateGroups == undefined || $scope.assignment.candidateGroups.length == 0)
        {
            $scope.assignment.candidateGroups = [{value: ''}];
        }

        var groupValueIndex = 1;
        $scope.addCandidateGroupValue = function(index) {
            $scope.assignment.candidateGroups.splice(index + 1, 0, {value: ''});
        };

        // Click handler for - button after enum value
        $scope.removeCandidateGroupValue = function(index) {
            $scope.assignment.candidateGroups.splice(index, 1);
        };

        $scope.save = function() {

            if ($scope.assignment.candidateGroups.length < 1
                || !$scope.assignment.candidateGroups[0].value) {
                alert('节点设置保存前必须选择至少一个审批角色');
                return;
            }

            $scope.property.value = {};
            handleAssignmentInput($scope);
            $scope.property.value.assignment = $scope.assignment;

            var sign_type_select_list = document.getElementsByName("signTypeSelect");
            var sign_type_index = sign_type_select_list[0].selectedIndex;
            var sign_type_str = sign_type_select_list[0].options[sign_type_index].value;

            var decision_type_select_list = document.getElementsByName("decisionTypeSelect");
            var decision_type_index = decision_type_select_list[0].selectedIndex;
            var decision_type_str = decision_type_select_list[0].options[decision_type_index].value;

            var vote_type_select_list = document.getElementsByName("voteTypeSelect");
            var vote_type_index = vote_type_select_list[0].selectedIndex;
            var vote_type_str = vote_type_select_list[0].options[vote_type_index].value;

            var condition_str = document.getElementById("condition").value;

            console.log("act_id:" + $scope.property_id.value);
            console.log(decision_type_str + " " + vote_type_str + " " + condition_str);

            var params = {
                task_def_key: $scope.property_id.value,
                decision_type: decision_type_str,
                vote_type: vote_type_str,
                condition: condition_str
            };

            // Update
            $http({
                method: 'POST',
                data: params,
                ignoreErrors: true,
                headers: {'Content-Type': 'application/json;charset=UTF-8'},
                url: KISBPM.URL.setSignSetting()
            }).success(function (data, status, headers, config) {

                if(sign_type_str == "Parallel") {
                    $scope.property_multiinstance_collection.value = "${CounterUserAssignment.getSignUser(execution)}";
                    $scope.property_multiinstance_variable.value = "assignee";
                    $scope.property_multiinstance_condition.value = "${CounterUserAssignment.isComplete(execution)}";
                    $scope.property.value.assignment.assignee = "${assignee}";

                    if ($scope.property_id.value == null || $scope.property_id.value.length == 0) {
                        $scope.property_id.value = "Parallel-" + new Date().getTime();
                    }
                }
                else {
                    $scope.property_multiinstance_collection.value = "";
                    $scope.property_multiinstance_variable.value = "";
                    $scope.property_multiinstance_condition.value = "";
                    $scope.property.value.assignment.assignee = "";
                }
                $scope.property_multiinstance_type.value = sign_type_str;
                $scope.updatePropertyInModel($scope.property_multiinstance_collection);
                $scope.updatePropertyInModel($scope.property_multiinstance_variable);
                $scope.updatePropertyInModel($scope.property_multiinstance_condition);
                $scope.updatePropertyInModel($scope.property_multiinstance_type);
                $scope.updatePropertyInModel($scope.property_id);
                $scope.updatePropertyInModel($scope.property);
                $scope.close();
            })
            .error(function (data, status, headers, config) {
                $scope.error = {};
                console.log('Something went wrong when updating the counter sign rule:' + JSON.stringify(data));
                $scope.status.loading = false;
            });
        };

        // Close button handler
        $scope.close = function() {
            handleAssignmentInput($scope);
            $scope.property.mode = 'read';
            $scope.$hide();
        };

        var handleAssignmentInput = function($scope) {
            if ($scope.assignment.candidateUsers)
            {
                var emptyUsers = true;
                var toRemoveIndexes = [];
                for (var i = 0; i < $scope.assignment.candidateUsers.length; i++)
                {
                    if ($scope.assignment.candidateUsers[i].value != '')
                    {
                        emptyUsers = false;
                    }
                    else
                    {
                        toRemoveIndexes[toRemoveIndexes.length] = i;
                    }
                }

                for (var i = 0; i < toRemoveIndexes.length; i++)
                {
                    $scope.assignment.candidateUsers.splice(toRemoveIndexes[i], 1);
                }

                if (emptyUsers)
                {
                    $scope.assignment.candidateUsers = undefined;
                }
            }

            if ($scope.assignment.candidateGroups)
            {
                var emptyGroups = true;
                var toRemoveIndexes = [];
                for (var i = 0; i < $scope.assignment.candidateGroups.length; i++)
                {
                    //wangfp 检查输入的小组名称是否存在
                    // if ($scope.assignment.candidateGroups[i].value != '' && groupList.indexOf($scope.assignment.candidateGroups[i].value) != -1)
                    if ($scope.assignment.candidateGroups[i].value != '')
                    {
                        emptyGroups = false;
                    }
                    else
                    {
                        toRemoveIndexes[toRemoveIndexes.length] = i;
                    }
                }

                for (var i = 0; i < toRemoveIndexes.length; i++)
                {
                    $scope.assignment.candidateGroups.splice(toRemoveIndexes[i], 1);
                }

                if (emptyGroups)
                {
                    $scope.assignment.candidateGroups = undefined;
                }
            }
        };

        $scope.updatePropertyInModel = function (property, shapeId) {

            var shape = $scope.selectedShape;
            // Some updates may happen when selected shape is already changed, so when an additional
            // shapeId is supplied, we need to make sure the correct shape is updated (current or previous)
            if (shapeId) {
                if (shape.id != shapeId && $scope.previousSelectedShape && $scope.previousSelectedShape.id == shapeId) {
                    shape = $scope.previousSelectedShape;
                } else {
                    shape = null;
                }
            }

            if (!shape) {
                // When no shape is selected, or no shape is found for the alternative
                // shape ID, do nothing
                return;
            }
            var key = property.key;
            var newValue = property.value;
            var oldValue = shape.properties[key];

            if (newValue != oldValue) {
                var commandClass = ORYX.Core.Command.extend({
                    construct: function () {
                        this.key = key;
                        this.oldValue = oldValue;
                        this.newValue = newValue;
                        this.shape = shape;
                        this.facade = $scope.editor;
                    },
                    execute: function () {
                        this.shape.setProperty(this.key, this.newValue);
                        this.facade.getCanvas().update();
                        this.facade.updateSelection();
                    },
                    rollback: function () {
                        this.shape.setProperty(this.key, this.oldValue);
                        this.facade.getCanvas().update();
                        this.facade.updateSelection();
                    }
                });
                // Instantiate the class
                var command = new commandClass();

                // Execute the command
                $scope.editor.executeCommands([command]);
                $scope.editor.handleEvents({
                    type: ORYX.CONFIG.EVENT_PROPWINDOW_PROP_CHANGED,
                    elements: [shape],
                    key: key
                });

                // Switch the property back to read mode, now the update is done
                property.mode = 'read';

                // Fire event to all who is interested
                // Fire event to all who want to know about this
                var event = {
                    type: KISBPM.eventBus.EVENT_TYPE_PROPERTY_VALUE_CHANGED,
                    property: property,
                    oldValue: oldValue,
                    newValue: newValue
                };
                KISBPM.eventBus.dispatch(event.type, event);
            } else {
                // Switch the property back to read mode, no update was needed
                property.mode = 'read';
            }

        };
    });

    var params = { task_def_key: $scope.property_id.value };

    $http({
        method: 'POST',
        data: params,
        ignoreErrors: true,
        headers: {'Content-Type': 'application/json;charset=UTF-8'},
        url: KISBPM.URL.getSignSetting()
    }).success(function (data, status, headers, config) {
        console.log(data);
        if (data.result) {
            var data = data.result;
            $scope.decision_type = data.decision_type;
            $scope.vote_type = data.vote_type;
            $scope.condition = data.condition;
        }
    }).error(function (data, status, headers, config) {
        console.log('Something went wrong when get getSignSetting:' + JSON.stringify(data));
    });
}];