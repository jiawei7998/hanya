/*
 * Activiti Modeler component part of the Activiti project
 * Copyright 2005-2014 Alfresco Software, Ltd. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

/*
 * Condition expression
 */

var KisBpmConditionExpressionCtrl = [ '$scope', '$modal', function($scope, $modal) {

    // Config for the modal window
    var opts = {
        backdrop: "static",
        keyboard: false,
        template:  'editor-app/configuration/properties/condition-expression-popup.html?version=' + Date.now(),
        scope: $scope
    };

    // Open the dialog
    $modal(opts);
}];

var KisBpmConditionExpressionPopupCtrl = [ '$scope', '$translate', '$http', function($scope, $translate, $http) {

    //客户数据列表
    $scope.clientConfig = {
        data: [],
        placeholder: '尚无数据'
    };

    // getCustomers
    $http({
        method: 'POST',
        data: {},
        ignoreErrors: true,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        transformRequest: function (obj) {
            var str = [];
            for (var p in obj) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        },
        url: KISBPM.URL.getFlowDict()
    }).success(function (data, status, headers, config) {
         console.log(data);
        var data = data.result;
        for(var i=0;i<data.length;i++){
			var dictConfig = data[i];
			$scope[dictConfig.dictId] = dictConfig.dictItems;
        }
    }).error(function (data, status, headers, config) {
        console.log('Something went wrong when get customers:' + JSON.stringify(data));
    }).then(function(){     //避免执行顺序错误
		//操作数据列表 
       	//删除 modify by shenzl 2021年6月30日  由TT_FLOW_DICT进行保存
        //数学符号列表
        var typeListInit = [">=","<=","==",">","<","(in)"];
        var conditionExpressionStr = "";
        // Put json representing condition on scope
        if ($scope.property.value !== undefined && $scope.property.value !== null) {

            $scope.conditionExpression = {value: $scope.property.value};
            //截取规则字符串，去掉开始的"${"和结尾的"}"
            conditionExpressionStr = $scope.property.value.substring(2,$scope.property.value.length-1);
        } else {
            $scope.conditionExpression = {value: ''};
        }
        console.log("截取后的初始化规则:"+conditionExpressionStr);
        //定义规则列表
        $scope.rules = [];
        //根据"&&"分割
        var rulesList = conditionExpressionStr.split('&&');
        for(var i=0;i<rulesList.length;i++){
            for(var j=0;j<typeListInit.length;j++){
                if(rulesList[i].indexOf(typeListInit[j])!=-1){  //若规则中包含该数学符号
                    var ruleList = rulesList[i].split(typeListInit[j]);
                    if(ruleList[0]=='order.party_bigkind'){
                        var ruleObject = {
                            "name": ruleList[0],
                            "type": typeListInit[j],
                            "value": JSON.parse(ruleList[1])
                        };
                    }else{
                        var ruleObject = {
                            "name": ruleList[0],
                            "type": typeListInit[j],
                            "value": ruleList[1]
                        };
                    }

                    $scope.rules.push(ruleObject);
                    break;
                }
            }
        }
        //若规则数为0,则向规则列表中加入空对象
        if($scope.rules.length == 0){
            $scope.rules = [{}];
        }

        $scope.save = function() {

            //根据属性名称分别获取元素列表
            var name_select_list = document.getElementsByName("nameSelect");
            var type_select_list = document.getElementsByName("typeSelect");
            var value_select_list = document.getElementsByName("valueSelect");
            var risk_select_list = document.getElementsByName("riskSelect");
            var rating_select_list = document.getElementsByName("ratingSelect");
            var dir_select_list = document.getElementsByName("dirSelect");
            var bond_select_list = document.getElementsByName("bondSelect");
            var currency_select_list = document.getElementsByName("currencySelect");
            var trader_select_list = document.getElementsByName("traderSelect");
            var limitAmount_select_list = document.getElementsByName("limitAmountSelect");

            var rule_str = "${";
            var temp = false;

            //拼接规则
            for(var i=0;i<name_select_list.length;i++){

                var name_index = name_select_list[i].selectedIndex;
                var name_str = name_select_list[i].options[name_index].value;
                var type_index = type_select_list[i].selectedIndex;
                var type_str = type_select_list[i].options[type_index].value;
                var value_str = value_select_list[i].value.trim();

                var risk_index = risk_select_list[i].selectedIndex;
                var risk_str = '';
                if(risk_index > -1){
                	risk_str = risk_select_list[i].options[risk_index].value;
                }

                var rating_index = rating_select_list[i].selectedIndex;
                var rating_str = '';
                if(rating_index > -1) {
                	rating_str = rating_select_list[i].options[rating_index].value;
                }

                var dir_index = dir_select_list[i].selectedIndex;
                var dir_str = '';
                if(dir_index > -1) {
                	dir_str = dir_select_list[i].options[dir_index].value;
                }

                var bond_index = bond_select_list[i].selectedIndex;
                var bond_str = '';
                if(bond_index > -1){
                	bond_str = bond_select_list[i].options[bond_index].value;
                }

                var currency_index = currency_select_list[i].selectedIndex;
                var currency_str = '';
                if(currency_index > -1) {
                    currency_str = currency_select_list[i].options[currency_index].value;
                }
                
                var trader_index = trader_select_list[i].selectedIndex;
                var trader_str = '';
                if(trader_index > -1) {
                    trader_str = trader_select_list[i].options[trader_index].value;
                }

				var limitAmount_index = limitAmount_select_list[i].selectedIndex;
                var limitAmount_str = '';
                if(limitAmount_index > -1) {
                    limitAmount_str = limitAmount_select_list[i].options[limitAmount_index].value;
                }

                //判断是否是完整的规则
                if(name_str!=null && name_str!='' && type_str!=null && type_str!=''){
                    if(name_str == "OrderManagement.getTotalAmount(execution)"
                        && value_str!=null && value_str!='' ){
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += value_str;
                        rule_str += "&&";
                        temp = true;
                    }else if(name_str == "OrderManagement.getRiskDegree(execution)"
                        && risk_str!=null && risk_str!=''){
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += risk_str;
                        rule_str += "&&";
                        temp = true;
                    }else if(name_str == "OrderManagement.getBondRating(execution)"
                        && rating_str!=null && rating_str!='') {
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += rating_str;
                        rule_str += "&&";
                        temp = true;
                    }else if(name_str == "OrderManagement.getMyDir(execution)"
                        && dir_str!=null && dir_str!=''){
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += dir_str;
                        rule_str += "&&";
                        temp = true;
                    }else if(name_str == "OrderManagement.getBondProperties(execution)"
                        && bond_str!=null && bond_str!=''){
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += bond_str;
                        rule_str += "&&";
                        temp = true;
                    }else if(name_str == "OrderManagement.getTlCurrency(execution)"
                        && currency_str!=null && currency_str!='') {
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += currency_str;
                        rule_str += "&&";
                        temp = true;
                    }else if(name_str == "OrderManagement.isTrader(execution)"
                        && trader_str!=null && trader_str!='') {
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += trader_str;
                        rule_str += "&&";
                        temp = true;
                    }else if(name_str == "OrderManagement.isLimitAmount(execution)"
                        && limitAmount_str!=null && limitAmount_str!='') {
                        rule_str += name_str;
                        rule_str += type_str;
                        rule_str += limitAmount_str;
                        rule_str += "&&";
                        temp = true;
                    }
                }
            }
            if(rule_str.length>2){
                rule_str = rule_str.substring(0,rule_str.length-2);
            }
            rule_str += "}";
            //判断是否有规则加入
            if(temp){
                $scope.conditionExpression.value = rule_str;
            }else{
                $scope.conditionExpression.value = '';
            }
            console.log("点击保存的规则："+$scope.conditionExpression.value);
            $scope.property.value = $scope.conditionExpression.value;
            $scope.updatePropertyInModel($scope.property);
            $scope.close();
        };

        // Close button handler
        $scope.close = function() {
            $scope.property.mode = 'read';
            $scope.$hide();
        };

        //wangfp 添加规则
        $scope.addRule = function(index) {
            $scope.rules.splice(index + 1, 0, {value: ''});
        };

        //wangfp 移除规则
        $scope.removeRule = function(index) {
            $scope.rules.splice(index, 1);
        };

        //wangfp 规则类型修改事件
        $scope.changeName = function(index) {
            var ruleList = $scope.rules;
            var name_select_list = document.getElementsByName("nameSelect");
            for(var i=0;i<ruleList.length;i++){
                if(i==index){
                    var name_index = name_select_list[i].selectedIndex;
                    var name_str = name_select_list[i].options[name_index].value;
                    $scope.rules[i].name = name_str;
                    $scope.rules[i].value = '';
                    $scope.rules[i].type = '';
                    // console.log($scope.rules[i].name);
                }
            }
        };

        $scope.updatePropertyInModel = function (property, shapeId) {

            var shape = $scope.selectedShape;
            // Some updates may happen when selected shape is already changed, so when an additional
            // shapeId is supplied, we need to make sure the correct shape is updated (current or previous)
            if (shapeId) {
                if (shape.id != shapeId && $scope.previousSelectedShape && $scope.previousSelectedShape.id == shapeId) {
                    shape = $scope.previousSelectedShape;
                } else {
                    shape = null;
                }
            }

            if (!shape) {
                // When no shape is selected, or no shape is found for the alternative
                // shape ID, do nothing
                return;
            }
            var key = property.key;
            var newValue = property.value;
            var oldValue = shape.properties[key];

            if (newValue != oldValue) {
                var commandClass = ORYX.Core.Command.extend({
                    construct: function () {
                        this.key = key;
                        this.oldValue = oldValue;
                        this.newValue = newValue;
                        this.shape = shape;
                        this.facade = $scope.editor;
                    },
                    execute: function () {
                        this.shape.setProperty(this.key, this.newValue);
                        this.facade.getCanvas().update();
                        this.facade.updateSelection();
                    },
                    rollback: function () {
                        this.shape.setProperty(this.key, this.oldValue);
                        this.facade.getCanvas().update();
                        this.facade.updateSelection();
                    }
                });
                // Instantiate the class
                var command = new commandClass();

                // Execute the command
                $scope.editor.executeCommands([command]);
                $scope.editor.handleEvents({
                    type: ORYX.CONFIG.EVENT_PROPWINDOW_PROP_CHANGED,
                    elements: [shape],
                    key: key
                });

                // Switch the property back to read mode, now the update is done
                property.mode = 'read';

                // Fire event to all who is interested
                // Fire event to all who want to know about this
                var event = {
                    type: KISBPM.eventBus.EVENT_TYPE_PROPERTY_VALUE_CHANGED,
                    property: property,
                    oldValue: oldValue,
                    newValue: newValue
                };
                KISBPM.eventBus.dispatch(event.type, event);
            } else {
                // Switch the property back to read mode, no update was needed
                property.mode = 'read';
            }
        }
    });
}];

