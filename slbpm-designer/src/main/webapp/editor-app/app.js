/*
 * Activiti Modeler component part of the Activiti project
 * Copyright 2005-2014 Alfresco Software, Ltd. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
'use strict';

var activitiModeler = angular.module('activitiModeler', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngDragDrop',
  'mgcrea.ngStrap', 
  'ngGrid',
  'ngAnimate',
  'pascalprecht.translate',
  'duScroll'
]);

var activitiModule = activitiModeler;

activitiModule.directive('datepicker', function () {
    return {
        restrict: 'A',
        scope: {
            ngShow: '=',
        },
        link: function (scope, element, attrs) {
            // 初始化
            var $element = $(element);

            //初始化控件
            $element.datepicker({//添加日期选择功能
                numberOfMonths:1,//显示几个月
                showButtonPanel:true,//是否显示按钮面板
                dateFormat: 'yy-mm-dd',//日期格式
                clearText:"清除",//清除日期的按钮名称
                closeText:"关闭",//关闭选择框的按钮名称
                yearSuffix: '年', //年的后缀
                showMonthAfterYear:true,//是否把月放在年的后面
                monthNames: ['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月'],
                dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
                dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
                dayNamesMin: ['日','一','二','三','四','五','六'],
                gotoCurrent : false,
                initStatus : '请选择日期',
                inline: true,
                showOtherMonths: true,
                firstDay: 1,
                currentText: '今天',
                prevText : '上月',
                nextText : '下月'
                // changeYear: true
            }).datepicker('widget').wrap('<div class="ll-skin-latoja"/>');

        }
    }
});

//wangfp select2插件
activitiModule.directive('select2', function (select2Query) {
    return {
        restrict: 'A',
        scope: {
            config: '=',
            ngModel: '=',
            select2Model: '='
        },
        link: function (scope, element, attrs) {
            // 初始化
            var tagName = element[0].tagName,
                config = {
                    allowClear: true,
                    multiple: !!attrs.multiple,
                    placeholder: attrs.placeholder || ' '   // 修复不出现删除按钮的情况
                };

            // 生成select
            if(tagName === 'SELECT') {
                // 初始化
                var $element = $(element);
                delete config.multiple;

                angular.extend(config, scope.config);
                $element
                    .prepend('<option value=""></option>')
                    .val('')
                    .select2(config);

                // model - view
                scope.$watch('ngModel', function (newVal) {
                    setTimeout(function () {
                        $element.find('[value^="?"]').remove();    // 清除错误的数据
                        $element.select2('val', newVal);
                    },0);
                }, true);
                return false;
            }

            // 处理input
            if(tagName === 'INPUT') {
                // 初始化
                var $element = $(element);

                // 获取内置配置
                if(attrs.query) {
                    scope.config = select2Query[attrs.query]();
                }

                // 动态生成select2
                scope.$watch('config', function () {
                    angular.extend(config, scope.config);
                    $element.select2('destroy').select2(config);
                }, true);

                // view - model
                $element.on('change', function () {
                    scope.$apply(function () {
                        scope.select2Model = $element.select2('data');
                    });
                });

                // model - view
                scope.$watch('select2Model', function (newVal) {
                    if(isJSON(newVal)){
                        $element.select2('data', JSON.parse(newVal));
                    }
                }, true);

                // model - view
                scope.$watch('ngModel', function (newVal) {
                    // 跳过ajax方式以及多选情况
                    if(config.ajax || config.multiple) { return false }

                    $element.select2('val', newVal);
                }, true);
            }

            //wangfp 判断是否是Json字符串
            function isJSON (str, pass_object) {
                if (pass_object && isObject(str)) return true;

                if (!isString(str)) return false;

                str = str.replace(/\s/g, '').replace(/\n|\r/, '');

                if (/^\{(.*?)\}$/.test(str))
                    return /"(.*?)":(.*?)/g.test(str);

                if (/^\[(.*?)\]$/.test(str)) {
                    return str.replace(/^\[/, '')
                        .replace(/\]$/, '')
                        .replace(/},{/g, '}\n{')
                        .split(/\n/)
                        .map(function (s) { return isJSON(s); })
                        .reduce(function (prev, curr) { return !!curr; });
                }

                return false;
            }

            function isString(str){
                if(typeof str=="string"){
                    return true;
                }else{
                    return false;
                }
            }
        }
    }
});

/**
 * select2 内置查询功能
 */
activitiModule.factory('select2Query', function ($timeout) {
    return {
        testAJAX: function () {
            // var config = {
            //     minimumInputLength: 1,
            //     ajax: {
            //         url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
            //         dataType: 'jsonp',
            //         data: function (term) {
            //             return {
            //                 q: term,
            //                 page_limit: 10,
            //                 apikey: "ju6z9mjyajq2djue3gbvv26t"
            //             };
            //         },
            //         results: function (data, page) {
            //             return {results: data.movies};
            //         }
            //     },
            //     formatResult: function (data) {
            //         return data.title;
            //     },
            //     formatSelection: function (data) {
            //         return data.title;
            //     }
            // };
            //
            // return config;
        }
    }
});

activitiModeler
  // Initialize routes
  .config(['$selectProvider', '$translateProvider', function ($selectProvider, $translateProvider) {

      // Override caret for bs-select directive
      angular.extend($selectProvider.defaults, {
          caretHtml: '&nbsp;<i class="icon icon-caret-down"></i>'
      });
        
        // Initialize angular-translate
        $translateProvider.useStaticFilesLoader({
            prefix: './editor-app/i18n/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');

        // remember language
        $translateProvider.useCookieStorage();
        
  }])
  .run(['$rootScope', '$timeout', '$modal', '$translate', '$location', '$window', '$http', '$q',
        function($rootScope, $timeout, $modal, $translate, $location, $window, $http, $q) {
	  
			  $rootScope.config = ACTIVITI.CONFIG;
			  
			  $rootScope.editorInitialized = false;
		      
		      $rootScope.editorFactory = $q.defer();
		
		      $rootScope.forceSelectionRefresh = false;
		
		      $rootScope.ignoreChanges = false; // by default never ignore changes
		      
		      $rootScope.validationErrors = [];
		      
		      $rootScope.staticIncludeVersion = Date.now();

			  /**
		       * A 'safer' apply that avoids concurrent updates (which $apply allows).
		       */
		      $rootScope.safeApply = function(fn) {
		          var phase = this.$root.$$phase;
		          if(phase == '$apply' || phase == '$digest') {
		              if(fn && (typeof(fn) === 'function')) {
		                  fn();
		              }
		          } else {
		              this.$apply(fn);
		          }
		      };
	  
	  
            /**
             * Initialize the event bus: couple all Oryx events with a dispatch of the
             * event of the event bus. This way, it gets much easier to attach custom logic
             * to any event.
             */

            /* Helper method to fetch model from server (always needed) */
            function fetchModel(modelId) {

                var modelUrl = KISBPM.URL.getModel(modelId);

                $http({method: 'GET', url: modelUrl, headers: {'Pragma': 'no-cache','Cache-Control':'no-cache'}}).
                    success(function (data, status, headers, config) {
                        $rootScope.editor = new ORYX.Editor(data);
                        $rootScope.modelData = angular.fromJson(data);
                        $rootScope.editorFactory.resolve();
                    }).
                    error(function (data, status, headers, config) {
                      console.log('Error loading model with id ' + modelId + ' ' + data);
                    });
            }


            function initScrollHandling() {
                var canvasSection = jQuery('#canvasSection');
                canvasSection.scroll(function() {

                    // Hides the resizer and quick menu items during scrolling

                    var selectedElements = $rootScope.editor.selection;
                    var subSelectionElements = $rootScope.editor._subSelection;

                    $rootScope.selectedElements = selectedElements;
                    $rootScope.subSelectionElements = subSelectionElements;
                    if (selectedElements && selectedElements.length > 0) {
                    	$rootScope.selectedElementBeforeScrolling = selectedElements[0];
                    }

                    jQuery('.Oryx_button').each(function(i, obj) {
                    	$rootScope.orginalOryxButtonStyle = obj.style.display;
                    	obj.style.display = 'none';
                    });
                    
                    jQuery('.resizer_southeast').each(function(i, obj) {
                    	$rootScope.orginalResizerSEStyle = obj.style.display;
                        obj.style.display = 'none';
                    });
                    jQuery('.resizer_northwest').each(function(i, obj) {
                    	$rootScope.orginalResizerNWStyle = obj.style.display;
                        obj.style.display = 'none';
                    });
                    $rootScope.editor.handleEvents({type:ORYX.CONFIG.EVENT_CANVAS_SCROLL});
                });

                canvasSection.scrollStopped(function(){

                    // Puts the quick menu items and resizer back when scroll is stopped.

                    $rootScope.editor.setSelection([]); // needed cause it checks for element changes and does nothing if the elements are the same
                    $rootScope.editor.setSelection($rootScope.selectedElements, $rootScope.subSelectionElements);
                    $rootScope.selectedElements = undefined;
                    $rootScope.subSelectionElements = undefined;

                    function handleDisplayProperty(obj) {
                        if (jQuery(obj).position().top > 0) {
                            obj.style.display = 'block';
                        } else {
                            obj.style.display = 'none';
                        }
                    }

                    jQuery('.Oryx_button').each(function(i, obj) {
                        handleDisplayProperty(obj);
                    });
                    
                    jQuery('.resizer_southeast').each(function(i, obj) {
                        handleDisplayProperty(obj);
                    });
                    jQuery('.resizer_northwest').each(function(i, obj) {
                        handleDisplayProperty(obj);
                    });

                });
            }

            /**
             * Initialize the Oryx Editor when the content has been loaded
             */
            $rootScope.$on('$includeContentLoaded', function (event) {
	            if (!$rootScope.editorInitialized) {

	            	ORYX._loadPlugins();
	
	                var modelId = EDITOR.UTIL.getParameterByName('modelId');
	                fetchModel(modelId);
	
	                $rootScope.window = {};
	                var updateWindowSize = function() {
	                    $rootScope.window.width = $window.innerWidth;
	                    $rootScope.window.height  = $window.innerHeight;
	                };
	
	                // Window resize hook
	                angular.element($window).bind('resize', function() {
	                    $rootScope.safeApply(updateWindowSize());
	                });
	
	                $rootScope.$watch('window.forceRefresh', function(newValue) {
	                    if(newValue) {
	                        $timeout(function() {
	                            updateWindowSize();
	                            $rootScope.window.forceRefresh = false;
	                        });
	                    }
	                });
	
	                updateWindowSize();

	                // Hook in resizing of main panels when window resizes
	                // TODO: perhaps move to a separate JS-file?
	                jQuery(window).resize(function () {

	                    // Calculate the offset based on the bottom of the module header
	                    var offset = jQuery("#editor-header").offset();
	                    var propSectionHeight = jQuery('#propertySection').height();
	                    var canvas = jQuery('#canvasSection');
	                    var mainHeader = jQuery('#main-header');

	                    if (offset == undefined || offset === null
	                        || propSectionHeight === undefined || propSectionHeight === null
	                        || canvas === undefined || canvas === null || mainHeader === null) {
	                        return;
	                    }
	                    
	                    if ($rootScope.editor)
	                	{
	        	        	var selectedElements = $rootScope.editor.selection;
	        	            var subSelectionElements = $rootScope.editor._subSelection;
	        	
	        	            $rootScope.selectedElements = selectedElements;
	        	            $rootScope.subSelectionElements = subSelectionElements;
	        	            if (selectedElements && selectedElements.length > 0)
	        	            {
	        	            	$rootScope.selectedElementBeforeScrolling = selectedElements[0];
	        	            	
	        	            	$rootScope.editor.setSelection([]); // needed cause it checks for element changes and does nothing if the elements are the same
	        	                $rootScope.editor.setSelection($rootScope.selectedElements, $rootScope.subSelectionElements);
	        	                $rootScope.selectedElements = undefined;
	        	                $rootScope.subSelectionElements = undefined;
	        	            }
	                	}

	                    var totalAvailable = jQuery(window).height() - offset.top - mainHeader.height() - 21;
	                    canvas.height(totalAvailable - propSectionHeight);
	                    jQuery('#paletteSection').height(totalAvailable);

	                    // Update positions of the resize-markers, according to the canvas

	                    var actualCanvas = null;
	                    if (canvas && canvas[0].children[1]) {
	                        actualCanvas = canvas[0].children[1];
	                    }

	                    var canvasTop = canvas.position().top;
	                    var canvasLeft = canvas.position().left;
	                    var canvasHeight = canvas[0].clientHeight;
	                    var canvasWidth = canvas[0].clientWidth;
	                    var iconCenterOffset = 8;
	                    var widthDiff = 0;

	                    var actualWidth = 0;
	                    if(actualCanvas) {
	                        // In some browsers, the SVG-element clientwidth isn't available, so we revert to the parent
	                        actualWidth = actualCanvas.clientWidth || actualCanvas.parentNode.clientWidth;
	                    }


	                    if(actualWidth < canvas[0].clientWidth) {
	                        widthDiff = actualWidth - canvas[0].clientWidth;
	                        // In case the canvas is smaller than the actual viewport, the resizers should be moved
	                        canvasLeft -= widthDiff / 2;
	                        canvasWidth += widthDiff;
	                    }

	                    var iconWidth = 17;
	                    var iconOffset = 20;

	                    var north = jQuery('#canvas-grow-N');
	                    north.css('top', canvasTop + iconOffset + 'px');
	                    north.css('left', canvasLeft - 10 + (canvasWidth - iconWidth) / 2 + 'px');

	                    var south = jQuery('#canvas-grow-S');
	                    south.css('top', (canvasTop + canvasHeight - iconOffset - iconCenterOffset) +  'px');
	                    south.css('left', canvasLeft - 10 + (canvasWidth - iconWidth) / 2 + 'px');

	                    var east = jQuery('#canvas-grow-E');
	                    east.css('top', canvasTop - 10 + (canvasHeight - iconWidth) / 2 + 'px');
	                    east.css('left', (canvasLeft + canvasWidth - iconOffset - iconCenterOffset) + 'px');

	                    var west = jQuery('#canvas-grow-W');
	                    west.css('top', canvasTop -10 + (canvasHeight - iconWidth) / 2 + 'px');
	                    west.css('left', canvasLeft + iconOffset + 'px');

	                    north = jQuery('#canvas-shrink-N');
	                    north.css('top', canvasTop + iconOffset + 'px');
	                    north.css('left', canvasLeft + 10 + (canvasWidth - iconWidth) / 2 + 'px');

	                    south = jQuery('#canvas-shrink-S');
	                    south.css('top', (canvasTop + canvasHeight - iconOffset - iconCenterOffset) +  'px');
	                    south.css('left', canvasLeft +10 + (canvasWidth - iconWidth) / 2 + 'px');

	                    east = jQuery('#canvas-shrink-E');
	                    east.css('top', canvasTop + 10 + (canvasHeight - iconWidth) / 2 +  'px');
	                    east.css('left', (canvasLeft + canvasWidth - iconOffset - iconCenterOffset) + 'px');

	                    west = jQuery('#canvas-shrink-W');
	                    west.css('top', canvasTop + 10 + (canvasHeight - iconWidth) / 2 + 'px');
	                    west.css('left', canvasLeft + iconOffset + 'px');
	                });

	                jQuery(window).trigger('resize');

	                jQuery.fn.scrollStopped = function(callback) {
	                    jQuery(this).scroll(function(){
	                        var self = this, $this = jQuery(self);
	                        if ($this.data('scrollTimeout')) {
	                            clearTimeout($this.data('scrollTimeout'));
	                        }
	                        $this.data('scrollTimeout', setTimeout(callback,50,self));
	                    });
	                };
	                
	                // Always needed, cause the DOM element on which the scroll event listeners are attached are changed for every new model
	                initScrollHandling();
	                
	                $rootScope.editorInitialized = true;
	            }
            });

            /**
             * Initialize the event bus: couple all Oryx events with a dispatch of the
             * event of the event bus. This way, it gets much easier to attach custom logic
             * to any event.
             */

            $rootScope.editorFactory.promise.then(function() {

                KISBPM.eventBus.editor = $rootScope.editor;

                var eventMappings = [
                    { oryxType : ORYX.CONFIG.EVENT_SELECTION_CHANGED, kisBpmType : KISBPM.eventBus.EVENT_TYPE_SELECTION_CHANGE },
                    { oryxType : ORYX.CONFIG.EVENT_DBLCLICK, kisBpmType : KISBPM.eventBus.EVENT_TYPE_DOUBLE_CLICK },
                    { oryxType : ORYX.CONFIG.EVENT_MOUSEOUT, kisBpmType : KISBPM.eventBus.EVENT_TYPE_MOUSE_OUT },
                    { oryxType : ORYX.CONFIG.EVENT_MOUSEOVER, kisBpmType : KISBPM.eventBus.EVENT_TYPE_MOUSE_OVER }

                ];

                eventMappings.forEach(function(eventMapping) {
                    $rootScope.editor.registerOnEvent(eventMapping.oryxType, function(event) {
                        KISBPM.eventBus.dispatch(eventMapping.kisBpmType, event);
                    });
                });
                
                $rootScope.editor.registerOnEvent(ORYX.CONFIG.EVENT_SHAPEREMOVED, function (event) {
    	    		var validateButton = document.getElementById(event.shape.resourceId + "-validate-button");
    	    		if (validateButton)
    	    		{
    	    			validateButton.style.display = 'none';
    	    		}
                });

                // The Oryx canvas is ready (we know since we're in this promise callback) and the
                // event bus is ready. The editor is now ready for use
                KISBPM.eventBus.dispatch(KISBPM.eventBus.EVENT_TYPE_EDITOR_READY, {type : KISBPM.eventBus.EVENT_TYPE_EDITOR_READY});

            });
            
            // Alerts
            $rootScope.alerts = {
                queue: []
            };
          
            $rootScope.showAlert = function(alert) {
                if(alert.queue.length > 0) {
                    alert.current = alert.queue.shift();
                    // Start timout for message-pruning
                    alert.timeout = $timeout(function() {
                        if (alert.queue.length == 0) {
                            alert.current = undefined;
                            alert.timeout = undefined;
                        } else {
                            $rootScope.showAlert(alert);
                        }
                    }, (alert.current.type == 'error' ? 5000 : 1000));
                } else {
                    $rootScope.alerts.current = undefined;
                }
            };
          
            $rootScope.addAlert = function(message, type) {
                var newAlert = {message: message, type: type};
                if (!$rootScope.alerts.timeout) {
                    // Timeout for message queue is not running, start one
                    $rootScope.alerts.queue.push(newAlert);
                    $rootScope.showAlert($rootScope.alerts);
                } else {
                    $rootScope.alerts.queue.push(newAlert);
                }
            };
          
            $rootScope.dismissAlert = function() {
                if (!$rootScope.alerts.timeout) {
                    $rootScope.alerts.current = undefined;
                } else {
                    $timeout.cancel($rootScope.alerts.timeout);
                    $rootScope.alerts.timeout = undefined;
                    $rootScope.showAlert($rootScope.alerts);
                }
            };
          
            $rootScope.addAlertPromise = function(promise, type) {
                if (promise) {
                    promise.then(function(data) {
                        $rootScope.addAlert(data, type);
                    });
                }
            };
          
        }
  ])

    // Moment-JS date-formatting filter
    .filter('dateformat', function() {
        return function(date, format) {
            if (date) {
                if (format) {
                    return moment(date).format(format);
                } else {
                    return moment(date).calendar();
                }
            }
            return '';
        };
    });