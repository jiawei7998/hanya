package com.singlee.financial.cdtc;

import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/**
 * Cdtc模块业务处理类
 * @author 
 * @version 
 * @since JDK1.6,Hessian4.0.37
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/cdtcDMSGProcessJobServer")
public interface CdtcDMSGProcessJobServer {
	
	/***
	 *  定时在cdtc前置机取文件
	 * @return
	 * @throws Exception
	 */
	public RetBean readDMSGXmlFiles() throws Exception;
	

}
