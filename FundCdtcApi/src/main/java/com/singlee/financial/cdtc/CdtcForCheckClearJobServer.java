package com.singlee.financial.cdtc;

import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/**
 * Cdtc模块业务处理类
 * @author 
 * @version 
 * @since JDK1.6,Hessian4.0.37
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcForCheckClearJobServer")
public interface CdtcForCheckClearJobServer {
	/***
	 *  summit资金债券结算业务与中债登待结算业务核对要素
	 * @return
	 * @throws Exception
	 */
	public RetBean doQuartzJobCheck() throws Exception ;
	
	
	public RetBean validCdtcWorkTime()throws Exception ;
}

