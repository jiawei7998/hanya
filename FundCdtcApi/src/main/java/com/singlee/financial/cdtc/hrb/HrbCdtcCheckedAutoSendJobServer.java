package com.singlee.financial.cdtc.hrb;

import java.util.Map;

import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/**
 * Cdtc模块业务处理类
 * @author 
 * @version 
 * @since JDK1.6,Hessian4.0.37
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/HrbCdtcCheckedAutoSendJobServer")
public interface HrbCdtcCheckedAutoSendJobServer {
	/***
	 *  自动进行核算业务，定时发送核对完成的确认指令报文
	 * @return
	 * @throws Exception
	 */
	public RetBean autoSendCheckedXml(Map<String, Object> params) throws Exception ;
}
