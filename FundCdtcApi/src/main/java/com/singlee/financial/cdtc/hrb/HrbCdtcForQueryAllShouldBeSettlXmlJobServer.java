package com.singlee.financial.cdtc.hrb;

import java.util.Map;

import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/**
 * Cdtc模块业务处理类
 * @author 
 * @version 
 * @since JDK1.6,Hessian4.0.37
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/HrbCdtcForQueryAllShouldBeSettlXmlJobServer")
public interface HrbCdtcForQueryAllShouldBeSettlXmlJobServer {

	/***
	 *  定时向中债综合结算业务系统查询当日批量待结算业务
	 * @return
	 * @throws Exception
	 */
	public RetBean queryCdtcDetailToOracleDatabaseXmlTables(Map<String, Object> params) throws Exception;
	/***
	 *  定时向中债综合结算业务系统查询当日批量待结算业务
	 * @return
	 * @throws Exception
	 */
	public RetBean doPayProcessAutoJobsForConstract(Map<String, Object> params) throws Exception;
}
