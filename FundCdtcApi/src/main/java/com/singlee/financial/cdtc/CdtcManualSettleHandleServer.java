package com.singlee.financial.cdtc;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/***
 * 
 * 手工结算-经办
 *
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcManualSettleHandleServer")
public interface CdtcManualSettleHandleServer {
	
	public SmtOutBean settlInterCdtcMsg(BtchQryRslt btchQryRslt) throws RemoteConnectFailureException,Exception;

}
