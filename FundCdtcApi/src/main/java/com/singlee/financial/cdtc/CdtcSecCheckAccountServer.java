package com.singlee.financial.cdtc;

import java.util.List;
import java.util.Map;
import com.singlee.financial.bean.CdtcAcctTotalBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/***
 * 
 * 中债直连-债券总对账单
 *
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcSecCheckAccountServer")
public interface CdtcSecCheckAccountServer {
	/***
	 * 查询总对账单
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	List<CdtcAcctTotalBean> getSecCheckAccount(Map<String, Object> map);

	/***
	 * 对比总对账单
	 */
	String compareSecCheckAccount(Map<String, Object> map);
}
