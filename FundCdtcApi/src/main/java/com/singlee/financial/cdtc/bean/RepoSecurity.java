package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class RepoSecurity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String secid;
	private String secidnm;
	
	private BigDecimal qty;
	
	private BigDecimal commprocamt;
	
	private BigDecimal matprocamt;

	/**
	 * @return the secid
	 */
	public String getSecid() {
		return secid;
	}

	/**
	 * @param secid the secid to set
	 */
	public void setSecid(String secid) {
		this.secid = secid;
	}

	/**
	 * @return the qty
	 */
	public BigDecimal getQty() {
		return qty;
	}

	/**
	 * @param qty the qty to set
	 */
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getSecidnm() {
		return secidnm;
	}

	public void setSecidnm(String secidnm) {
		this.secidnm = secidnm;
	}

	public BigDecimal getCommprocamt() {
		return commprocamt;
	}

	public void setCommprocamt(BigDecimal commprocamt) {
		this.commprocamt = commprocamt;
	}

	public BigDecimal getMatprocamt() {
		return matprocamt;
	}

	public void setMatprocamt(BigDecimal matprocamt) {
		this.matprocamt = matprocamt;
	}


}
