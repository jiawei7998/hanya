package com.singlee.financial.cdtc;

import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/**
 * Cdtc模块业务处理类
 * @author 
 * @version 
 * @since JDK1.6,Hessian4.0.37
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcCheckedAutoSendJobServer")
public interface CdtcCheckedAutoSendJobServer {
	/***
	 *  自动进行核算业务，定时发送核对完成的确认指令报文
	 * @return
	 * @throws Exception
	 */
	public RetBean autoSendCheckedXml() throws Exception ;
}
