package com.singlee.financial.cdtc.hrb;

import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName HrbCdtcTestConnectServer.java
 * @Description 测试连通性
 * @createTime 2021年11月23日 16:31:00
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/cdtcTestConnectServer")
public interface HrbCdtcTestConnectServer {

    /**
     * 创建心跳报文
     * @return
     * @throws Exception
     */
    public RetBean createHertMessageXml(Map<String,Object> paramer) throws Exception;

    /**
     * 发送心跳报文
     * @return
     * @throws Exception
     */
    public RetBean sendHertMessageXml(Map<String,Object> paramer) throws Exception;
}
