package com.singlee.financial.cdtc;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcSecurityBatchSettlServer")
public interface CdtcSecurityBatchSettlServer {
	
	/***
	 * 中债直连 - CDTC业务批量查询
	 * @param 
	 * @return
	 * @throws Exception
	 */
	public List<BtchQryRslt> securityBatchSettlQuery(Map<String, Object> map) throws Exception;
	

}
