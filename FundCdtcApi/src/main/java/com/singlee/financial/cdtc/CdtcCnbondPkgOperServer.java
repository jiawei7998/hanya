package com.singlee.financial.cdtc;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.bean.CnbondPkgDetailBean;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;


/***
 * 中债直连 - CDTC 结算报文管理
 * @param 
 * @return
 * @throws Exception
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcCnbondPkgOperServer")
public interface CdtcCnbondPkgOperServer {
	
	/***
	 * 结算报文查询-列表页面
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	public Pair<SmtOutBean,List<AbstractSlIntfcCnbondPkgId>> queryCdtcCnbondPkg(Map<String, String> map) throws  Exception;
	
	/***
	 * 结算报文查询-详情页面
	 * @param AbstractSlIntfcCnbondPkgId
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	public Pair<SmtOutBean,CnbondPkgDetailBean> getCnbondPkgDetail(AbstractSlIntfcCnbondPkgId cbst1) throws  Exception;
	
	
	/***
	 * 获取结算报文业务类型
	 * @param AbstractSlIntfcCnbondPkgId
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	public Pair<SmtOutBean,String> getCnbondPkgBizTp(AbstractSlIntfcCnbondPkgId cbst1) throws  Exception;
	
	
	
}
