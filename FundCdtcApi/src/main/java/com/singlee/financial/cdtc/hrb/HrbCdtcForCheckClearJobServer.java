package com.singlee.financial.cdtc.hrb;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import com.singlee.financial.cdtc.bean.RepoSecurity;
import com.singlee.financial.cdtc.bean.SlCnbondlinkSecurDeal;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/**
 * Cdtc模块业务处理类
 * @author 
 * @version 

 * @since JDK1.6,Hessian4.0.37
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/HrbCdtcForCheckClearJobServer")
public interface HrbCdtcForCheckClearJobServer {
	/***
	 *  资金债券结算业务与中债登待结算业务核对要素
	 * @param getRepoList 
	 * @throws Exception
	 */
	public RetBean doQuartzJobCheck(Map<String, Object> params, Function<SlCnbondlinkSecurDeal, List<RepoSecurity>> getRepoList) throws Exception ;
	
	
	public RetBean validCdtcWorkTime(boolean isHolyDay)throws Exception ;



}

