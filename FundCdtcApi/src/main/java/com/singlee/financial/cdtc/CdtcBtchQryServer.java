package com.singlee.financial.cdtc;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CtrctBtchQryServer")
public interface CdtcBtchQryServer {
	/**
	 * 合同批量查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<BtchQryRslt> getCtrctBtchQry(Map<String, Object> map) throws  Exception;
	/**
	 * 结算综合查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<BtchQryRslt> getSettleQueryManage(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询合同详情
	 * @param map
	 * @return
	 */
	ManualSettleDetail getCtrctDetails(Map<String, Object> map);
	
	
	public RetBean validCdtcConnectTime(String type) throws Exception ;
	
}
