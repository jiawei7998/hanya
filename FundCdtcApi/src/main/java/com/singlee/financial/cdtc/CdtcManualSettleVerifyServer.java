package com.singlee.financial.cdtc;

import java.util.List;
import java.util.Map;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

/***
 * 
 * 手工结算-复核
 *
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcManualSettleVerifyServer")
public interface CdtcManualSettleVerifyServer {
	
	/**
	 * 结算复核
	 * @return
	 * @throws Exception
	 */
	public SmtOutBean settlVerfiyCdtcMsg(BtchQryRslt btchQryRslt) throws RemoteConnectFailureException,Exception ;
	
	
	/**
	 * 结算经办撤销
	 * @return
	 * @throws Exception
	 */
	public SmtOutBean cancelSettlCdtcMsg(BtchQryRslt btchQryRslt) throws RemoteConnectFailureException,Exception;
	
	/***
	 * 根据结算指令查询结算实体信息（BtchQryRslt）
	 * @param map
	 */
	public List<BtchQryRslt> selectBtchQryRslt(Map<String,Object> map)throws RemoteConnectFailureException,Exception;

}
