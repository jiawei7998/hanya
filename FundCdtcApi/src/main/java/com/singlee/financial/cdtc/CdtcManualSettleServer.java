package com.singlee.financial.cdtc;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;
/***
 * 
 * 中债直连-手工结算
 *
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcManualSettleServer")
public interface CdtcManualSettleServer {
	
	
	/**
	 * 指令批量查询-列表页面
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<BtchQryRslt> getManualSettleListQuery(Map<String, Object> map) throws Exception;
	
	
	
	/**
     * 指令批量下载
     * @param map
     * @return
     * @throws RemoteConnectFailureException
     * @throws Exception
     */
    List<BtchQryRslt> getSettleList(Map<String, Object> map) throws Exception;
	
	/**
	 * 指令查询-详情页面
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	Pair<SmtOutBean,ManualSettleDetail> getManualSettleDetailQuery(Map<String, Object> map) throws  Exception;

}
