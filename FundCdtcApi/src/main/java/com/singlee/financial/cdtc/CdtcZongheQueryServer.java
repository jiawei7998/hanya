package com.singlee.financial.cdtc;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.hessian.annotation.Cdtc;
import com.singlee.hessian.annotation.CdtcContext;

import java.util.Map;

/***
 * 
 * 中债直连-结算综合查询-详情查询
 *
 */
@Cdtc(context = CdtcContext.SINGLEE_API, uri = "/CdtcZongheQueryServer")
public interface CdtcZongheQueryServer {
	
	Pair<SmtOutBean,ManualSettleDetail> getZongheDetailQuery(Map<String, Object> map) throws  Exception;

}
