package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

public class AbstractSlIntfcCnbondPkgId implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String msgsn;        //编号
	private String msgfilename;  //报文名称
	private String msgtype;      //报文类型
	private String msgxml;       //报文体数据
	private Date msgtime;        //报文创建时间
	private Date msgopdt;
	private String msgfedealno;
	
	private String bizTp;  //业务类型
    private String cust;   //对手方

	// Constructors

	/**
	 * @return the msgopdt
	 */
	public Date getMsgopdt() {
		return msgopdt;
	}

	/**
	 * @param msgopdt the msgopdt to set
	 */
	public void setMsgopdt(Date msgopdt) {
		this.msgopdt = msgopdt;
	}

	/** default constructor */
	public AbstractSlIntfcCnbondPkgId() {
	}

	/** full constructor */
	public AbstractSlIntfcCnbondPkgId(String msgsn,String msgfilename, String msgtype,
			String msgxml, Date msgtime) {
		this.msgsn = msgsn;
		this.msgfilename = msgfilename;
		this.msgtype = msgtype;
		this.msgxml = msgxml;
		this.msgtime = msgtime;
	}

	// Property accessors

	public String getMsgsn() {
		return this.msgsn;
	}

	public void setMsgsn(String msgsn) {
		this.msgsn = msgsn;
	}

	public String getMsgtype() {
		return this.msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}
	public String getMsgfilename() {
		return msgfilename;
	}

	public void setMsgfilename(String msgfilename) {
		this.msgfilename = msgfilename;
	}
	public String getMsgxml() {
		return this.msgxml;
	}

	public void setMsgxml(String msgxml) {
		this.msgxml = msgxml;
	}

	public Date getMsgtime() {
		return this.msgtime;
	}

	public void setMsgtime(Date msgtime) {
		this.msgtime = msgtime;
	}

	/**
	 * @return the msgfedealno
	 */
	public String getMsgfedealno() {
		return msgfedealno;
	}

	/**
	 * @param msgfedealno the msgfedealno to set
	 */
	public void setMsgfedealno(String msgfedealno) {
		this.msgfedealno = msgfedealno;
	}

	public String getBizTp() {
		return bizTp;
	}

	public void setBizTp(String bizTp) {
		this.bizTp = bizTp;
	}

	public String getCust() {
		return cust;
	}

	public void setCust(String cust) {
		this.cust = cust;
	}

}
