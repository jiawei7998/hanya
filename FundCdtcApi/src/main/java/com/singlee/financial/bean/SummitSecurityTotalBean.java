package com.singlee.financial.bean;



import java.io.Serializable;
import java.math.BigDecimal;

public class SummitSecurityTotalBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String SECSACCT;
	
	private String SECID;
	
	private String SECIDNM;
	
	private String CCY;
	
	private String SETTDATE;
	
	private BigDecimal QTYAMT;
	
	private Character T;
	
	/**
	 * @return the t
	 */
	public Character getT() {
		return T;
	}
	/**
	 * @param t the t to set
	 */
	public void setT(Character t) {
		T = t;
	}
	public SummitSecurityTotalBean()
	{
		
	}
	/**
	 * @return the sECSACCT
	 */
	public String getSECSACCT() {
		return SECSACCT;
	}
	/**
	 * @param secsacct the sECSACCT to set
	 */
	public void setSECSACCT(String secsacct) {
		SECSACCT = secsacct;
	}
	/**
	 * @return the sECID
	 */
	public String getSECID() {
		return SECID;
	}
	/**
	 * @param secid the sECID to set
	 */
	public void setSECID(String secid) {
		SECID = secid;
	}
	/**
	 * @return the cCY
	 */
	public String getCCY() {
		return CCY;
	}
	/**
	 * @param ccy the cCY to set
	 */
	public void setCCY(String ccy) {
		CCY = ccy;
	}
	
	/**
	 * @return the sETTDATE
	 */
	public String getSETTDATE() {
		return SETTDATE;
	}
	/**
	 * @param settdate the sETTDATE to set
	 */
	public void setSETTDATE(String settdate) {
		SETTDATE = settdate;
	}
	/**
	 * @return the qTYAMT
	 */
	public BigDecimal getQTYAMT() {
		return QTYAMT;
	}
	/**
	 * @param qtyamt the qTYAMT to set
	 */
	public void setQTYAMT(BigDecimal qtyamt) {
		QTYAMT = qtyamt;
	}
	/**
	 * @return the sECIDNM
	 */
	public String getSECIDNM() {
		return SECIDNM;
	}
	/**
	 * @param secidnm the sECIDNM to set
	 */
	public void setSECIDNM(String secidnm) {
		SECIDNM = secidnm;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getSECSACCT().toUpperCase().toString();
	}

}
