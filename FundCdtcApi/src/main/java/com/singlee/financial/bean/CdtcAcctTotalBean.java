package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 
 * 查询债券总对账单 -实体
 *
 */
public class CdtcAcctTotalBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1247615832756825913L;
	
	/**
	 * 债券简称
	 */
	private String secidNm;
	/**
	 * 债券代码
	 */
	private String secid;
	/**
	 * 可用科目 万元
	 */
	private BigDecimal ableUseAmt;
	/**
	 * 待付科目 万元
	 */
	private BigDecimal waitPayAmt;
	/**
	 * 质押科目 万元
	 */
	private BigDecimal repoZYAmt;
	/**
	 * 待购回科目 万元
	 */
	private BigDecimal repoRevAmt;
	/**
	 * 冻结科目
	 */
	private BigDecimal frozenAmt;
	/**
	 * 债券余额 万元
	 */
	private BigDecimal totalAmt;
	
	/**
	 * 承销额度
	 * @return
	 */
	private BigDecimal cxAmt;
	
	/**
	 * 承销额度待付
	 * @return
	 */
	private BigDecimal cxWaitPayAmt;
	
	
	public String getSecidNm() {
		return secidNm;
	}
	public void setSecidNm(String secidNm) {
		this.secidNm = secidNm;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public BigDecimal getAbleUseAmt() {
		return ableUseAmt;
	}
	public void setAbleUseAmt(BigDecimal ableUseAmt) {
		this.ableUseAmt = ableUseAmt;
	}
	public BigDecimal getWaitPayAmt() {
		return waitPayAmt;
	}
	public void setWaitPayAmt(BigDecimal waitPayAmt) {
		this.waitPayAmt = waitPayAmt;
	}
	public BigDecimal getRepoZYAmt() {
		return repoZYAmt;
	}
	public void setRepoZYAmt(BigDecimal repoZYAmt) {
		this.repoZYAmt = repoZYAmt;
	}
	public BigDecimal getRepoRevAmt() {
		return repoRevAmt;
	}
	public void setRepoRevAmt(BigDecimal repoRevAmt) {
		this.repoRevAmt = repoRevAmt;
	}
	public BigDecimal getFrozenAmt() {
		return frozenAmt;
	}
	public void setFrozenAmt(BigDecimal frozenAmt) {
		this.frozenAmt = frozenAmt;
	}
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	public BigDecimal getCxAmt() {
		return cxAmt;
	}
	public void setCxAmt(BigDecimal cxAmt) {
		this.cxAmt = cxAmt;
	}
	public BigDecimal getCxWaitPayAmt() {
		return cxWaitPayAmt;
	}
	public void setCxWaitPayAmt(BigDecimal cxWaitPayAmt) {
		this.cxWaitPayAmt = cxWaitPayAmt;
	}
	
	

}
