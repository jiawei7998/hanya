package com.singlee.financial.bean;

import java.io.Serializable;

/***
 * 
 * 结算报文详情bean（三种业务类型总bean）
 * 第一种：基本债券
 * 第二种：质押式回购
 * 第三种：买断式回购
 *
 */
public class CnbondPkgDetailBean implements Serializable{
	
	private static final long serialVersionUID = 5828751518037440296L;
	
	/***
	 * 以下是三种业务类型基本属性
	 */
	private String InstrId;//结算指令标识
	private String BizTp;//业务类型
	private String TxRsltCd;//处理结果状态
	private String InstrSts;//指令处理状态
	private String CtrCnfrmInd;//对手方确认标识
	private String OrgtrCnfrmInd;//发令方确认标识
	private String InstrOrgn;//指令来源 
	private String TxId;//业务标识号  
	private String Val1;//结算金额1（基本债券：净价金额）（质押式回购：首期金额）
	private String Val2;//结算金额2 （基本债券：全价金额）（质押式回购：到期金额）
	private String Dt1;//交割日1   （基本债券：交割日期）（质押式回购：首期交割日期）
	private String Dt2;//交割日2 （质押式回购：到期交割日期）
	private String SttlmTp1;//结算方式1  （基本债券：结算方式）（质押式回购：首期结算方式）
	private String SttlmTp2;//结算方式2  （质押式回购：到期结算方式）
	private String TakAcctId;//收券方账户id  （基本债券：买入方债券账号）（质押式回购：逆回购方账号）
	private String TakAcctNm;//收券方账户名称    （基本债券：买入方简称）（质押式回购：逆回购方简称）
	private String GivAcctId;//付券方账户id   （基本债券：卖出方债券账号）（质押式回购：正回购方账号）
	private String GivAcctNm;//付券方账户名称    （基本债券：卖出方简称）（质押式回购：正回购方简称）
	private String Chckr;//操作员
	private String Oprtr;//操作员
	
	/***
	 * 以下是[质押式回购]特有
	 */
	private String Rate;//回购利率
	private String AggtFaceAmt;//全面金额    
	/***
	 * 以下是[基本债券]特有
	 */
	private String Pric1;//净价（百元面值）
	private String Pric2;//全价（百元面值）
	private String BdAmt; //债券总额
	
	/***
	 * 以下是[买断式回购]特有
	 */
	private String GrteTp;//担保方式
	private String NBdId;//逆回购方保证券
	private String NBdShrtNm;//逆回购方保证券名
	private String NBdAmt;//逆回购方保证券额
	private String ZBdId;//正回购方保证券
	private String ZBdShrtNm;//正回购方保证券名
	private String ZBdAmt;//正回购方保证券额
	
	
	
	
	
	
	public String getInstrId() {
		return InstrId;
	}
	public void setInstrId(String instrId) {
		InstrId = instrId;
	}
	public String getBizTp() {
		return BizTp;
	}
	public void setBizTp(String bizTp) {
		BizTp = bizTp;
	}
	public String getTxRsltCd() {
		return TxRsltCd;
	}
	public void setTxRsltCd(String txRsltCd) {
		TxRsltCd = txRsltCd;
	}
	public String getCtrCnfrmInd() {
		return CtrCnfrmInd;
	}
	public void setCtrCnfrmInd(String ctrCnfrmInd) {
		CtrCnfrmInd = ctrCnfrmInd;
	}
	public String getOrgtrCnfrmInd() {
		return OrgtrCnfrmInd;
	}
	public void setOrgtrCnfrmInd(String orgtrCnfrmInd) {
		OrgtrCnfrmInd = orgtrCnfrmInd;
	}
	public String getInstrOrgn() {
		return InstrOrgn;
	}
	public void setInstrOrgn(String instrOrgn) {
		InstrOrgn = instrOrgn;
	}
	public String getTxId() {
		return TxId;
	}
	public void setTxId(String txId) {
		TxId = txId;
	}
	public String getVal2() {
		return Val2;
	}
	public void setVal2(String val2) {
		Val2 = val2;
	}
	public String getDt2() {
		return Dt2;
	}
	public void setDt2(String dt2) {
		Dt2 = dt2;
	}
	public String getInstrSts() {
		return InstrSts;
	}
	public void setInstrSts(String instrSts) {
		InstrSts = instrSts;
	}
	public String getChckr() {
		return Chckr;
	}
	public void setChckr(String chckr) {
		Chckr = chckr;
	}
	public String getOprtr() {
		return Oprtr;
	}
	public void setOprtr(String oprtr) {
		Oprtr = oprtr;
	}
	public String getAggtFaceAmt() {
		return AggtFaceAmt;
	}
	public void setAggtFaceAmt(String aggtFaceAmt) {
		AggtFaceAmt = aggtFaceAmt;
	}
	public String getVal1() {
		return Val1;
	}
	public void setVal1(String val1) {
		Val1 = val1;
	}
	public String getDt1() {
		return Dt1;
	}
	public void setDt1(String dt1) {
		Dt1 = dt1;
	}
	public String getSttlmTp1() {
		return SttlmTp1;
	}
	public void setSttlmTp1(String sttlmTp1) {
		SttlmTp1 = sttlmTp1;
	}
	public String getSttlmTp2() {
		return SttlmTp2;
	}
	public void setSttlmTp2(String sttlmTp2) {
		SttlmTp2 = sttlmTp2;
	}
	public String getRate() {
		return Rate;
	}
	public void setRate(String rate) {
		Rate = rate;
	}
	public String getTakAcctId() {
		return TakAcctId;
	}
	public void setTakAcctId(String takAcctId) {
		TakAcctId = takAcctId;
	}
	public String getGivAcctId() {
		return GivAcctId;
	}
	public void setGivAcctId(String givAcctId) {
		GivAcctId = givAcctId;
	}
	public String getGivAcctNm() {
		return GivAcctNm;
	}
	public void setGivAcctNm(String givAcctNm) {
		GivAcctNm = givAcctNm;
	}
	public String getTakAcctNm() {
		return TakAcctNm;
	}
	public void setTakAcctNm(String takAcctNm) {
		TakAcctNm = takAcctNm;
	}
	public String getPric1() {
		return Pric1;
	}
	public void setPric1(String pric1) {
		Pric1 = pric1;
	}
	public String getPric2() {
		return Pric2;
	}
	public void setPric2(String pric2) {
		Pric2 = pric2;
	}
	public String getBdAmt() {
		return BdAmt;
	}
	public void setBdAmt(String bdAmt) {
		BdAmt = bdAmt;
	}
	public String getGrteTp() {
		return GrteTp;
	}
	public void setGrteTp(String grteTp) {
		GrteTp = grteTp;
	}
	public String getNBdId() {
		return NBdId;
	}
	public void setNBdId(String nBdId) {
		NBdId = nBdId;
	}
	public String getNBdShrtNm() {
		return NBdShrtNm;
	}
	public void setNBdShrtNm(String nBdShrtNm) {
		NBdShrtNm = nBdShrtNm;
	}
	public String getNBdAmt() {
		return NBdAmt;
	}
	public void setNBdAmt(String nBdAmt) {
		NBdAmt = nBdAmt;
	}
	public String getZBdId() {
		return ZBdId;
	}
	public void setZBdId(String zBdId) {
		ZBdId = zBdId;
	}
	public String getZBdShrtNm() {
		return ZBdShrtNm;
	}
	public void setZBdShrtNm(String zBdShrtNm) {
		ZBdShrtNm = zBdShrtNm;
	}
	public String getZBdAmt() {
		return ZBdAmt;
	}
	public void setZBdAmt(String zBdAmt) {
		ZBdAmt = zBdAmt;
	}
	
	
}
