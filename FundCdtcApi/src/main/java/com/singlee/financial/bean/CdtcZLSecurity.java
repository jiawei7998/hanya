package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 
 * 中债债券相关信息
 *
 */
public class CdtcZLSecurity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3048087324606596427L;

	private String secid;//债券ID
	
	private String secidNm;//债券名称
	
	private BigDecimal faceAmt;//面值
	
	private BigDecimal accrualAmt;//应计利息
	
	private BigDecimal accrualTotalAmt;//应计利息总和

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getSecidNm() {
		return secidNm;
	}

	public void setSecidNm(String secidNm) {
		this.secidNm = secidNm;
	}

	public BigDecimal getFaceAmt() {
		return faceAmt;
	}

	public void setFaceAmt(BigDecimal faceAmt) {
		this.faceAmt = faceAmt;
	}

	public BigDecimal getAccrualAmt() {
		return accrualAmt;
	}

	public void setAccrualAmt(BigDecimal accrualAmt) {
		this.accrualAmt = accrualAmt;
	}

	public BigDecimal getAccrualTotalAmt() {
		return accrualTotalAmt;
	}

	public void setAccrualTotalAmt(BigDecimal accrualTotalAmt) {
		this.accrualTotalAmt = accrualTotalAmt;
	}
	
	

}
