package com.singlee.financial.bean;

import java.io.Serializable;

import com.singlee.financial.pojo.component.RetStatusEnum;

/**
 * 输出参数类
 * 
 * @author chenxh
 * 
 */
public class SmtOutBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5214334334567177010L;

	/**
	 * 返回状态
	 */
	private RetStatusEnum retStatus;

	/**
	 * 返回代码
	 */
	private String retCode;

	/**
	 * 返回描述信息
	 */
	private String retMsg;

	private String clientNo;

	/**
	 * 核心记账流水号
	 */
	private String tellSeqNo;
	
	private Integer successCount;
	
	private String importLog;

	/**
	 * 构造方法.已重载,无参方法
	 */
	public SmtOutBean() {
		super();
	}

	/**
	 * 构造方法.已重载,有参方法
	 * 
	 * @param retStatus
	 *            处理状态
	 * @param retCode
	 *            处理状态代码
	 * @param retMsg
	 *            处理返回描述信息
	 */

	public SmtOutBean(RetStatusEnum retStatus, String retCode, String retMsg) {
		super();
		this.retStatus = retStatus;
		this.retCode = retCode;
		this.retMsg = retMsg;
	}

	public Integer getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(Integer successCount) {
		this.successCount = successCount;
	}

	public String getImportLog() {
		return importLog;
	}

	public void setImportLog(String importLog) {
		this.importLog = importLog;
	}

	public RetStatusEnum getRetStatus() {
		return retStatus;
	}

	public void setRetStatus(RetStatusEnum retStatus) {
		this.retStatus = retStatus;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		super.toString();
		StringBuilder builder = new StringBuilder();
		builder.append("PacketRetBean [retCode=");
		builder.append(retCode);
		builder.append(", retMsg=");
		builder.append(retMsg);
		builder.append(", retStatus=");
		builder.append(retStatus);
		builder.append("]");
		return builder.toString();
	}

	public void setClientNo(String clientNo) {
		this.clientNo = clientNo;
	}

	public String getClientNo() {
		return clientNo;
	}

	public String getTellSeqNo() {
		return tellSeqNo;
	}

	public void setTellSeqNo(String tellSeqNo) {
		this.tellSeqNo = tellSeqNo;
	}

}
