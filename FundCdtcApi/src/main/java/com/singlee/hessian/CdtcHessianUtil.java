package com.singlee.hessian;

import java.net.MalformedURLException;
import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.hessian.annotation.CdtcContext;

public class CdtcHessianUtil {
	private static HessianProxyFactory factory = null;

	@SuppressWarnings("unchecked")
	public static <T> T getService(Class<T> clazz, String url) throws ClassNotFoundException{
		if (null==factory) { factory = new HessianProxyFactory(); }
		try {
			url= CdtcContext.SINGLEE_API.getRemoteUrl()+url;
			return (T) factory.create(url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
    
}
