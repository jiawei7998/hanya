package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.InterBankDepositBean;
import com.singlee.financial.pojo.InterestCashFlowBean;
import com.singlee.financial.pojo.component.PayFrequency;
import com.singlee.financial.pojo.component.SettPartyEnum;
import imix.imix10.ExecutionReport;
import imix.imix10.component.UndInstrmtGrp;
import imix.imix10.component.UnderlyingInstrument;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 同业存款解析处理类
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbIbdEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, InterBankDepositBean> {
    /***
     *
     * 同业存款
     * @param message
     * @param interBankDepositBean
     * @throws Exception
     */
    public void convert(ExecutionReport message, InterBankDepositBean interBankDepositBean, String bankId) throws Exception {
        // 设置本方机构及清算信息
        convert(message, bankId, SettPartyEnum.TheParty, interBankDepositBean.getInstitutionInfo(),
                interBankDepositBean.getSettInfo());
        // 设置对手方机构及清算信息
        convert(message, bankId, SettPartyEnum.CounterParty, interBankDepositBean.getContraPatryInstitutionInfo(),
                interBankDepositBean.getContraPartySettInfo());
        // 交易模式
        interBankDepositBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        interBankDepositBean.setExecId(message.isSetExecID() ? message.getExecID().getValue() : CfetsUtils.EMPTY); // CFETS成交编号
        interBankDepositBean.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);// 交易日期
        interBankDepositBean.setTradeTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : CfetsUtils.EMPTY);// 交易成交时间
        interBankDepositBean.setPs(convert(message.isSetSide() ? message.getSide() : null, interBankDepositBean.getSettInfo().getMarkerTakerRole())); // 交易方向
        //
        interBankDepositBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态

        interBankDepositBean.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码
        interBankDepositBean.setMarketIndicator(message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : CfetsUtils.EMPTY);
        interBankDepositBean.setSecurityId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码
        interBankDepositBean.setTenor(message.isSetSecurityTermString() ? message.getSecurityTermString().getValue() : CfetsUtils.EMPTY); // 存款期限
        interBankDepositBean.setHoldingDays(message.isSetCashHoldingDays() ? message.getCashHoldingDays().getUnionTypeValue() : CfetsUtils.EMPTY); // 实际占款天数
        interBankDepositBean.setRate(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null);// 利率
        interBankDepositBean.setAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null);// 交易币种金额

        PayFrequency convert = convert(message.isSetCouponPaymentFrequency() ? message.getCouponPaymentFrequency() : null);
        if (convert == null) {
            interBankDepositBean.setCouponPaymentFrequency(message.isSetCouponPaymentFrequency() ? message.getCouponPaymentFrequency().getValue() : null);
        } else {
            interBankDepositBean.setPayFrequency(convert(message.isSetCouponPaymentFrequency() ? message.getCouponPaymentFrequency() : null));//付息频率
        }

        interBankDepositBean.setFirstPayDate(message.isSetCouponPaymentDate() ? message.getCouponPaymentDate().getValue() : CfetsUtils.EMPTY);//首次付息日
        interBankDepositBean.setFirstSettleDate(message.isSetFirstCouponSettlDate() ? message.getFirstCouponSettlDate().getValue() : CfetsUtils.EMPTY);//首次结息日

        interBankDepositBean.setSettlDayAgreement(message.isSetCouponSettlDateAgreement() ? message.getCouponSettlDateAgreement().getValue() : CfetsUtils.EMPTY);//结息日约定
        interBankDepositBean.setPayDayAgreement(message.isSetInterestPaymentDateAgreement() ? message.getInterestPaymentDateAgreement().getValue() : CfetsUtils.EMPTY);//付息日约定

        interBankDepositBean.setInterestAmt(message.isSetAccruedInterestTotalAmt() ? new BigDecimal(message.getAccruedInterestTotalAmt().getValue()) : null); // 应计利息总金额
        interBankDepositBean.setMaturityAmt(message.isSetSettlCurrAmt2() ? new BigDecimal(message.getSettlCurrAmt2().getValue()) : null); // 到期还款金额
        interBankDepositBean.setValueDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null); // 首次结算日
        interBankDepositBean.setMaturityDate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);// 到期结算日

        //提前支取约定 1不允许提前支取 2允许全额 3 允许全额或部分
        interBankDepositBean.setAdvanceWithDrawAgreement(message.isSetAdvanceWithDrawAgreement() ? message.getAdvanceWithDrawAgreement().getValue() : CfetsUtils.EMPTY);
        interBankDepositBean.setText(message.isSetText() ? message.getText().getValue() : CfetsUtils.EMPTY);
        //存出方经办行
        interBankDepositBean.setManagingEntity2(message.isSetManagingEntity2() ? message.getManagingEntity2().getValue() : CfetsUtils.EMPTY);
        //存入方经办行
        interBankDepositBean.setManagingEntity1(message.isSetManagingEntity1() ? message.getManagingEntity1().getValue() : CfetsUtils.EMPTY);
        //数据类型标识
        interBankDepositBean.setDataCategoryIndicator(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator().getValue() : null);
        interBankDepositBean.setDcie(convert(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator() : null)); //数据类型标识 Excel导出使用

        UndInstrmtGrp undInstrmtGrp = new UndInstrmtGrp();
        undInstrmtGrp = message.getUndInstrmtGrp();
        UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
        int NoUnderlyings = undInstrmtGrp.isSetNoUnderlyings() ? undInstrmtGrp.getNoUnderlyings().getValue() : 0;
        UnderlyingInstrument inst = new UnderlyingInstrument();
        //第二部分
        List<InterestCashFlowBean> icfbList = new ArrayList<>();
        InterestCashFlowBean icfb = null;
        for (int i = 1; i <= NoUnderlyings; i++) {
            undInstrmtGrp.getGroup(i, noUnderlyings);
            inst = noUnderlyings.getUnderlyingInstrument();
            icfb = new InterestCashFlowBean();
            //付息日 按期付息的时候有值
            icfb.setIntPayDate(inst.isSetUnderlyingCouponPaymentDate() ? (inst.getUnderlyingCouponPaymentDate().getValue() == null ? CfetsUtils.EMPTY : inst.getUnderlyingCouponPaymentDate().getValue()) : CfetsUtils.EMPTY);
            //应计利息 按期付息的时候有值
            icfb.setAccruedIntAmt(new BigDecimal(inst.isSetUnderlyingAccruedInterestAmt() ? (inst.getUnderlyingAccruedInterestAmt().getValue() == null ? "0" : inst.getUnderlyingAccruedInterestAmt().getValue()) : "0"));
            icfbList.add(icfb);
        }
        interBankDepositBean.setIcfbList(icfbList);
        //提前支取预期利率
        int stipulations = message.isSetNoStipulations() ? message.getNoStipulations().getValue() : 0;
        for (int i = 1; i <= stipulations; i++) {
            ExecutionReport.NoStipulations noStipulations = new ExecutionReport.NoStipulations();
            message.getGroup(i, noStipulations);
            interBankDepositBean.setExpYield(noStipulations.isSetStipulationValue() ? new BigDecimal(noStipulations.getStipulationValue().getValue()) : null); // 到期收益率
        }
    }
}
