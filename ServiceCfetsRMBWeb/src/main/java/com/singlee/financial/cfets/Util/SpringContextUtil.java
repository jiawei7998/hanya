package com.singlee.financial.cfets.Util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
@Component("springContextUtil")
public class SpringContextUtil implements ApplicationContextAware{
	
	private  static ApplicationContext applicationContext=null;
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		 SpringContextUtil.applicationContext = applicationContext;

		
	}
	
	 public static ApplicationContext getApplicationContext(){
	        return applicationContext;
	 }
	 
	 public static <T> T getBean(Class<T> clazz){
	      return applicationContext.getBean(clazz);
	 }
	 



}
