package com.singlee.financial.cfets.rmb;

import imix.field.MarketIndicator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.DocumentFactoryHelper;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.singlee.financial.pojo.DepositsAndLoansBean;
import com.singlee.financial.pojo.FixedIncomeBean;
import com.singlee.financial.pojo.RepoCrBean;
import com.singlee.financial.pojo.RepoOrBean;
import com.singlee.financial.pojo.SecurityLendingBean;
import com.singlee.financial.pojo.SwapIrsBean;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.financial.pojo.trade.IrsDetailBean;
import com.singlee.financial.pojo.trade.IrsInfoBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.financial.pojo.trade.SecuritySettAmtBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

/**
 * 验收反馈excel文件生成
 * @author Administrator
 *
 */
public class ImixRmbFeedBack {
	//每个文件一把锁,多线程操作同一文件会把文件毁坏
	//利率互换
	public static final Lock irsLock = new ReentrantLock();
	//买断式回购
	public static final Lock orLock = new ReentrantLock();
	//现券买卖
	public static final Lock cbtLock = new ReentrantLock();
	//信用拆借
	public static final Lock iboLock = new ReentrantLock();
	//债券借贷
	public static final Lock slLock = new ReentrantLock();
	//质押式回购
	public static final Lock crLock = new ReentrantLock();
	
	//追加数据到excel
		/**
		 * 
		 * @param marketIndicator 由于即期远期数据交叉，所以需要区分
		 * @param tradeBean
		 * @throws IOException
		 */
		public static void addDataExcelAll(String marketIndicator,TradeBaseBean tradeBean,String basePath)throws IOException{
			/**
			 * 外币验收正常交易和异常交易分开下行，所以文件应该创建双份
			 * 注意：excel文件名称目前写死，必须有，不判断。
			 */
			//TODO 文件路径
			if("".equals(basePath)){
				File file = new File("feedbackFile");
				basePath=file.getCanonicalPath().replace("\\", "/")+"/";
			}
			if(marketIndicator.equals(MarketIndicator.INTEREST_RATE_SWAP)){
				//利率互换
				String excelPath=basePath+"CSTPBBIRS.xlsx";//分开写，提高速度
				irsLock.lock();
				try {
					addExcelIRS("利率互换",excelPath,(SwapIrsBean) tradeBean);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					irsLock.unlock();
				}
				
			}else if(marketIndicator.equals(MarketIndicator.OUTRIGHT_REPO)){
				//买断式回购
				String excelPath=basePath+"CSTPBBOR.xlsx";//分开写，提高速度
				orLock.lock();
				try {
					addExcelOR("买断式回购",excelPath,(RepoOrBean) tradeBean);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					orLock.unlock();
				}
			}else if(marketIndicator.equals(MarketIndicator.CASH_BOND)){
				//现券买卖
				String excelPath=basePath+"CSTPBBCBT.xlsx";//分开写，提高速度
				cbtLock.lock();
				try {
					addExcelCBT("现券买卖",excelPath,(FixedIncomeBean) tradeBean);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					cbtLock.unlock();
				}
				
			} else if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {
				//信用拆借
				String excelPath=basePath+"CSTPBBIBO.xlsx";//分开写，提高速度
				iboLock.lock();
				try {
					addExcelIBO("信用拆借",excelPath,(DepositsAndLoansBean) tradeBean);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					iboLock.unlock();
				}
			}else if (marketIndicator.equals(MarketIndicator.SECURITY_LENDING)) {
				//债券借贷
				String excelPath=basePath+"CSTPBBSL.xlsx";//分开写，提高速度
				slLock.lock();
				try {
					addExcelSL("债券借贷",excelPath,(SecurityLendingBean) tradeBean);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					slLock.unlock();
				}
			}else if (marketIndicator.equals(MarketIndicator.COLLATERAL_REPO)) {
				//质押回购
				String excelPath=basePath+"CSTPBBCR.xlsx";//分开写，提高速度
				crLock.lock();
				try {
					addExcelCR("质押回购",excelPath,(RepoCrBean) tradeBean);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					crLock.unlock();
				}
			}else {
				//信用拆借
				String excelPath=basePath+"CSTPBBIBO.xlsx";//分开写，提高速度
				iboLock.lock();
				try {
					addExcelIBO("信用拆借",excelPath,(DepositsAndLoansBean) tradeBean);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					iboLock.unlock();
				}
			}
				
		}
		public static void addDataExcelAllError(Map<String,String> map)throws IOException{
			/**
			 * 外币验收正常交易和异常交易分开下行，所以文件应该创建双份
			 * 注意：excel文件名称目前写死，必须有，不判断。
			 */
			//TODO 文件路径
			String basePath=map.get("basePath")==null?"":map.get("basePath");
			if("".equals(basePath)){
				File file = new File("feedbackFile");
				basePath=file.getCanonicalPath().replace("\\", "/")+"/";
			}
			String marketIndicator = map.get("marketIndicator")==null?MarketIndicator.INTER_BANK_OFFERING:map.get("marketIndicator");//错误信息默认保存到信用拆借文件中
			
			if("".equals(marketIndicator)&& "6".equals(map.get("marketCode"))) {
			  //利率互换
                String excelPath=basePath+"CSTPBBIRS.xlsx";//分开写，提高速度
                irsLock.lock();
                try {
                    addExcelError(excelPath,map);
                } catch (Exception e) {
                    e.printStackTrace();
                }finally{
                    irsLock.unlock();
                }
                
			
			}else {
                
           
			if(marketIndicator.equals(MarketIndicator.INTEREST_RATE_SWAP)){
				//利率互换
				String excelPath=basePath+"CSTPBBIRS.xlsx";//分开写，提高速度
				irsLock.lock();
				try {
					addExcelError(excelPath,map);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					irsLock.unlock();
				}
				
			}else if(marketIndicator.equals(MarketIndicator.OUTRIGHT_REPO)){
				//买断式回购
				String excelPath=basePath+"CSTPBBOR.xlsx";//分开写，提高速度
				orLock.lock();
				try {
					addExcelError(excelPath,map);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					orLock.unlock();
				}
			}else if(marketIndicator.equals(MarketIndicator.CASH_BOND)){
				//现券买卖
				String excelPath=basePath+"CSTPBBCBT.xlsx";//分开写，提高速度
				cbtLock.lock();
				try {
					addExcelError(excelPath,map);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					cbtLock.unlock();
				}
				
			} else if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {
				//信用拆借
				String excelPath=basePath+"CSTPBBIBO.xlsx";//分开写，提高速度
				iboLock.lock();
				try {
					addExcelError(excelPath,map);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					iboLock.unlock();
				}
			}else if (marketIndicator.equals(MarketIndicator.SECURITY_LENDING)) {
				//债券借贷
				String excelPath=basePath+"CSTPBBSL.xlsx";//分开写，提高速度
				slLock.lock();
				try {
					addExcelError(excelPath,map);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					slLock.unlock();
				}
			}else if (marketIndicator.equals(MarketIndicator.COLLATERAL_REPO)) {
				//质押回购
				String excelPath=basePath+"CSTPBBCR.xlsx";//分开写，提高速度
				crLock.lock();
				try {
					addExcelError(excelPath,map);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					crLock.unlock();
				}
			}else{
				//信用拆借
				String excelPath=basePath+"CSTPBBIBO.xlsx";//分开写，提高速度
				iboLock.lock();
				try {
					addExcelError(excelPath,map);
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
					iboLock.unlock();
				}
			}
			}
		}
		public static void addExcelError(String excelPath,Map<String,String> map)throws IOException{
			Workbook wb = null;
			FileOutputStream out=null;
			try {
				FileInputStream fs = new FileInputStream(excelPath);//获取excel
				wb = getWorkbook(fs);//获取excel
				Sheet sheet = wb.getSheetAt(0);//获取工作表
				int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
				out = new FileOutputStream(excelPath);//向excel中添加数据
				Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
				//设置单元格的数据
			    row.createCell(0).setCellValue(map.get("execId")==null?"":map.get("execId"));//成交编号17
			    row.createCell(1).setCellValue(map.get("msg")==null?"":map.get("msg"));//错误内容
			    
				out.flush();
				wb.write(out);
//				wb.close();
			
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				if(out!=null){
					out.close();
				}
				if(wb!=null){
					wb.close();
				}
			}
		}
		/**
		 * 利率互换转换
		 * @param marketIndicator
		 * @param excelPath
		 * @param tradeBean
		 * @throws IOException
		 */
		public static void addExcelIRS(String marketIndicator,String excelPath,SwapIrsBean tradeBean)throws IOException{
			Workbook wb = null;
			FileOutputStream out=null;
			try {
				FileInputStream fs = new FileInputStream(excelPath);//获取excel
				wb = getWorkbook(fs);//获取excel
				Sheet sheet = wb.getSheetAt(0);//获取工作表
				int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
				out = new FileOutputStream(excelPath);//向excel中添加数据
				Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ss");
				//设置单元格的数据
				row.createCell(0).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
				row.createCell(1).setCellValue(tradeBean.getTradeTime()==null?"":sdf1.format(tradeBean.getTradeTime()));//交易时间10318
				row.createCell(2).setCellValue(tradeBean.getDealMode()==null?"":tradeBean.getDealMode().toString());//交易方式10317
				row.createCell(3).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
				IrsInfoBean iib = tradeBean.getIrsInfo();
				if(iib!=null){
					row.createCell(12).setCellValue(iib.getProductName()==null?"":iib.getProductName());//产品名称48
					row.createCell(14).setCellValue(iib.getNotCcyAmt()==null?"":iib.getNotCcyAmt().divide(new BigDecimal(10000)).toString());//名义本金金额(万元)32
					row.createCell(15).setCellValue(iib.getBasisRest()==null?"":iib.getBasisRest());//计息天数调整10018
					row.createCell(16).setCellValue(iib.getStartDate()==null?"":sdf.format(iib.getStartDate()));//起息日916
					row.createCell(17).setCellValue(iib.getFirstValueDate()==null?"":sdf.format(iib.getFirstValueDate()));//首期起息日10054
					row.createCell(18).setCellValue(iib.getEndDate()==null?"":sdf.format(iib.getEndDate()));//到期日917
					row.createCell(19).setCellValue(iib.getPayDateReset()==null?"":iib.getPayDateReset());//支付日调整10009
					row.createCell(21).setCellValue(iib.getCalculateAgency()==null?"":iib.getCalculateAgency());//计算机构10346
					row.createCell(22).setCellValue(iib.getSettType()==null?"":iib.getSettType());//清算方式11143
				}
				row.createCell(13).setCellValue(tradeBean.getTerm()==null?"":tradeBean.getTerm());//回购期限10316
				row.createCell(51).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
				//需要判断本方方向
				String side=tradeBean.getSide();//交易方向
				SettBaseBean mySett=tradeBean.getSettlement();//本方清算信息
				if("K".equals(side)){
					//浮动对浮动交易
					row.createCell(11).setCellValue("浮动对浮动");//交易方向
				}else{
					row.createCell(11).setCellValue(side);//交易方向
					//固定对浮动交易或交易方向错误
					MarkerTakerEnum mytaker= mySett.getMarkerTakerRole();//本方
					if(MarkerTakerEnum.Taker.equals(mytaker)){
						//119固定利率支付方（本方）
						row.createCell(7).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
					    row.createCell(8).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
					    row.createCell(9).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
					    row.createCell(10).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
					    returnRow(row,23,tradeBean.getIrsDetail());//本方
					    returnRow(row,27,tradeBean.getContraPatryIrsDetail());//对方
					    
					    returnRow(row,52,tradeBean.getSettlement());//本方
					    returnRow(row,57,tradeBean.getCounterPartySettlement());//对方
					}else{
						//120浮动利率支付方（本方）
						row.createCell(9).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
					    row.createCell(10).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
					    row.createCell(7).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
					    row.createCell(8).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
					    returnRow(row,27,tradeBean.getIrsDetail());//本方
					    returnRow(row,23,tradeBean.getContraPatryIrsDetail());//对方
					    
					    returnRow(row,57,tradeBean.getSettlement());//本方
					    returnRow(row,52,tradeBean.getCounterPartySettlement());//对方
					}
				}
				out.flush();
				wb.write(out);
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				if(out!=null){
					out.close();
				}
				if(wb!=null){
					wb.close();
				}
			}
		}
		
		/**
		 * 买断式回购转换
		 * @param marketIndicator
		 * @param excelPath
		 * @param tradeBean
		 * @throws IOException
		 */
		public static void addExcelOR(String marketIndicator,String excelPath,RepoOrBean tradeBean)throws IOException{
			Workbook wb = null;
			FileOutputStream out=null;
			try {
				FileInputStream fs = new FileInputStream(excelPath);//获取excel
				wb = getWorkbook(fs);//获取excel
				Sheet sheet = wb.getSheetAt(0);//获取工作表
				int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
				out = new FileOutputStream(excelPath);//向excel中添加数据
				Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ss");
				//设置单元格的数据
				row.createCell(0).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
				row.createCell(1).setCellValue(tradeBean.getTradeTime()==null?"":sdf1.format(tradeBean.getTradeTime()));//交易时间10318
				row.createCell(2).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
				row.createCell(7).setCellValue(tradeBean.getPs()==null?"":tradeBean.getPs().toString());//交易方向54
				SecurityInfoBean sib = tradeBean.getSecurityInfo();
				if(sib!=null){
					row.createCell(8).setCellValue(sib.getSecurityId()==null?"":sib.getSecurityId());//债券代码309
					row.createCell(9).setCellValue(sib.getSecurityName()==null?"":sib.getSecurityName());//债券名称311
					row.createCell(10).setCellValue(sib.getHaircut()==null?"":sib.getHaircut().toString());//回购利率44
					row.createCell(16).setCellValue(sib.getFaceAmt()==null?"":sib.getFaceAmt().toString());//券面总额32
				}
				SecurityAmtDetailBean first=tradeBean.getComSecurityAmt();//首次
				SecurityAmtDetailBean second=tradeBean.getMatSecurityAmt();//到期
				if(first!=null){
					row.createCell(12).setCellValue(first.getPrice()==null?"":first.getPrice().toString());//净价810
					row.createCell(14).setCellValue(first.getYeld()==null?"":first.getYeld().toString());//收益率889
					row.createCell(17).setCellValue(first.getSettlCcy()==null?"":first.getSettlCcy());//结算币种120
					row.createCell(19).setCellValue(first.getInterestAmt()==null?"":first.getInterestAmt().toString());//应计利息10321
					row.createCell(20).setCellValue(first.getDirtyPrice()==null?"":first.getDirtyPrice().toString());//全价882
					row.createCell(23).setCellValue(first.getSettDate()==null?"":sdf.format(first.getSettDate()));//结算日64
					row.createCell(26).setCellValue(first.getSettAmt()==null?"":first.getSettAmt().toString());//结算金额119
					row.createCell(29).setCellValue(first.getSettType()==null?"":first.getSettType());//结算方式919
				}
				if(second!=null){
					row.createCell(13).setCellValue(second.getPrice()==null?"":second.getPrice().toString());//净价10328
					row.createCell(15).setCellValue(second.getYeld()==null?"":second.getYeld().toString());//收益率889
					row.createCell(21).setCellValue(second.getInterestAmt()==null?"":second.getInterestAmt().toString());//应计利息10322
					row.createCell(22).setCellValue(second.getDirtyPrice()==null?"":second.getDirtyPrice().toString());//全价10325
					row.createCell(24).setCellValue(second.getSettDate()==null?"":sdf.format(second.getSettDate()));//结算日193
					row.createCell(27).setCellValue(second.getSettAmt()==null?"":second.getSettAmt().toString());//结算金额10289
					row.createCell(30).setCellValue(second.getSettType()==null?"":second.getSettType());//结算方式10045
				}
				row.createCell(18).setCellValue(tradeBean.getTerm()==null?"":tradeBean.getTerm());//回购期限10316
				row.createCell(25).setCellValue(tradeBean.getOccupancyDays()==null?"":tradeBean.getOccupancyDays());//实际占款天数10014
				row.createCell(28).setCellValue(tradeBean.getKindId()==null?"":tradeBean.getKindId());//交易品种48
				row.createCell(32).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
				//清算信息
			    String ps = tradeBean.getPs()==null?"":tradeBean.getPs().toString();//本方交易方向
			    if("S".equals(ps)){
			    	//本方正回购
			    	row.createCell(3).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(4).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(5).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(6).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,38,tradeBean.getSettInfo());
			    	returnRow(row,46,tradeBean.getContraPartySettInfo());
			    }else{
			    	//本方逆回购
			    	row.createCell(5).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(6).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(3).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(4).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,46,tradeBean.getSettInfo());
			    	returnRow(row,38,tradeBean.getContraPartySettInfo());
			    }
				out.flush();
				wb.write(out);
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				if(out!=null){
					out.close();
				}
				if(wb!=null){
					wb.close();
				}
			}
		}
		/**
		 * 现券买卖转换
		 * @param marketIndicator
		 * @param excelPath
		 * @param tradeBean
		 * @throws IOException
		 */
		public static void addExcelCBT(String marketIndicator,String excelPath,FixedIncomeBean tradeBean)throws IOException{
			Workbook wb = null;
			FileOutputStream out=null;
			try {
				FileInputStream fs = new FileInputStream(excelPath);//获取excel
				wb = getWorkbook(fs);//获取excel
				Sheet sheet = wb.getSheetAt(0);//获取工作表
				int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
				out = new FileOutputStream(excelPath);//向excel中添加数据
				Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ss");
				//设置单元格的数据
				row.createCell(0).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
				row.createCell(1).setCellValue(tradeBean.getTradeTime()==null?"":sdf1.format(tradeBean.getTradeTime()));//交易时间10318
			    row.createCell(2).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
			    row.createCell(7).setCellValue(tradeBean.getPs()==null?"":tradeBean.getPs().toString());//交易方向54
			    SecurityInfoBean securityInfo = tradeBean.getSecurityInfo();//债券信息
			    if(securityInfo!=null){
			    	row.createCell(8).setCellValue(securityInfo.getSecurityId()==null?"":securityInfo.getSecurityId());//债券代码48
			    	row.createCell(9).setCellValue(securityInfo.getSecurityName()==null?"":securityInfo.getSecurityName());//债券名称55
			    	row.createCell(13).setCellValue(securityInfo.getFaceAmt()==null?"":securityInfo.getFaceAmt().toString());//券面总额32
			    }
			    SecurityAmtDetailBean amtInfo =tradeBean.getSecurityAmt();//债券金额组件
			    if(amtInfo!=null){
			    	row.createCell(11).setCellValue(amtInfo.getPrice()==null?"":amtInfo.getPrice().toString());//净价44
			    	row.createCell(12).setCellValue(amtInfo.getYeld()==null?"":amtInfo.getYeld().toString());//收益率234
			    	row.createCell(16).setCellValue(amtInfo.getSettlCcy()==null?"":amtInfo.getSettlCcy());//结算币种120
			    	row.createCell(17).setCellValue(amtInfo.getAmt()==null?"":amtInfo.getAmt().toString());//交易金额10312
			    	row.createCell(18).setCellValue(amtInfo.getSettDate()==null?"":sdf.format(amtInfo.getSettDate()));//结算日64
			    	row.createCell(19).setCellValue(amtInfo.getInterestAmt()==null?"":amtInfo.getInterestAmt().toString());//应计利息159
			    	row.createCell(20).setCellValue(amtInfo.getDirtyPrice()==null?"":amtInfo.getDirtyPrice().toString());//全价10048
			    	row.createCell(21).setCellValue(amtInfo.getInterestTotalAmt()==null?"":amtInfo.getInterestTotalAmt().toString());//应计利息总额10002
			    	row.createCell(22).setCellValue(amtInfo.getSettAmt()==null?"":amtInfo.getSettAmt().toString());//结算金额119
			    	row.createCell(23).setCellValue(amtInfo.getSettType()==null?"":amtInfo.getSettType());//结算方式919
			    }
			    row.createCell(26).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
			  //清算信息
			    String ps = tradeBean.getPs()==null?"":tradeBean.getPs().toString();//本方交易方向
			    if("P".equals(ps)){
			    	//本方买入
			    	row.createCell(3).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(4).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(5).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(6).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,32,tradeBean.getSettInfo());
			    	returnRow(row,40,tradeBean.getContraPartySettInfo());
			    }else{
			    	//本方卖出
			    	row.createCell(5).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(6).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(3).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(4).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,40,tradeBean.getSettInfo());
			    	returnRow(row,32,tradeBean.getContraPartySettInfo());
			    }
				out.flush();
				wb.write(out);
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				if(out!=null){
					out.close();
				}
				if(wb!=null){
					wb.close();
				}
			}
		}
		/**
		 * 信用拆借转换
		 * @param marketIndicator
		 * @param excelPath
		 * @param tradeBean
		 * @throws IOException
		 */
		public static void addExcelIBO(String marketIndicator,String excelPath,DepositsAndLoansBean tradeBean)throws IOException{
			Workbook wb = null;
			FileOutputStream out=null;
			try {
				FileInputStream fs = new FileInputStream(excelPath);//获取excel
				wb = getWorkbook(fs);//获取excel
				Sheet sheet = wb.getSheetAt(0);//获取工作表
				int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
				out = new FileOutputStream(excelPath);//向excel中添加数据
				Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ss");
				//设置单元格的数据
				row.createCell(0).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
				row.createCell(1).setCellValue(tradeBean.getTradeTime()==null?"":sdf1.format(tradeBean.getTradeTime()));//交易时间10318
			    row.createCell(2).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
			    row.createCell(7).setCellValue(tradeBean.getPs()==null?"":tradeBean.getPs().toString());//交易方向54
			    row.createCell(8).setCellValue(tradeBean.getRate()==null?"":tradeBean.getRate().toString());//拆借利率44
			    row.createCell(9).setCellValue(tradeBean.getAmt()==null?"":tradeBean.getAmt().toString());//拆借金额32
			    row.createCell(11).setCellValue(tradeBean.getTenor()==null?"":tradeBean.getTenor());//拆借期限10316
			    row.createCell(12).setCellValue(tradeBean.getInterestAmt()==null?"":tradeBean.getInterestAmt().toString());//应计利息10002
			    row.createCell(13).setCellValue(tradeBean.getMaturityAmt()==null?"":tradeBean.getMaturityAmt().toString());//到期还款金额10289
			    row.createCell(14).setCellValue(tradeBean.getValueDate()==null?"":sdf.format(tradeBean.getValueDate()));//首次结算日64
			    row.createCell(15).setCellValue(tradeBean.getMaturityDate()==null?"":sdf.format(tradeBean.getMaturityDate()));//到期结算日193
			    row.createCell(16).setCellValue(tradeBean.getHoldingDays()==null?"":tradeBean.getHoldingDays());//实际占款天数10014
			    row.createCell(17).setCellValue(tradeBean.getKindId()==null?"":tradeBean.getKindId());//交易品种48
			    row.createCell(19).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
			    row.createCell(23).setCellValue(tradeBean.getDealMode()==null?"":tradeBean.getDealMode().toString());//交易方式10317
			    row.createCell(25).setCellValue(tradeBean.getDealStatus()==null?"":tradeBean.getDealStatus().toString());//报文动作10105
			    //清算信息
			    String ps = tradeBean.getPs()==null?"":tradeBean.getPs().toString();//本方交易方向
			    if("P".equals(ps)){
			    	//本方拆入
			    	row.createCell(3).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(4).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(5).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(6).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,26,tradeBean.getSettInfo());
			    	returnRow(row,31,tradeBean.getContraPartySettInfo());
			    }else{
			    	//本方拆出
			    	row.createCell(5).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(6).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(3).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(4).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,31,tradeBean.getSettInfo());
			    	returnRow(row,26,tradeBean.getContraPartySettInfo());
			    }
				wb.write(out);
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				if(out!=null){
					out.close();
				}
				if(wb!=null){
					wb.close();
				}
			}
		}
		/**
		 * 债券借贷转换
		 * @param marketIndicator
		 * @param excelPath
		 * @param tradeBean
		 * @throws IOException
		 */
		public static void addExcelSL(String marketIndicator,String excelPath,SecurityLendingBean tradeBean)throws IOException{
			Workbook wb = null;
			FileOutputStream out=null;
			try {
				FileInputStream fs = new FileInputStream(excelPath);//获取excel
				wb = getWorkbook(fs);//获取excel
				Sheet sheet = wb.getSheetAt(0);//获取工作表
				int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
				out = new FileOutputStream(excelPath);//向excel中添加数据
				Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ss");
				//设置单元格的数据
				row.createCell(0).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
				row.createCell(1).setCellValue(tradeBean.getTradeTime()==null?"":sdf1.format(tradeBean.getTradeTime()));//交易时间10318
				row.createCell(2).setCellValue(tradeBean.getDealMode()==null?"":tradeBean.getDealMode().toString());//交易方式10317
				row.createCell(3).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
				row.createCell(9).setCellValue(tradeBean.getPs()==null?"":tradeBean.getPs().toString());//交易方向54
				SecurityInfoBean sib = tradeBean.getSecurityInfo();
				if(sib!=null){
					row.createCell(10).setCellValue(sib.getSecurityId()==null?"":sib.getSecurityId());//标的债券代码309
					row.createCell(11).setCellValue(sib.getSecurityName()==null?"":sib.getSecurityName());//标的债券名称311
					row.createCell(13).setCellValue(sib.getFaceAmt()==null?"":sib.getFaceAmt().toString());//标的债券券面总额(万元)879
				}
				row.createCell(12).setCellValue(tradeBean.getLendingFeeRate()==null?"":tradeBean.getLendingFeeRate().toString());//借贷费率44
				row.createCell(15).setCellValue(tradeBean.getTerm()==null?"":tradeBean.getTerm());//借贷期限10316
				SecuritySettAmtBean first = tradeBean.getComSecurityAmt();//首期
				SecuritySettAmtBean second = tradeBean.getMatSecurityAmt();//到期
				if(first!=null){
					row.createCell(16).setCellValue(first.getSettDate()==null?"":sdf.format(first.getSettDate()));//首次结算日64
					row.createCell(19).setCellValue(first.getSettType()==null?"":first.getSettType());//首次结算方式919
				}
				if(second!=null){
					row.createCell(17).setCellValue(second.getSettDate()==null?"":sdf.format(second.getSettDate()));//到期结算日193
					row.createCell(20).setCellValue(second.getSettType()==null?"":second.getSettType());//到期结算方式10045
				}
				row.createCell(23).setCellValue(tradeBean.getOccupancyDays()==null?"":tradeBean.getOccupancyDays());//实际占款天数10014
				row.createCell(24).setCellValue(tradeBean.getDisputeWay()==null?"":tradeBean.getDisputeWay());//争议解决方式10051
				row.createCell(25).setCellValue(tradeBean.getLengingFeeCost()==null?"":tradeBean.getLengingFeeCost().toString());//借贷费用(元)137
				row.createCell(26).setCellValue(tradeBean.getProductName()==null?"":tradeBean.getProductName());//交易品种48
				row.createCell(30).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
				row.createCell(32).setCellValue(String.valueOf(tradeBean.getMarginReplacement()));//质押债券置换安排10157
				List<SecurityInfoBean> list = tradeBean.getLendingInfos();//质押券信息
				String SecurityId="";//质押券代码
				String SecurityName="";//质押券名称
				String FaceAmt="";//质押券金额
				for(int i=0;i<list.size();i++){
					String str = "/";
					if(i==0){
						str="";
					}
					SecurityId=SecurityId+str+list.get(i).getSecurityId();
					SecurityName=SecurityName+str+list.get(i).getSecurityName();
					FaceAmt=FaceAmt+str+list.get(i).getFaceAmt().divide(new BigDecimal(10000)).toString();
				}
				row.createCell(33).setCellValue(SecurityId);//质押债券代码10159
				row.createCell(34).setCellValue(SecurityName);//质押债券名称10173
				row.createCell(35).setCellValue(FaceAmt);//质押债券券面总额(万元)10150
				row.createCell(39).setCellValue(tradeBean.getMaginTotalAmt()==null?"":tradeBean.getMaginTotalAmt().divide(new BigDecimal(10000)).toString());//付息信息:质押债券券面总额合计(万元)10161
				//清算信息
			    String ps = tradeBean.getPs()==null?"":tradeBean.getPs().toString();//本方交易方向
			    if("P".equals(ps)){
			    	//本方融入
			    	row.createCell(5).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(6).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(7).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(8).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
				    returnRow(row,43,tradeBean.getSettInfo());
			    	returnRow(row,51,tradeBean.getContraPartySettInfo());
			    }else{
			    	//本方融出
			    	row.createCell(7).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(8).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(5).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(6).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
				    returnRow(row,51,tradeBean.getSettInfo());
			    	returnRow(row,43,tradeBean.getContraPartySettInfo());
			    }
				out.flush();
				wb.write(out);
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				if(out!=null){
					out.close();
				}
				if(wb!=null){
					wb.close();
				}
			}
		}
		/**
		 * 质押式回购转换
		 * @param marketIndicator
		 * @param excelPath
		 * @param tradeBean
		 * @throws IOException
		 */
		public static void addExcelCR(String marketIndicator,String excelPath,RepoCrBean tradeBean)throws IOException{
			Workbook wb = null;
			FileOutputStream out=null;
			try {
				FileInputStream fs = new FileInputStream(excelPath);//获取excel
				wb = getWorkbook(fs);//获取excel
				Sheet sheet = wb.getSheetAt(0);//获取工作表
				int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
				out = new FileOutputStream(excelPath);//向excel中添加数据
				Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm:ss");
				//设置单元格的数据
				row.createCell(0).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
				row.createCell(1).setCellValue(tradeBean.getTradeTime()==null?"":sdf1.format(tradeBean.getTradeTime()));//交易时间10318
				row.createCell(2).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
				row.createCell(7).setCellValue(tradeBean.getPs()==null?"":tradeBean.getPs().toString());//交易方向54
				row.createCell(11).setCellValue(tradeBean.getRate()==null?"":tradeBean.getRate().toString());//回购利率44
				List<SecurityInfoBean> list = tradeBean.getSecursInfo();//质押券信息
				String SecurityId="";//质押券代码
				String SecurityName="";//质押券名称
				String FaceAmt="";//质押券金额
				String Haircut="";//折算比例
				for(int i=0;i<list.size();i++){
					String str = "/";
					if(i==0){
						str="";
					}
					SecurityId=SecurityId+str+list.get(i).getSecurityId();
					SecurityName=SecurityName+str+list.get(i).getSecurityName();
					FaceAmt=FaceAmt+str+list.get(i).getFaceAmt().divide(new BigDecimal(10000)).toString();
					Haircut=Haircut+str+list.get(i).getHaircut().toString();
				}
				row.createCell(13).setCellValue(SecurityId);//债券代码309
				row.createCell(14).setCellValue(SecurityName);//债券名称311
				row.createCell(15).setCellValue(FaceAmt);//券面总额(万元)879
				row.createCell(16).setCellValue(Haircut);//折算比例889
				row.createCell(17).setCellValue(tradeBean.getTotalFaceAmt()==null?"":tradeBean.getTotalFaceAmt().divide(new BigDecimal(10000)).toString());//券面总额合计32
				SecurityAmtDetailBean sab = tradeBean.getAmtDeail();
				if(sab!=null){
					row.createCell(18).setCellValue(sab.getSettlCcy()==null?"":sab.getSettlCcy());//结算币种120
					row.createCell(19).setCellValue(sab.getAmt()==null?"":sab.getAmt().toString());//交易金额(元)10312
					row.createCell(21).setCellValue(sab.getInterestTotalAmt()==null?"":sab.getInterestTotalAmt().toString());//应计利息(元)10002
				}
				row.createCell(20).setCellValue(tradeBean.getTerm()==null?"":tradeBean.getTerm());//回购期限10316
				SecuritySettAmtBean first=tradeBean.getComSecuritySettAmt();//首次
				SecuritySettAmtBean second=tradeBean.getMatSecuritySettAmt();//到期
				if(first!=null){
					row.createCell(22).setCellValue(first.getSettDate()==null?"":sdf.format(first.getSettDate()));//结算日64
					row.createCell(27).setCellValue(first.getSettType()==null?"":first.getSettType());//结算方式919
				}
				if(second!=null){
					row.createCell(23).setCellValue(second.getSettDate()==null?"":sdf.format(second.getSettDate()));//结算日193
					row.createCell(25).setCellValue(second.getSettAmt()==null?"":second.getSettAmt().toString());//到期结算金额(元)10289
					row.createCell(28).setCellValue(second.getSettType()==null?"":second.getSettType());//结算方式10045
				}
				row.createCell(24).setCellValue(tradeBean.getOccupancyDays()==null?"":tradeBean.getOccupancyDays());//实际占款天数10014
				row.createCell(26).setCellValue(tradeBean.getKindId()==null?"":tradeBean.getKindId());//交易品种48
				row.createCell(30).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
				//清算信息
			    String ps = tradeBean.getPs()==null?"":tradeBean.getPs().toString();//本方交易方向
			    if("S".equals(ps)){
			    	//本方正回购
			    	row.createCell(3).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(4).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(5).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(6).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,35,tradeBean.getSettInfo());
			    	returnRow(row,43,tradeBean.getContraPartySettInfo());
			    }else{
			    	//本方逆回购
			    	row.createCell(5).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方机构码
				    row.createCell(6).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
				    row.createCell(3).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方机构码
				    row.createCell(4).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
			    	returnRow(row,43,tradeBean.getSettInfo());
			    	returnRow(row,35,tradeBean.getContraPartySettInfo());
			    }
				out.flush();
				wb.write(out);
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				if(out!=null){
					out.close();
				}
				if(wb!=null){
					wb.close();
				}
			}
		}
		
		/**
		 * 赋值清算信息
		 * @param row
		 * @param index
		 * @param fxSettBean
		 * @param type:Y是债券类，N不是
		 */
		public static void returnRow(Row row,int index,SettBaseBean settBean){
			if(settBean!=null){
			    row.createCell(index).setCellValue(settBean.getAcctName()==null?"":settBean.getAcctName());//资金账户户名 23
			    row.createCell(index+1).setCellValue(settBean.getBankName()==null?"":settBean.getBankName());//资金开户行，110
			    row.createCell(index+2).setCellValue(settBean.getAcctNo()==null?"":settBean.getAcctNo());//资金账号 15
			    row.createCell(index+3).setCellValue(settBean.getBankAcctNo()==null?"":settBean.getBankAcctNo());//资金开户行联行行号112
			}
		}
		public static void returnRow(Row row,int index,SecurSettBean settBean){
			if(settBean!=null){
			    row.createCell(index).setCellValue(settBean.getAcctName()==null?"":settBean.getAcctName());//资金账户户名 23
			    row.createCell(index+1).setCellValue(settBean.getBankName()==null?"":settBean.getBankName());//资金开户行，110
			    row.createCell(index+2).setCellValue(settBean.getAcctNo()==null?"":settBean.getAcctNo());//资金账号 15
			    row.createCell(index+3).setCellValue(settBean.getBankAcctNo()==null?"":settBean.getBankAcctNo());//资金开户行联行行号112
				row.createCell(index+4).setCellValue(settBean.getTrustAcctName()==null?"":settBean.getTrustAcctName());//托管账户户名22
				row.createCell(index+5).setCellValue(settBean.getTrustInstitutionId()==null?"":settBean.getTrustInstitutionId());//托管机构111
				row.createCell(index+6).setCellValue(settBean.getTrustAcctNo()==null?"":settBean.getTrustAcctNo());//托管账户10
			}
		}
		/**
		 * 利率互换明细
		 * @param row
		 * @param index
		 * @param irsDetailBean
		 */
		public static void returnRow(Row row,int index,IrsDetailBean irsDetailBean){
			if(irsDetailBean!=null){
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				int legPriceType = irsDetailBean.getLegPriceType();
				if(legPriceType==3){
					//固定端
					row.createCell(index).setCellValue(irsDetailBean.getIntRate()==null?"":irsDetailBean.getIntRate().toString());//固定利率566
					row.createCell(index+1).setCellValue(irsDetailBean.getPaycycle()==null?"":irsDetailBean.getPaycycle());//付息频率10096
					row.createCell(index+2).setCellValue(irsDetailBean.getFirstpaydate()==null?"":sdf.format(irsDetailBean.getFirstpaydate()));//首期支付日248
					row.createCell(index+3).setCellValue(irsDetailBean.getBasis()==null?"":irsDetailBean.getBasis());//计息基准10097
				}else{
					//浮动端
					row.createCell(index).setCellValue(irsDetailBean.getLegCurveName()==null?"":irsDetailBean.getLegCurveName());//参考利率677
					row.createCell(index+1).setCellValue(irsDetailBean.getRateDiff());//利差10090
					row.createCell(index+2).setCellValue(irsDetailBean.getFirstpaydate()==null?"":sdf.format(irsDetailBean.getFirstpaydate()));//首期支付日248
					row.createCell(index+3).setCellValue(irsDetailBean.getPaycycle()==null?"":irsDetailBean.getPaycycle());//付息频率10096
					row.createCell(index+4).setCellValue(irsDetailBean.getFirstconfirmdate()==null?"":sdf.format(irsDetailBean.getFirstconfirmdate()));//首次利率确定日956
					row.createCell(index+5).setCellValue(irsDetailBean.getRaterevcycle()==null?"":irsDetailBean.getRaterevcycle());//重置频率10108
					row.createCell(index+6).setCellValue(irsDetailBean.getRateType()==null?"":irsDetailBean.getRateType());//计息方式10106
					row.createCell(index+7).setCellValue(irsDetailBean.getBasis()==null?"":irsDetailBean.getBasis());//计息基准10097
				}
			}
		}
		
		public static Workbook getWorkbook(InputStream is) throws IOException{
			Workbook wb = null;
			 if(!is.markSupported()){
				 is = new PushbackInputStream(is,8);
			 }
			 if (POIFSFileSystem.hasPOIFSHeader(is)) {//Excel2003及以下版本  
				 wb = (Workbook) new HSSFWorkbook(is);
			 }else if (DocumentFactoryHelper.hasOOXMLHeader(is)) {//Excel2007及以上版本  
				 wb = new XSSFWorkbook(is);
			 }else{
				 throw new IllegalArgumentException("你的Excel版本目前poi无法解析！");
			 }
			return wb;
		}
}
