package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.SLFBean;
import com.singlee.financial.pojo.trade.SLFSettAmtBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;
import imix.imix10.component.UndInstrmtGrp;
import imix.imix10.component.UnderlyingInstrument;
import imix.imix10.component.UnderlyingStipulations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 常备借贷便利
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbSlfEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, SLFBean> {

    //常备借贷便利      ExecutionReport
    public void convert(ExecutionReport message, SLFBean mmBean, String bankId) throws FieldNotFound, FieldConvertError {
        ExecutionReport.NoPartyIDs noPartyIDs = new ExecutionReport.NoPartyIDs();
        int x = message.getNoPartyIDs().getValue();
        message.getGroup(x, noPartyIDs);
        //组装交易信息和清算信息
        convert(noPartyIDs, mmBean, message);
        setBaseInfo(message, bankId, mmBean, mmBean.getSettInfo(), mmBean.getContraPartySettInfo());
        mmBean.setMarketCode(message.isSetMarketCode() ? String.valueOf(message.getMarketCode().getValue()) : "-1");
        mmBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));//交易模式
        mmBean.setExecId(message.isSetExecID() ? message.getExecID().getValue() : CfetsUtils.EMPTY); // CFETS成交编号:17域
        mmBean.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);// 交易日期：75
        mmBean.setTradeTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : CfetsUtils.EMPTY);// 交易成交时间：10318

        mmBean.setPrice(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null); // 利率
        mmBean.setLastQty(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null); // 券面总额
        mmBean.setTradeCashAmt(message.isSetTradeCashAmt() ? new BigDecimal(message.getTradeCashAmt().getValue()) : null); // 申请金额
        mmBean.setAccruedInterestAmt(message.isSetAccruedInterestAmt() ? new BigDecimal(message.getAccruedInterestAmt().getValue()) : null); // 利息
        //mmBean.getAmtDeail().setSettlCcy(message.isSetSettlCurrency() ? message.getSettlCurrency().getValue() : CfetsUtils.EMPTY); // 结算币种

        mmBean.setSettlDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null); // 首次结算日
        mmBean.setSettlDate2(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);// 到期结算日
        // mmBean.setTradeLimitDays(message.isSetTradeLimitDays()?message.getTradeLimitDays().getValue():CfetsUtils.EMPTY); // 期限10316,单位：天，整数
        mmBean.setSettlAmt2(message.isSetSettlCurrAmt2() ? new BigDecimal(message.getSettlCurrAmt2().getValue()) : null); // 到期结算金额
        mmBean.setTransactTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : CfetsUtils.EMPTY);// 交易成交时间
        mmBean.setCollateralType(message.isSetCollateralType() ? message.getCollateralType().getValue() : null);// 抵押品种类
        mmBean.setDeliveryType(message.isSetDeliveryType() ? String.valueOf(message.getDeliveryType().getValue()) : null);// 结算方式
        mmBean.setProcessingStatusString(message.isSetProcessingStatusString() ? String.valueOf(message.getProcessingStatusString().getValue()) : null);// 流程状态
        mmBean.setDataCategoryIndicator(message.isSetDataCategoryIndicator() ? String.valueOf(message.getDataCategoryIndicator().getValue()) : null);// 数据类型 0-本方数据
        mmBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态
        mmBean.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码

        mmBean.setOccupancyDays(String.valueOf(message.isSetTradeLimitDays() ? message.getTradeLimitDays().getValue() : CfetsUtils.EMPTY));//实际占款天数

        List<SLFSettAmtBean> sLFSettAmtBeanList = new ArrayList<SLFSettAmtBean>();
        //质押券信息
        UndInstrmtGrp undInstrmtGrp = message.getUndInstrmtGrp();
        int NoUnderlyings = undInstrmtGrp.isSetNoUnderlyings() ? undInstrmtGrp.getNoUnderlyings().getValue() : 0;
        for (int i = 1; i <= NoUnderlyings; i++) {
            UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
            undInstrmtGrp.getGroup(i, noUnderlyings);
            UnderlyingInstrument underlyingInstrument = noUnderlyings.getUnderlyingInstrument();

            SLFSettAmtBean sLFSettAmtBean = new SLFSettAmtBean();
            sLFSettAmtBean.setUnderlyingQty(underlyingInstrument.isSetUnderlyingQty() ? new BigDecimal(underlyingInstrument.getUnderlyingQty().getValue()) : null);//质押券券面金额
            sLFSettAmtBean.setUnderlyingSecurityID(underlyingInstrument.isSetUnderlyingSecurityID() ? underlyingInstrument.getUnderlyingSecurityID().getValue() : CfetsUtils.EMPTY);//质押券代码
            // sLFSettAmtBean.setUnderlyingStipType(underlyingInstrument.isSetUnderlyingStipValue() ? new BigDecimal(underlyingInstrument.getUnderlyingStipValue().getValue()) : null); //折算率

            UnderlyingStipulations underlyingStipulations = noUnderlyings.getUnderlyingStipulations();
            int NoUnderlyingStips = underlyingStipulations.isSetNoUnderlyingStips() ? underlyingStipulations.getNoUnderlyingStips().getValue() : 0;
            for (int j = 1; j <= NoUnderlyingStips; j++) {
                UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips noUnderlyingStips = new UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips();
                underlyingStipulations.getGroup(j, noUnderlyingStips);
                if ("Haircut".equals(noUnderlyingStips.getUnderlyingStipType().getValue())) {
                    sLFSettAmtBean.setUnderlyingStipType(noUnderlyingStips.isSetUnderlyingStipValue() ? new BigDecimal(noUnderlyingStips.getUnderlyingStipValue().getValue()) : null); // 折算比例
                }
            }
            sLFSettAmtBeanList.add(sLFSettAmtBean);
        }
        mmBean.setsLFSettAmtBean(sLFSettAmtBeanList);
    }
}
