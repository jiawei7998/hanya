package com.singlee.financial.cfets.Util;

import com.singlee.financial.pojo.*;
import com.singlee.financial.pojo.component.*;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.financial.pojo.trade.*;
import imix.field.*;
import imix.imix10.CommissionData;
import imix.imix10.ExecutionReport;
import imix.imix10.SecurityDefinition;
import org.apache.tools.ant.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ImixTradeUtil {
	
	private static Logger LogManager = LoggerFactory.getLogger(TrdSocketListener.class);
	
	private static String split = "/";
	
	public static String nullToEmpty(String s){
		return s == null ? "" : s;
	}
	
	public static String nullToEmpty(Object s){
		return s == null ? "" : s.toString();
	}
	
	public static String nullToEmpty(Date s){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		return s == null ? "" : sdf.format(s);
	}
	
	public static String nullToEmpty(BigDecimal s){
		return s == null ? "" : s.toPlainString();
	}
	
	public static String nullToEmpty(BigDecimal s,int div){
		return s == null ? "" : s.divide(new BigDecimal(div)).toPlainString();
	}
	
	public static String transSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
		case P:
			side = "1-买入";
			break;
		
		case S:
			side = "4-卖出";
			break;

		default:
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		} 
		return side;
	}
	
	public static String transOptSptSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
		case P:
			side = "1-买";
			break;
		
		case S:
			side = "4-卖";
			break;

		default:
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		} 
		return side;
	}

	public static String transMMSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
		case P:
			side = "1-拆入";
			break;
		
		case S:
			side = "4-拆出";
			break;

		default:
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		} 
		return side;
	}

	/**
	 * 远期方向
	 * @param ps
	 * @return
	 */
	public static String transBfwSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("ps传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
			case P:
				side = "1-买入";
				break;
			case S:
				side = "4-卖出";
				break;
			default:
				throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}

	public static String transCkSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("ps传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
			case P:
				side = "1-存入";
				break;
			case S:
				side = "4-存出";
				break;
			default:
				throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}

	public static String transJkSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("ps传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
			case P:
				side = "1-借入";
				break;
			case S:
				side = "4-借出";
				break;
			default:
				throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}

	/**
	 *
	 * @param ps
	 * @return
	 */
	public static String transSwapSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("ps传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
			case P:
				side = "P-固定利率支付方";
				break;
			case S:
				side = "S-浮动利率支付方";
				break;
			default:
				throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}

	public static String transSlSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("ps传有误,不在正常范围[1,4]中");
		}
		String side = "";
		switch (ps) {
			case P:
				side = "1-融入";
				break;
			case S:
				side = "4-融出";
				break;
			default:
				throw new RuntimeException("Side传有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}

	public static String transPayFrequency(PayFrequency payFrequency){
		if(payFrequency == null)
		{
			throw new RuntimeException("payFrequency传有误");
		}
		String side = "";
		switch (payFrequency) {
			case A:
				side = "A-按年付息";
				break;
			case S:
				side = "S-按半年付息";
				break;
			case Q:
				side = "Q-按季付息";
				break;
			case M:
				side = "M-按月付息";
				break;
			case ONCE:
				side = "ONCE-到期一次";
				break;
			default:
				throw new RuntimeException("PayFrequency传有误,不在正常范围[A-按年付息,S-按半年付息,Q-按季付息,M-按月付息,ONCE-到期一次]中:"+payFrequency);
		}
		return side;
	}
	
	/***
	 * 到期结算方式
		0-DVP,4-PUD,5-DUP,6-BVB,7-NONE,8-BVBF,9-BVP,11-NDVP
	 * @param type
	 * @return
	 */
	public static String transSettType(String type){
		String settType = "";
		if("0".equals(type)){
			settType = "0-DVP 券款对付";
		} else if("4".equals(type)){
			settType = "4-PUD 见券付款";
		} else if("5".equals(type)){
			settType = "5-DUP 见款付券";
		} else if("6".equals(type)){
			settType = "6-券券对付";
		} else if("7".equals(type)){
			settType = "NONE";	
		} else if("8".equals(type)){
			settType = "8-返券付费解券";
		} else if("9".equals(type)){
			settType = "9-券费对付";
		} else if("11".equals(type)){
			settType = "11-NDVP-净额券款对付";
		}else{
			settType = type;
		}
//		else {
//			throw new RuntimeException("SettType取值有误,不在正常范围[0-DVP,4-PUD,5-DUP,6-BVB,7-NONE,8-BVBF,9-BVP,11-NDVP]中," + type);
//		}
		return settType;
	}

	/**
	 * 现券买卖专用
	 * @param type
	 * @return
	 */
	public static String transSettTypeSec(String type){
		String settType = "";
		if("0".equals(type)){
			settType = "0-DVP-券款对付";
		} else if("4".equals(type)){
			settType = "4-PUD-见券付款";
		} else if("5".equals(type)){
			settType = "5-DUP-见款付券";
		} else if("6".equals(type)){
			settType = "6-券券对付";
		} else if("7".equals(type)){
			settType = "NONE";
		} else if("8".equals(type)){
			settType = "8-返券付费解券";
		} else if("9".equals(type)){
			settType = "9-券费对付";
		} else if("11".equals(type)){
			settType = "11-NDVP-净额券款对付";
		}else{
			settType = type;
		}
//		else {
//			throw new RuntimeException("SettType取值有误,不在正常范围[0-DVP,4-PUD,5-DUP,6-BVB,7-NONE,8-BVBF,9-BVP,11-NDVP]中," + type);
//		}
		return settType;
	}

	/**
	 * 解决争议方式
	 * @param type
	 * @return
	 */
	public static String transDisputeWay(String type){
		String settType = "";
		if("0".equals(type)){
			settType = "0-仲裁";
		} else if("1".equals(type)){
			settType = "1-诉讼";
		} else {
			throw new RuntimeException("DisputeWay取值有误,不在正常范围[0-仲裁,1-诉讼]中," + type);
		}
		return settType;
	}

	public static String transBasisRest(String BasisRest){
		String settType = "";
		if("0".equals(BasisRest)){
			settType = "0-不调整";
		} else if("1".equals(BasisRest)){
			settType = "1-实际天数调整";
		}
		return settType;
	}

	public static String transSid(String sid){
		String settType = "";
		if("J".equals(sid)){
			settType = "J-固定-浮动";
		} else if("K".equals(sid)){
			settType = "K-浮动-浮动";
		}
		return settType;
	}



	public static String transRepoMethod(RepoMethodEnum repoMethodEnum){
		String settType = "";
		if(repoMethodEnum.equals(RepoMethodEnum.Enquiry)){
			settType = "双边回购";
		} else if(repoMethodEnum.equals(RepoMethodEnum.Competition)){
			settType = "三方回购";
		} else if(repoMethodEnum.equals(RepoMethodEnum.Married)){
			settType = "通用回购";
		}  else {
			throw new RuntimeException("RepoMethodEnum取值有误,不在正常范围中," + repoMethodEnum);
		}
		return settType;
	}
	
	/***
	 * 到期结算方式
		0-DVP,4-PUD,5-DUP,6-BVB,7-NONE,8-BVBF,9-BVP,11-NDVP
	 * @param type
	 * @return
	 */
	public static String transSettType2(String type){
		String settType = "";
		if("0".equals(type)){
			settType = "0-DVP 券款对付";
		} else if("4".equals(type)){
			settType = "4-PUD 见券付款";
		} else if("5".equals(type)){
			settType = "5-DUP 见款付券";
		} else if("6".equals(type)){
			settType = "6-券券对付";
		} else if("7".equals(type)){
			settType = "NONE";
		} else if("8".equals(type)){
			settType = "8-返券付费解券";
		} else if("9".equals(type)){
			settType = "9-券费对付";
		} else if("11".equals(type)){
			settType = "11-NDVP-净额券款对付";
		}else{
			settType = type;
		}
//		else {
//			throw new RuntimeException("SettType取值有误,不在正常范围[0-DVP,4-PUD,5-DUP,6-BVB,7-NONE,8-BVBF,9-BVP,11-NDVP]中," + type);
//		}
		return settType;
	}

	public static String transSide(String ps){
		if(ps == null)
		{
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中");
		}
		String side = "";
		if("1".equals(ps)){
			side = "1-买";
			
		}else if("4".equals(ps)){
			side = "4-卖";	
		} else{
			throw new RuntimeException("Side取值有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}
	
	public static String transRepoSide(PsEnum ps){
		if(ps == null)
		{
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中");
		}
		String side = "";
		if(PsEnum.P.equals(ps)){
			side = "1-逆回购";
			
		}else if(PsEnum.S.equals(ps)){
			side = "4-正回购";	
		}else{
			throw new RuntimeException("Side取值有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}

	public static String transLegSide(String ps){
		if(ps == null)
		{
			throw new RuntimeException("Side传有误,不在正常范围[1,4]中");
		}
		String side = "";
		if("P".equals(ps)){
			side = "1-卖出货币并收取该货币利息";
		}else if("S".equals(ps)){
			side = "4-买入货币并支付该货币利息";
		}else{
			throw new RuntimeException("Side取值有误,不在正常范围[1,4]中:"+ps);
		}
		return side;
	}
	
	public static String transBasisMM(BasisEnum basisEnum){
		if(basisEnum == null)
		{
			return "";
		}
		String basis = "";
		switch (basisEnum) {
		case A360:
			basis = "1-实际/360";
			break;
		
		case A365:
			basis = "3-实际/365";
			break;
		
		default:
			break;
		}
		return basis;		
	}

	public static InstitutionBean getTakerInst(InstitutionBean inst,InstitutionBean custInst,MarkerTakerEnum markerTakerRole)throws Exception{
		if(MarkerTakerEnum.Taker.equals(markerTakerRole)){
			return inst;
		}else{
			return custInst;
		}
	}
	
	public static InstitutionBean getMarkerInst(InstitutionBean inst,InstitutionBean custInst,MarkerTakerEnum markerTakerRole)throws Exception{
		if(MarkerTakerEnum.Marker.equals(markerTakerRole)){
			return inst;
		}else{
			return custInst;
		}
	}
	
	public static SettBaseBean getTakerSettls(SettBaseBean sett,SettBaseBean custSett)throws Exception{
		if(MarkerTakerEnum.Taker.equals(sett.getMarkerTakerRole())){
			return sett;
		}else{
			return custSett;
		}
	}
	
	public static SettBaseBean getMarkerSettls(SettBaseBean sett,SettBaseBean custSett)throws Exception{
		if(MarkerTakerEnum.Marker.equals(sett.getMarkerTakerRole())){
			return sett;
		}else{
			return custSett;
		}
	}
	
	public static Map<Integer, String> getValidMMMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(ExecID.FIELD, "ExecID");
		map.put(TradeDate.FIELD, "TradeDate");
		map.put(Side.FIELD, "Side");
		map.put(Price.FIELD, "Price");
		map.put(LastQty.FIELD, "LastQty");
		map.put(SettlCurrAmt2.FIELD, "SettlCurrAmt2");
		map.put(SettlDate.FIELD, "SettlDate");
		map.put(SettlDate2.FIELD, "SettlDate2");
		return map;
	}
	
	public static Map<Integer, String> getValidZyMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(ExecID.FIELD, "ExecID");
		map.put(TradeDate.FIELD, "TradeDate");
		map.put(Side.FIELD, "Side");
		map.put(Price.FIELD, "Price");
		map.put(TradeCashAmt.FIELD, "TradeCashAmt");
		map.put(SettlCurrAmt2.FIELD, "SettlCurrAmt2");
		map.put(SettlDate.FIELD, "SettlDate");
		map.put(SettlDate2.FIELD, "SettlDate2");
		map.put(DeliveryType.FIELD, "DeliveryType");
		map.put(DeliveryType2.FIELD, "DeliveryType2");
//		map.put(NoUnderlyings.FIELD, "NoUnderlyings");
		return map;
	}
	
	public static Map<Integer, String> getValidMdMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(ExecID.FIELD, "ExecID");
		map.put(TradeDate.FIELD, "TradeDate");
		map.put(Side.FIELD, "Side");
		map.put(Price.FIELD, "Price");
		map.put(LastQty.FIELD, "LastQty");
		map.put(SettlCurrAmt.FIELD, "SettlCurrAmt");
		map.put(SettlCurrAmt2.FIELD, "SettlCurrAmt2");
		map.put(SettlDate.FIELD, "SettlDate");
		map.put(SettlDate2.FIELD, "SettlDate2");
		map.put(DeliveryType.FIELD, "DeliveryType");
		map.put(DeliveryType2.FIELD, "DeliveryType2");
		map.put(NoUnderlyings.FIELD, "NoUnderlyings");
		return map;
	}
	
	public static Map<Integer, String> getValidBondMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(ExecID.FIELD, "ExecID");
		map.put(TradeDate.FIELD, "TradeDate");
		map.put(Side.FIELD, "Side");
		map.put(Price.FIELD, "Price");
		map.put(SecurityID.FIELD, "SecurityID");
		map.put(LastQty.FIELD, "LastQty");
		map.put(TradeCashAmt.FIELD, "TradeCashAmt");
		map.put(SettlCurrAmt.FIELD, "SettlCurrAmt");
		map.put(SettlDate.FIELD, "SettlDate");
		map.put(DeliveryType.FIELD, "DeliveryType");
		return map;
	}
	
	public static Map<Integer, String> getValidIrsMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(ExecID.FIELD, "ExecID");
		map.put(TradeDate.FIELD, "TradeDate");
		map.put(LastQty.FIELD, "LastQty");
		map.put(StartDate.FIELD, "StartDate");
		map.put(FirstPeriodStartDate.FIELD, "FirstPeriodStartDate");
		map.put(EndDate.FIELD, "EndDate");
		map.put(ClearingMethod.FIELD, "ClearingMethod");
		return map;
	}
	
	public static Map<Integer, String> getValidCDMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(Currency.FIELD, "Currency");
		map.put(SecurityType.FIELD, "SecurityType");
		map.put(SecurityDefinitionType.FIELD, "SecurityDefinitionType");
		map.put(FullSymbol.FIELD, "FullSymbol");
		map.put(SecurityID.FIELD, "SecurityID");
		map.put(CouponRateType.FIELD, "CouponRateType");
		map.put(IssueMethod.FIELD, "IssueMethod");
		map.put(SecurityStatus.FIELD, "SecurityStatus");
		map.put(NoPartyDetailAltIDs.FIELD, "NoPartyDetailAltIDs");
		return map;
	}
	
	public static Map<Integer, String> getValidCDCommMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(Currency.FIELD, "Currency");
		map.put(PaymentStatus.FIELD, "PaymentStatus");
		map.put(SecurityType.FIELD, "SecurityType");
		map.put(FullSymbol.FIELD, "FullSymbol");
		map.put(SecurityID.FIELD, "SecurityID");
		map.put(NoCommissions.FIELD, "NoCommissions");
		map.put(NoPartyIDs.FIELD, "NoPartyIDs");
		return map;
	}
	
	public static void validTrade(ExecutionReport message)throws Exception{
		if(message.isSetField(MarketCode.FIELD)){
			return;
		}
		if(!message.isSetField(MarketIndicator.FIELD)){
			throw new RuntimeException("报文缺少域:" + MarketIndicator.FIELD + "[MarketIndicator]");
		}
		String marketIndicator = message.getMarketIndicator().getValue();
		
		Map<Integer, String> map = new HashMap<Integer, String>();
		if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {// 信用拆借
			map = getValidMMMap();
			
		} else if (marketIndicator.equals(MarketIndicator.CASH_BOND)) {// 现券买卖
			map = getValidBondMap();
			
		} else if (marketIndicator.equals(MarketIndicator.OUTRIGHT_REPO)) {// 买断式
			map = getValidMdMap();
			
		} else if (marketIndicator.equals(MarketIndicator.COLLATERAL_REPO)) {// 质押式回购
			map = getValidZyMap();
			
		} else if (marketIndicator.equals(MarketIndicator.INTEREST_RATE_SWAP)) {// 利率互换
			map = getValidIrsMap();
			
		} else if (marketIndicator.equals(MarketIndicator.IBD)) {// 同业存款
			map = getValidMMMap();
			
		} else if (marketIndicator.equals(MarketIndicator.INTER_BANK_BORROWING)) { // 同业借款
			map = getValidMMMap();
			
		}
		for (Entry<Integer, String> en : map.entrySet()) 
		{
			if(!message.isSetField(en.getKey())){
				throw new RuntimeException("报文缺少域:" + en.getValue() + "[" +en.getKey() + "]");
			}
		}
	}
	
	public static void validTrade(SecurityDefinition securityDefinition) {
		Map<Integer, String> map = getValidCDMap();
		for (Entry<Integer, String> en : map.entrySet()) 
		{
			if(!securityDefinition.isSetField(en.getKey())){
				throw new RuntimeException("报文缺少域:" + en.getValue() + "[" +en.getKey() + "]");
			}
		}
	}

	public static void validTrade(CommissionData cData) {
		Map<Integer, String> map = getValidCDCommMap();
		for (Entry<Integer, String> en : map.entrySet()) 
		{
			if(!cData.isSetField(en.getKey())){
				throw new RuntimeException("报文缺少域:" + en.getValue() + "[" +en.getKey() + "]");
			}
		}
	}

	private static String addStr(String target, String add, String split2) {
		if(add != null && add.trim().length() > 0){
			return target += (add + split2);
		}
		return target;
	}

	private static String subStr(String target){
		if(target != null && target.length() > 0){
			return target.substring(0,target.length() - 1);
		}
		return target;
	}

	/***
	 *  1-价格
	 2-利率
	 3-利差
	 *
	 * @param tenderSubject
	 * @return
	 */
	private static String transTenderSubject(String tenderSubject) {
		String text = "";
		if("1".equals(tenderSubject)){
			text = "1-价格";

		}else if("2".equals(tenderSubject)){
			text = "2-利率";

		}else if("3".equals(tenderSubject)){
			text = "3-利差";

		}else{
			text = tenderSubject;
		}
		return text;
	}

	/**
	 *结算方式
	 * @param deliveryType
	 * @return
	 */
	private static String transDeliveryType(String deliveryType) {
		String text = "";
		if("0".equals(deliveryType)){
			text = "0-DVP";

		}else if("99".equals(deliveryType)){
			text = "99-非DVP";

		}else{
			text = deliveryType;
		}
		return text;
	}

	/**
	 * 远期结算方式
	 * @param deliveryType
	 * @return
	 */
	private static String transFwdDeliveryType(String deliveryType) {
		String text = "";
		if("0".equals(deliveryType)){
			text = "0-券款对付";

		}else if("4".equals(deliveryType)){
			text = "4-见券付款";

		}else if("5".equals(deliveryType)){
			text = "5-见款付券";

		}else{
			text = deliveryType;
		}
		return text;
	}

	/**
	 * 质押品种
	 * @param collateralType
	 * @return
	 */
	private static String transCollateralType(Integer collateralType) {
		String text = "";
		if(collateralType == 5){
			text = "5-国债公司债券";

		}else if(collateralType == 6){
			text = "6-上海清算所债券";

		}else if(collateralType == 7){
			text = "7-信贷资产";

		}else{
			text = collateralType.toString();
		}
		return text;
	}

	/***
	 * 1 发行成功 2 发行失败
	 * @param secStatus
	 * @return
	 */
	private static String transSecStatus(String secStatus) {
		String secStatusNm = "";
		if("1".equals(secStatus)){
			secStatusNm = "发行成功";

		}else if("2".equals(secStatus)){
			secStatusNm = "发行失败";
		}else{
			secStatusNm = secStatus;
		}
		return secStatusNm;
	}

	private static String transMarketCode(String marketCode) {
		String marketCodeNm = "";
		if("6".equals(marketCode)){
			marketCodeNm = "6-常备借贷便利";

		}else{
			throw new RuntimeException("marketCode枚举值错误,不在正常范围[6-常备借贷便利]中:"+marketCode);
		}
		return marketCodeNm;
	}

	/**
	 * 成交方式
	 * @param secStatus
	 * @return
	 */
	private static String transDealMode(DealModeEnum dealModeEnum) {
		String secStatusNm = "";
		if(DealModeEnum.Enquiry.equals(dealModeEnum)){
			secStatusNm = "1-询价";

		} else if(DealModeEnum.Competition.equals(dealModeEnum)){
			secStatusNm = "2-竞价";

		} else if(DealModeEnum.Married.equals(dealModeEnum)){
			secStatusNm = "9-撮合";

		} else{
			throw new RuntimeException("DealMode枚举值错误,不在正常范围[询价，竞价，撮合]中:"+dealModeEnum);
		}
		return secStatusNm;
	}

	/**
	 * 成交状态
	 * @param secStatus
	 * @return
	 */
	private static String transStatus(ExecStatus execStatus) {
		String secStatusNm = "";

		if(ExecStatus.Finish.equals(execStatus)){
			secStatusNm = "F-正常";

		}else if(ExecStatus.Revoke.equals(execStatus)){
			secStatusNm = "4-撤销";

		}else if(ExecStatus.Modify.equals(execStatus)){
			secStatusNm = "5-修改";

		} else{
			throw new RuntimeException("Status枚举值错误,不在正常范围[F-正常,4-撤销，5-修改]中:"+execStatus);
		}
		return secStatusNm;
	}

	private static String transMMStatus(ExecStatus execStatus) {
		String secStatusNm = "";

		if(ExecStatus.Finish.equals(execStatus)){
			secStatusNm = "F-成交";

		}else if(ExecStatus.Revoke.equals(execStatus)){
			secStatusNm = "4-撤销";

		}else if(ExecStatus.Modify.equals(execStatus)){
			secStatusNm = "5-修改";

		}else if(ExecStatus.Emergency.equals(execStatus)){
			secStatusNm = "101-应急";
		}else{
			throw new RuntimeException("Status枚举值错误,不在正常范围[F-正常,4-撤销，5-修改,101-应急]中:"+execStatus);
		}
		return secStatusNm;
	}

	private static String transStatus2(ExecStatus execStatus) {
		String secStatusNm = "";
		if(ExecStatus.Finish.equals(execStatus)){
			secStatusNm = "F-成交";

		}else if(ExecStatus.Modify.equals(execStatus)){
			secStatusNm = "5-更新";

		}else{
			throw new RuntimeException("Status枚举值错误,不在正常范围[F-正常,4-撤销，5-修改]中:"+execStatus);
		}
		return secStatusNm;
	}

	/**
	 * 报文动作
	 * @param transactionStatusEnum
	 * @return
	 */
	private static String transDealTransType(TransactionStatusEnum transactionStatusEnum) {
		String secStatusNm = "";
		if(TransactionStatusEnum.Entry.equals(transactionStatusEnum)){
			secStatusNm = "0-录入";

		}else if(TransactionStatusEnum.Modify.equals(transactionStatusEnum)){
			secStatusNm = "1-修改";

		}else if(TransactionStatusEnum.Back.equals(transactionStatusEnum)){
			secStatusNm = "2-撤销";

		}else{
			throw new RuntimeException("TransactionStatusEnum枚举值错误,不在正常范围[0-录入,1-修改]中:"+transactionStatusEnum);
		}
		return secStatusNm;
	}

	/**
	 * 现券买卖 成交传输动作状态
	 * @param transactionStatusEnum
	 * @return
	 */
	private static String transSecDealTransType(TransactionStatusEnum transactionStatusEnum) {
		String secStatusNm = "";
		if(TransactionStatusEnum.Entry.equals(transactionStatusEnum)){
			secStatusNm = "0-成交录入";

		}else if(TransactionStatusEnum.Modify.equals(transactionStatusEnum)){
			secStatusNm = "1-成交修改";

		}else if(TransactionStatusEnum.Back.equals(transactionStatusEnum)){
			secStatusNm = "2-成交撤销";

		}else{
			throw new RuntimeException("TransactionStatusEnum枚举值错误,不在正常范围[0-录入,1-修改]中:"+transactionStatusEnum);
		}
		return secStatusNm;
	}

	/**
	 * 数据类型标识
	 * @param dataCategoryIndicator
	 * @return
	 */
	private static String transDataCategoryIndicator(DataCategoryIndicatorEnum dcie) {
		String dcieNm = "";
		if(DataCategoryIndicatorEnum.THIS.equals(dcie)){
			dcieNm = "0-本方数据";

		} else if(DataCategoryIndicatorEnum.AI.equals(dcie)){
			dcieNm = "2-关联机构数据";

		} else if(DataCategoryIndicatorEnum.BAI.equals(dcie)){
			dcieNm = "5-债券关联机构数据";

		} else{
			throw new RuntimeException("DataCategoryIndicatorEnum枚举值错误,不在正常范围[0-本方数据,2-关联机构数据,5-债券关联机构数据]中:"+dcie);
		}
		return dcieNm;
	}

	/**
	 *附加协调类型
	 * @param dealModeEnum
	 * @return
	 */
	private static String transSupplType(String supplType) {
		String supplTypeNm = "";
		if("1".equals(supplType)){
			supplTypeNm = "1-提前支取申请";

		} else if("3".equals(supplType)){
			supplTypeNm = "3-补充定期账户";

		} else{
			throw new RuntimeException("SupplType枚举值错误,不在正常范围[1-提前支取申请，3-补充定期账户]中:"+supplType);
		}
		return supplTypeNm;
	}


	private static String transAdvanceCompensationType(String advanceCompensationType) {
		String supplTypeNm = "";
		if("1".equals(advanceCompensationType)){
			supplTypeNm = "1-约定新利率";

		} else if("2".equals(advanceCompensationType)){
			supplTypeNm = "2-约定补偿金额";

		} else{
			supplTypeNm = advanceCompensationType;
		}
		return supplTypeNm;
	}

	private static String transAdvanceAttachmentIndicator(String advanceAttachmentIndicator) {
		String supplTypeNm = "";
		if("0".equals(advanceAttachmentIndicator)){
			supplTypeNm = "0-无";

		} else{
			supplTypeNm = advanceAttachmentIndicator;
		}
		return supplTypeNm;
	}



	/**
	 *
	 * @param advanceStatus
	 * @return
	 */
	private static String transAdvanceStatus(String advanceStatus) {
		String advanceStatusNm = "";
		if("1".equals(advanceStatus)){
			advanceStatusNm = "1-正常";

		}else if("2".equals(advanceStatus)){
			advanceStatusNm = "2-撤销";

		} else{
			advanceStatusNm = advanceStatus;
		}
		return advanceStatusNm;
	}

	/**
	 * 消息类型
	 * @param SecDefType
	 * @return
	 */
	private static String transSecDefType(String SecDefType) {
		String SecDefTypeNm = "";
		if("2".equals(SecDefType)){
			SecDefTypeNm = "2-发行结果信息";

		} else if("3".equals(SecDefType)){
			SecDefTypeNm = "3-投标结果信息";

		} else{
			throw new RuntimeException("SupplType枚举值错误,不在正常范围[2-发行结果信息,3-投标结果信息]中:"+SecDefType);
		}
		return SecDefTypeNm;
	}

	/**
	 * 操作类型
	 * @param supplType
	 * @return
	 */
	private static String transSupplTransType(String supplType) {
		String supplTypeNm = "";
		if("N".equals(supplType)){
			supplTypeNm = "N-询价";

		} else if("R".equals(supplType)){
			supplTypeNm = "R-修改";

		} else if("C".equals(supplType)){
			supplTypeNm = "C-撤销";

		} else if("1".equals(supplType)){
			supplTypeNm = "1-确认";

		} else if("3".equals(supplType)){
			supplTypeNm = "3-应急撤销";

		} else{
			throw new RuntimeException("SupplTransType枚举值错误,不在正常范围[N-询价,R-修改,C-撤销,1-确认,3-应急撤销]中:"+supplType);
		}
		return supplTypeNm;
	}

	private static String transSupplTransTypeIbd(String supplType) {
		String supplTypeNm = "";
		if("N".equals(supplType)){
			supplTypeNm = "N-新增";

		} else if("R".equals(supplType)){
			supplTypeNm = "R-修改";

		} else if("C".equals(supplType)){
			supplTypeNm = "C-撤销";

		} else if("1".equals(supplType)){
			supplTypeNm = "1-确认";

		} else if("3".equals(supplType)){
			supplTypeNm = "3-应急撤销";

		} else{
			throw new RuntimeException("SupplTransType枚举值错误,不在正常范围[N-询价,R-修改,C-撤销,1-确认,3-应急撤销]中:"+supplType);
		}
		return supplTypeNm;
	}


	/***
	 * 0-单一价格招标发行 5-数量招标发行
	 * @param issMethod
	 * @return
	 */
	private static String transIssMethod(String issMethod) {
		String text = "";
		if("0".equals(issMethod)){
			text = "单一价格招标发行";

		}else if("5".equals(issMethod)){
			text = "数量招标发行";

		}else if("6".equals(issMethod)){
			text = "报价发行";

		}else{
			throw new RuntimeException("IssueMethod枚举值错误,不在正常范围[0,5,6]中:"+issMethod);
		}
		return text;
	}

	/***
	 * 1-浮息；2-零息/3-贴现；0-固息

	 * @param rateType
	 * @return
	 */
	private static String transRateType(String rateType) {
		String text = "";
		if("0".equals(rateType)){
			text = "固息";

		}else if("1".equals(rateType)){
			text = "浮息";

		}else if("2".equals(rateType)){
			text = "零息";

		}else if("3".equals(rateType)){
			text = "贴现";
		}else{
			text = rateType;
		}
//		else{
//			throw new RuntimeException("RateType枚举值错误,不在正常范围[0,1,2,3]中:"+rateType);
//		}
		return text;
	}

	/**
	 * 缴款状态
	 * @param paymentStatus
	 * @return
	 */
	private static String transPaymentStatus(int paymentStatus) {
		String text = "";
		if(paymentStatus == 0){
			text = "0-缴款失败";

		}else if(paymentStatus == 1){
			text = "1-缴款成功";

		}else{
			throw new RuntimeException("paymentStatus枚举值错误,不在正常范围[0-缴款失败,1-缴款成功]中:"+paymentStatus);
		}
		return text;
	}

	/***
	 * CD-同业存单
	 CD_FTA-自贸区同业存单
	 6-央行票据
	 0-国债
	 1-政策性金融债
	 5-企业债
	 3-次级债
	 2-短期融资券
	 7-国际开发机构债
	 8-混合资本债
	 10-中期票据
	 11-地方政府债
	 12-财务公司债
	 13-证券公司债
	 23-集合票据
	 27-金融租赁公司金融债
	 9-资产支持证券
	 22-政府支持机构债券
	 19-超短期融资券
	 21-定向工具
	 14-公司短期融资券
	 49-二级资本工具
	 50-保险公司资本补充债
	 45-资产管理公司金融债
	 * @param secType
	 * @return
	 */
	private static String transSecType(String secType) {
		String typeName = "";
		if("CD".equals(secType)){
			typeName = "CD-同业存单";
		}else if("CD_FTA".equals(secType)){
			typeName = "CD_FTA-自贸区同业存单";
		}else if("6".equals(secType)){
			typeName = "央行票据"          ;
		}else if("0".equals(secType)){
			typeName = "国债"              ;
		}else if("1".equals(secType)){
			typeName = "政策性金融债"      ;
		}else if("5".equals(secType)){
			typeName = "企业债"            ;
		}else if("3".equals(secType)){
			typeName = "次级债"            ;
		}else if("2".equals(secType)){
			typeName = "短期融资券"        ;
		}else if("7".equals(secType)){
			typeName = "国际开发机构债"    ;
		}else if("8".equals(secType)){
			typeName = "混合资本债"        ;
		}else if("10".equals(secType)){
			typeName = "中期票据"          ;
		}else if("11".equals(secType)){
			typeName = "地方政府债"        ;
		}else if("12".equals(secType)){
			typeName = "财务公司债"        ;
		}else if("13".equals(secType)){
			typeName = "证券公司债"        ;
		}else if("23".equals(secType)){
			typeName = "集合票据"          ;
		}else if("27".equals(secType)){
			typeName = "金融租赁公司金融债";
		}else if("9".equals(secType)){
			typeName = "资产支持证券"      ;
		}else if("22".equals(secType)){
			typeName = "政府支持机构债券"  ;
		}else if("19".equals(secType)){
			typeName = "超短期融资券"      ;
		}else if("21".equals(secType)){
			typeName = "定向工具"          ;
		}else if("14".equals(secType)){
			typeName = "公司短期融资券"    ;
		}else if("49".equals(secType)){
			typeName = "二级资本工具"      ;
		}else if("50".equals(secType)){
			typeName = "保险公司资本补充债";
		}else if("45".equals(secType)){
			typeName = "资产管理公司金融债";
		}else{
			typeName = secType;
		}
//		else{
//			throw new RuntimeException("工具类型SecurityType枚举值错误,不在正常范围中:"+typeName);
//		}
		return typeName;
	}

	/**
	 *
	 * 0- Preceding Day,1- Following Day,2- Modified Following,3- Modified Proceeding
	 * @param LegCouponPaymentDateReset
	 * @return
	 */
	public static String transLegCouponPaymentDateReset(String LegCouponPaymentDateReset){
		if("0".equals(LegCouponPaymentDateReset)){
			return "0-上一营业日";

		}else if("1".equals(LegCouponPaymentDateReset)){
			return "1-下一营业日";

		}else if("2".equals(LegCouponPaymentDateReset)){
			return "2-经调整的下一营业日";

		}
		return LegCouponPaymentDateReset;
	}

	public static String transCalculateAgency(String CalculateAgency){
		if("0".equals(CalculateAgency)){
			return "0-中国外汇交易中心";

		}else if("1".equals(CalculateAgency)){
			return "1-卖出方";

		}else if("2".equals(CalculateAgency)){
			return "2-买入方";

		}else if("3".equals(CalculateAgency)){
			return "3-交易双方";

		}
		return CalculateAgency;
	}



	/***
	 *
	 * 0-Semi annually,1-Annually,2-Maturity,3-Monthly,4-Quarterly,5-BiWeekly,6-Weekly,7-Daily,7D-LEGSETTLTYPE_VALUE_7D,1W-LEGSETTLTYPE_VALUE_1W,2W-LEGSETTLTYPE_VALUE_2W,3W-LEGSETTLTYPE_VALUE_3W,1M-LEGSETTLTYPE_VALUE_1M,2M-LEGSETTLTYPE_VALUE_2M,3M-LEGSETTLTYPE_VALUE_3M,4M-LEGSETTLTYPE_VALUE_4M,5M-LEGSETTLTYPE_VALUE_5M,6M-LEGSETTLTYPE_VALUE_6M,7M-LEGSETTLTYPE_VALUE_7M,8M-LEGSETTLTYPE_VALUE_8M,9M-LEGSETTLTYPE_VALUE_9M,10M-LEGSETTLTYPE_VALUE_10M,11M-LEGSETTLTYPE_VALUE_11M,1Y-LEGSETTLTYPE_VALUE_1Y,2Y-LEGSETTLTYPE_VALUE_2Y,3Y-LEGSETTLTYPE_VALUE_3Y,4Y-LEGSETTLTYPE_VALUE_4Y,5Y-LEGSETTLTYPE_VALUE_5Y,6Y-LEGSETTLTYPE_VALUE_6Y,7Y-LEGSETTLTYPE_VALUE_7Y,8Y-LEGSETTLTYPE_VALUE_8Y,9Y-LEGSETTLTYPE_VALUE_9Y,10Y-LEGSETTLTYPE_VALUE_10Y,11Y-LEGSETTLTYPE_VALUE_11Y,12Y-LEGSETTLTYPE_VALUE_12Y,13Y-LEGSETTLTYPE_VALUE_13Y,14Y-LEGSETTLTYPE_VALUE_14Y,15Y-LEGSETTLTYPE_VALUE_15Y,20Y-LEGSETTLTYPE_VALUE_20Y,1D-LEGSETTLTYPE_VALUE_1D,10D-LEGSETTLTYPE_VALUE_10D,8-Ten Days,9-Nine Months,10-In Advance

	 * @param PaymentFrequency
	 * @return
	 */
	public static String transPaymentFrequency(String PaymentFrequency){
		if("0".equals(PaymentFrequency)){
			return "0-半年";

		}else if("1".equals(PaymentFrequency)){
			return "1-年";

		}else if("2".equals(PaymentFrequency)){
			return "2-到期";

		}else if("3".equals(PaymentFrequency)){
			return "3-月";

		}else if("4".equals(PaymentFrequency)){
			return "4-季";

		}else if("5".equals(PaymentFrequency)){
			return "5-两周";

		}else if("6".equals(PaymentFrequency)){
			return "6-周";

		}else if("7".equals(PaymentFrequency)){
			return "7-天";

		}
		return PaymentFrequency;
	}

	public static String transLegDayCount(String LegDayCount){
		if("0".equals(LegDayCount)){
			return "0-实际/实际";

		}else if("1".equals(LegDayCount)){
			return "1-实际/360";

		}else if("3".equals(LegDayCount)){
			return "3-实际/365";

		}else if("5".equals(LegDayCount)){
			return "5-实际/365F";

		}else if("D".equals(LegDayCount)){
			return "30E/360";

		}else{
			throw new RuntimeException("LegDayCount枚举值错误:"+LegDayCount);
		}
	}

	/***
	 * 11143ClearingMethod
	 6-集中清算
	 13-自行清算
	 *
	 * @param clearingMethod
	 * @return
	 */
	private static String transClearMthod(String clearingMethod) {
		String cm = "";
		if("6".equals(clearingMethod)){
			cm = "6-集中清算";

		}else if("13".equals(clearingMethod)){
			cm = "13-自行清算";
		}else{
			throw new RuntimeException("ClearingMethod[11143]枚举值错误,不在正常范围[6-集中清算13-自行清算]中:"+clearingMethod);
		}
		return cm;
	}


	private static String transRateType2(String RateType) {
		String cm = "";
		if("0".equals(RateType)){
			cm = "0-单利";

		}else if("1".equals(RateType)){
			cm = "1-复利";
		}else{
			throw new RuntimeException("RateType枚举值错误,不在正常范围[0-单利，1-复利]中:"+RateType);
		}
		return cm;
	}

	/***
	 * 异常数据信息
	 * @param eUtil
	 * @param errs2
	 */
	public static void writeErrorTrader(ExcelUtil e,Map<String, String> map) {
		int sheetIdx = e.getWb().getSheetIndex("异常消息");
		int row = e.getWb().getSheet("异常消息").getLastRowNum() + 1;

		for (Entry<String, String> entry : map.entrySet())
		{
			try {
				//A	成交编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(entry.getKey()));
				//B 异常原因
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(entry.getValue()));

				row++;

			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入异常数据信息出错:"+entry.getKey(), e2);
			}
		}// end for
	}

	/***
	 * 报文解析
	 *
	 * 信用拆借
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeMMTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		DepositsAndLoansBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("信用拆借-普通");
		int row = e.getWb().getSheet("信用拆借-普通").getLastRowNum() + 1;
		InstitutionBean instT = null; 
		InstitutionBean instM = null;
		
		SettBaseBean settT = null;
		SettBaseBean settM = null;
		
		for (int i = 0; i < trds.size(); i++) 
		{
			tradeBean = (DepositsAndLoansBean)trds.get(i);
			try {

				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B	成交编号
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//C 报价编号
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), null);
				//D	成交日期
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//E 成交时间
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//F 更新日
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), null);
				//G 交易品种
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//H	交易方向
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transMMSide(tradeBean.getPs()));
				//I	拆借利率(%)
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));
				//J	拆借金额(万元)
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmt(),10000));
				//K 拆借期限(天)
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTenor()));
				//L	应计利息(元)
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInterestAmt()));
				//M	到期还款金额(元)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityAmt()));
				//N	首次结算日
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
				//O	到期结算日
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityDate()));
				//P	实际占款天数(天)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getHoldingDays()));
				//K 应急标识
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), null);
				//K 报文动作
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//K 数据类型标识
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//K 补充条款标识
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), null);
				//K 成交状态

				if(tradeBean.getStatus() == null){
					e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), String.valueOf(tradeBean.getExecType()));
				}else{
					e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), transMMStatus(tradeBean.getStatus()));
				}

				//K 补充条款
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), null);

				//119 拆入
				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				
				//W	拆入方机构6位代码
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//X	拆入方资金账号
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//Y	拆入方资金账户户名
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//Z	拆入方资金开户行
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//AA	拆入方资金开户行联行行号
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//AB 拆入方地址
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getAddress()));
				//AC	拆入方交易员中文名称
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AD 拆入方会员中文简称
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AE	拆入方会员中文全称
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AF 拆入方会员简称
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AG 拆入方交易成员群组
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradGroup()));
				
				
				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				settM = getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				
				//AH	拆出方机构6位代码
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//AI	拆出方资金账号
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//AJ	拆出方资金账户户名
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//AK	拆出方资金开户行
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//AL	拆出方资金开户行联行行号
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//AM 拆出方地址
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getAddress()));
				//AN	拆出方交易员中文名称
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//AO 拆出方会员中文简称
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//AP	拆出方会员中文全称
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//AQ 拆出方会员简称
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//AR 拆出方交易成员群组
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradGroup()));
				
				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));
				
				e2.printStackTrace();
				LogManager.error("写入拆借交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}
	}

	/***
	 * 质押式回购
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeZyRepoTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		RepoCrBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("质押式回购");
		int row = e.getWb().getSheet("质押式回购").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (RepoCrBean)trds.get(i);
			try {
				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B 市场标识
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), null);
				//C	成交日期
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				// 成交时间
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), null);
				//E	成交编号
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//F	交易方向
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transRepoSide(tradeBean.getPs()));
				//G 交易方式
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), null);
				//H 回购方式
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), null);
				//I 续作标识
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 应急标识
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), null);
				//K 报文动作
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), null);
				//L 原成交编号
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), null);
				//M	回购利率(%)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));

				String secId = "";
				String secNm = "";
				String secQty = "";
				String secHirCut = "";
				List<SecurityInfoBean> securInfoList = tradeBean.getSecursInfo();
				SecurityInfoBean s = null;
				for (int j = 0; j < securInfoList.size(); j++)
				{
					s = securInfoList.get(j);
					if(j == securInfoList.size() - 1){//最后一条
						secId += s.getSecurityId();
						secNm += s.getSecurityName();
						secQty += s.getFaceAmt().doubleValue() / 10000;
						secHirCut += s.getHaircut();

					}else{
						secId += s.getSecurityId() + split;
						secNm += s.getSecurityName() + split;
						secQty += s.getFaceAmt().doubleValue() / 10000 + split;
						secHirCut += s.getHaircut() + split;
					}
				}

				//N	债券代码
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(secId));
				//O	债券名称
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(secNm));
				//P	券面总额(万元)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(secQty));
				//Q	折算比例(%)
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(secHirCut));
				//R	券面总额合计(万元)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTotalFaceAmt() != null
						? tradeBean.getTotalFaceAmt().doubleValue() / 10000 : tradeBean.getTotalFaceAmt()));
				//S	结算币种
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getSettCCy()));
				//T	交易金额(元)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getAmt()));
				//U	回购期限(天)
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTerm()));
				//V 应计利息总额(元)
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), null);
				//W	首次结算日
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecuritySettAmt().getSettDate()));
				//X	到期结算日
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettDate()));
				//Y	实际占款天数(天)
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getOccupancyDays()));
				//Z	到期结算金额(元)
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettAmt()));
				//AA 交易品种
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//AB	首次结算方式
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), transSettType(tradeBean.getComSecuritySettAmt().getSettType()));
				//AC	到期结算方式
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), transSettType2(tradeBean.getMatSecuritySettAmt().getSettType()));
				//AD 清算方式
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), null);
				//AE 成交状态
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), null);
				//AF 更新日
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), null);
				//AG 补充条款标识
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), null);
				//AH 补充条款
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), null);
				//AI 报价编号
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), null);
				//AJ 数据类型标识
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), null);


				instT = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());

				//				AK	正回购方机构6位代码
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AL 正回购方电话
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AM 正回购方地址
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), null);
				//AN 正回购方会员简称
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AO 正回购方会员中文全称
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AP 正回购方会员中文简称
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AQ	正回购方交易员中文名称
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AR 正回购方交易员名称
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//				AS	正回购方资金账户户名
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//				AT	正回购方清算行名称
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//				AU	正回购方资金账号
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//				AV	正回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//				AW	正回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctName()));
				//				AX	正回购方证券托管机构
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustInstitutionId()));
				//				AY	正回购方证券托管账号
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctNo()));
				//AZ 正回购方交易成员群组
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), null);

				settM = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				instM = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				//BA	逆回购方机构6位代码
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//BB 逆回购方电话
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//BC 逆回购方地址
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), null);
				//BD 逆回购方会员简称
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//BE 逆回购方会员中文全称
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//				BF	逆回购方会员中文全称
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//				BG	逆回购方交易员中文名称
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BH 逆回购方交易员名称
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//				BI	逆回购方资金账户户名
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//				BJ	逆回购方清算行名称
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//				BK	逆回购方资金账号
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//				BL	逆回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//				BM	逆回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctName()));
				//				BN	逆回购方证券托管机构
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustInstitutionId()));
				//				BO	逆回购方证券托管账号
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctNo()));
				//BP 逆回购方交易成员群组
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), null);

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入质押回购出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}

		}
	}

	/**
	 * 质押-双边回购
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeZySbRepoTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		RepoCrBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("质押-双边回购");
		int row = e.getWb().getSheet("质押-双边回购").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (RepoCrBean)trds.get(i);
			try {
				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B	成交编号
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//C 市场标识
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), null);
				//D	成交日期
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//E 成交时间
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//F	交易方向
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transRepoSide(tradeBean.getPs()));
				//G 交易方式
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), null);
				//H 回购方式
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), null);
				//I 续作标识
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 应急标识
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), null);
				//K 报文动作
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//L 原成交编号
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), null);
				//M	回购利率(%)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));

				String secId = "";
				String secNm = "";
				String secQty = "";
				String secHirCut = "";
				List<SecurityInfoBean> securInfoList = tradeBean.getSecursInfo();
				SecurityInfoBean s = null;

				for (int k = 0;k < securInfoList.size(); k++)
				{
					s = securInfoList.get(k);
					secId = addStr(secId,nullToEmpty(s.getSecurityId()),split);
					secNm = addStr(secNm,nullToEmpty(s.getSecurityName()), split);
					secQty = addStr(secQty, nullToEmpty(s.getFaceAmt()), split);
					secHirCut = addStr(secHirCut, nullToEmpty(s.getHaircut()), split);

					if(k == securInfoList.size() - 1){
						secId = subStr(secId);
						secNm = subStr(secNm);
						secQty = subStr(secQty);
						secHirCut = subStr(secHirCut);
					}
				}

				//N	债券代码
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(secId));
				//O	债券名称
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(secNm));
				//P	券面总额(万元)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(secQty));
				//Q	折算比例(%)
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(secHirCut));
				//R	券面总额合计(万元)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTotalFaceAmt()));
				//S	结算币种
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getSettCCy()));
				//T	交易金额(元)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getAmt()));
				//U	回购期限(天)
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTerm()));
				//V 应计利息总额(元)
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getInterestTotalAmt()));
				//W	首次结算日
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecuritySettAmt().getSettDate()));
				//X	到期结算日
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettDate()));
				//Y	实际占款天数(天)
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getOccupancyDays()));
				//Z	到期结算金额(元)
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettAmt()));
				//AA 交易品种
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//AB	首次结算方式
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), transSettType(tradeBean.getComSecuritySettAmt().getSettType()));
				//AC	到期结算方式
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), transSettType2(tradeBean.getMatSecuritySettAmt().getSettType()));
				//AD 清算方式
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), null);
				//AE 成交状态
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), null);
				//AF 更新日
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), null);
				//AG 补充条款标识
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), null);
				//AH 补充条款
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), null);
				//AI 报价编号
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), null);
				//AJ 数据类型标识
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));


				instT = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());

				//				AK	正回购方机构6位代码
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));

				//				AL	正回购方交易员中文名称
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AM 正回购方交易员名称
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));

				//AN 正回购方电话
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AO 正回购方地址
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), null);
				//AP 正回购方机构英文简称
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getSwiftCode()));
				//AQ 正回购方会员中文全称
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AR 正回购方会员中文简称
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AS	正回购方资金账户户名
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//				AT	正回购方清算行名称
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//				AU	正回购方资金账号
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//				AV	正回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//				AW	正回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctName()));
				//				AX	正回购方证券托管机构
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustInstitutionId()));
				//				AY	正回购方证券托管账号
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctNo()));
				//AZ 正回购方交易成员群组
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradGroup()));

				settM = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				instM = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				//BA	逆回购方机构6位代码
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));

				//				BB	逆回购方交易员中文名称
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BC 逆回购方交易员名称
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BD 逆回购方电话
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//BE 逆回购方地址
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), null);
				//BF 逆回购方机构英文简称
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getSwiftCode()));
				//BG 逆回购方会员中文全称
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//				BH	逆回购方会员中文简称
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				BI	逆回购方资金账户户名
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//				BJ	逆回购方清算行名称
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//				BK	逆回购方资金账号
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//				BL	逆回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//				BM	逆回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctName()));
				//				BN	逆回购方证券托管机构
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustInstitutionId()));
				//				BO	逆回购方证券托管账号
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctNo()));
				//BP 逆回购方交易成员群组
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradGroup()));

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入质押回购出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}

		}
	}

	/**
	 * 质押-通用回购
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeZyTyRepoTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		RepoCrBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("质押-通用回购");
		int row = e.getWb().getSheet("质押-通用回购").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (RepoCrBean)trds.get(i);
			try {
				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B	成交编号
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//C 市场标识
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), null);
				//D	成交日期
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//E 成交时间
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//F	交易方向
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transRepoSide(tradeBean.getPs()));
				//G 交易方式
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), null);
				//H 回购方式
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), null);
				//I 续作标识
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 应急标识
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), null);
				//K 报文动作
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//L 原成交编号
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), null);
				//M	回购利率(%)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));

				String secId = "";
				String secNm = "";
				String secQty = "";
				String secHirCut = "";
				List<SecurityInfoBean> securInfoList = tradeBean.getSecursInfo();
				SecurityInfoBean s = null;
				for (int j = 0; j < securInfoList.size(); j++)
				{
					s = securInfoList.get(j);
					if(j == securInfoList.size() - 1){//最后一条
						secId += s.getSecurityId();
						secNm += s.getSecurityName();
						secQty += s.getFaceAmt().doubleValue() / 10000;
						secHirCut += s.getHaircut();

					}else{
						secId += s.getSecurityId() + split;
						secNm += s.getSecurityName() + split;
						secQty += s.getFaceAmt().doubleValue() / 10000 + split;
						secHirCut += s.getHaircut() + split;
					}
				}

				//N	债券代码
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(secId));
				//O	债券名称
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(secNm));
				//P	券面总额(万元)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(secQty));
				//Q	折算比例(%)
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(secHirCut));
				//R	券面总额合计(万元)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTotalFaceAmt() != null
						? tradeBean.getTotalFaceAmt().doubleValue() / 10000 : tradeBean.getTotalFaceAmt()));
				//S	结算币种
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getSettCCy()));
				//T	交易金额(元)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getAmt()));
				//U	回购期限(天)
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTerm()));
				//V 应计利息总额(元)
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getInterestTotalAmt()));
				//W	首次结算日
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecuritySettAmt().getSettDate()));
				//X	到期结算日
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettDate()));
				//Y	实际占款天数(天)
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getOccupancyDays()));
				//Z	到期结算金额(元)
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettAmt()));
				//AA 交易品种
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//AB	首次结算方式
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), transSettType(tradeBean.getComSecuritySettAmt().getSettType()));
				//AC	到期结算方式
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), transSettType2(tradeBean.getMatSecuritySettAmt().getSettType()));
				//AD 清算方式
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), null);
				//AE 成交状态
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), null);
				//AF 更新日
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), null);
				//AG 补充条款标识
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), null);
				//AH 补充条款
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), null);
				//AI 报价编号
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), null);
				//AJ 数据类型标识
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));


				instT = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());

				//				AK	正回购方机构6位代码
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));

				//				AL	正回购方交易员中文名称
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AM 正回购方交易员名称
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));

				//AN 正回购方电话
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AO 正回购方地址
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), null);
				//AP 正回购方机构英文简称
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getSwiftCode()));
				//AQ 正回购方会员中文全称
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AR 正回购方会员中文简称
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AS	正回购方资金账户户名
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//				AT	正回购方清算行名称
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//				AU	正回购方资金账号
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//				AV	正回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//				AW	正回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctName()));
				//				AX	正回购方证券托管机构
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustInstitutionId()));
				//				AY	正回购方证券托管账号
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctNo()));
				//AZ 正回购方交易成员群组
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), null);

				settM = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				instM = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				//BA	逆回购方机构6位代码
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));

				//				BB	逆回购方交易员中文名称
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BC 逆回购方交易员名称
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BD 逆回购方电话
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//BE 逆回购方地址
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), null);
				//BF 逆回购方机构英文简称
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getSwiftCode()));
				//BG 逆回购方会员中文全称
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//				BH	逆回购方会员中文简称
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				BI	逆回购方资金账户户名
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//				BJ	逆回购方清算行名称
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//				BK	逆回购方资金账号
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//				BL	逆回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//				BM	逆回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctName()));
				//				BN	逆回购方证券托管机构
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustInstitutionId()));
				//				BO	逆回购方证券托管账号
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctNo()));
				//BP 逆回购方交易成员群组
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), null);

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入质押回购出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}

		}
	}

	/**
	 * 质押-跨托管
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeZyKtgRepoTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		RepoCrBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("质押-跨托管");
		int row = e.getWb().getSheet("质押-跨托管").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (RepoCrBean)trds.get(i);
			try {
				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B	成交编号
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//C	结算币种
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getSettCCy()));
				//D	券面总额合计(万元)
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTotalFaceAmt()));
				//E 回购方式
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), transRepoMethod(tradeBean.getRepoMethod()));
				//F	回购利率(%)
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));
				//G 交易品种
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//H	交易方向
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transRepoSide(tradeBean.getPs()));
				//I 补充条款
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 更新日
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), null);
				//K	首次结算日
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecuritySettAmt().getSettDate()));
				//L	成交日期
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//M 报价编号
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), null);
				//N 成交状态
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), null);
				//O	到期结算日
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettDate()));
				//P	首次结算方式
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), transSettType(tradeBean.getComSecuritySettAmt().getSettType()));
				//Q 应计利息总额(元)
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getInterestTotalAmt()));
				//R	实际占款天数(天)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getOccupancyDays()));
				//S	到期结算方式
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), transSettType2(tradeBean.getMatSecuritySettAmt().getSettType()));
				//T 清算方式
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), null);
				//U 续作标识
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), null);
				//V 原成交编号
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), null);
				//W 应急标识
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), null);
				//X 报文动作
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//Y 市场标识
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), null);
				//Z 补充条款标识
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), null);
				//AA 成交时间
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), null);
				//AB 交易方式
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), null);
				//AC	交易金额(元)
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmtDeail().getAmt()));
				//AD	回购期限(天)
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTerm()));
				//AE	到期结算金额(元)
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecuritySettAmt().getSettAmt()));
				//AF 数据类型标识
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));

				String secId = "";
				String secNm = "";
				String secQty = "";
				String secHirCut = "";
				String securityAccNameS = "";

				List<SecurityInfoBean> securInfoList = tradeBean.getSecursInfo();
				SecurityInfoBean s = null;
				for (int j = 0; j < securInfoList.size(); j++)
				{
					s = securInfoList.get(j);

					if(j == securInfoList.size() - 1){//最后一条
						secId += s.getSecurityId();
						secNm += s.getSecurityName();
						secQty += s.getFaceAmt();
						secHirCut += s.getHaircut();
						securityAccNameS += s.getSecurityAccName();

					}else{
						secId += s.getSecurityId() + split;
						secNm += s.getSecurityName() + split;
						secQty += s.getFaceAmt() + split;
						secHirCut += s.getHaircut() + split;
						securityAccNameS += s.getSecurityAccName() + split;
					}
				}
				//AG	债券代码
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), nullToEmpty(secId));
				//AH	债券名称
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(secNm));
				//AI	券面总额(万元)
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(secQty));
				//AJ	折算比例(%)
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(secHirCut));

				String custodianInstitutionNameS = "";
				String custodianTradeAmtS = "";
				String custodianAccruedInterestAmtS = "";
				String custodianSettlCurrAmt2S = "";
				String custodianQtyS = "";

				SecurityCustodianBean c = null;
				List<SecurityCustodianBean> secursCustodian = tradeBean.getSecursCustodian();
				for (int j = 0; j < secursCustodian.size(); j++)
				{
					c = secursCustodian.get(j);
					if(j == secursCustodian.size() - 1){//最后一条

						custodianInstitutionNameS +=c.getCustodianInstitutionName();
						custodianTradeAmtS += c.getCustodianTradeAmt();
						custodianAccruedInterestAmtS += c.getCustodianAccruedInterestAmt();
						custodianSettlCurrAmt2S += c.getCustodianSettlCurrAmt2();
						custodianQtyS += c.getCustodianQty();

					}else{

						custodianInstitutionNameS +=c.getCustodianInstitutionName() + split;
						custodianTradeAmtS += c.getCustodianTradeAmt() + split;
						custodianAccruedInterestAmtS += c.getCustodianAccruedInterestAmt() + split;
						custodianSettlCurrAmt2S += c.getCustodianSettlCurrAmt2() + split;
						custodianQtyS += c.getCustodianQty()+ split;
					}
				}

				//AK 托管机构
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), null);
				//AL 债券托管机构名称
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(securityAccNameS));
				//AM 跨托管机构名称
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(custodianInstitutionNameS));
				//AN 跨托管交易金额(元)
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(custodianTradeAmtS));
				//AO 跨托管回购利息
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(custodianAccruedInterestAmtS));
				//AP 跨托管到期结算金额
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(custodianSettlCurrAmt2S));
				//AQ 跨托管券面总额
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(custodianQtyS));


				instT = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());

				String acctNames = "";
				String bankNames = "";
				String bankAcctNos = "";
				String acctNos = "";
				String trustAcctNames = "";
				String trustInstitutionIds = "";
				String trustAcctNos = "";
				String tradGroups = "";

				SecurSettBean securSettBean;
				for (int k = 0;k < settT.getSecurSettList().size(); k++)
				{
					securSettBean = settT.getSecurSettList().get(k);
					acctNames = addStr(acctNames,nullToEmpty(securSettBean.getAcctName()),split);
					bankNames = addStr(bankNames,nullToEmpty(securSettBean.getBankName()), split);
					bankAcctNos = addStr(bankAcctNos, nullToEmpty(securSettBean.getBankAcctNo()), split);
					acctNos = addStr(acctNos, nullToEmpty(securSettBean.getAcctNo()), split);
					trustAcctNames = addStr(trustAcctNames, nullToEmpty(securSettBean.getTrustAcctName()), split);
					trustInstitutionIds = addStr(trustInstitutionIds, nullToEmpty(securSettBean.getTrustInstitutionId()), split);
					trustAcctNos = addStr(trustAcctNos, nullToEmpty(securSettBean.getTrustAcctNo()), split);


					if(k == settT.getSecurSettList().size() - 1){
						acctNames = subStr(acctNames);
						bankNames = subStr(bankNames);
						bankAcctNos = subStr(bankAcctNos);
						acctNos = subStr(acctNos);
						trustAcctNames = subStr(trustAcctNames);
						trustInstitutionIds = subStr(trustInstitutionIds);
						trustAcctNos = subStr(trustAcctNos);
					}
				}

				//				AR	正回购方机构6位代码
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AS 正回购方电话
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AT 正回购方地址
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), null);
				//AU 正回购方机构英文简称
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getSwiftCode()));
				//AV 正回购方会员中文全称
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AW 正回购方会员中文简称
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AX	正回购方交易员中文名称
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AY 正回购方交易员名称
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AZ 正回购方交易成员群组
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradGroup()));
				//				BA	正回购方资金账户户名
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(acctNames));
				//				BB	正回购方清算行名称
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(bankNames));
				//				BC	正回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(bankAcctNos));
				//				BD	正回购方资金账号
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(acctNos));
				//				BE	正回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(trustAcctNames));
				//				BF	正回购方证券托管机构
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(trustInstitutionIds));
				//				BG	正回购方证券托管账号
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(trustAcctNos));

				settM = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				instM = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());

				String acctNamex = "";
				String bankNamex = "";
				String bankAcctNox = "";
				String acctNox = "";
				String trustAcctNamex = "";
				String trustInstitutionIdx = "";
				String trustAcctNox = "";

				SecurSettBean securSettBeanx;
				for (int k = 0;k < settM.getSecurSettList().size(); k++)
				{
					securSettBeanx = settM.getSecurSettList().get(k);
					acctNamex = addStr(acctNamex,nullToEmpty(securSettBeanx.getAcctName()),split);
					bankNamex = addStr(bankNamex,nullToEmpty(securSettBeanx.getBankName()), split);
					bankAcctNox = addStr(bankAcctNox, nullToEmpty(securSettBeanx.getBankAcctNo()), split);
					acctNox = addStr(acctNox, nullToEmpty(securSettBeanx.getAcctNo()), split);
					trustAcctNamex = addStr(trustAcctNamex, nullToEmpty(securSettBeanx.getTrustAcctName()), split);
					trustInstitutionIdx = addStr(trustInstitutionIdx, nullToEmpty(securSettBeanx.getTrustInstitutionId()), split);
					trustAcctNox = addStr(trustAcctNox, nullToEmpty(securSettBeanx.getTrustAcctNo()), split);

					if(k == settM.getSecurSettList().size() - 1){
						acctNamex = subStr(acctNamex);
						bankNamex = subStr(bankNamex);
						bankAcctNox = subStr(bankAcctNox);
						acctNox = subStr(acctNox);
						trustAcctNamex = subStr(trustAcctNamex);
						trustInstitutionIdx = subStr(trustInstitutionIdx);
						trustAcctNox = subStr(trustAcctNox);
					}
				}
				//BH	逆回购方机构6位代码
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//BI 逆回购方电话
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//BJ 逆回购方地址
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), null);
				//BK 逆回购方机构英文简称
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getSwiftCode()));
				//BL 逆回购方会员中文全称
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//				BM	逆回购方会员中文简称
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				BN	逆回购方交易员中文名称
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BO 逆回购方交易员名称
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BP 逆回购方交易成员群组
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradGroup()));
				//				BQ	逆回购方资金账户户名
				e.writeStr(sheetIdx, row, "BQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(acctNamex));
				//				BR	逆回购方清算行名称
				e.writeStr(sheetIdx, row, "BR", e.getDefaultLeftStrCellStyle(), nullToEmpty(bankNamex));
				//				BS	逆回购方清算行清算系统号
				e.writeStr(sheetIdx, row, "BS", e.getDefaultLeftStrCellStyle(), nullToEmpty(bankAcctNox));
				//				BT	逆回购方资金账号
				e.writeStr(sheetIdx, row, "BT", e.getDefaultLeftStrCellStyle(), nullToEmpty(acctNox));
				//				BU	逆回购方证券托管账户户名
				e.writeStr(sheetIdx, row, "BU", e.getDefaultLeftStrCellStyle(), nullToEmpty(trustAcctNamex));
				//				BV	逆回购方证券托管机构
				e.writeStr(sheetIdx, row, "BV", e.getDefaultLeftStrCellStyle(), nullToEmpty(trustInstitutionIdx));
				//				BW	逆回购方证券托管账号
				e.writeStr(sheetIdx, row, "BW", e.getDefaultLeftStrCellStyle(), nullToEmpty(trustAcctNox));

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入质押回购出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}

		}
	}


	/***
	 * 买断式回购
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeMdRepoTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		RepoOrBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("买断式回购-单券");
		int row = e.getWb().getSheet("买断式回购-单券").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (RepoOrBean)trds.get(i);
			try {

				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B 市场标识
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), "10-买断式回购");
				//C	成交日期
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//D 成交时间
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//E	成交编号
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//F 报价编号
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), null);
				//G	交易方向
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transRepoSide(tradeBean.getPs()));
				//H	债券代码
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityId()));
				//I	债券名称
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityName()));
				//J 单个券券面总额
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getFaceAmt()));
				//K	回购利率(%)
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getHaircut()));
				//L	首次净价(元)
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getPrice()));
				//M	到期净价(元)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getPrice()));
				//N 待偿期
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), null);
				//O 首次收益率(%)
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getYeld()));
				//P 到期收益率(%)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getYeld()));
				//Q	券面总额(万元)
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getFaceAmt()));
				//R	结算币种
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getSettCCy()));
				//S 数据类型标识
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//T	回购期限(天)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTerm()));
				//U 首次应计利息(元)
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getInterestAmt()));
				//V	  首次全价(元)
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getDirtyPrice()));
				//W 到期应计利息(元)
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getInterestAmt()));
				//X 到期全价(元)
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getDirtyPrice()));
				//Y	首次结算日
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getSettDate()));
				//Z	到期结算日
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getSettDate()));
				//AA	实际占款天数(天)
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getOccupancyDays()));
				//AB	首次结算金额(元)
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getSettAmt()));
				//AC	到期结算金额(元)
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getSettAmt()));
				//AD 交易品种
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//AE	首次结算方式
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), transSettType(tradeBean.getComSecurityAmt().getSettType()));
				//AF	到期结算方式
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), transSettType2(tradeBean.getMatSecurityAmt().getSettType()));
				//AG 清算方式
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), null);
				//AH 应急标识
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), null);
				//AI 报文动作
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//AJ 交易方式
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), null);
				//AK 成交状态
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), null);
				//AL 更新日
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), null);
				//AM 有无补充条款
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.isRemarkIndicator() ? "Y-YES" : "N-NO"));
				//AN 有无保证品
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), null);
				//AO 补充条款
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), null);

				instT = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());

				//				AP	正回购方机构6位代码
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AQ 正回购方电话
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AR 正回购方地址
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), null);
				//AS 正回购方会员简称
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AT	正回购方会员中文全称
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AU 正回购方会员中文简称
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AV	正回购方资金账户户名
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//				AW	正回购方资金开户行
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//				AX	正回购方资金账号
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//				AY	正回购方资金开户行联行行号
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//				AZ	正回购方托管账户户名
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctName()));
				//				BA	正回购方托管机构
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustInstitutionId()));
				//				BB	正回购方托管账号
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctNo()));
				//BC 正回购方交易成员群组
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradGroup()));


				instM = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settM = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());

				//				BD	逆回购方机构6位代码
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//BE 逆回购方电话
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//BF 逆回购方地址
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), null);
				//BG 逆回购方会员简称
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				BH	逆回购方会员中文全称
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//BI 逆回购方会员中文简称
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				BJ	逆回购方资金账户户名
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//				BK	逆回购方资金开户行
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//				BL	逆回购方资金账号
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//				BM	逆回购方资金开户行联行行号
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//				BN	逆回购方托管账户户名
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctName()));
				//				BO	逆回购方托管机构
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustInstitutionId()));
				//				BP	逆回购方托管账号
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctNo()));
				//BQ 逆回购方交易成员群组
				e.writeStr(sheetIdx, row, "BQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradGroup()));

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入买断回购出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}

	}

	/***
	 * 现券买卖
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeBondTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		FixedIncomeBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("现券买卖");
		int row = e.getWb().getSheet("现券买卖").getLastRowNum() + 1;
		InstitutionBean instT = null; 
		InstitutionBean instM = null;
		
		SecurSettBean settT = null;
		SecurSettBean settM = null;
		
		for (int i = 0; i < trds.size(); i++) 
		{
			tradeBean = (FixedIncomeBean)trds.get(i);
			try {
				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B 市场标识
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), null);
				//C	成交日期
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//D 成交时间
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//E	成交编号
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//F 业务发生时间
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), null);
				//G	交易方向
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transSide(tradeBean.getPs()));
				//H	债券代码
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityId()));
				//I	债券名称
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityName()));
				//J 清算速度
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), null);
				//K	净价(元)
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getPrice()));
				//L 行权收益率(%)
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), null);
				//M 到期收益率(%)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), null);
				//N	券面总额(万元)
				BigDecimal bigDecimal = tradeBean.getSecurityInfo().getFaceAmt().compareTo(new BigDecimal("0")) == 0 ? null : tradeBean.getSecurityInfo().getFaceAmt().multiply(new BigDecimal("10000"));
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(bigDecimal));
				//O 每百元本金额(元)
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), null);
				//P 当前本金额(元)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), null);
				//Q	结算币种
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getSettCCy()));
				//R	交易金额(元)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getAmt()));
				//S	结算日
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getSettDate()));
				//T	应计利息(元)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getInterestAmt()));
				//U	全价(元)
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getDirtyPrice()));
				//V	应计利息总额(元)
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getInterestTotalAmt()));
				//W	结算金额(元)
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityAmt().getSettAmt()));
				//X	结算方式
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), transSettTypeSec(tradeBean.getSecurityAmt().getSettType()));
				//Y 清算方式
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), null);
				//Z 补充条款标识
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), null);
				//AA 成交状态
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), null);
				//AB 成交类别
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), null);
				//AC 补充条款
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), null);
				//AD 待偿期
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), null);
				//AE 报价编号
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), null);
				//AF 请求报价编号
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), null);
				//AG 订单编号
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), null);
				//AH 应急标识
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), null);
				//AI 成交传输动作状态
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), transSecDealTransType(tradeBean.getDealStatus()));
				//AJ 现券标识
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), null);
				//AK 交易方式
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), null);
				//AL 数据类型标识
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				
				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				
				//				AM	买入方机构6位代码
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AN 买入方电话
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AO 买入方地址
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), null);
				//AP 买入方会员简称
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AQ	买入方会员中文全称
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AR 买入方会员中文简称
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AS	买入方交易员中文名称
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//				AT	买入方资金账户户名
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//				AU	买入方资金开户行
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//				AV	买入方资金账号
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//				AW	买入方资金开户行联行行号
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//				AX	买入方托管账户户名
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctName()));
				//				AY	买入方托管机构
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustInstitutionId()));
				//				AZ	买入方托管账号
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctNo()));
				//BA 买入方交易成员群组
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), null);

				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settM = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//				BB	卖出方机构6位代码
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//BC 卖出方电话
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//BD 卖出方地址
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), null);
				//BE 卖出方会员简称
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				BF	卖出方会员中文全称
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//BG 卖出方会员中文简称
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				BH	卖出方交易员中文名称
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//				BI	卖出方资金账户户名
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//				BJ	卖出方资金开户行
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//				BK	卖出方资金账号
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//				BL	卖出方资金开户行联行行号
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//				BM	卖出方托管账户户名
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctName()));
				//				BN	卖出方托管机构
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustInstitutionId()));
				//				BO	卖出方托管账号
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctNo()));
				//BP 卖出方交易成员群组
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), null);
				
				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));
				
				e2.printStackTrace();
				LogManager.error("写入债券交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			} 
		}	
	}

	/**
	 * 债券远期
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeBondFwTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs){
		BfdBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("债券远期");
		int row = e.getWb().getSheet("债券远期").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (BfdBean)trds.get(i);
			try{
				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B 市场标识
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), "5-债券远期");
				//C 成交日期
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//D 成交时间
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//E 成交编号
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//F 交易品种
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//G 交易方向
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transBfwSide(tradeBean.getPs()));
				//H 总券面总额(元)
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getFaceAmt()));
				//I 债券代码
//				if(tradeBean.getSecurityInfo().getSecurityId()==null){
//					throw new RuntimeException("债券代码取值有误,不在正常范围中," + tradeBean.getExecId());
//				}

				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityId()));
				//J 债券名称
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityName()));
				//K 远期净价(元)
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), null);
				//L 远期行权收益率(%)
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), null);
				//M 远期收益率(%)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), null);
				//N 券面总额(元)
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getFaceAmt()));
				//O 结算币种
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSettlCurrency()));
				//P 应急标识
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), null);
				//Q 报文动作
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//R 交易方式
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), null);
				//S 数据类型标识
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//T 交易金额(元)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeAmt()));
				//U 合约期限(天)
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeLimitDays()));
				//V 结算日
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSettlDate()));
				//W 应计利息(元)
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getAccruedInt()));
				//X 全价(元)
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getdPrice()));
				//Y 应计利息总额(元)
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAccruedIntAmt()));
				//Z 结算金额(元)
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSettlCurrAmt()));
				//AA 结算方式
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), transFwdDeliveryType(tradeBean.getDeliveryType()));
				//AB 补充条款标识
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), null);
				//AC 成交状态
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), null);
				//AD 更新日
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTransactTime()));
				//AE 有无保证品
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), null);
				//AF 补充条款
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), null);
				//AG 待偿期
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), null);
				//AH 报价编号
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), null);

				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//AI 买入方机构6位代码
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AJ 买入方电话
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AK 买入方地址
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), null);
				//AL 买入方会员简称
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AM 买入方会员中文全称
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AN 买入方会员中文简称
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AO 买入方交易员中文名称
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AP 买入方资金账户户名
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//AQ 买入方资金开户行
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//AR 买入方资金账号
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//AS 买入方资金开户行联行行号
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//AT 买入方托管账户户名
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctName()));
				//AU 买入方托管机构
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustInstitutionId()));
				//AV 买入方托管账号
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctNo()));
				//AW 买入方交易成员群组
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), null);

				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settM = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//AX 卖出方机构6位代码
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//AY 卖出方电话
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//AZ 卖出方地址
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), null);
				//BA 卖出方会员简称
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//BB 卖出方会员中文全称
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//BC 卖出方会员中文简称
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//BD 卖出方交易员中文名称
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BE 卖出方资金账户户名
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//BF 卖出方资金开户行
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//BG 卖出方资金账号
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//BH 卖出方资金开户行联行行号
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//BI 卖出方托管账户户名
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctName()));
				//BJ 卖出方托管机构
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustInstitutionId()));
				//BK 卖出方托管账号
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctNo()));
				//BL 卖出方交易成员群组
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), null);

				row++;
			}catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入债券远期交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}
	}

	/**
	 * 债券借贷
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeSlTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs){
		SecurityLendingBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("债券借贷");
		int row = e.getWb().getSheet("债券借贷").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (SecurityLendingBean)trds.get(i);
			try{
				//A 客户端参考编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B 市场标识
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), null);
				//C 成交日期
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//D 成交时间
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//E 交易方式
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), null);
				//F 成交编号
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//G 报价编号
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), null);
				//H 交易方向
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transSlSide(tradeBean.getPs()));
				//I 成交券面总额
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 标的债券代码
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityId()));
				//K 标的债券名称
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getSecurityName()));
				//L 借贷费率(%)
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getLendingFeeRate()));
				//M 标的债券券面总额（元）
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getFaceAmt()));
				//N 结算币种
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getSettCCy()));
				//O 应急标识
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), null);
				//P 报文动作
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//Q 数据类型标识
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//R 借贷期限(天)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTerm()));
				//S 首次结算日
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getComSecurityAmt().getSettDate()));
				//T 到期结算日
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatSecurityAmt().getSettDate()));
				//U 首次结算方式
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), transSettType(tradeBean.getComSecurityAmt().getSettType()));
				//V 到期结算方式
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), transSettType(tradeBean.getMatSecurityAmt().getSettType()));
				//W 实际占券天数
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getOccupancyDays()));
				//X 争议解决方式
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), transDisputeWay(tradeBean.getDisputeWay()));
				//Y 借贷费用(元)
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getLengingFeeCost()));
				//Z 交易品种
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getProductName()));
				//AA 补充条款标识
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), null);
				//AB 补充条款
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), null);
				//AC 付息标识
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), null);
				//AD 成交状态
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), null);
				//AE 更新日
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), null);

				String securityIds = "";
				String securityNames = "";
				String faceAmts = "";
				SecurityInfoBean infoBean;
				for (int k = 0;k < tradeBean.getLendingInfos().size(); k++)
				{
					infoBean = tradeBean.getLendingInfos().get(k);
					securityIds = addStr(securityIds,nullToEmpty(infoBean.getSecurityId()),split);
					securityNames = addStr(securityNames,nullToEmpty(infoBean.getSecurityName()), split);
					faceAmts = addStr(faceAmts, nullToEmpty(infoBean.getFaceAmt()), split);

					if(k == tradeBean.getLendingInfos().size() - 1){
						securityIds = subStr(securityIds);
						securityNames = subStr(securityNames);
						faceAmts = subStr(faceAmts);
					}
				}
				//AF 质押债券代码
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), securityIds);
				//AG 质押债券名称
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), securityNames);
				//AH 质押债券券面总额（元）
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), faceAmts);
				//AI 付息日
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getIntPayDate()));
				//AJ 票面利率（%）
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getRate()));
				//AK 应计利息总额（元）
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityInfo().getAccruedInt()));
				//AL 质押债券券面总额合计（元）
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaginTotalAmt()));
				//AM 置换标识
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMarginReplacement() ? "Y-YES":"N-NO"));

				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settT = (SecurSettBean)getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//AN 融入方机构6位代码
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AO 融入方电话
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AP 融入方地址
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), null);
				//AQ 融入方会员简称
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AR 融入方会员中文全称
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AS 融入方会员中文简称
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AT 融入方交易员中文名称
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AU 融入方资金账户户名
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//AV 融入方资金开户行
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//AW 融入方资金账号
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//AX 融入方资金开户行联行行号
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//AY 融入方托管账户户名
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctName()));
				//AZ 融入方托管机构
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustInstitutionId()));
				//BA 融入方托管账号
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getTrustAcctNo()));
				//BB 融入方交易成员群组
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), null);

				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(), tradeBean.getSettInfo().getMarkerTakerRole());
				settM = (SecurSettBean)getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//BC 融出方机构6位代码
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//BD 融出方电话
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//BE 融出方地址
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), null);
				//BF 融出方会员简称
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//BG 融出方会员中文全称
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//BH 融出方会员中文简称
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//BI 融出方交易员中文名称
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BJ 融出方资金账户户名
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//BK 融出方资金开户行
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//BL 融出方资金账号
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//BM 融出方资金开户行联行行号
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//BN 融出方托管账户户名
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctName()));
				//BO 融出方托管机构
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustInstitutionId()));
				//BP 融出方托管账号
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getTrustAcctNo()));
				//BQ 融出方交易成员群组
				e.writeStr(sheetIdx, row, "BQ", e.getDefaultLeftStrCellStyle(), null);

				row++;
			}catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入债券借贷交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}
	}

	/***
	 * 同业存单 招标发行
	 * 存单发型 招标发行 本方招标发行结果
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeCD_ZBTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		IbncdBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("本方招标发行结果");
		int row = e.getWb().getSheet("本方招标发行结果").getLastRowNum() + 1;
		InstitutionBean instM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (IbncdBean)trds.get(i);
			try {
				//A	工具类型
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), transSecType(tradeBean.getSecType()));
				//B	工具全称
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFullName()));
				//C	工具简称
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getShortName()));
				//D	工具代码
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecId()));
				//E	息票类型
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), transRateType(tradeBean.getRateType()));
				//F	发行方式/招标方式
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transIssMethod(tradeBean.getIssMethod()));
				//G	工具状态
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transSecStatus(tradeBean.getSecStatus()));
				//H	失败原因
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getText()));
				//I	发行价格（元）
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIssPrice()));
				//J	计划发行量（亿元）
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIssSizeForPlan()));
				//K	实际发行量（亿元）
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIssSize()));
				//L	利差（BP）
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSpread()));
				//M	票面利率(%)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCouponRate()));
				//N	招标标的
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), transTenderSubject(tradeBean.getTenderSubject()));
				//O	投标总量（亿元）
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTotalTenderAmt()));
				//P	中标总量（亿元）
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTotalTenderSuccessAmt()));
				//Q	投标倍数
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), tradeBean.getTenderMultiples() == null ? "" : nullToEmpty(tradeBean.getTenderMultiples().doubleValue()));
				//R	投标家数
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), tradeBean.getTenderParties() == 0 ? "" : nullToEmpty(tradeBean.getTenderParties()));
				//S	中标家数
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), tradeBean.getTenderSuccessParties() == 0 ? "" : nullToEmpty(tradeBean.getTenderSuccessParties()));
				//T	投标笔数
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), tradeBean.getTenderNumber() == 0 ? "" : nullToEmpty(tradeBean.getTenderNumber()));
				//U	中标笔数
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), tradeBean.getTenderSuccessNumber() == 0 ? "" : nullToEmpty(tradeBean.getTenderSuccessNumber()));
				//V	最高投标价位
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), null);
				//W	最低投标价位
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), null);
				//X	边际中标价位投标总量（亿元）
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), null);
				//Y	边际中标价位中标总量（亿元）
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), null);
				//Z	边际中标价
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), null);
				//AA 参考收益率
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), null);

				//发行人
				if(MarkerTakerEnum.Marker.equals(tradeBean.getSettInfo().getMarkerTakerRole())){
					instM = tradeBean.getInstitutionInfo();
				}else{
					instM = tradeBean.getContraPatryInstitutionInfo();
				}
				//AB	发行人机构21位码
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//AC	发行人机构中文全称
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));

				String instIds = "";
				String names = "";
				String feeActAmts = "";
				String pxs = "";
				String tenderAmts = "";
				String tenderSucPxs = "";
				String tenderSucAmts = "";
				SubScriptionInstBean inst;
				for (int k = 0;k < tradeBean.getSubScriptions().size(); k++)
				{
					inst = tradeBean.getSubScriptions().get(k);
					instIds = addStr(instIds,nullToEmpty(inst.getInstitutionId()),split);
					names = addStr(names,nullToEmpty(inst.getFullName()), split);
					feeActAmts = addStr(feeActAmts, nullToEmpty(inst.getFeeActualAmt()), split);
					pxs = addStr(pxs,nullToEmpty(inst.getTenderPx()),split);
					tenderAmts = addStr(tenderAmts,nullToEmpty(inst.getTenderAmt()) , split);
					tenderSucPxs = addStr(tenderSucPxs,nullToEmpty(inst.getTenderSuccessPx()), split);
					tenderSucAmts = addStr(tenderSucAmts,nullToEmpty(inst.getTenderSuccessAmt()) , split);

					if(k == tradeBean.getSubScriptions().size() - 1){
						instIds = subStr(instIds);
						names = subStr(names);
						feeActAmts = subStr(feeActAmts);
						pxs = subStr(pxs);
						tenderAmts = subStr(tenderAmts);
						tenderSucPxs = subStr(tenderSucPxs);
						tenderSucAmts = subStr(tenderSucAmts);
					}
				}
				//AD	投标机构21位码
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), instIds);
				//AE	投标机构中文全称
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), names);
				//AF	投标机构应缴纳金额（元）
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), feeActAmts);
				//AG	投标价位
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), pxs);
				//AH	投标量（亿元）
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), tenderAmts);
				//AI	中标价位
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), tenderSucPxs);
				//AJ	中标量（亿元）
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), tenderSucAmts);

				//AK 币种
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), null);
				//AL 数据类型标识
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//AM 消息类型
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), transSecDefType(tradeBean.getSecDefType()));

				row++;
			} catch (Exception e2) {
				//删除错误行
				row = row - 1;
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));
				e2.printStackTrace();
				LogManager.error("写入本方招标发行结果交易出错:"+tradeBean.getExecId(), e2);

				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}
	}

	/***
	 * 同业存单 报价发行
	 * 存单发型 报价发行 本方报价发行结果
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeCD_BJTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		IbncdBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("本方报价发行结果");
		int row = e.getWb().getSheet("本方报价发行结果").getLastRowNum() + 1;
		InstitutionBean instM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (IbncdBean)trds.get(i);
			try {

				//A 币种
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B	工具类型
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), transSecType(tradeBean.getSecType()));
				//C 消息类型
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), transSecDefType(tradeBean.getSecDefType()));
				//D	工具全称
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFullName()));
				//E	工具简称
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getShortName()));
				//F 数据类型标识
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//G	工具代码
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecId()));
				//H	息票类型
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transRateType(tradeBean.getRateType()));
				//I	发行方式/招标方式
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), transIssMethod(tradeBean.getIssMethod()));
				//J	工具状态
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), transSecStatus(tradeBean.getSecStatus()));
				//K	失败原因
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getText()));
				//L	发行价格（元）
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIssPrice()));
				//M	实际发行量（亿元）
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIssSize()));
				//N	利差（BP）
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSpread()));
				//O	票面利率(%)
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCouponRate()));

				//发行人
				if(MarkerTakerEnum.Marker.equals(tradeBean.getSettInfo().getMarkerTakerRole())){
					instM = tradeBean.getInstitutionInfo();
				}else{
					instM = tradeBean.getContraPatryInstitutionInfo();
				}
				//P	发行人机构21位码
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//Q	发行人机构中文全称
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));

				String names = "";
				String instIds = "";
				String tradeNames = "";
				String feeActAmts = "";
				String subAmts = "";
				String subTimes = "";
				SubScriptionInstBean inst;
				for (int k = 0;k < tradeBean.getSubScriptions().size(); k++)
				{
					inst = tradeBean.getSubScriptions().get(k);
					names = addStr(names,nullToEmpty(inst.getFullName()), split);
					instIds = addStr(instIds,nullToEmpty(inst.getInstitutionId()), split);
					tradeNames = addStr(tradeNames,nullToEmpty(inst.getTradeName()) , split);
					feeActAmts = addStr(feeActAmts,nullToEmpty(inst.getFeeActualAmt()) , split);
					subAmts = addStr(subAmts,nullToEmpty(inst.getSubScriptionAmt()), split);
					String date = null;
					if(inst.getSubScriptionTime() != null){
						date = DateUtils.format(inst.getSubScriptionTime(), "yyyy-MM-dd");
					}
					subTimes = addStr(subTimes,nullToEmpty(date), split);

					if(k == tradeBean.getSubScriptions().size() - 1){
						names = subStr(names);
						instIds = subStr(instIds);
						tradeNames = subStr(tradeNames);
						feeActAmts = subStr(feeActAmts);
						subAmts = subStr(subAmts);
						subTimes = subStr(subTimes);
					}
				}

				//R	申购机构 21 位码
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), instIds);
				//S	申购机构中文全称
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), names);
				//T	申购机构提交用户
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), tradeNames);
				//U	申购机构应缴款金额（元）
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), feeActAmts);
				//V	申购机构认购量（亿元）
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), subAmts);
				//W	申购机构认购时间
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), subTimes);

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));
				e2.printStackTrace();
				LogManager.error("写入本方报价发行结果交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}
	}

	/***
	 * 同业存单缴费
	 * U93 存单缴款 发行人缴款信息
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeCDCommTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		IbncdCommissionBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("发行人缴款信息");
		int row = e.getWb().getSheet("发行人缴款信息").getLastRowNum() + 1;
		InstitutionBean instM = null;
		SettBaseBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (IbncdCommissionBean)trds.get(i);
			try {
				//A 币种
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), null);
				//B 缴款状态
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), transPaymentStatus(tradeBean.getPaymentStatus()));
				//C	工具类型
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), transSecType(tradeBean.getSecType()));
				//D	标题
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getHeadine()));
				//E 数据类型标识
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//F	工具全称
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFullName()));
				//G	工具简称
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getShortName()));
				//H	工具代码
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecId()));

				String instIds = "";
				String names = "";
				String acctNos = "";
				String feeActAmts = "";
				String netAmts = "";
				String qtys = "";
				SubScriptionInstBean inst;
				for (int k = 0;k < tradeBean.getSubScriptions().size(); k++)
				{
					inst = tradeBean.getSubScriptions().get(k);
					instIds = addStr(instIds,nullToEmpty(inst.getInstitutionId()), split);
					names = addStr(names,nullToEmpty(inst.getFullName()) , split);
					acctNos = addStr(acctNos,nullToEmpty(inst.getTrustAcctNo()) , split);
					feeActAmts = addStr(feeActAmts,nullToEmpty(inst.getFeeActualAmt()) , split);
					netAmts = addStr(netAmts,nullToEmpty(inst.getNetCommissionAmt()) , split);
					qtys = addStr(qtys,nullToEmpty(inst.getQty()) , split);

					if(k == tradeBean.getSubScriptions().size() - 1){
						instIds = subStr(instIds);
						names = subStr(names);
						acctNos = subStr(acctNos);
						feeActAmts = subStr(feeActAmts);
						netAmts = subStr(netAmts);
						qtys = subStr(qtys);
					}
				}

				//I	投资人机构21位码
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), instIds);
				//J	投资人中文全称
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), names);
				//K	投资人托管账号
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), acctNos);
				//L	投资人应缴款金额（元）
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), feeActAmts);
				//M	投资人实缴款金额（元）
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), netAmts);
				//N	投资人持有量（万元）
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), qtys);

				//发行人
				if(MarkerTakerEnum.Marker.equals(tradeBean.getSettInfo().getMarkerTakerRole())){
					instM = tradeBean.getInstitutionInfo();
					settM = tradeBean.getSettInfo();
				}else{
					instM = tradeBean.getContraPatryInstitutionInfo();
					settM = tradeBean.getContraPartySettInfo();
				}
				//O	发行人机构21位码
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//P	发行人机构中文全称
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//Q	发行人资金账号
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入发行人缴款信息交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getSecId(), e2.getMessage());
			}
		}
	}

	/***
	 * 同业存款结果
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeIbdTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		InterBankDepositBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("同业存款结果");
		int row = e.getWb().getSheet("同业存款结果").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SettBaseBean settT = null;
		SettBaseBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (InterBankDepositBean)trds.get(i);
			try {
				//成交编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//市场
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), null);
				//客户端参考编号
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), null);
				//交易方式
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), transDealMode(tradeBean.getDealMode()));

				//成交状态
				if(tradeBean.getStatus() == null){
					e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), String.valueOf(tradeBean.getExecType()));
				}else{
					e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), transStatus(tradeBean.getStatus()));
				}

				//成交日期
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//成交时间
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//报价编号
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), null);
				//更新日
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//应急标识
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), null);
				//交易方向
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), transCkSide(tradeBean.getPs()));
				//交易品种代码
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSecurityId()));
				//存款期限(天)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTenor()));
				//实际占款天数(天)
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getHoldingDays()));
				//存款利率(%)
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));
				//存款金额(元)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmt()));
				//付息频率
//				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), transPayFrequency(tradeBean.getPayFrequency()));


				if(tradeBean.getPayFrequency() == null){
					e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), String.valueOf(tradeBean.getCouponPaymentFrequency()));
				}else{
					e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), transPayFrequency(tradeBean.getPayFrequency()));
				}

				//首次付息日
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFirstPayDate()));
				//应计利息总额(元)
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInterestAmt()));
				//到期支取金额(元)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityAmt()));
				//首次结算日
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
				//到期支取日
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityDate()));
				//提前支取约定
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), null);
				//有无补充条款
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), null);
				//补充条款
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), null);
				//存出方经办行/产品
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getManagingEntity2()));
				//存入方经办行/产品
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getManagingEntity1()));
				//数据类型标识
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
//				//付息日
//				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIntPayDate()));
//				//应计利息(元)
//				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAccruedIntAmt()));
				//提前支取预期利率(%)
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExpYield()));

				/**
				 * 119
				 * 存入方机构
				 */
				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				/**
				 * 存入方清算信息
				 */
				settT = getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//存入方机构6位码
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//存入方联系方式
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//存入方机构简称中文
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//存入方机构全称中文
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//存入方交易员姓名
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//存入方交易员中文名称
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//存入方机构英文简称
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getSwiftCode()));
				//存入方地址
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), null);
				//存入资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//存入资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//存入资金账户-资金账号
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//存入资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));

				/**
				 * 存出方机构
				 */
				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				/**
				 * 存出方清算信息
				 */
				settM = getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());

				//存出方机构6位码
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//存出方联系方式
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//存出方机构简称中文
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//存出方机构全称中文
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//存出方交易员姓名
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//存出方交易员中文名称
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//存出方机构英文简称
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getSwiftCode()));
				//存出方地址
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), null);
				//存出资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//存出资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//存出资金账户-资金账号
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//存出资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入同业存款结果交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}

	}

	/**
	 * 同业存款附加协议
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeIbdAddTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs){
		InterBankDepositAddBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("同业存款附加协议");
		int row = e.getWb().getSheet("同业存款附加协议").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SettBaseBean settT = null;
		SettBaseBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (InterBankDepositAddBean)trds.get(i);
			try{
				//A 成交编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//B 市场
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), null);
				//C 交易方式
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), transDealMode(tradeBean.getDealMode()));
				//D 成交状态
//				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), transStatus2(tradeBean.getStatus()));

				if(tradeBean.getStatus() == null){
					e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), String.valueOf(tradeBean.getExecType()));
				}else{
					e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), transStatus2(tradeBean.getStatus()));
				}

				//E 成交日期
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//F 成交时间
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//G 附加协议类型
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transSupplType(tradeBean.getSupplType()));
				//H 操作类型
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transSupplTransTypeIbd(tradeBean.getSupplTransType()));
				//I 更新日
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 交易方向
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), transCkSide(tradeBean.getPs()));
				//K 交易品种代码
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//L 存款期限(天)
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTenor()));
				//M 实际占款天数(天)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getHoldingDays()));
				//N 存款利率(%)
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));
				//O 存款金额(元)
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmt()));
				//P 付息频率
				if(tradeBean.getPayFrequency() == null){
					e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), String.valueOf(tradeBean.getCouponPaymentFrequency()));
				}else{
					e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), transPayFrequency(tradeBean.getPayFrequency()));
				}
				//Q 首次付息日
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFirstPayDate()));
				//R 应计利息总额(元)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInterestAmt()));
				//S 到期支取金额(元)
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityAmt()));
				//T 首次结算日
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
				//U 到期支取日
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityDate()));
				//V 提前支取约定
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), null);
				//W 有无补充条款
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), null);
				//X 补充条款
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), null);
				//Y 存出方经办行/产品
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getManagingEntity2()));
				//Z 存入方经办行/产品
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getManagingEntity1()));
				//AA 数据类型标识
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
//				//AB 付息日
//				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIntPayDate()));
//				//AC 应计利息(元)
//				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAccruedIntAmt()));
				//AD 提前支取预期利率(%)
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExpYield()));

				//AE 提前支取编号
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceId()));
				//AF 提前支取状态
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), transAdvanceStatus(tradeBean.getAdvanceStatus()));
				//AG 提前支取申请业务发生时间
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanveBusinessTime()));
				//AH 提前支取日
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceDate()));
				//AI 提前支取本金(元)
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceAmt()));
				//AJ 提前支取补偿类型
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), transAdvanceCompensationType(tradeBean.getAdvanceCompensationType()));
				//AK 提前支取利率(%)
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvancePrice()));
				//AL 提前支取补偿金额(元)

				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceLastQty()).equals("0") ? "" : nullToEmpty(tradeBean.getAdvanceLastQty()));
				//AM 提前支取应计利息(元)
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceInterestAmt()));
				//AN 提前支取结算金额(元)
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceSettlAmt()).equals("0") ? "" :nullToEmpty(tradeBean.getAdvanceSettlAmt()));
				//AO 提前支取后剩余存款本金(元)
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceLeavesAmt()));
				//AP 提前支取后到期支取金额(元)
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceExpireAmt()));
				//AQ 提前支取条款
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceClause()));
				//AR 有无提前支取条款附件
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), transAdvanceAttachmentIndicator(tradeBean.getAdvanceAttachmentIndicator()));
				//AS 提前支取存出方交易员姓名
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceTraderName2()));
				//AT 提前支取存入方交易员姓名
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceTraderName1()));

				//AU 补充定期账户编号
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupId()));
				//AV 补充定期账户状态
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupStatus()));
				//AW 补充定期账户业务发生时间
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupBusinessTime()));
				//AX 补充定期账户-交易员姓名
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupTradeName()));
				//AY 补充定期账户-资金开户行名称
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupCapitalBankName()));
				//AZ 补充定期账户-资金账户户名
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupCapitalAccountName()));
				//BA 补充定期账户-资金账号
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupCapitalAccountNumber()));
				//BB 补充定期账户-支付系统行号
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupPaymentSystemCode()));

				//存入方机构
				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				//存入方清算信息
				settT = getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//BC 存入方机构6位码
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//BD 存入方机构简称中文
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//BE 存入方机构全称中文
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//BF 存入方交易员姓名
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//BG 存入方交易员中文名称
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//BH 存入资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//BI 存入资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//BJ 存入资金账户-资金账号
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//BK 存入资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));

				//存出方机构
				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				//存出方清算信息
				settM = getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//BL 存出方机构6位码
				e.writeStr(sheetIdx, row, "BL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//BM 存出方机构简称中文
				e.writeStr(sheetIdx, row, "BM", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//BN 存出方机构全称中文
				e.writeStr(sheetIdx, row, "BN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//BO 存出方交易员姓名
				e.writeStr(sheetIdx, row, "BO", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BP 存出方交易员中文名称
				e.writeStr(sheetIdx, row, "BP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BQ 存出资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "BQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//BR 存出资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "BR", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//BS 存出资金账户-资金账号
				e.writeStr(sheetIdx, row, "BS", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//BT 存出资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "BT", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));

				row++;
			}catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入同业存款附加协议交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}
	}

	/***
	 * 同业借款
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeMMlendTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		InterBankLoansBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("同业借款");
		int row = e.getWb().getSheet("同业借款").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SettBaseBean settT = null;
		SettBaseBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (InterBankLoansBean)trds.get(i);
			try {

				//A 成交编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//B 市场
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMarketIndicator()));
				//C 客户端参考编号
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), null);
				//D 交易方式
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), null);
				//E 成交状态
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), transStatus(tradeBean.getStatus()));
				//F 成交日期
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//G 成交时间
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//H 报价编号
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), null);
				//I 更新日
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 应急标识
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), null);
				//K 交易方向
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), transJkSide(tradeBean.getPs()));
				//L 交易品种代码
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//M 借款期限(天)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTenor()));
				//N 实际占款天数(天)
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getHoldingDays()));
				//O 借款利率(%)
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));
				//P 借款金额(元)
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmt()));
				//Q 付息频率
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getPayFrequency()));
				//R 首次付息日
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFirstPayDate()));
				//S 应计利息总额(元)
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInterestAmt()));
				//T 到期还款金额(元)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityAmt()));
				//U 首次结算日
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
				//V 到期还款日
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityDate()));
				//W 有无补充条款
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), null);
				//X 补充条款
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), null);
				//Y 数据类型标识
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//Z 付息日
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIntPayDate()));
				//AA 应计利息(元)
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAccruedIntAmt()));

				//119 拆入
				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				settT = getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//AB 借入方机构6位码
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AC 借入方联系方式
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AD 借入方机构简称中文
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AE 借入方机构全称中文
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AF 借入方交易员姓名
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AG 借入方交易员中文名称
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AH 借入方机构英文简称
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getSwiftCode()));
				//AI 借入方地址
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), null);
				//AJ 借入资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//AK 借入资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//AL 借入资金账户-资金账号
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//AM 借入资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));

				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				settM = getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//AN 借出方机构6位码
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//AO 借出方联系方式
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//AP 借出方机构简称中文
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//AQ 借出方机构全称中文
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//AR 借出方交易员姓名
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//AS 借出方交易员中文名称
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//AT 借出方机构英文简称
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getSwiftCode()));
				//AU 借出方地址
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), null);
				//AV 借出资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//AW 借出资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//AX 借出资金账户-资金账号
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//AY 借出资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));
				e2.printStackTrace();
				LogManager.error("写入同业借款交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}

	}

	/**
	 * 同业借款附加协议
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeMMlendAddTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		InterBankLoansAddBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("同业借款附加协议");
		int row = e.getWb().getSheet("同业借款附加协议").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;
		SettBaseBean settT = null;
		SettBaseBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (InterBankLoansAddBean)trds.get(i);
			try {
				//A 成交编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//B 市场
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMarketIndicator()));
				//C 交易方式
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealMode()));
				//D 成交状态
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getStatus()));
				//E 成交日期
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//F 成交时间
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//G 附加协议类型
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transSupplType(tradeBean.getSupplType()));
				//H 操作类型
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transSupplTransType(tradeBean.getSupplTransType()));
				//I 更新日
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), null);
				//J 交易方向
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), transJkSide(tradeBean.getPs()));
				//K 交易品种代码
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getKindId()));
				//L 借款期限(天)
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTenor()));
				//M 实际占款天数(天)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getHoldingDays()));
				//N 借款利率(%)
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getRate()));
				//O 借款金额(元)
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAmt()));
				//P 付息频率
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getPayFrequency()));
				//Q 首次付息日
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFirstPayDate()));
				//R 应计利息总额(元)
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInterestAmt()));
				//S 到期还款金额(元)
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityAmt()));
				//T 首次结算日
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
				//U 到期还款日
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityDate()));
				//V 有无补充条款
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSupplementAttachmentIndicator()));
				//W 补充条款
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getText()));
				//X 数据类型标识
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//Y 付息日
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIntPayDate()));
				//Z 应计利息(元)
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAccruedIntAmt()));

				//AA 提前还款编号
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceId()));
				//AB 提前还款状态
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceStatus()));
				//AC 提前还款申请业务发生时间
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanveBusinessTime()));
				//AD 提前还款日
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceDate()));
				//AE 提前还款本金(元)
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceAmt()));
				//AF 提前还款补偿类型
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceCompensationType()));
				//AG 提前还款利率(%)
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvancePrice()));
				//AH 提前还款补偿金额(元)
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceLastQty()));
				//AI 提前还款应计利息(元)
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceInterestAmt()));
				//AJ 提前还款结算金额(元)
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceSettlAmt()));
				//AK 提前还款后剩余借款本金(元)
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceLeavesAmt()));
				//AL 提前还款后到期还款金额(元)
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceExpireAmt()));
				//AM 提前还款条款
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceClause()));
				//AN 有无提前还款条款附件
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceAttachmentIndicator()));
				//AO 提前还款借出方交易员姓名
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceTraderName2()));
				//AP 提前还款借入方交易员姓名
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAdvanceTraderName1()));

				//119 拆入
				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				settT = getTakerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//AQ 借入方机构6位码
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				//AR 借入方机构简称中文
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//AS 借入方机构全称中文
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AT 借入方交易员姓名
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AU 借入方交易员中文名称
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//AV 借入资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//AW 借入资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//AX 借入资金账户-资金账号
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//AY 借入资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));

				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettInfo().getMarkerTakerRole());
				settM = getMarkerSettls(tradeBean.getSettInfo(), tradeBean.getContraPartySettInfo());
				//AZ 借出方机构6位码
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//BA 借出方机构简称中文
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//BB 借出方机构全称中文
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//BC 借出方交易员姓名
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BD 借出方交易员中文名称
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//BE 借出资金账户-资金账户户名
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//BF 借出资金账户-资金开户行名称
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//BG 借出资金账户-资金账号
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//BH 借出资金账户-支付系统行号
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入同业借款附加协议交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}


		}

	}

	/***
	 * 利率互换
	 * @param e
	 * @param value
	 * @param marketIndicator
	 */
	public static void writeIrsTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs) {
		SwapIrsBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("利率互换(固浮)");
		int row = e.getWb().getSheet("利率互换(固浮)").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SettBaseBean settT = null;
		SettBaseBean settM = null;

		IrsDetailBean fixLeg = null;
		IrsDetailBean floLeg = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (SwapIrsBean)trds.get(i);
			try {
				//A	成交日期
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//B 成交时间
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeTime()));
				//C 交易方式
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), null);
				//D	成交编号
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//E 报价编号
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), null);
				//F 有无补充条款
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), null);
				//G 补充条款
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), null);
				//H 交易方向
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transSid(tradeBean.getSide()));
				//I 交易品种名称
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getProductName()));
				//J 期限
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTerm()));
				//K	名义本金金额(万元)
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getNotCcyAmt()));
				//L 计息天数调整
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), transBasisRest(tradeBean.getIrsInfo().getBasisRest()));
				//M	起息日
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getStartDate()));
				//N	首期起息日
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getFirstValueDate()));
				//O	到期日
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getEndDate()));
				//P	支付日调整
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), transLegCouponPaymentDateReset(tradeBean.getIrsInfo().getPayDateReset()));
				//Q 计算机构
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), transCalculateAgency(tradeBean.getIrsInfo().getCalculateAgency()));
				//R	清算方式
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), transClearMthod(tradeBean.getClearingMethod()));
				//S 成交状态
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), null);

				if(tradeBean.getIrsDetail().getLegPriceType() == LegPriceType.FIXED_AMOUNT){
					fixLeg = tradeBean.getIrsDetail();
					floLeg = tradeBean.getContraPatryIrsDetail();
				}else{
					fixLeg = tradeBean.getContraPatryIrsDetail();
					floLeg = tradeBean.getIrsDetail();
				}
				//T	固定利率明细-固定利率(%)
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(fixLeg.getIntRate()));
				//U	固定利率明细-付息频率
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(fixLeg.getPaycycle()));
				//V	固定利率明细-首期支付日
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(fixLeg.getFirstpaydate()));
				//W	固定利率明细-计息基准
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), transLegDayCount(fixLeg.getBasis()));
				//X	浮动利率明细-参考利率
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(floLeg.getLegCurveName()));
				//Y	浮动利率明细-利差(bps)
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(floLeg.getRateDiff()));
				//Z	浮动利率明细-首期付息日
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(floLeg.getFirstpaydate()));
				//AA	浮动利率明细-支付周期
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(floLeg.getPaycycle()));
				//AB	浮动利率明细-首次利率确定日
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(floLeg.getFirstconfirmdate()));
				//AC	浮动利率明细-重置频率
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(floLeg.getRaterevcycle()));
				//AD 浮动利率明细-计息方式
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), transRateType2(floLeg.getRateType()));
				//AE	浮动利率明细-计息基准
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), transLegDayCount(floLeg.getBasis()));

				instT = getTakerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettlement().getMarkerTakerRole());
				settT = getTakerSettls(tradeBean.getSettlement(), tradeBean.getCounterPartySettlement());

				//				AF	固定利率支付方机构6位代码
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getInstitutionId()));
				// AG 固定利率支付方电话
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getContactPhone()));
				//AH  固定利率支付方地址
				e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getAddress()));
				//AI 固定利率支付方会员简称
				e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AJ	固定利率支付方会员中文全称
				e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getFullName()));
				//AK 固定利率支付方会员中文简称
				e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getShortName()));
				//				AL	固定利率支付方交易员中文名称
				e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradeName()));
				//				AM	固定利率支付方资金账户户名
				e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctName()));
				//				AN	固定利率支付方资金开户行
				e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankName()));
				//				AO	固定利率支付方资金账号
				e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getAcctNo()));
				//				AP	固定利率支付方资金开户行联行行号
				e.writeStr(sheetIdx, row, "AP", e.getDefaultLeftStrCellStyle(), nullToEmpty(settT.getBankAcctNo()));
				//AQ 固定利率支付方交易成员群组
				e.writeStr(sheetIdx, row, "AQ", e.getDefaultLeftStrCellStyle(), nullToEmpty(instT.getTradGroup()));

				instM = getMarkerInst(tradeBean.getInstitutionInfo(), tradeBean.getContraPatryInstitutionInfo(),tradeBean.getSettlement().getMarkerTakerRole());
				settM = getMarkerSettls(tradeBean.getSettlement(), tradeBean.getCounterPartySettlement());
				//				AR	固定利率收取方机构6位代码
				e.writeStr(sheetIdx, row, "AR", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getInstitutionId()));
				//AS 固定利率收取方电话
				e.writeStr(sheetIdx, row, "AS", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getContactPhone()));
				//AT 固定利率收取方地址
				e.writeStr(sheetIdx, row, "AT", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getAddress()));
				//AU 固定利率收取方会员简称
				e.writeStr(sheetIdx, row, "AU", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				AV	固定利率收取方会员中文全称
				e.writeStr(sheetIdx, row, "AV", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getFullName()));
				//AW 固定利率收取方会员中文简称
				e.writeStr(sheetIdx, row, "AW", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getShortName()));
				//				AX	固定利率收取方交易员中文名称
				e.writeStr(sheetIdx, row, "AX", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradeName()));
				//				AY	固定利率收取方资金账户户名
				e.writeStr(sheetIdx, row, "AY", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctName()));
				//				AZ	固定利率收取方资金开户行
				e.writeStr(sheetIdx, row, "AZ", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankName()));
				//				BA	固定利率收取方资金账号
				e.writeStr(sheetIdx, row, "BA", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getAcctNo()));
				//				BB	固定利率收取方资金开户行联行行号
				e.writeStr(sheetIdx, row, "BB", e.getDefaultLeftStrCellStyle(), nullToEmpty(settM.getBankAcctNo()));
				//BC 固定利率收取方交易成员群组
				e.writeStr(sheetIdx, row, "BC", e.getDefaultLeftStrCellStyle(), nullToEmpty(instM.getTradGroup()));
				//BD 客户端参考编号
				e.writeStr(sheetIdx, row, "BD", e.getDefaultLeftStrCellStyle(), null);
				//BE 交易品种代码
				e.writeStr(sheetIdx, row, "BE", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getProductCode()));
				//BF 业务发生时间
				e.writeStr(sheetIdx, row, "BF", e.getDefaultLeftStrCellStyle(), null);
				//BG 应急标识
				e.writeStr(sheetIdx, row, "BG", e.getDefaultLeftStrCellStyle(), null);
				//BH 报文动作
				e.writeStr(sheetIdx, row, "BH", e.getDefaultLeftStrCellStyle(), transDealTransType(tradeBean.getDealStatus()));
				//BI 市场标识
				e.writeStr(sheetIdx, row, "BI", e.getDefaultLeftStrCellStyle(), null);
				//BJ 数据类型标识
				e.writeStr(sheetIdx, row, "BJ", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//BK 请求报价编号
				e.writeStr(sheetIdx, row, "BK", e.getDefaultLeftStrCellStyle(), null);

				row++;
			} catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入利率互换(固浮)出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}

		}

	}

	/**
	 * 常备借贷便利
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @param errs
	 */
	public static void writeCbRepoTrader(ExcelUtil e,List<TradeBaseBean> trds, String marketIndicator,Map<String, String> errs){
		SLFBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("常备借贷便利");
		int row = e.getWb().getSheet("常备借贷便利").getLastRowNum() + 1;
		InstitutionBean instT = null;
		InstitutionBean instM = null;

		SecurSettBean settT = null;
		SecurSettBean settM = null;

		for (int i = 0; i < trds.size(); i++)
		{
			tradeBean = (SLFBean)trds.get(i);
			try{

				//A 申请编号
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//B 成交日期
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeDate()));
				//C 申请金额(元)
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeCashAmt()));
				//D 期限
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getOccupancyDays()));
				//E 利率
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getPrice()));
				//F 消息类型
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transMarketCode(tradeBean.getMarketCode()));
				//G 质押券总券面总额(元)
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getLastQty()));

				String underlyingSecurityIDs = "";
				String underlyingQtys = "";
				String underlyingStipTypes = "";
				SLFSettAmtBean settAmtBean;
				for (int k = 0;k < tradeBean.getsLFSettAmtBean().size(); k++)
				{
					settAmtBean = tradeBean.getsLFSettAmtBean().get(k);
					underlyingSecurityIDs = addStr(underlyingSecurityIDs,nullToEmpty(settAmtBean.getUnderlyingSecurityID()),split);
					underlyingQtys = addStr(underlyingQtys,nullToEmpty(settAmtBean.getUnderlyingQty()), split);
					underlyingStipTypes = addStr(underlyingStipTypes, nullToEmpty(settAmtBean.getUnderlyingStipType()), split);

					if(k == tradeBean.getsLFSettAmtBean().size() - 1){
						underlyingSecurityIDs = subStr(underlyingSecurityIDs);
						underlyingQtys = subStr(underlyingQtys);
						underlyingStipTypes = subStr(underlyingStipTypes);
					}
				}
				//H 质押券代码
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), underlyingSecurityIDs);
				//I 质押券券面总额
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), underlyingQtys);
				//J 折算率(%)
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), underlyingStipTypes);
				//K 起息日
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSettlDate()));
				//L 到期日
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSettlDate2()));
				//M 到期结算金额(元)
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSettlAmt2()));
				//N 利息
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getAccruedInterestAmt()));
				//O 数据类型标识
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), transDataCategoryIndicator(tradeBean.getDcie()));
				//P 流程状态
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getProcessingStatusString()));
				//Q 更新时间
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTransactTime()));
				//R 质押品种类
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), transCollateralType(tradeBean.getCollateralType()));
				//S 结算方式
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), transDeliveryType(tradeBean.getDeliveryType()));
				//T 分支行
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getBranchNumber()));
				//U 质押合同签署方-分支行(机构中文名称)
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getSignName()));
				//V 贷款合同签署方-分支行(机构中文名称)
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getLoansName()));
				//W 法人机构代码
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getSignNumber()));
				//X 质押合同签署方-法人(机构中文名称)
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getLegalSignName()));
				//Y 贷款合同签署方-法人(机构中文名称)
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getLegalLoansName()));
				//Z 法人资金账号
				e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getSignFundID()));
				//AA 法人托管账号
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getsLFPledegBean().getSignEscrowID()));

				row++;
			}catch (Exception e2) {
				//删除错误行
				e.getWb().getSheetAt(sheetIdx).removeRow(e.getWb().getSheetAt(sheetIdx).getRow(row));

				e2.printStackTrace();
				LogManager.error("写入常备借贷便利交易出错:"+tradeBean.getExecId(), e2);
				errs.put(tradeBean.getExecId(), e2.getMessage());
			}
		}
	}

}
