package com.singlee.financial.cfets.rmb;

import imix.field.MarketIndicator;
import imix.imix10.ExecutionReport;

import java.util.HashMap;
import java.util.Map;

public class ImixRmbMessageProcessCheck {

		
		/**
		 * 用于校验必须域：即影响下行报文解析的域
		 * @param message 报文
		 * @param map key:control控制，value："10176,5"
		 * @return 错误结果
		 */
		public Map<String,String> checkFieldAll(ExecutionReport message,Map<String,String> map){
			Map<String,String> resultMap = new HashMap<String,String>();
			resultMap.put("controlFlag", "1");//校验通过
			resultMap.put("promptFlag", "1");//校验通过
			StringBuilder sbControl = new StringBuilder("");
			StringBuilder sbprompt = new StringBuilder("");
			String marketIndicator=map.get("marketIndicator");//eg:12,14
			//控制校验
			String control=getCheckValueByMarketIndicator(marketIndicator,"Y");
			if(!"".equals(control)){
				String[] controls = control.split(",");
				for(int i=0;i<controls.length;i++){
					try {
						sbControl.append(checkField(marketIndicator,controls[i],message));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(!"".equals(sbControl.toString())){
					resultMap.put("controlFlag", "0");
				}
			}
			//提醒校验
			String prompt=getCheckValueByMarketIndicator(marketIndicator,"N");
			if(!"".equals(prompt)){
				String[] prompts = prompt.split(",");
				for(int i=0;i<prompts.length;i++){
					try {
						sbprompt.append(checkField(marketIndicator,prompts[i],message));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(!"".equals(sbprompt.toString())){
					resultMap.put("promptFlag", "0");
					sbControl.append(sbprompt.toString());
				}
			}
			resultMap.put("msg", sbControl.toString());
			return resultMap;
		}
		
		/**
		 * 通过市场类型获取校验阈值字符串
		 * @param marketIndicator
		 * @param type 提醒还是控制Y:控制，N提醒
		 * @return
		 */
		private String getCheckValueByMarketIndicator(String marketIndicator,String type){
			String result ="";
			if("Y".equals(type)){
				if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)){
					//信用拆借
					result = "10176,10105";
				}else if (marketIndicator.equals(MarketIndicator.CASH_BOND)){
					//现券买卖
					result = "10176,10105";
				} else if(marketIndicator.equals(MarketIndicator.OUTRIGHT_REPO)){
					//买断式回购
					result = "10176,10105";
				}else if (marketIndicator.equals(MarketIndicator.COLLATERAL_REPO)) {
					//质押式回购
					result = "10176,10105";
				}else if (marketIndicator.equals(MarketIndicator.INTEREST_RATE_SWAP)) {
					// 利率互换
					result = "10176,10105";
				}else if (marketIndicator.equals(MarketIndicator.SECURITY_LENDING )) {
					// 债券借贷
					result = "10176,10105";
				}
			}else{
				if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)){
					//信用拆借
//					result = "";
				}else if (marketIndicator.equals(MarketIndicator.CASH_BOND)){
					//现券买卖
//					result = "";
				} else if(marketIndicator.equals(MarketIndicator.OUTRIGHT_REPO)){
					//买断式回购
//					result = "";
				}else if (marketIndicator.equals(MarketIndicator.COLLATERAL_REPO)) {
					//质押式回购
//					result = "";
				}else if (marketIndicator.equals(MarketIndicator.INTEREST_RATE_SWAP)) {
					// 利率互换
//					result = "";
				}else if (marketIndicator.equals(MarketIndicator.SECURITY_LENDING )) {
					// 债券借贷
//					result = "";
				}
			}
			return result;
		}
		
		/**
		 * 
		 * @param marketIndicator
		 * @param text :比较内容
		 * @return
		 */
		private String checkField(String marketIndicator,String text,ExecutionReport message){
			String msg="";
			try {
				String limit="";
				int key = Integer.valueOf(text);
				switch (key) {
				case 10176:
					limit =",12,14,21,";
					msg=message.isSetMarketIndicator()?compareField(message.getMarketIndicator().getValue(),limit,"市场类型[10176]="):"市场类型为空";
					break;
				case 75:
					msg=message.isSetTradeDate()?"":"成交日期为空";
					break;
				case 10105:
					limit =",0,F,G,1,";
					msg=message.isSetDealTransType()?compareField(message.getDealTransType().getValue(),limit,"成交单状态[10105]="):"成交单状态为空";
					break;
				case 150:
					limit =",F,4,5,101,";
					msg=message.isSetExecType()?compareField(message.getExecType().getValue(),limit,"成交状态[150]="):"成交状态为空";
					break;
				case 430:
					limit =",1,2,3,4,";
					msg=message.isSetNetGrossInd()?compareField(message.getNetGrossInd().getValue(),limit,"清算方式[430]="):"清算方式为空";
					break;
				case 54:
					limit =",1,4,";
					msg=message.isSetSide()?compareField(message.getSide().getValue(),limit,"交易方向[54]="):"交易方向为空";
					break;
				case 63:
					msg=message.isSetSettlType()?"":"交易期限为空";
					break;
				case 55:
					msg=message.isSetSymbol()?"":"货币对为空";
					break;
				case 15:
					msg=message.isSetCurrency()?"":"交易货币为空";
					break;
				case 11514:
					msg=message.isSetContraCurrency()?"":"对应货币为空";
					break;
				case 10064:
					msg=message.isSetCurrency1()?"":"基准货币为空";
					break;
				case 10065:
					msg=message.isSetCurrency2()?"":"非基准货币为空";
					break;
				case 11515:
					msg=message.isSetCurrency1Amt()?"":"基准货币金额为空";
					break;
				case 11516:
					msg=message.isSetCurrency2Amt()?"":"非基准货币金额为空";
					break;
				case 11519:
					msg=message.isSetCurrency1Amt2()?"":"远端基准货币金额为空";
					break;
				case 11520:
					msg=message.isSetCurrency2Amt2()?"":"远端非基准货币金额为空";
					break;
				case 32:
					msg=message.isSetLastQty()?"":"交易货币金额为空";
					break;
				case 10284:
					msg=message.isSetRiskLastQty()?"":"交易货币折美元金额为空";
					break;
				case 1056:
					msg=message.isSetCalculatedCcyLastQty()?"":"对应货币金额为空";
					break;
				case 64:
					msg=message.isSetSettlDate()?"":"起息日为空";
					break;
				case 194:
					msg=message.isSetLastSpotRate()?"":"即期价格为空";
					break;
				case 195:
					msg=message.isSetLastForwardPoints()?"":"远端远期点为空";
					break;
				case 31:
					msg=message.isSetLastPx()?"":"远端交易价格为空";
					break;
				case 11657:
					limit =",1,2,9";
					msg=message.isSetTradingMode()?compareField(message.getTradingMode().getValue(),limit,"交易模式[11657]="):"交易模式为空";
					break;
				case 10317:
					limit =",1,3,4,8,10,";
					msg=message.isSetTradeMethod()?compareField(message.getTradeMethod().getValue(),limit,"交易方式[10317]="):"交易方式为空";
					break;
				case 10315:
					limit =",Q,O,N,1,2,6,9,";
					msg=message.isSetTradeInstrument()?compareField(message.getTradeInstrument().getValue(),limit,"交易模型[10315]="):"交易模型为空";
					break;
				case 919:
					//结算方式非必传，校验值是否正确
					limit =",12,";
					msg=message.isSetDeliveryType()?compareField(message.getDeliveryType().getValue(),limit,"结算方式[919]="):"";
					break;
				case 48:
					//外币拆借交易品种
					limit =",ON,TN,SN,1W,2W,3W,1M,2M,3M,6M,9M,1Y,BROKEN,";
					msg=message.isSetSecurityID()?compareField(message.getSecurityID().getValue(),limit,"交易品种[48]="):"交易模型为空";
					break;
				default:
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(!"".equals(msg)){
				msg=msg+";";
			}
			return msg;
		}

		private String compareField(String val,String limit,String fieldPromt){
			String msg="";
			String comVal = ","+val+",";
			if(!limit.contains(comVal)){
				msg=fieldPromt+val;
			}
			return msg;
		}
		private String compareField(char val,String limit,String fieldPromt){
			String msg="";
			String comVal = ","+val+",";
			if(!limit.contains(comVal)){
				msg=fieldPromt+val;
			}
			return msg;
		}
		private String compareField(int val,String limit,String fieldPromt){
			String msg="";
			String comVal = ","+val+",";
			if(!limit.contains(comVal)){
				msg=fieldPromt+val;
			}
			return msg;
		}
}
