package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.SwapIrsBean;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.trade.IrsDetailBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 利率互换
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbIrsEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, SwapIrsBean> {
    /**
     * 转换本币利率互换 marketIndicator=2
     *
     * @param message
     * @param swapIrsBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     * @throws ParseException
     */
    public void convert(ExecutionReport message, SwapIrsBean swapIrsBean, String bankId) throws FieldNotFound, FieldConvertError, ParseException {

        setBaseInfo(message, bankId, swapIrsBean, swapIrsBean.getSettlement(), swapIrsBean.getCounterPartySettlement());
        swapIrsBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态
        // 设置本方nolegs信息
        convert(message, bankId, SettPartyEnum.TheParty, swapIrsBean.getIrsDetail(), swapIrsBean.getSettlement().getMarkerTakerRole());
        // 设置对方nolegs信息
        convert(message, bankId, SettPartyEnum.CounterParty, swapIrsBean.getContraPatryIrsDetail(), swapIrsBean.getSettlement().getMarkerTakerRole());
        // 交易模式
        swapIrsBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        swapIrsBean.setSide(message.isSetSide() ? message.getSide().getUnionTypeValue() : CfetsUtils.EMPTY);
        // 根据固定方判断方向
        swapIrsBean.setPs(convert(swapIrsBean.getIrsDetail().getLegSide())); // 重新设置买卖方向
        swapIrsBean.setText(message.isSetText() ? message.getText().getValue() : null);
        swapIrsBean.setTerm(message.isSetTradeLimitDays() ? message.getTradeLimitDays().getValue() : CfetsUtils.EMPTY);//期限
        swapIrsBean.setClearingMethod(message.isSetClearingMethod() ? message.getClearingMethod().getUnionTypeValue() : null);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        swapIrsBean.getIrsInfo().setProductName(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY); // 交易名称
        swapIrsBean.getIrsInfo().setNotCcyAmt(new BigDecimal(message.isSetLastQty() ? message.getLastQty().getValue() : CfetsUtils.EMPTY)); // 名义本金
        swapIrsBean.getIrsInfo().setStartDate(df.parse(message.isSetStartDate() ? message.getStartDate().getValue() : null));
        swapIrsBean.getIrsInfo().setFirstValueDate(message.isSetFirstPeriodStartDate() ? message.getFirstPeriodStartDate().getDateValue() : null); // 首次起息日
        swapIrsBean.getIrsInfo().setEndDate(df.parse(message.isSetEndDate() ? message.getEndDate().getValue() : null)); // 到期日
        swapIrsBean.getIrsInfo().setBasisRest(message.isSetInterestAccuralDaysAdjustment() ? message.getInterestAccuralDaysAdjustment().getUnionTypeValue() : CfetsUtils.EMPTY);
        swapIrsBean.getIrsInfo().setPayDateReset(message.isSetCouponPaymentDateReset() ? message.getCouponPaymentDateReset().getUnionTypeValue() : CfetsUtils.EMPTY); // 支付日调整
        swapIrsBean.getIrsInfo().setSettType(message.isSetClearingMethod() ? message.getClearingMethod().getUnionTypeValue() : CfetsUtils.EMPTY); // 清算方式
        swapIrsBean.getIrsInfo().setCalculateAgency(message.isSetCalculateAgency() ? message.getCalculateAgency().getUnionTypeValue() : CfetsUtils.EMPTY); // 计算机构
    }

    /**
     * 本币利率互换 nolegs处理
     *
     * @param message
     * @param marketTaker
     * @param irsDetail
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, String bankId, SettPartyEnum marketTaker, IrsDetailBean irsDetail, MarkerTakerEnum markerTaker)
            throws FieldNotFound, FieldConvertError {
        for (int i = 1; i <= message.getParties().getNoPartyIDs().getValue(); i++) {
            // 实例做市商/受市商清算信息列表对象
            ExecutionReport.NoPartyIDs noPartyIDs = new ExecutionReport.NoPartyIDs();
            // 获取做市商/受市商清算信息列表数据
            message.getGroup(i, noPartyIDs);
            // 获取机构编号
            String partyID = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : CfetsUtils.EMPTY;
            // 如果不是当前要取的信息,则继续读取下条记录
            if (!(partyID.equalsIgnoreCase(bankId) ? SettPartyEnum.TheParty : SettPartyEnum.CounterParty)
                    .equals(marketTaker)) {
                continue;
            }
            ExecutionReport.NoLegs nolegs = new ExecutionReport.NoLegs();
            message.getGroup(i, nolegs);
            // 3-固定值
            int legPriceType = nolegs.isSetLegPriceType() ? nolegs.getLegPriceType().getValue() : 0;
            irsDetail.setLegPriceType(legPriceType);
            // 1-固定利率支付 B-基准端
            irsDetail.setLegSide(nolegs.isSetLegSide() ? nolegs.getLegSide().getUnionTypeValue() : CfetsUtils.EMPTY);
            if (MarkerTakerEnum.Taker.equals(markerTaker)) {//119 参考利率1支付方
                if ("1".equals(irsDetail.getLegSide())) { //LEG1 支付方
                    irsDetail.setLegPs("S");
                } else if ("B".equals(irsDetail.getLegSide())) { //LEG2 收取方
                    irsDetail.setLegPs("P");
                }
            } else {   //120-参考利率2支付方
                if ("4".equals(irsDetail.getLegSide())) { //LEG1 收取方
                    irsDetail.setLegPs("P");
                } else if ("B".equals(irsDetail.getLegSide())) { //LEG2 支付方
                    irsDetail.setLegPs("S");
                }
            }
            if (legPriceType == 6) {
                // 利率明细为浮动利率时
                irsDetail.setLegCurveName(nolegs.isSetLegBenchmarkCurveName() ? nolegs.getLegBenchmarkCurveName().getValue() : CfetsUtils.EMPTY);
                // 利差
                irsDetail.setRateDiff(nolegs.isSetLegBenchmarkSpread() ? nolegs.getLegBenchmarkSpread().getDoubleValue() : null);
                // 首期支付日
                irsDetail.setFirstpaydate(nolegs.isSetLegCouponPaymentDate() ? nolegs.getLegCouponPaymentDate().getDateValue() : null);
                // 付息频率
                irsDetail.setPaycycle(nolegs.isSetLegCouponPaymentFrequency() ? nolegs.getLegCouponPaymentFrequency().getValue() : CfetsUtils.EMPTY);
                // 首期利率确定日
                irsDetail.setFirstconfirmdate(nolegs.isSetLegInterestAccrualDate() ? nolegs.getLegInterestAccrualDate().getDateValue() : null);
                // 重置频率
                irsDetail.setRaterevcycle(nolegs.isSetLegInterestAccrualResetFrequency() ? nolegs.getLegInterestAccrualResetFrequency().getValue() : CfetsUtils.EMPTY);
                // 计息方式
                irsDetail.setRateType(nolegs.isSetLegInterestAccrualMethod() ? nolegs.getLegInterestAccrualMethod().getUnionTypeValue() : CfetsUtils.EMPTY);
                // 计息基准
                irsDetail.setBasis(nolegs.isSetLegDayCount() ? nolegs.getLegDayCount().getUnionTypeValue() : CfetsUtils.EMPTY);
            } else {
                // 利率明细为固定利率时
                irsDetail.setIntRate(nolegs.isSetLegPrice() ? new BigDecimal(nolegs.getLegPrice().getValue()) : null);
                // 付息频率
                irsDetail.setPaycycle(nolegs.isSetLegCouponPaymentFrequency() ? nolegs.getLegCouponPaymentFrequency().getValue() : CfetsUtils.EMPTY);
                // 首期支付日
                irsDetail.setFirstpaydate(nolegs.isSetLegCouponPaymentDate() ? nolegs.getLegCouponPaymentDate().getDateValue() : null);
                // 计息基准
                irsDetail.setBasis(nolegs.isSetLegDayCount() ? nolegs.getLegDayCount().getUnionTypeValue() : CfetsUtils.EMPTY);
            }
        }
    }
}
