package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.IbncdCommissionBean;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SubScriptionInstBean;
import imix.field.NestedPartySubIDType;
import imix.field.SendingTime;
import imix.imix10.CommissionData;
import imix.imix10.ExecutionReport;
import imix.imix10.component.CommissionDetailGrp;

import java.math.BigDecimal;

/**
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbInstrumentsPaymentEntry extends RmbSettleEntry implements ImixRmbMessageHandler<CommissionData, IbncdCommissionBean> {
    /****
     *
     * 解析存单发行缴款人信息
     * @param message
     * @param ibBean
     * @throws Exception
     */
    public void convert(CommissionData message, IbncdCommissionBean ibBean, String instId) throws Exception {
        ibBean.setExecId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);
        ibBean.setCcy(message.isSetCurrency() ? message.getCurrency().getValue() : CfetsUtils.EMPTY);// 交易币种
        ibBean.setPaymentStatus(message.isSetPaymentStatus() ? Integer.parseInt(String.valueOf(message.getPaymentStatus().getValue())) : 0);//缴款状态 1-缴款成功 0-缴款失败
        ibBean.setSecType(message.isSetSecurityType() ? message.getSecurityType().getValue() : CfetsUtils.EMPTY);// 工具类型 CD-同业存单
        ibBean.setFullName(message.isSetFullSymbol() ? message.getFullSymbol().getValue() : CfetsUtils.EMPTY); // 工具全称
        ibBean.setShortName(message.isSetSymbol() ? message.getSymbol().getValue() : CfetsUtils.EMPTY);// 工具简称
        ibBean.setSecId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 工具代码
        ibBean.setDataCategoryIndicator(message.isSetDataCategoryIndicator() ? String.valueOf(message.getDataCategoryIndicator().getValue()) : CfetsUtils.EMPTY); // 数据类型标识
        ibBean.setDcie(convert(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator() : null)); //数据类型标识 Excel导出使用
        ibBean.setDealDate(message.getHeader().getUtcTimeStampValue(SendingTime.FIELD));
        /****************************缴款信息******************************/
        SubScriptionInstBean subScriptionInstBean = null;
        SecurSettBean securSettBean = null;
        imix.imix10.component.CommissionGrp.NoCommissions commissionGrp = null;
        imix.imix10.component.CommissionGrp.NoCommissions.NoCommissionDetails commissionDetails = null;
        int noCommiss = message.isSetNoCommissions() ? message.getNoCommissions().getValue() : 0;
        for (int i = 1; i <= noCommiss; i++) {
            commissionGrp = new imix.imix10.component.CommissionGrp.NoCommissions();
            message.getGroup(i, commissionGrp);
            int commType = commissionGrp.isSetCommType() ? commissionGrp.getCommType().getValue() : 0;
            if (commType != 7) {//发行人交款明细
                continue;
            }
            //循环解析缴款人信息
            int num = commissionGrp.isSetNoCommissionDetails() ? commissionGrp.getNoCommissionDetails().getValue() : 0;
            for (int j = 1; j <= num; j++) {
                commissionDetails = new imix.imix10.component.CommissionGrp.NoCommissions.NoCommissionDetails();
                commissionGrp.getGroup(j, commissionDetails);

                subScriptionInstBean = new SubScriptionInstBean();
                //应缴纳金额
                subScriptionInstBean.setFeeActualAmt(commissionDetails.isSetCommissionAmt() ? new BigDecimal(commissionDetails.getCommissionAmt().getValue()) : null);
                //实际缴纳金额
                subScriptionInstBean.setNetCommissionAmt(commissionDetails.isSetNetCommissionAmt() ? new BigDecimal(commissionDetails.getNetCommissionAmt().getValue()) : null);
                //持有量 万元
                subScriptionInstBean.setQty(commissionDetails.isSetQuantity() ? new BigDecimal(commissionDetails.getQuantity().getValue()) : null);

                CommissionDetailGrp.NoCommissionDetails.NoNestedPartyIDs nestedPartyIDs = null;
                securSettBean = new SecurSettBean();
                String partyId = null;
                int noNestPtIDs = commissionDetails.isSetNoNestedPartyIDs() ? commissionDetails.getNoNestedPartyIDs().getValue() : 0;
                for (int k = 1; k <= noNestPtIDs; k++) {
                    nestedPartyIDs = new CommissionDetailGrp.NoCommissionDetails.NoNestedPartyIDs();
                    commissionDetails.getGroup(k, nestedPartyIDs);
                    partyId = nestedPartyIDs.isSetNestedPartyID() ? nestedPartyIDs.getNestedPartyID().getValue() : CfetsUtils.EMPTY;

                    subScriptionInstBean.setInstitutionId(partyId);
                    securSettBean.setMarkerTakerRole(MarkerTakerEnum.Taker); //投资人信息

                    convert(nestedPartyIDs, subScriptionInstBean);

                    if (partyId.equals(instId)) {//是本方
                        ibBean.setInstitutionInfo(subScriptionInstBean);
                        ibBean.setSettInfo(securSettBean);
                    }
                }

                ibBean.getSubScriptions().add(subScriptionInstBean);
            }// end for
        }// 缴款信息结束

        /****************************发行人信息******************************/
        ExecutionReport.NoPartyIDs noPartyIDs = new ExecutionReport.NoPartyIDs();
        message.getGroup(1, noPartyIDs);
        // 获取机构编号
        String partyID = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : CfetsUtils.EMPTY;
        // 获取做市商/受市商角色
        int partyRole = noPartyIDs.isSetPartyRole() ? noPartyIDs.getPartyRole().getValue() : 0;
        logger.debug("partyID:" + partyID + ",partyRole:" + partyRole);
        InstitutionBean institution = new InstitutionBean();
        institution.setInstitutionId(partyID);// 交易对手ID/交易机构ID

        SecurSettBean settBean = new SecurSettBean();
        settBean.setMarkerTakerRole(MarkerTakerEnum.Marker); //发行人信息
        convert(noPartyIDs, institution, settBean);

        if (partyID.equals(instId)) {//是本方
            ibBean.setInstitutionInfo(institution);
            ibBean.setSettInfo(settBean);
        } else {
            ibBean.setContraPatryInstitutionInfo(institution);
            ibBean.setContraPartySettInfo(settBean);
        }
    }

    /**
     * 缴款人信息
     * NoPartyDetailAltIDs 处理公共处理
     *
     * @param nestedPartyIDs
     * @param institution
     * @throws Exception
     */
    public void convert(CommissionDetailGrp.NoCommissionDetails.NoNestedPartyIDs nestedPartyIDs, SubScriptionInstBean institution) throws Exception {
        int n = nestedPartyIDs.isSetNoNestedPartySubIDs() ? nestedPartyIDs.getNoNestedPartySubIDs().getValue() : 0;
        CommissionDetailGrp.NoCommissionDetails.NoNestedPartyIDs.NoNestedPartySubIDs nestedPartySubIDs = null;
        for (int i = 1; i <= n; i++) {
            nestedPartySubIDs = new CommissionDetailGrp.NoCommissionDetails.NoNestedPartyIDs.NoNestedPartySubIDs();
            nestedPartyIDs.getGroup(i, nestedPartySubIDs);
            // 获取清算信息类型
            int partySubIdType = nestedPartySubIDs.isSetNestedPartySubIDType() ? nestedPartySubIDs.getNestedPartySubIDType().getValue() : 0;
            // 获取清算信息类型对应的值
            String partySubIdValue = nestedPartySubIDs.isSetNestedPartySubID() ? nestedPartySubIDs.getNestedPartySubID().getValue() : CfetsUtils.EMPTY;
            logger.info("缴款人信息开始处理  partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
            switch (partySubIdType) {
                case NestedPartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM://机构中文全称
                    institution.setFullName(partySubIdValue);
                    break;
                case NestedPartySubIDType.SECURITIES_ACCOUNT_NUMBER://托管账号
                    institution.setTrustAcctNo(partySubIdValue);
                    break;
                default:
                    break;
            }
        }
    }
}
