package com.singlee.financial.cfets.Util;
/**
 * 
 * @author 代号_47
 *
 */
public enum MarketIndicatorEnum {
	
	
	CreditLending("信用拆借","1"),
	
	InterestRateSwap("利率互换","2"),
	
	ForwardInterestRate("远期利率协议","3"),
	
	CashToBuy("现券买卖","4"),
	
	BondForward("债券远期","5"),
	
	SecurityLending("债券借贷","8"),
	
	PledgedRepurchase("质押式回购","9"),
	
	BuyoutRepos("买断式回购","10"),
	
	CRMW("信用缓释凭证市场","18"),
	
	whenIssuedTrading("预发行市场","31"),
	
	InterbankBorrowing("同业借款","41"),
	
	StandardBondForward("标准债券远期","43"),
	
	InterestRateSwaption("利率互换期权市场","44"),
	
	RateUpperLimitAndLowerLimit("利率上限/下限期权市场","45"),
	
	CDS("信用违约互换","48"),
	
	InterbankDeposits("同业存款","53"),
	
	CD("同业存单发行","d"),
	
	COMM("同业存单缴款","U93");
	
	MarketIndicatorEnum(String name,String value){
        this.name = name;
        this.value = value;
    }
	
	private String name;
	
	private String value;
	/**
	 * 根据value值获取名字
	 * @param index
	 * @return
	 */
	public static String getName(String index){
        for(MarketIndicatorEnum market :MarketIndicatorEnum.values()){
            if(market.getValue().equals(index)){
                return market.name;
            }
        }
        return  "";
    }
	/**
	 * 根据名字获取value值
	 * @param name
	 * @return
	 */
	public static String getIndexByName(String name){
        for(MarketIndicatorEnum market :MarketIndicatorEnum.values()){
            if(name.equals(market.getName())){
                return market.getValue();
            }
        }
        return "";
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String enumName(){
		return this.toString();
	}
	
	

}
