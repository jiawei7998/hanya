package com.singlee.financial.cfets.translate;


import com.singlee.financial.pojo.component.*;
import imix.RuntimeError;
import imix.field.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 公共方法转换处理
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbBaseEntry {
    Logger logger = LoggerFactory.getLogger(RmbBaseEntry.class);

    /**
     * 方法已重载.转换买卖方向
     *
     * @param side        Cfets买卖方向
     * @param markerTaker 发起方/接收方标识
     * @return PsEnum
     */
    PsEnum convert(Side side, MarkerTakerEnum markerTaker) {
        if (null == side) {
            return null;
        }
        String sideValue = side.getUnionTypeValue();
        if ("1".equals(sideValue) || "J".equals(sideValue)) {
            return PsEnum.P;
        } else if ("4".equals(sideValue)) {
            return PsEnum.S;
        } else {
            logger.error("Cfets Side Convert Error!", new RuntimeError("Side[ " + side + " ] is invaild"));
            return null;
        }
    }

    /**
     * 方向转换
     *
     * @param legSide
     * @return
     */
    PsEnum convert(String legSide) {
        if (null == legSide || "".equals(legSide)) {
            return null;
        }
        if ("1".equals(legSide) || "B".equals(legSide)) {
            return PsEnum.P;
        } else if ("4".equals(legSide)) {
            return PsEnum.S;
        } else {
            logger.error("Swap Side Convert Error!", new RuntimeError("Side[ " + legSide + " ] is invaild"));
            return null;
        }
    }

    /**
     * 交易状态转换
     *
     * @param execType
     * @return
     */
    ExecStatus convert(ExecType execType) {
        if (null == execType) {
            return null;
        }
        if ("F".equals(execType.getValue())) {
            return ExecStatus.Finish;
        } else if ("4".equals(execType.getValue())) {
            return ExecStatus.Revoke;
        } else if ("5".equals(execType.getValue())) {
            return ExecStatus.Modify;
        } else if ("101".equals(execType.getValue())) {
            return ExecStatus.Emergency;
        }
        return null;
    }

    /**
     * 数据来源转换
     *
     * @param dci
     * @return
     */
    DataCategoryIndicatorEnum convert(DataCategoryIndicator dci) {
        if (null == dci) {
            return null;
        }
        if (0 == dci.getValue()) {
            return DataCategoryIndicatorEnum.THIS;
        } else if (2 == dci.getValue()) {
            return DataCategoryIndicatorEnum.AI;
        } else if (5 == dci.getValue()) {
            return DataCategoryIndicatorEnum.BAI;
        }
        return null;
    }

    /**
     * 方法已重载.转换交易状态信息
     *
     * @param dealTransType Cfets交易状态
     * @return
     */
    TransactionStatusEnum convert(DealTransType dealTransType) {
        TransactionStatusEnum transactionStatus = null;
        if (null == dealTransType) {
            return null;
        }
        switch (dealTransType.getValue()) {
            case (char) 48:
                // 0-录入
                transactionStatus = TransactionStatusEnum.Entry;
                break;
            case (char) 49:
                // 1-修改
                transactionStatus = TransactionStatusEnum.Modify;
                break;
            case (char) 50:
                // 2-撤销
                transactionStatus = TransactionStatusEnum.Back;
                break;
            case (char) 70:
                // F-应急录入
                transactionStatus = TransactionStatusEnum.EmergencyEntry;
                break;
            case (char) 71:
                // G-应急删除
                transactionStatus = TransactionStatusEnum.EmergencyDelete;
                break;
            default:
                break;
        }
        return transactionStatus;
    }

    /**
     * 方法已重载.转换交易模式信息
     *
     * @param tradingMode Cfets交易模式
     * @return
     */
    DealModeEnum convert(TradeMethod tradingMode) {
        DealModeEnum dealMode = null;
        if (null == tradingMode) {
            return null;
        }
        switch (tradingMode.getValue()) {
            case 1:
                dealMode = DealModeEnum.Enquiry;
                break;
            case 2:
                dealMode = DealModeEnum.Competition;
                break;
            case 9:
                dealMode = DealModeEnum.Married;
                break;
            default:
                break;
        }
        return dealMode;
    }

    /**
     * 回购方式
     *
     * @param repoMethod
     * @return
     */
    RepoMethodEnum convert(RepoMethod repoMethod) {
        RepoMethodEnum repoMethodEnum = null;
        if (null == repoMethod) {
            return null;
        }
        switch (repoMethod.getValue()) {
            case '1':
                repoMethodEnum = RepoMethodEnum.Enquiry;
                break;
            case '2':
                repoMethodEnum = RepoMethodEnum.Competition;
                break;
            case '3':
                repoMethodEnum = RepoMethodEnum.Married;
                break;
            default:
                break;
        }
        return repoMethodEnum;
    }

    /**
     * 方法已重载.转换交易模式信息
     *
     * @param cFrequency
     * @return
     */
    PayFrequency convert(CouponPaymentFrequency cFrequency) {
        PayFrequency fre = null;
        if (null == cFrequency) {
            return null;
        }
        switch (cFrequency.getValue()) {
            case 2://到期一次
                fre = PayFrequency.ONCE;
                break;
            case 3://按月
                fre = PayFrequency.M;
                break;
            case 4://按季
                fre = PayFrequency.Q;
                break;
            default:
                break;
        }
        return fre;
    }
}
