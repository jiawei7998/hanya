package com.singlee.financial.cfets.Util;

import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ContextLoaderListener;

public class PathUtils extends ContextLoaderListener{
	private static String webRootPath = null;
	
	public PathUtils(){}
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		webRootPath = arg0.getServletContext().getRealPath("");
		
		System.out.println("================>webRootPath : " + webRootPath);
    }
	
	public static String getWebRootPath()
	{
		return webRootPath;
	}
	
	public static String getAbsPath(String path)
	{
		return getWebRootPath() + "/" + path;
	}
	
	
}
