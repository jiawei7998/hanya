package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.IbncdBean;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SubScriptionInstBean;
import imix.field.PartyDetailAltRole;
import imix.field.SecurityDefinitionType;
import imix.imix10.SecurityDefinition;
import imix.imix10.component.PartyDetailAlt;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 工具发行,目前仅包含存单
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbInstrumentsIssueEntry extends RmbSettleEntry implements ImixRmbMessageHandler<SecurityDefinition, IbncdBean> {
    //存单发行
    public void convert(SecurityDefinition message, IbncdBean ibBean, String bankId) throws Exception {
        ibBean.setDealDate(message.getHeader().isSetSendingTime() ? message.getHeader().getSendingTime().getDateValue() : new Date());//成交日期
        ibBean.setExecId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);
        ibBean.setCcy(message.isSetCurrency() ? message.getCurrency().getValue() : CfetsUtils.EMPTY);// 交易币种
        ibBean.setSecType(message.isSetSecurityType() ? message.getSecurityType().getValue() : CfetsUtils.EMPTY);// 工具类型 CD-同业存单
        ibBean.setFullName(message.isSetFullSymbol() ? message.getFullSymbol().getValue() : CfetsUtils.EMPTY); // 工具全称
        ibBean.setShortName(message.isSetSymbol() ? message.getSymbol().getValue() : CfetsUtils.EMPTY);// 工具简称
        ibBean.setSecId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 工具代码
        ibBean.setRateType(message.isSetCouponRateType() ? String.valueOf(message.getCouponRateType().getValue()) : null); // 息票类型
        /***
         * 招标方式
         招标发行:0-单一价格招标发行 5-数量招标发行
         报价发行:6-报价发行
         */
        ibBean.setIssMethod(message.isSetIssueMethod() ? message.getIssueMethod().getValue() : CfetsUtils.EMPTY);// 发行方式、招标方式
        ibBean.setSecStatus(message.isSetSecurityStatus() ? message.getSecurityStatus().getUnionTypeValue() : null);// 工具状态
        ibBean.setText(message.isSetText() ? message.getText().getValue() : null);
        ibBean.setIssPrice(message.isSetIssuePx() ? new BigDecimal(message.getIssuePx().getValue()) : null);//发型价格
        ibBean.setIssSizeForPlan(message.isSetIssueSizeForPlan() ? new BigDecimal(message.getIssueSizeForPlan().getValue()) : null);
        ibBean.setIssSize(message.isSetIssueSize() ? new BigDecimal(message.getIssueSize().getValue()) : null);//实际发型量
        ibBean.setSpread(message.isSetSpread() ? new BigDecimal(message.getSpread().getValue()) : null);//利差
        ibBean.setCouponRate(message.isSetCouponRate() ? new BigDecimal(message.getCouponRate().getValue()) : null);//票面利率
        ibBean.setDataCategoryIndicator(message.isSetDataCategoryIndicator() ? String.valueOf(message.getDataCategoryIndicator().getValue()) : CfetsUtils.EMPTY); // 数据类型标识
        ibBean.setDcie(convert(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator() : null)); //数据类型标识 Excel导出使用
        //2 发行人接收  3 投资人接收
        ibBean.setSecDefType(message.isSetSecurityDefinitionType() ? message.getSecurityDefinitionType().getUnionTypeValue() : "");
        /***
         * 招标标的
         仅发行方式为“单一价格招标发行”时
         传输；
         1-价格
         2-利率
         3-利差
         */
        ibBean.setTenderSubject(message.isSetTenderSubject() ? message.getTenderSubject().getValue() : null);
        //投标总量（亿元）
        ibBean.setTotalTenderAmt(message.isSetTotalTenderAmt() ? new BigDecimal(message.getTotalTenderAmt().getValue()) : null);
        //中标总量（亿元）
        ibBean.setTotalTenderSuccessAmt(message.isSetTotalTenderSuccessAmt() ? new BigDecimal(message.getTotalTenderSuccessAmt().getValue()) : null);
        //投标倍数
        ibBean.setTenderMultiples(message.isSetTenderMultiples() ? new BigDecimal(message.getTenderMultiples().getValue()) : null);
        //TenderParties
        ibBean.setTenderParties(message.isSetTenderParties() ? message.getTenderParties().getValue() : 0);
        //中标家数
        ibBean.setTenderSuccessParties(message.isSetTenderSuccessParties() ? message.getTenderSuccessParties().getValue() : 0);
        //投标笔数
        ibBean.setTenderNumber(message.isSetTenderNumber() ? message.getTenderNumber().getValue() : 0);
        //中标笔数
        ibBean.setTenderSuccessNumber(message.isSetTenderSuccessNumber() ? message.getTenderSuccessNumber().getValue() : 0);

        SubScriptionInstBean institutionBean = null;
        SecurSettBean settBean = null;
        PartyDetailAlt.NoPartyDetailAltIDs noPartyDetailAltIDs = null;

        int noPartyIDs = message.isSetNoPartyDetailAltIDs() ? message.getNoPartyDetailAltIDs().getValue() : 0;
        for (int i = 1; i <= noPartyIDs; i++) {
            // 解析PartyDetailAlt重复组
            noPartyDetailAltIDs = new PartyDetailAlt.NoPartyDetailAltIDs();
            message.getGroup(i, noPartyDetailAltIDs);
            int partyDetailAltRole = noPartyDetailAltIDs.isSetPartyDetailAltRole() ? noPartyDetailAltIDs.getPartyDetailAltRole().getValue() : 0;
            String partyID = noPartyDetailAltIDs.isSetPartyDetailAltID() ? noPartyDetailAltIDs.getPartyDetailAltID().getValue() : CfetsUtils.EMPTY;

            institutionBean = new SubScriptionInstBean();
            institutionBean.setInstitutionId(partyID);
            //解析认购人
            convert(noPartyDetailAltIDs, institutionBean);

            settBean = new SecurSettBean();
            //发行人
            if (PartyDetailAltRole.BUYER_OR_BORROWER_OR_REVERSE_REPOER_OR_FIXED_PAYER_OR_BENCHMARK_1_PAYER == partyDetailAltRole) {
                settBean.setMarkerTakerRole(MarkerTakerEnum.Marker);
            } else if (PartyDetailAltRole.SELLER_OR_LENDER_OR_REPOER_OR_FIXED_RECEIVER_OR_BENCHMARK_1_RECEIVER == partyDetailAltRole) {
                //认购人
                settBean.setMarkerTakerRole(MarkerTakerEnum.Taker);
                //增加认购人
                ibBean.getSubScriptions().add(institutionBean);
            }
            //发行人接收 本方为发行人
            if (String.valueOf(SecurityDefinitionType.TENDERRESULTMSG).equals(ibBean.getSecDefType())) {
                // 119 发行人 120 投资人
                if (partyDetailAltRole == PartyDetailAltRole.BUYER_OR_BORROWER_OR_REVERSE_REPOER_OR_FIXED_PAYER_OR_BENCHMARK_1_PAYER) {
                    ibBean.setInstitutionInfo(institutionBean);
                    ibBean.setSettInfo(settBean);
                } else {
                    ibBean.setContraPatryInstitutionInfo(institutionBean);
                    ibBean.setContraPartySettInfo(settBean);
                }
            } else if (String.valueOf(SecurityDefinitionType.SUBSCRIPTIONMSG).equals(ibBean.getSecDefType())) {//投资人接收 本方为投资人
                // 119 发行人 120 投资人
                if (partyDetailAltRole == PartyDetailAltRole.SELLER_OR_LENDER_OR_REPOER_OR_FIXED_RECEIVER_OR_BENCHMARK_1_RECEIVER) {
                    ibBean.setInstitutionInfo(institutionBean);
                    ibBean.setSettInfo(settBean);
                } else {
                    ibBean.setContraPatryInstitutionInfo(institutionBean);
                    ibBean.setContraPartySettInfo(settBean);
                }
            } else {//方向错误
                throw new Exception("SecurityDefinitionType字段错误,值为[" + ibBean.getSecDefType() + "]无法解析本方和对手方信息");
            }
        }// end for
    }
}
