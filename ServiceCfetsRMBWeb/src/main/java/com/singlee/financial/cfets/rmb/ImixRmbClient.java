package com.singlee.financial.cfets.rmb;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.util.CfetsUtils;
import imix.ConfigError;
import imix.DataDictionary;
import imix.Message;
import imix.client.core.ImixApplication;
import imix.client.core.ImixSession;
import imix.client.core.ImixSessionExistingException;
import imix.field.ExecAckStatus;
import imix.field.SecurityID;
import imix.imix10.ExecutionAcknowledgement;
import imix.imix10.ExecutionReport;
import imix.imix10.SecurityDefinitionAck;
import imix.protocol.version.Version;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * CFETS本币监听管理类
 *
 * @author chenxh
 */
public class ImixRmbClient {

    // CFETS 配置类
    private ConfigBean configBean;
    // CFETS 监听事件类
    private ImixRmbClientListener listener;
    //当前申请
    private ImixSession session;

    private static Logger LogManager = Logger.getLogger(ImixRmbClient.class);

    /**
     * 初始化CFETS服务,登录CFETS服务器
     *
     * @return
     * @throws Exception
     */
    public boolean initLogin() throws Exception {
        // 是否登录
        boolean isLogin = false;
        try {
            LogManager.debug("configBean:" + configBean);
            //获取配置文件
            String configFilePath = ImixRmbClient.class.getResource("/").getFile();
            LogManager.debug("init configfile:" + configFilePath);
            //更改相对路径为绝对路径
            configFilePath = CfetsUtils.updateConfigFile(configBean.getUpdatePathKey(), configFilePath, ConfigBean.cfgClientPath, configBean.getCharset());
            LogManager.debug("update configfile:" + configFilePath);
            //初始化应用
            ImixApplication.initialize(listener, configFilePath);
            //启动session
            isLogin = this.sessionHandle(configBean, true);
            //输出协议包版本
            LogManager.info("***************IMIXProtocol信息****************************");
            LogManager.info("The Version of IMIX Protocol is :" + Version.getVersion());
            LogManager.info("Building Time is :" + Version.getBuildingTime());
            LogManager.info("*********************************************************");
            // 本地测试
            // testMessage();
        } catch (Exception e) {
            LogManager.error("CFETS RMB CSTP LOGIN Exception:[" + e.getMessage() + "]", e);
        }
        return isLogin;
    }

    /**
     * 方法已重载.发送信息到CFETS服务器
     *
     * @param execID
     * @param execAckStatus
     * @return
     */
    public static boolean sendMessage(ExecutionReport executionReport, String execID, int execAckStatus) {
        return sendMessage(executionReport, generateMsg(execID, null, execAckStatus));
    }

    /**
     * 方法已重载.发送信息到CFETS服务器
     *
     * @param message
     * @return
     */
    public static boolean sendMessage(ExecutionReport executionReport, Message message) {
        return ImixSession.lookupIMIXSession(executionReport).send(message);
    }

    /**
     * 方法已重载.发送信息到CFETS服务器
     *
     * @param execID
     * @param execAckStatus
     * @return
     */
    public static boolean sendMessage(imix.imix10.Message msg, String execID, int execAckStatus) {
        if (msg instanceof ExecutionReport) {
            return sendMessage(msg, generateMsg(execID, null, execAckStatus));
        } else {
            return sendMessage(msg, generateSecurityMsg(execID, null, execAckStatus));
        }
    }

    /**
     * 方法已重载.发送信息到CFETS服务器
     *
     * @param message
     * @return
     */
    public static boolean sendMessage(imix.imix10.Message msg, Message message) {
        return ImixSession.lookupIMIXSession(msg).send(message);
    }

    /**
     * 创建CFETS消息类
     *
     * @param execID
     * @param messageEncoding
     * @param execAckStatus
     * @return
     */
    private static Message generateMsg(String execID, String messageEncoding, int execAckStatus) {
        ExecutionAcknowledgement msg = new ExecutionAcknowledgement();
        msg.setString(17, execID);
        if (messageEncoding == null) {
            messageEncoding = "UTF-8";
        }
        msg.getHeader().setString(347, messageEncoding);
        msg.setChar(1036, (char) (execAckStatus + 49));
        return msg;
    }

    /**
     * 创建CFETS消息类
     *
     * @param securityID
     * @param messageEncoding
     * @param execAckStatus
     * @return
     */
    private static Message generateSecurityMsg(String securityID, String messageEncoding, int execAckStatus) {
        SecurityDefinitionAck msg = new SecurityDefinitionAck();
        msg.setString(SecurityID.FIELD, securityID);
        if (messageEncoding == null) {
            messageEncoding = "UTF-8";
        }
        msg.getHeader().setString(347, messageEncoding);
        msg.setChar(ExecAckStatus.FIELD, (char) (execAckStatus + 49));
        return msg;
    }

    /**
     * session处理逻辑
     *
     * @param configBean
     * @param starOrStop
     * @return
     */
    private Boolean sessionHandle(ConfigBean configBean, Boolean starOrStop) {
        LogManager.info("USERNAME[" + configBean.getUserName() + "]PASSWORD[" + configBean.getPassword() + "]MARKET[" + configBean.getMarket() + "]");
        boolean ret = true;
        try {
            ImixSession imixSession = new ImixSession(configBean.getUserName(), configBean.getPassword(), configBean.getMarket());
            imixSession.start();
        } catch (ImixSessionExistingException e) {
            ret = false;
            LogManager.error("ImixSessionExistingException异常", e);
        } catch (ConfigError e) {
            ret = false;
            LogManager.error("ConfigError异常", e);
        }
        return ret;
    }

    public ConfigBean getConfigBean() {
        return configBean;
    }

    public void setConfigBean(ConfigBean configBean) {
        this.configBean = configBean;
    }

    public ImixRmbClientListener getListener() {
        return listener;
    }

    public void setListener(ImixRmbClientListener listener) {
        this.listener = listener;
    }

    public ImixSession getSession() {
        return session;
    }

    /**
     * 本地测试
     *
     * @throws Exception
     */
    public void testMessage() throws Exception {

        BufferedReader reader = null;
        String msgFilePath = "C:\\Singlee\\cfets\\fx\\errData\\2018-05-29\\0000-0000-0762-0F.txt";
        StringBuffer sbf = null;
        try {
            sbf = new StringBuffer();
            LogManager.debug("read msgFilePath :" + msgFilePath);
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(msgFilePath)), "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sbf.append(line);
            }
            reader.close();

            ExecutionReport e = new ExecutionReport();
            e.fromString(sbf.toString(), new DataDictionary("IMIX10.xml"), false);
            listener.onMessage(e);

        } catch (Exception e) {
            throw e;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

    }
}
