package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.SLFBean;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import com.singlee.financial.pojo.trade.SLFPledegBean;
import com.singlee.financial.pojo.trade.SubScriptionInstBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.field.ContactInfoIDType;
import imix.field.PartyDetailAltSubIDType;
import imix.field.PartySubIDType;
import imix.imix10.ExecutionReport;
import imix.imix10.SupplementalReport;
import imix.imix10.component.PartyDetailAlt;
import imix.imix10.component.TenderDetails;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 清算信息解析
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbSettleEntry extends RmbBaseEntry {

    public void setBaseInfo(ExecutionReport message, String bankId, TradeBaseBean baseBean, SettBaseBean settInfo, SettBaseBean contraPartySettInfo) throws FieldNotFound, FieldConvertError {
        // 设置本方机构及清算信息
        convert(message, bankId, SettPartyEnum.TheParty, baseBean.getInstitutionInfo(), settInfo);
        // 设置对手方机构及清算信息
        convert(message, bankId, SettPartyEnum.CounterParty, baseBean.getContraPatryInstitutionInfo(), contraPartySettInfo);
        baseBean.setExecId(message.isSetExecID() ? message.getExecID().getValue() : CfetsUtils.EMPTY); // CFETS成交编号
        baseBean.setPs(convert(message.isSetSide() ? message.getSide() : null, settInfo.getMarkerTakerRole()));
        baseBean.setTradeTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : CfetsUtils.EMPTY);// 交易成交时间
        baseBean.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);
        baseBean.setDealDateTime(message.isSetTradeTime() ? message.getTradeTime().getDateValue() : null);
        baseBean.setDealStatus(convert(message.isSetDealTransType() ? message.getDealTransType() : null));//
        baseBean.setDealStatus(convert(message.isSetDealTransType() ? message.getDealTransType() : null));// 报文动作
        baseBean.setDcie(convert(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator() : null)); //数据类型标识
    }

    /**
     * noPartyIDs公共处理
     *
     * @param message
     * @param marketTaker
     * @param institution
     * @param settBean
     * @throws FieldNotFound
     */
    public void convert(ExecutionReport message, String bankId, SettPartyEnum marketTaker, InstitutionBean institution, SettBaseBean settBean) throws FieldNotFound {
        for (int i = 1; i <= message.getParties().getNoPartyIDs().getValue(); i++) {
            // 实例做市商/受市商清算信息列表对象
            ExecutionReport.NoPartyIDs noPartyIDs = new ExecutionReport.NoPartyIDs();
            // 获取做市商/受市商清算信息列表数据
            message.getGroup(i, noPartyIDs);
            // 获取机构编号
            String partyID = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : CfetsUtils.EMPTY;
            // 获取做市商/受市商角色
            int partyRole = noPartyIDs.isSetPartyRole() ? noPartyIDs.getPartyRole().getValue() : 0;
            logger.debug("partyID:" + partyID + ",partyRole:" + partyRole);
            /*
             * 判断当前清算信息是本方还是对手方
             */
            // 如果不是当前要取的信息,则继续读取下条记录
            if (!(partyID.equalsIgnoreCase(bankId) ? SettPartyEnum.TheParty : SettPartyEnum.CounterParty).equals(marketTaker)) {
                continue;
            }
            institution.setInstitutionId(partyID);// 交易对手ID/交易机构ID
            if (partyRole == 119) {
                settBean.setMarkerTakerRole(MarkerTakerEnum.Taker); // 买方
            } else {
                settBean.setMarkerTakerRole(MarkerTakerEnum.Marker); // 卖方
            }
            convert(noPartyIDs, institution, settBean);
        }
    }


    /**
     * noPartySubIDs 处理公共处理
     *
     * @param noPartyIDs
     * @param institution
     * @param settBean
     * @throws FieldNotFound
     */
    public void convert(ExecutionReport.NoPartyIDs noPartyIDs, InstitutionBean institution, SettBaseBean settBean) throws FieldNotFound {
        SecurSettBean securSettBean = new SecurSettBean();
        BeanUtils.copyProperties(settBean, securSettBean); // 属性copy
        //构造NoPartySubIDs【802】循环个数
        ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = null;
        int partySubNum = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.getNoPartySubIDs().getValue() : 0;
        for (int i = 1; i <= partySubNum; i++) {
            //解析机构信息802循环组
            noPartySubIDs = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
            // 获取清算信息明细信息对象数据
            noPartyIDs.getGroup(i, noPartySubIDs);
            // 获取清算信息类型
            int partySubIdType = noPartySubIDs.isSetPartySubIDType() ? noPartySubIDs.getPartySubIDType().getValue() : 0;
            // 获取清算信息类型对应的值
            String partySubIdValue = noPartySubIDs.isSetPartySubID() ? noPartySubIDs.getPartySubID().getValue() : CfetsUtils.EMPTY;
            logger.info("清算明细项开始处理 >>>> partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
            switch (partySubIdType) {
                case PartySubIDType.TRADER_NAME:
                    // 交易员名称-101
                    institution.setTradeName(partySubIdValue);
                    institution.setTradeId(partySubIdValue);
                    break;
                case PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM:
                    //会员中文全称
                    institution.setFullName(partySubIdValue);
                    break;
                case PartySubIDType.SHORT_LEGAL_CHINESE_NAME_OF_FIRM:
                    //会员中文简称
                    institution.setShortName(partySubIdValue);
                    break;
                case PartySubIDType.CONTACT_NAME://联系人名称
                    institution.setContactName(partySubIdValue);
                    break;
                case PartySubIDType.CASH_ACCOUNT_NUMBER:
                    securSettBean.setAcctNo(partySubIdValue); // 账户行资金账号
                    break;
                case PartySubIDType.CASH_ACCOUNT_NAME:
                    securSettBean.setAcctName(partySubIdValue); // 账户行名称
                    break;
                case PartySubIDType.SETTLEMENT_BANK_NAME:
                    securSettBean.setBankName(partySubIdValue);// 开户行名称
                    break;
                case PartySubIDType.SETTLEMENT_BANK_SORT_CODE:
                    securSettBean.setBankAcctNo(partySubIdValue); // 支付系统行行号
                    break;
                case PartySubIDType.SECURITIES_ACCOUNT_NAME:
                    securSettBean.setTrustAcctName(partySubIdValue); // 托管账户名称
                    break;
                case PartySubIDType.CUSTODIAN_INSTITUTION_NAME:
                    securSettBean.setTrustInstitutionId(partySubIdValue); // 托管机构Id
                    break;
                case PartySubIDType.SECURITIES_ACCOUNT_NUMBER:
                    securSettBean.setTrustAcctNo(partySubIdValue); // 托管账户号
                    break;
                case PartySubIDType.ELIGIBLE_COUNTERPARTY:
                    institution.setTradGroup(partySubIdValue); // 交易成员群组
                    break;
                case PartySubIDType.POSTAL_ADDRESS:
                    institution.setAddress(partySubIdValue); // 地址
                    break;
                case PartySubIDType.MULTI_CURRENCY_ACCOUNT_DESCRIPTION:
                    securSettBean.setRemark(partySubIdValue); // 附言、备注
                    break;
                default:
                    logger.warn("清算明细项未处理 >>>>>> partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
                    break;
            }
        }
        //联系人10601
        int contactNum = noPartyIDs.isSetNoContactInfos() ? noPartyIDs.getNoContactInfos().getValue() : 0;
        ExecutionReport.NoPartyIDs.NoContactInfos noContactInfos = null;
        for (int i = 1; i <= contactNum; i++) {
            noContactInfos = new ExecutionReport.NoPartyIDs.NoContactInfos();
            // 获取清算信息明细信息对象数据
            noPartyIDs.getGroup(i, noContactInfos);
            // 获取清算信息类型
            int contactInfoIDType = noContactInfos.isSetContactInfoIDType() ? noContactInfos.getContactInfoIDType().getValue() : 0;
            // 获取清算信息类型对应的值
            String contactInfoID = noContactInfos.isSetContactInfoID() ? noContactInfos.getContactInfoID().getValue() : CfetsUtils.EMPTY;
            logger.info("联系信息细项开始处理 >>>> contactInfoIDType:" + contactInfoIDType + ",contactInfoID:" + contactInfoID);
            switch (contactInfoIDType) {
                case ContactInfoIDType.PHONE_NO_1: //联系电话
                    institution.setContactPhone(contactInfoID);
                    break;
                case ContactInfoIDType.FAX_NO_1://联系传真
                    institution.setContactFax(contactInfoID);
                    break;
                default:
                    logger.warn("联系信息项未处理 >>>>>> contactInfoIDType:" + contactInfoIDType + ",contactInfoID:" + contactInfoID);
                    break;
            }
        }
        //关联方1562主要是处理【质押式-跨托管】
        int relatedNum = noPartyIDs.isSetNoRelatedPartyDetails() ? noPartyIDs.getNoRelatedPartyDetails().getValue() : 0;
        ExecutionReport.NoPartyIDs.NoRelatedPartyDetails noRelatedPartyDetails = null;
        List<SecurSettBean> secureSettList = new ArrayList<SecurSettBean>();
        SecurSettBean secureSett = null;
        for (int i = 1; i <= relatedNum; i++) {
            noRelatedPartyDetails = new ExecutionReport.NoPartyIDs.NoRelatedPartyDetails();
            noPartyIDs.getGroup(i, noRelatedPartyDetails);
            //分段信息
            int relateSubNum = noRelatedPartyDetails.isSetNoRelatedPartyDetailSubIDs() ? noRelatedPartyDetails.getNoRelatedPartyDetailSubIDs().getValue() : 0;
            secureSett = new SecurSettBean();
            ExecutionReport.NoPartyIDs.NoRelatedPartyDetails.NoRelatedPartyDetailSubIDs noRelatedPartyDetailSubIDs = null;
            for (int j = 1; j <= relateSubNum; j++) {
                noRelatedPartyDetailSubIDs = new ExecutionReport.NoPartyIDs.NoRelatedPartyDetails.NoRelatedPartyDetailSubIDs();
                // 获取清算信息明细信息对象数据
                noRelatedPartyDetails.getGroup(j, noRelatedPartyDetailSubIDs);
                // 获取清算信息类型
                int rpdsType = noRelatedPartyDetailSubIDs.isSetRelatedPartyDetailSubIDType() ? noRelatedPartyDetailSubIDs.getRelatedPartyDetailSubIDType().getValue() : 0;
                // 获取清算信息类型对应的值
                String rpdsId = noRelatedPartyDetailSubIDs.isSetRelatedPartyDetailSubID() ? noRelatedPartyDetailSubIDs.getRelatedPartyDetailSubID().getValue() : CfetsUtils.EMPTY;
                logger.info("跨托管细项开始处理 >>>> rpdsType:" + rpdsType + ",rpdsId:" + rpdsId);
                switch (rpdsType) {
                    case PartySubIDType.CASH_ACCOUNT_NAME: //资金账户户名
                        secureSett.setAcctName(rpdsId);
                        break;
                    case PartySubIDType.SETTLEMENT_BANK_NAME://资金开户行
                        secureSett.setBankName(rpdsId);
                        break;
                    case PartySubIDType.SETTLEMENT_BANK_SORT_CODE://资金开户行行号
                        secureSett.setBankAcctNo(rpdsId);
                        break;
                    case PartySubIDType.CASH_ACCOUNT_NUMBER://资金账号
                        secureSett.setAcctNo(rpdsId);
                        break;
                    case PartySubIDType.SECURITIES_ACCOUNT_NAME://托管账户户名
                        secureSett.setTrustAcctName(rpdsId);
                        break;
                    case PartySubIDType.CUSTODIAN_INSTITUTION_NAME://托管机构名称
                        secureSett.setTrustInstitutionId(rpdsId);
                        break;
                    case PartySubIDType.SECURITIES_ACCOUNT_NUMBER://托管账号
                        secureSett.setTrustAcctNo(rpdsId);
                        break;
                    default:
                        logger.warn("跨托管细项未处理 >>>>>> rpdsType:" + rpdsType + ",rpdsId:" + rpdsId);
                        break;
                }
            }
            secureSettList.add(secureSett);
        }
        securSettBean.setSecurSettList(secureSettList);
        BeanUtils.copyProperties(securSettBean, settBean); // 属性copy
    }

    /**
     * noPartySubIDs 处理公共处理
     * 常备借贷便利相关解析
     *
     * @param noPartyIDs
     * @param mmBean
     * @param message
     * @throws FieldNotFound
     */
    public void convert(ExecutionReport.NoPartyIDs noPartyIDs, SLFBean mmBean, ExecutionReport message) throws FieldNotFound {
//        SecurSettBean securSettBean = new SecurSettBean();
//        SLFPledegBean SLFPledegBean=new SLFPledegBean();
//        BeanUtils.copyProperties(settBean ,SLFPledegBean); // 属性copy
        ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = null;
        SLFPledegBean SLFPledegBean = new SLFPledegBean();
        int id = message.getNoPartyIDs().getValue();
        for (int x = 1; x <= id; x++) {
            // 获取清算信息明细信息对象数据
            message.getGroup(x, noPartyIDs);
            int partyRole = noPartyIDs.isSetPartyRole() ? noPartyIDs.getPartyRole().getValue() : 0;
            if (partyRole == 204) {
                String SignNumber = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : "";
                SLFPledegBean.setBranchNumber(SignNumber);//分支行机构码
                for (int j = 1; j <= noPartyIDs.getNoPartySubIDs().getValue(); j++) {
                    noPartySubIDs = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
                    // 获取清算信息明细信息对象数据
                    noPartyIDs.getGroup(j, noPartySubIDs);
                    // 获取清算信息类型
                    int partySubIdType = noPartySubIDs.isSetPartySubIDType() ? noPartySubIDs.getPartySubIDType().getValue() : 0;
                    // 获取清算信息类型对应的值
                    String partySubIdValue = noPartySubIDs.isSetPartySubID() ? noPartySubIDs.getPartySubID().getValue() : CfetsUtils.EMPTY;
                    logger.info("分支行信息 partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
                    switch (partySubIdType) {
                        case PartySubIDType.TRADER_NAME:
                            // 交易员名称-101
                            mmBean.getInstitutionInfo().setTradeName(partySubIdValue);
                            mmBean.getInstitutionInfo().setTradeId(partySubIdValue);
                            break;
                        case PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM:
                            mmBean.getInstitutionInfo().setFullName(partySubIdValue);
                            break;
                        case PartySubIDType.SHORT_LEGAL_CHINESE_NAME_OF_FIRM:
                            mmBean.getInstitutionInfo().setShortName(partySubIdValue);
                            break;
                        case PartySubIDType.CONTACT_NAME://联系人名称
                            mmBean.getInstitutionInfo().setContactName(partySubIdValue);
                            break;
//                   case PartySubIDType.CASH_ACCOUNT_NUMBER:
//                       SLFPledegBean.setSignFundID(partySubIdValue); // 账户行资金账号
//                       break;
//                   case PartySubIDType.SECURITIES_ACCOUNT_NUMBER:
//                       SLFPledegBean.setSignEscrowID(partySubIdValue); // 托管账户号
//                       break;
                        case PartySubIDType.PLEDGE_CONTRACT_SIGNATORY:
                            SLFPledegBean.setSignName(partySubIdValue); // 质押合同签署方机构中文名
                            break;
                        case PartySubIDType.LOAN_CONTRACT_SIGNATORY:
                            SLFPledegBean.setLoansName(partySubIdValue); //贷款合同签署方机构中文名
                            break;
                        default:
                            logger.warn("分支行信息 partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
                            break;
                    }
                }
            } else if (partyRole == 203) {
                String SignNumber = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : "";
                SLFPledegBean.setSignNumber(SignNumber);//法人机构码

                for (int j = 1; j <= noPartyIDs.getNoPartySubIDs().getValue(); j++) {
                    noPartySubIDs = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
                    // 获取清算信息明细信息对象数据
                    noPartyIDs.getGroup(j, noPartySubIDs);
                    // 获取清算信息类型
                    int partySubIdType = noPartySubIDs.isSetPartySubIDType() ? noPartySubIDs.getPartySubIDType().getValue() : 0;
                    // 获取清算信息类型对应的值
                    String partySubIdValue = noPartySubIDs.isSetPartySubID() ? noPartySubIDs.getPartySubID().getValue()
                            : CfetsUtils.EMPTY;
                    logger.info("法人信息 partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
                    switch (partySubIdType) {
                        case PartySubIDType.TRADER_NAME:
                            // 交易员名称-101
                            mmBean.getInstitutionInfo().setTradeName(partySubIdValue);
                            mmBean.getInstitutionInfo().setTradeId(partySubIdValue);
                            break;
                        case PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM:
                            mmBean.getInstitutionInfo().setFullName(partySubIdValue);
                            break;
                        case PartySubIDType.SHORT_LEGAL_CHINESE_NAME_OF_FIRM:
                            mmBean.getInstitutionInfo().setShortName(partySubIdValue);
                            break;
                        case PartySubIDType.CONTACT_NAME://联系人名称
                            mmBean.getInstitutionInfo().setContactName(partySubIdValue);
                            break;
                        case PartySubIDType.CASH_ACCOUNT_NUMBER:
                            SLFPledegBean.setSignFundID(partySubIdValue); // 法人资金账号
                            break;
                        case PartySubIDType.SECURITIES_ACCOUNT_NUMBER:
                            SLFPledegBean.setSignEscrowID(partySubIdValue); // 法人托管账户号
                            break;
                        case PartySubIDType.PLEDGE_CONTRACT_SIGNATORY:
                            SLFPledegBean.setLegalSignName(partySubIdValue); //质押合同签署方法人机构中文名
                            break;
                        case PartySubIDType.LOAN_CONTRACT_SIGNATORY:
                            SLFPledegBean.setLegalLoansName(partySubIdValue); //贷款合同签署方法人机构中文名
                            break;
                        default:
                            logger.warn("法人信息! partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
                            break;
                    }
                }
            }
            mmBean.setsLFPledegBean(SLFPledegBean);
        }
//        BeanUtils.copyProperties(securSettBean, settBean); // 属性copy
    }


    /**
     * noPartyIDs公共处理
     *
     * @param sReport
     * @param marketTaker
     * @param institution
     * @param settBean
     * @throws FieldNotFound
     */
    public void convert(SupplementalReport sReport, String bankId, SettPartyEnum marketTaker, InstitutionBean institution, SettBaseBean settBean) throws FieldNotFound {
        for (int i = 1; i <= sReport.getParties().getNoPartyIDs().getValue(); i++) {
            // 实例做市商/受市商清算信息列表对象
            ExecutionReport.NoPartyIDs noPartyIDs = new ExecutionReport.NoPartyIDs();
            // 获取做市商/受市商清算信息列表数据
            sReport.getGroup(i, noPartyIDs);
            // 获取机构编号
            String partyID = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : CfetsUtils.EMPTY;
            // 获取做市商/受市商角色
            int partyRole = noPartyIDs.isSetPartyRole() ? noPartyIDs.getPartyRole().getValue() : 0;
            logger.debug("partyID:" + partyID + ",partyRole:" + partyRole);
            /*
             * 判断当前清算信息是本方还是对手方
             */
            // 如果不是当前要取的信息,则继续读取下条记录
            if (!(partyID.equalsIgnoreCase(bankId) ? SettPartyEnum.TheParty : SettPartyEnum.CounterParty)
                    .equals(marketTaker)) {
                continue;
            }
            institution.setInstitutionId(partyID);// 交易对手ID/交易机构ID
            if (partyRole == 119) {
                settBean.setMarkerTakerRole(MarkerTakerEnum.Taker); // 买方
            } else {
                settBean.setMarkerTakerRole(MarkerTakerEnum.Marker); // 卖方
            }
            convert(noPartyIDs, institution, settBean);
        }
    }

    /**
     * NoPartyDetailAltIDs 处理公共处理
     *
     * @param detailAltIDs
     * @param institution
     * @throws FieldNotFound
     */
    public void convert(PartyDetailAlt.NoPartyDetailAltIDs detailAltIDs, SubScriptionInstBean institution) throws Exception {
        int n = detailAltIDs.isSetNoPartyDetailAltSubIDs() ? detailAltIDs.getNoPartyDetailAltSubIDs().getValue() : 0;
        for (int j = 1; j <= n; j++) {
            PartyDetailAlt.NoPartyDetailAltIDs.NoPartyDetailAltSubIDs noPartySubIDs = new PartyDetailAlt.NoPartyDetailAltIDs.NoPartyDetailAltSubIDs();
            detailAltIDs.getGroup(j, noPartySubIDs);
            // 获取清算信息类型
            int partySubIdType = noPartySubIDs.isSetPartyDetailAltSubIDType() ? noPartySubIDs.getPartyDetailAltSubIDType().getValue() : 0;
            // 获取清算信息类型对应的值
            String partySubIdValue = noPartySubIDs.isSetPartyDetailAltSubID() ? noPartySubIDs.getPartyDetailAltSubID().getValue()
                    : CfetsUtils.EMPTY;
            logger.info("机构信息开始处理。。 partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
            switch (partySubIdType) {
                case PartyDetailAltSubIDType.USER_INPUT_TRADES://提交用户
                    institution.setTradeId(partySubIdValue);
                    institution.setTradeName(partySubIdValue);
                    break;
                case PartyDetailAltSubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM://机构中文全称
                    institution.setFullName(partySubIdValue);
                    break;
                default:
                    break;
            }
        }
        int NoTenderDetails = detailAltIDs.isSetNoTenderDetailsAltGrpIDs() ? detailAltIDs.getNoTenderDetailsAltGrpIDs().getValue() : 0;
        if (NoTenderDetails > 0) {
            TenderDetails t = detailAltIDs.getTenderDetails();
            //投标价位
            institution.setTenderPx(t.isSetTenderPx() ? new BigDecimal(t.getTenderPx().getDoubleValue()) : null);
            //投标量（亿元）
            institution.setTenderAmt(t.isSetTenderAmt() ? new BigDecimal(t.getTenderAmt().getDoubleValue()) : null);
            //中标价位
            institution.setTenderSuccessPx(t.isSetTenderSuccessPx() ? new BigDecimal(t.getTenderSuccessPx().getDoubleValue()) : null);
            //中标量（亿元）
            institution.setTenderSuccessAmt(t.isSetTenderSuccessAmt() ? new BigDecimal(t.getTenderSuccessAmt().getDoubleValue()) : null);
        }
        institution.setFeeActualAmt(detailAltIDs.isSetFeeActualAmt() ? new BigDecimal(detailAltIDs.getFeeActualAmt().getValue()) : null);
        institution.setSubScriptionAmt(detailAltIDs.isSetSubscriptionAmt() ? new BigDecimal(detailAltIDs.getSubscriptionAmt().getValue()) : null);
        institution.setSubScriptionTime(detailAltIDs.isSetSubscriptionTime() ? detailAltIDs.getSubscriptionTime().getDateValue() : null);
    }
}
