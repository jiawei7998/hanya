package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.InterBankDepositAddBean;
import com.singlee.financial.pojo.InterestCashFlowBean;
import com.singlee.financial.pojo.component.SettPartyEnum;
import imix.imix10.SupplementalReport;
import imix.imix10.component.UndInstrmtGrp;
import imix.imix10.component.UnderlyingInstrument;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 同业存款附加协议
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbIbdSuppleAgreeEntry extends RmbSettleEntry implements ImixRmbMessageHandler<SupplementalReport, InterBankDepositAddBean> {

    /**
     * 同业存款附加协议
     *
     * @param sReport
     * @param interBankDepositAddBean
     * @throws Exception
     */
    public void convert(SupplementalReport sReport, InterBankDepositAddBean interBankDepositAddBean, String bankId) throws Exception {
        // 设置本方机构及清算信息
        convert(sReport, bankId, SettPartyEnum.TheParty, interBankDepositAddBean.getInstitutionInfo(), interBankDepositAddBean.getSettInfo());
        // 设置对手方机构及清算信息
        convert(sReport, bankId, SettPartyEnum.CounterParty, interBankDepositAddBean.getContraPatryInstitutionInfo(), interBankDepositAddBean.getContraPartySettInfo());

        interBankDepositAddBean.setTradeTime(sReport.isSetTradeTime() ? sReport.getTradeTime().getValue() : CfetsUtils.EMPTY);// 成交时间
        interBankDepositAddBean.setDealDate(sReport.isSetTradeDate() ? sReport.getTradeDate().getDateValue() : null); //成交日期
        interBankDepositAddBean.setDealMode(convert(sReport.isSetTradeMethod() ? sReport.getTradeMethod() : null)); //交易方式
        interBankDepositAddBean.setStatus(convert(sReport.isSetExecType() ? sReport.getExecType() : null));// 交易状态

        interBankDepositAddBean.setExecId(sReport.isSetExecID() ? sReport.getExecID().getValue() : CfetsUtils.EMPTY);//成交编号
        interBankDepositAddBean.setSupplType(sReport.isSetSupplType() ? sReport.getSupplType().getValue() : CfetsUtils.EMPTY);//附加协议类型
        interBankDepositAddBean.setSupplTransType(sReport.isSetSupplTransType() ? sReport.getSupplTransType().getValue() : CfetsUtils.EMPTY);//操作类型
        interBankDepositAddBean.setPs(convert(sReport.isSetSide() ? sReport.getSide() : null, interBankDepositAddBean.getSettInfo().getMarkerTakerRole())); // 交易方向
        interBankDepositAddBean.setKindId(sReport.isSetSecurityID() ? sReport.getSecurityID().getValue() : CfetsUtils.EMPTY);// 交易品种代码
        interBankDepositAddBean.setTenor(sReport.isSetSecurityTermString() ? sReport.getSecurityTermString().getValue() : CfetsUtils.EMPTY); // 存款期限
        interBankDepositAddBean.setHoldingDays(sReport.isSetCashHoldingDays() ? sReport.getCashHoldingDays().getUnionTypeValue() : CfetsUtils.EMPTY); // 实际占款天数
        interBankDepositAddBean.setRate(sReport.isSetPrice() ? new BigDecimal(sReport.getPrice().getValue()) : null);// 存款利率
        interBankDepositAddBean.setAmt(sReport.isSetOrderQty() ? new BigDecimal(sReport.getOrderQty().getValue()) : null);//存款金额
        interBankDepositAddBean.setPayFrequency(convert(sReport.isSetCouponPaymentFrequency() ? sReport.getCouponPaymentFrequency() : null));//付息频率
        interBankDepositAddBean.setFirstPayDate(sReport.isSetCouponPaymentDate() ? sReport.getCouponPaymentDate().getValue() : CfetsUtils.EMPTY);//首次付息日
        interBankDepositAddBean.setInterestAmt(sReport.isSetAccruedInterestTotalAmt() ? new BigDecimal(sReport.getAccruedInterestTotalAmt().getValue()) : null); // 应计利息总金额
        interBankDepositAddBean.setMaturityAmt(sReport.isSetSettlCurrAmt2() ? new BigDecimal(sReport.getSettlCurrAmt2().getValue()) : null); // 到期支取金额
        interBankDepositAddBean.setValueDate(sReport.isSetSettlDate() ? sReport.getSettlDate().getDateValue() : null); // 首次结算日
        interBankDepositAddBean.setMaturityDate(sReport.isSetSettlDate2() ? sReport.getSettlDate2().getDateValue() : null);// 到期支取日
        interBankDepositAddBean.setText(sReport.isSetText() ? sReport.getText().getValue() : CfetsUtils.EMPTY);// 补充条款
        interBankDepositAddBean.setAdvanceWithDrawAgreement(sReport.isSetAdvanceWithDrawAgreement() ? sReport.getAdvanceWithDrawAgreement().getValue() : CfetsUtils.EMPTY);//提前支取约定 1不允许提前支取 2允许全额 3 允许全额或部分
        interBankDepositAddBean.setManagingEntity2(sReport.isSetManagingEntity2() ? sReport.getManagingEntity2().getValue() : CfetsUtils.EMPTY);//存出方经办行
        interBankDepositAddBean.setManagingEntity1(sReport.isSetManagingEntity1() ? sReport.getManagingEntity1().getValue() : CfetsUtils.EMPTY);//存入方经办行
        interBankDepositAddBean.setDataCategoryIndicator(sReport.isSetDataCategoryIndicator() ? sReport.getDataCategoryIndicator().getValue() : null);//数据类型标识
        interBankDepositAddBean.setDcie(convert(sReport.isSetDataCategoryIndicator() ? sReport.getDataCategoryIndicator() : null)); //数据类型标识 Excel导出使用

        UndInstrmtGrp undInstrmtGrp = sReport.getUndInstrmtGrp();
        UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
        int NoUnderlyings = undInstrmtGrp.isSetNoUnderlyings() ? undInstrmtGrp.getNoUnderlyings().getValue() : 0;
        UnderlyingInstrument inst = new UnderlyingInstrument();

        List<InterestCashFlowBean> icfbList = new ArrayList<>();
        InterestCashFlowBean icfb = null;
        for (int i = 1; i <= NoUnderlyings; i++) {
            sReport.getGroup(i, noUnderlyings);
            inst = noUnderlyings.getUnderlyingInstrument();
            icfb = new InterestCashFlowBean();
            //付息日 按期付息的时候有值
            icfb.setIntPayDate(inst.isSetUnderlyingCouponPaymentDate() ? (inst.getUnderlyingCouponPaymentDate().getValue() == null ? CfetsUtils.EMPTY : inst.getUnderlyingCouponPaymentDate().getValue()) : CfetsUtils.EMPTY);
            //应计利息 按期付息的时候有值
            icfb.setAccruedIntAmt(new BigDecimal(inst.isSetUnderlyingAccruedInterestAmt() ? (inst.getUnderlyingAccruedInterestAmt().getValue() == null ? "0" : inst.getUnderlyingAccruedInterestAmt().getValue()) : "0"));
            icfbList.add(icfb);
        }
        interBankDepositAddBean.setIcfbList(icfbList);

        int NoStipulations = sReport.isSetNoStipulations() ? sReport.getNoStipulations().getValue() : 0;
        for (int i = 1; i <= NoStipulations; i++) {
            SupplementalReport.NoStipulations noStipulations = new SupplementalReport.NoStipulations();
            sReport.getGroup(i, noStipulations);
            // 到期收益率
            interBankDepositAddBean.setExpYield(noStipulations.isSetStipulationValue() ? new BigDecimal(noStipulations.getStipulationValue().getValue()) : null);
        }

        int NoAdvanceInfos = sReport.isSetNoAdvanceInfos() ? sReport.getNoAdvanceInfos().getValue() : 0;
        for (int i = 1; i <= NoAdvanceInfos; i++) {
            SupplementalReport.NoAdvanceInfos noAdvanceInfos = new SupplementalReport.NoAdvanceInfos();
            sReport.getGroup(i, noAdvanceInfos);
            interBankDepositAddBean.setAdvanceId(noAdvanceInfos.isSetAdvanceID() ? noAdvanceInfos.getAdvanceID().getValue() : CfetsUtils.EMPTY);  //提前支取编号
            interBankDepositAddBean.setAdvanceInfoType(noAdvanceInfos.isSetAdvanceInfoType() ? noAdvanceInfos.getAdvanceInfoType().getValue() : CfetsUtils.EMPTY);
            interBankDepositAddBean.setAdvanceStatus(noAdvanceInfos.isSetAdvanceStatus() ? noAdvanceInfos.getAdvanceStatus().getValue() : CfetsUtils.EMPTY);//提前支取状态
            interBankDepositAddBean.setAdvanveBusinessTime(noAdvanceInfos.isSetAdvanceBusinessTime() ? noAdvanceInfos.getAdvanceBusinessTime().getValue() : CfetsUtils.EMPTY);//提前支取申请业务发生时间
            interBankDepositAddBean.setAdvanceDate(noAdvanceInfos.isSetAdvanceDate() ? noAdvanceInfos.getAdvanceDate().getValue() : CfetsUtils.EMPTY); //提前支取日
            interBankDepositAddBean.setAdvanceAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceAmt() ? noAdvanceInfos.getAdvanceAmt().getValue() : "0")); //提前支取本金
            interBankDepositAddBean.setAdvanceCompensationType(noAdvanceInfos.isSetAdvanceCompensationType() ? noAdvanceInfos.getAdvanceCompensationType().getValue() : CfetsUtils.EMPTY);//提前支取补偿类型
            interBankDepositAddBean.setAdvancePrice(new BigDecimal(noAdvanceInfos.isSetAdvancePrice() ? noAdvanceInfos.getAdvancePrice().getValue() : "0"));//提前支取利率
            interBankDepositAddBean.setAdvanceLastQty(new BigDecimal(noAdvanceInfos.isSetAdvanceLastQty() ? noAdvanceInfos.getAdvanceLastQty().getValue() : "0")); //提前支取补偿金额
            interBankDepositAddBean.setAdvanceInterestAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceInterestAmt() ? noAdvanceInfos.getAdvanceInterestAmt().getValue() : "0")); //提前支取应计利息
            interBankDepositAddBean.setAdvanceSettlAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceSettlAmt() ? noAdvanceInfos.getAdvanceSettlAmt().getValue() : "0")); //提前支取结算金额
            interBankDepositAddBean.setAdvanceLeavesAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceLeavesAmt() ? noAdvanceInfos.getAdvanceLeavesAmt().getValue() : "0")); //提前支取后剩余存款本金
            interBankDepositAddBean.setAdvanceExpireAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceExpireAmt() ? noAdvanceInfos.getAdvanceExpireAmt().getValue() : "0")); //提前支取后到期支取金额
            interBankDepositAddBean.setAdvanceClause(noAdvanceInfos.isSetAdvanceClause() ? noAdvanceInfos.getAdvanceClause().getValue() : CfetsUtils.EMPTY);//提前支取条款
            interBankDepositAddBean.setAdvanceAttachmentIndicator(noAdvanceInfos.isSetAdvanceAttachmentIndicator() ? String.valueOf(noAdvanceInfos.getAdvanceAttachmentIndicator().getValue()) : CfetsUtils.EMPTY);
            interBankDepositAddBean.setAdvanceTraderName2(noAdvanceInfos.isSetAdvanceTraderName2() ? noAdvanceInfos.getAdvanceTraderName2().getValue() : CfetsUtils.EMPTY); //提前支取存出方交易员
            interBankDepositAddBean.setAdvanceTraderName1(noAdvanceInfos.isSetAdvanceTraderName1() ? noAdvanceInfos.getAdvanceTraderName1().getValue() : CfetsUtils.EMPTY);//提前支取存入方交易员
        }

        int NoSupFixedAccounts = sReport.isSetNoSupFixedAccounts() ? sReport.getNoSupFixedAccounts().getValue() : 0;
        for (int i = 1; i <= NoSupFixedAccounts; i++) {
            SupplementalReport.NoSupFixedAccounts noSupFixedAccounts = new SupplementalReport.NoSupFixedAccounts();
            sReport.getGroup(i, noSupFixedAccounts);
            interBankDepositAddBean.setSupId(noSupFixedAccounts.isSetSupID() ? noSupFixedAccounts.getSupID().getValue() : CfetsUtils.EMPTY);//补充定期账户编号
            interBankDepositAddBean.setSupStatus(noSupFixedAccounts.isSetSupStatus() ? noSupFixedAccounts.getSupStatus().getValue() : CfetsUtils.EMPTY);//补充定期账户状态
            interBankDepositAddBean.setSupBusinessTime(noSupFixedAccounts.isSetSupBusinessTime() ? noSupFixedAccounts.getSupBusinessTime().getValue() : CfetsUtils.EMPTY);//补充定期账户业务发生时间
            interBankDepositAddBean.setSupTradeName(noSupFixedAccounts.isSetSupTraderName() ? noSupFixedAccounts.getSupTraderName().getValue() : CfetsUtils.EMPTY);//补充定期账户交易员姓名
            interBankDepositAddBean.setSupCapitalBankName(noSupFixedAccounts.isSetSupCapitalBankName() ? noSupFixedAccounts.getSupCapitalBankName().getValue() : CfetsUtils.EMPTY); //补充定期账户资金开户行名称
            interBankDepositAddBean.setSupCapitalAccountName(noSupFixedAccounts.isSetSupCapitalAccountName() ? noSupFixedAccounts.getSupCapitalAccountName().getValue() : CfetsUtils.EMPTY); //补充定期账户资金账户户名
            interBankDepositAddBean.setSupCapitalAccountNumber(noSupFixedAccounts.isSetSupCapitalAccountNumber() ? noSupFixedAccounts.getSupCapitalAccountNumber().getValue() : CfetsUtils.EMPTY); //补充定期账户资金账号
            interBankDepositAddBean.setSupPaymentSystemCode(noSupFixedAccounts.isSetSupPaymentSystemCode() ? noSupFixedAccounts.getSupPaymentSystemCode().getValue() : CfetsUtils.EMPTY);//补充定期账户支付系统行号
        }

    }
}
