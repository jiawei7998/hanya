package com.singlee.financial.cfets.rmb;

import com.singlee.financial.cfets.config.ConfigBean;
import imix.*;
import imix.client.core.ImixSession;
import imix.client.core.Listener;
import imix.client.core.MessageCracker;
import imix.field.MsgType;
import imix.imix10.CommissionData;
import imix.imix10.ExecutionReport;
import imix.imix10.SecurityDefinition;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * CFETS消息监听服务
 *
 * @author chenxh
 */
public class ImixRmbClientListener extends MessageCracker implements Listener {

    private static int count = 0;

    // CFETS配置类
    private ConfigBean configBean;

    @Autowired
    ImixRmbCfetsApiService imixRmbCfetsApiService;
    /**
     * 收报线程队列管理
     */
    private ThreadPoolExecutor socketServerThreadPool;

    private static Logger logger = LoggerFactory.getLogger(ImixRmbClientListener.class);

    public ConfigBean getConfigBean() {
        return configBean;
    }

    public void setConfigBean(ConfigBean configBean) {
        this.configBean = configBean;
    }

    @Override
    public void fromAdmin(Message message, ImixSession imixSession) {

        try {
            logger.info("****************** fromAdmin()*************************");
            logger.info("****************** imixSession:" + imixSession);
            logger.info("****************** Ready to connection******************");
            String msgType = message.getHeader().getField(new MsgType()).getValue();
            //登录失败或登出成功报文返回类型
            if (!StringUtils.equals(msgType, MsgType.LOGOUT)) {
                count = 0;
                logger.info("***********************************");
                logger.info("******* 登录 成功 !************");
                logger.info("******* count reset to zero ! [" + count + "]******");
                logger.info("***********************************");
            }
        } catch (Exception e) {
            logger.error("********fromAdmin:", e);
        }
    }

    @Override
    public void fromApp(Message message, ImixSession imixSession)
            throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        logger.info("********fromApp:" + message);
        messageCrack(message);

    }

    @Override
    public void onError(ImixSession imixSession, int type) {
        logger.info("********connection failure:" + imixSession);
        // 断开 重连
        if (Listener.CONNECTION_FAILURE == type) {
            try {
                logger.info("*******系统捕获RmbClient错误消息,立即进行重新连接!**********");
                imixSession.start();
            } catch (ConfigError e) {
                logger.error("********onError exception", e);
            }
        }

    }

    @Override
    public void onLogon(ImixSession imixSession) {
        logger.info("********onLogon********" + imixSession);

    }

    @Override
    public void onLogout(ImixSession imixSession) {
        logger.info("********onLogonOut:" + imixSession);
        count++;
        logger.info("*******开始累计登录次数:" + count + "次**********");
        if (count > 10) {
            count = 0;
            logger.info("*******当前重连累计大于" + configBean.getReconCount() + "次,断开连接!**********");
            try {
                imixSession.stop();
            } catch (Exception e) {
                logger.error("********停止服务异常", e);
            }
        }
    }

    @Override
    public void toApp(Message message, ImixSession imixSession) {
        logger.info("********toApp:" + message);
        logger.info("********" + imixSession);
    }

    @Override
    public void onMessage(ExecutionReport message) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {
        // 消息为空,程序结束
        if (message.isEmpty()) {
            logger.error("********Cfets push message is empty", new RuntimeError());
            return;
        }
        // 获取报文类型
        String msgType = message.getHeader().getString(MsgType.FIELD);
        // 如果消息类型不是8的,不进行处理 35=S
        if (!MsgType.EXECUTION_REPORT.equals(msgType))// 成交单
        {
            logger.error("********Cfets packet type[" + msgType + "] is unsupporte!", new RuntimeError());
            return;
        }
        getThreadPool().execute(new ImixRmbMessageProcess(message, imixRmbCfetsApiService, configBean));
    }

    /***
     *
     * 存单发行报文
     */
    @Override
    public void onMessage(SecurityDefinition message) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {
        // 消息为空,程序结束
        if (message.isEmpty()) {
            logger.error("********Cfets push message is empty", new RuntimeError());
            return;
        }
        // 获取报文类型
        String msgType = message.getHeader().getString(MsgType.FIELD);

        // 如果消息类型不是d的,不进行处理
        if (!MsgType.SECURITY_DEFINITION.equals(msgType))// 存单发行
        {
            logger.error("Cfets packet type[" + msgType + "] is unsupporte!", new RuntimeError());
            return;
        }
        getThreadPool().execute(new ImixRmbMessageProcess(message, imixRmbCfetsApiService, configBean));
    }

    /***
     * 存单缴款报文
     *
     */
    @Override
    public void onMessage(CommissionData message) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {
        // 消息为空,程序结束
        if (message.isEmpty()) {
            logger.error("Cfets push message is empty", new RuntimeError());
            return;
        }

        // 获取报文类型
        String msgType = message.getHeader().getString(MsgType.FIELD);

        // 如果消息类型不是U93的,不进行处理
        if (!MsgType.COMMISSIONDATA.equals(msgType)) {
            logger.error("Cfets packet type[" + msgType + "] is unsupporte!", new RuntimeError());
            return;
        }

        // 接收到消息后,开启一个线程
        getThreadPool().execute(new ImixRmbMessageProcess(message, imixRmbCfetsApiService, configBean));
    }

    private ThreadPoolExecutor getThreadPool() {
        // 创建客户羰线程池管理对象
        if (null == socketServerThreadPool) {
            socketServerThreadPool = new ThreadPoolExecutor(configBean.getCorePoolSize(),
                    configBean.getMaximumPoolSize(), configBean.getKeepAliveTime(), TimeUnit.SECONDS,
                    new ArrayBlockingQueue<Runnable>(configBean.getArrayBlockingQueueSize()),
                    new ThreadPoolExecutor.CallerRunsPolicy());
        }
        return socketServerThreadPool;
    }
}
