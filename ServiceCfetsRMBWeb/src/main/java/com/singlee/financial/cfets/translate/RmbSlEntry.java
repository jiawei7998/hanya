package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.SecurityLendingBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.financial.pojo.trade.SecuritySettAmtBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;
import imix.imix10.component.MarginInfoGrp;

import java.math.BigDecimal;

/**
 * 债券借贷
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbSlEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, SecurityLendingBean> {
    /**
     * 本币债券借贷转换
     *
     * @param message
     * @param securityLendingBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, SecurityLendingBean securityLendingBean, String bankId)
            throws FieldNotFound, FieldConvertError {
        // 首次结算方式 到期结算方式 争议解决方式?
        setBaseInfo(message, bankId, securityLendingBean, securityLendingBean.getSettInfo(), securityLendingBean.getContraPartySettInfo());
        securityLendingBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态
        // 交易模式
        securityLendingBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        // 争议解决方式
        securityLendingBean.setDisputeWay(message.isSetDisputeSettlementMethod() ? message.getDisputeSettlementMethod().getValue() + "" : CfetsUtils.EMPTY);
        // 首期结算方式
        SecuritySettAmtBean firstSettle = new SecuritySettAmtBean();
        // 结算方式
        firstSettle.setSettType(message.isSetDeliveryType() ? message.getDeliveryType().getValue() + "" : CfetsUtils.EMPTY);
        firstSettle.setSettDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null);

        SecuritySettAmtBean secondSettle = new SecuritySettAmtBean();
        secondSettle.setSettType(message.isSetDeliveryType2() ? message.getDeliveryType2().getValue() + "" : CfetsUtils.EMPTY);
        secondSettle.setSettDate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);
        // 首期结算方式
        securityLendingBean.setComSecurityAmt(firstSettle);
        securityLendingBean.setMatSecurityAmt(secondSettle);

        securityLendingBean.setTerm(message.isSetTradeLimitDays() ? message.getTradeLimitDays().getValue() : CfetsUtils.EMPTY);//期限
        securityLendingBean.setOccupancyDays(String.valueOf(message.isSetCashHoldingDays() ? message.getCashHoldingDays().getValue() : CfetsUtils.EMPTY));//实际占款天数
        // 标的债券信息
        for (int i = 1; i <= message.getNoUnderlyings().getValue(); i++) {
            ExecutionReport.NoUnderlyings noUnderlyings = new ExecutionReport.NoUnderlyings();
            message.getGroup(i, noUnderlyings);
            // 标的债券代码
            securityLendingBean.getSecurityInfo().setSecurityId(noUnderlyings.isSetUnderlyingSecurityID() ? noUnderlyings.getUnderlyingSecurityID().getValue() : CfetsUtils.EMPTY);
            // 标的债券名称
            securityLendingBean.getSecurityInfo().setSecurityName(noUnderlyings.isSetUnderlyingSymbol() ? noUnderlyings.getUnderlyingSymbol().getValue() : CfetsUtils.EMPTY);
            // 标的债券券面总额
            securityLendingBean.getSecurityInfo().setFaceAmt(noUnderlyings.isSetUnderlyingQty() ? new BigDecimal(noUnderlyings.getUnderlyingQty().getValue()) : null);
            //票面利率
            securityLendingBean.getSecurityInfo().setRate(noUnderlyings.isSetUnderlyingCouponRate() ? new BigDecimal(noUnderlyings.getUnderlyingCouponRate().getValue()) : null);
            // 付息日
            securityLendingBean.getSecurityInfo().setIntPayDate(noUnderlyings.isSetUnderlyingCouponPaymentDate() ? noUnderlyings.getUnderlyingCouponPaymentDate().getValue() : "");
            // 应计利息总额
            securityLendingBean.getSecurityInfo().setAccruedInt(noUnderlyings.isSetUnderlyingAccruedInterestAmt() ? new BigDecimal(noUnderlyings.getUnderlyingAccruedInterestAmt().getValue()) : null);
        }
        // 交易品种
        securityLendingBean.setProductName(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);
        // 借贷费率
        securityLendingBean.setLendingFeeRate(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null);
        // 借贷费用
        for (int i = 1; i <= message.getNoMiscFees().getValue(); i++) {
            ExecutionReport.NoMiscFees noMiscFees = new ExecutionReport.NoMiscFees();
            message.getGroup(i, noMiscFees);
            if (noMiscFees.getMiscFeeType().getValue() == 13) {
                securityLendingBean.setLengingFeeCost(noMiscFees.isSetMiscFeeAmt() ? new BigDecimal(noMiscFees.getMiscFeeAmt().getValue()) : null);
            }
        }
        // 首次结算日
        securityLendingBean.getComSecurityAmt().setSettDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null);
        // 到期结算日
        securityLendingBean.getMatSecurityAmt().setSettDate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);
        for (int i = 1; i <= message.getNoMarginInfo().getValue(); i++) {
            MarginInfoGrp.NoMarginInfo noMarginInfo = new MarginInfoGrp.NoMarginInfo();
            message.getGroup(i, noMarginInfo);
            securityLendingBean.setMaginTotalAmt(noMarginInfo.isSetMarginTotalAmt() ? new BigDecimal(noMarginInfo.getMarginTotalAmt().getValue()) : null);
            securityLendingBean.setMarginReplacement(noMarginInfo.isSetMarginReplacement() ? noMarginInfo.getMarginReplacement().getValue() : null);
            for (int j = 1; j <= noMarginInfo.getNoMarginSecurities().getValue(); j++) {
                MarginInfoGrp.NoMarginInfo.NoMarginSecurities noMarginSecurities = new MarginInfoGrp.NoMarginInfo.NoMarginSecurities();
                noMarginInfo.getGroup(j, noMarginSecurities);
                SecurityInfoBean infoBean = new SecurityInfoBean();
                infoBean.setSecurityId(noMarginSecurities.isSetMarginSecuritiesID() ? noMarginSecurities.getMarginSecuritiesID().getValue() : CfetsUtils.EMPTY);
                infoBean.setSecurityName(noMarginSecurities.isSetMarginSymbol() ? noMarginSecurities.getMarginSymbol().getValue() : CfetsUtils.EMPTY);
                infoBean.setFaceAmt(noMarginSecurities.isSetMarginAMT() ? new BigDecimal(noMarginSecurities.getMarginAMT().getValue()) : null);
                securityLendingBean.getLendingInfos().add(infoBean);
            }
        }
    }
}
