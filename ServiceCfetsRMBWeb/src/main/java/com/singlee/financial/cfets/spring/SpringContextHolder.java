package com.singlee.financial.cfets.spring;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;

/**
 * 以静态变量保存Spring ApplicationContext, 可在任何代码任何地方任何时候中取出ApplicaitonContext.
 * 通过这个工具，可以在应用程序的内部直接访问spring管理的对象
 * 
 * @author lyon chen
 * 
 */
public class SpringContextHolder implements ApplicationContextAware {
	private static ApplicationContext applicationContext;

	/**
	 * 记录spring环境启动时间！
	 */
	private static long startTime =0;
	
	public static long getStartTime(){
		return startTime;
	}
	public static void setStartTime(long t){
		startTime = t;
	}
	
	/**
	 * 实现ApplicationContextAware接口的context注入函数, 将其存入静态变量.
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		SpringContextHolder.applicationContext = applicationContext; // NOSONAR
		
	}

	/**
	 * 取得存储在静态变量中的ApplicationContext.
	 */
	public static ApplicationContext getApplicationContext() {
		checkApplicationContext();
		return applicationContext;
	}

	/**
	 * 从静态变量ApplicationContext中取得Bean, 自动转型为所赋值对象的类型.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String name) {
		checkApplicationContext();
		return (T) applicationContext.getBean(name);
	}

	/**
	 * 从静态变量ApplicationContext中取得Bean, 自动转型为所赋值对象的类型.
	 */
	public static <T> T getBean(Class<T> clazz) {
		checkApplicationContext();
		return (T) applicationContext.getBean(clazz);
	}

	public static <T> T getBean(String name, Class<T> clazz) {
		checkApplicationContext();
		return (T) applicationContext.getBean(name, clazz);
	}

	public static <T> Map<String,T> getBeansOfType(Class<T> clazz) {
		checkApplicationContext();
		return applicationContext.getBeansOfType(clazz);
	}
	public static <T> Map<String, T> beansOfType(Class<T> clazz) {
		checkApplicationContext();
		return applicationContext.getBeansOfType(clazz, false, false);
	}
	public static String[] getBeanNames(Class<?> clazz) {
		checkApplicationContext();
		return applicationContext.getBeanNamesForType(clazz);
	}

	public static <T> T getDefaultBean(Class<T> clazz) {
		checkApplicationContext();
		// 非single的，懒加载的都过滤掉
		Map<String,T> m = applicationContext.getBeansOfType(clazz, false, false);
		if(m==null || m.size() ==0 ){
			return null;
		}
		Iterator<T> i = m.values().iterator();
		if(m.size()==1){
			return i.next();
		}
		// 查找 Prime 注解
		while(i.hasNext()){
			
		}
		return null;
	}

    /**
     * 如果BeanFactory包含一个与所给名称匹配的bean定义，则返回true
     *
     * @param name
     * @return boolean
     */
    public static boolean containsBean(String name) {
        return applicationContext.containsBean(name);
    }

    /**
     * 判断以给定名字注册的bean定义是一个singleton还是一个prototype。 如果与给定名字相应的bean定义没有被找到，将会抛出一个异常（NoSuchBeanDefinitionException）
     *
     * @param name
     * @return boolean
     * @throws org.springframework.beans.factory.NoSuchBeanDefinitionException
     */
    public static boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
        return applicationContext.isSingleton(name);
    }

    /**
     * @param name
     * @return Class 注册对象的类型
     * @throws org.springframework.beans.factory.NoSuchBeanDefinitionException
     */
    public static Class<?> getType(String name) throws NoSuchBeanDefinitionException {
        return applicationContext.getType(name);
    }

    /**
     * 如果给定的bean名字在bean定义中有别名，则返回这些别名
     *
     * @param name
     * @return
     * @throws org.springframework.beans.factory.NoSuchBeanDefinitionException
     */
    public static String[] getAliases(String name) throws NoSuchBeanDefinitionException {
        return applicationContext.getAliases(name);
    }
	
	/**
	 * 清除applicationContext静态变量.
	 */
	public static void cleanApplicationContext() {
		applicationContext = null;
	}

	private static void checkApplicationContext() {
		if (applicationContext == null) {
			// String msg = ErrorUtils.getCommonErrorMessage("error.common.0003", "");
			throw new IllegalStateException("application context not initial");
		}
	}
	
	
	
	/**
	 * 
	 * @param ms
	 * @param key
	 * @param params
	 * @param def
	 * @param l
	 * @return
	 */
	public static String getMessage(MessageSource ms, 
			String key, Object[] params, String def, Locale l){
		return ms==null ? def : ms.getMessage(key, params, def, l);
	}
	public static String getMessage(MessageSource ms, 
			String key, Object[] params, Locale l){
		if(ms == null){
			throw new NullPointerException("没有找到i18n国际化文件！");
		}
		return ms.getMessage(key, params, l);
	}
	
}