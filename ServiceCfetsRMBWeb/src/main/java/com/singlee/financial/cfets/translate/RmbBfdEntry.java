package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.BfdBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;
import imix.imix10.component.UndInstrmtGrp;
import imix.imix10.component.UnderlyingInstrument;

import java.math.BigDecimal;

/**
 * 债券远期
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbBfdEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, BfdBean> {
    /**
     * 转换本币-债券远期    marketIndicator=5
     *
     * @param message
     * @param bfdBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, BfdBean bfdBean, String bankId) throws FieldNotFound, FieldConvertError {
        //本方、对方机构信息及清算信息，成交单编号、成交日期、成交时间、方向等基本信息
        setBaseInfo(message, bankId, bfdBean, bfdBean.getSettInfo(), bfdBean.getContraPartySettInfo());
        //结算日期
        bfdBean.setSettlDate(message.isSetSettlDate() ? message.getSettlDate().getValue() : CfetsUtils.EMPTY);
        //交易金额
        bfdBean.setTradeAmt(message.isSetTradeCashAmt() ? new BigDecimal(message.getTradeCashAmt().getValue()) : null);
        //结算币种
        bfdBean.setSettlCurrency(message.isSetSettlCurrency() ? message.getSettlCurrency().getValue() : null);
        //结算金额
        bfdBean.setSettlCurrAmt(message.isSetSettlCurrAmt() ? new BigDecimal(message.getSettlCurrAmt().getValue()) : null);
        //应计利息
        bfdBean.setAccruedIntAmt(message.isSetAccruedInterestAmt() ? new BigDecimal(message.getAccruedInterestAmt().getValue()) : null);
        //合约期限
        bfdBean.setTradeLimitDays(message.isSetTradeLimitDays() ? new BigDecimal(message.getTradeLimitDays().getValue()) : null);
        //代偿期
        bfdBean.setTermToMaturity(message.isSetTermToMaturity() ? message.getTermToMaturity().getValue() : CfetsUtils.EMPTY);
        //交易品种
        bfdBean.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : null);
        //更新日
        bfdBean.setTransactTime(message.isSetTransactTime() ? message.getTransactTime().getDateValue() : null);
        //结算方式
        bfdBean.setDeliveryType(message.isSetDeliveryType() ? String.valueOf(message.getDeliveryType().getValue()) : CfetsUtils.EMPTY);
        //债券信息
        SecurityInfoBean securityInfo = new SecurityInfoBean();
        convert(message, securityInfo);
        bfdBean.setSecurityInfo(securityInfo);
    }

    /**
     * 远期债券处理 解析债券 marketIndicator=5
     *
     * @param message
     * @param securityInfo
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, SecurityInfoBean securityInfo) throws FieldNotFound, FieldConvertError {
        UndInstrmtGrp undInstrmtGrp = new UndInstrmtGrp();
        undInstrmtGrp = message.getUndInstrmtGrp();
        UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
        int NoUnderlyings = undInstrmtGrp.isSetNoUnderlyings() ? undInstrmtGrp.getNoUnderlyings().getValue() : 0;
        UnderlyingInstrument inst = new UnderlyingInstrument();
        //第二部分
        for (int i = 1; i <= NoUnderlyings; i++) {
            undInstrmtGrp.getGroup(i, noUnderlyings);
            inst = noUnderlyings.getUnderlyingInstrument();
            securityInfo.setSecurityId(inst.isSetUnderlyingSecurityID() ? (inst.getUnderlyingSecurityID().getValue() == null ? CfetsUtils.EMPTY : inst.getUnderlyingSecurityID().getValue()) : CfetsUtils.EMPTY);
            securityInfo.setSecurityName(inst.isSetUnderlyingSymbol() ? (inst.getUnderlyingSymbol().getValue() == null ? CfetsUtils.EMPTY : inst.getUnderlyingSymbol().getValue()) : CfetsUtils.EMPTY);
            securityInfo.setFaceAmt(new BigDecimal(inst.isSetUnderlyingQty() ? (inst.getUnderlyingQty().getValue() == null ? "0" : inst.getUnderlyingQty().getValue()) : "0"));
            securityInfo.setcPrice(new BigDecimal(inst.isSetUnderlyingPx() ? (inst.getUnderlyingPx().getValue() == null ? "0" : inst.getUnderlyingPx().getValue()) : "0"));
            securityInfo.setdPrice(new BigDecimal(inst.isSetUnderlyingDirtyPrice() ? (inst.getUnderlyingDirtyPrice().getValue() == null ? "0" : inst.getUnderlyingDirtyPrice().getValue()) : "0"));
            securityInfo.setAccruedInt(new BigDecimal(inst.isSetUnderlyingAccruedInterestAmt() ? (inst.getUnderlyingAccruedInterestAmt().getValue() == null ? "0" : inst.getUnderlyingAccruedInterestAmt().getValue()) : "0"));
            int NoUnderlyingStips = inst.isSetNoUnderlyingStips() ? inst.getNoUnderlyingStips().getValue() : 0;
            UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips noUnderlyingStips2 = new UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips();
            String type = null;
            for (int j = 1; j <= NoUnderlyingStips; j++) {
                inst.getGroup(j, noUnderlyingStips2);
                type = noUnderlyingStips2.isSetUnderlyingStipType() ? (noUnderlyingStips2.getUnderlyingStipType().getValue() == null ? CfetsUtils.EMPTY : noUnderlyingStips2.getUnderlyingStipType().getValue()) : CfetsUtils.EMPTY;
                if ("StrikeYield".equalsIgnoreCase(type)) {
                    securityInfo.setStrikeYield(new BigDecimal(noUnderlyingStips2.isSetUnderlyingStipValue() ? (noUnderlyingStips2.getUnderlyingStipValue().getValue() == null ? "0" : noUnderlyingStips2.getUnderlyingStipValue().getValue()) : "0"));
                } else if ("Yield2".equalsIgnoreCase(type)) {
                    securityInfo.setYield2(new BigDecimal(noUnderlyingStips2.isSetUnderlyingStipValue() ? (noUnderlyingStips2.getUnderlyingStipValue().getValue() == null ? "0" : noUnderlyingStips2.getUnderlyingStipValue().getValue()) : "0"));
                }
            }
        }
    }


}
