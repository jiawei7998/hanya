package com.singlee.financial.cfets.Util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Properties;

public class TrdSocketClient {
	
	public static void main(String[] args) {
		PrintWriter w = null;
		Socket s = null;
		BufferedReader in = null;
		try {
			Properties p = new Properties();
			p.load(TrdSocketClient.class.getClassLoader().getResourceAsStream("cfg//client.cfg"));
			int port = Integer.parseInt(p.getProperty("cfets.cstp.rmb.port"));
			s = new Socket("127.0.0.1", port);
			s.setSoTimeout(60 * 60 * 1000);
			System.out.println("======>连接服务成功");
			w = new PrintWriter(s.getOutputStream());
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			w.println("TRADES");
			w.flush();
			System.out.println("======>正在处理......");
			String ret = in.readLine();
			if(ret != null && ret.contains("成功")){
				String path = ret.substring(ret.indexOf("[") + 1, ret.indexOf("]"));
				Runtime.getRuntime().exec("cmd /c start " + path);
			}
			System.out.println("=======>处理完成：" + ret);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(w != null) {w.close();}
				
				if(in != null) {in.close();}
				
				if(s != null) {s.close();}
				
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}
