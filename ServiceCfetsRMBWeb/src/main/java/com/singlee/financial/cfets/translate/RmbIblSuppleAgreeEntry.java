package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.InterBankLoansAddBean;
import com.singlee.financial.pojo.component.SettPartyEnum;
import imix.imix10.SupplementalReport;
import imix.imix10.component.UndInstrmtGrp;
import imix.imix10.component.UnderlyingInstrument;

import java.math.BigDecimal;

/**
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbIblSuppleAgreeEntry extends RmbSettleEntry implements ImixRmbMessageHandler<SupplementalReport, InterBankLoansAddBean> {

    /**
     * 同业借款附加协议
     *
     * @param sReport
     * @param interBankLoansAddBean
     * @throws Exception
     */
    public void convert(SupplementalReport sReport, InterBankLoansAddBean interBankLoansAddBean, String bankId) throws Exception {
        // 设置本方机构及清算信息
        convert(sReport, bankId, SettPartyEnum.TheParty, interBankLoansAddBean.getInstitutionInfo(), interBankLoansAddBean.getSettInfo());
        // 设置对手方机构及清算信息
        convert(sReport, bankId, SettPartyEnum.CounterParty, interBankLoansAddBean.getContraPatryInstitutionInfo(), interBankLoansAddBean.getContraPartySettInfo());

        interBankLoansAddBean.setTradeTime(sReport.isSetTradeTime() ? sReport.getTradeTime().getValue() : CfetsUtils.EMPTY);// 成交时间
        interBankLoansAddBean.setDealDate(sReport.isSetTradeDate() ? sReport.getTradeDate().getDateValue() : null); //成交日期
        interBankLoansAddBean.setDealMode(convert(sReport.isSetTradeMethod() ? sReport.getTradeMethod() : null)); //交易方式
        interBankLoansAddBean.setStatus(convert(sReport.isSetExecType() ? sReport.getExecType() : null));// 交易状态

        interBankLoansAddBean.setExecId(sReport.isSetExecID() ? sReport.getExecID().getValue() : CfetsUtils.EMPTY);//成交编号
        interBankLoansAddBean.setDealMode(convert(sReport.isSetTradeMethod() ? sReport.getTradeMethod() : null));// 交易方式
        interBankLoansAddBean.setStatus(convert(sReport.isSetExecType() ? sReport.getExecType() : null));// 交易状态
        interBankLoansAddBean.setDealDate(sReport.isSetTradeDate() ? sReport.getTradeDate().getDateValue() : null);// 成交日期
        interBankLoansAddBean.setTradeTime(sReport.isSetTradeTime() ? sReport.getTradeTime().getValue() : CfetsUtils.EMPTY);// 成交时间
        interBankLoansAddBean.setSupplType(sReport.isSetSupplType() ? sReport.getSupplType().getValue() : CfetsUtils.EMPTY);//附加协议类型
        interBankLoansAddBean.setSupplTransType(sReport.isSetSupplTransType() ? sReport.getSupplTransType().getValue() : CfetsUtils.EMPTY);//操作类型
        interBankLoansAddBean.setPs(convert(sReport.isSetSide() ? sReport.getSide() : null, interBankLoansAddBean.getSettInfo().getMarkerTakerRole())); // 交易方向
        interBankLoansAddBean.setKindId(sReport.isSetSecurityID() ? sReport.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码
        interBankLoansAddBean.setTenor(sReport.isSetSecurityTermString() ? sReport.getSecurityTermString().getValue() : CfetsUtils.EMPTY); // 借款期限
        interBankLoansAddBean.setHoldingDays(sReport.isSetCashHoldingDays() ? sReport.getCashHoldingDays().getUnionTypeValue() : CfetsUtils.EMPTY); // 实际占款天数
        interBankLoansAddBean.setRate(sReport.isSetPrice() ? new BigDecimal(sReport.getPrice().getValue()) : null);// 利率
        interBankLoansAddBean.setAmt(sReport.isSetOrderQty() ? new BigDecimal(sReport.getOrderQty().getValue()) : null);//借款金额
        interBankLoansAddBean.setPayFrequency(convert(sReport.isSetCouponPaymentFrequency() ? sReport.getCouponPaymentFrequency() : null));//付息频率
        interBankLoansAddBean.setFirstPayDate(sReport.isSetCouponPaymentDate() ? sReport.getCouponPaymentDate().getValue() : CfetsUtils.EMPTY);//首次付息日
        interBankLoansAddBean.setInterestAmt(sReport.isSetAccruedInterestTotalAmt() ? new BigDecimal(sReport.getAccruedInterestTotalAmt().getValue()) : null); // 应计利息总金额
        interBankLoansAddBean.setMaturityAmt(sReport.isSetSettlCurrAmt2() ? new BigDecimal(sReport.getSettlCurrAmt2().getValue()) : null); // 到期还款金额
        interBankLoansAddBean.setValueDate(sReport.isSetSettlDate() ? sReport.getSettlDate().getDateValue() : null); // 首次结算日
        interBankLoansAddBean.setMaturityDate(sReport.isSetSettlDate2() ? sReport.getSettlDate2().getDateValue() : null);// 到期还款日
        interBankLoansAddBean.setText(sReport.isSetText() ? sReport.getText().getValue() : CfetsUtils.EMPTY);// 补充条款
        interBankLoansAddBean.setSupplementAttachmentIndicator(sReport.isSetSupplementAttachmentIndicator() ? String.valueOf(sReport.getSupplementAttachmentIndicator().getValue()) : CfetsUtils.EMPTY);// 有无补充条款
        interBankLoansAddBean.setDataCategoryIndicator(sReport.isSetDataCategoryIndicator() ? sReport.getDataCategoryIndicator().getValue() : null);//数据类型标识
        interBankLoansAddBean.setDcie(convert(sReport.isSetDataCategoryIndicator() ? sReport.getDataCategoryIndicator() : null)); //数据类型标识 Excel导出使用

        UndInstrmtGrp undInstrmtGrp = sReport.getUndInstrmtGrp();
        UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
        int NoUnderlyings = undInstrmtGrp.isSetNoUnderlyings() ? undInstrmtGrp.getNoUnderlyings().getValue() : 0;
        UnderlyingInstrument inst = new UnderlyingInstrument();
        for (int i = 1; i <= NoUnderlyings; i++) {
            sReport.getGroup(i, noUnderlyings);
            inst = noUnderlyings.getUnderlyingInstrument();
            //付息日 按期付息的时候有值
            interBankLoansAddBean.setIntPayDate(inst.isSetUnderlyingCouponPaymentDate() ? (inst.getUnderlyingCouponPaymentDate().getValue() == null ? CfetsUtils.EMPTY : inst.getUnderlyingCouponPaymentDate().getValue()) : CfetsUtils.EMPTY);
            //应计利息 按期付息的时候有值
            interBankLoansAddBean.setAccruedIntAmt(new BigDecimal(inst.isSetUnderlyingAccruedInterestAmt() ? (inst.getUnderlyingAccruedInterestAmt().getValue() == null ? "0" : inst.getUnderlyingAccruedInterestAmt().getValue()) : "0"));
        }

        int NoAdvanceInfos = sReport.isSetNoAdvanceInfos() ? sReport.getNoAdvanceInfos().getValue() : 0;
        SupplementalReport.NoAdvanceInfos noAdvanceInfos = new SupplementalReport.NoAdvanceInfos();
        for (int i = 1; i <= NoAdvanceInfos; i++) {
            sReport.getGroup(i, noAdvanceInfos);
            interBankLoansAddBean.setAdvanceId(noAdvanceInfos.isSetAdvanceID() ? noAdvanceInfos.getAdvanceID().getValue() : CfetsUtils.EMPTY);  //提前还款编号
            interBankLoansAddBean.setAdvanceInfoType(noAdvanceInfos.isSetAdvanceInfoType() ? noAdvanceInfos.getAdvanceInfoType().getValue() : CfetsUtils.EMPTY);
            interBankLoansAddBean.setAdvanceStatus(noAdvanceInfos.isSetAdvanceStatus() ? noAdvanceInfos.getAdvanceStatus().getValue() : CfetsUtils.EMPTY);//提前还款状态
            interBankLoansAddBean.setAdvanveBusinessTime(noAdvanceInfos.isSetAdvanceBusinessTime() ? noAdvanceInfos.getAdvanceBusinessTime().getValue() : CfetsUtils.EMPTY);//提前还款申请业务发生时间
            interBankLoansAddBean.setAdvanceDate(noAdvanceInfos.isSetAdvanceDate() ? noAdvanceInfos.getAdvanceDate().getValue() : CfetsUtils.EMPTY); //提前还款日
            interBankLoansAddBean.setAdvanceAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceAmt() ? noAdvanceInfos.getAdvanceAmt().getValue() : "0")); //提前还款本金
            interBankLoansAddBean.setAdvanceCompensationType(noAdvanceInfos.isSetAdvanceCompensationType() ? noAdvanceInfos.getAdvanceCompensationType().getValue() : CfetsUtils.EMPTY);//提前还款补偿类型
            interBankLoansAddBean.setAdvancePrice(new BigDecimal(noAdvanceInfos.isSetAdvancePrice() ? noAdvanceInfos.getAdvancePrice().getValue() : "0"));//提前还款利率
            interBankLoansAddBean.setAdvanceLastQty(new BigDecimal(noAdvanceInfos.isSetAdvanceLastQty() ? noAdvanceInfos.getAdvanceLastQty().getValue() : "0")); //提前还款补偿金额
            interBankLoansAddBean.setAdvanceInterestAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceInterestAmt() ? noAdvanceInfos.getAdvanceInterestAmt().getValue() : "0")); //提前还款应计利息
            interBankLoansAddBean.setAdvanceSettlAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceSettlAmt() ? noAdvanceInfos.getAdvanceSettlAmt().getValue() : "0")); //提前还款结算金额
            interBankLoansAddBean.setAdvanceLeavesAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceLeavesAmt() ? noAdvanceInfos.getAdvanceLeavesAmt().getValue() : "0")); //提前还款后剩余存款本金
            interBankLoansAddBean.setAdvanceExpireAmt(new BigDecimal(noAdvanceInfos.isSetAdvanceExpireAmt() ? noAdvanceInfos.getAdvanceExpireAmt().getValue() : "0")); //提前还款后到期支取金额
            interBankLoansAddBean.setAdvanceClause(noAdvanceInfos.isSetAdvanceClause() ? noAdvanceInfos.getAdvanceClause().getValue() : CfetsUtils.EMPTY);//提前还款条款
            interBankLoansAddBean.setAdvanceAttachmentIndicator(noAdvanceInfos.isSetAdvanceAttachmentIndicator() ? String.valueOf(noAdvanceInfos.getAdvanceAttachmentIndicator().getValue()) : CfetsUtils.EMPTY);
            interBankLoansAddBean.setAdvanceTraderName1(noAdvanceInfos.isSetAdvanceTraderName1() ? noAdvanceInfos.getAdvanceTraderName1().getValue() : CfetsUtils.EMPTY);//提前还款借入方交易员
            interBankLoansAddBean.setAdvanceTraderName2(noAdvanceInfos.isSetAdvanceTraderName2() ? noAdvanceInfos.getAdvanceTraderName2().getValue() : CfetsUtils.EMPTY); //提前还款借出方交易员
        }
    }
}
