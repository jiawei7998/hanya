package com.singlee.financial.cfets.rmb;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.translate.*;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.*;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.Message;
import imix.field.ExecID;
import imix.field.MarketIndicator;
import imix.field.SecurityID;
import imix.imix10.CommissionData;
import imix.imix10.ExecutionReport;
import imix.imix10.SecurityDefinition;
import imix.imix10.SupplementalReport;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * CFETS报文对象处理实体类
 *
 * @author chenxh
 */

public class ImixRmbMessageProcessImpl {

    //申明配置文件
    private ConfigBean configBean;
    //日志申明
    private static org.apache.log4j.Logger logger = Logger.getLogger(ImixRmbMessageProcessImpl.class);

    //初始化方法
    public ImixRmbMessageProcessImpl(ConfigBean configBean) {
        this.configBean = configBean;
    }


    /**
     * 消息转换【如果有新的报文需要转换】
     *
     * @param message
     * @return
     * @throws Exception
     */
    public TradeBaseBean parseMsg(Message message) throws Exception {
        TradeBaseBean tradeBaseBean = null;
        if (message instanceof ExecutionReport) {
            //成交报文
            ExecutionReport executionReport = (ExecutionReport) message;
            tradeBaseBean = parseMsg(executionReport, executionReport.isSetMarketIndicator() ? executionReport.getMarketIndicator().getValue() : "");
        } else if (message instanceof SecurityDefinition) {
            //工具发行
            SecurityDefinition executionReport = (SecurityDefinition) message;
            tradeBaseBean = parseMsg(executionReport);
        } else if (message instanceof CommissionData) {
            //缴款信息
            CommissionData commissionData = (CommissionData) message;
            tradeBaseBean = parseMsg(commissionData);
        } else if (message instanceof SupplementalReport) {
            //附加协议
            SupplementalReport supplementalReport = (SupplementalReport) message;
            tradeBaseBean = parseMsg(supplementalReport, supplementalReport.isSetMarketIndicator() ? supplementalReport.getMarketIndicator().getValue() : "");
        }
        return tradeBaseBean;
    }


    /***
     * 解析消息-执行报告消息
     * @param message
     * @param marketIndicator
     * @return
     * @throws Exception
     */
    public TradeBaseBean parseMsg(ExecutionReport message, String marketIndicator) throws Exception {
        TradeBaseBean tradeBaseBean = null;
        ImixRmbMessageHandler imixRmbMessageHandler = null;
        switch (marketIndicator) {
            //信用拆借
            case MarketIndicator.INTER_BANK_OFFERING:
                tradeBaseBean = new DepositsAndLoansBean();
                imixRmbMessageHandler = new RmbIboEntry();
                break;
            //现券买卖
            case MarketIndicator.CASH_BOND:
                tradeBaseBean = new FixedIncomeBean();
                imixRmbMessageHandler = new RmbCbtEntry();
                break;
            //债券远期
            case MarketIndicator.BOND_FORWARD:
                tradeBaseBean = new BfdBean();
                imixRmbMessageHandler = new RmbBfdEntry();
                break;
            //标准债券远期
            case MarketIndicator.STANDARD_BOND_FORWARD:
                tradeBaseBean = new StandardBfdBean();
                imixRmbMessageHandler = new RmbBfdStandardEntry();
                break;
            //买断式回购
            case MarketIndicator.OUTRIGHT_REPO:
                tradeBaseBean = new RepoOrBean();
                imixRmbMessageHandler = new RmbOrEntry();
                break;
            //质押式回购
            case MarketIndicator.COLLATERAL_REPO:
                tradeBaseBean = new RepoCrBean();
                imixRmbMessageHandler = new RmbCrEntry();
                break;
            //利率互换
            case MarketIndicator.INTEREST_RATE_SWAP:
                tradeBaseBean = new SwapIrsBean();
                imixRmbMessageHandler = new RmbIrsEntry();
                break;
            //债券借贷
            case MarketIndicator.SECURITY_LENDING:
                tradeBaseBean = new SecurityLendingBean();
                imixRmbMessageHandler = new RmbSlEntry();
                break;
            //同业存款
            case MarketIndicator.IBD:
                tradeBaseBean = new InterBankDepositBean();
                imixRmbMessageHandler = new RmbIbdEntry();
                break;
            //同业借款
            case MarketIndicator.INTER_BANK_BORROWING:
                tradeBaseBean = new InterBankLoansBean();
                imixRmbMessageHandler = new RmbIblEntry();
                break;
            default:
                String cstpRmbFlag = configBean.getCstpRmbFlag() == null ? "" : configBean.getCstpRmbFlag();
                if ("Y".equals(cstpRmbFlag)) {
                    //验收时打开
                    String execID = message.isSetField(ExecID.FIELD) ? message.getField(new ExecID()).getValue() : message.getField(new SecurityID()).getValue();
                    Map<String, String> map1 = new HashMap<String, String>();
                    map1.put("marketIndicator", marketIndicator);
                    map1.put("execId", execID);
                    map1.put("msg", "系统暂未支持的市场标识(marketIndicator):" + marketIndicator + ",不解析此报文");
                    saveExcelError(map1);
                }
                throw new Exception("系统暂未支持的市场标识(marketIndicator):" + marketIndicator + ",不解析此报文");
        }
        //解析报文
        imixRmbMessageHandler.convert(message, tradeBaseBean, configBean.getBankId());
        return tradeBaseBean;
    }


    /***
     * 解析消息-工具发行结果消息查询
     * @param message
     * @return
     * @throws Exception
     */
    public TradeBaseBean parseMsg(SecurityDefinition message) throws Exception {
        //工具发行处理报文,目前全部都是一个报文
        TradeBaseBean tradeBaseBean = new IbncdBean();
        ImixRmbMessageHandler imixRmbMessageHandler = new RmbInstrumentsIssueEntry();
        //解析报文,需要传入21机构码,否则发行工具无法判断
        imixRmbMessageHandler.convert(message, tradeBaseBean, configBean.getInstId());
        return tradeBaseBean;
    }

    /***
     * 解析消息-缴款信息查询
     * @param message
     * @return
     * @throws Exception
     */
    public TradeBaseBean parseMsg(CommissionData message) throws Exception {
        //工具发行处理报文,目前全部都是一个报文
        TradeBaseBean tradeBaseBean = new IbncdCommissionBean();
        ImixRmbMessageHandler imixRmbMessageHandler = new RmbInstrumentsPaymentEntry();
        //解析报文,需要传入21机构码,否则发行工具无法判断
        imixRmbMessageHandler.convert(message, tradeBaseBean, configBean.getInstId());
        return tradeBaseBean;
    }

    /***
     * 解析消息- 同业存款、同业借款补充协议
     * @param message
     * @param marketIndicator
     * @return
     * @throws Exception
     */
    public TradeBaseBean parseMsg(SupplementalReport message, String marketIndicator) throws Exception {
        TradeBaseBean tradeBaseBean = null;
        ImixRmbMessageHandler imixRmbMessageHandler = null;
        switch (marketIndicator) {
            case MarketIndicator.IBD:
                tradeBaseBean = new InterBankDepositAddBean();
                imixRmbMessageHandler = new RmbIbdSuppleAgreeEntry();
                break;
            case MarketIndicator.INTER_BANK_BORROWING:
                tradeBaseBean = new InterBankLoansAddBean();
                imixRmbMessageHandler = new RmbIblSuppleAgreeEntry();
                break;
            default:
                String cstpRmbFlag = configBean.getCstpRmbFlag() == null ? "" : configBean.getCstpRmbFlag();
                if ("Y".equals(cstpRmbFlag)) {
                    //验收时打开
                    String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
                    Map<String, String> map1 = new HashMap<String, String>();
                    map1.put("marketIndicator", marketIndicator);
                    map1.put("execId", execid);
                    map1.put("msg", "系统暂未支持的市场标识(marketIndicator):" + marketIndicator + ",不解析此报文");
                    saveExcelError(map1);
                }
                throw new Exception("系统暂未支持的市场标识(marketIndicator):" + marketIndicator + ",不解析此报文");
        }
        //解析报文
        imixRmbMessageHandler.convert(message, tradeBaseBean, configBean.getBankId());
        return tradeBaseBean;
    }


    /**
     * 发送CFETS业务对象对资金前置系统
     */
    void sendToCapital(final CfetsPackMsgBean cfetsPackMsg, final TradeBaseBean tradeBean, ImixRmbCfetsApiService imixRmbCfetsApiService) {
        new Thread() {
            @Override
            public void run() {
                // 资金前置返回对象
                RetBean retBean = null;
                String saveFilePath = null;
                try {
                    logger.debug("开始发送CFETS交易[" + tradeBean.getExecId() + "]到资金前置系统!");

                    retBean = sendMsg(tradeBean, cfetsPackMsg, imixRmbCfetsApiService);

                    logger.debug("资金前置系统返回对象:" + retBean);

                } catch (Exception e) {
                    // 异常时设置反回状态为失败
                    retBean = new RetBean();
                    retBean.setRetStatus(RetStatusEnum.F);
                    logger.error("发送CFETS交易[" + tradeBean.getExecId() + "]到资金前置系统异常!", e);
                }// end try catch

                try {
                    // 以txt形式保存CFETS报文对象
                    if (null == retBean || retBean.getRetStatus().equals(RetStatusEnum.F)) {
                        // 处理失败的记录保存路径
                        saveFilePath = configBean.getSaveFilePath() + File.separator + ConfigBean.errDataPath + File.separator + String.format("%tF", new Date()) + File.separator
                                + tradeBean.getExecId() + ConfigBean.txtSuffix;
                        // 如果处理失败保存到失败目录
                        CfetsUtils.saveFile(cfetsPackMsg.getPackmsg(), saveFilePath, configBean.getCharset());
                    }
                } catch (Exception e) {
                    logger.error("保存发送异常交易[" + tradeBean.getExecId() + "]到交易异常目录!", e);
                }// end try catch
            }// end run
        }.start();
    }

    /****
     * 发送交易方法，可以被其他类调用
     * @param tradeBean
     * @param cfetsPackMsg
     * @return
     * @throws Exception
     */
    public RetBean sendMsg(TradeBaseBean tradeBean, CfetsPackMsgBean cfetsPackMsg, ImixRmbCfetsApiService imixRmbCfetsApiService) throws Exception {
        RetBean retBean = null;
        if (tradeBean instanceof FixedIncomeBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixSecur().send((FixedIncomeBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof DepositsAndLoansBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixMM().send((DepositsAndLoansBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof SwapIrsBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixSwap().send((SwapIrsBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof RepoCrBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixRepo().send((RepoCrBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof RepoOrBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixRepo().send((RepoOrBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof SecurityLendingBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixLending().send((SecurityLendingBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof BfdBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixSecur().send((BfdBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof StandardBfdBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixSecur().send((StandardBfdBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof InterBankDepositBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixMM().send((InterBankDepositBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof InterBankLoansBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixMM().send((InterBankLoansBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof IbncdBean && !(tradeBean instanceof IbncdCommissionBean)) {//由于缴款信息是存单发行bean的子类，此处用class判断
            retBean = imixRmbCfetsApiService.getCfetsImixSecur().send((IbncdBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof IbncdCommissionBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixSecur().send((IbncdCommissionBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof SLFBean) {
            retBean = imixRmbCfetsApiService.getCfetsImixRepo().send((SLFBean) tradeBean, cfetsPackMsg);
        } else {
            logger.warn("参数类型不支持!");
            throw new Exception("参数类型不支持!");
        }
        return retBean;
    }

    /**
     * 验收保存
     */
    public void saveExcel(String marketIndicator, TradeBaseBean tradeBean) {
        //验收时打开
        String cstpRmbFlag = configBean.getCstpRmbFlag() == null ? "" : configBean.getCstpRmbFlag();
        if ("Y".equals(cstpRmbFlag)) {
            try {
                String basePath = configBean.getCstpFxUrl() == null ? "" : configBean.getCstpFxUrl();
                URL weburl = ImixRmbMessageProcess.class.getClassLoader().getResource("WEB-INF/classes");
                URL url = ImixRmbMessageProcess.class.getClassLoader().getResource("");
                String path = (null == weburl ? url.getPath() : weburl.getPath());

                ImixRmbFeedBack.addDataExcelAll(marketIndicator, tradeBean, path + basePath);
            } catch (Exception e) {
                logger.error("保存excel失败，单号为【" + tradeBean.getExecId() + "】");
            }
        }
    }

    /**
     * 验收前校验部分阈值
     */
    public void beforeCheck(ExecutionReport message, String marketIndicator, int marketCode, String execid) {
        //验收时打开
        String cstpRmbFlag = configBean.getCstpRmbFlag() == null ? "" : configBean.getCstpRmbFlag();
        if ("Y".equals(cstpRmbFlag)) {
            try {
                ImixRmbMessageProcessCheck imixFxMessageProcessCheck = new ImixRmbMessageProcessCheck();
                Map<String, String> map = new HashMap<String, String>();
                map.put("marketIndicator", marketIndicator);
                Map<String, String> checkMap = imixFxMessageProcessCheck.checkFieldAll(message, map);
                if ("1".equals(checkMap.get("controlFlag")) && "1".equals(checkMap.get("promptFlag"))) {
                    //即控制校验和提示校验全部通过，不用打印日志
                } else {
                    Map<String, String> map1 = new HashMap<String, String>();
                    if ("".equals(marketIndicator)) {
                        map1.put("marketCode", String.valueOf(marketCode));
                    } else {
                        map1.put("marketIndicator", marketIndicator);
                    }
                    map1.put("execId", execid);
                    map1.put("msg", checkMap.get("msg"));
                    saveExcelError(map1);
                    logger.debug("外汇即期业务对象：execid=[" + execid + "]的交易报文存在以下问题：" + checkMap.get("msg"));
                }
            } catch (Exception e) {
                logger.error("校验失败，单号为【" + execid + "】");
            }
        }
    }

    /**
     * 下行转换前校验有问题时保存
     *
     * @param map 包含市场类型，交易编号，错误信息
     */
    public void saveExcelError(Map<String, String> map) {
        //验收时打开
        String cstpRmbFlag = configBean.getCstpRmbFlag() == null ? "" : configBean.getCstpRmbFlag();
        if ("Y".equals(cstpRmbFlag)) {
            try {
                String basePath = configBean.getCstpFxUrl() == null ? "" : configBean.getCstpFxUrl();
                map.put("basePath", basePath);
                ImixRmbFeedBack.addDataExcelAllError(map);
            } catch (Exception e) {
                logger.error("保存excel失败，单号为【" + map.get("execId") + "】");
            }
        }
    }
}
