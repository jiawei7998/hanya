package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.RepoCrBean;
import com.singlee.financial.pojo.trade.SecurityCustodianBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;
import imix.imix10.component.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 质押式回购
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbCrEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport,RepoCrBean> {
    /**
     * 转换本币质押式回购 marketIndicator=9
     *
     * @param message
     * @param repoCrBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, RepoCrBean repoCrBean, String bankId) throws FieldNotFound, FieldConvertError {
        setBaseInfo(message, bankId, repoCrBean, repoCrBean.getSettInfo(), repoCrBean.getContraPartySettInfo());
        // 交易模式
        repoCrBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        //回购方式
        repoCrBean.setRepoMethod(convert(message.isSetRepoMethod() ? message.getRepoMethod() : null));

        repoCrBean.setRate(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null); // 回购利率
        repoCrBean.setTotalFaceAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null); // 券面总额
        repoCrBean.getAmtDeail().setAmt(message.isSetTradeCashAmt() ? new BigDecimal(message.getTradeCashAmt().getValue()) : null); // 交易金额
        repoCrBean.getAmtDeail().setInterestTotalAmt(message.isSetAccruedInterestTotalAmt() ? new BigDecimal(message.getAccruedInterestTotalAmt().getValue()) : null); // 应计利息
        repoCrBean.getAmtDeail().setSettlCcy(message.isSetSettlCurrency() ? message.getSettlCurrency().getValue() : CfetsUtils.EMPTY); // 结算币种

        repoCrBean.getComSecuritySettAmt().setSettAmt(message.isSetTradeCashAmt() ? new BigDecimal(message.getTradeCashAmt().getValue()) : null); // 首次结算金额
        repoCrBean.getComSecuritySettAmt().setSettDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null); // 首次结算日
        repoCrBean.getComSecuritySettAmt().setSettType(message.isSetDeliveryType() ? message.getDeliveryType().getUnionTypeValue() : CfetsUtils.EMPTY); // 首次结算方式

        repoCrBean.getMatSecuritySettAmt().setSettAmt(message.isSetSettlCurrAmt2() ? new BigDecimal(message.getSettlCurrAmt2().getValue()) : null); // 到期结算金额
        repoCrBean.getMatSecuritySettAmt().setSettDate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null); // 到期结算日
        repoCrBean.getMatSecuritySettAmt().setSettType(message.isSetDeliveryType2() ? message.getDeliveryType2().getUnionTypeValue() : CfetsUtils.EMPTY); // 到期结算方式

        repoCrBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态
        repoCrBean.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码

        repoCrBean.setTerm(message.isSetTradeLimitDays() ? message.getTradeLimitDays().getValue() : CfetsUtils.EMPTY);//回购期限
        repoCrBean.setOccupancyDays(String.valueOf(message.isSetCashHoldingDays() ? message.getCashHoldingDays().getValue() : CfetsUtils.EMPTY));//实际占款天数

        List<SecurityInfoBean> securInfoList = new ArrayList<SecurityInfoBean>();
        UndInstrmtGrp undInstrmtGrp = message.getUndInstrmtGrp();
        int NoUnderlyings = undInstrmtGrp.isSetNoUnderlyings() ? undInstrmtGrp.getNoUnderlyings().getValue() : 0;
        for (int i = 1; i <= NoUnderlyings; i++) {
            UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
            undInstrmtGrp.getGroup(i, noUnderlyings);
            UnderlyingInstrument underlyingInstrument = noUnderlyings.getUnderlyingInstrument();

            SecurityInfoBean securInfo = new SecurityInfoBean();
            securInfo.setSecurityId(underlyingInstrument.isSetUnderlyingSecurityID() ? underlyingInstrument.getUnderlyingSecurityID().getValue() : CfetsUtils.EMPTY); // 债券代码
            securInfo.setSecurityName(underlyingInstrument.isSetUnderlyingSymbol() ? underlyingInstrument.getUnderlyingSymbol().getValue() : CfetsUtils.EMPTY); // 债券名称
            securInfo.setFaceAmt(underlyingInstrument.isSetUnderlyingQty() ? new BigDecimal(underlyingInstrument.getUnderlyingQty().getValue()) : null); // 债券金额

            UnderlyingStipulations underlyingStipulations = noUnderlyings.getUnderlyingStipulations();
            int NoUnderlyingStips = underlyingStipulations.isSetNoUnderlyingStips() ? underlyingStipulations.getNoUnderlyingStips().getValue() : 0;
            for (int j = 1; j <= NoUnderlyingStips; j++) {
                UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips noUnderlyingStips = new UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips();
                underlyingStipulations.getGroup(j, noUnderlyingStips);
                if ("Haircut".equals(noUnderlyingStips.getUnderlyingStipType().getValue())) {
                    securInfo.setHaircut(noUnderlyingStips.isSetUnderlyingStipValue() ? new BigDecimal(noUnderlyingStips.getUnderlyingStipValue().getValue()) : null); // 折算比例
                }
            }

            UndlyInstrumentParties undlyInstrumentParties = noUnderlyings.getUndlyInstrumentParties();
            int NoUndlyInstrumentParties = undlyInstrumentParties.isSetNoUndlyInstrumentParties() ? undlyInstrumentParties.getNoUndlyInstrumentParties().getValue() : 0;
            for (int k = 1; k <= NoUndlyInstrumentParties; k++) {
                UndInstrmtGrp.NoUnderlyings.NoUndlyInstrumentParties noUndlyInstrumentParties = new UndInstrmtGrp.NoUnderlyings.NoUndlyInstrumentParties();
                undlyInstrumentParties.getGroup(k, noUndlyInstrumentParties);
                if (noUndlyInstrumentParties.getUnderlyingInstrumentPartyRole().getValue() == 105) {//105表示托管机构
                    System.out.println("进入");
                    for (int m = 1; m <= noUndlyInstrumentParties.getNoUndlyInstrumentPartySubIDs().getValue(); m++) {
                        UndInstrmtGrp.NoUnderlyings.NoUndlyInstrumentParties.NoUndlyInstrumentPartySubIDs noUndlyInstrumentPartySubIDs = new UndInstrmtGrp.NoUnderlyings.NoUndlyInstrumentParties.NoUndlyInstrumentPartySubIDs();
                        noUndlyInstrumentParties.getGroup(m, noUndlyInstrumentPartySubIDs);
                        if (noUndlyInstrumentPartySubIDs.getUnderlyingInstrumentPartySubIDType().getValue() == 111) {//11表示托管机构名称
                            securInfo.setSecurityAccName(noUndlyInstrumentPartySubIDs.isSetUnderlyingInstrumentPartySubID() ? noUndlyInstrumentPartySubIDs.getUnderlyingInstrumentPartySubID().getValue() : CfetsUtils.EMPTY);
                        }
                    }
                }
            }
            securInfoList.add(securInfo);
        }
        repoCrBean.setSecursInfo(securInfoList);

        List<SecurityCustodianBean> securCustodianList = new ArrayList<SecurityCustodianBean>();
        CrossCustodianGrp crossCustodianGrp = message.getCrossCustodianGrp();
        int NoCrossCustodians = crossCustodianGrp.isSetNoCrossCustodians() ? crossCustodianGrp.getNoCrossCustodians().getValue() : 0;
        for (int i = 1; i <= NoCrossCustodians; i++) {
            CrossCustodianGrp.NoCrossCustodians noCrossCustodians = new CrossCustodianGrp.NoCrossCustodians();
            crossCustodianGrp.getGroup(i, noCrossCustodians);

            SecurityCustodianBean securCustodian = new SecurityCustodianBean();
            //跨托管机构名称
            securCustodian.setCustodianInstitutionName(noCrossCustodians.isSetCrossCustodianInstitutionName() ? noCrossCustodians.getCrossCustodianInstitutionName().getValue() : CfetsUtils.EMPTY);
            //跨托管交易金额
            securCustodian.setCustodianTradeAmt(new BigDecimal(noCrossCustodians.isSetCrossCustodianTradeAmt() ? noCrossCustodians.getCrossCustodianTradeAmt().getValue() : "0"));
            //跨托管回购利息
            securCustodian.setCustodianAccruedInterestAmt(new BigDecimal(noCrossCustodians.isSetCrossCustodianAccruedInterestAmt() ? noCrossCustodians.getCrossCustodianAccruedInterestAmt().getValue() : "0"));
            //跨托管到期结算金额
            securCustodian.setCustodianSettlCurrAmt2(new BigDecimal(noCrossCustodians.isSetCrossCustodianSettlCurrAmt2() ? noCrossCustodians.getCrossCustodianSettlCurrAmt2().getValue() : "0"));
            //跨托管券面总额
            securCustodian.setCustodianQty(new BigDecimal(noCrossCustodians.isSetCrossCustodianQty() ? noCrossCustodians.getCrossCustodianQty().getValue() : "0"));

            securCustodianList.add(securCustodian);
        }
        repoCrBean.setSecursCustodian(securCustodianList);
    }

}
