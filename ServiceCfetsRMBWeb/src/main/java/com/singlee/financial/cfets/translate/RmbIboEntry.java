package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.DepositsAndLoansBean;
import com.singlee.financial.pojo.component.SettPartyEnum;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;

/**
 * 信用拆借
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbIboEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, DepositsAndLoansBean> {

    /**
     * 转换本币拆借交易 marketIndicator=1
     *
     * @param message
     * @param cfetsMm
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, DepositsAndLoansBean cfetsMm, String bankId) throws FieldNotFound, FieldConvertError {
        // 设置本方机构及清算信息
        convert(message, bankId, SettPartyEnum.TheParty, cfetsMm.getInstitutionInfo(), cfetsMm.getSettInfo());
        // 设置对手方机构及清算信息
        convert(message, bankId, SettPartyEnum.CounterParty, cfetsMm.getContraPatryInstitutionInfo(), cfetsMm.getContraPartySettInfo());
        // 交易模式（10317交易方式：eg:询价）
        cfetsMm.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));

        cfetsMm.setExecId(message.isSetExecID() ? message.getExecID().getValue() : CfetsUtils.EMPTY); // CFETS成交编号:17域
        cfetsMm.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);// 交易日期：75
        cfetsMm.setTradeTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : CfetsUtils.EMPTY);// 交易成交时间：10318
        cfetsMm.setPs(convert(message.isSetSide() ? message.getSide() : null, cfetsMm.getSettInfo().getMarkerTakerRole())); // 交易方向54

        cfetsMm.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态150
        cfetsMm.setTenor(message.isSetTradeLimitDays() ? message.getTradeLimitDays().getValue() : CfetsUtils.EMPTY); // 期限10316,单位：天，整数
        cfetsMm.setRate(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null);// 利率44
        cfetsMm.setAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null);// 交易币种金额32
        cfetsMm.setInterestAmt(message.isSetAccruedInterestTotalAmt() ? new BigDecimal(message.getAccruedInterestTotalAmt().getValue()) : null); // 应计利息总金额
        cfetsMm.setMaturityAmt(message.isSetSettlCurrAmt2() ? new BigDecimal(message.getSettlCurrAmt2().getValue()) : null); // 到期还款金额
        cfetsMm.setValueDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null); // 首次结算日
        cfetsMm.setMaturityDate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);// 到期结算日
        cfetsMm.setHoldingDays(message.isSetCashHoldingDays() ? message.getCashHoldingDays().getUnionTypeValue() : CfetsUtils.EMPTY); // 实际占款天数
        cfetsMm.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码
        cfetsMm.setDealStatus(convert(message.isSetDealTransType() ? message.getDealTransType() : null));// 成交单状态10105
        cfetsMm.setDcie(convert(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator() : null));
    }
}
