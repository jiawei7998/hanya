package com.singlee.financial.cfets.rmb;

/**
 * 转换消息入口
 *
 * @param <T>
 * @author shenzl
 */
public interface ImixRmbMessageHandler<Message, TradeBase> {

    /**
     * 消息转换类
     *
     * @param message
     * @param tradeBase
     * @param bankId
     * @return
     * @throws Exception
     */
    void convert(Message message, TradeBase tradeBase, String bankId) throws Exception;
}
