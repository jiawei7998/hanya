package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.InterBankLoansBean;
import com.singlee.financial.pojo.component.SettPartyEnum;
import imix.imix10.ExecutionReport;
import imix.imix10.component.UndInstrmtGrp;
import imix.imix10.component.UnderlyingInstrument;

import java.math.BigDecimal;

/**
 * 同业借款
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbIblEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, InterBankLoansBean> {

    /***
     * 同业借款
     * @param message
     * @param interBankLoansBean
     * @throws Exception
     */
    public void convert(ExecutionReport message, InterBankLoansBean interBankLoansBean, String bankId) throws Exception {
        // 设置本方机构及清算信息
        convert(message, bankId, SettPartyEnum.TheParty, interBankLoansBean.getInstitutionInfo(), interBankLoansBean.getSettInfo());
        // 设置对手方机构及清算信息
        convert(message, bankId, SettPartyEnum.CounterParty, interBankLoansBean.getContraPatryInstitutionInfo(), interBankLoansBean.getContraPartySettInfo());
        // 交易模式
        interBankLoansBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        interBankLoansBean.setExecId(message.isSetExecID() ? message.getExecID().getValue() : CfetsUtils.EMPTY); // CFETS成交编号
        interBankLoansBean.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);// 交易日期
        interBankLoansBean.setTradeTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : CfetsUtils.EMPTY);// 交易成交时间
        interBankLoansBean.setPs(convert(message.isSetSide() ? message.getSide() : null, interBankLoansBean.getSettInfo().getMarkerTakerRole())); // 交易方向
        // ?
        interBankLoansBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态
        interBankLoansBean.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码
        interBankLoansBean.setMarketIndicator(message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : CfetsUtils.EMPTY);
        interBankLoansBean.setSecurityId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码
        interBankLoansBean.setTenor(message.isSetTradeLimitDays() ? message.getTradeLimitDays().getValue() : CfetsUtils.EMPTY); // 期限
        interBankLoansBean.setHoldingDays(message.isSetCashHoldingDays() ? message.getCashHoldingDays().getUnionTypeValue() : CfetsUtils.EMPTY); // 实际占款天数
        interBankLoansBean.setRate(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null);// 利率
        interBankLoansBean.setAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null);// 交易币种金额
        interBankLoansBean.setPayFrequency(convert(message.isSetCouponPaymentFrequency() ? message.getCouponPaymentFrequency() : null));//付息频率
        interBankLoansBean.setFirstPayDate(message.isSetCouponPaymentDate() ? message.getCouponPaymentDate().getValue() : CfetsUtils.EMPTY);//首次付息日

        interBankLoansBean.setInterestAmt(message.isSetAccruedInterestTotalAmt() ? new BigDecimal(message.getAccruedInterestTotalAmt().getValue()) : null); // 应计利息总金额
        interBankLoansBean.setMaturityAmt(message.isSetSettlCurrAmt2() ? new BigDecimal(message.getSettlCurrAmt2().getValue()) : null); // 到期还款金额
        interBankLoansBean.setValueDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null); // 首次结算日
        interBankLoansBean.setMaturityDate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);// 到期结算日
        interBankLoansBean.setText(message.isSetText() ? message.getText().getValue() : CfetsUtils.EMPTY);
        //存出方经办行
        interBankLoansBean.setManagingEntity2(message.isSetManagingEntity2() ? message.getManagingEntity2().getValue() : CfetsUtils.EMPTY);
        //存入方经办行
        interBankLoansBean.setManagingEntity1(message.isSetManagingEntity1() ? message.getManagingEntity1().getValue() : CfetsUtils.EMPTY);
        //数据类型标识
        interBankLoansBean.setDataCategoryIndicator(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator().getValue() : null);
        interBankLoansBean.setDcie(convert(message.isSetDataCategoryIndicator() ? message.getDataCategoryIndicator() : null)); //数据类型标识 Excel导出使用

        UndInstrmtGrp undInstrmtGrp = new UndInstrmtGrp();
        undInstrmtGrp = message.getUndInstrmtGrp();
        UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
        int NoUnderlyings = undInstrmtGrp.isSetNoUnderlyings() ? undInstrmtGrp.getNoUnderlyings().getValue() : 0;
        UnderlyingInstrument inst = new UnderlyingInstrument();
        //第二部分
        for (int i = 1; i <= NoUnderlyings; i++) {
            undInstrmtGrp.getGroup(i, noUnderlyings);
            inst = noUnderlyings.getUnderlyingInstrument();
            //付息日 按期付息的时候有值
            interBankLoansBean.setIntPayDate(inst.isSetUnderlyingCouponPaymentDate() ? (inst.getUnderlyingCouponPaymentDate().getValue() == null ? CfetsUtils.EMPTY : inst.getUnderlyingCouponPaymentDate().getValue()) : CfetsUtils.EMPTY);
            //应计利息 按期付息的时候有值
            interBankLoansBean.setAccruedIntAmt(new BigDecimal(inst.isSetUnderlyingAccruedInterestAmt() ? (inst.getUnderlyingAccruedInterestAmt().getValue() == null ? "0" : inst.getUnderlyingAccruedInterestAmt().getValue()) : "0"));
        }
    }
}
