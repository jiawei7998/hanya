package com.singlee.financial.cfets.Util;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.rmb.ImixRmbCfetsApiService;
import com.singlee.financial.cfets.rmb.ImixRmbManageServer;
import com.singlee.financial.cfets.rmb.ImixRmbMessageProcessImpl;
import com.singlee.financial.pojo.IbncdBean;
import com.singlee.financial.pojo.RepoCrBean;
import com.singlee.financial.pojo.SLFBean;
import com.singlee.financial.pojo.component.RepoMethodEnum;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.DataDictionary;
import imix.field.ExecID;
import imix.field.MarketIndicator;
import imix.field.MsgType;
import imix.field.SecurityID;
import imix.imix10.CommissionData;
import imix.imix10.ExecutionReport;
import imix.imix10.SecurityDefinition;
import imix.imix10.SupplementalReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContextEvent;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

/***
 *
 * 1、交易自动验收生成数据服务,验收配置为Y后 启动项目,该服务会启动
 * 2、client.cfg文件的以下两个配置为日志读取的路径和服务端口
 *  CheckAndAcceptLogPath=D:/cfets/
 cfets.cstp.fx.port=8009
 * 3、将交易.log的日志文件放到以上配置的目录，文件要包含此次验收所有的交易报文，必须以.log结尾，可以是多个文件
 * 4、运行项目根目录下的 ParseTrades.cmd文件，服务会自动生成验收需要的数据到固定的目录下，生成的文件中没有多余的列，不需要手动在界面勾选
 * 5、直接上传文件进行匹配
 * @author lijie
 *
 */
public class TrdSocketListener extends ContextLoaderListener {

    private ConfigBean configBean;

    private ImixRmbCfetsApiService imixRmbCfetsApiService;

    private ServerSocket server = null;

    private String webRootPath = null;

    private static Logger LogManager = LoggerFactory.getLogger(TrdSocketListener.class);

    public TrdSocketListener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        webRootPath = arg0.getServletContext().getRealPath("");
        configBean = SpringGetBeanByClass.getBean(ConfigBean.class);
        imixRmbCfetsApiService = SpringGetBeanByClass.getBean(ImixRmbCfetsApiService.class);
        if (!"Y".equals(configBean.getCstpRmbFlag())) {
            return;
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Properties p = new Properties();
                    p.load(TrdSocketClient.class.getClassLoader().getResourceAsStream("cfg//client.cfg"));
                    int port = Integer.parseInt(p.getProperty("cfets.cstp.rmb.port"));
                    server = new ServerSocket(port);
                    //b)指定绑定的端口，并监听此端口。
                    System.out.println("******* 验收读取文件服务启动成功,绑定端口" + port);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Socket socket = null;
                while (true) {
                    try {
                        socket = server.accept();
                        doProcess(socket);
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }).start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if (!"Y".equals(configBean.getCstpFxFlag())) {
            return;
        }
        try {
            if (server != null && !server.isClosed()) {
                server.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     *
     * 解析日志中的报文
     * 解析报文到Map中
     * 将交易写入文件中
     * @param socket
     * @throws Exception
     */
    public void doProcess(final Socket socket) throws Exception {
        new Thread(new Runnable() {

            @Override
            public void run() {
                BufferedReader in = null;
                PrintWriter w = null;
                try {
                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    w = new PrintWriter(socket.getOutputStream());
                    String line = in.readLine();
                    System.out.println(line);
                    if ("TRADES".equals(line)) {
                        try {
                            readerMsgs();

                            w.println("执行成功,请到目录:[" + webRootPath + "/result/]下查看结果");
                            w.flush();
                        } catch (Exception e) {
                            e.printStackTrace();

                            w.println("执行失败:" + e.getMessage());
                            w.flush();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (in != null) {
                            in.close();
                        }

                        if (w != null) {
                            w.close();
                        }

                        if (socket != null) {
                            socket.close();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void readerMsgs() throws Exception {
        String path = configBean.getLogPath();
        File[] fileList = null;
        File file = new File(path);
        if (!file.exists()) {
            throw new RuntimeException("文件不存在");
        }
        fileList = file.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".log");
            }
        });
        ArrayList<String> arr = new ArrayList<String>();
        InputStream instream = null;
        for (File f : fileList) {
            try {
                instream = new FileInputStream(f);
                if (instream != null) {
                    InputStreamReader inputreader = new InputStreamReader(instream, "UTF-8");
                    BufferedReader buffreader = new BufferedReader(inputreader);
                    String line;
                    while ((line = buffreader.readLine()) != null) {
                        if (line.contains("8=IMIX.1.0") && ((line.contains("35=8") || line.contains("35=d") || line.contains("35=U93") || line.contains("35=U207")))) {
                            arr.add(line.substring(line.indexOf("8=IMIX.1.0")));
                        }
                    }
                    buffreader.close();
                }
            } catch (Exception e) {
                throw e;
            } finally {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        TradeBaseBean tradeBean = null;
        String marketIndicator = null;
        String msgType = null;
        List<TradeBaseBean> beans = null;
        String tmp = null;
        Map<String, String> map_1 = new HashMap<String, String>();
        Map<String, String> errs1 = new HashMap<String, String>();
        Map<String, List<TradeBaseBean>> map = new HashMap<String, List<TradeBaseBean>>();
        for (String s : arr) {
            try {
                tradeBean = parseTrade(s);

                tmp = s.substring(s.indexOf(MsgType.FIELD + "="));
                msgType = tmp.substring(0, tmp.indexOf(ImixRmbManageServer.SPLIT_CHAR)).split("=")[1];
                if (MsgType.EXECUTION_REPORT.equals(msgType)) {
                    int index = s.indexOf(MarketIndicator.FIELD + "=");
                    if (index == -1) {
                        marketIndicator = msgType;
                    } else {
                        tmp = s.substring(index);
                        marketIndicator = tmp.substring(0, tmp.indexOf(ImixRmbManageServer.SPLIT_CHAR)).split("=")[1];
                    }


                } else if (MsgType.SUPPLEMENTALREPORT.equals(msgType)) {
                    tmp = s.substring(s.indexOf(MarketIndicator.FIELD + "="));
                    marketIndicator = MsgType.SUPPLEMENTALREPORT + "_" + tmp.substring(0, tmp.indexOf(ImixRmbManageServer.SPLIT_CHAR)).split("=")[1];

                } else {
                    marketIndicator = msgType;
                }

                LogManager.info("解析完成：marketIndicator ===>" + marketIndicator + ", execId===>" + tradeBean.getExecId());

                if (tradeBean instanceof IbncdBean) {
                    IbncdBean t = (IbncdBean) tradeBean;
                    if ("6".equals(t.getIssMethod())) {//6-报价发行
                        marketIndicator = "CD_BJ";

                    } else if ("0".equals(t.getIssMethod()) || "5".equals(t.getIssMethod())) { //0-单一价格招标发行 5-数量招标发行
                        marketIndicator = "CD_ZB";

                    }
                }

                if (tradeBean instanceof SLFBean) {
                    SLFBean t = (SLFBean) tradeBean;
                    if ("6".equals(t.getMarketCode())) {//常备借贷便利
                        marketIndicator = "REPO_CB";
                    }
                }

                if (tradeBean instanceof RepoCrBean) {
                    RepoCrBean t = (RepoCrBean) tradeBean;
                    if (t.getSecursCustodian().size() <= 0) {
                        if (t.getRepoMethod().equals(RepoMethodEnum.Enquiry)) {
                            marketIndicator = "REPO_SB";
                        } else if (t.getRepoMethod().equals(RepoMethodEnum.Married)) {
                            marketIndicator = "REPO_TY";
                        } else {
                            marketIndicator = msgType;
                        }
                    } else {
                        marketIndicator = "REPO_KTG";
                    }
                }

                if (map_1.get(tradeBean.getExecId()) != null) {
                    //重复交易
                    marketIndicator = "RESEND_" + marketIndicator;
                } else {
                    map_1.put(tradeBean.getExecId(), tradeBean.getExecId());
                }

                beans = map.get(marketIndicator);
                if (beans == null) {
                    beans = new ArrayList<TradeBaseBean>();
                    map.put(marketIndicator, beans);
                }
                beans.add(tradeBean);
            } catch (Exception e) {
                e.printStackTrace();
                LogManager.error(s);
                LogManager.error("解析报错：", e);
                errs1.put(s, e.getMessage());
            }
        }// END FOR

        StringBuffer sdf = new StringBuffer();

        ExcelUtil eUtil = new ExcelUtil(webRootPath + "/本方成交v1.9.xlsx");
        String res = webRootPath + "/result/";
        File dir = new File(res);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        Map<String, String> errs2 = new HashMap<String, String>();
        List<TradeBaseBean> reSends = new ArrayList<TradeBaseBean>();
        for (Entry<String, List<TradeBaseBean>> e : map.entrySet()) {
            marketIndicator = e.getKey();
            LogManager.info("==============>marketIndicator: " + marketIndicator + "," + e.getValue().size());
            sdf = new StringBuffer();
            for (TradeBaseBean tradeBaseBean : e.getValue()) {
                sdf.append(tradeBaseBean.getExecId()).append(" ");
            }

            LogManager.info("==============>" + sdf.toString());

            if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {// 信用拆借
                ImixTradeUtil.writeMMTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.COLLATERAL_REPO)) {// 质押式回购
//				ImixTradeUtil.writeZyRepoTrader(eUtil, e.getValue(), marketIndicator,errs2);

            } else if ("REPO_SB".equals(marketIndicator)) {// 质押式回购-双边
                ImixTradeUtil.writeZySbRepoTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if ("REPO_TY".equals(marketIndicator)) {// 质押式回购-通用
                ImixTradeUtil.writeZyTyRepoTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if ("REPO_KTG".equals(marketIndicator)) {// 质押式回购-跨托管
                ImixTradeUtil.writeZyKtgRepoTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.OUTRIGHT_REPO)) {// 买断式回购
                ImixTradeUtil.writeMdRepoTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.CASH_BOND)) {// 现券买卖
                ImixTradeUtil.writeBondTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.BOND_FORWARD)) {// 债券远期
                ImixTradeUtil.writeBondFwTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.SECURITY_LENDING)) {// 债券借贷
                ImixTradeUtil.writeSlTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if ("CD_ZB".equals(marketIndicator)) { //存单发型 招标发行 本方招标发行结果
                ImixTradeUtil.writeCD_ZBTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if ("CD_BJ".equals(marketIndicator)) { //存单发型 报价发行 本方报价发行结果
                ImixTradeUtil.writeCD_BJTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MsgType.COMMISSIONDATA)) { //U93 存单缴款 发行人缴款信息
                ImixTradeUtil.writeCDCommTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.IBD)) {// 同业存款结果
                ImixTradeUtil.writeIbdTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MsgType.SUPPLEMENTALREPORT + "_" + MarketIndicator.IBD)) {// 同业存款附加协议
                ImixTradeUtil.writeIbdAddTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.INTER_BANK_BORROWING)) {// 同业借款
                ImixTradeUtil.writeMMlendTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MsgType.SUPPLEMENTALREPORT + "_" + MarketIndicator.INTER_BANK_BORROWING)) {// 同业借款附加协议
                ImixTradeUtil.writeMMlendAddTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.equals(MarketIndicator.INTEREST_RATE_SWAP)) {// 利率互换
                ImixTradeUtil.writeIrsTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if ("REPO_CB".equals(marketIndicator)) {// 常备借贷便利
                ImixTradeUtil.writeCbRepoTrader(eUtil, e.getValue(), marketIndicator, errs2);

            } else if (marketIndicator.startsWith("RESEND_")) {//重复交易
                reSends.addAll(e.getValue());

            }

        }
        //重发信息
        writeReSendTrades(reSends);

        try {
            Map<String, String> map_3 = new TreeMap<String, String>(new Comparator<String>() {
                @Override
                public int compare(String str1, String str2) {

                    return str1.compareTo(str2);
                }
            });

            String execId = null;
            String s = null;
            //解析错误
            for (Entry<String, String> e : errs1.entrySet()) {
                s = e.getKey();

                tmp = s.substring(s.indexOf(MsgType.FIELD + "="));
                msgType = tmp.substring(0, tmp.indexOf(ImixRmbManageServer.SPLIT_CHAR)).split("=")[1];
                if (MsgType.EXECUTION_REPORT.equals(msgType)) {
                    tmp = s.substring(s.indexOf(ExecID.FIELD + "="));
                    execId = tmp.substring(0, tmp.indexOf(ImixRmbManageServer.SPLIT_CHAR)).split("=")[1];

                } else if (MsgType.SECURITY_DEFINITION.equals(msgType)) {
                    tmp = s.substring(s.indexOf(SecurityID.FIELD + "="));
                    execId = tmp.substring(0, tmp.indexOf(ImixRmbManageServer.SPLIT_CHAR)).split("=")[1] + "_" + msgType;

                } else if (MsgType.COMMISSIONDATA.equals(msgType)) {
                    tmp = s.substring(s.indexOf(SecurityID.FIELD + "="));
                    execId = tmp.substring(0, tmp.indexOf(ImixRmbManageServer.SPLIT_CHAR)).split("=")[1] + "_" + msgType;
                }

                if (map_3.get(execId) != null) {
                    continue;
                } else {
                    map_3.put(execId, e.getValue());
                }
            }

            //写入错误
            for (Entry<String, String> e : errs2.entrySet()) {
                execId = e.getKey();
                if (map_3.get(execId) != null) {
                    continue;
                } else {
                    map_3.put(execId, e.getValue());
                }
            }

            ImixTradeUtil.writeErrorTrader(eUtil, map_3);

        } catch (Exception e) {
            LogManager.error("写入Error交易出错:", e);
        }

        System.out.println(res);
        eUtil.writeExcel(res + "本方成交_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".xlsx");
    }

    private void writeReSendTrades(List<TradeBaseBean> value) {
        PrintWriter p = null;
        try {
            p = new PrintWriter(new File(webRootPath + "/result/RESEND_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".txt"));
            Map<String, String> map_1 = new HashMap<String, String>();
            for (TradeBaseBean tradeBaseBean : value) {
                if (map_1.get(tradeBaseBean.getExecId()) != null) {
                    continue;
                } else {
                    p.println(tradeBaseBean.getExecId());

                    map_1.put(tradeBaseBean.getExecId(), tradeBaseBean.getExecId());
                }
            }
            p.flush();
        } catch (Exception e) {
            LogManager.error("写入重复交易出错:", e);
        } finally {
            if (p != null) {
                p.close();
            }
        }
    }

    public TradeBaseBean parseTrade(String msg) throws Exception {
        String imixType = null;
        String[] arr = msg.toString().split(String.valueOf(ImixRmbManageServer.SPLIT_CHAR));
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].startsWith(String.valueOf(MsgType.FIELD) + "=")) {
                imixType = arr[i].split("=")[1];
            }
        }
        CfetsPackMsgBean cfetsPackMsg = new CfetsPackMsgBean();
        TradeBaseBean tradeBaseBean = null;
        ImixRmbMessageProcessImpl process = new ImixRmbMessageProcessImpl(configBean);

        if (MsgType.EXECUTION_REPORT.equals(imixType)) {
            ExecutionReport message = new ExecutionReport();
            message.fromString(ImixRmbManageServer.reBuilderMsg(msg), new DataDictionary("IMIX10.xml"), false);
            //解析交易报文
            tradeBaseBean = process.parseMsg(message);

        } else if (MsgType.SECURITY_DEFINITION.equals(imixType)) {
            SecurityDefinition securityDefinition = new SecurityDefinition();
            securityDefinition.fromString(ImixRmbManageServer.reBuilderMsg(msg), new DataDictionary("IMIX10.xml"), false);
            //解析同业存单
            tradeBaseBean = process.parseMsg(securityDefinition);
        } else if (MsgType.COMMISSIONDATA.equals(imixType)) {
            CommissionData cData = new CommissionData();
            cData.fromString(ImixRmbManageServer.reBuilderMsg(msg), new DataDictionary("IMIX10.xml"), false);
            //解析同业存单缴款信息
            tradeBaseBean = process.parseMsg(cData);
        } else if (MsgType.SUPPLEMENTALREPORT.equals(imixType)) {
            SupplementalReport sReport = new SupplementalReport();
            sReport.fromString(ImixRmbManageServer.reBuilderMsg(msg), new DataDictionary("IMIX10.xml"), false);
            //解析同业存款附加协议 同业拆借附加协议
            tradeBaseBean = process.parseMsg(sReport);
        }
        return tradeBaseBean;
    }

}
