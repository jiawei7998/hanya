package com.singlee.financial.cfets.rmb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.ICfetsImixDepositsAndLoans;
import com.singlee.financial.cfets.ICfetsImixRepo;
import com.singlee.financial.cfets.ICfetsImixSecur;
import com.singlee.financial.cfets.ICfetsImixSecurityLending;
import com.singlee.financial.cfets.ICfetsImixSwap;


/**
 * CFETS本币接口服务
 * 
 * @author chenxh
 * 
 */
@Service
public class ImixRmbCfetsApiService {

	@Autowired
	ICfetsImixDepositsAndLoans cfetsImixDepositsAndLoans;
		
	@Autowired
	ICfetsImixSwap cfetsImixSwap;
	
	@Autowired
	ICfetsImixSecur cfetsImixSecur;
	
	@Autowired
	ICfetsImixRepo cfetsImixRepo;
	
	@Autowired
	ICfetsImixSecurityLending cfetsImixLending;

	public ICfetsImixDepositsAndLoans getCfetsImixMM() {
		return cfetsImixDepositsAndLoans;
	}

	public void setCfetsImixMM(ICfetsImixDepositsAndLoans cfetsImixDepositsAndLoans) {
		this.cfetsImixDepositsAndLoans = cfetsImixDepositsAndLoans;
	}

	public ICfetsImixSwap getCfetsImixSwap() {
		return cfetsImixSwap;
	}

	public void setCfetsImixSwap(ICfetsImixSwap cfetsImixSwap) {
		this.cfetsImixSwap = cfetsImixSwap;
	}

	public ICfetsImixSecur getCfetsImixSecur() {
		return cfetsImixSecur;
	}

	public void setCfetsImixSecur(ICfetsImixSecur cfetsImixSecur) {
		this.cfetsImixSecur = cfetsImixSecur;
	}

	public ICfetsImixRepo getCfetsImixRepo() {
		return cfetsImixRepo;
	}

	public void setCfetsImixRepo(ICfetsImixRepo cfetsImixRepo) {
		this.cfetsImixRepo = cfetsImixRepo;
	}

	public ICfetsImixSecurityLending getCfetsImixLending() {
		return cfetsImixLending;
	}

	public void setCfetsImixLending(ICfetsImixSecurityLending cfetsImixLending) {
		this.cfetsImixLending = cfetsImixLending;
	}
	
}
