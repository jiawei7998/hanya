package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.FixedIncomeBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;

/**
 * 现券买卖
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbCbtEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, FixedIncomeBean> {
    /**
     * 转换本币现券交易 marketIndicator=4,5
     *
     * @param message
     * @param imixFiBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, FixedIncomeBean imixFiBean, String bankId) throws FieldNotFound, FieldConvertError {
        setBaseInfo(message, bankId, imixFiBean, imixFiBean.getSettInfo(), imixFiBean.getContraPartySettInfo());
        // 交易模式
        imixFiBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));

        SecurityInfoBean securityInfo = new SecurityInfoBean();
        securityInfo.setSecurityId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY); // 债券ISNB代码
        securityInfo.setSecurityName(message.isSetSymbol() ? message.getSymbol().getValue() : CfetsUtils.EMPTY); // 债券简称
        securityInfo.setFaceAmt(BigDecimal.valueOf(message.isSetLastQty() ? message.getLastQty().getDoubleValue() : 0));// 券面总额
        securityInfo.setSide(message.isSetSide()?message.getSide().getValue():null);
        imixFiBean.setSecurityInfo(securityInfo);
        imixFiBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态
        SecurityAmtDetailBean amtInfo = new SecurityAmtDetailBean();
        amtInfo.setPrice(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null); // 净价
        amtInfo.setInterestAmt(message.isSetAccruedInterestAmt() ? new BigDecimal(message.getAccruedInterestAmt().getValue()) : null); // 应计利息
        amtInfo.setInterestTotalAmt(message.isSetAccruedInterestTotalAmt() ? new BigDecimal(message.getAccruedInterestTotalAmt().getValue()) : null); // 应计利息总额
        amtInfo.setDirtyPrice(message.isSetDirtyPrice() ? new BigDecimal(message.getDirtyPrice().getValue()) : null); // 全价
        amtInfo.setSettAmt(message.isSetSettlCurrAmt() ? new BigDecimal(message.getSettlCurrAmt().getValue()) : null); // 结算金额
        amtInfo.setSettType(message.isSetDeliveryType() ? message.getDeliveryType().getValue() + "" : CfetsUtils.EMPTY);// 结算方式
        amtInfo.setAmt(message.isSetTradeCashAmt() ? new BigDecimal(message.getTradeCashAmt().getValue()) : null); // 交易金额
        amtInfo.setSettDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null); // 结算日期
        amtInfo.setSettCCy(message.isSetSettlCurrency() ? message.getSettlCurrency().getValue() : null);//结算币种120

        int num = message.isSetNoStipulations() ? message.getNoStipulations().getValue() : 0;
        //233StipulationType 取值为Yield2 时 表示 234StipulationValue 取值为到 期收益率
        for (int i = 1; i <= num; i++) {
            ExecutionReport.NoStipulations noStipulations = new ExecutionReport.NoStipulations();
            message.getGroup(i, noStipulations);
            String type = noStipulations.isSetStipulationType() ? noStipulations.getStipulationType().getValue() : "";
            if ("Yield2".equals(type)) {
                amtInfo.setYeld(noStipulations.isSetStipulationValue() ? new BigDecimal(noStipulations.getStipulationValue().getValue()) : null); // 到期收益率
            }
        }
        imixFiBean.setSecurityAmt(amtInfo);
    }
}
