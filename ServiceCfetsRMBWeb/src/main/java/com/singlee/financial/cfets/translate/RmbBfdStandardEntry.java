package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.StandardBfdBean;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;

/**
 * 标准债券远期
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbBfdStandardEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport,StandardBfdBean> {

    public void convert(ExecutionReport message, StandardBfdBean standardBfdBean, String bankId) throws Exception {
        setBaseInfo(message, bankId, standardBfdBean, standardBfdBean.getSettInfo(), standardBfdBean.getContraPartySettInfo());
        //交易日期
        standardBfdBean.setTradeDate(message.isSetTradeDate() ? message.getTradeDate().getValue() : CfetsUtils.EMPTY);
        //交易模式
        standardBfdBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        //合约代码
        standardBfdBean.setSecurityId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);
        //品种名称
        standardBfdBean.setSecurityGroup(message.isSetSecurityGroup() ? message.getSecurityGroup().getValue() : CfetsUtils.EMPTY);
        //交割日
        standardBfdBean.setSettlDate(message.isSetSettlDate() ? message.getSettlDate().getValue() : CfetsUtils.EMPTY);
        //价格
        standardBfdBean.setPrice(message.isSetLastPx() ? new BigDecimal(message.getLastPx().getValue()) : null);
        //金额
        standardBfdBean.setAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null);
        //清算场所 6 上海清算所  13 双边自行清算
        standardBfdBean.setClearingMethod(message.isSetClearingMethod() ? message.getClearingMethod().getValue() : null);
        //是否备注
        standardBfdBean.setRemarkIndicator(message.isSetRemarkIndicator() ? message.getRemarkIndicator().getValue() : false);
        //备注信息
        standardBfdBean.setText(message.isSetText() ? message.getText().getValue() : CfetsUtils.EMPTY);
    }
}
