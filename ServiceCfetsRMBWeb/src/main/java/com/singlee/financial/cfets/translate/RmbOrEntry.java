package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.rmb.ImixRmbMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.RepoOrBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;
import imix.imix10.component.UndInstrmtGrp;
import imix.imix10.component.UnderlyingInstrument;

import java.math.BigDecimal;

/**
 * 买断式回购
 *
 * @author shenzl
 * @date 2021/10/27
 */
public class RmbOrEntry extends RmbSettleEntry implements ImixRmbMessageHandler<ExecutionReport, RepoOrBean> {
    /**
     * 转换本币买断式回购 marketIndicator=10
     *
     * @param message
     * @param repoOrBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, RepoOrBean repoOrBean, String bankId) throws FieldNotFound, FieldConvertError {
        setBaseInfo(message, bankId, repoOrBean, repoOrBean.getSettInfo(), repoOrBean.getContraPartySettInfo());
        // 交易模式
        repoOrBean.setDealMode(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        repoOrBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));// 交易状态
        repoOrBean.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : CfetsUtils.EMPTY);// 品种代码
        repoOrBean.setRemarkIndicator(message.isSetRemarkIndicator() ? message.getRemarkIndicator().getValue() : false);//有无补充条款
        repoOrBean.setTerm(message.isSetTradeLimitDays() ? message.getTradeLimitDays().getValue() : CfetsUtils.EMPTY);//回购期限
        repoOrBean.setOccupancyDays(String.valueOf(message.isSetCashHoldingDays() ? message.getCashHoldingDays().getValue() : CfetsUtils.EMPTY));//实际占款天数
        UndInstrmtGrp undInstrmtGrp = message.getUndInstrmtGrp();
        // 交易品种? 券面总额?
        for (int i = 1; i <= undInstrmtGrp.getNoUnderlyings().getValue(); i++) {
            UndInstrmtGrp.NoUnderlyings noUnderlyings = new UndInstrmtGrp.NoUnderlyings();
            undInstrmtGrp.getGroup(i, noUnderlyings);
            UnderlyingInstrument underlyingInstrument = noUnderlyings.getUnderlyingInstrument();
            // 债券ISNB代码
            repoOrBean.getSecurityInfo().setSecurityId(underlyingInstrument.isSetUnderlyingSecurityID() ? underlyingInstrument.getUnderlyingSecurityID().getValue() : CfetsUtils.EMPTY);
            // 债券简称
            repoOrBean.getSecurityInfo().setSecurityName(underlyingInstrument.isSetUnderlyingSymbol() ? underlyingInstrument.getUnderlyingSymbol().getValue() : CfetsUtils.EMPTY);
            // 券面总额
            repoOrBean.getSecurityInfo().setFaceAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null);
            // 回购利率
            repoOrBean.getSecurityInfo().setHaircut(message.isSetPrice() ? new BigDecimal(message.getPrice().getValue()) : null);
            // 首次应计利息
            repoOrBean.getComSecurityAmt().setInterestAmt(underlyingInstrument.isSetUnderlyingAccruedInterestAmt() ? new BigDecimal(underlyingInstrument.getUnderlyingAccruedInterestAmt().getValue()) : null);
            // 首次净价
            repoOrBean.getComSecurityAmt().setPrice(underlyingInstrument.isSetUnderlyingPx() ? new BigDecimal(underlyingInstrument.getUnderlyingPx().getValue()) : null);
            // 首次全价
            repoOrBean.getComSecurityAmt().setDirtyPrice(underlyingInstrument.isSetUnderlyingDirtyPrice() ? new BigDecimal(underlyingInstrument.getUnderlyingDirtyPrice().getValue()) : null);
            // 首次结算金额
            repoOrBean.getComSecurityAmt().setSettAmt(new BigDecimal(message.isSetSettlCurrAmt() ? message.getSettlCurrAmt().getValue() : null));
            // 首次结算日
            repoOrBean.getComSecurityAmt().setSettDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null);
            // 首次结算方式
            repoOrBean.getComSecurityAmt().setSettType(message.isSetDeliveryType() ? message.getDeliveryType().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 结算币种
            repoOrBean.getComSecurityAmt().setSettlCcy(message.isSetSettlCurrency() ? message.getSettlCurrency().getValue() : CfetsUtils.EMPTY);
            // 到期净价
            repoOrBean.getMatSecurityAmt().setPrice(underlyingInstrument.isSetUnderlyingPx2() ? new BigDecimal(underlyingInstrument.getUnderlyingPx2().getValue()) : null);
            // 到期应计利息
            repoOrBean.getMatSecurityAmt().setInterestAmt(underlyingInstrument.isSetUnderlyingAccruedInterestAmt2() ? new BigDecimal(underlyingInstrument.getUnderlyingAccruedInterestAmt2().getValue()) : null);
            // 到期全价
            repoOrBean.getMatSecurityAmt().setDirtyPrice(underlyingInstrument.isSetUnderlyingDirtyPrice2() ? new BigDecimal(underlyingInstrument.getUnderlyingDirtyPrice2().getValue()) : null);
            // 到期结算金额
            repoOrBean.getMatSecurityAmt().setSettAmt(message.isSetSettlCurrAmt2() ? new BigDecimal(message.getSettlCurrAmt2().getValue()) : null);
            // 到期结算日
            repoOrBean.getMatSecurityAmt().setSettDate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);
            // 到期结算方式
            repoOrBean.getMatSecurityAmt().setSettType(message.isSetDeliveryType2() ? message.getDeliveryType2().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 结算币种
            repoOrBean.getMatSecurityAmt().setSettlCcy(message.isSetSettlCurrency() ? message.getSettlCurrency().getValue() : CfetsUtils.EMPTY);

            for (int j = 1; j <= noUnderlyings.getNoUnderlyingStips().getValue(); j++) {
                UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips noUnderlyingStips = new UndInstrmtGrp.NoUnderlyings.NoUnderlyingStips();
                noUnderlyings.getGroup(j, noUnderlyingStips);
                if ("Yield".equals(noUnderlyingStips.getUnderlyingStipType().getValue())) {
                    // 首次收益率
                    repoOrBean.getComSecurityAmt().setYeld(noUnderlyingStips.isSetUnderlyingStipValue() ? new BigDecimal(noUnderlyingStips.getUnderlyingStipValue().getValue()) : null);
                } else {
                    // 到期收益率
                    repoOrBean.getMatSecurityAmt().setYeld(noUnderlyingStips.isSetUnderlyingStipValue() ? new BigDecimal(noUnderlyingStips.getUnderlyingStipValue().getValue()) : null);
                }
            }
        }
    }
}
