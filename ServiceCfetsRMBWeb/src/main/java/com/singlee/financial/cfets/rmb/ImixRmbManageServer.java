package com.singlee.financial.cfets.rmb;

import com.singlee.financial.cfets.Util.MarketIndicatorEnum;
import com.singlee.financial.cfets.Util.SpringContextUtil;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.spring.SpringContextHolder;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.DataDictionary;
import imix.client.core.ImixApplication;
import imix.client.core.ImixSession;
import imix.field.*;
import imix.imix10.CommissionData;
import imix.imix10.ExecutionReport;
import imix.imix10.SecurityDefinition;
import imix.imix10.SupplementalReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ImixRmbManageServer implements IImixRmbManageServer {

    private static Logger LogManager = LoggerFactory.getLogger(IImixRmbManageServer.class);
    public final static char SPLIT_CHAR = 0X01;
    /**
     * CFETS配置信息
     */
    @Autowired
    private ImixRmbClient imixRmbClient;
    // CFETS配置类
    @Autowired
    private ConfigBean configBean;
    @Autowired
    ImixRmbCfetsApiService imixRmbCfetsApiService;


    @Override
    public boolean startImixSession() { // 是否登录
        boolean isLogin = false;
        // fxImixClient名称是配置在main.spring.xml中bean的别名，这个需要注意
        ImixRmbClient rmbClient = SpringContextHolder.getBean("rmbImixClient");
        // 先停止应用服务,再进行开启
        ImixApplication.stop();
        // 重新初始化程序
        try {
            isLogin = rmbClient.initLogin();
        } catch (Exception e) {
            LogManager.error("CFETS RESTART RMB LOGIN ConfigError:[" + e.getMessage() + "]", e);
        }
        return isLogin;
    }

    @Override
    public boolean stopImixSession() {
        ImixSession imixSession = imixRmbClient.getSession();
        if (imixSession.isStarted()) {
            //登出单个会话
            imixSession.stop();
        }
        //登出所有会话
        ImixApplication.stop();
        return false;
    }


    @Override
    public String confirmRmbTransaction(String xml) {
        return null;
    }

    /**
     * 读取发送失败的交易记录
     */
    @Override
    public List<TradingFailureBean> getFailureSendByDate(String date) {
        LogManager.info("*******************开始读取发送失败的交易日志**********************");
        //自动加载日志文件的路径位置
        ConfigBean configBean = SpringContextUtil.getBean(ConfigBean.class);
        ArrayList<TradingFailureBean> list = new ArrayList<TradingFailureBean>();
        //解析当日交易报文
        Map<String, File> map = getFileByNameZz(date, getFileNames(getResourceAsFilePath(configBean.getLogPath())));
        if (map == null || map.size() == 0) {
            return list;
        }
        //解析报文
        ArrayList<String> arr = new ArrayList<String>();
        InputStream instream = null;
        InputStreamReader inputreader = null;
        BufferedReader buffreader = null;
        for (Map.Entry<String, File> entry : map.entrySet()) {
            try {
                instream = new FileInputStream(entry.getValue());
                if (instream != null) {
                    inputreader = new InputStreamReader(instream, configBean.getCharset());
                    buffreader = new BufferedReader(inputreader);
                    String line;
                    while ((line = buffreader.readLine()) != null) {
                        if (line.contains("INFO") && line.contains("8=IMIX.1.0") && (line.contains("35=8") || line.contains("35=d") || line.contains("35=U93"))) {
                            arr.add(line.substring(line.indexOf("8=IMIX.1.0")));
                        }
                    }
                }
            } catch (java.io.FileNotFoundException e) {
                LogManager.error(e.getMessage());
            } catch (IOException e) {
                LogManager.error(e.getMessage());
            } finally {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e) {
                        LogManager.debug(e.getMessage());
                    }
                }
                if (buffreader != null) {
                    try {
                        buffreader.close();
                    } catch (IOException e) {
                        LogManager.debug(e.getMessage());
                    }
                }
            }
        }// end for

        Map<String, String> mapDealno = new HashMap<String, String>();
        Map<String, String> map1 = null;
        String dealNo = null;
        TradingFailureBean bean = null;
        //解析报文封装成对象
        for (String str : arr) {
            map1 = new HashMap<String, String>();
            //根据这个东西进行分解  
            String[] strTemp = str.split(String.valueOf(SPLIT_CHAR));
            if (strTemp == null || strTemp.length == 0) {
                continue;
            }
            for (String strRecord : strTemp) {
                if (strRecord.startsWith(ExecID.FIELD + "=")
                        || strRecord.startsWith(SecurityID.FIELD + "=")
                        || strRecord.startsWith(MarketIndicator.FIELD + "=")
                        || strRecord.startsWith(TradeDate.FIELD + "=")
                        || strRecord.startsWith(MsgType.FIELD + "=")
                        || strRecord.startsWith(SendingTime.FIELD + "=")) {
                    System.out.println(strRecord);
                    String[] strLength = strRecord.split("=");
                    map1.put(strLength[0], strLength[1]);
                }
            }
            //封装数据
            dealNo = map1.get(String.valueOf(ExecID.FIELD)) == null ? map1.get(String.valueOf(SecurityID.FIELD)) : map1.get(String.valueOf(ExecID.FIELD));
            if (mapDealno.get(dealNo) == null) {
                mapDealno.put(dealNo, dealNo);
                bean = new TradingFailureBean();
                bean.setDealNo(dealNo);
                bean.setMarketIndicator(map1.get(String.valueOf(MarketIndicator.FIELD)) == null ? MarketIndicatorEnum.getName(map1.get(String.valueOf(MsgType.FIELD))) : MarketIndicatorEnum.getName(map1.get(String.valueOf(MarketIndicator.FIELD))));
                bean.setTradeDate(map1.get(String.valueOf(TradeDate.FIELD)) == null ? map1.get(String.valueOf(SendingTime.FIELD)) : map1.get(String.valueOf(TradeDate.FIELD)));
                bean.setXml(str);
                list.add(bean);
            }
        }// end for
        LogManager.info("*******************读取发送失败的交易日志完成**********************");
        return list;
    }

    /**
     * 获取文件名称和文件内容  map<key(文件名),value(对应的文件)>;
     *
     * @param files
     * @return
     */
    public static Map<String, File> getFileNames(File[] files) {
        Map<String, File> map = new HashMap<String, File>();
        if (files.length > 0) {
            for (File file : files) {
                map.put(file.getName(), file);
            }
        }
        return map;
    }

    /**
     * 获取指定格式的路径
     *
     * @param str
     * @param map
     */
    public static Map<String, File> getFileByNameZz(String str, Map<String, File> map) {
        Map<String, File> mapTemp = new HashMap<String, File>();
        if (str == null) {
            throw new RuntimeException("过滤条件不能为空");
        }
        for (Map.Entry<String, File> entry : map.entrySet()) {
            if (entry.getKey().toString().contains(str)) {
                mapTemp.put(entry.getKey(), entry.getValue());
            }
        }
        return mapTemp;
    }

    /**
     * 获取某一个文件夹下的所有文件
     */
    public static File[] getResourceAsFilePath(String path) {
        File[] fileList = null;
        if (path == null) {
            throw new RuntimeException("路径不能为空，请检查");
        }
        File files = new File(path);
        if (files.exists()) {
            fileList = files.listFiles();
        } else {
            throw new RuntimeException("文件不存在");
        }
        return fileList;
    }

    /**
     * 重新导入数据
     */
    @Override
    public RetBean resendRmbRecord(TradingFailureBean bean) throws Exception {
        LogManager.info("********************本币重发开始************************");
        try {
            String imixType = null;
            String[] arr = bean.getXml().toString().split(String.valueOf(SPLIT_CHAR));
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].startsWith(String.valueOf(MsgType.FIELD) + "=")) {
                    imixType = arr[i].split("=")[1];
                }
            }
            CfetsPackMsgBean cfetsPackMsg = new CfetsPackMsgBean();
            TradeBaseBean tradeBaseBean = null;
            ImixRmbMessageProcessImpl process = new ImixRmbMessageProcessImpl(configBean);
            if (MsgType.EXECUTION_REPORT.equals(imixType)) {
                ExecutionReport message = new ExecutionReport();
                message.fromString(reBuilderMsg(bean.getXml()), new DataDictionary("IMIX10.xml"), false);
                //解析交易报文
                tradeBaseBean = process.parseMsg(message);
            } else if (MsgType.SECURITY_DEFINITION.equals(imixType)) {
                SecurityDefinition securityDefinition = new SecurityDefinition();
                securityDefinition.fromString(reBuilderMsg(bean.getXml()), new DataDictionary("IMIX10.xml"), false);
                //解析同业存单
                tradeBaseBean = process.parseMsg(securityDefinition);
            } else if (MsgType.COMMISSIONDATA.equals(imixType)) {
                CommissionData cData = new CommissionData();
                cData.fromString(reBuilderMsg(bean.getXml()), new DataDictionary("IMIX10.xml"), false);
                //解析同业存单缴款信息
                tradeBaseBean = process.parseMsg(cData);
            } else if (MsgType.SUPPLEMENTALREPORT.equals(imixType)) {
                SupplementalReport sReport = new SupplementalReport();
                sReport.fromString(reBuilderMsg(bean.getXml()), new DataDictionary("IMIX10.xml"), false);
                //解析附加协议
                tradeBaseBean = process.parseMsg(sReport);
            }
            cfetsPackMsg.setMsgtype(ConfigBean.reSendFlag);
            LogManager.info(tradeBaseBean.getExecId() + "解析完成");
            return process.sendMsg(tradeBaseBean, cfetsPackMsg, imixRmbCfetsApiService);
        } catch (Exception e) {
            LogManager.error("*************重发时出现异常**************** : " + e.getMessage(), e);
            throw e;
        }
    }

    /***
     *
     * 由于验证字符串会有编码问题，导致无法反解析报文，此方法将验证消息替换成00000
     * 93域为验证字符串长度 89域为验证字符串内容
     * @param msg
     * @return
     * @throws Exception
     */
    public static String reBuilderMsg(String msg) throws Exception {
        //93=7489=XXX10=191
        //获取93域验证字符串的长度
        if (msg.indexOf(SPLIT_CHAR + "93=") != -1 && msg.indexOf(SPLIT_CHAR + "89=") != -1) {
            int index = msg.indexOf(SPLIT_CHAR + "93=") + 4;
            String validStr = msg.substring(index);
            int validLen = Integer.parseInt(validStr.substring(0, validStr.indexOf(SPLIT_CHAR)));
            //赋值给89域,长度为validLen
            int i = msg.indexOf(SPLIT_CHAR + "89=") + 4;
            String s1 = msg.substring(0, i);
            String tmp = msg.substring(i);
            String s2 = tmp.substring(tmp.indexOf(SPLIT_CHAR));
            msg = s1 + String.format("%0" + validLen + "d", 0) + s2;
        }
        return msg;
    }

    @Override
    public boolean getImixSessionState() {
        ImixSession imixSession = imixRmbClient.getSession();
        return imixSession.isStarted();
    }
}
