package com.singlee.cfets;

import org.eclipse.jetty.util.ajax.JSON;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.cfets.rmb.IImixRmbManageServer;
import com.singlee.financial.cfets.rmb.ImixRmbClientListener;
import com.singlee.financial.cfets.rmb.ImixRmbManageServer;
import com.singlee.financial.cfets.spring.SpringContextHolder;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;

import imix.ConfigError;
import imix.DataDictionary;
import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.InvalidMessage;
import imix.UnsupportedMessageType;
import imix.imix10.ExecutionReport;

/**
 * @apiNote 现券买卖IMIX报文测试 测试通过
 * @author Administrator
 *
 */
public class CashTradingTest {
	public static void main(String[] args) {
		try {
			TradingFailureBean failureBean = new TradingFailureBean();
			test(failureBean);
			IImixRmbManageServer manageServer = (IImixRmbManageServer) new HessianProxyFactory().create(
					IImixRmbManageServer.class,
					"http://127.0.0.1:8081/fund-cfetsRmbWeb/api/service/IImixRmbManageServerExporter");
			RetBean ret = manageServer.resendRmbRecord(failureBean);
			System.out.println("返回结果===>" + JSON.toString(ret));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static void test(TradingFailureBean failureBean) {
		String imix = "8=IMIX.1.09=162435=834=2277649=CFETS-RMB-CSTP52=20201202-05:58:59.95456=10228357=jsyh822601115=CFETS-RMB627=1628=Agent-JSYH1630=4234629=20201202-05:58:59.64817=CBT20342532=4350000044=99.64448=01200396154=455=234250358=-60=20201202-13:58:59.19863=264=2020120375=20201202117=20120205464502005981119=9543258660.27120=CNY150=F159=0.13926919=010002=134320.2710022=N10048=99.588710105=010176=410243=M8D1110282=N10312=99432400.0010317=110318=13:58:5910319=110351=Y10465=011143=13232=1233=Yield2234=3.8000453=2448=102418452=11910601=110602=021-2052719810603=6802=14523=热源803=101523=而已803=126523=10200300291719803=15523=4325清算所803=110523=909290000007803=112523=国金证4325公司803=22523=上海清算所803=111523=B0002917803=10523=3425算专户803=23523=CFETS803=29523=上海浦东432520楼803=6523=国金证券803=125523=国金4325有限公司803=124523=GJSC803=102448=102283452=12010601=110602=021-2053623510603=6802=13523=规范803=101523=讽德诵功803=126523=990103600001803=15523=江苏银行803=110523=313301099999803=112523=股份有限公司803=22523=上海清算所803=111523=B0000531803=10523=江苏银行资金系统往来803=23523=CFETS803=29523=江苏银行803=125523=股份有限公司803=124523=JSBK803=10293=6189=000000000000000000000000000000000000000000000000000000000000010=021";
		failureBean.setXml(imix);// 报文	
		failureBean.setDealNo("CBT203425");// 交易编号 17 
		failureBean.setMarketIndicator("4");// 报文中的  10176
		failureBean.setTradeDate("20201202");// 报文中的 75
	}
}
