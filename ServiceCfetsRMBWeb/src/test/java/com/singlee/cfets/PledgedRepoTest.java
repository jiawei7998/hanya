package com.singlee.cfets;

import org.eclipse.jetty.util.ajax.JSON;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.cfets.rmb.IImixRmbManageServer;
import com.singlee.financial.cfets.rmb.ImixRmbClientListener;
import com.singlee.financial.cfets.rmb.ImixRmbManageServer;
import com.singlee.financial.cfets.spring.SpringContextHolder;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;

import imix.ConfigError;
import imix.DataDictionary;
import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.InvalidMessage;
import imix.UnsupportedMessageType;
import imix.imix10.ExecutionReport;

/**
 * @apiNote 质押式回购IMIX报文测试 测试通过
 * @author Administrator
 *
 */
public class PledgedRepoTest {
	public static void main(String[] args) {
		try {
			TradingFailureBean failureBean = new TradingFailureBean();
			test(failureBean);
			IImixRmbManageServer manageServer = (IImixRmbManageServer) new HessianProxyFactory().create(
					IImixRmbManageServer.class,
					"http://127.0.0.1:8081/fund-cfetsRmbWeb/api/service/IImixRmbManageServerExporter");
			RetBean ret = manageServer.resendRmbRecord(failureBean);
			System.out.println("返回结果===>" + JSON.toString(ret));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static void test(TradingFailureBean failureBean) {
		String imix = "8=IMIX.1.09=176435=834=3234649=CFETS-RMB-CSTP52=20201210-06:02:40.28756=10228357=jsyh822601115=CFETS-RMB627=1628=Agent-JSYH1630=6621629=20201210-06:02:40.02017=CR20204325669032=5543250000044=1.350048=R00154=160=20201210-14:02:39.93264=2020121075=20201210117=20124325005502120=CNY150=F193=20201211919=010002=1963.5310014=110022=N10045=010105=010176=910282=N10289=5303425963.5310312=543258000.0010316=110317=110318=14:02:3910465=011143=1311376=111377=1453=2448=102283452=11910601=110602=021-2053625510603=6802=14523=435803=101523=个人803=126523=-803=6523=JSBK803=102523=股份有限公司803=124523=江苏银行803=125523=江苏银行803=110523=99010热舞她40001803=15523=313301099999803=112523=3425金系统往来803=23523=股份有限公司803=22523=上海清算所803=111523=B0000531803=10523=CFETS803=29448=435815452=12010601=110602=0755-83169999-173610603=6802=14523=热舞她803=101523=热舞她803=126523=-803=6523=3425富欣1号803=102523=博4325号集合资产管理计划803=124523=4321号803=125523=435清算所803=110523=100022759220010803=15523=909290000007803=112523=4325金结算专户803=23523=4325产管理计划803=22523=上海清算所803=111523=B2275922803=10523=CFETS803=29711=2309=112008183879=30000000311=20中信银行CD183887=1888=Haircut889=0.9600309=112017194879=25300000311=20光大银行CD194887=1888=Haircut889=0.960093=6789=？？？僬？？？？？？搄沀？？？？？？？？？？夀嬠？搄？？？？？？？夀嬠？？？搄沀？？？？？？？？？？？？？？？？？？| :@)10=065";
		failureBean.setXml(imix);// 报文	
		failureBean.setDealNo("CR202043256690");// 交易编号 17 
		failureBean.setMarketIndicator("9");// 报文中的  10176
		failureBean.setTradeDate("20201210");// 报文中的 75
	}
}
