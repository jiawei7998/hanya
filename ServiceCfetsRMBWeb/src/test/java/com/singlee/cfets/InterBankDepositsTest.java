package com.singlee.cfets;

import org.eclipse.jetty.util.ajax.JSON;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.cfets.rmb.IImixRmbManageServer;
import com.singlee.financial.cfets.rmb.ImixRmbClientListener;
import com.singlee.financial.cfets.rmb.ImixRmbManageServer;
import com.singlee.financial.cfets.spring.SpringContextHolder;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;

import imix.ConfigError;
import imix.DataDictionary;
import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.InvalidMessage;
import imix.UnsupportedMessageType;
import imix.imix10.ExecutionReport;

/**
 * @apiNote 同业存款IMIX报文测试 前置未实现
 * @author Administrator
 *
 */
public class InterBankDepositsTest {
	public static void main(String[] args) {
		try {
			TradingFailureBean failureBean = new TradingFailureBean();
			test(failureBean);
			IImixRmbManageServer manageServer = (IImixRmbManageServer) new HessianProxyFactory().create(
					IImixRmbManageServer.class,
					"http://127.0.0.1:8081/fund-cfetsRmbWeb/api/service/IImixRmbManageServerExporter");
			RetBean ret = manageServer.resendRmbRecord(failureBean);
			System.out.println("返回结果===>" + JSON.toString(ret));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static void test(TradingFailureBean failureBean) {
		String imix = "8=IMIX.1.09=147235=834=1370249=CFETS-RMB-CSTP52=20200825-05:40:07.51256=10228357=jsyh822601115=CFETS-RMB627=1628=Agent-JSYH1630=2471629=20200825-05:40:07.27311=5850432262600117=IBD2342000132=12430000.0044=1.54650048=IBD00154=460=20200825-13:40:07.05864=2020082575=20200825117=2023422000001150=F193=2020082610002=534166.6710014=110022=N10030=210176=5310289=34254166.6710317=110318=13:40:0710465=010751=111773=111788=432银行股份有限公司11789=资324中心11862=0453=2448=301346452=11910601=110602=0519-8602523510603=6802=10523=江苏而问题3号803=6523=热问题803=101523=342村商业银行803=125523=342股份有限公司803=124523=WURB803=102523=01888015010000004883803=15523=股份有限公司803=23523=江南农商行清算中心803=110523=314304083006803=112523=而已803=126448=102283452=12010601=110602=021-2053621610603=6802=10523=上4342342广场E座19层803=6523=张三803=101523=江苏银行803=125523=股份有限公司803=124523=JSBK803=102523=9903420000001803=15523=34242金系统往来803=23523=股份有限公司803=110523=313301099999803=112523=张三803=12693=5189=？？？僔？？？？？？搄沀？？？？？？？？夀勤？？？？？？？？？？？夀嬠？？　？？？|| :@)10=092";
		failureBean.setXml(imix);// 报文	
		failureBean.setDealNo("IBD23420001");// 交易编号 17 
		failureBean.setMarketIndicator("53");// 报文中的  10176
		failureBean.setTradeDate("20200825");// 报文中的 75
	}
}
