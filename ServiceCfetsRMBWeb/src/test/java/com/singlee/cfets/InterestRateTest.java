package com.singlee.cfets;

import org.eclipse.jetty.util.ajax.JSON;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.cfets.rmb.IImixRmbManageServer;
import com.singlee.financial.cfets.rmb.ImixRmbClientListener;
import com.singlee.financial.cfets.rmb.ImixRmbManageServer;
import com.singlee.financial.cfets.spring.SpringContextHolder;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;

import imix.ConfigError;
import imix.DataDictionary;
import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.InvalidMessage;
import imix.UnsupportedMessageType;
import imix.imix10.ExecutionReport;

/**
 * @apiNote 利率互换IMIX报文测试  测试通过
 * @author Administrator
 *
 */
public class InterestRateTest {
	public static void main(String[] args) {
		try {
			TradingFailureBean failureBean = new TradingFailureBean();
			test(failureBean);
			IImixRmbManageServer manageServer = (IImixRmbManageServer) new HessianProxyFactory().create(
					IImixRmbManageServer.class,
					"http://127.0.0.1:8081/fund-cfetsRmbWeb/api/service/IImixRmbManageServerExporter");
			RetBean ret = manageServer.resendRmbRecord(failureBean);
			System.out.println("返回结果===>" + JSON.toString(ret));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static void test(TradingFailureBean failureBean) {
		String imix = "8=IMIX.1.09=143735=834=3871349=CFETS-RMB-CSTP52=20200821-08:35:07.56256=10228357=jsyh822601115=CFETS-RMB627=1628=Agent-JSYH1630=7249629=20200821-08:35:07.31217=IRS2020433422732=343400048=FR007_1Y54=J55=FR007_1Y60=20200821-16:35:06.00075=20200821150=F916=20200824917=2021082410009=210018=110022=N10054=2020082410105=010176=210282=N10316=4332500010317=310318=16:35:0610346=310465=011143=6453=2448=102283452=11910601=110602=021-2053620910603=6802=11523=热舞803=101523=热问题803=126523=-803=6523=股份有限公司803=124523=江苏银行803=125523=JSBK803=102523=CFETS803=29523=江苏银行803=110523=43253435系统往来803=23523=99010159060000001803=15523=313301099999803=112448=100011452=12010601=110602=021-3318953810603=6802=11523=钱布克803=101523=钱布克803=126523=432589号803=6523=4325银行803=124523=浦发银行803=125523=SPDB803=102523=CFETS803=29523=342531024325）803=110523=上海4325银行803=23523=99010135800051002803=15523=310290000013803=112555=2624=1248=2020112410096=410097=3566=2.4475686=3624=B248=2020112410096=410097=3956=2020082110106=110108=6686=6677=FR00710090=0.0093=5689=？？？傼？？？？？？搄沀？？？？？？？？？？？？？？？？？？？？？夀嬠？？？？爆？嚄？？？？？| :@)10=075";
		failureBean.setXml(imix);// 报文	
		failureBean.setDealNo("IRS20204334227");// 交易编号 17 
		failureBean.setMarketIndicator("2");// 报文中的  10176
		failureBean.setTradeDate("20200821");// 报文中的 75
	}
}
