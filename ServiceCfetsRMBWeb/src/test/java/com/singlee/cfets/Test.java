package com.singlee.cfets;

import imix.ConfigError;
import imix.DataDictionary;
import imix.FieldNotFound;
import imix.InvalidMessage;
import imix.field.SendingTime;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;

public class Test {

    public static void main(String[] args) {
        String imix = "8=IMIX.1.09=48835=d34=249=CFETS-RMB-CSTP52=20211117-05:51:17.31556=10049557=hebcstp1115=CFETS-TS-MMDIIS-CSTP627=1628=Agent-HEB630=11629=20211117-05:54:13.77115=CNY48=11219062455=21哈尔滨银行CD00758=认购量为0167=CD965=210031=210280=N10465=010653=哈尔滨银行股份有限公司2021年第007期同业存单10706=611272=21516=11517=11000002301000010400111140=1191519=11520=哈尔滨银行股份有限公司1521=12493=2289=ᐒ䮐ޠ偤⹀ᒸ䁚ᨔƀ| :@)10=109";

        try {
            ExecutionReport message = new ExecutionReport();
            message.fromString(imix, new DataDictionary("IMIX10.xml"), false);
            String tradeDate = message.isSetField(TradeDate.FIELD) ? message.getField(new TradeDate()).getValue() : message.getField(new SendingTime()).getValue();
            System.out.println(tradeDate);
        } catch (InvalidMessage ex) {
            ex.printStackTrace();
        } catch (ConfigError | FieldNotFound ex) {
            ex.printStackTrace();
        }
    }
}
