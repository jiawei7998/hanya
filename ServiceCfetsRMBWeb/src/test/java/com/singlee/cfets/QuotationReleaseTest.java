package com.singlee.cfets;

import org.eclipse.jetty.util.ajax.JSON;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.cfets.rmb.IImixRmbManageServer;
import com.singlee.financial.cfets.rmb.ImixRmbClientListener;
import com.singlee.financial.cfets.rmb.ImixRmbManageServer;
import com.singlee.financial.cfets.spring.SpringContextHolder;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;

import imix.ConfigError;
import imix.DataDictionary;
import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.InvalidMessage;
import imix.UnsupportedMessageType;
import imix.imix10.ExecutionReport;

/**
 * @apiNote 报价发行结果IMIX报文测试 前置未实现
 * @author Administrator
 *
 */
public class QuotationReleaseTest {
	public static void main(String[] args) {
		try {
			TradingFailureBean failureBean = new TradingFailureBean();
			test(failureBean);
			IImixRmbManageServer manageServer = (IImixRmbManageServer) new HessianProxyFactory().create(
					IImixRmbManageServer.class,
					"http://127.0.0.1:8081/fund-cfetsRmbWeb/api/service/IImixRmbManageServerExporter");
			RetBean ret = manageServer.resendRmbRecord(failureBean);
			System.out.println("返回结果===>" + JSON.toString(ret));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	public static void test(TradingFailureBean failureBean) {
		String imix = "8=IMIX.1.09=137635=d34=4118349=CFETS-RMB-CSTP52=20201127-06:37:37.10756=10228357=jsyh822601115=CFETS-TS-MMDIIS-CSTP627=1628=Agent-JSYH1630=8164629=20201127-06:37:36.79815=CNY48=11207361855=20erret3e6167=CD965=110031=210076=99.171710078=4.510280=N10465=010653=erte股份有限公司2erert业存单10706=611272=31516=61517=111033254300111140=1191519=11520=同意让他有限公司1521=1241517=11100143510400110407=8934230.0011140=1201519=21520=张三1521=1701520=股份有限公司1521=12411303=0.911304=20201127-14:37:121517=11100113200000010400110407=843230.0011140=1201519=21520=3425煜1521=1701520=股345限公司1521=12411303=0.911304=20201127-14:37:241517=111001345300010400110407=83424530.0011140=1201519=21520=张三1521=1701520=股份有限公司1521=12411303=0.911304=20201127-14:37:301517=11154630000010400110407=3243580.0011140=1201519=21520=张三1521=1701520=股份有限公司1521=12411303=0.411304=20201127-14:37:361517=1110432500010400110407=893420.0011140=1201519=21520=张三1521=1701520=股份有限公司1521=12411303=0.911304=20201127-14:37:1993=4789=？？？偬？？？？？？搄梀？？？？？？Ζ？夀娠？？搄沀？？？爂？？？？？？？￠| :@)10=057";
		failureBean.setXml(imix);// 报文	
//		failureBean.setDealNo("CBT203425");// 交易编号 17 
//		failureBean.setMarketIndicator("4");// 报文中的  10176
//		failureBean.setTradeDate("20201202");// 报文中的 75
	}
}
