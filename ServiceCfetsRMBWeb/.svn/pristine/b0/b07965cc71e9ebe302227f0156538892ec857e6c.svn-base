package com.singlee.financial.cfets.rmb;


import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.field.*;
import imix.imix10.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Date;

/**
 * CFETS消息线程序处理
 *
 * @author chenxh
 */
public class ImixRmbMessageProcess extends Thread {
    //交易信息
    Message message;
    // Cfets接口服务对象
    ImixRmbCfetsApiService imixRmbCfetsApiService;
    // Cfets消息线程处理实现类
    ImixRmbMessageProcessImpl imixRmbMessageProcessImpl;

    private static Logger LogManager = LoggerFactory.getLogger(ImixRmbMessageProcess.class);
    // CFETS配置类
    private ConfigBean configBean;

    public ImixRmbMessageProcess(Message message, ImixRmbCfetsApiService imixRmbCfetsApiService, ConfigBean configBean) {
        this.message = message;
        this.imixRmbCfetsApiService = imixRmbCfetsApiService;
        this.configBean = configBean;
        imixRmbMessageProcessImpl = new ImixRmbMessageProcessImpl(configBean);

    }

    @Override
    public void run() {
        CfetsPackMsgBean cfetsPackMsgBean = null;
        try {
            cfetsPackMsgBean = builder(message);
            //解析报文并发送前置【CSTP】=>【前置】
            TradeBaseBean tradeBaseBean = imixRmbMessageProcessImpl.parseMsg(message);
            imixRmbMessageProcessImpl.sendToCapital(cfetsPackMsgBean, tradeBaseBean, imixRmbCfetsApiService);
            // 保存CFETS消息到XML文件中
            if (configBean.isSaveFile()) {
                LogManager.debug("开始保存文件.............");
                String filePath = configBean.getSaveFilePath() + File.separator + ConfigBean.dataPath + File.separator + String.format("%tF", new Date())
                        + File.separator + cfetsPackMsgBean.getExecid() + ConfigBean.txtSuffix;
                CfetsUtils.saveFile(cfetsPackMsgBean.getPackmsg(), filePath, configBean.getCharset());
            }
            // 发送确认消息到CFETS
            ImixRmbClient.sendMessage(message, cfetsPackMsgBean.getExecid(), ConfigBean.ExecAckStatus_Accept_Confirm);
        } catch (Exception e) {
            LogManager.error("CFETS 线程处理异常!", e);
            try {
                ImixRmbClient.sendMessage(message, cfetsPackMsgBean.getExecid(), ConfigBean.ExecAckStatus_Reject_Confirm);
            } catch (Exception e1) {
                LogManager.error("发送拒收交易[" + cfetsPackMsgBean.getExecid() + "]到CFETS服务器异常", e);
            }
        }
    }

    /**
     * 转换保存报文
     *
     * @param message
     * @return
     */
    CfetsPackMsgBean builder(Message message) {
        // 将CFETS消息转换为字符串
        CfetsPackMsgBean cfetsPackMsg = null;
        try {
            //申明对象
            cfetsPackMsg = new CfetsPackMsgBean();
            // 成交单号
            String execID = message.isSetField(ExecID.FIELD) ? message.getField(new ExecID()).getValue() : message.getField(new SecurityID()).getValue();
            cfetsPackMsg.setExecid(execID);
            // 成交日期
            String tradeDate = message.isSetField(TradeDate.FIELD) ? message.getField(new TradeDate()).getValue() : message.getHeader().getField(new SendingTime()).getValue();
            cfetsPackMsg.setTradedate(tradeDate);
            // 发送时间
            cfetsPackMsg.setBeginstring(message.getHeader().getString(SendingTime.FIELD));
            // 报文类型
            String dealStatus = message.isSetField(DealTransType.FIELD) ? String.valueOf(message.getField(new DealTransType()).getValue()) : ExecType.TRADE;
            cfetsPackMsg.setDealtranstype(dealStatus);
            // 市场标识
            String marketIndicator = message.isSetField(MarketIndicator.FIELD) ? message.getString(MarketIndicator.FIELD) : message.getField(new SecurityType()).getValue();
            cfetsPackMsg.setMarketindicator(marketIndicator);
            // 消息类型
            String msgType = message.getHeader().isSetField(MsgType.FIELD) ? message.getHeader().getString(MsgType.FIELD) : "";
            cfetsPackMsg.setMsgtype(msgType);
            // 报文
            cfetsPackMsg.setPackmsg(message.toString());
            LogManager.info("ExecutionReport Received marketIndicator:" + marketIndicator + ", execID:" + execID);
            return cfetsPackMsg;
        } catch (Exception e) {
            LogManager.error("转换保存对象失败异常", e);
        }
        return cfetsPackMsg;
    }
}
