@echo off
if '%1=='## goto ENVSET
SET LIBDIR=lib
SET CLSNAME=agent.Agent

SET CLSPATH=.
FOR %%c IN (%LIBDIR%\*.jar) DO CALL %0 ## %%c
GOTO RUN

:RUN
java -cp %CLSPATH% %CLSNAME%
goto END

:ENVSET
set CLSPATH=%CLSPATH%;%2
goto END

:END  
