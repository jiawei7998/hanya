package com.singlee.esb;

import com.singlee.capital.system.service.SystemProperties;
import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.bean.GJ0011.GJ0011Req;
import com.singlee.financial.esb.hbcb.bean.GJ0011.GJ0011Res;
import com.singlee.financial.esb.hbcb.bean.GJ0011.RequestBody;
import com.singlee.financial.esb.hbcb.bean.GJ0011.ResponseBody;
import com.singlee.financial.esb.hbcb.bean.GJ0011.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.GJ0011.SoapResBody;
import com.singlee.financial.esb.hbcb.bean.common.Detail;
import com.singlee.financial.esb.hbcb.bean.common.Fault;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.bean.common.ResponseHeader;
import com.singlee.financial.esb.hbcb.bean.common.SoapHeader;

import com.singlee.xstream.utils.XmlUtils;

public class GJ0011ReqText {
    /**
     * 当前操作返回码,返回信息,返回状态
     */
    public static void main(String[] args) throws Exception {
        try {

            EsbPacket<SoapReqBody> requestPakcet = getgJ0011Req();
            System.out.println("========================测试GJ0011请求报文==============");
            System.out.println(XmlUtils.toXml(requestPakcet));
            System.out.println("==================================================================");

            HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
            httpHeadAttrConfig.setCharset("GB2312");
            httpHeadAttrConfig.setUrl("www.cqrcb.com.cn");
//          httpHeadAttrConfig.setCharset("UTF-8");
            httpHeadAttrConfig.setConnTimeOut(3000);
            httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
            httpHeadAttrConfig.setContentLength(true);
            httpHeadAttrConfig.setContentMd5(true);
            String host = "130.1.12.152";
            String port = "8010";
            String server = "S010003010GJ0011";
            String strURL = "http://" + host+":"+ port+"/"+server;
            httpHeadAttrConfig.setUrl(strURL);
            // 初始化输出对象
            EsbPacket<SoapResBody> responsePakcet = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
                    "soapenv:Envelope");
            responsePakcet.setPackge_type("www.cqrcb.com.cn");
            responsePakcet.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
            responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);

            // 解析返回对象
            SoapResBody soapResBody = responsePakcet.getBody();
            Fault fault = soapResBody.getGJ0011Res().getFault();
//            String fileName=soapResBody.getGJ0011Res().getResponseBody().getFileName(); 
      System.out.println(soapResBody.getGJ0011Res().getResponseBody());
            System.out.println(String.format("当前操作返回码%s,返回信息%s,返回状态%s", fault.getFaultCode(), fault.getFaultString(),
                    fault.getDetail().getTxnStat()));
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 组装GJ0011请求对象
     * 
     * @param request
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static EsbPacket<SoapReqBody> getgJ0011Req()
            throws InstantiationException, IllegalAccessException {
        // 初始化请求对象
        EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
                "soapenv:Envelope");
        request.setPackge_type("www.cqrcb.com.cn");
        request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
        // 获取当前请求报文体内容
        SoapReqBody soapReqBody = request.getBody();
        // 申明报文对象
        GJ0011Req GJ0011ReqBody = new GJ0011Req();
        RequestBody requestBody = new RequestBody();
        RequestHeader requestHeader = new RequestHeader();
        // 设置body中的请求头信息

        requestHeader.setBrchNo("00100");
        
        // 设置报文中请求头对象
        GJ0011ReqBody.setRequestHeader(requestHeader);
        // body里面的内容
       requestBody.setMark("0");

//       
        // 设置body
        GJ0011ReqBody.setRequestBody(requestBody);
        // 设置报文
        soapReqBody.setGJ0011Req(GJ0011ReqBody);
        return request;
    }

    /**
     * 组装GJ0011返回对象
     * 
     * @param response
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    @SuppressWarnings("unused")
    private static EsbPacket<SoapResBody> getGJ0011Res()
            throws InstantiationException, IllegalAccessException {
        // 初始化输出对象
        EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
                "soapenv:Envelope");
        response.setPackge_type("www.cqrcb.com.cn");
        response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
        // 获取响应报文体
        SoapResBody soapResBody = response.getBody();
        // 申明报文对象
        GJ0011Res gJ0011ResBody = new GJ0011Res();
        ResponseBody responseBody = new ResponseBody();
        ResponseHeader responseHeader = new ResponseHeader();
//      responseHeader.setReqDt("01042021");
//      responseHeader.setFlag1("0");
//      responseHeader.setFlag4("5");
//      responseHeader.setBrchNo("00100");
//      responseHeader.setTlrNo("9001351");
//      responseHeader.setBancsSeqNo("0");
//      responseHeader.setTrmNo("000");
//      responseHeader.setChnlNo("80");
        gJ0011ResBody.setResponseHeader(responseHeader);
        Fault fault = new Fault();
        fault.setFaultCode("002001000000");
        fault.setFaultString("成功");
        Detail detail = new Detail();
        detail.setTxnStat("SUCCESS");
        fault.setDetail(detail);
        gJ0011ResBody.setFault(fault);
        // 设置body
        gJ0011ResBody.setResponseBody(responseBody);
        // 设置报文
        soapResBody.setGJ0011Res(gJ0011ResBody);
        return response;
    }

}
