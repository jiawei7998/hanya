package com.singlee.esb;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.esb.hbcb.bean.s100001003000103.*;
import com.singlee.xstream.utils.XmlUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class S100001003000103Test {

	/**
	 * 成功 ---当前操作返回码00200190002,返回信息失败:无记录,返回状态FAIL
	 */
	public static void main(String[] args) throws Exception {
		try {

			EsbPacket<SoapReqBody> requestPakcet = gets100001003000103Req();
			System.out.println("========================测试S100001003000103请求报文==============");
			System.out.println(XmlUtils.toXml(requestPakcet));
			System.out.println("==================================================================");

			HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
			httpHeadAttrConfig.setCharset("GB2312");
			httpHeadAttrConfig.setUrl("www.cqrcb.com.cn");
//			httpHeadAttrConfig.setCharset("UTF-8");
			httpHeadAttrConfig.setConnTimeOut(3000);
			httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
			httpHeadAttrConfig.setContentLength(true);
			httpHeadAttrConfig.setContentMd5(true);
			String host = "130.1.12.152";
			String port = "8010";
			String server = "S100001003000103";
			String strURL = "http://" + host+":"+ port+"/"+server;
			httpHeadAttrConfig.setUrl(strURL);
			// 初始化输出对象
			EsbPacket<SoapResBody> responsePakcet = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
					"soapenv:Envelope");
			responsePakcet.setPackge_type("www.cqrcb.com.cn");
			responsePakcet.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
			responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);

			// 解析返回对象
			SoapResBody soapResBody = responsePakcet.getBody();
			Fault fault = soapResBody.getS100001003000103Res().getFault();
			System.out.println(String.format("当前操作返回码%s,返回信息%s,返回状态%s", fault.getFaultCode(), fault.getFaultString(),
					fault.getDetail().getTxnStat()));
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 组装S100001003000103请求对象
	 * 
	 * @param request
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private static EsbPacket<SoapReqBody> gets100001003000103Req()
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
				"soapenv:Envelope");
		request.setPackge_type("www.cqrcb.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		// 申明报文对象
		S100001003000103Req s00103ReqBody = new S100001003000103Req();
		RequestBody requestBody = new RequestBody();
		RequestHeader requestHeader = new RequestHeader();
		// 设置body中的请求头信息
		requestHeader.setReqDt("01042021");
		requestHeader.setTerminalType("0");
		requestHeader.setFlag1("0");
		requestHeader.setFlag4("5");
		requestHeader.setChnlNo("80");
		requestHeader.setBrchNo("00100");
		requestHeader.setTrmNo("000");
		requestHeader.setTlrNo("9001351");
		requestHeader.setBancsSeqNo("0");
		requestHeader.setSupervisorID("0000000");
		requestHeader.setOldAcctFlag("0");
		requestHeader.setUUID(StringUtils.replace(UUID.randomUUID().toString(), "-", ""));
		
		ExchangeHeader exchangeHeader = new ExchangeHeader();
		requestHeader.setExchangeHeader(exchangeHeader);
		MasterHeader masterHeader = new MasterHeader();
		requestHeader.setMasterHeader(masterHeader);
		
		// 设置报文中请求头对象
		s00103ReqBody.setRequestHeader(requestHeader);
		// body里面的内容
		requestBody.setSTART_DT("20170825");
		requestBody.setEND_DT("20180825");
		requestBody.setGL_CODE1("5140204");
		requestBody.setGL_CODE2("10000");
		requestBody.setFCY_CODE("CNY");
		requestBody.setGL_BRCH("00100");
		// 设置body
		s00103ReqBody.setRequestBody(requestBody);
		// 设置报文
		soapReqBody.setS100001003000103Req(s00103ReqBody);
		return request;
	}

	/**
	 * 组装S100001003000103返回对象
	 * 
	 * @param response
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unused")
	private static EsbPacket<SoapResBody> getS100001003000103Res()
			throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
				"soapenv:Envelope");
		response.setPackge_type("www.cqrcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();
		// 申明报文对象
		S100001003000103Res s00103ResBody = new S100001003000103Res();
		ResponseBody responseBody = new ResponseBody();
		ResponseHeader responseHeader = new ResponseHeader();
		responseHeader.setReqDt("01042021");
		responseHeader.setFlag1("0");
		responseHeader.setFlag4("5");
		responseHeader.setBrchNo("00100");
		responseHeader.setTlrNo("9001351");
		responseHeader.setBancsSeqNo("0");
		responseHeader.setTrmNo("000");
		responseHeader.setChnlNo("80");
		s00103ResBody.setResponseHeader(responseHeader);
		Fault fault = new Fault();
		fault.setFaultCode("002001000000");
		fault.setFaultString("成功");
		Detail detail = new Detail();
		detail.setTxnStat("SUCCESS");
		fault.setDetail(detail);
		s00103ResBody.setFault(fault);
		// 设置body中循环标签
		List<ResDetailList> detailLists = new ArrayList<ResDetailList>();
		responseBody.setDetailList(detailLists);
		// 设置body
		s00103ResBody.setResponseBody(responseBody);
		// 设置报文
		soapResBody.setS100001003000103Res(s00103ResBody);
		return response;
	}
}
