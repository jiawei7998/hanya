package com.singlee.esb;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.esb.hbcb.bean.s002001010990002.*;
import com.singlee.xstream.utils.XmlUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class S002001010990002Test {

	/**
	 * 当前操作返回码,返回信息,返回状态
	 */
	public static void main(String[] args) throws Exception {
		try {

			EsbPacket<SoapReqBody> requestPakcet = gets002001010990002Req();
			System.out.println("========================测试S002001010990002请求报文==============");
			System.out.println(XmlUtils.toXml(requestPakcet));
			System.out.println("==================================================================");

			HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
			httpHeadAttrConfig.setCharset("GB2312");
			httpHeadAttrConfig.setUrl("www.cqrcb.com.cn");
//			httpHeadAttrConfig.setCharset("UTF-8");
			httpHeadAttrConfig.setConnTimeOut(3000);
			httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
			httpHeadAttrConfig.setContentLength(true);
			httpHeadAttrConfig.setContentMd5(true);
			String host = "130.1.12.152";
			String port = "8010";
			String server = "S002001010990002";
			String strURL = "http://" + host+":"+ port+"/"+server;
			httpHeadAttrConfig.setUrl(strURL);
			// 初始化输出对象
			EsbPacket<SoapResBody> responsePakcet = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
					"soapenv:Envelope");
			responsePakcet.setPackge_type("www.cqrcb.com.cn");
			responsePakcet.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
			responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);

			// 解析返回对象
			SoapResBody soapResBody = responsePakcet.getBody();
			Fault fault = soapResBody.getS002001010990002Res().getFault();
			System.out.println(String.format("当前操作返回码%s,返回信息%s,返回状态%s", fault.getFaultCode(), fault.getFaultString(),
					fault.getDetail().getTxnStat()));
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 组装S002001010990002请求对象
	 * 
	 * @param request
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private static EsbPacket<SoapReqBody> gets002001010990002Req()
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
				"soapenv:Envelope");
		request.setPackge_type("www.cqrcb.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		// 申明报文对象
		S002001010990002Req s90002ReqBody = new S002001010990002Req();
		RequestBody requestBody = new RequestBody();
		RequestHeader requestHeader = new RequestHeader();
		// 设置body中的请求头信息

		requestHeader.setBrchNo("00100");
		
		// 设置报文中请求头对象
		s90002ReqBody.setRequestHeader(requestHeader);
		// body里面的内容
		requestBody.setAcctNo("9001000150209300");
		requestBody.setChkMagFlg("0");
		requestBody.setExtndStat("1111100000000000");
		// 设置body
		s90002ReqBody.setRequestBody(requestBody);
		// 设置报文
		soapReqBody.setS002001010990002Req(s90002ReqBody);
		return request;
	}

	/**
	 * 组装S002001010990002返回对象
	 * 
	 * @param response
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unused")
	private static EsbPacket<SoapResBody> getS002001010990002Res()
			throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
				"soapenv:Envelope");
		response.setPackge_type("www.cqrcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();
		// 申明报文对象
		S002001010990002Res s90002ResBody = new S002001010990002Res();
		ResponseBody responseBody = new ResponseBody();
		ResponseHeader responseHeader = new ResponseHeader();
//		responseHeader.setReqDt("01042021");
//		responseHeader.setFlag1("0");
//		responseHeader.setFlag4("5");
//		responseHeader.setBrchNo("00100");
//		responseHeader.setTlrNo("9001351");
//		responseHeader.setBancsSeqNo("0");
//		responseHeader.setTrmNo("000");
//		responseHeader.setChnlNo("80");
		s90002ResBody.setResponseHeader(responseHeader);
		Fault fault = new Fault();
		fault.setFaultCode("002001000000");
		fault.setFaultString("成功");
		Detail detail = new Detail();
		detail.setTxnStat("SUCCESS");
		fault.setDetail(detail);
		s90002ResBody.setFault(fault);
		// 设置body
		s90002ResBody.setResponseBody(responseBody);
		// 设置报文
		soapResBody.setS002001010990002Res(s90002ResBody);
		return response;
	}
}
