package com.singlee.esb;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.*;
import com.singlee.xstream.utils.XmlUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Test {

	public static void main(String[] args) throws Exception {
		try {

			EsbPacket<SoapReqBody> requestPakcet = getS100001001084015Req();
			System.out.println("========================测试S100001001084015请求报文==============");
			System.out.println(XmlUtils.toXml(requestPakcet));
			System.out.println("==================================================================");

			HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
			httpHeadAttrConfig.setCharset("GB2312");
			httpHeadAttrConfig.setUrl("www.cqrcb.com.cn");
//			httpHeadAttrConfig.setCharset("UTF-8");
			httpHeadAttrConfig.setConnTimeOut(3000);
			httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
			httpHeadAttrConfig.setContentLength(true);
			httpHeadAttrConfig.setContentMd5(true);
			String host = "130.1.12.152";
			String port = "8010";
			String server = "S100001001084015";
			String strURL = "http://" + host+":"+ port+"/"+server;
			httpHeadAttrConfig.setUrl(strURL);
			// 初始化输出对象
			EsbPacket<SoapResBody> responsePakcet = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
					"soapenv:Envelope");
			responsePakcet.setPackge_type("www.cqrcb.com.cn");
			responsePakcet.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
			responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);

			// 解析返回对象
			SoapResBody soapResBody = responsePakcet.getBody();
			Fault fault = soapResBody.getS100001001084015Res().getFault();
			System.out.println(String.format("当前操作返回码%s,返回信息%s,返回状态%s", fault.getFaultCode(), fault.getFaultString(),
					fault.getDetail().getTxnStat()));
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 组装S100001001084015请求对象
	 * 
	 * @param request
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private static EsbPacket<SoapReqBody> getS100001001084015Req()
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
				"soapenv:Envelope");
		request.setPackge_type("www.cqrcb.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		// 申明报文对象
		S100001001084015Req s84015ReqBody = new S100001001084015Req();
		RequestBody requestBody = new RequestBody();
		RequestHeader requestHeader = new RequestHeader();
		ExchangeHeader exchangeHeader = new ExchangeHeader();
		// 设置body中的请求头信息
		requestHeader.setReqDt("31032021");
		requestHeader.setTerminalType("0");
		requestHeader.setFlag1("0");
		requestHeader.setFlag4("5");
		requestHeader.setChnlNo("80");
		requestHeader.setBrchNo("00100");
		requestHeader.setTrmNo("000");
		requestHeader.setTlrNo("9001351");
		requestHeader.setBancsSeqNo("0");
		requestHeader.setSupervisorID("0000000");
		requestHeader.setOldAcctFlag("0");
		requestHeader.setUUID(StringUtils.replace(UUID.randomUUID().toString(), "-", ""));
		requestHeader.setExchangeHeader(exchangeHeader);
		MasterHeader masterHeader = new MasterHeader();
		requestHeader.setMasterHeader(masterHeader);
		// 设置报文中请求头对象
		s84015ReqBody.setRequestHeader(requestHeader);
		// body里面的内容
		ReqDetail1 detail1 = new ReqDetail1();
		detail1.setTfroutBrch("00100");// 转出机构
		detail1.setTfroutProCode("83001020");// 转出提示码
		detail1.setTfroutAcctNo("83001020");// 转出帐号
		detail1.setTfroutAcctNoSys("CGL");// 科目账务
		detail1.setTfrInBrch("00100");// 转入机构
		detail1.setTfrinAcctNo("83001001");// 转入提示吗
		detail1.setTfrinAcctNoSys("CGL");
		detail1.setTfrInProCode("83001001");
		detail1.setAmt("30000000");
		detail1.setCurrency("USD");
		List<ReqDetail1> detail1s = new ArrayList<ReqDetail1>();
		detail1s.add(detail1);
		requestBody.setDetail1(detail1s);
		// 设置body
		s84015ReqBody.setRequestBody(requestBody);
		// 设置报文
		soapReqBody.setS100001001084015(s84015ReqBody);
		return request;
	}

	/**
	 * 组装S100001001084015返回对象
	 * 
	 * @param response
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private static EsbPacket<SoapResBody> getS100001001084015Res()
			throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
				"soapenv:Envelope");
		response.setPackge_type("www.cqrcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();
		// 申明报文对象
		S100001001084015Res s84015ResBody = new S100001001084015Res();
		ResponseBody responseBody = new ResponseBody();
		ResponseHeader responseHeader = new ResponseHeader();
		responseHeader.setReqDt("31032021");
		responseHeader.setFlag1("0");
		responseHeader.setFlag4("5");
		responseHeader.setBrchNo("00100");
		responseHeader.setTlrNo("9001351");
		responseHeader.setTrmNo("0");
		responseHeader.setChnlNo("000");
		s84015ResBody.setResponseHeader(responseHeader);
		Fault fault = new Fault();
		fault.setFaultCode("002001000000");
		fault.setFaultString("成功");
		Detail detail = new Detail();
		detail.setTxnStat("SUCCESS");
		fault.setDetail(detail);
		s84015ResBody.setFault(fault);
		// 设置body中循环标签
		List<ResDetailList> detailLists = new ArrayList<ResDetailList>();
		responseBody.setDetailList(detailLists);
		// 设置body
		s84015ResBody.setResponseBody(responseBody);
		// 设置报文
		soapResBody.setS100001001084015Res(s84015ResBody);
		return response;
	}
}
