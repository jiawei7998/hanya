package com.singlee.esb;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.esb.hbcb.bean.s003003990MS5702.*;
import com.singlee.xstream.utils.XmlUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class S003003990MS5702Test {

	/**
	 * 成功 --当前操作返回码001001230001,返回信息ESB-服务端DTA内部请求失败,返回状态FAIL
	 */
	public static void main(String[] args) throws Exception {
		try {

			EsbPacket<SoapReqBody> requestPakcet = gets003003990MS5702Req();
			System.out.println("========================测试S003003990MS5702请求报文==============");
			System.out.println(XmlUtils.toXml(requestPakcet));
			System.out.println("==================================================================");

			HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
			httpHeadAttrConfig.setCharset("GB2312");
			httpHeadAttrConfig.setUrl("www.cqrcb.com.cn");
//			httpHeadAttrConfig.setCharset("UTF-8");
			httpHeadAttrConfig.setConnTimeOut(3000);
			httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
			httpHeadAttrConfig.setContentLength(true);
			httpHeadAttrConfig.setContentMd5(true);
			String host = "130.1.12.152";
			String port = "8010";
			String server = "S003003990MS5702";
			String strURL = "http://" + host+":"+ port+"/"+server;
			httpHeadAttrConfig.setUrl(strURL);
			// 初始化输出对象
			EsbPacket<SoapResBody> responsePakcet = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
					"soapenv:Envelope");
			responsePakcet.setPackge_type("www.cqrcb.com.cn");
			responsePakcet.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
			responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);

			// 解析返回对象
			SoapResBody soapResBody = responsePakcet.getBody();
			Fault fault = soapResBody.getS003003990MS5702Res().getFault();
			System.out.println(String.format("当前操作返回码%s,返回信息%s,返回状态%s", fault.getFaultCode(), fault.getFaultString(),
					fault.getDetail().getTxnStat()));
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 组装S003003990MS5702请求对象
	 * 
	 * @param request
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	private static EsbPacket<SoapReqBody> gets003003990MS5702Req()
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
				"soapenv:Envelope");
		request.setPackge_type("www.adtec.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		// 申明报文对象
		S003003990MS5702Req sMS5702ReqBody = new S003003990MS5702Req();
		RequestBody requestBody = new RequestBody();
		RequestHeader requestHeader = new RequestHeader();
		// 设置body中的请求头信息
//		requestHeader.setReqDt("01042021");
//		requestHeader.setTerminalType("0");
//		requestHeader.setFlag1("0");
//		requestHeader.setFlag4("5");
//		requestHeader.setChnlNo("80");
//		requestHeader.setBrchNo("00100");
//		requestHeader.setTrmNo("000");
//		requestHeader.setTlrNo("9001351");
//		requestHeader.setBancsSeqNo("0");
//		requestHeader.setSupervisorID("0000000");
//		requestHeader.setOldAcctFlag("0");
//		requestHeader.setUUID(StringUtils.replace(UUID.randomUUID().toString(), "-", ""));
//		
//		ExchangeHeader exchangeHeader = new ExchangeHeader();
//		requestHeader.setExchangeHeader(exchangeHeader);
//		MasterHeader masterHeader = new MasterHeader();
//		requestHeader.setMasterHeader(masterHeader);
		
		requestHeader.setReqTm("20150410");
		requestHeader.setReqSeqNo("C01102761803104100010000132000");
		requestHeader.setChnlNo("91");
		requestHeader.setBrchNo("0100");
		requestHeader.setTlrNo("2388");
		
		// 设置报文中请求头对象
		sMS5702ReqBody.setRequestHeader(requestHeader);
		// body里面的内容
		requestBody.setSyscd("E1");
		requestBody.setProcuctflag("0");
		requestBody.setPaytyp("0101");
		requestBody.setFeemod("0");
		requestBody.setPriority("2");
		requestBody.setTxtpcd("A100");
		requestBody.setCtgypurpcd("02102");
		requestBody.setDbtrno("010001139010410000");
		requestBody.setDbtrnm("哈尔滨银行股份有限公司");
		requestBody.setCdtrno("173001562010000093");
		requestBody.setCdtrnm("广发银行股份有限公司");
		requestBody.setCdtbranchid("306581000003");
		requestBody.setTrftrno("010001139010410000");
		requestBody.setTrftrnm("哈尔滨银行股份有限公司");
		requestBody.setCcy("CNY");
		requestBody.setAmt("31041000.00");
		requestBody.setActflg("1");
		
		List<ReqMsgHead> msghead = new ArrayList<ReqMsgHead>();
		ReqMsgHead reqMsgHead = new ReqMsgHead();
		reqMsgHead.setChanid("91");
		reqMsgHead.setChandt("20150513");
		reqMsgHead.setChanflow("C01102761803104100010000132000");
		reqMsgHead.setBizdate("20150513");
		reqMsgHead.setTlno("2388");
		reqMsgHead.setSbno("0100");
		msghead.add(reqMsgHead);
		requestBody.setMsghead(msghead);
		
		// 设置body
		sMS5702ReqBody.setRequestBody(requestBody);
		// 设置报文
		soapReqBody.setS003003990MS5702Req(sMS5702ReqBody);
		return request;
	}

	/**
	 * 组装S003003990MS5702返回对象
	 * 
	 * @param response
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	@SuppressWarnings("unused")
	private static EsbPacket<SoapResBody> getS003003990MS5702Res()
			throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
				"soapenv:Envelope");
		response.setPackge_type("www.cqrcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();
		// 申明报文对象
		S003003990MS5702Res sMS5702ResBody = new S003003990MS5702Res();
		ResponseBody responseBody = new ResponseBody();
		ResponseHeader responseHeader = new ResponseHeader();
//		responseHeader.setReqDt("01042021");
//		responseHeader.setFlag1("0");
//		responseHeader.setFlag4("5");
//		responseHeader.setBrchNo("00100");
//		responseHeader.setTlrNo("9001351");
//		responseHeader.setBancsSeqNo("0");
//		responseHeader.setTrmNo("000");
//		responseHeader.setChnlNo("80");
		
		responseHeader.setReqTm("20150410");
		responseHeader.setReqSeqNo("C01102761803104100010000132000");
		responseHeader.setBrchNo("0100");
		responseHeader.setTlrNo("2388");
		responseHeader.setChnlNo("91");
		sMS5702ResBody.setResponseHeader(responseHeader);
		Fault fault = new Fault();
		fault.setFaultCode("003003AAAAAAA");
		fault.setFaultString("成功");
		Detail detail = new Detail();
		detail.setTxnStat("SUCCESS");
		fault.setDetail(detail);
		sMS5702ResBody.setFault(fault);
		// 设置body中循环标签
		List<ResReturnCode> resReturnCode = new ArrayList<ResReturnCode>();
		List<ResMsgHeadOut> resMsgHeadOut = new ArrayList<ResMsgHeadOut>();
		responseBody.setResReturnCode(resReturnCode);
		responseBody.setResMsgHeadOut(resMsgHeadOut);
		// 设置body
		sMS5702ResBody.setResponseBody(responseBody);
		// 设置报文
		soapResBody.setS003003990MS5702Res(sMS5702ResBody);
		return response;
	}
}
