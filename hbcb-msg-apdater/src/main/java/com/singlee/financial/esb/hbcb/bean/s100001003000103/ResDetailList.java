package com.singlee.financial.esb.hbcb.bean.s100001003000103;

import java.io.Serializable;

/**
 *
 */
public class ResDetailList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易日期
	 */
	private String TRANS_DATE;
	/**
	 * 借贷方向 D:借 C:贷
	 */
	private String DC_IND;
	/**
	 * 借贷发生额
	 */
	private String AMOUNT;
	/**
	 * 交易流水号
	 */
	private String JRNL_NO;
	/**
	 * 操作柜员号
	 */
	private String TELLER_NO;
	/**
	 * 记账机构
	 */
	private String GL_BRCH;
	/**
	 * 渠道号
	 */
	private String CHANNEL;
	/**
	 * 币种
	 */
	private String FCY_CODE;
	/**
	 * 科目号
	 */
	private String GL_CODE1;
	/**
	 * 科目序号
	 */
	private String GL_CODE2;
	/**
	 * 参考账号
	 */
	private String GLIF_REFERENCES;
	/**
	 * 冲正错账流水号
	 */
	private String CORR_JRNL_NO;
	/**
	 * 冲销标志
	 */
	private String CORR_IND;

	public String getTRANS_DATE() {
		return TRANS_DATE;
	}

	public void setTRANS_DATE(String tRANS_DATE) {
		TRANS_DATE = tRANS_DATE;
	}

	public String getDC_IND() {
		return DC_IND;
	}

	public void setDC_IND(String dC_IND) {
		DC_IND = dC_IND;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getJRNL_NO() {
		return JRNL_NO;
	}

	public void setJRNL_NO(String jRNL_NO) {
		JRNL_NO = jRNL_NO;
	}

	public String getTELLER_NO() {
		return TELLER_NO;
	}

	public void setTELLER_NO(String tELLER_NO) {
		TELLER_NO = tELLER_NO;
	}

	public String getGL_BRCH() {
		return GL_BRCH;
	}

	public void setGL_BRCH(String gL_BRCH) {
		GL_BRCH = gL_BRCH;
	}

	public String getCHANNEL() {
		return CHANNEL;
	}

	public void setCHANNEL(String cHANNEL) {
		CHANNEL = cHANNEL;
	}

	public String getFCY_CODE() {
		return FCY_CODE;
	}

	public void setFCY_CODE(String fCY_CODE) {
		FCY_CODE = fCY_CODE;
	}

	public String getGL_CODE1() {
		return GL_CODE1;
	}

	public void setGL_CODE1(String gL_CODE1) {
		GL_CODE1 = gL_CODE1;
	}

	public String getGL_CODE2() {
		return GL_CODE2;
	}

	public void setGL_CODE2(String gL_CODE2) {
		GL_CODE2 = gL_CODE2;
	}

	public String getGLIF_REFERENCES() {
		return GLIF_REFERENCES;
	}

	public void setGLIF_REFERENCES(String gLIF_REFERENCES) {
		GLIF_REFERENCES = gLIF_REFERENCES;
	}

	public String getCORR_JRNL_NO() {
		return CORR_JRNL_NO;
	}

	public void setCORR_JRNL_NO(String cORR_JRNL_NO) {
		CORR_JRNL_NO = cORR_JRNL_NO;
	}

	public String getCORR_IND() {
		return CORR_IND;
	}

	public void setCORR_IND(String cORR_IND) {
		CORR_IND = cORR_IND;
	}

}
