package com.singlee.financial.esb.hbcb.bean.GJ0011;

import java.io.Serializable;

/**
 * 中俄跨境支付平台系统通过ESB获取国结系统提供的牌价。
 * 
 *
 */
public class RequestBody implements Serializable {

    private static final long serialVersionUID = 1L;
   //备注信息
    private String mark;
    public String getMark() {
        return mark;
    }
    public void setMark(String mark) {
        this.mark = mark;
    }
    
}
