package com.singlee.financial.esb.hbcb.model;

import java.io.Serializable;
import java.util.List;

import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.ReqDetail1;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.ReqDetail2;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.RequestBody;

public class S84015Bean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 请求头
	 */
	private RequestHeader requestHeader;

	/**
	 * 
	 */
	private List<ReqDetail1> reqDetail1;

	/**
	 * 
	 */
	private List<ReqDetail2> reqDetail2;

	/**
	 * 请求体
	 */
	private RequestBody requestBody;

	public RequestHeader getRequestHeader() {
		return requestHeader;
	}

	public void setRequestHeader(RequestHeader requestHeader) {
		this.requestHeader = requestHeader;
	}

	public List<ReqDetail1> getReqDetail1() {
		return reqDetail1;
	}

	public void setReqDetail1(List<ReqDetail1> reqDetail1) {
		this.reqDetail1 = reqDetail1;
	}

	public List<ReqDetail2> getReqDetail2() {
		return reqDetail2;
	}

	public void setReqDetail2(List<ReqDetail2> reqDetail2) {
		this.reqDetail2 = reqDetail2;
	}

	public RequestBody getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(RequestBody requestBody) {
		this.requestBody = requestBody;
	}

}