package com.singlee.financial.esb.hbcb.bean.common;

import com.singlee.esb.client.converter.ListMultipleSameTagsConvert;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;
import com.singlee.esb.client.packet.EsbSysHeaderPacket;

/**
 * 主要适用于<soap:Header>内容
 * 
 * @author shenzl
 *
 */
public class SoapNoEnvHeader extends EsbSysHeaderPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public EsbSubPacketConvert getSysHeaderConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soap:Header");
		info.setSubPacketClassType(SoapNoEnvHeader.class);
		info.addAlias(EsbPacket.class, "soap:Header", new ListMultipleSameTagsConvert(info));
		return info;
	}

}
