package com.singlee.financial.esb.hbcb.bean.s100001001700105;

import java.io.Serializable;

import com.singlee.financial.esb.hbcb.bean.common.Fault;
import com.singlee.financial.esb.hbcb.bean.common.ResponseHeader;

/**
 *
 * 
 * 主要处理<ns:S100001001700105>标签
 * 
 *
 */
public class S100001001700105Res implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 响应头
	 */
	private ResponseHeader ResponseHeader;
	/**
	 * 响应体
	 */
	private ResponseBody ResponseBody;

	/**
	 * 包含返回的错误相关信息
	 */
	private Fault Fault;

	public Fault getFault() {
		return Fault;
	}

	public void setFault(Fault fault) {
		Fault = fault;
	}

	public ResponseHeader getResponseHeader() {
		return ResponseHeader;
	}

	public void setResponseHeader(ResponseHeader responseHeader) {
		ResponseHeader = responseHeader;
	}

	public ResponseBody getResponseBody() {
		return ResponseBody;
	}

	public void setResponseBody(ResponseBody responseBody) {
		ResponseBody = responseBody;
	}

}
