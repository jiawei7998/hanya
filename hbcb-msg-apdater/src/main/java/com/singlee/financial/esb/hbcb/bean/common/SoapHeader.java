package com.singlee.financial.esb.hbcb.bean.common;

import com.singlee.esb.client.converter.ListMultipleSameTagsConvert;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;
import com.singlee.esb.client.packet.EsbSysHeaderPacket;

/**
 * 主要适用于<soapenv:Header>内容
 * 
 * @author shenzl
 *
 */
public class SoapHeader extends EsbSysHeaderPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public EsbSubPacketConvert getSysHeaderConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Header");
		info.setSubPacketClassType(SoapHeader.class);
		info.addAlias(EsbPacket.class, "soapenv:Header", new ListMultipleSameTagsConvert(info));
		return info;
	}

}
