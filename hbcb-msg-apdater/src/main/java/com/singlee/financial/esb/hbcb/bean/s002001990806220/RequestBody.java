package com.singlee.financial.esb.hbcb.bean.s002001990806220;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S002001990806220>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	private String SumryCd;
	private String BachNo;

	public String getSumryCd() {
		return SumryCd;
	}

	public void setSumryCd(String sumryCd) {
		SumryCd = sumryCd;
	}

	public String getBachNo() {
		return BachNo;
	}

	public void setBachNo(String bachNo) {
		BachNo = bachNo;
	}

}
