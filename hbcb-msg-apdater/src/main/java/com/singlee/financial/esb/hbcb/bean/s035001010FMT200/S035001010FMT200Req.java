package com.singlee.financial.esb.hbcb.bean.s035001010FMT200;

import java.io.Serializable;

import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;

/**
 *境内外外币支付接口
 * 
 * 主要处理<cqr:S035001010FMT200>标签
 * 
 *
 */
public class S035001010FMT200Req implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 请求头
	 */
	private RequestHeader RequestHeader;
	/**
	 * 请求体
	 */
	private RequestBody RequestBody;

	public RequestHeader getRequestHeader() {
		return RequestHeader;
	}

	public void setRequestHeader(RequestHeader requestHeader) {
		RequestHeader = requestHeader;
	}

	public RequestBody getRequestBody() {
		return RequestBody;
	}

	public void setRequestBody(RequestBody requestBody) {
		RequestBody = requestBody;
	}

}
