package com.singlee.financial.esb.hbcb.bean.s002001990806220;

import java.io.Serializable;

import com.singlee.financial.esb.hbcb.bean.common.Fault;
import com.singlee.financial.esb.hbcb.bean.common.ResponseHeader;

/**
 * 传账接口
 * 
 * 主要处理<ns:S002001990806220>标签
 * 
 *
 */
public class S002001990806220Res implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 响应头
	 */
	private ResponseHeader responseHeader;
	/**
	 * 响应体
	 */
	private ResponseBody responseBody;

	/**
	 * 包含返回的错误相关信息
	 */
	private Fault Fault;

	public Fault getFault() {
		return Fault;
	}

	public void setFault(Fault fault) {
		Fault = fault;
	}

	public ResponseHeader getResponseHeader() {
		return responseHeader;
	}

	public void setResponseHeader(ResponseHeader responseHeader) {
		this.responseHeader = responseHeader;
	}

	public ResponseBody getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(ResponseBody responseBody) {
		this.responseBody = responseBody;
	}

}
