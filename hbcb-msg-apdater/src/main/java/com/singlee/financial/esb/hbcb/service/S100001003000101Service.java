package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001003000101.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001003000101.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S3000101Bean;

public interface S100001003000101Service {

	/**
	 * 
	 * @param s3000101Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S3000101Bean s3000101Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001003000101Req(S3000101Bean s3000101Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001003000101Res() throws InstantiationException, IllegalAccessException;
}
