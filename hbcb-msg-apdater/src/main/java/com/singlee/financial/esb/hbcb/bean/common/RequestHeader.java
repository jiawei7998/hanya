package com.singlee.financial.esb.hbcb.bean.common;

import java.io.Serializable;

/**
 * 子标记包含报文信息中公用请求头中的所有信息。
 * 
 * 其中又包括了<ExchangeHeader>兑换信息和<MasterHeader>半自动收费信息
 * 
 * @author shenzl
 *
 */
public class RequestHeader implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 版本号 非必输
	 */
	private String VerNo;
	/**
	 * 请求方系统代码 非必输
	 */
	private String ReqSysCd;

	/**
	 * 请求方安全节点号 非必输
	 */
	private String ReqSecCd;

	/**
	 * 交易类型 非必输
	 */
	private String TxnTyp;
	/**
	 * 交易模式 非必输
	 */
	private String TxnMod;
	/**
	 * 交易码 非必输
	 */
	private String TxnCd;
	/**
	 * 公共交易名称 非必输
	 */
	private String TxnNme;
	/**
	 * 请求方交易日期 输入前置日期ddmmyyyy；输出核心帐务日期ddmmyyyy
	 */
	private String ReqDt;
	/**
	 * 请求方交易时间戳
	 */
	private String ReqTm;
	/**
	 * 半自动/自动标识 正常交易置空，如果发生预收费确认交易时，上送值为3。
	 */
	private String AutoFlag;
	/**
	 * 终端类型 输入有效值：0，1，2 0：柜员前端和ESB
	 */
	private String TerminalType;
	/**
	 * 标识1 0-正常交易 1-倒退日交易 2-正常授权交易 3-倒退日授权交易 4,5,6,7ATM/POS交易使用 倒退日交易见详细的各个模块交易说明
	 */
	private String Flag1;
	/**
	 * 查看多包的返回，
	 * 
	 * 标识2 输入时为0，输出时的情况请见后面的描述1。 输出
	 * STRT-AREA-IEXEC-CNTL.，2或6说明这是输出序列中最后的信息（在这张表后有详细的解释） 0——同一屏幕还有后续的数据
	 * 2——同一屏幕没有后续的数据 4——安排新的屏幕显示后续数据 6——安排新的屏幕没有后续数据
	 */
	private String Flag2;
	/**
	 * 标识4 渠道标识： 0-柜面 5-ESB（表示渠道交易）
	 */
	private String Flag4;
	/**
	 * 渠道号
	 */
	private String ChnlNo;
	/**
	 * 机构号
	 */
	private String BrchNo;
	/**
	 * 公共交易机构名称
	 */
	private String BrchNme;
	/**
	 * 片段数 输入/输出都填“** ”，它能使Bancs内的序列号检查失效。置**+右补两个空格， 可以不用输入
	 */
	private String SegNo;
	/**
	 * 终端号 柜员的终端号是通过银行内部分配的；渠道的终端号为“000”。
	 */
	private String TrmNo;
	/**
	 * 终端IP地址 非必输
	 */
	private String TrmIP;
	/**
	 * 柜员号
	 */
	private String TlrNo;
	/**
	 * 流水号 输入填0，输出为bancs的流水号. 多金融组合交易的流水号是一样的
	 */
	private String BancsSeqNo;
	/**
	 * 柜员名称 非必输
	 */
	private String TlrNme;
	/**
	 * 操作柜员级别 非必输
	 */
	private String TlrLvl;
	/**
	 * 操作员种类 非必输
	 */
	private String TlrTyp;
	/**
	 * 柜员密码 非必输
	 */
	private String TlrPwd;
	/**
	 * 授权柜员 非必输
	 */
	private String AuthTlr;
	/**
	 * 授权码 非必输
	 */
	private String AuthCd;
	/**
	 * 授权标志 非必输
	 */
	private String AuthFlg;
	/**
	 * 授权信息 非必输
	 */
	private String AuthDisc;
	/**
	 * 授权执行动作 非必输
	 */
	private String AuthWk;
	/**
	 * 发送文件名 非必输
	 */
	private String SndFileNme;
	/**
	 * 开始记录数 非必输
	 */
	private String BgnRec;
	/**
	 * 一次查询最大记录数 非必输
	 */
	private String MaxRec;
	/**
	 * 文件MAC值 非必输
	 */
	private String FileHMac;
	/**
	 * 报文MAC值 非必输
	 */
	private String HMac;
	/**
	 * 子系统渠道
	 */
	private String SubsystemChannel;
	/**
	 * 主管Id
	 * 复核或授权时输入主管id号，复核或授权的时候才有效，其他建议填”0000000”。主管ID只提供给柜面前端使用，其他外围系统的授权处理在各自的业务系统
	 */
	private String SupervisorID;
	/**
	 * 旧账号标识 在相应交易码的xml文件里面若字段ProvinceCode有2段（ProvinceCode1、ProvinceCode2），填2；
	 * 在相应交易码的xml文件里面若字段ProvinceCode有1段，填1； 在相应交易码的xml文件里面若字段ProvinceCode有0段，填0；
	 * 表示发送给Bancs交易的旧帐号栏位的个数，见各个具体交易。 有一个旧账号栏位，值为1； 有两个旧账号栏位，值为2； 其他情况，值为0。
	 */
	private String OldAcctFlag;
	/**
	 * UUID流水号
	 */
	private String UUID;
	/**
	 * 兑换信息
	 */
	private ExchangeHeader ExchangeHeader;
	/**
	 * 半自动收费信息
	 */
	private MasterHeader MasterHeader;
	
	private String ReqSeqNo;

	public String getVerNo() {
		return VerNo;
	}

	public void setVerNo(String verNo) {
		VerNo = verNo;
	}

	public String getReqSysCd() {
		return ReqSysCd;
	}

	public void setReqSysCd(String reqSysCd) {
		ReqSysCd = reqSysCd;
	}

	public String getReqSecCd() {
		return ReqSecCd;
	}

	public void setReqSecCd(String reqSecCd) {
		ReqSecCd = reqSecCd;
	}

	public String getTxnTyp() {
		return TxnTyp;
	}

	public void setTxnTyp(String txnTyp) {
		TxnTyp = txnTyp;
	}

	public String getTxnMod() {
		return TxnMod;
	}

	public void setTxnMod(String txnMod) {
		TxnMod = txnMod;
	}

	public String getTxnCd() {
		return TxnCd;
	}

	public void setTxnCd(String txnCd) {
		TxnCd = txnCd;
	}

	public String getTxnNme() {
		return TxnNme;
	}

	public void setTxnNme(String txnNme) {
		TxnNme = txnNme;
	}

	public String getReqDt() {
		return ReqDt;
	}

	public void setReqDt(String reqDt) {
		ReqDt = reqDt;
	}

	public String getReqTm() {
		return ReqTm;
	}

	public void setReqTm(String reqTm) {
		ReqTm = reqTm;
	}

	public String getAutoFlag() {
		return AutoFlag;
	}

	public void setAutoFlag(String autoFlag) {
		AutoFlag = autoFlag;
	}

	public String getTerminalType() {
		return TerminalType;
	}

	public void setTerminalType(String terminalType) {
		TerminalType = terminalType;
	}

	public String getFlag1() {
		return Flag1;
	}

	public void setFlag1(String flag1) {
		Flag1 = flag1;
	}

	public String getFlag2() {
		return Flag2;
	}

	public void setFlag2(String flag2) {
		Flag2 = flag2;
	}

	public String getFlag4() {
		return Flag4;
	}

	public void setFlag4(String flag4) {
		Flag4 = flag4;
	}

	public String getChnlNo() {
		return ChnlNo;
	}

	public void setChnlNo(String chnlNo) {
		ChnlNo = chnlNo;
	}

	public String getBrchNo() {
		return BrchNo;
	}

	public void setBrchNo(String brchNo) {
		BrchNo = brchNo;
	}

	public String getBrchNme() {
		return BrchNme;
	}

	public void setBrchNme(String brchNme) {
		BrchNme = brchNme;
	}

	public String getSegNo() {
		return SegNo;
	}

	public void setSegNo(String segNo) {
		SegNo = segNo;
	}

	public String getTrmNo() {
		return TrmNo;
	}

	public void setTrmNo(String trmNo) {
		TrmNo = trmNo;
	}

	public String getTrmIP() {
		return TrmIP;
	}

	public void setTrmIP(String trmIP) {
		TrmIP = trmIP;
	}

	public String getTlrNo() {
		return TlrNo;
	}

	public void setTlrNo(String tlrNo) {
		TlrNo = tlrNo;
	}

	public String getBancsSeqNo() {
		return BancsSeqNo;
	}

	public void setBancsSeqNo(String bancsSeqNo) {
		BancsSeqNo = bancsSeqNo;
	}

	public String getTlrNme() {
		return TlrNme;
	}

	public void setTlrNme(String tlrNme) {
		TlrNme = tlrNme;
	}

	public String getTlrLvl() {
		return TlrLvl;
	}

	public void setTlrLvl(String tlrLvl) {
		TlrLvl = tlrLvl;
	}

	public String getTlrTyp() {
		return TlrTyp;
	}

	public void setTlrTyp(String tlrTyp) {
		TlrTyp = tlrTyp;
	}

	public String getTlrPwd() {
		return TlrPwd;
	}

	public void setTlrPwd(String tlrPwd) {
		TlrPwd = tlrPwd;
	}

	public String getAuthTlr() {
		return AuthTlr;
	}

	public void setAuthTlr(String authTlr) {
		AuthTlr = authTlr;
	}

	public String getAuthCd() {
		return AuthCd;
	}

	public void setAuthCd(String authCd) {
		AuthCd = authCd;
	}

	public String getAuthFlg() {
		return AuthFlg;
	}

	public void setAuthFlg(String authFlg) {
		AuthFlg = authFlg;
	}

	public String getAuthDisc() {
		return AuthDisc;
	}

	public void setAuthDisc(String authDisc) {
		AuthDisc = authDisc;
	}

	public String getAuthWk() {
		return AuthWk;
	}

	public void setAuthWk(String authWk) {
		AuthWk = authWk;
	}

	public String getSndFileNme() {
		return SndFileNme;
	}

	public void setSndFileNme(String sndFileNme) {
		SndFileNme = sndFileNme;
	}

	public String getBgnRec() {
		return BgnRec;
	}

	public void setBgnRec(String bgnRec) {
		BgnRec = bgnRec;
	}

	public String getMaxRec() {
		return MaxRec;
	}

	public void setMaxRec(String maxRec) {
		MaxRec = maxRec;
	}

	public String getFileHMac() {
		return FileHMac;
	}

	public void setFileHMac(String fileHMac) {
		FileHMac = fileHMac;
	}

	public String getHMac() {
		return HMac;
	}

	public void setHMac(String hMac) {
		HMac = hMac;
	}

	public String getSubsystemChannel() {
		return SubsystemChannel;
	}

	public void setSubsystemChannel(String subsystemChannel) {
		SubsystemChannel = subsystemChannel;
	}

	public String getSupervisorID() {
		return SupervisorID;
	}

	public void setSupervisorID(String supervisorID) {
		SupervisorID = supervisorID;
	}

	public String getOldAcctFlag() {
		return OldAcctFlag;
	}

	public void setOldAcctFlag(String oldAcctFlag) {
		OldAcctFlag = oldAcctFlag;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

	public ExchangeHeader getExchangeHeader() {
		return ExchangeHeader;
	}

	public void setExchangeHeader(ExchangeHeader exchangeHeader) {
		ExchangeHeader = exchangeHeader;
	}

	public MasterHeader getMasterHeader() {
		return MasterHeader;
	}

	public void setMasterHeader(MasterHeader masterHeader) {
		MasterHeader = masterHeader;
	}

	public String getReqSeqNo() {
		return ReqSeqNo;
	}

	public void setReqSeqNo(String reqSeqNo) {
		ReqSeqNo = reqSeqNo;
	}

}
