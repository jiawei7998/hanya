package com.singlee.financial.esb.hbcb.bean.s002001010176013;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S002001010176013>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	private String AcctNo;
	private String StartDt;
	private String EndDt;
	private String CxlAcctSeqNo;

	public String getAcctNo() {
		return AcctNo;
	}

	public void setAcctNo(String acctNo) {
		AcctNo = acctNo;
	}

	public String getStartDt() {
		return StartDt;
	}

	public void setStartDt(String startDt) {
		StartDt = startDt;
	}

	public String getEndDt() {
		return EndDt;
	}

	public void setEndDt(String endDt) {
		EndDt = endDt;
	}

	public String getCxlAcctSeqNo() {
		return CxlAcctSeqNo;
	}

	public void setCxlAcctSeqNo(String cxlAcctSeqNo) {
		CxlAcctSeqNo = cxlAcctSeqNo;
	}

}
