package com.singlee.financial.esb.hbcb.bean.GJ0011;



/**
 * 中俄跨境支付平台系统通过ESB获取国结系统提供的牌价。
 * 返回体
 *
 */
public class ResponseBody {
    //返回码
    private String returnCode;
    //牌价文件全路径
    private String File1Name;
    //牌价文件MD5码
    private String File1Md5;
   
    //备注信息
    private String memo;

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getFile1Name() {
        return File1Name;
    }

    public void setFile1Name(String file1Name) {
        File1Name = file1Name;
    }

    public String getFile1Md5() {
        return File1Md5;
    }

    public void setFile1Md5(String file1Md5) {
        File1Md5 = file1Md5;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }


    
}
