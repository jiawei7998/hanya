package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.bean.s003003990MS6770.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s003003990MS6770.SoapResBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.MS6770Bean;


public interface S003003990MS6770Service  {

	/**
	 * 发送Esb报文
	 * @param mS6671Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(MS6770Bean mS6770Bean) throws Exception;
	
	/**
	 * 组装请求报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets003003990MS6770Req(MS6770Bean mS6770Bean) throws InstantiationException, IllegalAccessException;
	
	/**
	 * 初始化响应报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS003003990MS6770Res() throws InstantiationException, IllegalAccessException;
}
