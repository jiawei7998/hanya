package com.singlee.financial.esb.hbcb.bean.s100001001700105;


/**
 * 请求返回体 主要应用与<ns:S100001001700105>标签
 * 
 *
 */
public class ResponseBody {

	/**
	 * 文件处理状态 主要针对查询功能 
	 * 0：已接收
	 * 1：正在处理
	 * 4：处理完毕
	 * 6：处理完毕未传出
	 * 9：已撤销
	 * 7：文件已生成并已上送FTPSVR
	 * 8：查询失败
	 * 
	 * 外部接口批量出错信息
	 * 
	 * 接口和前置报文中文件状态值如下：
	 * 0.文件已接收
	 * 1.正在预处理
	 * 2.预处理失败（生成返回文件）
	 * 3.正在记帐处理
	 * 4.记帐完毕
	 * 5.记帐完毕，生成返回文件失败
	 * 6.记帐完毕，生成返回文件成功（没有传文件到FtpServer服务器)
	 * 7.批量交易处理完成（已传文件到FtpServer服务器)
	 * 8.无此批量交易记录
	 * 9.已撤销
	 * 
	 * 关于批量交易返回码的定义
	 * ‘000000’：交易成功
	 * ‘000001’：交易处理失败
	 * ‘000002’：文件传输失败
	 * ‘000099’：系统忙，请稍后再发。
	 * 
	 * 关于对帐交易返回码的定义
	 * ‘0’：交易成功
	 * ‘1’：交易正在处理
	 * ‘2’：交易失败
	 * ‘3’：无符合条件的记录
	 * ‘9’：交易系统忙，请稍后再发。
	 */
	private String BatFileStat;

	public String getBatFileStat() {
		return BatFileStat;
	}

	public void setBatFileStat(String batFileStat) {
		BatFileStat = batFileStat;
	}
	
}
