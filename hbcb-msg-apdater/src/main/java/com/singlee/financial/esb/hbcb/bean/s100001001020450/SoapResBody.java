package com.singlee.financial.esb.hbcb.bean.s100001001020450;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前返回报文S100001001020450
 * 
 *
 */
public class SoapResBody extends EsbBodyPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private S100001001020450Res s100001001020450Res;

	public S100001001020450Res getS100001001020450Res() {
		return s100001001020450Res;
	}

	public void setS100001001020450Res(S100001001020450Res s100001001020450Res) {
		this.s100001001020450Res = s100001001020450Res;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapResBody.class);
		// 给当前类起别名
		info.addAlias(SoapResBody.class, "s100001001020450Res", "ns:S100001001020450");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:ns");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		info.addAlias(ResponseBody.class, "detail01", "Detail01",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detail05", "Detail05",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detail20", "Detail20",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detail21", "Detail21",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detail22", "Detail22",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detailOth22", "DetailOth22",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detail36", "Detail36",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detail69", "Detail69",ResDetailList.class);
		info.addAlias(ResponseBody.class, "detail70", "Detail70",ResDetailList.class);
		return info;
	}

}
