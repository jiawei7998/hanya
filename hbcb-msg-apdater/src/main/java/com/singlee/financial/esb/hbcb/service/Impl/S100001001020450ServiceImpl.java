package com.singlee.financial.esb.hbcb.service.Impl;

import org.springframework.stereotype.Service;
import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.esb.hbcb.bean.s100001001020450.*;
import com.singlee.financial.esb.hbcb.model.S20450Bean;
import com.singlee.financial.esb.hbcb.service.S100001001020450Service;
import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.xstream.utils.XmlUtils;

@Service
public class S100001001020450ServiceImpl implements S100001001020450Service {

	@Override
	public EsbOutBean send(S20450Bean s20450Bean) throws Exception {
		// 初始化请求报文
		EsbPacket<SoapReqBody> requestPakcet = gets100001001020450Req(s20450Bean);
		// 初始化HttpHeadAttrConfig
		HttpHeadAttrConfig httpHeadAttrConfig = EsbSend.getHttpHeadAttrConfig("S100001001020450", "GB2312");
		// 初始化输出对象
		EsbPacket<SoapResBody> responsePakcet = getS100001001020450Res();
		// 发送报文
		responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);
		// 解析返回对象
		SoapResBody soapResBody = responsePakcet.getBody();
		Fault fault = soapResBody.getS100001001020450Res().getFault();

		// 封装返回对象
		EsbOutBean result = new EsbOutBean();
		// result.setClientNo(resMsgHeadOut.getMsgdetailflow());//二代交易流水号
		// result.setTellSeqNo(resMsgHeadOut.getCoreflow());//二代核心流水号
		result.setSendmsg(XmlUtils.toXml(requestPakcet));
		result.setRecmsg(XmlUtils.toXml(responsePakcet));
		result.setRetCode(fault.getFaultCode());
		result.setRetMsg(fault.getFaultString());
		return result;
	}

	@Override
	public EsbPacket<SoapReqBody> gets100001001020450Req(S20450Bean s20450Bean)
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
				"soapenv:Envelope");
		request.setPackge_type("http://www.cqrcb.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope/");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		S100001001020450Req s20450ReqBody = new S100001001020450Req();
		// 设置报文中请求头对象
		s20450ReqBody.setRequestHeader(s20450Bean.getRequestHeader());

		RequestBody requestBody = s20450Bean.getRequestBody();
		// 设置body
		s20450ReqBody.setRequestBody(requestBody);

		soapReqBody.setS100001001020450Req(s20450ReqBody);
		return request;
	}

	@Override
	public EsbPacket<SoapResBody> getS100001001020450Res() throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
				"soapenv:Envelope");
		response.setPackge_type("http://www.hrbcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope/");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();

		S100001001020450Res s20450ResBody = new S100001001020450Res();
		// 初始化响应头 并设置
		s20450ResBody.setResponseHeader(new ResponseHeader());

		// 初始化响应体并设置
		ResponseBody responseBody = new ResponseBody();
		
		s20450ResBody.setResponseBody(responseBody);

		// 初始化错误类 并设置
		Fault fault = new Fault();
		Detail detail = new Detail();
		fault.setDetail(detail);
		s20450ResBody.setFault(fault);

		soapResBody.setS100001001020450Res(s20450ResBody);
		return response;
	}

}
