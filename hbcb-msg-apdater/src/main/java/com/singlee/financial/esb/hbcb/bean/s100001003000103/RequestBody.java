package com.singlee.financial.esb.hbcb.bean.s100001003000103;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S100001003000103>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 开始日期 M
	 */
	private String START_DT;
	/**
	 * 截止日期 M
	 */
	private String END_DT;
	/**
	 * 科目号 M
	 */
	private String GL_CODE1;
	/**
	 * 科目序号 O
	 */
	private String GL_CODE2;
	/**
	 * 流水号 O
	 */
	private String JRNL_NO;
	/**
	 * 金额 O
	 */
	private String AMOUNT;
	/**
	 * 币种 O
	 */
	private String FCY_CODE;
	/**
	 * 记账机构 O
	 */
	private String GL_BRCH;

	public String getSTART_DT() {
		return START_DT;
	}

	public void setSTART_DT(String sTART_DT) {
		START_DT = sTART_DT;
	}

	public String getEND_DT() {
		return END_DT;
	}

	public void setEND_DT(String eND_DT) {
		END_DT = eND_DT;
	}

	public String getGL_CODE1() {
		return GL_CODE1;
	}

	public void setGL_CODE1(String gL_CODE1) {
		GL_CODE1 = gL_CODE1;
	}

	public String getGL_CODE2() {
		return GL_CODE2;
	}

	public void setGL_CODE2(String gL_CODE2) {
		GL_CODE2 = gL_CODE2;
	}

	public String getJRNL_NO() {
		return JRNL_NO;
	}

	public void setJRNL_NO(String jRNL_NO) {
		JRNL_NO = jRNL_NO;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getFCY_CODE() {
		return FCY_CODE;
	}

	public void setFCY_CODE(String fCY_CODE) {
		FCY_CODE = fCY_CODE;
	}

	public String getGL_BRCH() {
		return GL_BRCH;
	}

	public void setGL_BRCH(String gL_BRCH) {
		GL_BRCH = gL_BRCH;
	}

}
