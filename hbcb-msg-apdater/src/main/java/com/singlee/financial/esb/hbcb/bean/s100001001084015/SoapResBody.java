package com.singlee.financial.esb.hbcb.bean.s100001001084015;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前返回报文S100001001084015
 * 
 * @author shenzl
 *
 */
public class SoapResBody extends EsbBodyPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private S100001001084015Res s100001001084015Res;

	public S100001001084015Res getS100001001084015Res() {
		return s100001001084015Res;
	}

	public void setS100001001084015Res(S100001001084015Res s100001001084015Res) {
		this.s100001001084015Res = s100001001084015Res;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapResBody.class);
		// 给当前类起别名
		info.addAlias(SoapResBody.class, "s100001001084015Res", "ns:S100001001084015");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:ns");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		info.addAlias(ResponseBody.class, "detailList", "Detail1", ResDetailList.class);
		return info;
	}

}
