package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s002001010176013.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s002001010176013.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S176013Bean;

public interface S002001010176013Service {

	/**
	 * 
	 * @param s176013Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S176013Bean s176013Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets002001010176013Req(S176013Bean s176013Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS002001010176013Res() throws InstantiationException, IllegalAccessException;
}
