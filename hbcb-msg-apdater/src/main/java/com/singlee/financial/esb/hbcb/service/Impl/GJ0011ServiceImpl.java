package com.singlee.financial.esb.hbcb.service.Impl;

import org.springframework.stereotype.Service;
import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.bean.GJ0011.GJ0011Req;
import com.singlee.financial.esb.hbcb.bean.GJ0011.GJ0011Res;
import com.singlee.financial.esb.hbcb.bean.GJ0011.RequestBody;
import com.singlee.financial.esb.hbcb.bean.GJ0011.ResponseBody;
import com.singlee.financial.esb.hbcb.bean.GJ0011.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.GJ0011.SoapResBody;
import com.singlee.financial.esb.hbcb.bean.common.*;

import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.GJ0011Bean;
import com.singlee.financial.esb.hbcb.service.GJ0011Service;

import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.xstream.utils.XmlUtils;

@Service
public class GJ0011ServiceImpl implements GJ0011Service {

	@Override
	public EsbOutBean send(GJ0011Bean gJ0011Bean) throws Exception {
		// 初始化请求报文
		EsbPacket<SoapReqBody> requestPakcet = getgJ0011Req(gJ0011Bean);
		// 初始化HttpHeadAttrConfig
		HttpHeadAttrConfig httpHeadAttrConfig = EsbSend.getHttpHeadAttrConfig("S010003010GJ0011", "GB2312");
		// 初始化输出对象
		EsbPacket<SoapResBody> responsePakcet = getGJ0011Res();
		// 发送报文
		responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);
		// 解析返回对象
		SoapResBody soapResBody = responsePakcet.getBody();
		Fault fault = soapResBody.getGJ0011Res().getFault();

		// 封装返回对象
		EsbOutBean result = new EsbOutBean();
		result.setSendmsg(XmlUtils.toXml(requestPakcet));
		result.setRecmsg(XmlUtils.toXml(responsePakcet));
		result.setRetCode(fault.getFaultCode());
		result.setRetMsg(fault.getFaultString());
		return result;
	}
	
	@Override
	public EsbPacket<SoapReqBody> getgJ0011Req(GJ0011Bean gJ0011Bean)
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
				"soapenv:Envelope");
		request.setPackge_type("http://www.cqrcb.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope/");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		GJ0011Req gJ0011ReqBody = new GJ0011Req();
		// 设置报文中请求头对象
		gJ0011ReqBody.setRequestHeader(gJ0011Bean.getRequestHeader());

		RequestBody requestBody = gJ0011Bean.getRequestBody();
		// 设置body
		gJ0011ReqBody.setRequestBody(requestBody);

		soapReqBody.setGJ0011Req(gJ0011ReqBody);
		return request;
	}

	@Override
	public EsbPacket<SoapResBody> getGJ0011Res() throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
				"soapenv:Envelope");
		response.setPackge_type("http://www.hrbcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope/");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();

		GJ0011Res gJ0011ResBody = new GJ0011Res();
		// 初始化响应头 并设置
		gJ0011ResBody.setResponseHeader(new ResponseHeader());

		// 初始化响应体并设置
		ResponseBody responseBody = new ResponseBody();
		gJ0011ResBody.setResponseBody(responseBody);

		// 初始化错误类 并设置
		Fault fault = new Fault();
		Detail detail = new Detail();
		fault.setDetail(detail);
		gJ0011ResBody.setFault(fault);

		soapResBody.setGJ0011Res(gJ0011ResBody);
		return response;
	}

}
