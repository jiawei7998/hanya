package com.singlee.financial.esb.hbcb.bean.s100001001060450;

public class ResDetailList {

	/**
	 * 账号 账号
	 */
	private String AcctNo;
	/**
	 * 状态 OPEN-正常 CLOS-关户 PURG-未找到 STOP-止付 STPD-只允许借 STPW-只允许贷 DRMT-休眠 ONLI-冻结
	 * UNCL-无主 ORDW-透支 TBCL-待销户 CLTD-已销户待删除 EONL-仅供查询 DONL-存入限定 WONL-支取限定 DISH-拒付
	 * INAT-未激活 APPR-核准 FADV-全额发放 CNCL-取消 EXTN-展期 MATU-到期
	 */
	private String Stat5;

	/**
	 * 类型 D-存款账户 L-贷款账户 C-cta账户
	 */
	private String Type;

	/**
	 * 产品 产品描述
	 */
	private String Product;
	/**
	 * 货币 币种，例如人民币CNY
	 */
	private String Currency2;
	/**
	 * 关联 OWN:独立连接   O&J:联合账户的一级账户持有者  JOIN:联合账户的二级账户持有者
	 */
	private String Relate;

	/**
	 * 余额 账户余额 高位置空，两位小数；最后一位为符号位，格式为ZZZZZZZZZZZZZZ9.99-----S。如果为正，则为空。例如“0.00”
	 */
	private String Balance2;

	/**
	 * 限额 限额 1、高位置空，两位小数；最后一位为符号位，格式为ZZZZZZZZZZZZZZ9.99-----S 。如果为正，则为空。 2、如果是0，则为空。
	 */
	private String Limit;

	/**
	 * 开户行 开户行网点
	 */
	private String OpenBank;
	/**
	 * 产品大类 账户对应的产品大类
	 */
	private String ProductType;
	/**
	 * 产品子类 账户对应的产品子类
	 */
	private String ProductSubType;
	/**
	 * 定活标志 T:定期 S:活期
	 */
	private String AcctFlag;

	/**
	 * 余额累计 9(14)V999S截住返回最近一条记录的余额累计；同时作为第二次上送字段
	 */
	private String BalanceAcm;
	/**
	 * 限额累计 9(14)V999S截住返回最近一条记录的限额累计；同时作为第二次上送字段
	 */
	private String LimitAccumu;
	/**
	 * 记录号 用于第二次查询上送字段
	 */
	private String RecNumber9;
	/**
	 * 组号 用于第二次查询上送字段
	 */
	private String GroupNum;
	/**
	 * 客户号 用于第二次查询上送字段
	 */
	private String CustNum2;

	public String getAcctNo() {
		return AcctNo;
	}

	public void setAcctNo(String acctNo) {
		AcctNo = acctNo;
	}

	public String getStat5() {
		return Stat5;
	}

	public void setStat5(String stat5) {
		Stat5 = stat5;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getProduct() {
		return Product;
	}

	public void setProduct(String product) {
		Product = product;
	}

	public String getCurrency2() {
		return Currency2;
	}

	public void setCurrency2(String currency2) {
		Currency2 = currency2;
	}

	public String getRelate() {
		return Relate;
	}

	public void setRelate(String relate) {
		Relate = relate;
	}

	public String getBalance2() {
		return Balance2;
	}

	public void setBalance2(String balance2) {
		Balance2 = balance2;
	}

	public String getLimit() {
		return Limit;
	}

	public void setLimit(String limit) {
		Limit = limit;
	}

	public String getOpenBank() {
		return OpenBank;
	}

	public void setOpenBank(String openBank) {
		OpenBank = openBank;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	public String getProductSubType() {
		return ProductSubType;
	}

	public void setProductSubType(String productSubType) {
		ProductSubType = productSubType;
	}

	public String getAcctFlag() {
		return AcctFlag;
	}

	public void setAcctFlag(String acctFlag) {
		AcctFlag = acctFlag;
	}

	public String getBalanceAcm() {
		return BalanceAcm;
	}

	public void setBalanceAcm(String balanceAcm) {
		BalanceAcm = balanceAcm;
	}

	public String getLimitAccumu() {
		return LimitAccumu;
	}

	public void setLimitAccumu(String limitAccumu) {
		LimitAccumu = limitAccumu;
	}

	public String getRecNumber9() {
		return RecNumber9;
	}

	public void setRecNumber9(String recNumber9) {
		RecNumber9 = recNumber9;
	}

	public String getGroupNum() {
		return GroupNum;
	}

	public void setGroupNum(String groupNum) {
		GroupNum = groupNum;
	}

	public String getCustNum2() {
		return CustNum2;
	}

	public void setCustNum2(String custNum2) {
		CustNum2 = custNum2;
	}

}
