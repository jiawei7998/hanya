package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001001009040.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001001009040.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S9040Bean;

public interface S100001001009040Service {

	/**
	 * 
	 * @param s9040Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S9040Bean s9040Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001001009040Req(S9040Bean s9040Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001001009040Res() throws InstantiationException, IllegalAccessException;
}
