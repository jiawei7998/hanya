package com.singlee.financial.esb.hbcb.bean.s003003990MS6771;

import java.util.List;
import java.util.Map;

import com.singlee.financial.esb.hbcb.bean.common.ResMsgHeadOut;
import com.singlee.financial.esb.hbcb.bean.common.ResReturnCode;

/**
 * 请求返回体 主要应用与<ns:S003003990MS6771>标签
 * 
 *
 */
public class ResponseBody {

	/**
	 * 业务状态 查询申请的处理状态 PR00:已转发 PR05:已成功 PR07:已处理 PR09:已拒绝 (PR09时近返回 从公文登记薄中获取)
	 */
	private String STSID;
	/**
	 * 业务拒绝信息 (PR09时近返回 从公文登记薄中获取)
	 */
	private String NPCRJTINFO;
	/**
	 * 报文标识号 报文标识号(原支付贷记业务)
	 */
	private String OGLBIZMSGID;
	/**
	 * 发起参与机构 发起参与机构(原支付贷记业务)
	 */
	private String OGLBIZSNDPARTY;
	/**
	 * 报文类型 报文类型(原支付贷记业务)
	 */
	private String OGLBIZMSGTYPE;
	/**
	 * 发起间接参与机构 发起间接参与机构(原支付贷记业务)
	 */
	private String OGLBIZSNDIDIRECTPARTY;
	/**
	 * 接收间接参与机构 接收间接参与机构(原支付贷记业务)
	 */
	private String OGLBIZRCVIDIRECTPARTY;
	/**
	 * 明细标识号 明细标识号(原支付贷记业务)
	 */
	private String OGLBIZDETAILID;
	/**
	 * 业务类型编码 业务类型编码(原支付贷记业务)
	 */
	private String OGLBIZTYPE;
	/**
	 * 入账状态 入账状态(对手行) TA00:已入账 TA01:待人工确认 TA02:验签失败 TA03:间连无法实时回复 TA04:需退汇 TA05:已退汇
	 * TA06:无此业务 TA07:待定状态
	 */
	private String TTFNLST;
	/**
	 * 退汇原因 退汇原因(对手行) Rc00:收款人账号户名不符 RE01:账户已销户或账户不存在 Rc02:未面核账户不能入账
	 * Rc03:转账金额超过当日/年累计限额 Rc04:账号为收支管控账户 Rc05:账户类型非法 Rc06:账户己东结 Rc0T:其他
	 */
	private String PXCINF;
	/**
	 * 处理信息 处理信息(对手行)
	 */
	private String PRCINF;
	/**
	 * 扩展域
	 */
	private Map<Object, Object> EXTENDDATA;

	private List<ResReturnCode> RETURNCODE;

	private List<ResMsgHeadOut> MSGHEADOUT;

	public String getSTSID() {
		return STSID;
	}

	public void setSTSID(String sTSID) {
		STSID = sTSID;
	}

	public String getNPCRJTINFO() {
		return NPCRJTINFO;
	}

	public void setNPCRJTINFO(String nPCRJTINFO) {
		NPCRJTINFO = nPCRJTINFO;
	}

	public String getOGLBIZMSGID() {
		return OGLBIZMSGID;
	}

	public void setOGLBIZMSGID(String oGLBIZMSGID) {
		OGLBIZMSGID = oGLBIZMSGID;
	}

	public String getOGLBIZSNDPARTY() {
		return OGLBIZSNDPARTY;
	}

	public void setOGLBIZSNDPARTY(String oGLBIZSNDPARTY) {
		OGLBIZSNDPARTY = oGLBIZSNDPARTY;
	}

	public String getOGLBIZMSGTYPE() {
		return OGLBIZMSGTYPE;
	}

	public void setOGLBIZMSGTYPE(String oGLBIZMSGTYPE) {
		OGLBIZMSGTYPE = oGLBIZMSGTYPE;
	}

	public String getOGLBIZSNDIDIRECTPARTY() {
		return OGLBIZSNDIDIRECTPARTY;
	}

	public void setOGLBIZSNDIDIRECTPARTY(String oGLBIZSNDIDIRECTPARTY) {
		OGLBIZSNDIDIRECTPARTY = oGLBIZSNDIDIRECTPARTY;
	}

	public String getOGLBIZRCVIDIRECTPARTY() {
		return OGLBIZRCVIDIRECTPARTY;
	}

	public void setOGLBIZRCVIDIRECTPARTY(String oGLBIZRCVIDIRECTPARTY) {
		OGLBIZRCVIDIRECTPARTY = oGLBIZRCVIDIRECTPARTY;
	}

	public String getOGLBIZDETAILID() {
		return OGLBIZDETAILID;
	}

	public void setOGLBIZDETAILID(String oGLBIZDETAILID) {
		OGLBIZDETAILID = oGLBIZDETAILID;
	}

	public String getOGLBIZTYPE() {
		return OGLBIZTYPE;
	}

	public void setOGLBIZTYPE(String oGLBIZTYPE) {
		OGLBIZTYPE = oGLBIZTYPE;
	}

	public String getTTFNLST() {
		return TTFNLST;
	}

	public void setTTFNLST(String tTFNLST) {
		TTFNLST = tTFNLST;
	}

	public String getPXCINF() {
		return PXCINF;
	}

	public void setPXCINF(String pXCINF) {
		PXCINF = pXCINF;
	}

	public String getPRCINF() {
		return PRCINF;
	}

	public void setPRCINF(String pRCINF) {
		PRCINF = pRCINF;
	}

	public Map<Object, Object> getEXTENDDATA() {
		return EXTENDDATA;
	}

	public void setEXTENDDATA(Map<Object, Object> eXTENDDATA) {
		EXTENDDATA = eXTENDDATA;
	}

	public List<ResReturnCode> getRETURNCODE() {
		return RETURNCODE;
	}

	public void setRETURNCODE(List<ResReturnCode> rETURNCODE) {
		RETURNCODE = rETURNCODE;
	}

	public List<ResMsgHeadOut> getMSGHEADOUT() {
		return MSGHEADOUT;
	}

	public void setMSGHEADOUT(List<ResMsgHeadOut> mSGHEADOUT) {
		MSGHEADOUT = mSGHEADOUT;
	}

}
