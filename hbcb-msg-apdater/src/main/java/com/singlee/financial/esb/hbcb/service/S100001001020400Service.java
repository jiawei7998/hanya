package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001001020400.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001001020400.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S20400Bean;

public interface S100001001020400Service {

	/**
	 * 
	 * @param s20400Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S20400Bean s20400Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001001020400Req(S20400Bean s20400Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001001020400Res() throws InstantiationException, IllegalAccessException;
}
