package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001001009022.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001001009022.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S9022Bean;

public interface S100001001009022Service {

	/**
	 * 
	 * @param s9022Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S9022Bean s9022Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001001009022Req(S9022Bean s9022Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001001009022Res() throws InstantiationException, IllegalAccessException;
}
