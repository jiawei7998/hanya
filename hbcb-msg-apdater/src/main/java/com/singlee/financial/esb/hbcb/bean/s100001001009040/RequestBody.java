package com.singlee.financial.esb.hbcb.bean.s100001001009040;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S100001001009040>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * UUID 外围渠道交易的uuid
	 */
	private String UUID;

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

}
