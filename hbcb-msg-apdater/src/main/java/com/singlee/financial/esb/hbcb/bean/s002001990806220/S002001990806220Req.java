package com.singlee.financial.esb.hbcb.bean.s002001990806220;

import java.io.Serializable;

import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;

/**
 * 传账接口
 * 
 * 主要处理<cqr:S002001990806220>标签
 * 
 *
 */
public class S002001990806220Req implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 请求头
	 */
	private RequestHeader RequestHeader;
	/**
	 * 请求体
	 */
	private RequestBody RequestBody;

	public RequestHeader getRequestHeader() {
		return RequestHeader;
	}

	public void setRequestHeader(RequestHeader requestHeader) {
		RequestHeader = requestHeader;
	}

	public RequestBody getRequestBody() {
		return RequestBody;
	}

	public void setRequestBody(RequestBody requestBody) {
		RequestBody = requestBody;
	}

}
