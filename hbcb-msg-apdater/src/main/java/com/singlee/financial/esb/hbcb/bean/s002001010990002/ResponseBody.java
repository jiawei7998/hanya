package com.singlee.financial.esb.hbcb.bean.s002001010990002;

/**
 * 请求返回体 主要应用与<ns:S002001010990002>标签
 * 
 *
 */
public class ResponseBody {

	/**
	 * 发生额TxnAmt
	 */
	private String TxnAmt;

	/**
	 * 余额Bal
	 */
	private String Bal;

	public String getTxnAmt() {
		return TxnAmt;
	}

	public void setTxnAmt(String txnAmt) {
		TxnAmt = txnAmt;
	}

	public String getBal() {
		return Bal;
	}

	public void setBal(String bal) {
		Bal = bal;
	}

}
