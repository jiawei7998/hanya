package com.singlee.financial.esb.hbcb.bean.GJ0011;

import java.io.Serializable;

import com.singlee.financial.esb.hbcb.bean.common.Fault;
import com.singlee.financial.esb.hbcb.bean.common.ResponseHeader;

/**
中俄跨境支付平台系统通过ESB获取国结系统提供的牌价。
 * 
 *
 */
public class GJ0011Res implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 响应头
	 */
	private ResponseHeader ResponseHeader;
	/**
	 * 响应体
	 */
	private ResponseBody ResponseBody;

	/**
	 * 包含返回的错误相关信息
	 */
	private Fault Fault;

    public ResponseHeader getResponseHeader() {
        return ResponseHeader;
    }

    public void setResponseHeader(ResponseHeader responseHeader) {
        ResponseHeader = responseHeader;
    }

    public ResponseBody getResponseBody() {
        return ResponseBody;
    }

    public void setResponseBody(ResponseBody responseBody) {
        ResponseBody = responseBody;
    }

    public Fault getFault() {
        return Fault;
    }

    public void setFault(Fault fault) {
        Fault = fault;
    }


}
