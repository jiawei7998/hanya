package com.singlee.financial.esb.hbcb.bean.s035001010FMT201;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前返回报文S035001010FMT201
 * 
 *
 */
public class SoapResBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	private S035001010FMT201Res s035001010FMT201Res;

	public S035001010FMT201Res getS035001010FMT201Res() {
		return s035001010FMT201Res;
	}

	public void setS035001010FMT201Res(S035001010FMT201Res s035001010fmt201Res) {
		s035001010FMT201Res = s035001010fmt201Res;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soap:Body");
		info.setSubPacketClassType(SoapResBody.class);
		// 给当前类起别名
		info.addAlias(SoapResBody.class, "s035001010FMT201Res", "ns:S035001010FMT201");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:esb");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soap");
		// 处理子标签包含集合的内容
		// info.addAlias(ResponseBody.class,"detailList","Detail1",ResDetailList.class);
		return info;
	}

}
