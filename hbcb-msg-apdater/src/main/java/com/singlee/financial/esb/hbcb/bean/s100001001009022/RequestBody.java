package com.singlee.financial.esb.hbcb.bean.s100001001009022;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S100001001009022>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 必输 账号 输入对应的账号，获取核心日期可以给17个1
	 */
	private String AcctNo;

	public String getAcctNo() {
		return AcctNo;
	}

	public void setAcctNo(String acctNo) {
		AcctNo = acctNo;
	}

}
