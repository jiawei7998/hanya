package com.singlee.financial.esb.hbcb.bean.s100001001084015;

import java.io.Serializable;
import java.util.List;

/**
 * 主要应用与<cqr:S100001001084015>标签
 * 
 * @author shenzl
 *
 */
public class RequestBody implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	/**
	 * 十二借十二贷交易信息组*12
	 */
	private List<ReqDetail1> Detail1;

	/**
	 * 原始交易流水号 当“转出帐号系统”为REM时，该字段为原冻结流水号
	 */
	private String OrigTrcNo;
	/**
	 * O 冻结日期 DDMMYYYY“转出帐号系统”为REM时，该字段为原冻结日期；当“转出帐号系统”为SET时，该字段为; 冻结到期日。
	 */
	private String FreezeDate;
	/**
	 * 冲正标识 M
	 */
	private String SMSFlag;
	/**
	 * O 自动圈提标识 Y:是 （到期后自动圈提到转入账户) N:否
	 */
	private String AutoQTFlag;
	/**
	 * 货币兑换区域×4 货币兑换时必须输入
	 */
	private List<ReqDetail2> Detail2;
	/**
	 * O 渠道编号
	 */
	private String ChannelNo;
	/**
	 * O 附言代码
	 */
	private String PostscriptCode;
	/**
	 * O 资金用途
	 */
	private String ProductAmtUse;

	public List<ReqDetail1> getDetail1() {
		return Detail1;
	}

	public void setDetail1(List<ReqDetail1> detail1) {
		Detail1 = detail1;
	}

	public String getOrigTrcNo() {
		return OrigTrcNo;
	}

	public void setOrigTrcNo(String origTrcNo) {
		OrigTrcNo = origTrcNo;
	}

	public String getFreezeDate() {
		return FreezeDate;
	}

	public void setFreezeDate(String freezeDate) {
		FreezeDate = freezeDate;
	}

	public String getSMSFlag() {
		return SMSFlag;
	}

	public void setSMSFlag(String sMSFlag) {
		SMSFlag = sMSFlag;
	}

	public String getAutoQTFlag() {
		return AutoQTFlag;
	}

	public void setAutoQTFlag(String autoQTFlag) {
		AutoQTFlag = autoQTFlag;
	}

	public List<ReqDetail2> getDetail2() {
		return Detail2;
	}

	public void setDetail2(List<ReqDetail2> detail2) {
		Detail2 = detail2;
	}

	public String getChannelNo() {
		return ChannelNo;
	}

	public void setChannelNo(String channelNo) {
		ChannelNo = channelNo;
	}

	public String getPostscriptCode() {
		return PostscriptCode;
	}

	public void setPostscriptCode(String postscriptCode) {
		PostscriptCode = postscriptCode;
	}

	public String getProductAmtUse() {
		return ProductAmtUse;
	}

	public void setProductAmtUse(String productAmtUse) {
		ProductAmtUse = productAmtUse;
	}

}
