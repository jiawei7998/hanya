package com.singlee.financial.esb.hbcb.bean.s100001001084015;

import java.io.Serializable;

/**
 * 十二借十二贷交易信息组*12
 * 
 * @author shenzl
 *
 */
public class ReqDetail1 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 不使用旧账号必须使用，使用旧账号该字段为空 转出帐号 如果不足17位，前面补0
	 */
	private String TfroutAcctNo;
	/**
	 * 如使用旧账号该字段必须输入，否则为空 省代码/转出方 原是定义各省的代码而用，依据目前银行社现状，应该定义各地市县代码，由业务组确定。
	 */
	private String ProvinceCodeOut;
	/**
	 * 如使用旧账号该字段必须输入，否则为空 转出旧帐号 转出客户的旧帐号
	 */
	private String TfroutOldAcctNo;
	/**
	 * M 转出帐号系统 BGL：BGL内部账 DEP：存款账户 REM：解冻 SET：冻结 SRN ： 销账码 CGL ：CGL 科目账 HOD: 圈存
	 * FRE: 解圈
	 */
	private String TfroutAcctNoSys;
	/**
	 * 如果为活期一本通则必输，否则不输 子账户类型
	 */
	private String SubAccountTp;
	/**
	 * 不使用旧账号必须使用，使用旧账号该字段为空 转入帐号
	 */
	private String TfrinAcctNo;
	/**
	 * 如使用旧账号该字段必须输入，否则为空 省代码/转入方 原是定义各省的代码而用，依据目前银行社现状，应该定义各地市县代码，由业务组确定。
	 */
	private String ProvinceCodeIn;
	/**
	 * 如使用旧账号该字段必须输入，否则为空 转入旧帐号 转入客户的旧帐号
	 */
	private String TfrinOldAcctNo;
	/**
	 * 必输 转入帐号系统 DEP：存款账户 BGL：BGL内部账户 CGL：CGL科目账 SRN：销账码
	 */
	private String TfrinAcctNoSys;
	/**
	 * O 子账户类型(转入账号） 如果为活期一本通则必输，否则不输
	 */
	private String SubInAccountTp;
	/**
	 * O 产品类型 产品类型取值见附表4
	 */
	private String ProductType;
	/**
	 * O 产品子类型 产品子类取值见附表4
	 */
	private String ProductSubType;
	/**
	 * M 金额 9(14)V999S
	 */
	private String Amt;
	/**
	 * O 提示码
	 */
	private String PromptNum;
	/**
	 * O 重空类型
	 */
	private String ImptblnkvchrTp;
	/**
	 * O 重空号码
	 */
	private String ImptblnkvchrNo;
	/**
	 * O 重空密码
	 */
	private String ImptblnkvchrPswd;
	/**
	 * O 支付方式 支取方式见附表7
	 */
	private String PayMth;
	/**
	 * O 备注 为了支持支票销票，此字段的后8位用作“支票签发日期”字段，格式“DDYYMMMM
	 */
	private String Remark;
	/**
	 * O 转出机构
	 */
	private String TfroutBrch;
	/**
	 * O 转出提示码
	 */
	private String TfroutProCode;
	/**
	 * O 转入机构
	 */
	private String TfrInBrch;
	/**
	 * O 转入提示码
	 */
	private String TfrInProCode;
	/**
	 * 币种
	 */
	private String Currency;
	/**
	 * 货币转换标识 Y or N ( must be N for CGL txn) 货币转换时必须输入
	 */
	private String ExchangeFlag;
	/**
	 * O 是否圈提标识 A:圈提（支持DEP-BGL的圈提）
	 */
	private String QTflag;

	public String getTfroutAcctNo() {
		return TfroutAcctNo;
	}

	public void setTfroutAcctNo(String tfroutAcctNo) {
		TfroutAcctNo = tfroutAcctNo;
	}

	public String getProvinceCodeOut() {
		return ProvinceCodeOut;
	}

	public void setProvinceCodeOut(String provinceCodeOut) {
		ProvinceCodeOut = provinceCodeOut;
	}

	public String getTfroutOldAcctNo() {
		return TfroutOldAcctNo;
	}

	public void setTfroutOldAcctNo(String tfroutOldAcctNo) {
		TfroutOldAcctNo = tfroutOldAcctNo;
	}

	public String getTfroutAcctNoSys() {
		return TfroutAcctNoSys;
	}

	public void setTfroutAcctNoSys(String tfroutAcctNoSys) {
		TfroutAcctNoSys = tfroutAcctNoSys;
	}

	public String getSubAccountTp() {
		return SubAccountTp;
	}

	public void setSubAccountTp(String subAccountTp) {
		SubAccountTp = subAccountTp;
	}

	public String getTfrinAcctNo() {
		return TfrinAcctNo;
	}

	public void setTfrinAcctNo(String tfrinAcctNo) {
		TfrinAcctNo = tfrinAcctNo;
	}

	public String getProvinceCodeIn() {
		return ProvinceCodeIn;
	}

	public void setProvinceCodeIn(String provinceCodeIn) {
		ProvinceCodeIn = provinceCodeIn;
	}

	public String getTfrinOldAcctNo() {
		return TfrinOldAcctNo;
	}

	public void setTfrinOldAcctNo(String tfrinOldAcctNo) {
		TfrinOldAcctNo = tfrinOldAcctNo;
	}

	public String getTfrinAcctNoSys() {
		return TfrinAcctNoSys;
	}

	public void setTfrinAcctNoSys(String tfrinAcctNoSys) {
		TfrinAcctNoSys = tfrinAcctNoSys;
	}

	public String getSubInAccountTp() {
		return SubInAccountTp;
	}

	public void setSubInAccountTp(String subInAccountTp) {
		SubInAccountTp = subInAccountTp;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	public String getProductSubType() {
		return ProductSubType;
	}

	public void setProductSubType(String productSubType) {
		ProductSubType = productSubType;
	}

	public String getAmt() {
		return Amt;
	}

	public void setAmt(String amt) {
		Amt = amt;
	}

	public String getPromptNum() {
		return PromptNum;
	}

	public void setPromptNum(String promptNum) {
		PromptNum = promptNum;
	}

	public String getImptblnkvchrTp() {
		return ImptblnkvchrTp;
	}

	public void setImptblnkvchrTp(String imptblnkvchrTp) {
		ImptblnkvchrTp = imptblnkvchrTp;
	}

	public String getImptblnkvchrNo() {
		return ImptblnkvchrNo;
	}

	public void setImptblnkvchrNo(String imptblnkvchrNo) {
		ImptblnkvchrNo = imptblnkvchrNo;
	}

	public String getImptblnkvchrPswd() {
		return ImptblnkvchrPswd;
	}

	public void setImptblnkvchrPswd(String imptblnkvchrPswd) {
		ImptblnkvchrPswd = imptblnkvchrPswd;
	}

	public String getPayMth() {
		return PayMth;
	}

	public void setPayMth(String payMth) {
		PayMth = payMth;
	}

	public String getRemark() {
		return Remark;
	}

	public void setRemark(String remark) {
		Remark = remark;
	}

	public String getTfroutBrch() {
		return TfroutBrch;
	}

	public void setTfroutBrch(String tfroutBrch) {
		TfroutBrch = tfroutBrch;
	}

	public String getTfroutProCode() {
		return TfroutProCode;
	}

	public void setTfroutProCode(String tfroutProCode) {
		TfroutProCode = tfroutProCode;
	}

	public String getTfrInBrch() {
		return TfrInBrch;
	}

	public void setTfrInBrch(String tfrInBrch) {
		TfrInBrch = tfrInBrch;
	}

	public String getTfrInProCode() {
		return TfrInProCode;
	}

	public void setTfrInProCode(String tfrInProCode) {
		TfrInProCode = tfrInProCode;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getExchangeFlag() {
		return ExchangeFlag;
	}

	public void setExchangeFlag(String exchangeFlag) {
		ExchangeFlag = exchangeFlag;
	}

	public String getQTflag() {
		return QTflag;
	}

	public void setQTflag(String qTflag) {
		QTflag = qTflag;
	}

}
