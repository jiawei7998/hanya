package com.singlee.financial.esb.hbcb.bean.common;

public class Fault {
	/**
	 * 错误码
	 */
	private String FaultCode;

	private String FaultString;
	/**
	 * 返回状态
	 */
	private Detail Detail;

	public String getFaultCode() {
		return FaultCode;
	}

	public void setFaultCode(String faultCode) {
		FaultCode = faultCode;
	}

	public String getFaultString() {
		return FaultString;
	}

	public void setFaultString(String faultString) {
		FaultString = faultString;
	}

	public Detail getDetail() {
		return Detail;
	}

	public void setDetail(Detail detail) {
		Detail = detail;
	}

}
