package com.singlee.financial.esb.hbcb.bean.common;

import java.io.Serializable;

/**
 * 
 *二代接口Body里面必须的请求头
 */
public class ReqMsgHead implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易码
	 */
	private String bkbizkey;
	/**
	 * 处理代码
	 */
	private String bkdesposekey;
	
	/**
	 *发起渠道
	 */
	private String chanid;
	/**
	 * 原渠道日期
	 */
	private String chandt;
	/**
	 * 渠道流水号
	 */
	private String chanflow;
	/**
	 * 交易日期
	 */
	private String bizdate;
	/**
	 * 受理柜员
	 */
	private String tlno;
	/**
	 * 录入柜员
	 */
	private String crttlno;
	/**
	 * 复核柜员
	 */
	private String chktlno;
	/**
	 * 授权柜员
	 */
	private String authtlno;
	/**
	 * 交易发起机构号
	 */
	private String sbno;
	/**
	 * 代理渠道标识
	 */
	private String prxchanid;
	/**
	 * 代理交易序号
	 */
	private String faflow;
	/**
	 * 代理报文参考号
	 */
	private String facospdref;

	public String getBkbizkey() {
		return bkbizkey;
	}

	public void setBkbizkey(String bkbizkey) {
		this.bkbizkey = bkbizkey;
	}

	public String getBkdesposekey() {
		return bkdesposekey;
	}

	public void setBkdesposekey(String bkdesposekey) {
		this.bkdesposekey = bkdesposekey;
	}

	public String getChanid() {
		return chanid;
	}

	public void setChanid(String chanid) {
		this.chanid = chanid;
	}

	public String getChandt() {
		return chandt;
	}

	public void setChandt(String chandt) {
		this.chandt = chandt;
	}

	public String getChanflow() {
		return chanflow;
	}

	public void setChanflow(String chanflow) {
		this.chanflow = chanflow;
	}

	public String getBizdate() {
		return bizdate;
	}

	public void setBizdate(String bizdate) {
		this.bizdate = bizdate;
	}

	public String getTlno() {
		return tlno;
	}

	public void setTlno(String tlno) {
		this.tlno = tlno;
	}

	public String getCrttlno() {
		return crttlno;
	}

	public void setCrttlno(String crttlno) {
		this.crttlno = crttlno;
	}

	public String getChktlno() {
		return chktlno;
	}

	public void setChktlno(String chktlno) {
		this.chktlno = chktlno;
	}

	public String getAuthtlno() {
		return authtlno;
	}

	public void setAuthtlno(String authtlno) {
		this.authtlno = authtlno;
	}

	public String getSbno() {
		return sbno;
	}

	public void setSbno(String sbno) {
		this.sbno = sbno;
	}

	public String getPrxchanid() {
		return prxchanid;
	}

	public void setPrxchanid(String prxchanid) {
		this.prxchanid = prxchanid;
	}

	public String getFaflow() {
		return faflow;
	}

	public void setFaflow(String faflow) {
		this.faflow = faflow;
	}

	public String getFacospdref() {
		return facospdref;
	}

	public void setFacospdref(String facospdref) {
		this.facospdref = facospdref;
	}

}
