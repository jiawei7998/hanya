package com.singlee.financial.esb.hbcb.bean.s035001010FMT201;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前请求报文S035001010FMT201
 * 
 *
 */
public class SoapReqBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易码
	 */
	private S035001010FMT201Req s035001010FMT201Req;

	public S035001010FMT201Req getS035001010FMT201Req() {
		return s035001010FMT201Req;
	}

	public void setS035001010FMT201Req(S035001010FMT201Req s035001010fmt201Req) {
		s035001010FMT201Req = s035001010fmt201Req;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soap:Body");
		info.setSubPacketClassType(SoapReqBody.class);
		// 给当前类起别名
		info.addAlias(SoapReqBody.class, "s035001010FMT201Req", "esb:S035001010FMT201");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:esb");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soap");
		// 处理子标签包含集合的内容
		// info.addAlias(RequestBody.class, "Detail1", "Detail1", ReqDetail1.class);
		return info;
	}

}
