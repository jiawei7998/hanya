package com.singlee.financial.esb.hbcb.bean.common;

import java.util.List;

/**
 * 二代接口Body里面必须的返回头
 *
 */
public class ResReturnCode {

	/**
	 * 系统编号
	 */
	private String domain;
	/**
	 * 返回码类型
	 */
	private String type;
	/**
	 * 业务编码
	 */
	private String code;
	/**
	 * 业务信息
	 */
	private String message;
	/**
	 * 参数列表
	 */
	private List<String> parameterList;
	/**
	 * 外部系统编号
	 */
	private String externalsystem;
	/**
	 * 外部系统返回码
	 */
	private String externalcode;
	/**
	 * 外部系统返回信息
	 */
	private String externalmessage;

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getParameterList() {
		return parameterList;
	}

	public void setParameterList(List<String> parameterList) {
		this.parameterList = parameterList;
	}

	public String getExternalsystem() {
		return externalsystem;
	}

	public void setExternalsystem(String externalsystem) {
		this.externalsystem = externalsystem;
	}

	public String getExternalcode() {
		return externalcode;
	}

	public void setExternalcode(String externalcode) {
		this.externalcode = externalcode;
	}

	public String getExternalmessage() {
		return externalmessage;
	}

	public void setExternalmessage(String externalmessage) {
		this.externalmessage = externalmessage;
	}

}
