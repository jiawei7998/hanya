package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001003000103.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001003000103.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S3000103Bean;

public interface S100001003000103Service {

	/**
	 * 
	 * @param s3000103Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S3000103Bean s3000103Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001003000103Req(S3000103Bean s3000103Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001003000103Res() throws InstantiationException, IllegalAccessException;
}
