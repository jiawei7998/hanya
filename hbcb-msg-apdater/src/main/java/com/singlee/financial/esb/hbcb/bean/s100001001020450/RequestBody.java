package com.singlee.financial.esb.hbcb.bean.s100001001020450;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S100001001020450>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * M BGL帐号
	 */
	private String BGLAcctNo;
	/**
	 * O 起始交易号 隐藏字段，默认值0001
	 */
	private String TxBeginNo;
	/**
	 * O 交易类型 01：金融交易 05：BGL利息计提及结转 20：对账单摘要 21：交易备注 22：其他情形 36：转账信息 69：对手信息 70：代理信息
	 */
	private String TxType;

	/**
	 * O 起始交易日
	 */
	private String TxBeginDate;
	/**
	 * O 柜员号
	 */
	private String TellerNo;
	/**
	 * O 交易码
	 */
	private String TxCode;
	/**
	 * O ATM柜员号
	 */
	private String ATMTeller;
	/**
	 * O 截至交易日期
	 */
	private String Endate;
	/**
	 * O 记录号 翻页时需要上送上一次查询的最后一个包的原值
	 */
	private String RecNumber;
	/**
	 * O 下一页选项 1：是；0：否; 9：略
	 */
	private String Option;
	/**
	 * O 交易日期
	 */
	private String TxDate;
	/**
	 * O 交易金额
	 */
	private String TxAmt;

	public String getBGLAcctNo() {
		return BGLAcctNo;
	}

	public void setBGLAcctNo(String bGLAcctNo) {
		BGLAcctNo = bGLAcctNo;
	}

	public String getTxBeginNo() {
		return TxBeginNo;
	}

	public void setTxBeginNo(String txBeginNo) {
		TxBeginNo = txBeginNo;
	}

	public String getTxType() {
		return TxType;
	}

	public void setTxType(String txType) {
		TxType = txType;
	}

	public String getTxBeginDate() {
		return TxBeginDate;
	}

	public void setTxBeginDate(String txBeginDate) {
		TxBeginDate = txBeginDate;
	}

	public String getTellerNo() {
		return TellerNo;
	}

	public void setTellerNo(String tellerNo) {
		TellerNo = tellerNo;
	}

	public String getTxCode() {
		return TxCode;
	}

	public void setTxCode(String txCode) {
		TxCode = txCode;
	}

	public String getATMTeller() {
		return ATMTeller;
	}

	public void setATMTeller(String aTMTeller) {
		ATMTeller = aTMTeller;
	}

	public String getEndate() {
		return Endate;
	}

	public void setEndate(String endate) {
		Endate = endate;
	}

	public String getRecNumber() {
		return RecNumber;
	}

	public void setRecNumber(String recNumber) {
		RecNumber = recNumber;
	}

	public String getOption() {
		return Option;
	}

	public void setOption(String option) {
		Option = option;
	}

	public String getTxDate() {
		return TxDate;
	}

	public void setTxDate(String txDate) {
		TxDate = txDate;
	}

	public String getTxAmt() {
		return TxAmt;
	}

	public void setTxAmt(String txAmt) {
		TxAmt = txAmt;
	}

}
