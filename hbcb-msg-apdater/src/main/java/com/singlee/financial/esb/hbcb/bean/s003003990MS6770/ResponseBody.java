package com.singlee.financial.esb.hbcb.bean.s003003990MS6770;



import java.util.List;
import java.util.Map;

import com.singlee.financial.esb.hbcb.bean.common.ResMsgHeadOut;
import com.singlee.financial.esb.hbcb.bean.common.ResReturnCode;
import com.singlee.financial.esb.hbcb.bean.common.ResponseHeader;

/**
 * 请求返回体 主要应用与<ns:S003003990MS6670>标签
 * 
 *
 */
public class ResponseBody {
    //报文头
    private List<ResMsgHeadOut> msghead_out;
    //标准返回信息
	private List<ResReturnCode> RETURNCODE;
	/**
     * 扩展域
     */
    private Map<Object, Object> EXTENDDATA;
    public List<ResMsgHeadOut> getMsghead_out() {
        return msghead_out;
    }
    public void setMsghead_out(List<ResMsgHeadOut> msghead_out) {
        this.msghead_out = msghead_out;
    }
    public List<ResReturnCode> getRETURNCODE() {
        return RETURNCODE;
    }
    public void setRETURNCODE(List<ResReturnCode> rETURNCODE) {
        RETURNCODE = rETURNCODE;
    }
    public Map<Object, Object> getEXTENDDATA() {
        return EXTENDDATA;
    }
    public void setEXTENDDATA(Map<Object, Object> eXTENDDATA) {
        EXTENDDATA = eXTENDDATA;
    }
   
}
