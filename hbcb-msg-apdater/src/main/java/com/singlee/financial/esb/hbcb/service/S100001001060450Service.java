package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001001060450.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001001060450.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S60450Bean;

public interface S100001001060450Service {

	/**
	 * 
	 * @param s60450Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S60450Bean s60450Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001001060450Req(S60450Bean s60450Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001001060450Res() throws InstantiationException, IllegalAccessException;
}
