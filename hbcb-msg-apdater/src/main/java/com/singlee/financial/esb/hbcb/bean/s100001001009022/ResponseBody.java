package com.singlee.financial.esb.hbcb.bean.s100001001009022;

/**
 * 请求返回体 主要应用与<ns:S100001001009022>标签
 * 
 *
 */
public class ResponseBody {

	/**
	 * 帐号 返回新账号 格式XXXXXXXXXXXXXXXX-X
	 * 
	 */
	private String AcctNo1;

	public String getAcctNo1() {
		return AcctNo1;
	}

	public void setAcctNo1(String acctNo1) {
		AcctNo1 = acctNo1;
	}

}
