package com.singlee.financial.esb.hbcb.service.Impl;

import org.springframework.stereotype.Service;
import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.esb.hbcb.bean.s002001990806220.*;
import com.singlee.financial.esb.hbcb.model.S806220Bean;
import com.singlee.financial.esb.hbcb.service.S002001990806220Service;
import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.xstream.utils.XmlUtils;

@Service
public class S002001990806220ServiceImpl implements S002001990806220Service {

	@Override
	public EsbOutBean send(S806220Bean s806220Bean) throws Exception {
		// 初始化请求报文
		EsbPacket<SoapReqBody> requestPakcet = gets002001990806220Req(s806220Bean);
		// 初始化HttpHeadAttrConfig
		HttpHeadAttrConfig httpHeadAttrConfig = EsbSend.getHttpHeadAttrConfig("S002001990806220", "GB2312");
		// 初始化输出对象
		EsbPacket<SoapResBody> responsePakcet = getS002001990806220Res();
		// 发送报文
		responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);
		// 解析返回对象
		SoapResBody soapResBody = responsePakcet.getBody();
		Fault fault = soapResBody.getS002001990806220Res().getFault();

		// 封装返回对象
		EsbOutBean result = new EsbOutBean();
		result.setSendmsg(XmlUtils.toXml(requestPakcet));
		result.setRecmsg(XmlUtils.toXml(responsePakcet));
		result.setRetCode(fault.getFaultCode());
		result.setRetMsg(fault.getFaultString());
		return result;
	}

	@Override
	public EsbPacket<SoapReqBody> gets002001990806220Req(S806220Bean s806220Bean)
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapHeader.class,
				"soapenv:Envelope");
		request.setPackge_type("http://www.cqrcb.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope/");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		S002001990806220Req s806220ReqBody = new S002001990806220Req();
		// 设置报文中请求头对象
		s806220ReqBody.setRequestHeader(s806220Bean.getRequestHeader());

		RequestBody requestBody = s806220Bean.getRequestBody();
		// 设置body
		s806220ReqBody.setRequestBody(requestBody);

		soapReqBody.setS002001990806220Req(s806220ReqBody);
		return request;
	}

	@Override
	public EsbPacket<SoapResBody> getS002001990806220Res() throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,
				"soapenv:Envelope");
		response.setPackge_type("http://www.hrbcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope/");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();

		S002001990806220Res s806220ResBody = new S002001990806220Res();
		// 初始化响应头 并设置
		s806220ResBody.setResponseHeader(new ResponseHeader());

		// 初始化响应体并设置
		ResponseBody responseBody = new ResponseBody();
		s806220ResBody.setResponseBody(responseBody);

		// 初始化错误类 并设置
		Fault fault = new Fault();
		Detail detail = new Detail();
		fault.setDetail(detail);
		s806220ResBody.setFault(fault);

		soapResBody.setS002001990806220Res(s806220ResBody);
		return response;
	}

}
