package com.singlee.financial.esb.hbcb.bean.s002001990806220;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前请求报文S002001990806220
 * 
 *
 */
public class SoapReqBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易码
	 */
	private S002001990806220Req s002001990806220Req;

	public S002001990806220Req getS002001990806220Req() {
		return s002001990806220Req;
	}

	public void setS002001990806220Req(S002001990806220Req s002001990806220Req) {
		this.s002001990806220Req = s002001990806220Req;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapReqBody.class);
		// 给当前类起别名
		info.addAlias(SoapReqBody.class, "s002001990806220Req", "cqr:S002001990806220");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:cqr");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		// info.addAlias(RequestBody.class, "Detail1", "Detail1", ReqDetail1.class);
		return info;
	}

}
