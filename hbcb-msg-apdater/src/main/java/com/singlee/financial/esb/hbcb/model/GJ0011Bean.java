package com.singlee.financial.esb.hbcb.model;

import java.io.Serializable;

import com.singlee.financial.esb.hbcb.bean.GJ0011.RequestBody;
import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;


public class GJ0011Bean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 请求头
     */
    private RequestHeader requestHeader;

    /**
     * 请求体
     */
    private RequestBody requestBody;

    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    public void setRequestHeader(RequestHeader requestHeader) {
        this.requestHeader = requestHeader;
    }

    public RequestBody getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(RequestBody requestBody) {
        this.requestBody = requestBody;
    }

}