package com.singlee.financial.esb.hbcb.util;


import com.singlee.capital.system.service.SystemProperties;

public class HttpConstants {
    

    public final static class IpConfig{
        /***IP**/
        public static final String HOST = SystemProperties.esbIp;
        /***端口****/
        public static final String PORT = SystemProperties.esbTransPort;
    }
    
}
