package com.singlee.financial.esb.hbcb.bean.s002001010990002;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S002001010990002>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	private String AcctNo;
	private String ChkMagFlg;
	private String ExtndStat;

	public String getAcctNo() {
		return AcctNo;
	}

	public void setAcctNo(String acctNo) {
		AcctNo = acctNo;
	}

	public String getChkMagFlg() {
		return ChkMagFlg;
	}

	public void setChkMagFlg(String chkMagFlg) {
		ChkMagFlg = chkMagFlg;
	}

	public String getExtndStat() {
		return ExtndStat;
	}

	public void setExtndStat(String extndStat) {
		ExtndStat = extndStat;
	}

}
