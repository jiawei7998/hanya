package com.singlee.financial.esb.hbcb.bean.s100001001020400;

/**
 * 请求返回体
 * 主要应用与<ns:S100001001020400>标签
 * 
 *
 */
public class ResponseBody {

	/**
	 * O 账号
	 */
	private String AcctNo;
	/**
	 * O 分类账名称
	 */
	private String AcctName;
	/**
	 * O 其他语言名称
	 */
	private String OtherLangName;
	/**
	 * O 银行号
	 */
	private String BnkNo;
	/**
	 * O 机构号
	 */
	private String BrchNo;
	/**
	 * O 货币
	 */
	private String Currency;
	/**
	 * O 余额 例：正数： 3032657.39 负数 6967341.61-
	 */
	private String Balance;
	/**
	 * O 产品类型
	 */
	private String ProductType;
	/**
	 * O 收/发标识
	 */
	private String RecSendFlag;
	/**
	 * O 账户状态
	 */
	private String AcctStat;
	/**
	 * O 月份1
	 */
	private String Month1;
	/**
	 * O 月份2
	 */
	private String Month2;
	/**
	 * O 月份3
	 */
	private String Month3;
	/**
	 * O 月份4
	 */
	private String Month4;
	/**
	 * O 月份5
	 */
	private String Month5;
	/**
	 * O 月份6
	 */
	private String Month6;
	/**
	 * O 月份7
	 */
	private String Month7;
	/**
	 * O 月份8
	 */
	private String Month8;
	/**
	 * O 月份9
	 */
	private String Month9;
	/**
	 * O 月份10
	 */
	private String Month10;
	/**
	 * O 月份11
	 */
	private String Month11;
	/**
	 * O 月份12
	 */
	private String Month12;
	/**
	 * O 分类账价值
	 */
	private String AcctValue;
	/**
	 * O 跨货币金额
	 */
	private String CurrAmount;
	/**
	 * O 联行值
	 */
	private String UniBankValue;
	/**
	 * O 非跨货币价值交易
	 */
	private String NonCurrValue;
	/**
	 * O 周期
	 */
	private String Period;
	/**
	 * O 对账单日期标识
	 */
	private String StmtDateIden;
	/**
	 * O 周期
	 */
	private String StringPeriod2;
	/**
	 * O 日
	 */
	private String Day;
	/**
	 * O 开户日
	 */
	private String OpenDate;
	/**
	 * O 总账分类码
	 */
	private String GeneralAcctClCode;
	/**
	 * O 部门
	 */
	private String Department;
	/**
	 * O 上一产品类型变更日
	 */
	private String LastTypeDate;
	/**
	 * O 特殊参考号标识
	 */
	private String SpecRefIden1;
	/**
	 * O 特殊参考号借记、贷记标识
	 */
	private String SpecRefIden2;
	/**
	 * O 预约转账标识
	 */
	private String PreFlag;
	/**
	 * O 延迟天数
	 */
	private String DelayDay;
	/**
	 * O 母账户
	 */
	private String RootAcct;
	/**
	 * O 定时转账指示
	 */
	private String TimTransferInsr;
	/**
	 * O 安全存取标识
	 */
	private String SecuRestoreFlag;
	/**
	 * O 特殊账户标识
	 */
	private String SpecAcctIden;
	/**
	 * O 受益代码
	 */
	private String BenyCode;
	/**
	 * O 关闭标识
	 */
	private String CloseFlag1;
	/**
	 * O closeflag
	 */
	private String CloseFlag2;
	/**
	 * O 库存限制金额
	 */
	private String LimitAmt;
	/**
	 * O 零余额标识 0：No-机构签退时不需检查账户余额是否为零 1：Yes-机构签退时需要需检查余额是否为零
	 */
	private String ZeroFlag;
	/**
	 * O 发生额标识 C-贷方 D-借方 B- 双向
	 */
	private String UseFlag;
	/**
	 * O 借方透支限额 取值范围0-9999999999999.99 0：不可透支 如果贷方透支限额大于0，此处不可大于0
	 */
	private String DebOverAmt;
	/**
	 * O 贷方透支限额 取值范围0-9999999999999.99 如果借方透支限额大于0，此处不可大于0
	 */
	private String CreOverAmt;
	/**
	 * O 活/定期标识 S-活期 T-定期
	 */
	private String CurFlag;
	/**
	 * O 定期存期 取值范围0-9999
	 */
	private String RegularDate;
	/**
	 * O 期限类型 D-日 M-月 Y-年
	 */
	private String TrmTyp;

	/**
	 * O 通存通兑 0：通存通兑 1:跨城授权通兑;通存 2:同城内通兑;通存 3:跨网点授权通兑;通存 4:不通兑;通存 5:通兑;跨城授权通存
	 * 6:通兑;同城内通存 7:跨城授权通兑;跨城授权通存 8:同城通兑;同城通存 9:跨网点授权通兑;跨城授权通存 A:不通兑;同城通存
	 * B:通兑;跨网点授权通存 C:通兑;不通存 D:跨城授权通兑;跨网点授权通存 E:同城通兑;不通存 F:跨网点授权通兑;跨网点授权通存
	 * G:不通兑;不通存
	 */
	private String USW;

	/**
	 * O 手工记账 0-允许 1-不允许
	 */
	private String HandAccount;
	/**
	 * O 余额方向 1-借方 2-贷方 0- 双向
	 */
	private String DcFlag;

	/**
	 * O 自动结息 0- 不自动 1- 自动
	 */
	private String AutoIntSetl;
	/**
	 * O 结息频率 取值范围0-999
	 */
	private String IntSetlFreq;
	/**
	 * O 结息单位 D-日 M-月 Y-年
	 */
	private String IntSetlUnit;

	/**
	 * O 转息账户
	 */
	private String TfrinIntAcctNo;
	/**
	 * O 下一结息日期 ddMMyyyy
	 */
	private String NextIneterDate;
	/**
	 * O 指定结息日 取值范围1-31
	 */
	private String SureIneterDate;
	/**
	 * O 指定结息频率 M-月 Q-季
	 */
	private String SureIneterDateFreq;
	/**
	 * O 开户机构
	 */
	private String OpenBank;
	/**
	 * O 开户柜员
	 */
	private String OpenTellerNo;
	/**
	 * O 销户机构
	 */
	private String CxlAcctBrch;
	/**
	 * O 销户柜员
	 */
	private String CxlAcctOper;
	/**
	 * O 销户日期
	 */
	private String CxlAcctDt;
	/**
	 * O 上次付息日期
	 */
	private String LastIntDate;

	public String getAcctNo() {
		return AcctNo;
	}

	public void setAcctNo(String acctNo) {
		AcctNo = acctNo;
	}

	public String getAcctName() {
		return AcctName;
	}

	public void setAcctName(String acctName) {
		AcctName = acctName;
	}

	public String getOtherLangName() {
		return OtherLangName;
	}

	public void setOtherLangName(String otherLangName) {
		OtherLangName = otherLangName;
	}

	public String getBnkNo() {
		return BnkNo;
	}

	public void setBnkNo(String bnkNo) {
		BnkNo = bnkNo;
	}

	public String getBrchNo() {
		return BrchNo;
	}

	public void setBrchNo(String brchNo) {
		BrchNo = brchNo;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getBalance() {
		return Balance;
	}

	public void setBalance(String balance) {
		Balance = balance;
	}

	public String getProductType() {
		return ProductType;
	}

	public void setProductType(String productType) {
		ProductType = productType;
	}

	public String getRecSendFlag() {
		return RecSendFlag;
	}

	public void setRecSendFlag(String recSendFlag) {
		RecSendFlag = recSendFlag;
	}

	public String getAcctStat() {
		return AcctStat;
	}

	public void setAcctStat(String acctStat) {
		AcctStat = acctStat;
	}

	public String getMonth1() {
		return Month1;
	}

	public void setMonth1(String month1) {
		Month1 = month1;
	}

	public String getMonth2() {
		return Month2;
	}

	public void setMonth2(String month2) {
		Month2 = month2;
	}

	public String getMonth3() {
		return Month3;
	}

	public void setMonth3(String month3) {
		Month3 = month3;
	}

	public String getMonth4() {
		return Month4;
	}

	public void setMonth4(String month4) {
		Month4 = month4;
	}

	public String getMonth5() {
		return Month5;
	}

	public void setMonth5(String month5) {
		Month5 = month5;
	}

	public String getMonth6() {
		return Month6;
	}

	public void setMonth6(String month6) {
		Month6 = month6;
	}

	public String getMonth7() {
		return Month7;
	}

	public void setMonth7(String month7) {
		Month7 = month7;
	}

	public String getMonth8() {
		return Month8;
	}

	public void setMonth8(String month8) {
		Month8 = month8;
	}

	public String getMonth9() {
		return Month9;
	}

	public void setMonth9(String month9) {
		Month9 = month9;
	}

	public String getMonth10() {
		return Month10;
	}

	public void setMonth10(String month10) {
		Month10 = month10;
	}

	public String getMonth11() {
		return Month11;
	}

	public void setMonth11(String month11) {
		Month11 = month11;
	}

	public String getMonth12() {
		return Month12;
	}

	public void setMonth12(String month12) {
		Month12 = month12;
	}

	public String getAcctValue() {
		return AcctValue;
	}

	public void setAcctValue(String acctValue) {
		AcctValue = acctValue;
	}

	public String getCurrAmount() {
		return CurrAmount;
	}

	public void setCurrAmount(String currAmount) {
		CurrAmount = currAmount;
	}

	public String getUniBankValue() {
		return UniBankValue;
	}

	public void setUniBankValue(String uniBankValue) {
		UniBankValue = uniBankValue;
	}

	public String getNonCurrValue() {
		return NonCurrValue;
	}

	public void setNonCurrValue(String nonCurrValue) {
		NonCurrValue = nonCurrValue;
	}

	public String getPeriod() {
		return Period;
	}

	public void setPeriod(String period) {
		Period = period;
	}

	public String getStmtDateIden() {
		return StmtDateIden;
	}

	public void setStmtDateIden(String stmtDateIden) {
		StmtDateIden = stmtDateIden;
	}

	public String getStringPeriod2() {
		return StringPeriod2;
	}

	public void setStringPeriod2(String stringPeriod2) {
		StringPeriod2 = stringPeriod2;
	}

	public String getDay() {
		return Day;
	}

	public void setDay(String day) {
		Day = day;
	}

	public String getOpenDate() {
		return OpenDate;
	}

	public void setOpenDate(String openDate) {
		OpenDate = openDate;
	}

	public String getGeneralAcctClCode() {
		return GeneralAcctClCode;
	}

	public void setGeneralAcctClCode(String generalAcctClCode) {
		GeneralAcctClCode = generalAcctClCode;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public String getLastTypeDate() {
		return LastTypeDate;
	}

	public void setLastTypeDate(String lastTypeDate) {
		LastTypeDate = lastTypeDate;
	}

	public String getSpecRefIden1() {
		return SpecRefIden1;
	}

	public void setSpecRefIden1(String specRefIden1) {
		SpecRefIden1 = specRefIden1;
	}

	public String getSpecRefIden2() {
		return SpecRefIden2;
	}

	public void setSpecRefIden2(String specRefIden2) {
		SpecRefIden2 = specRefIden2;
	}

	public String getPreFlag() {
		return PreFlag;
	}

	public void setPreFlag(String preFlag) {
		PreFlag = preFlag;
	}

	public String getDelayDay() {
		return DelayDay;
	}

	public void setDelayDay(String delayDay) {
		DelayDay = delayDay;
	}

	public String getRootAcct() {
		return RootAcct;
	}

	public void setRootAcct(String rootAcct) {
		RootAcct = rootAcct;
	}

	public String getTimTransferInsr() {
		return TimTransferInsr;
	}

	public void setTimTransferInsr(String timTransferInsr) {
		TimTransferInsr = timTransferInsr;
	}

	public String getSecuRestoreFlag() {
		return SecuRestoreFlag;
	}

	public void setSecuRestoreFlag(String secuRestoreFlag) {
		SecuRestoreFlag = secuRestoreFlag;
	}

	public String getSpecAcctIden() {
		return SpecAcctIden;
	}

	public void setSpecAcctIden(String specAcctIden) {
		SpecAcctIden = specAcctIden;
	}

	public String getBenyCode() {
		return BenyCode;
	}

	public void setBenyCode(String benyCode) {
		BenyCode = benyCode;
	}

	public String getCloseFlag1() {
		return CloseFlag1;
	}

	public void setCloseFlag1(String closeFlag1) {
		CloseFlag1 = closeFlag1;
	}

	public String getCloseFlag2() {
		return CloseFlag2;
	}

	public void setCloseFlag2(String closeFlag2) {
		CloseFlag2 = closeFlag2;
	}

	public String getLimitAmt() {
		return LimitAmt;
	}

	public void setLimitAmt(String limitAmt) {
		LimitAmt = limitAmt;
	}

	public String getZeroFlag() {
		return ZeroFlag;
	}

	public void setZeroFlag(String zeroFlag) {
		ZeroFlag = zeroFlag;
	}

	public String getUseFlag() {
		return UseFlag;
	}

	public void setUseFlag(String useFlag) {
		UseFlag = useFlag;
	}

	public String getDebOverAmt() {
		return DebOverAmt;
	}

	public void setDebOverAmt(String debOverAmt) {
		DebOverAmt = debOverAmt;
	}

	public String getCreOverAmt() {
		return CreOverAmt;
	}

	public void setCreOverAmt(String creOverAmt) {
		CreOverAmt = creOverAmt;
	}

	public String getCurFlag() {
		return CurFlag;
	}

	public void setCurFlag(String curFlag) {
		CurFlag = curFlag;
	}

	public String getRegularDate() {
		return RegularDate;
	}

	public void setRegularDate(String regularDate) {
		RegularDate = regularDate;
	}

	public String getTrmTyp() {
		return TrmTyp;
	}

	public void setTrmTyp(String trmTyp) {
		TrmTyp = trmTyp;
	}

	public String getUSW() {
		return USW;
	}

	public void setUSW(String uSW) {
		USW = uSW;
	}

	public String getHandAccount() {
		return HandAccount;
	}

	public void setHandAccount(String handAccount) {
		HandAccount = handAccount;
	}

	public String getDcFlag() {
		return DcFlag;
	}

	public void setDcFlag(String dcFlag) {
		DcFlag = dcFlag;
	}

	public String getAutoIntSetl() {
		return AutoIntSetl;
	}

	public void setAutoIntSetl(String autoIntSetl) {
		AutoIntSetl = autoIntSetl;
	}

	public String getIntSetlFreq() {
		return IntSetlFreq;
	}

	public void setIntSetlFreq(String intSetlFreq) {
		IntSetlFreq = intSetlFreq;
	}

	public String getIntSetlUnit() {
		return IntSetlUnit;
	}

	public void setIntSetlUnit(String intSetlUnit) {
		IntSetlUnit = intSetlUnit;
	}

	public String getTfrinIntAcctNo() {
		return TfrinIntAcctNo;
	}

	public void setTfrinIntAcctNo(String tfrinIntAcctNo) {
		TfrinIntAcctNo = tfrinIntAcctNo;
	}

	public String getNextIneterDate() {
		return NextIneterDate;
	}

	public void setNextIneterDate(String nextIneterDate) {
		NextIneterDate = nextIneterDate;
	}

	public String getSureIneterDate() {
		return SureIneterDate;
	}

	public void setSureIneterDate(String sureIneterDate) {
		SureIneterDate = sureIneterDate;
	}

	public String getSureIneterDateFreq() {
		return SureIneterDateFreq;
	}

	public void setSureIneterDateFreq(String sureIneterDateFreq) {
		SureIneterDateFreq = sureIneterDateFreq;
	}

	public String getOpenBank() {
		return OpenBank;
	}

	public void setOpenBank(String openBank) {
		OpenBank = openBank;
	}

	public String getOpenTellerNo() {
		return OpenTellerNo;
	}

	public void setOpenTellerNo(String openTellerNo) {
		OpenTellerNo = openTellerNo;
	}

	public String getCxlAcctBrch() {
		return CxlAcctBrch;
	}

	public void setCxlAcctBrch(String cxlAcctBrch) {
		CxlAcctBrch = cxlAcctBrch;
	}

	public String getCxlAcctOper() {
		return CxlAcctOper;
	}

	public void setCxlAcctOper(String cxlAcctOper) {
		CxlAcctOper = cxlAcctOper;
	}

	public String getCxlAcctDt() {
		return CxlAcctDt;
	}

	public void setCxlAcctDt(String cxlAcctDt) {
		CxlAcctDt = cxlAcctDt;
	}

	public String getLastIntDate() {
		return LastIntDate;
	}

	public void setLastIntDate(String lastIntDate) {
		LastIntDate = lastIntDate;
	}

}
