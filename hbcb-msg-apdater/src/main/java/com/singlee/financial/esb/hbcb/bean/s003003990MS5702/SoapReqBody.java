package com.singlee.financial.esb.hbcb.bean.s003003990MS5702;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;
import com.singlee.financial.esb.hbcb.bean.common.ReqMsgHead;

/**
 * 定义报文体
 * 
 * 主要用于标记当前请求报文S003003990MS5702
 * 
 *
 */
public class SoapReqBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易码
	 */
	private S003003990MS5702Req s003003990MS5702Req;

	public S003003990MS5702Req getS003003990MS5702Req() {
		return s003003990MS5702Req;
	}

	public void setS003003990MS5702Req(S003003990MS5702Req s003003990ms5702Req) {
		s003003990MS5702Req = s003003990ms5702Req;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soap:Body");
		info.setSubPacketClassType(SoapReqBody.class);
		// 给当前类起别名
		info.addAlias(SoapReqBody.class, "s003003990MS5702Req", "esb:S003003990MS5702");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:esb");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soap");
		// 处理子标签包含集合的内容
		info.addAlias(RequestBody.class, "msghead", "msghead", ReqMsgHead.class);
		return info;
	}

}
