package com.singlee.financial.esb.hbcb.bean.s100001001084015;

public class ResDetailList {

	/**
	 * 序号(如‘01’)
	 */
	private String SerialNo;
	/**
	 * 销账码
	 */
	private String CanlNo;
	/**
	 * 销账码标识
	 */
	private String CanlNoFlag;

	public String getSerialNo() {
		return SerialNo;
	}

	public void setSerialNo(String serialNo) {
		SerialNo = serialNo;
	}

	public String getCanlNo() {
		return CanlNo;
	}

	public void setCanlNo(String canlNo) {
		CanlNo = canlNo;
	}

	public String getCanlNoFlag() {
		return CanlNoFlag;
	}

	public void setCanlNoFlag(String canlNoFlag) {
		CanlNoFlag = canlNoFlag;
	}

}
