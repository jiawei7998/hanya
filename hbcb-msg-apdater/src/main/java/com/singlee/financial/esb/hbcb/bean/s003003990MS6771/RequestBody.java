package com.singlee.financial.esb.hbcb.bean.s003003990MS6771;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.singlee.financial.esb.hbcb.bean.common.ReqMsgHead;

/**
 * 主要应用与<cqr:S003003990MS6671>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 支付平台流水号 发起贷记业务入账状态查询业务时，支付平台返回的支付平台流水号
	 */
	private String MSGDETAILFLOW;

	/**
	 * 匹配方式
	 * 1)当匹配方式为1时,支付平台流水号必填,其余不填写; 2）当匹配方式为2时，原渠道号、原渠道日期、渠道流水号必填，其余不填写。
	 * 
	 * 1.按支付平台信息查询 2.按渠道三要素查询道三要素为渠道号、渠道日期、原渠道流水号
	 * 
	 */
	private String MATCHMODE;
	/**
	 * 原渠道号
	 */
	private String OCHANID;
	/**
	 * 原渠道日期
	 */
	private String OCHANDT;
	/**
	 * 原渠道流水号
	 */
	private String OCHANFLOW;
	/**
	 * 扩展域
	 */
	private Map<Object, Object> EXTENDDATA;

	private List<ReqMsgHead> MSGHEAD;

	public String getMSGDETAILFLOW() {
		return MSGDETAILFLOW;
	}

	public void setMSGDETAILFLOW(String mSGDETAILFLOW) {
		MSGDETAILFLOW = mSGDETAILFLOW;
	}

	public String getMATCHMODE() {
		return MATCHMODE;
	}

	public void setMATCHMODE(String mATCHMODE) {
		MATCHMODE = mATCHMODE;
	}

	public String getOCHANID() {
		return OCHANID;
	}

	public void setOCHANID(String oCHANID) {
		OCHANID = oCHANID;
	}

	public String getOCHANDT() {
		return OCHANDT;
	}

	public void setOCHANDT(String oCHANDT) {
		OCHANDT = oCHANDT;
	}

	public String getOCHANFLOW() {
		return OCHANFLOW;
	}

	public void setOCHANFLOW(String oCHANFLOW) {
		OCHANFLOW = oCHANFLOW;
	}

	public Map<Object, Object> getEXTENDDATA() {
		return EXTENDDATA;
	}

	public void setEXTENDDATA(Map<Object, Object> eXTENDDATA) {
		EXTENDDATA = eXTENDDATA;
	}

	public List<ReqMsgHead> getMSGHEAD() {
		return MSGHEAD;
	}

	public void setMSGHEAD(List<ReqMsgHead> mSGHEAD) {
		MSGHEAD = mSGHEAD;
	}

}
