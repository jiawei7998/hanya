package com.singlee.financial.esb.hbcb.bean.s003003990MS6770;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;
import com.singlee.financial.esb.hbcb.bean.common.ResMsgHeadOut;
import com.singlee.financial.esb.hbcb.bean.common.ResReturnCode;

/**
 * 定义报文体
 * 
 * 主要用于标记当前返回报文S003003990MS6770
 * 
 *
 */
public class SoapResBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	private S003003990MS6770Res s003003990MS6770Res;

	public S003003990MS6770Res getS003003990MS6770Res() {
		return s003003990MS6770Res;
	}

	public void setS003003990MS6770Res(S003003990MS6770Res s003003990ms6770Res) {
		s003003990MS6770Res = s003003990ms6770Res;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapResBody.class);
		// 给当前类起别名
		info.addAlias(SoapResBody.class, "s003003990MS6770Res", "ns:S003003990MS6770");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:ns");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		info.addAlias(ResponseBody.class, "RETURNCODE", "returncode", ResReturnCode.class);
        info.addAlias(ResponseBody.class, "msghead_out", "msghead_out", ResMsgHeadOut.class);
        return info;
	}

}
