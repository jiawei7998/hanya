package com.singlee.financial.esb.hbcb.bean.s003003990MS5702;

import java.util.List;

import com.singlee.financial.esb.hbcb.bean.common.ResMsgHeadOut;
import com.singlee.financial.esb.hbcb.bean.common.ResReturnCode;

/**
 * 请求返回体 主要应用与<ns:S003003990MS5702>标签
 * 
 *
 */
public class ResponseBody {

	private String extenddata_out;

	private List<ResReturnCode> resReturnCode;

	private List<ResMsgHeadOut> resMsgHeadOut;

	public String getExtenddata_out() {
		return extenddata_out;
	}

	public void setExtenddata_out(String extenddata_out) {
		this.extenddata_out = extenddata_out;
	}

	public List<ResReturnCode> getResReturnCode() {
		return resReturnCode;
	}

	public void setResReturnCode(List<ResReturnCode> resReturnCode) {
		this.resReturnCode = resReturnCode;
	}

	public List<ResMsgHeadOut> getResMsgHeadOut() {
		return resMsgHeadOut;
	}

	public void setResMsgHeadOut(List<ResMsgHeadOut> resMsgHeadOut) {
		this.resMsgHeadOut = resMsgHeadOut;
	}

}
