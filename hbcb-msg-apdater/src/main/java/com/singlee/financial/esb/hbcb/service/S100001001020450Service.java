package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001001020450.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001001020450.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S20450Bean;

public interface S100001001020450Service {

	/**
	 * 
	 * @param s20450Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S20450Bean s20450Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001001020450Req(S20450Bean s20450Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001001020450Res() throws InstantiationException, IllegalAccessException;
}
