package com.singlee.financial.esb.hbcb.bean.s100001001700105;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前请求报文S100001001700105
 * 
 *
 */
public class SoapReqBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易码
	 */
	private S100001001700105Req s100001001700105Req;

	public S100001001700105Req getS100001001700105Req() {
		return s100001001700105Req;
	}

	public void setS100001001700105Req(S100001001700105Req s100001001700105Req) {
		this.s100001001700105Req = s100001001700105Req;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapReqBody.class);
		// 给当前类起别名
		info.addAlias(SoapReqBody.class, "s100001001700105Req", "cqr:S100001001700105");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:cqr");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		// info.addAlias(RequestBody.class, "Detail1", "Detail1", ReqDetail1.class);
		return info;
	}

}
