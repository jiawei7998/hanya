package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s002001990806220.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s002001990806220.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S806220Bean;

public interface S002001990806220Service {

	/**
	 * 
	 * @param s806220Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S806220Bean s806220Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets002001990806220Req(S806220Bean s806220Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS002001990806220Res() throws InstantiationException, IllegalAccessException;
}
