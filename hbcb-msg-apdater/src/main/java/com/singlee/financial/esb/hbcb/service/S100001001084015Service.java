package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001001084015.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S84015Bean;

public interface S100001001084015Service {

	/**
	 * 
	 * @param s84015Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S84015Bean s84015Bean) throws Exception;
	
	/**
	 * 组装请求报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001001084015Req(S84015Bean s84015Bean) throws InstantiationException, IllegalAccessException;
	
	/**
	 * 初始化响应报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001001084015Res() throws InstantiationException, IllegalAccessException;
}
