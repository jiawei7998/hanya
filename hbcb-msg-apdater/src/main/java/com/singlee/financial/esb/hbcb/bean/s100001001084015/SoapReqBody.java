package com.singlee.financial.esb.hbcb.bean.s100001001084015;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前请求报文S100001001084015
 * 
 * @author shenzl
 *
 */
public class SoapReqBody extends EsbBodyPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 交易码
	 */
	private S100001001084015Req s100001001084015Req;

	public S100001001084015Req getS100001001084015() {
		return s100001001084015Req;
	}

	public void setS100001001084015(S100001001084015Req s100001001084015Req) {
		this.s100001001084015Req = s100001001084015Req;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapReqBody.class);
		// 给当前类起别名
		info.addAlias(SoapReqBody.class, "s100001001084015Req", "adt:S100001001084015");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:adt");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		info.addAlias(RequestBody.class, "Detail1", "Detail1", ReqDetail1.class);
		return info;
	}

}
