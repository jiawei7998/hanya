package com.singlee.financial.esb.hbcb.bean.s100001001060450;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S100001001060450>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 必输 客户号/帐号 根据“类型”来决定输入的是客户号还是账户
	 */
	private String CustNum3;
	/**
	 * 必输 类型 A：账户 C：客户号
	 */
	private String Type;

	public String getCustNum3() {
		return CustNum3;
	}

	public void setCustNum3(String custNum3) {
		CustNum3 = custNum3;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

}
