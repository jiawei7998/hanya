package com.singlee.financial.esb.hbcb.bean.s100001003000101;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S100001003000101>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 机构代码 M 参考核心机构码
	 */
	private String BRANCH_NO;
	/**
	 * 科目代码 M 不足8位前补0
	 */
	private String CGL_CODE;
	/**
	 * 科目序号 M 不足8位前补0
	 */
	private String CGL_SEQ;
	/**
	 * 币种 M 参考核心币种
	 */
	private String CURRENCY;
	/**
	 * 日期 M 查询历史信息
	 */
	private String DATE;
	/**
	 * 标识 M 区分查询单机构和汇总机构数据标识 Y- 汇总机构 N-单机构
	 */
	private String FLAG;

	public String getBRANCH_NO() {
		return BRANCH_NO;
	}

	public void setBRANCH_NO(String bRANCH_NO) {
		BRANCH_NO = bRANCH_NO;
	}

	public String getCGL_CODE() {
		return CGL_CODE;
	}

	public void setCGL_CODE(String cGL_CODE) {
		CGL_CODE = cGL_CODE;
	}

	public String getCGL_SEQ() {
		return CGL_SEQ;
	}

	public void setCGL_SEQ(String cGL_SEQ) {
		CGL_SEQ = cGL_SEQ;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public String getFLAG() {
		return FLAG;
	}

	public void setFLAG(String fLAG) {
		FLAG = fLAG;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
