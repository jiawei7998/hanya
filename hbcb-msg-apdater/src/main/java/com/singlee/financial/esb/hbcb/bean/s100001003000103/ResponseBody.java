package com.singlee.financial.esb.hbcb.bean.s100001003000103;

import java.util.List;

/**
 * 请求返回体 主要应用与<ns:S100001003000103>标签
 * 
 *
 */
public class ResponseBody {

	private List<ResDetailList> detailList;

	public List<ResDetailList> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ResDetailList> detailList) {
		this.detailList = detailList;
	}

}
