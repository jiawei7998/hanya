package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s100001001700105.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s100001001700105.SoapResBody;
import com.singlee.financial.esb.hbcb.model.S700105Bean;

public interface S100001001700105Service {

	/**
	 * 
	 * @param s700105Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(S700105Bean s700105Bean) throws Exception;

	/**
	 * 组装请求报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets100001001700105Req(S700105Bean s700105Bean)
			throws InstantiationException, IllegalAccessException;

	/**
	 * 初始化响应报文
	 * 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS100001001700105Res() throws InstantiationException, IllegalAccessException;
}
