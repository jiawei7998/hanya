package com.singlee.financial.esb.hbcb.bean.GJ0011;

import java.util.List;
import java.util.Map;

public interface ListPriceMapper  {
    
    void insertPrice(Map<String,Object> map);
    
    List<FileNameList> getPrice(String fileTime);
 
}
