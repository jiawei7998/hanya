package com.singlee.financial.esb.hbcb.bean.s100001003000101;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前返回报文S100001003000101
 * 
 *
 */
public class SoapResBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	private S100001003000101Res s100001003000101Res;

	public S100001003000101Res getS100001003000101Res() {
		return s100001003000101Res;
	}

	public void setS100001003000101Res(S100001003000101Res s100001003000101Res) {
		this.s100001003000101Res = s100001003000101Res;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapResBody.class);
		// 给当前类起别名
		info.addAlias(SoapResBody.class, "s100001003000101Res", "ns:S100001003000101");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:ns");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		// info.addAlias(ResponseBody.class, "detailList", "Detail1",ResDetailList.class);
		return info;
	}

}
