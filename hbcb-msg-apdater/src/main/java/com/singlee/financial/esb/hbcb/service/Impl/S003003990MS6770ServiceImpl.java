package com.singlee.financial.esb.hbcb.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.common.*;
import com.singlee.financial.esb.hbcb.bean.s003003990MS6770.*;
import com.singlee.financial.esb.hbcb.model.MS6770Bean;
import com.singlee.financial.esb.hbcb.service.S003003990MS6770Service;
import com.singlee.financial.esb.hbcb.util.EsbSend;
import com.singlee.xstream.utils.XmlUtils;

@Service
public class S003003990MS6770ServiceImpl implements S003003990MS6770Service {

	@Override
	public EsbOutBean send(MS6770Bean mS6770Bean) throws Exception {
		// 初始化请求报文
		EsbPacket<SoapReqBody> requestPakcet = gets003003990MS6770Req(mS6770Bean);
		// 初始化HttpHeadAttrConfig
		HttpHeadAttrConfig httpHeadAttrConfig = EsbSend.getHttpHeadAttrConfig("S003003990MS6770", "UTF-8");
		// 初始化输出对象
		EsbPacket<SoapResBody> responsePakcet = getS003003990MS6770Res();
		// 发送报文
		responsePakcet = EsbClient.send(httpHeadAttrConfig, requestPakcet, responsePakcet);
		// 解析返回对象
		SoapResBody soapResBody = responsePakcet.getBody();
		Fault fault = soapResBody.getS003003990MS6770Res().getFault();
		
		// 封装返回对象
		EsbOutBean result = new EsbOutBean();
		//result.setClientNo(resMsgHeadOut.getMsgdetailflow());//二代交易流水号
		//result.setTellSeqNo(resMsgHeadOut.getCoreflow());//二代核心流水号
		result.setSendmsg(XmlUtils.toXml(requestPakcet));
		result.setRecmsg(XmlUtils.toXml(responsePakcet));
		result.setRetCode(fault.getFaultCode());
		result.setRetMsg(fault.getFaultString());
		return result;
	}

	@Override
	public EsbPacket<SoapReqBody> gets003003990MS6770Req(MS6770Bean mS6770Bean)
			throws InstantiationException, IllegalAccessException {
		// 初始化请求对象
		EsbPacket<SoapReqBody> request = EsbPacket.initEsbPacket(SoapReqBody.class, SoapNoEnvHeader.class, "soap:Envelope");
		request.setPackge_type("http://www.adtec.com.cn");
		request.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope/");
		// 获取当前请求报文体内容
		SoapReqBody soapReqBody = request.getBody();
		S003003990MS6770Req sM6770ReqBody = new S003003990MS6770Req();
		
        RequestBody requestBody = mS6770Bean.getRequestBody();
		// 设置报文中请求头对象
		sM6770ReqBody.setRequestHeader(mS6770Bean.getRequestHeader());
		
		List<ReqMsgHead> msghead = new ArrayList<ReqMsgHead>();
		msghead.add(mS6770Bean.getReqMsgHead());
		requestBody.setMsghead(msghead);
		// 设置body
		sM6770ReqBody.setRequestBody(requestBody);
		
		// 设置报文
		soapReqBody.setS003003990M6770Req(sM6770ReqBody);
		return request;
	}

	@Override
	public EsbPacket<SoapResBody> getS003003990MS6770Res() throws InstantiationException, IllegalAccessException {
		// 初始化输出对象
		EsbPacket<SoapResBody> response = EsbPacket.initEsbPacket(SoapResBody.class, SoapHeader.class,"soapenv:Envelope");
		response.setPackge_type("http://www.cqrcb.com.cn");
		response.setSoapenvAttr("http://schemas.xmlsoap.org/soap/envelope");
		// 获取响应报文体
		SoapResBody soapResBody = response.getBody();
		// 申明报文对象
		S003003990MS6770Res sMS6770ResBody = new S003003990MS6770Res();
		//设置请求头
		ResponseHeader responseHeader = new ResponseHeader();
		sMS6770ResBody.setResponseHeader(responseHeader);
		
		ResponseBody responseBody = new ResponseBody();
		// 设置body中循环标签
		List<ResReturnCode> resReturnCode = new ArrayList<ResReturnCode>();
		List<ResMsgHeadOut> resMsgHeadOut = new ArrayList<ResMsgHeadOut>();
	    responseBody.setRETURNCODE(resReturnCode);
	    responseBody.setMsghead_out(resMsgHeadOut);
		// 设置body
		sMS6770ResBody.setResponseBody(responseBody);
		
		Fault fault = new Fault();
		Detail detail = new Detail();
		fault.setDetail(detail);
		sMS6770ResBody.setFault(fault);
		
		soapResBody.setS003003990MS6770Res(sMS6770ResBody);
		return response;
	}

}
