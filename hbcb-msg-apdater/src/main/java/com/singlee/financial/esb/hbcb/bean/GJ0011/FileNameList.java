package com.singlee.financial.esb.hbcb.bean.GJ0011;



public class FileNameList {
    //牌价时间，格式为“YYYY-MM-DD H24:Min:SS”
    private String fileTime;
    //币种
    private String currency;
    //基准价   
    private String base;
    //外币兑人民币市场价 即时更新，单位100   
    private String foreignMarketPrice;
    //外币兑美元价 即时更新，单位1  
    private String foreignToUSDPrice;
    //外币兑美元标价法 取值为“A”（间接标价法）和“B”（直接标价法）  
    private String foreignToUSDType;
    //钞买价   单位为100
    private String purchasePrice;
    //钞卖价   单位为100
    private String cashSellingRate;
    //汇买价   单位为100
    private String buyingRate;
    //汇卖价   单位为100
    private String sellingRate;
    //中间价   单位为100
    private String centralParity;
    //系统内平盘买入价   单位为100
    private String systemFlatBuying;
    //系统内平盘卖出价   单位为100
    private String systemFlatSelling;
    //决算价   单位为100
    private String finalPrice;
    public String getFileTime() {
        return fileTime;
    }
    public void setFileTime(String fileTime) {
        this.fileTime = fileTime;
    }
    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public String getBase() {
        return base;
    }
    public void setBase(String base) {
        this.base = base;
    }
    public String getForeignMarketPrice() {
        return foreignMarketPrice;
    }
    public void setForeignMarketPrice(String foreignMarketPrice) {
        this.foreignMarketPrice = foreignMarketPrice;
    }
    public String getForeignToUSDPrice() {
        return foreignToUSDPrice;
    }
    public void setForeignToUSDPrice(String foreignToUSDPrice) {
        this.foreignToUSDPrice = foreignToUSDPrice;
    }
    public String getForeignToUSDType() {
        return foreignToUSDType;
    }
    public void setForeignToUSDType(String foreignToUSDType) {
        this.foreignToUSDType = foreignToUSDType;
    }
    public String getPurchasePrice() {
        return purchasePrice;
    }
    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
    public String getCashSellingRate() {
        return cashSellingRate;
    }
    public void setCashSellingRate(String cashSellingRate) {
        this.cashSellingRate = cashSellingRate;
    }
    public String getBuyingRate() {
        return buyingRate;
    }
    public void setBuyingRate(String buyingRate) {
        this.buyingRate = buyingRate;
    }
    public String getSellingRate() {
        return sellingRate;
    }
    public void setSellingRate(String sellingRate) {
        this.sellingRate = sellingRate;
    }
    public String getCentralParity() {
        return centralParity;
    }
    public void setCentralParity(String centralParity) {
        this.centralParity = centralParity;
    }
    public String getSystemFlatBuying() {
        return systemFlatBuying;
    }
    public void setSystemFlatBuying(String systemFlatBuying) {
        this.systemFlatBuying = systemFlatBuying;
    }
    public String getSystemFlatSelling() {
        return systemFlatSelling;
    }
    public void setSystemFlatSelling(String systemFlatSelling) {
        this.systemFlatSelling = systemFlatSelling;
    }
    public String getFinalPrice() {
        return finalPrice;
    }
    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }
    @Override
    public String toString() {
        return "FileNameList [fileTime=" + fileTime + ", currency=" + currency + ", base=" + base
                + ", foreignMarketPrice=" + foreignMarketPrice + ", foreignToUSDPrice=" + foreignToUSDPrice
                + ", foreignToUSDType=" + foreignToUSDType + ", purchasePrice=" + purchasePrice + ", cashSellingRate="
                + cashSellingRate + ", buyingRate=" + buyingRate + ", sellingRate=" + sellingRate + ", centralParity="
                + centralParity + ", systemFlatBuying=" + systemFlatBuying + ", systemFlatSelling=" + systemFlatSelling
                + ", finalPrice=" + finalPrice + "]";
    }
    
    
    
}
