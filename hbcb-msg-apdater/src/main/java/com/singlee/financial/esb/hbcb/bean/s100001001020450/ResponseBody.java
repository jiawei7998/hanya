package com.singlee.financial.esb.hbcb.bean.s100001001020450;

import java.util.List;

/**
 * 请求返回体 主要应用与<ns:S100001001020450>标签
 * 
 *
 */
public class ResponseBody {

	/**
	 * 记录号
	 */
	private String RecNumber;
	/**
	 * 下一页选项 1：是 0：否
	 */
	private String Option;

	/**
	 * 交易日期
	 */
	private String TxDate;

	/**
	 * 交易类型为01-金融交易
	 */
	private List<ResDetailList> detail01;
	/**
	 * 当交易类型为05-BGL利息计提及结转
	 */
	private List<ResDetailList> detail05;
	/**
	 * 当交易类型为20-对账单摘要
	 */
	private List<ResDetailList> detail20;
	/**
	 * 当交易类别为21-交易备注
	 */
	private List<ResDetailList> detail21;
	/**
	 * 当交易类型为22-销账信息，且来往帐标示为FROM或者TO(GL0011-FROM-TO=‘FROM‘or‘TO’)
	 */
	private List<ResDetailList> detail22;
	/**
	 * 当交易类型为22,其他情形
	 */
	private List<ResDetailList> detailOth22;
	/**
	 * 当交易类型为36-转账信息
	 */
	private List<ResDetailList> detail36;
	/**
	 * 当交易类型为69-对手信息
	 */
	private List<ResDetailList> detail69;
	/**
	 * 当交易类型为70-代理信息
	 */
	private List<ResDetailList> detail70;

	public String getRecNumber() {
		return RecNumber;
	}

	public void setRecNumber(String recNumber) {
		RecNumber = recNumber;
	}

	public String getOption() {
		return Option;
	}

	public void setOption(String option) {
		Option = option;
	}

	public String getTxDate() {
		return TxDate;
	}

	public void setTxDate(String txDate) {
		TxDate = txDate;
	}

	public List<ResDetailList> getDetail01() {
		return detail01;
	}

	public void setDetail01(List<ResDetailList> detail01) {
		this.detail01 = detail01;
	}

	public List<ResDetailList> getDetail05() {
		return detail05;
	}

	public void setDetail05(List<ResDetailList> detail05) {
		this.detail05 = detail05;
	}

	public List<ResDetailList> getDetail20() {
		return detail20;
	}

	public void setDetail20(List<ResDetailList> detail20) {
		this.detail20 = detail20;
	}

	public List<ResDetailList> getDetail21() {
		return detail21;
	}

	public void setDetail21(List<ResDetailList> detail21) {
		this.detail21 = detail21;
	}

	public List<ResDetailList> getDetail22() {
		return detail22;
	}

	public void setDetail22(List<ResDetailList> detail22) {
		this.detail22 = detail22;
	}

	public List<ResDetailList> getDetailOth22() {
		return detailOth22;
	}

	public void setDetailOth22(List<ResDetailList> detailOth22) {
		this.detailOth22 = detailOth22;
	}

	public List<ResDetailList> getDetail36() {
		return detail36;
	}

	public void setDetail36(List<ResDetailList> detail36) {
		this.detail36 = detail36;
	}

	public List<ResDetailList> getDetail69() {
		return detail69;
	}

	public void setDetail69(List<ResDetailList> detail69) {
		this.detail69 = detail69;
	}

	public List<ResDetailList> getDetail70() {
		return detail70;
	}

	public void setDetail70(List<ResDetailList> detail70) {
		this.detail70 = detail70;
	}

}
