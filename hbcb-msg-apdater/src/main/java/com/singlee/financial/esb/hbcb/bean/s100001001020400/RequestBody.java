package com.singlee.financial.esb.hbcb.bean.s100001001020400;

import java.io.Serializable;

/**
 * 主要应用与<cqr:S100001001020400>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 必输 账号
	 */
	private String AcctNo;

	/**
	 * 必输 选项 选择：‘1' – 会计帐号一般查询 (渠道仅需求选项1) 选择：‘2' –存同\本行支票短查询 选择：‘3' –存同\本行支票余额查询
	 */
	private String Option;

	public String getAcctNo() {
		return AcctNo;
	}

	public void setAcctNo(String acctNo) {
		AcctNo = acctNo;
	}

	public String getOption() {
		return Option;
	}

	public void setOption(String option) {
		Option = option;
	}

}
