package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.bean.GJ0011.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.GJ0011.SoapResBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.GJ0011Bean;


public interface GJ0011Service {
    
    /**
     * 
     * @param gJ0011Bean
     * @return
     * @throws Exception
     */
    EsbOutBean send(GJ0011Bean gJ0011Bean) throws Exception;

    /**
     * 组装请求报文
     * 
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    EsbPacket<SoapReqBody> getgJ0011Req(GJ0011Bean gJ0011Bean)
            throws InstantiationException, IllegalAccessException;

    /**
     * 初始化响应报文
     * 
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    EsbPacket<SoapResBody> getGJ0011Res() throws InstantiationException, IllegalAccessException;
}



