package com.singlee.financial.esb.hbcb.bean.s100001001020400;

import java.io.Serializable;

import com.singlee.financial.esb.hbcb.bean.common.RequestHeader;

/**
 * 内部账BGL短查询
 * 
 * 主要处理<cqr:S100001001020400>标签
 * 
 *
 */
public class S100001001020400Req implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
	 * 请求头
	 */
	private RequestHeader RequestHeader;
	/**
	 * 请求体
	 */
	private RequestBody RequestBody;

	public RequestHeader getRequestHeader() {
		return RequestHeader;
	}

	public void setRequestHeader(RequestHeader requestHeader) {
		RequestHeader = requestHeader;
	}

	public RequestBody getRequestBody() {
		return RequestBody;
	}

	public void setRequestBody(RequestBody requestBody) {
		RequestBody = requestBody;
	}

}
