package com.singlee.financial.esb.hbcb.bean.GJ0011;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体(响应报文)
 * 
 * 中俄跨境支付平台系统通过ESB获取国结系统提供的牌价。
 * 
 *
 */
public class SoapResBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	private GJ0011Res gJ0011Res;

	public GJ0011Res getGJ0011Res() {
		return gJ0011Res;
	}

	public void setGJ0011Res(GJ0011Res gJ0011Res) {
		this.gJ0011Res = gJ0011Res;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapResBody.class);
		// 给当前类起别名                               
		info.addAlias(SoapResBody.class, "gJ0011Res", "ns:S010003010GJ0011");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:cqr");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		// info.addAlias(ResponseBody.class, "detailList","Detail1",ResDetailList.class);
		return info;
	}

}
