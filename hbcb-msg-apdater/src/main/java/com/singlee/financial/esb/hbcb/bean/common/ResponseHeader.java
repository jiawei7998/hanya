package com.singlee.financial.esb.hbcb.bean.common;

/**
 * 字段包含报文信息中公用应答头中的所有信息
 * 
 * @author shenzl
 *
 */
public class ResponseHeader {

	/**
	 * 版本号
	 */
	private String VerNo;
	/**
	 * 响应方系统代码
	 */
	private String RespSysCd;
	/**
	 * 响应方安全节点号
	 */
	private String RespSecCd;
	/**
	 * 交易码
	 */
	private String TxnCd;
	/**
	 * 公共交易名称
	 */
	private String TxnNme;
	/**
	 * 请求方交易日期
	 */
	private String ReqDt;
	/**
	 * 请求方交易时间戳
	 */
	private String ReqTm;
	/**
	 * 请求方流水号
	 */
	private String ReqSeqNo;
	/**
	 * 服务方交易日期
	 */
	private String SvrDt;
	/**
	 * 服务方交易时间戳
	 */
	private String SvrTm;
	/**
	 * 服务方交易流水号
	 */
	private String SvrSeqNo;
	/**
	 * 半自动/自动标识
	 */
	private String AutoFlag;
	/**
	 * 标识1
	 */
	private String Flag1;
	/**
	 * 标识4
	 */
	private String Flag4;
	/**
	 * 终端类型
	 */
	private String TrmType;
	/**
	 * 授权标志
	 */
	private String AuthFlg;
	/**
	 * 机构码
	 */
	private String BrchNo;
	/**
	 * 公共交易机构名称
	 */
	private String BrchNme;
	/**
	 * 申请密钥的设备代码
	 */
	private String DevCd;
	/**
	 * 交易柜员
	 */
	private String TlrNo;
	/**
	 * 
	 * 流水号
	 */
	private String BancsSeqNo;
	/**
	 * 操作员级别
	 */
	private String TlrLvl;
	/**
	 * 柜员名称
	 */
	private String TlrNme;
	/**
	 * 操作员种类
	 */
	private String TlrTyp;
	/**
	 * 柜员密码
	 */
	private String TlrPwd;
	/**
	 * 终端号
	 */
	private String TrmNo;
	/**
	 * 终端IP地址
	 */
	private String TrmIP;
	/**
	 * 交易来源
	 */
	private String ChnlNo;
	/**
	 * 接收文件名
	 */
	private String RcvFileNme;
	/**
	 * 总记录数
	 */
	private String TotNum;
	/**
	 * 
	 * 当前记录数
	 */
	private String CurrRecNum;
	/**
	 * 文件MAC值
	 */
	private String FileHMac;
	/**
	 * 报文MAC值
	 */
	private String HMac;
	
	private String AgtSeqNo;
	private String SvrTxnCd;
	private String TermNo;
	private String HostSeqNo;
	private String AsFlg;
	private String FileFlg;

	public String getVerNo() {
		return VerNo;
	}

	public void setVerNo(String verNo) {
		VerNo = verNo;
	}

	public String getRespSysCd() {
		return RespSysCd;
	}

	public void setRespSysCd(String respSysCd) {
		RespSysCd = respSysCd;
	}

	public String getRespSecCd() {
		return RespSecCd;
	}

	public void setRespSecCd(String respSecCd) {
		RespSecCd = respSecCd;
	}

	public String getTxnCd() {
		return TxnCd;
	}

	public void setTxnCd(String txnCd) {
		TxnCd = txnCd;
	}

	public String getTxnNme() {
		return TxnNme;
	}

	public void setTxnNme(String txnNme) {
		TxnNme = txnNme;
	}

	public String getReqDt() {
		return ReqDt;
	}

	public void setReqDt(String reqDt) {
		ReqDt = reqDt;
	}

	public String getReqTm() {
		return ReqTm;
	}

	public void setReqTm(String reqTm) {
		ReqTm = reqTm;
	}

	public String getReqSeqNo() {
		return ReqSeqNo;
	}

	public void setReqSeqNo(String reqSeqNo) {
		ReqSeqNo = reqSeqNo;
	}

	public String getSvrDt() {
		return SvrDt;
	}

	public void setSvrDt(String svrDt) {
		SvrDt = svrDt;
	}

	public String getSvrTm() {
		return SvrTm;
	}

	public void setSvrTm(String svrTm) {
		SvrTm = svrTm;
	}

	public String getSvrSeqNo() {
		return SvrSeqNo;
	}

	public void setSvrSeqNo(String svrSeqNo) {
		SvrSeqNo = svrSeqNo;
	}

	public String getAutoFlag() {
		return AutoFlag;
	}

	public void setAutoFlag(String autoFlag) {
		AutoFlag = autoFlag;
	}

	public String getFlag1() {
		return Flag1;
	}

	public void setFlag1(String flag1) {
		Flag1 = flag1;
	}

	public String getFlag4() {
		return Flag4;
	}

	public void setFlag4(String flag4) {
		Flag4 = flag4;
	}

	public String getTrmType() {
		return TrmType;
	}

	public void setTrmType(String trmType) {
		TrmType = trmType;
	}

	public String getAuthFlg() {
		return AuthFlg;
	}

	public void setAuthFlg(String authFlg) {
		AuthFlg = authFlg;
	}

	public String getBrchNo() {
		return BrchNo;
	}

	public void setBrchNo(String brchNo) {
		BrchNo = brchNo;
	}

	public String getBrchNme() {
		return BrchNme;
	}

	public void setBrchNme(String brchNme) {
		BrchNme = brchNme;
	}

	public String getDevCd() {
		return DevCd;
	}

	public void setDevCd(String devCd) {
		DevCd = devCd;
	}

	public String getTlrNo() {
		return TlrNo;
	}

	public void setTlrNo(String tlrNo) {
		TlrNo = tlrNo;
	}

	public String getBancsSeqNo() {
		return BancsSeqNo;
	}

	public void setBancsSeqNo(String bancsSeqNo) {
		BancsSeqNo = bancsSeqNo;
	}

	public String getTlrLvl() {
		return TlrLvl;
	}

	public void setTlrLvl(String tlrLvl) {
		TlrLvl = tlrLvl;
	}

	public String getTlrNme() {
		return TlrNme;
	}

	public void setTlrNme(String tlrNme) {
		TlrNme = tlrNme;
	}

	public String getTlrTyp() {
		return TlrTyp;
	}

	public void setTlrTyp(String tlrTyp) {
		TlrTyp = tlrTyp;
	}

	public String getTlrPwd() {
		return TlrPwd;
	}

	public void setTlrPwd(String tlrPwd) {
		TlrPwd = tlrPwd;
	}

	public String getTrmNo() {
		return TrmNo;
	}

	public void setTrmNo(String trmNo) {
		TrmNo = trmNo;
	}

	public String getTrmIP() {
		return TrmIP;
	}

	public void setTrmIP(String trmIP) {
		TrmIP = trmIP;
	}

	public String getChnlNo() {
		return ChnlNo;
	}

	public void setChnlNo(String chnlNo) {
		ChnlNo = chnlNo;
	}

	public String getRcvFileNme() {
		return RcvFileNme;
	}

	public void setRcvFileNme(String rcvFileNme) {
		RcvFileNme = rcvFileNme;
	}

	public String getTotNum() {
		return TotNum;
	}

	public void setTotNum(String totNum) {
		TotNum = totNum;
	}

	public String getCurrRecNum() {
		return CurrRecNum;
	}

	public void setCurrRecNum(String currRecNum) {
		CurrRecNum = currRecNum;
	}

	public String getFileHMac() {
		return FileHMac;
	}

	public void setFileHMac(String fileHMac) {
		FileHMac = fileHMac;
	}

	public String getHMac() {
		return HMac;
	}

	public void setHMac(String hMac) {
		HMac = hMac;
	}

	public String getAgtSeqNo() {
		return AgtSeqNo;
	}

	public void setAgtSeqNo(String agtSeqNo) {
		AgtSeqNo = agtSeqNo;
	}

	public String getSvrTxnCd() {
		return SvrTxnCd;
	}

	public void setSvrTxnCd(String svrTxnCd) {
		SvrTxnCd = svrTxnCd;
	}

	public String getTermNo() {
		return TermNo;
	}

	public void setTermNo(String termNo) {
		TermNo = termNo;
	}

	public String getHostSeqNo() {
		return HostSeqNo;
	}

	public void setHostSeqNo(String hostSeqNo) {
		HostSeqNo = hostSeqNo;
	}

	public String getAsFlg() {
		return AsFlg;
	}

	public void setAsFlg(String asFlg) {
		AsFlg = asFlg;
	}

	public String getFileFlg() {
		return FileFlg;
	}

	public void setFileFlg(String fileFlg) {
		FileFlg = fileFlg;
	}

}
