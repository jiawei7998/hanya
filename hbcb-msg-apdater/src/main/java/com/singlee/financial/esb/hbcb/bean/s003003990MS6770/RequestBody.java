package com.singlee.financial.esb.hbcb.bean.s003003990MS6770;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.singlee.financial.esb.hbcb.bean.common.ReqMsgHead;

/**
 * 主要应用与<cqr:S003003990MS6670>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 支付平台流水号 发起贷记业务入账状态查询业务时，支付平台返回的支付平台流水号MSGDETAILFLOW
	 */// MSGDETAILFLOW
	private String msgdetailflow;

	/**
	 * 匹配方式
	 * 1)当匹配方式为1时,支付平台流水号必填,其余不填写; 2）当匹配方式为2时，原渠道号、原渠道日期、渠道流水号必填，其余不填写。
	 * 
	 * 1.按支付平台信息查询 2.按渠道三要素查询道三要素为渠道号、渠道日期、原渠道流水号
	 * 
	 */
	private String matchmode;
	
	
	//原支付业务报文标识号
	private String oglmsgid;

	//原支付业务明细标识号
	private String ogldetailid;

	//原发起行行号OGLSNDPARTY
	private String oglsndparty;
	//原通道 ORGNLSYSCD
	private String orgnlsyscd ; 
	/**
	 * 原渠道号 OCHANID
	 */
	private String ochanid;
	/**
	 * 原渠道日期 OCHANDT
	 */
	private String ochandt;
	/**
	 * 原渠道流水号 OCHANFLOW
	 */
	private String ocganflow;
	/**
	 * 扩展域 EXTENDDATA
	 */
	private Map<Object, Object> extenddata;
	
    //报文头 MSGHEAD
	private List<ReqMsgHead> msghead;

    public String getMsgdetailflow() {
        return msgdetailflow;
    }

    public void setMsgdetailflow(String msgdetailflow) {
        this.msgdetailflow = msgdetailflow;
    }

    public String getMatchmode() {
        return matchmode;
    }

    public void setMatchmode(String matchmode) {
        this.matchmode = matchmode;
    }

    public String getOglmsgid() {
        return oglmsgid;
    }

    public void setOglmsgid(String oglmsgid) {
        this.oglmsgid = oglmsgid;
    }

    public String getOgldetailid() {
        return ogldetailid;
    }

    public void setOgldetailid(String ogldetailid) {
        this.ogldetailid = ogldetailid;
    }

    public String getOglsndparty() {
        return oglsndparty;
    }

    public void setOglsndparty(String oglsndparty) {
        this.oglsndparty = oglsndparty;
    }

    public String getOrgnlsyscd() {
        return orgnlsyscd;
    }

    public void setOrgnlsyscd(String orgnlsyscd) {
        this.orgnlsyscd = orgnlsyscd;
    }

    public String getOchanid() {
        return ochanid;
    }

    public void setOchanid(String ochanid) {
        this.ochanid = ochanid;
    }

    public String getOchandt() {
        return ochandt;
    }

    public void setOchandt(String ochandt) {
        this.ochandt = ochandt;
    }

    public String getOcganflow() {
        return ocganflow;
    }

    public void setOcganflow(String ocganflow) {
        this.ocganflow = ocganflow;
    }

    public Map<Object, Object> getExtenddata() {
        return extenddata;
    }

    public void setExtenddata(Map<Object, Object> extenddata) {
        this.extenddata = extenddata;
    }

    public List<ReqMsgHead> getMsghead() {
        return msghead;
    }

    public void setMsghead(List<ReqMsgHead> msghead) {
        this.msghead = msghead;
    }

 
}
