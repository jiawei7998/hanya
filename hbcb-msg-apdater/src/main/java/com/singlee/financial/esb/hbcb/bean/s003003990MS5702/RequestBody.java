package com.singlee.financial.esb.hbcb.bean.s003003990MS5702;

import java.io.Serializable;
import java.util.List;

import com.singlee.financial.esb.hbcb.bean.common.ReqMsgHead;

/**
 * 主要应用与<cqr:S003003990MS5702>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * O 通道
	 */
	private String syscd;
	/**
	 * O 系统编码
	 */
	private String domain;
	/**
	 * M 交易行为 该字段解释权归FDM
	 */
	private String operationtype;
	/**
	 * O 产品编码
	 */
	private String productid;
	/**
	 * O 强制执行标志 大额通道关闭时是否继续交易
	 */
	private String procuctflag;
	/**
	 * O 银行附言
	 */
	private String brfno;
	/**
	 * O 交易附言 报文中的交易附言，业务类型为国库汇款时必填
	 */
	private String remark;
	/**
	 * O 交易附言2 报文中的交易附言2
	 */
	private String remark2;
	/**
	 * O 备注
	 */
	private String rmkinfo;
	/**
	 * O 备注2
	 */
	private String rmkinfo2;
	/**
	 * M 支付类型 区别客户发起和金融机构发起
	 */
	private String paytyp;
	/**
	 * M 手续费收费方式
	 */
	private String feemod;
	/**
	 * O 手续费挂账机构 收费时填写
	 */
	private String clearpartyid;
	/**
	 * O 是否累加手续费发生额 收费时填写
	 */
	private String feeaddcount;
	/**
	 * O 手续费协议编码 收费时填写/（原费率种类）
	 */
	private String feetyp;
	/**
	 * O 手续费金额 收费时填写
	 */
	private String feeamt;
	/**
	 * O 手续费扣款账号 收费时填写/（原付费账号）
	 */
	private String feetrno;
	/**
	 * O 财务项目编码 收费时填写
	 */
	private String feeitem;
	/**
	 * O 手续费账号密码 收费时填写/付费账号密码
	 */
	private String feepwd;
	/**
	 * O 手续费账号密码校验方式 收费时填写，按PE规则传入 此项为本汇票票据、支票等凭证、卡活期一本通支付的密码校验标志共三位XXX：
	 * 第一位X为卡密码校验方式，取值（1-不检查，2-检查交易密码、3-检查查询密码）
	 * 第二位X为凭证支付密码校验方式，取值（0-有则验，无则不验、1-不检查、2-强制校验密码） 第三位X为票据密押方式，取值（0-有押验，无押不验 1-不校验
	 * 2-强制校验）
	 */
	private String feepwdcheck;
	/**
	 * O 付款凭证种类
	 */
	private String proofctg;
	/**
	 * O 付款凭证号码
	 */
	private String proofno;
	/**
	 * O 业务优先级 当通道为1225时必输
	 */
	private String priority;
	/**
	 * M 业务类型
	 */
	private String txtpcd;
	/**
	 * M 业务种类
	 */
	private String ctgypurpcd;
	/**
	 * O 端到端标识号
	 * 1.当业务类型为“委托收款（划回）、托收承付（划回）、商业汇票、支票、银行汇票、银行本票、城市商业银行汇票”时：填写内容为“票据号码/付款凭证号码”；
	 * 2.当业务类型为“跨境支付”时：填写内容为“关联业务参考号”
	 */
	private String ptpid;
	/**
	 * O 付款账户类型 付款人账户类型
	 */
	private String dbtrtyp;
	/**
	 * O 付款账户账号 付款人账号 现金交易时不上送值
	 */
	private String dbtrno;
	/**
	 * O 付款账户名称 付款人名称 现金交易时不上送值
	 */
	private String dbtrnm;
	/**
	 * O 付款人地址
	 */
	private String dbtradr;
	/**
	 * O 收款账户账号 收款人账号
	 */
	private String cdtrno;
	/**
	 * O 收款账户名称 收款人名称
	 */
	private String cdtrnm;
	/**
	 * O 收款行行号
	 */
	private String cdtbranchid;
	/**
	 * O 收款人地址
	 */
	private String cdtradr;
	/**
	 * O 收款账户类型 收款账户种类
	 */
	private String cdtrtyp;
	/**
	 * O 收款凭证种类
	 */
	private String vouchctg;
	/**
	 * O 收款凭证号码
	 */
	private String vouchno;
	/**
	 * O 中介机构1 不允许中文
	 */
	private String mediparty1;
	/**
	 * O 中介机构1名称
	 */
	private String mediparty1nm;
	/**
	 * O 解圈标志 1-解圈 0-不需要解圈
	 */
	private String dfuflag;
	/**
	 * O 圈存流水号 解圈标志为1时，需解圈扣款时填写(必填)
	 */
	private String dfuflow;
	/**
	 * O 圈存金额 解圈标志为1时，原圈存时的圈存金额(必填)
	 */
	private String dfuamt;
	/**
	 * O 中介机构2 不允许中文
	 */
	private String mediparty2;
	/**
	 * O 中介机构2名称
	 */
	private String mediparty2nm;
	/**
	 * M 转账账号 现金交易时，上送0
	 */
	private String trftrno;
	/**
	 * M 转账户名 现金交易时，上送0
	 */
	private String trftrnm;
	/**
	 * O 转账账户类型 现转标识为转账时必输
	 */
	private String trftrtp;
	/**
	 * M 币种
	 */
	private String ccy;
	/**
	 * M 交易金额 金额
	 */
	private String amt;
	/**
	 * M 现转标识
	 */
	private String actflg;
	/**
	 * O 现金项目代码 按统一的编码规则，现转标识为现金时必填
	 */
	private String cashprojectcode;
	/**
	 * O 尾箱号 现转标识为现金时必输
	 */
	private String cashuserloginid;
	/**
	 * O 交易类型 现转标识为现金时使用
	 */
	private String transtyp;
	/**
	 * O 付款账号密码 付款账号密码
	 */
	private String paypwd;
	/**
	 * O 付款账号密码校验方式 按PE规则传入 此项为本汇票票据、支票等凭证、卡活期一本通支付的密码校验标志共三位XXX：
	 * 第一位X为卡密码校验方式，取值（1-不检查，2-检查交易密码、3-检查查询密码）
	 * 第二位X为凭证支付密码校验方式，取值（0-有则验，无则不验、1-不检查、2-强制校验密码） 第三位X为票据密押方式，取值（0-有押验，无押不验 1-不校验
	 * 2-强制校验）
	 */
	private String paypwdcheck;
	/**
	 * O 支付密码
	 */
	private String reiptpwd;
	/**
	 * 交易编码
	 */
	private String trcode;
	/**
	 * 收款人国别
	 */
	private String cdtrcounty;
	/**
	 * O 付款人国别
	 */
	private String dbtrcounty;
	/**
	 * O 付款人证件种类
	 */
	private String dbtrcredtp;
	/**
	 * O 付款人证件号码
	 */
	private String dbtrcredno;
	/**
	 * O 付款人证件发证机关
	 */
	private String dbtrcredog;
	/**
	 * O 付款人联系电话
	 */
	private String dbtrtelno;
	/**
	 * O 经办人名称
	 */
	private String dealnm;
	/**
	 * O 经办人联系电话
	 */
	private String dealtelno;
	/**
	 * O 经办人国籍
	 */
	private String dealcounty;
	/**
	 * O 经办人证件种类
	 */
	private String dealcredtp;
	/**
	 * O 经办人证件号码
	 */
	private String dealcredno;
	/**
	 * O 经办人证件发件机关
	 */
	private String dealcredog;
	/**
	 * O 票据日期 业务类型为委托收款划回、托收承付划回时必填
	 */
	private String repitdt;
	/**
	 * O 票据种类 业务类型为委托收款时必填
	 */
	private String reiptctg;
	/**
	 * 票据号码 通道为1226，业务类型为委托收款划回、托收承付划回时必填
	 */
	private String reiptno;
	/**
	 * O 赔偿金额 业务类型为托收承付划回时选填
	 */
	private String claimamt;
	/**
	 * O 拒付金额 业务类型为托收承付划回时选填
	 */
	private String rejectamt;
	/**
	 * O 原托金额 业务类型为托收承付划回时选填
	 */
	private String oglamt;
	/**
	 * O 支付金额 业务类型为托收承付划回时选填
	 */
	private String payamt;
	/**
	 * O 多付金额 业务类型为托收承付划回时选填
	 */
	private String paybalance;
	/**
	 * O 出票日期 业务类型为商业汇票、支票业务、银行汇票、银行本票时必填
	 */
	private String reiptdt;
	/**
	 * O 拆借利率 业务类型为行间资金汇划时填写
	 */
	private String rate;
	/**
	 * O 拆借期限 业务类型为行间资金汇划时填写
	 */
	private String brwdline;
	/**
	 * O 二磁道信息
	 */
	private String secondinfo;
	/**
	 * O 三磁道数据
	 */
	private String thirdinfo;
	/**
	 * O 密钥组内索引
	 */
	private String keyindex;
	/**
	 * O 应用系统标识
	 */
	private String systenid;

	private List<ReqMsgHead> msghead;

	public String getSyscd() {
		return syscd;
	}

	public void setSyscd(String syscd) {
		this.syscd = syscd;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getOperationtype() {
		return operationtype;
	}

	public void setOperationtype(String operationtype) {
		this.operationtype = operationtype;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getProcuctflag() {
		return procuctflag;
	}

	public void setProcuctflag(String procuctflag) {
		this.procuctflag = procuctflag;
	}

	public String getBrfno() {
		return brfno;
	}

	public void setBrfno(String brfno) {
		this.brfno = brfno;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRmkinfo() {
		return rmkinfo;
	}

	public void setRmkinfo(String rmkinfo) {
		this.rmkinfo = rmkinfo;
	}

	public String getRmkinfo2() {
		return rmkinfo2;
	}

	public void setRmkinfo2(String rmkinfo2) {
		this.rmkinfo2 = rmkinfo2;
	}

	public String getPaytyp() {
		return paytyp;
	}

	public void setPaytyp(String paytyp) {
		this.paytyp = paytyp;
	}

	public String getFeemod() {
		return feemod;
	}

	public void setFeemod(String feemod) {
		this.feemod = feemod;
	}

	public String getClearpartyid() {
		return clearpartyid;
	}

	public void setClearpartyid(String clearpartyid) {
		this.clearpartyid = clearpartyid;
	}

	public String getFeeaddcount() {
		return feeaddcount;
	}

	public void setFeeaddcount(String feeaddcount) {
		this.feeaddcount = feeaddcount;
	}

	public String getFeetyp() {
		return feetyp;
	}

	public void setFeetyp(String feetyp) {
		this.feetyp = feetyp;
	}

	public String getFeeamt() {
		return feeamt;
	}

	public void setFeeamt(String feeamt) {
		this.feeamt = feeamt;
	}

	public String getFeetrno() {
		return feetrno;
	}

	public void setFeetrno(String feetrno) {
		this.feetrno = feetrno;
	}

	public String getFeeitem() {
		return feeitem;
	}

	public void setFeeitem(String feeitem) {
		this.feeitem = feeitem;
	}

	public String getFeepwd() {
		return feepwd;
	}

	public void setFeepwd(String feepwd) {
		this.feepwd = feepwd;
	}

	public String getFeepwdcheck() {
		return feepwdcheck;
	}

	public void setFeepwdcheck(String feepwdcheck) {
		this.feepwdcheck = feepwdcheck;
	}

	public String getProofctg() {
		return proofctg;
	}

	public void setProofctg(String proofctg) {
		this.proofctg = proofctg;
	}

	public String getProofno() {
		return proofno;
	}

	public void setProofno(String proofno) {
		this.proofno = proofno;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getTxtpcd() {
		return txtpcd;
	}

	public void setTxtpcd(String txtpcd) {
		this.txtpcd = txtpcd;
	}

	public String getCtgypurpcd() {
		return ctgypurpcd;
	}

	public void setCtgypurpcd(String ctgypurpcd) {
		this.ctgypurpcd = ctgypurpcd;
	}

	public String getPtpid() {
		return ptpid;
	}

	public void setPtpid(String ptpid) {
		this.ptpid = ptpid;
	}

	public String getDbtrtyp() {
		return dbtrtyp;
	}

	public void setDbtrtyp(String dbtrtyp) {
		this.dbtrtyp = dbtrtyp;
	}

	public String getDbtrno() {
		return dbtrno;
	}

	public void setDbtrno(String dbtrno) {
		this.dbtrno = dbtrno;
	}

	public String getDbtrnm() {
		return dbtrnm;
	}

	public void setDbtrnm(String dbtrnm) {
		this.dbtrnm = dbtrnm;
	}

	public String getDbtradr() {
		return dbtradr;
	}

	public void setDbtradr(String dbtradr) {
		this.dbtradr = dbtradr;
	}

	public String getCdtrno() {
		return cdtrno;
	}

	public void setCdtrno(String cdtrno) {
		this.cdtrno = cdtrno;
	}

	public String getCdtrnm() {
		return cdtrnm;
	}

	public void setCdtrnm(String cdtrnm) {
		this.cdtrnm = cdtrnm;
	}

	public String getCdtbranchid() {
		return cdtbranchid;
	}

	public void setCdtbranchid(String cdtbranchid) {
		this.cdtbranchid = cdtbranchid;
	}

	public String getCdtradr() {
		return cdtradr;
	}

	public void setCdtradr(String cdtradr) {
		this.cdtradr = cdtradr;
	}

	public String getCdtrtyp() {
		return cdtrtyp;
	}

	public void setCdtrtyp(String cdtrtyp) {
		this.cdtrtyp = cdtrtyp;
	}

	public String getVouchctg() {
		return vouchctg;
	}

	public void setVouchctg(String vouchctg) {
		this.vouchctg = vouchctg;
	}

	public String getVouchno() {
		return vouchno;
	}

	public void setVouchno(String vouchno) {
		this.vouchno = vouchno;
	}

	public String getMediparty1() {
		return mediparty1;
	}

	public void setMediparty1(String mediparty1) {
		this.mediparty1 = mediparty1;
	}

	public String getMediparty1nm() {
		return mediparty1nm;
	}

	public void setMediparty1nm(String mediparty1nm) {
		this.mediparty1nm = mediparty1nm;
	}

	public String getDfuflag() {
		return dfuflag;
	}

	public void setDfuflag(String dfuflag) {
		this.dfuflag = dfuflag;
	}

	public String getDfuflow() {
		return dfuflow;
	}

	public void setDfuflow(String dfuflow) {
		this.dfuflow = dfuflow;
	}

	public String getDfuamt() {
		return dfuamt;
	}

	public void setDfuamt(String dfuamt) {
		this.dfuamt = dfuamt;
	}

	public String getMediparty2() {
		return mediparty2;
	}

	public void setMediparty2(String mediparty2) {
		this.mediparty2 = mediparty2;
	}

	public String getMediparty2nm() {
		return mediparty2nm;
	}

	public void setMediparty2nm(String mediparty2nm) {
		this.mediparty2nm = mediparty2nm;
	}

	public String getTrftrno() {
		return trftrno;
	}

	public void setTrftrno(String trftrno) {
		this.trftrno = trftrno;
	}

	public String getTrftrnm() {
		return trftrnm;
	}

	public void setTrftrnm(String trftrnm) {
		this.trftrnm = trftrnm;
	}

	public String getTrftrtp() {
		return trftrtp;
	}

	public void setTrftrtp(String trftrtp) {
		this.trftrtp = trftrtp;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getActflg() {
		return actflg;
	}

	public void setActflg(String actflg) {
		this.actflg = actflg;
	}

	public String getCashprojectcode() {
		return cashprojectcode;
	}

	public void setCashprojectcode(String cashprojectcode) {
		this.cashprojectcode = cashprojectcode;
	}

	public String getCashuserloginid() {
		return cashuserloginid;
	}

	public void setCashuserloginid(String cashuserloginid) {
		this.cashuserloginid = cashuserloginid;
	}

	public String getTranstyp() {
		return transtyp;
	}

	public void setTranstyp(String transtyp) {
		this.transtyp = transtyp;
	}

	public String getPaypwd() {
		return paypwd;
	}

	public void setPaypwd(String paypwd) {
		this.paypwd = paypwd;
	}

	public String getPaypwdcheck() {
		return paypwdcheck;
	}

	public void setPaypwdcheck(String paypwdcheck) {
		this.paypwdcheck = paypwdcheck;
	}

	public String getReiptpwd() {
		return reiptpwd;
	}

	public void setReiptpwd(String reiptpwd) {
		this.reiptpwd = reiptpwd;
	}

	public String getTrcode() {
		return trcode;
	}

	public void setTrcode(String trcode) {
		this.trcode = trcode;
	}

	public String getCdtrcounty() {
		return cdtrcounty;
	}

	public void setCdtrcounty(String cdtrcounty) {
		this.cdtrcounty = cdtrcounty;
	}

	public String getDbtrcounty() {
		return dbtrcounty;
	}

	public void setDbtrcounty(String dbtrcounty) {
		this.dbtrcounty = dbtrcounty;
	}

	public String getDbtrcredtp() {
		return dbtrcredtp;
	}

	public void setDbtrcredtp(String dbtrcredtp) {
		this.dbtrcredtp = dbtrcredtp;
	}

	public String getDbtrcredno() {
		return dbtrcredno;
	}

	public void setDbtrcredno(String dbtrcredno) {
		this.dbtrcredno = dbtrcredno;
	}

	public String getDbtrcredog() {
		return dbtrcredog;
	}

	public void setDbtrcredog(String dbtrcredog) {
		this.dbtrcredog = dbtrcredog;
	}

	public String getDbtrtelno() {
		return dbtrtelno;
	}

	public void setDbtrtelno(String dbtrtelno) {
		this.dbtrtelno = dbtrtelno;
	}

	public String getDealnm() {
		return dealnm;
	}

	public void setDealnm(String dealnm) {
		this.dealnm = dealnm;
	}

	public String getDealtelno() {
		return dealtelno;
	}

	public void setDealtelno(String dealtelno) {
		this.dealtelno = dealtelno;
	}

	public String getDealcounty() {
		return dealcounty;
	}

	public void setDealcounty(String dealcounty) {
		this.dealcounty = dealcounty;
	}

	public String getDealcredtp() {
		return dealcredtp;
	}

	public void setDealcredtp(String dealcredtp) {
		this.dealcredtp = dealcredtp;
	}

	public String getDealcredno() {
		return dealcredno;
	}

	public void setDealcredno(String dealcredno) {
		this.dealcredno = dealcredno;
	}

	public String getDealcredog() {
		return dealcredog;
	}

	public void setDealcredog(String dealcredog) {
		this.dealcredog = dealcredog;
	}

	public String getRepitdt() {
		return repitdt;
	}

	public void setRepitdt(String repitdt) {
		this.repitdt = repitdt;
	}

	public String getReiptctg() {
		return reiptctg;
	}

	public void setReiptctg(String reiptctg) {
		this.reiptctg = reiptctg;
	}

	public String getReiptno() {
		return reiptno;
	}

	public void setReiptno(String reiptno) {
		this.reiptno = reiptno;
	}

	public String getClaimamt() {
		return claimamt;
	}

	public void setClaimamt(String claimamt) {
		this.claimamt = claimamt;
	}

	public String getRejectamt() {
		return rejectamt;
	}

	public void setRejectamt(String rejectamt) {
		this.rejectamt = rejectamt;
	}

	public String getOglamt() {
		return oglamt;
	}

	public void setOglamt(String oglamt) {
		this.oglamt = oglamt;
	}

	public String getPayamt() {
		return payamt;
	}

	public void setPayamt(String payamt) {
		this.payamt = payamt;
	}

	public String getPaybalance() {
		return paybalance;
	}

	public void setPaybalance(String paybalance) {
		this.paybalance = paybalance;
	}

	public String getReiptdt() {
		return reiptdt;
	}

	public void setReiptdt(String reiptdt) {
		this.reiptdt = reiptdt;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getBrwdline() {
		return brwdline;
	}

	public void setBrwdline(String brwdline) {
		this.brwdline = brwdline;
	}

	public String getSecondinfo() {
		return secondinfo;
	}

	public void setSecondinfo(String secondinfo) {
		this.secondinfo = secondinfo;
	}

	public String getThirdinfo() {
		return thirdinfo;
	}

	public void setThirdinfo(String thirdinfo) {
		this.thirdinfo = thirdinfo;
	}

	public String getKeyindex() {
		return keyindex;
	}

	public void setKeyindex(String keyindex) {
		this.keyindex = keyindex;
	}

	public String getSystenid() {
		return systenid;
	}

	public void setSystenid(String systenid) {
		this.systenid = systenid;
	}

	public List<ReqMsgHead> getMsghead() {
		return msghead;
	}

	public void setMsghead(List<ReqMsgHead> msghead) {
		this.msghead = msghead;
	}

}
