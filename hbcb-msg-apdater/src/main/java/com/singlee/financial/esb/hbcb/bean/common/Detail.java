package com.singlee.financial.esb.hbcb.bean.common;

/**
 * 返回报文状态
 * 
 * @author shenzl
 *
 */
public class Detail {
	/**
	 * 处理状态
	 */
	private String TxnStat;

	public String getTxnStat() {
		return TxnStat;
	}

	public void setTxnStat(String txnStat) {
		TxnStat = txnStat;
	}

}
