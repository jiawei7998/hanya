package com.singlee.financial.esb.hbcb.bean.s100001001020450;

import java.io.Serializable;

/**
 *
 */
public class ResDetailList implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易类型为01-金融交易，占2行，2*80位，*10
	 * 
	 * 交易类型1
	 */
	private String Jylx_01;
	/**
	 * 交易日期1
	 */
	private String Jyrq_01;
	/**
	 * 机构号
	 */
	private String Jgh_01;
	/**
	 * 终端号
	 */
	private String Zdh_01;
	/**
	 * 柜员号
	 */
	private String Gyh_01;
	/**
	 * 交易代码
	 */
	private String Jydm_01;
	/**
	 * 提交日
	 */
	private String Tjr_01;
	/**
	 * 流水号
	 */
	private String Lsh_01;
	/**
	 * 交易状态
	 */
	private String JyZt_01;
	/**
	 * SUSP帐号
	 */
	private String SuspAcctNo;
	/**
	 * 交易完整描述
	 */
	private String Description;
	/**
	 * 交易金额
	 */
	private String Jyje_01;
	/**
	 * 账户余额
	 */
	private String Zhye_01;
	/**
	 * 提示码
	 */
	private String TsM_01;

	/**
	 * 当交易类型为05-BGL利息计提及结转，占2行，2*80位，*10
	 * 
	 * 交易类型 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Jylx_05;
	/**
	 * 借方计提
	 */
	private String Jfjt_05;
	/**
	 * 当日借方计提 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Drjfjt_05;
	/**
	 * 贷方计提
	 */
	private String Dfjt_05;
	/**
	 * 当日贷方计提
	 */
	private String Drdfjt_05;
	/**
	 * 利息类型 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Lxlx_05;
	/**
	 * 计息天数 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Jxts_05;
	/**
	 * 原流水 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Yls_05;
	/**
	 * 备注 //Alphanumeric 14 FILLER 置空格，set space
	 */
	private String Bz_05;

	/**
	 * 当交易类型为20-对账单摘要，*10
	 * 
	 * 交易类型2
	 */
	private String Jylx_20;
	/**
	 * 交易日期2
	 */
	private String Jyrq_20;
	/**
	 * 日志号
	 */
	private String Rzh_20;
	/**
	 * 摘要信息
	 */
	private String Zyxx_20;
	/**
	 * 提交日
	 */
	private String Tjr_20;

	/**
	 * 当交易类别为21-交易备注，*10
	 * 
	 * 交易类型3
	 */
	private String Jylx_21;
	/**
	 * 交易日期3
	 */
	private String Jyrq_21;
	/**
	 * 机构号
	 */
	private String Jgh_21;
	/**
	 * 终端号
	 */
	private String Zdh_21;
	/**
	 * 柜员号
	 */
	private String Gyh_21;
	/**
	 * 交易代码
	 */
	private String Jydm_21;
	/**
	 * 记账日期
	 */
	private String Jzrq_21;
	/**
	 * 日志号
	 */
	private String Rzh_21;
	/**
	 * 备注信息
	 */
	private String Bzxx_21;

	/**
	 * 当交易类型为22-销账信息，且来往帐标示为FROM或者TO(GL0011-FROM-TO=‘FROM‘or‘TO’)
	 * 
	 * 交易类型4
	 */
	private String Jylx_22;
	/**
	 * 交易日期4
	 */
	private String Jyrq_22;
	/**
	 * 日志号
	 */
	private String Rzh_22;
	/**
	 * 来往帐标示
	 */
	private String Wlzbs_22;
	/**
	 * 系统
	 */
	private String Xt_22;
	/**
	 * 账号
	 */
	private String Zh_22;
	/**
	 * 币种
	 */
	private String Bz_22;
	/**
	 * 金额
	 */
	private String Je_22;

	/**
	 * 当交易类型为22,其他情形
	 * 
	 * 交易类型5
	 */
	private String Jylxo_22;
	/**
	 * 交易日期5
	 */
	private String Jyrqo_22;
	/**
	 * 日志号
	 */
	private String Rzho_22;
	/**
	 * 销账信息
	 */
	private String Xzxxo_22;
	/**
	 * 销账码
	 */
	private String Xzmo_22;

	/**
	 * 当交易类型为36-转账信息
	 * 
	 * 交易类型6
	 */
	private String Jylx_36;
	/**
	 * 交易日期6
	 */
	private String Jyrq_36;
	/**
	 * 日志号
	 */
	private String Rzh_36;
	/**
	 * 转账信息
	 */
	private String Zzxx_36;

	/**
	 * 当交易类型为69-对手信息
	 * 
	 * 交易类型 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Jylx_69;

	/**
	 * 交易日期 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Jyrq_69;

	/**
	 * 日志号 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Rzh_69;

	/**
	 * 他行的账号或者卡号 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Thzh_69;

	/**
	 * 交易对手他行的行号 （交易支付行的行号，由人行规定，14位编码） //Alphanumeric 9 FILLER 置空格，set space
	 */
	private String Jyds_69;

	/**
	 * 当交易类型为70-代理信息
	 * 
	 * 交易类型 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Jylx_70;
	/**
	 * 交易日期 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Jyrq_70;
	/**
	 * 日志号 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Rzh_70;
	/**
	 * 取款人姓名 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Qkrxm_70;
	/**
	 * 签发行行号 //Alphanumeric 9 FILLER 置空格，set space //Alphanumeric 1 FILLER 置空格，set
	 * space
	 */
	private String Qfhh_70;

	/**
	 * 代理人证件类型 //Alphanumeric 1 FILLER 置空格，set space
	 */
	private String Dlrlx_70;

	/**
	 * 代理人证件号码 Alphanumeric 42 FILLER 置空格，set space
	 */
	private String Dlrhm_70;

	public String getJylx_01() {
		return Jylx_01;
	}

	public void setJylx_01(String jylx_01) {
		Jylx_01 = jylx_01;
	}

	public String getJyrq_01() {
		return Jyrq_01;
	}

	public void setJyrq_01(String jyrq_01) {
		Jyrq_01 = jyrq_01;
	}

	public String getJgh_01() {
		return Jgh_01;
	}

	public void setJgh_01(String jgh_01) {
		Jgh_01 = jgh_01;
	}

	public String getZdh_01() {
		return Zdh_01;
	}

	public void setZdh_01(String zdh_01) {
		Zdh_01 = zdh_01;
	}

	public String getGyh_01() {
		return Gyh_01;
	}

	public void setGyh_01(String gyh_01) {
		Gyh_01 = gyh_01;
	}

	public String getJydm_01() {
		return Jydm_01;
	}

	public void setJydm_01(String jydm_01) {
		Jydm_01 = jydm_01;
	}

	public String getTjr_01() {
		return Tjr_01;
	}

	public void setTjr_01(String tjr_01) {
		Tjr_01 = tjr_01;
	}

	public String getLsh_01() {
		return Lsh_01;
	}

	public void setLsh_01(String lsh_01) {
		Lsh_01 = lsh_01;
	}

	public String getJyZt_01() {
		return JyZt_01;
	}

	public void setJyZt_01(String jyZt_01) {
		JyZt_01 = jyZt_01;
	}

	public String getSuspAcctNo() {
		return SuspAcctNo;
	}

	public void setSuspAcctNo(String suspAcctNo) {
		SuspAcctNo = suspAcctNo;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getJyje_01() {
		return Jyje_01;
	}

	public void setJyje_01(String jyje_01) {
		Jyje_01 = jyje_01;
	}

	public String getZhye_01() {
		return Zhye_01;
	}

	public void setZhye_01(String zhye_01) {
		Zhye_01 = zhye_01;
	}

	public String getTsM_01() {
		return TsM_01;
	}

	public void setTsM_01(String tsM_01) {
		TsM_01 = tsM_01;
	}

	public String getJylx_05() {
		return Jylx_05;
	}

	public void setJylx_05(String jylx_05) {
		Jylx_05 = jylx_05;
	}

	public String getJfjt_05() {
		return Jfjt_05;
	}

	public void setJfjt_05(String jfjt_05) {
		Jfjt_05 = jfjt_05;
	}

	public String getDrjfjt_05() {
		return Drjfjt_05;
	}

	public void setDrjfjt_05(String drjfjt_05) {
		Drjfjt_05 = drjfjt_05;
	}

	public String getDfjt_05() {
		return Dfjt_05;
	}

	public void setDfjt_05(String dfjt_05) {
		Dfjt_05 = dfjt_05;
	}

	public String getDrdfjt_05() {
		return Drdfjt_05;
	}

	public void setDrdfjt_05(String drdfjt_05) {
		Drdfjt_05 = drdfjt_05;
	}

	public String getLxlx_05() {
		return Lxlx_05;
	}

	public void setLxlx_05(String lxlx_05) {
		Lxlx_05 = lxlx_05;
	}

	public String getJxts_05() {
		return Jxts_05;
	}

	public void setJxts_05(String jxts_05) {
		Jxts_05 = jxts_05;
	}

	public String getYls_05() {
		return Yls_05;
	}

	public void setYls_05(String yls_05) {
		Yls_05 = yls_05;
	}

	public String getBz_05() {
		return Bz_05;
	}

	public void setBz_05(String bz_05) {
		Bz_05 = bz_05;
	}

	public String getJylx_20() {
		return Jylx_20;
	}

	public void setJylx_20(String jylx_20) {
		Jylx_20 = jylx_20;
	}

	public String getJyrq_20() {
		return Jyrq_20;
	}

	public void setJyrq_20(String jyrq_20) {
		Jyrq_20 = jyrq_20;
	}

	public String getRzh_20() {
		return Rzh_20;
	}

	public void setRzh_20(String rzh_20) {
		Rzh_20 = rzh_20;
	}

	public String getZyxx_20() {
		return Zyxx_20;
	}

	public void setZyxx_20(String zyxx_20) {
		Zyxx_20 = zyxx_20;
	}

	public String getTjr_20() {
		return Tjr_20;
	}

	public void setTjr_20(String tjr_20) {
		Tjr_20 = tjr_20;
	}

	public String getJylx_21() {
		return Jylx_21;
	}

	public void setJylx_21(String jylx_21) {
		Jylx_21 = jylx_21;
	}

	public String getJyrq_21() {
		return Jyrq_21;
	}

	public void setJyrq_21(String jyrq_21) {
		Jyrq_21 = jyrq_21;
	}

	public String getJgh_21() {
		return Jgh_21;
	}

	public void setJgh_21(String jgh_21) {
		Jgh_21 = jgh_21;
	}

	public String getZdh_21() {
		return Zdh_21;
	}

	public void setZdh_21(String zdh_21) {
		Zdh_21 = zdh_21;
	}

	public String getGyh_21() {
		return Gyh_21;
	}

	public void setGyh_21(String gyh_21) {
		Gyh_21 = gyh_21;
	}

	public String getJydm_21() {
		return Jydm_21;
	}

	public void setJydm_21(String jydm_21) {
		Jydm_21 = jydm_21;
	}

	public String getJzrq_21() {
		return Jzrq_21;
	}

	public void setJzrq_21(String jzrq_21) {
		Jzrq_21 = jzrq_21;
	}

	public String getRzh_21() {
		return Rzh_21;
	}

	public void setRzh_21(String rzh_21) {
		Rzh_21 = rzh_21;
	}

	public String getBzxx_21() {
		return Bzxx_21;
	}

	public void setBzxx_21(String bzxx_21) {
		Bzxx_21 = bzxx_21;
	}

	public String getJylx_22() {
		return Jylx_22;
	}

	public void setJylx_22(String jylx_22) {
		Jylx_22 = jylx_22;
	}

	public String getJyrq_22() {
		return Jyrq_22;
	}

	public void setJyrq_22(String jyrq_22) {
		Jyrq_22 = jyrq_22;
	}

	public String getRzh_22() {
		return Rzh_22;
	}

	public void setRzh_22(String rzh_22) {
		Rzh_22 = rzh_22;
	}

	public String getWlzbs_22() {
		return Wlzbs_22;
	}

	public void setWlzbs_22(String wlzbs_22) {
		Wlzbs_22 = wlzbs_22;
	}

	public String getXt_22() {
		return Xt_22;
	}

	public void setXt_22(String xt_22) {
		Xt_22 = xt_22;
	}

	public String getZh_22() {
		return Zh_22;
	}

	public void setZh_22(String zh_22) {
		Zh_22 = zh_22;
	}

	public String getBz_22() {
		return Bz_22;
	}

	public void setBz_22(String bz_22) {
		Bz_22 = bz_22;
	}

	public String getJe_22() {
		return Je_22;
	}

	public void setJe_22(String je_22) {
		Je_22 = je_22;
	}

	public String getJylxo_22() {
		return Jylxo_22;
	}

	public void setJylxo_22(String jylxo_22) {
		Jylxo_22 = jylxo_22;
	}

	public String getJyrqo_22() {
		return Jyrqo_22;
	}

	public void setJyrqo_22(String jyrqo_22) {
		Jyrqo_22 = jyrqo_22;
	}

	public String getRzho_22() {
		return Rzho_22;
	}

	public void setRzho_22(String rzho_22) {
		Rzho_22 = rzho_22;
	}

	public String getXzxxo_22() {
		return Xzxxo_22;
	}

	public void setXzxxo_22(String xzxxo_22) {
		Xzxxo_22 = xzxxo_22;
	}

	public String getXzmo_22() {
		return Xzmo_22;
	}

	public void setXzmo_22(String xzmo_22) {
		Xzmo_22 = xzmo_22;
	}

	public String getJylx_36() {
		return Jylx_36;
	}

	public void setJylx_36(String jylx_36) {
		Jylx_36 = jylx_36;
	}

	public String getJyrq_36() {
		return Jyrq_36;
	}

	public void setJyrq_36(String jyrq_36) {
		Jyrq_36 = jyrq_36;
	}

	public String getRzh_36() {
		return Rzh_36;
	}

	public void setRzh_36(String rzh_36) {
		Rzh_36 = rzh_36;
	}

	public String getZzxx_36() {
		return Zzxx_36;
	}

	public void setZzxx_36(String zzxx_36) {
		Zzxx_36 = zzxx_36;
	}

	public String getJylx_69() {
		return Jylx_69;
	}

	public void setJylx_69(String jylx_69) {
		Jylx_69 = jylx_69;
	}

	public String getJyrq_69() {
		return Jyrq_69;
	}

	public void setJyrq_69(String jyrq_69) {
		Jyrq_69 = jyrq_69;
	}

	public String getRzh_69() {
		return Rzh_69;
	}

	public void setRzh_69(String rzh_69) {
		Rzh_69 = rzh_69;
	}

	public String getThzh_69() {
		return Thzh_69;
	}

	public void setThzh_69(String thzh_69) {
		Thzh_69 = thzh_69;
	}

	public String getJyds_69() {
		return Jyds_69;
	}

	public void setJyds_69(String jyds_69) {
		Jyds_69 = jyds_69;
	}

	public String getJylx_70() {
		return Jylx_70;
	}

	public void setJylx_70(String jylx_70) {
		Jylx_70 = jylx_70;
	}

	public String getJyrq_70() {
		return Jyrq_70;
	}

	public void setJyrq_70(String jyrq_70) {
		Jyrq_70 = jyrq_70;
	}

	public String getRzh_70() {
		return Rzh_70;
	}

	public void setRzh_70(String rzh_70) {
		Rzh_70 = rzh_70;
	}

	public String getQkrxm_70() {
		return Qkrxm_70;
	}

	public void setQkrxm_70(String qkrxm_70) {
		Qkrxm_70 = qkrxm_70;
	}

	public String getQfhh_70() {
		return Qfhh_70;
	}

	public void setQfhh_70(String qfhh_70) {
		Qfhh_70 = qfhh_70;
	}

	public String getDlrlx_70() {
		return Dlrlx_70;
	}

	public void setDlrlx_70(String dlrlx_70) {
		Dlrlx_70 = dlrlx_70;
	}

	public String getDlrhm_70() {
		return Dlrhm_70;
	}

	public void setDlrhm_70(String dlrhm_70) {
		Dlrhm_70 = dlrhm_70;
	}

}
