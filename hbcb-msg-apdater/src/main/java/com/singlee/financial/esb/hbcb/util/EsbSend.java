package com.singlee.financial.esb.hbcb.util;

import com.singlee.capital.common.util.DateUtil;
import com.singlee.esb.utils.HttpHeadAttrConfig;

public class EsbSend {

	public static HttpHeadAttrConfig getHttpHeadAttrConfig(String server,String charset) {
		HttpHeadAttrConfig httpHeadAttrConfig = new HttpHeadAttrConfig();
		httpHeadAttrConfig.setCharset(charset);
		httpHeadAttrConfig.setUrl("http://www.cqrcb.com.cn");
		httpHeadAttrConfig.setConnTimeOut(3000);
		httpHeadAttrConfig.setContentType("text/html; charset=UTF-8");
		httpHeadAttrConfig.setContentLength(true);
		httpHeadAttrConfig.setContentMd5(true);
		String host = HttpConstants.IpConfig.HOST;
		String port = HttpConstants.IpConfig.PORT;
		String strURL = "http://" + host+":"+ port+"/"+server;
		httpHeadAttrConfig.setUrl(strURL);
		return httpHeadAttrConfig;
	}

	public static String createUUID(){
		String date = DateUtil.getCurrentCompactDateTimeAsString();
		int max=100000000, min=1;
		int random = (int) (Math.random()*(max-min)+min);
		return "174" + date.substring(0,8) + "00" + date.substring(8) + random + "00000" ;
	}
}
