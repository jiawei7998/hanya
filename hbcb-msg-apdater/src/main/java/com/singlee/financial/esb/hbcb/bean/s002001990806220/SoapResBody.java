package com.singlee.financial.esb.hbcb.bean.s002001990806220;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

/**
 * 定义报文体
 * 
 * 主要用于标记当前返回报文
 * 
 *
 */
public class SoapResBody extends EsbBodyPacket {

	private static final long serialVersionUID = 1L;

	private S002001990806220Res s002001990806220Res;

	public S002001990806220Res getS002001990806220Res() {
		return s002001990806220Res;
	}

	public void setS002001990806220Res(S002001990806220Res s002001990806220Res) {
		this.s002001990806220Res = s002001990806220Res;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("soapenv:Body");
		info.setSubPacketClassType(SoapResBody.class);
		// 给当前类起别名
		info.addAlias(SoapResBody.class, "s002001990806220Res", "ns:S002001990806220");
		// 处理属性值
		info.addAlias(EsbPacket.class, "packge_type", "xmlns:ns");
		info.addAlias(EsbPacket.class, "soapenvAttr", "xmlns:soapenv");
		// 处理子标签包含集合的内容
		// info.addAlias(ResponseBody.class, "detailList","Detail1",ResDetailList.class);
		return info;
	}

}
