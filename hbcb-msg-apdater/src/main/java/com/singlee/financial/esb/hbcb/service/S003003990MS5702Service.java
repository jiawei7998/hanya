package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.bean.s003003990MS5702.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s003003990MS5702.SoapResBody;
import com.singlee.financial.esb.hbcb.model.MS5702Bean;

public interface S003003990MS5702Service {

	/**
	 * 发送Esb报文
	 * @param mS5702Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(MS5702Bean mS5702Bean) throws Exception;
	
	/**
	 * 组装请求报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets003003990MS5702Req(MS5702Bean mS5702Bean) throws InstantiationException, IllegalAccessException;
	
	/**
	 * 初始化响应报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS003003990MS5702Res() throws InstantiationException, IllegalAccessException;
}
