package com.singlee.financial.esb.hbcb.bean.s100001001700105;

import java.io.Serializable;

/**
 * 主要应用与<adt:S100001001700105>标签
 * 
 *
 */
public class RequestBody implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * M 交易码 通知：700105 查询：700106 撤销：700107
	 */
	private String TxCode;
	/**
	 * M 文件名 需处理的文件全名
	 */
	private String FileName;
	/**
	 * M 应用系统标志 见附录
	 */
	private String SysFlag;
	/**
	 * M 机构码
	 */
	private String BrchNum;
	/**
	 * M 文件响应方式 I
	 */
	private String RetuFlag;
	/**
	 * M 柜面外围标志 0：柜面 1：外围系统
	 */
	private String Flag;
	/**
	 * M 通知处理类型 A：批开客户信息 B：批开帐号 C：批量转账 D: 批量转账冲正 O：无处理 E：批量多金融交易 F：批量渠道多金融交易
	 * G:批量圈存、解圈 H：银承兑付 I:销帐码查询 J:批量对账 K: 批量补录客户信息文件 主要针对通知功能
	 */
	private String NotiType;

	/**
	 * M 提交日期 YYYYMMDD
	 */
	private String SubmitDate;
	/**
	 * M 顺序号 当日内不能重复。右对齐,左补0
	 */
	private String BatSeq;

	public String getTxCode() {
		return TxCode;
	}

	public void setTxCode(String txCode) {
		TxCode = txCode;
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	public String getSysFlag() {
		return SysFlag;
	}

	public void setSysFlag(String sysFlag) {
		SysFlag = sysFlag;
	}

	public String getBrchNum() {
		return BrchNum;
	}

	public void setBrchNum(String brchNum) {
		BrchNum = brchNum;
	}

	public String getRetuFlag() {
		return RetuFlag;
	}

	public void setRetuFlag(String retuFlag) {
		RetuFlag = retuFlag;
	}

	public String getFlag() {
		return Flag;
	}

	public void setFlag(String flag) {
		Flag = flag;
	}

	public String getNotiType() {
		return NotiType;
	}

	public void setNotiType(String notiType) {
		NotiType = notiType;
	}

	public String getSubmitDate() {
		return SubmitDate;
	}

	public void setSubmitDate(String submitDate) {
		SubmitDate = submitDate;
	}

	public String getBatSeq() {
		return BatSeq;
	}

	public void setBatSeq(String batSeq) {
		BatSeq = batSeq;
	}

}
