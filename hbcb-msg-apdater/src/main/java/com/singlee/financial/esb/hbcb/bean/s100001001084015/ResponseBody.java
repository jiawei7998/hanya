package com.singlee.financial.esb.hbcb.bean.s100001001084015;

import java.util.List;


/**
 * 请求返回体
 * 
 * @author shenzl
 *
 */
public class ResponseBody {

	/**
	 * 销账码信息*7
	 */
	private List<ResDetailList> detailList;


	public List<ResDetailList> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ResDetailList> detailList) {
		this.detailList = detailList;
	}

}
