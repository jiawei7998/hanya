package com.singlee.financial.esb.hbcb.bean.s100001001060450;

import java.util.List;


/**
 * 请求返回体 主要应用与<ns:S100001001060450>标签
 * 
 *
 */
public class ResponseBody {

	/**
	 * 客户号 客户编号
	 */
	private String CustNum1;
	/**
	 * 客户类型 01 个人客户 02 企业客户 03 金融机构客户
	 */
	private String CustType;

	/**
	 * 用户号 开立客户的柜员号
	 */
	private String UsrNo;
	/**
	 * 称谓 回显栏位，1-先生 2-女士
	 */
	private String Salutation;
	/**
	 * 客户名称 客户名称
	 */
	private String CustName3;
	/**
	 * 地址栏1 地址栏1
	 */
	private String Address1201;
	/**
	 * 地址栏2 地址栏2
	 */
	private String Address2;
	/**
	 * 地址栏3 地址栏3
	 */
	private String Address3;
	/**
	 * 地址栏4 地址栏4
	 */
	private String Address4;
	/**
	 * 邮政编码 邮政编码
	 */
	private String ZipCode;
	/**
	 * 家庭电话 家庭电话
	 */
	private String HomePhone1;
	/**
	 * 传真号码 传真号码
	 */
	private String FAXNo;
	/**
	 * 居住/注册国家 国家代码简称CN-中国
	 */
	private String RsdncCountry;
	/**
	 * 办公电话 办公电话
	 */
	private String OfficePhone;
	/**
	 * 移动电话 移动电话
	 */
	private String Mobile;
	/**
	 * 国家代码 国家代码简称CN-中国
	 */
	private String Nationality;
	/**
	 * 支票簿持有量 回显，支票簿持有量
	 */
	private String ChkBookHoldNbr;
	/**
	 * 银行卡持有量 回显，银行卡持有量
	 */
	private String BnkCardHoldNbr;
	/**
	 * 使用授信数目 回显，使用授信数目
	 */
	private String UseNumber;
	/**
	 * 客户限额 回显，客户限额。高位置空，两位小数；最后一位为符号位，如果为正，则为空。例如“0.00”
	 */
	private String CustLimit;
	/**
	 * 合计 不用
	 */
	private String Total;
	/**
	 * 货币 默认CNY
	 */
	private String Currency;
	/**
	 * 用户名称 开立柜员号姓名
	 */
	private String UsrName;
	/**
	 * 综合对账单周期 回显
	 */
	private String CprsvStmtPrd;
	/**
	 * 证件号码 证件号码
	 */
	private String CerNo;
	/**
	 * 证件类型 见附表证件类型
	 */
	private String CerType;

	private List<ResDetailList> detailList;

	public String getCustNum1() {
		return CustNum1;
	}

	public void setCustNum1(String custNum1) {
		CustNum1 = custNum1;
	}

	public String getCustType() {
		return CustType;
	}

	public void setCustType(String custType) {
		CustType = custType;
	}

	public String getUsrNo() {
		return UsrNo;
	}

	public void setUsrNo(String usrNo) {
		UsrNo = usrNo;
	}

	public String getSalutation() {
		return Salutation;
	}

	public void setSalutation(String salutation) {
		Salutation = salutation;
	}

	public String getCustName3() {
		return CustName3;
	}

	public void setCustName3(String custName3) {
		CustName3 = custName3;
	}

	public String getAddress1201() {
		return Address1201;
	}

	public void setAddress1201(String address1201) {
		Address1201 = address1201;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress3() {
		return Address3;
	}

	public void setAddress3(String address3) {
		Address3 = address3;
	}

	public String getAddress4() {
		return Address4;
	}

	public void setAddress4(String address4) {
		Address4 = address4;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getHomePhone1() {
		return HomePhone1;
	}

	public void setHomePhone1(String homePhone1) {
		HomePhone1 = homePhone1;
	}

	public String getFAXNo() {
		return FAXNo;
	}

	public void setFAXNo(String fAXNo) {
		FAXNo = fAXNo;
	}

	public String getRsdncCountry() {
		return RsdncCountry;
	}

	public void setRsdncCountry(String rsdncCountry) {
		RsdncCountry = rsdncCountry;
	}

	public String getOfficePhone() {
		return OfficePhone;
	}

	public void setOfficePhone(String officePhone) {
		OfficePhone = officePhone;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getNationality() {
		return Nationality;
	}

	public void setNationality(String nationality) {
		Nationality = nationality;
	}

	public String getChkBookHoldNbr() {
		return ChkBookHoldNbr;
	}

	public void setChkBookHoldNbr(String chkBookHoldNbr) {
		ChkBookHoldNbr = chkBookHoldNbr;
	}

	public String getBnkCardHoldNbr() {
		return BnkCardHoldNbr;
	}

	public void setBnkCardHoldNbr(String bnkCardHoldNbr) {
		BnkCardHoldNbr = bnkCardHoldNbr;
	}

	public String getUseNumber() {
		return UseNumber;
	}

	public void setUseNumber(String useNumber) {
		UseNumber = useNumber;
	}

	public String getCustLimit() {
		return CustLimit;
	}

	public void setCustLimit(String custLimit) {
		CustLimit = custLimit;
	}

	public String getTotal() {
		return Total;
	}

	public void setTotal(String total) {
		Total = total;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getUsrName() {
		return UsrName;
	}

	public void setUsrName(String usrName) {
		UsrName = usrName;
	}

	public String getCprsvStmtPrd() {
		return CprsvStmtPrd;
	}

	public void setCprsvStmtPrd(String cprsvStmtPrd) {
		CprsvStmtPrd = cprsvStmtPrd;
	}

	public String getCerNo() {
		return CerNo;
	}

	public void setCerNo(String cerNo) {
		CerNo = cerNo;
	}

	public String getCerType() {
		return CerType;
	}

	public void setCerType(String cerType) {
		CerType = cerType;
	}

	public List<ResDetailList> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ResDetailList> detailList) {
		this.detailList = detailList;
	}

}
