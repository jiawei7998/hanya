package com.singlee.financial.esb.hbcb.bean.common;

/**
 * 兑换信息
 * 
 * @author shenzl
 *
 */
public class ExchangeHeader {

	/**
	 * 货币版本2 兑换外币的货币版本
	 */
	private String ExCurrency2;

	/**
	 * 汇率类型 01：现钞；02：现汇
	 */
	private String ExRatetype;
	/**
	 * 账号汇率
	 */
	private String ExAccountrate;
	/**
	 * 兑换汇率
	 */
	private String Exchangerate;
	/**
	 * 结售汇统计码
	 */
	private String ExJshcode;
	/**
	 * 购汇单位(个人)
	 */
	private String ExGhdw;
	/**
	 * 购汇原因
	 */
	private String ExGhreason;
	/**
	 * 提供批件名称
	 */
	private String ExPjname;
	/**
	 * 批准单位名称
	 */
	private String ExPzdw;
	/**
	 * 货币版本1 账户货币版本
	 */
	private String ExCureency1;

	public String getExCurrency2() {
		return ExCurrency2;
	}

	public void setExCurrency2(String exCurrency2) {
		ExCurrency2 = exCurrency2;
	}

	public String getExRatetype() {
		return ExRatetype;
	}

	public void setExRatetype(String exRatetype) {
		ExRatetype = exRatetype;
	}

	public String getExAccountrate() {
		return ExAccountrate;
	}

	public void setExAccountrate(String exAccountrate) {
		ExAccountrate = exAccountrate;
	}

	public String getExchangerate() {
		return Exchangerate;
	}

	public void setExchangerate(String exchangerate) {
		Exchangerate = exchangerate;
	}

	public String getExJshcode() {
		return ExJshcode;
	}

	public void setExJshcode(String exJshcode) {
		ExJshcode = exJshcode;
	}

	public String getExGhdw() {
		return ExGhdw;
	}

	public void setExGhdw(String exGhdw) {
		ExGhdw = exGhdw;
	}

	public String getExGhreason() {
		return ExGhreason;
	}

	public void setExGhreason(String exGhreason) {
		ExGhreason = exGhreason;
	}

	public String getExPjname() {
		return ExPjname;
	}

	public void setExPjname(String exPjname) {
		ExPjname = exPjname;
	}

	public String getExPzdw() {
		return ExPzdw;
	}

	public void setExPzdw(String exPzdw) {
		ExPzdw = exPzdw;
	}

	public String getExCureency1() {
		return ExCureency1;
	}

	public void setExCureency1(String exCureency1) {
		ExCureency1 = exCureency1;
	}

}
