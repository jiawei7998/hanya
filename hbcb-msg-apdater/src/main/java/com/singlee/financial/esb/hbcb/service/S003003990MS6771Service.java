package com.singlee.financial.esb.hbcb.service;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.esb.hbcb.bean.s003003990MS6771.SoapReqBody;
import com.singlee.financial.esb.hbcb.bean.s003003990MS6771.SoapResBody;
import com.singlee.financial.esb.hbcb.model.EsbOutBean;
import com.singlee.financial.esb.hbcb.model.MS6771Bean;

public interface S003003990MS6771Service {

	/**
	 * 发送Esb报文
	 * @param mS6771Bean
	 * @return
	 * @throws Exception
	 */
	EsbOutBean send(MS6771Bean mS6771Bean) throws Exception;
	
	/**
	 * 组装请求报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapReqBody> gets003003990MS6771Req(MS6771Bean mS6771Bean) throws InstantiationException, IllegalAccessException;
	
	/**
	 * 初始化响应报文
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	EsbPacket<SoapResBody> getS003003990MS6771Res() throws InstantiationException, IllegalAccessException;
}
