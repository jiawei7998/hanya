insert into tt_institution (INST_ID, INST_FULLNAME, INST_NAME, INST_LR_INST_CODE, P_INST_ID, INST_TYPE, INST_STATUS, ONLINE_DATE, LINK_MAN, TELEPHONE, MOBILE, ADDRESS, B_INST_ID)
values ('999999', '杭州新利软件股份有限公司', '总行', '', '-1', '0', '2', null, null, '8121197', null, '杭州西湖区三墩紫宣路158号西城博司铭座9幢16层', null);


insert into ta_user (USER_ID, USER_NAME, USER_PWD, IS_ACTIVE, USER_CREATETIME, USER_STOPTIME, USER_PWDCHGTIME, USER_MEMO, USER_EMAIL, USER_FIXEDPHONE, USER_CELLPHONE, INST_ID, USER_FLAG, USER_LOGONTYPE)
values ('admin', '超级管理员', BASEUTIL.md5('123456'), '1', '', '', '', '', 'admin', '', 'admin', '999999', '', '1');

delete from ta_role;
insert into ta_role values('1',1,'系统管理员','1','初始系统管理员',0);
delete from ta_user_role_map;
insert into ta_user_role_map(role_id,  user_id) values('1','admin');
delete from  ta_role_module_map m where m.role_id = '1';
insert into ta_role_module_map select '1', module_id, 1, module_url from ta_module;

