-- Create table
create table TT_INSTITUTION
(
  inst_id           VARCHAR2(32) not null,
  inst_fullname     VARCHAR2(255),
  inst_name         VARCHAR2(64),
  inst_lr_inst_code VARCHAR2(64),
  p_inst_id         VARCHAR2(32) not null,
  inst_type         VARCHAR2(32),
  inst_status       VARCHAR2(32),
  online_date       VARCHAR2(10) default '2050-12-31',
  link_man          VARCHAR2(32),
  telephone         VARCHAR2(32),
  mobile            VARCHAR2(32),
  address           VARCHAR2(255),
  b_inst_id         VARCHAR2(32)
);
-- Add comments to the table 
comment on table TT_INSTITUTION
  is '机构表';
-- Add comments to the columns 
comment on column TT_INSTITUTION.inst_id
  is '机构号';
comment on column TT_INSTITUTION.inst_fullname
  is '机构全称';
comment on column TT_INSTITUTION.inst_name
  is '机构简称';
comment on column TT_INSTITUTION.inst_lr_inst_code
  is '机构代码证';
comment on column TT_INSTITUTION.p_inst_id
  is '父机构号';
comment on column TT_INSTITUTION.inst_type
  is '机构类型，字典';
comment on column TT_INSTITUTION.inst_status
  is '机构状态，字典';
comment on column TT_INSTITUTION.online_date
  is '上线是日期';
comment on column TT_INSTITUTION.link_man
  is '联系人';
comment on column TT_INSTITUTION.telephone
  is '电话';
comment on column TT_INSTITUTION.mobile
  is '手机';
comment on column TT_INSTITUTION.address
  is '地址';
comment on column TT_INSTITUTION.b_inst_id
  is '记账机构号';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TT_INSTITUTION
  add constraint PK_TT_INSTITUTION primary key (INST_ID)
  using index;
