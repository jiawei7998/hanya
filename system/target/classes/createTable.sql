-- Create table
create table TA_DICT
(
  dict_id     VARCHAR2(32) not null,
  dict_name   VARCHAR2(32) not null,
  dict_module VARCHAR2(32),
  dict_desc   VARCHAR2(200)
);
-- Add comments to the table 
comment on table TA_DICT
  is '字典表TA_DICT';
-- Add comments to the columns 
comment on column TA_DICT.dict_id
  is '字典项关键字';
comment on column TA_DICT.dict_name
  is '字典项名称';
comment on column TA_DICT.dict_desc
  is '字典项描述';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_DICT
  add constraint PK_DICT primary key (DICT_ID)
  using index;
  
  
  -- Create table
create table TA_DICT_ITEM
(
  dict_id     VARCHAR2(32) not null,
  dict_key    VARCHAR2(100) not null,
  dict_sort   NUMBER,
  is_active   VARCHAR2(1),
  dict_folder VARCHAR2(200)
);
-- Add comments to the table 
comment on table TA_DICT_ITEM
  is '字典项明细表';
-- Add comments to the columns 
comment on column TA_DICT_ITEM.dict_id
  is '字典项关键字';
comment on column TA_DICT_ITEM.dict_key
  is '字典项字典值key';
comment on column TA_DICT_ITEM.dict_sort
  is '排序';
comment on column TA_DICT_ITEM.is_active
  is '是否启用 使用000001 字典，1=正常未被删除，0=删除';
comment on column TA_DICT_ITEM.dict_folder
  is '字典子目录：规则：[规则ID]';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_DICT_ITEM
  add constraint PK_DICT_ITEM primary key (DICT_ID, DICT_KEY)
  using index ;
  
  
  
  -- Create table
create table TA_FUNCTION
(
  module_id     VARCHAR2(32) not null,
  function_id   VARCHAR2(32) not null,
  function_name VARCHAR2(32),
  function_url  VARCHAR2(64),
  is_active     VARCHAR2(32)
);
-- Add comments to the table 
comment on table TA_FUNCTION
  is '功能点信息表';
-- Add comments to the columns 
comment on column TA_FUNCTION.module_id
  is 'MODULE_ID';
comment on column TA_FUNCTION.function_id
  is 'FUNCTION_ID';
comment on column TA_FUNCTION.function_name
  is 'FUNCTION_NAME';
comment on column TA_FUNCTION.function_url
  is 'FUNCTION_URL';
comment on column TA_FUNCTION.is_active
  is 'IS_ACTIVE';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_FUNCTION
  add constraint PK_TA_FUNCTION primary key (FUNCTION_ID, MODULE_ID)
  using index ;
  
  -- Create table
create table TA_MODULE
(
  module_id       VARCHAR2(16) not null,
  module_pid      VARCHAR2(16) not null,
  module_code     VARCHAR2(64),
  module_name     VARCHAR2(32),
  module_sortno   INTEGER,
  is_active       VARCHAR2(1),
  module_readonly VARCHAR2(1),
  module_type     VARCHAR2(1),
  module_shortcut VARCHAR2(20),
  module_icon     VARCHAR2(30),
  module_url      VARCHAR2(200),
  module_business VARCHAR2(2)
);
-- Add comments to the table 
comment on table TA_MODULE
  is '系统模块表';
-- Add comments to the columns 
comment on column TA_MODULE.module_id
  is '模块ID';
comment on column TA_MODULE.module_pid
  is '模块父节点ID';
comment on column TA_MODULE.module_code
  is '模块编码';
comment on column TA_MODULE.module_name
  is '模块名称';
comment on column TA_MODULE.module_sortno
  is '排序';
comment on column TA_MODULE.is_active
  is '是否启用标志';
comment on column TA_MODULE.module_readonly
  is '是否可写标志';
comment on column TA_MODULE.module_type
  is '模块类型';
comment on column TA_MODULE.module_shortcut
  is '快捷键';
comment on column TA_MODULE.module_icon
  is '图标';
comment on column TA_MODULE.module_url
  is '模块URL';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_MODULE
  add constraint PK_TA_MODULE primary key (MODULE_ID)
  using index;
  
  
  -- Create table
create table TA_MODULE_FUNCTION_MAP
(
  function_id VARCHAR2(16) not null,
  module_id   VARCHAR2(16) not null
);
-- Add comments to the table 
comment on table TA_MODULE_FUNCTION_MAP
  is '模块菜单与功能点映射表';
-- Add comments to the columns 
comment on column TA_MODULE_FUNCTION_MAP.function_id
  is '功能点ID';
comment on column TA_MODULE_FUNCTION_MAP.module_id
  is '菜单模块id';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_MODULE_FUNCTION_MAP
  add constraint TA_MODULE_FUNCTION_MAP primary key (FUNCTION_ID, MODULE_ID)
  using index;
  
  -- Create table
create table TA_ROLE
(
  role_id   VARCHAR2(16) not null,
  role_flag VARCHAR2(1) not null,
  role_name VARCHAR2(50) not null,
  is_active VARCHAR2(1) not null,
  role_memo VARCHAR2(200),
  inst_type VARCHAR2(32)
);
-- Add comments to the table 
comment on table TA_ROLE
  is '角色';
-- Add comments to the columns 
comment on column TA_ROLE.role_id
  is '角色ID';
comment on column TA_ROLE.role_flag
  is '角色类别';
comment on column TA_ROLE.role_name
  is '角色名称';
comment on column TA_ROLE.is_active
  is '启用标记';
comment on column TA_ROLE.role_memo
  is '备注';
comment on column TA_ROLE.inst_type
  is '机构类型:总行、分行、支行';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_ROLE
  add constraint PK_TA_ROLE primary key (ROLE_ID)
  using index;
  
  
  -- Create table
create table TA_ROLE_FUNCTION_MAP
(
  role_id     VARCHAR2(32) not null,
  function_id VARCHAR2(32) not null,
  module_id   VARCHAR2(32) not null,
  module_url  VARCHAR2(256)
);
-- Add comments to the table 
comment on table TA_ROLE_FUNCTION_MAP
  is '角色功能点关系映射表';
-- Add comments to the columns 
comment on column TA_ROLE_FUNCTION_MAP.role_id
  is 'ROLE_ID';
comment on column TA_ROLE_FUNCTION_MAP.function_id
  is 'FUNCTION_ID';
comment on column TA_ROLE_FUNCTION_MAP.module_id
  is 'MODULE_ID';
comment on column TA_ROLE_FUNCTION_MAP.module_url
  is '模块url地址，冗余存储';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_ROLE_FUNCTION_MAP
  add constraint PK_TA_ROLE_FUNCTION_MAP primary key (ROLE_ID, FUNCTION_ID, MODULE_ID)
  using index;
  
  -- Create table
create table TA_ROLE_MODULE_MAP
(
  role_id     VARCHAR2(16) not null,
  module_id   VARCHAR2(16) not null,
  access_flag INTEGER,
  module_url  VARCHAR2(256)
);
-- Add comments to the table 
comment on table TA_ROLE_MODULE_MAP
  is '角色模块菜单映射表';
-- Add comments to the columns 
comment on column TA_ROLE_MODULE_MAP.role_id
  is '角色ID';
comment on column TA_ROLE_MODULE_MAP.module_id
  is '模块ID';
comment on column TA_ROLE_MODULE_MAP.access_flag
  is '访问标志';
comment on column TA_ROLE_MODULE_MAP.module_url
  is '模块对应的URl 冗余存储';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_ROLE_MODULE_MAP
  add constraint PK_TA_ROLE_MODULE_MAP primary key (ROLE_ID, MODULE_ID)
  using index;
  
  
  -- Create table
create table TA_SYS_PARAM
(
  p_code      VARCHAR2(30) not null,
  p_value     VARCHAR2(200) not null,
  p_type      VARCHAR2(30),
  p_type_name VARCHAR2(50),
  p_memo      VARCHAR2(200),
  p_editabled VARCHAR2(1),
  p_prop1     VARCHAR2(200),
  p_prop2     VARCHAR2(200),
  p_name      VARCHAR2(200)
);
-- Add comments to the table 
comment on table TA_SYS_PARAM
  is '系统参数表';
-- Add comments to the columns 
comment on column TA_SYS_PARAM.p_code
  is '参数代码';
comment on column TA_SYS_PARAM.p_value
  is '参数值';
comment on column TA_SYS_PARAM.p_type
  is '参数类型';
comment on column TA_SYS_PARAM.p_type_name
  is '参数类型名称';
comment on column TA_SYS_PARAM.p_memo
  is '备注';
comment on column TA_SYS_PARAM.p_editabled
  is '是否可修改';
comment on column TA_SYS_PARAM.p_prop1
  is '扩展属性1';
comment on column TA_SYS_PARAM.p_prop2
  is '扩展属性2';
comment on column TA_SYS_PARAM.p_name
  is '参数名称';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_SYS_PARAM
  add constraint PK_TA_SYS_PARAM primary key (P_CODE, P_VALUE)
  using index;
  
  
  
  
  -- Create table
create table TA_USER
(
  user_id           VARCHAR2(32) not null,
  user_name         VARCHAR2(64),
  user_pwd          VARCHAR2(64),
  is_active         VARCHAR2(1),
  user_createtime   VARCHAR2(20),
  user_stoptime     VARCHAR2(20),
  user_pwdchgtime   VARCHAR2(20),
  user_memo         VARCHAR2(200),
  user_email        VARCHAR2(100),
  user_fixedphone   VARCHAR2(20),
  user_cellphone    VARCHAR2(20),
  inst_id           VARCHAR2(32),
  user_flag         VARCHAR2(1),
  user_logontype    VARCHAR2(1) default '0',
  is_online         VARCHAR2(1),
  latest_ip         VARCHAR2(64),
  latest_login_time VARCHAR2(20),
  latest_time       VARCHAR2(20),
  emp_id            VARCHAR2(32),
  user_pwdexptime   VARCHAR2(20),
  user_pwdexpdate   VARCHAR2(20)
);
-- Add comments to the table 
comment on table TA_USER
  is '柜员表';
-- Add comments to the columns 
comment on column TA_USER.user_id
  is '用户ID';
comment on column TA_USER.user_name
  is '用户姓名';
comment on column TA_USER.user_pwd
  is '密码';
comment on column TA_USER.is_active
  is '状态 1-启用 0-停用';
comment on column TA_USER.user_createtime
  is '用户创建时间';
comment on column TA_USER.user_stoptime
  is '用户停用时间';
comment on column TA_USER.user_pwdchgtime
  is '密码修改时间毫秒数';
comment on column TA_USER.user_memo
  is '备注';
comment on column TA_USER.user_email
  is '用户EMAIL';
comment on column TA_USER.user_fixedphone
  is '固定电话';
comment on column TA_USER.user_cellphone
  is '移动电话';
comment on column TA_USER.inst_id
  is '机构号';
comment on column TA_USER.user_flag
  is '标识';
comment on column TA_USER.user_logontype
  is '登陆方式';
comment on column TA_USER.is_online
  is '是否在线';
comment on column TA_USER.latest_ip
  is '最后一次ip地址';
comment on column TA_USER.latest_login_time
  is '最后一次登录时间';
comment on column TA_USER.latest_time
  is '最后访问时间';
comment on column TA_USER.emp_id
  is '员工ID';
comment on column TA_USER.user_pwdexptime
  is '密码到期时间毫秒数';
comment on column TA_USER.user_pwdexpdate
  is '密码有效期毫秒数';

  
  
  -- Create table
create table TA_USER_PARAM
(
  p_id          VARCHAR2(30) not null,
  p_type        VARCHAR2(30) not null,
  p_sub_type    VARCHAR2(30) not null,
  p_name        VARCHAR2(150) not null,
  p_value       VARCHAR2(100),
  p_sort        VARCHAR2(30),
  p_sub_name    VARCHAR2(30),
  p_code        VARCHAR2(30),
  p_parent_code VARCHAR2(30),
  p_expiry_day  VARCHAR2(30),
  p_start_alarm VARCHAR2(30)
);
-- Add comments to the table 
comment on table TA_USER_PARAM
  is '用户参数表';
-- Add comments to the columns 
comment on column TA_USER_PARAM.p_id
  is '参数Id';
comment on column TA_USER_PARAM.p_type
  is '参数大类';
comment on column TA_USER_PARAM.p_sub_type
  is '参数小类';
comment on column TA_USER_PARAM.p_name
  is '参数名称';
comment on column TA_USER_PARAM.p_value
  is '参数值';
comment on column TA_USER_PARAM.p_sort
  is '参数排序';
comment on column TA_USER_PARAM.p_sub_name
  is '参数小类描述';
comment on column TA_USER_PARAM.p_code
  is '参数子节点';
comment on column TA_USER_PARAM.p_parent_code
  is '参数父节点';
comment on column TA_USER_PARAM.p_expiry_day
  is '密码有效期';
comment on column TA_USER_PARAM.p_start_alarm
  is '距密码到期开始告警天数';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_USER_PARAM
  add constraint PK_TA_USER_PARAM primary key (P_ID)
  using index;
  
  
  -- Create table
create table TA_USER_ROLE_MAP
(
  role_id VARCHAR2(16) not null,
  user_id VARCHAR2(32) not null
);
-- Add comments to the table 
comment on table TA_USER_ROLE_MAP
  is '角色用户映射';
-- Add comments to the columns 
comment on column TA_USER_ROLE_MAP.role_id
  is '角色ID';
comment on column TA_USER_ROLE_MAP.user_id
  is '用户ID';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TA_USER_ROLE_MAP
  add constraint PK_TA_USER_ROLE_MAP primary key (USER_ID, ROLE_ID)
  using index;

  
  -- Create table
create table TA_CUSTOM_REPORT
(
  custid        VARCHAR2(30),
  reportclob    CLOB,
  reporttype    VARCHAR2(2),
  title         VARCHAR2(50),
  is_active     VARCHAR2(1),
  operator_id   VARCHAR2(30),
  operate_time  VARCHAR2(20),
  template_file BLOB,
  module_pid    VARCHAR2(16),
  module_name   VARCHAR2(32),
  module_id     VARCHAR2(16),
  module_url    VARCHAR2(200)
);
-- Add comments to the table 
comment on table TA_CUSTOM_REPORT
  is '自定义报表';
-- Add comments to the columns 
comment on column TA_CUSTOM_REPORT.custid
  is '报表编号';
comment on column TA_CUSTOM_REPORT.reportclob
  is '自定义报表json对象';
comment on column TA_CUSTOM_REPORT.reporttype
  is '报表类型';
comment on column TA_CUSTOM_REPORT.title
  is '报表标题';
comment on column TA_CUSTOM_REPORT.is_active
  is '启用状态';
comment on column TA_CUSTOM_REPORT.operator_id
  is '操作员编号 ';
comment on column TA_CUSTOM_REPORT.operate_time
  is '操作时间';
comment on column TA_CUSTOM_REPORT.template_file
  is '模板文件';
comment on column TA_CUSTOM_REPORT.module_pid
  is '菜单父节点ID.';
comment on column TA_CUSTOM_REPORT.module_name
  is '菜单名称.';
comment on column TA_CUSTOM_REPORT.module_id
  is '菜单节点ID.';
comment on column TA_CUSTOM_REPORT.module_url
  is '菜单URL.';
  
  
  -- Create table
create table TC_PRINT_TEMPLATE
(
  temp_code     VARCHAR2(10) not null,
  template_name VARCHAR2(100),
  bean_name     VARCHAR2(32),
  temp_content  BLOB,
  temp_desc     VARCHAR2(200)
);
-- Add comments to the columns 
comment on column TC_PRINT_TEMPLATE.temp_code
  is '模板编号';
comment on column TC_PRINT_TEMPLATE.template_name
  is '模板名称';
comment on column TC_PRINT_TEMPLATE.bean_name
  is '数据加载类名';
comment on column TC_PRINT_TEMPLATE.temp_content
  is '模板内容';
comment on column TC_PRINT_TEMPLATE.temp_desc
  is '描述';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TC_PRINT_TEMPLATE
  add constraint PK_TC_PRINT_TEMPLATE primary key (TEMP_CODE)
  using index;





-- Create table
create table TC_PRODUCT_TYPE
(
  prd_type_no     VARCHAR2(20) not null,
  sup_prd_type_no VARCHAR2(20),
  prd_type_name   VARCHAR2(100),
  prd_type_desc   NVARCHAR2(1024),
  icon_cls        VARCHAR2(100)
)
;
-- Add comments to the table 
comment on table TC_PRODUCT_TYPE
  is '产品种类';
-- Add comments to the columns 
comment on column TC_PRODUCT_TYPE.prd_type_no
  is '产品类别编号';
comment on column TC_PRODUCT_TYPE.sup_prd_type_no
  is '父产品类别编号';
comment on column TC_PRODUCT_TYPE.prd_type_name
  is '产品类别名称';
comment on column TC_PRODUCT_TYPE.prd_type_desc
  is '产品类别描述';
comment on column TC_PRODUCT_TYPE.icon_cls
  is '显示图标';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TC_PRODUCT_TYPE
  add constraint PK_TC_PRODUCT_TYPE primary key (PRD_TYPE_NO)
  using index;
-- Create table
create table TC_PRODUCT
(
  prd_no        VARCHAR2(32) not null,
  prd_inst      VARCHAR2(20),
  prd_name      VARCHAR2(100),
  prd_term      VARCHAR2(32),
  prd_type      VARCHAR2(32),
  prd_prop      VARCHAR2(32),
  is_template   VARCHAR2(32),
  use_flag      VARCHAR2(32),
  last_update   VARCHAR2(20),
  module_id     VARCHAR2(16),
  module_pid    VARCHAR2(16),
  module_name   VARCHAR2(32),
  module_sortno INTEGER,
  module_url    VARCHAR2(200)
)
;
-- Add comments to the table 
comment on table TC_PRODUCT
  is '产品';
-- Add comments to the columns 
comment on column TC_PRODUCT.prd_no
  is '产品编号 序列自动生成';
comment on column TC_PRODUCT.prd_inst
  is '所属机构';
comment on column TC_PRODUCT.prd_name
  is '产品名称';
comment on column TC_PRODUCT.prd_term
  is '产品期限 1-定期 2-活期';
comment on column TC_PRODUCT.prd_type
  is '产品种类';
comment on column TC_PRODUCT.prd_prop
  is '产品性质 1-资产 2-负债';
comment on column TC_PRODUCT.is_template
  is '是否模版 0-否 1-是';
comment on column TC_PRODUCT.use_flag
  is '启用标志 0-禁用 1-启用';
comment on column TC_PRODUCT.last_update
  is '更新时间';
comment on column TC_PRODUCT.module_id
  is '模块ID';
comment on column TC_PRODUCT.module_pid
  is '模块父节点ID';
comment on column TC_PRODUCT.module_name
  is '模块名称';
comment on column TC_PRODUCT.module_sortno
  is '排序';
comment on column TC_PRODUCT.module_url
  is '模块URL';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TC_PRODUCT
  add constraint PK_TC_PRODUCT primary key (PRD_NO)   using index;
-- Create table
create table TT_INSTITUTION
(
  inst_id           VARCHAR2(32) not null,
  inst_fullname     VARCHAR2(255),
  inst_name         VARCHAR2(64),
  inst_lr_inst_code VARCHAR2(64),
  p_inst_id         VARCHAR2(32) not null,
  inst_type         VARCHAR2(32),
  inst_status       VARCHAR2(32),
  online_date       VARCHAR2(10) default '2050-12-31',
  link_man          VARCHAR2(32),
  telephone         VARCHAR2(32),
  mobile            VARCHAR2(32),
  address           VARCHAR2(255),
  b_inst_id         VARCHAR2(32)
);
-- Add comments to the table 
comment on table TT_INSTITUTION
  is '机构表';
-- Add comments to the columns 
comment on column TT_INSTITUTION.inst_id
  is '机构号';
comment on column TT_INSTITUTION.inst_fullname
  is '机构全称';
comment on column TT_INSTITUTION.inst_name
  is '机构简称';
comment on column TT_INSTITUTION.inst_lr_inst_code
  is '机构代码证';
comment on column TT_INSTITUTION.p_inst_id
  is '父机构号';
comment on column TT_INSTITUTION.inst_type
  is '机构类型，字典';
comment on column TT_INSTITUTION.inst_status
  is '机构状态，字典';
comment on column TT_INSTITUTION.online_date
  is '上线是日期';
comment on column TT_INSTITUTION.link_man
  is '联系人';
comment on column TT_INSTITUTION.telephone
  is '电话';
comment on column TT_INSTITUTION.mobile
  is '手机';
comment on column TT_INSTITUTION.address
  is '地址';
comment on column TT_INSTITUTION.b_inst_id
  is '记账机构号';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TT_INSTITUTION
  add constraint PK_TT_INSTITUTION primary key (INST_ID)
  using index;


-- Create sequence 
create sequence SEQ_TA_LOG
minvalue 1
maxvalue 999999999999
start with 1
increment by 1
cache 20;

create sequence SEQ_TA_BASE
minvalue 1
maxvalue 999999999999
start with 100
increment by 1
cache 20;


-- Create table
create table TC_PRODUCT
(
  prd_no        VARCHAR2(32) not null,
  prd_inst      VARCHAR2(20),
  prd_name      VARCHAR2(100),
  prd_short     VARCHAR2(100),
  prd_type      VARCHAR2(32),
  prd_prop      VARCHAR2(32),
  is_template   VARCHAR2(32),
  use_flag      VARCHAR2(32),
  last_update   VARCHAR2(20),
  module_id     VARCHAR2(16),
  module_pid    VARCHAR2(16),
  module_name   VARCHAR2(32),
  module_sortno INTEGER,
  module_url    VARCHAR2(200),
  prd_term      VARCHAR2(32)
);
-- Add comments to the table 
comment on table TC_PRODUCT
  is '产品';
-- Add comments to the columns 
comment on column TC_PRODUCT.prd_no
  is '产品编号 序列自动生成';
comment on column TC_PRODUCT.prd_inst
  is '所属机构';
comment on column TC_PRODUCT.prd_name
  is '产品名称';
comment on column TC_PRODUCT.prd_short
  is '产品标签';
comment on column TC_PRODUCT.prd_type
  is '产品种类';
comment on column TC_PRODUCT.prd_prop
  is '产品性质 1-资产 2-负债';
comment on column TC_PRODUCT.is_template
  is '是否模版 0-否 1-是';
comment on column TC_PRODUCT.use_flag
  is '启用标志 0-禁用 1-启用';
comment on column TC_PRODUCT.last_update
  is '更新时间';
comment on column TC_PRODUCT.module_id
  is '模块ID';
comment on column TC_PRODUCT.module_pid
  is '模块父节点ID';
comment on column TC_PRODUCT.module_name
  is '模块名称';
comment on column TC_PRODUCT.module_sortno
  is '排序';
comment on column TC_PRODUCT.module_url
  is '模块URL';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TC_PRODUCT
  add constraint PK_TC_PRODUCT primary key (PRD_NO)
  using index 
  ;

-- Create table
create table TC_PRODUCT_TYPE
(
  prd_type_no     VARCHAR2(20) not null,
  sup_prd_type_no VARCHAR2(20),
  prd_type_name   VARCHAR2(100),
  prd_type_desc   NVARCHAR2(1024),
  icon_cls        VARCHAR2(100)
);
-- Add comments to the table 
comment on table TC_PRODUCT_TYPE
  is '产品种类';
-- Add comments to the columns 
comment on column TC_PRODUCT_TYPE.prd_type_no
  is '产品类别编号';
comment on column TC_PRODUCT_TYPE.sup_prd_type_no
  is '父产品类别编号';
comment on column TC_PRODUCT_TYPE.prd_type_name
  is '产品类别名称';
comment on column TC_PRODUCT_TYPE.prd_type_desc
  is '产品类别描述';
comment on column TC_PRODUCT_TYPE.icon_cls
  is '显示图标';
-- Create/Recreate primary, unique and foreign key constraints 
alter table TC_PRODUCT_TYPE
  add constraint PK_TC_PRODUCT_TYPE primary key (PRD_TYPE_NO)
  using index ;

