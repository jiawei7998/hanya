CREATE OR REPLACE PACKAGE BASEUTIL IS



  --��������MD5����
  FUNCTION MD5(PASSWD IN VARCHAR2) RETURN VARCHAR2;


END BASEUTIL;

/
CREATE OR REPLACE PACKAGE BODY BASEUTIL IS




  FUNCTION MD5(PASSWD IN VARCHAR2) RETURN VARCHAR2 IS
    RETVAL VARCHAR2(32);
  BEGIN
    RETVAL := UTL_RAW.CAST_TO_RAW(DBMS_OBFUSCATION_TOOLKIT.MD5(INPUT_STRING => PASSWD));
    RETURN RETVAL;
  END;

 
END BASEUTIL;
/