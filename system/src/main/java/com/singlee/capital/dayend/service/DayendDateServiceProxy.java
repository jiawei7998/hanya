package com.singlee.capital.dayend.service;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;

/**
 * 日终 服务
 * 
 * @author Lyon Chen
 * 
 */
public class DayendDateServiceProxy  {
	
	
	private static class SingletonHolder {
		private static final DayendDateServiceProxy INSTANCE = new DayendDateServiceProxy();
	}

	public static final DayendDateServiceProxy getInstance() {
		return SingletonHolder.INSTANCE;
	}

	/**
	 * 接口实现
	 */
	private DayendDateService dayendDateService;

	/**
	 * 从spring中获得实际的bean对象
	 */
	private DayendDateServiceProxy() {
		try{
			this.dayendDateService = SpringContextHolder.getBean(DayendDateService.class);
		}catch(Exception e){
			JY.warn(e.getMessage());
		}
	}
	public String getDayendDate() {
		return dayendDateService==null?"N/A":dayendDateService.getDayendDate();	
	}

	public String getDayendDateStatus() {
		return dayendDateService==null?"N/A":dayendDateService.getDayendDateStatus();
	}


	public String getNextDayendDate() {
		return dayendDateService==null?"N/A":dayendDateService.getNextDayendDate();
	}

	public String getPrevDayendDate() {
		return dayendDateService==null?"N/A":dayendDateService.getPrevDayendDate();
	}

	
	/**  
	 * constants 的部分迁移过来。同一个入口处理
	 */

	/**
	 * 当前业务日 + 系统时间 HH:MM:SS
	 * @return
	 */
	public String getSettlementDateTime() {
		return dayendDateService==null?"N/A":dayendDateService.getSettlementDateTime();
	}

	/**
	 * 获得当前业务结算日 从结算日期表中获取
	 * 
	 * @return
	 */
	public String getSettlementDate() {
		return dayendDateService==null?"N/A":dayendDateService.getSettlementDate();
	}

	public boolean isSettlementDate(String date){
		return dayendDateService==null?false:dayendDateService.isSettlementDate(date);
		
	}

}
