package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.model.TtDayendDate;
import com.singlee.capital.system.dict.ExternalDictConstants;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;


/**
 * 日终处理的第一步
 * 此处是filter，确保后续步骤有异常的情况下，会postprocess调用！！
 * 其他步骤可以继承 CommonDayendFlowHandler
 *
 * @author x230i
 *
 */
@Component("initDayendFlowHandler")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class InitDayendFlowHandler extends CommonDayendFlowHandler{

	@Override
	public void execute(SlSession session) {
		// ----------------------------------乐观锁开始占位------------------------------------------
		TtDayendDate tdd = dayendDateService.getTtDayendDate();
		// 是否是轧账完成准备状态/或者错误状态。
		JY.require(ExternalDictConstants.DayendDateStatus_Checked.equals(tdd.getStatus())
				|| ExternalDictConstants.DayendDateStatus_Error.equals(tdd.getStatus()), 
				MsgUtils.getMessage("error.dayend.0004", tdd.getStatus()));
		// ----------------------------------乐观锁开始检验------------------------------------------
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("newStatus", ExternalDictConstants.DayendDateStatus_Executing);
		map.put("curDate", tdd.getCurDate());
		map.put("status", tdd.getStatus());
		map.put("version", tdd.getVersion());
		int lock = dayendDateService.updateTtDayendDate(map);
		// ----------------------------------乐观锁结束------------------------------------------
		JY.require(lock == 1 ,"----------------------------有人代劳了日终任务的工作------------------------" );
	}

	@Override
	public boolean postprocess(SlSession session, Exception exception) {
		// 没有异常 就准备翻日终。
		String newStatus = exception==null?ExternalDictConstants.DayendDateStatus_Normal:ExternalDictConstants.DayendDateStatus_Error;
		//String newDate = exception==null? dayendDateService.getNextDayendDate() : null;
		String newDate=null;
		if(exception==null){
		Map<String, Object> mapJJR = new HashMap<String, Object>();
		newDate=dayendDateService.getNextDayendDate();
		mapJJR.put("calDate", newDate);
		while (dayendDateService.queryJJR(mapJJR)>0){
			newDate=DateUtil.dateAdjust(newDate, 1);
			
			mapJJR.put("calDate", newDate);
			
		};
		
		}
		// ----------------------------------乐观锁开始占位------------------------------------------
		TtDayendDate tdd = dayendDateService.getTtDayendDate();
		// 是否是轧账完成准备状态/或者错误状态。
		JY.require(ExternalDictConstants.DayendDateStatus_Executing.equals(tdd.getStatus()) , 
				MsgUtils.getMessage("error.dayend.0005", tdd.getStatus()));
		// ----------------------------------乐观锁开始检验------------------------------------------
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("newStatus", newStatus);
		map.put("newDate", newDate);
		map.put("curDate", tdd.getCurDate());
		map.put("status", tdd.getStatus());
		map.put("version", tdd.getVersion());
		int lock = dayendDateService.updateTtDayendDate(map);
		// ----------------------------------乐观锁结束------------------------------------------
		JY.require(lock == 1 ,"----------------------------日终认为发生不可思议的错误------------------------" );
		if(exception==null){
			// 如果日中没有异常，就更新常量
			// 常量重新定义 
			// dayendDateService.setSettlementDate(dayendDateService.getDayendDate());
			
		}
		return false;
	}

}
