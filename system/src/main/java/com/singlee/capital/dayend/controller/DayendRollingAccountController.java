package com.singlee.capital.dayend.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.model.TtDayendRollingAccountLog;
import com.singlee.capital.dayend.service.DayendRollingAccountService;
import com.singlee.capital.system.controller.CommonController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 轧帐控制层
 * @author kevin_gm
 *
 */
@Controller
@RequestMapping(value = "/DayendRollingAccountController")
public class DayendRollingAccountController extends CommonController{
	
	@Autowired
	private DayendRollingAccountService dayendRollingAccountService;
	
	/**
	 * 根据轧帐日期查询所有机构轧帐状态
	 * @param rolling_date 轧帐日期
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchInstitutionRollingAccountStatus")
	public RetMsg<List<TtDayendRollingAccountLog>> searchInstitutionRollingAccountStatus(@RequestBody Map<String,Object> params){
		String rollingDate = ParameterUtil.getString(params,"bizDate","");
		//判断是否是总行
		String instId = ParameterUtil.getString(params,"instId","");
		//if(StringUtil.IsEqual(SystemProperties.headBankInstId, instId)){
		//	instId=null;
		//}
		List<TtDayendRollingAccountLog> list = dayendRollingAccountService.searchInstitutionRollingAccountStatus(rollingDate,instId,null);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 轧帐执行
	 * @param rolling_date 轧帐日期
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/checkAllInstitutionRollingAccountStatus")
	public RetMsg<Serializable> checkAllInstitutionRollingAccountStatus(@RequestBody Map<String,Object> params){
		String rollingDate = ParameterUtil.getString(params,"bizDate","");
		dayendRollingAccountService.checkAllInstitutionRollingAccountStatus(rollingDate);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 轧帐执行前，需要先把【未处理 || 待发起 + 未作回款处理】的回款指令，筛选出来（系统自动做指令延期）
	 * @param rolling_date 轧帐日期
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/filtrateAllInstitutionRollingAccountStatus")
	public RetMsg<Map<String,Object>> filtrateAllInstitutionRollingAccountStatus(@RequestBody Map<String,Object> params){
		String rollingDate = ParameterUtil.getString(params,"bizDate","");
		Map<String,Object> map = dayendRollingAccountService.filtrateAllInstitutionRollingAccountStatus(rollingDate);
		return RetMsgHelper.ok(map);
	}
	
}
