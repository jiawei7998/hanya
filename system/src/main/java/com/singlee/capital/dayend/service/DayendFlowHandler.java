package com.singlee.capital.dayend.service;	

import org.apache.commons.chain.Filter;



/**
 * 日终流程 处理 接口 
 * @author cz
 *
 */
public interface DayendFlowHandler extends Filter {

	/**
	 * 存放会话信息
	 */
	public static final String slsession="slsession";
	
	
	/**
	 * 此变量为true则：
	 * 正在执行的步骤正常运行！后续步骤将停止。
	 */
	public static final String stop="stop-immedieatly";
	
//	
//	public String getFlowId();
//	
//	
//	public String getFlowName();
	

}
