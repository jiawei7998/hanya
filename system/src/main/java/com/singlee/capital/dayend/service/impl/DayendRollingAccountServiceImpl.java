package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.mapper.TtDayendRollingAccountLogMapper;
import com.singlee.capital.dayend.model.TtDayendDate;
import com.singlee.capital.dayend.model.TtDayendRollingAccountLog;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.DayendRollingAccountService;
import com.singlee.capital.system.dict.ExternalDictConstants;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 轧账service
 * @author kevin_gm
 *
 */
public class DayendRollingAccountServiceImpl implements DayendRollingAccountService {

	/** 轧账dao **/
	@Autowired
	private TtDayendRollingAccountLogMapper ttDayendRollingAccountLogMapper;

	@Autowired
	private DayendDateService dayendDateService;
	
	
//	@Autowired
//	private RollingAccountHandle rollingAccountHandle;
	/**
	 * 根据轧账日期查询所有机构轧账状态
	 * @param rolling_date
	 * @return
	 */
	@Override
	public List<TtDayendRollingAccountLog> searchInstitutionRollingAccountStatus(String rollingDate,String instId, String negativeStatus){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("rollingDate", rollingDate);
		map.put("instId", instId);
		map.put("status", negativeStatus);
		return ttDayendRollingAccountLogMapper.searchInstitutionRollingAccountStatus(map);
	}

	@Override
	public String rollingAccount(String rolling_date, String instId) {
		return null;
	}

	@Override
	public void checkAllInstitutionRollingAccountStatus(String rolling_date) {
		// ----------------------------------乐观锁开始占位------------------------------------------
		TtDayendDate tdd = dayendDateService.getTtDayendDate();
		// 是否是正常状态
		JY.require(ExternalDictConstants.DayendDateStatus_Normal.equals(tdd.getStatus()), 
				MsgUtils.getMessage("error.dayend.0006", tdd.getStatus()));
		// ----------------------------------乐观锁开始检验------------------------------------------
		//确认当天所有机构均轧帐完毕后，更改业务日状态为轧帐完毕
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("curDate", tdd.getCurDate());
		map.put("status", tdd.getStatus());
		map.put("version", tdd.getVersion());
		int lock = dayendDateService.updateTtDayendDate(map);
		// ----------------------------------乐观锁结束------------------------------------------
		JY.require(lock == 1 ,"----------------------------有人代劳了轧账任务的工作------------------------" );
		
		// 
		boolean dayendLock = rollingAccountHandle(tdd.getCurDate());
		
		// 其他模块 轧帐检查 
		if(dayendLock && noFailToday(tdd.getCurDate())){
			TtDayendDate tdd2 = dayendDateService.getTtDayendDate();			
			map.put("newStatus", ExternalDictConstants.DayendDateStatus_Checked);
			map.put("curDate", tdd2.getCurDate());
			map.put("status", tdd2.getStatus());
			map.put("version", tdd2.getVersion());
			dayendDateService.updateTtDayendDate(map);
		}
		
	}
	/**
	 * 应该被继承者 重写 
	 * @param curDate
	 * @return
	 */
	protected boolean rollingAccountHandle(String curDate){
		return true;
	}
	
	/**
	 * 应该被继承者 重写 
	 * @param curDate
	 * @param map
	 */
	protected void rollingAccountCheck(String curDate, Map<String, Object> map){
		return ;
	}

	/**
	 * 轧帐执行前，需要先把【未处理 || 待发起 + 未作回款处理】的回款指令筛选出来（系统自动做指令延期）
	 * @param rolling_date
	 */
	@Override
	public Map<String,Object> filtrateAllInstitutionRollingAccountStatus(String rolling_date) {
		
		// ----------------------------------乐观锁开始占位------------------------------------------
		TtDayendDate tdd = dayendDateService.getTtDayendDate();
		// 是否是正常状态
		JY.require(ExternalDictConstants.DayendDateStatus_Normal.equals(tdd.getStatus()), 
				MsgUtils.getMessage("error.dayend.0006", tdd.getStatus()));
		// ----------------------------------乐观锁开始检验------------------------------------------
		//确认当天所有机构均轧帐完毕后，更改业务日状态为轧帐完毕
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("curDate", tdd.getCurDate());
		map.put("status", tdd.getStatus());
		map.put("version", tdd.getVersion());
		int lock = dayendDateService.updateTtDayendDate(map);
		// ----------------------------------乐观锁结束------------------------------------------
		JY.require(lock == 1 ,"----------------------------有人代劳了轧账任务的工作------------------------" );
		
		rollingAccountCheck(tdd.getCurDate(), map);
		return map;

	}

	/**
	 * 
	 * @param curDate
	 * @return
	 */
	protected final boolean noFailToday(String curDate) {
		List<TtDayendRollingAccountLog> list = this.searchInstitutionRollingAccountStatus(curDate,null,ExternalDictConstants.RollingStatus_Success);
		if(list==null || list.size() == 0){
			return true;
		}else{
			return false;
		}
	}

	protected final TtDayendRollingAccountLog getInstitutionRollingAccountStatus(String curDate, String inst) {
		List<TtDayendRollingAccountLog> list = this.searchInstitutionRollingAccountStatus(curDate,inst,null);
		if(list==null || list.size() == 0){
			return null;
		}else{
			return list.get(0);
		}
	}
}
