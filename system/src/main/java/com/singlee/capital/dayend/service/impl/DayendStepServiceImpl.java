package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.dayend.mapper.TtDayendStepLogMapper;
import com.singlee.capital.dayend.mapper.TtDayendStepMapper;
import com.singlee.capital.dayend.model.TtDayendStep;
import com.singlee.capital.dayend.model.TtDayendStepLog;
import com.singlee.capital.dayend.service.DayendStepService;
import com.singlee.capital.system.dict.ExternalDictConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 柜员 服务
 * 
 * @author Lyon Chen
 * 
 */
@Service("dayendStepService")
public class DayendStepServiceImpl implements DayendStepService {


	@Autowired
	private TtDayendStepMapper dsm ;
	@Autowired
	private TtDayendStepLogMapper dslm ;
	@Override
	public List<TtDayendStep> listDayendStepIdWithSort() {
		List<TtDayendStep> list = dsm.selectAll();
		// 排序 
		Collections.sort(list, new Comparator<TtDayendStep>(){
			@Override
			public int compare(TtDayendStep arg0, TtDayendStep arg1) {
				if(arg0.getStepSort() > arg1.getStepSort() ){
					return 1;
				}else if (arg0.getStepSort() == arg1.getStepSort() ){
					return 0;
				}else {
					return -1;
				}
			}
		});
		return list;
	}

	@Override
	public List<TtDayendStepLog> currentDayendStepLog(String bussnessDate) {
		TtDayendStepLog arg0 = new TtDayendStepLog();
		arg0.setBizDate(bussnessDate);
		return dslm.select(arg0);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class,propagation=Propagation.REQUIRES_NEW) 
	public void addDayendStepLog(TtDayendStepLog tdsl) {
		dslm.insert(tdsl);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class,propagation=Propagation.REQUIRES_NEW) 
	public void modifyDayendStepLog(TtDayendStepLog tdsl) {
		dslm.updateByPrimaryKey(tdsl);
	}

	@Override
	public List<TtDayendStepLog> searchDayEndStepLog(String biz_date) {
		return dsm.searchDayendStepLog(biz_date);
	}

	@Override
	public void stopDayendStep(String stepId) {
		dsm.updateDayendStepStopFlag(stepId, ExternalDictConstants.YesNo_YES);
	}

	@Override
	public void resumeDayendStep(String stepId) {
		dsm.updateDayendStepStopFlag(stepId, ExternalDictConstants.YesNo_NO);
	}

}
