package com.singlee.capital.dayend.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 日终流程步骤(基础信息对象)
 * @author CZ
 *
 */
@Entity
@Table(name = "TT_DAYEND_STEP")
public class TtDayendStep implements Serializable {

	private static final long serialVersionUID = -2169409304235839704L;
	
	@Id
	private String stepId;//步骤编号
	private String stepName;//步骤名称
	private String executeName;//执行方法
	private int stepSort;//步骤排序
	private String isSkip;//是否跳过
	private String isLoop;//是否重跑
	private int version;

	/**
	 * 是否停止标志cz add it for support 步骤临时停止！
	 */
	private String isStopFlag; // 是否停止标志; 缺省为false

	@Transient
	private TtDayendStepLog ttDayendStepLog; // 当天的步骤执行日志，可能为null
	
	public int getVersion() {
		return version;
	}
	public TtDayendStepLog getTtDayendStepLog() {
		return ttDayendStepLog;
	}
	public void setTtDayendStepLog(TtDayendStepLog ttDayendStepLog) {
		this.ttDayendStepLog = ttDayendStepLog;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public String getExecuteName() {
		return executeName;
	}
	public void setExecuteName(String executeName) {
		this.executeName = executeName;
	}
	public int getStepSort() {
		return stepSort;
	}
	public void setStepSort(int stepSort) {
		this.stepSort = stepSort;
	}
	public String getIsSkip() {
		return isSkip;
	}
	public void setIsSkip(String isSkip) {
		this.isSkip = isSkip;
	}
	public String getIsLoop() {
		return isLoop;
	}
	public void setIsLoop(String isLoop) {
		this.isLoop = isLoop;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TtDayendStep [stepId=");
		builder.append(stepId);
		builder.append(", stepName=");
		builder.append(stepName);
		builder.append(", executeName=");
		builder.append(executeName);
		builder.append(", stepSort=");
		builder.append(stepSort);
		builder.append(", isSkip=");
		builder.append(isSkip);
		builder.append(", isLoop=");
		builder.append(isLoop);
		builder.append(", version=");
		builder.append(version);
		builder.append("]");
		return builder.toString();
	}
	public String getIsStopFlag() {
		return isStopFlag;
	}
	public void setIsStopFlag(String isStopFlag) {
		this.isStopFlag = isStopFlag;
	}
	

}
