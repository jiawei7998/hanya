package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.model.TtDayendStep;
import com.singlee.capital.dayend.model.TtDayendStepLog;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.DayendFlowHandler;
import com.singlee.capital.dayend.service.DayendStepService;
import com.singlee.capital.system.dict.ExternalDictConstants;
import org.apache.commons.chain.Context;
import org.apache.commons.chain.impl.ChainBase;
import org.apache.commons.chain.impl.ContextBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 日终 任务 说明： 
 * 1.该线程属于单线程，不允许多个线程同时允许 
 * 2.该线程启动时，系统相关操作将被禁止，并返回当前首页面
 * 
 * @author gm
 * 
 */
@Service
public class DayendTask implements Callable<String>{
	/** 日终步骤日志服务 */
	@Autowired
	private DayendStepService dayendStepService;
	/** 日终步骤的初始和终结步骤 处理  */
	@Autowired
	private DayendFlowHandler initDayendFlowHandler;
	@Autowired
	private DayendDateService dayendDateService;
	
	/** 日终 上下文空间 */
	private final Context ctx = new ContextBase();
	
	/** 使用 volatile标注，注明此flag的volatile关键字保证了多线程访问操作的可见性*/
	private volatile boolean flag = true;
	
//	public DayendTask() {
//		this.dayendStepService = SpringContextHolder.getBean("dayendStepService");
//		this.dayendDateService = SpringContextHolder.getBean("dayendDateService");
//		this.initDayendFlowHandler = SpringContextHolder.getBean("initDayendFlowHandler");
//	}

	/**
	 * @param virtualSession 日终的虚拟session 
	 */
	@SuppressWarnings("unchecked")
	protected void init(SlSession s, String bizDate){
		// 检查 多线程 是否 安全
		JY.require(flag(),bizDate + " 日终任务线程 已经在运行中，请耐心等待并及时查询步骤状态!");
		this.flag = false;
		// flag 为false 标记说明其他线程 init 时将不允许！！！
		this.ctx.put(DayendFlowHandler.slsession, s);
		this.ctx.put(DayendFlowHandler.stop, Boolean.FALSE);		
	}
	/**
	 * 人工介入中止日终事务 
	 */
	@SuppressWarnings("unchecked")
	protected void stop(){
		this.ctx.put(DayendFlowHandler.stop, Boolean.TRUE);
	}
	/**
	 * 返回 true 说明已经执行完毕，返回 false 说明还在执行中
	 * @return
	 */
	protected boolean flag(){
		return flag;
	}
	@Override
	public String call() {
		try {
			// 取得当前日终表中的所有步骤记录
			List<TtDayendStep> list = dayendStepService
					.listDayendStepIdWithSort();
			
			// 取得当前日终步骤日志表中的所有当天的记录
			List<TtDayendStepLog> log = dayendStepService
					.currentDayendStepLog(dayendDateService.getSettlementDate());
			
			List<DayendFlowHandler> handlerList = new LinkedList<DayendFlowHandler>();
			for (TtDayendStep tds : list) {
				CommonDayendFlowHandler arg0 = SpringContextHolder.getBean(tds.getExecuteName());
				// 检验是否都有对应的 spring bean存在
				JY.require(arg0 != null,MsgUtils.getMessage("error.dayend.0003", tds.getStepName()));
				// 检查此步骤是否有正确的日终记录，如果有，则说明已经成功执行，跳过！！
				if (checkFlow(tds, log)) {
					//
					JY.info("步骤(%s)---%s---状态为已经执行，或者该步骤允许失败跳过。跳过！", tds.getStepId(),
							tds.getExecuteName());
				} else {
					// 捆绑 数据库对象，此对象中已经有可能有 步骤日志。第一次执行时候是 null，需要新建！
					arg0.setTtDayendStep(tds);
					
					/**
					 * 如果步骤已经是停止状态的处理！！！
					 * 
					 */
					arg0.setStopFlag(ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tds.getIsStopFlag()));
					if(arg0.getStopFlag()){
						JY.info("步骤(%s)---%s---状态为停止状态，页面需要设置后，才可以继续此步骤后的跑批！", tds.getStepId(),
								tds.getExecuteName());
					}
					
					// 
					handlerList.add(arg0);
				}
			}
			ChainBase cb  = new ChainBase ();
			// 链条的头部增加一个 日终步骤的初始和终结步骤 处理  (filter)
			cb.addCommand(initDayendFlowHandler);
			// 加入到 chain 中，
			for(DayendFlowHandler dfh : handlerList){
				cb.addCommand(dfh);
			}
			// 执行 chain 链条  每个链条 command
			try {
				cb.execute(ctx);
			} catch (Exception e) {
				// chain 中已经处理了异常，这里记录一下即可。
				JY.error(e);
				return e.getMessage();
			}
			return "日终任务线程完成!";
		} catch (Exception e){
			JY.error(e); //记录日志 
			return e.getMessage();
		} finally {
			// 运行结束 重置标志位 ，下次线程进入
			flag = true;
		}
	}
	

	private boolean checkFlow(TtDayendStep tds, List<TtDayendStepLog> log) {
		if(log !=null){
			for (TtDayendStepLog tdsl : log) {
				if (tdsl.getStepId().equals(tds.getStepId())) {
					// 此步骤以及ok 成功或者警告，则
					if (ExternalDictConstants.DayendFlowStatus_Success.equals(tdsl.getStatus()) ||
						ExternalDictConstants.DayendFlowStatus_Warning.equals(tdsl.getStatus())) {
						return true;
						// 失败的情况下，判断是否该步骤是允许跳过的。
					}else if (ExternalDictConstants.DayendFlowStatus_Error.equals(tdsl
							.getStatus()) && ExternalDictConstants.YesNo_YES.equals(tds.getIsSkip())) {
							return true;
					}else{
						tds.setTtDayendStepLog(tdsl);
						return false;
					}
				}
			}
			tds.setTtDayendStepLog(null);
		}
		return false;
	}
	// public void run2() {
	// //1.读取日终执行步骤。
	// HashMap<String, Object> map = new HashMap<String, Object>();
	// map.put("biz_date", settlementDate);
	// List<DayEndStepLogVo> list = dayendStepDao.searchDayEndStepLog(map);
	// String stepId = "",bizDate = "",status = "";
	//
	// //2.增加内存及数据库日终锁，在拦截器处增加内存日终锁的判断逻辑。
	// DayEndCache.isExecuting = true;
	// dayendStepService.lockedDayendForDataBase();
	//
	// //3.内存中记录当前线层信息
	// DayEndCache.threadToInterrupt = Thread.currentThread();
	//
	// try {
	// log.info(String.format("日终程序启动.时间:%s",DateUtil.getCurrentDateTimeAsString()));
	// boolean isStartWhenAssign = false;//当从指定步骤开始重跑日终是需要设定开关，确认是否开始重跑
	// for (DayEndStepLogVo dayEndStepLogVo : list) {
	// Thread.sleep(0);//用于停止线程方法
	//
	// stepId = dayEndStepLogVo.getStep_id();
	// bizDate = dayEndStepLogVo.getBiz_date();
	// status = dayEndStepLogVo.getStatus();
	//
	// //判断:方法名称不允许为空
	// if(StringUtil.checkEmptyNull(dayEndStepLogVo.getExecute_name())){
	// dayendStepService.executeStatus(stepId,bizDate,"执行方法为空.</br>",DictConstants.DayEndStatus.Skip.value());
	// continue;
	// }
	//
	// //判断：是否只从指定方法开始重新执行日终
	// if(StringUtil.checkEmptyNull(assignStepId)){
	// if(DictConstants.DayEndStatus.Seccuss.compare(status)){
	// continue;//成功执行过的日终步骤不再重复执行
	// }
	// executeDayEndListion(dayEndStepLogVo);//所有程序都需要执行日终操作
	// }else{
	// /** 取消指定步骤日终按钮 ***/
	// //判断：当isStartWhenAssign = false && step_id != assignStepId && status !=
	// success时,说明在所指定的步骤前存在日终步骤未正确执行
	// JY.require(isStartWhenAssign == true || stepId.equals(assignStepId) ||
	// status.equals(DictConstants.DayEndStatus.Seccuss.value()),
	// "所指定的步骤前存在日终步骤未正确执行");
	// if(stepId.equals(assignStepId) || isStartWhenAssign == true){
	// isStartWhenAssign = true;
	// executeDayEndListion(dayEndStepLogVo);
	// }
	// }
	// }
	// log.info(String.format("日终程序运行完成.时间:%s",DateUtil.getCurrentDateTimeAsString()));
	// }catch (InterruptedException e) {
	// log.error(String.format("日终程序强制结束.时间:%s",DateUtil.getCurrentDateTimeAsString()),e);
	// dayendStepService.executeStatus(stepId,bizDate,e.toString(),DictConstants.DayEndStatus.Stop.value());
	// }catch (Exception e) {
	// log.error(String.format("日终程序异常结束.时间:%s",DateUtil.getCurrentDateTimeAsString()),e);
	// dayendStepService.executeStatus(stepId,bizDate,e.toString(),DictConstants.DayEndStatus.Error.value());
	// }finally{
	// dayendStepService.unLockedDayendForDataBase();
	// //4.进程结束(含异常)时清空cache对象
	// DayEndCache.cleanDayEndCache();
	// }
	// }
	//
	// /**
	// * 执行日终方法
	// * @param dayEndStepLogVo
	// */
	// private void executeDayEndListion(DayEndStepLogVo dayEndStepLogVo) throws
	// Exception{
	// DataSourceTransactionManager transactionManager =
	// (DataSourceTransactionManager)
	// SpringContextHolder.getBean("transactionManager");
	// DefaultTransactionDefinition def = new DefaultTransactionDefinition();
	// def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
	// //事物隔离级别，开启新事务，与A类和B类不使用同一个事务。
	// TransactionStatus transactionStatus =
	// transactionManager.getTransaction(def); //获得事务状态
	//
	// //0.判断：当前步骤是否已执行，且不允许重跑
	// String executeFun = dayEndStepLogVo.getExecute_name();
	// String bizDate = dayEndStepLogVo.getBiz_date();
	// String status = dayEndStepLogVo.getStatus();
	// String stepId = dayEndStepLogVo.getStep_id();
	// String isLoop = dayEndStepLogVo.getIs_loop();
	// String stepName = dayEndStepLogVo.getStep_name();
	// String isSkip = dayEndStepLogVo.getIs_skip();
	// if(DictConstants.DayEndStatus.Seccuss.compare(status) &&
	// DictConstants.YesNo.NO.compare(isLoop)){
	// return;
	// }
	//
	// //1.初始化日志对象
	// TtDayendStepLog ttDayendStepLog = new TtDayendStepLog();
	// ttDayendStepLog.setStep_id(stepId);
	// ttDayendStepLog.setBiz_date(bizDate);
	//
	// //2.循环调用接口程序
	// String logs = String.format("1.任务[%s]开始.时间:%s</br>",stepName,
	// DateUtil.getCurrentDateTimeAsString());
	// //edit by gm 启动时记录日终步骤"执行中"
	// dayendStepService.executeStatus(stepId, bizDate, "",
	// DictConstants.DayEndStatus.Executing.value());
	//
	// try{
	// DayendListener dayendListener = SpringContextHolder.getBean(executeFun);
	// String exelogs = dayendListener.executeDayEnd(virtualSession) + "</br>";
	// logs += String.format("2.任务[%s]执行日志: %s </br>",stepName,exelogs);
	// logs += String.format("3.任务[%s]执行完成.时间:%s</br>",stepName,
	// DateUtil.getCurrentDateTimeAsString());
	//
	// //3.执行完成后更改步骤执行状态
	// status = DictConstants.DayEndStatus.Seccuss.value();
	// transactionManager.commit(transactionStatus);
	// }catch (Exception e) {
	// //增加允许跳过流程
	// if(DictConstants.YesNo.NO.compare(isSkip)){
	// transactionManager.rollback(transactionStatus);
	// logs += String.format("2.任务[%s]执行异常: %s </br>",stepName,e.toString());
	// status = DictConstants.DayEndStatus.Error.value();
	// throw e;
	// }else{
	// logs +=
	// String.format("2.任务[%s]执行异常,<该步骤允许跳过>: %s </br>",stepName,e.toString());
	// status = DictConstants.DayEndStatus.Error.value();
	// }
	// }finally{
	// ttDayendStepLog.setLogs(logs);
	// ttDayendStepLog.setStatus(status);
	// ttDayendStepLog.setOperate_time(DateUtil.getCurrentDateTimeAsString());
	// ttDayendStepLog.setOperator_id(virtualSession.getUserVo().getUser_id());
	// saveDayendStepLog(ttDayendStepLog, stepId, bizDate);
	// }
	// }
	//
	// private void saveDayendStepLog(TtDayendStepLog ttDayendStepLog, String
	// stepId, String bizDate){
	// DataSourceTransactionManager transactionManager =
	// (DataSourceTransactionManager)
	// SpringContextHolder.getBean("transactionManager");
	// DefaultTransactionDefinition def = new DefaultTransactionDefinition();
	// def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
	// //事物隔离级别，开启新事务，与A类和B类不使用同一个事务。
	// TransactionStatus transactionStatus =
	// transactionManager.getTransaction(def); //获得事务状态
	//
	// //4.业务日为空时在日志表中不存在记录
	// HashMap<String, Object> map = new HashMap<String, Object>();
	// map.put("biz_date", ttDayendStepLog.getBiz_date());
	// map.put("step_id", stepId);
	// // 记录步骤日志
	// dayendStepService.recordStepLog(map, bizDate, ttDayendStepLog);
	//
	// // List<DayEndStepLogVo> list = dayendStepDao.searchDayEndLog(map);
	// // if(list.size() == 0){
	// // ttDayendStepLog.setBiz_date(bizDate);
	// // dayendStepDao.insertStepLog(ttDayendStepLog);
	// // }else{
	// //
	// // TtDayendStepLog ttTemp = dayendStepDao.getStepLog(ttDayendStepLog);
	// // ttDayendStepLog.setLogs(ttTemp.getLogs()+ttDayendStepLog.getLogs());
	// //
	// // dayendStepDao.updateStepLog(ttDayendStepLog);
	// // }
	// transactionManager.commit(transactionStatus);
	// }
	//

}
