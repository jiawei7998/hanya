package com.singlee.capital.dayend.mapper;

import com.singlee.capital.dayend.model.TtDayendStep;
import com.singlee.capital.dayend.model.TtDayendStepLog;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TtDayendStepMapper extends Mapper<TtDayendStep> {
	
//	/**
//	 * 按顺序加载所有激活状态的步骤
//	 * @return
//	 */
//	List<TtDayendStep> selectBySort(String active);
//	
	/**
	 * 返回指定日期的日终步骤 执行日志  
	 * @param busiDate
	 * @return
	 */
	List<TtDayendStepLog> searchDayendStepLog(String busiDate);
	
	
	/**
	 * 
	 * @param busiDate
	 * @param flag
	 * @return
	 */
	int updateDayendStepStopFlag(@Param("stepId") String stepId, @Param("isStopFlag") String isStopFlag);
	
}