package com.singlee.capital.dayend.model;

import javax.persistence.*;
/**
 * 轧帐日志表
 * @author CZ
 *
 */

@Entity
@Table(name = "TT_DAYEND_ROLLING_ACCOUNT_LOG")
public class TtDayendRollingAccountLog implements java.io.Serializable {

	private static final long serialVersionUID = -8088140173863418551L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TT_DAYEND_ROLLING_LOG.NEXTVAL FROM DUAL")
	private String logId;//日志编号
	private String rollingDate;//轧帐日期
	private String instId;//机构编号
	private String status;//轧帐状态
	private String operateTime;//最后操作时间
	private String operatorId;//最后操作人
	private String logs;//日志信息
	@Transient
	private String instName; // 机构名称
	
	public String getLogId() {
		return logId;
	}
	public void setLogId(String logId) {
		this.logId = logId;
	}
	public String getRollingDate() {
		return rollingDate;
	}
	public void setRollingDate(String rollingDate) {
		this.rollingDate = rollingDate;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOperateTime() {
		return operateTime;
	}
	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getLogs() {
		return logs;
	}
	public void setLogs(String logs) {
		this.logs = logs;
	}
	public String getInstName() {
		return instName;
	}
	public void setInstName(String instName) {
		this.instName = instName;
	}
	
}
