package com.singlee.capital.dayend.mapper;

import com.singlee.capital.dayend.model.TtDayendStepLog;
import tk.mybatis.mapper.common.Mapper;

public interface TtDayendStepLogMapper extends Mapper<TtDayendStepLog> {
}