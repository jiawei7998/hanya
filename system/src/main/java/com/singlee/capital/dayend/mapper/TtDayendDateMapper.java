package com.singlee.capital.dayend.mapper;

import com.singlee.capital.dayend.model.TtDayendDate;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtDayendDateMapper extends Mapper<TtDayendDate> {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	int updateDayendDate(Map<String,Object> map);
	/**
	 * 查询是否是节假日
	 * @param map
	 * @return
	 */
	int queryJJR(Map<String,Object> map);
	/**
	 * 强制 flashCache = true
	 * @return
	 */
	List<TtDayendDate> selecteTtDayendDate();
	  
}