package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SelfCallWrapperService;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.mapper.TtDayendDateMapper;
import com.singlee.capital.dayend.model.TtDayendDate;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.DayendRollingAccountService;
import com.singlee.capital.system.dict.ExternalDictConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * 日终 服务
 * @author Lyon Chen
 */
@Service("dayendDateService")
public class DayendDateServiceImpl extends SelfCallWrapperService<DayendDateService>  implements DayendDateService {

	private static final String cacheName = "com.singlee.capital.dayend.date";
	@Autowired
	private TtDayendDateMapper ddMapper;
	
	@Autowired
	private DayendRollingAccountService dayendRollingAccountService;

	/** 日终任务 线程 引用，jvm内部保证唯一项！  */
	@Autowired
	private DayendTask dt;

	@Override
	@Cacheable(value = cacheName, key="#root.methodName")	
	public String getDayendDate() {
		return myself().getTtDayendDate().getCurDate();	
	}

	@Override
	@Cacheable(value = cacheName, key="#root.methodName")	
	public String getDayendDateStatus() {
		return myself().getTtDayendDate().getStatus();
	}

	@Override
	@Cacheable(value = cacheName, key="#root.methodName")	
	public TtDayendDate getTtDayendDate(){
		List<TtDayendDate> list = ddMapper.selecteTtDayendDate();
		JY.require(list!=null && list.size()==1,
				MsgUtils.getMessage("error.dayend.0001", ""));
		return list.get(0);
	}

	@Override
	@CacheEvict(value=cacheName,allEntries=true)
	@Transactional(value="transactionManager",rollbackFor=Exception.class,propagation=Propagation.REQUIRES_NEW) 
	public int updateTtDayendDate(Map<String, Object> map) {
		return ddMapper.updateDayendDate(map);
	}
	
	@Override
	@Cacheable(value = cacheName, key="#root.methodName")	
	public String getNextDayendDate() {
		return DateUtil.dateAdjust(myself().getDayendDate(), 1);
	}

	@Override
	@Cacheable(value = cacheName, key="#root.methodName")	
	public String getPrevDayendDate() {
		return DateUtil.dateAdjust(myself().getDayendDate(), -1);
	}

	@Override
	public String getDeltaDayendDate(int delta) {
		if(delta == 0 ){
			return myself().getDayendDate();
		}else if(delta == -1){
			return myself().getPrevDayendDate();
		}else if(delta == 1){
			return myself().getNextDayendDate();
		}else{
			return DateUtil.dateAdjust(myself().getDayendDate(), delta);
		}
	}

	@Override
	public String startDayend(SlSession session) {
		return startDayend(session, 3);
	}
	
	@Override
	public String startDayend(SlSession session, int waitSeconds) {
		// 初始化 会话资料 
		dt.init(session, myself().getDayendDate());
        String ret = null;
		//直接使用Thread的方式执行    
        FutureTask<String> ft = new FutureTask<String>(dt);    
        Thread t = new Thread(ft, "dayend thread");    
        t.start(); 
        // 等待 3秒获取错误内容
        try {    
        	if(waitSeconds <= 0){
        		ret = ft.get();//直接等待
        	}else{
        		ret = ft.get(waitSeconds, TimeUnit.SECONDS);//等待 x 秒
        	}
        } catch (Exception e) {
        	ret = e.getMessage();
		} 
        if(ret == null){
        	ret = "日终任务线程已经运行"+waitSeconds+"秒，还在持续运行中，请耐心等待并及时查询步骤状态!";
        }
        return ret;
	}
	
	@Override
	public String startDayendContinue(Map<String,Object> map, SlSession session) {
		//当前业务日
		String bizDate = myself().getDayendDate();
		//目标业务日
		String tgtDate = ParameterUtil.getString(map, "tgtDate", DateUtil.dateAdjust(bizDate, 2));
//		//天数
//		int days = DateUtil.daysBetween(bizDate, tgtDate);
		
		//日志
		String log = "日终任务线程完成!";
		try{
			//for(int i = 0; i < days; i++){
			while(bizDate.compareTo(tgtDate)<0){
				//检查业务日状态
				String status = myself().getDayendDateStatus();
				//如果业务日状态为checked/error 则跳过轧帐
				if(!(ExternalDictConstants.DayendDateStatus_Checked.equals(status)
						|| ExternalDictConstants.DayendDateStatus_Error.equals(status) )){
					//轧帐操作
					dayendRollingAccountService.checkAllInstitutionRollingAccountStatus(bizDate);
				}
				// 初始化 会话资料 
				dt.init(session, bizDate);
				// 直接使用Thread的方式执行    
		        FutureTask<String> ft = new FutureTask<String>(dt);    
		        Thread t = new Thread(ft, "dayend thread");    
		        t.start(); 
		        // 等待结果
		        String ret = ft.get();
		        JY.info("------------------------------------------------");
		        JY.info("-------------当前的业务日 %s 跑批结果 %s---------------------", bizDate, ret);
		        // 新业务日
		        bizDate =  myself().getDayendDate();
		        JY.info("-------------当前的业务日 %s---------------------", bizDate);
		        JY.require("日终任务线程完成!".equals(ret),ret);
			}
		}catch(Exception e){
			 log = e.getMessage();
		}
		return log;
		
	}

	@Override
	public void stopDayend(SlSession session) {
		if(dt!=null){
			dt.stop();
		}
	}
	/** 系统初始化业务日	 */
	@Override
	public void initDayendDate() {
		List<TtDayendDate> ttDayendDates = ddMapper.selectAll();
		JY.require(ttDayendDates.size()==1, ("系统业务日期设置错误"));
		TtDayendDate ttDayendDate = ttDayendDates.get(0);
		JY.require(ttDayendDate!=null, ("系统业务日期读取错误"));
	}

	
	/**  
	 * constants 的部分迁移过来。同一个入口处理
	 */

	/**
	 * 当前业务日 + 系统时间 HH:MM:SS
	 * @return
	 */
	@Override
	public String getSettlementDateTime() {
		return myself().getSettlementDate() + " " + DateTimeUtil.getLocalTime();
	}

	/**
	 * 获得当前业务结算日 从结算日期表中获取
	 * 
	 * @return
	 */
	@Override
	@Cacheable(value = cacheName, key="#root.methodName")	
	public String getSettlementDate() {
		return myself().getDayendDate();
	}

	@Override
	public boolean isSettlementDate(String date){
		return myself().getSettlementDate().equals(date);
	}

	@Override
	public int queryJJR(Map<String, Object> map) {
		
		return ddMapper.queryJJR(map);
	}
	
}
