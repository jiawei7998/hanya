package com.singlee.capital.dayend.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.dayend.model.TtDayendStepLog;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.DayendStepService;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 日终相关
 * @author Lyon Chen 
 */
@Controller
@RequestMapping(value = "/DayendController")
public class DayendController extends CommonController {

	
	/** 日终步骤 管理业务层 **/
	@Autowired
	public DayendStepService dayendStepService;
	/** 日终 管理业务层 **/
	@Autowired
	public DayendDateService dayendDateService;
	
	/**
	 * 查询日终执行步骤日志表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDayendStepLog")
	public RetMsg<List<TtDayendStepLog>> searchDayendStepLog(@RequestBody Map<String,Object> params) {
		String bizDate = ParameterUtil.getString(params, "bizDate", DateUtil.nowDate());
		List<TtDayendStepLog> list = dayendStepService.searchDayEndStepLog(bizDate);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 暂停某个日终步骤
	 */
	@ResponseBody
	@RequestMapping(value = "/stopDayendStep/{stepId}")
	public RetMsg<Serializable> stopDayendStep(@PathVariable("stepId") String stepId) {
		dayendStepService.stopDayendStep(stepId);
		return RetMsgHelper.ok();
	}

	/**
	 * 恢复某个日终步骤
	 */
	@ResponseBody
	@RequestMapping(value = "/resumeDayendStep/{stepId}")
	public RetMsg<Serializable> resumeDayendStep(@PathVariable("stepId") String stepId) {
		dayendStepService.resumeDayendStep(stepId);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 启动日终程序
	 */
	@RequestMapping(value = "/startDayendTaskJob")
	public void startDayendTaskJob() {
	}
	/**
	 * 启动日终程序
	 */
	@ResponseBody
	@RequestMapping(value = "/startDayendTask")
	public RetMsg<Serializable> startDayendTask() {
		String s = dayendDateService.startDayend(SlSessionHelper.getSlSession());
		return RetMsgHelper.ok(s);
	}
	
	/**
	 * 启动日终程序
	 * @throws IOException 
	 */
	@RequestMapping(value = "/startDayendTaskJob", method = RequestMethod.POST)
	public void startDayendTaskJob(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String s="";
		try{
			s = dayendDateService.startDayend(SlSessionHelper.getSlSession());
			res.setStatus(200);
		}
		catch(Exception e){
			s=e.getMessage();
			res.setStatus(400);
		}
		OutputStream out = res.getOutputStream();
		res.setHeader("Biz-Date",dayendDateService.getDayendDate());
		out.write(s.getBytes());
		out.flush();
	}
	

	/**
	 * 停止日终程序
	 */
	@ResponseBody
	@RequestMapping(value = "/stopDayendTask")
	public RetMsg<Serializable> stopDayendTask() {
		dayendDateService.stopDayend(SlSessionHelper.getSlSession());
		return RetMsgHelper.ok();
	}
	
//	/**
//	 * 日终回退程序
//	 */
//	@ResponseBody
//	@RequestMapping(value = "/rollbackDayendTask")
//	public void rollbackDayendTask(){
//		//dayendStepService.rollbackEndDay();
//	}
	
	/**
	 * 获取系统业务日期
	 */
	@ResponseBody
	@RequestMapping(value = "/getDayendDate")
	public RetMsg<Serializable> getDayendDate() {
		String dayendDate = dayendDateService.getDayendDate();
		return RetMsgHelper.ok(dayendDate);
	}
	
	/**
	 * 查询日终执行步骤日志表
	 * @throws IOException 
	 */
	@RequestMapping(value = "/searchDayendStepLogList", method = RequestMethod.POST)
	public void searchDayendStepLogList(HttpServletRequest req, HttpServletResponse res) throws IOException{
		List<TtDayendStepLog> list = dayendStepService.searchDayEndStepLog(dayendDateService.getDayendDate());
		OutputStream out = res.getOutputStream();
		res.setHeader("Biz-Date",dayendDateService.getDayendDate());
		out.write(FastJsonUtil.toJSONString(list).getBytes());
		out.flush();
	}
	
	/**
	 * 批量日终
	 */
	@ResponseBody
	@RequestMapping(value = "/startDayendContinue")
	public RetMsg<Serializable> startDayendContinue(@RequestBody Map<String,Object> params) {
		String log = dayendDateService.startDayendContinue(params, SlSessionHelper.getSlSession());
		if("日终任务线程完成!".equals(log)){
			return RetMsgHelper.ok(log);	
		}else{
			return RetMsgHelper.simple("-1111", log);
		}
		
	}
}
