package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.model.TtDayendDate;
import com.singlee.capital.system.dict.ExternalDictConstants;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 日终处理步骤 的抽象类
 * 
 * @author x230i
 * 
 */
@Service("bussCalendarDayendFlowHandler")
public class BussCalendarDayendFlowHandler extends CommonDayendFlowHandler{
	@Override
	public void execute(SlSession session) {
		String newDate = dayendDateService.getNextDayendDate();
		// ----------------------------------乐观锁开始占位------------------------------------------
		TtDayendDate tdd = dayendDateService.getTtDayendDate();
		// 是否是轧账完成准备状态/或者错误状态。
		JY.require(ExternalDictConstants.DayendDateStatus_Executing.equals(tdd.getStatus()) , 
				MsgUtils.getMessage("error.dayend.0005", tdd.getStatus()));
		// ----------------------------------乐观锁开始检验------------------------------------------
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("newDate", newDate);
		map.put("curDate", tdd.getCurDate());
		map.put("status", tdd.getStatus());
		map.put("version", tdd.getVersion());
		@SuppressWarnings("unused")
		int lock = dayendDateService.updateTtDayendDate(map);
		// ----------------------------------乐观锁结束------------------------------------------
	}
}
