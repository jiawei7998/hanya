package com.singlee.capital.dayend.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 日终审批流程日志
 * @author CZ
 *
 */

@Entity
@Table(name = "TT_DAYEND_STEP_LOG")
public class TtDayendStepLog implements java.io.Serializable {
	private static final long serialVersionUID = 7775998496033012482L;

	@Id
	private String bizDate;//业务日期
	@Id
	private String stepId;//步骤编号
	private String status;// 步骤执行状态
	private String logs;//日志
	private String operateTime;//最后执行时间
	private String operatorId;//最后执行人

	@Transient
	private String stepName;//步骤名称
	@Transient
	private int stepSort;//步骤排序
	@Transient
	private String isSkip;//是否跳过
	@Transient
	private String isLoop;//是否重跑
	@Transient
	private String isStopFlag;//

	
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public int getStepSort() {
		return stepSort;
	}
	public void setStepSort(int stepSort) {
		this.stepSort = stepSort;
	}
	public String getIsSkip() {
		return isSkip;
	}
	public void setIsSkip(String isSkip) {
		this.isSkip = isSkip;
	}
	public String getIsLoop() {
		return isLoop;
	}
	public void setIsLoop(String isLoop) {
		this.isLoop = isLoop;
	}
	public String getLogs() {
		return logs;
	}
	public void setLogs(String logs) {
		this.logs = logs;
	}
	public void appendLogs(String logs){
		if(this.logs == null){
			this.logs = logs;
		}else{
			this.logs = logs + "\r\n\r\n" + this.logs ;
		}
	}
	public String getBizDate() {
		return bizDate;
	}
	public void setBizDate(String bizDate) {
		this.bizDate = bizDate;
	}
	public String getStepId() {
		return stepId;
	}
	public void setStepId(String stepId) {
		this.stepId = stepId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOperateTime() {
		return operateTime;
	}
	public void setOperateTime(String operateTime) {
		this.operateTime = operateTime;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getIsStopFlag() {
		return isStopFlag;
	}
	public void setIsStopFlag(String isStopFlag) {
		this.isStopFlag = isStopFlag;
	}
	
	
}
