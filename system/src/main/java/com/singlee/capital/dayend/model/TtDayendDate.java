package com.singlee.capital.dayend.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 当前 业务日 
 * @author LyonChen
 */

@Entity
@Table(name = "TT_DAYEND_DATE")
public class TtDayendDate implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8528430317940830162L;
	
	// 当前业务日
	@Id
	private String curDate;
	private String status;//业务日状态
	private int version;//版本 mvvc 

	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public TtDayendDate() {
	}

	public String getCurDate() {
		return curDate;
	}

	public void setCurDate(String cur_date) {
		this.curDate = cur_date;
	}

	@Override
	public String toString() {
		return String.format("TtDayendDate [curDate=%s]", curDate);
	}

	
}
