package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.model.TtDayendStep;
import com.singlee.capital.dayend.model.TtDayendStepLog;
import com.singlee.capital.dayend.service.DayendFlowHandler;
import com.singlee.capital.dayend.service.DayendStepService;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.chain.Context;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 日终处理步骤 的抽象类
 * 
 * @author x230i
 * 
 */
public class CommonDayendFlowHandler extends AbstractDayendFlowHandler{

	@Autowired
	protected DayendStepService dss;

	private TtDayendStep tds;

	/**
	 * 同步 日终步骤日志 数据库记录 
	 * @param tds
	 * @param userId
	 */
	@Override
	protected void sychroniseTtdayendStepLog(String userId, String status, String log) {
		if(tds==null) {return;}
		boolean isNew = false;
		if(tds.getTtDayendStepLog() == null){
			TtDayendStepLog tdsl = new TtDayendStepLog();
			tdsl.setBizDate(dayendDateService.getSettlementDate());
			tdsl.setStepId(tds.getStepId());
			tds.setTtDayendStepLog(tdsl);
			isNew = true;
		}
		tds.getTtDayendStepLog() .appendLogs(log);
		tds.getTtDayendStepLog() .setOperatorId(userId);
		tds.getTtDayendStepLog() .setOperateTime(DateTimeUtil.getLocalDateTime());
		tds.getTtDayendStepLog() .setStatus(status);
		if(isNew){
			dss.addDayendStepLog(tds.getTtDayendStepLog());
		}else{
			dss.modifyDayendStepLog(tds.getTtDayendStepLog());
		}
		
	}
	
	
	@Override
	public boolean execute(Context arg0) throws Exception {
		
		
		JY.info("----DAYEND(CDFH)----[%s]----execute ", this.getClass().getSimpleName());
		SlSession session = (SlSession)arg0.get(DayendFlowHandler.slsession);
		// 修改日终步骤 状态 为 执行中
		sychroniseTtdayendStepLog(SlSessionHelper.getUserId(session), ExternalDictConstants.DayendFlowStatus_Executing, "");
		JY.info("----DAYEND(CDFH)----[%s]----executing ", this.getClass().getSimpleName());
		String log = null;
		try{
			
			JY.require(!stopFlag, "本步骤被临时停止，请恢复本步骤后再重新开始日终！");
			
			// DO IT 
			if(tds!=null){ // 为了 事务注解能被spring正确执行！再绕一边！
				CommonDayendFlowHandler cdfh = SpringContextHolder.getBean(tds.getExecuteName());
				cdfh.execute(session);
			}else{
				// initDayendFlowHandler
				execute(session);
			}
			log = "---success---"+DateTimeUtil.getLocalDateTime()+"---\r\n";
			// 修改 日终 步骤状态为成功 
			sychroniseTtdayendStepLog(SlSessionHelper.getUserId(session), ExternalDictConstants.DayendFlowStatus_Success, 
					log);
			JY.info("----DAYEND(CDFH)----[%s]----success ", this.getClass().getSimpleName());
		}catch(Throwable e){
			log = "\r\n---error---"+DateTimeUtil.getLocalDateTime()+"---\r\n" + 
					StringUtil.join(ExceptionUtils.getRootCauseStackTrace(e), "\r\n");
			// 修改 日终 步骤状态为错误  
			sychroniseTtdayendStepLog(SlSessionHelper.getUserId(session), ExternalDictConstants.DayendFlowStatus_Error, log);
			JY.info("----DAYEND(CDFH)----[%s]----error \r\n %s ", this.getClass().getSimpleName(), log);
			// 这个步骤是否是允许跳过的。 
			if(tds!=null && ExternalDictConstants.YesNo_YES.equals(tds.getIsSkip())){
				// 可以忽略  
			}else{
				// 不能忽略，就继续 往外面 抛异常 
				throw new Exception(e);
			}
		}
		return false;
	}
	
	
	@Override
	public void execute(SlSession session) {
	}
	/**
	 * 兼容 老的没有告警返回的写法
	 */
	@Override
	public String execute2(SlSession session) {
		execute(session);
		return null;
	}

	@Override
	public boolean postprocess(SlSession session, Exception exception) {
		return false;
	}
	
	public void setTtDayendStep(TtDayendStep tds) {
		this.tds = tds;
	}
	

	private boolean stopFlag = false;
	
	
	public void setStopFlag(boolean f){
		stopFlag = f;
	}
	public boolean getStopFlag(){
		return stopFlag;
	}
}
