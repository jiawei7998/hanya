package com.singlee.capital.dayend.mapper;

import com.singlee.capital.dayend.model.TtDayendRollingAccountLog;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtDayendRollingAccountLogMapper extends Mapper<TtDayendRollingAccountLog> {

	/**
	 * 根据轧账日期查询所有机构轧账状态
	 * @param map
	 * @return
	 */
	public List<TtDayendRollingAccountLog> searchInstitutionRollingAccountStatus(Map<String,Object> map);
	
}