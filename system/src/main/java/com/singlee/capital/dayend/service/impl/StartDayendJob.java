package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.cron.CronRunnable;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.session.impl.SlSessionHelper;

import java.util.Map;

public class StartDayendJob implements CronRunnable {

	private DayendDateService dayendDateServiceImpl = SpringContextHolder.getBean(DayendDateServiceImpl.class);

		
	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		
		JY.info("==========定时跑批开始==========");
		dayendDateServiceImpl.startDayend(SlSessionHelper.getSlSession());
		JY.info("==========定时跑批结束==========");
		return true;
	}

	@Override
	public void terminate() {
		
	}

}
