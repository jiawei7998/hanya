package com.singlee.capital.dayend.service;

import com.singlee.capital.dayend.model.TtDayendStep;
import com.singlee.capital.dayend.model.TtDayendStepLog;

import java.util.List;

public interface DayendStepService {
//	/**
//	 * 检索所有步骤 
//	 * @param isOnline 是否限制是 上线 的步骤 ，否的话，将所偶系统内的步骤整理出来。
//	 */
//	public void searchDayendStep(boolean isOnline);
//	
//	/**
//	 * 将某个步骤上线
//	 * @param stepId
//	 */
//	public void onlineDayendStep(String stepId);
//	
//	/**
//	 * 将某个步骤下线
//	 * @param stepId
//	 */
//	public void offlineDayendStep(String stepId);
	

	/**
	 * 带上 顺序 返回 步骤  列表 
	 * @return
	 */
	public  List<TtDayendStep> listDayendStepIdWithSort();
	

	
	
	
	
	
	
	
	/////////////////////////////////////步骤相关////////////////////////////////////////

	/**
	 * 查询日终执行步骤日志表
	 * @param map
	 * @return
	 */
	public List<TtDayendStepLog> searchDayEndStepLog(String biz_date);
//
//	/**
//	 * 日终日志状态写入方法
//	 * @param stepId
//	 * @param bizDate
//	 */
//	public void executeStatus(String stepId,String bizDate,String logs,String status);
//
//
//





	/**
	 * 当天 的日终步骤记录 
	 * @return
	 */
	public List<TtDayendStepLog> currentDayendStepLog(String bussnessDate);


//
//	
//	/**
//	 * 根据日终步骤表，初始化 日终步骤记录表数据
//	 * @param bussnessDate
//	 */
//	public void initDayendStepLog(String bussnessDate);

	
	/**
	 * 增加 一个日终步骤 
	 * @param tdsl
	 */
	public void addDayendStepLog(TtDayendStepLog tdsl);
	/**
	 * 修改 一个日终步骤 
	 * @param tdsl
	 */
	public void modifyDayendStepLog(TtDayendStepLog tdsl);
	
	
	/**
	 * 暂停日终步骤 stepId
	 * @param stepId
	 */
	public void stopDayendStep(String stepId);

	/**
	 * 恢复日终步骤 stepId
	 * @param stepId
	 */
	public void resumeDayendStep(String stepId);
	
//	
//	/**
//	 * 启动日终程序
//	 */
//	public void startEndDay(String step_id);
//	
//	/**
//	 * 强制停止日终程序
//	 */
//	public void stopEndDay();
//	/**
//	 * 启动日终回退程序
//	 */
//	public void rollbackEndDay();
//
//	/**
//	 * 解锁数据库的日终执行状态
//	 */
//	public void unLockedDayendForDataBase();
//
//	/**
//	 * 锁定数据库的日终执行状态
//	 */
//	public void lockedDayendForDataBase();
	
	
}
