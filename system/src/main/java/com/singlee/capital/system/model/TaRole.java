package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;


/**
 * 角色对象
 * 
 * @author lyonchen
 * 
 */

@Entity
@Table(name = "TA_ROLE")
public class TaRole implements Serializable {

	private static final long serialVersionUID = -1107628330209413905L;
	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TA_BASE.NEXTVAL FROM DUAL")
	private String roleId;// 角色ID
	private String roleFlag;// 角色类别
	private String roleName;// 角色名称
	private String isActive;// 启用标记
	private String roleMemo;// 备注
	private String instType;// 机构类型
	private String instId;// 所属机构
	private String roleLevel;// 角色级别
	private String opratorType;
	private String approveStatus;
	private String submitUserId;
	private String submitInstId;
	private String approveUserId;
	private String approveInstId;
	private String approveDate;
	private String pRoleId;
	private String indexPage;
	private String branchId;//银行号
	
	
	@Transient
	private List<TaRoleModuleMap> moduleMapList;
	@Transient
	private List<TaRoleFunctionMap> functionMapList;
	@Transient
	private String instName;
	@Transient
	private String instsId;//多机构时使用
	@Transient
	private String instsType;//多机构时使用

	public TaRole() {
	}
	
 
	
	public String getBranchId() {
		return branchId;
	}



	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}



	public String getInstsId() {
		return instsId;
	}


	public void setInstsId(String instsId) {
		this.instsId = instsId;
	}


	public String getInstsType() {
		return instsType;
	}


	public void setInstsType(String instsType) {
		this.instsType = instsType;
	}


	public String getRoleId() {
		return roleId;
	}

	public List<TaRoleFunctionMap> getFunctionMapList() {
		return functionMapList;
	}


	public void setFunctionMapList(List<TaRoleFunctionMap> functionMapList) {
		this.functionMapList = functionMapList;
	}


	public List<TaRoleModuleMap> getModuleMapList() {
		return moduleMapList;
	}

	public void setModuleMapList(List<TaRoleModuleMap> moduleMapList) {
		this.moduleMapList = moduleMapList;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleFlag() {
		return roleFlag;
	}

	public void setRoleFlag(String roleFlag) {
		this.roleFlag = roleFlag;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getRoleMemo() {
		return roleMemo;
	}

	public void setRoleMemo(String roleMemo) {
		this.roleMemo = roleMemo;
	}

	public String getInstType() {
		return instType;
	}

	public void setInstType(String instType) {
		this.instType = instType;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getRoleLevel() {
		return roleLevel;
	}

	public void setRoleLevel(String roleLevel) {
		this.roleLevel = roleLevel;
	}

	public String getOpratorType() {
		return opratorType;
	}

	public void setOpratorType(String opratorType) {
		this.opratorType = opratorType;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getSubmitUserId() {
		return submitUserId;
	}

	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	public String getSubmitInstId() {
		return submitInstId;
	}

	public void setSubmitInstId(String submitInstId) {
		this.submitInstId = submitInstId;
	}

	public String getApproveUserId() {
		return approveUserId;
	}

	public void setApproveUserId(String approveUserId) {
		this.approveUserId = approveUserId;
	}

	public String getApproveInstId() {
		return approveInstId;
	}

	public void setApproveInstId(String approveInstId) {
		this.approveInstId = approveInstId;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getpRoleId() {
		return pRoleId;
	}

	public String getIndexPage() {
		return indexPage;
	}

	public void setIndexPage(String indexPage) {
		this.indexPage = indexPage;
	}

	public void setpRoleId(String pRoleId) {
		this.pRoleId = pRoleId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TaRole [roleId=");
		builder.append(roleId);
		builder.append(", roleFlag=");
		builder.append(roleFlag);
		builder.append(", roleName=");
		builder.append(roleName);
		builder.append(", isActive=");
		builder.append(isActive);
		builder.append(", roleMemo=");
		builder.append(roleMemo);
		builder.append(", instType=");
		builder.append(instType);
		builder.append(", moduleMapList=");
		builder.append(moduleMapList);
		builder.append(", functionMapList=");
		builder.append(functionMapList);
		builder.append("]");
		return builder.toString();
	}

}
