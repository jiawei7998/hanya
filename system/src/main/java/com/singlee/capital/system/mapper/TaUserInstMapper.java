package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaUserInst;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;

public interface TaUserInstMapper extends Mapper<TaUserInst>{
	List<String> searchInstByUserId(HashMap<String, Object> map);

}
