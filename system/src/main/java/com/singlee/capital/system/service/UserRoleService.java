package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaRole;
import com.singlee.capital.system.model.TaUserRoleMapCopy;

import java.util.List;
import java.util.Map;

public interface UserRoleService {

	public Page<TaUserRoleMapCopy> getWaitApproveList(Map<String, Object> map);
	
	public List<TaRole> getOldRoleList(Map<String, Object> map);
	
	public List<TaRole> getNewRoleList(Map<String, Object> map);

	public Page<TaUserRoleMapCopy> getApprovedList(Map<String, Object> map);
	
	public void doApprove(Map<String, Object> map);
}
