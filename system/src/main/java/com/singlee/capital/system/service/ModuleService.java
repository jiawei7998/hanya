package com.singlee.capital.system.service;

import com.singlee.capital.system.model.TaModule;

import java.util.List;
import java.util.Map;

public interface ModuleService {

	
	/** 
	 * 递归获得所有 roldeIds 指定的 模块
	 *  
	 * @param roleIds
	 * @return
	 */
	List<TaModule> recursionModule(List<String> list);
	void updateModuleBranchId(Map<String, Object> map);
	void saveModuleFunction(Map<String, Object> map);
//	/**
//	 * 根据 roleid 获得 模块 
//	 * @param list
//	 * @return
//	 */
//	LinkedList<TreeModule> recursionModule(String role);
	
	TaModule getModuleByBranchIdAndModuleId(Map<String, Object> map);
	/**
	 * 
	 */
	public void updateModuleDesk(Map<String, Object> map);
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	public void saveModuleByBranchId(Map<String, Object> map);
	
	List<String> getUserModule(Map<String, Object> map);
	List<String> getHavingFuncId(Map<String, Object> map);
	
	List<TaModule>  getMenuUrlsByUserId(String userId);
	
	List<TaModule> getTotalPageUrls();

	List <TaModule> getDeskModuleList();
}
