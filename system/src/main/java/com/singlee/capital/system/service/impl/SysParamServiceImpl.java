package com.singlee.capital.system.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.TaSysParamMapper;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.service.SysParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("sysParamService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SysParamServiceImpl implements SysParamService {
	
	private static final String cacheName = "com.singlee.capital.system.sysparam";

	
	@Autowired
	private TaSysParamMapper sysParamMapper;	
	@Override
	public void add(TaSysParam sysParam) {
		sysParamMapper.insert(sysParam);
	}

	@Override
	public void modify(TaSysParam sysParam) {

	}

	@Override
	public void delSysParam(String pCode, String pValue) {

	}
	
	/**
	 * 根据p_code 与 p_value查询系统参数
	 * @param p_code
	 * @param p_value
	 * @return
	 */
	@Cacheable(value = cacheName, key="#p_code")
	@Override
	public TaSysParam selectSysParam(String p_code,String p_value){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("p_code", p_code);
		map.put("p_value", p_value);
		List<TaSysParam> list = sysParamMapper.selectTaSysParam(map);
		if(list.size() == 0){
			JY.raiseRException("系统参数%s不存在.", p_code);
		}else if(list.size() > 1){
			JY.raiseRException("关联出多条系统参数 %s .", p_code);
		}
		return list.get(0);
	}

	@Override
	public Page<TaSysParam> getSysParamList(Map<String, Object> map) {
		return  sysParamMapper.searchTaSysParam(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TaSysParam> getSysParamList(TaSysParam sysParam) {
		return null;
	}

	@Override
	public void modifySysParam(TaSysParam sysParam) {
		sysParamMapper.updateByPrimaryKey(sysParam);
		
	}


}
