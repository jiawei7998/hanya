package com.singlee.capital.system.model;

import com.singlee.capital.common.util.TreeInterface;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 模块对象
 * @author x230i
 *
 */
@Entity
@Table(name = "TA_MODULE")
public class TaModule implements Serializable , TreeInterface{
	
	private static final long serialVersionUID = -990570599511625157L;
	@Id
	private String moduleId;//模块ID
	private String modulePid;//模块父节点ID
	private String moduleCode;//模块编码
	private String moduleName;//模块名称
	private Integer moduleSortno;//排序
	private String isActive;//是否启用
	private String moduleReadonly;//是否可写标志
	private String moduleType;//模块类型
	private String moduleShortcut;//快捷键
	private String moduleIcon;//图标
	private String moduleUrl;//模块URL
	private String moduleBusiness;//
	
	private String branchId;//银行号
	private String deskId;//台子Id

	
	@Transient
	private List<TaModuleFunctionMap> moduleFunctionList;
	@Transient
	private List<TaModule> childrenList;
	@Transient
	private List<TaModule> children;
	@Transient
	private List<TaFunction> functions;
	/**
	 * 
	 * @param tm
	 */
	public void addChildrenList(TaModule tm){
		if(childrenList==null){
			childrenList = new ArrayList<TaModule>();
		}
		childrenList.add(tm);
	}
	public List<TaModule> getChildrenList() {
		return childrenList;
	}
	public void setChildrenList(List<TaModule> l) {
		childrenList= l;
		children= l;
	}
	public List<TaModule> getChildren() {
		return children;
	}
	public void setChildren(List<TaModule> children) {
		this.children = children;
	}
	public TaModule() {
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getModulePid() {
		return modulePid;
	}
	public void setModulePid(String modulePid) {
		this.modulePid = modulePid;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public Integer getModuleSortno() {
		return moduleSortno;
	}
	public void setModuleSortno(Integer moduleSortno) {
		this.moduleSortno = moduleSortno;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getModuleReadonly() {
		return moduleReadonly;
	}
	public void setModuleReadonly(String moduleReadonly) {
		this.moduleReadonly = moduleReadonly;
	}
	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getModuleShortcut() {
		return moduleShortcut;
	}
	public void setModuleShortcut(String moduleShortcut) {
		this.moduleShortcut = moduleShortcut;
	}
	public String getModuleIcon() {
		return moduleIcon;
	}
	public void setModuleIcon(String moduleIcon) {
		this.moduleIcon = moduleIcon;
	}
	public String getModuleUrl() {
		return moduleUrl;
	}
	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
	public String getModuleBusiness() {
		return moduleBusiness;
	}
	public void setModuleBusiness(String moduleBusiness) {
		this.moduleBusiness = moduleBusiness;
	}
	public List<TaModuleFunctionMap> getModuleFunctionList() {
		return moduleFunctionList;
	}
	public void setModuleFunctionList(List<TaModuleFunctionMap> moduleFunctionList) {
		this.moduleFunctionList = moduleFunctionList;
	}
	@Override
	public String getId() {
		return moduleId;
	}
	@Override
	public String getParentId() {
		return modulePid;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void children(List<? extends TreeInterface> list) {
		setChildrenList((List<TaModule>) list);
	}
	@Override
	public String getText() {
		return moduleName;
	}
	@Override
	public String getValue() {
		return moduleId;
	}
	@Override
	public List<? extends TreeInterface> children() {
		return (List<? extends TreeInterface>) getChildrenList();
	}
	@Override
	public int getSort() {
		return moduleSortno;
	}
	@Override
	public String getIconCls() {
		return null;
	}
	public List<TaFunction> getFunctions() {
		return functions;
	}
	public void setFunctions(List<TaFunction> functions) {
		this.functions = functions;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getDeskId() {
		return deskId;
	}
	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}
	
	
	
	
}
