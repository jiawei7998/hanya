package com.singlee.capital.system.service.impl;

import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.system.mapper.TaPortalMapper;
import com.singlee.capital.system.mapper.TaPortalVirtualMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.TaPortal;
import com.singlee.capital.system.model.TaPortalVirtual;
import com.singlee.capital.system.service.PortalVirtualService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class PortalVirtualServiceImpl implements PortalVirtualService {
	@Autowired
	TaPortalVirtualMapper taPortalVirtualMapper;//首页样式默认表
	
	@Autowired
	TaPortalMapper taPortalMapper;//首页样式真实表
	
	@Autowired
	TaUserMapper taUserMapper;//用户表
	
	
	@Override
	public void searchDefaultPortal(Map<String, Object> map) {
		String userId= (String) map.get("userId");
		
		List<TaPortalVirtual>  list = taPortalVirtualMapper.searchDefaultPortal(map);
		if(list == null){
			return;
		}
		//1.删除该用户所在银行的所有首页样式表
		Map<String, Object> portalMap = new HashMap<String, Object>();
		portalMap.put("userId", userId);
		portalMap.put("branchId", map.get("branchId"));
		taPortalMapper.deletePortal(portalMap);
		//2.遍历插入真实样式表
		for(TaPortalVirtual tpv:list){
			TaPortal tp = new TaPortal();
			Map<String,Object> map1 = BeanUtil.beanToMap(tpv);
			map1.put("portalUserid", userId);
			try {
				tp = (TaPortal)BeanUtil.mapToBean(TaPortal.class, map1);
				taPortalMapper.insertPortal(tp);
			} catch (IntrospectionException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
			
			
		}
		
		
		
		
		
		
		
		
		
	}

}
