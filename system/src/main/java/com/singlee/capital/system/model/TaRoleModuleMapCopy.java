package com.singlee.capital.system.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TA_ROLE_MODULE_MAP_COPY")
public class TaRoleModuleMapCopy implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String roleId;
	
	private String moduleId;
	
	private String accessFlag;
	
	private String submitUserId;
	
	private String approveUserId;
	
	private String approveStatus;
	
	private String approveDate;
	
	@Transient
	private String roleName;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator="SELECT SEQ_TA_ROLE_MODULE_MAP_COPY.nextval FROM DUAL")
	private String roleModuleId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getAccessFlag() {
		return accessFlag;
	}

	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}

	public String getSubmitUserId() {
		return submitUserId;
	}

	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	public String getApproveUserId() {
		return approveUserId;
	}

	public void setApproveUserId(String approveUserId) {
		this.approveUserId = approveUserId;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getRoleModuleId() {
		return roleModuleId;
	}

	public void setRoleModuleId(String roleModuleId) {
		this.roleModuleId = roleModuleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
