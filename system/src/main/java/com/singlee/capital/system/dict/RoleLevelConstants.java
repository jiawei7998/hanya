package com.singlee.capital.system.dict;

public class RoleLevelConstants {

	public static final String ROLE_MANAGER = "1";// 角色级别--管理员
	
	public static final String ROLE_NORMAL = "2";// 角色级别--负责人
}
