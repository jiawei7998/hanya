package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * 机构 业务辖区
 * 
 * @author lyonchen
 * 
 */

@Entity
@Table(name = "TT_INSTITUTION_SERIAL")
public class TtInstitutionSerial implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String instId;// 机构编号
	private String serialChildInstId;//业务下属子机构
	
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	public String getSerialChildInstId() {
		return serialChildInstId;
	}
	public void setSerialChildInstId(String serialChildInstId) {
		this.serialChildInstId = serialChildInstId;
	}
	
	
}
