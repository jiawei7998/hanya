package com.singlee.capital.system.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.CException;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.session.LoginListener;
import com.singlee.capital.common.session.SessionService;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.*;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TaUserInfo;
import com.singlee.capital.system.model.TaUserRoleMap;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.service.TaUserInfoService;
import com.singlee.capital.system.service.UserRoleMapService;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户管理控制器
 * 
 * @author lihb
 * @date 2016-07-06
 * @company 杭州新利科技有限公司
 */
@Controller
@RequestMapping(value = "/UserController")
public class UserController extends CommonController {
	/* 用户service */
	@Autowired
	private UserService userService;
	/* session service */
	@Autowired
	private SessionService sessionService;
	/* 用户角色映射service */
	@Autowired
	private UserRoleMapService userRoleMapService;
	@Autowired
	private LoginListener loginListener;
	@Autowired
	private TaUserInfoService userInfoService;


	/**
	 * 条件查询列表
	 * 
	 * @param
	 *            - 用户对象
	 * @return 用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageUser")
	public RetMsg<PageInfo<TaUser>> searchPageUser(@RequestBody Map<String, Object> params) throws RException {
		Page<TaUser> page = userService.getPageUser(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 根据银行Id 查询用户
	 */
	@ResponseBody
	@RequestMapping(value = "/searchUserByBranchId")
	public RetMsg<PageInfo<TaUser>> searchUserByBranchId(@RequestBody Map<String, Object> params) throws RException {
		Page<TaUser> page = userService.searchUserByBranchId(params);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 加签候选人分页查询
	 * 
	 * @param param
	 *            pageSize
	 * @author wang
	 * @date 2017-9-26
	 */
	@ResponseBody
	@RequestMapping(value = "/searchInvitationUser")
	public RetMsg<PageInfo<TaUser>> selectInvitationUser(@RequestBody Map<String, Object> param) {
		Page<TaUser> page = userService.getPageInvitationUser(param);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 查询
	 * 
	 * @param params
	 *            - 用户对象
	 * @return 用户对象列表
	 * @author Hunter
	 * @DATE 2016-7-21
	 */
	@ResponseBody
	@RequestMapping(value = "/selectUserWithInstIds")
	public RetMsg<List<TaUser>> selectUserWithInstIds(@RequestBody Map<String, Object> params) throws RException {
		List<TaUser> list = userService.selectUserWithInstIds(params);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 查询用户对象列表(带分页)
	 * 
	 * @param
	 * @return 用户对象列表(带分页)
	 * @author lijing
	 * @date 2018-1-11
	 */
	@ResponseBody
	@RequestMapping(value = "/selectUserWithInstIdsPage")
	public RetMsg<PageInfo<TaUser>> selectUserWithInstIdsPage(@RequestBody Map<String, Object> params) throws RException {
		Page<TaUser> page = userService.selectUserWithInstIdsPage(params);
		return RetMsgHelper.ok(page);
	}
	/**
	 * 根据用户Id 银行Id 查询用户信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/verifyUniqueness")
	public RetMsg<List<String>> verifyUniqueness(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userService.verifyUniqueness(map));
	}

	@ResponseBody
	@RequestMapping(value = "/searchPageRoleUser")
	public RetMsg<PageInfo<TaUser>> searchPageRoleUser(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userService.getPageRoleUser(map));
	}

	/**
	 * 新增用户
	 * 
	 * @param taUser
	 *            - 用户对象
	 * @author wang_chen
	 * @date 2016-7-26
	 */
	@ResponseBody
	@RequestMapping(value = "/addUser")
	public RetMsg<Serializable> addUser(@RequestBody TaUser taUser) throws RException {
		userService.createUser(taUser);
		return RetMsgHelper.ok();
	}

	/**
	 * 修改柜员
	 * 
	 * @param taUser
	 *            - 用户对象
	 * @author wang_chen
	 * @date 2016-7-28
	 */
	@ResponseBody
	@RequestMapping(value = "/updateUser")
	public RetMsg<Serializable> updateUser(@RequestBody TaUser taUser) throws RException {
		userService.updateUser(taUser);
		return RetMsgHelper.ok();
	}

	/**
	 * 新增用户信息
	 * 
	 * @param userInfo
	 * @return
	 * @throws RException
	 */
	@RequestMapping(value = "/addUserInfo")
	@ResponseBody
	public RetMsg<Serializable> createUserInfo(@RequestBody TaUserInfo userInfo) throws RException {
		userInfoService.createUserInfo(userInfo);
		return RetMsgHelper.ok();
	}

	/**
	 * 新增用户信息
	 * 
	 * @param postObj
	 * @return
	 * @throws RException
	 */
	// @RequestMapping(value = "/updateUserInfo")
	// @ResponseBody
	// public RetMsg<Serializable> updateUserInfo(@RequestBody TaUserInfo
	// userInfo) throws RException {
	// userInfoService.updateUserInfo(userInfo);
	// return RetMsgHelper.ok();
	// }
	@RequestMapping(value = "/updateUserInfo")
	@ResponseBody
	public RetMsg<Serializable> updateUserInfo(@RequestBody Map<String, Object> postObj) throws RException {
		TaUser userKey =userInfoService.selectUserInfoById((String) postObj.get("userId"));
		userInfoService.updateUserInfo(postObj,userKey);
		return RetMsgHelper.ok();
	}

	/**
	 * 删除柜员
	 * 
	 * @param userIds
	 *            [] - 用户ID[]
	 * @author wang_chen
	 * @date 2016-7-28
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteUser")
	public RetMsg<Serializable> deleteUser(@RequestBody String[] userIds) throws RException {
		userService.deleteUser(userIds);
		return RetMsgHelper.ok();
	}

	/**
	 * 启用停用柜员
	 * 
	 * @param userIds
	 *            [] - 用户ID[]
	 * @author wang_chen
	 * @date 2016-7-26
	 */
	@ResponseBody
	@RequestMapping(value = "/activeUser")
	public RetMsg<Serializable> activeUser(@RequestBody String[] userIds) throws RException {
		userService.switchUserState(userIds);
		return RetMsgHelper.ok();
	}


	/**
	 * 登录系统
	 * 用户名+密码
	 * @param request
	 *            内置对象，还有response
	 * @param postObj
	 * @return
	 * @throws CException
	 *             可能的业务逻辑异常checked Exception，由父类controller统一处理
	 */
	@ResponseBody
	@RequestMapping(value = "/loginSimple")
	public RetMsg<Serializable> loginSimple(HttpServletRequest request, @RequestBody Map<String, Object> postObj) throws CException {
		String userId =ParameterUtil.getString(postObj, "userId", null);
		sessionService.sessionMap(userId,request.getSession().getId());
		//登录并创建SlSession存放ThreadLocal
		RetMsg<Serializable> key = sessionService.loginSimple(postObj, loginListener);
		if (RetMsgHelper.isOk(key)) {
			//设置SlSession存入httpSession中,
			SessionStaticUtil.setSessionByHttp(request, SessionStaticUtil.getSlSessionThreadLocal());
			//登录成功更新登录信息
			postObj.put("isOnline", "1");
			postObj.put("latestIp", request.getSession().getId());
			sessionService.updateUserLoginInfo(postObj);
		}
		return key;
	}

	/**
	 * 登录系统
	 * 用户名+机构+角色+密码方式登录
	 * @param request
	 *            内置对象，还有response
	 * @param postObj
	 * @return
	 * @throws CException
	 *             可能的业务逻辑异常checked Exception，由父类controller统一处理
	 */
	@ResponseBody
	@RequestMapping(value = "/login")
	public RetMsg<Serializable> login(HttpServletRequest request, @RequestBody Map<String, Object> postObj) throws CException {
		String userId =ParameterUtil.getString(postObj, "userId", null);
		sessionService.sessionMap(userId,request.getSession().getId());
		//登录并创建SlSession存放ThreadLocal
		RetMsg<Serializable> key = sessionService.login(postObj, loginListener);
		if (RetMsgHelper.isOk(key)) {
			//设置SlSession存入httpSession中,
			SessionStaticUtil.setSessionByHttp(request, SessionStaticUtil.getSlSessionThreadLocal());
			//登录成功更新登录信息
			postObj.put("isOnline", "1");
			postObj.put("latestIp", request.getSession().getId());
			sessionService.updateUserLoginInfo(postObj);
		}
		return key;
	}

	
	@ResponseBody
	@RequestMapping(value = "/checkLogin")
	public RetMsg<Serializable> checkLogin(HttpServletRequest request,@RequestBody Map<String, Object> postObj)
			throws CException {
		RetMsg<Serializable> key= RetMsgHelper.ok("error.system.0000");
		String userId = ParameterUtil.getString(postObj, "userId", null);
		//处理session,改为ehcache缓存存储
//		if (null != redissonCacheManager.get(SessionService.cacheName, userId)) {//是否存在当前用户绑定session
//			postObj.put("ip", HttpUtil.getIp(request));
//			key = sessionService.checkLogin(postObj);
//		}else {
//			return key;
//		}
		
		return key;
	}
	
/**
     * 登录系统
     * 用户名+密码的方式登录
     * 
     * @param request
     *            内置对象，还有response
     * @param postObj
     * @return
     * @throws CException
     *             可能的业务逻辑异常checked Exception，由父类controller统一处理
     */
    @ResponseBody
    @RequestMapping(value = "/singleLogin")
    public RetMsg<Serializable> singleLogin(HttpServletRequest request, @RequestBody Map<String, Object> postObj) throws CException {
        RetMsg<Serializable> key = sessionService.singleLogin(postObj, loginListener);
        if (RetMsgHelper.isOk(key)) {
            SessionStaticUtil.setSessionByHttp(request, SessionStaticUtil.getSlSessionThreadLocal());

        }
        return key;
    }
	/**
	 * 登录系统
	 * 
	 * @param request
	 *            内置对象，还有response
	 * @param postObj
	 * @return
	 * @throws CException
	 *             可能的业务逻辑异常checked Exception，由父类controller统一处理
	 */
	@ResponseBody
	@RequestMapping(value = "/empLogin")
	public RetMsg<Serializable> empLogin(HttpServletRequest request, @RequestBody Map<String, String> postObj) throws CException {
		RetMsg<Serializable> key = sessionService.empLogin(postObj, loginListener);
		if (RetMsgHelper.isOk(key)) {
			SessionStaticUtil.setSessionByHttp(request, SessionStaticUtil.getSlSessionThreadLocal());

		}
		return key;
	}

	/**
	 * 注销系统
	 * 
	 * @param request
	 *            http请求对象
	 * @author lihb
	 * @date 2016-7-21
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/logout")
	public RetMsg<Serializable> logout(HttpServletRequest request) {
		SlSession session = SessionStaticUtil.getSlSessionByHttp(request);
		RetMsg<Serializable> key = sessionService.logout(session);
		SessionStaticUtil.setSessionByHttp(request, null);
		TaUser taUser =(TaUser)session.get("User");
		//移除缓存数据
		sessionService.sessionMap(taUser.getUserId(),request.getSession().getId());
		//登录成功更新登录信息
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", taUser.getUserId());
		map.put("branchId", taUser.getBranchId());
		map.put("isOnline", "0");
		map.put("latestIp", HttpUtil.getIp(request));
		sessionService.updateUserLoginInfo(map);
		return key;
	}

	@ResponseBody
	@RequestMapping(value = "/swithRoleInst")
	public RetMsg<Serializable> swithRoleInst(@RequestBody Map<String, Object> map, HttpServletRequest request) {
		SlSession session = SessionStaticUtil.getSlSessionByHttp(request);
		RetMsg<Serializable> key = sessionService.switchRoleInst(session, map);
		if (RetMsgHelper.isOk(key)) {
			SessionStaticUtil.setSessionByHttp(request, SessionStaticUtil.getSlSessionThreadLocal());

		}
		return key;
	}

	/**
	 * 柜员密码重置(初始密码"123456")
	 * 
	 * @param userIds
	 *            - 用户ID password- 密码
	 * @author wang_chen
	 * @date 2016-7-26
	 */
	@ResponseBody
	@RequestMapping(value = "/resetUserPassword")
	public RetMsg<Serializable> resetUserPassword(@RequestBody Map<String, Object> map) throws RException {
		String[] userIds = ParameterUtil.getString(map, "userIds", "").split(",");
		String password = SHAUtil.SHA512(SystemProperties.empId_passwd.trim()).toUpperCase();
		JY.require(!(StringUtil.isNullOrEmpty(password)), "新密码不得为空.");
		userService.resetUserPwd(userIds, password);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 柜员密码全部重置(初始密码"123456")
	 * 
	 * @param
	 * - 用户ID password- 密码
	 * @author lsc
	 * @date 2019-7-2
	 */
	@ResponseBody
	@RequestMapping(value = "/resetUserPasswords")
	public RetMsg<Serializable> resetUserPasswords() throws RException {
		String password = SHAUtil.SHA512(SystemProperties.empId_passwd.trim()).toUpperCase();
		JY.require(!(StringUtil.isNullOrEmpty(password)), "新密码不得为空.");
		userService.resetUserPwds(password);
		return RetMsgHelper.ok();
	}

	/**
	 * 柜员密码修改
	 * 
	 * @param
	 * @author wang_chen
	 * @date 2016-7-26
	 */
	@ResponseBody
	@RequestMapping(value = "/changeUserPassword")
	public RetMsg<Serializable> changeUserPassword(@RequestBody Map<String, Object> map) throws RException {
		userService.changeUserPwd(map);
		return RetMsgHelper.ok();
	}

	/**
	 * 条件查询列表
	 * 
	 * @param user
	 *            - 用户对象
	 * @return 用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	@ResponseBody
	@RequestMapping(value = "/saveUserRole")
	public RetMsg<Serializable> saveUserRole(@RequestBody TaUserRoleMap userRoleMap) throws RException {
		userRoleMapService.saveUserRole(userRoleMap);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据审批角色查询柜员及角色
	 * 
	 * @param param
	 *            审批角色ID
	 * @author wang
	 * @date 2016-9-28
	 */
	@ResponseBody
	@RequestMapping(value = "/selectUserWithRoleName")
	public RetMsg<List<TaUser>> selectUserWithRoleName(@RequestBody Map<String, Object> param) {
		String flowRoleId = ParameterUtil.getString(param, "flowRoleId", "");
		List<TaUser> list = userService.selectUserWithRoleName(flowRoleId);
		return RetMsgHelper.ok(list);
	}

	/**
	 * 根据审批角色,机构ID 查询柜员及角色
	 * 
	 * @param param
	 *            审批角色ID
	 * @param param
	 *            机构ID
	 * @author lijing
	 * @date 2018-1-10
	 */
	@ResponseBody
	@RequestMapping(value = "/selectUserByRoleNameAndInstId")
	public RetMsg<PageInfo<TaUser>> selectUserByRoleNameAndInstId(@RequestBody Map<String, Object> param) {
		Page<TaUser> page = userService.selectUserByRoleNameAndInstId(param);
		return RetMsgHelper.ok(page);
	}

	/**
	 * 中转登录系统
	 * 
	 * @param request
	 *            内置对象，还有response
	 * @return
	 * @throws CException
	 *             可能的业务逻辑异常checked Exception，由父类controller统一处理
	 */
	@RequestMapping(value = "/transfer")
	@ResponseBody
	public boolean transfer(HttpServletRequest request, @RequestBody Map<String, String> postObj) throws CException {
		String userId = ParameterUtil.getString(postObj, "userId", "");
		String ip = request.getRemoteAddr();// 地址
		String uri = request.getRequestURI();// 员工号
		String agent = request.getHeader("user-agent");// 员工号
		boolean bool = sessionService.login(userId, ip, uri, agent);
		if (bool) {
			SessionStaticUtil.setSessionByHttp(request, SessionStaticUtil.getSlSessionThreadLocal());
		}
		return bool;
	}

	/**
	 * 查询用户详细信息
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchUserInfo")
	public RetMsg<PageInfo<TaUserInfo>> getUserInfo(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userInfoService.findUserInfo(map));
	}

	/**
	 * 将图片转换为二进制
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/upload", method = RequestMethod.POST,produces = "text/plain;charset=UTF-8")
	public String upload(MultipartHttpServletRequest request) throws Exception {
		// 上传用户工作照片
		String decode = userService.upload(request);
		String photoPath = null;
		if (decode != null) {
			//获取session中实时参数
			photoPath = userService.readImage(request);
		}
		return photoPath;
	}



	@ResponseBody
	@RequestMapping(value = "/waitApproveList")
	public RetMsg<PageInfo<TaUser>> getWaitApproveList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userService.getWaitApprovePage(map));
	}

	@ResponseBody
	@RequestMapping(value = "/approvedList")
	public RetMsg<PageInfo<TaUser>> getApprovedList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userService.getApprovedList(map));
	}

	@ResponseBody
	@RequestMapping(value = "/approveAdd")
	public RetMsg<Serializable> approveAdd(@RequestBody Map<String, Object> map) {
		userService.approveAdd(map);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/approveUpdate")
	public RetMsg<Serializable> approveUpdate(@RequestBody Map<String, Object> map) {
		userService.approveUpdate(map);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/isTotalManager")
	public RetMsg<Boolean> isTotalManager() {
		return RetMsgHelper.ok(userService.isTotalManager(SlSessionHelper.getUserId()));
	}

	@ResponseBody
	@RequestMapping(value = "/checkUserByBranchId")
	public RetMsg<Boolean> checkUserByBranchId(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userService.checkUserByBranchId(map));
	}

}
