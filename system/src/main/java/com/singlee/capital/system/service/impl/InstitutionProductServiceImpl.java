package com.singlee.capital.system.service.impl;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.TcInstitutionProductMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TcInstitutionProduct;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class InstitutionProductServiceImpl implements InstitutionProductService {
	
	@Autowired
	TcInstitutionProductMapper tcInstitutionProductMapper;
	@Autowired
	TtInstitutionMapper ttInstitutionMapper;

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void addInstProduct(Map<String, Object> map) {
		String prdNoList = ParameterUtil.getString(map, "prdNoList", "");
		String[] prdNoArray = prdNoList.split(",");
		String instId = ParameterUtil.getString(map, "instId", "");
		TcInstitutionProduct tcInstitutionProduct = new TcInstitutionProduct();
		tcInstitutionProduct.setInstId(instId);
		tcInstitutionProductMapper.deleteInstitutionProduct(instId);
		for(int i = 0; i < prdNoArray.length; i++) {
			tcInstitutionProduct.setPrdNo(prdNoArray[i]);
			tcInstitutionProductMapper.insert(tcInstitutionProduct);
		}
	}

	@Override
	public String getProNames(Map<String, Object> map) {
		List<String> prdNameList = tcInstitutionProductMapper.getPrdNames(map);
		String prdNames = "";
		if(!prdNameList.isEmpty()) {
			for(int i = 0; i < prdNameList.size(); i++) {
				if(i == prdNameList.size() - 1) {
					prdNames += prdNameList.get(i);
				} else {
					prdNames += prdNameList.get(i) + ",";
				}
			}
		}
		return prdNames;
	}

	@Override
	public String getInstName(Map<String, Object> map) {
		String instId = ParameterUtil.getString(map, "instId", "");
		TtInstitution institution = ttInstitutionMapper.selectByPrimaryKey(instId);
		return institution == null ? "" : institution.getInstName();
	}
}
