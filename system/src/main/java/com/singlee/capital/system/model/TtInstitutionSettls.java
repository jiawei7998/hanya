package com.singlee.capital.system.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "tt_institution_settls")
@Entity
public class TtInstitutionSettls implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY,generator="SELECT SEQ_TT_INST_SETTL.nextval FROM DUAL")
	private String settlId;
	
	private String instId;
	
	private String instFullname;
	
	private String busiType;
	
	private String instAcctNm;
	
	private String instAcctNo;
	
	private String instAcctBknm;
	
	private String instAcctPbocNo;
	
	private String instRemark;
	
	private String isActive;
	
	private String lastUpdate;
	
	private String userId;
	
	private String userInstId;
	
	private String ccy;
	
	private String mediumType;
	
	private String payerAccType;
	
	
	
	
	
	
	public String getPayerAccType() {
		return payerAccType;
	}

	public void setPayerAccType(String payerAccType) {
		this.payerAccType = payerAccType;
	}

	public String getMediumType() {
		return mediumType;
	}

	public void setMediumType(String mediumType) {
		this.mediumType = mediumType;
	}

	@Transient
	private String userInstName;

	public String getSettlId() {
		return settlId;
	}

	public void setSettlId(String settlId) {
		this.settlId = settlId;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getInstFullname() {
		return instFullname;
	}

	public void setInstFullname(String instFullname) {
		this.instFullname = instFullname;
	}

	public String getBusiType() {
		return busiType;
	}

	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}

	public String getInstAcctNm() {
		return instAcctNm;
	}

	public void setInstAcctNm(String instAcctNm) {
		this.instAcctNm = instAcctNm;
	}

	public String getInstAcctNo() {
		return instAcctNo;
	}

	public void setInstAcctNo(String instAcctNo) {
		this.instAcctNo = instAcctNo;
	}

	public String getInstAcctBknm() {
		return instAcctBknm;
	}

	public void setInstAcctBknm(String instAcctBknm) {
		this.instAcctBknm = instAcctBknm;
	}

	public String getInstAcctPbocNo() {
		return instAcctPbocNo;
	}

	public void setInstAcctPbocNo(String instAcctPbocNo) {
		this.instAcctPbocNo = instAcctPbocNo;
	}

	public String getInstRemark() {
		return instRemark;
	}

	public void setInstRemark(String instRemark) {
		this.instRemark = instRemark;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserInstId() {
		return userInstId;
	}

	public void setUserInstId(String userInstId) {
		this.userInstId = userInstId;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getUserInstName() {
		return userInstName;
	}

	public void setUserInstName(String userInstName) {
		this.userInstName = userInstName;
	}
}
