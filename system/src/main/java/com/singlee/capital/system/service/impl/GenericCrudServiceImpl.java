package com.singlee.capital.system.service.impl;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.service.GenericCrudService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;


/**
 * 机构 服务 
 * @author x230i
 *
 */
@Service
public class GenericCrudServiceImpl implements GenericCrudService{

	
	@SuppressWarnings("unchecked")
	private <T> Mapper<T> getMapper(String mapperName){
		Class<?> clz;
		try {
			clz = Class.forName(mapperName);
			return (Mapper<T>) SpringContextHolder.getBean(clz);
		} catch (ClassNotFoundException e) {
			JY.raiseRException(" class not found %s " , mapperName);
			return null;
		}
	}


	private Object populate(String beanName, Map<String, Object> map) {
		try {
			Class<?> clz = Class.forName(beanName);
			Object o = clz.newInstance();
			BeanUtil.populate(o, map);
			return o;
		} catch (Exception e) {
			throw new RException(e);
		}
	}

	@Override
	public List<Object> page(String beanName, String mapperName, Map<String, String> map) {
		return getMapper(mapperName).selectAll();
	}

	@Override
	public <T> void add(String beanName, String mapperName, Map<String, Object> map) {
		getMapper(mapperName).insert(populate(beanName, map));
	}

	@Override
	public <T> void mod(String beanName, String mapperName, Map<String, Object> map) {
		getMapper(mapperName).updateByPrimaryKey(populate(beanName, map));
	}

	@Override
	public <T> void rem(String beanName, String mapperName, Map<String, Object> map) {
		getMapper(mapperName).deleteByPrimaryKey(populate(beanName, map));
	}

	
	
}
