package com.singlee.capital.system.controller;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户相关
 * @author Lyon Chen 
 */
@Controller
@RequestMapping(value = "/InstitutionController")
public class InstitutionController extends CommonController {

	@Autowired
	private InstitutionService institutionService;

	@ResponseBody
	@RequestMapping(value = "/searchInstitution")
	public RetMsg<List<TtInstitution>> searchInstitution() throws RException {
		List<TtInstitution> list = institutionService.mergedTreeInstitution();			
		return RetMsgHelper.ok(list);
	}
	
	//searchInstitutions 查询机构信息
		@ResponseBody
		@RequestMapping(value = "/searchInstitutions")
		public RetMsg<List<TtInstitution>> searchInstitutions(@RequestBody HashMap<String, Object> map) throws RException {
			List<TtInstitution> list = institutionService.mergedTreeInstitutions(map);		
			System.out.println("list结果为"+list);
			return RetMsgHelper.ok(list);
		}
	/**
	 * 根据银行Id  查询机构列表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchInstitutionByBranchId")
	public RetMsg<List<TtInstitution>> searchInstitutionByBranchId(@RequestBody HashMap<String, Object> map) throws RException {
		List<TtInstitution> list = institutionService.searchInstitutionByBranchId(map);			
		return RetMsgHelper.ok(list);
	}
	/**
	 * 根据机构Id 查询机构信息getInstById
	 */
	@ResponseBody
	@RequestMapping(value = "/getInstById")
	public RetMsg<TtInstitution> getInstById(@RequestBody HashMap<String, Object> map) throws RException{
		return RetMsgHelper.ok(institutionService.getInstById(ParameterUtil.getString(map, "instId", "")));
	}
	
	/**
	 * 根据登录机构（包含子机构）
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchChildrenInstitution")
	public RetMsg<List<TtInstitution>> searchChildrenInstitution(@RequestBody HashMap<String, Object> map) throws RException {
		map.put("instId", SlSessionHelper.getInstitutionId());
		List<TtInstitution> list = institutionService.searchByInstId(map);	
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/getTtInstitutionByUserId")
	public RetMsg<List<TtInstitution>> getTtInstitutionByUserId(@RequestBody HashMap<String, Object> map) throws RException {
		List<TtInstitution> list=null;
		if(map.get("userId")!=null){
			list=institutionService.getInstitutionByUserId(map);
		}
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/searchInstByUserId")
	public RetMsg<List<String>> searchInstByUserId(@RequestBody HashMap<String, Object> map) throws RException {
		List<String> list=institutionService.searchInstByUserId(map);
		return RetMsgHelper.ok(list);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/getTtInstitutionByRoleId")
	public RetMsg<List<TtInstitution>> getTtInstitutionByRoleId(@RequestBody HashMap<String, Object> map) throws RException{
		List<TtInstitution> list=institutionService.getTtInstitutionByRoleId(map);
		return RetMsgHelper.ok(list);
	}
	

	
	@ResponseBody
	@RequestMapping(value = "/addInstitution")
	public RetMsg<Serializable> addInstitution(@RequestBody TtInstitution inst) throws RException {
		institutionService.addInstitution(inst);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/updateInstitution")
	public RetMsg<Serializable> updateInstitution(@RequestBody TtInstitution inst) throws RException {
		TtInstitution oldInstitution = institutionService.getInstitutionById(inst.getInstId());
		institutionService.updateInstitutionAndOld(inst,oldInstitution);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteInstitution")
	public RetMsg<Serializable> deleteInstitution(@RequestBody HashMap<String,Object> map) throws RException {
		institutionService.deleteInstitution(ParameterUtil.getString(map, "instId", ""));
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/updateInstitutionStatus")
	public RetMsg<Serializable> updateInstitutionStatus(@RequestBody HashMap<String,Object> map) throws RException {
		institutionService.updateInstitutionStatus(ParameterUtil.getString(map, "instId", ""));
		return RetMsgHelper.ok();
	}
	
	/**
	 * 根据登录机构(总行还是分行划分权限)
	 * @return
	 * @throws RException
	 */
	@ResponseBody
	@RequestMapping(value = "/searchClassifyInstitution")
	public RetMsg<List<TtInstitution>> searchClassifyInstitution(@RequestBody HashMap<String, Object> map) throws RException {
		String instType = SlSessionHelper.getInstitutionType();
		List<TtInstitution> list;
		if("0".equals(instType)){
			 list = institutionService.mergedTreeInstitution();	
		}else{
			map.put("instId", SlSessionHelper.getInstitutionId());
		    list = institutionService.searchByInstId(map);	
		}		
		return RetMsgHelper.ok(list);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getFtaInst")
	public RetMsg<List<TtInstitution>> getFtaInstitution(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(institutionService.getFtaInstitution(map));
	}
	
	/**
	 * 根据输入的用户名查询所属机构
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getInstList")
	public RetMsg<List<TtInstitution>> getInstitutionList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(institutionService.getInstListByUser(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/searchFtaInst")
	public RetMsg<List<TtInstitution>> searchFtaInst(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(institutionService.searchFtaInstList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/getInstByUser")
	public RetMsg<List<TtInstitution>> getInstByUser(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(institutionService.getInstByUser(map));
	}
	
}
