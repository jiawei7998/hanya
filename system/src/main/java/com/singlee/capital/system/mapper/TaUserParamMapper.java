package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.capital.system.model.TaUserParam;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 用户参数dao
 * @author Libo
 *
 */
public interface TaUserParamMapper extends MyMapper<TaUserParam>{

	/**
	 * 查询用户参数  分页信息
	 * @param params
	 * @param rowBounds
	 * @return
	 */
	public Page<TaUserParam> pageTaUserParams(Map<String, Object> params, RowBounds rowBounds);
	
	/**
	 * 查询用户参数  普通列表查询
	 * @param params
	 * @return
	 */
	public List<TaUserParam> searchTaUserParam(Map<String, Object> params);
	
	/**
	 * 查询用户参数对象
	 * @param param
	 * @return
	 */
	public TaUserParam searchTaUserParam(TaUserParam param);
	
	/**
	 * 删除用户参数
	 * @param userParam
	 */
	public void delTaUserParam(String p_id);
	
	/**
	 * 查询用户复核参数  分页信息
	 * @param params
	 * @param rowBounds
	 * @return
	 */
	public Page<TaUserParam> pageTaUserParamsVerify(Map<String, Object> params, RowBounds rowBounds);
	
}
