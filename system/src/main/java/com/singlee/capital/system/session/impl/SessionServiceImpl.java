package com.singlee.capital.system.session.impl;

import com.singlee.capital.base.model.Greeting;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.session.LoginListener;
import com.singlee.capital.common.session.SessionService;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.*;
import com.singlee.capital.system.dict.ApproveStatusConstants;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.mapper.TaBrccMapper;
import com.singlee.capital.system.mapper.TaRoleMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.*;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.service.RoleService;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.service.UserService;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.spring.session.RedissonSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.session.Session;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
 * session服务 实现
 *
 * @author cz
 */
@Service("sessionService")
public class SessionServiceImpl implements SessionService {

    private static String[] NO_INTERCEPT_URL;

    static {
        PropertiesConfiguration config;
        try {
            config = PropertiesUtil.parseFile("NO_INTERCEPT_URL.properties");
            NO_INTERCEPT_URL = config.getString("NO_INTERCEPT_URL").split(";");
            /*
             * for (String s : NO_INTERCEPT_URL) { System.out.println(s); }
             */
        } catch (Exception e) {
            JY.error(e);
            JY.raise("LOAD NO_INTERCEPT_URL.properties 错误:" + e.getMessage());
        }
    }

    @Autowired
    private RoleService rs;
    @Autowired
    private UserService us;
    @Autowired
    private InstitutionService is;
    @Autowired
    private TaRoleMapper roleMapper;
    @Autowired
    private TaUserMapper userMapper;
    @Autowired
    private TaBrccMapper brccMapper;
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private RedissonSessionRepository redissonSessionRepository;
    @Autowired
    private SimpMessageSendingOperations simpMessageSendingOperations;

    @Override
    @AutoLogMethod(value = "登录系统")
    public RetMsg<Serializable> login(Map<String, Object> params, LoginListener loal) {
        // 1.校验前台机构、角色、密码是否输入。因为用户在之前已经输入过了
        // 用户机构
        String instId = ParameterUtil.getString(params, "instId", null);
        // 用户角色
        String roleId = ParameterUtil.getString(params, "roleId", null);
        // 用户密码
        String passwd = ParameterUtil.getString(params, "passwd", null);
        // branchId和用户,用户放置在session中
        String branchId = ParameterUtil.getString(params, "branchId", null);
        @SuppressWarnings("unused")
        String checkCode = ParameterUtil.getString(params, "checkCode", null);
        // 2.当调用该方法时说明branchId和用户已经不为空了
		/*if (StringUtils.isEmpty(instId) || StringUtils.isEmpty(roleId) || StringUtils.isEmpty(passwd)) {
			return RetMsgHelper.simple("error.system.0001", "机构/角色/密码为空");
		}*/
        // 3.有效性、密码、用户状态校验 （包括登录错误次数、等等）
        TaUser tu = us.getUserByIds(params);
        if (!StringUtils.isEmpty(passwd)) {
            if (tu == null || !tu.getUserPwd().equalsIgnoreCase(MD5Util.MD5(tu.getUserId() + passwd.toUpperCase())) || ApproveStatusConstants.APPROVE_ING.equals(tu.getApproveStatus())) {
                return RetMsgHelper.simple("error.system.0002", "机构/角色/密码参数错误,未找到用户");
            }
        } else {
            if (tu == null || ApproveStatusConstants.APPROVE_ING.equals(tu.getApproveStatus())) {
                return RetMsgHelper.simple("error.system.0002", "机构/角色/密码参数错误,未找到用户");
            }
        }

        if (!ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tu.getIsActive())) {
            return RetMsgHelper.simple("error.system.0002", "用户已被停用，不允许登录");
        }
        if (!ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tu.getIsAccess())) {
            return RetMsgHelper.simple("error.system.0002", "用户不允许直接通过loginjsp登陆界面登录！必须授权！");
        }
        // 4 是否已经登录校验 TODO

        // 5 进行数据更新，并获取需要存放到 slsession 中的数据 TODO
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("branchId", branchId);
        map.put("instId", instId);
        TtInstitution in = is.getInstByBranchId(map);

        // 表中检查是否存在
        if (in == null) {
            return RetMsgHelper.simple("error.system.0002", "请选择机构");
        }
        tu.setInstName(in.getInstFullname());
        tu.setInstId(in.getInstId());// 当前用户登入机构
        tu.setRoles(roleId);// 当前用户登入角色
        tu.setBranchId(branchId);// 当前用户输入那个行的
        // 查询系统配置信息表
        TaBrcc brcc = brccMapper.selectByBranchId(branchId);
        SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
        // 是否匿名
        SlSessionHelper.setAnonymity(session, false);
        // 用户信息
        SlSessionHelper.setUser(session, tu);
        // 机构信息
        SlSessionHelper.setInstitution(session, in);
        // BRCC信息
        SlSessionHelper.setBrcc(session, brcc);

        if (loal != null) {
            loal.loginOk(session);
        }
        SessionStaticUtil.holdSlSessionThreadLocal(session);
        // 返回index
        return RetMsgHelper.ok(brcc.getSystemHome(), tu.getUserId(), tu);
    }

    @Override
    @AutoLogMethod(value = "登录系统", id = "0001")
    public RetMsg<Serializable> loginSimple(Map<String, Object> params, LoginListener loal) {
        String branchId = ParameterUtil.getString(params, "branchId", null);
        String passwd = ParameterUtil.getString(params, "passwd", null);
        //1.根据用户检查是否存在根据userId和BranchdId检查
        int ckRet = userMapper.checkUserByBranchId(params);
        if (ckRet != 1) {//说明存在
            return RetMsgHelper.simple("error.system.0002", "用户/密码错误，请重新输入!");
        }
        //2.根据用户查询机构用户机构信息userId和BranchdId 查询信息
        List<TtInstitution> instLst = is.getInstByUser(params);
        if (instLst.size() == 0) {
            return RetMsgHelper.simple("error.system.0002", "未分配机构，请重新分配!");
        }
        params.put("instId", instLst.get(0).getInstId());
        //3.根据用户/机构/部门信息查询 userId instId branchId
        List<TaRole> roleLst = rs.getRoleByUserIdAndInstId(params);
        if (roleLst.size() == 0) {
            return RetMsgHelper.simple("error.system.0002", "未分配角色，请重新分配!");
        }
        //3.1转换角色IDs
        List<String> roleIdsLst = new ArrayList<>();
        for (TaRole taRole : roleLst) {
            roleIdsLst.add(taRole.getRoleId());
        }
        params.put("roleIds", roleIdsLst);
        //4.开始登录操作-查询用户
        TaUser tu = us.getUserByIds(params);
        if (!StringUtils.isEmpty(passwd)) {
            if (tu == null || !tu.getUserPwd().equalsIgnoreCase(MD5Util.MD5(tu.getUserId() + passwd.toUpperCase())) || ApproveStatusConstants.APPROVE_ING.equals(tu.getApproveStatus())) {
                return RetMsgHelper.simple("error.system.0002", "用户/密码错误,请重新输入!");
            }
        } else {
            if (tu == null || ApproveStatusConstants.APPROVE_ING.equals(tu.getApproveStatus())) {
                return RetMsgHelper.simple("error.system.0002", "用户/密码错误,请重新输入!");
            }
        }
        if (!ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tu.getIsActive())) {
            return RetMsgHelper.simple("error.system.0002", "用户已被停用，不允许登录");
        }
        if (!ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tu.getIsAccess())) {
            return RetMsgHelper.simple("error.system.0002", "用户不允许直接通过loginjsp登陆界面登录！必须授权！");
        }
        // 4 是否已经登录校验 TODO

        // 5 进行数据更新，并获取需要存放到 slsession 中的数据 TODO
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("branchId", branchId);
        map.put("instId", params.get("instId"));
        TtInstitution in = is.getInstByBranchId(map);

        // 表中检查是否存在
        if (in == null) {
            return RetMsgHelper.simple("error.system.0002", "请选择机构");
        }
        tu.setInstName(in.getInstFullname());
        tu.setInstId(in.getInstId());// 当前用户登入机构
        tu.setRoleIdList(roleIdsLst);// 当前用户登入角色
        tu.setBranchId(branchId);// 当前用户输入那个行的
        // 查询系统配置信息表
        TaBrcc brcc = brccMapper.selectByBranchId(branchId);
        SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
        // 是否匿名
        SlSessionHelper.setAnonymity(session, false);
        // 用户信息
        SlSessionHelper.setUser(session, tu);
        // 机构信息
        SlSessionHelper.setInstitution(session, in);
        // BRCC信息
        SlSessionHelper.setBrcc(session, brcc);
        //机构内部帐户【已屏蔽】
        //		if (loal != null) {
        //			loal.loginOk(session);
        //		}
        SessionStaticUtil.holdSlSessionThreadLocal(session);
        // 返回index
        return RetMsgHelper.ok(brcc.getSystemHome(), tu.getUserId(), tu);
    }

    @AutoLogMethod("登录系统")
    @Override
    public RetMsg<Serializable> singleLogin(Map<String, Object> params, LoginListener loal) {
        String passwd = ParameterUtil.getString(params, "passwd", null);

        String branchId = ParameterUtil.getString(params, "branchId", null);

        String checkCode = ParameterUtil.getString(params, "checkCode", null);
        if (StringUtils.isEmpty(passwd)) {
            return RetMsgHelper.simple("error.system.0001", "密码为空");
        }
        TaUser tu = this.us.getUserByIds(params);
        if ((tu == null) || (!tu.getUserPwd().equalsIgnoreCase(passwd)) || ("1".equals(tu.getApproveStatus()))) {
            return RetMsgHelper.simple("error.system.0002", "密码参数错误,未找到用户");
        }
        if (!ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tu.getIsActive())) {
            return RetMsgHelper.simple("error.system.0006", "用户已被停用,不允许登录");
        }
        if (!ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tu.getIsAccess())) {
            return RetMsgHelper.simple("error.system.0002", "用户不允许直接通过loginjsp登录！必须授权！");
        }
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("branchId", branchId);
        map.put("instId", tu.getInstId());
        TtInstitution in = this.is.getInstByBranchId(map);

        HashMap<String, Object> roleMap = new HashMap<String, Object>();
        roleMap.put("userId", tu.getUserId());
        //需要角色进行拆分
        String roleId = this.userMapper.getAllRoleByUserID(roleMap);
        if (in == null) {
            return RetMsgHelper.simple("error.system.0002", "清算机构");
        }
        tu.setInstName(in.getInstFullname());
        tu.setInstId(in.getInstId());
        tu.setRoles(roleId);
        //角色列表
        tu.setRoleIdList(Arrays.asList(roleId));
        tu.setBranchId(branchId);

        TaBrcc brcc = this.brccMapper.selectByBranchId(branchId);

        SlSession session = SessionStaticUtil.getSlSessionThreadLocal();

        SlSessionHelper.setAnonymity(session, false);

        SlSessionHelper.setUser(session, tu);

        SlSessionHelper.setInstitution(session, in);

        SlSessionHelper.setBrcc(session, brcc);
        if (loal != null) {
            loal.loginOk(session);
        }
        SessionStaticUtil.holdSlSessionThreadLocal(session);

        return RetMsgHelper.ok(brcc.getSystemHome());
    }

    @Override
    @AutoLogMethod(value = "登录系统")
    public RetMsg<Serializable> empLogin(Map<String, String> params, LoginListener loal) {
        // 1 获得输入的用户名/工号
        String empId = ParameterUtil.getString(params, "empId", null);
        String instId = ParameterUtil.getString(params, "instId", "");
        @SuppressWarnings("unused")
        String checkCode = ParameterUtil.getString(params, "checkCode", null);
        if (StringUtils.isEmpty(empId)) {
            return RetMsgHelper.simple("error.system.0008");
        }

        // 2 校验码 TODO

        // 3 密码、用户状态校验 （包括登录错误次数、等等）
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("empId", empId);
        List<TaUser> tuList = userMapper.findByEmpId(map);
        TaUser tu = null;
        if (!tuList.isEmpty()) {
            tu = tuList.get(0);
        }
        if (tu == null || ApproveStatusConstants.APPROVE_ING.equals(tu.getApproveStatus())) {
            return RetMsgHelper.simple("error.system.0002");
        }
        if (!ExternalDictConstants.YesNo_YES.equalsIgnoreCase(tu.getIsActive())) {
            return RetMsgHelper.simple("error.system.0006", "用户已被停用，不允许登录");
        }

        // 4 是否已经登录校验 TODO

        // 5 进行数据更新，并获取需要存放到 slsession 中的数据 TODO
        TtInstitution in = is.getInstitutionById(instId);

        //
        if (in == null) {
            return RetMsgHelper.simple("111", "请选择机构");
        }
        tu.setInstName(in.getInstFullname());

        SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
        // 是否匿名
        SlSessionHelper.setAnonymity(session, false);
        TaUser oldUser = SlSessionHelper.getUser(session);
        if (!oldUser.getUserId().equalsIgnoreCase(tu.getUserId())) {
            return RetMsgHelper.simple("111", "原工号：" + oldUser.getUserId() + "被篡改为：" + tu.getUserId() + "不允许登陆！");
        }
        tu.setInstId(in.getInstId());
        SlSessionHelper.setUser(session, tu);
        SlSessionHelper.setInstitution(session, in);

        if (loal != null) {
            loal.loginOk(session);
        }
        SessionStaticUtil.holdSlSessionThreadLocal(session);

        map.put("userId", tu.getUserId());
        map.put("instId", instId);
        List<TaRole> role = roleMapper.findByUserIdAndInstId(map);
        if (null != role && role.size() > 0) {
            String indexPage = StringUtil.isEmpty(role.get(0).getIndexPage()) ? "index_approve.jsp" : role.get(0).getIndexPage();
            return RetMsgHelper.ok(indexPage);
        }
        return RetMsgHelper.ok("index_approve.jsp");
    }

    @Override
    public RetMsg<Serializable> logout(SlSession session) {
        // TODO 更新 最后登录时间 等
        @SuppressWarnings("unused")
        TaUser tu = SlSessionHelper.getUser(session);
        String iamgate = SystemProperties.iamgate_httpUrl;
        return RetMsgHelper.ok(iamgate);
    }

    @Override
    public SlSession anonymousSlSession(String ip, String uri, String agent) {
        SlSession s = SessionStaticUtil.createSlSession();
        SlSessionHelper.setAnonymity(s, true);
        SlSessionHelper.setIp(s, ip);
        SlSessionHelper.setUa(s, agent);
        SlSessionHelper.setUri(s, uri);
        return s;
    }

    @Override
    public boolean checkSlSession(SlSession session, String ip, String url, String agent) {
        /** 匿名 session 直接拦截 */
        if (SlSessionHelper.getAnonymity(session)) {
            return false;
        }
        TaUser tu = SlSessionHelper.getUser(session);
        if (tu == null) {
            JY.info("checkSlSession 没有找到 TaUser 在SlSession中");
            return false;
        }

        SlSessionHelper.setUri(session, url);
        SlSessionHelper.setIp(session, ip);
        SlSessionHelper.setUa(session, agent);

        return true;
    }

    @Override
    public boolean checkSlSessionUrlPermission(SlSession session, String url) {
        // TODO
        /**
         *
         */
        System.out.println(url);
        return true;
    }

    @Override
    public boolean checkSlSessionUrlFunction(SlSession session, String url, String functionId) {

        if (StringUtils.isEmpty(functionId) || StringUtils.isEmpty(url)) {
            return false;
        }
        // 如果是管理员，则直接返回校验ok
        if (isSystemAdmin(session)) {
            return true;
        }
        // 角色是否包含 模块菜单 功能点
        for (String roleId : getRoleIdList(session)) {
            TaRole role = rs.getRoleById(roleId);
            if (role == null || ExternalDictConstants.YesNo_NO.equals(role.getIsActive())) {
                continue; // 查看下一个角色
            }
            List<TaRoleFunctionMap> list = role.getFunctionMapList();
            if (list != null) {
                for (TaRoleFunctionMap trfm : list) {
                    if (url.equals(trfm.getModuleUrl()) && functionId.equals(trfm.getFunctionId())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean isNotNeedSessionInterceptor(String url) {
        if (StringUtils.isBlank(url)) {
            return false;
        } else {
            for (String s : NO_INTERCEPT_URL) {
                if (url.indexOf(s) > 0) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * 是否是 系统管理员
     *
     * @return
     */
    protected boolean isSystemAdmin(SlSession session) {
        TaUser u = SlSessionHelper.getUser(session);
        if (u != null && "0".equals(u.getInstId())) {
            return true;
        }
        return false;
    }

    /**
     * 所属的所有的角色列表（包含 禁用状态的）
     */
    protected List<String> getRoleIdList(SlSession session) {
        TaUser u = SlSessionHelper.getUser(session);
        if (u != null) {
            return u.getRoleIdList();
        } else {
            return null;
        }
    }

    @Override
    public boolean login(String userId, String ip, String uri, String agent) {
        return false;
    }

    @Override
    public RetMsg<Serializable> switchRoleInst(SlSession session, Map<String, Object> params) {

        String instId = ParameterUtil.getString(params, "instId", null);
        String roleId = ParameterUtil.getString(params, "roleId", null);

        TaUser taUser = SlSessionHelper.getUser();
        taUser.setInstId(instId);
        taUser.setRoles(roleId);

        // 用户信息
        SlSessionHelper.setUser(session, taUser);
        //
        SessionStaticUtil.holdSlSessionThreadLocal(session);
        return RetMsgHelper.ok();
    }

    //更新用户登录信息
    @Override
    public void updateUserLoginInfo(Map<String, Object> map) {
        //获取前台传输信息
        String userId = ParameterUtil.getString(map, "userId", "");
        String branchId = ParameterUtil.getString(map, "branchId", "");
        String isOnline = ParameterUtil.getString(map, "isOnline", "");
        String sessionId = ParameterUtil.getString(map, "sessionId", "");
        String latestIp = ParameterUtil.getString(map, "latestIp", "");
        TaUser user = new TaUser();
        user.setUserId(userId);
        user.setBranchId(branchId);
        user.setIsOnline(isOnline);//Y-在线 N-不在线
        user.setLatestIp(latestIp);
        user.setSessionId(sessionId);
        user.setLatestLoginTime(DateTimeUtil.getLocalDateTime());
        user.setLatestTime(DateTimeUtil.getLocalTime());
        userMapper.updateUserLoginInfo(user);
    }

    @Override
    public RetMsg<Serializable> checkLogin(Map<String, Object> postObj) {
        String userId = ParameterUtil.getString(postObj, "userId", null);
        String ip = ParameterUtil.getString(postObj, "ip", null);
        TaUser taUser = userMapper.selectUser(userId);
        if (taUser != null && taUser.getLatestIp() != null) {
            if ("1".equals(taUser.getIsOnline()) && !taUser.getLatestIp().equals(ip)) {
                return RetMsgHelper.ok("error.system.0001");
            }
        }
        return RetMsgHelper.ok("error.system.0000");
    }

    @Override
    public void sessionMap(String userId, String sessionId) {
        //获取所有缓存名称,处理包含session关键字的
        RMap<String, String> rMapSession = redissonClient.getMapCache(SessionService.SessionUser);
        //查看userId与sessionId的是否存在绑定【开始解除关系】
        String userSessionId = rMapSession.get(userId);
        //存入session
        rMapSession.put(userId, sessionId);
        rMapSession.expire(30 * 60, TimeUnit.SECONDS);
        //获取同一个浏览器登录的其他用户,除了当前用户【在解除关系之前发送】
        List<String> users = getKeys(userId, sessionId, rMapSession);
        if (null != users && users.size() > 0) {
            for (String user : users) {
                //发给除当前登录的新退出信息
                simpMessageSendingOperations.convertAndSendToUser(user, "/message", new Greeting("<a href='.././'>【" + user + "】与你在同一个浏览器登录<br>你已被迫下线>>>><a>",".././"));
                rMapSession.remove(user);
            }
        } else {
            //发给前面登录的人信息
            simpMessageSendingOperations.convertAndSendToUser(userId, "/message", new Greeting("<a href='.././'>【" + userId + "】已在其他地方登录<br>你已被迫下线>>>><a>",".././"));
        }
        //判断之前是否存在绑定关系
        if (null != userSessionId) {
            //已登录，获取登录的session 信息
            Session oldSession = redissonSessionRepository.findById(userSessionId);
            SlSession session = (SlSession) oldSession.getAttribute(SessionService.SessionKey);
            if (session != null) {
                //获取登录信息的userId
                TaUser taUser = (TaUser) session.get("User");
                //同一个浏览器只能保存一个session,如果相同则不进行删除了
                if (StringUtils.equals(taUser.getUserId(), userId)) {
                    oldSession.removeAttribute(SessionService.SessionKey);
                    redissonSessionRepository.deleteById(userSessionId);
                    redissonClient.getBucket("spring:session:" + userSessionId).delete();
                    JY.info("移除当前" + userSessionId + "的Session缓存信息");
                } else {
                    //删除前一次的记录信息即可
                    rMapSession.remove(taUser.getUserId());
                }
            } else {
                rMapSession.remove(userId);
            }
        }
    }

    @Override
    public void sendTopic(String userId, String content) {
        //查询到今天有还本付息数据,
        simpMessageSendingOperations.convertAndSendToUser(userId, "/message", new Greeting(content,""));
    }

    private List<String> getKeys(String userId, String value, Map<String, String> map) {
        ArrayList<String> keys = new ArrayList<String>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            //key与value 不等于当前的传入值
            if (StringUtils.equals(value, entry.getValue()) && !StringUtils.equals(userId, entry.getKey())) {
                keys.add(entry.getKey());
            } else {
                continue;
            }
        }
        return keys;
    }
}
