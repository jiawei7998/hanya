package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TtInstitution;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TtInstitutionMapper extends Mapper<TtInstitution> {
	
	Page<TtInstitution> pageInst(Map<String,Object> map, RowBounds rb);
	
	public List<TtInstitution> searchCanInstitution(HashMap<String, Object> map);
	
	public List<TtInstitution> searchChildrenInst(HashMap<String, Object> map);
	
	public List<TtInstitution> searchParentInst(HashMap<String, Object> map);

	public String queryInstId(String instName);
	public String queryInstName(String instId);
	public TtInstitution getInstById(String instId);
	List<TtInstitution> getTtInstitutionByRoleId(HashMap<String, Object> map);
	List<TtInstitution> getInstitutionAndPrdName();
	List<TtInstitution> searchInstitutionByBranchId(HashMap<String, Object> map);
	
	List<TtInstitution> getFtaInstitution(Map<String, Object> map);
	List<TtInstitution> getInstitutionByUserId(Map<String, Object> map);
	
	List<TtInstitution> getInstByUser(Map<String, Object> map);
	
	List<TtInstitution> getInstitutionAndPrdNames(HashMap<String, Object> map);
	
	TtInstitution getInstByBranchId(HashMap<String, Object> map);

	String getMaxInstId();

    void updateInstitutionStatus(String instId);
}