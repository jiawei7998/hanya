package com.singlee.capital.system.pojo;

import com.singlee.capital.system.model.TaFunction;

import java.util.List;

/**
 * RoleManage页面上树资源信息
 * @author gm
 *
 */
public class RoleModule {
	private String id;
	private String text;
	private List<TaFunction> attributes;
	private List<RoleModule> children;
	private String pid;
	private String deskId;
	
	
	
	public String getDeskId() {
		return deskId;
	}
	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}
	public List<TaFunction> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<TaFunction> attributes) {
		this.attributes = attributes;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public List<RoleModule> getChildren() {
		return children;
	}
	public void setChildren(List<RoleModule> children) {
		this.children = children;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RoleModule [id=");
		builder.append(id);
		builder.append(", text=");
		builder.append(text);
		builder.append(", attributes=");
		builder.append(attributes);
		builder.append(", children=");
		builder.append(children);
		builder.append(", pid=");
		builder.append(pid);
		builder.append("]");
		return builder.toString();
	}
	
}
