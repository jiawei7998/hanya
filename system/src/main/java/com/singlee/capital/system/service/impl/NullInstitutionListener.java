package com.singlee.capital.system.service.impl;

import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionListener;


public class NullInstitutionListener implements InstitutionListener{


	@Override
	public void addAfter(TtInstitution add) {
		
	}

	@Override
	public void modAfter(TtInstitution mod) {
		
	}

	@Override
	public void remAfter(TtInstitution rem) {
		
	}

}
