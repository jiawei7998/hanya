package com.singlee.capital.system.service;

import com.singlee.capital.common.log.LogHandler;
import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.system.model.TaLog;


public class SingleeLogHandler implements LogHandler{
	@Override
	public void logError(String value, String string, String mn, Throwable t) {
		SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
		TaLog log = LogFactory.getInstance().create(session, mn, string, mn);
		LogFactory.getInstance().handle(log, t);
	}

	@Override
	public void logOk(String value, String string, String mn, Object retVal) {
		SlSession session = SessionStaticUtil.getSlSessionThreadLocal();
		TaLog log = LogFactory.getInstance().create(session, mn, string, mn);
		LogFactory.getInstance().handle(log, retVal);
	}
	
}
