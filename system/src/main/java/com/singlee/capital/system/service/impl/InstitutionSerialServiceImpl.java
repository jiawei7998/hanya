package com.singlee.capital.system.service.impl;


import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.mapper.TtInstitutionSerialMapper;
import com.singlee.capital.system.model.TtInstitutionSerial;
import com.singlee.capital.system.service.InstitutionSerialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 机构  业务辖区 服务 
 * @author wangchen
 *
 */
@Service
public class InstitutionSerialServiceImpl  implements InstitutionSerialService{
	
	@Autowired
	private TtInstitutionSerialMapper ttInstitutionSerialMapper;
	@Autowired
	private BatchDao batchDao;
	
	@Override
	public List<String> selectSerialChildrenInst(String instId){
		List<String> result = ttInstitutionSerialMapper.searchSerialChildren(instId);
		return result;
	}
	
	@Override
	public void saveSerialChildrenInst(String instId, String serialChildInstIds){
		List<String> serialInstIdList = Arrays.asList(serialChildInstIds.split(","));
		List<TtInstitutionSerial> list = new ArrayList<TtInstitutionSerial>();
		//先清空后重新添加
		ttInstitutionSerialMapper.deleteSerialChildren(instId);
		for(String item : serialInstIdList){
			if(StringUtil.notNullOrEmpty(item)){
				TtInstitutionSerial ttInstitutionSerial = new TtInstitutionSerial();
				ttInstitutionSerial.setInstId(instId);
				ttInstitutionSerial.setSerialChildInstId(item);
				list.add(ttInstitutionSerial);
			}
		}
		batchDao.batch("com.singlee.capital.system.mapper.TtInstitutionSerialMapper.insert", list);
	}
}
