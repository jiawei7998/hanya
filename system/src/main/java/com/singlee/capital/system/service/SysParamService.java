package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaSysParam;

import java.util.Map;

public interface SysParamService {
	/**
	 * 新增系统参数
	 * @param sysParam			系统参数对象
	 */
	public void add(TaSysParam sysParam);
	
	/**
	 * 修改系统参数
	 * @param sysParam			系统参数对象
	 */
	public void modify(TaSysParam sysParam);
	
	/**
	 * 删除系统参数
	 * @param pCode			参数代码
	 * @param pValue       参数值
	 */
	public void delSysParam(String pCode,String pValue);
	/**
	 * 根据p_code 与 p_value查询系统参数
	 * @param p_code
	 * @param p_value
	 * @return
	 */
	public TaSysParam selectSysParam(String p_code,String p_value);
	/**
	 * 根据条件查询系统参数
	 * @param map
	 * map内参数：
	 * pCode     ：参数id 
	 * pName     ：参数名称
	 * pValue    ：参数值
	 * pType     ：参数类型
	 * pEditabled       ：参数是否可修改
	 * @return
	 */
	public Page<TaSysParam> getSysParamList(Map<String, Object> map);
	
	/**
	 * 根据示例对象上的属性查找系统参数
	 * @param sysParam
	 * @param pageNum	当前页码
	 * @param pageSize	条数
	 * @return
	 */
	public Page<TaSysParam> getSysParamList(TaSysParam sysParam);
	
	/**
	 * 修改系统参数
	 * @param sysParam
	 */
	public void modifySysParam(TaSysParam sysParam);
	
}
