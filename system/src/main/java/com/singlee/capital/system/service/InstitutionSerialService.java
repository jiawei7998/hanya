package com.singlee.capital.system.service;

import java.util.List;

/**
 * 机构  业务辖区 服务 
 * @author wangchen
 *
 */
public interface InstitutionSerialService {

	public List<String> selectSerialChildrenInst(String instId);

	public void saveSerialChildrenInst(String instId, String serialChildInstIds);

}
