package com.singlee.capital.system.service;

import com.singlee.capital.system.model.TtInstitution;


/**
 * 机构新增/修改/删除的通知处理 
 * @author x230i
 *
 */


public interface InstitutionListener {

	void addAfter(TtInstitution add);
	
	void modAfter(TtInstitution mod);
	
	void remAfter(TtInstitution rem);
	
	
}

