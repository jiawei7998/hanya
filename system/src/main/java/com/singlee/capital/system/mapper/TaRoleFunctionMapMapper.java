package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaRoleFunctionMap;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaRoleFunctionMapMapper extends Mapper<TaRoleFunctionMap> {
	
	/**
	 * 根据角色ID读取角色无权限的功能点ID
	 * @param map
	 * role_id:角色id(不允许为空)
	 * module_id :菜单id(不允许为空)
	 * @return
	 */
	public List<String> searchFunctionIdForRoleId(Map<String, Object> map);
	public List<String> searchFunctionIdByRoleIdAndModuleIdAndBranchId(Map<String, Object> map);
	
	public List<String> getFunctionByMid (Map<String, Object> map);
	
	public void deleteByRoleIdAndBranchId(Map<String, Object> map);
	
	public void addBatchByRoleFunctionMap(List<TaRoleFunctionMap> taFunction);
}