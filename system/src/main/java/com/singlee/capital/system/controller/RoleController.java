package com.singlee.capital.system.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.model.*;
import com.singlee.capital.system.pojo.RoleManageForSaveVo;
import com.singlee.capital.system.pojo.RoleModule;
import com.singlee.capital.system.service.RoleService;
import com.singlee.capital.system.service.UserRoleMapService;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 角色控制器层
 * @author lihuabing
 *
 */
@Controller
@RequestMapping(value = "/RoleController")
public class RoleController  extends CommonController {
	
	@Autowired
	private RoleService roleService;
	@Autowired
	private UserRoleMapService userRoleMapService;
	
    /**
     * 获取角色列表
     * @return
     */
//	@ResponseBody
//	@RequestMapping(value = "/searchAllRole")
//	public RetMsg<Pair<Object,Object>> getAllRole(TaUserRoleMap userRoleMap){
//		List<TaRole> list = roleService.getInstRoles(userRoleMap);
//		List<TaUserRoleMap> list1 = userRoleMapService.getAllUserRole(userRoleMap);
//		return RetMsgHelper.ok(list, list1);
//	}
//	
	@ResponseBody
	@RequestMapping(value = "/searchAllRole")
	public RetMsg<Pair<Object,Object>> getAllRole(@RequestBody Map<String, Object> map){
		List<TaRole> list = roleService.getInstRoles(map);
		TaUserRoleMap userRoleMap =new TaUserRoleMap();
		userRoleMap.setUserId((String)map.get("userId"));
		List<TaUserRoleMap> list1 = userRoleMapService.getAllUserRole(userRoleMap);
		return RetMsgHelper.ok(list, list1);
	}

	@ResponseBody
	@RequestMapping(value = "/searchAllRoleByInstID")
	public RetMsg<Pair<Object,Object>> searchAllRoleByInstID(@RequestBody Map<String, Object> map){
		List<TaRole> list = roleService.getInstRoles(map);

		List<TaRoleInstMap> list1 = roleService.getAllInstRole(map);

		return RetMsgHelper.ok(list, list1);
	}



	/**
	 * 根据机构Id 角色名称 查询角色内容
	 */
	@ResponseBody
	@RequestMapping(value = "/getRolesByInstAndRoleName")
	public RetMsg<PageInfo<TaRole>> getRolesByInstAndRoleName(@RequestBody Map<String, Object> map){
		Page<TaRole> list = roleService.getRolesByInstAndRoleName(map);
//		TaUserRoleMap taUserRoleMap = new TaUserRoleMap();
//		taUserRoleMap.setUserId((String)map.get("userId"));
//		List<TaUserRoleMap> list1 = userRoleMapService.getAllUserRole(taUserRoleMap);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 根据银行Id 查询改银行下所有角色
	 */
	@ResponseBody
	@RequestMapping(value = "/pageAllRoleBranchId")
	public RetMsg<PageInfo<TaRole>> pageAllRoleBranchId(@RequestBody Map<String, Object> map){
		Page<TaRole> list = roleService.pageAllRoleBranchId(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 根据角色Id 查询台子id 该角色有的台子功能
	 */
	@ResponseBody
	@RequestMapping(value = "/getHaveDeskByRoleIdAndInstId")
	public RetMsg<List<String>> getHaveDeskByRoleIdAndInstId(@RequestBody Map<String, Object> map){
		List<String> list =roleService.getHaveDeskByRoleIdAndInstId(map);
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/getAllModulesHaving")
	public RetMsg<List<String>> getAllModulesHaving(@RequestBody Map<String, Object> map){
		List<String> list =roleService.getAllModulesHaving(map);
		return RetMsgHelper.ok(list);
	}
	/**
	 * 分页查询角色列表
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/pageRole")
	public RetMsg<PageInfo<TaRole>> pageRole(@RequestBody Map<String, Object> map){
		Page<TaRole> list = roleService.pageRole(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 根据角色获取所拥有的权限 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchModuleIdForRoleId")
	public RetMsg<List<String>> searchModuleIdForRoleId(@RequestBody Map<String,Object> map){
		String roleId = ParameterUtil.getString(map,"roleId","");
		return RetMsgHelper.ok(roleService.searchModuleIdForRoleId(roleId));
	}
	
	/**
	 * 根据角色Id 查询机构 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchInstIdByRoleId")
	public RetMsg<List<String>> searchInstIdByRoleId(@RequestBody Map<String,Object> map){
		List<String> list =roleService.searchInstIdByRoleId(map);
		return RetMsgHelper.ok(list);
	}
	
	 /**
     * 获取角色列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/searchAllModule")
	public RetMsg<List<RoleModule>> searchAllModule(){
		List<RoleModule> retList = roleService.searchModuleForRole();
		return RetMsgHelper.ok(retList);
	}
	/**
	 * 根据银行号进行查询菜单列表
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllResourceByBranchId")
	public RetMsg<List<RoleModule>> getAllResourceByBranchId(@RequestBody Map<String,Object> map){
		List<RoleModule> list = roleService.getAllResourceByBranchId(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 保存角色信息
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveRole")
	public RetMsg<Serializable> saveRole(@RequestBody Map<String,Object> post){
		TaRole role = new TaRole();
		BeanUtil.populate(role, post);
		roleService.createRole(role);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/getFunc")
	public RetMsg<TaFunction> getFunc(@RequestBody Map<String,Object> post){
		TaFunction taFunction =new TaFunction();
		BeanUtil.populate(taFunction, post);
		TaFunction function = roleService.getFunc(taFunction);
		return RetMsgHelper.ok(function);
	}

	/**
	 * 保存功能点
	 * @param post
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveFunc")
	public RetMsg<Serializable> saveFunc(@RequestBody Map<String,Object> post){
		TaFunction taFunction =new TaFunction();
		BeanUtil.populate(taFunction, post);
		roleService.saveFunc(taFunction);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/updateFunc")
	public RetMsg<Serializable> updateFunc(@RequestBody Map<String,Object> post){
		TaFunction taFunction =new TaFunction();
		BeanUtil.populate(taFunction, post);
		if(StringUtil.isEmptyString(taFunction.getFunctionId())||StringUtil.isEmptyString(taFunction.getModuleId())){
			return RetMsgHelper.ok(RetMsgHelper.codeFail);
		}
		roleService.updateFunc(taFunction);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 更新角色信息
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateRole")
	public RetMsg<Serializable> updateRole(@RequestBody Map<String,Object> post){
		TaRole role = new TaRole();
		BeanUtil.populate(role, post);
		TaRole oldRole = roleService.getRoleById(role.getRoleId());
		roleService.updateRoleAndOld(role,oldRole);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 保存权限菜单和功能点
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveModuleAndFunction")
	public RetMsg<Serializable> saveModuleAndFunction(@RequestBody RoleManageForSaveVo roleManageForSaveVo) throws IOException{
//		RoleManageForSaveVo roleManageForSaveVo = new RoleManageForSaveVo();
//		BeanUtil.populate(roleManageForSaveVo, post);
		roleService.saveModuleAndFunctionForRole(roleManageForSaveVo);
		return RetMsgHelper.ok();
	}

	/**
	 * 根据角色Id 读取拥有该角色的柜员信息
	 * @param role_id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchUserByRoleId")
	public RetMsg<PageInfo<TaUser>> searchUserByRoleId(@RequestBody Map<String,Object> post) throws IOException{
		Page<TaUser> taUserList = roleService.searchUserByRoleId(post);
		return RetMsgHelper.ok(taUserList);
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteRole")
	public RetMsg<Serializable> deleteRole(@RequestBody Map<String, Object> map){
		roleService.deleteRole(map);
		return RetMsgHelper.ok();
	}
	/**
	 * 根据角色Id读取有权限的Module信息(包含有权限的功能点)
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	@ResponseBody
	@RequestMapping(value = "/searchModuleByRoleId")
	public List<HashMap<String, Object>> searchModuleInfoByRoleId(@RequestBody Map<String,Object> post) throws IOException{
		String roleId = ParameterUtil.getString(post,"roleId","");
		List<HashMap<String, Object>> retList = roleService.searchModuleInfoByRoleId(roleId);
		return retList;
	}


	@ResponseBody
	@RequestMapping(value = "/searchModuleAndFunctionByRoleId")
	public List<HashMap<String, List<TaFunction>>> searchModuleAndFunctionByRoleId(@RequestBody Map<String,Object> post) throws IOException{
		String roleId = ParameterUtil.getString(post,"roleId","");
		String branchId = ParameterUtil.getString(post,"branchId","");
		List<HashMap<String, List<TaFunction>>> retList = roleService.searchModuleAndFunctionByRoleId(roleId,branchId);
		return retList;
	}

	/**
	 * 保存角色菜单表(ta_role_desk_menu_map)--角色权限表(ta_role_module_map)--角色功能点表(ta_role_function_map)--更新ta_module表
	 * @param map
	 * @return
	 */
	
	@ResponseBody
	@RequestMapping(value = "/savaAllData")
	public RetMsg<Serializable> savaAllData(@RequestBody Map<String,Object> map){
		//台子数据
		List<TaRoleDeskMap> tadesk =FastJsonUtil.parseArrays(map.get("deskMemuSelected").toString(),TaRoleDeskMap.class);
		//权限数据
		List<TaRoleModuleMap> tamodule =FastJsonUtil.parseArrays(map.get("moduleSelected").toString(),TaRoleModuleMap.class);
		//function 功能点
		List<TaRoleFunctionMap> taFunction = FastJsonUtil.parseArrays(map.get("functionSelected").toString(),TaRoleFunctionMap.class);
		roleService.savaAllData(tadesk,tamodule,taFunction,map);
		return RetMsgHelper.ok();
	}
	/**
	 * 根据角色Id  菜单Id (deskid) 银行号 锁定该  菜单应该显示什么
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchModuleByRoleIdAndDeskIdAndBranchId")
	public List<HashMap<String, Object>> searchModuleByRoleIdAndDeskIdAndBranchId(@RequestBody Map<String,Object> map){
		List<HashMap<String, Object>> result=roleService.searchModuleByRoleIdAndDeskIdAndBranchId(map);
		return result;
	}
	
	@ResponseBody 
	@RequestMapping(value = "/searchModuleDetailByRoleIdAndDeskIdAndBranchId")
	public List<String> searchModuleDetailByRoleIdAndDeskIdAndBranchId(@RequestBody Map<String,Object> map){
		List<String> result=roleService.searchModuleDetailByRoleIdAndDeskIdAndBranchId(map);
		return result;
	}
	@ResponseBody 
	@RequestMapping(value = "/searchModuleDetailByAndDeskIdAndBranchId")
	public List<String> searchModuleDetailByAndDeskIdAndBranchId(@RequestBody Map<String,Object> map){
		List<String> result=roleService.searchModuleDetailByAndDeskIdAndBranchId(map);
		return result;
	}
	
	/**
	 * 根据银行Id 查询 functions getFunctionByBranchId
	 */
	@ResponseBody
	@RequestMapping(value = "/getFunctionByBranchId")
	public RetMsg<PageInfo<TaFunction>> getFunctionByBranchId(@RequestBody Map<String,Object> map){
		Page<TaFunction> functions=roleService.getFunctionByBranchId(map);
		return RetMsgHelper.ok(functions);
	}
	/**
	 * 根据 moduleId roleId branchid 获取功能点  
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value ="/getFunctionByRoleIdAndModuleId")
	public RetMsg<PageInfo<TaFunction>> getFunctionByRoleIdAndModuleId(@RequestBody Map<String,Object> map){
		Page<TaFunction> functions =roleService.getFunctionByRoleIdAndModuleId(map);
	    return RetMsgHelper.ok(functions);
	}
	
	/**
	 * 根据用户Id 机构Id 查询角色信息
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value ="/getRoleByUserIdAndInstIds")
	public RetMsg<PageInfo<TaRole>> getRoleByUserIdAndInstIds(@RequestBody Map<String,Object> map){
		return RetMsgHelper.ok(roleService.findByUserIdAndInstIdPag(map));
	}
	/**
	 * 复制角色
	 * @param vo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/copyRole")
	public RetMsg<Serializable> copyRole(@RequestBody RoleManageForSaveVo roleManageForSaveVo) throws IOException{
		roleService.copyRole(roleManageForSaveVo);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询父菜单信息
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchParModule")
	public List<RoleModule> searchParModule(HttpEntity<String> request){
		List<RoleModule> retList = roleService.searchParModule();
		return retList;
	}
	
	@ResponseBody
	@RequestMapping(value = "/waitApproveList")
	public RetMsg<PageInfo<TaRole>> getWaitApproveList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(roleService.getWaitApprovePage(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/approvedList")
	public RetMsg<PageInfo<TaRole>> getApprovedList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(roleService.getApprovedList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/approveAdd")
	public RetMsg<Serializable> approveAdd(@RequestBody Map<String, Object> map) {
		roleService.approveAdd(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/approveUpdate")
	public RetMsg<Serializable> approveUpdate(@RequestBody Map<String, Object> map) {
		roleService.approveUpdate(map);
		return RetMsgHelper.ok();
	}
	
	@ResponseBody
	@RequestMapping(value = "/getRoleByUserIdAndInstId")
	public RetMsg<List<TaRole>> getRoleByUserIdAndInstId(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(roleService.getRoleByUserIdAndInstId(map));
	}
	@ResponseBody
	@RequestMapping(value = "/getFunctionByMid")
	public RetMsg<List<String>> getFunctionByMid(@RequestBody Map<String, Object> map) {
		String roleIdStr = ParameterUtil.getString(map, "roleIds", null);
		map.put("roleIds", JSON.parseArray(roleIdStr,String.class));
		return RetMsgHelper.ok(roleService.getFunctionByMid(map));
	}
	@ResponseBody
	@RequestMapping(value = "/saveInstRole")
	public RetMsg<Serializable> saveInstRole(@RequestBody TaRoleInstMap  instRoleMap) throws RException {
		roleService.saveInstRole(instRoleMap);
		return RetMsgHelper.ok();
	}

	@ResponseBody
	@RequestMapping(value = "/getRoleListByInstId")
	public RetMsg<List<TaRole>> getRoleListByInstId(@RequestBody Map<String, Object> params) {
		return RetMsgHelper.ok(roleService.getRoleListByInstId(params));
	}

	@ResponseBody
	@RequestMapping(value = "/searchRoleByUserid")
	public RetMsg<List<TaUserRoleMap>> getRoleByUserid(@RequestBody Map<String, Object> map){
		TaUserRoleMap userRoleMap =new TaUserRoleMap();
		userRoleMap.setUserId(ParameterUtil.getString(map,"userId",""));
		List<TaUserRoleMap> list1 = userRoleMapService.getAllUserRole(userRoleMap);
		return RetMsgHelper.ok(list1);
	}
}
