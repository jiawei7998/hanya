package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * 角色拥有菜单权限对应关系
 * @author lyonchen
 */

@Entity
@Table(name = "TA_ROLE_MODULE_MAP")
public class TaRoleModuleMap implements Serializable {
	
	private static final long serialVersionUID = -1107628330209413905L;
	@Id
	private String roleId;//角色ID
	@Id
	private String moduleId;//模块ID
	private String accessFlag;//访问标志
	private String moduleUrl; //模块对应的URl 冗余存储
	private String deskId;//菜单Id
	@Transient
	private List<String> moduleList;

	public TaRoleModuleMap() {
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getAccessFlag() {
		return accessFlag;
	}
	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}
	public String getModuleUrl() {
		return moduleUrl;
	}
	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
	public String getDeskId() {
		return deskId;
	}
	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}
	public List<String> getModuleList() {
		return moduleList;
	}
	public void setModuleList(List<String> moduleList) {
		this.moduleList = moduleList;
	}
	
	
	
	
}
