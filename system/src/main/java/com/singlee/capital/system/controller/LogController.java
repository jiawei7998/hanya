package com.singlee.capital.system.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.system.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 日志相关
 * 
 */
@Controller
@RequestMapping(value = "/LogController")
public class LogController extends CommonController {
	
	@Autowired
	private LogService logService;

	/**
	 * 条件查询列表
	 * 
	 * @param map
	 * @return 日志对象列表
	 */
	@ResponseBody
	@RequestMapping(value = "/searchPageLog")
	public RetMsg<PageInfo<TaLog>> searchPageLog(@RequestBody Map<String,Object> params) throws RException {
		Page<TaLog> page = logService.pageLog(params);
		return RetMsgHelper.ok(page);
	}
	
}
