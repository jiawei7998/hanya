package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 员工信息表
 * 
 * @author liyimin
 * 
 */
@Entity
@Table(name = "TA_USER_INFO")
public class TaUserInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String userId;// 用户Id
	private String sex;// 性别
	private String birthDate;// 出生年月
	private String highestEducation;// 最高学历
	private String major;// 专业
	private String position;// 业务岗位
	private String interbankStartDate;// 同业业务从业起始年月
	private String workStartDate;// 工作起始年月
	private String businessDomain;// 我行业务领域
	private String employmentExperience;// 从业经历
	private String otherInstitution;// 服务其他机构
	private String certificateQualification;// 上岗证资格
	private String examinationResults;// 考试成绩（最新）
	private String qualificationEndDate;// 资格有效截止日
	private String isCertificate;// 是否有对公客户经理上岗证
	private String comm;// 备注
//	private String branchId;// 银行id

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getHighestEducation() {
		return highestEducation;
	}

	public void setHighestEducation(String highestEducation) {
		this.highestEducation = highestEducation;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getInterbankStartDate() {
		return interbankStartDate;
	}

	public void setInterbankStartDate(String interbankStartDate) {
		this.interbankStartDate = interbankStartDate;
	}

	public String getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(String workStartDate) {
		this.workStartDate = workStartDate;
	}

	public String getBusinessDomain() {
		return businessDomain;
	}

	public void setBusinessDomain(String businessDomain) {
		this.businessDomain = businessDomain;
	}

	public String getEmploymentExperience() {
		return employmentExperience;
	}

	public void setEmploymentExperience(String employmentExperience) {
		this.employmentExperience = employmentExperience;
	}

	public String getOtherInstitution() {
		return otherInstitution;
	}

	public void setOtherInstitution(String otherInstitution) {
		this.otherInstitution = otherInstitution;
	}

	public String getCertificateQualification() {
		return certificateQualification;
	}

	public void setCertificateQualification(String certificateQualification) {
		this.certificateQualification = certificateQualification;
	}

	public String getExaminationResults() {
		return examinationResults;
	}

	public void setExaminationResults(String examinationResults) {
		this.examinationResults = examinationResults;
	}

	public String getQualificationEndDate() {
		return qualificationEndDate;
	}

	public void setQualificationEndDate(String qualificationEndDate) {
		this.qualificationEndDate = qualificationEndDate;
	}

	public String getIsCertificate() {
		return isCertificate;
	}

	public void setIsCertificate(String isCertificate) {
		this.isCertificate = isCertificate;
	}

	public String getComm() {
		return comm;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

//	public String getBranchId() {
//		return branchId;
//	}
//
//	public void setBranchId(String branchId) {
//		this.branchId = branchId;
//	}

}
