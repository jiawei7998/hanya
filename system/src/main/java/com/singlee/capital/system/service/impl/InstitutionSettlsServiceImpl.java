package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.TtInstitutionSettlsMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitutionSettls;
import com.singlee.capital.system.service.InstitutionSettlsService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
public class InstitutionSettlsServiceImpl implements InstitutionSettlsService {
	
	@Autowired
	private TtInstitutionSettlsMapper institutionSettlsMapper;

	@Override
	public Page<TtInstitutionSettls> getPageSettls(Map<String, Object> map) {
		return institutionSettlsMapper.getPageInstSettls(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void saveInstitutionSettl(TtInstitutionSettls institutionSettls) throws RException {
		// 获取当前用户和机构
		TaUser user = SlSessionHelper.getUser();
		String instId = SlSessionHelper.getInstitutionId();
		if(user == null || instId == null) {
			JY.raiseRException("登陆信息失效，请重新登陆");
		}
		// 获取当前时间
		String lastUpdate = DateTimeUtil.getLocalDateTime();
		institutionSettls.setLastUpdate(lastUpdate);
		institutionSettls.setUserId(user.getUserId());
		institutionSettls.setUserInstId(instId);
		// 保存
		institutionSettlsMapper.insert(institutionSettls);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void deleteInstitutionSettl(Map<String, Object> map) throws RException {
		String settlId = ParameterUtil.getString(map, "settlId", "");
		TtInstitutionSettls institutionSettls = institutionSettlsMapper.selectByPrimaryKey(settlId);
		if(institutionSettls == null) {
			JY.raiseRException("机构清算不存在");
		}
		institutionSettlsMapper.deleteByPrimaryKey(settlId);
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void updateInstitutionSettl(TtInstitutionSettls institutionSettls) throws RException {
		TtInstitutionSettls institutionSettl = institutionSettlsMapper.selectByPrimaryKey(institutionSettls);
		if(institutionSettl == null) {
			JY.raiseRException("机构清算不存在");
		}
		// 获取当前用户和机构
		TaUser user = SlSessionHelper.getUser();
		String instId = SlSessionHelper.getInstitutionId();
		if(user == null || instId == null) {
			JY.raiseRException("登陆信息失效，请重新登陆");
		}
		// 获取当前时间
		String lastUpdate = DateTimeUtil.getLocalDateTime();
		institutionSettls.setLastUpdate(lastUpdate);
		institutionSettls.setUserId(user.getUserId());
		institutionSettls.setUserInstId(instId);
		institutionSettlsMapper.updateByPrimaryKey(institutionSettls);
	}

}
