package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.system.model.*;
import com.singlee.capital.system.pojo.RoleManageForSaveVo;
import com.singlee.capital.system.pojo.RoleModule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色服务-接口
 * @author lihuabing
 *
 */
public interface RoleService {
	/**
	 * 根据角色Id 获取已经拥有的modules
	 * @param map
	 * @return
	 */
	List<String> getAllModulesHaving(Map<String, Object> map);
	
	/**
	 * 
	 */
	List<String> searchModuleIdForRoleId(String roleId);
	List<String> searchModuleDetailByAndDeskIdAndBranchId(Map<String, Object> map);
	/**
	 * 
	 * @param map
	 * @return
	 */
	List<String> searchModuleDetailByRoleIdAndDeskIdAndBranchId(Map<String, Object> map);
	/**
	 * 获取function
	 * @param map
	 * @return
	 */
	Page<TaFunction> getFunctionByBranchId(Map<String, Object> map);
	/**
	 * 根据角色Id 机构Id 查询台子id 该角色在某机构下有的台子功能
	 * @param map
	 * @return
	 */

	List<String> getHaveDeskByRoleIdAndInstId(Map<String, Object> map);
	/**
	 * 根据roleId获取角色信息获得完整的 role对象
	 * @param roleId 角色ID
	 * @return
	 */
	TaRole getRoleById(String roleId);
	List<RoleModule> getAllResourceByBranchId(Map<String, Object> map);
	
	List<String> searchInstIdByRoleId(Map<String, Object> map);
	/**
	 * 根据银行Id 查询角色
	 */
	Page<TaRole> pageAllRoleBranchId(Map<String, Object> map);
	/**
	 * 根据机构Id 角色名称 查询角色信息
	 * @param map
	 * @return
	 */
	Page<TaRole> getRolesByInstAndRoleName(Map<String, Object> map);
	/**
	 * 获取功能点
	 * @param map
	 * @return
	 */
	Page<TaFunction> getFunctionByRoleIdAndModuleId(Map<String, Object> map);
	/**
	 * 分页查询角色列表
	 * @param pageData  查询条件
	 * @return
	 */
	Page<TaRole> pageRole(Map<String, Object> pageData);
	
	/**
	 * 查询所有角色信息
	 * @param role
	 * @return
	 */
	List<TaRole> getAllRole(TaRole role);
	/**
	 * 查询所有角色信息
	 * @param map
	 * @return
	 */
	List<TaRole> getRoleList(Map<String, Object> params);
	
	/**
	 * 查询角色资源信息
	 * @return
	 */
	List<RoleModule> searchModuleForRole();
	
	/**
	 * 根据角色Id 读取拥有该角色的柜员信息
	 * @param role_id
	 * @return
	 */
	public List<TaUser> searchUserByRoleId(String role_id);
	public Page<TaUser> searchUserByRoleId(Map<String, Object> map);
	
	/**
	 * 根据角色Id读取有权限的Module信息(包含有权限的功能点)
	 * @param roleId
	 * @return
	 */
	public List<HashMap<String, Object>> searchModuleInfoByRoleId(String roleId);

	/**
	 * 根据角色Id读取有权限的Module信息(包含有权限的功能点 全部信息)
	 * @param roleId
	 * @return
	 */
	public List<HashMap<String, List<TaFunction>>> searchModuleAndFunctionByRoleId(String roleId,String branchId);

	/**
	 *
	 * @param roleId
	 * @param branchId
	 * @return
	 */
	public List<TaFunction> searchFunctionByRoleId(String roleId,String branchId,String moduleId);
	
	public List<HashMap<String, Object>> searchModuleByRoleIdAndDeskIdAndBranchId(Map<String, Object> map);
	List<String> getFunctionByMid(Map<String, Object> map);
	/**
	 * 保存角色
	 * @param TaRole taRole
	 * @return
	 */
	public void createRole(TaRole taRole) throws RException;
	
	public void savaAllData(List<TaRoleDeskMap> tadesk,List<TaRoleModuleMap> tamodule,List<TaRoleFunctionMap> taFunction,Map<String, Object> map) throws RException;

	/**
	 * 根据角色id，保存该角色下有权限的菜单及功能点
	 * @param roleManageForSaveVo
	 */
	public void saveModuleAndFunctionForRole(RoleManageForSaveVo roleManageForSaveVo);
	
	/**
	 * 复制角色
	 * @param roleManageForSaveVo
	 */
	public void copyRole(RoleManageForSaveVo roleManageForSaveVo);

	/**
	 * 更新角色
	 * @param TaRole
	 */
	public void updateRoleAndOld(TaRole taRole,TaRole oldRole) throws RException;

	/**
	 * 查询父菜单信息
	 * @return
	 */
	List<RoleModule> searchParModule();
	
	/**
	 * 查询机构下所有子机构的权限
	 * @param userRoleMap
	 * @return
	 */
	//List<TaRole> getInstRoles(TaUserRoleMap userRoleMap);
	List<TaRole> getInstRoles(Map<String, Object> map);

	Page<TaRole> getWaitApprovePage(Map<String, Object> map);

	Page<TaRole> getApprovedList(Map<String, Object> map);

	TaFunction getFunc(TaFunction taFunction);
    void saveFunc(TaFunction taFunction);
	void updateFunc(TaFunction taFunction);
	void approveAdd(Map<String, Object> map);
	void deleteRole(Map<String, Object> map);
	void approveUpdate(Map<String, Object> map);
	Page<TaRole> findByUserIdAndInstIdPag(Map<String, Object> map);
	
	/**
	 * 根据用户+机构查询出角色
	 * @param map
	 * @return
	 */
	List<TaRole> getRoleByUserIdAndInstId(Map<String, Object> map);

	List<TaRoleInstMap> getAllInstRole(Map<String, Object> map);

	void saveInstRole(TaRoleInstMap instRoleMap);

	/**
	 * 根据reoleid查询角色树
	 * @param map
	 * @return
	 */
	List<TaRole> getRoleListByInstId(Map<String, Object> map);
}
