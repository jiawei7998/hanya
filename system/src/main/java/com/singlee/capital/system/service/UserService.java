package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaUser;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
/**
 * 用户管理service
 * @author lihb
 * @date 2016-07-06
 * @company 杭州新利科技有限公司
 */
public interface UserService {
	/**
	 * 根据银行Id 查询用户
	 * @param params
	 * @return
	 */
	Page<TaUser> searchUserByBranchId(Map<String,Object> params);
    List<String> verifyUniqueness(Map<String, Object> map);
	/**
	 * 根据主键查询对象
	 * 
	 * @param UserId - 用户主键
	 * @return 用户对象
	 * @author Hunter
	 * @date 2016-7-21
	 */
	TaUser getUserById(String UserId);

	/**
	 * 根据参数map分页查询列表
	 * 
	 * @param params - 参数map
	 * @return Page<TaUser> 分页用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	Page<TaUser> getPageUser(Map<String,Object> params);
	
	/**
	 * 根据参数map分页查询列表
	 * 
	 * @param params - 参数map
	 * @return Page<TaUser> 分页用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	Page<TaUser> getPageRoleUser(Map<String,Object> params);
	
	/**
	 * 新增用户
	 * 
	 * @param user - 用户对象
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void createUser(TaUser user);
	
	/**
	 * 修改用户
	 * 
	 * @param user - 用户对象
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void updateUser(TaUser user);
	
	/**
	 * 删除用户
	 * 
	 * @param userIdList - 用户主键列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void deleteUser(String[] userIdList);
	
	/**
	 * 启用/停用
	 * 
	 * @param userIdList - 用户主键列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void switchUserState(String[] userIdList);
	
	/**
	 * 重置用户密码
	 * 
	 * @param userid pwd
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void resetUserPwd(String[] userIdList, String userPwd);
	
	/**
	 * 修改用户密码
	 * 
	 * @param user - 用户对象
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void changeUserPwd(Map<String,Object> map);
	
	/**
	 * 根据审批角色查询柜员及角色
	 * 
	 * @param user - 用户对象
	 * @author wang
	 * @date 2016-9-28
	 */
	List<TaUser> selectUserWithRoleName(String flowRoleId);
	
	/**
	 * 根据审批角色,机构ID 查询柜员及角色
	 * 
	 * @param flowRoleId - 角色id
	 * @param instId - 机构id
	 * @author lijing
	 * @date 2018-1-10
	 */
	Page<TaUser> selectUserByRoleNameAndInstId(Map<String,Object> map);
	
	
	
	/**
	 * 查询用户个人信息
	 * @param map
	 * @return
	 */
	public TaUser searchTaUserbyMap(Map<String,Object> map);
	
	/**
	 * 查询用户个人信息
	 * @param String
	 * @return
	 */
	public TaUser queryUser(String User);
	
	/**
	 * 将图片转换为二进制
	 * @param request
	 * @return
	 */
	public String upload(MultipartHttpServletRequest request) throws Exception;
	
	/**
	 * 将二进制图片写到前台页面
	 * @param map
	 * @param response
	 */
	public String readImage(HttpServletRequest request);


	List<TaUser> selectUserWithInstIds(Map<String, Object> params);
	
	/**
	 * 查询用户对象列表(带分页)
	 * @param 
	 * @return 用户对象列表(带分页)
	 * @author lijing
	 * @date 2018-1-11
	 */
	public Page<TaUser> selectUserWithInstIdsPage(Map<String, Object> params);

	/**
	 * 查询待审批列表
	 * @return
	 */
	public Page<TaUser> getWaitApprovePage(Map<String, Object> map);
	
	/**
	 * 查询已审批列表
	 * @param map
	 * @return
	 */
	public Page<TaUser> getApprovedList(Map<String, Object> map);
	
	/**
	 * 通过新增审批申请
	 * @param map
	 */
	public void approveAdd(Map<String, Object> map);
	
	/**
	 * 通过修改审批申请
	 * @param map
	 */
	public void approveUpdate(Map<String, Object> map);
	
	public boolean isTotalManager(String userId);

	/**
	 * 加签候选人分页查询
	 * @param map
	 */
	public Page<TaUser> getPageInvitationUser(Map<String, Object> param);
	
	/**
	 * 检查用户是否存在
	 * @param map
	 * @return
	 */
	boolean checkUserByBranchId(Map<String, Object> map);
	/**
	 * 新登录功能，根据用户+机构+角色+用户密码
	 * @param map
	 * @return
	 */
	TaUser getUserByIds(Map<String, Object> map);
	
	public void updateUserByEsb(TaUser user);
	/**徽商OPICS项目组
	 * 根据APPROVE_USER_ID查询用户id和机构
	 * @param approveuserid
	 * @return
	 */
	TaUser getUserByApproveuserid(String approveUserId);
	/**
	 * 通过用户id查询br
	 * @param userId
	 * @return
	 */
	String getBrByUser(String userId);
	/**
	 * 柜员密码全部重置(初始密码"123456")
	 * 
	 * @param
	 * - 用户ID password- 密码
	 * @author lsc
	 * @date 2019-7-2
	 */
	void resetUserPwds(String password);
	
}
