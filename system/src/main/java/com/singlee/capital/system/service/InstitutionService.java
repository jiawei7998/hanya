package com.singlee.capital.system.service;

import com.singlee.capital.system.model.TtInstitution;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 机构 服务 
 * @author x230i
 *
 */
public interface InstitutionService { 
	public List<String> searchInstByUserId(HashMap<String, Object> map);
	/**
	 * 根据角色Id 查询机构信息
	 */
	public List<TtInstitution> getTtInstitutionByRoleId(HashMap<String, Object> map);

	/**
	 * 添加机构
	 */
	public void addInstitution(TtInstitution inst);
	/**
	 * 
	 * @param instId
	 * @return
	 */
	public TtInstitution getInstitutionById(String instId);
		
	/**
	 * 合并成一颗树的所有机构列表  
	 * @return
	 */
	public List<TtInstitution> mergedTreeInstitution();
	/**
	 * 根据银行Id 查询机构
	 * @param map
	 * @return
	 */
	public List<TtInstitution> searchInstitutionByBranchId(HashMap<String, Object> map);
	/**
	 * 根据登陆机构（包含子机构）
	 * @return
	 */
	public List<TtInstitution> searchByInstId(HashMap<String, Object> map);
	
	/**
	 * 某个机构instId下的所有子机构，递归
	 * @param instId
	 * @return
	 */
	public List<TtInstitution> childrenInstitution(String instId);

	/**
	 * 某个机构instId下的所有子机构的id ，递归排序
	 * @param instId
	 * @return
	 */
	public List<String> childrenInstitutionIds(String instId);
	
	/**
	 * 更新机构
	 */
	public void updateInstitution(TtInstitution inst);
	
	/**
	 * 删除机构
	 */
	public void deleteInstitution(String id);
	/**
	 * 根据用户机构（包含子机构）
	 * @return
	 */
	public List<TtInstitution> searchCanInstitution(HashMap<String, Object> map);

	public List<String> searchCanInstitutionIds(HashMap<String, Object> map);
	/**
	 * 权限能查看操作的机构
	 */
	public List<TtInstitution> searchChildrenInst(HashMap<String, Object> map);
	
	/**
	 * 根据机构名称查询机构编号
	 * @param instName
	 * @return
	 */
	public String queryInstId(String instName);
	
	public TtInstitution getInstById(String instId);
	
	/**
	 * 根据Map查询
	 * @param map
	 * @return
	 */
	public TtInstitution getInstByBranchId(HashMap<String, Object> map); 
	
	/**
	 * 查询机构下的自贸区机构
	 * @param map
	 * @return
	 */
	public List<TtInstitution> getFtaInstitution(Map<String, Object> map);
	
	/**
	 * 根据输入的用户名查询所属机构
	 * @param map
	 * @return
	 */
	public List<TtInstitution> getInstListByUser(Map<String, Object> map);
	/**
	 * 根据用户Id查询机构  一对多
	 */
	public List<TtInstitution> getInstitutionByUserId (Map<String, Object> map);
	/**
	 * 查询自贸区机构
	 * @return
	 */
	public List<TtInstitution> searchFtaInstList(Map<String, Object> map);
	
	
	public List<String> searchChildrenInstList(String instId);
	
	
	public List<String> searchTrueChildrenInstitutionIds(HashMap<String, Object> map);
	
	public  List<TtInstitution> searchParentInst(String instId);
	
	/**
	 * 根据用户查询所在的机构
	 * @param map
	 * @return
	 */
	public List<TtInstitution> getInstByUser(Map<String, Object> map);
	public List<TtInstitution> mergedTreeInstitutions(HashMap<String, Object> map);

	void updateInstitutionAndOld(TtInstitution inst, TtInstitution oldInstitution);

    void updateInstitutionStatus(String instId);
}
