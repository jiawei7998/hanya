package com.singlee.capital.system.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.model.TaRole;
import com.singlee.capital.system.model.TaUserRoleMapCopy;
import com.singlee.capital.system.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/UserRoleController")
public class UserRoleController extends CommonController {
	
	@Autowired
	private UserRoleService userRoleService;

	@ResponseBody
	@RequestMapping(value = "/waitApproveList")
	public RetMsg<PageInfo<TaUserRoleMapCopy>> getWaitApproveList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userRoleService.getWaitApproveList(map));
	}
	
	@RequestMapping(value = "/oldRoleList")
	@ResponseBody
	public RetMsg<List<TaRole>> getOldRoleList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userRoleService.getOldRoleList(map));
	}
	
	@RequestMapping(value = "/newRoleList")
	@ResponseBody
	public RetMsg<List<TaRole>> getNewRoleList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userRoleService.getNewRoleList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/approvedList")
	public RetMsg<PageInfo<TaUserRoleMapCopy>> getApprovedList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(userRoleService.getApprovedList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/doApprove")
	public RetMsg<Serializable> doApprove(@RequestBody Map<String, Object> map) {
		userRoleService.doApprove(map);
		return RetMsgHelper.ok();
	}
}
