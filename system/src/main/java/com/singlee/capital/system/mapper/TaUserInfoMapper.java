package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaUserInfo;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface TaUserInfoMapper extends Mapper<TaUserInfo> {

	Page<TaUserInfo> findUserInfoByUserId(Map<String, Object> map);
}
