package com.singlee.capital.system.pojo;

import java.util.List;

/**
 * 角色权限保存对象
 * @author gm
 *
 */
public class RoleManageForSaveVo {

	private String roleId;
	private List<Module> moduleList;//菜单数组
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public List<Module> getModuleList() {
		return moduleList;
	}
	public void setModuleList(List<Module> moduleList) {
		this.moduleList = moduleList;
	}
	

	
}
