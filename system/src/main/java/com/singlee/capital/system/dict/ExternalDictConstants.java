package com.singlee.capital.system.dict;








/**
 * 外部字典项
 * @author Libo
 *
 */
public class ExternalDictConstants {

	/**		000001 通用 是否 是否开关 */
	public static String YesNo_YES = "1"; /** 是 */
	public static String YesNo_NO = "0"; /** 否 */
   
	 /**   000099 自定义报表 显示位置 显示位置 */
	public static String ColumnAlign_left = "left"; /** 居左显示 */
	public static String ColumnAlign_center = "center"; /** 居中显示 */
	public static String ColumnAlign_right = "right";/** 居右显示 */
	
	/**   000097 自定义报表 自定义报表类型 自定义报表类型 */
	public static String  ReportType_JoyReport = "1";/** 系统报表 */
	public static String  ReportType_CustReport = "2";/** 客户报表 */
	
	/**   000092 自定义报表 Jasper查询控件类型 Jasper查询控件类型  */
	public static String  JasperSearchType_input = "input";/** 文本框 */
	public static String  JasperSearchType_hidden = "hidden";/** 隐藏文本 */
	public static String  JasperSearchType_dict = "dict";/** 字典下拉框(单选) */
	public static String  JasperSearchType_dictcheck = "dictcheck";/** 字典下拉框(多选) */
	public static String  JasperSearchType_datebox = "datebox";/** 日期控件 */
	public static String  JasperSearchType_dategroupbox = "dategroupbox";/** 日期组控件 */
	public static String  JasperSearchType_bondsearch = "bondsearch";/** 债券模糊查询 */
	public static String  JasperSearchType_acctree = "acctree";/** 账户树 */
	public static String  JasperSearchType_dbcombo = "dbcombo";/** 后台下拉框 */
	public static String  JasperSearchType_bondtypesearch = "bondtypesearch";/** 债券类型查询 */
	public static String  JasperSearchType_inst = "inst";	  /** 机构下拉框 */
	
	/**   InstitutionType 通用 机构类型 机构类别 */
	public static String InstitutionType_Head = "0";	/** 总行 */
	public static String InstitutionType_Branch = "1";	/** 分行 */
	public static String InstitutionType_Subbranch = "2";	/** 支行 */
	public static String InstitutionType_Department = "9";	  /** 部门 */

	/**   000138 交易 参数大类 参数大类	 */
	public static String ParameterBigKind_SystemParameter = "0";	  /** 系统参数 */
	public static String ParameterBigKind_UserParameter = "1";	  /** 用户参数 */
	
	/**   000139 交易 用户参数类型 用户参数类型  */
	public static String  UserParameterType_SystemConfigType = "1";/** 系统配置类型 */

	/**  DoubleApproveStatus 通用 双岗复核状态 双岗复核状态 */
	public static String  DoubleApproveStatus_WaitOperate = "1";/** 待经办 */
	public static String  DoubleApproveStatus_WaitApprove = "2";/** 待复核 */
	public static String  DoubleApproveStatus_Approvepass = "3";/** 复核通过 */
	public static String  DoubleApproveStatus_ApproveRefuse = "0";/** 复核拒绝 */
	
	/**
	 *   OperationType 通用 操作类型 操作类型
	 */
	public static String  OperationType_add = "add";/** 新增 */
	public static String  OperationType_edit = "edit";/** 修改 */
	public static String  OperationType_delete = "delete";/** 删除 */
	public static String  OperationType_imports = "imports";/** 导入 */
	public static String  OperationType_back = "back";/** 回退 */

	/**
	 *   DayendFlowStatus 日终-新版本 执行状态 日终执行状态
	 */
	public static String DayendFlowStatus_Waiting = "Waiting ";///** 等待执行 */
	public static String DayendFlowStatus_Executing = "Executing ";///** 执行中 */
	public static String DayendFlowStatus_Error = "Error ";///** 失败 */
	public static String DayendFlowStatus_Success = "Success";///** 正确完成 */
	public static String DayendFlowStatus_Warning = "Warning";///** 带告警的正确完成 */

	/**
	 *   DayendDateStatus 日终-新版本 业务日状态 业务日状态
	 */
	public static String DayendDateStatus_Normal = "Normal" ;	/** 正常状态(非日终) */
	public static String DayendDateStatus_Checked = "Checked"; 	    /** 日终准备 */
	public static String DayendDateStatus_Executing = "Executing";/** 日终执行中 */
	public static String DayendDateStatus_Error = "Error";/** 失败 */

	
	/**
	 *   000089 日终 轧账状态 轧帐状态
	 */
	public static String RollingStatus_NEW = "0"; /** 待轧账 */
	public static String RollingStatus_Error = "8";/** 失败 */
  	public static String RollingStatus_Success = "9";	    /** 成功 */

  	
}
