package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TtInstitutionSettls;

import java.util.Map;

public interface InstitutionSettlsService {

	/**
	 * 分页查询清算列表
	 * @param map
	 * @return
	 */
	public Page<TtInstitutionSettls> getPageSettls(Map<String, Object> map);
	
	/**
	 * 新增机构清算
	 * @param institutionSettls
	 */
	public void saveInstitutionSettl(TtInstitutionSettls institutionSettls); 
	
	/**
	 * 删除机构清算
	 * @param map
	 */
	public void deleteInstitutionSettl(Map<String, Object> map);
	
	/**
	 * 更新机构清算
	 * @param institutionSettls
	 */
	public void updateInstitutionSettl(TtInstitutionSettls institutionSettls);
}
