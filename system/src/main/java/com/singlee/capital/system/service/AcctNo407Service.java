package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.AcctNo;

import java.util.HashMap;
import java.util.List;

public interface AcctNo407Service {

	/**
	 * 添加账号
	 */
	public void createAcctNo(AcctNo acctNo);
	
	public Page<AcctNo> searchAcctNo407Page(HashMap<String, Object> map);
	
	public List<AcctNo> searchAcctNo407List(HashMap<String, Object> map);
	
	public void updateAcctNoById(HashMap<String, Object> map);
	
	public void deletaAcctNoById(HashMap<String, Object> map);
	
}
