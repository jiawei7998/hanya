package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name = "TA_USER_INST_MAP")
public class TaUserInst implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4279522670463675760L;
	
	@Id
	private String userId;//用户Id
	private String instId;//机构Id
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getInstId() {
		return instId;
	}
	public void setInstId(String instId) {
		this.instId = instId;
	}
	
	

}
