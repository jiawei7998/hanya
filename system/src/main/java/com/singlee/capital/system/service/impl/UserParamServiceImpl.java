package com.singlee.capital.system.service.impl;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.mapper.TaUserParamMapper;
import com.singlee.capital.system.model.TaUserParam;
import com.singlee.capital.system.service.UserParamService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户参数
 * @author dingzy
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class UserParamServiceImpl implements UserParamService {
	
	private static final String cacheName = "com.singlee.capital.system.userparam";
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
	@Autowired
	private TaUserParamMapper userParamMapper;

	@Override
	@CacheEvict(value = cacheName ,allEntries=true)
	public RetMsg<Boolean> addUserParam(TaUserParam param) {
		RetMsg<Boolean> ret=RetMsgHelper.ok(true);
		if(param.getP_parent_code()==null || "".equals(param.getP_parent_code())){
			param.setP_parent_code("-1");
		}
		String p_type = param.getP_type();
		String p_sub_type = param.getP_sub_type();
		String p_name = param.getP_name();
		Map<String, Object>  map = new HashMap<String, Object>();
		map.put("p_type", p_type);
		map.put("p_sub_type", p_sub_type);
		map.put("p_name", p_name);
		List<TaUserParam> list = userParamMapper.searchTaUserParam(map);
		if(list != null && list.size() > 0){
			ret.setDetail("120");
		}else{
			ret.setDetail("000");
			userParamMapper.insert(param);
		}
		return ret;
	}
	
	/**
	 * @param param 
	 * @author lijing
	 * @since 20180223
	 * 在原有基础上新增功能：修改时,如果状态为已复核(2)或者为退回(B),
	 * 将状态改为经办待复核(1),
	 * 增加经办人，经办时间
	 * 
	 */
	@Override
	@CacheEvict(value = cacheName ,allEntries=true)
	public void modifyUserParam(TaUserParam param) {
		param.setIoper(SlSessionHelper.getUserId());
		param.setItime(sdf.format(new Date()));
		String status = param.getStatus();
		if("2".equals(status)  || "B".equals(status)){
			param.setStatus("0");
		}
		userParamMapper.updateByPrimaryKey(param);
		
	}

	@Override
	@CacheEvict(value = cacheName ,allEntries=true)
	public void delUserParam(TaUserParam userParam) {
		userParamMapper.delete(userParam);
		
	}

	@Override
	public Page<TaUserParam> search(Map<String, Object> param) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(param);
		Page<TaUserParam> result = userParamMapper.pageTaUserParams(param, rowBounds);
		return result;
	}
	
	@Override
	public List<TaUserParam> getParamList(Map<String,Object> map) {
		List<TaUserParam> oriParamList = TreeUtil.mergeChildrenList(userParamMapper.searchTaUserParam(map), "-1");
		//List<TaUserParam> oriParamList = userParamMapper.searchTaUserParam(map);
		return oriParamList;
	}
	
	@Override
	public Page<TaUserParam> pageTaUserParams(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TaUserParam> list = userParamMapper.pageTaUserParams(map,rowBounds);
		//List<TaUserParam> list = TreeUtil.mergeChildrenList(userParamMapper.searchTaUserParam(map), "-1");
		return list;
	}
	
	@Override
	public List<TaUserParam> getParamTree(Map<String,Object> map) {
		List<TaUserParam> list = TreeUtil.mergeChildrenList(userParamMapper.searchTaUserParam(map), "-1");
		//List<TaUserParam> oriParamList = userParamMapper.searchTaUserParam(map);
		return list;
	}

	@Override
	@Cacheable(value = cacheName, key="#p_name")
	public String getSysParamByName(String p_name, String def) {
		Map<String,Object> searchMap = new HashMap<String,Object>();
		searchMap.put("p_type", ExternalDictConstants.ParameterBigKind_SystemParameter);
		searchMap.put("p_sub_type", ExternalDictConstants.UserParameterType_SystemConfigType);
		searchMap.put("p_name", p_name);
		List<TaUserParam> oriParamList = userParamMapper.searchTaUserParam(searchMap);
		if(oriParamList != null && oriParamList.size() == 1){
			return oriParamList.get(0).getP_value();
		}
		return def;
	}

	@Override
	public boolean getBooleanSysParamByName(String pName, boolean def) {
		String pValue = myself().getSysParamByName(pName,"");
		if(StringUtils.equalsIgnoreCase("true", pValue)){
			return true;
		}else if(StringUtils.equalsIgnoreCase("false", pValue)){
			return false;
		}
		return def;
	}

	@Override
	public int getIntSysParamByName(String pName, int def) {
		String pValue = myself().getSysParamByName(pName,"");
		try{
			return Integer.parseInt(pValue);
		}catch(Exception e){
			return def;
		}
	}

	@Override
	public double getDoubleSysParamByName(String pName, double def) {
		String pValue = myself().getSysParamByName(pName,"");
		try{
			return Double.parseDouble(pValue);
		}catch(Exception e){
			return def;
		}
	}
	/**
	 * 查询是否银行机构
	 */
	@Override
	public TaUserParam searchTaUserParam(TaUserParam param) {
		return userParamMapper.searchTaUserParam(param);
	}
	
	/**
	 * 为了 代理 能进入
	 * @return
	 */
	private UserParamServiceImpl myself() {
		return SpringContextHolder.getBean(UserParamServiceImpl.class);
	}
	
	
	/**
	 * 复核查询
	 */
	@Override
	public Page<TaUserParam> pageTaUserParamsVerify(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		Page<TaUserParam> list = userParamMapper.pageTaUserParamsVerify(map,rowBounds);
		//List<TaUserParam> list = TreeUtil.mergeChildrenList(userParamMapper.searchTaUserParam(map), "-1");
		return list;
	}
	
	/**
	 * 提交复核，修改状态为1
	 */
	@Override
	@CacheEvict(value = cacheName ,allEntries=true)
	public void modifyUserParamVerify1(TaUserParam param) {
		param.setIoper(SlSessionHelper.getUserId());
		param.setItime(sdf.format(new Date()));
		param.setStatus("1");
		//userParamMapper.updateStatus1(param);
		userParamMapper.updateByPrimaryKey(param);
		
	}
	
	/**
	 * 复核通过，修改状态为2
	 */
	@Override
	@CacheEvict(value = cacheName ,allEntries=true)
	public RetMsg<Serializable> modifyUserParamVerify2(TaUserParam param) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "");
		String user1=param.getIoper();
		String user2=SlSessionHelper.getUserId();
		System.out.println("user1:"+user1);
		System.out.println("user2:"+user2);
		System.out.println(param.toString());
		if(param.getIoper()!=null&&param.getIoper().equals(SlSessionHelper.getUserId())){
			ret.setCode("999999");
			ret.setDesc("复核人不能跟经办人重复");
			return ret;
		}
		param.setVtime(sdf.format(new Date()));
		param.setVoper(SlSessionHelper.getUserId());
		param.setStatus("2");
		//userParamMapper.updateStatus2(param);
		userParamMapper.updateByPrimaryKey(param);
		return ret;
	}
	
	
	/**
	 * 复核拒绝，修改状态为B(退回)
	 */
	@Override
	@CacheEvict(value = cacheName ,allEntries=true)
	public RetMsg<Serializable> modifyUserParamVerify3(TaUserParam param) {
		RetMsg<Serializable> ret = new RetMsg<Serializable>(RetMsgHelper.codeOk, "");
		if(param.getIoper()!=null&&param.getIoper().equals(SlSessionHelper.getUserId())){
			ret.setCode("999999");
			ret.setDesc("复核人不能跟经办人重复");
			return ret;
		}
		param.setVtime(sdf.format(new Date()));
		param.setVoper(SlSessionHelper.getUserId());
		param.setStatus("B");
		userParamMapper.updateByPrimaryKey(param);
		return ret;
	}

	@Override
	public List<TaUserParam> searchTaUserParam(Map<String, Object> params) {
		return userParamMapper.searchTaUserParam(params);
	}

	
}
