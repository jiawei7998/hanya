package com.singlee.capital.system.mapper;


import com.github.pagehelper.Page;
import com.singlee.capital.system.model.AcctNo;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;

public interface AcctNoMapper extends Mapper<AcctNo> {
	
	public Page<AcctNo> searchAcctNoPage(HashMap<String, Object> map, RowBounds rowBounds);
	
	public List<AcctNo> searchAcctNo407List(HashMap<String, Object> map);
	
	public void deletaAcctNoById(HashMap<String, Object> map);
	
	public void insertAcctNo(AcctNo acctNo);
	
}