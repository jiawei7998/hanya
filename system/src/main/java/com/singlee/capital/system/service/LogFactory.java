package com.singlee.capital.system.service;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.DoubleBufferedList;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.List;


/**
 * 日志 处理工厂
 * 不应该在spring加载完成之前调用。是给业务系统记录业务请求处理日志使用！ 
 * @author x230i
 *
 */
public class LogFactory {

	
	private static class SingletonHolder {
		private static final LogFactory INSTANCE = new LogFactory();
	}

	/**
	 * 统一对外提供的实例入口
	 * @return
	 */
	public static final LogFactory getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	
	private final DoubleBufferedList<TaLog> doubleBufferedList;

//	private final BatchDao batchDao;
	private LogFactory(){
		//batchDao = SpringContextHolder.getBean("batchDao");
		doubleBufferedList = new DoubleBufferedList<TaLog>("----业务日志----"){
			@Override
			protected int handleList(List<TaLog> t) {
				// t batch 操作 如数据库 200/100 
				//batchDao.batch("com.singlee.capital.system.mapper.TaLogMapper.insert", t, 100);
				return t.size();
			}
		};
	}

	public TaLog create(SlSession session, String value, String req, String method) {
		TaLog l = new TaLog();
		l.setReqDesc(value);
		l.setReqObject(req);
		l.setReqIp(SlSessionHelper.getIp(session));
		l.setReqUserAgent(SlSessionHelper.getUa(session));
		l.setReqUrl(SlSessionHelper.getUri(session));
		l.setReqUserId(SlSessionHelper.getUserId(session));
		l.setReqMethod(method);
		l.setReqTime(DateUtil.getCurrentDateTimeAsString());
		return l;
	}
	
	public void handle(TaLog l , Object retVal ){
		if(retVal instanceof RetMsg<?>){
			RetMsg<?> rm = (RetMsg<?>) retVal;
			l.setRspCode(rm.getCode());
			l.setRspDesc(rm.getDesc());
			l.setRspDetail(rm.getDetail());
			l.setRspObject(rm.getObj()==null?"":rm.getObj().toString());
		}
		l.setRspTime(DateUtil.getCurrentDateTimeAsString());
		handle(l);
	}
	public void handle(TaLog l , Throwable t){
		l.setRspCode(t.getClass().getSimpleName());
		l.setRspDesc(t.getMessage());
		l.setRspDetail(t.getMessage());
		l.setRspObject(StringUtils.join(ExceptionUtils.getRootCauseStackTrace(t),"\r\n"));
		l.setRspTime(DateUtil.getCurrentDateTimeAsString());
		handle(l);
	}
	
	/**
	 * 
	 * @param session
	 * @param value
	 * @param startTime
	 * @param retVal
	 * @param stopTime
	 */
	public void handle(SlSession session, String value, String req, long startTime,
			Object retVal, long stopTime) {
		String code = "";
		String msg = "";
		String detail = "";
		String rsp = "";
		boolean exce = false;
		if(retVal instanceof RetMsg<?>){
			RetMsg<?> rm = (RetMsg<?>)retVal ;
			code = rm.getCode();
			msg = rm.getDesc();
			detail = rm.getDetail();
			
			rsp = rm.getObj() != null ? rm.getObj().toString() : "";
		}
		handle(SlSessionHelper.getUserId(session), 
				SlSessionHelper.getIp(session), 
				SlSessionHelper.getUri(session),
				SlSessionHelper.getUa(session), value, 
				req, DateUtil.getCurrentTimeAsString(), 
				code,msg,detail,rsp,exce, DateUtil.getCurrentTimeAsString());

		
	}

	
	/**
	 * 处理 
	 * @param sesion
	 * @param desc
	 */
	public void handle(SlSession session, String desc){
		handle(SlSessionHelper.getUserId(session), "n/a", "n/a", "n/a", desc, "", DateUtil.getCurrentTimeAsString(), "","","","",false, DateUtil.getCurrentTimeAsString());
	}
	public void handle(SlSession session, String url, String desc, String req, String code, String msg, String detail, String rsp){
		handle(SlSessionHelper.getUserId(session), 
				"n/a", "n/a", "n/a", desc, "", DateUtil.getCurrentTimeAsString(), "","","","",false, DateUtil.getCurrentTimeAsString());
	}
	/**
	 * 
	 * @param uid
	 * @param ip
	 * @param url
	 * @param ua
	 * @param desc
	 * @param req
	 * @param rt
	 * @param code
	 * @param msg
	 * @param detail
	 * @param rsp
	 * @param e
	 * @param rspTime
	 */
	public void handle(String uid, String ip, String url, String ua, String desc, 
			String req, String rt, String code, String msg, String detail, String rsp, boolean e,
			String rspTime){
		TaLog l = new TaLog();
		l.setException(e);
		l.setReqDesc(desc);
		l.setReqIp(ip);
		l.setReqUserAgent(ua);
		l.setReqObject(req);
		l.setReqTime(rt);
		l.setReqUrl(url);
		l.setReqUserId(uid);
		l.setRspCode(code);
		l.setRspDesc(msg);
		l.setRspDetail(detail);
		l.setRspObject(rsp);
		l.setRspTime(rspTime);
		handle(l);
	}
	
	/**
	 * 
	 * @param log
	 */
	public void handle(TaLog log){
		doubleBufferedList.produce(log);
	}

}
