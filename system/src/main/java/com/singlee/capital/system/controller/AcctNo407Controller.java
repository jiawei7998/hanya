package com.singlee.capital.system.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.model.AcctNo;
import com.singlee.capital.system.service.AcctNo407Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;

@Controller
@RequestMapping(value = "/AcctNo407Controller")
public class AcctNo407Controller extends CommonController {

	@Autowired
	private AcctNo407Service acctNo407Service;

	@ResponseBody
	@RequestMapping(value = "/searchAcctNo407Page")
	public RetMsg<PageInfo<AcctNo>> searchInstitution(@RequestBody HashMap<String, Object> map) throws RException {
		Page<AcctNo> list = acctNo407Service.searchAcctNo407Page(map);			
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/addAcctNo")
	public RetMsg<Serializable> addAcctNor(@RequestBody AcctNo acctNo) throws RException {
		acctNo407Service.createAcctNo(acctNo);
		return RetMsgHelper.ok();
	}
	@ResponseBody
	@RequestMapping(value = "/deleteAcctNo")
	public RetMsg<Serializable> deleteAcctNo(@RequestBody HashMap<String, Object> map) throws RException {
		acctNo407Service.deletaAcctNoById(map);
		return RetMsgHelper.ok();
	}
	
}
