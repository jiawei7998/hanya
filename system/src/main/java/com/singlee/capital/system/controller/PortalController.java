package com.singlee.capital.system.controller;

import com.singlee.capital.system.mapper.TaPortalMapper;
import com.singlee.capital.system.model.TaPortal;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/PortalController")
public class PortalController extends CommonController {
	@Autowired
	TaPortalMapper portalMapper;

	/**
	 * 根据归属银行Id 用户id 查询首页样式布局表
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllPortal")
	public List<TaPortal> getAllPortal(Map<String,Object> map) {
		map.put("userId", SlSessionHelper.getUserId());
		return portalMapper.getAllPortal(map);
	}
}
