package com.singlee.capital.system.service.impl;

import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.dict.ExternalDictUtil;
import com.singlee.capital.system.mapper.TaDictMapper;
import com.singlee.capital.system.model.TaDict;
import com.singlee.capital.system.model.TaDictItem;
import com.singlee.capital.system.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author x230i
 *
 */
@Service
public class DictionaryServiceImpl implements DictionaryService{


	private static final String cacheName = "com.singlee.capital.system.dictionary";

	
	private static final String prev =  "dictionary.%s.%s";
	private static final String prev2 =  "dictionary.%s";
	
	private static final Pattern p = Pattern.compile("\\d{6}"); 
	
	@Autowired
	private TaDictMapper tdm;
	@Override
	@Cacheable(value = cacheName,key = "0")
	public String getDictionaryJsonString() {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		List<TaDict> list = tdm.loadAllDictionary();
		for (TaDict d : list) {
			sb.append(getJsonByTaDict(d, null));
			sb.append(",");
			Matcher m = p.matcher(d.getDict_id());
			// 为了兼顾 000000 这样6位数字的字典前台显示！！！！按 dict_key再来一次
			if (m.matches()) {
				sb.append(getJsonByTaDict(d, d.getDict_name()));
				sb.append(",");
			}
		}
		try {
			Class<?>[] clzs = ExternalDictUtil.clazzDictional.getClasses();
			// 增加 字典项的字典
			sb.append(createDictDict(clzs));
		} catch (Exception e) {
			sb.append("获得DictionalInterface错误" + e.getMessage());
		}

		sb.append("}");
		return sb.toString();
	}

	private String createDictDict(Class<?>[] clzs) {
		StringBuffer sb = new StringBuffer();
		sb.append("\"dict\":");
		sb.append("{");		
		boolean first = true;
		for(Class<?> clz : clzs){
			String key;
			try {
				key = clz.getSimpleName();
				String code = String.format(prev2, key);
				String value = MsgUtils.getMessage(code);
				if(!first) { 
					sb.append(",");}
				else {
					first = false;}
				sb.append("\"").append(key).append("\":{\"text\":\"").append(value).append("\",\"folder\":[]}");
			} catch (Exception e) {
				JY.raiseRException("dict dict dictionary error!");
			}
		}

		sb.append("}");		
		return sb.toString();
	}

	private String getJsonByTaDict(TaDict clz, String name){
		StringBuffer sb = new StringBuffer();
		sb.append("\"");
		if(name!=null){
			sb.append(name);	
		}else{
			sb.append(clz.getDict_id());
		}
		sb.append("\":");
		sb.append("{");		
		List<TaDictItem> fs = clz.getDictItems();
		boolean first = true;
		for(TaDictItem f : fs){
			try {
				String code = String.format(prev, clz.getDict_name(), f.getDict_key());
				String value = MsgUtils.getMessage(code);
				if(!first) {
					sb.append(",");}
				else {
					first = false;}
				String folder = f.getDict_folder2();
				// "CM_SP_BND":{"text":"普通债券","folder":["1","2","5"]}
				sb.append("\"").append(f.getDict_key()).append("\":{\"text\":\"").append(value).append("\",\"folder\":").append(folder).append("}"); 
			} catch (Exception e) {
//				e.printStackTrace();
				JY.raiseRException(clz.getDict_name() + " dictionary error!" + e.getMessage());
			}
		}
		sb.append("}");		
		return sb.toString();
	}

	@SuppressWarnings("unused")
	private String getJsonByClass(Class<?> clz){
		StringBuffer sb = new StringBuffer();
		sb.append("\"");
		sb.append(clz.getSimpleName());
		sb.append("\":");
		sb.append("{");		
		Field[] fs = clz.getDeclaredFields();
		boolean first = true;
		for(Field f : fs){
			String key;
			try {
				key = (String)f.get(null);
				String code = String.format(prev, clz.getSimpleName(), key);
				String value = MsgUtils.getMessage(code);
				if(!first) {
					sb.append(",");}
				else {
					first = false;}
				sb.append("\"").append(key).append("\":\"").append(value).append("\""); 
			} catch (Exception e) {
				JY.raiseRException(f.getName() + " dictionary error!");
			}
		}
		sb.append("}");		
		return sb.toString();
	}
	
}
