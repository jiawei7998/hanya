package com.singlee.capital.system.service;

import java.util.Map;

public interface InstitutionProductService {

	/**
	 * 新增机构产品准入
	 * @param map
	 */
	public void addInstProduct(Map<String, Object> map);
	
	/**
	 * 查询已准入的产品名称
	 * @param map
	 * @return
	 */
	public String getProNames(Map<String, Object> map);
	
	/**
	 * 查询机构名称
	 * @param map
	 * @return
	 */
	public String getInstName(Map<String, Object> map);
}
