package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaRoleDeskMap;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;



public interface TaRoleDeskMapMapper extends Mapper<TaRoleDeskMap> {
	public List<String> getHaveDeskByRoleIdAndInstId(Map<String, Object> map);
	public void deleteBatchRoleDeskMap(Map<String, Object> map);
	/**
	 * 批量插入数据
	 * @param tadesk
	 */
	public void addBatchRoleDeskMap(List<TaRoleDeskMap> tadesk);

}
