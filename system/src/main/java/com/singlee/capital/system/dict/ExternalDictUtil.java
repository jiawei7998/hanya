package com.singlee.capital.system.dict;

import com.singlee.capital.common.util.JY;

import java.lang.reflect.Field;

public class ExternalDictUtil {

	
	public static Class<?> clazzDictional;
	
	/**
	 * 使用外部的字典对 本数据字典进行赋值处理！
	 * @param dictClzName
	 */
	public static void initDict(String dictClzName){
		try {
			Class<?> demo = ExternalDictConstants.class;
			Field[] fields=demo.getDeclaredFields();
			for(int i=0;i<fields.length;i++){ //遍历所有属性
				String name=fields[i].getName();//获取属性的名称
				//解析字符串
				String dictClass= name.substring(0, name.indexOf("_"));
				String dictProp = name.substring(name.indexOf("_")+1);
				//反射类名
				Class<?> dictClz = Class.forName(dictClzName + "$"+ dictClass);
				try{
					Field dictField = dictClz.getDeclaredField(dictProp);
					Object dictObj = dictField.get(dictClz);
					JY.require(dictObj !=null, "所需的字典[%s]没有定义！！", fields[i].getName());
					fields[i].set(demo, dictObj);
				} catch (NoSuchFieldException e){
					JY.raise("%s$%s->%s没有定义！",dictClzName, dictClass,dictProp);
				} catch (IllegalArgumentException e) {
					JY.raise("%s$%s->%s无效参数！",dictClzName, dictClass,dictProp);
				} catch (IllegalAccessException e) {
					JY.raise("%s$%s->%s无法访问！",dictClzName, dictClass,dictProp);
				}
				clazzDictional = dictClz;
			}
		} catch (ClassNotFoundException e1) {
			JY.raise("无法找到类%s--%s！",dictClzName, e1.getMessage());
		};
	}
}
