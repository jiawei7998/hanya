package com.singlee.capital.system.dict;

public class ModuleTypeConstants {

	public static final String FIRST = "1";// 一级父节点
	
	public static final String SECOND = "2";// 二级父节点
	
	public static final String THIRD = "3";// 三级父节点
	
	public static final String FOUTH = "4";// 四级父节点
	
	public static final String FIFTH = "5";// 五级父节点
}
