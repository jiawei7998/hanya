package com.singlee.capital.system.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限保存时，菜单数组
 * @author gm

}
 */
public class Module {
	private String module_id;
	private List<String> fun_list;//功能点id数组
	
	public String getModule_id() {
		return module_id;
	}

	public void setModule_id(String moduleId) {
		module_id = moduleId;
	}

	public List<String> getFun_list() {
		if(fun_list != null){
			return fun_list;
		}else{
			return new ArrayList<String>();
		}
	}

	public void setFun_list(List<String> funList) {
		fun_list = funList;
	}
}