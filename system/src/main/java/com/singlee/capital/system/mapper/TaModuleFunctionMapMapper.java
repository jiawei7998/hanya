package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaModuleFunctionMap;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaModuleFunctionMapMapper extends Mapper<TaModuleFunctionMap> {
	void deleteByModuleId(Map<String,Object> map);
	List<String> getHavingFuncId(Map<String,Object> map);
}