package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaModuleCopy;
import com.singlee.capital.system.model.TaRoleModuleMap;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaRoleModuleMapMapper extends Mapper<TaRoleModuleMap> {
	/**
	 * 根据角色ID读取角色有权限的菜单ID
	 * @param role_id:角色id(不允许为空)
	 * @return
	 */
	public List<String> searchModuleIdForRoleId(String role_id);
	
	public List<String> searchModuleIdForRoleIdAndDeskIdAndBranchId(Map<String, Object> map);

	public void deleteRoleModuleByRoleId(String roleId);
	
	public void deleteRoleModuleByRoleIdAndModules(TaRoleModuleMap taRoleModuleMap);
	
	public void insertBatchRoleModuleMap(TaRoleModuleMap taRoleModuleMap);

	public List<TaModuleCopy> findModuleByRoleId(Map<String, Object> map);
	
	public List<String> getAllModulesHaving(Map<String, Object> map);
	
	public void deleteByRoleIdAndBranchId(Map<String, Object> map);
	
	public void addBatchRoleModuleMap(List<TaRoleModuleMap> tamodule);
	
}