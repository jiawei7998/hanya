package com.singlee.capital.system.model;

import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 
 * @author x230i
 *
 */
@Table(name="TA_LOG")
@Entity
public class TaLog implements Serializable {

	private static final long serialVersionUID = 1L;

	public TaLog() {
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TA_LOG.NEXTVAL FROM DUAL")
	private String id; 
	private String reqIp; // 请求的ip
	private String reqUrl;// 请求地址
	private String reqUserId; // 请求的用户id
	private String reqUserAgent; //处理请求的方法
	private String reqMethod; //处理请求的方法
	private String reqDesc; //描述
	private String reqObject; // 处理请求内容对象
	private String reqTime;//请求时间
	private boolean exception; //是否有异常
	private String rspCode; // 
	private String rspDesc; //
	private String rspDetail; // 
	private String rspObject; // 处理响应的日志，如果是异常就是异常的详细信息	
	private String rspTime; //最后时间
	private String refNo;
	
	@Transient
	private String reqUserName;

	public String getReqMethod() {
		return reqMethod;
	}
	public void setReqMethod(String reqMethod) {
		this.reqMethod = StringUtils.abbreviate(reqMethod, 100-10);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReqIp() {
		return reqIp;
	}
	public void setReqIp(String reqIp) {
		this.reqIp = reqIp;
	}
	public String getReqUrl() {
		return reqUrl;
	}
	public void setReqUrl(String reqUrl) {
		this.reqUrl = StringUtils.abbreviate(reqUrl,100-3);
	}
	public String getReqUserId() {
		return reqUserId;
	}
	public void setReqUserId(String reqUserId) {
		this.reqUserId = StringUtils.abbreviate(reqUserId,40-3);
	}
	
	public String getReqUserAgent() {
		return reqUserAgent;
	}
	public void setReqUserAgent(String reqUserAgent) {
		this.reqUserAgent = StringUtils.abbreviate(reqUserAgent,200-15);
	}
	public String getReqDesc() {
		return reqDesc;
	}
	public void setReqDesc(String reqDesc) {
		this.reqDesc = StringUtils.abbreviate(reqDesc,500-15);
	}
	public String getReqObject() {
		return reqObject;
	}
	public void setReqObject(String reqObject) {
		this.reqObject = StringUtils.abbreviate(reqObject,1024-15);
	}
	
	public String getReqTime() {
		return reqTime;
	}
	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}
	public boolean isException() {
		return exception;
	}
	public void setException(boolean exception) {
		this.exception = exception;
	}
	public String getRspCode() {
		return rspCode;
	}
	public void setRspCode(String rspCode) {
		this.rspCode = rspCode;
	}
	public String getRspDesc() {
		return rspDesc;
	}
	public void setRspDesc(String rspDesc) {
		this.rspDesc = StringUtils.abbreviate(rspDesc,256-15);
	}
	public String getRspDetail() {
		return rspDetail;
	}
	public void setRspDetail(String rspDetail) {
		this.rspDetail = StringUtils.abbreviate(rspDetail,1024-15);
	}
	public String getRspObject() {
		return rspObject;
	}
	public void setRspObject(String rspObject) {
		this.rspObject = rspObject;
	}
	
	public String getRspTime() {
		return rspTime;
	}
	public void setRspTime(String rspTime) {
		this.rspTime = rspTime;
	}
	public String getReqUserName() {
		return reqUserName;
	}
	public void setReqUserName(String reqUserName) {
		this.reqUserName = reqUserName;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	
//	@Override
//	public String toString() {
//		return String.format("%s@%s --> %s \t method(%s)-> %s \t duration(%d) \t(%s) \t log=%s .",
//				logId, logIp, logUrl, logDesc, logMethod, (lastTime-requestTime), logException?err:"ok", logContent, err);
//	}
//	
	
	
}
