package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.dict.ApproveStatusConstants;
import com.singlee.capital.system.mapper.TaRoleMapper;
import com.singlee.capital.system.mapper.TaRoleModuleMapCopyMapper;
import com.singlee.capital.system.mapper.TaRoleModuleMapMapper;
import com.singlee.capital.system.model.TaModuleCopy;
import com.singlee.capital.system.model.TaRoleModuleMap;
import com.singlee.capital.system.model.TaRoleModuleMapCopy;
import com.singlee.capital.system.service.RoleModuleService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class RoleModuleServiceImpl implements RoleModuleService {
	
	@Autowired
	private TaRoleModuleMapCopyMapper roleModuleMapCopyMapper;
	@Autowired
	private TaRoleModuleMapMapper roleModuleMapMapper;
	@Autowired
	private TaRoleMapper roleMapper;

	@Override
	public Page<TaRoleModuleMapCopy> getWaitApproveList(Map<String, Object> map) {
		map.put("approveStatus", ApproveStatusConstants.APPROVE_ING);
		return roleModuleMapCopyMapper.findByRoleIdAndApproveStatus(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<TaModuleCopy> findOldModuleList(Map<String, Object> map) {
		return TreeUtil.mergeChildrenList(roleModuleMapMapper.findModuleByRoleId(map), "0");
	}

	@Override
	public List<TaModuleCopy> findNewModuleList(Map<String, Object> map) {
		String roleModuleIdStr = ParameterUtil.getString(map, "roleModuleId", "");
		String[] roleModuleIdArray = roleModuleIdStr.split(",");
		List<String> roleModuleIdList = Arrays.asList(roleModuleIdArray);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("roleModuleIdList", roleModuleIdList);
		List<TaModuleCopy> moduleList = roleModuleMapCopyMapper.findByRoleModuleIdList(param);
		return TreeUtil.mergeChildrenList(moduleList, "0");
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void doApprove(Map<String, Object> map) {
		String roleModuleIds = ParameterUtil.getString(map, "roleModuleIds", "");
		String roleId = ParameterUtil.getString(map, "roleId", "");
		String[] roleModuleIdArray = roleModuleIds.split(",");
		List<String> roleModuleIdList = Arrays.asList(roleModuleIdArray);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("roleModuleIdList", roleModuleIdList);
		param.put("currentUserId", SlSessionHelper.getUserId());
		param.put("approveStatus", ApproveStatusConstants.APPROVE_ED);
		param.put("approveDate", DateTimeUtil.getLocalDateTime());
		roleModuleMapCopyMapper.updateByRoleModuleIdList(param);
		roleModuleMapMapper.deleteRoleModuleByRoleId(roleId);
		List<TaRoleModuleMap> roleModuleList = new ArrayList<TaRoleModuleMap>();
		for(String roleModuleId : roleModuleIdList) {
			TaRoleModuleMapCopy roleModuleCopy = roleModuleMapCopyMapper.selectByPrimaryKey(roleModuleId);
			TaRoleModuleMap roleModuleMap = new TaRoleModuleMap();
			roleModuleMap.setRoleId(roleModuleCopy.getRoleId());
			roleModuleMap.setModuleId(roleModuleCopy.getModuleId());
			roleModuleMap.setAccessFlag(roleModuleCopy.getAccessFlag());
			roleModuleList.add(roleModuleMap);
		}
		HashMap<String, List<TaRoleModuleMap>> TaRoleModuleMapBoMap = new HashMap<String, List<TaRoleModuleMap>>();
		TaRoleModuleMapBoMap.put("RoleForModules", roleModuleList);
		roleMapper.insertRoleForModuleBatch(TaRoleModuleMapBoMap);
	}

	@Override
	public Page<TaRoleModuleMapCopy> getApprovedList(Map<String, Object> map) {
		map.put("approveStatus", ApproveStatusConstants.APPROVE_ED);
		return roleModuleMapCopyMapper.findByRoleIdAndApproveStatus(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<String> searchModuleIdForRoleIdAndDeskIdAndBranchId(Map<String, Object> map) {
		return roleModuleMapMapper.searchModuleIdForRoleIdAndDeskIdAndBranchId(map);
	}

	@Override
	public List<String> getModuleByRoleId(String role_id) {
		return roleModuleMapMapper.searchModuleIdForRoleId(role_id);
	}
}
