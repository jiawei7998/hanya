package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.AcctNoMapper;
import com.singlee.capital.system.model.AcctNo;
import com.singlee.capital.system.service.AcctNo407Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;


@Service
public class AcctNo407ServiceImpl  implements AcctNo407Service{

	@Autowired
	private AcctNoMapper acctNoMapper;

	@Override
	public void createAcctNo(AcctNo acctNo) {
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("instId", acctNo.getInstId());
		map.put("ccy", acctNo.getCcy());
		map.put("acctNo", acctNo.getAcctNo());
		List<AcctNo> list=acctNoMapper.searchAcctNo407List(map);
		if(list.size()>0){
			JY.raiseRException("机构号["+acctNo.getInstId()+"],币种["+acctNo.getCcy()+"],账号["+acctNo.getAcctNo()+"]已存在");
		}else{
			acctNoMapper.insertAcctNo(acctNo);
		}
	}

	@Override
	public Page<AcctNo> searchAcctNo407Page(HashMap<String, Object> map) {
		return acctNoMapper.searchAcctNoPage(map,ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public List<AcctNo> searchAcctNo407List(HashMap<String, Object> map) {
		return acctNoMapper.searchAcctNo407List(map);
	}

	@Override
	public void updateAcctNoById(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletaAcctNoById(HashMap<String, Object> map) {
		acctNoMapper.deletaAcctNoById(map);
		
	}

}
