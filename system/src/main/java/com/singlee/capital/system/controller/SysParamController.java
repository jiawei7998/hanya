package com.singlee.capital.system.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.model.TaSysParam;
import com.singlee.capital.system.service.SysParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

 
/**
 * 系统参数控制器 请求前缀 /SysParamController/
 * @author lihb
 */
@Controller
@RequestMapping(value = "/SysParamController")
public class SysParamController extends CommonController {
	
	/** 系统参数服务接口 spring 负责注入 */
	@Autowired
	private SysParamService sysParamService;
	
	/**
	 * 获取多个系统参数
	 * @throws IOException 
	 */
	@RequestMapping(value = "/searchSysParam")
	@ResponseBody
	public RetMsg<PageInfo<TaSysParam>> searchSysParam(@RequestBody Map<String,Object> paramMap) throws IOException{
		return RetMsgHelper.ok(sysParamService.getSysParamList(paramMap));
	}
	
	/**
	 * 修改系统参数
	 * @date 2016-08-25
	 * @author
	 * @returnType void
	 */
	@RequestMapping(value = "/editTaSysParam")
	@ResponseBody
	public RetMsg<Serializable> editTaSysParam(@RequestBody TaSysParam sysParam) throws IOException {
		sysParamService.modifySysParam(sysParam);
		return RetMsgHelper.ok();		
	}
	
}