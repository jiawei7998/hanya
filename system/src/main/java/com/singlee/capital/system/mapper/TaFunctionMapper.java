package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaFunction;
import com.singlee.capital.system.model.TaModule;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaFunctionMapper extends Mapper<TaFunction> {
	
	/**
	 * 有权限的function Boolean 区分
	 * @param map
	 * @return
	 */
	public List<TaFunction> getFunctionByBranchIdAndRoleId(Map<String, Object> map);
	/**
	 * 根据module_id读取该模块相关功能点信息 
	 * @param module_id :菜单id(不允许为空)
	 * @return
	 */
	public List<TaFunction> searchFunctionByModuleId(TaModule taModule);
	/**
	 * 根据 moduleId roleId branchid 获取功能点 
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TaFunction> getFunctionByRoleIdAndModuleId(Map<String, Object> map, RowBounds rb);
	/**
	 * 查询全部functions
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TaFunction> getFunctionByBranchId(Map<String, Object> map, RowBounds rb);
	/**
	 * 根据moduleId branchId  获取functionId 的最大值
	 * @param map
	 * @return
	 */
	public String getMaxFuncByModuleId(Map<String, Object> map);
}