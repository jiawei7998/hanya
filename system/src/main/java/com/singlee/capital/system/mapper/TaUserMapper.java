package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @projectName 同业业务管理系统
 * @className 用户持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-21 上午09:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TaUserMapper extends Mapper<TaUser> {
	
	Page<TaUser> searchUserByBranchId(Map<String, Object> params, RowBounds rb);
	/**
	 * 根据对象查询列表
	 * 
	 * @param user - 用户对象
	 * @param rb - 分页对象
	 * @return 用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	Page<TaUser> pageUser(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 根据对象查询列表
	 * 
	 * @param user - 用户对象
	 * @param rb - 分页对象
	 * @return 用户对象列表
	 * @author Hunter
	 * @date 2016-7-21
	 */
	Page<TaUser> pageRoleUser(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 修改用户状态
	 * 
	 * @param user - 用户对象
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void updateUserState(TaUser user);
	
	/**
	 * 修改用户密码
	 * 
	 * @param userId - 用户主键
	 * @param userPwd - 用户密码
	 * @author Hunter
	 * @date 2016-7-21
	 */
	void updateUserPwd(TaUser user);
	
	void updateUserPhoto(TaUser user);
	
	/**
	 * 修改用户登录信息
	 * @param taUser
	 */
	void updateUserLoginInfo(TaUser taUser);
	
	/**
	 * 根据角色Id 读取拥有该角色的用户信息
	 * @param role_id
	 * @return
	 */
	List<TaUser> searchUserByRoleId(String roleId);
	Page<TaUser> searchUserByRoleId(String roleId,RowBounds rb);

	/**
	 * 
	 * @param flowRoleId
	 * @return
	 */
	List<TaUser> selectUserWithRoleName(@Param("flowRoleId") String flowRoleId);
	
	/**
	 * 根据审批角色,机构ID 查询柜员及角色
	 * 
	 * @param flowRoleId - 角色id
	 * @param instId - 机构id
	 * @author lijing
	 * @date 2018-1-10
	 */
	Page<TaUser> selectUserByRoleNameAndInstId(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 获得用户信息
	 * @param taUserBo
	 * @return
	 */
	TaUser selectUser(String userId);
	/**
	 * 根据条件查询用户信息  用户Id 银行Id 机构Id
	 * @param map
	 * @return
	 */
	TaUser selectTaUserByInstAndBranchIdAndUserId(Map<String, Object> map);
	
	/**
	 * 根据map参数查询用户列表
	 * @param map
	 * @return
	 */
	List<TaUser> searchUser(Map<String, Object> map);
	
	/**
	 * 查询用户个人信息
	 * @param map
	 * @return
	 */
	TaUser pageUser(Map<String,Object> map);
	
	//
//	/**
//	 * 根据主键查询对象
//	 * 
//	 * @param userId - 用户主键
//	 * @return 用户对象
//	 * @author Hunter
//	 * @date 2016-7-21
//	 */
//	TaUser selectUserByUserId(String userId);
	/**
	 * 根据主键查询对象
	 * @param map
	 * @return
	 */
	TaUser queryUser(String USER);
	
	/**
	 * 根据主键 userId 、branchId 查询出唯一的用户信息
	 * @param map
	 * @return TaUser
	 */
	TaUser queryUserByMap(Map<String, Object> map);
	
	/**
	 * 查询用户列表
	 * @param map
	 * @return
	 */
	Page<TaUser> getPageUser(Map<String, Object> map, RowBounds rowBounds);

	/**
	 * 
	 * @param map
	 * @return
	 */
	List<TaUser> selectUserWithInstIds(Map<String,Object> params);
	
	/**
	 * 查询用户对象列表(带分页)
	 * @param 
	 * @return 用户对象列表(带分页)
	 * @author lijing
	 * @date 2018-1-11
	 */
	Page<TaUser> selectUserWithInstIdsPage(Map<String,Object> params, RowBounds rowBounds);

	/**
	 * 查询待审批列表
	 * @return
	 */
	Page<TaUser> selectWaitApproveList(Map<String, Object> map, RowBounds rowBounds);
	
	/**
	 * 查询已审批列表
	 * @param map
	 * @param rowBounds
	 * @return
	 */
	Page<TaUser> selectApprovedList(Map<String, Object> map, RowBounds rowBounds);

	List<TaUser> findByEmpId(Map<String, Object> map);
	
	/**
	 * 检查用户是否存在
	 * @param map
	 * @return
	 */
	int checkUserByBranchId(Map<String, Object> map);
	/**
	 * 新登录，根据用户id+机构Id+角色ID
	 * @param map
	 * @return
	 */
	TaUser getUserByIds(Map<String, Object> map);
	/**
	 * 验证用户唯一性
	 * @param map
	 * @return
	 */
	List<String> verifyUniqueness(Map<String, Object> map);
	/**徽商OPICS项目组
	 * 根据APPROVE_USER_ID查询用户id和机构
	 * @param approveuserid
	 * @return
	 */
	TaUser getUserByApproveuserid(String approveUserId);
	/**
	 * 根据登陆cfets用户查询用户id和机构
	 * @param cfetsTrad
	 * @return
	 */
	TaUser getUserByCfetsTrad(String cfetsTrad);
	
	/**
	 * 查询当前用户所有的角色
	 * @param map
	 * @return
	 */
	String getAllRoleByUserID(Map<String, Object> map);

	/**
	 * 查询当前用户所有的角色
	 * @param map
	 * @return
	 */
	Map<String, Object> getAllRoleNameByUserID(Map<String, Object> map);

	/**
	 * 查询当前用户所有的角色
	 * @param map
	 * @return
	 */
	Map<String, Object> getAllInstNameByUserID(Map<String, Object> map);

	/**
	 * 查询当前银行所有的角色
	 * @param map
	 * @return
	 */
	List<TaUser> getAllUserByIds(String branchId);

    TaUser selectUserInfoById(Map<String,Object> map);

	List<String> getAllUserByUserId(List<String> list);


}