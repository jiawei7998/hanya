package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaModule;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaModuleMapper extends Mapper<TaModule> {
	
	/**
	 * 根据台子递归出该台子下的所有功能
	 */
	List <TaModule> getResourceByDeskId(Map<String, Object> map);
	/**
	 * 释放选中的台子
	 */
	public void updateModuleDeskIsNull(Map<String, Object> map);
	/**
	 * 修改模块归属
	 */
	public void updateModuleDesk(Map<String, Object> map);
	TaModule getModuleByBranchIdAndModuleId(Map<String, Object> map);
	void saveModuleByBranchId(TaModule taModule);
	public Integer getModuleSortNoMax(Map<String, Object> map);
	String getModuleMax(Map<String, Object> map);
	void updateModuleBranchId(Map<String, Object> map);
	/**
	 * 获取一级目录的最大值
	 * @param map
	 * @return
	 */
	public Integer getModuleOneLevelMax(Map<String, Object> map);
	/**
	 * 获取一级目录的最大排序值
	 * @param map
	 * @return
	 */
	public Integer getModuleSortOneLevelMax(Map<String, Object> map);
	/**
	 * 获取一级目录最右边的最大值
	 * @param map
	 * @return
	 */
	public Integer getModuleOneLevelRightMax(Map<String, Object> map);
	/** 
	 * 递归获得所有 roldeIds 指定的 模块 
	 * @param roleIds
	 * @return
	 */
	List<TaModule> recursionModule(List<String> list);
	/**
	 * 根据菜单Id 查询这个菜单下所拥有的权限
	 * @param map
	 * @return
	 */
	List<String> searchModuleDetailByRoleIdAndDeskIdAndBranchId(Map<String, Object> map);
	
	List<String> searchModuleDetailByAndDeskIdAndBranchId(Map<String, Object> map);
	
	List<TaModule> getAllResourceByBranchId(Map<String, Object> map);
	/** 
	 * 递归获得所有 roldeIds 指定的 模块 
	 * @param roleIds
	 * @return
	 */
	List<TaModule> recursionModuleAll();
	
	/**
	 * 查询父菜单信息
	 * @return
	 */
	List<TaModule> searchParModule();
	
	/**
	 * 查询用户拥有的菜单权限
	 * @param map
	 * @return
	 */
	List<TaModule> getUserModule(Map<String, Object> map);
	
	/**
	 * 根据父菜单查询子菜单
	 * @param pModuleId
	 * @return
	 */
	List<String> getChildrenModuleIdList(Map<String, Object> map);
	
	List<TaModule> getTaModulesByUserId(String userId);

	List<TaModule> getTotalPageUrls();

	/**
	 *
	 *台子  菜单   树
	 * @return
	 */
	List <TaModule> getDeskModuleList();
}