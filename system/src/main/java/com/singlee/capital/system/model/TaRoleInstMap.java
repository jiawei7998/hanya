package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name = "TA_ROLE_INST_MAP")
public class TaRoleInstMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3921997601663369773L;
	
	private String roleId;
	
	private String instId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}
	
	

}
