package com.singlee.capital.system.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.model.TaModuleCopy;
import com.singlee.capital.system.model.TaRoleModuleMapCopy;
import com.singlee.capital.system.service.RoleModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/RoleModuleController")
public class RoleModuleController extends CommonController {
	
	@Autowired
	private RoleModuleService roleModuleService;

	@ResponseBody
	@RequestMapping(value = "/waitApproveList")
	public RetMsg<PageInfo<TaRoleModuleMapCopy>> waitApproveList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(roleModuleService.getWaitApproveList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/oldModuleList")
	public RetMsg<List<TaModuleCopy>> oldModuleTreeList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(roleModuleService.findOldModuleList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/newModuleList")
	public RetMsg<List<TaModuleCopy>> newModuleTreeList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(roleModuleService.findNewModuleList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/approvedList")
	public RetMsg<PageInfo<TaRoleModuleMapCopy>> approvedList(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(roleModuleService.getApprovedList(map));
	}
	
	@ResponseBody
	@RequestMapping(value = "/doApprove")
	public RetMsg<Serializable> doApprove(@RequestBody Map<String, Object> map) {
		roleModuleService.doApprove(map);
		return RetMsgHelper.ok();
	}
	
	/***
	 *  roleId
		deskId
		branchId
		moduleId
	 */
	@ResponseBody
	@RequestMapping(value = "/checkModulePermissions")
	public RetMsg<Serializable> checkModulePermissions(@RequestBody Map<String, Object> map) {
		RetMsg<Serializable> ret = RetMsgHelper.ok();
		List<String> modules = roleModuleService.searchModuleIdForRoleIdAndDeskIdAndBranchId(map);
		if(modules.size() > 0){
			ret.setDesc("999");
			ret.setDetail("成功");
		}else{
			ret.setDesc("100");
			ret.setDetail("无权限");
		}
		return ret;
	}

	@ResponseBody
	@RequestMapping(value = "/searchModuleByRoleIdList")
	public RetMsg<List<String>> searchModuleByRoleIdList(@RequestBody Map<String, Object> map) {
		String roleId = ParameterUtil.getString(map,"roleId","");
		return RetMsgHelper.ok(roleModuleService.getModuleByRoleId(roleId));
	}
}
