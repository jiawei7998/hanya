package com.singlee.capital.system.service;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.system.model.TaUserParam;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 用户参数 Service层
 * @author Libo
 *
 */
public interface UserParamService {

	/**
	 * 新增用户参数
	 * @param param
	 * @return 
	 */
	public RetMsg<Boolean> addUserParam(TaUserParam param);
	/**
	 * 修改用户参数
	 * @param param
	 */
	public void modifyUserParam(TaUserParam param);
	/**
	 * 删除用户参数
	 * @param param
	 */
	public void delUserParam(TaUserParam userParam);
	
	/**
	 * 查询系统参数
	 * @param 
	 * @param 
	 * @return
	 */
	public Page<TaUserParam> search(Map<String, Object> param);
	
	/**
	 * 根据参数名查询系统参数
	 * @param pName
	 * @param def
	 * @return
	 */
	public String getSysParamByName(String pName, String def);
	/**
	 * 根据参数名查询系统参数返回boolean类型
	 * @param pName
	 * @param def
	 * @return
	 */
	public boolean getBooleanSysParamByName(String pName, boolean def);
	/**
	 * 根据参数名查询系统参数返回int类型
	 * @param pName
	 * @param def
	 * @return
	 */
	public int getIntSysParamByName(String pName, int def);
	/**
	 * 根据参数名查询系统参数返回double类型
	 * @param pName
	 * @param def
	 * @return
	 */
	public double getDoubleSysParamByName(String pName, double def);
	/**
	 * 根据参数查询列表
	 * @param map
	 * @return
	 */
	List<TaUserParam> getParamList(Map<String, Object> map);
	
	/**
	 * 查询是否银行机构
	 * @param param
	 * @return
	 */
	public TaUserParam searchTaUserParam(TaUserParam param);
	
	/**
	 * 根据参数查询树形结构
	 * @param map
	 * @return
	 */
	Page<TaUserParam> pageTaUserParams(Map<String, Object> map);
	List<TaUserParam> getParamTree(Map<String, Object> map);
	
	/**
	 * 复核查询
	 * @param map
	 * @return
	 */
	Page<TaUserParam> pageTaUserParamsVerify(Map<String, Object> map);
	
	/**
	 * 提交复核
	 * @param param
	 */
	public void modifyUserParamVerify1(TaUserParam param);
	
	/**
	 * 复核通过
	 * @param param
	 */
	public RetMsg<Serializable> modifyUserParamVerify2(TaUserParam param);
	
	/**
	 * 复核拒绝
	 * @param param
	 */
	public RetMsg<Serializable> modifyUserParamVerify3(TaUserParam param);
	
	
	/**
	 * 查询用户参数  普通列表查询
	 * @param params
	 * @return
	 */
	public List<TaUserParam> searchTaUserParam(Map<String, Object> params);
	
}
