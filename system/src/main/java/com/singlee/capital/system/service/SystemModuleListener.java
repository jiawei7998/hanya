package com.singlee.capital.system.service;

import com.singlee.capital.common.spring.ReleaseResourceInitListener;
import org.springframework.stereotype.Service;
@Service
public class SystemModuleListener extends ReleaseResourceInitListener{

	@Override
	protected String jarFilePath() {
		return SystemModuleListener.class.getResource("").getPath();
	}

	@Override
	protected String jarVersion() {
		return "1.0.0.1";
	}

}
