package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TT_TOTAL_INSTITUTION")
public class TtTotalInstitution implements Serializable {

	private static final long serialVersionUID = 1L;

	private String instId;
	
	private String instFullname;
	
	private String isActive;

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getInstFullname() {
		return instFullname;
	}

	public void setInstFullname(String instFullname) {
		this.instFullname = instFullname;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
}
