package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaRole;
import com.singlee.capital.system.model.TaUserRoleMapCopy;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaUserRoleMapCopyMapper extends Mapper<TaUserRoleMapCopy> {

	Page<TaUserRoleMapCopy> findByUserIdAndApproveStatus(Map<String, Object> map, RowBounds roBounds);
	
	void deleteByUserIdAndApproveStatus(Map<String, Object> map);
	
	Page<TaUserRoleMapCopy> pageUserRoleMap(Map<String, Object> map, RowBounds roBounds);
	
	List<TaRole> findByUserRoleIdList(Map<String, Object> map);
	
	List<TaRole> findRoleByUserId(Map<String, Object> map);
	
	void updateByUserRoleIdList(Map<String, Object> map);
}
