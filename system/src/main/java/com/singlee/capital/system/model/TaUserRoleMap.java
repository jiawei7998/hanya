package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 柜员角色对应关系
 * @author cz
 */

@Entity
@Table(name = "TA_USER_ROLE_MAP")
public class TaUserRoleMap  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String roleId;//角色ID
	@Id
	private String userId;//柜员ID
	
	public TaUserRoleMap() {
	}

	public TaUserRoleMap(String roleId, String userId) {
		this.roleId = roleId;
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
