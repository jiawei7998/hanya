package com.singlee.capital.system.controller;

import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.model.TtInstitutionSettls;
import com.singlee.capital.system.service.InstitutionSettlsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

@Controller
@RequestMapping(value = "/InstitutionSettlsController")
public class InstitutionSettlsController extends CommonController {

	@Autowired
	private InstitutionSettlsService institutionSettlsService;
	
	/**
	 * 分页查询列表
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/settlsList")
	public RetMsg<PageInfo<TtInstitutionSettls>> institutionSettlsList(@RequestBody Map<String, Object> map) {
		
		Page<TtInstitutionSettls> page=institutionSettlsService.getPageSettls(map);
		return RetMsgHelper.ok(page);
	}
	
	/**
	 * 删除机构清算
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteSettl")
	public RetMsg<Serializable> deleteInstitutionSettl(@RequestBody Map<String, Object> map) {
		institutionSettlsService.deleteInstitutionSettl(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 新增机构清算
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addInstitutionSettl")
	public RetMsg<Serializable> addInstitutionSettl(@RequestBody TtInstitutionSettls institutionSettls) {
		institutionSettlsService.saveInstitutionSettl(institutionSettls);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 修改机构清算
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateInstitutionSettl")
	public RetMsg<Serializable> updateInstitutionSettl(@RequestBody TtInstitutionSettls institutionSettls) {
		institutionSettlsService.updateInstitutionSettl(institutionSettls);
		return RetMsgHelper.ok();
	}
}
