package com.singlee.capital.system.controller;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.model.TaFunction;
import com.singlee.capital.system.model.TaModule;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.ModuleService;
import com.singlee.capital.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
/**
 * 模块菜单 相关 
 * @author cz
 *
 */
@Controller
@RequestMapping(value = "/ModuleController")
public class ModuleController  extends CommonController {
	
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private RoleService roleService;
	
    /**
     * 获取菜单列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/modules")
	public RetMsg<List<TaModule>> modules(){
		TaUser user = getThreadLocalUser();
		List<TaModule> page = moduleService.recursionModule(user==null?null:user.getRoleIdList());
		return RetMsgHelper.ok(page); 
	}
	  /**
     * 获取菜单列表
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/module")
	public RetMsg<Serializable> module(){
		return RetMsgHelper.simple("error.base.0001"); 
//		TaUser user = getThreadLocalUser();
//		LinkedList<TreeModule> page = moduleService.recursionModule(user.getRoleIdList());
		
	}
	/**
	 * 更新菜单的模块配置
	 * @param mapgetModuleByBranchIdAndModuleId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/updateModuleDesk")
	public RetMsg<Serializable> updateModuleDesk(@RequestBody Map<String, Object> map){
		moduleService.updateModuleDesk(map);
		return RetMsgHelper.ok();
	}
	/**
	 * 
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getModuleByBranchIdAndModuleId")
	public RetMsg<TaModule> getModuleByBranchIdAndModuleId(@RequestBody Map<String, Object> map){
		TaModule module = moduleService.getModuleByBranchIdAndModuleId(map);
		return RetMsgHelper.ok(module);
	}
	
	@ResponseBody
	@RequestMapping(value = "/updateModuleBranchId")
	public RetMsg<Serializable> updateModuleBranchId(@RequestBody Map<String, Object> map){
	    moduleService.updateModuleBranchId(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 保存module--格式化module_id
	 * @param map 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveModuleByBranchId")
	public RetMsg<Serializable> saveModuleByBranchId(@RequestBody Map<String, Object> map){
		moduleService.saveModuleByBranchId(map);
		return RetMsgHelper.ok();
	}
	/**
	 * 模块功能点绑定
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveModuleFunction")
	public RetMsg<Serializable> saveModuleFunction(@RequestBody Map<String, Object> map){
		moduleService.saveModuleFunction(map);
		return RetMsgHelper.ok();
	}
	@ResponseBody
	@RequestMapping(value = "/getHavingFuncId")
	public RetMsg<List<String>> getHavingFuncId(@RequestBody Map<String, Object> map){
		List<String> list=moduleService.getHavingFuncId(map);
		return RetMsgHelper.ok(list);
	}
	@ResponseBody
	@RequestMapping(value = "/getUserModule")
	public RetMsg<List<String>> getUserModule(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(moduleService.getUserModule(map));
	}

	@ResponseBody
	@RequestMapping(value = "/searchDeskModuleList")
	public RetMsg<List<TaModule>> searchDeskModuleList(@RequestBody HashMap<String, Object> map) throws RException {
		List<TaModule> list = moduleService.getDeskModuleList();
		String roleId = ParameterUtil.getString(map,"roleId","");
		String branchId = ParameterUtil.getString(map,"branchId","");

		List<TaFunction> function = roleService.searchFunctionByRoleId(roleId, branchId, null);
		Map<String, List<TaFunction>> roleList = function.stream().collect(Collectors.groupingBy(TaFunction::getModuleId));

		for (TaModule module:list) {//台子
			List<TaModule> children1 = module.getChildren();
			for (TaModule module1:children1) {//一级菜单
				List<TaModule> children2 = module1.getChildren();
				for (TaModule module2:children2) {//二级菜单
					List<TaFunction> functionList = roleList.get(module2.getModuleId().trim());
					module2.setFunctions(functionList);
				}
			}
		}
		System.out.println("list结果为"+list);
		return RetMsgHelper.ok(list);
	}
}
