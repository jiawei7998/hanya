package com.singlee.capital.system.session.impl;

import com.singlee.capital.common.session.SessionStaticUtil;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.model.TaBrcc;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;

import java.util.List;

/**
 * SlSession 帮助类，指定了 机构/柜员等，需要在slsession中存储的对象
 * 
 * @author x230i
 * 
 */
public class SlSessionHelper {

	/**
	 * 
	 * @return
	 */
	public static SlSession getSlSession() {
		return SessionStaticUtil.getSlSessionThreadLocal();
	}

	/**
	 * 机构
	 */
	private static final String KeyInstitution = "Institution";

	public static void setInstitution(SlSession session, TtInstitution inst) {
		session.put(KeyInstitution, inst);
	}

	public static TtInstitution getInstitution(SlSession session) {
		return session == null ? null : (TtInstitution) session.get(KeyInstitution);
	}

	public static String getInstitutionId(SlSession session) {
		TtInstitution inst = getInstitution(session);
		return inst == null ? null : inst.getInstId();
	}

	public static TtInstitution getInstitution() {
		SlSession session = getSlSession();
		return session != null ? getInstitution(session) : null;
	}

	public static String getInstitutionId() {
		TtInstitution inst = getInstitution();
		return inst != null ? inst.getInstId() : null;
	}

	public static List<String> getSubInstitutionsId() {
		String instId = getInstitutionId();
		InstitutionService is = SpringContextHolder.getBean(InstitutionService.class);
		return is.childrenInstitutionIds(instId);
	}

	/**
	 * BRCC
	 */
	private static final String KeyBrcc = "Brcc";

	public static void setBrcc(SlSession session, TaBrcc brcc) {
		session.put(KeyBrcc, brcc);
	}

	public static TaBrcc getBrcc(SlSession session) {
		return session == null ? null : (TaBrcc) session.get(KeyBrcc);
	}

	/**
	 * 获取机构类型
	 * 
	 * @return
	 */
	public static String getInstitutionType() {
		SlSession session = getSlSession();
		TtInstitution inst = getInstitution(session);
		return inst == null ? null : inst.getInstType();
	}

	public static String getInstitutionType(SlSession session) {
		TtInstitution inst = getInstitution(session);
		return inst == null ? null : inst.getInstType();
	}

	/**
	 * 机构是否是总行
	 * 
	 * @return
	 */
	public static boolean isInstitutionTypeHeader() {
		return ExternalDictConstants.InstitutionType_Head.equals(getInstitutionType());
	}

	public static String isInstitutionTypeFt(SlSession session) {
		TtInstitution inst = getInstitution(session);
		String isFt = "1".equalsIgnoreCase(inst == null ? "0" : (inst.getIsFta() == null ? "0" : inst.getIsFta())) ? "_FT" : "";
		return isFt;
	}

	public static boolean isInstitutionTypeHeader(SlSession session) {
		return ExternalDictConstants.InstitutionType_Head.equals(getInstitutionType(session));
	}

	/**
	 * 是否匿名
	 */
	private static final String KeyAnonymity = "Anonymity";

	public static void setAnonymity(SlSession session, Boolean bl) {
		session.put(KeyAnonymity, bl);
	}

	public static Boolean getAnonymity(SlSession session) {
		return (Boolean)session.get(KeyAnonymity);
	}


	/**
	 * 柜员
	 */
	public static final String KeyUser = "User";

	public static void setUser(SlSession session, TaUser user) {
		session.put(KeyUser, user);
	}

	public static String getEmpId(SlSession session) {
		TaUser u = getUser(session);
		return u == null ? null : u.getEmpId();
	}

	public static String getEmpId() {
		TaUser u = getUser();
		return u != null ? u.getEmpId() : null;
	}

	public static String getUserId(SlSession session) {
		TaUser u = getUser(session);
		return u == null ? null : u.getUserId();
	}

	public static TaUser getUser(SlSession session) {
		if (session == null) {
			return null;}
		return (TaUser) session.get(KeyUser);
	}

	public static TaUser getUser() {
		SlSession session = getSlSession();
		return session != null ? getUser(session) : null;
	}

	public static String getUserId() {
		TaUser u = getUser();
		return u != null ? u.getUserId() : null;
	}

	/**
	 * 所属机构编号 为0 的用户，为admin
	 * 
	 * @return
	 */
	public static boolean isAdmin(SlSession session) {
		TaUser u = getUser(session);
		return u != null ? "0".equals(u.getInstId()) : false;
	}

	public static boolean isAdmin() {
		TaUser u = getUser();
		return u != null ? "0".equals(u.getInstId()) : false;
	}

	/**
	 * @return
	 */
	// private static final String KeyInCashAcc = "InCashAcc";

	// public static String getInCashAccId(){
	// TtAccInCash getInCashAcc = getInCashAcc();
	// return getInCashAcc ==null ? "" : getInCashAcc.getAccid();
	// }
	//
	// public static TtAccInCash getInCashAcc(){
	// SlSession session = getSlSession();
	// return session==null?null:(TtAccInCash)session.get(KeyInCashAcc);
	// }
	//
	// public static void setAccInCash(SlSession session, TtAccInCash
	// accByInstId) {
	// session.put(KeyInCashAcc, accByInstId);
	// }

	/**
	 * ip
	 */
	private static final String KeyIp = "IP";

	public static void setIp(SlSession session, String ip) {
		session.put(KeyIp, ip);
	}

	public static String getIp(SlSession session) {
		return session == null ? "0.0.0.0" : (String) session.get(KeyIp);
	}

	/**
	 * ua
	 */
	private static final String KeyUa = "Usre-Agent";

	public static void setUa(SlSession session, String ua) {
		session.put(KeyUa, ua);
	}

	public static String getUa(SlSession session) {
		return session == null ? "" : (String) session.get(KeyUa);
	}

	/**
	 * ua
	 */
	private static final String KeyUri = "URI";

	public static void setUri(SlSession session, String ua) {
		session.put(KeyUri, ua);
	}

	public static String getUri(SlSession session) {
		return session == null ? "" : (String) session.get(KeyUri);
	}

}
