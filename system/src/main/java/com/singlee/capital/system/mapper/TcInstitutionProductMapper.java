package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TcInstitutionProduct;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TcInstitutionProductMapper extends Mapper<TcInstitutionProduct> {

	public void deleteInstitutionProduct(String instId);
	
	public List<String> getPrdNames(Map<String, Object> map);
	
	public List<String> getPrdNos(Map<String, Object> map);
}
