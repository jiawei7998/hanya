package com.singlee.capital.system.service.impl;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.dict.ModuleTypeConstants;
import com.singlee.capital.system.mapper.TaModuleFunctionMapMapper;
import com.singlee.capital.system.mapper.TaModuleMapper;
import com.singlee.capital.system.mapper.TaRoleModuleMapMapper;
import com.singlee.capital.system.model.TaModule;
import com.singlee.capital.system.model.TaModuleFunctionMap;
import com.singlee.capital.system.model.TaRoleModuleMap;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.ModuleService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 模块服务
 * 
 * @author Lyon Chen
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ModuleServiceImpl implements ModuleService {
	private static final  String cacheNamePageUrl="com.singlee.capital.system.service.TotalPageUrls";
	@Autowired
	private TaModuleMapper taModuleMapper;
	@Autowired
	private TaModuleFunctionMapMapper taModuleFunctionMapMapper;
	@Autowired
	TaRoleModuleMapMapper taRoleModuleMapMapper;


	@Override
	public List<TaModule> recursionModule(List<String> list) {
		List<TaModule> orignial = taModuleMapper.recursionModule(list);
		return TreeUtil.mergeChildrenList(orignial, "0");
	}	
//	@Override
//	@Cacheable(value = cacheName, key="#role")
//	public LinkedList<TreeModule> recursionModule(String role){
//		List<String> roleList = new LinkedList<String>();
//		roleList.add(role);
//		LinkedList<TaModule> orignial = taModuleMapper.recursionModule(roleList);
//		return createModleList(orignial);
//	}

	/***
	 * ----以上为对外服务-------------------------------
	 */

	@SuppressWarnings("unused")
	private ModuleServiceImpl myself() {
		return SpringContextHolder.getBean(ModuleServiceImpl.class);
	}

	@Override
	public List<String> getUserModule(Map<String, Object> map) {
		TaUser user = SlSessionHelper.getUser();
		List<String> moduleNameList = new ArrayList<String>();
		if(user != null) {
			List<String> moduleIdList = taModuleMapper.getChildrenModuleIdList(map);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("pModuleId", map.get("pModuleId"));
			param.put("userId", user.getUserId());
			param.put("moduleIdList", moduleIdList);
			param.put("moduleType", ModuleTypeConstants.FIFTH);
			List<TaModule> moduleList = taModuleMapper.getUserModule(param);
			for(TaModule module : moduleList) {
				moduleNameList.add(module.getModuleName());
			}
		}
		return moduleNameList;
	}

	@Override
	public List<TaModule> getMenuUrlsByUserId(String userId) {
		// TODO Auto-generated method stub
		return this.taModuleMapper.getTaModulesByUserId(userId);
	}
	
	@Override
	@Cacheable(value = cacheNamePageUrl,key="#root.methodName")
	public List<TaModule> getTotalPageUrls(){
		return this.taModuleMapper.getTotalPageUrls();
	}

	@Override
	public List<TaModule> getDeskModuleList() {
		return TreeUtil.mergeChildrenList(taModuleMapper.getDeskModuleList(), "-1");
	}

	@Override
	public void updateModuleDesk(Map<String, Object> map) {
		//先释放选中的模块 然后在赋值
		taModuleMapper.updateModuleDeskIsNull(map);
		taModuleMapper.updateModuleDesk(map);
	}

	@Override
	public void saveModuleByBranchId(Map<String, Object> map) {
		TaModule taModule =new TaModule();
		String moduleId1="";
		//新增模块时，节点挂靠模块
		if(map.get("topNodeType")!=null && "1".equals(map.get("topNodeType"))){
			taModule.setModulePid((String)map.get("ModulePid"));//父节点Id
			taModule.setModuleName((String)map.get("deskName"));//节点名称
			taModule.setIsActive((String)map.get("IsActive"));//是否启用
			taModule.setBranchId((String)map.get("branchId"));//银行Id
			taModule.setModuleUrl((String)map.get("deskUrl"));//moduleurl
			String maxModule=taModuleMapper.getModuleMax(map);
			Integer moduleId=null;
			if(maxModule!=null){
				moduleId=Integer.valueOf(maxModule)+1;
				moduleId1=(String)map.get("ModulePid")+"-"+moduleId.toString();
			}else{
				moduleId=1;
				moduleId1=(String)map.get("ModulePid")+"-"+moduleId.toString();
			}
			 
			taModule.setModuleId(moduleId1);
			taModule.setModuleIcon("icon-module-xitongcanshu");
			Integer moduleSortno =taModuleMapper.getModuleSortNoMax(map);
			if(moduleSortno!=null){
				taModule.setModuleSortno(moduleSortno);
			}else{
				taModule.setModuleSortno(1);
			}
			
		}else{//新增模块时，节点挂靠台子
			//保存顶级菜单模块--两种情况  第一：已经存在的台子下边增加模块 第二：新增加的台子下边挂靠模块
			
			taModule.setModulePid("-1");//父节点Id
			taModule.setModuleName((String)map.get("deskName"));//节点名称
			taModule.setIsActive((String)map.get("IsActive"));//是否启用
			taModule.setBranchId((String)map.get("branchId"));//银行Id
			//获取一级目录的最大值 
			Integer moduleMax=taModuleMapper.getModuleOneLevelMax(map);
			
			if(moduleMax==null){
				moduleId1="1-1";
				taModule.setModuleId(moduleId1);
				
			}else{
				moduleId1=String.valueOf(moduleMax+1)+"-1";
				taModule.setModuleId(moduleId1);
			}
			/*Integer moduleRightMax=1;
			if(moduleMax!=null){
				map.put("moduleMax", moduleMax.toString());
				Integer str=taModuleMapper.getModuleOneLevelRightMax(map);
				if(str!=null){
					moduleRightMax=str;
				}
			}
			taModule.setModuleId(String.valueOf(moduleMax)+"-"+moduleRightMax.toString());//解决
			*/
			
			
			//taModule.setModuleIcon("product_transaction_14");
			taModule.setModuleIcon("fk");
			//获取排序信息:根据modulepid=-1,branchid,deskid
			Integer moduleSortnoOneLevel=taModuleMapper.getModuleSortOneLevelMax(map);
			if(moduleSortnoOneLevel!=null){
				taModule.setModuleSortno(moduleSortnoOneLevel);
			}else{
				taModule.setModuleSortno(1);
			}
			
			taModule.setDeskId((String)map.get("ModulePid"));//此处获取的是菜单父节点的值--可以看页面
			
			
			
			
		}
		
		taModuleMapper.saveModuleByBranchId(taModule);
		String roleId = ParameterUtil.getString(map, "roleId", "");
		TaRoleModuleMap taRoleModuleMap = new TaRoleModuleMap();
		taRoleModuleMap.setRoleId(roleId);
		taRoleModuleMap.setModuleId(moduleId1);
		taRoleModuleMap.setAccessFlag("1");
		taRoleModuleMapMapper.insert(taRoleModuleMap);
		
	}

	@Override
	public TaModule getModuleByBranchIdAndModuleId(Map<String, Object> map) {
		
		return taModuleMapper.getModuleByBranchIdAndModuleId(map);
	}

	@Override
	public void updateModuleBranchId(Map<String, Object> map) {
		taModuleMapper.updateModuleBranchId(map);
	}
	
	@Override
	public void saveModuleFunction(Map<String, Object> map) {
		//保存之前把原来的绑定关系删除
		taModuleFunctionMapMapper.deleteByModuleId(map);
		TaModuleFunctionMap func =new TaModuleFunctionMap();
		String functionIds =(String)map.get("functionId");
		String[] strs=functionIds.split(",");
		List<String> list = Arrays.asList(strs);
		for(String str:list){
			func.setModuleId((String)map.get("moduleId"));
			func.setFunctionId(str);
			taModuleFunctionMapMapper.insert(func);
		}
	}

	@Override
	public List<String> getHavingFuncId(Map<String, Object> map) {
		List<String> list =taModuleFunctionMapMapper.getHavingFuncId(map);
		return list;
	}
}
