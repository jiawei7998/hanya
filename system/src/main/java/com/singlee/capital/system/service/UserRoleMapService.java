package com.singlee.capital.system.service;

import com.singlee.capital.system.model.TaUserRoleMap;

import java.util.List;

public interface UserRoleMapService {

	/**
	 * 根据对象查询列表
	 * 
	 * @param userRoleMap - 用户角色对象
	 * @return 用户角色对象列表
	 * @author Hunter
	 * @date 2016-7-25
	 */
	public List<TaUserRoleMap> getAllUserRole(TaUserRoleMap userRoleMap);
	
	/**
	 * 新增
	 * 
	 * @param userRole - 用户角色对象
	 * @author Hunter
	 * @date 2016-7-25
	 */
	public void saveUserRole(TaUserRoleMap userRoleMap);
}
