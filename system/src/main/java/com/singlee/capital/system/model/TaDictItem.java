package com.singlee.capital.system.model;

import org.apache.commons.lang.StringUtils;
import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
/**
 *  字典项 
 * @author lyon chen
 */

@Entity
@Table(name = "TA_DICT_ITEM")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TaDictItem implements Serializable {

	private static final long serialVersionUID = -8893309615800451101L;

	/** 字典项代码 */
	private String dict_key;
	/** 字典排序  */
	private int dict_sort;
	/** 是否启用 */
	private char is_active;
	/** 子项  */
	private String dict_folder;
	
	
	public String getDict_key() {
		return dict_key;
	}

	/** 
	 * [1][2][3][4] ---> ["1","2","3","4"] 
	 */
	public String getDict_folder2() {
		if(StringUtils.isEmpty(dict_folder)){
			return "[]";
		}else{
			String s = dict_folder.replaceAll("\\]\\[", "\",\"");
			s = s.replaceAll("\\[", "[\"");
			s = s.replaceAll("\\]", "\"]");
			return s;
		}
	}
	public String getDict_folder() {
		return dict_folder;
	}
	public void setDict_folder(String dict_folder) {
		this.dict_folder = dict_folder;
	}
	public void setDict_key(String dict_key) {
		this.dict_key = dict_key;
	}
	public int getDict_sort() {
		return dict_sort;
	}
	public void setDict_sort(int dict_sort) {
		this.dict_sort = dict_sort;
	}
	
	public char getIs_active() {
		return is_active;
	}
	public void setIs_active(char is_active) {
		this.is_active = is_active;
	}
	public TaDictItem() {
	}
	
}
