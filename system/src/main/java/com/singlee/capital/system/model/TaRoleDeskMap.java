package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TA_ROLE_DESK_MENU_MAP")
public class TaRoleDeskMap implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8490972760429430082L;

	@Id
	private String roleId;//角色Id
	@Id
	private String deskId;//台子id
	
	private String accessFlag;//访问标识
	
	private String deskUrl;//台子url
	

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getDeskId() {
		return deskId;
	}

	public void setDeskId(String deskId) {
		this.deskId = deskId;
	}

	public String getAccessFlag() {
		return accessFlag;
	}

	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}

	public String getDeskUrl() {
		return deskUrl;
	}

	public void setDeskUrl(String deskUrl) {
		this.deskUrl = deskUrl;
	}
	
	
	
	

}
