package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TtInstitutionSerial;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TtInstitutionSerialMapper extends Mapper<TtInstitutionSerial> {
	
	public List<String> searchSerialChildren(@Param("instId")String instId);
	
	public void deleteSerialChildren(@Param("instId")String instId);
	
}