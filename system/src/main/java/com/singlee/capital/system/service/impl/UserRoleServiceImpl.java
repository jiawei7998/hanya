package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.ApproveStatusConstants;
import com.singlee.capital.system.mapper.TaUserRoleMapCopyMapper;
import com.singlee.capital.system.mapper.TaUserRoleMapMapper;
import com.singlee.capital.system.model.TaRole;
import com.singlee.capital.system.model.TaUserRoleMap;
import com.singlee.capital.system.model.TaUserRoleMapCopy;
import com.singlee.capital.system.service.UserRoleService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
	private TaUserRoleMapCopyMapper userRoleMapCopyMapper;
	@Autowired
	private TaUserRoleMapMapper userRoleMapMapper;
	
	@Override
	public Page<TaUserRoleMapCopy> getWaitApproveList(Map<String, Object> map) {
		map.put("approveStatus", ApproveStatusConstants.APPROVE_ING);
		return userRoleMapCopyMapper.findByUserIdAndApproveStatus(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public List<TaRole> getOldRoleList(Map<String, Object> map) {
		return userRoleMapCopyMapper.findRoleByUserId(map);
	}

	@Override
	public List<TaRole> getNewRoleList(Map<String, Object> map) {
		String userRoleIdStr = ParameterUtil.getString(map, "userRoleId", "");
		String[] userRoleIdArray = userRoleIdStr.split(",");
		List<String> userRoleIdList = Arrays.asList(userRoleIdArray);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userRoleIdList", userRoleIdList);
		return userRoleMapCopyMapper.findByUserRoleIdList(param);
	}

	@Override
	public Page<TaUserRoleMapCopy> getApprovedList(Map<String, Object> map) {
		map.put("approveStatus", ApproveStatusConstants.APPROVE_ED);
		return userRoleMapCopyMapper.findByUserIdAndApproveStatus(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void doApprove(Map<String, Object> map) {
		String userRoleIds = ParameterUtil.getString(map, "userRoleIds", "");
		String userId = ParameterUtil.getString(map, "userId", "");
		String[] userRoleIdArray = userRoleIds.split(",");
		List<String> userRoleIdList = Arrays.asList(userRoleIdArray);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userRoleIdList", userRoleIdList);
		param.put("currentUserId", SlSessionHelper.getUserId());
		param.put("approveStatus", ApproveStatusConstants.APPROVE_ED);
		param.put("approveDate", DateTimeUtil.getLocalDateTime());
		userRoleMapCopyMapper.updateByUserRoleIdList(param);
		userRoleMapMapper.deleteUserRoleByUserId(userId);
		for(String userRoleId : userRoleIdList) {
			TaUserRoleMapCopy userRole = userRoleMapCopyMapper.selectByPrimaryKey(userRoleId);
			TaUserRoleMap userRoleMap = new TaUserRoleMap();
			userRoleMap.setUserId(userId);
			userRoleMap.setRoleId(userRole.getRoleId());
			userRoleMapMapper.insert(userRoleMap);
		}
	}
}
