package com.singlee.capital.system.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.service.BrccService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 银行归属
 * 
 * @author SINGLEE
 * 
 */
@Controller
@RequestMapping(value = "/brccController")
public class TaBrccController {
	@Autowired
	BrccService brccService;

	/**
	 * 获取银行列表
	 * 
	 * @param params
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getBrccs")
	public RetMsg<?> getBrccs() {
		return RetMsgHelper.ok(brccService.getAllTaBrcc());
	}
}
