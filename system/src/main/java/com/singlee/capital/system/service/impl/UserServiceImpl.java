package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.*;
import com.singlee.capital.system.dict.*;
import com.singlee.capital.system.mapper.*;
import com.singlee.capital.system.model.*;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.service.SystemProperties;
import com.singlee.capital.system.service.UserRoleMapService;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.codec.binary.Base64;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.*;

/**
 * 用户管理service实现
 * 
 * @author lihb
 * @date 2016-07-06
 * @company 杭州新利科技有限公司
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class UserServiceImpl implements UserService {

	/* 用户缓存 */
	// private static final String cacheName =
	// "com.singlee.capital.system.users";

	/* 用户mapper层 */
	@Autowired
	private TaUserMapper userMapper;

	/* 机构service */
	@Autowired
	private InstitutionService institutionService;

	@Autowired
	private TaRoleMapper roleMapper;

	@Autowired
	private TaUserInfoMapper userInfoMapper;

	@Autowired
	private TtTotalInstitutionMapper totalInstitutionMapper;
	@Autowired
	private TaUserInstMapper taInstMapper;
	@Autowired
	private UserRoleMapService userRoleMapService;

	@Override
	// @Cacheable(value = cacheName, key="#userId")
	public TaUser getUserById(String userId) {
		TaUser u = userMapper.selectUser(userId);
		return u;
	}

	@Override
	public Page<TaUser> getPageUser(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		// 若查询条件里含有机构，则查出所有子机构
		String instId = ParameterUtil.getString(params, "instId", null);
		if (!(StringUtil.isNullOrEmpty(instId))) {
			List<String> instIds = institutionService.childrenInstitutionIds(instId);
			instIds.add(instId);
			// params.remove("instId");
			params.put("instIds", instIds);
		}
		TaUser user = SlSessionHelper.getUser();
		TtInstitution ttInstitution = SlSessionHelper.getInstitution();
		List<String> userInstIdList = new ArrayList<String>();
		if (user != null && ttInstitution.getInstId() != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("userId", user.getUserId());
			map.put("currentInstId", ttInstitution.getInstId());
			map.put("branchId", params.get("branchId"));
			List<TaRole> roleList = roleMapper.getRoleByUserIdAndRoleLevel(map);

			if (roleList.isEmpty()) {
				// 用户还未分配角色，只能管理自己
				params.put("userId", user.getUserId());
			} else {
				// 获得机构向下的所有机构
				HashMap<String, Object> map2 = new HashMap<String, Object>();
				map2.put("instId", ttInstitution.getInstId());
				List<TtInstitution> institutions = institutionService.searchChildrenInst(map2);
				userInstIdList.add(ttInstitution.getInstId());
				for (TtInstitution institution : institutions) {
					userInstIdList.add(institution.getInstId());
				}
				params.put("userInstIdList", userInstIdList);
			}
		}
		Page<TaUser> result = userMapper.getPageUser(params, rowBounds);
		return result;
	}

	@Override
	public Page<TaUser> getPageInvitationUser(Map<String, Object> param) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(param);
		HashMap<String, Object> instParam = new HashMap<String, Object>();
		instParam.put("instId", SlSessionHelper.getInstitutionId());
		List<TtInstitution> pInstList = institutionService.searchParentInst(SlSessionHelper.getInstitutionId());
		List<String> instIds = new ArrayList<String>();
		for (TtInstitution item : pInstList) {
			instIds.add(item.getInstId());
		}
		instIds.add(SlSessionHelper.getInstitutionId());
		param.put("instIds", instIds);
		Page<TaUser> result = userMapper.getPageUser(param, rowBounds);
		return result;
	}

	@Override
	@AutoLogMethod(value = "创建用户")
	public void createUser(TaUser user) throws RException {
		String[] inst = user.getInstId().split(",");
		TaUser u = userMapper.selectByPrimaryKey(user);
		if (u != null) {
			JY.raiseRException("用户已存在或用户正在审批");
		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("empId", user.getEmpId());
		if (user.getEmpId() != null) {
			List<TaUser> userEmpList = userMapper.findByEmpId(map);
			if (!userEmpList.isEmpty()) {
				JY.raiseRException("该用户工号已存在或正在审批");
			}
		}
		TaUser taUser = SlSessionHelper.getUser();
		String currentInstId = SlSessionHelper.getInstitutionId();
		if (taUser != null && currentInstId != null) {
			List<String> instIdList = institutionService.childrenInstitutionIds(currentInstId);
			instIdList.add(currentInstId);
			for (int i = 0; i < inst.length; i++) {
				if (!instIdList.contains(inst[i])) {
					JY.raiseRException("只能选择从属机构");
				}
			}

		}
		/*
		 * int count = userMapper.selectCountByExample(user); if (count > 0) {
		 * JY
		 * .raiseRException(ErrorUtils.getBaseErrorMessage("error.system.0005")
		 * ); }
		 */
		user.setUserCreatetime(DateTimeUtil.getLocalDateTime());

		if (taUser != null) {
			user.setApproveStatus(ApproveStatusConstants.MANAGER_OP);
			user.setOpratorType(OpratorConstants.ADD);
			user.setSubmitUserId(SlSessionHelper.getUserId());
			user.setSubmitInstId(SlSessionHelper.getInstitutionId());
			user.setIsActive("1");
			user.setIsAccess("1");// 0-不能直接LOGIN界面登陆 1-可以
			// if(isTotalManager(taUser.getUserId())) {
			// user.setApproveStatus(ApproveStatusConstants.MANAGER_OP);
			// }else{
			// user.setOpratorType(OpratorConstants.ADD);
			// user.setApproveStatus(ApproveStatusConstants.APPROVE_ING);
			// user.setSubmitUserId(SlSessionHelper.getUserId());
			// user.setSubmitInstId(SlSessionHelper.getInstitutionId());
			// }
		}
		// 多机构时候保存机构与用户信息
		if (user.getInstId() != null) {
			if (inst.length > 0) {
				for (int i = 0; i < inst.length; i++) {
					TaUserInst record = new TaUserInst();
					record.setInstId(inst[i]);
					record.setUserId(user.getUserId());
					taInstMapper.insert(record);
				}
			}
		}

		//多角色,保存用户-角色信息
		if(user.getRoles() != null){
			TaUserRoleMap taUserRoleMap = new TaUserRoleMap(user.getRoles(),user.getUserId());
			userRoleMapService.saveUserRole(taUserRoleMap);
		}

		//用户加盐处理
		user.setUserPwd(MD5Util.MD5(user.getUserId()+SHAUtil.SHA512(SystemProperties.empId_passwd.trim()).toUpperCase()));
		userMapper.insert(user);
	}

	@Override
	public void updateUser(TaUser user) {
		TaUser u = userMapper.selectByPrimaryKey(user);
		if (u == null) {
			JY.raiseRException(MsgUtils.getMessage("error.system.0003"));
		}
		if (ExternalDictConstants.YesNo_NO.equals(u.getIsActive())) {
			JY.raiseRException(MsgUtils.getMessage("error.system.0006"));
		}
		TaUser taUser = SlSessionHelper.getUser();
		String currentInstId = SlSessionHelper.getInstitutionId();
		if (taUser != null && currentInstId != null) {
			List<String> instIdList = institutionService.childrenInstitutionIds(currentInstId);
			instIdList.add(currentInstId);
			if (!instIdList.contains(user.getInstId())) {
				JY.raiseRException("只能选择从属机构");
			}
		}
		// 该代码引起了柜员修改出现BUG，由于不确定是否在其他地方有使用，暂时注解
		// if (u.getUserName().equals(user.getUserName())) {
		// JY.raiseRException(MsgUtils.getMessage("error.system.0005"));
		// }
		
		if (isTotalManager(taUser.getUserId())) {
			userMapper.updateByPrimaryKey(user);
		} else {
			String userIdCopy = user.getUserId() + "_copy";
			user.setUserId(userIdCopy);
			user.setOpratorType(OpratorConstants.EDIT);
			user.setApproveStatus(ApproveStatusConstants.APPROVE_ING);
			user.setSubmitUserId(taUser.getUserId());
			user.setSubmitInstId(currentInstId);
			if (userMapper.selectByPrimaryKey(userIdCopy) == null) {
				userMapper.insert(user);
			} else {
				userMapper.updateByPrimaryKey(user);
			}
		}
	}

	@Override
	public void deleteUser(String[] userIdList) {
		for (int i = 0; i < userIdList.length; i++) {
			TaUser u = userMapper.selectByPrimaryKey(userIdList[0]);
			if (u == null) {
				JY.raiseRException(MsgUtils.getMessage("error.system.0003"));
			}
			if (ExternalDictConstants.YesNo_NO.equals(u.getIsActive())) {
				JY.raiseRException(MsgUtils.getMessage("error.system.0006"));
			}
			userMapper.deleteByPrimaryKey(userIdList[i]);
		}
	}

	@Override
	public void switchUserState(String[] userIdList) {
		TaUser u = userMapper.selectByPrimaryKey(userIdList[0]);
		TaUser user = new TaUser();
		JY.require(u != null, MsgUtils.getMessage("error.system.0003"));
		if (ExternalDictConstants.YesNo_YES.equals(u.getIsActive())) {
			user.setIsActive(ExternalDictConstants.YesNo_NO);
		} else {
			user.setIsActive(ExternalDictConstants.YesNo_YES);
		}
		user.setBranchId(SlSessionHelper.getUser().getBranchId());
		for (int i = 0; i < userIdList.length; i++) {
			user.setUserId(userIdList[i]);
			userMapper.updateUserState(user);
		}
	}

	@Override
	public void resetUserPwd(String[] userIdList, String userPwd) {
		TaUser user = new TaUser();
		for (int i = 0; i < userIdList.length; i++) {
			user.setUserId(userIdList[i]);
			user.setBranchId(SlSessionHelper.getUser().getBranchId());
			//用户加盐处理
	        user.setUserPwd(MD5Util.MD5(user.getUserId()+userPwd));
			userMapper.updateUserPwd(user);
		}
	}
	
	@Override
	public void resetUserPwds(String userPwd) {
		TaUser user = new TaUser();
		String branchId=SlSessionHelper.getUser().getBranchId();
		user.setBranchId(branchId);
		List<TaUser> userIdList=userMapper.getAllUserByIds(branchId);
		for (int i = 0; i < userIdList.size(); i++) {
		user.setUserId(userIdList.get(i).getUserId());
			//用户加盐处理
	    user.setUserPwd(MD5Util.MD5(user.getUserId()+userPwd));
		userMapper.updateUserPwd(user);
		}
	}

	@Override
	public void changeUserPwd(Map<String, Object> map) {
		// 密码加密在前台进行，防止密码泄漏
		String oldPwd = ParameterUtil.getString(map, "oldPwd", "");
		String newPwd = ParameterUtil.getString(map, "newPwd", "");
		// 先校验原密码是否正确，不正确则终止密码修改
		
		JY.require(SlSessionHelper.getUser().getUserPwd().equals(MD5Util.MD5(SlSessionHelper.getUser().getUserId()+oldPwd)), "原密码输入错误.");

		TaUser user = new TaUser();
		user.setUserId(SlSessionHelper.getUserId());
		//用户加盐处理
        user.setUserPwd(MD5Util.MD5(user.getUserId()+newPwd));
		user.setBranchId(SlSessionHelper.getUser().getBranchId());
		userMapper.updateUserPwd(user);
	}

	/**
	 * 根据审批角色查询柜员及角色
	 * 
	 * @param flowRoleId
	 *            审批角色ID
	 * @author wang
	 * @date 2016-9-28
	 */
	@Override
	public List<TaUser> selectUserWithRoleName(String flowRoleId) {
		return userMapper.selectUserWithRoleName(flowRoleId);
	}

	/**
	 * 根据审批角色,机构ID 查询柜员及角色
	 * 
	 * @param flowRoleId
	 *            - 角色id
	 * @param instId
	 *            - 机构id
	 * @author lijing
	 * @date 2018-1-10
	 */
	@Override
	public Page<TaUser> selectUserByRoleNameAndInstId(Map<String, Object> map) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(map);
		return userMapper.selectUserByRoleNameAndInstId(map, rowBounds);
	}

	/**
	 * 查询柜员by机构1，机构2，。。。
	 * 
	 * @param map
	 * @author wangchen
	 * @date 2017-6-15
	 */
	@Override
	public List<TaUser> selectUserWithInstIds(Map<String, Object> params) {
		String instIds = ParameterUtil.getString(params, "instIds", null);
		List<String> list = null;
		params.remove("instIds");
		if (!(StringUtil.isNullOrEmpty(instIds))) {
			list = Arrays.asList(instIds.split(","));
			params.put("instIds", list);
		}
		return userMapper.selectUserWithInstIds(params);
	}

	/**
	 * 查询用户对象列表(带分页)
	 * 
	 * @param
	 * @return 用户对象列表(带分页)
	 * @author lijing
	 * @date 2018-1-11
	 */
	@Override
	public Page<TaUser> selectUserWithInstIdsPage(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		String instIds = ParameterUtil.getString(params, "instIds", null);
		List<String> list = null;
		params.remove("instIds");
		if (!(StringUtil.isNullOrEmpty(instIds))) {
			list = Arrays.asList(instIds.split(","));
			params.put("instIds", list);
		}
		return userMapper.selectUserWithInstIdsPage(params, rowBounds);
	}

	/**
	 * 查询用户个人信息
	 */
	@Override
	public TaUser searchTaUserbyMap(Map<String, Object> map) {
		return userMapper.pageUser(map);
	}

	@Override
	public TaUser queryUser(String User) {
		// TODO Auto-generated method stub
		return userMapper.queryUser(User);
	}

	@Override
	public String upload(MultipartHttpServletRequest request) throws Exception {
		List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
		String decode = "";
		for (UploadedFile file : uploadedFileList) {
			byte[] fileByte = file.getBytes();
			decode = Base64.encodeBase64String(fileByte);
		}
		TaUser user = new TaUser();
		user.setBranchId(SlSessionHelper.getUser().getBranchId());
		user.setUserId(SlSessionHelper.getUserId());
		user.setUserImage(decode);
		userMapper.updateUserPhoto(user);
		return decode;
	}

	@Override
	public String readImage(HttpServletRequest request) {
		try {

			TaUser taUser = SlSessionHelper.getUser();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("branchId", taUser.getBranchId());
			map.put("userId", taUser.getUserId());
			map.put("instId", taUser.getInstId());
			map.put("roleId", taUser.getRoles());
			// 获取当前机构角色下唯一用户
			TaUser user = userMapper.getUserByIds(map);
			String userImage = null;
			if (user != null) {
				userImage = user.getUserImage();
				byte[] imageByte = Base64.decodeBase64(userImage);
				ByteArrayInputStream bais = new ByteArrayInputStream(imageByte);
				BufferedImage bi = ImageIO.read(bais);
				String basePath = request.getSession().getServletContext().getRealPath("");
				File file = new File(basePath + "/miniScript/images/users/" + user.getBranchId() + "-" + user.getUserId() + ".jpg");
				ImageIO.write(bi, "jpg", file);
				return user.getBranchId() + "-" + user.getUserId();
			}

			return userImage;
		} catch (Exception e) {
			// e.printStackTrace();
			return e.toString();
		}
	}

	/**
	 * 判断用户是否是总行管理员
	 * 
	 * @param userId
	 * @return
	 */
	@Override
	public boolean isTotalManager(String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("isActive", "1");
		List<TtTotalInstitution> totalInstitutionList = totalInstitutionMapper.findByIsActive(map);
		String instId = "";
		if (!totalInstitutionList.isEmpty()) {
			instId = totalInstitutionList.get(0).getInstId();
		}
		if (!InstConstants.TOTAL_MANAGER_INST_ID.equalsIgnoreCase(instId)) {
			return false;}
		map.put("instId", instId);
		// map.put("roleLevel", RoleLevelConstants.ROLE_MANAGER);
		map.put("userId", userId);
		List<TaRole> roleList = roleMapper.getRoleByUserIdAndRoleLevel(map);
		if (roleList.isEmpty()) {
			return false;
		} else {
			if (!instId.equals(SlSessionHelper.getInstitutionId())) {
				return false;
			}
		}
		return true;
	}

	@Override
	public Page<TaUser> getWaitApprovePage(Map<String, Object> map) {
		Page<TaUser> userPage = userMapper.selectWaitApproveList(map, ParameterUtil.getRowBounds(map));
		for (TaUser user : userPage) {
			if (user.getUserId().indexOf("_copy") > -1) {
				String userId = user.getUserId();
				user.setUserId(userId.substring(0, userId.indexOf("_copy")));
			}
		}
		return userPage;
	}

	@Override
	public Page<TaUser> getApprovedList(Map<String, Object> map) {
		Page<TaUser> userPage = userMapper.selectApprovedList(map, ParameterUtil.getRowBounds(map));
		for (TaUser user : userPage) {
			if (user.getUserId().indexOf("_copy") > -1) {
				String userId = user.getUserId();
				user.setUserId(userId.substring(0, userId.indexOf("_copy")));
			}
		}
		return userPage;
	}

	@Override
	public Page<TaUser> getPageRoleUser(Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		// 若查询条件里含有机构，则查出所有子机构
		TaUser user = SlSessionHelper.getUser();
		String currentInstId = SlSessionHelper.getInstitutionId();
		if (user != null && currentInstId != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("userId", user.getUserId());
			map.put("currentInstId", currentInstId);
			List<TaRole> roleList = roleMapper.getRoleByUserIdAndRoleLevel(map);

			if (roleList.isEmpty()) {
				// 用户还未分配角色，只能管理自己
				params.put("userId", user.getUserId());
			} else {
				// 用户已经分配角色，判断是否有管理员角色
				List<String> roleLevelList = new ArrayList<String>();
				for (TaRole role : roleList) {
					roleLevelList.add(role.getRoleLevel());
				}
				// 如果包含管理员角色，则用户可以管理机构下所有用户
				if (roleLevelList.contains(RoleLevelConstants.ROLE_MANAGER)) {
					List<String> userInstIdList = institutionService.childrenInstitutionIds(currentInstId);
					userInstIdList.add(currentInstId);
					params.put("userInstIdList", userInstIdList);
				}
			}
		}
		Page<TaUser> result = userMapper.pageRoleUser(params, rowBounds);
		return result;
	}

	@Override
	public void approveAdd(Map<String, Object> map) {
		String userId = ParameterUtil.getString(map, "userId", "");
		TaUser user = userMapper.selectByPrimaryKey(userId);
		// 修改状态标志为审批通过
		user.setApproveStatus(ApproveStatusConstants.APPROVE_ED);
		user.setApproveUserId(SlSessionHelper.getUserId());
		user.setApproveInstId(SlSessionHelper.getInstitutionId());
		user.setApproveDate(DateTimeUtil.getLocalDateTime());
		userMapper.updateByPrimaryKey(user);
	}

	@Override
	public void approveUpdate(Map<String, Object> map) {
		// 插入的临时数据的userId
		String newUserId = ParameterUtil.getString(map, "userId", "");
		TaUser newUser = userMapper.selectByPrimaryKey(newUserId);
		String userId = newUserId.substring(0, newUserId.indexOf("_copy"));

		// 更新需要修改的用户信息
		TaUser user = userMapper.selectByPrimaryKey(userId);
		user.setEmpId(newUser.getEmpId());
		user.setUserName(newUser.getUserName());
		user.setInstId(newUser.getInstId());
		user.setFtaInstId(newUser.getFtaInstId());
		user.setUserEmail(newUser.getUserEmail());
		user.setUserFixedphone(newUser.getUserFixedphone());
		user.setUserCellphone(newUser.getUserCellphone());
		user.setIsInterbank(newUser.getIsInterbank());
		user.setIsActive(newUser.getIsActive());
		user.setUserImage(newUser.getUserImage());
		userMapper.updateByPrimaryKey(user);

		// 更新临时数据为已审批
		newUser.setApproveStatus(ApproveStatusConstants.APPROVE_ED);
		newUser.setApproveUserId(SlSessionHelper.getUserId());
		newUser.setApproveInstId(SlSessionHelper.getInstitutionId());
		newUser.setApproveDate(DateTimeUtil.getLocalDateTime());
		userMapper.updateByPrimaryKey(newUser);

		// 更新userInfo信息
		TaUserInfo newUserInfo = userInfoMapper.selectByPrimaryKey(newUserId);
		TaUserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
		if (newUserInfo == null) {
			if (userInfo != null) {
				userInfoMapper.deleteByPrimaryKey(userId);
			}
		} else {
			newUserInfo.setUserId(userId);
			if (userInfo == null) {
				userInfoMapper.insert(newUserInfo);
			} else {
				userInfoMapper.updateByPrimaryKey(newUserInfo);
			}
		}
	}

	@Override
	public boolean checkUserByBranchId(Map<String, Object> map) {
		int res = userMapper.checkUserByBranchId(map);
		return res == 1 ? true : false;
	}

	@Override
	public TaUser getUserByIds(Map<String, Object> map) {
		return userMapper.getUserByIds(map);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */

	/*
	 * private UserServiceImpl myself() { return
	 * SpringContextHolder.getBean(UserServiceImpl.class); }
	 */

	public static void main(String[] args) {
		System.out.println(MD5Util.MD5("123456").toUpperCase());
	}

	@Override
	public Page<TaUser> searchUserByBranchId(Map<String, Object> params) {
		// TODO Auto-generated method stub
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		return userMapper.searchUserByBranchId(params, rowBounds);
	}

	@Override
	public List<String> verifyUniqueness(Map<String, Object> map) {
		return userMapper.verifyUniqueness(map);
	}
	
	@Override
	public void updateUserByEsb(TaUser user) {
		userMapper.updateByPrimaryKey(user);
	}
	/**徽商OPICS项目组
	 * 根据APPROVE_USER_ID查询用户id和机构
	 * @param approveuserid
	 * @return
	 */
	@Override
	public TaUser getUserByApproveuserid(String approveUserId) {
		TaUser taUser = new TaUser();
		taUser = userMapper.getUserByApproveuserid(approveUserId);
		return taUser;
	}

	/**
	 * 通过用户id查询br
	 * @param userId
	 * @return
	 */
	@Override
	public String getBrByUser(String userId) {
		//查询审批提交用户的BR
		Map<String, Object> brMap = new HashMap<String, Object>();
		SlSession session = SlSessionHelper.getSlSession();
		TtInstitution inst = SlSessionHelper.getInstitution(session);
		brMap.put("userId", userId);// 审批提交人id
		brMap.put("branchId", inst.getBranchId());// 机构
		TaUser tu = userMapper.queryUserByMap(brMap);
		String br=tu.getOpicsBr();
		return br;
	}

}