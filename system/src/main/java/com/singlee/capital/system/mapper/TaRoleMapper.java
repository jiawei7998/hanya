package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaRole;
import com.singlee.capital.system.model.TaRoleFunctionMap;
import com.singlee.capital.system.model.TaRoleModuleMap;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TaRoleMapper extends Mapper<TaRole> {

	/**
	 * 根据角色id查询角色详细信息
	 * 
	 * @param roleId - 角色ID
	 * @return TaRole 角色对象
	 * @author lihuabing
	 * @date 2016-7-21
	 */
	public TaRole selectRoleByRoleId(String roleId);
	
	/**
	 * 根据银行Id 查询角色信息
	 */
	public Page<TaRole> pageAllRoleBranchId(Map<String, Object> params,RowBounds rb);
	public Integer getMaxRoleId(Map<String, Object> params);
	
	/**
	 * 
	 * @param params
	 */
	public void deleteRoleByBranchId(Map<String, Object> params);
	/**
	 * 普通查询角色集合信息
	 * 
	 * @param role - 角色对象
	 * @return List<TaRole>
	 * @author lihuabing
	 * @date 2016-7-21
	 */
	public List<TaRole> listRole(TaRole role);
	/**
	 * 根据机构Ids 银行号查找角色
	 * @param params
	 * @return
	 */
	public List<TaRole> selectRoleListByInstsAndBranchId(Map<String, Object> params);
	/**
	 * 查询角色
	 * 
	 * @param params - 参数map
	 * @return List<TaRole>
	 * @author lihuabing
	 * @date 2016-7-21
	 */
	public List<TaRole> selectRoleList(Map<String, Object> params);
	/**
	 * 根据机构Id 角色名称 查询角色信息
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TaRole> getRolesByInstAndRoleName(Map<String, Object> map, RowBounds rb);
	/**
	 * 分页查询角色列表
	 * @param role - 角色对象
	 * @param rb - rowBounds
	 * @return Page<TaRole>
	 * @author lihuabing
	 * @date 2016-7-21
	 */
	public Page<TaRole> pageRole(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 删除角色所有对应权限的菜单关系
	 * @param role_id:角色id(不允许为空)
	 */
	public void deleteRoleForModule(String role_id);
	
	/**
	 * 删除角色所有对应无权限的功能点关系
	 * @param role_id:角色id(不允许为空)
	 */
	public void deleteRoleForFunction(String role_id);
	
	/**
	 * 批量写入用户菜单权限数据
	 * @param map
	 */
	public void insertRoleForModuleBatch(HashMap<String, List<TaRoleModuleMap>> map);
	
	/**
	 * 批量写入用户功能点无权限数据
	 * @param map
	 */
	public void insertRoleForFunctionBatch(HashMap<String, List<TaRoleFunctionMap>> map);
	
	/**
	 * 查询机构下所有角色
	 * @param map
	 * @return
	 */
	public List<TaRole> selectRoleByInstIdList(Map<String, Object> map);
	
	/**
	 * 查询角色下的指定角色级别的角色
	 * @param map
	 * @return
	 */
	public List<TaRole> getRoleByUserIdAndRoleLevel(Map<String, Object> map);
	
	/**
	 * 查询机构下的指定角色级别的角色
	 * @param map
	 * @return
	 */
	public List<TaRole> getRoleByInstIdAndRoleLevel(Map<String, Object> map);
	
	/**
	 * 根据关联的roleId查询
	 * @param pRoleId
	 * @return
	 */
	public List<TaRole> findByPRoleId(Map<String, Object> map);
	
	/**
	 * 根据pRoleId更新
	 * @param role
	 */
	public void updateByPRoleId(TaRole role);
	
	public Page<TaRole> findByOpratorTypeAndApproveStatus(Map<String, Object> map, RowBounds rowBounds);
	
	public List<TaRole> findByUserIdAndInstId(Map<String, Object> map);
	/**
	 * 根据用户ID 机构id 查询角色信息
	 * 
	 * @param map
	 * @return
	 */
	public Page<TaRole> findByUserIdAndInstIdPag(Map<String, Object> map, RowBounds rowBounds);
	/**
	 * 根据用户、机构查询角色
	 * @param map
	 * @return
	 */
	public List<TaRole> getRoleByUserIdAndInstId(Map<String, Object> map);

	/**
	 * 根据role_id查询所属机构名称
	 * @param roleid
	 * @return
	 */
	public String selectInstNameByRoleId(String roleid);
}