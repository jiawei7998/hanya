package com.singlee.capital.system.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.service.InstitutionProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.Map;

/**
 *  机构产品准入
 * @author liyimin
 *
 */
@Controller
@RequestMapping(value = "/InstitutionProductController")
public class InstitutionProductController extends CommonController {
	
	@Autowired
	InstitutionProductService institutionProductService;

	/**
	 * 新增机构产品准入
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addInstProduct")
	public RetMsg<Serializable> addInstProduct(@RequestBody Map<String, Object> map) {
		institutionProductService.addInstProduct(map);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 查询已准入的产品名称
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getProNames")
	public RetMsg<Serializable> getProNames(@RequestBody Map<String, Object> map) { 
		String result = institutionProductService.getProNames(map);
		return RetMsgHelper.ok(result);
	}
	
	/**
	 * 查询机构名称
	 * @param map
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getInstName")
	public RetMsg<Serializable> getInstName(@RequestBody Map<String, Object> map) {
		return RetMsgHelper.ok(institutionProductService.getInstName(map));
	}
}
