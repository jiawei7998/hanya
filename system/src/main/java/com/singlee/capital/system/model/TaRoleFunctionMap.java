package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 角色拥有功能点权限对应关系
 * 
 * @author lyonchen
 */
@Entity
@Table(name = "TA_ROLE_FUNCTION_MAP")
public class TaRoleFunctionMap implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String roleId;// 角色ID
	@Id
	private String functionId;// 功能点编号
	private String moduleId;// 功能点模块
	private String moduleUrl; //模块对应的URl 冗余存储
	
	public TaRoleFunctionMap() {
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getModuleUrl() {
		return moduleUrl;
	}
	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
    
	
}
