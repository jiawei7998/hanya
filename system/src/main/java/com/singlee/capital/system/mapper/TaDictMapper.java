package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaDict;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TaDictMapper extends Mapper<TaDict> {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */

	/**
	 * 读出所有的字典项数据
	 * @return
	 */
	List<TaDict> loadAllDictionary();
}