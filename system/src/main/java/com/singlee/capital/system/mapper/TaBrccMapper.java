package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaBrcc;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface TaBrccMapper extends Mapper<TaBrcc> {
	/**
	 * 获取全部的brcc
	 * 
	 * @return
	 */
	List<TaBrcc> getAllTaBrcc();

	/**
	 * 根据branchId 查询
	 * 
	 * @param branchId
	 * @return
	 */
	TaBrcc selectByBranchId(String branchId);

}
