package com.singlee.capital.system.service;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.PropertiesUtil;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * 系统配置
 *
 * @author Libo
 */
public class SystemProperties {
    /**
     * 是否判断系统日期和业务日期
     */
    public static boolean isJudgeSysDateAndBusDate = false;
    /**
     * 是否为演示模式不发送上行数据，直接生成交易单据
     */
    public static boolean isCreateTradeForDemonstrate = false;
    /** 是否允许同一柜员授权、复核结算 */
    // public static boolean isSameUserSettle = false;
    /**
     * 首页是否自动刷新消息
     **/
    public static boolean isAutoReceiveMsg = true;
    /**
     * 首页上是否打开js异常捕捉
     **/
    public static boolean isCatchingJsException = true;
    /**
     * 是否启用外汇交易中心上行
     **/
    public static boolean isSendToCstp = true;
    /**
     * 是否开启核算相关线程
     */
    public static boolean isOpenAccountingThread = true;
    /** 轧帐完成后是否允许继续做业务 */
    // public static boolean rollingAccountedContinue= false;
    /**
     * 大额来账分页查询每页记录数
     */
    public static int largeReceiveRecords = 10;
    /**
     * 核心客户分页查询每页记录数
     */
    public static int coreCustRecords = 14;
    /**
     * 记账柜员号
     */
    public static String bookUserId = "M0020";
    /**
     * 总行机构号
     */
    public static String headBankInstId = "50";
    /**
     * 回购业务的首期结算金额允许偏离 百分比
     */
    public static double pledgeRepoDeviate = 0.01;
    /** 是否校验支付系统来账状态 */
    // public static boolean isCheckPaySysRcvInfo = true;
    /** 是否发送信贷系统处理 */
    // public static boolean isSendCreditSys = false;

    /**
     * #数据库备份命令
     */
    public static String dbBackupCmd = null;
    public static String dbOs;
    public static String sshHost;
    public static String sshPass;
    public static String sshUser;
    public static int sshPort;
    /**
     * 是否异地备份
     **/
    public static boolean isDistanceBackUp = false;
    public static String ftpHost;
    public static String ftpPass;
    public static String ftpUser;
    public static int ftpPort;
    public static int dbBackUpCount;

    public static String passwordExcel = "joyard.com";
    // 以下为测试字段
    /** 是否校验行名行号及抹账大额支付状态 */
    // public static boolean isCheckLargePayInfo = true;
    /**
     * 大额支付查证是否Q(待发送)变T(已清算)
     */
    public static boolean isQChangeT = false;
    /**
     * 存在未完成审批单时是否允许直接轧帐
     */
    public static boolean rollingHasApproved = true;
    /** 公司名称 */
    // public static String companyName;

    //金融机构代码
    public static String bankInistCode;

    //内部机构号
    public static String InsideInistCode;
    /**
     * 行名行号更新
     **/
    public static String paysysUpdateCmd;

    public static String SOCKET_GTPIP;
    public static int SOCKET_GTPPORT;
    public static int SOCKET_GTPTIMEOUT;

    public static String smartDocument_url;
    public static String smartDocument_appCode;
    public static String smartDocument_userName;
    public static String smartDocument_password;
    public static String smartDocument_openflag;
    public static String smartDocument_archiveTypeCode;
    public static String iamgate_httpUrl;
    public static String empId_passwd;
    public static String my_bank_cname;
    public static String shpath;
    // swift报文地址
    public static String swiftIp;
    // 端口
    public static int swiftPort;
    // 用户名
    public static String swiftUserName;
    // 密码
    public static String swiftPassword;
    // 发送目录
    public static String swiftCatalog;
    // 备份目录
    public static String swiftArchive;
    // swift报文类型
    public static String swiftType;
    // swift报文备份路径
    public static String swiftPath;


    // 私钥文件
    public static String swiftKeyFile;
    // 报文上传sftp地址
    public static String swiftSftpPath;

    // 文件名
    public static String swiftFileName;

    public static String cstpStartPath;

    public static String cstpStopPath;
    // 大连银行成交单上传地址
    public static String upload_url;
    // 大连银行报价附件上传地址
    public static String uploadPrice_url;
    // 日终文件服务器地址
    public static String acupIp;
    // 端口
    public static int acupPort;
    // 用户名
    public static String acupUserName;
    // 密码
    public static String acupPassword;
    //备份文件路径
    public static String acupPath;
    //服务器文件路径
    public static String acupCatalog;

    // cstp本外币客户端启动
    public static String cstpVmIp;// 虚拟机IP
    public static String cstpVmUsername;// 虚拟机登录用户
    public static String cstpVmPassword;// 虚拟机登录名称
    public static int cstpVmPort;// 开通22端口
    public static String cstpFxHome;// 外币客户端home位置
    public static String cstpFxAgentStart;// 外币客户端Agent启动命令
    public static String cstpFxClientStart;// 外币客户端启动命令
    public static String cstpFxStop;// 外币停止命令
    public static String cstpFxConsole;// 外币停止查看
    public static String cstpRmbHome;// 本币客户端home位置
    public static String cstpRmbAgentStart;// 本币客户端Agent启动命令
    public static String cstpRmbClientStart;// 本币客户端启动命令
    public static String cstpRmbStop;// 本币停止命令
    public static String cstpRmbConsole;// 本币停止查看

    public static String cpisClientHome;//CPIS客户端home位置
    public static String cpisClientStart;// CPIS启动命令
    public static String cpisStop;// CPIS停止命令
    public static String cpisConsole;// CPIS停止查看

    // 监控平台
    public static String monhost;
    public static int monconnTime;
    public static int monreadTime;
    public static String monencoding;
    //万得数据配置
    public static String marketdataTargetpath;//万得读取文件路径
    public static String marketdataArchivepath;//万得备份文件路径
    public static String marketdataLocalfilenameBond;//万得债券文件名
    public static String marketdataLocalfilenameSecm;//万得债券信息文件名
    public static String marketdataLocalfilenameRate;//万得重定利率数据文件名
    public static String marketdataLocalfilenameRisk;//万得风险数据文件名
    public static String marketdataLocalfilenameYc;//万得收益率曲线数据文件名
    public static String marketdataLocalfilenameSecLevel;//万得债券评级数据文件名
    public static String marketdataLocalfilenameBondAdd;//万得债券追加数据文件名

    public static String marketdataWindfilenameFundNAV;//万得基金信息文件名
    public static String marketdataWindfilenameFundInfo;//万得中国共同基金基本资料文件名
    public static String marketdataWindfundInCome;//万得基金-万分收益

    public static String marketdataLocalfundInComepath;//万份收益文件夹
    public static String marketdataLocalfilenameFundNAVpath;//基金净值文件夹
    public static String marketdataLocalfilenameFundInfopath;//中国共同基金基本资料文件夹

    public static String marketdataLocalfundInComeGZpath;//万份收益文件夹(压缩文件)
    public static String marketdataLocalfilenameFundNAVGZpath;//基金净值文件夹(压缩文件)
    public static String marketdataLocalfilenameFundInfoGZpath;//中国共同基金基本资料文件夹(压缩文件)

    public static String marketdataFundPath;//万得文件下载目录




    //国结数据配置
    public static String PositionPath;//文件操作目录
    public static String PositionArchive;//文件备份目录
    public static String Positioncust;
    public static String Positiontrad;

    public static String PositiongjfileName;
    public static String Positiongjjqcost;
    public static String Positiongjjqport;
    public static String Positiongjyqcost;
    public static String Positiongjyqport;

    public static String PositionhxfileName;

    //增值税系统上送文件名
    public static String OpcPath;
    public static String OpcKpsjfileName;
    public static String OpcNssbfileName;
    public static String OpcCzsyfileName;

    //数据库用户
    public static String opicsUserName;//opics用户名
    public static String hrbcbUserName;//前置用户名

    //客户号
    public static String hrbReportCno;//哈尔滨银行客户号
    //ESB
    public static String esbIp;//esb IP
    public static String esbTransPort;//esb 通讯端口
    public static int esbFtpPort;//esb FTP 端口
    public static String esbUser;//esb  user
    public static String esbPasswd;//esb 密码
    public static String esbRoot;//根目录

    //cfets 默认用户
    public static String cfetsDefautUserInstId;
    public static String cfetsDefautUserId;

    //核心
    public static String hxFxfilename;
    public static String hxPath;


    //cfets21位机构码
    public static String cfetsInstId;


    //万得
    public static String windsftpId;
    public static String windsftpUser;
    public static String windsftpPasswd;
    public static int windsftpPort;

    //管会
    public static String dwPath;
    public static String dwFilename;

    //redisson
    public static String redisAddress;
    public static String redisPassword;


    static {
        PropertiesConfiguration config;
        try {
            config = PropertiesUtil.parseFile("system.properties");
            isJudgeSysDateAndBusDate = config.getBoolean("system.isJudgeSysDateAndBusDate");
            isCreateTradeForDemonstrate = config.getBoolean("system.isCreateTradeForDemonstrate");

            largeReceiveRecords = config.getInt("system.largeReceiveRecords", 10);

            headBankInstId = config.getString("system.headBankInstId", "50");
            pledgeRepoDeviate = config.getDouble("system.pledgeRepoDeviate", 0.01);

            bankInistCode = config.getString("system.bankInistCode", "912301001275921118");
            InsideInistCode = config.getString("system.InsideInistCode", "0");



            SOCKET_GTPIP = config.getString("socket.gtpip", "10.240.94.208");// sit10.240.94.208
            SOCKET_GTPPORT = config.getInt("socket.gtpport", 7771);
            SOCKET_GTPTIMEOUT = config.getInt("socket.gtptimeout", 60000);

            /**
             * 影像配置
             */
            smartDocument_url = config.getString("smartDocument.url", "");
            smartDocument_appCode = config.getString("smartDocument.appCode", "");
            smartDocument_userName = config.getString("smartDocument.userName", "");
            smartDocument_password = config.getString("smartDocument.password", "");
            smartDocument_archiveTypeCode = config.getString("smartDocument.archiveTypeCode", "");
            smartDocument_openflag = config.getString("smartDocument.openflag", "1");
            /**
             * 统一认证跳转路径 测试 iamgate.httpUrl= http://10.240.41.10:31201/iamgate/index.jsp 生产
             * iamgate.httpUrl= http://iam.bosc/iamgate/index.jsp
             */
            iamgate_httpUrl = config.getString("iamgate.httpUrl", "");

            empId_passwd = config.getString("empId.passwd", "");
            // shell脚本地址
            shpath = config.getString("shpath", "");
            // 本行简称
            my_bank_cname = config.getString("my_bank_cname", "");

            // swift报文地址
            swiftIp = config.getString("swiftIp", "");
            swiftPort = config.getInt("swiftPort", 21);
            swiftUserName = config.getString("swiftUserName", "");
            swiftPassword = config.getString("swiftPassword", "");
            swiftCatalog = config.getString("swiftCatalog", "");
            swiftArchive = config.getString("swiftArchive", "");
            // swift报文类型
            swiftType = config.getString("swiftType", "");
            // swift报文备份路径
            swiftPath = config.getString("swiftPath", "");


            swiftFileName = config.getString("swiftFileName", "");
            swiftKeyFile = config.getString("swiftKeyFile", "");
            swiftSftpPath = config.getString("swiftSftpPath", "");

            cstpStartPath = config.getString("cstpStartPath", "");
            cstpStopPath = config.getString("cstpStopPath", "");
            // 大连银行成交单上传地址
            upload_url = config.getString("upload_url", "");
            // 大连银行报价附件上传地址
            uploadPrice_url = config.getString("uploadPrice_url", "");
            // swift报文类型
            swiftType = config.getString("swiftType", "");
            // swift报文备份路径
            swiftPath = config.getString("swiftPath", "");

            //日终文件服务器地址
            acupIp = config.getString("acupIp", "");
            //端口
            acupPort = config.getInt("acupPort", 22);
            //用户名
            acupUserName = config.getString("acupUserName", "");
            //密码
            acupPassword = config.getString("acupPassword", "");
            //备份文件路径
            acupPath = config.getString("acupPath", "");
            //服务器文件路径
            acupCatalog = config.getString("acupCatalog", "");

            // shell相关配置信息
            cstpVmIp = config.getString("cstp.vm.ip", "");
            cstpVmUsername = config.getString("cstp.vm.username", "");
            cstpVmPassword = config.getString("cstp.vm.password", "");
            cstpVmPort = config.getInt("cstp.vm.port", 22);
            cstpFxHome = config.getString("cstp.fx.home", "");
            cstpFxAgentStart = config.getString("cstp.fx.agent.start", "");
            cstpFxClientStart = config.getString("cstp.fx.client.start", "");
            cstpFxStop = config.getString("cstp.fx.stop", "");
            cstpFxConsole = config.getString("cstp.fx.console", "");
            cstpRmbHome = config.getString("cstp.rmb.home", "");
            cstpRmbAgentStart = config.getString("cstp.rmb.agent.start", "");
            cstpRmbClientStart = config.getString("cstp.rmb.client.start", "");
            cstpRmbStop = config.getString("cstp.rmb.stop", "");
            cstpRmbConsole = config.getString("cstp.rmb.console", "");

            cpisClientHome = config.getString("cpis.home", "");
            cpisClientStart = config.getString("cpis.client.start", "");
            cpisStop = config.getString("cpis.stop", "");
            cpisConsole = config.getString("cpis.console", "");

            // 监控平台
            monhost = config.getString("monitor.url", "");
            monconnTime = config.getInt("monitor.connectTimeout", 10000);
            monreadTime = config.getInt("monitor.readtimeout", 150000);
            monencoding = config.getString("monitor.encoding", "");

            marketdataTargetpath = config.getString("marketdata.targetpath", "");
            marketdataLocalfilenameBond = config.getString("marketdata.localfilename.bond", "BOND");
            marketdataLocalfilenameSecm = config.getString("marketdata.localfilename.secm", "SECM");
            marketdataLocalfilenameRate = config.getString("marketdata.localfilename.rate", "RATE");
            marketdataLocalfilenameRisk = config.getString("marketdata.localfilename.risk", "RISK");
            marketdataLocalfilenameYc = config.getString("marketdata.localfilename.yc", "YC");
            marketdataLocalfilenameSecLevel = config.getString("marketdata.localfilename.seclevel", "ZQPJ");
            marketdataLocalfilenameBondAdd = config.getString("marketdata.localfilename.bondadd", "ZQEWXX");

            marketdataArchivepath = config.getString("marketdata.archivepath", "");


            //基金
            marketdataWindfilenameFundNAV = config.getString("marketdata.windfilename.fundNAV", "");
            marketdataWindfilenameFundInfo = config.getString("marketdata.windfilename.fundInfo", "");
            //万份收益
            marketdataWindfundInCome = config.getString("marketdata.windfilename.fundInCome", "");

            //基金净值文件夹
            marketdataLocalfilenameFundNAVpath = config.getString("marketdata.localfilename.fundNAVPath", "");
            //基金基本资料文件夹
            marketdataLocalfilenameFundInfopath = config.getString("marketdata.localfilename.fundInfoPath", "");
            //万份收益文件夹
            marketdataLocalfundInComepath = config.getString("marketdata.localfilename.fundInComePath", "");



            //基金净值文件夹（压缩文件）
            marketdataLocalfilenameFundNAVGZpath = config.getString("marketdata.localfilename.fundNAVGZPath", "");
            //基金基本资料文件夹（压缩文件）
            marketdataLocalfilenameFundInfoGZpath = config.getString("marketdata.localfilename.fundInfoGZPath", "");
            //万份收益文件夹（压缩文件）
            marketdataLocalfundInComeGZpath = config.getString("marketdata.localfilename.fundInComeGZPath", "");




            marketdataFundPath = config.getString("marketdata.fundPath", "");

            //国结
            PositionPath = config.getString("position.src.folder", "");
            PositionArchive = config.getString("position.archive.folder", "C:/interface/coreposition/archive");
            Positiontrad = config.getString("position.trad", "TRAD");
            Positioncust = config.getString("position.cust", "9000000000");

            PositiongjfileName = config.getString("position.gj.filename", "ICL_FI_YQINTERFACEFOROPICSINFO_");
            Positiongjjqport = config.getString("position.gjjq.port", "POSP");
            Positiongjyqport = config.getString("position.gjyq.port", "POFW");
            Positiongjjqcost = config.getString("position.gjjq.cost", "102010");
            Positiongjyqcost = config.getString("position.gjyq.cost", "102020");

            PositionhxfileName = config.getString("position.hx.filename", "CBS_DPSFORETRADEREG_");

            //增值税
            OpcPath = config.getString("ygz.src.folder", "");
            OpcKpsjfileName = config.getString("ygz.opc.kpsj.filename", "OPC_KPSJ_");
            OpcNssbfileName = config.getString("ygz.opc.nssb.filename", "OPC_NSSB_");
            OpcCzsyfileName = config.getString("ygz.opc.czsy.filename", "OPC_CZSY_");

            opicsUserName = config.getString("db.opicsUserName", "OPICS47");
            hrbcbUserName = config.getString("db.hrbcbUserName", "HRBBK");

            hrbReportCno = config.getString("hrbReport.cno.hrbbk", "8000000000");

            esbIp = config.getString("esb.ip", "130.1.12.236");
            esbTransPort = config.getString("esb.transPort", "8010");
            esbFtpPort = config.getInt("esb.ftpPort", 22);
            esbUser = config.getString("esb.user", "esb");
            esbPasswd = config.getString("esb.passwd", "esb");
            esbRoot = config.getString("esb.root", "/home/esb/print/");
            cfetsDefautUserId = config.getString("cfets.DefautUserId", "SYS1");
            cfetsDefautUserInstId = config.getString("cfets.DefautUserInstId", "00000000");

            cfetsInstId = config.getString("cfets.cstp.rmb.instId", "110000023010000104001");

            windsftpId = config.getString("wind.sftp.ip", "");
            windsftpUser = config.getString("wind.sftp.user", "");
            windsftpPasswd = config.getString("wind.sftp.passwd", "");
            windsftpPort = config.getInt("wind.sftp.port", 22);


            hxFxfilename = config.getString("Hx.FxfileName", "BCS_FXSM_");
            hxPath = config.getString("Hx.Path", "");

            dwPath = config.getString("dw.Path", "");
            dwFilename = config.getString("dw.Filename", "");

            redisAddress = config.getString("redisson.address", "");
            redisPassword = config.getString("redisson.password", "");

        } catch (ConfigurationException e) {
            JY.raise("system.properties文件解析失败   ", e);
        }

    }
}
