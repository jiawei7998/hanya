package com.singlee.capital.system.controller;


import com.singlee.capital.common.exception.CException;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.JQueryFileUploadResult;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.webmvc.HandlerMulipartFile;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * controller公共基类。
 * @author Lyon Chen
 *
 */
public abstract class CommonController{


	/**
	 *
	 * 统一处理 业务处理 运行期 异常，不需要日志记录
	 *
	 * @return e
	 */
	@ExceptionHandler({ RException.class })
	@ResponseBody
	public RetMsg<String> exception(RException e) {
		JY.error("控制器发生运行期异常RException：", e);
		return handleRException(e);
	}

	/**
	 * 统一处理 不在程序控制范围的运行期异常 RuntimeException，需要日志记录
	 *
	 * @return e
	 */
	@ExceptionHandler({ RuntimeException.class })
	@ResponseBody
	public RetMsg<String> exception(RuntimeException e) {
		JY.error("控制器发生运行期异常RuntimeException：", e);
		return handleRException(e);
	}

	@ExceptionHandler({ CException.class })
	@ResponseBody
	public RetMsg<String> exception(CException e) {
		if (JY.isLogDebugEnabled()) {
			JY.error("控制器发生异常CException：", e);
		}
		return handleCException(e);
	}

	@ExceptionHandler({ IOException.class })
	@ResponseBody
	public RetMsg<String> exception(IOException e) {
		JY.error("控制器发生IO异常IOException：", e);
		return handleException(e);
	}


	protected RetMsg<List<Pair<String,String>>> uploadFiles(MultipartHttpServletRequest request,
															HttpServletResponse response, HandlerMulipartFile handler) {
		List<Pair<String,String>> ret = new LinkedList<Pair<String,String>>();

		// 1.文件获取
		Map<String, MultipartFile> itr = request.getFileMap();
		if(itr==null||itr.values()==null||itr.values().size()<1){
			return RetMsgHelper.ok("no files uploaded", ret);
		}
		// 2.顺序解析
		for (MultipartFile uploadedFile : itr.values()) {
			ret.add(new Pair<String,String>(uploadedFile.getOriginalFilename(),
					handler.process(uploadedFile)));
		}
		return RetMsgHelper.ok("ok",ret);
	}


	protected JQueryFileUploadResult uploadFile(MultipartHttpServletRequest request,
												HttpServletResponse response, HandlerMulipartFile handler) {
		// 1.文件获取
		MultipartFile multipartFile = request.getFile((String)request.getFileNames().next());
		// 2.顺序解析
		return FastJsonUtil.parseObject(handler.process(multipartFile), JQueryFileUploadResult.class);
	}

	//对checked异常的处理
	protected RetMsg<String> handleRException(RuntimeException e) {
		return RetMsgHelper.excetpion("RuntimeException", ExceptionUtils.getRootCauseMessage(e), e);
	}

	//对checked异常的处理
	protected RetMsg<String> handleCException(CException e) {
		return RetMsgHelper.excetpion("CException", ExceptionUtils.getRootCauseMessage(e), e);
	}

	//对checked异常的处理
	protected RetMsg<String> handleException(Exception e) {
		return RetMsgHelper.excetpion("Exception", ExceptionUtils.getRootCauseMessage(e), e);
	}
	protected String getThreadLocalUserId(){
		return SlSessionHelper.getUserId();
	}

	@Deprecated
	protected String getThreadLocalUserEmpId(){
		return SlSessionHelper.getEmpId();
	}
	
//	protected String getThreadLocalInCashAccId(){
//		return SlSessionHelper.getInCashAccId();
//	}
	
	protected TaUser getThreadLocalUser(){
		return SlSessionHelper.getUser();
	}

	@Deprecated
	protected String getThreadLocalInstitutionId(){
		return SlSessionHelper.getInstitutionId();
	}

	@Deprecated
	protected TtInstitution getThreadLocalInstitution(){
		return SlSessionHelper.getInstitution();
	}
	
	
	//注入 map 登录者信息
	@Deprecated
	protected void putThreadContextMap(Map<String,Object> map){
		System.out.println(map);
	}
	
}
