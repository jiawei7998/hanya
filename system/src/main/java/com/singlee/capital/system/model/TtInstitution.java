package com.singlee.capital.system.model;

import com.singlee.capital.common.util.TreeInterface;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * 机构
 * 
 * @author lyonchen
 * 
 */

@Entity
@Table(name = "TT_INSTITUTION")
public class TtInstitution implements Serializable, TreeInterface {
	private static final long serialVersionUID = 1L;
	@Id
	private String instId;// 机构编号
	private String instFullname;// 机构全称
	private String instName;// 机构简称
	private String pInstId;// 父机构号
	@Transient
	private String pInstName;// 父机构号中文
	private String instType; // 机构类型，字典
	private String instStatus; // 机构状态，字典
	private String instLrInstCode; // 机构代码证
	private String linkMan; // 联系人
	private String telephone; // 电话
	private String mobile; // 手机
	private String address; // 地址
	private String onlineDate;// 上线日期
	private String bInstId;// 记账机构号
	private String instSimpleName;// 简名
	private String isFta;// 是否自贸区机构
	private String instPinyinname;// 中文拼音名称
	private String instPinyinabbreviations;// 中文拼音缩写
	private String internationalPayments;// 国际支付号
	private String paymentBank;// 安全支付行
	private String settlmentno;// 清算号
	private String paymentBankno;// 支付系统号
	private String morningStarttime;// 上午开始营业时间
	private String morningEndtime;// 上午结束营业时间
	private String afternoonStarttime;// 下午开始营业时间
	private String afternoonEndtime;// 下午结束营业时间
	private String businessUnit;// 业务单元
	private String printInstId;
	private String branchId;// 银行ID
	private String thirdPartyId;// 交易映射机构ID(OPICS)
	@Transient
	private String prdName;

	@Transient
	private List<TtInstitution> children;

	public String getBusinessUnit() {
		return businessUnit;
	}

	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	public List<TtInstitution> getChildren() {
		return children;
	}

	public void setChildren(List<TtInstitution> children) {
		this.children = children;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getInstFullname() {
		return instFullname;
	}

	public void setInstFullname(String instFullname) {
		this.instFullname = instFullname;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}

	public String getInstLrInstCode() {
		return instLrInstCode;
	}

	public void setInstLrInstCode(String instLrInstCode) {
		this.instLrInstCode = instLrInstCode;
	}

	public String getOnlineDate() {
		return onlineDate;
	}

	public void setOnlineDate(String onlineDate) {
		this.onlineDate = onlineDate;
	}

	public String getPInstId() {
		return pInstId;
	}

	public void setPInstId(String pInstId) {
		this.pInstId = pInstId;
	}

	public String getInstType() {
		return instType;
	}

	public void setInstType(String instType) {
		this.instType = instType;
	}

	public String getInstStatus() {
		return instStatus;
	}

	public void setInstStatus(String instStatus) {
		this.instStatus = instStatus;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getpInstId() {
		return pInstId;
	}

	public void setpInstId(String pInstId) {
		this.pInstId = pInstId;
	}

	@Override
	public String getText() {
		return instName;
	}

	@Override
	public String getValue() {
		return instId;
	}



	@Override
	public String getId() {
		return getInstId();
	}

	@Override
	public String getParentId() {
		return getPInstId();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void children(List<? extends TreeInterface> list) {
		setChildren((List<TtInstitution>) list);
	}

	@Override
	public List<? extends TreeInterface> children() {
		return getChildren();
	}

	@Override
	public int getSort() {
		return StringUtils.length(getInstId());
	}

	@Override
	public String getIconCls() {
		return "";
	}

	public String getbInstId() {
		return bInstId;
	}

	public void setbInstId(String bInstId) {
		this.bInstId = bInstId;
	}

	public String getInstSimpleName() {
		return instSimpleName;
	}

	public void setInstSimpleName(String instSimpleName) {
		this.instSimpleName = instSimpleName;
	}

	public String getPrdName() {
		return prdName;
	}

	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	public String getIsFta() {
		return isFta;
	}

	public void setIsFta(String isFta) {
		this.isFta = isFta;
	}

	public String getInstPinyinname() {
		return instPinyinname;
	}

	public void setInstPinyinname(String instPinyinname) {
		this.instPinyinname = instPinyinname;
	}

	public String getInstPinyinabbreviations() {
		return instPinyinabbreviations;
	}

	public void setInstPinyinabbreviations(String instPinyinabbreviations) {
		this.instPinyinabbreviations = instPinyinabbreviations;
	}

	public String getInternationalPayments() {
		return internationalPayments;
	}

	public void setInternationalPayments(String internationalPayments) {
		this.internationalPayments = internationalPayments;
	}

	public String getPaymentBank() {
		return paymentBank;
	}

	public void setPaymentBank(String paymentBank) {
		this.paymentBank = paymentBank;
	}

	public String getSettlmentno() {
		return settlmentno;
	}

	public void setSettlmentno(String settlmentno) {
		this.settlmentno = settlmentno;
	}

	public String getPaymentBankno() {
		return paymentBankno;
	}

	public void setPaymentBankno(String paymentBankno) {
		this.paymentBankno = paymentBankno;
	}

	public String getMorningStarttime() {
		return morningStarttime;
	}

	public void setMorningStarttime(String morningStarttime) {
		this.morningStarttime = morningStarttime;
	}

	public String getMorningEndtime() {
		return morningEndtime;
	}

	public void setMorningEndtime(String morningEndtime) {
		this.morningEndtime = morningEndtime;
	}

	public String getAfternoonStarttime() {
		return afternoonStarttime;
	}

	public void setAfternoonStarttime(String afternoonStarttime) {
		this.afternoonStarttime = afternoonStarttime;
	}

	public String getAfternoonEndtime() {
		return afternoonEndtime;
	}

	public void setAfternoonEndtime(String afternoonEndtime) {
		this.afternoonEndtime = afternoonEndtime;
	}

	public String getPrintInstId() {
		return printInstId;
	}

	public void setPrintInstId(String printInstId) {
		this.printInstId = printInstId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(String thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}

	public String getpInstName() {
		return pInstName;
	}

	public void setpInstName(String pInstName) {
		this.pInstName = pInstName;
	}

	@Override
	public String toString() {
		return "机构信息{" +
				"机构代码='" + instId + '\'' +
				", 机构全称='" + instFullname + '\'' +
				", 机构简称='" + instName + '\'' +
				", 上级机构代码='" + pInstId + '\'' +
				", 机构类型='" + instType + '\'' +
				", 机构状态='" + instStatus + '\'' +
				", 交易映射机构[OPICS]='" + thirdPartyId + '\'' +
				'}';
	}
}
