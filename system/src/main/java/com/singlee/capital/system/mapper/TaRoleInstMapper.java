package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaRoleInstMap;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TaRoleInstMapper extends Mapper<TaRoleInstMap>{
	public List<String> searchInstIdByRoleId(Map<String, Object> map);

	public List<TaRoleInstMap> getAllInstRole(Map<String, Object> map);


	void deleteUserRoleByInstId(@Param("instId") String instId);
}
