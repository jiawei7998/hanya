package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.ApproveStatusConstants;
import com.singlee.capital.system.dict.OpratorConstants;
import com.singlee.capital.system.dict.RoleLevelConstants;
import com.singlee.capital.system.mapper.*;
import com.singlee.capital.system.model.*;
import com.singlee.capital.system.pojo.Module;
import com.singlee.capital.system.pojo.RoleManageForSaveVo;
import com.singlee.capital.system.pojo.RoleModule;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.service.RoleService;
import com.singlee.capital.system.service.UserService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 角色服务-实现
 * 
 * @author lihuabing
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RoleServiceImpl implements RoleService {

	@Autowired
	private TaUserMapper userMapper;
	@Autowired
	private TaRoleMapper roleMapper;
	@Autowired
	private TaRoleFunctionMapMapper roleFunctionMapMapper;
	@Autowired
	private TaRoleModuleMapMapper roleModuleMapMapper;
	@Autowired
	private TaModuleMapper moduleMapper;
	@Autowired
	private TaFunctionMapper functionMapper;
//	@Autowired
//	private TtInstitutionMapper institutionMapper;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private UserService userService;
	@Autowired
	private TaRoleModuleMapCopyMapper roleModuleMapCopyMapper;
	@Autowired
	private TaRoleInstMapper taRoleInstMapper;
	@Autowired
	private TaRoleDeskMapMapper taRoleDeskMapMapper;
	@Autowired
	private TaBrccMapper taBrccMapper;
	@Autowired
	private TaUserRoleMapMapper taUserRoleMapMapper;
	@Autowired
	private TaUserInstMapper taUserInstMapper;
	@Autowired
	private BatchDao batchDao;

	/**
	 * 根据roleId获取角色信息
	 * 
	 * @param roleId
	 * @return
	 */
	@Override
	public TaRole getRoleById(String roleId) {
		TaRole u = roleMapper.selectRoleByRoleId(roleId);
		if(u!=null){
			TaRoleFunctionMap arg0 = new TaRoleFunctionMap();
			arg0.setRoleId(roleId);
			List<TaRoleFunctionMap> roleAllFuncList = roleFunctionMapMapper.select(arg0);
			u.setFunctionMapList(roleAllFuncList);
			TaRoleModuleMap arg1 = new TaRoleModuleMap();
			arg1.setRoleId(roleId);
			List<TaRoleModuleMap> roleAllModuleList = roleModuleMapMapper.select(arg1);
			u.setModuleMapList(roleAllModuleList);
			u.setInstName(roleMapper.selectInstNameByRoleId(roleId));
		}
		return u;
	}
	
	@Override
	public Page<TaRole> pageRole(Map<String, Object> pageData) {
		TaUser user = SlSessionHelper.getUser();
		TtInstitution ttInstitution = SlSessionHelper.getInstitution();
		List<String> userInstIdList = new ArrayList<String>();
		if(user != null && ttInstitution.getInstId() != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("userId", user.getUserId());
			map.put("currentInstId", ttInstitution.getInstId());
			List<TaRole> roleList = roleMapper.getRoleByUserIdAndRoleLevel(map);
			
			if(roleList.isEmpty()) {
				// 用户还未分配角色，只能管理自己
				pageData.put("userId", user.getUserId());
			} else {
				//获得机构向下的所有机构
				HashMap<String, Object> map2 = new HashMap<String, Object>();
					map2.put("instId", ttInstitution.getInstId());
					List<TtInstitution> institutions = institutionService.searchCanInstitution(map2);
					for(TtInstitution institution : institutions){
						userInstIdList.add(institution.getInstId());
					}
					pageData.put("userInstIdList", userInstIdList);
				/*// 用户已经分配角色，判断是否有管理员角色
				List<String> roleLevelList = new ArrayList<String>();
				for(TaRole role : roleList) {
					roleLevelList.add(role.getRoleLevel());
				}
				
				//reload by wangchen on 2017/8/28
				//机构类型:0总行；1分行；2支行；9部门
				if("0".equals(ttInstitution.getInstType())){
					//总行机构管理员不设隔离
					//总行机构非管理员仅能维护自己所属机构
					if(!(roleLevelList.contains(RoleLevelConstants.ROLE_MANAGER))) {
						userInstIdList.add(ttInstitution.getInstId());
						pageData.put("instIdList", userInstIdList);
					}
				}else if("1".equals(ttInstitution.getInstType()) || "2".equals(ttInstitution.getInstType())){
					//分行支行机构：维护各自机构
					//note 原本想法是分行机构开放和自己共用一个父机构的所有机构，但是上海银行营业柜的机构树令该想法无法实现
					userInstIdList.add(ttInstitution.getInstId());
					pageData.put("instIdList", userInstIdList);
				}else{
					userInstIdList.add(ttInstitution.getInstId());
					pageData.put("instIdList", userInstIdList);
				}*/
			}
		}
		return roleMapper.pageRole(pageData, ParameterUtil.getRowBounds(pageData));
	}
	
	@Override
	public List<TaRole> getAllRole(TaRole role) {
		List<TaRole> result = roleMapper.listRole(role);
		return result;
	}

	@Override
	public List<RoleModule> searchModuleForRole() {
		List<TaModule> orignial =  moduleMapper.recursionModuleAll();
		return createModleList(orignial);
	}
	
	/**
	 * 根据查询出的树结构生成TreeModuleBo对象
	 * @param hashList
	 * @param parentTreeModule
	 * @return
	 */
	private List<RoleModule> createModleList(List<TaModule> hashList){
		JY.require(hashList != null, "没有找到菜单模块资源信息,请确认！");
		//1.将数组构建为hashMap<String,List<TreeModuleBo>> 对象
		HashMap<String, List<RoleModule>> taModuleHashMap = new HashMap<String, List<RoleModule>>();
		for (TaModule taModule : hashList) {
			String pID = taModule.getModulePid();
			List<RoleModule> list = new ArrayList<RoleModule>();
			//判断taModuleHashMap是否已存在pId的对象。存在时从hashmap中读取到list，再项list中添加数据
			if(taModuleHashMap.get(pID) != null){
				list = taModuleHashMap.get(pID);
			}
			list.add(TaModuleToTreeModuleBo(taModule));
			taModuleHashMap.put(pID, list);
		}
		//2.从String = 0 的数组开始迭代，读取hashMap中module_id = PId 的数据
		List<RoleModule> TaModuleRoots = taModuleHashMap.get("-1");
//		List<RoleModule> TaModuleRoots = new ArrayList<RoleModule>();
//		for (Entry<String, List<RoleModule>> e : taModuleHashMap.entrySet()) {
//			if(e.getKey().contains("M000")){
//				TaModuleRoots.addAll(e.getValue());
//			}
//		}
		
		
		for (RoleModule treeModuleBo : TaModuleRoots) {
			setTreeModuleBoChildren(taModuleHashMap,treeModuleBo);
		}
		return TaModuleRoots;
	}
	
	private List<RoleModule> createModleListSpecial(List<TaModule> hashList){
		JY.require(hashList != null, "没有找到菜单模块资源信息,请确认！");
		//1.将数组构建为hashMap<String,List<TreeModuleBo>> 对象
		HashMap<String, List<RoleModule>> taModuleHashMap = new HashMap<String, List<RoleModule>>();
		for (TaModule taModule : hashList) {
			String pID = taModule.getModulePid();
			List<RoleModule> list = new ArrayList<RoleModule>();
			//判断taModuleHashMap是否已存在pId的对象。存在时从hashmap中读取到list，再项list中添加数据
			if(taModuleHashMap.get(pID) != null){
				list = taModuleHashMap.get(pID);
			}
			list.add(TaModuleToTreeModuleBoSpecial(taModule));
			taModuleHashMap.put(pID, list);
		}
		//2.从String = 0 的数组开始迭代，读取hashMap中module_id = PId 的数据
		List<RoleModule> TaModuleRoots = taModuleHashMap.get("-1");
		for (RoleModule treeModuleBo : TaModuleRoots) {
			
			setTreeModuleBoChildren(taModuleHashMap,treeModuleBo);
		}
		return TaModuleRoots;
	}
	
	/**
	 * 设置TreeModuleBo对象中children属性
	 * @param taModuleHashMap
	 * @param treeModuleBo
	 */
	private void setTreeModuleBoChildren(HashMap<String, List<RoleModule>> taModuleHashMap,RoleModule treeModuleBo){
		String id = treeModuleBo.getId();
		if(taModuleHashMap.get(id) != null){
			List<RoleModule> list = taModuleHashMap.get(id);
			for (RoleModule treeModuleBo2 : list) {
				treeModuleBo2.setDeskId(treeModuleBo.getDeskId());
				setTreeModuleBoChildren(taModuleHashMap,treeModuleBo2);
			}
			treeModuleBo.setChildren(list);
		}
	}
	
	private RoleModule TaModuleToTreeModuleBoSpecial(TaModule taModule){
		RoleModule RoleModule = new RoleModule();
		RoleModule.setId(taModule.getModuleId());
		RoleModule.setText(taModule.getModuleName());
		List<TaFunction> listFun = taModule.getFunctions();
		RoleModule.setDeskId(taModule.getDeskId());
		RoleModule.setAttributes(listFun);
		RoleModule.setPid(taModule.getModulePid());
		return RoleModule;
	}
	
	/**
	 * 将TaModules对象转换为TreeModuleBo对象
	 * @param TaModules
	 * @return
	 */
	private RoleModule TaModuleToTreeModuleBo(TaModule taModule){
		RoleModule RoleModule = new RoleModule();
		RoleModule.setId(taModule.getModuleId());
		RoleModule.setText(taModule.getModuleName());
		List<TaFunction> listFun = functionMapper.searchFunctionByModuleId(taModule);
		RoleModule.setAttributes(listFun);
		RoleModule.setPid(taModule.getModulePid());
		return RoleModule;
	}

	@Override
	public List<TaUser> searchUserByRoleId(String role_id) {
		return userMapper.searchUserByRoleId(role_id);
	}
	
	@Override
	public Page<TaUser> searchUserByRoleId(Map<String,Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		String role_id = map.get("roleId").toString();
		return userMapper.searchUserByRoleId(role_id, rb);
	}

	/**
	 * 根据角色Id读取有权限的Module信息(包含有权限的功能点)
	 * @param roleId
	 * @return
	 */
	@Override
	public List<HashMap<String, Object>> searchModuleInfoByRoleId(String roleId){
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String,Object>>();
		List<String> moduleList = roleModuleMapMapper.searchModuleIdForRoleId(roleId);
		for (String string : moduleList) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("moduleId",string );
			HashMap<String, Object> paraMap = new HashMap<String, Object>();
			paraMap.put("role_id", roleId);
			paraMap.put("module_id", string);
			List<String> functionList = roleFunctionMapMapper.searchFunctionIdForRoleId(paraMap);
			map.put("list", functionList);
			
			retList.add(map);
		}
		return retList;
	}

	@Override
	public List<HashMap<String, List<TaFunction>>> searchModuleAndFunctionByRoleId(String roleId,String branchId) {
		List<HashMap<String, List<TaFunction>>> retList = new ArrayList<HashMap<String,List<TaFunction>>>();
		HashMap<String, Object> maper = new HashMap<String, Object>();
		maper.put("branchId", branchId);
		List<TaModule> moduleList = moduleMapper.getAllResourceByBranchId(maper);//查询全部模块
		for (TaModule taModule : moduleList) {
			HashMap<String, List<TaFunction>> map = new HashMap<String, List<TaFunction>>();
			HashMap<String, Object> paraMap = new HashMap<String, Object>();
			paraMap.put("roleId", roleId);
			paraMap.put("moduleId", taModule.getModuleId());
			paraMap.put("branchId", branchId);
			List<TaFunction> functionList = functionMapper.getFunctionByBranchIdAndRoleId(paraMap);
			map.put(taModule.getModuleId(), functionList);
			retList.add(map);
		}
		return retList;
	}

	@Override
	public List<TaFunction> searchFunctionByRoleId(String roleId, String branchId,String moduleId) {
		HashMap<String, Object> paraMap = new HashMap<String, Object>();
		paraMap.put("roleId", roleId);
		//paraMap.put("moduleId", moduleId);
		paraMap.put("branchId", branchId);
		return functionMapper.getFunctionByBranchIdAndRoleId(paraMap);
	}

	/**
	 * 保存角色
	 * @param TaRole taRole
	 * @return
	 */
	@Override
	public void createRole(TaRole taRole) throws RException {
		TaRole u = roleMapper.selectByPrimaryKey(taRole);
		String[] insts = taRole.getInstId().split(",");
		if (u != null) {
			JY.raiseRException(MsgUtils.getMessage("error.base.0004"));
		}
		TaUser user = SlSessionHelper.getUser();
		if(user != null && user.getInstId() != null) {
			List<String> instIdList = institutionService.childrenInstitutionIds(user.getInstId());
			instIdList.add(user.getInstId());
			for(int i=0;i<insts.length;i++){
				if(!instIdList.contains(insts[i])) {
					JY.raiseRException("只能选择从属机构");
				}
			}
			
		}
		
		// 管理员角色每个机构只会有一个
		if(RoleLevelConstants.ROLE_MANAGER.equals(taRole.getRoleLevel())) {
			Map<String, Object> map = new HashMap<String, Object>();
			//逐个遍历机构中的管理员是否存在
			for(int i=0;i<insts.length;i++){
				map.put("instId", insts[i]);
				map.put("roleLevel", RoleLevelConstants.ROLE_MANAGER);
				List<TaRole> roleList = roleMapper.getRoleByInstIdAndRoleLevel(map);
				if(!roleList.isEmpty()) {
					JY.raiseRException("管理员已存在");
				}
			}
			
		}
		
		/*int count = userMapper.selectCountByExample(user);
		if (count > 0) {
			JY.raiseRException(ErrorUtils.getBaseErrorMessage("error.base.0005"));
		}*/
		//逐个赋值机构类型  机构类型赋值需要去机构角色表中查看
		
//		TtInstitution institution = institutionMapper.selectByPrimaryKey(taRole.getInstId());
//		if(institution != null) {
//			taRole.setInstType(institution.getInstType());
//		}
		if(userService.isTotalManager(user.getUserId())) {
			taRole.setApproveStatus(ApproveStatusConstants.MANAGER_OP);
		}else{
			taRole.setOpratorType(OpratorConstants.ADD);
			taRole.setApproveStatus(ApproveStatusConstants.MANAGER_OP);
			taRole.setSubmitUserId(user.getUserId());
			taRole.setSubmitInstId(SlSessionHelper.getInstitutionId());
		}
		//查询brcc
		TaBrcc tabrcc= taBrccMapper.selectByBranchId(user.getBranchId());
		if(tabrcc!=null){
			//获取该银行下角色Id的最大值   select max(to_number(REPLACE(t.Role_id,'QD'||'000',''))) ts from ta_role t where branch_id='00000001';
			Map<String,Object> map =new HashMap<String, Object>();
			map.put("branchId", taRole.getBranchId());
			map.put("Type", tabrcc.getBranchCn());
			
			Integer roleIdMax= roleMapper.getMaxRoleId(map)+1;
			String str = String.format("%07d", roleIdMax);
			
			taRole.setRoleId(tabrcc.getBranchCn()+str);
		}
		
		roleMapper.insert(taRole);
		//插入机构角色表
		for(int i=0;i<insts.length;i++){
			TaRoleInstMap taRoleInstMap =new TaRoleInstMap();
			taRoleInstMap.setRoleId(taRole.getRoleId());
			taRoleInstMap.setInstId(insts[i]);
			taRoleInstMapper.insert(taRoleInstMap);
		}
	}
	
	/**
	 * 更新角色
	 * @param TaRole taRole
	 * @return
	 */
	@Override
	@AutoLogMethod(value = "角色修改",id = "0004")
	public void updateRoleAndOld(TaRole taRole,TaRole oldRole) throws RException {
		TaRole u = roleMapper.selectByPrimaryKey(taRole);
		if (u == null) {
			JY.raiseRException(MsgUtils.getMessage("error.base.0004"));
		}
		/*int count = userMapper.selectCountByExample(user);
		if (count > 0) {
			JY.raiseRException(ErrorUtils.getBaseErrorMessage("error.base.0005"));
		}*/
//		TtInstitution institution = institutionMapper.selectByPrimaryKey(taRole.getInstId());
//		if(institution != null) {
//			taRole.setInstType(institution.getInstType());
//		}
		String[] insts =taRole.getInstId().split(",");
		TaUser user = SlSessionHelper.getUser();
		if(user != null && user.getInstId() != null) {
			List<String> instIdList = institutionService.childrenInstitutionIds(user.getInstId());
			instIdList.add(user.getInstId());
			for(int i=0;i<insts.length;i++){
				if(!instIdList.contains(insts[i])) {
					JY.raiseRException("只能选择从属机构");
				}
			}
			
		}
		
		if(RoleLevelConstants.ROLE_MANAGER.equals(taRole.getRoleLevel())) {
			Map<String, Object> map = new HashMap<String, Object>();
			for(int i=0;i<insts.length;i++){
				map.put("instId", insts[i]);
				map.put("roleLevel", RoleLevelConstants.ROLE_MANAGER);
				map.put("roleId", taRole.getRoleId());
				List<TaRole> roleList = roleMapper.getRoleByInstIdAndRoleLevel(map);
				if(!roleList.isEmpty()) {
					JY.raiseRException("管理员已存在");
				}
			}
			
		}
		
		roleMapper.updateByPrimaryKey(taRole);
		//删除原来的   增加新的
		TaRoleInstMap taRoleInstMapDelete =new TaRoleInstMap();
		taRoleInstMapDelete.setRoleId(taRole.getRoleId());
		taRoleInstMapper.delete(taRoleInstMapDelete);
		for(int i=0;i<insts.length;i++){
			TaRoleInstMap taRoleInstMap = new TaRoleInstMap();
			taRoleInstMap.setInstId(insts[i]);
			taRoleInstMap.setRoleId(taRole.getRoleId());
			taRoleInstMapper.insert(taRoleInstMap);
		}
//		if(userService.isTotalManager(user.getUserId())) {
//			roleMapper.updateByPrimaryKey(taRole);
//			//删除原来的   增加新的
//			TaRoleInstMap taRoleInstMapDelete =new TaRoleInstMap();
//			taRoleInstMapDelete.setRoleId(taRole.getRoleId());
//			taRoleInstMapper.delete(taRoleInstMapDelete);
//			for(int i=0;i<insts.length;i++){
//				TaRoleInstMap taRoleInstMap = new TaRoleInstMap();
//				taRoleInstMap.setInstId(insts[i]);
//				taRoleInstMap.setRoleId(taRole.getRoleId());
//				taRoleInstMapper.insert(taRoleInstMap);
//			}
//		} else {
//			taRole.setOpratorType(OpratorConstants.EDIT);
//			taRole.setApproveStatus(ApproveStatusConstants.APPROVE_ING);
//			taRole.setSubmitUserId(user.getUserId());
//			taRole.setSubmitInstId(SlSessionHelper.getInstitutionId());
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("pRoleId", taRole.getRoleId());
//			taRole.setpRoleId(taRole.getRoleId());
//			roleMapper.insert(taRole);
//			//插入机构角色表
//			for(int i=0;i<insts.length;i++){
//				TaRoleInstMap taRoleInstMap =new TaRoleInstMap();
//				taRoleInstMap.setRoleId(taRole.getRoleId());
//				taRoleInstMap.setInstId(insts[i]);
//				taRoleInstMapper.insert(taRoleInstMap);
//			}
//		}
	}
	
	/**
	 * 根据角色id，保存该角色下有权限的菜单及功能点
	 * @param roleManageForSaveVo
	 */
	@Override
	public void saveModuleAndFunctionForRole(RoleManageForSaveVo roleManageForSaveVo){
		//1.将传入数据转换为数组
		List<TaRoleFunctionMap> taRoleFunctionMapBos = new ArrayList<TaRoleFunctionMap>();
		List<TaRoleModuleMap> taRoleModuleMapBos = new ArrayList<TaRoleModuleMap>();
		
		String roleID = roleManageForSaveVo.getRoleId();
		//循环读取角色对应的菜单信息
		if(roleManageForSaveVo.getModuleList() != null){
			for(int i=0;i < roleManageForSaveVo.getModuleList().size();i++){
				Module module_info = roleManageForSaveVo.getModuleList().get(i);
				TaRoleModuleMap taRoleModuleMapBo = new TaRoleModuleMap();
				taRoleModuleMapBo.setRoleId(roleID);
				taRoleModuleMapBo.setAccessFlag("1");
				String moduleId = module_info.getModule_id();
				taRoleModuleMapBo.setModuleId(moduleId);
				
				//循环读取角色对应功能点
				for(int j=0;j < module_info.getFun_list().size();j++){
					TaRoleFunctionMap taRoleFunctionMapBo = new TaRoleFunctionMap();
					String functionId = module_info.getFun_list().get(j);
					taRoleFunctionMapBo.setFunctionId(functionId);
					taRoleFunctionMapBo.setModuleId(moduleId);
					taRoleFunctionMapBo.setRoleId(roleID);
					taRoleFunctionMapBos.add(taRoleFunctionMapBo);
				}
				
				taRoleModuleMapBos.add(taRoleModuleMapBo);
			}
		}
		if(userService.isTotalManager(SlSessionHelper.getUserId())) {
			// 总行管理员直接生效
			//2. 删除角色与功能点权限关系
			roleMapper.deleteRoleForFunction(roleID);
			//3. 删除角色与菜单权限关系
			roleMapper.deleteRoleForModule(roleID);
			
			//4.批量写入角色与功能点权限关系.(清空权限时，不批量写入数据)
			if(taRoleFunctionMapBos.size() > 0){ 
				HashMap<String, List<TaRoleFunctionMap>> TaRoleFunctionMapBoMap = new HashMap<String, List<TaRoleFunctionMap>>();
				TaRoleFunctionMapBoMap.put("RoleForFunctions", taRoleFunctionMapBos);
				roleMapper.insertRoleForFunctionBatch(TaRoleFunctionMapBoMap);
			}
			
			//5.批量写入角色与菜单权限关系.(清空权限时，不批量写入数据)
			if(taRoleModuleMapBos.size() > 0){
				HashMap<String, List<TaRoleModuleMap>> TaRoleModuleMapBoMap = new HashMap<String, List<TaRoleModuleMap>>();
				TaRoleModuleMapBoMap.put("RoleForModules", taRoleModuleMapBos);
				roleMapper.insertRoleForModuleBatch(TaRoleModuleMapBoMap);
			}
			
			//6.重新加载角色权限至内存中
			//roleCache.reload(roleID,null);
		} else {
			if(taRoleModuleMapBos.size() > 0){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("roleId", roleID);
				param.put("approveStatus", ApproveStatusConstants.APPROVE_ING);
				roleModuleMapCopyMapper.deleteByRoleIdAndApproveStatus(param);
				HashMap<String, List<TaRoleModuleMapCopy>> TaRoleModuleMapBoMapCopy = new HashMap<String, List<TaRoleModuleMapCopy>>();
				List<TaRoleModuleMapCopy> roleModuleList = new ArrayList<TaRoleModuleMapCopy>();
				for(TaRoleModuleMap roleModuleMap : taRoleModuleMapBos) {
					TaRoleModuleMapCopy roleModuleMapCopy = new TaRoleModuleMapCopy();
					roleModuleMapCopy.setRoleId(roleModuleMap.getRoleId());
					roleModuleMapCopy.setModuleId(roleModuleMap.getModuleId());
					roleModuleMapCopy.setAccessFlag(roleModuleMap.getAccessFlag());
					roleModuleMapCopy.setSubmitUserId(SlSessionHelper.getUserId());
					roleModuleMapCopy.setApproveStatus(ApproveStatusConstants.APPROVE_ING);
					roleModuleList.add(roleModuleMapCopy);
				}
				TaRoleModuleMapBoMapCopy.put("RoleForModules", roleModuleList);
				roleModuleMapCopyMapper.insertRoleForModuleBatch(TaRoleModuleMapBoMapCopy);
			}
		}
	}

	/**
	 * 复制角色
	 * @param roleManageForSaveVo
	 */
	@Override
	public void copyRole(RoleManageForSaveVo roleManageForSaveVo){
		/**
		 * 角色部分
		 */
		//1.查询角色详细
		TaRole param = new TaRole();
		param.setRoleId(roleManageForSaveVo.getRoleId());
		TaRole taRole = roleMapper.selectByPrimaryKey(param);
		//2.清空角色ID以便于根据序列生成新的角色ID
		taRole.setRoleId(null);
		//3.修改角色名称
		taRole.setRoleName(taRole.getRoleName() + "_copy");
		//4.保存新角色
		this.createRole(taRole);
		
		/**
		 * 权限部分
		 */
		//5.角色ID修改为新ID 除此之外原封不动
		roleManageForSaveVo.setRoleId(taRole.getRoleId());
		//6.保存新角色权限及功能点
		this.saveModuleAndFunctionForRole(roleManageForSaveVo);
	}
	
	/**
	 * 查询父菜单信息
	 * @return
	 */
	@Override
	public List<RoleModule> searchParModule() {
		List<TaModule> list = moduleMapper.searchParModule();
		return createModleList(list);
	}

	@Override
	public List<TaRole> getRoleList(Map<String, Object> params) {
		return roleMapper.selectRoleList(params);
	}

//	@Override
//	public List<TaRole> getInstRoles(Map<String, Object> map) {
//		//多机构时候区分到底该用户属于哪个机构即使名字重复
//		TaUser user = userMapper.selectByPrimaryKey(userRoleMap.getUserId());
//		
//		
//		List<TaRole> roleList = new ArrayList<TaRole>();
//		if(user != null) {
//			List<String> instIdList = institutionService.childrenInstitutionIds(user.getInstId());
//			instIdList.add(user.getInstId());
//			if(user.getFtaInstId() != null) {
//				List<String> ftaInstIdList = institutionService.childrenInstitutionIds(user.getFtaInstId());
//				ftaInstIdList.add(user.getFtaInstId());
//				instIdList.addAll(ftaInstIdList);
//			}
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("instIdList", instIdList);
//			roleList = roleMapper.selectRoleByInstIdList(map);
//		}
//		return roleList;
//	}
	@Override
	public List<TaRole> getInstRoles(Map<String, Object> map) {
	    //获取当前登录人应该能看到的权限
		TaUser user = userMapper.selectTaUserByInstAndBranchIdAndUserId(map);
		List<TaRole> roleList = new ArrayList<TaRole>();
		if(user != null) {
			List<String> instIdLists = new ArrayList<String>();
			if(user.getInstId()!=null && !"".equals(user.getInstId())) {
				HashMap<String,Object> mapInst  =new HashMap<String, Object>();
				mapInst.put("userId", user.getUserId());
				List<String> inst_id = taUserInstMapper.searchInstByUserId(mapInst);
				if(inst_id!=null && inst_id.size()>0) {
					for(String inst:inst_id) {
						List<String> instIdList = institutionService.childrenInstitutionIds(inst);
						if(instIdList!=null && instIdList.size()>0) {
							instIdLists.addAll(instIdList);
						}
					}
					instIdLists.addAll(inst_id);
				}
			}
			//List<String> instIdList = institutionService.childrenInstitutionIds(user.getInstId());
			
			if(user.getFtaInstId() != null) {
				List<String> ftaInstIdList = institutionService.childrenInstitutionIds(user.getFtaInstId());
				ftaInstIdList.add(user.getFtaInstId());
				instIdLists.addAll(ftaInstIdList);
			}
			Map<String, Object> mapRoleList = new HashMap<String, Object>();
			mapRoleList.put("instIdList", instIdLists);
			mapRoleList.put("branchId", map.get("branchId"));
			//根据机构Id 银行Id 查询角色
			roleList =roleMapper.selectRoleListByInstsAndBranchId(mapRoleList);
			//roleList = roleMapper.selectRoleByInstIdList(mapRoleList);
		}
		return roleList;
	}

	@Override
	public Page<TaRole> getWaitApprovePage(Map<String, Object> map) {
		map.put("approveStatus", ApproveStatusConstants.APPROVE_ING);
		return roleMapper.findByOpratorTypeAndApproveStatus(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TaRole> getApprovedList(Map<String, Object> map) {
		map.put("approveStatus", ApproveStatusConstants.APPROVE_ED);
		return roleMapper.findByOpratorTypeAndApproveStatus(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public TaFunction getFunc(TaFunction taFunction) {
		return functionMapper.selectOne(taFunction);
	}

	@Override
	public void approveAdd(Map<String, Object> map) {
		String roleId = ParameterUtil.getString(map, "roleId", "");
		TaRole role = roleMapper.selectByPrimaryKey(roleId);
		// 修改状态标志为审批通过
		role.setApproveStatus(ApproveStatusConstants.APPROVE_ED);
		role.setApproveUserId(SlSessionHelper.getUserId());
		role.setApproveInstId(SlSessionHelper.getInstitutionId());
		role.setApproveDate(DateTimeUtil.getLocalDateTime());
		roleMapper.updateByPrimaryKey(role);
	}

	@Override
	public void approveUpdate(Map<String, Object> map) {
		String pRoleId = ParameterUtil.getString(map, "pRoleId", "");
				
		// 更新需要修改的角色信息
		TaRole role = roleMapper.selectByPrimaryKey(pRoleId);
		TaRole newRole = roleMapper.findByPRoleId(map).get(0);
		role.setRoleFlag(newRole.getRoleFlag());
		role.setRoleName(newRole.getRoleName());
		role.setIsActive(newRole.getIsActive());
		role.setRoleMemo(newRole.getRoleMemo());
		role.setInstType(newRole.getInstType());
		role.setInstId(newRole.getInstId());
		role.setRoleLevel(newRole.getRoleLevel());
		roleMapper.updateByPrimaryKey(role);
				
		// 更新临时数据为已审批
		newRole.setApproveStatus(ApproveStatusConstants.APPROVE_ED);
		newRole.setApproveUserId(SlSessionHelper.getUserId());
		newRole.setApproveInstId(SlSessionHelper.getInstitutionId());
		newRole.setApproveDate(DateTimeUtil.getLocalDateTime());
		roleMapper.updateByPrimaryKey(newRole);
	}

	@Override
	public List<TaRole> getRoleByUserIdAndInstId(Map<String, Object> map) {
		
		return roleMapper.getRoleByUserIdAndInstId(map);
	}

	@Override
	public List<TaRoleInstMap> getAllInstRole(Map<String, Object> map) {
		List<TaRoleInstMap> list = taRoleInstMapper.getAllInstRole(map);
		return list;
	}

	@Override
	public void saveInstRole(TaRoleInstMap instRoleMap) {
		taRoleInstMapper.deleteUserRoleByInstId(instRoleMap.getInstId());
		String roleId = instRoleMap.getRoleId();
		if (StringUtils.isNotEmpty(roleId)) {
			String[] roleIds = roleId.split(",");
			List<TaRoleInstMap> list = new LinkedList<TaRoleInstMap>();
			for (int i = 0; i < roleIds.length; i++) {
				TaRoleInstMap taRoleInstMap = new TaRoleInstMap();
				taRoleInstMap.setInstId(instRoleMap.getInstId());
				taRoleInstMap.setRoleId(roleIds[i]);
				list.add(taRoleInstMap);
			}
			batchDao.batch("com.singlee.capital.system.mapper.TaRoleInstMapper.insert", list);

		}
	}

	@Override
	public Page<TaRole> findByUserIdAndInstIdPag(Map<String, Object> map) {
		
		return roleMapper.findByUserIdAndInstIdPag(map, ParameterUtil.getRowBounds(map));
	}

	@Override
	public Page<TaRole> getRolesByInstAndRoleName(Map<String, Object> map) {
		
		return roleMapper.getRolesByInstAndRoleName(map, ParameterUtil.getRowBounds(map));
	}
	/**
	 * 查询权限功能点
	 */

	@Override
	public List<RoleModule> getAllResourceByBranchId(Map<String, Object> map) {
		List<TaModule> list =null;
		if(map.get("deskId")!=null){
			list =moduleMapper.getResourceByDeskId(map);//根据台子递归出该台子下的所有功能
		}else{
			
			list = moduleMapper.getAllResourceByBranchId(map);//查询全部模块
		}
		if(list.size()>0){
			for (TaModule module : list) {
				HashMap<String, Object> paraMap = new HashMap<String, Object>();
				paraMap.put("roleId", map.get("roleId"));
				paraMap.put("moduleId", module.getModuleId());
				paraMap.put("branchId", map.get("branchId"));
				List<TaFunction> functionList = functionMapper.getFunctionByBranchIdAndRoleId(paraMap);

				module.setFunctions(functionList);
				
			}
			return createModleListSpecial(list);
		}
		return null;
	}
	/**
	 * 根据角色Id  菜单Id (deskid) 银行号 锁定该  菜单应该显示什么
	 */

	@Override
	public List<HashMap<String, Object>> searchModuleByRoleIdAndDeskIdAndBranchId(
			Map<String, Object> mapObj) {
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String,Object>>();
		//根据角色Id  菜单Id (deskid) 银行号 锁定该  菜单应该显示什么
		List<String> moduleList = roleModuleMapMapper.searchModuleIdForRoleIdAndDeskIdAndBranchId(mapObj);
		for (String string : moduleList) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("moduleId",string );
			HashMap<String, Object> paraMap = new HashMap<String, Object>();
			paraMap.put("role_id", map.get("roleId"));
			paraMap.put("module_id", string);
			paraMap.put("branch_id", map.get("branchId"));
			List<String> functionList = roleFunctionMapMapper.searchFunctionIdByRoleIdAndModuleIdAndBranchId(paraMap);
			map.put("list", functionList);
			retList.add(map);
		}
		return retList;
	}
	@Override
	public List<String> getFunctionByMid(Map<String, Object> map) {
		return  roleFunctionMapMapper.getFunctionByMid(map);
	}

	@Override
	public Page<TaFunction> getFunctionByRoleIdAndModuleId(
			Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return functionMapper.getFunctionByRoleIdAndModuleId(map,rb) ;
	}
	/**
	 * 处理原则先删除再插入
	 */

	@Override
	public void savaAllData(List<TaRoleDeskMap> tadesk,List<TaRoleModuleMap> tamodule,List<TaRoleFunctionMap> taFunction,Map<String, Object> map) throws RException {
		//获取前台传过来的数据
		if(tadesk !=null && tadesk.size()>0){
			//批量删除该角色下的台子
			taRoleDeskMapMapper.deleteBatchRoleDeskMap(map);
			//批量插入该角色下的台子
			taRoleDeskMapMapper.addBatchRoleDeskMap(tadesk);
		}
		//插入tamodule
		if(tamodule !=null && tamodule.size()>0){
			//删除 数据
			roleModuleMapMapper.deleteByRoleIdAndBranchId(map);
			//插入数据
			roleModuleMapMapper.addBatchRoleModuleMap(tamodule);
		}
		//删除原来的数据
		roleFunctionMapMapper.deleteByRoleIdAndBranchId(map);
		//插入function 
		if(taFunction !=null && taFunction.size()>0){
			//插入新的数据
			roleFunctionMapMapper.addBatchByRoleFunctionMap(taFunction);
		}
		
		
		
		
	}

	@Override
	public Page<TaRole> pageAllRoleBranchId(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return roleMapper.pageAllRoleBranchId(map,rb);
	}

	@Override
	public List<String> searchInstIdByRoleId(Map<String, Object> map) {
		
		return taRoleInstMapper.searchInstIdByRoleId(map);
	}

	@Override
	public List<String> getHaveDeskByRoleIdAndInstId(Map<String, Object> map) {
		
		return taRoleDeskMapMapper.getHaveDeskByRoleIdAndInstId(map);
	}

	@Override
	public Page<TaFunction> getFunctionByBranchId(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		return functionMapper.getFunctionByBranchId(map,rb);
	}

	@Override
	public List<String> getAllModulesHaving(Map<String, Object> map) {
		return roleModuleMapMapper.getAllModulesHaving(map);
	}

	@Override
	public List<String> searchModuleDetailByRoleIdAndDeskIdAndBranchId(
			Map<String, Object> map) {
		
		return moduleMapper.searchModuleDetailByRoleIdAndDeskIdAndBranchId(map);
	}

	@Override
	public List<String> searchModuleIdForRoleId(String roleId) {
		
		return roleModuleMapMapper.searchModuleIdForRoleId(roleId);
	}
	/**
	 * 角色删除
	 */

	@Override
	public void deleteRole(Map<String, Object> map) {
		//删除角色的同时删除该角色绑定的关系
		//角色台子关系表
		TaRoleDeskMap taRoleDeskMap = new TaRoleDeskMap();
	    taRoleDeskMap.setRoleId((String)map.get("roleId"));
		taRoleDeskMapMapper.delete(taRoleDeskMap);
		
		//角色机构关系表
		TaRoleInstMap taRoleInstMap = new TaRoleInstMap();
		taRoleInstMap.setRoleId((String)map.get("roleId"));
		taRoleInstMapper.delete(taRoleInstMap);
		//角色模块关系表
		TaRoleModuleMap taRoleModuleMap = new TaRoleModuleMap();
		taRoleModuleMap.setRoleId((String)map.get("roleId"));
		roleModuleMapMapper.delete(taRoleModuleMap);
		//角色用户关系表
		TaUserRoleMap taUserRoleMap =new TaUserRoleMap();
		taUserRoleMap.setRoleId((String)map.get("roleId"));
		taUserRoleMapMapper.delete(taUserRoleMap);
		//角色功能关系表
		TaRoleFunctionMap taRoleFunctionMap = new TaRoleFunctionMap();
		taRoleFunctionMap.setRoleId((String)map.get("roleId"));
		roleFunctionMapMapper.delete(taRoleFunctionMap);
		//主表数据
		roleMapper.deleteRoleByBranchId(map);
		
	}

	@Override
	public List<String> searchModuleDetailByAndDeskIdAndBranchId(
			Map<String, Object> map) {
		// TODO Auto-generated method stub
		return moduleMapper.searchModuleDetailByAndDeskIdAndBranchId(map);
	}
    /**
     * 新增func
     */
	@Override
	public void saveFunc(TaFunction taFunction) {
		//获取funcId 最大值
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("moduleId", taFunction.getModuleId());
		map.put("branchId", taFunction.getBranchId());
		String funcId =functionMapper.getMaxFuncByModuleId(map);
		if(funcId!=null){
			taFunction.setFunctionId(taFunction.getModuleId()+"-"+funcId);
		}else{
			taFunction.setFunctionId(taFunction.getModuleId()+"-1");
		}
		
		functionMapper.insert(taFunction);
		
	}

	@Override
	public void updateFunc(TaFunction taFunction) {
		functionMapper.updateByPrimaryKey(taFunction);
	}

	/**
	 * 根据instid查询下属的roleid
	 * @param map
	 * @return
	 */
	@Override
	public List<TaRole> getRoleListByInstId(Map<String, Object> map) {
		//按逗号分隔,组装instList
		String instStr = ParameterUtil.getString(map, "instList", "");
		String[] instArray = instStr.split(",");
		List<String> instList = new ArrayList<String>(instArray.length);
		Collections.addAll(instList,instArray);

		Map<String, Object> mapRoleList = new HashMap<String, Object>();
		mapRoleList.put("instIdList", instList);
		mapRoleList.put("branchId", ParameterUtil.getString(map,"branchId",null));
		return roleMapper.selectRoleListByInstsAndBranchId(mapRoleList);
	}
}

