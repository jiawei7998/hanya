package com.singlee.capital.system.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.service.PortalVirtualService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;


/***
 * 首页布局样式表-默认表  controller
 * @author singlee4
 *
 */
@Controller
@RequestMapping(value = "/PortalVirtualController")
public class PortalVirtualController {
	@Autowired
	PortalVirtualService portalVirtualService;
	
	@ResponseBody
	@RequestMapping(value = "/searchPortalVirtual")
	public RetMsg<Serializable> searchPortalVirtual(@RequestBody Map<String,Object> map) throws IOException {
		
		portalVirtualService.searchDefaultPortal(map);
		return RetMsgHelper.ok();
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
