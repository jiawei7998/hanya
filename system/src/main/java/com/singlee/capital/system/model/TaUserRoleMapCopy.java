package com.singlee.capital.system.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TA_USER_ROLE_MAP_COPY")
public class TaUserRoleMapCopy implements Serializable {

	private static final long serialVersionUID = 1L;

	private String roleId;
	
	private String userId;
	
	private String submitUserId;
	
	private String approveUserId;
	
	private String approveStatus;
	
	private String approveDate;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY,generator="SELECT SEQ_TA_USER_ROLE_MAP_COPY.nextval FROM DUAL")
	private String userRoleId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getSubmitUserId() {
		return submitUserId;
	}

	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	public String getApproveUserId() {
		return approveUserId;
	}

	public void setApproveUserId(String approveUserId) {
		this.approveUserId = approveUserId;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}
}
