package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.dict.InstConstants;
import com.singlee.capital.system.dict.RoleLevelConstants;
import com.singlee.capital.system.mapper.TaRoleMapper;
import com.singlee.capital.system.mapper.TaUserInfoMapper;
import com.singlee.capital.system.mapper.TaUserInstMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.model.*;
import com.singlee.capital.system.service.TaUserInfoService;
import com.singlee.capital.system.service.UserRoleMapService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaUserInfoServiceImpl implements TaUserInfoService {
	
	@Autowired
	private TaUserMapper userMapper;
	@Autowired
	private TaUserInfoMapper userInfoMapper;
	@Autowired
	private TaRoleMapper roleMapper;
	@Autowired
	private TaUserInstMapper  taUserInstMapper;
	@Autowired
	private UserRoleMapService userRoleMapService;
	

	@Override
	@Transactional(value="transactionManager",rollbackFor=Exception.class)
	public void createUserInfo(TaUserInfo userInfo) throws RException {
		userInfoMapper.insert(userInfo);
	}

//	@Override
//	@Transactional(value="transactionManager",rollbackFor=Exception.class)
//	public void updateUserInfo(TaUserInfo userInfo) throws RException {
//		TaUser u = userMapper.selectByPrimaryKey(userInfo);
//		if (u == null) {
//			JY.raiseRException(MsgUtils.getMessage("error.system.0003"));
//		}
//		if (ExternalDictConstants.YesNo_NO.equals(u.getIsActive())) {
//			JY.raiseRException(MsgUtils.getMessage("error.system.0006"));
//		}
//		TaUserInfo taUserInfo = userInfoMapper.selectByPrimaryKey(userInfo);
//		if(taUserInfo == null) {
//			userInfoMapper.insert(userInfo);
//		} else {
//			userInfoMapper.updateByPrimaryKey(userInfo);
//		}
//		if(isTotalManager(SlSessionHelper.getUserId())) {
//			TaUserInfo taUserInfo = userInfoMapper.selectByPrimaryKey(userInfo);
//			if(taUserInfo == null) {
//				userInfoMapper.insert(userInfo);
//			} else {
//				userInfoMapper.updateByPrimaryKey(userInfo);
//			}
//		}else{
//			userInfo.setUserId(userInfo.getUserId() + "_copy");
//			TaUserInfo taUserInfo = userInfoMapper.selectByPrimaryKey(userInfo);
//			if(taUserInfo == null) {
//				userInfoMapper.insert(userInfo);
//			} else {
//				userInfoMapper.updateByPrimaryKey(userInfo);
//			}
//		}
//	}

	@Override
	public Page<TaUserInfo> findUserInfo(Map<String, Object> map) {
		return userInfoMapper.findUserInfoByUserId(map);
	}

	@Override
	public TaUser selectUserInfoById(String userId) {

		HashMap<String, Object> map = new HashMap<>(2);
		map.put("userId",userId);
		TaUser taUser = userMapper.selectUserInfoById(map);
		//获取用户下的角色
		Map<String, Object> allRoleNameByUserID = userMapper.getAllRoleNameByUserID(map);
		taUser.setRoles(allRoleNameByUserID == null ? "" :ParameterUtil.getString(allRoleNameByUserID,"ROLEID",""));
		taUser.setRoleNames(allRoleNameByUserID == null ? "" :ParameterUtil.getString(allRoleNameByUserID,"ROLENAME",""));
		//获取用户所属机构的名称
		Map<String, Object> allInstNameByUserID = userMapper.getAllInstNameByUserID(map);
		taUser.setInstName(ParameterUtil.getString(allInstNameByUserID,"INSTNAME",""));
		return taUser;
	}

	/**
	 * 判断用户是否是总行管理员
	 * @param userId
	 * @return
	 */
	private boolean isTotalManager(String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("instId", InstConstants.TOTAL_MANAGER_INST_ID);
		map.put("roleLevel", RoleLevelConstants.ROLE_MANAGER);
		map.put("userId", userId);
		List<TaRole> roleList = roleMapper.getRoleByUserIdAndRoleLevel(map);
		if(roleList.isEmpty()) {
			return false;
		}else{
			if(!InstConstants.TOTAL_MANAGER_INST_ID.equals(SlSessionHelper.getInstitutionId())) {
				return false;
			}
		}
		return true;
	}

	@Override
	@AutoLogMethod(value = "用户修改",id="0003")
	public void updateUserInfo(Map<String, Object> map,TaUser oldUser) {
		TaUser userKey =userMapper.selectByPrimaryKey(map.get("userId"));
		if(userKey !=null){
			//修改用户主表数据
			//TaUser user =new TaUser();
			BeanUtil.populate(userKey, map);
			userMapper.updateByPrimaryKey(userKey);

			//修改用户机构表数据
			String insts=(String)map.get("instId");
			//删除原来的机构；
			TaUserInst userInst =new TaUserInst();
			userInst.setUserId(userKey.getUserId());
			taUserInstMapper.delete(userInst);
			//插入新的机构
			String[] instsList =insts.split(",");
			if(instsList!=null && instsList.length>0){
				for(int i=0;i<instsList.length;i++){
					TaUserInst userInstAdd =new TaUserInst();
					userInstAdd.setUserId(userKey.getUserId());
					userInstAdd.setInstId(instsList[i]);
					taUserInstMapper.insert(userInstAdd);
				}
			}

			//更新角色
			String roleId = ParameterUtil.getString(map, "roles", "");
			String userId = ParameterUtil.getString(map, "userId", "");
			TaUserRoleMap taUserRoleMap = new TaUserRoleMap(roleId,userId);
			userRoleMapService.saveUserRole(taUserRoleMap);

			//修改用户附表数据

			TaUserInfo userInfo =userInfoMapper.selectByPrimaryKey(map.get("userId"));
			if(userInfo!=null){
				BeanUtil.populate(userInfo, map);
				userInfoMapper.updateByPrimaryKey(userInfo);
			}
		}
	}
}
