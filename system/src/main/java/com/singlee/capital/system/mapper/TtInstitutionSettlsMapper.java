package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TtInstitutionSettls;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtInstitutionSettlsMapper extends Mapper<TtInstitutionSettls> {

	Page<TtInstitutionSettls> getPageInstSettls(Map<String, Object> map, RowBounds rowBounds);
	
	/**
	 * 根据机构号查询98账号，用来查询T24 98账号 明细
	 * @param map
	 * @return
	 */
	List<TtInstitutionSettls> getInstSettlsList(Map<String, Object> map);
	
	List<TtInstitutionSettls> getInstSettlsListByInstAcctNo(Map<String, Object> map);
	
	public TtInstitutionSettls getInstSettlsByInstAcctNo(String inst_acct_no);
	public TtInstitutionSettls getInstSettlsByInstAcctNoAndDealno(Map<String, Object> map);
	
	/**
	 * 根据动账通知账号模糊匹配98账号
	 * Demo：31608198000004596 模糊匹配 '%'||98000004596
	 * @param inst_acct_no：账号， inst_id：机构号
	 * @return
	 */
	public TtInstitutionSettls getInstSettlsByLikeAcctNo(Map<String, Object> map);
}
