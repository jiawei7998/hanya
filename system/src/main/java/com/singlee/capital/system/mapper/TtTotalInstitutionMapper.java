package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TtTotalInstitution;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtTotalInstitutionMapper extends Mapper<TtTotalInstitution>{

	List<TtTotalInstitution> findByIsActive(Map<String, Object> map);
}
