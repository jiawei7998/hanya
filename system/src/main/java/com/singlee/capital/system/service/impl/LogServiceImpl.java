package com.singlee.capital.system.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.TaLogMapper;
import com.singlee.capital.system.model.TaLog;
import com.singlee.capital.system.service.LogService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 日志服务层
 * 
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class LogServiceImpl implements LogService {

	@Autowired
	private TaLogMapper logMapper;

	@Override
	public Page<TaLog> pageLog (Map<String, Object> params) {
		RowBounds rowBounds = ParameterUtil.getRowBounds(params);
		Page<TaLog> result = logMapper.pageLog(params, rowBounds);
		return result;
	}

}
