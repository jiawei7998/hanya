package com.singlee.capital.system.mapper;

import com.singlee.capital.system.model.TaPortalVirtual;

import java.util.List;
import java.util.Map;


/***
 * 首页布局样式表-默认表   mapper
 * @author singlee4
 *
 */
public interface TaPortalVirtualMapper {
	
	//查询默认样式表
	List<TaPortalVirtual> searchDefaultPortal(Map<String, Object> params);

}
