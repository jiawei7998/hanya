/**
 * 
 */
package com.singlee.capital.system.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaLog;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 生产业务日志
 */
public interface  TaLogMapper extends Mapper<TaLog> {
	
	/**
	 * 根据对象查询列表
	 * 
	 * @param map
	 * @param rb - 分页对象
	 * @return 日志对象列表
	 */
	public Page<TaLog> pageLog(Map<String, Object> params, RowBounds rb);
	
	/**
	 * 查詢報文
	 * @param map
	 * @return
	 */
	public List<TaLog> queryTaLogMessage(Map<String, Object> map);
	
}
