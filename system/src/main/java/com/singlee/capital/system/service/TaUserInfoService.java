package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TaUserInfo;

import java.util.Map;

public interface TaUserInfoService {

	/**
	 * 新增用户信息
	 * @param userInfo
	 */
	void createUserInfo(TaUserInfo userInfo);
	
	/**
	 * 更新用户信息
	 * @param userInfo
	 */
//	void updateUserInfo(TaUserInfo userInfo);
	void updateUserInfo(Map<String, Object> map,TaUser oldUser);
	
	/**
	 * 查询用户详细信息
	 * @param map
	 * @return
	 */
	Page<TaUserInfo> findUserInfo(Map<String, Object> map);

	TaUser selectUserInfoById(String userId);
}
