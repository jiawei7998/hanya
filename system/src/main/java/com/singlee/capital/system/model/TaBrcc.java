package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TA_BRCC")
public class TaBrcc implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1142409011880859367L;

	/**
	 * 银行ID
	 */
	private String branchId;
	/**
	 * 银行名称
	 */
	private String branchName;
	/**
	 * 银行标志
	 */
	private String branchLogo;
	/**
	 * 系统名称
	 */
	private String systemName;
	/**
	 * 银行简称
	 */
	private String branchCn;

	private String branchModule;

	private String branchTitle;
	/**
	 * 系统首页
	 */
	private String systemHome;

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchLogo() {
		return branchLogo;
	}

	public void setBranchLogo(String branchLogo) {
		this.branchLogo = branchLogo;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getBranchCn() {
		return branchCn;
	}

	public void setBranchCn(String branchCn) {
		this.branchCn = branchCn;
	}

	public String getBranchModule() {
		return branchModule;
	}

	public void setBranchModule(String branchModule) {
		this.branchModule = branchModule;
	}

	public String getBranchTitle() {
		return branchTitle;
	}

	public void setBranchTitle(String branchTitle) {
		this.branchTitle = branchTitle;
	}

	public String getSystemHome() {
		return systemHome;
	}

	public void setSystemHome(String systemHome) {
		this.systemHome = systemHome;
	}

}
