package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaLog;

import java.util.Map;

public interface LogService {

	/**
	 * 根据对象查询列表
	 * 
	 * @param map
	 * @param rb - 分页对象
	 * @return 日志对象列表
	 */
	public Page<TaLog> pageLog(Map<String,Object> params);

}
