package com.singlee.capital.system.mapper;

import com.singlee.capital.common.spring.mybatis.MyMapper;
import com.singlee.capital.system.model.TaUserRoleMap;

import java.util.List;

/**
 * @projectName 同业业务管理系统
 * @className 用户角色映射持久层
 * @description TODO
 * @author Hunter
 * @createDate 2016-7-25 上午09:02:28
 * @mender TODO
 * @modifyDate TODO
 * @modifyContent TODO
 * @company 杭州新利科技有限公司

 * @version 1.0
 */
public interface TaUserRoleMapMapper extends MyMapper<TaUserRoleMap> {
	
	/**
	 * 根据用户主键查询列表
	 * 
	 * @param userRoleMap - 用户角色映射对象
	 * @return 用户角色对象列表
	 * @author Hunter
	 * @date 2016-7-25
	 */
	public List<TaUserRoleMap> listUserRole(TaUserRoleMap userRoleMap);
	
	/**
	 * 根据用户主键删除用户角色映射
	 * 
	 * @param userId - 用户主键
	 * @author Hunter
	 * @date 2016-7-25
	 */
	public void deleteUserRoleByUserId(String userId);
}