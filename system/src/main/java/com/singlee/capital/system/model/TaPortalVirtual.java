package com.singlee.capital.system.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 首页布局样式表-默认表
 * @author singlee4
 *
 */
@Entity
@Table(name = "TA_PORTAL_VIRTUAL")
public class TaPortalVirtual implements Serializable{

	private static final long serialVersionUID = 1L;
	
	// 流水号
	private String protalSn;
	
	// portal定位
	private Integer portalColumn;
	
	// panel id
	private String portalId;
	
	// panel 标题
	private String portalTitle;
	
	// panel 内容
	private String portalBody;
	
	// 是否允许拖拽
	private String portalAllowDrag;
	
	// panel 高度
	private String portalHeight;
	
	// portal 行数
	private BigDecimal portalRows;
	
	// 角色ID
	private String portalRoleid;
	
	// 银行归属ID
	private String portalBranchid;
		
	// 用户ID
	private String portalUserid;

	public String getProtalSn() {
		return protalSn;
	}

	public void setProtalSn(String protalSn) {
		this.protalSn = protalSn;
	}

	public Integer getPortalColumn() {
		return portalColumn;
	}

	public void setPortalColumn(Integer portalColumn) {
		this.portalColumn = portalColumn;
	}

	public String getPortalId() {
		return portalId;
	}

	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	public String getPortalTitle() {
		return portalTitle;
	}

	public void setPortalTitle(String portalTitle) {
		this.portalTitle = portalTitle;
	}

	public String getPortalBody() {
		return portalBody;
	}

	public void setPortalBody(String portalBody) {
		this.portalBody = portalBody;
	}

	public String getPortalAllowDrag() {
		return portalAllowDrag;
	}

	public void setPortalAllowDrag(String portalAllowDrag) {
		this.portalAllowDrag = portalAllowDrag;
	}

	public String getPortalHeight() {
		return portalHeight;
	}

	public void setPortalHeight(String portalHeight) {
		this.portalHeight = portalHeight;
	}

	public BigDecimal getPortalRows() {
		return portalRows;
	}

	public void setPortalRows(BigDecimal portalRows) {
		this.portalRows = portalRows;
	}

	public String getPortalRoleid() {
		return portalRoleid;
	}

	public void setPortalRoleid(String portalRoleid) {
		this.portalRoleid = portalRoleid;
	}

	public String getPortalBranchid() {
		return portalBranchid;
	}

	public void setPortalBranchid(String portalBranchid) {
		this.portalBranchid = portalBranchid;
	}

	public String getPortalUserid() {
		return portalUserid;
	}

	public void setPortalUserid(String portalUserid) {
		this.portalUserid = portalUserid;
	}

	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	

}
