package com.singlee.capital.system.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.system.model.TaUserParam;
import com.singlee.capital.system.service.UserParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户参数请求控制器
 * 
 * @author dingzy
 * 
 */
@Controller
@RequestMapping(value = "/UserParamController")
public class UserParamController extends CommonController {


	/** 柜员服务接口 spring 负责注入 */
	@Autowired
	private UserParamService userParamService;
	
	/**
	 * 查询柜员 列表 map 包含
	 * 
	 * @param inst_id_search
	 *            可以为空，返回所有。不为空返回模糊匹配
	 * @param user_id_search
	 *            可以为空，返回所有。不为空返回模糊匹配
	 * @param no_user_id
	 *            可以为空，返回所有。不为空返回模糊匹配
	 * @param user_name_search
	 *            可以为空，返回所有。不为空返回模糊匹配
	 * @param is_active
	 *            : 是否为启用 可以为空，返回所有。不为空返回匹配记录
	 * @param pageNumber
	 *            页号
	 * @param pageSize
	 *            页大小
	 */
	@RequestMapping(value = "/pageTaUserParams")
	@ResponseBody
	public RetMsg<PageInfo<TaUserParam>> pageTaUserParams(@RequestBody Map<String,Object> map) throws IOException {
		return RetMsgHelper.ok(userParamService.pageTaUserParams(map));
	}

	/**
	 * 添加用户参数
	 * @date 2015-12-7
	 * @author
	 * @returnType void
	 */
	@RequestMapping(value = "/addTaUserParam")
	@ResponseBody
	public RetMsg<Boolean> addTaUserParam(@RequestBody TaUserParam taUserParam) throws IOException {
		return userParamService.addUserParam(taUserParam);
	}

	/**
	 * 修改用户参数
	 * @date 2015-12-7
	 * @author
	 * @returnType void
	 */
	@RequestMapping(value = "/editTaUserParam")
	@ResponseBody
	public RetMsg<Serializable> editTaUserParam(@RequestBody TaUserParam taUserParam) throws IOException {
		userParamService.modifyUserParam(taUserParam);
		return RetMsgHelper.ok();		
	}

	/**
	 * 删除用户参数
	 * @date 2015-12-7
	 * @author
	 * @returnType void
	 */
	@RequestMapping(value = "/delTaUserParam")
	@ResponseBody
	public RetMsg<Serializable> delTaUserParam(@RequestBody TaUserParam taUserParam) throws IOException {
		userParamService.delUserParam(taUserParam);
		return RetMsgHelper.ok();
	}
	
	/**
	 * 根据类别查询特定参数
	 * @param  p_type 大类
	 * @param  p_sub_type 小类
	 */
	@RequestMapping(value = "/searchParamByType")
	@ResponseBody
	public List<TaUserParam> searchParamByType(HttpServletRequest request) throws IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("p_type", request.getParameter("p_type"));
		map.put("p_sub_type", request.getParameter("p_sub_type"));
		List<TaUserParam> list = userParamService.getParamList(map);
		return list;
	}

	
	/**
	 * 根据类别查询特定参数
	 * @param  p_type 大类
	 * @param  p_sub_type 小类
	 */
	@RequestMapping(value = "/searchParamByTypeByMap")
	@ResponseBody
	public RetMsg<List<TaUserParam>> searchParamByTypeByMap(@RequestBody Map<String,Object> map) throws IOException {
		List<TaUserParam> list = userParamService.getParamList(map);
		return RetMsgHelper.ok(list);
	}
	
	/**
	 * 参数复核查询
	 * @param map
	 * @return
	 * @throws IOException
	 * @author lijing
	 */
	@RequestMapping(value = "/searchUserParamWaitingVerify")
	@ResponseBody
	public RetMsg<PageInfo<TaUserParam>> searchUserParamWaitingVerify(@RequestBody Map<String,Object> map) throws IOException {
		return RetMsgHelper.ok(userParamService.pageTaUserParamsVerify(map));
	}
	
	/**
	 * 提交复核
	 * @date 2018-1-25
	 * @author lijing
	 * @returnType 
	 */
	@RequestMapping(value = "/submitVerify")
	@ResponseBody
	public RetMsg<Serializable> submitVerify(@RequestBody TaUserParam taUserParam) throws IOException {
		userParamService.modifyUserParamVerify1(taUserParam);
		return RetMsgHelper.ok();		
	}
	
	/**
	 * 复核通过
	 * @date 2018-1-25
	 * @author lijing
	 * @returnType 
	 */
	@RequestMapping(value = "/verifyOperation")
	@ResponseBody
	public RetMsg<Serializable> verifyOperation(@RequestBody TaUserParam taUserParam) throws IOException {
		return userParamService.modifyUserParamVerify2(taUserParam);
		
	}
	
	/**
	 * 复核拒绝
	 * @date 2018-1-25
	 * @author lijing
	 * @returnType 
	 */
	@RequestMapping(value = "/verifyRefuse")
	@ResponseBody
	public RetMsg<Serializable> verifyRefuse(@RequestBody TaUserParam taUserParam) throws IOException {
		return userParamService.modifyUserParamVerify3(taUserParam);
	}
	
}