package com.singlee.capital.system.model;

import com.singlee.capital.common.util.StringUtil;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * 柜员 用户
 * 
 * @author LyonChen
 * 
 */

@Entity
@Table(name = "TA_USER")
public class TaUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String userId;// 用户ID
	private String userName;// 用户姓名
	private String userPwd;// 密码
	private String isActive;// 状态 1-启用 0-停用 字典项 000031
	private String userCreatetime;// 用户创建时间
	private String userStoptime;// 用户停用时间
	private String userPwdchgtime;// 上次密码更改时间
	private String userMemo;// 备注
	private String userEmail;// 用户EMAIL
	private String userFixedphone;// 固定电话
	private String userCellphone;// 移动电话
	private String instId;// 机构号
	private String userFlag;// 标识 字典项 000003
	private String userLogontype;// 登陆方式 0000005
	private String isOnline;// 是否在线
	private String latestIp;// 最后一次ip地址
	private String latestLoginTime;// 最后一次登录时间
	private String latestTime;// 最后访问时间
	private String userImage;// 用户照片
	private String ftaInstId;// 所属自贸区机构
	private String isInterbank;// 是否同业业务人员
	private String opratorType;
	private String approveStatus;
	private String submitUserId;
	private String submitInstId;
	private String approveUserId;
	private String approveInstId;
	private String approveDate;
	private String versionCode;
	private String isAccess;
	private String branchId;

	@Transient
	private List<String> roleIdList; //角色 的id 列表
	@Transient
	private List<String> roleNameList; //角色 的名称 列表
	
	@Transient
	private String instName; // 机构名称
	private String empId;//员工编号
	@Transient
	private String ftaInstName;// 所属自贸区机构
	@Transient
	private String roles;// 角色id（逗号隔开）
	@Transient
	private String roleNames;// 角色名称（逗号隔开）
	@Transient
	private String sex;
	@Transient
	private String birthDate;         
	@Transient
	private String highestEducation;    
	@Transient
	private String major;       
	@Transient
	private String position;    
	@Transient
	private String interbankStartDate;    
	@Transient
	private String workStartDate;     
	@Transient
	private String businessDomain;       
	@Transient
	private String employmentExperience;   
	@Transient
	private String otherInstitution;       
	@Transient
	private String certificateQualification; 
	@Transient
	private String examinationResults;     
	@Transient
	private String qualificationEndDate;   
	@Transient
	private String isCertificate;    
	@Transient
	private String comm;
	@Transient
	private String submitInstName;
	@Transient
	private String approveInstName;
	@Transient 
	private String insts;
	@Transient 
	private String instsId;//多机构查询时使用这个机构Id 用于显示
	private String cfetsTrad;//登陆cfets用户
	private String opicsTrad;//登陆opics用户
	private String opicsBr;//用户所属OPICS部门
	@Transient
	private String sessionId;//sessionId用于查询
	

	public String getOpicsBr() {
		return opicsBr;
	}

	public void setOpicsBr(String opicsBr) {
		this.opicsBr = opicsBr;
	}

	public String getInsts() {
		return insts;
	}

	public void setInsts(String insts) {
		this.insts = insts;
	}

	public String getInstsId() {
		return instsId;
	}

	public void setInstsId(String instsId) {
		this.instsId = instsId;
	}

	public String getIsAccess() {
		return isAccess;
	}

	public void setIsAccess(String isAccess) {
		this.isAccess = isAccess;
	}

	public List<String> getRoleNameList() {
		return roleNameList;
	}

	public void setRoleNameList(List<String> roleNameList) {
		this.roleNameList = roleNameList;
	}

	public List<String> getRoleIdList() {
		return roleIdList;
	}

	public void setRoleIdList(List<String> roleList) {
		this.roleIdList = roleList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getUserCreatetime() {
		return userCreatetime;
	}

	public void setUserCreatetime(String userCreatetime) {
		this.userCreatetime = userCreatetime;
	}

	public String getUserStoptime() {
		return userStoptime;
	}

	public void setUserStoptime(String userStoptime) {
		this.userStoptime = userStoptime;
	}

	public String getUserPwdchgtime() {
		return userPwdchgtime;
	}

	public void setUserPwdchgtime(String userPwdchgtime) {
		this.userPwdchgtime = userPwdchgtime;
	}

	public String getUserMemo() {
		return userMemo;
	}

	public void setUserMemo(String userMemo) {
		this.userMemo = userMemo;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserFixedphone() {
		return userFixedphone;
	}

	public void setUserFixedphone(String userFixedphone) {
		this.userFixedphone = userFixedphone;
	}

	public String getUserCellphone() {
		return userCellphone;
	}

	public void setUserCellphone(String userCellphone) {
		this.userCellphone = userCellphone;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getUserFlag() {
		return userFlag;
	}

	public void setUserFlag(String userFlag) {
		this.userFlag = userFlag;
	}

	public String getUserLogontype() {
		return userLogontype;
	}

	public void setUserLogontype(String userLogontype) {
		this.userLogontype = userLogontype;
	}

	public String getIsOnline() {
		return isOnline;
	}

	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}

	public String getLatestIp() {
		return latestIp;
	}

	public void setLatestIp(String latestIp) {
		this.latestIp = latestIp;
	}

	public String getLatestLoginTime() {
		return latestLoginTime;
	}

	public void setLatestLoginTime(String latestLoginTime) {
		this.latestLoginTime = latestLoginTime;
	}

	public String getLatestTime() {
		return latestTime;
	}

	public void setLatestTime(String latestTime) {
		this.latestTime = latestTime;
	}

	public String getInstName() {
		return instName;
	}

	public void setInstName(String instName) {
		this.instName = instName;
	}
	
	/**
	 * 检查密码 送过来的 pass 参数已经是 由js加密成 md5
	 * @return
	 */
	public boolean checkPassword(String pass){
		// 是否状态合法
		return !StringUtil.checkEmptyNull(pass) && pass.equalsIgnoreCase(this.userPwd);
	}

	@Override
	public String toString() {
		return String
				.format("TaUser [userId=%s, userName=%s, userPwd=%s, isActive=%s, userCreatetime=%s, userStoptime=%s, " +
						"userPwdchgtime=%s, userMemo=%s, userEmail=%s, userFixedphone=%s, userCellphone=%s, instId=%s, " +
						"userFlag=%s, userLogontype=%s, isOnline=%s, latestIp=%s, latestLoginTime=%s, latestTime=%s," +
						"empId=%s,cfetsTrad=%s,opicsTrad=%s]",
						userId, userName, userPwd, isActive, userCreatetime,
						userStoptime, userPwdchgtime, userMemo, userEmail,
						userFixedphone, userCellphone, instId, userFlag,
						userLogontype, isOnline, latestIp, latestLoginTime,
						latestTime,empId,cfetsTrad,opicsTrad);
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	public String getFtaInstId() {
		return ftaInstId;
	}

	public void setFtaInstId(String ftaInstId) {
		this.ftaInstId = ftaInstId;
	}

	public String getFtaInstName() {
		return ftaInstName;
	}

	public void setFtaInstName(String ftaInstName) {
		this.ftaInstName = ftaInstName;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getIsInterbank() {
		return isInterbank;
	}

	public void setIsInterbank(String isInterbank) {
		this.isInterbank = isInterbank;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getHighestEducation() {
		return highestEducation;
	}

	public void setHighestEducation(String highestEducation) {
		this.highestEducation = highestEducation;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getInterbankStartDate() {
		return interbankStartDate;
	}

	public void setInterbankStartDate(String interbankStartDate) {
		this.interbankStartDate = interbankStartDate;
	}

	public String getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(String workStartDate) {
		this.workStartDate = workStartDate;
	}

	public String getBusinessDomain() {
		return businessDomain;
	}

	public void setBusinessDomain(String businessDomain) {
		this.businessDomain = businessDomain;
	}

	public String getEmploymentExperience() {
		return employmentExperience;
	}

	public void setEmploymentExperience(String employmentExperience) {
		this.employmentExperience = employmentExperience;
	}

	public String getOtherInstitution() {
		return otherInstitution;
	}

	public void setOtherInstitution(String otherInstitution) {
		this.otherInstitution = otherInstitution;
	}

	public String getCertificateQualification() {
		return certificateQualification;
	}

	public void setCertificateQualification(String certificateQualification) {
		this.certificateQualification = certificateQualification;
	}

	public String getExaminationResults() {
		return examinationResults;
	}

	public void setExaminationResults(String examinationResults) {
		this.examinationResults = examinationResults;
	}

	public String getQualificationEndDate() {
		return qualificationEndDate;
	}

	public void setQualificationEndDate(String qualificationEndDate) {
		this.qualificationEndDate = qualificationEndDate;
	}

	public String getIsCertificate() {
		return isCertificate;
	}

	public void setIsCertificate(String isCertificate) {
		this.isCertificate = isCertificate;
	}

	public String getComm() {
		return comm;
	}

	public void setComm(String comm) {
		this.comm = comm;
	}

	public String getOpratorType() {
		return opratorType;
	}

	public void setOpratorType(String opratorType) {
		this.opratorType = opratorType;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getSubmitUserId() {
		return submitUserId;
	}

	public void setSubmitUserId(String submitUserId) {
		this.submitUserId = submitUserId;
	}

	public String getSubmitInstId() {
		return submitInstId;
	}

	public void setSubmitInstId(String submitInstId) {
		this.submitInstId = submitInstId;
	}

	public String getApproveUserId() {
		return approveUserId;
	}

	public void setApproveUserId(String approveUserId) {
		this.approveUserId = approveUserId;
	}

	public String getApproveInstId() {
		return approveInstId;
	}

	public void setApproveInstId(String approveInstId) {
		this.approveInstId = approveInstId;
	}

	public String getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getSubmitInstName() {
		return submitInstName;
	}

	public void setSubmitInstName(String submitInstName) {
		this.submitInstName = submitInstName;
	}

	public String getApproveInstName() {
		return approveInstName;
	}

	public void setApproveInstName(String approveInstName) {
		this.approveInstName = approveInstName;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public void setOpicsTrad(String opicsTrad) {
		this.opicsTrad = opicsTrad;
	}

	public String getOpicsTrad() {
		return opicsTrad;
	}

	public void setCfetsTrad(String cfetsTrad) {
		this.cfetsTrad = cfetsTrad;
	}

	public String getCfetsTrad() {
		return cfetsTrad;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getRoleNames() {
		return roleNames;
	}

	public void setRoleNames(String roleNames) {
		this.roleNames = roleNames;
	}
}
