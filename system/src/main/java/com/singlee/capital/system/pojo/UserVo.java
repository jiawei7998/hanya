package com.singlee.capital.system.pojo;

import com.singlee.capital.system.model.TaUser;

import java.util.List;

/**
 * 柜员 用户
 * 
 * @author LyonChen
 * 
 */
public class UserVo extends TaUser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<String> role_id_list; // 所属的所有的角色列表（包含 禁用状态的）
	
	private String inst_name;
	private String types;
	
	public UserVo() {
	}

	public List<String> getRole_id_list() {
		return role_id_list;
	}

	public void setRole_id_list(List<String> role_id_list) {
		this.role_id_list = role_id_list;
	}

	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public String getInst_name() {
		return inst_name;
	}

	public void setInst_name(String inst_name) {
		this.inst_name = inst_name;
	}
	
}
