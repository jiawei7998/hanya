package com.singlee.capital.system.service;

import com.github.pagehelper.Page;
import com.singlee.capital.system.model.TaModuleCopy;
import com.singlee.capital.system.model.TaRoleModuleMapCopy;

import java.util.List;
import java.util.Map;

public interface RoleModuleService {
	
	public Page<TaRoleModuleMapCopy> getWaitApproveList(Map<String, Object> map);
	
	public List<TaModuleCopy> findOldModuleList(Map<String, Object> map);
	
	public List<TaModuleCopy> findNewModuleList(Map<String, Object> map);
	
	public void doApprove(Map<String, Object> map);

	public Page<TaRoleModuleMapCopy> getApprovedList(Map<String, Object> map);
	
	public List<String> searchModuleIdForRoleIdAndDeskIdAndBranchId(Map<String, Object> map);

	public List<String> getModuleByRoleId(String role_id);
}
