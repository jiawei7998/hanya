package com.singlee.capital.system.controller;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.system.service.GenericCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 通用对象crud操作
 * @author Lyon Chen 
 */
@Controller
@RequestMapping(value = "/GenericCrudController")
public class GenericCrudController extends CommonController {

	@Autowired
	private GenericCrudService gcs;


	@ResponseBody
	@RequestMapping(value = "/{beanName}/{mapperName}/page")
	public RetMsg<List<Object>> page(@PathVariable("beanName") String beanName,
							@PathVariable("mapperName") String mapperName,		
			@RequestBody Map<String,String> postObj) throws RException {
		List<Object> list = gcs.page(beanName, mapperName, postObj);
		return RetMsgHelper.ok(list);
	}

	
	@ResponseBody
	@RequestMapping(value = "/{beanName}/{mapperName}/add")
	public RetMsg<Serializable> addInstitution(@PathVariable("beanName") String beanName,
			@PathVariable("mapperName") String mapperName,
			@RequestBody Map<String,Object> postObj) throws RException {
		gcs.add(beanName,mapperName, postObj);
		return RetMsgHelper.ok();
	}


}
