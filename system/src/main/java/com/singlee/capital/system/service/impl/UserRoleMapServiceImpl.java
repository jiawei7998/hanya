package com.singlee.capital.system.service.impl;

import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.system.mapper.TaUserRoleMapCopyMapper;
import com.singlee.capital.system.mapper.TaUserRoleMapMapper;
import com.singlee.capital.system.model.TaUserRoleMap;
import com.singlee.capital.system.service.UserRoleMapService;
import com.singlee.capital.system.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * 柜员 服务
 * 
 * @author Lyon Chen
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class UserRoleMapServiceImpl implements UserRoleMapService {

//	private static final String cacheName = "com.singlee.capital.system.userRoles";
	@Autowired
	private TaUserRoleMapMapper userRoleMapMapper;
	@Autowired
	private BatchDao batchDao;
	@Autowired
	private UserService userService;
	@Autowired
	private TaUserRoleMapCopyMapper userRoleMapCopyMapper;
	
	@Override
	public List<TaUserRoleMap> getAllUserRole(TaUserRoleMap userRoleMap) {
		List<TaUserRoleMap> list = userRoleMapMapper.listUserRole(userRoleMap);
		return list;
	}
	@Override
	public void saveUserRole(TaUserRoleMap userRoleMap) {
		userRoleMapMapper.deleteUserRoleByUserId(userRoleMap.getUserId());
		String roleId = userRoleMap.getRoleId();
		if (StringUtils.isNotEmpty(roleId)){
			String[] roleIds = roleId.split(",");
			List<TaUserRoleMap> list = new LinkedList<TaUserRoleMap>();
			for (int i = 0; i < roleIds.length; i++) {
				TaUserRoleMap userRole = new TaUserRoleMap();
				userRole.setUserId(userRoleMap.getUserId());
				userRole.setRoleId(roleIds[i]);
				list.add(userRole);
			}
			batchDao.batch("com.singlee.capital.system.mapper.TaUserRoleMapMapper.insert", list);
		}
//		if(userService.isTotalManager(SlSessionHelper.getUserId())) {
//			// 总行管理员直接生效
//			
//		} else {
//			Map<String, Object> map = new HashMap<String, Object>();
//			map.put("userId", userRoleMap.getUserId());
//			map.put("approveStatus", ApproveStatusConstants.APPROVE_ING);
//			userRoleMapCopyMapper.deleteByUserIdAndApproveStatus(map);
//			String roleId = userRoleMap.getRoleId();
//			if (StringUtils.isNotEmpty(roleId)){
//				String currentUserId = SlSessionHelper.getUserId();
//				String[] roleIds = roleId.split(",");
//				for (int i = 0; i < roleIds.length; i++) {
//					TaUserRoleMapCopy userRole = new TaUserRoleMapCopy();
//					userRole.setUserId(userRoleMap.getUserId());
//					userRole.setRoleId(roleIds[i]);
//					userRole.setSubmitUserId(currentUserId);
//					userRole.setApproveStatus(ApproveStatusConstants.APPROVE_ING);
//					userRoleMapCopyMapper.insert(userRole);
//				}
//			}
//		}
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */

}
