package com.singlee.capital.system.dict;

public class ApproveStatusConstants {

	public static final String APPROVE_ING = "1";// 审批中
	public static final String APPROVE_ED = "2";// 已审批
	public static final String MANAGER_OP = "3";// 管理员操作
}
