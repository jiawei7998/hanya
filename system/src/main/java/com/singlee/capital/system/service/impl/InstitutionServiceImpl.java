package com.singlee.capital.system.service.impl;

import com.singlee.capital.common.log.AutoLogMethod;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.common.util.TreeUtil;
import com.singlee.capital.system.dict.ApproveStatusConstants;
import com.singlee.capital.system.mapper.TaUserInstMapper;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionListener;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 机构 服务
 * 
 * @author x230i
 * 
 */
@Service
public class InstitutionServiceImpl implements InstitutionService {

	private static final String cacheName = "com.singlee.capital.system.institutions";

	@Autowired
	private TtInstitutionMapper mapper;

	@Autowired
	private InstitutionListener institutionListener;

	@Autowired
	private TaUserInstMapper taUserInstMapper;

	@Autowired
	private TaUserMapper userMapper;

//	@Cacheable(value = cacheName, key = "'whole'")
	@Override
	public List<TtInstitution> mergedTreeInstitution() {
		return TreeUtil.mergeChildrenList(mapper.getInstitutionAndPrdName(), "-1");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TtInstitution> childrenInstitution(String instId) {
		return (List<TtInstitution>) TreeUtil.getChildrenList(myself().mergedTreeInstitution(), instId);
	}

	@Override
	public List<String> childrenInstitutionIds(String instId) {
		List<String> ret = new ArrayList<String>();
		List<TtInstitution> list = childrenInstitution(instId);
		if (list != null) {
			for (TtInstitution ti : list) {
				ret.add(ti.getId());
				addSubInstitution(ti, ret);
			}
		}
		return ret;
	}

	@Override
	public List<TtInstitution> searchParentInst(String instId) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("instId", instId);
		return mapper.searchParentInst(map);
	}

	private void addSubInstitution(TtInstitution ti, List<String> ret) {
		if (!(ti.getChildren() == null || ti.getChildren().size() == 0)) {
			for (TtInstitution tti : ti.getChildren()) {
				ret.add(tti.getId());
				addSubInstitution(tti, ret);
			}
		}
	}

	@Override
	@AutoLogMethod(value = "增加机构")
	@CacheEvict(value = cacheName, allEntries = true)
	public void addInstitution(TtInstitution inst) {
		// String hasExistHostAcc =
		// accOutSecuService.checkHostAccId(inst.getZzd_accid(),
		// inst.getSqs_accid(), inst.getInst_id());
		// if (hasExistHostAcc != null) {
		// // 已经存在
		// throw new JYException(
		// String.format("中债登托管账户(%s)或者上清所托管账户(%s)，已经在其他的外部证券账户(%s)中存在",
		// inst.getZzd_accid(), inst.getSqs_accid(), hasExistHostAcc));
		// }
		//TtInstitution old = mapper.selectByPrimaryKey(inst);
//		HashMap<String, Object> map = new HashMap<String, Object>();

//		map.put("instId", inst.getInstId());
//		map.put("branchId", inst.getBranchId());
		//根据机构id和branchId查询机构
//		TtInstitution old = mapper.getInstByBranchId(map);
//		if (old != null) {//MsgUtils.getMessage("error.system.0007")
//			JY.raiseRException("机构代码已存在!");
//		}

		//查询最大的instid,然后+1
		String maxInstId = mapper.getMaxInstId();
		String instId = StringUtil.leftPad(maxInstId, 5, '0');
		inst.setInstId(instId);
		if (inst.getPInstId() == null || "".equals(inst.getPInstId())) {
			inst.setpInstId("-1");
		}
		mapper.insert(inst);

		// 新增的后续处理
		institutionListener.addAfter(inst);
	}

	@Override
	@Cacheable(value = cacheName, key = "#id")
	public TtInstitution getInstitutionById(String id) {
		return mapper.getInstById(id);
	}

	@Override
	@CacheEvict(value = cacheName, allEntries = true)
	public TtInstitution getInstByBranchId(HashMap<String, Object> map) {
		return mapper.getInstByBranchId(map);
	}
	@Override
	@CacheEvict(value = cacheName, allEntries = true)
	public void updateInstitution(TtInstitution inst) {
		mapper.updateByPrimaryKey(inst);
		// 修改的后续通知处理
		institutionListener.modAfter(inst);
	}

	@Override
	@AutoLogMethod(value = "删除机构")
	@CacheEvict(value = cacheName, allEntries = true)
	public void deleteInstitution(String id) {
		mapper.deleteByPrimaryKey(id);
	}

	/***
	 * ----以上为对外服务-------------------------------
	 */
	/**
	 * 为了 代理 能进入
	 * 
	 * @return
	 */
	private InstitutionServiceImpl myself() {
		return SpringContextHolder.getBean(InstitutionServiceImpl.class);
	}

	/**
	 * 根据用户机构（包含子机构）
	 * 
	 * @return
	 */
	@Override
	public List<TtInstitution> searchByInstId(HashMap<String, Object> map) {
		TtInstitution institution = new TtInstitution();
		institution.setInstId(String.valueOf(map.get("instId")));
		institution = mapper.selectByPrimaryKey(institution);
		return TreeUtil.mergeChildrenList(mapper.searchChildrenInst(map), institution.getpInstId());
	}

	/**
	 * 查询传入机构的父、子机构
	 */
	@Override
	public List<TtInstitution> searchCanInstitution(HashMap<String, Object> map) {
		return mapper.searchCanInstitution(map);
	}

	@Override
	public List<String> searchCanInstitutionIds(HashMap<String, Object> map) {
		List<String> result = new ArrayList<String>();
		String instId = ParameterUtil.getString(map, "instId", "");
		if (StringUtil.isNullOrEmpty(instId)) {
			instId = SlSessionHelper.getInstitution().getInstId();
		}
		List<TtInstitution> childrenInstList = this.searchCanInstitution(map);
		for (TtInstitution item : childrenInstList) {
			result.add(item.getInstId());
		}
		return result;
	}

	@Override
	public List<String> searchTrueChildrenInstitutionIds(HashMap<String, Object> map) {
		List<String> result = new ArrayList<String>();
		String instId = ParameterUtil.getString(map, "instId", "");
		if (StringUtil.isNullOrEmpty(instId)) {
			instId = SlSessionHelper.getInstitution().getInstId();
		}
		List<TtInstitution> childrenInstList = this.searchCanInstitution(map);
		for (TtInstitution item : childrenInstList) {
			result.add(item.getInstId());
		}
		return result;
	}

	@Override
	public List<TtInstitution> searchChildrenInst(HashMap<String, Object> map) {
		return mapper.searchChildrenInst(map);
	}

	@Override
	public String queryInstId(String instName) {
		return mapper.queryInstId(instName);
	}

	@Override
	public List<TtInstitution> getFtaInstitution(Map<String, Object> map) {
		String instId = ParameterUtil.getString(map, "instId", "");
		List<String> instIdList = childrenInstitutionIds(instId);
		List<TtInstitution> institutions = new ArrayList<TtInstitution>();
		if (!instIdList.isEmpty()) {
			map.put("instIdList", instIdList);
			institutions = mapper.getFtaInstitution(map);
		}
		return institutions;
	}

	@Override
	public List<TtInstitution> getInstListByUser(Map<String, Object> map) {
		String userId = ParameterUtil.getString(map, "userId", null);
		TaUser user = null;
		if (userId != null) {
			user = userMapper.selectByPrimaryKey(userId);
			if (user != null) {
				if (ApproveStatusConstants.APPROVE_ING.equals(user.getApproveStatus())) {
					user = null;
				}
			} else {
				List<TaUser> userList = userMapper.findByEmpId(map);
				if (!userList.isEmpty()) {
					user = userList.get(0);
					if (ApproveStatusConstants.APPROVE_ING.equals(user.getApproveStatus())) {
						user = null;
					}
				}
			}
		}
		List<TtInstitution> instList = new ArrayList<TtInstitution>();
		if (user != null) {
			if (user.getInstId() != null) {
				instList.add(mapper.selectByPrimaryKey(user.getInstId()));
			}
			if (user.getFtaInstId() != null) {
				String[] instIds = StringUtils.split(user.getFtaInstId(), ",");
				for (String instId : instIds) {
					if (null != StringUtils.trimToNull(instId)) {
						instList.add(mapper.selectByPrimaryKey(instId));
					}
				}
			}
		}
		return instList;
	}

	@Override
	public List<TtInstitution> searchFtaInstList(Map<String, Object> map) {
		return mapper.getFtaInstitution(map);
	}

	@Override
	public List<String> searchChildrenInstList(String instId) {
		// 根据机构主键获取机构信息
		TtInstitution ttInstitution = new TtInstitution();
		ttInstitution.setInstId(instId);
		ttInstitution = mapper.selectByPrimaryKey(ttInstitution);

		String inst_type = ttInstitution.getInstType();
		String parent_inst_id = ttInstitution.getPInstId();

		List<String> ttList = new ArrayList<String>();
		HashMap<String, Object> map = new HashMap<String, Object>();

		if (!Arrays.asList("0", "9").contains(inst_type)) {
			// 根据登录人机构ID查询他的子机构
			map.put("instId", instId);
			List<TtInstitution> list = this.searchChildrenInst(map);
			for (TtInstitution tt : list) {
				ttList.add(tt.getInstId());
			}
		} else if ("9".equals(inst_type)) {
			// 9-总行部门，根据登录人机构的父机构ID查询他的子机构
			map.put("instId", parent_inst_id);
			List<TtInstitution> list = this.searchChildrenInst(map);
			for (TtInstitution tt : list) {
				ttList.add(tt.getInstId());
			}
		}

		return ttList;
	}

	@Override
	public List<TtInstitution> getInstByUser(Map<String, Object> map) {
		return mapper.getInstByUser(map);
	}

	/**
	 * 根据用户Id查询机构，一对多
	 */

	@Override
	public List<TtInstitution> getInstitutionByUserId(Map<String, Object> map) {
		return mapper.getInstitutionByUserId(map);
	}

	/**
	 * 根据机构Id查询机构信息
	 */

	@Override
	public TtInstitution getInstById(String instId) {

		return mapper.getInstById(instId);
	}

	/**
	 * 根据银行Id 查询机构
	 */

	@Override
	public List<TtInstitution> searchInstitutionByBranchId(HashMap<String, Object> map) {

		return TreeUtil.mergeChildrenList(mapper.searchInstitutionByBranchId(map), "-1");
	}

	@Override
	public List<TtInstitution> getTtInstitutionByRoleId(HashMap<String, Object> map) {

		return mapper.getTtInstitutionByRoleId(map);
	}

	@Override
	public List<TtInstitution> mergedTreeInstitutions(HashMap<String, Object> map) {
		// System.out.println("----------"+mapper.getInstitutionAndPrdNames(map));
		return TreeUtil.mergeChildrenList(mapper.getInstitutionAndPrdNames(map), "-1");
	}

	@Override
	@AutoLogMethod(value = "机构修改",id="0002")
	@CacheEvict(value = cacheName, allEntries = true)
	public void updateInstitutionAndOld(TtInstitution inst, TtInstitution oldInstitution) {
		mapper.updateByPrimaryKey(inst);
		// 修改的后续通知处理【已废弃】
//		institutionListener.modAfter(inst);
	}

	@Override
	public void updateInstitutionStatus(String instId) {
		mapper.updateInstitutionStatus(instId);
	}

	@Override
	public List<String> searchInstByUserId(HashMap<String, Object> map) {

		return taUserInstMapper.searchInstByUserId(map);
	}

	
}
