package com.singlee.capital.base.pojo;
/**
 * 查询结果显示对象
 * @author kevin_gm
 *
 */
public class ResultListVo {

	private String result_label;//标签
	private String result_value;//名称
	private String result_formatter;//格式化函数
	private String column_width;//列宽
	private String column_align;//显示位置
	private String detail_show;//是否只在明细页面显示

	public String getCollect_type() {
		return collect_type;
	}

	public void setCollect_type(String collect_type) {
		this.collect_type = collect_type;
	}

	private String collect_type;
	
	public String getColumn_width() {
		return column_width;
	}
	public void setColumn_width(String column_width) {
		this.column_width = column_width;
	}
	public String getColumn_align() {
		return column_align;
	}
	public void setColumn_align(String column_align) {
		this.column_align = column_align;
	}
	public String getResult_label() {
		return result_label;
	}
	public void setResult_label(String result_label) {
		this.result_label = result_label;
	}
	public String getResult_value() {
		return result_value;
	}
	public void setResult_value(String result_value) {
		this.result_value = result_value;
	}
	public String getResult_formatter() {
		return result_formatter;
	}
	public void setResult_formatter(String result_formatter) {
		this.result_formatter = result_formatter;
	}
	public String getDetail_show() {
		return detail_show;
	}
	public void setDetail_show(String detailShow) {
		detail_show = detailShow;
	}
	
}
