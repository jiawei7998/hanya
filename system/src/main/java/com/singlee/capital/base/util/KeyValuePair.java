package com.singlee.capital.base.util;

/**
 * k v 对
 * @author LyonChen
 */
public class KeyValuePair<K, V> {

	private final K key;
	private final V value;

	public KeyValuePair(final K key, final V value) {
		this.key = key;
		this.value = value;
	}

	public K key() {
		return key;
	}

	public V value() {
		return value;
	}

}
