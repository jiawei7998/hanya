package com.singlee.capital.base.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.base.model.TaCustomReport;
import com.singlee.capital.base.pojo.CustomReportListVo;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface TaCustomReportMapper extends Mapper<TaCustomReport> {
	
	/**
	 * ******************************其他必须手写的接口************************************
	 */
	
	/**
	 * 自定义报表
	 * @param map
	 * is_active ： 启用状态
	 * title 标题（模糊查询）
	 * @return
	 */
	List<CustomReportListVo> selectReport(HashMap<String, Object> map);
	
	/**
	 * 根据表名查询列属性(此方法的结果集需用于数组间比较，故仅查询COLUMN_NAME字段)
	 * @param map
	 * @return
	 */
	List<String> selectTableColums(Map<String, Object> map);
	
	/**
	 * 分页查询自定义报表列表
	 * @param role - 角色对象
	 * @param rb - rowBounds
	 * @return Page<CustomReportListVo>
	 * @author lihuabing
	 * @date 2016-7-21
	 */
	Page<CustomReportListVo> pageCustomReports(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 根据编号查询自定义报表
	 * @param custId
	 * @return
	 */
	TaCustomReport getReport(String custId);
	
	/**
	 * 自定义报表查询
	 * @param map
	 * @return
	 */
	Page<HashMap<String, Object>> searchReport(Map<String,Object> sql, RowBounds rb);
	
	/**
	 * 自定义报表查询
	 * @param map
	 * @return
	 */
	List<HashMap<String, Object>> searchReport(Map<String,Object> sql);
	
	/**
	 * 删除报表
	 * @param map
	 */
	void delReport(HashMap<String,Object> map);
	
	/**
	 * 增加报表 
	 */
	void insertReport(TaCustomReport r);
	  
}