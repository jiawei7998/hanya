package com.singlee.capital.base.pojo;

import com.singlee.capital.base.model.TaCustomReport;

/**
 * 自定义报表查询列表
 * @author kevin_gm
 *
 */
public class CustomReportListVo  extends TaCustomReport {
	/**
	 * 
	 */
	private static final long serialVersionUID = -69015915880973151L;
	private String operator_name;
	public String getOperator_name() {
		return operator_name;
	}

	public void setOperator_name(String operator_name) {
		this.operator_name = operator_name;
	}
	
}
