package com.singlee.capital.base.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;


/**
 * 自定义报表对象
 * 
 * @author lihuabing
 * 
 */

@Entity
@Table(name = "TA_CUSTOM_REPORT")
public class TaCustomReport implements Serializable {

	private static final long serialVersionUID = -1107628330209413905L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TA_BASE.NEXTVAL FROM DUAL")
	private String custid;//报表编号
	private String reportclob;//自定义报表json对象
	private String reporttype;//报表类型
	private String title;//报表标题
	private String branch_id;
	private String is_active;//启用状态
	private String operator_id;//操作员编号
	private String operate_time; //操作时间
	private byte[] template_file; //模板文件
	private String module_id;//节点ID
	private String module_url;//菜单地址
	private String module_pid;//父节点ID
	private String module_name;//菜单名
	@Transient
	private String add_time;//创建页面的创建时间
	@Transient
	private String types;//创建页面的类型

	public TaCustomReport() {
	}

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getReportclob() {
		return reportclob;
	}

	public void setReportclob(String reportclob) {
		this.reportclob = reportclob;
	}

	public String getReporttype() {
		return reporttype;
	}

	public void setReporttype(String reporttype) {
		this.reporttype = reporttype;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIs_active() {
		return is_active;
	}

	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}

	public String getOperator_id() {
		return operator_id;
	}

	public void setOperator_id(String operator_id) {
		this.operator_id = operator_id;
	}

	public String getOperate_time() {
		return operate_time;
	}

	public void setOperate_time(String operate_time) {
		this.operate_time = operate_time;
	}

	public byte[] getTemplate_file() {
		return template_file;
	}

	public void setTemplate_file(byte[] template_file) {
		this.template_file = template_file;
	}

	public String getModule_id() {
		return module_id;
	}

	public void setModule_id(String module_id) {
		this.module_id = module_id;
	}

	public String getModule_url() {
		return module_url;
	}

	public void setModule_url(String module_url) {
		this.module_url = module_url;
	}

	public String getModule_pid() {
		return module_pid;
	}

	public void setModule_pid(String module_pid) {
		this.module_pid = module_pid;
	}

	public String getModule_name() {
		return module_name;
	}

	public void setModule_name(String module_name) {
		this.module_name = module_name;
	}

	public String getAdd_time() {
		return add_time;
	}

	public void setAdd_time(String add_time) {
		this.add_time = add_time;
	}

	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public String getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(String branch_id) {
		this.branch_id = branch_id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TaCustomReport [custid=");
		builder.append(custid);
		builder.append(", reportclob=");
		builder.append(reportclob);
		builder.append(", reporttype=");
		builder.append(reporttype);
		builder.append(", title=");
		builder.append(title);
		builder.append(", branch_id=");
		builder.append(branch_id);
		builder.append(", is_active=");
		builder.append(is_active);
		builder.append(", operator_id=");
		builder.append(operator_id);
		builder.append(", operate_time=");
		builder.append(operate_time);
		builder.append(", template_file=");
		builder.append(Arrays.toString(template_file));
		builder.append(", module_id=");
		builder.append(module_id);
		builder.append(", module_url=");
		builder.append(module_url);
		builder.append(", module_pid=");
		builder.append(module_pid);
		builder.append(", module_name=");
		builder.append(module_name);
		builder.append(", add_time=");
		builder.append(add_time);
		builder.append(", types=");
		builder.append(types);
		builder.append("]");
		return builder.toString();
	}

}
