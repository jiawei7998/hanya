package com.singlee.capital.base.controller;


import com.alibaba.fastjson.TypeReference;
import com.github.pagehelper.Page;
import com.singlee.capital.base.pojo.CustomReportListVo;
import com.singlee.capital.base.pojo.ReportClobVo;
import com.singlee.capital.base.pojo.ResultListVo;
import com.singlee.capital.base.service.CustomReportService;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.spring.webmvc.AttachFileHandlerMulipartFile;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.HttpUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.controller.CommonController;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

/**
 * 自定义报表控制层
 *
 * @author kevin_gm
 */
@Controller
@RequestMapping(value = "/ReportManageController")
public class ReportManageController extends CommonController {

    /**
     * 自定报表业务层
     **/
    @Autowired
    private CustomReportService customReportService;

    /**
     * 根据用户输入的SQL语句生成查询控件及grid网格控件
     *
     * @param reportClobVo
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/autoCreateControls")
    public HashMap<String, Object> autoCreateControls(@RequestBody ReportClobVo reportClobVo) throws IOException {
        return customReportService.autoCreateControls(reportClobVo);
    }

    /**
     * 查询自定义报表
     *
     * @param pageData
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/searchReportList")
    public RetMsg<PageInfo<CustomReportListVo>> searchReportList(@RequestBody Map<String, Object> pageData) throws IOException {
        Page<CustomReportListVo> retList = customReportService.pageCustomReports(pageData);
        return RetMsgHelper.ok(retList);
    }

    /**
     * 保存自定义报表
     *
     * @param reportClobVo
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/saveReportInfo")
    public RetMsg<Serializable> saveReportInfo(@RequestBody ReportClobVo reportClobVo) throws IOException {
        byte[] templateFile = reportClobVo.getTemplate_file();
        reportClobVo.setTemplate_file(Base64.getDecoder().decode(templateFile));
        //获取当前登录柜员、机构
        customReportService.saveReport(reportClobVo, SlSessionHelper.getUserId(), SlSessionHelper.getInstitutionId());
        return RetMsgHelper.ok();
    }

    /**
     * 查询自定义报表
     *
     * @param map 参数 ： pageNumber 页码 pageSize 记录
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/selectReportInfo")
    public ReportClobVo selectReportInfo(@RequestBody Map<String, Object> map) throws IOException {
        String custid = ParameterUtil.getString(map, "custid", "");
        ReportClobVo reportClobVo = customReportService.selectCustomReportInfo(custid, "");
        return reportClobVo;
    }

    /**
     * 报表显示search
     *
     * @param map 参数 ： pageNumber 页码 pageSize 记录
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/reportSearch")
    public RetMsg<PageInfo<HashMap<String, Object>>> reportSearch(@RequestBody HashMap<String, Object> map) throws IOException {
        Page<HashMap<String, Object>> retList = customReportService.searchReport(map);
        return RetMsgHelper.ok(retList);
    }

//    /**
//     * EXCEL导出
//     *
//     * @param request
//     * @param response
//     * @throws IOException
//     * @throws JRException
//     * @throws SQLException
//     * @throws ParsePropertyException
//     * @throws InvalidFormatException
//     */
//    @SuppressWarnings({"rawtypes", "unchecked", "unused"})
//    @RequestMapping(value = "/excel")
//    public void excel(HttpServletRequest request, HttpServletResponse response) {
//        HashMap map = requestParamToMap(request);
//        // 读取查询条件返回结果
//        List<HashMap<String, Object>> list = customReportService.searchReportExport(map);
//        // 读取报表对象
//        String custid = ParameterUtil.getString(map, "custid", "");
//        ReportClobVo reportClobVo = customReportService.selectCustomReportInfo(custid, "");
//
//        String ua = request.getHeader("User-Agent");
//        HashMap<String, Object> excelMap = reportClobVo.getExcelModule().get(0);
//        String file_src = ParameterUtil.getString(excelMap, "file_src", "");
//        String filename = ParameterUtil.getString(excelMap, "file_name", "");
//        String encode_filename = "";
//        try {
//            encode_filename = URLEncoder.encode(filename, "utf-8");
//        } catch (UnsupportedEncodingException e) {
//            throw new RException(e.getMessage());
//        }
//        if (ua != null && ua.indexOf("Firefox/") >= 0) {
//            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
//        } else {
//            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
//        }
//        response.setHeader("Connection", "close");
//        Map beans = new HashMap();
//        beans.put("list", list);
//        beans.put("info", reportClobVo);
//        // String pathString = request.getRealPath("/");
//        XLSTransformer transformer = new XLSTransformer();
//
//        // FileInputStream fileInputStream = new FileInputStream(pathString
//        // + "/excel/" + file_src);
//        ByteArrayInputStream byteInputStream = new ByteArrayInputStream(reportClobVo.getTemplate_file());
//        Workbook wb;
//        try {
//            wb = transformer.transformXLS(byteInputStream, beans);
//            HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
//            OutputStream out = response.getOutputStream();
//            wb.write(out);
//            out.flush();
//            // fileInputStream.close();
//            byteInputStream.close();
//        } catch (ParsePropertyException e) {
//            throw new RException(e.getMessage());
//        } catch (InvalidFormatException e) {
//            throw new RException(e.getMessage());
//        } catch (IOException e) {
//            throw new RException(e.getMessage());
//        }
//    }

    /**
     * 读取request中的参数,转换为map对象
     *
     * @param request
     * @return
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private HashMap<String, Object> requestParamToMap(HttpServletRequest request) {
        HashMap<String, Object> parameters = new HashMap();
        Enumeration<String> Enumeration = request.getParameterNames();
        while (Enumeration.hasMoreElements()) {
            String paramName = (String) Enumeration.nextElement();
            String paramValue = request.getParameter(paramName);
            // 形成键值对应的map
            if (!StringUtils.isEmpty(paramValue)) {
                parameters.put(paramName, paramValue);
            }
        }
        return parameters;
    }

    /**
     * EXCEL导出直接导出grid中数据 (此方法导出的文件名不能是xlsx的格式)
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/excelGrid")
    public void excelGrid(HttpServletRequest request, HttpServletResponse response) {
        HashMap<String, Object> map = requestParamToMap(request);
        // String json = request.getBody();
        // HashMap map = JacksonUtil.readJson2Entity(json, HashMap.class);
        // 读取报表对象
        String custid = ParameterUtil.getString(map, "custid", "");
        JY.require(!StringUtils.isEmpty(custid), "传入的模板编号(custid)不能为空.");

        ReportClobVo reportClobVo = customReportService.selectCustomReportInfo(custid, "");

        String ua = request.getHeader("User-Agent");
        String filename = reportClobVo.getTitle() + ".xls";
        String encode_filename = "";
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            //e1.printStackTrace();
            throw new RException(e1.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, reportClobVo.getTitle());
        HSSFRow row = sheet.createRow((short) 0);

        HSSFCell cell;
        // 写入各个字段的名称
        List<ResultListVo> result_list = reportClobVo.getResult_list();
        for (int i = 0; i < result_list.size(); i++) {
            ResultListVo resultListVo = result_list.get(i);
            cell = row.createCell(i);
            cell.setCellType(CellType.STRING);
            cell.setCellValue(resultListVo.getResult_label());
        }

        // 读取查询条件返回结果
        List<HashMap<String, Object>> list = customReportService.searchReportExport(map);

        // 写入各条记录，每条记录对应Excel中的一行
        for (int i = 0; i < list.size(); i++) {
            HashMap<String, Object> listMap = list.get(i);
            row = sheet.createRow(i + 1);
            for (int j = 0; j < result_list.size(); j++) {
                ResultListVo resultListVo = result_list.get(j);
                cell = row.createCell(j);
                cell.setCellType(CellType.STRING);
                cell.setCellValue(ParameterUtil.getString(listMap, resultListVo.getResult_value(), ""));
            }
        }
        try {
            OutputStream ouputStream = response.getOutputStream();
            workbook.write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (IOException e) {
            //e.printStackTrace();
            throw new RException(e.getMessage());
        }
    }

    /**
     * 文件上传
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public void upload(MultipartHttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        StringBuilder builder = new StringBuilder();
        PrintWriter out = null;
        // String pathName = request.getSession().getServletContext()
        // .getRealPath("/excel/");
        try {
            // File fileDir = new File(pathName);
            // if (!fileDir.exists()) {
            // fileDir.mkdirs();
            // }
            // 1.文件解析
            List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
            HashMap<String, Object> retMap = new HashMap<String, Object>();
            List<HashMap<String, Object>> mapList = new ArrayList<HashMap<String, Object>>();
            for (UploadedFile uploadedFile : uploadedFileList) {
                if (!"xls".equals(uploadedFile.getType()) && !"xlsx".equals(uploadedFile.getType())) {
                    JY.raise("上传文件类型错误,仅允许上传xls及xlsx格式文件.");
                }
                String oriName = uploadedFile.getName() + "." + uploadedFile.getType();
                String newFileName = uploadedFile.getName()
                        // + DateUtil.getCurrentCompactDateTimeAsString() + "."
                        + DateUtil.getCurrentTimeAsString("yyyyMMddHHmmssSSS") + "." + uploadedFile.getType();
                uploadedFile.setFullname(newFileName);
                HashMap<String, Object> map = new HashMap<String, Object>();
                // map.put("file_name", uploadedFile.getBeforeName());
                map.put("file_name", oriName);
                map.put("file_src", uploadedFile.getFullname());
                map.put("edit_types", "add");
                // map.put("file_path", pathName);
                mapList.add(map);
            }

            // 3.报文返回
            retMap.put("file_list", mapList);
            retMap.put("status", "succ");
            if (uploadedFileList != null && uploadedFileList.size() > 0) {
                retMap.put("content", Base64.getEncoder().encode(uploadedFileList.get(0).getBytes()));
            }
            builder.append(FastJsonUtil.toJSONString(retMap));
        } catch (Exception e) {
            builder.append("{\"status\":\"error\",\"msg\":\"" + e.toString() + "\"}");
        } finally {
            try {
                out = response.getWriter();
                out.print(builder.toString());
                out.close();
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }

    }

    /**
     * 文件下载
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/filedown")
    public void filedown(HttpServletRequest request, HttpServletResponse response) {
        HashMap<String,Object> map = requestParamToMap(request);
        String file_src = ParameterUtil.getString(map, "file_src", "");
        String file_name = ParameterUtil.getString(map, "file_name", "");
        String before_name = file_name;
        String report_id = ParameterUtil.getString(map, "report_id", "");
        String add_time = ParameterUtil.getString(map, "add_time", "");
        String types = ParameterUtil.getString(map, "types", "");
        try {
            file_src = URLDecoder.decode(file_src, "UTF-8");
            file_name = URLDecoder.decode(file_name, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            //e1.printStackTrace();
        }
        StringBuilder builder = new StringBuilder();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = null;

        // 参数验证
        if (StringUtils.isEmpty(file_src)) {
            builder.append("{\"status\":\"error\",\"msg\":\"输入参数[file_name]不存在或传入空值.\"}");
            closeResponse(response, out, builder);
            return;
        }
        // String pathName = "";
        // if("add".equals(edit_types)){
        // pathName =
        // request.getSession().getServletContext().getRealPath("/cacheCatalog/");
        // }else{
        // pathName = request.getSession().getServletContext()
        // .getRealPath("/excel/");
        // }
        try {
            // UploadedFile uploadedFile = FileManage.getUploadedFile(pathName,
            // file_name);
            // File file = new File(pathName, file_name);
            // 文件验证
            // if (!file.isFile()) {
            // builder.append("{\"status\":\"error\",\"msg\":\"文件["
            // + file_name + "]不存在.\"}");
            // closeResponse(response, out, builder);
            // return;
            // }
            UploadedFile uploadedFile = null;
//			// 先在缓存中查找
//			if (!StringUtils.equals(types, "detail")) {
//				List<UploadedFile> templateList = CustomReportServiceImpl.templateMap.get(add_time);
//				if (templateList != null && templateList.size() == 1) {
//					uploadedFile = templateList.get(0);
//				}
//			}
            // 缓存找不到，从数据库获取模板
            if (uploadedFile == null && !StringUtils.equals(types, "add")) {
                uploadedFile = customReportService.getUploadedFile(report_id, file_src);
            }
            if (uploadedFile == null) {
                builder.append("{\"status\":\"error\",\"msg\":\"文件[" + file_name + "]不存在.\"}");
                closeResponse(response, out, builder);
                return;
            }
            String ua = request.getHeader("User-Agent");
            // uploadedFile = new UploadedFile();
            // uploadedFile.setFullname(file_src);
            if (ua != null && ua.indexOf("Firefox/") >= 0) {
                response.setHeader("Content-Disposition", "attachment; filename*=" + before_name);
                // + URLEncoder.encode(
                // uploadedFile.getBeforeName(), "UTF8"));
            } else {
                response.setHeader("Content-Disposition", "attachment; filename=" + before_name);
                // + URLEncoder.encode(
                // uploadedFile.getBeforeName(), "UTF8"));
            }
            // 清空response
            OutputStream toClient = response.getOutputStream();
            BufferedInputStream fis = new BufferedInputStream(new ByteArrayInputStream(uploadedFile.getBytes()));
            IOUtils.copy(fis, toClient);
            toClient.flush();
            toClient.close();
        } catch (Exception e) {
            builder.append("{\"status\":\"error\",\"msg\":\"文件[" + file_name + "]下载异常.错误信息:" + e.toString() + "\"}");
            closeResponse(response, out, builder);
        }
    }

    // 文件下载关闭流统一调用
    private void closeResponse(HttpServletResponse response, PrintWriter out, StringBuilder builder) {
        try {
            out = response.getWriter();
            out.print(builder.toString());
            out.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    /**
     * @param request 启用状态 标题（模糊查询）
     * @param
     * @return
     * @deprecated
     */
    @ResponseBody
    @RequestMapping(value = "/exportSQL")
    public HashMap<String, Object> exportSQL(HttpEntity<String> request) throws IOException {
        return customReportService.exportSQL();
    }

    @RequestMapping(value = "/exportSQLText", produces = "application/x-msdownload")
    public void exportSQLText(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HashMap<String,Object> map = requestParamToMap(request);
        response.setContentType("application/x-msdownload");
        String fileName = "";
        if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
            fileName = new String("自定义报表数据.json".getBytes("UTF-8"), "ISO8859-1");// firefox浏览器
        } else {
            fileName = URLEncoder.encode("自定义报表数据.json", "UTF-8");// 其他浏览器包括IE浏览器和google浏览器
        }
        response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
        String s = customReportService.exportSQLString(map);
        InputStream input = new ByteArrayInputStream(s.getBytes());
        IOUtils.copy(input, response.getOutputStream());
    }

    @ResponseBody
    @RequestMapping(value = "/importSQLText", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String importSQLText(MultipartHttpServletRequest request, HttpServletResponse response) throws IOException {
        StringBuffer sb = new StringBuffer();
        try {
            List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
            for (UploadedFile uploadedFile : uploadedFileList) {
                JY.ensure("json".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传json格式文件.");
                String s = new String(uploadedFile.getBytes());
                customReportService.importSQLString(s);
                sb.append("处理完成！");
            }
        } catch (RException e) {
            JY.error(e);
            sb.append(e.getMessage());
        } catch (IOException e) {
            JY.error(e);
            sb.append(e.getMessage());
        } catch (Exception e) {
            JY.error(e);
            sb.append(e.getMessage());
        }
        return sb.toString();
    }

    /**
     * 删除模板缓存
     *
     * @param request
     * @throws IOException

     @ResponseBody
     @RequestMapping(value = "/delReportCache")
     public void delReportCache(@RequestBody HashMap<String, Object> map) throws IOException {
     customReportService.delReportCache(map);
     } */

    /**
     * 报表明细查询
     *
     * @param map
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/getReportInfo")
    public HashMap<String, Object> getReportInfo(@RequestBody HashMap<String, Object> map) throws IOException {
        return customReportService.getReportInfo(map);
    }

    /**
     * 报表模板自动生成
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/autoCreateTemplate")
    public void autoCreateTemplate(HttpServletRequest request, HttpServletResponse response) {
        HashMap<String,Object> map = requestParamToMap(request);
        List<ResultListVo> resultList = FastJsonUtil.parseObject(ParameterUtil.getString(map, "result_list", ""), new TypeReference<List<ResultListVo>>() {
        });
        JY.require(resultList != null && resultList.size() > 0, "无报表显示字段!");
        String title = ParameterUtil.getString(map, "title", "自定义报表");
        String ua = request.getHeader("User-Agent");
        String filename = title + ".xls";
        String encode_filename;
        try {
            encode_filename = URLEncoder.encode(filename, "utf-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            throw new RException(e.getMessage());
        }
        response.setContentType("application/vnd.ms-excel");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*=" + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename=" + encode_filename);
        }
        response.setHeader("Connection", "close");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, title);
        // 表头
        HSSFRow row = sheet.createRow(0);
        HSSFCell titleCell = row.createCell(0);
        titleCell.setCellType(CellType.STRING);
        HSSFCellStyle titleStyle = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 14);
        titleStyle.setFont(font);
        titleCell.setCellStyle(titleStyle);
        titleCell.setCellValue("标题：");
        titleCell = row.createCell(1);
        titleCell.setCellStyle(titleStyle);
        titleCell.setCellValue("${info.title}");
        row = sheet.createRow(1);
        // 各个字段的名称
        row = sheet.createRow(2);
        HSSFCell cell;
        HSSFCellStyle thStyle = workbook.createCellStyle(); // 样式
        thStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        thStyle.setAlignment(HorizontalAlignment.CENTER);
        HSSFFont thFont = workbook.createFont();
        thFont.setFontName("宋体");
        thFont.setFontHeightInPoints((short) 11);
        thStyle.setFont(thFont);
        thStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.GREY_25_PERCENT.getIndex());
        for (int i = 0; i < resultList.size(); i++) {
            ResultListVo resultListVo = resultList.get(i);
            cell = row.createCell(i);
            cell.setCellType(CellType.STRING);
            cell.setCellStyle(thStyle);
            sheet.setColumnWidth(i, 36 * (StringUtils.isEmpty(resultListVo.getColumn_width()) ? 100 : Integer.parseInt(resultListVo.getColumn_width())));
            cell.setCellValue(resultListVo.getResult_label());
        }
        // 列表
        row = sheet.createRow(3);
        cell = row.createCell(0);
        cell.setCellValue("<jx:forEach items=\"${list}\" var=\"list\">");
        row = sheet.createRow(4);
        for (int i = 0; i < resultList.size(); i++) {
            ResultListVo resultListVo = resultList.get(i);
            cell = row.createCell(i);
            cell.setCellType(CellType.STRING);
            HSSFCellStyle style = workbook.createCellStyle(); // 样式
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            if (StringUtils.equals(resultListVo.getColumn_align(), ExternalDictConstants.ColumnAlign_left)) {
                style.setAlignment(HorizontalAlignment.LEFT);
            } else if (StringUtils.equals(resultListVo.getColumn_align(), ExternalDictConstants.ColumnAlign_center)) {
                style.setAlignment(HorizontalAlignment.CENTER);
            } else {
                style.setAlignment(HorizontalAlignment.RIGHT);
            }
            cell.setCellStyle(style);
            cell.setCellValue("${list." + resultListVo.getResult_value() + "}");
        }
        row = sheet.createRow(5);
        cell = row.createCell(0);
        cell.setCellValue("</jx:forEach>");
        OutputStream ouputStream;
        try {
            ouputStream = response.getOutputStream();
            workbook.write(ouputStream);
            ouputStream.flush();
            ouputStream.close();
        } catch (IOException e) {
            //e.printStackTrace();
            throw new RException(e.getMessage());
        }
    }

    /**
     * 删除报表
     *
     * @param map
     * @return
     * @throws IOException
     */
    @ResponseBody
    @RequestMapping(value = "/delReport")
    public RetMsg<Serializable> delReport(@RequestBody HashMap<String, Object> map) throws IOException {
        customReportService.delReport(map);
        return RetMsgHelper.ok();
    }

    /**
     * @param request
     * @param response
     */

    @ResponseBody
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public RetMsg<List<Pair<String, String>>> uploadFile(MultipartHttpServletRequest request,
                                                         HttpServletResponse response) {
        final String fieldName = HttpUtil.getParameterString(request, "fieldName", null);
        return super.uploadFiles(request, response, new AttachFileHandlerMulipartFile() {

            @Override
            protected void checkFileName(String fileName) {
                JY.info(fieldName);
            }

            @Override
            protected String attachment(String fileName, String contentFile,
                                        byte[] bytes) {
                return new String(Base64.getEncoder().encode(bytes));
            }
        });
    }
}
