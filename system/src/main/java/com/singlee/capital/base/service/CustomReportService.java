package com.singlee.capital.base.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.Page;
import com.singlee.capital.base.pojo.CustomReportListVo;
import com.singlee.capital.base.pojo.ReportClobVo;
import com.singlee.capital.common.pojo.UploadedFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自定义报表
 * @author kevin_gm
 *
 */
public interface CustomReportService {
	/**
	 * 自动生成查询控件及grid的数组
	 * 
	 * @param map
	 * @return
	 */
	HashMap<String, Object> autoCreateControls(ReportClobVo reportClobVo);
	/**
	 * 自定义报表
	 * @param is_active 启用状态
	 * @param title		标题（模糊查询）
	 * @return
	 */
	Page<CustomReportListVo> searchReportList(String is_active,String title,int pageNum,int pageSize);
//	
	/**
	 * 保存自定报表对象
	 * @param taCustomReport
	 * @return 
	 */
	void saveReport(ReportClobVo reportClobVo, String userId, String instId);

	/**
	 * 根据
	 * @param custid 	自定报表编号
	 * @param is_active 启用状态（允许为空）
	 * @return 
	 * @throws IOException 
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	ReportClobVo selectCustomReportInfo(String custid,String is_active);

	/**
	 * 自定报表查询功能
	 * @param map
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	Page<HashMap<String, Object>> searchReport(HashMap<String, Object> map);
	/**
	 * 自定报表查询导出
	 * @param map
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	List<HashMap<String, Object>> searchReportExport(HashMap<String, Object> map);

	/**
	 * 导出SQL语句
	 * @param is_active
	 *            启用状态
	 * @param title
	 *            标题（模糊查询）
	 * @return
	 */
	HashMap<String, Object> exportSQL();
	/**
	 * 将自定义报表的table转换成字符串导出
	 * @param post
	 * @return
	 */
	String exportSQLString(HashMap<String,Object> post);
	/**
	 * 将自定义报表的table转换成字符串导入
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	void importSQLString(String strSQL);
	/**
	 * 删除模板缓存
	 * @param map
	 */
	//void delReportCache(HashMap<String,Object> map);
	/**
	 * 获取上传文件
	 * @param custId
	 * @param fileSrc
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	UploadedFile getUploadedFile(String custId, String fileSrc);
	/**
	 * 报表明细查询
	 * @param map
	 * @return
	 */
	HashMap<String, Object> getReportInfo(HashMap<String, Object> map);
	/**
	 * 删除报表
	 * @param map
	 */
	void delReport(HashMap<String,Object> map);
	
	/**
	 * 分页查询自定义报表
	 * @param pageData
	 * @return
	 */
	Page<CustomReportListVo> pageCustomReports(Map<String, Object> pageData);
}
