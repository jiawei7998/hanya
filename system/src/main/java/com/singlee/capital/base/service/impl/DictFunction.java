package com.singlee.capital.base.service.impl;

import com.singlee.capital.base.pojo.ResultListVo;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("dictFunction")
public class DictFunction implements IFunction{

	
	/**
	 * 字典项文本转换
	 * @param resultListVo
	 * @param map
	 * @param param
	 * @return
	 */
	@Override
	public String invoke(ResultListVo resultListVo, Map<String,Object> map,String param){
		String dictId = param.split(",")[0];
		String valueName = param.split(",")[1];
		String value = ParameterUtil.getString(map, valueName, "");
		if(StringUtil.isNullOrEmpty(value)) { return "";}
		return MsgUtils.getDictItemText(dictId, value);
	}
}
