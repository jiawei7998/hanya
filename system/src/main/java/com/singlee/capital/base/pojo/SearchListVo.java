package com.singlee.capital.base.pojo;
/**
 * 自定义查询条件对象数组
 * @author kevin_gm
 *
 */
public class SearchListVo {
	private String search_label;//标签
	private String search_value;//控件名称
	private String search_type;//控件类型
	private String search_dict;//字典项
	private String search_default;//默认值
	private String group_value;//关联组编号
	private String folder;//字典子项
	private String path;//后台路径
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getGroup_value() {
		return group_value;
	}
	public void setGroup_value(String group_value) {
		this.group_value = group_value;
	}
	public String getSearch_label() {
		return search_label;
	}
	public void setSearch_label(String search_label) {
		this.search_label = search_label;
	}
	public String getSearch_value() {
		return search_value;
	}
	public void setSearch_value(String search_value) {
		this.search_value = search_value;
	}
	public String getSearch_type() {
		return search_type;
	}
	public void setSearch_type(String search_type) {
		this.search_type = search_type;
	}
	public String getSearch_dict() {
		return search_dict;
	}
	public void setSearch_dict(String search_dict) {
		this.search_dict = search_dict;
	}
	public String getSearch_default() {
		return search_default;
	}
	public void setSearch_default(String search_default) {
		this.search_default = search_default;
	}
	
}
