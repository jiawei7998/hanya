package com.singlee.capital.base.pojo;

import com.singlee.capital.base.model.TaCustomReport;

import java.util.HashMap;
import java.util.List;


/**
 * 自定报表对象
 * @author kevin_gm
 *
 */
public class ReportClobVo extends TaCustomReport {
	private static final long serialVersionUID = 4723068231901429775L;
	private String jasperPath;//jasper报表模板路径
	private String execSQL;//执行sql
	private List<HashMap<String, Object>> excelModule;//excel导出模板
	private String dialogUrl;//弹出路径
	private String dialogModule;//明细页面模板
	private String dialogSQL;//明细页面模板数据源
	private double dialogWidth;//弹出页面宽度
	private double dialogHight;//弹出页面高度
	private List<ResultListVo> result_list;//查询结果对象数组
	private List<SearchListVo> search_list;//查询条件对象数组
	private List<String> sumFieldList; // 合计字段
	private List<String> avgFieldList; // 平均字段 
	
	public String getDialogModule() {
		return dialogModule;
	}
	public void setDialogModule(String dialogModule) {
		this.dialogModule = dialogModule;
	}
	public String getDialogSQL() {
		return dialogSQL;
	}
	public void setDialogSQL(String dialogSQL) {
		this.dialogSQL = dialogSQL;
	}
	public double getDialogWidth() {
		return dialogWidth;
	}
	public void setDialogWidth(double dialogWidth) {
		this.dialogWidth = dialogWidth;
	}
	public double getDialogHight() {
		return dialogHight;
	}
	public void setDialogHight(double dialogHight) {
		this.dialogHight = dialogHight;
	}
	public String getJasperPath() {
		return jasperPath;
	}
	public void setJasperPath(String jasperPath) {
		this.jasperPath = jasperPath;
	}
	public String getExecSQL() {
		return execSQL;
	}
	public void setExecSQL(String execSQL) {
		this.execSQL = execSQL;
	}
	public String getDialogUrl() {
		return dialogUrl;
	}
	public void setDialogUrl(String dialogUrl) {
		this.dialogUrl = dialogUrl;
	}
	public List<ResultListVo> getResult_list() {
		return result_list;
	}
	public void setResult_list(List<ResultListVo> result_list) {
		this.result_list = result_list;
	}
	public List<SearchListVo> getSearch_list() {
		return search_list;
	}
	public void setSearch_list(List<SearchListVo> search_list) {
		this.search_list = search_list;
	}
	public List<HashMap<String, Object>> getExcelModule() {
		return excelModule;
	}
	public void setExcelModule(List<HashMap<String, Object>> excelModule) {
		this.excelModule = excelModule;
	}
	public List<String> getSumFieldList() {
		return sumFieldList;
	}
	public void setSumFieldList(List<String> sumFieldList) {
		this.sumFieldList = sumFieldList;
	}
	public List<String> getAvgFieldList() {
		return avgFieldList;
	}
	public void setAvgFieldList(List<String> avgFieldList) {
		this.avgFieldList = avgFieldList;
	}
	
	
}
