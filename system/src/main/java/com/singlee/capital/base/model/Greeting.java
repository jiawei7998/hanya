package com.singlee.capital.base.model;

public class Greeting {
    private String content;
    /**
     * 为“” 则不进行跳转，如果设置请考虑在index.jsp页面进行跳转
     */
    private String call;

    public Greeting(String content, String call) {
        this.content = content;
        this.call = call;
    }

    public String getContent() {
        return content;
    }

    public String getCall() {
        return call;
    }
}
