package com.singlee.capital.base.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.Page;
import com.singlee.capital.base.mapper.TaCustomReportMapper;
import com.singlee.capital.base.model.TaCustomReport;
import com.singlee.capital.base.pojo.CustomReportListVo;
import com.singlee.capital.base.pojo.ReportClobVo;
import com.singlee.capital.base.pojo.ResultListVo;
import com.singlee.capital.base.pojo.SearchListVo;
import com.singlee.capital.base.service.CustomReportService;
import com.singlee.capital.base.util.FreeMarkerStringGenerator;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

/**
 * 自定义报表
 * 
 * @author kevin_gm
 * 
 */
@Service("customReportService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CustomReportServiceImpl implements CustomReportService {

	/** 自定报表dao **/
	@Autowired
	private TaCustomReportMapper customReportMapper;
	@Autowired
	private DayendDateService dds;


	/**
	 * 自动生成查询控件及grid的数组
	 * 
	 * @param map
	 * @return
	 */
	@Override
	public HashMap<String, Object> autoCreateControls(ReportClobVo reportClobVo) {
		HashMap<String, Object> retMap = new HashMap<String, Object>();
		String execSQL = reportClobVo.getExecSQL();
		JY.require(!StringUtils.isEmpty(execSQL), "输入参数[execSQL]不允许为空.");
		//1.将SQL语句转换为大写
		retMap.put("execSQL", execSQL);
		
		//2.1将SQL语句中的所有参数提取出来
		List<SearchListVo> searchList = reportClobVo.getSearch_list() == null?new ArrayList<SearchListVo>():reportClobVo.getSearch_list();
		List<String> paramList = StringUtil.regularSplit$(execSQL);
		//2.2根据传入的查询条件数组匹配数据，当主键存在时，使用原数组的，主键不存在时使用新的数组
		Iterator<SearchListVo> iter = searchList.iterator();
		while(iter.hasNext()){
			SearchListVo searchListInfo = iter.next();
			if(paramList.contains(searchListInfo.getSearch_value())){
				paramList.remove(searchListInfo.getSearch_value());//已存在的查询字段则从数组中删除
			}
		}
		for (String string : paramList) {
			SearchListVo searchListInfo = new SearchListVo();
			searchListInfo.setSearch_value(string);
			searchListInfo.setSearch_type(ExternalDictConstants.JasperSearchType_input);
			searchListInfo.setSearch_label("默认标签");
			searchList.add(searchListInfo);
		}
		retMap.put("search_list", searchList);

		Map<String, Object> map = new HashMap<String, Object>(); 
		setDefaultMap(map);
		String directSql = FreeMarkerStringGenerator.getInstance().stringGenerate("test", execSQL, map);
		JY.debug(String.format("AutoCreateControls--生成的SQL--->%s",directSql));
		//3.2.取第一条SQL语句作为grid的查询语句，同事将grid查询语句中所有参数替换为空
		//去除排序
		if(directSql.lastIndexOf("ORDER BY") > 0 && directSql.lastIndexOf("ORDER BY") > directSql.lastIndexOf(")")){
			directSql = directSql.substring(0, directSql.lastIndexOf("ORDER BY"));
		}
		//3.3.创建视图，通过视图查询grid的列
		map.put("sql","create or replace view V_JOYREPORT_COLUMS as( " + directSql+" )");
		customReportMapper.searchReport(map);
		map.put("table_name","V_JOYREPORT_COLUMS");
		List<String> columsLists = customReportMapper.selectTableColums(map);
		List<String> columsList = new ArrayList<String>();
		for (String string : columsLists) {
			columsList.add(StringUtil.underLineToCamel(string));
		}
		//3.4根据传入的查询条件数组匹配数据，当主键存在时，使用原数组的，主键不存在时使用新的数组;
		List<ResultListVo> resultListVo = reportClobVo.getResult_list()==null?new ArrayList<ResultListVo>():reportClobVo.getResult_list();
		Iterator<ResultListVo> resultIter = resultListVo.iterator();
		while(resultIter.hasNext()){
			ResultListVo resultListInfo = resultIter.next();
			if(columsList.contains(resultListInfo.getResult_value())){
				columsList.remove(resultListInfo.getResult_value());//已存在的查询字段则从数组中删除
			}
		}
		for (String string : columsList) {
			ResultListVo resultListInfo = new ResultListVo();
			resultListInfo.setResult_value(string);
			resultListInfo.setResult_label("默认标签");
			resultListInfo.setColumn_align(ExternalDictConstants.ColumnAlign_left);
			resultListInfo.setColumn_width("100");
			resultListInfo.setDetail_show(ExternalDictConstants.YesNo_NO);
			resultListVo.add(resultListInfo);
		}
		retMap.put("result_list", resultListVo);
		return retMap;
	}

	/**
	 * 自定义报表
	 * 
	 * @param is_active
	 *            启用状态
	 * @param title
	 *            标题（模糊查询）
	 * @return
	 */
	@Override
	public Page<CustomReportListVo> searchReportList(String is_active, String title, int pageNum, int pageSize) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("is_active", is_active);
		map.put("title", title);
		return customReportMapper.pageCustomReports(map,ParameterUtil.getRowBounds(map));
	}
	
	@Override
	public Page<CustomReportListVo> pageCustomReports(Map<String, Object> pageData) {
		return customReportMapper.pageCustomReports(pageData, ParameterUtil.getRowBounds(pageData));
	}

	/**
	 * 保存自定报表对象
	 * 
	 * @param taCustomReport
	 * @return 
	 */
	@Override
	public  void saveReport(ReportClobVo reportClobVo, String userId, String instId) {
		String reportclob = FastJsonUtil.toJSONString(reportClobVo);
		reportClobVo.setReportclob(reportclob);
		reportClobVo.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		reportClobVo.setOperator_id(userId);

		// 无菜单名称用报表名
		if(StringUtils.isEmpty(reportClobVo.getModule_name())){
			reportClobVo.setModule_name(reportClobVo.getTitle());
		}
		//1.当前仅支持的报表类型为“自定义报表”
		if(StringUtils.isEmpty(reportClobVo.getReporttype())){
			reportClobVo.setReporttype(ExternalDictConstants.ReportType_JoyReport);
		}
		
		// 尝试解析下 sql 语句是否合法
		FreeMarkerStringGenerator.getInstance().addTemplate(reportClobVo.getCustid(), reportClobVo.getExecSQL());
		
		Map<String, Object> map = new HashMap<String, Object>() ;
		setDefaultMap(map );
		if(reportClobVo != null && reportClobVo.getSearch_list() != null && reportClobVo.getSearch_list().size() > 0){
			for(SearchListVo vo : reportClobVo.getSearch_list()){
				map.put(vo.getSearch_value(), null);
			}
		}
		
		//FreeMarkerStringGenerator.getInstance().checkTemplate(reportClobVo.getCustid(), map);
		
		if (StringUtils.equals("add", reportClobVo.getTypes())) {
			JY.require(customReportMapper.getReport(reportClobVo.getCustid()) == null, "报表编号(%s)已存在!", reportClobVo.getCustid());
			customReportMapper.insert(reportClobVo);
		} 
		// 菜单节点ID
		if(!StringUtils.isEmpty(reportClobVo.getModule_pid())){
			if(StringUtil.isNotEmpty(reportClobVo.getCustid())){
				reportClobVo.setModule_id(reportClobVo.getModule_pid()+"-"+reportClobVo.getCustid());
							// 菜单地址
			reportClobVo.setModule_url("base/report/ReportShow.jsp?id=" + reportClobVo.getCustid());
			customReportMapper.updateByPrimaryKey(reportClobVo);	
			}

		}
		
	}

	/**
	 * 根据
	 * 
	 * @param custid
	 *            自定报表编号
	 * @param is_active
	 *            启用状态（允许为空）
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@Override
	public ReportClobVo selectCustomReportInfo(String custid, String is_active){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("custid", custid);
		map.put("is_active", is_active);
		List<CustomReportListVo> list = customReportMapper.selectReport(map);
		JY.require(list.size() == 1, "自定报表编号[" + custid + "]不存在。");
		CustomReportListVo taCustomReport = list.get(0);
		// 将clob字段转换为页面所需的ReportClobVo对象
		ReportClobVo reportClobVo = FastJsonUtil.parseObject(taCustomReport.getReportclob(), ReportClobVo.class);
		reportClobVo.setCustid(custid);
		reportClobVo.setTemplate_file(taCustomReport.getTemplate_file());
		return reportClobVo;
	}

	private void dealJavaFunction(ReportClobVo reportClobVo,List<HashMap<String, Object>> list){
		for(ResultListVo resultListVo:reportClobVo.getResult_list()){
			if(StringUtil.isNotEmpty(resultListVo.getResult_formatter()) && resultListVo.getResult_formatter().startsWith("java.")){
				//解析出执行方法
				// java.coreBalance(SELF_ACC_CODE)
				String beanName=resultListVo.getResult_formatter().substring(5, resultListVo.getResult_formatter().indexOf("("));
				IFunction iFunction = SpringContextHolder.getBean(beanName);
				String value = "no bean (" + beanName+")";
				String param = resultListVo.getResult_formatter().substring(resultListVo.getResult_formatter().indexOf("(")+1,resultListVo.getResult_formatter().indexOf(")"));
				if(list.size() > 0){
					for(HashMap<String, Object> dbResult : list){
						try {
							if(iFunction!=null){
								value = iFunction.invoke(resultListVo,dbResult,param);
							}
						}catch (Exception e) {
							value = e.getMessage();
						}
						dbResult.put(resultListVo.getResult_value(),value);
					}
				}
			}
		}
	}
	/**
	 * 自定报表查询功能
	 * 
	 * @param map
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@Override
	public Page<HashMap<String, Object>> searchReport(HashMap<String, Object> map){
		String custid = ParameterUtil.getString(map, "custid", "");
		JY.require(!StringUtils.isEmpty(custid), "传入参数缺少报表编号.");
		ReportClobVo reportClobVo = selectCustomReportInfo(custid, "");
		String sql = reportClobVo.getExecSQL();
		setDefaultMap(map);
		
		String directSql = FreeMarkerStringGenerator.getInstance().stringGenerate(custid, sql, map);
		map.put("sql", directSql);
		JY.debug(String.format("自定义报表生成的SQL--->%s",directSql));
		Page<HashMap<String, Object>> ret = customReportMapper.searchReport(map, ParameterUtil.getRowBounds(map));
		if(ret.getResult() != null && ret.getResult().size() > 0){
			dealJavaFunction(reportClobVo,ret.getResult());
		}
		//去掉汇总、平均自定义报表字段   mod by lihb 20160805
//		StringBuffer sb = new StringBuffer();
//		String sums = SqlUtil.toSumSqlString(reportClobVo.getSumFieldList());
//		if(StringUtils.isNotEmpty(sums)){
//			sb.append(sums);
//		}
//		String avgs = SqlUtil.toAvgSqlString(reportClobVo.getAvgFieldList());
//		if(StringUtils.isNotEmpty(avgs)){
//			sb.append(avgs);
//		}
//		map.put(SqlUtil.SUMFIELDS,sb.toString());
//        Set<Entry<String, Object>>  set=map.entrySet();  
//        Iterator<Entry<String, Object>>   iterator=set.iterator();  
//        while (iterator.hasNext()) {  
//          Entry  entry = (Entry) iterator.next();  
//           
//        } 
		return ret;
	}

	/**
	 * 自定报表查询导出
	 * 
	 * @param map
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@Override
	public List<HashMap<String, Object>> searchReportExport(HashMap<String, Object> map){
		String custid = ParameterUtil.getString(map, "custid", "");
		JY.require(!StringUtils.isEmpty(custid), "传入参数缺少报表编号.");
		ReportClobVo reportClobVo = selectCustomReportInfo(custid, "");
		setDefaultMap(map);
		String directSql = FreeMarkerStringGenerator.getInstance().stringGenerate(custid, reportClobVo.getExecSQL(), map);
		map.put("sql", directSql);
		JY.debug(String.format("自定义报表生成的SQL--->%s",directSql));
		List<HashMap<String, Object>> ret = customReportMapper.searchReport(map);
		dealJavaFunction(reportClobVo,ret);
		// 1. 先执行一次不分页查询，用于统计记录数
		return ret;
	}
	
	/**
	 * 对查询内容追加默认参数
	 * @param map
	 * @return
	 */
	private void setDefaultMap(Map<String, Object> map){
		map.put("_USER_ID", SlSessionHelper.getUserId());
		map.put("_INST_ID", SlSessionHelper.getInstitutionId());
		String settlementDate = dds.getSettlementDate();
		map.put("_SETTLE_DATE", settlementDate);
		map.put("_NOW_DATE",DateUtil.getCurrentDateAsString());
//		map.put("_HEAD_INST_ID", SystemProperties.headBankInstId);
	}
//	
//	/**
//	 * 被执行的SQL语句格式化，如select aa from a where a1=${aa}
//	 * 1.将sql语句中aa替换为map中的aa的值
//	 * 2.当map中不存在aa的值是将${aa}替换为空
//	 * @param execSQL
//	 * @param map
//	 * @return
//	 */
//	private String sqlStringReplace(String execSQL,HashMap<String, Object> map){
//		String template = StringUtil.replaceMapToString(execSQL, map);
//		String patternString = "\\$\\{([a-zA-Z0-9_\\.\\-\\+]+)\\}";
//		Pattern pattern = Pattern.compile(patternString);
//		Matcher matcher = pattern.matcher(template);
//		return matcher.replaceAll("''");
//	}
	
	/**
	 * 导出SQL语句
	 * @param is_active
	 *            启用状态
	 * @param title
	 *            标题（模糊查询）
	 * @return
	 */
	@Override
	public HashMap<String, Object> exportSQL() {
		StringBuffer sb = new StringBuffer();
		HashMap<String, Object> map = new HashMap<String, Object>();
		List<CustomReportListVo> list = customReportMapper.selectReport(map);

		sb.append("SET DEFINE OFF;<br/>");
		sb.append("Delete from JOY_TRD.TA_CUSTOM_REPORT;<br/>");
		for (CustomReportListVo customReportListVo : list) {
			sb.append("INSERT INTO ta_custom_report(custid, reportclob, reporttype, title, is_active, operator_id, operate_time) VALUES ");
			sb.append("('"+customReportListVo.getCustid()+"', '"+customReportListVo.getReportclob().replaceAll("'", "''")+"', '"+customReportListVo.getReporttype()+"', '"+customReportListVo.getTitle()+"', '"+customReportListVo.getIs_active()+"', '"+customReportListVo.getOperator_id()+"', '"+customReportListVo.getOperate_time()+"');<br/>");
		}
		sb.append("COMMIT;");
		
		map = new HashMap<String, Object>();
		map.put("sql", sb.toString());
		return map;
	}
	
	/**
	 * 将自定义报表的table转换成字符串导出
	 * @param post
	 * @return
	 */
	@Override
	public String exportSQLString(@SuppressWarnings("rawtypes") HashMap post){
		HashMap<String, Object> map = new HashMap<String, Object>();
		String custidsStr = ParameterUtil.getString(post, "custids", "");
		JY.require(!StringUtils.isEmpty(custidsStr), "报表编号为空!");
		String[] custidsArr = custidsStr.split(",");
		List<String> custids = Arrays.asList(custidsArr);
		map.put("custids", custids);
		List<CustomReportListVo> list = customReportMapper.selectReport(map);
		return FastJsonUtil.toJSONString(list);
	}

	/**
	 * 将自定义报表的table转换成字符串导入
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@Override
	public void importSQLString(String strSQL){
		List<CustomReportListVo> list = FastJsonUtil.parseObject(strSQL, new TypeReference<List<CustomReportListVo>>() {});
		for (CustomReportListVo customReportListVo : list) {
			TaCustomReport taCustomReport = customReportMapper.getReport(customReportListVo.getCustid());
			if(taCustomReport == null){
				customReportMapper.insertReport(customReportListVo);
			}else{
				customReportMapper.updateByPrimaryKey(customReportListVo);
			}
		}
	}

	/**
	 * 删除模板缓存
	 * @param map
	 */
//	@Override
//	public void delReportCache(HashMap<String,Object> map) {
//		String add_time = ParameterUtil.getString(map, "add_time", null);
//		templateMap.remove(add_time);
//	}

	/**
	 * 获取上传文件
	 * @param custId
	 * @param fileSrc
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@Override
	public UploadedFile getUploadedFile(String custId, String fileSrc) {
		if(!StringUtils.isEmpty(custId)){
			TaCustomReport taCustomReport = customReportMapper.getReport(custId);
			if(taCustomReport != null && taCustomReport.getTemplate_file() != null && taCustomReport.getTemplate_file().length > 0){
				ReportClobVo reportClobVo = FastJsonUtil.parseObject(taCustomReport.getReportclob(), ReportClobVo.class);
				if(reportClobVo != null){
					List<HashMap<String, Object>> excelList = reportClobVo.getExcelModule();
					if(excelList != null && excelList.size() > 0){
						for(HashMap<String, Object> excelMap : excelList){
							if(StringUtils.equals(fileSrc, ParameterUtil.getString(excelMap, "file_src", ""))){
								UploadedFile uploadedFile = new UploadedFile();
								uploadedFile.setBytes(taCustomReport.getTemplate_file());
								return uploadedFile;
							}
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * 报表明细查询
	 * @param map
	 * @return
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@Override
	public HashMap<String, Object> getReportInfo(HashMap<String, Object> map) {
		try {
			String dialogSQL = ParameterUtil.getString(map, "dialogSQL", null);
			String custid = ParameterUtil.getString(map, "custid", "");
			JY.require(!StringUtils.isEmpty(dialogSQL), "缺少明细数据集!");
			setDefaultMap(map);
			String directSql = FreeMarkerStringGenerator.getInstance().stringGenerate(custid + "_detail", dialogSQL, map);
			map.put("sql", directSql);
			JY.debug(String.format("自定义报表生成的SQL--->%s",directSql));
			List<HashMap<String, Object>> list = customReportMapper.searchReport(map);
			JY.require(list != null && list.size() == 1, "无对应数据!");
			HashMap<String,Object> retMap = new HashMap<String,Object>();
			retMap.put("reportInfo", list.get(0));
			return retMap;
		} catch (Exception e) {
			JY.error(e);
			JY.raise(e.getMessage());
		}
		return null;
	}

	/**
	 * 删除报表
	 * @param map
	 */
	@Override
	public void delReport(HashMap<String,Object> map) {
		String custidsStr = ParameterUtil.getString(map, "custids", "");
		JY.require(!StringUtils.isEmpty(custidsStr), "报表编号为空!");
		String[] custidsArr = custidsStr.split(",");
		List<String> custids = Arrays.asList(custidsArr);
		map.put("custids", custids);
		customReportMapper.delReport(map);
	}
}
