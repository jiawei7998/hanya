package com.singlee.capital.print.service;

import java.util.Map;

/**
 * 打印组装数据接口
 * @author xuhui
 *
 */
public interface AssemblyDataService {

	public Map<String,Object> getData(String id);
}
