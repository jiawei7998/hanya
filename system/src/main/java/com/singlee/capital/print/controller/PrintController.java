package com.singlee.capital.print.controller;


import com.github.pagehelper.Page;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.HttpUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.print.model.TcPrintTemplate;
import com.singlee.capital.print.service.PrintTemplateService;
import com.singlee.capital.system.controller.CommonController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * 打印模板控制层
 *
 * @author XUHUI
 */
@Controller
@RequestMapping(value = "/PrintController")
public class PrintController extends CommonController {
    @Autowired
    private PrintTemplateService printTemplateService;


    /**
     * 上传打印文件
     *
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/uploadPrintTemplate", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String uploadPrintTemplate(MultipartHttpServletRequest request,
                                      HttpServletResponse response) {
        StringBuffer sb = new StringBuffer();
        try {
            // 保存模板
            printTemplateService.savePrintTemplate(request);

        } catch (Exception e) {
            JY.error(e);
            sb.append(e.getMessage());
        }
        return StringUtils.isEmpty(sb.toString()) ? "保存成功" : sb.toString();
    }

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/searchPrintTemplatePage")
    public RetMsg<PageInfo<TcPrintTemplate>> searchPrintTemplatePage(
            @RequestBody Map<String, Object> params) {
        Page<TcPrintTemplate> page = printTemplateService
                .searchPrintTemplatePage(params);
        return RetMsgHelper.ok(page);
    }

    /**
     * 根据模板编号获取信息
     *
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPrintTemplate")
    public RetMsg<TcPrintTemplate> getPrintTemplate(
            @RequestBody Map<String, Object> params) {
        String tempCode = ParameterUtil.getString(params, "tempCode", "");
        TcPrintTemplate temp = printTemplateService.getPrintTemplate(tempCode);
        return RetMsgHelper.ok(temp);
    }

    /**
     * 下载模板
     *
     * @param tempCode
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/downloadTemplate/{tempCode}", method = RequestMethod.GET)
    public void downloadTemplate(@PathVariable String tempCode,
                                 HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        TcPrintTemplate temp = printTemplateService.getPrintTemplate(tempCode);
        JY.require(temp != null, "模板%s 没有找到！", tempCode);
        String encode_filename = URLEncoder.encode(temp.getTemplateName()
                + ".xls", "utf-8");
        String ua = request.getHeader("User-Agent");
        if (ua != null && ua.indexOf("Firefox/") >= 0) {
            response.setHeader("Content-Disposition", "attachment; filename*="
                    + encode_filename);
        } else {
            response.setHeader("Content-Disposition", "attachment; filename="
                    + encode_filename);
        }

        HttpUtil.flushHttpResponse(response, "application/msexcel",
                temp.getTempContent());
    }
}
