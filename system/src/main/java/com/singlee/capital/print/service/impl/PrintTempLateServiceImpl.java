package com.singlee.capital.print.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.base.util.FileManage;
import com.singlee.capital.common.pojo.UploadedFile;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.print.mapper.TcPrintTemplateMapper;
import com.singlee.capital.print.model.TcPrintTemplate;
import com.singlee.capital.print.service.PrintTemplateService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.List;
import java.util.Map;

/**
 * 打印模板服务层
 * @author xuhui
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PrintTempLateServiceImpl implements PrintTemplateService {


	private static final String cacheName = "com.singlee.capital.print";
	
	
    @Autowired
    private TcPrintTemplateMapper tempMapper;
    /**
     * 查询分页
     */
	@Override
	public Page<TcPrintTemplate> searchPrintTemplatePage(
			Map<String, Object> params) {
		return tempMapper.searchPrintTemplatePage(params, ParameterUtil.getRowBounds(params));
	}
	
	@Override
	@CacheEvict(value = cacheName ,allEntries=true)
	public void savePrintTemplate(MultipartHttpServletRequest request) throws Exception {
		// 1.文件获取
		List<UploadedFile> uploadedFileList = FileManage.requestExtractor(request);
		String tempCode = request.getParameter("tempCode");
		TcPrintTemplate template = new TcPrintTemplate();
		template.setTempCode(tempCode);
		template = tempMapper.selectByPrimaryKey(template);
		JY.require(template!=null, "没有找到编号%s的模板!",tempCode);
		template.setTemplateName(request.getParameter("templateName"));
		template.setBeanName(request.getParameter("beanName"));
		if(uploadedFileList!=null && uploadedFileList.size()>0){
			UploadedFile uploadedFile =uploadedFileList.get(0);
			if(StringUtils.isNotEmpty(uploadedFile.getFullname())){
				//JY.ensure("xls".equals(uploadedFile.getType()), "上传文件类型错误,仅允许上传xls格式文件.");
				template.setTempContent(uploadedFile.getBytes());
			}
		}
		template.setTempDesc(request.getParameter("tempDesc"));
		
		tempMapper.updateByPrimaryKey(template);
	}
	
	@Override
	@Cacheable(value = cacheName, key = "#tempCode")
	public TcPrintTemplate getPrintTemplate(String tempCode) {
		return tempMapper.getPrintTemplate(tempCode);
	}

}
