package com.singlee.capital.print.service;

import com.singlee.capital.common.spring.ReleaseResourceInitListener;
import org.springframework.stereotype.Service;
@Service
public class PrintModuleListener extends ReleaseResourceInitListener{

	@Override
	protected String jarFilePath() {
		return PrintModuleListener.class.getResource("").getPath();
	}

	@Override
	protected String jarVersion() {
		return "1.0.0.1";
	}

}
