package com.singlee.capital.print.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 打印模板对象
 * @author xuhui
 *
 */
@Entity
@Table(name = "TC_PRINT_TEMPLATE")
public class TcPrintTemplate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 模板编号
	 */
	@Id
	private String tempCode;
	/**
	 * 模板名称
	 */
	private String templateName;
	/**
	 * 模板数据源获取的bean类
	 */
	private String beanName;
	/**
	 * 模板内容
	 */
	private byte[] tempContent;
	/**
	 * 描述
	 */
	private String tempDesc;
	
	public String getTempCode() {
		return tempCode;
	}
	public void setTempCode(String tempCode) {
		this.tempCode = tempCode;
	}
	public byte[] getTempContent() {
		return tempContent;
	}
	public void setTempContent(byte[] tempContent) {
		this.tempContent = tempContent;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTempDesc() {
		return tempDesc;
	}
	public void setTempDesc(String tempDesc) {
		this.tempDesc = tempDesc;
	}
	public String getBeanName() {
		return beanName;
	}
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}
	
}
