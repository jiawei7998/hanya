package com.singlee.capital.print.mapper;

import com.github.pagehelper.Page;
import com.singlee.capital.print.model.TcPrintTemplate;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;
/**
 * 打印模板持久层
 * @author xuhui
 *
 */
public interface TcPrintTemplateMapper extends Mapper<TcPrintTemplate>{
	/**
	 * 查询分页
	 * @param params
	 * @param rb
	 * @return
	 */
	public Page<TcPrintTemplate> searchPrintTemplatePage(Map<String, Object> params,RowBounds rb);
	
	/**
	 * 根据编号获取
	 * @param tempId
	 * @return
	 */
	public TcPrintTemplate getPrintTemplate(@Param("tempCode")String tempCode);
}
