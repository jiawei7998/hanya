package com.singlee.capital.print.service;

import com.github.pagehelper.Page;
import com.singlee.capital.print.model.TcPrintTemplate;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

/**
 * 打印模板服务层
 * @author xuhui
 *
 */
public interface PrintTemplateService {
	/**
	 * 打印模板查询分页
	 * @param params
	 * @return
	 */
	public Page<TcPrintTemplate> searchPrintTemplatePage(Map<String,Object> params);
	
	/**
	 * 保存上传的模板文件
	 * @param request
	 */
	public void savePrintTemplate(MultipartHttpServletRequest request) throws Exception;
	
	/**
	 * 根据模板编号获取
	 * @param tempCode
	 * @return
	 */
	public TcPrintTemplate getPrintTemplate(String tempCode);
}
