package com.singlee.capital.dayend.service.impl;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.dayend.service.DayendFlowHandler;
import com.singlee.capital.system.dict.ExternalDictConstants;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import org.apache.commons.chain.Context;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 日终处理步骤 的抽象类
 * 
 * @author x230i
 * 
 */
public abstract class AbstractDayendFlowHandler implements DayendFlowHandler {


	/** 日终日期服务 */
	@Autowired
	protected DayendDateService dayendDateService;
	
	

	/***
	 * ----以上为对外服务-------------------------------
	 */

	/**
	 * 
	 */
	@Override
	public boolean execute(Context arg0) throws Exception {
		JY.info("----DAYEND(ADFH)----[%s]----execute ", this.getClass().getSimpleName());
		// 检查是否需要立刻终止  
		Boolean isStop = (Boolean)arg0.get(DayendFlowHandler.stop);
		JY.require(!isStop, "人为中止了日终步骤！");
		
		SlSession session = (SlSession)arg0.get(DayendFlowHandler.slsession);
		// 修改日终步骤 状态 为 执行中
		sychroniseTtdayendStepLog(SlSessionHelper.getUserId(session), ExternalDictConstants.DayendFlowStatus_Executing, "");
		JY.info("----DAYEND(ADFH)----[%s]----executing ", this.getClass().getSimpleName());
		try{
			// DO IT
			String ret = execute2(session);
			// 修改 日终 步骤状态为成功或者告警 
			sychroniseTtdayendStepLog(SlSessionHelper.getUserId(session), 
					StringUtils.isEmpty(ret)? ExternalDictConstants.DayendFlowStatus_Success:ExternalDictConstants.DayendFlowStatus_Warning
							, ret);
			JY.info("----DAYEND(ADFH)----[%s]----success ", this.getClass().getSimpleName());
		}catch(Exception e){
			// 修改 日终 步骤状态为错误  
			sychroniseTtdayendStepLog(SlSessionHelper.getUserId(session), ExternalDictConstants.DayendFlowStatus_Error, "");
			JY.info("----DAYEND(ADFH)----[%s]----error ", this.getClass().getSimpleName());
			// 继续 往外面 抛异常 
			throw e;
		}
		return false;
	}
	/**
	 * 同步 日终步骤日志 数据库记录 
	 * @param tds
	 * @param userId
	 */
	protected abstract void sychroniseTtdayendStepLog( String userId, String status, String log) ;
	
	@Override
	public boolean postprocess(Context arg0, Exception exception) {
		JY.info("----DAYEND(ADFH)----[%s]----postprocess -> %s", this.getClass().getSimpleName(), exception==null?"":exception.getMessage());
		SlSession session = (SlSession)arg0.get(DayendFlowHandler.slsession);
		return postprocess(session, exception);
	}

	/**
	 * 
	 * 可以override 
	 * 
	 * @param session
	 * @param exception
	 */
	protected abstract void execute(SlSession session);
	

	/**
	 * 
	 * 可以override 
	 * 
	 * @param session
	 * @param exception
	 * @return 返回 null 就是 
	 */
	protected abstract String execute2(SlSession session);
	/**
	 * 
	 * 可以override 
	 * @param session
	 * @param exception
	 */
	protected abstract boolean postprocess(SlSession session, Exception exception);
}
