package com.singlee.capital.system.session.redisson;

import com.google.common.collect.Maps;
import com.singlee.capital.system.service.SystemProperties;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.redisson.spring.session.config.EnableRedissonHttpSession;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

import java.util.Map;

/**
 * redisson Session 配置类
 */
@Configuration
@EnableRedissonHttpSession
@EnableCaching
public class RedissonSessionConfig extends AbstractHttpSessionApplicationInitializer {

    @Bean(destroyMethod = "shutdown")
    RedissonClient redissonClient() {
        return loadRedisson();
    }

    @Bean
    public CookieSerializer httpSessionIdResolver() {
        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();
        //取消仅同一站点设置
        cookieSerializer.setSameSite(null);
        return cookieSerializer;
    }

    @Bean
    RedissonSpringCacheManager cacheManager(RedissonClient redissonClient) {
        Map<String, CacheConfig> config = Maps.newHashMap();
        // 创建一个名称为"testMap"的缓存，过期时间ttl为24分钟，同时最长空闲时maxIdleTime为12分钟。
//        config.put("defaultCache", new CacheConfig(24*60*60*1000, 12*60*1000));
        return new RedissonSpringCacheManager(redissonClient);
    }

    /**
     * 单例模式连接
     *
     * @return
     */
    public RedissonClient loadRedisson() {
        //单节点
        Config config = new Config();
        if (StringUtils.isNotBlank(SystemProperties.redisPassword)) {
            config.useSingleServer().setPassword(SystemProperties.redisPassword);
        }
        config.setCodec(new JsonJacksonCodec());
        config.useSingleServer().setAddress(SystemProperties.redisAddress);
        return Redisson.create(config);
    }

    /**
     * 失败重连
     *
     * @return
     */
    public RedissonClient retryGetRedisson() {
        return loadRedisson();
    }
}
