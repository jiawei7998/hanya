package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.RecForeignCrsModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.List;

@Capital(context = CapitalContext.SINGLEE_API, uri = "/cpisCrsTradeServiceExporter")
public interface ICpisCrsTradeService {
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(RecForeignCrsModel recForeignCrsModel) throws  Exception;
	
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(List<RecForeignCrsModel> list) throws  Exception;
}
