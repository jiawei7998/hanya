package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.SecurityLendingBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * 债券借贷模块业务处理类,发送债券借贷业务对象到Capital Web资金前置系统中
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/cfetsImixSecurityLendingExporter")
public interface ICfetsImixSecurityLending {

    /**
     * 发送债券借贷交易到资金前置平台
     *
     * @param securityLendingBean 债券借贷业务对象
     * @param packet              报文对象  modify by shenzl add
     * @return
     */
    RetBean send(SecurityLendingBean securityLendingBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;
}
