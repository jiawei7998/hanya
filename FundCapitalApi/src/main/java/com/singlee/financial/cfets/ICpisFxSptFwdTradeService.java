package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.RecForeignForwardSpotModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.List;

@Capital(context = CapitalContext.SINGLEE_API,uri = "/cpisFxSptFwdTradeServiceExporter")
public interface ICpisFxSptFwdTradeService {
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(RecForeignForwardSpotModel model) throws  Exception;
	
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(List<RecForeignForwardSpotModel> list) throws  Exception;
}
