package com.singlee.financial.cfets.bean;


/**
 * 接收查询(确认状态)失败的实体
 * @author Administrator
 *
 */
public class FailRecConfirmStatusModel extends RecMsgBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private	String  affirmStatus;	 //0-数据匹配  其他-对应的问题编码
	
	private	String  matchStatus; //查询匹配状态 1-数据不匹配
	
	private	String queryRequestID; //查询请求编号 
	
	private	String text;  //未匹配成功字段

	public String getAffirmStatus() {
		return affirmStatus;
	}

	public void setAffirmStatus(String affirmStatus) {
		this.affirmStatus = affirmStatus;
	}

	public String getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(String matchStatus) {
		this.matchStatus = matchStatus;
	}

	public String getQueryRequestID() {
		return queryRequestID;
	}

	public void setQueryRequestID(String queryRequestID) {
		this.queryRequestID = queryRequestID;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
	

}
