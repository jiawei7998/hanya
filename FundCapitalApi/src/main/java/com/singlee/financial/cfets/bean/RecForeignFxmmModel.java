package com.singlee.financial.cfets.bean;


public class RecForeignFxmmModel extends RecMsgBean {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String execID; //成交编号
	
	private String confirmID;//报文编号
	
	private String marketIndicator;//交易市场类型
	
	private String tradeDate;//成交日期
	
	private String  netGrossInd;//清算方式
	
	private	String settlDate;//起息日
	
	private	String settlDate2;//到期日
	
	private	String currency;//买入币种
	
	private	String lastPx;//成交价格
	
	private String dayCount;//计息基础
	
	private String side;//本方交易方向
	
	private String accruedInterestTotalAmt;//应计利息
	
	private	String lastQty;//买入金额
	
	private String settleCurrAmt2;//到期还款金额
	
	private	String othPartyID;//对手方机构21 位码
	
	private	String ownPartyID; //本方机构 21 位码
	
	private	String ownCapitalAccountName; //资金账户户名
	
	private	String ownPayBankNo;//支付系统行号
	
	private	String ownCapitalAccountBIC; //开户行BICCODE
	
	private	String ownCapitalBakeName; //资金开户行
	
	private String ownCapitalAccount; //资金账号
	
	private	String ownCnName; //中文全称
	
	private	String ownEnName; //英文全称
	
	private	String ownRemark; //附言
	
	private	int ownPartyRole;
	
	private	int othPartyRole;
	
	private	String ownOpenAccount; //开户行账号
	
	private	String othCapitalBakeName; //资金开户行
	
	private	String othCapitalAccountName; //资金账户户名
	
	private	String othCapitalAccount; //资金账号
	
	private	String othPayBankNo;//支付系统行号
	
    private String execType;
	
	private String dealTransType;

	private String confirmStatus;//成交状态
	
	private int ownCnfmTimeType;//本方确认时间
	
	private	String ownCnfmTime;
	
	private int othCnfmTimeType;
	
	private	String othCnfmTime;//对手方 确认时间
		
	private	String othRemark; //附言
	
	private	String midCnName; //中间行名称
	
	private	String midBankBIC; //中间行BICCODE
	
	private	String midBankAccount; //中间行账号
	
	private	String othCnName; //中文全称
	
	private	String othEnName; //英文全称

	private String currency2;//卖出币种
	
	private	String calculatedCcyLastQty;//卖出金额

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getDealTransType() {
		return dealTransType;
	}

	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getNetGrossInd() {
		return netGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

	public String getConfirmStatus() {
		return confirmStatus;
	}

	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}

	public int getOwnCnfmTimeType() {
		return ownCnfmTimeType;
	}

	public void setOwnCnfmTimeType(int ownCnfmTimeType) {
		this.ownCnfmTimeType = ownCnfmTimeType;
	}

	public String getOwnCnfmTime() {
		return ownCnfmTime;
	}

	public void setOwnCnfmTime(String ownCnfmTime) {
		this.ownCnfmTime = ownCnfmTime;
	}

	public int getOthCnfmTimeType() {
		return othCnfmTimeType;
	}

	public void setOthCnfmTimeType(int othCnfmTimeType) {
		this.othCnfmTimeType = othCnfmTimeType;
	}

	public String getOthCnfmTime() {
		return othCnfmTime;
	}

	public void setOthCnfmTime(String othCnfmTime) {
		this.othCnfmTime = othCnfmTime;
	}

	public String getSettlDate() {
		return settlDate;
	}

	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}

	public String getLastPx() {
		return lastPx;
	}

	public void setLastPx(String lastPx) {
		this.lastPx = lastPx;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getLastQty() {
		return lastQty;
	}

	public void setLastQty(String lastQty) {
		this.lastQty = lastQty;
	}

	public String getCurrency2() {
		return currency2;
	}

	public void setCurrency2(String currency2) {
		this.currency2 = currency2;
	}

	public String getCalculatedCcyLastQty() {
		return calculatedCcyLastQty;
	}

	public void setCalculatedCcyLastQty(String calculatedCcyLastQty) {
		this.calculatedCcyLastQty = calculatedCcyLastQty;
	}

	public String getOwnPartyID() {
		return ownPartyID;
	}

	public void setOwnPartyID(String ownPartyID) {
		this.ownPartyID = ownPartyID;
	}

	public int getOwnPartyRole() {
		return ownPartyRole;
	}

	public void setOwnPartyRole(int ownPartyRole) {
		this.ownPartyRole = ownPartyRole;
	}

	public String getOthPartyID() {
		return othPartyID;
	}

	public void setOthPartyID(String othPartyID) {
		this.othPartyID = othPartyID;
	}

	public int getOthPartyRole() {
		return othPartyRole;
	}

	public void setOthPartyRole(int othPartyRole) {
		this.othPartyRole = othPartyRole;
	}

	public String getOwnCnName() {
		return ownCnName;
	}

	public void setOwnCnName(String ownCnName) {
		this.ownCnName = ownCnName;
	}

	public String getOwnCapitalBakeName() {
		return ownCapitalBakeName;
	}

	public void setOwnCapitalBakeName(String ownCapitalBakeName) {
		this.ownCapitalBakeName = ownCapitalBakeName;
	}

	public String getOwnCapitalAccountName() {
		return ownCapitalAccountName;
	}

	public void setOwnCapitalAccountName(String ownCapitalAccountName) {
		this.ownCapitalAccountName = ownCapitalAccountName;
	}

	public String getOwnPayBankNo() {
		return ownPayBankNo;
	}

	public void setOwnPayBankNo(String ownPayBankNo) {
		this.ownPayBankNo = ownPayBankNo;
	}

	public String getOwnCapitalAccountBIC() {
		return ownCapitalAccountBIC;
	}

	public void setOwnCapitalAccountBIC(String ownCapitalAccountBIC) {
		this.ownCapitalAccountBIC = ownCapitalAccountBIC;
	}

	public String getOwnCapitalAccount() {
		return ownCapitalAccount;
	}

	public void setOwnCapitalAccount(String ownCapitalAccount) {
		this.ownCapitalAccount = ownCapitalAccount;
	}

	public String getOwnRemark() {
		return ownRemark;
	}

	public void setOwnRemark(String ownRemark) {
		this.ownRemark = ownRemark;
	}

	public String getMidCnName() {
		return midCnName;
	}

	public void setMidCnName(String midCnName) {
		this.midCnName = midCnName;
	}

	public String getMidBankBIC() {
		return midBankBIC;
	}

	public void setMidBankBIC(String midBankBIC) {
		this.midBankBIC = midBankBIC;
	}

	public String getMidBankAccount() {
		return midBankAccount;
	}

	public void setMidBankAccount(String midBankAccount) {
		this.midBankAccount = midBankAccount;
	}

	public String getOwnOpenAccount() {
		return ownOpenAccount;
	}

	public void setOwnOpenAccount(String ownOpenAccount) {
		this.ownOpenAccount = ownOpenAccount;
	}

	public String getOthCnName() {
		return othCnName;
	}

	public void setOthCnName(String othCnName) {
		this.othCnName = othCnName;
	}

	public String getOthCapitalBakeName() {
		return othCapitalBakeName;
	}

	public void setOthCapitalBakeName(String othCapitalBakeName) {
		this.othCapitalBakeName = othCapitalBakeName;
	}

	public String getOthCapitalAccountName() {
		return othCapitalAccountName;
	}

	public void setOthCapitalAccountName(String othCapitalAccountName) {
		this.othCapitalAccountName = othCapitalAccountName;
	}

	public String getOthCapitalAccount() {
		return othCapitalAccount;
	}

	public void setOthCapitalAccount(String othCapitalAccount) {
		this.othCapitalAccount = othCapitalAccount;
	}

	public String getOthPayBankNo() {
		return othPayBankNo;
	}

	public void setOthPayBankNo(String othPayBankNo) {
		this.othPayBankNo = othPayBankNo;
	}

	public String getOthRemark() {
		return othRemark;
	}

	public void setOthRemark(String othRemark) {
		this.othRemark = othRemark;
	}

	public String getSettlDate2() {
		return settlDate2;
	}

	public void setSettlDate2(String settlDate2) {
		this.settlDate2 = settlDate2;
	}

	public String getDayCount() {
		return dayCount;
	}

	public void setDayCount(String dayCount) {
		this.dayCount = dayCount;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getAccruedInterestTotalAmt() {
		return accruedInterestTotalAmt;
	}

	public void setAccruedInterestTotalAmt(String accruedInterestTotalAmt) {
		this.accruedInterestTotalAmt = accruedInterestTotalAmt;
	}

	public String getSettleCurrAmt2() {
		return settleCurrAmt2;
	}

	public void setSettleCurrAmt2(String settleCurrAmt2) {
		this.settleCurrAmt2 = settleCurrAmt2;
	}

	public String getOwnEnName() {
		return ownEnName;
	}

	public void setOwnEnName(String ownEnName) {
		this.ownEnName = ownEnName;
	}

	public String getOthEnName() {
		return othEnName;
	}

	public void setOthEnName(String othEnName) {
		this.othEnName = othEnName;
	}
	
	
	
	
}
