package com.singlee.financial.cfets.bean;

import java.io.Serializable;



public class RecPageInfoModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private	String confirmType; //106  请求类型： 确认成交明细查询
	
	private	String queryRequestID; //查询请求编号和查询请求 BH 报文

	private	String queryStartNumber;// 查询返回起始结果数
			
	private	String totNumReports;//  查询结果总数
			
	private	String totNumReportPages;// 查询结果分页数
			
	private	String queryPageNumber;//  当前页码
			
	private	String msgNumofCurrPage; //  当前页序号
			
	private	String currentNumReports; // 当前页记录数
			
	private	 String lastRptCurrPage;//当前页是否结束
		
	private	String lastRptRequested ;//当前查询是否结束标识

	public String getConfirmType() {
		return confirmType;
	}

	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}

	public String getQueryRequestID() {
		return queryRequestID;
	}

	public void setQueryRequestID(String queryRequestID) {
		this.queryRequestID = queryRequestID;
	}

	public String getQueryStartNumber() {
		return queryStartNumber;
	}

	public void setQueryStartNumber(String queryStartNumber) {
		this.queryStartNumber = queryStartNumber;
	}

	public String getTotNumReports() {
		return totNumReports;
	}

	public void setTotNumReports(String totNumReports) {
		this.totNumReports = totNumReports;
	}

	public String getTotNumReportPages() {
		return totNumReportPages;
	}

	public void setTotNumReportPages(String totNumReportPages) {
		this.totNumReportPages = totNumReportPages;
	}

	public String getQueryPageNumber() {
		return queryPageNumber;
	}

	public void setQueryPageNumber(String queryPageNumber) {
		this.queryPageNumber = queryPageNumber;
	}

	public String getMsgNumofCurrPage() {
		return msgNumofCurrPage;
	}

	public void setMsgNumofCurrPage(String msgNumofCurrPage) {
		this.msgNumofCurrPage = msgNumofCurrPage;
	}

	public String getCurrentNumReports() {
		return currentNumReports;
	}

	public void setCurrentNumReports(String currentNumReports) {
		this.currentNumReports = currentNumReports;
	}

	public String getLastRptCurrPage() {
		return lastRptCurrPage;
	}

	public void setLastRptCurrPage(String lastRptCurrPage) {
		this.lastRptCurrPage = lastRptCurrPage;
	}

	public String getLastRptRequested() {
		return lastRptRequested;
	}

	public void setLastRptRequested(String lastRptRequested) {
		this.lastRptRequested = lastRptRequested;
	}
	
	
		
}
