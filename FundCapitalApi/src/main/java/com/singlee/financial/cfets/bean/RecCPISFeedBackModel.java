package com.singlee.financial.cfets.bean;


/**
 * 接收CFETS消息反馈的报文实体
 * @author pengqian
 *
 */
public class RecCPISFeedBackModel extends RecMsgBean{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//确认编号
	private	String confirmID;
		
	//成交编号
	private	String execID;
		
	//校验状态	
	private	String affirmStatus;
		
	//成交日期
	private	String tradeDate;
		
	//交易场所	
	private	String marketID;

	//市场类型	
	private	String marketIndicator;
		
	//查询ID
	private	String queryRequestID;
		

	//匹配状态
	private	String matchStatus;

	//未匹配字段
	private	String text;
	
	//确认状态
	private String confirmStatus;
	
	//SUMMIT交易编号
	private String dealno;

	//成交状态
	private String execType;

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getAffirmStatus() {
		return affirmStatus;
	}

	public void setAffirmStatus(String affirmStatus) {
		this.affirmStatus = affirmStatus;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getQueryRequestID() {
		return queryRequestID;
	}

	public void setQueryRequestID(String queryRequestID) {
		this.queryRequestID = queryRequestID;
	}

	public String getMatchStatus() {
		return matchStatus;
	}

	public void setMatchStatus(String matchStatus) {
		this.matchStatus = matchStatus;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getConfirmStatus() {
		return confirmStatus;
	}

	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	
	

}
















