package com.singlee.financial.cfets.bean;


public class RecInterestRateSwapModel extends RecMsgBean {
	/**
	 * 接收利率互换报文实体类
	 */
	private static final long serialVersionUID = 1L;

	private String confirmID;
	
	private	String confirmType;
	
	private	String confirmTransType;
	
	private	String confirmStatus;
	
	private	String execType;
	
	private	String dealTransType;
	
	private	String marketID;
	
	private	String execID;

	private	String lastQty;
	
	private	String marketIndicator;
		
	private	String symbol;
		
	private	String text;
	
	private	String startDate;
				
	private	String tradeDate;
				
	private	String endDate;
	
	private	String couponPaymentDateReset;
	
	private	String firstPeriodStartDate;
	
	private	String tradeLimitDays;
	
	private	String calculateAgency;
	
	private	String netGrossInd;
	
	private	String side;
	
	private	String fixedLegSide; //固 定 
	
	private	String fixedLegPriceType; //固 定 
	
	private	String fixedLegPrice; //固 定 利 率 ， 单位：%，精度：4位
	
	private	String fixedLegCouponPaymentFrequency; //固定利率支付周期0-半年；1-年；2-到期； 3-月； 4-季；5-两周；6-周；7-天；9-九个月
	
	private	String fixedLegCouponPaymentDate; //固定利率支付日
	
	private	String fixedLegDayCount; //固定利率计息基准0-实际/实际；1-实 际 /360 ；2-30/360； 3-实际/365； D-30E/360；5-实际/365F
	
	private	String fixedLegInterestAccrualMethod ; //固定利率计息方法 0-单利 1-复利
	
	private	String floatingLegSide; 
	
	private	String floatingLegPriceType; //浮动 
	
	private	String floatingLegBenchmarkCurveName; //参考利率
	
	private	String floatingLegBenchmarkSpread; //利差，单位：bps，精度：2 位
	
	private	String floatingLegCouponPaymentDate; //浮动利率支付日
	
	private String floatingLegCouponPaymentFrequency; //浮动利率支付周期0-半年；1-年；2-到期； 3-月； 4-季；5-两周；6-周；7-天；9-九个月
	
	private	String floatingLegInterestAccrualDate; //利率确定日
	
	private	String floatingLegInterestAccrualResetFrequency; //重置频率 0-半年；1-年；2-到期； 3-月； 4-季；5-两周；6-周；7-天；9-九个月
	
	private	String floatingLegInterestAccrualMethod; //浮动利率计息方法 0-单利 1-复利
	
	private	String floatingLegDayCount; //浮动利计息基准0-实际/实际；1-实 际 /360 ；2-30/360； 3-实际/365； D-30E/360；5-实际/365F
	
	private	String fixedPartyID; //机构 21 位码
	
	private	int fixedPartyRole ; //固定利率支付方
	
	private	String fixedTel; //电话
	
	private	String fixedFax; //传真
	
	private	String fixedBrhName; //机构中文全称
	
	private	String fixedUser;//联系人
	
	private	String fixedAccountName; //资金账户户名
	
	private	String fixedAccount; //资金账号
	
	private	String fixedCapitalBankName; //资金开户行
	
	private	String fixedPayBankNo; //支付系统行号
	
	
	private	String floatingPartyID;//机构 21 位码
	
	private	int floatingPartyRole; // 固定利率收取方  (浮动利率支付方）
	
	private	String floatingTel; //电话
	
	private	String floatingFax; //传真
	
	private	String floatingUser;//联系人
	
	private	String floatingBrhName; //机构中文全称
	
	private	String floatingAccountName; //资金账户户名
	
	private	String floatingAccount; //资金账号
	
	private	String floatingCapitalBankName; //资金开户行
	
	private	String floatingPayBankNo; //支付系统行号
	
	private String	interestAccuralDaysAdjustment;
	
	private	String settlementStatus;
	
	private	String tradeMethod;
				
	private	String trdType;
	
	private	String ownCnfmTime;
	
	private	String othCnfmTime;

	public String getInterestAccuralDaysAdjustment() {
		return interestAccuralDaysAdjustment;
	}

	public void setInterestAccuralDaysAdjustment(
			String interestAccuralDaysAdjustment) {
		this.interestAccuralDaysAdjustment = interestAccuralDaysAdjustment;
	}

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

	public String getConfirmType() {
		return confirmType;
	}

	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}

	public String getConfirmTransType() {
		return confirmTransType;
	}

	public void setConfirmTransType(String confirmTransType) {
		this.confirmTransType = confirmTransType;
	}

	public String getConfirmStatus() {
		return confirmStatus;
	}

	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getDealTransType() {
		return dealTransType;
	}

	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getLastQty() {
		return lastQty;
	}

	public void setLastQty(String lastQty) {
		this.lastQty = lastQty;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNetGrossInd() {
		return netGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	public String getSettlementStatus() {
		return settlementStatus;
	}

	public void setSettlementStatus(String settlementStatus) {
		this.settlementStatus = settlementStatus;
	}

	public String getCouponPaymentDateReset() {
		return couponPaymentDateReset;
	}

	public void setCouponPaymentDateReset(String couponPaymentDateReset) {
		this.couponPaymentDateReset = couponPaymentDateReset;
	}

	public String getFirstPeriodStartDate() {
		return firstPeriodStartDate;
	}

	public void setFirstPeriodStartDate(String firstPeriodStartDate) {
		this.firstPeriodStartDate = firstPeriodStartDate;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getTradeLimitDays() {
		return tradeLimitDays;
	}

	public void setTradeLimitDays(String tradeLimitDays) {
		this.tradeLimitDays = tradeLimitDays;
	}

	public String getTradeMethod() {
		return tradeMethod;
	}

	public void setTradeMethod(String tradeMethod) {
		this.tradeMethod = tradeMethod;
	}

	public String getTrdType() {
		return trdType;
	}

	public void setTrdType(String trdType) {
		this.trdType = trdType;
	}

	public String getCalculateAgency() {
		return calculateAgency;
	}

	public void setCalculateAgency(String calculateAgency) {
		this.calculateAgency = calculateAgency;
	}

	public String getOwnCnfmTime() {
		return ownCnfmTime;
	}

	public void setOwnCnfmTime(String ownCnfmTime) {
		this.ownCnfmTime = ownCnfmTime;
	}

	public String getOthCnfmTime() {
		return othCnfmTime;
	}

	public void setOthCnfmTime(String othCnfmTime) {
		this.othCnfmTime = othCnfmTime;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getFixedLegSide() {
		return fixedLegSide;
	}

	public void setFixedLegSide(String fixedLegSide) {
		this.fixedLegSide = fixedLegSide;
	}

	public String getFixedLegPriceType() {
		return fixedLegPriceType;
	}

	public void setFixedLegPriceType(String fixedLegPriceType) {
		this.fixedLegPriceType = fixedLegPriceType;
	}

	public String getFixedLegPrice() {
		return fixedLegPrice;
	}

	public void setFixedLegPrice(String fixedLegPrice) {
		this.fixedLegPrice = fixedLegPrice;
	}

	public String getFixedLegCouponPaymentFrequency() {
		return fixedLegCouponPaymentFrequency;
	}

	public void setFixedLegCouponPaymentFrequency(
			String fixedLegCouponPaymentFrequency) {
		this.fixedLegCouponPaymentFrequency = fixedLegCouponPaymentFrequency;
	}

	public String getFixedLegCouponPaymentDate() {
		return fixedLegCouponPaymentDate;
	}

	public void setFixedLegCouponPaymentDate(String fixedLegCouponPaymentDate) {
		this.fixedLegCouponPaymentDate = fixedLegCouponPaymentDate;
	}

	public String getFixedLegDayCount() {
		return fixedLegDayCount;
	}

	public void setFixedLegDayCount(String fixedLegDayCount) {
		this.fixedLegDayCount = fixedLegDayCount;
	}

	public String getFixedLegInterestAccrualMethod() {
		return fixedLegInterestAccrualMethod;
	}

	public void setFixedLegInterestAccrualMethod(
			String fixedLegInterestAccrualMethod) {
		this.fixedLegInterestAccrualMethod = fixedLegInterestAccrualMethod;
	}

	public String getFloatingLegSide() {
		return floatingLegSide;
	}

	public void setFloatingLegSide(String floatingLegSide) {
		this.floatingLegSide = floatingLegSide;
	}

	public String getFloatingLegPriceType() {
		return floatingLegPriceType;
	}

	public void setFloatingLegPriceType(String floatingLegPriceType) {
		this.floatingLegPriceType = floatingLegPriceType;
	}

	public String getFloatingLegBenchmarkCurveName() {
		return floatingLegBenchmarkCurveName;
	}

	public void setFloatingLegBenchmarkCurveName(
			String floatingLegBenchmarkCurveName) {
		this.floatingLegBenchmarkCurveName = floatingLegBenchmarkCurveName;
	}

	public String getFloatingLegBenchmarkSpread() {
		return floatingLegBenchmarkSpread;
	}

	public void setFloatingLegBenchmarkSpread(String floatingLegBenchmarkSpread) {
		this.floatingLegBenchmarkSpread = floatingLegBenchmarkSpread;
	}

	public String getFloatingLegCouponPaymentDate() {
		return floatingLegCouponPaymentDate;
	}

	public void setFloatingLegCouponPaymentDate(String floatingLegCouponPaymentDate) {
		this.floatingLegCouponPaymentDate = floatingLegCouponPaymentDate;
	}

	public String getFloatingLegCouponPaymentFrequency() {
		return floatingLegCouponPaymentFrequency;
	}

	public void setFloatingLegCouponPaymentFrequency(
			String floatingLegCouponPaymentFrequency) {
		this.floatingLegCouponPaymentFrequency = floatingLegCouponPaymentFrequency;
	}

	public String getFloatingLegInterestAccrualDate() {
		return floatingLegInterestAccrualDate;
	}

	public void setFloatingLegInterestAccrualDate(
			String floatingLegInterestAccrualDate) {
		this.floatingLegInterestAccrualDate = floatingLegInterestAccrualDate;
	}

	public String getFloatingLegInterestAccrualResetFrequency() {
		return floatingLegInterestAccrualResetFrequency;
	}

	public void setFloatingLegInterestAccrualResetFrequency(
			String floatingLegInterestAccrualResetFrequency) {
		this.floatingLegInterestAccrualResetFrequency = floatingLegInterestAccrualResetFrequency;
	}

	public String getFloatingLegInterestAccrualMethod() {
		return floatingLegInterestAccrualMethod;
	}

	public void setFloatingLegInterestAccrualMethod(
			String floatingLegInterestAccrualMethod) {
		this.floatingLegInterestAccrualMethod = floatingLegInterestAccrualMethod;
	}

	public String getFloatingLegDayCount() {
		return floatingLegDayCount;
	}

	public void setFloatingLegDayCount(String floatingLegDayCount) {
		this.floatingLegDayCount = floatingLegDayCount;
	}

	public String getFixedPartyID() {
		return fixedPartyID;
	}

	public void setFixedPartyID(String fixedPartyID) {
		this.fixedPartyID = fixedPartyID;
	}

	public int getFixedPartyRole() {
		return fixedPartyRole;
	}

	public void setFixedPartyRole(int fixedPartyRole) {
		this.fixedPartyRole = fixedPartyRole;
	}

	public String getFixedTel() {
		return fixedTel;
	}

	public void setFixedTel(String fixedTel) {
		this.fixedTel = fixedTel;
	}

	public String getFixedFax() {
		return fixedFax;
	}

	public void setFixedFax(String fixedFax) {
		this.fixedFax = fixedFax;
	}

	public String getFixedUser() {
		return fixedUser;
	}

	public void setFixedUser(String fixedUser) {
		this.fixedUser = fixedUser;
	}

	public String getFixedBrhName() {
		return fixedBrhName;
	}

	public void setFixedBrhName(String fixedBrhName) {
		this.fixedBrhName = fixedBrhName;
	}

	public String getFixedAccount() {
		return fixedAccount;
	}

	public void setFixedAccount(String fixedAccount) {
		this.fixedAccount = fixedAccount;
	}

	public String getFixedAccountName() {
		return fixedAccountName;
	}

	public void setFixedAccountName(String fixedAccountName) {
		this.fixedAccountName = fixedAccountName;
	}

	public String getFixedCapitalBankName() {
		return fixedCapitalBankName;
	}

	public void setFixedCapitalBankName(String fixedCapitalBankName) {
		this.fixedCapitalBankName = fixedCapitalBankName;
	}

	public String getFixedPayBankNo() {
		return fixedPayBankNo;
	}

	public void setFixedPayBankNo(String fixedPayBankNo) {
		this.fixedPayBankNo = fixedPayBankNo;
	}

	public String getFloatingPartyID() {
		return floatingPartyID;
	}

	public void setFloatingPartyID(String floatingPartyID) {
		this.floatingPartyID = floatingPartyID;
	}

	public int getFloatingPartyRole() {
		return floatingPartyRole;
	}

	public void setFloatingPartyRole(int floatingPartyRole) {
		this.floatingPartyRole = floatingPartyRole;
	}

	public String getFloatingTel() {
		return floatingTel;
	}

	public void setFloatingTel(String floatingTel) {
		this.floatingTel = floatingTel;
	}

	public String getFloatingFax() {
		return floatingFax;
	}

	public void setFloatingFax(String floatingFax) {
		this.floatingFax = floatingFax;
	}

	public String getFloatingUser() {
		return floatingUser;
	}

	public void setFloatingUser(String floatingUser) {
		this.floatingUser = floatingUser;
	}

	public String getFloatingBrhName() {
		return floatingBrhName;
	}

	public void setFloatingBrhName(String floatingBrhName) {
		this.floatingBrhName = floatingBrhName;
	}

	public String getFloatingAccount() {
		return floatingAccount;
	}

	public void setFloatingAccount(String floatingAccount) {
		this.floatingAccount = floatingAccount;
	}

	public String getFloatingAccountName() {
		return floatingAccountName;
	}

	public void setFloatingAccountName(String floatingAccountName) {
		this.floatingAccountName = floatingAccountName;
	}

	public String getFloatingCapitalBankName() {
		return floatingCapitalBankName;
	}

	public void setFloatingCapitalBankName(String floatingCapitalBankName) {
		this.floatingCapitalBankName = floatingCapitalBankName;
	}

	public String getFloatingPayBankNo() {
		return floatingPayBankNo;
	}

	public void setFloatingPayBankNo(String floatingPayBankNo) {
		this.floatingPayBankNo = floatingPayBankNo;
	}

}
