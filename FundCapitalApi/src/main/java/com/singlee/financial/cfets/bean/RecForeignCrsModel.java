package com.singlee.financial.cfets.bean;

/**
 * CRS
 * @author 
 *
 */
public class RecForeignCrsModel extends RecMsgBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  confirmid                   ;  //确认报文ID
	private String  execId;
	private String  execType;
	private String  confirmStatus;
	private String  marketIndicator              ;  //市场标识 17-货币掉期
	private String  tradedate                   ;  //交易日期
	private String  settldate                   ;   //生效日-清算日期
	private String  netGrossInd                 ;	//清算方式
	private String  maturitydate                ;	//到期日
	private String  dateadjustmentindic         ;   //起息日调整
	private String  iniexdate                   ;	//期初本金交换日
	private String  finalexdate                 ;	//期末本金交换日
	private String  calculateagency             ;	//计算机构 0-中国外汇交易中心
	private String  notionalexchangetype        ;	//本金交换形式 0-期初期末均交换 1-期初交换期末不交换 2-期初不交换期末交换 3-期初期末均不交换
	private String  lastspotrate                ;	//即期汇率
	private String  subtype                     ;	
	private String  text                        ;	//备注
	private String  legcurrency_1               ;	//本方货币代码
	private String  legcouponpaymentdate_1      ;	//本方首次付息日
	private String  legorderqty_1               ;	//本方名义本金
	private String  legpricetype_1              ;	//本方利率类型 3-固定利率 6-浮动利率
	private String  legbenchmarkcurvename_1     ;	//本方适用利率
	private String  legbenchmarktenor_1         ;	//本方利率期限 ON-O/N;7D
	private String  legbenchmarkspread_1        ;	//本方利差
	private String  legprice_1                  ;	//本方固定利率
	private String  legintaccresetfrequency_1   ;	//本方定息周期 1W-1W
	private String  legintfixdateadjustment_1   ;	//本方定息规则 0-V_0;1-V_1;2-V_2
	private String  legdaycount_1               ;	//本方计息基准 0-A/A;1-A/360;2-30/360;3-A/365;5-A/365F
	private String  legcouponpaymentfrequency_1 ;	//本方计息周期 1W-1W
	private String  legcouponpaymentdatereset_1 ;	//本方付息日调整规则 0-上一营业日;1-下一营业日;2经调整的下一营业日
	private String  legstubindicator_1          ;	//本方残段标识 1-前置残段标识;2-后置残段标识;3-前置和后置残段标识
	private String  leg1_paymentamt             ;	//本方摊销金额
	private String  leg1_paymentcurrency        ;	//本方摊销币种
	private String  leg1_altsettldate           ;	//本方摊销计息开始日
	private String  leg1_altsettldate2          ;	//本方摊销计息终止日
	private String  leg1_paydate                ;	//本方摊销付息日期
	private String  legcurrency_2               ;	//对手方货币代码
	private String  legcouponpaymentdate_2      ;	//对手方首次付息日期
	private String  legorderqty_2               ;	//对手方名义本金
	private String  legpricetype_2              ;	//对手方利率类型 3-固定利率 6-浮动利率
	private String  legbenchmarkcurvename_2     ;	//对手方适用利率
	private String  legbenchmarktenor_2         ;	//对手方利率期限 ON-O/N;7D-7D
	private String  legbenchmarkspread_2        ;	//对手方利差
	private String  legprice_2                  ;	//对手方固定利率
	private String  legintaccresetfrequency_2   ;	//对手方定息周期 1W-1W
	private String  legintfixdateadjustment_2   ;	//对手方定息规则 0-V_0;1-V_1;2-V_2
	private String  legdaycount_2               ;	//对手方计息基准 0-A/A;1-A/360;2-30/360;3-A/365;5-A/365F
	private String  legcouponpaymentfrequency_2 ;	//对手方计息周期 1W-1W
	private String  legcouponpaymentdatereset_2 ;	//对手方付息日调整规则 0-上一营业日;1-下一营业日;2经调整的下一营业日
	private String  legstubindicator_2          ;	//对手方残段标识 1-前置残段标识;2-后置残段标识;3-前置和后置残段标识
	private String  leg2_paymentamt             ;	//对手方摊销金额
	private String  leg2_paymentcurrency        ;	//对手方摊销币种	
	private String  leg2_altsettldate           ;	//对手方摊销计息开始日期
	private String  leg2_altsettldate2          ;  //对手方摊销计息终止日
	private String  leg2_paydate                ;//对手方摊销付息日期
	private String ownCnfmTime; // 本方确认时间
	private String othCnfmTime; // 对方确认时间
	private String  ownCfetsNo;	//本方21位机构号
	private String  partyCfetsNo;	//对手方21位机构号
	private String  bankName;	 //本方近端 收款行名称   /资金账户户名  
	private String  bankOpenNo;	//本方近端 收款行SWIFTCODE / 
	private String  bankAcctNo;	//本方近端 收款行账号  /资金账号
	private String  acctName;	//本方近端 开户行名称  /资金开户名称 
	private String  acctOpenNo;	//本方近端 开户行SWIFT CODE /  
	private String  acctNo;	//本方近端 开户行账号   /支付系统行号 
	private String  intermeDiaryBankName;	 //本方近端 中间行名称
	private String  intermeDiaryBankBicCode;	 //本方近端 中间行SWIFT CODE/中间行行号
	private String  intermeDiaryBankAcctNo;	 //本方近端 中间行在开户行的资金账号
	private String  remark;	 //本方近端 付言
	private String  farBankName;	//本方远端 收款行名称   /资金账户户名
	private String  farBankOpenNo;	//本方远端 收款行SWIFTCODE 
	private String  farBankAcctNo;	//本方远端 款行账号  /资金账号
	private String  farAcctName;	//本方远端 开户行名称  /资金开户名称 
	private String  farAcctOpenNo;	//本方远端资 金开户行SWIFT CODE 
	private String  farAcctNo;	//本方远端 开户行账号   /支付系统行号
	private String  farIntermeDiaryBankName;	 //本方远端 中间行名称-209
	private String  farIntermeDiaryBankBicCode;	 //本方远端 中间行SWIFT CODE/中间行行号-211
	private String  farIntermeDiaryBankAcctNo;	 //本方远端 中间行在开户行的资金账号-213
	private String  farRemark;	 //本方远端 付言-139
	private String  c_settCcy;	//对手方清算币种
	private String  c_bankName;	//对手方近端 收款行名称   /资金账户户名 
	private String  c_bankOpenNo;	//对手方近端 收款行SWIFTCODE
	private String  c_bankAcctNo;	//对手方近端 款行账号  /资金账号
	private String  c_acctName;	//对手方近端 开户行名称  /资金开户名称
	private String  c_acctOpenNo;	//对手方近端 金开户行SWIFT CODE
	private String  c_acctNo;	//对手方近端 开户行账号   /支付系统行号
	private String  c_intermeDiaryBankName;	//对手方近端 中间行名称-209
	private String  c_intermeDiaryBankBicCode;	//对手方近端 中间行SWIFT CODE/中间行行号-211
	private String  c_intermeDiaryBankAcctNo;	//对手方近端 中间行在开户行的资金账号-213
	private String  c_remark;	//对手方近端 付言-139
	private String  c_farBankName;	//对手方远端 收款行名称   /资金账户户名 
	private String  c_farBankOpenNo;	//对手方远端 收款行SWIFTCODE
	private String  c_farBankAcctNo;	//对手方远端 款行账号  /资金账号
	private String  c_farAcctName;	//对手方远端 开户行名称  /资金开户名称
	private String  c_farAcctOpenNo;	//对手方远端 金开户行SWIFT CODE
	private String  c_farAcctNo;	//对手方远端 开户行账号   /支付系统行号
	private String  c_farIntermeDiaryBankName;	//对手方远端 中间行名称-209
	private String  c_farIntermeDiaryBankBicCode;	//对手方远端 中间行SWIFT CODE/中间行行号-211
	private String  c_farIntermeDiaryBankAcctNo;	//对手方远端 中间行在开户行的资金账号-213
	private String  c_farRemark;	//对手方远端 付言-139
	private String  partyName;	//对手方机构中文全称
	private String  partyEngName;	//对手方机构英文全称
	private String ownName; //本方中文全称
	private String  settCcy;	//本方清算币种
	public String getConfirmid() {
		return confirmid;
	}
	public void setConfirmid(String confirmid) {
		this.confirmid = confirmid;
	}
	public String getExecId() {
		return execId;
	}
	public void setExecId(String execId) {
		this.execId = execId;
	}
	public String getExecType() {
		return execType;
	}
	public void setExecType(String execType) {
		this.execType = execType;
	}
	public String getConfirmStatus() {
		return confirmStatus;
	}
	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}
	public String getMarketIndicator() {
		return marketIndicator;
	}
	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}
	public String getTradedate() {
		return tradedate;
	}
	public void setTradedate(String tradedate) {
		this.tradedate = tradedate;
	}
	public String getSettldate() {
		return settldate;
	}
	public void setSettldate(String settldate) {
		this.settldate = settldate;
	}
	public String getNetGrossInd() {
		return netGrossInd;
	}
	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}
	public String getMaturitydate() {
		return maturitydate;
	}
	public void setMaturitydate(String maturitydate) {
		this.maturitydate = maturitydate;
	}
	public String getDateadjustmentindic() {
		return dateadjustmentindic;
	}
	public void setDateadjustmentindic(String dateadjustmentindic) {
		this.dateadjustmentindic = dateadjustmentindic;
	}
	public String getIniexdate() {
		return iniexdate;
	}
	public void setIniexdate(String iniexdate) {
		this.iniexdate = iniexdate;
	}
	public String getFinalexdate() {
		return finalexdate;
	}
	public void setFinalexdate(String finalexdate) {
		this.finalexdate = finalexdate;
	}
	public String getCalculateagency() {
		return calculateagency;
	}
	public void setCalculateagency(String calculateagency) {
		this.calculateagency = calculateagency;
	}
	public String getNotionalexchangetype() {
		return notionalexchangetype;
	}
	public void setNotionalexchangetype(String notionalexchangetype) {
		this.notionalexchangetype = notionalexchangetype;
	}
	public String getLastspotrate() {
		return lastspotrate;
	}
	public void setLastspotrate(String lastspotrate) {
		this.lastspotrate = lastspotrate;
	}
	public String getSubtype() {
		return subtype;
	}
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLegcurrency_1() {
		return legcurrency_1;
	}
	public void setLegcurrency_1(String legcurrency_1) {
		this.legcurrency_1 = legcurrency_1;
	}
	public String getLegcouponpaymentdate_1() {
		return legcouponpaymentdate_1;
	}
	public void setLegcouponpaymentdate_1(String legcouponpaymentdate_1) {
		this.legcouponpaymentdate_1 = legcouponpaymentdate_1;
	}
	public String getLegorderqty_1() {
		return legorderqty_1;
	}
	public void setLegorderqty_1(String legorderqty_1) {
		this.legorderqty_1 = legorderqty_1;
	}
	public String getLegpricetype_1() {
		return legpricetype_1;
	}
	public void setLegpricetype_1(String legpricetype_1) {
		this.legpricetype_1 = legpricetype_1;
	}
	public String getLegbenchmarkcurvename_1() {
		return legbenchmarkcurvename_1;
	}
	public void setLegbenchmarkcurvename_1(String legbenchmarkcurvename_1) {
		this.legbenchmarkcurvename_1 = legbenchmarkcurvename_1;
	}
	public String getLegbenchmarktenor_1() {
		return legbenchmarktenor_1;
	}
	public void setLegbenchmarktenor_1(String legbenchmarktenor_1) {
		this.legbenchmarktenor_1 = legbenchmarktenor_1;
	}
	public String getLegbenchmarkspread_1() {
		return legbenchmarkspread_1;
	}
	public void setLegbenchmarkspread_1(String legbenchmarkspread_1) {
		this.legbenchmarkspread_1 = legbenchmarkspread_1;
	}
	public String getLegprice_1() {
		return legprice_1;
	}
	public void setLegprice_1(String legprice_1) {
		this.legprice_1 = legprice_1;
	}
	public String getLegintaccresetfrequency_1() {
		return legintaccresetfrequency_1;
	}
	public void setLegintaccresetfrequency_1(String legintaccresetfrequency_1) {
		this.legintaccresetfrequency_1 = legintaccresetfrequency_1;
	}
	public String getLegintfixdateadjustment_1() {
		return legintfixdateadjustment_1;
	}
	public void setLegintfixdateadjustment_1(String legintfixdateadjustment_1) {
		this.legintfixdateadjustment_1 = legintfixdateadjustment_1;
	}
	public String getLegdaycount_1() {
		return legdaycount_1;
	}
	public void setLegdaycount_1(String legdaycount_1) {
		this.legdaycount_1 = legdaycount_1;
	}
	public String getLegcouponpaymentfrequency_1() {
		return legcouponpaymentfrequency_1;
	}
	public void setLegcouponpaymentfrequency_1(String legcouponpaymentfrequency_1) {
		this.legcouponpaymentfrequency_1 = legcouponpaymentfrequency_1;
	}
	public String getLegcouponpaymentdatereset_1() {
		return legcouponpaymentdatereset_1;
	}
	public void setLegcouponpaymentdatereset_1(String legcouponpaymentdatereset_1) {
		this.legcouponpaymentdatereset_1 = legcouponpaymentdatereset_1;
	}
	public String getLegstubindicator_1() {
		return legstubindicator_1;
	}
	public void setLegstubindicator_1(String legstubindicator_1) {
		this.legstubindicator_1 = legstubindicator_1;
	}
	public String getLeg1_paymentamt() {
		return leg1_paymentamt;
	}
	public void setLeg1_paymentamt(String leg1_paymentamt) {
		this.leg1_paymentamt = leg1_paymentamt;
	}
	public String getLeg1_paymentcurrency() {
		return leg1_paymentcurrency;
	}
	public void setLeg1_paymentcurrency(String leg1_paymentcurrency) {
		this.leg1_paymentcurrency = leg1_paymentcurrency;
	}
	public String getLeg1_altsettldate() {
		return leg1_altsettldate;
	}
	public void setLeg1_altsettldate(String leg1_altsettldate) {
		this.leg1_altsettldate = leg1_altsettldate;
	}
	public String getLeg1_altsettldate2() {
		return leg1_altsettldate2;
	}
	public void setLeg1_altsettldate2(String leg1_altsettldate2) {
		this.leg1_altsettldate2 = leg1_altsettldate2;
	}
	public String getLeg1_paydate() {
		return leg1_paydate;
	}
	public void setLeg1_paydate(String leg1_paydate) {
		this.leg1_paydate = leg1_paydate;
	}
	public String getLegcurrency_2() {
		return legcurrency_2;
	}
	public void setLegcurrency_2(String legcurrency_2) {
		this.legcurrency_2 = legcurrency_2;
	}
	public String getLegcouponpaymentdate_2() {
		return legcouponpaymentdate_2;
	}
	public void setLegcouponpaymentdate_2(String legcouponpaymentdate_2) {
		this.legcouponpaymentdate_2 = legcouponpaymentdate_2;
	}
	public String getLegorderqty_2() {
		return legorderqty_2;
	}
	public void setLegorderqty_2(String legorderqty_2) {
		this.legorderqty_2 = legorderqty_2;
	}
	public String getLegpricetype_2() {
		return legpricetype_2;
	}
	public void setLegpricetype_2(String legpricetype_2) {
		this.legpricetype_2 = legpricetype_2;
	}
	public String getLegbenchmarkcurvename_2() {
		return legbenchmarkcurvename_2;
	}
	public void setLegbenchmarkcurvename_2(String legbenchmarkcurvename_2) {
		this.legbenchmarkcurvename_2 = legbenchmarkcurvename_2;
	}
	public String getLegbenchmarktenor_2() {
		return legbenchmarktenor_2;
	}
	public void setLegbenchmarktenor_2(String legbenchmarktenor_2) {
		this.legbenchmarktenor_2 = legbenchmarktenor_2;
	}
	public String getLegbenchmarkspread_2() {
		return legbenchmarkspread_2;
	}
	public void setLegbenchmarkspread_2(String legbenchmarkspread_2) {
		this.legbenchmarkspread_2 = legbenchmarkspread_2;
	}
	public String getLegprice_2() {
		return legprice_2;
	}
	public void setLegprice_2(String legprice_2) {
		this.legprice_2 = legprice_2;
	}
	public String getLegintaccresetfrequency_2() {
		return legintaccresetfrequency_2;
	}
	public void setLegintaccresetfrequency_2(String legintaccresetfrequency_2) {
		this.legintaccresetfrequency_2 = legintaccresetfrequency_2;
	}
	public String getLegintfixdateadjustment_2() {
		return legintfixdateadjustment_2;
	}
	public void setLegintfixdateadjustment_2(String legintfixdateadjustment_2) {
		this.legintfixdateadjustment_2 = legintfixdateadjustment_2;
	}
	public String getLegdaycount_2() {
		return legdaycount_2;
	}
	public void setLegdaycount_2(String legdaycount_2) {
		this.legdaycount_2 = legdaycount_2;
	}
	public String getLegcouponpaymentfrequency_2() {
		return legcouponpaymentfrequency_2;
	}
	public void setLegcouponpaymentfrequency_2(String legcouponpaymentfrequency_2) {
		this.legcouponpaymentfrequency_2 = legcouponpaymentfrequency_2;
	}
	public String getLegcouponpaymentdatereset_2() {
		return legcouponpaymentdatereset_2;
	}
	public void setLegcouponpaymentdatereset_2(String legcouponpaymentdatereset_2) {
		this.legcouponpaymentdatereset_2 = legcouponpaymentdatereset_2;
	}
	public String getLegstubindicator_2() {
		return legstubindicator_2;
	}
	public void setLegstubindicator_2(String legstubindicator_2) {
		this.legstubindicator_2 = legstubindicator_2;
	}
	public String getLeg2_paymentamt() {
		return leg2_paymentamt;
	}
	public void setLeg2_paymentamt(String leg2_paymentamt) {
		this.leg2_paymentamt = leg2_paymentamt;
	}
	public String getLeg2_paymentcurrency() {
		return leg2_paymentcurrency;
	}
	public void setLeg2_paymentcurrency(String leg2_paymentcurrency) {
		this.leg2_paymentcurrency = leg2_paymentcurrency;
	}
	public String getLeg2_altsettldate() {
		return leg2_altsettldate;
	}
	public void setLeg2_altsettldate(String leg2_altsettldate) {
		this.leg2_altsettldate = leg2_altsettldate;
	}
	public String getLeg2_altsettldate2() {
		return leg2_altsettldate2;
	}
	public void setLeg2_altsettldate2(String leg2_altsettldate2) {
		this.leg2_altsettldate2 = leg2_altsettldate2;
	}
	public String getLeg2_paydate() {
		return leg2_paydate;
	}
	public void setLeg2_paydate(String leg2_paydate) {
		this.leg2_paydate = leg2_paydate;
	}
	public String getOwnCnfmTime() {
		return ownCnfmTime;
	}
	public void setOwnCnfmTime(String ownCnfmTime) {
		this.ownCnfmTime = ownCnfmTime;
	}
	public String getOthCnfmTime() {
		return othCnfmTime;
	}
	public void setOthCnfmTime(String othCnfmTime) {
		this.othCnfmTime = othCnfmTime;
	}
	public String getOwnCfetsNo() {
		return ownCfetsNo;
	}
	public void setOwnCfetsNo(String ownCfetsNo) {
		this.ownCfetsNo = ownCfetsNo;
	}
	public String getPartyCfetsNo() {
		return partyCfetsNo;
	}
	public void setPartyCfetsNo(String partyCfetsNo) {
		this.partyCfetsNo = partyCfetsNo;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankOpenNo() {
		return bankOpenNo;
	}
	public void setBankOpenNo(String bankOpenNo) {
		this.bankOpenNo = bankOpenNo;
	}
	public String getBankAcctNo() {
		return bankAcctNo;
	}
	public void setBankAcctNo(String bankAcctNo) {
		this.bankAcctNo = bankAcctNo;
	}
	public String getAcctName() {
		return acctName;
	}
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	public String getAcctOpenNo() {
		return acctOpenNo;
	}
	public void setAcctOpenNo(String acctOpenNo) {
		this.acctOpenNo = acctOpenNo;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getIntermeDiaryBankName() {
		return intermeDiaryBankName;
	}
	public void setIntermeDiaryBankName(String intermeDiaryBankName) {
		this.intermeDiaryBankName = intermeDiaryBankName;
	}
	public String getIntermeDiaryBankBicCode() {
		return intermeDiaryBankBicCode;
	}
	public void setIntermeDiaryBankBicCode(String intermeDiaryBankBicCode) {
		this.intermeDiaryBankBicCode = intermeDiaryBankBicCode;
	}
	public String getIntermeDiaryBankAcctNo() {
		return intermeDiaryBankAcctNo;
	}
	public void setIntermeDiaryBankAcctNo(String intermeDiaryBankAcctNo) {
		this.intermeDiaryBankAcctNo = intermeDiaryBankAcctNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getFarBankName() {
		return farBankName;
	}
	public void setFarBankName(String farBankName) {
		this.farBankName = farBankName;
	}
	public String getFarBankOpenNo() {
		return farBankOpenNo;
	}
	public void setFarBankOpenNo(String farBankOpenNo) {
		this.farBankOpenNo = farBankOpenNo;
	}
	public String getFarBankAcctNo() {
		return farBankAcctNo;
	}
	public void setFarBankAcctNo(String farBankAcctNo) {
		this.farBankAcctNo = farBankAcctNo;
	}
	public String getFarAcctName() {
		return farAcctName;
	}
	public void setFarAcctName(String farAcctName) {
		this.farAcctName = farAcctName;
	}
	public String getFarAcctOpenNo() {
		return farAcctOpenNo;
	}
	public void setFarAcctOpenNo(String farAcctOpenNo) {
		this.farAcctOpenNo = farAcctOpenNo;
	}
	public String getFarAcctNo() {
		return farAcctNo;
	}
	public void setFarAcctNo(String farAcctNo) {
		this.farAcctNo = farAcctNo;
	}
	public String getFarIntermeDiaryBankName() {
		return farIntermeDiaryBankName;
	}
	public void setFarIntermeDiaryBankName(String farIntermeDiaryBankName) {
		this.farIntermeDiaryBankName = farIntermeDiaryBankName;
	}
	public String getFarIntermeDiaryBankBicCode() {
		return farIntermeDiaryBankBicCode;
	}
	public void setFarIntermeDiaryBankBicCode(String farIntermeDiaryBankBicCode) {
		this.farIntermeDiaryBankBicCode = farIntermeDiaryBankBicCode;
	}
	public String getFarIntermeDiaryBankAcctNo() {
		return farIntermeDiaryBankAcctNo;
	}
	public void setFarIntermeDiaryBankAcctNo(String farIntermeDiaryBankAcctNo) {
		this.farIntermeDiaryBankAcctNo = farIntermeDiaryBankAcctNo;
	}
	public String getFarRemark() {
		return farRemark;
	}
	public void setFarRemark(String farRemark) {
		this.farRemark = farRemark;
	}
	public String getC_settCcy() {
		return c_settCcy;
	}
	public void setC_settCcy(String c_settCcy) {
		this.c_settCcy = c_settCcy;
	}
	public String getC_bankName() {
		return c_bankName;
	}
	public void setC_bankName(String c_bankName) {
		this.c_bankName = c_bankName;
	}
	public String getC_bankOpenNo() {
		return c_bankOpenNo;
	}
	public void setC_bankOpenNo(String c_bankOpenNo) {
		this.c_bankOpenNo = c_bankOpenNo;
	}
	public String getC_bankAcctNo() {
		return c_bankAcctNo;
	}
	public void setC_bankAcctNo(String c_bankAcctNo) {
		this.c_bankAcctNo = c_bankAcctNo;
	}
	public String getC_acctName() {
		return c_acctName;
	}
	public void setC_acctName(String c_acctName) {
		this.c_acctName = c_acctName;
	}
	public String getC_acctOpenNo() {
		return c_acctOpenNo;
	}
	public void setC_acctOpenNo(String c_acctOpenNo) {
		this.c_acctOpenNo = c_acctOpenNo;
	}
	public String getC_acctNo() {
		return c_acctNo;
	}
	public void setC_acctNo(String c_acctNo) {
		this.c_acctNo = c_acctNo;
	}
	public String getC_intermeDiaryBankName() {
		return c_intermeDiaryBankName;
	}
	public void setC_intermeDiaryBankName(String c_intermeDiaryBankName) {
		this.c_intermeDiaryBankName = c_intermeDiaryBankName;
	}
	public String getC_intermeDiaryBankBicCode() {
		return c_intermeDiaryBankBicCode;
	}
	public void setC_intermeDiaryBankBicCode(String c_intermeDiaryBankBicCode) {
		this.c_intermeDiaryBankBicCode = c_intermeDiaryBankBicCode;
	}
	public String getC_intermeDiaryBankAcctNo() {
		return c_intermeDiaryBankAcctNo;
	}
	public void setC_intermeDiaryBankAcctNo(String c_intermeDiaryBankAcctNo) {
		this.c_intermeDiaryBankAcctNo = c_intermeDiaryBankAcctNo;
	}
	public String getC_remark() {
		return c_remark;
	}
	public void setC_remark(String c_remark) {
		this.c_remark = c_remark;
	}
	public String getC_farBankName() {
		return c_farBankName;
	}
	public void setC_farBankName(String c_farBankName) {
		this.c_farBankName = c_farBankName;
	}
	public String getC_farBankOpenNo() {
		return c_farBankOpenNo;
	}
	public void setC_farBankOpenNo(String c_farBankOpenNo) {
		this.c_farBankOpenNo = c_farBankOpenNo;
	}
	public String getC_farBankAcctNo() {
		return c_farBankAcctNo;
	}
	public void setC_farBankAcctNo(String c_farBankAcctNo) {
		this.c_farBankAcctNo = c_farBankAcctNo;
	}
	public String getC_farAcctName() {
		return c_farAcctName;
	}
	public void setC_farAcctName(String c_farAcctName) {
		this.c_farAcctName = c_farAcctName;
	}
	public String getC_farAcctOpenNo() {
		return c_farAcctOpenNo;
	}
	public void setC_farAcctOpenNo(String c_farAcctOpenNo) {
		this.c_farAcctOpenNo = c_farAcctOpenNo;
	}
	public String getC_farAcctNo() {
		return c_farAcctNo;
	}
	public void setC_farAcctNo(String c_farAcctNo) {
		this.c_farAcctNo = c_farAcctNo;
	}
	public String getC_farIntermeDiaryBankName() {
		return c_farIntermeDiaryBankName;
	}
	public void setC_farIntermeDiaryBankName(String c_farIntermeDiaryBankName) {
		this.c_farIntermeDiaryBankName = c_farIntermeDiaryBankName;
	}
	public String getC_farIntermeDiaryBankBicCode() {
		return c_farIntermeDiaryBankBicCode;
	}
	public void setC_farIntermeDiaryBankBicCode(String c_farIntermeDiaryBankBicCode) {
		this.c_farIntermeDiaryBankBicCode = c_farIntermeDiaryBankBicCode;
	}
	public String getC_farIntermeDiaryBankAcctNo() {
		return c_farIntermeDiaryBankAcctNo;
	}
	public void setC_farIntermeDiaryBankAcctNo(String c_farIntermeDiaryBankAcctNo) {
		this.c_farIntermeDiaryBankAcctNo = c_farIntermeDiaryBankAcctNo;
	}
	public String getC_farRemark() {
		return c_farRemark;
	}
	public void setC_farRemark(String c_farRemark) {
		this.c_farRemark = c_farRemark;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getPartyEngName() {
		return partyEngName;
	}
	public void setPartyEngName(String partyEngName) {
		this.partyEngName = partyEngName;
	}
	public String getOwnName() {
		return ownName;
	}
	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}
	public String getSettCcy() {
		return settCcy;
	}
	public void setSettCcy(String settCcy) {
		this.settCcy = settCcy;
	}
	
	
	
}
