package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.RecForeignSwapModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.List;

@Capital(context = CapitalContext.SINGLEE_API,uri = "/cpisFxSwpTradeServiceExporter")
public interface ICpisFxSwpTradeService {
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(RecForeignSwapModel model) throws  Exception;
	
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(List<RecForeignSwapModel> model) throws  Exception;
}
