package com.singlee.financial.cfets.bean;

import java.io.Serializable;

/**
 * 该对象主要用于交易后确认
 * 
 * @author shenzl
 *
 */
public class CfetsPackMsgBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6888766403077282221L;
	// 成交编号
	private String execid;
	// 成交日期
	private String tradedate;
	// 报文协议
	private String beginstring;
	// 成交状态
	private String dealtranstype;
	// 市场标识
	private String marketindicator;
	// 报文类型
	private String msgtype;
	// 报文类型
	private String packmsg;

	public String getExecid() {
		return execid;
	}

	public void setExecid(String execid) {
		this.execid = execid;
	}

	public String getTradedate() {
		return tradedate;
	}

	public void setTradedate(String tradedate) {
		this.tradedate = tradedate;
	}

	public String getBeginstring() {
		return beginstring;
	}

	public void setBeginstring(String beginstring) {
		this.beginstring = beginstring;
	}

	public String getDealtranstype() {
		return dealtranstype;
	}

	public void setDealtranstype(String dealtranstype) {
		this.dealtranstype = dealtranstype;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getPackmsg() {
		return packmsg;
	}

	public void setPackmsg(String packmsg) {
		this.packmsg = packmsg;
	}

	public String getMarketindicator() {
		return marketindicator;
	}

	public void setMarketindicator(String marketindicator) {
		this.marketindicator = marketindicator;
	}

}
