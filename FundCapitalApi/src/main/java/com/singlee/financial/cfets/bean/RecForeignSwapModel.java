package com.singlee.financial.cfets.bean;


/**
 * <p>接收外币掉期消息实体类  </P>
 * 
 * <P>company:新利  </P>
 * 
 * @author Administrator
 *
 */
public class RecForeignSwapModel extends RecMsgBean {

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String execID; //成交编号
	
	private	String tradeDate;//成交日期
			
	private	String execType;//成交状态
	
	private	String dealTransType;//0-成交单录入
			
	private	String confirmID; //确认报文ID
	
	private	String confirmStatus;//确认状态

	private	String netGrossInd;//清算方式
		
	private	String marketIndicator;//交易类型
	
	private	String ownCnfmTime; //本方确认时间

	private	String othCnfmTime; //对手方确认时间
		
	private	String nearEndValueDate; //近端起息日， 格式为：YYYYMMDD
	
	private	String nearEndPrice; //近端成交价格
	
	private	String nearEndSellAmount; //近端卖出金额
	
	private	String nearEndBuyAmount; //近端买入金额
	
	private	String nearEndSellCcy; //近端卖出币种
		
	private	String nearEndBuyCcy; //近端卖出币种
	
	private	String farEndValueDate; //近端起息日， 格式为：YYYYMMDD
	
	private	String farEndPrice; //近端成交价格
		
	private	String farEndSellAmount; //近端卖出金额
	
	private	String farEndBuyAmount; //近端买入金额
	
	private	String farEndSellCcy; //近端卖出币种
	
	private String farEndBuyCcy; //近端卖出币种
	
	private	String ownPartyID; //本方机构 21 位码
	
	private	String othPartyID;//对手方机构 21 位码
	
	private	String ownCnName; //中文全称
	
	private String nearEndOwnCapitalBakeName; //近端资金开户行
	
	private String nearEndOwnCapitalAccountName; //近端资金账户户名
	
	private String nearEndOwnPayBankNo;//近端支付系统行号
	
	private	String nearEndOwnCapitalAccount; //近端资金账号
	
	private String nearEndOwnRemark; //近端附言
	
	private	String farEndMidCnName; //远端中间行名称
	
	private	String farEndMidBankBIC; //远端中间行 BICCODE
	
	private	String farEndMidBankAccount; //远端中间行账号
	
	private String farEndOwnCapitalBakeName; //远端开户行名称
	
	
	private String farEndOwnCapitalAccountBIC; //远端开户行 BICCODE
	
	private	String farEndOwnCapitalAccount; //远端开户行账号
	
	private	String farEndOwnReceiptBankName; //远端收款行名称
	
	private String farEndOwnReceiptBankBIC; //远端收款行BIC
	
	private String farEndOwnReceiptBankAccount; //远端收款行账号
	
	private String farEndOwnRemark; //远端附言
	
	
	private	String othCnName; //中文全称
	
	private	String farEndOthCapitalBakeName; //远端资金开户行
	
	private String farEndOthCapitalAccountName; //远端资金账户户名
	
	private String farEndOthPayBankNo; //远端支付系统行号
	
	private String farEndOthCapitalAccount; //远端资金账号
	
	private String farEndOthRemark; //远端附言
	
	private String nearEndMidCnName; //近端中间行名称
	
	private String nearEndMidBankBIC; //近端中间行 BICCODE
	
	private String nearEndMidBankAccount; //近端中间行账号
	
	private	String nearEndOthCapitalBakeName; //近端开户行名称
	
	private String nearEndOthCapitalAccountBIC; //近端开户行 BICCODE
	
	private	String nearEndOthCapitalAccount; //近端开户行账号
	
	private	String nearEndOthReceiptBankName; //近端收款行名称
	
	private	String nearEndOthReceiptBankBIC; //近端收款行BIC
	
	private String nearEndOthReceiptAccount; //近端收款行账号
	
	private String nearEndOthRemark; //近端附言
	
	private String securityID;//货币对
	
	private String legSide_1;
	
	private String legSide_2;

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getDealTransType() {
		return dealTransType;
	}

	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

	public String getConfirmStatus() {
		return confirmStatus;
	}

	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}

	public String getNetGrossInd() {
		return netGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}


	public String getOwnCnfmTime() {
		return ownCnfmTime;
	}

	public void setOwnCnfmTime(String ownCnfmTime) {
		this.ownCnfmTime = ownCnfmTime;
	}

	
	public String getOthCnfmTime() {
		return othCnfmTime;
	}

	public void setOthCnfmTime(String othCnfmTime) {
		this.othCnfmTime = othCnfmTime;
	}

	public String getNearEndValueDate() {
		return nearEndValueDate;
	}

	public void setNearEndValueDate(String nearEndValueDate) {
		this.nearEndValueDate = nearEndValueDate;
	}

	public String getNearEndPrice() {
		return nearEndPrice;
	}

	public void setNearEndPrice(String nearEndPrice) {
		this.nearEndPrice = nearEndPrice;
	}

	public String getNearEndSellAmount() {
		return nearEndSellAmount;
	}

	public void setNearEndSellAmount(String nearEndSellAmount) {
		this.nearEndSellAmount = nearEndSellAmount;
	}

	public String getNearEndBuyAmount() {
		return nearEndBuyAmount;
	}

	public void setNearEndBuyAmount(String nearEndBuyAmount) {
		this.nearEndBuyAmount = nearEndBuyAmount;
	}

	public String getNearEndSellCcy() {
		return nearEndSellCcy;
	}

	public void setNearEndSellCcy(String nearEndSellCcy) {
		this.nearEndSellCcy = nearEndSellCcy;
	}

	public String getNearEndBuyCcy() {
		return nearEndBuyCcy;
	}

	public void setNearEndBuyCcy(String nearEndBuyCcy) {
		this.nearEndBuyCcy = nearEndBuyCcy;
	}

	public String getFarEndValueDate() {
		return farEndValueDate;
	}

	public void setFarEndValueDate(String farEndValueDate) {
		this.farEndValueDate = farEndValueDate;
	}

	public String getFarEndPrice() {
		return farEndPrice;
	}

	public void setFarEndPrice(String farEndPrice) {
		this.farEndPrice = farEndPrice;
	}

	public String getFarEndSellAmount() {
		return farEndSellAmount;
	}

	public void setFarEndSellAmount(String farEndSellAmount) {
		this.farEndSellAmount = farEndSellAmount;
	}

	public String getFarEndBuyAmount() {
		return farEndBuyAmount;
	}

	public void setFarEndBuyAmount(String farEndBuyAmount) {
		this.farEndBuyAmount = farEndBuyAmount;
	}

	public String getFarEndSellCcy() {
		return farEndSellCcy;
	}

	public void setFarEndSellCcy(String farEndSellCcy) {
		this.farEndSellCcy = farEndSellCcy;
	}

	public String getFarEndBuyCcy() {
		return farEndBuyCcy;
	}

	public void setFarEndBuyCcy(String farEndBuyCcy) {
		this.farEndBuyCcy = farEndBuyCcy;
	}

	public String getOwnPartyID() {
		return ownPartyID;
	}

	public void setOwnPartyID(String ownPartyID) {
		this.ownPartyID = ownPartyID;
	}


	public String getOthPartyID() {
		return othPartyID;
	}

	public void setOthPartyID(String othPartyID) {
		this.othPartyID = othPartyID;
	}

	public String getOwnCnName() {
		return ownCnName;
	}

	public void setOwnCnName(String ownCnName) {
		this.ownCnName = ownCnName;
	}

	public String getNearEndOwnCapitalBakeName() {
		return nearEndOwnCapitalBakeName;
	}

	public void setNearEndOwnCapitalBakeName(String nearEndOwnCapitalBakeName) {
		this.nearEndOwnCapitalBakeName = nearEndOwnCapitalBakeName;
	}

	public String getNearEndOwnCapitalAccountName() {
		return nearEndOwnCapitalAccountName;
	}

	public void setNearEndOwnCapitalAccountName(String nearEndOwnCapitalAccountName) {
		this.nearEndOwnCapitalAccountName = nearEndOwnCapitalAccountName;
	}

	public String getNearEndOwnPayBankNo() {
		return nearEndOwnPayBankNo;
	}

	public void setNearEndOwnPayBankNo(String nearEndOwnPayBankNo) {
		this.nearEndOwnPayBankNo = nearEndOwnPayBankNo;
	}

	public String getNearEndOwnCapitalAccount() {
		return nearEndOwnCapitalAccount;
	}

	public void setNearEndOwnCapitalAccount(String nearEndOwnCapitalAccount) {
		this.nearEndOwnCapitalAccount = nearEndOwnCapitalAccount;
	}

	public String getNearEndOwnRemark() {
		return nearEndOwnRemark;
	}

	public void setNearEndOwnRemark(String nearEndOwnRemark) {
		this.nearEndOwnRemark = nearEndOwnRemark;
	}

	public String getFarEndMidCnName() {
		return farEndMidCnName;
	}

	public void setFarEndMidCnName(String farEndMidCnName) {
		this.farEndMidCnName = farEndMidCnName;
	}

	public String getFarEndMidBankBIC() {
		return farEndMidBankBIC;
	}

	public void setFarEndMidBankBIC(String farEndMidBankBIC) {
		this.farEndMidBankBIC = farEndMidBankBIC;
	}

	public String getFarEndMidBankAccount() {
		return farEndMidBankAccount;
	}

	public void setFarEndMidBankAccount(String farEndMidBankAccount) {
		this.farEndMidBankAccount = farEndMidBankAccount;
	}

	public String getFarEndOwnCapitalBakeName() {
		return farEndOwnCapitalBakeName;
	}

	public void setFarEndOwnCapitalBakeName(String farEndOwnCapitalBakeName) {
		this.farEndOwnCapitalBakeName = farEndOwnCapitalBakeName;
	}

	public String getFarEndOwnCapitalAccountBIC() {
		return farEndOwnCapitalAccountBIC;
	}

	public void setFarEndOwnCapitalAccountBIC(String farEndOwnCapitalAccountBIC) {
		this.farEndOwnCapitalAccountBIC = farEndOwnCapitalAccountBIC;
	}

	public String getFarEndOwnCapitalAccount() {
		return farEndOwnCapitalAccount;
	}

	public void setFarEndOwnCapitalAccount(String farEndOwnCapitalAccount) {
		this.farEndOwnCapitalAccount = farEndOwnCapitalAccount;
	}

	public String getFarEndOwnReceiptBankName() {
		return farEndOwnReceiptBankName;
	}

	public void setFarEndOwnReceiptBankName(String farEndOwnReceiptBankName) {
		this.farEndOwnReceiptBankName = farEndOwnReceiptBankName;
	}

	public String getFarEndOwnReceiptBankBIC() {
		return farEndOwnReceiptBankBIC;
	}

	public void setFarEndOwnReceiptBankBIC(String farEndOwnReceiptBankBIC) {
		this.farEndOwnReceiptBankBIC = farEndOwnReceiptBankBIC;
	}

	public String getFarEndOwnReceiptBankAccount() {
		return farEndOwnReceiptBankAccount;
	}

	public void setFarEndOwnReceiptBankAccount(String farEndOwnReceiptBankAccount) {
		this.farEndOwnReceiptBankAccount = farEndOwnReceiptBankAccount;
	}

	public String getFarEndOwnRemark() {
		return farEndOwnRemark;
	}

	public void setFarEndOwnRemark(String farEndOwnRemark) {
		this.farEndOwnRemark = farEndOwnRemark;
	}

	public String getOthCnName() {
		return othCnName;
	}

	public void setOthCnName(String othCnName) {
		this.othCnName = othCnName;
	}

	public String getFarEndOthCapitalBakeName() {
		return farEndOthCapitalBakeName;
	}

	public void setFarEndOthCapitalBakeName(String farEndOthCapitalBakeName) {
		this.farEndOthCapitalBakeName = farEndOthCapitalBakeName;
	}

	public String getFarEndOthCapitalAccountName() {
		return farEndOthCapitalAccountName;
	}

	public void setFarEndOthCapitalAccountName(String farEndOthCapitalAccountName) {
		this.farEndOthCapitalAccountName = farEndOthCapitalAccountName;
	}

	public String getFarEndOthPayBankNo() {
		return farEndOthPayBankNo;
	}

	public void setFarEndOthPayBankNo(String farEndOthPayBankNo) {
		this.farEndOthPayBankNo = farEndOthPayBankNo;
	}

	public String getFarEndOthCapitalAccount() {
		return farEndOthCapitalAccount;
	}

	public void setFarEndOthCapitalAccount(String farEndOthCapitalAccount) {
		this.farEndOthCapitalAccount = farEndOthCapitalAccount;
	}

	public String getFarEndOthRemark() {
		return farEndOthRemark;
	}

	public void setFarEndOthRemark(String farEndOthRemark) {
		this.farEndOthRemark = farEndOthRemark;
	}

	public String getNearEndMidCnName() {
		return nearEndMidCnName;
	}

	public void setNearEndMidCnName(String nearEndMidCnName) {
		this.nearEndMidCnName = nearEndMidCnName;
	}

	public String getNearEndMidBankBIC() {
		return nearEndMidBankBIC;
	}

	public void setNearEndMidBankBIC(String nearEndMidBankBIC) {
		this.nearEndMidBankBIC = nearEndMidBankBIC;
	}

	public String getNearEndMidBankAccount() {
		return nearEndMidBankAccount;
	}

	public void setNearEndMidBankAccount(String nearEndMidBankAccount) {
		this.nearEndMidBankAccount = nearEndMidBankAccount;
	}

	public String getNearEndOthCapitalBakeName() {
		return nearEndOthCapitalBakeName;
	}

	public void setNearEndOthCapitalBakeName(String nearEndOthCapitalBakeName) {
		this.nearEndOthCapitalBakeName = nearEndOthCapitalBakeName;
	}

	public String getNearEndOthCapitalAccountBIC() {
		return nearEndOthCapitalAccountBIC;
	}

	public void setNearEndOthCapitalAccountBIC(String nearEndOthCapitalAccountBIC) {
		this.nearEndOthCapitalAccountBIC = nearEndOthCapitalAccountBIC;
	}

	public String getNearEndOthCapitalAccount() {
		return nearEndOthCapitalAccount;
	}

	public void setNearEndOthCapitalAccount(String nearEndOthCapitalAccount) {
		this.nearEndOthCapitalAccount = nearEndOthCapitalAccount;
	}

	public String getNearEndOthReceiptBankName() {
		return nearEndOthReceiptBankName;
	}

	public void setNearEndOthReceiptBankName(String nearEndOthReceiptBankName) {
		this.nearEndOthReceiptBankName = nearEndOthReceiptBankName;
	}

	public String getNearEndOthReceiptBankBIC() {
		return nearEndOthReceiptBankBIC;
	}

	public void setNearEndOthReceiptBankBIC(String nearEndOthReceiptBankBIC) {
		this.nearEndOthReceiptBankBIC = nearEndOthReceiptBankBIC;
	}

	public String getNearEndOthReceiptAccount() {
		return nearEndOthReceiptAccount;
	}

	public void setNearEndOthReceiptAccount(String nearEndOthReceiptAccount) {
		this.nearEndOthReceiptAccount = nearEndOthReceiptAccount;
	}

	public String getNearEndOthRemark() {
		return nearEndOthRemark;
	}

	public void setNearEndOthRemark(String nearEndOthRemark) {
		this.nearEndOthRemark = nearEndOthRemark;
	}

	public String getSecurityID() {
		return securityID;
	}

	public void setSecurityID(String securityID) {
		this.securityID = securityID;
	}

	public String getLegSide_1() {
		return legSide_1;
	}

	public void setLegSide_1(String legSide_1) {
		this.legSide_1 = legSide_1;
	}

	public String getLegSide_2() {
		return legSide_2;
	}

	public void setLegSide_2(String legSide_2) {
		this.legSide_2 = legSide_2;
	}	
	
	

}
