package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.*;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * 现券模块业务处理类,发送现券买卖业务对象到Capital Web资金前置系统中
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/cfetsImixSecurExporter")
public interface ICfetsImixSecur {

    /**
     * 发送现券买卖交易到资金前置平台
     *
     * @param fixedIncome 现券买卖业务对象
     * @param packet      报文对象  modify by shenzl add
     * @return
     */
    RetBean send(FixedIncomeBean fixedIncome, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /***
     * 债券远期交易到资金前置平台
     * @param bfdBean
     * @param cfetsPackMsg
     * @return
     * @throws RemoteConnectFailureException
     * @throws Exception
     */
    RetBean send(BfdBean bfdBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /***
     * 发送标准债券远期交易到资金前置平台
     * @param bfdBean
     * @param cfetsPackMsg
     * @return
     * @throws RemoteConnectFailureException
     * @throws Exception
     */
    RetBean send(StandardBfdBean standardBfdBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;


    /**
     * 发送同业存单到资金前置平台
     *
     * @param tradeBean
     * @param cfetsPackMsg
     * @return
     */
    RetBean send(IbncdBean tradeBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 发送缴款到资金前置
     *
     * @param tradeBean
     * @param cfetsPackMsg
     * @return
     */
    RetBean send(IbncdCommissionBean tradeBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;
}
