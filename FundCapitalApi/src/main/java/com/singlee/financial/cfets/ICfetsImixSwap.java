package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.SwapCrsBean;
import com.singlee.financial.pojo.SwapIrsBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * 互换模块业务处理类,发送利率互换、货币互换业务对象到Capital Web资金前置系统中
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/cfetsImixSwapExporter")
public interface ICfetsImixSwap {

    /**
     * 方法已重载.发送利率互换交易到资金前置平台
     *
     * @param irs    利率互换业务对象
     * @param packet 报文对象  modify by shenzl add
     * @return
     */
    RetBean send(SwapIrsBean irs, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 方法已重载.发送货币互换交易到资金前置平台
     *
     * @param crs    货币互换业务
     * @param packet 报文对象  modify by shenzl add
     * @return
     */
    RetBean send(SwapCrsBean crs, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;


    /**
     * 方法已重载.发送利率互换交易到资金前置平台
     *
     * @param irs    利率互换业务对象
     * @param packet 报文对象  modify by shenzl add
     * @return
     */
    RetBean sendFxIrs(SwapIrsBean irs, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;
}
