package com.singlee.financial.cfets.bean;


public class RecForeignOptionModel extends RecMsgBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String confirmID;
	private String execID;
	private String confirmStatus;
	private String tradeDate;
	private String side;
	private String netGrossInd;
	private String optPremiumCurrency;
	private String currency1;
	private String grossTradeAmt;
	private String currency2;
	private String calculatedCcyLastTradeAmt;
	private String agreementCurrency;
	private String putOrCall;
	private String marketIndicator;
	private String strikePrice;
	private String exerciseStyle;
	private String maturityDate;
	private String benchmarkCurveName;
	private String expireTime;
	private String expireTimeZone;
	private String deliveryDate;
	private String optSettlAmtType;
	private String paymentDate;
	private String optPremiumBasis;
	private String optPremiumValue;
	private String optPremiumAmt;
	private String ownCnfmTimeType;
	private String ownCnfmTime;
	private String othCnfmTimeType;
	private String othCnfmTime;
	private String ownPartyID;
	private String ownPartyRole;
	private String othPartyID;
	private String othPartyRole;
	private String optionCostCapitalBakeName;
	private String optionCostCapitalAccountName;
	private String optionCostPayBankNo;
	private String optionCostCapitalAccount;
	private String optionCostRemark;
	private String optDataType;
	private String combinationID;
	private String optPayoutType;
	private String currency1Amt;
	private String currency2Amt;
	private String derivativeExerciseStype;
	public String getConfirmID() {
		return confirmID;
	}
	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}
	public String getExecID() {
		return execID;
	}
	public void setExecID(String execID) {
		this.execID = execID;
	}
	public String getConfirmStatus() {
		return confirmStatus;
	}
	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}
	public String getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public String getNetGrossInd() {
		return netGrossInd;
	}
	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}
	public String getOptPremiumCurrency() {
		return optPremiumCurrency;
	}
	public void setOptPremiumCurrency(String optPremiumCurrency) {
		this.optPremiumCurrency = optPremiumCurrency;
	}
	public String getCurrency1() {
		return currency1;
	}
	public void setCurrency1(String currency1) {
		this.currency1 = currency1;
	}
	public String getGrossTradeAmt() {
		return grossTradeAmt;
	}
	public void setGrossTradeAmt(String grossTradeAmt) {
		this.grossTradeAmt = grossTradeAmt;
	}
	public String getCurrency2() {
		return currency2;
	}
	public void setCurrency2(String currency2) {
		this.currency2 = currency2;
	}
	public String getCalculatedCcyLastTradeAmt() {
		return calculatedCcyLastTradeAmt;
	}
	public void setCalculatedCcyLastTradeAmt(String calculatedCcyLastTradeAmt) {
		this.calculatedCcyLastTradeAmt = calculatedCcyLastTradeAmt;
	}
	public String getAgreementCurrency() {
		return agreementCurrency;
	}
	public void setAgreementCurrency(String agreementCurrency) {
		this.agreementCurrency = agreementCurrency;
	}
	public String getPutOrCall() {
		return putOrCall;
	}
	public void setPutOrCall(String putOrCall) {
		this.putOrCall = putOrCall;
	}
	public String getMarketIndicator() {
		return marketIndicator;
	}
	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}
	public String getStrikePrice() {
		return strikePrice;
	}
	public void setStrikePrice(String strikePrice) {
		this.strikePrice = strikePrice;
	}
	public String getExerciseStyle() {
		return exerciseStyle;
	}
	public void setExerciseStyle(String exerciseStyle) {
		this.exerciseStyle = exerciseStyle;
	}
	public String getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	public String getBenchmarkCurveName() {
		return benchmarkCurveName;
	}
	public void setBenchmarkCurveName(String benchmarkCurveName) {
		this.benchmarkCurveName = benchmarkCurveName;
	}
	public String getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}
	public String getExpireTimeZone() {
		return expireTimeZone;
	}
	public void setExpireTimeZone(String expireTimeZone) {
		this.expireTimeZone = expireTimeZone;
	}
	public String getOptSettlAmtType() {
		return optSettlAmtType;
	}
	public void setOptSettlAmtType(String optSettlAmtType) {
		this.optSettlAmtType = optSettlAmtType;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getOptPremiumBasis() {
		return optPremiumBasis;
	}
	public void setOptPremiumBasis(String optPremiumBasis) {
		this.optPremiumBasis = optPremiumBasis;
	}
	public String getOptPremiumValue() {
		return optPremiumValue;
	}
	public void setOptPremiumValue(String optPremiumValue) {
		this.optPremiumValue = optPremiumValue;
	}
	public String getOptPremiumAmt() {
		return optPremiumAmt;
	}
	public void setOptPremiumAmt(String optPremiumAmt) {
		this.optPremiumAmt = optPremiumAmt;
	}
	public String getOwnCnfmTimeType() {
		return ownCnfmTimeType;
	}
	public void setOwnCnfmTimeType(String ownCnfmTimeType) {
		this.ownCnfmTimeType = ownCnfmTimeType;
	}
	public String getOwnCnfmTime() {
		return ownCnfmTime;
	}
	public void setOwnCnfmTime(String ownCnfmTime) {
		this.ownCnfmTime = ownCnfmTime;
	}
	public String getOthCnfmTimeType() {
		return othCnfmTimeType;
	}
	public void setOthCnfmTimeType(String othCnfmTimeType) {
		this.othCnfmTimeType = othCnfmTimeType;
	}
	public String getOthCnfmTime() {
		return othCnfmTime;
	}
	public void setOthCnfmTime(String othCnfmTime) {
		this.othCnfmTime = othCnfmTime;
	}
	public String getOwnPartyID() {
		return ownPartyID;
	}
	public void setOwnPartyID(String ownPartyID) {
		this.ownPartyID = ownPartyID;
	}
	public String getOwnPartyRole() {
		return ownPartyRole;
	}
	public void setOwnPartyRole(String ownPartyRole) {
		this.ownPartyRole = ownPartyRole;
	}
	public String getOthPartyID() {
		return othPartyID;
	}
	public void setOthPartyID(String othPartyID) {
		this.othPartyID = othPartyID;
	}
	public String getOthPartyRole() {
		return othPartyRole;
	}
	public void setOthPartyRole(String othPartyRole) {
		this.othPartyRole = othPartyRole;
	}
	public String getOptionCostCapitalBakeName() {
		return optionCostCapitalBakeName;
	}
	public void setOptionCostCapitalBakeName(String optionCostCapitalBakeName) {
		this.optionCostCapitalBakeName = optionCostCapitalBakeName;
	}
	public String getOptionCostCapitalAccountName() {
		return optionCostCapitalAccountName;
	}
	public void setOptionCostCapitalAccountName(String optionCostCapitalAccountName) {
		this.optionCostCapitalAccountName = optionCostCapitalAccountName;
	}
	public String getOptionCostPayBankNo() {
		return optionCostPayBankNo;
	}
	public void setOptionCostPayBankNo(String optionCostPayBankNo) {
		this.optionCostPayBankNo = optionCostPayBankNo;
	}
	public String getOptionCostCapitalAccount() {
		return optionCostCapitalAccount;
	}
	public void setOptionCostCapitalAccount(String optionCostCapitalAccount) {
		this.optionCostCapitalAccount = optionCostCapitalAccount;
	}
	public String getOptionCostRemark() {
		return optionCostRemark;
	}
	public void setOptionCostRemark(String optionCostRemark) {
		this.optionCostRemark = optionCostRemark;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getOptDataType() {
		return optDataType;
	}
	public void setOptDataType(String optDataType) {
		this.optDataType = optDataType;
	}
	public String getCombinationID() {
		return combinationID;
	}
	public void setCombinationID(String combinationID) {
		this.combinationID = combinationID;
	}
	public String getOptPayoutType() {
		return optPayoutType;
	}
	public void setOptPayoutType(String optPayoutType) {
		this.optPayoutType = optPayoutType;
	}
	public String getCurrency1Amt() {
		return currency1Amt;
	}
	public void setCurrency1Amt(String currency1Amt) {
		this.currency1Amt = currency1Amt;
	}
	public String getCurrency2Amt() {
		return currency2Amt;
	}
	public void setCurrency2Amt(String currency2Amt) {
		this.currency2Amt = currency2Amt;
	}
	public String getDerivativeExerciseStype() {
		return derivativeExerciseStype;
	}
	public void setDerivativeExerciseStype(String derivativeExerciseStype) {
		this.derivativeExerciseStype = derivativeExerciseStype;
	}
	
	
}
