package com.singlee.financial.cfets.bean;

import java.io.Serializable;

public class MatchFieldDetails  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String sendFieldID;
	private String sendFieldValue;
	private String receivedFieldID;
	private String receivedFieldValue;
	private int fieldMatchResult;//匹配结果

	
	
	
	public String getSendFieldID() {
		return sendFieldID;
	}
	public void setSendFieldID(String sendFieldID) {
		this.sendFieldID = sendFieldID;
	}
	public String getSendFieldValue() {
		return sendFieldValue;
	}
	public void setSendFieldValue(String sendFieldValue) {
		this.sendFieldValue = sendFieldValue;
	}
	public String getReceivedFieldID() {
		return receivedFieldID;
	}
	public void setReceivedFieldID(String receivedFieldID) {
		this.receivedFieldID = receivedFieldID;
	}
	public String getReceivedFieldValue() {
		return receivedFieldValue;
	}
	public void setReceivedFieldValue(String receivedFieldValue) {
		this.receivedFieldValue = receivedFieldValue;
	}
	public int getFieldMatchResult() {
		return fieldMatchResult;
	}
	public void setFieldMatchResult(int fieldMatchResult) {
		this.fieldMatchResult = fieldMatchResult;
	}
	
}
