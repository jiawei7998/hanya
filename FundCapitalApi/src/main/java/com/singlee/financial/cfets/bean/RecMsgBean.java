package com.singlee.financial.cfets.bean;

import java.io.Serializable;

public class RecMsgBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//分页信息
	private RecPageInfoModel pageInfo;

	//报文信息
	private String message;
	
	public RecPageInfoModel getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(RecPageInfoModel pageInfo) {
		this.pageInfo = pageInfo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
