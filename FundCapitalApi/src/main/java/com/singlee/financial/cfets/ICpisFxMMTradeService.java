package com.singlee.financial.cfets;


import com.singlee.financial.cfets.bean.RecForeignFxmmModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.List;

@Capital(context = CapitalContext.SINGLEE_API,uri = "/cpisFxMMTradeServiceExporter")
public interface ICpisFxMMTradeService {
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(RecForeignFxmmModel foreignFxmmModel) throws  Exception;
	
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(List<RecForeignFxmmModel> list) throws  Exception;
}
