package com.singlee.financial.cfets.bean;


public class RecForeignForwardSpotModel extends RecMsgBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String execID;//成交编号
	private String tradeDate;//成交日期
	private String netGrossInd;//清算方式
	private String marketIndicator;//市场类型
	private String execType;//成交状态 F-TRADE-已成交
	private String dealTransType;//成交单录入 0-dealentry
	private String confirmID;//报文编号
	private String confirmStatus;//确认状态
	private String ownCnfmTime;//本方确认时间
	private String othCnfmTime;//对手方确认时间
	private	String settlDate;//起息日
	private String lastPx;//成交价格
	private String currency1;//买入币种
	private String lastQty;//买入金额
	private String currency2;//卖出币种
	private String calculatedCcyLastQty;//卖出金额
	private String ownPartyID;//本方机构21位码
	private String ownCnName;//本方机构中文全称
	private String ownCapitalBakeName;//本方资金开户行
	private String ownCapitalAccountName;//本方资金账户户名
	private String ownPayBankNo;//本方支付系统行号
	private String ownCapitalAccountBIC;//本方开户行BICCODE
	private String ownCapitalAccount;//本方资金账号
	private String ownRemark;//本方附言
	private String midCnName;//中间行名称
	private String midBankBIC;//中间行BICCODE
	private String midBankAccount;//中间行账号
	private String ownOpenAccount;//开户行账号
	private String othPartyID;//对手方机构21位码
	private String othCnName;//对手方机构中文全称
	private String othCapitalBankName;//对手方资金开户行
	private String othCapitalAccountName;//对手方资金账户户名
	private String othPayBankNo;//对手方支付系统行号
	private String othCapitalAccount;//对手方资金账号
	private String othRemark;//对手方附言
	
	private String securityID;//货币对
	
	public String getExecID() {
		return execID;
	}
	public void setExecID(String execID) {
		this.execID = execID;
	}
	public String getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}
	public String getNetGrossInd() {
		return netGrossInd;
	}
	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}
	public String getMarketIndicator() {
		return marketIndicator;
	}
	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}
	public String getExecType() {
		return execType;
	}
	public void setExecType(String execType) {
		this.execType = execType;
	}
	public String getDealTransType() {
		return dealTransType;
	}
	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}
	public String getConfirmID() {
		return confirmID;
	}
	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}
	public String getConfirmStatus() {
		return confirmStatus;
	}
	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}
	public String getOwnCnfmTime() {
		return ownCnfmTime;
	}
	public void setOwnCnfmTime(String ownCnfmTime) {
		this.ownCnfmTime = ownCnfmTime;
	}
	public String getOthCnfmTime() {
		return othCnfmTime;
	}
	public void setOthCnfmTime(String othCnfmTime) {
		this.othCnfmTime = othCnfmTime;
	}
	public String getSettlDate() {
		return settlDate;
	}
	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}
	public String getLastPx() {
		return lastPx;
	}
	public void setLastPx(String lastPx) {
		this.lastPx = lastPx;
	}
	public String getCurrency1() {
		return currency1;
	}
	public void setCurrency1(String currency1) {
		this.currency1 = currency1;
	}
	public String getLastQty() {
		return lastQty;
	}
	public void setLastQty(String lastQty) {
		this.lastQty = lastQty;
	}
	public String getCurrency2() {
		return currency2;
	}
	public void setCurrency2(String currency2) {
		this.currency2 = currency2;
	}
	public String getCalculatedCcyLastQty() {
		return calculatedCcyLastQty;
	}
	public void setCalculatedCcyLastQty(String calculatedCcyLastQty) {
		this.calculatedCcyLastQty = calculatedCcyLastQty;
	}
	public String getOwnPartyID() {
		return ownPartyID;
	}
	public void setOwnPartyID(String ownPartyID) {
		this.ownPartyID = ownPartyID;
	}
	public String getOwnCnName() {
		return ownCnName;
	}
	public void setOwnCnName(String ownCnName) {
		this.ownCnName = ownCnName;
	}
	public String getOwnCapitalBakeName() {
		return ownCapitalBakeName;
	}
	public void setOwnCapitalBakeName(String ownCapitalBakeName) {
		this.ownCapitalBakeName = ownCapitalBakeName;
	}
	public String getOwnCapitalAccountName() {
		return ownCapitalAccountName;
	}
	public void setOwnCapitalAccountName(String ownCapitalAccountName) {
		this.ownCapitalAccountName = ownCapitalAccountName;
	}
	public String getOwnPayBankNo() {
		return ownPayBankNo;
	}
	public void setOwnPayBankNo(String ownPayBankNo) {
		this.ownPayBankNo = ownPayBankNo;
	}
	public String getOwnCapitalAccountBIC() {
		return ownCapitalAccountBIC;
	}
	public void setOwnCapitalAccountBIC(String ownCapitalAccountBIC) {
		this.ownCapitalAccountBIC = ownCapitalAccountBIC;
	}
	public String getOwnCapitalAccount() {
		return ownCapitalAccount;
	}
	public void setOwnCapitalAccount(String ownCapitalAccount) {
		this.ownCapitalAccount = ownCapitalAccount;
	}
	public String getOwnRemark() {
		return ownRemark;
	}
	public void setOwnRemark(String ownRemark) {
		this.ownRemark = ownRemark;
	}
	public String getMidCnName() {
		return midCnName;
	}
	public void setMidCnName(String midCnName) {
		this.midCnName = midCnName;
	}
	public String getMidBankBIC() {
		return midBankBIC;
	}
	public void setMidBankBIC(String midBankBIC) {
		this.midBankBIC = midBankBIC;
	}
	public String getMidBankAccount() {
		return midBankAccount;
	}
	public void setMidBankAccount(String midBankAccount) {
		this.midBankAccount = midBankAccount;
	}
	public String getOwnOpenAccount() {
		return ownOpenAccount;
	}
	public void setOwnOpenAccount(String ownOpenAccount) {
		this.ownOpenAccount = ownOpenAccount;
	}
	public String getOthPartyID() {
		return othPartyID;
	}
	public void setOthPartyID(String othPartyID) {
		this.othPartyID = othPartyID;
	}
	public String getOthCnName() {
		return othCnName;
	}
	public void setOthCnName(String othCnName) {
		this.othCnName = othCnName;
	}
	public String getOthCapitalBankName() {
		return othCapitalBankName;
	}
	public void setOthCapitalBankName(String othCapitalBankName) {
		this.othCapitalBankName = othCapitalBankName;
	}
	public String getOthCapitalAccountName() {
		return othCapitalAccountName;
	}
	public void setOthCapitalAccountName(String othCapitalAccountName) {
		this.othCapitalAccountName = othCapitalAccountName;
	}
	public String getOthPayBankNo() {
		return othPayBankNo;
	}
	public void setOthPayBankNo(String othPayBankNo) {
		this.othPayBankNo = othPayBankNo;
	}
	public String getOthCapitalAccount() {
		return othCapitalAccount;
	}
	public void setOthCapitalAccount(String othCapitalAccount) {
		this.othCapitalAccount = othCapitalAccount;
	}
	public String getOthRemark() {
		return othRemark;
	}
	public void setOthRemark(String othRemark) {
		this.othRemark = othRemark;
	}
	public String getSecurityID() {
		return securityID;
	}
	public void setSecurityID(String securityID) {
		this.securityID = securityID;
	}
	
	
	
	
}
