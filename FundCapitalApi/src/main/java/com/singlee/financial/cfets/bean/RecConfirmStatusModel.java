package com.singlee.financial.cfets.bean;


/**
 * 接收查询(确认状态)的实体
 * @author pengqian
 *
 */
public class RecConfirmStatusModel extends RecMsgBean {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private	String confirmType;
	
	private	String confirmStatus;
	
	private	String queryRequestID;
	
	private	String execID;
	
	private	String tradeDate;
	
	private	String marketIndicator;
	
	private String ownCnfmTimeType;
	
	private	String ownCnfmTime;
	
	private String othCnfmTimeType;
	
	private	String othCnfmTime;
	
	private	String ownPartyID = ""; //本方机构 21 位码
	
	private	int ownPartyRole;
	
	private	String othPartyID;//对手方机构 21 位码
	
	private	int othPartyRole;
	
	private	String ownCnName; //中文全称
	
	private	String othCnName; //中文全称

	public String getConfirmType() {
		return confirmType;
	}

	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}

	public String getConfirmStatus() {
		return confirmStatus;
	}

	public void setConfirmStatus(String confirmStatus) {
		this.confirmStatus = confirmStatus;
	}

	public String getQueryRequestID() {
		return queryRequestID;
	}

	public void setQueryRequestID(String queryRequestID) {
		this.queryRequestID = queryRequestID;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getOwnCnfmTimeType() {
		return ownCnfmTimeType;
	}

	public void setOwnCnfmTimeType(String ownCnfmTimeType) {
		this.ownCnfmTimeType = ownCnfmTimeType;
	}

	public String getOwnCnfmTime() {
		return ownCnfmTime;
	}

	public void setOwnCnfmTime(String ownCnfmTime) {
		this.ownCnfmTime = ownCnfmTime;
	}

	public String getOthCnfmTimeType() {
		return othCnfmTimeType;
	}

	public void setOthCnfmTimeType(String othCnfmTimeType) {
		this.othCnfmTimeType = othCnfmTimeType;
	}

	public String getOthCnfmTime() {
		return othCnfmTime;
	}

	public void setOthCnfmTime(String othCnfmTime) {
		this.othCnfmTime = othCnfmTime;
	}

	public String getOwnPartyID() {
		return ownPartyID;
	}

	public void setOwnPartyID(String ownPartyID) {
		this.ownPartyID = ownPartyID;
	}

	public int getOwnPartyRole() {
		return ownPartyRole;
	}

	public void setOwnPartyRole(int ownPartyRole) {
		this.ownPartyRole = ownPartyRole;
	}

	public String getOthPartyID() {
		return othPartyID;
	}

	public void setOthPartyID(String othPartyID) {
		this.othPartyID = othPartyID;
	}

	public int getOthPartyRole() {
		return othPartyRole;
	}

	public void setOthPartyRole(int othPartyRole) {
		this.othPartyRole = othPartyRole;
	}

	public String getOwnCnName() {
		return ownCnName;
	}

	public void setOwnCnName(String ownCnName) {
		this.ownCnName = ownCnName;
	}

	public String getOthCnName() {
		return othCnName;
	}

	public void setOthCnName(String othCnName) {
		this.othCnName = othCnName;
	}
	
	

}
