package com.singlee.financial.cfets;


import com.singlee.financial.cfets.bean.RecInterestRateSwapModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.List;

@Capital(context = CapitalContext.SINGLEE_API,uri = "/cpisIrsTradeServiceExporter")
public interface ICpisIrsTradeService {
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(RecInterestRateSwapModel model) throws  Exception;
	
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(List<RecInterestRateSwapModel> list) throws  Exception;
}
