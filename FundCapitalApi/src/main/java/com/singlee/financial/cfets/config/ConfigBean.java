package com.singlee.financial.cfets.config;

import java.io.Serializable;

/**
 * Cfets配置信息实体对象
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
public class ConfigBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1991893920409782517L;
    /**
     * 0-表示收到对 ExecID 所指定的需确认的成交报告，但是尚未做出处理
     */
    public final static int ExecAckStatus_Recieve_Confirm = 0;
    /**
     * 1-表示对 ExecID 所指定的需确认的成交报告进行确认并接受
     */
    public final static int ExecAckStatus_Accept_Confirm = 1;
    /**
     * 2-表示对 ExecID 所指定的需确认的成交报告拒绝接受
     */
    public final static int ExecAckStatus_Reject_Confirm = 2;
    /**
     * CFETS CLIENT.CFG文件相对路径
     */
    public final static String cfgClientPath = "cfg/client.cfg";
    /**
     * CFETS报文保存目录
     */
    public final static String dataPath = "Data";
    /**
     * 资金前置异常报文保存目录
     */
    public final static String errDataPath = "errData";
    /**
     * 外币接口报文保存目录
     */
    public final static String fxDir = "Fx";
    /**
     * 本币接口报文保存目录
     */
    public final static String RmbDir = "Rmb";
    /**
     * XML文件后缀名
     */
    public final static String xmlSuffix = ".xml";
    /**
     * TXT文件后缀名
     */
    public final static String txtSuffix = ".txt";
    /**
     * 重新解析报文标识
     */
    public final static String reSendFlag = "SL_R";
    // 银行唯一标识号
    private String bankId;
    //机构\投资人 21位码
    private String instId;
    // CFETS消息XML文件保存对象
    private String saveFilePath;
    // 是否保存CFETS消息对象XML文件
    private boolean isSaveFile;
    // CFETS市场标识
    private String market;
    // CFETS用户名
    private String userName;
    // CFETS密码
    private String password;
    // CFETS市场标识1
    private String market1;
    // CFETS用户名1
    private String userName1;
    // CFETS密码1
    private String password1;

    // 需要更新的配置项
    private String updatePathKey;
    // 字符集
    private String charset;
    // 线程池维护线程的最少数量
    private int corePoolSize;
    // 线程池维护线程的最大数量
    private int maximumPoolSize;
    // 线程池维护线程所允许的空闲时间
    private int keepAliveTime;
    // 线程池队列大小
    private int arrayBlockingQueueSize;
    // 断线重连连接次数
    private int reconCount;
    // 日志路径
    private String logPath;
    //外币下行
    private String cstpFxFlag;//验收开启标志
    private String cstpFxUrl;//验收反馈文件路径
    //本币下行
    private String cstpRmbFlag;//验收开启标志
    private String cstpRmbUrl;//验收反馈文件路径

    /**
     * 获取字符集
     *
     * @return
     */
    public String getCharset() {
        return charset;
    }

    /**
     * 设置字符集
     *
     * @param charset
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * 获取银行唯一标识号
     *
     * @return
     */
    public String getBankId() {
        return bankId;
    }

    /**
     * 设置银行唯一标识号
     *
     * @param bankId
     */
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    /**
     * 获取CFETS对象XML文件保存路径
     *
     * @return
     */
    public String getSaveFilePath() {
        return saveFilePath;
    }

    /**
     * 设置CFETS对象XML文件保存路径
     *
     * @param saveFilePath
     */
    public void setSaveFilePath(String saveFilePath) {
        this.saveFilePath = saveFilePath;
    }

    /**
     * 是否将CFETS对象以XML文件保存到文件中
     *
     * @return
     */
    public boolean isSaveFile() {
        return isSaveFile;
    }

    /**
     * 是否将CFETS对象以XML文件保存到文件中
     *
     * @param isSaveFile
     */
    public void setIsSaveFile(boolean isSaveFile) {
        this.isSaveFile = isSaveFile;
    }

    /**
     * 获取CFETS用户名
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置CFETS用户名
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 获取CFETS密码
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置CFETS密码
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取CFETS市场标识
     *
     * @return
     */
    public String getMarket() {
        return market;
    }

    /**
     * 设置CFETS市场标识
     *
     * @param market
     */
    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * 获取client.cfg配置文件中需要更新的key
     *
     * @return
     */
    public String getUpdatePathKey() {
        return updatePathKey;
    }

    /**
     * 设置client.cfg配置文件中需要更新的key
     *
     * @param updatePathKey
     */
    public void setUpdatePathKey(String updatePathKey) {
        this.updatePathKey = updatePathKey;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public int getArrayBlockingQueueSize() {
        return arrayBlockingQueueSize;
    }

    public void setArrayBlockingQueueSize(int arrayBlockingQueueSize) {
        this.arrayBlockingQueueSize = arrayBlockingQueueSize;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getReconCount() {
        return reconCount;
    }

    public void setReconCount(int reconCount) {
        this.reconCount = reconCount;
    }

    public String getLogPath() {
        return logPath;
    }

    public void setLogPath(String logPath) {
        this.logPath = logPath;
    }

    public String getInstId() {
        return instId;
    }

    public void setInstId(String instId) {
        this.instId = instId;
    }

    public String getCstpFxFlag() {
        return cstpFxFlag;
    }

    public void setCstpFxFlag(String cstpFxFlag) {
        this.cstpFxFlag = cstpFxFlag;
    }

    public String getCstpFxUrl() {
        return cstpFxUrl;
    }

    public void setCstpFxUrl(String cstpFxUrl) {
        this.cstpFxUrl = cstpFxUrl;
    }

    public String getCstpRmbFlag() {
        return cstpRmbFlag;
    }

    public void setCstpRmbFlag(String cstpRmbFlag) {
        this.cstpRmbFlag = cstpRmbFlag;
    }

    public String getCstpRmbUrl() {
        return cstpRmbUrl;
    }

    public void setCstpRmbUrl(String cstpRmbUrl) {
        this.cstpRmbUrl = cstpRmbUrl;
    }

    public String getMarket1() {
        return market1;
    }

    public void setMarket1(String market1) {
        this.market1 = market1;
    }

    public String getUserName1() {
        return userName1;
    }

    public void setUserName1(String userName1) {
        this.userName1 = userName1;
    }

    public String getPassword1() {
        return password1;
    }

    public void setPassword1(String password1) {
        this.password1 = password1;
    }

    @Override
    public String toString() {
        return "ConfigBean{" +
                "bankId='" + bankId + '\'' +
                ", instId='" + instId + '\'' +
                ", saveFilePath='" + saveFilePath + '\'' +
                ", isSaveFile=" + isSaveFile +
                ", market='" + market + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", market1='" + market1 + '\'' +
                ", userName1='" + userName1 + '\'' +
                ", password1='" + password1 + '\'' +
                ", updatePathKey='" + updatePathKey + '\'' +
                ", charset='" + charset + '\'' +
                ", corePoolSize=" + corePoolSize +
                ", maximumPoolSize=" + maximumPoolSize +
                ", keepAliveTime=" + keepAliveTime +
                ", arrayBlockingQueueSize=" + arrayBlockingQueueSize +
                ", reconCount=" + reconCount +
                ", logPath='" + logPath + '\'' +
                ", cstpFxFlag='" + cstpFxFlag + '\'' +
                ", cstpFxUrl='" + cstpFxUrl + '\'' +
                ", cstpRmbFlag='" + cstpRmbFlag + '\'' +
                ", cstpRmbUrl='" + cstpRmbUrl + '\'' +
                '}';
    }
}
