package com.singlee.financial.cfets;


import com.singlee.financial.cfets.bean.RecMatchingModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

@Capital(context = CapitalContext.SINGLEE_API, uri = "/cpisSwiftMatchServiceExporter")
public interface ICpisSwiftMatchService {
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(RecMatchingModel model) throws  Exception;
}
