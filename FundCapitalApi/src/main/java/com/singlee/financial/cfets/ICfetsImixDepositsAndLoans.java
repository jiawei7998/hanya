package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.*;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * 拆借模块业务处理类,发送外币拆借业务对象到Capital Web资金前置系统中
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/cfetsImixMMExporter")
public interface ICfetsImixDepositsAndLoans {

    /**
     * 发送拆借交易到资金前置平台
     *
     * @param depositsAndLoans 外币拆借业务对象
     * @param packet           报文对象  modify by shenzl add
     * @return
     */
    RetBean send(DepositsAndLoansBean depositsAndLoans, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 发送外币拆借交易到资金前置平台
     *
     * @param depositsAndLoans 外币拆借业务对象
     * @param packet           报文对象  modify by shenzl add
     * @return
     */
    RetBean send(DepositsAndLoansFxBean depositsAndLoans, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 发送同业存放交易到资金前置平台
     *
     * @param depositsAndLoans 外币拆借业务对象
     * @param packet           报文对象  modify by shenzl add
     * @return
     */
    RetBean send(InterBankDepositBean interBankDepositBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 发送同业借款交易到资金前置平台
     *
     * @param depositsAndLoans 外币拆借业务对象
     * @param packet           报文对象  modify by shenzl add
     * @return
     */
    RetBean send(InterBankLoansBean interBankLoansBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

}
