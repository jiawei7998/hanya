package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.FxOptionBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * 期权模块业务处理类,发送期权业务对象到Capital Web资金前置系统中
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/cfetsImixOptionExporter")
public interface ICfetsImixOption {
    /**
     * 方法已重载.发送货币互换交易到资金前置平台
     *
     * @param crs    货币互换业务
     * @param packet 报文对象  modify by shenzl add
     * @return
     */
    RetBean send(FxOptionBean fxOptionBean, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;
}
