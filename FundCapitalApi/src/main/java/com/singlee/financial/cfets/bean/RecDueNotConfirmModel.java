package com.singlee.financial.cfets.bean;


/**
 * <p>接收到期未确认推送的实体类  </P>
 * <p>新利公司 </p>
 * 
 * @author Administrator
 *
 */

public class RecDueNotConfirmModel extends RecMsgBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//市场类型
	private	String tradeInstrument;
	
	//交易类型
	private	String marketIndicator;
	
	//类型
	private String confirmType;
	
	//到期未确认笔数
	private	String transactionNum;

	public String getTradeInstrument() {
		return tradeInstrument;
	}

	public void setTradeInstrument(String tradeInstrument) {
		this.tradeInstrument = tradeInstrument;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getConfirmType() {
		return confirmType;
	}

	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}

	public String getTransactionNum() {
		return transactionNum;
	}

	public void setTransactionNum(String transactionNum) {
		this.transactionNum = transactionNum;
	}
	
	

}
