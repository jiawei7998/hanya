package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.RecForeignOptionModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.List;


@Capital(context = CapitalContext.SINGLEE_API,uri = "/cpisFxoptTradeServiceExporter")
public interface ICpisFxoptTradeService {
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(RecForeignOptionModel recForeignOptionModel) throws  Exception;
	
	/**
	 * 交易处理服务
	 * @return
	 */
	RetBean send(List<RecForeignOptionModel> list) throws  Exception;
}
