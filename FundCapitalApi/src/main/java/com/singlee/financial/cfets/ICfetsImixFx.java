package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.FxSptFwdBean;
import com.singlee.financial.pojo.FxSwapBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * 外汇模块业务处理类,发送外汇即、远、掉期业务对象到Capital Web资金前置系统中
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/cfetsImixFxExporter")
public interface ICfetsImixFx {

    /**
     * 方法已重载.发送即远期交易到资金前置平台
     *
     * @param sptFwd
     * @param packet 报文对象  modify by shenzl add
     * @return
     */
    RetBean send(FxSptFwdBean sptFwd, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 方法已重载.发送即外汇掉期交易到资金前置平台
     *
     * @param fxSwap
     * @param packet 报文对象  modify by shenzl add
     * @return
     */
    RetBean send(FxSwapBean fxSwap, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;
}
