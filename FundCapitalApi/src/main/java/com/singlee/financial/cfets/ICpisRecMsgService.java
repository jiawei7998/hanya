package com.singlee.financial.cfets;


import com.singlee.financial.cfets.bean.FailRecConfirmStatusModel;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecConfirmStatusModel;
import com.singlee.financial.cfets.bean.RecDueNotConfirmModel;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.List;

@Capital(context = CapitalContext.SINGLEE_API, uri = "/cpisRecMsgServiceExporter")
public interface ICpisRecMsgService {
	
	
	/**
	 * 接收发送匹配报文后CFETS返回的匹配状态 
	 * @return
	 */
	RetBean send(RecCPISFeedBackModel model) throws  Exception;
	
	/**
	 * 接收查询失败报文
	 * @return
	 */
	RetBean send(FailRecConfirmStatusModel model) throws  Exception;
	
	/**
	 * 接收查询报文状态结果报文   
	 * @return
	 */
	RetBean send(List<RecConfirmStatusModel> model) throws  Exception;
	
	/**
	 * 接收查询报文状态结果报文   
	 * @return
	 */
	RetBean send(RecConfirmStatusModel model) throws  Exception;
	
	/**
	 * 接收到期未确认推送的实体类
	 * @return
	 */
	RetBean send(RecDueNotConfirmModel model) throws  Exception;
	
	
}


