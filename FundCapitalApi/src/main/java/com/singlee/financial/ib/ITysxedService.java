package com.singlee.financial.ib;

import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;

import java.util.Map;

/**
 * 同业授信额度查询、占用、释放接口 主要用于查询 capitalweb中数据
 *
 * @author zh
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/tysxedServiceExporter")
public interface ITysxedService {

    /**
     * 查询额度总览
     *
     * @param map
     * @return
     */
    public Map<String, Object> queryAll(Map<String, Object> map);

    /**
     * 查询额度明细
     *
     * @param map
     * @return
     */
    public Map<String, Object> queryDetail(Map<String, Object> map);

    /**
     * 额度占用
     *
     * @param map
     */
    public Map<String, Object> releaseLimit(Map<String, Object> map);

    /**
     * 额度释放
     *
     * @param map
     */
    public Map<String, Object> occupyLimit(Map<String, Object> map);
}
