package com.singlee.hessian.annotation;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum CapitalContext {
    SINGLEE_API("com.singlee.remote.url");
    
    //util,spring 2种方式都用，直接写到这了; spring_scanner中便于放入及之后其内使用，用的PropertiesLoaderUtils.fillProperties
    private String propfile="capitalUrl.properties";
    private String remoteUrlConfigKey;
 
    public void setPropfile(String propfile) {
		this.propfile = propfile;
	}

	private CapitalContext(String remoteUrlConfigKey) {
        this.remoteUrlConfigKey = remoteUrlConfigKey;
    }

	public String getRemoteUrl() {
        //return System.getProperty(remoteUrlConfigKey, "http://not_find/err/remoting");//调用已存入sysEnv内容
        return getJarPropsValue(propfile, remoteUrlConfigKey);
    }
	
    //根据Key读取Value
    public static String getJarPropsValue(String filePath, String key) {
        Properties pps = new Properties();
        try {
        	//filePath= ClassLoader.getSystemResource(filePath).toString().replace("file:/", "");//win, nux
        	//InputStream in = new BufferedInputStream (new FileInputStream(filePath));  
        	
            InputStream in = CapitalContext.class.getClassLoader().getResourceAsStream(filePath);
            pps.load(in);
            String value = pps.getProperty(key);
            return value;
            
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /*public static void main(String[] args) throws Exception {
		String filePath="hessianurl.properties";
		InputStream stream = Context.class.getClassLoader().getResourceAsStream(filePath);
		Properties pps = new Properties();
		pps.load(stream);
		String value = pps.getProperty("api.v2.remote.url");
		
		System.out.println(value);
	}*/
}