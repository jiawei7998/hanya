package com.singlee.financial.cfets;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.pojo.RepoCrBean;
import com.singlee.financial.pojo.RepoOrBean;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.SLFBean;
import com.singlee.hessian.annotation.Capital;
import com.singlee.hessian.annotation.CapitalContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * 回购模块业务处理类,发送回购业务对象到Capital Web资金前置系统中
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
@Capital(context = CapitalContext.SINGLEE_API, uri = "/cfetsImixRepoExporter")
public interface ICfetsImixRepo {

    /**
     * 方法已重载.发送质押式回购交易到资金前置平台
     *
     * @param repoCr 质押回购业务对象
     * @param packet 报文对象  modify by shenzl add
     * @return
     */
    RetBean send(RepoCrBean repoCr, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 方法已重载.发送买断式回购交易到资金前置平台
     *
     * @param repoOr 买断式回购业务对象
     * @return
     */
    RetBean send(RepoOrBean repoOr, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

    /**
     * 方法已重载.发送买断式回购交易到资金前置平台
     *
     * @param repoOr 常備借貸便利业务对象
     * @return
     */
    RetBean send(SLFBean SLF, CfetsPackMsgBean cfetsPackMsg) throws RemoteConnectFailureException, Exception;

}
