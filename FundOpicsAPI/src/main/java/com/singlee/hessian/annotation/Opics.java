package com.singlee.hessian.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Opics {

	String description() default "";

	// 是否支持方法重载
	boolean overloadEnabled() default true;

	// 是否启用hessian2协议
	boolean hessian2() default true;

	// 连接时间
	long connectTimeOut() default -1;

	// 读取数据时间
	long readTimeOut() default -1;

	// 用户名
	String username() default "Opics";

	// 密码
	String password() default "Opics";

	// 用于服务端bean名称，也是客户端访问链接的后半部分 配置。如: /talentService
	String uri();

	// 客户端访问链接前半部分配置 如 http://localhost:8004/remote
	OpicsContext context();
}