package com.singlee.hessian;

import java.net.MalformedURLException;
import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.hessian.annotation.OpicsContext;

public class OpicsHessianUtil {
	private static HessianProxyFactory factory = null;

	@SuppressWarnings("unchecked")
	public static <T> T getService(Class<T> clazz, String url){
		if (null==factory) { factory = new HessianProxyFactory(); }
		try {
			url= OpicsContext.SINGLEE_API.getRemoteUrl()+url;
			return (T) factory.create(clazz, url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
    
}
