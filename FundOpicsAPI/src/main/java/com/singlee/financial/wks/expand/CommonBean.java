package com.singlee.financial.wks.expand;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 投资组合 产品+类型+PORT
	 */
	private String desk;
	/**
	 * 成本中心
	 */
	private String book;
	/**
	 * 交易员
	 */
	private String trad;
	/**
	 * 机构编号
	 */
	private String instId;
	
	/**
	 * 清算路径,清算账使用CNAP/DVP
	 */
	private String settmeans;
	/**
	 * 清算账户,清算账使用
	 */
	private String settacct;
	
    /**
     * 指定枚举值字段的匹配项
     */
    private FieldMapType fieldMapType;
    
    /**
     * 指定报文的处理方法
     */
    private SwiftMethod swiftMethod;
    
    /**
     * 备注
     */
    private String remark;

	public String getDesk() {
		return desk;
	}

	public void setDesk(String desk) {
		this.desk = desk;
	}

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettacct() {
		return settacct;
	}

	public void setSettacct(String settacct) {
		this.settacct = settacct;
	}

	public FieldMapType getFieldMapType() {
		return fieldMapType;
	}

	public void setFieldMapType(FieldMapType fieldMapType) {
		this.fieldMapType = fieldMapType;
	}

	public SwiftMethod getSwiftMethod() {
		return swiftMethod;
	}

	public void setSwiftMethod(SwiftMethod swiftMethod) {
		this.swiftMethod = swiftMethod;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
    
    
}
