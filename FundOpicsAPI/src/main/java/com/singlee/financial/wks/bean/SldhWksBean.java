package com.singlee.financial.wks.bean;

import java.math.BigDecimal;
import java.util.List;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;
import com.singlee.financial.wks.expand.SldaWksBean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 债券借贷
 * 
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SldhWksBean extends CommonBean {
	private static final long serialVersionUID = -21657151721286424L;

	/**
	 * 交易编号
	 */
	private String tradeNo;
	/**
	 * 计息基础
	 */
	private String basis;
	/**
	 * 交易货币
	 */
	private String ccy;
	/**
	 * 交易对手
	 */
	private String cno;
	/**
	 * 担保代码[不填写,系统默认ALL]
	 */
	private String CollCode;

	/**
	 * 费用金额
	 */
	private BigDecimal feeAmt;
	/**
	 * 费率
	 */
	private BigDecimal feePer8;

	/**
	 * 交易日
	 */
	private String dealDate;
	/**
	 * 起息日
	 */
	private String vdate;
	/**
	 * 到日期
	 */
	private String matDate;
	/**
	 * 付息日
	 */
	private String payIntDate;
	/**
	 * 买卖方向,只给借入/借出券方向即可
	 * 
	 * 情况1:借入券:P-买[负债L]/押出S-买[资产A]
	 * 
	 * 情况2:借出券:S-买[资产A]/押入P-买[负债L]
	 */
	private String ps;
	/**
	 * 债券托管账户
	 */
	private String safeKeepAcct;
	/**
	 * 操作员
	 */
	private String trad;
	/**
	 * 抵押债列表
	 * 
	 */
	private List<SldaWksBean> sldaList;
	/**
	 * 债券借贷费用对象
	 */
	private FeeWksBean feesWksBean;
	/**
	 * 标的债券[借入/出的债券]
	 */
	private SldaWksBean slda;
	/**
	 * 清算信息
	 */
	private SettleInfoWksBean settleAcct;
	/**
	 * 清算路径对应对象
	 */
	private SettleInfoWksBean settleInfoWksBean;
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getCollCode() {
		return CollCode;
	}
	public void setCollCode(String collCode) {
		CollCode = collCode;
	}
	public BigDecimal getFeeAmt() {
		return feeAmt;
	}
	public void setFeeAmt(BigDecimal feeAmt) {
		this.feeAmt = feeAmt;
	}
	public BigDecimal getFeePer8() {
		return feePer8;
	}
	public void setFeePer8(BigDecimal feePer8) {
		this.feePer8 = feePer8;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getMatDate() {
		return matDate;
	}
	public void setMatDate(String matDate) {
		this.matDate = matDate;
	}
	public String getPayIntDate() {
		return payIntDate;
	}
	public void setPayIntDate(String payIntDate) {
		this.payIntDate = payIntDate;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public String getSafeKeepAcct() {
		return safeKeepAcct;
	}
	public void setSafeKeepAcct(String safeKeepAcct) {
		this.safeKeepAcct = safeKeepAcct;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public List<SldaWksBean> getSldaList() {
		return sldaList;
	}
	public void setSldaList(List<SldaWksBean> sldaList) {
		this.sldaList = sldaList;
	}
	public FeeWksBean getFeesWksBean() {
		return feesWksBean;
	}
	public void setFeesWksBean(FeeWksBean feesWksBean) {
		this.feesWksBean = feesWksBean;
	}
	public SldaWksBean getSlda() {
		return slda;
	}
	public void setSlda(SldaWksBean slda) {
		this.slda = slda;
	}
	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}
	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}
	public SettleInfoWksBean getSettleInfoWksBean() {
		return settleInfoWksBean;
	}
	public void setSettleInfoWksBean(SettleInfoWksBean settleInfoWksBean) {
		this.settleInfoWksBean = settleInfoWksBean;
	}
	
	
	
}
