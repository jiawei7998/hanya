package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.IboWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/mmExporter")
public interface MmService {

	/**
	 * 交易录入
	 * 
	 * @param ibo
	 * @return
	 */
	SlOutBean saveEntry(IboWksBean ibo);

	/**
	 * 提前终止
	 * 
	 * @param ibo
	 * @return
	 */
	SlOutBean earlyTermination(IboWksBean ibo);

	/**
	 * 交易冲销
	 * 
	 * @param ibo
	 * @return
	 */
	SlOutBean reverseTrading(IboWksBean ibo);

	/**
	 * 调整本金金额，追加减少
	 * 
	 * @param ibo
	 * @return
	 */
	SlOutBean adaptPrinAmt(IboWksBean ibo);

	/**
	 * 展期
	 * 
	 * @param ibo
	 * @return
	 */
	SlOutBean adaptDueDate(IboWksBean ibo);

	/**
	 * 利率调整
	 * 
	 * @param ibo
	 * @return
	 */
	SlOutBean adjustRate(IboWksBean ibo);
}
