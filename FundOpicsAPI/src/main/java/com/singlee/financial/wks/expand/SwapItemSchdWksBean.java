package com.singlee.financial.wks.expand;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * 互换收付息对象
 * @author xuqq
 *20200206
 */
@Data
public class SwapItemSchdWksBean implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private String seq;//明细顺序号(腿序号)
    private String schdSeq;//收付息记划条数：不需要传值
	private String schdType;//类型标识：V-首期,P-中间付息,R-中间收息,M-到期
	private String compound;//复利标志:F=复利/N=单利
	private String endDate;//利息截至日期
	private String basis;//计息基础
	private String rateType;//固定/浮动
	private BigDecimal implintAmt;//远期利率利息金额
	private BigDecimal implintRate8;//远期利率
	private BigDecimal interestRate;//利率
	private String intsacct;//清算账号
	private String intsmeans;//清算方式
	private String beginDate;//利息开始日期
	private String payDate;//付款日期
	private BigDecimal netipayAmt;//收付利息差额：自己循环计算
	private String tradeDirection;//收/付款标识
	private BigDecimal posnintamt;//货币头寸利息金额：补0
	private BigDecimal prinAmt;//名义本金
	private String ccy;//名义币种/利息币种
	private BigDecimal interestAmt;//名义币种利息金额/利息金额
	private String rateCode;//利率代码
	private String fixingDate;//利率重定日期是 利率重定日期:前一个工作日
	private String actualFixingDate;//利率重定生效日期
	private String verdate;//复核日期：固定端是当前时间，浮动端v,确定利息的
	private BigDecimal flatintcompamt;//当期复利与单利的差额
	private BigDecimal sumcompoundamt;//当期复利利息累加金额
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getSchdSeq() {
		return schdSeq;
	}
	public void setSchdSeq(String schdSeq) {
		this.schdSeq = schdSeq;
	}
	public String getSchdType() {
		return schdType;
	}
	public void setSchdType(String schdType) {
		this.schdType = schdType;
	}
	public String getCompound() {
		return compound;
	}
	public void setCompound(String compound) {
		this.compound = compound;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public BigDecimal getImplintAmt() {
		return implintAmt;
	}
	public void setImplintAmt(BigDecimal implintAmt) {
		this.implintAmt = implintAmt;
	}
	public BigDecimal getImplintRate8() {
		return implintRate8;
	}
	public void setImplintRate8(BigDecimal implintRate8) {
		this.implintRate8 = implintRate8;
	}
	public BigDecimal getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}
	public String getIntsacct() {
		return intsacct;
	}
	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}
	public String getIntsmeans() {
		return intsmeans;
	}
	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	public String getPayDate() {
		return payDate;
	}
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}
	public BigDecimal getNetipayAmt() {
		return netipayAmt;
	}
	public void setNetipayAmt(BigDecimal netipayAmt) {
		this.netipayAmt = netipayAmt;
	}
	public String getTradeDirection() {
		return tradeDirection;
	}
	public void setTradeDirection(String tradeDirection) {
		this.tradeDirection = tradeDirection;
	}
	public BigDecimal getPosnintamt() {
		return posnintamt;
	}
	public void setPosnintamt(BigDecimal posnintamt) {
		this.posnintamt = posnintamt;
	}
	public BigDecimal getPrinAmt() {
		return prinAmt;
	}
	public void setPrinAmt(BigDecimal prinAmt) {
		this.prinAmt = prinAmt;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getInterestAmt() {
		return interestAmt;
	}
	public void setInterestAmt(BigDecimal interestAmt) {
		this.interestAmt = interestAmt;
	}
	public String getRateCode() {
		return rateCode;
	}
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}
	public String getFixingDate() {
		return fixingDate;
	}
	public void setFixingDate(String fixingDate) {
		this.fixingDate = fixingDate;
	}
	public String getActualFixingDate() {
		return actualFixingDate;
	}
	public void setActualFixingDate(String actualFixingDate) {
		this.actualFixingDate = actualFixingDate;
	}
	public String getVerdate() {
		return verdate;
	}
	public void setVerdate(String verdate) {
		this.verdate = verdate;
	}
	public BigDecimal getFlatintcompamt() {
		return flatintcompamt;
	}
	public void setFlatintcompamt(BigDecimal flatintcompamt) {
		this.flatintcompamt = flatintcompamt;
	}
	public BigDecimal getSumcompoundamt() {
		return sumcompoundamt;
	}
	public void setSumcompoundamt(BigDecimal sumcompoundamt) {
		this.sumcompoundamt = sumcompoundamt;
	}
	
	
	
}
