package com.singlee.financial.wks.expand;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class IboPrinSchdWksBean implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -4394226596676170980L;
    /**
     * 交易的编号
     */
    private String tradeNo;
    /**
     * 现金流类型
     */
    private String cfType;
    /**
     * 收付方向
     */
    private String payDirection;
    /**
     * 期末支付金额
     */
    private BigDecimal endPrincipal;
    /**
     * 序号
     */
    private String seqNumber;
    /**
     * 现金流事件
     */
    private String cfEvent;
    /**
     * 实际支付日期
     */
    private String paymentDate;
    /**
     * 本金
     */
    private BigDecimal principal;
    /**
     * 利率
     */
    private BigDecimal intRate;
    /**
     * 计息基准
     */
    private String dayCount;
    /**
     * 清算信息
     */
    private SettleInfoWksBean settleAcct;
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getPayDirection() {
		return payDirection;
	}
	public void setPayDirection(String payDirection) {
		this.payDirection = payDirection;
	}
	public BigDecimal getEndPrincipal() {
		return endPrincipal;
	}
	public void setEndPrincipal(BigDecimal endPrincipal) {
		this.endPrincipal = endPrincipal;
	}
	public String getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(String seqNumber) {
		this.seqNumber = seqNumber;
	}
	public String getCfEvent() {
		return cfEvent;
	}
	public void setCfEvent(String cfEvent) {
		this.cfEvent = cfEvent;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public BigDecimal getPrincipal() {
		return principal;
	}
	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}
	public BigDecimal getIntRate() {
		return intRate;
	}
	public void setIntRate(BigDecimal intRate) {
		this.intRate = intRate;
	}
	public String getDayCount() {
		return dayCount;
	}
	public void setDayCount(String dayCount) {
		this.dayCount = dayCount;
	}
	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}
	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}
    
    
}
