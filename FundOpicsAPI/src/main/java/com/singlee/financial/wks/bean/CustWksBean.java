package com.singlee.financial.wks.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import com.singlee.financial.wks.expand.CommonBean;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CustWksBean extends CommonBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 客户编号 required
	 */
	private String custId;
	/**
	 * 是否有效 required
	 */
	private boolean active;
	/**
	 * 客户代码 required
	 */
	private String custCode;
	/**
	 * 核心客户号 optional
	 */
	private String custCoreNo;
	/**
	 * 客户全称 required
	 */
	private String custFullname;
	/**
	 * 客户全称 required
	 */
	private String bizStatus;
	/**
	 * 客户简称 required
	 */
	private String custShortname;
	/**
	 * 英文全称 optional
	 */
	private String custEfullname;
	/**
	 * 英文简称 optional
	 */
	private String custEshortname;
	/**
	 * ECIF编号 optional
	 */
	private String custEcifId;
	/**
	 * 第三方系统编号 optional
	 */
	private String custThirdId;
	/**
	 * 客户分类: 银行类金融机构/非银金融机构/集团企业/普通企业 required
	 */
	private String custCategory;
	/**
	 * 是否一级法人标志: 集团/总行 required
	 */
	private int isLegalLevel1;
	/**
	 * 上级法人客户编号 optional
	 */
	private String legalLevelupCustId;
	/**
	 * 是否债券发行人标志 required
	 */
	private int isBondIssuer;
	/**
	 * 债券发行人编号(TT_MKT_BOND_ISSUER) optional
	 */
	private String bondIssuerId;
	/**
	 * SWIFT代码 optional
	 */
	private String swiftCode;
	/**
	 * 大类 required
	 */
	private String custBigKind;
	/**
	 * 大类名称
	 */
	private String custBigKindName;
	/**
	 * 小类 required
	 */
	private String custSmallKind;
	/**
	 * 小类名称
	 */
	private String custSmallKindName;
	/**
	 * 地址 optional
	 */
	private String custAddress;
	/**
	 * 邮编 optional
	 */
	private String custZip;
	/**
	 * 业务归属客户经理 optional
	 */
	private String custUserId;
	/**
	 * 业务归属机构 optional
	 */
	private String custInstId;
	/**
	 * 国家 optional
	 */
	private String custCountry;
	/**
	 * 地区 optional
	 */
	private String custArea;
	/**
	 * 上级法人客户名称
	 */
	private String legalLevelupCustName;
	/**
	 * 客户资产规模
	 */
	private double assetsScale;
	/**
	 * 存放对手在交易中心的机构号
	 */
	private String cfetsInstId;
	/**
	 * 会计类型
	 */
	private String acctngtype;
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getCustCoreNo() {
		return custCoreNo;
	}
	public void setCustCoreNo(String custCoreNo) {
		this.custCoreNo = custCoreNo;
	}
	public String getCustFullname() {
		return custFullname;
	}
	public void setCustFullname(String custFullname) {
		this.custFullname = custFullname;
	}
	public String getBizStatus() {
		return bizStatus;
	}
	public void setBizStatus(String bizStatus) {
		this.bizStatus = bizStatus;
	}
	public String getCustShortname() {
		return custShortname;
	}
	public void setCustShortname(String custShortname) {
		this.custShortname = custShortname;
	}
	public String getCustEfullname() {
		return custEfullname;
	}
	public void setCustEfullname(String custEfullname) {
		this.custEfullname = custEfullname;
	}
	public String getCustEshortname() {
		return custEshortname;
	}
	public void setCustEshortname(String custEshortname) {
		this.custEshortname = custEshortname;
	}
	public String getCustEcifId() {
		return custEcifId;
	}
	public void setCustEcifId(String custEcifId) {
		this.custEcifId = custEcifId;
	}
	public String getCustThirdId() {
		return custThirdId;
	}
	public void setCustThirdId(String custThirdId) {
		this.custThirdId = custThirdId;
	}
	public String getCustCategory() {
		return custCategory;
	}
	public void setCustCategory(String custCategory) {
		this.custCategory = custCategory;
	}
	public int getIsLegalLevel1() {
		return isLegalLevel1;
	}
	public void setIsLegalLevel1(int isLegalLevel1) {
		this.isLegalLevel1 = isLegalLevel1;
	}
	public String getLegalLevelupCustId() {
		return legalLevelupCustId;
	}
	public void setLegalLevelupCustId(String legalLevelupCustId) {
		this.legalLevelupCustId = legalLevelupCustId;
	}
	public int getIsBondIssuer() {
		return isBondIssuer;
	}
	public void setIsBondIssuer(int isBondIssuer) {
		this.isBondIssuer = isBondIssuer;
	}
	public String getBondIssuerId() {
		return bondIssuerId;
	}
	public void setBondIssuerId(String bondIssuerId) {
		this.bondIssuerId = bondIssuerId;
	}
	public String getSwiftCode() {
		return swiftCode;
	}
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}
	public String getCustBigKind() {
		return custBigKind;
	}
	public void setCustBigKind(String custBigKind) {
		this.custBigKind = custBigKind;
	}
	public String getCustBigKindName() {
		return custBigKindName;
	}
	public void setCustBigKindName(String custBigKindName) {
		this.custBigKindName = custBigKindName;
	}
	public String getCustSmallKind() {
		return custSmallKind;
	}
	public void setCustSmallKind(String custSmallKind) {
		this.custSmallKind = custSmallKind;
	}
	public String getCustSmallKindName() {
		return custSmallKindName;
	}
	public void setCustSmallKindName(String custSmallKindName) {
		this.custSmallKindName = custSmallKindName;
	}
	public String getCustAddress() {
		return custAddress;
	}
	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}
	public String getCustZip() {
		return custZip;
	}
	public void setCustZip(String custZip) {
		this.custZip = custZip;
	}
	public String getCustUserId() {
		return custUserId;
	}
	public void setCustUserId(String custUserId) {
		this.custUserId = custUserId;
	}
	public String getCustInstId() {
		return custInstId;
	}
	public void setCustInstId(String custInstId) {
		this.custInstId = custInstId;
	}
	public String getCustCountry() {
		return custCountry;
	}
	public void setCustCountry(String custCountry) {
		this.custCountry = custCountry;
	}
	public String getCustArea() {
		return custArea;
	}
	public void setCustArea(String custArea) {
		this.custArea = custArea;
	}
	public String getLegalLevelupCustName() {
		return legalLevelupCustName;
	}
	public void setLegalLevelupCustName(String legalLevelupCustName) {
		this.legalLevelupCustName = legalLevelupCustName;
	}
	public double getAssetsScale() {
		return assetsScale;
	}
	public void setAssetsScale(double assetsScale) {
		this.assetsScale = assetsScale;
	}
	public String getCfetsInstId() {
		return cfetsInstId;
	}
	public void setCfetsInstId(String cfetsInstId) {
		this.cfetsInstId = cfetsInstId;
	}
	public String getAcctngtype() {
		return acctngtype;
	}
	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}
	
	
}
