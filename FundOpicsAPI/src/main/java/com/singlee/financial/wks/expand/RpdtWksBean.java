package com.singlee.financial.wks.expand;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * 回购券信息
 * @author xuqq
 *
 */
@Data
public class RpdtWksBean implements Serializable {

	private static final long serialVersionUID = -2650277332184350472L;
	private String secid;//债券id
	private BigDecimal qty;//数量
	private BigDecimal ccyamt;//本金金额
	private BigDecimal purchintamt;//买回应计利息金额/债券本身应计利息
	private BigDecimal repointamt;//回购交易产生的利息
	private BigDecimal vdcleanprice8;//起息日净价
	private BigDecimal vddirtyprice8;//起息日全价
	private BigDecimal mdcleanprice8;//到期日净价
	private BigDecimal mddirtyprice8;//到期日全价
	private BigDecimal disprice8;//票面价格
	private BigDecimal commprocamt;//开始收入金额
	private BigDecimal matprocamt;//到期收入金额
	private BigDecimal hcpurchintamt;//初始基本利率:利息金额
	private String cost;//质押债本身投资组合-COST
	private BigDecimal convintamt;//兑换利息：一天的利息
	private BigDecimal yield8;//收益率
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public BigDecimal getQty() {
		return qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
	public BigDecimal getCcyamt() {
		return ccyamt;
	}
	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}
	public BigDecimal getPurchintamt() {
		return purchintamt;
	}
	public void setPurchintamt(BigDecimal purchintamt) {
		this.purchintamt = purchintamt;
	}
	public BigDecimal getRepointamt() {
		return repointamt;
	}
	public void setRepointamt(BigDecimal repointamt) {
		this.repointamt = repointamt;
	}
	public BigDecimal getVdcleanprice8() {
		return vdcleanprice8;
	}
	public void setVdcleanprice8(BigDecimal vdcleanprice8) {
		this.vdcleanprice8 = vdcleanprice8;
	}
	public BigDecimal getVddirtyprice8() {
		return vddirtyprice8;
	}
	public void setVddirtyprice8(BigDecimal vddirtyprice8) {
		this.vddirtyprice8 = vddirtyprice8;
	}
	public BigDecimal getMdcleanprice8() {
		return mdcleanprice8;
	}
	public void setMdcleanprice8(BigDecimal mdcleanprice8) {
		this.mdcleanprice8 = mdcleanprice8;
	}
	public BigDecimal getMddirtyprice8() {
		return mddirtyprice8;
	}
	public void setMddirtyprice8(BigDecimal mddirtyprice8) {
		this.mddirtyprice8 = mddirtyprice8;
	}
	public BigDecimal getDisprice8() {
		return disprice8;
	}
	public void setDisprice8(BigDecimal disprice8) {
		this.disprice8 = disprice8;
	}
	public BigDecimal getCommprocamt() {
		return commprocamt;
	}
	public void setCommprocamt(BigDecimal commprocamt) {
		this.commprocamt = commprocamt;
	}
	public BigDecimal getMatprocamt() {
		return matprocamt;
	}
	public void setMatprocamt(BigDecimal matprocamt) {
		this.matprocamt = matprocamt;
	}
	public BigDecimal getHcpurchintamt() {
		return hcpurchintamt;
	}
	public void setHcpurchintamt(BigDecimal hcpurchintamt) {
		this.hcpurchintamt = hcpurchintamt;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public BigDecimal getConvintamt() {
		return convintamt;
	}
	public void setConvintamt(BigDecimal convintamt) {
		this.convintamt = convintamt;
	}
	public BigDecimal getYield8() {
		return yield8;
	}
	public void setYield8(BigDecimal yield8) {
		this.yield8 = yield8;
	}
	
	
}
