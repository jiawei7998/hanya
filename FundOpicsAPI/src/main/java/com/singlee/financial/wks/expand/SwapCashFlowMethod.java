package com.singlee.financial.wks.expand;

public enum SwapCashFlowMethod {

	/**
	 * 计算浮动端第一个重定周期
	 */
	First,
	
	/**
	 * 计算浮动端从起息日到当前系统日期部分利息
	 */
	ResetAccrual,
	
	/**
	 * 计算浮动端已重定部分以及未重定利率的现金流
	 */
	All
}
