package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.OtcWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/otcExporter")
public interface OtcService {

    /**
     * 交易录入
     * 
     * @param otc
     * @return
     */
    SlOutBean saveEntry(OtcWksBean otc);

    /**
     * 行权
     * 
     * @param otc
     * @return
     */
    SlOutBean exercise(OtcWksBean otc);

    /**
     * 冲销
     * 
     * @param otc
     * @return
     */
    SlOutBean reverseTrading(OtcWksBean otc);
}
