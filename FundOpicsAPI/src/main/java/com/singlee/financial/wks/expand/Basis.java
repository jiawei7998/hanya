package com.singlee.financial.wks.expand;
/**
 * 计息基础
 * @author chenxh
 *
 */
public enum Basis {
	/**
	 * Actual/360
	 */
	A360,
	
	/**
	 * Actual/365
	 */
	A365,
	
	/**
	 * Actual/Actual
	 */
	ACTUAL,
	
	/**
	 * 30/360
	 */
	BOND,
	
	/**
	 * 30E/360 (Eurobond)
	 */
	EBOND,
	
	/**
	 * Actual 365 or Actual 366
	 */
	ACT365
}
