package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.SecmWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/secmmExporter")
public interface SecmmService {

	/**
     * 交易录入
     * 
     * @param ibo
     * @return
     */
    SlOutBean saveEntry(SecmWksBean secmWksBean);
}
