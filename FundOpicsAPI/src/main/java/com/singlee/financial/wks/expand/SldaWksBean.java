package com.singlee.financial.wks.expand;

import java.io.Serializable;
import java.math.BigDecimal;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SldaWksBean extends CommonBean implements Serializable {

	/**
	 * 债券借贷 标的债/质押债
	 */
	private static final long serialVersionUID = -8026891693862550360L;
    private String 		ccy;//币种
    private BigDecimal 	clprice_8;//债券净价
    private String 		cost;//成本中心
    private String 		dealtime;//交易时间
	private String 		dispricerateind;//全价/净价标识，c-净价，d-全价录入的价格是以全价计算还是以净价计算
	private BigDecimal 	drprice_8;//全价金额
	private BigDecimal 	faceamt;//交易券面总额
	private BigDecimal 	fillprice_8;//取drprice_8
	private BigDecimal 	haircut_8;//折算比例
	private String 		haircutterms;//折算比例计算规则，m-乘；d-除
	private String 		invtype;//借入/借出债券投资类型
	private String 		mdate;//到期日，取sldh.matdate
	private String 		notccy;//交易货币
	private String 		port;//投资组合
	private String 		ps;//交易方向，p-买入，s-卖出
	private BigDecimal 	qty;//借入/借出债券交易手数
	private String 		secid;//债券代码
	private String 		sectypeind;//押券标识
	private String 		settccy;//清算货币，取notccy
	private String 		vdate;//首期交割日期
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getClprice_8() {
		return clprice_8;
	}
	public void setClprice_8(BigDecimal clprice_8) {
		this.clprice_8 = clprice_8;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getDealtime() {
		return dealtime;
	}
	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}
	public String getDispricerateind() {
		return dispricerateind;
	}
	public void setDispricerateind(String dispricerateind) {
		this.dispricerateind = dispricerateind;
	}
	public BigDecimal getDrprice_8() {
		return drprice_8;
	}
	public void setDrprice_8(BigDecimal drprice_8) {
		this.drprice_8 = drprice_8;
	}
	public BigDecimal getFaceamt() {
		return faceamt;
	}
	public void setFaceamt(BigDecimal faceamt) {
		this.faceamt = faceamt;
	}
	public BigDecimal getFillprice_8() {
		return fillprice_8;
	}
	public void setFillprice_8(BigDecimal fillprice_8) {
		this.fillprice_8 = fillprice_8;
	}
	public BigDecimal getHaircut_8() {
		return haircut_8;
	}
	public void setHaircut_8(BigDecimal haircut_8) {
		this.haircut_8 = haircut_8;
	}
	public String getHaircutterms() {
		return haircutterms;
	}
	public void setHaircutterms(String haircutterms) {
		this.haircutterms = haircutterms;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getNotccy() {
		return notccy;
	}
	public void setNotccy(String notccy) {
		this.notccy = notccy;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public BigDecimal getQty() {
		return qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getSectypeind() {
		return sectypeind;
	}
	public void setSectypeind(String sectypeind) {
		this.sectypeind = sectypeind;
	}
	public String getSettccy() {
		return settccy;
	}
	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	
	
}
