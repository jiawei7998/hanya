package com.singlee.financial.wks.bean;

import java.io.Serializable;
import java.util.List;

import com.singlee.financial.wks.expand.FxdItemWksBean;

import lombok.Data;

/**
 * 外汇交易数据
 * 
 * @author shenzl
 * @date 2020/01/10
 */
@Data
public class FxdWksBean implements Serializable {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;
	/**
	 * 审批编号
	 */
	private String approveNo;

	private List<FxdItemWksBean> itemBeanLst;

	public String getApproveNo() {
		return approveNo;
	}

	public void setApproveNo(String approveNo) {
		this.approveNo = approveNo;
	}

	public List<FxdItemWksBean> getItemBeanLst() {
		return itemBeanLst;
	}

	public void setItemBeanLst(List<FxdItemWksBean> itemBeanLst) {
		this.itemBeanLst = itemBeanLst;
	}
	
	
}
