package com.singlee.financial.wks.bean;

import java.math.BigDecimal;

import com.singlee.financial.wks.expand.CommonBean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FeeWksBean extends CommonBean {

	private static final long serialVersionUID = -21657151721286424L;
	/**
	 * 费用类型 收费/付费 映射OPICS的PROD/TYPE
	 */
	private String feeDir;
	/**
	 * 交易的编号,外部费用编号
	 */
	private String tradeNo;
	/**
	 * 费用货币代码
	 */
	private String ccy;

	/**
	 * 费用金额
	 */
	private BigDecimal ccyAmt;

	/**
	 * 原始交易的交易对手编号
	 */
	private String cno;

	/**
	 * 费用开始日期
	 */
	private String startDate;

	/**
	 * 费用结束日期
	 */
	private String endDate;
	/**
	 * 费率
	 */
	private BigDecimal feePer8;

	/**
	 * 账户方式 C-CASH 现金方式,不进行账务处理 A-AMORT 摊销方式处理
	 */
	private String acctgMethod;
	/**
	 * 摊销方法, 当acctgMethod=A时,必填 S-直线摊销 C-摊余固定收益率
	 */
	private String amortMethod;
	/**
	 * 计息基础 当amortMethod=C时,必填
	 */
	private String basis;
	/**
	 * 冲销原因
	 */
	private String revtext;
	public String getFeeDir() {
		return feeDir;
	}
	public void setFeeDir(String feeDir) {
		this.feeDir = feeDir;
	}
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getCcyAmt() {
		return ccyAmt;
	}
	public void setCcyAmt(BigDecimal ccyAmt) {
		this.ccyAmt = ccyAmt;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public BigDecimal getFeePer8() {
		return feePer8;
	}
	public void setFeePer8(BigDecimal feePer8) {
		this.feePer8 = feePer8;
	}
	public String getAcctgMethod() {
		return acctgMethod;
	}
	public void setAcctgMethod(String acctgMethod) {
		this.acctgMethod = acctgMethod;
	}
	public String getAmortMethod() {
		return amortMethod;
	}
	public void setAmortMethod(String amortMethod) {
		this.amortMethod = amortMethod;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getRevtext() {
		return revtext;
	}
	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}
	
	
}
