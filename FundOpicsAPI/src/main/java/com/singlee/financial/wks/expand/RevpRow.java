package com.singlee.financial.wks.expand;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RevpRow implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5862905665475165819L;

	private String br;
	
	private String ccy;
	
	private String terms;
	
	private String mty;
	
	private Date mtyDate;
	
	private Date mtyModifyDate;
	
	private BigDecimal rate;
	
	private BigDecimal rateBp;
	
	private Date lstmntdate;
	
	private Date spotDate;
	
	private BigDecimal spotRate;
	
	private String baseCcy;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getMty() {
		return mty;
	}

	public void setMty(String mty) {
		this.mty = mty;
	}

	public Date getMtyDate() {
		return mtyDate;
	}

	public void setMtyDate(Date mtyDate) {
		this.mtyDate = mtyDate;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

	public BigDecimal getRateBp() {
		return rateBp;
	}

	public void setRateBp(BigDecimal rateBp) {
		this.rateBp = rateBp;
	}

	public BigDecimal getSpotRate() {
		return spotRate;
	}

	public void setSpotRate(BigDecimal spotRate) {
		this.spotRate = spotRate;
	}

	public String getBaseCcy() {
		return baseCcy;
	}

	public void setBaseCcy(String baseCcy) {
		this.baseCcy = baseCcy;
	}

	public Date getMtyModifyDate() {
		return mtyModifyDate;
	}

	public void setMtyModifyDate(Date mtyModifyDate) {
		this.mtyModifyDate = mtyModifyDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RevpRow [br=");
		builder.append(br);
		builder.append(", ccy=");
		builder.append(ccy);
		builder.append(", baseCcy=");
		builder.append(baseCcy);
		builder.append(", terms=");
		builder.append(terms);
		builder.append(", spotDate=");
		builder.append(String.format("%tF",spotDate));
		builder.append(", spotRate=");
		builder.append(spotRate);
		builder.append(", mty=");
		builder.append(mty);
		builder.append(", mtyDate=");
		builder.append(String.format("%tF",mtyDate));
		builder.append(", mtyModifyDate=");
		builder.append(String.format("%tF",mtyModifyDate));
		builder.append(", rateBp=");
		builder.append(rateBp);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", lstmntdate=");
		builder.append(String.format("%tF",lstmntdate));
		builder.append("]");
		return builder.toString();
	}
	
}
