package com.singlee.financial.wks.intfc;

import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * @Author zhangkai
 * @Description
 * @Date
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/adnmMidService")
public interface AdnmMidService {
    /**
     * 生成客户号
     * @return
     */
    String createCustNo();

}
