package com.singlee.financial.wks.expand;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class IboIntSchdWksBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5644815080615732247L;
    /**
     * 交易的编号
     */
    private String tradeNo;
    /**
     * 开始日期
     */
    private String refBeginDate;
    /**
     * 结束日期
     */
    private String refEndDate;
    /**
     * 理论支付日期
     */
    private String theoryPaymentDate;
    /**
     * 现金流类型
     */
    private String cfType;
    /**
     * 收付方向
     */
    private String payDirection;
    /**
     * 收付金额
     */
    private BigDecimal payAmt;
    /**
     * 利率
     */
    private BigDecimal executeRate;
    /**
     * 应计算利息
     */
    private BigDecimal interest;
    /**
     * 序号
     */
    private String seqNumber;

    /**
     * 计息基准
     */
    private String dayCount;
    /**
     * 实际支付日期
     */
    private String paymentDate;
    /**
     * 实际收到利息
     */
    private BigDecimal ActualRamt;
    /**
     * 清算信息
     */
    private SettleInfoWksBean settleAcct;
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getRefBeginDate() {
		return refBeginDate;
	}
	public void setRefBeginDate(String refBeginDate) {
		this.refBeginDate = refBeginDate;
	}
	public String getRefEndDate() {
		return refEndDate;
	}
	public void setRefEndDate(String refEndDate) {
		this.refEndDate = refEndDate;
	}
	public String getTheoryPaymentDate() {
		return theoryPaymentDate;
	}
	public void setTheoryPaymentDate(String theoryPaymentDate) {
		this.theoryPaymentDate = theoryPaymentDate;
	}
	public String getCfType() {
		return cfType;
	}
	public void setCfType(String cfType) {
		this.cfType = cfType;
	}
	public String getPayDirection() {
		return payDirection;
	}
	public void setPayDirection(String payDirection) {
		this.payDirection = payDirection;
	}
	public BigDecimal getPayAmt() {
		return payAmt;
	}
	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}
	public BigDecimal getExecuteRate() {
		return executeRate;
	}
	public void setExecuteRate(BigDecimal executeRate) {
		this.executeRate = executeRate;
	}
	public BigDecimal getInterest() {
		return interest;
	}
	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}
	public String getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(String seqNumber) {
		this.seqNumber = seqNumber;
	}
	public String getDayCount() {
		return dayCount;
	}
	public void setDayCount(String dayCount) {
		this.dayCount = dayCount;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public BigDecimal getActualRamt() {
		return ActualRamt;
	}
	public void setActualRamt(BigDecimal actualRamt) {
		ActualRamt = actualRamt;
	}
	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}
	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}
    
    
    
}
