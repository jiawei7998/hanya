package com.singlee.financial.wks.expand;

import java.io.Serializable;

import lombok.Data;

@Data
public class SchduleRefix implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// br
	private String br;
	// 交易流水号
	private String dealno;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	
	
}
