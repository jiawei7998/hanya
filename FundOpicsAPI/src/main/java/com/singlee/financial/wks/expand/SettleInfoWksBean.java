package com.singlee.financial.wks.expand;

import java.io.Serializable;

import lombok.Data;

@Data
public class SettleInfoWksBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4289274712801550496L;
	/**
	 * 本方成交货币开户行biccode
	 */
	private String hostCcyAwBiccode;
	/**
	 * 本方成交货币开户行名称
	 */
	private String hostCcyAwName;
	/**
	 * 本方成交货币开户行在中间行的资金账号
	 */
	private String hostCcyAwAcctno;
	/**
	 * 本方成交货币收款行biccode
	 */
	private String hostCcyBeBiccode;
	/**
	 * 本方成交货币收款行行名称
	 */
	private String hostCcyBeName;
	/**
	 * 本方成交货币收款行在开户行的资金账号
	 */
	private String hostCcyBeAcctno;
	/**
	 * 本方成交货币中间行biccode
	 */
	private String hostCcyInBiccode;
	/**
	 * 本方成交货币中间行名称
	 */
	private String hostCcyInName;
	/**
	 * 本方成交货币中间行在其开户行的账号
	 */
	private String hostCcyInAcctno;
	/**
	 * 本方成交货币附言
	 */
	private String hostCcyPost;
	/**
	 * 本方对应货币开户行名
	 */
	private String hostCtrAwBiccode;
	/**
	 * 本方对应货币开户行biccode
	 */
	private String hostCtrAwName;
	/**
	 * 本方对应货币开户行在中间行的资金账号
	 */
	private String hostCtrAwAcctno;
	/**
	 * 本方对应货币收款行biccode
	 */
	private String hostCtrBeBiccode;
	/**
	 * 本方对应货币收款行行名称
	 */
	private String hostCtrBeName;
	/**
	 * 本方对应货币收款行在开户行的资金账号
	 */
	private String hostCtrBeAcctno;
	/**
	 * 本方对应货币中间行BICCODE
	 */
	private String hostCtrInBiccode;
	/**
	 * 本方对应货币中间行名称
	 */
	private String hostCtrInName;
	/**
	 * 本方对应货币中间行在其开户行的账号
	 */
	private String hostCtrInAcctno;
	/**
	 * 本方对应货币附言
	 */
	private String hostCtrPost;

	/**
	 * 对方成交货币开户行biccode
	 */
	private String guestCcyAwBiccode;
	/**
	 * 对方成交货币开户行名称
	 */
	private String guestCcyAwName;
	/**
	 * 对方成交货币开户行在中间行的资金账号
	 */
	private String guestCcyAwAcctno;
	/**
	 * 对方成交货币收款行biccode
	 */
	private String guestCcyBeBiccode;
	/**
	 * 对方成交货币收款行行名称
	 */
	private String guestCcyBeName;
	/**
	 * 对方成交货币收款行在开户行的资金账号
	 */
	private String guestCcyBeAcctno;
	/**
	 * 对方成交货币中间行biccode
	 */
	private String guestCcyInBiccode;
	/**
	 * 对方成交货币中间行名称
	 */
	private String guestCcyInName;
	/**
	 * 对方成交货币中间行在其开户行的账号
	 */
	private String guestCcyInAcctno;
	/**
	 * 对方成交货币附言
	 */
	private String guestCcyPost;
	/**
	 * 对方对应货币开户行名
	 */
	private String guestCtrAwBiccode;
	/**
	 * 对方对应货币开户行biccode
	 */
	private String guestCtrAwName;
	/**
	 * 对方对应货币开户行在中间行的资金账号
	 */
	private String guestCtrAwAcctno;
	/**
	 * 对方对应货币收款行biccode
	 */
	private String guestCtrBeBiccode;
	/**
	 * 对方对应货币收款行行名称
	 */
	private String guestCtrBeName;
	/**
	 * 对方对应货币收款行在开户行的资金账号
	 */
	private String guestCtrBeAcctno;
	/**
	 * 对方对应货币中间行BICCODE
	 */
	private String guestCtrInBiccode;
	/**
	 * 对方对应货币中间行名称
	 */
	private String guestCtrInName;
	/**
	 * 对方对应货币中间行在其开户行的账号
	 */
	private String guestCtrInAcctno;
	/**
	 * 对方对应货币附言
	 */
	private String guestCtrPost;
	public String getHostCcyAwBiccode() {
		return hostCcyAwBiccode;
	}
	public void setHostCcyAwBiccode(String hostCcyAwBiccode) {
		this.hostCcyAwBiccode = hostCcyAwBiccode;
	}
	public String getHostCcyAwName() {
		return hostCcyAwName;
	}
	public void setHostCcyAwName(String hostCcyAwName) {
		this.hostCcyAwName = hostCcyAwName;
	}
	public String getHostCcyAwAcctno() {
		return hostCcyAwAcctno;
	}
	public void setHostCcyAwAcctno(String hostCcyAwAcctno) {
		this.hostCcyAwAcctno = hostCcyAwAcctno;
	}
	public String getHostCcyBeBiccode() {
		return hostCcyBeBiccode;
	}
	public void setHostCcyBeBiccode(String hostCcyBeBiccode) {
		this.hostCcyBeBiccode = hostCcyBeBiccode;
	}
	public String getHostCcyBeName() {
		return hostCcyBeName;
	}
	public void setHostCcyBeName(String hostCcyBeName) {
		this.hostCcyBeName = hostCcyBeName;
	}
	public String getHostCcyBeAcctno() {
		return hostCcyBeAcctno;
	}
	public void setHostCcyBeAcctno(String hostCcyBeAcctno) {
		this.hostCcyBeAcctno = hostCcyBeAcctno;
	}
	public String getHostCcyInBiccode() {
		return hostCcyInBiccode;
	}
	public void setHostCcyInBiccode(String hostCcyInBiccode) {
		this.hostCcyInBiccode = hostCcyInBiccode;
	}
	public String getHostCcyInName() {
		return hostCcyInName;
	}
	public void setHostCcyInName(String hostCcyInName) {
		this.hostCcyInName = hostCcyInName;
	}
	public String getHostCcyInAcctno() {
		return hostCcyInAcctno;
	}
	public void setHostCcyInAcctno(String hostCcyInAcctno) {
		this.hostCcyInAcctno = hostCcyInAcctno;
	}
	public String getHostCcyPost() {
		return hostCcyPost;
	}
	public void setHostCcyPost(String hostCcyPost) {
		this.hostCcyPost = hostCcyPost;
	}
	public String getHostCtrAwBiccode() {
		return hostCtrAwBiccode;
	}
	public void setHostCtrAwBiccode(String hostCtrAwBiccode) {
		this.hostCtrAwBiccode = hostCtrAwBiccode;
	}
	public String getHostCtrAwName() {
		return hostCtrAwName;
	}
	public void setHostCtrAwName(String hostCtrAwName) {
		this.hostCtrAwName = hostCtrAwName;
	}
	public String getHostCtrAwAcctno() {
		return hostCtrAwAcctno;
	}
	public void setHostCtrAwAcctno(String hostCtrAwAcctno) {
		this.hostCtrAwAcctno = hostCtrAwAcctno;
	}
	public String getHostCtrBeBiccode() {
		return hostCtrBeBiccode;
	}
	public void setHostCtrBeBiccode(String hostCtrBeBiccode) {
		this.hostCtrBeBiccode = hostCtrBeBiccode;
	}
	public String getHostCtrBeName() {
		return hostCtrBeName;
	}
	public void setHostCtrBeName(String hostCtrBeName) {
		this.hostCtrBeName = hostCtrBeName;
	}
	public String getHostCtrBeAcctno() {
		return hostCtrBeAcctno;
	}
	public void setHostCtrBeAcctno(String hostCtrBeAcctno) {
		this.hostCtrBeAcctno = hostCtrBeAcctno;
	}
	public String getHostCtrInBiccode() {
		return hostCtrInBiccode;
	}
	public void setHostCtrInBiccode(String hostCtrInBiccode) {
		this.hostCtrInBiccode = hostCtrInBiccode;
	}
	public String getHostCtrInName() {
		return hostCtrInName;
	}
	public void setHostCtrInName(String hostCtrInName) {
		this.hostCtrInName = hostCtrInName;
	}
	public String getHostCtrInAcctno() {
		return hostCtrInAcctno;
	}
	public void setHostCtrInAcctno(String hostCtrInAcctno) {
		this.hostCtrInAcctno = hostCtrInAcctno;
	}
	public String getHostCtrPost() {
		return hostCtrPost;
	}
	public void setHostCtrPost(String hostCtrPost) {
		this.hostCtrPost = hostCtrPost;
	}
	public String getGuestCcyAwBiccode() {
		return guestCcyAwBiccode;
	}
	public void setGuestCcyAwBiccode(String guestCcyAwBiccode) {
		this.guestCcyAwBiccode = guestCcyAwBiccode;
	}
	public String getGuestCcyAwName() {
		return guestCcyAwName;
	}
	public void setGuestCcyAwName(String guestCcyAwName) {
		this.guestCcyAwName = guestCcyAwName;
	}
	public String getGuestCcyAwAcctno() {
		return guestCcyAwAcctno;
	}
	public void setGuestCcyAwAcctno(String guestCcyAwAcctno) {
		this.guestCcyAwAcctno = guestCcyAwAcctno;
	}
	public String getGuestCcyBeBiccode() {
		return guestCcyBeBiccode;
	}
	public void setGuestCcyBeBiccode(String guestCcyBeBiccode) {
		this.guestCcyBeBiccode = guestCcyBeBiccode;
	}
	public String getGuestCcyBeName() {
		return guestCcyBeName;
	}
	public void setGuestCcyBeName(String guestCcyBeName) {
		this.guestCcyBeName = guestCcyBeName;
	}
	public String getGuestCcyBeAcctno() {
		return guestCcyBeAcctno;
	}
	public void setGuestCcyBeAcctno(String guestCcyBeAcctno) {
		this.guestCcyBeAcctno = guestCcyBeAcctno;
	}
	public String getGuestCcyInBiccode() {
		return guestCcyInBiccode;
	}
	public void setGuestCcyInBiccode(String guestCcyInBiccode) {
		this.guestCcyInBiccode = guestCcyInBiccode;
	}
	public String getGuestCcyInName() {
		return guestCcyInName;
	}
	public void setGuestCcyInName(String guestCcyInName) {
		this.guestCcyInName = guestCcyInName;
	}
	public String getGuestCcyInAcctno() {
		return guestCcyInAcctno;
	}
	public void setGuestCcyInAcctno(String guestCcyInAcctno) {
		this.guestCcyInAcctno = guestCcyInAcctno;
	}
	public String getGuestCcyPost() {
		return guestCcyPost;
	}
	public void setGuestCcyPost(String guestCcyPost) {
		this.guestCcyPost = guestCcyPost;
	}
	public String getGuestCtrAwBiccode() {
		return guestCtrAwBiccode;
	}
	public void setGuestCtrAwBiccode(String guestCtrAwBiccode) {
		this.guestCtrAwBiccode = guestCtrAwBiccode;
	}
	public String getGuestCtrAwName() {
		return guestCtrAwName;
	}
	public void setGuestCtrAwName(String guestCtrAwName) {
		this.guestCtrAwName = guestCtrAwName;
	}
	public String getGuestCtrAwAcctno() {
		return guestCtrAwAcctno;
	}
	public void setGuestCtrAwAcctno(String guestCtrAwAcctno) {
		this.guestCtrAwAcctno = guestCtrAwAcctno;
	}
	public String getGuestCtrBeBiccode() {
		return guestCtrBeBiccode;
	}
	public void setGuestCtrBeBiccode(String guestCtrBeBiccode) {
		this.guestCtrBeBiccode = guestCtrBeBiccode;
	}
	public String getGuestCtrBeName() {
		return guestCtrBeName;
	}
	public void setGuestCtrBeName(String guestCtrBeName) {
		this.guestCtrBeName = guestCtrBeName;
	}
	public String getGuestCtrBeAcctno() {
		return guestCtrBeAcctno;
	}
	public void setGuestCtrBeAcctno(String guestCtrBeAcctno) {
		this.guestCtrBeAcctno = guestCtrBeAcctno;
	}
	public String getGuestCtrInBiccode() {
		return guestCtrInBiccode;
	}
	public void setGuestCtrInBiccode(String guestCtrInBiccode) {
		this.guestCtrInBiccode = guestCtrInBiccode;
	}
	public String getGuestCtrInName() {
		return guestCtrInName;
	}
	public void setGuestCtrInName(String guestCtrInName) {
		this.guestCtrInName = guestCtrInName;
	}
	public String getGuestCtrInAcctno() {
		return guestCtrInAcctno;
	}
	public void setGuestCtrInAcctno(String guestCtrInAcctno) {
		this.guestCtrInAcctno = guestCtrInAcctno;
	}
	public String getGuestCtrPost() {
		return guestCtrPost;
	}
	public void setGuestCtrPost(String guestCtrPost) {
		this.guestCtrPost = guestCtrPost;
	}
	
	
	
}
