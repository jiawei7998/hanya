package com.singlee.financial.wks.bean;

import java.math.BigDecimal;
import java.util.List;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.RpdtWksBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 银行间回购对象
 * 
 * @author xuqq
 *
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class RepoWksBean extends CommonBean {

	private static final long serialVersionUID = 7961684965834828497L;

	private String tradeNo;// 审批编号
//	private String instId;// 机构编号
	private String cno;// 交易对手
	/**
	 * 交易日
	 */
	private String dealDate;
	/**
	 * 起息日
	 */
	private String vdate;
	/**
	 * 到日期
	 */
	private String mdate;
	/**
	 * 计息基础
	 */
	private String basis;
	/**
	 * 交易货币
	 */
	private String ccy;// 目前所有币种用的是CNY,不传默认CNY,以后需要扩展
	/**
	 * 交易货币金额
	 */
	private BigDecimal ccyamt;// 名义金额/开始收入金额:
	/**
	 * 交易货币金额
	 */
	private BigDecimal mccyamt;// 到期收入金额:

	private String trdType;// 交易种类:REPO001/P（交易方向）
	private String settlementMethod;// 结算方式
	private String ccysacct;// 资金结算账号:清算账号
	private BigDecimal repoRate;// 回购利率
	private String secsacct;// 托管帐户:托管账户
	private String collateralcode;// 担保品编码:不传默认ALL
	private String collunit;// 担保品单位:
	private String delivtype;// 不传值：默认DVP
	private BigDecimal convintamt;// 兑换利息:
	private String invtype;// A:投资类型

	/**
	 * 清算信息
	 */
	private SettleInfoWksBean settleAcct;

	private List<RpdtWksBean> rpdtWksBeanList;

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getCcyamt() {
		return ccyamt;
	}

	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}

	public BigDecimal getMccyamt() {
		return mccyamt;
	}

	public void setMccyamt(BigDecimal mccyamt) {
		this.mccyamt = mccyamt;
	}

	public String getTrdType() {
		return trdType;
	}

	public void setTrdType(String trdType) {
		this.trdType = trdType;
	}

	public String getSettlementMethod() {
		return settlementMethod;
	}

	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod;
	}

	public String getCcysacct() {
		return ccysacct;
	}

	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}

	public BigDecimal getRepoRate() {
		return repoRate;
	}

	public void setRepoRate(BigDecimal repoRate) {
		this.repoRate = repoRate;
	}

	public String getSecsacct() {
		return secsacct;
	}

	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}

	public String getCollateralcode() {
		return collateralcode;
	}

	public void setCollateralcode(String collateralcode) {
		this.collateralcode = collateralcode;
	}

	public String getCollunit() {
		return collunit;
	}

	public void setCollunit(String collunit) {
		this.collunit = collunit;
	}

	public String getDelivtype() {
		return delivtype;
	}

	public void setDelivtype(String delivtype) {
		this.delivtype = delivtype;
	}

	public BigDecimal getConvintamt() {
		return convintamt;
	}

	public void setConvintamt(BigDecimal convintamt) {
		this.convintamt = convintamt;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}

	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}

	public List<RpdtWksBean> getRpdtWksBeanList() {
		return rpdtWksBeanList;
	}

	public void setRpdtWksBeanList(List<RpdtWksBean> rpdtWksBeanList) {
		this.rpdtWksBeanList = rpdtWksBeanList;
	}
	
	
}
