package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/securExporter")
public interface SecurService {

    /**
     * 现券买卖保存
     * 
     * @return
     */
    SlOutBean saveEntry();

    /**
     * 交易冲销
     * 
     * @return
     */
    SlOutBean reverseTrading();
}
