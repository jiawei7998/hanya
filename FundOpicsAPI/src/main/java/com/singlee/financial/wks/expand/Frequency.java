package com.singlee.financial.wks.expand;

/**
 * 付息频率、利率重置频率
 * @author chenxh
 *
 */
public enum Frequency {

	/**
	 * Annually
	 */
	A,
	
	/**
	 * Semi-annually
	 */
	S,
	
	/**
	 * Quarterly
	 */
	Q,
	
	/**
	 * Monthly
	 */
	M,
	
	/**
	 * Weekly
	 */
	W,
	
	/**
	 * Daily
	 */
	D,
	
	/**
	 * MaturityOnly
	 */
	B;

}
