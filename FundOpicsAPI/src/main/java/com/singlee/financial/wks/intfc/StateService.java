package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.StateBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/stateExporter")
public interface StateService {

	/**
	 * 交易录入
	 * 
	 * @param ibo
	 * @return
	 */
	SlOutBean queryEntry(StateBean sb);
}
