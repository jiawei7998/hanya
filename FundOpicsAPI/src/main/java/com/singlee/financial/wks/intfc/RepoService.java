package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.RepoWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/repoExporter")
public interface RepoService {

    /**
     * 质押式回购保存
     * 
     * @return
     */
    SlOutBean saveCrEntry(RepoWksBean repoWksBean);

    /**
     * 买断式回购交易保存
     * 
     * @return
     */
    SlOutBean saveOrEntry();

    /**
     * 交易冲销
     * 
     * @return
     */
    SlOutBean reverseTrading(RepoWksBean repoWksBean);
}
