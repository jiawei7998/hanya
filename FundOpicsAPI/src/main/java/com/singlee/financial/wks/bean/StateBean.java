package com.singlee.financial.wks.bean;

import java.io.Serializable;

import lombok.Data;

@Data
public class StateBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3639650168889918147L;
	/**
	 * 交易编号
	 */
	private String tradeNo;
	/**
	 * 交易类型
	 */
	private String tradeType;
	/**
	 * 交易事件
	 */
	private String tradeAction;
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public String getTradeAction() {
		return tradeAction;
	}
	public void setTradeAction(String tradeAction) {
		this.tradeAction = tradeAction;
	}
	
	
}
