package com.singlee.financial.wks.expand;

public enum WksErrorCode {
	
	PARAM_NULL("SL-000001","参数为空,不允许输入空指"),
    BR_NULL("SL-000001", "分支/部门,不允许为空值!"),
    BR_NOT_FOUND("SL-000002", "分支/部门,输入不正确!"),
    DESK_FORM_ERR("SL-000003", "产品/类型/投组,输入不正确!"),
    DESK_NOT_FOUND("SL-000004", "产品&类型,输入不正确!"),
    DESK_PORT_ERR("SL-000005", "投组组合,输入不正确!"),
    CCY_NOT_FOUND("SL-000006", "货币代码,输入不正确!"),
    CCYP_NOT_FOUND("SL-000007", "货币代码,输入不正确!"),
    RATE_CODE_NOT_FOUND("SL-000008", "根据货币代码查询利率代码,输入不正确!"),
    BOOK_NOT_FOUND("SL-000009", "成本中心,输入不正确!"),
    CUST_NOT_FOUND("SL-000010", "交易对手,输入不正确!"),
    TRAD_NOT_FOUND("SL-000011", "交易员,输入不正确!"),
    HOST_BIC_NOT_FOUND("SL-000012", "本方BIC,输入不正确!"),
    BOND_NULL("SL-000013", "债券基本信息对象,不允许为空值!"),
    FEES_NULL("SL-000013", "费用信息对象,不允许为空值!"),
    CUST_NULL("SL-00014", "交易对手对象,不允许为空值!"),
    CUST_CNO_NUMERIC("SL-00015", "交易对手对象,不允许编号为空或者非数字或长度大于10!"),
    CUST_CMNE_UPPER("SL-00016", "交易对手对象,不允许对手代码为空或者必须大写字母!"),
    CUST_BIC_NULL("SL-00017", "交易对手对象,不允许BIC为空!"),
    CUST_BIC_NOT_FOUND("SL-00018", "交易对手对象,BIC未在系统中查询到!"),
    CUST_CCODE_NULL("SL-00019", "交易对手对象,不允许CCODE为空!"),
    CUST_CCODE_NOT_FOUND("SL-00020", "交易对手对象,CCODE未在系统中查询到!"),
    CUST_SIC_NULL("SL-00021", "交易对手对象,不允许SIC为空!"),
    CUST_SIC_NOT_FOUND("SL-00022", "交易对手对象,SIC未在系统中查询到!"),
    CUST_SN_NULL("SL-00018", "交易对手对象,不允许SN为空!"),
    CUST_CFN_NULL("SL-00019", "交易对手对象,不允许CFN为空!"),
    CUST_CA4_NULL("SL-00020", "交易对手对象,不允许CA4为空或不存在!"),
    CUST_UCCODE_NULL("SL-00021", "交易对手对象,不允许UCCODE为空!"),
    CUST_ACTNGTYPE_NULL("SL-00022", "交易对手对象,不允许ACTNGTYPE为空!"),
    CUST_ACTNGTYPE_NOT_FOUND("SL-00022", "交易对手对象,ACTNGTYPE未在系统中查询到!"),
    CUST_TYPE_NOT_FOUND("SL-00023", "交易对手对象,CUSTTYPE枚举值未在系统中查询到!"),
    SETA_SMEANS_NOT_FOUND("SL-00050", "清算路径,SMEANS未在系统中查询到!"),
    SETA_SACCT_NOT_FOUND("SL-00051", "清算路径,SACCT未在系统中查询到!"),
    IBO_NULL("SL-000100", "拆借交易对象,不允许为空值!"),
    IBO_NOT_FOUND("SL-000101", "拆借交易交易,未在系统中查询到!"),
    CBT_NULL("SL-000200", "债券交易对象,不允许为空值!"),
    CBT_NOT_FOUND("SL-000201", "债券交易对象,未在系统中查询到!"),
    CBT_BOND_NULL("SL-000202", "债券交易债券代码,未在系统中查询到!"),
    FXD_SPTFWD_NULL("SL-000310", "外汇即/远期交易对象,不允许为空值或循环组错误!"),
    FXD_SWAP_NULL("SL-000320", "外汇掉期交易对象,不允许为空值或循环组错误!"),
    IRS_NULL("SL-000400", "利率互换交易对象,不允许为空值!"),
    REVERSE_NULL("SL-000900", "冲销对象,不允许为空值!"),
    FEES_NOT_FOUND("SL-000310", "费用交易对象,不允许为空值或循环组错误!"),
    
    
    SUCCESS("SL-000000", "交易成功"),
    FAILED("SL-999999", "处理失败");
    private String errCode;
    private String errMsg;

    private WksErrorCode(String errCode, String errMsg) {
        this.setErrCode(errCode);
        this.setErrMsg(errMsg);
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
    @Override
    public String toString() {
        return "[" + this.errCode + "]" + this.errMsg;
    }
}
