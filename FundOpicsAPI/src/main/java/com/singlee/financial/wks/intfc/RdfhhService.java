package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.wks.bean.RdfhWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.Map;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/rdfhExporter")
public interface RdfhhService {


    PageInfo<RdfhWksBean> searchDlCount(Map<String,Object> map);

	/**
     * 收息
     * @param cbt
     * @return
     */
    SlOutBean receiveInterestTrading(RdfhWksBean rdfhWksBean);
    /**
     * 收息冲销
     * @param cbt
     * @return
     */
    SlOutBean reverseInterestTrading(RdfhWksBean rdfhWksBean);




}
