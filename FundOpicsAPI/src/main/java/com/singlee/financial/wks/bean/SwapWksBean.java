package com.singlee.financial.wks.bean;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;
import com.singlee.financial.wks.expand.SwapCashFlowMethod;
import com.singlee.financial.wks.expand.SwapItemSchdWksBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 互换交易数据
 * 
 * @author xuqq 20200204
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SwapWksBean extends CommonBean {
	private static final long serialVersionUID = 1L;
	private String tradeNo;// 交易编号
	// 交易信息
	private String partyId;// 客户号
	private String dealDate;// 交易日
	private String vDate;// 开始日期
	private String mDate;// 到期日|提前终止日
	private String cno;// 交易对手
	// 互换的腿信息
	private String ccy;// 资金币种
	private BigDecimal amt;// 名义货币金额
	private String intPayAdjMod;// 付息日确定规则
	private String firstIntPayDate;// 首次付息日期
	private String lastIntPayDate;// 最后一个付息日期
	private String pType;// IRS_P1R2-付固定收浮动 IRS_P2R1-付浮动收固定
	// 付
	private String payRateType;// 固定利率浮动利率标志|提前终止
	private BigDecimal payFixedRate;// 固定利率
	private String payFloatIndicator;// 浮动指标 浮动方基准利率
	private String payFloatType;// 浮动方式 点数/百分比 BasisPointOrPercentage
	private BigDecimal payFloatBps;// BP点差
	private BigDecimal payFloatPerc; // 总分浮动百分比
	private String payCalIntBasic; // 计息基数代码
	private String payCalIntType; // 复利标志:M=复利/N=单利
	private String payPaymentFrequency;// 付息周期代码
	private String payRefixRule; // 浮动方日期调整规则 DateAdjustment
	private String payInterestResetFrequency;// 利率重置周期
	private BigDecimal payFxrate;// 折人民币汇率
	private String payAcctType;// 利息结算方式
	private String payAcctNo;// 利息结算帐户
	private String payYieldcurve;// 收益率曲线：为空默认swap TODO 未确认
	private String payRateCode;// 利率代码 TODO 未确认
	private String payRaterevdte;// 利率重置日期 TODO 未确认
	private String payRaterevpayrule;// 利率重置规则 TODO 未确认
	private BigDecimal payNominalAmount;// 支付端本金
	private String payCashFlowMethod;// 现金流调整规则 CHINA -- ISDA
	private String payCcy;// 付端币种
	private String paySeq;// 付端序号
	private String payDiscountYieldcurve;//付端贴线曲线
	// 收
	private String recRateType; // 利率类型 固定/浮动 FixedOrFloat
	private BigDecimal recFixedRate; // 固定利率
	private String recFloatIndicator; // 浮动指标 浮动方基准利率
	private String recFloatType; // 浮动方式 点数/百分比 BasisPointOrPercentage
	private BigDecimal recFloatBps; // 总分浮动点数
	private BigDecimal recFloatPerc; // 总分浮动百分比
	private String recCalIntBasic; // 计息基准 DayCounter
	private String recCalIntType; // 计息方式 单利/复利 Compounding
	private String recPaymentFrequency; // 付息频率 CouponIntFrequently
	private String recRefixRule; // 浮动方日期调整规则 DateAdjustment
	private String recInterestResetFrequency; // 浮动方重置频率 frequency
	private BigDecimal recFxrate;// 折人民币汇率
	private String recAcctType;// 利息结算方式
	private String recAcctNo;// 利息结算帐户
	private String recYieldcurve;// 收益率曲线：为空默认swap TODO 未确认
	private String recRateCode;// 利率代码 TODO 未确认
	private String recRaterevdte;// 利率重置日期 TODO 未确认
	private String recRaterevpayrule;// 利率重置规则 TODO 未确认
	private BigDecimal recNominalAmount;// 支付端本金
	private String recCashFlowMethod;// 现金流调整规则 CHINA -- ISDA
	private String recCcy;// 收端币种
	private String recSeq;// 收端序号
	private String recDiscountYieldcurve;//收端贴线曲线
	private SettleInfoWksBean settleAcct;// 清算信息
	private List<SwapItemSchdWksBean> itemBeanSwds;// 收付息计划
	
	private SwapCashFlowMethod cashFlowMethod;//计算现金流方法
	private String orgTradeNo;//原交易流水号
	
	private String earlyDate;//提前终止日期
	private BigDecimal earlyAmount;//罚金(CFETS估值,系统自动扣减已有利息金额)
	private String earlyCcy;//罚金货币
	private String earlyDirection;//罚金收付方向
	
	private String swapType = "I";//I-利率互换;C-货币互换
	private String exchPrin;//本金交换方式
	private BigDecimal payFinExchAmt;
	private String payFinExchCcy;
	private String payFinExchDate;
	private BigDecimal payInitExchAmt;
	private String payInitExchCcy;
	private String payInitExchDate;
	private BigDecimal recFinExchAmt;
	private String recFinExchCcy;
	private String recFinExchDate;
	private BigDecimal recInitExchAmt;
	private String recInitExchCcy;
	private String recInitExchDate;

	/**
	 * 当前系统批量日期
	 */
	private Date batchDate;

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getVDate() {
		return vDate;
	}

	public void setVDate(String vDate) {
		this.vDate = vDate;
	}

	public String getMDate() {
		return mDate;
	}

	public void setMDate(String mDate) {
		this.mDate = mDate;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public String getIntPayAdjMod() {
		return intPayAdjMod;
	}

	public void setIntPayAdjMod(String intPayAdjMod) {
		this.intPayAdjMod = intPayAdjMod;
	}

	public String getFirstIntPayDate() {
		return firstIntPayDate;
	}

	public void setFirstIntPayDate(String firstIntPayDate) {
		this.firstIntPayDate = firstIntPayDate;
	}

	public String getLastIntPayDate() {
		return lastIntPayDate;
	}

	public void setLastIntPayDate(String lastIntPayDate) {
		this.lastIntPayDate = lastIntPayDate;
	}

	public String getpType() {
		return pType;
	}

	public void setpType(String pType) {
		this.pType = pType;
	}

	public String getPayRateType() {
		return payRateType;
	}

	public void setPayRateType(String payRateType) {
		this.payRateType = payRateType;
	}

	public BigDecimal getPayFixedRate() {
		return payFixedRate;
	}

	public void setPayFixedRate(BigDecimal payFixedRate) {
		this.payFixedRate = payFixedRate;
	}

	public String getPayFloatIndicator() {
		return payFloatIndicator;
	}

	public void setPayFloatIndicator(String payFloatIndicator) {
		this.payFloatIndicator = payFloatIndicator;
	}

	public String getPayFloatType() {
		return payFloatType;
	}

	public void setPayFloatType(String payFloatType) {
		this.payFloatType = payFloatType;
	}

	public BigDecimal getPayFloatBps() {
		return payFloatBps;
	}

	public void setPayFloatBps(BigDecimal payFloatBps) {
		this.payFloatBps = payFloatBps;
	}

	public BigDecimal getPayFloatPerc() {
		return payFloatPerc;
	}

	public void setPayFloatPerc(BigDecimal payFloatPerc) {
		this.payFloatPerc = payFloatPerc;
	}

	public String getPayCalIntBasic() {
		return payCalIntBasic;
	}

	public void setPayCalIntBasic(String payCalIntBasic) {
		this.payCalIntBasic = payCalIntBasic;
	}

	public String getPayCalIntType() {
		return payCalIntType;
	}

	public void setPayCalIntType(String payCalIntType) {
		this.payCalIntType = payCalIntType;
	}

	public String getPayPaymentFrequency() {
		return payPaymentFrequency;
	}

	public void setPayPaymentFrequency(String payPaymentFrequency) {
		this.payPaymentFrequency = payPaymentFrequency;
	}

	public String getPayRefixRule() {
		return payRefixRule;
	}

	public void setPayRefixRule(String payRefixRule) {
		this.payRefixRule = payRefixRule;
	}

	public String getPayInterestResetFrequency() {
		return payInterestResetFrequency;
	}

	public void setPayInterestResetFrequency(String payInterestResetFrequency) {
		this.payInterestResetFrequency = payInterestResetFrequency;
	}

	public BigDecimal getPayFxrate() {
		return payFxrate;
	}

	public void setPayFxrate(BigDecimal payFxrate) {
		this.payFxrate = payFxrate;
	}

	public String getPayAcctType() {
		return payAcctType;
	}

	public void setPayAcctType(String payAcctType) {
		this.payAcctType = payAcctType;
	}

	public String getPayAcctNo() {
		return payAcctNo;
	}

	public void setPayAcctNo(String payAcctNo) {
		this.payAcctNo = payAcctNo;
	}

	public String getPayYieldcurve() {
		return payYieldcurve;
	}

	public void setPayYieldcurve(String payYieldcurve) {
		this.payYieldcurve = payYieldcurve;
	}

	public String getPayRateCode() {
		return payRateCode;
	}

	public void setPayRateCode(String payRateCode) {
		this.payRateCode = payRateCode;
	}

	public String getPayRaterevdte() {
		return payRaterevdte;
	}

	public void setPayRaterevdte(String payRaterevdte) {
		this.payRaterevdte = payRaterevdte;
	}

	public String getPayRaterevpayrule() {
		return payRaterevpayrule;
	}

	public void setPayRaterevpayrule(String payRaterevpayrule) {
		this.payRaterevpayrule = payRaterevpayrule;
	}

	public BigDecimal getPayNominalAmount() {
		return payNominalAmount;
	}

	public void setPayNominalAmount(BigDecimal payNominalAmount) {
		this.payNominalAmount = payNominalAmount;
	}

	public String getPayCashFlowMethod() {
		return payCashFlowMethod;
	}

	public void setPayCashFlowMethod(String payCashFlowMethod) {
		this.payCashFlowMethod = payCashFlowMethod;
	}

	public String getPayCcy() {
		return payCcy;
	}

	public void setPayCcy(String payCcy) {
		this.payCcy = payCcy;
	}

	public String getPaySeq() {
		return paySeq;
	}

	public void setPaySeq(String paySeq) {
		this.paySeq = paySeq;
	}

	public String getPayDiscountYieldcurve() {
		return payDiscountYieldcurve;
	}

	public void setPayDiscountYieldcurve(String payDiscountYieldcurve) {
		this.payDiscountYieldcurve = payDiscountYieldcurve;
	}

	public String getRecRateType() {
		return recRateType;
	}

	public void setRecRateType(String recRateType) {
		this.recRateType = recRateType;
	}

	public BigDecimal getRecFixedRate() {
		return recFixedRate;
	}

	public void setRecFixedRate(BigDecimal recFixedRate) {
		this.recFixedRate = recFixedRate;
	}

	public String getRecFloatIndicator() {
		return recFloatIndicator;
	}

	public void setRecFloatIndicator(String recFloatIndicator) {
		this.recFloatIndicator = recFloatIndicator;
	}

	public String getRecFloatType() {
		return recFloatType;
	}

	public void setRecFloatType(String recFloatType) {
		this.recFloatType = recFloatType;
	}

	public BigDecimal getRecFloatBps() {
		return recFloatBps;
	}

	public void setRecFloatBps(BigDecimal recFloatBps) {
		this.recFloatBps = recFloatBps;
	}

	public BigDecimal getRecFloatPerc() {
		return recFloatPerc;
	}

	public void setRecFloatPerc(BigDecimal recFloatPerc) {
		this.recFloatPerc = recFloatPerc;
	}

	public String getRecCalIntBasic() {
		return recCalIntBasic;
	}

	public void setRecCalIntBasic(String recCalIntBasic) {
		this.recCalIntBasic = recCalIntBasic;
	}

	public String getRecCalIntType() {
		return recCalIntType;
	}

	public void setRecCalIntType(String recCalIntType) {
		this.recCalIntType = recCalIntType;
	}

	public String getRecPaymentFrequency() {
		return recPaymentFrequency;
	}

	public void setRecPaymentFrequency(String recPaymentFrequency) {
		this.recPaymentFrequency = recPaymentFrequency;
	}

	public String getRecRefixRule() {
		return recRefixRule;
	}

	public void setRecRefixRule(String recRefixRule) {
		this.recRefixRule = recRefixRule;
	}

	public String getRecInterestResetFrequency() {
		return recInterestResetFrequency;
	}

	public void setRecInterestResetFrequency(String recInterestResetFrequency) {
		this.recInterestResetFrequency = recInterestResetFrequency;
	}

	public BigDecimal getRecFxrate() {
		return recFxrate;
	}

	public void setRecFxrate(BigDecimal recFxrate) {
		this.recFxrate = recFxrate;
	}

	public String getRecAcctType() {
		return recAcctType;
	}

	public void setRecAcctType(String recAcctType) {
		this.recAcctType = recAcctType;
	}

	public String getRecAcctNo() {
		return recAcctNo;
	}

	public void setRecAcctNo(String recAcctNo) {
		this.recAcctNo = recAcctNo;
	}

	public String getRecYieldcurve() {
		return recYieldcurve;
	}

	public void setRecYieldcurve(String recYieldcurve) {
		this.recYieldcurve = recYieldcurve;
	}

	public String getRecRateCode() {
		return recRateCode;
	}

	public void setRecRateCode(String recRateCode) {
		this.recRateCode = recRateCode;
	}

	public String getRecRaterevdte() {
		return recRaterevdte;
	}

	public void setRecRaterevdte(String recRaterevdte) {
		this.recRaterevdte = recRaterevdte;
	}

	public String getRecRaterevpayrule() {
		return recRaterevpayrule;
	}

	public void setRecRaterevpayrule(String recRaterevpayrule) {
		this.recRaterevpayrule = recRaterevpayrule;
	}

	public BigDecimal getRecNominalAmount() {
		return recNominalAmount;
	}

	public void setRecNominalAmount(BigDecimal recNominalAmount) {
		this.recNominalAmount = recNominalAmount;
	}

	public String getRecCashFlowMethod() {
		return recCashFlowMethod;
	}

	public void setRecCashFlowMethod(String recCashFlowMethod) {
		this.recCashFlowMethod = recCashFlowMethod;
	}

	public String getRecCcy() {
		return recCcy;
	}

	public void setRecCcy(String recCcy) {
		this.recCcy = recCcy;
	}

	public String getRecSeq() {
		return recSeq;
	}

	public void setRecSeq(String recSeq) {
		this.recSeq = recSeq;
	}

	public String getRecDiscountYieldcurve() {
		return recDiscountYieldcurve;
	}

	public void setRecDiscountYieldcurve(String recDiscountYieldcurve) {
		this.recDiscountYieldcurve = recDiscountYieldcurve;
	}

	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}

	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}

	public List<SwapItemSchdWksBean> getItemBeanSwds() {
		return itemBeanSwds;
	}

	public void setItemBeanSwds(List<SwapItemSchdWksBean> itemBeanSwds) {
		this.itemBeanSwds = itemBeanSwds;
	}

	public SwapCashFlowMethod getCashFlowMethod() {
		return cashFlowMethod;
	}

	public void setCashFlowMethod(SwapCashFlowMethod cashFlowMethod) {
		this.cashFlowMethod = cashFlowMethod;
	}

	public String getOrgTradeNo() {
		return orgTradeNo;
	}

	public void setOrgTradeNo(String orgTradeNo) {
		this.orgTradeNo = orgTradeNo;
	}

	public String getEarlyDate() {
		return earlyDate;
	}

	public void setEarlyDate(String earlyDate) {
		this.earlyDate = earlyDate;
	}

	public BigDecimal getEarlyAmount() {
		return earlyAmount;
	}

	public void setEarlyAmount(BigDecimal earlyAmount) {
		this.earlyAmount = earlyAmount;
	}

	public String getEarlyCcy() {
		return earlyCcy;
	}

	public void setEarlyCcy(String earlyCcy) {
		this.earlyCcy = earlyCcy;
	}

	public String getEarlyDirection() {
		return earlyDirection;
	}

	public void setEarlyDirection(String earlyDirection) {
		this.earlyDirection = earlyDirection;
	}

	public String getSwapType() {
		return swapType;
	}

	public void setSwapType(String swapType) {
		this.swapType = swapType;
	}

	public String getExchPrin() {
		return exchPrin;
	}

	public void setExchPrin(String exchPrin) {
		this.exchPrin = exchPrin;
	}

	public BigDecimal getPayFinExchAmt() {
		return payFinExchAmt;
	}

	public void setPayFinExchAmt(BigDecimal payFinExchAmt) {
		this.payFinExchAmt = payFinExchAmt;
	}

	public String getPayFinExchCcy() {
		return payFinExchCcy;
	}

	public void setPayFinExchCcy(String payFinExchCcy) {
		this.payFinExchCcy = payFinExchCcy;
	}

	public String getPayFinExchDate() {
		return payFinExchDate;
	}

	public void setPayFinExchDate(String payFinExchDate) {
		this.payFinExchDate = payFinExchDate;
	}

	public BigDecimal getPayInitExchAmt() {
		return payInitExchAmt;
	}

	public void setPayInitExchAmt(BigDecimal payInitExchAmt) {
		this.payInitExchAmt = payInitExchAmt;
	}

	public String getPayInitExchCcy() {
		return payInitExchCcy;
	}

	public void setPayInitExchCcy(String payInitExchCcy) {
		this.payInitExchCcy = payInitExchCcy;
	}

	public String getPayInitExchDate() {
		return payInitExchDate;
	}

	public void setPayInitExchDate(String payInitExchDate) {
		this.payInitExchDate = payInitExchDate;
	}

	public BigDecimal getRecFinExchAmt() {
		return recFinExchAmt;
	}

	public void setRecFinExchAmt(BigDecimal recFinExchAmt) {
		this.recFinExchAmt = recFinExchAmt;
	}

	public String getRecFinExchCcy() {
		return recFinExchCcy;
	}

	public void setRecFinExchCcy(String recFinExchCcy) {
		this.recFinExchCcy = recFinExchCcy;
	}

	public String getRecFinExchDate() {
		return recFinExchDate;
	}

	public void setRecFinExchDate(String recFinExchDate) {
		this.recFinExchDate = recFinExchDate;
	}

	public BigDecimal getRecInitExchAmt() {
		return recInitExchAmt;
	}

	public void setRecInitExchAmt(BigDecimal recInitExchAmt) {
		this.recInitExchAmt = recInitExchAmt;
	}

	public String getRecInitExchCcy() {
		return recInitExchCcy;
	}

	public void setRecInitExchCcy(String recInitExchCcy) {
		this.recInitExchCcy = recInitExchCcy;
	}

	public String getRecInitExchDate() {
		return recInitExchDate;
	}

	public void setRecInitExchDate(String recInitExchDate) {
		this.recInitExchDate = recInitExchDate;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}
	
	
}
