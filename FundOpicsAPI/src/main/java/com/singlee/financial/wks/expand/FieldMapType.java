package com.singlee.financial.wks.expand;

public enum FieldMapType {

	/**
	 * CFETS系统枚举值字段
	 */
	CFETS,
	/**
	 * 资金工作站
	 */
	WKS,
	/**
	 * OPICS系统值,系统不做转换
	 */
	OPICS
}
