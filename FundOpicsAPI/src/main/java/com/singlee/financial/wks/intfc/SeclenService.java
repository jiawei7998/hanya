package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.SldhWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/seclenExporter")
public interface SeclenService {

    /**
     * 债券借贷
     * 
     * @return
     */
    SlOutBean saveEntry(SldhWksBean sldh);

    /**
     * 交易冲销
     * 
     * @return
     */
    SlOutBean reverseTrading(SldhWksBean sldh);
}
