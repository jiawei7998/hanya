package com.singlee.financial.wks.bean;

import java.math.BigDecimal;
import java.util.List;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.SecsWksBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 债券信息
 * 
 * @author xuqq
 *
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SecmWksBean extends CommonBean {

	private static final long serialVersionUID = 1L;

	private String iCode;// 债券代码
	private String bName;// 债券名称
	private String bFullName;// 债券全称
	private String bIssueDate;// 发行日期
	private String bIssuer;// 发行人:CUST表里面取
	private BigDecimal bCouponRate;// 票面利率
	private String cny;// 币种
	private String bDaycount;// 计息基准
	private String bValueDate;// 起息日期
	private String bMaturityDate;// 到期日
	private String firstipaydate;// 首次付息日
	private String lastipaydate;// 最后一次付息日期
	private String intpaycycle;// 付息周期代码:A按年/S半年/Q季度/M月
	private String intenddaterule;// 利息结束日期规则:D默认/S向前/M向后
	private String intpayrule;// 付息日期规则:D默认/S向前/M向后
	private String denom;// 计量单位:一手的量DENOM:通常传入10000

	private String intcalcrule;// 利息计算规则:NPV按期付息/IAM到期换本/DIS贴现
	private String product;// 产品代码:
	private String prodtype;// 产品类型代码:根据实际业务模式选择SD/FI/CD/MBS
	private String secunit;// 单位:FMT/BON
	private String acctngtype;// 资产类型 JRZ/QYZ/GZ...

	private String settdays;// 债券交易日与起息日之间标准天数:

	private List<SecsWksBean> secsSchdList;

	public String getICode() {
		return iCode;
	}

	public void setICode(String iCode) {
		this.iCode = iCode;
	}

	public String getBName() {
		return bName;
	}

	public void setBName(String bName) {
		this.bName = bName;
	}

	public String getbFullName() {
		return bFullName;
	}

	public void setbFullName(String bFullName) {
		this.bFullName = bFullName;
	}

	public String getBIssueDate() {
		return bIssueDate;
	}

	public void setbIssueDate(String bIssueDate) {
		this.bIssueDate = bIssueDate;
	}

	public String getBIssuer() {
		return bIssuer;
	}

	public void setBIssuer(String bIssuer) {
		this.bIssuer = bIssuer;
	}

	public BigDecimal getBCouponRate() {
		return bCouponRate;
	}

	public void setBCouponRate(BigDecimal bCouponRate) {
		this.bCouponRate = bCouponRate;
	}

	public String getCny() {
		return cny;
	}

	public void setCny(String cny) {
		this.cny = cny;
	}

	public String getBDaycount() {
		return bDaycount;
	}

	public void setBDaycount(String bDaycount) {
		this.bDaycount = bDaycount;
	}

	public String getBValueDate() {
		return bValueDate;
	}

	public void setBValueDate(String bValueDate) {
		this.bValueDate = bValueDate;
	}

	public String getBMaturityDate() {
		return bMaturityDate;
	}

	public void setBMaturityDate(String bMaturityDate) {
		this.bMaturityDate = bMaturityDate;
	}

	public String getFirstipaydate() {
		return firstipaydate;
	}

	public void setFirstipaydate(String firstipaydate) {
		this.firstipaydate = firstipaydate;
	}

	public String getLastipaydate() {
		return lastipaydate;
	}

	public void setLastipaydate(String lastipaydate) {
		this.lastipaydate = lastipaydate;
	}

	public String getIntpaycycle() {
		return intpaycycle;
	}

	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}

	public String getIntenddaterule() {
		return intenddaterule;
	}

	public void setIntenddaterule(String intenddaterule) {
		this.intenddaterule = intenddaterule;
	}

	public String getIntpayrule() {
		return intpayrule;
	}

	public void setIntpayrule(String intpayrule) {
		this.intpayrule = intpayrule;
	}

	public String getDenom() {
		return denom;
	}

	public void setDenom(String denom) {
		this.denom = denom;
	}

	public String getIntcalcrule() {
		return intcalcrule;
	}

	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getSecunit() {
		return secunit;
	}

	public void setSecunit(String secunit) {
		this.secunit = secunit;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public String getSettdays() {
		return settdays;
	}

	public void setSettdays(String settdays) {
		this.settdays = settdays;
	}

	public List<SecsWksBean> getSecsSchdList() {
		return secsSchdList;
	}

	public void setSecsSchdList(List<SecsWksBean> secsSchdList) {
		this.secsSchdList = secsSchdList;
	}
	
	
}
