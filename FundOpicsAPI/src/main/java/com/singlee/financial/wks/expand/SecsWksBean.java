package com.singlee.financial.wks.expand;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

@Data
public class SecsWksBean implements Serializable {
	
	private static final long serialVersionUID = 8656426990949300911L;

	private String iCode;//债券代码
	private String paymentdate;//理论支付日
	private BigDecimal paymentsum;//应付金额
	private BigDecimal couponrate;//当期执行利率
	private BigDecimal paymentinterest;//应付利息
	private String carrydate;//参考开始日期
	private String enddate;//参考结束日期
	private BigDecimal paymentparvalue;//应付本金
	public String getICode() {
		return iCode;
	}
	public void setICode(String iCode) {
		this.iCode = iCode;
	}
	public String getPaymentdate() {
		return paymentdate;
	}
	public void setPaymentdate(String paymentdate) {
		this.paymentdate = paymentdate;
	}
	public BigDecimal getPaymentsum() {
		return paymentsum;
	}
	public void setPaymentsum(BigDecimal paymentsum) {
		this.paymentsum = paymentsum;
	}
	public BigDecimal getCouponrate() {
		return couponrate;
	}
	public void setCouponrate(BigDecimal couponrate) {
		this.couponrate = couponrate;
	}
	public BigDecimal getPaymentinterest() {
		return paymentinterest;
	}
	public void setPaymentinterest(BigDecimal paymentinterest) {
		this.paymentinterest = paymentinterest;
	}
	public String getCarrydate() {
		return carrydate;
	}
	public void setCarrydate(String carrydate) {
		this.carrydate = carrydate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public BigDecimal getPaymentparvalue() {
		return paymentparvalue;
	}
	public void setPaymentparvalue(BigDecimal paymentparvalue) {
		this.paymentparvalue = paymentparvalue;
	}
	
	

}
