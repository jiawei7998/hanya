package com.singlee.financial.wks.expand;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FxdItemWksBean extends CommonBean {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;

	/**
	 * 交易号
	 */
	private String dealNo;
	/**
	 * 序号，默认0
	 */
	private String seq;
	/**
	 * 归属机构编号
	 */
	private String bInstId;
	/**
	 * 归属机构交易员
	 */
	private String bUserId;
	/**
	 * 平盘类型0：代客 1：自营
	 */
	private String tradeType;
	/**
	 * 结售汇类型0：外汇1：结售汇
	 */
	private String sasType;
	/**
	 * 客户号
	 */
	private String partyId;
	/**
	 * 客户名称
	 */
	private String partyName;
	/**
	 * 期权开始日期
	 */
	private String oDate;
	/**
	 * 交易日期
	 */
	private String dealDate;
	/**
	 * 交易时间
	 */
	private String dealTime;
	/**
	 * 交易来源
	 */
	private String dealSource;
	/**
	 * 买卖标志
	 */
	private String ps;
	/**
	 * 交割日期
	 */
	private String vDate;
	/**
	 * 择期交割日期
	 */
	private String dDate;
	/**
	 * 货币１
	 */
	private String ccy;
	/**
	 * 货币１客户金额
	 */
	private BigDecimal ccyAmt;
	/**
	 * 货币１汇率乘除形式
	 */
	private String ccyTerms;
	/**
	 * 货币１客户汇率
	 */
	private BigDecimal ccyRate;
	/**
	 * 货币１客户升贴水
	 */
	private BigDecimal ccyPd;
	/**
	 * 货币１相应的基础货币金额
	 */
	private BigDecimal ccyBAmt;
	/**
	 * 货币１对基础货币汇率
	 */
	private BigDecimal ccyBRate;
	/**
	 * 货币１对基础货币升水/贴水
	 */
	private BigDecimal ccyBPd;
	/**
	 * 货币1基础货币汇率乘除形式
	 */
	private String ccyBTerms;
	/**
	 * 货币2
	 */
	private String ctrCcy;
	/**
	 * 货币2客户金额
	 */
	private BigDecimal ctrAmt;
	/**
	 * 货币2客户汇率
	 */
	private BigDecimal ctrRate;
	/**
	 * 货币2客户升贴水
	 */
	private BigDecimal ctrPd;
	/**
	 * 货币2相应的基础货币金额
	 */
	private BigDecimal ctrBAmt;
	/**
	 * 货币2对基础货币汇率
	 */
	private BigDecimal ctrBRate;
	/**
	 * 货币2对应基础货币乘除形式
	 */
	private String ctrBTerms;
	/**
	 * 货币1重估
	 */
	private BigDecimal ccyRevalAmt;
	/**
	 * 货币2重估
	 */
	private BigDecimal ctrRevalAmt;
	/**
	 * 货币1净现值
	 */
	private BigDecimal ccyNpvAmt;
	/**
	 * 货币2净现值
	 */
	private BigDecimal ctrNpvAmt;
	/**
	 * 期限
	 */
	private String tenor;
	/**
	 * 货币1结算日期
	 */
	private String ccySettDate;
	/**
	 * 货币2结算日期
	 */
	private String ctrSettDate;
	/**
	 * 近端/远端交易标志
	 */
	private String farNearind;
	/**
	 * 掉期交易生效日期
	 */
	private String swapVdate;
	/**
	 * 即期/远期标志
	 */
	private String spotFwdind;
	/**
	 * 分解币种
	 */
	private String disaggCcy;
	/**
	 * 输入系统日期
	 */
	private String inputDate;
	/**
	 * 输入系统时间
	 */
	private String inputTime;
	/**
	 * 是否有效
	 */
	private String isActive;
	/**
	 * 清算状态
	 */
	private String settlStatus;
	/**
	 * 即期汇率
	 */
	private BigDecimal spotRate;
	/**
	 * 是否冲销
	 */
	private String writeOff;
	/**
	 * 冲销备注
	 */
	private String writeOffRemark;
	/**
	 * 外汇交易标识
	 */
	private String fxSign;
	/**
	 * 货币对
	 */
	private String ccyPair;
	/**
	 * 外部系统编号
	 */
	private String outSystemId;
	/**
	 * 关联交易编号
	 */
	private String relationDealId;
	/**
	 * 是否拆分远期交易
	 */
	private String isSplitFwd;
	/**
	 * 即期日期
	 */
	private String spotDate;
	/**
	 * 清算信息
	 */
	private SettleInfoWksBean settleAcct;
	
	/**
	 * 清算方式.1-双边净额清算、2-双边全额清算、3-集中净额清算、4-SGE
	 */
	private String settlementMeans;
	
	/**
	 * CCY货币清算路径
	 */
	private String currencySettlementAccount;
	
	/**
	 * CTRCCY货币清算路径
	 */
	private String counterCurrencySettlementAccount;
	
	/**
	 * 交易到期损益
	 */
	private BigDecimal fwdRealPlAmt;

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getbInstId() {
		return bInstId;
	}

	public void setbInstId(String bInstId) {
		this.bInstId = bInstId;
	}

	public String getbUserId() {
		return bUserId;
	}

	public void setbUserId(String bUserId) {
		this.bUserId = bUserId;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getSasType() {
		return sasType;
	}

	public void setSasType(String sasType) {
		this.sasType = sasType;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getoDate() {
		return oDate;
	}

	public void setoDate(String oDate) {
		this.oDate = oDate;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getDealTime() {
		return dealTime;
	}

	public void setDealTime(String dealTime) {
		this.dealTime = dealTime;
	}

	public String getDealSource() {
		return dealSource;
	}

	public void setDealSource(String dealSource) {
		this.dealSource = dealSource;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getVDate() {
		return vDate;
	}

	public void setVDate(String vDate) {
		this.vDate = vDate;
	}

	public String getdDate() {
		return dDate;
	}

	public void setdDate(String dDate) {
		this.dDate = dDate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getCcyAmt() {
		return ccyAmt;
	}

	public void setCcyAmt(BigDecimal ccyAmt) {
		this.ccyAmt = ccyAmt;
	}

	public String getCcyTerms() {
		return ccyTerms;
	}

	public void setCcyTerms(String ccyTerms) {
		this.ccyTerms = ccyTerms;
	}

	public BigDecimal getCcyRate() {
		return ccyRate;
	}

	public void setCcyRate(BigDecimal ccyRate) {
		this.ccyRate = ccyRate;
	}

	public BigDecimal getCcyPd() {
		return ccyPd;
	}

	public void setCcyPd(BigDecimal ccyPd) {
		this.ccyPd = ccyPd;
	}

	public BigDecimal getCcyBAmt() {
		return ccyBAmt;
	}

	public void setCcyBAmt(BigDecimal ccyBAmt) {
		this.ccyBAmt = ccyBAmt;
	}

	public BigDecimal getCcyBRate() {
		return ccyBRate;
	}

	public void setCcyBRate(BigDecimal ccyBRate) {
		this.ccyBRate = ccyBRate;
	}

	public BigDecimal getCcyBPd() {
		return ccyBPd;
	}

	public void setCcyBPd(BigDecimal ccyBPd) {
		this.ccyBPd = ccyBPd;
	}

	public String getCcyBTerms() {
		return ccyBTerms;
	}

	public void setCcyBTerms(String ccyBTerms) {
		this.ccyBTerms = ccyBTerms;
	}

	public String getCtrCcy() {
		return ctrCcy;
	}

	public void setCtrCcy(String ctrCcy) {
		this.ctrCcy = ctrCcy;
	}

	public BigDecimal getCtrAmt() {
		return ctrAmt;
	}

	public void setCtrAmt(BigDecimal ctrAmt) {
		this.ctrAmt = ctrAmt;
	}

	public BigDecimal getCtrRate() {
		return ctrRate;
	}

	public void setCtrRate(BigDecimal ctrRate) {
		this.ctrRate = ctrRate;
	}

	public BigDecimal getCtrPd() {
		return ctrPd;
	}

	public void setCtrPd(BigDecimal ctrPd) {
		this.ctrPd = ctrPd;
	}

	public BigDecimal getCtrBAmt() {
		return ctrBAmt;
	}

	public void setCtrBAmt(BigDecimal ctrBAmt) {
		this.ctrBAmt = ctrBAmt;
	}

	public BigDecimal getCtrBRate() {
		return ctrBRate;
	}

	public void setCtrBRate(BigDecimal ctrBRate) {
		this.ctrBRate = ctrBRate;
	}

	public String getCtrBTerms() {
		return ctrBTerms;
	}

	public void setCtrBTerms(String ctrBTerms) {
		this.ctrBTerms = ctrBTerms;
	}

	public BigDecimal getCcyRevalAmt() {
		return ccyRevalAmt;
	}

	public void setCcyRevalAmt(BigDecimal ccyRevalAmt) {
		this.ccyRevalAmt = ccyRevalAmt;
	}

	public BigDecimal getCtrRevalAmt() {
		return ctrRevalAmt;
	}

	public void setCtrRevalAmt(BigDecimal ctrRevalAmt) {
		this.ctrRevalAmt = ctrRevalAmt;
	}

	public BigDecimal getCcyNpvAmt() {
		return ccyNpvAmt;
	}

	public void setCcyNpvAmt(BigDecimal ccyNpvAmt) {
		this.ccyNpvAmt = ccyNpvAmt;
	}

	public BigDecimal getCtrNpvAmt() {
		return ctrNpvAmt;
	}

	public void setCtrNpvAmt(BigDecimal ctrNpvAmt) {
		this.ctrNpvAmt = ctrNpvAmt;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getCcySettDate() {
		return ccySettDate;
	}

	public void setCcySettDate(String ccySettDate) {
		this.ccySettDate = ccySettDate;
	}

	public String getCtrSettDate() {
		return ctrSettDate;
	}

	public void setCtrSettDate(String ctrSettDate) {
		this.ctrSettDate = ctrSettDate;
	}

	public String getFarNearind() {
		return farNearind;
	}

	public void setFarNearind(String farNearind) {
		this.farNearind = farNearind;
	}

	public String getSwapVdate() {
		return swapVdate;
	}

	public void setSwapVdate(String swapVdate) {
		this.swapVdate = swapVdate;
	}

	public String getSpotFwdind() {
		return spotFwdind;
	}

	public void setSpotFwdind(String spotFwdind) {
		this.spotFwdind = spotFwdind;
	}

	public String getDisaggCcy() {
		return disaggCcy;
	}

	public void setDisaggCcy(String disaggCcy) {
		this.disaggCcy = disaggCcy;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getSettlStatus() {
		return settlStatus;
	}

	public void setSettlStatus(String settlStatus) {
		this.settlStatus = settlStatus;
	}

	public BigDecimal getSpotRate() {
		return spotRate;
	}

	public void setSpotRate(BigDecimal spotRate) {
		this.spotRate = spotRate;
	}

	public String getWriteOff() {
		return writeOff;
	}

	public void setWriteOff(String writeOff) {
		this.writeOff = writeOff;
	}

	public String getWriteOffRemark() {
		return writeOffRemark;
	}

	public void setWriteOffRemark(String writeOffRemark) {
		this.writeOffRemark = writeOffRemark;
	}

	public String getFxSign() {
		return fxSign;
	}

	public void setFxSign(String fxSign) {
		this.fxSign = fxSign;
	}

	public String getCcyPair() {
		return ccyPair;
	}

	public void setCcyPair(String ccyPair) {
		this.ccyPair = ccyPair;
	}

	public String getOutSystemId() {
		return outSystemId;
	}

	public void setOutSystemId(String outSystemId) {
		this.outSystemId = outSystemId;
	}

	public String getRelationDealId() {
		return relationDealId;
	}

	public void setRelationDealId(String relationDealId) {
		this.relationDealId = relationDealId;
	}

	public String getIsSplitFwd() {
		return isSplitFwd;
	}

	public void setIsSplitFwd(String isSplitFwd) {
		this.isSplitFwd = isSplitFwd;
	}

	public String getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(String spotDate) {
		this.spotDate = spotDate;
	}

	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}

	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}

	public String getSettlementMeans() {
		return settlementMeans;
	}

	public void setSettlementMeans(String settlementMeans) {
		this.settlementMeans = settlementMeans;
	}

	public String getCurrencySettlementAccount() {
		return currencySettlementAccount;
	}

	public void setCurrencySettlementAccount(String currencySettlementAccount) {
		this.currencySettlementAccount = currencySettlementAccount;
	}

	public String getCounterCurrencySettlementAccount() {
		return counterCurrencySettlementAccount;
	}

	public void setCounterCurrencySettlementAccount(String counterCurrencySettlementAccount) {
		this.counterCurrencySettlementAccount = counterCurrencySettlementAccount;
	}

	public BigDecimal getFwdRealPlAmt() {
		return fwdRealPlAmt;
	}

	public void setFwdRealPlAmt(BigDecimal fwdRealPlAmt) {
		this.fwdRealPlAmt = fwdRealPlAmt;
	}
	
}
