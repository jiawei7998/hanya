package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.CbtWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/cbtExporter")
public interface CbtService {

	/**
     * 交易录入
     * 
     * @param ibo
     * @return
     */
    SlOutBean saveEntry(CbtWksBean cbt);
    
    /**
     * 交易冲销
     * @param cbt
     * @return
     */
    SlOutBean reverseTrading(CbtWksBean cbt);
}
