package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.expand.SchduleRefix;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/swapExporter")
public interface SwapService {

	/**
	 * 利率互换保存
	 * 
	 * @return
	 */
	SlOutBean saveIrsEntry(SwapWksBean irs);

	/**
	 * 利率互换+现金流产生
	 * 
	 * @param swap
	 * @return
	 */
	SlOutBean saveIrsWithSchedule(SwapWksBean irs);

	/**
	 * 根据假日调整现金流
	 * 
	 * @param irs
	 * @return
	 */
	SlOutBean RefixScheduleByHlyd(List<SchduleRefix> srLst);

	/**
	 * 货币互换保存保存
	 * 
	 * @return
	 */
	SlOutBean saveCcsEntry(SwapWksBean ccs);

	/**
	 * 利率互换+现金流产生
	 * 
	 * @param swap
	 * @return
	 */
	SlOutBean saveCcsWithSchedule(SwapWksBean ccs);

	/**
	 * 交易冲销
	 *
	 * @return
	 */
	SlOutBean reverseCcs(SwapWksBean ccs);

	SlOutBean reverseIrs(SwapWksBean irs);

	/**
	 * 提前终止
	 * @param irs
	 * @return
	 */
	SlOutBean terminate(SwapWksBean irs);

}
