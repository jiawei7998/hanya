package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.FeeWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/feeExporter")
public interface FeeService {
	
	/**
	 * 添加费用信息
	 * @param feeWksBean
	 * @return
	 */
	SlOutBean saveEntry(FeeWksBean feeWksBean);
	
	/**
	 * 冲销费用记录
	 * 
	 * @param feeWksBean
	 * @return
	 */
	SlOutBean reverseTrading(FeeWksBean feeWksBean);
}
