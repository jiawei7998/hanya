package com.singlee.financial.wks.expand;

/**
 * 日期调整规则对应的业务类型
 * @author chenxh
 *
 */
public enum DateConventionType {

	/**
	 * 利息计息结束日期
	 */
	INTEREST,
	
	/**
	 * 利率重定日期
	 */
	RESET,
	
	/**
	 * 付息日期
	 */
	PAYMENT
}
