package com.singlee.financial.wks.bean;

import java.math.BigDecimal;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 银行间现券买卖对象
 * 
 * @author xuqq
 *
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CbtWksBean extends CommonBean {
	private static final long serialVersionUID = 1L;
	private String tradeNo;// 审批编号
	private String cno;// 交易对手
	private String bondCode;// 债券代码
	private String bookId; // 成本中心
	private String transDate; // 交易日期
	private BigDecimal totalFaceValue;// 券面总额
	private String ptype;// 产品类型
	private BigDecimal netPrice;// 净价
	private String trdType;// 交易种类
	private BigDecimal fullAmt;// 结算金额
	private BigDecimal totalInterestAmount;// 应计利息总额
	private BigDecimal tradeAmount;// 券数量
	private String settlementMethod;// 结算方式
	private String settlementAmount;// 结算日期
	private BigDecimal yield;// 到期收益率
	private String secsacct;// 托管帐户:托管账户
	private String ccysacct;// 资金结算账号:清算账号
	private String Invtype;// 投资类型:管理要素—A/T/H/S/I 可供出售
	
	
	public String getTradeNo() {
		return tradeNo;
	}


	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getBondCode() {
		return bondCode;
	}


	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}


	public String getBookId() {
		return bookId;
	}


	public void setBookId(String bookId) {
		this.bookId = bookId;
	}


	public String getTransDate() {
		return transDate;
	}


	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}


	public BigDecimal getTotalFaceValue() {
		return totalFaceValue;
	}


	public void setTotalFaceValue(BigDecimal totalFaceValue) {
		this.totalFaceValue = totalFaceValue;
	}


	public String getPtype() {
		return ptype;
	}


	public void setPtype(String ptype) {
		this.ptype = ptype;
	}


	public BigDecimal getNetPrice() {
		return netPrice;
	}


	public void setNetPrice(BigDecimal netPrice) {
		this.netPrice = netPrice;
	}


	public String getTrdType() {
		return trdType;
	}


	public void setTrdType(String trdType) {
		this.trdType = trdType;
	}


	public BigDecimal getFullAmt() {
		return fullAmt;
	}


	public void setFullAmt(BigDecimal fullAmt) {
		this.fullAmt = fullAmt;
	}


	public BigDecimal getTotalInterestAmount() {
		return totalInterestAmount;
	}


	public void setTotalInterestAmount(BigDecimal totalInterestAmount) {
		this.totalInterestAmount = totalInterestAmount;
	}


	public BigDecimal getTradeAmount() {
		return tradeAmount;
	}


	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}


	public String getSettlementMethod() {
		return settlementMethod;
	}


	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod;
	}


	public String getSettlementAmount() {
		return settlementAmount;
	}


	public void setSettlementAmount(String settlementAmount) {
		this.settlementAmount = settlementAmount;
	}


	public BigDecimal getYield() {
		return yield;
	}


	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}


	public String getSecsacct() {
		return secsacct;
	}


	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}


	public String getCcysacct() {
		return ccysacct;
	}


	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}


	public String getInvtype() {
		return Invtype;
	}


	public void setInvtype(String invtype) {
		Invtype = invtype;
	}


	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}


	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}


	/**
	 * 清算信息
	 */
	private SettleInfoWksBean settleAcct;
}
