package com.singlee.financial.wks.bean;

import java.math.BigDecimal;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class OtcWksBean extends CommonBean {
	/**
	*
	*/
	private static final long serialVersionUID = 1L;

	/**
	 * 交易日期YYYY-MM-DD
	 */
	private String dealDate;
	/**
	 * 期权费交割日期YYYY-MM-DD
	 */
	private String fDDate;
	/**
	 * 到日期YYYY-MM-DD
	 */
	private String mDate;
	/**
	 * 行权日YYYY-MM-DD
	 */
	private String dDate;
	/**
	 * 交易方向
	 */
	private String ps;
	/**
	 * 买入看涨看跌
	 */
	private String ccyCp;

	/**
	 * 交易货币1
	 */
	private String ccy;
	/**
	 * 交易货币1金额
	 */
	private BigDecimal ccyAmt;
	/**
	 * 执行价格
	 */
	private BigDecimal strikeRate;
	/**
	 * 交易货币2
	 */
	private String ctrCcy;
	/**
	 * 交易货币2金额,CCYPRIN* STRIKE_8
	 */
	private BigDecimal ctrAmt;
	/**
	 * 期权费货币，只能是CCY或者CTRCCY，不支持第三币种
	 */
	private String feesCcy;
	/**
	 * 期权费费率,如果值为空或者0并且PREMCCY=CCY则为PREMAMT/CCYPRIN；如果PREMCCY=
	 * CTRCCY则为PREMAMT/CTRPRIN
	 */
	private BigDecimal feesRate;
	/**
	 * 费用金额
	 */
	private BigDecimal feesAmt;
//    /**
//     * 期权费拆本国货币汇率
//     */
//    private BigDecimal ccyBaseRate;
//    /**
//     * 期权费拆本国货币金额
//     */
//    private BigDecimal ccyPremBaseAmt;
//    /**
//     * 货币1期权费率,如果PREMCCY=CCY则为PREMRATE_8否则为0
//     */
//    private BigDecimal ccyPremRate;
//    /**
//     * 货币1期权费金额,如果PREMCCY=CCY则为PREMAMT否则为0
//     */
//    private BigDecimal ccyPremAmt;
//    /**
//     * 货币2期权费率,如果PREMCCY=CTRCCY则为PREMRATE_8否则为0
//     */
//    private BigDecimal ctrPremRate;
//    /**
//     * 货币2期权费金额,如果值PREMCCY=CTRCCY则为PREMAMT否则为0
//     */
//    private BigDecimal ctrPremAmt;
	/**
	 * 期权类型
	 */
	private String otcType;
	/**
	 * 期权模式
	 */
	private String style;
	/**
	 * CCY兑 CTRCCY的汇率，如果CCY或者CTRCCY=人民币，取REVP.SPOTRATE_8
	 * REVP.CCY=(CCY或者是CTRCCY不等于人民币的币种)，否则分别取CCY、CTRCCY货币汇率进行计算，CCY SPOTRATE /
	 * CTRCCY SPOTRATE
	 */
	private BigDecimal spotRate;
	/**
	 * 货币1delta
	 */
	private BigDecimal ccyDelta;
	/**
	 * 货币2delta
	 */
	private BigDecimal ctrDelta;
	/**
	 * 隐含波动率
	 */
	private BigDecimal volatility;

	/**
	 * 交易对手
	 */
	private String cno;
	/**
	 * 机构编号
	 */
	private String instId;

	/**
	 * 清算信息
	 */
	private SettleInfoWksBean settleAcct;

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getFDDate() {
		return fDDate;
	}

	public void setFDDate(String fDDate) {
		this.fDDate = fDDate;
	}

	public String getMDate() {
		return mDate;
	}

	public void setMDate(String mDate) {
		this.mDate = mDate;
	}

	public String getDDate() {
		return dDate;
	}

	public void setdDate(String dDate) {
		this.dDate = dDate;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getCcyCp() {
		return ccyCp;
	}

	public void setCcyCp(String ccyCp) {
		this.ccyCp = ccyCp;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getCcyAmt() {
		return ccyAmt;
	}

	public void setCcyAmt(BigDecimal ccyAmt) {
		this.ccyAmt = ccyAmt;
	}

	public BigDecimal getStrikeRate() {
		return strikeRate;
	}

	public void setStrikeRate(BigDecimal strikeRate) {
		this.strikeRate = strikeRate;
	}

	public String getCtrCcy() {
		return ctrCcy;
	}

	public void setCtrCcy(String ctrCcy) {
		this.ctrCcy = ctrCcy;
	}

	public BigDecimal getCtrAmt() {
		return ctrAmt;
	}

	public void setCtrAmt(BigDecimal ctrAmt) {
		this.ctrAmt = ctrAmt;
	}

	public String getFeesCcy() {
		return feesCcy;
	}

	public void setFeesCcy(String feesCcy) {
		this.feesCcy = feesCcy;
	}

	public BigDecimal getFeesRate() {
		return feesRate;
	}

	public void setFeesRate(BigDecimal feesRate) {
		this.feesRate = feesRate;
	}

	public BigDecimal getFeesAmt() {
		return feesAmt;
	}

	public void setFeesAmt(BigDecimal feesAmt) {
		this.feesAmt = feesAmt;
	}

	public String getOtcType() {
		return otcType;
	}

	public void setOtcType(String otcType) {
		this.otcType = otcType;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public BigDecimal getSpotRate() {
		return spotRate;
	}

	public void setSpotRate(BigDecimal spotRate) {
		this.spotRate = spotRate;
	}

	public BigDecimal getCcyDelta() {
		return ccyDelta;
	}

	public void setCcyDelta(BigDecimal ccyDelta) {
		this.ccyDelta = ccyDelta;
	}

	public BigDecimal getCtrDelta() {
		return ctrDelta;
	}

	public void setCtrDelta(BigDecimal ctrDelta) {
		this.ctrDelta = ctrDelta;
	}

	public BigDecimal getVolatility() {
		return volatility;
	}

	public void setVolatility(BigDecimal volatility) {
		this.volatility = volatility;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public SettleInfoWksBean getSettleAcct() {
		return settleAcct;
	}

	public void setSettleAcct(SettleInfoWksBean settleAcct) {
		this.settleAcct = settleAcct;
	}
	
	
}
