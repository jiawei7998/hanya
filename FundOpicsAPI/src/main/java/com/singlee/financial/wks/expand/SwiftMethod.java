package com.singlee.financial.wks.expand;

public enum SwiftMethod {

	/**
	 * 打开所有报文
	 */
	ENABLE,
	
	/**
	 * 处理确认报文
	 */
	CONFIRMATION,
	
	/**
	 * 处理支付和收款报文
	 */
	PAYANDREC,
	
	/**
	 * 关闭所有报文
	 */
	DISABLE
	
}
