package com.singlee.financial.wks.intfc;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.wks.bean.FxdWksBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/fxdExporter")
public interface FxdService {

    /**
     * 外汇交易即期、远期标志
     * 
     * @param fxdWksBean
     * @return
     */
    SlOutBean saveStopFwdEntry(FxdWksBean fxdWksBean);
    
    /**
     * 外汇交易即期、远期录入接口.根据基础交易信息计算相关字段值
     * @param fxdWksBean
     * @return
     */
    SlOutBean saveStopFwdByDealInfo(FxdWksBean fxdWksBean);

    /**
     * 外汇掉期交易
     */
    SlOutBean saveSwapEntry(FxdWksBean fxdWksBean);
    
    /**
     * 外汇掉期交易交易接口.根据基础交易信息计算相关字段值
     * @param fxdWksBean
     * @return
     */
    SlOutBean saveSwapByDealInfo(FxdWksBean fxdWksBean);

    /**
     * 交易冲销
     * 
     * @param fxdWksBean
     * @return
     */
    SlOutBean reverseTrading(FxdWksBean fxdWksBean);
}
