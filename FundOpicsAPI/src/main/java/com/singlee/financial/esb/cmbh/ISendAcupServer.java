package com.singlee.financial.esb.cmbh;


import com.singlee.financial.bean.SlAcupTaxBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 总账发送
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhSendAcupServerExporter")
public interface ISendAcupServer {
	/**
	   *   总账文件发送
	 * @param acupBean
	 * @return
	 */
	public SlOutBean sendAcup(SlAcupTaxBean acupBean);
	/**
	   *   总账文件取回
	 * @param acupBean
	 * @return
	 */
	public SlOutBean getAcup(SlAcupTaxBean acupBean);

}
