package com.singlee.financial.esb.dlcb;



import com.singlee.financial.bean.SlNupdBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankDealOffsetServerExporter")
public interface ISlDealOffsetServer {

	/**
	 * 发送交易冲销报文发送
	 * 
	 * @param map
	 * @return
	 */
	SlOutBean offsetSend(SlNupdBean nupd) throws Exception;
}
