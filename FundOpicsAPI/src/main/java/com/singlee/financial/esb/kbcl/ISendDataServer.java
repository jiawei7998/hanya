package com.singlee.financial.esb.kbcl;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.KBStruckBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * ClassName:IBuildServer <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-7-5 上午08:51:23 <br/>
 * @author   xuqq
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/kbSendDataServerExporter")
public interface ISendDataServer {

	public KBStruckBean sendDealDataToCore(KBStruckBean kBStruckBean);
	
	public List<String> queryDealDataEntry(Map<String,Object> map);
	public List<String> queryDealDataExp(Map<String,Object> map);
	public List<String> queryDealDataCancel(Map<String,Object> map);
	public List<String> getOtcPreApprovalPmtqInf(Map<String,Object> map);
	public List<String> queryDealDataNight(Map<String,Object> map);
	public void updateFeesInth(Map<String,Object> map);
}
