/**
 * Project Name:FundOpicsAPI
 * File Name:IQueryServer.java
 * Package Name:com.singlee.financial.esb
 * Date:2018-7-5上午09:03:58
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.esb.tlcb;

import com.singlee.financial.bean.SlCustBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * ClassName:IQueryServer <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-7-5 上午09:03:58 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbQueryServerExporter")
public interface IQueryServer {
	/**
	 * 
	 * 查询对公客户基本信息
	 * @author zhengfl
	 * @param custBean
	 * @return
	 * @since JDK 1.6
	 */
	public SlCustBean ecif0014(SlCustBean custBean);
}

