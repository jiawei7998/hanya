package com.singlee.financial.esb.hsbank;

import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/hsbankCoreServerExporter")
public interface ICoreServer {

	/**
	 * 1.总账文件生成,2.上传文件到ESB,3.通知核心系统入账
	 * 
	 * @param map
	 * @return
	 */
	SlOutBean sendCore(Map<String, String> map);

	/**
	 * 查询核心回执文件,解析后更新到SL_ACUP表
	 * 
	 * @param map
	 * @return
	 */
	SlOutBean reviceCoreResult(Map<String, String> map);

}
