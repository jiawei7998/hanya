package com.singlee.financial.esb.cmbh;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlIntfcGent;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 接口参数表
 * @author zhengfl
 *
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhIntfcGentServerExporter")
public interface ISlIntfcGentServer {

	/**
	 * 添加opics 库中的SL_INTFC_GENT表
	 * 
	 * @param gentBean
	 * @return
	 */
	SlOutBean addSlGent(SlIntfcGent gentBean);

	/**
	 * 修改opics 库中的SL_INTFC_GENT表
	 * 
	 * @param gentBean
	 * @return
	 */
	SlOutBean updateSlGent(SlIntfcGent gentBean);

	/**
	 * 删除opics 库中的SL_INTFC_GENT表的某一条数据
	 * 
	 * @param gentBean
	 * @return
	 */
	SlOutBean deleteSlGent(SlIntfcGent gentBean);
	
	/**
	 * 分页查询
	 */
	PageInfo<SlIntfcGent> searchPageGent(Map<String,Object> map);
	
	
	
	/**
	 * 根据主键查询有无此数据
	 */
	SlIntfcGent searchSlIntfcGent(Map<String,Object> map);

	String getObject(String paramod, String br,String paraid,String part1,String part2);
	
	String getPart2(String paramod, String br,String paraid,String part1,String object);
	
	/***
	 * 获取参数列表
	 * @param map
	 * @return
	 */
	List<SlIntfcGent>  getListSlIntfcGent(Map<String,Object> map);
	
	
	
	
	
	
	
	
	
	
	
	
	
}