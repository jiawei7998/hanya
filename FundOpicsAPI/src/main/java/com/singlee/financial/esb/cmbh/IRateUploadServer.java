package com.singlee.financial.esb.cmbh;


import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 利率上传
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhRateUploadServerExporter")
public interface IRateUploadServer {
	
	public SlOutBean rateUpload(Map<String, Object> map);
	
	
}
