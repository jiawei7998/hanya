package com.singlee.financial.esb.hrbcb;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlPmtqAcupBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/hrbbankSlPmtqAcupServerExporter")
public interface ISlPmtqAcupServer {

	/**
	 *  获取日间往来账
	 * @param slPmtqAcupBean
	 * @return
	 */
	public List<SlPmtqAcupBean> getSlPmtqAcupList(SlPmtqAcupBean slPmtqAcupBean);

	/**
	 * 获取日间往来账 dealno带trim
	 * @param slPmtqAcupBean
	 * @return
	 */
	public List<SlPmtqAcupBean> getSlPmtqAcupListAndTrim(SlPmtqAcupBean slPmtqAcupBean);
	
	/**
	 * 查询单个日间账
	 * @param slPmtqAcupBean
	 * @return
	 */
	public SlPmtqAcupBean getSlPmtqAcup(SlPmtqAcupBean slPmtqAcupBean);

	/**
	 * 账务发送后更新发送状态
	 * @param slPmtqAcupBean
	 * @param example
	 */
	public void updateSlPmtqAcup(SlPmtqAcupBean slPmtqAcupBean);

//   List<SlPmtqAcupBean> getSlPmtqAcupByDealno(Map<String, Object> reMap);
}
