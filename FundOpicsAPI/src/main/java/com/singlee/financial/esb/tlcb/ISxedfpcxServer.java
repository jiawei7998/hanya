package com.singlee.financial.esb.tlcb;

import com.singlee.financial.bean.SlLoanBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 授信额度分配查询
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbSxedfpcxServerExporter")
public interface ISxedfpcxServer {

	/**
	 * 信贷查询接口
	 */
	public SlLoanBean LN03020015(SlLoanBean loanBean);
}