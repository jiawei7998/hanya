package com.singlee.financial.esb.dlcb;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlCoreSubject;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 记账科目操作
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankSubjectAcupServerExporter")
public interface ISubjectAcupServer {
	// 新增
	public void insert(SlCoreSubject entity);

	// 修改
	void updateById(SlCoreSubject entity);

	// 删除
	void deleteById(String id);

	// 分页查询
	PageInfo<SlCoreSubject> searchPageOpicsSubject(Map<String, Object> map);

	// 下拉树查询
	public List<SlCoreSubject> searchSubject(HashMap<String, Object> map);

	SlCoreSubject searchById(String id);

	void updateStatus(SlCoreSubject entity);

	// 批量校准
	public SlOutBean batchCalibration(String type, String[] costcents);

}
