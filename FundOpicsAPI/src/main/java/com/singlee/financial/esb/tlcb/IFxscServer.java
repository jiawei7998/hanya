package com.singlee.financial.esb.tlcb;

import java.util.Map;

import com.singlee.financial.bean.SlIfsBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 同业接口服务
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbFxscServerExporter")
public interface IFxscServer {

	/**
	 * 信用风险审查查询接口
	 */
	public SlIfsBean TY004(Map<String,String> map);
}