package com.singlee.financial.esb.cmbh;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.financial.bean.ColDefBean;
import com.singlee.financial.bean.ImportOpicsResultBean;
import com.singlee.financial.bean.OpicsDealResultBean;
import com.singlee.financial.bean.ResultBean;
import com.singlee.financial.bean.SlMmktIrevBean;
import com.singlee.financial.bean.SlMmktIrhsBean;
import com.singlee.financial.bean.SlMmktIselForBean;
import com.singlee.financial.bean.SlMmktIselTmpBean;
import com.singlee.financial.bean.SlMmktIvolBean;
import com.singlee.financial.bean.SlMmktIycrBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.bean.SlImportResultQueryBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 市场数据
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhMmktServerExporter")
public interface IMmktServer {
	
	public Map<String, ColDefBean> getColDefMap(String paramString);

	public int getFilNextImpSeq(String mmktDataType, Date branPrcDate) throws Exception;

	public void invalidPendding(String mmktDataType, List<String> brs, Date branPrcDate) throws Exception;

	public String getFedealNo(String tableId);

	public int queryAwaitDealImpSeq(String br, Date branPrcDate, String dataType) throws Exception;

	public ImportOpicsResultBean importIrevToOpics(String br, Date branPrcDate,
			String dataType, int impSeq, String iOper);

	public ImportOpicsResultBean importIrhsToOpics(String br, Date branPrcDate,
			int impSeq, String iOper);

	public ImportOpicsResultBean importIvolToOpics(String br, Date branPrcDate,
			int impSeq, String iOper);

	public ImportOpicsResultBean importIycrToOpics(String br, Date branPrcDate,
			int impSeq, String iOper);

	public ImportOpicsResultBean importIselToOpics(String br, Date branPrcDate,
			int impSeq, String iOper);

	public ImportOpicsResultBean importIselForToOpics(String br,
			Date branPrcDate, int impSeq, String iOper);

	public boolean saveSlIrev(SlMmktIrevBean slMmktIrevBean) throws Exception;

	public boolean saveSlIycr(SlMmktIycrBean bean) throws Exception;

	public boolean saveSlIvol(SlMmktIvolBean bean) throws Exception;

	public boolean saveSlIselFor(SlMmktIselForBean bean) throws Exception;

	public boolean saveSlIselTmp(SlMmktIselTmpBean bean) throws Exception;

	public void checkSeclData(Date branPrcDate, int impSeq);

	public ResultBean saveIselProc(String br, Date branPrcDate, int impSeq,
			String userId);

	public void truncateSlIselTemp();

	public boolean saveSlIrhs(SlMmktIrhsBean bean) throws Exception;
	
	PageInfo<SlImportResultQueryBean> queryMmktPage(Map<String,Object> map) throws RemoteConnectFailureException, Exception;

	public <T> PageInfo<T> queryMmktDetailPage(Map<String, Object> map);
	
	PageInfo<OpicsDealResultBean> queryMmktOpicsPage(Map<String,Object> map) throws RemoteConnectFailureException, Exception;

	public <T> PageInfo<T> queryMmktOpicsDetailPage(Map<String, Object> map);

}
