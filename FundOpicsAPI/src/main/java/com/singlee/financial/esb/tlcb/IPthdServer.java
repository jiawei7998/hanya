/**
 * Project Name:FundOpicsAPI
 * File Name:PthdServer.java
 * Package Name:com.singlee.financial.esb
 * Date:2018-7-2下午06:00:33
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.esb.tlcb;


import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSettleBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * ClassName:PthdServer <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-7-2 下午06:00:33 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbPthdServerExporter")
public interface IPthdServer {
	/**
	 * 
	 * 与esb通讯，发送及返回报文数据以及错误信息
	 * @author zhengfl
	 * @param settleBean
	 * @return
	 * @since JDK 1.6
	 */
	public SlOutBean cnaps01(SlSettleBean settleBean);
}

