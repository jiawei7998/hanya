package com.singlee.financial.esb.hrbcb;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.SendInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/hrbbankSlAcupServerExporter")
public interface ISlAcupServer {

	/**
	 * 查询当前日期下所有日终总账
	 * @param map
	 * @return
	 */
	public List<AcupSendBean> getSlAcupList(Map<String, Object> map);
	
	/**
	 *查询错账
	 * @param map
	 * @return
	 */
	public List<AcupSendBean> getSlAcupBySetNo(Map<String, Object> map);
	
	/**
	 * 批量修改状态
	 * @param list
	 */
	public void updateBatchSlAcupHendle(List<AcupSendBean> list);
	
	/**
	 * 修改总账发送监控表状态
	 * @param sendInfo
	 */
	public void updateSLAcupSendinfo(SendInfo sendInfo);
	
	/**
	 * 查询总账发送监控表
	 * @return
	 */
	public SendInfo getSLAcupSendinfo();
	
}
