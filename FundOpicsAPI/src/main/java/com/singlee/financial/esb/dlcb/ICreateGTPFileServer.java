package com.singlee.financial.esb.dlcb;

import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 大连数据仓库文件生成
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankCreateGTPFileServerExporter")
public interface ICreateGTPFileServer {
	
	public SlOutBean createGTPFile(Map<String, Object> map);
}
