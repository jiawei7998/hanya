/**
 * Project Name:FundOpicsAPI
 * File Name:IJyztcxServer.java
 * Package Name:com.singlee.financial.esb
 * Date:2018-7-4下午06:30:31
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.esb.tlcb;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSettleBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * ClassName:IJyztcxServer <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-7-4 下午06:30:31 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbJyztcxServerExporter")
public interface IJyztcxServer {
	/**
	 * esb交易状态查询
	 *
	 * @author zhengfl
	 * @param settleBean
	 * @return
	 * @since JDK 1.6
	 */
	public SlOutBean cnaps02(SlSettleBean settleBean);
}

