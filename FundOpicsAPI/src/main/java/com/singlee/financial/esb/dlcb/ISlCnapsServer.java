package com.singlee.financial.esb.dlcb;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlCnapsBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSettleBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS系统cnaps JOB发送。
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankCnapsServerExporter")
public interface ISlCnapsServer {
	/**
	 * 查询SL_CNAPS表中未取走的当日数据
	 * @param map
	 * @return
	 */
	List<SlCnapsBean>  getCnapsList(Map<String, Object> map);
	
	public SlOutBean search(SlSettleBean settleBean);
	
	public SlOutBean send(SlSettleBean settleBean);
}
