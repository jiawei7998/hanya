package com.singlee.financial.esb.dlcb;

import java.util.List;
import java.util.Map;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 同业客户信息综合查询
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankQueryCustServerExporter")
public interface IQueryCustServer {
	/**
	 * 根据客户号查询一条同业客户信息
	 * @param SlCustBean
	 * @return
	 */
	public SlCustBean searchOneCust(SlCustBean custBean);
	/**
	 * 根据传入参数,查询多条同业客户信息
	 * @param map
	 * @return
	 */
	public List<SlCustBean> searchCustList(Map<String, Object> map);
	
}
