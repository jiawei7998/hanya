package com.singlee.financial.esb.cmbh;

import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 查询利率互换代客交易
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhQueryCoreIrsServerExporter")
public interface IQueryCoreIrsServer {
	
	public SlOutBean queryCoreIrs(Map<String, Object> map);
}
