package com.singlee.financial.esb.dlcb;

import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 批量代理业务通知:本服务用于通用账务处理及交易信息登记(日终并账) 1661
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankAcupSendServerExporter")
public interface IAcupSendServer {
	/**
	 * 根据日期生成并账文件并上传
	 * @param SlAcupBean
	 * @return
	 */
	public SlOutBean acupSend(SlAcupBean acupBean);
	
	
}
