package com.singlee.financial.esb.tlcb;

import java.util.Map;

import com.singlee.financial.bean.SlNcbsBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 内部户交易信息查询
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbNbhcxServerExporter")
public interface INbhcxServer {
	
	public SlNcbsBean nbhcx(Map<String,String> map);
}