package com.singlee.financial.esb.cmbh;

import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 查询核心外汇交易
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhQueryCoreFxServerExporter")
public interface IQueryCoreFxServer {
	
	public SlOutBean queryCoreFx(Map<String, Object> map);
}
