package com.singlee.financial.esb.cmbh;


import com.singlee.financial.bean.FxblBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 外币记账
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhSendFxblServerExporter")
public interface ISendFxblServer {
	
	public SlOutBean send(FxblBean fxblBean);

	public SlOutBean search(FxblBean fxblBean);
}
