package com.singlee.financial.esb.cmbh;

import java.util.List;
import java.util.Map;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.financial.bean.RetFileInfo;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.util.Pair;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
/***
 * 债券信息上传
 * @author LIJ
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/odsUploadServerExporter")
public interface IOdsUploadServer {
	
	public Pair<SlOutBean,List<RetFileInfo>> odsUpload(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	

}
