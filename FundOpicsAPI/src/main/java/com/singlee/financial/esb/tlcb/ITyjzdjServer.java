/**
 * Project Name:FundOpicsAPI
 * File Name:ITyjzdjServer.java
 * Package Name:com.singlee.financial.esb.tlcb
 * Date:2018-7-26下午02:53:15
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.esb.tlcb;

import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * ClassName:ITyjzdjServer <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-7-26 下午02:53:15 <br/>
 * @author   10093
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbTyjzdjServerExporter")
public interface ITyjzdjServer {
	
	public SlOutBean w060(SlAcupBean acupBean);
}

