package com.singlee.financial.esb.dlcb;

import java.util.List;
import java.util.Map;


import com.singlee.financial.bean.SlNupdBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankNupdServerExporter")
public interface ISlNupdServer {

	/**
	 * 发送nupd报文
	 * 
	 * @param map
	 * @return
	 */
	SlOutBean nupdSend(List<SlNupdBean> list) throws Exception;

	/**
	 * 监控查询往来帐的结果
	 * 
	 * @param map
	 * @return
	 */
	PageInfo<SlNupdBean> getNupdRetMsg(Map<String, String> map);

	List<SlNupdBean> getNupdById(Map<String, String> map);
	
	List<SlNupdBean> getSendNudp(Map<String, String> map);
	
	void updateAllNudp();

	void updateNudp(Map<String, String> map);
}
