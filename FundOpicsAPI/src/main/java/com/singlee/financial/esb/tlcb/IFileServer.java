package com.singlee.financial.esb.tlcb;

import java.util.List;

import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.LoanBean;
import com.singlee.financial.bean.TdedCust;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 泰隆文件传输接口
 * 
 * @author Qxj
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/fileServerExporter")
public interface IFileServer {

	/**
	 * 生成总账文件
	 * 
	 * @param list
	 *            文件内容
	 * @param fileNo
	 *            文件名称
	 * @return
	 */
	public String[] CreateAcupFile(List<AcupSendBean> list, String fileNo);

	/**
	 * 文件上传
	 * 
	 * @param sysName
	 *            系统名称
	 * @param localFile
	 *            本地文件
	 * @param remoteFile
	 *            远端文件
	 * @param tranCode
	 *            ESB交易代码
	 * @return
	 */
	public String filePut(String sysName, String localFile, String remoteFile, String tranCode);

	/**
	 * 文件下载
	 * 
	 * @param sysName
	 *            系统名称
	 * @param localFile
	 *            本地文件
	 * @param remoteFile
	 *            远端文件
	 * @param tranCode
	 *            ESB交易代码
	 * @param retMsg
	 *            返回信息
	 * @param flag
	 *            返回标志位
	 * @return
	 */
	public String fileGet(String sysName, String localFile, String remoteFile, String tranCode, String retMsg, String flag);

	/**
	 * 生成信贷额度明细文件
	 * 
	 * @param list
	 *            额度明细内容
	 * @return
	 */
	public String createLoanDetailFile(List<LoanBean> list);

	/**
	 * 生成信贷额度汇总文件
	 * 
	 * @param list额度汇总内容
	 * @return
	 */
	public String createLoanGatherFile(List<TdedCust> list);

	/**
	 * 创建OK文件
	 * 
	 * @return
	 */
	public String createBlankFile();
}