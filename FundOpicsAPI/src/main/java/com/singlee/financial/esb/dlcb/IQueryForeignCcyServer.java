package com.singlee.financial.esb.dlcb;

import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 总行外币平盘数据查询接口 7445 JOB查询。
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankQueryForeignCcyServerExporter")
public interface IQueryForeignCcyServer {
	
	public SlOutBean queryForeignCcy(Map<String, Object> map);
	
	public SlOutBean irs9RefluxFile(Map<String, Object> map);
}
