/**
 * Project Name:FundOpicsAPI
 * File Name:IBuildServer.java
 * Package Name:com.singlee.financial.esb
 * Date:2018-7-5上午08:51:23
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.esb.tlcb;

import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * ClassName:IBuildServer <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-7-5 上午08:51:23 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/tlcbBuildServerExporter")
public interface IBuildServer {
	/**
	 * 
	 * 新建对公客户基本信息
	 * @author zhengfl
	 * @param custBean
	 * @return
	 * @since JDK 1.6
	 */
	public SlOutBean ecif0031(SlCustBean custBean);
}

