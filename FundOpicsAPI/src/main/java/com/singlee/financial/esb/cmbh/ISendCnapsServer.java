package com.singlee.financial.esb.cmbh;


import com.singlee.financial.bean.PmtqBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * CNAPS
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhSendCnapsServerExporter")
public interface ISendCnapsServer {
	
	public SlOutBean send(PmtqBean pmtqBean);

	public SlOutBean search(PmtqBean pmtqBean);
}
