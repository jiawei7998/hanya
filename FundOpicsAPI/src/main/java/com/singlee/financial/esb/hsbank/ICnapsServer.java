package com.singlee.financial.esb.hsbank;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlCnapsBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS系统cnaps JOB发送。
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/hsbankCnapsServerExporter")
public interface ICnapsServer {

	/**
	 * 发送cnaps报文
	 * 
	 * @param map
	 * @return
	 */
	SlOutBean cnapsSend(List<SlCnapsBean> list);
	
	
	PageInfo<SlCnapsBean>  getAcupList(Map<String, Object> map);
}
