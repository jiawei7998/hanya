package com.singlee.financial.esb.dlcb;


import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 代发结果查询:本服务用于查询日终并账结果 1604
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/dlbankAcupGetServerExporter")
public interface IAcupGetServer {
	/**
	 * 根据委托号取回并账结果文件
	 * @param SlAcupBean
	 * @return
	 */
	public SlOutBean acupGet(SlAcupBean acupBean);

}
