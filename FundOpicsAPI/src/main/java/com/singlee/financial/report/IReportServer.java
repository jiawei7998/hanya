package com.singlee.financial.report;

import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * OPICS报表接口
 * 
 * 主要处理opics导出报表
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/reportServerExporter")
public interface IReportServer {

	/**
	 * 1.查询ServiceOpicsWeb项目报表文件并返回json文件
	 * 
	 * @param map
	 * @return
	 */
	String getGrssFile(File paramFile) throws  Exception;

	/**
	 * 添加文件
	 * 
	 * @param paramFile
	 * @param fileName
	 *            null 创建文件
	 * @return
	 */
	String addGrssFile(String paramFile, String fileName) throws  Exception;

	/**
	 * 删除文件
	 * 
	 * @param paramFile
	 * @return
	 */
	String delGrssFile(String paramFile, String filename) throws  Exception;

	/**
	 * 保存内容到指定的path下文件中
	 * 
	 * @param paramSql
	 * @param path
	 * @return
	 */
	String addSqltoFile(String paramSql, String path) throws  Exception;

	/**
	 * 保存内容到指定的path下文件中
	 * 
	 * @param path
	 * @return
	 */
	String getSqlfromFile(String path) throws  Exception;

	/**
	 * 执行sql
	 * 
	 * @param paramSql
	 * @return
	 */
	List<LinkedHashMap<String, Object>> runSql(String paramSql) throws  Exception;

	/**
	 * 查询表名称
	 * 
	 * @return
	 */
	List<String> getUserTabComments(String paramTabId) throws  Exception;

	/**
	 * 根据tableId查询具体表字段
	 * 
	 * @param paramTableId
	 * @return
	 */
	List<String> getUserTabColums(String paramTableId) throws  Exception;

}
