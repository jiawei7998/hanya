package com.singlee.financial.util;

public class HessianUtils {

	public static final String EMPTY = "";

	/**
	 * 方法已重载.去掉字符串的空格
	 * 
	 * @param value
	 *            java.lang.String
	 * @return java.lang.String
	 * @throws Exception
	 */
	public static String trim(String value) {
		if (null == value || value.trim().length() == 0) {
			return EMPTY;
		}

		return value.trim();
	}

	/**
	 * 方法已重载.去掉字符串的空格
	 * 
	 * @param value
	 *            java.lang.String
	 * @return java.lang.String
	 * @throws Exception
	 */
	public static String trim(Object value) {
		String tmp = null;
		if (null == value) {
			return EMPTY;
		}

		if (value instanceof String) {
			tmp = (String) value;
		} else {
			tmp = value.toString();
		}

		return trim(tmp);
	}

}
