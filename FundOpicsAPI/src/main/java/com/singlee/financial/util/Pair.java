package com.singlee.financial.util;

import java.io.Serializable;

/**
 * 对对子
 * 
 * @author LyonChen
 */
public class Pair<C1, C2> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private C1 left;
	private C2 right;

	public Pair() {
	}

	public C1 getLeft() {
		return left;
	}

	public void setLeft(C1 left) {
		this.left = left;
	}

	public C2 getRight() {
		return right;
	}

	public void setRight(C2 right) {
		this.right = right;
	}

	public Pair(C1 left, C2 right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return String.format("Pair [left=%s, right=%s]", left, right);
	}


}
