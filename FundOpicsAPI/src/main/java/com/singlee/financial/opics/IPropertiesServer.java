package com.singlee.financial.opics;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProperties;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/propertiesServerExporter")
public interface IPropertiesServer {

	/**
	 * 操作配置文件,存在则修改，不存在则增加
	 */
	SlOutBean operateProperties(SlProperties properties) throws  Exception;

	/**
	 * 获取列表信息
	 * 
	 * @return
	 */
	List<SlProperties> getProperties() throws  Exception;
}
