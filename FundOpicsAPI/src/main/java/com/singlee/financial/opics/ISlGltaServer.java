package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlGltaBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS接口账务匹配规则表
 * 
 * @author shenzl
 *
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/slGltaServerExporter")
public interface ISlGltaServer {

	/**
	 * 添加opics 库中的SL_GLTA表
	 * 
	 * @param gltaBean
	 * @return
	 */
	SlOutBean addSlGent(SlGltaBean gltaBean);

	/**
	 * 添加opics 库中的SL_GLTA表
	 * 
	 * @param gltaBean
	 * @return
	 */
	SlOutBean updateSlGent(SlGltaBean gltaBean);

	/**
	 * 删除opics 库中的SL_GLTA表的某一条数据
	 * 
	 * @param gltaBean
	 * @return
	 */
	void deleteSlGent(SlGltaBean gltaBean);

	/**
	 * 添加opics 库中的SL_GLTA表 根据TABLEID+SECID查询，主要用于判断
	 * 
	 * @param gltaBean
	 * @return
	 */
	SlGltaBean getSlGent(SlGltaBean gltaBean);
	
	/**
	 * 分页查询
	 */
	PageInfo<SlGltaBean> searchPageGent(Map<String,Object> map);
	
	/**
	 * 修改时保存
	 */
	void updateById(SlGltaBean gltaBean);
	
	/**
	 * 根据主键查询有无此数据
	 */
	SlGltaBean searchIfsOpicsGent(Map<String,String> map);

	List<SlGltaBean> searchAllTableid();
}