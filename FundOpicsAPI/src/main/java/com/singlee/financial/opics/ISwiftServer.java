package com.singlee.financial.opics;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/SwiftServerExporter")
public interface ISwiftServer {

	/**
	 * 下载OPics中swift报文,并解析在页面中显示
	 * 
	 * @param map
	 * @return
	 */
	void getSwiftFile(String localPath) throws RemoteConnectFailureException, Exception;

	/**
	 * 下载OPics中swift报文,并解析在页面中显示
	 * 
	 * @param map
	 * @return
	 */
	InputStream getSwiftSmbFileInputStream() throws RemoteConnectFailureException, Exception;

	/**
	 * 直接发送swift文件
	 * 
	 * @param map
	 * @return
	 */
	SlOutBean sendSwiftFile() throws RemoteConnectFailureException, Exception;
	
	/**
	 * 查询WMTI表中的相关数据
	 */
	List<SlSwiftBean> searchPageSwift(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	/**
	 * 根据发送标识查询SL_WMTI表中的相关数据
	 */
	List<SlSwiftBean> searchSwiftBySendflag(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	/**
	 * 修改SL_WMTI的发送标识
	 * @param map
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	void updateSendflag(Map<String, Object> map) throws  Exception;
}