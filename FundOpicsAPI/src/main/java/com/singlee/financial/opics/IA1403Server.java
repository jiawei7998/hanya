package com.singlee.financial.opics;

import com.singlee.financial.bean.A1403Bean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/**
 * 主要用于查询SL_REPORT_A1403（A1403-外资银行资产负债项目月报表）数据
 * @author wangzhao
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/a1403ServerExporter")
public interface IA1403Server {

	/**
	 * 带参数  分页查询 A1403-外资银行资产负债项目月报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<A1403Bean> getA1403Report(Map<String, Object> map) throws  Exception;
	
	
	/**
	 * 带参数  列表查询 A1403-外资银行资产负债项目月报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<A1403Bean> getA1403ReportList(Map<String, Object> map) throws  Exception;
	
	/**
	 * 获取  A1403-外资银行资产负债项目月报表 所有机构
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<String> getA1403ReportAllBr() throws  Exception;

}
