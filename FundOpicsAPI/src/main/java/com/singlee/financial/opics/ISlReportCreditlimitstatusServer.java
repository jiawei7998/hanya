package com.singlee.financial.opics;

import com.singlee.financial.bean.SlReportCreditlimitstatus;
import com.singlee.financial.bean.SlReportCreditrate;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/***
 * 同业额度使用情况报告周报
 * @author LIJ
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/slReportCreditlimitstatusServerExporter")
public interface ISlReportCreditlimitstatusServer {
	
	/**
	 * 分页查询-同业额度使用情况报告周报
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlReportCreditlimitstatus> getPageCreditlimitstatusReport(Map<String, Object> map) throws  Exception;
	
	/**
	 * 列表查询-同业额度使用情况报告周报
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlReportCreditlimitstatus> getListCreditlimitstatusReport(Map<String, Object> map) throws  Exception;

	/***
	 * 获取不重复的sheet集合
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<String> selectDistinctSheet()throws  Exception;
	
	
	/**
	 * 列表查询-汇率
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlReportCreditrate> getListCreditrate(Map<String, Object> map) throws  Exception;
	
	
	
	
	
	
	
	
	
	
	
	




}
