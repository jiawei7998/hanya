package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlCustSettleAcct;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/slCustSettleAcctServerExporter")
public interface ISlCustSettleAcctServer {

	PageInfo<SlCustSettleAcct> getCSList(Map<String, Object> map);

	void updateByCno(Map<String, Object> map);

	SlCustSettleAcct selectByCno(Map<String, Object> map);

	void addCustSettle(Map<String, Object> map);

	void deleteSettle(Map<String, Object> map);

	SlCustSettleAcct selectById(Map<String, Object> map);

	List<SlCustSettleAcct> getListByCno(Map<String, Object> map);

	void updateIsdefaultById(Map<String, Object> map);

	void updateDealFlagById(Map<String, Object> map);
	
}
