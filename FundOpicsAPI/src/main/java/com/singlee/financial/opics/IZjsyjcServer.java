package com.singlee.financial.opics;

import com.singlee.financial.bean.ZjsyjcBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/**
 * 主要用于查询SL_REPORT_ZJSYJC（Zjsyjc-资金业务收益检测表）数据
 * @author wangzhao
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/zjsyjcServerExporter")
public interface IZjsyjcServer {

	/**
	 * 带参数 分页查询Zjsyjc-资金业务收益检测表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<ZjsyjcBean> getZjsyjcReport(Map<String, Object> map) throws  Exception;
	
	/**
	 * 带参数 列表查询Zjsyjc-资金业务收益检测表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<ZjsyjcBean> getZjsyjcReportList(Map<String, Object> map) throws  Exception;
}
