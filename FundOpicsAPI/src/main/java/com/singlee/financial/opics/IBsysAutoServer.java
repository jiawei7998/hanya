package com.singlee.financial.opics;

import java.util.List;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * 主要用于OPICS自动批量任务调度服务
 * 1.在OPICS数据库中创建SL_AUTO_BATCH表,记录每步处理状态.
 * 2.提供调用OPICS日终批量方法,
 * 
 * 
 * 
 * @author shenzl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/bsysAutoExporter")
public interface IBsysAutoServer {
	
	/**
	 * 调用OPICS BSYS批量
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	public SlOutBean callAutoBsys(String br,String postdate,String jobId) throws RemoteConnectFailureException, Exception; 
	
	public SlOutBean callAutoBsys(List<String> brs,String postdate,String jobId) throws RemoteConnectFailureException, Exception; 
}
