package com.singlee.financial.opics;

import com.singlee.financial.bean.SlHsReportBean;
import com.singlee.financial.bean.SlOutSonBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS系统报表报文发送。
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/hsReportServerExporter")
public interface IHsReportServer {

	/**
	 * 获取报表数据
	 * 
	 * @param map
	 * @return
	 */
	SlOutSonBean getHsReportData(SlHsReportBean hsReport) throws  Exception;
}
