package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlOpcKpsjBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/IOpcKpsjServerExporter")
public interface IOpcKpsjServer {

	/**
	 * 拆借交易
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjDldt(String datetime);

	/**
	 * 回购交易
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjRepo(String datetime);

	/**
	 * 债券交易
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjSPsh(String datetime);

	/**
	 * 债券交易处置收益
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjSPshGain(String datetime);

	/**
	 * 通过卖单交易号算出处置收益
	 * @param datetime
	 * @return
	 */
	public SlOpcKpsjBean selectOpcKpsjCountGain(Map<String,Object> map);
	
	/**
	 * 
	 * @param dealno
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjByDelno(String dealno);
}
