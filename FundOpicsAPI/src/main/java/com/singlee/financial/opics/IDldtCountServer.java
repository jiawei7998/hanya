package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.DldtRevaluation;
import com.singlee.financial.bean.SlDldtBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/dldtCountServerExporter")
public interface IDldtCountServer {
	/**
	 * 分页查询
	 */
	PageInfo<SlDldtBean> searchDlCount(Map<String,Object> map);

	/**
	 * 列表查询
	 */
	List<SlDldtBean> searchDlCountlis(Map<String,Object> map);
	/**
	 * 	条件查询
	 * @param map
	 * @return
	 */
	SlDldtBean searchSlDldtBean(Map<String,Object> map);

	/**
	 * 浮动拆借定息查询
	 */
	PageInfo<DldtRevaluation> searchDldtRev(Map<String,Object> map);
}
