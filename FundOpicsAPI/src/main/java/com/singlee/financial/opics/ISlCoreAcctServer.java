package com.singlee.financial.opics;

import com.singlee.financial.bean.SlCoreAcctBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Map;

/**
   * 核心账户
 * @author zhengfl
 *
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/coreAcctServeExporter")
public interface ISlCoreAcctServer {
	/**
	 * 
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlCoreAcctBean> searchPageCoreAcct(Map<String, Object> map) throws  Exception;

	void coreAcctAdd(Map<String, Object> map);

	void updateById(SlCoreAcctBean entity);

	void deleteCoreAcct(Map<String, String> map);

	
	
	
	
	
}