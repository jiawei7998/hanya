package com.singlee.financial.opics;

import com.singlee.financial.bean.SlIdriBean;
import com.singlee.financial.bean.SlIinsBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.Map;

/**
 * 提供OPICS 默认清算路径
 * 
 * CSPI:付款 CSRI:收款
 * 
 * CSPI有头信息(CSPI)和明细(CPCI)两个表 CSPI包含EFFDATE日期选项的有头信息(USPI)和明细(UPCI)两个表
 * 
 * CSRI有一张表(CSRI) CSRI包含EFFDATE日期选项的有一张表(USRI)
 * 
 * SDVP有头信息(SFDI)和明细(SFDC)两个表 SDVP包含EFFDATE日期选项的有头信息(UFDI)和明细(UFDC)两个表
 * 
 * @author shenzl
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/settleManagerServerExporter")
public interface ISettleManagerServer {

	/**
	 * OPICS清算信息（非债券相关） 需配合INTH操作
	 * 
	 * @param iinsBean
	 * @return
	 */
	SlOutBean addSlIinsBean(SlIinsBean iinsBean) throws  Exception;

	/**
	 * OPICS清算信息（债券相关） 需配合INTH操作
	 * 
	 * @param idriBean
	 * @return
	 */
	SlOutBean addSlIdriBean(SlIdriBean idriBean) throws  Exception;
	
	/**
	 * 判断CSPI是否有数据
	 * @param map
	 * @return
	 */
	int searchCspiById(Map<String, Object> map) throws  Exception;
	
	/**
	 * 判断CSRI是否有数据
	 * @param map
	 * @return
	 */
	int searchCsriById(Map<String, Object> map) throws  Exception;
	/**
	 * 判断SDVP是否有数据
	 * @param map
	 * @return
	 */
	int searchSdvpById(Map<String, Object> map) throws  Exception;
	
	
	

}
