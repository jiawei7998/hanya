package com.singlee.financial.opics;

import com.singlee.financial.bean.SlHldyInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;

/**
 * 主要处理OPICS假日及假日文件
 *
 * @author shenzl
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/hldyDbAndFileServer")
public interface IHldyDbAndFileServer {

    /**
     * 处理假日
     *
     * @param hildy
     * @throws RemoteConnectFailureException
     * @throws Exception
     */
    void addHldyProcess(List<SlHldyInfo> slHldyInfoList) throws RemoteConnectFailureException, Exception;
}
