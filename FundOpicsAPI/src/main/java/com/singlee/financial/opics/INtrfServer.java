package com.singlee.financial.opics;

import com.singlee.financial.bean.SlNtrfBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

/**
 * OPICS系统头寸调拨NTRF 接口服务
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/ntrfServerExporter")
public interface INtrfServer {

    /**
     * 头寸调拨导入OPICS中
     *
     * @author shenzl
     * @param SlNtrfBean
     * @return
     * @since JDK 1.6
     */
    SlOutBean intr(SlNtrfBean SlNtrfBean)throws RemoteConnectFailureException, Exception;
}
