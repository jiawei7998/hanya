package com.singlee.financial.opics;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.financial.bean.SlIotdBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS系统 期权 模块服务接口。
 * 
 * @author lij
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/otcServerExporter")
public interface IOtcServer {

	/**
	 * 校验OPICS期权
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean verifiedOtc(SlIotdBean slIotdBean) throws RemoteConnectFailureException, Exception;

	/**
	 * 期权导入
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean otc(SlIotdBean slIotdBean) throws RemoteConnectFailureException, Exception;

	/**
	 * 期权冲销
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean otcRev(SlIotdBean slIotdBean) throws RemoteConnectFailureException, Exception;

	/**
	 * 校验期权冲销
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean verifiedOtcRev(SlIotdBean slIotdBean) throws RemoteConnectFailureException, Exception;

	/**
	 * Deal 行权/到期/触碰
	 * 以新交易方式处理原始DEALNO需要放在IOTD表中
	 * OTC-EXPR-OTDX-到期SERVER
	 * OTC-EXER-OTDX-行权SERVER
	 * OTC-INSP-OTDX-触碰SERVER
	 * @param slIotdBean
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlOutBean otcExecute(SlIotdBean slIotdBean) throws RemoteConnectFailureException, Exception;
}
