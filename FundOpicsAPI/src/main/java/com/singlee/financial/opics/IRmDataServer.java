package com.singlee.financial.opics;

import com.singlee.financial.bean.*;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;
/**
 * 导入市场数据
 * @author zhengfl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/rmDataServerExporter")
public interface IRmDataServer {
	/**
	 * 
	 * @param irevBean
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlOutBean addIrev(SlIrevBean irevBean) throws  Exception;
	/**
	 * 
	 * @param irhsBean
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlOutBean addIrhs(SlIrhsBean irhsBean) throws  Exception;
	/**
	 * 
	 * @param ivolBean
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlOutBean addIvol(SlIvolBean ivolBean) throws  Exception;
	/**
	 * 
	 * @param iycrBean
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlOutBean addIycr(SlIycrBean iycrBean) throws  Exception;

}
