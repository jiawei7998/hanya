package com.singlee.financial.opics;

import com.singlee.financial.bean.SlRprhDldtBean;
import com.singlee.financial.bean.SlSpshBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Map;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/spshServerExporter")
public interface ISpshServer {
	/**
	 * 根据条件查询SPSH表数据
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlSpshBean getSpshBean(Map<String, Object> map) throws  Exception;
	/**
	 * 资金往来类业务校验（首期结算金额，到期结算金额）,查询SPSH,DLDT,PMTQ表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlRprhDldtBean getRprhDldtBean(Map<String, Object> map) throws  Exception;
}
