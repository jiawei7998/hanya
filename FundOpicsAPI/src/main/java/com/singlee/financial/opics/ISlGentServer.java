package com.singlee.financial.opics;

import java.util.Map;

import com.singlee.financial.bean.SlGentBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS接口账务匹配规则表
 * 
 * @author shenzl
 *
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/slGentServerExporter")
public interface ISlGentServer {

	/**
	 * 添加opics 库中的SL_GENT表
	 * 
	 * @param gentBean
	 * @return
	 */
	SlOutBean addSlGent(SlGentBean gentBean);

	/**
	 * 添加opics 库中的SL_GENT表
	 * 
	 * @param gentBean
	 * @return
	 */
	SlOutBean updateSlGent(SlGentBean gentBean);

	/**
	 * 删除opics 库中的SL_GENT表的某一条数据
	 * 
	 * @param gentBean
	 * @return
	 */
	void deleteSlGent(SlGentBean gentBean);

	/**
	 * 添加opics 库中的SL_GENT表 根据TABLEID+SECID查询，主要用于判断
	 * 
	 * @param gentBean
	 * @return
	 */
	SlGentBean getSlGent(SlGentBean gentBean);
	
	/**
	 * 分页查询
	 */
	PageInfo<SlGentBean> searchPageGent(Map<String,Object> map);
	
	/**
	 * 修改时保存
	 */
	void updateById(SlGentBean gentBean);
	
	/**
	 * 根据主键查询有无此数据
	 */
	SlGentBean searchIfsOpicsGent(Map<String,String> map);
}