package com.singlee.financial.opics;


import com.singlee.financial.bean.SlIntrBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;


/**
 * OPICS系统INTR表
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/intrServerExporter")
public interface IIntrServer {


     /**
      * 插入OPICS系统INTR表
      * 
      * @param securityLendingBean
      * @return
      */
     SlOutBean insertOpicsIntr(SlIntrBean slIntrBean) throws  Exception;


}