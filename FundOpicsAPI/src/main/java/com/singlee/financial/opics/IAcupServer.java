package com.singlee.financial.opics;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.remoting.RemoteConnectFailureException;

import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlAcupErrBean;
import com.singlee.financial.bean.SlAcupRecord;
import com.singlee.financial.bean.SlAcupTaxBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlOutSonBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS系统日终账务处理
 * 
 * 1.该类属于公共方法,主要适用于OPICS相关处理方式
 * 
 * 2.该类不涉及任何接口成分,modify by 20180709
 * 
 * @author shenzl
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/acupServerExporter")
public interface IAcupServer {

	/**
	 * 1.调用总账过程
	 * 
	 * @param date
	 *            当前账务日期
	 * @return
	 */
	SlOutBean callAcupProc(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	/**
	 * 2.检查总账状态
	 * 
	 * @return
	 */
	SlOutBean checkSendFlag() throws RemoteConnectFailureException, Exception;

	/**
	 * 3.查询账务回执信息
	 * 
	 * @param map
	 * @return
	 */
	PageInfo<SlAcupBean> getAcupList(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	/**
	 * 3. 单笔查询
	 * 
	 * @param map
	 * @return
	 */
	List<SlAcupBean> getAcupById(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	/**
	 * 4.查询账务回执信息
	 * 
	 * 主要用于错账检查
	 * 
	 * @param map
	 * @return
	 */
	PageInfo<SlAcupErrBean> getAcupErrList(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	/**
	 * 4.单笔查询
	 * 
	 * 主要用于错账检查
	 * 
	 * @param map
	 * @return
	 */
	List<SlAcupErrBean> getAcupErrById(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	/**
	 * 总账日期回显
	 * 
	 * @param params
	 * @return
	 * */
	String getAcupDate(Map<String, String> params) throws RemoteConnectFailureException, Exception;

	/**
	 * 总账查询
	 */
	PageInfo<SlAcupBean> getLedgerList(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	/**
	 * 按条件查询总账(返回list)
	 */
	List<SlAcupBean> getAllData(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	/**
	 * 查询opics br
	 * 
	 * @param map
	 * @return
	 * */
	List<Map<String, String>> getOpicsBr(Map<String, String> map);

	List<SlAcupRecord> queryOperaRec(Map<String, String> map);

	SlOutSonBean sendValid(Map<String, String> map);

	/**
	 * 泰隆生成总账数据
	 */
	SlOutBean createAcupProc(Map<String, String> map);

	/**
	 * 检查总账状态(泰隆)
	 */
	SlOutBean checkSendFlag(String date);

	/**
	 * 查询估值
	 */
	BigDecimal queryValue(Map<String, String> map);

	/**
	 * 查询汇率
	 */
	BigDecimal queryRate(Map<String, String> map);
	
	/**
	 * 查询汇率计算方式
	 */
	String queryRateCCType(String br,String ccy,String ccybTerms);

	/**
	 * 查询债券市场价格
	 */
	BigDecimal queryPrice(Map<String, String> map);

	/**
	 * 查询债券上次付息日
	 */
	Date queryDate(Map<String, Object> map);

	/**
	 * 查询价格（价格异常检测偏离度使用）
	 */
	BigDecimal queryRhisIntrate(Map<String, Object> map);

	BigDecimal querySeclPrice(Map<String, Object> map);

	BigDecimal querySeclRate(Map<String, Object> map);

	/**
	 * 按条件查询历史总账(返回list)
	 */
	List<SlAcupBean> getAllHistoryData(Map<String, String> map) throws RemoteConnectFailureException, Exception;

	BigDecimal queryFxdSum(Map<String, Object> map) throws RemoteConnectFailureException, Exception;

	int checkHldy(Map<String, Object> map);

	/**
	 * 总账更新
	 * 
	 * @param AcupSendBean
	 * @return
	 * */
	String updateAcup(AcupSendBean acupTempBean) throws RemoteConnectFailureException, Exception;
	
	/**
	 * 调用OPICS 自动BSYS批量
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	String callOpicsBsys() throws RemoteConnectFailureException, Exception;
	
	
	/***********************
	 * 华商银行
	 * 
	 ***************************/
	
	/**
	 * 华商生成总账数据
	 */
	SlOutBean createAcupProcCmbh(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	/**
	 * 检查总账发送状态
	 */
	SlOutBean checkSendFlagCmbh(Map<String, Object> map);
	/**
	   *   总账查询
	 * @param params
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlAcupTaxBean> queryAcupPage(Map<String, Object> params) throws RemoteConnectFailureException, Exception;
	/**
	 * 按条件查询总账(返回list)
	 */
	List<SlAcupTaxBean> queryAcupList(Map<String, String> map) throws RemoteConnectFailureException, Exception;
	/**
	   * 查询所有部门
	 * @return
	 */
	List<String> getAllOpicsBr();

	SlOutBean checkAcupSendInfo(Map<String, Object> map);
	
	/**
	 * 外汇异常账务列表查询
	 * @return
	 */
	PageInfo<SlAcupTaxBean> queryErrorAcupPage(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	List<SlAcupTaxBean> queryErrorAcuplist(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	/**
	 *	全天交易查询 
	 * @return
	 */
	PageInfo<SlAcupTaxBean> queryTodayPage(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	List<SlAcupTaxBean> queryTodaylist(Map<String, Object> map) throws RemoteConnectFailureException, Exception;

	Map<String, Object> callProfitLoss(Map<String, Object> map);

	Map<String, Object> callAcupProfitLoss(Map<String, Object> qMap);

}