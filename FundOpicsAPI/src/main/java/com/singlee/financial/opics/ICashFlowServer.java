package com.singlee.financial.opics;

import java.util.Map;

import com.singlee.financial.bean.SlCashFlowBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/cashFlowServerExporter")
public interface ICashFlowServer {
	
	/**
	 * 查询现金流列表信息
	 * 
	 * @param map
	 * @return
	 */
	PageInfo<SlCashFlowBean> getCashFlowList(Map<String, String> map);
}
