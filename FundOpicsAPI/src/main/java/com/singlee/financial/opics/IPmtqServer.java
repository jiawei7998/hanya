package com.singlee.financial.opics;

import com.singlee.financial.bean.*;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/cnapsServerExporter")
public interface IPmtqServer {

	/**
	 * 根据br和dealno获取PMTQ数据
	 * 
	 * @param queryMap
	 * @return
	 */
	List<SlPmtqBean> getPmtq(Map<String, Object> queryMap) throws  Exception;
	/**
	 * 根据发送标识查询SL_PMTQ数据
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlPmtqBean> getPmtqBySendflag(Map<String, Object> map) throws  Exception;
	
	/**
	 * SL_PMTQ 查询需要发送二代的支付信息  （哈尔滨）
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlPmtqBean> getPmtqByMap(Map<String, Object> map) throws  Exception;
	
	/**
	 * 国库定期存款,支小再贷款,中期借贷便利,常备借贷便利
	 * @param dealno
	 * @return
	 */
	SlPmtqAccBean selectRepoAmt(Map<String, String> map);

	/**
	 * 回购和债券除[国库定期存款,支小再贷款,中期借贷便利,常备借贷便利]
	 * @param map
	 * @return
	 */
	SlPmtqAccBean selectSecurAmt(Map<String, String> map);
	
	/**
	 * 修改SL_PMTQ的发送标识
	 * @param map
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	void updateSendflag(Map<String, Object> map) throws  Exception;
	/**
	   *   外币记账分页查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlPmtqFxBean> searchPagePmtqFx(Map<String,Object> map) throws  Exception;
	/**
	   *    外币记账查询
	 * @param param
	 * @return
	 */
	List<SlPmtqFxBean> getPmtqFxList(Map<String, Object> param);
	/**
	   *  外币记账新增
	 * @param entity
	 */
	void fxblAdd(SlPmtqFxBean entity);
	/**
	   *   外币记账修改
	 * @param entity
	 */
	void fxblEdit(SlPmtqFxBean entity);
	/**
	   *  外币记账删除
	 * @param entity
	 */
	void deleteFxbl(SlPmtqFxBean entity);
	/**
	   * 根据sn查询
	 */
	SlPmtqFxBean selectById(Map<String, Object> map);
	/**
	   *   经办复核处理
	 * @param sn
	 * @param oper
	 * @param date
	 * @param note
	 * @param verind
	 */
	void handleFxbl(String sn, String oper, Date date, String note, String verind);
	
	/**
	 * 账号名称
	 * @param psix
	 * @return
	 */
	List<PsixBean> selectPsix(PsixBean psix);
	
	/**
	 * 账号信息
	 * @param pcix
	 * @return
	 */
	List<PcixBean> selectPcix(PcixBean pcix);

	List<SlPmtqBean> selectBySetmeansAndProduct(String cdate);

	List<String> queryInthBydealno(String dealno);
}
