package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlFifoBean;
import com.singlee.financial.bean.SlSpshViewBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/IFifoServerExporter")
public interface IFifoServer {

	public void deleteFifoByBr(String br);
	
	public List<SlSpshViewBean> selectSpshViewListGroup(String br);
	
	public List<SlSpshViewBean> selectSpshViewListP(Map<String,Object> map);
	
	public SlSpshViewBean selectSpshViewS(Map<String,Object> map);

	public void bathInsertFifo(List<SlFifoBean> fbList);
	
	public List<SlFifoBean> getAllList(Map<String, Object> map);
}
