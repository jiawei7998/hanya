package com.singlee.financial.opics;

import com.singlee.financial.bean.BondinvestBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/**
 * 主要用于查询SL_REPORT_BONDINVEST（Bondinvest-债券投资业务表）数据
 * @author wangzhao
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/bondinvestServerExporter")
public interface IBondinvestServer {

	/**
	 * 带参数 分页查询 Bondinvest-债券投资业务表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<BondinvestBean> getBondinvestReport(Map<String, Object> map) throws  Exception;
	
	/**
	 * 带参数 列表查询 Bondinvest-债券投资业务表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<BondinvestBean> getBondinvestReportList(Map<String, Object> map) throws  Exception;
}
