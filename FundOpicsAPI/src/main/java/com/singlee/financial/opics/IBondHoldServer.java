package com.singlee.financial.opics;

import com.singlee.financial.bean.SlBondHoldBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/**
 * 债券报表
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/bondHoldServerExporter")
public interface IBondHoldServer {
	/**
	 * 债券持仓查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlBondHoldBean> searchBondHoldCount(Map<String, Object> map) throws  Exception;
	

	List<String> tposList(Map<String, Object> map);
}