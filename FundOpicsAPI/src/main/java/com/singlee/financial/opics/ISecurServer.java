package com.singlee.financial.opics;

import com.singlee.financial.bean.*;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;
import java.util.Map;

/**
 * OPICS现券模块
 * <p>
 * 现券买卖、现券借贷
 *
 * @author SINGLEE
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/securServerExporter")
public interface ISecurServer {

    /**
     * 将现券买卖,包括普通债券、ABS、国债等交易导入OPICS系统
     *
     * @param map
     * @return
     */
    @Deprecated
    SlOutBean bredProc(Map<String, String> map) throws Exception;

    /**
     * 将债券借贷交易导入OPICS系统
     *
     * @param map
     * @return
     */
    @Deprecated
    SlOutBean isldProc(Map<String, String> map) throws Exception;

    /**
     * 校验OPICS债券买卖
     *
     * @param fixedIncomeBean
     * @return
     * @author shenzl
     * @since JDK 1.6
     */
    SlOutBean verifiedFi(SlFixedIncomeBean fixedIncomeBean) throws Exception;

    /**
     * 债券买卖导入
     *
     * @param fixedIncomeBean
     * @return
     */
    SlOutBean fi(SlFixedIncomeBean fixedIncomeBean) throws Exception;

    /**
     * 债券买卖冲销
     *
     * @param fixedIncomeBean
     * @return
     */
    SlOutBean fiRev(SlFixedIncomeBean fixedIncomeBean) throws Exception;

    /**
     * 校验OPICS现券买卖冲销
     */
    SlOutBean verifiedFiRev(SlFixedIncomeBean fixedIncomeBean) throws Exception;

    /**
     * 债券借贷导入
     *
     * @param securityLendingBean
     * @return
     */
    SlOutBean seclend(SlSecurityLendingBean securityLendingBean) throws Exception;

    /**
     * 债券借贷冲销
     *
     * @param securityLendingBean
     * @return
     */
    SlOutBean seclendRev(SlSecurityLendingBean securityLendingBean) throws Exception;

    /**
     * 校验OPICS债券借贷冲销
     *
     * @param securityLendingBean
     * @return
     */
    SlOutBean verifiedSeclendRev(SlSecurityLendingBean securityLendingBean) throws Exception;

    /**
     * MBS债券买卖导入
     *
     * @param imbdBean
     * @return
     */
    SlOutBean imbd(SlImbdBean imbdBean) throws Exception;

    /**
     * 债券模块
     *
     * @param
     * @return
     */
    SlOutBean rdfe() throws Exception;
    /**
     * 获取当前需要还本收息操作记录
     *
     * @param map
     * @return
     */
    PageInfo<SlSecmRedemption> getRedemption(Map<String, Object> map) throws Exception;

    List<SlSecmRedemption> getRedemptdWithList(Map<String, Object> map) throws Exception;

    PageInfo<SlSecmRevaluation> getPageSecmRevaluation(Map<String, Object> map) throws Exception;

    /**
     * 查询secm 单条数据
     */
    SlSecmBean getSlSecmOne(String secid) throws Exception;

}
