package com.singlee.financial.opics;

import com.singlee.financial.bean.SlFxCashFlowBean;
import com.singlee.financial.bean.SlFxdhBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/fxdhServerExporter")
public interface IFxdhServer {
	/**
	 * 根据条件查询FXDH表数据
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlFxdhBean getFxdhBean(Map<String, Object> map) throws  Exception;
	/**
	 * 查询外汇现金流
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlFxCashFlowBean> searchFxCashFlowPage(Map<String, Object> map) throws  Exception;
	/**
	 * 现金流中查询交易明细
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlFxdhBean> getFxdhList(Map<String, Object> map) throws  Exception;
}
