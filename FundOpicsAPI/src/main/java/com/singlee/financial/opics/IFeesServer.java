/**
 * Project Name:FundOpicsAPI
 * File Name:IFeesServer.java
 * Package Name:com.singlee.financial.opics
 * Date:2018-10-17下午02:21:39
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.opics;

import com.singlee.financial.bean.SlIfeeBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * ClassName:IFeesServer <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-10-17 下午02:21:39 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/feesServerExporter")
public interface IFeesServer {
	
	/**
	 * 费用导入
	 * 
	 * @param ifeeBean
	 * @return
	 */
	SlOutBean fees(SlIfeeBean ifeeBean) throws  Exception;
}

