package com.singlee.financial.opics;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlQuotedPrice;
import com.singlee.financial.bean.TRtrate;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.Map;

/**
 * OPICS系统外汇牌价查询
 * 
 * 1.该类属于公共方法,主要适用于OPICS相关处理方式
 * 
 * 2.该类不涉及任何接口成分,modify by 20190304
 * 
 * @author zhangcm
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/quotedPriceServerExporter")
public interface IQuotedPriceServer {
	/**
	 * 1.查询牌价信息
	 * 
	 * @param map
	 * @return
	 */
	PageInfo<SlQuotedPrice> getQuotedPriceList(Map<String, Object> map) throws  Exception;
	
	SlOutBean quotedPriceEvent();
	
	SlOutBean impRmdata(TRtrate tRtrate);
}
