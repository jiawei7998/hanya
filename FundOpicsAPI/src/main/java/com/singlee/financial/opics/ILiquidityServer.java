package com.singlee.financial.opics;

import com.singlee.financial.bean.LiquidityBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/**
 * 主要用于查询SL_REPORT_LIQUIDITY（带参数查询 Liquidity-表一 报表数据）数据
 * @author wangzhao
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/ILiquidityServerExporter")
public interface ILiquidityServer {

	/**
	 * 带参数 分页查询 Liquidity-表一 报表数据
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<LiquidityBean> getLiquidityReport(Map<String, Object> map) throws  Exception;
	
	/**
	 * 带参数 列表查询 Liquidity-表一 报表数据
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<LiquidityBean> getLiquidityListReport(Map<String, Object> map) throws  Exception;
	
}
