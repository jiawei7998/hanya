package com.singlee.financial.opics;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;
import java.util.Map;

/**
 * OPICS结构衍生
 * 
 * @author xuqq
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/struckServerExporter")
public interface IStruckServer {

	/**
	 * 鏍￠獙OPICS鏈熸潈
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean verifiedStruck(Map<String,List<Object>> map) throws  Exception;

	/**
	 * 鏈熸潈瀵煎叆
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean struck(Map<String,List<Object>> map) throws  Exception;

	/**
	 * 鏈熸潈鍐查攢
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean struckRev(Map<String,List<Object>> map) throws  Exception;

	/**
	 * 鏍￠獙鏈熸潈鍐查攢
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlOutBean verifiedStruckRev(Map<String,List<Object>> map) throws  Exception;
}
