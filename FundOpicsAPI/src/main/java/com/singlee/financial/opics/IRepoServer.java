package com.singlee.financial.opics;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlRepoCrBean;
import com.singlee.financial.bean.SlRepoOrBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.Map;

/**
 * OPICS回购模块
 * 
 * 主要处理债券回购
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/repoServerExporter")
public interface IRepoServer {

	/**
	 * 将回购交易导入OPICS系统
	 * 
	 * @param map
	 * @return
	 */
	@Deprecated
	SlOutBean ircaProc(Map<String, String> map) throws  Exception;

	/**
	 * 校验OPICS质押式回购
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedRepoCr(SlRepoCrBean crBean) throws  Exception;

	/**
	 * 质押式回购导入
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean repoCr(SlRepoCrBean crBean) throws  Exception;

	/**
	 * 质押式回购冲销
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean repoCrRev(SlRepoCrBean crBean) throws  Exception;

	/**
	 * 校验OPICS买断式回购
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedRepoOr(SlRepoOrBean orBean) throws  Exception;

	/**
	 * 买断式回购导入
	 * 
	 * @param orBean
	 * @return
	 */
	SlOutBean repoOr(SlRepoOrBean orBean) throws  Exception;

	/**
	 * 买断式回购冲销
	 * 
	 * @param orBean
	 * @return
	 */
	SlOutBean repoOrRev(SlRepoOrBean orBean) throws  Exception;

	/**
	 * 校验回购类冲销
	 * 
	 * @param orBean
	 * @return
	 */
	SlOutBean verifiedRepoOrRev(SlRepoOrBean orBean) throws  Exception;

}
