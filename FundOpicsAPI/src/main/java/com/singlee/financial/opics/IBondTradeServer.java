package com.singlee.financial.opics;

import com.singlee.financial.bean.SlBondTradeBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Map;

/**
 * 债券报表
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/bondTradeServerExporter")
public interface IBondTradeServer {
	/**
	 * 债券交易查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlBondTradeBean> searchBondTradeCount(Map<String, Object> map) throws  Exception;
	
	
}