package com.singlee.financial.opics;

import java.util.Map;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;



/**
 * i9回流文件读写
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/refluxFileExporter")
public interface IRS9RefluxFileService {
	
	public SlOutBean readIrs9File(Map<String, Object> map) ;
	

}
