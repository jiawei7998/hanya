package com.singlee.financial.opics;

import com.singlee.financial.bean.SlDepositsAndLoansBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Map;

/**
 * OPICS拆借模块
 * 
 * 主要处理本币拆借
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/mmServerExporter")
public interface IMmServer {
	/**
	 * 将拆借交易导入OPICS系统
	 * 
	 * @param map
	 * @return
	 */
	@Deprecated
	SlOutBean idldProc(Map<String, String> map) throws  Exception;

	/**
	 * 校验OPICS信用拆借
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedMm(SlDepositsAndLoansBean depositsAndLoansBean) throws  Exception;

	/**
	 * 信用拆借导入
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean mm(SlDepositsAndLoansBean depositsAndLoansBean) throws  Exception;
	
	/**
	 * 提前还款
	 * @param depositsAndLoansBean
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlOutBean mmAdv(SlDepositsAndLoansBean depositsAndLoansBean) throws  Exception;
	
	/**
	 * 信用拆借冲销
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean mmRev(SlDepositsAndLoansBean depositsAndLoansBean) throws  Exception;

	/**
	 * 校验信用拆借冲销
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean verifiedMmRev(SlDepositsAndLoansBean depositsAndLoansBean) throws  Exception;

}
