package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlOpcNssbBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/IOpcNssbServerExporter")
public interface IOpcNssbServer {

	/**
	 * 纳税申报申请取数
	 * 
	 * @param map
	 * @return
	 */
	public List<SlOpcNssbBean> selectOpcNssbPayTaxes(Map<String, Object> map);

	/**
	 * 根据转让债权计算表
	 * 
	 * @param map
	 * @return
	 */
	public List<SlOpcNssbBean> selectOpcNssbCreditorRight(Map<String, Object> map);
}
