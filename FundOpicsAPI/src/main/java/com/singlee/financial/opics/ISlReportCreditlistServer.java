package com.singlee.financial.opics;

import com.singlee.financial.bean.SlReportCreditlist;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/***
 * 同业额度明细报表
 * @author LIJ
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/slReportCreditlistServerExporter")
public interface ISlReportCreditlistServer {
	
	/**
	 * 分页查询-同业额度明细报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlReportCreditlist> getPageCreditlistReport(Map<String, Object> map) throws  Exception;
	
	/**
	 * 列表查询-同业额度明细报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlReportCreditlist> getListCreditlistReport(Map<String, Object> map) throws  Exception;

}
