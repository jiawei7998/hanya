/**
 * Project Name:FundOpicsApi
 * File Name:IBaseServer.java
 * Package Name:com.singlee.financial.opics
 * Date:2018-6-7下午05:26:00
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
 */

package com.singlee.financial.opics;

import com.singlee.financial.bean.*;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * ClassName:IBaseServer <br/>
 * Date: 2018-6-7 下午05:26:00 <br/>
 * 
 * @author shenzl
 * @version
 * @since JDK 1.6
 * @see
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/baseServeExporter")
public interface IBaseServer {

	/**
	 * 查询INTH表处理状态
	 * 
	 * @param baseBean
	 */
	SlInthBean getInthStatus(SlInthBean inthBean) throws  Exception;

	/**
	 * 
	 * getInthStatusList:查询INTH表处理状态
	 * 
	 * @param inthBean
	 * @return
	 * @since JDK 1.6
	 */
	List<SlInthBean> getInthStatusList(SlInthBean inthBean) throws  Exception;

	/**
	 * 根据br获取brps表中的系统日期
	 * 
	 * @param br
	 */
	
	Date getOpicsSysDate(String br) throws  Exception;
	/**
	 * 根据br获取brps表中的上一跑批日期(prevbranprcdate)
	 * 
	 * @param br
	 */
	
	Date getOpicsPrevbranprcDate(String br) throws  Exception;

	/**
	 * 根据br获取brps表中的下一跑批日期(nextbranprcdate)
	 * @param br
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	Date getOpicsNextbranprcdate(String br) throws  Exception;

	/**
	 * 判断债券是否短头寸
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	SlSposBean getSpos(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询获取DV01，久期限，止损值等数据
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlCommonBean queryMarketData(Map<String,Object> map) throws  Exception;
	/**
	 * 查询债券持仓余额
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	PageInfo<SlBondPositionBalanceBean> getBondPositionBalance(Map<String, Object> map) throws  Exception;
	/**
	 * 查询债券盈亏统计
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	List<SlSdCdProfitAndLossBean> getSdProfitAndLoss(Map<String, Object> map) throws  Exception;
	/**
	 * 查询存单盈亏统计
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	List<SlSdCdProfitAndLossBean> getCdProfitAndLoss(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询债券投资历史
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	PageInfo<SlSdCdInvestHistoryBean> getSdInvestHistory(Map<String, Object> map) throws  Exception;
	/**
	 * 查询存单投资历史
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	PageInfo<SlSdCdInvestHistoryBean> getCdInvestHistory(Map<String, Object> map) throws  Exception;
	/**
	 * 调用债券投资历史存储过程
	 * 
	 */
	SlOutBean callSdInvestHistory(Map<String, String> map) throws  Exception;
	/**
	 * 调用存单投资历史存储过程
	 * 
	 */
	SlOutBean callCdInvestHistory(Map<String, String> map) throws  Exception;
	/**
	 * 调用债券损益存储过程
	 * 
	 */
	SlOutBean callSdProfitLoss(Map<String, String> map) throws  Exception;
	/**
	 * 调用存单损益存储过程
	 * 
	 */
	SlOutBean callCdProfitLoss(Map<String, String> map) throws  Exception;
	/**
	 * 同业存单发行每日余额清单
	 */
	PageInfo<SlCdIssueBalanceBean> getCdIssueBalance(Map<String, Object> map) throws  Exception;
	
	
	/**
	 * 查询债券持仓余额导出
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	List<SlBondPositionBalanceBean> getBondPositionBalanceExport(Map<String, Object> map) throws  Exception;
	/**
	 * 查询债券投资历史导出
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	List<SlSdCdInvestHistoryBean> getSdInvestHistoryExport(Map<String, Object> map) throws  Exception;
	/**
	 * 查询存单投资历史导出
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 * @since JDK 1.6
	 */
	
	List<SlSdCdInvestHistoryBean> getCdInvestHistoryExport(Map<String, Object> map) throws  Exception;
	/**
	 * 同业存单发行每日余额清单导出
	 */
	List<SlCdIssueBalanceBean> getCdIssueBalanceExport(Map<String, Object> map) throws  Exception;
	
	/***
	 * 获取OPICS客户列表
	 * @param map
	 * @return
	 */
	List<SlCustBean> getListOpicsCust(Map<String, Object> map)throws  Exception;
	
	/***
	 * 根据BR  CCY1  CCY2获取 TERMS(乘除关系)
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<String> getTermsFromCcyp(Map<String, Object> map)throws  Exception;

	String getProfitLoss(Map<String, Object> map) throws  Exception;

	SlOutBean checkProfitLossH() throws RemoteConnectFailureException, Exception;

	SlOutBean callProfitLossInit(Map<String, Object> map) throws RemoteConnectFailureException, Exception;

	SlOutBean callProfitLossH(Map<String, Object> map) throws RemoteConnectFailureException, Exception;
	
	
	
	
	
	
	
	
	
}