package com.singlee.financial.opics;

import com.singlee.financial.bean.SlIrs9Bean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Map;

/**
 * 回流文件报表
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/refluxFileServerExporter")
public interface IRefluxFileServer {
	/**
	 * 债券持仓查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlIrs9Bean> searchRefluxFileCount(Map<String, Object> map) throws  Exception;
	
	
}