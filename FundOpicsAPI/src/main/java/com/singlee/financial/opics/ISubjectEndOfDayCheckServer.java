package com.singlee.financial.opics;

import com.singlee.financial.bean.SlAcupBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;
import java.util.Map;

/**
 * 主要用于查询sl_acup   科目余额
 * @author copysun
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/SubjectEndOfDayCheckServerExporter")
public interface ISubjectEndOfDayCheckServer {

	/**
	 * 查询科目号余额
	 * @param map
	 * @return
	 */
	List<SlAcupBean> getList(Map<String,Object> map) throws  Exception ;
//	/**
//	 * 获取  科目余额
//	 * @param map
//	 * @return
//	 * @throws RemoteConnectFailureException
//	 * @throws Exception
//	 */
//	List<String> getA1403ReportAllBr() throws  Exception;

}
