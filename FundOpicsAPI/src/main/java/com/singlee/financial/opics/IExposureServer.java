
package com.singlee.financial.opics;

import com.singlee.financial.bean.SlBondCashBean;
import com.singlee.financial.bean.SlExposureBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Map;

/**
 * 敞口查询
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/exposureServerExporter")
public interface IExposureServer {
	/**
	 * 即期外汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxExposure(Map<String, Object> map) throws  Exception;
	/**
	 * 历史即期外汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxExposureHistory(Map<String, Object> map) throws  Exception;
	/**
	 * 即期结售汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxSettleExposure(Map<String, Object> map) throws  Exception;
	/**
	 * 历史即期结售汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxSettleExposureHistory(Map<String, Object> map) throws  Exception;
	
	/**
	 * 债券头寸查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlBondCashBean> searchBondCashCount(Map<String, Object> map) throws  Exception;
	
	/**
	 * 远期外汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxForwardExposure(Map<String, Object> map) throws  Exception;
	/**
	 * 远期结售汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxForwardSettleExposure(Map<String, Object> map) throws  Exception;
	
	
	/**
	 * 掉期外汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxSwapExposure(Map<String, Object> map) throws  Exception;
	
	/**
	 * 掉期结售汇敞口查询
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlExposureBean> getFxSwapSettleExposure(Map<String, Object> map) throws  Exception;
	
	
}