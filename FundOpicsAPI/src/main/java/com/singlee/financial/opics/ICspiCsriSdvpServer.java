package com.singlee.financial.opics;

import com.singlee.financial.bean.SlCspiCsriBean;
import com.singlee.financial.bean.SlCspiCsriDetailBean;
import com.singlee.financial.bean.SlSdvpBean;
import com.singlee.financial.bean.SlSdvpDetailBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;
import java.util.Map;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/cspiCsriSdvpServer")
public interface ICspiCsriSdvpServer {

	/**
	 * 检查收付款标识
	 * 
	 * @param cspiCsriBean
	 * @return
	 */
	public List<SlCspiCsriBean> getCspiCsri(SlCspiCsriBean cspiCsriBean);

	/**
	 * 检查是否SDVP路径
	 * 
	 * @param sdvpBean
	 * @return
	 */
	public List<SlSdvpBean> getSdvp(SlSdvpBean sdvpBean);
	
	/**
	 * 查询付款cspicsri清算路径
	 */
	SlCspiCsriBean searchpaycspicsrihead(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询收款cspicsri清算路径
	 */
	SlCspiCsriBean searchreccspicsrihead(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询cspicsri清算路径明细
	 */
	List<SlCspiCsriDetailBean> searchcspicsridetail(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询付款sdvp清算路径
	 */
	SlSdvpBean searchpaysdvphead(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询收款sdvp清算路径
	 */
	SlSdvpBean searchrecsdvphead(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询sdvp清算路径明细
	 */
	List<SlSdvpDetailBean> searchsdvpdetail(Map<String, Object> map) throws  Exception;
	
	/**
	 * 查询csri清算路径明细
	 */
	List<SlCspiCsriDetailBean> searchcsridetail(Map<String, Object> map) throws  Exception;
}