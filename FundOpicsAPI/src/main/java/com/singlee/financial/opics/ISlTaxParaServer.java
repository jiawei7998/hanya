package com.singlee.financial.opics;

import java.util.Map;

import com.singlee.financial.bean.SlTaxParaBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
   *    营改增参数维护
 * 
 * @author shenzl
 *
 */

@Opics(context = OpicsContext.SINGLEE_API, uri = "/slTaxParaServerExporter")
public interface ISlTaxParaServer {

	/**
	 * 添加opics 库中的SL_TAX_PARA表
	 * 
	 * @param taxParaBean
	 * @return
	 */
	SlOutBean addSlTaxPara(SlTaxParaBean taxParaBean);

	/**
	 * 添加opics 库中的SL_TAX_PARA表
	 * 
	 * @param taxParaBean
	 * @return
	 */
	SlOutBean updateSlTaxPara(SlTaxParaBean taxParaBean);

	/**
	 * 删除opics 库中的SL_TAX_PARA表的某一条数据
	 * 
	 * @param taxParaBean
	 * @return
	 */
	SlOutBean deleteSlTaxPara(SlTaxParaBean taxParaBean);

	/**
	   *  查询
	 * 
	 * @param map
	 * @return
	 */
	SlTaxParaBean getSlTaxPara(Map<String, String> map);
	
	/**
	 * 分页查询
	 */
	PageInfo<SlTaxParaBean> searchPageTaxPara(Map<String,Object> map);
	
	

}