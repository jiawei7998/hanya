package com.singlee.financial.opics;

import com.singlee.financial.bean.SlReportZjgyjz;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;
/***
 * 资金交易公允价值监测报表
 * @author LIJ
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/slReportZjgyjzServerExporter")
public interface ISlReportZjgyjzServer {
	
	/**
	 * 分页查询-资金交易公允价值监测报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlReportZjgyjz> getPageZjgyjzReport(Map<String, Object> map) throws  Exception;
	
	/**
	 * 列表查询-资金交易公允价值监测报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlReportZjgyjz> getListZjgyjzReport(Map<String, Object> map) throws  Exception;

}
