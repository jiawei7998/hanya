package com.singlee.financial.opics;

import com.singlee.financial.bean.SlFxSptFwdBean;
import com.singlee.financial.bean.SlFxSwapBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.Date;
import java.util.Map;

/**
 * OPICS系统外汇模块服务接口。
 * 
 * 目前支持外汇即期、远期、掉期接口的OPICS入库操作
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/fxdServerExporter")
public interface IFxdServer {

	/**
	 * CFEST外汇即、远期处理过程
	 * 
	 * @param map
	 * @return
	 */
	@Deprecated
	SlOutBean fxSptAndFwdProc(Map<String, String> map) throws  Exception;

	/**
	 * CFEST掉期处理过程
	 * 
	 * @param map
	 * @return
	 */
	@Deprecated
	SlOutBean fxSwapProc(Map<String, String> map) throws  Exception;

	/**
	 * 校验OPICS外汇即期/远期
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedfxSptFwd(SlFxSptFwdBean fxSptFwdBean) throws  Exception;

	/**
	 * 外汇即期/远期导入
	 * 
	 * @param fxSptFwdBean
	 * @return
	 */
	SlOutBean fxSptFwd(SlFxSptFwdBean fxSptFwdBean) throws  Exception;

	/**
	 * 外汇即期/远期冲销
	 * 
	 * @param fxSptFwdBean
	 * @return
	 */
	SlOutBean fxSptFwdRev(SlFxSptFwdBean fxSptFwdBean) throws  Exception;

	/**
	 * 校验外汇即期/远期/掉期冲销
	 * 
	 * @param fxSptFwdBean
	 * @return
	 */
	SlOutBean verifiedFxSptFwdRev(SlFxSptFwdBean fxSptFwdBean) throws  Exception;

	/**
	 * 校验OPICS外汇掉期
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedfxSwap(SlFxSwapBean fxSwapBean) throws  Exception;

	/**
	 * 外汇掉期导入
	 * 
	 * @param fxSptFwdBean
	 * @return
	 */
	SlOutBean fxSwap(SlFxSwapBean fxSwapBean) throws  Exception;

	/**
	 * 外汇掉期冲销
	 * 
	 * @param fxSptFwdBean
	 * @return
	 */
	SlOutBean fxSwapRev(SlFxSwapBean fxSwapBean) throws  Exception;
	
	/**
	 * 判断即期、远期期限
	 * @param dealdate
	 * @param vdate
	 * @param ccy
	 * @param ctrccy
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlOutBean fxdCheckedTenor(Date dealdate,Date vdate,String ccy,String ctrccy) throws  Exception;
	
	/**
	 * 根据fedealno查询inth表中的dealno
	 */
	String queryDealno(Map<String,Object> map) throws  Exception;
	
	/**
	 * 根据近端dealno查询远端dealno
	 * @param nearDealno
	 * @return
	 */
	String getFxdhSwapDeal(String nearDealno) throws  Exception;
	
	public String fxdCheckedTdate(String ccy1, String ccy2)	throws RemoteConnectFailureException, Exception;
}