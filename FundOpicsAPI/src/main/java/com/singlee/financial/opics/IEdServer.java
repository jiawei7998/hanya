package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlAcupBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/EdServerExporter")
public interface IEdServer {

	/**
	 * 根据币种查询汇率
	 */
	String queryRate(Map<String,String> map);
	
	/**
	 * 查询估值
	 */
	List<SlAcupBean> queryValue(Map<String,String> map);
}