package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlOpcCzsyBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/IOpcCzsyServerExporter")
public interface IOpcCzsyServer {

	public List<SlOpcCzsyBean> selectOpcCzsy(Map<String, Object> map);
	
	/**
	 * 获取科目号
	 * @param map
	 * @return
	 */
	public List<SlOpcCzsyBean> selectGlno(Map<String, Object> map);
}
