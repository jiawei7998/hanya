package com.singlee.financial.opics;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSwapCrsBean;
import com.singlee.financial.bean.SlSwapCurBean;
import com.singlee.financial.bean.SlSwapIrsBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;
import java.util.Map;

/**
 * OPICS掉期交易接口
 * 
 * 主要处理外汇掉期、利率掉期、货币掉期
 * 
 * @author SINGLEE
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/swapServerExporter")
public interface ISwapServer {

	/**
	 * 将利率互换交易导入到OPICS系统
	 * 
	 * @param map
	 * @return
	 */
	@Deprecated
	SlOutBean irsSwapProc(Map<String, String> map) throws  Exception;

	/**
	 * 将货币互换交易导入到OPICS系统
	 * 
	 * @param map
	 * @return
	 */
	@Deprecated
	SlOutBean crsSwapProc(Map<String, String> map) throws  Exception;

	/**
	 * 校验OPICS货币互换
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedSwapCrs(SlSwapCrsBean crsBean) throws  Exception;

	/**
	 * 货币互换导入
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean swapCrs(SlSwapCrsBean crsBean) throws  Exception;

	/**
	 * 货币互换冲销
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean swapCrsRev(SlSwapCrsBean crsBean) throws  Exception;

	/**
	 * 校验OPICS货币互换
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedSwapIrs(SlSwapIrsBean irsBean) throws  Exception;

	/**
	 * 利率互换导入
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean swapIrs(SlSwapIrsBean irsBean) throws  Exception;

	/**
	 * 利率互换冲销
	 * 
	 * @param mmBean
	 * @return
	 */
	SlOutBean swapIrsRev(SlSwapIrsBean irsBean) throws  Exception;

	/**
	 * 查询INTH表处理状态
	 * 
	 * @param baseBean
	 */
	SlOutBean getInthStatus(TradeBaseBean baseBean) throws  Exception;
	/**
	 * 货币互换头寸敞口查询
	 * @param map
	 * @return
	 */
	PageInfo<SlSwapCurBean> getSwapCrsExpose(Map<String, Object> map)throws  Exception;
	
	List<SlSwapCurBean> getSwapCrsCount(Map<String, Object> map)throws  Exception;
}
