package com.singlee.financial.opics;

import com.singlee.financial.bean.*;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/**
 * 提供直接操作OPICS基础参数
 * 
 * @author shenzl
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/staticServerExporter")
public interface IStaticServer {

	/**
	 * 插入OPICS产品表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addProd(SlProdBean prodBean) throws  Exception;

	/**
	 * 插入OPICS产品类型表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addProdType(SlProdTypeBean prodTypeBean) throws  Exception;

	/**
	 * 插入OPICS成本中心表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addCost(SlCostBean costBean) throws  Exception;

	/**
	 * 插入OPICS投资组合表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addPort(SlPortBean portBean) throws  Exception;

	/**
	 * 插入OPICS国家代码表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addCoun(SlCounBean counBean) throws  Exception;

	/**
	 * 插入OPICS银行唯一SWIFT码表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addBico(SlBicoBean bicoBean) throws  Exception;

	/**
	 * 插入OPICS行业代码表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addSico(SlSicoBean sicoBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。
	 * 
	 * @param prodBean
	 * @return
	 */
	SlOutBean addActy(SlActyBean actyBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。
	 * 
	 * @param setaBean
	 * @return
	 */
	SlOutBean addSeta(SlSetaBean setaBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。
	 * 
	 * @param nostBean
	 * @return
	 */
	SlOutBean addNost(SlNostBean nostBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。 清算路径
	 * 
	 * @param setmBean
	 * @return
	 */
	SlOutBean addSetm(SlSetmBean setmBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。 OPICS的债券的托管账户
	 * 
	 * @param saccBean
	 * @return
	 */
	SlOutBean addSacc(SlSaccBean saccBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。 OPICS的利率代码
	 * 
	 * @param rateBean
	 * @return
	 */
	SlOutBean addRate(SlRateBean rateBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。 OPICS的利率曲线
	 * 
	 * @param ychdBean
	 * @return
	 */
	SlOutBean addYchd(SlYchdBean ychdBean) throws  Exception;

	/**
	 * 插入OPICS表中 需逐步判断是否已经存在。 OPICS的节假日
	 * 
	 * @param hldyBean
	 * @return
	 */
	SlOutBean addHldy(SlHldyBean hldyBean) throws  Exception;

	/**
	 * 批量查询OPICS 节假日
	 * 
	 * @return
	 */
	List<SlHldyBean> synchroHldy() throws  Exception;

	/**
	 * 批量查询OPICS 利率代码
	 * 
	 * @return
	 */
	List<SlRateBean> synchroRate() throws  Exception;

	/**
	 * 批量查询OPICS 利率曲线
	 * 
	 * @return
	 */
	List<SlYchdBean> synchroYchd() throws  Exception;

	/**
	 * 批量查询OPICS 清算路径
	 * 
	 * @return
	 */
	List<SlSetmBean> synchroSetm() throws  Exception;

	/**
	 * 批量查询OPICS OPICS的债券的托管账户
	 * 
	 * @return
	 */
	List<SlSaccBean> synchroSacc() throws  Exception;

	/**
	 * 批量查询OPICS 清算
	 * 
	 * @return
	 */
	List<SlSetaBean> synchroSeta() throws  Exception;

	/**
	 * 批量查询OPICS 清算
	 * 
	 * @return
	 */
	List<SlNostBean> synchroNost() throws  Exception;
	/**
	   *    分页查询NOST
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlNostBean> searchPageNos(Map<String, Object> map) throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlProdBean> synchroProd() throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlProdTypeBean> synchroProdType() throws  Exception;
	/**
	 * 通过产品代码查询产品类型
	 * 
	 * @return
	 */
	List<SlProdTypeBean> synchroProdTypeByProd(Map<String, Object> map) throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlCostBean> synchroCost() throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlPortBean> synchroPort() throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlCounBean> synchroCoun() throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlBicoBean> synchroBico() throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlSicoBean> synchroSico() throws  Exception;

	/**
	 * 批量查询OPICS 产品信息用于日终同步
	 * 
	 * @return
	 */
	List<SlActyBean> synchroActy() throws  Exception;
}
