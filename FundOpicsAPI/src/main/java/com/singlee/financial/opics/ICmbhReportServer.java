package com.singlee.financial.opics;

import com.singlee.financial.bean.*;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.List;
import java.util.Map;

/**
 *	华商报表 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/cmbhReportServerExceport")
public interface ICmbhReportServer {

	//G14  分页查询
	PageInfo<SlReportG14Bean> getReportG14List(Map<String, Object> map) throws  Exception;
	//列表查询
	List<SlReportG14Bean> searchReportG14List(Map<String, Object> map) throws  Exception;
	
	//G18
	PageInfo<SlReportG18Bean> getReportG18List(Map<String, Object> map) throws  Exception;
	List<SlReportG18Bean> searchReportG18List(Map<String, Object> map) throws  Exception;
	//G21
	PageInfo<SlReportG21Bean> getReportG21List(Map<String, Object> map) throws  Exception;
	List<SlReportG21Bean> searchReportG21List(Map<String, Object> map) throws  Exception;
	
	//G22
	PageInfo<SlReportG22Bean> getReportG22List(Map<String, Object> map) throws  Exception;
	List<SlReportG22Bean> searchReportG22List(Map<String, Object> map) throws  Exception;
	
	//G24
	PageInfo<SlReportG24Bean> getReportG24List(Map<String, Object> map) throws  Exception;
	List<SlReportG24Bean> searchReportG24List(Map<String, Object> map) throws  Exception;
	
	//G31
	PageInfo<SlReportG31Bean> getReportG31List(Map<String, Object> map) throws  Exception;
	List<SlReportG31Bean> searchReportG31List(Map<String, Object> map) throws  Exception;
	
	//G33
	PageInfo<SlReportG33Bean> getReportG33List(Map<String, Object> map) throws  Exception;
	List<SlReportG33Bean> searchReportG33List(Map<String, Object> map) throws  Exception;
	//Y0085
	PageInfo<SlReportY0085Bean> getReportY0085List(Map<String, Object> map) throws  Exception;
	List<SlReportY0085Bean> searchReportY0085List(Map<String, Object> map) throws  Exception;

}
