package com.singlee.financial.orp;

import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

/**
 * OPICS RISK 指标查询 DV01 久期
 * 
 * @author shenzl
 *
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/orpQuerySeriveExporter")
public interface OrpQuerySerive {

}
