package com.singlee.financial.marketdata.bean;

import java.math.BigDecimal;

public enum FxRateTypes {

	/**
	 * 汇率
	 */
	Rate(1),
	
	/**
	 * 掉期点,Percentageinpoint的缩写(或 price interest point)价格一般在小数点后四位数左右,报价的方式为"4"和"2"
	 */
	Pips(10000),
	
	/**
	 * 掉期点,1 point=0.1 pip,价格一般在小数点后五位数左右，报价的方式为"5"和"3"
	 */
	Points(100000);
	
	private int value;
	
	private FxRateTypes(int value) {
		this.value = value;
	}

	public BigDecimal getValue() {
		return new BigDecimal(this.value);
	}

	public void setValue(int value) {
		this.value = value;
	}
	
}
