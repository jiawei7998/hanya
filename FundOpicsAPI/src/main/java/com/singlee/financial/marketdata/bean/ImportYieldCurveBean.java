package com.singlee.financial.marketdata.bean;


public class ImportYieldCurveBean extends YieldCurveIndex {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2319998119567785206L;

	/**
	 * 网点
	 */
	private String br;
	
	/**
	 * 期限开始点
	 */
	private String mtystart;
	
	/**
	 * 利率类型 'C' for Money Market, 'S' for Swap, or 'R' for Spread.
	 */
	private String ratetype;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getMtystart() {
		return mtystart;
	}

	public void setMtystart(String mtyStart) {
		this.mtystart = mtyStart;
	}

	public String getRatetype() {
		return ratetype;
	}

	public void setRatetype(String rateType) {
		this.ratetype = rateType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImportYieldCurveBean [br=");
		builder.append(br);
		builder.append(", mtystart=");
		builder.append(mtystart);
		builder.append(", ratetype=");
		builder.append(ratetype);
		builder.append(", getCcy()=");
		builder.append(getCcy());
		builder.append(", getYieldCurveName()=");
		builder.append(getYieldCurveName());
		builder.append(", getSpotDate()=");
		builder.append(getSpotDate());
		builder.append(", getDesignatedMaturity()=");
		builder.append(getDesignatedMaturity());
		builder.append(", getMaturityDate()=");
		builder.append(getMaturityDate());
		builder.append(", getRate()=");
		builder.append(getRate());
		builder.append(", getLstmntdate()=");
		builder.append(getLstmntdate());
		builder.append(", getRateAsk()=");
		builder.append(getRateAsk());
		builder.append(", getRateBid()=");
		builder.append(getRateBid());
		builder.append("]");
		return builder.toString();
	}
	
}
