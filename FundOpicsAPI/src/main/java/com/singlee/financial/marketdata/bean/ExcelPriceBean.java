package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExcelPriceBean implements Serializable {

	/**
	 * Excel smcl bean
	 */
	private static final long serialVersionUID = 8733211339843799531L;
	//B-债券代码 
	private String secid;
	//H-估价净价(元)  
	private BigDecimal cleanPrice;
	//AB-日终估价全价(元)
	private BigDecimal dirtyPrice;
	//W - 可信度
	private String condition;
	//D-流通场所
	private String market;
	//J-估价修正久期
	private BigDecimal duration;
	//AD -剩余本金(元)
	private BigDecimal parValue;
	
	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}

	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}

	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public BigDecimal getDuration() {
		return duration;
	}

	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getParValue() {
		return parValue;
	}

	public void setParValue(BigDecimal parValue) {
		this.parValue = parValue;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ExcelPriceBean [cleanPrice=");
		builder.append(cleanPrice);
		builder.append(", condition=");
		builder.append(condition);
		builder.append(", dirtyPrice=");
		builder.append(dirtyPrice);
		builder.append(", duration=");
		builder.append(duration);
		builder.append(", market=");
		builder.append(market);
		builder.append(", parValue=");
		builder.append(parValue);
		builder.append(", secid=");
		builder.append(secid);
		builder.append("]");
		return builder.toString();
	}
	
	
}
