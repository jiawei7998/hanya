package com.singlee.financial.marketdata.bean;

public enum VolatilityTypes {

	/**
	 * 终波动率对应的值
	 */
	Volatility,
	
	/**
	 * 相对于DETAL50的增量值
	 */
	Increments
}
