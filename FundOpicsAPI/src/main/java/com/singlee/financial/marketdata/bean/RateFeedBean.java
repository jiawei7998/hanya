package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * OPICS市场数据中间表T_RTRATE,该表存放路透的原始数据,对应EXCLE中的RMDATA SHEET页的数据
 * OSYS的XTFX和RTXT会将errorCode=0的数据导入到INTH和IREV、IVOL、IYCR、IRIS表中
 * @author chenxh
 *
 */
public class RateFeedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9028678368420432399L;
	
	/**
	 * 市场数据分组ID,对应OPICS.RSRM表中的GROUPID字段
	 */
	private String rateFeedName;
	
	/**
	 * 市场数据类型,对应OPICS.RSRM表中的rateType字段
	 */
	private RateFeedTypes rateFeedType;

	/**
	 * 市场数据BID公式
	 */
	private String formulaBid;
	
	/**
	 * 市场数据ASK公式
	 */
	private String formulaAsk;
	
	/**
	 * 导入日期
	 */
	private Date inputDate;
	
	/**
	 * 利率/汇率/收益率曲线/波动率
	 */
	private BigDecimal rateBid;
	
	/**
	 * 利率/汇率/收益率曲线/波动率
	 */
	private BigDecimal rateAsk;
	
	/**
	 * 错误 代码
	 */
	private String errorCode;

	public String getRateFeedName() {
		return rateFeedName;
	}

	public void setRateFeedName(String rateFeedName) {
		this.rateFeedName = rateFeedName;
	}

	public RateFeedTypes getRateFeedType() {
		return rateFeedType;
	}

	public void setRateFeedType(RateFeedTypes rateFeedType) {
		this.rateFeedType = rateFeedType;
	}

	public String getFormulaBid() {
		return formulaBid;
	}

	public void setFormulaBid(String formulaBid) {
		this.formulaBid = formulaBid;
	}

	public String getFormulaAsk() {
		return formulaAsk;
	}

	public void setFormulaAsk(String formulaAsk) {
		this.formulaAsk = formulaAsk;
	}

	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public BigDecimal getRateBid() {
		return rateBid;
	}

	public void setRateBid(BigDecimal rateBid) {
		this.rateBid = rateBid;
	}

	public BigDecimal getRateAsk() {
		return rateAsk;
	}

	public void setRateAsk(BigDecimal rateAsk) {
		this.rateAsk = rateAsk;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RateFeedBean [rateFeedName=");
		builder.append(rateFeedName);
		builder.append(", rateFeedType=");
		builder.append(rateFeedType);
		builder.append(", formulaBid=");
		builder.append(formulaBid);
		builder.append(", formulaAsk=");
		builder.append(formulaAsk);
		builder.append(", inputDate=");
		builder.append(inputDate);
		builder.append(", rateBid=");
		builder.append(rateBid);
		builder.append(", rateAsk=");
		builder.append(rateAsk);
		builder.append(", errorCode=");
		builder.append(errorCode);
		builder.append("]");
		return builder.toString();
	}

	
}
