package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 债券收盘价/估值
 * 
 * @author shenzl
 *
 */
public class BondMarketPrices implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7004488091939700977L;

	/**
	 * 债券编号 ISIN
	 */
	private String bondCode;
	/**
	 * 生效日期
	 */
	private Date spotDate;
	/**
	 * 债券净价
	 */
	private BigDecimal cleanPrice;
	
	/**
	 * 债券全价
	 */
	private BigDecimal dirtyPrice;
	
	/**
	 * 债券久期
	 */
	private BigDecimal duration;
	
	/**
	 * 债券估值流通场所
	 */
	private String market;
	
	/**
	 * 债券估值可信度
	 */
	private String condition;
	
	/**
	 * 债券剩余本金,ABS、MBS债券时为本金还款比例后剩余值,其它债券为100
	 */
	private BigDecimal parValue;
	
	/**
	 * OPICS托管账户
	 */
	private String secsacct;


	public String getBondCode() {
		return bondCode;
	}

	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}

	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}

	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public BigDecimal getDuration() {
		return duration;
	}

	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public BigDecimal getParValue() {
		return parValue;
	}

	public void setParValue(BigDecimal parValue) {
		this.parValue = parValue;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSecsacct() {
		return secsacct;
	}

	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BondMarketPrices [bondCode=");
		builder.append(bondCode);
		builder.append(", spotDate=");
		builder.append(spotDate);
		builder.append(", cleanPrice=");
		builder.append(cleanPrice);
		builder.append(", dirtyPrice=");
		builder.append(dirtyPrice);
		builder.append(", duration=");
		builder.append(duration);
		builder.append(", market=");
		builder.append(market);
		builder.append(", condition=");
		builder.append(condition);
		builder.append(", parValue=");
		builder.append(parValue);
		builder.append(", secsacct=");
		builder.append(secsacct);
		builder.append("]");
		return builder.toString();
	}

}
