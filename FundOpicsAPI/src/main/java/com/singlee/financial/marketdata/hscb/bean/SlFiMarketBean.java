package com.singlee.financial.marketdata.hscb.bean;

import java.math.BigDecimal;

import com.singlee.financial.marketdata.bean.ImportSeclPriceBean;

/**
 * 债券收盘价实体类
 * @author chenxh
 *
 */
public class SlFiMarketBean extends ImportSeclPriceBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6322380655095713890L;
	
	private String fedealno;
	
	private String condition;
	
	private String market;
	
	private BigDecimal duration;
	
	private BigDecimal cleanPrice;
	
	private BigDecimal dirtyPrice;
	@Override
	public String getFedealno() {
		return fedealno;
	}
	@Override
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public BigDecimal getDuration() {
		return duration;
	}

	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}

	public BigDecimal getCleanPrice() {
		return cleanPrice;
	}

	public void setCleanPrice(BigDecimal cleanPrice) {
		this.cleanPrice = cleanPrice;
	}

	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SlFiMarketBean [fedealno=");
		builder.append(fedealno);
		builder.append(", condition=");
		builder.append(condition);
		builder.append(", market=");
		builder.append(market);
		builder.append(", duration=");
		builder.append(duration);
		builder.append(", cleanPrice=");
		builder.append(cleanPrice);
		builder.append(", dirtyPrice=");
		builder.append(dirtyPrice);
		builder.append(", getBr()=");
		builder.append(getBr());
		builder.append(", getSecid()=");
		builder.append(getSecid());
		builder.append(", getPrice()=");
		builder.append(getPrice());
		builder.append(", getSecsAcct()=");
		builder.append(getSecsAcct());
		builder.append(", getFactor()=");
		builder.append(getFactor());
		builder.append(", getIntcalcrule()=");
		builder.append(getIntcalcrule());
		builder.append("]");
		return builder.toString();
	}
	
}
