package com.singlee.financial.marketdata.intfc;

import java.util.HashMap;
import java.util.Map;

import com.singlee.financial.marketdata.bean.BondMarketPrices;

/**
 * 债券收盘价处理接口,请在capitalWeb项目中实现
 * @author chenxh
 *
 */
public interface ISmclPriceFileService {

	/**
	 * 根据上传文件导入债券收盘价到OPICS系统
	 * @param excelPath
	 * @throws Exception
	 */
	Map<String,BondMarketPrices> process(String filePath,HashMap<String,String> securInfo) throws Exception;
}
