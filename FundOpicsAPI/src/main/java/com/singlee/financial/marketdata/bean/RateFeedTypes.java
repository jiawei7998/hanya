package com.singlee.financial.marketdata.bean;

/**
 * OPICS市场数据类型,该枚举值为OPICS固定值不可更改
 * @author chenxh
 *
 */
public enum RateFeedTypes {
	/**
	 * 外汇汇率
	 */
	FXRATE,  
	/**
	 * 利率
	 */
	INTRATE,   
	/**
	 * 收益率曲线
	 */
	YCRATE,      
	/**
	 * 波动率曲线
	 */
	VOLRATE,	
	/**
	 * 债券收盘价OPICS的RSRM模式
	 */
	CMRATE,	
	/**
	 * 债券收盘价OPICS的SECL+INTH模式
	 */
	SCML
}
