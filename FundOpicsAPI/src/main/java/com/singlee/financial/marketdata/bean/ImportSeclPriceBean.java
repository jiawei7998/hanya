package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ImportSeclPriceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3098179340413366099L;

	private String br;

	private String secid;
	
	private BigDecimal price;
	
	private String secsAcct;
	
	private BigDecimal factor;
	
	private String intcalcrule;
	
	private String fedealno;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSecsAcct() {
		return secsAcct;
	}

	public void setSecsAcct(String secsAcct) {
		this.secsAcct = secsAcct;
	}

	public BigDecimal getFactor() {
		return factor;
	}

	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	public String getIntcalcrule() {
		return intcalcrule;
	}

	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((br == null) ? 0 : br.hashCode());
		result = prime * result + ((secid == null) ? 0 : secid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;}
		if (obj == null) {
			return false;}
		if (getClass() != obj.getClass()) {
			return false;}
		ImportSeclPriceBean other = (ImportSeclPriceBean) obj;
		if (br == null) {
			if (other.br != null) {
				return false;}
		} else if (!br.equals(other.br)) {
			return false;}
		if (secid == null) {
			if (other.secid != null) {
				return false;}
		} else if (!secid.equals(other.secid)) {
			return false;}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImportSeclPriceBean [br=");
		builder.append(br);
		builder.append(", secid=");
		builder.append(secid);
		builder.append(", price=");
		builder.append(price);
		builder.append(", secsAcct=");
		builder.append(secsAcct);
		builder.append(", factor=");
		builder.append(factor);
		builder.append(", intcalcrule=");
		builder.append(intcalcrule);
		builder.append(", fedealno=");
		builder.append(fedealno);
		builder.append("]");
		return builder.toString();
	}
	
	
}
