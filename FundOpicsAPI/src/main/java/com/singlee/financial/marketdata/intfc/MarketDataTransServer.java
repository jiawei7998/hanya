package com.singlee.financial.marketdata.intfc;

import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlIrevBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSecmBean;
import com.singlee.financial.marketdata.bean.*;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Opics(context = OpicsContext.SINGLEE_API, uri = "/marketDataTransServExporter")
public interface MarketDataTransServer {

	/**
	 * 外汇价格数据
	 * 
	 * @param frrLst    传入数据
	 * @param repeatNum 当日导入次数,最多99次
	 * @return
	 */
	@Deprecated
	SlOutBean saveFxRateData(List<FxRevaluationRates> frrLst, int repeatNum);

	/**
	 * 利率值
	 * 
	 * @param riLst     传入数据
	 * @param repeatNum 当日导入次数,最多99次
	 * @return
	 */
	@Deprecated
	SlOutBean saveRateData(List<RateHistoryInformation> rhiLst, int repeatNum);

	/**
	 * 收益率值
	 * 
	 * @param yciLst    传入数据
	 * @param repeatNum 当日导入次数,最多99次
	 * @return
	 */
	@Deprecated
	SlOutBean saveYieldData(List<YieldCurveIndex> yciLst, int repeatNum);

	/**
	 * 波动率
	 * 
	 * @param ovLst     传入数据
	 * @param repeatNum 当日导入次数,最多99次
	 * @return
	 */
	@Deprecated
	SlOutBean saveVoldData(List<OptionsVolatility> ovLst, int repeatNum);

	/**
	 * 债券收盘价
	 * 
	 * @param bmpLst    传入数据
	 * @param repeatNum 当日导入次数,最多99次
	 * @return
	 */
	@Deprecated
	SlOutBean savePriceData(List<BondMarketPrices> bmpLst, int repeatNum);
	
	/**
	 *查找订阅码
	 * 
	 * @param groupId    数据源
	 * @return 相关订阅码
	 */
	List<SubscriptionCode> queryRsrmByGroupId(String groupId);
	
	/**
	 * 市场数据获取OPICS 市场数据  涉及表(SECM  SECL  SEFM )
	 *
	 */
	@Deprecated
	List<OpicsSmclBean> getSmclOpicsList();
	
	/**
	 * 债券信息
	 * @param sibLst
	 * @param repeatNum
	 * @return
	 */
	SlOutBean saveBondData(List<SlSecmBean> sibLst, int repeatNum);
	
	/**
	 * 外汇汇率导入
	 * @param frRateMap
	 * @param fxRateType
	 * @return
	 */
	SlOutBean saveFxRateData(Map<String,FxRevaluationRates> frRateMap,FxRateTypes fxRateType);

	/**
	 * 定盘利率导入
	 * @param rhisMap
	 * @return
	 */
	SlOutBean saveRateData(Map<String,RateHistoryInformation> rhisMap);

	/**
	 * 收益率曲线导入
	 * @param ycrtMap
	 * @return
	 */
	SlOutBean saveYieldData(Map<String,YieldCurveIndex> ycrtMap);

	/**
	 * 波动率导入
	 * @param ovlMap
	 * @param volatilityType
	 * @return
	 */
	SlOutBean saveVoldData(Map<String,OptionsVolatility> ovlMap,VolatilityTypes volatilityType);
	
	/**
	 * 债券收盘价导入
	 * @param excelMap
	 * @return
	 */
	SlOutBean savePriceData(Map<String,BondMarketPrices> excelMap);
	
	/**
	 * 查询OPICS系统中需要导入的债券信息
	 * @return
	 */
	HashMap<String,String> queryImportSecurData();
	
	/**
	 * 徽商银行债券收盘导入,增加了SL_FI_MARKET表操作
	 * @param excelMap
	 * @return
	 */
	SlOutBean savePriceDataHscb(Map<String,BondMarketPrices> excelMap);

	public int getMaxFedealNo(Date postdate, RateFeedTypes rateFeedType);

	public String createFedealno(Date postdate,int count);


    public Boolean insertToIrev(List<SlIrevBean> irevBeans,List<SlInthBean> inthBeans);

}
