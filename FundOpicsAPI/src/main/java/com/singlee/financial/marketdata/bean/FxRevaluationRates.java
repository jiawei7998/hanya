package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 外汇汇率
 * 
 * 市场即期汇率+远/掉期点
 * 
 * @author shenzl
 *
 */
public class FxRevaluationRates implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7943259290828767018L;

	private String br;
	/**
	 * 货币代码
	 * 
	 * @see https://www.iso.org/iso-4217-currency-codes.html
	 */
	private String ccy;
	/**
	 * 表达式
	 */
	private String terms;

	/**
	 * 即期价格日期
	 */
	private Date spotDate;
	/**
	 * 即期汇率
	 */
	private BigDecimal spotRate;
	/**
	 * 期限指标【ON/1W/2W/1Y...】
	 */
	private String designatedMaturity;
	/**
	 * 对应的到期日期
	 */
	private Date maturityDate;
	/**
	 * 期限对应的远/掉期点
	 */
	private BigDecimal baseRatePoint;
	/**
	 * 对应期限对应的汇率,如果不存在期限点,直接填写汇率即可
	 */
	private BigDecimal rate;
	/**
	 * 价格更新日期
	 */
	private Date lstmntdate;
	
	/**
	 * 询价
	 */
	private BigDecimal rateAsk;
	
	/**
	 * 卖价
	 */
	private BigDecimal rateBid;

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

	public BigDecimal getSpotRate() {
		return spotRate;
	}

	public void setSpotRate(BigDecimal spotRate) {
		this.spotRate = spotRate;
	}

	public String getDesignatedMaturity() {
		return designatedMaturity;
	}

	public void setDesignatedMaturity(String designatedMaturity) {
		this.designatedMaturity = designatedMaturity;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	public BigDecimal getBaseRatePoint() {
		return baseRatePoint;
	}

	public void setBaseRatePoint(BigDecimal baseRatePoint) {
		this.baseRatePoint = baseRatePoint;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public BigDecimal getRateAsk() {
		return rateAsk;
	}

	public void setRateAsk(BigDecimal rateAsk) {
		this.rateAsk = rateAsk;
	}

	public BigDecimal getRateBid() {
		return rateBid;
	}

	public void setRateBid(BigDecimal rateBid) {
		this.rateBid = rateBid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FxRevaluationRates [br=");
		builder.append(br);
		builder.append(", ccy=");
		builder.append(ccy);
		builder.append(", terms=");
		builder.append(terms);
		builder.append(", spotDate=");
		builder.append(spotDate);
		builder.append(", spotRate=");
		builder.append(spotRate);
		builder.append(", designatedMaturity=");
		builder.append(designatedMaturity);
		builder.append(", maturityDate=");
		builder.append(maturityDate);
		builder.append(", baseRatePoint=");
		builder.append(baseRatePoint);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", lstmntdate=");
		builder.append(lstmntdate);
		builder.append(", rateAsk=");
		builder.append(rateAsk);
		builder.append(", rateBid=");
		builder.append(rateBid);
		builder.append("]");
		return builder.toString();
	}
	
}
