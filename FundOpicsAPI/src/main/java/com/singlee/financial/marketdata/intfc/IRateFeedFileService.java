package com.singlee.financial.marketdata.intfc;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.singlee.financial.marketdata.bean.FxRevaluationRates;
import com.singlee.financial.marketdata.bean.OptionsVolatility;
import com.singlee.financial.marketdata.bean.RateFeedBean;
import com.singlee.financial.marketdata.bean.RateFeedTypes;
import com.singlee.financial.marketdata.bean.RateHistoryInformation;
import com.singlee.financial.marketdata.bean.YieldCurveIndex;

/**
 * RateFeed市场数据处理,请在capitalWeb项目中实现
 * @author chenxh
 *
 */
public interface IRateFeedFileService {

	/**
	 * 解析EXCEL文件,转换成Hashtable对象
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	Hashtable<RateFeedTypes, List<RateFeedBean>> process(String filePath) throws Exception;
	
	/**
	 * 处理波动率数据
	 * @param vols
	 * @return
	 * @throws Exception
	 */
	Map<String,OptionsVolatility> processVolatility(List<RateFeedBean> vols) throws Exception;
	
	/**
	 * 处理收益率曲线数据
	 * @param yields
	 * @return
	 * @throws Exception
	 */
	Map<String,YieldCurveIndex> processYieldCurve(List<RateFeedBean> yields) throws Exception;
	
	/**
	 * 处理定利利率数据
	 * @param rates
	 * @return
	 * @throws Exception
	 */
	Map<String,RateHistoryInformation> processIntrestRate(List<RateFeedBean> rates) throws Exception;
	
	/**
	 * 处理外汇汇率数据
	 * @param fxRates
	 * @return
	 * @throws Exception
	 */
	Map<String,FxRevaluationRates> processFxRate(List<RateFeedBean> fxRates) throws Exception;
}
