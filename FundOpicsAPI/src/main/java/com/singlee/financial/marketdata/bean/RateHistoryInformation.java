package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 产品利率
 * 
 * @author shenzl
 *
 */
public class RateHistoryInformation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8402141237760279115L;

	private String br;
	/**
	 * 产品/利率代码
	 */
	private String rateCode;
	/**
	 * 生效日期
	 */
	private Date spotDate;
	/**
	 * 对应利率中间价
	 */
	private BigDecimal intRate;
	/**
	 * 价格更新日期
	 */
	private Date lstmntdate;
	
	/**
	 * 对应利率询价
	 */
	private BigDecimal intRateAsk;
	
	/**
	 * 对应利率卖价
	 */
	private BigDecimal intRateBid;
	

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

	public BigDecimal getIntRate() {
		return intRate;
	}

	public void setIntRate(BigDecimal intRate) {
		this.intRate = intRate;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public BigDecimal getIntRateAsk() {
		return intRateAsk;
	}

	public void setIntRateAsk(BigDecimal intRateAsk) {
		this.intRateAsk = intRateAsk;
	}

	public BigDecimal getIntRateBid() {
		return intRateBid;
	}

	public void setIntRateBid(BigDecimal intRateBid) {
		this.intRateBid = intRateBid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RateHistoryInformation [br=");
		builder.append(br);
		builder.append(", rateCode=");
		builder.append(rateCode);
		builder.append(", spotDate=");
		builder.append(spotDate);
		builder.append(", intRate=");
		builder.append(intRate);
		builder.append(", lstmntdate=");
		builder.append(lstmntdate);
		builder.append(", intRateAsk=");
		builder.append(intRateAsk);
		builder.append(", intRateBid=");
		builder.append(intRateBid);
		builder.append("]");
		return builder.toString();
	}

}
