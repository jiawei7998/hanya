package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *	OPCIS SMCL bean 
 *
 */
public class OpicsSmclBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3957076261801486753L;
	//B-债券代码 
	private String secid;
	private String br;
	private String sNumber;
	private String secsacct;
	//SEFM.FACTOR_12 FACTOR
	private BigDecimal factor;
    //SECM.INTCALCRULE
    private String Intcalcrule;
    //SECL.CLSGPRICE_8 
    private BigDecimal price;
    
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getsNumber() {
		return sNumber;
	}
	public void setsNumber(String sNumber) {
		this.sNumber = sNumber;
	}
	public String getSecsacct() {
		return secsacct;
	}
	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}
	public BigDecimal getFactor() {
		return factor;
	}
	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}
	public String getIntcalcrule() {
		return Intcalcrule;
	}
	public void setIntcalcrule(String intcalcrule) {
		Intcalcrule = intcalcrule;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
    
}
