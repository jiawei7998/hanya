package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ImportVolBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6867009825980000385L;
	
	private String br;
	
	private String optiontype;
	
	private String optiontypekey;
	
	private String desmat;
	
	private BigDecimal strikeprice;
	
	private String term;
	
	private BigDecimal callbidvolatility;
	
	private BigDecimal putbidvolatility;
	
	private BigDecimal callaskvolatility;
	
	private BigDecimal putaskvolatility;
	
	private String atmoneyind;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getOptiontype() {
		return optiontype;
	}

	public void setOptiontype(String optiontype) {
		this.optiontype = optiontype;
	}

	public String getOptiontypekey() {
		return optiontypekey;
	}

	public void setOptiontypekey(String optiontypekey) {
		this.optiontypekey = optiontypekey;
	}

	public String getDesmat() {
		return desmat;
	}

	public void setDesmat(String desmat) {
		this.desmat = desmat;
	}

	public BigDecimal getStrikeprice() {
		return strikeprice;
	}

	public void setStrikeprice(BigDecimal strikeprice) {
		this.strikeprice = strikeprice;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public BigDecimal getCallbidvolatility() {
		return callbidvolatility;
	}

	public void setCallbidvolatility(BigDecimal callbidvolatility) {
		this.callbidvolatility = callbidvolatility;
	}

	public BigDecimal getPutbidvolatility() {
		return putbidvolatility;
	}

	public void setPutbidvolatility(BigDecimal putbidvolatility) {
		this.putbidvolatility = putbidvolatility;
	}

	public BigDecimal getCallaskvolatility() {
		return callaskvolatility;
	}

	public void setCallaskvolatility(BigDecimal callaskvolatility) {
		this.callaskvolatility = callaskvolatility;
	}

	public BigDecimal getPutaskvolatility() {
		return putaskvolatility;
	}

	public void setPutaskvolatility(BigDecimal putaskvolatility) {
		this.putaskvolatility = putaskvolatility;
	}

	public String getAtmoneyind() {
		return atmoneyind;
	}

	public void setAtmoneyind(String atmoneyind) {
		this.atmoneyind = atmoneyind;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImportVolBean [br=");
		builder.append(br);
		builder.append(", optiontype=");
		builder.append(optiontype);
		builder.append(", optiontypekey=");
		builder.append(optiontypekey);
		builder.append(", desmat=");
		builder.append(desmat);
		builder.append(", strikeprice=");
		builder.append(strikeprice);
		builder.append(", term=");
		builder.append(term);
		builder.append(", callbidvolatility=");
		builder.append(callbidvolatility);
		builder.append(", putbidvolatility=");
		builder.append(putbidvolatility);
		builder.append(", callaskvolatility=");
		builder.append(callaskvolatility);
		builder.append(", putaskvolatility=");
		builder.append(putaskvolatility);
		builder.append(", atmoneyind=");
		builder.append(atmoneyind);
		builder.append("]");
		return builder.toString();
	}
	
	
}
