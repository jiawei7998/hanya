package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 期权波动率
 * 
 * @author shenzl
 *
 */
public class OptionsVolatility implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3316674709463599316L;

	/**
	 * 期权类型【FX/FI】
	 */
	private String optionType;
	/**
	 * 货币对
	 */
	private String ccyParis;
	/**
	 * 生效日期
	 */
	private Date spotDate;
	/**
	 * 期限指标
	 */
	private String designatedMaturity;
	/**
	 * 对应的到期日期
	 */
	private Date maturityDate;
	/**
	 * 行使价格10,25,50
	 */
	private BigDecimal strikePrice;
	/**
	 * 表达式
	 */
	private String terms;
	/**
	 * 波动率
	 */
	private BigDecimal volatility;
	/**
	 * 价格更新日期
	 */
	private Date lstmntdate;
	
	/**
	 * 涨跌标识
	 */
	private String callputInd;
	
	/**
	 * 是否为detal50波动率
	 */
	private BigDecimal volatilityAmtRate;
	
	/**
	 * 询价波动率
	 * strikePrice=detal 50时为实际值,其它值时为相对于detal50的偏移量
	 */
	private BigDecimal volatilityAsk;
	
	/**
	 * 卖价波动率
	 * strikePrice=detal 50时为实际值,其它值时为相对于detal50的偏移量
	 */
	private BigDecimal volatilityBid;
	

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public String getCcyParis() {
		return ccyParis;
	}

	public void setCcyParis(String ccyParis) {
		this.ccyParis = ccyParis;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

	public String getDesignatedMaturity() {
		return designatedMaturity;
	}

	public void setDesignatedMaturity(String designatedMaturity) {
		this.designatedMaturity = designatedMaturity;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	public BigDecimal getStrikePrice() {
		return strikePrice;
	}

	public void setStrikePrice(BigDecimal strikePrice) {
		this.strikePrice = strikePrice;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public BigDecimal getVolatility() {
		return volatility;
	}

	public void setVolatility(BigDecimal volatility) {
		this.volatility = volatility;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public String getCallputInd() {
		return callputInd;
	}

	public void setCallputInd(String callputInd) {
		this.callputInd = callputInd;
	}

	public BigDecimal getVolatilityAmtRate() {
		return volatilityAmtRate;
	}

	public void setVolatilityAmtRate(BigDecimal volatilityAmtRate) {
		this.volatilityAmtRate = volatilityAmtRate;
	}

	public BigDecimal getVolatilityAsk() {
		return volatilityAsk;
	}

	public void setVolatilityAsk(BigDecimal volatilityAsk) {
		this.volatilityAsk = volatilityAsk;
	}

	public BigDecimal getVolatilityBid() {
		return volatilityBid;
	}

	public void setVolatilityBid(BigDecimal volatilityBid) {
		this.volatilityBid = volatilityBid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OptionsVolatility [optionType=");
		builder.append(optionType);
		builder.append(", ccyParis=");
		builder.append(ccyParis);
		builder.append(", spotDate=");
		builder.append(spotDate);
		builder.append(", designatedMaturity=");
		builder.append(designatedMaturity);
		builder.append(", maturityDate=");
		builder.append(maturityDate);
		builder.append(", strikePrice=");
		builder.append(strikePrice);
		builder.append(", terms=");
		builder.append(terms);
		builder.append(", volatility=");
		builder.append(volatility);
		builder.append(", lstmntdate=");
		builder.append(lstmntdate);
		builder.append(", callputInd=");
		builder.append(callputInd);
		builder.append(", volatilityAmtRate=");
		builder.append(volatilityAmtRate);
		builder.append(", volatilityAsk=");
		builder.append(volatilityAsk);
		builder.append(", volatilityBid=");
		builder.append(volatilityBid);
		builder.append("]");
		return builder.toString();
	}

}
