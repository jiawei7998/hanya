package com.singlee.financial.bean;

import java.math.BigDecimal;
import java.util.Date;

import com.singlee.financial.pojo.DepositsAndLoansBean;

public class SlDepositsAndLoansBean extends DepositsAndLoansBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -643863733285093133L;
	/**
	 * INTH 头表信息
	 */
	private SlInthBean inthBean;
	/**
	 * OPICS拓展属性
	 */
	private SlExternalBean externalBean;
	/**
	 * 期限
	 */
	private String tenor;
	/**
	 * 点差
	 */
	private BigDecimal spread;
	/**
	 * 逾期计算利率
	 */
	private String intrate;
	/**
	 * 计划类型
	 */
	private String scheduletype;
	/**
	 * 利息日期规则
	 */
	private String intdaterule;
	/**
	 * 利息日期方向
	 */
	private String intdatedir;
	/**
	 * 开始类型代码
	 */
	private String commtypecode;
	/**
	 * 
	 */
	private String commusualid;
	/**
	 * 利率代码
	 */
	private String rateCode;
	/**
	 * COMMMEANS 开始结算方式
	 */
	private String commmeans;
	/**
	 * COMMACCT 开始结算账户
	 */
	private String commacct;
	/**
	 * MATMEANS 到期结算方式
	 */
	private String matmeans;
	/**
	 * MATACCT 到期结算账户
	 */
	private String matacct;
	/**
	 * INTCALCRULE 利息计算规则 
	 */
	private String intcalcrule;
	/**
	 * INTPAYCYCLE 利息支付周期
	 */
	private String intpaycycle;
	/**
	 * INTSMEANS 利息结算方式
	 */
	private String intsmeans;
	/**
	 * INTSACCT  利息结算帐户 
	 */
	private String intsacct;
	
	/**
	 * 拆借提前还款
	 */
	// 输入日期
	private Date inputdate;
	//输入时间
	private String inputtime;
	// OPICS交易流水号
	private String dealno;
	//
	private String dealseq;
	// 起息日
	private Date vdate;
	// 到期日
	private Date mdate;
	// 提前还款日
	private Date emdate;
	// 付款日期
	private Date settdate;
	// 提前还款金额
	private BigDecimal advAmt;
	// 处罚金额
	private BigDecimal penaltyAmt;
	// 支付金额
	private BigDecimal payAmt;
	// 是否带利息
	private String capintind;
	// I 增加本金 D 减少本金
	private String prinincdecind;
	// D到期日适用 P按比例北京 M到期适用
	private String prorateind;

	//计息规则
	private String scheduleType;

	//付息频率
	private String intPayCycle;

	//利息付款日期
	private String intPayDay;

	//利息日期规则
	private String intDateRule;

	//利率差
	private BigDecimal spread8;

	//首次付息日
	private Date firstIPayDate;

	//最后一次付息日
	private Date lastIntPayDate;
	/**
	 * 拆借初始化
	 * 
	 * @param br
	 *            业务部门分支
	 * @param tag
	 *            标识
	 * @param detail
	 *            细节
	 * @note 1、交易导入 TAG="DLDE" , DETAIL="IDLD"
	 * 
	 *       2、交易冲销 TAG="DLDR", DETAIL="IRVV"
	 * 
	 *       3、交易提前 部分:TAG="DLUP",DETAIL="IDLV" 全部： TAG:"DLEM",DETAIL="IDLV"
	 * 
	 */
	public SlDepositsAndLoansBean(String br, String server, String tag,
			String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.DL.SEQ);
			inthBean.setInoutind(SlDealModule.DL.INOUTIND);
			inthBean.setPriority(SlDealModule.DL.PRIORITY);
			inthBean.setStatcode(SlDealModule.DL.STATCODE);
			inthBean.setSyst(SlDealModule.DL.SYST);
			inthBean.setIoper(SlDealModule.DL.IOPER);
		}
	}


	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getIntPayCycle() {
		return intPayCycle;
	}

	public void setIntPayCycle(String intPayCycle) {
		this.intPayCycle = intPayCycle;
	}

	public String getIntPayDay() {
		return intPayDay;
	}

	public void setIntPayDay(String intPayDay) {
		this.intPayDay = intPayDay;
	}

	public String getIntDateRule() {
		return intDateRule;
	}

	public void setIntDateRule(String intDateRule) {
		this.intDateRule = intDateRule;
	}

	public BigDecimal getSpread8() {
		return spread8;
	}

	public void setSpread8(BigDecimal spread8) {
		this.spread8 = spread8;
	}

	public Date getFirstIPayDate() {
		return firstIPayDate;
	}

	public void setFirstIPayDate(Date firstIPayDate) {
		this.firstIPayDate = firstIPayDate;
	}

	public Date getLastIntPayDate() {
		return lastIntPayDate;
	}

	public void setLastIntPayDate(Date lastIntPayDate) {
		this.lastIntPayDate = lastIntPayDate;
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public SlExternalBean getExternalBean() {
		return externalBean;
	}

	public void setExternalBean(SlExternalBean externalBean) {
		this.externalBean = externalBean;
	}
	@Override
	public String getTenor() {
		return tenor;
	}
	@Override
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public BigDecimal getSpread() {
		return spread;
	}

	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}

	public String getIntrate() {
		return intrate;
	}

	public void setIntrate(String intrate) {
		this.intrate = intrate;
	}

	public String getScheduletype() {
		return scheduletype;
	}

	public void setScheduletype(String scheduletype) {
		this.scheduletype = scheduletype;
	}

	public String getIntdaterule() {
		return intdaterule;
	}

	public void setIntdaterule(String intdaterule) {
		this.intdaterule = intdaterule;
	}

	public String getIntdatedir() {
		return intdatedir;
	}

	public void setIntdatedir(String intdatedir) {
		this.intdatedir = intdatedir;
	}

	public String getCommtypecode() {
		return commtypecode;
	}

	public void setCommtypecode(String commtypecode) {
		this.commtypecode = commtypecode;
	}

	public String getCommusualid() {
		return commusualid;
	}

	public void setCommusualid(String commusualid) {
		this.commusualid = commusualid;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getCommmeans() {
		return commmeans;
	}

	public void setCommmeans(String commmeans) {
		this.commmeans = commmeans;
	}

	public String getCommacct() {
		return commacct;
	}

	public void setCommacct(String commacct) {
		this.commacct = commacct;
	}

	public String getMatmeans() {
		return matmeans;
	}

	public void setMatmeans(String matmeans) {
		this.matmeans = matmeans;
	}

	public String getMatacct() {
		return matacct;
	}

	public void setMatacct(String matacct) {
		this.matacct = matacct;
	}

	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}

	public String getIntcalcrule() {
		return intcalcrule;
	}

	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}

	public String getIntpaycycle() {
		return intpaycycle;
	}

	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}

	public String getIntsmeans() {
		return intsmeans;
	}

	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}

	public String getIntsacct() {
		return intsacct;
	}

	public String getDealno() {
		return dealno;
	}

	public Date getInputdate() {
		return inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getDealseq() {
		return dealseq;
	}

	public void setDealseq(String dealseq) {
		this.dealseq = dealseq;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public Date getMdate() {
		return mdate;
	}

	public void setMdate(Date mdate) {
		this.mdate = mdate;
	}

	public Date getEmdate() {
		return emdate;
	}

	public void setEmdate(Date emdate) {
		this.emdate = emdate;
	}

	public Date getSettdate() {
		return settdate;
	}

	public void setSettdate(Date settdate) {
		this.settdate = settdate;
	}

	public BigDecimal getAdvAmt() {
		return advAmt;
	}

	public void setAdvAmt(BigDecimal advAmt) {
		this.advAmt = advAmt;
	}

	public BigDecimal getPenaltyAmt() {
		return penaltyAmt;
	}

	public void setPenaltyAmt(BigDecimal penaltyAmt) {
		this.penaltyAmt = penaltyAmt;
	}

	public BigDecimal getPayAmt() {
		return payAmt;
	}

	public void setPayAmt(BigDecimal payAmt) {
		this.payAmt = payAmt;
	}

	public String getCapintind() {
		return capintind;
	}

	public void setCapintind(String capintind) {
		this.capintind = capintind;
	}

	public String getPrinincdecind() {
		return prinincdecind;
	}

	public void setPrinincdecind(String prinincdecind) {
		this.prinincdecind = prinincdecind;
	}

	public String getProrateind() {
		return prorateind;
	}

	public void setProrateind(String prorateind) {
		this.prorateind = prorateind;
	}

	
}
