package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SlCnapsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String certNo; // 业务号
	private String balanceDate; // 清算日期
	private BigDecimal amount; // 清算金额
	private String certState; // 业务状态
	private String recNo; // 收款人账号
	private String recName; // 收款人名称
	private String payNo; // 汇入行行号
	private String payName; // 汇入行名称
	private String remark; // 汇款附言（可修改）
	private Date inputTime;
	private Date lastTime;
	//新增 2019-3-1
	private String operator;//操作人
	private String dealno;//交易单号
	private String instid;//机构号
	private String flag;//0-待处理 1-处理成功 2处理失败
	private String br;//OPICS网点
	private String product;//OPICS产品
	private String type;//OPICS业务类型
	private String cno;//OPICS交易对手编号
	private String seq;//交易对手序号
	private String setmeans;//OPICS清算方式
	private String setacct;//OPICS清算路径
	private String trad;//OPICS交易员
	private String pmtqmsg;//插入状态，NEW(新建)，UPDATE(有更新但是由于状态不符没做操作),Rebuild(重建)
	

	public String getCertNo() {
		return certNo;
	}

	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}

	public String getBalanceDate() {
		return balanceDate;
	}

	public void setBalanceDate(String balanceDate) {
		this.balanceDate = balanceDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCertState() {
		return certState;
	}

	public void setCertState(String certState) {
		this.certState = certState;
	}

	public String getRecNo() {
		return recNo;
	}

	public void setRecNo(String recNo) {
		this.recNo = recNo;
	}

	public String getRecName() {
		return recName;
	}

	public void setRecName(String recName) {
		this.recName = recName;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getInputTime() {
		return inputTime;
	}

	public void setInputTime(Date inputTime) {
		this.inputTime = inputTime;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getInstid() {
		return instid;
	}

	public void setInstid(String instid) {
		this.instid = instid;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getSetmeans() {
		return setmeans;
	}

	public void setSetmeans(String setmeans) {
		this.setmeans = setmeans;
	}

	public String getSetacct() {
		return setacct;
	}

	public void setSetacct(String setacct) {
		this.setacct = setacct;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getPmtqmsg() {
		return pmtqmsg;
	}

	public void setPmtqmsg(String pmtqmsg) {
		this.pmtqmsg = pmtqmsg;
	}
	
}
