package com.singlee.financial.bean;

import java.io.Serializable;

public class SlProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3777625698091355784L;

	private String key;

	private String value;

	private String commont;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCommont() {
		return commont;
	}

	public void setCommont(String commont) {
		this.commont = commont;
	}

}
