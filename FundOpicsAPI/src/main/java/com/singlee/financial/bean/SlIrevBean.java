package com.singlee.financial.bean;

import java.io.Serializable;

public class SlIrevBean implements Serializable{

	/**
	 * IREV
	 */
	private static final long serialVersionUID = 4841323815441766031L;
	private SlInthBean inthBean;
	private String ccy;//币种
	private String xccy;
	private String spotrate;
	private String period1Rate;
	private String period2Rate;
	private String period3Rate;
	private String period4Rate;
	private String period5Rate;
	private String period6Rate;
	private String period7Rate;
	private String period8Rate;
	private String period9Rate;
	private String period10Rate;
	private String period11Rate;
	private String period12Rate;
	private String period13Rate;
	private String period14Rate;
	private String period15Rate;
	private String period16Rate;
	private String period17Rate;
	private String period18Rate;
	private String period19Rate;
	private String period20Rate;
	private String period21Rate;
	private String period22Rate;
	private String period23Rate;
	private String period24Rate;
	private String period25Rate;
	private String period26Rate;
	private String period27Rate;
	private String period28Rate;
	private String period29Rate;
	private String period30Rate;
	private String period31Rate;
	private String period32Rate;
	private String period33Rate;
	private String period34Rate;
	private String period35Rate;
	private String period36Rate;
	private String period37Rate;
	private String period38Rate;
	private String period39Rate;
	private String lstmntdate;
	
	//市场数据查询所需字段
	private String datatype;
	private String feidate;
	private String statcode;
	private String errorcode;
	
	
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getXccy() {
		return xccy;
	}
	public void setXccy(String xccy) {
		this.xccy = xccy;
	}
	public String getSpotrate() {
		return spotrate;
	}
	public void setSpotrate(String spotrate) {
		this.spotrate = spotrate;
	}
	public String getPeriod1Rate() {
		return period1Rate;
	}
	public void setPeriod1Rate(String period1Rate) {
		this.period1Rate = period1Rate;
	}
	public String getPeriod2Rate() {
		return period2Rate;
	}
	public void setPeriod2Rate(String period2Rate) {
		this.period2Rate = period2Rate;
	}
	public String getPeriod3Rate() {
		return period3Rate;
	}
	public void setPeriod3Rate(String period3Rate) {
		this.period3Rate = period3Rate;
	}
	public String getPeriod4Rate() {
		return period4Rate;
	}
	public void setPeriod4Rate(String period4Rate) {
		this.period4Rate = period4Rate;
	}
	public String getPeriod5Rate() {
		return period5Rate;
	}
	public void setPeriod5Rate(String period5Rate) {
		this.period5Rate = period5Rate;
	}
	public String getPeriod6Rate() {
		return period6Rate;
	}
	public void setPeriod6Rate(String period6Rate) {
		this.period6Rate = period6Rate;
	}
	public String getPeriod7Rate() {
		return period7Rate;
	}
	public void setPeriod7Rate(String period7Rate) {
		this.period7Rate = period7Rate;
	}
	public String getPeriod8Rate() {
		return period8Rate;
	}
	public void setPeriod8Rate(String period8Rate) {
		this.period8Rate = period8Rate;
	}
	public String getPeriod9Rate() {
		return period9Rate;
	}
	public void setPeriod9Rate(String period9Rate) {
		this.period9Rate = period9Rate;
	}
	public String getPeriod10Rate() {
		return period10Rate;
	}
	public void setPeriod10Rate(String period10Rate) {
		this.period10Rate = period10Rate;
	}
	public String getPeriod11Rate() {
		return period11Rate;
	}
	public void setPeriod11Rate(String period11Rate) {
		this.period11Rate = period11Rate;
	}
	public String getPeriod12Rate() {
		return period12Rate;
	}
	public void setPeriod12Rate(String period12Rate) {
		this.period12Rate = period12Rate;
	}
	public String getPeriod13Rate() {
		return period13Rate;
	}
	public void setPeriod13Rate(String period13Rate) {
		this.period13Rate = period13Rate;
	}
	public String getPeriod14Rate() {
		return period14Rate;
	}
	public void setPeriod14Rate(String period14Rate) {
		this.period14Rate = period14Rate;
	}
	public String getPeriod15Rate() {
		return period15Rate;
	}
	public void setPeriod15Rate(String period15Rate) {
		this.period15Rate = period15Rate;
	}
	public String getPeriod16Rate() {
		return period16Rate;
	}
	public void setPeriod16Rate(String period16Rate) {
		this.period16Rate = period16Rate;
	}
	public String getPeriod17Rate() {
		return period17Rate;
	}
	public void setPeriod17Rate(String period17Rate) {
		this.period17Rate = period17Rate;
	}
	public String getPeriod18Rate() {
		return period18Rate;
	}
	public void setPeriod18Rate(String period18Rate) {
		this.period18Rate = period18Rate;
	}
	public String getPeriod19Rate() {
		return period19Rate;
	}
	public void setPeriod19Rate(String period19Rate) {
		this.period19Rate = period19Rate;
	}
	public String getPeriod20Rate() {
		return period20Rate;
	}
	public void setPeriod20Rate(String period20Rate) {
		this.period20Rate = period20Rate;
	}
	public String getPeriod21Rate() {
		return period21Rate;
	}
	public void setPeriod21Rate(String period21Rate) {
		this.period21Rate = period21Rate;
	}
	public String getPeriod22Rate() {
		return period22Rate;
	}
	public void setPeriod22Rate(String period22Rate) {
		this.period22Rate = period22Rate;
	}
	public String getPeriod23Rate() {
		return period23Rate;
	}
	public void setPeriod23Rate(String period23Rate) {
		this.period23Rate = period23Rate;
	}
	public String getPeriod24Rate() {
		return period24Rate;
	}
	public void setPeriod24Rate(String period24Rate) {
		this.period24Rate = period24Rate;
	}
	public String getPeriod25Rate() {
		return period25Rate;
	}
	public void setPeriod25Rate(String period25Rate) {
		this.period25Rate = period25Rate;
	}
	public String getPeriod26Rate() {
		return period26Rate;
	}
	public void setPeriod26Rate(String period26Rate) {
		this.period26Rate = period26Rate;
	}
	public String getPeriod27Rate() {
		return period27Rate;
	}
	public void setPeriod27Rate(String period27Rate) {
		this.period27Rate = period27Rate;
	}
	public String getPeriod28Rate() {
		return period28Rate;
	}
	public void setPeriod28Rate(String period28Rate) {
		this.period28Rate = period28Rate;
	}
	public String getPeriod29Rate() {
		return period29Rate;
	}
	public void setPeriod29Rate(String period29Rate) {
		this.period29Rate = period29Rate;
	}
	public String getPeriod30Rate() {
		return period30Rate;
	}
	public void setPeriod30Rate(String period30Rate) {
		this.period30Rate = period30Rate;
	}
	public String getPeriod31Rate() {
		return period31Rate;
	}
	public void setPeriod31Rate(String period31Rate) {
		this.period31Rate = period31Rate;
	}
	public String getPeriod32Rate() {
		return period32Rate;
	}
	public void setPeriod32Rate(String period32Rate) {
		this.period32Rate = period32Rate;
	}
	public String getPeriod33Rate() {
		return period33Rate;
	}
	public void setPeriod33Rate(String period33Rate) {
		this.period33Rate = period33Rate;
	}
	public String getPeriod34Rate() {
		return period34Rate;
	}
	public void setPeriod34Rate(String period34Rate) {
		this.period34Rate = period34Rate;
	}
	public String getPeriod35Rate() {
		return period35Rate;
	}
	public void setPeriod35Rate(String period35Rate) {
		this.period35Rate = period35Rate;
	}
	public String getPeriod36Rate() {
		return period36Rate;
	}
	public void setPeriod36Rate(String period36Rate) {
		this.period36Rate = period36Rate;
	}
	public String getPeriod37Rate() {
		return period37Rate;
	}
	public void setPeriod37Rate(String period37Rate) {
		this.period37Rate = period37Rate;
	}
	public String getPeriod38Rate() {
		return period38Rate;
	}
	public void setPeriod38Rate(String period38Rate) {
		this.period38Rate = period38Rate;
	}
	public String getPeriod39Rate() {
		return period39Rate;
	}
	public void setPeriod39Rate(String period39Rate) {
		this.period39Rate = period39Rate;
	}

	public String getLstmntdate() {
		return lstmntdate;
	}
	public void setLstmntdate(String lstmntdate) {
		this.lstmntdate = lstmntdate;
	}
	public String getDatatype() {
		return datatype;
	}
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}
	public String getFeidate() {
		return feidate;
	}
	public void setFeidate(String feidate) {
		this.feidate = feidate;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIrevBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 收益
			inthBean.setBr(br);//
			inthBean.setServer(SlDealModule.IREV.SERVER);
			inthBean.setSeq(SlDealModule.IREV.SEQ);
			inthBean.setInoutind(SlDealModule.IREV.INOUTIND);
			inthBean.setTag(SlDealModule.IREV.TAG);
			inthBean.setDetail(SlDealModule.IREV.DETAIL);
			inthBean.setPriority(SlDealModule.IREV.PRIORITY);
			inthBean.setStatcode(SlDealModule.IREV.STATCODE);
			inthBean.setSyst(SlDealModule.IREV.SYST);
			inthBean.setIoper(SlDealModule.IREV.IOPER);
		}
	}
	
}
