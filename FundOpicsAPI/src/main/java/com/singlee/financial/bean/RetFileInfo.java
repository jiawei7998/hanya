package com.singlee.financial.bean;

import java.io.Serializable;

/***
 * Gmrm 文件信息
 * @author LIJ
 *
 */
public class RetFileInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2711553193672811264L;

	/***
	 * 文件名
	 */
	private String fileName;
	
	/***
	 * 文件大小
	 */
	private String fileSize;
	
	/***
	 * 文件类型
	 */
	private String fileType;
	
	/***
	 * 修改时间
	 */
	private String lastTime;
	
	/***
	 * 错误信息
	 */
	private String errorMsg;
	
	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	
	

}
