package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 记账科目实体类(SL_subject_ACUP)
 * 
 * @author zhuxl
 * 
 * @date 2019-07-17
 */
public class SlCoreSubject implements Serializable {
	/**
	 * 账号
	 */
    private String acctno;

	/**
	 * 机构
	 */
    private String br;

	/**
	 * 科目号
	 */
    private String subno;

	/**
	 * 科目名称
	 */
    private String subname;

	/**
	 * 账号名称
	 */
    private String acctname;

	/**
	 * 借贷方向
	 */
    private String drcrind;

	/**
	 * 备注
	 */
    private String note;

	/**
	 * 标识
	 */
    private String flag;

    private static final long serialVersionUID = 1L;

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno == null ? null : acctno.trim();
    }

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getSubno() {
        return subno;
    }

    public void setSubno(String subno) {
        this.subno = subno == null ? null : subno.trim();
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname == null ? null : subname.trim();
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname == null ? null : acctname.trim();
    }

    public String getDrcrind() {
        return drcrind;
    }

    public void setDrcrind(String drcrind) {
        this.drcrind = drcrind == null ? null : drcrind.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }
}