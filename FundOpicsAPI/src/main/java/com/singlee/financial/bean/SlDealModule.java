package com.singlee.financial.bean;

/**
 * 定义多个
 *
 * @author shenzl
 */
public class SlDealModule {

    public static  String BROKER = "D";

    /**
     * 拆借模块静态参数
     */
    public static interface DL {
        /**
         * 标识
         */
        String TAG = "DLDE";
        /**
         * 标识-冲销
         */
        String TAG_R = "DLDR";
        /**
         * 标识-提前回款-全部
         */
        String TAG_LV_ALL = "DLUP";
        /**
         * 标识-提前回款-部分
         */
        String TAG_LV_PART = "DLEM";
        /**
         * 具体接口
         */
        String DETAIL = "IDLD";
        /**
         * 具体接口-冲销
         */
        String DETAIL_R = "IRVV";
        /**
         * 具体接口-提前回款
         */
        String DETAIL_LV = "IDLV";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_IDLD";
        /**
         * 服务-冲销
         */
        String SERVER_R = "SL_FUND_IDLD_R";
        /**
         * 服务-提前回款
         */
        String SERVER_LV = "SL_FUND_IDLD_LV";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";
        /************** 交易直通要素 ************************/
        /**
         * 自动复核(Y:自动 N:手工)
         */
        String VERIND = "Y";
        /**
         * 自动添加清算路径(Y:自动 N:手工 ),必须选择N,
         * 需要填写IDLD表中COMMMEANS、COMMACCT、MATMEANS、MATACCT否则SCHD中SMEANS和SACCT中没值
         */
        String SIIND = "N";
        /**
         * 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPPAYIND = "N";
        /**
         * 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPRECIND = "N";
        /**
         * 禁止发确认书 (Y:是 N：否)
         */
        String SUPCONFIND = "N";
        /**
         * 自动授权 (Y:是 N：否)
         */
        String AUTHSI = "N";
    }

    /**
     * 外汇模块静态参数
     */
    public static interface FXD {
        /**
         * 标识
         */
        String TAG = "FXDE";
        /**
         * 标识 -冲销
         */
        String TAG_R = "FXDR";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "IFXD";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_IFXD";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_IFXD_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";

        /************** 交易直通要素 ************************/
        /**
         * 自动复核(Y:自动 N:手工)
         */
        String VERIND = "Y";
        /**
         * 自动添加清算路径(Y:自动 N:手工 )选择Y时需要填写CCYSMEANS,CCYSACCT,CTRSMEANS,CTRSACCT
         */
        String SIIND = "N";
        /**
         * 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPPAYIND = "N";
        /**
         * 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPRECIND = "N";
        /**
         * 禁止发确认书 (Y:是 N：否)
         */
        String SUPCONFIND = "N";
        /**
         * 自动授权 (Y:是 N：否)
         */
        String AUTHSI = "N";

    }

    /**
     * 现券模块静态参数
     */
    public static interface FI {
        /**
         * 标识
         */
        String TAG = "FIDE";
        /**
         * 标识 -冲销
         */
        String TAG_R = "FIDR";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "BRED";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_BRED";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_BRED_R";
        /**
         * 服务 -收息
         */
        String SERVER_RDFE = "RDFE";
        /**
         * 服务 -收息冲销
         */
        String SERVER_RDFE_R = "RDFR";

        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";

        /************** 交易直通要素 ************************/
        /**
         * 1、债券交易需要注意SETTDATE>=DEALDATE 且不能超过三天否则会提示延迟交割
         *
         * 2、SETTDATE>=债券的VDATE否则会错误提示
         **/
        /**
         * 自动复核(Y:自动 N:手工) 需要填写对应的VERDATE,否则不会出PMTQ,
         */
        String VERIND = "Y";
        /**
         * 独立的清算指令(Y:自动,N:手工)选择Y时需要填写CCYSMEANS,CCYSACCT
         */
        String STANDINSTR = "N";
        /**
         * 存续期是否自动赎回,默认N
         */
        String COUPREINVEST = "N";

    }

    /**
     * 回购模块静态参数
     */
    public static interface REPO {
        /**
         * 标识
         */
        String TAG = "REDE";
        /**
         * 标识 -冲销 待确认
         */
        String TAG_R = "REDR";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "IRCA";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_IRCA";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_IRCA_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";
        /************** 交易直通要素 ************************/
        /**
         * 1.因为存在多券的情况，则抵押券SEQ自己需要递增处理
         *
         * 2.回购交易导入状态位-1,抵押券信息为-5通过触发器处理后续状态修改 成功后查看RPRH和RODT
         */
        /**
         * 担保物代码,使用OPICS的默认值,在RCCM命令中维护
         **/
        String COLLATERALCODE = "ALL";
        /**
         * 交付类型
         */
        String DELIVTYPE = "DVP";
        /**
         * 自动复核(Y:自动 N:手工)
         */
        String VERIND = "Y";
        /**
         * 自动添加清算路径(Y:自动 N:手工 )选择Y时需要填写COMCCYSMEANS,COMCCYSACCT,MATCTRSMEANS,MATCTRSACCT
         */
        String SIIND = "N";
        /**
         * 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPPPAYIND = "N";
        /**
         * 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPPRECIND = "N";
        /**
         * 禁止发确认书 (Y:是 N：否)
         */
        String SUPPCONFIND = "N";
        /**
         * 该值可以是“BON”以指定标题数量，也可以是“FMT”以指定票面金额。该值表示指定抵押品数量的单位。
         */
        String COLLUNIT_CR = "BON";
        String COLLUNIT_OR = "BON";
        /**
         * 净价/全价标识
         */
        String DCPRICEIND = "D";
        /**
         * 回购利率计算类型
         * <p>
         * SIM :Simple interest mehtod
         * <p>
         * AIM :Average interest mehtod
         * <p>
         * CIM:Compounded interest mehtod
         */
        String INTCALCTYPE = "SIM";
        /**
         * 保全货币标识
         */
        String PMVIND = "Y";
        /**
         * 自动授权 (Y:是 N：否)
         */
        String AUTHSI = "N";
    }

    /**
     * 债券借贷模块静态参数
     */
    public static interface SECLEND {
        /**
         * 标识
         */
        String TAG = "SLTE";
        /**
         * 标识 -冲销
         */
        String TAG_R = "SLTE";
        /**
         * 具体接口
         */
        String DETAIL = "ISLD";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_ISLD";
        /**
         * 服务-冲销
         */
        String SERVER_R = "SL_FUND_ISLD_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";
        /************** 交易直通要素 ************************/
        /**
         * 自动复核(Y:自动 N:手工)
         */
        String VERIND = "Y";
        /** 一个是借入借出的标的，一个是标的对应的抵押品，标的只有一个，抵押品可以有多个 **/
        /**
         * 债券借贷标的
         */
        String DEALIND = "SLDH";
        /**
         * 债券借贷抵押物,多个每条都是一样
         */
        String DEALIND_C = "CLDH";
        /**
         * 是否添加清算路径(Y:自动 N:手工)
         */
        String ADDSI = "N";
        /**
         * 自动授权 (Y:是 N：否)
         */
        String AUTHSI = "N";
        /**
         * 担保物代码,使用OPICS的默认值,在RCCM命令中维护
         **/
        String COLLATERALCODE = "ALL";
        /**
         * 担保物必须标志(Y:必须,N:非必须)
         */
        String COLLREQUIREDIND = "Y";
        /**
         * 交付类型
         */
        String DELIVTYPE = "DVP";
        /**
         * 保全货币标识
         */
        String PMVIND = "Y";
        /**
         * 置换标识
         */
        String SUBIND = "N";
        /**
         * 禁止现金标识 ,Y-PMTQ不会产生数据
         */
        String SUPPCASHIND = "N";
        /**
         * 禁止确认标识 ,Y-PMTQ不会产生数据
         */
        String SUPPCONFIND = "N";
        /**
         * 禁止费用标识,Y-PMTQ不会产生数据
         */
        String SUPPFEEIND = "N";
        /**
         * 禁止债券变动标识
         */
        String SUPSECMOVEIND = "N";

    }

    /**
     * 互换模块静态参数
     */
    public static interface SWAP {
        /**
         * 标识
         */
        String TAG = "SWAP";
        /**
         * 标识 -冲销 待确认
         */
        String TAG_R = "SWAR";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "ISWH";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_ISWH";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_ISWH_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";
        /**
         * 交易类型I-IRS C-CRS
         */
        String DEALTYPE = "I";
        /************** 交易直通要素 ************************/
        /**
         * 自动复核(Y:自动 N:手工) VERDATE必输
         */
        String VERIND = "Y";
        /**
         * 自动添加清算路径(Y:自动 N:手工 ),必须选择N,
         * 需要填写IDLD表中COMMMEANS、COMMACCT、MATMEANS、MATACCT否则SCHD中SMEANS和SACCT中没值
         */
        String SIIND = "N";
        /**
         * 自动授权 (Y:是 N：否)
         */
        String AUTHSIIND = "N";
        /**
         * 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPPAYIND = "N";
        /**
         * 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPRECIND = "N";
        /**
         * 禁止发确认书 (Y:是 N：否)
         */
        String SUPCONFIND = "N";
        /**
         * 轧差支付标志 (Y:是 N：否)
         */
        String NETPAYIND = "N";
        /**
         * 清算关闭(Y:是 N：否)
         */
        String SETOFF = "";
        /**
         * 理论支付头寸 -表外
         */
        String PAYPOSTNOTIONAL = "N";
        /**
         * 理论收取头寸 -表外
         */
        String RECPOSTNOTIONAL = "N";
    }

    /**
     * 期权 基本信息模块静态参数
     */
    public static interface OTC {
        /**
         * 标识
         */
        String TAG = "OTDE";
        /**
         * 标识 -冲销 待确认
         */
        String TAG_R = "OTDR";
        /**
         * 标识 -期权行权
         */
        String TAG_X = "OTDX";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "IOTD";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_IOTD";
        /**
         * 到期
         */
        String SERVER_EXPR = "OTC-EXPR-OTDX";
        /**
         * 行权
         */
        String SERVER_EXER = "OTC-EXER-OTDX";
        /**
         * 触碰
         */
        String SERVER_INSP = "OTC-INSP-OTDX";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_IOTD_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";
        /************** 交易直通要素 ************************/
        /**
         * 自动复核(Y:自动 N:手工) VERDATE必输
         */
        String VERIND = "Y";
        /**
         * 自动添加清算路径(Y:自动 N:手工 ),必须选择N,
         * 需要填写IDLD表中COMMMEANS、COMMACCT、MATMEANS、MATACCT否则SCHD中SMEANS和SACCT中没值
         */
        String SIIND = "N";
        /**
         * 自动授权 (Y:是 N：否)
         */
        String AUTHSIIND = "N";
        /**
         * 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPPAYIND = "N";
        /**
         * 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据
         */
        String SUPRECIND = "N";
        /**
         * 禁止发确认书 (Y:是 N：否)
         */
        String SUPCONFIND = "N";
    }

    /**
     * 现券基本信息模块静态参数
     */
    public static interface SECUR {
        /**
         * 标识
         */
        String TAG = "SEMM";
        /**
         * 具体接口
         */
        String DETAIL = "ISEC";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_ISEC";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";
        /************** 交易直通要素 ************************/
    }

    /**
     * 交易对手基本信息模块静态参数
     */
    public static interface CUST {
        /**
         * 标识
         */
        String TAG = "CUST";
        /**
         * 具体接口
         */
        String DETAIL = "ICUS";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_ICUS";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";
        /************** 交易直通要素 ************************/
    }

    /**
     * 交易对手清算信息
     */
    public static interface SETTLE {
        /**
         * 标识
         */
        String TAG_CSPI = "CSPI";
        String TAG_CSRI = "CSRI";
        String TAG_SDVP = "SDVP";
        /**
         * 具体接口
         */
        String DETAIL_IINS = "IINS";
        String DETAIL_IDRI = "IDRI";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 服务
         */
        String SERVER_CSPI = "SL_FUND_CSPI";
        String SERVER_CSRI = "SL_FUND_CSRI";
        String SERVER_SDVP = "SL_FUND_SDVP";

    }

    /**
     * 基本信息静态参数
     */
    public static interface BASE {
        /**
         * 机构分支
         */
        String BR = "01";

    }

    /**
     * 现券模块静态参数
     */
    public static interface IMBD {
        /**
         * 标识
         */
        String TAG = "MBDE";
        /**
         * 标识 -冲销
         */
        String TAG_R = "MBDR";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "IMBD";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_IMBD";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_IMBD_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";

        /************** 交易直通要素 ************************/
        /**
         * 1、债券交易需要注意SETTDATE>=DEALDATE 且不能超过三天否则会提示延迟交割
         *
         * 2、SETTDATE>=债券的VDATE否则会错误提示
         **/
        /**
         * 自动复核(Y:自动 N:手工) 需要填写对应的VERDATE,否则不会出PMTQ,
         */
        String VERIND = "Y";
        /**
         * 独立的清算指令(Y:自动,N:手工)选择Y时需要填写CCYSMEANS,CCYSACCT
         */
        String STANDINSTR = "N";
        /**
         * 存续期是否自动赎回,默认N
         */
        String COUPREINVEST = "N";

    }

    /**
     * 费用模块静态参数
     */
    public static interface FEES {
        /**
         * 标识
         */
        String TAG = "FEES";
        /**
         * 标识 -冲销
         */
        String TAG_R = "FEER";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "IFEE";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_IFEE";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_IFEE_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";

        /************** 交易直通要素 ************************/
        /**
         * 1、债券交易需要注意SETTDATE>=DEALDATE 且不能超过三天否则会提示延迟交割
         *
         * 2、SETTDATE>=债券的VDATE否则会错误提示
         **/
        /**
         * 自动复核(Y:自动 N:手工) 需要填写对应的VERDATE,否则不会出PMTQ,
         */
        String VERIND = "Y";
        /**
         * 独立的清算指令(Y:自动,N:手工)选择Y时需要填写CCYSMEANS,CCYSACCT
         */
        String STANDINSTR = "N";
        /**
         * 存续期是否自动赎回,默认N
         */
        String COUPREINVEST = "N";
    }

    /**
     * IREV模块静态参数
     */
    public static interface IREV {
        /**
         * 标识
         */
        String TAG = "REVR";
        /**
         * 具体接口
         */
        String DETAIL = "IREV";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_REVR";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "1";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
    }

    /**
     * IRHS模块静态参数
     */
    public static interface IRHS {
        /**
         * 标识
         */
        String TAG = "RHIS";
        /**
         * 具体接口
         */
        String DETAIL = "IRHS";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_RHIS";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "1";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
    }

    /**
     * IVOL模块静态参数
     */
    public static interface IVOL {
        /**
         * 标识
         */
        String TAG = "VOLD";
        /**
         * 具体接口
         */
        String DETAIL = "IVOL";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_VOLD";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "1";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
    }

    /**
     * IYCR模块静态参数
     */
    public static interface IYCR {
        /**
         * 标识
         */
        String TAG = "YCRT";
        /**
         * 具体接口
         */
        String DETAIL = "IYCR";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_YCRT";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "1";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
    }

    /**
     * ISEL債券收盤價模块静态参数
     */

    public static interface ISEL {
        /**
         * 标识
         */
        String TAG = "SMCL";
        /**
         * 具体接口
         */
        String DETAIL = "ISEL";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_SECL";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "1";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
    }

    /**
     * 头寸调拨
     */
    public static interface NTRF {
        /**
         * 标识
         */
        String TAG = "NTRF";
        /**
         * 标识 -冲销
         */
        String TAG_R = "NTRR";
        /**
         * 具体接口(适用于冲销)
         */
        String DETAIL = "INTR";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_INTR";
        /**
         * 服务 -冲销
         */
        String SERVER_R = "SL_FUND_INTR_R";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";

        /************** 交易直通要素 ************************/
        /**
         * 自动复核(Y:自动 N:手工)
         */
        String VERIND = "Y";
        /**
         * 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据 默认选择N 否则不出
         */
        String SUPPAYIND = "N";

    }

    /**
     * INTR静态参数
     */
    public static interface INTR {
        /**
         * 标识
         */
        String TAG = "NTRF";
        /**
         * 具体接口
         */
        String DETAIL = "INTR";
        /**
         * 服务
         */
        String SERVER = "SL_FUND_INTR";
        /**
         * 导入顺序
         */
        String SEQ = "0";
        /**
         * 输入输出标识
         */
        String INOUTIND = "I";
        /**
         * 处理优先级
         */
        String PRIORITY = "7";
        /**
         * 初始化状态
         */
        String STATCODE = "-1";
        /**
         * 数据来源
         */
        String SYST = "SING";
        /**
         * 默认经办人员
         */
        String IOPER = "SYS1";
        /**
         * 默认冲销原因
         */
        String REVREASON = "99";

    }
}
