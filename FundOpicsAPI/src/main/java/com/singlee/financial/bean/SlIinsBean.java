package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * CSPI CSRI 添加清算路径
 * 
 * @author shenzl
 *
 */
public class SlIinsBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3778222009086962857L;

	private SlInthBean inthBean;

	private String dealno;
	private String dealseq;
	private String product;
	private String prodtype;
	private String usualid;
	private String recordtype;
	private String payrecind;
	private String settmeans;
	private String settaccount;
	private String pcc;
	private String acctno;
	private String bic;
	private String p1;
	private String p2;
	private String p3;
	private String p4;
	private String det;
	private String r1;
	private String r2;
	private String r3;
	private String r4;
	private String r5;
	private String r6;
	private String bif;
	private String authind;
	private String cnarr;
	private String suprecind;
	private String suppayind;
	private String supconfind;
	private String freeformat;
	private Date lstmntdte;
	private String ccy;
	private String cno;
	private Date effdate;
	private String addupdelind;
	private String defaultind;
	private String ioper;
	private Date inputdate;
	private String inputtime;
	private String veroper;
	private Date verdate;
	private String vertime;
	private String swiftmtind;

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getDealseq() {
		return dealseq;
	}

	public void setDealseq(String dealseq) {
		this.dealseq = dealseq;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getUsualid() {
		return usualid;
	}

	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}

	public String getRecordtype() {
		return recordtype;
	}

	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettaccount() {
		return settaccount;
	}

	public void setSettaccount(String settaccount) {
		this.settaccount = settaccount;
	}

	public String getPcc() {
		return pcc;
	}

	public void setPcc(String pcc) {
		this.pcc = pcc;
	}

	public String getAcctno() {
		return acctno;
	}

	public void setAcctno(String acctno) {
		this.acctno = acctno;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getP1() {
		return p1;
	}

	public void setP1(String p1) {
		this.p1 = p1;
	}

	public String getP2() {
		return p2;
	}

	public void setP2(String p2) {
		this.p2 = p2;
	}

	public String getP3() {
		return p3;
	}

	public void setP3(String p3) {
		this.p3 = p3;
	}

	public String getP4() {
		return p4;
	}

	public void setP4(String p4) {
		this.p4 = p4;
	}

	public String getDet() {
		return det;
	}

	public void setDet(String det) {
		this.det = det;
	}

	public String getR1() {
		return r1;
	}

	public void setR1(String r1) {
		this.r1 = r1;
	}

	public String getR2() {
		return r2;
	}

	public void setR2(String r2) {
		this.r2 = r2;
	}

	public String getR3() {
		return r3;
	}

	public void setR3(String r3) {
		this.r3 = r3;
	}

	public String getR4() {
		return r4;
	}

	public void setR4(String r4) {
		this.r4 = r4;
	}

	public String getR5() {
		return r5;
	}

	public void setR5(String r5) {
		this.r5 = r5;
	}

	public String getR6() {
		return r6;
	}

	public void setR6(String r6) {
		this.r6 = r6;
	}

	public String getBif() {
		return bif;
	}

	public void setBif(String bif) {
		this.bif = bif;
	}

	public String getAuthind() {
		return authind;
	}

	public void setAuthind(String authind) {
		this.authind = authind;
	}

	public String getCnarr() {
		return cnarr;
	}

	public void setCnarr(String cnarr) {
		this.cnarr = cnarr;
	}

	public String getSuprecind() {
		return suprecind;
	}

	public void setSuprecind(String suprecind) {
		this.suprecind = suprecind;
	}

	public String getSuppayind() {
		return suppayind;
	}

	public void setSuppayind(String suppayind) {
		this.suppayind = suppayind;
	}

	public String getSupconfind() {
		return supconfind;
	}

	public void setSupconfind(String supconfind) {
		this.supconfind = supconfind;
	}

	public String getFreeformat() {
		return freeformat;
	}

	public void setFreeformat(String freeformat) {
		this.freeformat = freeformat;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public Date getEffdate() {
		return effdate;
	}

	public void setEffdate(Date effdate) {
		this.effdate = effdate;
	}

	public String getAddupdelind() {
		return addupdelind;
	}

	public void setAddupdelind(String addupdelind) {
		this.addupdelind = addupdelind;
	}

	public String getDefaultind() {
		return defaultind;
	}

	public void setDefaultind(String defaultind) {
		this.defaultind = defaultind;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public Date getInputdate() {
		return inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public String getVeroper() {
		return veroper;
	}

	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public String getVertime() {
		return vertime;
	}

	public void setVertime(String vertime) {
		this.vertime = vertime;
	}

	public String getSwiftmtind() {
		return swiftmtind;
	}

	public void setSwiftmtind(String swiftmtind) {
		this.swiftmtind = swiftmtind;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SlIinsBean() {

	}

	/**
	 * 债券基本信息导入始化
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 		1、交易导入 TAG="CSPI/CSRI" , DETAIL="IINS"
	 * 
	 * 
	 */
	public SlIinsBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setInoutind(SlDealModule.SETTLE.INOUTIND);
			inthBean.setPriority(SlDealModule.SETTLE.PRIORITY);
			inthBean.setStatcode(SlDealModule.SETTLE.STATCODE);
			inthBean.setSyst(SlDealModule.SETTLE.SYST);
			inthBean.setIoper(SlDealModule.SETTLE.IOPER);
		}
	}
}
