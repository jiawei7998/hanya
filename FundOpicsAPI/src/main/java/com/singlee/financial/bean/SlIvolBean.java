package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class SlIvolBean implements Serializable{

	/**
	 * IVOL
	 */
	private static final long serialVersionUID = -1769020465434545861L;
	private SlInthBean inthBean;
	private String br;//部门
	private String server;
	private String fedealno;
	private String seq;
	private String inoutind;
	private String optiontype;
	private String optiontypekey;
	private String desmat;
	private BigDecimal strikeprice8;
	private BigDecimal callvolatility8;
	private BigDecimal putvolatility8;
	private String  atmoneyind;
	private String term;
	private BigDecimal callbidvolatility8;
	private BigDecimal putbidvolatility8;
	private BigDecimal callaskvolatility8;
	private BigDecimal putaskvolatility8;
	private BigDecimal riskrev8;
	private BigDecimal stranglemgn8;
	
	//市场数据查询所需字段
	private String feidate;
	private String statcode;
	private String errorcode;
	
	
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFedealno() {
		return fedealno;
	}
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getInoutind() {
		return inoutind;
	}
	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}
	public String getOptiontype() {
		return optiontype;
	}
	public void setOptiontype(String optiontype) {
		this.optiontype = optiontype;
	}
	public String getOptiontypekey() {
		return optiontypekey;
	}
	public void setOptiontypekey(String optiontypekey) {
		this.optiontypekey = optiontypekey;
	}
	public String getDesmat() {
		return desmat;
	}
	public void setDesmat(String desmat) {
		this.desmat = desmat;
	}
	public String getAtmoneyind() {
		return atmoneyind;
	}
	public void setAtmoneyind(String atmoneyind) {
		this.atmoneyind = atmoneyind;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getFeidate() {
		return feidate;
	}
	public void setFeidate(String feidate) {
		this.feidate = feidate;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	
	public BigDecimal getStrikeprice8() {
		return strikeprice8;
	}
	public void setStrikeprice8(BigDecimal strikeprice8) {
		this.strikeprice8 = strikeprice8;
	}
	public BigDecimal getCallvolatility8() {
		return callvolatility8;
	}
	public void setCallvolatility8(BigDecimal callvolatility8) {
		this.callvolatility8 = callvolatility8;
	}
	public BigDecimal getPutvolatility8() {
		return putvolatility8;
	}
	public void setPutvolatility8(BigDecimal putvolatility8) {
		this.putvolatility8 = putvolatility8;
	}
	public BigDecimal getCallbidvolatility8() {
		return callbidvolatility8;
	}
	public void setCallbidvolatility8(BigDecimal callbidvolatility8) {
		this.callbidvolatility8 = callbidvolatility8;
	}
	public BigDecimal getPutbidvolatility8() {
		return putbidvolatility8;
	}
	public void setPutbidvolatility8(BigDecimal putbidvolatility8) {
		this.putbidvolatility8 = putbidvolatility8;
	}
	public BigDecimal getCallaskvolatility8() {
		return callaskvolatility8;
	}
	public void setCallaskvolatility8(BigDecimal callaskvolatility8) {
		this.callaskvolatility8 = callaskvolatility8;
	}
	public BigDecimal getPutaskvolatility8() {
		return putaskvolatility8;
	}
	public void setPutaskvolatility8(BigDecimal putaskvolatility8) {
		this.putaskvolatility8 = putaskvolatility8;
	}
	public BigDecimal getRiskrev8() {
		return riskrev8;
	}
	public void setRiskrev8(BigDecimal riskrev8) {
		this.riskrev8 = riskrev8;
	}
	public BigDecimal getStranglemgn8() {
		return stranglemgn8;
	}
	public void setStranglemgn8(BigDecimal stranglemgn8) {
		this.stranglemgn8 = stranglemgn8;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIvolBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); 
			inthBean.setBr(br);
			inthBean.setServer(SlDealModule.IVOL.SERVER);
			inthBean.setSeq(SlDealModule.IVOL.SEQ);
			inthBean.setInoutind(SlDealModule.IVOL.INOUTIND);
			inthBean.setTag(SlDealModule.IVOL.TAG);
			inthBean.setDetail(SlDealModule.IVOL.DETAIL);
			inthBean.setPriority(SlDealModule.IVOL.PRIORITY);
			inthBean.setStatcode(SlDealModule.IVOL.STATCODE);
			inthBean.setSyst(SlDealModule.IVOL.SYST);
			inthBean.setIoper(SlDealModule.IVOL.IOPER);
		}
   }
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SlIvolBean [inthBean=");
		builder.append(inthBean);
		builder.append(", br=");
		builder.append(br);
		builder.append(", server=");
		builder.append(server);
		builder.append(", fedealno=");
		builder.append(fedealno);
		builder.append(", seq=");
		builder.append(seq);
		builder.append(", inoutind=");
		builder.append(inoutind);
		builder.append(", optiontype=");
		builder.append(optiontype);
		builder.append(", optiontypekey=");
		builder.append(optiontypekey);
		builder.append(", desmat=");
		builder.append(desmat);
		builder.append(", strikeprice8=");
		builder.append(strikeprice8);
		builder.append(", callvolatility8=");
		builder.append(callvolatility8);
		builder.append(", putvolatility8=");
		builder.append(putvolatility8);
		builder.append(", atmoneyind=");
		builder.append(atmoneyind);
		builder.append(", term=");
		builder.append(term);
		builder.append(", callbidvolatility8=");
		builder.append(callbidvolatility8);
		builder.append(", putbidvolatility8=");
		builder.append(putbidvolatility8);
		builder.append(", callaskvolatility8=");
		builder.append(callaskvolatility8);
		builder.append(", putaskvolatility8=");
		builder.append(putaskvolatility8);
		builder.append(", riskrev8=");
		builder.append(riskrev8);
		builder.append(", stranglemgn8=");
		builder.append(stranglemgn8);
		builder.append(", feidate=");
		builder.append(feidate);
		builder.append(", statcode=");
		builder.append(statcode);
		builder.append(", errorcode=");
		builder.append(errorcode);
		builder.append("]");
		return builder.toString();
	}
	
}
