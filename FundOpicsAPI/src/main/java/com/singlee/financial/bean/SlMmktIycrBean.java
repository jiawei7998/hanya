package com.singlee.financial.bean;

public class SlMmktIycrBean extends SlMarketDataBaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = -692396847939791320L;
	protected String CCY;
	  protected String MIDRATE;
	  protected String TENOR;
	  protected String CURVEID;
	  protected String TYPE;
	  protected String SECID;
	public String getCCY() {
		return CCY;
	}
	public void setCCY(String cCY) {
		CCY = cCY;
	}
	public String getMIDRATE() {
		return MIDRATE;
	}
	public void setMIDRATE(String mIDRATE) {
		MIDRATE = mIDRATE;
	}
	public String getTENOR() {
		return TENOR;
	}
	public void setTENOR(String tENOR) {
		TENOR = tENOR;
	}
	public String getCURVEID() {
		return CURVEID;
	}
	public void setCURVEID(String cURVEID) {
		CURVEID = cURVEID;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getSECID() {
		return SECID;
	}
	public void setSECID(String sECID) {
		SECID = sECID;
	}

}
