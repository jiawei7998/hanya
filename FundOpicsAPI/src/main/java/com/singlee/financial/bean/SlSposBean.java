/**
 * Project Name:FundOpicsAPI
 * File Name:SlSposBean.java
 * Package Name:com.singlee.financial.bean
 * Date:2018-9-21下午04:02:49
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * ClassName:SlSposBean <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2018-9-21 下午04:02:49 <br/>
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SlSposBean implements Serializable{

	private static final long serialVersionUID = 1617452002885851010L;
	
	private String br;
	private String ccy;
	private String port;
	private String secid;
	private String invtype;
	private String cost;
	private BigDecimal amt;
	private BigDecimal totalamt;
	
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public void setTotalamt(BigDecimal totalamt) {
		this.totalamt = totalamt;
	}
	public BigDecimal getTotalamt() {
		return totalamt;
	}
	

}

