/**
 * Project Name:FundOpicsAPI
 * File Name:SlSposBean.java
 * Package Name:com.singlee.financial.bean
 * Date:2018-9-21下午04:02:49
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/


package com.singlee.financial.bean;


import java.io.Serializable;


/**
 * 债券和存单持仓盈亏统计表
 * 
 */
public class SlSdCdProfitAndLossBean implements Serializable{


     private static final long serialVersionUID = 1617452002885851010L;
     /**
      * 投资类型
      */
     private String invtype;
     /**
      * 债券类型
      */
     private String acctngtype;
     /**
      * 当前名义本金
      */
     private String prinamt;
     /**
      * 年初名义本金
      */
     private String yearprinamt;
     /**
      * 当前账面价值
      */
     private String tdbookvalue;
     /**
      * 年初账面价值
      */
     private String yearbookvalue;
     /**
      * 当前久期
      */
     private String duration;
     /**
      * 年初久期
      */
     private String yearduation;
     /**
      * 当前持有到期收益率
      */
     private String rate;
     /**
      * 年初持有到期收益率
      */
     private String yearrate;
     /**
      * 当前估值损益
      */
     private String diffamt;
     /**
      * 年初估值损益
      */
     private String yeardiffamt;
     /**
      * 日损益
      */
     private String dayprofitloss;
     /**
      * 月损益
      */
     private String monthprofitloss;
     /**
      * 年损益
      */
     private String yearprofitloss;
     /**
      * 当前DV01
      */
     private String dv01;
     /**
      * 年初DV01
      */
     private String yeardv01;
     
     
     public String getInvtype() {
          return invtype;
     }
     public void setInvtype(String invtype) {
          this.invtype = invtype;
     }
     public String getAcctngtype() {
          return acctngtype;
     }
     public void setAcctngtype(String acctngtype) {
          this.acctngtype = acctngtype;
     }
     public String getPrinamt() {
          return prinamt;
     }
     public void setPrinamt(String prinamt) {
          this.prinamt = prinamt;
     }
     public String getYearprinamt() {
          return yearprinamt;
     }
     public void setYearprinamt(String yearprinamt) {
          this.yearprinamt = yearprinamt;
     }
     public String getTdbookvalue() {
          return tdbookvalue;
     }
     public void setTdbookvalue(String tdbookvalue) {
          this.tdbookvalue = tdbookvalue;
     }
     public String getYearbookvalue() {
          return yearbookvalue;
     }
     public void setYearbookvalue(String yearbookvalue) {
          this.yearbookvalue = yearbookvalue;
     }
     public String getDuration() {
          return duration;
     }
     public void setDuration(String duration) {
          this.duration = duration;
     }
     public String getYearduation() {
          return yearduation;
     }
     public void setYearduation(String yearduation) {
          this.yearduation = yearduation;
     }
     public String getRate() {
          return rate;
     }
     public void setRate(String rate) {
          this.rate = rate;
     }
     public String getYearrate() {
          return yearrate;
     }
     public void setYearrate(String yearrate) {
          this.yearrate = yearrate;
     }
     public String getDiffamt() {
          return diffamt;
     }
     public void setDiffamt(String diffamt) {
          this.diffamt = diffamt;
     }
     public String getYeardiffamt() {
          return yeardiffamt;
     }
     public void setYeardiffamt(String yeardiffamt) {
          this.yeardiffamt = yeardiffamt;
     }
     public String getDayprofitloss() {
          return dayprofitloss;
     }
     public void setDayprofitloss(String dayprofitloss) {
          this.dayprofitloss = dayprofitloss;
     }
     public String getMonthprofitloss() {
          return monthprofitloss;
     }
     public void setMonthprofitloss(String monthprofitloss) {
          this.monthprofitloss = monthprofitloss;
     }
     public String getYearprofitloss() {
          return yearprofitloss;
     }
     public void setYearprofitloss(String yearprofitloss) {
          this.yearprofitloss = yearprofitloss;
     }
     public String getDv01() {
          return dv01;
     }
     public void setDv01(String dv01) {
          this.dv01 = dv01;
     }
     public String getYeardv01() {
          return yeardv01;
     }
     public void setYeardv01(String yeardv01) {
          this.yeardv01 = yeardv01;
     }
     
}