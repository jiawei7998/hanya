/**
 * Project Name:FundOpicsAPI
 * File Name:SlSposBean.java
 * Package Name:com.singlee.financial.bean
 * Date:2018-9-21下午04:02:49
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 同业存单发行每日余额清单
 * 
 */
public class SlCdIssueBalanceBean implements Serializable{

	private static final long serialVersionUID = 1617452002885851010L;
	/**
	 * 数据日期
	 */
	private String datadate;  
	/**
	 * 机构号
	 */
	private String inst;  
	/**
	 * 存单代码
	 */
	private String secid;   
	/**
	 * 存单名称
	 */
	private String secidnm;  
	/**
	 * 认购人名称
	 */
	private String subname;  
	/**
	 * 机构类型
	 */
	private String insttype;   
	/**
	 * 产品性质
	 */
	private String prodtype;    
	/**
	 * 产品分类
	 */
	private String product;   
	/**
	 * 账面余额
	 */
	private String bookbalance;  
	/**
	 * 起息日
	 */
	private String vdate;   
	/**
	 * 到期日
	 */
	private String mdate;  
	/**
	 * 面值
	 */
	private String facevalue;    
	/**
	 * 利息调整
	 */
	private String interestadj;
	/**
	 * 核心科目号
	 */
	private String subjectno;
	
	public String getDatadate() {
		return datadate;
	}
	public void setDatadate(String datadate) {
		this.datadate = datadate;
	}
	public String getInst() {
		return inst;
	}
	public void setInst(String inst) {
		this.inst = inst;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getSecidnm() {
		return secidnm;
	}
	public void setSecidnm(String secidnm) {
		this.secidnm = secidnm;
	}
	public String getSubname() {
		return subname;
	}
	public void setSubname(String subname) {
		this.subname = subname;
	}
	public String getInsttype() {
		return insttype;
	}
	public void setInsttype(String insttype) {
		this.insttype = insttype;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getBookbalance() {
		return bookbalance;
	}
	public void setBookbalance(String bookbalance) {
		this.bookbalance = bookbalance;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getFacevalue() {
		return facevalue;
	}
	public void setFacevalue(String facevalue) {
		this.facevalue = facevalue;
	}
	public String getInterestadj() {
		return interestadj;
	}
	public void setInterestadj(String interestadj) {
		this.interestadj = interestadj;
	}
	public void setSubjectno(String subjectno) {
		this.subjectno = subjectno;
	}
	public String getSubjectno() {
		return subjectno;
	}   
	

}

