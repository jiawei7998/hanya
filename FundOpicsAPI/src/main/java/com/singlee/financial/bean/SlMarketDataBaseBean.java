package com.singlee.financial.bean;


import java.io.Serializable;
import java.util.Date;

public class SlMarketDataBaseBean
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  protected String FEDEALNO;
  protected String BR;
  protected Date BRANPRCDATE;
  protected int IMPSEQ;
  protected int LINENUM;
  protected String STATUS;
  protected String ERRORMSG;
  protected String INPUTOPR;
  protected Date INPUTTIME;
  protected String INPUTOPICSOPR;
  protected Date INPUTOPICSTIME;
  
  public String getFEDEALNO()
  {
    return this.FEDEALNO;
  }
  
  public void setFEDEALNO(String fEDEALNO)
  {
    this.FEDEALNO = fEDEALNO;
  }
  
  public String getBR()
  {
    return this.BR;
  }
  
  public void setBR(String bR)
  {
    this.BR = bR;
  }
  
  public Date getBRANPRCDATE()
  {
    return this.BRANPRCDATE;
  }
  
  public void setBRANPRCDATE(Date bRANPRCDATE)
  {
    this.BRANPRCDATE = bRANPRCDATE;
  }
  
  public int getIMPSEQ()
  {
    return this.IMPSEQ;
  }
  
  public void setIMPSEQ(int iMPSEQ)
  {
    this.IMPSEQ = iMPSEQ;
  }
  
  
  
  public int getLINENUM() {
	return LINENUM;
}

public void setLINENUM(int lINENUM) {
	LINENUM = lINENUM;
}

public String getSTATUS()
  {
    return this.STATUS;
  }
  
  public void setSTATUS(String sTATUS)
  {
    this.STATUS = sTATUS;
  }
  
  public String getERRORMSG()
  {
    return this.ERRORMSG;
  }
  
  public void setERRORMSG(String eRRORMSG)
  {
    this.ERRORMSG = eRRORMSG;
  }
  
  public String getINPUTOPR()
  {
    return this.INPUTOPR;
  }
  
  public void setINPUTOPR(String iNPUTOPR)
  {
    this.INPUTOPR = iNPUTOPR;
  }
  
  public Date getINPUTTIME()
  {
    return this.INPUTTIME;
  }
  
  public void setINPUTTIME(Date iNPUTTIME)
  {
    this.INPUTTIME = iNPUTTIME;
  }
  
  public String getINPUTOPICSOPR()
  {
    return this.INPUTOPICSOPR;
  }
  
  public void setINPUTOPICSOPR(String iNPUTOPICSOPR)
  {
    this.INPUTOPICSOPR = iNPUTOPICSOPR;
  }
  
  public Date getINPUTOPICSTIME()
  {
    return this.INPUTOPICSTIME;
  }
  
  public void setINPUTOPICSTIME(Date iNPUTOPICSTIME)
  {
    this.INPUTOPICSTIME = iNPUTOPICSTIME;
  }
}
