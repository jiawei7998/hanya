/**
 * Project Name:FundOpicsAPI
 * File Name:SlSpshBean.java
 * Package Name:com.singlee.financial.bean
 * Date:2018-10-8下午06:08:59
 * Copyright (c) 2018; chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.bean;

import java.util.Date;

import com.singlee.financial.pojo.FixedIncomeBean;

/**
 * 
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class SlImbdBean extends FixedIncomeBean{

	
	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = 7656616940328563316L;
	private SlInthBean inthBean;
	/**
	 * OPICS拓展属性
	 */
	private SlExternalBean externalBean;
	
	private String  revdealno;
	//private String  cno;
	private String  secsacct;
	private String  invtype;
	private String  origamt;
	//private String  trad;
	//private String  secid;
	private String  origqty;
	//private String  price_8;
	//private String  settdate;
	private String  commamt;
	private String  feeamt;
	private String  vatamt;
	private String  brokfeeamt;
	private String  spreadrate_8;
	private String  ratecode;
	private String  delaydelivind;
	private String  exchind;
	private Date  verdate;
	private String  standinstr;
	private Date  authdate;
	private String  authind;
	private String  revreason;
	private String  revtext;
	private String  convintamt;
	private String  convintbamt;
	private String  settamt;
	private String  settbaseamt;
	private String  costamt;
	private String  costbamt;
	private String  whtamt;
	private String  intccy;
	private String  settccy;
	private String  ccysmeans  ;
	private String  ccysacct  ;
	private String settprocexchrate_8; 
	private String settprocterms;
	private String settprocpremdisc_8;
	private String intsettexchrate_8;
	private String intsettterms;
	private String intsettpremdisc_8;
	private String settbaseexchrate_8;
	private String settbaseterms;
	private String settbasepremdisc_8;
	private String intbaseexchrate_8;
	private String intbaseterms;
	private String intbasepremdisc_8;
	private String benbic;
	private String ben1;
	private String ben2;
	private String ben3;
	private String ben4;
	private String recdelbic;
	private String recdel1;
	private String recdel2;
	private String recdel3;
	private String recdel4;
	private String overridewxtaxind;
	private String strategyid;
	private String presmonval;
	private String amendind;
	private String rfudate1;
	private String rfudate2;
	private String rfuchar1;
	private String rfuchar2;
	private String rfunum1;
	private String rfunum2;
	private String paymentholdind;
	
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public String getRevdealno() {
		return revdealno;
	}
	public void setRevdealno(String revdealno) {
		this.revdealno = revdealno;
	}
	public String getSecsacct() {
		return secsacct;
	}
	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getOrigamt() {
		return origamt;
	}
	public void setOrigamt(String origamt) {
		this.origamt = origamt;
	}
	public String getOrigqty() {
		return origqty;
	}
	public void setOrigqty(String origqty) {
		this.origqty = origqty;
	}
	public String getCommamt() {
		return commamt;
	}
	public void setCommamt(String commamt) {
		this.commamt = commamt;
	}
	public String getFeeamt() {
		return feeamt;
	}
	public void setFeeamt(String feeamt) {
		this.feeamt = feeamt;
	}
	public String getVatamt() {
		return vatamt;
	}
	public void setVatamt(String vatamt) {
		this.vatamt = vatamt;
	}
	public String getBrokfeeamt() {
		return brokfeeamt;
	}
	public void setBrokfeeamt(String brokfeeamt) {
		this.brokfeeamt = brokfeeamt;
	}
	public String getSpreadrate_8() {
		return spreadrate_8;
	}
	public void setSpreadrate_8(String spreadrate_8) {
		this.spreadrate_8 = spreadrate_8;
	}
	public String getRatecode() {
		return ratecode;
	}
	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}
	public String getDelaydelivind() {
		return delaydelivind;
	}
	public void setDelaydelivind(String delaydelivind) {
		this.delaydelivind = delaydelivind;
	}
	public String getExchind() {
		return exchind;
	}
	public void setExchind(String exchind) {
		this.exchind = exchind;
	}
	public Date getVerdate() {
		return verdate;
	}
	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}
	public String getStandinstr() {
		return standinstr;
	}
	public void setStandinstr(String standinstr) {
		this.standinstr = standinstr;
	}
	public Date getAuthdate() {
		return authdate;
	}
	public void setAuthdate(Date authdate) {
		this.authdate = authdate;
	}
	public String getAuthind() {
		return authind;
	}
	public void setAuthind(String authind) {
		this.authind = authind;
	}
	public String getRevreason() {
		return revreason;
	}
	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}
	public String getRevtext() {
		return revtext;
	}
	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}
	public String getConvintamt() {
		return convintamt;
	}
	public void setConvintamt(String convintamt) {
		this.convintamt = convintamt;
	}
	public String getConvintbamt() {
		return convintbamt;
	}
	public void setConvintbamt(String convintbamt) {
		this.convintbamt = convintbamt;
	}
	public String getSettamt() {
		return settamt;
	}
	public void setSettamt(String settamt) {
		this.settamt = settamt;
	}
	public String getSettbaseamt() {
		return settbaseamt;
	}
	public void setSettbaseamt(String settbaseamt) {
		this.settbaseamt = settbaseamt;
	}
	public String getCostamt() {
		return costamt;
	}
	public void setCostamt(String costamt) {
		this.costamt = costamt;
	}
	public String getCostbamt() {
		return costbamt;
	}
	public void setCostbamt(String costbamt) {
		this.costbamt = costbamt;
	}
	public String getWhtamt() {
		return whtamt;
	}
	public void setWhtamt(String whtamt) {
		this.whtamt = whtamt;
	}
	public String getIntccy() {
		return intccy;
	}
	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}
	public String getSettccy() {
		return settccy;
	}
	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}
	public String getCcysmeans() {
		return ccysmeans;
	}
	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans;
	}
	public String getCcysacct() {
		return ccysacct;
	}
	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}
	
	public String getSettprocexchrate_8() {
		return settprocexchrate_8;
	}
	public void setSettprocexchrate_8(String settprocexchrate_8) {
		this.settprocexchrate_8 = settprocexchrate_8;
	}
	public String getSettprocterms() {
		return settprocterms;
	}
	public void setSettprocterms(String settprocterms) {
		this.settprocterms = settprocterms;
	}
	public String getSettprocpremdisc_8() {
		return settprocpremdisc_8;
	}
	public void setSettprocpremdisc_8(String settprocpremdisc_8) {
		this.settprocpremdisc_8 = settprocpremdisc_8;
	}
	public String getIntsettexchrate_8() {
		return intsettexchrate_8;
	}
	public void setIntsettexchrate_8(String intsettexchrate_8) {
		this.intsettexchrate_8 = intsettexchrate_8;
	}
	public String getIntsettterms() {
		return intsettterms;
	}
	public void setIntsettterms(String intsettterms) {
		this.intsettterms = intsettterms;
	}
	public String getIntsettpremdisc_8() {
		return intsettpremdisc_8;
	}
	public void setIntsettpremdisc_8(String intsettpremdisc_8) {
		this.intsettpremdisc_8 = intsettpremdisc_8;
	}
	public String getSettbaseexchrate_8() {
		return settbaseexchrate_8;
	}
	public void setSettbaseexchrate_8(String settbaseexchrate_8) {
		this.settbaseexchrate_8 = settbaseexchrate_8;
	}
	public String getSettbaseterms() {
		return settbaseterms;
	}
	public void setSettbaseterms(String settbaseterms) {
		this.settbaseterms = settbaseterms;
	}
	public String getSettbasepremdisc_8() {
		return settbasepremdisc_8;
	}
	public void setSettbasepremdisc_8(String settbasepremdisc_8) {
		this.settbasepremdisc_8 = settbasepremdisc_8;
	}
	public String getIntbaseexchrate_8() {
		return intbaseexchrate_8;
	}
	public void setIntbaseexchrate_8(String intbaseexchrate_8) {
		this.intbaseexchrate_8 = intbaseexchrate_8;
	}
	public String getIntbaseterms() {
		return intbaseterms;
	}
	public void setIntbaseterms(String intbaseterms) {
		this.intbaseterms = intbaseterms;
	}
	public String getIntbasepremdisc_8() {
		return intbasepremdisc_8;
	}
	public void setIntbasepremdisc_8(String intbasepremdisc_8) {
		this.intbasepremdisc_8 = intbasepremdisc_8;
	}
	public String getBenbic() {
		return benbic;
	}
	public void setBenbic(String benbic) {
		this.benbic = benbic;
	}
	public String getBen1() {
		return ben1;
	}
	public void setBen1(String ben1) {
		this.ben1 = ben1;
	}
	public String getBen2() {
		return ben2;
	}
	public void setBen2(String ben2) {
		this.ben2 = ben2;
	}
	public String getBen3() {
		return ben3;
	}
	public void setBen3(String ben3) {
		this.ben3 = ben3;
	}
	public String getBen4() {
		return ben4;
	}
	public void setBen4(String ben4) {
		this.ben4 = ben4;
	}
	public String getRecdelbic() {
		return recdelbic;
	}
	public void setRecdelbic(String recdelbic) {
		this.recdelbic = recdelbic;
	}
	public String getRecdel1() {
		return recdel1;
	}
	public void setRecdel1(String recdel1) {
		this.recdel1 = recdel1;
	}
	public String getRecdel2() {
		return recdel2;
	}
	public void setRecdel2(String recdel2) {
		this.recdel2 = recdel2;
	}
	public String getRecdel3() {
		return recdel3;
	}
	public void setRecdel3(String recdel3) {
		this.recdel3 = recdel3;
	}
	public String getRecdel4() {
		return recdel4;
	}
	public void setRecdel4(String recdel4) {
		this.recdel4 = recdel4;
	}
	public String getOverridewxtaxind() {
		return overridewxtaxind;
	}
	public void setOverridewxtaxind(String overridewxtaxind) {
		this.overridewxtaxind = overridewxtaxind;
	}
	public String getStrategyid() {
		return strategyid;
	}
	public void setStrategyid(String strategyid) {
		this.strategyid = strategyid;
	}
	public String getPresmonval() {
		return presmonval;
	}
	public void setPresmonval(String presmonval) {
		this.presmonval = presmonval;
	}
	public String getAmendind() {
		return amendind;
	}
	public void setAmendind(String amendind) {
		this.amendind = amendind;
	}
	public String getRfudate1() {
		return rfudate1;
	}
	public void setRfudate1(String rfudate1) {
		this.rfudate1 = rfudate1;
	}
	public String getRfudate2() {
		return rfudate2;
	}
	public void setRfudate2(String rfudate2) {
		this.rfudate2 = rfudate2;
	}
	public String getRfuchar1() {
		return rfuchar1;
	}
	public void setRfuchar1(String rfuchar1) {
		this.rfuchar1 = rfuchar1;
	}
	public String getRfuchar2() {
		return rfuchar2;
	}
	public void setRfuchar2(String rfuchar2) {
		this.rfuchar2 = rfuchar2;
	}
	public String getRfunum1() {
		return rfunum1;
	}
	public void setRfunum1(String rfunum1) {
		this.rfunum1 = rfunum1;
	}
	public String getRfunum2() {
		return rfunum2;
	}
	public void setRfunum2(String rfunum2) {
		this.rfunum2 = rfunum2;
	}
	public String getPaymentholdind() {
		return paymentholdind;
	}
	public void setPaymentholdind(String paymentholdind) {
		this.paymentholdind = paymentholdind;
	}
	/**
	 * MBS债券
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 		1、交易导入 TAG="MBDE" , DETAIL="IMBD"
	 * 
	 * 
	 */
	public SlImbdBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean();
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setInoutind(SlDealModule.IMBD.INOUTIND);
			inthBean.setPriority(SlDealModule.IMBD.PRIORITY);
			inthBean.setStatcode(SlDealModule.IMBD.STATCODE);
			inthBean.setSyst(SlDealModule.IMBD.SYST);
			inthBean.setIoper(SlDealModule.IMBD.IOPER);
		}
	}
	public void setExternalBean(SlExternalBean externalBean) {
		this.externalBean = externalBean;
	}
	public SlExternalBean getExternalBean() {
		return externalBean;
	}

}

