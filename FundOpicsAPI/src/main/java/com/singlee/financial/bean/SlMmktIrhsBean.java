package com.singlee.financial.bean;

public class SlMmktIrhsBean extends SlMarketDataBaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7013708867946656422L;
	protected String CCY;
	  protected String MIDRATE;
	  protected String TENOR;
	  protected String RATECODE;
	  protected String TYPE;
	public String getCCY() {
		return CCY;
	}
	public void setCCY(String cCY) {
		CCY = cCY;
	}
	public String getMIDRATE() {
		return MIDRATE;
	}
	public void setMIDRATE(String mIDRATE) {
		MIDRATE = mIDRATE;
	}
	public String getTENOR() {
		return TENOR;
	}
	public void setTENOR(String tENOR) {
		TENOR = tENOR;
	}
	public String getRATECODE() {
		return RATECODE;
	}
	public void setRATECODE(String rATECODE) {
		RATECODE = rATECODE;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	  	
}
