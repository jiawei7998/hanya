package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;


/***
 * 债券即期
 * @author LIJ
 *
 */
public class GmrmBondSpot implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = -63934951395243885L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期，YYYYMMDD 
     */
    private String batchDate;

    /**
     * 债券序号
     */
    private String secId;

    /**
     * 头寸编号，唯一标识一笔债券的编号
     */
    private String posId;

    /**
     * Folder
     */
    private String folder;

    /**
     * 债券面值，多头减去空头
     */
    private BigDecimal position;

    /**
     * 债券币种
     */
    private String ccy;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 数据来源
     */
    private String source;

    /**
     * 多头头寸本金（Buy手数 x FACE VALUE）
     */
    private BigDecimal buyPosition;

    /**
     * 空头头寸本金（Sell手数 x FACE VALUE）
     */
    private BigDecimal sellPosition;

    /**
     * 多头头寸平均成本（一手的平均成本）
     */
    private BigDecimal buyAeragePrice;

    /**
     * 空头头寸平均成本（一手的平均成本）
     */
    private BigDecimal sellAveragePrice;

    /**
     * 一手面值
     */
    private BigDecimal faceValue;

    /**
     * 损益币种
     */
    private String plCcy;

    /**
     * 每日损益
     */
    private BigDecimal dailyPl;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期，YYYYMMDD 
     * @return BATCH_DATE 批量日期，YYYYMMDD 
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期，YYYYMMDD 
     * @param batchDate 批量日期，YYYYMMDD 
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 债券序号
     * @return SEC_ID 债券序号
     */
    public String getSecId() {
        return secId;
    }

    /**
     * 债券序号
     * @param secId 债券序号
     */
    public void setSecId(String secId) {
        this.secId = secId == null ? null : secId.trim();
    }

    /**
     * 头寸编号，唯一标识一笔债券的编号
     * @return POS_ID 头寸编号，唯一标识一笔债券的编号
     */
    public String getPosId() {
        return posId;
    }

    /**
     * 头寸编号，唯一标识一笔债券的编号
     * @param posId 头寸编号，唯一标识一笔债券的编号
     */
    public void setPosId(String posId) {
        this.posId = posId == null ? null : posId.trim();
    }

    /**
     * Folder
     * @return FOLDER Folder
     */
    public String getFolder() {
        return folder;
    }

    /**
     * Folder
     * @param folder Folder
     */
    public void setFolder(String folder) {
        this.folder = folder == null ? null : folder.trim();
    }

    /**
     * 债券面值，多头减去空头
     * @return POSITION 债券面值，多头减去空头
     */
    public BigDecimal getPosition() {
        return position;
    }

    /**
     * 债券面值，多头减去空头
     * @param position 债券面值，多头减去空头
     */
    public void setPosition(BigDecimal position) {
        this.position = position;
    }

    /**
     * 债券币种
     * @return CCY 债券币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 债券币种
     * @param ccy 债券币种
     */
    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    /**
     * 机构号
     * @return BRANCH_ID 机构号
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * 机构号
     * @param branchId 机构号
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }

    /**
     * 数据来源
     * @return SOURCE 数据来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 数据来源
     * @param source 数据来源
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * 多头头寸本金（Buy手数 x FACE VALUE）
     * @return BUY_POSITION 多头头寸本金（Buy手数 x FACE VALUE）
     */
    public BigDecimal getBuyPosition() {
        return buyPosition;
    }

    /**
     * 多头头寸本金（Buy手数 x FACE VALUE）
     * @param buyPosition 多头头寸本金（Buy手数 x FACE VALUE）
     */
    public void setBuyPosition(BigDecimal buyPosition) {
        this.buyPosition = buyPosition;
    }

    /**
     * 空头头寸本金（Sell手数 x FACE VALUE）
     * @return SELL_POSITION 空头头寸本金（Sell手数 x FACE VALUE）
     */
    public BigDecimal getSellPosition() {
        return sellPosition;
    }

    /**
     * 空头头寸本金（Sell手数 x FACE VALUE）
     * @param sellPosition 空头头寸本金（Sell手数 x FACE VALUE）
     */
    public void setSellPosition(BigDecimal sellPosition) {
        this.sellPosition = sellPosition;
    }

    /**
     * 多头头寸平均成本（一手的平均成本）
     * @return BUY_AERAGE_PRICE 多头头寸平均成本（一手的平均成本）
     */
    public BigDecimal getBuyAeragePrice() {
        return buyAeragePrice;
    }

    /**
     * 多头头寸平均成本（一手的平均成本）
     * @param buyAeragePrice 多头头寸平均成本（一手的平均成本）
     */
    public void setBuyAeragePrice(BigDecimal buyAeragePrice) {
        this.buyAeragePrice = buyAeragePrice;
    }

    /**
     * 空头头寸平均成本（一手的平均成本）
     * @return SELL_AVERAGE_PRICE 空头头寸平均成本（一手的平均成本）
     */
    public BigDecimal getSellAveragePrice() {
        return sellAveragePrice;
    }

    /**
     * 空头头寸平均成本（一手的平均成本）
     * @param sellAveragePrice 空头头寸平均成本（一手的平均成本）
     */
    public void setSellAveragePrice(BigDecimal sellAveragePrice) {
        this.sellAveragePrice = sellAveragePrice;
    }

    /**
     * 一手面值
     * @return FACE_VALUE 一手面值
     */
    public BigDecimal getFaceValue() {
        return faceValue;
    }

    /**
     * 一手面值
     * @param faceValue 一手面值
     */
    public void setFaceValue(BigDecimal faceValue) {
        this.faceValue = faceValue;
    }

    /**
     * 损益币种
     * @return PL_CCY 损益币种
     */
    public String getPlCcy() {
        return plCcy;
    }

    /**
     * 损益币种
     * @param plCcy 损益币种
     */
    public void setPlCcy(String plCcy) {
        this.plCcy = plCcy == null ? null : plCcy.trim();
    }

    /**
     * 每日损益
     * @return DAILY_PL 每日损益
     */
    public BigDecimal getDailyPl() {
        return dailyPl;
    }

    /**
     * 每日损益
     * @param dailyPl 每日损益
     */
    public void setDailyPl(BigDecimal dailyPl) {
        this.dailyPl = dailyPl;
    }
}