package com.singlee.financial.bean;


public class MmktConstaint
{
  public static String AUTO_SWITCH_ON = "ON";
  public static String AUTO_SWITCH_OFF = "OFF";
  public static String AUTO_RUNDATE_ED = "ED";
  public static String AUTO_RUNDATE_WD = "WD";
  
  public static String INTFLAG_REVR = "REVR";
  public static String INTFLAG_PRVR = "PRVR";
  public static String INTFLAG_RATE = "RATE";
  public static String INTFLAG_VOLD = "VOLD";
  public static String INTFLAG_YCRT = "YCRT";
  public static String INTFLAG_SMCL = "SMCL";
  public static String INTFLAG_SMCL_FOR = "SMCL_FOR";
  
  public static String SL_STATUS_SUCCESS = "0";
  public static String SL_STATUS_FAILD = "1";
  public static String SL_STATUS_PENDDING = "2";
  public static String PROC_STATUS_SUCCESS = "999";
  public static String OPICS_STATUS_PENDDING = "-1";
  public static String OPICS_STATUS_FEERROR = "-2";
  public static String OPICS_STATUS_DEALING = "-3";
  public static String OPICS_STATUS_SUCCESS = "-4";
  public static String OPICS_STATUS_DIUQI = "-5";
  public static String OPICS_STATUS_AWAITING_REPAIR = "-6";
  public static String OPICS_STATUS_PENDDING_REVERSAL = "-7";
  public static String OPICS_STATUS_PROCESSED_REVERSAL = "-8";
  public static String FORMULA_CAL_ERROR = "#NAME?";
  public static String COL_TYPE_CHAR = "C";
  public static String COL_TYPE_NUMBER = "N";
  public static int DEFAULT_NUMBER_SCALE = 8;
  public static String PROP_TYPE_int = "int";
  public static String PROP_TYPE_long = "long";
  public static String PROP_TYPE_STRING = "class java.lang.String";
  public static String PROP_TYPE_INTEGER = "class java.lang.Integer";
  public static String PROP_TYPE_LONG = "class java.lang.Long";
  public static String PROP_TYPE_BIGDECIMAL = "class java.math.BigDecimal";
}
