package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 错账查询
 * 
 * @author shenzl
 * 
 */
public class SlAcupErrBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2331673764118022945L;
	/**
	 * 套号
	 */
	private String setNo;
	/**
	 * 序列
	 */
	private String seqNo;
	/**
	 * 产品代码
	 */
	private String product;
	/**
	 * 类型代码
	 */
	private String type;
	/**
	 * 交易流水
	 */

	private String dealno;

	/**
	 * opics科目
	 */
	private String glno;
	/**
	 * 外部映射科目
	 */
	private String intglno;
	/**
	 * 描述
	 */
	private String descr;
	/**
	 * 科目映射
	 */
	private String subject;
	/**
	 * 借贷标识
	 */
	private String drcrind;
	/**
	 * 本金
	 */
	private String amount;
	/**
	 * 错误信息
	 */
	private String errMsg;

	private String voucher;

	/**
	 * 获取套号
	 * 
	 * @return
	 */
	public String getSetNo() {
		return setNo;
	}

	/**
	 * 设置套号
	 * 
	 * @return
	 */
	public void setSetNo(String setNo) {
		this.setNo = setNo;
	}

	/**
	 * 获取序列
	 */
	public String getSeqNo() {
		return seqNo;
	}

	/**
	 * 设置序列
	 */
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * 产品代码
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * 产品代码
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * 类型代码
	 */
	public String getType() {
		return type;
	}

	/**
	 * 类型代码
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 交易流水
	 */

	public String getDealno() {
		return dealno;
	}

	/**
	 * 交易流水
	 */

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	/**
	 * opics科目
	 */
	public String getGlno() {
		return glno;
	}

	/**
	 * opics科目
	 */
	public void setGlno(String glno) {
		this.glno = glno;
	}

	/**
	 * 外部映射科目
	 */
	public String getIntglno() {
		return intglno;
	}

	/**
	 * 外部映射科目
	 */
	public void setIntglno(String intglno) {
		this.intglno = intglno;
	}

	/**
	 * 描述
	 */
	public String getDescr() {
		return descr;
	}

	/**
	 * 描述
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

	/**
	 * 科目映射
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 科目映射
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 借贷标识
	 */
	public String getDrcrind() {
		return drcrind;
	}

	/**
	 * 借贷标识
	 */
	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}

	/**
	 * 本金
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * 本金
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * 错误信息
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * 错误信息
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getVoucher() {
		return voucher;
	}

	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}
}
