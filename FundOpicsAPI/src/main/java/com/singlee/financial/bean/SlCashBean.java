package com.singlee.financial.bean;

import java.io.Serializable;


public class SlCashBean implements Serializable{
	/**
	 * 生成头寸文件实体
	 */
	private static final long serialVersionUID = 1L;

	private String br;
	
	private String dealNo;
	
	private String tradUsr;
	
	private String orgCd;
	
	private String fundDate ;
	
	private String amtSq;
	
	private String amtDq;
	
	private String businessType;
	
	private String fundType;
	
	private String custCd;
	
	private String custNm;
	
	private String dealDate;
	
	private String matCcysMeans;
	
	private String dealText;
	
	private String revDate;
	
	private String ps;
	
	private String type;
	
	private String sett;
	
	
	
	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSett() {
		return sett;
	}

	public void setSett(String sett) {
		this.sett = sett;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getMatCcysMeans() {
		return matCcysMeans;
	}

	public void setMatCcysMeans(String matCcysMeans) {
		this.matCcysMeans = matCcysMeans;
	}

	public String getDealText() {
		return dealText;
	}

	public void setDealText(String dealText) {
		this.dealText = dealText;
	}

	public String getRevDate() {
		return revDate;
	}

	public void setRevDate(String revDate) {
		this.revDate = revDate;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getTradUsr() {
		return tradUsr;
	}

	public void setTradUsr(String tradUsr) {
		this.tradUsr = tradUsr;
	}

	public String getOrgCd() {
		return orgCd;
	}

	public void setOrgCd(String orgCd) {
		this.orgCd = orgCd;
	}

	public String getFundDate() {
		return fundDate;
	}

	public void setFundDate(String fundDate) {
		this.fundDate = fundDate;
	}

	public String getAmtSq() {
		return amtSq;
	}

	public void setAmtSq(String amtSq) {
		this.amtSq = amtSq;
	}

	public String getAmtDq() {
		return amtDq;
	}

	public void setAmtDq(String amtDq) {
		this.amtDq = amtDq;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getFundType() {
		return fundType;
	}

	public void setFundType(String fundType) {
		this.fundType = fundType;
	}

	public String getCustCd() {
		return custCd;
	}

	public void setCustCd(String custCd) {
		this.custCd = custCd;
	}

	public String getCustNm() {
		return custNm;
	}

	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}
	
	
}
