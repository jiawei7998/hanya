package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 外币记账实体类
 * @author zhengfl
 *
 */
public class SlPmtqFxBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8384180365657734400L;
	private String sn;//		交易流水
	private String br;//		部门
	private String product;//		产品代码
	private String prodtype;//		产品类型
	private String dealno;//		交易编号
	private String seq;//		序号
	private String payrecind;//		收付标识 p-付款
	private String cdate;//		创建日期
	private String ctime;//		创建时间
	private String ccy;//		币种
	private BigDecimal amount;//		金额
	private String swiftfmt;//		swift报文类型
	private String settmeans;//		清算方式
	private String setacct;//		清算账户
	private Date dealdate;//		交易日期
	private Date vdate;//		起息日
	private String cno;//		交易对手
	private String pbnkbic;//		发起行bic
	private String pbnknm;//		发起行名称
	private String pacctno;//		付款行账号
	private String pacctnm;//		付款行户名
	private String rbnkbic;//		报文接收行bic
	private String rbnknm;//		报文接收行名称
	private String awacct;//		收款行账户行账号
	private String awbnkbic;//		收款行账户行bic
	private String awbnknm;//		收款行账户行名称
	private String racctno;//		收款行账号
	private String racctnm;//		收款行名称
	private String racbic;//		收款行bic
	private String racbnkflag;//		收款行标识 0-境内他行  1-境外行
	private String scorresbic;//		发报方代理行代码
	private String scorresacct;//		发报方代理行账号
	private String rcorresbic;//		收报代理行代码
	private String rcorresacct;//		收报代理行账号
	private String interbnkbic;//		中间行代码
	private String interacct;//		中间行账号
	private String paynote;//		附言
	private String coreacctno;//		核心记账账号账号
	private String coreacctnm;//		核心记账户名 
	private String custlocation;//		对手开户地
	private String plocation;//		付款方开户地
	private String rlocation;//		收款方开户地  
	private String voidflag;//		撤单标志
	private String verind;//		经办复核标志
	private BigDecimal realamount;//		实际金额
	private BigDecimal balance;//		差额
	private String ioper;//		经办人
	private String inote;//		经办备注
	private String idate;//		经办时间
	private String veroper;//		复核人
	private String vernote;//		复核备注
	private String verdate;//		复核时间
	private String authoper;//		授权人
	private String authdate;//		授权时间
	private String sendflag;//		发送标识
	private String sendoper;//		发送人 
	private String senddate;//		发送时间
	private String srretucd;//		响应码
	private String srtrandt;//		返回交易日期
	private String srtransq;//		返回外围大交易流水 
	private String tranrefno;//		业务编号
	private String relaterefno;//		参考编号
	private String qrretucd;//		查询返回响应码
	private String qrstatus;//		查询返回交易状态 0-记账成功  1-记账失败  2-交易重复  3-交易不存在
	private String qrtime;//		查询返回时间 
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getPayrecind() {
		return payrecind;
	}
	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}
	public String getCdate() {
		return cdate;
	}
	public void setCdate(String cdate) {
		this.cdate = cdate;
	}
	public String getCtime() {
		return ctime;
	}
	public void setCtime(String ctime) {
		this.ctime = ctime;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getSwiftfmt() {
		return swiftfmt;
	}
	public void setSwiftfmt(String swiftfmt) {
		this.swiftfmt = swiftfmt;
	}
	public String getSettmeans() {
		return settmeans;
	}
	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}
	public String getSetacct() {
		return setacct;
	}
	public void setSetacct(String setacct) {
		this.setacct = setacct;
	}
	public Date getDealdate() {
		return dealdate;
	}
	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}
	public Date getVdate() {
		return vdate;
	}
	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getPbnkbic() {
		return pbnkbic;
	}
	public void setPbnkbic(String pbnkbic) {
		this.pbnkbic = pbnkbic;
	}
	public String getPbnknm() {
		return pbnknm;
	}
	public void setPbnknm(String pbnknm) {
		this.pbnknm = pbnknm;
	}
	public String getPacctno() {
		return pacctno;
	}
	public void setPacctno(String pacctno) {
		this.pacctno = pacctno;
	}
	public String getPacctnm() {
		return pacctnm;
	}
	public void setPacctnm(String pacctnm) {
		this.pacctnm = pacctnm;
	}
	public String getRbnkbic() {
		return rbnkbic;
	}
	public void setRbnkbic(String rbnkbic) {
		this.rbnkbic = rbnkbic;
	}
	public String getRbnknm() {
		return rbnknm;
	}
	public void setRbnknm(String rbnknm) {
		this.rbnknm = rbnknm;
	}
	public String getAwacct() {
		return awacct;
	}
	public void setAwacct(String awacct) {
		this.awacct = awacct;
	}
	public String getAwbnkbic() {
		return awbnkbic;
	}
	public void setAwbnkbic(String awbnkbic) {
		this.awbnkbic = awbnkbic;
	}
	public String getAwbnknm() {
		return awbnknm;
	}
	public void setAwbnknm(String awbnknm) {
		this.awbnknm = awbnknm;
	}
	public String getRacctno() {
		return racctno;
	}
	public void setRacctno(String racctno) {
		this.racctno = racctno;
	}
	public String getRacctnm() {
		return racctnm;
	}
	public void setRacctnm(String racctnm) {
		this.racctnm = racctnm;
	}
	public String getRacbic() {
		return racbic;
	}
	public void setRacbic(String racbic) {
		this.racbic = racbic;
	}
	public String getRacbnkflag() {
		return racbnkflag;
	}
	public void setRacbnkflag(String racbnkflag) {
		this.racbnkflag = racbnkflag;
	}
	public String getScorresbic() {
		return scorresbic;
	}
	public void setScorresbic(String scorresbic) {
		this.scorresbic = scorresbic;
	}
	public String getScorresacct() {
		return scorresacct;
	}
	public void setScorresacct(String scorresacct) {
		this.scorresacct = scorresacct;
	}
	public String getRcorresbic() {
		return rcorresbic;
	}
	public void setRcorresbic(String rcorresbic) {
		this.rcorresbic = rcorresbic;
	}
	public String getRcorresacct() {
		return rcorresacct;
	}
	public void setRcorresacct(String rcorresacct) {
		this.rcorresacct = rcorresacct;
	}
	public String getInterbnkbic() {
		return interbnkbic;
	}
	public void setInterbnkbic(String interbnkbic) {
		this.interbnkbic = interbnkbic;
	}
	public String getInteracct() {
		return interacct;
	}
	public void setInteracct(String interacct) {
		this.interacct = interacct;
	}
	public String getPaynote() {
		return paynote;
	}
	public void setPaynote(String paynote) {
		this.paynote = paynote;
	}
	public String getCoreacctno() {
		return coreacctno;
	}
	public void setCoreacctno(String coreacctno) {
		this.coreacctno = coreacctno;
	}
	public String getCoreacctnm() {
		return coreacctnm;
	}
	public void setCoreacctnm(String coreacctnm) {
		this.coreacctnm = coreacctnm;
	}
	public String getCustlocation() {
		return custlocation;
	}
	public void setCustlocation(String custlocation) {
		this.custlocation = custlocation;
	}
	public String getPlocation() {
		return plocation;
	}
	public void setPlocation(String plocation) {
		this.plocation = plocation;
	}
	public String getRlocation() {
		return rlocation;
	}
	public void setRlocation(String rlocation) {
		this.rlocation = rlocation;
	}
	public String getVoidflag() {
		return voidflag;
	}
	public void setVoidflag(String voidflag) {
		this.voidflag = voidflag;
	}
	public String getVerind() {
		return verind;
	}
	public void setVerind(String verind) {
		this.verind = verind;
	}
	public BigDecimal getRealamount() {
		return realamount;
	}
	public void setRealamount(BigDecimal realamount) {
		this.realamount = realamount;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getIoper() {
		return ioper;
	}
	public void setIoper(String ioper) {
		this.ioper = ioper;
	}
	public String getInote() {
		return inote;
	}
	public void setInote(String inote) {
		this.inote = inote;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getVeroper() {
		return veroper;
	}
	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}
	public String getVernote() {
		return vernote;
	}
	public void setVernote(String vernote) {
		this.vernote = vernote;
	}
	public String getVerdate() {
		return verdate;
	}
	public void setVerdate(String verdate) {
		this.verdate = verdate;
	}
	public String getAuthoper() {
		return authoper;
	}
	public void setAuthoper(String authoper) {
		this.authoper = authoper;
	}
	public String getAuthdate() {
		return authdate;
	}
	public void setAuthdate(String authdate) {
		this.authdate = authdate;
	}
	public String getSendflag() {
		return sendflag;
	}
	public void setSendflag(String sendflag) {
		this.sendflag = sendflag;
	}
	public String getSendoper() {
		return sendoper;
	}
	public void setSendoper(String sendoper) {
		this.sendoper = sendoper;
	}
	public String getSenddate() {
		return senddate;
	}
	public void setSenddate(String senddate) {
		this.senddate = senddate;
	}
	public String getSrretucd() {
		return srretucd;
	}
	public void setSrretucd(String srretucd) {
		this.srretucd = srretucd;
	}
	public String getSrtrandt() {
		return srtrandt;
	}
	public void setSrtrandt(String srtrandt) {
		this.srtrandt = srtrandt;
	}
	public String getSrtransq() {
		return srtransq;
	}
	public void setSrtransq(String srtransq) {
		this.srtransq = srtransq;
	}
	public String getTranrefno() {
		return tranrefno;
	}
	public void setTranrefno(String tranrefno) {
		this.tranrefno = tranrefno;
	}
	public String getRelaterefno() {
		return relaterefno;
	}
	public void setRelaterefno(String relaterefno) {
		this.relaterefno = relaterefno;
	}
	public String getQrretucd() {
		return qrretucd;
	}
	public void setQrretucd(String qrretucd) {
		this.qrretucd = qrretucd;
	}
	public String getQrstatus() {
		return qrstatus;
	}
	public void setQrstatus(String qrstatus) {
		this.qrstatus = qrstatus;
	}
	public String getQrtime() {
		return qrtime;
	}
	public void setQrtime(String qrtime) {
		this.qrtime = qrtime;
	}


}
