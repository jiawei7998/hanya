package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 
 * @author LIJ
 *
 */
public class GmrmIrsLump implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 2574115427309955060L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 交易编号
     */
    private String dealId;

    /**
     * 支付日 YYYYMMDD
     */
    private String payDate;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 接收支付标志 1、Pay支付端  2、Receive接收端
     */
    private String payRecFlag;

    /**
     * 币种
     */
    private String ccy;

    /**
     * 费用类型 1-交易价差收入；2-期权费；3-其它；4-终止费(本币)；5-终止费(外币)；6-本金；7-利息调整费；8-期权现金行权交割
     */
    private BigDecimal lumpType;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期 YYYYMMDD
     * @return BATCH_DATE 批量日期 YYYYMMDD
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期 YYYYMMDD
     * @param batchDate 批量日期 YYYYMMDD
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 交易编号
     * @return DEAL_ID 交易编号
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 交易编号
     * @param dealId 交易编号
     */
    public void setDealId(String dealId) {
        this.dealId = dealId == null ? null : dealId.trim();
    }

    /**
     * 支付日 YYYYMMDD
     * @return PAY_DATE 支付日 YYYYMMDD
     */
    public String getPayDate() {
        return payDate;
    }

    /**
     * 支付日 YYYYMMDD
     * @param payDate 支付日 YYYYMMDD
     */
    public void setPayDate(String payDate) {
        this.payDate = payDate == null ? null : payDate.trim();
    }

    /**
     * 金额
     * @return AMOUNT 金额
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * 金额
     * @param amount 金额
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 接收支付标志 1、Pay支付端  2、Receive接收端
     * @return PAY_REC_FLAG 接收支付标志 1、Pay支付端  2、Receive接收端
     */
    public String getPayRecFlag() {
        return payRecFlag;
    }

    /**
     * 接收支付标志 1、Pay支付端  2、Receive接收端
     * @param payRecFlag 接收支付标志 1、Pay支付端  2、Receive接收端
     */
    public void setPayRecFlag(String payRecFlag) {
        this.payRecFlag = payRecFlag == null ? null : payRecFlag.trim();
    }

    /**
     * 币种
     * @return CCY 币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 币种
     * @param ccy 币种
     */
    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    /**
     * 费用类型 1-交易价差收入；2-期权费；3-其它；4-终止费(本币)；5-终止费(外币)；6-本金；7-利息调整费；8-期权现金行权交割
     * @return LUMP_TYPE 费用类型 1-交易价差收入；2-期权费；3-其它；4-终止费(本币)；5-终止费(外币)；6-本金；7-利息调整费；8-期权现金行权交割
     */
    public BigDecimal getLumpType() {
        return lumpType;
    }

    /**
     * 费用类型 1-交易价差收入；2-期权费；3-其它；4-终止费(本币)；5-终止费(外币)；6-本金；7-利息调整费；8-期权现金行权交割
     * @param lumpType 费用类型 1-交易价差收入；2-期权费；3-其它；4-终止费(本币)；5-终止费(外币)；6-本金；7-利息调整费；8-期权现金行权交割
     */
    public void setLumpType(BigDecimal lumpType) {
        this.lumpType = lumpType;
    }

    /**
     * 机构号
     * @return BRANCH_ID 机构号
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * 机构号
     * @param branchId 机构号
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }
}