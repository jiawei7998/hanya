package com.singlee.financial.bean;

import java.io.Serializable;

public class SlAcupRecord implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4217808415368000348L;

	// 机构
	private String br;
	// 账务日期
	private String postDate;
	// 操作步骤
	private String currStep;
	// 返回状态
	private String retCode;
	// 返回信息
	private String retMsg;
	// 套数
	private String setNo;
	// 成功
	private String succCount;
	// 失败
	private String failCount;
	// 更新日期
	private String lastDate;

	private String status;

	// 上送核心流水号
	private String coreseq;

	// 核心反馈文件路径
	private String corefile;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getCurrStep() {
		return currStep;
	}

	public void setCurrStep(String currStep) {
		this.currStep = currStep;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String getSetNo() {
		return setNo;
	}

	public void setSetNo(String setNo) {
		this.setNo = setNo;
	}

	public String getSuccCount() {
		return succCount;
	}

	public void setSuccCount(String succCount) {
		this.succCount = succCount;
	}

	public String getFailCount() {
		return failCount;
	}

	public void setFailCount(String failCount) {
		this.failCount = failCount;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCoreseq() {
		return coreseq;
	}

	public void setCoreseq(String coreseq) {
		this.coreseq = coreseq;
	}

	public String getCorefile() {
		return corefile;
	}

	public void setCorefile(String corefile) {
		this.corefile = corefile;
	}

}
