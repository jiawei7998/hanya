package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 敞口查询
 * @author zhengfl
 *
 */
public class SlExposureBean implements Serializable {

	private static final long serialVersionUID = 2331673764118022945L;
	/**
	 * 部门
	 */
	private String br;
	/**
	 * 敞口类型
	 */
	private String prod;

	/**
	 * 起息日
	 */
	private String vdate;

	/**
	 * 货币1
	 */
	private String ccy1;

	/**
	 * 货币1敞口
	 */
	private BigDecimal ccy1amt;

	/**
	 * 货币2
	 */
	private String ccy2;

	/**
	 * 货币2敞口
	 */
	private BigDecimal ccy2amt;

	/**
	 * 预警状态
	 */
	private String ce;

	/**
	 * 成本汇率
	 */
	private BigDecimal bidprice;

	/**
	 * 市场汇率
	 */
	private BigDecimal price;

	/**
	 * 浮动损益（CNY）
	 */
	private BigDecimal plamt;
	
	
	/**
	 * 折美元浮动金额
	 */
	private BigDecimal usdamt;
	
	/**
	 * 外汇兑人名币汇率
	 */
	private BigDecimal rmbprice;
	
	/**
	 * 外币金额
	 */
	private BigDecimal amt;
	
	private String port;
	
	private String trad;
	
	private String ccy;
	
	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getCcy1() {
		return ccy1;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public BigDecimal getCcy1amt() {
		return ccy1amt;
	}

	public void setCcy1amt(BigDecimal ccy1amt) {
		this.ccy1amt = ccy1amt;
	}

	public String getCcy2() {
		return ccy2;
	}

	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}

	public BigDecimal getCcy2amt() {
		return ccy2amt;
	}

	public void setCcy2amt(BigDecimal ccy2amt) {
		this.ccy2amt = ccy2amt;
	}

	public String getCe() {
		return ce;
	}

	public void setCe(String ce) {
		this.ce = ce;
	}

	public BigDecimal getBidprice() {
		return bidprice;
	}

	public void setBidprice(BigDecimal bidprice) {
		this.bidprice = bidprice;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getPlamt() {
		return plamt;
	}

	public void setPlamt(BigDecimal plamt) {
		this.plamt = plamt;
	}

	public BigDecimal getUsdamt() {
		return usdamt;
	}

	public void setUsdamt(BigDecimal usdamt) {
		this.usdamt = usdamt;
	}

	public BigDecimal getRmbprice() {
		return rmbprice;
	}

	public void setRmbprice(BigDecimal rmbprice) {
		this.rmbprice = rmbprice;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getPort() {
		return port;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getTrad() {
		return trad;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcy() {
		return ccy;
	}
	
}