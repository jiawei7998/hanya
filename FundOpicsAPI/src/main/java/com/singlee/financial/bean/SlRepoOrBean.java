package com.singlee.financial.bean;

import java.math.BigDecimal;
import java.util.Date;

import com.singlee.financial.pojo.RepoOrBean;

public class SlRepoOrBean extends RepoOrBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4824515686091033448L;

	/**
	 * INTH 头表信息
	 */
	private SlInthBean inthBean;
	/**
	 * OPICS拓展属性
	 */
	private SlExternalBean externalBean;
	/**
	 * 债券ISNB代码
	 */
	private String securityId;
	/**
	 * 到期日
	 */
	private Date maturityDate;
	/**
	 * 分配序列号
	 */
	private String assignseq;
	/**
	 * 保管帐户
	 */
	private String safekeepacct;
	/**
	 * 抵押品代码
	 */
	private String collateralcode;
	/**
	 * 回购成本中心
	 */
	private String repocost;
	/**
	 * 交货类型
	 */
	private String delivtype;
	/**
	 * 优惠券再投资
	 */
	private String coupreinvest;
	/**
	 * 回购选项卡指示器
	 */
	private String tabindicator;
	/**
	 * 回购利率
	 */
	private BigDecimal reporate;
	/**
	 * 抑制取消
	 */
	private String suppcancelind;
	/**
	 * 抵押单位
	 */
	private String collunit;
	/**
	 * 数量
	 */
	private String qty;
	/**
	 * 投资类型
	 */
	private String invtype;
	/**
	 * 全价/净价指标
	 */
	private String dcpriceind;
	/**
	 * 回购利息计算类型
	 */
	private String intcalctype;
	/**
	 * 完全指定的指标
	 */
	private String fullyassignind;
	/**
	 * 保留货币价值指标
	 */
	private String pmvind;
	/**
	 * 免费格式说明
	 */
	private String freeformat;
	/**
	 * 货币
	 */
	private String ccy;
	/**
	 * 计息基础
	 */
	private String basis;
	/**
	 * 期限
	 */
	private String tenor;
	/**
	 * COMCCYSMEANS
	 */
	private String comccysmeans;
	/**
	 * COMCCYSACCT
	 */
	private String comccysacct;
	/**
	 * MATCCYSMEANS
	 */
	private String matccysmeans;
	/**
	 * MATCCYSACCT
	 */
	private String matccysacct;
	/**
	 * 回购投资组合
	 */
	private String repoport;
	
	/**
	 * 买断式回购初始化
	 * 
	 * @param br
	 *            业务部门分支
	 * @param tag
	 *            标识
	 * @param detail
	 *            细节
	 * @note
	 * 
	 *       1、交易导入 TAG="REDE" , DETAIL="IRCA"
	 * 
	 *       2、交易冲销 TAG="REDR", DETAIL="IRCA"
	 * 
	 */
	public SlRepoOrBean(String br,String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.REPO.SEQ);
			inthBean.setInoutind(SlDealModule.REPO.INOUTIND);
			inthBean.setPriority(SlDealModule.REPO.PRIORITY);
			inthBean.setStatcode(SlDealModule.REPO.STATCODE);
			inthBean.setSyst(SlDealModule.REPO.SYST);
			inthBean.setIoper(SlDealModule.REPO.IOPER);
		}
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public SlExternalBean getExternalBean() {
		return externalBean;
	}

	public void setExternalBean(SlExternalBean externalBean) {
		this.externalBean = externalBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSecurityId() {
		return securityId;
	}

	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	public String getAssignseq() {
		return assignseq;
	}

	public void setAssignseq(String assignseq) {
		this.assignseq = assignseq;
	}

	public String getSafekeepacct() {
		return safekeepacct;
	}

	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}

	public String getCollateralcode() {
		return collateralcode;
	}

	public void setCollateralcode(String collateralcode) {
		this.collateralcode = collateralcode;
	}

	public String getRepocost() {
		return repocost;
	}

	public void setRepocost(String repocost) {
		this.repocost = repocost;
	}

	public String getDelivtype() {
		return delivtype;
	}

	public void setDelivtype(String delivtype) {
		this.delivtype = delivtype;
	}

	public String getCoupreinvest() {
		return coupreinvest;
	}

	public void setCoupreinvest(String coupreinvest) {
		this.coupreinvest = coupreinvest;
	}

	public String getTabindicator() {
		return tabindicator;
	}

	public void setTabindicator(String tabindicator) {
		this.tabindicator = tabindicator;
	}

	public BigDecimal getReporate() {
		return reporate;
	}

	public void setReporate(BigDecimal reporate) {
		this.reporate = reporate;
	}

	public String getSuppcancelind() {
		return suppcancelind;
	}

	public void setSuppcancelind(String suppcancelind) {
		this.suppcancelind = suppcancelind;
	}

	public String getCollunit() {
		return collunit;
	}

	public void setCollunit(String collunit) {
		this.collunit = collunit;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getDcpriceind() {
		return dcpriceind;
	}

	public void setDcpriceind(String dcpriceind) {
		this.dcpriceind = dcpriceind;
	}

	public String getIntcalctype() {
		return intcalctype;
	}

	public void setIntcalctype(String intcalctype) {
		this.intcalctype = intcalctype;
	}

	public String getFullyassignind() {
		return fullyassignind;
	}

	public void setFullyassignind(String fullyassignind) {
		this.fullyassignind = fullyassignind;
	}

	public String getPmvind() {
		return pmvind;
	}

	public void setPmvind(String pmvind) {
		this.pmvind = pmvind;
	}

	public String getFreeformat() {
		return freeformat;
	}

	public void setFreeformat(String freeformat) {
		this.freeformat = freeformat;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcy() {
		return ccy;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getBasis() {
		return basis;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getTenor() {
		return tenor;
	}

	public String getComccysmeans() {
		return comccysmeans;
	}

	public void setComccysmeans(String comccysmeans) {
		this.comccysmeans = comccysmeans;
	}

	public String getComccysacct() {
		return comccysacct;
	}

	public void setComccysacct(String comccysacct) {
		this.comccysacct = comccysacct;
	}

	public String getMatccysmeans() {
		return matccysmeans;
	}

	public void setMatccysmeans(String matccysmeans) {
		this.matccysmeans = matccysmeans;
	}

	public String getMatccysacct() {
		return matccysacct;
	}

	public void setMatccysacct(String matccysacct) {
		this.matccysacct = matccysacct;
	}

	public void setRepoport(String repoport) {
		this.repoport = repoport;
	}

	public String getRepoport() {
		return repoport;
	}

}
