package com.singlee.financial.bean;

public class SlMmktIselForBean extends SlMarketDataBaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = -318410184105132783L;
	protected String SECID;
	  protected String SECIDORGI;
	  protected String CCY;
	  protected String SECNAME;
	  protected String ISSUERID;
	  protected String POSITION;
	  protected String PURPRICE;
	  protected String SELLPRICE;
	  protected String CLOSEPRICE;
	  protected String DV01;
	  protected String DURATION;
	  protected String POSDUR;
	  protected String RISK;
	  protected String RATINGORG;
	  protected String RATING;
	  protected String ISSUERRATINGORG1;
	  protected String ISSUERRATING1;
	  protected String ISSUERRATINGORG2;
	  protected String ISSUERRATING2;
	  protected String ISSUERRATINGORG3;
	  protected String ISSUERRATING3;
	public String getSECID() {
		return SECID;
	}
	public void setSECID(String sECID) {
		SECID = sECID;
	}
	public String getSECIDORGI() {
		return SECIDORGI;
	}
	public void setSECIDORGI(String sECIDORGI) {
		SECIDORGI = sECIDORGI;
	}
	public String getCCY() {
		return CCY;
	}
	public void setCCY(String cCY) {
		CCY = cCY;
	}
	public String getSECNAME() {
		return SECNAME;
	}
	public void setSECNAME(String sECNAME) {
		SECNAME = sECNAME;
	}
	public String getISSUERID() {
		return ISSUERID;
	}
	public void setISSUERID(String iSSUERID) {
		ISSUERID = iSSUERID;
	}
	public String getPOSITION() {
		return POSITION;
	}
	public void setPOSITION(String pOSITION) {
		POSITION = pOSITION;
	}
	public String getPURPRICE() {
		return PURPRICE;
	}
	public void setPURPRICE(String pURPRICE) {
		PURPRICE = pURPRICE;
	}
	public String getSELLPRICE() {
		return SELLPRICE;
	}
	public void setSELLPRICE(String sELLPRICE) {
		SELLPRICE = sELLPRICE;
	}
	public String getCLOSEPRICE() {
		return CLOSEPRICE;
	}
	public void setCLOSEPRICE(String cLOSEPRICE) {
		CLOSEPRICE = cLOSEPRICE;
	}
	public String getDV01() {
		return DV01;
	}
	public void setDV01(String dV01) {
		DV01 = dV01;
	}
	public String getDURATION() {
		return DURATION;
	}
	public void setDURATION(String dURATION) {
		DURATION = dURATION;
	}
	public String getPOSDUR() {
		return POSDUR;
	}
	public void setPOSDUR(String pOSDUR) {
		POSDUR = pOSDUR;
	}
	public String getRISK() {
		return RISK;
	}
	public void setRISK(String rISK) {
		RISK = rISK;
	}
	public String getRATINGORG() {
		return RATINGORG;
	}
	public void setRATINGORG(String rATINGORG) {
		RATINGORG = rATINGORG;
	}
	public String getRATING() {
		return RATING;
	}
	public void setRATING(String rATING) {
		RATING = rATING;
	}
	public String getISSUERRATINGORG1() {
		return ISSUERRATINGORG1;
	}
	public void setISSUERRATINGORG1(String iSSUERRATINGORG1) {
		ISSUERRATINGORG1 = iSSUERRATINGORG1;
	}
	public String getISSUERRATING1() {
		return ISSUERRATING1;
	}
	public void setISSUERRATING1(String iSSUERRATING1) {
		ISSUERRATING1 = iSSUERRATING1;
	}
	public String getISSUERRATINGORG2() {
		return ISSUERRATINGORG2;
	}
	public void setISSUERRATINGORG2(String iSSUERRATINGORG2) {
		ISSUERRATINGORG2 = iSSUERRATINGORG2;
	}
	public String getISSUERRATING2() {
		return ISSUERRATING2;
	}
	public void setISSUERRATING2(String iSSUERRATING2) {
		ISSUERRATING2 = iSSUERRATING2;
	}
	public String getISSUERRATINGORG3() {
		return ISSUERRATINGORG3;
	}
	public void setISSUERRATINGORG3(String iSSUERRATINGORG3) {
		ISSUERRATINGORG3 = iSSUERRATINGORG3;
	}
	public String getISSUERRATING3() {
		return ISSUERRATING3;
	}
	public void setISSUERRATING3(String iSSUERRATING3) {
		ISSUERRATING3 = iSSUERRATING3;
	}
	  
}
