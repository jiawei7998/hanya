package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "SL_PMTQ_ACUP")
public class SlPmtqAcupBean implements Serializable {

	private static final long serialVersionUID = 1901551160942215641L;

	private String setno;// 账套号
	private String seqno;// 账务顺序号
	private Date postdate;// 账务日期
	private Date valdate;// 起息日
	private Date oldvdate;// 原起息日
	private String fedealno;// 发送核心流水号
	private String sn;// 业务流水号
	private String dealno;// 交易流水号
	private String cno;// 客户号
	private String cmne;// 客户简称
	private String ccy;// 币种
	private String ccycode;// 币种编码
	private String drcrind;// 借贷标识
	private String account;// 账号
	private BigDecimal amount;// 金额
	private String revflag;// 冲销标识
	private String issend;// 是否发送
	private String issuccess;// 是否成功
	private String errmsg;// 错误信息
	private String coreDealno;// 核心流水号
	private String br;// 机构
	private String product;// 产品
	private String type;// 类型
	private String payrecind;//
	private Date vdate;//
	private String status;//
	private String swiftfmt;// SWIFT报文类型
	private String setmeans;// 清算方式
	private String setacct;// 清算账号
	private String cost;// 成本中心
	private String crno;// 借方账号
	private String glno;// glno号
	private String drno;// 借方账号
	private Date intime;// 创建时间
	private String recode;//
	private String reinfo;//
	private String sendmsg;// 发送报文
	private String resultmsg;// 回执报文
	private String memo;// 是否是人民币
	@Transient
	private String frampostdate;//账务日期

	public String getSetno() {
		return setno;
	}

	public void setSetno(String setno) {
		this.setno = setno;
	}

	public String getSeqno() {
		return seqno;
	}

	public void setSeqno(String seqno) {
		this.seqno = seqno;
	}

	public Date getPostdate() {
		return postdate;
	}

	public void setPostdate(Date postdate) {
		this.postdate = postdate;
	}

	public Date getValdate() {
		return valdate;
	}

	public void setValdate(Date valdate) {
		this.valdate = valdate;
	}

	public Date getOldvdate() {
		return oldvdate;
	}

	public void setOldvdate(Date oldvdate) {
		this.oldvdate = oldvdate;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCmne() {
		return cmne;
	}

	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcycode() {
		return ccycode;
	}

	public void setCcycode(String ccycode) {
		this.ccycode = ccycode;
	}

	public String getDrcrind() {
		return drcrind;
	}

	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getRevflag() {
		return revflag;
	}

	public void setRevflag(String revflag) {
		this.revflag = revflag;
	}

	public String getIssend() {
		return issend;
	}

	public void setIssend(String issend) {
		this.issend = issend;
	}

	public String getIssuccess() {
		return issuccess;
	}

	public void setIssuccess(String issuccess) {
		this.issuccess = issuccess;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getCoreDealno() {
		return coreDealno;
	}

	public void setCoreDealno(String coreDealno) {
		this.coreDealno = coreDealno;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSwiftfmt() {
		return swiftfmt;
	}

	public void setSwiftfmt(String swiftfmt) {
		this.swiftfmt = swiftfmt;
	}

	public String getSetmeans() {
		return setmeans;
	}

	public void setSetmeans(String setmeans) {
		this.setmeans = setmeans;
	}

	public String getSetacct() {
		return setacct;
	}

	public void setSetacct(String setacct) {
		this.setacct = setacct;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getCrno() {
		return crno;
	}

	public void setCrno(String crno) {
		this.crno = crno;
	}

	public String getGlno() {
		return glno;
	}

	public void setGlno(String glno) {
		this.glno = glno;
	}

	public String getDrno() {
		return drno;
	}

	public void setDrno(String drno) {
		this.drno = drno;
	}

	public Date getIntime() {
		return intime;
	}

	public void setIntime(Date intime) {
		this.intime = intime;
	}

	public String getRecode() {
		return recode;
	}

	public void setRecode(String recode) {
		this.recode = recode;
	}

	public String getReinfo() {
		return reinfo;
	}

	public void setReinfo(String reinfo) {
		this.reinfo = reinfo;
	}

	public String getSendmsg() {
		return sendmsg;
	}

	public void setSendmsg(String sendmsg) {
		this.sendmsg = sendmsg;
	}

	public String getResultmsg() {
		return resultmsg;
	}

	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getFrampostdate() {
		return frampostdate;
	}

	public void setFrampostdate(String frampostdate) {
		this.frampostdate = frampostdate;
	}
}
