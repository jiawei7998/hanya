package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 国家代码
 * 
 * @author shenzl
 * 
 */
public class SlCounBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3783592164904671227L;
	/**
	 * 国家代码
	 */
	private String ccode;
	/**
	 * 国家名称
	 */
	private String coun;
	/**
	 * 最后维护时间
	 */
	private String lstmntdte;
	/**
	 * 影响标识,暂时无用
	 */
	private String sensitiveind;
	/**
	 * RPTG条约标识,暂时无用
	 */
	private String treatying;

	public String getCcode() {
		return ccode;
	}

	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	public String getCoun() {
		return coun;
	}

	public void setCoun(String coun) {
		this.coun = coun;
	}

	public String getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getSensitiveind() {
		return sensitiveind;
	}

	public void setSensitiveind(String sensitiveind) {
		this.sensitiveind = sensitiveind;
	}

	public String getTreatying() {
		return treatying;
	}

	public void setTreatying(String treatying) {
		this.treatying = treatying;
	}

}
