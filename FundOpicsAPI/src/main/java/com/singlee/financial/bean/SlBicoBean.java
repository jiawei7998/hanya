package com.singlee.financial.bean;

import java.io.Serializable;

public class SlBicoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 534146828816265733L;
	/**
	 * 银行识别码，相当于银行的SWIFT号
	 */
	private String bic;
	/**
	 * 银行代码简称
	 */
	private String sn;
	/**
	 * 开户行地址1
	 */
	private String ba1;
	/**
	 * 开户行地址2
	 */
	private String ba2;
	/**
	 * 开户行地址3
	 */
	private String ba3;
	/**
	 * 开户行地址4
	 */
	private String ba4;
	/**
	 * 开户行地址5
	 */
	private String ba5;
	/**
	 * 最后维护时间
	 */
	private String lstmntdte;

	/**
	 * 银行识别码，相当于银行的SWIFT号
	 * 
	 * @return
	 */
	public String getBic() {
		return bic;
	}

	/**
	 * 银行识别码，相当于银行的SWIFT号
	 * 
	 * @return
	 */
	public void setBic(String bic) {
		this.bic = bic;
	}

	/**
	 * 银行代码简称
	 * 
	 * @return
	 */
	public String getSn() {
		return sn;
	}

	/**
	 * 银行代码简称
	 * 
	 * @return
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}

	/**
	 * 开户行地址1
	 * 
	 * @return
	 */
	public String getBa1() {
		return ba1;
	}

	/**
	 * 开户行地址1
	 * 
	 * @return
	 */
	public void setBa1(String ba1) {
		this.ba1 = ba1;
	}

	/**
	 * 开户行地址2
	 * 
	 * @return
	 */
	public String getBa2() {
		return ba2;
	}

	/**
	 * 开户行地址2
	 * 
	 * @return
	 */
	public void setBa2(String ba2) {
		this.ba2 = ba2;
	}

	/**
	 * 开户行地址3
	 * 
	 * @return
	 */
	public String getBa3() {
		return ba3;
	}

	/**
	 * 开户行地址4
	 * 
	 * @return
	 */
	public void setBa3(String ba3) {
		this.ba3 = ba3;
	}

	/**
	 * 开户行地址4
	 * 
	 * @return
	 */
	public String getBa4() {
		return ba4;
	}

	/**
	 * 开户行地址4
	 * 
	 * @return
	 */
	public void setBa4(String ba4) {
		this.ba4 = ba4;
	}

	/**
	 * 开户行地址5
	 * 
	 * @return
	 */
	public String getBa5() {
		return ba5;
	}

	/**
	 * 开户行地址5
	 * 
	 * @return
	 */
	public void setBa5(String ba5) {
		this.ba5 = ba5;
	}

	/**
	 * 最后维护时间
	 * 
	 * @return
	 */
	public String getLstmntdte() {
		return lstmntdte;
	}

	/**
	 * 最后维护时间
	 * 
	 * @return
	 */
	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

}
