package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 接口账务参数匹配规则
 * 
 * @author shenzl
 *
 */
public class SlGltaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6465780287457595805L;

	private String tableid;

	private String br;

	private String tablevalue;

	private String tabletext;

	private String text;

	private String text1;

	private String text2;

	private String text3;

	private String lstmntdate;

	public String getTableid() {
		return tableid;
	}

	public void setTableid(String tableid) {
		this.tableid = tableid;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getTablevalue() {
		return tablevalue;
	}

	public void setTablevalue(String tablevalue) {
		this.tablevalue = tablevalue;
	}

	public String getTabletext() {
		return tabletext;
	}

	public void setTabletext(String tabletext) {
		this.tabletext = tabletext;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public String getText3() {
		return text3;
	}

	public void setText3(String text3) {
		this.text3 = text3;
	}

	public String getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(String lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
