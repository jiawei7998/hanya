package com.singlee.financial.bean;

import java.io.Serializable;
/**
 * G14 授信集中情况 实体表
 */
public class SlReportG14Bean implements Serializable {
 
	
	private static final long serialVersionUID = 1L;
	private String groupCn; //金融机构名称
	private String  groupId;//金融机构代码
	private Integer amt1;//报告期内最高风险额
	private Integer  amt2;//正常类
	private Integer amt3;// 其他表内同业授信
	private Integer  amt4;//保证金 银行存单 ，国债
	private String  rpdate;
	
	
	public String getGroupCn() {
		return groupCn;
	}
	public void setGroupCn(String groupCn) {
		this.groupCn = groupCn;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public Integer getAmt1() {
		return amt1;
	}
	public void setAmt1(Integer amt1) {
		this.amt1 = amt1;
	}
	public Integer getAmt2() {
		return amt2;
	}
	public void setAmt2(Integer amt2) {
		this.amt2 = amt2;
	}
	public Integer getAmt3() {
		return amt3;
	}
	public void setAmt3(Integer amt3) {
		this.amt3 = amt3;
	}
	public Integer getAmt4() {
		return amt4;
	}
	public void setAmt4(Integer amt4) {
		this.amt4 = amt4;
	}
	public String getRpdate() {
		return rpdate;
	}
	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}
	@Override
	public String toString() {
		return "SlReportG14Bean [groupCn=" + groupCn + ", groupId=" + groupId
				+ ", amt1=" + amt1 + ", amt2=" + amt2 + ", amt3=" + amt3
				+ ", amt4=" + amt4 + ", rpdate=" + rpdate + ", getGroupCn()="
				+ getGroupCn() + ", getGroupId()=" + getGroupId()
				+ ", getAmt1()=" + getAmt1() + ", getAmt2()=" + getAmt2()
				+ ", getAmt3()=" + getAmt3() + ", getAmt4()=" + getAmt4()
				+ ", getRpdate()=" + getRpdate() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
}
