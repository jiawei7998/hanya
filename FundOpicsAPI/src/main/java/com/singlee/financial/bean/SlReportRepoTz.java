package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * @author zc
 */
public class SlReportRepoTz implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String br;
	private String dealNo;
	private String secId;
	private String port;
	private String prodType;
	private String type;
	private String account;
	private String rateType;
	private String ccy;
	private String rate;
	private String sqsettDate;
	private String dqsettDate;
	private String qty;
	private String dealText;
	private String sqjsAmt;
	private String costType;
	private String dqjsAmt;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getSqsettDate() {
		return sqsettDate;
	}
	public void setSqsettDate(String sqsettDate) {
		this.sqsettDate = sqsettDate;
	}
	public String getDqsettDate() {
		return dqsettDate;
	}
	public void setDqsettDate(String dqsettDate) {
		this.dqsettDate = dqsettDate;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getDealText() {
		return dealText;
	}
	public void setDealText(String dealText) {
		this.dealText = dealText;
	}
	public String getSqjsAmt() {
		return sqjsAmt;
	}
	public void setSqjsAmt(String sqjsAmt) {
		this.sqjsAmt = sqjsAmt;
	}
	public String getCostType() {
		return costType;
	}
	public void setCostType(String costType) {
		this.costType = costType;
	}
	public String getDqjsAmt() {
		return dqjsAmt;
	}
	public void setDqjsAmt(String dqjsAmt) {
		this.dqjsAmt = dqjsAmt;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	public String getAcctngType() {
		return acctngType;
	}
	public void setAcctngType(String acctngType) {
		this.acctngType = acctngType;
	}
	private String trad;
	private String cmne;
	private String acctngType;
	
	
}
