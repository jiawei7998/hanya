package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 账务信息数组
 * 
 * @author Qxj
 */
public class GlInfoArray implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 交易日期
	 */
	private String TRAN_DATE;
	/**
	 * 交易柜员
	 */
	private String TRNSCTN_USR;
	/**
	 * 借贷标志
	 */
	private String DR_CR_FLAG;
	/**
	 * 记账金额
	 */
	private BigDecimal ACCT_AMT;
	/**
	 * 借方余额
	 */
	private BigDecimal DR_BAL;
	/**
	 * 贷方余额
	 */
	private BigDecimal CR_BAL;
	/**
	 * 主机日期
	 */
	private String HOST_DATE;
	/**
	 * 交易时间
	 */
	private String TX_TIME;
	/**
	 * 交易码
	 */
	private String TRAN_CODE;
	/**
	 * 原服务处理流水号
	 */
	private String ORI_SERV_SEQ_NO;
	/**
	 * 币种
	 */
	private String CCY;
	/**
	 * 外部流水
	 */
	private String EXT_SEQ_NO;
	/**
	 * 对方账号
	 */
	private String OTHR_ACCT_NO;
	/**
	 * 对方账户名称
	 */
	private String OTHR_ACCT_NM;
	/**
	 * 现转标志
	 */
	private String CASH_TRAN_FLAG;
	/**
	 * 摘要代码
	 */
	private String SMMRY_CD;
	/**
	 * 摘要描述
	 */
	private String SMMRY_MSG;
	/**
	 * 交易机构
	 */
	private String TRAN_BRANCH_ID;
	/**
	 * 核算机构
	 */
	private String ACCOUNTING_BRANCH_ID;

	public String getTRAN_DATE() {
		return TRAN_DATE;
	}

	public void setTRAN_DATE(String tRAN_DATE) {
		TRAN_DATE = tRAN_DATE;
	}

	public String getTRNSCTN_USR() {
		return TRNSCTN_USR;
	}

	public void setTRNSCTN_USR(String tRNSCTN_USR) {
		TRNSCTN_USR = tRNSCTN_USR;
	}

	public String getDR_CR_FLAG() {
		return DR_CR_FLAG;
	}

	public void setDR_CR_FLAG(String dR_CR_FLAG) {
		DR_CR_FLAG = dR_CR_FLAG;
	}

	public BigDecimal getACCT_AMT() {
		return ACCT_AMT;
	}

	public void setACCT_AMT(BigDecimal aCCT_AMT) {
		ACCT_AMT = aCCT_AMT;
	}

	public BigDecimal getDR_BAL() {
		return DR_BAL;
	}

	public void setDR_BAL(BigDecimal dR_BAL) {
		DR_BAL = dR_BAL;
	}

	public BigDecimal getCR_BAL() {
		return CR_BAL;
	}

	public void setCR_BAL(BigDecimal cR_BAL) {
		CR_BAL = cR_BAL;
	}

	public String getHOST_DATE() {
		return HOST_DATE;
	}

	public void setHOST_DATE(String hOST_DATE) {
		HOST_DATE = hOST_DATE;
	}

	public String getTX_TIME() {
		return TX_TIME;
	}

	public void setTX_TIME(String tX_TIME) {
		TX_TIME = tX_TIME;
	}

	public String getTRAN_CODE() {
		return TRAN_CODE;
	}

	public void setTRAN_CODE(String tRAN_CODE) {
		TRAN_CODE = tRAN_CODE;
	}

	public String getORI_SERV_SEQ_NO() {
		return ORI_SERV_SEQ_NO;
	}

	public void setORI_SERV_SEQ_NO(String oRI_SERV_SEQ_NO) {
		ORI_SERV_SEQ_NO = oRI_SERV_SEQ_NO;
	}

	public String getCCY() {
		return CCY;
	}

	public void setCCY(String cCY) {
		CCY = cCY;
	}

	public String getEXT_SEQ_NO() {
		return EXT_SEQ_NO;
	}

	public void setEXT_SEQ_NO(String eXT_SEQ_NO) {
		EXT_SEQ_NO = eXT_SEQ_NO;
	}

	public String getOTHR_ACCT_NO() {
		return OTHR_ACCT_NO;
	}

	public void setOTHR_ACCT_NO(String oTHR_ACCT_NO) {
		OTHR_ACCT_NO = oTHR_ACCT_NO;
	}

	public String getOTHR_ACCT_NM() {
		return OTHR_ACCT_NM;
	}

	public void setOTHR_ACCT_NM(String oTHR_ACCT_NM) {
		OTHR_ACCT_NM = oTHR_ACCT_NM;
	}

	public String getCASH_TRAN_FLAG() {
		return CASH_TRAN_FLAG;
	}

	public void setCASH_TRAN_FLAG(String cASH_TRAN_FLAG) {
		CASH_TRAN_FLAG = cASH_TRAN_FLAG;
	}

	public String getSMMRY_CD() {
		return SMMRY_CD;
	}

	public void setSMMRY_CD(String sMMRY_CD) {
		SMMRY_CD = sMMRY_CD;
	}

	public String getSMMRY_MSG() {
		return SMMRY_MSG;
	}

	public void setSMMRY_MSG(String sMMRY_MSG) {
		SMMRY_MSG = sMMRY_MSG;
	}

	public String getTRAN_BRANCH_ID() {
		return TRAN_BRANCH_ID;
	}

	public void setTRAN_BRANCH_ID(String tRAN_BRANCH_ID) {
		TRAN_BRANCH_ID = tRAN_BRANCH_ID;
	}

	public String getACCOUNTING_BRANCH_ID() {
		return ACCOUNTING_BRANCH_ID;
	}

	public void setACCOUNTING_BRANCH_ID(String aCCOUNTING_BRANCH_ID) {
		ACCOUNTING_BRANCH_ID = aCCOUNTING_BRANCH_ID;
	}
}