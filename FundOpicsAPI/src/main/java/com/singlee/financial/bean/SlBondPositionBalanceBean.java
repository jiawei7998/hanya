/**
 * Project Name:FundOpicsAPI
 * File Name:SlSposBean.java
 * Package Name:com.singlee.financial.bean
 * Date:2018-9-21下午04:02:49
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 债券持仓余额
 * 
 */
public class SlBondPositionBalanceBean implements Serializable{

	private static final long serialVersionUID = 1617452002885851010L;
	
	/**
	 * 债券代码
	 */
	private String secid;   
	/**
	 * 发行人
	 */
	private String issuer;  
	/**
	 * 债券类型
	 */
	private String acctngtype;  
	/**
	 * 成本中心，所属账户
	 */
	private String cost;   
	/**
	 * 投资组合，组合分类
	 */
	private String port;    
	/**
	 * 投资类型，账户类型
	 */
	private String invtype;   
	/**
	 * 下一付息日
	 */
	private String ipaydate;    
	/**
	 * 券面价值
	 */
	private String paramt;    
	/**
	 * 摊余成本
	 */
	private String amortamt;    
	/**
	 * 本日计提
	 */
	private String tdyintamt;  
	/**
	 * 截止本日计提
	 */
	private String accrintamt; 
	/**
	 * 未摊余额
	 */
	private String unamortamt;  
	/**
	 * 本日摊销
	 */
	private String tdyamortamt;
	
	/**
	 * 单位成本
	 */
	private String unitcost;   
	/**
	 * 止损净价
	 */
	private String cutpoint;   
	/**
	 * 公允价格
	 */
	private String clsgprice;
	/**
	 * 券面
	 */
	private String facevalue;   
	/**
	 * 公允价值
	 */
	private String fairvalue;  
	/**
	 * 实际收益率
	 */
	private String RealRate;
	/**
	 * 估值损益
	 */
	private String diffamt;   
	/**
	 * 起息日
	 */
	private String vdate;   
	/**
	 * 到期日
	 */
	private String mdate;  
	/**
	 * 票面利率
	 */
	private String couprate; 
	/**
	 * 剩余期限
	 */
	private String yearstomaturity;  
	/**
	 * 利率类型
	 */
	private String ratetype;   
	/**
	 * 付息频率
	 */
	private String intpaycycle;   
	/**
	 * 主体评级
	 */
	private String subjectrating;
	/**
	 * 债项评级
	 */
	private String bondtrating;
	/**
	 * 数据日期
	 */
	private String postdate;  
	
	private String pledgeface;  
	private String bookvalue;  
	private String issuertype;  
	
	
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public String getAcctngtype() {
		return acctngtype;
	}
	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}
	public String getIpaydate() {
		return ipaydate;
	}
	public void setIpaydate(String ipaydate) {
		this.ipaydate = ipaydate;
	}
	public String getParamt() {
		return paramt;
	}
	public void setParamt(String paramt) {
		this.paramt = paramt;
	}
	public String getAmortamt() {
		return amortamt;
	}
	public void setAmortamt(String amortamt) {
		this.amortamt = amortamt;
	}
	public String getTdyintamt() {
		return tdyintamt;
	}
	public void setTdyintamt(String tdyintamt) {
		this.tdyintamt = tdyintamt;
	}
	public String getAccrintamt() {
		return accrintamt;
	}
	public void setAccrintamt(String accrintamt) {
		this.accrintamt = accrintamt;
	}
	public String getUnamortamt() {
		return unamortamt;
	}
	public void setUnamortamt(String unamortamt) {
		this.unamortamt = unamortamt;
	}
	public String getTdyamortamt() {
		return tdyamortamt;
	}
	public void setTdyamortamt(String tdyamortamt) {
		this.tdyamortamt = tdyamortamt;
	}
	public String getUnitcost() {
		return unitcost;
	}
	public void setUnitcost(String unitcost) {
		this.unitcost = unitcost;
	}
	public String getCutpoint() {
		return cutpoint;
	}
	public void setCutpoint(String cutpoint) {
		this.cutpoint = cutpoint;
	}
	public String getClsgprice() {
		return clsgprice;
	}
	public void setClsgprice(String clsgprice) {
		this.clsgprice = clsgprice;
	}
	public String getFacevalue() {
		return facevalue;
	}
	public void setFacevalue(String facevalue) {
		this.facevalue = facevalue;
	}
	public String getFairvalue() {
		return fairvalue;
	}
	public void setFairvalue(String fairvalue) {
		this.fairvalue = fairvalue;
	}
	public String getRealRate() {
		return RealRate;
	}
	public void setRealRate(String realRate) {
		RealRate = realRate;
	}
	public String getDiffamt() {
		return diffamt;
	}
	public void setDiffamt(String diffamt) {
		this.diffamt = diffamt;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getCouprate() {
		return couprate;
	}
	public void setCouprate(String couprate) {
		this.couprate = couprate;
	}
	public String getYearstomaturity() {
		return yearstomaturity;
	}
	public void setYearstomaturity(String yearstomaturity) {
		this.yearstomaturity = yearstomaturity;
	}
	public String getRatetype() {
		return ratetype;
	}
	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}
	public String getIntpaycycle() {
		return intpaycycle;
	}
	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}
	public String getSubjectrating() {
		return subjectrating;
	}
	public void setSubjectrating(String subjectrating) {
		this.subjectrating = subjectrating;
	}
	public String getBondtrating() {
		return bondtrating;
	}
	public void setBondtrating(String bondtrating) {
		this.bondtrating = bondtrating;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public String getPostdate() {
		return postdate;
	}
	public String getPledgeface() {
		return pledgeface;
	}
	public void setPledgeface(String pledgeface) {
		this.pledgeface = pledgeface;
	}
	public String getBookvalue() {
		return bookvalue;
	}
	public void setBookvalue(String bookvalue) {
		this.bookvalue = bookvalue;
	}
	public String getIssuertype() {
		return issuertype;
	}
	public void setIssuertype(String issuertype) {
		this.issuertype = issuertype;
	}
	
	

}

