package com.singlee.financial.bean;



import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 汇率
 * @author LIJ
 *
 */
public class SlReportCreditrate implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 7411868537291029585L;

	/**
     * 货币对
     */
    private String ccypair;

    /**
     * 
     */
    private BigDecimal lineid;

    /**
     * 
     */
    private String rowsid;

    /**
     * 汇率
     */
    private BigDecimal spotrate;

    /**
     * sheet
     */
    private String sheet;

    /**
     * 报表日期
     */
    private String rpdate;

    /**
     * 货币对
     * @return CCYPAIR 货币对
     */
    public String getCcypair() {
        return ccypair;
    }

    /**
     * 货币对
     * @param ccypair 货币对
     */
    public void setCcypair(String ccypair) {
        this.ccypair = ccypair == null ? null : ccypair.trim();
    }

    /**
     * null
     * @return LINEID null
     */
    public BigDecimal getLineid() {
        return lineid;
    }

    /**
     * null
     * @param lineid null
     */
    public void setLineid(BigDecimal lineid) {
        this.lineid = lineid;
    }

    /**
     * null
     * @return ROWSID null
     */
    public String getRowsid() {
        return rowsid;
    }

    /**
     * null
     * @param rowsid null
     */
    public void setRowsid(String rowsid) {
        this.rowsid = rowsid == null ? null : rowsid.trim();
    }

    /**
     * 汇率
     * @return SPOTRATE 汇率
     */
    public BigDecimal getSpotrate() {
        return spotrate;
    }

    /**
     * 汇率
     * @param spotrate 汇率
     */
    public void setSpotrate(BigDecimal spotrate) {
        this.spotrate = spotrate;
    }

    /**
     * sheet
     * @return SHEET sheet
     */
    public String getSheet() {
        return sheet;
    }

    /**
     * sheet
     * @param sheet sheet
     */
    public void setSheet(String sheet) {
        this.sheet = sheet == null ? null : sheet.trim();
    }

    /**
     * 报表日期
     * @return RPDATE 报表日期
     */
    public String getRpdate() {
        return rpdate;
    }

    /**
     * 报表日期
     * @param rpdate 报表日期
     */
    public void setRpdate(String rpdate) {
        this.rpdate = rpdate == null ? null : rpdate.trim();
    }
}