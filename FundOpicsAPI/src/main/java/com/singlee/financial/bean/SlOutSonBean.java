package com.singlee.financial.bean;

import java.util.Map;

import com.singlee.financial.pojo.component.RetStatusEnum;


/**
 * 输出参数类
 * 
 * @author chenxh
 * 
 */
public class SlOutSonBean extends SlOutBean {
	private static final long serialVersionUID = 5944138040057282583L;
	
	public SlOutSonBean() {
		super();
	}
	
	public SlOutSonBean(RetStatusEnum retStatus, String retCode, String retMsg){
		super(retStatus, retCode, retMsg);
	}
	
	public SlOutSonBean(RetStatusEnum retStatus, String retCode, String retMsg, Map<String, Object> dataMap) {
		super(retStatus, retCode, retMsg);
		this.dataMap = dataMap;
	}
	

	private Map<String,Object> dataMap;

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}
}
