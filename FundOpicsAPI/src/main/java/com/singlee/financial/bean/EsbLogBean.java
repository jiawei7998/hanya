package com.singlee.financial.bean;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 泰隆银行接口日志记录
 */
public class EsbLogBean implements Serializable {

	private static final long serialVersionUID = 5107898234195563991L;

	/**
	 * 序号
	 */
	private String seq;

	/**
	 * 请求系统流水号
	 */
	private String bussSeqNo;

	/**
	 * 服务代码+场景
	 */
	private String serviceInfo;

	/**
	 * 请求报文信息
	 */
	private String requestXml;

	/**
	 * 返回报文信息
	 */
	private String responseXml;

	/**
	 * 触发用户
	 */
	private String userName;

	/**
	 * 发送时间
	 */
	private Timestamp sendTime;

	/**
	 * 接收时间
	 */
	private Timestamp recvTime;

	/**
	 * 备注1
	 */
	private String remark1;

	/**
	 * 备注2
	 */
	private String remark2;

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getBussSeqNo() {
		return bussSeqNo;
	}

	public void setBussSeqNo(String bussSeqNo) {
		this.bussSeqNo = bussSeqNo;
	}

	public String getServiceInfo() {
		return serviceInfo;
	}

	public void setServiceInfo(String serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	public String getRequestXml() {
		return requestXml;
	}

	public void setRequestXml(String requestXml) {
		this.requestXml = requestXml;
	}

	public String getResponseXml() {
		return responseXml;
	}

	public void setResponseXml(String responseXml) {
		this.responseXml = responseXml;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getSendTime() {
		return sendTime;
	}

	public void setSendTime(Timestamp sendTime) {
		this.sendTime = sendTime;
	}

	public Timestamp getRecvTime() {
		return recvTime;
	}

	public void setRecvTime(Timestamp recvTime) {
		this.recvTime = recvTime;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}
}