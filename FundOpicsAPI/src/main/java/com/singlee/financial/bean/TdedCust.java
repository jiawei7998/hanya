package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * IFBM客户总额度表
 * 
 * @author SINGLEE
 */
public class TdedCust implements Serializable {

	private static final long serialVersionUID = 1L;

	private String custCreditId;
	/**
	 * ECIF客户号
	 */
	private String ecifNum;
	/**
	 * CRMS客户名称
	 */
	private String customer;
	/**
	 * 额度品种
	 */
	private String productType;
	/**
	 * 币种
	 */
	private String currency;
	/**
	 * 授信额度
	 */
	private BigDecimal loanAmt;
	/**
	 * 可用金额
	 */
	private BigDecimal avlAmt;
	/**
	 * 期限ID
	 */
	private String term;
	/**
	 * 期限类型
	 */
	private String termType;
	/**
	 * 到期日
	 */
	private String dueDate;
	/**
	 * 状态
	 */
	private String cntrStatus;
	/**
	 * 操作时间
	 */
	private String opTime;

	/**
	 * 风险级别
	 */
	private int riskLevel;
	/**
	 * 优先级
	 */
	private String propertyL;
	/**
	 * 根据额度期限，类型，对年对月对日 时刻到期日
	 */
	private String edMdate;
	/**
	 * ED_MDATE-M_DATE 天数
	 */
	private int edMdays;
	/**
	 * 当前系统时间
	 */
	private String postDate;
	/**
	 * CREDIT_MDATE-系统时间 天数
	 */
	private int creditMdays;

	/**
	 * 调整额度
	 */
	private double adjAmt;
	/**
	 * 冻结额度
	 */
	private double frozenAmt;

	private String termName;

	public double getAdjAmt() {
		return adjAmt;
	}

	public void setAdjAmt(double adjAmt) {
		this.adjAmt = adjAmt;
	}

	public double getFrozenAmt() {
		return frozenAmt;
	}

	public void setFrozenAmt(double frozenAmt) {
		this.frozenAmt = frozenAmt;
	}

	public int getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(int riskLevel) {
		this.riskLevel = riskLevel;
	}

	public String getPropertyL() {
		return propertyL;
	}

	public void setPropertyL(String propertyL) {
		this.propertyL = propertyL;
	}

	public String getEdMdate() {
		return edMdate;
	}

	public void setEdMdate(String edMdate) {
		this.edMdate = edMdate;
	}

	public int getEdMdays() {
		return edMdays;
	}

	public void setEdMdays(int edMdays) {
		this.edMdays = edMdays;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public int getCreditMdays() {
		return creditMdays;
	}

	public void setCreditMdays(int creditMdays) {
		this.creditMdays = creditMdays;
	}

	public String getCustCreditId() {
		return custCreditId;
	}

	public void setCustCreditId(String custCreditId) {
		this.custCreditId = custCreditId;
	}

	public String getEcifNum() {
		return ecifNum;
	}

	public void setEcifNum(String ecifNum) {
		this.ecifNum = ecifNum;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	public BigDecimal getAvlAmt() {
		return avlAmt;
	}

	public void setAvlAmt(BigDecimal avlAmt) {
		this.avlAmt = avlAmt;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getTermType() {
		return termType;
	}

	public void setTermType(String termType) {
		this.termType = termType;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getCntrStatus() {
		return cntrStatus;
	}

	public void setCntrStatus(String cntrStatus) {
		this.cntrStatus = cntrStatus;
	}

	public String getOpTime() {
		return opTime;
	}

	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}

	/**
	 * 查询辅助字段
	 */
	/**
	 * 客户中文名称
	 */
	private String cnName;
	/**
	 * 额度中类名称
	 */
	private String creditName;
	/**
	 * 额度余量%
	 */
	private String percent;

	/**
	 * 串用额度
	 */
	private double astrAmt;
	/**
	 * 预串用额度
	 */
	private double preAstrAmt;
	/**
	 * 被串用额度
	 */
	private double acstrAmt;
	/**
	 * 预被串用额度
	 */
	private double preAcstrAmt;
	/**
	 * 占用额度
	 */
	private double usedAmt;
	/**
	 * 预占用额度
	 */
	private double preUsedAmt;

	public double getAstrAmt() {
		return astrAmt;
	}

	public void setAstrAmt(double astrAmt) {
		this.astrAmt = astrAmt;
	}

	public double getPreAstrAmt() {
		return preAstrAmt;
	}

	public void setPreAstrAmt(double preAstrAmt) {
		this.preAstrAmt = preAstrAmt;
	}

	public double getAcstrAmt() {
		return acstrAmt;
	}

	public void setAcstrAmt(double acstrAmt) {
		this.acstrAmt = acstrAmt;
	}

	public double getPreAcstrAmt() {
		return preAcstrAmt;
	}

	public void setPreAcstrAmt(double preAcstrAmt) {
		this.preAcstrAmt = preAcstrAmt;
	}

	public double getUsedAmt() {
		return usedAmt;
	}

	public void setUsedAmt(double usedAmt) {
		this.usedAmt = usedAmt;
	}

	public double getPreUsedAmt() {
		return preUsedAmt;
	}

	public void setPreUsedAmt(double preUsedAmt) {
		this.preUsedAmt = preUsedAmt;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public String getCreditName() {
		return creditName;
	}

	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}

	private int asegFlag;// 切大还是切小

	public int getAsegFlag() {
		return asegFlag;
	}

	public void setAsegFlag(int asegFlag) {
		this.asegFlag = asegFlag;
	}

	private int level;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}
}