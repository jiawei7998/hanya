package com.singlee.financial.bean;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 描述：SL_PMTQ_FX显示数据bean类
 * 
 * @author ZFH
 * @since 2013-01-28
 * @version 1.0v
 */
public class FxblBean implements java.io.Serializable {
    private static final long serialVersionUID = -772320120929091204L;
    
    private String SN           ;   //VARCHAR(20)       NOT NULL,   --交易流水
    private String BR           ;   //VARCHAR(2)        NULL,       --部门
    private String PRODUCT      ;   //VARCHAR(6)        NOT NULL,   --产品代码
    private String PRODTYPE     ;   //VARCHAR(2)        NULL,       --产品类型
    private String DEALNO       ;   //VARCHAR(8)        NULL,       --交易编号
    private String SEQ          ;   //VARCHAR(4)        NULL,       --序号
    private String PAYRECIND    ;   //VARCHAR(1)        NULL,       --收付标识 P-付款 
    private Date CDATE        ;   //DATETIME          NULL,       --创建日期
    private String CTIME        ;   //VARCHAR(8)        NULL,       --创建时间
    private String CCY          ;   //VARCHAR(3)        NULL,       --币种
    private BigDecimal AMOUNT       ;   //NUMERIC(19, 4)    NULL,       --金额
    private String SWIFTFMT     ;   //VARCHAR(4)        NULL,       --SWIFT报文类型
    private String SETTMEANS    ;   //VARCHAR(10)       NULL,       --清算方式
    private String SETACCT      ;   //VARCHAR(50)       NULL,       --清算账户
    private Date DEALDATE     ;   //DATETIME          NULL,       --交易日期
    private Date VDATE        ;   //DATETIME          NULL,       --起息日
    private String CNO          ;   //VARCHAR(11)       NULL,       --交易对手
    private String PBNKBIC      ;   //VARCHAR(11)       NULL,       --发起行BIC
    private String PBNKNM       ;   //NVARCHAR(60)      NULL,       --发起行名称
    private String PACCTNO      ;   //VARCHAR(34)       NULL,       --付款行账号
    private String PACCTNM      ;   //NVARCHAR(80)      NULL,       --付款行户名
    private String RBNKBIC      ;   //VARCHAR(11)       NULL,       --报文接收行BIC
    private String RBNKNM       ;   //NVARCHAR(60)      NULL,       --报文接收行名称
    private String AWACCT       ;   //VARCHAR(34)       NULL,       --收款行账户行账号
    private String AWBNKBIC     ;   //VARCHAR(11)       NULL,       --收款行账户行BIC
    private String AWBNKNM      ;   //VARCHAR(60)       NULL,       --收款行账户行名称
    private String RACCTNO      ;   //VARCHAR(34)       NULL,       --收款行账号
    private String RACCTNM      ;   //NVARCHAR(60)      NULL,       --收款行名称
    private String RACBIC       ;   //VARCHAR(11)       NULL,       --收款行BIC
    private String RACBNKFLAG   ;   //VARCHAR(1)        NULL,       --收款行标识 0-境内他行  1-境外行
    private String SCORRESBIC   ;   //VARCHAR(11)       NULL,       --发报方代理行代码
    private String SCORRESACCT  ;   //VARCHAR(34)       NULL,       --发报方代理行账号
    private String RCORRESBIC   ;   //VARCHAR(11)       NULL,       --收报代理行代码
    private String RCORRESACCT  ;   //VARCHAR(34)       NULL,       --收报代理行账号
    private String INTERBNKBIC  ;   //VARCHAR(11)       NULL,       --中间行代码
    private String INTERACCT    ;   //VARCHAR(34)       NULL,       --中间行账号
    private String PAYNOTE      ;   //NVARCHAR(100)     NULL,       --附言
    private String COREACCTNO   ;   //VARCHAR(34)       NULL,       --核心记账账号账号
    private String COREACCTNM   ;   //NVARCHAR(60)      NULL,       --核心记账户名   
    private String CUSTLOCATION ;   //NVARCHAR(50)      NULL,       --对手开户地
    private String PLOCATION    ;   //NVARCHAR(50)      NULL,       --付款方开户地
    private String RLOCATION    ;   //NVARCHAR(50)      NULL,       --收款方开户地  
    private String VOIDFLAG     ;   //VARCHAR(1)        NULL,       --撤单标志
    private String VERIND       ;   //VARCHAR(1)        NULL,       --经办复核标志
    private BigDecimal REALAMOUNT   ;   //NUMERIC(19, 4)    NULL,       --实际金额
    private BigDecimal BALANCE      ;   //NUMERIC(19, 4)    NULL,       --差额
    private String IOPER        ;   //VARCHAR(20)       NULL,       --经办人
    private String INOTE        ;   //NVARCHAR(100)     NULL,       --经办备注
    private Date IDATE        ;   //DATETIME          NULL,       --经办时间
    private String VEROPER      ;   //VARCHAR(20)       NULL,       --复核人
    private String VERNOTE      ;   //NVARCHAR(100)     NULL,       --复核备注
    private Date VERDATE      ;   //DATETIME          NULL,       --复核时间
    private String AUTHOPER     ;   //VARCHAR(20)       NULL,       --授权人
    private Date AUTHDATE     ;   //DATETIME          NULL,       --授权时间
    private String SENDFLAG     ;   //VARCHAR(1)        NULL,       --发送标识
    private String SENDOPER     ;   //VARCHAR(20)       NULL,       --发送人 
    private Date SENDDATE     ;   //DATETIME          NULL,       --发送时间
    private String SRRETUCD     ;   //VARCHAR(4)        NULL,       --响应码
    private String SRTRANDT     ;   //VARCHAR(10)       NULL,       --返回交易日期
    private String SRTRANSQ     ;   //VARCHAR(9)        NULL,       --返回外围大交易流水 
    private String TRANREFNO    ;   //VARCHAR(16)       NULL,       --业务编号
    private String RELATEREFNO  ;   //VARCHAR(16)       NULL,       --参考编号
    private String QRRETUCD     ;   //VARCHAR(8)        NULL,       --查询返回响应码 
    private String QRSTATUS     ;   //VARCHAR(2)          NULL,       --查询返回交易状态 0-记账成功  1-记账失败  2-交易重复  3-交易不存在
    private Date QRTIME       ;   // DATETIME          NULL        --查询返回时间 
    
    private String reSendFlg;   //重发标识  1-重发    其他-正常交易
    private String hostIntfCode = "50699";  //主机接口代码
    
    private String teller;//柜员
	private String areaCode;//地区
	private String branchNo;//网点号
    
    
    public String getSN() {
        return SN;
    }

    public void setSN(String sn) {
        SN = sn;
    }

    public String getBR() {
        return BR;
    }

    public void setBR(String br) {
        BR = br;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public void setPRODUCT(String product) {
        PRODUCT = product;
    }

    public String getPRODTYPE() {
        return PRODTYPE;
    }

    public void setPRODTYPE(String prodtype) {
        PRODTYPE = prodtype;
    }

    public String getDEALNO() {
        return DEALNO;
    }

    public void setDEALNO(String dealno) {
        DEALNO = dealno;
    }

    public String getSEQ() {
        return SEQ;
    }

    public void setSEQ(String seq) {
        SEQ = seq;
    }

    public String getPAYRECIND() {
        return PAYRECIND;
    }

    public void setPAYRECIND(String payrecind) {
        PAYRECIND = payrecind;
    }

    public Date getCDATE() {
        return CDATE;
    }

    public void setCDATE(Date cdate) {
        CDATE = cdate;
    }

    public String getCTIME() {
        return CTIME;
    }

    public void setCTIME(String ctime) {
        CTIME = ctime;
    }

    public String getCCY() {
        return CCY;
    }

    public void setCCY(String ccy) {
        CCY = ccy;
    }

    public BigDecimal getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(BigDecimal amount) {
        AMOUNT = amount;
    }

    public String getSWIFTFMT() {
        return SWIFTFMT;
    }

    public void setSWIFTFMT(String swiftfmt) {
        SWIFTFMT = swiftfmt;
    }

    public String getSETTMEANS() {
        return SETTMEANS;
    }

    public void setSETTMEANS(String settmeans) {
        SETTMEANS = settmeans;
    }

    public String getSETACCT() {
        return SETACCT;
    }

    public void setSETACCT(String setacct) {
        SETACCT = setacct;
    }

    public Date getDEALDATE() {
        return DEALDATE;
    }

    public void setDEALDATE(Date dealdate) {
        DEALDATE = dealdate;
    }

    public Date getVDATE() {
        return VDATE;
    }

    public void setVDATE(Date vdate) {
        VDATE = vdate;
    }

    public String getCNO() {
        return CNO;
    }

    public void setCNO(String cno) {
        CNO = cno;
    }

    public String getPBNKBIC() {
        return PBNKBIC;
    }

    public void setPBNKBIC(String pbnkbic) {
        PBNKBIC = pbnkbic;
    }

    public String getPBNKNM() {
        return PBNKNM;
    }

    public void setPBNKNM(String pbnknm) {
        PBNKNM = pbnknm;
    }

    public String getPACCTNO() {
        return PACCTNO;
    }

    public void setPACCTNO(String pacctno) {
        PACCTNO = pacctno;
    }

    public String getPACCTNM() {
        return PACCTNM;
    }

    public void setPACCTNM(String pacctnm) {
        PACCTNM = pacctnm;
    }

    public String getRBNKBIC() {
        return RBNKBIC;
    }

    public void setRBNKBIC(String rbnkbic) {
        RBNKBIC = rbnkbic;
    }

    public String getRBNKNM() {
        return RBNKNM;
    }

    public void setRBNKNM(String rbnknm) {
        RBNKNM = rbnknm;
    }

    public String getRACCTNO() {
        return RACCTNO;
    }

    public void setRACCTNO(String racctno) {
        RACCTNO = racctno;
    }

    public String getRACCTNM() {
        return RACCTNM;
    }

    public void setRACCTNM(String racctnm) {
        RACCTNM = racctnm;
    }

    public String getRACBIC() {
        return RACBIC;
    }

    public void setRACBIC(String racbic) {
        RACBIC = racbic;
    }

    public String getRACBNKFLAG() {
        return RACBNKFLAG;
    }

    public void setRACBNKFLAG(String racbnkflag) {
        RACBNKFLAG = racbnkflag;
    }

    public String getSCORRESBIC() {
        return SCORRESBIC;
    }

    public void setSCORRESBIC(String scorresbic) {
        SCORRESBIC = scorresbic;
    }

    public String getSCORRESACCT() {
        return SCORRESACCT;
    }

    public void setSCORRESACCT(String scorresacct) {
        SCORRESACCT = scorresacct;
    }

    public String getRCORRESBIC() {
        return RCORRESBIC;
    }

    public void setRCORRESBIC(String rcorresbic) {
        RCORRESBIC = rcorresbic;
    }

    public String getRCORRESACCT() {
        return RCORRESACCT;
    }

    public void setRCORRESACCT(String rcorresacct) {
        RCORRESACCT = rcorresacct;
    }

    public String getINTERBNKBIC() {
        return INTERBNKBIC;
    }

    public void setINTERBNKBIC(String interbnkbic) {
        INTERBNKBIC = interbnkbic;
    }

    public String getINTERACCT() {
        return INTERACCT;
    }

    public void setINTERACCT(String interacct) {
        INTERACCT = interacct;
    }

    public String getPAYNOTE() {
        return PAYNOTE;
    }

    public void setPAYNOTE(String paynote) {
        PAYNOTE = paynote;
    }

    public String getCOREACCTNO() {
        return COREACCTNO;
    }

    public void setCOREACCTNO(String coreacctno) {
        COREACCTNO = coreacctno;
    }

    public String getCOREACCTNM() {
        return COREACCTNM;
    }

    public void setCOREACCTNM(String coreacctnm) {
        COREACCTNM = coreacctnm;
    }

    public String getCUSTLOCATION() {
        return CUSTLOCATION;
    }

    public void setCUSTLOCATION(String custlocation) {
        CUSTLOCATION = custlocation;
    }

    public String getPLOCATION() {
        return PLOCATION;
    }

    public void setPLOCATION(String plocation) {
        PLOCATION = plocation;
    }

    public String getRLOCATION() {
        return RLOCATION;
    }

    public void setRLOCATION(String rlocation) {
        RLOCATION = rlocation;
    }

    public String getVOIDFLAG() {
        return VOIDFLAG;
    }

    public void setVOIDFLAG(String voidflag) {
        VOIDFLAG = voidflag;
    }

    public String getVERIND() {
        return VERIND;
    }

    public void setVERIND(String verind) {
        VERIND = verind;
    }

    public BigDecimal getREALAMOUNT() {
        return REALAMOUNT;
    }

    public void setREALAMOUNT(BigDecimal realamount) {
        REALAMOUNT = realamount;
    }

    public BigDecimal getBALANCE() {
        return BALANCE;
    }

    public void setBALANCE(BigDecimal balance) {
        BALANCE = balance;
    }

    public String getIOPER() {
        return IOPER;
    }

    public void setIOPER(String ioper) {
        IOPER = ioper;
    }

    public String getINOTE() {
        return INOTE;
    }

    public void setINOTE(String inote) {
        INOTE = inote;
    }

    public Date getIDATE() {
        return IDATE;
    }

    public void setIDATE(Date idate) {
        IDATE = idate;
    }

    public String getVEROPER() {
        return VEROPER;
    }

    public void setVEROPER(String veroper) {
        VEROPER = veroper;
    }

    public String getVERNOTE() {
        return VERNOTE;
    }

    public void setVERNOTE(String vernote) {
        VERNOTE = vernote;
    }

    public Date getVERDATE() {
        return VERDATE;
    }

    public void setVERDATE(Date verdate) {
        VERDATE = verdate;
    }

    public String getAUTHOPER() {
        return AUTHOPER;
    }

    public void setAUTHOPER(String authoper) {
        AUTHOPER = authoper;
    }

    public Date getAUTHDATE() {
        return AUTHDATE;
    }

    public void setAUTHDATE(Date authdate) {
        AUTHDATE = authdate;
    }

    public String getSENDFLAG() {
        return SENDFLAG;
    }

    public void setSENDFLAG(String sendflag) {
        SENDFLAG = sendflag;
    }

    public String getSENDOPER() {
        return SENDOPER;
    }

    public void setSENDOPER(String sendoper) {
        SENDOPER = sendoper;
    }

    public Date getSENDDATE() {
        return SENDDATE;
    }

    public void setSENDDATE(Date senddate) {
        SENDDATE = senddate;
    }

    public String getSRRETUCD() {
        return SRRETUCD;
    }

    public void setSRRETUCD(String srretucd) {
        SRRETUCD = srretucd;
    }

    public String getSRTRANDT() {
        return SRTRANDT;
    }

    public void setSRTRANDT(String srtrandt) {
        SRTRANDT = srtrandt;
    }

    public String getSRTRANSQ() {
        return SRTRANSQ;
    }

    public void setSRTRANSQ(String srtransq) {
        SRTRANSQ = srtransq;
    }

    public String getTRANREFNO() {
        return TRANREFNO;
    }

    public void setTRANREFNO(String tranrefno) {
        TRANREFNO = tranrefno;
    }

    public String getRELATEREFNO() {
        return RELATEREFNO;
    }

    public void setRELATEREFNO(String relaterefno) {
        RELATEREFNO = relaterefno;
    }

    public String getQRRETUCD() {
        return QRRETUCD;
    }

    public void setQRRETUCD(String qrretucd) {
        QRRETUCD = qrretucd;
    }

    public String getQRSTATUS() {
        return QRSTATUS;
    }

    public void setQRSTATUS(String qrstatus) {
        QRSTATUS = qrstatus;
    }

    public Date getQRTIME() {
        return QRTIME;
    }

    public void setQRTIME(Date qrtime) {
        QRTIME = qrtime;
    }

    public String getReSendFlg() {
        return reSendFlg;
    }

    public void setReSendFlg(String reSendFlg) {
        this.reSendFlg = reSendFlg;
    }

    public String getHostIntfCode() {
        return hostIntfCode;
    }

    public void setHostIntfCode(String hostIntfCode) {
        this.hostIntfCode = hostIntfCode;
    }


    public String getAWACCT() {
        return AWACCT;
    }

    public void setAWACCT(String awacct) {
        AWACCT = awacct;
    }

    public String getAWBNKBIC() {
        return AWBNKBIC;
    }

    public void setAWBNKBIC(String awbnkbic) {
        AWBNKBIC = awbnkbic;
    }

    public String getAWBNKNM() {
        return AWBNKNM;
    }

    public void setAWBNKNM(String awbnknm) {
        AWBNKNM = awbnknm;
    }

	public void setTeller(String teller) {
		this.teller = teller;
	}

	public String getTeller() {
		return teller;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setBranchNo(String branchNo) {
		this.branchNo = branchNo;
	}

	public String getBranchNo() {
		return branchNo;
	}

}
