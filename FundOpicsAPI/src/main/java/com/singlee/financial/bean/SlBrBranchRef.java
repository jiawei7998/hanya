package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 华商银行-机构地区网点
 * @author zhengfl
 *
 */
public class SlBrBranchRef implements Serializable {
	
    private static final long serialVersionUID = 109802878537885952L;

    /**
     * 机构
     */
    private String br;
    
    /**
     * 地区
     */
    private String areaCode;
    
    /**
     * 网点号
     */
    private String branchNo;
    
    /**
     * 柜员
     */
    private String teller;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(String branchNo) {
        this.branchNo = branchNo;
    }
    
    public String getTeller() {
        return teller;
    }

    public void setTeller(String teller) {
        this.teller = teller;
    }

    
}
