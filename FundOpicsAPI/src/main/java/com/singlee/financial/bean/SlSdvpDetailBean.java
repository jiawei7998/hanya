package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * sdvp明细
 * 
 * @author Qxj
 */
public class SlSdvpDetailBean implements Serializable {

	private static final long serialVersionUID = -5766006871895531270L;
	/**
	 * 分支
	 */
	private String br;
	/**
	 * 交易对手编号
	 */
	private String cno;
	/**
	 * 交割收款标识
	 */
	private String delrecind;
	/**
	 * 产品代码
	 */
	private String product;
	/**
	 * 产品类型
	 */
	private String prodtype;
	/**
	 * 币种
	 */
	private String ccy;
	/**
	 * 托管账户
	 */
	private String safekeepacct;

	private String dcc;

	private String accountno;

	private String bic;

	private String c1;

	private String c2;

	private String c3;

	private String c4;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getDelrecind() {
		return delrecind;
	}

	public void setDelrecind(String delrecind) {
		this.delrecind = delrecind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSafekeepacct() {
		return safekeepacct;
	}

	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}

	public String getDcc() {
		return dcc;
	}

	public void setDcc(String dcc) {
		this.dcc = dcc;
	}

	public String getAccountno() {
		return accountno;
	}

	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getC1() {
		return c1;
	}

	public void setC1(String c1) {
		this.c1 = c1;
	}

	public String getC2() {
		return c2;
	}

	public void setC2(String c2) {
		this.c2 = c2;
	}

	public String getC3() {
		return c3;
	}

	public void setC3(String c3) {
		this.c3 = c3;
	}

	public String getC4() {
		return c4;
	}

	public void setC4(String c4) {
		this.c4 = c4;
	}
}