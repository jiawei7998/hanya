package com.singlee.financial.bean;

import java.io.Serializable;
/**
 *  G31 有价证券及投资情况表  实体
 */
public class SlReportG31Bean implements Serializable {


	private static final long serialVersionUID = 3665414502654983661L;
	
	private String tableid;
	private String br;
	private String lineid;
	private String rowsid;
	private Double amt1;//正面价值
	private Double amt2;//公允价值
	private String rpdate;
	
	
	public String getTableid() {
		return tableid;
	}


	public void setTableid(String tableid) {
		this.tableid = tableid;
	}


	public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getLineid() {
		return lineid;
	}


	public void setLineid(String lineid) {
		this.lineid = lineid;
	}


	public String getRowsid() {
		return rowsid;
	}


	public void setRowsid(String rowsid) {
		this.rowsid = rowsid;
	}


	public Double getAmt1() {
		return amt1;
	}


	public void setAmt1(Double amt1) {
		this.amt1 = amt1;
	}


	public Double getAmt2() {
		return amt2;
	}


	public void setAmt2(Double amt2) {
		this.amt2 = amt2;
	}


	public String getRpdate() {
		return rpdate;
	}


	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}


	@Override
	public String toString() {
		return "SlReportG31Bean [tableid=" + tableid + ", br=" + br
				+ ", lineid=" + lineid + ", rowsid=" + rowsid + ", amt1="
				+ amt1 + ", amt2=" + amt2 + ", rpdate=" + rpdate + "]";
	}
	
	

}
