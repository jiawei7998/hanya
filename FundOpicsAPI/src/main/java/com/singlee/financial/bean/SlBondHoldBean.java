package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 债券持仓查询
 * 
 * @author wangzt
 *
 */
public class SlBondHoldBean implements Serializable {

    /**
     * 唯一ID
     */
    private static final long serialVersionUID = -8699600309200663140L;
    /**
     * 债券类型
     */
    private String br;
    /**
     * 债券类型
     */
    private String port;
    /**
     * 债券类型
     */
    private String acctngtype;
    /**
     * 债券代码
     */
    private String secid;

    /**
     * 债券名称
     */
    private String descr;

    /**
     * 账户分类
     */
    private String invtype;

    /**
     * 发行人
     */
    private String cmne;

    /**
     * 剩余年限
     */
    private BigDecimal tenor;

    /**
     * 付息剩余天数
     */
    private String intday;

    /**
     * 付息方式
     */
    private String intpaycycle;

    /**
     * 下个付息日
     */
    private String ipaydate;

    /**
     * 年限
     */
    private String years;

    /**
     * 债券信用级别
     */
    private String rating;

    /**
     * 含权类
     */
    private String weightedclass;

    /**
     * 行权日
     */
    private String exercisedate;

    /**
     * 行权剩余年限
     */
    private BigDecimal exerciseyears;

    /**
     * 起息日
     */
    private String vdate;

    /**
     * 到期日
     */
    private String mdate;

    /**
     * 利率方式
     */
    private String ratecode;

    /**
     * 债券主体评级
     */
    private String subjectrating;

    /**
     * 票面利率
     */
    private BigDecimal couprate_8;
    /**
     * 持仓面额(元)
     */
    private BigDecimal prinamt;
    /**
     * 已质押面额
     */
    private BigDecimal pledgedfaceamt;
    /**
     * 全价成本(元)
     */
    private BigDecimal fullcost;
    /**
     * 净价成本(元)
     */
    private BigDecimal netcost;
    /**
     * 公允价值调整
     */
    private BigDecimal unamortamt;
    /**
     * 账面余额(元)
     */
    private BigDecimal settavgcost;
    /**
     * 应收利息
     */
    private BigDecimal accr_intamt;
    /**
     * 本金
     */
    private BigDecimal amt;
    /**
     * 百元净价成本
     */
    private BigDecimal unitcost;
    /**
     * 百元全价成本
     */
    private BigDecimal netpricecost;
    /**
     * 到期收益率
     */
    private String tclsgprice_8;
    /**
     * 百元净价估值
     */
    private BigDecimal sclsgprice_8;
    /**
     * 百元全价估值
     */
    private BigDecimal sclsgprice2_8;
    /**
     * 净价浮盈(元)
     */
    private BigDecimal diff_amt;
    /**
     * 浮动盈亏比
     */
    private String floatamt;
    /**
     * 币种
     */
    private String ccy;
    /**
     * 成本中心
     */
    private String cost;
    /**
     * 业务类型
     */
    private String product;
    /**
     * qty
     */
    private String qty;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAcctngtype() {
        return acctngtype;
    }

    public void setAcctngtype(String acctngtype) {
        this.acctngtype = acctngtype;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    public String getCmne() {
        return cmne;
    }

    public void setCmne(String cmne) {
        this.cmne = cmne;
    }

    public BigDecimal getTenor() {
        return tenor;
    }

    public void setTenor(BigDecimal tenor) {
        this.tenor = tenor;
    }

    public String getIntday() {
        return intday;
    }

    public void setIntday(String intday) {
        this.intday = intday;
    }

    public String getIntpaycycle() {
        return intpaycycle;
    }

    public void setIntpaycycle(String intpaycycle) {
        this.intpaycycle = intpaycycle;
    }

    public String getIpaydate() {
        return ipaydate;
    }

    public void setIpaydate(String ipaydate) {
        this.ipaydate = ipaydate;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getWeightedclass() {
        return weightedclass;
    }

    public void setWeightedclass(String weightedclass) {
        this.weightedclass = weightedclass;
    }

    public String getExercisedate() {
        return exercisedate;
    }

    public void setExercisedate(String exercisedate) {
        this.exercisedate = exercisedate;
    }

    public BigDecimal getExerciseyears() {
        return exerciseyears;
    }

    public void setExerciseyears(BigDecimal exerciseyears) {
        this.exerciseyears = exerciseyears;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public String getRatecode() {
        return ratecode;
    }

    public void setRatecode(String ratecode) {
        this.ratecode = ratecode;
    }

    public String getSubjectrating() {
        return subjectrating;
    }

    public void setSubjectrating(String subjectrating) {
        this.subjectrating = subjectrating;
    }

    public BigDecimal getCouprate_8() {
        return couprate_8;
    }

    public void setCouprate_8(BigDecimal couprate_8) {
        this.couprate_8 = couprate_8;
    }

    public BigDecimal getPrinamt() {
        return prinamt;
    }

    public void setPrinamt(BigDecimal prinamt) {
        this.prinamt = prinamt;
    }

    public BigDecimal getPledgedfaceamt() {
        return pledgedfaceamt;
    }

    public void setPledgedfaceamt(BigDecimal pledgedfaceamt) {
        this.pledgedfaceamt = pledgedfaceamt;
    }

    public BigDecimal getFullcost() {
        return fullcost;
    }

    public void setFullcost(BigDecimal fullcost) {
        this.fullcost = fullcost;
    }

    public BigDecimal getNetcost() {
        return netcost;
    }

    public void setNetcost(BigDecimal netcost) {
        this.netcost = netcost;
    }

    public BigDecimal getUnamortamt() {
        return unamortamt;
    }

    public void setUnamortamt(BigDecimal unamortamt) {
        this.unamortamt = unamortamt;
    }

    public BigDecimal getSettavgcost() {
        return settavgcost;
    }

    public void setSettavgcost(BigDecimal settavgcost) {
        this.settavgcost = settavgcost;
    }

    public BigDecimal getAccr_intamt() {
        return accr_intamt;
    }

    public void setAccr_intamt(BigDecimal accr_intamt) {
        this.accr_intamt = accr_intamt;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getUnitcost() {
        return unitcost;
    }

    public void setUnitcost(BigDecimal unitcost) {
        this.unitcost = unitcost;
    }

    public BigDecimal getNetpricecost() {
        return netpricecost;
    }

    public void setNetpricecost(BigDecimal netpricecost) {
        this.netpricecost = netpricecost;
    }

    public String getTclsgprice_8() {
        return tclsgprice_8;
    }

    public void setTclsgprice_8(String tclsgprice_8) {
        this.tclsgprice_8 = tclsgprice_8;
    }

    public BigDecimal getSclsgprice_8() {
        return sclsgprice_8;
    }

    public void setSclsgprice_8(BigDecimal sclsgprice_8) {
        this.sclsgprice_8 = sclsgprice_8;
    }

    public BigDecimal getSclsgprice2_8() {
        return sclsgprice2_8;
    }

    public void setSclsgprice2_8(BigDecimal sclsgprice2_8) {
        this.sclsgprice2_8 = sclsgprice2_8;
    }

    public BigDecimal getDiff_amt() {
        return diff_amt;
    }

    public void setDiff_amt(BigDecimal diff_amt) {
        this.diff_amt = diff_amt;
    }

    public String getFloatamt() {
        return floatamt;
    }

    public void setFloatamt(String floatamt) {
        this.floatamt = floatamt;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}