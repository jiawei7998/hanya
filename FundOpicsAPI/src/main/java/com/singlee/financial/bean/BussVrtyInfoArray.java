package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 业务种类信息数组
 */
public class BussVrtyInfoArray implements Serializable {

	private static final long serialVersionUID = -763271276478626110L;

	/**
	 * 业务类型
	 */
	private String BUSS_TYPE;

	/**
	 * 业务类型名称
	 */
	private String BUSS_TP_NM;

	public String getBUSS_TYPE() {
		return BUSS_TYPE;
	}

	public void setBUSS_TYPE(String bUSS_TYPE) {
		BUSS_TYPE = bUSS_TYPE;
	}

	public String getBUSS_TP_NM() {
		return BUSS_TP_NM;
	}

	public void setBUSS_TP_NM(String bUSS_TP_NM) {
		BUSS_TP_NM = bUSS_TP_NM;
	}
}