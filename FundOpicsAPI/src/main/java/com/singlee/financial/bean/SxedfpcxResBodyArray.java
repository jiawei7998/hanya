package com.singlee.financial.bean;

import java.io.Serializable;

public class SxedfpcxResBodyArray implements Serializable {

	private static final long serialVersionUID = 2122990014301819184L;

	/**
	 * 额度信息流水号
	 */
	private String LINE_SEQ_NO;

	/**
	 * 客户号
	 */
	private String CLIENT_NO;

	/**
	 * 客户名称
	 */
	private String CLIENT_NAME;

	/**
	 * 业务名称
	 */
	private String BUSS_NAME;

	/**
	 * 循环标志
	 */
	private String CYCLE_FLAG;

	/**
	 * 保证金比例
	 */
	private String MRGN_RTO;

	/**
	 * 授信额度
	 */
	private String CRDT_LMT;

	/**
	 * 敞口额度（ESB保留字段）
	 */
	private String OPN_LMT;

	/**
	 * 经办柜员（ESB保留字段）
	 */
	private String APP_USER_ID;

	/**
	 * 经办人姓名（ESB保留字段）
	 */
	private String APP_USER_NAME;

	/**
	 * 经办日期（ESB保留字段）
	 */
	private String APP_TRAN_DATE;

	/**
	 * 开始日期
	 */
	private String START_DATE;

	/**
	 * 终止日期
	 */
	private String END_DATE;

	/**
	 * 币种
	 */
	private String CCY;

	/**
	 * 宽限期
	 */
	private String GRC_PRD;

	/**
	 * 担保方式
	 */
	private String ENSURE_MODE;

	/**
	 * 额度状态
	 */
	private String LMT_ST;

	/**
	 * 经办机构名称
	 */
	private String APP_BRANCH_NAME;

	/**
	 * 登记人姓名
	 */
	private String REGISTER_USER_NAME;

	/**
	 * 登记机构名称
	 */
	private String REGISTER_BRANCH_NAME;

	/**
	 * 登记日期
	 */
	private String REGISTER_DATE;

	/**
	 * 更新日期
	 */
	private String UPDATE_DATE;

	/**
	 * 使用机构名称
	 */
	private String USE_ORG_NM;

	/**
	 * 产品编号
	 */
	private String PRDCT_NO;

	/**
	 * 使用机构号
	 */
	private String USE_ORG_ID;

	/**
	 * 可用授信总额
	 */
	private String AVL_CRDT_TOT_AMT;

	/**
	 * 可用授信敞口
	 */
	private String AVL_CRDT_EAD_AMT;

	public String getLINE_SEQ_NO() {
		return LINE_SEQ_NO;
	}

	public void setLINE_SEQ_NO(String lINE_SEQ_NO) {
		LINE_SEQ_NO = lINE_SEQ_NO;
	}

	public String getCLIENT_NO() {
		return CLIENT_NO;
	}

	public void setCLIENT_NO(String cLIENT_NO) {
		CLIENT_NO = cLIENT_NO;
	}

	public String getCLIENT_NAME() {
		return CLIENT_NAME;
	}

	public void setCLIENT_NAME(String cLIENT_NAME) {
		CLIENT_NAME = cLIENT_NAME;
	}

	public String getBUSS_NAME() {
		return BUSS_NAME;
	}

	public void setBUSS_NAME(String bUSS_NAME) {
		BUSS_NAME = bUSS_NAME;
	}

	public String getCYCLE_FLAG() {
		return CYCLE_FLAG;
	}

	public void setCYCLE_FLAG(String cYCLE_FLAG) {
		CYCLE_FLAG = cYCLE_FLAG;
	}

	public String getMRGN_RTO() {
		return MRGN_RTO;
	}

	public void setMRGN_RTO(String mRGN_RTO) {
		MRGN_RTO = mRGN_RTO;
	}

	public String getCRDT_LMT() {
		return CRDT_LMT;
	}

	public void setCRDT_LMT(String cRDT_LMT) {
		CRDT_LMT = cRDT_LMT;
	}

	public String getOPN_LMT() {
		return OPN_LMT;
	}

	public void setOPN_LMT(String oPN_LMT) {
		OPN_LMT = oPN_LMT;
	}

	public String getAPP_USER_ID() {
		return APP_USER_ID;
	}

	public void setAPP_USER_ID(String aPP_USER_ID) {
		APP_USER_ID = aPP_USER_ID;
	}

	public String getAPP_USER_NAME() {
		return APP_USER_NAME;
	}

	public void setAPP_USER_NAME(String aPP_USER_NAME) {
		APP_USER_NAME = aPP_USER_NAME;
	}

	public String getAPP_TRAN_DATE() {
		return APP_TRAN_DATE;
	}

	public void setAPP_TRAN_DATE(String aPP_TRAN_DATE) {
		APP_TRAN_DATE = aPP_TRAN_DATE;
	}

	public String getSTART_DATE() {
		return START_DATE;
	}

	public void setSTART_DATE(String sTART_DATE) {
		START_DATE = sTART_DATE;
	}

	public String getEND_DATE() {
		return END_DATE;
	}

	public void setEND_DATE(String eND_DATE) {
		END_DATE = eND_DATE;
	}

	public String getCCY() {
		return CCY;
	}

	public void setCCY(String cCY) {
		CCY = cCY;
	}

	public String getGRC_PRD() {
		return GRC_PRD;
	}

	public void setGRC_PRD(String gRC_PRD) {
		GRC_PRD = gRC_PRD;
	}

	public String getENSURE_MODE() {
		return ENSURE_MODE;
	}

	public void setENSURE_MODE(String eNSURE_MODE) {
		ENSURE_MODE = eNSURE_MODE;
	}

	public String getLMT_ST() {
		return LMT_ST;
	}

	public void setLMT_ST(String lMT_ST) {
		LMT_ST = lMT_ST;
	}

	public String getAPP_BRANCH_NAME() {
		return APP_BRANCH_NAME;
	}

	public void setAPP_BRANCH_NAME(String aPP_BRANCH_NAME) {
		APP_BRANCH_NAME = aPP_BRANCH_NAME;
	}

	public String getREGISTER_USER_NAME() {
		return REGISTER_USER_NAME;
	}

	public void setREGISTER_USER_NAME(String rEGISTER_USER_NAME) {
		REGISTER_USER_NAME = rEGISTER_USER_NAME;
	}

	public String getREGISTER_BRANCH_NAME() {
		return REGISTER_BRANCH_NAME;
	}

	public void setREGISTER_BRANCH_NAME(String rEGISTER_BRANCH_NAME) {
		REGISTER_BRANCH_NAME = rEGISTER_BRANCH_NAME;
	}

	public String getREGISTER_DATE() {
		return REGISTER_DATE;
	}

	public void setREGISTER_DATE(String rEGISTER_DATE) {
		REGISTER_DATE = rEGISTER_DATE;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getUSE_ORG_NM() {
		return USE_ORG_NM;
	}

	public void setUSE_ORG_NM(String uSE_ORG_NM) {
		USE_ORG_NM = uSE_ORG_NM;
	}

	public String getPRDCT_NO() {
		return PRDCT_NO;
	}

	public void setPRDCT_NO(String pRDCT_NO) {
		PRDCT_NO = pRDCT_NO;
	}

	public String getUSE_ORG_ID() {
		return USE_ORG_ID;
	}

	public void setUSE_ORG_ID(String uSE_ORG_ID) {
		USE_ORG_ID = uSE_ORG_ID;
	}

	public String getAVL_CRDT_TOT_AMT() {
		return AVL_CRDT_TOT_AMT;
	}

	public void setAVL_CRDT_TOT_AMT(String aVL_CRDT_TOT_AMT) {
		AVL_CRDT_TOT_AMT = aVL_CRDT_TOT_AMT;
	}

	public String getAVL_CRDT_EAD_AMT() {
		return AVL_CRDT_EAD_AMT;
	}

	public void setAVL_CRDT_EAD_AMT(String aVL_CRDT_EAD_AMT) {
		AVL_CRDT_EAD_AMT = aVL_CRDT_EAD_AMT;
	}
}