package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 国民银行发送交易数据到核心(结构衍生)
 * 金额18,2，日期yyyyMMdd
 * @author xuqq
 *
 */
public class KBStruckBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
     * 交易主键
     */
    private String dealid;

    /**
     * 交易状态（0：新建，1导入opics成功，2.导入opics失败，3作废）
     */
    private String status;

    /**
     * 类型（0：汇率，1：利率）
     */
    private String dealtype;

    /**
     * 操作人id
     */
    private String sponsor;

    /**
     * 操作人机构
     */
    private String sponinst;

    /**
     * 修改日期和时间
     */
    private String updatedate;

    /**
     * 外部合同号
     */
    private String contractno;

    /**
     * 分行
     */
    private String branchid;

    /**
     * 核心产品号
     */
    private String coreproductno;

    /**
     * 交易日
     */
    private String dealdate;

    /**
     * 观察起始日
     */
    private String observevaluedate;

    /**
     * 交易费支付日
     */
    private String dealfeepaydate;

    /**
     * 期权收益支付日
     */
    private String optionprofitpaydate;

    /**
     * 存款本金币种
     */
    private String ccy;
    
    /**
     * 存款本金（元）
     */
    private BigDecimal amt;

    /**
     * 币种1
     */
    private String ccy1;

    /**
     * 币种2
     */
    private String ccy2;

    /**
     * 观测标的下限
     */
    private BigDecimal observetargetlower;

    /**
     * 观测标的确定
     */
    private String observetargetsure;

    /**
     * 计息天数
     */
    private BigDecimal interestdays;

    /**
     * 起息日
     */
    private String valuedate;

    /**
     * 观察结束日
     */
    private String observeenddate;

    /**
     * 期权费支付日
     */
    private String optionfeepaydate;

    /**
     * 到期日
     */
    private String enddate;

    /**
     * 观测标的
     */
    private String observetarget;

    /**
     * 金额1
     */
    private BigDecimal amt1;

    /**
     * 金额2
     */
    private BigDecimal amt2;

    /**
     * 观测标的上限
     */
    private BigDecimal observetargettop;

    /**
     * 工作日调整惯例
     */
    private String adjustday;

    /**
     * 观测标的工作日
     */
    private String observetargetdate;

    /**
     * 期权费支付方向
     */
    private String optiondirect;

    /**
     * 期权费币种
     */
    private String optionccy;

    /**
     * 期权费金额
     */
    private BigDecimal optionamt;

    /**
     * pay off支付方向
     */
    private String payoffdirect;

    /**
     * pay off币种
     */
    private String payoffccy;

    /**
     * pay off金额
     */
    private BigDecimal payoffamt;

    /**
     * 交易费支付方向
     */
    private String dealdirect;

    /**
     * 交易费币种
     */
    private String dealccy;

    /**
     * 交易费金额
     */
    private BigDecimal dealamt;

    /**
     * 后端交易费支付方向
     */
    private String backenddealdirect;

    /**
     * 后端交易费币种
     */
    private String backenddealccy;

    /**
     * 后端交易费金额
     */
    private BigDecimal backenddealamt;

    /**
     * 清算方式
     */
    private String settlemode;

    /**
     * 清算工作日（拼音全拼）
     */
    private String settleday;

    /**
     * 描述
     */
    private String remark;

    /**
     * 债券号(利率结构衍生)
     */
    private String bondno;

    /**
     * 计息期开始(利率结构衍生)
     */
    private String intereststartdate;

    /**
     * 计息期结束(利率结构衍生)
     */
    private String interestenddate;

    /**
     * 交易方向
     */
    private String direct;
    /**
     * 交付条款其他信息
     */
    private String itemremark;
    
    /**
     * 后端交易费支付日
     */
    private String backenddealfeepaydate;
    /**
     * 浮动利率
     */
    private BigDecimal floatrate;
    /**
     * 计息基础
     */
    private String basis;
    /**
     * 即期汇率
     */
    private BigDecimal rate;
    /**
     * opics dealno
     */
    private String dealno;
    /**
     * 观察日
     */
    private String observedate;
    /**
     * 交易发送核心（0：未发送，1发送成功，2发送失败）
     */
    private String sendstatus;
    
    //opics数据
    private BigDecimal mtm;//估值
    private String dealFlag;//opics交易状态（1，正常，2。行权，3.取消）
    
    //交易发送核心反馈结果start
    private String prcssstusdstcd;//处理状态(Y/N)
    private String brncomgtno;//KBStar交易编号 交易登记完成后返回值(变更/取消操作时必输项)
    private String refserno;//KBStar交易序号 交易登记完成后返回值(变更/取消操作时必输项)
    private String serno;//当日序号
    private String slipno;//会计传票号码
    private String swiftno;//Swift电文编号
    private String cnapstmpno;//Cnaps电文编号
    private String outvalue;//查询结果(例:交易编号+交易序号+Opics交易序号+交易状态)
    /**
     * kremit状态
     */
    private String returnstatus;
    //交易发送核心反馈结果end
    
  //兼容其它模板
    private String updown;
    private BigDecimal triger;
    
    private String putcall;
    private BigDecimal strike;
    private String isrange;
    
	public String getDealid() {
		return dealid;
	}
	public void setDealid(String dealid) {
		this.dealid = dealid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDealtype() {
		return dealtype;
	}
	public void setDealtype(String dealtype) {
		this.dealtype = dealtype;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getSponinst() {
		return sponinst;
	}
	public void setSponinst(String sponinst) {
		this.sponinst = sponinst;
	}
	public String getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	public String getContractno() {
		return contractno;
	}
	public void setContractno(String contractno) {
		this.contractno = contractno;
	}
	public String getBranchid() {
		return branchid;
	}
	public void setBranchid(String branchid) {
		this.branchid = branchid;
	}
	public String getCoreproductno() {
		return coreproductno;
	}
	public void setCoreproductno(String coreproductno) {
		this.coreproductno = coreproductno;
	}
	public String getDealdate() {
		return dealdate;
	}
	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}
	public String getObservevaluedate() {
		return observevaluedate;
	}
	public void setObservevaluedate(String observevaluedate) {
		this.observevaluedate = observevaluedate;
	}
	public String getDealfeepaydate() {
		return dealfeepaydate;
	}
	public void setDealfeepaydate(String dealfeepaydate) {
		this.dealfeepaydate = dealfeepaydate;
	}
	public String getOptionprofitpaydate() {
		return optionprofitpaydate;
	}
	public void setOptionprofitpaydate(String optionprofitpaydate) {
		this.optionprofitpaydate = optionprofitpaydate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public String getCcy1() {
		return ccy1;
	}
	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}
	public String getCcy2() {
		return ccy2;
	}
	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}
	public BigDecimal getObservetargetlower() {
		return observetargetlower;
	}
	public void setObservetargetlower(BigDecimal observetargetlower) {
		this.observetargetlower = observetargetlower;
	}
	public String getObservetargetsure() {
		return observetargetsure;
	}
	public void setObservetargetsure(String observetargetsure) {
		this.observetargetsure = observetargetsure;
	}
	public BigDecimal getInterestdays() {
		return interestdays;
	}
	public void setInterestdays(BigDecimal interestdays) {
		this.interestdays = interestdays;
	}
	public String getValuedate() {
		return valuedate;
	}
	public void setValuedate(String valuedate) {
		this.valuedate = valuedate;
	}
	public String getObserveenddate() {
		return observeenddate;
	}
	public void setObserveenddate(String observeenddate) {
		this.observeenddate = observeenddate;
	}
	public String getOptionfeepaydate() {
		return optionfeepaydate;
	}
	public void setOptionfeepaydate(String optionfeepaydate) {
		this.optionfeepaydate = optionfeepaydate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getObservetarget() {
		return observetarget;
	}
	public void setObservetarget(String observetarget) {
		this.observetarget = observetarget;
	}
	public BigDecimal getAmt1() {
		return amt1;
	}
	public void setAmt1(BigDecimal amt1) {
		this.amt1 = amt1;
	}
	public BigDecimal getAmt2() {
		return amt2;
	}
	public void setAmt2(BigDecimal amt2) {
		this.amt2 = amt2;
	}
	public BigDecimal getObservetargettop() {
		return observetargettop;
	}
	public void setObservetargettop(BigDecimal observetargettop) {
		this.observetargettop = observetargettop;
	}
	public String getAdjustday() {
		return adjustday;
	}
	public void setAdjustday(String adjustday) {
		this.adjustday = adjustday;
	}
	public String getObservetargetdate() {
		return observetargetdate;
	}
	public void setObservetargetdate(String observetargetdate) {
		this.observetargetdate = observetargetdate;
	}
	public String getOptiondirect() {
		return optiondirect;
	}
	public void setOptiondirect(String optiondirect) {
		this.optiondirect = optiondirect;
	}
	public String getOptionccy() {
		return optionccy;
	}
	public void setOptionccy(String optionccy) {
		this.optionccy = optionccy;
	}
	public BigDecimal getOptionamt() {
		return optionamt;
	}
	public void setOptionamt(BigDecimal optionamt) {
		this.optionamt = optionamt;
	}
	public String getPayoffdirect() {
		return payoffdirect;
	}
	public void setPayoffdirect(String payoffdirect) {
		this.payoffdirect = payoffdirect;
	}
	public String getPayoffccy() {
		return payoffccy;
	}
	public void setPayoffccy(String payoffccy) {
		this.payoffccy = payoffccy;
	}
	public BigDecimal getPayoffamt() {
		return payoffamt;
	}
	public void setPayoffamt(BigDecimal payoffamt) {
		this.payoffamt = payoffamt;
	}
	public String getDealdirect() {
		return dealdirect;
	}
	public void setDealdirect(String dealdirect) {
		this.dealdirect = dealdirect;
	}
	public String getDealccy() {
		return dealccy;
	}
	public void setDealccy(String dealccy) {
		this.dealccy = dealccy;
	}
	public BigDecimal getDealamt() {
		return dealamt;
	}
	public void setDealamt(BigDecimal dealamt) {
		this.dealamt = dealamt;
	}
	public String getBackenddealdirect() {
		return backenddealdirect;
	}
	public void setBackenddealdirect(String backenddealdirect) {
		this.backenddealdirect = backenddealdirect;
	}
	public String getBackenddealccy() {
		return backenddealccy;
	}
	public void setBackenddealccy(String backenddealccy) {
		this.backenddealccy = backenddealccy;
	}
	public BigDecimal getBackenddealamt() {
		return backenddealamt;
	}
	public void setBackenddealamt(BigDecimal backenddealamt) {
		this.backenddealamt = backenddealamt;
	}
	public String getSettlemode() {
		return settlemode;
	}
	public void setSettlemode(String settlemode) {
		this.settlemode = settlemode;
	}
	public String getSettleday() {
		return settleday;
	}
	public void setSettleday(String settleday) {
		this.settleday = settleday;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getBondno() {
		return bondno;
	}
	public void setBondno(String bondno) {
		this.bondno = bondno;
	}
	public String getIntereststartdate() {
		return intereststartdate;
	}
	public void setIntereststartdate(String intereststartdate) {
		this.intereststartdate = intereststartdate;
	}
	public String getInterestenddate() {
		return interestenddate;
	}
	public void setInterestenddate(String interestenddate) {
		this.interestenddate = interestenddate;
	}
	public String getDirect() {
		return direct;
	}
	public void setDirect(String direct) {
		this.direct = direct;
	}
	public String getItemremark() {
		return itemremark;
	}
	public void setItemremark(String itemremark) {
		this.itemremark = itemremark;
	}
	public String getBackenddealfeepaydate() {
		return backenddealfeepaydate;
	}
	public void setBackenddealfeepaydate(String backenddealfeepaydate) {
		this.backenddealfeepaydate = backenddealfeepaydate;
	}
	public BigDecimal getFloatrate() {
		return floatrate;
	}
	public void setFloatrate(BigDecimal floatrate) {
		this.floatrate = floatrate;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getObservedate() {
		return observedate;
	}
	public void setObservedate(String observedate) {
		this.observedate = observedate;
	}
	public String getSendstatus() {
		return sendstatus;
	}
	public void setSendstatus(String sendstatus) {
		this.sendstatus = sendstatus;
	}
	public BigDecimal getMtm() {
		return mtm;
	}
	public void setMtm(BigDecimal mtm) {
		this.mtm = mtm;
	}
	public String getDealFlag() {
		return dealFlag;
	}
	public void setDealFlag(String dealFlag) {
		this.dealFlag = dealFlag;
	}
	public String getPrcssstusdstcd() {
		return prcssstusdstcd;
	}
	public void setPrcssstusdstcd(String prcssstusdstcd) {
		this.prcssstusdstcd = prcssstusdstcd;
	}
	public String getBrncomgtno() {
		return brncomgtno;
	}
	public void setBrncomgtno(String brncomgtno) {
		this.brncomgtno = brncomgtno;
	}
	public String getRefserno() {
		return refserno;
	}
	public void setRefserno(String refserno) {
		this.refserno = refserno;
	}
	public String getSerno() {
		return serno;
	}
	public void setSerno(String serno) {
		this.serno = serno;
	}
	public String getSlipno() {
		return slipno;
	}
	public void setSlipno(String slipno) {
		this.slipno = slipno;
	}
	public String getSwiftno() {
		return swiftno;
	}
	public void setSwiftno(String swiftno) {
		this.swiftno = swiftno;
	}
	public String getCnapstmpno() {
		return cnapstmpno;
	}
	public void setCnapstmpno(String cnapstmpno) {
		this.cnapstmpno = cnapstmpno;
	}
	public String getOutvalue() {
		return outvalue;
	}
	public void setOutvalue(String outvalue) {
		this.outvalue = outvalue;
	}
	public String getReturnstatus() {
		return returnstatus;
	}
	public void setReturnstatus(String returnstatus) {
		this.returnstatus = returnstatus;
	}
	public String getUpdown() {
		return updown;
	}
	public void setUpdown(String updown) {
		this.updown = updown;
	}
	public BigDecimal getTriger() {
		return triger;
	}
	public void setTriger(BigDecimal triger) {
		this.triger = triger;
	}
	public String getPutcall() {
		return putcall;
	}
	public void setPutcall(String putcall) {
		this.putcall = putcall;
	}
	public BigDecimal getStrike() {
		return strike;
	}
	public void setStrike(BigDecimal strike) {
		this.strike = strike;
	}
	public String getIsrange() {
		return isrange;
	}
	public void setIsrange(String isrange) {
		this.isrange = isrange;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("KBStruckBean [dealid=");
		builder.append(dealid);
		builder.append(", status=");
		builder.append(status);
		builder.append(", dealtype=");
		builder.append(dealtype);
		builder.append(", sponsor=");
		builder.append(sponsor);
		builder.append(", sponinst=");
		builder.append(sponinst);
		builder.append(", updatedate=");
		builder.append(updatedate);
		builder.append(", contractno=");
		builder.append(contractno);
		builder.append(", branchid=");
		builder.append(branchid);
		builder.append(", coreproductno=");
		builder.append(coreproductno);
		builder.append(", dealdate=");
		builder.append(dealdate);
		builder.append(", observevaluedate=");
		builder.append(observevaluedate);
		builder.append(", dealfeepaydate=");
		builder.append(dealfeepaydate);
		builder.append(", optionprofitpaydate=");
		builder.append(optionprofitpaydate);
		builder.append(", ccy=");
		builder.append(ccy);
		builder.append(", amt=");
		builder.append(amt);
		builder.append(", ccy1=");
		builder.append(ccy1);
		builder.append(", ccy2=");
		builder.append(ccy2);
		builder.append(", observetargetlower=");
		builder.append(observetargetlower);
		builder.append(", observetargetsure=");
		builder.append(observetargetsure);
		builder.append(", interestdays=");
		builder.append(interestdays);
		builder.append(", valuedate=");
		builder.append(valuedate);
		builder.append(", observeenddate=");
		builder.append(observeenddate);
		builder.append(", optionfeepaydate=");
		builder.append(optionfeepaydate);
		builder.append(", enddate=");
		builder.append(enddate);
		builder.append(", observetarget=");
		builder.append(observetarget);
		builder.append(", amt1=");
		builder.append(amt1);
		builder.append(", amt2=");
		builder.append(amt2);
		builder.append(", observetargettop=");
		builder.append(observetargettop);
		builder.append(", adjustday=");
		builder.append(adjustday);
		builder.append(", observetargetdate=");
		builder.append(observetargetdate);
		builder.append(", optiondirect=");
		builder.append(optiondirect);
		builder.append(", optionccy=");
		builder.append(optionccy);
		builder.append(", optionamt=");
		builder.append(optionamt);
		builder.append(", payoffdirect=");
		builder.append(payoffdirect);
		builder.append(", payoffccy=");
		builder.append(payoffccy);
		builder.append(", payoffamt=");
		builder.append(payoffamt);
		builder.append(", dealdirect=");
		builder.append(dealdirect);
		builder.append(", dealccy=");
		builder.append(dealccy);
		builder.append(", dealamt=");
		builder.append(dealamt);
		builder.append(", backenddealdirect=");
		builder.append(backenddealdirect);
		builder.append(", backenddealccy=");
		builder.append(backenddealccy);
		builder.append(", backenddealamt=");
		builder.append(backenddealamt);
		builder.append(", settlemode=");
		builder.append(settlemode);
		builder.append(", settleday=");
		builder.append(settleday);
		builder.append(", remark=");
		builder.append(remark);
		builder.append(", bondno=");
		builder.append(bondno);
		builder.append(", intereststartdate=");
		builder.append(intereststartdate);
		builder.append(", interestenddate=");
		builder.append(interestenddate);
		builder.append(", direct=");
		builder.append(direct);
		builder.append(", itemremark=");
		builder.append(itemremark);
		builder.append(", backenddealfeepaydate=");
		builder.append(backenddealfeepaydate);
		builder.append(", floatrate=");
		builder.append(floatrate);
		builder.append(", basis=");
		builder.append(basis);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", dealno=");
		builder.append(dealno);
		builder.append(", observedate=");
		builder.append(observedate);
		builder.append(", sendstatus=");
		builder.append(sendstatus);
		builder.append(", mtm=");
		builder.append(mtm);
		builder.append(", dealFlag=");
		builder.append(dealFlag);
		builder.append(", prcssstusdstcd=");
		builder.append(prcssstusdstcd);
		builder.append(", brncomgtno=");
		builder.append(brncomgtno);
		builder.append(", refserno=");
		builder.append(refserno);
		builder.append(", serno=");
		builder.append(serno);
		builder.append(", slipno=");
		builder.append(slipno);
		builder.append(", swiftno=");
		builder.append(swiftno);
		builder.append(", cnapstmpno=");
		builder.append(cnapstmpno);
		builder.append(", outvalue=");
		builder.append(outvalue);
		builder.append(", returnstatus=");
		builder.append(returnstatus);
		builder.append(", updown=");
		builder.append(updown);
		builder.append(", triger=");
		builder.append(triger);
		builder.append(", putcall=");
		builder.append(putcall);
		builder.append(", strike=");
		builder.append(strike);
		builder.append(", isrange=");
		builder.append(isrange);
		builder.append("]");
		return builder.toString();
	}
    
    
}
