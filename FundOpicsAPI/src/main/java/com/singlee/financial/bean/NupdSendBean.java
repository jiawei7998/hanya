package com.singlee.financial.bean;

import java.io.Serializable;

public class NupdSendBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String gid;
	private String flag1;
	private String payacctno;
	private String paybrc;
	private String flag2;
	private String accptacctno;
	private String accptbrc;
	private String profitbrc;
	private String opnflag;
	private String subctrlcode1;
	private String seqno1;
	private String subctrlcode2;
	private String seqno2;
	private String ccy;
	private String drcrind;
	private String amt;
	private String memocode;
	private String setno;
	private String issuccess;
	private String errmsg;
	private String core_dealno;

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getFlag1() {
		return flag1;
	}

	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}

	public String getPayacctno() {
		return payacctno;
	}

	public void setPayacctno(String payacctno) {
		this.payacctno = payacctno;
	}

	public String getPaybrc() {
		return paybrc;
	}

	public void setPaybrc(String paybrc) {
		this.paybrc = paybrc;
	}

	public String getFlag2() {
		return flag2;
	}

	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}

	public String getAccptacctno() {
		return accptacctno;
	}

	public void setAccptacctno(String accptacctno) {
		this.accptacctno = accptacctno;
	}

	public String getAccptbrc() {
		return accptbrc;
	}

	public void setAccptbrc(String accptbrc) {
		this.accptbrc = accptbrc;
	}

	public String getProfitbrc() {
		return profitbrc;
	}

	public void setProfitbrc(String profitbrc) {
		this.profitbrc = profitbrc;
	}

	public String getOpnflag() {
		return opnflag;
	}

	public void setOpnflag(String opnflag) {
		this.opnflag = opnflag;
	}

	public String getSubctrlcode1() {
		return subctrlcode1;
	}

	public void setSubctrlcode1(String subctrlcode1) {
		this.subctrlcode1 = subctrlcode1;
	}

	public String getSeqno1() {
		return seqno1;
	}

	public void setSeqno1(String seqno1) {
		this.seqno1 = seqno1;
	}

	public String getSubctrlcode2() {
		return subctrlcode2;
	}

	public void setSubctrlcode2(String subctrlcode2) {
		this.subctrlcode2 = subctrlcode2;
	}

	public String getSeqno2() {
		return seqno2;
	}

	public void setSeqno2(String seqno2) {
		this.seqno2 = seqno2;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getDrcrind() {
		return drcrind;
	}

	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getMemocode() {
		return memocode;
	}

	public void setMemocode(String memocode) {
		this.memocode = memocode;
	}

	public String getSetno() {
		return setno;
	}

	public void setSetno(String setno) {
		this.setno = setno;
	}

	public String getIssuccess() {
		return issuccess;
	}

	public void setIssuccess(String issuccess) {
		this.issuccess = issuccess;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getCore_dealno() {
		return core_dealno;
	}

	public void setCore_dealno(String core_dealno) {
		this.core_dealno = core_dealno;
	}

}
