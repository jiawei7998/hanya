package com.singlee.financial.bean;

import java.io.Serializable;

public class SlNupdBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1901551160942215641L;

	private String setNo;
	private String seqNo;

	/**
	 * 交易流水
	 */
	private String dealNo;

	/**
	 * 对手号
	 */
	private String cNo;
	/**
	 * 清算方式
	 */
	private String nos;
	/**
	 * 产品
	 */
	private String product;
	/**
	 * 起息日
	 */
	private String valdate;
	private String postDate;
	/**
	 * 币种
	 */
	private String ccy;
	private String ccyCode;
	/**
	 * 借贷
	 */
	private String drcrind;
	/**
	 * 账户
	 */
	private String account;
	/**
	 * 金额
	 */
	private String amount;
	/**
	 * 返回消息
	 */
	private String errMsg;
	/**
	 * rev标识
	 */
	private String revFlag;
	/**
	 * 保存日期
	 */
	private String inputDate;

	private String cmne;
	private String sn;
	private String issend;
	private String issuccess;
	private String core_dealno;
	/**
	 * 记账流水
	 */
	private String fedealno;
	/**
	 * 原交易起息日
	 */
	private String oldvdate;

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getSetNo() {
		return setNo;
	}

	public void setSetNo(String setNo) {
		this.setNo = setNo;
	}

	public String getcNo() {
		return cNo;
	}

	public void setcNo(String cNo) {
		this.cNo = cNo;
	}

	public String getNos() {
		return nos;
	}

	public void setNos(String nos) {
		this.nos = nos;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getValdate() {
		return valdate;
	}

	public void setValdate(String valdate) {
		this.valdate = valdate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getDrcrind() {
		return drcrind;
	}

	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getRevFlag() {
		return revFlag;
	}

	public void setRevFlag(String revFlag) {
		this.revFlag = revFlag;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public String getCmne() {
		return cmne;
	}

	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getIssend() {
		return issend;
	}

	public void setIssend(String issend) {
		this.issend = issend;
	}

	public String getIssuccess() {
		return issuccess;
	}

	public void setIssuccess(String issuccess) {
		this.issuccess = issuccess;
	}

	public String getCore_dealno() {
		return core_dealno;
	}

	public void setCore_dealno(String core_dealno) {
		this.core_dealno = core_dealno;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getOldvdate() {
		return oldvdate;
	}

	public void setOldvdate(String oldvdate) {
		this.oldvdate = oldvdate;
	}
	
}
