package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;
/**
 * 华商银行，查询核心利率互换代客交易
 * @author zhengfl
 *
 */
public class SlSwapDkBean implements Serializable{
	private static final long serialVersionUID = 1L;
	  private String DATASOURCE = "SWAPDK";
	  private String SN;
	  private String TRANCLS;
	  private String TRANSQ;
	  private String TRANDT;
	  private String TRANTP;
	  private String CONTRACTID;
	  private String CONTRACTNO;
	  private String NOTCCY;
	  private String NOTAMOUNT;
	  private String STRDATE;
	  private String MATDATE;
	  private String VDATE;
	  private String PAYCYCLE;
	  private String PAYCYCLERULE;
	  private String REVISIONCYCLE;
	  private String REVISIONCYCLERULE;
	  private String PAYINTSTYLE;
	  private String PAYRATCODE;
	  private String PAYSPREAD;
	  private String PAYFIXRATE;
	  private String PAYBASIS;
	  private String PAYCOMPOUND;
	  private String RECINTSTYLE;
	  private String RECRATECODE;
	  private String RECSPREAD;
	  private String RECFIXRATE;
	  private String RECBASIS;
	  private String RECCOMPOUND;
	  private String NOVACNO;
	  private String CNAME;
	  private String ACCTNO;
	  private String NETPAYAMOUNT;
	  private String NETRECAMOUNT;
	  private String NETAMOUNT;
	  private String ZONENO;
	  private String BRNO;
	  private Date INPUTTIME;
	  private String SENDFLAG;
	  private Date SENDTIME;
	  private String BR;
	  private String SERVER;
	  private String BACK1;
	  private String BACK2;
	  private String BACK3;
	public String getDATASOURCE() {
		return DATASOURCE;
	}
	public void setDATASOURCE(String dATASOURCE) {
		DATASOURCE = dATASOURCE;
	}
	public String getSN() {
		return SN;
	}
	public void setSN(String sN) {
		SN = sN;
	}
	public String getTRANCLS() {
		return TRANCLS;
	}
	public void setTRANCLS(String tRANCLS) {
		TRANCLS = tRANCLS;
	}
	public String getTRANSQ() {
		return TRANSQ;
	}
	public void setTRANSQ(String tRANSQ) {
		TRANSQ = tRANSQ;
	}
	public String getTRANDT() {
		return TRANDT;
	}
	public void setTRANDT(String tRANDT) {
		TRANDT = tRANDT;
	}
	public String getTRANTP() {
		return TRANTP;
	}
	public void setTRANTP(String tRANTP) {
		TRANTP = tRANTP;
	}
	public String getCONTRACTID() {
		return CONTRACTID;
	}
	public void setCONTRACTID(String cONTRACTID) {
		CONTRACTID = cONTRACTID;
	}
	public String getCONTRACTNO() {
		return CONTRACTNO;
	}
	public void setCONTRACTNO(String cONTRACTNO) {
		CONTRACTNO = cONTRACTNO;
	}
	public String getNOTCCY() {
		return NOTCCY;
	}
	public void setNOTCCY(String nOTCCY) {
		NOTCCY = nOTCCY;
	}
	public String getNOTAMOUNT() {
		return NOTAMOUNT;
	}
	public void setNOTAMOUNT(String nOTAMOUNT) {
		NOTAMOUNT = nOTAMOUNT;
	}
	public String getSTRDATE() {
		return STRDATE;
	}
	public void setSTRDATE(String sTRDATE) {
		STRDATE = sTRDATE;
	}
	public String getMATDATE() {
		return MATDATE;
	}
	public void setMATDATE(String mATDATE) {
		MATDATE = mATDATE;
	}
	public String getVDATE() {
		return VDATE;
	}
	public void setVDATE(String vDATE) {
		VDATE = vDATE;
	}
	public String getPAYCYCLE() {
		return PAYCYCLE;
	}
	public void setPAYCYCLE(String pAYCYCLE) {
		PAYCYCLE = pAYCYCLE;
	}
	public String getPAYCYCLERULE() {
		return PAYCYCLERULE;
	}
	public void setPAYCYCLERULE(String pAYCYCLERULE) {
		PAYCYCLERULE = pAYCYCLERULE;
	}
	public String getREVISIONCYCLE() {
		return REVISIONCYCLE;
	}
	public void setREVISIONCYCLE(String rEVISIONCYCLE) {
		REVISIONCYCLE = rEVISIONCYCLE;
	}
	public String getREVISIONCYCLERULE() {
		return REVISIONCYCLERULE;
	}
	public void setREVISIONCYCLERULE(String rEVISIONCYCLERULE) {
		REVISIONCYCLERULE = rEVISIONCYCLERULE;
	}
	public String getPAYINTSTYLE() {
		return PAYINTSTYLE;
	}
	public void setPAYINTSTYLE(String pAYINTSTYLE) {
		PAYINTSTYLE = pAYINTSTYLE;
	}
	public String getPAYRATCODE() {
		return PAYRATCODE;
	}
	public void setPAYRATCODE(String pAYRATCODE) {
		PAYRATCODE = pAYRATCODE;
	}
	public String getPAYSPREAD() {
		return PAYSPREAD;
	}
	public void setPAYSPREAD(String pAYSPREAD) {
		PAYSPREAD = pAYSPREAD;
	}
	public String getPAYFIXRATE() {
		return PAYFIXRATE;
	}
	public void setPAYFIXRATE(String pAYFIXRATE) {
		PAYFIXRATE = pAYFIXRATE;
	}
	public String getPAYBASIS() {
		return PAYBASIS;
	}
	public void setPAYBASIS(String pAYBASIS) {
		PAYBASIS = pAYBASIS;
	}
	public String getPAYCOMPOUND() {
		return PAYCOMPOUND;
	}
	public void setPAYCOMPOUND(String pAYCOMPOUND) {
		PAYCOMPOUND = pAYCOMPOUND;
	}
	public String getRECINTSTYLE() {
		return RECINTSTYLE;
	}
	public void setRECINTSTYLE(String rECINTSTYLE) {
		RECINTSTYLE = rECINTSTYLE;
	}
	public String getRECRATECODE() {
		return RECRATECODE;
	}
	public void setRECRATECODE(String rECRATECODE) {
		RECRATECODE = rECRATECODE;
	}
	public String getRECSPREAD() {
		return RECSPREAD;
	}
	public void setRECSPREAD(String rECSPREAD) {
		RECSPREAD = rECSPREAD;
	}
	public String getRECFIXRATE() {
		return RECFIXRATE;
	}
	public void setRECFIXRATE(String rECFIXRATE) {
		RECFIXRATE = rECFIXRATE;
	}
	public String getRECBASIS() {
		return RECBASIS;
	}
	public void setRECBASIS(String rECBASIS) {
		RECBASIS = rECBASIS;
	}
	public String getRECCOMPOUND() {
		return RECCOMPOUND;
	}
	public void setRECCOMPOUND(String rECCOMPOUND) {
		RECCOMPOUND = rECCOMPOUND;
	}
	public String getNOVACNO() {
		return NOVACNO;
	}
	public void setNOVACNO(String nOVACNO) {
		NOVACNO = nOVACNO;
	}
	public String getCNAME() {
		return CNAME;
	}
	public void setCNAME(String cNAME) {
		CNAME = cNAME;
	}
	public String getACCTNO() {
		return ACCTNO;
	}
	public void setACCTNO(String aCCTNO) {
		ACCTNO = aCCTNO;
	}
	public String getNETPAYAMOUNT() {
		return NETPAYAMOUNT;
	}
	public void setNETPAYAMOUNT(String nETPAYAMOUNT) {
		NETPAYAMOUNT = nETPAYAMOUNT;
	}
	public String getNETRECAMOUNT() {
		return NETRECAMOUNT;
	}
	public void setNETRECAMOUNT(String nETRECAMOUNT) {
		NETRECAMOUNT = nETRECAMOUNT;
	}
	public String getNETAMOUNT() {
		return NETAMOUNT;
	}
	public void setNETAMOUNT(String nETAMOUNT) {
		NETAMOUNT = nETAMOUNT;
	}
	public String getZONENO() {
		return ZONENO;
	}
	public void setZONENO(String zONENO) {
		ZONENO = zONENO;
	}
	public String getBRNO() {
		return BRNO;
	}
	public void setBRNO(String bRNO) {
		BRNO = bRNO;
	}
	public Date getINPUTTIME() {
		return INPUTTIME;
	}
	public void setINPUTTIME(Date iNPUTTIME) {
		INPUTTIME = iNPUTTIME;
	}
	public String getSENDFLAG() {
		return SENDFLAG;
	}
	public void setSENDFLAG(String sENDFLAG) {
		SENDFLAG = sENDFLAG;
	}
	public Date getSENDTIME() {
		return SENDTIME;
	}
	public void setSENDTIME(Date sENDTIME) {
		SENDTIME = sENDTIME;
	}
	public String getBR() {
		return BR;
	}
	public void setBR(String bR) {
		BR = bR;
	}
	public String getSERVER() {
		return SERVER;
	}
	public void setSERVER(String sERVER) {
		SERVER = sERVER;
	}
	public String getBACK1() {
		return BACK1;
	}
	public void setBACK1(String bACK1) {
		BACK1 = bACK1;
	}
	public String getBACK2() {
		return BACK2;
	}
	public void setBACK2(String bACK2) {
		BACK2 = bACK2;
	}
	public String getBACK3() {
		return BACK3;
	}
	public void setBACK3(String bACK3) {
		BACK3 = bACK3;
	}
	  
}
