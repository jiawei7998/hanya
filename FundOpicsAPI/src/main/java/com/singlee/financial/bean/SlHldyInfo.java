package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用于货币的假日传输
 */
public class SlHldyInfo implements Serializable {
    /**
     * 货币代码
     */
    private String ccy;
    /**
     * 货币对应的节假日
     */
    private List<Date> hilDayLst;
    /**
     * 货币对应的调修日期
     */
    private List<Date> adjDayLst;

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public List<Date> getHilDayLst() {
        return hilDayLst;
    }

    public void setHilDayLst(List<Date> hilDayLst) {
        this.hilDayLst = hilDayLst;
    }

    public List<Date> getAdjDayLst() {
        return adjDayLst;
    }

    public void setAdjDayLst(List<Date> adjDayLst) {
        this.adjDayLst = adjDayLst;
    }
}
