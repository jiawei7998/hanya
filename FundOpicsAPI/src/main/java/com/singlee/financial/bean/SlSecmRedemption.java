package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 债券还本收息提醒
 * 
 * @author shenzl
 *
 */
public class SlSecmRedemption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6253101236466854696L;

	private String br;

	private String brprcindte;

	private String product;

	private String prodtype;

	private String dealno;

	private String ccy;

	private String cno;
	// 本金
	private String prinamt;
	// 收益金额
	private String proceedamt;
	// 利息
	private String purchintamt;
	// 结算日期
	private Date settdate;
	// 复核标识
	private String verind;
	// 复核日期
	private Date verdate;
	// 复核人
	private String voper;
	// 交易状态
	private String status;
	
	private String secid;

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getBrprcindte() {
		return brprcindte;
	}

	public void setBrprcindte(String brprcindte) {
		this.brprcindte = brprcindte;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getPrinamt() {
		return prinamt;
	}

	public void setPrinamt(String prinamt) {
		this.prinamt = prinamt;
	}

	public String getProceedamt() {
		return proceedamt;
	}

	public void setProceedamt(String proceedamt) {
		this.proceedamt = proceedamt;
	}

	public String getPurchintamt() {
		return purchintamt;
	}

	public void setPurchintamt(String purchintamt) {
		this.purchintamt = purchintamt;
	}

	public Date getSettdate() {
		return settdate;
	}

	public void setSettdate(Date settdate) {
		this.settdate = settdate;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
