package com.singlee.financial.bean;
import java.math.BigDecimal;
import java.io.Serializable;
/***
 * 
 * @author LIJ
 *
 */
public class GmrmIrsRefix implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8007387491013255883L;

	
    /**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 交易编号
     */
    private String dealId;

    /**
     * 重置序列编号，相同DEAL_ID的每条记录不能重复
     */
    private String resetId;

    /**
     * 接收支付标志 1、Pay支付端  2、Receive接收端
     */
    private String payRecFlag;

    /**
     * 重置日 YYYYMMDD
     */
    private String resetDate;

    /**
     * 重置获取日 YYYYMMDD
     */
    private String fixingDate;

    /**
     * 重置利率
     */
    private BigDecimal resetRate;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期 YYYYMMDD
     * @return BATCH_DATE 批量日期 YYYYMMDD
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期 YYYYMMDD
     * @param batchDate 批量日期 YYYYMMDD
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 交易编号
     * @return DEAL_ID 交易编号
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 交易编号
     * @param dealId 交易编号
     */
    public void setDealId(String dealId) {
        this.dealId = dealId == null ? null : dealId.trim();
    }

    /**
     * 重置序列编号，相同DEAL_ID的每条记录不能重复
     * @return RESET_ID 重置序列编号，相同DEAL_ID的每条记录不能重复
     */
    public String getResetId() {
        return resetId;
    }

    /**
     * 重置序列编号，相同DEAL_ID的每条记录不能重复
     * @param resetId 重置序列编号，相同DEAL_ID的每条记录不能重复
     */
    public void setResetId(String resetId) {
        this.resetId = resetId == null ? null : resetId.trim();
    }

    /**
     * 接收支付标志 1、Pay支付端  2、Receive接收端
     * @return PAY_REC_FLAG 接收支付标志 1、Pay支付端  2、Receive接收端
     */
    public String getPayRecFlag() {
        return payRecFlag;
    }

    /**
     * 接收支付标志 1、Pay支付端  2、Receive接收端
     * @param payRecFlag 接收支付标志 1、Pay支付端  2、Receive接收端
     */
    public void setPayRecFlag(String payRecFlag) {
        this.payRecFlag = payRecFlag == null ? null : payRecFlag.trim();
    }

    /**
     * 重置日 YYYYMMDD
     * @return RESET_DATE 重置日 YYYYMMDD
     */
    public String getResetDate() {
        return resetDate;
    }

    /**
     * 重置日 YYYYMMDD
     * @param resetDate 重置日 YYYYMMDD
     */
    public void setResetDate(String resetDate) {
        this.resetDate = resetDate == null ? null : resetDate.trim();
    }

    /**
     * 重置获取日 YYYYMMDD
     * @return FIXING_DATE 重置获取日 YYYYMMDD
     */
    public String getFixingDate() {
        return fixingDate;
    }

    /**
     * 重置获取日 YYYYMMDD
     * @param fixingDate 重置获取日 YYYYMMDD
     */
    public void setFixingDate(String fixingDate) {
        this.fixingDate = fixingDate == null ? null : fixingDate.trim();
    }

    /**
     * 重置利率
     * @return RESET_RATE 重置利率
     */
    public BigDecimal getResetRate() {
        return resetRate;
    }

    /**
     * 重置利率
     * @param resetRate 重置利率
     */
    public void setResetRate(BigDecimal resetRate) {
        this.resetRate = resetRate;
    }

    /**
     * 机构号
     * @return BRANCH_ID 机构号
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * 机构号
     * @param branchId 机构号
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }
}
