package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

public class SlSwiftBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String br;
    private String dealNo;
    private String batchNo;
    private String prodCode;
    private Date cdate;
    private String ctime;
    private String msgType;
    private String msg;
    private String leng;
    private String prodType;
    private Date swftcDate;
    private String ioper;
    private String idate;
    private String verOper;
    private String verind;
    private String verDate;
    private String vdate;
    private String fileurl;
    private String lstmntDate;
    private String settFlag;
    private String returnCode;
    private String returnMsg;
    private String seq_no;
    private String swift_status;
    private String swiift_msg;

    private String settWay;
    private String sysdate;
    private String sendflag;

    public String getSettWay() {
        return settWay;
    }

    public void setSettWay(String settWay) {
        this.settWay = settWay;
    }

    public String getSysdate() {
        return sysdate;
    }

    public void setSysdate(String sysdate) {
        this.sysdate = sysdate;
    }

    public String getSendflag() {
        return sendflag;
    }

    public void setSendflag(String sendflag) {
        this.sendflag = sendflag;
    }

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getProdCode() {
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public Date getCdate() {
        return cdate;
    }

    public void setCdate(Date cdate) {
        this.cdate = cdate;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getLeng() {
        return leng;
    }

    public void setLeng(String leng) {
        this.leng = leng;
    }

    public String getProdType() {
        return prodType;
    }

    public void setProdType(String prodType) {
        this.prodType = prodType;
    }

    public Date getSwftcDate() {

        return swftcDate;
    }

    public void setSwftcDate(Date swftcDate) {
        this.swftcDate = swftcDate;
    }

    public String getIoper() {
        return ioper;
    }

    public void setIoper(String ioper) {
        this.ioper = ioper;
    }

    public String getIdate() {
        return idate;
    }

    public void setIdate(String idate) {
        this.idate = idate;
    }

    public String getVerOper() {
        return verOper;
    }

    public void setVerOper(String verOper) {
        this.verOper = verOper;
    }

    public String getVerind() {
        return verind;
    }

    public void setVerind(String verind) {
        this.verind = verind;
    }

    public String getVerDate() {
        return verDate;
    }

    public void setVerDate(String verDate) {
        this.verDate = verDate;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getFileurl() {
        return fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }

    public String getLstmntDate() {
        return lstmntDate;
    }

    public void setLstmntDate(String lstmntDate) {
        this.lstmntDate = lstmntDate;
    }

    public String getSettFlag() {
        return settFlag;
    }

    public void setSettFlag(String settFlag) {
        this.settFlag = settFlag;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getSeq_no() {
        return seq_no;
    }

    public void setSeq_no(String seq_no) {
        this.seq_no = seq_no;
    }

    public String getSwift_status() {
        return swift_status;
    }

    public void setSwift_status(String swift_status) {
        this.swift_status = swift_status;
    }

    public String getSwiift_msg() {
        return swiift_msg;
    }

    public void setSwiift_msg(String swiift_msg) {
        this.swiift_msg = swiift_msg;
    }


    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }
}