package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 核心接口返回数据
 */
public class SlNcbsBean implements Serializable {

	private static final long serialVersionUID = -4336218541537615622L;

	/**
	 * 交易返回状态
	 */
	private String RET_STATUS;

	/**
	 * 交易返回码
	 */
	private String RET_CODE;

	/**
	 * 交易返回信息
	 */
	private String RET_MSG;

	/**
	 * 返回账务信息数组
	 */
	private List<GlInfoArray> GL_INFO_ARRAY;

	/**
	 * 账号
	 */
	private String ACCT_NO;

	/**
	 * 账户名称
	 */
	private String ACCT_NAME;

	/**
	 * 上期余额方向
	 */
	private String CRN_PRD_BAL_FLG;

	/**
	 * 上期借方余额
	 */
	private String LAST_PRD_CR_BAL;

	/**
	 * 上期贷方余额
	 */
	private String LAST_PRD_DR_BAL;

	/**
	 * 本期余额方向
	 */
	private String CRN_BAL_DRC;

	/**
	 * 本期借方余额
	 */
	private String CRN_DR_BAL;

	/**
	 * 本期贷方余额
	 */
	private String CRN_CR_BAL;

	public String getRET_STATUS() {
		return RET_STATUS;
	}

	public void setRET_STATUS(String rET_STATUS) {
		RET_STATUS = rET_STATUS;
	}

	public String getRET_CODE() {
		return RET_CODE;
	}

	public void setRET_CODE(String rET_CODE) {
		RET_CODE = rET_CODE;
	}

	public String getRET_MSG() {
		return RET_MSG;
	}

	public void setRET_MSG(String rET_MSG) {
		RET_MSG = rET_MSG;
	}

	public List<GlInfoArray> getGL_INFO_ARRAY() {
		return GL_INFO_ARRAY;
	}

	public void setGL_INFO_ARRAY(List<GlInfoArray> gL_INFO_ARRAY) {
		GL_INFO_ARRAY = gL_INFO_ARRAY;
	}

	public String getACCT_NO() {
		return ACCT_NO;
	}

	public void setACCT_NO(String aCCT_NO) {
		ACCT_NO = aCCT_NO;
	}

	public String getACCT_NAME() {
		return ACCT_NAME;
	}

	public void setACCT_NAME(String aCCT_NAME) {
		ACCT_NAME = aCCT_NAME;
	}

	public String getCRN_PRD_BAL_FLG() {
		return CRN_PRD_BAL_FLG;
	}

	public void setCRN_PRD_BAL_FLG(String cRN_PRD_BAL_FLG) {
		CRN_PRD_BAL_FLG = cRN_PRD_BAL_FLG;
	}

	public String getLAST_PRD_CR_BAL() {
		return LAST_PRD_CR_BAL;
	}

	public void setLAST_PRD_CR_BAL(String lAST_PRD_CR_BAL) {
		LAST_PRD_CR_BAL = lAST_PRD_CR_BAL;
	}

	public String getLAST_PRD_DR_BAL() {
		return LAST_PRD_DR_BAL;
	}

	public void setLAST_PRD_DR_BAL(String lAST_PRD_DR_BAL) {
		LAST_PRD_DR_BAL = lAST_PRD_DR_BAL;
	}

	public String getCRN_BAL_DRC() {
		return CRN_BAL_DRC;
	}

	public void setCRN_BAL_DRC(String cRN_BAL_DRC) {
		CRN_BAL_DRC = cRN_BAL_DRC;
	}

	public String getCRN_DR_BAL() {
		return CRN_DR_BAL;
	}

	public void setCRN_DR_BAL(String cRN_DR_BAL) {
		CRN_DR_BAL = cRN_DR_BAL;
	}

	public String getCRN_CR_BAL() {
		return CRN_CR_BAL;
	}

	public void setCRN_CR_BAL(String cRN_CR_BAL) {
		CRN_CR_BAL = cRN_CR_BAL;
	}
}