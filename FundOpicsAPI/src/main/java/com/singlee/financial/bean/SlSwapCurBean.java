package com.singlee.financial.bean;

import java.io.Serializable;
/**
 * 货币互换 头寸敞口统计
 * @author 
 *
 */
public class SlSwapCurBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6629100589740254837L;
	/**
	 * 机构
	 */
	private String br;
	/**
	 * 交易编号
	 */
	private String dealno;
	/**
	 * 产品
	 */
	private String product;
	/**
	 * 产品类型
	 */
	private String prodtype;
	/**
	 * 成本中心号
	 */
	private String cost;
	/**
	 * 交易员
	 */
	private String trad;
	/**
	 * 交易日
	 */
	private String dealdate;
	/**
	 * 起息日
	 */
	private String settledate;
	/**
	 * 到期日
	 */
	private String matdate;
	/**
	 * 互换币种
	 */
	private String currency;
	/**
	 * 交易方向
	 */
	private String side;
	/**
	 * 利率代码
	 */
	private String ratecode;
	/**
	 * 本金交割方式
	 */
	private String principle;
	/**
	 * 期初交割日期
	 */
	private String initexchdate;
	/**
	 * 期初交割币种1
	 */
	private String iccy1;
	/**
	 * 期初交割金额1
	 */
	private String initexchamtccy1;
	/**
	 * 期初交割币种2
	 */
	private String iccy2;
	/**
	 * 期初交割金额2
	 */
	private String initexchamtccy2;
	/**
	 * 期末交割日期
	 */
	private String finexchdate;
	/**
	 * 期末交割币种1
	 */
	private String fccy1;
	/**
	 * 期末交割金额1
	 */
	private String finexchamtccy1;
	/**
	 * 期末交割币种2
	 */
	private String fccy2;
	/**
	 * 期末交割金额2
	 */
	private String finexchamtccy2;
	/**
	 * 交易对手
	 */

	private String cmne ;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getDealdate() {
		return dealdate;
	}
	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}
	public String getSettledate() {
		return settledate;
	}
	public void setSettledate(String settledate) {
		this.settledate = settledate;
	}
	public String getMatdate() {
		return matdate;
	}
	public void setMatdate(String matdate) {
		this.matdate = matdate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public String getRatecode() {
		return ratecode;
	}
	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}
	public String getPrinciple() {
		return principle;
	}
	public void setPrinciple(String principle) {
		this.principle = principle;
	}
	public String getInitexchdate() {
		return initexchdate;
	}
	public void setInitexchdate(String initexchdate) {
		this.initexchdate = initexchdate;
	}
	public String getIccy1() {
		return iccy1;
	}
	public void setIccy1(String iccy1) {
		this.iccy1 = iccy1;
	}
	public String getInitexchamtccy1() {
		return initexchamtccy1;
	}
	public void setInitexchamtccy1(String initexchamtccy1) {
		this.initexchamtccy1 = initexchamtccy1;
	}
	public String getIccy2() {
		return iccy2;
	}
	public void setIccy2(String iccy2) {
		this.iccy2 = iccy2;
	}
	public String getInitexchamtccy2() {
		return initexchamtccy2;
	}
	public void setInitexchamtccy2(String initexchamtccy2) {
		this.initexchamtccy2 = initexchamtccy2;
	}
	public String getFinexchdate() {
		return finexchdate;
	}
	public void setFinexchdate(String finexchdate) {
		this.finexchdate = finexchdate;
	}
	public String getFccy1() {
		return fccy1;
	}
	public void setFccy1(String fccy1) {
		this.fccy1 = fccy1;
	}
	public String getFinexchamtccy1() {
		return finexchamtccy1;
	}
	public void setFinexchamtccy1(String finexchamtccy1) {
		this.finexchamtccy1 = finexchamtccy1;
	}
	public String getFccy2() {
		return fccy2;
	}
	public void setFccy2(String fccy2) {
		this.fccy2 = fccy2;
	}
	public String getFinexchamtccy2() {
		return finexchamtccy2;
	}
	public void setFinexchamtccy2(String finexchamtccy2) {
		this.finexchamtccy2 = finexchamtccy2;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	
}
