package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;
/***
 * 清算路径
 * @author lij
 *
 */
public class SlSetmBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String br;

    private String settlmeans;
    
    private Object descr;

    private String checkdigit;

    private String genledgno;

    private String acctval;

    private String valacc;

    private Date lstmntdte;

    private String extintflag;

    private String offacct;

    private String autoauthorize;

    
	
    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getSettlmeans() {
        return settlmeans;
    }

    public void setSettlmeans(String settlmeans) {
        this.settlmeans = settlmeans == null ? null : settlmeans.trim();
    }
	
    public Object getDescr() {
        return descr;
    }

    public void setDescr(Object descr) {
        this.descr = descr;
    }

    public String getCheckdigit() {
        return checkdigit;
    }

    public void setCheckdigit(String checkdigit) {
        this.checkdigit = checkdigit == null ? null : checkdigit.trim();
    }

    public String getGenledgno() {
        return genledgno;
    }

    public void setGenledgno(String genledgno) {
        this.genledgno = genledgno == null ? null : genledgno.trim();
    }

    public String getAcctval() {
        return acctval;
    }

    public void setAcctval(String acctval) {
        this.acctval = acctval == null ? null : acctval.trim();
    }

    public String getValacc() {
        return valacc;
    }

    public void setValacc(String valacc) {
        this.valacc = valacc == null ? null : valacc.trim();
    }

    public Date getLstmntdte() {
        return lstmntdte;
    }

    public void setLstmntdte(Date lstmntdte) {
        this.lstmntdte = lstmntdte;
    }

    public String getExtintflag() {
        return extintflag;
    }

    public void setExtintflag(String extintflag) {
        this.extintflag = extintflag == null ? null : extintflag.trim();
    }

    public String getOffacct() {
        return offacct;
    }

    public void setOffacct(String offacct) {
        this.offacct = offacct == null ? null : offacct.trim();
    }

    public String getAutoauthorize() {
        return autoauthorize;
    }

    public void setAutoauthorize(String autoauthorize) {
        this.autoauthorize = autoauthorize == null ? null : autoauthorize.trim();
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
