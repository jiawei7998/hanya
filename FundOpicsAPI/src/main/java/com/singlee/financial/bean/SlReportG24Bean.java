package com.singlee.financial.bean;

import java.io.Serializable;
/**
 *	G24  最大十家金融机构同业拆入情况表 实体
 */
public class SlReportG24Bean implements Serializable {

	private static final long serialVersionUID = 7508698561040677984L;
	
	private String group_cn;//金融机构名称
	private String group_id;//金融机构代码
	private Double amt1;//同业融入余额
	private Double amt2;//同业拆入(含回购)余额
	private String rpdate;
	
	
	public String getGroup_cn() {
		return group_cn;
	}


	public void setGroup_cn(String group_cn) {
		this.group_cn = group_cn;
	}


	public String getGroup_id() {
		return group_id;
	}


	public void setGroup_id(String group_id) {
		this.group_id = group_id;
	}


	public Double getAmt1() {
		return amt1;
	}


	public void setAmt1(Double amt1) {
		this.amt1 = amt1;
	}


	public Double getAmt2() {
		return amt2;
	}


	public void setAmt2(Double amt2) {
		this.amt2 = amt2;
	}


	public String getRpdate() {
		return rpdate;
	}


	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}


	@Override
	public String toString() {
		return "SlReportG24Bean [group_cn=" + group_cn + ", group_id="
				+ group_id + ", amt1=" + amt1 + ", amt2=" + amt2 + ", rpdate="
				+ rpdate + "]";
	}
	
	

}
