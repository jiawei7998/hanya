package com.singlee.financial.bean;

import java.io.Serializable;

import com.github.pagehelper.StringUtil;

public class SlOpcNssbBean implements Serializable {

	private static final long serialVersionUID = 1901551160942215641L;

	private String glno;
	private String ps;
	private String amount;

	public String getGlno() {
		return glno;
	}

	public void setGlno(String glno) {
		this.glno = glno;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getAmount() {
		return StringUtil.isEmpty(amount) ? "" : amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
