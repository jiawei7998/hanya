package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Opics 头寸调拨实体bean
 */
public class SlNtrfBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8059468097870466214L;
    /**
     * INTH 头表信息
     */
    private SlInthBean inthBean;
    /**
     * OPICS拓展属性
     */
    private SlExternalBean externalBean;

    // 头寸调拨币种
    private String ccy;
    // 头寸调拨日期
    private Date vdate;
    // 头寸调拨付款账户
    private String payacct;
    // 头寸调拨收款账户
    private String recacct;
    // 头寸调拨金额
    private BigDecimal trfamt;
    // 头寸调拨备注char(35)
    private String sr;

    /**
     * 头寸调拨初始化
     *
     * @param br
     *            业务部门分支
     * @param tag
     *            标识
     * @param detail
     *            细节
     * @note
     *
     *       1、交易导入 TAG="NTRF" , DETAIL="INTR"
     *
     *       2、交易冲销 TAG="NTRR", DETAIL="INTR"
     *
     */
    public SlNtrfBean(String br, String server, String tag, String detail) {
        if (null == inthBean) {
            inthBean = new SlInthBean(); // 头寸调拨
            inthBean.setBr(br);//
            inthBean.setServer(server);
            inthBean.setTag(tag);
            inthBean.setDetail(detail);
            inthBean.setSeq(SlDealModule.NTRF.SEQ);
            inthBean.setInoutind(SlDealModule.NTRF.INOUTIND);
            inthBean.setPriority(SlDealModule.NTRF.PRIORITY);
            inthBean.setStatcode(SlDealModule.NTRF.STATCODE);
            inthBean.setSyst(SlDealModule.NTRF.SYST);
            inthBean.setIoper(SlDealModule.NTRF.IOPER);
        }
    }

    public SlInthBean getInthBean() {
        return inthBean;
    }

    public void setInthBean(SlInthBean inthBean) {
        this.inthBean = inthBean;
    }

    public SlExternalBean getExternalBean() {
        return externalBean;
    }

    public void setExternalBean(SlExternalBean externalBean) {
        this.externalBean = externalBean;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public Date getVdate() {
        return vdate;
    }

    public void setVdate(Date vdate) {
        this.vdate = vdate;
    }

    public String getPayacct() {
        return payacct;
    }

    public void setPayacct(String payacct) {
        this.payacct = payacct;
    }

    public String getRecacct() {
        return recacct;
    }

    public void setRecacct(String recacct) {
        this.recacct = recacct;
    }

    public BigDecimal getTrfamt() {
        return trfamt;
    }

    public void setTrfamt(BigDecimal trfamt) {
        this.trfamt = trfamt;
    }

    public String getSr() {
        return sr;
    }

    public void setSr(String sr) {
        this.sr = sr;
    }
}
