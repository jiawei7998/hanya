package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * A1403-外资银行资产负债项目月报表
 * @author wangzhao
 *
 */
public class A1403Bean implements Serializable{
	  private static final long serialVersionUID = 1L;
	  private String tableid;//单据类型
	  private String br;//部门
	  private String lineid;
	  private String rowsid;
	  private Double amt1;//人民币余额(币种单位:元)
	  private Double amt2;//外币折美元余额(币种单位:美元)
	  private String rpdate;//指标日期
	
	public String getTableid() {
		return tableid;
	}
	public void setTableid(String tableid) {
		this.tableid = tableid;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getLineid() {
		return lineid;
	}
	public void setLineid(String lineid) {
		this.lineid = lineid;
	}
	public String getRowsid() {
		return rowsid;
	}
	public void setRowsid(String rowsid) {
		this.rowsid = rowsid;
	}
	public Double getAmt1() {
		return amt1;
	}
	public void setAmt1(Double amt1) {
		this.amt1 = amt1;
	}
	public Double getAmt2() {
		return amt2;
	}
	public void setAmt2(Double amt2) {
		this.amt2 = amt2;
	}
	public String getRpdate() {
		return rpdate;
	}
	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}
	@Override
	public String toString() {
		return "A1403Bean [tableid=" + tableid + ", br=" + br + ", lineid="
				+ lineid + ", rowsid=" + rowsid + ", amt1=" + amt1 + ", amt2="
				+ amt2 + ", rpdate=" + rpdate + "]";
	}
	  
}
