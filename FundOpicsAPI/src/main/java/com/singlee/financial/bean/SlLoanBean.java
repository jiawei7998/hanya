package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 信贷额度分配查询类
 */
public class SlLoanBean implements Serializable {

	private static final long serialVersionUID = -6526993965897020851L;

	/**
	 * 核心客户号
	 */
	private String coreClientNo;

	private String retMsg;

	private String retCode;

	/**
	 * 协议编号
	 */
	private String AGREE_NO;

	/**
	 * 总额度
	 */
	private String TOTAL_LMT;

	/**
	 * 可用额度
	 */
	private String AVAILABLE_LMT;

	/**
	 * 冻结标志
	 */
	private String FRZ_FLG;

	/**
	 * 授信额度
	 */
	private String CRDT_LMT;

	/**
	 * 敞口额度
	 */
	private String OPN_LMT;

	/**
	 * 担保额度
	 */
	private String WRNT_LMT;

	/**
	 * 币种
	 */
	private String CCY;

	/**
	 * 机构总额度
	 */
	private String ORG_TOT_LMT;

	/**
	 * 开始日期
	 */
	private String START_DATE;

	/**
	 * 终止日期
	 */
	private String END_DATE;

	/**
	 * 登记机构
	 */
	private String REGISTER_BRANCH_ID;

	/**
	 * 修改日期(变更日期)
	 */
	private String CHANGE_DATE;

	/**
	 * 基础授信额度
	 */
	private String BASE_CRDT_LMT;

	/**
	 * 查询结果返回数组
	 */
	private List<SxedfpcxResBodyArray> QUERY_RESULT_ARRAY;
	
	/**
	 * 前置用户名
	 */
	private String username;

	public String getAGREE_NO() {
		return AGREE_NO;
	}

	public void setAGREE_NO(String aGREE_NO) {
		AGREE_NO = aGREE_NO;
	}

	public String getTOTAL_LMT() {
		return TOTAL_LMT;
	}

	public void setTOTAL_LMT(String tOTAL_LMT) {
		TOTAL_LMT = tOTAL_LMT;
	}

	public String getAVAILABLE_LMT() {
		return AVAILABLE_LMT;
	}

	public void setAVAILABLE_LMT(String aVAILABLE_LMT) {
		AVAILABLE_LMT = aVAILABLE_LMT;
	}

	public String getFRZ_FLG() {
		return FRZ_FLG;
	}

	public void setFRZ_FLG(String fRZ_FLG) {
		FRZ_FLG = fRZ_FLG;
	}

	public String getCRDT_LMT() {
		return CRDT_LMT;
	}

	public void setCRDT_LMT(String cRDT_LMT) {
		CRDT_LMT = cRDT_LMT;
	}

	public String getOPN_LMT() {
		return OPN_LMT;
	}

	public void setOPN_LMT(String oPN_LMT) {
		OPN_LMT = oPN_LMT;
	}

	public String getWRNT_LMT() {
		return WRNT_LMT;
	}

	public void setWRNT_LMT(String wRNT_LMT) {
		WRNT_LMT = wRNT_LMT;
	}

	public String getCCY() {
		return CCY;
	}

	public void setCCY(String cCY) {
		CCY = cCY;
	}

	public String getORG_TOT_LMT() {
		return ORG_TOT_LMT;
	}

	public void setORG_TOT_LMT(String oRG_TOT_LMT) {
		ORG_TOT_LMT = oRG_TOT_LMT;
	}

	public String getSTART_DATE() {
		return START_DATE;
	}

	public void setSTART_DATE(String sTART_DATE) {
		START_DATE = sTART_DATE;
	}

	public String getEND_DATE() {
		return END_DATE;
	}

	public void setEND_DATE(String eND_DATE) {
		END_DATE = eND_DATE;
	}

	public String getREGISTER_BRANCH_ID() {
		return REGISTER_BRANCH_ID;
	}

	public void setREGISTER_BRANCH_ID(String rEGISTER_BRANCH_ID) {
		REGISTER_BRANCH_ID = rEGISTER_BRANCH_ID;
	}

	public String getCHANGE_DATE() {
		return CHANGE_DATE;
	}

	public void setCHANGE_DATE(String cHANGE_DATE) {
		CHANGE_DATE = cHANGE_DATE;
	}

	public String getBASE_CRDT_LMT() {
		return BASE_CRDT_LMT;
	}

	public void setBASE_CRDT_LMT(String bASE_CRDT_LMT) {
		BASE_CRDT_LMT = bASE_CRDT_LMT;
	}

	public String getCoreClientNo() {
		return coreClientNo;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public void setCoreClientNo(String coreClientNo) {
		this.coreClientNo = coreClientNo;
	}

	public List<SxedfpcxResBodyArray> getQUERY_RESULT_ARRAY() {
		return QUERY_RESULT_ARRAY;
	}

	public void setQUERY_RESULT_ARRAY(List<SxedfpcxResBodyArray> qUERY_RESULT_ARRAY) {
		QUERY_RESULT_ARRAY = qUERY_RESULT_ARRAY;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}