package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * @author fll
 */
public class SendInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6870896489957569018L;
	// 机构
	private String br;
	// 财务日期
	private String postdate;
	// 发送系统日期
	private String senddate;
	// 发送账务类型：TODAY-当前账务
	private String sendtype;
	// 发送账务状态：0-生成;1-已发送；2-记账成功
	private String sendflag;
	// 中文
	private String typename;
	// 备注
	private String note;
	// 返回状态
	private String retcode;
	// 返回信息
	private String retmsg;
	// 上送核心流水号
	private String coreseq;
	// 核心反馈文件路径
	private String corefile;
	// 录入时间
	private String inputdate;
	// 最后修改时间
	private String lastdate;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public String getSenddate() {
		return senddate;
	}

	public void setSenddate(String senddate) {
		this.senddate = senddate;
	}

	public String getSendtype() {
		return sendtype;
	}

	public void setSendtype(String sendtype) {
		this.sendtype = sendtype;
	}

	public String getSendflag() {
		return sendflag;
	}

	public void setSendflag(String sendflag) {
		this.sendflag = sendflag;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRetcode() {
		return retcode;
	}

	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getRetmsg() {
		return retmsg;
	}

	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}

	public String getCoreseq() {
		return coreseq;
	}

	public void setCoreseq(String coreseq) {
		this.coreseq = coreseq;
	}

	public String getCorefile() {
		return corefile;
	}

	public void setCorefile(String corefile) {
		this.corefile = corefile;
	}

	public String getInputdate() {
		return inputdate;
	}

	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}

	public String getLastdate() {
		return lastdate;
	}

	public void setLastdate(String lastdate) {
		this.lastdate = lastdate;
	}

	public String getTypename() {
		return typename;
	}

	public void setTypename(String typename) {
		this.typename = typename;
	}

	@Override
	public String toString() {
		return "SendInfo [postdate=" + postdate + ", senddate=" + senddate + ", sendtype=" + sendtype + ", sendflag="
				+ sendflag + ", note=" + note + ", retcode=" + retcode + ", retmsg=" + retmsg + ", coreseq=" + coreseq
				+ ", corefile=" + corefile + ", inputdate=" + inputdate + ", lastdate=" + lastdate + "]";
	}
}