package com.singlee.financial.bean;

import java.io.Serializable;
/***
 * 债券评级
 * @author LIJ
 *
 */
public class GmrmBondRating implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 5767517973794846910L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 债券序号
     */
    private String secId;

    /**
     * 评级类型，发行体评级或债项评级 0-发行体评级 1-债项评级
     */
    private String ratingType;

    /**
     * 评级机构
     */
    private String ratingBranch;

    /**
     * 评级
     */
    private String ratingLevel;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 债券序号
     * @return SEC_ID 债券序号
     */
    public String getSecId() {
        return secId;
    }

    /**
     * 债券序号
     * @param secId 债券序号
     */
    public void setSecId(String secId) {
        this.secId = secId == null ? null : secId.trim();
    }

    /**
     * 评级类型，发行体评级或债项评级 0-发行体评级 1-债项评级
     * @return RATING_TYPE 评级类型，发行体评级或债项评级 0-发行体评级 1-债项评级
     */
    public String getRatingType() {
        return ratingType;
    }

    /**
     * 评级类型，发行体评级或债项评级 0-发行体评级 1-债项评级
     * @param ratingType 评级类型，发行体评级或债项评级 0-发行体评级 1-债项评级
     */
    public void setRatingType(String ratingType) {
        this.ratingType = ratingType == null ? null : ratingType.trim();
    }

    /**
     * 评级机构
     * @return RATING_BRANCH 评级机构
     */
    public String getRatingBranch() {
        return ratingBranch;
    }

    /**
     * 评级机构
     * @param ratingBranch 评级机构
     */
    public void setRatingBranch(String ratingBranch) {
        this.ratingBranch = ratingBranch == null ? null : ratingBranch.trim();
    }

    /**
     * 评级
     * @return RATING_LEVEL 评级
     */
    public String getRatingLevel() {
        return ratingLevel;
    }

    /**
     * 评级
     * @param ratingLevel 评级
     */
    public void setRatingLevel(String ratingLevel) {
        this.ratingLevel = ratingLevel == null ? null : ratingLevel.trim();
    }
}