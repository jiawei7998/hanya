package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * opics的产品类型
 * 
 * @author shenzl
 * 
 */
public class SlProdTypeBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6322922538500666901L;
	/**
	 * 产品代码
	 */
	private String prodCode;
	/**
	 * 产品类型
	 */
	private String prodType;
	/**
	 * 描述
	 */
	private String descr;
	/**
	 * 维护时间
	 */
	private Date lstmntdte;
	/**
	 * 模块
	 */
	private String al;

	public String getProdCode() {
		return prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getAl() {
		return al;
	}

	public void setAl(String al) {
		this.al = al;
	}
}
