package com.singlee.financial.bean;

import java.io.Serializable;
public class SLReportSwapRest implements Serializable {
	
	private static final long serialVersionUID = 1242208695986978898L;

	private String iPayDate;
    private String dealno;
    private String cost;
    private String port;
    private String FixIntAmt;//固定收付息
    private String FloatIntAmt;//浮动收付息
    private String intAmt;//总收付息
    
    
    public String getFixIntAmt() {
		return FixIntAmt;
	}
	public void setFixIntAmt(String fixIntAmt) {
		FixIntAmt = fixIntAmt;
	}
	public String getFloatIntAmt() {
		return FloatIntAmt;
	}
	public void setFloatIntAmt(String floatIntAmt) {
		FloatIntAmt = floatIntAmt;
	}
	public String getiPayDate() {
		return iPayDate;
	}
	public void setiPayDate(String iPayDate) {
		this.iPayDate = iPayDate;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getIntAmt() {
		return intAmt;
	}
	public void setIntAmt(String intAmt) {
		this.intAmt = intAmt;
	}
}
