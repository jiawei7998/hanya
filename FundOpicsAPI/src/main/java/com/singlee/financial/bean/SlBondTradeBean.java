package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 债券交易查询
 * @author wangzt
 *
 */
public class SlBondTradeBean implements Serializable {

	/**
	 * 唯一ID
	 */
	private static final long serialVersionUID = -8699600309200663140L;
	/**
	 * 交易单号
	 */
	private String dealno;
	/**
	 * 状态
	 */
	private String status;

	/**
	 * 交易对手
	 */
	private String cmne;

	/**
	 * 交易序号
	 */
	private String seq;

	/**
	 * 账户
	 */
	private String invtype;

	/**
	 * 交易日	
	 */
	private String dealdate;

	/**
	 * 债券类型
	 */
	private String acctngtype;

	/**
	 * 市场
	 */
	private String market;

	/**
	 * 代码
	 */
	private String secid;

	/**
	 * 名称
	 */
	private String descr;

	/**
	 * 方向
	 */
	private String ps;
	
	
	/**
	 * 清算方式
	 */
	private String method;
	
	/**
	 * 结算方式
	 */
	private String dvp;
	
	/**
	 * 净价(元)
	 */
	private BigDecimal price;
	
	/**
	 * 结算日
	 */
	private String settdate;
	
	/**
	 * 收益率(%)
	 */
	private String yield_8;
	
	/**
	 * 全价(元)
	 */
	private BigDecimal price_8;
	
	/**
	 * 成交面额(万元)
	 */
	private BigDecimal faceamt;
	
	/**
	 * 成交金额(元)
	 */
	private BigDecimal settamt;
	/**
	 * 现金流(元)
	 */
	private BigDecimal cashsettamt;
	/**
	 * 净价金额(元)
	 */
	private BigDecimal costamt;
	/**
	 * 利息(元)
	 */
	private BigDecimal purchintamt;
	/**
	 * 发起人
	 */
	private String trad;
	/**
	 * 备注
	 */
	private String dealtext;
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getDealdate() {
		return dealdate;
	}
	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}
	public String getAcctngtype() {
		return acctngtype;
	}
	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getDvp() {
		return dvp;
	}
	public void setDvp(String dvp) {
		this.dvp = dvp;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getSettdate() {
		return settdate;
	}
	public void setSettdate(String settdate) {
		this.settdate = settdate;
	}
	public String getYield_8() {
		return yield_8;
	}
	public void setYield_8(String yield_8) {
		this.yield_8 = yield_8;
	}
	public BigDecimal getPrice_8() {
		return price_8;
	}
	public void setPrice_8(BigDecimal price_8) {
		this.price_8 = price_8;
	}
	public BigDecimal getFaceamt() {
		return faceamt;
	}
	public void setFaceamt(BigDecimal faceamt) {
		this.faceamt = faceamt;
	}
	public BigDecimal getSettamt() {
		return settamt;
	}
	public void setSettamt(BigDecimal settamt) {
		this.settamt = settamt;
	}
	public BigDecimal getCashsettamt() {
		return cashsettamt;
	}
	public void setCashsettamt(BigDecimal cashsettamt) {
		this.cashsettamt = cashsettamt;
	}
	public BigDecimal getCostamt() {
		return costamt;
	}
	public void setCostamt(BigDecimal costamt) {
		this.costamt = costamt;
	}
	public BigDecimal getPurchintamt() {
		return purchintamt;
	}
	public void setPurchintamt(BigDecimal purchintamt) {
		this.purchintamt = purchintamt;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getDealtext() {
		return dealtext;
	}
	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}
	

}