package com.singlee.financial.bean;

import java.io.Serializable;

public class SlCostBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1659560898864288615L;

	/**
	 * 成本中心
	 */
	private String costCent;
	/**
	 * 成本中心描述
	 */
	private String costDesc;
	/**
	 * 最后维护时间
	 */
	private String lstmntdte;
	/**
	 * 营业单位
	 */
	private String busunit;
	/**
	 * 组织单位
	 */
	private String orgunit;

	public String getCostCent() {
		return costCent;
	}

	public void setCostCent(String costCent) {
		this.costCent = costCent;
	}

	public String getCostDesc() {
		return costDesc;
	}

	public void setCostDesc(String costDesc) {
		this.costDesc = costDesc;
	}

	public String getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getBusunit() {
		return busunit;
	}

	public void setBusunit(String busunit) {
		this.busunit = busunit;
	}

	public String getOrgunit() {
		return orgunit;
	}

	public void setOrgunit(String orgunit) {
		this.orgunit = orgunit;
	}
}
