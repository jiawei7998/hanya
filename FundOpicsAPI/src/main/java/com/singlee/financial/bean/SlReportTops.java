package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * @author zc
 */
public class SlReportTops implements Serializable{
	private static final long serialVersionUID = 1L;
	private String  secId;
	private String  acctngType;
	private String  intcalcRule;
	private String  invType;
	private String  port;
	private String  cost;
	private String  qty;
	private String  settavgCost;
	private String  yield;
	private String  amort_today;
	private String  amortize;
	private String  accrual_today;
	private String  accRual;
	private String  mtm;
	private String  invadj;
	private String  rateCode;
	private String  mdate;
	private String  calltopar;
	private String  puttopar;
	private String  secmSic;
	private String  postDate;
	private String  slte;
	private String br;
	
	
	
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getSlte() {
		return slte;
	}
	public void setSlte(String slte) {
		this.slte = slte;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getAcctngType() {
		return acctngType;
	}
	public void setAcctngType(String acctngType) {
		this.acctngType = acctngType;
	}
	public String getIntcalcRule() {
		return intcalcRule;
	}
	public void setIntcalcRule(String intcalcRule) {
		this.intcalcRule = intcalcRule;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getSettavgCost() {
		return settavgCost;
	}
	public void setSettavgCost(String settavgCost) {
		this.settavgCost = settavgCost;
	}
	public String getYield() {
		return yield;
	}
	public void setYield(String yield) {
		this.yield = yield;
	}
	public String getAmort_today() {
		return amort_today;
	}
	public void setAmort_today(String amort_today) {
		this.amort_today = amort_today;
	}
	public String getAmortize() {
		return amortize;
	}
	public void setAmortize(String amortize) {
		this.amortize = amortize;
	}
	public String getAccrual_today() {
		return accrual_today;
	}
	public void setAccrual_today(String accrual_today) {
		this.accrual_today = accrual_today;
	}
	public String getAccRual() {
		return accRual;
	}
	public void setAccRual(String accRual) {
		this.accRual = accRual;
	}
	public String getMtm() {
		return mtm;
	}
	public void setMtm(String mtm) {
		this.mtm = mtm;
	}
	public String getInvadj() {
		return invadj;
	}
	public void setInvadj(String invadj) {
		this.invadj = invadj;
	}
	public String getRateCode() {
		return rateCode;
	}
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getCalltopar() {
		return calltopar;
	}
	public void setCalltopar(String calltopar) {
		this.calltopar = calltopar;
	}
	public String getPuttopar() {
		return puttopar;
	}
	public void setPuttopar(String puttopar) {
		this.puttopar = puttopar;
	}
	public String getSecmSic() {
		return secmSic;
	}
	public void setSecmSic(String secmSic) {
		this.secmSic = secmSic;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	
}
