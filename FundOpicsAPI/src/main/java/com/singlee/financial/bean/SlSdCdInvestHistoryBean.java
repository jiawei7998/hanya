/**
 * Project Name:FundOpicsAPI
 * File Name:SlSposBean.java
 * Package Name:com.singlee.financial.bean
 * Date:2018-9-21下午04:02:49
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 债券和存单投资损益历史表
 * 
 */
public class SlSdCdInvestHistoryBean implements Serializable{

	private static final long serialVersionUID = 1617452002885851010L;
	
	/**
	 * 数据日期
	 */
	private Date inputdate;   
	/**
	 * 总投资券面合计
	 */
	private String totalFacevalue;  
	/**
	 * 总投资投资收益合计
	 */
	private String totalInvestIncome;  
	/**
	 * 总投资加权收益率合计
	 */
	private String totalWeightRate;   
	/**
	 * 总投资久期
	 */
	private String totalDuration;    
	/**
	 * 总投资DV01
	 */
	private String totalDv01;   
	/**
	 * 总投资估值损益
	 */
	private String totalGainsLosses;    
	/**
	 * H类券面合计
	 */
	private String hFacevalue;  
	/**
	 * H类投资收益合计
	 */
	private String hInvestIncome;  
	/**
	 * H类加权收益率合计
	 */
	private String hWeightRate;   
	/**
	 * H类久期
	 */
	private String hDuration;    
	/**
	 * H类DV01
	 */
	private String hDv01;   
	/**
	 * H类估值损益
	 */
	private String hGainsLosses;  
	/**
	 * A类券面合计
	 */
	private String aFacevalue;  
	/**
	 * A类投资收益合计
	 */
	private String aInvestIncome;  
	/**
	 * A类加权收益率合计
	 */
	private String aWeightRate;   
	/**
	 * A类久期
	 */
	private String aDuration;    
	/**
	 * A类DV01
	 */
	private String aDv01;   
	/**
	 * A类估值损益
	 */
	private String aGainsLosses;  
	/**
	 * T类券面合计
	 */
	private String tFacevalue;  
	/**
	 * T类投资收益合计
	 */
	private String tInvestIncome;  
	/**
	 * T类加权收益率合计
	 */
	private String tWeightRate;   
	/**
	 * T类久期
	 */
	private String tDuration;    
	/**
	 * T类DV01
	 */
	private String tDv01;   
	/**
	 * T类估值损益
	 */
	private String tGainsLosses;
	public Date getInputdate() {
		return inputdate;
	}
	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}
	public String getTotalFacevalue() {
		return totalFacevalue;
	}
	public void setTotalFacevalue(String totalFacevalue) {
		this.totalFacevalue = totalFacevalue;
	}
	public String getTotalInvestIncome() {
		return totalInvestIncome;
	}
	public void setTotalInvestIncome(String totalInvestIncome) {
		this.totalInvestIncome = totalInvestIncome;
	}
	public String getTotalWeightRate() {
		return totalWeightRate;
	}
	public void setTotalWeightRate(String totalWeightRate) {
		this.totalWeightRate = totalWeightRate;
	}
	public String getTotalDuration() {
		return totalDuration;
	}
	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}
	public String getTotalDv01() {
		return totalDv01;
	}
	public void setTotalDv01(String totalDv01) {
		this.totalDv01 = totalDv01;
	}
	public String getTotalGainsLosses() {
		return totalGainsLosses;
	}
	public void setTotalGainsLosses(String totalGainsLosses) {
		this.totalGainsLosses = totalGainsLosses;
	}
	public String gethFacevalue() {
		return hFacevalue;
	}
	public void sethFacevalue(String hFacevalue) {
		this.hFacevalue = hFacevalue;
	}
	public String gethInvestIncome() {
		return hInvestIncome;
	}
	public void sethInvestIncome(String hInvestIncome) {
		this.hInvestIncome = hInvestIncome;
	}
	public String gethWeightRate() {
		return hWeightRate;
	}
	public void sethWeightRate(String hWeightRate) {
		this.hWeightRate = hWeightRate;
	}
	public String gethDuration() {
		return hDuration;
	}
	public void sethDuration(String hDuration) {
		this.hDuration = hDuration;
	}
	public String gethDv01() {
		return hDv01;
	}
	public void sethDv01(String hDv01) {
		this.hDv01 = hDv01;
	}
	public String gethGainsLosses() {
		return hGainsLosses;
	}
	public void sethGainsLosses(String hGainsLosses) {
		this.hGainsLosses = hGainsLosses;
	}
	public String getaFacevalue() {
		return aFacevalue;
	}
	public void setaFacevalue(String aFacevalue) {
		this.aFacevalue = aFacevalue;
	}
	public String getaInvestIncome() {
		return aInvestIncome;
	}
	public void setaInvestIncome(String aInvestIncome) {
		this.aInvestIncome = aInvestIncome;
	}
	public String getaWeightRate() {
		return aWeightRate;
	}
	public void setaWeightRate(String aWeightRate) {
		this.aWeightRate = aWeightRate;
	}
	public String getaDuration() {
		return aDuration;
	}
	public void setaDuration(String aDuration) {
		this.aDuration = aDuration;
	}
	public String getaDv01() {
		return aDv01;
	}
	public void setaDv01(String aDv01) {
		this.aDv01 = aDv01;
	}
	public String getaGainsLosses() {
		return aGainsLosses;
	}
	public void setaGainsLosses(String aGainsLosses) {
		this.aGainsLosses = aGainsLosses;
	}
	public String gettFacevalue() {
		return tFacevalue;
	}
	public void settFacevalue(String tFacevalue) {
		this.tFacevalue = tFacevalue;
	}
	public String gettInvestIncome() {
		return tInvestIncome;
	}
	public void settInvestIncome(String tInvestIncome) {
		this.tInvestIncome = tInvestIncome;
	}
	public String gettWeightRate() {
		return tWeightRate;
	}
	public void settWeightRate(String tWeightRate) {
		this.tWeightRate = tWeightRate;
	}
	public String gettDuration() {
		return tDuration;
	}
	public void settDuration(String tDuration) {
		this.tDuration = tDuration;
	}
	public String gettDv01() {
		return tDv01;
	}
	public void settDv01(String tDv01) {
		this.tDv01 = tDv01;
	}
	public String gettGainsLosses() {
		return tGainsLosses;
	}
	public void settGainsLosses(String tGainsLosses) {
		this.tGainsLosses = tGainsLosses;
	}  
	
}

