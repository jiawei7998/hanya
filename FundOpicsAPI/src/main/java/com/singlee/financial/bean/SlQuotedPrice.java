package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 外汇牌价实体类
 * 
 * @author zhangcm
 * 
 * @date 2019-03-04
 */
public class SlQuotedPrice  implements Serializable {
	 /**
     * 货币代码1
     */
    private String ccyCode;

    /**
     *  货币代码2 
     */
    private String ctrCcyCode;
    /**
     *  生效日期 
     */
    private String effDate;

    /**
     *  生效时间
     */
    private String effTime;

    /**
     *  货币1
     */
    private String ccy;

    /**
     *  货币2
     */
    private String ctrCcy;

    /**
     *  牌价单位
     */
    private BigDecimal price;

    /**
     * 汇买价（最低监管价）
     */
    private BigDecimal minPrice;

    /**
     * 汇卖价（最高监管价）
     */
    private BigDecimal maxPrice;

    /**
     * 中间价（监管中间价） 
     */
    private BigDecimal midPrice;

    /**
     * 炒买价 
     */
    private BigDecimal hotBuyPrice;

    /**
     * 炒卖价 
     */
    private BigDecimal hotSellPrice;

    /**
     *  平盘买入价
     */
    private BigDecimal buyPrice;

    /**
     *  平盘卖出价
     */
    private BigDecimal sellPrice;

    /**
     *  自贸区标志
     */
    private String tradeAreaFlag;

    /**
     * 价格类型
     */
    private String priceType;
    /**
     * 系统日期
     */
    private String postDate;

    private static final long serialVersionUID = 1L;
    public String getCcyCode() {
        return ccyCode;
    }

    public void setCcyCode(String ccyCode) {
        this.ccyCode = ccyCode == null ? null : ccyCode.trim();
    }

    public String getCtrCcyCode() {
        return ctrCcyCode;
    }

    public void setCtrCcyCode(String ctrCcyCode) {
        this.ctrCcyCode = ctrCcyCode == null ? null : ctrCcyCode.trim();
    }
    public String getEffDate() {
        return effDate;
    }

    public void setEffDate(String effDate) {
        this.effDate = effDate == null ? null : effDate.trim();
    }

    public String getEffTime() {
        return effTime;
    }

    public void setEffTime(String effTime) {
        this.effTime = effTime == null ? null : effTime.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getCtrCcy() {
        return ctrCcy;
    }

    public void setCtrCcy(String ctrCcy) {
        this.ctrCcy = ctrCcy == null ? null : ctrCcy.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMidPrice() {
        return midPrice;
    }

    public void setMidPrice(BigDecimal midPrice) {
        this.midPrice = midPrice;
    }

    public BigDecimal getHotBuyPrice() {
        return hotBuyPrice;
    }

    public void setHotBuyPrice(BigDecimal hotBuyPrice) {
        this.hotBuyPrice = hotBuyPrice;
    }

    public BigDecimal getHotSellPrice() {
        return hotSellPrice;
    }

    public void setHotSellPrice(BigDecimal hotSellPrice) {
        this.hotSellPrice = hotSellPrice;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public BigDecimal getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(BigDecimal sellPrice) {
        this.sellPrice = sellPrice;
    }

    public String getTradeAreaFlag() {
        return tradeAreaFlag;
    }

    public void setTradeAreaFlag(String tradeAreaFlag) {
        this.tradeAreaFlag = tradeAreaFlag == null ? null : tradeAreaFlag.trim();
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType == null ? null : priceType.trim();
    }

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
    
}