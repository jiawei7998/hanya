package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 会计类型业务处理类
 * 
 * @author shenzl
 * 
 */
public class SlActyBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -43889239501334610L;

	private String acctngType;

	private String acctDesc;

	private Date lstmntdate;

	/**
	 * 获取会计类型
	 */
	public String getAcctngType() {
		return acctngType;
	}

	/**
	 * 设置会计类型
	 */
	public void setAcctngType(String acctngType) {
		this.acctngType = acctngType;
	}

	/**
	 * 获取会计类型描述
	 */
	public String getAcctDesc() {
		return acctDesc;
	}

	/**
	 * 设置会计类型描述
	 */
	public void setAcctDesc(String acctDesc) {
		this.acctDesc = acctDesc;
	}

	/**
	 * 获取最后维护日期
	 */
	public Date getLstmntdate() {
		return lstmntdate;
	}

	/**
	 * 设置最后维护日期
	 */
	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}
}
