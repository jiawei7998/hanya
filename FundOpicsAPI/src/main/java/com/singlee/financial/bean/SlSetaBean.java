package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/***
 * 清算实体类
 * @author lij
 *
 */
public class SlSetaBean implements Serializable{
	//机构分支
	private String br;
	//清算路径
    private String smeans;
    //清算账户
    private String sacct;
    //币种
	private String ccy;
	//成本中心
    private String costcent;
    //
    private String genledgno;
    //交易对手
    private String cno;
    //
    private BigDecimal acctbal;
    //最后维护时间
    private Date lstmntdte;

    private static final long serialVersionUID = 1L;

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getCostcent() {
        return costcent;
    }

    public void setCostcent(String costcent) {
        this.costcent = costcent == null ? null : costcent.trim();
    }

    public String getGenledgno() {
        return genledgno;
    }

    public void setGenledgno(String genledgno) {
        this.genledgno = genledgno == null ? null : genledgno.trim();
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno == null ? null : cno.trim();
    }

    public BigDecimal getAcctbal() {
        return acctbal;
    }

    public void setAcctbal(BigDecimal acctbal) {
        this.acctbal = acctbal;
    }

    public Date getLstmntdte() {
        return lstmntdte;
    }

    public void setLstmntdte(Date lstmntdte) {
        this.lstmntdte = lstmntdte;
    }
    
    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getSmeans() {
        return smeans;
    }

    public void setSmeans(String smeans) {
        this.smeans = smeans == null ? null : smeans.trim();
    }

    public String getSacct() {
        return sacct;
    }

    public void setSacct(String sacct) {
        this.sacct = sacct == null ? null : sacct.trim();
    }

}
