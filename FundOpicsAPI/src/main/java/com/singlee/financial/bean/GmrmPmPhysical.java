package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 贵金属
 * @author LIJ
 *
 */
public class GmrmPmPhysical implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = -5621646822049176156L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 交易ID
     */
    private String dealId;

    /**
     * 交易员ID
     */
    private String traderId;

    /**
     * 交易日期 YYYYMMDD
     */
    private String tradeDate;

    /**
     * 交易时间 YYYYMMDD HH24:MI:SS
     */
    private String tradeTime;

    /**
     * 起息日
     */
    private String valueDate;

    /**
     * Folder
     */
    private String folderId;

    /**
     * 账户类型 1-银行账户、2-交易账户
     */
    private String accountType;

    /**
     * 买卖标识 1-买入/卖出、3-卖出/买入
     */
    private String bsFlag;

    /**
     * 合约号 默认SP.PAu99.99
     */
    private String contractId;

    /**
     * 合约手数
     */
    private String qty;

    /**
     * 报价单位 1-克、2-盎司、3-千克
     */
    private String quotaUnit;

    /**
     * 品种代码 1-AU、2-AG、3-PT、4-PD
     */
    private String varietyId;

    /**
     * 交易所代码
     */
    private String exchCode;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     * 重量
     */
    private String weight;

    /**
     * 交易所委托号
     */
    private String orderNo;

    /**
     * 手续费
     */
    private BigDecimal commFee;

    /**
     * 交易对手
     */
    private String cptyId;

    /**
     * 币种一名
     */
    private String ccy1;

    /**
     * 币种二名
     */
    private String ccy2;

    /**
     * 金额一
     */
    private BigDecimal amount1;

    /**
     * 金额二
     */
    private BigDecimal amount2;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期 YYYYMMDD
     * @return BATCH_DATE 批量日期 YYYYMMDD
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期 YYYYMMDD
     * @param batchDate 批量日期 YYYYMMDD
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 交易ID
     * @return DEAL_ID 交易ID
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 交易ID
     * @param dealId 交易ID
     */
    public void setDealId(String dealId) {
        this.dealId = dealId == null ? null : dealId.trim();
    }

    /**
     * 交易员ID
     * @return TRADER_ID 交易员ID
     */
    public String getTraderId() {
        return traderId;
    }

    /**
     * 交易员ID
     * @param traderId 交易员ID
     */
    public void setTraderId(String traderId) {
        this.traderId = traderId == null ? null : traderId.trim();
    }

    /**
     * 交易日期 YYYYMMDD
     * @return TRADE_DATE 交易日期 YYYYMMDD
     */
    public String getTradeDate() {
        return tradeDate;
    }

    /**
     * 交易日期 YYYYMMDD
     * @param tradeDate 交易日期 YYYYMMDD
     */
    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate == null ? null : tradeDate.trim();
    }

    /**
     * 交易时间 YYYYMMDD HH24:MI:SS
     * @return TRADE_TIME 交易时间 YYYYMMDD HH24:MI:SS
     */
    public String getTradeTime() {
        return tradeTime;
    }

    /**
     * 交易时间 YYYYMMDD HH24:MI:SS
     * @param tradeTime 交易时间 YYYYMMDD HH24:MI:SS
     */
    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime == null ? null : tradeTime.trim();
    }

    /**
     * 起息日
     * @return VALUE_DATE 起息日
     */
    public String getValueDate() {
        return valueDate;
    }

    /**
     * 起息日
     * @param valueDate 起息日
     */
    public void setValueDate(String valueDate) {
        this.valueDate = valueDate == null ? null : valueDate.trim();
    }

    /**
     * Folder
     * @return FOLDER_ID Folder
     */
    public String getFolderId() {
        return folderId;
    }

    /**
     * Folder
     * @param folderId Folder
     */
    public void setFolderId(String folderId) {
        this.folderId = folderId == null ? null : folderId.trim();
    }

    /**
     * 账户类型 1-银行账户、2-交易账户
     * @return ACCOUNT_TYPE 账户类型 1-银行账户、2-交易账户
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * 账户类型 1-银行账户、2-交易账户
     * @param accountType 账户类型 1-银行账户、2-交易账户
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType == null ? null : accountType.trim();
    }

    /**
     * 买卖标识 1-买入/卖出、3-卖出/买入
     * @return BS_FLAG 买卖标识 1-买入/卖出、3-卖出/买入
     */
    public String getBsFlag() {
        return bsFlag;
    }

    /**
     * 买卖标识 1-买入/卖出、3-卖出/买入
     * @param bsFlag 买卖标识 1-买入/卖出、3-卖出/买入
     */
    public void setBsFlag(String bsFlag) {
        this.bsFlag = bsFlag == null ? null : bsFlag.trim();
    }

    /**
     * 合约号 默认SP.PAu99.99
     * @return CONTRACT_ID 合约号 默认SP.PAu99.99
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * 合约号 默认SP.PAu99.99
     * @param contractId 合约号 默认SP.PAu99.99
     */
    public void setContractId(String contractId) {
        this.contractId = contractId == null ? null : contractId.trim();
    }

    /**
     * 合约手数
     * @return QTY 合约手数
     */
    public String getQty() {
        return qty;
    }

    /**
     * 合约手数
     * @param qty 合约手数
     */
    public void setQty(String qty) {
        this.qty = qty == null ? null : qty.trim();
    }

    /**
     * 报价单位 1-克、2-盎司、3-千克
     * @return QUOTA_UNIT 报价单位 1-克、2-盎司、3-千克
     */
    public String getQuotaUnit() {
        return quotaUnit;
    }

    /**
     * 报价单位 1-克、2-盎司、3-千克
     * @param quotaUnit 报价单位 1-克、2-盎司、3-千克
     */
    public void setQuotaUnit(String quotaUnit) {
        this.quotaUnit = quotaUnit == null ? null : quotaUnit.trim();
    }

    /**
     * 品种代码 1-AU、2-AG、3-PT、4-PD
     * @return VARIETY_ID 品种代码 1-AU、2-AG、3-PT、4-PD
     */
    public String getVarietyId() {
        return varietyId;
    }

    /**
     * 品种代码 1-AU、2-AG、3-PT、4-PD
     * @param varietyId 品种代码 1-AU、2-AG、3-PT、4-PD
     */
    public void setVarietyId(String varietyId) {
        this.varietyId = varietyId == null ? null : varietyId.trim();
    }

    /**
     * 交易所代码
     * @return EXCH_CODE 交易所代码
     */
    public String getExchCode() {
        return exchCode;
    }

    /**
     * 交易所代码
     * @param exchCode 交易所代码
     */
    public void setExchCode(String exchCode) {
        this.exchCode = exchCode == null ? null : exchCode.trim();
    }

    /**
     * 价格
     * @return PRICE 价格
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * 价格
     * @param price 价格
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * 重量
     * @return WEIGHT 重量
     */
    public String getWeight() {
        return weight;
    }

    /**
     * 重量
     * @param weight 重量
     */
    public void setWeight(String weight) {
        this.weight = weight == null ? null : weight.trim();
    }

    /**
     * 交易所委托号
     * @return ORDER_NO 交易所委托号
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * 交易所委托号
     * @param orderNo 交易所委托号
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * 手续费
     * @return COMM_FEE 手续费
     */
    public BigDecimal getCommFee() {
        return commFee;
    }

    /**
     * 手续费
     * @param commFee 手续费
     */
    public void setCommFee(BigDecimal commFee) {
        this.commFee = commFee;
    }

    /**
     * 交易对手
     * @return CPTY_ID 交易对手
     */
    public String getCptyId() {
        return cptyId;
    }

    /**
     * 交易对手
     * @param cptyId 交易对手
     */
    public void setCptyId(String cptyId) {
        this.cptyId = cptyId == null ? null : cptyId.trim();
    }

    /**
     * 币种一名
     * @return CCY_1 币种一名
     */
    public String getCcy1() {
        return ccy1;
    }

    /**
     * 币种一名
     * @param ccy1 币种一名
     */
    public void setCcy1(String ccy1) {
        this.ccy1 = ccy1 == null ? null : ccy1.trim();
    }

    /**
     * 币种二名
     * @return CCY_2 币种二名
     */
    public String getCcy2() {
        return ccy2;
    }

    /**
     * 币种二名
     * @param ccy2 币种二名
     */
    public void setCcy2(String ccy2) {
        this.ccy2 = ccy2 == null ? null : ccy2.trim();
    }

    /**
     * 金额一
     * @return AMOUNT1 金额一
     */
    public BigDecimal getAmount1() {
        return amount1;
    }

    /**
     * 金额一
     * @param amount1 金额一
     */
    public void setAmount1(BigDecimal amount1) {
        this.amount1 = amount1;
    }

    /**
     * 金额二
     * @return AMOUNT2 金额二
     */
    public BigDecimal getAmount2() {
        return amount2;
    }

    /**
     * 金额二
     * @param amount2 金额二
     */
    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    /**
     * 机构号
     * @return BRANCH_ID 机构号
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * 机构号
     * @param branchId 机构号
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }
}