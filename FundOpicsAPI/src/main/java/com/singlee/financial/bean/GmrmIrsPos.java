package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 利率互换
 * @author LIJ
 *
 */
public class GmrmIrsPos implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = -3143563677456032125L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 交易编号
     */
    private String dealId;

    /**
     * 数据来源
     */
    private String source;

    /**
     * 产品类型
     */
    private String prodType;

    /**
     * Folder
     */
    private String folder;

    /**
     * 交易日期 YYYYMMDD
     */
    private String tradeDate;

    /**
     * 起息日 (Pay Leg) YYYYMMDD
     */
    private String valueDate;

    /**
     * 到期日 (Pay Leg) YYYYMMDD
     */
    private String maturityDat;

    /**
     * 本金交换方式 0、NONE 1、START 2、END 3、BOTH 4、NEITHER
     */
    private String notionalExchange;

    /**
     * 支付端本金
     */
    private BigDecimal payNotional;

    /**
     * 支付端币种
     */
    private String payCcy;

    /**
     * 支付端本金变更日期 YYYYMMDD
     */
    private String payChangeDate;

    /**
     * 其他支付端本金金额
     */
    private BigDecimal payOtherNotional;

    /**
     * 支付端是浮动或固定标志 1、Fixed固定 2、Floating浮动
     */
    private String payFlag;

    /**
     * 支付端重置标志 0-不重置，1-重置
     */
    private String payResetFlag;

    /**
     * 支付端摊销标志 0-不摊销，1-摊销
     */
    private String payAmortFlag;

    /**
     * 支付端利率
     */
    private BigDecimal payRate;

    /**
     * 支付端复利计息类型 0、none 1、Spread Included 2、Spread Excluded 3、Flat 4、Forward Rate
     */
    private String payCompoundType;

    /**
     * 支付端点差
     */
    private BigDecimal paySpread;

    /**
     * 支付端指标 1-LIBOR，2-SHIBOR，3-FIXED，4-BBSW，5-DEP1Y，6-FR001，7-FR007，8-SHI3M......
     */
    private String payIndex;

    /**
     * 支付端期限
     */
    private String payTerm;

    /**
     * 支付端日期惯例
     */
    private String payCalcDays;

    /**
     * 支付端付息频率
     */
    private String payFreq;

    /**
     * 支付端支付日 days
     */
    private String payPayDay;

    /**
     * 支付端支付节假日计数规则
     */
    private String payRolldateRule;

    /**
     * 支付端利率重置频率
     */
    private String payResetFreq;

    /**
     * 支付端重置日 days
     */
    private String payResetDay;

    /**
     * 支付端重置日节假日计数规则
     */
    private String payResetRolldateRule;

    /**
     * 收取端本金
     */
    private BigDecimal recNotional;

    /**
     * 收取端币种
     */
    private String recCcy;

    /**
     * 收取端本金变更日期 YYYYMMDD
     */
    private String recChangeDate;

    /**
     * 其他收取端本金金额
     */
    private BigDecimal recOtherNotional;

    /**
     * 收取端是浮动或固定标志
     */
    private String recFlag;

    /**
     * 收取端重置标志
     */
    private String recResetFlag;

    /**
     * 收取端摊销标志
     */
    private String recAmortFlag;

    /**
     * 收取端利率
     */
    private BigDecimal recRate;

    /**
     * 收取端复利计息类型
     */
    private String recCompoundType;

    /**
     * 收取端点差
     */
    private BigDecimal recSpread;

    /**
     * 收取端指标
     */
    private String recIndex;

    /**
     * 收取端期限
     */
    private String recTerm;

    /**
     * 收取端日期惯例
     */
    private String recCalcDays;

    /**
     * 收取端支付频率
     */
    private String recFreq;

    /**
     * 收取端支付日 days
     */
    private String recPayDay;

    /**
     * 收取端支付节假日计数规则
     */
    private String recRolldateRule;

    /**
     * 收取端利率重置频率
     */
    private String recResetFreq;

    /**
     * 收取端重置日 days
     */
    private String recResetDay;

    /**
     * 收取端重置日节假日计数规则
     */
    private String recResetRolldateRule;

    /**
     * 收取端首次起息日 YYYYMMDD
     */
    private String recValueDate;

    /**
     * 收取端到期日 YYYYMMDD
     */
    private String recMaturityDate;

    /**
     * 支付端本金币种
     */
    private String payNotionalCcy;

    /**
     * 收取端本金币种
     */
    private String recNotionalCcy;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * PV币种
     */
    private String pvCcy;

    /**
     * 每日PL
     */
    private BigDecimal dailyPl;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期 YYYYMMDD
     * @return BATCH_DATE 批量日期 YYYYMMDD
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期 YYYYMMDD
     * @param batchDate 批量日期 YYYYMMDD
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 交易编号
     * @return DEAL_ID 交易编号
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 交易编号
     * @param dealId 交易编号
     */
    public void setDealId(String dealId) {
        this.dealId = dealId == null ? null : dealId.trim();
    }

    /**
     * 数据来源
     * @return SOURCE 数据来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 数据来源
     * @param source 数据来源
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * 产品类型
     * @return PROD_TYPE 产品类型
     */
    public String getProdType() {
        return prodType;
    }

    /**
     * 产品类型
     * @param prodType 产品类型
     */
    public void setProdType(String prodType) {
        this.prodType = prodType == null ? null : prodType.trim();
    }

    /**
     * Folder
     * @return FOLDER Folder
     */
    public String getFolder() {
        return folder;
    }

    /**
     * Folder
     * @param folder Folder
     */
    public void setFolder(String folder) {
        this.folder = folder == null ? null : folder.trim();
    }

    /**
     * 交易日期 YYYYMMDD
     * @return TRADE_DATE 交易日期 YYYYMMDD
     */
    public String getTradeDate() {
        return tradeDate;
    }

    /**
     * 交易日期 YYYYMMDD
     * @param tradeDate 交易日期 YYYYMMDD
     */
    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate == null ? null : tradeDate.trim();
    }

    /**
     * 起息日 (Pay Leg) YYYYMMDD
     * @return VALUE_DATE 起息日 (Pay Leg) YYYYMMDD
     */
    public String getValueDate() {
        return valueDate;
    }

    /**
     * 起息日 (Pay Leg) YYYYMMDD
     * @param valueDate 起息日 (Pay Leg) YYYYMMDD
     */
    public void setValueDate(String valueDate) {
        this.valueDate = valueDate == null ? null : valueDate.trim();
    }

    /**
     * 到期日 (Pay Leg) YYYYMMDD
     * @return MATURITY_DAT 到期日 (Pay Leg) YYYYMMDD
     */
    public String getMaturityDat() {
        return maturityDat;
    }

    /**
     * 到期日 (Pay Leg) YYYYMMDD
     * @param maturityDat 到期日 (Pay Leg) YYYYMMDD
     */
    public void setMaturityDat(String maturityDat) {
        this.maturityDat = maturityDat == null ? null : maturityDat.trim();
    }

    /**
     * 本金交换方式 0、NONE 1、START 2、END 3、BOTH 4、NEITHER
     * @return NOTIONAL_EXCHANGE 本金交换方式 0、NONE 1、START 2、END 3、BOTH 4、NEITHER
     */
    public String getNotionalExchange() {
        return notionalExchange;
    }

    /**
     * 本金交换方式 0、NONE 1、START 2、END 3、BOTH 4、NEITHER
     * @param notionalExchange 本金交换方式 0、NONE 1、START 2、END 3、BOTH 4、NEITHER
     */
    public void setNotionalExchange(String notionalExchange) {
        this.notionalExchange = notionalExchange == null ? null : notionalExchange.trim();
    }

    /**
     * 支付端本金
     * @return PAY_NOTIONAL 支付端本金
     */
    public BigDecimal getPayNotional() {
        return payNotional;
    }

    /**
     * 支付端本金
     * @param payNotional 支付端本金
     */
    public void setPayNotional(BigDecimal payNotional) {
        this.payNotional = payNotional;
    }

    /**
     * 支付端币种
     * @return PAY_CCY 支付端币种
     */
    public String getPayCcy() {
        return payCcy;
    }

    /**
     * 支付端币种
     * @param payCcy 支付端币种
     */
    public void setPayCcy(String payCcy) {
        this.payCcy = payCcy == null ? null : payCcy.trim();
    }

    /**
     * 支付端本金变更日期 YYYYMMDD
     * @return PAY_CHANGE_DATE 支付端本金变更日期 YYYYMMDD
     */
    public String getPayChangeDate() {
        return payChangeDate;
    }

    /**
     * 支付端本金变更日期 YYYYMMDD
     * @param payChangeDate 支付端本金变更日期 YYYYMMDD
     */
    public void setPayChangeDate(String payChangeDate) {
        this.payChangeDate = payChangeDate == null ? null : payChangeDate.trim();
    }

    /**
     * 其他支付端本金金额
     * @return PAY_OTHER_NOTIONAL 其他支付端本金金额
     */
    public BigDecimal getPayOtherNotional() {
        return payOtherNotional;
    }

    /**
     * 其他支付端本金金额
     * @param payOtherNotional 其他支付端本金金额
     */
    public void setPayOtherNotional(BigDecimal payOtherNotional) {
        this.payOtherNotional = payOtherNotional;
    }

    /**
     * 支付端是浮动或固定标志 1、Fixed固定 2、Floating浮动
     * @return PAY_FLAG 支付端是浮动或固定标志 1、Fixed固定 2、Floating浮动
     */
    public String getPayFlag() {
        return payFlag;
    }

    /**
     * 支付端是浮动或固定标志 1、Fixed固定 2、Floating浮动
     * @param payFlag 支付端是浮动或固定标志 1、Fixed固定 2、Floating浮动
     */
    public void setPayFlag(String payFlag) {
        this.payFlag = payFlag == null ? null : payFlag.trim();
    }

    /**
     * 支付端重置标志 0-不重置，1-重置
     * @return PAY_RESET_FLAG 支付端重置标志 0-不重置，1-重置
     */
    public String getPayResetFlag() {
        return payResetFlag;
    }

    /**
     * 支付端重置标志 0-不重置，1-重置
     * @param payResetFlag 支付端重置标志 0-不重置，1-重置
     */
    public void setPayResetFlag(String payResetFlag) {
        this.payResetFlag = payResetFlag == null ? null : payResetFlag.trim();
    }

    /**
     * 支付端摊销标志 0-不摊销，1-摊销
     * @return PAY_AMORT_FLAG 支付端摊销标志 0-不摊销，1-摊销
     */
    public String getPayAmortFlag() {
        return payAmortFlag;
    }

    /**
     * 支付端摊销标志 0-不摊销，1-摊销
     * @param payAmortFlag 支付端摊销标志 0-不摊销，1-摊销
     */
    public void setPayAmortFlag(String payAmortFlag) {
        this.payAmortFlag = payAmortFlag == null ? null : payAmortFlag.trim();
    }

    /**
     * 支付端利率
     * @return PAY_RATE 支付端利率
     */
    public BigDecimal getPayRate() {
        return payRate;
    }

    /**
     * 支付端利率
     * @param payRate 支付端利率
     */
    public void setPayRate(BigDecimal payRate) {
        this.payRate = payRate;
    }

    /**
     * 支付端复利计息类型 0、none 1、Spread Included 2、Spread Excluded 3、Flat 4、Forward Rate
     * @return PAY_COMPOUND_TYPE 支付端复利计息类型 0、none 1、Spread Included 2、Spread Excluded 3、Flat 4、Forward Rate
     */
    public String getPayCompoundType() {
        return payCompoundType;
    }

    /**
     * 支付端复利计息类型 0、none 1、Spread Included 2、Spread Excluded 3、Flat 4、Forward Rate
     * @param payCompoundType 支付端复利计息类型 0、none 1、Spread Included 2、Spread Excluded 3、Flat 4、Forward Rate
     */
    public void setPayCompoundType(String payCompoundType) {
        this.payCompoundType = payCompoundType == null ? null : payCompoundType.trim();
    }

    /**
     * 支付端点差
     * @return PAY_SPREAD 支付端点差
     */
    public BigDecimal getPaySpread() {
        return paySpread;
    }

    /**
     * 支付端点差
     * @param paySpread 支付端点差
     */
    public void setPaySpread(BigDecimal paySpread) {
        this.paySpread = paySpread;
    }

    /**
     * 支付端指标 1-LIBOR，2-SHIBOR，3-FIXED，4-BBSW，5-DEP1Y，6-FR001，7-FR007，8-SHI3M......
     * @return PAY_INDEX 支付端指标 1-LIBOR，2-SHIBOR，3-FIXED，4-BBSW，5-DEP1Y，6-FR001，7-FR007，8-SHI3M......
     */
    public String getPayIndex() {
        return payIndex;
    }

    /**
     * 支付端指标 1-LIBOR，2-SHIBOR，3-FIXED，4-BBSW，5-DEP1Y，6-FR001，7-FR007，8-SHI3M......
     * @param payIndex 支付端指标 1-LIBOR，2-SHIBOR，3-FIXED，4-BBSW，5-DEP1Y，6-FR001，7-FR007，8-SHI3M......
     */
    public void setPayIndex(String payIndex) {
        this.payIndex = payIndex == null ? null : payIndex.trim();
    }

    /**
     * 支付端期限
     * @return PAY_TERM 支付端期限
     */
    public String getPayTerm() {
        return payTerm;
    }

    /**
     * 支付端期限
     * @param payTerm 支付端期限
     */
    public void setPayTerm(String payTerm) {
        this.payTerm = payTerm == null ? null : payTerm.trim();
    }

    /**
     * 支付端日期惯例
     * @return PAY_CALC_DAYS 支付端日期惯例
     */
    public String getPayCalcDays() {
        return payCalcDays;
    }

    /**
     * 支付端日期惯例
     * @param payCalcDays 支付端日期惯例
     */
    public void setPayCalcDays(String payCalcDays) {
        this.payCalcDays = payCalcDays == null ? null : payCalcDays.trim();
    }

    /**
     * 支付端付息频率
     * @return PAY_FREQ 支付端付息频率
     */
    public String getPayFreq() {
        return payFreq;
    }

    /**
     * 支付端付息频率
     * @param payFreq 支付端付息频率
     */
    public void setPayFreq(String payFreq) {
        this.payFreq = payFreq == null ? null : payFreq.trim();
    }

    /**
     * 支付端支付日 days
     * @return PAY_PAY_DAY 支付端支付日 days
     */
    public String getPayPayDay() {
        return payPayDay;
    }

    /**
     * 支付端支付日 days
     * @param payPayDay 支付端支付日 days
     */
    public void setPayPayDay(String payPayDay) {
        this.payPayDay = payPayDay == null ? null : payPayDay.trim();
    }

    /**
     * 支付端支付节假日计数规则
     * @return PAY_ROLLDATE_RULE 支付端支付节假日计数规则
     */
    public String getPayRolldateRule() {
        return payRolldateRule;
    }

    /**
     * 支付端支付节假日计数规则
     * @param payRolldateRule 支付端支付节假日计数规则
     */
    public void setPayRolldateRule(String payRolldateRule) {
        this.payRolldateRule = payRolldateRule == null ? null : payRolldateRule.trim();
    }

    /**
     * 支付端利率重置频率
     * @return PAY_RESET_FREQ 支付端利率重置频率
     */
    public String getPayResetFreq() {
        return payResetFreq;
    }

    /**
     * 支付端利率重置频率
     * @param payResetFreq 支付端利率重置频率
     */
    public void setPayResetFreq(String payResetFreq) {
        this.payResetFreq = payResetFreq == null ? null : payResetFreq.trim();
    }

    /**
     * 支付端重置日 days
     * @return PAY_RESET_DAY 支付端重置日 days
     */
    public String getPayResetDay() {
        return payResetDay;
    }

    /**
     * 支付端重置日 days
     * @param payResetDay 支付端重置日 days
     */
    public void setPayResetDay(String payResetDay) {
        this.payResetDay = payResetDay == null ? null : payResetDay.trim();
    }

    /**
     * 支付端重置日节假日计数规则
     * @return PAY_RESET_ROLLDATE_RULE 支付端重置日节假日计数规则
     */
    public String getPayResetRolldateRule() {
        return payResetRolldateRule;
    }

    /**
     * 支付端重置日节假日计数规则
     * @param payResetRolldateRule 支付端重置日节假日计数规则
     */
    public void setPayResetRolldateRule(String payResetRolldateRule) {
        this.payResetRolldateRule = payResetRolldateRule == null ? null : payResetRolldateRule.trim();
    }

    /**
     * 收取端本金
     * @return REC_NOTIONAL 收取端本金
     */
    public BigDecimal getRecNotional() {
        return recNotional;
    }

    /**
     * 收取端本金
     * @param recNotional 收取端本金
     */
    public void setRecNotional(BigDecimal recNotional) {
        this.recNotional = recNotional;
    }

    /**
     * 收取端币种
     * @return REC_CCY 收取端币种
     */
    public String getRecCcy() {
        return recCcy;
    }

    /**
     * 收取端币种
     * @param recCcy 收取端币种
     */
    public void setRecCcy(String recCcy) {
        this.recCcy = recCcy == null ? null : recCcy.trim();
    }

    /**
     * 收取端本金变更日期 YYYYMMDD
     * @return REC_CHANGE_DATE 收取端本金变更日期 YYYYMMDD
     */
    public String getRecChangeDate() {
        return recChangeDate;
    }

    /**
     * 收取端本金变更日期 YYYYMMDD
     * @param recChangeDate 收取端本金变更日期 YYYYMMDD
     */
    public void setRecChangeDate(String recChangeDate) {
        this.recChangeDate = recChangeDate == null ? null : recChangeDate.trim();
    }

    /**
     * 其他收取端本金金额
     * @return REC_OTHER_NOTIONAL 其他收取端本金金额
     */
    public BigDecimal getRecOtherNotional() {
        return recOtherNotional;
    }

    /**
     * 其他收取端本金金额
     * @param recOtherNotional 其他收取端本金金额
     */
    public void setRecOtherNotional(BigDecimal recOtherNotional) {
        this.recOtherNotional = recOtherNotional;
    }

    /**
     * 收取端是浮动或固定标志
     * @return REC_FLAG 收取端是浮动或固定标志
     */
    public String getRecFlag() {
        return recFlag;
    }

    /**
     * 收取端是浮动或固定标志
     * @param recFlag 收取端是浮动或固定标志
     */
    public void setRecFlag(String recFlag) {
        this.recFlag = recFlag == null ? null : recFlag.trim();
    }

    /**
     * 收取端重置标志
     * @return REC_RESET_FLAG 收取端重置标志
     */
    public String getRecResetFlag() {
        return recResetFlag;
    }

    /**
     * 收取端重置标志
     * @param recResetFlag 收取端重置标志
     */
    public void setRecResetFlag(String recResetFlag) {
        this.recResetFlag = recResetFlag == null ? null : recResetFlag.trim();
    }

    /**
     * null
     * @return REC_AMORT_FLAG null
     */
    public String getRecAmortFlag() {
        return recAmortFlag;
    }

    /**
     * null
     * @param recAmortFlag null
     */
    public void setRecAmortFlag(String recAmortFlag) {
        this.recAmortFlag = recAmortFlag == null ? null : recAmortFlag.trim();
    }

    /**
     * null
     * @return REC_RATE null
     */
    public BigDecimal getRecRate() {
        return recRate;
    }

    /**
     * null
     * @param recRate null
     */
    public void setRecRate(BigDecimal recRate) {
        this.recRate = recRate;
    }

    /**
     * null
     * @return REC_COMPOUND_TYPE null
     */
    public String getRecCompoundType() {
        return recCompoundType;
    }

    /**
     * null
     * @param recCompoundType null
     */
    public void setRecCompoundType(String recCompoundType) {
        this.recCompoundType = recCompoundType == null ? null : recCompoundType.trim();
    }

    /**
     * null
     * @return REC_SPREAD null
     */
    public BigDecimal getRecSpread() {
        return recSpread;
    }

    /**
     * null
     * @param recSpread null
     */
    public void setRecSpread(BigDecimal recSpread) {
        this.recSpread = recSpread;
    }

    /**
     * null
     * @return REC_INDEX null
     */
    public String getRecIndex() {
        return recIndex;
    }

    /**
     * null
     * @param recIndex null
     */
    public void setRecIndex(String recIndex) {
        this.recIndex = recIndex == null ? null : recIndex.trim();
    }

    /**
     * null
     * @return REC_TERM null
     */
    public String getRecTerm() {
        return recTerm;
    }

    /**
     * null
     * @param recTerm null
     */
    public void setRecTerm(String recTerm) {
        this.recTerm = recTerm == null ? null : recTerm.trim();
    }

    /**
     * null
     * @return REC_CALC_DAYS null
     */
    public String getRecCalcDays() {
        return recCalcDays;
    }

    /**
     * null
     * @param recCalcDays null
     */
    public void setRecCalcDays(String recCalcDays) {
        this.recCalcDays = recCalcDays == null ? null : recCalcDays.trim();
    }

    /**
     * null
     * @return REC_FREQ null
     */
    public String getRecFreq() {
        return recFreq;
    }

    /**
     * null
     * @param recFreq null
     */
    public void setRecFreq(String recFreq) {
        this.recFreq = recFreq == null ? null : recFreq.trim();
    }

    /**
     * null
     * @return REC_PAY_DAY null
     */
    public String getRecPayDay() {
        return recPayDay;
    }

    /**
     * null
     * @param recPayDay null
     */
    public void setRecPayDay(String recPayDay) {
        this.recPayDay = recPayDay == null ? null : recPayDay.trim();
    }

    /**
     * null
     * @return REC_ROLLDATE_RULE null
     */
    public String getRecRolldateRule() {
        return recRolldateRule;
    }

    /**
     * null
     * @param recRolldateRule null
     */
    public void setRecRolldateRule(String recRolldateRule) {
        this.recRolldateRule = recRolldateRule == null ? null : recRolldateRule.trim();
    }

    /**
     * null
     * @return REC_RESET_FREQ null
     */
    public String getRecResetFreq() {
        return recResetFreq;
    }

    /**
     * null
     * @param recResetFreq null
     */
    public void setRecResetFreq(String recResetFreq) {
        this.recResetFreq = recResetFreq == null ? null : recResetFreq.trim();
    }

    /**
     * null
     * @return REC_RESET_DAY null
     */
    public String getRecResetDay() {
        return recResetDay;
    }

    /**
     * null
     * @param recResetDay null
     */
    public void setRecResetDay(String recResetDay) {
        this.recResetDay = recResetDay == null ? null : recResetDay.trim();
    }

    /**
     * null
     * @return REC_RESET_ROLLDATE_RULE null
     */
    public String getRecResetRolldateRule() {
        return recResetRolldateRule;
    }

    /**
     * null
     * @param recResetRolldateRule null
     */
    public void setRecResetRolldateRule(String recResetRolldateRule) {
        this.recResetRolldateRule = recResetRolldateRule == null ? null : recResetRolldateRule.trim();
    }

    /**
     * null
     * @return REC_VALUE_DATE null
     */
    public String getRecValueDate() {
        return recValueDate;
    }

    /**
     * null
     * @param recValueDate null
     */
    public void setRecValueDate(String recValueDate) {
        this.recValueDate = recValueDate == null ? null : recValueDate.trim();
    }

    /**
     * null
     * @return REC_MATURITY_DATE null
     */
    public String getRecMaturityDate() {
        return recMaturityDate;
    }

    /**
     * null
     * @param recMaturityDate null
     */
    public void setRecMaturityDate(String recMaturityDate) {
        this.recMaturityDate = recMaturityDate == null ? null : recMaturityDate.trim();
    }

    /**
     * null
     * @return PAY_NOTIONAL_CCY null
     */
    public String getPayNotionalCcy() {
        return payNotionalCcy;
    }

    /**
     * null
     * @param payNotionalCcy null
     */
    public void setPayNotionalCcy(String payNotionalCcy) {
        this.payNotionalCcy = payNotionalCcy == null ? null : payNotionalCcy.trim();
    }

    /**
     * null
     * @return REC_NOTIONAL_CCY null
     */
    public String getRecNotionalCcy() {
        return recNotionalCcy;
    }

    /**
     * null
     * @param recNotionalCcy null
     */
    public void setRecNotionalCcy(String recNotionalCcy) {
        this.recNotionalCcy = recNotionalCcy == null ? null : recNotionalCcy.trim();
    }

    /**
     * null
     * @return BRANCH_ID null
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * null
     * @param branchId null
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }

    /**
     * null
     * @return PV_CCY null
     */
    public String getPvCcy() {
        return pvCcy;
    }

    /**
     * null
     * @param pvCcy null
     */
    public void setPvCcy(String pvCcy) {
        this.pvCcy = pvCcy == null ? null : pvCcy.trim();
    }

    /**
     * null
     * @return DAILY_PL null
     */
    public BigDecimal getDailyPl() {
        return dailyPl;
    }

    /**
     * null
     * @param dailyPl null
     */
    public void setDailyPl(BigDecimal dailyPl) {
        this.dailyPl = dailyPl;
    }
}
