package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 债券重定价提醒
 * 
 *
 */
public class SlSecmRevaluation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6253101236466854696L;

	private String br;
	
	private String secid;

	private String product;

	private String prodtype;
	
	private Date mdate;
	
	private Date ratefixdate;
	
	private Date intstrtdte;

	private String seq;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public Date getMdate() {
		return mdate;
	}

	public void setMdate(Date mdate) {
		this.mdate = mdate;
	}

	public Date getRatefixdate() {
		return ratefixdate;
	}

	public void setRatefixdate(Date ratefixdate) {
		this.ratefixdate = ratefixdate;
	}

	public Date getIntstrtdte() {
		return intstrtdte;
	}

	public void setIntstrtdte(Date intstrtdte) {
		this.intstrtdte = intstrtdte;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	

	
}
