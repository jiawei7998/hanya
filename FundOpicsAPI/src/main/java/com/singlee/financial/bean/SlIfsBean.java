package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 同业接口处理类
 */
public class SlIfsBean implements Serializable {

	private static final long serialVersionUID = 6228437051315169088L;

	/**
	 * ESB返回码
	 */
	private String retCode;

	/**
	 * ESB返回信息
	 */
	private String retMsg;

	/**
	 * 产品属性信息数组
	 */
	private List<PdAttrInfoArray> PD_ATTR_INFO_ARRAY;

	/**
	 * 业务种类信息数组
	 */
	private List<BussVrtyInfoArray> BUSS_VRTY_INFO_ARRAY;

	/**
	 * 核心客户号
	 */
	private String CORE_CLNT_NO;

	/**
	 * 公司名称
	 */
	private String CO_NM;

	/**
	 * 业务类型
	 */
	private String BUSS_TYPE;

	/**
	 * 业务类型名称
	 */
	private String BUSS_TP_NM;

	/**
	 * 机构码
	 */
	private String BRANCH_CODE;

	/**
	 * 机构名称
	 */
	private String BRANCH_NAME;

	/**
	 * 包含下级标志
	 */
	private String INCL_SUB_FLG;

	/**
	 * 审批日期
	 */
	private Date APPROVAL_DATE;

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public List<PdAttrInfoArray> getPD_ATTR_INFO_ARRAY() {
		return PD_ATTR_INFO_ARRAY;
	}

	public void setPD_ATTR_INFO_ARRAY(List<PdAttrInfoArray> pD_ATTR_INFO_ARRAY) {
		PD_ATTR_INFO_ARRAY = pD_ATTR_INFO_ARRAY;
	}

	public List<BussVrtyInfoArray> getBUSS_VRTY_INFO_ARRAY() {
		return BUSS_VRTY_INFO_ARRAY;
	}

	public void setBUSS_VRTY_INFO_ARRAY(
			List<BussVrtyInfoArray> bUSS_VRTY_INFO_ARRAY) {
		BUSS_VRTY_INFO_ARRAY = bUSS_VRTY_INFO_ARRAY;
	}

	public String getCORE_CLNT_NO() {
		return CORE_CLNT_NO;
	}

	public void setCORE_CLNT_NO(String cORE_CLNT_NO) {
		CORE_CLNT_NO = cORE_CLNT_NO;
	}

	public String getCO_NM() {
		return CO_NM;
	}

	public void setCO_NM(String cO_NM) {
		CO_NM = cO_NM;
	}

	public String getBUSS_TYPE() {
		return BUSS_TYPE;
	}

	public void setBUSS_TYPE(String bUSS_TYPE) {
		BUSS_TYPE = bUSS_TYPE;
	}

	public String getBUSS_TP_NM() {
		return BUSS_TP_NM;
	}

	public void setBUSS_TP_NM(String bUSS_TP_NM) {
		BUSS_TP_NM = bUSS_TP_NM;
	}

	public String getBRANCH_CODE() {
		return BRANCH_CODE;
	}

	public void setBRANCH_CODE(String bRANCH_CODE) {
		BRANCH_CODE = bRANCH_CODE;
	}

	public String getBRANCH_NAME() {
		return BRANCH_NAME;
	}

	public void setBRANCH_NAME(String bRANCH_NAME) {
		BRANCH_NAME = bRANCH_NAME;
	}

	public String getINCL_SUB_FLG() {
		return INCL_SUB_FLG;
	}

	public void setINCL_SUB_FLG(String iNCL_SUB_FLG) {
		INCL_SUB_FLG = iNCL_SUB_FLG;
	}

	public Date getAPPROVAL_DATE() {
		return APPROVAL_DATE;
	}

	public void setAPPROVAL_DATE(Date aPPROVAL_DATE) {
		APPROVAL_DATE = aPPROVAL_DATE;
	}
}