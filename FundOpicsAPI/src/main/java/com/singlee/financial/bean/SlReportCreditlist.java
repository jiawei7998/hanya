package com.singlee.financial.bean;


import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 同业额度明细报表
 * @author LIJ
 *
 */
public class SlReportCreditlist implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5779919366596517479L;

	/**
     * FI Name
     */
    private String groupid;

    /**
     * 金融机构名称
     */
    private Object groupname;

    /**
     * 币种
     */
    private String ccy;

    /**
     * 外汇交易-外汇信贷
     */
    private BigDecimal fxccredit;

    /**
     * 外汇交易-即日结算额度
     */
    private BigDecimal fxdslcredit;

    /**
     * 同业存放-审批额度
     */
    private BigDecimal mmcredit;

    /**
     * 同业存放-已使用额度
     */
    private BigDecimal mmusedcredit;

    /**
     * 同业存放-剩余额度
     */
    private BigDecimal mmremaincredit;

    /**
     * 贸易融资-审批额度
     */
    private BigDecimal tradecredit;

    /**
     * 贸易融资-已使用额度
     */
    private BigDecimal tradeusedcredit;

    /**
     * 贸易融资-剩余额度
     */
    private BigDecimal traderemaincredit;

    /**
     * 合计：（外汇交易-外汇信贷）+（同业存放-审批额度）+（贸易融资-审批额度）
     */
    private BigDecimal totcredit;

    /**
     * 
     */
    private BigDecimal errflag;

    /**
     * 查询日期
     */
    private String rpdate;

    
    public String getGroupid() {
        return groupid;
    }

    
    public void setGroupid(String groupid) {
        this.groupid = groupid == null ? null : groupid.trim();
    }

    
    public Object getGroupname() {
        return groupname;
    }

    
    public void setGroupname(Object groupname) {
        this.groupname = groupname;
    }

    /**
     * 币种
     * @return CCY 币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 币种
     * @param ccy 币种
     */
    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    /**
     * 外汇交易-外汇信贷
     * @return FXCCREDIT 外汇交易-外汇信贷
     */
    public BigDecimal getFxccredit() {
        return fxccredit;
    }

    /**
     * 外汇交易-外汇信贷
     * @param fxccredit 外汇交易-外汇信贷
     */
    public void setFxccredit(BigDecimal fxccredit) {
        this.fxccredit = fxccredit;
    }

    /**
     * 外汇交易-即日结算额度
     * @return FXDSLCREDIT 外汇交易-即日结算额度
     */
    public BigDecimal getFxdslcredit() {
        return fxdslcredit;
    }

    /**
     * 外汇交易-即日结算额度
     * @param fxdslcredit 外汇交易-即日结算额度
     */
    public void setFxdslcredit(BigDecimal fxdslcredit) {
        this.fxdslcredit = fxdslcredit;
    }

    /**
     * 同业存放-审批额度
     * @return MMCREDIT 同业存放-审批额度
     */
    public BigDecimal getMmcredit() {
        return mmcredit;
    }

    /**
     * 同业存放-审批额度
     * @param mmcredit 同业存放-审批额度
     */
    public void setMmcredit(BigDecimal mmcredit) {
        this.mmcredit = mmcredit;
    }

    /**
     * 同业存放-已使用额度
     * @return MMUSEDCREDIT 同业存放-已使用额度
     */
    public BigDecimal getMmusedcredit() {
        return mmusedcredit;
    }

    /**
     * 同业存放-已使用额度
     * @param mmusedcredit 同业存放-已使用额度
     */
    public void setMmusedcredit(BigDecimal mmusedcredit) {
        this.mmusedcredit = mmusedcredit;
    }

    /**
     * 同业存放-剩余额度
     * @return MMREMAINCREDIT 同业存放-剩余额度
     */
    public BigDecimal getMmremaincredit() {
        return mmremaincredit;
    }

    /**
     * 同业存放-剩余额度
     * @param mmremaincredit 同业存放-剩余额度
     */
    public void setMmremaincredit(BigDecimal mmremaincredit) {
        this.mmremaincredit = mmremaincredit;
    }

    /**
     * 贸易融资-审批额度
     * @return TRADECREDIT 贸易融资-审批额度
     */
    public BigDecimal getTradecredit() {
        return tradecredit;
    }

    /**
     * 贸易融资-审批额度
     * @param tradecredit 贸易融资-审批额度
     */
    public void setTradecredit(BigDecimal tradecredit) {
        this.tradecredit = tradecredit;
    }

    /**
     * 贸易融资-已使用额度
     * @return TRADEUSEDCREDIT 贸易融资-已使用额度
     */
    public BigDecimal getTradeusedcredit() {
        return tradeusedcredit;
    }

    /**
     * 贸易融资-已使用额度
     * @param tradeusedcredit 贸易融资-已使用额度
     */
    public void setTradeusedcredit(BigDecimal tradeusedcredit) {
        this.tradeusedcredit = tradeusedcredit;
    }

    /**
     * 贸易融资-剩余额度
     * @return TRADEREMAINCREDIT 贸易融资-剩余额度
     */
    public BigDecimal getTraderemaincredit() {
        return traderemaincredit;
    }

    /**
     * 贸易融资-剩余额度
     * @param traderemaincredit 贸易融资-剩余额度
     */
    public void setTraderemaincredit(BigDecimal traderemaincredit) {
        this.traderemaincredit = traderemaincredit;
    }

    /**
     * 合计：（外汇交易-外汇信贷）+（同业存放-审批额度）+（贸易融资-审批额度）
     * @return TOTCREDIT 合计：（外汇交易-外汇信贷）+（同业存放-审批额度）+（贸易融资-审批额度）
     */
    public BigDecimal getTotcredit() {
        return totcredit;
    }

    /**
     * 合计：（外汇交易-外汇信贷）+（同业存放-审批额度）+（贸易融资-审批额度）
     * @param totcredit 合计：（外汇交易-外汇信贷）+（同业存放-审批额度）+（贸易融资-审批额度）
     */
    public void setTotcredit(BigDecimal totcredit) {
        this.totcredit = totcredit;
    }

    
    public BigDecimal getErrflag() {
        return errflag;
    }

   
    public void setErrflag(BigDecimal errflag) {
        this.errflag = errflag;
    }

    /**
     * 查询日期
     * @return RPDATE 查询日期
     */
    public String getRpdate() {
        return rpdate;
    }

    /**
     * 查询日期
     * @param rpdate 查询日期
     */
    public void setRpdate(String rpdate) {
        this.rpdate = rpdate == null ? null : rpdate.trim();
    }
}