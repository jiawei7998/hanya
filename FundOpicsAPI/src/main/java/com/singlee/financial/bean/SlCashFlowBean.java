package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 现金流
 * 
 * @author shenzl
 * 
 */
public class SlCashFlowBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4288157012403029647L;
	
	// 债券代码
	private String secId;
	// 序列号
	private String seq;
	// 利息支付日期
	private String ipayDate;
	// 赎回价格
	private String callPrice_8;
	// 现金流数量
	private String cashFlow_8;
	// 利率
	private String intRate_8;
	// 除息日
	private String exdiVdate;
	// 利息金额
	private String intPayamt_8;
	// 息期结束日期
	private String intEnddte;
	// 息期开始日期
	private String intStrtdte;
	// 利率上限
	private String caprate_8;
	// 利率代码
	private String rateCode;
	// 利率固定日期
	private String rateFixDate;
	// 最后维护日期
	private String lstmntDate;
	// 本金
	private String prinamt_8;
	// 主要付款金额
	private String prinPayamt_8;
	// 赎回价格
	private String putPrice_8;
	// 基准代码
	private String basis;
	// 名义本金额
	private String notPrinamt_8;
	// 名义本金付款金额
	private String notPrinPayamt_8;
	// 名义利息付款金额
	private String notIntPayamt_8;
	// 更新次数
	private String updateCounter;
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getIpayDate() {
		return ipayDate;
	}
	public void setIpayDate(String ipayDate) {
		this.ipayDate = ipayDate;
	}
	public String getCallPrice_8() {
		return callPrice_8;
	}
	public void setCallPrice_8(String callPrice_8) {
		this.callPrice_8 = callPrice_8;
	}
	public String getCashFlow_8() {
		return cashFlow_8;
	}
	public void setCashFlow_8(String cashFlow_8) {
		this.cashFlow_8 = cashFlow_8;
	}
	public String getIntRate_8() {
		return intRate_8;
	}
	public void setIntRate_8(String intRate_8) {
		this.intRate_8 = intRate_8;
	}
	public String getExdiVdate() {
		return exdiVdate;
	}
	public void setExdiVdate(String exdiVdate) {
		this.exdiVdate = exdiVdate;
	}
	public String getIntPayamt_8() {
		return intPayamt_8;
	}
	public void setIntPayamt_8(String intPayamt_8) {
		this.intPayamt_8 = intPayamt_8;
	}
	public String getIntEnddte() {
		return intEnddte;
	}
	public void setIntEnddte(String intEnddte) {
		this.intEnddte = intEnddte;
	}
	public String getIntStrtdte() {
		return intStrtdte;
	}
	public void setIntStrtdte(String intStrtdte) {
		this.intStrtdte = intStrtdte;
	}
	public String getCaprate_8() {
		return caprate_8;
	}
	public void setCaprate_8(String caprate_8) {
		this.caprate_8 = caprate_8;
	}
	public String getRateCode() {
		return rateCode;
	}
	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}
	public String getRateFixDate() {
		return rateFixDate;
	}
	public void setRateFixDate(String rateFixDate) {
		this.rateFixDate = rateFixDate;
	}
	public String getLstmntDate() {
		return lstmntDate;
	}
	public void setLstmntDate(String lstmntDate) {
		this.lstmntDate = lstmntDate;
	}
	public String getPrinamt_8() {
		return prinamt_8;
	}
	public void setPrinamt_8(String prinamt_8) {
		this.prinamt_8 = prinamt_8;
	}
	public String getPrinPayamt_8() {
		return prinPayamt_8;
	}
	public void setPrinPayamt_8(String prinPayamt_8) {
		this.prinPayamt_8 = prinPayamt_8;
	}
	public String getPutPrice_8() {
		return putPrice_8;
	}
	public void setPutPrice_8(String putPrice_8) {
		this.putPrice_8 = putPrice_8;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getNotPrinamt_8() {
		return notPrinamt_8;
	}
	public void setNotPrinamt_8(String notPrinamt_8) {
		this.notPrinamt_8 = notPrinamt_8;
	}
	public String getNotPrinPayamt_8() {
		return notPrinPayamt_8;
	}
	public void setNotPrinPayamt_8(String notPrinPayamt_8) {
		this.notPrinPayamt_8 = notPrinPayamt_8;
	}
	public String getNotIntPayamt_8() {
		return notIntPayamt_8;
	}
	public void setNotIntPayamt_8(String notIntPayamt_8) {
		this.notIntPayamt_8 = notIntPayamt_8;
	}
	public String getUpdateCounter() {
		return updateCounter;
	}
	public void setUpdateCounter(String updateCounter) {
		this.updateCounter = updateCounter;
	}
}
