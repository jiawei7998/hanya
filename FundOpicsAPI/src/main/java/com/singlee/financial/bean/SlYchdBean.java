package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/***
 * 利率曲线
 * @author lij
 *
 */
public class SlYchdBean implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
     * 机构分支
     */
    private String br;

    /**
     * 币种
     */
    private String ccy;

    /**
     * 利率曲线
     */
    private String yieldcurve;
    
    /**
     * 
     */
    private String curvepurpose;

    /**
     * 英文描述
     */
    private String descr;

    /**
     * 类型
     */
    private String curvetype;

    /**
     * 
     */
    private String interpmethod;

    /**
     * 
     */
    private String spotdays;

    /**
     * 
     */
    private String payrule;

    /**
     * 
     */
    private String defaultflag;

    /**
     * 
     */
    private String quotetype;

    /**
     * 
     */
    private String gennightly;

    /**
     * 
     */
    private String importedcurve;

    /**
     * 
     */
    private String fincent;

    /**
     * 最后维护时间
     */
    private Date lstmntdte;

    /**
     *
     */
    private String riskfreecurve;

    

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getYieldcurve() {
        return yieldcurve;
    }

    public void setYieldcurve(String yieldcurve) {
        this.yieldcurve = yieldcurve == null ? null : yieldcurve.trim();
    }
    
    

    public String getCurvepurpose() {
        return curvepurpose;
    }

    public void setCurvepurpose(String curvepurpose) {
        this.curvepurpose = curvepurpose == null ? null : curvepurpose.trim();
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    public String getCurvetype() {
        return curvetype;
    }

    public void setCurvetype(String curvetype) {
        this.curvetype = curvetype == null ? null : curvetype.trim();
    }

    public String getInterpmethod() {
        return interpmethod;
    }

    public void setInterpmethod(String interpmethod) {
        this.interpmethod = interpmethod == null ? null : interpmethod.trim();
    }

    public String getSpotdays() {
        return spotdays;
    }

    public void setSpotdays(String spotdays) {
        this.spotdays = spotdays == null ? null : spotdays.trim();
    }

    public String getPayrule() {
        return payrule;
    }

    public void setPayrule(String payrule) {
        this.payrule = payrule == null ? null : payrule.trim();
    }

    public String getDefaultflag() {
        return defaultflag;
    }

    public void setDefaultflag(String defaultflag) {
        this.defaultflag = defaultflag == null ? null : defaultflag.trim();
    }

    public String getQuotetype() {
        return quotetype;
    }

    public void setQuotetype(String quotetype) {
        this.quotetype = quotetype == null ? null : quotetype.trim();
    }

    public String getGennightly() {
        return gennightly;
    }

    public void setGennightly(String gennightly) {
        this.gennightly = gennightly == null ? null : gennightly.trim();
    }

    public String getImportedcurve() {
        return importedcurve;
    }

    public void setImportedcurve(String importedcurve) {
        this.importedcurve = importedcurve == null ? null : importedcurve.trim();
    }

    public String getFincent() {
        return fincent;
    }

    public void setFincent(String fincent) {
        this.fincent = fincent == null ? null : fincent.trim();
    }

    public Date getLstmntdte() {
        return lstmntdte;
    }

    public void setLstmntdte(Date lstmntdte) {
        this.lstmntdte = lstmntdte;
    }

    public String getRiskfreecurve() {
        return riskfreecurve;
    }

    public void setRiskfreecurve(String riskfreecurve) {
        this.riskfreecurve = riskfreecurve == null ? null : riskfreecurve.trim();
    }

    

}
