package com.singlee.financial.bean;

import java.io.Serializable;

public class SlReportFund implements Serializable {
 
	private static final long serialVersionUID = 1242208695986978898L;
	
	//资金缺口预测
	private String mDate;
	private String weekList;
	private String weekTener;
	private String dealno;
	private String cmne;
	private String mDateAmt;
	private String secId;
	private String banlance;
	
	//资金预测明细
	private String product;
	private String prodType;
	private String vDate;
	private String rate;
	private String vDateAmt;
	private String cfn1;
	private String invType;
	private String qty;
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getWeekList() {
		return weekList;
	}
	public void setWeekList(String weekList) {
		this.weekList = weekList;
	}
	public String getWeekTener() {
		return weekTener;
	}
	public void setWeekTener(String weekTener) {
		this.weekTener = weekTener;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	public String getmDateAmt() {
		return mDateAmt;
	}
	public void setmDateAmt(String mDateAmt) {
		this.mDateAmt = mDateAmt;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getBanlance() {
		return banlance;
	}
	public void setBanlance(String banlance) {
		this.banlance = banlance;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getProdType() {
		return prodType;
	}
	public void setProdType(String prodType) {
		this.prodType = prodType;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getvDateAmt() {
		return vDateAmt;
	}
	public void setvDateAmt(String vDateAmt) {
		this.vDateAmt = vDateAmt;
	}
	public String getCfn1() {
		return cfn1;
	}
	public void setCfn1(String cfn1) {
		this.cfn1 = cfn1;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	
	
	
}
