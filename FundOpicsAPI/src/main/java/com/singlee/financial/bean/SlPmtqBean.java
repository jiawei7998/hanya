package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * opics收付款信息对象
 * 
 * @author shenzl
 * 
 */
public class SlPmtqBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6768933647634529540L;

	// 部门
	private String br;
	// 产品代码
	private String product;
	// 产品类型
	private String type;
	// seq
	private String seq;
	private String dealno;
	// 创建日期
	private Date cdate;
	// 创建时间
	private String ctime;
	// 收付款标识
	private String payrecind;
	// 交割时间
	private Date vdate;
	// 付款类型
	private String paytype;
	// 状态
	private String status;
	// 支付币种
	private String ccy;
	// 支付金额
	private String amount;
	// 交易对手编号
	private String cno;
	//
	private String msgprty;
	// 报文类型
	private String swiftfmt;
	// 实际日期
	private Date reldte;
	// 实际时间
	private String reltime;
	// 最后维护时间
	private Date lstmntdte;
	// 清算方式
	private String setmeans;
	// 清算账户
	private String setacct;
	private String comrefno;
	//发送标识，0：未发送，1：已发送
	private String sendflag;
	//1：新增，2：撤销
	private String dealstatus;
	//前置审批单号
	private String fedealno;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public Date getCdate() {
		return cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	public String getCtime() {
		return ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getPayrecind() {
		return payrecind;
	}

	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public String getPaytype() {
		return paytype;
	}

	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getMsgprty() {
		return msgprty;
	}

	public void setMsgprty(String msgprty) {
		this.msgprty = msgprty;
	}

	public String getSwiftfmt() {
		return swiftfmt;
	}

	public void setSwiftfmt(String swiftfmt) {
		this.swiftfmt = swiftfmt;
	}

	public Date getReldte() {
		return reldte;
	}

	public void setReldte(Date reldte) {
		this.reldte = reldte;
	}

	public String getReltime() {
		return reltime;
	}

	public void setReltime(String reltime) {
		this.reltime = reltime;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getSetmeans() {
		return setmeans;
	}

	public void setSetmeans(String setmeans) {
		this.setmeans = setmeans;
	}

	public String getSetacct() {
		return setacct;
	}

	public void setSetacct(String setacct) {
		this.setacct = setacct;
	}

	public String getComrefno() {
		return comrefno;
	}

	public void setComrefno(String comrefno) {
		this.comrefno = comrefno;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getSeq() {
		return seq;
	}

	public void setSendflag(String sendflag) {
		this.sendflag = sendflag;
	}

	public String getSendflag() {
		return sendflag;
	}

	public void setDealstatus(String dealstatus) {
		this.dealstatus = dealstatus;
	}

	public String getDealstatus() {
		return dealstatus;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
}
