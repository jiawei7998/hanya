package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;
/***
 * OPICS的债券的托管账户
 * @author lij
 *
 */
public class SlSaccBean implements Serializable{
	private String br;

    private String accountno;

    private static final long serialVersionUID = 1L;
    
    private String trad;

    private Date opendate;

    private String accttitle;

    private String cost;

    private String cno;

    private String discretind;

    private String glno;

    private Date lstmntdate;

    private Date laststmtdate;

    private Date nextstmtdate;

    private String nostrovostro;

    private String stmtcycle;

    private String stmtday;

    private String stmtdayrule;

    private String timelimit;

    private String port;

    private String shortposind;

    private String autosettind;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno == null ? null : accountno.trim();
    }
    
    public String getTrad() {
        return trad;
    }

    public void setTrad(String trad) {
        this.trad = trad == null ? null : trad.trim();
    }

    public Date getOpendate() {
        return opendate;
    }

    public void setOpendate(Date opendate) {
        this.opendate = opendate;
    }

    public String getAccttitle() {
        return accttitle;
    }

    public void setAccttitle(String accttitle) {
        this.accttitle = accttitle == null ? null : accttitle.trim();
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost == null ? null : cost.trim();
    }

    public String getCno() {
        return cno;
    }

    public void setCno(String cno) {
        this.cno = cno == null ? null : cno.trim();
    }

    public String getDiscretind() {
        return discretind;
    }

    public void setDiscretind(String discretind) {
        this.discretind = discretind == null ? null : discretind.trim();
    }

    public String getGlno() {
        return glno;
    }

    public void setGlno(String glno) {
        this.glno = glno == null ? null : glno.trim();
    }

    public Date getLstmntdate() {
        return lstmntdate;
    }

    public void setLstmntdate(Date lstmntdate) {
        this.lstmntdate = lstmntdate;
    }

    public Date getLaststmtdate() {
        return laststmtdate;
    }

    public void setLaststmtdate(Date laststmtdate) {
        this.laststmtdate = laststmtdate;
    }

    public Date getNextstmtdate() {
        return nextstmtdate;
    }

    public void setNextstmtdate(Date nextstmtdate) {
        this.nextstmtdate = nextstmtdate;
    }

    public String getNostrovostro() {
        return nostrovostro;
    }

    public void setNostrovostro(String nostrovostro) {
        this.nostrovostro = nostrovostro == null ? null : nostrovostro.trim();
    }

    public String getStmtcycle() {
        return stmtcycle;
    }

    public void setStmtcycle(String stmtcycle) {
        this.stmtcycle = stmtcycle == null ? null : stmtcycle.trim();
    }

    public String getStmtday() {
        return stmtday;
    }

    public void setStmtday(String stmtday) {
        this.stmtday = stmtday == null ? null : stmtday.trim();
    }

    public String getStmtdayrule() {
        return stmtdayrule;
    }

    public void setStmtdayrule(String stmtdayrule) {
        this.stmtdayrule = stmtdayrule == null ? null : stmtdayrule.trim();
    }

    public String getTimelimit() {
        return timelimit;
    }

    public void setTimelimit(String timelimit) {
        this.timelimit = timelimit == null ? null : timelimit.trim();
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port == null ? null : port.trim();
    }

    public String getShortposind() {
        return shortposind;
    }

    public void setShortposind(String shortposind) {
        this.shortposind = shortposind == null ? null : shortposind.trim();
    }

    public String getAutosettind() {
        return autosettind;
    }

    public void setAutosettind(String autosettind) {
        this.autosettind = autosettind == null ? null : autosettind.trim();
    }

}
