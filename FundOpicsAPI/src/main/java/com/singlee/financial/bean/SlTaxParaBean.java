package com.singlee.financial.bean;

import java.io.Serializable;

/**
   *  营改增
 * @author zhengfl
 *
 */
public class SlTaxParaBean implements Serializable {
	private static final long serialVersionUID = -9029694466053662263L;
	private String br;//部门
	private String product;//产品代码
	private String prodtype;//产品类型
	private String pdddescr;
	private String freetax;
	private String code;
	private String codedescr;
	private String qual;
	private String tradflag;
	private String istax;
	private String drcrind;
	private String taxitem;
	private String taxitemtype;
	private String incomeitem;
	private String salpur;
	private String invprtflag;
	private String incomeflag;
	private String taxsep;
	private String cidttype;
	private String taxidttype;
	private String discountflag;
	private String isuseflag;
	private String cmbbak1;
	private String cmbbak2;
	private String cmbbak3;
	private String cmbbak4;
	private String cmbbak5;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public String getPdddescr() {
		return pdddescr;
	}
	public void setPdddescr(String pdddescr) {
		this.pdddescr = pdddescr;
	}
	public String getFreetax() {
		return freetax;
	}
	public void setFreetax(String freetax) {
		this.freetax = freetax;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCodedescr() {
		return codedescr;
	}
	public void setCodedescr(String codedescr) {
		this.codedescr = codedescr;
	}
	public String getQual() {
		return qual;
	}
	public void setQual(String qual) {
		this.qual = qual;
	}
	public String getTradflag() {
		return tradflag;
	}
	public void setTradflag(String tradflag) {
		this.tradflag = tradflag;
	}
	public String getIstax() {
		return istax;
	}
	public void setIstax(String istax) {
		this.istax = istax;
	}
	public String getDrcrind() {
		return drcrind;
	}
	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}
	public String getTaxitem() {
		return taxitem;
	}
	public void setTaxitem(String taxitem) {
		this.taxitem = taxitem;
	}
	public String getTaxitemtype() {
		return taxitemtype;
	}
	public void setTaxitemtype(String taxitemtype) {
		this.taxitemtype = taxitemtype;
	}
	public String getIncomeitem() {
		return incomeitem;
	}
	public void setIncomeitem(String incomeitem) {
		this.incomeitem = incomeitem;
	}
	public String getSalpur() {
		return salpur;
	}
	public void setSalpur(String salpur) {
		this.salpur = salpur;
	}
	public String getInvprtflag() {
		return invprtflag;
	}
	public void setInvprtflag(String invprtflag) {
		this.invprtflag = invprtflag;
	}
	public String getIncomeflag() {
		return incomeflag;
	}
	public void setIncomeflag(String incomeflag) {
		this.incomeflag = incomeflag;
	}
	public String getTaxsep() {
		return taxsep;
	}
	public void setTaxsep(String taxsep) {
		this.taxsep = taxsep;
	}
	public String getCidttype() {
		return cidttype;
	}
	public void setCidttype(String cidttype) {
		this.cidttype = cidttype;
	}
	public String getTaxidttype() {
		return taxidttype;
	}
	public void setTaxidttype(String taxidttype) {
		this.taxidttype = taxidttype;
	}
	public String getDiscountflag() {
		return discountflag;
	}
	public void setDiscountflag(String discountflag) {
		this.discountflag = discountflag;
	}
	public String getIsuseflag() {
		return isuseflag;
	}
	public void setIsuseflag(String isuseflag) {
		this.isuseflag = isuseflag;
	}
	public String getCmbbak1() {
		return cmbbak1;
	}
	public void setCmbbak1(String cmbbak1) {
		this.cmbbak1 = cmbbak1;
	}
	public String getCmbbak2() {
		return cmbbak2;
	}
	public void setCmbbak2(String cmbbak2) {
		this.cmbbak2 = cmbbak2;
	}
	public String getCmbbak3() {
		return cmbbak3;
	}
	public void setCmbbak3(String cmbbak3) {
		this.cmbbak3 = cmbbak3;
	}
	public String getCmbbak4() {
		return cmbbak4;
	}
	public void setCmbbak4(String cmbbak4) {
		this.cmbbak4 = cmbbak4;
	}
	public String getCmbbak5() {
		return cmbbak5;
	}
	public void setCmbbak5(String cmbbak5) {
		this.cmbbak5 = cmbbak5;
	}
	
	
	
}