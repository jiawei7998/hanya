package com.singlee.financial.bean;

import java.io.Serializable;
public class OpicsBankChackBean implements Serializable{
	
	/**
	 * OPICS交易回查
	 */
	private static final long serialVersionUID = 1L;
	
	//交易单号
	private  String  dealNo;
	//交易员
	private  String  trad;
	//交易对手
	private  String  cno;
	//产品名称
	private  String  product;
	//交易方向
	private  String  ps;
	//成本中心
	private  String  Cost;
	//金额
	private  String  ccyamt;
	//交易录入日期
	private  String  brprcindte;
	//冲销日期
	private  String  revDate;
	//复合状态
	private  String  verind;
	//起息日
	private String vDate;
	//到期日
	private String mDate;
	
	
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public String getCost() {
		return Cost;
	}
	public void setCost(String cost) {
		Cost = cost;
	}
	public String getCcyamt() {
		return ccyamt;
	}
	public void setCcyamt(String ccyamt) {
		this.ccyamt = ccyamt;
	}
	public String getBrprcindte() {
		return brprcindte;
	}
	public void setBrprcindte(String brprcindte) {
		this.brprcindte = brprcindte;
	}
	public String getRevDate() {
		return revDate;
	}
	public void setRevDate(String revDate) {
		this.revDate = revDate;
	}
	public String getVerind() {
		return verind;
	}
	public void setVerind(String verind) {
		this.verind = verind;
	}
	
	
	

}
