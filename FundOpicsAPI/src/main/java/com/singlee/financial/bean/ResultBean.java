package com.singlee.financial.bean;


import java.io.Serializable;

public class ResultBean
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String br;
  private int impSeq;
  private int totalnum;
  private int succnum;
  private int faildnum;
  private int totalnum2;
  private String retCode;
  private String retMsg;
  
  public String getBr()
  {
    return this.br;
  }
  
  public void setBr(String br)
  {
    this.br = br;
  }
  
  public int getTotalnum()
  {
    return this.totalnum;
  }
  
  public void setTotalnum(int totalnum)
  {
    this.totalnum = totalnum;
  }
  
  public int getSuccnum()
  {
    return this.succnum;
  }
  
  public void setSuccnum(int succnum)
  {
    this.succnum = succnum;
  }
  
  public int getFaildnum()
  {
    return this.faildnum;
  }
  
  public void setFaildnum(int faildnum)
  {
    this.faildnum = faildnum;
  }
  
  public int getTotalnum2()
  {
    return this.totalnum2;
  }
  
  public void setTotalnum2(int totalnum2)
  {
    this.totalnum2 = totalnum2;
  }
  
  public int getImpSeq()
  {
    return this.impSeq;
  }
  
  public void setImpSeq(int impSeq)
  {
    this.impSeq = impSeq;
  }
  
  public String getRetCode()
  {
    return this.retCode;
  }
  
  public void setRetCode(String retCode)
  {
    this.retCode = retCode;
  }
  
  public String getRetMsg()
  {
    return this.retMsg;
  }
  
  public void setRetMsg(String retMsg)
  {
    this.retMsg = retMsg;
  }
}

