package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class SlRprhDldtBean implements Serializable{

	/**
	 * 资金往来类业务校验（首期结算金额，到期结算金额）,查询SPSH,DLDT,PMTQ表
	 */
	private static final long serialVersionUID = -2812632628298187412L;
	private String	dealno;   
	private String	dealdate;    
	private String	product;
	private String	prodtype;
	//首期结算金额
	private BigDecimal	comamt;
	//到期结算金额
	private BigDecimal	matamt;
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getDealdate() {
		return dealdate;
	}
	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public BigDecimal getComamt() {
		return comamt;
	}
	public void setComamt(BigDecimal comamt) {
		this.comamt = comamt;
	}
	public void setMatamt(BigDecimal matamt) {
		this.matamt = matamt;
	}
	public BigDecimal getMatamt() {
		return matamt;
	}
	

}
