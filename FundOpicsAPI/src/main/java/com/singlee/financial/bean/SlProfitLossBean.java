package com.singlee.financial.bean;


import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 损益监测
 * 
 */
public class SlProfitLossBean implements Serializable {
     /**
      * 
      */
     private static final long serialVersionUID = -4462901607664924870L;
     
     private BigDecimal tInterestIncomeSd;//交易账户利息收入（债券）
     private BigDecimal tSpreadIncomeSd;//交易账户价差收入（债券）
     private BigDecimal tFairValueSd;//交易账户公允价值变动（债券）
     private BigDecimal tInterestIncomeCd;//交易账户利息收入（同业存单）
     private BigDecimal tSpreadIncomeCd;//交易账户价差收入（同业存单）
     private BigDecimal tFairValueCd;//交易账户公允价值变动（同业存单）
     private BigDecimal tInvestmentIncomeOther;//交易账户投资收益（其他）
     private BigDecimal tFairValueOther;//交易账户公允价值变动（其他）
     private BigDecimal aInterestIncomeSd;//可供出售账户利息收入（债券）
     private BigDecimal aSpreadIncomeSd;//可供出售账户价差收入（债券）
     private BigDecimal aFairValueSd;//可供出售账户公允价值变动（债券）
     private BigDecimal aInterestIncomeCd;//可供出售账户利息收入（同业存单）
     private BigDecimal aSpreadIncomeCd;//可供出售账户价差收入（同业存单）
     private BigDecimal aFairValueCd;//可供出售账户公允价值变动（同业存单）
     private BigDecimal fxProfitLoss;//外汇损益
     private String inputtime;//
     private String postdate;//
     private BigDecimal tProfitLossSd;//债券交易账户损益
     private BigDecimal tProfitLossCd;//存单交易账户损益
     private BigDecimal aProfitLossSd;//债券可供出售账户损益
     private BigDecimal aProfitLossCd;//存单可供出售账户损益


     public BigDecimal gettInterestIncomeSd() {
          return tInterestIncomeSd;
     }
     public void settInterestIncomeSd(BigDecimal tInterestIncomeSd) {
          this.tInterestIncomeSd = tInterestIncomeSd;
     }
     public BigDecimal gettSpreadIncomeSd() {
          return tSpreadIncomeSd;
     }
     public void settSpreadIncomeSd(BigDecimal tSpreadIncomeSd) {
          this.tSpreadIncomeSd = tSpreadIncomeSd;
     }
     public BigDecimal gettFairValueSd() {
          return tFairValueSd;
     }
     public void settFairValueSd(BigDecimal tFairValueSd) {
          this.tFairValueSd = tFairValueSd;
     }
     public BigDecimal gettInterestIncomeCd() {
          return tInterestIncomeCd;
     }
     public void settInterestIncomeCd(BigDecimal tInterestIncomeCd) {
          this.tInterestIncomeCd = tInterestIncomeCd;
     }
     public BigDecimal gettSpreadIncomeCd() {
          return tSpreadIncomeCd;
     }
     public void settSpreadIncomeCd(BigDecimal tSpreadIncomeCd) {
          this.tSpreadIncomeCd = tSpreadIncomeCd;
     }
     public BigDecimal gettFairValueCd() {
          return tFairValueCd;
     }
     public void settFairValueCd(BigDecimal tFairValueCd) {
          this.tFairValueCd = tFairValueCd;
     }
     public BigDecimal gettInvestmentIncomeOther() {
          return tInvestmentIncomeOther;
     }
     public void settInvestmentIncomeOther(BigDecimal tInvestmentIncomeOther) {
          this.tInvestmentIncomeOther = tInvestmentIncomeOther;
     }
     public BigDecimal gettFairValueOther() {
          return tFairValueOther;
     }
     public void settFairValueOther(BigDecimal tFairValueOther) {
          this.tFairValueOther = tFairValueOther;
     }
     public BigDecimal getaInterestIncomeSd() {
          return aInterestIncomeSd;
     }
     public void setaInterestIncomeSd(BigDecimal aInterestIncomeSd) {
          this.aInterestIncomeSd = aInterestIncomeSd;
     }
     public BigDecimal getaSpreadIncomeSd() {
          return aSpreadIncomeSd;
     }
     public void setaSpreadIncomeSd(BigDecimal aSpreadIncomeSd) {
          this.aSpreadIncomeSd = aSpreadIncomeSd;
     }
     public BigDecimal getaFairValueSd() {
          return aFairValueSd;
     }
     public void setaFairValueSd(BigDecimal aFairValueSd) {
          this.aFairValueSd = aFairValueSd;
     }
     public BigDecimal getaInterestIncomeCd() {
          return aInterestIncomeCd;
     }
     public void setaInterestIncomeCd(BigDecimal aInterestIncomeCd) {
          this.aInterestIncomeCd = aInterestIncomeCd;
     }
     public BigDecimal getaSpreadIncomeCd() {
          return aSpreadIncomeCd;
     }
     public void setaSpreadIncomeCd(BigDecimal aSpreadIncomeCd) {
          this.aSpreadIncomeCd = aSpreadIncomeCd;
     }
     public BigDecimal getaFairValueCd() {
          return aFairValueCd;
     }
     public void setaFairValueCd(BigDecimal aFairValueCd) {
          this.aFairValueCd = aFairValueCd;
     }
     public BigDecimal getFxProfitLoss() {
          return fxProfitLoss;
     }
     public void setFxProfitLoss(BigDecimal fxProfitLoss) {
          this.fxProfitLoss = fxProfitLoss;
     }
     public String getInputtime() {
          return inputtime;
     }
     public void setInputtime(String inputtime) {
          this.inputtime = inputtime;
     }
     public String getPostdate() {
          return postdate;
     }
     public void setPostdate(String postdate) {
          this.postdate = postdate;
     }
     public void settProfitLossSd(BigDecimal tProfitLossSd) {
          this.tProfitLossSd = tProfitLossSd;
     }
     public BigDecimal gettProfitLossSd() {
          return tProfitLossSd;
     }
     public void settProfitLossCd(BigDecimal tProfitLossCd) {
          this.tProfitLossCd = tProfitLossCd;
     }
     public BigDecimal gettProfitLossCd() {
          return tProfitLossCd;
     }
     public void setaProfitLossSd(BigDecimal aProfitLossSd) {
          this.aProfitLossSd = aProfitLossSd;
     }
     public BigDecimal getaProfitLossSd() {
          return aProfitLossSd;
     }
     public void setaProfitLossCd(BigDecimal aProfitLossCd) {
          this.aProfitLossCd = aProfitLossCd;
     }
     public BigDecimal getaProfitLossCd() {
          return aProfitLossCd;
     }
     


}