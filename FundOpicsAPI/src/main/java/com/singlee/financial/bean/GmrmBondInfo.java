package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 债券信息
 * @author LIJ
 *
 */
public class GmrmBondInfo implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 5254102250149945083L;

	/**
     * 数据日期
     */
    private String datadate;

    /**
     * 债券序号，唯一标识一笔债券的编号
     */
    private String secId;

    /**
     * 债券类型
     */
    private String secType;

    /**
     * 债券简称
     */
    private String shortName;

    /**
     * 债券描述
     */
    private String secDesc;

    /**
     * 发行体
     */
    private String issuer;

    /**
     * 发行体类型
     */
    private String issuertype;

    /**
     * 中债登代码
     */
    private String zzdSecCode;

    /**
     * ISIN号
     */
    private String isin;

    /**
     * CUSIP号
     */
    private String cusip;

    /**
     * 债券币种
     */
    private String ccy;

    /**
     * 计息类型，1-固定，2-浮动，3-零息，05-其他
     */
    private String couponType;

    /**
     * 日期规则，0-否，1-是（月末规则）
     */
    private String dayRule;

    /**
     * 起息日，YYYYMMDD
     */
    private String interestAccrueDate;

    /**
     * 到期日，YYYYMMDD
     */
    private String maturityDate;

    /**
     * 交割惯例，取值为0,1等，含义为T+0，T+1等
     */
    private String settlementConvention;

    /**
     * 首次付息日，YYYYMMDD
     */
    private String firstCouponPaymentDate;

    /**
     * 付息频率
     */
    private String couponFreq;

    /**
     * 利率，浮动债券可以为空，需送原值，类似0.13
     */
    private BigDecimal couponRate;

    /**
     * 浮动利率基准代码，人民币债券不能为空，亚洲提供原值
     */
    private String floatCode;

    /**
     * 浮动利率基准，人民币债券不能为空，亚洲提供原值
     */
    private String floatIndex;

    /**
     * 浮动利率标准期限，人民币债券不能为空，亚洲提供原值
     */
    private String floatIndexTenor;

    /**
     * 浮动基点差，浮动债券不可以为空，需送原值(类似0.003这样的数值)
     */
    private BigDecimal floatSpread;

    /**
     * 浮动利率重置频率
     */
    private String resetFrequency;

    /**
     * 天数计算惯例
     */
    private String dayCountBasis;

    /**
     * 工作日惯例
     */
    private String payRollConvention;

    /**
     * MIC码
     */
    private String mic;

    /**
     * 参考利率天数
     */
    private String indexAverageDays;

    /**
     * 均值类型，亚洲请提供1，算数平均
     */
    private String averageType;

    /**
     * 债券价格，债券的前台估值价
     */
    private BigDecimal bondPrice;

    /**
     * 数据日期
     * @return DATADATE 数据日期
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期
     * @param datadate 数据日期
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 债券序号，唯一标识一笔债券的编号
     * @return SEC_ID 债券序号，唯一标识一笔债券的编号
     */
    public String getSecId() {
        return secId;
    }

    /**
     * 债券序号，唯一标识一笔债券的编号
     * @param secId 债券序号，唯一标识一笔债券的编号
     */
    public void setSecId(String secId) {
        this.secId = secId == null ? null : secId.trim();
    }

    /**
     * 债券类型
     * @return SEC_TYPE 债券类型
     */
    public String getSecType() {
        return secType;
    }

    /**
     * 债券类型
     * @param secType 债券类型
     */
    public void setSecType(String secType) {
        this.secType = secType == null ? null : secType.trim();
    }

    /**
     * 债券简称
     * @return SHORT_NAME 债券简称
     */
    public String getShortName() {
        return shortName;
    }

    /**
     * 债券简称
     * @param shortName 债券简称
     */
    public void setShortName(String shortName) {
        this.shortName = shortName == null ? null : shortName.trim();
    }

    /**
     * 债券描述
     * @return SEC_DESC 债券描述
     */
    public String getSecDesc() {
        return secDesc;
    }

    /**
     * 债券描述
     * @param secDesc 债券描述
     */
    public void setSecDesc(String secDesc) {
        this.secDesc = secDesc == null ? null : secDesc.trim();
    }

    /**
     * 发行体
     * @return ISSUER 发行体
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * 发行体
     * @param issuer 发行体
     */
    public void setIssuer(String issuer) {
        this.issuer = issuer == null ? null : issuer.trim();
    }

    /**
     * 发行体类型
     * @return ISSUERTYPE 发行体类型
     */
    public String getIssuertype() {
        return issuertype;
    }

    /**
     * 发行体类型
     * @param issuertype 发行体类型
     */
    public void setIssuertype(String issuertype) {
        this.issuertype = issuertype == null ? null : issuertype.trim();
    }

    /**
     * 中债登代码
     * @return ZZD_SEC_CODE 中债登代码
     */
    public String getZzdSecCode() {
        return zzdSecCode;
    }

    /**
     * 中债登代码
     * @param zzdSecCode 中债登代码
     */
    public void setZzdSecCode(String zzdSecCode) {
        this.zzdSecCode = zzdSecCode == null ? null : zzdSecCode.trim();
    }

    /**
     * ISIN号
     * @return ISIN ISIN号
     */
    public String getIsin() {
        return isin;
    }

    /**
     * ISIN号
     * @param isin ISIN号
     */
    public void setIsin(String isin) {
        this.isin = isin == null ? null : isin.trim();
    }

    /**
     * CUSIP号
     * @return CUSIP CUSIP号
     */
    public String getCusip() {
        return cusip;
    }

    /**
     * CUSIP号
     * @param cusip CUSIP号
     */
    public void setCusip(String cusip) {
        this.cusip = cusip == null ? null : cusip.trim();
    }

    /**
     * 债券币种
     * @return CCY 债券币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 债券币种
     * @param ccy 债券币种
     */
    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    /**
     * 计息类型，1-固定，2-浮动，3-零息，05-其他
     * @return COUPON_TYPE 计息类型，1-固定，2-浮动，3-零息，05-其他
     */
    public String getCouponType() {
        return couponType;
    }

    /**
     * 计息类型，1-固定，2-浮动，3-零息，05-其他
     * @param couponType 计息类型，1-固定，2-浮动，3-零息，05-其他
     */
    public void setCouponType(String couponType) {
        this.couponType = couponType == null ? null : couponType.trim();
    }

    /**
     * 日期规则，0-否，1-是（月末规则）
     * @return DAY_RULE 日期规则，0-否，1-是（月末规则）
     */
    public String getDayRule() {
        return dayRule;
    }

    /**
     * 日期规则，0-否，1-是（月末规则）
     * @param dayRule 日期规则，0-否，1-是（月末规则）
     */
    public void setDayRule(String dayRule) {
        this.dayRule = dayRule == null ? null : dayRule.trim();
    }

    /**
     * 起息日，YYYYMMDD
     * @return INTEREST_ACCRUE_DATE 起息日，YYYYMMDD
     */
    public String getInterestAccrueDate() {
        return interestAccrueDate;
    }

    /**
     * 起息日，YYYYMMDD
     * @param interestAccrueDate 起息日，YYYYMMDD
     */
    public void setInterestAccrueDate(String interestAccrueDate) {
        this.interestAccrueDate = interestAccrueDate == null ? null : interestAccrueDate.trim();
    }

    /**
     * 到期日，YYYYMMDD
     * @return MATURITY_DATE 到期日，YYYYMMDD
     */
    public String getMaturityDate() {
        return maturityDate;
    }

    /**
     * 到期日，YYYYMMDD
     * @param maturityDate 到期日，YYYYMMDD
     */
    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate == null ? null : maturityDate.trim();
    }

    /**
     * 交割惯例，取值为0,1等，含义为T+0，T+1等
     * @return SETTLEMENT_CONVENTION 交割惯例，取值为0,1等，含义为T+0，T+1等
     */
    public String getSettlementConvention() {
        return settlementConvention;
    }

    /**
     * 交割惯例，取值为0,1等，含义为T+0，T+1等
     * @param settlementConvention 交割惯例，取值为0,1等，含义为T+0，T+1等
     */
    public void setSettlementConvention(String settlementConvention) {
        this.settlementConvention = settlementConvention == null ? null : settlementConvention.trim();
    }

    /**
     * 首次付息日，YYYYMMDD
     * @return FIRST_COUPON_PAYMENT_DATE 首次付息日，YYYYMMDD
     */
    public String getFirstCouponPaymentDate() {
        return firstCouponPaymentDate;
    }

    /**
     * 首次付息日，YYYYMMDD
     * @param firstCouponPaymentDate 首次付息日，YYYYMMDD
     */
    public void setFirstCouponPaymentDate(String firstCouponPaymentDate) {
        this.firstCouponPaymentDate = firstCouponPaymentDate == null ? null : firstCouponPaymentDate.trim();
    }

    /**
     * 付息频率
     * @return COUPON_FREQ 付息频率
     */
    public String getCouponFreq() {
        return couponFreq;
    }

    /**
     * 付息频率
     * @param couponFreq 付息频率
     */
    public void setCouponFreq(String couponFreq) {
        this.couponFreq = couponFreq == null ? null : couponFreq.trim();
    }

    /**
     * 利率，浮动债券可以为空，需送原值，类似0.13
     * @return COUPON_RATE 利率，浮动债券可以为空，需送原值，类似0.13
     */
    public BigDecimal getCouponRate() {
        return couponRate;
    }

    /**
     * 利率，浮动债券可以为空，需送原值，类似0.13
     * @param couponRate 利率，浮动债券可以为空，需送原值，类似0.13
     */
    public void setCouponRate(BigDecimal couponRate) {
        this.couponRate = couponRate;
    }

    /**
     * 浮动利率基准代码，人民币债券不能为空，亚洲提供原值
     * @return FLOAT_CODE 浮动利率基准代码，人民币债券不能为空，亚洲提供原值
     */
    public String getFloatCode() {
        return floatCode;
    }

    /**
     * 浮动利率基准代码，人民币债券不能为空，亚洲提供原值
     * @param floatCode 浮动利率基准代码，人民币债券不能为空，亚洲提供原值
     */
    public void setFloatCode(String floatCode) {
        this.floatCode = floatCode == null ? null : floatCode.trim();
    }

    /**
     * 浮动利率基准，人民币债券不能为空，亚洲提供原值
     * @return FLOAT_INDEX 浮动利率基准，人民币债券不能为空，亚洲提供原值
     */
    public String getFloatIndex() {
        return floatIndex;
    }

    /**
     * 浮动利率基准，人民币债券不能为空，亚洲提供原值
     * @param floatIndex 浮动利率基准，人民币债券不能为空，亚洲提供原值
     */
    public void setFloatIndex(String floatIndex) {
        this.floatIndex = floatIndex == null ? null : floatIndex.trim();
    }

    /**
     * 浮动利率标准期限，人民币债券不能为空，亚洲提供原值
     * @return FLOAT_INDEX_TENOR 浮动利率标准期限，人民币债券不能为空，亚洲提供原值
     */
    public String getFloatIndexTenor() {
        return floatIndexTenor;
    }

    /**
     * 浮动利率标准期限，人民币债券不能为空，亚洲提供原值
     * @param floatIndexTenor 浮动利率标准期限，人民币债券不能为空，亚洲提供原值
     */
    public void setFloatIndexTenor(String floatIndexTenor) {
        this.floatIndexTenor = floatIndexTenor == null ? null : floatIndexTenor.trim();
    }

    /**
     * 浮动基点差，浮动债券不可以为空，需送原值(类似0.003这样的数值)
     * @return FLOAT_SPREAD 浮动基点差，浮动债券不可以为空，需送原值(类似0.003这样的数值)
     */
    public BigDecimal getFloatSpread() {
        return floatSpread;
    }

    /**
     * 浮动基点差，浮动债券不可以为空，需送原值(类似0.003这样的数值)
     * @param floatSpread 浮动基点差，浮动债券不可以为空，需送原值(类似0.003这样的数值)
     */
    public void setFloatSpread(BigDecimal floatSpread) {
        this.floatSpread = floatSpread;
    }

    /**
     * 浮动利率重置频率
     * @return RESET_FREQUENCY 浮动利率重置频率
     */
    public String getResetFrequency() {
        return resetFrequency;
    }

    /**
     * 浮动利率重置频率
     * @param resetFrequency 浮动利率重置频率
     */
    public void setResetFrequency(String resetFrequency) {
        this.resetFrequency = resetFrequency == null ? null : resetFrequency.trim();
    }

    /**
     * 天数计算惯例
     * @return DAY_COUNT_BASIS 天数计算惯例
     */
    public String getDayCountBasis() {
        return dayCountBasis;
    }

    /**
     * 天数计算惯例
     * @param dayCountBasis 天数计算惯例
     */
    public void setDayCountBasis(String dayCountBasis) {
        this.dayCountBasis = dayCountBasis == null ? null : dayCountBasis.trim();
    }

    /**
     * 工作日惯例
     * @return PAY_ROLL_CONVENTION 工作日惯例
     */
    public String getPayRollConvention() {
        return payRollConvention;
    }

    /**
     * 工作日惯例
     * @param payRollConvention 工作日惯例
     */
    public void setPayRollConvention(String payRollConvention) {
        this.payRollConvention = payRollConvention == null ? null : payRollConvention.trim();
    }

    /**
     * MIC码
     * @return MIC MIC码
     */
    public String getMic() {
        return mic;
    }

    /**
     * MIC码
     * @param mic MIC码
     */
    public void setMic(String mic) {
        this.mic = mic == null ? null : mic.trim();
    }

    /**
     * 参考利率天数
     * @return INDEX_AVERAGE_DAYS 参考利率天数
     */
    public String getIndexAverageDays() {
        return indexAverageDays;
    }

    /**
     * 参考利率天数
     * @param indexAverageDays 参考利率天数
     */
    public void setIndexAverageDays(String indexAverageDays) {
        this.indexAverageDays = indexAverageDays == null ? null : indexAverageDays.trim();
    }

    /**
     * 均值类型，亚洲请提供1，算数平均
     * @return AVERAGE_TYPE 均值类型，亚洲请提供1，算数平均
     */
    public String getAverageType() {
        return averageType;
    }

    /**
     * 均值类型，亚洲请提供1，算数平均
     * @param averageType 均值类型，亚洲请提供1，算数平均
     */
    public void setAverageType(String averageType) {
        this.averageType = averageType == null ? null : averageType.trim();
    }

    /**
     * 债券价格，债券的前台估值价
     * @return BOND_PRICE 债券价格，债券的前台估值价
     */
    public BigDecimal getBondPrice() {
        return bondPrice;
    }

    /**
     * 债券价格，债券的前台估值价
     * @param bondPrice 债券价格，债券的前台估值价
     */
    public void setBondPrice(BigDecimal bondPrice) {
        this.bondPrice = bondPrice;
    }
}