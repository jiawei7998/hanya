package com.singlee.financial.bean;

import java.io.Serializable;

public class ImportOpicsResultBean
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String br;
  private String retCode;
  private String retMsg;
  private int totalNum;
  private int succNum;
  private int faildNum;
  
  public String getBr()
  {
    return this.br;
  }
  
  public void setBr(String br)
  {
    this.br = br;
  }
  
  public String getRetCode()
  {
    return this.retCode;
  }
  
  public void setRetCode(String retCode)
  {
    this.retCode = retCode;
  }
  
  public String getRetMsg()
  {
    return this.retMsg;
  }
  
  public void setRetMsg(String retMsg)
  {
    this.retMsg = retMsg;
  }
  
  public int getTotalNum()
  {
    return this.totalNum;
  }
  
  public void setTotalNum(int totalNum)
  {
    this.totalNum = totalNum;
  }
  
  public int getSuccNum()
  {
    return this.succNum;
  }
  
  public void setSuccNum(int succNum)
  {
    this.succNum = succNum;
  }
  
  public int getFaildNum()
  {
    return this.faildNum;
  }
  
  public void setFaildNum(int faildNum)
  {
    this.faildNum = faildNum;
  }
}
