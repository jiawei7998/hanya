package com.singlee.financial.bean;

import java.math.BigDecimal;
import java.util.Date;

import com.singlee.financial.pojo.SwapCrsBean;

public class SlSwapCrsBean extends SwapCrsBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2612068452902328208L;
	/**
	 * INTH 头表信息
	 */
	private SlInthBean inthBean;
	/**
	 * OPICS拓展属性
	 */
	private SlExternalBean externalBean;
	/**
	 * 利润/亏损
	 */
	//private BigDecimal plmethod;
	/***
	 * 交易方式  T:Trading
	 *       H:Hedge套期保值
	 */
	private String plmethod;
	
	/**
	 * 
	 */
	private String setoff;
	/**
	 * 清算日期
	 */
	private Date settleDate;
	/**
	 * SWAP类型（固定利率支付方/浮动利率收取方）
	 */
	private String swapType;
	/**
	 * 利率代码
	 */
	private String rateCode;
	/**
	 * 浮动利率
	 */
	private String spread;
	/**
	 * 交易类型
	 */
	private String dealType;
	/**
	 * 期限
	 */
	private String tenor;
	/**
	 * 支付方计息基准
	 */
	private String payBasis;
	/**
	 * 收取方计息基准
	 */
	private String recBasis;
	/**
	 * 支付方日期方向
	 */
	private String payDateDir;
	/**
	 * 收取方日期方向
	 */
	private String recDateDir;
	/**
	 * 支付方利率支付周期
	 */
	private String payIntpaycycle;
	/**
	 * 收取方利率支付周期
	 */
	private String recIntpaycycle;
	/**
	 * 支付方利率支付规则
	 */
	private String payIntpayRule;
	/**
	 * 收取方利率支付规则
	 */
	private String recIntpayRule;
	/**
	 * 支付方利率
	 */
	private BigDecimal payIntRate;
	/**
	 * 收取方利率
	 */
	private BigDecimal recIntRate;
	/**
	 * 支付方名义币种
	 */
	private String payNotCcy;
	/**
	 * 收取方名义币种
	 */
	private String recNotCcy;
	/**
	 * 支付方名义金额
	 */
	private BigDecimal payNotCcyAmt;
	/**
	 * 收取方名义金额
	 */
	private BigDecimal recNotCcyAmt;
	/**
	 * 支付方名义本金重置金额
	 */
	private BigDecimal payPrinadjAmt;
	/**
	 * 收取方名义本金重置金额
	 */
	private BigDecimal recPrinadjAmt;
	/**
	 * 支付方利率曲线
	 */
	private String payYieldcurve;
	/**
	 * 收取方利率曲线
	 */
	private String recYieldcurve;
	/**
	 * 收取方利率代码
	 */
	private String recRateCode;
	/**
	 * 收取方利差
	 */
	private BigDecimal recSpread;
	/**
	 * 添加指令标志
	 */
	private String addsiind;
	/**
	 * 轧差支付标志
	 */
	private String netpayind;
	/**
	 * 策略号
	 */
	private String stratid;
	/**
	 * 支付方国有标志
	 */
	private String paypostnotional;
	/**
	 * 收取方国有标志
	 */
	private String recpostnotional;
	/**
	 * 投资类型
	 */
	private String invtype;
	/**
	 * 支付方首期支付日
	 */
	private Date payfirstpaydate;
	/**
	 * 收取方首期支付日
	 */
	private Date recfirstpaydate;
	/**
	 * 利率修正周期
	 */
	private String raterevcycle;
	/**
	 * 收取方利率修正周期
	 */
	private String recraterevcycle;
	/**
	 * 首次/最后利率修正标志
	 */
	private String raterevfrstlst;
	/**
	 * 收取方首次/最后利率修正标志
	 */
	private String recraterevfrstlst;
	/**
	 * 浮动利率计算规则
	 */
	private String calcrule;
	/**
	 * 收取方浮动利率计算规则
	 */
	private String reccalcrule;

	private String dealind;

	/**
	 * 到期日
	 */
	private String maturityDate;

	/**
	 * 货币互换初始化
	 * 
	 * @param br
	 *            业务部门分支
	 * @param tag
	 *            标识
	 * @param detail
	 *            细节
	 * @note
	 * 
	 *       1、交易导入 TAG="SWAP" , DETAIL="ISWH"
	 * 
	 *       2、交易冲销 TAG="SWDP", DETAIL="ISWH"
	 * 
	 */
	public SlSwapCrsBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.SWAP.SEQ);
			inthBean.setInoutind(SlDealModule.SWAP.INOUTIND);
			inthBean.setPriority(SlDealModule.SWAP.PRIORITY);
			inthBean.setStatcode(SlDealModule.SWAP.STATCODE);
			inthBean.setSyst(SlDealModule.SWAP.SYST);
			inthBean.setIoper(SlDealModule.SWAP.IOPER);
		}
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public SlExternalBean getExternalBean() {
		return externalBean;
	}

	public void setExternalBean(SlExternalBean externalBean) {
		this.externalBean = externalBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	public String getPlmethod() {
		return plmethod;
	}

	public void setPlmethod(String plmethod) {
		this.plmethod = plmethod;
	}

	public Date getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(Date settleDate) {
		this.settleDate = settleDate;
	}

	public String getSwapType() {
		return swapType;
	}

	public void setSwapType(String swapType) {
		this.swapType = swapType;
	}

	public String getRateCode() {
		return rateCode;
	}

	public void setRateCode(String rateCode) {
		this.rateCode = rateCode;
	}

	public String getSpread() {
		return spread;
	}

	public void setSpread(String spread) {
		this.spread = spread;
	}

	public String getDealType() {
		return dealType;
	}

	public void setDealType(String dealType) {
		this.dealType = dealType;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getPayBasis() {
		return payBasis;
	}

	public void setPayBasis(String payBasis) {
		this.payBasis = payBasis;
	}

	public String getRecBasis() {
		return recBasis;
	}

	public void setRecBasis(String recBasis) {
		this.recBasis = recBasis;
	}

	public String getPayDateDir() {
		return payDateDir;
	}

	public void setPayDateDir(String payDateDir) {
		this.payDateDir = payDateDir;
	}

	public String getRecDateDir() {
		return recDateDir;
	}

	public void setRecDateDir(String recDateDir) {
		this.recDateDir = recDateDir;
	}

	public String getPayIntpaycycle() {
		return payIntpaycycle;
	}

	public void setPayIntpaycycle(String payIntpaycycle) {
		this.payIntpaycycle = payIntpaycycle;
	}

	public String getRecIntpaycycle() {
		return recIntpaycycle;
	}

	public void setRecIntpaycycle(String recIntpaycycle) {
		this.recIntpaycycle = recIntpaycycle;
	}

	public String getPayIntpayRule() {
		return payIntpayRule;
	}

	public void setPayIntpayRule(String payIntpayRule) {
		this.payIntpayRule = payIntpayRule;
	}

	public String getRecIntpayRule() {
		return recIntpayRule;
	}

	public void setRecIntpayRule(String recIntpayRule) {
		this.recIntpayRule = recIntpayRule;
	}

	public BigDecimal getPayIntRate() {
		return payIntRate;
	}

	public void setPayIntRate(BigDecimal payIntRate) {
		this.payIntRate = payIntRate;
	}

	public BigDecimal getRecIntRate() {
		return recIntRate;
	}

	public void setRecIntRate(BigDecimal recIntRate) {
		this.recIntRate = recIntRate;
	}

	public String getPayNotCcy() {
		return payNotCcy;
	}

	public void setPayNotCcy(String payNotCcy) {
		this.payNotCcy = payNotCcy;
	}

	public String getRecNotCcy() {
		return recNotCcy;
	}

	public void setRecNotCcy(String recNotCcy) {
		this.recNotCcy = recNotCcy;
	}

	public BigDecimal getPayNotCcyAmt() {
		return payNotCcyAmt;
	}

	public void setPayNotCcyAmt(BigDecimal payNotCcyAmt) {
		this.payNotCcyAmt = payNotCcyAmt;
	}

	public BigDecimal getRecNotCcyAmt() {
		return recNotCcyAmt;
	}

	public void setRecNotCcyAmt(BigDecimal recNotCcyAmt) {
		this.recNotCcyAmt = recNotCcyAmt;
	}

	public BigDecimal getPayPrinadjAmt() {
		return payPrinadjAmt;
	}

	public void setPayPrinadjAmt(BigDecimal payPrinadjAmt) {
		this.payPrinadjAmt = payPrinadjAmt;
	}

	public BigDecimal getRecPrinadjAmt() {
		return recPrinadjAmt;
	}

	public void setRecPrinadjAmt(BigDecimal recPrinadjAmt) {
		this.recPrinadjAmt = recPrinadjAmt;
	}

	public String getPayYieldcurve() {
		return payYieldcurve;
	}

	public void setPayYieldcurve(String payYieldcurve) {
		this.payYieldcurve = payYieldcurve;
	}

	public String getRecYieldcurve() {
		return recYieldcurve;
	}

	public void setRecYieldcurve(String recYieldcurve) {
		this.recYieldcurve = recYieldcurve;
	}

	public String getRecRateCode() {
		return recRateCode;
	}

	public void setRecRateCode(String recRateCode) {
		this.recRateCode = recRateCode;
	}

	public BigDecimal getRecSpread() {
		return recSpread;
	}

	public void setRecSpread(BigDecimal recSpread) {
		this.recSpread = recSpread;
	}

	public String getAddsiind() {
		return addsiind;
	}

	public void setAddsiind(String addsiind) {
		this.addsiind = addsiind;
	}

	public String getNetpayind() {
		return netpayind;
	}

	public void setNetpayind(String netpayind) {
		this.netpayind = netpayind;
	}

	public String getStratid() {
		return stratid;
	}

	public void setStratid(String stratid) {
		this.stratid = stratid;
	}

	public String getPaypostnotional() {
		return paypostnotional;
	}

	public void setPaypostnotional(String paypostnotional) {
		this.paypostnotional = paypostnotional;
	}

	public String getRecpostnotional() {
		return recpostnotional;
	}

	public void setRecpostnotional(String recpostnotional) {
		this.recpostnotional = recpostnotional;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public Date getPayfirstpaydate() {
		return payfirstpaydate;
	}

	public void setPayfirstpaydate(Date payfirstpaydate) {
		this.payfirstpaydate = payfirstpaydate;
	}

	public Date getRecfirstpaydate() {
		return recfirstpaydate;
	}

	public void setRecfirstpaydate(Date recfirstpaydate) {
		this.recfirstpaydate = recfirstpaydate;
	}

	public String getRaterevcycle() {
		return raterevcycle;
	}

	public void setRaterevcycle(String raterevcycle) {
		this.raterevcycle = raterevcycle;
	}

	public String getRecraterevcycle() {
		return recraterevcycle;
	}

	public void setRecraterevcycle(String recraterevcycle) {
		this.recraterevcycle = recraterevcycle;
	}

	public String getRaterevfrstlst() {
		return raterevfrstlst;
	}

	public void setRaterevfrstlst(String raterevfrstlst) {
		this.raterevfrstlst = raterevfrstlst;
	}

	public String getRecraterevfrstlst() {
		return recraterevfrstlst;
	}

	public void setRecraterevfrstlst(String recraterevfrstlst) {
		this.recraterevfrstlst = recraterevfrstlst;
	}

	public String getCalcrule() {
		return calcrule;
	}

	public void setCalcrule(String calcrule) {
		this.calcrule = calcrule;
	}

	public String getReccalcrule() {
		return reccalcrule;
	}

	public void setReccalcrule(String reccalcrule) {
		this.reccalcrule = reccalcrule;
	}

	public void setDealind(String dealind) {
		this.dealind = dealind;
	}

	public String getDealind() {
		return dealind;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}

	public String getMaturityDate() {
		return maturityDate;
	}

	public String getSetoff() {
		return setoff;
	}

	public void setSetoff(String setoff) {
		this.setoff = setoff;
	}

}
