package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * SDVP 添加路径
 * 
 * @author shenzl
 *
 */
public class SlIdriBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5801359395158745687L;
	private SlInthBean inthBean;

	/**
	 * 交易编号
	 */
	private String dealno;
	/**
	 * 交易编号顺序号
	 */
	private String dealseq;
	/**
	 * 产品类型
	 */
	private String product;
	/**
	 * 产品类型
	 */
	private String prodtype;
	/**
	 * 交易对手编号
	 */
	private String cno;
	/**
	 * 币种
	 */
	private String ccy;
	/**
	 * 托管账户
	 */
	private String safekeepacct;

	private String recordtype;

	private String delrecind;
	/**
	 * 清算方式
	 */
	private String settmeans;
	/**
	 * 清算账户
	 */
	private String settacct;
	private String dcc;
	private String acctno;
	private String bic;
	private String c1;
	private String c2;
	private String c3;
	private String c4;
	private String c5;
	private String c6;
	private String ac1;
	private String ac2;
	private String ac3;
	private String ac4;
	private String ac5;
	private String ac6;
	/**
	 * 默认A-add U-update D-delete
	 */
	private String addupddel;
	/**
	 * 禁止发确认
	 */
	private String supconfind;

	private String supsecind;

	private String supccyind;
	/**
	 * 授权标识
	 */
	private String authind;
	/**
	 * 生效日期
	 */
	private Date effdate;

	private String usualid;

	private String ioper;

	private String inputdate;

	private String inputtime;
	private String veroper;
	private String verdate;
	private String vertime;
	private Date lstmntdte;

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getDealseq() {
		return dealseq;
	}

	public void setDealseq(String dealseq) {
		this.dealseq = dealseq;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSafekeepacct() {
		return safekeepacct;
	}

	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}

	public String getRecordtype() {
		return recordtype;
	}

	public void setRecordtype(String recordtype) {
		this.recordtype = recordtype;
	}

	public String getDelrecind() {
		return delrecind;
	}

	public void setDelrecind(String delrecind) {
		this.delrecind = delrecind;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettacct() {
		return settacct;
	}

	public void setSettacct(String settacct) {
		this.settacct = settacct;
	}

	public String getDcc() {
		return dcc;
	}

	public void setDcc(String dcc) {
		this.dcc = dcc;
	}

	public String getAcctno() {
		return acctno;
	}

	public void setAcctno(String acctno) {
		this.acctno = acctno;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getC1() {
		return c1;
	}

	public void setC1(String c1) {
		this.c1 = c1;
	}

	public String getC2() {
		return c2;
	}

	public void setC2(String c2) {
		this.c2 = c2;
	}

	public String getC3() {
		return c3;
	}

	public void setC3(String c3) {
		this.c3 = c3;
	}

	public String getC4() {
		return c4;
	}

	public void setC4(String c4) {
		this.c4 = c4;
	}

	public String getC5() {
		return c5;
	}

	public void setC5(String c5) {
		this.c5 = c5;
	}

	public String getC6() {
		return c6;
	}

	public void setC6(String c6) {
		this.c6 = c6;
	}

	public String getAc1() {
		return ac1;
	}

	public void setAc1(String ac1) {
		this.ac1 = ac1;
	}

	public String getAc2() {
		return ac2;
	}

	public void setAc2(String ac2) {
		this.ac2 = ac2;
	}

	public String getAc3() {
		return ac3;
	}

	public void setAc3(String ac3) {
		this.ac3 = ac3;
	}

	public String getAc4() {
		return ac4;
	}

	public void setAc4(String ac4) {
		this.ac4 = ac4;
	}

	public String getAc5() {
		return ac5;
	}

	public void setAc5(String ac5) {
		this.ac5 = ac5;
	}

	public String getAc6() {
		return ac6;
	}

	public void setAc6(String ac6) {
		this.ac6 = ac6;
	}

	public String getAddupddel() {
		return addupddel;
	}

	public void setAddupddel(String addupddel) {
		this.addupddel = addupddel;
	}

	public String getSupconfind() {
		return supconfind;
	}

	public void setSupconfind(String supconfind) {
		this.supconfind = supconfind;
	}

	public String getSupsecind() {
		return supsecind;
	}

	public void setSupsecind(String supsecind) {
		this.supsecind = supsecind;
	}

	public String getSupccyind() {
		return supccyind;
	}

	public void setSupccyind(String supccyind) {
		this.supccyind = supccyind;
	}

	public String getAuthind() {
		return authind;
	}

	public void setAuthind(String authind) {
		this.authind = authind;
	}

	public Date getEffdate() {
		return effdate;
	}

	public void setEffdate(Date effdate) {
		this.effdate = effdate;
	}

	public String getUsualid() {
		return usualid;
	}

	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getInputdate() {
		return inputdate;
	}

	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public String getVeroper() {
		return veroper;
	}

	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}

	public String getVerdate() {
		return verdate;
	}

	public void setVerdate(String verdate) {
		this.verdate = verdate;
	}

	public String getVertime() {
		return vertime;
	}

	public void setVertime(String vertime) {
		this.vertime = vertime;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SlIdriBean() {

	}

	/**
	 * 债券基本信息导入始化
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 		1、交易导入 TAG="SDVP" , DETAIL="IDRI"
	 * 
	 * 
	 */
	public SlIdriBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setInoutind(SlDealModule.SETTLE.INOUTIND);
			inthBean.setPriority(SlDealModule.SETTLE.PRIORITY);
			inthBean.setStatcode(SlDealModule.SETTLE.STATCODE);
			inthBean.setSyst(SlDealModule.SETTLE.SYST);
			inthBean.setIoper(SlDealModule.SETTLE.IOPER);
		}
	}
}
