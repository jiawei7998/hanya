package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 产品属性信息数组
 */
public class PdAttrInfoArray implements Serializable {

	private static final long serialVersionUID = 8871714876393157773L;

	/**
	 * 审批单号
	 */
	private String ORDER_ID;

	/**
	 * 交易类型
	 */
	private String TRAN_TYPE;

	/**
	 * 交易状态
	 */
	private String TRAN_STATUS;

	/**
	 * 录入柜员编码
	 */
	private String INPT_TELLER_NO;

	/**
	 * 产品名称
	 */
	private String PRODUCT_NAME;

	/**
	 * 年化收益率
	 */
	private String YLD_RATE;

	/**
	 * 投资计划
	 */
	private String IVTNT_PLN;

	/**
	 * 产品到期日期
	 */
	private String PD_EXP_DT;

	/**
	 * 审批件编号
	 */
	private String APPROVE_FL_NO;

	/**
	 * 交易类型中文
	 */
	private String trantp;

	/**
	 * 交易状态中文
	 */
	private String transt;

	public String getORDER_ID() {
		return ORDER_ID;
	}

	public void setORDER_ID(String oRDER_ID) {
		ORDER_ID = oRDER_ID;
	}

	public String getTRAN_TYPE() {
		return TRAN_TYPE;
	}

	public void setTRAN_TYPE(String tRAN_TYPE) {
		TRAN_TYPE = tRAN_TYPE;
	}

	public String getTRAN_STATUS() {
		return TRAN_STATUS;
	}

	public void setTRAN_STATUS(String tRAN_STATUS) {
		TRAN_STATUS = tRAN_STATUS;
	}

	public String getINPT_TELLER_NO() {
		return INPT_TELLER_NO;
	}

	public void setINPT_TELLER_NO(String iNPT_TELLER_NO) {
		INPT_TELLER_NO = iNPT_TELLER_NO;
	}

	public String getPRODUCT_NAME() {
		return PRODUCT_NAME;
	}

	public void setPRODUCT_NAME(String pRODUCT_NAME) {
		PRODUCT_NAME = pRODUCT_NAME;
	}

	public String getYLD_RATE() {
		return YLD_RATE;
	}

	public void setYLD_RATE(String yLD_RATE) {
		YLD_RATE = yLD_RATE;
	}

	public String getIVTNT_PLN() {
		return IVTNT_PLN;
	}

	public void setIVTNT_PLN(String iVTNT_PLN) {
		IVTNT_PLN = iVTNT_PLN;
	}

	public String getPD_EXP_DT() {
		return PD_EXP_DT;
	}

	public void setPD_EXP_DT(String pD_EXP_DT) {
		PD_EXP_DT = pD_EXP_DT;
	}

	public String getAPPROVE_FL_NO() {
		return APPROVE_FL_NO;
	}

	public void setAPPROVE_FL_NO(String aPPROVE_FL_NO) {
		APPROVE_FL_NO = aPPROVE_FL_NO;
	}

	public String getTrantp() {
		return trantp;
	}

	public void setTrantp(String trantp) {
		this.trantp = trantp;
	}

	public String getTranst() {
		return transt;
	}

	public void setTranst(String transt) {
		this.transt = transt;
	}
}