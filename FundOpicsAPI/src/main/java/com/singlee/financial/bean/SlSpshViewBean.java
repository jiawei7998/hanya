package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SlSpshViewBean implements Serializable {

	private static final long serialVersionUID = 1901551160942215641L;

	private String br;
	private String dealno;
	private Date settdate;
	private String cost;
	private String port;
	private String secid;
	private String invtype;
	private BigDecimal faceamt;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public Date getSettdate() {
		return settdate;
	}

	public void setSettdate(Date settdate) {
		this.settdate = settdate;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public BigDecimal getFaceamt() {
		return faceamt;
	}

	public void setFaceamt(BigDecimal faceamt) {
		this.faceamt = faceamt;
	}

}
