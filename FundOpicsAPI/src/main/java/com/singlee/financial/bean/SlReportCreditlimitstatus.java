package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 同业额度使用情况报告周报
 * @author LIJ
 *
 */
public class SlReportCreditlimitstatus implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -2807061279227512825L;

	/**
     * 
     */
    private String groupid;

    /**
     * 银行名称
     */
    private Object groupname;

    /**
     * 币种
     */
    private String ccy;

    /**
     * 授信额度
     */
    private BigDecimal credit;

    /**
     * 已使用额度
     */
    private BigDecimal usedcredit;

    /**
     * 剩余额度
     */
    private BigDecimal remaincredit;

    /**
     * 备注
     */
    private Object note;

    /**
     * 
     */
    private BigDecimal priority;

    /**
     * 
     */
    private String sheet;

    /**
     * 
     */
    private BigDecimal errflag;

    /**
     * 
     */
    private String rpdate;

    /**
     * null
     * @return GROUPID null
     */
    public String getGroupid() {
        return groupid;
    }

    /**
     * null
     * @param groupid null
     */
    public void setGroupid(String groupid) {
        this.groupid = groupid == null ? null : groupid.trim();
    }

    /**
     * null
     * @return GROUPNAME null
     */
    public Object getGroupname() {
        return groupname;
    }

    /**
     * null
     * @param groupname null
     */
    public void setGroupname(Object groupname) {
        this.groupname = groupname;
    }

    /**
     * 币种
     * @return CCY 币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 币种
     * @param ccy 币种
     */
    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    /**
     * 授信额度
     * @return CREDIT 授信额度
     */
    public BigDecimal getCredit() {
        return credit;
    }

    /**
     * 授信额度
     * @param credit 授信额度
     */
    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    /**
     * 已使用额度
     * @return USEDCREDIT 已使用额度
     */
    public BigDecimal getUsedcredit() {
        return usedcredit;
    }

    /**
     * 已使用额度
     * @param usedcredit 已使用额度
     */
    public void setUsedcredit(BigDecimal usedcredit) {
        this.usedcredit = usedcredit;
    }

    /**
     * 剩余额度
     * @return REMAINCREDIT 剩余额度
     */
    public BigDecimal getRemaincredit() {
        return remaincredit;
    }

    /**
     * 剩余额度
     * @param remaincredit 剩余额度
     */
    public void setRemaincredit(BigDecimal remaincredit) {
        this.remaincredit = remaincredit;
    }

    /**
     * 备注
     * @return NOTE 备注
     */
    public Object getNote() {
        return note;
    }

    /**
     * 备注
     * @param note 备注
     */
    public void setNote(Object note) {
        this.note = note;
    }

    /**
     * null
     * @return PRIORITY null
     */
    public BigDecimal getPriority() {
        return priority;
    }

    /**
     * null
     * @param priority null
     */
    public void setPriority(BigDecimal priority) {
        this.priority = priority;
    }

    /**
     * null
     * @return SHEET null
     */
    public String getSheet() {
        return sheet;
    }

    /**
     * null
     * @param sheet null
     */
    public void setSheet(String sheet) {
        this.sheet = sheet == null ? null : sheet.trim();
    }

    /**
     * null
     * @return ERRFLAG null
     */
    public BigDecimal getErrflag() {
        return errflag;
    }

    /**
     * null
     * @param errflag null
     */
    public void setErrflag(BigDecimal errflag) {
        this.errflag = errflag;
    }

    /**
     * null
     * @return RPDATE null
     */
    public String getRpdate() {
        return rpdate;
    }

    /**
     * null
     * @param rpdate null
     */
    public void setRpdate(String rpdate) {
        this.rpdate = rpdate == null ? null : rpdate.trim();
    }
}