package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 外汇远期
 * @author LIJ
 *
 */
public class GmrmFxForward implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 3961927282487692392L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 头寸编号
     */
    private String posId;

    /**
     * 数据来源
     */
    private String source;

    /**
     * Folder
     */
    private String folder;

    /**
     * 交割日 YYYYMMDD
     */
    private String valueDate;

    /**
     * 到期日 YYYYMMDD
     */
    private String maturityDate;

    /**
     * 币种1名称
     */
    private String ccy1;

    /**
     * 币种2名称
     */
    private String ccy2;

    /**
     * 币种一金额，带符号数值
     */
    private BigDecimal amount1;

    /**
     * 币种二金额，带符号数值
     */
    private BigDecimal amount2;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 损益币种
     */
    private String plCcy;

    /**
     * 每日损益，带符号数值
     */
    private BigDecimal dailyPl;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期 YYYYMMDD
     * @return BATCH_DATE 批量日期 YYYYMMDD
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期 YYYYMMDD
     * @param batchDate 批量日期 YYYYMMDD
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 头寸编号
     * @return POS_ID 头寸编号
     */
    public String getPosId() {
        return posId;
    }

    /**
     * 头寸编号
     * @param posId 头寸编号
     */
    public void setPosId(String posId) {
        this.posId = posId == null ? null : posId.trim();
    }

    /**
     * 数据来源
     * @return SOURCE 数据来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 数据来源
     * @param source 数据来源
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * Folder
     * @return FOLDER Folder
     */
    public String getFolder() {
        return folder;
    }

    /**
     * Folder
     * @param folder Folder
     */
    public void setFolder(String folder) {
        this.folder = folder == null ? null : folder.trim();
    }

    /**
     * 交割日 YYYYMMDD
     * @return VALUE_DATE 交割日 YYYYMMDD
     */
    public String getValueDate() {
        return valueDate;
    }

    /**
     * 交割日 YYYYMMDD
     * @param valueDate 交割日 YYYYMMDD
     */
    public void setValueDate(String valueDate) {
        this.valueDate = valueDate == null ? null : valueDate.trim();
    }

    /**
     * 到期日 YYYYMMDD
     * @return MATURITY_DATE 到期日 YYYYMMDD
     */
    public String getMaturityDate() {
        return maturityDate;
    }

    /**
     * 到期日 YYYYMMDD
     * @param maturityDate 到期日 YYYYMMDD
     */
    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate == null ? null : maturityDate.trim();
    }

    /**
     * 币种1名称
     * @return CCY1 币种1名称
     */
    public String getCcy1() {
        return ccy1;
    }

    /**
     * 币种1名称
     * @param ccy1 币种1名称
     */
    public void setCcy1(String ccy1) {
        this.ccy1 = ccy1 == null ? null : ccy1.trim();
    }

    /**
     * 币种2名称
     * @return CCY2 币种2名称
     */
    public String getCcy2() {
        return ccy2;
    }

    /**
     * 币种2名称
     * @param ccy2 币种2名称
     */
    public void setCcy2(String ccy2) {
        this.ccy2 = ccy2 == null ? null : ccy2.trim();
    }

    /**
     * 币种一金额，带符号数值
     * @return AMOUNT1 币种一金额，带符号数值
     */
    public BigDecimal getAmount1() {
        return amount1;
    }

    /**
     * 币种一金额，带符号数值
     * @param amount1 币种一金额，带符号数值
     */
    public void setAmount1(BigDecimal amount1) {
        this.amount1 = amount1;
    }

    /**
     * 币种二金额，带符号数值
     * @return AMOUNT2 币种二金额，带符号数值
     */
    public BigDecimal getAmount2() {
        return amount2;
    }

    /**
     * 币种二金额，带符号数值
     * @param amount2 币种二金额，带符号数值
     */
    public void setAmount2(BigDecimal amount2) {
        this.amount2 = amount2;
    }

    /**
     * 机构号
     * @return BRANCH_ID 机构号
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * 机构号
     * @param branchId 机构号
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }

    /**
     * 损益币种
     * @return PL_CCY 损益币种
     */
    public String getPlCcy() {
        return plCcy;
    }

    /**
     * 损益币种
     * @param plCcy 损益币种
     */
    public void setPlCcy(String plCcy) {
        this.plCcy = plCcy == null ? null : plCcy.trim();
    }

    /**
     * 每日损益，带符号数值
     * @return DAILY_PL 每日损益，带符号数值
     */
    public BigDecimal getDailyPl() {
        return dailyPl;
    }

    /**
     * 每日损益，带符号数值
     * @param dailyPl 每日损益，带符号数值
     */
    public void setDailyPl(BigDecimal dailyPl) {
        this.dailyPl = dailyPl;
    }
}