package com.singlee.financial.bean;


import java.io.Serializable;

public class ColDefBean  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String DATATYPE;
  private String PROPNAME;
  private String COLNAME;
  private int COLINDEX;
  private String COLTYPE;
  private int LENGTH;
  private int SCALE;
  private String OPERID;
  private String DESCR;
  private String LSTMNTDATE;
  
  public String getDATATYPE()
  {
    return this.DATATYPE;
  }
  
  public void setDATATYPE(String dATATYPE)
  {
    this.DATATYPE = dATATYPE;
  }
  
  public String getPROPNAME()
  {
    return this.PROPNAME;
  }
  
  public void setPROPNAME(String pROPNAME)
  {
    this.PROPNAME = pROPNAME;
  }
  
  public String getCOLNAME()
  {
    return this.COLNAME;
  }
  
  public void setCOLNAME(String cOLNAME)
  {
    this.COLNAME = cOLNAME;
  }
  
  public int getCOLINDEX()
  {
    return this.COLINDEX;
  }
  
  public void setCOLINDEX(int cOLINDEX)
  {
    this.COLINDEX = cOLINDEX;
  }
  
  public String getCOLTYPE()
  {
    return this.COLTYPE;
  }
  
  public void setCOLTYPE(String cOLTYPE)
  {
    this.COLTYPE = cOLTYPE;
  }
  
  public int getLENGTH()
  {
    return this.LENGTH;
  }
  
  public void setLENGTH(int lENGTH)
  {
    this.LENGTH = lENGTH;
  }
  
  public int getSCALE()
  {
    return this.SCALE;
  }
  
  public void setSCALE(int sCALE)
  {
    this.SCALE = sCALE;
  }
  
  public String getOPERID()
  {
    return this.OPERID;
  }
  
  public void setOPERID(String oPERID)
  {
    this.OPERID = oPERID;
  }
  
  public String getDESCR()
  {
    return this.DESCR;
  }
  
  public void setDESCR(String dESCR)
  {
    this.DESCR = dESCR;
  }
  
  public String getLSTMNTDATE()
  {
    return this.LSTMNTDATE;
  }
  
  public void setLSTMNTDATE(String lSTMNTDATE)
  {
    this.LSTMNTDATE = lSTMNTDATE;
  }
}
