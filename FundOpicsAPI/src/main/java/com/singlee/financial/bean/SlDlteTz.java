package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * @author fll
 */
public class SlDlteTz implements Serializable{
	private static final long serialVersionUID = 1L;
	private String br;
	private String dealNo;
	private String ps;
	private String rateType;
	private String ccy;
	private String verDate;
	private String mDate;
	private String intRate;
	private String ccyAmt;
	private String origocAmt;
	private String trad;
	private String cmne;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getVerDate() {
		return verDate;
	}
	public void setVerDate(String verDate) {
		this.verDate = verDate;
	}
	public String getmDate() {
		return mDate;
	}
	public void setmDate(String mDate) {
		this.mDate = mDate;
	}
	public String getIntRate() {
		return intRate;
	}
	public void setIntRate(String intRate) {
		this.intRate = intRate;
	}
	public String getCcyAmt() {
		return ccyAmt;
	}
	public void setCcyAmt(String ccyAmt) {
		this.ccyAmt = ccyAmt;
	}
	public String getOrigocAmt() {
		return origocAmt;
	}
	public void setOrigocAmt(String origocAmt) {
		this.origocAmt = origocAmt;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	
}
