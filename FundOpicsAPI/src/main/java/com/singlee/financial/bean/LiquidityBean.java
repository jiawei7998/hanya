package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * Liquidity-表一
 * @author wangzhao
 *
 */
public class LiquidityBean implements Serializable{
	  private static final long serialVersionUID = 1L;
	  private String tableid;//单据类型
	  private String br;//部门
	  private String lineid;
	  private String rowsid;
	  private Double amt1;//余额(所有币种折港币)
	  private Double amt2;//加权余额(港币+美元折港币)
	  private Double amt3;//加权余额(其他币别折港币)
	  private Double amt4;//内部抵消余额(港币+美元折港币)
	  private Double amt5;//内部抵消余额(其他币别折港币)
	  private String rpdate;//指标日期
	
	public String getTableid() {
		return tableid;
	}
	public void setTableid(String tableid) {
		this.tableid = tableid;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getLineid() {
		return lineid;
	}
	public void setLineid(String lineid) {
		this.lineid = lineid;
	}
	public String getRowsid() {
		return rowsid;
	}
	public void setRowsid(String rowsid) {
		this.rowsid = rowsid;
	}
	public Double getAmt1() {
		return amt1;
	}
	public void setAmt1(Double amt1) {
		this.amt1 = amt1;
	}
	public Double getAmt2() {
		return amt2;
	}
	public void setAmt2(Double amt2) {
		this.amt2 = amt2;
	}
	public String getRpdate() {
		return rpdate;
	}
	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}
	public Double getAmt3() {
		return amt3;
	}
	public void setAmt3(Double amt3) {
		this.amt3 = amt3;
	}
	public Double getAmt4() {
		return amt4;
	}
	public void setAmt4(Double amt4) {
		this.amt4 = amt4;
	}
	public Double getAmt5() {
		return amt5;
	}
	public void setAmt5(Double amt5) {
		this.amt5 = amt5;
	}
	@Override
	public String toString() {
		return "LiquidityBean [tableid=" + tableid + ", br=" + br + ", lineid="
				+ lineid + ", rowsid=" + rowsid + ", amt1=" + amt1 + ", amt2="
				+ amt2 + ", amt3=" + amt3 + ", amt4=" + amt4 + ", amt5=" + amt5
				+ ", rpdate=" + rpdate + "]";
	}

	  
}
