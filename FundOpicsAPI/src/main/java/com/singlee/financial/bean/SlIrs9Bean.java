package com.singlee.financial.bean;

import java.io.Serializable;

public class SlIrs9Bean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7771331998891387741L;
    /**
     * 回流文件返回字段
     */
    private String irs9_id; // id
    private String data_dt; // 数据日期
    private String batch_id; // 批次号
    private String br; // 部门
    private String secid; // 债券号
    private String irs9_cost; // 投资组合
    private String invtype; // 交易类
    private String port; // 投资组合
    private String asset_no;// 资产编号
    private String biz_type_cd; // 业务条线代码
    private String product_level_one_cd; // 产品大类代码
    private String busi_type_cd; // 业务产品类型代码
    private String stage_rslt_sys; // 系统判断阶段结果
    private String stage_rslt_affirm; // 人工认定阶段结果
    private String stage_rslt_final; // 最终阶段判断结果
    private String pd; // 违约概率
    private String pd_pre_adj; // 前瞻性调整后违约概率
    private String lgd; // 违约损失率
    private String ead; // 违约风险暴露
    private String ecl_prin; // 本金计提金额
    private String ecl_int; // 利息计提金额
    private String ecl_out; // 表外计提金额
    private String ecl_add; // 增提金额
    private String ecl_add_ratio; // 增提比例
    private String ecl_sys; // 系统计算计提金额
    private String ecl_affirm; // 人工认定计提金额
    private String ecl_bf_add; // 增提前计提金额
    private String ecl_final; // 最终结果计提金额
    private String asset_bal; // 资产余额
    private String currency_cd; // 币种代码
    private String customer_no; // 客户编号
    private String customer_name; // 客户名称
    private String rating_level_cd; // 信用等级代码
    private String apply_rating_level_cd; // 申请时点信用等级代码
    private String contract_no; // 合同编号
    private String org_no; // 业务所在机构编号
    private String org_name; // 业务所在机构名称
    private String is_forward_adj; // 是否前瞻性调整
    private String putout_dt; // 发放日期
    private String maturity; // 到期日期
    private String overdue_days; // 逾期天数
    private String loan_cls_cd; // 资产分类结果代码
    private String acct_subject_no; // 会计科目编号
    private String asset_three_class_cd; // 金融资产三分类
    private String overdue_status_cd; // 逾期状态代码
    private String customer_size_cd; // 企业规模代码
    private String industry_cd; // 国标行业类型代码
    private String industry_level_one_cd; // 国标行业门类代码
    private String industry_level_two_cd; // 国标行业大类代码
    private String industry_level_three_cd; // 国标行业中类代码
    private String exec_int_rate; // 执行利率
    private String src_sys; // 来源系统
    private String create_user; // 创建人
    private String create_time; // 创建时间
    private String update_user; // 更新人
    private String update_time; // 更新时间
    private String tenant_id; // 多实体标识
    private String irs9_version; // 框架版本号
    private String postdate; // 账务日期

    public String getIrs9_id() {
        return irs9_id;
    }

    public void setIrs9_id(String irs9_id) {
        this.irs9_id = irs9_id;
    }

    public String getData_dt() {
        return data_dt;
    }

    public void setData_dt(String data_dt) {
        this.data_dt = data_dt;
    }

    public String getBatch_id() {
        return batch_id;
    }

    public void setBatch_id(String batch_id) {
        this.batch_id = batch_id;
    }

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid;
    }

    public String getIrs9_cost() {
        return irs9_cost;
    }

    public void setIrs9_cost(String irs9_cost) {
        this.irs9_cost = irs9_cost;
    }

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAsset_no() {
        return asset_no;
    }

    public void setAsset_no(String asset_no) {
        this.asset_no = asset_no;
    }

    public String getBiz_type_cd() {
        return biz_type_cd;
    }

    public void setBiz_type_cd(String biz_type_cd) {
        this.biz_type_cd = biz_type_cd;
    }

    public String getProduct_level_one_cd() {
        return product_level_one_cd;
    }

    public void setProduct_level_one_cd(String product_level_one_cd) {
        this.product_level_one_cd = product_level_one_cd;
    }

    public String getBusi_type_cd() {
        return busi_type_cd;
    }

    public void setBusi_type_cd(String busi_type_cd) {
        this.busi_type_cd = busi_type_cd;
    }

    public String getStage_rslt_sys() {
        return stage_rslt_sys;
    }

    public void setStage_rslt_sys(String stage_rslt_sys) {
        this.stage_rslt_sys = stage_rslt_sys;
    }

    public String getStage_rslt_affirm() {
        return stage_rslt_affirm;
    }

    public void setStage_rslt_affirm(String stage_rslt_affirm) {
        this.stage_rslt_affirm = stage_rslt_affirm;
    }

    public String getStage_rslt_final() {
        return stage_rslt_final;
    }

    public void setStage_rslt_final(String stage_rslt_final) {
        this.stage_rslt_final = stage_rslt_final;
    }

    public String getPd() {
        return pd;
    }

    public void setPd(String pd) {
        this.pd = pd;
    }

    public String getPd_pre_adj() {
        return pd_pre_adj;
    }

    public void setPd_pre_adj(String pd_pre_adj) {
        this.pd_pre_adj = pd_pre_adj;
    }

    public String getLgd() {
        return lgd;
    }

    public void setLgd(String lgd) {
        this.lgd = lgd;
    }

    public String getEad() {
        return ead;
    }

    public void setEad(String ead) {
        this.ead = ead;
    }

    public String getEcl_prin() {
        return ecl_prin;
    }

    public void setEcl_prin(String ecl_prin) {
        this.ecl_prin = ecl_prin;
    }

    public String getEcl_int() {
        return ecl_int;
    }

    public void setEcl_int(String ecl_int) {
        this.ecl_int = ecl_int;
    }

    public String getEcl_out() {
        return ecl_out;
    }

    public void setEcl_out(String ecl_out) {
        this.ecl_out = ecl_out;
    }

    public String getEcl_add() {
        return ecl_add;
    }

    public void setEcl_add(String ecl_add) {
        this.ecl_add = ecl_add;
    }

    public String getEcl_add_ratio() {
        return ecl_add_ratio;
    }

    public void setEcl_add_ratio(String ecl_add_ratio) {
        this.ecl_add_ratio = ecl_add_ratio;
    }

    public String getEcl_sys() {
        return ecl_sys;
    }

    public void setEcl_sys(String ecl_sys) {
        this.ecl_sys = ecl_sys;
    }

    public String getEcl_affirm() {
        return ecl_affirm;
    }

    public void setEcl_affirm(String ecl_affirm) {
        this.ecl_affirm = ecl_affirm;
    }

    public String getEcl_bf_add() {
        return ecl_bf_add;
    }

    public void setEcl_bf_add(String ecl_bf_add) {
        this.ecl_bf_add = ecl_bf_add;
    }

    public String getEcl_final() {
        return ecl_final;
    }

    public void setEcl_final(String ecl_final) {
        this.ecl_final = ecl_final;
    }

    public String getAsset_bal() {
        return asset_bal;
    }

    public void setAsset_bal(String asset_bal) {
        this.asset_bal = asset_bal;
    }

    public String getCurrency_cd() {
        return currency_cd;
    }

    public void setCurrency_cd(String currency_cd) {
        this.currency_cd = currency_cd;
    }

    public String getCustomer_no() {
        return customer_no;
    }

    public void setCustomer_no(String customer_no) {
        this.customer_no = customer_no;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getRating_level_cd() {
        return rating_level_cd;
    }

    public void setRating_level_cd(String rating_level_cd) {
        this.rating_level_cd = rating_level_cd;
    }

    public String getApply_rating_level_cd() {
        return apply_rating_level_cd;
    }

    public void setApply_rating_level_cd(String apply_rating_level_cd) {
        this.apply_rating_level_cd = apply_rating_level_cd;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getOrg_no() {
        return org_no;
    }

    public void setOrg_no(String org_no) {
        this.org_no = org_no;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getIs_forward_adj() {
        return is_forward_adj;
    }

    public void setIs_forward_adj(String is_forward_adj) {
        this.is_forward_adj = is_forward_adj;
    }

    public String getPutout_dt() {
        return putout_dt;
    }

    public void setPutout_dt(String putout_dt) {
        this.putout_dt = putout_dt;
    }

    public String getMaturity() {
        return maturity;
    }

    public void setMaturity(String maturity) {
        this.maturity = maturity;
    }

    public String getOverdue_days() {
        return overdue_days;
    }

    public void setOverdue_days(String overdue_days) {
        this.overdue_days = overdue_days;
    }

    public String getLoan_cls_cd() {
        return loan_cls_cd;
    }

    public void setLoan_cls_cd(String loan_cls_cd) {
        this.loan_cls_cd = loan_cls_cd;
    }

    public String getAcct_subject_no() {
        return acct_subject_no;
    }

    public void setAcct_subject_no(String acct_subject_no) {
        this.acct_subject_no = acct_subject_no;
    }

    public String getAsset_three_class_cd() {
        return asset_three_class_cd;
    }

    public void setAsset_three_class_cd(String asset_three_class_cd) {
        this.asset_three_class_cd = asset_three_class_cd;
    }

    public String getOverdue_status_cd() {
        return overdue_status_cd;
    }

    public void setOverdue_status_cd(String overdue_status_cd) {
        this.overdue_status_cd = overdue_status_cd;
    }

    public String getCustomer_size_cd() {
        return customer_size_cd;
    }

    public void setCustomer_size_cd(String customer_size_cd) {
        this.customer_size_cd = customer_size_cd;
    }

    public String getIndustry_cd() {
        return industry_cd;
    }

    public void setIndustry_cd(String industry_cd) {
        this.industry_cd = industry_cd;
    }

    public String getIndustry_level_one_cd() {
        return industry_level_one_cd;
    }

    public void setIndustry_level_one_cd(String industry_level_one_cd) {
        this.industry_level_one_cd = industry_level_one_cd;
    }

    public String getIndustry_level_two_cd() {
        return industry_level_two_cd;
    }

    public void setIndustry_level_two_cd(String industry_level_two_cd) {
        this.industry_level_two_cd = industry_level_two_cd;
    }

    public String getIndustry_level_three_cd() {
        return industry_level_three_cd;
    }

    public void setIndustry_level_three_cd(String industry_level_three_cd) {
        this.industry_level_three_cd = industry_level_three_cd;
    }

    public String getExec_int_rate() {
        return exec_int_rate;
    }

    public void setExec_int_rate(String exec_int_rate) {
        this.exec_int_rate = exec_int_rate;
    }

    public String getSrc_sys() {
        return src_sys;
    }

    public void setSrc_sys(String src_sys) {
        this.src_sys = src_sys;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(String update_user) {
        this.update_user = update_user;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getTenant_id() {
        return tenant_id;
    }

    public void setTenant_id(String tenant_id) {
        this.tenant_id = tenant_id;
    }

    public String getIrs9_version() {
        return irs9_version;
    }

    public void setIrs9_version(String irs9_version) {
        this.irs9_version = irs9_version;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

}
