package com.singlee.financial.bean;

import com.singlee.financial.pojo.FixedIncomeBean;

import java.util.Date;

/**
 * 债券买卖实体类
 */
public class SlFixedIncomeBean extends FixedIncomeBean {

    /**
     *
     */
    private static final long serialVersionUID = 4952160136190504643L;

    /**
     * INTH 头表信息
     */
    private SlInthBean inthBean;
    /**
     * OPICS拓展属性
     */
    private SlExternalBean externalBean;

    private String standinstr;

    private String usualid;
    /**
     * 投资类型
     */
    private String invtype;
    /**
     * 原始金额
     */
    private String origamt;
    /**
     * 原始数量
     */
    private String origqty;
    /**
     *
     */
    private String secsacct;
    /**
     * 清算路径
     */
    private String ccysmeans;
    /**
     * 清算账户
     */
    private String ccysacct;
    /**
     * 复核日期
     */
    private Date verdate;
    /**
     * 延递交割
     */
    private String delaydelivind;




    public String getCcysmeans() {
        return ccysmeans;
    }

    public void setCcysmeans(String ccysmeans) {
        this.ccysmeans = ccysmeans;
    }

    public String getCcysacct() {
        return ccysacct;
    }

    public void setCcysacct(String ccysacct) {
        this.ccysacct = ccysacct;
    }

    /**
     * 固定收益率初始化
     *
     * @param br     业务部门分支
     * @param tag    标识
     * @param detail 细节
     * @note 1、交易导入 TAG="FIDE" , DETAIL="BRED"
     * <p>
     * 2、交易冲销 TAG="FIDR", DETAIL="BRED"
     */
    public SlFixedIncomeBean(String br, String server, String tag, String detail) {
        if (null == inthBean) {
            inthBean = new SlInthBean(); // 拆借
            inthBean.setBr(br);//
            inthBean.setServer(server);
            inthBean.setTag(tag);
            inthBean.setDetail(detail);
            inthBean.setSeq(SlDealModule.FI.SEQ);
            inthBean.setInoutind(SlDealModule.FI.INOUTIND);
            inthBean.setPriority(SlDealModule.FI.PRIORITY);
            inthBean.setStatcode(SlDealModule.FI.STATCODE);
            inthBean.setSyst(SlDealModule.FI.SYST);
            inthBean.setIoper(SlDealModule.FI.IOPER);
        }
    }


    public SlInthBean getInthBean() {
        return inthBean;
    }

    public void setInthBean(SlInthBean inthBean) {
        this.inthBean = inthBean;
    }

    public SlExternalBean getExternalBean() {
        return externalBean;
    }

    public void setExternalBean(SlExternalBean externalBean) {
        this.externalBean = externalBean;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getStandinstr() {
        return standinstr;
    }

    public void setStandinstr(String standinstr) {
        this.standinstr = standinstr;
    }

    public String getUsualid() {
        return usualid;
    }

    public void setUsualid(String usualid) {
        this.usualid = usualid;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    public String getInvtype() {
        return invtype;
    }

    public String getOrigamt() {
        return origamt;
    }

    public void setOrigamt(String origamt) {
        this.origamt = origamt;
    }

    public String getOrigqty() {
        return origqty;
    }

    public void setOrigqty(String origqty) {
        this.origqty = origqty;
    }

    public String getSecsacct() {
        return secsacct;
    }

    public void setSecsacct(String secsacct) {
        this.secsacct = secsacct;
    }

    public void setVerdate(Date verdate) {
        this.verdate = verdate;
    }

    public Date getVerdate() {
        return verdate;
    }

    public void setDelaydelivind(String delaydelivind) {
        this.delaydelivind = delaydelivind;
    }

    public String getDelaydelivind() {
        return delaydelivind;
    }

}
