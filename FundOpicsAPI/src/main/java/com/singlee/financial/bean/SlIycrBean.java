package com.singlee.financial.bean;

import java.io.Serializable;

public class SlIycrBean implements Serializable{

	/**
	 * IYCR
	 */
	private static final long serialVersionUID = 1470217465574773121L;
	private SlInthBean inthBean;
	private String br;//部门
	private String server;
	private String fedealno;
	private String seq;
	private String inoutind;
	private String ratetype;//利率类型
	private String ccy;//币种
	private String yieldcurve;//利率曲线
	private String mtystart;
	private String mtyend;//期限
	private double bidrate8;
	private double midrate8;
	private double offerrate8;
	private String lstmntdate;
	//市场数据查询所需字段
	private String feidate;
	private String statcode;
	private String errorcode;
	
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getFedealno() {
		return fedealno;
	}
	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getInoutind() {
		return inoutind;
	}
	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}
	public String getRatetype() {
		return ratetype;
	}
	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getYieldcurve() {
		return yieldcurve;
	}
	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}
	public String getMtystart() {
		return mtystart;
	}
	public void setMtystart(String mtystart) {
		this.mtystart = mtystart;
	}
	public String getMtyend() {
		return mtyend;
	}
	public void setMtyend(String mtyend) {
		this.mtyend = mtyend;
	}
	public double getBidrate8() {
		return bidrate8;
	}
	public void setBidrate8(double bidrate8) {
		this.bidrate8 = bidrate8;
	}
	public double getMidrate8() {
		return midrate8;
	}
	public void setMidrate8(double midrate8) {
		this.midrate8 = midrate8;
	}
	public double getOfferrate8() {
		return offerrate8;
	}
	public void setOfferrate8(double offerrate8) {
		this.offerrate8 = offerrate8;
	}
	
	public String getLstmntdate() {
		return lstmntdate;
	}
	public void setLstmntdate(String lstmntdate) {
		this.lstmntdate = lstmntdate;
	}
	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIycrBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); 
			inthBean.setBr(br);
			inthBean.setServer(SlDealModule.IYCR.SERVER);
			inthBean.setSeq(SlDealModule.IYCR.SEQ);
			inthBean.setInoutind(SlDealModule.IYCR.INOUTIND);
			inthBean.setTag(SlDealModule.IYCR.TAG);
			inthBean.setDetail(SlDealModule.IYCR.DETAIL);
			inthBean.setPriority(SlDealModule.IYCR.PRIORITY);
			inthBean.setStatcode(SlDealModule.IYCR.STATCODE);
			inthBean.setSyst(SlDealModule.IYCR.SYST);
			inthBean.setIoper(SlDealModule.IYCR.IOPER);
		}
   }
	public String getFeidate() {
		return feidate;
	}
	public void setFeidate(String feidate) {
		this.feidate = feidate;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	
}
