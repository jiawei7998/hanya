package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class SlIselBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SlInthBean inthBean;
	private String br;// 部门
	private String server;
	private String fedealno;
	private String seq;
	private String inoutind;
	private String secid;
	private BigDecimal clsgprice8;
	private BigDecimal askprice8;
	private BigDecimal bidprice8;
	private BigDecimal clsgyield8;
	private BigDecimal askyield8;
	private BigDecimal bidyield8;
	private BigDecimal offclsgprice8;
	private BigDecimal offclsgpriceyst8;
	private String lstmntDate;

	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIselBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean();
			inthBean.setBr(br);
			inthBean.setServer(SlDealModule.ISEL.SERVER);
			inthBean.setSeq(SlDealModule.ISEL.SEQ);
			inthBean.setInoutind(SlDealModule.ISEL.INOUTIND);
			inthBean.setTag(SlDealModule.ISEL.TAG);
			inthBean.setDetail(SlDealModule.ISEL.DETAIL);
			inthBean.setPriority(SlDealModule.ISEL.PRIORITY);
			inthBean.setStatcode(SlDealModule.ISEL.STATCODE);
			inthBean.setSyst(SlDealModule.ISEL.SYST);
			inthBean.setIoper(SlDealModule.ISEL.IOPER);
		}
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getInoutind() {
		return inoutind;
	}

	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public BigDecimal getClsgprice8() {
		return clsgprice8;
	}

	public void setClsgprice8(BigDecimal clsgprice8) {
		this.clsgprice8 = clsgprice8;
	}

	public BigDecimal getAskprice8() {
		return askprice8;
	}

	public void setAskprice8(BigDecimal askprice8) {
		this.askprice8 = askprice8;
	}

	public BigDecimal getBidprice8() {
		return bidprice8;
	}

	public void setBidprice8(BigDecimal bidprice8) {
		this.bidprice8 = bidprice8;
	}

	public BigDecimal getClsgyield8() {
		return clsgyield8;
	}

	public void setClsgyield8(BigDecimal clsgyield8) {
		this.clsgyield8 = clsgyield8;
	}

	public BigDecimal getAskyield8() {
		return askyield8;
	}

	public void setAskyield8(BigDecimal askyield8) {
		this.askyield8 = askyield8;
	}

	public BigDecimal getBidyield8() {
		return bidyield8;
	}

	public void setBidyield8(BigDecimal bidyield8) {
		this.bidyield8 = bidyield8;
	}

	public BigDecimal getOffclsgprice8() {
		return offclsgprice8;
	}

	public void setOffclsgprice8(BigDecimal offclsgprice8) {
		this.offclsgprice8 = offclsgprice8;
	}

	public BigDecimal getOffclsgpriceyst8() {
		return offclsgpriceyst8;
	}

	public void setOffclsgpriceyst8(BigDecimal offclsgpriceyst8) {
		this.offclsgpriceyst8 = offclsgpriceyst8;
	}

	public String getLstmntDate() {
		return lstmntDate;
	}

	public void setLstmntDate(String lstmntDate) {
		this.lstmntDate = lstmntDate;
	}

}
