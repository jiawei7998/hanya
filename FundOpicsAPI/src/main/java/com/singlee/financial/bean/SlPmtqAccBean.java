package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 大额支付账号
 */
public class SlPmtqAccBean implements Serializable {

	private static final long serialVersionUID = -6768933647634529540L;

	private String recvbankname;//行名
	private String recvbankno;//行号
	private String recvaccname;//账户名
	private String recvaccno;//账户号
	private String mbfememo;//附言

	private String amount;//本金
	private String brecvbankname;//本金行名
	private String brecvbankno;//本金行号
	private String brecvaccname;//本金账户名
	private String brecvaccno;//本金账户号
	private String bmbfememo;//本金附言

	private String rate;//利息
	private String rrecvbankname;//利息行名
	private String rrecvbankno;//利息行号
	private String rrecvaccname;//利息账户名
	private String rrecvaccno;//利息账户号
	private String rmbfememo;//利息附言

	public String getRecvbankname() {
		return recvbankname;
	}

	public void setRecvbankname(String recvbankname) {
		this.recvbankname = recvbankname;
	}

	public String getRecvbankno() {
		return recvbankno;
	}

	public void setRecvbankno(String recvbankno) {
		this.recvbankno = recvbankno;
	}

	public String getRecvaccname() {
		return recvaccname;
	}

	public void setRecvaccname(String recvaccname) {
		this.recvaccname = recvaccname;
	}

	public String getRecvaccno() {
		return recvaccno;
	}

	public void setRecvaccno(String recvaccno) {
		this.recvaccno = recvaccno;
	}

	public String getMbfememo() {
		return mbfememo;
	}

	public void setMbfememo(String mbfememo) {
		this.mbfememo = mbfememo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBrecvbankname() {
		return brecvbankname;
	}

	public void setBrecvbankname(String brecvbankname) {
		this.brecvbankname = brecvbankname;
	}

	public String getBrecvbankno() {
		return brecvbankno;
	}

	public void setBrecvbankno(String brecvbankno) {
		this.brecvbankno = brecvbankno;
	}

	public String getBrecvaccname() {
		return brecvaccname;
	}

	public void setBrecvaccname(String brecvaccname) {
		this.brecvaccname = brecvaccname;
	}

	public String getBrecvaccno() {
		return brecvaccno;
	}

	public void setBrecvaccno(String brecvaccno) {
		this.brecvaccno = brecvaccno;
	}

	public String getBmbfememo() {
		return bmbfememo;
	}

	public void setBmbfememo(String bmbfememo) {
		this.bmbfememo = bmbfememo;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getRrecvbankname() {
		return rrecvbankname;
	}

	public void setRrecvbankname(String rrecvbankname) {
		this.rrecvbankname = rrecvbankname;
	}

	public String getRrecvbankno() {
		return rrecvbankno;
	}

	public void setRrecvbankno(String rrecvbankno) {
		this.rrecvbankno = rrecvbankno;
	}

	public String getRrecvaccname() {
		return rrecvaccname;
	}

	public void setRrecvaccname(String rrecvaccname) {
		this.rrecvaccname = rrecvaccname;
	}

	public String getRrecvaccno() {
		return rrecvaccno;
	}

	public void setRrecvaccno(String rrecvaccno) {
		this.rrecvaccno = rrecvaccno;
	}

	public String getRmbfememo() {
		return rmbfememo;
	}

	public void setRmbfememo(String rmbfememo) {
		this.rmbfememo = rmbfememo;
	}
}
