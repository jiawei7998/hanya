package com.singlee.financial.bean;

import java.math.BigDecimal;
import java.util.Date;

/** 
 * 华商银行-大额支付显示数据bean类  
 * @author zhengfl
 *
 */
public class PmtqBean implements java.io.Serializable {
	private static final long serialVersionUID = -772320120929091204L;
	
	private String      SN ;//             varchar(20)       NOT NULL,1
	private String      BR;//              char(2)           NULL,2
	private String      PRODUCT;//         varchar(6)        NOT NULL,3
	private String      PRODTYPE;//        char(2)           NULL,4
	private String      DEALNO ;//         varchar(8)        NULL,5
	
	private String      SEQ ; //           varchar(4)        NULL,6
	private String      PAYRECIND;//       char(1)           NULL,7
	private Date        CDATE ;//          datetime          NULL,8
	private String      CTIME;//           char(8)           NULL,9
	private String      CCY ;//            char(3)           NULL,10
	
	private BigDecimal  AMOUNT;//          numeric(19, 4)    NULL,11
	private String      SWIFTFMT ;//       varchar(4)        NULL,12
	private String      SETTMEANS ;//      varchar(10)       NULL,13
	private String      SETACCT  ;//       varchar(50)       NULL,14
	private String      SETTLTYP  ;//      varchar(20)       NULL,15
	
	private String      INOUTFLAG  ;//     char(1)           NULL,16
	private String      CITYFLAG  ;//      char(1)           NULL,17
	private Date        DEALDATE ;//       datetime          NULL,18
	private Date        VDATE ;//          datetime          NULL,19
	private String      CNO  ;//           varchar(11)       NULL,20
	
	private String      PBNKNO  ;//        nvarchar(60)      NULL,21
	private String      PBNKNM  ;//        nvarchar(60)      NULL,22
	private String      PACCTNO ;//        varchar(60)       NULL,23
	private String      PACCTNM ;//        nvarchar(80)      NULL,24
	private String      RBNKNO   ;//       nvarchar(60)      NULL,25
	
	private String      RBNKNM  ;//        nvarchar(60)      NULL,26
	private String      RACCTNO  ;//       varchar(60)       NULL,27
	private String      RACCTNM;//         nvarchar(80)      NULL,28
	private String      PAYNOTE  ;//       nvarchar(100)     NULL,29
	private String      CUSTLOCATION  ;//  nvarchar(50)      NULL,30
	
	private String      PLOCATION ;//      nvarchar(50)      NULL,31
	private String      RLOCATION  ;//     nvarchar(50)      NULL,32
	private String      VOIDFLAG  ;//      char(1)           NULL,33
	private String      VERIND ;//         char(1)           NULL,34
	private BigDecimal  REALAMOUNT ;//     numeric(19, 4)    NULL,35
	
	private BigDecimal  BALANCE;//         numeric(19, 4)    NULL,36
	private String      IOPER ;//          varchar(20)       NULL,37
	private String      INOTE ;//          nvarchar(100)     NULL,38
	private Date        IDATE  ;//         datetime          NULL,39
	private String      VEROPER   ;//      varchar(20)       NULL,40
	
	private String      VERNOTE ;//        nvarchar(100)     NULL,41
	private Date        VERDATE   ;//      datetime          NULL,42
	private String      AUTHOPER ;//       varchar(20)       NULL,43
	private Date        AUTHDATE ;//       datetime          NULL,44
	private String      SENDFLAG ;//       char(1)           NULL,45
	
	private String      SENDOPER  ;//      varchar(20)       NULL,46	
	private Date        SENDDATE  ;//      datetime          NULL,47
	private String      SENDMSG ;//        nvarchar(1000)    NULL,48
	private String      SRLENGTH  ;//      char(4)           NULL,49
	private String      SRPRCSCD  ;//      char(3)           NULL,50
	
	private String      SRRETUCD  ;//      char(4)           NULL,51
	private String      SRTRANDT  ;//      char(8)           NULL,52
	private String      SRTRANSQ  ;//      char(8)           NULL,53
	private String      SRMAINSEQ  ;//     char(8)           NULL,54
	private String      SRTRANSSQE ;//     char(8)           NULL,55
	
	private String      REVERSEMSG;//      nvarchar(1000)    NULL,56
	private String      RRLENGTH   ;//     char(4)           NULL,57
	private String      RRPRCSCD ;//       char(3)           NULL,58
	private String      RRRETUCD ;//       char(4)           NULL,59
	private String      QUERYMSG ;//       nvarchar(1000)    NULL,60
	
	private String      QRLENGTH ;//       char(4)           NULL,61
	private String      QRPRCSCD ;//       char(3)           NULL,62
	private String      QRRETUCD  ;//      char(4)           NULL,63
	private String      QRSTATUS ;//       char(2)           NULL,64
	private Date        QRTIME  ;//        datetime          NULL,65
	
	private String      CHECKFLAG ;//      char(1)           NULL,66
	private String      CHECKIOPER ;//     varchar(20)       NULL,67
	private String      CHECKINOTE ;//     nvarchar(100)     NULL,68
	private Date        CHECKIDATE   ;//   datetime          NULL,69
	private String      CHECKVOPER  ;//    varchar(20)       NULL,70
	
	private String      CHECKVNOTE ;//     nvarchar(50)      NULL,71
	private Date        CHECKVDATE;//      datetime          NULL,72
	private String      FEDEALNO   ;//     varchar(20)       NULL,73
	private String      SERVER   ;//       varchar(20)       NULL,74
	private String      SUMSN  ;//         varchar(50)       NULL,75
	
	private Integer     SUMCOUNT  ;//      int               NULL,76
	
	private String reSendFlg;   //重发标识  1-重发    其他-正常交易
	private String hostIntfCode;  //主机接口代码
	
	
	private String teller;//柜员
	private String areaCode;//地区
	private String branchNo;//网点号
    
	
	public String getSN() {
		return SN;
	}
	public void setSN(String sN) {
		SN = sN;
	}
	public String getBR() {
		return BR;
	}
	public void setBR(String bR) {
		BR = bR;
	}
	public String getPRODUCT() {
		return PRODUCT;
	}
	public void setPRODUCT(String pRODUCT) {
		PRODUCT = pRODUCT;
	}
	public String getPRODTYPE() {
		return PRODTYPE;
	}
	public void setPRODTYPE(String pRODTYPE) {
		PRODTYPE = pRODTYPE;
	}
	public String getDEALNO() {
		return DEALNO;
	}
	public void setDEALNO(String dEALNO) {
		DEALNO = dEALNO;
	}
	public String getSEQ() {
		return SEQ;
	}
	public void setSEQ(String sEQ) {
		SEQ = sEQ;
	}
	public String getPAYRECIND() {
		return PAYRECIND;
	}
	public void setPAYRECIND(String pAYRECIND) {
		PAYRECIND = pAYRECIND;
	}
	public Date getCDATE() {
		return CDATE;
	}
	public void setCDATE(Date cDATE) {
		CDATE = cDATE;
	}
	public String getCTIME() {
		return CTIME;
	}
	public void setCTIME(String cTIME) {
		CTIME = cTIME;
	}
	public String getCCY() {
		return CCY;
	}
	public void setCCY(String cCY) {
		CCY = cCY;
	}
	public BigDecimal getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(BigDecimal aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public String getSWIFTFMT() {
		return SWIFTFMT;
	}
	public void setSWIFTFMT(String sWIFTFMT) {
		SWIFTFMT = sWIFTFMT;
	}
	public String getSETTMEANS() {
		return SETTMEANS;
	}
	public void setSETTMEANS(String sETTMEANS) {
		SETTMEANS = sETTMEANS;
	}
	public String getSETACCT() {
		return SETACCT;
	}
	public void setSETACCT(String sETACCT) {
		SETACCT = sETACCT;
	}
	public String getSETTLTYP() {
		return SETTLTYP;
	}
	public void setSETTLTYP(String sETTLTYP) {
		SETTLTYP = sETTLTYP;
	}
	public String getINOUTFLAG() {
		return INOUTFLAG;
	}
	public void setINOUTFLAG(String iNOUTFLAG) {
		INOUTFLAG = iNOUTFLAG;
	}
	public String getCITYFLAG() {
		return CITYFLAG;
	}
	public void setCITYFLAG(String cITYFLAG) {
		CITYFLAG = cITYFLAG;
	}
	public Date getDEALDATE() {
		return DEALDATE;
	}
	public void setDEALDATE(Date dEALDATE) {
		DEALDATE = dEALDATE;
	}
	public Date getVDATE() {
		return VDATE;
	}
	public void setVDATE(Date vDATE) {
		VDATE = vDATE;
	}
	public String getCNO() {
		return CNO;
	}
	public void setCNO(String cNO) {
		CNO = cNO;
	}
	public String getPBNKNO() {
		return PBNKNO;
	}
	public void setPBNKNO(String pBNKNO) {
		PBNKNO = pBNKNO;
	}
	public String getPBNKNM() {
		return PBNKNM;
	}
	public void setPBNKNM(String pBNKNM) {
		PBNKNM = pBNKNM;
	}
	public String getPACCTNO() {
		return PACCTNO;
	}
	public void setPACCTNO(String pACCTNO) {
		PACCTNO = pACCTNO;
	}
	public String getPACCTNM() {
		return PACCTNM;
	}
	public void setPACCTNM(String pACCTNM) {
		PACCTNM = pACCTNM;
	}
	public String getRBNKNO() {
		return RBNKNO;
	}
	public void setRBNKNO(String rBNKNO) {
		RBNKNO = rBNKNO;
	}
	public String getRBNKNM() {
		return RBNKNM;
	}
	public void setRBNKNM(String rBNKNM) {
		RBNKNM = rBNKNM;
	}
	public String getRACCTNO() {
		return RACCTNO;
	}
	public void setRACCTNO(String rACCTNO) {
		RACCTNO = rACCTNO;
	}
	public String getRACCTNM() {
		return RACCTNM;
	}
	public void setRACCTNM(String rACCTNM) {
		RACCTNM = rACCTNM;
	}
	public String getPAYNOTE() {
		return PAYNOTE;
	}
	public void setPAYNOTE(String pAYNOTE) {
		PAYNOTE = pAYNOTE;
	}
	public String getCUSTLOCATION() {
		return CUSTLOCATION;
	}
	public void setCUSTLOCATION(String cUSTLOCATION) {
		CUSTLOCATION = cUSTLOCATION;
	}
	public String getPLOCATION() {
		return PLOCATION;
	}
	public void setPLOCATION(String pLOCATION) {
		PLOCATION = pLOCATION;
	}
	public String getRLOCATION() {
		return RLOCATION;
	}
	public void setRLOCATION(String rLOCATION) {
		RLOCATION = rLOCATION;
	}
	public String getVOIDFLAG() {
		return VOIDFLAG;
	}
	public void setVOIDFLAG(String vOIDFLAG) {
		VOIDFLAG = vOIDFLAG;
	}
	public String getVERIND() {
		return VERIND;
	}
	public void setVERIND(String vERIND) {
		VERIND = vERIND;
	}
	public BigDecimal getREALAMOUNT() {
		return REALAMOUNT;
	}
	public void setREALAMOUNT(BigDecimal rEALAMOUNT) {
		REALAMOUNT = rEALAMOUNT;
	}
	public BigDecimal getBALANCE() {
		return BALANCE;
	}
	public void setBALANCE(BigDecimal bALANCE) {
		BALANCE = bALANCE;
	}
	public String getIOPER() {
		return IOPER;
	}
	public void setIOPER(String iOPER) {
		IOPER = iOPER;
	}
	public String getINOTE() {
		return INOTE;
	}
	public void setINOTE(String iNOTE) {
		INOTE = iNOTE;
	}
	public Date getIDATE() {
		return IDATE;
	}
	public void setIDATE(Date iDATE) {
		IDATE = iDATE;
	}
	public String getVEROPER() {
		return VEROPER;
	}
	public void setVEROPER(String vEROPER) {
		VEROPER = vEROPER;
	}
	public String getVERNOTE() {
		return VERNOTE;
	}
	public void setVERNOTE(String vERNOTE) {
		VERNOTE = vERNOTE;
	}
	public Date getVERDATE() {
		return VERDATE;
	}
	public void setVERDATE(Date vERDATE) {
		VERDATE = vERDATE;
	}
	public String getAUTHOPER() {
		return AUTHOPER;
	}
	public void setAUTHOPER(String aUTHOPER) {
		AUTHOPER = aUTHOPER;
	}
	public Date getAUTHDATE() {
		return AUTHDATE;
	}
	public void setAUTHDATE(Date aUTHDATE) {
		AUTHDATE = aUTHDATE;
	}
	public String getSENDFLAG() {
		return SENDFLAG;
	}
	public void setSENDFLAG(String sENDFLAG) {
		SENDFLAG = sENDFLAG;
	}
	public String getSENDOPER() {
		return SENDOPER;
	}
	public void setSENDOPER(String sENDOPER) {
		SENDOPER = sENDOPER;
	}
	public Date getSENDDATE() {
		return SENDDATE;
	}
	public void setSENDDATE(Date sENDDATE) {
		SENDDATE = sENDDATE;
	}
	public String getSENDMSG() {
		return SENDMSG;
	}
	public void setSENDMSG(String sENDMSG) {
		SENDMSG = sENDMSG;
	}
	public String getSRLENGTH() {
		return SRLENGTH;
	}
	public void setSRLENGTH(String sRLENGTH) {
		SRLENGTH = sRLENGTH;
	}
	public String getSRPRCSCD() {
		return SRPRCSCD;
	}
	public void setSRPRCSCD(String sRPRCSCD) {
		SRPRCSCD = sRPRCSCD;
	}
	public String getSRRETUCD() {
		return SRRETUCD;
	}
	public void setSRRETUCD(String sRRETUCD) {
		SRRETUCD = sRRETUCD;
	}
	public String getSRTRANDT() {
		return SRTRANDT;
	}
	public void setSRTRANDT(String sRTRANDT) {
		SRTRANDT = sRTRANDT;
	}
	public String getSRTRANSQ() {
		return SRTRANSQ;
	}
	public void setSRTRANSQ(String sRTRANSQ) {
		SRTRANSQ = sRTRANSQ;
	}
	public String getSRMAINSEQ() {
		return SRMAINSEQ;
	}
	public void setSRMAINSEQ(String sRMAINSEQ) {
		SRMAINSEQ = sRMAINSEQ;
	}
	public String getSRTRANSSQE() {
		return SRTRANSSQE;
	}
	public void setSRTRANSSQE(String sRTRANSSQE) {
		SRTRANSSQE = sRTRANSSQE;
	}
	public String getREVERSEMSG() {
		return REVERSEMSG;
	}
	public void setREVERSEMSG(String rEVERSEMSG) {
		REVERSEMSG = rEVERSEMSG;
	}
	public String getRRLENGTH() {
		return RRLENGTH;
	}
	public void setRRLENGTH(String rRLENGTH) {
		RRLENGTH = rRLENGTH;
	}
	public String getRRPRCSCD() {
		return RRPRCSCD;
	}
	public void setRRPRCSCD(String rRPRCSCD) {
		RRPRCSCD = rRPRCSCD;
	}
	public String getRRRETUCD() {
		return RRRETUCD;
	}
	public void setRRRETUCD(String rRRETUCD) {
		RRRETUCD = rRRETUCD;
	}
	public String getQUERYMSG() {
		return QUERYMSG;
	}
	public void setQUERYMSG(String qUERYMSG) {
		QUERYMSG = qUERYMSG;
	}
	public String getQRLENGTH() {
		return QRLENGTH;
	}
	public void setQRLENGTH(String qRLENGTH) {
		QRLENGTH = qRLENGTH;
	}
	public String getQRPRCSCD() {
		return QRPRCSCD;
	}
	public void setQRPRCSCD(String qRPRCSCD) {
		QRPRCSCD = qRPRCSCD;
	}
	public String getQRRETUCD() {
		return QRRETUCD;
	}
	public void setQRRETUCD(String qRRETUCD) {
		QRRETUCD = qRRETUCD;
	}
	public String getQRSTATUS() {
		return QRSTATUS;
	}
	public void setQRSTATUS(String qRSTATUS) {
		QRSTATUS = qRSTATUS;
	}
	public Date getQRTIME() {
		return QRTIME;
	}
	public void setQRTIME(Date qRTIME) {
		QRTIME = qRTIME;
	}
	public String getCHECKFLAG() {
		return CHECKFLAG;
	}
	public void setCHECKFLAG(String cHECKFLAG) {
		CHECKFLAG = cHECKFLAG;
	}
	public String getCHECKIOPER() {
		return CHECKIOPER;
	}
	public void setCHECKIOPER(String cHECKIOPER) {
		CHECKIOPER = cHECKIOPER;
	}
	public String getCHECKINOTE() {
		return CHECKINOTE;
	}
	public void setCHECKINOTE(String cHECKINOTE) {
		CHECKINOTE = cHECKINOTE;
	}
	public Date getCHECKIDATE() {
		return CHECKIDATE;
	}
	public void setCHECKIDATE(Date cHECKIDATE) {
		CHECKIDATE = cHECKIDATE;
	}
	public String getCHECKVOPER() {
		return CHECKVOPER;
	}
	public void setCHECKVOPER(String cHECKVOPER) {
		CHECKVOPER = cHECKVOPER;
	}
	public String getCHECKVNOTE() {
		return CHECKVNOTE;
	}
	public void setCHECKVNOTE(String cHECKVNOTE) {
		CHECKVNOTE = cHECKVNOTE;
	}
	public Date getCHECKVDATE() {
		return CHECKVDATE;
	}
	public void setCHECKVDATE(Date cHECKVDATE) {
		CHECKVDATE = cHECKVDATE;
	}
	public String getFEDEALNO() {
		return FEDEALNO;
	}
	public void setFEDEALNO(String fEDEALNO) {
		FEDEALNO = fEDEALNO;
	}
	public String getSERVER() {
		return SERVER;
	}
	public void setSERVER(String sERVER) {
		SERVER = sERVER;
	}
	public String getSUMSN() {
		return SUMSN;
	}
	public void setSUMSN(String sUMSN) {
		SUMSN = sUMSN;
	}
	public Integer getSUMCOUNT() {
		return SUMCOUNT;
	}
	public void setSUMCOUNT(Integer sUMCOUNT) {
		SUMCOUNT = sUMCOUNT;
	}
	public String getReSendFlg() {
		return reSendFlg;
	}
	public void setReSendFlg(String reSendFlg) {
		this.reSendFlg = reSendFlg;
	}
    
    public String getHostIntfCode() {
		return hostIntfCode;
	}
	public void setHostIntfCode(String hostIntfCode) {
		this.hostIntfCode = hostIntfCode;
	}
	public void setTeller(String teller) {
		this.teller = teller;
	}
	public String getTeller() {
		return teller;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setBranchNo(String branchNo) {
		this.branchNo = branchNo;
	}
	public String getBranchNo() {
		return branchNo;
	}
}
