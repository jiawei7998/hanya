package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SlNostBean implements Serializable{
	private static final long serialVersionUID = 1L;
	//机构分支
	private String br;
	//清算账户
    private String nos;
    //币种
    private String ccy;
    //成本中心
    private String cost;

    private String genledgno;
    //交易对手
    private String cust;

    private BigDecimal acctbal;

    private BigDecimal amtbalprev;
    //最后维护日期
    private Date lstmntdte;
    

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getNos() {
        return nos;
    }

    public void setNos(String nos) {
        this.nos = nos == null ? null : nos.trim();
    }
    
    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost == null ? null : cost.trim();
    }

    public String getGenledgno() {
        return genledgno;
    }

    public void setGenledgno(String genledgno) {
        this.genledgno = genledgno == null ? null : genledgno.trim();
    }

    public String getCust() {
        return cust;
    }

    public void setCust(String cust) {
        this.cust = cust == null ? null : cust.trim();
    }

    public BigDecimal getAcctbal() {
        return acctbal;
    }

    public void setAcctbal(BigDecimal acctbal) {
        this.acctbal = acctbal;
    }

    public BigDecimal getAmtbalprev() {
        return amtbalprev;
    }

    public void setAmtbalprev(BigDecimal amtbalprev) {
        this.amtbalprev = amtbalprev;
    }

    public Date getLstmntdte() {
        return lstmntdte;
    }

    public void setLstmntdte(Date lstmntdte) {
        this.lstmntdte = lstmntdte;
    }

}
