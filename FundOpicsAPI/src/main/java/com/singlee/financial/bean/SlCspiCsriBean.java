package com.singlee.financial.bean;

import java.io.Serializable;

public class SlCspiCsriBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5552695527396146264L;
	// 分支
	private String br;
	// 交易对手
	private String cno;
	// 交割收款标识
	private String delrecind;
	// 产品代码
	private String product;
	// 类型
	private String type;
	// 币种
	private String ccy;
	// 付款币种
	private String ctrccy;
	// 清算方式
	private String smeans;
	// 清算账户
	private String sacct;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getDelrecind() {
		return delrecind;
	}

	public void setDelrecind(String delrecind) {
		this.delrecind = delrecind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSmeans() {
		return smeans;
	}

	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}

	public String getSacct() {
		return sacct;
	}

	public void setSacct(String sacct) {
		this.sacct = sacct;
	}

	public String getCtrccy() {
		return ctrccy;
	}

	public void setCtrccy(String ctrccy) {
		this.ctrccy = ctrccy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
