package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SlPortBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2373069092723542252L;

	/**
	 * 部门
	 */
	private String br;
	/**
	 * 投资组合
	 */
	private String portfolio;
	/**
	 * 投资组合描述
	 */
	private String portdesc;
	/**
	 * 最后修改时间
	 */
	private Date lstmntdte;
	/**
	 * 成本中心
	 */
	private String cost;
	/**
	 * 备注1
	 */
	private String text1;
	/**
	 * 备注2
	 */
	private String text2;
	/**
	 * 日期1
	 */
	private Date date1;
	/**
	 * 日期2
	 */
	private Date date2;
	/**
	 * 金额1
	 */
	private BigDecimal amount1;
	/**
	 * 金额2
	 */
	private BigDecimal amount2;
	/**
	 * 更改次数
	 */
	private String updateCounter;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(String portfolio) {
		this.portfolio = portfolio;
	}

	public String getPortdesc() {
		return portdesc;
	}

	public void setPortdesc(String portdesc) {
		this.portdesc = portdesc;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	public BigDecimal getAmount1() {
		return amount1;
	}

	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}

	public BigDecimal getAmount2() {
		return amount2;
	}

	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}

	public String getUpdateCounter() {
		return updateCounter;
	}

	public void setUpdateCounter(String updateCounter) {
		this.updateCounter = updateCounter;
	}
}
