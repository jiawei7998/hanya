package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class HsLimiltBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		//非信贷额度编号
		private String  limitNumber;
		//用信金额
		private BigDecimal limitAmont;
		//非信贷交易对手编号
		private String limitCust;
		//推送日期
		private String pushDate;
		//用信操作人员编号
		private String limitTrader;
		//业务品种
		private String limitProductType;
		//额度状态 0-未占用 1-已占用 2-已冲销
		private String status;
		//前置接入CFETS成交单编号
		private String ticketId;
		//手工录入成交编号
		private String contractId;
		//OPICS成交编号
		private String dealno;
		//折算金额(用于外汇模块)
		private String limitfxAmount;
		//交易员
		private String trad;
		//起息日
		private String vDate;
		//到期日
		private String mDate;
		//交易金额
		private BigDecimal amt;
		//剩余可释放金额
		private BigDecimal limitRemainAmt;
		//释放信贷金额
		private BigDecimal limitRevaseAmt;
		//对手全称
		private String cname;
		//债券代码
		private String secid;
		
		
		
		public String getSecid() {
			return secid;
		}
		public void setSecid(String secid) {
			this.secid = secid;
		}
		public BigDecimal getLimitRemainAmt() {
			return limitRemainAmt;
		}
		public void setLimitRemainAmt(BigDecimal limitRemainAmt) {
			this.limitRemainAmt = limitRemainAmt;
		}
		public BigDecimal getLimitRevaseAmt() {
			return limitRevaseAmt;
		}
		public void setLimitRevaseAmt(BigDecimal limitRevaseAmt) {
			this.limitRevaseAmt = limitRevaseAmt;
		}
		public String getCname() {
			return cname;
		}
		public void setCname(String cname) {
			this.cname = cname;
		}
		public BigDecimal getAmt() {
			return amt;
		}
		public void setAmt(BigDecimal amt) {
			this.amt = amt;
		}
		public String getvDate() {
			return vDate;
		}
		public void setvDate(String vDate) {
			this.vDate = vDate;
		}
		public String getmDate() {
			return mDate;
		}
		public void setmDate(String mDate) {
			this.mDate = mDate;
		}
		public String getLimitTrader() {
			return limitTrader;
		}
		public void setLimitTrader(String limitTrader) {
			this.limitTrader = limitTrader;
		}
		public String getTrad() {
			return trad;
		}
		public void setTrad(String trad) {
			this.trad = trad;
		}
		public String getLimitNumber() {
			return limitNumber;
		}
		public void setLimitNumber(String limitNumber) {
			this.limitNumber = limitNumber;
		}
		public BigDecimal getLimitAmont() {
			return limitAmont;
		}
		public void setLimitAmont(BigDecimal limitAmont) {
			this.limitAmont = limitAmont;
		}
		public String getLimitCust() {
			return limitCust;
		}
		public void setLimitCust(String limitCust) {
			this.limitCust = limitCust;
		}
		public String getPushDate() {
			return pushDate;
		}
		public void setPushDate(String pushDate) {
			this.pushDate = pushDate;
		}
		
		public String getLimitProductType() {
			return limitProductType;
		}
		public void setLimitProductType(String limitProductType) {
			this.limitProductType = limitProductType;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getTicketId() {
			return ticketId;
		}
		public void setTicketId(String ticketId) {
			this.ticketId = ticketId;
		}
		public String getContractId() {
			return contractId;
		}
		public void setContractId(String contractId) {
			this.contractId = contractId;
		}
		public String getDealno() {
			return dealno;
		}
		public void setDealno(String dealno) {
			this.dealno = dealno;
		}
		public String getLimitfxAmount() {
			return limitfxAmount;
		}
		public void setLimitfxAmount(String limitfxAmount) {
			this.limitfxAmount = limitfxAmount;
		}
		
		
	
	

}
