package com.singlee.financial.bean;

import java.io.Serializable;
/**
 *	G33 利率重新定价风险情况表  实体
 */
public class SlReportG33Bean implements Serializable {
	
	private static final long serialVersionUID = 2848141541266196488L;
	
	private String tableid; 
	private String br; 
	private String lineid; 
	private String rowsid; 
	private String ccy; 
	private Double amt1; 
	private Double amt2; 
	private Double amt3; 
	private Double amt4; 
	private Double amt5; 
	private Double amt6; 
	private Double amt7; 
	private Double amt8; 
	private Double amt9; 
	private Double amt10; 
	private Double amt11; 
	private Double amt12; 
	private Double amt13; 
	private String rpdate;
	
	
	public String getTableid() {
		return tableid;
	}


	public void setTableid(String tableid) {
		this.tableid = tableid;
	}


	public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getLineid() {
		return lineid;
	}


	public void setLineid(String lineid) {
		this.lineid = lineid;
	}


	public String getRowsid() {
		return rowsid;
	}


	public void setRowsid(String rowsid) {
		this.rowsid = rowsid;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public Double getAmt1() {
		return amt1;
	}


	public void setAmt1(Double amt1) {
		this.amt1 = amt1;
	}


	public Double getAmt2() {
		return amt2;
	}


	public void setAmt2(Double amt2) {
		this.amt2 = amt2;
	}


	public Double getAmt3() {
		return amt3;
	}


	public void setAmt3(Double amt3) {
		this.amt3 = amt3;
	}


	public Double getAmt4() {
		return amt4;
	}


	public void setAmt4(Double amt4) {
		this.amt4 = amt4;
	}


	public Double getAmt5() {
		return amt5;
	}


	public void setAmt5(Double amt5) {
		this.amt5 = amt5;
	}


	public Double getAmt6() {
		return amt6;
	}


	public void setAmt6(Double amt6) {
		this.amt6 = amt6;
	}


	public Double getAmt7() {
		return amt7;
	}


	public void setAmt7(Double amt7) {
		this.amt7 = amt7;
	}


	public Double getAmt8() {
		return amt8;
	}


	public void setAmt8(Double amt8) {
		this.amt8 = amt8;
	}


	public Double getAmt9() {
		return amt9;
	}


	public void setAmt9(Double amt9) {
		this.amt9 = amt9;
	}


	public Double getAmt10() {
		return amt10;
	}


	public void setAmt10(Double amt10) {
		this.amt10 = amt10;
	}


	public Double getAmt11() {
		return amt11;
	}


	public void setAmt11(Double amt11) {
		this.amt11 = amt11;
	}


	public Double getAmt12() {
		return amt12;
	}


	public void setAmt12(Double amt12) {
		this.amt12 = amt12;
	}


	public Double getAmt13() {
		return amt13;
	}


	public void setAmt13(Double amt13) {
		this.amt13 = amt13;
	}


	public String getRpdate() {
		return rpdate;
	}


	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}


	@Override
	public String toString() {
		return "SlReportG33Bean [tableid=" + tableid + ", br=" + br
				+ ", lineid=" + lineid + ", rowsid=" + rowsid + ", ccy=" + ccy
				+ ", amt1=" + amt1 + ", amt2=" + amt2 + ", amt3=" + amt3
				+ ", amt4=" + amt4 + ", amt5=" + amt5 + ", amt6=" + amt6
				+ ", amt7=" + amt7 + ", amt8=" + amt8 + ", amt9=" + amt9
				+ ", amt10=" + amt10 + ", amt11=" + amt11 + ", amt12=" + amt12
				+ ", amt13=" + amt13 + ", rpdate=" + rpdate + "]";
	}
	
	
	
}
