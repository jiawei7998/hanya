package com.singlee.financial.bean;

import com.singlee.financial.pojo.FxSptFwdBean;

public class SlFxSptFwdBean extends FxSptFwdBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5179009562302425278L;

	/**
	 * INTH 头表信息
	 */
	private SlInthBean inthBean;
	/**
	 * OPICS拓展属性
	 */
	private SlExternalBean externalBean;

	/**
	 * 外汇即远期初始化
	 * 
	 * @param br
	 *            业务部门分支
	 * @param tag
	 *            标识
	 * @param detail
	 *            细节
	 * @note
	 * 
	 *       1、交易导入 TAG="FXDE" , DETAIL="IFXD"
	 * 
	 *       2、交易冲销 TAG="FXDR", DETAIL="IFXD"
	 * 
	 */
	public SlFxSptFwdBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.FXD.SEQ);
			inthBean.setInoutind(SlDealModule.FXD.INOUTIND);
			inthBean.setPriority(SlDealModule.FXD.PRIORITY);
			inthBean.setStatcode(SlDealModule.FXD.STATCODE);
			inthBean.setSyst(SlDealModule.FXD.SYST);
			inthBean.setIoper(SlDealModule.FXD.IOPER);
		}
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public SlExternalBean getExternalBean() {
		return externalBean;
	}

	public void setExternalBean(SlExternalBean externalBean) {
		this.externalBean = externalBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
