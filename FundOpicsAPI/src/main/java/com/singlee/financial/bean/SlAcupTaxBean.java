package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.List;

/**
 * SL_ACUP_TAX
 * @author zhengfl
 *
 */
public class SlAcupTaxBean implements Serializable {

	private static final long serialVersionUID = -9029694466053662263L;
	
	private String setno;//套号
	private String seqno;//序号
	private String br;//部门
	private String product;//产品代码
	private String type;//产品类型
	private String argno;//
	private String dealno;//交易号
	private String seq;//顺序号
	private String glno;//opics科目
	private String costcent;//成本中心
	private String ccy;//币种
	private String ccycode;//核心币种
	private String beind;
	private String code;//会计类型
	private String qual;
	private String effdate;//账务日期
	private String postdate;//送账日期
	private String cmne;//交易对手
	private String drcrind;//借贷方向
	private String amount;//金额
	private String smeans;
	private String sacct;
	private String descr;//opics备注
	private String aglno;//主机科目
	private String intglno;
	private String subject;//主机帐号
	private String subbr;
	private String reversal;
	private String account;
	private String voucher;
	private String remark;
	private String errcode;
	private String errmsg;//错误信息
	private String oper;
	private String note;
	private String retcode;
	private String retmsg;
	private String dealflag;
	private String inputtime;
	private String tradflag;//冲销标识
	private String invprtflag;
	private String tpfeat;
	private String incomeflag;
	private String taxsep;
	private String cidttype;
	private String taxidttype;
	private String discountflag;
	private String cmbbak1;
	private String cmbbak2;
	private String cmbbak3;
	private String cmbbak4;
	private String cmbbak5;
	List<String> brList;
	
	private String cno;
	
	
	//添加20191015
	private String tenor;//期限
	private String dealDate;//交易日
	private String vDate;//交割日
	
	private String  startDate;
	private String endDate;
	//总账添加字段
	private String settG01;//赦税免税标志
	//全天交易查询 字段添加
	private String port;//投资组合
	private String ps;//买卖方向
	private String ctrccy;//币种2
	private String ctramt;//币种2金额
	private String mdate;//到期日
	private String ioper;//经办人
	private String voper;//复核人
	private String payoper;//添加清算路径人
	private String payauthoper;//路径授权人
	private String revoper;//冲销人
	private String ccysmeans;//清算方式
	private String ccysacct;//清算路径
	
	
	public String getSetno() {
		return setno;
	}
	public void setSetno(String setno) {
		this.setno = setno;
	}
	public String getSeqno() {
		return seqno;
	}
	public void setSeqno(String seqno) {
		this.seqno = seqno;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getArgno() {
		return argno;
	}
	public void setArgno(String argno) {
		this.argno = argno;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getGlno() {
		return glno;
	}
	public void setGlno(String glno) {
		this.glno = glno;
	}
	public String getCostcent() {
		return costcent;
	}
	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getCcycode() {
		return ccycode;
	}
	public void setCcycode(String ccycode) {
		this.ccycode = ccycode;
	}
	public String getBeind() {
		return beind;
	}
	public void setBeind(String beind) {
		this.beind = beind;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getQual() {
		return qual;
	}
	public void setQual(String qual) {
		this.qual = qual;
	}
	public String getEffdate() {
		return effdate;
	}
	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}
	public String getPostdate() {
		return postdate;
	}
	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	public String getDrcrind() {
		return drcrind;
	}
	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSmeans() {
		return smeans;
	}
	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}
	public String getSacct() {
		return sacct;
	}
	public void setSacct(String sacct) {
		this.sacct = sacct;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getAglno() {
		return aglno;
	}
	public void setAglno(String aglno) {
		this.aglno = aglno;
	}
	public String getIntglno() {
		return intglno;
	}
	public void setIntglno(String intglno) {
		this.intglno = intglno;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSubbr() {
		return subbr;
	}
	public void setSubbr(String subbr) {
		this.subbr = subbr;
	}
	public String getReversal() {
		return reversal;
	}
	public void setReversal(String reversal) {
		this.reversal = reversal;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getVoucher() {
		return voucher;
	}
	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getErrcode() {
		return errcode;
	}
	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}
	public String getRetmsg() {
		return retmsg;
	}
	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}
	public String getDealflag() {
		return dealflag;
	}
	public void setDealflag(String dealflag) {
		this.dealflag = dealflag;
	}
	public String getInputtime() {
		return inputtime;
	}
	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
	public String getTradflag() {
		return tradflag;
	}
	public void setTradflag(String tradflag) {
		this.tradflag = tradflag;
	}
	public String getInvprtflag() {
		return invprtflag;
	}
	public void setInvprtflag(String invprtflag) {
		this.invprtflag = invprtflag;
	}
	public String getTpfeat() {
		return tpfeat;
	}
	public void setTpfeat(String tpfeat) {
		this.tpfeat = tpfeat;
	}
	public String getIncomeflag() {
		return incomeflag;
	}
	public void setIncomeflag(String incomeflag) {
		this.incomeflag = incomeflag;
	}
	public String getTaxsep() {
		return taxsep;
	}
	public void setTaxsep(String taxsep) {
		this.taxsep = taxsep;
	}
	public String getCidttype() {
		return cidttype;
	}
	public void setCidttype(String cidttype) {
		this.cidttype = cidttype;
	}
	public String getTaxidttype() {
		return taxidttype;
	}
	public void setTaxidttype(String taxidttype) {
		this.taxidttype = taxidttype;
	}
	public String getDiscountflag() {
		return discountflag;
	}
	public void setDiscountflag(String discountflag) {
		this.discountflag = discountflag;
	}
	public String getCmbbak1() {
		return cmbbak1;
	}
	public void setCmbbak1(String cmbbak1) {
		this.cmbbak1 = cmbbak1;
	}
	public String getCmbbak2() {
		return cmbbak2;
	}
	public void setCmbbak2(String cmbbak2) {
		this.cmbbak2 = cmbbak2;
	}
	public String getCmbbak3() {
		return cmbbak3;
	}
	public void setCmbbak3(String cmbbak3) {
		this.cmbbak3 = cmbbak3;
	}
	public String getCmbbak4() {
		return cmbbak4;
	}
	public void setCmbbak4(String cmbbak4) {
		this.cmbbak4 = cmbbak4;
	}
	public String getCmbbak5() {
		return cmbbak5;
	}
	public void setCmbbak5(String cmbbak5) {
		this.cmbbak5 = cmbbak5;
	}
	public List<String> getBrList() {
		return brList;
	}
	public void setBrList(List<String> brList) {
		this.brList = brList;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getvDate() {
		return vDate;
	}
	public void setvDate(String vDate) {
		this.vDate = vDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String getSettG01() {
		return settG01;
	}
	public void setSettG01(String settG01) {
		this.settG01 = settG01;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public String getCtrccy() {
		return ctrccy;
	}
	public void setCtrccy(String ctrccy) {
		this.ctrccy = ctrccy;
	}
	public String getCtramt() {
		return ctramt;
	}
	public void setCtramt(String ctramt) {
		this.ctramt = ctramt;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getIoper() {
		return ioper;
	}
	public void setIoper(String ioper) {
		this.ioper = ioper;
	}
	public String getVoper() {
		return voper;
	}
	public void setVoper(String voper) {
		this.voper = voper;
	}
	public String getPayoper() {
		return payoper;
	}
	public void setPayoper(String payoper) {
		this.payoper = payoper;
	}
	public String getPayauthoper() {
		return payauthoper;
	}
	public void setPayauthoper(String payauthoper) {
		this.payauthoper = payauthoper;
	}
	public String getRevoper() {
		return revoper;
	}
	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}
	public String getCcysmeans() {
		return ccysmeans;
	}
	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans;
	}
	public String getCcysacct() {
		return ccysacct;
	}
	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}
		
}