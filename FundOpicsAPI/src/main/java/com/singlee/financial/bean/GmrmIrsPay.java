package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 
 * @author LIJ
 *
 */
public class GmrmIrsPay implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 8257438246129948652L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 交易编号
     */
    private String dealId;

    /**
     * 支付序列编号，相同DEAL_ID的每条记录不能重复
     */
    private String scheduleId;

    /**
     * 支付日期 YYYYMMDD
     */
    private String payDate;

    /**
     * 接收支付标识，1、Pay支付端  2、Receive接收端
     */
    private String payRecFlag;

    /**
     * 计息区间开始日期 YYYYMMDD
     */
    private String accrStartDate;

    /**
     * 计息区间结束日期 YYYYMMDD
     */
    private String accrEndDate;

    /**
     * 本金金额
     */
    private BigDecimal notional;

    /**
     * 固定利率/点差(单位%)
     */
    private BigDecimal rate;

    /**
     * 参考指标
     */
    private String refIndex;

    /**
     * 计息区间计息金额
     */
    private BigDecimal payAmount;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期 YYYYMMDD
     * @return BATCH_DATE 批量日期 YYYYMMDD
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期 YYYYMMDD
     * @param batchDate 批量日期 YYYYMMDD
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 交易编号
     * @return DEAL_ID 交易编号
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 交易编号
     * @param dealId 交易编号
     */
    public void setDealId(String dealId) {
        this.dealId = dealId == null ? null : dealId.trim();
    }

    /**
     * 支付序列编号，相同DEAL_ID的每条记录不能重复
     * @return SCHEDULE_ID 支付序列编号，相同DEAL_ID的每条记录不能重复
     */
    public String getScheduleId() {
        return scheduleId;
    }

    /**
     * 支付序列编号，相同DEAL_ID的每条记录不能重复
     * @param scheduleId 支付序列编号，相同DEAL_ID的每条记录不能重复
     */
    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId == null ? null : scheduleId.trim();
    }

    /**
     * 支付日期 YYYYMMDD
     * @return PAY_DATE 支付日期 YYYYMMDD
     */
    public String getPayDate() {
        return payDate;
    }

    /**
     * 支付日期 YYYYMMDD
     * @param payDate 支付日期 YYYYMMDD
     */
    public void setPayDate(String payDate) {
        this.payDate = payDate == null ? null : payDate.trim();
    }

    /**
     * 接收支付标识，1、Pay支付端  2、Receive接收端
     * @return PAY_REC_FLAG 接收支付标识，1、Pay支付端  2、Receive接收端
     */
    public String getPayRecFlag() {
        return payRecFlag;
    }

    /**
     * 接收支付标识，1、Pay支付端  2、Receive接收端
     * @param payRecFlag 接收支付标识，1、Pay支付端  2、Receive接收端
     */
    public void setPayRecFlag(String payRecFlag) {
        this.payRecFlag = payRecFlag == null ? null : payRecFlag.trim();
    }

    /**
     * 计息区间开始日期 YYYYMMDD
     * @return ACCR_START_DATE 计息区间开始日期 YYYYMMDD
     */
    public String getAccrStartDate() {
        return accrStartDate;
    }

    /**
     * 计息区间开始日期 YYYYMMDD
     * @param accrStartDate 计息区间开始日期 YYYYMMDD
     */
    public void setAccrStartDate(String accrStartDate) {
        this.accrStartDate = accrStartDate == null ? null : accrStartDate.trim();
    }

    /**
     * 计息区间结束日期 YYYYMMDD
     * @return ACCR_END_DATE 计息区间结束日期 YYYYMMDD
     */
    public String getAccrEndDate() {
        return accrEndDate;
    }

    /**
     * 计息区间结束日期 YYYYMMDD
     * @param accrEndDate 计息区间结束日期 YYYYMMDD
     */
    public void setAccrEndDate(String accrEndDate) {
        this.accrEndDate = accrEndDate == null ? null : accrEndDate.trim();
    }

    /**
     * 本金金额
     * @return NOTIONAL 本金金额
     */
    public BigDecimal getNotional() {
        return notional;
    }

    /**
     * 本金金额
     * @param notional 本金金额
     */
    public void setNotional(BigDecimal notional) {
        this.notional = notional;
    }

    /**
     * 固定利率/点差(单位%)
     * @return RATE 固定利率/点差(单位%)
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * 固定利率/点差(单位%)
     * @param rate 固定利率/点差(单位%)
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * 参考指标
     * @return REF_INDEX 参考指标
     */
    public String getRefIndex() {
        return refIndex;
    }

    /**
     * 参考指标
     * @param refIndex 参考指标
     */
    public void setRefIndex(String refIndex) {
        this.refIndex = refIndex == null ? null : refIndex.trim();
    }

    /**
     * 计息区间计息金额
     * @return PAY_AMOUNT 计息区间计息金额
     */
    public BigDecimal getPayAmount() {
        return payAmount;
    }

    /**
     * 计息区间计息金额
     * @param payAmount 计息区间计息金额
     */
    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    /**
     * 机构号
     * @return BRANCH_ID 机构号
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * 机构号
     * @param branchId 机构号
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }
}