package com.singlee.financial.bean;



import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 资金交易公允价值监测报表
 * @author LIJ
 *
 */
public class SlReportZjgyjz implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6484457585717243577L;

	/**
     * 部门
     */
    private String br;

    /**
     * 交易种类
     */
    private String dealtype;

    /**
     * 交易对手
     */
    private String cno;

    /**
     * 交易金额
     */
    private BigDecimal amount;

    /**
     * 币种（货币对）
     */
    private String ccy;

    /**
     * 交易日
     */
    private String dealdate;

    /**
     * 起息日
     */
    private String vdate;

    /**
     * 到期日
     */
    private String mdate;

    /**
     * 期限
     */
    private BigDecimal term;

    /**
     * 交易价格（利率/汇率）
     */
    private BigDecimal rate;

    /**
     * 公允价值
     */
    private BigDecimal mktval;

    /**
     * 差异
     */
    private BigDecimal differ;

    /**
     * 备注
     */
    private String note;

    /**
     * 序号
     */
    private BigDecimal seq;
    
    

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}


	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	
	public BigDecimal getTerm() {
		return term;
	}

	public void setTerm(BigDecimal term) {
		this.term = term;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getMktval() {
		return mktval;
	}

	public void setMktval(BigDecimal mktval) {
		this.mktval = mktval;
	}

	public BigDecimal getDiffer() {
		return differ;
	}

	public void setDiffer(BigDecimal differ) {
		this.differ = differ;
	}


	public BigDecimal getSeq() {
		return seq;
	}

	public void setSeq(BigDecimal seq) {
		this.seq = seq;
	}

	public String getDealtype() {
		return dealtype;
	}

	public void setDealtype(String dealtype) {
		this.dealtype = dealtype;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getDealdate() {
		return dealdate;
	}

	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getMdate() {
		return mdate;
	}

	public void setMdate(String mdate) {
		this.mdate = mdate;
	}

    
    
    
}