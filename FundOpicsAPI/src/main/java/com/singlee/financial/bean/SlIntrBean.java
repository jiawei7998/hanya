/**
 * Project Name:FundOpicsAPI
 * File Name:SlSecurityLendingBean.java
 * Package Name:com.singlee.financial.bean
 *
*/


package com.singlee.financial.bean;



import java.math.BigDecimal;
import java.util.Date;


import com.singlee.financial.pojo.SecurityLendingBean;


/**
 * Opics系统INTR表
 */
public class SlIntrBean extends SecurityLendingBean{


     private static final long serialVersionUID = -3339509667342727579L;
     /**
      * INTH 头表信息
      */
     private SlInthBean inthBean;
     /**
      * OPICS拓展属性
      */
     private SlExternalBean externalBean;
     /**
      * INTR表初始化
      * 
      * @param br
      *            业务部门分支
      * @param tag
      *            标识
      * @param detail
      *            细节
      * 
      */
     public SlIntrBean(String br, String server, String tag, String detail) {
          if (null == inthBean) {
               inthBean = new SlInthBean();
               inthBean.setBr(br);
               inthBean.setServer(server);
               inthBean.setTag(tag);
               inthBean.setDetail(detail);
               inthBean.setSeq(SlDealModule.INTR.SEQ);
               inthBean.setInoutind(SlDealModule.INTR.INOUTIND);
               inthBean.setPriority(SlDealModule.INTR.PRIORITY);
               inthBean.setStatcode(SlDealModule.INTR.STATCODE);
               inthBean.setSyst(SlDealModule.INTR.SYST);
               inthBean.setIoper(SlDealModule.INTR.IOPER);
          }
     }
     
     /**
     * 机构分支
     */
    private String br;
    
    /**
     * 服务标识
     */
    private String server;
    
    /**
     * 审批流水号
     */
    private String fedealno;
    
     /**
     * 序号
     */
    private String seq;


    /**
     * I-导入 O-导出
     */
    private String inoutind;


    /**
     * Opics系统交易单号
     */
    private String dealno;


    /**
     * 头寸调拨币种
     */
    private String ccy;


    /**
     * 调拨交割日期
     */
    private Date vdate;
    
    /**
     * 付款账号
     */
    private String payacct;
    
    /**
     * 收款账号
     */
    private String recacct;
    
    /**
     * 交割金额
     */
    private BigDecimal trfamt;


    /**
     * 是否自动复核
     */
    private String autoverind;


    /**
     * 禁止自动付款
     */
    private String supppayind;


    /**
     * 
     */
    private String sr;
     
     
     public SlInthBean getInthBean() {
          return inthBean;
     }
     public void setInthBean(SlInthBean inthBean) {
          this.inthBean = inthBean;
     }
     public SlExternalBean getExternalBean() {
          return externalBean;
     }
     public void setExternalBean(SlExternalBean externalBean) {
          this.externalBean = externalBean;
     }
     public String getBr() {
          return br;
     }
     public void setBr(String br) {
          this.br = br;
     }
     public String getServer() {
          return server;
     }
     public void setServer(String server) {
          this.server = server;
     }
     public String getFedealno() {
          return fedealno;
     }
     public void setFedealno(String fedealno) {
          this.fedealno = fedealno;
     }
     public String getSeq() {
          return seq;
     }
     public void setSeq(String seq) {
          this.seq = seq;
     }
     public String getInoutind() {
          return inoutind;
     }
     public void setInoutind(String inoutind) {
          this.inoutind = inoutind;
     }
     public String getDealno() {
          return dealno;
     }
     public void setDealno(String dealno) {
          this.dealno = dealno;
     }
     public String getCcy() {
          return ccy;
     }
     public void setCcy(String ccy) {
          this.ccy = ccy;
     }
     public Date getVdate() {
          return vdate;
     }
     public void setVdate(Date vdate) {
          this.vdate = vdate;
     }
     public String getPayacct() {
          return payacct;
     }
     public void setPayacct(String payacct) {
          this.payacct = payacct;
     }
     public String getRecacct() {
          return recacct;
     }
     public void setRecacct(String recacct) {
          this.recacct = recacct;
     }
     public BigDecimal getTrfamt() {
          return trfamt;
     }
     public void setTrfamt(BigDecimal trfamt) {
          this.trfamt = trfamt;
     }
     public String getAutoverind() {
          return autoverind;
     }
     public void setAutoverind(String autoverind) {
          this.autoverind = autoverind;
     }
     public String getSupppayind() {
          return supppayind;
     }
     public void setSupppayind(String supppayind) {
          this.supppayind = supppayind;
     }
     public String getSr() {
          return sr;
     }
     public void setSr(String sr) {
          this.sr = sr;
     }


}
