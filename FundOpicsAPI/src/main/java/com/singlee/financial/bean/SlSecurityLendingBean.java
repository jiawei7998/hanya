/**
 * Project Name:FundOpicsAPI
 * File Name:SlSecurityLendingBean.java
 * Package Name:com.singlee.financial.bean
 * Date:2018-7-26上午10:51:00
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.bean;


import java.math.BigDecimal;
import java.util.Date;

import com.singlee.financial.pojo.SecurityLendingBean;

/**
 * 债券借贷
 */
public class SlSecurityLendingBean extends SecurityLendingBean{

	
	/**
	 * serialVersionUID:TODO(用一句话描述这个变量表示什么).
	 * @since JDK 1.6
	 */
	private static final long serialVersionUID = -3339509667342727579L;
	/**
	 * INTH 头表信息
	 */
	private SlInthBean inthBean;
	/**
	 * OPICS拓展属性
	 */
	private SlExternalBean externalBean;
	/**
	 * 债券借贷初始化
	 * 
	 * @param br
	 *            业务部门分支
	 * @param tag
	 *            标识
	 * @param detail
	 *            细节
	 * @note
	 * 
	 *       1、交易导入 TAG="SLTE" , DETAIL="ISLD"
	 * 
	 *       2、交易冲销 TAG="SLTE", DETAIL="ISLD"
	 * 
	 */
	public SlSecurityLendingBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean();
			inthBean.setBr(br);
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.SECLEND.SEQ);
			inthBean.setInoutind(SlDealModule.SECLEND.INOUTIND);
			inthBean.setPriority(SlDealModule.SECLEND.PRIORITY);
			inthBean.setStatcode(SlDealModule.SECLEND.STATCODE);
			inthBean.setSyst(SlDealModule.SECLEND.SYST);
			inthBean.setIoper(SlDealModule.SECLEND.IOPER);
		}
	}
	
	private String dealseq;
	/**
	 * 交易指标
	 */
	private String dealind;
	/**
	 * 添加站立指令指示器 
	 */
	private String addsi;
	/**
	 * 计息基础
	 */
	private String basis;
	/**
	 * 现金成本中心 
	 */
	private String cashcost;
	/**
	 * 币种
	 */
	private String ccy;
	/**
	 * 抵押品代码 
	 */
	private String collateralcode;
	/**
	 * 期限
	 */
	private String tenor;
	/**
	 * 抵押品要求指示器 
	 */
	private String collrequiredind;
	/**
	 * 交易来源 
	 */
	private String dealscre;
	/**
	 * 交货类型
	 */
	private String delivtype;
	/**
	 * 费用金额 
	 */
	private BigDecimal feeamt;
	/**
	 * 费用产品类型
	 */
	private String feeProdtype;
	/**
	 * 费用产品
	 */
	private String feeProduct;
	/**
	 * 完全指定指示器 
	 */
	private String fullyassignind;
	/**
	 * 投资类型
	 */
	private String invtype;
	/**
	 * 到期日
	 */
	private Date maturityDate;
	/**
	 * 保存货币值指标 
	 */
	private String pmvind;
	/**
	 *  Proceed amount haircut 
	 */
	private BigDecimal proceedamthc;
	/**
	 * 数量
	 */
	private String qty;
	/**
	 * 托管单位
	 */
	private String safekeepacct;
	/**
	 * spread rate
	 * 
	 */
	private BigDecimal spreadrate;
	/**
	 * 替代指标 
	 */
	private String subind;
	/**
	 * 抑制现金指标 
	 */
	private String suppcashind;
	/**
	 * 禁止确认指示器 
	 */
	private String suppconfind;
	/**
	 * 抑制费用指示器 
	 */
	private String suppfeeind;
	/**
	 * 抑制安全移动指示器 
	 */
	private String supsecmoveind;
	/**
	 * 债券ISNB代码
	 */
	private String securityId;
	/**
	 * 净价
	 */
	private BigDecimal clprice;
	/**
	 * 全价
	 */
	private BigDecimal drprice;
	/**
	 * 面额
	 */
	private BigDecimal faceamt;
	
	
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public SlExternalBean getExternalBean() {
		return externalBean;
	}
	public void setExternalBean(SlExternalBean externalBean) {
		this.externalBean = externalBean;
	}
	public String getDealind() {
		return dealind;
	}
	public void setDealind(String dealind) {
		this.dealind = dealind;
	}
	public String getAddsi() {
		return addsi;
	}
	public void setAddsi(String addsi) {
		this.addsi = addsi;
	}
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	public String getCashcost() {
		return cashcost;
	}
	public void setCashcost(String cashcost) {
		this.cashcost = cashcost;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getCollateralcode() {
		return collateralcode;
	}
	public void setCollateralcode(String collateralcode) {
		this.collateralcode = collateralcode;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getCollrequiredind() {
		return collrequiredind;
	}
	public void setCollrequiredind(String collrequiredind) {
		this.collrequiredind = collrequiredind;
	}
	public String getDealscre() {
		return dealscre;
	}
	public void setDealscre(String dealscre) {
		this.dealscre = dealscre;
	}
	public String getDelivtype() {
		return delivtype;
	}
	public void setDelivtype(String delivtype) {
		this.delivtype = delivtype;
	}
	public BigDecimal getFeeamt() {
		return feeamt;
	}
	public void setFeeamt(BigDecimal feeamt) {
		this.feeamt = feeamt;
	}
	public String getFeeProdtype() {
		return feeProdtype;
	}
	public void setFeeProdtype(String feeProdtype) {
		this.feeProdtype = feeProdtype;
	}
	public String getFeeProduct() {
		return feeProduct;
	}
	public void setFeeProduct(String feeProduct) {
		this.feeProduct = feeProduct;
	}
	public String getFullyassignind() {
		return fullyassignind;
	}
	public void setFullyassignind(String fullyassignind) {
		this.fullyassignind = fullyassignind;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public Date getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}
	public String getPmvind() {
		return pmvind;
	}
	public void setPmvind(String pmvind) {
		this.pmvind = pmvind;
	}
	public BigDecimal getProceedamthc() {
		return proceedamthc;
	}
	public void setProceedamthc(BigDecimal proceedamthc) {
		this.proceedamthc = proceedamthc;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getSafekeepacct() {
		return safekeepacct;
	}
	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}
	public BigDecimal getSpreadrate() {
		return spreadrate;
	}
	public void setSpreadrate(BigDecimal spreadrate) {
		this.spreadrate = spreadrate;
	}
	public String getSubind() {
		return subind;
	}
	public void setSubind(String subind) {
		this.subind = subind;
	}
	public String getSuppcashind() {
		return suppcashind;
	}
	public void setSuppcashind(String suppcashind) {
		this.suppcashind = suppcashind;
	}
	public String getSuppconfind() {
		return suppconfind;
	}
	public void setSuppconfind(String suppconfind) {
		this.suppconfind = suppconfind;
	}
	public String getSuppfeeind() {
		return suppfeeind;
	}
	public void setSuppfeeind(String suppfeeind) {
		this.suppfeeind = suppfeeind;
	}
	public String getSupsecmoveind() {
		return supsecmoveind;
	}
	public void setSupsecmoveind(String supsecmoveind) {
		this.supsecmoveind = supsecmoveind;
	}
	public String getSecurityId() {
		return securityId;
	}
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}
	public void setDealseq(String dealseq) {
		this.dealseq = dealseq;
	}
	public String getDealseq() {
		return dealseq;
	}
	public void setClprice(BigDecimal clprice) {
		this.clprice = clprice;
	}
	public BigDecimal getClprice() {
		return clprice;
	}
	public BigDecimal getDrprice() {
		return drprice;
	}
	public void setDrprice(BigDecimal drprice) {
		this.drprice = drprice;
	}
	public BigDecimal getFaceamt() {
		return faceamt;
	}
	public void setFaceamt(BigDecimal faceamt) {
		this.faceamt = faceamt;
	}

}

