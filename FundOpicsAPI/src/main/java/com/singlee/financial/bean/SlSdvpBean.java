package com.singlee.financial.bean;

import java.io.Serializable;

public class SlSdvpBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6040213353474043626L;
	/**
	 * 分支
	 */
	private String br;
	/**
	 * 交易对手编号
	 */
	private String cno;
	/**
	 * 交割收款标识
	 */
	private String delrecind;
	/**
	 * 产品代码
	 */
	private String product;
	/**
	 * 产品类型
	 */
	private String prodtype;
	/**
	 * 币种
	 */
	private String ccy;
	/**
	 * 托管账户
	 */
	private String safekeepacct;

	/**
	 * 清算方式
	 */
	private String settmeans;
	/**
	 * 清算账户
	 */
	private String settacct;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getDelrecind() {
		return delrecind;
	}

	public void setDelrecind(String delrecind) {
		this.delrecind = delrecind;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSafekeepacct() {
		return safekeepacct;
	}

	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettacct() {
		return settacct;
	}

	public void setSettacct(String settacct) {
		this.settacct = settacct;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
