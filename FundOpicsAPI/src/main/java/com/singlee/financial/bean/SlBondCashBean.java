package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 债券头寸查询
 * @author zhuxl
 *
 */
public class SlBondCashBean implements Serializable {

	private static final long serialVersionUID = 23123124118022945L;
	/**
	 * 债券ISIN
	 */
	private String secid;
	/**
	 * 成本中心
	 */
	private String cost;

	/**
	 * 投资组合
	 */
	private String port;

	/**
	 * 资产类型
	 */
	private String invtype;

	/**
	 * 收款日
	 */
	private String ipaydate;

	/**
	 * 票面价值	
	 */
	private String paramt;

	/**
	 * 成本
	 */
	private String amortamt;

	/**
	 * 计提
	 */
	private String tdyintamt;

	/**
	 * 截止上个付息日利息
	 */
	private String accrintamt;

	/**
	 * 未摊销金额
	 */
	private String unamortamt;

	/**
	 * 成本价
	 */
	private String unitcost;
	
	
	/**
	 * 收盘价
	 */
	private String clsgprice;
	
	/**
	 * 面值
	 */
	private String facevalue;
	
	/**
	 * 市场价值
	 */
	private String fairvalue;
	
	/**
	 * 估值
	 */
	private String diffamt;
	
	/**
	 * 起息日
	 */
	private String vdate;
	
	/**
	 * 到期日
	 */
	private String mdate;
	
	/**
	 * 票面利率
	 */
	private String couprate;
	
	/**
	 * 到期收益率
	 */
	private String yearstomaturity;
	/**
	 * 计息规则
	 */
	private String ratetype;
	/**
	 * 付息频率
	 */
	private String intpaycycle;
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getIpaydate() {
		return ipaydate;
	}
	public void setIpaydate(String ipaydate) {
		this.ipaydate = ipaydate;
	}
	public String getParamt() {
		return paramt;
	}
	public void setParamt(String paramt) {
		this.paramt = paramt;
	}
	public String getAmortamt() {
		return amortamt;
	}
	public void setAmortamt(String amortamt) {
		this.amortamt = amortamt;
	}
	public String getTdyintamt() {
		return tdyintamt;
	}
	public void setTdyintamt(String tdyintamt) {
		this.tdyintamt = tdyintamt;
	}
	public String getAccrintamt() {
		return accrintamt;
	}
	public void setAccrintamt(String accrintamt) {
		this.accrintamt = accrintamt;
	}
	public String getUnamortamt() {
		return unamortamt;
	}
	public void setUnamortamt(String unamortamt) {
		this.unamortamt = unamortamt;
	}
	public String getUnitcost() {
		return unitcost;
	}
	public void setUnitcost(String unitcost) {
		this.unitcost = unitcost;
	}
	public String getClsgprice() {
		return clsgprice;
	}
	public void setClsgprice(String clsgprice) {
		this.clsgprice = clsgprice;
	}
	public String getFacevalue() {
		return facevalue;
	}
	public void setFacevalue(String facevalue) {
		this.facevalue = facevalue;
	}
	public String getFairvalue() {
		return fairvalue;
	}
	public void setFairvalue(String fairvalue) {
		this.fairvalue = fairvalue;
	}
	public String getDiffamt() {
		return diffamt;
	}
	public void setDiffamt(String diffamt) {
		this.diffamt = diffamt;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getCouprate() {
		return couprate;
	}
	public void setCouprate(String couprate) {
		this.couprate = couprate;
	}
	public String getYearstomaturity() {
		return yearstomaturity;
	}
	public void setYearstomaturity(String yearstomaturity) {
		this.yearstomaturity = yearstomaturity;
	}
	public String getRatetype() {
		return ratetype;
	}
	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}
	public String getIntpaycycle() {
		return intpaycycle;
	}
	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}
	
	
}