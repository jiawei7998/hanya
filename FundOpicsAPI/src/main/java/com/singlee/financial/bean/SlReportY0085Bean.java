package com.singlee.financial.bean;

import java.io.Serializable;
/**
 * Y0085 表内加权风险资产计算明细表 实体
 */
public class SlReportY0085Bean implements Serializable {

	private static final long serialVersionUID = -2996059884357116280L;
	
	private String tableid;
	private String br;
	private String lineid; 
	private String rowsid;
	private Double amt;//余额
	private String rpdate;
	public String getTableid() {
		return tableid;
	}
	public void setTableid(String tableid) {
		this.tableid = tableid;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getLineid() {
		return lineid;
	}
	public void setLineid(String lineid) {
		this.lineid = lineid;
	}
	public String getRowsid() {
		return rowsid;
	}
	public void setRowsid(String rowsid) {
		this.rowsid = rowsid;
	}
	public Double getAmt() {
		return amt;
	}
	public void setAmt(Double amt) {
		this.amt = amt;
	}
	public String getRpdate() {
		return rpdate;
	}
	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}
	@Override
	public String toString() {
		return "SlReportY0085Bean [tableid=" + tableid + ", br=" + br
				+ ", lineid=" + lineid + ", rowsid=" + rowsid + ", amt=" + amt
				+ ", rpdate=" + rpdate + "]";
	}
	

	

}
