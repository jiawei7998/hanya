package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/***
 * 利率代码
 * @author lij
 *
 */
public class SlRateBean implements Serializable{
	/**
     * 机构分支
     */
    private String br;

    /**
     * 利率代码
     */
    private String ratecode;

    /**
     * 币种
     */
    private String ccy;

    /**
     * 类型
     */
    private String ratetype;

    /**
     * 
     */
    private String varfix;

    /**
     * 来源
     */
    private String source;

    /**
     * 
     */
    private String desmat;

    /**
     * 
     */
    private BigDecimal spread8;

    /**
     * 计息基准
     */
    private String basis;

    /**
     * 英文描述
     */
    private String descr;


    /**
     * 
     */
    private String ratefixday;

    /**
     * 最后维护时间
     */
    private Date lstmntdate;

    /**
     * 
     */
    private String fincal;

    /**
     * 
     */
    private String ratesourceDde;

    /**
     * 
     */
    private String ratesourceIsda;
    
    
    private String coreccy;
    private String effdate;
    private String intrate;
    private String verind;

    

    private static final long serialVersionUID = 1L;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getRatecode() {
        return ratecode;
    }

    public void setRatecode(String ratecode) {
        this.ratecode = ratecode == null ? null : ratecode.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getRatetype() {
        return ratetype;
    }

    public void setRatetype(String ratetype) {
        this.ratetype = ratetype == null ? null : ratetype.trim();
    }

    public String getVarfix() {
        return varfix;
    }

    public void setVarfix(String varfix) {
        this.varfix = varfix == null ? null : varfix.trim();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public String getDesmat() {
        return desmat;
    }

    public void setDesmat(String desmat) {
        this.desmat = desmat == null ? null : desmat.trim();
    }

    public BigDecimal getSpread8() {
        return spread8;
    }

    public void setSpread8(BigDecimal spread8) {
        this.spread8 = spread8;
    }

    public String getBasis() {
        return basis;
    }

    public void setBasis(String basis) {
        this.basis = basis == null ? null : basis.trim();
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr == null ? null : descr.trim();
    }

    

    public String getRatefixday() {
        return ratefixday;
    }

    public void setRatefixday(String ratefixday) {
        this.ratefixday = ratefixday == null ? null : ratefixday.trim();
    }

    public Date getLstmntdate() {
        return lstmntdate;
    }

    public void setLstmntdate(Date lstmntdate) {
        this.lstmntdate = lstmntdate;
    }

    public String getFincal() {
        return fincal;
    }

    public void setFincal(String fincal) {
        this.fincal = fincal == null ? null : fincal.trim();
    }

    public String getRatesourceDde() {
        return ratesourceDde;
    }

    public void setRatesourceDde(String ratesourceDde) {
        this.ratesourceDde = ratesourceDde == null ? null : ratesourceDde.trim();
    }

    public String getRatesourceIsda() {
        return ratesourceIsda;
    }

    public void setRatesourceIsda(String ratesourceIsda) {
        this.ratesourceIsda = ratesourceIsda == null ? null : ratesourceIsda.trim();
    }

	public String getCoreccy() {
		return coreccy;
	}

	public void setCoreccy(String coreccy) {
		this.coreccy = coreccy;
	}

	public String getEffdate() {
		return effdate;
	}

	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}

	public String getIntrate() {
		return intrate;
	}

	public void setIntrate(String intrate) {
		this.intrate = intrate;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

    

}
