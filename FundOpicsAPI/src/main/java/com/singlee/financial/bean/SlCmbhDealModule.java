package com.singlee.financial.bean;

/**
 * 定义多个
 * 
 * @author shenzl
 * 
 */
public class SlCmbhDealModule {

	public static final String BROKER = "D";

	/**
	 * 拆借模块静态参数
	 */
	public static interface DL {
		/** 标识 */
		public static final String TAG = "DLDE";
		/** 标识-冲销 */
		public static final String TAG_R = "DLDR";
		/** 标识-提前回款-全部 */
		public static final String TAG_LV_ALL = "DLUP";
		/** 标识-提前回款-部分 */
		public static final String TAG_LV_PART = "DLEM";
		/** 具体接口 */
		public static final String DETAIL = "IDLD";
		/** 具体接口-冲销 */
		public static final String DETAIL_R = "IRVV";
		/** 具体接口-提前回款 */
		public static final String DETAIL_LV = "IDLV";
		/** 服务 */
		public static final String SERVER = "SL_FUND_IDLD";
		/** 服务-冲销 */
		public static final String SERVER_R = "SL_FUND_IDLD_R";
		/** 服务-提前回款 */
		public static final String SERVER_LV = "SL_FUND_IDLD_LV";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";
		/************** 交易直通要素 ************************/
		/** 自动复核(Y:自动 N:手工) */
		public static final String VERIND = "N";
		/**
		 * 自动添加清算路径(Y:自动 N:手工 ),必须选择N,
		 * 需要填写IDLD表中COMMMEANS、COMMACCT、MATMEANS、MATACCT否则SCHD中SMEANS和SACCT中没值
		 */
		public static final String SIIND = "Y";
		/** 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPPAYIND = "N";
		/** 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPRECIND = "N";
		/** 禁止发确认书 (Y:是 N：否) */
		public static final String SUPCONFIND = "N";
		/** 自动授权 (Y:是 N：否) */
		public static final String AUTHSI = "N";
	}

	/**
	 * 外汇模块静态参数
	 */
	public static interface FXD {
		/** 标识 */
		public static final String TAG = "FXDE";
		/** 标识 -冲销 */
		public static final String TAG_R = "FXDR";
		/** 具体接口(适用于冲销) */
		public static final String DETAIL = "IFXD";
		/** 服务 */
		public static final String SERVER = "SL_FUND_IFXD";
		/** 服务 -冲销 */
		public static final String SERVER_R = "SL_FUND_IFXD_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";

		/************** 交易直通要素 ************************/
		/** 自动复核(Y:自动 N:手工) */
		public static final String VERIND = "N";
		/** 自动添加清算路径(Y:自动 N:手工 )选择Y时需要填写CCYSMEANS,CCYSACCT,CTRSMEANS,CTRSACCT */
		public static final String SIIND = "Y";
		/** 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPPAYIND = "N";
		/** 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPRECIND = "N";
		/** 禁止发确认书 (Y:是 N：否) */
		public static final String SUPCONFIND = "N";
		/** 自动授权 (Y:是 N：否) */
		public static final String AUTHSI = "N";

	}

	/**
	 * 现券模块静态参数
	 */
	public static interface FI {
		/** 标识 */
		public static final String TAG = "FIDE";
		/** 标识 -冲销 */
		public static final String TAG_R = "FIDR";
		/** 具体接口(适用于冲销) */
		public static final String DETAIL = "BRED";
		/** 服务 */
		public static final String SERVER = "SL_FUND_BRED";
		/** 服务 -冲销 */
		public static final String SERVER_R = "SL_FUND_BRED_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";

		/************** 交易直通要素 ************************/
		/**
		 * 1、债券交易需要注意SETTDATE>=DEALDATE 且不能超过三天否则会提示延迟交割
		 * 
		 * 2、SETTDATE>=债券的VDATE否则会错误提示
		 **/
		/** 自动复核(Y:自动 N:手工) 需要填写对应的VERDATE,否则不会出PMTQ, */
		public static final String VERIND = "N";
		/** 独立的清算指令(Y:自动,N:手工)选择Y时需要填写CCYSMEANS,CCYSACCT */
		public static final String STANDINSTR = "Y";
		/** 存续期是否自动赎回,默认N */
		public static final String COUPREINVEST = "N";

	}

	/**
	 * 回购模块静态参数
	 */
	public static interface REPO {
		/** 标识 */
		public static final String TAG = "REDE";
		/** 标识 -冲销 待确认 */
		public static final String TAG_R = "REDR";
		/** 具体接口(适用于冲销) */
		public static final String DETAIL = "IRCA";
		/** 服务 */
		public static final String SERVER = "SL_FUND_IRCA";
		/** 服务 -冲销 */
		public static final String SERVER_R = "SL_FUND_IRCA_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";
		/************** 交易直通要素 ************************/
		/**
		 * 1.因为存在多券的情况，则抵押券SEQ自己需要递增处理
		 * 
		 * 2.回购交易导入状态位-1,抵押券信息为-5通过触发器处理后续状态修改 成功后查看RPRH和RODT
		 */
		/** 担保物代码,使用OPICS的默认值,在RCCM命令中维护 **/
		public static final String COLLATERALCODE = "ALL";
		/** 交付类型 */
		public static final String DELIVTYPE = "DVP";
		/** 自动复核(Y:自动 N:手工) */
		public static final String VERIND = "N";
		/**
		 * 自动添加清算路径(Y:自动 N:手工 )选择Y时需要填写COMCCYSMEANS,COMCCYSACCT,MATCTRSMEANS,MATCTRSACCT
		 */
		public static final String SIIND = "Y";
		/** 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPPPAYIND = "Y";
		/** 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPPRECIND = "Y";
		/** 禁止发确认书 (Y:是 N：否) */
		public static final String SUPPCONFIND = "Y";
		/** 债券单位,FMT face amount of collateral assigned to the deal */
		public static final String COLLUNIT = "FMT";
		/** 净价/全价标识 */
		public static final String DCPRICEIND = "D";
		/**
		 * 回购利率计算类型
		 * 
		 * SIM :Simple interest mehtod
		 * 
		 * AIM :Average interest mehtod
		 * 
		 * CIM:Compounded interest mehtod
		 */
		public static final String INTCALCTYPE = "SIM";
		/** 保全货币标识 */
		public static final String PMVIND = "Y";
		/** 自动授权 (Y:是 N：否) */
		public static final String AUTHSI = "N";
	}

	/**
	 * 债券借贷模块静态参数
	 */
	public static interface SECLEND {
		/** 标识 */
		public static final String TAG = "SLTE";
		/** 标识 -冲销 */
		public static final String TAG_R = "SLTE";
		/** 具体接口 */
		public static final String DETAIL = "ISLD";
		/** 服务 */
		public static final String SERVER = "SL_FUND_ISLD";
		/** 服务-冲销 */
		public static final String SERVER_R = "SL_FUND_ISLD_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";
		/************** 交易直通要素 ************************/
		/** 自动复核(Y:自动 N:手工) */
		public static final String VERIND = "N";
		/** 一个是借入借出的标的，一个是标的对应的抵押品，标的只有一个，抵押品可以有多个 **/
		/** 债券借贷标的 */
		public static final String DEALIND = "SLDH";
		/** 债券借贷抵押物,多个每条都是一样 */
		public static final String DEALIND_C = "CLDH";
		/** 是否添加清算路径(Y:自动 N:手工) */
		public static final String ADDSI = "N";
		/** 自动授权 (Y:是 N：否) */
		public static final String AUTHSI = "N";
		/** 担保物代码,使用OPICS的默认值,在RCCM命令中维护 **/
		public static final String COLLATERALCODE = "ALL";
		/** 担保物必须标志(Y:必须,N:非必须) */
		public static final String COLLREQUIREDIND = "Y";
		/** 交付类型 */
		public static final String DELIVTYPE = "DVP";
		/** 保全货币标识 */
		public static final String PMVIND = "Y";
		/** 置换标识 */
		public static final String SUBIND = "Y";
		/** 禁止现金标识 ,Y-PMTQ不会产生数据 */
		public static final String SUPPCASHIND = "N";
		/** 禁止确认标识 ,Y-PMTQ不会产生数据 */
		public static final String SUPPCONFIND = "N";
		/** 禁止费用标识,Y-PMTQ不会产生数据 */
		public static final String SUPPFEEIND = "N";
		/** 禁止债券变动标识 */
		public static final String SUPSECMOVEIND = "Y";

	}

	/**
	 * 互换模块静态参数
	 */
	public static interface SWAP {
		/** 标识 */
		public static final String TAG = "SWAP";
		/** 标识 -冲销 待确认 */
		public static final String TAG_R = "SWDR";
		/** 具体接口(适用于冲销) */
		public static final String DETAIL = "ISWH";
		/** 服务 */
		public static final String SERVER = "SL_FUND_ISWH";
		/** 服务 -冲销 */
		public static final String SERVER_R = "SL_FUND_ISWH_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";
		/** 交易类型I-IRS C-CRS */
		public static final String DEALTYPE = "I";
		/************** 交易直通要素 ************************/
		/** 自动复核(Y:自动 N:手工) VERDATE必输 */
		public static final String VERIND = "N";
		/**
		 * 自动添加清算路径(Y:自动 N:手工 ),必须选择N,
		 * 需要填写IDLD表中COMMMEANS、COMMACCT、MATMEANS、MATACCT否则SCHD中SMEANS和SACCT中没值
		 */
		public static final String SIIND = "Y";
		/** 自动授权 (Y:是 N：否) */
		public static final String AUTHSIIND = "N";
		/** 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPPAYIND = "N";
		/** 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPRECIND = "N";
		/** 禁止发确认书 (Y:是 N：否) */
		public static final String SUPCONFIND = "N";
		/** 轧差支付标志 (Y:是 N：否) */
		public static final String NETPAYIND = "Y";
		/** 清算关闭(Y:是 N：否) */
		public static final String SETOFF = "";
		/** 理论支付头寸 -表外 */
		public static final String PAYPOSTNOTIONAL = "Y";
		/** 理论收取头寸 -表外 */
		public static final String RECPOSTNOTIONAL = "Y";
	}

	/**
	 * 期权 基本信息模块静态参数
	 */
	public static interface OTC {
		/** 标识 */
		public static final String TAG = "OTDE";
		/** 标识 -冲销 待确认 */
		public static final String TAG_R = "OTDR";
		/** 标识 -期权行权 */
		public static final String TAG_X = "OTDX";
		/** 具体接口(适用于冲销) */
		public static final String DETAIL = "IOTD";
		/** 服务 */
		public static final String SERVER = "SL_FUND_IOTD";
		/** 服务 -冲销 */
		public static final String SERVER_R = "SL_FUND_IOTD_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";
		/************** 交易直通要素 ************************/
		/** 自动复核(Y:自动 N:手工) VERDATE必输 */
		public static final String VERIND = "N";
		/**
		 * 自动添加清算路径(Y:自动 N:手工 ),必须选择N,
		 * 需要填写IDLD表中COMMMEANS、COMMACCT、MATMEANS、MATACCT否则SCHD中SMEANS和SACCT中没值
		 */
		public static final String SIIND = "Y";
		/** 自动授权 (Y:是 N：否) */
		public static final String AUTHSIIND = "N";
		/** 禁止自动付款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPPAYIND = "N";
		/** 禁止自动收款(Y:是 N：否) 选择Y是PMTQ没数据 */
		public static final String SUPRECIND = "N";
		/** 禁止发确认书 (Y:是 N：否) */
		public static final String SUPCONFIND = "N";
	}

	/**
	 * 现券基本信息模块静态参数
	 */
	public static interface SECUR {
		/** 标识 */
		public static final String TAG = "SEMM";
		/** 具体接口 */
		public static final String DETAIL = "ISEC";
		/** 服务 */
		public static final String SERVER = "SL_FUND_ISEC";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";
		/************** 交易直通要素 ************************/
	}

	/**
	 * 交易对手基本信息模块静态参数
	 */
	public static interface CUST {
		/** 标识 */
		public static final String TAG = "CUST";
		/** 具体接口 */
		public static final String DETAIL = "ICUS";
		/** 服务 */
		public static final String SERVER = "SL_FUND_ICUS";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";
		/************** 交易直通要素 ************************/
	}

	/**
	 * 交易对手清算信息
	 */
	public static interface SETTLE {
		/** 标识 */
		public static final String TAG_CSPI = "CSPI";
		public static final String TAG_CSRI = "CSRI";
		public static final String TAG_SDVP = "SDVP";
		/** 具体接口 */
		public static final String DETAIL_IINS = "IINS";
		public static final String DETAIL_IDRI = "IDRI";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 服务 */
		public static final String SERVER_CSPI = "SL_FUND_CSPI";
		public static final String SERVER_CSRI = "SL_FUND_CSRI";
		public static final String SERVER_SDVP = "SL_FUND_SDVP";

	}

	/**
	 * 基本信息静态参数
	 */
	public static interface BASE {
		/** 机构分支 */
		public static final String BR = "01";

	}

	/**
	 * 现券模块静态参数
	 */
	public static interface IMBD {
		/** 标识 */
		public static final String TAG = "MBDE";
		/** 标识 -冲销 */
		public static final String TAG_R = "MBDR";
		/** 具体接口(适用于冲销) */
		public static final String DETAIL = "IMBD";
		/** 服务 */
		public static final String SERVER = "SL_FUND_IMBD";
		/** 服务 -冲销 */
		public static final String SERVER_R = "SL_FUND_IMBD_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";

		/************** 交易直通要素 ************************/
		/**
		 * 1、债券交易需要注意SETTDATE>=DEALDATE 且不能超过三天否则会提示延迟交割
		 * 
		 * 2、SETTDATE>=债券的VDATE否则会错误提示
		 **/
		/** 自动复核(Y:自动 N:手工) 需要填写对应的VERDATE,否则不会出PMTQ, */
		public static final String VERIND = "N";
		/** 独立的清算指令(Y:自动,N:手工)选择Y时需要填写CCYSMEANS,CCYSACCT */
		public static final String STANDINSTR = "Y";
		/** 存续期是否自动赎回,默认N */
		public static final String COUPREINVEST = "N";

	}

	/**
	 * 费用模块静态参数
	 */
	public static interface FEES {
		/** 标识 */
		public static final String TAG = "FEES";
		/** 标识 -冲销 */
		public static final String TAG_R = "FEER";
		/** 具体接口(适用于冲销) */
		public static final String DETAIL = "IFEE";
		/** 服务 */
		public static final String SERVER = "SL_FUND_IFEE";
		/** 服务 -冲销 */
		public static final String SERVER_R = "SL_FUND_IFEE_R";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "7";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "SING";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
		/** 默认冲销原因 */
		public static final String REVREASON = "99";

		/************** 交易直通要素 ************************/
		/**
		 * 1、债券交易需要注意SETTDATE>=DEALDATE 且不能超过三天否则会提示延迟交割
		 * 
		 * 2、SETTDATE>=债券的VDATE否则会错误提示
		 **/
		/** 自动复核(Y:自动 N:手工) 需要填写对应的VERDATE,否则不会出PMTQ, */
		public static final String VERIND = "N";
		/** 独立的清算指令(Y:自动,N:手工)选择Y时需要填写CCYSMEANS,CCYSACCT */
		public static final String STANDINSTR = "Y";
		/** 存续期是否自动赎回,默认N */
		public static final String COUPREINVEST = "N";
	}
	
	
	
	/**
	 * IREV模块静态参数
	 */
	public static interface IREV {
		/** 标识 */
		public static final String TAG = "REVR";
		/** 具体接口 */
		public static final String DETAIL = "IREV";
		/** 服务 */
		public static final String SERVER = "XTFXREVR";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "4";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "RATE FEED";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
	}
	/**
	 * IRHS模块静态参数
	 */
	public static interface IRHS {
		/** 标识 */
		public static final String TAG = "RHIS";
		/** 具体接口 */
		public static final String DETAIL = "IRHS";
		/** 服务 */
		public static final String SERVER = "RTXTRHIS";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "1";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "RATE FEED";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
	}
	/**
	 * IVOL模块静态参数
	 */
	public static interface IVOL {
		/** 标识 */
		public static final String TAG = "VOLD";
		/** 具体接口 */
		public static final String DETAIL = "IVOL";
		/** 服务 */
		public static final String SERVER = "RTXTVOLD";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "1";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "RATE FEED";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
	}
	/**
	 * IYCR模块静态参数
	 */
	public static interface IYCR {
		/** 标识 */
		public static final String TAG = "YCRT";
		/** 具体接口 */
		public static final String DETAIL = "IYCR";
		/** 服务 */
		public static final String SERVER = "RTXTYCRT";
		/** 导入顺序 */
		public static final String SEQ = "0";
		/** 输入输出标识 */
		public static final String INOUTIND = "I";
		/** 处理优先级 */
		public static final String PRIORITY = "1";
		/** 初始化状态 */
		public static final String STATCODE = "-1";
		/** 数据来源 */
		public static final String SYST = "RATE FEED";
		/** 默认经办人员 */
		public static final String IOPER = "SYS1";
	}
	
	
	
}
