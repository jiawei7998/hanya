package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * @author zc
 */
public class SlReportSecm implements Serializable{
	private static final long serialVersionUID = 1L;
	private  String  branch;
	private  String  cost;
	private  String  port;
	private  String  trad;
	private  String  secId;
	private  String  secmSic;
	private  String  pps;
	private  String  pqty;
	private  String  pprice;
	private  String  pcostAmt;
	private  String  sps;
	private  String  sqty;
	private  String  sprice;
	private  String  scostAmt;
	private  String  mtm;
	private  String  invType;
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getSecId() {
		return secId;
	}
	public void setSecId(String secId) {
		this.secId = secId;
	}
	public String getSecmSic() {
		return secmSic;
	}
	public void setSecmSic(String secmSic) {
		this.secmSic = secmSic;
	}
	public String getPps() {
		return pps;
	}
	public void setPps(String pps) {
		this.pps = pps;
	}
	public String getPqty() {
		return pqty;
	}
	public void setPqty(String pqty) {
		this.pqty = pqty;
	}
	public String getPprice() {
		return pprice;
	}
	public void setPprice(String pprice) {
		this.pprice = pprice;
	}
	public String getPcostAmt() {
		return pcostAmt;
	}
	public void setPcostAmt(String pcostAmt) {
		this.pcostAmt = pcostAmt;
	}
	public String getSps() {
		return sps;
	}
	public void setSps(String sps) {
		this.sps = sps;
	}
	public String getSqty() {
		return sqty;
	}
	public void setSqty(String sqty) {
		this.sqty = sqty;
	}
	public String getSprice() {
		return sprice;
	}
	public void setSprice(String sprice) {
		this.sprice = sprice;
	}
	public String getScostAmt() {
		return scostAmt;
	}
	public void setScostAmt(String scostAmt) {
		this.scostAmt = scostAmt;
	}
	public String getMtm() {
		return mtm;
	}
	public void setMtm(String mtm) {
		this.mtm = mtm;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
	
	
}
