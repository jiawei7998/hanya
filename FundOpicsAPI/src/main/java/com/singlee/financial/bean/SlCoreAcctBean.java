package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 核心账户
 */
public class SlCoreAcctBean implements Serializable {
	
	private static final long serialVersionUID = 7997034095479329314L;

    private String br  ; // 部门 
    private String nos   ; //  清算户
    private String ccy       ; //  币种 
    private String coreacct  ; //  收款人账号 
    private String coreacctnm; //   收款人名称 
    private String note      ; //  备注 
    private String ioper     ; //   创建人 
    private Date idate     ; //  创建时间 
    private String uoper     ; //  最后修改人 
    private Date udate     ; //  最后修改时间
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getNos() {
		return nos;
	}
	public void setNos(String nos) {
		this.nos = nos;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getCoreacct() {
		return coreacct;
	}
	public void setCoreacct(String coreacct) {
		this.coreacct = coreacct;
	}
	public String getCoreacctnm() {
		return coreacctnm;
	}
	public void setCoreacctnm(String coreacctnm) {
		this.coreacctnm = coreacctnm;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getIoper() {
		return ioper;
	}
	public void setIoper(String ioper) {
		this.ioper = ioper;
	}
	public Date getIdate() {
		return idate;
	}
	public void setIdate(Date idate) {
		this.idate = idate;
	}
	public String getUoper() {
		return uoper;
	}
	public void setUoper(String uoper) {
		this.uoper = uoper;
	}
	public Date getUdate() {
		return udate;
	}
	public void setUdate(Date udate) {
		this.udate = udate;
	}
    

}