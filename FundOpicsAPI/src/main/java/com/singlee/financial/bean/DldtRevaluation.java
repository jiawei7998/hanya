package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 浮动拆借定息
 * @Author zhangkai
 * @Description
 * @Date
 */
public class DldtRevaluation implements Serializable {

    private static final long serialVersionUID = -2649376146109729861L;
    private String br;
    private String dealno;
    private String ratecode;
    private String rateRevDte;
    private String nxtRateRev;
    private String product;
    private String prodtype;
    private String ccy;
    private String ccyamt;
    private String vdate;
    private String mdate;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno;
    }

    public String getRatecode() {
        return ratecode;
    }

    public void setRatecode(String ratecode) {
        this.ratecode = ratecode;
    }

    public String getRateRevDte() {
        return rateRevDte;
    }

    public void setRateRevDte(String rateRevDte) {
        this.rateRevDte = rateRevDte;
    }

    public String getNxtRateRev() {
        return nxtRateRev;
    }

    public void setNxtRateRev(String nxtRateRev) {
        this.nxtRateRev = nxtRateRev;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProdtype() {
        return prodtype;
    }

    public void setProdtype(String prodtype) {
        this.prodtype = prodtype;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getCcyamt() {
        return ccyamt;
    }

    public void setCcyamt(String ccyamt) {
        this.ccyamt = ccyamt;
    }

    public String getVdate() {
        return vdate;
    }

    public void setVdate(String vdate) {
        this.vdate = vdate;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }
}
