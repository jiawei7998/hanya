package com.singlee.financial.bean;

import java.io.Serializable;

public class SlIrhsBean implements Serializable{

	/**
	 * IRHS
	 */
	private static final long serialVersionUID = -2284821866476918823L;
	private SlInthBean inthBean;
	private String ratecode;
	private String effdate;
	private double intrate8;
	private String lstmntdate;
	//市场数据查询所需字段
	private String statcode;
	private String errorcode;
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	
	public String getRatecode() {
		return ratecode;
	}
	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}
	public String getEffdate() {
		return effdate;
	}
	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}
	public double getIntrate8() {
		return intrate8;
	}
	public void setIntrate8(double intrate8) {
		this.intrate8 = intrate8;
	}
	public String getLstmntdate() {
		return lstmntdate;
	}
	public void setLstmntdate(String lstmntdate) {
		this.lstmntdate = lstmntdate;
	}
	/**
	 * 
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note
	 * 
	 * 
	 */
	public SlIrhsBean(String br) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); 
			inthBean.setBr(br);
			inthBean.setServer(SlDealModule.IRHS.SERVER);
			inthBean.setSeq(SlDealModule.IRHS.SEQ);
			inthBean.setInoutind(SlDealModule.IRHS.INOUTIND);
			inthBean.setTag(SlDealModule.IRHS.TAG);
			inthBean.setDetail(SlDealModule.IRHS.DETAIL);
			inthBean.setPriority(SlDealModule.IRHS.PRIORITY);
			inthBean.setStatcode(SlDealModule.IRHS.STATCODE);
			inthBean.setSyst(SlDealModule.IRHS.SYST);
			inthBean.setIoper(SlDealModule.IRHS.IOPER);
		}
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
}
