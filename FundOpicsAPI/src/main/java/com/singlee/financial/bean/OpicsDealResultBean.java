package com.singlee.financial.bean;


import java.io.Serializable;
import java.util.Date;

public class OpicsDealResultBean  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String BR;
  private String DATATYPE;
  private Date FEIDATE;
  private int TOTAL;
  private int PENDDING;
  private int FEERROR;
  private int DEALING;
  private int SUCCESS;
  private int RMRQUEUE;
  private int REPAIR;
  private int PENDREV;
  private int REVERALED;
  
  public String getBR()
  {
    return this.BR;
  }
  
  public void setBR(String bR)
  {
    this.BR = bR;
  }
  
  public String getDATATYPE()
  {
    return this.DATATYPE;
  }
  
  public void setDATATYPE(String dATATYPE)
  {
    this.DATATYPE = dATATYPE;
  }
  
  public Date getFEIDATE()
  {
    return this.FEIDATE;
  }
  
  public void setFEIDATE(Date fEIDATE)
  {
    this.FEIDATE = fEIDATE;
  }
  
  public int getTOTAL()
  {
    return this.TOTAL;
  }
  
  public void setTOTAL(int tOTAL)
  {
    this.TOTAL = tOTAL;
  }
  
  public int getPENDDING()
  {
    return this.PENDDING;
  }
  
  public void setPENDDING(int pENDDING)
  {
    this.PENDDING = pENDDING;
  }
  
  public int getFEERROR()
  {
    return this.FEERROR;
  }
  
  public void setFEERROR(int fEERROR)
  {
    this.FEERROR = fEERROR;
  }
  
  public int getDEALING()
  {
    return this.DEALING;
  }
  
  public void setDEALING(int dEALING)
  {
    this.DEALING = dEALING;
  }
  
  public int getSUCCESS()
  {
    return this.SUCCESS;
  }
  
  public void setSUCCESS(int sUCCESS)
  {
    this.SUCCESS = sUCCESS;
  }
  
  public int getRMRQUEUE()
  {
    return this.RMRQUEUE;
  }
  
  public void setRMRQUEUE(int rMRQUEUE)
  {
    this.RMRQUEUE = rMRQUEUE;
  }
  
  public int getREPAIR()
  {
    return this.REPAIR;
  }
  
  public void setREPAIR(int rEPAIR)
  {
    this.REPAIR = rEPAIR;
  }
  
  public int getPENDREV()
  {
    return this.PENDREV;
  }
  
  public void setPENDREV(int pENDREV)
  {
    this.PENDREV = pENDREV;
  }
  
  public int getREVERALED()
  {
    return this.REVERALED;
  }
  
  public void setREVERALED(int rEVERALED)
  {
    this.REVERALED = rEVERALED;
  }
}
