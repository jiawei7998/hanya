package com.singlee.financial.bean;

import java.io.Serializable;

public class AcupSendBean implements Serializable {

	private static final long serialVersionUID = 1L;
	//套号
	private String setno;
	//顺序号
	private String seqno;
	//部门
	private String br;
	//产品代码
	private String product;
	//产品类型
	private String type;
	//顺序号
	private String argno;
	//流水号
	private String dealno;
	//序号
	private String seq;
	//opics科目号
	private String glno;
	//成本中心
	private String costcent;
	//币种
	private String ccy;
	//opics标志
	private String beind;
	//事件代码
	private String code;
	//金额指针
	private String qual;
	//生效日期
	private String effdate;
	// 账务日期
	private String postdate;
	//交易对手
	private String cmne;
	//借贷标识
	private String drcrind;
	//金額
	private String amount;
	//清算方式
	private String smeans;
	//清算账户
	private String sacct;
	//描述
	private String descr;
	//机构
	private String subbr;
	//货币代码
	private String ccycode;
	//核心科目/业务编码
	private String intglno;
	//科目顺序号
	private String subject;
	//冲销标志
	private String reversal;
	//暂时无用
	private String account;
	//暂时无用
	private String voucher;
	//备注
	private String remark;
	//错误码
	private String errcode;
	//错误原因
	private String errmsg;
	//操作员
	private String oper;
	//opics备注
	private String note;
	//核心返回码
	private String retcode;
	//核心返回错误原因
	private String retmsg;
	//处理标识
	private String dealFlag;
	//录入时间
	private String inputtime;
	//徽商特有
	private String seqcount;
	private String setcount;
	private String cramount;
	private String dramount;
	private String branch;
	private String accttype;
	private String acctcrl;
	private String sts;
	//费用笔数
	private String costCount;
	//费用金额
	private String costAmount;
	public String getCostCount() {
		return costCount;
	}

	public void setCostCount(String costCount) {
		this.costCount = costCount;
	}

	public String getCostAmount() {
		return costAmount;
	}

	public void setCostAmount(String costAmount) {
		this.costAmount = costAmount;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public String getSeqcount() {
		return seqcount;
	}

	public void setSeqcount(String seqcount) {
		this.seqcount = seqcount;
	}

	public String getSetcount() {
		return setcount;
	}

	public void setSetcount(String setcount) {
		this.setcount = setcount;
	}

	public String getCramount() {
		return cramount;
	}

	public void setCramount(String cramount) {
		this.cramount = cramount;
	}

	public String getDramount() {
		return dramount;
	}

	public void setDramount(String dramount) {
		this.dramount = dramount;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getSetno() {
		return setno;
	}

	public void setSetno(String setno) {
		this.setno = setno;
	}

	public String getCcycode() {
		return ccycode;
	}

	public void setCcycode(String ccycode) {
		this.ccycode = ccycode;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAccttype() {
		return accttype;
	}

	public void setAccttype(String accttype) {
		this.accttype = accttype;
	}

	public String getAcctcrl() {
		return acctcrl;
	}

	public void setAcctcrl(String acctcrl) {
		this.acctcrl = acctcrl;
	}

	public String getDrcrind() {
		return drcrind;
	}

	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getVoucher() {
		return voucher;
	}

	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}

	public String getSeqno() {
		return seqno;
	}

	public void setSeqno(String seqno) {
		this.seqno = seqno;
	}

	public String getSts() {
		return sts;
	}

	public void setSts(String sts) {
		this.sts = sts;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getArgno() {
		return argno;
	}

	public void setArgno(String argno) {
		this.argno = argno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getGlno() {
		return glno;
	}

	public void setGlno(String glno) {
		this.glno = glno;
	}

	public String getCostcent() {
		return costcent;
	}

	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getBeind() {
		return beind;
	}

	public void setBeind(String beind) {
		this.beind = beind;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getQual() {
		return qual;
	}

	public void setQual(String qual) {
		this.qual = qual;
	}

	public String getEffdate() {
		return effdate;
	}

	public void setEffdate(String effdate) {
		this.effdate = effdate;
	}

	public String getCmne() {
		return cmne;
	}

	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	public String getSmeans() {
		return smeans;
	}

	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}

	public String getSacct() {
		return sacct;
	}

	public void setSacct(String sacct) {
		this.sacct = sacct;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getSubbr() {
		return subbr;
	}

	public void setSubbr(String subbr) {
		this.subbr = subbr;
	}

	public String getIntglno() {
		return intglno;
	}

	public void setIntglno(String intglno) {
		this.intglno = intglno;
	}

	public String getReversal() {
		return reversal;
	}

	public void setReversal(String reversal) {
		this.reversal = reversal;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getErrcode() {
		return errcode;
	}

	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getRetcode() {
		return retcode;
	}

	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}

	public String getRetmsg() {
		return retmsg;
	}

	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}

	public String getDealFlag() {
		return dealFlag;
	}

	public void setDealFlag(String dealFlag) {
		this.dealFlag = dealFlag;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
}