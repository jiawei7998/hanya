package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
/***
 * 外汇即期
 * @author LIJ
 *
 */
public class GmrmFxSpot implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -8873034197277091673L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 头寸编号
     */
    private String posId;

    /**
     * 数据来源
     */
    private String source;

    /**
     * Folder
     */
    private String folder;

    /**
     * 币种一
     */
    private String ccy1;

    /**
     * 币种二
     */
    private String ccy2;

    /**
     * 币种一买入金额，带符号数值
     */
    private BigDecimal buyAmount1;

    /**
     * 币种一卖出金额，带符号数值
     */
    private BigDecimal sellAmount1;

    /**
     * 币种二买入金额，带符号数值
     */
    private BigDecimal buyAmount2;

    /**
     * 币种二卖出金额，带符号数值
     */
    private BigDecimal sellAmount2;

    /**
     * 起息日 YYYYMMDD
     */
    private String valueDate;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 损益币种
     */
    private String plCcy;

    /**
     * 每日损益，带符号数值
     */
    private BigDecimal dailyPl;

    /**
     * null
     * @return DATADATE null
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * null
     * @param datadate null
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * null
     * @return BATCH_DATE null
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * null
     * @param batchDate null
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * null
     * @return POS_ID null
     */
    public String getPosId() {
        return posId;
    }

    /**
     * null
     * @param posId null
     */
    public void setPosId(String posId) {
        this.posId = posId == null ? null : posId.trim();
    }

    /**
     * null
     * @return SOURCE null
     */
    public String getSource() {
        return source;
    }

    /**
     * null
     * @param source null
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * null
     * @return FOLDER null
     */
    public String getFolder() {
        return folder;
    }

    /**
     * null
     * @param folder null
     */
    public void setFolder(String folder) {
        this.folder = folder == null ? null : folder.trim();
    }

    /**
     * null
     * @return CCY1 null
     */
    public String getCcy1() {
        return ccy1;
    }

    /**
     * null
     * @param ccy1 null
     */
    public void setCcy1(String ccy1) {
        this.ccy1 = ccy1 == null ? null : ccy1.trim();
    }

    /**
     * null
     * @return CCY2 null
     */
    public String getCcy2() {
        return ccy2;
    }

    /**
     * null
     * @param ccy2 null
     */
    public void setCcy2(String ccy2) {
        this.ccy2 = ccy2 == null ? null : ccy2.trim();
    }

    /**
     * null
     * @return BUY_AMOUNT1 null
     */
    public BigDecimal getBuyAmount1() {
        return buyAmount1;
    }

    /**
     * null
     * @param buyAmount1 null
     */
    public void setBuyAmount1(BigDecimal buyAmount1) {
        this.buyAmount1 = buyAmount1;
    }

    /**
     * null
     * @return SELL_AMOUNT1 null
     */
    public BigDecimal getSellAmount1() {
        return sellAmount1;
    }

    /**
     * null
     * @param sellAmount1 null
     */
    public void setSellAmount1(BigDecimal sellAmount1) {
        this.sellAmount1 = sellAmount1;
    }

    /**
     * null
     * @return BUY_AMOUNT2 null
     */
    public BigDecimal getBuyAmount2() {
        return buyAmount2;
    }

    /**
     * null
     * @param buyAmount2 null
     */
    public void setBuyAmount2(BigDecimal buyAmount2) {
        this.buyAmount2 = buyAmount2;
    }

    /**
     * null
     * @return SELL_AMOUNT2 null
     */
    public BigDecimal getSellAmount2() {
        return sellAmount2;
    }

    /**
     * null
     * @param sellAmount2 null
     */
    public void setSellAmount2(BigDecimal sellAmount2) {
        this.sellAmount2 = sellAmount2;
    }

    /**
     * null
     * @return VALUE_DATE null
     */
    public String getValueDate() {
        return valueDate;
    }

    /**
     * null
     * @param valueDate null
     */
    public void setValueDate(String valueDate) {
        this.valueDate = valueDate == null ? null : valueDate.trim();
    }

    /**
     * null
     * @return BRANCH_ID null
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * null
     * @param branchId null
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }

    /**
     * null
     * @return PL_CCY null
     */
    public String getPlCcy() {
        return plCcy;
    }

    /**
     * null
     * @param plCcy null
     */
    public void setPlCcy(String plCcy) {
        this.plCcy = plCcy == null ? null : plCcy.trim();
    }

    /**
     * null
     * @return DAILY_PL null
     */
    public BigDecimal getDailyPl() {
        return dailyPl;
    }

    /**
     * null
     * @param dailyPl null
     */
    public void setDailyPl(BigDecimal dailyPl) {
        this.dailyPl = dailyPl;
    }
}