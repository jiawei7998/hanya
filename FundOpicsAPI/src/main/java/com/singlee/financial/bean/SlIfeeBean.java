package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 费用模块
 *
 * @author zhengfl
 * @see
 * @since JDK 1.6
 */
public class SlIfeeBean implements Serializable {

	private static final long serialVersionUID = -2638065782953640548L;

	private SlInthBean inthBean;
	private String feeno;
	private String feeseq;
	private String feeproduct;
	private String feeprodtype;
	private String dealno;
	private String dealseq;
	private String product;
	private String prodtype;
	private String ccy;
	private String ccyamt;
	private String cost;
	private String cno;
	private String tenor;
	private String al;
	private String acctgmethod;
	private Date startdate;
	private Date enddate;
	private Date vdate;
	private String ioper;
	private Date inputdate;
	private String inputtime;
	private Date brprcindte;
	private String verind;
	private String veroper;
	private Date verdate;
	private String settmeans;
	private String settacct;
	private String settauthind;
	private Date settauthdte;
	private String settoper;
	private String feetext;
	private String revreason;
	private Date revdate;
	private String revtext;
	private Date lastmntdte;
	private String secid;
	private String feesind;
	private String feeper_8;
	private Date dealmdate;
	private String dealccyamt;
	private Date dealvdate;
	private String port;
	private String trad;
	private Date dealdate;
	private String fincent;
	private String coverbyfx;
	private String settccy;
	private String settexchrate_8;
	private String settexchterms;
	private String settamtperiod;
	private String dealamtperiod;
	private String baseamtperiod;
	private String netdeal;
	private String netfeeno;
	private String origcno;
	private Date lstpaiddate;
	private String siind;
	private String suppconfind;
	private String suppayind;
	private String suprecind;
	private String supplemental;
	private String updatecounter;
	private String feetype;
	private String action;
	private String basis;
	private String baseccyamt;
	private String notamt;
	private String amortmethod;

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public String getFeeno() {
		return feeno;
	}

	public void setFeeno(String feeno) {
		this.feeno = feeno;
	}

	public String getFeeseq() {
		return feeseq;
	}

	public void setFeeseq(String feeseq) {
		this.feeseq = feeseq;
	}

	public String getFeeproduct() {
		return feeproduct;
	}

	public void setFeeproduct(String feeproduct) {
		this.feeproduct = feeproduct;
	}

	public String getFeeprodtype() {
		return feeprodtype;
	}

	public void setFeeprodtype(String feeprodtype) {
		this.feeprodtype = feeprodtype;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getDealseq() {
		return dealseq;
	}

	public void setDealseq(String dealseq) {
		this.dealseq = dealseq;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcyamt() {
		return ccyamt;
	}

	public void setCcyamt(String ccyamt) {
		this.ccyamt = ccyamt;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getAl() {
		return al;
	}

	public void setAl(String al) {
		this.al = al;
	}

	public String getAcctgmethod() {
		return acctgmethod;
	}

	public void setAcctgmethod(String acctgmethod) {
		this.acctgmethod = acctgmethod;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public Date getInputdate() {
		return inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public Date getBrprcindte() {
		return brprcindte;
	}

	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public String getVeroper() {
		return veroper;
	}

	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettacct() {
		return settacct;
	}

	public void setSettacct(String settacct) {
		this.settacct = settacct;
	}

	public String getSettauthind() {
		return settauthind;
	}

	public void setSettauthind(String settauthind) {
		this.settauthind = settauthind;
	}

	public Date getSettauthdte() {
		return settauthdte;
	}

	public void setSettauthdte(Date settauthdte) {
		this.settauthdte = settauthdte;
	}

	public String getSettoper() {
		return settoper;
	}

	public void setSettoper(String settoper) {
		this.settoper = settoper;
	}

	public String getFeetext() {
		return feetext;
	}

	public void setFeetext(String feetext) {
		this.feetext = feetext;
	}

	public String getRevreason() {
		return revreason;
	}

	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}

	public Date getRevdate() {
		return revdate;
	}

	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}

	public String getRevtext() {
		return revtext;
	}

	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}

	public Date getLastmntdte() {
		return lastmntdte;
	}

	public void setLastmntdte(Date lastmntdte) {
		this.lastmntdte = lastmntdte;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getFeesind() {
		return feesind;
	}

	public void setFeesind(String feesind) {
		this.feesind = feesind;
	}

	public String getFeeper_8() {
		return feeper_8;
	}

	public void setFeeper_8(String feeper_8) {
		this.feeper_8 = feeper_8;
	}

	public Date getDealmdate() {
		return dealmdate;
	}

	public void setDealmdate(Date dealmdate) {
		this.dealmdate = dealmdate;
	}

	public String getDealccyamt() {
		return dealccyamt;
	}

	public void setDealccyamt(String dealccyamt) {
		this.dealccyamt = dealccyamt;
	}

	public Date getDealvdate() {
		return dealvdate;
	}

	public void setDealvdate(Date dealvdate) {
		this.dealvdate = dealvdate;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public Date getDealdate() {
		return dealdate;
	}

	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}

	public String getFincent() {
		return fincent;
	}

	public void setFincent(String fincent) {
		this.fincent = fincent;
	}

	public String getCoverbyfx() {
		return coverbyfx;
	}

	public void setCoverbyfx(String coverbyfx) {
		this.coverbyfx = coverbyfx;
	}

	public String getSettccy() {
		return settccy;
	}

	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}

	public String getSettexchrate_8() {
		return settexchrate_8;
	}

	public void setSettexchrate_8(String settexchrate_8) {
		this.settexchrate_8 = settexchrate_8;
	}

	public String getSettexchterms() {
		return settexchterms;
	}

	public void setSettexchterms(String settexchterms) {
		this.settexchterms = settexchterms;
	}

	public String getSettamtperiod() {
		return settamtperiod;
	}

	public void setSettamtperiod(String settamtperiod) {
		this.settamtperiod = settamtperiod;
	}

	public String getDealamtperiod() {
		return dealamtperiod;
	}

	public void setDealamtperiod(String dealamtperiod) {
		this.dealamtperiod = dealamtperiod;
	}

	public String getBaseamtperiod() {
		return baseamtperiod;
	}

	public void setBaseamtperiod(String baseamtperiod) {
		this.baseamtperiod = baseamtperiod;
	}

	public String getNetdeal() {
		return netdeal;
	}

	public void setNetdeal(String netdeal) {
		this.netdeal = netdeal;
	}

	public String getNetfeeno() {
		return netfeeno;
	}

	public void setNetfeeno(String netfeeno) {
		this.netfeeno = netfeeno;
	}

	public String getOrigcno() {
		return origcno;
	}

	public void setOrigcno(String origcno) {
		this.origcno = origcno;
	}

	public Date getLstpaiddate() {
		return lstpaiddate;
	}

	public void setLstpaiddate(Date lstpaiddate) {
		this.lstpaiddate = lstpaiddate;
	}

	public String getSiind() {
		return siind;
	}

	public void setSiind(String siind) {
		this.siind = siind;
	}

	public String getSuppconfind() {
		return suppconfind;
	}

	public void setSuppconfind(String suppconfind) {
		this.suppconfind = suppconfind;
	}

	public String getSuppayind() {
		return suppayind;
	}

	public void setSuppayind(String suppayind) {
		this.suppayind = suppayind;
	}

	public String getSuprecind() {
		return suprecind;
	}

	public void setSuprecind(String suprecind) {
		this.suprecind = suprecind;
	}

	public String getSupplemental() {
		return supplemental;
	}

	public void setSupplemental(String supplemental) {
		this.supplemental = supplemental;
	}

	public String getUpdatecounter() {
		return updatecounter;
	}

	public void setUpdatecounter(String updatecounter) {
		this.updatecounter = updatecounter;
	}

	public String getFeetype() {
		return feetype;
	}

	public void setFeetype(String feetype) {
		this.feetype = feetype;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getBaseccyamt() {
		return baseccyamt;
	}

	public void setBaseccyamt(String baseccyamt) {
		this.baseccyamt = baseccyamt;
	}

	public String getNotamt() {
		return notamt;
	}

	public void setNotamt(String notamt) {
		this.notamt = notamt;
	}

	public String getAmortmethod() {
		return amortmethod;
	}

	public void setAmortmethod(String amortmethod) {
		this.amortmethod = amortmethod;
	}

	public SlIfeeBean() {
	}

	/**
	 * 费用导入始化
	 *
	 * @param br     业务部门分支
	 * @param tag    标识
	 * @param detail 细节
	 * @note 1、交易导入 TAG="FEES" , DETAIL="IFEE"
	 */
	public SlIfeeBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean();
			inthBean.setBr(br);
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.FEES.SEQ);
			inthBean.setInoutind(SlDealModule.FEES.INOUTIND);
			inthBean.setPriority(SlDealModule.FEES.PRIORITY);
			inthBean.setStatcode(SlDealModule.FEES.STATCODE);
			inthBean.setSyst(SlDealModule.FEES.SYST);
			inthBean.setIoper(SlDealModule.FEES.IOPER);
		}
	}

}

