package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TRtrate implements Serializable {

	private static final long serialVersionUID = 1L;

	private String formula;
	
	private String inputDate;
	
	private BigDecimal rate_8;
	
	private String errorcode;

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}
	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public BigDecimal getRate_8() {
		return rate_8;
	}

	public void setRate_8(BigDecimal rate_8) {
		this.rate_8 = rate_8;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

}
