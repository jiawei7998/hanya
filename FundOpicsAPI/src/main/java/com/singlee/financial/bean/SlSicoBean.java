package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * OPICS定义行业代码
 * 
 * @author shenzl
 * 
 */
public class SlSicoBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5583518728694902151L;
	/**
	 * 行业标准代码
	 */
	private String sic;
	/**
	 * 行业标准代码描述
	 */
	private String sd;
	/**
	 * 最后维护时间
	 */
	private Date lstmntdte;

	public String getSic() {
		return sic;
	}

	public void setSic(String sic) {
		this.sic = sic;
	}

	public String getSd() {
		return sd;
	}

	public void setSd(String sd) {
		this.sd = sd;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

}
