package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * SL_ACUP表实体类
 * 
 * @author Qxj
 */
public class SlAcupBean implements Serializable {

	private static final long serialVersionUID = 2331673764118022945L;

	/**
	 * 套号
	 */
	private String setNo;

	/**
	 * 顺序号
	 */
	private String seqNo;

	/**
	 * 序列
	 */
	private String seq;

	/**
	 * 部门
	 */
	private String br;

	/**
	 * 产品代码
	 */
	private String product;

	/**
	 * 类型代码
	 */
	private String type;

	/**
	 * 交易流水
	 */
	private String dealno;

	/**
	 * opics科目
	 */
	private String glno;

	/**
	 * 外部映射科目
	 */
	private String intglno;

	/**
	 * 描述
	 */
	private String descr;

	/**
	 * 记账网点
	 */
	private String subbr;

	/**
	 * 核心账号
	 */
	private String subject;

	/**
	 * 借贷标识
	 */
	private String drcrind;

	/**
	 * 本金
	 */
	private String amount;

	/**
	 * 错误码
	 */
	private String errCode;

	/**
	 * 错误信息
	 */
	private String errMsg;

	/**
	 * 流水号
	 */
	private String voucher;

	// 生效日期
	private String effDate;

	// 过账日期
	private String postDate;

	// 核心返回码
	private String retCode;

	// 核心返回消息
	private String retMsg;

	// 币种
	private String ccy;

	/**
	 * 币种代码
	 */
	private String ccyCode;

	// 结算方式
	private String smeans;

	// 结算账号
	private String sacct;

	// 英文简称
	private String cmne;

	private String argno;
	private String costcent; // 成本中心
	private String beind;
	private String code;
	private String qual;
	private String reversal;
	private String account;
	private String remark; // 备注
	private String oper; // 操作员
	private String dealflag;
	private String note;
	private String inputtime; 
	
	private String aglno;

	private String tdescr;
	/**
	 * 泰隆要求传入当前用户
	 */
	private String username;

	/**
	 * 获取套号
	 * 
	 * @return
	 */
	public String getSetNo() {
		return setNo;
	}

	/**
	 * 设置套号
	 * 
	 * @return
	 */
	public void setSetNo(String setNo) {
		this.setNo = setNo;
	}

	/**
	 * 获取序列
	 */
	public String getSeqNo() {
		return seqNo;
	}

	/**
	 * 设置序列
	 */
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	/**
	 * 产品代码
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * 产品代码
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * 类型代码
	 */
	public String getType() {
		return type;
	}

	/**
	 * 类型代码
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 交易流水
	 */

	public String getDealno() {
		return dealno;
	}

	/**
	 * 交易流水
	 */

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	/**
	 * opics科目
	 */
	public String getGlno() {
		return glno;
	}

	/**
	 * opics科目
	 */
	public void setGlno(String glno) {
		this.glno = glno;
	}

	/**
	 * 外部映射科目
	 */
	public String getIntglno() {
		return intglno;
	}

	/**
	 * 外部映射科目
	 */
	public void setIntglno(String intglno) {
		this.intglno = intglno;
	}

	/**
	 * 描述
	 */
	public String getDescr() {
		return descr;
	}

	/**
	 * 描述
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

	/**
	 * 科目映射
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * 科目映射
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * 借贷标识
	 */
	public String getDrcrind() {
		return drcrind;
	}

	/**
	 * 借贷标识
	 */
	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}

	/**
	 * 本金
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * 本金
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	/**
	 * 错误信息
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * 错误信息
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getVoucher() {
		return voucher;
	}

	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getSubbr() {
		return subbr;
	}

	public void setSubbr(String subbr) {
		this.subbr = subbr;
	}

	public String getEffDate() {
		return effDate;
	}

	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcyCode() {
		return ccyCode;
	}

	public void setCcyCode(String ccyCode) {
		this.ccyCode = ccyCode;
	}

	public String getSmeans() {
		return smeans;
	}

	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}

	public String getSacct() {
		return sacct;
	}

	public void setSacct(String sacct) {
		this.sacct = sacct;
	}

	public String getCmne() {
		return cmne;
	}

	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getArgno() {
		return argno;
	}

	public void setArgno(String argno) {
		this.argno = argno;
	}

	public String getCostcent() {
		return costcent;
	}

	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}

	public String getBeind() {
		return beind;
	}

	public void setBeind(String beind) {
		this.beind = beind;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getQual() {
		return qual;
	}

	public void setQual(String qual) {
		this.qual = qual;
	}

	public String getReversal() {
		return reversal;
	}

	public void setReversal(String reversal) {
		this.reversal = reversal;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getDealflag() {
		return dealflag;
	}

	public void setDealflag(String dealflag) {
		this.dealflag = dealflag;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public void setAglno(String aglno) {
		this.aglno = aglno;
	}

	public String getAglno() {
		return aglno;
	}

	public String getTdescr() {
		return tdescr;
	}

	public void setTdescr(String tdescr) {
		this.tdescr = tdescr;
	}
}