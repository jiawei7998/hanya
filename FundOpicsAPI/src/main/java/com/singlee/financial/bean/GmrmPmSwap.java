package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;

/***
 * 贵金属掉期
 * @author LIJ
 *
 */
public class GmrmPmSwap implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = 6428061700363305769L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 批量日期 YYYYMMDD
     */
    private String batchDate;

    /**
     * 交易ID
     */
    private String dealId;

    /**
     * 交易员ID
     */
    private String traderId;

    /**
     * 交易日期 YYYYMMDD
     */
    private String tradeDate;

    /**
     * 交易时间 YYYYMMDD HH24:MI:SS
     */
    private String tradeTime;

    /**
     * 起息日 YYYYMMDD
     */
    private String valueDate;

    /**
     * 交割日 YYYYMMDD
     */
    private String matDate;

    /**
     * Folder
     */
    private String folderId;

    /**
     * 账户类型 1-银行账户、2-交易账户
     */
    private String accountType;

    /**
     * 买卖标识 1-买入/卖出、3-卖出/买入
     */
    private String bsFlag;

    /**
     * 合约号 默认SP.PAu99.99
     */
    private String contractId;

    /**
     * 合约手数
     */
    private String qty;

    /**
     * 报价单位 1-克、2-盎司、3-千克
     */
    private String quotaUnit;

    /**
     * 品种代码 1-AU、2-AG、3-PT、4-PD
     */
    private String varietyId;

    /**
     * 交易所代码
     */
    private String exchCode;

    /**
     * 即期价格
     */
    private BigDecimal spotPrice;

    /**
     * 近端点数
     */
    private BigDecimal nearPoints;

    /**
     * 远端点数
     */
    private BigDecimal fwdPoints;

    /**
     * 近端价格
     */
    private BigDecimal nearPrice;

    /**
     * 远端价格
     */
    private BigDecimal fwdPrice;

    /**
     * 交易所委托号
     */
    private String orderNo;

    /**
     * 近端手续费
     */
    private BigDecimal commFee1;

    /**
     * 远端手续费
     */
    private BigDecimal commFee2;

    /**
     * 交易对手
     */
    private String cptyId;

    /**
     * 币种一名
     */
    private String ccy1;

    /**
     * 币种二名
     */
    private String ccy2;

    /**
     * 币种一金额一
     */
    private BigDecimal amt1Ccy1;

    /**
     * 币种二金额一
     */
    private BigDecimal amt1Ccy2;

    /**
     * 币种一金额二
     */
    private BigDecimal amt2Ccy1;

    /**
     * 币种二金额二
     */
    private BigDecimal amt2Ccy2;

    /**
     * 机构号
     */
    private String branchId;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 批量日期 YYYYMMDD
     * @return BATCH_DATE 批量日期 YYYYMMDD
     */
    public String getBatchDate() {
        return batchDate;
    }

    /**
     * 批量日期 YYYYMMDD
     * @param batchDate 批量日期 YYYYMMDD
     */
    public void setBatchDate(String batchDate) {
        this.batchDate = batchDate == null ? null : batchDate.trim();
    }

    /**
     * 交易ID
     * @return DEAL_ID 交易ID
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 交易ID
     * @param dealId 交易ID
     */
    public void setDealId(String dealId) {
        this.dealId = dealId == null ? null : dealId.trim();
    }

    /**
     * 交易员ID
     * @return TRADER_ID 交易员ID
     */
    public String getTraderId() {
        return traderId;
    }

    /**
     * 交易员ID
     * @param traderId 交易员ID
     */
    public void setTraderId(String traderId) {
        this.traderId = traderId == null ? null : traderId.trim();
    }

    /**
     * 交易日期 YYYYMMDD
     * @return TRADE_DATE 交易日期 YYYYMMDD
     */
    public String getTradeDate() {
        return tradeDate;
    }

    /**
     * 交易日期 YYYYMMDD
     * @param tradeDate 交易日期 YYYYMMDD
     */
    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate == null ? null : tradeDate.trim();
    }

    /**
     * 交易时间 YYYYMMDD HH24:MI:SS
     * @return TRADE_TIME 交易时间 YYYYMMDD HH24:MI:SS
     */
    public String getTradeTime() {
        return tradeTime;
    }

    /**
     * 交易时间 YYYYMMDD HH24:MI:SS
     * @param tradeTime 交易时间 YYYYMMDD HH24:MI:SS
     */
    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime == null ? null : tradeTime.trim();
    }

    /**
     * 起息日 YYYYMMDD
     * @return VALUE_DATE 起息日 YYYYMMDD
     */
    public String getValueDate() {
        return valueDate;
    }

    /**
     * 起息日 YYYYMMDD
     * @param valueDate 起息日 YYYYMMDD
     */
    public void setValueDate(String valueDate) {
        this.valueDate = valueDate == null ? null : valueDate.trim();
    }

    /**
     * 交割日 YYYYMMDD
     * @return MAT_DATE 交割日 YYYYMMDD
     */
    public String getMatDate() {
        return matDate;
    }

    /**
     * 交割日 YYYYMMDD
     * @param matDate 交割日 YYYYMMDD
     */
    public void setMatDate(String matDate) {
        this.matDate = matDate == null ? null : matDate.trim();
    }

    /**
     * Folder
     * @return FOLDER_ID Folder
     */
    public String getFolderId() {
        return folderId;
    }

    /**
     * Folder
     * @param folderId Folder
     */
    public void setFolderId(String folderId) {
        this.folderId = folderId == null ? null : folderId.trim();
    }

    /**
     * 账户类型 1-银行账户、2-交易账户
     * @return ACCOUNT_TYPE 账户类型 1-银行账户、2-交易账户
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * 账户类型 1-银行账户、2-交易账户
     * @param accountType 账户类型 1-银行账户、2-交易账户
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType == null ? null : accountType.trim();
    }

    /**
     * 买卖标识 1-买入/卖出、3-卖出/买入
     * @return BS_FLAG 买卖标识 1-买入/卖出、3-卖出/买入
     */
    public String getBsFlag() {
        return bsFlag;
    }

    /**
     * 买卖标识 1-买入/卖出、3-卖出/买入
     * @param bsFlag 买卖标识 1-买入/卖出、3-卖出/买入
     */
    public void setBsFlag(String bsFlag) {
        this.bsFlag = bsFlag == null ? null : bsFlag.trim();
    }

    /**
     * 合约号 默认SP.PAu99.99
     * @return CONTRACT_ID 合约号 默认SP.PAu99.99
     */
    public String getContractId() {
        return contractId;
    }

    /**
     * 合约号 默认SP.PAu99.99
     * @param contractId 合约号 默认SP.PAu99.99
     */
    public void setContractId(String contractId) {
        this.contractId = contractId == null ? null : contractId.trim();
    }

    /**
     * 合约手数
     * @return QTY 合约手数
     */
    public String getQty() {
        return qty;
    }

    /**
     * 合约手数
     * @param qty 合约手数
     */
    public void setQty(String qty) {
        this.qty = qty == null ? null : qty.trim();
    }

    /**
     * 报价单位 1-克、2-盎司、3-千克
     * @return QUOTA_UNIT 报价单位 1-克、2-盎司、3-千克
     */
    public String getQuotaUnit() {
        return quotaUnit;
    }

    /**
     * 报价单位 1-克、2-盎司、3-千克
     * @param quotaUnit 报价单位 1-克、2-盎司、3-千克
     */
    public void setQuotaUnit(String quotaUnit) {
        this.quotaUnit = quotaUnit == null ? null : quotaUnit.trim();
    }

    /**
     * 品种代码 1-AU、2-AG、3-PT、4-PD
     * @return VARIETY_ID 品种代码 1-AU、2-AG、3-PT、4-PD
     */
    public String getVarietyId() {
        return varietyId;
    }

    /**
     * 品种代码 1-AU、2-AG、3-PT、4-PD
     * @param varietyId 品种代码 1-AU、2-AG、3-PT、4-PD
     */
    public void setVarietyId(String varietyId) {
        this.varietyId = varietyId == null ? null : varietyId.trim();
    }

    /**
     * 交易所代码
     * @return EXCH_CODE 交易所代码
     */
    public String getExchCode() {
        return exchCode;
    }

    /**
     * 交易所代码
     * @param exchCode 交易所代码
     */
    public void setExchCode(String exchCode) {
        this.exchCode = exchCode == null ? null : exchCode.trim();
    }

    /**
     * 即期价格
     * @return SPOT_PRICE 即期价格
     */
    public BigDecimal getSpotPrice() {
        return spotPrice;
    }

    /**
     * 即期价格
     * @param spotPrice 即期价格
     */
    public void setSpotPrice(BigDecimal spotPrice) {
        this.spotPrice = spotPrice;
    }

    /**
     * 近端点数
     * @return NEAR_POINTS 近端点数
     */
    public BigDecimal getNearPoints() {
        return nearPoints;
    }

    /**
     * 近端点数
     * @param nearPoints 近端点数
     */
    public void setNearPoints(BigDecimal nearPoints) {
        this.nearPoints = nearPoints;
    }

    /**
     * 远端点数
     * @return FWD_POINTS 远端点数
     */
    public BigDecimal getFwdPoints() {
        return fwdPoints;
    }

    /**
     * 远端点数
     * @param fwdPoints 远端点数
     */
    public void setFwdPoints(BigDecimal fwdPoints) {
        this.fwdPoints = fwdPoints;
    }

    /**
     * 近端价格
     * @return NEAR_PRICE 近端价格
     */
    public BigDecimal getNearPrice() {
        return nearPrice;
    }

    /**
     * 近端价格
     * @param nearPrice 近端价格
     */
    public void setNearPrice(BigDecimal nearPrice) {
        this.nearPrice = nearPrice;
    }

    /**
     * 远端价格
     * @return FWD_PRICE 远端价格
     */
    public BigDecimal getFwdPrice() {
        return fwdPrice;
    }

    /**
     * 远端价格
     * @param fwdPrice 远端价格
     */
    public void setFwdPrice(BigDecimal fwdPrice) {
        this.fwdPrice = fwdPrice;
    }

    /**
     * null
     * @return ORDER_NO null
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * null
     * @param orderNo null
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    /**
     * null
     * @return COMM_FEE1 null
     */
    public BigDecimal getCommFee1() {
        return commFee1;
    }

    /**
     * null
     * @param commFee1 null
     */
    public void setCommFee1(BigDecimal commFee1) {
        this.commFee1 = commFee1;
    }

    /**
     * null
     * @return COMM_FEE2 null
     */
    public BigDecimal getCommFee2() {
        return commFee2;
    }

    /**
     * null
     * @param commFee2 null
     */
    public void setCommFee2(BigDecimal commFee2) {
        this.commFee2 = commFee2;
    }

    /**
     * null
     * @return CPTY_ID null
     */
    public String getCptyId() {
        return cptyId;
    }

    /**
     * null
     * @param cptyId null
     */
    public void setCptyId(String cptyId) {
        this.cptyId = cptyId == null ? null : cptyId.trim();
    }

    /**
     * null
     * @return CCY_1 null
     */
    public String getCcy1() {
        return ccy1;
    }

    /**
     * null
     * @param ccy1 null
     */
    public void setCcy1(String ccy1) {
        this.ccy1 = ccy1 == null ? null : ccy1.trim();
    }

    /**
     * null
     * @return CCY_2 null
     */
    public String getCcy2() {
        return ccy2;
    }

    /**
     * null
     * @param ccy2 null
     */
    public void setCcy2(String ccy2) {
        this.ccy2 = ccy2 == null ? null : ccy2.trim();
    }

    /**
     * null
     * @return AMT1_CCY1 null
     */
    public BigDecimal getAmt1Ccy1() {
        return amt1Ccy1;
    }

    /**
     * null
     * @param amt1Ccy1 null
     */
    public void setAmt1Ccy1(BigDecimal amt1Ccy1) {
        this.amt1Ccy1 = amt1Ccy1;
    }

    /**
     * null
     * @return AMT1_CCY2 null
     */
    public BigDecimal getAmt1Ccy2() {
        return amt1Ccy2;
    }

    /**
     * null
     * @param amt1Ccy2 null
     */
    public void setAmt1Ccy2(BigDecimal amt1Ccy2) {
        this.amt1Ccy2 = amt1Ccy2;
    }

    /**
     * null
     * @return AMT2_CCY1 null
     */
    public BigDecimal getAmt2Ccy1() {
        return amt2Ccy1;
    }

    /**
     * null
     * @param amt2Ccy1 null
     */
    public void setAmt2Ccy1(BigDecimal amt2Ccy1) {
        this.amt2Ccy1 = amt2Ccy1;
    }

    /**
     * null
     * @return AMT2_CCY2 null
     */
    public BigDecimal getAmt2Ccy2() {
        return amt2Ccy2;
    }

    /**
     * null
     * @param amt2Ccy2 null
     */
    public void setAmt2Ccy2(BigDecimal amt2Ccy2) {
        this.amt2Ccy2 = amt2Ccy2;
    }

    /**
     * null
     * @return BRANCH_ID null
     */
    public String getBranchId() {
        return branchId;
    }

    /**
     * null
     * @param branchId null
     */
    public void setBranchId(String branchId) {
        this.branchId = branchId == null ? null : branchId.trim();
    }
}