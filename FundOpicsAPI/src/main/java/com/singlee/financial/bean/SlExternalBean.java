package com.singlee.financial.bean;

import java.io.Serializable;

public class SlExternalBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8122069995749863329L;
	/**
	 * 产品代码
	 */
	private String prodcode;
	/**
	 * 产品类型
	 */
	private String prodtype;
	/**
	 * 投资组合
	 */
	private String port;
	/**
	 * 成本中心
	 */
	private String cost;
	/**
	 * 中间人 一般默认D
	 */
	private String broker;
	/**
	 * 放inth的fedealno
	 */
	private String custrefno;
	/**
	 * 备注,放入原始交易（CFETS）的流水号
	 */
	private String dealtext;
	/**
	 * 备注
	 */
	private String notetext;

	/**
	 * 授权标识 1/Y-是 0/N-否
	 */
	private String siind;
	/**
	 * 授权标识 1/Y-是 0/N-否
	 */
	private String authsi;
	/**
	 * 复核标识 1/Y-是 0/N-否
	 */
	private String verind;
	/**
	 * 付款报文标识1/Y-是 0/N-否
	 */
	private String suppayind;
	/**
	 * 收款报文标识1/Y-是 0/N-否
	 */
	private String suprecind;
	/**
	 * 确认款报文标识1/Y-是 0/N-否
	 */
	private String supconfind;
	/**
	 * 清算路径1在NOST表中配置
	 */
	private String ccysmeans;
	/***
	 * 清算账户1在NOST表中配置
	 */
	private String ccysacct;
	/**
	 * 清算路径2在NOST表中配置
	 */
	private String ctrsmeans;
	/***
	 * 清算账户2在NOST表中配置
	 */
	private String ctrsacct;
	/***
	 * 浮动点差
	 */
	private String ccypd_8;
	
	private String usualid;

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public String getCustrefno() {
		return custrefno;
	}

	public void setCustrefno(String custrefno) {
		this.custrefno = custrefno;
	}

	public String getDealtext() {
		return dealtext;
	}

	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}

	public String getNotetext() {
		return notetext;
	}

	public void setNotetext(String notetext) {
		this.notetext = notetext;
	}

	public String getSiind() {
		return siind;
	}

	public void setSiind(String siind) {
		this.siind = siind;
	}

	public String getAuthsi() {
		return authsi;
	}

	public void setAuthsi(String authsi) {
		this.authsi = authsi;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public String getSuppayind() {
		return suppayind;
	}

	public void setSuppayind(String suppayind) {
		this.suppayind = suppayind;
	}

	public String getSuprecind() {
		return suprecind;
	}

	public void setSuprecind(String suprecind) {
		this.suprecind = suprecind;
	}

	public String getSupconfind() {
		return supconfind;
	}

	public void setSupconfind(String supconfind) {
		this.supconfind = supconfind;
	}

	public String getCcysmeans() {
		return ccysmeans;
	}

	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans;
	}

	public String getCcysacct() {
		return ccysacct;
	}

	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}

	public String getCtrsmeans() {
		return ctrsmeans;
	}

	public void setCtrsmeans(String ctrsmeans) {
		this.ctrsmeans = ctrsmeans;
	}

	public String getCtrsacct() {
		return ctrsacct;
	}

	public void setCtrsacct(String ctrsacct) {
		this.ctrsacct = ctrsacct;
	}

	public String getCcypd_8() {
		return ccypd_8;
	}

	public void setCcypd_8(String ccypd_8) {
		this.ccypd_8 = ccypd_8;
	}

	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}

	public String getUsualid() {
		return usualid;
	}
	

}
