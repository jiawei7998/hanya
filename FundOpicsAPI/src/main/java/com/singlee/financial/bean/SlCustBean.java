package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 交易对手基础处理
 * 
 * @author shenzl
 * 
 */
public class SlCustBean implements Serializable {
	/**
	 * 交易对手导入始化
	 * 
	 * @param br
	 *            业务部门分支
	 * @param tag
	 *            标识
	 * @param detail
	 *            细节
	 * @note
	 * 
	 *       1、交易导入 TAG="CUST" , DETAIL="ICUS"
	 * 
	 * 
	 */
	public SlCustBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.CUST.SEQ);
			inthBean.setInoutind(SlDealModule.CUST.INOUTIND);
			inthBean.setPriority(SlDealModule.CUST.PRIORITY);
			inthBean.setStatcode(SlDealModule.CUST.STATCODE);
			inthBean.setSyst(SlDealModule.CUST.SYST);
			inthBean.setIoper(SlDealModule.CUST.IOPER);
		}
	}

	public SlCustBean() {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6131508287688118304L;
	/**
	 * 交易对手编号
	 */
	private String cno;
	/**
	 * 交易对手短名
	 */
	private String cmne;
	/**
	 * 银行识别码
	 */
	private String bic;
	/**
	 * 证件发行国家
	 */
	private String ccode;
	/**
	 * 工业代码
	 */
	private String sic;
	/**
	 * 简称
	 */
	private String sn;
	/**
	 * 全称
	 */
	private String cfn1;
	private String cfn2;
	/**
	 * 地址
	 */
	private String ca1;
	private String ca2;
	private String ca3;
	private String ca4;
	private String ca5;
	/**
	 * Customer Type
	 */
	private String ctype;

	private String cpost;
	/**
	 * Ultimate Risk Country Code
	 */
	private String uccode;
	/**
	 * 注册日期
	 */
	private String birthdate;
	/**
	 * Taxpayer Identifier
	 */
	private String taxid;
	/**
	 * Accounting Type
	 */
	private String acctngtype;
	/**
	 * 
	 */
	private String notetext;
	/**
	 * 
	 */
	private String agentind;
	/**
	 * 
	 */
	private String supplemental;
	/**
	 * 
	 */
	private String brokid;
	/**
	 * 
	 */
	private String brokno;
	/**
	 * 最后维护日期
	 */
	private String lstmntdte;

	/**
	 * 接口流水
	 */
	private String interfaceNo;
	/**
	 * 证件类型
	 */
	private String gtype;
	/**
	 * 证件号码
	 */
	private String gid;
	/**
	 * 客户名称
	 */
	private String cliname;
	/**
	 * ECIF客户编号
	 */
	private String ecifNo;
	/**
	 * 境内外标志
	 */
	private String crossBordFlag;
	/**
	 * 金融许可证
	 */
	private String finLicenseCode;
	/**
	 * 国民行业类型
	 */
	private String ntnlIndstryTp;
	/**
	 * 国民经济类型
	 */
	private String ctznEcnmDeptTp;
	/**
	 * 金融机构类型
	 */
	private String finOrgType;
	/**
	 * 经营范围
	 */
	private String mngeScop;

	/**
	 * 客户建档柜员号
	 */
	private String custTelNo;

	/**
	 * 客户建档机构号
	 */
	private String custOrgNo;

	private String retCode;

	private String retMsg;

	private String retStatus;

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String getRetStatus() {
		return retStatus;
	}

	public void setRetStatus(String retStatus) {
		this.retStatus = retStatus;
	}

	public String getCrossBordFlag() {
		return crossBordFlag;
	}

	public void setCrossBordFlag(String crossBordFlag) {
		this.crossBordFlag = crossBordFlag;
	}

	public String getFinLicenseCode() {
		return finLicenseCode;
	}

	public void setFinLicenseCode(String finLicenseCode) {
		this.finLicenseCode = finLicenseCode;
	}

	public String getNtnlIndstryTp() {
		return ntnlIndstryTp;
	}

	public void setNtnlIndstryTp(String ntnlIndstryTp) {
		this.ntnlIndstryTp = ntnlIndstryTp;
	}

	public String getCtznEcnmDeptTp() {
		return ctznEcnmDeptTp;
	}

	public void setCtznEcnmDeptTp(String ctznEcnmDeptTp) {
		this.ctznEcnmDeptTp = ctznEcnmDeptTp;
	}

	public String getFinOrgType() {
		return finOrgType;
	}

	public void setFinOrgType(String finOrgType) {
		this.finOrgType = finOrgType;
	}

	public String getMngeScop() {
		return mngeScop;
	}

	public void setMngeScop(String mngeScop) {
		this.mngeScop = mngeScop;
	}

	private SlInthBean inthBean;

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCmne() {
		return cmne;
	}

	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getCcode() {
		return ccode;
	}

	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	public String getSic() {
		return sic;
	}

	public void setSic(String sic) {
		this.sic = sic;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getCfn1() {
		return cfn1;
	}

	public void setCfn1(String cfn1) {
		this.cfn1 = cfn1;
	}

	public String getCfn2() {
		return cfn2;
	}

	public void setCfn2(String cfn2) {
		this.cfn2 = cfn2;
	}

	public String getCa1() {
		return ca1;
	}

	public void setCa1(String ca1) {
		this.ca1 = ca1;
	}

	public String getCa2() {
		return ca2;
	}

	public void setCa2(String ca2) {
		this.ca2 = ca2;
	}

	public String getCa3() {
		return ca3;
	}

	public void setCa3(String ca3) {
		this.ca3 = ca3;
	}

	public String getCa4() {
		return ca4;
	}

	public void setCa4(String ca4) {
		this.ca4 = ca4;
	}

	public String getCa5() {
		return ca5;
	}

	public void setCa5(String ca5) {
		this.ca5 = ca5;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public String getCpost() {
		return cpost;
	}

	public void setCpost(String cpost) {
		this.cpost = cpost;
	}

	public String getUccode() {
		return uccode;
	}

	public void setUccode(String uccode) {
		this.uccode = uccode;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getTaxid() {
		return taxid;
	}

	public void setTaxid(String taxid) {
		this.taxid = taxid;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public void setInterfaceNo(String interfaceNo) {
		this.interfaceNo = interfaceNo;
	}

	public String getInterfaceNo() {
		return interfaceNo;
	}

	public void setGtype(String gtype) {
		this.gtype = gtype;
	}

	public String getGtype() {
		return gtype;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getGid() {
		return gid;
	}

	public void setCliname(String cliname) {
		this.cliname = cliname;
	}

	public String getCliname() {
		return cliname;
	}

	public void setNotetext(String notetext) {
		this.notetext = notetext;
	}

	public String getNotetext() {
		return notetext;
	}

	public String getAgentind() {
		return agentind;
	}

	public void setAgentind(String agentind) {
		this.agentind = agentind;
	}

	public String getSupplemental() {
		return supplemental;
	}

	public void setSupplemental(String supplemental) {
		this.supplemental = supplemental;
	}

	public String getBrokid() {
		return brokid;
	}

	public void setBrokid(String brokid) {
		this.brokid = brokid;
	}

	public String getBrokno() {
		return brokno;
	}

	public void setBrokno(String brokno) {
		this.brokno = brokno;
	}

	public void setLstmntdte(String lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getLstmntdte() {
		return lstmntdte;
	}

	public void setEcifNo(String ecifNo) {
		this.ecifNo = ecifNo;
	}

	public String getEcifNo() {
		return ecifNo;
	}

	public String getCustTelNo() {
		return custTelNo;
	}

	public void setCustTelNo(String custTelNo) {
		this.custTelNo = custTelNo;
	}

	public String getCustOrgNo() {
		return custOrgNo;
	}

	public void setCustOrgNo(String custOrgNo) {
		this.custOrgNo = custOrgNo;
	}
}