package com.singlee.financial.bean;

import java.io.Serializable;
/**
 * 华商银行，查询核心外汇交易
 * @author zhengfl
 *
 */
public class SlCoreFxBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String DATASOURCE = "COREFX";// varchar(20),
	private String LENGTH ;// 报文长度 LENGTH char(4)
	private String PRCSCD ;// 处理码 PRCSCD char(3) 400
	private String RETUCD ;// 响应码 RETUCD char(4)
	private String RETNUM ;// 剩余记录数 retnum char(4)
	//
	private String RECDNM ;// 本次返回记录数 recdnm char(3)
	//
	private String TRANTP ;// 交易类型 TranTp char(2) SP即期 FW远期 SW掉期
	
	private String TRANSQ ;// 交易编号 transq char(12)
	private String TRANDT ;// 交易日期 trandt char(8)
	private String BGINDT ;// 起息日期 bgindt char(8)
	private String PS ;// 交易方向 tarndre char(1) p 买 s 卖
	private String ACCTNO ;// 交易对手账号 acctno char(20)
	private String ACCTNA ;// 交易对手名称 acctna char(60)
	private String CRCYCD1 ;// 交易币种 crcycd1 char(2)
	private String TRANAM1 ;// 交易金额 tranam1 char(20)
	private String CRCYCD2 ;// 交易对手币种 crcycd2 char(2)
	private String TRANAM2 ;// 交易对手金额 tranam2 char(20)
	private String FBGINDT ;// 远端起息日期 fordate char(8)
	private String FTRANAM2 ;// 远端交易对手金额 tranam3 char(20)
	private String EXCHRT ;// 成交汇率 exchrt char(15)
	private String SPOTRATE ;// 实时汇率 ertu2c char(15)
	private String TRANBR ;// 交易机构 tranbr char(4)
	private String REVFLAG ;// 冲销标识 revflg char(1) 0:正常 1:冲销
	private String O_RETCODE ;// CHAR(3) OUTPUT,-- 999成功，其它失败
	private String O_RETMSG ;// VARCHAR(100) OUTPUT--

	public String getDATASOURCE() {
		return DATASOURCE;
	}

	public void setDATASOURCE(String dATASOURCE) {
		DATASOURCE = dATASOURCE;
	}

	public String getLENGTH() {
		return LENGTH;
	}

	public void setLENGTH(String lENGTH) {
		LENGTH = lENGTH;
	}

	public String getPRCSCD() {
		return PRCSCD;
	}

	public void setPRCSCD(String pRCSCD) {
		PRCSCD = pRCSCD;
	}

	public String getRETNUM() {
		return RETNUM;
	}

	public void setRETNUM(String rETNUM) {
		RETNUM = rETNUM;
	}

	public String getRECDNM() {
		return RECDNM;
	}

	public void setRECDNM(String rECDNM) {
		RECDNM = rECDNM;
	}

	public String getTRANSQ() {
		return TRANSQ;
	}

	public void setTRANSQ(String tRANSQ) {
		TRANSQ = tRANSQ;
	}

	public String getTRANDT() {
		return TRANDT;
	}

	public void setTRANDT(String tRANDT) {
		TRANDT = tRANDT;
	}

	public String getBGINDT() {
		return BGINDT;
	}

	public void setBGINDT(String bGINDT) {
		BGINDT = bGINDT;
	}

	public String getPS() {
		return PS;
	}

	public void setPS(String pS) {
		PS = pS;
	}

	public String getACCTNO() {
		return ACCTNO;
	}

	public void setACCTNO(String aCCTNO) {
		ACCTNO = aCCTNO;
	}

	public String getACCTNA() {
		return ACCTNA;
	}

	public void setACCTNA(String aCCTNA) {
		ACCTNA = aCCTNA;
	}

	public String getCRCYCD1() {
		return CRCYCD1;
	}

	public void setCRCYCD1(String cRCYCD1) {
		CRCYCD1 = cRCYCD1;
	}

	public String getTRANAM1() {
		return TRANAM1;
	}

	public void setTRANAM1(String tRANAM1) {
		TRANAM1 = tRANAM1;
	}

	public String getCRCYCD2() {
		return CRCYCD2;
	}

	public void setCRCYCD2(String cRCYCD2) {
		CRCYCD2 = cRCYCD2;
	}

	public String getTRANAM2() {
		return TRANAM2;
	}

	public void setTRANAM2(String tRANAM2) {
		TRANAM2 = tRANAM2;
	}

	public String getEXCHRT() {
		return EXCHRT;
	}

	public void setEXCHRT(String eXCHRT) {
		EXCHRT = eXCHRT;
	}

	public String getTRANBR() {
		return TRANBR;
	}

	public void setTRANBR(String tRANBR) {
		TRANBR = tRANBR;
	}

	public String getSPOTRATE() {
		return SPOTRATE;
	}

	public void setSPOTRATE(String sPOTRATE) {
		SPOTRATE = sPOTRATE;
	}

	public String getREVFLAG() {
		return REVFLAG;
	}

	public void setREVFLAG(String rEVFLAG) {
		REVFLAG = rEVFLAG;
	}

	public String getFBGINDT() {
		return FBGINDT;
	}

	public void setFBGINDT(String fBGINDT) {
		FBGINDT = fBGINDT;
	}

	public String getFTRANAM2() {
		return FTRANAM2;
	}

	public void setFTRANAM2(String fTRANAM2) {
		FTRANAM2 = fTRANAM2;
	}

	public String getO_RETCODE() {
		return O_RETCODE;
	}

	public void setO_RETCODE(String oRETCODE) {
		O_RETCODE = oRETCODE;
	}

	public String getO_RETMSG() {
		return O_RETMSG;
	}

	public void setO_RETMSG(String oRETMSG) {
		O_RETMSG = oRETMSG;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRETUCD() {
		return RETUCD;
	}

	public void setRETUCD(String rETUCD) {
		RETUCD = rETUCD;
	}

	public String getTRANTP() {
		return TRANTP;
	}

	public void setTRANTP(String tRANTP) {
		TRANTP = tRANTP;
	}

}
