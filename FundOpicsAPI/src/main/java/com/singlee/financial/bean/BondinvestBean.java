package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * A1403-外资银行资产负债项目月报表
 * @author wangzhao
 *
 */
public class BondinvestBean implements Serializable{
	  private static final long serialVersionUID = 1L;
	  private String glno;
	  private String glnonm;
	  private String firstbr;
	  private String firstbr_address;
	  private String seconbr;
	  private String seconbr_address;
	  private String secid;
	  private String issuer_nm;
	  private String isstype;
	  private String isstype_sec;
	  private String sic;
	  private String issuer_nation;
	  private String issuer_credit;
	  private String issuer_area;
	  private String issdate;
	  private String mdate;
	  private String initerm_cn;
	  private String remianterm_cn;
	  private String sec_credit;
	  private Double couprate_8;
	  private String ccy_nm;
	  private Double sec_prinamt;
	  private Double sec_tdymtm;
	  private Double sec_unamortamt;
	  private Double sec_mkvalue;
	  private Double tdy_intamt;
	  private Double sec_priamt_cny;
	  private String rpdate;
	public String getGlno() {
		return glno;
	}
	public void setGlno(String glno) {
		this.glno = glno;
	}
	public String getGlnonm() {
		return glnonm;
	}
	public void setGlnonm(String glnonm) {
		this.glnonm = glnonm;
	}
	public String getFirstbr() {
		return firstbr;
	}
	public void setFirstbr(String firstbr) {
		this.firstbr = firstbr;
	}
	public String getFirstbr_address() {
		return firstbr_address;
	}
	public void setFirstbr_address(String firstbr_address) {
		this.firstbr_address = firstbr_address;
	}
	public String getSeconbr() {
		return seconbr;
	}
	public void setSeconbr(String seconbr) {
		this.seconbr = seconbr;
	}
	public String getSeconbr_address() {
		return seconbr_address;
	}
	public void setSeconbr_address(String seconbr_address) {
		this.seconbr_address = seconbr_address;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public String getIssuer_nm() {
		return issuer_nm;
	}
	public void setIssuer_nm(String issuer_nm) {
		this.issuer_nm = issuer_nm;
	}
	public String getIsstype() {
		return isstype;
	}
	public void setIsstype(String isstype) {
		this.isstype = isstype;
	}
	public String getIsstype_sec() {
		return isstype_sec;
	}
	public void setIsstype_sec(String isstype_sec) {
		this.isstype_sec = isstype_sec;
	}
	public String getSic() {
		return sic;
	}
	public void setSic(String sic) {
		this.sic = sic;
	}
	public String getIssuer_nation() {
		return issuer_nation;
	}
	public void setIssuer_nation(String issuer_nation) {
		this.issuer_nation = issuer_nation;
	}
	public String getIssuer_credit() {
		return issuer_credit;
	}
	public void setIssuer_credit(String issuer_credit) {
		this.issuer_credit = issuer_credit;
	}
	public String getIssuer_area() {
		return issuer_area;
	}
	public void setIssuer_area(String issuer_area) {
		this.issuer_area = issuer_area;
	}
	public String getIssdate() {
		return issdate;
	}
	public void setIssdate(String issdate) {
		this.issdate = issdate;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
	public String getIniterm_cn() {
		return initerm_cn;
	}
	public void setIniterm_cn(String initerm_cn) {
		this.initerm_cn = initerm_cn;
	}
	public String getRemianterm_cn() {
		return remianterm_cn;
	}
	public void setRemianterm_cn(String remianterm_cn) {
		this.remianterm_cn = remianterm_cn;
	}
	public String getSec_credit() {
		return sec_credit;
	}
	public void setSec_credit(String sec_credit) {
		this.sec_credit = sec_credit;
	}
	public Double getCouprate_8() {
		return couprate_8;
	}
	public void setCouprate_8(Double couprate_8) {
		this.couprate_8 = couprate_8;
	}
	public String getCcy_nm() {
		return ccy_nm;
	}
	public void setCcy_nm(String ccy_nm) {
		this.ccy_nm = ccy_nm;
	}
	public Double getSec_prinamt() {
		return sec_prinamt;
	}
	public void setSec_prinamt(Double sec_prinamt) {
		this.sec_prinamt = sec_prinamt;
	}
	public Double getSec_tdymtm() {
		return sec_tdymtm;
	}
	public void setSec_tdymtm(Double sec_tdymtm) {
		this.sec_tdymtm = sec_tdymtm;
	}
	public Double getSec_unamortamt() {
		return sec_unamortamt;
	}
	public void setSec_unamortamt(Double sec_unamortamt) {
		this.sec_unamortamt = sec_unamortamt;
	}
	public Double getSec_mkvalue() {
		return sec_mkvalue;
	}
	public void setSec_mkvalue(Double sec_mkvalue) {
		this.sec_mkvalue = sec_mkvalue;
	}
	public Double getTdy_intamt() {
		return tdy_intamt;
	}
	public void setTdy_intamt(Double tdy_intamt) {
		this.tdy_intamt = tdy_intamt;
	}
	public Double getSec_priamt_cny() {
		return sec_priamt_cny;
	}
	public void setSec_priamt_cny(Double sec_priamt_cny) {
		this.sec_priamt_cny = sec_priamt_cny;
	}
	public String getRpdate() {
		return rpdate;
	}
	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}
	@Override
	public String toString() {
		return "BondinvestBean [glno=" + glno + ", glnonm=" + glnonm
				+ ", firstbr=" + firstbr + ", firstbr_address="
				+ firstbr_address + ", seconbr=" + seconbr
				+ ", seconbr_address=" + seconbr_address + ", secid=" + secid
				+ ", issuer_nm=" + issuer_nm + ", isstype=" + isstype
				+ ", isstype_sec=" + isstype_sec + ", sic=" + sic
				+ ", issuer_nation=" + issuer_nation + ", issuer_credit="
				+ issuer_credit + ", issuer_area=" + issuer_area + ", issdate="
				+ issdate + ", mdate=" + mdate + ", initerm_cn=" + initerm_cn
				+ ", remianterm_cn=" + remianterm_cn + ", sec_credit="
				+ sec_credit + ", couprate_8=" + couprate_8 + ", ccy_nm="
				+ ccy_nm + ", sec_prinamt=" + sec_prinamt + ", sec_tdymtm="
				+ sec_tdymtm + ", sec_unamortamt=" + sec_unamortamt
				+ ", sec_mkvalue=" + sec_mkvalue + ", tdy_intamt=" + tdy_intamt
				+ ", sec_priamt_cny=" + sec_priamt_cny + ", rpdate=" + rpdate
				+ "]";
	}

}
