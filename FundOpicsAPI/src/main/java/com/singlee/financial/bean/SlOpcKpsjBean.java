package com.singlee.financial.bean;

import java.io.Serializable;

public class SlOpcKpsjBean implements Serializable {

	private static final long serialVersionUID = 1901551160942215641L;

	private String dealno;
	private String product;
	private String type;
	private String drcrind;
	private String amount;
	private String glno;
	private String cmne;
	private String postdate;
	private String code;

	private String descr;
	private String secid;
	private String invtype;
	private String acctngtype;
	private String coreglno;

	private String br;
	private String cost;
	private String ps;
	private String dealdate;
	private String settdate;

	private String czsy;

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDrcrind() {
		return drcrind;
	}

	public void setDrcrind(String drcrind) {
		this.drcrind = drcrind;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getGlno() {
		return glno;
	}

	public void setGlno(String glno) {
		this.glno = glno;
	}

	public String getCmne() {
		return cmne;
	}

	public void setCmne(String cmne) {
		this.cmne = cmne;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public String getCoreglno() {
		return coreglno;
	}

	public void setCoreglno(String coreglno) {
		this.coreglno = coreglno;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getDealdate() {
		return dealdate;
	}

	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}

	public String getSettdate() {
		return settdate;
	}

	public void setSettdate(String settdate) {
		this.settdate = settdate;
	}

	public String getCzsy() {
		return czsy;
	}

	public void setCzsy(String czsy) {
		this.czsy = czsy;
	}

}
