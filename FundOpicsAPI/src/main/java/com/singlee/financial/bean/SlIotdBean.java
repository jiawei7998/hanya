package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/***
 * 期权表
 * 
 * @author lij
 *
 */
public class SlIotdBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * INTH 头表信息
	 */
	private SlInthBean inthBean;

	/**
	 * OPICS拓展属性
	 */
	private SlExternalBean externalBean;
	/***
	 * 买卖方向
	 */
	private String ps;
	/***
	 * 币种1
	 */
	private String ccy;
	/***
	 * 币种1看涨/看跌
	 */
	private String ccyCp;
	/***
	 * 币种1金额
	 */
	private BigDecimal ccyPrin;

	/***
	 * 币种2
	 */
	private String ctrCcy;
	/***
	 * 币种2看涨/看跌
	 */
	private String ctrCp;
	/***
	 * 币种2金额
	 */
	private BigDecimal ctrPrin;

	/***
	 * 交易对手
	 */
	private String cno;

	/***
	 * 期权终止时间
	 */
	private String cutOff;

	/***
	 * 交易日期
	 */
	private Date dealDate;

	/***
	 * 交易生效日期
	 */
	private Date dealVDate;

	/***
	 * 交易详情
	 */
	private String dealText;

	/***
	 * 交易交割日期
	 */
	private Date settleDte;

	/***
	 * 交易到期日
	 */
	private Date expDate;
	/**
	 * 行权-Y,到期-2
	 */
	private String exerind;
	
	/**
	 * 行权日期
	 */
	private Date exerdate;
	/***
	 * 录入日期
	 */
	private Date inputDate;

	/***
	 * 录入时间
	 */
	private String inputTime;

	/***
	 * 期权类型
	 */
	private String otcType;
	/***
	 * 损益计算方法 固定为 M M - Trading D - Hedged
	 */
	private String plmethod;

	/**
	 * 行权参考汇率（执行价格）
	 */
	private BigDecimal strike_8;

	/***
	 * 期权类型 EURO - 欧式 AMER - 美式
	 */
	private String style;

	/**
	 * 期限
	 */
	private String tenor;

	/**
	 * 交易员
	 */
	private String trad;

	/**
	 * 
	 */
	private String unit;

	/**
	 * 复核人员
	 */
	private String voper;

	/**
	 * 期权费货币
	 */
	private String premCcy;

	/**
	 * 期权费
	 */
	private BigDecimal premAmt;

	/**
	 * 期权 清算路径
	 */
	private String premsmeans;
	/**
	 * 期权 清算账户
	 */
	private String premsacct;

	/**
	 * 默认地方时间，默认'BEIJING'
	 */
	private String location;

	/**
	 * 估值模型‘FINCAD’
	 */
	private String mathSource;

	/**
	 * 计算方 1 - Branch Customer(自己) 2 - Counterparty(交易对手) 3 - Joint(两方)
	 */
	private String calcaGent;

	/**
	 * 期权支付币种
	 */
	private String binaryCcy;

	/**
	 * 支付费用
	 */
	private BigDecimal binary;

	/**
	 * 
	 */
	private String exotic;

	/**
	 * 障碍价格
	 */
	private BigDecimal kitrig_8;

	/**
	 * 障碍价格
	 */
	private BigDecimal kotrig_8;

	/**
	 * 障碍价格上限
	 */
	private BigDecimal dkitrig1_8;

	/**
	 * 障碍价格下限
	 */
	private BigDecimal dkitrig2_8;
	/**
	 * 障碍价格上限
	 */
	private BigDecimal dkotrig1_8;

	/**
	 * 障碍价格下限
	 */
	private BigDecimal dkotrig2_8;

	/**
	 * 是由VOLD中自动获取的
	 */
	private BigDecimal volatility_8;

	/**
	 * 
	 */
	private BigDecimal delta_8;

	/**
	 * 
	 */
	private BigDecimal rate1_8;

	/**
	 * 冲销日期
	 */
	private Date revDate;

	/**
	 * opics交易界面 “premium %”:注意传入2到opics会显示成200;所以要除以100
	 */
	private BigDecimal premrate_8;

	/**
	 * 对应opics交易界面“base Conv. Spot rate”
	 */
	private BigDecimal ccyprembamt;

	/**
	 * 对应opcis界面“base Premium Amount”
	 */
	private BigDecimal ccybrate_8;
	/**
	 * otc fi 期权对应的债券号
	 */
	private String secid;

	/**
	 * 期权初始化
	 */
	public SlIotdBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.OTC.SEQ);
			inthBean.setInoutind(SlDealModule.OTC.INOUTIND);
			inthBean.setPriority(SlDealModule.OTC.PRIORITY);
			inthBean.setStatcode(SlDealModule.OTC.STATCODE);
			inthBean.setSyst(SlDealModule.OTC.SYST);
			inthBean.setIoper(SlDealModule.OTC.IOPER);
		}
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public SlExternalBean getExternalBean() {
		return externalBean;
	}

	public void setExternalBean(SlExternalBean externalBean) {
		this.externalBean = externalBean;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcyCp() {
		return ccyCp;
	}

	public void setCcyCp(String ccyCp) {
		this.ccyCp = ccyCp;
	}

	public BigDecimal getCcyPrin() {
		return ccyPrin;
	}

	public void setCcyPrin(BigDecimal ccyPrin) {
		this.ccyPrin = ccyPrin;
	}

	public String getCtrCcy() {
		return ctrCcy;
	}

	public void setCtrCcy(String ctrCcy) {
		this.ctrCcy = ctrCcy;
	}

	public String getCtrCp() {
		return ctrCp;
	}

	public void setCtrCp(String ctrCp) {
		this.ctrCp = ctrCp;
	}

	public BigDecimal getCtrPrin() {
		return ctrPrin;
	}

	public void setCtrPrin(BigDecimal ctrPrin) {
		this.ctrPrin = ctrPrin;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public String getCutOff() {
		return cutOff;
	}

	public void setCutOff(String cutOff) {
		this.cutOff = cutOff;
	}

	public Date getDealDate() {
		return dealDate;
	}

	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	public Date getDealVDate() {
		return dealVDate;
	}

	public void setDealVDate(Date dealVDate) {
		this.dealVDate = dealVDate;
	}

	public String getDealText() {
		return dealText;
	}

	public void setDealText(String dealText) {
		this.dealText = dealText;
	}

	public Date getSettleDte() {
		return settleDte;
	}

	public void setSettleDte(Date settleDte) {
		this.settleDte = settleDte;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public Date getInputDate() {
		return inputDate;
	}

	public void setInputDate(Date inputDate) {
		this.inputDate = inputDate;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getOtcType() {
		return otcType;
	}

	public void setOtcType(String otcType) {
		this.otcType = otcType;
	}

	public String getPlmethod() {
		return plmethod;
	}

	public void setPlmethod(String plmethod) {
		this.plmethod = plmethod;
	}

	public BigDecimal getStrike_8() {
		return strike_8;
	}

	public void setStrike_8(BigDecimal strike_8) {
		this.strike_8 = strike_8;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper;
	}

	public String getPremCcy() {
		return premCcy;
	}

	public void setPremCcy(String premCcy) {
		this.premCcy = premCcy;
	}

	public BigDecimal getPremAmt() {
		return premAmt;
	}

	public void setPremAmt(BigDecimal premAmt) {
		this.premAmt = premAmt;
	}

	public String getPremsmeans() {
		return premsmeans;
	}

	public void setPremsmeans(String premsmeans) {
		this.premsmeans = premsmeans;
	}

	public String getPremsacct() {
		return premsacct;
	}

	public void setPremsacct(String premsacct) {
		this.premsacct = premsacct;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMathSource() {
		return mathSource;
	}

	public void setMathSource(String mathSource) {
		this.mathSource = mathSource;
	}

	public String getCalcaGent() {
		return calcaGent;
	}

	public void setCalcaGent(String calcaGent) {
		this.calcaGent = calcaGent;
	}

	public String getBinaryCcy() {
		return binaryCcy;
	}

	public void setBinaryCcy(String binaryCcy) {
		this.binaryCcy = binaryCcy;
	}

	public BigDecimal getBinary() {
		return binary;
	}

	public void setBinary(BigDecimal binary) {
		this.binary = binary;
	}

	public String getExotic() {
		return exotic;
	}

	public void setExotic(String exotic) {
		this.exotic = exotic;
	}

	public BigDecimal getKitrig_8() {
		return kitrig_8;
	}

	public void setKitrig_8(BigDecimal kitrig_8) {
		this.kitrig_8 = kitrig_8;
	}

	public BigDecimal getKotrig_8() {
		return kotrig_8;
	}

	public void setKotrig_8(BigDecimal kotrig_8) {
		this.kotrig_8 = kotrig_8;
	}

	public BigDecimal getDkitrig1_8() {
		return dkitrig1_8;
	}

	public void setDkitrig1_8(BigDecimal dkitrig1_8) {
		this.dkitrig1_8 = dkitrig1_8;
	}

	public BigDecimal getDkitrig2_8() {
		return dkitrig2_8;
	}

	public void setDkitrig2_8(BigDecimal dkitrig2_8) {
		this.dkitrig2_8 = dkitrig2_8;
	}

	public BigDecimal getDkotrig1_8() {
		return dkotrig1_8;
	}

	public void setDkotrig1_8(BigDecimal dkotrig1_8) {
		this.dkotrig1_8 = dkotrig1_8;
	}

	public BigDecimal getDkotrig2_8() {
		return dkotrig2_8;
	}

	public void setDkotrig2_8(BigDecimal dkotrig2_8) {
		this.dkotrig2_8 = dkotrig2_8;
	}

	public BigDecimal getVolatility_8() {
		return volatility_8;
	}

	public void setVolatility_8(BigDecimal volatility_8) {
		this.volatility_8 = volatility_8;
	}

	public BigDecimal getDelta_8() {
		return delta_8;
	}

	public void setDelta_8(BigDecimal delta_8) {
		this.delta_8 = delta_8;
	}

	public BigDecimal getRate1_8() {
		return rate1_8;
	}

	public void setRate1_8(BigDecimal rate1_8) {
		this.rate1_8 = rate1_8;
	}

	public Date getRevDate() {
		return revDate;
	}

	public void setRevDate(Date revDate) {
		this.revDate = revDate;
	}

	public BigDecimal getPremrate_8() {
		return premrate_8;
	}

	public void setPremrate_8(BigDecimal premrate_8) {
		this.premrate_8 = premrate_8;
	}

	public BigDecimal getCcyprembamt() {
		return ccyprembamt;
	}

	public void setCcyprembamt(BigDecimal ccyprembamt) {
		this.ccyprembamt = ccyprembamt;
	}

	public BigDecimal getCcybrate_8() {
		return ccybrate_8;
	}

	public void setCcybrate_8(BigDecimal ccybrate_8) {
		this.ccybrate_8 = ccybrate_8;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public final String getExerind() {
		return exerind;
	}

	public final void setExerind(String exerind) {
		this.exerind = exerind;
	}

	public final Date getExerdate() {
		return exerdate;
	}

	public final void setExerdate(Date exerdate) {
		this.exerdate = exerdate;
	}

}
