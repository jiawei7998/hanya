package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

public class SlInthBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4358555608432073998L;
	/**
	 * 分支代码
	 */
	private String br;
	/**
	 * 服务,定义每种交易类型标志
	 */
	private String server;
	/**
	 * 交易前端流水号<外部流水号>
	 */
	private String fedealno;
	/**
	 * 序列号,一般默认0
	 */
	private String seq;
	/**
	 * IN/OUT标识,一般默认I
	 */
	private String inoutind;
	/**
	 * 优先级,值越小优先级越高一般1即可
	 */
	private String priority;
	/**
	 * 程序细节描述,表面程序来自OPICS模块录入
	 */
	private String tag;
	/**
	 * 细节接口记录,一般默认对应接口表
	 */
	private String detail;
	/**
	 * 状态码-1:初始状态,-3:正在处理,-4:处理成功,-5:交易失效,-6:交易错误,需要修复
	 */
	private String statcode;
	/**
	 * 接口错误码0:成功，其它码值参照OPICS文档
	 */
	private String errorcode;
	/**
	 * 系统描述,交易来那里SING
	 */
	private String syst;
	/**
	 * 操作员
	 */
	private String ioper;
	/**
	 * 交易前端录入日期
	 */
	private Date feidate;
	/**
	 * 交易前端录入时间
	 */
	private String feitime;
	/**
	 * 交易流水号,statuscode=4时会有值
	 */
	private String dealno;
	/**
	 * 掉期交易流水号
	 */
	private String swapdealno;
	/**
	 * 后台输出 日期
	 */
	private String boodate;
	/**
	 * 后台输出时间
	 */
	private String bootime;
	/**
	 * 最后维护日期
	 */
	private Date lstmntdate;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getInoutind() {
		return inoutind;
	}

	public void setInoutind(String inoutind) {
		this.inoutind = inoutind;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getStatcode() {
		return statcode;
	}

	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public String getSyst() {
		return syst;
	}

	public void setSyst(String syst) {
		this.syst = syst;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public Date getFeidate() {
		return feidate;
	}

	public void setFeidate(Date feidate) {
		this.feidate = feidate;
	}

	public String getFeitime() {
		return feitime;
	}

	public void setFeitime(String feitime) {
		this.feitime = feitime;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSwapdealno() {
		return swapdealno;
	}

	public void setSwapdealno(String swapdealno) {
		this.swapdealno = swapdealno;
	}

	public String getBoodate() {
		return boodate;
	}

	public void setBoodate(String boodate) {
		this.boodate = boodate;
	}

	public String getBootime() {
		return bootime;
	}

	public void setBootime(String bootime) {
		this.bootime = bootime;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
