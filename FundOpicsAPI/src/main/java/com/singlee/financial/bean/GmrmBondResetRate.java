package com.singlee.financial.bean;
import java.io.Serializable;
import java.math.BigDecimal;


public class GmrmBondResetRate implements Serializable{


    /**
	 * 
	 */
	private static final long serialVersionUID = -8372835963029784769L;

	/**
     * 数据日期 YYYYMMDD
     */
    private String datadate;

    /**
     * 债券序号
     */
    private String secId;

    /**
     * 重置日期 YYYYMMDD
     */
    private String resetDate;

    /**
     * 生效日期 YYYYMMDD
     */
    private String effectiveDate;

    /**
     * 利率
     */
    private BigDecimal rate;

    /**
     * 数据日期 YYYYMMDD
     * @return DATADATE 数据日期 YYYYMMDD
     */
    public String getDatadate() {
        return datadate;
    }

    /**
     * 数据日期 YYYYMMDD
     * @param datadate 数据日期 YYYYMMDD
     */
    public void setDatadate(String datadate) {
        this.datadate = datadate == null ? null : datadate.trim();
    }

    /**
     * 债券序号
     * @return SEC_ID 债券序号
     */
    public String getSecId() {
        return secId;
    }

    /**
     * 债券序号
     * @param secId 债券序号
     */
    public void setSecId(String secId) {
        this.secId = secId == null ? null : secId.trim();
    }

    /**
     * 重置日期 YYYYMMDD
     * @return RESET_DATE 重置日期 YYYYMMDD
     */
    public String getResetDate() {
        return resetDate;
    }

    /**
     * 重置日期 YYYYMMDD
     * @param resetDate 重置日期 YYYYMMDD
     */
    public void setResetDate(String resetDate) {
        this.resetDate = resetDate == null ? null : resetDate.trim();
    }

    /**
     * 生效日期 YYYYMMDD
     * @return EFFECTIVE_DATE 生效日期 YYYYMMDD
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * 生效日期 YYYYMMDD
     * @param effectiveDate 生效日期 YYYYMMDD
     */
    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate == null ? null : effectiveDate.trim();
    }

    /**
     * 利率
     * @return RATE 利率
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * 利率
     * @param rate 利率
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}