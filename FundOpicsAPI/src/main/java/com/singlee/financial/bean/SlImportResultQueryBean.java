package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

public class SlImportResultQueryBean implements Serializable{
	/**
	 * 查询市场数据
	 */
	private static final long serialVersionUID = -1138652466087571816L;
	private String DATATYPE;
	  private String BR;
	  private Date BRANPRCDATE;
	  private int IMPSEQ;
	  private int TOTAL;
	  private int SUCCESS;
	  private int FAILD;
	  private int PANDDING;
	  private int CANCEL;
	  private int OPICS_SUCCESS;
	  private int OPICS_FAILD;
	  private int OPICS_PENDDING;
	  private int OPICS_REPAIR;
	  private int OPICS_OTHERS;
	public String getDATATYPE() {
		return DATATYPE;
	}
	public void setDATATYPE(String dATATYPE) {
		DATATYPE = dATATYPE;
	}
	public String getBR() {
		return BR;
	}
	public void setBR(String bR) {
		BR = bR;
	}
	public Date getBRANPRCDATE() {
		return BRANPRCDATE;
	}
	public void setBRANPRCDATE(Date bRANPRCDATE) {
		BRANPRCDATE = bRANPRCDATE;
	}
	public int getIMPSEQ() {
		return IMPSEQ;
	}
	public void setIMPSEQ(int iMPSEQ) {
		IMPSEQ = iMPSEQ;
	}
	public int getTOTAL() {
		return TOTAL;
	}
	public void setTOTAL(int tOTAL) {
		TOTAL = tOTAL;
	}
	public int getSUCCESS() {
		return SUCCESS;
	}
	public void setSUCCESS(int sUCCESS) {
		SUCCESS = sUCCESS;
	}
	public int getFAILD() {
		return FAILD;
	}
	public void setFAILD(int fAILD) {
		FAILD = fAILD;
	}
	public int getPANDDING() {
		return PANDDING;
	}
	public void setPANDDING(int pANDDING) {
		PANDDING = pANDDING;
	}
	public int getCANCEL() {
		return CANCEL;
	}
	public void setCANCEL(int cANCEL) {
		CANCEL = cANCEL;
	}
	public int getOPICS_SUCCESS() {
		return OPICS_SUCCESS;
	}
	public void setOPICS_SUCCESS(int oPICS_SUCCESS) {
		OPICS_SUCCESS = oPICS_SUCCESS;
	}
	public int getOPICS_FAILD() {
		return OPICS_FAILD;
	}
	public void setOPICS_FAILD(int oPICS_FAILD) {
		OPICS_FAILD = oPICS_FAILD;
	}
	public int getOPICS_PENDDING() {
		return OPICS_PENDDING;
	}
	public void setOPICS_PENDDING(int oPICS_PENDDING) {
		OPICS_PENDDING = oPICS_PENDDING;
	}
	public int getOPICS_REPAIR() {
		return OPICS_REPAIR;
	}
	public void setOPICS_REPAIR(int oPICS_REPAIR) {
		OPICS_REPAIR = oPICS_REPAIR;
	}
	public int getOPICS_OTHERS() {
		return OPICS_OTHERS;
	}
	public void setOPICS_OTHERS(int oPICS_OTHERS) {
		OPICS_OTHERS = oPICS_OTHERS;
	}
	  

}
