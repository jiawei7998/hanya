package com.singlee.financial.bean;

public class SlMmktIselBean extends SlMarketDataBaseBean{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8804298136233193270L;
	protected String SECID;
	  protected String SECNAME;
	  protected String CCY;
	  protected String VALDATE;
	  protected String PUBLICCOMM;
	  protected String PUBLICCOMMTXT;
	  protected String REMAINTERM;
	  protected String DAYTIMEPRICE;
	  protected String DAYTIMEINT;
	  protected String NETPRICE;
	  protected String VALYIELD;
	  protected String VALDURATION;
	  protected String CONVEXITY;
	  protected String VALBASIC;
	  protected String SPREADDUR;
	  protected String SPREADCON;
	  protected String WAVGPRICE;
	  protected String WAVGNETPRICE;
	  protected String WAVGYIELD;
	  protected String WAVGDURATION;
	  protected String WAVGCON;
	  protected String WAVGBP;
	  protected String WAVGSPDUR;
	  protected String WAVGSPCON;
	  protected String CREDIBILITY;
	  protected String VALRATEDUR;
	  protected String VALRATECON;
	  protected String WAVGRATEDUR;
	  protected String WAVGRATECON;
	  protected String CLOSEPRICE;
	  protected String CLOSEINT;
	  protected String REMAINAMT;
	  protected String SPREADYIELD;
	  protected String CURVEID;
	  protected String COMMSECID;
	  protected String EXERCISERATE;
	  protected String YSTCLOSEPRICE;
	  protected String TDYYSTDIFF;
	public String getSECID() {
		return SECID;
	}
	public void setSECID(String sECID) {
		SECID = sECID;
	}
	public String getSECNAME() {
		return SECNAME;
	}
	public void setSECNAME(String sECNAME) {
		SECNAME = sECNAME;
	}
	public String getCCY() {
		return CCY;
	}
	public void setCCY(String cCY) {
		CCY = cCY;
	}
	public String getVALDATE() {
		return VALDATE;
	}
	public void setVALDATE(String vALDATE) {
		VALDATE = vALDATE;
	}
	public String getPUBLICCOMM() {
		return PUBLICCOMM;
	}
	public void setPUBLICCOMM(String pUBLICCOMM) {
		PUBLICCOMM = pUBLICCOMM;
	}
	public String getPUBLICCOMMTXT() {
		return PUBLICCOMMTXT;
	}
	public void setPUBLICCOMMTXT(String pUBLICCOMMTXT) {
		PUBLICCOMMTXT = pUBLICCOMMTXT;
	}
	public String getREMAINTERM() {
		return REMAINTERM;
	}
	public void setREMAINTERM(String rEMAINTERM) {
		REMAINTERM = rEMAINTERM;
	}
	public String getDAYTIMEPRICE() {
		return DAYTIMEPRICE;
	}
	public void setDAYTIMEPRICE(String dAYTIMEPRICE) {
		DAYTIMEPRICE = dAYTIMEPRICE;
	}
	public String getDAYTIMEINT() {
		return DAYTIMEINT;
	}
	public void setDAYTIMEINT(String dAYTIMEINT) {
		DAYTIMEINT = dAYTIMEINT;
	}
	public String getNETPRICE() {
		return NETPRICE;
	}
	public void setNETPRICE(String nETPRICE) {
		NETPRICE = nETPRICE;
	}
	public String getVALYIELD() {
		return VALYIELD;
	}
	public void setVALYIELD(String vALYIELD) {
		VALYIELD = vALYIELD;
	}
	public String getVALDURATION() {
		return VALDURATION;
	}
	public void setVALDURATION(String vALDURATION) {
		VALDURATION = vALDURATION;
	}
	public String getCONVEXITY() {
		return CONVEXITY;
	}
	public void setCONVEXITY(String cONVEXITY) {
		CONVEXITY = cONVEXITY;
	}
	public String getVALBASIC() {
		return VALBASIC;
	}
	public void setVALBASIC(String vALBASIC) {
		VALBASIC = vALBASIC;
	}
	public String getSPREADDUR() {
		return SPREADDUR;
	}
	public void setSPREADDUR(String sPREADDUR) {
		SPREADDUR = sPREADDUR;
	}
	public String getSPREADCON() {
		return SPREADCON;
	}
	public void setSPREADCON(String sPREADCON) {
		SPREADCON = sPREADCON;
	}
	public String getWAVGPRICE() {
		return WAVGPRICE;
	}
	public void setWAVGPRICE(String wAVGPRICE) {
		WAVGPRICE = wAVGPRICE;
	}
	public String getWAVGNETPRICE() {
		return WAVGNETPRICE;
	}
	public void setWAVGNETPRICE(String wAVGNETPRICE) {
		WAVGNETPRICE = wAVGNETPRICE;
	}
	public String getWAVGYIELD() {
		return WAVGYIELD;
	}
	public void setWAVGYIELD(String wAVGYIELD) {
		WAVGYIELD = wAVGYIELD;
	}
	public String getWAVGDURATION() {
		return WAVGDURATION;
	}
	public void setWAVGDURATION(String wAVGDURATION) {
		WAVGDURATION = wAVGDURATION;
	}
	public String getWAVGCON() {
		return WAVGCON;
	}
	public void setWAVGCON(String wAVGCON) {
		WAVGCON = wAVGCON;
	}
	public String getWAVGBP() {
		return WAVGBP;
	}
	public void setWAVGBP(String wAVGBP) {
		WAVGBP = wAVGBP;
	}
	public String getWAVGSPDUR() {
		return WAVGSPDUR;
	}
	public void setWAVGSPDUR(String wAVGSPDUR) {
		WAVGSPDUR = wAVGSPDUR;
	}
	public String getWAVGSPCON() {
		return WAVGSPCON;
	}
	public void setWAVGSPCON(String wAVGSPCON) {
		WAVGSPCON = wAVGSPCON;
	}
	public String getCREDIBILITY() {
		return CREDIBILITY;
	}
	public void setCREDIBILITY(String cREDIBILITY) {
		CREDIBILITY = cREDIBILITY;
	}
	public String getVALRATEDUR() {
		return VALRATEDUR;
	}
	public void setVALRATEDUR(String vALRATEDUR) {
		VALRATEDUR = vALRATEDUR;
	}
	public String getVALRATECON() {
		return VALRATECON;
	}
	public void setVALRATECON(String vALRATECON) {
		VALRATECON = vALRATECON;
	}
	public String getWAVGRATEDUR() {
		return WAVGRATEDUR;
	}
	public void setWAVGRATEDUR(String wAVGRATEDUR) {
		WAVGRATEDUR = wAVGRATEDUR;
	}
	public String getWAVGRATECON() {
		return WAVGRATECON;
	}
	public void setWAVGRATECON(String wAVGRATECON) {
		WAVGRATECON = wAVGRATECON;
	}
	public String getCLOSEPRICE() {
		return CLOSEPRICE;
	}
	public void setCLOSEPRICE(String cLOSEPRICE) {
		CLOSEPRICE = cLOSEPRICE;
	}
	public String getCLOSEINT() {
		return CLOSEINT;
	}
	public void setCLOSEINT(String cLOSEINT) {
		CLOSEINT = cLOSEINT;
	}
	public String getREMAINAMT() {
		return REMAINAMT;
	}
	public void setREMAINAMT(String rEMAINAMT) {
		REMAINAMT = rEMAINAMT;
	}
	public String getSPREADYIELD() {
		return SPREADYIELD;
	}
	public void setSPREADYIELD(String sPREADYIELD) {
		SPREADYIELD = sPREADYIELD;
	}
	public String getCURVEID() {
		return CURVEID;
	}
	public void setCURVEID(String cURVEID) {
		CURVEID = cURVEID;
	}
	public String getCOMMSECID() {
		return COMMSECID;
	}
	public void setCOMMSECID(String cOMMSECID) {
		COMMSECID = cOMMSECID;
	}
	public String getEXERCISERATE() {
		return EXERCISERATE;
	}
	public void setEXERCISERATE(String eXERCISERATE) {
		EXERCISERATE = eXERCISERATE;
	}
	public String getYSTCLOSEPRICE() {
		return YSTCLOSEPRICE;
	}
	public void setYSTCLOSEPRICE(String ySTCLOSEPRICE) {
		YSTCLOSEPRICE = ySTCLOSEPRICE;
	}
	public String getTDYYSTDIFF() {
		return TDYYSTDIFF;
	}
	public void setTDYYSTDIFF(String tDYYSTDIFF) {
		TDYYSTDIFF = tDYYSTDIFF;
	}
	  
}
