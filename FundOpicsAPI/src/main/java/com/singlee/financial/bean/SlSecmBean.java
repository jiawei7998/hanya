package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 债券基本信息业务处理类
 * 
 * @author shenzl
 * 
 */
public class SlSecmBean implements Serializable {

	/**
	 * 债券基本信息导入始化
	 * 
	 * @param br
	 *            业务部门分支
	 * @param tag
	 *            标识
	 * @param detail
	 *            细节
	 * @note
	 * 
	 *       1、交易导入 TAG="SEMM" , DETAIL="ISEC"
	 * 
	 * 
	 */
	public SlSecmBean(String br, String server, String tag, String detail) {
		if (null == inthBean) {
			inthBean = new SlInthBean(); // 拆借
			inthBean.setBr(br);//
			inthBean.setServer(server);
			inthBean.setTag(tag);
			inthBean.setDetail(detail);
			inthBean.setSeq(SlDealModule.SECUR.SEQ);
			inthBean.setInoutind(SlDealModule.SECUR.INOUTIND);
			inthBean.setPriority(SlDealModule.SECUR.PRIORITY);
			inthBean.setStatcode(SlDealModule.SECUR.STATCODE);
			inthBean.setSyst(SlDealModule.SECUR.SYST);
			inthBean.setIoper(SlDealModule.SECUR.IOPER);
		}
	}

	public SlSecmBean() {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -599459007617997486L;
	/**
	 * 债券ID
	 */
	private String secid;
	/**
	 * 币种
	 */
	private String ccy;
	/**
	 * 会计类型
	 */
	private String acctngtype;
	/**
	 * 债券单位
	 */
	private String secunit;
	/**
	 * 一手多少量
	 */
	private String denom;
	/**
	 * 发行日期
	 */
	private Date issdate;
	/**
	 * 发行人
	 */
	private String issuer;
	/**
	 * 发行价格
	 */
	private BigDecimal issueprice;
	/**
	 * 描述
	 */
	private String descr;
	/**
	 * 起息日
	 */
	private Date vdate;
	/**
	 * 到期日
	 */
	private Date mdate;
	/**
	 * 固定收益
	 */
	private String couprate;
	/**
	 * 计息基础
	 */
	private String basis;
	/**
	 * 付息循环
	 */
	private String intpaycycle;
	/**
	 * 利息计算规则
	 */
	private String intcalcrule;
	/**
	 * 点差
	 */
	private String spreadRate;
	/**
	 * 付息规则
	 */
	private String intpayrule;
	/**
	 * 末期付息规则
	 */
	private String intenddaterule;
	/**
	 * 首次付息日
	 */
	private Date firstipaydate;
	/**
	 * 产品
	 */
	private String product;
	/**
	 * 产品类型
	 */
	private String prodtype;

	/**
	 * 清算币种
	 */
	private String settccy;
	/**
	 * 期限
	 */
	private String tenor;
	/**
	 * 债券类型
	 */
	private String sectype;
	/**
	 * Price tolerance percentage 价格容差百分比
	 */
	private String pricetol;
	/**
	 * Price Number of Decimals 价格小数
	 */
	private String pricedecs;
	/**
	 * 结算天数
	 */
	private String settdays;
	/**
	 * ABS结算天数
	 */
	private String delaydays;
	/**
	 * 票面金额
	 */
	private String paramt;
	/**
	 * 赎回金额
	 */
	private String redempamt;
	/**
	 * 利率代码
	 */
	private String ratecode;
	/**
	 * Rate Number of Decimals 利率小数
	 */
	private String ratedecs;
	/**
	 * Rate Tolerance Percentage 利率容差百分比
	 */
	private String ratetol;
	/**
	 * 利率四舍五入规则
	 */
	private String raterndrule;
	/**
	 * Repo Collateral Percent 回购抵押品百分比
	 */
	private String repocollpct;
	/**
	 * 保管账户
	 */
	private String secsacct;
	/**
	 * 税收指标
	 */
	private String taxind;
	/**
	 * 价格指示器
	 */
	private String dcpriceind;
	/**
	 * Cash Flow No. Of Decimals 小数现金流号
	 */
	private String cashdecs;
	/**
	 * AI number of decimals
	 */
	private String aidecs;
	/**
	 * Security Standard Industry
	 */
	private String secmsic;
	/**
	 * 息票利息终止日
	 */
	private String coupenddate;
	/**
	 * 金额四舍五入规则
	 */
	private String pricerndrule;

	/**
	 * 最新更新时间
	 */
	private Date lstmntdate;

	private SlInthBean inthBean;

	/**
	 * ABS MBS券
	 */
	private String prepmtmodel;

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getAcctngtype() {
		return acctngtype;
	}

	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}

	public String getSecunit() {
		return secunit;
	}

	public void setSecunit(String secunit) {
		this.secunit = secunit;
	}

	public String getDenom() {
		return denom;
	}

	public void setDenom(String denom) {
		this.denom = denom;
	}

	public Date getIssdate() {
		return issdate;
	}

	public void setIssdate(Date issdate) {
		this.issdate = issdate;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public Date getMdate() {
		return mdate;
	}

	public void setMdate(Date mdate) {
		this.mdate = mdate;
	}

	public String getCouprate() {
		return couprate;
	}

	public void setCouprate(String couprate) {
		this.couprate = couprate;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getIntpaycycle() {
		return intpaycycle;
	}

	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}

	public String getIntcalcrule() {
		return intcalcrule;
	}

	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}

	public String getSpreadRate() {
		return spreadRate;
	}

	public void setSpreadRate(String spreadRate) {
		this.spreadRate = spreadRate;
	}

	public String getIntpayrule() {
		return intpayrule;
	}

	public void setIntpayrule(String intpayrule) {
		this.intpayrule = intpayrule;
	}

	public String getIntenddaterule() {
		return intenddaterule;
	}

	public void setIntenddaterule(String intenddaterule) {
		this.intenddaterule = intenddaterule;
	}

	public Date getFirstipaydate() {
		return firstipaydate;
	}

	public void setFirstipaydate(Date firstipaydate) {
		this.firstipaydate = firstipaydate;
	}

	public SlInthBean getInthBean() {
		return inthBean;
	}

	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getSettccy() {
		return settccy;
	}

	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getSectype() {
		return sectype;
	}

	public void setSectype(String sectype) {
		this.sectype = sectype;
	}

	public String getPricetol() {
		return pricetol;
	}

	public void setPricetol(String pricetol) {
		this.pricetol = pricetol;
	}

	public String getPricedecs() {
		return pricedecs;
	}

	public void setPricedecs(String pricedecs) {
		this.pricedecs = pricedecs;
	}

	public String getSettdays() {
		return settdays;
	}

	public void setSettdays(String settdays) {
		this.settdays = settdays;
	}

	public String getParamt() {
		return paramt;
	}

	public void setParamt(String paramt) {
		this.paramt = paramt;
	}

	public String getRedempamt() {
		return redempamt;
	}

	public void setRedempamt(String redempamt) {
		this.redempamt = redempamt;
	}

	public String getRatecode() {
		return ratecode;
	}

	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}

	public String getRatedecs() {
		return ratedecs;
	}

	public void setRatedecs(String ratedecs) {
		this.ratedecs = ratedecs;
	}

	public String getRatetol() {
		return ratetol;
	}

	public void setRatetol(String ratetol) {
		this.ratetol = ratetol;
	}

	public String getRaterndrule() {
		return raterndrule;
	}

	public void setRaterndrule(String raterndrule) {
		this.raterndrule = raterndrule;
	}

	public String getRepocollpct() {
		return repocollpct;
	}

	public void setRepocollpct(String repocollpct) {
		this.repocollpct = repocollpct;
	}

	public String getSecsacct() {
		return secsacct;
	}

	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}

	public String getTaxind() {
		return taxind;
	}

	public void setTaxind(String taxind) {
		this.taxind = taxind;
	}

	public String getDcpriceind() {
		return dcpriceind;
	}

	public void setDcpriceind(String dcpriceind) {
		this.dcpriceind = dcpriceind;
	}

	public String getCashdecs() {
		return cashdecs;
	}

	public void setCashdecs(String cashdecs) {
		this.cashdecs = cashdecs;
	}

	public String getAidecs() {
		return aidecs;
	}

	public void setAidecs(String aidecs) {
		this.aidecs = aidecs;
	}

	public String getSecmsic() {
		return secmsic;
	}

	public void setSecmsic(String secmsic) {
		this.secmsic = secmsic;
	}

	public String getCoupenddate() {
		return coupenddate;
	}

	public void setCoupenddate(String coupenddate) {
		this.coupenddate = coupenddate;
	}

	public String getPricerndrule() {
		return pricerndrule;
	}

	public void setPricerndrule(String pricerndrule) {
		this.pricerndrule = pricerndrule;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public String getDelaydays() {
		return delaydays;
	}

	public void setDelaydays(String delaydays) {
		this.delaydays = delaydays;
	}

	public BigDecimal getIssueprice() {
		return issueprice;
	}

	public void setIssueprice(BigDecimal issueprice) {
		this.issueprice = issueprice;
	}

	public String getPrepmtmodel() {
		return prepmtmodel;
	}

	public void setPrepmtmodel(String prepmtmodel) {
		this.prepmtmodel = prepmtmodel;
	}
}
