package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * 通用实体类
 * 
 * @author Qxj
 */
public class SlCommonBean implements Serializable {

	private static final long serialVersionUID = -2386577567681989997L;

	/**
	 * 久期限
	 */
	private String duration;

	/**
	 * 止损
	 */
	private String convexity;

	/**
	 * DV01
	 */
	private String delta;

	/**
	 * 券的投资类型代码
	 */
	private String code;

	/**
	 * 成本中心
	 */
	private String costCent;

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getConvexity() {
		return convexity;
	}

	public void setConvexity(String convexity) {
		this.convexity = convexity;
	}

	public String getDelta() {
		return delta;
	}

	public void setDelta(String delta) {
		this.delta = delta;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCostCent() {
		return costCent;
	}

	public void setCostCent(String costCent) {
		this.costCent = costCent;
	}
}