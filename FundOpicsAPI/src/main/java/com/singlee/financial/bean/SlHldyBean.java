package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;
/***
 * 节假日表
 * @author lij
 *
 */
public class SlHldyBean implements Serializable{
	/***
	 * 日历id
	 */
	private String calendarid;
	/***
	 * 是否节假日
	 */
    private String holidaytype;
    /***
	 * 节假日期
	 */
    private Date holidate;

    private static final long serialVersionUID = 1L;

    public String getCalendarid() {
        return calendarid;
    }

    public void setCalendarid(String calendarid) {
        this.calendarid = calendarid == null ? null : calendarid.trim();
    }

    public String getHolidaytype() {
        return holidaytype;
    }

    public void setHolidaytype(String holidaytype) {
        this.holidaytype = holidaytype == null ? null : holidaytype.trim();
    }

    public Date getHolidate() {
        return holidate;
    }

    public void setHolidate(Date holidate) {
        this.holidate = holidate;
    }

}
