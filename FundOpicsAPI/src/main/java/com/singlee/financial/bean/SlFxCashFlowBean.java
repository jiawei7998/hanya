package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 外汇现金流查询
 * 
 * 
 */
public class SlFxCashFlowBean implements Serializable {

	private static final long serialVersionUID = 4288157012403029647L;

	// 币种1
	private String ccy;
	// 币种2
	private String ctrccy;
	// 交易日期
	private String dealdate;
	// T+0 币种1买入金额
	private BigDecimal pccyamt0;
	// T+0 币种2买入金额
	private BigDecimal pctramt0;
	// T+1 币种1买入金额
	private BigDecimal pccyamt1;
	// T+1 币种2买入金额
	private BigDecimal pctramt1;
	// T+2 币种1买入金额
	private BigDecimal pccyamt2;
	// T+2 币种2买入金额
	private BigDecimal pctramt2;
	// T+3 币种1买入金额
	private BigDecimal pccyamt3;
	// T+3 币种2买入金额
	private BigDecimal pctramt3;

	// T+0 币种1卖出金额
	private BigDecimal sccyamt0;
	// T+0 币种2卖出金额
	private BigDecimal sctramt0;
	// T+1 币种1卖出金额
	private BigDecimal sccyamt1;
	// T+1 币种2卖出金额
	private BigDecimal sctramt1;
	// T+2 币种1卖出金额
	private BigDecimal sccyamt2;
	// T+2 币种2卖出金额
	private BigDecimal sctramt2;
	// T+3 币种1卖出金额
	private BigDecimal sccyamt3;
	// T+3 币种2卖出金额
	private BigDecimal sctramt3;

	private String br;

	private String prod;

	private String port;

	private String vdate;

	private String ccy1;

	private BigDecimal ccy1amt;

	private String ccy2;

	private BigDecimal ccy2amt;

	private String trad;

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCtrccy() {
		return ctrccy;
	}

	public void setCtrccy(String ctrccy) {
		this.ctrccy = ctrccy;
	}

	public String getDealdate() {
		return dealdate;
	}

	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}

	public BigDecimal getPccyamt0() {
		return pccyamt0;
	}

	public void setPccyamt0(BigDecimal pccyamt0) {
		this.pccyamt0 = pccyamt0;
	}

	public BigDecimal getPctramt0() {
		return pctramt0;
	}

	public void setPctramt0(BigDecimal pctramt0) {
		this.pctramt0 = pctramt0;
	}

	public BigDecimal getPccyamt1() {
		return pccyamt1;
	}

	public void setPccyamt1(BigDecimal pccyamt1) {
		this.pccyamt1 = pccyamt1;
	}

	public BigDecimal getPctramt1() {
		return pctramt1;
	}

	public void setPctramt1(BigDecimal pctramt1) {
		this.pctramt1 = pctramt1;
	}

	public BigDecimal getPccyamt2() {
		return pccyamt2;
	}

	public void setPccyamt2(BigDecimal pccyamt2) {
		this.pccyamt2 = pccyamt2;
	}

	public BigDecimal getPctramt2() {
		return pctramt2;
	}

	public void setPctramt2(BigDecimal pctramt2) {
		this.pctramt2 = pctramt2;
	}

	public BigDecimal getPccyamt3() {
		return pccyamt3;
	}

	public void setPccyamt3(BigDecimal pccyamt3) {
		this.pccyamt3 = pccyamt3;
	}

	public BigDecimal getPctramt3() {
		return pctramt3;
	}

	public void setPctramt3(BigDecimal pctramt3) {
		this.pctramt3 = pctramt3;
	}

	public BigDecimal getSccyamt0() {
		return sccyamt0;
	}

	public void setSccyamt0(BigDecimal sccyamt0) {
		this.sccyamt0 = sccyamt0;
	}

	public BigDecimal getSctramt0() {
		return sctramt0;
	}

	public void setSctramt0(BigDecimal sctramt0) {
		this.sctramt0 = sctramt0;
	}

	public BigDecimal getSccyamt1() {
		return sccyamt1;
	}

	public void setSccyamt1(BigDecimal sccyamt1) {
		this.sccyamt1 = sccyamt1;
	}

	public BigDecimal getSctramt1() {
		return sctramt1;
	}

	public void setSctramt1(BigDecimal sctramt1) {
		this.sctramt1 = sctramt1;
	}

	public BigDecimal getSccyamt2() {
		return sccyamt2;
	}

	public void setSccyamt2(BigDecimal sccyamt2) {
		this.sccyamt2 = sccyamt2;
	}

	public BigDecimal getSctramt2() {
		return sctramt2;
	}

	public void setSctramt2(BigDecimal sctramt2) {
		this.sctramt2 = sctramt2;
	}

	public BigDecimal getSccyamt3() {
		return sccyamt3;
	}

	public void setSccyamt3(BigDecimal sccyamt3) {
		this.sccyamt3 = sccyamt3;
	}

	public BigDecimal getSctramt3() {
		return sctramt3;
	}

	public void setSctramt3(BigDecimal sctramt3) {
		this.sctramt3 = sctramt3;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getProd() {
		return prod;
	}

	public void setProd(String prod) {
		this.prod = prod;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getVdate() {
		return vdate;
	}

	public void setVdate(String vdate) {
		this.vdate = vdate;
	}

	public String getCcy1() {
		return ccy1;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public BigDecimal getCcy1amt() {
		return ccy1amt;
	}

	public void setCcy1amt(BigDecimal ccy1amt) {
		this.ccy1amt = ccy1amt;
	}

	public String getCcy2() {
		return ccy2;
	}

	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}

	public BigDecimal getCcy2amt() {
		return ccy2amt;
	}

	public void setCcy2amt(BigDecimal ccy2amt) {
		this.ccy2amt = ccy2amt;
	}

}
