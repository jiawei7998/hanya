package com.singlee.financial.bean;

import java.io.Serializable;

/**
 * A1403-外资银行资产负债项目月报表
 * @author wangzhao
 *
 */
public class ZjsyjcBean implements Serializable{
	  private static final long serialVersionUID = 1L;
	  private String startdate;
	  private String enddate;
	  private String tableid;
	  private String cno;
	  private String ccy;
	  private Double amount;
	  private Double accroutst;
	  private Double realgainslosses;
	  private Double unrealgainslosses;
	  private Double feeamt;
	  private Double allincome;
	  private String note;
	  private String rpdate;
	  private String seq;
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getTableid() {
		return tableid;
	}
	public void setTableid(String tableid) {
		this.tableid = tableid;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public Double getAccroutst() {
		return accroutst;
	}
	public void setAccroutst(Double accroutst) {
		this.accroutst = accroutst;
	}
	public Double getRealgainslosses() {
		return realgainslosses;
	}
	public void setRealgainslosses(Double realgainslosses) {
		this.realgainslosses = realgainslosses;
	}
	public Double getUnrealgainslosses() {
		return unrealgainslosses;
	}
	public void setUnrealgainslosses(Double unrealgainslosses) {
		this.unrealgainslosses = unrealgainslosses;
	}
	public Double getFeeamt() {
		return feeamt;
	}
	public void setFeeamt(Double feeamt) {
		this.feeamt = feeamt;
	}
	public Double getAllincome() {
		return allincome;
	}
	public void setAllincome(Double allincome) {
		this.allincome = allincome;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getRpdate() {
		return rpdate;
	}
	public void setRpdate(String rpdate) {
		this.rpdate = rpdate;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "ZjsyjcBean [startdate=" + startdate + ", enddate=" + enddate
				+ ", tableid=" + tableid + ", cno=" + cno + ", ccy=" + ccy
				+ ", amount=" + amount + ", accroutst=" + accroutst
				+ ", realgainslosses=" + realgainslosses
				+ ", unrealgainslosses=" + unrealgainslosses + ", feeamt="
				+ feeamt + ", allincome=" + allincome + ", note=" + note
				+ ", rpdate=" + rpdate + ", seq=" + seq + "]";
	}

}
