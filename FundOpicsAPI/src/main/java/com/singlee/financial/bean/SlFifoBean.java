package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "FIFO")
public class SlFifoBean implements Serializable {

	private static final long serialVersionUID = 1901551160942215641L;

	private String br;
	private String pdealno;
	private String seq;
	private String cost;
	private String ccy;
	private String invtype;
	private String port;
	private String secid;
	private Date purchasedate;
	private BigDecimal pamount;
	private String sdealno;
	private Date saledate;
	private BigDecimal samount;
	
	
	
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getPdealno() {
		return pdealno;
	}
	public void setPdealno(String pdealno) {
		this.pdealno = pdealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public String getInvtype() {
		return invtype;
	}
	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public Date getPurchasedate() {
		return purchasedate;
	}
	public void setPurchasedate(Date purchasedate) {
		this.purchasedate = purchasedate;
	}
	public BigDecimal getPamount() {
		return pamount;
	}
	public void setPamount(BigDecimal pamount) {
		this.pamount = pamount;
	}
	public String getSdealno() {
		return sdealno;
	}
	public void setSdealno(String sdealno) {
		this.sdealno = sdealno;
	}
	public Date getSaledate() {
		return saledate;
	}
	public void setSaledate(Date saledate) {
		this.saledate = saledate;
	}
	public BigDecimal getSamount() {
		return samount;
	}
	public void setSamount(BigDecimal samount) {
		this.samount = samount;
	}

	
}
