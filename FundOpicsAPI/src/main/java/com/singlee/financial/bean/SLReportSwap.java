package com.singlee.financial.bean;

import java.io.Serializable;

public class SLReportSwap implements Serializable  {
	private static final long serialVersionUID = -5902927615667660372L;
	private String dealText;
	private String br;
	private String dealno;
	private String cno;
	private String cmne;
	private String cost;
	private String dealDate;
	private String startDate;
	private String matDate;
	private String port;
	private String trad;
	//固定数据
	private String fixBasis;
	private String fixPayRecind;
	private String fixIntPayCycle;
	private String fixNotCcyAmt;
	private String fixIntRate_8;
	private String fixRateCode;
	private String fixNpvAmt;
	//浮动数据
	private String floatBasis;
	private String floatnotCcyAmt;
	private String floatRateCode;
	private String floatIntRate_8;
	private String floatNpvAmt;
	private String mtm;
	
	public String getDealText() {
		return dealText;
	}
	public void setDealText(String dealText) {
		this.dealText = dealText;
	}
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getCno() {
		return cno;
	}
	public void setCno(String cno) {
		this.cno = cno;
	}
	public String getCmne() {
		return cmne;
	}
	public void setCmne(String cmne) {
		this.cmne = cmne;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getDealDate() {
		return dealDate;
	}
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getMatDate() {
		return matDate;
	}
	public void setMatDate(String matDate) {
		this.matDate = matDate;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getFixBasis() {
		return fixBasis;
	}
	public void setFixBasis(String fixBasis) {
		this.fixBasis = fixBasis;
	}
	public String getFixPayRecind() {
		return fixPayRecind;
	}
	public void setFixPayRecind(String fixPayRecind) {
		this.fixPayRecind = fixPayRecind;
	}
	public String getFixIntPayCycle() {
		return fixIntPayCycle;
	}
	public void setFixIntPayCycle(String fixIntPayCycle) {
		this.fixIntPayCycle = fixIntPayCycle;
	}
	public String getFixNotCcyAmt() {
		return fixNotCcyAmt;
	}
	public void setFixNotCcyAmt(String fixNotCcyAmt) {
		this.fixNotCcyAmt = fixNotCcyAmt;
	}
	public String getFixIntRate_8() {
		return fixIntRate_8;
	}
	public void setFixIntRate_8(String fixIntRate_8) {
		this.fixIntRate_8 = fixIntRate_8;
	}
	public String getFixRateCode() {
		return fixRateCode;
	}
	public void setFixRateCode(String fixRateCode) {
		this.fixRateCode = fixRateCode;
	}
	public String getFixNpvAmt() {
		return fixNpvAmt;
	}
	public void setFixNpvAmt(String fixNpvAmt) {
		this.fixNpvAmt = fixNpvAmt;
	}
	public String getFloatBasis() {
		return floatBasis;
	}
	public void setFloatBasis(String floatBasis) {
		this.floatBasis = floatBasis;
	}
	public String getFloatnotCcyAmt() {
		return floatnotCcyAmt;
	}
	public void setFloatnotCcyAmt(String floatnotCcyAmt) {
		this.floatnotCcyAmt = floatnotCcyAmt;
	}
	public String getFloatRateCode() {
		return floatRateCode;
	}
	public void setFloatRateCode(String floatRateCode) {
		this.floatRateCode = floatRateCode;
	}
	public String getFloatIntRate_8() {
		return floatIntRate_8;
	}
	public void setFloatIntRate_8(String floatIntRate_8) {
		this.floatIntRate_8 = floatIntRate_8;
	}
	public String getFloatNpvAmt() {
		return floatNpvAmt;
	}
	public void setFloatNpvAmt(String floatNpvAmt) {
		this.floatNpvAmt = floatNpvAmt;
	}
	public String getMtm() {
		return mtm;
	}
	public void setMtm(String mtm) {
		this.mtm = mtm;
	}

	
}
