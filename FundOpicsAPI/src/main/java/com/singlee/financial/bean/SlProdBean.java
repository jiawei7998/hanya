package com.singlee.financial.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * opics 产品代码
 * 
 * @author shenzl
 * 
 */
public class SlProdBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3178363812140721414L;
	/**
	 * 产品代码
	 */
	private String pcode;
	/**
	 * 产品描述
	 */
	private String pdesc;
	/**
	 * 所属模块
	 */
	private String sys;
	/**
	 * 最后维护时间
	 */
	private Date lstmntdte;

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	public String getPdesc() {
		return pdesc;
	}

	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}

	public String getSys() {
		return sys;
	}

	public void setSys(String sys) {
		this.sys = sys;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

}
