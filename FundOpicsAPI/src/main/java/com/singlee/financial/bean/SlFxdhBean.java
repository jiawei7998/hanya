package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class SlFxdhBean implements Serializable{

	/**
	 * 外汇FXDH表
	 */
	private static final long serialVersionUID = 6167793814390430188L;
	
	private String	br;    
	private String	dealno;    
	private String	seq;
	private String	trad;    
	private String	vdate;   
	private String	cust;  
	private String	brok;   
	private String	brokccy;   
	private BigDecimal	brokamt;   
	private String	phonci;    
	private String	port;    
	private String	cost;    
	private String	odate;   
	private String	dealdate;    
	private String	dealtime;    
	private String	ioper;   
	private String	voper;   
	private String	brokcdate;   
	private String	custcdate;   
	private String	phonecdate;    
	private String	dealtext;    
	private String	phonetext;   
	private String	ps;
	private String	prodcode; 
	private String	verind ;
	private String	revtext;
	private String	swapdeal;
	private String	farnearind;
	private String	payauthind; 
	private String	swapvdate;
	private String	revreason;
	private String	inputdate;
	private String	inputtime;
	private String	dealsrce;
	private BigDecimal	origocamt;
	private String	trcedate;
	private String	tracecnt; 
	private String	payoper;
	private String	lstmntdate;
	private String	ccy;
	private BigDecimal	ccyamt;
	private String	ccyterms;
	private BigDecimal	ccyrate_8; 
	private BigDecimal	ccypd_8;
	private BigDecimal	ccybamt;
	private BigDecimal	ccybrate_8;   
	private BigDecimal	ccybpd_8;  
	private String	baseterms;   
	private String	netsi   ;
	private String	spotfwdind ;   
	private String	netdeal;
	private String	ccysmeans;
	private String	ccysacct;
	private String	ctrsmeans;
	private String	ctrsacct; 
	private String	ctrccy;
	private BigDecimal	ctramt;
	private BigDecimal	ctrrate_8; 
	private BigDecimal	ctrpd_8;
	private BigDecimal	ctrbamt;
	private BigDecimal	ctrbrate_8; 
	private String	nsrnoccy;
	private String	nsrccyseq;
	private String	nsrnoctr;
	private String	nsrctrseq;
	private String	brprcindte;   
	private String	recoper;
	private String	payauthdte;  
	private String	recauthdte;    
	private String	verdate;
	private String	revdate;
	private BigDecimal	ccyrevalamt;  
	private BigDecimal	ctrrevalamt; 
	private BigDecimal	ccynpvamt;
	private BigDecimal	ctrnpvamt; 
	private String	recauthoper;  
	private String	payauthoper;   
	private String	siind ;  
	private String	prodtype;    
	private String	spottrad;    
	private String	spotport;    
	private String	spotcost;    
	private BigDecimal	internalrate_8;    
	private BigDecimal	corpspread_8;    
	private String	corpcost;    
	private BigDecimal	brspread_8;    
	private String	brcost;    
	private String	tenor;   
	private String	ccysettdate;   
	private String	ctrsettdate;   
	private String	custenteredind;    
	private String	linkdealno;    
	private String	linkproduct;   
	private String	linkprodtype;    
	private String	fincntr1;    
	private String	fincntr2;    
	private BigDecimal	commamt;   
	private String	excoveralllim;   
	private String	exccustlim;    
	private String	corpccy;   
	private String	corpport;    
	private String	corptrad;    
	private String	revdealno;   
	private String	optionind;   
	private BigDecimal	marginamt;   
	private String	marginccy;   
	private BigDecimal	corpspreadamt;   
	private String	spotccy;   
	private String	spotind;   
	private String	tenorexceedind;    
	private String	ccysettstatus;   
	private String	ctrsettstatus;   
	private String	custrefno;   
	private String	ccyfaildate;   
	private String	ccycleardate;    
	private String	ccyfailoper;   
	private String	ccyclearoper;    
	private String	ctrfaildate;   
	private String	ctrcleardate;    
	private String	ctrfailoper;   
	private String	ctrclearoper;    
	private BigDecimal	marginfeeamt;    
	private String	marginfeeccy;    
	private String	marginfeesettmeans;    
	private String	marginfeesettacct;   
	private BigDecimal	marginfeerebamt;   
	private String	marginfeerebccy;   
	private String	marginfeerebsettmeans;   
	private String	marginfeerebsettacct;    
	private String	phoneconfoper;   
	private String	phoneconftime;   
	private String	swiftmatchind;   
	private String	siindpay;    
	private String	commccy;   
	private String	rexratetolind;   
	private String	dealnoexcess;    
	private String	fixrateind;    
	private String	fixratecode;   
	private String	revoper;   
	private String	ctrpartydealno;    
	private String	ctrpartyphoneno;   
	private String	ctrpartyphoneoper;   
	private String	blockind;    
	private String	confcreatedind;    
	private String	spotdate;    
	private String	disaggccy;   
	private String	disaggspottrad;    
	private String	disaggspotport;    
	private String	disaggspotcost;    
	private String	disaggfwdtrad;   
	private String	disaggfwdport;   
	private String	disaggfwdcost;   
	private BigDecimal	fwdpremamt;    
	private String	amenddate;
	public String getBr() {
		return br;
	}
	public void setBr(String br) {
		this.br = br;
	}
	public String getDealno() {
		return dealno;
	}
	public void setDealno(String dealno) {
		this.dealno = dealno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getTrad() {
		return trad;
	}
	public void setTrad(String trad) {
		this.trad = trad;
	}
	public String getVdate() {
		return vdate;
	}
	public void setVdate(String vdate) {
		this.vdate = vdate;
	}
	public String getCust() {
		return cust;
	}
	public void setCust(String cust) {
		this.cust = cust;
	}
	public String getBrok() {
		return brok;
	}
	public void setBrok(String brok) {
		this.brok = brok;
	}
	public String getBrokccy() {
		return brokccy;
	}
	public void setBrokccy(String brokccy) {
		this.brokccy = brokccy;
	}
	public BigDecimal getBrokamt() {
		return brokamt;
	}
	public void setBrokamt(BigDecimal brokamt) {
		this.brokamt = brokamt;
	}
	public String getPhonci() {
		return phonci;
	}
	public void setPhonci(String phonci) {
		this.phonci = phonci;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getOdate() {
		return odate;
	}
	public void setOdate(String odate) {
		this.odate = odate;
	}
	public String getDealdate() {
		return dealdate;
	}
	public void setDealdate(String dealdate) {
		this.dealdate = dealdate;
	}
	public String getDealtime() {
		return dealtime;
	}
	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}
	public String getIoper() {
		return ioper;
	}
	public void setIoper(String ioper) {
		this.ioper = ioper;
	}
	public String getVoper() {
		return voper;
	}
	public void setVoper(String voper) {
		this.voper = voper;
	}
	public String getBrokcdate() {
		return brokcdate;
	}
	public void setBrokcdate(String brokcdate) {
		this.brokcdate = brokcdate;
	}
	public String getCustcdate() {
		return custcdate;
	}
	public void setCustcdate(String custcdate) {
		this.custcdate = custcdate;
	}
	public String getPhonecdate() {
		return phonecdate;
	}
	public void setPhonecdate(String phonecdate) {
		this.phonecdate = phonecdate;
	}
	public String getDealtext() {
		return dealtext;
	}
	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}
	public String getPhonetext() {
		return phonetext;
	}
	public void setPhonetext(String phonetext) {
		this.phonetext = phonetext;
	}
	public String getPs() {
		return ps;
	}
	public void setPs(String ps) {
		this.ps = ps;
	}
	public String getProdcode() {
		return prodcode;
	}
	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}
	public String getVerind() {
		return verind;
	}
	public void setVerind(String verind) {
		this.verind = verind;
	}
	public String getRevtext() {
		return revtext;
	}
	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}
	public String getSwapdeal() {
		return swapdeal;
	}
	public void setSwapdeal(String swapdeal) {
		this.swapdeal = swapdeal;
	}
	public String getFarnearind() {
		return farnearind;
	}
	public void setFarnearind(String farnearind) {
		this.farnearind = farnearind;
	}
	public String getPayauthind() {
		return payauthind;
	}
	public void setPayauthind(String payauthind) {
		this.payauthind = payauthind;
	}
	public String getSwapvdate() {
		return swapvdate;
	}
	public void setSwapvdate(String swapvdate) {
		this.swapvdate = swapvdate;
	}
	public String getRevreason() {
		return revreason;
	}
	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}
	public String getInputdate() {
		return inputdate;
	}
	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}
	public String getInputtime() {
		return inputtime;
	}
	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}
	public String getDealsrce() {
		return dealsrce;
	}
	public void setDealsrce(String dealsrce) {
		this.dealsrce = dealsrce;
	}
	public BigDecimal getOrigocamt() {
		return origocamt;
	}
	public void setOrigocamt(BigDecimal origocamt) {
		this.origocamt = origocamt;
	}
	public String getTrcedate() {
		return trcedate;
	}
	public void setTrcedate(String trcedate) {
		this.trcedate = trcedate;
	}
	public String getTracecnt() {
		return tracecnt;
	}
	public void setTracecnt(String tracecnt) {
		this.tracecnt = tracecnt;
	}
	public String getPayoper() {
		return payoper;
	}
	public void setPayoper(String payoper) {
		this.payoper = payoper;
	}
	public String getLstmntdate() {
		return lstmntdate;
	}
	public void setLstmntdate(String lstmntdate) {
		this.lstmntdate = lstmntdate;
	}
	public String getCcy() {
		return ccy;
	}
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
	public BigDecimal getCcyamt() {
		return ccyamt;
	}
	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}
	public String getCcyterms() {
		return ccyterms;
	}
	public void setCcyterms(String ccyterms) {
		this.ccyterms = ccyterms;
	}
	public BigDecimal getCcyrate_8() {
		return ccyrate_8;
	}
	public void setCcyrate_8(BigDecimal ccyrate_8) {
		this.ccyrate_8 = ccyrate_8;
	}
	public BigDecimal getCcypd_8() {
		return ccypd_8;
	}
	public void setCcypd_8(BigDecimal ccypd_8) {
		this.ccypd_8 = ccypd_8;
	}
	public BigDecimal getCcybamt() {
		return ccybamt;
	}
	public void setCcybamt(BigDecimal ccybamt) {
		this.ccybamt = ccybamt;
	}
	public BigDecimal getCcybrate_8() {
		return ccybrate_8;
	}
	public void setCcybrate_8(BigDecimal ccybrate_8) {
		this.ccybrate_8 = ccybrate_8;
	}
	public BigDecimal getCcybpd_8() {
		return ccybpd_8;
	}
	public void setCcybpd_8(BigDecimal ccybpd_8) {
		this.ccybpd_8 = ccybpd_8;
	}
	public String getBaseterms() {
		return baseterms;
	}
	public void setBaseterms(String baseterms) {
		this.baseterms = baseterms;
	}
	public String getNetsi() {
		return netsi;
	}
	public void setNetsi(String netsi) {
		this.netsi = netsi;
	}
	public String getSpotfwdind() {
		return spotfwdind;
	}
	public void setSpotfwdind(String spotfwdind) {
		this.spotfwdind = spotfwdind;
	}
	public String getNetdeal() {
		return netdeal;
	}
	public void setNetdeal(String netdeal) {
		this.netdeal = netdeal;
	}
	public String getCcysmeans() {
		return ccysmeans;
	}
	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans;
	}
	public String getCcysacct() {
		return ccysacct;
	}
	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}
	public String getCtrsmeans() {
		return ctrsmeans;
	}
	public void setCtrsmeans(String ctrsmeans) {
		this.ctrsmeans = ctrsmeans;
	}
	public String getCtrsacct() {
		return ctrsacct;
	}
	public void setCtrsacct(String ctrsacct) {
		this.ctrsacct = ctrsacct;
	}
	public String getCtrccy() {
		return ctrccy;
	}
	public void setCtrccy(String ctrccy) {
		this.ctrccy = ctrccy;
	}
	public BigDecimal getCtramt() {
		return ctramt;
	}
	public void setCtramt(BigDecimal ctramt) {
		this.ctramt = ctramt;
	}
	public BigDecimal getCtrrate_8() {
		return ctrrate_8;
	}
	public void setCtrrate_8(BigDecimal ctrrate_8) {
		this.ctrrate_8 = ctrrate_8;
	}
	public BigDecimal getCtrpd_8() {
		return ctrpd_8;
	}
	public void setCtrpd_8(BigDecimal ctrpd_8) {
		this.ctrpd_8 = ctrpd_8;
	}
	public BigDecimal getCtrbamt() {
		return ctrbamt;
	}
	public void setCtrbamt(BigDecimal ctrbamt) {
		this.ctrbamt = ctrbamt;
	}
	public BigDecimal getCtrbrate_8() {
		return ctrbrate_8;
	}
	public void setCtrbrate_8(BigDecimal ctrbrate_8) {
		this.ctrbrate_8 = ctrbrate_8;
	}
	public String getNsrnoccy() {
		return nsrnoccy;
	}
	public void setNsrnoccy(String nsrnoccy) {
		this.nsrnoccy = nsrnoccy;
	}
	public String getNsrccyseq() {
		return nsrccyseq;
	}
	public void setNsrccyseq(String nsrccyseq) {
		this.nsrccyseq = nsrccyseq;
	}
	public String getNsrnoctr() {
		return nsrnoctr;
	}
	public void setNsrnoctr(String nsrnoctr) {
		this.nsrnoctr = nsrnoctr;
	}
	public String getNsrctrseq() {
		return nsrctrseq;
	}
	public void setNsrctrseq(String nsrctrseq) {
		this.nsrctrseq = nsrctrseq;
	}
	public String getBrprcindte() {
		return brprcindte;
	}
	public void setBrprcindte(String brprcindte) {
		this.brprcindte = brprcindte;
	}
	public String getRecoper() {
		return recoper;
	}
	public void setRecoper(String recoper) {
		this.recoper = recoper;
	}
	public String getPayauthdte() {
		return payauthdte;
	}
	public void setPayauthdte(String payauthdte) {
		this.payauthdte = payauthdte;
	}
	public String getRecauthdte() {
		return recauthdte;
	}
	public void setRecauthdte(String recauthdte) {
		this.recauthdte = recauthdte;
	}
	public String getVerdate() {
		return verdate;
	}
	public void setVerdate(String verdate) {
		this.verdate = verdate;
	}
	public String getRevdate() {
		return revdate;
	}
	public void setRevdate(String revdate) {
		this.revdate = revdate;
	}
	public BigDecimal getCcyrevalamt() {
		return ccyrevalamt;
	}
	public void setCcyrevalamt(BigDecimal ccyrevalamt) {
		this.ccyrevalamt = ccyrevalamt;
	}
	public BigDecimal getCtrrevalamt() {
		return ctrrevalamt;
	}
	public void setCtrrevalamt(BigDecimal ctrrevalamt) {
		this.ctrrevalamt = ctrrevalamt;
	}
	public BigDecimal getCcynpvamt() {
		return ccynpvamt;
	}
	public void setCcynpvamt(BigDecimal ccynpvamt) {
		this.ccynpvamt = ccynpvamt;
	}
	public BigDecimal getCtrnpvamt() {
		return ctrnpvamt;
	}
	public void setCtrnpvamt(BigDecimal ctrnpvamt) {
		this.ctrnpvamt = ctrnpvamt;
	}
	public String getRecauthoper() {
		return recauthoper;
	}
	public void setRecauthoper(String recauthoper) {
		this.recauthoper = recauthoper;
	}
	public String getPayauthoper() {
		return payauthoper;
	}
	public void setPayauthoper(String payauthoper) {
		this.payauthoper = payauthoper;
	}
	public String getSiind() {
		return siind;
	}
	public void setSiind(String siind) {
		this.siind = siind;
	}
	public String getProdtype() {
		return prodtype;
	}
	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}
	public String getSpottrad() {
		return spottrad;
	}
	public void setSpottrad(String spottrad) {
		this.spottrad = spottrad;
	}
	public String getSpotport() {
		return spotport;
	}
	public void setSpotport(String spotport) {
		this.spotport = spotport;
	}
	public String getSpotcost() {
		return spotcost;
	}
	public void setSpotcost(String spotcost) {
		this.spotcost = spotcost;
	}
	public BigDecimal getInternalrate_8() {
		return internalrate_8;
	}
	public void setInternalrate_8(BigDecimal internalrate_8) {
		this.internalrate_8 = internalrate_8;
	}
	public BigDecimal getCorpspread_8() {
		return corpspread_8;
	}
	public void setCorpspread_8(BigDecimal corpspread_8) {
		this.corpspread_8 = corpspread_8;
	}
	public String getCorpcost() {
		return corpcost;
	}
	public void setCorpcost(String corpcost) {
		this.corpcost = corpcost;
	}
	public BigDecimal getBrspread_8() {
		return brspread_8;
	}
	public void setBrspread_8(BigDecimal brspread_8) {
		this.brspread_8 = brspread_8;
	}
	public String getBrcost() {
		return brcost;
	}
	public void setBrcost(String brcost) {
		this.brcost = brcost;
	}
	public String getTenor() {
		return tenor;
	}
	public void setTenor(String tenor) {
		this.tenor = tenor;
	}
	public String getCcysettdate() {
		return ccysettdate;
	}
	public void setCcysettdate(String ccysettdate) {
		this.ccysettdate = ccysettdate;
	}
	public String getCtrsettdate() {
		return ctrsettdate;
	}
	public void setCtrsettdate(String ctrsettdate) {
		this.ctrsettdate = ctrsettdate;
	}
	public String getCustenteredind() {
		return custenteredind;
	}
	public void setCustenteredind(String custenteredind) {
		this.custenteredind = custenteredind;
	}
	public String getLinkdealno() {
		return linkdealno;
	}
	public void setLinkdealno(String linkdealno) {
		this.linkdealno = linkdealno;
	}
	public String getLinkproduct() {
		return linkproduct;
	}
	public void setLinkproduct(String linkproduct) {
		this.linkproduct = linkproduct;
	}
	public String getLinkprodtype() {
		return linkprodtype;
	}
	public void setLinkprodtype(String linkprodtype) {
		this.linkprodtype = linkprodtype;
	}
	public String getFincntr1() {
		return fincntr1;
	}
	public void setFincntr1(String fincntr1) {
		this.fincntr1 = fincntr1;
	}
	public String getFincntr2() {
		return fincntr2;
	}
	public void setFincntr2(String fincntr2) {
		this.fincntr2 = fincntr2;
	}
	public BigDecimal getCommamt() {
		return commamt;
	}
	public void setCommamt(BigDecimal commamt) {
		this.commamt = commamt;
	}
	public String getExcoveralllim() {
		return excoveralllim;
	}
	public void setExcoveralllim(String excoveralllim) {
		this.excoveralllim = excoveralllim;
	}
	public String getExccustlim() {
		return exccustlim;
	}
	public void setExccustlim(String exccustlim) {
		this.exccustlim = exccustlim;
	}
	public String getCorpccy() {
		return corpccy;
	}
	public void setCorpccy(String corpccy) {
		this.corpccy = corpccy;
	}
	public String getCorpport() {
		return corpport;
	}
	public void setCorpport(String corpport) {
		this.corpport = corpport;
	}
	public String getCorptrad() {
		return corptrad;
	}
	public void setCorptrad(String corptrad) {
		this.corptrad = corptrad;
	}
	public String getRevdealno() {
		return revdealno;
	}
	public void setRevdealno(String revdealno) {
		this.revdealno = revdealno;
	}
	public String getOptionind() {
		return optionind;
	}
	public void setOptionind(String optionind) {
		this.optionind = optionind;
	}
	public BigDecimal getMarginamt() {
		return marginamt;
	}
	public void setMarginamt(BigDecimal marginamt) {
		this.marginamt = marginamt;
	}
	public String getMarginccy() {
		return marginccy;
	}
	public void setMarginccy(String marginccy) {
		this.marginccy = marginccy;
	}
	public BigDecimal getCorpspreadamt() {
		return corpspreadamt;
	}
	public void setCorpspreadamt(BigDecimal corpspreadamt) {
		this.corpspreadamt = corpspreadamt;
	}
	public String getSpotccy() {
		return spotccy;
	}
	public void setSpotccy(String spotccy) {
		this.spotccy = spotccy;
	}
	public String getSpotind() {
		return spotind;
	}
	public void setSpotind(String spotind) {
		this.spotind = spotind;
	}
	public String getTenorexceedind() {
		return tenorexceedind;
	}
	public void setTenorexceedind(String tenorexceedind) {
		this.tenorexceedind = tenorexceedind;
	}
	public String getCcysettstatus() {
		return ccysettstatus;
	}
	public void setCcysettstatus(String ccysettstatus) {
		this.ccysettstatus = ccysettstatus;
	}
	public String getCtrsettstatus() {
		return ctrsettstatus;
	}
	public void setCtrsettstatus(String ctrsettstatus) {
		this.ctrsettstatus = ctrsettstatus;
	}
	public String getCustrefno() {
		return custrefno;
	}
	public void setCustrefno(String custrefno) {
		this.custrefno = custrefno;
	}
	public String getCcyfaildate() {
		return ccyfaildate;
	}
	public void setCcyfaildate(String ccyfaildate) {
		this.ccyfaildate = ccyfaildate;
	}
	public String getCcycleardate() {
		return ccycleardate;
	}
	public void setCcycleardate(String ccycleardate) {
		this.ccycleardate = ccycleardate;
	}
	public String getCcyfailoper() {
		return ccyfailoper;
	}
	public void setCcyfailoper(String ccyfailoper) {
		this.ccyfailoper = ccyfailoper;
	}
	public String getCcyclearoper() {
		return ccyclearoper;
	}
	public void setCcyclearoper(String ccyclearoper) {
		this.ccyclearoper = ccyclearoper;
	}
	public String getCtrfaildate() {
		return ctrfaildate;
	}
	public void setCtrfaildate(String ctrfaildate) {
		this.ctrfaildate = ctrfaildate;
	}
	public String getCtrcleardate() {
		return ctrcleardate;
	}
	public void setCtrcleardate(String ctrcleardate) {
		this.ctrcleardate = ctrcleardate;
	}
	public String getCtrfailoper() {
		return ctrfailoper;
	}
	public void setCtrfailoper(String ctrfailoper) {
		this.ctrfailoper = ctrfailoper;
	}
	public String getCtrclearoper() {
		return ctrclearoper;
	}
	public void setCtrclearoper(String ctrclearoper) {
		this.ctrclearoper = ctrclearoper;
	}
	public BigDecimal getMarginfeeamt() {
		return marginfeeamt;
	}
	public void setMarginfeeamt(BigDecimal marginfeeamt) {
		this.marginfeeamt = marginfeeamt;
	}
	public String getMarginfeeccy() {
		return marginfeeccy;
	}
	public void setMarginfeeccy(String marginfeeccy) {
		this.marginfeeccy = marginfeeccy;
	}
	public String getMarginfeesettmeans() {
		return marginfeesettmeans;
	}
	public void setMarginfeesettmeans(String marginfeesettmeans) {
		this.marginfeesettmeans = marginfeesettmeans;
	}
	public String getMarginfeesettacct() {
		return marginfeesettacct;
	}
	public void setMarginfeesettacct(String marginfeesettacct) {
		this.marginfeesettacct = marginfeesettacct;
	}
	public BigDecimal getMarginfeerebamt() {
		return marginfeerebamt;
	}
	public void setMarginfeerebamt(BigDecimal marginfeerebamt) {
		this.marginfeerebamt = marginfeerebamt;
	}
	public String getMarginfeerebccy() {
		return marginfeerebccy;
	}
	public void setMarginfeerebccy(String marginfeerebccy) {
		this.marginfeerebccy = marginfeerebccy;
	}
	public String getMarginfeerebsettmeans() {
		return marginfeerebsettmeans;
	}
	public void setMarginfeerebsettmeans(String marginfeerebsettmeans) {
		this.marginfeerebsettmeans = marginfeerebsettmeans;
	}
	public String getMarginfeerebsettacct() {
		return marginfeerebsettacct;
	}
	public void setMarginfeerebsettacct(String marginfeerebsettacct) {
		this.marginfeerebsettacct = marginfeerebsettacct;
	}
	public String getPhoneconfoper() {
		return phoneconfoper;
	}
	public void setPhoneconfoper(String phoneconfoper) {
		this.phoneconfoper = phoneconfoper;
	}
	public String getPhoneconftime() {
		return phoneconftime;
	}
	public void setPhoneconftime(String phoneconftime) {
		this.phoneconftime = phoneconftime;
	}
	public String getSwiftmatchind() {
		return swiftmatchind;
	}
	public void setSwiftmatchind(String swiftmatchind) {
		this.swiftmatchind = swiftmatchind;
	}
	public String getSiindpay() {
		return siindpay;
	}
	public void setSiindpay(String siindpay) {
		this.siindpay = siindpay;
	}
	public String getCommccy() {
		return commccy;
	}
	public void setCommccy(String commccy) {
		this.commccy = commccy;
	}
	public String getRexratetolind() {
		return rexratetolind;
	}
	public void setRexratetolind(String rexratetolind) {
		this.rexratetolind = rexratetolind;
	}
	public String getDealnoexcess() {
		return dealnoexcess;
	}
	public void setDealnoexcess(String dealnoexcess) {
		this.dealnoexcess = dealnoexcess;
	}
	public String getFixrateind() {
		return fixrateind;
	}
	public void setFixrateind(String fixrateind) {
		this.fixrateind = fixrateind;
	}
	public String getFixratecode() {
		return fixratecode;
	}
	public void setFixratecode(String fixratecode) {
		this.fixratecode = fixratecode;
	}
	public String getRevoper() {
		return revoper;
	}
	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}
	public String getCtrpartydealno() {
		return ctrpartydealno;
	}
	public void setCtrpartydealno(String ctrpartydealno) {
		this.ctrpartydealno = ctrpartydealno;
	}
	public String getCtrpartyphoneno() {
		return ctrpartyphoneno;
	}
	public void setCtrpartyphoneno(String ctrpartyphoneno) {
		this.ctrpartyphoneno = ctrpartyphoneno;
	}
	public String getCtrpartyphoneoper() {
		return ctrpartyphoneoper;
	}
	public void setCtrpartyphoneoper(String ctrpartyphoneoper) {
		this.ctrpartyphoneoper = ctrpartyphoneoper;
	}
	public String getBlockind() {
		return blockind;
	}
	public void setBlockind(String blockind) {
		this.blockind = blockind;
	}
	public String getConfcreatedind() {
		return confcreatedind;
	}
	public void setConfcreatedind(String confcreatedind) {
		this.confcreatedind = confcreatedind;
	}
	public String getSpotdate() {
		return spotdate;
	}
	public void setSpotdate(String spotdate) {
		this.spotdate = spotdate;
	}
	public String getDisaggccy() {
		return disaggccy;
	}
	public void setDisaggccy(String disaggccy) {
		this.disaggccy = disaggccy;
	}
	public String getDisaggspottrad() {
		return disaggspottrad;
	}
	public void setDisaggspottrad(String disaggspottrad) {
		this.disaggspottrad = disaggspottrad;
	}
	public String getDisaggspotport() {
		return disaggspotport;
	}
	public void setDisaggspotport(String disaggspotport) {
		this.disaggspotport = disaggspotport;
	}
	public String getDisaggspotcost() {
		return disaggspotcost;
	}
	public void setDisaggspotcost(String disaggspotcost) {
		this.disaggspotcost = disaggspotcost;
	}
	public String getDisaggfwdtrad() {
		return disaggfwdtrad;
	}
	public void setDisaggfwdtrad(String disaggfwdtrad) {
		this.disaggfwdtrad = disaggfwdtrad;
	}
	public String getDisaggfwdport() {
		return disaggfwdport;
	}
	public void setDisaggfwdport(String disaggfwdport) {
		this.disaggfwdport = disaggfwdport;
	}
	public String getDisaggfwdcost() {
		return disaggfwdcost;
	}
	public void setDisaggfwdcost(String disaggfwdcost) {
		this.disaggfwdcost = disaggfwdcost;
	}
	public BigDecimal getFwdpremamt() {
		return fwdpremamt;
	}
	public void setFwdpremamt(BigDecimal fwdpremamt) {
		this.fwdpremamt = fwdpremamt;
	}
	public String getAmenddate() {
		return amenddate;
	}
	public void setAmenddate(String amenddate) {
		this.amenddate = amenddate;
	}
	

}
