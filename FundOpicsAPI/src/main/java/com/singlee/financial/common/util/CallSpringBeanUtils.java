package com.singlee.financial.common.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CallSpringBeanUtils {
	
	private static String springContextHolder = "com.singlee.financial.common.spring.SpringContextHolder";

	/**
	 * 执行带返回参数的方法,输入参数可以有也可以为空
	 * @param beanId
	 * @param methodName
	 * @param retClz
	 * @param inParam
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static<T extends Object,K extends Object> T call(String beanId,String methodName,Class<T> retClz,K...inParam) {
		T retObj = null;
		Object objServer = null;
		Object callObj = null;
		try {
			Class<T> threadClazz = (Class<T>) Class.forName(springContextHolder);
			Method method = threadClazz.getMethod("getBean", String.class);
			objServer = method.invoke(null, beanId);
			List<Class<?>> list_clzs = new ArrayList<Class<?>>();
			for (K clz : inParam) {
				list_clzs.add(clz.getClass());
			}
			method = objServer.getClass().getMethod(methodName, list_clzs.toArray(new Class<?>[list_clzs.size()]));
			callObj = method.invoke(objServer,inParam);
			if(null != retClz) {
				retObj = (T) callObj;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return retObj;
	}
	
	/**
	 * 执行带参无返回的方法
	 * @param beanId
	 * @param methodName
	 * @param inParam
	 */
	@SuppressWarnings("unchecked")
	public static<K extends Object> void call(String beanId,String methodName,K...inParam) {
		call(beanId,methodName,null,inParam);
	}
	
}
