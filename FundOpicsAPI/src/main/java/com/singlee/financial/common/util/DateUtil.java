package com.singlee.financial.common.util;

import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 日期类型与字符串类型相互转换工具类
 *
 * @author cz
 */
public class DateUtil {

    private static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
    public static final FastDateFormat COMPACT_DATETIME_FORMAT = FastDateFormat.getInstance("yyyyMMddHHmmss");
    public static final FastDateFormat TIME_FORMAT_PATTERN = FastDateFormat.getInstance("HH:mm:ss");
    public static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd");
    public static final String DATETIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static String[] parsePatterns = {"yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy/MM/dd",
            "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyyMMdd"};

    /**
     * 取当前系统时间【HH:mm:ss】
     */
    public static String getCurrentTimeAsString() {
        return TIME_FORMAT_PATTERN.format(new Date());
    }

    public static String getCurrentTimeAsString(String pattern) {
        FastDateFormat formatter = FastDateFormat.getInstance(pattern);
        return formatter.format(new Date());
    }

    /**
     * 根据当前传输值对【HH:mm:ss】进行加减处理
     *
     * @param secondCount
     * @return
     */
    public static String getTimeAsStringByCount(int secondCount) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, secondCount);
        return TIME_FORMAT_PATTERN.format(calendar);
    }

    /**
     * 返回当前日期，按<code>yyyyMMddHHmmss</code>格式化日期成字符串
     */
    public static String getCurrentCompactDateTimeAsString() {
        return COMPACT_DATETIME_FORMAT.format(new Date());
    }

    /**
     * 返回当前日期，按<code>yyyy-MM-dd</code>格式化日期成字符串
     */
    public static String getCurrentDateAsString() {
        return DATE_FORMAT.format(new Date());
    }

    /**
     * 返回按照当前日期，按<code>pattern</code>格式化的字符串
     */
    public static String getCurrentDateAsString(String pattern) {
        FastDateFormat formatter = FastDateFormat.getInstance(pattern);
        return formatter.format(new Date());
    }

    /**
     * 返回当前日期，按 <code>yyyy-MM-dd HH:mm:ss</code>格式化成字符串
     */
    public static String getCurrentDateTimeAsString() {
        return format(new Date(), DATETIME_FORMAT_PATTERN);
    }

    /**
     * 返回当前日期，按 <code>yyyy-MM-dd HH:mm:ss</code>格式化成字符串
     */
    public static String getCurrentDateTimeAsString(int field, int delta) {
        Calendar cal = Calendar.getInstance();
        cal.add(field, delta);
        Date date = cal.getTime();
        return format(date, DATETIME_FORMAT_PATTERN);
    }

    /**
     * 日期调整 默认为天
     *
     * @param date 日期格式yyyy-MM-dd
     * @param adj
     * @return
     */
    public static final String dateAdjust(String date, int adj) {
        return dateAdjust(date, adj, Calendar.DAY_OF_YEAR);
    }

    /**
     * 日期调整
     *
     * @param date     日期格式 yyyy-MM-dd
     * @param adj      日期调整 正数 往后，负数往前
     * @param timeUnit 为时间单位 天、月、年
     * @return
     * @throws ParseException
     */
    public static final String dateAdjust(String date, int adj, int timeUnit) {
        Date d;
        try {
            d = DateUtils.parseDate(date, "yyyy-MM-dd");
            Date t = dateAdjust(d, adj, timeUnit);
            return DateFormatUtils.format(t, "yyyy-MM-dd");
        } catch (ParseException e) {
            throw new RuntimeException(String.format("[%s] not match for yyyy-MM-dd !", date));
        }
    }

    public static final Date dateAdjust(Date date, int adj, int timeUnit) {
        switch (timeUnit) {
            case Calendar.DAY_OF_YEAR:
                return DateUtils.addDays(date, adj);
            case Calendar.MONTH:
                return DateUtils.addMonths(date, adj);
            case Calendar.YEAR:
                return DateUtils.addYears(date, adj);
            case Calendar.HOUR:
                return DateUtils.addHours(date, adj);
            case Calendar.MINUTE:
                return DateUtils.addMinutes(date, adj);
            default:
                throw new RuntimeException(String.format("[%d] time unit not support !", timeUnit));
        }
    }

    /**
     * 返回当前日期所在月份的第一天
     *
     * @return
     */
    public static Date getStartDateTimeOfCurrentMonth() {
        return getStartDateTimeOfMonth(new Date());
    }

    /**
     * 返回指定日期所在月份的第一天 The value of
     * <ul>
     * <li>Calendar.HOUR_OF_DAY
     * <li>Calendar.MINUTE
     * <li>Calendar.MINUTE
     * </ul>
     * will be set 0.
     */
    public static Date getStartDateTimeOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 返回当前日期所在月份的最后一天
     *
     * @return
     */
    public static Date getEndDateTimeOfCurrentMonth() {
        return getEndDateTimeOfMonth(new Date());
    }

    /**
     * 返回指定日期所在月份的最后一天
     *
     * @param date
     * @return
     */
    public static Date getEndDateTimeOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    /**
     * 返回当天的凌晨时间
     *
     * @return
     */
    public static Date getStartTimeOfCurrentDate() {
        return getStartTimeOfDate(new Date());
    }

    /**
     * 返回指定日期的凌晨时间
     *
     * @param date
     * @return
     */
    public static Date getStartTimeOfDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * 返回当天的结束时间 <tt>2005-12-27 17:58:56</tt> will be returned as
     * <tt>2005-12-27 23:59:59</tt>
     */
    public static Date getEndTimeOfCurrentDate() {
        return getEndTimeOfDate(new Date());
    }

    /**
     * 返回指定日期的结束时间
     *
     * @param date
     * @return
     */
    public static Date getEndTimeOfDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTime();
    }

    /**
     * 解析字符串成java.util.Date 使用常用日期类型
     * <p>
     * 支持的日期字符串格式包括"yyyy-MM-dd","yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
     * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm"
     *
     * @param str
     * @return
     * @throws ParseException
     */
    public static Date parse(String str) {
        try {
            if (null == str) {
                return null;
            }
            return DateUtils.parseDate(str, parsePatterns);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Can't parse " + str + " using " + parsePatterns);
        }
    }

    /**
     * 解析字符串成java.util.Date 使用pattern
     *
     * @param str
     * @param pattern
     * @return
     */
    public static Date parse(String str, String pattern) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        try {
            return DateUtils.parseDate(str, pattern);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Can't parse " + str + " using " + pattern);
        }
    }

    /**
     * 根据时间变量返回时间字符串
     */
    public static String format(Date date, String pattern) {
        if (date == null) {
            return null;
        }
        FastDateFormat df = FastDateFormat.getInstance(pattern);
        return df.format(date);
    }

    /**
     * 按<code>yyyy-MM-dd</code>格式化日期成字符串
     */
    public static String format(Date date) {
        return date == null ? null : DATE_FORMAT.format(date);
    }

    /**
     * 计算两个日期之间的天数
     */
    public static final int daysBetween(java.util.Date early, java.util.Date late) {
        Calendar ecal = Calendar.getInstance();
        Calendar lcal = Calendar.getInstance();
        ecal.setTime(early);
        lcal.setTime(late);

        long etime = ecal.getTimeInMillis();
        long ltime = lcal.getTimeInMillis();

        return (int) ((ltime - etime) / MILLIS_PER_DAY);
    }

    /**
     * 计算两个日期之间的天数
     */
    public static final int daysBetween(String early, String late) {
        try {
            java.util.Date dateEarly = DateUtils.parseDate(early, "yyyy-MM-dd");
            java.util.Date dateLate = DateUtils.parseDate(late, "yyyy-MM-dd");
            return daysBetween(dateEarly, dateLate);
        } catch (ParseException e) {
            throw new RuntimeException(String.format("[%s,%s] not match for yyyy-MM-dd !", early, late));
        }
    }

    /**
     * yyyyMMdd yyyy-MM-dd互相转换
     *
     * @param yyyy_mm_dd
     * @return
     */
    public static final String toDate8Char(String yyyy_mm_dd) {
        return StringUtils.replace(yyyy_mm_dd, "-", "");
    }

    public static final String toDate10Char(String yyyymmdd) {
        return toDate10Char(yyyymmdd, "-");
    }

    public static final String toDate10Char(String yyyymmdd, String splitChar) {
        if (yyyymmdd == null || yyyymmdd.length() < 8) {
            return yyyymmdd;
        } else {
            return yyyymmdd.substring(0, 4) + splitChar + yyyymmdd.substring(4, 6) + splitChar
                    + yyyymmdd.substring(6, 8);
        }
    }

    public static String nowDate() {
        return nowDate("-");
    }

    public static String nowDate(String sper) {
        GregorianCalendar Time = new GregorianCalendar();

        String s_nowD = "";
        String s_nowM = "";
        if (sper == null) {
            sper = "-";
        } else {
            sper = sper.trim();
        }

        int nowY = Time.get(1);
        int nowM = Time.get(2) + 1;
        s_nowM = leftPad(nowM, 2, '0');

        int nowD = Time.get(5);
        s_nowD = leftPad(nowD, 2, '0');

        String result = nowY + sper + s_nowM + sper + s_nowD;

        return result;
    }

    /**
     * 计算两个日期之间小时差
     *
     * @param early 之前时间 yyyy-MM-dd HH:mm:ss
     * @param late  之后时间yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static int hoursBetween(String early, String late) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date1 = sdf.parse(early);
            Date date2 = sdf.parse(late);
            return (int) ((date2.getTime() - date1.getTime()) * 24 / MILLIS_PER_DAY);
        } catch (ParseException e) {
            return 0;
        }
    }

    /**
     * 计算两个日期之差
     *
     * @param early 之前时间 yyyy-MM-dd HH:mm:ss
     * @param late  之后时间yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static int timesBetween(String early, String late) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        try {
            Date date1 = sdf.parse(early);
            Date date2 = sdf.parse(late);
            return (int) ((date2.getTime() - date1.getTime()));
        } catch (ParseException e) {
            // e.printStackTrace();
            return 0;
        }
    }

    public static Date getYearLast(int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.roll(Calendar.DAY_OF_YEAR, -1);
        Date currYearLast = calendar.getTime();
        return currYearLast;
    }

    /**
     * 获取当年最后一天
     *
     * @return
     */
    public static Date getCurrYearLast() {
        Calendar currCalendar = Calendar.getInstance();
        int currYear = currCalendar.get(Calendar.YEAR);
        return getYearLast(currYear);
    }

    /**
     * 判断当前时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @param nowTime   当前时间
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     * @author
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime() || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.after(begin) && date.before(end)) {
            return true;
        } else {
            return false;
        }
    }

    public static String leftPad(int value, int size, char padChar) {
        String str = String.valueOf(value);
        str = leftPad(str, size, padChar);
        return str;
    }

    public static String rightPad(int value, int size, char padChar) {
        String str = String.valueOf(value);
        str = StringUtils.rightPad(str, size, padChar);
        return str;
    }

    /**
     * s 左补 p，按size大小
     */
    public static String leftPad(String s, int size, char p) {
        if (s == null || s.length() > size) {
            return s;
        } else {
            int ps = size - s.length();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < ps; i++) {
                sb.append(p);
            }
            sb.append(s);
            return sb.toString();
        }
    }

    /**
     * 将日期转换为5位int类型， 计算方法：日期距离~1900-01-01天数
     *
     * @param dates
     * @param dit   精度调整excel与java计算相差2天
     * @return
     */
    public static List<Integer> getDayFromOriginDate(List<Date> dates, int dit) {
        List<Integer> ret = new ArrayList<Integer>();
        Date orginDate = DateUtil.parse("1900-01-01", "yyyy-MM-dd");
        for (Date date : dates) {
            ret.add(DateUtil.daysBetween(orginDate, date) + dit);
        }
        return ret;
    }

    /**
     * 根据币种获取假日
     * <p>
     * 阿联酋迪拉姆（AED）和沙特里亚尔（SAR）的周末为周五、周六，其余货币周末为周六、周日。
     *
     * @param startYear
     * @param endYear
     * @param ccy
     * @return
     */
    public static List<Date> getWeekDay(int startYear, int endYear, String ccy) {
        List<Date> dateList = new ArrayList<Date>();
        for (int j = 0; j <= endYear - startYear; j++) {
            Calendar calendar = new GregorianCalendar(startYear + j, 0, 1);
            int i = 1;
            int year = startYear + j;
            while (calendar.get(Calendar.YEAR) < year + 1) {
                calendar.set(Calendar.WEEK_OF_YEAR, i++);
                calendar.set(Calendar.DAY_OF_WEEK,
                        ("SAR".equalsIgnoreCase(ccy) || "AED".equalsIgnoreCase(ccy)) ? Calendar.FRIDAY
                                : Calendar.SUNDAY);
                if (calendar.get(Calendar.YEAR) == year) {
                    dateList.add(calendar.getTime());
                }
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                if (calendar.get(Calendar.YEAR) == year) {
                    dateList.add(calendar.getTime());
                }
            }
        }
        return dateList;
    }

    /**
     * 默认生产50年的年度假日
     *
     * @param year
     * @param annual
     * @return
     */
    public static List<Date> getAnnelDay(Integer year, List<String> annual) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM,yyyy", Locale.ENGLISH);
        List<Date> dateList = new ArrayList<Date>();
        for (int i = year; i <= year + 50; i++) {
            for (String anl : annual) {
                try {
                    dateList.add(sdf.parse(anl + "," + i + ""));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return dateList;
    }

    public static void main(String[] args) throws ParseException {
        List<String> annual = new ArrayList<String>();
        annual.add("01 May");
        annual.add("01 Oct");
        List<Date> dts = DateUtil.getAnnelDay(2021, annual);
        for (Date date : dts) {
            System.out.println("====> " + date);
        }
        List<Integer> its = getDayFromOriginDate(dts, 2);
        for (Integer integer : its) {
            System.out.println(integer);
        }
    }

}
