package com.singlee.financial.common.util;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 字符处理类
 * 
 * @author shenzhaola335
 * 
 */
public class DataUtils {

	private static Pattern NUMBER_PATTERN = Pattern.compile("[0-9]*.?[0-9]*$");
	/**
	 * 获取字符串中的中文字符数
	 * 
	 * @param o
	 * @return
	 */
	public static int getChineseCharLen(Object o) {

		String str = (String) o;

		int count = 0;

		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) < 0 || str.charAt(i) > 255) {
				count += 1;
			}
		}

		return count;
	}

	/**
	 * 字符类型格式化函数右对齐左补空格 根据传入的
	 * 
	 * type =null对字符进行数字格式化,其它值只对字符进行格式化处理
	 * 
	 * @param o
	 * @param length
	 * @param fillStr
	 * @param digits
	 * @return
	 */
	public static String formatStringByLeftBlank(String o, int length, String type) {

		if (("").equals(o) || null == o) {
			return o;
		}

		String formatStr = "";

		String formatRule = "%1$" + (length - getChineseCharLen(o)) + "s";

		if (type == null) {
			DecimalFormat format = new DecimalFormat("#0.00");
			if (!NUMBER_PATTERN.matcher(o).matches()) {
				return o;
			}
			o = format.format(Double.parseDouble(o));
		}
		formatStr = String.format(formatRule, o);
		return formatStr;
	}

	/**
	 * 字符类型格式化函数左对齐右补空格
	 * 
	 * @param o
	 * @param length
	 * @param fillStr
	 * @return
	 */
	public static String formatStringByRightBlank(String o, int length, String fillStr) {
		o = o == null ? "" : o;
		String formatStr = "";
		String formatRule = "";
		if (o == null || length <= 0) {
			formatRule = "%" + length + "$s";
			formatStr = String.format(formatRule, "");
		} else {
			formatRule = "%-" + (length - getChineseCharLen(o)) + "s";
			formatStr = String.format(formatRule, o);

		}

		return formatStr;
	}

	/**
	 * BigDecimal格式化函数
	 * 
	 * @param o
	 * @param length
	 * @param fillStr
	 *            "0"
	 * @return
	 */
	public static String formatBigDecimal(BigDecimal o, int length, String fillStr) {

		String formatStr = "";

		if (o.toString().contains(".")) {

			String leftStr = o.toString().substring(0, o.toString().lastIndexOf("."));

			String rightStr = o.toString().substring(o.toString().lastIndexOf("."), o.toString().length());

			formatStr = String.format("%0" + length + "d", (new BigDecimal(leftStr)).longValue()) + rightStr;

		} else {
			String formatRule = "%1$" + fillStr + (length) + "d";

			formatStr = String.format(formatRule, ((BigDecimal) o).longValue());
		}

		return formatStr;
	}

	/**
	 * Integer格式化函数
	 * 
	 * @param o
	 * @param length
	 * @param fillStr
	 *            "0"
	 * @return
	 */
	public static String formatInteger(Integer o, int length, String fillStr) {

		return String.format("%1$" + fillStr + length + "d", o);

	}

	/**
	 * /** Double格式化函数
	 * 
	 * @param o
	 * @param length
	 * @param fillStr
	 *            "0"
	 * @return
	 */
	public static String formatDouble(Double o, int length, String fillStr) {

		return String.format("%1$" + fillStr + length + "f", o);

	}

	/**
	 * 日期格式化函数
	 * 
	 * @param o
	 *            ---java.util.Date
	 * @param length
	 * @param fillStr
	 *            日期格式化表达式 "yyyy-MM-dd"
	 * @return
	 */
	public static String formatDate(Date o, int length, String fillStr) {

		SimpleDateFormat sdf = new SimpleDateFormat(fillStr);

		return String.format("%1$" + length + "s", sdf.format(o));

	}

	/**
	 * 数据库格式时间戳格式化函数
	 * 
	 * @param o
	 *            --java.sql.Timestamp
	 * @param length
	 * @param fillStr
	 *            日期格式化表达式 "yyyy-MM-dd"
	 * @return
	 */
	public static String formatTimestamp(Timestamp o, int length, String fillStr) {

		SimpleDateFormat sdf = new SimpleDateFormat(fillStr);
		return String.format("%1$" + length + "s", sdf.format(o));
	}

	/**
	 * 格式化为字符
	 * 
	 * @param s
	 * @return
	 */
	public static String forString(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != "0".charAt(0)) {
				return s.substring(i, s.length());
			}
		}
		return s;
	}

	/**
	 * 去掉所有的双引号"
	 */
	public static String deleteSYH(String str) {
		return str.replaceAll("\"", "");
	}

	/**
	 * 格式化为数字
	 * 
	 * @param str
	 * @return
	 */
	public static Integer getInteger(String str) {
		if (str != null && !"".equals(str)) {
			return new Integer(forString(str));
		} else {
			return null;
		}

	}

	public static Map Obj2Map(Object obj) {
		if (obj instanceof Map) {
			return (Map) obj;
		}
		// return null;
		return new HashMap();
	}

	public static String formatStringByLeftZero(String str, int strlength) {
		if (str == null || "".equals(str)) {
			return "";
		}
		int strlen = str.length();
		if (strlen < strlength) {
			while (strlen < strlength) {
				StringBuffer sb = new StringBuffer();
				sb.append("0").append(str);
				str = sb.toString();
				strlen = str.length();
			}
		} else {
			str = str.substring(strlen - strlength, strlen);
		}
		return str;
	}

	public static void main(String[] args) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, Exception {

		// System.out.println(DataUtils.getChineseCharLen("xxx申照腊xxx"));
		// System.out.println(DataUtils.formatStringByLeftBlank("1234657983",
		// 17, null));
		// System.out.println(DataUtils.formatStringByRightBlank("123",
		// 10, "0") + "结束!");
		// System.out.println(DataUtils.formatBigDecimal(new BigDecimal(".84"),
		// 20, "") + "结束!");
		// System.out.println(DataUtils.formatInteger(80213111, 3, "0"));
		// System.out.println(DataUtils.formatDouble(8.56240000000551, 30, ""));
		// System.out.println(DataUtils.formatTimestamp(new Timestamp(18), 10,
		// "yyyy-MM-dd"));
		// System.out.println(DataUtils.forString(""));
		// System.out.println(DataUtils.getInteger("124"));
		// System.out.println(Double.valueOf("0.48"));
		// System.out.println(Double.parseDouble("0.48"));
		// System.out.println(DataUtils.getInteger(""));
		// String sb = "11845C161110301       990900000";
		// System.out.println(StringUtils.deleteWhitespace(sb));
//		System.out.println(DataUtils.formatStringByLeftZero("658999125559", 7));
	}
}