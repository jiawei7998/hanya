package com.singlee.financial.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 文件拷贝类
 */
public class FileChannelUtil {

    /**
     * 使用文件通道的方式复制文件
     *
     * @param src 源文件
     * @param dst 复制到的新文件
     */
    public static boolean fileCopy(File src, File dst) {
        Boolean ret = true;
        FileInputStream fi = null;
        FileOutputStream fo = null;
        FileChannel in = null;
        FileChannel out = null;
        try {
            fi = new FileInputStream(src);
            fo = new FileOutputStream(dst);
            in = fi.getChannel();//得到对应的文件通道
            out = fo.getChannel();//得到对应的文件通道
            in.transferTo(0, in.size(), out);//连接两个通道，并且从in通道读取，然后写入out通道
        } catch (IOException e) {
            ret = false;
            e.printStackTrace();
        } finally {
            try {
                fi.close();
                in.close();
                fo.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public static boolean createDirs(Path path) {
        try {
            if (!Files.exists(path)) {
                Path directories = Files.createDirectories(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return Files.exists(path);
        }
    }

    public static boolean createDirs(URI uri) {
        return createDirs(Paths.get(uri));
    }

    public static boolean createDirs(String path) {
        return createDirs(Paths.get(path));
    }

    public static boolean createFile(File file) {
        try {
            if (createDirs(file.getParent())) {
                Files.createFile(file.toPath());
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return Files.exists(file.toPath());
        }
    }

    public static boolean createFile(URI uri) {
        return createFile(Paths.get(uri));
    }

    public static boolean createFile(String path) {
        return createFile(Paths.get(path));
    }

    public static boolean createFile(Path path) {
        try {
            if (createDirs(path.getParent())) {
                Files.createFile(path);
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            return Files.exists(path);
        }
    }

    /**
     * 测试
     */
    public static void main(String[] args) {
        FileChannelUtil fileChannelUtil = new FileChannelUtil();
        // 本地存放地址
        String localPath = "D:\\apache-tomcat-9.0.45\\bin\\singleeMsg\\acup\\";
        // Sftp下载路径
        String sftpPath = "d:/interface_SIT/file_in/20250323/in/";
        String name = "I00300100SAFM202503239007.DAT";
        createDirs(sftpPath);
        fileChannelUtil.fileCopy(new File(localPath + name), new File(sftpPath + name));

    }
}
