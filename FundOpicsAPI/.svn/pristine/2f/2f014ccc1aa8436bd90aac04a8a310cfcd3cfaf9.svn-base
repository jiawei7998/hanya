package com.singlee.financial.wks.bean;

import java.math.BigDecimal;
import java.util.List;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.IboIntSchdWksBean;
import com.singlee.financial.wks.expand.IboPrinSchdWksBean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 银行间拆借对象
 * 
 * @author shenzl
 *
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class IboWksBean extends CommonBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8209706355985882503L;

	/**
	 * 交易的编号
	 */
	private String tradeNo;

	/**
	 * 产品类型 MM_BR_L 拆入 MM_LD_A 存放 MM_LN_A 拆出 MM_DP_L 存放
	 * 
	 */
	private String prodtype;

	/**
	 * 交易日
	 */
	private String dealDate;
	/**
	 * 起息日
	 */
	private String vdate;
	/**
	 * 到日期
	 */
	private String mdate;

	/**
	 * 计息基础
	 */
	private String basis;

	/**
	 * 交易货币
	 */
	private String ccy;

	/**
	 * 交易货币金额/本金调整后金额
	 */
	private BigDecimal ccyamt;

	/**
	 * 利率代码
	 */
	private String ratecode;
	/**
	 * 利率/利率调整后值
	 */
	private BigDecimal intrate8;
	/**
	 * 付息频率
	 */
	private String sechuleType;
	/**
	 * 交易对手
	 */
	private String cno;
	/**
	 * 兑付本金计划
	 */
	private List<IboPrinSchdWksBean> prinSchdList;
	/**
	 * 兑付利息计划
	 */
	private List<IboIntSchdWksBean> intSchdList;

	/**
	 * 提前终止/展期/利率调整/本金调整日期
	 */
	private String emdate;
	/**
	 * 违约金额
	 */
	private BigDecimal empenamt;

}
