package com.singlee.financial.opics;

import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSecmBean;
import com.singlee.financial.page.PageInfo;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;
import java.util.Map;

/**
 * 提供调用外部接口
 * 
 * @param <T>
 *            输入泛型对象
 * 
 * @param <V>
 *            输出泛型对象
 * 
 */
@Opics(context = OpicsContext.SINGLEE_API, uri = "/externalServerExporter")
public interface IExternalServer {

	/**
	 * 将债券信息导入到OPICS系统中
	 * 
	 * @param map
	 * @return
	 */
	@Deprecated
	SlOutBean secmProc(Map<String, String> map) throws  Exception;

	/**
	 * 校验OPICS交易对手信息
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedCust(SlCustBean custBean) throws  Exception;

	/**
	 * 导入OPICS交易对手信息
	 * 
	 * @param custBean
	 * @return
	 */
	SlOutBean addCust(SlCustBean custBean) throws  Exception;

	/**
	 * 校验OPICS债券基本信息
	 * 
	 * @author shenzl
	 * @param baseBean
	 * @return
	 * @since JDK 1.6
	 */
	SlOutBean verifiedSemm(SlSecmBean secmBean) throws  Exception;

	/**
	 * 导入债券基本信息
	 * 
	 * @param secmBean
	 * @return
	 */
	SlOutBean addSemm(SlSecmBean secmBean) throws  Exception;
	
	/**
	 * 批量查询OPICS 交易对手信息用于日终同步
	 * 
	 * @return
	 */
	List<SlCustBean> synchroCust() throws  Exception;
	/**
	 * 
	 * 通过secid查询债券基本信息
	 * @param secid
	 * @return
	 * @since JDK 1.6
	 */
	SlSecmBean getSemmById(String secid) throws  Exception;
	/**
	 * 批量查询OPICS 债券信息用于日终同步
	 * @return
	 * @since JDK 1.6
	 */
	List<SlSecmBean> synchroSemm() throws  Exception;
	/**
	 * 获取交易对手信息
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	SlCustBean getCust(Map<String, String> map)throws  Exception;
	/**
	 * 分页查询交易对手
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	PageInfo<SlCustBean> searchPageCust(Map<String, Object> map) throws  Exception;

}
