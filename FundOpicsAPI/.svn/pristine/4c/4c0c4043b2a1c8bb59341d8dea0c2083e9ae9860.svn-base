package com.singlee.financial.marketdata.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 收益率
 * 
 * @author shenzl
 *
 */
public class YieldCurveIndex implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7531161042227486715L;

	/**
	 * 货币代码
	 */
	private String ccy;
	/**
	 * 曲线名称
	 */
	private String yieldCurveName;

	/**
	 * 生效日期
	 */
	private Date spotDate;
	/**
	 * 期限指标【ON/1W/2W/1Y...】
	 */
	private String designatedMaturity;
	/**
	 * 对应的到期日期
	 */
	private Date maturityDate;
	/**
	 * 收益率中间价
	 */
	private BigDecimal rate;

	/**
	 * 价格更新日期
	 */
	private Date lstmntdate;
	
	/**
	 * 收益率询价
	 */
	private BigDecimal rateAsk;
	
	
	/**
	 * 收益率卖价
	 */
	private BigDecimal rateBid;

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getYieldCurveName() {
		return yieldCurveName;
	}

	public void setYieldCurveName(String yieldCurveName) {
		this.yieldCurveName = yieldCurveName;
	}

	public Date getSpotDate() {
		return spotDate;
	}

	public void setSpotDate(Date spotDate) {
		this.spotDate = spotDate;
	}

	public String getDesignatedMaturity() {
		return designatedMaturity;
	}

	public void setDesignatedMaturity(String designatedMaturity) {
		this.designatedMaturity = designatedMaturity;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public BigDecimal getRateAsk() {
		return rateAsk;
	}

	public void setRateAsk(BigDecimal rateAsk) {
		this.rateAsk = rateAsk;
	}

	public BigDecimal getRateBid() {
		return rateBid;
	}

	public void setRateBid(BigDecimal rateBid) {
		this.rateBid = rateBid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("YieldCurveIndex [ccy=");
		builder.append(ccy);
		builder.append(", yieldCurveName=");
		builder.append(yieldCurveName);
		builder.append(", spotDate=");
		builder.append(spotDate);
		builder.append(", designatedMaturity=");
		builder.append(designatedMaturity);
		builder.append(", maturityDate=");
		builder.append(maturityDate);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", lstmntdate=");
		builder.append(lstmntdate);
		builder.append(", rateAsk=");
		builder.append(rateAsk);
		builder.append(", rateBid=");
		builder.append(rateBid);
		builder.append("]");
		return builder.toString();
	}

}
