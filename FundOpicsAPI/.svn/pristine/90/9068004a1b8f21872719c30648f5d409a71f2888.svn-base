package com.singlee.financial.wks.bean;

import com.singlee.financial.wks.expand.CommonBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;
import com.singlee.financial.wks.expand.SwapCashFlowMethod;
import com.singlee.financial.wks.expand.SwapItemSchdWksBean;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 互换交易数据
 * 
 * @author xuqq 20200204
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SwapWksBean extends CommonBean {
	private static final long serialVersionUID = 1L;
	private String tradeNo;// 交易编号
	// 交易信息
	private String partyId;// 客户号
	private String dealDate;// 交易日
	private String vDate;// 开始日期
	private String mDate;// 到期日|提前终止日
	private String cno;// 交易对手
	// 互换的腿信息
	private String ccy;// 资金币种
	private BigDecimal amt;// 名义货币金额
	private String intPayAdjMod;// 付息日确定规则
	private String firstIntPayDate;// 首次付息日期
	private String lastIntPayDate;// 最后一个付息日期
	private String pType;// IRS_P1R2-付固定收浮动 IRS_P2R1-付浮动收固定
	// 付
	private String payRateType;// 固定利率浮动利率标志|提前终止
	private BigDecimal payFixedRate;// 固定利率
	private String payFloatIndicator;// 浮动指标 浮动方基准利率
	private String payFloatType;// 浮动方式 点数/百分比 BasisPointOrPercentage
	private BigDecimal payFloatBps;// BP点差
	private BigDecimal payFloatPerc; // 总分浮动百分比
	private String payCalIntBasic; // 计息基数代码
	private String payCalIntType; // 复利标志:M=复利/N=单利
	private String payPaymentFrequency;// 付息周期代码
	private String payRefixRule; // 浮动方日期调整规则 DateAdjustment
	private String payInterestResetFrequency;// 利率重置周期
	private BigDecimal payFxrate;// 折人民币汇率
	private String payAcctType;// 利息结算方式
	private String payAcctNo;// 利息结算帐户
	private String payYieldcurve;// 收益率曲线：为空默认swap TODO 未确认
	private String payRateCode;// 利率代码 TODO 未确认
	private String payRaterevdte;// 利率重置日期 TODO 未确认
	private String payRaterevpayrule;// 利率重置规则 TODO 未确认
	private BigDecimal payNominalAmount;// 支付端本金
	private String payCashFlowMethod;// 现金流调整规则 CHINA -- ISDA
	private String payCcy;// 付端币种
	private String paySeq;// 付端序号
	private String payDiscountYieldcurve;//付端贴线曲线
	// 收
	private String recRateType; // 利率类型 固定/浮动 FixedOrFloat
	private BigDecimal recFixedRate; // 固定利率
	private String recFloatIndicator; // 浮动指标 浮动方基准利率
	private String recFloatType; // 浮动方式 点数/百分比 BasisPointOrPercentage
	private BigDecimal recFloatBps; // 总分浮动点数
	private BigDecimal recFloatPerc; // 总分浮动百分比
	private String recCalIntBasic; // 计息基准 DayCounter
	private String recCalIntType; // 计息方式 单利/复利 Compounding
	private String recPaymentFrequency; // 付息频率 CouponIntFrequently
	private String recRefixRule; // 浮动方日期调整规则 DateAdjustment
	private String recInterestResetFrequency; // 浮动方重置频率 frequency
	private BigDecimal recFxrate;// 折人民币汇率
	private String recAcctType;// 利息结算方式
	private String recAcctNo;// 利息结算帐户
	private String recYieldcurve;// 收益率曲线：为空默认swap TODO 未确认
	private String recRateCode;// 利率代码 TODO 未确认
	private String recRaterevdte;// 利率重置日期 TODO 未确认
	private String recRaterevpayrule;// 利率重置规则 TODO 未确认
	private BigDecimal recNominalAmount;// 支付端本金
	private String recCashFlowMethod;// 现金流调整规则 CHINA -- ISDA
	private String recCcy;// 收端币种
	private String recSeq;// 收端序号
	private String recDiscountYieldcurve;//收端贴线曲线
	private SettleInfoWksBean settleAcct;// 清算信息
	private List<SwapItemSchdWksBean> itemBeanSwds;// 收付息计划
	
	private SwapCashFlowMethod cashFlowMethod;//计算现金流方法
	private String orgTradeNo;//原交易流水号
	
	private String earlyDate;//提前终止日期
	private BigDecimal earlyAmount;//罚金(CFETS估值,系统自动扣减已有利息金额)
	private String earlyCcy;//罚金货币
	private String earlyDirection;//罚金收付方向
	
	private String swapType = "I";//I-利率互换;C-货币互换
	private String exchPrin;//本金交换方式
	private BigDecimal payFinExchAmt;
	private String payFinExchCcy;
	private String payFinExchDate;
	private BigDecimal payInitExchAmt;
	private String payInitExchCcy;
	private String payInitExchDate;
	private BigDecimal recFinExchAmt;
	private String recFinExchCcy;
	private String recFinExchDate;
	private BigDecimal recInitExchAmt;
	private String recInitExchCcy;
	private String recInitExchDate;

	/**
	 * 当前系统批量日期
	 */
	private Date batchDate;
}
