package com.singlee.slbpm.expression;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.slbpm.externalInterface.QualificationInterface;
import org.activiti.engine.delegate.DelegateExecution;

import java.io.Serializable;

public class QualificationManagement implements Serializable {

	private static final long serialVersionUID = 8144955105515962847L;

	public int checkQualification(DelegateExecution execution) {
		
		// 获取业务流水号
		String serial_no = execution.getProcessBusinessKey();
		
		QualificationInterface qi = SpringContextHolder.getBean("FlowIndexCalcUtil");
		
		return qi.checkQualification(serial_no);
	}

	public int getCustomerLevel(DelegateExecution execution) {

		// 获取业务流水号
		String serial_no = execution.getProcessBusinessKey();
		QualificationInterface qi = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return qi.getCustomerLevel(serial_no);
	}
}
