package com.singlee.slbpm.expression;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.slbpm.externalInterface.RiskInterface;
import org.activiti.engine.delegate.DelegateExecution;

import java.io.Serializable;

public class RiskManagement implements Serializable {

	private static final long serialVersionUID = 6838398782043912969L;

	/**
	 * 风险等级判断
	 * @return 0-低 1-中 2-高
	 */
	public int getRiskLevel(DelegateExecution execution) {
		
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getRiskLevel(serial_no);
	}

	/**
	 * 资金来源判断
	 * @return 0-自营资金 1-理财资金
	 */
	public int checkSourceOfMoney(DelegateExecution execution) {
		
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.checkSourceOfMoney(serial_no);
	}

	/**
	 * 发起机构类型判断
	 * @return 0-总行 1-分行 2-支行 9-部门
	 */
	public int checkInstitutionType(DelegateExecution execution) {
		
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.checkInstitutionType(serial_no);
	}
}
