package com.singlee.slbpm.expression;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.slbpm.dict.DictConstants;
import com.singlee.slbpm.externalInterface.CreditInterface;
import org.activiti.engine.delegate.DelegateExecution;

import java.io.Serializable;

public class CreditManagement implements Serializable {

	private static final long serialVersionUID = -6547467520472988657L;

	/**
	 * 额度占用
	 * @param execution
	 * @return
	 */
	public String UseCredit(DelegateExecution execution) {
		
		// 获取业务流水号
		String serial_no = execution.getProcessBusinessKey();
		
		CreditInterface ci = SpringContextHolder.getBean("CustCreditInterfaceService");
		
		return ci.UseCredit(serial_no) ? DictConstants.YesNo.YES : DictConstants.YesNo.NO;
	}
	
	/**
	 * 额度释放
	 * @param execution
	 * @return
	 */
	public String ReleaseCredit(DelegateExecution execution) {
		
		// 获取业务流水号
		String serial_no = execution.getProcessBusinessKey();
		
		CreditInterface ci = SpringContextHolder.getBean("CustCreditInterfaceService");
		
		return ci.ReleaseCredit(serial_no) ? DictConstants.YesNo.YES : DictConstants.YesNo.NO;
	}
	
	/**
	 * 获取额度占用期限
	 * @param execution
	 * @return
	 */
	public int getCreditPeriod(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();		
		CreditInterface ci = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ci.getCreditPeriod(serial_no);
	}
}
