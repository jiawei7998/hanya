package com.singlee.slbpm.expression;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.slbpm.pojo.bo.ProcSignSetting;
import com.singlee.slbpm.pojo.vo.ApproveUser;
import com.singlee.slbpm.pojo.vo.VoteResult;
import com.singlee.slbpm.service.FlowQueryService;
import com.singlee.slbpm.service.FlowRoleService;
import com.singlee.slbpm.util.SlbpmNodeSignConstants;
import com.singlee.slbpm.util.SlbpmVariableNameConstants;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.impl.bpmn.behavior.ParallelMultiInstanceBehavior;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.task.TaskDefinition;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;

@SuppressWarnings("unchecked")
public class CounterUserAssignment implements Serializable {

    private static Logger logger = LoggerFactory.getLogger("SLBPM");

	private static final long serialVersionUID = -5487304503889919250L;

	/**
	 * 获取会签用户集合
	 * 先去数据表内取得当前会签节点指定的用户集合（一般为上一步审批人指定）
	 * 如果找不到，则默认取流程设计时该节点指定角色下的所有用户
	 * @param execution
	 * @return
	 */
	public List<String> getSignUser(ActivityExecution execution) {
        
		// 查询自定义会签用户	
		List<String> list = new ArrayList<String>();
		String task_def_key = "";
		Map<String, String> map = (HashMap<String, String>)execution.getVariable(SlbpmVariableNameConstants.CountersignUser);
		
		if (map != null) {
			task_def_key = map.get("task_def_key");
			list = Arrays.asList(map.get("sign_user").split(","));
		}
		
		String act_id = execution.getActivity().getId();
		
		if (task_def_key.equals(act_id) && list.size() > 0) {
			
			return list;
		}
		else {
		
			// 取流程设计时该节点指定角色下的所有用户
			ActivityImpl activityImpl = (ActivityImpl)execution.getActivity();
			return this.getDefaultUserlist(activityImpl);
		}
	}
	
	/**
	 * 清除自定义会签用户
	 * @param execution
	 */
	private void clearSignUser(ActivityExecution execution) {
		
		execution.removeVariable(SlbpmVariableNameConstants.CountersignUser);
	}
	
	/**
	 * 取流程节点默认角色下的用户
	 * @param activity
	 * @return
	 */
	private List<String> getDefaultUserlist(ActivityImpl activity) {

		FlowRoleService flowRoleService = SpringContextHolder.getBean("flowRoleService");
		
		List<ApproveUser> userList = new ArrayList<ApproveUser>(); 
		List<String> list = new ArrayList<String>();
		
		if (activity.getActivityBehavior().getClass().equals(ParallelMultiInstanceBehavior.class)) {
			
			TaskDefinition td = ((UserTaskActivityBehavior)((ParallelMultiInstanceBehavior)activity.getActivityBehavior()).getInnerActivityBehavior()).getTaskDefinition();
		
			List<String> lst_group = new ArrayList<String>();
			for(Expression group_id : td.getCandidateGroupIdExpressions()) {
				if (group_id != null && StringUtils.isNoneEmpty(group_id.getExpressionText())) {
					lst_group.add(group_id.getExpressionText());
				}
			}
			
			String roles = StringUtils.join(lst_group, ",");
			userList = flowRoleService.getUserMapByRole(roles, null, null);
		}
		
		for (ApproveUser user: userList) {
			list.add(user.getOriginalUserId());
		}
		
		return list;
	}
	
	/**
	 * 判断会签节点是否满足通过条件
	 * @param execution
	 * @return
	 */
	public boolean isComplete(ActivityExecution execution) throws Exception {

		FlowQueryService flowQueryService = SpringContextHolder.getBean("flowQueryService");
	    
	    logger.debug("判断会签节点是否结束...");
	    
	    String actInstId = execution.getProcessInstanceId();
	    String nodeId = execution.getActivity().getId();

	    // 取得会签设置的规则
	    ProcSignSetting procSignSetting = flowQueryService.getSignSetting(nodeId);
	    
	    // 完成会签的次数
	    Integer completeCounter = (Integer)execution.getVariable("nrOfCompletedInstances");
	    
	    // 总循环次数
	    Integer instanceOfNumbers = (Integer)execution.getVariable("nrOfInstances");
	    
	    // 计算投票结果
	    VoteResult voteResult = calcResult(procSignSetting, actInstId, nodeId, completeCounter, instanceOfNumbers);
	    
	    String signResult = voteResult.getSignResult();
	    boolean isCompleted = voteResult.getIsCompleted();
	    
	    if(isCompleted){
	    
	        // 统计会签的结果
	        execution.setVariable("signResult_" + nodeId, signResult);
	        
	        // 清除会签投票结果
	        this.clearSignUser(execution);
	    }

	    return isCompleted;
	}
	
	/**  
	 * 根据会签规则计算投票结果
	 * 1.如果会签规则为空，那么需要所有的人同意通过会签，否则不通过
	 * 2.否则按照规则计算投票结果
	 * @param procSignSetting       会签规则
	 * @param actInstId         	流程实例ID
	 * @param nodeId            	节点id名称
	 * @param completeCounter       循环次数
	 * @param instanceOfNumbers     总的会签次数
	 * @return  
	 */  
	private VoteResult calcResult(ProcSignSetting procSignSetting, String actInstId, String nodeId, 
								  Integer completeCounter, Integer instanceOfNumbers) throws Exception {

		FlowQueryService flowQueryService = SpringContextHolder.getBean("flowQueryService");
		
		VoteResult voteResult = new VoteResult();
		
	    // 没有会签实例
	    if(instanceOfNumbers==0) {
	        return voteResult;
	    }
	    
	    // 投同意票数
	    Integer agreeVotesCounts = flowQueryService.getAgreeVoteCount(actInstId, nodeId);
	    
	    // 没有设置会签规则
	    // （那么得全部会签通过才通过，否则不通过)
	    if(procSignSetting == null){
	    	
	        // 还没有完成可以退出
	        if(completeCounter<instanceOfNumbers){
	            return voteResult;
	        }
	        else{
	        	
	            // 完成了 (全部同意才通过)
	            if(agreeVotesCounts.equals(instanceOfNumbers)){
	                return new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_PASS, true);
	            }
	            else{
	                return new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_REFUSE, true);
	            }
	        }
	    }
	      
	    // 投反对票数
	    Integer refuseVotesCounts = flowQueryService.getRefuseVoteCount(actInstId, nodeId);
	    
	    String voteType = procSignSetting.getVote_type();
	    String decisionType = procSignSetting.getDecision_type();
	    int condition = Integer.valueOf(procSignSetting.getCondition());
	      
	    // 检查投票是否完成
	    if(SlbpmNodeSignConstants.VOTE_TYPE_PERCENT.equals(voteType)){
	        
	    	float percents = 0;
	    	
	        // 按同意票数进行决定  
	        if(SlbpmNodeSignConstants.DECISION_TYPE_PASS.equals(decisionType)){

	            percents = agreeVotesCounts/instanceOfNumbers;
	            
	            // 投票同意票符合条件  
	            if(percents >= condition){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_PASS, true);
	            }
	            // 投票已经全部完成
	            else if(completeCounter.equals(instanceOfNumbers)){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_REFUSE, true);
	            }
	        }
	        // 按反对票数进行决定  
	        else{
	        	
	            percents = refuseVotesCounts/instanceOfNumbers;
	            
	            // 投票
	            if(percents >= condition){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_REFUSE, true);
	            }
	            // 投票已经全部完成
	            else if(completeCounter.equals(instanceOfNumbers)){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_PASS, true);
	            }
	        }
	    }
	    // 按绝对票数投票
	    else{
	    	
	        // 按同意票数进行决定
	        if(SlbpmNodeSignConstants.DECISION_TYPE_PASS.equals(decisionType)){
	        	
	            // 投票同意票符合条件
	            if(agreeVotesCounts >= condition){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_PASS, true);
	            }
	            // 投票已经全部完成
	            else if(completeCounter.equals(instanceOfNumbers)){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_REFUSE, true);
	            }
	        }
	        // 按反对票数进行决定
	        else{
	        	
	            // 投票
	            if(refuseVotesCounts >= condition){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_REFUSE, true);
	            }
	            // 投票已经全部完成
	            else if(completeCounter.equals(instanceOfNumbers)){
	                voteResult = new VoteResult(SlbpmNodeSignConstants.SIGN_RESULT_PASS, true);
	            }
	        }
	    }
	    return voteResult;
	}
}