package com.singlee.slbpm.expression;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.BeanUtil;
import com.singlee.slbpm.externalInterface.RiskInterface;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import org.activiti.engine.delegate.DelegateExecution;

import java.io.Serializable;
import java.util.Map;

public class OrderManagement implements Serializable {

	private static final long serialVersionUID = -600440351074621013L;

	/**
	 * 获取交易金额
	 * 
	 * @return double
	 */
	public double getTotalAmount(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getTotalAmount(serial_no);
	}

	/**
	 * 获取泰隆交易方向
	 * 
	 * @return double
	 */
	public int getMyDir(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getMyDir(serial_no);
	}

	/**
	 * 获取泰隆债券类型
	 * 
	 * @return double
	 */
	public int getBondProperties(DelegateExecution execution) {
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getBondProperties(serial_no);
	}

	/**
	 * 根据产品
	 * 
	 * @return
	 */
	public int getProduct(DelegateExecution execution) {
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getProduct(serial_no);
	}

	/**
	 * 更加产品类型
	 * 
	 * @return
	 */
	public int getProdType(DelegateExecution execution) {
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getProdType(serial_no);
	}

	/**
	 * 根据成本中心
	 * 
	 * @return
	 */
	public int getCost(DelegateExecution execution) {
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getCost(serial_no);
	}

	/**
	 * 获取泰隆风险程度
	 * 
	 * @return double
	 */
	public int getRiskDegree(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getRiskDegree(serial_no);
	}

	/**
	 * 获取泰隆主体评级
	 * 
	 * @return double
	 */
	public int getBondRating(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getBondRating(serial_no);
	}

	/**
	 * 获取泰隆币种分支
	 * 
	 * @return double
	 */
	public int getTlCurrency(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getTlCurrency(serial_no);
	}

	/**
	 * 泰隆-交易员判断
	 * 
	 * @return double
	 */
	public int isTrader(DelegateExecution execution) {
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.isTrader(serial_no);
	}

	/**
	 * 哈尔滨-是否限额
	 * 
	 * @param execution
	 * @return
	 */
	public int isLimitAmount(DelegateExecution execution) {
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.isLimitAmount(serial_no);
	}

	/**
	 * 获取交易利率
	 * 
	 * @return double
	 */
	public double getInterestRate(DelegateExecution execution) {

		double rate;

		try {
			Map<String, Object> order = this.getOrderByExecution(execution);
			rate = Double.parseDouble(order.get("ytm").toString());
		} catch (NumberFormatException ex) {
			rate = 0.0;
		}

		return rate;
	}

	/**
	 * 计息天数计算
	 * 
	 * @return double
	 */
	public double calcDays(DelegateExecution execution) {

		double day;

		try {
			Map<String, Object> order = this.getOrderByExecution(execution);
			day = Double.parseDouble(order.get("term_days").toString());
		} catch (NumberFormatException ex) {
			day = 0.0;
		}

		return day;
	}

	/**
	 * 获取理财类型
	 * 
	 * @return 0-保本 1-保本浮动 2-非保本
	 */
	public int getFinType(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getFinType(serial_no);
	}

	/**
	 * 底层资产是否穿透
	 * 
	 * @return 1-是 0-否
	 */
	public int AssetJudgement(DelegateExecution execution) {

		// TODO - 底层资产是否穿透，业务判断代码实现
		return 0;
	}

	/**
	 * 齐商银行 - 额度授信金额
	 * 
	 * @return
	 */
	public double getCreditAmt(DelegateExecution execution) {
		// TODO
		return 0.0;
	}

	/**
	 * 齐商银行 - 放款金额
	 * 
	 * @return
	 */
	public double getLoanAmt(DelegateExecution execution) {
		// TODO
		return 0.0;
	}

	/**
	 * 上海银行 - 是否满足交易金额
	 * 
	 * @return 1-是 0-否
	 */
	public int isAmtSatisfied(DelegateExecution execution) {

		// TODO - 是否满足交易金额
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.isAmtSatisfied(serial_no);
	}

	/**
	 * 上海银行 - 是否需要放款
	 * 
	 * @return 1-是 0-否
	 */
	public int needLoan(DelegateExecution execution) {

		// TODO - 是否需要放款
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.needLoan(serial_no);
	}

	/**
	 * 上海银行 - 币种
	 * 
	 * @return CNY-人民币 OTHER-外币
	 */
	public int getCurrency(DelegateExecution execution) {

		// TODO - 币种
		String serial_no = execution.getProcessBusinessKey();
		RiskInterface ri = SpringContextHolder.getBean("FlowIndexCalcUtil");
		return ri.getCurrency(serial_no);
	}

	private Map<String, Object> getOrderByExecution(DelegateExecution execution) {

		String serial_no = execution.getProcessBusinessKey();
		TtFlowSerialMapMapper mapper = (TtFlowSerialMapMapper) SpringContextHolder.getBean(TtFlowSerialMapMapper.class);
		TtFlowSerialMap flowSerMap = mapper.get(null, serial_no, null, 0);
		SlbpmCallBackInteface callBack = (SlbpmCallBackInteface) SpringContextHolder
				.getBean(flowSerMap.getStatus_change_listener());

		Object obj = callBack.getBizObj(flowSerMap.getFlow_type(), flowSerMap.getFlow_id(), serial_no);

		return BeanUtil.beanToMap(obj);
	}
}