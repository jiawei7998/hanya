package com.singlee.slbpm.util;

/**
 * 流程变量名常量
 * @author Libo
 *
 */
public class SlbpmVariableNameConstants {
	/** 外部序号  */
	public final static String serialNo="serialNo";
	/** 回调的Ban  */
	public final static String callBackBeanName="callBackBeanName";
	/** 最后操作人 */
	public final static String lastOperator="lastOperator";
	/** 指定用户 */
	public final static String specificUser="specificUser";
	/** 角色 */
	public final static String roles="roles";
	/** 决策类型 */
	public final static String voteType="vote";
	/** 审批步骤的路径 */
	public final static String approveStepPath="approveStepPath";
	/** 子流程信息 */
	public final static String subTaskInfos="subTaskInfos";
	/** 通过数量 */
	public final static String passCount="passCount";
	/** 不通过数量 */
	public final static String nopassCount="nopassCount";
	/** 继续的数量 */
	public final static String continueCount="continueCount";
	/** 所有数量 */
	public final static String totalCount="totalCount";
	/** 节点名称*/
	public final static String nodeName="nodeName";
	/** 流程发起人 */
	public final static String Initiator="Initiator";
	/** 额度自动占用成功标志 */
	public final static String CreditUsed="CreditUsed";
	/** 额度自动释放成功标志 */
	public final static String CreditReleased="CreditReleased";
	/** 准入资格判断标志 */
	public final static String Qualification="Qualification";
	/** 会签自定义用户 */
	public final static String CountersignUser="CountersignUser";

	/** 大连银行需求 - 合同审查岗 - 修改意见（无意见/可供参考/应当修改） */
	public final static String ContractReview="ContractReview";
	/** 大连银行需求 - 已按法审意见修改 */
	public final static String ContinueAsPerAdvice="ContinueAsPerAdvice";
	/** 大连银行需求 - 是否上会 */
	public final static String VoteJudgement="VoteJudgement";
}
