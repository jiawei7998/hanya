package com.singlee.slbpm.util;

public class SlbpmNodeSignConstants {

	/** 投票类型 0-绝对票数 1-百分比  */
	public final static String VOTE_TYPE_VOTES = "0";
	public final static String VOTE_TYPE_PERCENT = "1";

	/** 决策方式 0-拒绝 1-通过 */
	public final static String DECISION_TYPE_REFUSE = "0";
	public final static String DECISION_TYPE_PASS = "1";

	/** 会签结果 0-不通过 1-通过 */
	public final static String SIGN_RESULT_REFUSE = "0";
	public final static String SIGN_RESULT_PASS = "1";
	
}
