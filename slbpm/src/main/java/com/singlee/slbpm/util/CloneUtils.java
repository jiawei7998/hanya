package com.singlee.slbpm.util;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;

/**
 * 实现对象的克隆功能
 *
 */
public abstract class CloneUtils
{
	public static void copyFields(Object source, Object target, String... fieldNames)
	{
		for (String fieldName : fieldNames)
		{
			try
			{
				Field field = FieldUtils.getField(source.getClass(), fieldName, true);
				field.setAccessible(true);
				field.set(target, field.get(source));
			}
			catch (Exception e)
			{
				Logger.getLogger(CloneUtils.class).warn(e.getMessage());
			}
		}
	}
}
