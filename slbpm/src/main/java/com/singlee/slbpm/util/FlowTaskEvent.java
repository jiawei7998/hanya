package com.singlee.slbpm.util;

public class FlowTaskEvent {

	/** 额度预检查 */
	public final static String QUOTA_PRE_CHECK = "1";
	/** 额度预占用 */
	public final static String QUOTA_PRE_OCCUPY = "2";
	/** 额度实际占用 */
	public final static String QUOTA_OCCUPY = "3";
	/** 大额划款 */
	public final static String TRANSFER = "4";
	/** 预记账 */
	public final static String PRE_ACCOUNTING = "5";
}
