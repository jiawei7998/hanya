package com.singlee.slbpm.util;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;

import java.util.List;

public class ProcessDefinitionUtils {

	public static ActivityImpl getActivity(ProcessEngine processEngine, String processDefId, String activityId) {
		ProcessDefinitionEntity pde = getProcessDefinition(processEngine, processDefId);
		return (ActivityImpl) pde.findActivity(activityId);
	}
	
	public static List<ActivityImpl> getActivities(ProcessEngine processEngine, String processDefId) {
		ProcessDefinitionEntity pde = getProcessDefinition(processEngine, processDefId);
		return pde.getActivities();
	}

	public static ProcessDefinitionEntity getProcessDefinition(ProcessEngine processEngine, String processDefId) {
		return (ProcessDefinitionEntity) ((RepositoryServiceImpl) processEngine.getRepositoryService()).getDeployedProcessDefinition(processDefId);
	}

	public static TaskEntity getCurrentTask(ProcessEngine processEngine, String procInsId) {
		return (TaskEntity) processEngine.getTaskService().createTaskQuery().processInstanceId(procInsId).active().singleResult();
	}

	public static TaskEntity getTaskEntity(ProcessEngine processEngine, String taskId) {
		return (TaskEntity) processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
	}
	
	public static ExecutionEntity getExecutionEntity(ProcessEngine processEngine, String executionId) {
		return (ExecutionEntity) processEngine.getRuntimeService().createExecutionQuery().executionId(executionId).singleResult();
	}
}