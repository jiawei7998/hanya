package com.singlee.slbpm.listener;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.slbpm.dict.DictConstants;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.delegate.event.impl.ActivitiEntityEventImpl;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.ProcessDefinitionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 任务创建事件实现类
 * @author Yang Yang 2017/4/21
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaskCreatedListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger("SLBPM");
	
	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;

    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {

            case TASK_CREATED:
            	
                ActivitiEntityEventImpl eventImpl = (ActivitiEntityEventImpl)event;
                TaskEntity taskEntity = (TaskEntity)eventImpl.getEntity();
                
                logger.debug("task is " + taskEntity.getName() + " key is:" + taskEntity.getTaskDefinitionKey());
                logger.debug("进入任务创建事件监听器 ---->" + event.getType().name());
                
                // 审批单状态设置成待审批
                this.updateFlowStatus(taskEntity);
                
                break;

            default:
                logger.debug("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
    
    /**
     * 如果流程状态是未完成审批，则把状态设置成待审批
     * @param taskEntity	任务实体
     */
    private void updateFlowStatus(TaskEntity taskEntity) {
        
        // 根据外部流水号查询流程实例相关信息
        String serial_no = taskEntity.getProcessInstance().getBusinessKey();
        
        TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, 0);
        
        // 获取回调接口
        SlbpmCallBackInteface callBack = (SlbpmCallBackInteface)SpringContextHolder
        								 .getBean(flowSerMap.getStatus_change_listener());
		
//		// 如果流程状态是未完成审批，则把状态设置成待审批
//		if(DictConstants.YesNo.NO.equals(flowSerMap.getApproved())){
//
//			callBack.statusChange(flowSerMap.getFlow_type(),
//								  flowSerMap.getFlow_id(),
//								  flowSerMap.getSerial_no(),
//								  DictConstants.ApproveStatus.WaitApprove, "");
//		}
		// 如果流程状态是审批中，说明是退回或驳回到发起岗，则状态重新设置成待审批
		if (DictConstants.YesNo.YES.equals(flowSerMap.getApproved())) {
			
			// 获取第一个审批节点
	        ProcessDefinitionImpl pdi = taskEntity.getProcessInstance().getProcessDefinition();
	        ActivityImpl act = (ActivityImpl)pdi.getInitial().getOutgoingTransitions().get(0).getDestination();
	        // 判断当前是否在第一个审批节点上
	        if (taskEntity.getTaskDefinitionKey().equals(act.getId())) {
	        	
				callBack.statusChange(flowSerMap.getFlow_type(), 
			  			  flowSerMap.getFlow_id(), 
			  			  flowSerMap.getSerial_no(), 
			  			  DictConstants.ApproveStatus.New, 
			  			  DictConstants.FlowCompleteType.Redo);
	        }
		}
    }
}