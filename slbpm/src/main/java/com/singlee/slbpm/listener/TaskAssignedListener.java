package com.singlee.slbpm.listener;

import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.delegate.event.impl.ActivitiEntityEventImpl;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 任务指派事件实现类
 * @author Yang Yang 2017/4/21
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class TaskAssignedListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger("SLBPM");

    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {

            case TASK_ASSIGNED:
                ActivitiEntityEventImpl eventImpl=(ActivitiEntityEventImpl)event;
                TaskEntity taskEntity=(TaskEntity)eventImpl.getEntity();
                logger.debug("task is "+taskEntity.getName()+" key is:"+taskEntity.getTaskDefinitionKey());
                logger.debug("进入任务委派事件监听器 ---->" + event.getType().name());
                break;

            default:
                logger.info("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
