package com.singlee.slbpm.listener;

import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 杨阳 on 2017/5/8.
 * 流程取消事件实现类
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProcessCancelledListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger("SLBPM");
	
    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {

            case PROCESS_CANCELLED:
            	logger.debug("流程取消 ---->" + event.getType().name());
                break;

            default:
                logger.debug("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}