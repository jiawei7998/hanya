package com.singlee.slbpm.listener;

import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProcessStartedListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger("SLBPM");
    
	/** 流程日志接口 */
//	@Autowired
//	private FlowLogService flowLogService;

    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {

            case PROCESS_STARTED:
                
            	logger.debug("流程实例启动 ---->" + event.getType().name());
            	
                break;

            default:
                logger.debug("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
