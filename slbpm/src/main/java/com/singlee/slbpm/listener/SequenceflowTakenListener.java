package com.singlee.slbpm.listener;

import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SequenceflowTakenListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger("SLBPM");

    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {

            case SEQUENCEFLOW_TAKEN:
            	logger.debug("进入路线 ---->" + event.getType().name());            	
                break;

            default:
                logger.debug("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return false;
    }
}
