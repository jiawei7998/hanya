package com.singlee.slbpm.ctrl.impl;

import com.singlee.slbpm.cfg.SqlMapperBasedServiceBase;
import com.singlee.slbpm.ctrl.RuntimeActivityDefinitionEntity;
import com.singlee.slbpm.ctrl.RuntimeActivityDefinitionManager;
import com.singlee.slbpm.ctrl.mapper.SqlRuntimeActivityDefinitionManagerMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(value="transactionManager",rollbackFor=Exception.class,readOnly = true)
public class RuntimeActivityDefinitionManagerImpl extends
		SqlMapperBasedServiceBase<SqlRuntimeActivityDefinitionManagerMapper> implements
		RuntimeActivityDefinitionManager
{
	@Override
	public List<RuntimeActivityDefinitionEntity> list()
	{
		List<RuntimeActivityDefinitionEntity> list = new ArrayList<RuntimeActivityDefinitionEntity>();
		list.addAll(_mapper.findAll());
		return list;
	}

	@Transactional(value="transactionManager",rollbackFor=Exception.class,readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void removeAll()
	{
		_mapper.deleteAll();
	}

	@Transactional(value="transactionManager",rollbackFor=Exception.class,readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public void save(RuntimeActivityDefinitionEntity entity)
	{
		_mapper.save(entity);
	}
}
