package com.singlee.slbpm.ctrl.impl;

import com.singlee.slbpm.ctrl.RuntimeActivityCtrlService;
import com.singlee.slbpm.ctrl.RuntimeActivityDefinitionManager;
import com.singlee.slbpm.ctrl.cmd.CreateAndTakeTransitionCmd;
import com.singlee.slbpm.ctrl.cmd.DeleteRunningTaskCmd;
import com.singlee.slbpm.ctrl.cmd.StartActivityCmd;
import com.singlee.slbpm.ctrl.creator.ChainedActivitiesCreator;
import com.singlee.slbpm.ctrl.creator.MultiInstanceActivityCreator;
import com.singlee.slbpm.ctrl.creator.RuntimeActivityDefinitionEntityInterpreter;
import com.singlee.slbpm.ctrl.entity.RuntimeActivityDefinitionEntityImpl;
import com.singlee.slbpm.util.ProcessDefinitionUtils;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.RuntimeServiceImpl;
import org.activiti.engine.impl.identity.Authentication;
import org.activiti.engine.impl.interceptor.Command;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class RuntimeActivityCtrlServiceImpl implements RuntimeActivityCtrlService
{
	RuntimeActivityDefinitionManager _activitiesCreationStore;

	ProcessDefinitionEntity _processDefinition;

	ProcessEngine _processEngine;

	private String _processInstanceId;

	public RuntimeActivityCtrlServiceImpl(RuntimeActivityDefinitionManager activitiesCreationStore,
			ProcessEngine processEngine, String processId)
	{
		_activitiesCreationStore = activitiesCreationStore;
		_processEngine = processEngine;
		_processInstanceId = processId;

		String processDefId = _processEngine.getRuntimeService().createProcessInstanceQuery()
				.processInstanceId(_processInstanceId).singleResult().getProcessDefinitionId();

		_processDefinition = ProcessDefinitionUtils.getProcessDefinition(_processEngine, processDefId);
	}

	private void executeCommand(Command<java.lang.Void> command)
	{
		((RuntimeServiceImpl) _processEngine.getRuntimeService()).getCommandExecutor().execute(command);
	}

	private TaskEntity getCurrentTask()
	{
		return (TaskEntity) _processEngine.getTaskService().createTaskQuery().processInstanceId(_processInstanceId)
				.active().singleResult();
	}

	private TaskEntity getTaskById(String taskId)
	{
		return (TaskEntity) _processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
	}

	/**
	 * 后加签
	 */
	@Override
	public ActivityImpl[] insertTasksAfter(String targetTaskDefinitionKey, String... assignees) throws Exception
	{
		List<String> assigneeList = new ArrayList<String>();
		assigneeList.add(Authentication.getAuthenticatedUserId());
		assigneeList.addAll(CollectionUtils.arrayToList(assignees));
		String[] newAssignees = assigneeList.toArray(new String[0]);

		ActivityImpl prototypeActivity = ProcessDefinitionUtils.getActivity(_processEngine, _processDefinition.getId(),
			targetTaskDefinitionKey);

		return cloneAndMakeChain(targetTaskDefinitionKey, prototypeActivity.getOutgoingTransitions().get(0)
				.getDestination().getId(), newAssignees);
	}

	/**
	 * 前加签
	 */
	@Override
	public ActivityImpl[] insertTasksBefore(String targetTaskDefinitionKey, String... assignees) throws Exception
	{
		return cloneAndMakeChain(targetTaskDefinitionKey, targetTaskDefinitionKey, assignees);
	}

	private ActivityImpl[] cloneAndMakeChain(String prototypeActivityId, String nextActivityId, String... assignees)
			throws Exception
	{
		RuntimeActivityDefinitionEntityImpl info = new RuntimeActivityDefinitionEntityImpl();
		info.setProcessDefinitionId(_processDefinition.getId());
		info.setProcessInstanceId(_processInstanceId);

		RuntimeActivityDefinitionEntityInterpreter radei = new RuntimeActivityDefinitionEntityInterpreter(info);
		radei.setPrototypeActivityId(prototypeActivityId);
		radei.setAssignees(CollectionUtils.arrayToList(assignees));
		radei.setNextActivityId(nextActivityId);

		ActivityImpl[] activities = new ChainedActivitiesCreator().createActivities(_processEngine, _processDefinition,
			info);

		TaskEntity org_entity = (TaskEntity) _processEngine.getTaskService()
														   .createTaskQuery()
				                                           .processInstanceId(_processInstanceId)
														   .taskDefinitionKey(prototypeActivityId)
														   .singleResult();

		moveTo(org_entity, activities[0].getId());
		recordActivitiesCreation(info);

		return activities;
	}
	
	/**
	 * 后退一步
	 */
	@Override
	public void moveBack() throws Exception
	{
		moveBack(getCurrentTask());
	}
	
	/**
	 * 后退至指定活动
	 */
	@Override
	public void moveBack(TaskEntity currentTaskEntity) throws Exception
	{
		String processDefId = currentTaskEntity.getProcessDefinitionId();
		String activityId = currentTaskEntity.getTaskDefinitionKey();
		ActivityImpl activity = seekPreviousNode(processDefId, activityId);

		// ATT - 如果上一个节点是决策节点，那么继续往前退回到角色节点之前
		if ("exclusiveGateway".equalsIgnoreCase(String.valueOf(activity.getProperty("type")))) {
			activity = seekPreviousNode(processDefId, activity.getId());
		}

		moveTo(currentTaskEntity, activity);
	}

	/**
	 * 找到当前节点的前一个物理节点
	 * @param processDefId		流程定义Id
	 * @param activityId 		当前节点Guid
	 * @return 前一个物理节点
	 * @throws Exception
	 */
	private ActivityImpl seekPreviousNode(String processDefId, String activityId) throws Exception
	{
		return (ActivityImpl) ProcessDefinitionUtils.getActivity(_processEngine, processDefId, activityId)
				.getIncomingTransitions().get(0).getSource();
	}

	/**
	 * 前进一步
	 */
	@Override
	public void moveForward() throws Exception
	{
		moveForward(getCurrentTask());
	}
	
	/**
	 * 前进至指定活动
	 */
	@Override
	public void moveForward(TaskEntity currentTaskEntity) throws Exception
	{
		String processDefId = currentTaskEntity.getProcessDefinitionId();
		String activityId = currentTaskEntity.getTaskDefinitionKey();
		ActivityImpl activity = seekNextNode(processDefId, activityId);
		moveTo(currentTaskEntity, activity);
	}

	/**
	 * 找到当前节点的后一个物理节点
	 * @param processDefId		流程定义Id
	 * @param activityId 		当前节点Guid
	 * @return 后一个物理节点
	 * @throws Exception
	 */
	private ActivityImpl seekNextNode(String processDefId, String activityId) throws Exception
	{
		return (ActivityImpl)ProcessDefinitionUtils.getActivity(_processEngine, processDefId, activityId)
				 								   .getOutgoingTransitions().get(0).getDestination();
	}

	/**
	 * 跳转（包括回退和向前）至指定活动节点
	 */
	@Override
	public void moveTo(String targetTaskDefinitionKey) throws Exception
	{
		moveTo(getCurrentTask(), targetTaskDefinitionKey);
	}
	
	/**
	 * 跳转（包括回退和向前）至指定活动节点
	 */
	@Override
	public void moveTo(String currentTaskId, String targetTaskDefinitionKey) throws Exception
	{
		moveTo(getTaskById(currentTaskId), targetTaskDefinitionKey);
	}

	private void moveTo(TaskEntity currentTaskEntity, ActivityImpl activity)
	{
		executeCommand(new StartActivityCmd(currentTaskEntity.getExecutionId(), activity));
		executeCommand(new DeleteRunningTaskCmd(currentTaskEntity));
	}

	/**
	 * 跳转（包括回退和向前）至指定活动节点
	 * 
	 * @param currentTaskEntity
	 *            当前任务节点
	 * @param targetTaskDefinitionKey
	 *            目标任务节点（在模型定义里面的节点名称）
	 * @throws Exception
	 */
	@Override
	public void moveTo(TaskEntity currentTaskEntity, String targetTaskDefinitionKey) throws Exception
	{
		ActivityImpl activity = ProcessDefinitionUtils.getActivity(_processEngine,
			currentTaskEntity.getProcessDefinitionId(), targetTaskDefinitionKey);

		moveTo(currentTaskEntity, activity);
	}

	/**
	 * 分裂某节点为多实例节点
	 * 
	 * @param targetTaskDefinitionKey
	 * @param isSequential
	 * @param assignees
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	@Override
	public ActivityImpl split(String targetTaskDefinitionKey, boolean isSequential, String... assignees)
			throws Exception
	{
		RuntimeActivityDefinitionEntityImpl info = new RuntimeActivityDefinitionEntityImpl();
		info.setProcessDefinitionId(_processDefinition.getId());
		info.setProcessInstanceId(_processInstanceId);

		RuntimeActivityDefinitionEntityInterpreter radei = new RuntimeActivityDefinitionEntityInterpreter(info);

		radei.setPrototypeActivityId(targetTaskDefinitionKey);
		radei.setAssignees(CollectionUtils.arrayToList(assignees));
		radei.setSequential(isSequential);

		ActivityImpl clone = new MultiInstanceActivityCreator().createActivities(_processEngine, _processDefinition,
			info)[0];

		TaskEntity currentTaskEntity = getCurrentTask();
		executeCommand(new CreateAndTakeTransitionCmd(currentTaskEntity.getExecutionId(), clone));
		executeCommand(new DeleteRunningTaskCmd(currentTaskEntity));

		recordActivitiesCreation(info);
		return clone;
	}
	
	/**
	 * 分裂某节点为多实例节点
	 */
	@Override
	public ActivityImpl split(String targetTaskDefinitionKey, String... assignee) throws Exception
	{
		return split(targetTaskDefinitionKey, true, assignee);
	}

	private void recordActivitiesCreation(RuntimeActivityDefinitionEntityImpl info) throws Exception
	{
		info.serializeProperties();
		_activitiesCreationStore.save(info);
	}
}
