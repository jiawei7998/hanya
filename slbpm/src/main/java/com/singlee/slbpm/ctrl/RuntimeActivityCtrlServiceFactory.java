package com.singlee.slbpm.ctrl;

public interface RuntimeActivityCtrlServiceFactory
{
	RuntimeActivityCtrlService create(String processId);
}