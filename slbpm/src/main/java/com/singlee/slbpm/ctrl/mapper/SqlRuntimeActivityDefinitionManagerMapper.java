package com.singlee.slbpm.ctrl.mapper;

import com.singlee.slbpm.ctrl.RuntimeActivityDefinitionEntity;
import com.singlee.slbpm.ctrl.entity.RuntimeActivityDefinitionEntityImpl;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface SqlRuntimeActivityDefinitionManagerMapper
{
	@Delete("DELETE FROM TT_PROC_ACTIVITY_CREATION")
	void deleteAll();

	@Select("SELECT * FROM TT_PROC_ACTIVITY_CREATION")
	@Results(value = { @Result(property = "factoryName", column = "FACTORY_NAME"),
			@Result(property = "processDefinitionId", column = "PROCESS_DEFINITION_ID"),
			@Result(property = "processInstanceId", column = "PROCESS_INSTANCE_ID"),
			@Result(property = "propertiesText", column = "PROPERTIES_TEXT") })
	List<RuntimeActivityDefinitionEntityImpl> findAll();

	@Insert("INSERT INTO TT_PROC_ACTIVITY_CREATION (FACTORY_NAME,PROCESS_DEFINITION_ID,PROCESS_INSTANCE_ID,PROPERTIES_TEXT) values (#{factoryName},#{processDefinitionId},#{processInstanceId},#{propertiesText})")
	void save(RuntimeActivityDefinitionEntity entity);
}
