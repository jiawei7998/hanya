package com.singlee.slbpm.ctrl.impl;

import com.singlee.slbpm.ctrl.RuntimeActivityCtrlService;
import com.singlee.slbpm.ctrl.RuntimeActivityCtrlServiceFactory;
import com.singlee.slbpm.ctrl.RuntimeActivityDefinitionManager;
import org.activiti.engine.ProcessEngine;
import org.springframework.beans.factory.annotation.Autowired;

public class RuntimeActivityCtrlServiceFactoryImpl implements RuntimeActivityCtrlServiceFactory
{
	@Autowired
	RuntimeActivityDefinitionManager _activitiesCreationStore;

	@Autowired
	ProcessEngine _processEngine;

	@Override
	public RuntimeActivityCtrlService create(String processId)
	{
		return new RuntimeActivityCtrlServiceImpl(_activitiesCreationStore, _processEngine, processId);
	}
}
