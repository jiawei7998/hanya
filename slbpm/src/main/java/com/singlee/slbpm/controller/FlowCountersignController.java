package com.singlee.slbpm.controller;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.slbpm.service.FlowCountersignService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping(value = "/FlowCountersignController", method = RequestMethod.POST)
@SuppressWarnings("unused")
public class FlowCountersignController {
	
	private static Logger logger = LoggerFactory.getLogger("SLBPM");
	
	@Autowired
    private FlowCountersignService flowCountersignService;
	
    /**
     * 获取会签投票信息
     */
    @RequestMapping(value = "/getCountersignDetailList")
    @ResponseBody
    public Map<String, Object> getCountersignDetailList(@RequestBody Map<String, Object> paramMap) {

    	String serial_no = ParameterUtil.getString(paramMap, "serial_no", "");
    	Map<String, Object> resultMap = flowCountersignService.getCountersignDetailList(serial_no);
        return resultMap;
    }
}
