package com.singlee.slbpm.controller;

import com.alibaba.fastjson.JSONObject;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.slbpm.pojo.vo.ProcDefInfoVo;
import com.singlee.slbpm.service.FlowDefService;
import org.activiti.bpmn.exceptions.XMLException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/FlowModelController", method = RequestMethod.POST)
public class FlowModelController {
	
	private static Logger logger = LoggerFactory.getLogger("SLBPM");
	
	/** 注入activiti自带的RepositoryService接口 */
	@Autowired
    private RepositoryService repositoryService;

	/** 审批流程定义接口 */
	@Autowired
	private FlowDefService flowDefService;

    /**
     * 获取模型列表
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/getModelList")
    @ResponseBody
    public JSONObject getModelList(@RequestBody Map<String, Object> paramMap) {
        JSONObject result = new JSONObject();
        String moduleName = ParameterUtil.getString(paramMap,"moduleName","%%");
        try {
            List<Model> list = repositoryService.createModelQuery().modelNameLike(moduleName).list();
            result.put("modelList", list);
            result.put("message", "获取模型列表成功");
            
        } catch (Exception e) {
            logger.error("获取模型列表失败：", e);
            result.put("message", "获取模型列表失败");
        }

        return result;
    }

    /**
     * 删除模型
     * @param request
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/deleteModel")
    @ResponseBody
    public JSONObject deleteModel(@RequestBody Map<String, Object> paramMap) {
        
    	JSONObject result = new JSONObject();
    	
        try {
        	String modelId = ParameterUtil.getString(paramMap, "modelId", "");
            repositoryService.deleteModel(modelId);
            result.put("message", "删除成功，模型ID=" + modelId);
        } 
        catch (Exception e) {
            logger.error("删除模型失败：", e);
            result.put("message", "删除模型失败");
        }
        
        return result;
    }
	
    /**
     * 新建流程定义（模型部署）
     */
    @RequestMapping(value = "/deployModel")
    @ResponseBody
    public JSONObject deployModel(@RequestBody Map<String, Object> paramMap) {

    	String model_id = ParameterUtil.getString(paramMap, "model_id", "");
        String proc_def_info = ParameterUtil.getString(paramMap, "proc_def_info", "");
        boolean override = ParameterUtil.getBoolean(paramMap, "override", false);

        ProcDefInfoVo procDefInfoVo = JSONObject.parseObject(proc_def_info, ProcDefInfoVo.class);

        JSONObject obj = new JSONObject();

        try {
            obj = flowDefService.addNewProcDef(model_id, procDefInfoVo, override);
        }
        catch (XMLException xe) {
            obj.put("message", "模型不符合设计规则 - "+ xe.getMessage());
        }
        catch (Exception e) {
            obj.put("message", "模型部署失败 - " + e.getMessage());
        }

        return obj;
    }
}
