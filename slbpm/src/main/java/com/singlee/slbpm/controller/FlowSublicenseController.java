package com.singlee.slbpm.controller;

import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.pojo.page.PageInfo;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.pojo.bo.ProcAuthority;
import com.singlee.slbpm.service.FlowSublicenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/**
 * Created by Wangfp on 2018/2/27.
 */
@Controller
@RequestMapping(value = "/FlowSublicenseController", method = RequestMethod.POST)
public class FlowSublicenseController {

    @SuppressWarnings("unused")
	private static Logger logger = LoggerFactory.getLogger("SLBPM");

    @Autowired
    private FlowSublicenseService flowSublicenseService;

    /**
     * 添加用户转授权信息
     */
    @RequestMapping(value = "/addUserSublicenseDetail")
    @ResponseBody
    public RetMsg<Serializable> addUserSublicenseDetail(@RequestBody Map<String, Object> paramMap) throws Exception  {
        flowSublicenseService.addUserSublicenseDetail(paramMap);
        return RetMsgHelper.ok();
    }

    /**
     * 获取用户转授权信息列表
     */
    @RequestMapping(value = "/getUserSublicenseListMine")
    @ResponseBody
    public RetMsg<PageInfo<ProcAuthority>> getUserSublicenseListMine(@RequestBody Map<String, Object> paramMap) {
    	paramMap.put("to_user_id", SlSessionHelper.getUserId());
    	paramMap.put("inst_id", SlSessionHelper.getInstitutionId());
    	paramMap.put("branch_id", SlSessionHelper.getInstitution().getBranchId());
        return RetMsgHelper.ok(flowSublicenseService.getUserSublicenseList(paramMap));
    }
    
    @RequestMapping(value = "/getUserSublicenseListApprove")
    @ResponseBody
    public RetMsg<PageInfo<ProcAuthority>> getUserSublicenseListApprove(@RequestBody Map<String, Object> paramMap) {
    	paramMap.put("from_user_id", SlSessionHelper.getUserId());
    	paramMap.put("auth_status", "4");
    	paramMap.put("inst_id", SlSessionHelper.getInstitutionId());
    	paramMap.put("branch_id", SlSessionHelper.getInstitution().getBranchId());
        return RetMsgHelper.ok(flowSublicenseService.getUserSublicenseList(paramMap));
    }

    /**
     * 通过id获取单个用户转授权信息
     */
    @RequestMapping(value = "/getUserSublicenseById")
    @ResponseBody
    public Map<String, Object> getUserSublicenseById(@RequestBody Map<String, Object> paramMap) {
        return flowSublicenseService.getUserSublicenseById(paramMap);
    }

    /**
     * 修改用户转授权信息
     */
    @RequestMapping(value = "/updateUserSublicenseDetail")
    @ResponseBody
    public RetMsg<Serializable> updateUserSublicenseDetail(@RequestBody Map<String, Object> paramMap) {
    	try{
	        Map<String, Object> resultMap = new HashMap<String, Object>();
	        resultMap.put("id",ParameterUtil.getString(paramMap, "id", ""));
	        resultMap.put("flow_id",ParameterUtil.getString(paramMap, "flow_id", ""));
	        resultMap.put("task_def_key",ParameterUtil.getString(paramMap, "task_def_key", ""));
	        resultMap.put("from_user_id",ParameterUtil.getString(paramMap, "from_user_id", ""));
	        resultMap.put("to_user_id",ParameterUtil.getString(paramMap, "to_user_id", ""));
	        resultMap.put("auth_start_date",ParameterUtil.getString(paramMap, "auth_start_date", ""));
	        resultMap.put("auth_end_date",ParameterUtil.getString(paramMap, "auth_end_date", ""));
	        resultMap.put("automatic_renewal",ParameterUtil.getString(paramMap, "automatic_renewal", ""));
	        resultMap.put("is_active",ParameterUtil.getString(paramMap, "is_active", ""));
	        resultMap.put("authority_rule",ParameterUtil.getString(paramMap, "authority_rule", ""));
	        flowSublicenseService.updateUserSublicenseDetail(resultMap);
	        return RetMsgHelper.ok();
		}catch(Exception e){
			return RetMsgHelper.ok(e.getMessage());
		}
    }

    /** 
     * 通过id删除单个用户转授权信息
     */
    @RequestMapping(value = "/deleteUserSublicenseById")
    @ResponseBody
    public Map<String, Object> deleteUserSublicenseById(@RequestBody Map<String, Object> paramMap) {
        return flowSublicenseService.deleteUserSublicenseById(paramMap);
    }
    
    /**
     * 提交/审批
     */
    @RequestMapping(value = "/approve")
    @ResponseBody
    public RetMsg<?> approve(@RequestBody Map<String,Object> param) throws Exception{
    	int status = ParameterUtil.getInt(param, "status", -1);
    	JY.require(status > 0, "状态有误，请确认。");
    	String id = ParameterUtil.getString(param, "id", "");
    	flowSublicenseService.approve(id, status);
        return RetMsgHelper.ok();
    }
    
    /** 
     * 将key发送到jsp页面
     * @throws Exception 
     */
    @RequestMapping(value = "/sendKeyToJsp")
    @ResponseBody
    public RetMsg<?> sendKeyToJsp(@RequestBody Map<String, Object> paramMap) throws Exception {
    	String id = ParameterUtil.getString(paramMap, "id", "");
    	if(StringUtil.isNullOrEmpty(id)){
    		return RetMsgHelper.ok("");
    	}
        return RetMsgHelper.ok(flowSublicenseService.sendKeyToJsp(id));
    }
    
    /** 
     * 是否需要授权校验
     * @throws Exception 
     */
    @RequestMapping(value = "/isNeedCheck")
    @ResponseBody
    public RetMsg<Serializable> isNeedCheck(@RequestBody Map<String, Object> paramMap) throws Exception {
    	String task_id = ParameterUtil.getString(paramMap, "task_id", "");
    	if(StringUtil.isNullOrEmpty(task_id)){
    		return RetMsgHelper.ok("");
    	}
        return RetMsgHelper.ok(flowSublicenseService.isNeedCheck(task_id));
    }
    
    /** 
     * 进行校验
     * @throws Exception 
     */
    @RequestMapping(value = "/check")
    @ResponseBody
    public RetMsg<?> check(@RequestBody Map<String, Object> paramMap) throws Exception {
    	String id = ParameterUtil.getString(paramMap, "ids", "");
    	String auth_key = ParameterUtil.getString(paramMap, "auth_key", "");
        return RetMsgHelper.ok(flowSublicenseService.check(id, auth_key));
    }
}
