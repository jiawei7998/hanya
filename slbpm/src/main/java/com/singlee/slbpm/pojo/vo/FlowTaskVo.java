package com.singlee.slbpm.pojo.vo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

/**
 * 流程任务vo类
 * @author Libo
 *
 */
@NameStyle(Style.lowercase)
public class FlowTaskVo{
	private String serial_no; //流水号
	private String task_id;
	private String task_name;
	private String user_id;
	private String super_task;
	private String execution;
	private String execution_name;
	
	public String getSerial_no() {
		return serial_no;
	}
	public void setSerial_no(String serial_no) {
		this.serial_no = serial_no;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getTask_name() {
		return task_name;
	}
	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getSuper_task() {
		return super_task;
	}
	public void setSuper_task(String super_task) {
		this.super_task = super_task;
	}
	public String getExecution() {
		return execution;
	}
	public void setExecution(String execution) {
		this.execution = execution;
	}
	public String getExecution_name() {
		return execution_name;
	}
	public void setExecution_name(String execution_name) {
		this.execution_name = execution_name;
	}
	
}
