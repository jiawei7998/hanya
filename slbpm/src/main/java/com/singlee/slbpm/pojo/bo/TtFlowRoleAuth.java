package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * 审批角色 功能授权表
 * 
 * @author Yang Yang
 * 
 */
@Entity
@Table(name = "TT_FLOW_ROLE_AUTH")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtFlowRoleAuth{

	@Id
	private String flow_role_id;		// 审批角色ID

	private String flow_operation_id;	// 审批操作类型ID


	public String getFlow_role_id() {
		return flow_role_id;
	}

	public void setFlow_role_id(String flow_role_id) {
		this.flow_role_id = flow_role_id;
	}

	public String getFlow_operation_id() {
		return flow_operation_id;
	}

	public void setFlow_operation_id(String flow_operation_id) {
		this.flow_operation_id = flow_operation_id;
	}
}
