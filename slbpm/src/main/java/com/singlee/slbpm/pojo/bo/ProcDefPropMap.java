package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_PROC_DEF_PROP_MAP")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcDefPropMap {
	
	private String flow_id;				// 流程定义ID
	private String version;				// 版本号
	private String product_type;		// 产品类型
	private String inst_code;			// 机构代码
	
	public String getFlow_id() {
		return flow_id;
	}
	public void setFlow_id(String flow_id) {
		this.flow_id = flow_id;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getProduct_type() {
		return product_type;
	}
	public void setProduct_type(String product_type) {
		this.product_type = product_type;
	}
	public String getInst_code() {
		return inst_code;
	}
	public void setInst_code(String inst_code) {
		this.inst_code = inst_code;
	}
	
}
