package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_PROC_TASK_URL")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcTaskUrl {

    private String flow_id;	        // 流程定义ID
    private String version;         // 版本号
    private String task_def_key;    // 任务定义key
    private String prd_code;		// 产品编号
    private String urls;     		// 地址

    public String getFlow_id() {
        return flow_id;
    }    
    public void setFlow_id(String flow_id) {
        this.flow_id = flow_id;
    }

    public String getVersion() {
        return version;
    }    
    public void setVersion(String version) {
        this.version = version;
    }

    public String getTask_def_key() {
        return task_def_key;
    }    
    public void setTask_def_key(String task_def_key) {
        this.task_def_key = task_def_key;
    }

    public String getPrd_code() {
        return prd_code;
    }    
    
    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }
    
	public String getUrls() {
		return urls;
	}
	
	public void setUrls(String urls) {
		this.urls = urls;
	}
    
}
