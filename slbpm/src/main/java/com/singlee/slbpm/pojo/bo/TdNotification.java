package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TD_NOTIFICATION")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TdNotification {

    private String from_user_id;		// 消息发送人ID
    private String to_user_id;         	// 消息接收人ID
    private String deal_no;    			// 业务流水号
    private String deal_type;  			// 业务类型
    private String create_time;			// 发送时间
    private String is_read;				// 是否已读
    private String read_time;			// 打开时间
    private String text;				// 消息内容

    public String getFrom_user_id() {
        return from_user_id;
    }
    
    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }
    
    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getDeal_no() {
        return deal_no;
    }
    
    public void setDeal_no(String deal_no) {
        this.deal_no = deal_no;
    }

    public String getDeal_type() {
        return deal_type;
    }
    
    public void setDeal_type(String deal_type) {
        this.deal_type = deal_type;
    }
    
    public String getCreate_time() {
        return create_time;
    }
    
    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getIs_read() {
        return is_read;
    }
    
    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getRead_time() {
        return read_time;
    }
    
    public void setRead_time(String read_time) {
        this.read_time = read_time;
    }

    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }    
}
