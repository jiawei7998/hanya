package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 
 * 流程角色 用户 关系对象
 * 
 * @author LyonChen
 * 
 */
@Entity
@Table(name = "TT_FLOW_ROLE_USER_MAP")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtFlowRoleUserMap implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String role_id;// 角色ID
	@Id
	private String user_id;// 用户ID
	private String auth_userid;// 授权用户ID 空为没有转授权
	private String auth_begdate;// 授权开始日期
	private String auth_enddate;// 授权结束日期

	public TtFlowRoleUserMap() {
		super();
	}

	@Override
	public String toString() {
		return String.format("TtFlowRoleUserMap [role_id=%s, user_id=%s, auth_userid=%s, auth_begdate=%s, auth_enddate=%s]", role_id, user_id, auth_userid, auth_begdate, auth_enddate);
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getAuth_userid() {
		return auth_userid;
	}

	public void setAuth_userid(String auth_userid) {
		this.auth_userid = auth_userid;
	}

	public String getAuth_begdate() {
		return auth_begdate;
	}

	public void setAuth_begdate(String auth_begdate) {
		this.auth_begdate = auth_begdate;
	}

	public String getAuth_enddate() {
		return auth_enddate;
	}

	public void setAuth_enddate(String auth_enddate) {
		this.auth_enddate = auth_enddate;
	}

}
