package com.singlee.slbpm.pojo.vo;

public class VoteResult {
	
	private String signResult;
	private boolean isCompleted;

	public VoteResult(){}

	public VoteResult(String signResult, boolean isCompleted){
		this.signResult = signResult;
		this.isCompleted = isCompleted;
	}
	
    public String getSignResult() {
        return signResult;
    }
    
    public boolean getIsCompleted() {
    	return isCompleted;
    }
}
