package com.singlee.slbpm.pojo.vo;

import com.singlee.slbpm.pojo.bo.TdNotification;

public class TdNotificationVo extends TdNotification{
	
	private String from_user_name;
	private String to_user_name;
	
	
	public String getFrom_user_name() {
		return from_user_name;
	}
	public void setFrom_user_name(String from_user_name) {
		this.from_user_name = from_user_name;
	}
	public String getTo_user_name() {
		return to_user_name;
	}
	public void setTo_user_name(String to_user_name) {
		this.to_user_name = to_user_name;
	}
}
