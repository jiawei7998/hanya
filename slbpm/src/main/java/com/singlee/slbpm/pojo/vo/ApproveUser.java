package com.singlee.slbpm.pojo.vo;

/**
 * 审批员
 * @author Libo
 *
 */
public class ApproveUser {

	/**
	 * 审批用户ID（如果A转授权给了B   这里为B）
	 */
	private String userId;
	private String userName;
	/**
	 * 审批用户角色
	 */
	private String userRoleId;
	private String userRoleName;
	/**
	 * 原始用户ID
	 */
	private String originalUserId;
	private String originalUserName;
	/**
	 * 原始用户角色
	 */
	private String originalUserRoleId;
	private String originalUserRoleName;
	
	
	public ApproveUser(String userId, String userName, String userRoleId,
			String userRoleName, String originalUserId,
			String originalUserName, String originalUserRoleId,
			String originalUserRoleName) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userRoleId = userRoleId;
		this.userRoleName = userRoleName;
		this.originalUserId = originalUserId;
		this.originalUserName = originalUserName;
		this.originalUserRoleId = originalUserRoleId;
		this.originalUserRoleName = originalUserRoleName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getUserRoleName() {
		return userRoleName;
	}
	public void setUserRoleName(String userRoleName) {
		this.userRoleName = userRoleName;
	}
	public String getOriginalUserId() {
		return originalUserId;
	}
	public void setOriginalUserId(String originalUserId) {
		this.originalUserId = originalUserId;
	}
	public String getOriginalUserName() {
		return originalUserName;
	}
	public void setOriginalUserName(String originalUserName) {
		this.originalUserName = originalUserName;
	}
	public String getOriginalUserRoleId() {
		return originalUserRoleId;
	}
	public void setOriginalUserRoleId(String originalUserRoleId) {
		this.originalUserRoleId = originalUserRoleId;
	}
	public String getOriginalUserRoleName() {
		return originalUserRoleName;
	}
	public void setOriginalUserRoleName(String originalUserRoleName) {
		this.originalUserRoleName = originalUserRoleName;
	}
	
	
}
