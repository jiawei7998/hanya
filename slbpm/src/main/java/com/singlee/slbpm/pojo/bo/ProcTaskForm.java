package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_PROC_TASK_FORM")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcTaskForm {

    private String flow_id;	        // 流程定义ID
    private String version;         // 版本号
    private String task_def_key;    // 任务定义key
    private String prd_code;		// 产品编号
    private String form_no;  		// 自定义表单ID
    private String form_fields_json;// 表单业务要素自定义JSON

    public String getFlow_id() {
        return flow_id;
    }    
    public void setFlow_id(String flow_id) {
        this.flow_id = flow_id;
    }

    public String getVersion() {
        return version;
    }    
    public void setVersion(String version) {
        this.version = version;
    }

    public String getTask_def_key() {
        return task_def_key;
    }    
    public void setTask_def_key(String task_def_key) {
        this.task_def_key = task_def_key;
    }

    public String getPrd_code() {
        return prd_code;
    }    
    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getForm_no() {
        return form_no;
    }    
    public void setForm_no(String form_no) {
        this.form_no = form_no;
    }

    public String getForm_fields_json() {
        return form_fields_json;
    }    
    public void setForm_fields_json(String form_fields_json) {
        this.form_fields_json = form_fields_json;
    }
    
}
