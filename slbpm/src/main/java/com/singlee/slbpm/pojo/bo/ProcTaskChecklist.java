package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TT_PROC_TASK_CHECKLIST")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcTaskChecklist {

    private String flow_id;	        // 流程定义ID
    private String version;         // 版本号
    private String task_def_key;    // 任务定义key
    private String ck_prd;  		// 产品类型
    private String ck_type;			// 审查类型
    
    public String getFlow_id() {
        return flow_id;
    }
    
    public void setFlow_id(String flow_id) {
        this.flow_id = flow_id;
    }

    public String getVersion() {
        return version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }

    public String getTask_def_key() {
        return task_def_key;
    }
    
    public void setTask_def_key(String task_def_key) {
        this.task_def_key = task_def_key;
    }

	public String getCk_prd() {
		return ck_prd;
	}

	public void setCk_prd(String ck_prd) {
		this.ck_prd = ck_prd;
	}

	public String getCk_type() {
		return ck_type;
	}

	public void setCk_type(String ck_type) {
		this.ck_type = ck_type;
	}
}
