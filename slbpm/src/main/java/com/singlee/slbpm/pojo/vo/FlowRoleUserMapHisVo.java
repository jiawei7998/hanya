package com.singlee.slbpm.pojo.vo;

import com.singlee.slbpm.pojo.bo.TtFlowRoleUserMapHis;

/**
 * 
 * 流程角色 用户 关系对象
 * 
 * @author LyonChen
 * 
 */
public class FlowRoleUserMapHisVo extends TtFlowRoleUserMapHis{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String role_name;// 角色名称
	
	private String user_name;// 柜员名称

	private String auth_username;// 被授权人名称
	

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAuth_username() {
		return auth_username;
	}

	public void setAuth_username(String auth_username) {
		this.auth_username = auth_username;
	}

}
