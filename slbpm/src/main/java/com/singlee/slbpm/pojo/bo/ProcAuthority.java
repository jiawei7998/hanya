package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Created by wangfp on 2018/2/27.
 * 转授权实体类
 */
@Entity
@Table(name = "TT_PROC_AUTHORITY")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcAuthority {

    @Id
    private String id;	        // 流程定义ID
    private String flow_id;	        // 流程定义ID
    private String task_def_key;       // 流程节点key
    private String from_user_id;       // 审批权限转授人ID
    private String to_user_id;         // 审批权限接受人ID
    private String auth_start_date;       // 规则生效日期
    private String auth_end_date;         // 规则失效日期
    private int is_active;                  // 规则是否生效(0-否 1-是)
    private String authority_rule;          // 授权限制规则(空则表示无限制)
    private int auth_status;                
    private String auth_key;          

    @Transient
    private String from_user_name;//审批权限转授人名字
    @Transient
    private String to_user_name;//审批权限接受人名字
    @Transient
    private String from_user_inst;//审批权限转授人所属机构
    @Transient
    private String to_user_inst;//审批权限接受人所属机构
    @Transient
    private String flow_name;   //流程定义名称
    @Transient
    private String task_def_name;   //流程节点名称

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlow_id() {
        return flow_id;
    }

    public void setFlow_id(String flow_id) {
        this.flow_id = flow_id;
    }

    public String getTask_def_key() {
        return task_def_key;
    }

    public void setTask_def_key(String task_def_key) {
        this.task_def_key = task_def_key;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getAuth_start_date() {
        return auth_start_date;
    }

    public void setAuth_start_date(String auth_start_date) {
        this.auth_start_date = auth_start_date;
    }

    public String getAuth_end_date() {
        return auth_end_date;
    }

    public void setAuth_end_date(String auth_end_date) {
        this.auth_end_date = auth_end_date;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public String getAuthority_rule() {
        return authority_rule;
    }

    public void setAuthority_rule(String authority_rule) {
        this.authority_rule = authority_rule;
    }

    public String getFrom_user_name() {
        return from_user_name;
    }

    public void setFrom_user_name(String from_user_name) {
        this.from_user_name = from_user_name;
    }

    public String getTo_user_name() {
        return to_user_name;
    }

    public void setTo_user_name(String to_user_name) {
        this.to_user_name = to_user_name;
    }

    public String getFrom_user_inst() {
        return from_user_inst;
    }

    public void setFrom_user_inst(String from_user_inst) {
        this.from_user_inst = from_user_inst;
    }

    public String getTo_user_inst() {
        return to_user_inst;
    }

    public void setTo_user_inst(String to_user_inst) {
        this.to_user_inst = to_user_inst;
    }

    public String getFlow_name() {
        return flow_name;
    }

    public void setFlow_name(String flow_name) {
        this.flow_name = flow_name;
    }

    public String getTask_def_name() {
        return task_def_name;
    }

    public void setTask_def_name(String task_def_name) {
        this.task_def_name = task_def_name;
    }

	public int getAuth_status() {
		return auth_status;
	}

	public void setAuth_status(int auth_status) {
		this.auth_status = auth_status;
	}

	public String getAuth_key() {
		return auth_key;
	}

	public void setAuth_key(String auth_key) {
		this.auth_key = auth_key;
	}
}
