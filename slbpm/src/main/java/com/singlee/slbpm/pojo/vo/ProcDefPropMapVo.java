package com.singlee.slbpm.pojo.vo;

import com.singlee.slbpm.pojo.bo.ProcDefPropMap;

public class ProcDefPropMapVo extends ProcDefPropMap{
	
	//流程类型
	private String flow_type;

	//产品名称
	private String product_name;
	
	public String getFlow_type() {
		return flow_type;
	}

	public void setFlow_type(String flow_type) {
		this.flow_type = flow_type;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	
}
