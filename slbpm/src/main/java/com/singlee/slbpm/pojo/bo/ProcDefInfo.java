package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Created by 杨阳 on 2017/5/3.
 * 流程定义实体类
 */
@Entity
@Table(name = "TT_PROC_DEF")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcDefInfo {

    @Id
    private String flow_id;	        // 流程定义ID
    private String flow_name;       // 流程名称
    private String flow_type;       // 流程类型
    private String version;         // 版本号
    private String is_active;       // 是否启用
    private String model_id;        // 模型ID
    @Transient
    private String model_name;        // 模型名称
    

    public String getModel_name() {
		return model_name;
	}
	public void setModel_name(String model_name) {
		this.model_name = model_name;
	}
	public String getModel_id() {
		return model_id;
	}
	public void setModel_id(String model_id) {
		this.model_id = model_id;
	}
	public String getFlow_id() {
        return flow_id;
    }
    public void setFlow_id(String flow_id) {
        this.flow_id = flow_id;
    }

    public String getFlow_name() {
        return flow_name;
    }
    public void setFlow_name(String flow_name) {
        this.flow_name = flow_name;
    }

    public String getFlow_type() {
        return flow_type;
    }
    public void setFlow_type(String flow_type) {
        this.flow_type = flow_type;
    }

    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    public String getIs_active() {
        return is_active;
    }
    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

}
