package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TT_PROC_SUSPENSION_LOG")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcSuspensionLog {

	private String deal_no;		// 交易流水ID
	private String user_id;		// 操作员
	@Transient
	private String user_name;	// 操作员
	private String op_time;		// 操作时间
	private String op_desc;		// 变更描述
	private String reason;		// 变更理由
	

    public String getDeal_no() {
        return deal_no;
    }    
    public void setDeal_no(String deal_no) {
        this.deal_no = deal_no;
    }

    public String getUser_id() {
        return user_id;
    }    
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOp_time() {
        return op_time;
    }    
    public void setOp_time(String op_time) {
        this.op_time = op_time;
    }

    public String getOp_desc() {
        return op_desc;
    }    
    public void setOp_desc(String op_desc) {
        this.op_desc = op_desc;
    }

    public String getReason() {
        return reason;
    }    
    public void setReason(String reason) {
        this.reason = reason;
    }
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
    
}
