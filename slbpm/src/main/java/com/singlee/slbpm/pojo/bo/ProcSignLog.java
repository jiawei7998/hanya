package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TT_PROC_SIGN_LOG")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class ProcSignLog {
	
	private String proc_inst_id;	// 流程实例ID
    private String task_def_key;	// 节点定义KEY
    private String vote_user;		// 投票用户
    private String vote_time;		// 投票时间
    private String result;			// 投票结果 0-拒绝 1-通过
    private String task_id;			//任务ID
    private String activity_name;	//节点名称
    
    @Transient
    private String comment;			//审批意见
 
    public String getProc_inst_id() {
        return proc_inst_id;
    }
    public void setProc_inst_id(String proc_inst_id) {
        this.proc_inst_id = proc_inst_id;
    }
    
    public String getTask_def_key() {
        return task_def_key;
    }
    public void setTask_def_key(String task_def_key) {
        this.task_def_key = task_def_key;
    }
    
    public String getVote_user() {
        return vote_user;
    }
    public void setVote_user(String vote_user) {
        this.vote_user = vote_user;
    }
    
    public String getVote_time() {
        return vote_time;
    }
    public void setVote_time(String vote_time) {
        this.vote_time = vote_time;
    }
    
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getTask_id() {
		return task_id;
	}
	public void setTask_id(String task_id) {
		this.task_id = task_id;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	
}
