package com.singlee.slbpm.pojo.vo;

import com.singlee.slbpm.pojo.bo.TtFlowLog;

public class ApproveLogVo extends TtFlowLog{
	
	/**  外部序号（审批单号） */
	private String serial_no;
	/**  用户名称 */
	private String user_name;
	/**  转授权用户名称 */
	private String auth_user_name;
	/**  角色名称 */
	private String role_name;
	/**  当前版本  */
	private String current_version;
	/**  当前节点  */
	private String current_node;
	
	public ApproveLogVo(){
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ApproveLogVo [serial_no=");
		builder.append(serial_no);
		builder.append(", user_name=");
		builder.append(user_name);
		builder.append(", auth_user_name=");
		builder.append(auth_user_name);
		builder.append(", role_name=");
		builder.append(role_name);
		builder.append(", current_version=");
		builder.append(current_version);
		builder.append(", current_node=");
		builder.append(current_node);
		builder.append("]");
		return builder.toString();
	}


	public String getAuth_user_name() {
		return auth_user_name;
	}

	public void setAuth_user_name(String auth_user_name) {
		this.auth_user_name = auth_user_name;
	}

	public String getSerial_no() {
		return serial_no;
	}

	public void setSerial_no(String serial_no) {
		this.serial_no = serial_no;
	}


	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getCurrent_version() {
		return current_version;
	}

	public void setCurrent_version(String current_version) {
		this.current_version = current_version;
	}

	public String getCurrent_node() {
		return current_node;
	}

	public void setCurrent_node(String current_node) {
		this.current_node = current_node;
	}
	
	
	
}
