package com.singlee.slbpm.pojo.vo;

import com.singlee.slbpm.pojo.bo.TtFlowRoleUserMap;

/**
 * 
 * 流程角色 用户 关系对象
 * 
 * @author LyonChen
 * 
 */
public class FlowRoleUserMapVo extends TtFlowRoleUserMap{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String role_name;// 角色名称
	
	private String is_valid;//转授权是否有效  1：有效  0：无效
	
	private String user_name;// 柜员名称
	
	

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public FlowRoleUserMapVo() {
		super();
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getIs_valid() {
		return is_valid;
	}

	public void setIs_valid(String is_valid) {
		this.is_valid = is_valid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
