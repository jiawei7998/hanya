package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * 审批功能字典表
 * 
 * @author Yang Yang
 * 
 */
@Entity
@Table(name = "TT_FLOW_OPERATION")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtFlowOperation{

	@Id
	private String operation_id;	// 审批操作类型ID
	
	private String operation_name;	// 审批操作类型名称


	public String getOperation_id() {
		return operation_id;
	}

	public void setOperation_id(String operation_id) {
		this.operation_id = operation_id;
	}

	public String getOperation_name() {
		return operation_name;
	}

	public void setOperation_name(String operation_name) {
		this.operation_name = operation_name;
	}
}
