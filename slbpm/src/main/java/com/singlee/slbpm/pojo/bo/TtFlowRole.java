package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.*;


/**
 * TT_FLOW_ROLE      审批流程角色
 * @author   Libo
 * Create Time  2012-11-22 09:40:12
 */
@Entity
@Table(name = "TT_FLOW_ROLE")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtFlowRole {

    /**  审批角色ID  */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY,generator = "SELECT SEQ_TA_BASE.NEXTVAL FROM DUAL")
	private String role_id;

    /**  角色名称  */
    private String role_name;

    /**  备注  */
    private String memo;

    /**  机构号  */
    private String inst_id;
    
    @Transient
    private String user_list;

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getInst_id() {
		return inst_id;
	}

	public void setInst_id(String inst_id) {
		this.inst_id = inst_id;
	}

	public String getUser_list() {
		return user_list;
	}

	public void setUser_list(String user_list) {
		this.user_list = user_list;
	}
}
