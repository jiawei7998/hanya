package com.singlee.slbpm.pojo.bo;

import tk.mybatis.mapper.annotation.NameStyle;
import tk.mybatis.mapper.code.Style;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 
 * 流程角色 用户 关系对象
 * 
 * @author LyonChen
 * 
 */
@Entity
@Table(name = "TT_FLOW_ROLE_USER_MAP_HIS")
@NameStyle(Style.lowercase) // 使用转换成小写的方式
public class TtFlowRoleUserMapHis implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String role_id;// 角色ID
	@Id
	private String user_id;// 用户ID
	private String auth_userid;// 授权用户ID 空为没有转授权
	private String auth_begdate;// 授权开始日期
	private String auth_enddate;// 授权结束日期
	private String operate_time;// 操作时间
	private String operate_type;// 操作类型

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getAuth_userid() {
		return auth_userid;
	}

	public void setAuth_userid(String auth_userid) {
		this.auth_userid = auth_userid;
	}

	public String getAuth_begdate() {
		return auth_begdate;
	}

	public void setAuth_begdate(String auth_begdate) {
		this.auth_begdate = auth_begdate;
	}

	public String getAuth_enddate() {
		return auth_enddate;
	}

	public void setAuth_enddate(String auth_enddate) {
		this.auth_enddate = auth_enddate;
	}

	public String getOperate_time() {
		return operate_time;
	}

	public void setOperate_time(String operate_time) {
		this.operate_time = operate_time;
	}

	public String getOperate_type() {
		return operate_type;
	}

	public void setOperate_type(String operate_type) {
		this.operate_type = operate_type;
	}

}
