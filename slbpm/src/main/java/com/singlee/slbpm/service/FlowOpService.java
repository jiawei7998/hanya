package com.singlee.slbpm.service;

import org.activiti.engine.runtime.ProcessInstance;


/**
 * 流程操作接口
 * @author Yang Yang
 *
 */
public interface FlowOpService {
	
	/**
	 * 创建一个新的审批实例
	 * @param user_id					用户id(提交人)
	 * @param flow_id					流程定义ID
	 * @param flow_type					流程类型
	 * @param serial_no					编号,通常为外部审批单号
	 * @param specific_user 			指定用户
	 * @param need_risk_approve			是否需要风险预审
	 * @param is_pre_auth_pass  		是否预授权通过
	 * @param limit_level				限额类型
	 * @param status_change_listener	审批流程状态变更监听service，该service需要交spring管理，
	 * 									同时需要实现[com.joyard.jir.service.flow.FlowEndListener]接口
	 * @param msg						审批信息
	 * @param prd_no					产品编号 
	 * @return processInstance  		流程实例
	 * 
	 * @author 杨阳 	updated @ 2017.5.7
	 */
	public ProcessInstance startNewFlowInstance(String user_id, String flow_id, String flow_type, 
												String serial_no, String specific_user, 
												boolean need_risk_approve, 
												boolean is_pre_auth_pass, 
												String limit_level, String status_change_listener, 
												String msg, String prd_no)throws Exception;

	/**
	 * 个人审批操作
	 * @param userId					用户ID
	 * @param taskId					任务ID
	 * @param taskId					serial_no编号,通常为外部审批单号 modify by shenzl20211012
	 * @param completeType				类型   【-1:不通过  0：继续    1：通过 】
	 * @param specific_user 			指定用户
	 * @param msg						备注信息
	 * @param back_to					回退的目标节点
	 */
	public void completePersonalTask(String userId, String taskId, String serial_no, String completeType,
									 String specific_user, String msg, String back_to) throws Exception;

	/**
	 * 挂起流程实例
	 * @param 	processInstanceId		流程实例ID
	 * @param 	userId					操作员ID
	 * @param 	reason					变更理由
	 * @throws 	Exception
	 */
	public void suspendFlowByTaskId(String processInstanceId, String userId, String reason) throws Exception;

	/**
	 * 激活流程实例
	 * @param 	processInstanceId		流程实例ID
	 * @param 	userId					操作员ID
	 * @param 	reason					变更理由
	 * @throws 	Exception
	 */
	public void activateFlowByTaskId(String processInstanceId, String userId, String reason) throws Exception;
	
	/**
	 * 任务转办
	 * @param task_id					任务实例ID
	 * @param to_user_id				目标用户ID
	 * @throws 	Exception
	 */
	public void transferTask(String task_id, String to_user_id) throws Exception;
	
	/**
	 * 根据参数业务编号撤消其流程
	 * @param deal_no
	 */
	public void ApproveCancel(String deal_no);

	public void recall(String deal_no) throws Exception;

	/**
	 * 删除流程实例
	 * @param deal_no
	 */
	public void deleteProcessInstance(String deal_no, String reason) throws Exception;

}
