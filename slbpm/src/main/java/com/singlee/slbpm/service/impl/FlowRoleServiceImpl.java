package com.singlee.slbpm.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.ParentChildUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.dict.DictConstants;
import com.singlee.slbpm.mapper.*;
import com.singlee.slbpm.pojo.bo.*;
import com.singlee.slbpm.pojo.vo.ApproveUser;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapHisVo;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapVo;
import com.singlee.slbpm.service.FlowQueryService;
import com.singlee.slbpm.service.FlowRoleService;
import com.singlee.slbpm.util.ProcessDefinitionUtils;
import com.singlee.slbpm.util.SlbpmVariableNameConstants;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 流程角色管理Service
 * @author Libo
 *
 */
@Service("flowRoleService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FlowRoleServiceImpl implements FlowRoleService {


	/**	流程角色dao  */
	@Autowired
	private TtFlowRoleMapper ttFlowRoleMapper;
	
	/** 流程角色转授权 dao */
	@Autowired
	private TtFlowRoleUserMapMapper ttFlowRoleUserMapMapper;
	
	/** 流程角色转授权历史 dao */
	@Autowired
	private TtFlowRoleUserMapHisMapper ttFlowRoleUserMapHisMapper;
	
	@Autowired
	private TtFlowOperationMapper ttFlowOperationMapper;
	
	@Autowired
	private TtFlowRoleAuthMapper ttFlowRoleAuthMapper;
	
	@Autowired
	private FlowQueryService flowQueryService;
	
	@Autowired
	private ProcessEngine processEngine;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private RuntimeService runtimeService;
	

	@Autowired
	private DayendDateService ddService;
	/**
	 * 新增审批角色
	 * @param flowRole
	 */
	@Override
	public void addNewFlowRole(TtFlowRole flowRole){
		ttFlowRoleMapper.insert(flowRole);
	}
	
	/**
	 * 删除审批角色
	 * @param roleId
	 * @deprecated
	 */
	@Override
	public void deleteFlowRole(String roleId){
//		ttFlowRoleMapper.delete(roleId);
//		TtFlowRoleUserMap map = new TtFlowRoleUserMap();
//		map.setRole_id(roleId);
//		ttFlowRoleUserMapMapper.deleteFlowRoleUserMap(map);
	}
	
	/**
	 * 修改审批角色
	 * @param flowRole
	 */
	@Override
	public void modifyFlowRole(TtFlowRole flowRole){
		ttFlowRoleMapper.updateByPrimaryKey(flowRole);
	}
	
	/**
	 * 查询审批角色
	 * @param roleId	角色id	【可为空】
	 * @param roleName	角色名称  【可为空】
	 * @param memo		备注		【可为空】
	 * @param instId	机构id  【可为空】
	 */
	@Override
	public List<TtFlowRole> searchFlowRole(String roleId, String roleName, String memo, String instId){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("role_id", roleId);
		map.put("role_name", roleName);
		map.put("memo", memo);
		map.put("inst_id", instId);
		TtFlowRole r = new TtFlowRole();
		ParentChildUtil.HashMapToClass(map, r);
		return ttFlowRoleMapper.select(r);
	}
	
	/**
	 * 查询审批角色
	 * @param roleId	角色id	【可为空】
	 * @param roleName	角色名称  【可为空】
	 * @param memo		备注		【可为空】
	 * @param instId	机构id  【可为空】
	 */
	@Override
	public Page<TtFlowRole> searchFlowRolePage(Map<String,Object> map){
		return ttFlowRoleMapper.selectFlowRolePage(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 获取 转授权 
	 * 获得 柜员（user_id）的所有审批角色柜员映射关系
	 * 一般不建议两参数同时为空，同时为空可能返回记录不受控制，导致内存占用
	 * 为空请填 null
	 *@map.user_id 允许为空，
	 *@map.role_id 允许为空 为空则不关注   
	 *@map.user_id_list 指定的用户id列表
	 * @author LyonChen
	 */
	@Override
	public List<FlowRoleUserMapVo> listFlowRoleUserMap(Map<String, Object>map) {	
		map.put("business_date", ddService.getSettlementDate());
		return ttFlowRoleUserMapMapper.listFlowRoleUserMap(map);
	}
	
	/**
	 * 清空 转授权 
	 * 清除 柜员（user_id）的所有审批角色柜员映射关系
	 * 一般不建议两参数同时为空，同时为空可能返回记录不受控制，导致内存占用
	 * 为空请填 null
	 *@map.user_id 允许为空，
	 *@map.role_id 允许为空 为空则不关注   
	 * @author LyonChen
	 */
	@Override
	public void resetFlowRoleUserMap(Map<String, Object> paramMap){
		ttFlowRoleUserMapMapper.resetFlowRoleUserMap(paramMap);
		
		//录入转授权历史
		paramMap.put("operate_type", DictConstants.OperationType.delete);
		this.saveFlowRoleUserMapHis(paramMap);
	}
	
	/**
	 * 保存  转授权 记录表
	 * @author LyonChen
	 * @param user_id 指定柜员 
	 * list.role_id 指定角色 （如果为空，表示所有的角色都授权给 auth_id 柜员
	 * list.auth_id 指定
	 * list.beg_date 
	 * list.end_date
	 * @author LyonChen
	 */
	@Override
	public void saveFlowRoleUserMap(String user_id, List<FlowRoleUserMapVo> list){
		if(list==null || list.size()<=0){
			return;
		}
		Map<String, Object> paramMap = new HashMap<String,Object>();
		paramMap .put("user_id",user_id);
		paramMap.put("business_date", ddService.getSettlementDate());
		List<FlowRoleUserMapVo> dbList = ttFlowRoleUserMapMapper.listFlowRoleUserMap(paramMap);
		if(list.size()==1 && "*".equals(list.get(0).getRole_id())){
			//统一授权
			for(FlowRoleUserMapVo dbmap : dbList){
				dbmap.setAuth_begdate(list.get(0).getAuth_begdate());
				dbmap.setAuth_enddate(list.get(0).getAuth_enddate());
				dbmap.setAuth_userid(list.get(0).getAuth_userid());
				ttFlowRoleUserMapMapper.updateFlowRoleUserMap(dbmap);
				
				//录入转授权历史
				Map<String, Object> hisMap = ParentChildUtil.ClassToHashMap(dbmap);
				hisMap.put("operate_type", DictConstants.OperationType.add);
				this.saveFlowRoleUserMapHis(hisMap);
			}
		}else{
			// list是js提交数据，dblist是session userid数据库中所有的映射关系，需要明确确认 list中的数据在dblist中存在
			for(FlowRoleUserMapVo map : list){
				for(FlowRoleUserMapVo dbmap : dbList){
					if(map.getRole_id().equals(dbmap.getRole_id()) && map.getUser_id().equals(dbmap.getUser_id())){
						//说明 在数据库中确实有 userid+roleid的组合关系，使用js提交的数据更新
						ttFlowRoleUserMapMapper.updateFlowRoleUserMap(map);
						
						//录入转授权历史
						Map<String, Object> hisMap = ParentChildUtil.ClassToHashMap(map);
						hisMap.put("operate_type", DictConstants.OperationType.add);
						this.saveFlowRoleUserMapHis(hisMap);
						
						break;
					}
				}
			}
		}
	}
	
	/**
	 * 修改角色用户对应关系
	 * @param role_id	角色id [非空]
	 * @param user_ids	用户id列表，用“，”隔开
	 */
	@Override
	public void modifyFlowRoleUserMap(String role_id,String user_ids){
		//删除多余的关联关系
		Map<String,String> map= new HashMap<String,String>();
		for(String user_id:user_ids.split(",")){
			if(StringUtil.isEmpty(user_id)) { continue;}
			map.put("role_id", role_id);
			map.put("user_ids", user_id);
			ttFlowRoleUserMapMapper.deleteFlowRoleUserMapExceptUserIds(map);
		}
		//创建不存在的关联关系
		for(String user_id:user_ids.split(",")){
			if(StringUtil.isEmpty(user_id)) {continue;}
			Map<String,Object> oneMap= new HashMap<String,Object>();
			oneMap.put("role_id", role_id);
			oneMap.put("user_id", user_id);
			//判断用户是否有审批列表的页面权限
//			if(StringUtil.isNotEmpty(user_id) && 
//					!sessionService.checkUserRoleWithModuleId(user_id, "100001")){
//				UserVo userVo = userService.selectUser(user_id);
//				throw new RException((userVo!=null?userVo.getUser_name():"")+"["+user_id+"] 没有[审批列表]页面权限");
//			}
			oneMap.put("business_date", ddService.getSettlementDate());
			List<FlowRoleUserMapVo> list = ttFlowRoleUserMapMapper.listFlowRoleUserMap(oneMap);
			if(list==null || list.size()==0){
				TtFlowRoleUserMap roleUserMap = new TtFlowRoleUserMap();
				roleUserMap.setRole_id(role_id);
				roleUserMap.setUser_id(user_id);
				ttFlowRoleUserMapMapper.insert(roleUserMap);
			}
		}
	}
	/**
	 * 删除角色用户对应关系
	 * @param role_id	角色id [非空]
	 * @param user_ids	用户id列表，用“，”隔开
	 */
	@Override
	public void deleteFlowRoleUserMap(String role_id,String user_ids){
		//删除多余的关联关系
		Map<String,String> map= new HashMap<String,String>();
		for(String user_id:user_ids.split(",")){
			if(StringUtil.isEmpty(user_id)) {continue;}
			map.put("role_id", role_id);
			map.put("user_ids", user_id);
			ttFlowRoleUserMapMapper.deleteFlowRoleUserMapExceptUserIds(map);
		}
	}
	/**
	 * 添加角色用户对应关系
	 * @param role_id	角色id [非空]
	 * @param user_ids	用户id列表，用“，”隔开
	 */
	@Override
	public void addFlowRoleUserMap(String role_id,String user_ids){
		//删除多余的关联关系
		Map<String,String> map= new HashMap<String,String>();
		map.put("role_id", role_id);
		map.put("user_ids", user_ids);
		ttFlowRoleUserMapMapper.deleteFlowRoleUserMapExceptUserIds(map);
	}
	/**
	 * 转授权历史保存
	 * @param map
	 */
	@Override
	public void saveFlowRoleUserMapHis(Map<String,Object> map){
		TtFlowRoleUserMapHis ttFlowRoleUserMapHis = new TtFlowRoleUserMapHis();
		ParentChildUtil.HashMapToClass(map, ttFlowRoleUserMapHis);
		ttFlowRoleUserMapHis.setOperate_time(DateUtil.getCurrentDateTimeAsString());
		ttFlowRoleUserMapHisMapper.insert(ttFlowRoleUserMapHis);
	}
	
	/**
	 * 转授权历史查询
	 * @param map
	 */
	@Override
	public Page<FlowRoleUserMapHisVo> pageFlowRoleUserMapHis(Map<String,Object> map){
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<FlowRoleUserMapHisVo> page = ttFlowRoleUserMapHisMapper.pageFlowRoleUserMapHis(map, rb);
		return page;
	}
	
	/**
	 * 根据角色ID，查询该角色下的所有用户	
	 * @param roles				角色列表
	 * @param userIdList		用户的ID列表，如果传了，则从此列表中选取
	 * @param lastOperator		上一步审批人，为了控制是否允许两步为同一审批人
	 * @return
	 */
	@Override
	public List<ApproveUser> getUserMapByRole(String roles,
											  List<String> userIdList,
											  String lastOperator){
		// TODO  取实际要分配的审批人员，需考虑转授权
		List<ApproveUser> list = new ArrayList<ApproveUser>();
		
		for (String role_id : roles.split(",")) {
			if(StringUtil.isEmpty(role_id)) {continue;}
			// 给角色内用户分派任务
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("role_id", role_id);
			map.put("user_id_list", userIdList);
			if(!StringUtil.isEmpty(lastOperator)){
				map.put("except_user", lastOperator);
			}else{
				map.put("except_user", FlowOpServiceImpl.startNewFlowUser.toString());
			}
			List<FlowRoleUserMapVo> flowRoleUserMapVoList = listFlowRoleUserMap(map);
			for(FlowRoleUserMapVo user:flowRoleUserMapVoList){
				if(DictConstants.YesNo.YES.equals(user.getIs_valid())){
					//转授权
					list.add(new ApproveUser(user.getAuth_userid(),"","","",
							     			 user.getUser_id(),user.getUser_name(),
							     			 user.getRole_id(),user.getRole_name()));
				}else{
					list.add(new ApproveUser(user.getUser_id(),user.getUser_name(),
											 user.getRole_id(),user.getRole_name(),
											 user.getUser_id(),user.getUser_name(),
											 user.getRole_id(),user.getRole_name()));
				}
			}
		}
		return list;
	}
	
	/** + 2017.5.16 - Yang Yang - 审批权限设置模块 */
	
	/**
	 * 获取审批操作字典列表
	 */
	@Override
	public List<TtFlowOperation> getFlowOperations() {
		return ttFlowOperationMapper.selectAll();
	}

	/**
	 * 获取审批角色的操作权限
	 * @param	role_id			审批角色ID
	 */
	@Override
	public List<TtFlowRoleAuth> getFlowRoleOperations(String role_id) {
		TtFlowRoleAuth ttFlowRoleAuth = new TtFlowRoleAuth();
		ttFlowRoleAuth.setFlow_role_id(role_id);
		return ttFlowRoleAuthMapper.select(ttFlowRoleAuth);
	}

	/**
	 * 设置审批角色的操作权限
	 * @param	role_id			审批角色ID
	 * @param	op_ids			权限列表
	 */
	@Override
	public void setFlowRoleOperations(String role_id, String op_ids) {
		
		// 删除审批角色下原来的权限
		TtFlowRoleAuth ttFlowRoleAuth_delete = new TtFlowRoleAuth();
		ttFlowRoleAuth_delete.setFlow_role_id(role_id);
		ttFlowRoleAuthMapper.delete(ttFlowRoleAuth_delete);
		
		// 插入更新后的权限
		for(String op_id: op_ids.split(",")){
			TtFlowRoleAuth ttFlowRoleAuth = new TtFlowRoleAuth();
			ttFlowRoleAuth.setFlow_role_id(role_id);
			ttFlowRoleAuth.setFlow_operation_id(op_id);
			ttFlowRoleAuthMapper.insert(ttFlowRoleAuth);
		}		
	}
	
	/** - 2017.5.16 - Yang Yang - 审批权限设置模块 */
	
	/**
	 * add by wangchen on 2017/5/22
	 * 根据登录用户查询审批权限
	 * @param task_id			任务ID
	 * @return List<String>
	 */
	@Override
	public List<String> getUserFlowOperations(String task_id) throws Exception{
		
	    // 获取当前登陆人
	    String loginUserId = SlSessionHelper.getUserId();
	    
	    // 返回用List
	    List<String> resultList = new ArrayList<String>();
	    
	    // 判断当前节点的属性 [竞签节点/会签节点/加签节点]
	    String task_def_key = "";
	    
	    if (StringUtils.isNotEmpty(task_id)) {
	    	
	    	task_def_key = flowQueryService.getTaskKeyByTaskId(task_id);
	    }

		// 根据登录用户查询审批角色
		List<String> lst_user_role_id = this.getUserRoleList(loginUserId);

		// 加签节点
		if (task_def_key.contains(":sid-")) {

			// 默认有审批权限
			resultList.add("4");

			TaskEntity entity = ProcessDefinitionUtils.getTaskEntity(processEngine, task_id);
			String flow_id = entity.getProcessDefinitionId();

			// 获取流程内所有人工任务节点
			List<Map<String, Object>> mapNode = flowQueryService.getFlowActivities(flow_id);

			// 获取任务节点下的审批角色，并汇总
			List<String> lst_flow_role_id = new ArrayList<String>();

			for (Map<String, Object> map : mapNode) {

				String node_id = String.valueOf(map.get("Id"));
				ActivityImpl activity = ProcessDefinitionUtils.getActivity(processEngine, flow_id, node_id);
				List<String> role_ids = flowQueryService.getUserTaskRole(activity);
				for (String role_id : role_ids) {
					if (lst_user_role_id.contains(role_id) && !lst_flow_role_id.contains(role_id)) {
						lst_flow_role_id.add(role_id);
					}
				}
			}

			// 获取角色下权限
			for (String role_id : lst_flow_role_id) {

				List<TtFlowRoleAuth> lst_auth = this.getFlowRoleOperations(role_id);

				// 判断角色是否有修改权限
				for(TtFlowRoleAuth role_auth : lst_auth){

					String op_id = role_auth.getFlow_operation_id();

					if ("3".equals(op_id)) {
						resultList.add("3");
						break;
					}
				}
			}
		}
		// 竞签节点/会签节点
		else {

			if (StringUtils.isNotEmpty(task_id)) {

				// 获取当前节点审批角色
				Task task = taskService.createTaskQuery().taskId(task_id).singleResult();
				String flow_id = task.getProcessDefinitionId();
				ActivityImpl activity = ProcessDefinitionUtils.getActivity(processEngine, flow_id, task_def_key);

				// 获取节点审批角色
				List<String> lst_flow_role_id = flowQueryService.getUserTaskRole(activity);

				// 当前节点审批角色和登录用户所属的审批角色取交集
				lst_user_role_id.retainAll(lst_flow_role_id);

				// 获取角色下权限
				for (String role_id : lst_user_role_id) {

					List<TtFlowRoleAuth> lst_auth = this.getFlowRoleOperations(role_id);

					for(TtFlowRoleAuth role_auth : lst_auth){
						String op_id = role_auth.getFlow_operation_id();
						if(!(resultList.contains(op_id))){
							resultList.add(op_id);
						}
					}
				}

				// 会签节点
				if (task_def_key.startsWith("Parallel-")) {

					if (resultList.contains("21")) {
						// 办公室用印只显示通过按钮
						resultList.clear();
						resultList.add("4");
					}
					else {
						// 会签节点默认只显示赞成/反对/弃权三个操作按钮
						resultList.clear();
						resultList.add("vote");
					}
				}

				// 大连银行需求：合同审查岗附上修改意见后，发起人重新提交时，需要显示是否按法审意见修改的确认框
				String instance_id = task.getProcessInstanceId();
				Object obj1 = runtimeService.getVariable(instance_id, SlbpmVariableNameConstants.ContractReview);
				Object obj2 = runtimeService.getVariable(instance_id, SlbpmVariableNameConstants.Initiator);
				String review_advice = (obj1 == null) ? "" : obj1.toString();
				String initiator = (obj2 == null) ? "0" : obj2.toString();

				// 合同审查岗已告知发起人应当修改，并且当前用户是发起人
				if ("2".equals(review_advice) && initiator.equals(loginUserId)) {
					resultList.add("modify_as_required");
				}
			}
		}
	    
	    return resultList;
	}

    /**
     * 检查当前用户是否能发起审批
     * @param user_id           用户ID
     * @param flow_id			流程定义ID
     * @return					是/否
     */
	@Override
	public boolean checkFlowInitiator(String user_id, String flow_id) {

        // 获取当前登陆人
        if (StringUtils.isEmpty(user_id)) {
            user_id = SlSessionHelper.getUserId();
        }

		// 获取流程第一个节点[经办岗]
		ActivityImpl first_activity = flowQueryService.getFirstActivity(flow_id);

        // 获取节点审批角色
        List<String> lst_act_role_id = flowQueryService.getUserTaskRole(first_activity);

        // 获取登录用户所属审批角色
        List<String> lst_user_role_id = this.getUserRoleList(user_id);

        // 当前节点审批角色和登录用户所属的审批角色取交集
        lst_user_role_id.retainAll(lst_act_role_id);

		return lst_user_role_id.size() > 0;
	}

    /**
     * 检查当前用户是否能审批当前任务
     * @param user_id           用户ID
     * @param task			任务ID
     * @return					是/否
     */
    @Override
    public boolean checkTaskAccess(String user_id, Task task) {
        // 获取节点审批角色
        ActivityImpl activity = ProcessDefinitionUtils.getActivity(processEngine,task.getProcessDefinitionId(), task.getTaskDefinitionKey());
        List<String> lst_act_role_id = flowQueryService.getUserTaskRole(activity);
        // 获取登录用户所属审批角色
        List<String> lst_user_role_id = this.getUserRoleList(user_id);
        // 当前节点审批角色和登录用户所属的审批角色取交集
        lst_user_role_id.retainAll(lst_act_role_id);

        return lst_user_role_id.size() > 0;
    }

    /**
     * 获取登录用户所属审批角色
     * @param user_id       用户ID
     * @return              角色ID列表
     */
	private List<String> getUserRoleList(String user_id) {

        List<String> lst_user_role_id = new ArrayList<String>();

        // 根据登录用户查询审批角色
        Map<String,Object> param = new HashMap<String, Object>();
        param.put("user_id", user_id);
        List<FlowRoleUserMapVo> lst_user_role = this.listFlowRoleUserMap(param);
        for (FlowRoleUserMapVo user_role : lst_user_role) {
            String role_id = user_role.getRole_id();
            if (!lst_user_role_id.contains(role_id)) {
                lst_user_role_id.add(role_id);
            }
        }
        
        //查询被授权的角色
        Map<String,Object> paramA = new HashMap<String, Object>();
        paramA.put("auth_user", user_id);
        List<FlowRoleUserMapVo> lst_user_role_a = this.listFlowRoleUserMap(paramA);
        for (FlowRoleUserMapVo user_role_a : lst_user_role_a) {
        	if("0".equals(user_role_a.getIs_valid())){
        		continue;
        	}
            String role_id = user_role_a.getRole_id();
            if (!lst_user_role_id.contains(role_id)) {
                lst_user_role_id.add(role_id);
            }
        }

        return lst_user_role_id;
    }
	
	/**
     *   根据流程获取用户所属审批角色
     * @param user_id       用户ID
     * @param flow_id       流程ID
     * @return              节电list
	 * @throws Exception 
     */
	@Override
	public List<Map<String,Object>> getUserRoleListByFlow(String user_id, String flow_id) throws Exception {
		//获取审批节点
		List<Map<String, Object>> list = flowQueryService.getFlowActivities(flow_id);
		//获取传入用户拥有的审批角色
		List<String> userRole = this.getUserRoleList(user_id);
		//返回List
		List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
		for(Map<String,Object> item : list){
			String task_def_key = ParameterUtil.getString(item, "Id", "");
			ActivityImpl activity = ProcessDefinitionUtils.getActivity(processEngine, flow_id, task_def_key);
			List<String> taskDefRole = flowQueryService.getUserTaskRole(activity);
			for(String item2 : taskDefRole){
				if(userRole.contains(item2)){
					result.add(item);
				}
			}
		}
		return result;
	}
}
