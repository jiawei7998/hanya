package com.singlee.slbpm.service;

import com.github.pagehelper.Page;
import com.singlee.slbpm.pojo.bo.*;
import com.singlee.slbpm.pojo.vo.ProcDefPropMapVo;
import com.singlee.slbpm.pojo.vo.TdNotificationVo;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.apache.ibatis.session.RowBounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程查询接口
 * @author Yang Yang
 *
 */
public interface FlowQueryService {

	/**
	 * 通过外部流水号获取流程定义ID和流程实例ID
	 * @param 	serial_no				外部流水号（审批单号）
	 * @return 	Map<String, String>
	 * 			serial_no				外部流水号（审批单号）
	 * 			flow_id					流程定义ID
	 * 			instance_id				流程实例ID
	 * @author 							杨阳
	 */
	public Map<String, String> getFlowIdAndInstanceIdBySerialNo(String serial_no);
    
	/**
	 * 通过外部流水号查询相关的流程信息
	 * @param serial_no
	 * @return
	 */
	public TtFlowSerialMap getFlowSerialMap(String serial_no);
	
	public TtFlowSerialMap getFlowSerialMap(Map<String, Object> map);
	
	/**
	 * 取指定版本的流程定义信息，若version为空，则取最新的流程
	 * @param 	flow_id			流程id【非空】
	 * @param 	version			版本号
	 * @return	ProcDefInfo		流程定义对象
	 */
	public ProcDefInfo getFlowDefine(String flow_id, String version);
	
	/**
	 * 根据外部审批单号查询使用的流程定义信息
	 * @param 	serial_no		外部流水号
	 * @return	ProcDefInfo		流程定义对象
	 */
	public ProcDefInfo getFlowDefineBySerialNo(String serial_no);
	
	/**
	 * 查询待办任务预览(控制台显示)
	 * @return	
	 */
	public Page<HashMap<String, Object>> searchTaskPreview(Map<String, Object> param, RowBounds rb);
	
	/**
	 * 是否是最后一个人工节点
	 * @param taskId
	 * @return
	 */
	public boolean isLastManualStep(String taskId);
	public boolean isLastManualStep2(String taskId);
	
	/**
	 * 查询下一个人工审批任务节点的审批候选人
	 * 注：如果下个节点为互斥或并行节点，则返回空，界面上就不能指定下一个节点的审批人
	 * @param 	flowId						流程定义ID
	 * @param	stepId						任务实例ID（步骤）
	 * @param	serialId					
	 * @return	Map<String, Object>			
	 * 										actType		节点类型 （竞签userTask/会签voteTask/其他other）
	 * 										userList	审批用户列表
	 * @author 	Yang Yang 2017/5/23
	 */
	public Map<String, Object> getNextStepCandidatesList(String flowId, String stepId, String serialNo);
	
	/**
	 * 获取流程内所有人工任务节点
	 * @param processDefinitionId
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> getFlowActivities(String processDefinitionId) throws Exception;
	
	/**
	 * 获取流程人工任务节点上的绑定表单ID
	 * @param procDefId						流程定义ID
	 * @param taskDefKey					人工任务节点key
	 * @param prdCode						产品编号
	 * @return
	 * @throws Exception
	 */
	public ProcTaskForm getUserTaskForm(String procDefId, String taskDefKey, String prdCode) throws Exception;
	
	/**
	 * 获取流程人工任务节点上绑定的审查要素
	 * @param procDefId						流程定义ID
	 * @param taskDefKey					人工任务节点key
	 * @param prdCode						产品编号
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getUserTaskChecklists(String procDefId, String taskDefKey, String prdCode) throws Exception;
	
	/**
	 * 获取流程人工任务节点上的绑定事件ID
	 * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
	 * @return
	 * @throws Exception
	 */
	public List<TdFlowTaskEvent> getUserTaskEvents(Map<String, Object> map) throws Exception;

	/**
	 * 查询流程人工任务节点的未选事件列表
	 * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
	 * @return
	 * @throws Exception
	 */
	public List<TdFlowTaskEvent> getFlowTaskEventUnselect(Map<String, Object> map) throws Exception;
	
	/**
	 * 查询流程节点事件列表
	 * @return
	 */
	public Page<TdFlowTaskEvent> getFlowTaskEvent(Map<String, Object> map);
	
	/**
	 * 查询流程节点已选事件列表
	 * @return
	 */
	public List<TdFlowTaskEvent> getFlowTaskEventSelect(Map<String, Object> map);
	
	/**
	 * 查询流程节点已选事件列表
	 * @return
	 */
	public List<TdFlowTaskEvent> getFlowTaskEventSelectByTask(Map<String, Object> map);

	/**
	 * 获得当前任务节点的审批角色
	 * @param activityImpl          任务实例
	 * @return
	 */
	public List<String> getUserTaskRole(ActivityImpl activityImpl);
	
	/**
	 * 根据流程关联属性查询唯一的流程定义ID，取得流程定义第一个节点的配置项
	 * 目前只返回该节点的审批角色和审查要素信息
	 * @param product_no	产品编码
	 * @param flow_type		流程类型
	 * @param inst_id		机构ID
	 * @return map(lst_flow_role, lst_checklist)
	 */
	public Map<String, Object> getFirstUserTaskSetting(String product_no, String flow_type, String inst_id) throws Exception;

	/**
	 * 返回指定任务节点的审批角色和审查要素信息
	 * @param task_id	任务ID
	 * @return map(lst_flow_role, lst_checklist)
	 */
	public Map<String, Object> getUserTaskSetting(String task_id, String product_no) throws Exception;
	
	/**
	 * 返回流程会签节点设置
	 * @param task_def_key		节点定义key
	 * @return
	 * @throws Exception
	 */
	public ProcSignSetting getSignSetting(String task_def_key) throws Exception;

	public void addFlowTaskEvent(Map<String, Object> param);

	public void updateFlowTaskEvent(Map<String, Object> param);

	public void deleteFlowTaskEvent(Map<String, Object> param);
	
	/**
	 * 获取会签节点投赞成票数
	 * @param proc_inst_id		流程实例ID
	 * @param task_def_key		会签节点KEY
	 * @return
	 * @throws Exception
	 */
	public int getAgreeVoteCount(String proc_inst_id, String task_def_key) throws Exception;

	/**
	 * 获取会签节点投反对票数
	 * @param proc_inst_id		流程实例ID
	 * @param task_def_key		会签节点KEY
	 * @return
	 * @throws Exception
	 */
	public int getRefuseVoteCount(String proc_inst_id, String task_def_key) throws Exception;

	/**
	 * 根据任务ID获得任务节点定义KEY
	 * @param 	task_id					任务ID
	 * @return 	String					任务节点定义KEY
	 * @author 							杨阳
	 */
	public String getTaskKeyByTaskId(String task_id);

	/**
	 * 查询业务流程定义
	 * @param   map
	 * @return  List<ProcDefPropMapVo>
	 * @author  wangchen
	 */
	public List<ProcDefPropMapVo> getProcDefPropMap(Map<String, Object> map);

    /**
     * 查询消息
     * @param map
     * @return List<ProcDefPropMapVo>
     * @author wangchen
     */
	public List<TdNotificationVo> getNotification(Map<String, Object> map);

    /**
     * 获得下个节点
     */
	public ActivityImpl getNextActivity(String taskId);

    /**
     * 查询流程定义的第一个任务节点
     */
    public ActivityImpl getFirstActivity(String flow_id);

    /**
	 * 获取流程人工任务节点上的绑定url
	 * @param procDefId						流程定义ID
	 * @param taskDefKey					人工任务节点key
	 * @param prdCode						产品编号
	 * @return
	 * @throws Exception
	 */
	public ProcTaskUrl getUserTaskUrl(String procDefId, String taskDefKey, String prdCode, String taskId) throws Exception;

}
