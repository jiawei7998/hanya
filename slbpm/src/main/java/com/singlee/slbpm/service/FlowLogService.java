package com.singlee.slbpm.service;

import com.singlee.slbpm.pojo.bo.ProcSuspensionLog;
import com.singlee.slbpm.pojo.vo.ApproveLogVo;

import java.util.List;
import java.util.Map;

/**
 * 流程日志接口
 * @author Yang Yang
 *
 */
public interface FlowLogService {
	
	/**
	 * 新增审批日志
	 * @param instance_id	流程实例ID
	 * @param user_id		用户ID
	 * @param role_id		角色ID
	 * @param node_id		节点ID
	 * @param node_name		节点名称
	 * @param opFlag		操作类型
	 * @param flow_type		流程类型
	 * @param auth_user		转授权用户
	 * @param msg			审批信息
	 * 
	 * @deprecated			该方法已弃用，流程日志改为由流程引擎生成
	 */
	public void addFlowLog(String instance_id, String user_id, String role_id, 
						   String node_id, String node_name, String opFlag, 
						   String flow_type, String auth_user, String msg);

	/**
	 * 查询审批日志
	 * @param log_id		日志ID
	 * @param instance_id	实例ID
	 * @param serial_no  	序号
	 * @param user_id		用户ID
	 * @param role_id		角色ID
	 * @param node_id		节点ID
	 * @param onlyNew   	只查最新的
	 * 
	 * @deprecated			该方法已弃用，请使用getFlowlogBySerialNo来获取流程日志
	 */
	public List<ApproveLogVo> searchFlowLog(String log_id, String instance_id, String serial_no, 
											String user_id, String role_id, String node_id, 
											boolean onlyNew);

	/**
	 * 更新最新流程节点信息至流程业务关联表
	 * @param serial_no		外部流水号
	 * @param node_id		节点ID
	 */
	public void updateFlowCurrentNode(String serial_no, String node_id);

	/**
	 * 根据业务流水号查询流程日志
	 * @param serial_no		业务流水号
	 */
	public List<Map<String, Object>> getFlowlogBySerialNo(String serial_no);

	/**
	 * 根据业务流水号查询流程挂起日志
	 * @param serial_no		业务流水号
	 */
	public List<ProcSuspensionLog> getSuspendlogBySerialNo(String serial_no);

	/**
	 * 根据serial_no 查询经办复核等操作员
	 * @param serial_no
	 * @return
	 */
	public List<String> selectBySerialNo(String serial_no);

}
