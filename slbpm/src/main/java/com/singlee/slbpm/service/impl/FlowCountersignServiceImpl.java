package com.singlee.slbpm.service.impl;

import com.singlee.slbpm.mapper.ProcSignLogMapper;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.ProcSignLog;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import com.singlee.slbpm.service.FlowCountersignService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Comment;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FlowCountersignServiceImpl implements FlowCountersignService{

	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;
	
	/** 会签日志Dao */
	@Autowired
	private ProcSignLogMapper procSignLogMapper;
	
	/** Activiti任务接口 */
	@Autowired
	private TaskService taskService;
	
	@Override
	public Map<String, Object> getCountersignDetailList(String serial_no) {
		TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, 0);
		String processInstanceId = String.valueOf(flowSerMap.getExecution());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("proc_inst_id", processInstanceId);
		List<ProcSignLog> sign_list =  procSignLogMapper.getSignLogListByInstId(map);
		List<Comment> lst_comments = taskService.getProcessInstanceComments(processInstanceId, "comment");
		
		List<String> list = new ArrayList<String>();
		for(ProcSignLog p : sign_list){
			if(!list.contains(p.getTask_def_key())){
				list.add(p.getTask_def_key());
			}
		}
		List<HashMap<String, Object>> listMap = new ArrayList<HashMap<String, Object>>();
		for(int i=0;i<list.size();i++){
			int count = 0;//投票总人数
			int approve = 0;//同意人数
			HashMap<String, Object> param = new HashMap<String, Object>();
			param.put("taskDefKey", list.get(i));
			List<ProcSignLog> counterSignList = new ArrayList<ProcSignLog>();
			for(ProcSignLog p : sign_list){
				if(p.getTask_def_key().equals(list.get(i))){
					count++;
					counterSignList.add(p);
					param.put("activity_name", p.getActivity_name());
					if("1".equals(p.getResult())){
						approve++;
					}
					for (Comment comment : lst_comments) {
						if(StringUtils.isNotEmpty(p.getTask_id()) 
								&& p.getTask_id().equals(comment.getTaskId())) {
							p.setComment(comment.getFullMessage());
						}
					}
				}
			}
			param.put("Result", approve+"人同意，"+(count-approve)+"人不同意，共"+count+"人参与投票");
			param.put("counterSignList", counterSignList);
			listMap.add(param);
		}	
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", listMap);
		
		return resultMap;
	}

}
