package com.singlee.slbpm.service;

import com.github.pagehelper.Page;
import com.singlee.slbpm.pojo.vo.TaskWaitingView;

import java.util.List;
import java.util.Map;

/**
 * 流程相关service
 * @author Libo
 */
@SuppressWarnings("rawtypes")
public interface FlowService  {
	
	/**
	 * 强制删除一个流程，该操作会强制结束该流程，同时删除该实例
	 * @param user_id	用户id
	 * @param serial_no	单号
	 * @param msg		信息
	 * @param successState  成功以后的状态，不修改则传null
	 * @deprecated
	 */
	public void endFlowInstance(String user_id,String serial_no,String msg,String successState);
	
	/**
	 * 查询个人的任务列表
	 * @param userId	用户ID
	 * @return	
	 * @deprecated
	 */
	public List<Map> findPersonalTasks(String userId);
	
	/**
	 * 获取个人的已审批列表
	 * @param user_id
	 * @param beginDate
	 * @param endDate
	 * @return
	 * @deprecated
	 */
	public Page<Map> findPersonalApprovedLog(String user_id, String beginDate,String endDate,int pageNum,int pageSize);
	
	/**
	 * 转向某个节点
	 * @param taskId
	 * @param nodeName
	 * @deprecated
	 */
	public void goToNode(String taskId,String nodeName,String msg);

	/**
	 * @param taskId
	 * @param userList
	 * @deprecated
	 */
	public void invitationWithTask(String taskId, List<String> userList);

	/**
	 * 获取待办任务列表
	 */
	public List<Map<String, Object>> searchTaskWaitting();
	
	public Page<TaskWaitingView> searchTaskWaittingForConsole(Map<String,Object> param);

	/**
	 * 获取正在进行中的流程列表
	 */
	public List<Map<String, Object>> searchProcessInstance();
	
	public Page<TaskWaitingView> searchDurationTaskWaittingForConsole(Map<String,Object> param);
	
	public Integer searchDurationTaskWaittingCountForConsole(Map<String,Object> param);
	
	
}
