package com.singlee.slbpm.service;

import com.github.pagehelper.Page;
import com.singlee.slbpm.pojo.bo.ProcAuthority;

import java.util.List;
import java.util.Map;

/**
 * Created by wangfp on 2018/2/27.
 */
public interface FlowSublicenseService {

    /**
     * 添加用户转授权信息
     * @param paramMap		转授权信息
     */
    public void addUserSublicenseDetail(Map<String, Object> paramMap) throws Exception;

    /**
     * 获取用户转授权信息列表
     * @param paramMap		转授权查询条件
     */
    public Page<ProcAuthority> getUserSublicenseList(Map<String, Object> paramMap);

    /**
     * 通过id获取单个用户转授权信息
     * @param paramMap		转授权查询条件
     */
    public Map<String, Object> getUserSublicenseById(Map<String, Object> paramMap);

    /**
     * 修改用户转授权信息
     * @param paramMap		转授权信息
     */
    public Map<String, Object> updateUserSublicenseDetail(Map<String, Object> paramMap);

    /**
     * 通过id删除单个用户转授权信息
     * @param paramMap		转授权查询条件
     */
    public Map<String, Object> deleteUserSublicenseById(Map<String, Object> paramMap);

    /**
     * 修改授权期限
     */
    public void renewalUserSublicense(Map<String, Object> paramMap);

    /**
     * 授权期限到期提醒
     */
    public List<ProcAuthority> remindUserSublicense(Map<String, Object> paramMap);

    /**
     * 转办已在流程中的审批流
     */
    public void SublicenseToUserInFlow(Map<String, Object> paramMap);

    /**
     * 查询指定结束日期的转授权记录
     */
    public List<ProcAuthority> getUserSublicenseListByEndDate(String authEndDate);
    
    /**
	 * 提交/同意/拒绝
	 */
    public void approve(String id, int status) throws Exception;

    /**
	 * 将验证码发送到jsp页面
	 */
    public String sendKeyToJsp(String id) throws Exception;

    /**
	 * 确认是否需要校验授权
	 */
    public String isNeedCheck(String taskId) throws Exception;

    /**
	 * 校验
	 */
    public boolean check(String id, String key) throws Exception;

}
