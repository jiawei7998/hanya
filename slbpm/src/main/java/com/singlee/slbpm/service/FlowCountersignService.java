package com.singlee.slbpm.service;

import java.util.Map;

public interface FlowCountersignService {
	
	/**
	 * 根据业务流水号查询会签日志
	 * @param serial_no		业务流水号
	 */
	public Map<String, Object> getCountersignDetailList(String serial_no);
	
}
