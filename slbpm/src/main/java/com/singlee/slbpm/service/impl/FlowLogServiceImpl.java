package com.singlee.slbpm.service.impl;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.service.UserService;
import com.singlee.slbpm.mapper.ProcSuspensionLogMapper;
import com.singlee.slbpm.mapper.TtFlowLogMapper;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.ProcSuspensionLog;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import com.singlee.slbpm.pojo.vo.ApproveLogVo;
import com.singlee.slbpm.service.FlowLogService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Comment;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@SuppressWarnings("unused")
public class FlowLogServiceImpl implements FlowLogService {

	private static Logger logger = LoggerFactory.getLogger("SLBPM");
	
	/** 流程日志Dao */
	@Autowired
	private TtFlowLogMapper ttFlowLogMapper;
	
	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;

	/** Activiti任务接口 */
	@Autowired
	private TaskService taskService;
	
	/** Activiti实例接口 */
	@Autowired
	private RuntimeService runtimeService;

	/** Activiti历史接口 */
	@Autowired
	private HistoryService historyService;
	
	/** Activiti历史接口 */
	@Autowired
	private ProcSuspensionLogMapper procSuspensionLogMapper;
	
	/** 用户service */
	@Autowired
	private UserService userService;
	
	/**
	 * 新增审批日志
	 * @param instance_id	流程实例ID
	 * @param user_id		用户ID
	 * @param role_id		角色ID
	 * @param node_id		节点ID
	 * @param node_name		节点名称
	 * @param opFlag		操作类型
	 * @param flow_type		流程类型
	 * @param auth_user		转授权用户
	 * @param msg			审批信息
	 */
	@Override
	public void addFlowLog(String instance_id, String user_id, String role_id, 
						   String node_id, String node_name, String opFlag, 
						   String flow_type, String auth_user, String msg){

		throw new RException("该方法已弃用，流程日志改为由流程引擎生成");
		
//		TtFlowLog flowLog = new TtFlowLog();
//		flowLog.setInstance_id(instance_id);
//		flowLog.setUser_id(user_id);
//		flowLog.setRole_id(role_id);
//		flowLog.setNode_id(node_id);
//		flowLog.setNode_name(node_name);
//		flowLog.setOpflag(opFlag);
//		flowLog.setFlow_type(flow_type);
//		flowLog.setAuth_user(auth_user);
//		flowLog.setMsg(msg);
//		flowLog.setTime(DateTimeUtil.getLocalDateTime());
//		
//		ttFlowLogMapper.insert(flowLog);
	}	

	/**
	 * 查询审批日志  （注意：该方法不分页）
	 * @param log_id		日志id
	 * @param instance_id	实例id
	 * @param serial_no  	序号
	 * @param user_id		用户id
	 * @param role_id		角色id
	 * @param node_id		节点id
	 */
	@Override
	public List<ApproveLogVo> searchFlowLog(String log_id, String instance_id, String serial_no, 
											String user_id, String role_id, String node_id, 
											boolean onlyNew){
		
		throw new RException("该方法已弃用，请使用getFlowlogBySerialNo来获取流程日志");
		
//		Map<String,Object> map = new HashMap<String,Object>();
//		map.put("log_id", log_id);
//		map.put("instance_id", instance_id);
//		map.put("serial_no", serial_no);
//		map.put("user_id", user_id);
//		map.put("role_id", role_id);
//		map.put("node_id", node_id);
//		map.put("only_new", onlyNew ? "1" : "");
//		
//		this.getFlowlogBySerialNo(serial_no);
//		
//		return ttFlowLogMapper.selectLogVo(map);		
	}

	/**
	 * 更新最新流程节点信息至流程业务关联表
	 * @param serial_no		外部流水号
	 * @param node_id		节点ID
	 */
	@Override
	public void updateFlowCurrentNode(String serial_no, String node_id) {
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("serial_no", serial_no);
		map.put("node_id", node_id);
		
		ttFlowSerialMapMapper.updateCurrentNodeId(map);
	}

	/**
	 * 根据业务流水号查询流程日志
	 * @param serial_no		业务流水号
	 */
	@Override
	public List<Map<String, Object>> getFlowlogBySerialNo(String serial_no) {
		TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, 0);
		if(flowSerMap == null){
			return null;
		}
		String processInstanceId = String.valueOf(flowSerMap.getExecution());
		//根据结束日期进行排正序
		List<HistoricTaskInstance> lst_his = historyService.createHistoricTaskInstanceQuery()
																.processInstanceId(processInstanceId)
																.orderByHistoricTaskInstanceEndTime()
																.asc()
																.list();
		List<Comment> lst_comments = taskService.getProcessInstanceComments(processInstanceId, "comment");
		List<Map<String, Object>> lst_resultMap = new ArrayList<Map<String, Object>>();
		Map<String, Object> resultMap = null;
		//循环处理
		for (HistoricTaskInstance his : lst_his) {
			String task_id = his.getId();
			String task_def_key = his.getTaskDefinitionKey();
			resultMap = new HashMap<String, Object>();
			resultMap.put("TaskId", task_id);
			resultMap.put("ActivityName", his.getName());
			resultMap.put("ProcessInstanceId", his.getProcessInstanceId());
			resultMap.put("StartTime", his.getStartTime());
			resultMap.put("EndTime", his.getEndTime());
			resultMap.put("Duration", his.getDurationInMillis());
			resultMap.put("Assignee", his.getAssignee());
			if(StringUtil.isNotEmpty(his.getAssignee())){
				TaUser assigneeInfo = userService.getUserById(his.getAssignee());
				resultMap.put("AssigneeName", assigneeInfo.getUserName());
			}
			for (Comment comment : lst_comments) {
				if(StringUtils.isNotEmpty(task_id) && task_id.equals(comment.getTaskId())) {
					if (task_def_key.startsWith("Parallel-")) {
						resultMap.put("Message", "*");
					} else {
						resultMap.put("Message", comment.getFullMessage());
					}
				}
			}
			lst_resultMap.add(resultMap);
		}
		return lst_resultMap;
	}
	
	/**
	 * 根据业务流水号查询流程挂起日志
	 * @param serial_no		业务流水号
	 */
	@Override
	public List<ProcSuspensionLog> getSuspendlogBySerialNo(String serial_no) {
		List<ProcSuspensionLog> result = procSuspensionLogMapper.selectSuspensionLogByDealNo(serial_no);
		return result;
	}

	@Override
	public List<String> selectBySerialNo(String serial_no) {
		List<String> result = ttFlowSerialMapMapper.selectBySerialNo(serial_no);
		return result;
	}
}
