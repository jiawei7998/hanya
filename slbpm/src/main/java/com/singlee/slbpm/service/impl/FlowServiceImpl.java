package com.singlee.slbpm.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.mapper.*;
import com.singlee.slbpm.pojo.vo.TaskWaitingView;
import com.singlee.slbpm.service.FlowService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 流程相关Service，包括新建流程定义、实例、处理
 * @author Libo
 *
 */
@Service("flowService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@SuppressWarnings({"unused","rawtypes"})
public class FlowServiceImpl implements FlowService {

	private static Logger log = LoggerFactory.getLogger("SLBPM");
	
	/**
	 * 预授权服务层
	 */
//	@Autowired
//	private PreAuthService preAuthService;
	
	/** 流程定义Dao */
	@Autowired
	private ProcDefInfoMapper procDefInfoMapper;
	
	/** 流程和产品绑定关系Dao */
	@Autowired
	private ProcDefPropMapMapper procDefMapMapper;
	
	/** 任务Dao */
	@Autowired
	private TtFlowTaskMapper ttFlowTaskMapper;
	
	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;
	
	/** 流程日志Dao */
	@Autowired
	private TtFlowLogMapper ttFlowLogMapper;
	
	/** 批量Dao */
	@Autowired
	private BatchDao batchDao;
	
	/**
	 * Activiti Services
	 */	
	@Autowired
	private ProcessEngine processEngine;
	
	@Autowired
	private RepositoryService  repositoryService;
	
	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private TaskService taskService;
	@Autowired
	private InstitutionService institutionService;
	

	/**
	 * 撤销审批     强制删除一个流程，该操作会强制结束该流程，同时删除该实例及其相关信息
	 * @param user_id	用户id
	 * @param serial_no	单号
	 * @param msg		信息
	 * @param successState  成功以后的状态，不修改则传null
	 */
	@Override
	public void endFlowInstance(String user_id,String serial_no,String msg, String successState){
		throw new RException("endFlowInstance方法已弃用");
		/*String flow_id = null;
		TtFlowSerialMap flowSerMap = getFlowSerialMap(serial_no);
		if(flowSerMap!=null){
			flow_id = flowSerMap.getFlow_id();
		}else{
			throw new RException(StringUtil.join(",", "未找到对应的审批流程",serial_no));
		}
		
		try{
			String status_change_listener =executionService.getVariable(flow_id+"."+serial_no, "status_change_listener").toString();
			executionService.deleteProcessInstanceCascade(flow_id+"."+serial_no);
			FlowStatusChangeListener listener = SpringContextHolder.getBean(status_change_listener);
			if(!StringUtil.checkEmptyNull(successState)){
				listener.statusChange(flow_id, serial_no, SlbpmDict.ApproveStatus.New.value());
			}
//			ttFlowSerialMapMapper.updateCurState(serial_no, SlbpmDict.ApproveStatus.New.value());
		}catch(org.jbpm.api.JbpmException e){
			if(e.getMessage().endsWith("no processInstance found for the given id")||
			   e.getMessage().equals("execution "+flow_id+"."+serial_no+" doesn't exist")){
				
			}else{
				throw e;
			}
		}
		ttFlowSerialMapMapper.deleteBySerial(serial_no);  
		addFlowLog(flow_id+"."+serial_no, user_id, " ", "flow_start", "结束", "撤销审批",flowSerMap.getFlow_type(),"","");
//		ttFlowLogMapper.deleteByInstance(flow_id+"."+serial_no);
 */
	}
	
	
	/**
	 * 查询个人的任务列表
	 * @param userId	用户ID
	 * @return	
	 */
	@Override
	public List<Map> findPersonalTasks(String userId){
		throw new RException("findPersonalTasks方法已弃用");
//		MyMap param = new MyMap();
//		param.put("user_id", userId);
//		return ttFlowTaskMapper.searchTask(param);
	}
	
	/**
	 * 获取个人的已审批列表
	 * @param user_id
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	@Override
	public Page<Map> findPersonalApprovedLog(String userId, String beginDate,String endDate,int pageNum,int pageSize){

		throw new RException("findPersonalApprovedLog方法已弃用");
		
		// + FIXME - 杨阳 - useless code?
//		Page<ApproveLogVo> ph = new Page<ApproveLogVo>();
//		ph.setPageSize(pageSize);
//		ph.setPageNum(pageNum);
		// - 
		
//		Map<String,Object> param = new HashMap<String,Object>();
//		param.put("user_id", userId);
//		param.put("begin_date", beginDate);
//		param.put("end_date", endDate);
//		param.put("pageNumber", pageNum);
//		param.put("pageSize", pageSize);
//
//		return ttFlowLogMapper.pageLogVo(param, ParameterUtil.getRowBounds(param));		
	}

	/**
	 * 邀请某人加入
	 * @param task
	 * @param userList
	 */
	@Override
	public void invitationWithTask(String taskId,List<String> invitationUserList){
		
		throw new RException("invitationWithTask方法已弃用");
		
//		EnvironmentImpl env = ((EnvironmentFactory)processEngine).openEnvironment(); 
//		try{ 
//			TaskImpl task = (TaskImpl)taskService.getTask(taskId);
//			task.setVariable(SlbpmVariableNameConstants.roles,"");
//			new SlTaskAssignHandle(invitationUserList).assign(task, task.getSuperTask().getExecution());
//		} catch (Exception e) {
//			throw new RException(e);
//		} finally{ 
//			env.close(); 
//		} 
	}
	
	/**
	 * 转向某个已有节点
	 * @param stepName
	 */
	@Override
	public void goToNode(String taskId,String nodeName,String msg){
		
		throw new RException("goToNode方法已弃用");
		
//		//1.校验任务是否存在
//		TaskImpl task = (TaskImpl)taskService.getTask(taskId);
//		if(task == null){
//			throw new RException("任务已过期或不存在");
//		}
//
//		//2.获取父任务
//		TaskImpl parentTask = task.getSuperTask();
//		EnvironmentImpl env = ((EnvironmentFactory)processEngine).openEnvironment(); 
//		try{ 
////			String newNodeName = "newTask_"+task.getName();
////			//3.完成该子任务
////			parentTask.removeSubTask(task);
////			taskService.completeTask(taskId);
////			completeAllSubTask(parentTask);
////			ExecutionImpl executionImpl  = parentTask.getExecution();
////			ActivityImpl sourceActivity  = executionImpl.getActivity();
////
////			//创建新节点,并指向原节点
////			ActivityImpl freedomActivity = executionImpl.getProcessDefinition().getActivity("freedomTask");
////			ActivityImpl newActivity = executionImpl.getProcessDefinition().createActivity(newNodeName);
////			newActivity.setActivityBehaviour(freedomActivity.getActivityBehaviour());
////			newActivity.setName(newNodeName);
////			newActivity.setType(freedomActivity.getType());
////			TransitionImpl backTransitionImpl = newActivity.createOutgoingTransition();
////			backTransitionImpl.setName("pass");
////			backTransitionImpl.setDestination(sourceActivity);
////			newActivity.addOutgoingTransition(backTransitionImpl);
////			newActivity.getVariableDefinitions();
////			//添加指向新节点的流向
////			TransitionImpl transitionImpl= sourceActivity.createOutgoingTransition();
////			transitionImpl.setName("goto_"+newNodeName);
////			transitionImpl.setDestination(newActivity);
////			sourceActivity.addOutgoingTransition(transitionImpl);
////			Map<String,String> varMap = new HashMap();
////			varMap.put(SlbpmVariableNameConstants.roles, "123");
////			taskService.completeTask(parentTask.getId(), "goto_"+newNodeName,varMap);
//			//3.完成所有子任务
//			parentTask.removeSubTask(task);
//			taskService.completeTask(taskId);
//			completeAllSubTask(parentTask);
//			ExecutionImpl executionImpl  = parentTask.getExecution();
//			ActivityImpl sourceActivity  = executionImpl.getActivity();
//			ActivityImpl targetActivity = executionImpl.getProcessDefinition().getActivity(nodeName);//getCopyofActivity(executionImpl,"freedomTask");
//			//添加指向新节点的流向
//			TransitionImpl transitionImpl= sourceActivity.createOutgoingTransition();
//			transitionImpl.setName(nodeName);
//			transitionImpl.setDestination(targetActivity);
//			
//			addVariableDef(targetActivity,"roles","55555555");
//			addVariableDef(targetActivity,"voteType","0");
//			taskService.completeTask(parentTask.getId(), nodeName);
//			
//		}catch(Exception e){
//			e.printStackTrace();
//			throw new RException("error");
//		} finally{ 
//		   env.close(); 
//		} 
	}
	
//	private VariableDefinitionImpl addVariableDef(ActivityImpl activity,String name,String value){
//		VariableDefinitionImpl var = activity.createVariableDefinition();
//		var.setName(name);
//		var.setInitExpression(Expression.create(value));
//		return var;
//		return null;
//	}
	
//	private ActivityImpl getCopyofActivity(ExecutionImpl executionImpl,String name){
//		ActivityImpl sourceActivity = executionImpl.getProcessDefinition().getActivity(name);
//		ActivityImpl targetActivity = executionImpl.getProcessDefinition().createActivity("copyof_"+name);
//		targetActivity.setActivityBehaviour(sourceActivity.getActivityBehaviour());
//		return targetActivity;
//	}
	
	/**    -----------------------     以下是非接口实现的内部方法            --------------------------*/
	/**
	 * 完成所有SubTask
	 * @param task  父节点
	 */
	private void completeAllSubTask(Task task){
		
		throw new RException("completeAllSubTask方法已弃用");
		
//		//完成所有子任务
//		if(task == null) return ;
//		Set<Task> subTaskSet = ((TaskImpl) task).getSubTasks();
//		List<Task> subTaskList = new ArrayList<Task>(subTaskSet); 
//		for(int i = subTaskList.size()-1 ; i >= 0 ; i--){
//			Task subTask = subTaskList.get(i);
//			completeAllSubTask(subTask);
//			String id = subTask.getId();
//			((TaskImpl) task).removeSubTask(subTask);
//			taskService.completeTask(id);
//			
//		}
	}

	/**
	 * 获取待办任务列表
	 */
	@Override
	public List<Map<String, Object>> searchTaskWaitting() {
		//管理员视角
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("isAdmin", true);
		return ttFlowTaskMapper.searchTaskWaitting(param);
	}

	/**
	 * 获取正在进行中的流程列表
	 */
	@Override
	public List<Map<String, Object>> searchProcessInstance() {
		return ttFlowTaskMapper.searchProcessInstance();
	}


	@Override
	public Page<TaskWaitingView> searchTaskWaittingForConsole(Map<String, Object> param) {
		HashMap<String, Object> map =  new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitution().getInstId());
		List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
		List<String> institutionList = new ArrayList<String>();
		for(TtInstitution institution:tlist){
			institutionList.add(institution.getInstId());
		}
		param.put("institutionList", institutionList);
		return this.ttFlowTaskMapper.searchTaskWaittingForConsole(param, ParameterUtil.getRowBounds(param));
	}
	
	@Override
	public Page<TaskWaitingView> searchDurationTaskWaittingForConsole(Map<String, Object> param) {
		HashMap<String, Object> map =  new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitution().getInstId());
		List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
		List<String> institutionList = new ArrayList<String>();
		for(TtInstitution institution:tlist){
			institutionList.add(institution.getInstId());
		}
		param.put("institutionList", institutionList);
		return this.ttFlowTaskMapper.searchDurationTaskWaittingForConsole(param, ParameterUtil.getRowBounds(param));
	}


	@Override
	public Integer searchDurationTaskWaittingCountForConsole(Map<String, Object> param) {
		HashMap<String, Object> map =  new HashMap<String, Object>();
		map.put("instId", SlSessionHelper.getInstitution().getInstId());
		List <TtInstitution> tlist = institutionService.searchChildrenInst(map);
		List<String> institutionList = new ArrayList<String>();
		for(TtInstitution institution:tlist){
			institutionList.add(institution.getInstId());
		}
		param.put("institutionList", institutionList);
		return ttFlowTaskMapper.searchDurationTaskWaittingCountForConsole(param);
	}
	
	
}
