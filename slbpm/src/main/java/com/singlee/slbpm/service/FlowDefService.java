package com.singlee.slbpm.service;

import com.alibaba.fastjson.JSONObject;
import com.singlee.slbpm.pojo.bo.ProcDefInfo;
import com.singlee.slbpm.pojo.vo.ProcDefInfoVo;
import com.singlee.slbpm.pojo.vo.ProcDefPropMapVo;

import java.util.List;
import java.util.Map;

/**
 * 流程定义接口
 * @author Yang Yang
 *
 */
public interface FlowDefService {
	
	/**
     * 部署模型 - 新增流程定义
     * @param model_id      模型ID
     * @param pdInfoVo
     * 		  flow_name     流程名称
     * 		  flow_type     流程类型
     * 		  is_active     是否启用
     * @param override		是否升级流程定义[覆盖老版本]
     * @return              JSONObject
     */
    public JSONObject addNewProcDef(String model_id, ProcDefInfoVo pdInfoVo, boolean override) throws Exception;
	
	/**
     * 查询某个产品类别下的流程定义列表（启用状态）
     * @param 	product_type		产品类型
     * @param	flow_type			流程类型（可选）
     * @return	List<ProcDefInfo>	流程定义列表
     * @author 	杨阳					2017.5.19
     */
	public List<ProcDefInfo> searchProcDefByPrdType(String product_type, String flow_type);

    /**
     * 根据流程定义查找对应的机构列表
     * @param	flowId				流程定义ID
     * @param	productType			产品类型
     * @return	List<String>		机构ID列表
	 * @author 						杨阳 2017.5.17
     */
	public List<String> getInstitutionsByFlowId(String flow_id, String productType);
	
	/**
     * 新建 流程定义/产品类型/所属机构 三者之间的绑定关系
     * @param	flowId				流程定义ID
     * @param	version				流程定义版本
     * @param	productType			产品类型
     * @param	lstInstCode			机构代码列表
     * @param	flow_type			流程类型
	 * @author 						杨阳 2017.5.19
     */
    public void bindProcDefProps(String flowId, String version, String productType, 
    							 List<String> lstInstCode,String flow_type);
    
    /**
     * 解除 流程定义/产品类型 之间的绑定关系 （关联机构也随之失效）
     * @param	flowId				流程定义ID
     * @param	version				流程定义版本
     * @param	productType			产品类型
	 * @author 						杨阳 2017.5.19
     */
    public void unbindProcDefProps(String flowId, String version, String productType);
	
	/**
     * 查询流程定义列表
     * @param 	flow_name     		流程名称
     * @param 	flow_type     		流程类型
     * @param 	is_active     		是否启用
     * @param 	product_type  		产品类型
     * @param 	flow_belong   		机构
     * @return	List<ProcDefInfo>	流程定义对象列表
	 * @author 	杨阳
     */
	public List<ProcDefInfo> listProcDef(String flow_name, String flow_type, String is_active, 
			 							 String product_type, String flow_belong);

    /**
     * 根据流程类型查询可用流程
     * @param 	flow_type     		流程类型
     * @return	List<ProcDefInfo>	流程定义对象列表
	 * @author 	杨阳
     */
	List<ProcDefInfo> searchProcDefByType(String flow_type);
	
	/**
     * 流程人工任务节点绑定自定义表单
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param prd_code				产品编号
     * @param form_no				自定义表单ID
     * @param form_fields_json		表单业务要素自定义JSON
     */
	public void setProcTaskForm(String flow_id, String version, String task_def_key, 
								String prd_code, String form_no, String form_fields_json) throws Exception;
	
	/**
     * 流程人工任务节点绑定审查要素列表
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param prd_code				产品编号
     * @param lst_checklist_no		绑定checklist_no
     */
	public void setProcTaskChecklist(String flow_id, String version, String task_def_key, 
									 String prd_code, List<String> lst_checktype) throws Exception;
	
	/**
     * 流程人工任务节点绑定审批触发事件
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param prd_code				产品编号
     * @param lst_event_key			绑定事件key
     */
    public void setProcTaskEvents(String flow_id, String version, String task_def_key, 
    							  String prd_code, List<String> lst_event_key) throws Exception;
    
    /**
     * 删除人工任务节点绑定的事件列表
     * @param map
     * 			task_def_key		任务节点定义key
     * 			prd_code			产品编号
     * 			flow_id				流程定义ID
     */
    public void deleteEventByTask(Map<String, Object> map);
    
    /**
     * 删除人工任务节点绑定的审查要素列表
     * @param map
     * 			task_def_key		任务节点定义key
     * 			prd_code			产品编号
     * 			flow_id				流程定义ID
     */
    public void deleteCheckListByTask(Map<String, Object> map);

    /**
     * 删除人工任务节点绑定的自定义表单列表
     * @param map
     * 			task_def_key		任务节点定义key
     * 			prd_code			产品编号
     * 			flow_id				流程定义ID
     */
    public void deleteFormByTask(Map<String, Object> map);
    
    /**
     * 流程人工任务节点绑定自定义表单
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param urls				    url
     */
    public void setProcTaskFormUrl(String flow_id, String version, String task_def_key, String prd_code, String urls) throws Exception;
    
    /**
     * 删除人工任务节点绑定的自定义界面地址
     * @param map
     * 			m: task_def_key		任务节点定义key
     * 			m: prd_code			产品编号
     * 			o: flow_id			流程定义ID
     */
    public void deleteUrlByTask(Map<String, Object> map);

    /**
	 * 根据流程编号查询关联产品
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
    public List<ProcDefPropMapVo> getPrdInfoByFlow(Map<String, Object> map);

	/**
	 * 删除流程定义（伪删除，将is_active改为0）
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
    public void deleteFlowDef(Map<String, Object> map);
}
