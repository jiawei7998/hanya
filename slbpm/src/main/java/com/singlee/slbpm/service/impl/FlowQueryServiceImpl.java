package com.singlee.slbpm.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.mapper.TaUserMapper;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TaUser;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.service.InstitutionService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.assignment.impl.MyUserTaskActivityBehavior;
import com.singlee.slbpm.mapper.*;
import com.singlee.slbpm.pojo.bo.*;
import com.singlee.slbpm.pojo.vo.ApproveUser;
import com.singlee.slbpm.pojo.vo.ProcDefPropMapVo;
import com.singlee.slbpm.pojo.vo.TdNotificationVo;
import com.singlee.slbpm.service.FlowDefService;
import com.singlee.slbpm.service.FlowLogService;
import com.singlee.slbpm.service.FlowQueryService;
import com.singlee.slbpm.service.FlowRoleService;
import com.singlee.slbpm.util.ProcessDefinitionUtils;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.behavior.ParallelMultiInstanceBehavior;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.PvmActivity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.task.TaskDefinition;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service("flowQueryService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@SuppressWarnings("unused")
public class FlowQueryServiceImpl implements FlowQueryService {

	private static Logger logger = LoggerFactory.getLogger("SLBPM");
	
	/** Activiti引擎接口 */
	@Autowired
	private ProcessEngine processEngine;
	
	/** Activiti定义接口 */
	@Autowired
	private RepositoryService repositoryService;

	/** Activiti运行时接口 */
	@Autowired
	private RuntimeService runtimeService;
	
	/** Activiti任务接口 */
	@Autowired
	private TaskService taskService;
	
	/** 审批角色管理接口 */
	@Autowired
	private FlowRoleService flowRoleService;
	
	/** 流程定义Dao */
	@Autowired
	private ProcDefInfoMapper procDefInfoMapper;
	
	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;
	
	/** 流程任务Dao */
	@Autowired
	private TtFlowTaskMapper ttFlowTaskMapper;
	
	/** 流程任务事件 Dao */
	@Autowired
	private ProcTaskEventMapper procTaskEventMapper;
	
	/** 流程任务表单 Dao */
	@Autowired
	private ProcTaskFormMapper procTaskFormMapper;
	
	@Autowired
	private ProcTaskUrlMapper procTaskUrlMapper;
	
	/** 流程任务Checklist Dao */
	@Autowired
	private ProcTaskChecklistMapper procTaskChecklistMapper;
	
	@Autowired
	private TdFlowTaskEventMapper flowTaskEventMapper;
	
	@Autowired
	private FlowDefService flowDefService;

	@Autowired
	private ProcSignSettingMapper procSignSettingMapper;
	
	@Autowired
	private ProcSignLogMapper procSignLogMapper;
	
	/** 业务流程关系dao */
	@Autowired
	private ProcDefPropMapMapper procDefPropMapMapper;

	/** 消息dao */
	@Autowired
	private TdNotificationMapper tdNotificationMapper;

	@Autowired
    private TaUserMapper taUserMapper;

    @Autowired
    private TtInstitutionMapper ttInstitutionMapper;

    @Autowired
    private InstitutionService institutionService;

    @Autowired
    private FlowLogService flowLogService;
	
	/**
	 * 通过外部流水号获取流程定义ID和流程实例ID
	 * @param 	serial_no				外部流水号（审批单号）
	 * @return 	Map<String, String>
	 * 			serial_no				外部流水号（审批单号）
	 * 			flow_id					流程定义ID
	 * 			instance_id				流程实例ID
	 * @author 							杨阳
	 */
	@Override
	public Map<String, String> getFlowIdAndInstanceIdBySerialNo(String serial_no){
		
		// 根据业务流水号(审批单号)查询对应的流程ID和版本号
		TtFlowSerialMap fsMap = getFlowSerialMap(serial_no);
		
		if(fsMap == null) { return null;}
		
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("serial_no", serial_no);
		map.put("flow_id", fsMap.getFlow_id());
		map.put("instance_id", String.valueOf(fsMap.getExecution()));

		return map;		
	}

	/**
	 * 通过外部流水号查询相关的流程信息
	 * @param serial_no
	 * @return
	 */
	@Override
	public TtFlowSerialMap getFlowSerialMap(String serial_no){
		return ttFlowSerialMapMapper.get("", serial_no, "", 0);
	}
	
	@Override
	public TtFlowSerialMap getFlowSerialMap(Map<String, Object> map){
		String flow_id = ParameterUtil.getString(map, "flow_id", "");
		String serial_no = ParameterUtil.getString(map, "serial_no", "");
		String flow_type = ParameterUtil.getString(map, "flow_type", "");
		Long execution = ParameterUtil.getLong(map, "execution", 0);
		return ttFlowSerialMapMapper.get(flow_id, serial_no, flow_type, execution);
	}

	/**
	 * 取指定版本的流程定义信息，若version为空，则取最新的流程
	 * @param 	flow_id			流程id【非空】
	 * @param 	version			版本号
	 * @return	ProcDefInfo		流程定义对象
	 */
	@Override
	public ProcDefInfo getFlowDefine(String flow_id, String version){
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		// 如果版本号为空，则去流程定义表里取最高版本
		if(StringUtil.isEmpty(version)){
			version = procDefInfoMapper.getHighVersionNo(flow_id);
		}
		
		map.put("flow_id", flow_id);
		map.put("version", version);
		
		// 根据流程ID和版本号查询特定版本的流程信息
		return procDefInfoMapper.getFlowDefine(map);
	}

	/**
	 * 根据外部审批单号查询使用的流程定义信息
	 * @param 	serial_no		外部流水号
	 * @return	ProcDefInfo		流程定义对象
	 */
	@Override
	public ProcDefInfo getFlowDefineBySerialNo(String serial_no){
		
		// 根据业务流水号(审批单号)查询对应的流程ID和版本号
		TtFlowSerialMap map = getFlowSerialMap(serial_no);
		
		if(map == null) { return null;}
		
		// 根据流程ID和版本号查询特定版本的流程详细信息
		return getFlowDefine(map.getFlow_id(), map.getVersion());
	}
	
	/**
	 * 查询待办任务预览(控制台显示)
	 * @return	
	 */
	@Override
	public Page<HashMap<String,Object>> searchTaskPreview(Map<String, Object> param, RowBounds rb){

		return ttFlowTaskMapper.searchTaskPreview(param,rb);
	}

	/**
	 * 是否是最后一个人工节点
	 * @param taskId
	 * @return
	 */
	@Override
	public boolean isLastManualStep(String taskId) {
		
		boolean bIsLastStep = false;
		ActivityImpl nextActivity = this.getNextActivity(taskId);
    	if (nextActivity.getProperty("type") == "endEvent") {
    		bIsLastStep = true;
    	}
    	return bIsLastStep;
	}
	
	/**
	 * 是否是最后一个人工节点
	 * @param taskId
	 * @return
	 */
	@Override
	public boolean isLastManualStep2(String taskId) {
		
		boolean bIsLastStep = false;
		ActivityImpl nextActivity = this.getNextActivity(taskId);
    	if (nextActivity==null) {
    		bIsLastStep = true;
    	}
    	return bIsLastStep;
	}

	/**
	 * 查询下一个人工审批任务节点的审批角色下的候选人
	 * 注：如果下个节点为互斥或并行节点，则返回空，界面上就不能指定下一个节点的审批人
	 * @param 	flowId						流程定义ID
	 * @param	stepId						任务实例ID（步骤）
	 * @param	serialNo					业务编号
	 * @return	Map<String, Object>			
	 * 										actType		节点类型 （竞签userTask/会签voteTask/其他other）
	 * 										userList	审批用户列表
	 * @author 	Yang Yang 2017/5/23
	 */
	@Override
	public Map<String, Object> getNextStepCandidatesList(String flowId, String stepId, String serialNo) {
		
		ActivityImpl nextActivity = this.getNextActivity(stepId);
		
		if(nextActivity == null){
			return new HashMap<String, Object>();
		}
		
		// 判断下一个节点的类型
		Map<String, Object> map = new HashMap<String, Object>();
		if (nextActivity.getActivityBehavior().getClass().equals(UserTaskActivityBehavior.class) 
		 || nextActivity.getActivityBehavior().getClass().equals(MyUserTaskActivityBehavior.class)) {
			map.put("actType", "userTask");
		}
		else if (nextActivity.getActivityBehavior().getClass().equals(ParallelMultiInstanceBehavior.class)) {
			map.put("actType", "voteTask");
		}
		else {
			map.put("actType", "other");
		}
		
		List<ApproveUser> userList = new ArrayList<ApproveUser>();
		
		// 获取下一个人工任务节点定义
		if (nextActivity.getActivityBehavior().getClass().equals(UserTaskActivityBehavior.class) 
		 || nextActivity.getActivityBehavior().getClass().equals(MyUserTaskActivityBehavior.class)
		 || nextActivity.getActivityBehavior().getClass().equals(ParallelMultiInstanceBehavior.class)) {

			TaskDefinition td = (TaskDefinition) nextActivity.getProperty("taskDefinition");
		
			// 获得下一个人工任务节点的审批角色
			List<String> lst_group = new ArrayList<String>();
			for(Expression group_id : td.getCandidateGroupIdExpressions()) {
				if (group_id != null && StringUtils.isNoneEmpty(group_id.getExpressionText())) {
					lst_group.add(group_id.getExpressionText());
				}
			}

			String roles = StringUtils.join(lst_group, ",");
            List<ApproveUser> list = flowRoleService.getUserMapByRole(roles, null, SlSessionHelper.getUserId());
            //edit 20171031 -机构默认改为业务发起人所属机构
            List<Map<String,Object>> flowLog = flowLogService.getFlowlogBySerialNo(serialNo);
            List<String> instList = new ArrayList<String>();
            if(flowLog != null && flowLog.size() > 0){
                String spon_user = ParameterUtil.getString(flowLog.get(0),"Assignee","");
                if(StringUtil.isEmpty(spon_user)){
                	instList = institutionService.searchChildrenInstList(SlSessionHelper.getInstitutionId());
                }else{
                	String spon_inst = taUserMapper.selectUser(spon_user).getInstId();
                	instList = institutionService.searchChildrenInstList(spon_inst);
                }
            }else{
            	instList = institutionService.searchChildrenInstList(SlSessionHelper.getInstitutionId());
            }
            //end
            for (ApproveUser user: list) {
                TaUser taUser = taUserMapper.selectUser(user.getUserId());
                if(null != taUser){
	                Map<String, Object> param = new HashMap<String, Object>();
	                param.put("instId", taUser.getInstId()==null?"X":taUser.getInstId());
	                TtInstitution ttInstitution = ttInstitutionMapper.selectByPrimaryKey(param);
	                if(null != ttInstitution) {
		                if(instList.contains(taUser.getInstId())
		                        || Arrays.asList("0", "9").contains(ttInstitution.getInstType())) {
		                    userList.add(user);
		                }}
                }
            }
        }
		
		map.put("userList", userList);
		
		return map;
	}
	
	/**
	 * 获取流程内所有人工任务节点
	 * @param processDefinitionId			流程定义ID
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Map<String, Object>> getFlowActivities(String processDefinitionId) throws Exception {
		
		List<ActivityImpl> activitiesActivityImpls = ProcessDefinitionUtils.getActivities(processEngine, processDefinitionId);
		
		List<Map<String, Object>> userTasks = new ArrayList<Map<String, Object>>();
		for (ActivityImpl activity : activitiesActivityImpls) {
			if(!activity.getId().startsWith("sid-")) {continue;}
			if ("userTask".equalsIgnoreCase(String.valueOf(activity.getProperty("type")))) {
				
				TaskDefinition td = (TaskDefinition) activity.getProperty("taskDefinition");
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("Id", td.getKey());
				map.put("Name", td.getNameExpression());
				
				// 排除会签和加签节点
				if (td.getKey().startsWith("sid-")) {
					userTasks.add(map);
				}
			}
		}
		
		return userTasks;
	}

	/**
	 * 获取流程人工任务节点上的绑定表单ID
	 * @param procDefId						流程定义ID
	 * @param taskDefKey					人工任务节点key
	 * @param prdCode						产品编号
	 * @return
	 * @throws Exception
	 */
	@Override
	public ProcTaskForm getUserTaskForm(String procDefId, String taskDefKey, String prdCode) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("flow_id", procDefId);
		map.put("task_def_key", taskDefKey);
		map.put("prd_code", prdCode);
		
		List<ProcTaskForm> lst_form_no = procTaskFormMapper.getProcTaskForm(map);
		
		if (lst_form_no.size() > 0) {
			return lst_form_no.get(0);
		}
		else {
			return null;
		}
	}
	
	/**
	 * 获取流程人工任务节点上的绑定url
	 * @param procDefId						流程定义ID
	 * @param taskDefKey					人工任务节点key
	 * @param prdCode						产品编号
	 * @return
	 * @throws Exception
	 */
	@Override
	public ProcTaskUrl getUserTaskUrl(String procDefId, String taskDefKey, String prdCode, String taskId) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("prd_code", prdCode);
		
		if(StringUtils.isNoneEmpty(taskId)){
			TaskEntity entity = ProcessDefinitionUtils.getTaskEntity(processEngine, taskId);
			map.put("flow_id", entity.getProcessDefinitionId());
			map.put("task_def_key", entity.getTaskDefinitionKey());
		}else{
			map.put("flow_id", procDefId);
			map.put("task_def_key", taskDefKey);
		}
		
		List<ProcTaskUrl> lst_form_no = procTaskUrlMapper.getProcTaskUrl(map);
		
		if (lst_form_no.size() > 0) {
			return lst_form_no.get(0);
		}
		else {
			return null;
		}
	}

	/**
	 * 获取流程人工任务节点上绑定的审查要素
	 * @param procDefId						流程定义ID
	 * @param taskDefKey					人工任务节点key
	 * @param prdCode						产品编号
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, String> getUserTaskChecklists(String procDefId, String taskDefKey, String prdCode) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("flow_id", procDefId);
		map.put("task_def_key", taskDefKey);
		map.put("prd_code", prdCode);
		
		Map<String, String> lst_procTaskChecklist = procTaskChecklistMapper.getProcTaskChecklists(map);

		return lst_procTaskChecklist;
	}

	/**
	 * 获取流程人工任务节点上的绑定事件
	 * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<TdFlowTaskEvent> getUserTaskEvents(Map<String, Object> map) throws Exception {
		return flowTaskEventMapper.getFlowTaskEventSelect(map);
	}
	
	/**
	 * 查询流程人工任务节点的未选事件列表
	 * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<TdFlowTaskEvent> getFlowTaskEventUnselect(Map<String, Object> map) {
		return flowTaskEventMapper.getFlowTaskEventUnselect(map);
	}

	/**
	 * 查询所有事件列表
	 */
	@Override
	public Page<TdFlowTaskEvent> getFlowTaskEvent(Map<String, Object> map) {
		return flowTaskEventMapper.getFlowTaskEventPage(map, ParameterUtil.getRowBounds(map));
	}
	
	/**
	 * 查询已选事件列表
	 */
	@Override
	public List<TdFlowTaskEvent> getFlowTaskEventSelectByTask(Map<String, Object> map) {
		String task_id = ParameterUtil.getString(map, "task_id", "");
		String prdNo = ParameterUtil.getString(map, "prdNo", "");
		if(null != StringUtils.trimToNull(task_id)){
			Task task = taskService.createTaskQuery().taskId(task_id).singleResult();
			String task_def_key = task.getTaskDefinitionKey();
			map.put("task_def_key", task_def_key);
			map.put("prdNo", prdNo);
		}else{
			map.put("task_def_key", "wknfdjrwanglrwanvl");
		}
		return flowTaskEventMapper.getFlowTaskEventSelectByTask(map);
	}
	
	/**
	 * 查询已选事件列表
	 */
	@Override
	public List<TdFlowTaskEvent> getFlowTaskEventSelect(Map<String, Object> map) {
		return flowTaskEventMapper.getFlowTaskEventSelect(map);
	}
	
	@Override
	public void addFlowTaskEvent(Map<String, Object> param) {
		flowTaskEventMapper.addFlowTaskEvent(param);
	}

	@Override
	public void updateFlowTaskEvent(Map<String, Object> param) {
		flowTaskEventMapper.updateFlowTaskEvent(param);
	}

	@Override
	public void deleteFlowTaskEvent(Map<String, Object> param) {
		flowTaskEventMapper.deleteFlowTaskEvent(param);
	}

    /**
     * 获得当前任务节点的审批角色
     * @param activityImpl          任务实例
     * @return
     */
	@Override
	public List<String> getUserTaskRole(ActivityImpl activityImpl) {
		
		TaskDefinition td = (TaskDefinition) activityImpl.getProperty("taskDefinition");

		List<String> lst_group_id = new ArrayList<String>();
		for(Expression group_id : td.getCandidateGroupIdExpressions()) {
			if (group_id != null && StringUtils.isNoneEmpty(group_id.getExpressionText())) {
				lst_group_id.add(group_id.getExpressionText());
			}
		}
		
		return lst_group_id;
	}

	/**
	 * 根据流程关联属性查询唯一的流程定义ID，取得流程定义第一个节点的配置项
	 * 目前只返回该节点的审批角色和审查要素信息
	 * @param product_no	产品编码
	 * @param flow_type		流程类型
	 * @param inst_id		机构ID
	 * @return map(lst_flow_role, lst_checklist)
	 */
	@Override
	public Map<String, Object> getFirstUserTaskSetting(String product_no, String flow_type, String inst_id) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();

		String flow_id = this.getFlowIdByProps(product_no, flow_type, inst_id);

		if (StringUtils.isNotEmpty(flow_id)) {

			ActivityImpl first_activity = this.getFirstActivity(flow_id);

			String act_id = first_activity.getId();

			// 获取节点审查要素
			Map<String,String> lst_checklist = this.getUserTaskChecklists(flow_id, act_id, product_no);

			// 获取节点审批角色
			List<String> lst_flow_role = this.getUserTaskRole(first_activity);
			map.put("lst_flow_role", lst_flow_role);
			map.put("lst_checklist", lst_checklist);
		}

		return map;
	}

	/**
	 * 返回指定人工任务节点的审批角色和审查要素信息
	 * @param task_id	任务ID
	 * @return map(lst_flow_role, lst_checklist)
	 */
	@Override
	public Map<String, Object> getUserTaskSetting(String task_id, String product_no) throws Exception {

		Task task = taskService.createTaskQuery().taskId(task_id).singleResult();
		String flow_id = task.getProcessDefinitionId();
		String task_def_key = task.getTaskDefinitionKey();
		
		ActivityImpl activity = ProcessDefinitionUtils.getActivity(processEngine, flow_id, task_def_key);

		// 获取节点审查要素
		Map<String, String> lst_checklist = this.getUserTaskChecklists(flow_id, task_def_key, product_no);
		
		// 获取节点审批角色
		List<String> lst_flow_role = this.getUserTaskRole(activity);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("lst_flow_role", lst_flow_role);
		map.put("lst_checklist", lst_checklist);
		
		return map;	
	}
	
	/**
	 * 返回流程会签节点设置
	 * @param task_def_key		节点定义key
	 * @return
	 * @throws Exception
	 */
	@Override
	public ProcSignSetting getSignSetting(String task_def_key) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("task_def_key", task_def_key);
		return procSignSettingMapper.getProcSignSetting(map);
	}

	/**
	 * 获取会签节点投赞成票数
	 * @param proc_inst_id		流程实例ID
	 * @param task_def_key		会签节点KEY
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getAgreeVoteCount(String proc_inst_id, String task_def_key) throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("proc_inst_id", proc_inst_id);
		map.put("task_def_key", task_def_key);
		map.put("result", "1");
		
		return procSignLogMapper.getVoteCount(map);
	}

	/**
	 * 获取会签节点投反对票数
	 * @param proc_inst_id		流程实例ID
	 * @param task_def_key		会签节点KEY
	 * @return
	 * @throws Exception
	 */
	@Override
	public int getRefuseVoteCount(String proc_inst_id, String task_def_key) throws Exception {	

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("proc_inst_id", proc_inst_id);
		map.put("task_def_key", task_def_key);
		map.put("result", "0");
		
		return procSignLogMapper.getVoteCount(map);
	}

	/**
	 * 根据任务ID获得任务节点定义KEY
	 * @param 	task_id					任务ID
	 * @return 	String					任务节点定义KEY
	 * @author 							杨阳
	 */
	@Override
	public String getTaskKeyByTaskId(String task_id) {
		
		String task_def_key = "";
		
		try {
			TaskEntity entity = ProcessDefinitionUtils.getTaskEntity(processEngine, task_id);
			task_def_key = entity.getTaskDefinitionKey();
		}
		catch(Exception e) {
			task_def_key = "";
		}
		
		return task_def_key;
	}
	
	/**
	 * 查询业务流程定义
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
	@Override
	public List<ProcDefPropMapVo> getProcDefPropMap(Map<String, Object> map) {
		List<ProcDefPropMapVo> result = procDefPropMapMapper.getProcDefPropMap(map);
		return result;
	}
	
	/**
	 * 查询消息
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
	@Override
	public List<TdNotificationVo> getNotification(Map<String, Object> map) {
		List<TdNotificationVo> result = tdNotificationMapper.getNotification(map);
		return result;
	}
	
	/**
	 * 获得下个节点
	 */
	@Override
	public ActivityImpl getNextActivity(String taskId) {

		// 获得当前任务节点
		TaskEntity taskEntity = ProcessDefinitionUtils.getTaskEntity(processEngine, taskId);
		
		if (taskEntity == null) {return null;}
		
		// 根据任务获取当前流程实例ID以及当前流程实例的最新节点ID
		ExecutionEntity execution = ProcessDefinitionUtils.getExecutionEntity(processEngine, taskEntity.getExecutionId());
		String activityId = execution.getActivityId();		
		
        // 取得当前节点的后续分支
		ActivityImpl activityImpl = ProcessDefinitionUtils.getActivity(processEngine, taskEntity.getProcessDefinitionId(), activityId);
        List<PvmTransition> outTransitions = activityImpl.getOutgoingTransitions();
		
		// 如果当前节点的后续分支只有一条，则取得那个唯一的后续节点
        PvmActivity nextActivity = null;
		if (outTransitions.size() == 1) {			
			nextActivity = outTransitions.get(0).getDestination();
		}
		
		return (ActivityImpl)nextActivity;
	}

    /**
     * 查询流程定义的第一个任务节点
     */
    @Override
    public ActivityImpl getFirstActivity(String flow_id) {

        // 获得当前流程的定义模型
        ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity)
                ((RepositoryServiceImpl)repositoryService)
                        .getDeployedProcessDefinition(flow_id);

        // 获取流程开始节点
        ActivityImpl startActivityImpl = null;
        List<ActivityImpl> activitiList = processDefinition.getActivities();
        for(ActivityImpl act : activitiList){
            Map<String, Object> prop = act.getProperties();
            if ("startEvent".equals(String.valueOf(prop.get("type")))){
                startActivityImpl = act;
            }
        }

        // 获取开始节点后的唯一节点
        ActivityImpl firstActivity = (ActivityImpl)startActivityImpl.getOutgoingTransitions().get(0).getDestination();

        return firstActivity;
    }
	
	/**
	 * 根据流程关联属性查询唯一的流程定义ID
	 */
	private String getFlowIdByProps(String product_no, String flow_type, String inst_id) {
		
		List<ProcDefInfo> list = 
				flowDefService.listProcDef(null, flow_type, null, product_no, inst_id);
		
		if (list.size() == 0) {
//			throw new RException(
//					String.format("没有匹配到合适的流程定义，产品编码=%s，流程类型=%s，机构ID=%s", 
//					product_no, flow_type, inst_id));
			return "";
		}
		else {
			
			return list.get(0).getFlow_id();	
		}
	}
}
