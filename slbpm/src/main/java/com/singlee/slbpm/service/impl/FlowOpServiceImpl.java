package com.singlee.slbpm.service.impl;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.ctrl.RuntimeActivityCtrlService;
import com.singlee.slbpm.ctrl.RuntimeActivityCtrlServiceFactory;
import com.singlee.slbpm.dict.DictConstants;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.externalInterface.TaskEventInterface;
import com.singlee.slbpm.mapper.*;
import com.singlee.slbpm.pojo.bo.ProcSignLog;
import com.singlee.slbpm.pojo.bo.ProcSuspensionLog;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import com.singlee.slbpm.service.FlowLogService;
import com.singlee.slbpm.service.FlowOpService;
import com.singlee.slbpm.service.FlowQueryService;
import com.singlee.slbpm.service.FlowRoleService;
import com.singlee.slbpm.util.ProcessDefinitionUtils;
import com.singlee.slbpm.util.SlbpmVariableNameConstants;
import org.activiti.engine.*;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.behavior.ParallelMultiInstanceBehavior;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


@Service("FlowOpService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@SuppressWarnings("unused")
public class FlowOpServiceImpl implements FlowOpService {

	private static Logger logger = LoggerFactory.getLogger("SLBPM");
	/** 定义锁 */
	private static Lock lock = new ReentrantLock();
	
	/** Activiti引擎接口 */
	@Autowired
	private ProcessEngine processEngine;
		
	/** Activiti实例接口 */
	@Autowired
	private RuntimeService runtimeService;

	/** Activiti任务接口 */
	@Autowired
	private TaskService taskService;

	/** Activiti对象接口 */
    @Autowired
    protected RepositoryService repositoryService;

	/** 流程查询接口 */
	@Autowired
	private FlowQueryService flowQueryService;

    /** 流程权限接口 */
	@Autowired
    private FlowRoleService flowRoleService;
	
	/** 流程定义Dao */
	@Autowired
	private ProcDefInfoMapper procDefInfoMapper;
	
	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;
	
	/** 流程日志Dao */
	@Autowired
	private TtFlowLogMapper ttFlowLogMapper;
	
	/** 流程运行时操作工厂接口 */
	@Autowired
    private RuntimeActivityCtrlServiceFactory runtimeActivityCtrlServiceFactory;
	
	@Autowired
	private ProcSuspensionLogMapper procSuspensionLogMapper;
	
	@Autowired
	private ProcSignLogMapper procSignLogMapper;
	
	/** 流程日志service */
	@Autowired
	private FlowLogService flowLogService;
	
	/** Activiti历史接口 */
	@Autowired
	private HistoryService historyService;
	
	
	

	
	public static  final StringBuffer startNewFlowUser = new StringBuffer();
	
	public static  final Map<String, Object> flowSerialCache =new HashMap<String, Object>();
	
	/**
	 * 创建一个新的审批实例
	 * @param 	user_id					用户id(提交人)
	 * @param 	flow_id					流程定义ID
	 * @param 	flow_type				流程类型
	 * @param 	serial_no				编号,通常为外部审批单号
	 * @param 	specific_user 			指定用户
	 * @param 	need_risk_approve		是否需要风险预审
	 * @param 	is_pre_auth_pass  		是否预授权通过
	 * @param 	limit_level				限额类型
	 * @param 	status_change_listener	审批流程状态变更监听service，该service需要交spring管理，
	 * 									同时需要实现[com.joyard.jir.service.flow.FlowEndListener]接口
	 * @param 	msg						审批信息
	 * @param 	prd_no					产品编号
	 * @return 	processInstance  		流程实例
	 * @author 	杨阳 					updated @ 2017.5.7
	 */
	@Override
	public ProcessInstance startNewFlowInstance(String user_id, String flow_id, String flow_type, String serial_no, String specific_user, boolean need_risk_approve, boolean is_pre_auth_pass,
												String limit_level, String status_change_listener, String msg, String prd_no) throws Exception{
		ProcessInstance processInstance =null;
		try {
			//1.1 提交前根据流程第一个节点的审批角色设置，来判断当前用户是否可以发起流程
			 if (!flowRoleService.checkFlowInitiator(user_id, flow_id)) {
		            throw new RException("当前用户没有该流程的发起权限");
		        }
			 //1.2 查询流程与单据匹配情况
			 TtFlowSerialMap flowSerialMap = flowQueryService.getFlowSerialMap(serial_no);
			 //1.3 因为在发起阶段,如果存在则说明改交易之前已经发起过,直接异常即可
			 if(flowSerialMap != null){
				 throw new RException("当前交易已发起审批,请勿重复提交");
				}
			// 1.4根据外部流水号，标记为历史日志
			ttFlowLogMapper.remarkToHistory(serial_no);
			//1.5取指定流程的最高版本号
			String version = procDefInfoMapper.getHighVersionNo(flow_id);
			//1.6 建立流程和流水号的映射关系
			TtFlowSerialMap flowSerMap = new TtFlowSerialMap();
			flowSerMap.setFlow_id(flow_id);
			flowSerMap.setFlow_type(flow_type);
			flowSerMap.setSerial_no(serial_no);
			flowSerMap.setVersion(version);
			flowSerMap.setApproved(DictConstants.YesNo.NO);
			flowSerMap.setStatus_change_listener(status_change_listener);
			flowSerMap.setPrd_no(prd_no);
			//add by wangchen start   DANGER
			//防止出现ora-00001 暂定先行查询，若已有记录则删除
//			TtFlowSerialMap checkSerial = ttFlowSerialMapMapper.get(null, serial_no, null, 0);
//			if(checkSerial != null){
//				ttFlowSerialMapMapper.deleteBySerial(serial_no);
//			}
			//1.7 保存流程与业务单号关联关系
			ttFlowSerialMapMapper.insert(flowSerMap);
			//1.8 关联关系放入内存map中 
			flowSerialCache.put("flowSerMap", flowSerMap);
			// 1.9 组装流程参数
			Map<String, Object> variableMap = new HashMap<String, Object>();
			// 指定审批人
			variableMap.put(SlbpmVariableNameConstants.specificUser, specific_user);
			// 上一步审批人
			//variableMap.put(SlbpmVariableNameConstants.lastOperator, user_id);
			// 业务流水号
			variableMap.put(SlbpmVariableNameConstants.serialNo, serial_no);
			// 业务回调实现类
			variableMap.put(SlbpmVariableNameConstants.callBackBeanName, status_change_listener);
			// 流程发起人
			variableMap.put(SlbpmVariableNameConstants.Initiator, user_id);
			// FIXME - 2017/7/24 - yangyang - 初始化准入资格变量
			// 有些过时的XML里可能还配置着${Qualification == 0/1}
			// 所以不初始化该变量的话，执行到此表达式就会报错
			variableMap.put(SlbpmVariableNameConstants.Qualification, 0);
			// 初始化变量
			variableMap.put(SlbpmVariableNameConstants.CreditUsed, 0);
			variableMap.put(SlbpmVariableNameConstants.CreditReleased, 0);
			variableMap.put(SlbpmVariableNameConstants.ContractReview, 10);
			variableMap.put(SlbpmVariableNameConstants.ContinueAsPerAdvice, 0);
	        variableMap.put(SlbpmVariableNameConstants.VoteJudgement, 20);
	        
			// 2.0调用引擎接口启动流程
			processInstance =  runtimeService.startProcessInstanceById(flow_id, serial_no, variableMap);
			String processInstanceId = processInstance.getId();
			// 2.1 获取回调方法
			SlbpmCallBackInteface listener = SpringContextHolder.getBean(status_change_listener);
			// 自动跳过第一个节点（经办岗）
			Task task = taskService.createTaskQuery().processInstanceId(processInstanceId).active().singleResult();
			String task_id = task.getId();
			//受领人领取任务后更新到hi表
			taskService.claim(task_id, user_id);
			//记录发起人
			FlowOpServiceImpl.startNewFlowUser.append(user_id);
			// ATT - 后续代码没有用到task实例，但是查询动作可以刷新缓存，以便记录日志时能把最新的审批人信息也保存进去
			taskService.createTaskQuery().taskId(task_id).singleResult();
			
			// 2.2 如果是第一个审批节点（业务状态为待审批），那么把状态置为审批中
			if(DictConstants.YesNo.NO.equalsIgnoreCase(flowSerMap.getApproved())){
				//回调方法中接受到审批中状态
				listener.statusChange(flowSerMap.getFlow_type(),
									  flowSerMap.getFlow_id(),
									  flowSerMap.getSerial_no(),
									  DictConstants.ApproveStatus.Approving,
									  DictConstants.FlowCompleteType.Continue);
				flowSerMap.setApproved(DictConstants.YesNo.YES);
			}
			// 任务添加审批备注【提交审批】
			taskService.addComment(task_id, processInstanceId, "提交审批");
			//2.3 额度回调方法，额度占用（自动），自动的额度占用不走审批，直接占用
			TaskEventInterface taskEventInterface = SpringContextHolder.getBean(status_change_listener);
			//taskEventInterface.LimitOccupy(serial_no, prd_no);
			//2.4定义其他额度处理事件
			Map<String, Object> map = taskEventInterface.fireEvent(flow_id, serial_no, task_id, task.getTaskDefinitionKey());
			// 如果触发额度自动占用事件，并且返回成功，则在流程变量里保存这个状态
			if (map != null && map.containsKey("quotaAutoOccupy") && map.get("quotaAutoOccupy").equals(true)) {
				runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditUsed, 1);
				runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditReleased, 0);
			}
			// 如果触发额度自动释放事件，并且返回成功，则在流程变量里保存这个状态
			if (map != null && map.containsKey("quotaAutoRelease") && map.get("quotaAutoRelease").equals(true)) {
				runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditUsed, 0);
				runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditReleased, 1);
			}
			//2.5 完成当前任务节点
			taskService.complete(task_id);
			//2.6 更新流程实例ID到映射关系表
			flowSerMap.setExecution(Long.valueOf(processInstanceId));
			ttFlowSerialMapMapper.updateByPrimaryKey(flowSerMap);
			//2.7 销毁发起人
			FlowOpServiceImpl.flowSerialCache.remove("flowSerMap");
			FlowOpServiceImpl.startNewFlowUser.delete(0, startNewFlowUser.length());
		} catch (Exception e) {
			JY.error(serial_no+",流程异常处理", e);
			JY.raiseRException(serial_no+",流程异常处理", e);
		}
		return processInstance;		
	}
	
	/**
	 * 个人审批操作
	 * @param 	userId			用户ID
	 * @param 	taskId			任务ID
	 * @param 	completeType	审批完成类型,字典项000046，
	 * 							SlbpmDict.FlowCompleteType，【-1:不通过  0：通过    1：继续 】
	 * @param 	specific_user	指定下一个人工节点的审批人
	 * @param 	msg				备注信息
	 * @param 	back_to			回退的目标节点
	 * @author 	杨阳 			updated @ 2017.5.7
	 */
	@Override
	public void completePersonalTask(String userId, String taskId, String serial_no, String completeType,String specific_user, String msg, String back_to) throws Exception {
		try {
			// 1.1 校验任务是否存在
			TaskQuery taskQuery = taskService.createTaskQuery().taskId(taskId);
			if(taskQuery == null){			
				throw new RException("任务已过期或不存在");
			}
			//1.2 检查当前流程是否已被挂起
			Task task = taskQuery.singleResult();
			if (task == null || task.isSuspended()) {
				throw new RException("当前任务不存在或任务已被挂起【多次提交】，无法执行任务审批操作!");
			}
			// 1.3 校验当前用户userId是否在当前任务的审批角色内【勿删,防止越权审批】
	        if (!flowRoleService.checkTaskAccess(userId, task) && !StringUtils.equals(completeType,DictConstants.FlowCompleteType.Undo)) {
	            throw new RException("当前用户没有该任务的审批权限");
	        }
			// 1.4 如果当前是竞签任务，则审批人在审批前需要先签收任务
			if (task.getAssignee() == null || StringUtils.isEmpty(task.getAssignee())) {
				// 签收任务
				taskService.claim(taskId, userId);
				// 刷新task信息
				task = taskService.createTaskQuery().taskId(taskId).singleResult();
			}
			// 1.5 如果审批人已提前指派，则判断当前登录用户是否为审批人，如果当前用户与任务审批人不符，说明该任务已经被转办，无法继续审批
			if(!userId.equals(task.getAssignee())){			
				throw new RException("该任务当前审批人不属于" + userId);
			}
			// 1.6 获取当前流程实例ID、定义ID、任务节点KEY
			String processInstanceId = task.getProcessInstanceId();
			String flow_id = task.getProcessDefinitionId();
			String task_def_key = task.getTaskDefinitionKey();
			String task_name = task.getName();
			// 1.7 根据流程实例ID找到业务映射记录【包括回调接口、业务流水号、流程类型】
			TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, Long.valueOf(processInstanceId));
			String status_change_listener = flowSerMap.getStatus_change_listener();
			String flow_type = flowSerMap.getFlow_type();
			JY.info(String.format("开始处理单号[%s]；审批类型为[%s]；回调接口为[%s]的交易······", serial_no,flow_type,status_change_listener));
			// 1.8 获取回调接口，状态更新为审批中；等待客制化处理。
			SlbpmCallBackInteface listener = SpringContextHolder.getBean(status_change_listener);
			listener.statusChange(flow_type, flow_id, serial_no, DictConstants.ApproveStatus.Approving, completeType);
			// 1.9设置审批是否已审批过 
			flowSerMap.setApproved(DictConstants.YesNo.YES);
			ttFlowSerialMapMapper.updateByPrimaryKey(flowSerMap);
			// 2.0 更新当前审批人信息到流程实例中
			runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.lastOperator, userId);
			// 2.1 判断是否会签逻辑指定多个用户，则肯定是会签逻辑
			if (specific_user.contains(",")) {
				ActivityImpl act = flowQueryService.getNextActivity(taskId);			
				String next_task_def_key = act.getId();
				// 如果是下一步是会签节点，则指定会签的投票人选
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("task_def_key", next_task_def_key);
				map.put("sign_user", specific_user);
				runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CountersignUser, map);
			}else {
				// 如果是普通节点，则指定下一步审批人
				runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.specificUser, specific_user);
			}
			// 2.2 判断当前是否是最后一个审批节点
			boolean bIsLastStep = flowQueryService.isLastManualStep(taskId);
			Map<String, Object> result = new HashMap<String, Object>();
			ActivityImpl activity = ProcessDefinitionUtils.getActivity(processEngine, flow_id, task_def_key);
			// ***会签逻辑***
			if (activity.getActivityBehavior().getClass().equals(ParallelMultiInstanceBehavior.class)) {
				// completeType: 1同意|-1反对|0弃权
				this.insertVoteLog(processInstanceId, taskId, task_def_key, task_name, userId, completeType);			
				taskService.addComment(taskId, processInstanceId, "已投票：" + msg);
				taskService.complete(taskId, result);
			}else {
				// ***正常审批逻辑***
				if(DictConstants.FlowCompleteType.Pass.equals(completeType)){
					// 流程直接审批通过	
					throw new RException("目前流程不支持直接审批通过");
				}else if(DictConstants.FlowCompleteType.Refuse.equals(completeType) || 
						DictConstants.FlowCompleteType.Undo.equals(completeType)){
					String status = DictConstants.FlowCompleteType.Undo.equals(completeType)?
									DictConstants.ApproveStatus.Cancel:
									DictConstants.ApproveStatus.ApprovedNoPass;
					// 更新最新审批状态 -> 审批拒绝
					listener.statusChange(flow_type, flow_id, serial_no, status, completeType);
					// 保存审批意见 - 流程拒绝
					taskService.addComment(taskId, processInstanceId, 
							StringUtils.isNotEmpty(msg) ? "流程拒绝" + "-" + msg : "流程拒绝");
					// 流程撤销
					runtimeService.deleteProcessInstance(processInstanceId, msg);
				}else if(DictConstants.FlowCompleteType.Continue.equals(completeType) 
						|| DictConstants.FlowCompleteType.ContractSuggestion.equals(completeType)
						|| DictConstants.FlowCompleteType.ContractChangeRequired.equals(completeType)
						|| DictConstants.FlowCompleteType.ContinueAsPerAdvice.equals(completeType)){
						// 完成当前任务前，触发任务事件
						TaskEventInterface taskEventInterface = SpringContextHolder.getBean(status_change_listener);
						Map<String, Object> map = taskEventInterface.fireEvent(flow_id, serial_no, taskId, task.getTaskDefinitionKey());
						// 如果触发额度自动占用事件，并且返回成功，则在流程变量里保存这个状态 
						if (map != null && map.containsKey("quotaAutoOccupy") && map.get("quotaAutoOccupy").equals(true)) {
							runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditUsed, 1);
							runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditReleased, 0);
						}
						// 如果触发额度自动释放事件，并且返回成功，则在流程变量里保存这个状态
						if (map != null && map.containsKey("quotaAutoRelease") && map.get("quotaAutoRelease").equals(true)) {
							runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditUsed, 0);
							runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.CreditReleased, 1);
						}
					// 当前节点通过 - 进入下一步
					taskService.addComment(taskId, processInstanceId, msg);
					taskService.complete(taskId, result);
				}else if(DictConstants.FlowCompleteType.Back.equals(completeType)){
					if(StringUtils.isEmpty(back_to)){
						listener.statusChange(flow_type, flow_id, serial_no, DictConstants.ApproveStatus.Approving, completeType);
						// 保存审批意见 - 驳回
						taskService.addComment(taskId, processInstanceId,StringUtils.isNotEmpty(msg) ? "驳回" + "-" + msg : "驳回");
						// 驳回 - 退回上一步
						this.rejectTask(taskId);
					}else {
						// 退回至特定步骤
						// yangyang - 2017/6/6 - 暂时不实现自由回退
	                    throw new RException("流程暂时不支持自由回退功能");
					}			
				}else if(DictConstants.FlowCompleteType.Redo.equals(completeType)){
					// 保存审批意见 - 退回发起
					taskService.addComment(taskId, processInstanceId,StringUtils.isNotEmpty(msg) ? "退回发起" + "-" + msg : "退回发起");
					// 流程退回到发起人
					this.returnTask(taskId);
				}else if (DictConstants.FlowCompleteType.Delegate.equals(completeType)){
					// 保存审批意见 - 加签
					taskService.addComment(taskId, processInstanceId,StringUtils.isNotEmpty(msg) ? "加签" + "-" + msg : "加签");
					// 加签至指定审批人
					this.insertTaskBefore(taskId, specific_user.split(","));				
				}else {			
					throw new RException("处理方式错误");
				}
			}
		} catch (Exception e) {
			JY.error("流程审批处理失败", e);
			JY.raiseRException("流程异常处理", e);
		}
	}
	
	/**
	 * 任务驳回
	 * @param 	taskId					任务ID
	 * @throws 	Exception
	 */
	private void rejectTask(String taskId) throws Exception {
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		// 根据流程实例ID初始化运行时对象控制服务
		RuntimeActivityCtrlService runtimeActivityCtrlService = 
				runtimeActivityCtrlServiceFactory.create(task.getProcessInstanceId());
		
		runtimeActivityCtrlService.moveBack();
	}
	
	/**
	 * 任务退回（到发起人）
	 * @param 	taskId					任务ID
	 * @throws 	Exception
	 */
	private void returnTask(String taskId) throws Exception {
		
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		
		// 获得当前流程的定义模型
		ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity)
													((RepositoryServiceImpl)repositoryService)
													.getDeployedProcessDefinition(task.getProcessDefinitionId());

		// 获取流程第一个人工任务节点（发起人）
		String targetTaskDefinitionKey = "";
        List<ActivityImpl> activitiList = processDefinition.getActivities();
        for(ActivityImpl act : activitiList){
        	Map<String, Object> prop = act.getProperties();
        	if ("startEvent".equals(String.valueOf(prop.get("type")))){
        		targetTaskDefinitionKey = act.getId();
        	}
        }
		
		// 根据流程实例ID初始化运行时对象控制服务
		RuntimeActivityCtrlService runtimeActivityCtrlService = 
				runtimeActivityCtrlServiceFactory.create(task.getProcessInstanceId());
		
		runtimeActivityCtrlService.moveTo(targetTaskDefinitionKey);
	}
	
	/**
	 * 向前加签（多人）
	 * 被加签人处理结束后返回加签人
	 * @param 	taskId					任务ID
	 * @param 	assignees				加签指派人（可多选）
	 * @throws 	Exception
	 */
	private void insertTaskBefore(String taskId, String... assignees) throws Exception {

		// 获得当前任务节点
		Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
		
		// 根据流程实例ID初始化运行时对象控制服务
		RuntimeActivityCtrlService runtimeActivityCtrlService = 
				runtimeActivityCtrlServiceFactory.create(task.getProcessInstanceId());

		String targetTaskDefinitionKey = task.getTaskDefinitionKey();
		
		runtimeActivityCtrlService.insertTasksBefore(targetTaskDefinitionKey, assignees);
	}

	/**
	 * 挂起流程实例
	 * @param 	processInstanceId		流程实例ID
	 * @param 	userId					操作员ID
	 * @param 	reason					变更理由
	 * @throws 	Exception
	 */
	@Override
	public void suspendFlowByTaskId(String processInstanceId, String userId, String reason) throws Exception {
		
		ProcessInstance pi = runtimeService.createProcessInstanceQuery()
										   .processInstanceId(processInstanceId)
										   .singleResult();
		
		if (!pi.isSuspended()) {
			runtimeService.suspendProcessInstanceById(processInstanceId);
			this.insertSuspensionLog(pi.getBusinessKey(), userId, "正常->挂起", reason);
		}
		else {
			throw new RException("当前流程实例已经处于挂起状态");
		}
	}

	/**
	 * 激活流程实例
	 * @param 	processInstanceId		流程实例ID
	 * @param 	userId					操作员ID
	 * @param 	reason					变更理由
	 * @throws 	Exception
	 */
	@Override
	public void activateFlowByTaskId(String processInstanceId, String userId, String reason) throws Exception {
		
		ProcessInstance pi = runtimeService.createProcessInstanceQuery()
										   .processInstanceId(processInstanceId)
										   .singleResult();

		if (pi.isSuspended()) {
			runtimeService.activateProcessInstanceById(processInstanceId);
			this.insertSuspensionLog(pi.getBusinessKey(), userId, "挂起->正常", reason);
		}
		else {
			throw new RException("当前流程实例已经处于激活状态");
		}
	}
	
	/**
	 * 任务转办
	 * @param 	task_id					任务实例ID
	 * @param 	to_user_id				目标用户ID
	 * @throws 	Exception
	 */
	@Override
	public void transferTask(String task_id, String to_user_id) throws Exception {

		// 获得当前任务节点
		Task task = taskService.createTaskQuery().taskId(task_id).singleResult();

		// 校验任务是否存在
		if(task == null){
			throw new Exception("任务已过期或不存在");
		}
		
		taskService.setAssignee(task_id, to_user_id);
	}
	

	/**
	 * 根据参数业务编号撤消其流程
	 * @param deal_no
	 */
	@Override
	public void ApproveCancel(String deal_no) {
		//查询是否存在审批日志，若有则执行撤销操作退出流程
//		List<Map<String, Object>> flowLogList = flowLogService.getFlowlogBySerialNo(deal_no);
//		if(null !=flowLogList && flowLogList.size()>0){
//			String taskId = ParameterUtil.getString(flowLogList.get(flowLogList.size()-1), "TaskId", "");
//			if(flowLogList != null && flowLogList.size() > 0 && StringUtil.isNotEmpty(taskId)){
//				try {
//					this.completePersonalTask(SlSessionHelper.getUserId(), taskId, DictConstants.FlowCompleteType.Refuse, "", "", "");
//				} catch (Exception e) {
//					//e.printStackTrace();
//					throw new RException(e);
//				}
//			}
//		}
		//方法重做，上述旧方法的taskId获取方式有风险 by wangchen
		TtFlowSerialMap flowSer = ttFlowSerialMapMapper.get(null, deal_no, null, 0);
		if(flowSer != null){
			Task task = taskService.createTaskQuery().executionId(Long.toString(flowSer.getExecution())).singleResult();
			if(task != null){
				try {
					this.completePersonalTask(SlSessionHelper.getUserId(), task.getId(),deal_no, DictConstants.FlowCompleteType.Undo, "", "发起人"+SlSessionHelper.getUser().getUserName()+SlSessionHelper.getUserId()+"撤销", "");
				} catch (Exception e) {
					//e.printStackTrace();
					throw new RException(e);
				}
			}
		}
	}
	
	/**
	 * 撤回流程
	 * @param deal_no
	 */
	@Override
	public void recall(String deal_no) throws Exception {
		Map<String,String> instanceInfo = flowQueryService.getFlowIdAndInstanceIdBySerialNo(deal_no);
		String processInstanceId = ParameterUtil.getString(instanceInfo, "instance_id", "");
		String flowId = ParameterUtil.getString(instanceInfo, "flow_id", "");
		List<Task> taskContinueList = taskService.createTaskQuery().processInstanceId(processInstanceId).active().list();
		
		//并行分支不召回
		JY.require(taskContinueList.size() == 1, "存在并行任务,无法撤回");
		
		// 更新当前审批人信息到流程实例中
		runtimeService.setVariable(processInstanceId, SlbpmVariableNameConstants.lastOperator, SlSessionHelper.getUserId());
		
		// 保存审批意见 - 撤回
		taskService.addComment(taskContinueList.get(0).getId(), processInstanceId, "发起人"+SlSessionHelper.getUser().getUserName()+SlSessionHelper.getUserId()+"撤回");

		// 退回发起人
		this.returnTask(taskContinueList.get(0).getId());
	}
	
	/**
	 * 删除流程实例
	 * @param deal_no 交易单号
	 * @param reason  删除理由
	 */
	@Override
	public void deleteProcessInstance (String deal_no, String reason) throws Exception {
		//解除业务与流程实例的关系
		Map<String,String> instanceInfo = flowQueryService.getFlowIdAndInstanceIdBySerialNo(deal_no);
		String processInstanceId = ParameterUtil.getString(instanceInfo, "instance_id", "");
		runtimeService.deleteProcessInstance(processInstanceId, reason);
		
		//将状态更改为“7：审批拒绝”
		TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get("", deal_no, "", Long.parseLong(processInstanceId));
		SlbpmCallBackInteface listener = SpringContextHolder.getBean(flowSerMap.getStatus_change_listener());
		listener.statusChange(flowSerMap.getFlow_type(),
				  flowSerMap.getFlow_id(),
				  flowSerMap.getSerial_no(),
				  DictConstants.ApproveStatus.ApprovedNoPass,
				  DictConstants.FlowCompleteType.Refuse);
		flowSerMap.setApproved(DictConstants.YesNo.YES);
		
		//删除业务-流程映射数据
		ttFlowSerialMapMapper.deleteBySerial(deal_no);
		
	}

	private void insertSuspensionLog(String deal_no, String user_id,
									 String op_desc, String reason) throws Exception {
		
		ProcSuspensionLog procSuspensionLog = new ProcSuspensionLog();
		procSuspensionLog.setDeal_no(deal_no);
		procSuspensionLog.setUser_id(user_id);
		procSuspensionLog.setOp_time(DateUtil.getCurrentDateTimeAsString());
		procSuspensionLog.setOp_desc(op_desc);
		procSuspensionLog.setReason(reason);
		procSuspensionLogMapper.insert(procSuspensionLog);		
	}
	
	/**
	 * 写入投票记录
	 * @param proc_inst_id		流程实例ID
	 * @param task_id			任务ID
	 * @param task_def_key		流程节点KEY
	 * @param task_name			任务名称
	 * @param vote_user			投票用户ID
	 * @param result			投票结果	1同意|-1反对|0弃权
	 */
	private void insertVoteLog(String proc_inst_id, String task_id, String task_def_key, 
							   String task_name, String vote_user, String result) {
		
		ProcSignLog procSignLog = new ProcSignLog();
		procSignLog.setProc_inst_id(proc_inst_id);
		procSignLog.setTask_id(task_id);
		procSignLog.setTask_def_key(task_def_key);
		procSignLog.setActivity_name(task_name);
		procSignLog.setVote_time(DateUtil.getCurrentDateTimeAsString());
		procSignLog.setVote_user(vote_user);
		procSignLog.setResult(result);
		procSignLogMapper.insert(procSignLog);
	}
}