package com.singlee.slbpm.service.impl;

import com.github.pagehelper.Page;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.common.util.StringUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.dict.DictConstants;
import com.singlee.slbpm.mapper.ProcAuthorityMapper;
import com.singlee.slbpm.mapper.ProcDefInfoMapper;
import com.singlee.slbpm.pojo.bo.ProcAuthority;
import com.singlee.slbpm.pojo.bo.ProcDefInfo;
import com.singlee.slbpm.service.FlowQueryService;
import com.singlee.slbpm.service.FlowRoleService;
import com.singlee.slbpm.service.FlowSublicenseService;
import com.singlee.slbpm.util.ProcessDefinitionUtils;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.task.Task;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by wangfp on 2018/2/27.
 */
@Service("flowSublicenseService")
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FlowSublicenseServiceImpl implements FlowSublicenseService{

    private static Logger logger = LoggerFactory.getLogger("SLBPM");

    @Autowired
    private ProcessEngine processEngine;

    /** Activiti运行时接口 */
    @Autowired
    private RuntimeService runtimeService;

    /** Activiti任务接口 */
    @Autowired
    private TaskService taskService;

    @Autowired
    private DayendDateService dayendDateService;
    
    @Autowired
    private FlowRoleService flowRoleService;

    @Autowired
    private ProcAuthorityMapper procAuthorityMapper;
    
    /** 流程定义Dao */
    @Autowired
    private ProcDefInfoMapper procDefInfoMapper;

    /** 审批流程查询接口 */
    @Autowired
    private FlowQueryService flowQueryService;


    @Override
    public void addUserSublicenseDetail(Map<String, Object> paramMap) throws Exception {
    	//添加之前先查询是否有重复数据
//        	Map<String, Object> authParam = new HashMap<String, Object>();
//			authParam.put("task_def_key", ParameterUtil.getString(paramMap, "task_def_key", ""));
//			authParam.put("flow_id", ParameterUtil.getString(paramMap, "flow_id", ""));
//			authParam.put("to_user_id",  ParameterUtil.getString(paramMap, "to_user_id", ""));
//			authParam.put("auth_end_date", dayendDateService.getDayendDate());
//			authParam.put("is_active", "1");
//			Map<String, Object> authResult = this.getUserSublicenseById(authParam);
//			ProcAuthority procAuthority = (ProcAuthority)authResult.get("data");
//			if(procAuthority != null){
//				throw new RException();
//			}
        procAuthorityMapper.addProcAuthority(paramMap);
    }

    @Override
    public Page<ProcAuthority> getUserSublicenseList(Map<String, Object> paramMap) {
        Page<ProcAuthority> procAuthorityList = procAuthorityMapper.getUserSublicenseList(paramMap, ParameterUtil.getRowBounds(paramMap));
        for(ProcAuthority p:procAuthorityList){
            if(StringUtils.isNotEmpty(p.getFlow_id())){
                String version = procDefInfoMapper.getHighVersionNo(p.getFlow_id());
                ProcDefInfo procDefInfo = flowQueryService.getFlowDefine(p.getFlow_id(), version);
                if(procDefInfo != null){
                    p.setFlow_name(procDefInfo.getFlow_name());
                    if(StringUtils.isNotEmpty(p.getTask_def_key())){
                        ActivityImpl activity = (ActivityImpl) ProcessDefinitionUtils.getActivity(processEngine, p.getFlow_id(), p.getTask_def_key());
                        if(activity != null){
                            p.setTask_def_name(String.valueOf(activity.getProperty("name")));
                        }
                    }
                }
            }
        }
        return procAuthorityList;
    }
    
    /**
     * 查询指定结束日期的转授权记录
     * @param authEndDate
     * @return
     */
    @Override
    public List<ProcAuthority> getUserSublicenseListByEndDate(String authEndDate) {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("auth_end_date", authEndDate);
        List<ProcAuthority> procAuthorityList = procAuthorityMapper.getUserSublicenseList(map);
        return procAuthorityList;
    }
    
    @Override
    public Map<String, Object> getUserSublicenseById(Map<String, Object> paramMap) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            ProcAuthority procAuthority = procAuthorityMapper.getUserSublicenseById(paramMap);
            resultMap.put("data",procAuthority);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
        }
        return resultMap;
    }

    @Override
    public Map<String, Object> updateUserSublicenseDetail(Map<String, Object> paramMap) {
        try {
            procAuthorityMapper.updateUserSublicenseDetail(paramMap);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
        }
        return paramMap;
    }


    @Override
    public Map<String, Object> deleteUserSublicenseById(Map<String, Object> paramMap) {
        try {
            procAuthorityMapper.deleteUserSublicenseById(paramMap);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
        }
        return paramMap;
    }

    /*
        根据业务日期续期转授权
     */
    @Override
    public void renewalUserSublicense(Map<String, Object> paramMap) {
        try {
            Map<String, Object> resultMap = new HashMap<String, Object>();
            resultMap.put("date", dayendDateService.getDayendDate());
            procAuthorityMapper.renewalUserSublicense(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug(e.getMessage());
        }
    }

    /*
        根据业务日期及提醒天数查询待提醒的转授权列表
     */
    @Override
    public List<ProcAuthority> remindUserSublicense(Map<String, Object> paramMap) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("date", dayendDateService.getDayendDate());
        resultMap.put("number", ParameterUtil.getString(paramMap, "number", "0"));
        List<ProcAuthority> procAuthorityList = procAuthorityMapper.remindUserSublicense(resultMap);
        return procAuthorityList;
    }

    /*
                   转办已在流程中的审批流
     */
    @Override
    public void SublicenseToUserInFlow(Map<String, Object> paramMap) {
        String task_id = ParameterUtil.getString(paramMap, "task_id", "");
        String from_user_id = ParameterUtil.getString(paramMap, "from_user_id", "");
        String to_user_id = ParameterUtil.getString(paramMap, "to_user_id", "");
        PropertiesConfiguration config = null;
        try {
            config = PropertiesUtil.parseFile("system.properties");
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        //是否覆盖授权人
        String flag = config.getString("system.userAuthOverwrite", "false");

        TaskEntity entity = ProcessDefinitionUtils.getTaskEntity(processEngine, task_id);
        String specificUser = entity.getAssignee();

        if(!"".equals(task_id) && !"".equals(from_user_id) && !"".equals(to_user_id)){
            //会签节点除外
            if(!entity.getTaskDefinitionKey().startsWith("Parallel-")){
                //若任务已分配
                if(!"".equals(specificUser) && specificUser != null){
                    taskService.setAssignee(task_id,"");
                    if("false".equals(flag)){
                        taskService.addCandidateUser(task_id,specificUser);
                    }else if("true".equals(flag)){
                        taskService.deleteCandidateUser(task_id,from_user_id);
                    }
                }else{
                    if("true".equals(flag)){
                        taskService.deleteCandidateUser(task_id,from_user_id);
                    }
                }
                taskService.addCandidateUser(task_id,to_user_id);
            }
        }
    }
    
    
	/**
	 * 提交/同意/拒绝
	 */
	@Override
	public void approve(String id, int status)  throws Exception{
		ProcAuthority pa = new ProcAuthority();
		pa.setId(id);
		pa = procAuthorityMapper.selectByPrimaryKey(pa);
		pa.setAuth_status(status);
		if(Integer.parseInt(DictConstants.ApproveStatus.ApprovedPass) == status){
			String randomKey = this.getRandomKey();
			pa.setAuth_key(randomKey);
		}
		procAuthorityMapper.updateByPrimaryKey(pa);
	}
	
	/**
	 * 将验证码发送到jsp页面
	 */
	@Override
	public String sendKeyToJsp(String id)  throws Exception{
		ProcAuthority pa = new ProcAuthority();
		pa.setId(id);
		pa = procAuthorityMapper.selectByPrimaryKey(pa);
		return pa.getAuth_key();
	}
	
	/**
	 * 确认是否需要校验授权
	 */
	@Override
	public String isNeedCheck(String taskId)  throws Exception{
		TaskEntity entity = ProcessDefinitionUtils.getTaskEntity(processEngine, taskId);
		String task_def_key = entity.getTaskDefinitionKey();
		String flow_id = entity.getProcessDefinitionId();
		//排除加签
		String task_def_key_first = task_def_key.substring(0, 1);
		if(":".equals(task_def_key_first)){
			return "";
		}
		//排除自己可以审批的任务
        Task task  = taskService.createTaskQuery().taskId(taskId).singleResult();
		boolean taskAccess = flowRoleService.checkTaskAccess("", task);
		if(taskAccess){
			return "";
		}
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("to_user_id", SlSessionHelper.getUserId());
		param.put("auth_end_date", dayendDateService.getDayendDate());
		param.put("is_active", "1");
		param.put("task_def_key", task_def_key);
		param.put("flow_id", flow_id);
		List<ProcAuthority> procAuthority = procAuthorityMapper.getUserSublicenseList(param);
		String result = "";
		for(int i = 0; i < procAuthority.size(); i++){
			result += procAuthority.get(i).getId();
			
		}
		return result;
	}
    
	/**
	 * 校验
	 */
	@Override
	public boolean check(String id, String key) throws Exception {
		Map<String, Object> param = new HashMap<String, Object>();
		if(StringUtil.isNullOrEmpty(id)){
			param.put("id", id);
		}else{
			String[] idArr = id.split(",");
			List<String> ids = Arrays.asList(idArr);
			param.put("ids", ids);
		}
		param.put("auth_key", key);
		param.put("is_active", "1");
		param.put("to_user_id", SlSessionHelper.getUserId());
		ProcAuthority procAuthority = procAuthorityMapper.getUserSublicenseById(param);
		if(procAuthority == null){
			return false;
		}
		return true;
	}
    
    private String getRandomKey(){
        String val = "";
        Random random = new Random();
        for ( int i = 0; i < 12; i++ ){
            String str = random.nextInt( 2 ) % 2 == 0 ? "num" : "char";
             if ( "char".equalsIgnoreCase(str)){ 
            	// 产生字母
                int nextInt = 65;//固定大写字母 random.nextInt( 2 ) % 2 == 0 ? 65 : 97;
                val += (char)(nextInt + random.nextInt( 26 ));
            }else if ( "num".equalsIgnoreCase(str)){
            	// 产生数字
                val += String.valueOf(random.nextInt(10));
            }
        }
        
        //如果检查发现重复key　则递归重新生成
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("auth_key", val);
		ProcAuthority procAuthorize = procAuthorityMapper.getUserSublicenseById(param);
		if(procAuthorize != null){
			this.getRandomKey();
		}
        return val;
    }
}
