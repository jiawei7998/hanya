package com.singlee.slbpm.service;

import com.github.pagehelper.Page;
import com.singlee.slbpm.pojo.bo.TtFlowOperation;
import com.singlee.slbpm.pojo.bo.TtFlowRole;
import com.singlee.slbpm.pojo.bo.TtFlowRoleAuth;
import com.singlee.slbpm.pojo.vo.ApproveUser;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapHisVo;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapVo;
import org.activiti.engine.task.Task;

import java.util.List;
import java.util.Map;

/**
 * 审批角色管理，包括预授权管理
 * @author Libo
 *
 */
public interface FlowRoleService {

	/**
	 * 新增审批角色
	 * @param flowRole
	 */
	public void addNewFlowRole(TtFlowRole flowRole);
	
	/**
	 * 删除审批角色
	 * @param roleId
	 */
	public void deleteFlowRole(String roleId);
	
	/**
	 * 修改审批角色
	 * @param flowRole
	 */
	
	public void modifyFlowRole(TtFlowRole flowRole);
	
	/**
	 * 查询审批角色
	 * @param roleId	角色id	【可为空】
	 * @param roleName	角色名称  【可为空】
	 * @param memo		备注		【可为空】
	 * @param instId	机构id  【可为空】
	 */
	public List<TtFlowRole> searchFlowRole(String roleId, String roleName, String memo, String instId);
	
	/**
	 * 查询审批角色
	 * @param roleId	角色id	【可为空】
	 * @param roleName	角色名称  【可为空】
	 * @param memo		备注		【可为空】
	 * @param instId	机构id  【可为空】
	 */
	public Page<TtFlowRole> searchFlowRolePage(Map<String,Object> paramMap);
	
	/**
	 * 获取 转授权 
	 * 获得 柜员（user_id）的所有审批角色柜员映射关系
	 * 一般不建议两参数同时为空，同时为空可能返回记录不受控制，导致内存占用
	 * 为空请填 null
	 *@map.user_id 允许为空，
	 *@map.role_id 允许为空 为空则不关注   
	 * @author LyonChen
	 */
	public List<FlowRoleUserMapVo> listFlowRoleUserMap(Map<String,Object> map);
	
	/**
	 * 清空 转授权 
	 * 清除 柜员（user_id）的所有审批角色柜员映射关系
	 * 一般不建议两参数同时为空，同时为空可能返回记录不受控制，导致内存占用
	 * 为空请填 null
	 *@map.user_id 允许为空，
	 *@map.role_id 允许为空 为空则不关注   
	 * @author LyonChen
	 */
	public void resetFlowRoleUserMap(Map<String, Object> paramMap);
	
	/**
	 * 更新 转授权 记录表
	 * @author LyonChen
	 * @param user_id 指定柜员 
	 * @param list.role_id 指定角色 （如果为空，表示所有的角色都授权给 auth_id 柜员
	 * @param list.auth_id 指定
	 * @param list.beg_date 
	 * @param list.end_date
	 */
	public void saveFlowRoleUserMap(String user_id, List<FlowRoleUserMapVo> list);
	
	/**
	 * 修改角色用户对应关系
	 * @param role_id	角色id
	 * @param user_ids	用户id列表，用“，”隔开
	 */
	public void modifyFlowRoleUserMap(String role_id,String user_ids);
	
	/**
	 * 修改角色用户对应关系
	 * @param role_id	角色id
	 * @param user_ids	用户id列表，用“，”隔开
	 */
	public void deleteFlowRoleUserMap(String role_id,String user_ids);
	/**
	 * 新增角色用户对应关系
	 * @param role_id	角色id
	 * @param user_ids	用户id列表，用“，”隔开
	 */
	public void addFlowRoleUserMap(String role_id,String user_ids);
	/**
	 * 转授权历史保存
	 * @param map
	 */
	public void saveFlowRoleUserMapHis(Map<String,Object> map);
	
	/**
	 * 转授权历史查询
	 * @param map
	 */
	public Page<FlowRoleUserMapHisVo> pageFlowRoleUserMapHis(Map<String,Object> map);

	/**
	 * 根据角色ID，查询该角色下的所有用户	
	 * @param roles				角色列表
	 * @param userIdList		用户的ID列表，如果传了，则从此列表中选取
	 * @param lastOperator		上一步审批人，为了控制是否允许两步为同一审批人
	 * @return
	 */
	public List<ApproveUser> getUserMapByRole(String roles, 
											  List<String> userIdList, 
											  String lastOperator);
	
	/** + 2017.5.16 - Yang Yang - 审批权限设置模块 */
	
	/**
	 * 获取审批操作字典列表
	 */
	public List<TtFlowOperation> getFlowOperations();
	
	/**
	 * 获取审批角色的操作权限
	 * @param	role_id			审批角色ID
	 */
	public List<TtFlowRoleAuth> getFlowRoleOperations(String role_id);
	
	/**
	 * 设置审批角色的操作权限
	 * @param	role_id			审批角色ID
	 * @param	op_ids			权限列表
	 */
	public void setFlowRoleOperations(String role_id, String op_ids);
	
	/** - 2017.5.16 - Yang Yang - 审批权限设置模块 */
	
	/**
	 * add by wangchen on 2017/5/22
	 * 根据登录用户查询审批权限
	 * @param task_id			任务ID
	 * @return List<String>
	 */
	public List<String> getUserFlowOperations(String task_id) throws Exception;

    /**
     * 检查当前用户是否能发起审批
     * @param user_id           用户ID
     * @param flow_id			流程定义ID
     * @return					是/否
     */
	public boolean checkFlowInitiator(String user_id, String flow_id);

	/**
	 * 检查当前用户是否能审批当前任务
     * @param user_id           用户ID
	 * @param task_id			任务ID
	 * @return					是/否
	 */
    public boolean checkTaskAccess(String user_id, Task task);

    /**
     *   根据流程获取用户所属审批角色
     * @param user_id       用户ID
     * @param flow_id       流程ID
     * @return              节电list
	 * @throws Exception 
     */
    public List<Map<String, Object>> getUserRoleListByFlow(String user_id,
			String flow_id) throws Exception;

}
