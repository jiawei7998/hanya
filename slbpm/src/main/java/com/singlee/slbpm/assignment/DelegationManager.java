package com.singlee.slbpm.assignment;

import org.activiti.engine.impl.persistence.entity.TaskEntity;

public interface DelegationManager
{
	/**
	 * 获取指定用户的代理人列表 
	 */
	String[] getDelegates(String delegated, TaskEntity task);
}
