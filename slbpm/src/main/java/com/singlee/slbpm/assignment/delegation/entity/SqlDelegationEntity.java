package com.singlee.slbpm.assignment.delegation.entity;

import com.singlee.slbpm.assignment.DelegationEntity;

public class SqlDelegationEntity implements DelegationEntity
{
	private String role_id;			// 角色ID
	private String user_id;			// 用户ID
	private String auth_userid;		// 授权用户ID 空为没有转授权
	private String auth_begdate;	// 授权开始日期
	private String auth_enddate;	// 授权结束日期

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getAuth_userid() {
		return auth_userid;
	}

	public void setAuth_userid(String auth_userid) {
		this.auth_userid = auth_userid;
	}

	public String getAuth_begdate() {
		return auth_begdate;
	}

	public void setAuth_begdate(String auth_begdate) {
		this.auth_begdate = auth_begdate;
	}

	public String getAuth_enddate() {
		return auth_enddate;
	}

	public void setAuth_enddate(String auth_enddate) {
		this.auth_enddate = auth_enddate;
	}

	@Override
	public String getDelegate()
	{
		return auth_userid;
	}

	@Override
	public String getDelegated()
	{
		return user_id;
	}
}
