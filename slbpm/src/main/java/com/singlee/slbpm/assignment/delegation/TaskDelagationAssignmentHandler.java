package com.singlee.slbpm.assignment.delegation;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.PropertiesUtil;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.slbpm.assignment.DelegationManager;
import com.singlee.slbpm.assignment.TaskAssignmentHandler;
import com.singlee.slbpm.assignment.TaskAssignmentHandlerChain;
import com.singlee.slbpm.externalInterface.AuthorityInterface;
import com.singlee.slbpm.pojo.bo.ProcAuthority;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import com.singlee.slbpm.service.FlowQueryService;
import com.singlee.slbpm.service.FlowSublicenseService;
import com.singlee.slbpm.service.impl.FlowOpServiceImpl;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.activiti.engine.task.IdentityLink;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.util.*;

public class TaskDelagationAssignmentHandler implements TaskAssignmentHandler
{
	DelegationManager _delegationManager;

	public DelegationManager getDelegationManager()
	{
		return _delegationManager;
	}

	public void setDelegationManager(DelegationManager delegationManager)
	{
		_delegationManager = delegationManager;
	}

	@Override
	public void handleAssignment(TaskAssignmentHandlerChain chain, TaskEntity task, ActivityExecution execution)
	{
		//先执行其它规则
		chain.resume(task, execution);

		overwriteAssignee(task);
	}

	/**
	 * 如果该任务已经指派审批人
	 * 则检测此审批人在该任务下的某个审批角色是否被代理
	 * 并且是否在有效时间范围内
	 * 如果条件都满足,则把当前审批人替换成代理人
	 * @param task
	 */
	protected void overwriteAssignee(TaskEntity task)
	{
		PropertiesConfiguration config = null;
		try {
			config = PropertiesUtil.parseFile("system.properties");
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		FlowQueryService flowQueryService = SpringContextHolder.getBean("flowQueryService");
		FlowSublicenseService flowSublicenseService = SpringContextHolder.getBean("flowSublicenseService");
		DayendDateService dayendDateService = SpringContextHolder.getBean("dayendDateService");
		String flag = config.getString("system.userAuthOverwrite", "false");
		if(!task.getTaskDefinitionKey().startsWith("Parallel-")){
			Set<IdentityLink> candidates = task.getCandidates();
			for(IdentityLink candidate:candidates) {
				String user_id = candidate.getUserId();
				if (!"".equals(user_id) && user_id != null) {
					// 根据审批人获取有效的转授权代理人
					// 根据用户名、流程定义id、流程节点key和转授权起止时间查询
					// 如果有多条匹配默认则全部分配
					// TODO 后续讨论机构变更事宜
					String[] delegates = _delegationManager.getDelegates(user_id,task);
					
					// add by wangchen 2018-3-6 start
					//转授权校验
					List<String> result = new ArrayList<String>();
					String flow_id = task.getProcessDefinitionId();
					String task_def_key = task.getTaskDefinitionKey();
					for(String authUser : delegates){
						Map<String, Object> authParam = new HashMap<String, Object>();
						authParam.put("task_def_key", task_def_key);
						authParam.put("flow_id", flow_id);
						authParam.put("to_user_id", authUser);
						authParam.put("auth_end_date", dayendDateService.getDayendDate());
						Map<String, Object> authResult = flowSublicenseService.getUserSublicenseById(authParam);
						ProcAuthority procAuthority = (ProcAuthority)authResult.get("data");
						if(procAuthority != null){
							//如果用户不在岗位内并且能查询到授权给本人的转授权，则转入规则校验是否可以审批该交易
							// 获得当前流程实例的回调接口
							boolean approve = false;
							try {
								Map<String, Object> serMap = new HashMap<String, Object>();
								serMap.put("execution", task.getProcessInstanceId());
								//查询业务映射   (ps.提交审批时可能会查不到  此时使用简易静态变量获取)
								TtFlowSerialMap flowSerialMap = flowQueryService.getFlowSerialMap(serMap);
								if(flowSerialMap == null){
									flowSerialMap = (TtFlowSerialMap)FlowOpServiceImpl.flowSerialCache.get("flowSerMap");
								}
								//去各个业务service各自校验规则
								AuthorityInterface listener = SpringContextHolder.getBean(flowSerialMap.getStatus_change_listener());
								approve = listener.AuthorityCheck(flowSerialMap.getSerial_no(), procAuthority.getAuthority_rule());
							} catch (Exception e) {
								e.printStackTrace();
							}
							//校验通过后再加入候选人
							if(approve){
								result.add(authUser);
							}

						}
					}
					//end
					
					if (result != null && result.size() > 0) {
						for(int i = 0; i<result.size();i++){
							if("true".equals(flag)){
								task.deleteCandidateUser(user_id);
								task.addCandidateUser(result.get(i));
							}else if("false".equals(flag)){
								task.addCandidateUser(result.get(i));
							}
						}
					}
				}
			}
		}
	}
}
