package com.singlee.slbpm.assignment.permission;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.slbpm.assignment.TaskAssignmentHandler;
import com.singlee.slbpm.assignment.TaskAssignmentHandlerChain;
import com.singlee.slbpm.pojo.vo.ApproveUser;
import com.singlee.slbpm.service.FlowRoleService;
import com.singlee.slbpm.util.SlbpmVariableNameConstants;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ActivityPermissionAssignmentHandler implements TaskAssignmentHandler
{	
	@Override
	public void handleAssignment(TaskAssignmentHandlerChain chain, 
								 TaskEntity task, 
								 ActivityExecution execution)
	{
		FlowRoleService flowRoleService = SpringContextHolder.getBean("flowRoleService");
				
		// 如果上一步审批人已指定下一步审批人，则直接分配
    	Object obj1 = task.getVariables().get(SlbpmVariableNameConstants.specificUser);
		String specificUser = (obj1 == null) ? "" : obj1.toString();
		
		if (StringUtils.isNotEmpty(specificUser)) {
			
			task.setAssignee(specificUser);
		}
		else {

	        // 获得上一个任务节点的审批人
	        Object obj2 = task.getVariables().get(SlbpmVariableNameConstants.lastOperator);
			String lastOperator = (obj2 == null) ? "" : obj2.toString();
			

			// 获得当前任务节点的审批角色
			List<String> lst_group_id = new ArrayList<String>();
			for(Expression group_id : task.getTaskDefinition().getCandidateGroupIdExpressions()) {
				if (group_id != null && StringUtils.isNoneEmpty(group_id.getExpressionText())) {
					lst_group_id.add(group_id.getExpressionText());
				}
			}
			
			String roles = StringUtils.join(lst_group_id, ",");
			
			List<ApproveUser> userList = flowRoleService.getUserMapByRole(roles, null, lastOperator);
			
			if(userList == null || userList.size() == 0){
				throw new RException("无法获取下一步的审批人员，请检查流程设置");
			}
			
			// 把角色下面的人员加入到任务候选人中
			List<String> lst_user_id = new ArrayList<String>();
			for(ApproveUser user : userList){
				lst_user_id.add(user.getUserId());
			}
			
			task.addCandidateUsers(lst_user_id);
			
		}

		chain.resume(task, execution);
	}
}
