package com.singlee.slbpm.assignment.impl;

import com.singlee.slbpm.assignment.TaskAssignmentHandler;
import com.singlee.slbpm.assignment.TaskAssignmentHandlerChain;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.activiti.engine.impl.task.TaskDefinition;

import java.util.List;
import java.util.Set;

public class MyUserTaskActivityBehavior extends UserTaskActivityBehavior
{
	private static final long serialVersionUID = 1L;
	
	List<TaskAssignmentHandler> _handlers;

	public MyUserTaskActivityBehavior(List<TaskAssignmentHandler> handlers, 
									  UserTask userTask, TaskDefinition taskDefinition)
	{
		super(userTask.getId(), taskDefinition);
		_handlers = handlers;
	}

	protected TaskAssignmentHandlerChainImpl createHandlerChain()
	{
		TaskAssignmentHandlerChainImpl handlerChain = new TaskAssignmentHandlerChainImpl();
		final MyUserTaskActivityBehavior myUserTaskActivityBehavior = this;
		handlerChain.addHandler(new TaskAssignmentHandler()
		{
			@Override
			public void handleAssignment(TaskAssignmentHandlerChain chain, TaskEntity task, ActivityExecution execution)
			{
				myUserTaskActivityBehavior.superHandleAssignments(task, execution);
			}
		});

		handlerChain.addHandlers(_handlers);
		return handlerChain;
	}

	@Override
	protected void handleAssignments(Expression assigneeExpression,
									 Expression ownerExpression,
									 Set<Expression> candidateUserExpressions,
									 Set<Expression> candidateGroupExpressions,
									 TaskEntity task, ActivityExecution execution)
	{
		createHandlerChain().resume(task, execution);
	}

	protected void superHandleAssignments(TaskEntity task, ActivityExecution execution)
	{
		Expression assigneeExpression = task.getTaskDefinition().getAssigneeExpression();
		Expression ownerExpression = task.getTaskDefinition().getOwnerExpression();
		Set<Expression> candidateUserExpressions = task.getTaskDefinition().getCandidateUserIdExpressions();
		Set<Expression> candidateGroupExpressions = task.getTaskDefinition().getCandidateGroupIdExpressions();
		
		super.handleAssignments(assigneeExpression, 
								ownerExpression, 
								candidateUserExpressions, 
								candidateGroupExpressions, 
								task, execution);
	}
}
