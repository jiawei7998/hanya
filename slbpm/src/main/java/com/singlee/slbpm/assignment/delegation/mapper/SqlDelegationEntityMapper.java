package com.singlee.slbpm.assignment.delegation.mapper;

import com.singlee.slbpm.assignment.delegation.entity.SqlDelegationEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SqlDelegationEntityMapper
{

	@Select("SELECT * FROM TT_FLOW_ROLE_USER_MAP T " +
			"WHERE TO_CHAR(SYSDATE,'YYYY-MM-DD') >= T.AUTH_BEGDATE " +
			"AND TO_CHAR(SYSDATE,'YYYY-MM-DD') <= T.AUTH_ENDDATE " +
			"AND T.USER_ID = #{delegated}")
	List<SqlDelegationEntity> findByDelegated(@Param("delegated") String delegated);
}
