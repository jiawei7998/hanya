package com.singlee.slbpm.assignment.impl;

import com.singlee.slbpm.assignment.TaskAssignmentHandler;
import com.singlee.slbpm.assignment.TaskAssignmentHandlerChain;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;

import java.util.List;
import java.util.Stack;

public class TaskAssignmentHandlerChainImpl implements TaskAssignmentHandlerChain
{
	static TaskAssignmentHandler NULL_HANDLER = new TaskAssignmentHandler()
	{
		@Override
		public void handleAssignment(TaskAssignmentHandlerChain chain, TaskEntity task, ActivityExecution execution)
		{
		}
	};

	Stack<TaskAssignmentHandler> _handlers = new Stack<TaskAssignmentHandler>();

	public void addHandler(TaskAssignmentHandler handler)
	{
		_handlers.push(handler);
	}

	public void addHandlers(List<TaskAssignmentHandler> handlers)
	{
		for (TaskAssignmentHandler handler : handlers)
		{
			_handlers.push(handler);
		}
	}

	public TaskAssignmentHandler next()
	{
		if (_handlers.isEmpty())
		{
			return NULL_HANDLER;
		}

		return _handlers.pop();
	}

	@Override
	public void resume(TaskEntity task, ActivityExecution execution)
	{
		next().handleAssignment(this, task, execution);
	}
}
