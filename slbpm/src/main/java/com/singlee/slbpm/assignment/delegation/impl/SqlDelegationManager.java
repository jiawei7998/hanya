package com.singlee.slbpm.assignment.delegation.impl;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.dayend.service.DayendDateService;
import com.singlee.slbpm.assignment.DelegationManager;
import com.singlee.slbpm.assignment.delegation.mapper.SqlDelegationEntityMapper;
import com.singlee.slbpm.cfg.SqlMapperBasedServiceBase;
import com.singlee.slbpm.mapper.ProcAuthorityMapper;
import com.singlee.slbpm.pojo.bo.ProcAuthority;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional(value="transactionManager",rollbackFor=Exception.class,readOnly = true)
public class SqlDelegationManager extends SqlMapperBasedServiceBase<SqlDelegationEntityMapper> implements
		DelegationManager
{
	
	@Autowired
	private ProcAuthorityMapper procAuthorityMapper;
	@Override
	public String[] getDelegates(String delegated, TaskEntity task)
	{
		DayendDateService dayendDateService = SpringContextHolder.getBean("dayendDateService");
		
		Map<String, Object> delegates = new HashMap<String, Object>();

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("delegated",delegated);
		paramMap.put("processDefinitionId",task.getProcessDefinitionId());
		paramMap.put("taskDefinitionKey",task.getTaskDefinitionKey());
		paramMap.put("dayendDate",dayendDateService.getDayendDate());
		List<ProcAuthority> authorityList = procAuthorityMapper.getAccordUserSublicense(paramMap);
		for (ProcAuthority authority : authorityList)
		{
			if (StringUtils.isNotEmpty(authority.getTo_user_id())) {
				delegates.put(authority.getTo_user_id(), 0);
			}
		}

		return delegates.keySet().toArray(new String[0]);
	}
}
