package com.singlee.slbpm.cfg.impl;

import com.singlee.slbpm.assignment.TaskAssignmentHandler;
import com.singlee.slbpm.assignment.impl.MyUserTaskActivityBehavior;
import com.singlee.slbpm.cfg.ActivityBehaviorFactoryDelegate;
import org.activiti.bpmn.model.ServiceTask;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.impl.bpmn.behavior.ServiceTaskExpressionActivityBehavior;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.bpmn.parser.factory.ActivityBehaviorFactory;
import org.activiti.engine.impl.bpmn.parser.factory.DefaultActivityBehaviorFactory;
import org.activiti.engine.impl.task.TaskDefinition;

import java.util.List;

public class MyActivityBehaviorFactory extends ActivityBehaviorFactoryDelegate implements ActivityBehaviorFactory
{
	List<TaskAssignmentHandler> _handlers;

	public MyActivityBehaviorFactory(ActivityBehaviorFactory source, List<TaskAssignmentHandler> handlers)
	{
		super(source);
		_handlers = handlers;
	}

	@Override
	public UserTaskActivityBehavior createUserTaskActivityBehavior(UserTask userTask, TaskDefinition taskDefinition)
	{
		return new MyUserTaskActivityBehavior(_handlers, userTask, taskDefinition);
	}
	
	@Override
	public ServiceTaskExpressionActivityBehavior createServiceTaskExpressionActivityBehavior(ServiceTask serviceTask)
	{
		// BUGFIX - yangyang - 2017/6/7 - 把引擎初始化后生成的expressionManager赋值给原对象
		// 避免出现 ServiceTaskParseHandler.java的executeParse方法Line76调用时出现expressionManager=null的情况
		if (_source instanceof DefaultActivityBehaviorFactory) {
			((DefaultActivityBehaviorFactory)_source).setExpressionManager(expressionManager);
		}
		
		return super.createServiceTaskExpressionActivityBehavior(serviceTask);
	}
}
