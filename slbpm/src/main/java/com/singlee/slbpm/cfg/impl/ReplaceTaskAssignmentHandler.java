package com.singlee.slbpm.cfg.impl;

import com.singlee.slbpm.assignment.TaskAssignmentHandler;
import com.singlee.slbpm.cfg.StartEngineEventListener;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.bpmn.parser.factory.ActivityBehaviorFactory;
import org.activiti.engine.impl.bpmn.parser.factory.DefaultActivityBehaviorFactory;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;

import java.util.List;

public class ReplaceTaskAssignmentHandler implements StartEngineEventListener
{
	List<TaskAssignmentHandler> _handlers;

	@Override
	public void afterStartEngine(ProcessEngineConfigurationImpl processEngineConfiguration, ProcessEngine processEngine) throws Exception
	{
	}

	@Override
	public void beforeStartEngine(ProcessEngineConfigurationImpl processEngineConfiguration) throws Exception
	{
		ActivityBehaviorFactory activityBehaviorFactory = processEngineConfiguration.getActivityBehaviorFactory();
		if (activityBehaviorFactory == null)
		{
			activityBehaviorFactory = new DefaultActivityBehaviorFactory();
		}

		processEngineConfiguration.setActivityBehaviorFactory(new MyActivityBehaviorFactory(activityBehaviorFactory,
				_handlers));
	}

	public List<TaskAssignmentHandler> getHandlers()
	{
		return _handlers;
	}

	public void setHandlers(List<TaskAssignmentHandler> handlers)
	{
		_handlers = handlers;
	}
}
