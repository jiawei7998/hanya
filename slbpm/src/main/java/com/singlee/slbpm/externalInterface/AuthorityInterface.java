package com.singlee.slbpm.externalInterface;

/**
 * 转授权 校验 外部接口
 * @author wangchen
 *
 */
public interface AuthorityInterface {

	boolean AuthorityCheck(String dealNo, String rule) throws Exception;
}
