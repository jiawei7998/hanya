package com.singlee.slbpm.externalInterface;

/**
 * 额度外部调用接口，外部系统需实现此接口的方法
 * @author yangyang
 *
 */
public interface CreditInterface {

	/**
	 * 额度占用
	 * @param serial_no		业务流水号
	 * @return				是否成功
	 */
	public Boolean UseCredit(String serial_no);
	
	/**
	 * 额度释放
	 * @param serial_no		业务流水号
	 * @return				是否成功
	 */
	public Boolean ReleaseCredit(String serial_no);
	
	/**
	 * 获取额度占用期限
	 * @param serial_no		业务流水号
	 * @return				天数
	 */
	public int getCreditPeriod(String serial_no);
}
