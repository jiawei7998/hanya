package com.singlee.slbpm.externalInterface;

import java.util.Map;

public interface TaskEventInterface {

	/**
	 * 人工任务节点自定义调用事件
	 * @param flow_id					流程定义ID
	 * @param serial_no					业务流水号
	 * @param task_id					当前任务ID
	 * @param task_def_key				当前任务定义key
	 * @return map<event, [true/false]>	执行成功事件 		
	 * 									quotaPreCheck 		额度预检查
	 * 									quotaPreOccupy 		额度预占用
	 * 									transfer 			大额划款
	 * 									preAccounting		预记账
	 * 									quotaAutoOccupy 	额度实际占用（自动）
	 * 									quotaManualOccupy 	额度实际占用（人工额度岗）
	 * 									quotaAutoRelease	额度释放（自动）
	 * 									quotaManualRelease	额度释放（人工额度岗）
	 */
	public Map<String, Object> fireEvent(String flow_id, String serial_no, String task_id, String task_def_key) throws Exception;
	
	//额度占用（自动）
	public void LimitOccupy(String serial_no, String prd_no) throws Exception;

}