package com.singlee.slbpm.externalInterface;

/**
 * 风险外部调用接口，外部系统需实现此接口的方法
 * @author yangyang
 * 
 */
public interface RiskInterface {
	
	/**
	 * 风险等级判断
	 * @param serial_no			业务流水号
	 * @return 					0-低 1-中 2-高
	 */
	public int getRiskLevel(String serial_no);
	
	/**
	 * 资金来源判断
	 * @param serial_no			业务流水号
	 * @return 					0-自营资金 1-理财资金
	 */
	public int checkSourceOfMoney(String serial_no);
	
	/**
	 * 发起机构类型判断
	 * @param serial_no			业务流水号
	 * @return					0-总行 1-分行 2-支行 9-部门
	 */
	public int checkInstitutionType(String serial_no);

	/**
	 * 获取理财类型
	 * @param serial_no			业务流水号
	 * @return 					0-保本 1-保本浮动 2-非保本
	 */
	public int getFinType(String serial_no);

	/**
	 * 上海银行 - 是否满足交易金额
	 * @param serial_no			业务流水号
	 * @return 					1-是 0-否
	 */
	public int isAmtSatisfied(String serial_no);

	/**
	 * 上海银行 - 是否需要放款
	 * @param serial_no			业务流水号
	 * @return 					1-是 0-否
	 */
	public int needLoan(String serial_no);

	/**
	 * 上海银行 - 币种
	 * @param serial_no			业务流水号
	 * @return 					CNY-人民币 OTHER-外币
	 */
	public int getCurrency(String serial_no);
	
	/**
	 * 获取交易金额
	 * @param serial_no			业务流水号
	 * @return 					0-低 1-中 2-高
	 */
	public double getTotalAmount(String serial_no);
	/**
	 * 获取交易方向
	 * @param serial_no			业务流水号
	 * @return 					0-低 1-中 2-高
	 */
	public int getMyDir(String serial_no);
	/**
	 * 获取债券类型
	 * @param serial_no			业务流水号
	 * @return 					0-低 1-中 2-高
	 */
	public int getBondProperties(String serial_no);
	
	/**
	 * 产品代码
	 * @param serial_no
	 * @return
	 */
	public int getProduct(String serial_no);
	/**
	 * 产品类型
	 * @param serial_no
	 * @return
	 */
	public int getProdType(String serial_no);
	/**
	 * 成本中心
	 * @param serial_no
	 * @return
	 */
	public int getCost(String serial_no);
	/**
	 * 获取风险程度
	 * @param serial_no			业务流水号
	 * @return 					0-低 1-中 2-高
	 */
	public int getRiskDegree(String serial_no);
	/**
	 * 获取主体评级
	 * @param serial_no			业务流水号
	 * @return 					0-低 1-中 2-高
	 */
	public int getBondRating(String serial_no);
	/**
	 * 获取泰隆币种
	 * @param serial_no			业务流水号
	 * @return 					0-低 1-中 2-高
	 */
	public int getTlCurrency(String serial_no);
	/**
	 * 特殊用户
	 * @param serial_no
	 * @return
	 */
	public int isTrader(String serial_no);
	/**
	 * 是否限额
	 * @param serial_no
	 * @return
	 */
	public int isLimitAmount(String serial_no);
}
