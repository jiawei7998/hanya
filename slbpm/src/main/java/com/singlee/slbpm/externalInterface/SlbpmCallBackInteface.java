package com.singlee.slbpm.externalInterface;

/**
 * 流程回调接口，每个业务实现自己的回调方法
 * @author Libo
 *
 */
public interface SlbpmCallBackInteface {
	
	/**
	 * 获取提交审批的业务对象，如审批单对象
	 * @param flow_type
	 * @param flow_id
	 * @param serial_no	业务对象流水号
	 * @return
	 */
	public Object getBizObj(String flow_type, String flow_id, String serial_no);
	
	/**
	 * 流程状态变更
	 * @param flow_type		流程类型
	 * @param flow_id		流程id
	 * @param serial_no		序号
	 * @param status		最新审批状态
	 */
	public void statusChange(String flow_type, String flow_id, String serial_no, 
							 String status, String flowCompleteType);
}
