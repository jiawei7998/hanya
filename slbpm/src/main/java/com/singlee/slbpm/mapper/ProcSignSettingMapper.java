package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.ProcSignSetting;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface ProcSignSettingMapper extends Mapper<ProcSignSetting> {

	/**
	 * 根据节点定义key查询对应的会签规则
	 * @param map
	 * @return
	 */
	public ProcSignSetting getProcSignSetting(Map<String, Object> map);
}
