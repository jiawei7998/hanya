package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.ProcDefPropMap;
import com.singlee.slbpm.pojo.vo.ProcDefPropMapVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface ProcDefPropMapMapper extends Mapper<ProcDefPropMap> {
	
	/**
	 * 查询业务流程定义
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
	public List<ProcDefPropMapVo> getProcDefPropMap(Map<String, Object> map);

	/**
	 * 根据流程编号查询关联产品
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
	public List<ProcDefPropMapVo> getPrdInfoByFlow(Map<String, Object> map);
}
