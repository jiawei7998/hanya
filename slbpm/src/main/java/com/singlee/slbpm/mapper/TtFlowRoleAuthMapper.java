package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.TtFlowRoleAuth;
import tk.mybatis.mapper.common.Mapper;

public interface TtFlowRoleAuthMapper extends Mapper<TtFlowRoleAuth> {

}
