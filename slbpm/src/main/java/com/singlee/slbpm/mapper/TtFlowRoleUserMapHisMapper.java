package com.singlee.slbpm.mapper;

import com.github.pagehelper.Page;
import com.singlee.slbpm.pojo.bo.TtFlowRoleUserMapHis;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapHisVo;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

public interface TtFlowRoleUserMapHisMapper extends Mapper<TtFlowRoleUserMapHis> {
	
	public Page<FlowRoleUserMapHisVo> pageFlowRoleUserMapHis(Map<String, Object> map, RowBounds rb);
}
