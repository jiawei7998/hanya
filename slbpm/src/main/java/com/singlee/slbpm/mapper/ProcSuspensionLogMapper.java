package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.ProcSuspensionLog;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

;

public interface ProcSuspensionLogMapper extends Mapper<ProcSuspensionLog> {
	
	public List<ProcSuspensionLog> selectSuspensionLogByDealNo (@Param("deal_no")String deal_no);

}
