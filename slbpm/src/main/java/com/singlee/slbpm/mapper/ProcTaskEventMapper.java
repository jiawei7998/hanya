package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.ProcTaskEvent;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface ProcTaskEventMapper extends Mapper<ProcTaskEvent> {

	/**
	 * 查询流程人工任务绑定的触发事件
	 * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
	 * @return
	 */
    List<String> getProcTaskEvents(Map<String, Object> map);
    
    /**
     * 删除流程节点对应的事件
     * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
     */
    void deleteByTask(Map<String, Object> map);
    
}
