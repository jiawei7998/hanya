package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.ProcSignLog;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface ProcSignLogMapper extends Mapper<ProcSignLog> {
	
	public int getVoteCount(Map<String, Object> map);
	
	List<ProcSignLog> getSignLogListByInstId(Map<String, Object> map);
}