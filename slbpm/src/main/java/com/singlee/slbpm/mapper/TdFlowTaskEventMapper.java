package com.singlee.slbpm.mapper;

import com.github.pagehelper.Page;
import com.singlee.slbpm.pojo.bo.TdFlowTaskEvent;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TdFlowTaskEventMapper extends Mapper<TdFlowTaskEvent> {

	public Page<TdFlowTaskEvent> getFlowTaskEventPage(Map<String, Object> map,RowBounds rb);
	
	public List<TdFlowTaskEvent> getFlowTaskEventUnselect(Map<String, Object> map);
	
	public List<TdFlowTaskEvent> getFlowTaskEventSelect(Map<String, Object> map);
	
	public List<TdFlowTaskEvent> getFlowTaskEventSelectByTask(Map<String, Object> map);

	public void addFlowTaskEvent(Map<String, Object> param);

	public void updateFlowTaskEvent(Map<String, Object> param);

	public void deleteFlowTaskEvent(Map<String, Object> param);
}
