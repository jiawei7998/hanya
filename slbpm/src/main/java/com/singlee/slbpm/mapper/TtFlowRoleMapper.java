package com.singlee.slbpm.mapper;

import com.github.pagehelper.Page;
import com.singlee.slbpm.pojo.bo.TtFlowRole;
import org.apache.ibatis.session.RowBounds;
import tk.mybatis.mapper.common.Mapper;

import java.util.Map;

/**
 * 审批角色对应mapper类
 * @author Libo
 *
 */
public interface TtFlowRoleMapper extends Mapper<TtFlowRole> {
	/**
	 * 分页查询
	 * @param map
	 * @param rb
	 * @return
	 */
	public Page<TtFlowRole> selectFlowRolePage(Map<String,Object> map,RowBounds rb);
}