package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.TtFlowOperation;
import tk.mybatis.mapper.common.Mapper;

public interface TtFlowOperationMapper extends Mapper<TtFlowOperation> {

}
