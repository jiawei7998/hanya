package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.ProcTaskUrl;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface ProcTaskUrlMapper extends Mapper<ProcTaskUrl> {

	/**
	 * 查询流程人工任务节点绑定的自定义表单
	 * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
	 * @return
	 */
    List<ProcTaskUrl> getProcTaskUrl(Map<String, Object> map);

    /**
     * 删除流程节点对应的checkList
     * @param map
	 * 			flow_id			流程定义ID
	 * 			task_def_key	流程任务节点KEY
	 * 			prd_code		产品编号
     */
    void deleteByTaskKey(Map<String, Object> map);
}
