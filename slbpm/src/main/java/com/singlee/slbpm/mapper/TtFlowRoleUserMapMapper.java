package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.TtFlowRoleUserMap;
import com.singlee.slbpm.pojo.vo.FlowRoleUserMapVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TtFlowRoleUserMapMapper extends Mapper<TtFlowRoleUserMap> {
	
	/**
	 * 删除指定role_id中，多余的角色信息（不再user_ids中的信息）
	 * @param map
	 * 		role_id: 角色id
	 * 		user_ids:用户id列表，逗号隔开
	 */
	public void deleteFlowRoleUserMapExceptUserIds(Map<String,String> map);
	
	/** 
	 * 获取  
	 * 一般不建议两参数同时为空，同时为空可能返回记录不受控制，导致内存占用
	 * 为空请填 null
	 *@map.user_id 允许为空，
	 *@map.role_id 允许为空 为空则不关注   
	 */
	public List<FlowRoleUserMapVo> listFlowRoleUserMap(Map<String,Object> map);
	
	/**
	 * 清空 转授权 
	 * 清除 柜员（user_id）的所有审批角色柜员映射关系
	 * 一般不建议两参数同时为空，同时为空可能返回记录不受控制，导致内存占用
	 * 为空请填 null
	 *@map.user_id 允许为空，
	 *@map.role_id 允许为空 为空则不关注   
	 * @author LyonChen
	 */
	public void resetFlowRoleUserMap(Map<String, Object> paramMap);
	

	/** 修改 */
	public void updateFlowRoleUserMap(TtFlowRoleUserMap map);
}
