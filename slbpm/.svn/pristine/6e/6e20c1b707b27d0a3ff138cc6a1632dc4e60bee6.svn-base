package com.singlee.slbpm.listener;

import com.singlee.capital.common.spring.SpringContextHolder;
import com.singlee.capital.common.util.DateUtil;
import com.singlee.capital.common.util.JY;
import com.singlee.slbpm.dict.DictConstants;
import com.singlee.slbpm.externalInterface.SlbpmCallBackInteface;
import com.singlee.slbpm.mapper.TdNotificationMapper;
import com.singlee.slbpm.mapper.TtFlowSerialMapMapper;
import com.singlee.slbpm.pojo.bo.TdNotification;
import com.singlee.slbpm.pojo.bo.TtFlowSerialMap;
import com.singlee.slbpm.util.SlbpmVariableNameConstants;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.event.ActivitiEvent;
import org.activiti.engine.delegate.event.ActivitiEventListener;
import org.activiti.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 流程结束事件实现类
 * @author Yang Yang 2017/5/8
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ProcessCompletedListener implements ActivitiEventListener {

    private static Logger logger = LoggerFactory.getLogger("SLBPM");

	/** 流程和外部流水号关系Dao */
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;
	
	@Autowired
	private TdNotificationMapper tdNotificationMapper;
	
    @Override
    public void onEvent(ActivitiEvent event) {
        switch (event.getType()) {
            case PROCESS_COMPLETED:
            	logger.debug("流程正常结束 ---->" + event.getType().name());
				this.procCompletedEventHandler(event);
            	this.remindInitiator(event);
                break;
            case PROCESS_COMPLETED_WITH_ERROR_END_EVENT:
            	logger.debug("流程异常结束 ---->" + event.getType().name());
				this.procCompletedWithError(event);
            	break;
            default:
                logger.debug("收到事件: " + event.getType());
        }
    }

    @Override
    public boolean isFailOnException() {
        return true;
    }
    
    /**
     * 流程结束时，发送提醒给流程发起人
     * @param event
     */
    private void remindInitiator(ActivitiEvent event) {
    	
    	String instance_id = event.getProcessInstanceId();

    	RuntimeService runtimeService = event.getEngineServices().getRuntimeService();    
    	
    	ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(instance_id).singleResult();
    	//业务编号
    	String serial_no = processInstance.getBusinessKey();
    	
		TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, 0);
		String flow_type = flowSerMap.getFlow_type();
		// 通知目标用户为发起人
		String to_user = (String) runtimeService.getVariable(instance_id,SlbpmVariableNameConstants.Initiator);
    	
    	TdNotification tdNotification = new TdNotification();
    	tdNotification.setDeal_no(serial_no);
    	tdNotification.setDeal_type(flow_type);
    	tdNotification.setFrom_user_id("system");
    	tdNotification.setTo_user_id(to_user);
    	tdNotification.setCreate_time(DateUtil.getCurrentDateTimeAsString());
    	tdNotification.setText("流程已审批通过");
    	tdNotificationMapper.insert(tdNotification);    	
    }

	/**
	 * 流程正常结束
	 * @param event
	 */
	private void procCompletedEventHandler(ActivitiEvent event) {
		try {
			String processInstanceId = event.getProcessInstanceId();

			RuntimeService runtimeService = event.getEngineServices().getRuntimeService();

			ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
					.processInstanceId(processInstanceId)
					.singleResult();

			// 根据外部流水号（审批单号）获取流程定义ID
			String serial_no = processInstance.getBusinessKey();
			TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, 0);
			String flow_id = flowSerMap.getFlow_id();
			String flow_type = flowSerMap.getFlow_type();
			String status_change_listener = flowSerMap.getStatus_change_listener();

			// 获取事件分发回调接口
			SlbpmCallBackInteface listener = SpringContextHolder.getBean(status_change_listener);

			// + ATT - 哈尔滨银行特殊需求 - 如果是法审流程，那么流程全局变量内的合同审查岗审批意见需要取出来传入审批完成方法，以便后续判断
			Object obj1 = runtimeService.getVariable(processInstanceId, SlbpmVariableNameConstants.ContractReview);
			String review_advice = (obj1 == null) ? "0" : obj1.toString();

			if (DictConstants.FlowCompleteType.ContractSuggestion.equals(review_advice)
					|| DictConstants.FlowCompleteType.ContractChangeRequired.equals(review_advice)) {

				// 更新最新审批状态 -> 审批通过（可供参考意见/应当修改意见）
				listener.statusChange(flow_type, flow_id, serial_no,
						DictConstants.ApproveStatus.ApprovedPass,
						review_advice);
			}
			// - ATT - 哈尔滨银行特殊需求 - 如果是法审流程，那么流程全局变量内的合同审查岗审批意见需要取出来传入审批完成方法，以便后续判断
			else {
				// 更新最新审批状态 -> 审批通过
				listener.statusChange(flow_type, flow_id, serial_no,
						DictConstants.ApproveStatus.ApprovedPass,
						DictConstants.FlowCompleteType.Pass);
			}
		 } catch (Exception e) {
			JY.raiseRException("流程正常结束处理失败", e);
		}
	}
    
    /**
     * 流程异常结束
     * @param event
     */
    private void procCompletedWithError(ActivitiEvent event){
		try {
			String instance_id = event.getProcessInstanceId();

			RuntimeService runtimeService = event.getEngineServices().getRuntimeService();

			ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
													.processInstanceId(instance_id)
													.singleResult();

			String serial_no = processInstance.getBusinessKey();

			boolean canRedo = (Boolean)processInstance.getProcessVariables().get("canRedo");

			// 根据外部流水号（审批单号）获取流程定义ID
			TtFlowSerialMap flowSerMap = ttFlowSerialMapMapper.get(null, serial_no, null, 0);
			String flow_id = flowSerMap.getFlow_id();
			String flow_type = flowSerMap.getFlow_type();
			String listener = flowSerMap.getStatus_change_listener();

			// 获取事件分发回调接口
			SlbpmCallBackInteface callBack = (SlbpmCallBackInteface)SpringContextHolder.getBean(listener);

			if(!canRedo){
				// 拒绝
				callBack.statusChange(flow_type, flow_id, serial_no,
									  DictConstants.ApproveStatus.ApprovedNoPass,
									  DictConstants.FlowCompleteType.Refuse);
			}
			else{
				// 退回发起，可以重新提交
				callBack.statusChange(flow_type, flow_id, serial_no,
									  DictConstants.ApproveStatus.New,
									  DictConstants.FlowCompleteType.Redo);
			}
		 } catch (Exception e) {
			JY.raiseRException("流程异常结束处理失败", e);
		}
    }
}