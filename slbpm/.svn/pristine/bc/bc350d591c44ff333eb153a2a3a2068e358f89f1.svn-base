package com.singlee.slbpm.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.pagehelper.StringUtil;
import com.singlee.capital.common.spring.mybatis.BatchDao;
import com.singlee.capital.common.util.JY;
import com.singlee.capital.system.mapper.TtInstitutionMapper;
import com.singlee.capital.system.model.TtInstitution;
import com.singlee.capital.system.session.impl.SlSessionHelper;
import com.singlee.slbpm.dict.DictConstants;
import com.singlee.slbpm.mapper.*;
import com.singlee.slbpm.pojo.bo.*;
import com.singlee.slbpm.pojo.vo.ProcDefInfoVo;
import com.singlee.slbpm.pojo.vo.ProcDefPropMapVo;
import com.singlee.slbpm.service.FlowDefService;
import com.singlee.slbpm.service.FlowOpService;
import com.singlee.slbpm.service.FlowQueryService;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
@SuppressWarnings("unused")
public class FlowDefServiceImpl implements FlowDefService {

	private static Logger logger = LoggerFactory.getLogger("SLBPM");
	
	/** 注入activiti自带的RepositoryService接口 */
	@Autowired
    private RepositoryService repositoryService;
	
	@Autowired
    private FlowQueryService flowQueryService;
	
	/** 流程定义Dao */
	@Autowired
	private ProcDefInfoMapper procDefInfoMapper;
	
	/** 流程和产品绑定关系Dao */
	@Autowired
	private ProcDefPropMapMapper procDefMapMapper;
	
	/** 流程人工任务节点自定义表单 Dao */
	@Autowired
	private ProcTaskFormMapper procTaskFormMapper;
	
	/** 流程人工任务节点自定义表单 Dao */
	@Autowired
	private ProcTaskUrlMapper procTaskUrlMapper;
	
	@Autowired
	private ProcTaskEventMapper procTaskEvenMapper;
	
	@Autowired
	private ProcTaskChecklistMapper procTaskChecklistMapper;
	
	@Autowired
	private TtFlowSerialMapMapper ttFlowSerialMapMapper;

	@Autowired
	private FlowOpService flowOpService;
	
	/** Activiti引擎接口 */
	@Autowired
	private ProcessEngine processEngine;
	
	/** Activiti引擎接口 */
	@Autowired
	private TaskService taskService;
	
	/** 批量Dao */
	@Autowired
	private BatchDao batchDao;

	@Autowired
	private TtInstitutionMapper ttInstitutionMapper;

    /**
     * 部署模型 - 新增流程定义
     * @param model_id      模型ID
     * @param pdInfoVo
     * 		  flow_name     流程名称
     * 		  flow_type     流程类型
     * 		  is_active     是否启用
     * @param override		是否升级流程定义[覆盖老版本]
     * @return              JSONObject
     */
    @Override
    public JSONObject addNewProcDef(String model_id, ProcDefInfoVo pdInfoVo, boolean override) throws Exception {

        JSONObject result = new JSONObject();
        String pd_id = "";
        String pd_version = "";
        String deployment_id = "";
        
        // + yangyang - 2017/7/24 - 如果在部署时，同一流程类型下有重名的流程定义，则需要弹出对话框询问用户是否覆盖老版本
        String flow_name = pdInfoVo.getFlow_name();
        String version = pdInfoVo.getVersion();
        String flow_type = pdInfoVo.getFlow_type();
        Map<String, Object> queryMap = new HashMap<String, Object>();
        queryMap.put("flow_name", flow_name);
        queryMap.put("version", version);
        queryMap.put("flow_type", flow_type);
        List<ProcDefInfo> lst_proc_def = procDefInfoMapper.searchDuplicateProcDef(queryMap);
        
        if (lst_proc_def.size() > 0) {     
        	
        	// 如果是第一次进来，那么不覆写只提示
        	if (!override) {
        		
        		result.put("need_override", true);

        		String msg = "";
        		for (ProcDefInfo procDefInfo : lst_proc_def) {
        			msg = msg + "流程名称：" + procDefInfo.getFlow_name() + "\n";
        			msg = msg + "流程定义ID：" + procDefInfo.getFlow_id() + "\n";
        		}
        		
            	result.put("message", "有重名的流程定义，是否需要升级？\n" + msg);
        		return result;
        	} 
        	else {
        		
            	ProcessDefinition proc_def = this.deployModel(model_id, pdInfoVo);
                pd_id = proc_def.getId();
                pd_version = String.valueOf(proc_def.getVersion());
                deployment_id = proc_def.getDeploymentId();
        		
        		// 更新流程定义表，并且更新对应的流程定义业务关联表
        		
        		for (ProcDefInfo procDefInfo : lst_proc_def) {
        			
            		Map<String, Object> updateMap = new HashMap<String, Object>();
            		
            		updateMap.put("old_flow_id", procDefInfo.getFlow_id());
            		updateMap.put("old_version", procDefInfo.getVersion());
            		procDefInfoMapper.disableDuplicateProcDef(updateMap);
            		
            		updateMap.put("new_flow_id", pd_id);
            		updateMap.put("new_version", pd_version);
            		procDefInfoMapper.upgradeProcDefPropMap(updateMap);
				}
        	}
        }
        else {
        	
        	// 全新部署
        	ProcessDefinition proc_def = this.deployModel(model_id, pdInfoVo);
            pd_id = proc_def.getId();
            pd_version = String.valueOf(proc_def.getVersion());
            deployment_id = proc_def.getDeploymentId();
        }
	    // - yangyang - 2017/7/24
	
        // 保存流程定义配置信息到业务表
        ProcDefInfo pd_info = new ProcDefInfo();
        pd_info.setFlow_id(pd_id);
        pd_info.setVersion(pd_version);
        pd_info.setFlow_name(pdInfoVo.getFlow_name());
        pd_info.setFlow_type(pdInfoVo.getFlow_type());
        pd_info.setIs_active(pdInfoVo.getIs_active());
        pd_info.setModel_id(model_id);
        procDefInfoMapper.insert(pd_info);

        pdInfoVo.setFlow_id(pd_id);
        pdInfoVo.setVersion(pd_version);

        result.put("message", String.format("部署成功，流程定义ID=%s，模型ID=%s，部署ID=%s", pd_id, model_id, deployment_id));
        return result;
    }
	
	private ProcessDefinition deployModel(String model_id, ProcDefInfoVo pdInfoVo) throws Exception {

        Model modelData = repositoryService.getModel(model_id);
        ObjectNode modelNode = (ObjectNode)new ObjectMapper().readTree(repositoryService.getModelEditorSource(modelData.getId()));

        BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
        byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);
        String bpmnString = new String(bpmnBytes, "UTF-8");
        String modelName = modelData.getName();
        String processName = modelName + ".bpmn20.xml";

        Deployment deployment = repositoryService.createDeployment()
                                                 .name(modelName)
                                                 .addString(processName, bpmnString)
                                                 .deploy();
        
        String deployment_id = deployment.getId();

        ProcessDefinition proc_def = repositoryService.createProcessDefinitionQuery()
                                                      .deploymentId(deployment_id)
                                                      .singleResult();

        String pd_id = proc_def.getId();

        // 挂起、激活流程定义
        System.out.println(proc_def.isSuspended());
        if ("1".equals(pdInfoVo.getIs_active()) && proc_def.isSuspended()) {
            repositoryService.activateProcessDefinitionById(pd_id, true, null);
        }
        else if ("0".equals(pdInfoVo.getIs_active()) && !proc_def.isSuspended()) {
            repositoryService.suspendProcessDefinitionById(pd_id, true, null);
        }
        
        return proc_def;
	}
	
	/**
     * 查询某个产品类别下的流程定义列表（启用状态）
     * @param 	product_type		产品类型
     * @param	flow_type			流程类型（可选）
     * @return	List<ProcDefInfo>	流程定义列表
     * @author 	杨阳					2017.5.19
     */
	@Override
	public List<ProcDefInfo> searchProcDefByPrdType(String product_type, String flow_type) {
		
		Map<String, Object> map = new HashMap<String, Object>();
        map.put("product_type", product_type);
        map.put("flow_type", flow_type);
        return procDefInfoMapper.searchProcDefByPrdType(map);
	}
    
    /**
     * 根据流程定义查找对应的机构列表
     * @param	flowId				流程定义ID
     * @param	productType			产品类型
     * @return	List<String>		机构ID列表
	 * @author 						杨阳 2017.5.17
     */
    @Override
    public List<String> getInstitutionsByFlowId(String flowId, String productType) {
    	
    	// 取指定流程的最高版本号
    	String version = procDefInfoMapper.getHighVersionNo(flowId);
    			
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("flow_id", flowId);
        map.put("version", version);
        map.put("product_type", productType);
    	
    	return procDefInfoMapper.getInstitutionsByFlowId(map);
    }

    /**
	 * 建立流程定义、产品类型之间的关联关系
	 * 一对流程定义、产品类型会绑定一组特定的机构列表
	 * （三者之间的关系应为1 - M - M*N）
     * @param	flowId				流程定义ID
     * @param	version				流程定义版本
     * @param	productType			产品类型
     * @param	lstInstCode			机构代码列表
	 * @author 						杨阳 2017.5.19
     */
    @Override
    public void bindProcDefProps(String flowId, String version, String productType, 
    							 List<String> lstInstCode,String flow_type) {

		// 如果版本号为空，则去流程定义表里取最高版本
		if(StringUtil.isEmpty(version)){
			version = procDefInfoMapper.getHighVersionNo(flowId);
		}
		
        List<ProcDefPropMap> list = new ArrayList<ProcDefPropMap>();
        
        for (String instCode : lstInstCode) {
        	//先判断有没有
			List<ProcDefInfo> procDefInfos = listProcDef("", flow_type, DictConstants.YesNo.YES, productType, instCode);
			if(procDefInfos!= null && procDefInfos.size() > 0){
				TtInstitution institution = ttInstitutionMapper.getInstById(instCode);
        		JY.raise(institution.getInstName() + "已配置流程,请将原流程删除后再操作", "系统提示");
        		return;
			}
            ProcDefPropMap pdpMap = new ProcDefPropMap();
            pdpMap.setFlow_id(flowId);
            pdpMap.setVersion(version);
            pdpMap.setProduct_type(productType);
            pdpMap.setInst_code(instCode);
            list.add(pdpMap);
        }

        //批量添加
        batchDao.batch("com.singlee.slbpm.mapper.ProcDefPropMapMapper.insert", list);
    }
    
    /**
     * 解除 流程定义/产品类型 之间的绑定关系 （关联机构也随之失效）
     * @param	flowId				流程定义ID
     * @param	version				流程定义版本
     * @param	productType			产品类型
	 * @author 						杨阳 2017.5.19
     */
    @Override
    public void unbindProcDefProps(String flowId, String version, String productType) {
    	
		// 如果版本号为空，则去流程定义表里取最高版本
		if(StringUtil.isEmpty(version)){
			version = procDefInfoMapper.getHighVersionNo(flowId);
		}
    	
    	ProcDefPropMap pdpMap = new ProcDefPropMap();
        pdpMap.setFlow_id(flowId);
        pdpMap.setVersion(version);
        pdpMap.setProduct_type(productType);
    	procDefMapMapper.delete(pdpMap);
    	
    	//*另需解除所有使用这个流程定义的实例
    	List<TtFlowSerialMap> list = ttFlowSerialMapMapper.getList(flowId, null, null, 0, productType);
    	for(TtFlowSerialMap item : list){
			Task task = taskService.createTaskQuery().executionId(Long.toString(item.getExecution())).singleResult();
			if(task != null){
				try {
					flowOpService.completePersonalTask(SlSessionHelper.getUserId(), task.getId(),null, DictConstants.FlowCompleteType.Undo, "", "因流程替换而自动终止", "");
				} catch (Exception e) {
					e.printStackTrace();
					logger.debug(e.getMessage());
				}
			}
    	}
    }

	/**
     * 模糊查询流程定义列表
     * @param 	flow_name     		流程名称
     * @param 	flow_type     		流程类型
     * @param 	is_active     		是否启用
     * @param 	product_type  		产品类型
     * @param 	flow_belong   		机构
     * @return	List<ProcDefInfo>	流程定义对象列表
	 * @author 	杨阳
     */
    @Override
    public List<ProcDefInfo> listProcDef(String flow_name, String flow_type, String is_active, 
    									 String product_type, String flow_belong) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("flow_name", flow_name);
        map.put("flow_type", flow_type);
        // 2017/6/12 - 杨阳 - 默认只取启用中的流程定义
        map.put("is_active", is_active == null ? '1' : is_active);
        map.put("product_type", product_type);
        map.put("flow_belong", flow_belong);
        List<ProcDefInfo> list1=procDefInfoMapper.searchProcDef(map);
        return list1;
    }
    
    /**
     * 根据流程类型查询可用流程
     * @param 	flow_type     		流程类型
     * @return	List<ProcDefInfo>	流程定义对象列表
	 * @author 	杨阳
     */
    @Override
    public List<ProcDefInfo> searchProcDefByType(String flow_type) {

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("flow_type", flow_type);
        // 2017/6/12 - 杨阳 - 默认只取启用中的流程定义
        map.put("is_active", '1');

        return procDefInfoMapper.searchProcDefByType(map);
    }
    
    /**
     * 流程人工任务节点绑定自定义表单
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param form_no				自定义表单ID
     * @param form_fields_json		表单业务要素自定义JSON
     */
    @Override
    public void setProcTaskForm(String flow_id, String version, String task_def_key, 
								String prd_code, String form_no, String form_fields_json) throws Exception {
    	
    	ProcTaskForm procTaskForm = new ProcTaskForm();
    	procTaskForm.setFlow_id(flow_id);
    	procTaskForm.setVersion(version);
    	procTaskForm.setTask_def_key(task_def_key);
    	procTaskForm.setPrd_code(prd_code);
    	procTaskForm.setForm_no(form_no);
    	procTaskForm.setForm_fields_json(form_fields_json);
    	
    	procTaskFormMapper.insert(procTaskForm);    	
    }
    
    /**
     * 流程人工任务节点绑定自定义表单
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param urls				    url
     */
    @Override
    public void setProcTaskFormUrl(String flow_id, String version, String task_def_key, String prd_code, String urls) throws Exception {
    	ProcTaskUrl procTaskForm = new ProcTaskUrl();
    	procTaskForm.setFlow_id(flow_id);
    	procTaskForm.setVersion(version);
    	procTaskForm.setTask_def_key(task_def_key);
    	procTaskForm.setPrd_code(prd_code);
    	procTaskForm.setUrls(urls);
    	ProcTaskUrl result = flowQueryService.getUserTaskUrl(flow_id, task_def_key, prd_code, null);
    	JY.require(result == null, "该节点下已存在审批url,无法重复添加.");
    	procTaskUrlMapper.insert(procTaskForm); 
    }
    
    /**
     * 流程人工任务节点绑定审查要素列表
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param lst_checklist_no		绑定checklist_no
     */
    @Override
	public void setProcTaskChecklist(String flow_id, String version, String task_def_key, 
									 String prd_code, List<String> lst_checktype) throws Exception {
		
		List<ProcTaskChecklist> list = new ArrayList<ProcTaskChecklist>();
		
		for (String checkType : lst_checktype) {
			
			ProcTaskChecklist procTaskChecklist = new ProcTaskChecklist();
			procTaskChecklist.setFlow_id(flow_id);
			procTaskChecklist.setVersion(version);
			procTaskChecklist.setTask_def_key(task_def_key);
			procTaskChecklist.setCk_prd(prd_code);
			procTaskChecklist.setCk_type(checkType);
			list.add(procTaskChecklist);
		}
        
        //批量添加
        batchDao.batch("com.singlee.slbpm.mapper.ProcTaskChecklistMapper.insert", list);
	}
    
    /**
     * 流程人工任务节点绑定审批触发事件
     * @param flow_id				流程定义ID
     * @param version				流程定义版本
     * @param task_def_key			任务节点定义key
     * @param prd_code				产品编号
     * @param lst_event_key			绑定事件key
     */
    @Override
    public void setProcTaskEvents(String flow_id, String version, String task_def_key, 
			  					  String prd_code, List<String> lst_event_key) throws Exception {

        List<ProcTaskEvent> list = new ArrayList<ProcTaskEvent>();
        
        for (String event_key : lst_event_key) {
        	
        	ProcTaskEvent procTaskEvent = new ProcTaskEvent();
        	procTaskEvent.setFlow_id(flow_id);
        	procTaskEvent.setVersion(version);
        	procTaskEvent.setTask_def_key(task_def_key);
        	procTaskEvent.setPrd_code(prd_code);
        	procTaskEvent.setTask_event_key(event_key);
            list.add(procTaskEvent);
        }
        
        //批量添加
        batchDao.batch("com.singlee.slbpm.mapper.ProcTaskEventMapper.insert", list);
    }

    /**
     * 删除人工任务节点绑定的事件列表
     * @param map
     * 			m: task_def_key		任务节点定义key
     * 			m: prd_code			产品编号
     * 			o: flow_id			流程定义ID
     */
	@Override
	public void deleteEventByTask(Map<String, Object> map) {
		procTaskEvenMapper.deleteByTask(map);
	}

    /**
     * 删除人工任务节点绑定的审查要素列表
     * @param map
     * 			m: task_def_key		任务节点定义key
     * 			m: prd_code			产品编号
     * 			o: flow_id			流程定义ID
     */
	@Override
	public void deleteCheckListByTask(Map<String, Object> map) {
		procTaskChecklistMapper.deleteByTask(map);
	}

    /**
     * 删除人工任务节点绑定的自定义表单列表
     * @param map
     * 			m: task_def_key		任务节点定义key
     * 			m: prd_code			产品编号
     * 			o: flow_id			流程定义ID
     */
	@Override
	public void deleteFormByTask(Map<String, Object> map) {
		procTaskFormMapper.deleteByTaskKey(map);
	}
	
	/**
     * 删除人工任务节点绑定的自定义界面地址
     * @param map
     * 			m: task_def_key		任务节点定义key
     * 			m: prd_code			产品编号
     * 			o: flow_id			流程定义ID
     */
	@Override
	public void deleteUrlByTask(Map<String, Object> map) {
		procTaskUrlMapper.deleteByTaskKey(map);
	}
	
	/**
	 * 根据流程编号查询关联产品
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
	@Override
	public List<ProcDefPropMapVo> getPrdInfoByFlow(Map<String, Object> map) {
		map.put("branch_id", SlSessionHelper.getUser().getBranchId());
		List<ProcDefPropMapVo> list = procDefMapMapper.getPrdInfoByFlow(map);
		return list;
	}
	
	/**
	 * 删除流程定义（伪删除，将is_active改为0）
	 * @param map
	 * @return List<ProcDefPropMapVo>
	 * @author wangchen
	 */
	@Override
	public void deleteFlowDef(Map<String, Object> map) {
		procDefInfoMapper.disableDuplicateProcDef(map);
	}
}
