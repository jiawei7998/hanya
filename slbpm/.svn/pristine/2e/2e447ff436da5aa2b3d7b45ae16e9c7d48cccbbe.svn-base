package com.singlee.slbpm.mapper;

import com.singlee.slbpm.pojo.bo.ProcDefInfo;
import com.singlee.slbpm.pojo.vo.ProcDefInfoVo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

/**
 * Created by 杨阳 on 2017/5/3.
 * 流程定义dao
 */
public interface ProcDefInfoMapper extends Mapper<ProcDefInfo> {

    /**
     * 模糊查询流程定义列表
     * @param map
     * flow_name		流程名称
     * flow_type		流程类型
     * is_active		是否启用
     * product_type		产品类型
     * flow_belong		机构
     */
    List<ProcDefInfo> searchProcDef(Map<String, Object> map);
    
    /**
     * 绑定流程定义界面-根据流程类型查询可用流程
     * @param map
     * flow_type		流程类型
     * is_active		是否启用
     */
    List<ProcDefInfo> searchProcDefByType(Map<String, Object> map);

	/**
	 * 取高版本号
	 * @param flow_id	流程ID
	 * @return			该流程的最高版本号
	 */
	public String getHighVersionNo(String flow_id);

	/**
	 * 获取流程定义信息
	 * @param map
	 * @return
	 */
	public ProcDefInfoVo getFlowDefine(Map<String, Object> map);
    
    /**
     * 查询某个产品类别下的流程定义列表（启用状态）
     * @param map
     * product_type		产品类型
     * flow_type		流程类型（可选）
     */
	public List<ProcDefInfo> searchProcDefByPrdType(Map<String, Object> map);
	
	/**
     * 根据流程定义查找对应的机构列表
	 * @param map		
	 * 				flow_id				流程ID
	 * 				version				流程版本
	 * 				product_type		产品类型
     * @return		List<String>		机构ID列表
	 * @author 		杨阳 2017.5.17
     */
	public List<String> getInstitutionsByFlowId(Map<String, Object> map);
	
	/**
	 * 搜索同一流程类型下是否有重名的流程定义
	 * @param map
	 * 				flow_name			流程定义名称
	 * 				version				流程定义版本
	 * 				flow_type			流程类型
	 * @return		List<ProcDefInfo>
	 * @author 		杨阳 2017.7.24
	 */
	public List<ProcDefInfo> searchDuplicateProcDef(Map<String, Object> map);
	
	/**
	 * 部署前，禁用同一流程类型下重名的流程定义
	 * @param map
	 * 				old_flow_id			升级前的流程定义ID
	 * 				old_version			升级前的流程定义版本
	 */
	public void disableDuplicateProcDef(Map<String, Object> map);
	
	/**
	 * 部署后，升级流程定义业务关联表
	 * @param map
	 * 				old_flow_id			升级前的流程定义ID
	 * 				old_version			升级前的流程定义版本
	 * 				new_flow_id			升级后的流程定义ID
	 * 				new_version			升级后的流程定义版本
	 */
	public void upgradeProcDefPropMap(Map<String, Object> map);
	
}