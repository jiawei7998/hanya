package com.singlee.esb.server.socket.impl;


import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.server.socket.bean.MqServerConfig;

import cn.com.agree.mq.api.ApiException;
import cn.com.agree.mq.api.app.MQApplication;
import cn.com.agree.mq.api.app.MQConfig;

/**
 * 用于接收MQ消息,并进行分发
 * 
 * @author shenzl
 * @date 2019/02/25
 */
public class MQOpenAndRecv implements Runnable {

	public MQApplication apprecv = null; // 用于接收
	public MQApplication appsend = null; // 用于发送
	public String appcode = "";
	public String cfgfile = "";
	public static boolean openflag = false;
	public static int count = 0;
	public  static int maxcount = 5; // 可以启动的最大线程数
	public  static int waittime = 1000;// 单位毫秒，建议等待时间不要太长
	private MqServerConfig mqServerConfig;//mq服务端配置
	Log log = LogFactory.getLog(MQOpenAndRecv.class);
    
    /*static{
    	TaUserParamEsbMapper mapper = SpringContextHolder.getBean(TaUserParamEsbMapper.class);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pName", "maxcount");
		maxcount = Integer.parseInt(mapper.getUserParamList(map).get(0).get("P_VALUE").toString());
		map.put("pName", "waittime");
		waittime = Integer.parseInt(mapper.getUserParamList(map).get(0).get("P_VALUE").toString());
    }*/

    private MQOpenAndRecv(String cfgfile, String appcode) {
		this.appcode = appcode;
		this.cfgfile = cfgfile;
		apprecv = new MQApplication(cfgfile, appcode);
		appsend = new MQApplication(cfgfile, appcode);
	}

	public MQOpenAndRecv(MqServerConfig mqServerConfig) {
		this(getCfgFilePath(mqServerConfig.getCfgfile()),mqServerConfig.getAppcodeServer());
		this.mqServerConfig = mqServerConfig;
	}
	
	/**
	 * 获取指定文件的绝对路径
	 * @param cfgfile
	 * @return
	 */
    public static String getCfgFilePath(String cfgfile) {
    	//获取mqapi配置文件的路径
    	String cfgfilePath = null;
		try {
			URL weburl = EsbClient.class.getClassLoader().getResource("WEB-INF/classes");
			URL url = EsbClient.class.getClassLoader().getResource("");
			cfgfilePath = (null == weburl ? url.getPath() : weburl.getPath())+cfgfile;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    	return cfgfilePath;
    }
    @Override
	public void run() {
		try {
			apprecv.opensvr(cfgfile, appcode); // 用于接收的连接
			appsend.open(cfgfile, appcode); // 用于发送的连接
		} catch (ApiException e2) {
			e2.printStackTrace();
			try {
				apprecv.close(); // 所有open打开连接出现异常都执行close
				appsend.close();
			} catch (ApiException e) {
				e.printStackTrace();
			}
		}
		MQOpenAndRecv.openflag = true;
		log.info("开始接收");
		startRecv();
	}

    public void startRecvThread(String cfgfile, String appcode) throws InterruptedException {
		MQConfig mc = new MQConfig();
		boolean flag = true;
		int i = 0;
		String stmp = appcode;
		while (flag) {
			if (i != 0) {
				stmp = appcode + String.valueOf(i);
			}
			if (mc.checkTag(cfgfile, stmp) == 1) {
				log.info("启动一个接收线程 appcode=" + stmp);
				MQOpenAndRecv opc = new MQOpenAndRecv(cfgfile, stmp);
				Thread th = new Thread(opc);
				th.start();
			} else {
				log.warn("已经没有查到可启动的接收线程");
				flag = false;
				break;
			}
			i++;
		}
	}
    public void startRecvThread() throws InterruptedException {
		MQConfig mc = new MQConfig();
		boolean flag = true;
		int i = 0;
		String stmp = appcode;
		while (flag) {
			if (i != 0) {
				stmp = appcode + String.valueOf(i);
			}
			if (mc.checkTag(cfgfile, stmp) == 1) {
				log.info("启动一个接收线程 appcode=" + stmp);
				MQOpenAndRecv opc = new MQOpenAndRecv(cfgfile, stmp);
				Thread th = new Thread(opc);
				th.start();
			} else {
				log.warn("已经没有查到可启动的接收线程");
				flag = false;
				break;
			}
			i++;
		}
	}
    
    public void startRecv() {
		while (openflag) {
			try {
				byte[] messageId = new byte[24];
				byte[] correlationId = new byte[24];
				// 建议判断一下是否有可用的线程来处理交易，如果没有可以等待
				//System.out.println("当前线程数 count=" + MQOpenAndRecv.count);
				
				if (MQOpenAndRecv.count >= MQOpenAndRecv.maxcount) {
					log.warn("==========线程超限==========");
					try {
						Thread.sleep(waittime);// 建议等待时间不要太长
						continue;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				byte[] data = apprecv.receive(messageId, correlationId);
				if (data != null) {
					//System.out.println("==========交易开始==========");
					log.info("接收消息:【" + new String(data) + "】");
					MQManageReply mr = new MQManageReply(this.mqServerConfig);
					mr.mqa = appsend;
					mr.messageId = messageId;
					mr.correlationId = correlationId;
					mr.recvmsg = data;
//					if (this.appcode.equals("server1")) {//为了测试，真实使用时请删除
//						mr.recvmsg = "bbbbbbbbb000000bbbbb".getBytes();
//					}
					Thread th = new Thread(mr); // 创建独立的线程来处理交易
					// System.out.println("" + th.getName());
					th.start();
					//System.out.println(System.currentTimeMillis());
				}
			} catch (ApiException e2) {
				if (e2.getReason() == 2033) {
					if ("reconnection".equals(e2.getMessage())) {
					//	System.out.println("==========appcode:" + appcode+ "==========");
						try {
							appsend.open(cfgfile, appcode); // 如果执行了重连需要再次打开返回的MQ连接
						} catch (ApiException e) {
							e.printStackTrace();
							try {
								appsend.close();
							} catch (ApiException e1) {
								e1.printStackTrace();
							}
						}
					}
					continue; // 2033忽略继续执行接收
				}
				e2.printStackTrace();
				continue; // 忽略ApiException
			}
		}
	}
}
