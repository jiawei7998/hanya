package com.singlee.esb.server.webservice.impl;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.server.webservice.IEsbReceiveBiz;
import com.singlee.esb.server.webservice.IEsbReceiveDispatcher;
import com.singlee.esb.server.webservice.bean.RequestPacketClassTypeBean;
import com.singlee.xstream.utils.XmlUtils;

/**
 * WebService服务端,请求分发处理
 * @author chenxh
 *
 */
public class EsbReceiveDispatcherImpl implements IEsbReceiveDispatcher {

	Log log = LogFactory.getLog(EsbReceiveDispatcherImpl.class);
	
	@Override
	public String recevieEsbMsg(final String requestXml) {
		String responseXml = null;
		IEsbReceiveBiz biz = null;
		String beanId = null;
		try {
			// 根据场境、服务名称创建对应的业务逻辑处理类
			log.debug("EsbReceiveDispatcherImpl recevieEsbMsg method");
			
			log.debug("EsbReceiveDispatcherImpl receive get Service Impl Class");
			
			beanId = EsbReceiveHandler.esbReceiveService.getSpringBeanId(requestXml,EsbReceiveHandler.config);
			
			Class<?> claz = Class.forName(EsbReceiveHandler.config.getSpringContextHolderClassPath());
			Method method = claz.getDeclaredMethod("getBean", java.lang.String.class, java.lang.Class.class);
			biz = (IEsbReceiveBiz) method.invoke(null,beanId, IEsbReceiveBiz.class);

			if (null == biz) {
				log.warn("EsbReceiveDispatcherImpl receive Impl class is not found!");
				throw new NullPointerException("EsbReceiveDispatcherImpl receive Impl class is not found!");
			}
			log.info("EsbReceiveDispatcherImpl receive esb Call Service:"+biz.getClass().getName()+"["+beanId+"]");
			log.info("EsbReceiveDispatcherImpl receive esb xml Packet :" + requestXml);
			RequestPacketClassTypeBean classType = biz.requestPacketClassType();
			log.info("EsbReceiveDispatcherImpl receive esb Packet Class Type:" + classType);
			// 将请求的xml转换成实体对象
			EsbPacket<?> request = EsbPacket.convertRequestDto(requestXml,classType.getBodyClassType(),classType.getSysHeaderClassType(),classType.getAppHeaderClassType(),classType.getLocalHeaderClassType(),classType.getTailClassType(),classType.getEsbPacketDtoAliasName(),classType.getRootNodeAttribute());
			log.debug("EsbReceiveDispatcherImpl receive esb requestDto :" + request);
			// 调用服务类提供的方法并接收返回对象
			EsbPacket<?> responseDto = biz.receive(request);
			log.debug("EsbReceiveDispatcherImpl receive esb responseDto :" + responseDto);
			responseXml = XmlUtils.toXml(responseDto);
			log.info("EsbReceiveDispatcherImpl retrun esb xml Packet :" + responseXml);
		} catch (Exception e) {
			log.error("EsbReceiveDispatcherImpl process client exception!", e);
			if(null == biz && (null == responseXml || responseXml.trim().length() == 0)){
				try {
					EsbPacket<?> retDto = EsbReceiveHandler.esbReceiveService.createReturnDefaultByException();
					if(null != retDto){
						responseXml = XmlUtils.toXml(retDto);
						log.info("EsbReceiveDispatcherImpl Retrun Exception Default xml Packet :" + responseXml);
					}
				} catch (Exception e1) {
					log.error("EsbReceiveDispatcherImpl create Default return Dto exception!", e1);
				}//end try catch
			}//end if
		}//end try catch
		log.debug("EsbReceiveDispatcherImpl recevieEsbMsg end");
		return responseXml;
	}

}
