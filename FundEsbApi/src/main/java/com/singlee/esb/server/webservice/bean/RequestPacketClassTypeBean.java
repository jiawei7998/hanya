package com.singlee.esb.server.webservice.bean;

import java.io.Serializable;

import com.singlee.esb.client.packet.EsbAppHeaderPacket;
import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbLocalHeaderPacket;
import com.singlee.esb.client.packet.EsbSysHeaderPacket;
import com.singlee.esb.client.packet.EsbTailPacket;

/**
 * 设置ESB请求报文类型
 * @author chenxh
 *
 */
public class RequestPacketClassTypeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1345387552868747512L;

	private Class<?> bodyClassType;
	
	private Class<?> sysHeaderClassType;
	
	private Class<?> appHeaderClassType;
	
	private Class<?> localHeaderClassType;
	
	private Class<?> tailClassType;
	
	private String esbPacketDtoAliasName;
	
	private String rootNodeAttribute;
	
	public  RequestPacketClassTypeBean(){}
	
	public <T extends EsbBodyPacket,M extends EsbSysHeaderPacket> RequestPacketClassTypeBean(Class<T> bodyClassType,Class<M> sysHeaderClassType){
		this.bodyClassType = bodyClassType;
		this.sysHeaderClassType = sysHeaderClassType;
	}
	
	public <T extends EsbBodyPacket,M extends EsbSysHeaderPacket> RequestPacketClassTypeBean(Class<T> bodyClassType,Class<M> sysHeaderClassType,String rootNodeAttribute){
		this(bodyClassType,sysHeaderClassType);
		this.rootNodeAttribute = rootNodeAttribute;
	}

	@SuppressWarnings("unchecked")
	public <T extends EsbBodyPacket> Class<T> getBodyClassType() {
		return (Class<T>) bodyClassType;
	}

	public <T extends EsbBodyPacket> void setBodyClassType(Class<T> bodyClassType) {
		this.bodyClassType = bodyClassType;
	}

	@SuppressWarnings("unchecked")
	public <M extends EsbSysHeaderPacket> Class<M> getSysHeaderClassType() {
		return (Class<M>) sysHeaderClassType;
	}

	public <M extends EsbSysHeaderPacket> void setSysHeaderClassType(Class<M> sysHeaderClassType) {
		this.sysHeaderClassType = sysHeaderClassType;
	}

	@SuppressWarnings("unchecked")
	public <N extends EsbAppHeaderPacket> Class<N> getAppHeaderClassType() {
		return (Class<N>) appHeaderClassType;
	}

	public <N extends EsbAppHeaderPacket> void setAppHeaderClassType(Class<N> appHeaderClassType) {
		this.appHeaderClassType = appHeaderClassType;
	}

	@SuppressWarnings("unchecked")
	public <Q extends EsbTailPacket> Class<Q> getTailClassType() {
		return (Class<Q>) this.tailClassType;
	}

	public <Q extends EsbTailPacket> void setTailClassType(Class<Q> tailClassType) {
		this.tailClassType = tailClassType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEsbPacketDtoAliasName() {
		return esbPacketDtoAliasName;
	}

	public void setEsbPacketDtoAliasName(String esbPacketDtoAliasName) {
		this.esbPacketDtoAliasName = esbPacketDtoAliasName;
	}

	public String getRootNodeAttribute() {
		return rootNodeAttribute;
	}

	public void setRootNodeAttribute(String rootNodeAttribute) {
		this.rootNodeAttribute = rootNodeAttribute;
	}
	
	@SuppressWarnings("unchecked")
	public <V extends EsbLocalHeaderPacket> Class<V> getLocalHeaderClassType() {
		return (Class<V>) localHeaderClassType;
	}

	public <V extends EsbLocalHeaderPacket> void setLocalHeaderClassType(Class<V> localHeaderClassType) {
		this.localHeaderClassType = localHeaderClassType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RequestPacketClassTypeBean [bodyClassType=");
		builder.append(bodyClassType);
		builder.append(", sysHeaderClassType=");
		builder.append(sysHeaderClassType);
		builder.append(", appHeaderClassType=");
		builder.append(appHeaderClassType);
		builder.append(", localHeaderClassType=");
		builder.append(localHeaderClassType);
		builder.append(", tailClassType=");
		builder.append(tailClassType);
		builder.append(", esbPacketDtoAliasName=");
		builder.append(esbPacketDtoAliasName);
		builder.append(", rootNodeAttribute=");
		builder.append(rootNodeAttribute);
		builder.append("]");
		return builder.toString();
	}
	
}
