package com.singlee.esb.server.socket.bean;


public class MqServerConfig {
    private String socketCharSet = "GBK";
    private int corePoolSize = 5;
    private int maximumPoolSize = 20;
    private int keepAliveTime = 30;
    private int arrayBlockingQueueSize = 50;
    private int headerLength = 8;
    private String cfgfile = "mqapi.xml";// mqapi.xml文件
    private String appcodeServer = "server";// 服务标识
    private String charset = "GBK";
    private String appcodeClient = "client";
    private String serviceCodeXpath = "Message/ICTRL/SERVICE_CODE";
    
    public String getSocketCharSet() {
        return socketCharSet;
    }

    public void setSocketCharSet(String socketCharSet) {
        this.socketCharSet = socketCharSet;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public int getArrayBlockingQueueSize() {
        return arrayBlockingQueueSize;
    }

    public void setArrayBlockingQueueSize(int arrayBlockingQueueSize) {
        this.arrayBlockingQueueSize = arrayBlockingQueueSize;
    }

    public int getHeaderLength() {
        return headerLength;
    }

    public void setHeaderLength(int headerLength) {
        this.headerLength = headerLength;
    }

    public String getCfgfile() {
        return cfgfile;
    }
    
    public void setCfgfile(String cfgfile) {
        this.cfgfile = cfgfile;
    }

    public String getServiceCodeXpath() {
        return serviceCodeXpath;
    }

    public void setServiceCodeXpath(String serviceCodeXpath) {
        this.serviceCodeXpath = serviceCodeXpath;
    }

    public String getAppcodeServer() {
        return appcodeServer;
    }

    public void setAppcodeServer(String appcodeServer) {
        this.appcodeServer = appcodeServer;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getAppcodeClient() {
        return appcodeClient;
    }

    public void setAppcodeClient(String appcodeClient) {
        this.appcodeClient = appcodeClient;
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MqServerConfig [socketCharSet=");
		builder.append(socketCharSet);
		builder.append(", corePoolSize=");
		builder.append(corePoolSize);
		builder.append(", maximumPoolSize=");
		builder.append(maximumPoolSize);
		builder.append(", keepAliveTime=");
		builder.append(keepAliveTime);
		builder.append(", arrayBlockingQueueSize=");
		builder.append(arrayBlockingQueueSize);
		builder.append(", headerLength=");
		builder.append(headerLength);
		builder.append(", cfgfile=");
		builder.append(cfgfile);
		builder.append(", appcodeServer=");
		builder.append(appcodeServer);
		builder.append(", charset=");
		builder.append(charset);
		builder.append(", appcodeClient=");
		builder.append(appcodeClient);
		builder.append(", serviceCodeXpath=");
		builder.append(serviceCodeXpath);
		builder.append("]");
		return builder.toString();
	}

}
