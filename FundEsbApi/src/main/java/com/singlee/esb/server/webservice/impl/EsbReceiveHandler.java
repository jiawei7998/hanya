package com.singlee.esb.server.webservice.impl;

import org.apache.axis.AxisFault;
import org.apache.axis.MessageContext;
import org.apache.axis.handlers.BasicHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.server.webservice.IEsbReceiveService;
import com.singlee.esb.server.webservice.bean.WebServiceConfigBean;

/**
 * Axis1.x WebService服务端配置文件初始化拦截器处理类
 * @author chenxh
 *
 */
public class EsbReceiveHandler extends BasicHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3111085026587326724L;

	Log log = LogFactory.getLog(EsbReceiveHandler.class);

	/**
	 * 配置文件实现接口定义
	 */
	public static IEsbReceiveService esbReceiveService;
	
	/**
	 * 配置文件实体对象
	 */
	public static WebServiceConfigBean config = null;

	@Override
	public void invoke(MessageContext arg0) throws AxisFault {
		try {
			log.debug("EsbReceiveHandler run invoke");
			//第一次访问时实例化对象
			if (null == esbReceiveService || null == config) {
				String esbReceiveServiceImplClassName = (String) this.getOption("esbReceiveServiceImplClassName");

				log.info("EsbReceiveHandler esbReceiveServiceImplClassName:" + esbReceiveServiceImplClassName);

				Class<?> esbReceiveServiceClz = Class.forName(esbReceiveServiceImplClassName);
				
				esbReceiveService = (IEsbReceiveService) esbReceiveServiceClz.newInstance();
				
				log.debug("EsbReceiveHandler newInstance esbReceiveService:" + esbReceiveService);
				
				config = esbReceiveService.getWebServiceConfigBean();
				
				log.debug("EsbReceiveHandler newInstance WebServiceConfig:" + config);
			}
			log.debug("EsbReceiveHandler invoke end");
		} catch (ClassNotFoundException e) {
			log.error("EsbReceiveHandler invoke ClassNotFoundException", e);
		} catch (InstantiationException e) {
			log.error("EsbReceiveHandler invoke InstantiationException", e);
		} catch (IllegalAccessException e) {
			log.error("EsbReceiveHandler invoke IllegalAccessException", e);
		}

	}

}
