package com.singlee.esb.server.webservice.impl;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.server.webservice.IEsbReceiveDispatcher;
import com.singlee.esb.server.webservice.IEsbReceiveService;
import com.singlee.esb.server.webservice.impl.EsbReceiveHandler;

/**
 * 接收ESB系统HTTP请求,解析请求的报文,根据交易码调用处理逻辑类,并返回报文给ESB系统
 * @author chenxh
 *
 */
public class EsbHttpServletServer extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6887604026825938879L;

	/**
	 * 
	 */

	Log log = LogFactory.getLog(EsbHttpServletServer.class);
	
	private IEsbReceiveDispatcher iEsbReceiveDispatcher;
	
	private IEsbReceiveService iEsbReceiveService;
	
	private String charSet;
	
	@Override
	public void init() throws ServletException {
		String esbReceiveServiceImplClass = null;
		String esbReceiveDispatcherImplClass = null;
		Class<?> claz = null;
		try {
			super.init();
			this.charSet = this.getServletConfig().getInitParameter("CharSet");
			log.info("HttpServletServer.init获取CharSet参数值："+charSet);
			esbReceiveServiceImplClass = this.getServletConfig().getInitParameter("IEsbReceiveServiceImplClass");
			log.info("HttpServletServer.init获取IEsbReceiveServiceImplClass参数值："+esbReceiveServiceImplClass);
			esbReceiveDispatcherImplClass = this.getServletConfig().getInitParameter("IEsbReceiveDispatcherImplClass");
			log.info("HttpServletServer.init获取IEsbReceiveDispatcherImplClass参数值："+esbReceiveDispatcherImplClass);
			
			claz = Class.forName(esbReceiveServiceImplClass);
			this.iEsbReceiveService = (IEsbReceiveService) claz.newInstance();
			log.info("HttpServletServer.init实例IEsbReceiveService对象:"+this.iEsbReceiveService);
			claz = Class.forName(esbReceiveDispatcherImplClass);
			this.iEsbReceiveDispatcher = (IEsbReceiveDispatcher) claz.newInstance();
			log.info("HttpServletServer.init实例IEsbReceiveDispatcher对象:"+this.iEsbReceiveDispatcher);
			
			EsbReceiveHandler.config = iEsbReceiveService.getWebServiceConfigBean();
			EsbReceiveHandler.esbReceiveService = iEsbReceiveService;
			
			log.info("HttpServletServer.init初始化EsbReceiveHandler对象");
		} catch (ClassNotFoundException e) {
			log.error("HttpServletServer.init找不到加载的类路径!", e);
		} catch (InstantiationException e) {
			log.error("HttpServletServer.init类转换异常!", e);
		} catch (IllegalAccessException e) {
			log.error("HttpServletServer.init加载类路径异常!", e);
		}
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("doGet转发到doPost处理");
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("doPot处理Http请求");
		String reqXml = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		StringWriter writer = null;
		String outXml = null;
		byte [] outByte = null;
		
		try {
			dis = new DataInputStream(new BufferedInputStream(req.getInputStream()));
			writer = new StringWriter();
			org.apache.commons.io.IOUtils.copy(dis, writer);
			reqXml = writer.toString();
			log.info("HttpServletServer接收报文长度："+ (null == reqXml ? 0 : reqXml.trim().getBytes(charSet).length));
			log.debug("HttpServletServer接收Http请求内容:"+reqXml);
			log.info("HttpServletServer调用esbReceiveDispatcherImpl分发处理");
			outXml = iEsbReceiveDispatcher.recevieEsbMsg(reqXml);
			log.debug("HttpServletServer调用分发处理返回结果:"+outXml);
			dos = new DataOutputStream(resp.getOutputStream()); 
			outByte = null == outXml ? new byte[1] : outXml.getBytes(charSet);
			log.info("HttpServletServer返回报文长度："+outByte.length);
			log.info("HttpServletServer写入返回内容到输出流");
			dos.write(outByte);
			log.info("HttpServletServer提交输出流");
			dos.flush();
		} catch (Exception e) {
			log.error("HttpServletServer处理异常",e);
		}finally{
			try {
				if(null != dos) {
					dos.close();}
			} catch (Exception e) {
				log.warn("HttpServletServer关闭DataOutputStream流异常",e);
			}
			try {
				if(null != dis) {
					dis.close();}
			} catch (Exception e) {
				log.warn("HttpServletServer关闭DataInputStream异常",e);
			}
			try {
				if(null != writer) {
					writer.close();}
			} catch (Exception e) {
				log.warn("HttpServletServer关闭StringWriter异常",e);
			}
		}//end try catch finally
	}

}
