package com.singlee.esb.server.socket.bean;

import java.io.Serializable;

/**
 * 通讯服务配置文件实体类
 * @author chenxh
 *
 */
public class EsbSocketServerConfig implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -280715725067426048L;

	//一次读取socket数据的最大字节数
	private int maxBytesNum = 8192;

	//Socket服务接口端口号
	private int serverPort = 10010;

	//报文头中存放报文体长度的位数
	private int headerLength = 8;
	
	//响应客户端的超时时间
	private int clientTimeOut = 120;

	//读取SOCKET数据使用的字符集编码
	private String socketCharSet = "UTF-8";
	
	//线程池维护线程的最少数量
	private int corePoolSize = 5;
	
	//线程池维护线程的最大数量
	private int maximumPoolSize = 20;
	
	//线程池维护线程所允许的空闲时间
	private int keepAliveTime = 30;
	
	//线程池队列大小
	private int arrayBlockingQueueSize = 50;
	
	//Esb服务编码在XML文件中的路径
	private String serviceCodeXpath = "service/SYS_HEAD/ServiceCode";
	
	//Esb服务场景在XML文件中的路径
	private String serviceSceneXpath = "service/SYS_HEAD/ServiceScene";

	public int getArrayBlockingQueueSize() {
		return this.arrayBlockingQueueSize;
	}

	public void setArrayBlockingQueueSize(int arrayBlockingQueueSize) {
		this.arrayBlockingQueueSize = arrayBlockingQueueSize;
	}

	public int getCorePoolSize() {
		return this.corePoolSize;
	}

	public void setCorePoolSize(int corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	public int getMaximumPoolSize() {
		return this.maximumPoolSize;
	}

	public void setMaximumPoolSize(int maximumPoolSize) {
		this.maximumPoolSize = maximumPoolSize;
	}

	public int getKeepAliveTime() {
		return this.keepAliveTime;
	}

	public void setKeepAliveTime(int keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	public int getMaxBytesNum() {
		return this.maxBytesNum;
	}

	public void setMaxBytesNum(int maxBytesNum) {
		this.maxBytesNum = maxBytesNum;
	}

	public int getServerPort() {
		return this.serverPort;
	}

	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	public int getClientTimeOut() {
		return this.clientTimeOut;
	}

	public void setClientTimeOut(int clientTimeOut) {
		this.clientTimeOut = clientTimeOut;
	}

	public String getSocketCharSet() {
		return this.socketCharSet;
	}

	public void setSocketCharSet(String socketCharSet) {
		this.socketCharSet = socketCharSet;
	}

	public static long getSerialversionuid() {
		return -280715725067426048L;
	}

	public int getHeaderLength() {
		return this.headerLength;
	}

	public void setHeaderLength(int headerLength) {
		this.headerLength = headerLength;
	}

	public String getServiceCodeXpath() {
		return this.serviceCodeXpath;
	}

	public void setServiceCodeXpath(String serviceCodeXpath) {
		this.serviceCodeXpath = serviceCodeXpath;
	}

	public String getServiceSceneXpath() {
		return this.serviceSceneXpath;
	}

	public void setServiceSceneXpath(String serviceSceneXpath) {
		this.serviceSceneXpath = serviceSceneXpath;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EsbSocketServerConfig [arrayBlockingQueueSize=");
		builder.append(this.arrayBlockingQueueSize);
		builder.append(", clientTimeOut=");
		builder.append(this.clientTimeOut);
		builder.append(", corePoolSize=");
		builder.append(this.corePoolSize);
		builder.append(", headerLength=");
		builder.append(this.headerLength);
		builder.append(", keepAliveTime=");
		builder.append(this.keepAliveTime);
		builder.append(", maxBytesNum=");
		builder.append(this.maxBytesNum);
		builder.append(", maximumPoolSize=");
		builder.append(this.maximumPoolSize);
		builder.append(", serverPort=");
		builder.append(this.serverPort);
		builder.append(", serviceCodeXpath=");
		builder.append(this.serviceCodeXpath);
		builder.append(", serviceSceneXpath=");
		builder.append(this.serviceSceneXpath);
		builder.append(", socketCharSet=");
		builder.append(this.socketCharSet);
		builder.append("]");
		return builder.toString();
	}
}
