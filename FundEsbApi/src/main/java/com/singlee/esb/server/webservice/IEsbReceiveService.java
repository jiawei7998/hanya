package com.singlee.esb.server.webservice;

import org.dom4j.DocumentException;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.server.webservice.bean.WebServiceConfigBean;

/**
 * Axis1.x WebService服务端配置类,每个银行都需要根据自身报文结构实现这个接口
 * @author chenxh
 *
 */
public interface IEsbReceiveService {

	/**
	 * 设置WebService配置信息
	 * @return
	 */
	WebServiceConfigBean getWebServiceConfigBean();
	
	/**
	 * 获取WebService服务端对应某个服务的实现类(SPRING BEAN ID)
	 * @param requestXml
	 * @return
	 * @throws DocumentException
	 */
	String getSpringBeanId(String requestXml,WebServiceConfigBean config) throws DocumentException;
	
	/**
	 * 当WEBSERVICE的分发处理出现未知异常时,返回的默认报文
	 * @param <T>
	 * @return
	 */
	<T extends EsbBodyPacket> EsbPacket<T> createReturnDefaultByException();
}
