package com.singlee.esb.server.webservice.bean;

import java.io.Serializable;

public class WebServiceConfigBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7475960508894396860L;

	//Esb服务编码在XML文件中的路径
	private String serviceCodeXpath ;
	
	//Esb服务场景在XML文件中的路径
	private String serviceSceneXpath ;
	
	////Spring容器管理对象类路径
	private String springContextHolderClassPath;

	public String getServiceCodeXpath() {
		return serviceCodeXpath;
	}

	public void setServiceCodeXpath(String serviceCodeXpath) {
		this.serviceCodeXpath = serviceCodeXpath;
	}

	public String getServiceSceneXpath() {
		return serviceSceneXpath;
	}

	public void setServiceSceneXpath(String serviceSceneXpath) {
		this.serviceSceneXpath = serviceSceneXpath;
	}

	public String getSpringContextHolderClassPath() {
		return springContextHolderClassPath;
	}

	public void setSpringContextHolderClassPath(String springContextHolderClassPath) {
		this.springContextHolderClassPath = springContextHolderClassPath;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WebServiceConfigBean [serviceCodeXpath=");
		builder.append(serviceCodeXpath);
		builder.append(", serviceSceneXpath=");
		builder.append(serviceSceneXpath);
		builder.append(", springContextHolderClassPath=");
		builder.append(springContextHolderClassPath);
		builder.append("]");
		return builder.toString();
	}
	
}
