package com.singlee.esb.server.socket;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;

public abstract interface IEsbReceiveBiz {
	public abstract <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> receive(String paramString) throws Exception;
}