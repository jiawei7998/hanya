package com.singlee.esb.server.webservice;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.server.webservice.bean.RequestPacketClassTypeBean;

/**
 * <p>
 * Title: ESB接收接口,Webservice通讯方法
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * <p>
 * Company: singlee
 * </p>
 * 
 * @author:chenxh
 * @version:20190722
 */
public interface IEsbReceiveBiz {

	/**
	 * 接收ESB的请求报文,处理后返回响应报文到ESB
	 * @param <T>	ESB返回报文类型
	 * @param <K> 请求报文对象
	 * @return
	 * @throws Exception
	 */
	<K extends EsbBodyPacket,T extends EsbBodyPacket> EsbPacket<T> receive(EsbPacket<K> requestDto)throws Exception;
	
	/**
	 * 指定报文BODY、SYSHEADER、APPHEADER、LOCALHEADER类型
	 * @return
	 */
	RequestPacketClassTypeBean requestPacketClassType();
}
