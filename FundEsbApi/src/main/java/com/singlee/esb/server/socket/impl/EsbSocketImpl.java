package com.singlee.esb.server.socket.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.server.socket.IEsbReceiveBiz;
import com.singlee.esb.server.socket.bean.EsbSocketServerConfig;
import com.singlee.esb.utils.SingleeUtils;
import com.singlee.xstream.utils.XmlUtils;

/**
 * EsbSocket服务实现处理类
 * @author chenxh
 *
 */
public class EsbSocketImpl implements Runnable {
	private Socket client;
	
	//Esb Socket对象参数配置类
	private EsbSocketServerConfig config;
	
	//所有服务端实现类
	private Hashtable<String,IEsbReceiveBiz> bizs;
	
	//Spring容器管理对象类路径
	private String springContextHolderClassPath;
	Log log = LogFactory.getLog(EsbSocketImpl.class);

	public EsbSocketImpl(Socket client, Hashtable<String, IEsbReceiveBiz> bizs, EsbSocketServerConfig config, String springContextHolderClassPath) {
		this.client = client;
		this.config = config;
		this.bizs = bizs;
		this.springContextHolderClassPath = springContextHolderClassPath;
	}

	@Override
	public void run() {
		BufferedInputStream in = null;
		BufferedOutputStream output = null;
		IEsbReceiveBiz biz = null;
		String responseXml = null;
		try {
			in = new BufferedInputStream(this.client.getInputStream());
			output = new BufferedOutputStream(client.getOutputStream());
			// 1.0接收数据
			try {
				// 1.1获取报文长度(前8位)
				byte[] tmpcbuf = new byte[8];
				in.read(tmpcbuf);
				String strPacket = new String(tmpcbuf, this.config.getSocketCharSet());

				if ((strPacket == null) || (strPacket.trim().length() == 0)) {
					this.log.error("EsbSocketServer receive ESB msg length(" + this.config.getHeaderLength() + "):Nothing!");
					try {
						if (in != null) {
							in.close();}
					} catch (IOException localIOException) {
					}
					try {
						if (output != null) {
							output.close();}
					} catch (IOException localIOException1) {
					}
					try {
						if (this.client != null) {
							this.client.close();}
					} catch (IOException localIOException2) {
					}
					return;
				}

				int packetLen = Integer.valueOf(strPacket).intValue();
				if (packetLen == 0) {
					this.log.error("EsbSocketServer receive ESB msg length=0!");
					try {
						if (in != null) {
							in.close();}
					} catch (IOException localIOException3) {
					}
					try {
						if (output != null) {
							output.close();}
					} catch (IOException localIOException4) {
					}
					try {
						if (this.client != null) {
							this.client.close();}
					} catch (IOException localIOException5) {
					}
					return;
				}

				this.log.info("EsbSocketServer receive ESB msg length(8):" + strPacket);

				// 1.2 读取compositeData报文
				byte[] cbuf = new byte[packetLen];// 分配存储报文内容字节数组
				//
				tmpcbuf = new byte[config.getMaxBytesNum()];// 每次读取的字节数
				int count = 0;
				int desPos = 0;
				while (-1 != (count = in.read(tmpcbuf))) {
					System.arraycopy(tmpcbuf, 0, cbuf, desPos, count);
					desPos += count;
					if (desPos >= packetLen) {
						break;
					}
				}
				
				//获取请求的XML报文内容
				String requestXml = new String(cbuf,config.getSocketCharSet());
				log.debug("EsbSocketServer revice request xml:"+requestXml);
				
				//根据场境、服务名称创建对应的业务逻辑处理类			
				biz = getBizImplClass(getImplBizKey(requestXml));

				if (biz == null) {
					this.log.warn("EsbSocketServer receive Impl class is not found!");
					throw new NullPointerException("EsbSocketServer receive Impl class is not found!");
				}

				@SuppressWarnings("rawtypes")
				EsbPacket responseDto = biz.receive(requestXml);
				this.log.info("EsbSocketServer receive esb responseDto :" + responseDto);
				responseXml = XmlUtils.toXml(responseDto);
			} catch (Exception e) {
				this.log.error("EsbSocketServer receive esb Packet to exception!", e);
				responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>Impl class is not found!";
			} // end try catch

			// 2.0发送返回的数据
			try {
				this.log.debug("EsbSocketServer response esb xml :" + responseXml);

				// 2.2将返回的CompositeData数据转换成byte[]
				byte[] retByte = responseXml.getBytes(config.getSocketCharSet());
				// 前8位为报文长度,不足补0
				String retLength = String.format("%0"+config.getHeaderLength()+"d", retByte.length);
				log.debug("EsbSocketServer response retLength :" + retLength);
				// 重新拼装报文加上前8位报文长度位
				StringBuffer stringBuffer = new StringBuffer();
				//添加报文头
				stringBuffer.append(retLength);
				// 添加报文体
				stringBuffer.append(responseXml);

				// 2.3发送到ESB
				output.write(stringBuffer.toString().getBytes());
				output.flush();
				
				client.shutdownOutput();
				
				log.info(String.format("EsbSocketServer response to ESB finsh! PackageLength=[ %s ]", retLength));

			} catch (Exception e) {
				log.error("EsbSocketServer response to ESB exception!", e);
			}// end try catch
		} catch (Exception e) {
			log.error("EsbSocketServer process client request Thread exception!", e);
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException e) {
			}
			try {
				if (output != null) {
					output.close();
				}
			} catch (IOException e) {
			}
			try {
				if (client != null) {
					client.close();
				}
			} catch (IOException e) {
			}
		}
	}
	
	/**
	 * 如果有自已配置实现类,则用自已配置得否则根据规则自动去SPRING容器中找
	 * @param bizKey
	 * @return
	 * @throws ClassNotFoundException 
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public IEsbReceiveBiz getBizImplClass(String bizKey) throws ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException{
		IEsbReceiveBiz biz = null;
		
		if(null != bizs){
			log.debug("从MAP中获取服务端实现类,bizKey:"+bizKey);
			biz = bizs.get(bizKey);
		}else{
			log.debug("从Spring中获取服务端实现类");
			Class<?> claz = Class.forName(springContextHolderClassPath);
			Method method = claz.getDeclaredMethod("getBean",java.lang.String.class,java.lang.Class.class);
			biz = (IEsbReceiveBiz) method.invoke(null, bizKey,IEsbReceiveBiz.class);
		}

		return biz;
	}
	
	/**
	 * 获取xml报文中的服务名称与场影
	 * @param xml
	 * @return
	 */
	public String getImplBizKey(String xml){
		// 服务名称
		String serviceCode = null;
		// 服务场景
		String serviceScene = null;
		try {
			log.debug("EsbSocketServer receive getImplBizKey");
			//初始化XML解析器
			SAXReader saxReader = new SAXReader();
			log.debug("EsbSocketServer receive SAXReader");
			
			//将XML文件转换为Document对象
			Document document = saxReader.read(new StringReader(xml));
			
			log.debug("EsbSocketServer receive Document");
			
			//获取服务名称
			serviceCode = (document.selectSingleNode(config.getServiceCodeXpath())).getStringValue();
			log.debug("EsbSocketServer receive ServiceCode:"+serviceCode);
			//获取服务场景
			serviceScene = (document.selectSingleNode(config.getServiceSceneXpath())).getStringValue();
			log.debug("EsbSocketServer receive ServiceScene:"+serviceScene);
		} catch (DocumentException e) {
			log.error("EsbSocketServer process ServiceCode And ServiceScene!", e);
		}

		return SingleeUtils.trim(serviceCode) + "_" + SingleeUtils.trim(serviceScene);
	}
}
