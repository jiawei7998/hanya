package com.singlee.esb.server.socket.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.com.agree.mq.api.ApiException;
import cn.com.agree.mq.api.app.MQApplication;

import com.singlee.esb.server.socket.bean.MqServerConfig;

public class MqTransferServer {
	public MQApplication apprecv = null; // 用于接收
	public MQApplication appsend = null; // 用于发送
    //private Hashtable<String, IEsbReceiveBiz> bizs;//业务队列
    private MqServerConfig mqServerConfig;//线程池相关配置

    //private MQApplication mqApp = null; // MQApplication实例
    //private ThreadPoolExecutor socketServerThreadPool;//线程池处理
    //private String springContextHolderClassPath = "com.singlee.capital.common.spring.SpringContextHolder";
    Log log = LogFactory.getLog(MqTransferServer.class);

    public MqTransferServer() {
    	//获取MqServerConfig的内容
        this(new MqServerConfig());
    }

    public MqTransferServer(MqServerConfig mqServerConfig) {
        this.mqServerConfig = mqServerConfig;
    }
    public void doListenThread() {
         MqTransferServer.this.doListen();
    }

    public void doListen() {
    	//创建 MQOpenAndRecv对象,以便调用方法
    	MQOpenAndRecv rts1 = new MQOpenAndRecv(this.mqServerConfig);
		try {
			//将路径和标识作为参数,创建两个mq对象,用于接收和用于发送
			rts1.startRecvThread();
		} catch (InterruptedException e) {
			this.log.error("MQApplication recvice to Exception", e);
		}
    }
    
    public void closeServerSocket() throws Exception {
    	MQOpenAndRecv.openflag = false;
		try {
			apprecv.close();
			appsend.close();
			this.log.info("MQApplication is closed!");
		} catch (ApiException ae) {
			this.log.error("MQApplication close to Exception", ae);
		}
    }

    public MqServerConfig getMqServerConfig() {
        return mqServerConfig;
    }

    public void setMqServerConfig(MqServerConfig mqServerConfig) {
        this.mqServerConfig = mqServerConfig;
    }
    
}
