package com.singlee.esb.server.socket.impl;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Hashtable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.server.socket.IEsbReceiveBiz;
import com.singlee.esb.server.socket.bean.EsbSocketServerConfig;


/**
 * EsbSocket服务端监听类
 * 
 * @author chenxh
 * 
 */
public class EsbTransferSocketServer {

	//Socket服务对象
	private ServerSocket server = null;
	
	//线程池管理对象
	private ThreadPoolExecutor socketServerThreadPool;

	//Esb Socket对象参数配置类
	private EsbSocketServerConfig config;
	//服务端实现类
	private Hashtable<String, IEsbReceiveBiz> bizs;
	//Spring容器管理对象类路径
	private String springContextHolderClassPath = "com.singlee.financial.common.spring.SpringContextHolder";

	//日志
	Log log = LogFactory.getLog(EsbTransferSocketServer.class);

	public EsbTransferSocketServer() {
		this(new EsbSocketServerConfig());
	}

	public EsbTransferSocketServer(EsbSocketServerConfig config) {
		this.config = config;
	}
	/**
	 * 线程启动
	 */

	public void doListenThread() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				EsbTransferSocketServer.this.doListen();
			}
		}).start();
	}

	/**
	 * 启动服务端侦听
	 * 
	 * @throws IOException
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void doListen(){

		InetAddress inetAddress = null;
		try {
			if (this.config == null) {
				throw new NullPointerException("EsbServerSocket Config is null!");
			}

			// 创建客户端线程池管理对象
			if (this.socketServerThreadPool == null) {
				this.socketServerThreadPool = new ThreadPoolExecutor(this.config.getCorePoolSize(), this.config.getMaximumPoolSize(), this.config.getKeepAliveTime(), TimeUnit.SECONDS,
						new ArrayBlockingQueue(this.config.getArrayBlockingQueueSize()), new ThreadPoolExecutor.CallerRunsPolicy());
			}

			this.log.info("Start ServerSocket at port:" + this.config.getServerPort());

			// 初始化Socket服务端
			this.server = new ServerSocket(this.config.getServerPort());

			// 绶冲数组大小
			this.log.info("receive ESB msg read maxBytesLength:" + this.config.getMaxBytesNum());

			// SOCKET通讯字符集字符集
			this.log.info("receive ESB msg socketCharSet:" + this.config.getSocketCharSet());

			// 服务通讯超时时间
			this.log.info("receive ESB msg ClientSocket timeout:" + this.config.getClientTimeOut());
			// 循环处理客户端请求
			while (true) {
				this.log.info("ESB listen to socket client...");

				// 如果服务对象为空，或者服务对象已关闭则退出循环
				if ((this.server == null) || (this.server.isClosed())) {
					this.log.info("ESB socket server is closed or null");
					break;
				}

				// 等待客户端的连接
				Socket client = this.server.accept();
				inetAddress = client.getInetAddress();
				this.log.info("socket client [" + inetAddress.getHostAddress() + "," + inetAddress.getHostName() + "] connect...");

				// 设置超时时间
				client.setSoTimeout(this.config.getClientTimeOut() * 1000);
				// 来一个客户端，开一个线程，支持多客户端，使用线程池管理线程
				try {
					this.socketServerThreadPool.execute(new EsbSocketImpl(client, this.bizs, this.config, this.springContextHolderClassPath));
				} catch (Exception e) {
					this.log.error("ServerSocket process client request thread exception!", e);
				}
			}// end while
		} catch (SocketTimeoutException e) {
			this.log.warn("client reader timeout: " + e.getMessage());
		} catch (Exception e) {
			if ((this.server != null) && (!this.server.isClosed())) {
				this.log.error("ServerSocket process client exception!", e);}
		} finally {
			if ((this.server != null) && (!this.server.isClosed())) {
				try {
					this.server.close();
				} catch (Exception localException3) {
				}
				this.log.info("ServerSocket process client request finsh and close ServerSocket !");
			}
		}
	}
	/**
	 * 关闭服务端
	 * 
	 * @throws Exception
	 * 
	 * @throws Exception
	 */

	public void closeServerSocket() throws Exception {
		try {
			if ((this.server != null) && (!this.server.isClosed())) {
				this.server.close();
			}
			this.server = null;
			this.log.info("ServerSocket is closed!");
		} catch (Exception e) {
			this.log.error("ServerSocket close to Exception", e);
		}
	}

	public Hashtable<String, IEsbReceiveBiz> getBizs() {
		return this.bizs;
	}

	public void setBizs(Hashtable<String, IEsbReceiveBiz> bizs) {
		this.bizs = bizs;
	}

	public String getSpringContextHolderClassPath() {
		return this.springContextHolderClassPath;
	}

	public void setSpringContextHolderClassPath(String springContextHolderClassPath) {
		this.springContextHolderClassPath = springContextHolderClassPath;
	}

	public EsbSocketServerConfig getConfig() {
		return config;
	}

	public void setConfig(EsbSocketServerConfig config) {
		this.config = config;
	}
	
}