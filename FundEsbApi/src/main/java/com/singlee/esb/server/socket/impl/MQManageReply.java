package com.singlee.esb.server.socket.impl;

import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.server.socket.IEsbReceiveBiz;
import com.singlee.esb.server.socket.bean.MqServerConfig;
import com.singlee.esb.server.socket.impl.MQOpenAndRecv;
import com.singlee.esb.utils.SingleeUtils;
import com.singlee.xstream.utils.XmlUtils;

import cn.com.agree.mq.api.ApiException;
import cn.com.agree.mq.api.app.MQApplication;

public class MQManageReply implements Runnable {
	public MQApplication mqa = null;
	public byte[] messageId = new byte[24];
	public byte[] correlationId = new byte[24];
	public byte[] recvmsg = null;
	Log log = LogFactory.getLog(MQOpenAndRecv.class);
    private Hashtable<String, IEsbReceiveBiz> bizs;//业务队列
    private MqServerConfig mqServerConfig;//线程池相关配置
    
    public MQManageReply(MqServerConfig config) {
    	this.mqServerConfig = config;
    }
    
    //设置读取相应配置文件
    private String springContextHolderClassPath = "com.singlee.financial.common.spring.SpringContextHolder";
    @Override
	public void run() {
		byte[] retByte = null;
		setCount(1);
		// 处理交易代码放在这里，然后生成返回报文
		try {
			 String requestXml = new String(recvmsg, this.mqServerConfig.getCharset());
	         log.debug("MQOpenAndRecv receive MQ msg xml:" + requestXml);
	         // 分发接收到报文
	         IEsbReceiveBiz biz = getBizImplClass(getImplBizKey(requestXml));
	         if (biz == null) {
	             this.log.warn("EsbSocketServer receive Impl class is not found!");
	             throw new NullPointerException("Server receive Impl class is not found!");
	         }
	         EsbPacket<EsbBodyPacket> responseDto = biz.receive(requestXml);
	         this.log.info("EsbSocketServer receive esb responseDto :" + responseDto);
	         String responseXml = XmlUtils.toXml(responseDto);
	         this.log.debug("EsbSocketServer response esb xml :" + responseXml);
	         retByte = responseXml.getBytes(this.mqServerConfig.getCharset());
		} catch (Exception e) {
			
			log.error("生成返回报文时发生异常！", e);
		}

		byte[] data = retByte;
		try {
			mqa.sendWithId(data, messageId, correlationId);
			//System.out.println("返回时间："+System.currentTimeMillis());
			//System.out.println("返回消息长度  ：" + data.length);
			log.info("返回消息  ：【 " + new String(data) + "】");
			//System.out.println("交易处理用时  ："+(System.currentTimeMillis()-currtime));
			//System.out.println(Thread.currentThread());
			log.info("==========交易结束==========");
		} catch (ApiException e) {
			e.printStackTrace();
		} finally {
			setCount(0);
		}
	}

	public synchronized void setCount(int f) {
		if (f == 1) {
			MQOpenAndRecv.count++;
			//System.out.println("MQOpenAndRecv.count++" + MQOpenAndRecv.count);
		} else {
			MQOpenAndRecv.count--;
			//System.out.println("MQOpenAndRecv.count--" + MQOpenAndRecv.count);
		}
	}
	

	 public IEsbReceiveBiz getBizImplClass(String bizKey) throws ClassNotFoundException, SecurityException,
     NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	     IEsbReceiveBiz biz = null;
	
	     if (this.bizs != null) {
	         this.log.debug("从MAP中获取服务端实现类,bizKey:" + bizKey);
	         biz = (IEsbReceiveBiz)this.bizs.get(bizKey);
	     } else {
	         this.log.debug("从Spring中获取服务端实现类");
	         Class<?> claz = Class.forName(this.springContextHolderClassPath);
	         Method method = claz.getDeclaredMethod("getBean", new Class[] {String.class, Class.class});
	         biz = (IEsbReceiveBiz)method.invoke(null, new Object[] {bizKey, IEsbReceiveBiz.class});
	     }
	     return biz;
	 }

	 public String getImplBizKey(String xml) {
	     String serviceCode = null;
	     try {
	         this.log.debug("EsbSocketServer receive getImplBizKey");
	
	         SAXReader saxReader = new SAXReader();
	         this.log.debug("EsbSocketServer receive SAXReader");
	
	         Document document = saxReader.read(new StringReader(xml));
	
	         this.log.debug("EsbSocketServer receive Document");
	
	         serviceCode = document.selectSingleNode(this.mqServerConfig.getServiceCodeXpath()).getStringValue();
	         this.log.debug("EsbSocketServer receive ServiceCode:" + serviceCode);
	
	     } catch (DocumentException e) {
	         this.log.error("EsbSocketServer process ServiceCode And ServiceScene!", e);
	     }
	     return SingleeUtils.trim(serviceCode);
	 }

}
