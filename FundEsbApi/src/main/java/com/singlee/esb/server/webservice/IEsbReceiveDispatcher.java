package com.singlee.esb.server.webservice;

public interface IEsbReceiveDispatcher {

	/**
	 * 接收ESB请求的XML文件 ,根据报文内容分发到相应的处理类进行处理
	 * @param requestXml
	 * @return
	 */
	String recevieEsbMsg(String requestEsbXml);
}
