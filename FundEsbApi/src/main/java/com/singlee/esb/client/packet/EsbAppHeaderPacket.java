package com.singlee.esb.client.packet;

import java.io.Serializable;

/**
 * Esb应用头报文抽象类
 * @author chenxh
 *
 */
public abstract class EsbAppHeaderPacket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 793201452739313148L;

	/**
	 * 组装应用头的节点转换信息
	 * @return
	 */
	public abstract EsbSubPacketConvert getAppHeaderConvert();
}
