package com.singlee.esb.client.packet;

import java.io.Serializable;

/**
 * Esb本地尾报文抽象类
 * @author shenzl
 *
 */
public abstract class EsbTailPacket implements Serializable{

	/**
	 */
	private static final long serialVersionUID = -8367896670133412836L;

	/**
	 * 组装报文体的节点转换信息
	 * @return
	 */
	public abstract EsbSubPacketConvert getTailConvert();
}
