package com.singlee.esb.client.packet;

import com.singlee.xstream.bean.XmlBaseBean;
import com.singlee.xstream.bean.XstreamConfig;
import com.singlee.xstream.utils.XmlUtils;

/**
 * ESB报文对象
 * 
 * @author chenxh
 * 
 * @param <T>
 */
public class EsbPacket<T extends EsbBodyPacket> extends XmlBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -317843229953817899L;

	/**
	 * 系统头
	 */
	private EsbSysHeaderPacket sysHeader;

	/**
	 * 应用头
	 */
	private EsbAppHeaderPacket appHeader;

	/**
	 * 本地头
	 */
	private EsbLocalHeaderPacket localHeader;

	/**
	 * 报文体
	 */
	private T body;
	
	/**
	 * 报文尾
	 */
	private EsbTailPacket tail;

	/**
	 * 当前对象根节点的别名
	 */
	private String esbPacketDtoAliasName;
	
	/**
	 * 指定该属性为当前对象根节点的属性1
	 */
	private String  packge_type;
	/**
	 * 指定该属性为当前对象根节点的属性2
	 */
	private String  soapenvAttr;

	protected EsbPacket() {
		super();
	}
	
	/**
	 * 方法已重载.初始化ESB报文结构,增加报文根节点属性
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param <Q>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param localHeaderClz
	 * @param tailClz
	 * @param esbPacketDtoAliasName
	 * @param rootNodeAttribute
	 * @param rootNodeAttribute2
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket ,Q extends EsbTailPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz,
			Class<M> sysHeaderClz, Class<N> appHeaderClz, Class<V> localHeaderClz,Class<Q> tailClz, String esbPacketDtoAliasName,String rootNodeAttribute,String soapenvAttr) throws InstantiationException, IllegalAccessException {
		EsbPacket<T> responseDto = initEsbPacket(bodyClz,sysHeaderClz,appHeaderClz,localHeaderClz,tailClz,esbPacketDtoAliasName,rootNodeAttribute);
		responseDto.setSoapenvAttr(soapenvAttr);
		return responseDto;
	}
	
	/**
	 * 方法已重载.初始化ESB报文结构,增加重庆银行报文根节点属性
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param localHeaderClz
	 * @param tailClz
	 * @param esbPacketDtoAliasName
	 * @param rootNodeAttribute
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket ,Q extends EsbTailPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz,
			Class<M> sysHeaderClz, Class<N> appHeaderClz, Class<V> localHeaderClz,Class<Q> tailClz, String esbPacketDtoAliasName,String rootNodeAttribute) throws InstantiationException, IllegalAccessException {
		
		EsbPacket<T> responseDto = initEsbPacket(bodyClz,sysHeaderClz,appHeaderClz,localHeaderClz,tailClz,esbPacketDtoAliasName);
		responseDto.setPackge_type(rootNodeAttribute);
		return responseDto;
	}

	/**
	 * 方法已重载.初始化ESB报文结构,增加大连银行EsbTailPacket结尾域
	 * 
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param localHeaderClz
	 * @param esbPacketDtoAliasName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket ,Q extends EsbTailPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz,
			Class<M> sysHeaderClz, Class<N> appHeaderClz, Class<V> localHeaderClz,Class<Q> tailClz, String esbPacketDtoAliasName) throws InstantiationException, IllegalAccessException {
		
		EsbPacket<T> responseDto = initEsbPacket(bodyClz,sysHeaderClz,appHeaderClz,localHeaderClz,esbPacketDtoAliasName);
		
		if (null != tailClz && !tailClz.getName().equalsIgnoreCase(EsbTailPacket.class.getName())) {
			responseDto.setTail(tailClz.newInstance());
		}

		return responseDto;
	}
	
	/**
	 * 方法已重载.初始化ESB报文结构
	 * 
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param localHeaderClz
	 * @param esbPacketDtoAliasName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz,
			Class<M> sysHeaderClz, Class<N> appHeaderClz, Class<V> localHeaderClz, String esbPacketDtoAliasName) throws InstantiationException, IllegalAccessException {
		EsbPacket<T> responseDto = new EsbPacket<T>();

		if (null != sysHeaderClz && !sysHeaderClz.getName().equalsIgnoreCase(EsbSysHeaderPacket.class.getName())) {
			responseDto.setSysHeader(sysHeaderClz.newInstance());
		}

		if (null != appHeaderClz && !appHeaderClz.getName().equalsIgnoreCase(EsbAppHeaderPacket.class.getName())) {
			responseDto.setAppHeader(appHeaderClz.newInstance());
		}
		if (null != localHeaderClz && !localHeaderClz.getName().equalsIgnoreCase(EsbLocalHeaderPacket.class.getName())) {
			responseDto.setLocalHeader(localHeaderClz.newInstance());
		}
		if (null != bodyClz && !bodyClz.getName().equalsIgnoreCase(EsbBodyPacket.class.getName())) {
			responseDto.setBody(bodyClz.newInstance());
		}

		responseDto.setEsbPacketDtoAliasName(esbPacketDtoAliasName);

		return responseDto;
	}

	/**
	 * 方法已重载.初始化ESB报文结构
	 * 
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param localHeaderClz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz,
			Class<M> sysHeaderClz, Class<N> appHeaderClz, Class<V> localHeaderClz) throws InstantiationException, IllegalAccessException {
		return initEsbPacket(bodyClz, sysHeaderClz, appHeaderClz, localHeaderClz,null,null);
	}

	/**
	 * 方法已重载.初始化ESB报文结构
	 * 
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz, Class<M> sysHeaderClz, Class<N> appHeaderClz)
			throws InstantiationException, IllegalAccessException {
		return initEsbPacket(bodyClz, sysHeaderClz, appHeaderClz, EsbLocalHeaderPacket.class, null,null);
	}

	/**
	 * 方法已重载.初始化ESB报文结构
	 * 
	 * @param <T>
	 * @param <M>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param esbPacketDtoAliasName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz, Class<M> sysHeaderClz, String esbPacketDtoAliasName)
			throws InstantiationException, IllegalAccessException {
		return initEsbPacket(bodyClz, sysHeaderClz, EsbAppHeaderPacket.class, EsbLocalHeaderPacket.class, null,esbPacketDtoAliasName);
	}

	/**
	 * 方法已重载.初始化ESB报文结构
	 * @param <T>
	 * @param <M>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param esbPacketType
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket,M extends EsbSysHeaderPacket> EsbPacket<T> initEsbPacket(String esbPacketType,Class<T> bodyClz,Class<M> sysHeaderClz) throws InstantiationException, IllegalAccessException {
		return initEsbPacket(bodyClz,sysHeaderClz,EsbAppHeaderPacket.class,EsbLocalHeaderPacket.class,null,null,esbPacketType);
	}
	
	/**
	 * 方法已重载.初始化ESB报文结构
	 * @param <T>
	 * @param <M>
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket> EsbPacket<T> initEsbPacket(Class<T> bodyClz, Class<M> sysHeaderClz) throws InstantiationException, IllegalAccessException {
		return (EsbPacket<T>) initEsbPacket(bodyClz, sysHeaderClz, EsbAppHeaderPacket.class, EsbLocalHeaderPacket.class,  null,null);
	}

	/**
	 * 获取系统头
	 * 
	 * @param <M>
	 * @param sysHeaderClz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <M extends EsbSysHeaderPacket> M getSysHeader(Class<M> sysHeaderClz) {
		if (null != sysHeader && null != sysHeaderClz && !sysHeaderClz.getName().equalsIgnoreCase(sysHeader.getClass().getName())) {
			throw new RuntimeException("SysHeader definedClass is not equals implements class!");
		}
		if (null == sysHeader) {
			throw new RuntimeException("No Set SysHeader implements class! Please Check EsbPacket.initEsbPacket Method Parameters!");
		}
		return (M) sysHeader;
	}

	/**
	 * 获取应用头
	 * 
	 * @param <N>
	 * @param appHeaderClz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <N extends EsbAppHeaderPacket> N getAppHeader(Class<N> appHeaderClz) {
		if (null != appHeader && null != appHeaderClz && !appHeaderClz.getName().equalsIgnoreCase(appHeader.getClass().getName())) {
			throw new RuntimeException("AppHeader definedClass is not equals implements class!");
		}
		if (null == appHeader) {
			throw new RuntimeException("No Set AppHeader implements class! Please Check EsbPacket.initEsbPacket Method Parameters!");
		}
		return (N) appHeader;
	}

	/**
	 * 获取本地头
	 * 
	 * @param <V>
	 * @param localHeaderClz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <V extends EsbLocalHeaderPacket> V getLocalHeader(Class<V> localHeaderClz) {
		if (null != localHeader && null != localHeader && !localHeaderClz.getName().equalsIgnoreCase(localHeader.getClass().getName())) {
			throw new RuntimeException("LocalHeader definedClass is not equals implements class!");
		}
		if (null == localHeader) {
			throw new RuntimeException("No Set LocalHeader implements class! Please Check EsbPacket.initEsbPacket Method Parameters!");
		}
		return (V) localHeader;
	}

	/**
	 * 获取本地尾
	 * 
	 * @param <Q>
	 * @param tailClz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <Q extends EsbTailPacket> Q getTail(Class<Q> tailClz ) {
		if (null != tail && null != tail && !tailClz.getName().equalsIgnoreCase(tail.getClass().getName())) {
			throw new RuntimeException("tail definedClass is not equals implements class!");
		}
		if (null == tail) {
			throw new RuntimeException("No Set tail implements class! Please Check EsbPacket.initEsbPacket Method Parameters!");
		}
		return (Q) tail;
	}
	
	/**
	 * 方法已重载.初始化ESB报文结构
	 * 2020年11月10日修改:1.增加大连银行EsbTailPacket结尾域;2.增加重庆银行报文根节点属性
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param requestXml
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param localHeaderClz
	 * @param esbPacketDtoAliasName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket,Q extends EsbTailPacket> EsbPacket<T> convertRequestDto(String requestXml,
			Class<T> bodyClz, Class<M> sysHeaderClz, Class<N> appHeaderClz, Class<V> localHeaderClz,Class<Q> tailClz, String esbPacketDtoAliasName,String rootNodeAttribute) throws InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		EsbPacket<T> requestDto = initEsbPacket(bodyClz, sysHeaderClz, appHeaderClz, localHeaderClz, tailClz, esbPacketDtoAliasName,rootNodeAttribute);

		requestDto = (EsbPacket<T>) XmlUtils.fromXML(requestXml, requestDto);

		return requestDto;
	}
	
	/**
	 * 方法已重载.初始化ESB报文结构.
	 * 支持2020年11月10之前的方法参数数个数
	 * @param requestXml
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param localHeaderClz
	 * @param esbPacketDtoAliasName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket,Q extends EsbTailPacket> EsbPacket<T> convertRequestDto(String requestXml,
			Class<T> bodyClz, Class<M> sysHeaderClz, Class<N> appHeaderClz, Class<V> localHeaderClz,String esbPacketDtoAliasName) throws InstantiationException, IllegalAccessException,
			ClassNotFoundException {
		return convertRequestDto(requestXml, bodyClz, sysHeaderClz, appHeaderClz, localHeaderClz, null,esbPacketDtoAliasName,null);
	}

	/**
	 * 方法已重载.初始化ESB报文结构
	 * 
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param requestXml
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param appHeaderClz
	 * @param esbPacketDtoAliasName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket> EsbPacket<T> convertRequestDto(String requestXml,
			Class<T> bodyClz, Class<M> sysHeaderClz, Class<N> appHeaderClz, String esbPacketDtoAliasName) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return convertRequestDto(requestXml, bodyClz, sysHeaderClz, appHeaderClz, null, null,esbPacketDtoAliasName,null);
	}

	/**
	 * 方法已重载.初始化ESB报文结构
	 * 
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param requestXml
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @param esbPacketDtoAliasName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket> EsbPacket<T> convertRequestDto(String requestXml,
			Class<T> bodyClz, Class<M> sysHeaderClz, String esbPacketDtoAliasName) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return convertRequestDto(requestXml, bodyClz, sysHeaderClz, null, null,null, esbPacketDtoAliasName,null);
	}

	/**
	 * 方法已重载.初始化ESB报文结构
	 * 
	 * @param <T>
	 * @param <M>
	 * @param <N>
	 * @param <V>
	 * @param requestXml
	 * @param bodyClz
	 * @param sysHeaderClz
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static <T extends EsbBodyPacket, M extends EsbSysHeaderPacket, N extends EsbAppHeaderPacket, V extends EsbLocalHeaderPacket> EsbPacket<T> convertRequestDto(String requestXml,
			Class<T> bodyClz, Class<M> sysHeaderClz) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return convertRequestDto(requestXml, bodyClz, sysHeaderClz, null, null, null,null,null);
	}
	
	

	public T getBody() {
		return body;
	}

	protected void setBody(T body) {
		this.body = body;
	}

	protected void setSysHeader(EsbSysHeaderPacket sysHeader) {
		this.sysHeader = sysHeader;
	}

	protected EsbAppHeaderPacket getAppHeader() {
		return appHeader;
	}

	protected void setAppHeader(EsbAppHeaderPacket appHeader) {
		this.appHeader = appHeader;
	}

	protected EsbLocalHeaderPacket getLocalHeader() {
		return localHeader;
	}

	protected void setLocalHeader(EsbLocalHeaderPacket localHeader) {
		this.localHeader = localHeader;
	}

	public String getEsbPacketDtoAliasName() {
		return esbPacketDtoAliasName;
	}

	public void setEsbPacketDtoAliasName(String esbPacketDtoAliasName) {
		this.esbPacketDtoAliasName = esbPacketDtoAliasName;
	}
	
	public void setTail(EsbTailPacket tail) {
		this.tail = tail;
	}

	public String getPackge_type() {
		return packge_type;
	}

	public void setPackge_type(String packge_type) {
		this.packge_type = packge_type;
	}

	
	public String getSoapenvAttr() {
		return soapenvAttr;
	}

	public void setSoapenvAttr(String soapenvAttr) {
		this.soapenvAttr = soapenvAttr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

	@Override
	public XstreamConfig initXstreamConfig() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		XstreamConfig config = new XstreamConfig();

		if (null != esbPacketDtoAliasName && esbPacketDtoAliasName.trim().length() > 0) {
			// 指定报文的根
			config.addAliasClass(esbPacketDtoAliasName, getClass());
		} else {
			// 指定报文的根
			config.addAliasClass("Service", getClass());
		}

		// 设置系统头显示名称,不设置默认使用属性名做为XML标签名
		if (null != sysHeader) {
			config.addAliasFiled("sysHeader", sysHeader.getSysHeaderConvert().getSubPacketAliasName(), getClass());
			config.addAliasInterfaceClass("sysHeader", EsbSysHeaderPacket.class, sysHeader.getSysHeaderConvert().getSubPacketClassType());
			config.addAliasInfo(sysHeader.getSysHeaderConvert().getAliasInfos());
		}

		// 设置应用头显示名称,不设置默认使用属性名做为XML标签名
		if (null != appHeader) {
			config.addAliasFiled("appHeader", appHeader.getAppHeaderConvert().getSubPacketAliasName(), getClass());
			config.addAliasInterfaceClass("appHeader", EsbAppHeaderPacket.class, appHeader.getAppHeaderConvert().getSubPacketClassType());
			config.addAliasInfo(appHeader.getAppHeaderConvert().getAliasInfos());
		}

		// 设置本地头显示名称,不设置默认使用属性名做为XML标签名
		if (null != localHeader) {
			config.addAliasFiled("localHeader", localHeader.getLocalHeaderConvert().getSubPacketAliasName(), getClass());
			config.addAliasInterfaceClass("localHeader", EsbLocalHeaderPacket.class, localHeader.getLocalHeaderConvert().getSubPacketClassType());
			config.addAliasInfo(localHeader.getLocalHeaderConvert().getAliasInfos());
		}

		// 调置报文体各标签显示名称,不设置默认使用属性名做为XML标签名
		if (null != body) {
			config.addAliasFiled("body", body.getBodyConvert().getSubPacketAliasName(), getClass());
			config.addAliasInterfaceClass("body", EsbBodyPacket.class, body.getBodyConvert().getSubPacketClassType());
			config.addAliasInfo(body.getBodyConvert().getAliasInfos());
		}
		// 设置本地尾显示名称,不设置默认使用属性名做为XML标签名
		if(null != tail) {
			config.addAliasFiled("tail", tail.getTailConvert().getSubPacketAliasName(), getClass());
			config.addAliasInterfaceClass("tail", EsbTailPacket.class, tail.getTailConvert().getSubPacketClassType());
			config.addAliasInfo(tail.getTailConvert().getAliasInfos());
		}

		// 该属性不用生成标签,必须过滤
		config.addOmitField(getClass(), "esbPacketDtoAliasName");

		return config;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EsbPacket [appHeader=");
		builder.append(appHeader);
		builder.append(", body=");
		builder.append(body);
		builder.append(", esbPacketDtoAliasName=");
		builder.append(esbPacketDtoAliasName);
		builder.append(", localHeader=");
		builder.append(localHeader);
		builder.append(", sysHeader=");
		builder.append(sysHeader);
		builder.append("]");
		return builder.toString();
	}

}
