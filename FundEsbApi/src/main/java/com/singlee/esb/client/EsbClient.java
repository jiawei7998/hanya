package com.singlee.esb.client;

import java.net.URL;

import org.apache.axis.encoding.XMLType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.client.packet.EsbSysHeaderPacket;
import com.singlee.esb.client.packet.EsbTailPacket;
import com.singlee.esb.utils.CommumicationUtil;
import com.singlee.esb.utils.HttpHeadAttrConfig;
import com.singlee.esb.utils.HttpServletConfig;
import com.singlee.esb.utils.TcpConfig;
import com.singlee.xstream.bean.AxisParameterBean;
import com.singlee.xstream.utils.XmlUtils;

/**
 * ESB客户端工具类
 * 
 * @author chenxh
 *
 */
public class EsbClient {

    /**
     * 方法已重载.SOCKET通讯,发送ESB报文对象到ESB服务器
     * 
     * @param <K> ESB请求报文类型
     * @param <T> ESB返回报文类型
     * @param ip ESB服务品IP地址
     * @param port ESB服务器端口
     * @param requestPakcet ESB请求报文
     * @param responsePakcet ESB返回报文
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> send(String ip, int port,
        EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet) throws Exception {
        return send(new TcpConfig(ip, port), requestPakcet, responsePakcet);
    }

    /**
     * 方法已重载.SOCKET通讯,发送ESB报文对象到ESB服务器
     * 
     * @param <K> ESB请求报文类型
     * @param <T> ESB返回报文类型
     * @param tcpConfig TCP配置类
     * @param requestPakcet ESB请求报文
     * @param responsePakcet ESB返回报文
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> send(TcpConfig tcpConfig,
        EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet) throws Exception {
        return send(tcpConfig, null, requestPakcet, responsePakcet);
    }

    /**
     * 方法已重载.SOCKET通讯,发送ESB报文对象到ESB服务器
     * 
     * @param <K> ESB请求报文类型
     * @param <T> ESB返回报文类型
     * @param tcpConfig TCP配置类
     * @param headerMsg 自定义报文头
     * @param requestPakcet ESB请求报文
     * @param responsePakcet ESB返回报文
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> send(TcpConfig tcpConfig,
        String headerMsg, EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet) throws Exception {
    	Log log = LogFactory.getLog(EsbClient.class);
        log.debug("进入Socket ESB发送方法");

        String esbXml = null;
        String xsdName = null;

        // 将esb的报文对象转换成xml文件
        esbXml = XmlUtils.toXml(requestPakcet);

        log.info("ESB请求对象" + requestPakcet.getBody().getClass().getName() + "转换XML:" + esbXml);

        // 获取报文体的类名
        xsdName = requestPakcet.getBody().getClass().getName();

        // 组装xsd文件名称
        xsdName = xsdName.substring(xsdName.lastIndexOf(".") + 1);

        log.info("Esb报文体校验scham名称:" + xsdName);

        // 用xsd文件校验待发送的xml报文格式是否正常
        // XmlUtils.validate(String.format("%s/%s.xsd", schamDir,xsdName), esbXml);

        // 调用服务器方法并返回结果
        esbXml = CommumicationUtil.callSocket(tcpConfig, headerMsg, esbXml);

        log.info("Esb响应对象待转换XML:" + esbXml);

        return (EsbPacket<T>)XmlUtils.fromXML(esbXml, responsePakcet);
    }

    /**
     * 方法已重载.WebService通讯,发送ESB报文对象到ESB服务器
     * 
     * @param <K>
     * @param <T>
     * @param wsdl
     * @param requestPakcet
     * @param responsePakcet
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> send(String wsdl,
        EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet) throws Exception {
        return send(wsdl, "esbRequest", "esbRequestRequest", requestPakcet, responsePakcet);
    }

    /**
     * 方法已重载.WebService通讯,发送ESB报文对象到ESB服务器
     * 
     * @param <K>
     * @param <T>
     * @param wsdl
     * @param method
     * @param parmName
     * @param requestPakcet
     * @param responsePakcet
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket, M extends EsbSysHeaderPacket> EsbPacket<T>
        send(String wsdl, String method, String parmName, EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet)
            throws Exception {
    	Log log = LogFactory.getLog(EsbClient.class);
        log.debug("进入Webservice ESB发送方法");

        String esbXml = null;
        String xsdName = null;

        // 将esb的报文对象转换成xml文件
        esbXml = XmlUtils.toXml(requestPakcet);

        log.info("Esb请求报文:" + esbXml);

        // 获取报文体的类名
        xsdName = requestPakcet.getBody().getClass().getName();

        // 组装xsd文件名称
        xsdName = xsdName.substring(xsdName.lastIndexOf(".") + 1);

        log.info("Esb报文体校验scham名称:" + xsdName);

        // 用xsd文件校验待发送的xml报文格式是否正常
        // XmlUtils.validate(String.format("%s/%s.xsd", schamDir,xsdName), esbXml);

        // 调用服务器方法并返回结果
        // 调用服务器方法并返回结果
        esbXml = CommumicationUtil.callAxisClientWebService(String.class, wsdl, method,
            new AxisParameterBean(parmName, XMLType.XSD_STRING, esbXml));

        log.info("Esb响应报文:" + esbXml);

        return (EsbPacket<T>)XmlUtils.fromXML(esbXml, responsePakcet);
    }

    /**
     * 方法已重载.MQ通讯,发送MQ报文对象到服务器
     * 
     * @param mqapi mqAPI文件地址
     * @param requestPakcet 请求消息
     * @param responsePakcet 返回消息
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket, M extends EsbSysHeaderPacket,Q extends EsbTailPacket> EsbPacket<T>
        send(EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet, String charset) throws Exception {
    	Log log = LogFactory.getLog(EsbClient.class);
        log.debug("进入MQ 发送方法");
        // 配置文件
        URL weburl = EsbClient.class.getClassLoader().getResource("WEB-INF/classes");
        URL url = EsbClient.class.getClassLoader().getResource("");
        String cfgfile =  (null == weburl ? url.getPath() : weburl.getPath())+ "mqapi.xml";
        
        log.debug("EsbClient get mq file path:" + cfgfile);
        // 应用代码
        String appcode = "client";

        String esbXml = null;
        String xsdName = null;

        // 将esb的报文对象转换成xml文件
        esbXml = XmlUtils.toXml(requestPakcet).replaceFirst("encoding[a-zA-Z0-9\"-=]{1,}", "encoding=\"GBK\"");;

        log.debug("MQ请求报文:" + esbXml);
        System.out.println("MQ请求报文:" + esbXml);

        // 获取报文体的类名
        xsdName = requestPakcet.getBody().getClass().getName();

        // 组装xsd文件名称
        xsdName = xsdName.substring(xsdName.lastIndexOf(".") + 1);

        log.debug("MQ报文体校验scham名称:" + xsdName);

        // 用xsd文件校验待发送的xml报文格式是否正常
        // XmlUtils.validate(String.format("%s/%s.xsd", schamDir,xsdName), esbXml);

        // 调用服务器方法并返回结果
        esbXml = CommumicationUtil.callMq(cfgfile, appcode, esbXml, charset);

        log.debug("MQ响应报文:" + esbXml);
        System.out.println("MQ响应报文:" + esbXml);

        return (EsbPacket<T>)XmlUtils.fromXML(esbXml, responsePakcet);
    }
    
    /**
     * 方法已重载.HttpServlet通讯,发送XML报文对象到服务器
     * @param httpConfig
     * @param requestPakcet
     * @param responsePakcet
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> send(HttpServletConfig httpConfig,
            EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet) throws Exception {
    	EsbPacket<T> outPacket = null;
    	//初始化日志类
    	Log log = LogFactory.getLog(EsbClient.class);
        log.debug("进入HttpServlet ESB发送方法");

        String esbXml = null;
        
        log.info("EsbHttpServlet请求对象:" + requestPakcet);

        // 将esb的报文对象转换成xml文件
        esbXml = XmlUtils.toXml(requestPakcet);

        // 调用服务器方法并返回结果
        esbXml = CommumicationUtil.callHttpServletServer(httpConfig, esbXml);
        
        //将响应XML文件转成对象
        outPacket =  (EsbPacket<T>)XmlUtils.fromXML(esbXml, responsePakcet);

        log.info("EsbHttpServlet响应对象:" + outPacket);
        
        log.debug("HttpServlet ESB发送方法结束");
        return outPacket;
    }
    
    /**
     * 方法已重载.HttpServlet通讯,发送XML报文对象到服务器
     * @param httpConfig
     * @param requestPakcet
     * @param responsePakcet
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> send(HttpHeadAttrConfig httpHeadAttrConfig,
            EsbPacket<K> requestPakcet, EsbPacket<T> responsePakcet) throws Exception {
    	EsbPacket<T> outPacket = null;
    	//初始化日志类
    	Log log = LogFactory.getLog(EsbClient.class);
        log.debug("进入HttpServlet ESB发送方法");

        String esbXml = null;
        
        log.info("EsbHttpServlet请求对象:" + requestPakcet);

        // 将esb的报文对象转换成xml文件
        esbXml = XmlUtils.toXml(requestPakcet);

        // 调用服务器方法并返回结果
        esbXml = CommumicationUtil.callHttpServletMd5Server(httpHeadAttrConfig, esbXml);
        
        //将响应XML文件转成对象
        outPacket =  (EsbPacket<T>)XmlUtils.fromXML(esbXml, responsePakcet);

        log.info("EsbHttpServlet响应对象:" + outPacket);
        
        log.debug("HttpServlet ESB发送方法结束");
        return outPacket;
    }
    
    /**
     * 方法已重载.HttpServlet通讯,发送XML报文对象到服务器
     * @param url
     * @param requestPakcet
     * @param responsePakcet
     * @param charset
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> send(String url,EsbPacket<K> requestPakcet, 
    		EsbPacket<T> responsePakcet,String charset) throws Exception {
    	return send(new HttpServletConfig(url, charset),requestPakcet,responsePakcet);
    }
    
}
