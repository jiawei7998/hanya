package com.singlee.esb.client.converter;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.client.packet.EsbSubPacketConvert;
import com.singlee.xstream.bean.AliasInfo;
import com.singlee.xstream.bean.AliasTypeEnum;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * List对象多个相同标签转换器
 * @author chenxh
 *
 */
public class ListMultipleSameTagsConvert implements Converter {
	
	/**
	 * 别名转换
	 */
	private List<AliasInfo> aliasInfos;
	
	Log log = LogFactory.getLog(ListMultipleSameTagsConvert.class);
	
	public ListMultipleSameTagsConvert(EsbSubPacketConvert esbSubPacketConvert) {
		super();
		if(null != esbSubPacketConvert){
			if(null != esbSubPacketConvert.getAliasInfos()){
				aliasInfos = esbSubPacketConvert.getAliasInfos();
				aliasInfos.add(new AliasInfo("", esbSubPacketConvert.getSubPacketAliasName(), esbSubPacketConvert.getSubPacketClassType(), esbSubPacketConvert.getSubPacketAliasName(), AliasTypeEnum.Unmarshal));
			}//end if
		}//end if
	}
	
	/**
	 * 获取list标签对应的别名信息
	 * @param fieldName
	 * @return
	 */
	private String aliasList(String fieldName){
		for (AliasInfo aliasInfo : aliasInfos) {
			if(!aliasInfo.getAliasType().equals(AliasTypeEnum.Unmarshal)){
				continue;
			}
			if(fieldName.equalsIgnoreCase(aliasInfo.getFieldName())){
				return aliasInfo.getAliasName();
			}
		}// end for
		return null;
	}
	
	/**
	 * 获取List标签别名对应的属性名
	 * @param aliasName
	 * @return
	 */
	private String unAliasList(String aliasName){
		for (AliasInfo aliasInfo : aliasInfos) {
			if(!aliasInfo.getAliasType().equals(AliasTypeEnum.Unmarshal)){
				continue;
			}
			if(aliasName.equalsIgnoreCase(aliasInfo.getAliasName())){
				return aliasInfo.getFieldName();
			}
		}// end for
		return null;
	}
	
	/**
	 * 获取List Item Class 对应的别名
	 * @param ImplClass
	 * @return
	 */
	private String aliasClass(Class<?> ImplClass){
		for (AliasInfo aliasInfo : aliasInfos) {
			if(!aliasInfo.getAliasType().equals(AliasTypeEnum.Unmarshal)){
				continue;
			}
			if(ImplClass.getName().equalsIgnoreCase(aliasInfo.getImplClass().getName())){
				return aliasInfo.getAliasImplClassName();
			}
		}// end for
		return null;
	}
	
	/**
	 * 获取List Item Class别名对应的对象类型
	 * @param aliasImplClassName
	 * @return
	 */
	private Class<?> unAliasClass(String aliasImplClassName){
		for (AliasInfo aliasInfo : aliasInfos) {
			if(!aliasInfo.getAliasType().equals(AliasTypeEnum.Unmarshal)){
				continue;
			}
			if(aliasImplClassName.equalsIgnoreCase(aliasInfo.getAliasImplClassName())){
				return aliasInfo.getImplClass();
			}
		}// end for
		return null;
	}
	
	/**
	 * 获取List Item Class别名对应的list属性名
	 * @param aliasImplClassName
	 * @return
	 */
	private String unListFieldAlias(String aliasImplClassName){
		for (AliasInfo aliasInfo : aliasInfos) {
			if(!aliasInfo.getAliasType().equals(AliasTypeEnum.Unmarshal)){
				continue;
			}
			if(aliasImplClassName.equalsIgnoreCase(aliasInfo.getAliasImplClassName())){
				return aliasInfo.getFieldName();
			}
		}// end for
		return null;
	}
	@Override
	public void marshal(Object arg0, HierarchicalStreamWriter arg1, MarshallingContext arg2) {
		wirte(arg0,arg1,arg2);
	}
	
	/**
	 * 将对象转换为XML标签
	 * @param obj
	 * @param arg1
	 * @param arg2
	 */
	@SuppressWarnings("unchecked")
	private void wirte(Object obj,HierarchicalStreamWriter arg1, MarshallingContext arg2){
		try {
			Field [] fields = obj.getClass().getDeclaredFields();
			
			//循环当前对象下所有属性,并转换为xml标签
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				
				//过滤序列化ID
				if("serialVersionUID".equalsIgnoreCase(field.getName())){
					continue;
				}
				
				//设置当前字段可访问
				field.setAccessible(true);
				
				//根据对象属性的类型,设置相应的标签
				if(field.getType().equals(java.lang.String.class) ||
						field.getType().equals(java.math.BigDecimal.class) ||
						field.getType().equals(java.lang.Integer.class) || 
						field.getType().equals(java.lang.Double.class) ||
						field.getType().equals(java.lang.Float.class) ){
					//处理字符串
					//处理数字类型
					if(null != field.get(obj)){
						arg1.startNode(field.getName());
						arg1.setValue(field.get(obj).toString());
						arg1.endNode();
					}
				}else if(field.getType().equals(java.util.Date.class)){
					//处理Date
					if(null != field.get(obj)){
						arg1.startNode(field.getName());
						arg1.setValue(new SimpleDateFormat("yyyy-MM-dd").format(field.get(obj)));
						arg1.endNode();
					}
				}else if(field.getType().equals(java.util.List.class)){
					//处理list集合对象
					String listName = aliasList(field.getName());
					arg1.startNode(listName == null || listName.trim().length() == 0 ? field.getName() : listName);
					ArrayList list = (ArrayList) field.get(obj);
					if(null != list){
						for (Object var : list) {
							//处理集合中每一个对象
							String name = aliasClass(var.getClass());
							arg1.startNode(null == name || name.trim().length() == 0 ? var.getClass().getName():name);
							wirte(var,arg1,arg2);
							arg1.endNode();
						}//end for
					}//end if
					arg1.endNode();
				}else{
					//处理自定义对象
					Object object = field.get(obj);
					if(null != object){
						arg1.startNode(field.getName());
						wirte(object,arg1,arg2);
						arg1.endNode();
					}//end if 
				}//end if else if ....
				
				//关闭属性访问
				field.setAccessible(false);
			}
		} catch (SecurityException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
		}
	}
	@Override
	public Object unmarshal(HierarchicalStreamReader arg0, UnmarshallingContext arg1) {
		Object obj = null;
		try {
			//获取要转换对象的类型
			Class<?> clz = unAliasClass(arg0.getNodeName());
			
			//实例化转换对象
			if(null != clz){
				obj = clz.newInstance();
			}
			
			//递归调用将XML标签转换为对象
			reader(arg0.getNodeName(),obj,arg0,arg1,null);
		} catch (InstantiationException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
		}catch (SecurityException e) {
			log.error(e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			log.error(e.getMessage(), e);
		} catch (NoSuchFieldException e) {
			log.error(e.getMessage(), e);
		} catch (ParseException e) {
			log.error(e.getMessage(), e);
		}
		
		return obj;
	}
	
	/**
	 * 将Xml标签转换为对象
	 * @param rootName
	 * @param obj
	 * @param arg0
	 * @param arg1
	 * @param list
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ParseException
	 */
	@SuppressWarnings("unchecked")
	private void reader(String rootName,Object obj,HierarchicalStreamReader arg0,UnmarshallingContext arg1,ArrayList list) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, InstantiationException, ParseException{
		
		//移动到下一个标签
		arg0.moveDown();
		
		if(arg0.hasMoreChildren()){
			//该标签有子标签处理
			String arrayName = unAliasList(arg0.getNodeName());
			if(null != arrayName && arrayName.trim().length() >0 ){
				//如果是List标签,实例化list对象
				ArrayList arraylist = new ArrayList();
				//递归处理List标签的子标签
				reader(rootName,obj,arg0,arg1,arraylist);
			}else{
				//获取对象中List属性对象
				Field fieldList = obj.getClass().getDeclaredField(unListFieldAlias(arg0.getNodeName()));
				//设置List属性字段可用
				fieldList.setAccessible(true);
				//获取当前List Ietm对象对应的类型
				Class<?> clz = unAliasClass(arg0.getNodeName());
				//实例化List Ietm对象
				Object var = clz.newInstance();
				//递归处理List Ietm对象的属性
				reader(rootName,var,arg0,arg1,list);
				//将List Ietm对象添加到ArrayList中
				list.add(var);
				//将ArrayList设置到父对象中
				fieldList.set(obj, list);
			}
		}else{
			//该标签为叶子节点处理
			Object var = null;
			String strValue = null;
			//获取叶子节点的Field对象
			Field field = obj.getClass().getDeclaredField(arg0.getNodeName());
			//保存当前xml标签的值
			strValue = arg0.getValue();
			//如果当前节点有值则为对象属性赋值,否则不做处理
			if(null != strValue && strValue.trim().length() > 0){
				//设置可以操作属性
				field.setAccessible(true);
				if(field.getType().equals(java.lang.String.class) ||
						field.getType().equals(java.math.BigDecimal.class) ||
						field.getType().equals(java.lang.Integer.class) || 
						field.getType().equals(java.lang.Double.class) ||
						field.getType().equals(java.lang.Float.class) ){
					//处理字符串
					//处理数字类型
					var = strValue;
				}else if(field.getType().equals(java.util.Date.class)){
					//处理Date
					var = new SimpleDateFormat("yyyy-MM-dd").parse(strValue);
				}//end if else if
				field.set(obj, var);
				//设置属性不可以操作
				field.setAccessible(false);
			}//end if
		}//end else
		//回退到上一个XML标签
		arg0.moveUp();
		if(arg0.getNodeName().equalsIgnoreCase(rootName) && !arg0.hasMoreChildren()){
			//如果读到该对象的结束标签则结束
			return;
		}else if(arg0.hasMoreChildren()){
			//如果XML的标签还没有读完,则递归续继处理
			reader(rootName,obj,arg0,arg1,list);
		}
		
	}
	@Override
	public boolean canConvert(Class arg0) {
		return true;
	}

}
