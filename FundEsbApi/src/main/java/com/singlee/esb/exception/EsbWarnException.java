package com.singlee.esb.exception;

public class EsbWarnException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2516011339825519940L;

	private  String ret_code;
	
	private  String ret_msg;
	
	private  String ret_status;

	public String getRet_status() {
		return ret_status;
	}

	public void setRet_status(String retStatus) {
		ret_status = retStatus;
	}

	public  String getRet_code() {
		return ret_code;
	}

	public  String getRet_msg() {
		return ret_msg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setRet_code(String retCode) {
		ret_code = retCode;
	}

	public void setRet_msg(String retMsg) {
		ret_msg = retMsg;
	}


	public EsbWarnException(String message) {
		super(message);
	}
	
	public EsbWarnException(String ret_code,String ret_msg) {
		super(ret_msg);
		this.ret_code = ret_code;
		this.ret_msg = ret_msg;
	}
}
