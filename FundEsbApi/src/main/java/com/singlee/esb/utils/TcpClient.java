package com.singlee.esb.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.server.socket.impl.EsbTransferSocketServer;

/**
 * SOCKET客户端工具类
 * 
 * @author chenxh
 * 
 */
public class TcpClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8636329147431668584L;
	/**
	 * 报文头信息
	 */
	private String headerMsg;

	/**
	 * 保存服务器地址,端口,超时时间等参数信息
	 */
	private TcpConfig tcpConfig;

	/**
	 * 日志对象
	 */
	Log log = LogFactory.getLog(EsbClient.class);

	protected TcpClient(TcpConfig tcpConfig) {
		this.tcpConfig = tcpConfig;
	}

	/**
	 * 发送请求报文,并接收服务器返回报文
	 * 
	 * @param requestXml
	 * @return
	 * @throws Exception
	 */
	public String send(String requestXml) throws Exception {
		String msg = null;
		BufferedInputStream in = null;
		BufferedOutputStream output = null;
		Socket client = null;

		try {
			if (null == tcpConfig) {
				throw new NullPointerException("TcpConfig bean is null");
			}

			if (tcpConfig.getAddress().isEmpty() == true) {
				throw new NullPointerException("TcpConfig.Address is null");
			}

			if (tcpConfig.getPort() <= 0) {
				throw new NullPointerException("TcpConfig.Port is null");
			}

			log.info("Esb Socket配置信息:" + tcpConfig);

			// 创建客户端套接字
			client = new Socket(tcpConfig.getAddress(), tcpConfig.getPort());

			if (client == null) {
				throw new NullPointerException("Create Socket Object is null");
			}

			if (client.isClosed() == true) {
				throw new RuntimeException("Socket is Closed");
			}

			// 设置读取超时时间
			client.setSoTimeout(tcpConfig.getTimeout() * 1000);

			// 缓冲输入输出流
			in = new BufferedInputStream(client.getInputStream());
			output = new BufferedOutputStream(client.getOutputStream());

			// 发送请求报文到服务端
			send(output, requestXml);

			// 将返回的字节数组转换为字符串
			msg = new String(receive(in), "UTF-8").trim();

			log.info("Esb Socket响应报文体信息:" + msg);

		} catch (NumberFormatException e) {
			throw e;
		} catch (UnknownHostException e) {
			throw e;
		} catch (SocketException e) {
			throw e;
		} catch (UnsupportedEncodingException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} finally {
			if (null != output) {
				try {
					output.close();
				} catch (IOException e) {
				}
			}// end if
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}// end if
			if (null != client) {
				try {
					client.shutdownOutput();
				} catch (IOException e1) {
				}
				try {
					client.shutdownInput();
				} catch (IOException e1) {
				}
				try {
					client.close();
				} catch (IOException e) {
				}
			}// end if
		}// end try catch

		return msg;
	}

	/**
	 * 接收并解析返回报文
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public byte[] receive(BufferedInputStream in) throws IOException {
		byte[] tmpbuff = null;
		String headerStr = null;

		if (tcpConfig.getHeaderLength() <= 0) {
			throw new NullPointerException("TcpConfig.HeaderLength is null");
		}

		// 创建报文头字节数组
		byte[] header = new byte[tcpConfig.getHeaderLength()];
		// 获取报文头信息
		in.read(header);

		headerStr = new String(header, tcpConfig.getCharset());

		log.info("Esb Socket响应头信息:" + headerStr);

		// 获取报文长度
		int bodylength = Integer.parseInt(headerStr);

		if (bodylength == 0) {
			throw new RuntimeException("socket receive header data is null");
		}

		// 分配存储报文内容字节数组
		byte[] recvbuff = new byte[bodylength];

		// 根据报文长度设置缓冲数组大小
		if (bodylength < tcpConfig.getMaxBytesNum()) {
			// -次读取所有字节数
			tmpbuff = new byte[bodylength];
		} else {
			// 每次读取的字节数
			tmpbuff = new byte[tcpConfig.getMaxBytesNum()];
		}

		// 循环读取服务端返回的报文信息并保存到recvbuff数组中
		int count = 0;
		int desPos = 0;
		while (-1 != (count = in.read(tmpbuff))) {
			System.arraycopy(tmpbuff, 0, recvbuff, desPos, count);
			desPos = desPos + count;
			if (desPos >= bodylength) {
				break;
			}// end if
		}// end while

		if (count == 0) {
			throw new RuntimeException("socket receive body data is null");
		}

		return recvbuff;
	}

	/**
	 * 组装发送报文并发送到服务端
	 * 
	 * @param output
	 * @param requestXml
	 * @throws IOException
	 */
	public void send(BufferedOutputStream output, String requestXml) throws IOException {
		String msg;
		String headerStr;

		// 请求报文为空,程序结束
		if (requestXml == null || requestXml.trim().length() == 0) {
			throw new NullPointerException("request packet is null");
		}

		// 组建输入报文头信息
		if (null == headerMsg || headerMsg.trim().length() == 0) {
			headerStr = String.format("%0" + tcpConfig.getHeaderLength() + "d", requestXml.getBytes().length);
		} else {
			headerStr = headerMsg;
		}

		log.debug("Esb Socket发送报文头信息:" + headerStr);

		msg = headerStr + requestXml;

		log.info("Esb Socket发送报文信息:" + msg);

		// 写入请求报文到服务端
		output.write(msg.getBytes(tcpConfig.getCharset()));
		output.flush();
	}

	public String getHeaderMsg() {
		return headerMsg;
	}

	public void setHeaderMsg(String headerMsg) {
		this.headerMsg = headerMsg;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TcpClient [headerMsg=");
		builder.append(headerMsg);
		builder.append(", tcpConfig=");
		builder.append(tcpConfig);
		builder.append("]");
		return builder.toString();
	}

	public static void main(String[] args) {
		try {
			Class claz = Class.forName("com.singlee.esb.server.socket.impl.EsbTransferSocketServer");
			
			
			System.out.println(claz);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
