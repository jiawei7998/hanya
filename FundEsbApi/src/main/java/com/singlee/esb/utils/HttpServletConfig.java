package com.singlee.esb.utils;

import java.io.Serializable;

/**
 * 访问HtppServletServer配置类
 * @author chenxh
 *
 */
public class HttpServletConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6312314233200655431L;

	/**
	 * 是否设置报文长度
	 */
	private boolean isHeaderLength=false;
	
	/**
	 * 是否设置请求报文类型
	 */
	private String contentType;

	/**
	 * 服务器访问url
	 */
	private String url;

	/**
	 * 连接超时时间,默认为60秒
	 */
	private int connTimeOut=60;
	
	/**
	 * 读取超时时间,默认为60秒
	 */
	private int readTimeOut=60;

	/**
	 * 字符集,默认为UTF-8
	 */
	private String charset="UTF-8";

	/**
	 * 绶冲区字节大小,默认为8192字节
	 */
	private int maxBytesNum=8192;
	
	public HttpServletConfig() {}
	
	public HttpServletConfig(String url,String charset) {
		this.url = url;
		this.charset = charset;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getConnTimeOut() {
		return connTimeOut;
	}

	public void setConnTimeOut(int connTimeOut) {
		this.connTimeOut = connTimeOut;
	}

	public int getReadTimeOut() {
		return readTimeOut;
	}

	public void setReadTimeOut(int readTimeOut) {
		this.readTimeOut = readTimeOut;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public int getMaxBytesNum() {
		return maxBytesNum;
	}

	public void setMaxBytesNum(int maxBytesNum) {
		this.maxBytesNum = maxBytesNum;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isHeaderLength() {
		return isHeaderLength;
	}

	public void setHeaderLength(boolean isHeaderLength) {
		this.isHeaderLength = isHeaderLength;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HttpServletConfig [isHeaderLength=");
		builder.append(isHeaderLength);
		builder.append(", contentType=");
		builder.append(contentType);
		builder.append(", url=");
		builder.append(url);
		builder.append(", connTimeOut=");
		builder.append(connTimeOut);
		builder.append(", readTimeOut=");
		builder.append(readTimeOut);
		builder.append(", charset=");
		builder.append(charset);
		builder.append(", maxBytesNum=");
		builder.append(maxBytesNum);
		builder.append("]");
		return builder.toString();
	}
	
}
