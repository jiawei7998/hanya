package com.singlee.esb.utils;

import java.io.Serializable;

/**
 * socket通讯配置实体类
 * 
 * @author chenxh
 * 
 */
public class TcpConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2625207574002101772L;

	/**
	 * 报文头长度,默认为8个长度
	 */
	private int headerLength;

	/**
	 * 服务器地址
	 */
	private String address;

	/**
	 * 服务器端口
	 */
	private int port;

	/**
	 * 服务器超时时间,默认为120秒
	 */
	private int timeout;

	/**
	 * 字符集,默认为UTF-8
	 */
	private String charset;

	/**
	 * 绶冲区字节大小,默认为8192字节
	 */
	private int maxBytesNum;

	/**
	 * 方法已重载.实例化SOCKET客户端参数信息
	 * 
	 * @param address
	 * @param port
	 * @param timeout
	 * @param headerLength
	 * @param maxBytesNum
	 * @param charset
	 */
	public TcpConfig(String address, int port, int timeout, int headerLength, int maxBytesNum, String charset) {
		super();
		if ((address == null) || (port == 0) || (timeout == 0)) {
			throw new IllegalArgumentException("address or port or timeout is illegal!!!");
		}
		this.headerLength = headerLength;
		this.address = address;
		this.charset = charset;
		this.maxBytesNum = maxBytesNum;
		this.port = port;
		this.timeout = timeout;
	}

	/**
	 * 方法已重载.实例化SOCKET客户端参数信息
	 * 
	 * @param address
	 * @param port
	 */
	public TcpConfig(String address, int port) {
		this(address, port, 120, 8, 8192, "UTF-8");
	}

	/**
	 * 方法已重载.实例化SOCKET客户端参数信息
	 * 
	 * @param address
	 * @param port
	 * @param timeout
	 */
	public TcpConfig(String address, int port, int timeout) {
		this(address, port, timeout, 8, 8192, "UTF-8");
	}

	/**
	 * 方法已重载.实例化SOCKET客户端参数信息
	 * 
	 * @param address
	 * @param port
	 * @param timeout
	 * @param headerLength
	 */
	public TcpConfig(String address, int port, int timeout, int headerLength) {
		this(address, port, timeout, headerLength, 8192, "UTF-8");
	}

	public int getHeaderLength() {
		return headerLength;
	}

	public void setHeaderLength(int headerLength) {
		this.headerLength = headerLength;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public int getMaxBytesNum() {
		return maxBytesNum;
	}

	public void setMaxBytesNum(int maxBytesNum) {
		this.maxBytesNum = maxBytesNum;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TcpConfig [address=");
		builder.append(address);
		builder.append(", charset=");
		builder.append(charset);
		builder.append(", headerLength=");
		builder.append(headerLength);
		builder.append(", maxBytesNum=");
		builder.append(maxBytesNum);
		builder.append(", port=");
		builder.append(port);
		builder.append(", timeout=");
		builder.append(timeout);
		builder.append("]");
		return builder.toString();
	}

}
