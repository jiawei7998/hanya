package com.singlee.esb.utils;

import cn.com.agree.mq.api.ApiException;
import cn.com.agree.mq.api.app.MQApplication;
import cn.com.agree.mq.api.app.MQFileTransmitter;
import com.singlee.xstream.bean.AxisParameterBean;
import org.apache.axis.encoding.XMLType;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

/**
 * 通讯调用工具类
 * 
 * @author chenxh
 * 
 */
public class CommumicationUtil {

    /**
     * 方法已重载.获取WEBSERVICE发布的接口定义对象 采用javax.xml.ws.Service的方式实现
     * 
     * @param <T> 泛型对象定义
     * @param cla webservice定义接口类
     * @param wsdlUrl wsdl发布的URL路径
     * @param targetNamespace wsdl中定义的命名空间名称
     * @param serviceName wsdl中定义的服务名称
     * @param portName wsdl中定义的服务接口名称
     * @return T 参数cla对象的CLASS对象
     * @throws MalformedURLException
     * @throws ServiceException
     */
    public static <T> T callJavaWsWebService(Class<T> cla, String wsdlUrl, String targetNamespace, String serviceName,
        String portName) throws MalformedURLException {
    	Log log = LogFactory.getLog(CommumicationUtil.class);
        log.debug("开始调用Java Ws WebService服务");
        // 创建WEBSERVICE服务对象
        javax.xml.ws.Service service = javax.xml.ws.Service.create(new URL(wsdlUrl), new QName(targetNamespace, serviceName));
        log.debug("创建远程WebService接口服务");
        // 根据指定的接口类型,创建接口服务对象
        T serviceImpl = service.getPort(new QName(targetNamespace, portName), cla);
        log.debug("调用远程WebService服务方法并接收返回对象");
        // 返回实现对象
        return serviceImpl;
    }

    /**
     * 方法已重载.获取WEBSERVICE发布的接口定义对象 采用javax.xml.ws.Service的方式实现
     * 
     * @param <T> 泛型对象定义
     * @param cla webservice定义接口类
     * @param wsdlUrl wsdl发布的URL路径
     * @param targetNamespace wsdl中定义的命名空间名称
     * @param serviceClassName wsdl中定义的接口实现类的类名
     * @return T 参数cla对象的CLASS对象
     * @throws MalformedURLException
     * @throws ServiceException
     */
    public static <T> T callJavaWsWebService(Class<T> cla, String wsdlUrl, String targetNamespace,
        String serviceClassName) throws MalformedURLException {
        return callJavaWsWebService(cla, wsdlUrl, targetNamespace, serviceClassName + "Service",
            serviceClassName + "Port");
    }
    
	/**
	 * 方法已重载.获取WEBSERVICE发布的接口定义对象
	 * 采用org.apache.axis.client.Service的方式实现
	 * @param <T>
	 * @param outType
	 * @param wsdlUrl
	 * @param methodName
	 * @param paramValue
	 * @return
	 * @throws ServiceException
	 * @throws RemoteException
	 */
	public static <T> T callAxisClientWebService(Class<T> outType,String wsdlUrl,String methodName,AxisParameterBean...paramValue) throws ServiceException, RemoteException{
		return callAxisClientWebService(outType,wsdlUrl,60,methodName,paramValue);
	}
	
	/**
	 * 方法已重载.获取WEBSERVICE发布的接口定义对象
	 * 采用org.apache.axis.client.Service的方式实现
	 * @param <T>
	 * @param outTye
	 * @param wsdlUrl
	 * @param methodName
	 * @param paramValue
	 * @return
	 * @throws ServiceException
	 * @throws RemoteException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T callAxisClientWebService(Class<T> outType,String wsdlUrl,int timeOut,String methodName,AxisParameterBean...paramValue) throws ServiceException, RemoteException{
		Log log = LogFactory.getLog(CommumicationUtil.class);
		//声名返回对象
		T outObj = null;
		//保存参数
		List<String> paranValues = new ArrayList<String>();
		//返回值类型
		QName outQName = null;
		
		log.debug("创建axis client Service接口服务");
		//创建WEBSERVICE服务对象
		org.apache.axis.client.Service service = new org.apache.axis.client.Service();
		log.debug("接收axis client Service回调处理对象");
		//创建WEBSERVICE回调处理对象
		org.apache.axis.client.Call call = (org.apache.axis.client.Call) service.createCall();
		log.debug("设置超时时间"+timeOut+"秒");
		//设置超时时间
		call.setTimeout(timeOut*1000);
		log.debug("设置WSDL服务地址:"+wsdlUrl);
		//设置WSDL服务地址
		call.setTargetEndpointAddress(wsdlUrl);
		log.debug("设置调用的方法名称:"+methodName);
		//设置调用的方法名称
		call.setOperationName(methodName);
		
		log.debug("添加参数及保存参数值");
		//添加参数及保存参数值
		for (AxisParameterBean axisParameterBean : paramValue) {
			call.addParameter(axisParameterBean.getParamName(), axisParameterBean.getXmlType(), ParameterMode.IN);
			paranValues.add(axisParameterBean.getParamValue());
			log.debug("参数值："+axisParameterBean);
		}
		
		//保存返回类型
		if(outType.getName().contains("String")){
			outQName = XMLType.XSD_STRING;
		}else if(outType.getName().contains("Double")){
			outQName = XMLType.XSD_DOUBLE;
		}else if(outType.getName().contains("Integer")){
			outQName = XMLType.XSD_INTEGER;
		}else if(outType.getName().contains("Float")){
			outQName = XMLType.XSD_FLOAT;
		}else if(outType.getName().contains("BigDecimal")){
			outQName = XMLType.XSD_DECIMAL;
		}else if(outType.getName().contains("Boolean")){
			outQName = XMLType.XSD_BOOLEAN;
		}else if(outType.getName().contains("Date")){
			outQName = XMLType.XSD_DATE;
		}else if(outType.getName().contains("Byte")){
			outQName = XMLType.XSD_BYTE;
		}else{
			throw new NullPointerException("return object type is null");
		}
		
		log.debug("设置返回类型");
		//设置返回类型
		call.setReturnType(outQName);
		
		log.debug("执行服务端的方法并接收返回结果");
		
		//执行服务端的方法并返回结果
		outObj = (T) call.invoke(paranValues.toArray());
		
		return outObj;
	}

    /**
     * 调用ESB SOCKET服务
     * 
     * @param tcpConfig
     * @param headerMsg
     * @param requestXml
     * @return
     * @throws Exception
     */
    public static String callSocket(TcpConfig tcpConfig, String headerMsg, String requestXml) throws Exception {
        String responseXml = null;
        Log log = LogFactory.getLog(CommumicationUtil.class);

        log.debug("创建SOCKET客户端对象");
        TcpClient client = new TcpClient(tcpConfig);
        log.debug("设置请求报文报文头");
        client.setHeaderMsg(headerMsg);
        log.debug("发送XML报文到SOCKET服务端,并接收返回值");
        responseXml = client.send(requestXml);

        return responseXml;
    }

    /**
     * 调用ESB MQ服务
     * 
     * @param sender 发送报文
     * @return
     * @throws Exception
     */
    public static String callMq(String mqApifile, String appcode, String sender, String charset) throws Exception {
        String receiver = null;
        Log log = LogFactory.getLog(CommumicationUtil.class);
        MQApplication app = null;
        try {
            log.debug("创建MQ对象");
            app = new MQApplication();
            log.debug("打开消息接口");
            app.open(mqApifile, appcode);
            log.debug("发送XML报文到MQ服务端,并接收返回值");
            receiver = new String(app.invoke(sender.getBytes(charset)), charset);
        } catch (ApiException e) {
            log.error("MQAPI打开操作异常", e);
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            log.error("字符集编码转换异常异常", e);
            e.printStackTrace();
        } finally {
            if (null != app) {
                try {
                    app.close();
                } catch (ApiException e) {
                    log.error("MQAPI关闭操作异常", e);
                    e.printStackTrace();
                }
            }
        }
        return receiver;
    }

    /**
     * DLCB 文件下载
     * 
     * @param fileId 文件id
     * @param localFile 本地文件路径
     * 
     */
    public void getFileNew(String fileId, String localFile) {
    	Log log = LogFactory.getLog(CommumicationUtil.class);
        MQFileTransmitter transmit = new MQFileTransmitter();
        try {
            // 配置文件
            URL weburl = CommumicationUtil.class.getClassLoader().getResource("WEB-INF/classes");
            URL url = CommumicationUtil.class.getClassLoader().getResource("");
            String mqApiFile = (null == weburl ? url.getPath() : weburl.getPath()) + "mqapi.xml";

            transmit.open(mqApiFile, "client");
            transmit.get(fileId, localFile);
        } catch (Exception e) {
            log.error("MQFILEAPI文件下载异常", e);
        } finally { // 必须执行
            try {
                transmit.close();// 必须执行
            } catch (ApiException e1) {
                log.error("MQFILEAPI关闭操作异常", e1);
            }
        }
    }

    /**
     * 上传文件，本地文件路径+文件名称
     * 
     * @param localFile
     * @return
     */
    public String putFileNew(String localFile) {
    	Log log = LogFactory.getLog(CommumicationUtil.class);
        MQFileTransmitter transmit = new MQFileTransmitter();
        try {
            // 配置文件
            URL weburl = CommumicationUtil.class.getClassLoader().getResource("WEB-INF/classes");
            URL url = CommumicationUtil.class.getClassLoader().getResource("");
            String mqApiFile = (null == weburl ? url.getPath() : weburl.getPath()) + "mqapi.xml";
            log.debug("CommumicationUtil get mq file path:" + mqApiFile);
            transmit.open(mqApiFile, "client");
            String fileId = transmit.put(localFile);
            return fileId;
        } catch (Exception e) {
            log.error("MQFILEAPI文件上传异常", e);
            return null;
        } finally { // 必须执行
            try {
                transmit.close();// 必须执行
            } catch (ApiException e1) {
                log.error("MQFILEAPI关闭操作异常", e1);
            }
        }
    }
    
    /**
     * 调用HttpServlet服务
     * @param httpConfig
     * @param requestXml
     * @return
     * @throws Exception
     */
	public static String callHttpServletServer(HttpServletConfig httpConfig,String requestXml) throws Exception{
		String responseXml = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		byte [] buff = null;
		int rc = 0;
		ByteArrayOutputStream byteArray = null;
		URLConnection conn = null;
		int length = 0;
		Log log = LogFactory.getLog(CommumicationUtil.class);
	    try {
	    	log.info("CommumicationUtil.callHttpServletServer HttpServletConfig:"+httpConfig);
	    	if(null == httpConfig) {
	    		throw new RuntimeException("HttpServletConfig配置不能为空");
	    	}
	    	if(null == httpConfig.getUrl() || httpConfig.getUrl().trim().length() == 0) {
	    		throw new RuntimeException("HttpServlet访问URL不能为空");
	    	}
	    	//初始化URL
			URL urlObj = new URL(httpConfig.getUrl());
			//连接服务器
			conn = urlObj.openConnection();
			//设置连接超时时间
			conn.setConnectTimeout(httpConfig.getConnTimeOut()*1000);
			//设置读取超时时间
			conn.setReadTimeout(httpConfig.getReadTimeOut()*1000);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			//设置HTTP数据类型
			if(null != httpConfig.getContentType() && httpConfig.getContentType().trim().length() > 0) {
				conn.setRequestProperty("Content-Type", "octet-stream;charset="+httpConfig.getCharset());
	    	}
		
			//初始化输入流
			dos = new DataOutputStream(new BufferedOutputStream(conn.getOutputStream()));
			log.info("CommumicationUtil.callHttpServletServer 请求XML报文:"+requestXml);
			length = requestXml == null ? 0 : requestXml.getBytes(httpConfig.getCharset()).length;
			log.info("CommumicationUtil.callHttpServletServer 请求报文长度:"+length);
			if(length == 0) {
	    		throw new RuntimeException("HttpServlet请求报文为空");
	    	}
			if (httpConfig.isHeaderLength()) {
				// 设置报文长度
				dos.writeInt(length);
			}
			//设置报文内容
			dos.write(requestXml.getBytes(httpConfig.getCharset()));
			//发送输出流到服务端
			dos.flush();
			
			//初始化输入流
			dis = new DataInputStream(new BufferedInputStream(conn.getInputStream()));
			if (httpConfig.isHeaderLength()) {
				// 读取响应报文长度
				length = dis.readInt();
				log.info("CommumicationUtil.callHttpServletServer 响应报文HeaderLength:"+length);
			}
			//读取输入流内容
			byteArray = new ByteArrayOutputStream();
			buff = new byte[httpConfig.getMaxBytesNum()];
			while((rc = dis.read(buff,0,httpConfig.getMaxBytesNum()))>0){
				byteArray.write(buff,0,rc);
			}
			byteArray.flush();
			//将输入流报文内容转换为字符串
			responseXml = byteArray.toString(httpConfig.getCharset());
			log.info("CommumicationUtil.callHttpServletServer 响应XML报文:"+responseXml);
			length = responseXml == null ? 0 : responseXml.getBytes(httpConfig.getCharset()).length ;
			log.info("CommumicationUtil.callHttpServletServer 响应报文体长度:"+length);
			if(length == 0) {
	    		throw new RuntimeException("HttpServlet响应报文为空");
	    	}
		} catch (Exception e) {
			throw e;
		}finally{
			try {
				if(null != byteArray) {
					byteArray.close();}
			} catch (Exception e) {
			}
			try {
				if(null != dis) {
					dis.close();}
			} catch (IOException e) {
			}
			try {
				if(null != dos) {
					dos.close();}
			} catch (IOException e) {
			}
		}
		return responseXml;
	}

	/**
	 * 调用HttpServletMD5服务
	 * @param headAttrConfig
	 * @param requestXml
	 * @return
	 * @throws Exception
	 */
	public static String callHttpServletMd5Server(HttpHeadAttrConfig headAttrConfig, String requestXml)throws Exception{
		String responseXml = null;
		DataOutputStream dos = null;
		DataInputStream dis = null;
		byte [] buff = null;
		int rc = 0;
		ByteArrayOutputStream byteArray = null;
		URLConnection conn = null;
		int length = 0;
		Log log = LogFactory.getLog(CommumicationUtil.class);
		try {
			log.info("CommumicationUtil.callHttpServletMd5Server HttpServletMd5Config:"+headAttrConfig);
			if(null == headAttrConfig) {
				throw new RuntimeException("HttpServletMd5Config配置不能为空");
			}
			if(null == headAttrConfig.getUrl() || headAttrConfig.getUrl().trim().length() == 0) {
				throw new RuntimeException("HttpServlet访问URL不能为空");
			}
			//初始化URL
			URL urlObj = new URL(headAttrConfig.getUrl());
			//连接服务器
			conn = urlObj.openConnection();
			//设置连接超时时间
			conn.setConnectTimeout(headAttrConfig.getConnTimeOut()*1000);
			//设置读取超时时间
			conn.setReadTimeout(headAttrConfig.getReadTimeOut()*1000);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			//设置HTTP数据类型
			if(null != headAttrConfig.getContentType() && headAttrConfig.getContentType().trim().length() > 0) {
				conn.setRequestProperty("Content-Type", "text/html; charset="+headAttrConfig.getCharset());
			}
			//md5加密校验值
			if(headAttrConfig.isContentMd5()) {
				conn.setRequestProperty("Content-MD5", Base64.encodeBase64String(DigestUtils.md5Hex(requestXml).getBytes()));
			}
			log.info("CommumicationUtil.callHttpServletMd5Server 请求XML报文:"+requestXml);
			length = requestXml == null ? 0 : requestXml.getBytes(headAttrConfig.getCharset()).length;
			log.info("CommumicationUtil.callHttpServletMd5Server 请求报文长度:"+length);

			if(length == 0) {
				throw new RuntimeException("HttpServlet请求报文为空");
			}
			if ( headAttrConfig.isContentLength()) {
				conn.setRequestProperty("Content-Length",length+"");
			}

			//初始化输入流
			dos = new DataOutputStream(new BufferedOutputStream(conn.getOutputStream()));
			//设置报文内容
			dos.write(requestXml.getBytes(headAttrConfig.getCharset()));
			//发送输出流到服务端
			dos.flush();

			//初始化输入流
			dis = new DataInputStream(new BufferedInputStream(conn.getInputStream()));

			//读取输入流内容
			byteArray = new ByteArrayOutputStream();
			buff = new byte[headAttrConfig.getMaxBytesNum()];
			while((rc = dis.read(buff,0,headAttrConfig.getMaxBytesNum()))!= -1){
				System.out.println(new String());
				byteArray.flush();
				byteArray.write(buff,0,rc);

			}

			//将输入流报文内容转换为字符串
			responseXml = byteArray.toString(headAttrConfig.getRepCharset());
			log.info("CommumicationUtil.callHttpServletMd5Server 响应XML报文:"+responseXml);
			length = responseXml == null ? 0 : responseXml.getBytes(headAttrConfig.getCharset()).length ;
			log.info("CommumicationUtil.callHttpServletMd5Server 响应报文体长度:"+length);
			if(length == 0) {
				throw new RuntimeException("HttpServlet响应报文为空");
			}
			
		} catch (Exception e) {
			throw e;
		}finally{
			try {
				if(null != byteArray) {
					byteArray.close();}
			} catch (Exception e) {
			}
			try {
				if(null != dis) {
					dis.close();}
			} catch (IOException e) {
			}
			try {
				if(null != dos) {
					dos.close();}
			} catch (IOException e) {
			}
		}
		return responseXml;
	}
}
