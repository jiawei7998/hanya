package com.singlee.esb.utils;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 工具类
 * 
 * @author chenxh
 * 
 */
public class SingleeUtils {

	// 日期格式化类
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	// 空字符串
	public static String EMPTY = "";

	/**
	 * 将日期对象转换成字符串,格式:YYYY-MM-DD
	 * 
	 * @param date
	 *            日期
	 * @return
	 */
	public static String getISODate(Date date) {
		if (date != null) {
			return dateToString(date, "yyyy-MM-dd");
		}
		return EMPTY;
	}

	/**
	 * 将日期对象转换成字符串,格式:YYYYMMDD HH:mm:ss
	 * 
	 * @param date
	 * @return
	 */
	public static String getDateTime(Date date) {
		if (date != null) {
			return dateToString(date, "yyyyMMdd hh:mm:ss");
		}
		return EMPTY;
	}

	/**
	 * 将日期对象转换成字符串,格式:YYYY-MM-DDThh:mm:ss.sss
	 * 
	 * @param date
	 * @return
	 */
	public static String getISODateTime(Date date) {
		if (date != null) {
			return dateToString(date, "yyyy-MM-dd'T'hh:mm:ss.sss");
		}
		return EMPTY;
	}

	/**
	 * 获取流水号，时间戳
	 * 
	 * @return
	 */
	public static String getTextFlowId() {
		sdf.applyPattern("yyyyMMddHHMMSSS");
		return sdf.format(getCurrentDate());
	}

	/**
	 * 将日期对象转换成时间字符串,格式:hh:mm:ss.sss
	 * 
	 * @param date
	 * @return
	 */
	public static String getISOTime(Date date) {
		if (date != null) {
			return dateToString(date, "hh:mm:ss.sss");
		}
		return EMPTY;
	}

	/**
	 * 将日期对象转换成指定格式的字符串 日期格式:yyyy-MM-dd 时间格式:hh:mm:ss.sss
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String dateToString(Date date, String pattern) {
		if (date != null && pattern != null) {
			sdf.applyPattern(pattern);
			return sdf.format(date);
		}
		return EMPTY;
	}

	/**
	 * 将日期字符串转换为指定格式的日期对象 日期格式:yyyy-MM-dd 时间格式:hh:mm:ss.sss
	 * 
	 * @param date
	 *            日期格式字符串
	 * @param pattern
	 *            日期格式化定义
	 * @return 转换后的日期
	 * @throws ParseException
	 */
	public static Date stringToDate(String date, String pattern) throws ParseException {
		Date dt = null;
		if (date != null && !("").equals(date) && pattern != null) {
			sdf.applyPattern(pattern);
			return sdf.parse(date);
		}
		return dt;
	}

	/**
	 * 将数字字符串转换为BigDecimal对象
	 * 
	 * @param data
	 * @return
	 */
	public static BigDecimal strToBigdecima(String data) {
		BigDecimal bd = null;
		if (data != null && data.trim().length() > 0) {
			bd = new BigDecimal(data);
		}
		return bd;
	}

	/**
	 * 字符串字符集转换 将ISO-8859-1转换为GBK
	 * 
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toGBK(String str) {

		if (str != null) {
			try {
				str = new String(str.getBytes("ISO-8859-1"), "GBK");
			} catch (UnsupportedEncodingException e) {
				return str;
			}
		}
		return str;
	}

	/**
	 * 字符串字符集转换 将GBK转换为ISO-8859-1
	 * 
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toISO88591(String str) {
		if (str != null) {
			try {
				str = new String(str.getBytes("GBK"), "ISO-8859-1");
			} catch (UnsupportedEncodingException e) {
				return str;
			}
		}
		return str;
	}

	/**
	 * 获取当前机器时间
	 * 
	 * @return
	 */
	public static Date getCurrentDate() {
		return new Date(System.currentTimeMillis());
	}

	/**
	 * 去掉字符串前后的空格
	 * 
	 * @param value
	 *            java.lang.String
	 * @return java.lang.String
	 * @throws Exception
	 */
	public static String trim(String value) {
		if (null == value) {
			return EMPTY;
		}

		return value.trim();
	}

	/**
	 * 去掉字符串中所有的空格
	 * 
	 * @param value
	 *            java.lang.String
	 * @return java.lang.String
	 * @throws Exception
	 */
	public static String trimAll(String value) throws Exception {
		StringBuilder sb = new StringBuilder();
		if (null == value) {
			return EMPTY;
		}
		for (char var : value.toCharArray()) {
			if (var == KeyEvent.VK_SPACE) {
				continue;
			}// end if
			sb.append(var);
		}// end for

		return sb.toString().trim();
	}

	/**
	 * 判断字符串是否为空
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isEmpty(String value) {
		boolean bool = true;
		if (null == value) {
			bool = false;
		} else if ("".equals(value.trim())) {
			bool = false;
		}
		return bool;
	}

	/**
	 * 获取时间标签
	 * 
	 * @return
	 */
	public static String getDateTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		return sdf.format(getCurrentDate());
	}

	/**
	 * 获取心跳序号 时间戳格式-HHmmss
	 * 
	 * @return
	 */
	public static String getCcdcHrBtId(Date date) {
		sdf.applyPattern("HHmmss");
		return sdf.format(date);
	}

	/**
	 * 比较参数1和参数2
	 * 
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static Boolean CompareObject(String str1, String str2) {
		Boolean rs = true;
		if (str1 != null && str2 != null) {
			rs = !str1.equals(str2);
		}
		return rs;
	}

	/**
	 * 比较参数1和参数2
	 * 
	 * @param dt1
	 * @param dt2
	 * @return
	 */
	public static Boolean CompareObject(Date dt1, Date dt2) {
		Boolean rs = true;
		if (dt1 != null && dt2 != null) {
			rs = !dt1.equals(dt2);
		}
		return rs;
	}

	/**
	 * 比较参数1和参数2
	 * 
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static Boolean CompareObject(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
		Boolean rs = true;
		if (bigDecimal1 != null && bigDecimal2 != null) {
			if (bigDecimal1.compareTo(bigDecimal2) != 0) {
				rs = true;}
			else {
				rs = false;}
		}
		return rs;
	}

	/**
	 * 计算2个日期之间的天数
	 * 
	 * @param dt1
	 * @param dt2
	 * @return
	 */
	public static int diffDate(Date dt1, Date dt2) {
		if (dt1 != null && dt2 != null) {
			return (int) ((dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24));}
		return 0;
	}

	public static BigDecimal diffDate(BigDecimal bd1, BigDecimal bd2) {
		if (bd1 != null && bd2 != null) {
			return bd2.subtract(bd1);}
		return null;
	}

	/*
	 * 小数格式化
	 */
	public static BigDecimal formateBigDecimal(BigDecimal bd1, int leg) {
		if (bd1 != null) {
			if (bd1.intValue() == 0) {
				return bd1;
			}
			return bd1.setScale(leg, RoundingMode.HALF_UP);
		}
		return null;
	}

	/**
	 * 创建任意深度的文件所在文件夹,可以用来替代直接new File(path)。
	 * 
	 * @param path
	 * @return File对象
	 */
	public static File createFile(String path) throws Exception {
		File file = new File(path);
		// 寻找父目录是否存在
		File parent = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator)));
		// 如果父目录不存在，则递归寻找更上一层目录
		if (!parent.exists()) {
			createFile(parent.getPath());
			// 创建父目录
			parent.mkdirs();
		}
		return file;
	}

	/**
	 * 全角转半角
	 * 
	 * @param QJstr
	 * @return
	 */
	public static final String QBchange(String QJstr) throws Exception {

		if (QJstr == null) {
			return null;
		}
		char[] c = QJstr.toCharArray();

		for (int i = 0; i < c.length; i++) {
			if (c[i] == 12288) {
				c[i] = (char) 32;
				continue;
			}
			if (c[i] > 65280 && c[i] < 65375) {
				c[i] = (char) (c[i] - 65248);
			}
		}

		return new String(c);
	}

	/**
	 * 将异常信息转换成字符串
	 * 
	 * @param e
	 * @return
	 */
	public static String getException(Exception e) {
		StringWriter msg = null;
		try {
			msg = new StringWriter();
			PrintWriter pw = new PrintWriter(msg);
			e.printStackTrace(pw);

		} catch (Exception e1) {
		}
		return msg.toString();
	}

	/**
	 * 由字符串中按照regex提取时间
	 * 
	 * @param excelFileName
	 * @param regex
	 * @return
	 */
	public static String getDateFromString(String excelFileName, String regex) {
		// 匹配复核的字符串
		List<String> matchers = null;
		// 去掉换行符
		String dateStr = excelFileName.replaceAll("r?n", "");
		// 将给定的正则表达式编译到模式中。
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		// 尝试将整个区域与模式匹配
		Matcher matcher = pattern.matcher(dateStr);
		// 判断是否找到匹配字符
		if (matcher.find() && matcher.groupCount() >= 1) {
			matchers = new ArrayList<String>();
			// 组装字符
			for (int i = 0; i < matcher.groupCount(); i++) {
				String temp = matcher.group(i);
				matchers.add(temp);
			}
		} else {
			matchers = Collections.emptyList();
		}
		if (matchers.size() > 0) {
			return ((String) matchers.get(0)).trim();
		} else {
			return "";
		}
	}

	/**
	 * 去掉两边符号'
	 * 
	 * @param data
	 * @return
	 */
	public static String trimHull(String data) {
		if (data == null) {
			return data;
		}
		return data.replace("'", "").trim();
	}
}
