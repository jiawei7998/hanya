package com.singlee.esb.utils;

import java.util.Date;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 * Title: 计数器
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2012
 * </p>
 * <p>
 * Company: singlee
 * </p>
 * 
 * @author:chenxh
 * @version:20130308
 */
public class SerialnoLocalUtil {

	private static Hashtable<String, AtomicInteger> counters = null;

	public enum SERVER_TYPE {
		SERVER, ESB_SEQ
	}

	public enum TABLEID {
		DEFAULT14, DEFAULT20, DEFAULT, DEFAULT16,DEFAULT8
	}

	public static String getPostdate2Str() {
		return String.format("%1$tY-%1$tm-%1$td", getPostdate());
	}

	public static Date getPostdate() {
		return new Date();
	}

	private static int count(String server) {
		int result = 0;
		boolean flag = false;
		AtomicInteger counter = null;

		if (counters == null) {
			counters = new Hashtable<String, AtomicInteger>();
		}

		counter = counters.get(server);

		if (counter == null) {
			counter = new AtomicInteger(1);
		}

		do {
			// 得到计数当前的值
			result = counter.get();
			// 断点
			// 单线程下, compareAndSet返回永远为true,
			// 多线程下, 在与result进行compare时, counter可能被其他线程set了新值, 这时需要重新再取一遍再比较,
			// 如果还是没有拿到最新的值, 则一直循环下去, 直到拿到最新的那个值
			flag = counter.compareAndSet(result, result + 1);
		} while (!flag);
		// Hashtable中总是保存最大的序列号
		counters.put(server, counter);

		return result;
	}

	/**
	 * 
	 * @return hhmmss+001
	 */
	public static String getSerialno9(String server) {
		return String.format("%1$tH%1$tM%1$tS", SerialnoLocalUtil.getPostdate()) + String.format("%03d", (count(server) % 1000000));
	}

	/**
	 * 
	 * @return yyyyMMddHHmmss+000001
	 */
	public static String getSerialno20(String server) {
		return String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS", SerialnoLocalUtil.getPostdate()) + String.format("%06d", (count(server) % 1000000));
	}

	/**
	 * 
	 * @return yyMMddHHmmss+0001
	 */
	public static String getSerialno16(String server) {
		return String.format("%1$ty%1$tm%1$td%1$tH%1$tM%1$tS", SerialnoLocalUtil.getPostdate()) + String.format("%04d", (count(server) % 1000));
	}

	/**
	 * 
	 * @return yyMMddHHmmss+0001
	 */
	public static String getSerialno16() {
		return getSerialno16(TABLEID.DEFAULT16.name());
	}

	/**
	 * 
	 * @return yyyyMMdd+0000001
	 */
	public static String getSerialno14(String server) {
		return String.format("%1$tY%1$tm%1$td", SerialnoLocalUtil.getPostdate()) + String.format("%06d", (count(server) % 1000000));
	}

	/**
	 * 
	 * @return yyyyMMddHHmmss+000001
	 */
	public static String getSerialno20() {
		return getSerialno20(TABLEID.DEFAULT20.name());
	}

	/**
	 * 
	 * @return yyyyMMdd+0000001
	 */
	public static String getSerialno14() {
		return getSerialno14(TABLEID.DEFAULT14.name());
	}

	/**
	 * 
	 * @return yyyyMMddHHmmss (length-14) xxxxxx1
	 */
	public static String getSerialno(int length) {
		if (length < 14) {
			return String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS", SerialnoLocalUtil.getPostdate());
		} else {
			return String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS", SerialnoLocalUtil.getPostdate()) + String.format("%0" + (length - 14) + "d", (count(TABLEID.DEFAULT.name()) % Double.valueOf(Math.pow(10, (length - 14))).intValue()));
		}
	}
	public static String getSerialno8() {
		int value = (count(TABLEID.DEFAULT8.name()) % Double.valueOf(Math.pow(10, 2)).intValue());
		if(0==value){
			value = (count(TABLEID.DEFAULT8.name()) % Double.valueOf(Math.pow(10, 2)).intValue());
		}
		return String.format("%1$tH%1$tM%1$tS", SerialnoLocalUtil.getPostdate()) + String.format("%02d",value);
	}
	
	public static void main(String[] args) {
		for (int i = 0; i < 105; i++) {
			System.out.println("getSerialno8():"+getSerialno8());
		}
	}
}
