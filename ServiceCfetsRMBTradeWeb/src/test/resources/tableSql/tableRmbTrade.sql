
create table CFETS_TRADE_PACKAGE
(
  table_id         VARCHAR2(32) not null,
  upordown         VARCHAR2(4) not null,
  quote_id         VARCHAR2(32),
  cl_ord_id        VARCHAR2(32),
  msg_type         VARCHAR2(8) not null,
  tradedate        VARCHAR2(32) not null,
  marketindicator  VARCHAR2(8),
  quote_type       VARCHAR2(8),
  quote_trans_type VARCHAR2(8),
  quote_status     VARCHAR2(8),
  sender_compid    VARCHAR2(32),
  sender_subid     VARCHAR2(32),
  target_compid    VARCHAR2(32),
  target_subid     VARCHAR2(32),
  begin_string     VARCHAR2(32),
  packmsg          CLOB
)
;
comment on table CFETS_TRADE_PACKAGE
  is '报文保存表';
comment on column CFETS_TRADE_PACKAGE.table_id
  is '主键';
comment on column CFETS_TRADE_PACKAGE.upordown
  is '上行或下行';
comment on column CFETS_TRADE_PACKAGE.quote_id
  is '报价编号';
comment on column CFETS_TRADE_PACKAGE.cl_ord_id
  is '客户参考编号';
comment on column CFETS_TRADE_PACKAGE.msg_type
  is '报文类型';
comment on column CFETS_TRADE_PACKAGE.tradedate
  is '成交日期';
comment on column CFETS_TRADE_PACKAGE.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_PACKAGE.quote_type
  is '报价类别';
comment on column CFETS_TRADE_PACKAGE.quote_trans_type
  is '操作类型';
comment on column CFETS_TRADE_PACKAGE.quote_status
  is '报价状态';
comment on column CFETS_TRADE_PACKAGE.begin_string
  is '报文版本';
comment on column CFETS_TRADE_PACKAGE.packmsg
  is '报文';
alter table CFETS_TRADE_PACKAGE
  add constraint CFETS_TRADE_PACKAGE_PK primary key (TABLE_ID);

create table CFETS_TRADE_PARTY
(
  table_id       VARCHAR2(32) not null,
  table_cid      VARCHAR2(32) not null,
  party_id       VARCHAR2(32) not null,
  party_role     VARCHAR2(4) not null,
  party_subid2   VARCHAR2(32),
  party_subid266 VARCHAR2(32)
)
;
comment on table CFETS_TRADE_PARTY
  is '交易机构信息';
comment on column CFETS_TRADE_PARTY.table_id
  is '主键';
comment on column CFETS_TRADE_PARTY.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_PARTY.party_id
  is '交易账号';
comment on column CFETS_TRADE_PARTY.party_role
  is '账号角色';
comment on column CFETS_TRADE_PARTY.party_subid2
  is '交易员ID';
comment on column CFETS_TRADE_PARTY.party_subid266
  is '交易账户6位码';
alter table CFETS_TRADE_PARTY
  add constraint CFETS_TRADE_PARTY_PK primary key (TABLE_ID);

create table CFETS_TRADE_PLEDGE
(
  table_id             VARCHAR2(32) not null,
  table_cid            VARCHAR2(32) not null,
  total_face_amt       NUMBER(22,4),
  trade_product        VARCHAR2(16),
  side                 VARCHAR2(4),
  repo_method          VARCHAR2(4),
  trade_limit_days     NUMBER(22),
  price                NUMBER(22,4),
  trade_cash_amt       NUMBER(22,2),
  sett_type            VARCHAR2(4),
  delivery_type        VARCHAR2(4),
  delivery_type2       VARCHAR2(4),
  clearing_method      VARCHAR2(4),
  settl_currency       VARCHAR2(8),
  settl_curr_fxrate    NUMBER(22,6),
  cross_cust_instname1 VARCHAR2(4),
  cross_cust_amt1      NUMBER(22,2),
  cross_cust_instname2 VARCHAR2(4),
  cross_cust_amt2      NUMBER(22,2)
)
;
comment on table CFETS_TRADE_PLEDGE
  is '质押式回购';
comment on column CFETS_TRADE_PLEDGE.table_id
  is '主键';
comment on column CFETS_TRADE_PLEDGE.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_PLEDGE.total_face_amt
  is '券面总额合计';
comment on column CFETS_TRADE_PLEDGE.trade_product
  is '交易品种';
comment on column CFETS_TRADE_PLEDGE.side
  is '交易方向';
comment on column CFETS_TRADE_PLEDGE.repo_method
  is '回购方式';
comment on column CFETS_TRADE_PLEDGE.trade_limit_days
  is '回购期限';
comment on column CFETS_TRADE_PLEDGE.price
  is '回购利率';
comment on column CFETS_TRADE_PLEDGE.trade_cash_amt
  is '交易金额';
comment on column CFETS_TRADE_PLEDGE.sett_type
  is '清算速度';
comment on column CFETS_TRADE_PLEDGE.delivery_type
  is '首次结算方式';
comment on column CFETS_TRADE_PLEDGE.delivery_type2
  is '到期结算方式';
comment on column CFETS_TRADE_PLEDGE.clearing_method
  is '清算类型';
comment on column CFETS_TRADE_PLEDGE.settl_currency
  is '结算币种';
comment on column CFETS_TRADE_PLEDGE.settl_curr_fxrate
  is '汇率';
comment on column CFETS_TRADE_PLEDGE.cross_cust_instname1
  is '跨托管机构名称1';
comment on column CFETS_TRADE_PLEDGE.cross_cust_amt1
  is '跨托管交易金额1';
comment on column CFETS_TRADE_PLEDGE.cross_cust_instname2
  is '跨托管机构名称2';
comment on column CFETS_TRADE_PLEDGE.cross_cust_amt2
  is '跨托管交易金额2';
alter table CFETS_TRADE_PLEDGE
  add constraint CFETS_TRADE_PLEDGE_PK primary key (TABLE_ID);

create table CFETS_TRADE_PROGRESS
(
  table_id          VARCHAR2(32) not null,
  table_rid         VARCHAR2(32),
  upordown          VARCHAR2(4) not null,
  ret_code          VARCHAR2(8),
  ret_msg           VARCHAR2(64),
  send_flag         VARCHAR2(2) not null,
  msg_type          VARCHAR2(8) not null,
  marketindicator   VARCHAR2(8),
  trade_recordid    VARCHAR2(32),
  channel           VARCHAR2(16),
  quote_type        VARCHAR2(8),
  quote_trans_type  VARCHAR2(8),
  quote_status      VARCHAR2(8),
  cl_ord_id         VARCHAR2(32),
  query_request_id  VARCHAR2(32),
  orig_cl_ord_id    VARCHAR2(32),
  quote_id          VARCHAR2(32),
  transact_time     VARCHAR2(32),
  quote_time        VARCHAR2(32),
  negotiation_count VARCHAR2(4),
  user_reference1   VARCHAR2(64),
  user_reference2   VARCHAR2(64),
  user_reference3   VARCHAR2(64),
  user_reference4   VARCHAR2(64),
  user_reference5   VARCHAR2(64),
  user_reference6   VARCHAR2(64),
  exec_id           VARCHAR2(32),
  trade_method      VARCHAR2(4),
  exec_type         VARCHAR2(4),
  trade_date        VARCHAR2(10),
  trade_time        VARCHAR2(20)
)
;
comment on table CFETS_TRADE_PROGRESS
  is '报文解析公用表';
comment on column CFETS_TRADE_PROGRESS.table_id
  is '主键';
comment on column CFETS_TRADE_PROGRESS.table_rid
  is '外键CFETS_TRADE_PACKAGE';
comment on column CFETS_TRADE_PROGRESS.upordown
  is '上行或下行';
comment on column CFETS_TRADE_PROGRESS.ret_code
  is '返回码';
comment on column CFETS_TRADE_PROGRESS.ret_msg
  is '提示信息';
comment on column CFETS_TRADE_PROGRESS.send_flag
  is '发送标识：0未发送，1发送';
comment on column CFETS_TRADE_PROGRESS.msg_type
  is '报文类型';
comment on column CFETS_TRADE_PROGRESS.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_PROGRESS.trade_recordid
  is '交易记录编号';
comment on column CFETS_TRADE_PROGRESS.channel
  is '渠道';
comment on column CFETS_TRADE_PROGRESS.quote_type
  is '报价类别';
comment on column CFETS_TRADE_PROGRESS.quote_trans_type
  is '操作类型';
comment on column CFETS_TRADE_PROGRESS.quote_status
  is '报价状态';
comment on column CFETS_TRADE_PROGRESS.cl_ord_id
  is '客户参考编号';
comment on column CFETS_TRADE_PROGRESS.query_request_id
  is '查询请求编号';
comment on column CFETS_TRADE_PROGRESS.orig_cl_ord_id
  is '原客户参考编号';
comment on column CFETS_TRADE_PROGRESS.quote_id
  is '报价编号';
comment on column CFETS_TRADE_PROGRESS.transact_time
  is '业务发生时间';
comment on column CFETS_TRADE_PROGRESS.quote_time
  is '报价发生时间';
comment on column CFETS_TRADE_PROGRESS.negotiation_count
  is '交谈轮次';
comment on column CFETS_TRADE_PROGRESS.user_reference1
  is '用户参考数据1';
comment on column CFETS_TRADE_PROGRESS.user_reference2
  is '用户参考数据2';
comment on column CFETS_TRADE_PROGRESS.user_reference3
  is '用户参考数据3';
comment on column CFETS_TRADE_PROGRESS.user_reference4
  is '用户参考数据4';
comment on column CFETS_TRADE_PROGRESS.user_reference5
  is '用户参考数据5';
comment on column CFETS_TRADE_PROGRESS.user_reference6
  is '用户参考数据6';
comment on column CFETS_TRADE_PROGRESS.exec_id
  is '成交编号';
comment on column CFETS_TRADE_PROGRESS.trade_method
  is '交易方式';
comment on column CFETS_TRADE_PROGRESS.exec_type
  is '成交状态';
comment on column CFETS_TRADE_PROGRESS.trade_date
  is '成交日期yyyymmdd';
comment on column CFETS_TRADE_PROGRESS.trade_time
  is '成交时间';
alter table CFETS_TRADE_PROGRESS
  add constraint CFETS_TRADE_PROGRESS_PK primary key (TABLE_ID);

create table CFETS_TRADE_SUBBOND
(
  table_id       VARCHAR2(32) not null,
  table_pid      VARCHAR2(32) not null,
  table_cid      VARCHAR2(32) not null,
  bond_code      VARCHAR2(32) not null,
  bond_name      VARCHAR2(128),
  total_face_amt NUMBER(22,2),
  discount_type  VARCHAR2(16),
  discount_value NUMBER(22,2),
  discount_amt   NUMBER(22,2)
)
;
comment on table CFETS_TRADE_SUBBOND
  is '质押券';
comment on column CFETS_TRADE_SUBBOND.table_id
  is '主键';
comment on column CFETS_TRADE_SUBBOND.table_pid
  is '外键CFETS_TRADE_PLEDGE';
comment on column CFETS_TRADE_SUBBOND.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_SUBBOND.bond_code
  is '债券编码';
comment on column CFETS_TRADE_SUBBOND.bond_name
  is '债券名称';
comment on column CFETS_TRADE_SUBBOND.total_face_amt
  is '券面总额';
comment on column CFETS_TRADE_SUBBOND.discount_type
  is '折算比例';
comment on column CFETS_TRADE_SUBBOND.discount_value
  is '折算比例值';
comment on column CFETS_TRADE_SUBBOND.discount_amt
  is '折算金额';
alter table CFETS_TRADE_SUBBOND
  add constraint CFETS_TRADE_SUBBOND_PK primary key (TABLE_ID);

