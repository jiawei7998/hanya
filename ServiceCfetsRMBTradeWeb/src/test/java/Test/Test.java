package Test;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.service.RecAAnalysisRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.ConfigError;
import imix.DataDictionary;
import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.InvalidMessage;
import imix.UnsupportedMessageType;
import imix.imix20.Quote;

/**
 * 测试报文解析
 * @author xuqq
 *
 */
public class Test {

	public static void main(String[] args) {
		CfetsTradePackage cfetsTradePackage = new CfetsTradePackage();//报文对象
		CfetsTradeProgress cfetsTradeProgress = new CfetsTradeProgress();//报文解析对象
		//转换对象
		//TODO 需要放入指定字符串
		String msg = "";
		
		Quote message = new Quote();
		
		try {
			message.fromString(msg, new DataDictionary("IMIX20.xml"), false);
		} catch (InvalidMessage | ConfigError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			RetBean retBean = RecAAnalysisRmbTrade.analysisMessage(cfetsTradePackage,cfetsTradeProgress,message);
		} catch (FieldNotFound | UnsupportedMessageType | IncorrectTagValue e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
