package Test;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.remoting.RemoteConnectFailureException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.cfets.rmb.trade.IImixRmbTradeManageServer;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeBondPledge;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeSubbond;
import com.singlee.financial.pojo.RetBean;

public class TestUp {

	public static void main(String[] args) {
		IImixRmbTradeManageServer iImixRmbTradeManageServer = null;
		try {
			iImixRmbTradeManageServer = (IImixRmbTradeManageServer) new HessianProxyFactory().create(IImixRmbTradeManageServer.class,
					"http://localhost:8081/fund-cfetsRmbTrade/api/service/ImixRmbTradeManageServerExporter");
//			smtTradeImpServerExporter
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			CfetsTradePackage cfetsTradePackage = new CfetsTradePackage();
			cfetsTradePackage.setTableId("20210304110122001540");//主键
			cfetsTradePackage.setUpordown("UP");//上行或下行
//			cfetsTradePackage.setQuoteId(quoteId);//报价编号
//			cfetsTradePackage.setClOrdId(clOrdId);//客户参考编号
			cfetsTradePackage.setMsgType("S");//报文类型
			cfetsTradePackage.setTradedate("20210304-11:01:22.047");//成交日期
			cfetsTradePackage.setMarketindicator("9");//市场标识
			cfetsTradePackage.setQuoteType("1");//报价类别
			cfetsTradePackage.setQuoteTransType("N");//操作类型
//			cfetsTradePackage.setQuoteStatus(quoteStatus);//报价状态
			cfetsTradePackage.setSenderCompid("111001132000000104001");//
			cfetsTradePackage.setSenderSubid("jsyhtrd");//
			cfetsTradePackage.setTargetCompid("CFETS-TRADING-INFI-2");//
			cfetsTradePackage.setTargetSubid("NDM");//
			cfetsTradePackage.setBeginString("IMIX.2.0");//报文版本
//			cfetsTradePackage.setPackmsg("");//报文

			CfetsTradeProgress cfetsTradeProgress = new CfetsTradeProgress();
			cfetsTradeProgress.setTableId("20210304110122001540");//主键
			cfetsTradeProgress.setTableRid("20210304110122001540");//外键CFETS_TRADE_PACKAGE
//			cfetsTradeProgress.setTableDid(tableDid);//业务外键FLOW_TRADE
			cfetsTradeProgress.setIsCurrent("0");//是否当前
			cfetsTradeProgress.setUpordown("UP");//上行或下行
//			cfetsTradeProgress.setRetCode(retCode);//返回码
//			cfetsTradeProgress.setRetMsg(retMsg);//提示信息
			cfetsTradeProgress.setSendFlag("0");//发送标识：0未发送，1发送成功，2发送失败，3报价成功，4报价失败，5过期
			cfetsTradeProgress.setMsgType("S");//报文类型
			cfetsTradeProgress.setMarketindicator("9");//市场标识
//			cfetsTradeProgress.setTradeRecordid(tradeRecordid);//交易记录编号
			cfetsTradeProgress.setChannel("CFETS");//渠道
//			cfetsTradeProgress.setChannelType(channelType);//渠道类型
			cfetsTradeProgress.setQuoteType("1");//报价类别
			cfetsTradeProgress.setQuoteTransType("N");//操作类型
//			cfetsTradeProgress.setQuoteStatus(quoteStatus);//报价状态
//			cfetsTradeProgress.setQuoteStatusDesc(quoteStatusDesc);//报价状态说明
			cfetsTradeProgress.setClOrdId("ODjsyhtrd000000001");//客户参考编号
//			cfetsTradeProgress.setQueryRequestId(queryRequestId);//查询请求编号
//			cfetsTradeProgress.setOrigClOrdId(origClOrdId);//原客户参考编号
//			cfetsTradeProgress.setQuoteReqId(quoteReqId);//请求报价编号
//			cfetsTradeProgress.setQuoteId(quoteId);//报价编号
//			cfetsTradeProgress.setPledgeSubmissionCode(pledgeSubmissionCode);//提券编号
			cfetsTradeProgress.setTransactTime("20210304-11:01:22.047");//业务发生时间
//			cfetsTradeProgress.setQuoteTime(quoteTime);//报价发生时间
//			cfetsTradeProgress.setNegotiationCount(negotiationCount);//交谈轮次
//			cfetsTradeProgress.setUserReference1(userReference1);//用户参考数据1
//			cfetsTradeProgress.setUserReference2(userReference2);//用户参考数据2
//			cfetsTradeProgress.setUserReference3(userReference3);//用户参考数据3
//			cfetsTradeProgress.setUserReference4(userReference4);//用户参考数据4
//			cfetsTradeProgress.setUserReference5(userReference5);//用户参考数据5
//			cfetsTradeProgress.setUserReference6(userReference6);//用户参考数据6
//			cfetsTradeProgress.setExecId(execId);//成交编号
//			cfetsTradeProgress.setTradeMethod(tradeMethod);//交易方式
//			cfetsTradeProgress.setExecType(execType);//成交状态
//			cfetsTradeProgress.setTradeDate(tradeDate);//成交日期yyyymmdd
//			cfetsTradeProgress.setTradeTime(tradeTime);//成交时间
//			cfetsTradeProgress.setText(text);//备注
//			cfetsTradeProgress.setAnonymousIndicator(anonymousIndicator);//匿名标识
//			cfetsTradeProgress.setConIndicator(conIndicator);//应急标识
//			cfetsTradeProgress.setValidUntilTime(validUntilTime);//有效时间
//			cfetsTradeProgress.setRoutingType(routingType);//发送对象
//			cfetsTradeProgress.setAllocInd(allocInd);//分账标识
//			cfetsTradeProgress.setDateConfirmed(dateConfirmed);//场次日期
			
			CfetsTradeBondPledge cfetsTradeBondPledge = new CfetsTradeBondPledge();
			cfetsTradeBondPledge.setTableId("20210304110122001541");//主键
			cfetsTradeBondPledge.setTableCid("20210304110122001540");//外键CFETS_TRADE_PROGRESS
			cfetsTradeBondPledge.setTotalFaceAmt(new BigDecimal("0"));//券面总额合计
			cfetsTradeBondPledge.setTotalDiscountAmt(new BigDecimal("0"));//折算总金额
			cfetsTradeBondPledge.setTradeProduct("7D");//交易品种
			cfetsTradeBondPledge.setSide("2");//交易方向
			cfetsTradeBondPledge.setRepoMethod("1");//回购方式
			cfetsTradeBondPledge.setTradeLimitDays(new BigDecimal("2"));//回购期限
			cfetsTradeBondPledge.setRealDays(new BigDecimal("4"));//实际占款天数
			cfetsTradeBondPledge.setPrice(new BigDecimal("2"));//回购利率
//			cfetsTradeBondPledge.setBenchmarkInterestRate(benchmarkInterestRate);//基准利率
//			cfetsTradeBondPledge.setSpread(spread);//点差
			cfetsTradeBondPledge.setTradeCashAmt(new BigDecimal("200000"));//交易金额
			cfetsTradeBondPledge.setSettType("1");//清算速度
			cfetsTradeBondPledge.setDeliveryType("0");//首次结算方式
			cfetsTradeBondPledge.setDeliveryType2("0");//到期结算方式
			cfetsTradeBondPledge.setStartSettleDate("2021-03-04");//首期结算日
			cfetsTradeBondPledge.setEndSettleDate("2021-03-08");//到期结算日
			cfetsTradeBondPledge.setClearingMethod("13");//清算类型
			cfetsTradeBondPledge.setSettlCurrency("CNY");//结算币种
			cfetsTradeBondPledge.setBondCurrency("CNY");//债券币种
			cfetsTradeBondPledge.setSettlCurrFxrate(new BigDecimal("1"));//汇率
//			cfetsTradeBondPledge.setCrossCustInstname1(crossCustInstname1);//跨托管机构名称1
//			cfetsTradeBondPledge.setCrossCustAmt1(crossCustAmt1);//跨托管交易金额1
//			cfetsTradeBondPledge.setCrossCustInstname2(crossCustInstname2);//跨托管机构名称2
//			cfetsTradeBondPledge.setCrossCustAmt2(crossCustAmt2);//跨托管交易金额2
//			cfetsTradeBondPledge.setLastPx(lastPx);//成交利率
//			cfetsTradeBondPledge.setLastQty(lastQty);//成交金额
//			cfetsTradeBondPledge.setLeavesQty(leavesQty);//剩余金额
//			cfetsTradeBondPledge.setSecurityId(securityId);//合同编号
			
			List<CfetsTradeSubbond> subBondList = new ArrayList<CfetsTradeSubbond>();
			CfetsTradeSubbond cfetsTradeSubbond = new CfetsTradeSubbond();
			cfetsTradeSubbond.setTableId("20210201091012000743");//主键
			cfetsTradeSubbond.setTablePid("20210201091012000742");//外键CFETS_TRADE_PLEDGE
			cfetsTradeSubbond.setTableCid("20210201091012000741");//外键CFETS_TRADE_PROGRESS
			cfetsTradeSubbond.setBondCode("ywxpthq0001");//债券编码
			cfetsTradeSubbond.setBondName("普通含权0001");//债券名称
			cfetsTradeSubbond.setFaceAmt(new BigDecimal("1000000"));//券面总额
			cfetsTradeSubbond.setDiscountType("HAIRCUT");//折算比例
			cfetsTradeSubbond.setDiscountValue(new BigDecimal("95"));//折算比例值
//			cfetsTradeSubbond.setDiscountAmt(discountAmt);//折算金额
//			cfetsTradeSubbond.setMarketAmt(marketAmt);//估值净价(元)

			subBondList.add(cfetsTradeSubbond);
			cfetsTradeBondPledge.setSubBondList(subBondList);
			
			cfetsTradeProgress.setCfetsTradeBondPledge(cfetsTradeBondPledge);
			
			List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
			CfetsTradeParty party1 = new CfetsTradeParty();
			party1.setTableId("20210304110122001542");//主键
			party1.setTableCid("20210304110122001540");//外键CFETS_TRADE_PROGRESS
			party1.setPartyId("111001132000000104001");//交易账号
			party1.setPartyRole("1");//账号角色
			party1.setPartySubid2("jsbkdealer");//交易员ID
//			party1.setPartySubid266(partySubid266);//交易账户6位码
//			party1.setPartySubid29(partySubid29);//机构来源
//			party1.setPartySubid135(partySubid135);//机构6位码
//			party1.setPartySubid125(partySubid125);//机构中文简称
//			party1.setPartySubid267(partySubid267);//交易账户中文简称
//			party1.setPartySubid101(partySubid101);//交易员名称
			
			partyList.add(party1);
			
			CfetsTradeParty party2 = new CfetsTradeParty();
			party2.setTableId("20210304110122001543");//主键
			party2.setTableCid("20210304110122001540");//外键CFETS_TRADE_PROGRESS
			party2.setPartyId("105001113060000104001");//交易账号
			party2.setPartyRole("17");//账号角色
			party2.setPartySubid2("bdyhdealerjsbk");//交易员ID

			partyList.add(party2);

			cfetsTradeProgress.setPartyList(partyList);
			
			System.out.println("开始成功");
			RetBean retBean = iImixRmbTradeManageServer.sendRMBTradeUp(cfetsTradePackage, cfetsTradeProgress);
			System.out.println("执行成功==>RetCode:" + retBean.getRetCode()+"RetMsg:"+retBean.getRetMsg());
		} catch (RemoteConnectFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
