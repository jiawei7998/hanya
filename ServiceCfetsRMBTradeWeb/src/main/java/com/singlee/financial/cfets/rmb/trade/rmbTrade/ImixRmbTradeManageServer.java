package com.singlee.financial.cfets.rmb.trade.rmbTrade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.rmb.trade.IImixRmbTradeManageServer;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeCommonOther;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMarketData;
import com.singlee.financial.cfets.rmb.trade.service.SendAPackageRmbTrade;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.Message;
import imix.client.core.ImixSession;

@Service
public class ImixRmbTradeManageServer implements IImixRmbTradeManageServer{

	private static Logger LogManager = LoggerFactory.getLogger(ImixRmbTradeManageServer.class);

	/**
	 * CFETS配置信息
	 */
	@Autowired
	private ImixRmbTradeClient imixRmbTradeClient;

	@Override
	public boolean startImixSession(String quoteType) {
		// 是否登录
		boolean isLogin = false;
		try {
			isLogin = imixRmbTradeClient.getImixRmbTradeAutoConn(quoteType).startConn();
		} catch (Exception e) {
			LogManager.error("连接出错:"+e.getMessage(),e);
			e.printStackTrace();
		}
		return isLogin;
	}

	@Override
	public boolean stopImixSession(String quoteType) {
//		imixRmbTradeClient.getImixRmbTradeAutoConn("").setStopImixSessionBySystem(true);
		// 是否登录
		boolean isLogin = false;
		ImixSession imixSession = null;
		imixSession = imixRmbTradeClient.getImixSession(quoteType);
		if (imixSession.isStarted()) {
			imixSession.stop();
			isLogin = true;
		} else {
			isLogin = true;
		}
		return isLogin;
	}
	
	@Override
	public RetBean sendRMBTradeUp(CfetsTradePackage cfetsTradePackage,CfetsTradeProgress cfetsTradeProgress) throws RemoteConnectFailureException, Exception {
		RetBean retBean = null;
		boolean flag = false;
		Message message = null;
		try {
			retBean = SendAPackageRmbTrade.packageMessage(message,cfetsTradeProgress);
			LogManager.info("报文组装结果：retcode=["+retBean.getRetCode()+"],retMsg=["+retBean.getRetMsg()+"],"
					+ "sendMessage:"+message);
			if(ConvertRmbTrade.retSeccess.equals(retBean.getRetCode())) {
				String targetCompid =cfetsTradePackage.getTargetCompid();
				flag = imixRmbTradeClient.getImixSession(targetCompid).send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			throw e;
		}
		return retBean;
	}
	
	@Override
	public RetBean sendRMBTradeUp(CfetsTradePackage cfetsTradePackage, CfetsTradeMarketData cfetsTradeMarketData)
			throws RemoteConnectFailureException, Exception {
		RetBean retBean = null;
		boolean flag = false;
		Message message = null;
		try {
			retBean = SendAPackageRmbTrade.packageMessage(message,cfetsTradeMarketData);
			LogManager.info("报文组装结果：retcode=["+retBean.getRetCode()+"],retMsg=["+retBean.getRetMsg()+"],"
					+ "sendMessage:"+message);
			if(ConvertRmbTrade.retSeccess.equals(retBean.getRetCode())) {
				String targetCompid =cfetsTradePackage.getTargetCompid();
				flag = imixRmbTradeClient.getImixSession(targetCompid).send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			throw e;
		}
		return retBean;
	}

	@Override
	public RetBean sendRMBTradeUp(CfetsTradePackage cfetsTradePackage, CfetsTradeCommonOther cfetsTradeCommonOther)
			throws RemoteConnectFailureException, Exception {
		RetBean retBean = null;
		boolean flag = false;
		Message message = null;
		try {
			retBean = SendAPackageRmbTrade.packageMessage(message,cfetsTradeCommonOther);
			LogManager.info("报文组装结果：retcode=["+retBean.getRetCode()+"],retMsg=["+retBean.getRetMsg()+"],"
					+ "sendMessage:"+message);
			if(ConvertRmbTrade.retSeccess.equals(retBean.getRetCode())) {
				String targetCompid =cfetsTradePackage.getTargetCompid();
				flag = imixRmbTradeClient.getImixSession(targetCompid).send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			throw e;
		}
		return retBean;
	}

	
	@Override
	public boolean getImixSessionState(String quoteType) {
		ImixSession imixSession = imixRmbTradeClient.getImixSession(quoteType);
		return imixSession.isStarted();
	}

	@Override
	public boolean isSessionStarted(String quoteType) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String runShell(String shfile) {
		// TODO Auto-generated method stub
		return null;
	}

}
