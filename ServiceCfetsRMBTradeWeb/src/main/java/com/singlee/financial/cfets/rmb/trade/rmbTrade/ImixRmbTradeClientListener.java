package com.singlee.financial.cfets.rmb.trade.rmbTrade;

import imix.ConfigError;
import imix.FieldNotFound;
import imix.IncorrectDataFormat;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.client.core.ImixSession;
import imix.client.core.Listener;
import imix.client.core.MessageCracker;
import imix.field.MsgType;
import imix.imix10.Logout;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.financial.cfets.rmb.trade.config.ConfigBean;

/**
 * CFETS消息监听服务
 * 
 * @author xuqq
 * 
 */
public class ImixRmbTradeClientListener extends MessageCracker implements Listener {
	@Autowired
	private ImixRmbTradeClient imixRmbTradeClient;
	@Autowired
	private ImixRmbTradeCfetsApiService imixRmbTradeCfetsApiService;

	// 收报线程队列管理
	// CFETS配置类
	private ConfigBean configBean;

	// 收报线程队列管理
	private ThreadPoolExecutor socketServerThreadPool;

	private static Logger LogManager = LoggerFactory.getLogger(ImixRmbTradeClientListener.class);

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}
	@Override
	public void fromAdmin(Message message, ImixSession imixSession)
			throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue {
		LogManager.info("******* fromAdmin() **************");
		LogManager.debug((new StringBuilder("imixSession:")).append(imixSession).toString());
		try {
			LogManager.info("*******String(35):" + message.getHeader().getString(35).toString() + "**********");
		} catch (FieldNotFound e) {
			LogManager.error("fromAdmin:", e);
		}
		LogManager.info("**********************************");
		try {
			if ("5".equals(message.getHeader().getString(MsgType.FIELD))) {
				// 登出
				Logout logout = (Logout) message;
				// 5表示登录失败
				if (logout.isSetLogoutStatus()) {
					LogManager.info("用户登录失败,失败代码:" + logout.getLogoutStatus() + "[17-密码过期]");
				} else if (logout.isSetSessionStatus()) {
					LogManager.info("用户注销成功,代码:" + logout.getSessionStatus() + "[4-注销成功]");
				} else if (logout.isSetText()) {
					LogManager.info("登录失败,失败代码:" + logout.getText() + "[2-用户信息验证失败]");
				} else {
					LogManager.info("登录失败,原因未知");
				}
			} else {
				String targetCompID = message.getHeader().getString(56);
				String targetSubID = message.getHeader().getString(57);
				String quoteType = targetCompID + "-" + targetSubID;
				LogManager.info("***********************************");
				LogManager.info("******* " + quoteType + "!************");
				LogManager.info("******* 登录 成功 !************");
				LogManager.info("***********************************");
				imixRmbTradeClient.getImixRmbTradeAutoConn(quoteType).setConn(true);
			}
		} catch (FieldNotFound e) {
			LogManager.error("fromAdmin:", e);
			return;
		}
	}
	@Override
	public void fromApp(Message message, ImixSession imixSession)
			throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
		LogManager.info("********fromApp:" + message);
		// 接收到消息后,开启一个线程
		getThreadPool().execute(new ImixRmbTradeRecMessageProcess(message, imixRmbTradeCfetsApiService, configBean,
				ImixRmbTradeClientListener.this));
	}
	@Override
	public void onError(ImixSession imixSession, int type) {
		LogManager.info("connection failure:" + (new StringBuilder("imixSession:")).append(imixSession).toString());
		// 断开 重连
		if (1 == type) {
			try {
				LogManager.info("*******系统捕获CpisClient错误消息,立即进行重新连接!**********");
				imixSession.start();
			} catch (ConfigError e) {
				LogManager.error("onError exception", e);
			}
		}

	}
	@Override
	public void onLogon(ImixSession imixSession) {
		LogManager.info("********onLogon********" + (new StringBuilder("imixSession:")).append(imixSession).toString());

	}
	@Override
	public void onLogout(ImixSession imixSession) {
		LogManager.info("onLogonOut:" + (new StringBuilder("imixSession:")).append(imixSession).toString());
		// 登陆失败,系统开始连接
		try {
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
//						imixRmbTradeClient.getImixRmbTradeAutoConn("").setConnStatus(false);
						imixRmbTradeClient.getImixRmbTradeAutoConn("").startConn();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			t.start();

		} catch (Exception e) {
			LogManager.error("连接出错:" + e.getMessage(), e);
			e.printStackTrace();
		}
	}
	@Override
	public void toApp(Message message, ImixSession imixSession) {
		LogManager.info("********toApp:" + message);
		LogManager.info("********" + (new StringBuilder("imixSession:")).append(imixSession).toString());
	}

	public ImixRmbTradeClient getImixRmbTradeClient() {
		return imixRmbTradeClient;
	}

	private ThreadPoolExecutor getThreadPool() {
		// 创建客户羰线程池管理对象
		if (null == socketServerThreadPool) {
			socketServerThreadPool = new ThreadPoolExecutor(configBean.getCorePoolSize(),
					configBean.getMaximumPoolSize(), configBean.getKeepAliveTime(), TimeUnit.SECONDS,
					new ArrayBlockingQueue<Runnable>(configBean.getArrayBlockingQueueSize()),
					new ThreadPoolExecutor.CallerRunsPolicy());
		}
		return socketServerThreadPool;
	}
}
