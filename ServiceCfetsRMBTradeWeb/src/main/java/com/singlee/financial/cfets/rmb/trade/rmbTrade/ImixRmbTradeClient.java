package com.singlee.financial.cfets.rmb.trade.rmbTrade;

import com.singlee.financial.cfets.rmb.trade.config.ConfigBean;
import com.singlee.financial.cfets.rmb.trade.utils.CfetsRmbTradeUtils;
import imix.ConfigError;
import imix.Message;
import imix.client.core.ImixApplication;
import imix.client.core.ImixSession;
import imix.imix10.Confirmation;
import imix.imix10.ExecutionAcknowledgement;
import imix.imix10.ExecutionReport;
import imix.protocol.version.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * CFETS监听管理类
 * 
 * @author xuqq
 * 
 */
public class ImixRmbTradeClient {

	// CFETS配置类
	private ConfigBean configBean;

	// CFETS监听事件类
	private ImixRmbTradeClientListener listener;

	private static Map<String, Message> qryMap = new HashMap<String, Message>();

	protected ConcurrentHashMap<String, ImixRmbTradeAutoConn> imixRmbTradeAutoConnMap = new ConcurrentHashMap<String, ImixRmbTradeAutoConn>();

	/***
	 * 存放各类ImixSession
	 */
	protected ConcurrentHashMap<String, ImixSession> sessionMap = new ConcurrentHashMap<String, ImixSession>();

	private static Logger LogManager = LoggerFactory.getLogger(ImixRmbTradeClient.class);

	/**
	 * 初始化CFETS服务,登录CFETS服务器
	 * 
	 * @return
	 * @throws Exception
	 */
	public void initLogin() throws Exception {
		// 是否登录
		boolean isLogin = false;
		String rtfttp;// 添加RelatedToFTPPath属性值
		try {
			LogManager.debug("configBean:" + configBean);
			// 获取当前加载器路径
			String originPath = ImixRmbTradeClient.class.getResource("/").getFile();
			LogManager.debug("Origin configfile:" + originPath);
			// 更改ftpclient.properties文件路径
			String ftpClientPath = CfetsRmbTradeUtils.updateConfigFile(configBean.getUpdateFtpPathKey(), originPath,
					ConfigBean.cfgFtpClientPath, configBean.getCharset());
			LogManager.debug("update ftpclient configfile:" + ftpClientPath);
			rtfttp = "RelatedToFTPPath=" + ftpClientPath;

			// 拷贝client.cfg文件,改相对路径为绝对路径
			String clientCfgPath = CfetsRmbTradeUtils.updateConfigFile(configBean.getUpdatePathKey(), originPath,
					ConfigBean.cfgClientPath, configBean.getCharset(), rtfttp);
			LogManager.debug("update client configfile:" + clientCfgPath);

			ImixApplication.initialize(listener, clientCfgPath);
			LogManager.info("USERNAME[" + this.configBean.getUserName() + "]PASSWORD[" + this.configBean.getPassword()
					+ "]MARKET[" + this.configBean.getMarket() + "]");
			LogManager.info("***************IMIXProtocol信息****************************");
			LogManager.info("The Version of IMIX Protocol is :"+ Version.getVersion());
			LogManager.info("Building Time is :"+Version.getBuildingTime());
			LogManager.info("*********************************************************");
//			imixSession = new ImixSession(this.configBean.getUserName(), this.configBean.getPassword(), this.configBean.getMarket());
//			isLogin = imixSession.start();
			connectImixSession("ALL");// 创建ImixSession
			isLogin = startImixSession("ALL");// 启动ImixSession
			LogManager.info("CFETS FX LOGIN:[登录 isLogin:" + isLogin + "]***********************************");
		} catch (Exception e) {
			LogManager.error("CFETS FX LOGIN Exception:[" + e.getMessage() + "]", e);
		}
	}

	public ImixRmbTradeAutoConn getImixRmbTradeAutoConn(String paramQuoteType) {
		ImixRmbTradeAutoConn imixRmbTradeAutoConn = imixRmbTradeAutoConnMap.get(paramQuoteType);
		if (imixRmbTradeAutoConn == null) {
			imixRmbTradeAutoConn = new ImixRmbTradeAutoConn(sessionMap.get(paramQuoteType), paramQuoteType,
					configBean.getReconCount(), configBean.getReconTime());
			imixRmbTradeAutoConnMap.put(paramQuoteType, imixRmbTradeAutoConn);
		}
		return imixRmbTradeAutoConn;
	}

	/**
	 * 方法已重载.发送信息到CFETS服务器
	 * 
	 * @param execID
	 * @param execAckStatus
	 * @return
	 */
	public static boolean sendMessage(ExecutionReport executionReport, String execID, int execAckStatus) {
		return sendMessage(executionReport, generateMsg(execID, null, execAckStatus));
	}

	/**
	 * 方法已重载.发送信息到CFETS服务器
	 * 
	 * @param message
	 * @return
	 */
	public static boolean sendMessage(ExecutionReport executionReport, Message message) {
		return ImixSession.lookupIMIXSession(executionReport).send(message);
	}

	public boolean sendMessage(String paramQuoteType, Confirmation message) {
		return sessionMap.get(paramQuoteType).send(message);
	}

	/**
	 * 创建CFETS消息类
	 * 
	 * @param execID
	 * @param messageEncoding
	 * @param execAckStatus
	 * @return
	 */
	private static Message generateMsg(String execID, String messageEncoding, int execAckStatus) {
		ExecutionAcknowledgement msg = new ExecutionAcknowledgement();
		msg.setString(17, execID);
		if (messageEncoding == null) {
			messageEncoding = "UTF-8";}
		msg.getHeader().setString(347, messageEncoding);
		msg.setChar(1036, (char) (execAckStatus + 49));
		return msg;
	}

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public ImixRmbTradeClientListener getListener() {
		return listener;
	}

	public void setListener(ImixRmbTradeClientListener listener) {
		this.listener = listener;
	}

	public Map<String, Message> getQryMap() {
		return qryMap;
	}

	public void setQryMap(Map<String, Message> qryMap) {
		ImixRmbTradeClient.qryMap = qryMap;
	}

	private void connectImixSession(String paramQuoteType) {
		String targetCompID = configBean.getTargetCompID();
		if (targetCompID == null || "".equals(targetCompID)) {
			LogManager.debug("配置参数targetCompID为空 : 不能启动");
			return;
		}
		String targetSubID = configBean.getTargetSubID();
		if (targetSubID == null || "".equals(targetSubID)) {
			LogManager.debug("配置参数targetSubID为空 : 不能启动");
			return;
		}
		String[] targetCompIDArr = targetCompID.split(",");
		String[] targetSubIDArr = targetSubID.split(",");
		ImixSession imixSession;
		for (int i = 0; i < targetCompIDArr.length; i++) {
			String quoteType = targetCompIDArr[i] + "-" + targetSubIDArr[i];
			try {
				if ("ALL".equals(paramQuoteType)) {
					imixSession = new ImixSession(configBean.getUserName(), configBean.getPassword(),
							configBean.getSendercompid(), targetCompIDArr[i], targetSubIDArr[i]);
					sessionMap.put(quoteType, imixSession);
				} else if (quoteType.equals(paramQuoteType)) {
					imixSession = new ImixSession(configBean.getUserName(), configBean.getPassword(),
							configBean.getSendercompid(), targetCompIDArr[i], targetSubIDArr[i]);
					sessionMap.put(quoteType, imixSession);
				}
				LogManager.debug("创建imixSession成功：" + quoteType);
			} catch (Exception e) {
				LogManager.debug("创建imixSession异常：" + quoteType + "。异常信息：" + e.getMessage());
				return;
			}
		}

	}

	boolean startImixSession(String paramQuoteType) throws ConfigError {
		boolean isLogon = false;
		for (String key : sessionMap.keySet()) {
			try {
				if ("ALL".equals(paramQuoteType)) {
					isLogon = sessionMap.get(key).start();
				} else if (key.equals(paramQuoteType)) {
					isLogon = sessionMap.get(key).start();
				}
				if (isLogon) {
					LogManager.debug("启动imixSession成功：" + key);
				} else {
					LogManager.debug("启动imixSession失败：" + key + "。停止进入重连。");
				}
			} catch (Exception e) {
				LogManager.debug("启动imixSession异常：" + key + "。异常信息" + e.getMessage());
			}
		}
		return isLogon;
	}

	boolean stopImixSession(String paramQuoteType) throws ConfigError {
		boolean isLogon = false;
		for (String key : sessionMap.keySet()) {
			try {
				if ("ALL".equals(paramQuoteType)) {
					sessionMap.get(key).stop();
				} else if (key.equals(paramQuoteType)) {
					sessionMap.get(key).stop();
				}
				LogManager.debug("停止imixSession成功：" + key);
			} catch (Exception e) {
				LogManager.debug("停止imixSession异常：" + key + "。异常信息" + e.getMessage());
			}
		}
		return isLogon;
	}

	public ImixSession getImixSession(String paramQuoteType) {
		return sessionMap.get(paramQuoteType);
	}
}
