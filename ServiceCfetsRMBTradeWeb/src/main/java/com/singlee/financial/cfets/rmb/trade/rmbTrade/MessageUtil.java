package com.singlee.financial.cfets.rmb.trade.rmbTrade;

import imix.FieldNotFound;
import imix.Message;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoLegs;


/**
 * 
 * <P>
 * DateUtil.java
 * </P>
 * <P>
 * 报文处理工具类
 * </P>
 * <P>
 * Copyright: Copyright (c) 2018
 * </P>
 * <P>
 * Company: 新利信息科技有限公司
 * </P>
 * 
 * @author 彭前
 */

public class MessageUtil {
	private Message message;

	public MessageUtil(Message message2) {
		this.message = (Message) message2;
	}

	/**
	 * 功能描述:返回报文对应域为字符串的值
	 * 
	 * @param int
	 * @author Administrator
	 * @throws FieldNotFound
	 * @date
	 */
	public String getStringValue(int arg0) throws FieldNotFound {
		if (message.isSetField(arg0)) {
			return message.getString(arg0);
		}
		return "";
	}

	/**
	 * 功能描述:返回报文对应域为整型的值
	 * 
	 * @param 域号
	 * @author Administrator
	 * @throws FieldNotFound
	 * @date
	 */
	public int getIntValue(int arg0) throws FieldNotFound {
		int num = 0;
		if (message.isSetField(arg0)) {
			String numStr = message.getString(arg0);
			try {
				num = Integer.parseInt(numStr.trim());
			} catch (NumberFormatException e) {
				num = 0;
				e.printStackTrace();
			}
			return num;
		}
		return num;
	}

	/**
	 * 功能描述:返回报文头对应域的值
	 * 
	 * @param 域号
	 * @author Administrator
	 * @throws FieldNotFound
	 * @date
	 */
	public String getHeadStringValue(int arg0) throws FieldNotFound {
		if (message.getHeader().isSetField(arg0)) {
			return message.getHeader().getString(arg0);
		}
		return "";
	}

	
	public int getIntValue(ExecutionReport.NoPartyIDs nopartyids1, int field) throws Exception{
		return nopartyids1.isSetField(field) ? nopartyids1.getInt(field) : 0;
	}
	
	public String getStringValue(ExecutionReport.NoPartyIDs nopartyids1, int field) throws Exception{
		return nopartyids1.isSetField(field) ? nopartyids1.getString(field) : "";
	}
	
	public int getIntValue(ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1, int field) throws Exception{
		return nopartysubids1.isSetField(field) ? nopartysubids1.getInt(field) : 0;
	}
	
	public String getStringValue(ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1, int field) throws Exception{
		return nopartysubids1.isSetField(field) ? nopartysubids1.getString(field) : "";
	}
	
	public String getStringValue(NoLegs noLegs, int field) throws Exception{
		return noLegs.isSetField(field) ? noLegs.getString(field) : "";
	}
	
	public int getIntValue(NoLegs noLegs, int field) throws Exception{
		return noLegs.isSetField(field) ? noLegs.getInt(field) :0;
	}

	
	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

}
