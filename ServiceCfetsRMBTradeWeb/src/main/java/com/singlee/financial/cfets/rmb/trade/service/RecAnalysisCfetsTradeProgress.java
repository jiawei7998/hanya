package com.singlee.financial.cfets.rmb.trade.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.cbt.CfetsTradeBond;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeAllocs;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeBondPledge;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeSubbond;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.FieldNotFound;
import imix.Group;
import imix.IncorrectTagValue;
import imix.UnsupportedMessageType;
import imix.field.AccruedInterestAmt;
import imix.field.AccruedInterestTotalAmt;
import imix.field.AnonymousAgencyIndicator;
import imix.field.ClearingMethod;
import imix.field.ContingencyIndicator;
import imix.field.DataSourceString;
import imix.field.DeliveryType;
import imix.field.DeliveryType2;
import imix.field.DirtyPrice;
import imix.field.GrossTradeAmt;
import imix.field.LastPx;
import imix.field.LastQty;
import imix.field.LeavesQty;
import imix.field.LegAccruedInterestAmt;
import imix.field.LegClearingMethod;
import imix.field.LegDeliveryType;
import imix.field.LegLastQty;
import imix.field.LegOrderQty;
import imix.field.LegPrice;
import imix.field.LegSecurityID;
import imix.field.LegSettlCurrFxRate;
import imix.field.LegSettlType;
import imix.field.LegSide;
import imix.field.MarketIndicator;
import imix.field.MatchType;
import imix.field.MaxFloor;
import imix.field.MsgType;
import imix.field.NoAllocs;
import imix.field.NoCrossCustodians;
import imix.field.NoLegStipulations;
import imix.field.NoLegs;
import imix.field.NoPartyIDs;
import imix.field.NoQuoteEntries;
import imix.field.NoRelatedSym;
import imix.field.NoRoutingIDs;
import imix.field.NoStipulations;
import imix.field.NoUnderlyings;
import imix.field.OrdStatus;
import imix.field.OrdType;
import imix.field.OrderQty;
import imix.field.Price;
import imix.field.Principal;
import imix.field.QuoteStatus;
import imix.field.QuoteType;
import imix.field.RoutingType;
import imix.field.SecurityID;
import imix.field.SenderSubID;
import imix.field.SettlCurrAmt;
import imix.field.SettlCurrFxRate;
import imix.field.SettlCurrency;
import imix.field.SettlDate;
import imix.field.SettlType;
import imix.field.Side;
import imix.field.Spread;
import imix.field.Symbol;
import imix.field.TotalPrincipal;
import imix.field.TradeCashAmt;
import imix.field.TransactTime;
import imix.field.ValidUntilTime;
import imix.field.Yield;
import imix.field.YieldType;
import imix.imix20.ExecutionReport;
import imix.imix20.Quote;
import imix.imix20.QuoteCancel;
import imix.imix20.QuoteRequest;
import imix.imix20.QuoteRequestAck;
import imix.imix20.QuoteRequestCancel;
import imix.imix20.QuoteRequestReject;
import imix.imix20.QuoteResponse;
import imix.imix20.QuoteStatusReport;
import imix.imix20.component.Parties;
import imix.imix20.component.QuotReqACKGrp;
import imix.imix20.component.QuotReqCxlGrp;
import imix.imix20.component.RoutingGrp;

public class RecAnalysisCfetsTradeProgress {

	private static Logger LogManager = LoggerFactory.getLogger("RecAnalysis");
	
	/**
	 * 解析报文内容
	 */
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,Quote quote) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";
		String marketIndicator="";//市场类型
		try {
			//1、报文解析通用字段
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=quote.getHeader().isSetField(MsgType.FIELD) ? quote.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			marketIndicator = quote.isSetMarketIndicator() ? quote.getMarketIndicator().getValue() : "";
			cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
			cfetsTradeProgress.setQuoteType(quote.isSetQuoteType() ? 
					String.valueOf(quote.getQuoteType().getValue()) : "");//报价类别
			cfetsTradeProgress.setQuoteTransType(quote.isSetQuoteTransType() ? 
					String.valueOf(quote.getQuoteTransType().getValue()) : "");//操作类型
			cfetsTradeProgress.setQuoteStatus(quote.isSetQuoteStatus() ? 
					String.valueOf(quote.getQuoteStatus().getValue()) : "");//报价状态
			cfetsTradeProgress.setClOrdId(quote.isSetClOrdID() ?
					quote.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setQueryRequestId(quote.isSetQueryRequestID() ?
					quote.getQueryRequestID().getValue() : "");//查询请求编号（查询场景）
			cfetsTradeProgress.setOrigClOrdId(quote.isSetOrigClOrdID() ?
					quote.getOrigClOrdID().getValue() : "");//原客户参考编号，预留字段
			cfetsTradeProgress.setQuoteReqId(quote.isSetQuoteReqID() ? 
					quote.getQuoteReqID().getValue() : "");//请求报价编号
			cfetsTradeProgress.setQuoteId(quote.isSetQuoteID() ? 
					quote.getQuoteID().getValue() : "");//报价编号/请求回复编号
			cfetsTradeProgress.setTransactTime(quote.isSetTransactTime() ?
					quote.getTransactTime().getValue() : "");//业务发生时间
			cfetsTradeProgress.setQuoteTime(quote.isSetQuoteTime() ?
					quote.getQuoteTime().getValue() : "");//报价发生时间
			cfetsTradeProgress.setUserReference1(quote.isSetUserReference1() ?
					quote.getUserReference1().getValue() : "");//用户参考数据1
			cfetsTradeProgress.setUserReference2(quote.isSetUserReference2() ?
					quote.getUserReference2().getValue() : "");//用户参考数据2
			cfetsTradeProgress.setUserReference3(quote.isSetUserReference3() ?
					quote.getUserReference3().getValue() : "");//用户参考数据3
			cfetsTradeProgress.setUserReference4(quote.isSetUserReference4() ?
					quote.getUserReference4().getValue() : "");//用户参考数据4
			cfetsTradeProgress.setUserReference5(quote.isSetUserReference5() ?
					quote.getUserReference5().getValue() : "");//用户参考数据5
			cfetsTradeProgress.setUserReference6(quote.isSetUserReference6() ?
					quote.getUserReference6().getValue() : "");//用户参考数据6
			cfetsTradeProgress.setChannel(quote.isSetChannel() ? 
					quote.getChannel().getValue() : "");//渠道
			
			if("4".equals(marketIndicator)) {
				//现券买卖
				analysisCfetsTradeProgressCbt(retBean,cfetsTradeProgress,quote);
				//4、交易机构信息
				Parties parties = quote.getParties();
				List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
	            if (parties !=null && parties.isSetNoPartyIDs()) {
	            	RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, parties.getGroups(NoPartyIDs.FIELD));
	            }else if (quote.isSetNoPartyIDs()) {
	            	RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, quote);
	            }
	            cfetsTradeProgress.setPartyList(partyList);
	            
	            RoutingGrp routingGrp = quote.getRoutingGrp();
	            if(routingGrp!=null && routingGrp.isSetNoRoutingIDs()) {
	            	Group group = routingGrp.getGroup(1, NoRoutingIDs.FIELD);
	            	cfetsTradeProgress.setRoutingType(group.isSetField(RoutingType.FIELD)?
                		group.getString(RoutingType.FIELD):"");//发送对象
	            }
			}else if("9".equals(marketIndicator)){
				//质押式回购
				analysisCfetsTradeProgressRepo(retBean,cfetsTradeProgress,quote);
				//4、交易机构信息
				List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
	            if (quote.isSetNoPartyIDs()) {
	            	RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, quote);
	            }
	            cfetsTradeProgress.setPartyList(partyList);
			}else {
				retBean.setRetCode(ConvertRmbTrade.retError);
				retBean.setRetMsg("组装对象CfetsTradeProgress失败:市场类型错误["+marketIndicator+"]");
			}
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteStatusReport quoteStatusReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";
		String marketIndicator="";//市场类型
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=quoteStatusReport.getHeader().isSetField(MsgType.FIELD) ? quoteStatusReport.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			marketIndicator = quoteStatusReport.isSetMarketIndicator() ? 
					quoteStatusReport.getMarketIndicator().getValue() : "";
			cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
			cfetsTradeProgress.setQuoteType(quoteStatusReport.isSetQuoteType() ? 
					String.valueOf(quoteStatusReport.getQuoteType().getValue()) : "");//报价类别
			cfetsTradeProgress.setQuoteTransType(quoteStatusReport.isSetQuoteTransType() ? 
					String.valueOf(quoteStatusReport.getQuoteTransType().getValue()) : "");//操作类型
			cfetsTradeProgress.setQuoteStatus(quoteStatusReport.isSetQuoteStatus() ? 
					String.valueOf(quoteStatusReport.getQuoteStatus().getValue()) : "");//报价状态
			cfetsTradeProgress.setClOrdId(quoteStatusReport.isSetClOrdID() ?
					quoteStatusReport.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setQuoteReqId(quoteStatusReport.isSetQuoteReqID() ?
					quoteStatusReport.getQuoteReqID().getValue() : "");//
			cfetsTradeProgress.setQuoteId(quoteStatusReport.isSetQuoteID() ? 
					quoteStatusReport.getQuoteID().getValue() : "");//报价编号
			cfetsTradeProgress.setTransactTime(quoteStatusReport.isSetTransactTime() ?
					quoteStatusReport.getTransactTime().getValue() : "");//业务发生时间
			cfetsTradeProgress.setUserReference1(quoteStatusReport.isSetUserReference1() ?
					quoteStatusReport.getUserReference1().getValue() : "");//用户参考数据1
			cfetsTradeProgress.setUserReference2(quoteStatusReport.isSetUserReference2() ?
					quoteStatusReport.getUserReference2().getValue() : "");//用户参考数据2
			cfetsTradeProgress.setUserReference3(quoteStatusReport.isSetUserReference3() ?
					quoteStatusReport.getUserReference3().getValue() : "");//用户参考数据3
			cfetsTradeProgress.setUserReference4(quoteStatusReport.isSetUserReference4() ?
					quoteStatusReport.getUserReference4().getValue() : "");//用户参考数据4
			cfetsTradeProgress.setUserReference5(quoteStatusReport.isSetUserReference5() ?
					quoteStatusReport.getUserReference5().getValue() : "");//用户参考数据5
			cfetsTradeProgress.setUserReference6(quoteStatusReport.isSetUserReference6() ?
					quoteStatusReport.getUserReference6().getValue() : "");//用户参考数据6
			
			
			if("4".equals(marketIndicator)) {
				//现券买卖
				analysisCfetsTradeProgressCbt(retBean,cfetsTradeProgress,quoteStatusReport);
			}else if("9".equals(marketIndicator)){
				//质押式回购
				analysisCfetsTradeProgressRepo(retBean,cfetsTradeProgress,quoteStatusReport);
			}else {
				retBean.setRetCode(ConvertRmbTrade.retError);
				retBean.setRetMsg("组装对象CfetsTradeProgress失败:市场类型错误["+marketIndicator+"]");
			}
			
			//4、交易机构信息
			List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
            if (quoteStatusReport.isSetNoPartyIDs()) {
            	RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, quoteStatusReport);
            }
            cfetsTradeProgress.setPartyList(partyList);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteRequest msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";
		String marketIndicator="";//市场类型
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			cfetsTradeProgress.setClOrdId(msg.isSetClOrdID() ?
					msg.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setQueryRequestId(msg.isSetQueryRequestID() ?
					msg.getQueryRequestID().getValue() : "");//查询请求编号
			cfetsTradeProgress.setAllocInd(msg.isSetAllocInd() ?
					msg.getAllocInd().getValue() : "");//分账标识
			cfetsTradeProgress.setQuoteReqId(msg.isSetQuoteReqID() ?
					msg.getQuoteReqID().getValue() : "");//请求报价编号
			cfetsTradeProgress.setQuoteId(msg.isSetReference() ?
					msg.getReference().getValue() : "");//指示性报价编号
			cfetsTradeProgress.setUserReference1(msg.isSetUserReference1() ?
					msg.getUserReference1().getValue() : "");//用户参考数据1
			cfetsTradeProgress.setUserReference2(msg.isSetUserReference2() ?
					msg.getUserReference2().getValue() : "");//用户参考数据2
			cfetsTradeProgress.setUserReference3(msg.isSetUserReference3() ?
					msg.getUserReference3().getValue() : "");//用户参考数据3
			cfetsTradeProgress.setUserReference4(msg.isSetUserReference4() ?
					msg.getUserReference4().getValue() : "");//用户参考数据4
			cfetsTradeProgress.setUserReference5(msg.isSetUserReference5() ?
					msg.getUserReference5().getValue() : "");//用户参考数据5
			cfetsTradeProgress.setUserReference6(msg.isSetUserReference6() ?
					msg.getUserReference6().getValue() : "");//用户参考数据6
			
			
			if(msg.isSetNoRelatedSym()) {
				List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
				CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
				Group group = msg.getGroup(1,NoRelatedSym.FIELD);
				
				marketIndicator = group.isSetField(MarketIndicator.FIELD) ? 
						group.getString(MarketIndicator.FIELD) : "";
				cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
				cfetsTradeProgress.setQuoteType(group.isSetField(QuoteType.FIELD) ? 
						group.getString(QuoteType.FIELD) : "");//报价类别
				cfetsTradeProgress.setQuoteStatus(group.isSetField(QuoteStatus.FIELD) ? 
						group.getString(QuoteStatus.FIELD) : "");//报价状态
				cfetsTradeProgress.setValidUntilTime(group.isSetField(ValidUntilTime.FIELD) ? 
						group.getString(ValidUntilTime.FIELD) : "");//有效时间
				cfetsTradeProgress.setTransactTime(group.isSetField(TransactTime.FIELD) ? 
						group.getString(TransactTime.FIELD) : "");//业务发生时间
				
				
				cfetsTradeBond.setSymbol(group.isSetField(Symbol.FIELD) ? 
						group.getString(Symbol.FIELD) : "");//债券名称
				cfetsTradeBond.setSecurityId(group.isSetField(SecurityID.FIELD) ? 
						group.getString(SecurityID.FIELD) : "");//债券代码
				cfetsTradeBond.setSide(group.isSetField(Side.FIELD) ? 
						group.getString(Side.FIELD) : "");//交易方向
				cfetsTradeBond.setAccruedInterestAmt(group.isSetField(AccruedInterestAmt.FIELD) ? 
						group.getDecimal(AccruedInterestAmt.FIELD) : null);//应计利息
				cfetsTradeBond.setAccruedInterestTotalamt(group.isSetField(AccruedInterestTotalAmt.FIELD) ? 
						group.getDecimal(AccruedInterestTotalAmt.FIELD) : null);//应计利息总额
				cfetsTradeBond.setOrderQty(group.isSetField(OrderQty.FIELD) ? 
						group.getDecimal(OrderQty.FIELD) : null);//券面总额
				cfetsTradeBond.setDeliveryType(group.isSetField(DeliveryType.FIELD) ? 
						group.getString(DeliveryType.FIELD) : "");//结算方式
				cfetsTradeBond.setClearingMethod(group.isSetField(ClearingMethod.FIELD) ? 
						group.getString(ClearingMethod.FIELD) : "");//清算类型
				cfetsTradeBond.setSettType(group.isSetField(SettlType.FIELD) ? 
						group.getString(SettlType.FIELD) : "");//清算速度
				cfetsTradeBond.setSettlDate(group.isSetField(SettlDate.FIELD) ? 
						group.getString(SettlDate.FIELD) : "");//结算日
				cfetsTradeBond.setPrincipal(group.isSetField(Principal.FIELD) ? 
						group.getDecimal(Principal.FIELD) : null);//每百元本金额
				cfetsTradeBond.setTotalPrincipal(group.isSetField(TotalPrincipal.FIELD) ? 
						group.getDecimal(TotalPrincipal.FIELD) : null);//本金额
				
				bondList.add(cfetsTradeBond);
				cfetsTradeProgress.setBondList(bondList);
				
				//4、交易机构信息
				List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
				if(group.isSetField(NoPartyIDs.FIELD)) {
					RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, group.getGroups(NoPartyIDs.FIELD));
				}
				cfetsTradeProgress.setPartyList(partyList);
				
				//5、分账数据
				List<CfetsTradeAllocs> allocsList = new ArrayList<CfetsTradeAllocs>();
				if(group.isSetField(NoAllocs.FIELD)) {
					RecAAnalysisRmbTradeGroup.analysisNoAllocs(retBean, allocsList, group.getGroups(NoAllocs.FIELD));
				}
				cfetsTradeProgress.setAllocsList(allocsList);
						
			}
			
			
			
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteRequestCancel msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";
		String marketIndicator="";//市场类型
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			cfetsTradeProgress.setClOrdId(msg.isSetClOrdID() ?
					msg.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setOrigClOrdId(msg.isSetOrigClOrdID() ?
					msg.getOrigClOrdID().getValue() : "");//原客户参考编号
			cfetsTradeProgress.setQuoteReqId(msg.isSetQuoteReqID() ?
					msg.getQuoteReqID().getValue() : "");//请求报价编号
			
			QuotReqCxlGrp quotReqCxlGrp = msg.getQuotReqCxlGrp();
			if(quotReqCxlGrp!=null) {
				if(quotReqCxlGrp.isSetNoRelatedSym()) {
					List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
					CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
					Group group = quotReqCxlGrp.getGroup(1,NoRelatedSym.FIELD);
					
					marketIndicator = group.isSetField(MarketIndicator.FIELD) ? 
							group.getString(MarketIndicator.FIELD) : "";
					cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
					cfetsTradeProgress.setQuoteType(group.isSetField(QuoteType.FIELD) ? 
							group.getString(QuoteType.FIELD) : "");//报价类别
					cfetsTradeProgress.setQuoteStatus(group.isSetField(QuoteStatus.FIELD) ? 
							group.getString(QuoteStatus.FIELD) : "");//报价状态
					cfetsTradeProgress.setTransactTime(group.isSetField(TransactTime.FIELD) ? 
							group.getString(TransactTime.FIELD) : "");//业务发生时间
					
					
					cfetsTradeBond.setSymbol(group.isSetField(Symbol.FIELD) ? 
							group.getString(Symbol.FIELD) : "-");//债券名称
					cfetsTradeBond.setSecurityId(group.isSetField(SecurityID.FIELD) ? 
							group.getString(SecurityID.FIELD) : "");//债券代码
					cfetsTradeBond.setPrice(group.isSetField(Price.FIELD) ? 
							group.getDecimal(Price.FIELD) : null);//
					
					bondList.add(cfetsTradeBond);
					cfetsTradeProgress.setBondList(bondList);
					
					//4、交易机构信息
					List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
					if(group.isSetField(NoPartyIDs.FIELD)) {
						RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, group.getGroups(NoPartyIDs.FIELD));
					}
					cfetsTradeProgress.setPartyList(partyList);
					
				}
			}
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteRequestAck msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";
		String marketIndicator="";//市场类型
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			cfetsTradeProgress.setClOrdId(msg.isSetClOrdID() ?
					msg.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setQuoteReqId(msg.isSetQuoteReqID() ?
					msg.getQuoteReqID().getValue() : "");//请求报价编号
			cfetsTradeProgress.setAllocInd(msg.isSetAllocInd() ?
					msg.getAllocInd().getValue() : "");//
			cfetsTradeProgress.setUserReference1(msg.isSetUserReference1() ?
					msg.getUserReference1().getValue() : "");//用户参考数据1
			cfetsTradeProgress.setUserReference2(msg.isSetUserReference2() ?
					msg.getUserReference2().getValue() : "");//用户参考数据2
			cfetsTradeProgress.setUserReference3(msg.isSetUserReference3() ?
					msg.getUserReference3().getValue() : "");//用户参考数据3
			cfetsTradeProgress.setUserReference4(msg.isSetUserReference4() ?
					msg.getUserReference4().getValue() : "");//用户参考数据4
			cfetsTradeProgress.setUserReference5(msg.isSetUserReference5() ?
					msg.getUserReference5().getValue() : "");//用户参考数据5
			cfetsTradeProgress.setUserReference6(msg.isSetUserReference6() ?
					msg.getUserReference6().getValue() : "");//用户参考数据6
			
			QuotReqACKGrp quotReqACKGrp = msg.getQuotReqACKGrp();
			if(quotReqACKGrp!=null) {
				if(quotReqACKGrp.isSetNoRelatedSym()) {
					List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
					CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
					Group group = quotReqACKGrp.getGroup(1,NoRelatedSym.FIELD);
					
					marketIndicator = group.isSetField(MarketIndicator.FIELD) ? 
							group.getString(MarketIndicator.FIELD) : "";
					cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
					cfetsTradeProgress.setQuoteType(group.isSetField(QuoteType.FIELD) ? 
							group.getString(QuoteType.FIELD) : "");//报价类别
					cfetsTradeProgress.setQuoteStatus(group.isSetField(QuoteStatus.FIELD) ? 
							group.getString(QuoteStatus.FIELD) : "");//报价状态
					cfetsTradeProgress.setTransactTime(group.isSetField(TransactTime.FIELD) ? 
							group.getString(TransactTime.FIELD) : "");//业务发生时间
					
					
					cfetsTradeBond.setSymbol(group.isSetField(Symbol.FIELD) ? 
							group.getString(Symbol.FIELD) : "-");//债券名称
					cfetsTradeBond.setSecurityId(group.isSetField(SecurityID.FIELD) ? 
							group.getString(SecurityID.FIELD) : "");//债券代码
					
					bondList.add(cfetsTradeBond);
					cfetsTradeProgress.setBondList(bondList);
					
					//4、交易机构信息
					List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
					if(group.isSetField(NoPartyIDs.FIELD)) {
						RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, group.getGroups(NoPartyIDs.FIELD));
					}
					cfetsTradeProgress.setPartyList(partyList);
					
					//5、分账数据
					List<CfetsTradeAllocs> allocsList = new ArrayList<CfetsTradeAllocs>();
					if(group.isSetField(NoAllocs.FIELD)) {
						RecAAnalysisRmbTradeGroup.analysisNoAllocs(retBean, allocsList, group.getGroups(NoAllocs.FIELD));
					}
					cfetsTradeProgress.setAllocsList(allocsList);
				}
			}
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteRequestReject msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";
		String marketIndicator="";//市场类型
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			
			cfetsTradeProgress.setClOrdId(msg.isSetClOrdID() ?
					msg.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setQuoteReqId(msg.isSetQuoteReqID() ?
					msg.getQuoteReqID().getValue() : "");//请求报价编号
			cfetsTradeProgress.setQuoteTransType(msg.isSetQuoteRequestRejectType() ?
					msg.getQuoteRequestRejectType().getValue() : "");//操作类型
			
			if(msg.isSetNoRelatedSym()) {
				List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
				CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
				Group group = msg.getGroup(1,NoRelatedSym.FIELD);
				
				marketIndicator = group.isSetField(MarketIndicator.FIELD) ? 
						group.getString(MarketIndicator.FIELD) : "";
				cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
				cfetsTradeProgress.setQuoteType(group.isSetField(QuoteType.FIELD) ? 
						group.getString(QuoteType.FIELD) : "");//报价类别
				cfetsTradeProgress.setTransactTime(group.isSetField(TransactTime.FIELD) ? 
						group.getString(TransactTime.FIELD) : "");//业务发生时间
				
				
				cfetsTradeBond.setSymbol(group.isSetField(Symbol.FIELD) ? 
						group.getString(Symbol.FIELD) : "-");//债券名称
				cfetsTradeBond.setSecurityId(group.isSetField(SecurityID.FIELD) ? 
						group.getString(SecurityID.FIELD) : "");//债券代码
				cfetsTradeBond.setSide(group.isSetField(Side.FIELD) ? 
						group.getString(Side.FIELD) : "");//
				
				bondList.add(cfetsTradeBond);
				cfetsTradeProgress.setBondList(bondList);
				
				//4、交易机构信息
				List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
				if(group.isSetField(NoPartyIDs.FIELD)) {
					RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, group.getGroups(NoPartyIDs.FIELD));
				}
				cfetsTradeProgress.setPartyList(partyList);
				
			}
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteResponse quoteResponse) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String quoteId="";
		String msgType="";
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=quoteResponse.getHeader().isSetField(MsgType.FIELD) ? quoteResponse.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			cfetsTradeProgress.setMarketindicator(quoteResponse.isSetMarketIndicator() ? 
					quoteResponse.getMarketIndicator().getValue() : "");//市场标识
			cfetsTradeProgress.setTradeRecordid(quoteResponse.isSetTradeRecordID() ? 
					quoteResponse.getTradeRecordID().getValue() : "");//交易记录编号
			cfetsTradeProgress.setChannel(quoteResponse.isSetChannel() ? 
					quoteResponse.getChannel().getValue() : "");//渠道
			cfetsTradeProgress.setQuoteType(quoteResponse.isSetQuoteType() ? 
					String.valueOf(quoteResponse.getQuoteType().getValue()) : "");//报价类别
//			cfetsTradeProgress.setQuoteTransType(quoteResponse.isSetQuoteTransType() ? 
//					String.valueOf(quoteResponse.getQuoteTransType().getValue()) : "");//操作类型
			cfetsTradeProgress.setQuoteTransType(quoteResponse.isSetQuoteRespType() ? 
					String.valueOf(quoteResponse.getQuoteRespType().getValue()) : "");//操作类型
			cfetsTradeProgress.setQuoteStatus(quoteResponse.isSetQuoteStatus() ? 
					String.valueOf(quoteResponse.getQuoteStatus().getValue()) : "");//报价状态
			cfetsTradeProgress.setClOrdId(quoteResponse.isSetClOrdID() ?
					quoteResponse.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setOrigClOrdId(quoteResponse.isSetOrigClOrdID() ? 
					String.valueOf(quoteResponse.getOrigClOrdID().getValue()) : "");//原客户参考编号
			quoteId = quoteResponse.isSetQuoteID() ? quoteResponse.getQuoteID().getValue() : "";
			cfetsTradeProgress.setQuoteId(quoteId);//报价编号
			cfetsTradeProgress.setTransactTime(quoteResponse.isSetTransactTime() ?
					quoteResponse.getTransactTime().getValue() : "");//业务发生时间
			cfetsTradeProgress.setUserReference1(quoteResponse.isSetUserReference1() ?
					quoteResponse.getUserReference1().getValue() : "");//用户参考数据1
			cfetsTradeProgress.setUserReference2(quoteResponse.isSetUserReference2() ?
					quoteResponse.getUserReference2().getValue() : "");//用户参考数据2
			cfetsTradeProgress.setUserReference3(quoteResponse.isSetUserReference3() ?
					quoteResponse.getUserReference3().getValue() : "");//用户参考数据3
			cfetsTradeProgress.setUserReference4(quoteResponse.isSetUserReference4() ?
					quoteResponse.getUserReference4().getValue() : "");//用户参考数据4
			cfetsTradeProgress.setUserReference5(quoteResponse.isSetUserReference5() ?
					quoteResponse.getUserReference5().getValue() : "");//用户参考数据5
			cfetsTradeProgress.setUserReference6(quoteResponse.isSetUserReference6() ?
					quoteResponse.getUserReference6().getValue() : "");//用户参考数据6
			//2、质押式回购
			CfetsTradeBondPledge cfetsTradeBondPledge = new CfetsTradeBondPledge();
//			cfetsTradeBondPledge.setTotalFaceAmt(totalFaceAmt);//券面总额合计
//			cfetsTradeBondPledge.setTradeProduct(tradeProduct);//交易品种
			cfetsTradeBondPledge.setSide(quoteResponse.isSetSide() ? String.valueOf(quoteResponse.getSide().getValue()):"");//交易方向
			cfetsTradeBondPledge.setRepoMethod(quoteResponse.isSetRepoMethod()?String.valueOf(quoteResponse.getRepoMethod().getValue()):"");//回购方式
			cfetsTradeBondPledge.setTradeLimitDays(quoteResponse.isSetTradeLimitDays()?new BigDecimal(quoteResponse.getTradeLimitDays().getValue()):null);//回购期限
			cfetsTradeBondPledge.setPrice(quoteResponse.isSetField(Price.FIELD) ? new BigDecimal(quoteResponse.getString(Price.FIELD)) : null);//回购利率
			cfetsTradeBondPledge.setTradeCashAmt(quoteResponse.isSetField(TradeCashAmt.FIELD)?new BigDecimal(quoteResponse.getString(TradeCashAmt.FIELD)):null);//交易金额
			cfetsTradeBondPledge.setSettType(quoteResponse.isSetField(SettlType.FIELD)?quoteResponse.getString(SettlType.FIELD):"");//清算速度
			cfetsTradeBondPledge.setDeliveryType(quoteResponse.isSetField(DeliveryType.FIELD)?quoteResponse.getString(DeliveryType.FIELD):"");//首次结算方式
			cfetsTradeBondPledge.setDeliveryType2(quoteResponse.isSetField(DeliveryType2.FIELD)?quoteResponse.getString(DeliveryType2.FIELD):"");//到期结算方式
			cfetsTradeBondPledge.setClearingMethod(quoteResponse.isSetField(ClearingMethod.FIELD) ? quoteResponse.getString(ClearingMethod.FIELD) : "");//清算类型
			cfetsTradeBondPledge.setSettlCurrency(quoteResponse.isSetField(SettlCurrency.FIELD) ? quoteResponse.getString(SettlCurrency.FIELD) : "");//结算币种
			cfetsTradeBondPledge.setSettlCurrFxrate(quoteResponse.isSetField(SettlCurrFxRate.FIELD) ? new BigDecimal(quoteResponse.getString(SettlCurrFxRate.FIELD)) : null);//汇率

			if(quoteResponse.isSetNoCrossCustodians()) {
				RecAAnalysisRmbTradeGroup.analysisNoCrossCustodians(retBean, cfetsTradeBondPledge, quoteResponse.getGroups(NoCrossCustodians.FIELD));
			}
			
			//3、质押券
			List<CfetsTradeSubbond> subBondList = new ArrayList<CfetsTradeSubbond>();
			if(quoteResponse.isSetNoUnderlyings()){
				RecAAnalysisRmbTradeGroup.analysisNoUnderlyings(retBean, subBondList, quoteResponse.getGroups(NoUnderlyings.FIELD));
            }
			
			cfetsTradeBondPledge.setSubBondList(subBondList);
			cfetsTradeProgress.setCfetsTradeBondPledge(cfetsTradeBondPledge);
			
			//4、交易机构信息
			List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
            if (quoteResponse.isSetNoPartyIDs()) {
            	RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, quoteResponse);
            }
            cfetsTradeProgress.setPartyList(partyList);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+msgType+"],quoteId=["+quoteId+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteCancel msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";
		String marketIndicator="";//市场类型
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			cfetsTradeProgress.setQuoteType(msg.isSetQuoteType() ? 
					String.valueOf(msg.getQuoteType().getValue()) : "");//报价类别
			cfetsTradeProgress.setQuoteStatus(msg.isSetQuoteStatus() ? 
					String.valueOf(msg.getQuoteStatus().getValue()) : "");//报价状态
			cfetsTradeProgress.setValidUntilTime(msg.isSetValidUntilTime() ? 
					String.valueOf(msg.getValidUntilTime().getValue()) : "");
			
			cfetsTradeProgress.setClOrdId(msg.isSetClOrdID() ?
					msg.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setOrigClOrdId(msg.isSetOrigClOrdID() ?
					msg.getOrigClOrdID().getValue() : "");//原客户参考编号
			cfetsTradeProgress.setQuoteReqId(msg.isSetQuoteReqID() ?
					msg.getQuoteReqID().getValue() : "");//请求报价编号
			cfetsTradeProgress.setQuoteId(msg.isSetQuoteID() ?
					msg.getQuoteID().getValue() : "");
			cfetsTradeProgress.setTransactTime(msg.isSetTransactTime() ?
					msg.getTransactTime().getValue() : "");
			cfetsTradeProgress.setConIndicator(msg.isSetField(ContingencyIndicator.FIELD) ? 
							msg.getString(ContingencyIndicator.FIELD) : "");
			
			if(msg.isSetNoQuoteEntries()) {
				List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
				CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
				Group group = msg.getGroup(1,NoQuoteEntries.FIELD);
				
				cfetsTradeBond.setSide(msg.isSetField(Side.FIELD) ? 
						msg.getString(Side.FIELD) : null);//
				
				marketIndicator = group.isSetField(MarketIndicator.FIELD) ? 
						group.getString(MarketIndicator.FIELD) : "";
				cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
				cfetsTradeBond.setSymbol(group.isSetField(Symbol.FIELD) ? 
						group.getString(Symbol.FIELD) : "-");//债券名称
				cfetsTradeBond.setSecurityId(group.isSetField(SecurityID.FIELD) ? 
						group.getString(SecurityID.FIELD) : "");//债券代码
				
				bondList.add(cfetsTradeBond);
				cfetsTradeProgress.setBondList(bondList);
				
				//4、交易机构信息
				List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
				if(group.isSetField(NoPartyIDs.FIELD)) {
					RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, group.getGroups(NoPartyIDs.FIELD));
				}
				cfetsTradeProgress.setPartyList(partyList);
				
			}
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	public static void analysisCfetsTradeProgress(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,ExecutionReport executionReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType="";//报文类型
		String marketIndicator = "";//市场类型
		try {
			//1、报文解析公用表
			cfetsTradeProgress.setUpordown("DOWN");//上行或下行
			msgType=executionReport.getHeader().isSetField(MsgType.FIELD) ? executionReport.getHeader().getString(MsgType.FIELD) : "";
			marketIndicator = executionReport.isSetMarketIndicator() ? executionReport.getMarketIndicator().getValue() : "";
			
			cfetsTradeProgress.setMsgType(msgType);//报文类型
			cfetsTradeProgress.setMarketindicator(marketIndicator);//市场标识
			//成交通知时回传此字段
			cfetsTradeProgress.setExecId(executionReport.isSetExecID() ?
					executionReport.getExecID().getValue() : "");//成交编号
			cfetsTradeProgress.setTradeMethod(executionReport.isSetTradeMethod() ?
					String.valueOf(executionReport.getTradeMethod().getValue()) : "");//交易方式
			cfetsTradeProgress.setExecType(executionReport.isSetExecType() ?
					executionReport.getExecType().getValue() : "");//成交状态
			cfetsTradeProgress.setTradeDate(executionReport.isSetTradeDate() ?
					executionReport.getTradeDate().getValue() : "");//成交日期yyyymmdd
			cfetsTradeProgress.setTradeTime(executionReport.isSetTradeTime() ?
					executionReport.getTradeTime().getValue() : "");//成交时间
			cfetsTradeProgress.setQuoteId(executionReport.isSetQuoteID() ?
					executionReport.getQuoteID().getValue() : "");//报价编号
			cfetsTradeProgress.setClOrdId(executionReport.isSetClOrdID() ?
					executionReport.getClOrdID().getValue() : "");//客户参考编号
			cfetsTradeProgress.setTransactTime(executionReport.isSetTransactTime() ?
					executionReport.getTransactTime().getValue() : "");//业务发生时间
			cfetsTradeProgress.setConIndicator(executionReport.isSetField(ContingencyIndicator.FIELD) ? 
					executionReport.getString(ContingencyIndicator.FIELD) : "");
			
			cfetsTradeProgress.setUserReference1(executionReport.isSetUserReference1() ?
					executionReport.getUserReference1().getValue() : "");//用户参考数据1
			cfetsTradeProgress.setUserReference2(executionReport.isSetUserReference2() ?
					executionReport.getUserReference2().getValue() : "");//用户参考数据2
			cfetsTradeProgress.setUserReference3(executionReport.isSetUserReference3() ?
					executionReport.getUserReference3().getValue() : "");//用户参考数据3
			cfetsTradeProgress.setUserReference4(executionReport.isSetUserReference4() ?
					executionReport.getUserReference4().getValue() : "");//用户参考数据4
			cfetsTradeProgress.setUserReference5(executionReport.isSetUserReference5() ?
					executionReport.getUserReference5().getValue() : "");//用户参考数据5
			cfetsTradeProgress.setUserReference6(executionReport.isSetUserReference6() ?
					executionReport.getUserReference6().getValue() : "");//用户参考数据6
			
			cfetsTradeProgress.setRetCode(executionReport.isSetApplErrorCode() ?
					executionReport.getApplErrorCode().getValue() : "");//错误代码
			cfetsTradeProgress.setRetMsg(executionReport.isSetApplErrorDesc() ?
					executionReport.getApplErrorDesc().getValue() : "");//错误原因
			
			if("4".equals(marketIndicator)) {
				//现券买卖
				analysisCfetsTradeProgressCbt(retBean,cfetsTradeProgress,executionReport);
			}else if("9".equals(marketIndicator)){
				//质押式回购
				analysisCfetsTradeProgressRepo(retBean,cfetsTradeProgress,executionReport);
			}else {
				retBean.setRetCode(ConvertRmbTrade.retError);
				retBean.setRetMsg("组装对象CfetsTradeProgress失败:市场类型错误["+marketIndicator+"]");
			}
			
			//4、交易机构信息
			List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
            if (executionReport.isSetNoPartyIDs()) {
            	RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, executionReport);
            }
            cfetsTradeProgress.setPartyList(partyList);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}

	public static void analysisCfetsTradeProgressRepo(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,Quote quote) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			
			cfetsTradeProgress.setTradeRecordid(quote.isSetTradeRecordID() ? 
					quote.getTradeRecordID().getValue() : "");//交易记录编号
			cfetsTradeProgress.setQuoteStatusDesc(quote.isSetQuoteStatusDesc() ? 
					quote.getQuoteStatusDesc().getValue() : "");//报价状态说明
			cfetsTradeProgress.setQuoteReqId(quote.isSetQuoteReqID() ?
					quote.getQuoteReqID().getValue() : "");//请求报价编号
			//TODO 未找到对应值
//			cfetsTradeProgress.setPledgeSubmissionCode(quote.isSetpied ?
//					quote.getcode.getValue() : "");//提券编号
			cfetsTradeProgress.setNegotiationCount(quote.isSetNegotiationCount() ?
					String.valueOf(quote.getNegotiationCount().getValue()) : "");//交谈轮次
			cfetsTradeProgress.setText(quote.isSetText() ?
					quote.getText().getValue() : "");//备注
			
			//2、质押式回购
			CfetsTradeBondPledge cfetsTradeBondPledge = new CfetsTradeBondPledge();
//			cfetsTradeBondPledge.setTotalFaceAmt(totalFaceAmt);//券面总额合计
//			cfetsTradeBondPledge.setTradeProduct(tradeProduct);//交易品种
			cfetsTradeBondPledge.setSide(quote.isSetSide() ? String.valueOf(quote.getSide().getValue()):"");//交易方向
			cfetsTradeBondPledge.setRepoMethod(quote.isSetRepoMethod()?String.valueOf(quote.getRepoMethod().getValue()):"");//回购方式
			cfetsTradeBondPledge.setTradeLimitDays(quote.isSetTradeLimitDays()?new BigDecimal(quote.getTradeLimitDays().getValue()):null);//回购期限
			cfetsTradeBondPledge.setPrice(quote.isSetField(Price.FIELD) ? new BigDecimal(quote.getString(Price.FIELD)) : null);//回购利率
			cfetsTradeBondPledge.setTradeCashAmt(quote.isSetField(TradeCashAmt.FIELD)?new BigDecimal(quote.getString(TradeCashAmt.FIELD)):null);//交易金额
			cfetsTradeBondPledge.setSettType(quote.isSetField(SettlType.FIELD)?quote.getString(SettlType.FIELD):"");//清算速度
			cfetsTradeBondPledge.setDeliveryType(quote.isSetField(DeliveryType.FIELD)?quote.getString(DeliveryType.FIELD):"");//首次结算方式
			cfetsTradeBondPledge.setDeliveryType2(quote.isSetField(DeliveryType2.FIELD)?quote.getString(DeliveryType2.FIELD):"");//到期结算方式
			cfetsTradeBondPledge.setClearingMethod(quote.isSetField(ClearingMethod.FIELD) ? quote.getString(ClearingMethod.FIELD) : "");//清算类型
			cfetsTradeBondPledge.setSettlCurrency(quote.isSetField(SettlCurrency.FIELD) ? quote.getString(SettlCurrency.FIELD) : "");//结算币种
			cfetsTradeBondPledge.setSettlCurrFxrate(quote.isSetField(SettlCurrFxRate.FIELD) ? new BigDecimal(quote.getString(SettlCurrFxRate.FIELD)) : null);//汇率

			//TODO 未找到数据
//			cfetsTradeBondPledge.setBenchmarkInterestRate(quote.isSetbenc ? new BigDecimal(quote.getString(SettlCurrFxRate.FIELD)) : null);//基准利率
			cfetsTradeBondPledge.setSpread(quote.isSetField(Spread.FIELD) ? new BigDecimal(quote.getString(Spread.FIELD)) : null);//点差
			
			if(quote.isSetNoCrossCustodians()) {
				RecAAnalysisRmbTradeGroup.analysisNoCrossCustodians(retBean, cfetsTradeBondPledge, quote.getGroups(NoCrossCustodians.FIELD));
			}
			
			//3、质押券
			List<CfetsTradeSubbond> subBondList = new ArrayList<CfetsTradeSubbond>();
			if(quote.isSetNoUnderlyings()) {
				RecAAnalysisRmbTradeGroup.analysisNoUnderlyings(retBean, subBondList, quote.getGroups(NoUnderlyings.FIELD));
			}
			
			cfetsTradeBondPledge.setSubBondList(subBondList);
			cfetsTradeProgress.setCfetsTradeBondPledge(cfetsTradeBondPledge);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	
	public static void analysisCfetsTradeProgressCbt(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,Quote quote) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String senderSubId="";//报价类型
		try {
			senderSubId=quote.getHeader().isSetField(SenderSubID.FIELD) ?
					quote.getHeader().getString(SenderSubID.FIELD) : "";
			
			cfetsTradeProgress.setRoutingType(quote.isSetMarketScope() ? 
					String.valueOf(quote.getMarketScope().getValue()) : "");//市场范围
			cfetsTradeProgress.setAnonymousIndicator(quote.isSetAnonymousIndicator() ? 
					String.valueOf(quote.getAnonymousIndicator().getValue()) : "");//匿名标识
			cfetsTradeProgress.setConIndicator(quote.isSetContingencyIndicator() ? 
					String.valueOf(quote.getContingencyIndicator().getValue()) : "");//应急标识
			cfetsTradeProgress.setChannelType(quote.isSetChannelType() ? 
					String.valueOf(quote.getChannelType().getValue()) : "");//渠道类型
			cfetsTradeProgress.setValidUntilTime(quote.isSetValidUntilTime() ? 
					String.valueOf(quote.getValidUntilTime().getValue()) : "");//报价有效时间
			cfetsTradeProgress.setDateConfirmed(quote.isSetDateConfirmed() ? 
					String.valueOf(quote.getDateConfirmed().getValue()) : "");//场次日期
			
			List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
			
			if("QDM".equalsIgnoreCase(senderSubId) && "117".equals(cfetsTradeProgress.getQuoteType())) {
				//指示性报价-现券买卖
				List<Group> groups = quote.getGroups(NoLegs.FIELD);
				if(groups!=null) {
					for(int i=0;i<groups.size();i++) {
						Group group = groups.get(i);
						CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
						analysisCfetsTradeProgressCbt(retBean,cfetsTradeBond,group);
						
						bondList.add(cfetsTradeBond);
					}
				}
			}else {
				CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
				cfetsTradeBond.setDataSource(quote.isSetDataSourceString() ? 
						quote.getDataSourceString().getValue() : "");//报价来源
				cfetsTradeBond.setSecurityId(quote.isSetSecurityID() ? 
						quote.getSecurityID().getValue() : "");//债券代码
				cfetsTradeBond.setSymbol(quote.isSetSymbol() ? 
						quote.getSymbol().getValue() : "");//债券名称
				cfetsTradeBond.setSide(quote.isSetSide() ? String.valueOf(quote.getSide().getValue()):"");//交易方向
				cfetsTradeBond.setPrice(quote.isSetField(Price.FIELD) ? 
						new BigDecimal(quote.getString(Price.FIELD)) : null);//净价
				cfetsTradeBond.setDirtyPrice(quote.isSetField(DirtyPrice.FIELD) ? 
						new BigDecimal(quote.getString(DirtyPrice.FIELD)) : null);//全价
				cfetsTradeBond.setAccruedInterestAmt(quote.isSetField(AccruedInterestAmt.FIELD) ? 
						new BigDecimal(quote.getString(AccruedInterestAmt.FIELD)) : null);//应计利息
				cfetsTradeBond.setOrderQty(quote.isSetField(OrderQty.FIELD) ? 
						new BigDecimal(quote.getString(OrderQty.FIELD)) : null);//券面总额
				cfetsTradeBond.setTradeCashAmt(quote.isSetField(TradeCashAmt.FIELD) ? 
						new BigDecimal(quote.getString(TradeCashAmt.FIELD)) : null);//交易金额
				cfetsTradeBond.setAccruedInterestTotalamt(quote.isSetField(AccruedInterestTotalAmt.FIELD) ? 
						new BigDecimal(quote.getString(AccruedInterestTotalAmt.FIELD)) : null);//应计利息总额
				cfetsTradeBond.setSettlCurrAmt(quote.isSetField(SettlCurrAmt.FIELD) ? 
						new BigDecimal(quote.getString(SettlCurrAmt.FIELD)) : null);//结算金额
				cfetsTradeBond.setSettlCurrency(quote.isSetSettlCurrency() ? 
						String.valueOf(quote.getSettlCurrency().getValue()):"");//结算币种
				cfetsTradeBond.setSettlCurrFxrate(quote.isSetField(SettlCurrFxRate.FIELD) ? 
						new BigDecimal(quote.getString(SettlCurrFxRate.FIELD)) : null);//汇率
				cfetsTradeBond.setDeliveryType(quote.isSetField(DeliveryType.FIELD) ? 
						quote.getString(DeliveryType.FIELD) : null);//结算方式
				cfetsTradeBond.setClearingMethod(quote.isSetField(ClearingMethod.FIELD) ? 
						quote.getString(ClearingMethod.FIELD) : null);//清算类型
				cfetsTradeBond.setSettType(quote.isSetField(SettlType.FIELD) ? 
						quote.getString(SettlType.FIELD) : null);//清算速度
				cfetsTradeBond.setSettlDate(quote.isSetField(SettlDate.FIELD) ? 
						quote.getString(SettlDate.FIELD) : null);//结算日
				cfetsTradeBond.setPrincipal(quote.isSetField(Principal.FIELD) ? 
						new BigDecimal(quote.getString(Principal.FIELD)) : null);//每百元本金额
				cfetsTradeBond.setTotalPrincipal(quote.isSetField(TotalPrincipal.FIELD) ? 
						new BigDecimal(quote.getString(TotalPrincipal.FIELD)) : null);//本金额
				cfetsTradeBond.setYieldType(quote.isSetField(YieldType.FIELD) ? 
						quote.getString(YieldType.FIELD) : null);//到期收益率
				cfetsTradeBond.setYield(quote.isSetField(Yield.FIELD) ? 
						quote.getDecimal(Yield.FIELD) : null);//到期收益率值
				
				if(quote.isSetNoStipulations()) {
					RecAAnalysisRmbTradeGroup.analysisNoStipulations(retBean, cfetsTradeBond, quote.getGroups(NoStipulations.FIELD));
				}
				bondList.add(cfetsTradeBond);
			}
			
			cfetsTradeProgress.setBondList(bondList);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	
	public static void analysisCfetsTradeProgressRepo(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteStatusReport quoteStatusReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			
			cfetsTradeProgress.setTradeRecordid(quoteStatusReport.isSetTradeRecordID() ? 
					quoteStatusReport.getTradeRecordID().getValue() : "");//交易记录编号
			cfetsTradeProgress.setChannel(quoteStatusReport.isSetChannel() ? 
					quoteStatusReport.getChannel().getValue() : "");//渠道
			
			//2、质押式回购
			CfetsTradeBondPledge cfetsTradeBondPledge = new CfetsTradeBondPledge();
			cfetsTradeBondPledge.setSide(quoteStatusReport.isSetSide() ? String.valueOf(quoteStatusReport.getSide().getValue()):"");//交易方向
			cfetsTradeBondPledge.setRepoMethod(quoteStatusReport.isSetRepoMethod()?String.valueOf(quoteStatusReport.getRepoMethod().getValue()):"");//回购方式
			cfetsTradeBondPledge.setTradeLimitDays(quoteStatusReport.isSetTradeLimitDays()?new BigDecimal(quoteStatusReport.getTradeLimitDays().getValue()):null);//回购期限
			cfetsTradeBondPledge.setPrice(quoteStatusReport.isSetField(Price.FIELD) ? new BigDecimal(quoteStatusReport.getString(Price.FIELD)) : null);//回购利率
			cfetsTradeBondPledge.setTradeCashAmt(quoteStatusReport.isSetField(TradeCashAmt.FIELD)?new BigDecimal(quoteStatusReport.getString(TradeCashAmt.FIELD)):null);//交易金额
			cfetsTradeBondPledge.setSettType(quoteStatusReport.isSetField(SettlType.FIELD)?quoteStatusReport.getString(SettlType.FIELD):"");//清算速度
			cfetsTradeBondPledge.setDeliveryType(quoteStatusReport.isSetField(DeliveryType.FIELD)?quoteStatusReport.getString(DeliveryType.FIELD):"");//首次结算方式
			cfetsTradeBondPledge.setDeliveryType2(quoteStatusReport.isSetField(DeliveryType2.FIELD)?quoteStatusReport.getString(DeliveryType2.FIELD):"");//到期结算方式
			cfetsTradeBondPledge.setClearingMethod(quoteStatusReport.isSetField(ClearingMethod.FIELD) ? quoteStatusReport.getString(ClearingMethod.FIELD) : "");//清算类型
			cfetsTradeBondPledge.setSettlCurrency(quoteStatusReport.isSetField(SettlCurrency.FIELD) ? quoteStatusReport.getString(SettlCurrency.FIELD) : "");//结算币种
			cfetsTradeBondPledge.setSettlCurrFxrate(quoteStatusReport.isSetField(SettlCurrFxRate.FIELD) ? new BigDecimal(quoteStatusReport.getString(SettlCurrFxRate.FIELD)) : null);//汇率

			if(quoteStatusReport.isSetNoCrossCustodians()) {
				RecAAnalysisRmbTradeGroup.analysisNoCrossCustodians(retBean, cfetsTradeBondPledge, quoteStatusReport.getGroups(NoCrossCustodians.FIELD));
			}
			
			//3、质押券
			List<CfetsTradeSubbond> subBondList = new ArrayList<CfetsTradeSubbond>();
			if(quoteStatusReport.isSetNoUnderlyings()){
				RecAAnalysisRmbTradeGroup.analysisNoUnderlyings(retBean, subBondList, quoteStatusReport.getGroups(NoUnderlyings.FIELD));
            }
			
			cfetsTradeBondPledge.setSubBondList(subBondList);
			cfetsTradeProgress.setCfetsTradeBondPledge(cfetsTradeBondPledge);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	
	public static void analysisCfetsTradeProgressCbt(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,QuoteStatusReport quoteStatusReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String senderSubId="";//报价类型
		try {
			senderSubId=quoteStatusReport.getHeader().isSetField(SenderSubID.FIELD) ?
					quoteStatusReport.getHeader().getString(SenderSubID.FIELD) : "";
			
			cfetsTradeProgress.setAnonymousIndicator(quoteStatusReport.isSetField(AnonymousAgencyIndicator.FIELD) ? 
					quoteStatusReport.getString(AnonymousAgencyIndicator.FIELD) : "");//匿名
			cfetsTradeProgress.setQuoteStatusDesc(quoteStatusReport.isSetQuoteStatusDesc() ? 
					quoteStatusReport.getQuoteStatusDesc().getValue() : "");//报价状态说明
			cfetsTradeProgress.setValidUntilTime(quoteStatusReport.isSetValidUntilTime() ? 
					quoteStatusReport.getValidUntilTime().getValue() : "");//报价有效时间
			
			List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
			
			
			if("QDM-ESP".equalsIgnoreCase(senderSubId)) {
				//做市报价-现券买卖
				List<Group> groups = quoteStatusReport.getGroups(NoLegs.FIELD);
				if(groups!=null) {
					for(int i=0;i<groups.size();i++) {
						Group group = groups.get(i);
						CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
						cfetsTradeBond.setMaxFloor(quoteStatusReport.isSetField(MaxFloor.FIELD) ? 
								quoteStatusReport.getDecimal(MaxFloor.FIELD) : null);//最大显示券面总额
						
						analysisCfetsTradeProgressCbt(retBean,cfetsTradeBond,group);
						
						bondList.add(cfetsTradeBond);
					}
				}
			}else {
				CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
				cfetsTradeBond.setMaxFloor(quoteStatusReport.isSetField(MaxFloor.FIELD) ? 
						quoteStatusReport.getDecimal(MaxFloor.FIELD) : null);//最大显示券面总额
				
				cfetsTradeBond.setSecurityId(quoteStatusReport.isSetSecurityID() ? 
						quoteStatusReport.getSecurityID().getValue() : "");//债券代码
				cfetsTradeBond.setSymbol(quoteStatusReport.isSetField(Symbol.FIELD) ? 
						quoteStatusReport.getString(Symbol.FIELD) : "");//债券名称
				cfetsTradeBond.setSide(quoteStatusReport.isSetSide() ? 
						String.valueOf(quoteStatusReport.getSide().getValue()):"");//交易方向
				cfetsTradeBond.setPrice(quoteStatusReport.isSetField(Price.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(Price.FIELD)) : null);//净价
				cfetsTradeBond.setDirtyPrice(quoteStatusReport.isSetField(DirtyPrice.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(DirtyPrice.FIELD)) : null);//全价
				cfetsTradeBond.setAccruedInterestAmt(quoteStatusReport.isSetField(AccruedInterestAmt.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(AccruedInterestAmt.FIELD)) : null);//应计利息
				cfetsTradeBond.setOrderQty(quoteStatusReport.isSetField(OrderQty.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(OrderQty.FIELD)) : null);//券面总额
				cfetsTradeBond.setTradeCashAmt(quoteStatusReport.isSetField(TradeCashAmt.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(TradeCashAmt.FIELD)) : null);//交易金额
				cfetsTradeBond.setAccruedInterestTotalamt(quoteStatusReport.isSetField(AccruedInterestTotalAmt.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(AccruedInterestTotalAmt.FIELD)) : null);//应计利息总额
				cfetsTradeBond.setSettlCurrAmt(quoteStatusReport.isSetField(SettlCurrAmt.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(SettlCurrAmt.FIELD)) : null);//结算金额
				cfetsTradeBond.setSettlCurrency(quoteStatusReport.isSetSettlCurrency() ? 
						String.valueOf(quoteStatusReport.getSettlCurrency().getValue()):"");//结算币种
				cfetsTradeBond.setSettlCurrFxrate(quoteStatusReport.isSetField(SettlCurrFxRate.FIELD) ? 
						new BigDecimal(quoteStatusReport.getString(SettlCurrFxRate.FIELD)) : null);//汇率
				cfetsTradeBond.setDeliveryType(quoteStatusReport.isSetField(DeliveryType.FIELD) ? 
						quoteStatusReport.getString(DeliveryType.FIELD) : null);//结算方式
				cfetsTradeBond.setClearingMethod(quoteStatusReport.isSetField(ClearingMethod.FIELD) ? 
						quoteStatusReport.getString(ClearingMethod.FIELD) : null);//清算类型
				cfetsTradeBond.setSettType(quoteStatusReport.isSetField(SettlType.FIELD) ? 
						quoteStatusReport.getString(SettlType.FIELD) : null);//清算速度
				cfetsTradeBond.setSettlDate(quoteStatusReport.isSetField(SettlDate.FIELD) ? 
						quoteStatusReport.getString(SettlDate.FIELD) : null);//结算日
				cfetsTradeBond.setYieldType(quoteStatusReport.isSetField(YieldType.FIELD) ? 
						quoteStatusReport.getString(YieldType.FIELD) : null);//到期收益率
				cfetsTradeBond.setYield(quoteStatusReport.isSetField(Yield.FIELD) ? 
						quoteStatusReport.getDecimal(Yield.FIELD) : null);//到期收益率值
				
				if(quoteStatusReport.isSetNoStipulations()) {
					RecAAnalysisRmbTradeGroup.analysisNoStipulations(retBean, cfetsTradeBond, quoteStatusReport.getGroups(NoStipulations.FIELD));
				}
				
				bondList.add(cfetsTradeBond);
			}
			cfetsTradeProgress.setBondList(bondList);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}

	public static void analysisCfetsTradeProgressRepo(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,ExecutionReport executionReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String quoteId="";
		String senderSubId="";//报价类型
		String marketIndicator = "";//市场类型
		try {
			senderSubId=executionReport.getHeader().isSetField(SenderSubID.FIELD) ? executionReport.getHeader().getString(SenderSubID.FIELD) : "";
			marketIndicator = cfetsTradeProgress.getMarketindicator();
			quoteId = cfetsTradeProgress.getQuoteId();
			
			cfetsTradeProgress.setTradeRecordid(executionReport.isSetTradeRecordID() ? 
					executionReport.getTradeRecordID().getValue() : "");//交易记录编号
			cfetsTradeProgress.setChannel(executionReport.isSetChannel() ? 
					executionReport.getChannel().getValue() : "");//渠道
			cfetsTradeProgress.setQuoteType(executionReport.isSetQuoteType() ? 
					String.valueOf(executionReport.getQuoteType().getValue()) : "");//报价类别
			
			
			//2、质押式回购
			CfetsTradeBondPledge cfetsTradeBondPledge = new CfetsTradeBondPledge();
			cfetsTradeBondPledge.setSide(executionReport.isSetSide() ? String.valueOf(executionReport.getSide().getValue()):"");//交易方向
			cfetsTradeBondPledge.setRepoMethod(executionReport.isSetRepoMethod()?String.valueOf(executionReport.getRepoMethod().getValue()):"");//回购方式
			cfetsTradeBondPledge.setTradeLimitDays(executionReport.isSetTradeLimitDays()?new BigDecimal(executionReport.getTradeLimitDays().getValue()):null);//回购期限
			cfetsTradeBondPledge.setPrice(executionReport.isSetField(Price.FIELD) ? new BigDecimal(executionReport.getString(Price.FIELD)) : null);//回购利率
			cfetsTradeBondPledge.setSettType(executionReport.isSetField(SettlType.FIELD)?executionReport.getString(SettlType.FIELD):"");//清算速度
			cfetsTradeBondPledge.setDeliveryType(executionReport.isSetField(DeliveryType.FIELD)?executionReport.getString(DeliveryType.FIELD):"");//首次结算方式
			cfetsTradeBondPledge.setDeliveryType2(executionReport.isSetField(DeliveryType2.FIELD)?executionReport.getString(DeliveryType2.FIELD):"");//到期结算方式
			cfetsTradeBondPledge.setClearingMethod(executionReport.isSetField(ClearingMethod.FIELD) ? executionReport.getString(ClearingMethod.FIELD) : "");//清算类型
			cfetsTradeBondPledge.setSettlCurrency(executionReport.isSetField(SettlCurrency.FIELD) ? executionReport.getString(SettlCurrency.FIELD) : "");//结算币种
			cfetsTradeBondPledge.setSettlCurrFxrate(executionReport.isSetField(SettlCurrFxRate.FIELD) ? new BigDecimal(executionReport.getString(SettlCurrFxRate.FIELD)) : null);//汇率

			//特殊
			cfetsTradeBondPledge.setTradeCashAmt(executionReport.isSetField(TradeCashAmt.FIELD)?new BigDecimal(executionReport.getString(TradeCashAmt.FIELD)):null);//交易金额
			if(executionReport.isSetField(GrossTradeAmt.FIELD)) {
				//提券时成交传输的交易金额
				cfetsTradeBondPledge.setTradeCashAmt(new BigDecimal(executionReport.getString(GrossTradeAmt.FIELD)));//交易金额
			}
			if("ODM".equals(senderSubId) && "9".equals(marketIndicator)) {
				//订单传输- 质押式回购-匿名点击
				if(executionReport.isSetOrdType()) {
					cfetsTradeProgress.setQuoteType(String.valueOf(executionReport.getOrdType().getValue()));//订单类型
				}
				if(executionReport.isSetOrderID()) {
					quoteId = executionReport.getOrderID().getValue();
					cfetsTradeProgress.setQuoteId(quoteId);//订单编号
				}
				if(executionReport.isSetOrdStatus()) {
					cfetsTradeProgress.setQuoteStatus(String.valueOf(executionReport.getOrdStatus().getValue()));//订单状态
				}
				if(executionReport.isSetExecAckStatus()) {
					cfetsTradeProgress.setQuoteTransType(String.valueOf(executionReport.getExecAckStatus().getValue()));//操作状态
				}
				if(executionReport.isSetValidUntilTime()) {
					cfetsTradeProgress.setValidUntilTime(executionReport.getValidUntilTime().getValue());//有效时间
				}
				if(executionReport.isSetSecurityID()) {
					cfetsTradeBondPledge.setSecurityId(executionReport.getSecurityID().getValue());//合约名称
				}
				if(executionReport.isSetOrderQty()) {
					cfetsTradeBondPledge.setTradeCashAmt(new BigDecimal(executionReport.getOrderQty().getValue()));//交易金额
				}
				if(executionReport.isSetLeavesQty()) {
					cfetsTradeProgress.setLeavesQty(new BigDecimal(executionReport.getLeavesQty().getValue()));//剩余金额
				}
				if(executionReport.isSetLastPx()) {
					cfetsTradeProgress.setLastPx(new BigDecimal(executionReport.getLastPx().getValue()));//成交利率
				}
				if(executionReport.isSetLastQty()) {
					cfetsTradeProgress.setLastQty(new BigDecimal(executionReport.getLastQty().getValue()));//成交金额
				}
			}
			
			if(executionReport.isSetNoCrossCustodians()) {
				RecAAnalysisRmbTradeGroup.analysisNoCrossCustodians(retBean, cfetsTradeBondPledge, executionReport.getGroups(NoCrossCustodians.FIELD));
			}
			//TODO noFloorDetails没有找到
			
			//3、质押券
			List<CfetsTradeSubbond> subBondList = new ArrayList<CfetsTradeSubbond>();
			if(executionReport.isSetNoUnderlyings()){
				RecAAnalysisRmbTradeGroup.analysisNoUnderlyings(retBean, subBondList, executionReport.getGroups(NoUnderlyings.FIELD));
            }
			
			cfetsTradeBondPledge.setSubBondList(subBondList);
			cfetsTradeProgress.setCfetsTradeBondPledge(cfetsTradeBondPledge);
			
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}
	
	public static void analysisCfetsTradeProgressCbt(RetBean retBean,CfetsTradeProgress cfetsTradeProgress,ExecutionReport executionReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			if(executionReport.isSetOrderID()) {
				cfetsTradeProgress.setQuoteReqId(executionReport.getOrderID().getValue());//订单编号
			}
			if(executionReport.isSetField(OrdType.FIELD)) {
				cfetsTradeProgress.setQuoteType(executionReport.getString(OrdType.FIELD));
			}
			if(executionReport.isSetField(OrdStatus.FIELD)) {
				cfetsTradeProgress.setQuoteStatus(executionReport.getString(OrdStatus.FIELD));
			}
			if(executionReport.isSetField(MatchType.FIELD)) {
				cfetsTradeProgress.setTradeMethod(executionReport.getString(MatchType.FIELD));
			}
			
			
			List<CfetsTradeBond> bondList = new ArrayList<CfetsTradeBond>();
			CfetsTradeBond cfetsTradeBond = new CfetsTradeBond();
			
			cfetsTradeBond.setDataSource(executionReport.isSetField(DataSourceString.FIELD) ? 
					executionReport.getString(DataSourceString.FIELD) : "");
			
			cfetsTradeBond.setSecurityId(executionReport.isSetSecurityID() ? 
					executionReport.getSecurityID().getValue() : "");//债券代码
			cfetsTradeBond.setSide(executionReport.isSetSide() ? 
					String.valueOf(executionReport.getSide().getValue()):"");//交易方向
			cfetsTradeBond.setPrice(executionReport.isSetField(Price.FIELD) ? 
					new BigDecimal(executionReport.getString(Price.FIELD)) : null);//净价
			cfetsTradeBond.setDirtyPrice(executionReport.isSetField(DirtyPrice.FIELD) ? 
					new BigDecimal(executionReport.getString(DirtyPrice.FIELD)) : null);//全价
			cfetsTradeBond.setAccruedInterestAmt(executionReport.isSetField(AccruedInterestAmt.FIELD) ? 
					new BigDecimal(executionReport.getString(AccruedInterestAmt.FIELD)) : null);//应计利息
			cfetsTradeBond.setOrderQty(executionReport.isSetField(OrderQty.FIELD) ? 
					new BigDecimal(executionReport.getString(OrderQty.FIELD)) : null);//券面总额
			cfetsTradeBond.setTradeCashAmt(executionReport.isSetField(TradeCashAmt.FIELD) ? 
					new BigDecimal(executionReport.getString(TradeCashAmt.FIELD)) : null);//交易金额
			if(executionReport.isSetField(GrossTradeAmt.FIELD)) {
				cfetsTradeBond.setTradeCashAmt(executionReport.getDecimal(GrossTradeAmt.FIELD));//交易金额
			}
			
			cfetsTradeBond.setAccruedInterestTotalamt(executionReport.isSetField(AccruedInterestTotalAmt.FIELD) ? 
					new BigDecimal(executionReport.getString(AccruedInterestTotalAmt.FIELD)) : null);//应计利息总额
			cfetsTradeBond.setSettlCurrAmt(executionReport.isSetField(SettlCurrAmt.FIELD) ? 
					new BigDecimal(executionReport.getString(SettlCurrAmt.FIELD)) : null);//结算金额
			cfetsTradeBond.setSettlCurrency(executionReport.isSetSettlCurrency() ? 
					String.valueOf(executionReport.getSettlCurrency().getValue()):"");//结算币种
			cfetsTradeBond.setSettlCurrFxrate(executionReport.isSetField(SettlCurrFxRate.FIELD) ? 
					new BigDecimal(executionReport.getString(SettlCurrFxRate.FIELD)) : null);//汇率
			cfetsTradeBond.setDeliveryType(executionReport.isSetField(DeliveryType.FIELD) ? 
					executionReport.getString(DeliveryType.FIELD) : null);//结算方式
			cfetsTradeBond.setClearingMethod(executionReport.isSetField(ClearingMethod.FIELD) ? 
					executionReport.getString(ClearingMethod.FIELD) : null);//清算类型
			cfetsTradeBond.setSettType(executionReport.isSetField(SettlType.FIELD) ? 
					executionReport.getString(SettlType.FIELD) : null);//清算速度
			cfetsTradeBond.setSettlDate(executionReport.isSetField(SettlDate.FIELD) ? 
					executionReport.getString(SettlDate.FIELD) : null);//结算日
			cfetsTradeBond.setPrincipal(executionReport.isSetField(Principal.FIELD) ? 
					new BigDecimal(executionReport.getString(Principal.FIELD)) : null);//每百元本金额
			cfetsTradeBond.setTotalPrincipal(executionReport.isSetField(TotalPrincipal.FIELD) ? 
					new BigDecimal(executionReport.getString(TotalPrincipal.FIELD)) : null);//本金额
			cfetsTradeBond.setYieldType(executionReport.isSetField(YieldType.FIELD) ? 
					executionReport.getString(YieldType.FIELD) : null);//到期收益率
			cfetsTradeBond.setYield(executionReport.isSetField(Yield.FIELD) ? 
					executionReport.getDecimal(Yield.FIELD) : null);//到期收益率值
			
			if(executionReport.isSetNoStipulations()) {
				RecAAnalysisRmbTradeGroup.analysisNoStipulations(retBean, cfetsTradeBond, executionReport.getGroups(NoStipulations.FIELD));
			}
			
			if(executionReport.isSetExecAckStatus()) {
				cfetsTradeProgress.setQuoteTransType(String.valueOf(executionReport.getExecAckStatus().getValue()));//操作状态
			}
			
			cfetsTradeProgress.setLeavesQty(executionReport.isSetField(LeavesQty.FIELD) ? 
					new BigDecimal(executionReport.getString(LeavesQty.FIELD)) : null);
			cfetsTradeProgress.setLastQty(executionReport.isSetField(LastQty.FIELD) ? 
					new BigDecimal(executionReport.getString(LastQty.FIELD)) : null);//券面总额
			cfetsTradeProgress.setLastPx(executionReport.isSetField(LastPx.FIELD) ? 
					new BigDecimal(executionReport.getString(LastPx.FIELD)) : null);
			
			bondList.add(cfetsTradeBond);
		
			cfetsTradeProgress.setBondList(bondList);
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeProgress失败msgType=["+cfetsTradeProgress.getMsgType()+"],"
					+ "quoteId=["+cfetsTradeProgress.getQuoteId()+"]",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeProgress失败");
		}
	}

	/**
	 * 多条腿调用
	 */
	public static void analysisCfetsTradeProgressCbt(RetBean retBean,CfetsTradeBond cfetsTradeBond,Group group) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			cfetsTradeBond.setSecurityId(group.isSetField(LegSecurityID.FIELD) ? 
					group.getString(LegSecurityID.FIELD) : "");//债券代码
			cfetsTradeBond.setSide(group.isSetField(LegSide.FIELD) ? 
					group.getString(LegSide.FIELD) : "");//交易方向
			cfetsTradeBond.setPrice(group.isSetField(LegPrice.FIELD) ? 
					group.getDecimal(LegPrice.FIELD) : null);//净价
//					cfetsTradeBond.setDirtyPrice(quoteStatusReport.isSetField(DirtyPrice.FIELD) ? 
//							new BigDecimal(quoteStatusReport.getString(DirtyPrice.FIELD)) : null);//全价
			cfetsTradeBond.setAccruedInterestAmt(group.isSetField(LegAccruedInterestAmt.FIELD) ? 
					group.getDecimal(LegAccruedInterestAmt.FIELD) : null);//应计利息
			cfetsTradeBond.setOrderQty(group.isSetField(LegOrderQty.FIELD) ? 
					group.getDecimal(LegOrderQty.FIELD) : null);//券面总额
			cfetsTradeBond.setTradeCashAmt(group.isSetField(LegLastQty.FIELD) ? 
					group.getDecimal(LegLastQty.FIELD) : null);//交易金额
//					cfetsTradeBond.setAccruedInterestTotalamt(quoteStatusReport.isSetField(AccruedInterestTotalAmt.FIELD) ? 
//							new BigDecimal(quoteStatusReport.getString(AccruedInterestTotalAmt.FIELD)) : null);//应计利息总额
//					cfetsTradeBond.setSettlCurrAmt(quoteStatusReport.isSetField(SettlCurrAmt.FIELD) ? 
//							new BigDecimal(quoteStatusReport.getString(SettlCurrAmt.FIELD)) : null);//结算金额
			cfetsTradeBond.setSettlCurrency(group.isSetField(LegSettlCurrFxRate.FIELD) ? 
					group.getString(LegSettlCurrFxRate.FIELD) : "");//结算币种
			cfetsTradeBond.setSettlCurrFxrate(group.isSetField(LegLastQty.FIELD) ? 
					group.getDecimal(LegLastQty.FIELD) : null);//汇率
			cfetsTradeBond.setDeliveryType(group.isSetField(LegDeliveryType.FIELD) ? 
					group.getString(LegDeliveryType.FIELD) : "");//结算方式
			cfetsTradeBond.setClearingMethod(group.isSetField(LegClearingMethod.FIELD) ? 
					group.getString(LegClearingMethod.FIELD) : "");//清算类型
			cfetsTradeBond.setSettType(group.isSetField(LegSettlType.FIELD) ? 
					group.getString(LegSettlType.FIELD) : "");//清算速度
			
			if(group.isSetField(NoLegStipulations.FIELD)) {
				RecAAnalysisRmbTradeGroup.analysisNoLegStipulations(retBean, cfetsTradeBond, group.getGroups(NoLegStipulations.FIELD));
			}
			
		} catch (Exception e) {
			LogManager.error("=========组装对象cfetsTradeBond失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象cfetsTradeBond失败");
		}
	}
}
