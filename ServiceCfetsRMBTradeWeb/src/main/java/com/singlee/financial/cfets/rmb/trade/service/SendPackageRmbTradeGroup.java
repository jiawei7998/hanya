package com.singlee.financial.cfets.rmb.trade.service;


import java.util.List;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeBondPledge;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeSubbond;

import imix.field.ClearingMethod;
import imix.field.DeliveryType;
import imix.field.DeliveryType2;
import imix.field.OrigClOrdID;
import imix.field.Price;
import imix.field.RepoMethod;
import imix.field.SettlType;
import imix.field.Side;
import imix.field.TradeCashAmt;
import imix.field.TradeLimitDays;
import imix.field.UnderlyingQty;
import imix.field.UnderlyingSecurityID;
import imix.field.UnderlyingStipType;
import imix.field.UnderlyingStipValue;
import imix.field.UnderlyingSymbol;
import imix.imix20.CollateralAssignment;
import imix.imix20.NewOrderSingle;
import imix.imix20.Quote;
import imix.imix20.QuoteResponse;
/**
 * 组装报文
 * @author xuqq
 *
 */
public class SendPackageRmbTradeGroup {
	
	public static void setUnderlying(CfetsTradeSubbond cfetsTradeSubbond, Quote.NoUnderlyings noUnderlyings, Quote.NoUnderlyings.NoUnderlyingStips noUnderlyingStips) {
        UnderlyingQty underlyingQty = new UnderlyingQty(String.valueOf(cfetsTradeSubbond.getFaceAmt()));//券面总额
        //重复组
        UnderlyingStipType underlyingStipType = new UnderlyingStipType(UnderlyingStipType.VALUATION_DISCOUNT);
        //折券比例，要传百数
        UnderlyingStipValue underlyingStipValue = new UnderlyingStipValue(String.valueOf(cfetsTradeSubbond.getDiscountValue()));
        noUnderlyingStips.set(underlyingStipType);
        noUnderlyingStips.set(underlyingStipValue);

        noUnderlyings.set(underlyingQty);
        noUnderlyings.addGroup(noUnderlyingStips);
    }
	public static void setUnderlying(CfetsTradeSubbond cfetsTradeSubbond, NewOrderSingle.NoUnderlyings noUnderlyings, NewOrderSingle.NoUnderlyings.NoUnderlyingStips noUnderlyingStips) {
        UnderlyingQty underlyingQty = new UnderlyingQty(String.valueOf(cfetsTradeSubbond.getFaceAmt()));//券面总额
        //重复组
        UnderlyingStipType underlyingStipType = new UnderlyingStipType(UnderlyingStipType.VALUATION_DISCOUNT);
        //折券比例，要传百数
        UnderlyingStipValue underlyingStipValue = new UnderlyingStipValue(String.valueOf(cfetsTradeSubbond.getDiscountValue()));
        noUnderlyingStips.set(underlyingStipType);
        noUnderlyingStips.set(underlyingStipValue);

        noUnderlyings.set(underlyingQty);
        noUnderlyings.addGroup(noUnderlyingStips);
    }
	public static void setUnderlying(CfetsTradeSubbond cfetsTradeSubbond, CollateralAssignment.NoUnderlyings noUnderlyings, CollateralAssignment.NoUnderlyings.NoUnderlyingStips noUnderlyingStips) {
        UnderlyingQty underlyingQty = new UnderlyingQty(String.valueOf(cfetsTradeSubbond.getFaceAmt()));//券面总额
        //重复组
        UnderlyingStipType underlyingStipType = new UnderlyingStipType(UnderlyingStipType.VALUATION_DISCOUNT);
        //折券比例，要传百数
        UnderlyingStipValue underlyingStipValue = new UnderlyingStipValue(String.valueOf(cfetsTradeSubbond.getDiscountValue()));
        noUnderlyingStips.set(underlyingStipType);
        noUnderlyingStips.set(underlyingStipValue);

        noUnderlyings.set(underlyingQty);
        noUnderlyings.addGroup(noUnderlyingStips);
    }
	
	//质押券信息
	public static void getNounderlyings(QuoteResponse quoteResponse, CfetsTradeBondPledge cfetsTradeBondPledge){
        //债券重复组
    	List<CfetsTradeSubbond> subBondList = cfetsTradeBondPledge.getSubBondList();
        Quote.NoUnderlyings noUnderlyings = new Quote.NoUnderlyings();
        for (int i =0;i<subBondList.size();i++) {
            Quote.NoUnderlyings.NoUnderlyingStips noUnderlyingStips = new Quote.NoUnderlyings.NoUnderlyingStips();
            //质押券代码
            UnderlyingSecurityID underlyingSecurityID = new UnderlyingSecurityID(subBondList.get(i).getBondCode());
            //质押券名称
            UnderlyingSymbol underlyingSymbol = new UnderlyingSymbol("-");
            UnderlyingQty underlyingQty = new UnderlyingQty(String.valueOf(subBondList.get(i).getFaceAmt()));
            //重复组
            UnderlyingStipType underlyingStipType = new UnderlyingStipType(UnderlyingStipType.VALUATION_DISCOUNT);
            UnderlyingStipValue underlyingStipValue = new UnderlyingStipValue(String.valueOf(subBondList.get(i).getDiscountValue()));

            noUnderlyingStips.set(underlyingStipType);
            noUnderlyingStips.set(underlyingStipValue);

            noUnderlyings.set(underlyingSecurityID);
            noUnderlyings.set(underlyingSymbol);
            noUnderlyings.set(underlyingQty);
            noUnderlyings.addGroup(noUnderlyingStips);
        }
        quoteResponse.addGroup(noUnderlyings);
    }
	
	//公共交易要素
	public static void getCommonTrade(QuoteResponse quoteResponse, CfetsTradeProgress cfetsTradeProgress){
        if(cfetsTradeProgress.getOrigClOrdId()!=null) {
            OrigClOrdID origClOrdID = new OrigClOrdID(cfetsTradeProgress.getOrigClOrdId());
            quoteResponse.set(origClOrdID);
        }
        CfetsTradeBondPledge cfetsTradeBondPledge = cfetsTradeProgress.getCfetsTradeBondPledge();
        Side side=new Side(cfetsTradeBondPledge.getSide().toCharArray()[0]);
        RepoMethod repoMethod=new RepoMethod(cfetsTradeBondPledge.getRepoMethod().toCharArray()[0]);
        TradeLimitDays tradeLimitDays=new TradeLimitDays(String.valueOf(cfetsTradeBondPledge.getTradeLimitDays()));
        Price price=new Price(String.valueOf(cfetsTradeBondPledge.getPrice()));
        TradeCashAmt tradeCashAmt=new TradeCashAmt(String.valueOf(cfetsTradeBondPledge.getTotalFaceAmt()));
        SettlType settlType=new SettlType(cfetsTradeBondPledge.getSettType());
        DeliveryType deliveryType=new DeliveryType(Integer.valueOf(cfetsTradeBondPledge.getDeliveryType()));
        DeliveryType2 deliveryType2=new DeliveryType2(Integer.valueOf(cfetsTradeBondPledge.getDeliveryType2()));
        ClearingMethod clearingMethod =new ClearingMethod(Integer.valueOf(cfetsTradeBondPledge.getClearingMethod()));
        quoteResponse.set(side);
        quoteResponse.set(repoMethod);
        quoteResponse.set(tradeLimitDays);
        quoteResponse.set(price);
        quoteResponse.set(tradeCashAmt);
        quoteResponse.set(settlType);
        quoteResponse.set(deliveryType);
        quoteResponse.set(deliveryType2);
        quoteResponse.set(clearingMethod);
    }
	
	
	
}
