package com.singlee.financial.cfets.rmb.trade.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMarketData;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.UnsupportedMessageType;
import imix.field.MDReqID;
import imix.field.MarketIndicator;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.SubscriptionRequestType;
import imix.field.Symbol;
import imix.imix20.MarketDataRequest;
/**
 * 组装报文
 * @author xuqq
 *
 */
public class SendPackageCfetsTradeMarketData {
	
	private static Logger LogManager = LoggerFactory.getLogger("Package");
	
	public static void packageMessage(RetBean retBean,MarketDataRequest marketDataRequest,CfetsTradeMarketData cfetsTradeMarketData) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			//V
			marketDataRequest.set(new MDReqID(cfetsTradeMarketData.getMdReqId()));
			marketDataRequest.set(new SubscriptionRequestType(cfetsTradeMarketData.getSubRequestType().toCharArray()[0]));
//			marketDataRequest.set(new RepoMethod(cfetsTradeBondPledge.getRepoMethod().toCharArray()[0]));
            
			MarketDataRequest.NoRelatedSym noRelatedSym = new MarketDataRequest.NoRelatedSym();
			noRelatedSym.set(new Symbol(cfetsTradeMarketData.getSymbol()));
			noRelatedSym.set(new MarketIndicator(cfetsTradeMarketData.getMarketindicator()));
			marketDataRequest.addGroup(noRelatedSym);
			
            //重复组1
            List<CfetsTradeParty> partyList = cfetsTradeMarketData.getPartyList();
            for(int i=0; i<partyList.size();i++) {
            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
            	MarketDataRequest.NoPartyIDs noPartyIDs_s = new MarketDataRequest.NoPartyIDs();
                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                noPartyIDs_s.set(partyID_s);
                noPartyIDs_s.set(partyRole_s);

                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid266())) {
                	MarketDataRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new MarketDataRequest.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid266());
                     PartySubIDType partySubIDType_s = new PartySubIDType(266);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid162())) {
                	MarketDataRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new MarketDataRequest.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid162());
                     PartySubIDType partySubIDType_s = new PartySubIDType(162);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                marketDataRequest.addGroup(noPartyIDs_s);
            } 
		}catch(Exception e){
        	LogManager.info("组装V报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装V报文异常");
            return;
        }
	}
	
	
}
