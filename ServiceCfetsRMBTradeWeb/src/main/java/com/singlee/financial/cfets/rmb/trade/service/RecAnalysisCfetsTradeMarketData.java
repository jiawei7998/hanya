package com.singlee.financial.cfets.rmb.trade.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMarketData;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMdEntry;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.FieldNotFound;
import imix.Group;
import imix.IncorrectTagValue;
import imix.UnsupportedMessageType;
import imix.field.MsgType;
import imix.field.NoMDEntries;
import imix.field.NoPartyIDs;
import imix.field.NoRelatedSym;
import imix.imix20.ListMarketDataAck;
import imix.imix20.MarketDataSnapshotFullRefresh;
import imix.imix20.component.InstrmtMDReqGrp;
import imix.imix20.component.Instrument;
import imix.imix20.component.MDFullGrp;
/**
 * 解析报文头
 * @author xuqq
 *
 */
public class RecAnalysisCfetsTradeMarketData {
	private static Logger LogManager = LoggerFactory.getLogger("RecAnalysis");
	
	public static void analysisCfetsTradeMarketData(RetBean retBean,CfetsTradeMarketData cfetsTradeMarketData,ListMarketDataAck msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType ="";
		try {
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? 
					msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeMarketData.setMsgType(msgType);//报文类型
			cfetsTradeMarketData.setMdBookType(msg.isSetMDBookType() ? 
					String.valueOf(msg.getMDBookType().getValue()) : "");//行情类型
			cfetsTradeMarketData.setMdSubBooktype(msg.isSetMDSubBookType() ? 
					String.valueOf(msg.getMDSubBookType().getValue()) : "");//行情类型
			cfetsTradeMarketData.setMdReqId(msg.isSetMDReqID() ? 
					msg.getMDReqID().getValue() : "");//订阅请求ID
			cfetsTradeMarketData.setSubRequestType(msg.isSetSubscriptionRequestType() ? 
					String.valueOf(msg.getSubscriptionRequestType().getValue()) : "");//订阅标识
			cfetsTradeMarketData.setSubStatus(msg.isSetSubscriptionStatus() ? 
					String.valueOf(msg.getSubscriptionStatus().getValue()) : "");//订阅状态
			
			cfetsTradeMarketData.setApplErrorCode(msg.isSetApplErrorCode() ? 
					msg.getApplErrorCode().getValue() : "");//错误代码
			cfetsTradeMarketData.setApplErrorDesc(msg.isSetApplErrorDesc() ? 
					msg.getApplErrorDesc().getValue() : "");//错误原因
			
			if(msg.isSetNoRelatedSym()) {
				//质押式回购匿名点击
				List<Group> groups = msg.getGroups(NoRelatedSym.FIELD);
				if(groups!=null && groups.size()>0) {
					ListMarketDataAck.NoRelatedSym group = (ListMarketDataAck.NoRelatedSym)groups.get(0);
					cfetsTradeMarketData.setSymbol(group.isSetSymbol()? 
							group.getSymbol().getValue() : "-");//组件必须域
					cfetsTradeMarketData.setMarketindicator(group.isSetMarketIndicator()? 
							group.getMarketIndicator().getValue() : "");//市场标识
					cfetsTradeMarketData.setSecurityId(group.isSetSecurityID()? 
							group.getSecurityID().getValue() : "");
				}
			}
			InstrmtMDReqGrp instrmtMDReqGrp = msg.getInstrmtMDReqGrp();
			if(instrmtMDReqGrp!=null) {
				//现券买卖-做市报价
				if(instrmtMDReqGrp.isSetNoRelatedSym()) {
					InstrmtMDReqGrp.NoRelatedSym noRelatedSym = new InstrmtMDReqGrp.NoRelatedSym();
					instrmtMDReqGrp.getGroup(1,noRelatedSym);
					Instrument instrument = noRelatedSym.getInstrument();
					if(instrument!=null) {
						cfetsTradeMarketData.setSymbol(instrument.isSetSymbol() ? 
								String.valueOf(instrument.getSymbol().getValue()) : "-");//组件必须域
						cfetsTradeMarketData.setMarketindicator(instrument.isSetMarketIndicator() ? 
								String.valueOf(instrument.getMarketIndicator().getValue()) : "");//市场类型
						cfetsTradeMarketData.setSecurityType(instrument.isSetSecurityType() ? 
								String.valueOf(instrument.getSecurityType().getValue()) : "");//债券类型 
					}
				}
			}

		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeMarketData失败:",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeMarketData失败");
		}
	}
	
	public static void analysisCfetsTradeMarketData(RetBean retBean,CfetsTradeMarketData cfetsTradeMarketData,MarketDataSnapshotFullRefresh msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType ="";
		String marketIndicator ="";
		try {
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? 
					msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeMarketData.setMsgType(msgType);//报文类型
			marketIndicator = msg.isSetMarketIndicator() ? 
					msg.getMarketIndicator().getValue() : "";
			cfetsTradeMarketData.setMarketindicator(marketIndicator);//市场标识
			cfetsTradeMarketData.setMdBookType(msg.isSetMDBookType() ? 
					String.valueOf(msg.getMDBookType().getValue()) : "");//行情类型
			cfetsTradeMarketData.setTransactTime(msg.isSetTransactTime() ?
					msg.getTransactTime().getValue() : "");//业务发生时间
			//TODO  回购方式 没有找到
//			cfetsTradeMarketData.setRepoMethod(msg.isSetSettlMethod() ?
//					msg.getTransactTime().getValue() : "");//回购方式
			cfetsTradeMarketData.setMarketDepth(msg.isSetMarketDepth() ?
					new BigDecimal(msg.getMarketDepth().getValue()) : null);//挡位信息
			
			List<CfetsTradeMdEntry> mdEntryList = new ArrayList<CfetsTradeMdEntry>();
			MDFullGrp mDFullGrp = msg.getMDFullGrp();
			if(mDFullGrp!=null) {
				if(mDFullGrp.isSetNoMDEntries()) {
					RecAAnalysisRmbTradeGroup.analysisNoMDEntries(retBean, mdEntryList, mDFullGrp.getGroups(NoMDEntries.FIELD),marketIndicator);
				}
				
			}
			cfetsTradeMarketData.setMdEntryList(mdEntryList);
			
			if("4".equals(marketIndicator)) {
				cfetsTradeMarketData.setMdSubBooktype(msg.isSetMDSubBookType() ? 
						String.valueOf(msg.getMDSubBookType().getValue()) : "");//行情类型
				cfetsTradeMarketData.setMdReqId(msg.isSetMDReqID() ? 
						msg.getMDReqID().getValue() : "");//订阅请求ID
				cfetsTradeMarketData.setSymbol(msg.isSetSymbol() ? 
						String.valueOf(msg.getSymbol().getValue()) : "");//组件必须域
				cfetsTradeMarketData.setSecurityType(msg.isSetSecurityType() ? 
						String.valueOf(msg.getSecurityType().getValue()) : "");//债券类型
				cfetsTradeMarketData.setSecurityId(msg.isSetSecurityID() ?
						msg.getSecurityID().getValue() : "");//合约名称
				
			}else if("9".equals(marketIndicator)) {
				Instrument instrument = msg.getInstrument();
				if(instrument!=null) {
					cfetsTradeMarketData.setSecurityId(instrument.isSetSecurityID() ?
							instrument.getSecurityID().getValue() : "");//合约名称
				}
				
				List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
      			if(mDFullGrp.isSetField(NoPartyIDs.FIELD)) {
      	            RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, mDFullGrp.getGroups(NoPartyIDs.FIELD));
      			}
      			cfetsTradeMarketData.setPartyList(partyList);
			}

		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeMarketData失败:",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeMarketData失败");
		}
	}
	
}
