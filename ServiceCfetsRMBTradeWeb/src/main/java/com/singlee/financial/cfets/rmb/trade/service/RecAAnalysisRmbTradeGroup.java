package com.singlee.financial.cfets.rmb.trade.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.cbt.CfetsTradeBond;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeAllocs;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMdEntry;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeBondPledge;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeSubbond;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.FieldNotFound;
import imix.Group;
import imix.IncorrectTagValue;
import imix.UnsupportedMessageType;
import imix.field.AllocAccount;
import imix.field.AllocQty;
import imix.field.ClearingMethod;
import imix.field.CrossCustodianInstitutionName;
import imix.field.CrossCustodianTradeAmt;
import imix.field.DeliveryType;
import imix.field.IndividualAllocID;
import imix.field.LastPx;
import imix.field.LegStipulationType;
import imix.field.LegStipulationValue;
import imix.field.MDEntryDate;
import imix.field.MDEntryID;
import imix.field.MDEntryPx;
import imix.field.MDEntrySize;
import imix.field.MDEntryTime;
import imix.field.MDEntryType;
import imix.field.MDPriceLevel;
import imix.field.MDQuoteType;
import imix.field.NoPartyIDs;
import imix.field.NoUnderlyingStips;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.SettlCurrFxRate;
import imix.field.SettlCurrency;
import imix.field.SettlDate;
import imix.field.SettlType;
import imix.field.StipulationType;
import imix.field.StipulationValue;
import imix.field.TradeVolume;
import imix.field.UnMatchQty;
import imix.field.UnderlyingQty;
import imix.field.UnderlyingSecurityID;
import imix.field.UnderlyingStipType;
import imix.field.UnderlyingStipValue;
import imix.field.UnderlyingSymbol;
import imix.field.Yield;
import imix.field.YieldType;
import imix.imix20.ExecutionReport;
import imix.imix20.Quote;
import imix.imix20.QuoteResponse;
import imix.imix20.QuoteStatusReport;

/**
 * 解析公用组件
 * @author yanfa232
 *
 */
public class RecAAnalysisRmbTradeGroup {
	
	private static Logger LogManager = LoggerFactory.getLogger("RecAnalysis");

	/**
	 * 解析：NoMDEntries
	 */
	public static void analysisNoMDEntries(RetBean retBean,List<CfetsTradeMdEntry> mdEntryList,List<Group> groups,String marketIndicator) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			for (int i=1;i<=groups.size();i++) {
                Group noMDEntries = groups.get(i);
                
                CfetsTradeMdEntry cfetsTradeMdEntry = new CfetsTradeMdEntry();
                cfetsTradeMdEntry.setMdEntryType(noMDEntries.isSetField(MDEntryType.FIELD)?
                		noMDEntries.getString(MDEntryType.FIELD):"");//方向
                cfetsTradeMdEntry.setMdPriceLevel(noMDEntries.isSetField(MDPriceLevel.FIELD)?
                		noMDEntries.getDecimal(MDPriceLevel.FIELD):null);//挡位
                cfetsTradeMdEntry.setMdEntryPx(noMDEntries.isSetField(MDEntryPx.FIELD)?
                		noMDEntries.getDecimal(MDEntryPx.FIELD):null);//利率
                cfetsTradeMdEntry.setMdEntrySize(noMDEntries.isSetField(MDEntrySize.FIELD)?
                		noMDEntries.getDecimal(MDEntrySize.FIELD):null);//可成交量
                cfetsTradeMdEntry.setTradeVolume(noMDEntries.isSetField(TradeVolume.FIELD)?
                		noMDEntries.getDecimal(TradeVolume.FIELD):null);//总量
                
               if("4".equals(marketIndicator)) {
            	   cfetsTradeMdEntry.setClearingMethod(noMDEntries.isSetField(ClearingMethod.FIELD)?
                		noMDEntries.getString(ClearingMethod.FIELD):"");//清算类型
                  cfetsTradeMdEntry.setSettType(noMDEntries.isSetField(SettlType.FIELD)?
                    		noMDEntries.getString(SettlType.FIELD):"");//清算速度
                  cfetsTradeMdEntry.setLastPx(noMDEntries.isSetField(LastPx.FIELD)?
                  		noMDEntries.getDecimal(LastPx.FIELD):null);//净价
                  cfetsTradeMdEntry.setYieldType(noMDEntries.isSetField(YieldType.FIELD)?
                  		noMDEntries.getString(YieldType.FIELD):"");//到期收益率
                  cfetsTradeMdEntry.setYield(noMDEntries.isSetField(Yield.FIELD)?
                    		noMDEntries.getDecimal(Yield.FIELD):null);//到期收益率值
                  cfetsTradeMdEntry.setMdEntryDate(noMDEntries.isSetField(MDEntryDate.FIELD)?
                    		noMDEntries.getString(MDEntryDate.FIELD):"");//业务发生日期
                  cfetsTradeMdEntry.setMdEntryTime(noMDEntries.isSetField(MDEntryTime.FIELD)?
                    		noMDEntries.getString(MDEntryTime.FIELD):"");//业务发生时间
                  cfetsTradeMdEntry.setQuoteType(noMDEntries.isSetField(MDQuoteType.FIELD)?
                  		noMDEntries.getString(MDQuoteType.FIELD):"");//报价方式
                  cfetsTradeMdEntry.setQuoteId(noMDEntries.isSetField(MDEntryID.FIELD)?
                    		noMDEntries.getString(MDEntryID.FIELD):"");//报价编号
                  cfetsTradeMdEntry.setSettlDate(noMDEntries.isSetField(SettlDate.FIELD)?
                    		noMDEntries.getString(SettlDate.FIELD):"");//结算日
                  cfetsTradeMdEntry.setDeliveryType(noMDEntries.isSetField(DeliveryType.FIELD)?
                  		noMDEntries.getString(DeliveryType.FIELD):"");//结算方式
                  cfetsTradeMdEntry.setSettlCurrency(noMDEntries.isSetField(SettlCurrency.FIELD)?
                    		noMDEntries.getString(SettlCurrency.FIELD):"");//结算币种
                  cfetsTradeMdEntry.setSettlCurrFxrate(noMDEntries.isSetField(SettlCurrFxRate.FIELD)?
                  		noMDEntries.getDecimal(SettlCurrFxRate.FIELD):null);//汇率
              
                  cfetsTradeMdEntry.setUnMatchQty(noMDEntries.isSetField(UnMatchQty.FIELD)?
                  		noMDEntries.getDecimal(UnMatchQty.FIELD):null);//卖出未匹配量
                  
                	//TODO 没找到
	      			List<CfetsTradeParty> partyList = new ArrayList<CfetsTradeParty>();
	      			if(noMDEntries.isSetField(NoPartyIDs.FIELD)) {
	      	            RecAAnalysisRmbTradeGroup.analysisNoPartyIDs(retBean, partyList, noMDEntries.getGroups(NoPartyIDs.FIELD));
	      			}
	      			cfetsTradeMdEntry.setPartyList(partyList);
                }
                mdEntryList.add(cfetsTradeMdEntry);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoUnderlyings失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoUnderlyings失败");
		}
	}
	
	/**
	 * 解析：NoUnderlyings
	 */
	public static void analysisNoUnderlyings(RetBean retBean,List<CfetsTradeSubbond> subBondList,List<Group> groups) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			for (int i=1;i<=groups.size();i++) {
                Group noUnderlyings = groups.get(i);
                CfetsTradeSubbond cfetsTradeSubbond = new CfetsTradeSubbond();
                
                cfetsTradeSubbond.setBondCode(noUnderlyings.isSetField(UnderlyingSecurityID.FIELD)?noUnderlyings.getString(UnderlyingSecurityID.FIELD):null);
                cfetsTradeSubbond.setBondName(noUnderlyings.isSetField(UnderlyingSymbol.FIELD)?noUnderlyings.getString(UnderlyingSymbol.FIELD):null);
                cfetsTradeSubbond.setFaceAmt(noUnderlyings.isSetField(UnderlyingQty.FIELD)?new BigDecimal(noUnderlyings.getString(UnderlyingQty.FIELD)):null);

                Group noUnderlyingStips = noUnderlyings.getGroup(1, NoUnderlyingStips.FIELD);
                cfetsTradeSubbond.setDiscountType(noUnderlyingStips.isSetField(UnderlyingStipType.FIELD)?noUnderlyingStips.getString(UnderlyingStipType.FIELD):"");
                cfetsTradeSubbond.setDiscountValue(noUnderlyingStips.isSetField(UnderlyingStipValue.FIELD)?new BigDecimal(noUnderlyingStips.getString(UnderlyingStipValue.FIELD)):null);
                
                subBondList.add(cfetsTradeSubbond);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoUnderlyings失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoUnderlyings失败");
		}
	}
	
	/**
	 * 解析：NoCrossCustodians
	 */
	public static void analysisNoCrossCustodians(RetBean retBean,CfetsTradeBondPledge cfetsTradeBondPledge,List<Group> groups) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			for(int i=0;i<groups.size();i++) {
				Group noCrossCustodian = groups.get(i);
				if(i==0) {
					cfetsTradeBondPledge.setCrossCustInstname1(noCrossCustodian.isSetField(CrossCustodianInstitutionName.FIELD)?
							noCrossCustodian.getString(CrossCustodianInstitutionName.FIELD):"");
					cfetsTradeBondPledge.setCrossCustAmt1(noCrossCustodian.isSetField(CrossCustodianTradeAmt.FIELD)?
							noCrossCustodian.getDecimal(CrossCustodianTradeAmt.FIELD):null);
				}else if(i==1) {
					cfetsTradeBondPledge.setCrossCustInstname2(noCrossCustodian.isSetField(CrossCustodianInstitutionName.FIELD)?
							noCrossCustodian.getString(CrossCustodianInstitutionName.FIELD):"");
					cfetsTradeBondPledge.setCrossCustAmt2(noCrossCustodian.isSetField(CrossCustodianTradeAmt.FIELD)?
							noCrossCustodian.getDecimal(CrossCustodianTradeAmt.FIELD):null);
				}
			}
		} catch (Exception e) {
			LogManager.error("=========解析对象NoCrossCustodians失败",e);
//			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoCrossCustodians失败");
		}
	}
	
	
	
	
	/**
	 * 解析：NoPartyIDs
	 */
	public static void analysisNoPartyIDs(RetBean retBean,List<CfetsTradeParty> partyList,Quote quote) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			List<Group> groups = quote.getGroups(NoPartyIDs.FIELD);
            for (int i = 1; i <= groups.size(); i++) {
            	CfetsTradeParty cfetsTradeParty = new CfetsTradeParty();//对象
            	Quote.NoPartyIDs noPartyIDs = (Quote.NoPartyIDs)groups.get(i);
                PartyID partyID = noPartyIDs.getPartyID();
                PartyRole partyRole = noPartyIDs.getPartyRole();
                cfetsTradeParty.setPartyId(partyID.getValue());
                cfetsTradeParty.setPartyRole(String.valueOf(partyRole.getValue()));
                Quote.NoPartyIDs.NoPartySubIDs noPartySubIDs = new Quote.NoPartyIDs.NoPartySubIDs();
                if (noPartyIDs.isSetNoPartySubIDs()) {
                    for (int j = 1; j <= noPartyIDs.getNoPartySubIDs().getValue(); j++) {
                        noPartyIDs.getGroup(j, noPartySubIDs);
                        PartySubID partySubID = noPartySubIDs.getPartySubID();
                        PartySubIDType partySubIDType = noPartySubIDs.getPartySubIDType();
                        if (partySubIDType.valueEquals(PartySubIDType.PERSON)) {
                        	cfetsTradeParty.setPartySubid2(partySubID.getValue());
                        }else if(partySubIDType.valueEquals(PartySubIDType.TRADE__ACCOUNT__ID)){
                        	cfetsTradeParty.setPartySubid266(partySubID.getValue());
                        }
                    }
                }
                partyList.add(cfetsTradeParty);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoPartyIDs失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoPartyIDs失败");
		}
	}
	
	/**
	 * 解析：NoPartyIDs
	 */
	public static void analysisNoPartyIDs(RetBean retBean,List<CfetsTradeParty> partyList,QuoteStatusReport quoteStatusReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			List<Group> groups = quoteStatusReport.getGroups(NoPartyIDs.FIELD);
            for (int i = 1; i <= groups.size(); i++) {
            	CfetsTradeParty cfetsTradeParty = new CfetsTradeParty();//对象
            	Quote.NoPartyIDs noPartyIDs = (Quote.NoPartyIDs)groups.get(i);
                PartyID partyID = noPartyIDs.getPartyID();
                PartyRole partyRole = noPartyIDs.getPartyRole();
                cfetsTradeParty.setPartyId(partyID.getValue());
                cfetsTradeParty.setPartyRole(String.valueOf(partyRole.getValue()));
                QuoteStatusReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new QuoteStatusReport.NoPartyIDs.NoPartySubIDs();
                if (noPartyIDs.isSetNoPartySubIDs()) {
                    for (int j = 1; j <= noPartyIDs.getNoPartySubIDs().getValue(); j++) {
                        noPartyIDs.getGroup(j, noPartySubIDs);
                        PartySubID partySubID = noPartySubIDs.getPartySubID();
                        PartySubIDType partySubIDType = noPartySubIDs.getPartySubIDType();
                        if (partySubIDType.valueEquals(PartySubIDType.PERSON)) {
                        	cfetsTradeParty.setPartySubid2(partySubID.getValue());
                        }else if(partySubIDType.valueEquals(PartySubIDType.TRADE__ACCOUNT__ID)){
                        	cfetsTradeParty.setPartySubid266(partySubID.getValue());
                        }
                    }
                }
                partyList.add(cfetsTradeParty);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoPartyIDs失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoPartyIDs失败");
		}
	}
	
	/**
	 * 解析：NoPartyIDs
	 */
	public static void analysisNoPartyIDs(RetBean retBean,List<CfetsTradeParty> partyList,QuoteResponse quoteResponse) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			List<Group> groups = quoteResponse.getGroups(NoPartyIDs.FIELD);
            for (int i = 1; i <= groups.size(); i++) {
            	CfetsTradeParty cfetsTradeParty = new CfetsTradeParty();//对象
            	Quote.NoPartyIDs noPartyIDs = (Quote.NoPartyIDs)groups.get(i);
                PartyID partyID = noPartyIDs.getPartyID();
                PartyRole partyRole = noPartyIDs.getPartyRole();
                cfetsTradeParty.setPartyId(partyID.getValue());
                cfetsTradeParty.setPartyRole(String.valueOf(partyRole.getValue()));
                QuoteResponse.NoPartyIDs.NoPartySubIDs noPartySubIDs = new QuoteResponse.NoPartyIDs.NoPartySubIDs();
                if (noPartyIDs.isSetNoPartySubIDs()) {
                    for (int j = 1; j <= noPartyIDs.getNoPartySubIDs().getValue(); j++) {
                        noPartyIDs.getGroup(j, noPartySubIDs);
                        PartySubID partySubID = noPartySubIDs.getPartySubID();
                        PartySubIDType partySubIDType = noPartySubIDs.getPartySubIDType();
                        if (partySubIDType.valueEquals(PartySubIDType.PERSON)) {
                        	cfetsTradeParty.setPartySubid2(partySubID.getValue());
                        }else if(partySubIDType.valueEquals(PartySubIDType.TRADE__ACCOUNT__ID)){
                        	cfetsTradeParty.setPartySubid266(partySubID.getValue());
                        }
                    }
                }
                partyList.add(cfetsTradeParty);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoPartyIDs失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoPartyIDs失败");
		}
	}
	
	/**
	 * 解析：NoPartyIDs
	 */
	public static void analysisNoPartyIDs(RetBean retBean,List<CfetsTradeParty> partyList,ExecutionReport executionReport) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			List<Group> groups = executionReport.getGroups(NoPartyIDs.FIELD);
            for (int i = 1; i <= groups.size(); i++) {
            	CfetsTradeParty cfetsTradeParty = new CfetsTradeParty();//对象
            	Quote.NoPartyIDs noPartyIDs = (Quote.NoPartyIDs)groups.get(i);
                PartyID partyID = noPartyIDs.getPartyID();
                PartyRole partyRole = noPartyIDs.getPartyRole();
                cfetsTradeParty.setPartyId(partyID.getValue());
                cfetsTradeParty.setPartyRole(String.valueOf(partyRole.getValue()));
                ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
                if (noPartyIDs.isSetNoPartySubIDs()) {
                    for (int j = 1; j <= noPartyIDs.getNoPartySubIDs().getValue(); j++) {
                        noPartyIDs.getGroup(j, noPartySubIDs);
                        PartySubID partySubID = noPartySubIDs.getPartySubID();
                        PartySubIDType partySubIDType = noPartySubIDs.getPartySubIDType();
                        if (partySubIDType.valueEquals(PartySubIDType.PERSON)) {
                        	cfetsTradeParty.setPartySubid2(partySubID.getValue());
                        }else if(partySubIDType.valueEquals(PartySubIDType.TRADE__ACCOUNT__ID)){
                        	cfetsTradeParty.setPartySubid266(partySubID.getValue());
                        }
                    }
                }
                partyList.add(cfetsTradeParty);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoPartyIDs失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoPartyIDs失败");
		}
	}
	/**
	 * 解析：NoPartyIDs
	 */
	public static void analysisNoPartyIDs(RetBean retBean,List<CfetsTradeParty> partyList,List<Group> groups) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
            for (int i = 1; i <= groups.size(); i++) {
            	CfetsTradeParty cfetsTradeParty = new CfetsTradeParty();//对象
            	Quote.NoPartyIDs noPartyIDs = (Quote.NoPartyIDs)groups.get(i);
                PartyID partyID = noPartyIDs.getPartyID();
                PartyRole partyRole = noPartyIDs.getPartyRole();
                cfetsTradeParty.setPartyId(partyID.getValue());
                cfetsTradeParty.setPartyRole(String.valueOf(partyRole.getValue()));
                ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
                if (noPartyIDs.isSetNoPartySubIDs()) {
                    for (int j = 1; j <= noPartyIDs.getNoPartySubIDs().getValue(); j++) {
                        noPartyIDs.getGroup(j, noPartySubIDs);
                        PartySubID partySubID = noPartySubIDs.getPartySubID();
                        PartySubIDType partySubIDType = noPartySubIDs.getPartySubIDType();
                        if (partySubIDType.valueEquals(PartySubIDType.PERSON)) {
                        	cfetsTradeParty.setPartySubid2(partySubID.getValue());
                        }else if(partySubIDType.valueEquals(PartySubIDType.TRADE__ACCOUNT__ID)){
                        	cfetsTradeParty.setPartySubid266(partySubID.getValue());
                        }
                    }
                }
                partyList.add(cfetsTradeParty);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoPartyIDs失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoPartyIDs失败");
		}
	}
	/**
	 * 解析 NoStipulations
	 * 行权收益率
	 */
	public static void analysisNoStipulations(RetBean retBean,CfetsTradeBond cfetsTradeBond,List<Group> groups) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			for (int i=1;i<=groups.size();i++) {
                Group group = groups.get(i);
                cfetsTradeBond.setStipulationType(group.isSetField(StipulationType.FIELD)?
                		group.getString(StipulationType.FIELD):"");//行权收益率
    			cfetsTradeBond.setStipulationValue(group.isSetField(StipulationValue.FIELD)?
                		group.getDecimal(StipulationValue.FIELD):null);//行权收益率值
                
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoStipulations失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoStipulations失败");
		}
	}
	/**
	 * 解析 NoLegStipulations
	 * 行权收益率
	 */
	public static void analysisNoLegStipulations(RetBean retBean,CfetsTradeBond cfetsTradeBond,List<Group> groups) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			for (int i=1;i<=groups.size();i++) {
                Group group = groups.get(i);
                String type = group.isSetField(LegStipulationType.FIELD) ? 
                		group.getString(LegStipulationType.FIELD) : "";
                if("YTM".equalsIgnoreCase(type)) {
                	cfetsTradeBond.setYieldType(group.isSetField(LegStipulationType.FIELD) ? 
                    		group.getString(LegStipulationType.FIELD) : null);//到期收益率
        			cfetsTradeBond.setYield(group.isSetField(LegStipulationValue.FIELD) ? 
        					group.getDecimal(LegStipulationValue.FIELD) : null);//到期收益率值
                }else if("STRIKEYEILD".equalsIgnoreCase(type)) {
                	cfetsTradeBond.setStipulationType(group.isSetField(LegStipulationType.FIELD)?
                    		group.getString(LegStipulationType.FIELD):"");//行权收益率
        			cfetsTradeBond.setStipulationValue(group.isSetField(LegStipulationValue.FIELD)?
                    		group.getDecimal(LegStipulationValue.FIELD):null);//行权收益率值
                }
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoLegStipulations失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoLegStipulations失败");
		}
	}
	
	/**
	 * 解析 NoAllocs
	 * 分账数据
	 */
	public static void analysisNoAllocs(RetBean retBean,List<CfetsTradeAllocs> allocsList,List<Group> groups) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			for (int i=1;i<=groups.size();i++) {
				CfetsTradeAllocs cfetsTradeAllocs = new CfetsTradeAllocs();
                Group group = groups.get(i);
                cfetsTradeAllocs.setAllocAccount(group.isSetField(AllocAccount.FIELD)?
                		group.getString(AllocAccount.FIELD):"");
                cfetsTradeAllocs.setIndividualAllocid(group.isSetField(IndividualAllocID.FIELD)?
                		group.getString(IndividualAllocID.FIELD):"");
                cfetsTradeAllocs.setAllocQty(group.isSetField(AllocQty.FIELD)?
                		group.getDecimal(AllocQty.FIELD):null);
                
                allocsList.add(cfetsTradeAllocs);
            }
		} catch (Exception e) {
			LogManager.error("=========解析对象NoStipulations失败",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("解析对象NoStipulations失败");
		}
	}
}
