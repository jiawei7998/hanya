package com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.impl;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.rmb.trade.entity.rdi.CfetsRdiTradeMemberContact;
import com.singlee.financial.cfets.rmb.trade.rmbTrade.ImixRmbTradeCfetsApiService;
import com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.RdiZipFileEntryHandle;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

@Service
public class CfetsRdiTradeMemberContactHandle extends AbstractRdiZipFileEntryHandle<CfetsRdiTradeMemberContact> implements RdiZipFileEntryHandle<CfetsRdiTradeMemberContact> {

	private Logger logger = LoggerFactory.getLogger(CfetsRdiTradeMemberContactHandle.class);
	
//    @Autowired
//    private CfetsRdiTradeMemberContactService cfetsRdiTradeMemberContactService;
	@Autowired
	ImixRmbTradeCfetsApiService imixCPISCfetsApiService;

    @Override
    public String getRdiZipFileEntryPathTemplate() {
        return RdiZipFileEntryHandle.TemplateMemberContactInterBank;
    }

    @Override
    public List<CfetsRdiTradeMemberContact> rdiZipFileEntryParse(String targetDate, InputStream is, String targetFilePath) {
        List<CfetsRdiTradeMemberContact> list = null;
        final int stepId = 2;
        try {

            list = dom4j(is);
            createCfetsRdiLog(targetDate,targetFilePath,"解析文件流"+ targetFilePath, stepId, null );
        } catch (Throwable e) {
            createCfetsRdiLog(targetDate,targetFilePath,"解析文件流"+ targetFilePath, stepId, e);
            logger.error("rdiZipFileEntryParse",e);
        }
        return list;
    }

    @Override
    public void rdiZipFileEntryMergeDb(String targetDate, List<CfetsRdiTradeMemberContact> list, String targetFilePath) {
        final int stepId = 3;
        try {
        	RetBean retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRdiCfetsRdiTradeMemberContact(list);
        	if(ConvertRmbTrade.retSeccess.equalsIgnoreCase(retBean.getRetCode())) {
            	logger.info("sendRdiCfetsRdiTradeMemberContact执行完成");
            }else {
            	logger.info("sendRdiCfetsRdiTradeMemberContact执行失败："+retBean.getRetMsg());
            }
        	//            cfetsRdiTradeMemberContactService.saveCfetsRdiTradeMemberContact(list);
            createCfetsRdiLog(targetDate,targetFilePath,"保存入库"+ targetFilePath, stepId, null );
        }catch (Throwable e){
            createCfetsRdiLog(targetDate,targetFilePath,"保存入库"+ targetFilePath, stepId, e);
            logger.error("rdiZipFileEntryMergeDb",e);
        }
    }

    @Override
    protected void createT(Element element, List<CfetsRdiTradeMemberContact> list) {
        CfetsRdiTradeMemberContact bean = new CfetsRdiTradeMemberContact();
        if("PtyDetlListRpt".equals(element.getName().trim())){
            bean = new CfetsRdiTradeMemberContact();
            for(Iterator iter1 = element.elementIterator(); iter1.hasNext();){
                Element element2 = (Element) iter1.next();
                if("PartyListGrp".equals(element2.getName().trim())){
                    // 机构6位代码
                    bean.setPartyId(element2.attributeValue("ID"));
                    for(Iterator iter2 = element2.elementIterator(); iter2.hasNext();){
                        Element element3 = (Element)iter2.next();
                        if ("PtysSubGrp".equals(element3.getName().trim())){
                            if ("2".equals(element3.attributeValue("Typ"))){
                                // 交易员ID
                                bean.setTradeUserId(element3.attributeValue("ID"));
                            }else if ("124".equals(element3.attributeValue("Typ"))){
                                // 交易成员
                                bean.setInstName(element3.attributeValue("ID"));
                            }else if ("126".equals(element3.attributeValue("Typ"))){
                                // 交易员姓名
                                bean.setTradeUserName(element3.attributeValue("ID"));
                            }
                        }
                        if ("ContactInfoGrp".equals(element3.getName().trim())){
                            if ("6".equals(element3.attributeValue("ContactInfoIDType"))){
                                // 联系电话
                                bean.setContactPhone(element3.attributeValue("ContactInfoID"));
                            }else if ("8".equals(element3.attributeValue("ContactInfoIDType"))){
                                // 传真
                                bean.setFax(element3.attributeValue("ContactInfoID"));
                            }else if ("10".equals(element3.attributeValue("ContactInfoIDType"))){
                                // EMail
                                bean.setEmail(element3.attributeValue("ContactInfoID"));
                            }else if ("11".equals(element3.attributeValue("ContactInfoIDType"))){
                                // MSN
                                bean.setMsn(element3.attributeValue("ContactInfoID"));
                            }else if ("0".equals(element3.attributeValue("ContactInfoIDType"))){
                                // 通讯地址
                                bean.setContactAddress(element3.attributeValue("ContactInfoID"));
                            }else if ("5".equals(element3.attributeValue("ContactInfoIDType"))){
                                // 邮政编码
                                bean.setPostCode(element3.attributeValue("ContactInfoID"));
                            }
                        }
                    }
                }
            }
            list.add(bean);
        }
    }


}
