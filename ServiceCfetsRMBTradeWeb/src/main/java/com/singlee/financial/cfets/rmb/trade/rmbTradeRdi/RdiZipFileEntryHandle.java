package com.singlee.financial.cfets.rmb.trade.rmbTradeRdi;


import java.io.InputStream;
import java.util.List;

/**
 * @author cz
 */
public interface RdiZipFileEntryHandle<T> {

    public static final String TemplateBondBase = "%s/盘后/债券信息_%s.xml.zip";
    public static final String TemplateMemberContactInterBank = "%s/盘后/本币交易成员信息_银行间/交易成员通讯录_银行间_%s.xml.zip";
    public static final String TemplateMemberInterBank = "%s/盘后/本币交易成员信息_银行间/交易成员基本信息_银行间_%s.xml.zip";
    public static final String TemplateXBondBase = "%s/盘前/X-Bond可交易债券信息/X-Bond可交易债券信息_%s.xml.zip";

    /**
     * 获取对应处理类对应的文件模板
     * @return
     */
    String getRdiZipFileEntryPathTemplate();

    /**
     * 获得文件输入流
     * @param targetDate
     * @param targetPath
     *
     * @return
     */
    InputStream getRdiZipFileEntryInputStream(String targetDate,String targetPath);


    /**
     * 根据输入的 zip 文件流信息 处理结果 返回
     * @param targetDate
     * @param is
     * @param targetFilePath
     * @return
     */
    List<T> rdiZipFileEntryParse(String targetDate, InputStream is, String targetFilePath);


    /**
     *
     * @param targetDate
     * @param list
     * @param targetFilePath
     * @return
     */
    void rdiZipFileEntryMergeDb(String targetDate, List<T> list, String targetFilePath);


}
