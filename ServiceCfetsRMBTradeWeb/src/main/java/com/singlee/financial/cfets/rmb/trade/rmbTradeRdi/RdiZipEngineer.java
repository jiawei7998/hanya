package com.singlee.financial.cfets.rmb.trade.rmbTradeRdi;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.config.ConfigRdiBean;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * zip 文件处理引擎
 */
public class RdiZipEngineer {

    private Logger logger = LoggerFactory.getLogger(RdiZipEngineer.class);
    
    private FTPSClient ftpClient = new FTPSClient(true);

    private String ftpServerPath = null;

    private boolean ftpReady;

    private ConfigRdiBean configRdiBean;

    /**
    *登录rdiftp
    */
   public void initFtp(){
      try {
          ftpReady = false;
          ftpClient.setControlEncoding("UTF-8");
          ftpClient.connect(configRdiBean.getHost(), configRdiBean.getPort());
          int reply = ftpClient.getReplyCode();
          if (FTPReply.isPositiveCompletion(reply)) {
              if (ftpClient.login(configRdiBean.getUsername(), configRdiBean.getPassword())) {
                  ftpClient.execPBSZ(0);
                  ftpClient.execPROT("P");
                  if (FTPReply.isPositiveCompletion(ftpClient.sendCommand(
                          "OPTS UTF8", "ON"))) {// 开启服务器对UTF-8的支持，如果服务器支持就用UTF-8编码，否则就使用本地编码（GBK）.
                  }
                  ftpClient.enterLocalPassiveMode();
                  ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                  ftpReady = true;
              }
          }
       } catch(Throwable e){
       	logger.error("initFtp:",e);
       }
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      handle("%s/盘前/本币交易成员信息_银行间/交易成员基本信息_银行间_%s.xml.zip",sdf.format(new Date()));
      handle("%s/盘前/债券信息_%s.xml.zip",sdf.format(new Date()));
      handle("%s/盘前/本币交易成员信息_银行间/交易成员通讯录_银行间_%s.xml.zip",sdf.format(new Date()));
      handle("%s/盘前/X-Bond可交易债券信息/X-Bond可交易债券信息_%s.xml.zip",sdf.format(new Date()));
      
   }

    private static class SingletonHolder {
        private static final RdiZipEngineer INSTANCE = new RdiZipEngineer();
    }

    public static final RdiZipEngineer getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private final Map<String, RdiZipFileEntryHandle<?>> zipFileHandleMap  = new ConcurrentHashMap<String, RdiZipFileEntryHandle<?>>();

    /**
     * 处理
     * CNYTS\{1}\盘前\债券信息_{2}.xml.zip
     * CNYTS\{1}\盘前\本币交易成员信息_银行间\交易成员基本信息_银行间_{2}.xml.zip
     * @param template 文件模板
     * @param targetDate 日期
     *                   CFETS_RDI_LOG
     */
    public void handle(String template, String targetDate){
        RdiZipFileEntryHandle handle = zipFileHandleMap.get(template);
        if(handle == null){
            // CFETS_RDI_LOG  template targetDate
            return;
        }
        /** 根据模板 + 日期 生成 目标文件 */
        String targetFilePath = String.format(template, targetDate, targetDate);
        logger.debug("格式化后的模板文件为%s",targetFilePath);

        /** 获取流 */
        InputStream is = handle.getRdiZipFileEntryInputStream(targetDate,targetFilePath);

        if (is == null ){
            return;
        }
        /** 解析  */
        List<?> list = handle.rdiZipFileEntryParse(targetDate, is,targetFilePath);
        if( list == null ){
            return;
        }
        //
        this.completePendingCommand();
        // CFETS_RDI_LOG  template targetDate

        /** 入库 */
        handle.rdiZipFileEntryMergeDb(targetDate, list,targetFilePath);

    }


    

    /**
     *
     */
    public InputStream inputStreamFtp(String targetPath){
        if(ftpClient!=null && ftpReady && ftpServerPath!=null){
            try {
                ftpClient.changeWorkingDirectory(ftpServerPath);

                String[] pathChanges = targetPath.split("/");
                for(int i =0; i< pathChanges.length -1; i++ ) {
                    ftpClient.changeWorkingDirectory(pathChanges[i]);
                }
                InputStream is = ftpClient.retrieveFileStream(pathChanges[pathChanges.length-1]);
                logger.info("pathChanges[pathChanges.length-1]文件名：" + pathChanges[pathChanges.length-1]);
                logger.info("is文件流：" + is == null ? "为空" : "不为空");
                return is;
            } catch (IOException e) {
            	logger.error("inputStreamFtp:",e);
            }
        }
        return null;
    }

    

    /**
     *
     */
    private void completePendingCommand(){
        try {
            if(ftpClient.completePendingCommand()){
            }else{
            	logger.info("ftp completePendingCommand return false");
            }
        } catch (Throwable e) {
        	logger.error("completePendingCommand",e);
        }
    }
    /**
     *
     */
    public void releaseFtp() {
        try {
            // 退出登录
            ftpClient.logout();
            ftpClient.disconnect();
        } catch (Throwable e) {
        	logger.error("releaseFtp",e);
        }
    }
}
