package com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.impl;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.rmb.trade.entity.rdi.CfetsRdiXbondBaseInfo;
import com.singlee.financial.cfets.rmb.trade.rmbTrade.ImixRmbTradeCfetsApiService;
import com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.RdiZipFileEntryHandle;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

/**
 * @ClassName CfetsRdiXbondBaseInfoHandle
 * @Description X-Bond可交易债券信息处理类
 * @Author qgh
 * @Date 2020-04-09 17:46
 **/
@Service
public class CfetsRdiXbondBaseInfoHandle extends AbstractRdiZipFileEntryHandle<CfetsRdiXbondBaseInfo> implements RdiZipFileEntryHandle<CfetsRdiXbondBaseInfo> {

    private Logger logger = LoggerFactory.getLogger(CfetsRdiXbondBaseInfoHandle.class);

//    @Resource
//    private CfetsRdiXbondBaseInfoService cfetsRdiXbondBaseInfoService;
    @Autowired
	ImixRmbTradeCfetsApiService imixCPISCfetsApiService;

    @Override
    protected void createT(Element element, List<CfetsRdiXbondBaseInfo> list) {
        if("SecDef".equals(element.getName().trim())){
            CfetsRdiXbondBaseInfo bean = new CfetsRdiXbondBaseInfo();
            //债券类型
            bean.setBondDesc(element.attributeValue("Desc"));
            //标准代偿期
            bean.setTermToMaturityString(element.attributeValue("TermToMaturityString"));
            //债券类型代码（6位码）
            bean.setSecurityTypeId(element.attributeValue("SecurityTypeID"));
            //是否集中报价债券
            bean.setCentraQuoteBondIndic("Y".equals(element.attributeValue("CentraQuoteBondIndic")) ? "1" : "0");
            for(Iterator iter1 = element.elementIterator(); iter1.hasNext();){
                Element element2 = (Element) iter1.next();
                if("Instrmt".equals(element2.getName().trim())){
                    // 债券名称
                    bean.setSym(element2.attributeValue("Sym"));
                    // 债券代码
                    bean.setBondId(element2.attributeValue("ID"));
                }
                if("Pty".equals(element2.getName().trim())) {
                    for(Iterator iter2 = element2.elementIterator(); iter2.hasNext();) {
                        Element element3 = (Element) iter2.next();
                        if("Sub".equals(element3.getName().trim())) {
                            if("135".equals(element3.attributeValue("Typ"))) {
                                bean.setPartySubId(element3.attributeValue("ID"));
                            }
                        }
                    }
                }
            }
            list.add(bean);
        }
    }

    @Override
    public String getRdiZipFileEntryPathTemplate() {
        return RdiZipFileEntryHandle.TemplateXBondBase;
    }

    @Override
    public List<CfetsRdiXbondBaseInfo> rdiZipFileEntryParse(String targetDate, InputStream is, String targetFilePath) {
        List<CfetsRdiXbondBaseInfo> list = null;
        final int stepId = 2;
        try {
            list = dom4j(is);
            createCfetsRdiLog(targetDate,targetFilePath,"解析文件流"+ targetFilePath, stepId, null );
        } catch (Throwable e) {
            createCfetsRdiLog(targetDate,targetFilePath,"解析文件流"+ targetFilePath, stepId, e);
            logger.error("rdiZipFileEntryParse",e);
        }
        return list;
    }

    @Override
    public void rdiZipFileEntryMergeDb(String targetDate, List<CfetsRdiXbondBaseInfo> list, String targetFilePath) {
        final int stepId = 3;
        try {
//            cfetsRdiXbondBaseInfoService.saveCfetsRdiXbondBaseInfo(list);
        	RetBean retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRdiCfetsRdiXbondBaseInfo(list);
        	if(ConvertRmbTrade.retSeccess.equalsIgnoreCase(retBean.getRetCode())) {
            	logger.info("sendRdiCfetsRdiXbondBaseInfo执行完成");
            }else {
            	logger.info("sendRdiCfetsRdiXbondBaseInfo执行失败："+retBean.getRetMsg());
            }
            createCfetsRdiLog(targetDate,targetFilePath,"保存入库"+ targetFilePath, stepId, null );
        }catch (Throwable e){
            createCfetsRdiLog(targetDate,targetFilePath,"保存入库"+ targetFilePath, stepId, e);
            logger.error("rdiZipFileEntryMergeDb",e);
        }
    }

}
