package com.singlee.financial.cfets.rmb.trade.config;

import java.io.Serializable;

/**
 * CfetsRmbTrade配置信息实体对象
 * 
 * @author xuqq
 * @version V2020-12-14
 * @since JDK1.8,Hessian4.0.51
 */
public class ConfigBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1991893920409782517L;

	/**
	 * 0-表示收到对 ExecID 所指定的需确认的成交报告，但是尚未做出处理
	 */
	public final static int ExecAckStatus_Recieve_Confirm = 0;

	/**
	 * 1-表示对 ExecID 所指定的需确认的成交报告进行确认并接受
	 */
	public final static int ExecAckStatus_Accept_Confirm = 1;

	/**
	 * 2-表示对 ExecID 所指定的需确认的成交报告拒绝接受
	 */
	public final static int ExecAckStatus_Reject_Confirm = 2;

	/**
	 * CFETS CLIENT.CFG文件相对路径
	 */
	public final static String cfgClientPath = "cfg/client.cfg";
	/**
	 * CFETS FTPCLIENT.PROPERTIES文件相对路径
	 */
	public final static String cfgFtpClientPath = "ftpConfig/ftpclient.properties";
	/**
	 * CFETS报文保存目录
	 */
	public final static String dataPath = "Data";

	/**
	 * 资金前置异常报文保存目录
	 */
	public final static String errDataPath = "errData";

	/**
	 * 外币接口报文保存目录
	 */
	public final static String fxDir = "Fx";

	/**
	 * 本币接口报文保存目录
	 */
	public final static String RmbDir = "Rmb";

	/**
	 * XML文件后缀名
	 */
	public final static String xmlSuffix = ".xml";

	/**
	 * TXT文件后缀名
	 */
	public final static String txtSuffix = ".txt";

	// 银行唯一标识号
	private String bankId;

	// CFETS消息XML文件保存对象
	private String saveFilePath;

	// 是否保存CFETS消息对象XML文件
	private boolean isSaveFile;

	// CFETS用户名
	private String userName;

	// CFETS密码
	private String password;

	// CFETS市场标识
	private String market;

	// xuqq start
	private String targetCompID;// 交易上行类型
	private String targetSubID;// 交易上行类型
	// xuqq end

	// 需要更新client.cfg文件的配置项
	private String updatePathKey;

	// 需要更新ftpclient.properties文件的配置项
	private String updateFtpPathKey;

	// 字符集
	private String charset;

	// 线程池维护线程的最少数量
	private int corePoolSize;

	// 线程池维护线程的最大数量
	private int maximumPoolSize;

	// 线程池维护线程所允许的空闲时间
	private int keepAliveTime;

	// 线程池队列大小
	private int arrayBlockingQueueSize;

	private String sendercompid;

	private String bankName;
	// 断线重连间隔时间
	private int reconTime;

	// 断线重连连接次数
	private int reconCount;

	/**
	 * 获取字符集
	 * 
	 * @return
	 */
	public String getCharset() {
		return charset;
	}

	/**
	 * 设置字符集
	 * 
	 * @param charset
	 */
	public void setCharset(String charset) {
		this.charset = charset;
	}

	/**
	 * 获取银行唯一标识号
	 * 
	 * @return
	 */
	public String getBankId() {
		return bankId;
	}

	/**
	 * 设置银行唯一标识号
	 * 
	 * @param bankId
	 */
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	/**
	 * 获取CFETS对象XML文件保存路径
	 * 
	 * @return
	 */
	public String getSaveFilePath() {
		return saveFilePath;
	}

	/**
	 * 设置CFETS对象XML文件保存路径
	 * 
	 * @param saveFilePath
	 */
	public void setSaveFilePath(String saveFilePath) {
		this.saveFilePath = saveFilePath;
	}

	/**
	 * 是否将CFETS对象以XML文件保存到文件中
	 * 
	 * @return
	 */
	public boolean isSaveFile() {
		return isSaveFile;
	}

	/**
	 * 是否将CFETS对象以XML文件保存到文件中
	 * 
	 * @param isSaveFile
	 */
	public void setIsSaveFile(boolean isSaveFile) {
		this.isSaveFile = isSaveFile;
	}

	/**
	 * 获取CFETS用户名
	 * 
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 设置CFETS用户名
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 获取CFETS密码
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 设置CFETS密码
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 获取CFETS市场标识
	 * 
	 * @return
	 */
	public String getMarket() {
		return market;
	}

	/**
	 * 设置CFETS市场标识
	 * 
	 * @param market
	 */
	public void setMarket(String market) {
		this.market = market;
	}

	/**
	 * 获取client.cfg配置文件中需要更新的key
	 * 
	 * @return
	 */
	public String getUpdatePathKey() {
		return updatePathKey;
	}

	/**
	 * 设置client.cfg配置文件中需要更新的key
	 * 
	 * @param updatePathKey
	 */
	public void setUpdatePathKey(String updatePathKey) {
		this.updatePathKey = updatePathKey;
	}

	public int getCorePoolSize() {
		return corePoolSize;
	}

	public void setCorePoolSize(int corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	public int getMaximumPoolSize() {
		return maximumPoolSize;
	}

	public void setMaximumPoolSize(int maximumPoolSize) {
		this.maximumPoolSize = maximumPoolSize;
	}

	public int getKeepAliveTime() {
		return keepAliveTime;
	}

	public void setKeepAliveTime(int keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	public int getArrayBlockingQueueSize() {
		return arrayBlockingQueueSize;
	}

	public void setArrayBlockingQueueSize(int arrayBlockingQueueSize) {
		this.arrayBlockingQueueSize = arrayBlockingQueueSize;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSendercompid() {
		return sendercompid;
	}

	public void setSendercompid(String sendercompid) {
		this.sendercompid = sendercompid;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public int getReconTime() {
		return reconTime;
	}

	public void setReconTime(int reconTime) {
		this.reconTime = reconTime;
	}

	public int getReconCount() {
		return reconCount;
	}

	public void setReconCount(int reconCount) {
		this.reconCount = reconCount;
	}

	public String getTargetCompID() {
		return targetCompID;
	}

	public void setTargetCompID(String targetCompID) {
		this.targetCompID = targetCompID;
	}

	public String getTargetSubID() {
		return targetSubID;
	}

	public void setTargetSubID(String targetSubID) {
		this.targetSubID = targetSubID;
	}

	public final String getUpdateFtpPathKey() {
		return updateFtpPathKey;
	}

	public final void setUpdateFtpPathKey(String updateFtpPathKey) {
		this.updateFtpPathKey = updateFtpPathKey;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigBean [bankId=");
		builder.append(bankId);
		builder.append(", saveFilePath=");
		builder.append(saveFilePath);
		builder.append(", isSaveFile=");
		builder.append(isSaveFile);
		builder.append(", userName=");
		builder.append(userName);
		builder.append(", password=");
		builder.append(password);
		builder.append(", market=");
		builder.append(market);
		builder.append(", targetCompID=");
		builder.append(targetCompID);
		builder.append(", targetSubID=");
		builder.append(targetSubID);
		builder.append(", updatePathKey=");
		builder.append(updatePathKey);
		builder.append(", updateFtpPathKey=");
		builder.append(updateFtpPathKey);
		builder.append(", charset=");
		builder.append(charset);
		builder.append(", corePoolSize=");
		builder.append(corePoolSize);
		builder.append(", maximumPoolSize=");
		builder.append(maximumPoolSize);
		builder.append(", keepAliveTime=");
		builder.append(keepAliveTime);
		builder.append(", arrayBlockingQueueSize=");
		builder.append(arrayBlockingQueueSize);
		builder.append(", sendercompid=");
		builder.append(sendercompid);
		builder.append(", bankName=");
		builder.append(bankName);
		builder.append(", reconTime=");
		builder.append(reconTime);
		builder.append(", reconCount=");
		builder.append(reconCount);
		builder.append("]");
		return builder.toString();
	}

}
