package com.singlee.financial.cfets.rmb.trade.rmbTrade;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import imix.client.core.ImixSession;

import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

public class ImixRmbTradeAutoConn {
	private ImixSession imixSession = null;
	private String quoteType = "";
	private int reConnCount = 10;
	private int reConnTime = 30000;
	private int reConnTimeOut = 8000;
	
	/***
     * 存放各类isConnecting
     */
    protected ConcurrentHashMap<String,String> isConnectingMap = new ConcurrentHashMap<String, String>();
    protected ConcurrentHashMap<String,String> loginStateMap = new ConcurrentHashMap<String, String>();
	
	private static Logger logger = LoggerFactory.getLogger(ImixRmbTradeAutoConn.class);
	
	public ImixRmbTradeAutoConn(ImixSession imixSession,String quoteType,int reConnCount,int reConnTime)
	{
		this.imixSession = imixSession; 
		this.quoteType = quoteType;
		this.reConnCount = reConnCount;
		this.reConnTime = reConnTime;
	}
	
	public boolean startConn()throws Exception{
		String isConnecting = isConnectingMap.get(quoteType);
		if("T".equals(isConnecting)) {
			return isConn();
		}
		logger.info("======>["+quoteType+"]系统开始登陆.....");
		
		isConnectingMap.put(quoteType,"T");
		try {
			logger.info("======>["+quoteType+"]开始第一次登陆.....");
			if(connServer(reConnTimeOut)) {
				loginStateMap.put(quoteType,"T");
			}else {
				loginStateMap.put(quoteType,"F");
			}
			logger.info("======>["+quoteType+"]第一次登陆结果:"+isConn());
			
			
			Thread.sleep(8000);
			if(imixSession.isStarted()) {
				loginStateMap.put(quoteType,"T");
			}else {
				loginStateMap.put(quoteType,"F");
			}
			logger.info("======>["+quoteType+"]当前连接状态:"+isConn());
			
			if(isConn()){
				return true;
			}
			
			Thread connThread = null;
			//开始重连
			logger.info("======>["+quoteType+"]准备开始重连......");
			for (int i = 1; i < reConnCount; i++) 
			{
				//暂停时长 reConnTime
				Thread.sleep(reConnTime);
				
				//如果登陆成功,直接退出
				if(imixSession.isStarted()) {
					loginStateMap.put(quoteType,"T");
				}else {
					loginStateMap.put(quoteType,"F");
				}
				logger.info("======>["+quoteType+"]当前连接状态:"+isConn());
				if(isConn()){
					break;
				}
				
				logger.info("======>["+quoteType+"]开始第" + i + "次重连......");
				connThread = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {
							if(connServer(reConnTimeOut)) {
								loginStateMap.put(quoteType,"T");
							}else {
								loginStateMap.put(quoteType,"F");
							}
							logger.info("======>["+quoteType+"]重连结果:"+isConn());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				connThread.start();
				//最后一次重连,需要等待连接超时时间后获取是否登陆成功状态
				if(i == reConnCount - 1){
					Thread.sleep(reConnTimeOut + 1000);
				}
			}// end for
			
			//判断最后是否连接成功,如果失败则断开连接
			if(!isConn()){
				logger.info("*******["+quoteType+"]重连累计大于" + reConnCount + "次,断开连接!**********");
	            try {
	                imixSession.stop();
	            } catch (Exception e) {
	            	logger.error("["+quoteType+"]停止服务异常", e);
	            }
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("["+quoteType+"]连接服务器出错:"+e.getMessage(),e);
		} finally {
			isConnectingMap.put(quoteType,"F");
		}
		return isConn();
	}
	
	public boolean isConn(){
		if("T".equals(loginStateMap.get(quoteType))) {
			return true;
		}else {
			return false;
		}
	}
	
	public void setConn(boolean flag){
		if(flag) {
			loginStateMap.put(quoteType,"T");
		}else {
			loginStateMap.put(quoteType,"F");
		}
	}
	
	/***
	 * 
	 * 连接服务器,timeOut时间后超时
	 * @param timeOut
	 * @return
	 * @throws Exception
	 */
	private boolean connServer(int timeOut)throws Exception{
		final RetBean ret = new RetBean();
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					if(imixSession.start()){
						ret.setRetStatus(RetStatusEnum.S);
					}else{
						ret.setRetStatus(RetStatusEnum.F);
					}
				} catch (Exception e) {
					logger.error("["+quoteType+"]连接服务出现异常:" + e.getMessage(), e);
					e.printStackTrace();
					ret.setRetStatus(RetStatusEnum.F);
				} 
			}
		});
		t.start();
		//线程等待timeOut 时长后自动停止
		t.join(timeOut);
		t.interrupt();
		
		return RetStatusEnum.S.equals(ret.getRetStatus());
	}
	
}
