package com.singlee.financial.cfets.rmb.trade.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeCommonOther;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.UnsupportedMessageType;
import imix.field.MarketIndicator;
import imix.field.QueryCondition;
import imix.field.QueryConditionValue;
import imix.field.QueryRequestID;
import imix.field.QueryType;
import imix.field.QuoteType;
import imix.field.TransactTime;
import imix.imix20.QueryRequest;
/**
 * 组装报文
 * @author xuqq
 *
 */
public class SendPackageCfetsTradeCommonOther {
	
	private static Logger LogManager = LoggerFactory.getLogger("Package");
	
	public static RetBean packageMessage(QueryRequest queryRequest,CfetsTradeCommonOther cfetsTradeCommonOther) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		RetBean retBean = new RetBean(RetStatusEnum.S,ConvertRmbTrade.retSeccess , "组装成功！");
		try {
			queryRequest.set(new QueryRequestID(cfetsTradeCommonOther.getQueryRequestId()));
			queryRequest.set(new TransactTime(cfetsTradeCommonOther.getTransactTime()));
			queryRequest.set(new QueryType(cfetsTradeCommonOther.getQueryType()));
			queryRequest.set(new QuoteType(Integer.valueOf(cfetsTradeCommonOther.getQuoteType())));
			queryRequest.set(new MarketIndicator(cfetsTradeCommonOther.getMarketindicator()));
			
			queryRequest.set(new QueryRequestID(cfetsTradeCommonOther.getQueryRequestId()));
			
			String[] queryConditionStr = "6,9,11,12".split(",");
			for(int i=0;i<queryConditionStr.length;i++) {
				QueryRequest.NoQueryConditions noQueryConditions = new QueryRequest.NoQueryConditions();
				if("6".equals(queryConditionStr[i])) {
					noQueryConditions.set(new QueryCondition(6));
					noQueryConditions.set(new QueryConditionValue(cfetsTradeCommonOther.getClOrdId()));
					queryRequest.addGroup(noQueryConditions);
				}else if("9".equals(queryConditionStr[i])) {
					noQueryConditions.set(new QueryCondition(9));
					noQueryConditions.set(new QueryConditionValue(cfetsTradeCommonOther.getQuoteId()));
					queryRequest.addGroup(noQueryConditions);
				}else if("11".equals(queryConditionStr[i])) {
					noQueryConditions.set(new QueryCondition(11));
					noQueryConditions.set(new QueryConditionValue(cfetsTradeCommonOther.getQuoteRequestId()));
					queryRequest.addGroup(noQueryConditions);
				}else if("12".equals(queryConditionStr[i])) {
					noQueryConditions.set(new QueryCondition(12));
					noQueryConditions.set(new QueryConditionValue(cfetsTradeCommonOther.getQuoteRfqId()));
					queryRequest.addGroup(noQueryConditions);
				}
			}
		} catch (Exception e) {
			LogManager.info("组装QueryRequest报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装QueryRequest报文异常");
            return retBean;
		}
		return retBean;
	}
	
	
}
