package com.singlee.financial.cfets.rmb.trade.config;

public class ConfigRdiBean {

	 // ftp地址
    private String host;
    // ftp端口
    private int port;
    // ftp用户名
    private String username;
    // ftp密码
    private String password;
    
    private String ftpServerPath;
    
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFtpServerPath() {
		return ftpServerPath;
	}
	public void setFtpServerPath(String ftpServerPath) {
		this.ftpServerPath = ftpServerPath;
	}
    
}
