package com.singlee.financial.cfets.rmb.trade.rmbTrade;

import imix.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.config.ConfigBean;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeCommonOther;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeStatus;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMarketData;
import com.singlee.financial.cfets.rmb.trade.service.RecAAnalysisRmbTrade;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

/**
 * CFETS消息线程序处理
 * 
 * @author chengmj
 * 
 */
public class ImixRmbTradeRecMessageProcess extends Thread {
	Message message;

	ImixRmbTradeCfetsApiService imixCPISCfetsApiService;
	
	ImixRmbTradeClientListener clientListener;
	// CFETS配置类
	private ConfigBean configBean;

	private static Logger LogManager = LoggerFactory.getLogger(ImixRmbTradeRecMessageProcess.class);

	public ImixRmbTradeRecMessageProcess(Message message, ImixRmbTradeCfetsApiService imixCPISCfetsApiService, ConfigBean configBean,
			ImixRmbTradeClientListener clientListener) {
		this.message = message;
		this.imixCPISCfetsApiService = imixCPISCfetsApiService;
		this.configBean = configBean;
		this.clientListener = clientListener;
	}
	@Override
	public void run() {
		try {
			CfetsTradePackage cfetsTradePackage = new CfetsTradePackage();//报文对象
			CfetsTradeProgress cfetsTradeProgress = new CfetsTradeProgress();//报文解析对象
			CfetsTradeStatus cfetsTradeStatus = new CfetsTradeStatus();//交易时段对象
			CfetsTradeMarketData cfetsTradeMarketData = new CfetsTradeMarketData();//行情对象
			CfetsTradeCommonOther cfetsTradeCommonOther = new CfetsTradeCommonOther();//失败报文对象
			//转换对象
			RetBean retBean = null;
			String functionMark = RecAAnalysisRmbTrade.getFunctionMark(message);
			
			LogManager.info("============>报文解析开始");//解析对象
			if("CfetsTradeStatus".equals(functionMark)) {
				//交易时段报文
				retBean = RecAAnalysisRmbTrade.analysisMessage(cfetsTradePackage,cfetsTradeStatus,message);
			}else if("CfetsTradeMarketData".equals(functionMark)){
				//行情表
				retBean = RecAAnalysisRmbTrade.analysisMessage(cfetsTradePackage,cfetsTradeMarketData,message);
			}else if("CfetsTradeCommonOther".equals(functionMark)){
				//失败报文表
				retBean = RecAAnalysisRmbTrade.analysisMessage(cfetsTradePackage,cfetsTradeCommonOther,message);
			}else if("CfetsTradeProgress".equals(functionMark)){
				//交易表
				retBean = RecAAnalysisRmbTrade.analysisMessage(cfetsTradePackage,cfetsTradeProgress,message);
			}else {
				LogManager.info("============>报文解析不解析:暂未开发报文类型：functionMark="+functionMark);
				return;
			}
			LogManager.info("============>报文解析结束:retCode=["+retBean.getRetCode()+"],retMsg=["+retBean.getRetMsg()+"]");
			
			if(ConvertRmbTrade.retSeccess.equals(retBean.getRetCode())) {
				//调用发送capital方法：如果retBean是成功，则处理，失败则保存失败信息
				LogManager.info("============>报文发送开始：");//解析对象
				
				if("CfetsTradeStatus".equals(functionMark)) {
					//交易时段报文
					retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRMBTrade(cfetsTradeStatus, cfetsTradePackage);
				}else if("CfetsTradeMarketData".equals(functionMark)){
					//行情表
					retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRMBTrade(cfetsTradeMarketData, cfetsTradePackage);
				}else if("CfetsTradeCommonOther".equals(functionMark)){
					//失败报文表
					retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRMBTrade(cfetsTradeCommonOther, cfetsTradePackage);
				}else if("CfetsTradeProgress".equals(functionMark)){
					//交易表
					retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRMBTrade(cfetsTradeProgress, cfetsTradePackage);
				}
				
				LogManager.info("============>报文发送结束:retCode=["+retBean.getRetCode()+"],retMsg=["+retBean.getRetMsg()+"]");
			}else {
				LogManager.info("============>报文解析失败，不发送：");//解析对象
				//TODO 保存文件
				LogManager.info("============>保存报文文件开始");
				LogManager.info("============>保存报文文件结束");
			}
        } catch (Exception e) {
            LogManager.error("fromApp Message", e);
        }
	}
	
}
