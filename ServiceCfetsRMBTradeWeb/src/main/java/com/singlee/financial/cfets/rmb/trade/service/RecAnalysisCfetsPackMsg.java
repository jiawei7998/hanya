package com.singlee.financial.cfets.rmb.trade.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.BeginString;
import imix.field.ClOrdID;
import imix.field.MarketIndicator;
import imix.field.MsgType;
import imix.field.QuoteID;
import imix.field.QuoteStatus;
import imix.field.QuoteTransType;
import imix.field.QuoteType;
import imix.field.SenderCompID;
import imix.field.SenderSubID;
import imix.field.SendingTime;
import imix.field.TargetCompID;
import imix.field.TargetSubID;
import imix.imix20.Message.Header;
import imix.imix20.BusinessMessageReject;
import imix.imix20.CollateralAssignment;
import imix.imix20.CollateralResponse;
import imix.imix20.ExecutionReport;
import imix.imix20.ListMarketDataAck;
import imix.imix20.MarketDataRequest;
import imix.imix20.MarketDataSnapshotFullRefresh;
import imix.imix20.NewOrderSingle;
import imix.imix20.OrderCancelReject;
import imix.imix20.OrderCancelRequest;
import imix.imix20.Quote;
import imix.imix20.QuoteCancel;
import imix.imix20.QuoteResponse;
import imix.imix20.QuoteStatusReport;
/**
 * 解析报文头
 * @author xuqq
 *
 */
public class RecAnalysisCfetsPackMsg {
	private static Logger LogManager = LoggerFactory.getLogger("RecAnalysis");
	
	public static void analysisCfetsPackMsg(RetBean retBean,CfetsTradePackage cfetsTradePackage,Message message) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String quoteId="";
		Header header = null;
		try {
			header = getHeader(retBean,message);
			if(header==null) {
				retBean.setRetCode(ConvertRmbTrade.retError);
				retBean.setRetMsg("此报文类型不解析");
			}else {
				cfetsTradePackage.setMsgType(header.isSetField(MsgType.FIELD) ?
						header.getString(MsgType.FIELD) : "");//报文类型
				cfetsTradePackage.setTradedate(header.isSetField(SendingTime.FIELD) ?
						header.getString(SendingTime.FIELD) : "");//成交日期
				cfetsTradePackage.setSenderCompid(header.isSetField(SenderCompID.FIELD) ? 
						header.getString(SenderCompID.FIELD) : "");//
				cfetsTradePackage.setSenderSubid(header.isSetField(SenderSubID.FIELD) ? 
						header.getString(SenderSubID.FIELD) : "");//
				cfetsTradePackage.setTargetCompid(header.isSetField(TargetCompID.FIELD) ? 
						header.getString(TargetCompID.FIELD) : "");//
				cfetsTradePackage.setTargetSubid(header.isSetField(TargetSubID.FIELD) ? 
						header.getString(TargetSubID.FIELD) : "");//
				cfetsTradePackage.setBeginString(header.isSetField(BeginString.FIELD) ? 
						header.getString(BeginString.FIELD) : "");//报文版本
			}
			
			cfetsTradePackage.setUpordown("DOWN");//上行或下行
			quoteId=message.isSetField(QuoteID.FIELD) ? message.getString(QuoteID.FIELD) : "";
			cfetsTradePackage.setQuoteId(quoteId);//报价编号
			cfetsTradePackage.setClOrdId(message.isSetField(ClOrdID.FIELD) ?
					message.getString(ClOrdID.FIELD) : "");//客户参考编号
			cfetsTradePackage.setMarketindicator(message.isSetField(MarketIndicator.FIELD) ? 
					message.getString(MarketIndicator.FIELD) : "");//市场标识
			cfetsTradePackage.setQuoteType(message.isSetField(QuoteType.FIELD) ? 
					message.getString(QuoteType.FIELD) : "");//报价类别
			cfetsTradePackage.setQuoteTransType(message.isSetField(QuoteTransType.FIELD) ? 
					message.getString(QuoteTransType.FIELD) : "");//操作类型
			cfetsTradePackage.setQuoteStatus(message.isSetField(QuoteStatus.FIELD) ? 
					message.getString(QuoteStatus.FIELD) : "");//报价状态
			cfetsTradePackage.setPackmsg(message.toString());//报文
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradePackage失败:quoteId="+quoteId,e);
			retBean.setRetMsg("组装对象CfetsTradePackage失败");
		}
	}
	/**
	 * 获取报文Header对象
	 * @param retBean
	 * @param message
	 * @return
	 * @throws FieldNotFound
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	public static Header getHeader(RetBean retBean,Message message) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		Header header = null;
		if(message instanceof Quote) {
			header = ((Quote)message).getHeader();
		}else if(message instanceof QuoteStatusReport) {
			header = ((QuoteStatusReport)message).getHeader();
		}else if(message instanceof QuoteResponse) {
			header = ((QuoteResponse)message).getHeader();
		}else if(message instanceof QuoteCancel) {
		}else if(message instanceof ExecutionReport) {
			header = ((ExecutionReport)message).getHeader();
		}else if(message instanceof NewOrderSingle) {
			
		}else if(message instanceof OrderCancelRequest) {
			
		}else if(message instanceof OrderCancelReject) {
			
		}else if(message instanceof CollateralAssignment) {
			
		}else if(message instanceof CollateralResponse) {
			
		}else if(message instanceof MarketDataRequest) {
			
		}else if(message instanceof ListMarketDataAck) {
			
		}else if(message instanceof MarketDataSnapshotFullRefresh) {
			
		}else if(message instanceof BusinessMessageReject) {
			
		}else {
			if(retBean!=null) {
				
			}
		}
		return header;
	}
}
