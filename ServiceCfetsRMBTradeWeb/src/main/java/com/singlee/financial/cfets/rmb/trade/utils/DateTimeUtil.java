package com.singlee.financial.cfets.rmb.trade.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期工具
 * @author LyonChen
 *
 */
public class DateTimeUtil {
	
	public static String utcDate(String s, String c){
		return s.replaceAll(c, "");
	}
	
	public static String formatTime(String transactTime) {
		if (transactTime.length() < 19) {
			return transactTime;}
		return transactTime.substring(11, 19);
	}
	
	/**
	 * 日期格式："yyyy-MM-dd HH:mm:ss"
	 * @return
	 */
	public static String getLocalDateTime() {
		SimpleDateFormat tempDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return tempDate.format(new java.util.Date());
	}

	/**
	 * 日期格式："yyyyMMddHHmmss"
	 * @return
	 */
	public static String getLocalCompatDateTime() {
		SimpleDateFormat tempDate = new SimpleDateFormat("yyyyMMddHHmmss");
		return tempDate.format(new java.util.Date());
	}
	
	/**
	 * 时间格式："HH:mm:ss"
	 * @return
	 */
	public static String getLocalTime() {
		SimpleDateFormat tempDate = new SimpleDateFormat("HH:mm:ss");
		return tempDate.format(new java.util.Date());
	}
	
	/**
	 * 日期比较大小,date2>=date1
	 * @param date1
	 * @param date2
	 * @param format 格式, 如: yyyyMMdd-HH:mm:ss.SSS
	 * @return
	 * @throws ParseException
	 */
	public static long compareDateTime(String date1, String date2, String format) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		Date dt1 = dateFormat.parse(date1);
		
		dateFormat = new SimpleDateFormat(format);
		Date dt2 = dateFormat.parse(date2);
		
		long diff = dt2.getTime() - dt1.getTime();
		return diff;
	}

	public static String getSerialNo() {
		SimpleDateFormat tempDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return tempDate.format(new java.util.Date());
	}
	/**
	 * yyyyMMdd-- > yyyy-MM-dd
	 * @param cfetsDate
	 * @return
	 */
	public static String standardDate(String cfetsDate) {
		if (cfetsDate==null || cfetsDate.length() < 8) {
			return cfetsDate;}
		return format(cfetsDate, '-');
	}
	
	
	public static String formatDate(String transactTime) {
		if (transactTime==null || transactTime.length() < 8) {
			return transactTime;}
		return format(transactTime, ':');
	}
	private final  static String format(String s, char p){
		return s.substring(0, 4) + p + s.substring(4, 6) + p + s.substring(6, 8);
	}
	
	public static String standardTime(String time,char p){
		if(time.length()>10){
			return time.substring(0, 4) + p + time.substring(4, 6) + p + time.substring(6, 8)+" "+time.substring(10, time.length());
		}
		else{
			return  format(time, p);
		}
	}
	
}
