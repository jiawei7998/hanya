package com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.impl;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.financial.cfets.rmb.trade.entity.rdi.CfetsRdiLog;
import com.singlee.financial.cfets.rmb.trade.rmbTrade.ImixRmbTradeCfetsApiService;
import com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.RdiZipEngineer;
import com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.RdiZipFileEntryHandle;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

/**
 * @author cz
 */
public abstract class AbstractRdiZipFileEntryHandle<T> implements RdiZipFileEntryHandle<T> {

	private Logger logger = LoggerFactory.getLogger(AbstractRdiZipFileEntryHandle.class);
	
//    @Autowired
//    private CfetsRdiLogService cfetsRdiLogService;
	@Autowired
	ImixRmbTradeCfetsApiService imixCPISCfetsApiService;


    @Override
    public InputStream getRdiZipFileEntryInputStream(String targetDate, String targetPath) {

        InputStream is = null;
        final int stepId = 1;
        try {
            InputStream remote = RdiZipEngineer.getInstance().inputStreamFtp(targetPath);
            ZipInputStream zipInputStream = new ZipInputStream(remote);
            ZipEntry ze = null;
            if ((ze = zipInputStream.getNextEntry()) != null) {
                if (!ze.isDirectory()) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] byte_s = new byte[2048];
                    int num = -1;
                    while ((num = zipInputStream.read(byte_s, 0, byte_s.length)) > -1) {//通过read方法来读取文件内容
                        byteArrayOutputStream.write(byte_s, 0, num);
                    }
                    //IOUtils.copy(zipInputStream, byteArrayOutputStream);
                    byte[] resultBytes = byteArrayOutputStream.toByteArray();
                    is = new ByteArrayInputStream(resultBytes);
                }
            }
            createCfetsRdiLog(targetDate, targetPath, "获取文件流" + targetPath, stepId, null);
        } catch (Throwable e) {
            createCfetsRdiLog(targetDate, targetPath, "获取文件流" + targetPath, stepId, e);
            logger.error("",e);
        }
        return is;

    }

    /**
     *
     * @param date
     * @param targetPath
     * @param jobTime
     * @param stepId
     * @param t
     * @return
     */
    protected final void createCfetsRdiLog(String date,String targetPath, String jobTime, int stepId, Throwable t) {
    	try {
    		CfetsRdiLog cfetsRdiLog = new CfetsRdiLog();
            cfetsRdiLog.setOperateDate(date);
            cfetsRdiLog.setJobTime(jobTime);
            cfetsRdiLog.setStepId(stepId);
            cfetsRdiLog.setTargetPath(targetPath);
            cfetsRdiLog.setStatus(t == null ? "成功" : "失败");
            cfetsRdiLog.setLogs((t == null? "": ("\r\n" + ExceptionUtils.getRootCauseMessage(t))));
            cfetsRdiLog.setOperateTime((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()));
            
            RetBean retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRdiCfetsRdiLog(cfetsRdiLog);
            if(ConvertRmbTrade.retSeccess.equalsIgnoreCase(retBean.getRetCode())) {
            	logger.info("sendRdiCfetsRdiLog执行完成");
            }else {
            	logger.info("sendRdiCfetsRdiLog执行失败："+retBean.getRetMsg());
            }
		} catch (Exception e) {
			logger.error("createCfetsRdiLog执行异常：",e);
		}
//        cfetsRdiLogService.saveRdiLog(cfetsRdiLog);
    }


    /**
     *
     * @param is
     * @return
     */
    protected List<T> dom4j(InputStream is){
        List<T> list = new ArrayList<T>();
        Document document = null;
        try {
            SAXReader saxReader = new SAXReader();
            document = saxReader.read(is);
        } catch (Exception ex) {
            // TODO 记录异常信息
            ex.printStackTrace();
        }
        Element root = document.getRootElement();
        // 循环遍历根节点
        for (Iterator iter = root.elementIterator(); iter.hasNext();) {
            Element element = (Element) iter.next();
            createT(element, list);
        }
        return list;
    }

    protected abstract void createT(Element element, List<T> list);



}
