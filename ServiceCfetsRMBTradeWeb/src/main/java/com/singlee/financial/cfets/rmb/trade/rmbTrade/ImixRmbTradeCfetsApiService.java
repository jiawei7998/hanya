package com.singlee.financial.cfets.rmb.trade.rmbTrade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.rmb.trade.IImixCfetsTradeProgress;



/**
 * 交易上行
 */
@Service
public class ImixRmbTradeCfetsApiService {

	@Autowired
	IImixCfetsTradeProgress iImixCfetsTradeProgress;//上行交易：下行数据接收

	public IImixCfetsTradeProgress getiImixCfetsTradeProgress() {
		return iImixCfetsTradeProgress;
	}

	public void setiImixCfetsTradeProgress(IImixCfetsTradeProgress iImixCfetsTradeProgress) {
		this.iImixCfetsTradeProgress = iImixCfetsTradeProgress;
	}


	
}
