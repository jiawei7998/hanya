package com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.impl;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.rmb.trade.entity.rdi.CfetsRdiTradeMember;
import com.singlee.financial.cfets.rmb.trade.rmbTrade.ImixRmbTradeCfetsApiService;
import com.singlee.financial.cfets.rmb.trade.rmbTradeRdi.RdiZipFileEntryHandle;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

@Service
public class CfetsRdiTradeMemberHandle extends AbstractRdiZipFileEntryHandle<CfetsRdiTradeMember> implements RdiZipFileEntryHandle<CfetsRdiTradeMember> {

	private Logger logger = LoggerFactory.getLogger(CfetsRdiTradeMemberHandle.class);
	
//    @Autowired
//    private CfetsRdiTradeMemberService cfetsRdiTradeMemberService;
	@Autowired
	ImixRmbTradeCfetsApiService imixCPISCfetsApiService;

    @Override
    public String getRdiZipFileEntryPathTemplate() {
        return RdiZipFileEntryHandle.TemplateMemberInterBank;
    }

    @Override
    public List<CfetsRdiTradeMember> rdiZipFileEntryParse(String targetDate, InputStream is, String targetFilePath) {
        List<CfetsRdiTradeMember> list = null;
        final int stepId = 2;
        try {

            list = dom4j(is);
            createCfetsRdiLog(targetDate,targetFilePath,"解析文件流"+ targetFilePath, stepId, null );
        } catch (Throwable e) {
            createCfetsRdiLog(targetDate,targetFilePath,"解析文件流"+ targetFilePath, stepId, e);
            logger.error("rdiZipFileEntryParse",e);
        }
        return list;
    }

    @Override
    public void rdiZipFileEntryMergeDb(String targetDate, List<CfetsRdiTradeMember> list, String targetFilePath) {

        final int stepId = 3;
        try {
        	RetBean retBean = imixCPISCfetsApiService.getiImixCfetsTradeProgress().sendRdiCfetsRdiTradeMember(list);
        	if(ConvertRmbTrade.retSeccess.equalsIgnoreCase(retBean.getRetCode())) {
            	logger.info("sendRdiCfetsRdiTradeMember执行完成");
            }else {
            	logger.info("sendRdiCfetsRdiTradeMember执行失败："+retBean.getRetMsg());
            }
//            cfetsRdiTradeMemberService.saveCfetsRdiTradeMember(list);
            createCfetsRdiLog(targetDate,targetFilePath,"保存入库"+ targetFilePath, stepId, null );
        } catch (Throwable e) {
            createCfetsRdiLog(targetDate,targetFilePath,"保存入库"+ targetFilePath, stepId, e);
            logger.error("rdiZipFileEntryMergeDb",e);
        }
    }

    @Override
    protected void createT(Element element, List<CfetsRdiTradeMember> list) {
        CfetsRdiTradeMember bean = new CfetsRdiTradeMember();
        if("PtyDetlListRpt".equals(element.getName().trim())){
            bean = new CfetsRdiTradeMember();
            for(Iterator iter1 = element.elementIterator(); iter1.hasNext();){
                Element element2 = (Element) iter1.next();
                if("PartyListGrp".equals(element2.getName().trim())){
                    // 机构21位代码
                    bean.setPartyId(element2.attributeValue("ID"));
                    for(Iterator iter2 = element2.elementIterator(); iter2.hasNext();){
                        Element element3 = (Element)iter2.next();
                        if ("PtysSubGrp".equals(element3.getName().trim())){
                            // 交易成员id
                            if ("2".equals(element3.attributeValue("Typ"))){
                                bean.setPartySubId(element3.attributeValue("ID"));
                            }else if ("5".equals(element3.attributeValue("Typ"))){
                                // 英文全称
                                bean.setEnFullname(element3.attributeValue("ID"));
                            }else if ("102".equals(element3.attributeValue("Typ"))){
                                // 英文简称
                                bean.setEnShortname(element3.attributeValue("ID"));
                            }else if("124".equals(element3.attributeValue("Typ"))){
                                // 交易成员全称
                                bean.setCnFullname(element3.attributeValue("ID"));
                            }else if("125".equals(element3.attributeValue("Typ"))){
                                // 中文简称
                                bean.setCnShortname(element3.attributeValue("ID"));
                            }
                        }
                    }
                }
            }
            list.add(bean);
        }
    }
}
