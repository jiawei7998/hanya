package com.singlee.financial.cfets.rmb.trade.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeCommonOther;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.FieldNotFound;
import imix.Group;
import imix.IncorrectTagValue;
import imix.UnsupportedMessageType;
import imix.field.MsgType;
import imix.field.NoQueryConditions;
import imix.field.QueryCondition;
import imix.field.QueryConditionValue;
import imix.imix10.Reject;
import imix.imix20.BusinessMessageReject;
import imix.imix20.CollateralResponse;
import imix.imix20.OrderCancelReject;
import imix.imix20.QueryResult;
/**
 * 解析报文头
 * @author xuqq
 *
 */
public class RecAnalysisCfetsTradeCommonOther {
	private static Logger LogManager = LoggerFactory.getLogger("RecAnalysis");
	
	public static void analysisCfetsTradeCommonOther(RetBean retBean,CfetsTradeCommonOther cfetsTradeCommonOther,QueryResult queryResult) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType ="";
		try {
			msgType=queryResult.getHeader().isSetField(MsgType.FIELD) ? 
					queryResult.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeCommonOther.setMsgType(msgType);//报文类型
			cfetsTradeCommonOther.setQueryType(queryResult.isSetQueryType() ? 
					String.valueOf(queryResult.getQueryType().getValue()) : "");//操作类型
			cfetsTradeCommonOther.setQueryRequestId(queryResult.isSetQueryRequestID() ? 
					queryResult.getQueryRequestID().getValue() : "");//查询请求编号
			cfetsTradeCommonOther.setTotNumReports(queryResult.isSetTotNumReports() ? 
					String.valueOf(queryResult.getTotNumReports().getValue()) : "");//为0：标识未查到
			if(queryResult.isSetNoQueryConditions()) {
				for(int i=0;i<queryResult.getNoQueryConditions().getValue();i++) {
					Group noQueryConditions = queryResult.getGroup(i, NoQueryConditions.FIELD);
					String queryCondition = noQueryConditions.isSetField(QueryCondition.FIELD)?
							noQueryConditions.getString(QueryCondition.FIELD):"";
					String queryConditionValue = noQueryConditions.isSetField(QueryConditionValue.FIELD)?
							noQueryConditions.getString(QueryConditionValue.FIELD):"";
					if("6".equals(queryCondition)) {
						cfetsTradeCommonOther.setClOrdId(queryConditionValue);//客户参考编号6
					}else if("9".equals(queryCondition)) {
						cfetsTradeCommonOther.setQuoteId(queryConditionValue);//报价编号9
					}else if("11".equals(queryCondition)) {
						cfetsTradeCommonOther.setQuoteRequestId(queryConditionValue);//报价请求编号11
					}else if("12".equals(queryCondition)) {
						cfetsTradeCommonOther.setQuoteRfqId(queryConditionValue);//RFQ报价提券编号12
					}
				}
			}
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeCommonOther[QueryResult]失败:",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeCommonOther[QueryResult]失败");
		}
	}
	public static void analysisCfetsTradeCommonOther(RetBean retBean,CfetsTradeCommonOther cfetsTradeCommonOther,BusinessMessageReject businessMessageReject) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType ="";
		try {
			msgType=businessMessageReject.getHeader().isSetField(MsgType.FIELD) ? 
					businessMessageReject.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeCommonOther.setMsgType(msgType);//报文类型
			cfetsTradeCommonOther.setTransactTime(businessMessageReject.isSetTransactTime() ? 
					businessMessageReject.getTransactTime().getValue() : "");//时间
			cfetsTradeCommonOther.setClOrdId(businessMessageReject.isSetClOrdID() ? 
					businessMessageReject.getClOrdID().getValue() : "");//客户参考编号6
			cfetsTradeCommonOther.setApplErrorCode(businessMessageReject.isSetApplErrorCode() ? 
					businessMessageReject.getApplErrorCode().getValue() : "");//错误代码
			cfetsTradeCommonOther.setApplErrorDesc(businessMessageReject.isSetApplErrorDesc() ? 
					businessMessageReject.getApplErrorDesc().getValue() : "");//错误原因
			cfetsTradeCommonOther.setQuoteType(businessMessageReject.isSetRefMsgType() ? 
					businessMessageReject.getRefMsgType().getValue() : "");//报价类别:代替原消息类型保存
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeCommonOther[BusinessMessageReject]失败:",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeCommonOther[BusinessMessageReject]失败");
		}
	}
	public static void analysisCfetsTradeCommonOther(RetBean retBean,CfetsTradeCommonOther cfetsTradeCommonOther,Reject reject) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType ="";
		try {
			msgType=reject.getHeader().isSetField(MsgType.FIELD) ? 
					reject.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeCommonOther.setMsgType(msgType);//报文类型
			cfetsTradeCommonOther.setApplErrorCode(reject.isSetSessionRejectReason() ? 
					String.valueOf(reject.getSessionRejectReason().getValue()) : "");//错误代码
			cfetsTradeCommonOther.setApplErrorDesc(reject.isSetText() ? 
					reject.getText().getValue() : "");//错误原因
			cfetsTradeCommonOther.setQuoteType(reject.isSetRefSeqNum() ? 
					String.valueOf(reject.getRefSeqNum().getValue()) : "");//报价类别:代替原消息序号
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeCommonOther[Reject]失败:",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeCommonOther[Reject]失败");
		}
	}
	public static void analysisCfetsTradeCommonOther(RetBean retBean,CfetsTradeCommonOther cfetsTradeCommonOther,OrderCancelReject msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType ="";
		try {
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? 
					msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeCommonOther.setMsgType(msgType);//报文类型
			
			cfetsTradeCommonOther.setMarketindicator(msg.isSetMarketIndicator() ? 
					msg.getMarketIndicator().getValue() : "");
			
			cfetsTradeCommonOther.setClOrdId(msg.isSetClOrdID() ? 
					msg.getClOrdID().getValue() : "");//客户参考编号6
			cfetsTradeCommonOther.setQuoteId(msg.isSetOrderID() ? 
					msg.getOrderID().getValue() : "");//订单编号
			cfetsTradeCommonOther.setApplErrorCode(msg.isSetApplErrorCode() ? 
					msg.getApplErrorCode().getValue() : "");//错误代码
			cfetsTradeCommonOther.setApplErrorDesc(msg.isSetApplErrorDesc() ? 
					msg.getApplErrorDesc().getValue() : "");//错误原因
			
			cfetsTradeCommonOther.setTransactTime(msg.isSetTransactTime() ?
					msg.getTransactTime().getValue() : "");//业务发生时间
			cfetsTradeCommonOther.setUserReference1(msg.isSetUserReference1() ?
					msg.getUserReference1().getValue() : "");//用户参考数据1
			cfetsTradeCommonOther.setUserReference2(msg.isSetUserReference2() ?
					msg.getUserReference2().getValue() : "");//用户参考数据2
			cfetsTradeCommonOther.setUserReference3(msg.isSetUserReference3() ?
					msg.getUserReference3().getValue() : "");//用户参考数据3
			cfetsTradeCommonOther.setUserReference4(msg.isSetUserReference4() ?
					msg.getUserReference4().getValue() : "");//用户参考数据4
			cfetsTradeCommonOther.setUserReference5(msg.isSetUserReference5() ?
					msg.getUserReference5().getValue() : "");//用户参考数据5
			cfetsTradeCommonOther.setUserReference6(msg.isSetUserReference6() ?
					msg.getUserReference6().getValue() : "");//用户参考数据6
			
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeCommonOther[Reject]失败:",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeCommonOther[Reject]失败");
		}
	}
	
	public static void analysisCfetsTradeCommonOther(RetBean retBean,CfetsTradeCommonOther cfetsTradeCommonOther,CollateralResponse msg) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		String msgType ="";
		try {
			msgType=msg.getHeader().isSetField(MsgType.FIELD) ? 
					msg.getHeader().getString(MsgType.FIELD) : "";
			cfetsTradeCommonOther.setMsgType(msgType);//报文类型
			
			cfetsTradeCommonOther.setMarketindicator(msg.isSetMarketIndicator() ? 
					msg.getMarketIndicator().getValue() : "");//市场标识
			cfetsTradeCommonOther.setClOrdId(msg.isSetClOrdID() ? 
					msg.getClOrdID().getValue() : "");//客户参考编号6
			cfetsTradeCommonOther.setQuoteId(msg.isSetTradeRecordID() ? 
					msg.getTradeRecordID().getValue() : "");//交易记录编号
			cfetsTradeCommonOther.setApplErrorCode(msg.isSetApplErrorCode() ? 
					msg.getApplErrorCode().getValue() : "");//错误代码
			cfetsTradeCommonOther.setApplErrorDesc(msg.isSetApplErrorDesc() ? 
					msg.getApplErrorDesc().getValue() : "");//错误原因
		} catch (Exception e) {
			LogManager.error("=========组装对象CfetsTradeCommonOther[Reject]失败:",e);
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对象CfetsTradeCommonOther[Reject]失败");
		}
	}
}
