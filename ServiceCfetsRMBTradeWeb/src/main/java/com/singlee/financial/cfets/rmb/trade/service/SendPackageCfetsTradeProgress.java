package com.singlee.financial.cfets.rmb.trade.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.cbt.CfetsTradeBond;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeBondPledge;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeSubbond;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.UnsupportedMessageType;
import imix.field.AccruedInterestAmt;
import imix.field.AccruedInterestTotalAmt;
import imix.field.AllocInd;
import imix.field.Channel;
import imix.field.ClOrdID;
import imix.field.ClearingMethod;
import imix.field.DeliveryType;
import imix.field.DeliveryType2;
import imix.field.MarketIndicator;
import imix.field.OrdType;
import imix.field.OrderID;
import imix.field.OrderQty;
import imix.field.OrigClOrdID;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.Price;
import imix.field.Principal;
import imix.field.QueryRequestID;
import imix.field.QuoteID;
import imix.field.QuoteReqID;
import imix.field.QuoteRequestRejectType;
import imix.field.QuoteRespType;
import imix.field.QuoteStatus;
import imix.field.QuoteTransType;
import imix.field.QuoteType;
import imix.field.Reference;
import imix.field.RepoMethod;
import imix.field.SecurityID;
import imix.field.SettlCurrency;
import imix.field.SettlDate;
import imix.field.SettlType;
import imix.field.Side;
import imix.field.Symbol;
import imix.field.TotalPrincipal;
import imix.field.TradeCashAmt;
import imix.field.TradeLimitDays;
import imix.field.TradeRecordID;
import imix.field.TransactTime;
import imix.field.UnderlyingSecurityID;
import imix.field.UnderlyingSymbol;
import imix.field.ValidUntilTime;
import imix.imix20.CollateralAssignment;
import imix.imix20.MarketDataRequest;
import imix.imix20.NewOrderSingle;
import imix.imix20.OrderCancelRequest;
import imix.imix20.Quote;
import imix.imix20.QuoteCancel;
import imix.imix20.QuoteRequest;
import imix.imix20.QuoteRequestCancel;
import imix.imix20.QuoteRequestReject;
import imix.imix20.QuoteResponse;
import imix.imix20.component.QuotReqCxlGrp;
/**
 * 组装报文
 * @author xuqq
 *
 */
public class SendPackageCfetsTradeProgress {
	
	private static Logger LogManager = LoggerFactory.getLogger("Package");
	
	public static void packageMessage(RetBean retBean,Quote quote,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			CfetsTradeBondPledge cfetsTradeBondPledge = cfetsTradeProgress.getCfetsTradeBondPledge();
            //公共
            MarketIndicator marketIndicator = new MarketIndicator(cfetsTradeProgress.getMarketindicator());
            Channel channel=new Channel(cfetsTradeProgress.getChannel());
            QuoteType quoteType = new QuoteType(Integer.valueOf(cfetsTradeProgress.getQuoteType()));
            char quoteTransTypeChar = cfetsTradeProgress.getQuoteTransType().toCharArray()[0];
            QuoteTransType quoteTransType = new QuoteTransType(quoteTransTypeChar);
            ClOrdID clOrdID = new ClOrdID(cfetsTradeProgress.getClOrdId());

            Side side = new Side(cfetsTradeBondPledge.getSide().toCharArray()[0]);
            RepoMethod repoMethod = new RepoMethod(cfetsTradeBondPledge.getRepoMethod().toCharArray()[0]);
            TradeLimitDays tradeLimitDays = new TradeLimitDays(cfetsTradeBondPledge.getTradeLimitDays().toString());
            Price price = new Price(cfetsTradeBondPledge.getPrice().toString());
            TradeCashAmt tradeCashAmt = new TradeCashAmt(cfetsTradeBondPledge.getTradeCashAmt().toString());
            SettlType settlType = new SettlType(cfetsTradeBondPledge.getSettType());
            DeliveryType deliveryType = new DeliveryType(Integer.parseInt(cfetsTradeBondPledge.getDeliveryType()));
            DeliveryType2 deliveryType2 = new DeliveryType2(Integer.parseInt(cfetsTradeBondPledge.getDeliveryType2()));
            ClearingMethod clearingMethod = new ClearingMethod(Integer.parseInt(cfetsTradeBondPledge.getClearingMethod()));
           
            if(quoteTransTypeChar==QuoteTransType.NEW){
            	//发送新增报价
            }else if(quoteTransTypeChar==QuoteTransType.REPLACE
            		||quoteTransTypeChar==QuoteTransType.COUNTER){
            	//发送修改报价、发送交谈报价
                if(cfetsTradeProgress.getOrigClOrdId()!=null){
                    //预留字段有则传，新增不传
                    OrigClOrdID origClOrdID = new OrigClOrdID(cfetsTradeProgress.getOrigClOrdId());
                    quote.set(origClOrdID);
                }

                QuoteID quoteID = new QuoteID(cfetsTradeProgress.getQuoteId());
                TransactTime time = new TransactTime(cfetsTradeProgress.getTransactTime());
                quote.set(quoteID);
                quote.set(time);
            }

            //组装质押券信息
            List<CfetsTradeSubbond> subBondList = cfetsTradeBondPledge.getSubBondList();
            if(subBondList.size()>0){
                for (CfetsTradeSubbond cfetsTradeSubbond: subBondList) {
                    //债券重复组
                    Quote.NoUnderlyings noUnderlyings = new Quote.NoUnderlyings();
                    //质押券名称
                    UnderlyingSymbol underlyingSymbol = new UnderlyingSymbol(cfetsTradeSubbond.getBondName());
                    //质押券代码
                    UnderlyingSecurityID underlyingSecurityID = new UnderlyingSecurityID(cfetsTradeSubbond.getBondCode());
                    Quote.NoUnderlyings.NoUnderlyingStips noUnderlyingStips = new Quote.NoUnderlyings.NoUnderlyingStips();

                    //CFETS 报价交谈必传
                    if("CFETS".equals(cfetsTradeProgress.getChannel())){
                        if(QuoteTransType.COUNTER == quoteTransTypeChar){
                        	SendPackageRmbTradeGroup.setUnderlying(cfetsTradeSubbond, noUnderlyings,noUnderlyingStips);
                        }else {
                            //正回购必传
                        	SendPackageRmbTradeGroup.setUnderlying(cfetsTradeSubbond, noUnderlyings,noUnderlyingStips);
                        }
                    }else {
                        //正回购必传
                    	SendPackageRmbTradeGroup.setUnderlying(cfetsTradeSubbond, noUnderlyings,noUnderlyingStips);
                        quote.addGroup(noUnderlyingStips);
                    }
                    noUnderlyings.set(underlyingSymbol);
                    noUnderlyings.set(underlyingSecurityID);
                    quote.addGroup(noUnderlyings);
                }
            }
            SettlCurrency settlCurrency = new SettlCurrency(cfetsTradeBondPledge.getSettlCurrency());
            //重复组1
            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
            for(int i=0; i<partyList.size();i++) {
            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
            	Quote.NoPartyIDs noPartyIDs_s = new Quote.NoPartyIDs();
                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                noPartyIDs_s.set(partyID_s);
                noPartyIDs_s.set(partyRole_s);

                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
                	Quote.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new Quote.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid266())) {
                	Quote.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new Quote.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid266());
                     PartySubIDType partySubIDType_s = new PartySubIDType(266);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                quote.addGroup(noPartyIDs_s);
            }

            quote.set(marketIndicator);
            quote.set(quoteType);
            quote.set(quoteTransType);
            quote.set(channel);
            quote.set(repoMethod);
            quote.set(tradeLimitDays);
            quote.set(tradeCashAmt);
            quote.set(settlType);
            quote.set(deliveryType2);
            quote.set(price);
            quote.set(settlCurrency);
            quote.set(clOrdID);
            quote.set(side);
            quote.set(deliveryType);
            quote.set(clearingMethod);

        } catch (Exception e) {
        	LogManager.info("组装质押式回购对话报价回报异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装质押式回购对话报价回报异常");
            return;
        }
	}
	
	public static void packageMessage(RetBean retBean,QuoteCancel quoteCancel,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
            QuoteType quoteType = new QuoteType(Integer.valueOf(cfetsTradeProgress.getQuoteType()));
            ClOrdID clOrdID = new ClOrdID(cfetsTradeProgress.getClOrdId());
            if(StringUtils.isNotEmpty(cfetsTradeProgress.getOrigClOrdId())) {
            	OrigClOrdID origClOrdID = new OrigClOrdID(cfetsTradeProgress.getOrigClOrdId());
                quoteCancel.set(origClOrdID);
            }
            QuoteID quoteID = new QuoteID(cfetsTradeProgress.getQuoteId());
            TransactTime transactTime = new TransactTime(cfetsTradeProgress.getTransactTime());

            quoteCancel.set(quoteType);
            quoteCancel.set(clOrdID);
            quoteCancel.set(quoteID);
            quoteCancel.set(transactTime);

            String market = null;
            market = cfetsTradeProgress.getMarketindicator();
            //TODO 筛选出相应的公共部分，对不同部分进行赋值操作
            if (MarketIndicator.CASH_BOND.equals(market)) {
//                Symbol symbol = new Symbol(cfetsTradeProgress.getSymbol());
//                SecurityID securityID = new SecurityID(qct.getSecurityID());
//                MarketIndicator marketIndicator = new MarketIndicator(qct.getMarketIndicator());
//                QuoteCancel.NoQuoteEntries noQuoteEntries = new QuoteCancel.NoQuoteEntries();
//                noQuoteEntries.set(symbol);
//                noQuoteEntries.set(marketIndicator);
//                noQuoteEntries.set(securityID);
//                quoteCancel.addGroup(noQuoteEntries);
            }else if(MarketIndicator.COLLATERAL_REPO.equals(market)){

                Symbol symbol = new Symbol("-");
                MarketIndicator marketIndicator = new MarketIndicator(market);
                QuoteCancel.NoQuoteEntries noQuoteEntries = new QuoteCancel.NoQuoteEntries();
                noQuoteEntries.set(symbol);
                noQuoteEntries.set(marketIndicator);
                quoteCancel.addGroup(noQuoteEntries);

                //重复组1
                List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
                for(int i=0; i<partyList.size();i++) {
                	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
                	Quote.NoPartyIDs noPartyIDs_s = new Quote.NoPartyIDs();
                    PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                    PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                    noPartyIDs_s.set(partyID_s);
                    noPartyIDs_s.set(partyRole_s);

                    if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
                    	Quote.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new Quote.NoPartyIDs.NoPartySubIDs();
                    	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
                         PartySubIDType partySubIDType_s = new PartySubIDType(2);
                         noPartySubIDs_s.set(partySubID_s);
                         noPartySubIDs_s.set(partySubIDType_s);
                         noPartyIDs_s.addGroup(noPartySubIDs_s);
                    }
                    if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid266())) {
                    	Quote.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new Quote.NoPartyIDs.NoPartySubIDs();
                    	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid266());
                         PartySubIDType partySubIDType_s = new PartySubIDType(266);
                         noPartySubIDs_s.set(partySubID_s);
                         noPartySubIDs_s.set(partySubIDType_s);
                         noPartyIDs_s.addGroup(noPartySubIDs_s);
                    }
                    quoteCancel.addGroup(noPartyIDs_s);
                }
            }

        } catch (Exception e) {
        	LogManager.info("组装对话报价撤销报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装对话报价撤销报文异常");
            return;
        }
	}
	
	public static void packageMessage(RetBean retBean,QuoteResponse quoteResponse,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		//公共信息
		int quoteRespTypeInt = -1;
        try {
            MarketIndicator marketIndicator = new MarketIndicator(cfetsTradeProgress.getMarketindicator());
            QuoteType quoteType = new QuoteType(Integer.valueOf(cfetsTradeProgress.getQuoteType()));
            quoteRespTypeInt = Integer.valueOf(cfetsTradeProgress.getQuoteTransType());
            QuoteRespType quoteRespType = new QuoteRespType(quoteRespTypeInt);
            ClOrdID clOrdID = new ClOrdID(cfetsTradeProgress.getClOrdId());
            QuoteID quoteID = new QuoteID(cfetsTradeProgress.getQuoteId());
            TransactTime transactTime = new TransactTime(cfetsTradeProgress.getTransactTime());

            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
            for(int i=0; i<partyList.size();i++) {
            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
            	Quote.NoPartyIDs noPartyIDs_s = new Quote.NoPartyIDs();
                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                noPartyIDs_s.set(partyID_s);
                noPartyIDs_s.set(partyRole_s);

                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
                	Quote.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new Quote.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid266())) {
                	Quote.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new Quote.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid266());
                     PartySubIDType partySubIDType_s = new PartySubIDType(266);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                quoteResponse.addGroup(noPartyIDs_s);
            }

            quoteResponse.set(marketIndicator);
            quoteResponse.set(quoteType);
            quoteResponse.set(quoteRespType);
            quoteResponse.set(clOrdID);
            quoteResponse.set(quoteID);
            quoteResponse.set(transactTime);
        } catch (Exception e) {
        	LogManager.info("组装公共对话报价确认报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装公共对话报价确认报文异常");
            return;
        }

        try{
            //现券买卖
            if(MarketIndicator.CASH_BOND.equals(cfetsTradeProgress.getMarketindicator())) {
//		                SecurityID securityID = new SecurityID(qct.getSecurityID());
//		                quoteResponse.set(securityID);
            }
            //质押回购
            else if(MarketIndicator.COLLATERAL_REPO.equals(cfetsTradeProgress.getMarketindicator())){
                //渠道聊天必传
                if("iDeal".equals(cfetsTradeProgress.getChannel())) {
                    TradeRecordID tradeRecordID = new TradeRecordID(cfetsTradeProgress.getTradeRecordid());
                    quoteResponse.set(tradeRecordID);
                }

                //成交确认
                if(quoteRespTypeInt==(QuoteRespType.HIT_LIFT)){
                    //质押券信息
                	SendPackageRmbTradeGroup.getNounderlyings(quoteResponse,cfetsTradeProgress.getCfetsTradeBondPledge());
                    Channel channel =new Channel(cfetsTradeProgress.getChannel());
                    quoteResponse.set(channel);
                } else if(quoteRespTypeInt==(QuoteRespType.END_TRADE)){
                    //报价拒绝
                	
                } else if(quoteRespTypeInt==(QuoteRespType.COUNTER)){
                    //交谈
                    Channel channel =new Channel(cfetsTradeProgress.getChannel());
                    quoteResponse.set(channel);
                    SendPackageRmbTradeGroup.getCommonTrade(quoteResponse,cfetsTradeProgress);
                    //质押券信息
                    SendPackageRmbTradeGroup.getNounderlyings(quoteResponse,cfetsTradeProgress.getCfetsTradeBondPledge());
                } else if(quoteRespTypeInt==(QuoteRespType.REPLACE)){
                    //修改
                    Channel channel =new Channel(cfetsTradeProgress.getChannel());
                    quoteResponse.set(channel);
                    SendPackageRmbTradeGroup.getCommonTrade(quoteResponse,cfetsTradeProgress);
                    //质押券信息
                    SendPackageRmbTradeGroup.getNounderlyings(quoteResponse,cfetsTradeProgress.getCfetsTradeBondPledge());
                } else if(quoteRespTypeInt==(QuoteRespType.CANCEL)){
                    //撤销
                    if(cfetsTradeProgress.getOrigClOrdId()!=null) {
                        OrigClOrdID origClOrdID = new OrigClOrdID(cfetsTradeProgress.getOrigClOrdId());
                        quoteResponse.set(origClOrdID);
                    }
                }

         }
        }catch(Exception e){
        	LogManager.info("组装质押对话报价确认报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装质押对话报价确认报文异常");
            return;
        }
	}
	
	public static void packageMessage(RetBean retBean,NewOrderSingle newOrderSingle,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			//D
			CfetsTradeBondPledge cfetsTradeBondPledge = cfetsTradeProgress.getCfetsTradeBondPledge();
			
			newOrderSingle.set(new MarketIndicator(cfetsTradeProgress.getMarketindicator()));
			newOrderSingle.set(new ClOrdID(cfetsTradeProgress.getClOrdId()));
			newOrderSingle.set(new OrdType(cfetsTradeProgress.getQuoteType().toCharArray()[0]));
			newOrderSingle.set(new SecurityID(cfetsTradeBondPledge.getSecurityId()));
			newOrderSingle.set(new Side(cfetsTradeBondPledge.getSide().toCharArray()[0]));
			newOrderSingle.set(new RepoMethod(cfetsTradeBondPledge.getRepoMethod().toCharArray()[0]));
			newOrderSingle.set(new Price(cfetsTradeBondPledge.getPrice().toString()));
			newOrderSingle.set(new OrderQty(cfetsTradeBondPledge.getTradeCashAmt().toString()));
			
			//组装质押券信息
            List<CfetsTradeSubbond> subBondList = cfetsTradeBondPledge.getSubBondList();
            if(subBondList.size()>0){
                for (CfetsTradeSubbond cfetsTradeSubbond: subBondList) {
                    //债券重复组
                	NewOrderSingle.NoUnderlyings noUnderlyings = new NewOrderSingle.NoUnderlyings();
                    //质押券名称
                    UnderlyingSymbol underlyingSymbol = new UnderlyingSymbol(cfetsTradeSubbond.getBondName());
                    //质押券代码
                    UnderlyingSecurityID underlyingSecurityID = new UnderlyingSecurityID(cfetsTradeSubbond.getBondCode());
                    NewOrderSingle.NoUnderlyings.NoUnderlyingStips noUnderlyingStips = new NewOrderSingle.NoUnderlyings.NoUnderlyingStips();

                    SendPackageRmbTradeGroup.setUnderlying(cfetsTradeSubbond, noUnderlyings,noUnderlyingStips);
                    
                    noUnderlyings.set(underlyingSymbol);
                    noUnderlyings.set(underlyingSecurityID);
                    newOrderSingle.addGroup(noUnderlyings);
                }
            }
            
            //TODO CfetsTradeFloorDetails
            //重复组1
            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
            for(int i=0; i<partyList.size();i++) {
            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
            	NewOrderSingle.NoPartyIDs noPartyIDs_s = new NewOrderSingle.NoPartyIDs();
                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                noPartyIDs_s.set(partyID_s);
                noPartyIDs_s.set(partyRole_s);

                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
                	NewOrderSingle.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new NewOrderSingle.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid266())) {
                	NewOrderSingle.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new NewOrderSingle.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid266());
                     PartySubIDType partySubIDType_s = new PartySubIDType(266);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                newOrderSingle.addGroup(noPartyIDs_s);
            } 
            
		}catch(Exception e){
        	LogManager.info("组装D报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装D报文异常");
            return;
        }
	}
	public static void packageMessage(RetBean retBean,CollateralAssignment collateralAssignment,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			//AY
			CfetsTradeBondPledge cfetsTradeBondPledge = cfetsTradeProgress.getCfetsTradeBondPledge();
			
			collateralAssignment.set(new MarketIndicator(cfetsTradeProgress.getMarketindicator()));
			collateralAssignment.set(new ClOrdID(cfetsTradeProgress.getClOrdId()));
			collateralAssignment.set(new RepoMethod(cfetsTradeBondPledge.getRepoMethod().toCharArray()[0]));
			collateralAssignment.set(new TradeRecordID(cfetsTradeProgress.getTradeRecordid()));
			
			//组装质押券信息
            List<CfetsTradeSubbond> subBondList = cfetsTradeBondPledge.getSubBondList();
            if(subBondList.size()>0){
                for (CfetsTradeSubbond cfetsTradeSubbond: subBondList) {
                    //债券重复组
                	CollateralAssignment.NoUnderlyings noUnderlyings = new CollateralAssignment.NoUnderlyings();
                    //质押券名称
                    UnderlyingSymbol underlyingSymbol = new UnderlyingSymbol(cfetsTradeSubbond.getBondName());
                    //质押券代码
                    UnderlyingSecurityID underlyingSecurityID = new UnderlyingSecurityID(cfetsTradeSubbond.getBondCode());
                    CollateralAssignment.NoUnderlyings.NoUnderlyingStips noUnderlyingStips = new CollateralAssignment.NoUnderlyings.NoUnderlyingStips();

                    SendPackageRmbTradeGroup.setUnderlying(cfetsTradeSubbond, noUnderlyings,noUnderlyingStips);
                    
                    noUnderlyings.set(underlyingSymbol);
                    noUnderlyings.set(underlyingSecurityID);
                    collateralAssignment.addGroup(noUnderlyings);
                }
            }
            
            //TODO CfetsTradeFloorDetails
            //重复组1
            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
            for(int i=0; i<partyList.size();i++) {
            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
            	CollateralAssignment.NoPartyIDs noPartyIDs_s = new CollateralAssignment.NoPartyIDs();
                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                noPartyIDs_s.set(partyID_s);
                noPartyIDs_s.set(partyRole_s);

                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
                	CollateralAssignment.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new CollateralAssignment.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid266())) {
                	CollateralAssignment.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new CollateralAssignment.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid266());
                     PartySubIDType partySubIDType_s = new PartySubIDType(266);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                collateralAssignment.addGroup(noPartyIDs_s);
            } 
            
		}catch(Exception e){
        	LogManager.info("组装AY报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装AY报文异常");
            return;
        }
	}
	public static void packageMessage(RetBean retBean,OrderCancelRequest orderCancelRequest,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			//F
			CfetsTradeBondPledge cfetsTradeBondPledge = cfetsTradeProgress.getCfetsTradeBondPledge();
			
			orderCancelRequest.set(new MarketIndicator(cfetsTradeProgress.getMarketindicator()));
			orderCancelRequest.set(new ClOrdID(cfetsTradeProgress.getClOrdId()));
			orderCancelRequest.set(new RepoMethod(cfetsTradeBondPledge.getRepoMethod().toCharArray()[0]));
			orderCancelRequest.set(new OrderID(cfetsTradeProgress.getQuoteId()));
			
            
            //TODO CfetsTradeFloorDetails
            //重复组1
            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
            for(int i=0; i<partyList.size();i++) {
            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
            	OrderCancelRequest.NoPartyIDs noPartyIDs_s = new OrderCancelRequest.NoPartyIDs();
                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                noPartyIDs_s.set(partyID_s);
                noPartyIDs_s.set(partyRole_s);

                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
                	OrderCancelRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new OrderCancelRequest.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid266())) {
                	OrderCancelRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new OrderCancelRequest.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid266());
                     PartySubIDType partySubIDType_s = new PartySubIDType(266);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                orderCancelRequest.addGroup(noPartyIDs_s);
            } 
            
		}catch(Exception e){
        	LogManager.info("组装F报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装F报文异常");
            return;
        }
	}
	
	public static void packageMessage(RetBean retBean,QuoteRequest quoteRequest,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			//R
			quoteRequest.set(new ClOrdID(cfetsTradeProgress.getClOrdId()));
			quoteRequest.set(new QueryRequestID(cfetsTradeProgress.getClOrdId()));
			quoteRequest.set(new AllocInd(cfetsTradeProgress.getClOrdId()));
			quoteRequest.set(new QuoteReqID(cfetsTradeProgress.getClOrdId()));
			quoteRequest.set(new Reference(cfetsTradeProgress.getClOrdId()));
			
			
			List<CfetsTradeBond> bondList = cfetsTradeProgress.getBondList();
			if(bondList!=null && bondList.size()==1) {
				QuoteRequest.NoRelatedSym noRelatedSym = new QuoteRequest.NoRelatedSym();
				noRelatedSym.set(new Symbol("-"));
				noRelatedSym.set(new MarketIndicator(cfetsTradeProgress.getMarketindicator()));
				noRelatedSym.set(new SecurityID(bondList.get(0).getSecurityId()));
				noRelatedSym.set(new QuoteType(Integer.valueOf(cfetsTradeProgress.getQuoteType())));
				noRelatedSym.set(new QuoteStatus(Integer.valueOf(cfetsTradeProgress.getQuoteStatus())));
				noRelatedSym.set(new ValidUntilTime(cfetsTradeProgress.getValidUntilTime()));
				noRelatedSym.set(new TransactTime(cfetsTradeProgress.getTransactTime()));
				noRelatedSym.set(new Side(bondList.get(0).getSide().toCharArray()[0]));
				noRelatedSym.set(new AccruedInterestAmt(bondList.get(0).getAccruedInterestAmt().toString()));
				noRelatedSym.set(new AccruedInterestTotalAmt(bondList.get(0).getAccruedInterestTotalamt().toString()));
				noRelatedSym.set(new OrderQty(bondList.get(0).getOrderQty().toString()));
				noRelatedSym.set(new DeliveryType(Integer.valueOf(bondList.get(0).getDeliveryType())));
				noRelatedSym.set(new ClearingMethod(Integer.valueOf(bondList.get(0).getClearingMethod())));
				noRelatedSym.set(new SettlType(bondList.get(0).getSettType()));
				noRelatedSym.set(new SettlDate(bondList.get(0).getSettlDate()));
				noRelatedSym.set(new Principal(bondList.get(0).getPrincipal().toString()));
				noRelatedSym.set(new TotalPrincipal(bondList.get(0).getTotalPrincipal().toString()));
				
				quoteRequest.addGroup(noRelatedSym);
			}
			//重复组1
            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
            for(int i=0; i<partyList.size();i++) {
            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
            	MarketDataRequest.NoPartyIDs noPartyIDs_s = new MarketDataRequest.NoPartyIDs();
                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
                noPartyIDs_s.set(partyID_s);
                noPartyIDs_s.set(partyRole_s);

                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
                	MarketDataRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new MarketDataRequest.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid29())) {
                	MarketDataRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new MarketDataRequest.NoPartyIDs.NoPartySubIDs();
                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid29());
                     PartySubIDType partySubIDType_s = new PartySubIDType(29);
                     noPartySubIDs_s.set(partySubID_s);
                     noPartySubIDs_s.set(partySubIDType_s);
                     noPartyIDs_s.addGroup(noPartySubIDs_s);
                }
                quoteRequest.addGroup(noPartyIDs_s);
            } 
			
		}catch(Exception e){
        	LogManager.info("组装R报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装R报文异常");
            return;
        }
	}
	public static void packageMessage(RetBean retBean,QuoteRequestCancel quoteRequestCancel,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			//U32
			quoteRequestCancel.set(new ClOrdID(cfetsTradeProgress.getClOrdId()));
			quoteRequestCancel.set(new OrigClOrdID(cfetsTradeProgress.getOrigClOrdId()));
			quoteRequestCancel.set(new QuoteReqID(cfetsTradeProgress.getQuoteReqId()));
			
			QuotReqCxlGrp quotReqCxlGrp = new QuotReqCxlGrp();
			List<CfetsTradeBond> bondList = cfetsTradeProgress.getBondList();
			if(bondList!=null && bondList.size()==1) {
				QuotReqCxlGrp.NoRelatedSym noRelatedSym = new QuotReqCxlGrp.NoRelatedSym();
				noRelatedSym.set(new Symbol("-"));
				noRelatedSym.set(new SecurityID(bondList.get(0).getSecurityId()));
				noRelatedSym.set(new MarketIndicator(cfetsTradeProgress.getMarketindicator()));
				noRelatedSym.set(new QuoteType(Integer.valueOf(cfetsTradeProgress.getQuoteType())));
				noRelatedSym.set(new QuoteStatus(Integer.valueOf(cfetsTradeProgress.getQuoteStatus())));
				noRelatedSym.set(new TransactTime(cfetsTradeProgress.getTransactTime()));
				noRelatedSym.set(new Price(bondList.get(0).getPrice().toString()));
				//重复组1
	            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
	            for(int i=0; i<partyList.size();i++) {
	            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
	            	MarketDataRequest.NoPartyIDs noPartyIDs_s = new MarketDataRequest.NoPartyIDs();
	                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
	                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
	                noPartyIDs_s.set(partyID_s);
	                noPartyIDs_s.set(partyRole_s);

	                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
	                	MarketDataRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new MarketDataRequest.NoPartyIDs.NoPartySubIDs();
	                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
	                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
	                     noPartySubIDs_s.set(partySubID_s);
	                     noPartySubIDs_s.set(partySubIDType_s);
	                     noPartyIDs_s.addGroup(noPartySubIDs_s);
	                }
	                noRelatedSym.addGroup(noPartyIDs_s);
	            } 
	            quotReqCxlGrp.addGroup(noRelatedSym);
				quoteRequestCancel.set(quotReqCxlGrp);
			}
			
			
		}catch(Exception e){
        	LogManager.info("组装U32报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装U32报文异常");
            return;
        }
	}
	public static void packageMessage(RetBean retBean,QuoteRequestReject quoteRequestReject,CfetsTradeProgress cfetsTradeProgress) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		try {
			//AG
			quoteRequestReject.set(new QuoteRequestRejectType(cfetsTradeProgress.getQuoteStatus()));
			quoteRequestReject.set(new ClOrdID(cfetsTradeProgress.getClOrdId()));
			quoteRequestReject.set(new QuoteReqID(cfetsTradeProgress.getQuoteReqId()));
			
			List<CfetsTradeBond> bondList = cfetsTradeProgress.getBondList();
			if(bondList!=null && bondList.size()==1) {
				QuotReqCxlGrp.NoRelatedSym noRelatedSym = new QuotReqCxlGrp.NoRelatedSym();
				noRelatedSym.set(new Symbol("-"));
				noRelatedSym.set(new SecurityID(bondList.get(0).getSecurityId()));
				noRelatedSym.set(new MarketIndicator(cfetsTradeProgress.getMarketindicator()));
				noRelatedSym.set(new QuoteType(Integer.valueOf(cfetsTradeProgress.getQuoteType())));
				noRelatedSym.set(new TransactTime(cfetsTradeProgress.getTransactTime()));
				noRelatedSym.set(new Side(bondList.get(0).getSide().toCharArray()[0]));
				//重复组1
	            List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
	            for(int i=0; i<partyList.size();i++) {
	            	CfetsTradeParty  cfetsTradeParty = partyList.get(i);
	            	MarketDataRequest.NoPartyIDs noPartyIDs_s = new MarketDataRequest.NoPartyIDs();
	                PartyID partyID_s = new PartyID(cfetsTradeParty.getPartyId());
	                PartyRole partyRole_s = new PartyRole(Integer.valueOf(cfetsTradeParty.getPartyRole()));
	                noPartyIDs_s.set(partyID_s);
	                noPartyIDs_s.set(partyRole_s);

	                if(StringUtils.isNotEmpty(cfetsTradeParty.getPartySubid2())) {
	                	MarketDataRequest.NoPartyIDs.NoPartySubIDs noPartySubIDs_s = new MarketDataRequest.NoPartyIDs.NoPartySubIDs();
	                	 PartySubID partySubID_s = new PartySubID(cfetsTradeParty.getPartySubid2());
	                     PartySubIDType partySubIDType_s = new PartySubIDType(2);
	                     noPartySubIDs_s.set(partySubID_s);
	                     noPartySubIDs_s.set(partySubIDType_s);
	                     noPartyIDs_s.addGroup(noPartySubIDs_s);
	                }
	                noRelatedSym.addGroup(noPartyIDs_s);
	            } 
	            quoteRequestReject.addGroup(noRelatedSym);
			}
		}catch(Exception e){
        	LogManager.info("组装AG报文异常:",e);
        	retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("组装AG报文异常");
            return;
        }
	}
}
