package com.singlee.financial.cfets.rmb.trade.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeCommonOther;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeStatus;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMarketData;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMdEntry;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeBondPledge;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeSubbond;
import com.singlee.financial.cfets.rmb.trade.utils.ConvertRmbTrade;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.imix10.Reject;
import imix.imix20.BusinessMessageReject;
import imix.imix20.CollateralAssignment;
import imix.imix20.CollateralResponse;
import imix.imix20.ExecutionReport;
import imix.imix20.ListMarketDataAck;
import imix.imix20.MarketDataSnapshotFullRefresh;
import imix.imix20.NewOrderSingle;
import imix.imix20.OrderCancelReject;
import imix.imix20.OrderCancelRequest;
import imix.imix20.QueryResult;
import imix.imix20.Quote;
import imix.imix20.QuoteCancel;
import imix.imix20.QuoteRequest;
import imix.imix20.QuoteRequestAck;
import imix.imix20.QuoteRequestCancel;
import imix.imix20.QuoteRequestReject;
import imix.imix20.QuoteResponse;
import imix.imix20.QuoteStatusReport;
import imix.imix20.TradingSessionStatus;


/**
 * 入口
 * 上行接口：解析交易中心发送的报文
 * 将报文对象转成落地对象
 * @author xuqq
 *
 */
public class RecAAnalysisRmbTrade {
	/**
	 * 回购市场
	 * 对话报价：Quote,QuoteStatusReport,QuoteResponse,QuoteCancel,ExecutionReport
	 * 请求报价提券：Quote,QuoteStatusReport,QuoteCancel,QuoteResponse,ExecutionReport
	 * 匿名点击：NewOrderSingle，OrderCancelRequest，OrderCancelReject，CollateralAssignment，
	 * 			CollateralResponse，ExecutionReport
	 */
	
	private static Logger LogManager = LoggerFactory.getLogger("RecAnalysis");
	
	/**
	 * 判断重载方法应该传入的参数
	 * @return
	 */
	public static String getFunctionMark(Message message) {
		String result="";
		try {
			if(message==null) {
				result="XXX";
			}else if(message instanceof TradingSessionStatus) {
				//TradingSessionStatus:交易时段
				result="CfetsTradeStatus";
			}else if(message instanceof QueryResult || message instanceof BusinessMessageReject
					|| message instanceof Reject || message instanceof OrderCancelReject
					|| message instanceof CollateralResponse) {
				//QueryResult:报价查询反馈无结果
				//BusinessMessageReject:失败反馈
				//Reject:失败反馈
				//OrderCancelReject:订单撤销失败反馈
				//CollateralResponse:提交质押券失败反馈
				result="CfetsTradeCommonOther";
			}else if(message instanceof ListMarketDataAck || message instanceof MarketDataSnapshotFullRefresh ) {
				//ListMarketDataAck:报价行情订阅反馈
				//MarketDataSnapshotFullRefresh:报价行情推送
				result="CfetsTradeMarketData";
			}else {
				//交易
				result="CfetsTradeProgress";
			}
		} catch (Exception e) {
			LogManager.error("RecAAnalysisRmbTrade.getFunctionMark异常:",e);
		}
		return result;
	} 
	
	/**
	 * 解析报文总方法：转换报文类型
	 * @param quoteReport:返回对象
	 * @param message：传入报文参数
	 * @return 反馈信息
	 * @throws FieldNotFound
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	public static RetBean analysisMessage(CfetsTradePackage cfetsTradePackage,CfetsTradeProgress cfetsTradeProgress,Message message) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		RetBean retBean = new RetBean(RetStatusEnum.S,ConvertRmbTrade.retSeccess , "解析成功！");
		LogManager.info("===========解析报文开始RecAAnalysisRmbTrade.analysisMessage===============");
		//解析报文类型
		RecAnalysisCfetsPackMsg.analysisCfetsPackMsg(retBean, cfetsTradePackage, message);
		LogManager.info("===========analysisCfetsPackMsg对象解析完成retBean=["+retBean.getRetMsg()+"]");
		
		if(message instanceof Quote) {
			//接口Quote报文：使用场景：询价方(会员)发送给交易中心，交易中心发送给回复方(会员：对手方)
	    	//报文类型：S
	    	//回购市场：发送(询价方)新增报价、修改报价、交谈报价、报价接收及报价查询反馈
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(Quote)message);
			LogManager.info("===========Quote->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else if(message instanceof QuoteStatusReport) {
			//接口QuoteStatusReport报文：使用场景：接收报价的状态，交易中心发送给会员双方。
	    	//报文类型：AI
	    	//回购市场：反馈报价的状态及发送报价撤销、拒绝的通知
			
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(QuoteStatusReport)message);
			LogManager.info("===========QuoteStatusReport->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else if(message instanceof QuoteResponse) {
			//接口QuoteResponse报文：
	    	//报文类型：AJ
	    	//回购市场：交易双方确认成交、拒绝成交。
				//回复方报价交谈、修改报价交谈、撤销报价交谈。
				//询价方接收回复方发送的报价通知。	
				//疑问：询价方收到对方的报价修改，再修改用Quote还是QuoteResponse？
			
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(QuoteResponse)message);
			LogManager.info("===========QuoteResponse->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else if(message instanceof QuoteCancel) {
			//接口QuoteCancel报文：
	    	//报文类型：Z
	    	//回购市场：询价方撤销报价或撤销报价交谈。
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(QuoteCancel)message);
			LogManager.info("===========QuoteResponse->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else if(message instanceof ExecutionReport) {
			//接口ExecutionReport报文：
	    	//报文类型：8
	    	//回购市场：传送成交通知
	    		//匿名点击：订单提交反馈，订单撤销成功反馈，订单状态变化推送，成交意向通知，成交通知
			
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(ExecutionReport)message);
			LogManager.info("===========ExecutionReport->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else if(message instanceof NewOrderSingle) {
			//接口NewOrderSingle报文：匿名点击
	    	//报文类型：D
	    	//回购市场：提交订单
			
		}else if(message instanceof OrderCancelRequest) {
			//接口OrderCancelRequest报文：匿名点击
	    	//报文类型：F
	    	//回购市场：订单撤销
			
		}else if(message instanceof CollateralAssignment) {
			//接口CollateralAssignment报文：匿名点击
	    	//报文类型：AY
	    	//回购市场：成交后提交质押券
			
		}else if(message instanceof QuoteRequest) {
			//接口QuoteRequest报文：
	    	//报文类型：R
	    	//首次编写：现券买卖请求报价
			
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(QuoteRequest)message);
			LogManager.info("===========QuoteResponse->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else if(message instanceof QuoteRequestCancel) {
			//接口QuoteRequestCancel报文：
	    	//报文类型：U32
	    	//首次编写：现券买卖请求报价
			
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(QuoteRequestCancel)message);
			LogManager.info("===========QuoteResponse->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else if(message instanceof QuoteRequestAck) {
			//接口QuoteRequestAck报文：
	    	//报文类型：U29
	    	//首次编写：现券买卖请求报价
			
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(QuoteRequestAck)message);
			LogManager.info("===========QuoteResponse->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else if(message instanceof QuoteRequestReject) {
			//接口QuoteRequestReject报文：
	    	//报文类型：AG
	    	//首次编写：现券买卖请求报价
			
			RecAnalysisCfetsTradeProgress.analysisCfetsTradeProgress(retBean,cfetsTradeProgress,(QuoteRequestReject)message);
			LogManager.info("===========QuoteResponse->cfetsTradeProgress对象解析完成retBean=["+retBean.getRetMsg()+"]");
			
		}else{
			LogManager.info("=========不解析报文类型RecAAnalysisRmbTrade.analysisMessage==========");
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("此报文类型不解析");
		}
		//打印解析后的对象
		outPutObj(cfetsTradePackage,cfetsTradeProgress);
		return retBean;
	}
	
	public static RetBean analysisMessage(CfetsTradePackage cfetsTradePackage,CfetsTradeMarketData cfetsTradeMarketData,Message message) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		RetBean retBean = new RetBean(RetStatusEnum.S,ConvertRmbTrade.retSeccess , "解析成功！");
		LogManager.info("===========解析报文开始RecAAnalysisRmbTrade.CfetsTradeMarketData===============");
		//解析报文类型
		RecAnalysisCfetsPackMsg.analysisCfetsPackMsg(retBean, cfetsTradePackage, message);
		LogManager.info("===========analysisCfetsPackMsg对象解析完成retBean=["+retBean.getRetMsg()+"]");
		if(message instanceof ListMarketDataAck) {
			RecAnalysisCfetsTradeMarketData.analysisCfetsTradeMarketData(retBean,cfetsTradeMarketData,(ListMarketDataAck)message);
			LogManager.info("===========ListMarketDataAck->cfetsTradeMarketData对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else if(message instanceof MarketDataSnapshotFullRefresh){
			RecAnalysisCfetsTradeMarketData.analysisCfetsTradeMarketData(retBean,cfetsTradeMarketData,(MarketDataSnapshotFullRefresh)message);
			LogManager.info("===========MarketDataSnapshotFullRefresh->cfetsTradeMarketData对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else {
			LogManager.info("=========不解析报文类型RecAAnalysisRmbTrade.analysisMessage==========");
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("此报文类型不解析");
		}
		
		//打印解析后的对象
		outPutObj(cfetsTradePackage,cfetsTradeMarketData);
		
		return retBean;
	}
	public static RetBean analysisMessage(CfetsTradePackage cfetsTradePackage,CfetsTradeStatus cfetsTradeStatus,Message message) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		RetBean retBean = new RetBean(RetStatusEnum.S,ConvertRmbTrade.retSeccess , "解析成功！");
		LogManager.info("===========解析报文开始RecAAnalysisRmbTrade.CfetsTradeStatus===============");
		
		RecAnalysisCfetsPackMsg.analysisCfetsPackMsg(retBean, cfetsTradePackage, message);
		LogManager.info("===========analysisCfetsPackMsg对象解析完成retBean=["+retBean.getRetMsg()+"]");
		
		RecAnalysisCfetsTradeStatus.analysisCfetsTradeStatus(retBean,cfetsTradeStatus,(TradingSessionStatus)message);
		LogManager.info("===========ListMarketDataAck->cfetsTradeMarketData对象解析完成retBean=["+retBean.getRetMsg()+"]");
	
		//打印解析后的对象
		outPutObj(cfetsTradePackage,cfetsTradeStatus);
		
		return retBean;
	}
	
	public static RetBean analysisMessage(CfetsTradePackage cfetsTradePackage,CfetsTradeCommonOther cfetsTradeCommonOther,Message message) 
    		throws FieldNotFound,UnsupportedMessageType,IncorrectTagValue{
		RetBean retBean = new RetBean(RetStatusEnum.S,ConvertRmbTrade.retSeccess , "解析成功！");
		LogManager.info("===========解析报文开始RecAAnalysisRmbTrade.CfetsTradeCommonOther===============");
		//解析报文类型
		RecAnalysisCfetsPackMsg.analysisCfetsPackMsg(retBean, cfetsTradePackage, message);
		LogManager.info("===========analysisCfetsPackMsg对象解析完成retBean=["+retBean.getRetMsg()+"]");
		if(message instanceof QueryResult) {
			RecAnalysisCfetsTradeCommonOther.analysisCfetsTradeCommonOther(retBean,cfetsTradeCommonOther,(QueryResult)message);
			LogManager.info("===========ListMarketDataAck->cfetsTradeCommonOther对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else if(message instanceof BusinessMessageReject){
			RecAnalysisCfetsTradeCommonOther.analysisCfetsTradeCommonOther(retBean,cfetsTradeCommonOther,(BusinessMessageReject)message);
			LogManager.info("===========MarketDataSnapshotFullRefresh->cfetsTradeCommonOther对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else if(message instanceof Reject){
			RecAnalysisCfetsTradeCommonOther.analysisCfetsTradeCommonOther(retBean,cfetsTradeCommonOther,(Reject)message);
			LogManager.info("===========MarketDataSnapshotFullRefresh->cfetsTradeCommonOther对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else if(message instanceof OrderCancelReject){
			RecAnalysisCfetsTradeCommonOther.analysisCfetsTradeCommonOther(retBean,cfetsTradeCommonOther,(OrderCancelReject)message);
			LogManager.info("===========MarketDataSnapshotFullRefresh->cfetsTradeCommonOther对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else if(message instanceof CollateralResponse){
			RecAnalysisCfetsTradeCommonOther.analysisCfetsTradeCommonOther(retBean,cfetsTradeCommonOther,(CollateralResponse)message);
			LogManager.info("===========MarketDataSnapshotFullRefresh->cfetsTradeCommonOther对象解析完成retBean=["+retBean.getRetMsg()+"]");
		}else {
			LogManager.info("=========不解析报文类型RecAAnalysisRmbTrade.analysisMessage==========");
			retBean.setRetCode(ConvertRmbTrade.retError);
			retBean.setRetMsg("此报文类型不解析");
		}
		
		//打印解析后的对象
		outPutObj(cfetsTradePackage,cfetsTradeCommonOther);
		
		return retBean;
	}
	
	
	
	/**
	 * 日志输出：解析后的对象
	 * @param cfetsTradePackage
	 * @param cfetsTradeProgress
	 */
	public static void outPutObj(CfetsTradePackage cfetsTradePackage,CfetsTradeProgress cfetsTradeProgress) {
		try {
			//输出对象
			LogManager.info("========报文解析对象输出开始CfetsTradeProgress===========");
			if(cfetsTradePackage!=null) {
				LogManager.info(cfetsTradePackage.toString());
			}
			if(cfetsTradeProgress!=null) {
				LogManager.info(cfetsTradeProgress.toString());
				CfetsTradeBondPledge cfetsTradeBondPledge = cfetsTradeProgress.getCfetsTradeBondPledge();
				if(cfetsTradeBondPledge!=null) {
					LogManager.info(cfetsTradeBondPledge.toString());
					List<CfetsTradeSubbond> subList = cfetsTradeBondPledge.getSubBondList();
					if(subList!=null) {
						for(int i=0;i<subList.size();i++) {
							LogManager.info(subList.get(i).toString());
							if(i>10) {
								LogManager.info("CfetsTradeParty输出省略：..........");
								break;
							}
						}
					}
				}
				List<CfetsTradeParty> partyList = cfetsTradeProgress.getPartyList();
				if(partyList!=null) {
					for(int i=0;i<partyList.size();i++) {
						LogManager.info(partyList.get(i).toString());
						if(i>10) {
							LogManager.info("CfetsTradeParty输出省略：..........");
							break;
						}
					}
					
				}
			}
			LogManager.info("========报文解析对象输出结束CfetsTradeProgress===========");
		} catch (Exception e) {
			LogManager.error("报文解析对象输出异常：",e);
		}
	} 
	public static void outPutObj(CfetsTradePackage cfetsTradePackage,CfetsTradeCommonOther cfetsTradeCommonOther) {
		try {
			//输出对象
			LogManager.info("========报文解析对象输出开始CfetsTradeCommonOther===========");
			if(cfetsTradePackage!=null) {
				LogManager.info(cfetsTradePackage.toString());
			}
			if(cfetsTradeCommonOther!=null) {
				LogManager.info(cfetsTradeCommonOther.toString());
			}
			LogManager.info("========报文解析对象输出结束CfetsTradeCommonOther===========");
		} catch (Exception e) {
			LogManager.error("报文解析对象输出异常：",e);
		}
	}
	public static void outPutObj(CfetsTradePackage cfetsTradePackage,CfetsTradeStatus cfetsTradeStatus) {
		try {
			//输出对象
			LogManager.info("========报文解析对象输出开始CfetsTradeCommonOther===========");
			if(cfetsTradePackage!=null) {
				LogManager.info(cfetsTradePackage.toString());
			}
			if(cfetsTradeStatus!=null) {
				LogManager.info(cfetsTradeStatus.toString());
			}
			LogManager.info("========报文解析对象输出结束CfetsTradeCommonOther===========");
		} catch (Exception e) {
			LogManager.error("报文解析对象输出异常：",e);
		}
	}
	public static void outPutObj(CfetsTradePackage cfetsTradePackage,CfetsTradeMarketData cfetsTradeMarketData) {
		try {
			//输出对象
			LogManager.info("========报文解析对象输出开始CfetsTradeCommonOther===========");
			if(cfetsTradePackage!=null) {
				LogManager.info(cfetsTradePackage.toString());
			}
			if(cfetsTradeMarketData!=null) {
				LogManager.info(cfetsTradeMarketData.toString());
				List<CfetsTradeMdEntry> list = cfetsTradeMarketData.getMdEntryList();
				if(list!=null) {
					for(int i = 0;i<list.size();i++) {
						LogManager.info(list.get(i).toString());
						if(i>10) {
							LogManager.info("CfetsTradeMdEntry输出省略：..........");
							break;
						}
					}
				}
			}
			LogManager.info("========报文解析对象输出结束CfetsTradeCommonOther===========");
		} catch (Exception e) {
			LogManager.error("报文解析对象输出异常：",e);
		}
	}
}