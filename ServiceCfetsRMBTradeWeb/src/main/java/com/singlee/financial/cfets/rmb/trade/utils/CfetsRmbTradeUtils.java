package com.singlee.financial.cfets.rmb.trade.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;

/**
 * Cfets Api工具类
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class CfetsRmbTradeUtils {

	public static final String EMPTY = "";

	/**
	 * 方法已重载.去掉字符串两边的空格
	 * 
	 * @param value java.lang.String
	 * @return java.lang.String
	 * @throws Exception
	 */
	public static String trim(String value) {
		if (null == value || value.trim().length() == 0) {
			return EMPTY;
		}

		return value.trim();
	}

	/**
	 * 方法已重载.去掉字符串中所有的空格信息
	 * 
	 * @param value java.lang.String
	 * @return java.lang.String
	 * @throws Exception
	 */
	public static String trim(Object value) {
		String tmp = null;
		if (null == value) {
			return EMPTY;
		}

		if (value instanceof String) {
			tmp = (String) value;
		} else {
			tmp = value.toString();
		}

		return trim(tmp);
	}

	/**
	 * 创建任意深度的文件所在文件夹,可以用来替代直接new File(path)
	 * 
	 * @param path 文件路径
	 * @return
	 * @throws Exception
	 */
	public static File createFile(String path) throws Exception {
		File file = new File(path);
		// 寻找父目录是否存在
		File parent = new File(file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator)));
		// 如果父目录不存在，则递归寻找更上一层目录
		if (!parent.exists()) {
			createFile(parent.getPath());
			// 创建父目录
			parent.mkdirs();
		}
		return file;
	}

	/**
	 * 将字符串内容写到指定路径的文件中
	 * 
	 * @param str      文件内容
	 * @param fileName 待生成的文件路径
	 * @throws Exception
	 */
	public static String saveFile(String str, String filePath, String charset) throws Exception {
		PrintWriter writer = null;
		try {
			// 创建新文件
			createFile(filePath);
			writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filePath), charset));
			writer.write(str);
			writer.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			if (null != writer) {
				writer.close();}
		} // end try

		return filePath;
	}

	/**
	 * 将cfets配置文件中的相对路径更改为绝对路径
	 * 
	 * @param line           一行记录内容
	 * @param configFilePath 配置文件路径
	 * @param updatePathKey  待更新路径的配置项
	 * @return
	 */
	private static String updateConfigKey(String line, String configFilePath, String updatePathKey) {
		String equals = "=";
		for (String key : updatePathKey.split("\\|")) {
			if (line.contains(key + equals)) {
				String value = line.substring(line.indexOf(equals) + 1, line.length());
//				log.debug("updateConfigKeyAndValue:"+key+equals+configFilePath+value);
				System.out.println("updateConfigKeyAndValue:" + key + equals + configFilePath + value);
				return key + equals + configFilePath + value;
			}
		}
		return line;
	}

	/**
	 * 将更改为绝对路径的记录保存到文件中
	 * 
	 * @param updatePathKey
	 * @param configFilePath
	 * @param cfgClientPath
	 * @param charset
	 * @return
	 * @throws Exception
	 */
	public static String updateConfigFile(String updatePathKey, String configFilePath, String cfgClientPath,
			String charset) throws Exception {
		BufferedReader reader = null;
		PrintWriter writer = null;
		File cfetsConfigPath = new File(configFilePath + cfgClientPath + ".tmp");
		try {
			StringBuffer sbf = new StringBuffer();
			String path = configFilePath + cfgClientPath;
			// log.debug("read classes config path:" + path);
			System.out.println("read classes config path:" + path);
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), charset));
			String line = null;
			while ((line = reader.readLine()) != null) {
				sbf.append(updateConfigKey(line, configFilePath, updatePathKey));
				sbf.append("\r\n");
			}
			reader.close();

			writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(cfetsConfigPath), charset));
			writer.write(sbf.toString());
			writer.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			if (reader != null) {
				reader.close();}
			if (writer != null) {
				writer.close();}
		}
		return cfetsConfigPath.getAbsolutePath().replace("\\", "/");
	}

	/**
	 * 将更改为绝对路径的记录保存到文件中
	 *
	 * @param updatePathKey
	 * @param configFilePath
	 * @param cfgClientPath
	 * @param charset
	 * @param ttfttp
	 * @return
	 * @throws Exception
	 */
	public static String updateConfigFile(String updatePathKey, String configFilePath, String cfgClientPath,
			String charset, String ttfttp) throws Exception {
		BufferedReader reader = null;
		PrintWriter writer = null;
		File cfetsConfigPath = new File(configFilePath + cfgClientPath + ".tmp");
		try {
			StringBuffer sbf = new StringBuffer();
			String path = configFilePath + cfgClientPath;
//			log.debug("read classes config path:" + path);
			System.out.println("read classes config path:" + path);
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path)), charset));
			String line = null;
			while ((line = reader.readLine()) != null) {
				sbf.append(updateConfigKey(line, configFilePath, updatePathKey));
				sbf.append("\r\n");
				if (StringUtils.equals("[default]", line)) {// 追加 RelatedToFTPPath属性
					sbf.append(ttfttp);
					sbf.append("\r\n");
				}
			}
			reader.close();

			writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(cfetsConfigPath), charset));
			writer.write(sbf.toString());
			writer.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			if (reader != null) {
				reader.close();}
			if (writer != null) {
				writer.close();}
		}
		return cfetsConfigPath.getAbsolutePath().replace("\\", "/");
	}
}
