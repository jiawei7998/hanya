
-- Drop
DROP SEQUENCE SEQ_FLOW_COMMON_TABLEID;--删除序列

drop table CFETS_TRADE_STATUS;
drop table CFETS_TRADE_COMMON_OTHER;
drop table CFETS_TRADE_PACKAGE;
drop table CFETS_TRADE_PROGRESS;
drop table CFETS_TRADE_PARTY;
drop table CFETS_TRADE_FLOOR_DETAILS;
drop table CFETS_TRADE_MARKET_DATA;
drop table CFETS_TRADE_MD_ENTRY;
drop table CFETS_TRADE_ALLOCS;
drop table CFETS_TRADE_PLEDGE;
drop table CFETS_TRADE_SUBBOND;
drop table CFETS_TRADE_BOND;

--序列
-- Create sequence 
create sequence SEQ_FLOW_COMMON_TABLEID
minvalue 1
maxvalue 999999
start with 21
increment by 1
cache 20
cycle;

create table CFETS_TRADE_ALLOCS
(
  table_id           VARCHAR2(32) not null,
  table_cid          VARCHAR2(32) not null,
  alloc_account      VARCHAR2(32),
  individual_allocid VARCHAR2(32),
  alloc_qty          NUMBER(22,4)
)
;
comment on table CFETS_TRADE_ALLOCS
  is '分账表';
comment on column CFETS_TRADE_ALLOCS.table_id
  is '主键';
comment on column CFETS_TRADE_ALLOCS.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_ALLOCS.alloc_account
  is '投资产品21位码';
comment on column CFETS_TRADE_ALLOCS.individual_allocid
  is '分账序号';
comment on column CFETS_TRADE_ALLOCS.alloc_qty
  is '分账券面总额';
alter table CFETS_TRADE_ALLOCS
  add constraint CFETS_TRADE_ALLOCS_PK primary key (TABLE_ID);


create table CFETS_TRADE_BOND
(
  table_id                  VARCHAR2(32) not null,
  table_cid                 VARCHAR2(32) not null,
  data_source               VARCHAR2(8),
  security_id               VARCHAR2(32),
  symbol                    VARCHAR2(32),
  side                      VARCHAR2(4),
  price                     NUMBER(22,4),
  dirty_price               NUMBER(22,4),
  accrued_interest_amt      NUMBER(22,2),
  order_qty                 NUMBER(22,2),
  trade_cash_amt            NUMBER(22,2),
  accrued_interest_totalamt NUMBER(22,2),
  settl_curr_amt            NUMBER(22,2),
  settl_currency            VARCHAR2(4),
  settl_curr_fxrate         NUMBER(22,6),
  delivery_type             VARCHAR2(4),
  clearing_method           VARCHAR2(4),
  sett_type                 VARCHAR2(4),
  settl_date                VARCHAR2(10),
  principal                 NUMBER(22,2),
  total_principal           NUMBER(22,2),
  yield_type                VARCHAR2(16),
  yield                     NUMBER(22,4),
  stipulation_type          VARCHAR2(16),
  stipulation_value         NUMBER(22,4),
  term_to_maturity          VARCHAR2(16),
  max_floor                 NUMBER(22,2)
)
;
comment on table CFETS_TRADE_BOND
  is '现券买卖';
comment on column CFETS_TRADE_BOND.table_id
  is '主键';
comment on column CFETS_TRADE_BOND.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_BOND.data_source
  is '报价来源';
comment on column CFETS_TRADE_BOND.security_id
  is '债券代码';
comment on column CFETS_TRADE_BOND.symbol
  is '债券名称';
comment on column CFETS_TRADE_BOND.side
  is '交易方向';
comment on column CFETS_TRADE_BOND.price
  is '净价';
comment on column CFETS_TRADE_BOND.dirty_price
  is '全价';
comment on column CFETS_TRADE_BOND.accrued_interest_amt
  is '应计利息';
comment on column CFETS_TRADE_BOND.order_qty
  is '券面总额';
comment on column CFETS_TRADE_BOND.trade_cash_amt
  is '交易金额';
comment on column CFETS_TRADE_BOND.accrued_interest_totalamt
  is '应计利息总额';
comment on column CFETS_TRADE_BOND.settl_curr_amt
  is '结算金额';
comment on column CFETS_TRADE_BOND.settl_currency
  is '结算币种';
comment on column CFETS_TRADE_BOND.settl_curr_fxrate
  is '汇率';
comment on column CFETS_TRADE_BOND.delivery_type
  is '结算方式';
comment on column CFETS_TRADE_BOND.clearing_method
  is '清算类型';
comment on column CFETS_TRADE_BOND.sett_type
  is '清算速度';
comment on column CFETS_TRADE_BOND.settl_date
  is '结算日';
comment on column CFETS_TRADE_BOND.principal
  is '每百元本金额';
comment on column CFETS_TRADE_BOND.total_principal
  is '本金额';
comment on column CFETS_TRADE_BOND.yield_type
  is '到期收益率';
comment on column CFETS_TRADE_BOND.yield
  is '到期收益率值';
comment on column CFETS_TRADE_BOND.stipulation_type
  is '行权收益率';
comment on column CFETS_TRADE_BOND.stipulation_value
  is '行权收益率值';
comment on column CFETS_TRADE_BOND.term_to_maturity
  is '代偿期';
comment on column CFETS_TRADE_BOND.max_floor
  is '最大券面总额';
alter table CFETS_TRADE_BOND
  add constraint CFETS_TRADE_BOND_PK primary key (TABLE_ID);


create table CFETS_TRADE_COMMON_OTHER
(
  table_id         VARCHAR2(32) not null,
  table_rid        VARCHAR2(32),
  msg_type         VARCHAR2(8) not null,
  query_request_id VARCHAR2(32),
  transact_time    VARCHAR2(32),
  marketindicator  VARCHAR2(8),
  query_type       VARCHAR2(8),
  quote_type       VARCHAR2(8),
  cl_ord_id        VARCHAR2(32),
  quote_id         VARCHAR2(32),
  quote_request_id VARCHAR2(32),
  quote_rfq_id     VARCHAR2(32),
  tot_num_reports  VARCHAR2(2),
  appl_error_code  VARCHAR2(16),
  appl_error_desc  VARCHAR2(64)
)
;
comment on table CFETS_TRADE_COMMON_OTHER
  is '上行接口通用接收报文';
comment on column CFETS_TRADE_COMMON_OTHER.table_id
  is '主键';
comment on column CFETS_TRADE_COMMON_OTHER.table_rid
  is '外键CFETS_TRADE_PACKAGE';
comment on column CFETS_TRADE_COMMON_OTHER.msg_type
  is '报文类型';
comment on column CFETS_TRADE_COMMON_OTHER.query_request_id
  is '查询请求编号';
comment on column CFETS_TRADE_COMMON_OTHER.transact_time
  is '时间';
comment on column CFETS_TRADE_COMMON_OTHER.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_COMMON_OTHER.query_type
  is '操作类型';
comment on column CFETS_TRADE_COMMON_OTHER.quote_type
  is '报价类别';
comment on column CFETS_TRADE_COMMON_OTHER.cl_ord_id
  is '客户参考编号6';
comment on column CFETS_TRADE_COMMON_OTHER.quote_id
  is '报价编号9';
comment on column CFETS_TRADE_COMMON_OTHER.quote_request_id
  is '报价请求编号11';
comment on column CFETS_TRADE_COMMON_OTHER.quote_rfq_id
  is 'RFQ报价提券编号12';
comment on column CFETS_TRADE_COMMON_OTHER.tot_num_reports
  is '为0：标识未查到';
comment on column CFETS_TRADE_COMMON_OTHER.appl_error_code
  is '错误代码';
comment on column CFETS_TRADE_COMMON_OTHER.appl_error_desc
  is '错误原因';
alter table CFETS_TRADE_COMMON_OTHER
  add constraint CFETS_TRADE_COMMON_OTHER_PK primary key (TABLE_ID);


create table CFETS_TRADE_FLOOR_DETAILS
(
  table_id    VARCHAR2(32) not null,
  table_cid   VARCHAR2(32) not null,
  fd_id       VARCHAR2(32),
  fd_price    NUMBER(22,4),
  fd_subid253 VARCHAR2(32)
)
;
comment on table CFETS_TRADE_FLOOR_DETAILS
  is '分层回购';
comment on column CFETS_TRADE_FLOOR_DETAILS.table_id
  is '主键';
comment on column CFETS_TRADE_FLOOR_DETAILS.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_FLOOR_DETAILS.fd_subid253
  is '群组';
alter table CFETS_TRADE_FLOOR_DETAILS
  add constraint CFETS_TRADE_FLOOR_DETAILS_PK primary key (TABLE_ID);


create table CFETS_TRADE_MARKET_DATA
(
  table_id         VARCHAR2(32) not null,
  table_rid        VARCHAR2(32),
  msg_type         VARCHAR2(8),
  md_req_id        VARCHAR2(32),
  md_book_type     VARCHAR2(2),
  market_depth     NUMBER(2),
  sub_request_type VARCHAR2(2),
  sub_status       VARCHAR2(2),
  repo_method      VARCHAR2(4),
  symbol           VARCHAR2(8),
  marketindicator  VARCHAR2(8),
  appl_error_code  VARCHAR2(16),
  appl_error_desc  VARCHAR2(64),
  transact_time    VARCHAR2(32),
  security_id      VARCHAR2(32) not null
)
;
comment on table CFETS_TRADE_MARKET_DATA
  is '行情主表';
comment on column CFETS_TRADE_MARKET_DATA.table_id
  is '主键';
comment on column CFETS_TRADE_MARKET_DATA.table_rid
  is '外键CFETS_TRADE_PACKAGE';
comment on column CFETS_TRADE_MARKET_DATA.msg_type
  is '报文类型';
comment on column CFETS_TRADE_MARKET_DATA.md_req_id
  is '订阅请求ID';
comment on column CFETS_TRADE_MARKET_DATA.md_book_type
  is '行情类型';
comment on column CFETS_TRADE_MARKET_DATA.market_depth
  is '挡位信息';
comment on column CFETS_TRADE_MARKET_DATA.sub_request_type
  is '订阅标识';
comment on column CFETS_TRADE_MARKET_DATA.sub_status
  is '订阅状态';
comment on column CFETS_TRADE_MARKET_DATA.repo_method
  is '回购方式';
comment on column CFETS_TRADE_MARKET_DATA.symbol
  is '组件必须域';
comment on column CFETS_TRADE_MARKET_DATA.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_MARKET_DATA.appl_error_code
  is '错误代码';
comment on column CFETS_TRADE_MARKET_DATA.appl_error_desc
  is '错误原因';
comment on column CFETS_TRADE_MARKET_DATA.transact_time
  is '业务发生时间';
comment on column CFETS_TRADE_MARKET_DATA.security_id
  is '合约名称';
alter table CFETS_TRADE_MARKET_DATA
  add constraint CFETS_TRADE_MARKET_DATA_PK primary key (TABLE_ID);


create table CFETS_TRADE_MD_ENTRY
(
  table_id          VARCHAR2(32) not null,
  table_cid         VARCHAR2(32) not null,
  md_entry_type     VARCHAR2(2),
  md_price_level    NUMBER(2),
  md_entry_px       NUMBER(22,4),
  md_entry_size     NUMBER(22,2),
  trade_volume      NUMBER(22,2),
  sett_type         VARCHAR2(4),
  last_px           NUMBER(22,4),
  yield_type        VARCHAR2(16),
  yield             NUMBER(22,4),
  md_entry_date     VARCHAR2(10) not null,
  md_entry_time     VARCHAR2(16) not null,
  party_id          VARCHAR2(32) not null,
  party_role        VARCHAR2(4) not null,
  security_id       VARCHAR2(32),
  symbol            VARCHAR2(32),
  quote_type        VARCHAR2(8) not null,
  quote_id          VARCHAR2(32) not null,
  marketindicator   VARCHAR2(8),
  settl_date        VARCHAR2(10),
  delivery_type     VARCHAR2(4),
  settl_currency    VARCHAR2(8),
  settl_curr_fxrate NUMBER(22,6)
)
;
comment on table CFETS_TRADE_MD_ENTRY
  is '行情挡位信息';
comment on column CFETS_TRADE_MD_ENTRY.table_id
  is '主键';
comment on column CFETS_TRADE_MD_ENTRY.table_cid
  is '外键CFETS_MARKET_DATA';
comment on column CFETS_TRADE_MD_ENTRY.md_entry_type
  is '方向';
comment on column CFETS_TRADE_MD_ENTRY.md_price_level
  is '挡位';
comment on column CFETS_TRADE_MD_ENTRY.md_entry_px
  is '利率';
comment on column CFETS_TRADE_MD_ENTRY.md_entry_size
  is '可成交量';
comment on column CFETS_TRADE_MD_ENTRY.trade_volume
  is '总量';
comment on column CFETS_TRADE_MD_ENTRY.sett_type
  is '清算速度';
comment on column CFETS_TRADE_MD_ENTRY.last_px
  is '净价';
comment on column CFETS_TRADE_MD_ENTRY.yield_type
  is '到期收益率';
comment on column CFETS_TRADE_MD_ENTRY.yield
  is '到期收益率值';
comment on column CFETS_TRADE_MD_ENTRY.md_entry_date
  is '业务发生日期';
comment on column CFETS_TRADE_MD_ENTRY.md_entry_time
  is '业务发生时间';
comment on column CFETS_TRADE_MD_ENTRY.party_id
  is '交易账号';
comment on column CFETS_TRADE_MD_ENTRY.party_role
  is '账号角色';
comment on column CFETS_TRADE_MD_ENTRY.security_id
  is '债券代码';
comment on column CFETS_TRADE_MD_ENTRY.symbol
  is '债券名称';
comment on column CFETS_TRADE_MD_ENTRY.quote_type
  is '报价方式';
comment on column CFETS_TRADE_MD_ENTRY.quote_id
  is '报价编号';
comment on column CFETS_TRADE_MD_ENTRY.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_MD_ENTRY.settl_date
  is '结算日';
comment on column CFETS_TRADE_MD_ENTRY.delivery_type
  is '结算方式';
comment on column CFETS_TRADE_MD_ENTRY.settl_currency
  is '结算币种';
comment on column CFETS_TRADE_MD_ENTRY.settl_curr_fxrate
  is '汇率';
alter table CFETS_TRADE_MD_ENTRY
  add constraint CFETS_TRADE_MD_ENTRY_PK primary key (TABLE_ID);


create table CFETS_TRADE_PACKAGE
(
  table_id         VARCHAR2(32) not null,
  upordown         VARCHAR2(4) not null,
  quote_id         VARCHAR2(32),
  cl_ord_id        VARCHAR2(32),
  msg_type         VARCHAR2(8) not null,
  tradedate        VARCHAR2(32) not null,
  marketindicator  VARCHAR2(8),
  quote_type       VARCHAR2(8),
  quote_trans_type VARCHAR2(8),
  quote_status     VARCHAR2(8),
  sender_compid    VARCHAR2(32),
  sender_subid     VARCHAR2(32),
  target_compid    VARCHAR2(32),
  target_subid     VARCHAR2(32),
  begin_string     VARCHAR2(32),
  packmsg          CLOB
)
;
comment on table CFETS_TRADE_PACKAGE
  is '报文保存表';
comment on column CFETS_TRADE_PACKAGE.table_id
  is '主键';
comment on column CFETS_TRADE_PACKAGE.upordown
  is '上行或下行';
comment on column CFETS_TRADE_PACKAGE.quote_id
  is '报价编号';
comment on column CFETS_TRADE_PACKAGE.cl_ord_id
  is '客户参考编号';
comment on column CFETS_TRADE_PACKAGE.msg_type
  is '报文类型';
comment on column CFETS_TRADE_PACKAGE.tradedate
  is '成交日期';
comment on column CFETS_TRADE_PACKAGE.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_PACKAGE.quote_type
  is '报价类别';
comment on column CFETS_TRADE_PACKAGE.quote_trans_type
  is '操作类型';
comment on column CFETS_TRADE_PACKAGE.quote_status
  is '报价状态';
comment on column CFETS_TRADE_PACKAGE.begin_string
  is '报文版本';
comment on column CFETS_TRADE_PACKAGE.packmsg
  is '报文';
alter table CFETS_TRADE_PACKAGE
  add constraint CFETS_TRADE_PACKAGE_PK primary key (TABLE_ID);


create table CFETS_TRADE_PARTY
(
  table_id       VARCHAR2(32) not null,
  table_cid      VARCHAR2(32) not null,
  party_id       VARCHAR2(32) not null,
  party_role     VARCHAR2(4) not null,
  party_subid2   VARCHAR2(32),
  party_subid266 VARCHAR2(32),
  party_subid29  VARCHAR2(32),
  party_subid135 VARCHAR2(8),
  party_subid125 VARCHAR2(64),
  party_subid267 VARCHAR2(64),
  party_subid101 VARCHAR2(16)
)
;
comment on table CFETS_TRADE_PARTY
  is '交易机构信息';
comment on column CFETS_TRADE_PARTY.table_id
  is '主键';
comment on column CFETS_TRADE_PARTY.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_PARTY.party_id
  is '交易账号';
comment on column CFETS_TRADE_PARTY.party_role
  is '账号角色';
comment on column CFETS_TRADE_PARTY.party_subid2
  is '交易员ID';
comment on column CFETS_TRADE_PARTY.party_subid266
  is '交易账户6位码';
comment on column CFETS_TRADE_PARTY.party_subid29
  is '机构来源';
comment on column CFETS_TRADE_PARTY.party_subid135
  is '机构6位码';
comment on column CFETS_TRADE_PARTY.party_subid125
  is '机构中文简称';
comment on column CFETS_TRADE_PARTY.party_subid267
  is '交易账户中文简称';
comment on column CFETS_TRADE_PARTY.party_subid101
  is '交易员名称';
alter table CFETS_TRADE_PARTY
  add constraint CFETS_TRADE_PARTY_PK primary key (TABLE_ID);


create table CFETS_TRADE_PLEDGE
(
  table_id                VARCHAR2(32) not null,
  table_cid               VARCHAR2(32) not null,
  total_face_amt          NUMBER(22,4),
  total_discount_amt      NUMBER(22,2),
  trade_product           VARCHAR2(16),
  side                    VARCHAR2(4),
  repo_method             VARCHAR2(4),
  trade_limit_days        NUMBER(22),
  real_days               NUMBER(22),
  price                   NUMBER(22,4),
  benchmark_interest_rate NUMBER(22,6),
  spread                  NUMBER(22),
  trade_cash_amt          NUMBER(22,2),
  sett_type               VARCHAR2(4),
  delivery_type           VARCHAR2(4),
  delivery_type2          VARCHAR2(4),
  start_settle_date       VARCHAR2(10),
  end_settle_date         VARCHAR2(10),
  clearing_method         VARCHAR2(4),
  settl_currency          VARCHAR2(8),
  bond_currency           VARCHAR2(8),
  settl_curr_fxrate       NUMBER(22,6),
  cross_cust_instname1    VARCHAR2(4),
  cross_cust_amt1         NUMBER(22,2),
  cross_cust_instname2    VARCHAR2(4),
  cross_cust_amt2         NUMBER(22,2),
  last_px                 NUMBER(22,4),
  last_qty                NUMBER(22,2),
  leaves_qty              NUMBER(22,2),
  security_id             VARCHAR2(32) not null
)
;
comment on table CFETS_TRADE_PLEDGE
  is '质押式回购';
comment on column CFETS_TRADE_PLEDGE.table_id
  is '主键';
comment on column CFETS_TRADE_PLEDGE.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_PLEDGE.total_face_amt
  is '券面总额合计';
comment on column CFETS_TRADE_PLEDGE.total_discount_amt
  is '折算总金额';
comment on column CFETS_TRADE_PLEDGE.trade_product
  is '交易品种';
comment on column CFETS_TRADE_PLEDGE.side
  is '交易方向';
comment on column CFETS_TRADE_PLEDGE.repo_method
  is '回购方式';
comment on column CFETS_TRADE_PLEDGE.trade_limit_days
  is '回购期限';
comment on column CFETS_TRADE_PLEDGE.real_days
  is '实际占款天数';
comment on column CFETS_TRADE_PLEDGE.price
  is '回购利率';
comment on column CFETS_TRADE_PLEDGE.benchmark_interest_rate
  is '基准利率';
comment on column CFETS_TRADE_PLEDGE.spread
  is '点差';
comment on column CFETS_TRADE_PLEDGE.trade_cash_amt
  is '交易金额';
comment on column CFETS_TRADE_PLEDGE.sett_type
  is '清算速度';
comment on column CFETS_TRADE_PLEDGE.delivery_type
  is '首次结算方式';
comment on column CFETS_TRADE_PLEDGE.delivery_type2
  is '到期结算方式';
comment on column CFETS_TRADE_PLEDGE.start_settle_date
  is '首期结算日';
comment on column CFETS_TRADE_PLEDGE.end_settle_date
  is '到期结算日';
comment on column CFETS_TRADE_PLEDGE.clearing_method
  is '清算类型';
comment on column CFETS_TRADE_PLEDGE.settl_currency
  is '结算币种';
comment on column CFETS_TRADE_PLEDGE.bond_currency
  is '债券币种';
comment on column CFETS_TRADE_PLEDGE.settl_curr_fxrate
  is '汇率';
comment on column CFETS_TRADE_PLEDGE.cross_cust_instname1
  is '跨托管机构名称1';
comment on column CFETS_TRADE_PLEDGE.cross_cust_amt1
  is '跨托管交易金额1';
comment on column CFETS_TRADE_PLEDGE.cross_cust_instname2
  is '跨托管机构名称2';
comment on column CFETS_TRADE_PLEDGE.cross_cust_amt2
  is '跨托管交易金额2';
comment on column CFETS_TRADE_PLEDGE.last_px
  is '成交利率';
comment on column CFETS_TRADE_PLEDGE.last_qty
  is '成交金额';
comment on column CFETS_TRADE_PLEDGE.leaves_qty
  is '剩余金额';
comment on column CFETS_TRADE_PLEDGE.security_id
  is '合同编号';
alter table CFETS_TRADE_PLEDGE
  add constraint CFETS_TRADE_PLEDGE_PK primary key (TABLE_ID);


create table CFETS_TRADE_PROGRESS
(
  table_id               VARCHAR2(32) not null,
  table_rid              VARCHAR2(32),
  table_did              VARCHAR2(32),
  is_current             VARCHAR2(2) default 'N' not null,
  upordown               VARCHAR2(4) default 'UP' not null,
  ret_code               VARCHAR2(8),
  ret_msg                VARCHAR2(64),
  send_flag              VARCHAR2(2) default 0 not null,
  msg_type               VARCHAR2(8) not null,
  marketindicator        VARCHAR2(8),
  trade_recordid         VARCHAR2(32),
  channel                VARCHAR2(16),
  channel_type           VARCHAR2(2),
  quote_type             VARCHAR2(8),
  quote_trans_type       VARCHAR2(8),
  quote_status           VARCHAR2(8),
  quote_status_desc      VARCHAR2(8),
  cl_ord_id              VARCHAR2(32),
  query_request_id       VARCHAR2(32),
  orig_cl_ord_id         VARCHAR2(32),
  quote_req_id           VARCHAR2(32),
  quote_id               VARCHAR2(32),
  pledge_submission_code VARCHAR2(32),
  transact_time          VARCHAR2(32),
  quote_time             VARCHAR2(32),
  negotiation_count      VARCHAR2(4),
  user_reference1        VARCHAR2(64),
  user_reference2        VARCHAR2(64),
  user_reference3        VARCHAR2(64),
  user_reference4        VARCHAR2(64),
  user_reference5        VARCHAR2(64),
  user_reference6        VARCHAR2(64),
  exec_id                VARCHAR2(32),
  trade_method           VARCHAR2(4),
  exec_type              VARCHAR2(4),
  trade_date             VARCHAR2(10),
  trade_time             VARCHAR2(20),
  text                   VARCHAR2(128),
  anonymous_indicator    VARCHAR2(2),
  con_indicator          VARCHAR2(2),
  valid_until_time       VARCHAR2(32),
  routing_type           VARCHAR2(4),
  alloc_ind              VARCHAR2(2),
  date_confirmed         VARCHAR2(10)
)
;
comment on table CFETS_TRADE_PROGRESS
  is '报文解析公用表';
comment on column CFETS_TRADE_PROGRESS.table_id
  is '主键';
comment on column CFETS_TRADE_PROGRESS.table_rid
  is '外键CFETS_TRADE_PACKAGE';
comment on column CFETS_TRADE_PROGRESS.table_did
  is '业务外键FLOW_TRADE';
comment on column CFETS_TRADE_PROGRESS.is_current
  is '是否当前';
comment on column CFETS_TRADE_PROGRESS.upordown
  is '上行或下行';
comment on column CFETS_TRADE_PROGRESS.ret_code
  is '返回码';
comment on column CFETS_TRADE_PROGRESS.ret_msg
  is '提示信息';
comment on column CFETS_TRADE_PROGRESS.send_flag
  is '发送标识：0未发送，1发送成功，2发送失败，3报价成功，4报价失败，5过期';
comment on column CFETS_TRADE_PROGRESS.msg_type
  is '报文类型';
comment on column CFETS_TRADE_PROGRESS.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_PROGRESS.trade_recordid
  is '交易记录编号';
comment on column CFETS_TRADE_PROGRESS.channel
  is '渠道';
comment on column CFETS_TRADE_PROGRESS.channel_type
  is '渠道类型';
comment on column CFETS_TRADE_PROGRESS.quote_type
  is '报价类别';
comment on column CFETS_TRADE_PROGRESS.quote_trans_type
  is '操作类型';
comment on column CFETS_TRADE_PROGRESS.quote_status
  is '报价状态';
comment on column CFETS_TRADE_PROGRESS.quote_status_desc
  is '报价状态说明';
comment on column CFETS_TRADE_PROGRESS.cl_ord_id
  is '客户参考编号';
comment on column CFETS_TRADE_PROGRESS.query_request_id
  is '查询请求编号';
comment on column CFETS_TRADE_PROGRESS.orig_cl_ord_id
  is '原客户参考编号';
comment on column CFETS_TRADE_PROGRESS.quote_req_id
  is '请求报价编号';
comment on column CFETS_TRADE_PROGRESS.quote_id
  is '报价编号';
comment on column CFETS_TRADE_PROGRESS.pledge_submission_code
  is '提券编号';
comment on column CFETS_TRADE_PROGRESS.transact_time
  is '业务发生时间';
comment on column CFETS_TRADE_PROGRESS.quote_time
  is '报价发生时间';
comment on column CFETS_TRADE_PROGRESS.negotiation_count
  is '交谈轮次';
comment on column CFETS_TRADE_PROGRESS.user_reference1
  is '用户参考数据1';
comment on column CFETS_TRADE_PROGRESS.user_reference2
  is '用户参考数据2';
comment on column CFETS_TRADE_PROGRESS.user_reference3
  is '用户参考数据3';
comment on column CFETS_TRADE_PROGRESS.user_reference4
  is '用户参考数据4';
comment on column CFETS_TRADE_PROGRESS.user_reference5
  is '用户参考数据5';
comment on column CFETS_TRADE_PROGRESS.user_reference6
  is '用户参考数据6';
comment on column CFETS_TRADE_PROGRESS.exec_id
  is '成交编号';
comment on column CFETS_TRADE_PROGRESS.trade_method
  is '交易方式';
comment on column CFETS_TRADE_PROGRESS.exec_type
  is '成交状态';
comment on column CFETS_TRADE_PROGRESS.trade_date
  is '成交日期yyyymmdd';
comment on column CFETS_TRADE_PROGRESS.trade_time
  is '成交时间';
comment on column CFETS_TRADE_PROGRESS.text
  is '备注';
comment on column CFETS_TRADE_PROGRESS.anonymous_indicator
  is '匿名标识';
comment on column CFETS_TRADE_PROGRESS.con_indicator
  is '应急标识';
comment on column CFETS_TRADE_PROGRESS.valid_until_time
  is '有效时间';
comment on column CFETS_TRADE_PROGRESS.routing_type
  is '发送对象';
comment on column CFETS_TRADE_PROGRESS.alloc_ind
  is '分账标识';
comment on column CFETS_TRADE_PROGRESS.date_confirmed
  is '场次日期';
alter table CFETS_TRADE_PROGRESS
  add constraint CFETS_TRADE_PROGRESS_PK primary key (TABLE_ID);


create table CFETS_TRADE_STATUS
(
  table_id            VARCHAR2(32) not null,
  table_rid           VARCHAR2(32),
  marketindicator     VARCHAR2(8),
  trad_ses_status     VARCHAR2(4),
  trade_method        VARCHAR2(4),
  trad_ses_start_time VARCHAR2(32),
  trad_ses_open_time  VARCHAR2(32),
  trad_ses_close_time VARCHAR2(32),
  trad_ses_end_time   VARCHAR2(32)
)
;
comment on table CFETS_TRADE_STATUS
  is '交易时段落地表';
comment on column CFETS_TRADE_STATUS.table_id
  is '主键';
comment on column CFETS_TRADE_STATUS.table_rid
  is '外键CFETS_TRADE_PACKAGE';
comment on column CFETS_TRADE_STATUS.marketindicator
  is '市场标识';
comment on column CFETS_TRADE_STATUS.trad_ses_status
  is '交易时段状态';
comment on column CFETS_TRADE_STATUS.trade_method
  is '交易方式';
comment on column CFETS_TRADE_STATUS.trad_ses_start_time
  is '时间';
comment on column CFETS_TRADE_STATUS.trad_ses_open_time
  is '时间';
comment on column CFETS_TRADE_STATUS.trad_ses_close_time
  is '时间';
comment on column CFETS_TRADE_STATUS.trad_ses_end_time
  is '时间';
alter table CFETS_TRADE_STATUS
  add constraint CFETS_TRADE_STATUS_PK primary key (TABLE_ID);


create table CFETS_TRADE_SUBBOND
(
  table_id       VARCHAR2(32) not null,
  table_pid      VARCHAR2(32) not null,
  table_cid      VARCHAR2(32) not null,
  bond_code      VARCHAR2(32) not null,
  bond_name      VARCHAR2(128),
  face_amt       NUMBER(22,2),
  discount_type  VARCHAR2(16),
  discount_value NUMBER(22,2),
  discount_amt   NUMBER(22,2),
  market_amt     NUMBER(22,2)
)
;
comment on table CFETS_TRADE_SUBBOND
  is '质押券';
comment on column CFETS_TRADE_SUBBOND.table_id
  is '主键';
comment on column CFETS_TRADE_SUBBOND.table_pid
  is '外键CFETS_TRADE_PLEDGE';
comment on column CFETS_TRADE_SUBBOND.table_cid
  is '外键CFETS_TRADE_PROGRESS';
comment on column CFETS_TRADE_SUBBOND.bond_code
  is '债券编码';
comment on column CFETS_TRADE_SUBBOND.bond_name
  is '债券名称';
comment on column CFETS_TRADE_SUBBOND.face_amt
  is '券面总额';
comment on column CFETS_TRADE_SUBBOND.discount_type
  is '折算比例';
comment on column CFETS_TRADE_SUBBOND.discount_value
  is '折算比例值';
comment on column CFETS_TRADE_SUBBOND.discount_amt
  is '折算金额';
comment on column CFETS_TRADE_SUBBOND.market_amt
  is '估值净价(元)';
alter table CFETS_TRADE_SUBBOND
  add constraint CFETS_TRADE_SUBBOND_PK primary key (TABLE_ID);

