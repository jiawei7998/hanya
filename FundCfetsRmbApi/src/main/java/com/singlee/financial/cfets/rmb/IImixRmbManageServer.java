package com.singlee.financial.cfets.rmb;

import java.util.List;

import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;
import com.singlee.hessian.annotation.CfetsRmb;
import com.singlee.hessian.annotation.CfetsRmbContext;

/**
 * 拆借模块业务处理类,发送外币拆借业务对象到Capital Web资金前置系统中
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
@CfetsRmb(context = CfetsRmbContext.SINGLEE_API, uri = "/IImixRmbManageServerExporter")
public interface IImixRmbManageServer {

	/**
	 * CFETS登出
	 * 
	 * @return
	 */
	boolean startImixSession();

	/**
	 * CFETS登入
	 * 
	 * @return
	 */
	boolean stopImixSession();
	
	/**
	 * 交易确认接口
	 * 
	 * @param xml
	 * @return
	 */
	String confirmRmbTransaction(String xml);
	/**
	 * 获取没有送出去的交易
	 * @param date
	 * @return
	 */
	List<TradingFailureBean> getFailureSendByDate(String date);
	
	RetBean resendRmbRecord(TradingFailureBean bean) throws Exception;
	
	/***
	 * 获取登陆状态
	 * @return
	 */
	boolean getImixSessionState();
}
