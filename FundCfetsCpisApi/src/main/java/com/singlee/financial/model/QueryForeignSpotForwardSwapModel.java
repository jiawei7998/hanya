package com.singlee.financial.model;

import java.io.Serializable;

/**
 * <p>外汇即期远期掉期查询请求业务要素实体类 </p>
 * <P>新利信息科技 </P>
 * @author 彭前
 *
 */
public class QueryForeignSpotForwardSwapModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//成交编号
	private String execID;
	
	//交易类型  0-all; 11-外汇掉期;12-外汇即期 14-外汇远期
	private String  marketIndicator;
	
	//市场类型  0-all,会员同时查询外汇业务时:10176取值0,10315取值0;
	//查询外汇即远掉的交易,分三条报文查询 10176=11,10176=12,10176=14.不传输10315
	private String tradeInstrucment;
	
	//成交起始日期
	private String  queryStartDate;

	//成交结束日期 格式为:YYYYMMDD
	private String queryEndDate;
	
	//0--New--正常(有效);4--Canceled--撤销;102--All--全部
	private String  queryExecType;
	
	//清算方式  1-净额清算	 2-双边清算 4--净额清算+双边清算
	private String netGrossInd;
	
	//查询类型 1-确认状态 106-成交明细
	private String confirmType;

	//查询请求编号与返回的AK报文对应
	private String  queryRequestID;
	
	//查询返回起始结果数 
	private String  queryStartNumber;

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getTradeInstrucment() {
		return tradeInstrucment;
	}

	public void setTradeInstrucment(String tradeInstrucment) {
		this.tradeInstrucment = tradeInstrucment;
	}

	public String getQueryStartDate() {
		return queryStartDate;
	}

	public void setQueryStartDate(String queryStartDate) {
		this.queryStartDate = queryStartDate;
	}

	public String getQueryEndDate() {
		return queryEndDate;
	}

	public void setQueryEndDate(String queryEndDate) {
		this.queryEndDate = queryEndDate;
	}

	public String getQueryExecType() {
		return queryExecType;
	}

	public void setQueryExecType(String queryExecType) {
		this.queryExecType = queryExecType;
	}

	public String getNetGrossInd() {
		return netGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	public String getConfirmType() {
		return confirmType;
	}

	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}

	public String getQueryRequestID() {
		return queryRequestID;
	}

	public void setQueryRequestID(String queryRequestID) {
		this.queryRequestID = queryRequestID;
	}

	public String getQueryStartNumber() {
		return queryStartNumber;
	}

	public void setQueryStartNumber(String queryStartNumber) {
		this.queryStartNumber = queryStartNumber;
	}

	
}
