package com.singlee.financial.model;

import com.singlee.financial.pojo.TradeConfirmBean;

import java.io.Serializable;
import java.util.List;

/*
 *<p>掉期交易的实体类 </P> 
 *
 */
public class SendForeignSwapModel extends TradeConfirmBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//成交编号
	private String  execID;
	
	//成交日期  格式为:YYYYMMDD
	private String tradeDate;
	
	//交易场所 
	private String marketID;
	
	//确认报文ID
	private String confirmID;
	
//	//清算方式
//	private String netGrossInd;
	
//	//11-外汇掉期
//	private String marketIndictor;
	
	//交收方式
	private String deliveryType;
	
	//通过起息日区分近端远端
	private String noLegs;
	
	//B
	private String legSide;
	
	//近端起息日  格式:YYYYMMDD
	private String legSettlDate_1;
	
	//近端成交价格
	private String legLastPx_1;
	
	//近端卖出金额
	private String legCalculatedCcyLastQty_1;
	
	//近端买入金额
	private String legLastQty_1;
	
	//近端卖出币种
	private String legCurrency2_1;
	
	//近端买入币种
	private String legCurrency1_1;
	
	//远端起息日
	private String legSettlDate_2;
	
	//远端成交价格
	private String legLastPx_2;
	
	//远端买入金额
	private String legLastQty_2;
	
	//远端卖出金额
	private String legCalculatedCcyLastQty_2;
	
	//远端买入币种
	private String legCurrency1_2;
	
	//远端卖出币种
	private String legCurrency2_2;
	
	//NoPartyIDs 2
	private String noPartyIDs;
	
	//本方21位机构代码
	private String partyID_1;
	
	//本方 1
	private String partyRole_1;
	
	//对手方21位机构代码
	private String partyID_2;
	
	//对手方 17
	private String partyRole_2;
	
	//NoPartySubIds 16
	private String  noPartySubIds;
		
	private String legCurrency_1;//近端交易标的
	
	private String legCurrency_2;//远端端交易标的
	
	private String securityID;//交易品种
	
	private String legSide_1;// 近端交易方向
	
	private String legSide_2;// 远端交易方向
	
	private String partyName;//对手方中文名称
	
	private String ownName;//本方中文名称

	private String br;
	//本方机构信息集合
	private List<AgencyBasicInformationModel> list1;
	
	//对手方机构信息
	private List<AgencyBasicInformationModel> list2;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

//	public String getNetGrossInd() {
//		return netGrossInd;
//	}
//
//	public void setNetGrossInd(String netGrossInd) {
//		this.netGrossInd = netGrossInd;
//	}
//
//	public String getMarketIndictor() {
//		return marketIndictor;
//	}
//
//	public void setMarketIndictor(String marketIndictor) {
//		this.marketIndictor = marketIndictor;
//	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getNoLegs() {
		return noLegs;
	}

	public void setNoLegs(String noLegs) {
		this.noLegs = noLegs;
	}

	public String getLegSide() {
		return legSide;
	}

	public void setLegSide(String legSide) {
		this.legSide = legSide;
	}

	public String getLegSettlDate_1() {
		return legSettlDate_1;
	}

	public void setLegSettlDate_1(String legSettlDate_1) {
		this.legSettlDate_1 = legSettlDate_1;
	}

	public String getLegLastPx_1() {
		return legLastPx_1;
	}

	public void setLegLastPx_1(String legLastPx_1) {
		this.legLastPx_1 = legLastPx_1;
	}

	public String getLegCalculatedCcyLastQty_1() {
		return legCalculatedCcyLastQty_1;
	}

	public void setLegCalculatedCcyLastQty_1(String legCalculatedCcyLastQty_1) {
		this.legCalculatedCcyLastQty_1 = legCalculatedCcyLastQty_1;
	}

	public String getLegLastQty_1() {
		return legLastQty_1;
	}

	public void setLegLastQty_1(String legLastQty_1) {
		this.legLastQty_1 = legLastQty_1;
	}

	public String getLegCurrency2_1() {
		return legCurrency2_1;
	}

	public void setLegCurrency2_1(String legCurrency2_1) {
		this.legCurrency2_1 = legCurrency2_1;
	}

	public String getLegCurrency1_1() {
		return legCurrency1_1;
	}

	public void setLegCurrency1_1(String legCurrency1_1) {
		this.legCurrency1_1 = legCurrency1_1;
	}

	public String getLegSettlDate_2() {
		return legSettlDate_2;
	}

	public void setLegSettlDate_2(String legSettlDate_2) {
		this.legSettlDate_2 = legSettlDate_2;
	}

	public String getLegLastPx_2() {
		return legLastPx_2;
	}

	public void setLegLastPx_2(String legLastPx_2) {
		this.legLastPx_2 = legLastPx_2;
	}

	public String getLegLastQty_2() {
		return legLastQty_2;
	}

	public void setLegLastQty_2(String legLastQty_2) {
		this.legLastQty_2 = legLastQty_2;
	}

	public String getLegCalculatedCcyLastQty_2() {
		return legCalculatedCcyLastQty_2;
	}

	public void setLegCalculatedCcyLastQty_2(String legCalculatedCcyLastQty_2) {
		this.legCalculatedCcyLastQty_2 = legCalculatedCcyLastQty_2;
	}

	public String getLegCurrency1_2() {
		return legCurrency1_2;
	}

	public void setLegCurrency1_2(String legCurrency1_2) {
		this.legCurrency1_2 = legCurrency1_2;
	}

	public String getLegCurrency2_2() {
		return legCurrency2_2;
	}

	public void setLegCurrency2_2(String legCurrency2_2) {
		this.legCurrency2_2 = legCurrency2_2;
	}

	public String getNoPartyIDs() {
		return noPartyIDs;
	}

	public void setNoPartyIDs(String noPartyIDs) {
		this.noPartyIDs = noPartyIDs;
	}

	public String getPartyID_1() {
		return partyID_1;
	}

	public void setPartyID_1(String partyID_1) {
		this.partyID_1 = partyID_1;
	}

	public String getPartyRole_1() {
		return partyRole_1;
	}

	public void setPartyRole_1(String partyRole_1) {
		this.partyRole_1 = partyRole_1;
	}

	public String getPartyID_2() {
		return partyID_2;
	}

	public void setPartyID_2(String partyID_2) {
		this.partyID_2 = partyID_2;
	}

	public String getPartyRole_2() {
		return partyRole_2;
	}

	public void setPartyRole_2(String partyRole_2) {
		this.partyRole_2 = partyRole_2;
	}

	public String getNoPartySubIds() {
		return noPartySubIds;
	}

	public void setNoPartySubIds(String noPartySubIds) {
		this.noPartySubIds = noPartySubIds;
	}

	public List<AgencyBasicInformationModel> getList1() {
		return list1;
	}

	public void setList1(List<AgencyBasicInformationModel> list1) {
		this.list1 = list1;
	}

	public List<AgencyBasicInformationModel> getList2() {
		return list2;
	}

	public void setList2(List<AgencyBasicInformationModel> list2) {
		this.list2 = list2;
	}

	public String getLegCurrency_1() {
		return legCurrency_1;
	}

	public void setLegCurrency_1(String legCurrency_1) {
		this.legCurrency_1 = legCurrency_1;
	}

	public String getLegCurrency_2() {
		return legCurrency_2;
	}

	public void setLegCurrency_2(String legCurrency_2) {
		this.legCurrency_2 = legCurrency_2;
	}

	public String getSecurityID() {
		return securityID;
	}

	public void setSecurityID(String securityID) {
		this.securityID = securityID;
	}

	public String getLegSide_1() {
		return legSide_1;
	}

	public void setLegSide_1(String legSide_1) {
		this.legSide_1 = legSide_1;
	}

	public String getLegSide_2() {
		return legSide_2;
	}

	public void setLegSide_2(String legSide_2) {
		this.legSide_2 = legSide_2;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getOwnName() {
		return ownName;
	}

	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}


	
	
}
