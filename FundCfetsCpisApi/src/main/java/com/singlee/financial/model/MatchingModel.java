package com.singlee.financial.model;

import java.io.Serializable;

public class MatchingModel implements Serializable {
	
	/**
	 * 匹配报文查询实体类
	 * <p>新利信息科技公司 </p>
	 * @author 理世超
	 * 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String queryRequestId;//查询编号
	
	private String matchMsgSubId;//业务编号
	
	public String getMatchMsgSubId() {
		return matchMsgSubId;
	}

	public void setMatchMsgSubId(String matchMsgSubId) {
		this.matchMsgSubId = matchMsgSubId;
	}

	public String getQueryRequestId() {
		return queryRequestId;
	}

	public void setQueryRequestId(String queryRequestId) {
		this.queryRequestId = queryRequestId;
	}

}
