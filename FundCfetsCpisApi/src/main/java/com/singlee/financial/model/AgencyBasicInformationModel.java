package com.singlee.financial.model;

import java.io.Serializable;

public class AgencyBasicInformationModel implements Serializable{
	
	
	/**
	 * 机构信息实体类
	 */
	private static final long serialVersionUID = 1L;

	private String partySubID;
	
	private String partySubIDType;

	public String getPartySubID() {
		return partySubID;
	}

	public void setPartySubID(String partySubID) {
		this.partySubID = partySubID;
	}

	public String getPartySubIDType() {
		return partySubIDType;
	}

	public void setPartySubIDType(String partySubIDType) {
		this.partySubIDType = partySubIDType;
	}

	@Override
	public String toString() {
		return "AgencyBasicInformationModel [PartySubID=" + partySubID
				+ ", PartySubIDType=" + partySubIDType + "]";
	}
	
	

}
