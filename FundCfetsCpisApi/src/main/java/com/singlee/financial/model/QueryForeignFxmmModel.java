package com.singlee.financial.model;

import java.io.Serializable;

/**
 * 外汇拆借查询实体类
 * <p>新利信息科技公司 </p>
 * @author 彭前
 * 
 * 
 */
public class QueryForeignFxmmModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//查询类型 1-确认状态 106-成交明细
	private String confirmType;
	
	//成交编号
	private String execID;
	
	//清算方式  2-双边清算
	private String netGrossInd;
	
	//市场交易类型  21-外币拆借
	private String  marketIndicator;
	
	//成交结束日期 格式为:YYYYMMDD
	private String queryEndDate;
	
	//成交起始日期
	private String  queryStartDate;
	
	//查询返回起始结果数
	private String  queryStartNumber;
	
	//查询请求编号与返回的AK报文对应
	private String  queryRequestID;
	
	//成交状态 106-有效 4-撤销 102-全部
	private String  queryExecType;

	public String getConfirmType() {
		return confirmType;
	}

	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getNetGrossInd() {
		return netGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getQueryEndDate() {
		return queryEndDate;
	}

	public void setQueryEndDate(String queryEndDate) {
		this.queryEndDate = queryEndDate;
	}

	public String getQueryStartDate() {
		return queryStartDate;
	}

	public void setQueryStartDate(String queryStartDate) {
		this.queryStartDate = queryStartDate;
	}

	public String getQueryStartNumber() {
		return queryStartNumber;
	}

	public void setQueryStartNumber(String queryStartNumber) {
		this.queryStartNumber = queryStartNumber;
	}

	public String getQueryRequestID() {
		return queryRequestID;
	}

	public void setQueryRequestID(String queryRequestID) {
		this.queryRequestID = queryRequestID;
	}

	public String getQueryExecType() {
		return queryExecType;
	}

	public void setQueryExecType(String queryExecType) {
		this.queryExecType = queryExecType;
	}
	
}
