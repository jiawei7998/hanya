package com.singlee.financial.model;

import java.io.Serializable;
import java.util.List;

import com.singlee.financial.pojo.TradeConfirmBean;
/**
 * 远期
 * @author 彭前
 *
 */
public class SendForeignForwardSpotModel extends TradeConfirmBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//成交编号
	private String execID;
	
	//成交日期
	private String tradeDate;
	
	//确认报文ID
	private String confirmID;
	
	//交易场所
	private String marketID;
	
//	//清算方式  2-双边清算 1-净额清算
//	private String netGrossInd;
	
//	//交易类型
//	private String marketIndicator;
	
	//交收方式
	private String deliveryType;
	
	//起息日
	private String settlDate;
	
	//成交价格
	private String lastPx;
	
	//买入币种
	private String currency1;
	
	//买入金额
	private String lastQty;
	
	//卖出币种
	private String currency2;
	
	//卖出金额
	private String calculatedCcyLastQty;
	
	//NoPartyIDs 2
	private String noPartyIDs;
	
	//NoPartySubIDs 11
	private String noPartySubIDs;
	
	//本方21位机构代码
	private String partyID_1;
	
	//本方角色 :1
	private String partyRole_1;
	
	//对手方21位机构代码
	private String partyID_2;
	
	//对手方角色 :17
	private String partyRole_2;
	
	private String partyCFETSNO;//对手方CFETS编号
	
	private String pay_postscript;//付款方附言
	
	private String rec_postscript;//收款方附言
	
	private String currency;//交易标的
	
	private String securityID;//交易品种
	
	private String side;// 交易方向
	
	private String partyName;//对手方中文名称
	
	private String ownName;//本方中文名称
	
	private String br;
	
	//本方机构信息集合
	private List<AgencyBasicInformationModel> list1;
	
	//对手方机构信息
	private List<AgencyBasicInformationModel> list2;

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

//	public String getNetGrossInd() {
//		return netGrossInd;
//	}
//
//	public void setNetGrossInd(String netGrossInd) {
//		this.netGrossInd = netGrossInd;
//	}

//	public String getMarketIndicator() {
//		return marketIndicator;
//	}
//
//	public void setMarketIndicator(String marketIndicator) {
//		this.marketIndicator = marketIndicator;
//	}

	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getSettlDate() {
		return settlDate;
	}

	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}

	public String getLastPx() {
		return lastPx;
	}

	public void setLastPx(String lastPx) {
		this.lastPx = lastPx;
	}

	public String getCurrency1() {
		return currency1;
	}

	public void setCurrency1(String currency1) {
		this.currency1 = currency1;
	}

	public String getLastQty() {
		return lastQty;
	}

	public void setLastQty(String lastQty) {
		this.lastQty = lastQty;
	}

	public String getCurrency2() {
		return currency2;
	}

	public void setCurrency2(String currency2) {
		this.currency2 = currency2;
	}

	public String getCalculatedCcyLastQty() {
		return calculatedCcyLastQty;
	}

	public void setCalculatedCcyLastQty(String calculatedCcyLastQty) {
		this.calculatedCcyLastQty = calculatedCcyLastQty;
	}

	public String getNoPartyIDs() {
		return noPartyIDs;
	}

	public void setNoPartyIDs(String noPartyIDs) {
		this.noPartyIDs = noPartyIDs;
	}

	public String getNoPartySubIDs() {
		return noPartySubIDs;
	}

	public void setNoPartySubIDs(String noPartySubIDs) {
		this.noPartySubIDs = noPartySubIDs;
	}

	public String getPartyID_1() {
		return partyID_1;
	}

	public void setPartyID_1(String partyID_1) {
		this.partyID_1 = partyID_1;
	}

	public String getPartyRole_1() {
		return partyRole_1;
	}

	public void setPartyRole_1(String partyRole_1) {
		this.partyRole_1 = partyRole_1;
	}

	public String getPartyID_2() {
		return partyID_2;
	}

	public void setPartyID_2(String partyID_2) {
		this.partyID_2 = partyID_2;
	}

	public String getPartyRole_2() {
		return partyRole_2;
	}

	public void setPartyRole_2(String partyRole_2) {
		this.partyRole_2 = partyRole_2;
	}

	public List<AgencyBasicInformationModel> getList1() {
		return list1;
	}

	public void setList1(List<AgencyBasicInformationModel> list1) {
		this.list1 = list1;
	}

	public List<AgencyBasicInformationModel> getList2() {
		return list2;
	}

	public void setList2(List<AgencyBasicInformationModel> list2) {
		this.list2 = list2;
	}

	public String getPartyCFETSNO() {
		return partyCFETSNO;
	}

	public void setPartyCFETSNO(String partyCFETSNO) {
		this.partyCFETSNO = partyCFETSNO;
	}

	public String getPay_postscript() {
		return pay_postscript;
	}

	public void setPay_postscript(String pay_postscript) {
		this.pay_postscript = pay_postscript;
	}

	public String getRec_postscript() {
		return rec_postscript;
	}

	public void setRec_postscript(String rec_postscript) {
		this.rec_postscript = rec_postscript;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getSecurityID() {
		return securityID;
	}

	public void setSecurityID(String securityID) {
		this.securityID = securityID;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getPartyName() {
		return partyName;
	}

	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	public String getOwnName() {
		return ownName;
	}

	public void setOwnName(String ownName) {
		this.ownName = ownName;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}
	
	
}
