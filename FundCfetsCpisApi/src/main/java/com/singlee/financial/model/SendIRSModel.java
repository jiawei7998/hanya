package com.singlee.financial.model;

import java.io.Serializable;
import java.util.List;

import com.singlee.financial.pojo.TradeSettlsBean;
/**
 * IRS
 * @author 彭前
 *
 */
public class SendIRSModel extends TradeSettlsBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//报文编号
	private String confirmID;
	
	//确认成交明细 
	private String confirmType;
	
	//交易场所
	private String marketID;
	
	//成交编号
	private String execID;
	
	//名义本金 精度:2
	private String lastQty;
	
	//市场类型:2
	private String marketIndicator;
	
	//产品名称
	private String symbol;
	
	//用于填写交易后业务字段中的补充条款信息
	private String text;
	
	//起息日
	private String startDate;
	
	//成交日期
	private String tradeDate;
	
	//到期日
	private String endDate;
	
	//支付日调整
	private String couponPaymentDaateReset;
	
	//计息天数调整
	private String interestAccuralDaysAdjustment;
	
	//首期起息日
	private String firstPeriodStartDate;
	
	//期限
	private String tradeLimitDays;
	
	//计算机构
	private String calculateAgency;
	
	//清算方式
	private String netGrossInd;
	
	//交易方向
	private String side;
	
	//NoLegs 2
	private String noLegs;
	
	//交易方向
	private String legSide_1;
	
	//参考利率类型
	private String legPriceType_1;
	
	//固定利率 精度4位
	private String legPrice;
	
	//固定利率支付周期
	private String legCouponPaymentFrequency_1;
	
	//固定利率首期定期支付日
	private String legCouponPaymentDate_1;
	
	//固定利率计息基准
	private String legDayCount_1;
	
	//浮动利率端
	private String legSide_2;
	
	//参考利率类型
	private String legPriceType_2;
	
	//参考利率
	private String legBenchmarkCurveName;
	
	//利差
	private String legBenchmarkSpread;
	
	//浮动利率首期支付日
	private String legCouponPaymentDate_2;
	
	//浮动利率支付周期
	private String legCouponPaymentFrequency_2;
	
	//首次利率确定日
	private String legInterestAccrualDate;
	
	//浮动利率重置频率
	private String degInterestAccrualResetFrequency;
	
	//浮动利率计息方法
	private String  legInterestAccrualMethod;
	
	//浮动利率计息基准
	private String legDayCount_2;
	
	//NoPartySubIDs 12
	private String noPartySubIDs;
	
	//NoPartyIDs 2
	private String noPartyIDs;
	
	//本方21位机构代码
	private String partyID_1;
	
	//固定利率支付方 119
	private String partyRole_1;
	
	//对手方21位机构代码
	private String partyID_2;
	
	//固定利率收取方
	private String partyRole_2;

	//本方机构信息集合
	private List<AgencyBasicInformationModel> list1;
	
	//对手方机构信息
	private List<AgencyBasicInformationModel> list2;

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

	public String getConfirmType() {
		return confirmType;
	}

	public void setConfirmType(String confirmType) {
		this.confirmType = confirmType;
	}

	public String getMarketID() {
		return marketID;
	}

	public void setMarketID(String marketID) {
		this.marketID = marketID;
	}

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getLastQty() {
		return lastQty;
	}

	public void setLastQty(String lastQty) {
		this.lastQty = lastQty;
	}
	@Override
	public String getMarketIndicator() {
		return marketIndicator;
	}
	@Override
	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getCouponPaymentDaateReset() {
		return couponPaymentDaateReset;
	}

	public void setCouponPaymentDaateReset(String couponPaymentDaateReset) {
		this.couponPaymentDaateReset = couponPaymentDaateReset;
	}

	public String getInterestAccuralDaysAdjustment() {
		return interestAccuralDaysAdjustment;
	}

	public void setInterestAccuralDaysAdjustment(
			String interestAccuralDaysAdjustment) {
		this.interestAccuralDaysAdjustment = interestAccuralDaysAdjustment;
	}

	public String getFirstPeriodStartDate() {
		return firstPeriodStartDate;
	}

	public void setFirstPeriodStartDate(String firstPeriodStartDate) {
		this.firstPeriodStartDate = firstPeriodStartDate;
	}

	public String getTradeLimitDays() {
		return tradeLimitDays;
	}

	public void setTradeLimitDays(String tradeLimitDays) {
		this.tradeLimitDays = tradeLimitDays;
	}

	public String getCalculateAgency() {
		return calculateAgency;
	}

	public void setCalculateAgency(String calculateAgency) {
		this.calculateAgency = calculateAgency;
	}
	@Override
	public String getNetGrossInd() {
		return netGrossInd;
	}
	@Override
	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getNoLegs() {
		return noLegs;
	}

	public void setNoLegs(String noLegs) {
		this.noLegs = noLegs;
	}

	public String getLegSide_1() {
		return legSide_1;
	}

	public void setLegSide_1(String legSide_1) {
		this.legSide_1 = legSide_1;
	}

	public String getLegPriceType_1() {
		return legPriceType_1;
	}

	public void setLegPriceType_1(String legPriceType_1) {
		this.legPriceType_1 = legPriceType_1;
	}

	public String getLegPrice() {
		return legPrice;
	}

	public void setLegPrice(String legPrice) {
		this.legPrice = legPrice;
	}

	public String getLegCouponPaymentFrequency_1() {
		return legCouponPaymentFrequency_1;
	}

	public void setLegCouponPaymentFrequency_1(String legCouponPaymentFrequency_1) {
		this.legCouponPaymentFrequency_1 = legCouponPaymentFrequency_1;
	}

	public String getLegCouponPaymentDate_1() {
		return legCouponPaymentDate_1;
	}

	public void setLegCouponPaymentDate_1(String legCouponPaymentDate_1) {
		this.legCouponPaymentDate_1 = legCouponPaymentDate_1;
	}

	public String getLegDayCount_1() {
		return legDayCount_1;
	}

	public void setLegDayCount_1(String legDayCount_1) {
		this.legDayCount_1 = legDayCount_1;
	}

	public String getLegSide_2() {
		return legSide_2;
	}

	public void setLegSide_2(String legSide_2) {
		this.legSide_2 = legSide_2;
	}

	public String getLegPriceType_2() {
		return legPriceType_2;
	}

	public void setLegPriceType_2(String legPriceType_2) {
		this.legPriceType_2 = legPriceType_2;
	}

	public String getLegBenchmarkCurveName() {
		return legBenchmarkCurveName;
	}

	public void setLegBenchmarkCurveName(String legBenchmarkCurveName) {
		this.legBenchmarkCurveName = legBenchmarkCurveName;
	}

	public String getLegBenchmarkSpread() {
		return legBenchmarkSpread;
	}

	public void setLegBenchmarkSpread(String legBenchmarkSpread) {
		this.legBenchmarkSpread = legBenchmarkSpread;
	}

	public String getLegCouponPaymentDate_2() {
		return legCouponPaymentDate_2;
	}

	public void setLegCouponPaymentDate_2(String legCouponPaymentDate_2) {
		this.legCouponPaymentDate_2 = legCouponPaymentDate_2;
	}

	public String getLegCouponPaymentFrequency_2() {
		return legCouponPaymentFrequency_2;
	}

	public void setLegCouponPaymentFrequency_2(String legCouponPaymentFrequency_2) {
		this.legCouponPaymentFrequency_2 = legCouponPaymentFrequency_2;
	}

	public String getLegInterestAccrualDate() {
		return legInterestAccrualDate;
	}

	public void setLegInterestAccrualDate(String legInterestAccrualDate) {
		this.legInterestAccrualDate = legInterestAccrualDate;
	}

	public String getDegInterestAccrualResetFrequency() {
		return degInterestAccrualResetFrequency;
	}

	public void setDegInterestAccrualResetFrequency(
			String degInterestAccrualResetFrequency) {
		this.degInterestAccrualResetFrequency = degInterestAccrualResetFrequency;
	}

	public String getLegInterestAccrualMethod() {
		return legInterestAccrualMethod;
	}

	public void setLegInterestAccrualMethod(String legInterestAccrualMethod) {
		this.legInterestAccrualMethod = legInterestAccrualMethod;
	}

	public String getLegDayCount_2() {
		return legDayCount_2;
	}

	public void setLegDayCount_2(String legDayCount_2) {
		this.legDayCount_2 = legDayCount_2;
	}

	public String getNoPartySubIDs() {
		return noPartySubIDs;
	}

	public void setNoPartySubIDs(String noPartySubIDs) {
		this.noPartySubIDs = noPartySubIDs;
	}

	public String getNoPartyIDs() {
		return noPartyIDs;
	}

	public void setNoPartyIDs(String noPartyIDs) {
		this.noPartyIDs = noPartyIDs;
	}

	public String getPartyID_1() {
		return partyID_1;
	}

	public void setPartyID_1(String partyID_1) {
		this.partyID_1 = partyID_1;
	}

	public String getPartyRole_1() {
		return partyRole_1;
	}

	public void setPartyRole_1(String partyRole_1) {
		this.partyRole_1 = partyRole_1;
	}

	public String getPartyID_2() {
		return partyID_2;
	}

	public void setPartyID_2(String partyID_2) {
		this.partyID_2 = partyID_2;
	}

	public String getPartyRole_2() {
		return partyRole_2;
	}

	public void setPartyRole_2(String partyRole_2) {
		this.partyRole_2 = partyRole_2;
	}

	public List<AgencyBasicInformationModel> getList1() {
		return list1;
	}

	public void setList1(List<AgencyBasicInformationModel> list1) {
		this.list1 = list1;
	}

	public List<AgencyBasicInformationModel> getList2() {
		return list2;
	}

	public void setList2(List<AgencyBasicInformationModel> list2) {
		this.list2 = list2;
	}

	

}






















