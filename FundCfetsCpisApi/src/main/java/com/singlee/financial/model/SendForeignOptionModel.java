package com.singlee.financial.model;

import java.io.Serializable;

import com.singlee.financial.pojo.TradeConfirmBean;
/**
 * 外汇期权
 * @author 
 *
 */
public class SendForeignOptionModel extends TradeConfirmBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    
	private String  execID               ;  //交易编号            
	private String  tradedate            ;  //交易日期           
	private String  confirmid            ;  //报文编号            
//	private String  marketindicator      ;  //市场标识 19-外汇期权         
	private String  optdatatype          ;  //交易来源            
	private String  combinationid        ;  //组合编号           
	private String  optpayouttype        ;  //组合类型 1-普通期权;103-看涨期权价差组合;104-看跌期权价差组合;107-风险逆转期权组合;105-跨式期权组合;106-异价跨式期权组合;108-蝶式期权组合;109-自定义期权组合            
	private String  nomasscofirmations   ;              
	private String  side                 ; //交易方向	             
//	private String  netgrossind          ; //清算方式           
	private String  putorcall            ; //期权类型 0-PUT;1-CALL            
	private String  agreementcurrency    ; //期权货币            
	private String  strikeprice          ; //行权价格             
	private String  currency1            ; //基准货币            
	private String  currency1amt         ; //基准货币金额             
	private String  currency2            ; //非基准货币             
	private String  currency2amt         ; //非基准货币金额             
	private String  maturitydate         ; //到期日             
	private String  paymentdate          ; //期权费支付日            
	private String  optpremiumamt        ; //期权费 1-PIPS;2-TERM%             
	private String  optpremiumbasis      ; //期权费类型 1-PIPS;2-TERM%             
	private String  optpremiumvalue      ; //期权费率             
	private String  derivativeexercisestyle ;   //0-欧式期权;1-美式期权        
	private String  expiretime          ;      //行权截止日              
	private String  expiretimezone      ;       //行权市区          
	private String  deliverydate        ;      //交割日      
	private String  optsettlamttype     ;     //交割方式 1-全额交割;2-差额交割          
	private String  benchmarkcurvename  ;    //参考价 1-中间价 2-10点参考价;3-11点参考价;4-14点参考价;5-15点参考价       
	private String  optpremiumcurrency  ;    //期权费货币              
	//本方21位机构代码
	private String partyID_1;
	//对手方21位机构代码
	private String partyID_2;
	
	public String getExecID() {
		return execID;
	}
	public void setExecID(String execID) {
		this.execID = execID;
	}
	public String getTradedate() {
		return tradedate;
	}
	public void setTradedate(String tradedate) {
		this.tradedate = tradedate;
	}
	public String getConfirmid() {
		return confirmid;
	}
	public void setConfirmid(String confirmid) {
		this.confirmid = confirmid;
	}
//	public String getMarketindicator() {
//		return marketindicator;
//	}
//	public void setMarketindicator(String marketindicator) {
//		this.marketindicator = marketindicator;
//	}
	public String getOptdatatype() {
		return optdatatype;
	}
	public void setOptdatatype(String optdatatype) {
		this.optdatatype = optdatatype;
	}
	public String getCombinationid() {
		return combinationid;
	}
	public void setCombinationid(String combinationid) {
		this.combinationid = combinationid;
	}
	public String getOptpayouttype() {
		return optpayouttype;
	}
	public void setOptpayouttype(String optpayouttype) {
		this.optpayouttype = optpayouttype;
	}
	public String getNomasscofirmations() {
		return nomasscofirmations;
	}
	public void setNomasscofirmations(String nomasscofirmations) {
		this.nomasscofirmations = nomasscofirmations;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
//	public String getNetgrossind() {
//		return netgrossind;
//	}
//	public void setNetgrossind(String netgrossind) {
//		this.netgrossind = netgrossind;
//	}
	public String getPutorcall() {
		return putorcall;
	}
	public void setPutorcall(String putorcall) {
		this.putorcall = putorcall;
	}
	public String getAgreementcurrency() {
		return agreementcurrency;
	}
	public void setAgreementcurrency(String agreementcurrency) {
		this.agreementcurrency = agreementcurrency;
	}
	public String getStrikeprice() {
		return strikeprice;
	}
	public void setStrikeprice(String strikeprice) {
		this.strikeprice = strikeprice;
	}
	public String getCurrency1() {
		return currency1;
	}
	public void setCurrency1(String currency1) {
		this.currency1 = currency1;
	}
	public String getCurrency1amt() {
		return currency1amt;
	}
	public void setCurrency1amt(String currency1amt) {
		this.currency1amt = currency1amt;
	}
	public String getCurrency2() {
		return currency2;
	}
	public void setCurrency2(String currency2) {
		this.currency2 = currency2;
	}
	public String getCurrency2amt() {
		return currency2amt;
	}
	public void setCurrency2amt(String currency2amt) {
		this.currency2amt = currency2amt;
	}
	public String getMaturitydate() {
		return maturitydate;
	}
	public void setMaturitydate(String maturitydate) {
		this.maturitydate = maturitydate;
	}
	public String getPaymentdate() {
		return paymentdate;
	}
	public void setPaymentdate(String paymentdate) {
		this.paymentdate = paymentdate;
	}
	public String getOptpremiumamt() {
		return optpremiumamt;
	}
	public void setOptpremiumamt(String optpremiumamt) {
		this.optpremiumamt = optpremiumamt;
	}
	public String getOptpremiumbasis() {
		return optpremiumbasis;
	}
	public void setOptpremiumbasis(String optpremiumbasis) {
		this.optpremiumbasis = optpremiumbasis;
	}
	public String getOptpremiumvalue() {
		return optpremiumvalue;
	}
	public void setOptpremiumvalue(String optpremiumvalue) {
		this.optpremiumvalue = optpremiumvalue;
	}
	public String getDerivativeexercisestyle() {
		return derivativeexercisestyle;
	}
	public void setDerivativeexercisestyle(String derivativeexercisestyle) {
		this.derivativeexercisestyle = derivativeexercisestyle;
	}
	public String getExpiretime() {
		return expiretime;
	}
	public void setExpiretime(String expiretime) {
		this.expiretime = expiretime;
	}
	public String getExpiretimezone() {
		return expiretimezone;
	}
	public void setExpiretimezone(String expiretimezone) {
		this.expiretimezone = expiretimezone;
	}
	public String getDeliverydate() {
		return deliverydate;
	}
	public void setDeliverydate(String deliverydate) {
		this.deliverydate = deliverydate;
	}
	public String getOptsettlamttype() {
		return optsettlamttype;
	}
	public void setOptsettlamttype(String optsettlamttype) {
		this.optsettlamttype = optsettlamttype;
	}
	public String getBenchmarkcurvename() {
		return benchmarkcurvename;
	}
	public void setBenchmarkcurvename(String benchmarkcurvename) {
		this.benchmarkcurvename = benchmarkcurvename;
	}
	public String getOptpremiumcurrency() {
		return optpremiumcurrency;
	}
	public void setOptpremiumcurrency(String optpremiumcurrency) {
		this.optpremiumcurrency = optpremiumcurrency;
	}
	public String getPartyID_1() {
		return partyID_1;
	}
	public void setPartyID_1(String partyID_1) {
		this.partyID_1 = partyID_1;
	}
	public String getPartyID_2() {
		return partyID_2;
	}
	public void setPartyID_2(String partyID_2) {
		this.partyID_2 = partyID_2;
	}
	
	
	
}
