package com.singlee.financial.model;

import java.io.Serializable;
import java.util.List;

import com.singlee.financial.pojo.TradeConfirmBean;
/**
 * 外汇期权
 * @author 
 *
 */
public class SendForeignCrsModel extends TradeConfirmBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    
	private String execID		;//交易编号
	private String  confirmid                   ;  //确认报文ID
//	private String  marketindictor              ;  //市场标识 17-货币掉期
	private String  tradedate                   ;  //交易日期
	private String  settldate                   ;   //生效日-清算日期
//	private String  netgrossind                 ;	//清算方式
	private String  maturitydate                ;	//到期日
	private String  dateadjustmentindic         ;   //起息日调整
	private String  iniexdate                   ;	//期初本金交换日
	private String  finalexdate                 ;	//期末本金交换日
	private String  calculateagency             ;	//计算机构 0-中国外汇交易中心
	private String  notionalexchangetype        ;	//本金交换形式 0-期初期末均交换 1-期初交换期末不交换 2-期初不交换期末交换 3-期初期末均不交换
	private String  lastspotrate                ;	//即期汇率
	private String  subtype                     ;	
	private String  text                        ;	//备注
	private String  legcurrency_1               ;	//本方货币代码
	private String  legcouponpaymentdate_1      ;	//本方首次付息日
	private String  legorderqty_1               ;	//本方名义本金
	private String  legpricetype_1              ;	//本方利率类型 3-固定利率 6-浮动利率
	private String  legbenchmarkcurvename_1     ;	//本方适用利率
	private String  legbenchmarktenor_1         ;	//本方利率期限 ON-O/N;7D
	private String  legbenchmarkspread_1        ;	//本方利差
	private String  legprice_1                  ;	//本方固定利率
	private String  legintaccresetfrequency_1   ;	//本方定息周期 1W-1W
	private String  legintfixdateadjustment_1   ;	//本方定息规则 0-V_0;1-V_1;2-V_2
	private String  legdaycount_1               ;	//本方计息基准 0-A/A;1-A/360;2-30/360;3-A/365;5-A/365F
	private String  legcouponpaymentfrequency_1 ;	//本方计息周期 1W-1W
	private String  legcouponpaymentdatereset_1 ;	//本方付息日调整规则 0-上一营业日;1-下一营业日;2经调整的下一营业日
	private String  legstubindicator_1          ;	//本方残段标识 1-前置残段标识;2-后置残段标识;3-前置和后置残段标识
	private String  leg1_paymentamt             ;	//本方摊销金额
	private String  leg1_paymentcurrency        ;	//本方摊销币种
	private String  leg1_altsettldate           ;	//本方摊销计息开始日
	private String  leg1_altsettldate2          ;	//本方摊销计息终止日
	private String  leg1_paydate                ;	//本方摊销付息日期
	private String  legcurrency_2               ;	//对手方货币代码
	private String  legcouponpaymentdate_2      ;	//对手方首次付息日期
	private String  legorderqty_2               ;	//对手方名义本金
	private String  legpricetype_2              ;	//对手方利率类型 3-固定利率 6-浮动利率
	private String  legbenchmarkcurvename_2     ;	//对手方适用利率
	private String  legbenchmarktenor_2         ;	//对手方利率期限 ON-O/N;7D-7D
	private String  legbenchmarkspread_2        ;	//对手方利差
	private String  legprice_2                  ;	//对手方固定利率
	private String  legintaccresetfrequency_2   ;	//对手方定息周期 1W-1W
	private String  legintfixdateadjustment_2   ;	//对手方定息规则 0-V_0;1-V_1;2-V_2
	private String  legdaycount_2               ;	//对手方计息基准 0-A/A;1-A/360;2-30/360;3-A/365;5-A/365F
	private String  legcouponpaymentfrequency_2 ;	//对手方计息周期 1W-1W
	private String  legcouponpaymentdatereset_2 ;	//对手方付息日调整规则 0-上一营业日;1-下一营业日;2经调整的下一营业日
	private String  legstubindicator_2          ;	//对手方残段标识 1-前置残段标识;2-后置残段标识;3-前置和后置残段标识
	private String  leg2_paymentamt             ;	//对手方摊销金额
	private String  leg2_paymentcurrency        ;	//对手方摊销币种	
	private String  leg2_altsettldate           ;	//对手方摊销计息开始日期
	private String  leg2_altsettldate2          ;  //对手方摊销计息终止日
	private String  leg2_paydate                ;//对手方摊销付息日期
	private String  legSide_1					;
	private String  legSide_2					;
	//本方21位机构代码
	private String partyID_1;
	//对手方21位机构代码
	private String partyID_2;
	
	private List<IntPayModel> intPayModels1;
	
	private List<IntPayModel> intPayModels2;
	
	public String getConfirmid() {
		return confirmid;
	}
	public void setConfirmid(String confirmid) {
		this.confirmid = confirmid;
	}
//	public String getMarketindictor() {
//		return marketindictor;
//	}
//	public void setMarketindictor(String marketindictor) {
//		this.marketindictor = marketindictor;
//	}
	public String getTradedate() {
		return tradedate;
	}
	public void setTradedate(String tradedate) {
		this.tradedate = tradedate;
	}
	public String getSettldate() {
		return settldate;
	}
	public void setSettldate(String settldate) {
		this.settldate = settldate;
	}
//	public String getNetgrossind() {
//		return netgrossind;
//	}
//	public void setNetgrossind(String netgrossind) {
//		this.netgrossind = netgrossind;
//	}
	public String getMaturitydate() {
		return maturitydate;
	}
	public void setMaturitydate(String maturitydate) {
		this.maturitydate = maturitydate;
	}
	public String getDateadjustmentindic() {
		return dateadjustmentindic;
	}
	public void setDateadjustmentindic(String dateadjustmentindic) {
		this.dateadjustmentindic = dateadjustmentindic;
	}
	public String getIniexdate() {
		return iniexdate;
	}
	public void setIniexdate(String iniexdate) {
		this.iniexdate = iniexdate;
	}
	public String getFinalexdate() {
		return finalexdate;
	}
	public void setFinalexdate(String finalexdate) {
		this.finalexdate = finalexdate;
	}
	public String getCalculateagency() {
		return calculateagency;
	}
	public void setCalculateagency(String calculateagency) {
		this.calculateagency = calculateagency;
	}
	public String getNotionalexchangetype() {
		return notionalexchangetype;
	}
	public void setNotionalexchangetype(String notionalexchangetype) {
		this.notionalexchangetype = notionalexchangetype;
	}
	public String getLastspotrate() {
		return lastspotrate;
	}
	public void setLastspotrate(String lastspotrate) {
		this.lastspotrate = lastspotrate;
	}
	public String getSubtype() {
		return subtype;
	}
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLegcurrency_1() {
		return legcurrency_1;
	}
	public void setLegcurrency_1(String legcurrency_1) {
		this.legcurrency_1 = legcurrency_1;
	}
	public String getLegcouponpaymentdate_1() {
		return legcouponpaymentdate_1;
	}
	public void setLegcouponpaymentdate_1(String legcouponpaymentdate_1) {
		this.legcouponpaymentdate_1 = legcouponpaymentdate_1;
	}
	public String getLegorderqty_1() {
		return legorderqty_1;
	}
	public void setLegorderqty_1(String legorderqty_1) {
		this.legorderqty_1 = legorderqty_1;
	}
	public String getLegpricetype_1() {
		return legpricetype_1;
	}
	public void setLegpricetype_1(String legpricetype_1) {
		this.legpricetype_1 = legpricetype_1;
	}
	public String getLegbenchmarkcurvename_1() {
		return legbenchmarkcurvename_1;
	}
	public void setLegbenchmarkcurvename_1(String legbenchmarkcurvename_1) {
		this.legbenchmarkcurvename_1 = legbenchmarkcurvename_1;
	}
	public String getLegbenchmarktenor_1() {
		return legbenchmarktenor_1;
	}
	public void setLegbenchmarktenor_1(String legbenchmarktenor_1) {
		this.legbenchmarktenor_1 = legbenchmarktenor_1;
	}
	public String getLegbenchmarkspread_1() {
		return legbenchmarkspread_1;
	}
	public void setLegbenchmarkspread_1(String legbenchmarkspread_1) {
		this.legbenchmarkspread_1 = legbenchmarkspread_1;
	}
	public String getLegprice_1() {
		return legprice_1;
	}
	public void setLegprice_1(String legprice_1) {
		this.legprice_1 = legprice_1;
	}
	public String getLegintaccresetfrequency_1() {
		return legintaccresetfrequency_1;
	}
	public void setLegintaccresetfrequency_1(String legintaccresetfrequency_1) {
		this.legintaccresetfrequency_1 = legintaccresetfrequency_1;
	}
	public String getLegintfixdateadjustment_1() {
		return legintfixdateadjustment_1;
	}
	public void setLegintfixdateadjustment_1(String legintfixdateadjustment_1) {
		this.legintfixdateadjustment_1 = legintfixdateadjustment_1;
	}
	public String getLegdaycount_1() {
		return legdaycount_1;
	}
	public void setLegdaycount_1(String legdaycount_1) {
		this.legdaycount_1 = legdaycount_1;
	}
	public String getLegcouponpaymentfrequency_1() {
		return legcouponpaymentfrequency_1;
	}
	public void setLegcouponpaymentfrequency_1(String legcouponpaymentfrequency_1) {
		this.legcouponpaymentfrequency_1 = legcouponpaymentfrequency_1;
	}
	public String getLegcouponpaymentdatereset_1() {
		return legcouponpaymentdatereset_1;
	}
	public void setLegcouponpaymentdatereset_1(String legcouponpaymentdatereset_1) {
		this.legcouponpaymentdatereset_1 = legcouponpaymentdatereset_1;
	}
	public String getLegstubindicator_1() {
		return legstubindicator_1;
	}
	public void setLegstubindicator_1(String legstubindicator_1) {
		this.legstubindicator_1 = legstubindicator_1;
	}
	public String getLeg1_paymentamt() {
		return leg1_paymentamt;
	}
	public void setLeg1_paymentamt(String leg1_paymentamt) {
		this.leg1_paymentamt = leg1_paymentamt;
	}
	public String getLeg1_paymentcurrency() {
		return leg1_paymentcurrency;
	}
	public void setLeg1_paymentcurrency(String leg1_paymentcurrency) {
		this.leg1_paymentcurrency = leg1_paymentcurrency;
	}
	public String getLeg1_altsettldate() {
		return leg1_altsettldate;
	}
	public void setLeg1_altsettldate(String leg1_altsettldate) {
		this.leg1_altsettldate = leg1_altsettldate;
	}
	public String getLeg1_altsettldate2() {
		return leg1_altsettldate2;
	}
	public void setLeg1_altsettldate2(String leg1_altsettldate2) {
		this.leg1_altsettldate2 = leg1_altsettldate2;
	}
	public String getLeg1_paydate() {
		return leg1_paydate;
	}
	public void setLeg1_paydate(String leg1_paydate) {
		this.leg1_paydate = leg1_paydate;
	}
	public String getLegcurrency_2() {
		return legcurrency_2;
	}
	public void setLegcurrency_2(String legcurrency_2) {
		this.legcurrency_2 = legcurrency_2;
	}
	public String getLegcouponpaymentdate_2() {
		return legcouponpaymentdate_2;
	}
	public void setLegcouponpaymentdate_2(String legcouponpaymentdate_2) {
		this.legcouponpaymentdate_2 = legcouponpaymentdate_2;
	}
	public String getLegorderqty_2() {
		return legorderqty_2;
	}
	public void setLegorderqty_2(String legorderqty_2) {
		this.legorderqty_2 = legorderqty_2;
	}
	public String getLegpricetype_2() {
		return legpricetype_2;
	}
	public void setLegpricetype_2(String legpricetype_2) {
		this.legpricetype_2 = legpricetype_2;
	}
	public String getLegbenchmarkcurvename_2() {
		return legbenchmarkcurvename_2;
	}
	public void setLegbenchmarkcurvename_2(String legbenchmarkcurvename_2) {
		this.legbenchmarkcurvename_2 = legbenchmarkcurvename_2;
	}
	public String getLegbenchmarktenor_2() {
		return legbenchmarktenor_2;
	}
	public void setLegbenchmarktenor_2(String legbenchmarktenor_2) {
		this.legbenchmarktenor_2 = legbenchmarktenor_2;
	}
	public String getLegbenchmarkspread_2() {
		return legbenchmarkspread_2;
	}
	public void setLegbenchmarkspread_2(String legbenchmarkspread_2) {
		this.legbenchmarkspread_2 = legbenchmarkspread_2;
	}
	public String getLegprice_2() {
		return legprice_2;
	}
	public void setLegprice_2(String legprice_2) {
		this.legprice_2 = legprice_2;
	}
	public String getLegintaccresetfrequency_2() {
		return legintaccresetfrequency_2;
	}
	public void setLegintaccresetfrequency_2(String legintaccresetfrequency_2) {
		this.legintaccresetfrequency_2 = legintaccresetfrequency_2;
	}
	public String getLegintfixdateadjustment_2() {
		return legintfixdateadjustment_2;
	}
	public void setLegintfixdateadjustment_2(String legintfixdateadjustment_2) {
		this.legintfixdateadjustment_2 = legintfixdateadjustment_2;
	}
	public String getLegdaycount_2() {
		return legdaycount_2;
	}
	public void setLegdaycount_2(String legdaycount_2) {
		this.legdaycount_2 = legdaycount_2;
	}
	public String getLegcouponpaymentfrequency_2() {
		return legcouponpaymentfrequency_2;
	}
	public void setLegcouponpaymentfrequency_2(String legcouponpaymentfrequency_2) {
		this.legcouponpaymentfrequency_2 = legcouponpaymentfrequency_2;
	}
	public String getLegcouponpaymentdatereset_2() {
		return legcouponpaymentdatereset_2;
	}
	public void setLegcouponpaymentdatereset_2(String legcouponpaymentdatereset_2) {
		this.legcouponpaymentdatereset_2 = legcouponpaymentdatereset_2;
	}
	public String getLegstubindicator_2() {
		return legstubindicator_2;
	}
	public void setLegstubindicator_2(String legstubindicator_2) {
		this.legstubindicator_2 = legstubindicator_2;
	}
	public String getLeg2_paymentamt() {
		return leg2_paymentamt;
	}
	public void setLeg2_paymentamt(String leg2_paymentamt) {
		this.leg2_paymentamt = leg2_paymentamt;
	}
	public String getLeg2_paymentcurrency() {
		return leg2_paymentcurrency;
	}
	public void setLeg2_paymentcurrency(String leg2_paymentcurrency) {
		this.leg2_paymentcurrency = leg2_paymentcurrency;
	}
	public String getLeg2_altsettldate() {
		return leg2_altsettldate;
	}
	public void setLeg2_altsettldate(String leg2_altsettldate) {
		this.leg2_altsettldate = leg2_altsettldate;
	}
	public String getLeg2_altsettldate2() {
		return leg2_altsettldate2;
	}
	public void setLeg2_altsettldate2(String leg2_altsettldate2) {
		this.leg2_altsettldate2 = leg2_altsettldate2;
	}
	public String getLeg2_paydate() {
		return leg2_paydate;
	}
	public void setLeg2_paydate(String leg2_paydate) {
		this.leg2_paydate = leg2_paydate;
	}
	public String getPartyID_1() {
		return partyID_1;
	}
	public void setPartyID_1(String partyID_1) {
		this.partyID_1 = partyID_1;
	}
	public String getPartyID_2() {
		return partyID_2;
	}
	public void setPartyID_2(String partyID_2) {
		this.partyID_2 = partyID_2;
	}
	public String getExecID() {
		return execID;
	}
	public void setExecID(String execID) {
		this.execID = execID;
	}
	public List<IntPayModel> getIntPayModels1() {
		return intPayModels1;
	}
	public void setIntPayModels1(List<IntPayModel> intPayModels1) {
		this.intPayModels1 = intPayModels1;
	}
	public List<IntPayModel> getIntPayModels2() {
		return intPayModels2;
	}
	public void setIntPayModels2(List<IntPayModel> intPayModels2) {
		this.intPayModels2 = intPayModels2;
	}
	public String getLegSide_1() {
		return legSide_1;
	}
	public void setLegSide_1(String legSide_1) {
		this.legSide_1 = legSide_1;
	}
	public String getLegSide_2() {
		return legSide_2;
	}
	public void setLegSide_2(String legSide_2) {
		this.legSide_2 = legSide_2;
	}
	
	
}
