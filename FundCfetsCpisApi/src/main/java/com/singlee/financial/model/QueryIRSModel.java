package com.singlee.financial.model;

import java.io.Serializable;

public class QueryIRSModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//成交编号
	private String ExecID;
	
	//清算方式
	private String NetGrossInd;
	
	//查询成交状态
	private String QueryExecType;
	
	//交易场所
	private String MarketID;
	
	//查询起始日
	private String QueryStartDate;

	//查询终止日
	private String QueryEndDate;
	
	//查询类型
	private String ConfirmType;
	
	//查询返回的结果数
	private String QueryStartNumber;
	
	//查询请求编号
	private String QueryRequestID;

	public String getExecID() {
		return ExecID;
	}

	public void setExecID(String execID) {
		ExecID = execID;
	}

	public String getNetGrossInd() {
		return NetGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		NetGrossInd = netGrossInd;
	}

	public String getQueryExecType() {
		return QueryExecType;
	}

	public void setQueryExecType(String queryExecType) {
		QueryExecType = queryExecType;
	}

	public String getMarketID() {
		return MarketID;
	}

	public void setMarketID(String marketID) {
		MarketID = marketID;
	}

	public String getQueryStartDate() {
		return QueryStartDate;
	}

	public void setQueryStartDate(String queryStartDate) {
		QueryStartDate = queryStartDate;
	}

	public String getQueryEndDate() {
		return QueryEndDate;
	}

	public void setQueryEndDate(String queryEndDate) {
		QueryEndDate = queryEndDate;
	}

	public String getConfirmType() {
		return ConfirmType;
	}

	public void setConfirmType(String confirmType) {
		ConfirmType = confirmType;
	}

	public String getQueryStartNumber() {
		return QueryStartNumber;
	}

	public void setQueryStartNumber(String queryStartNumber) {
		QueryStartNumber = queryStartNumber;
	}

	public String getQueryRequestID() {
		return QueryRequestID;
	}

	public void setQueryRequestID(String queryRequestID) {
		QueryRequestID = queryRequestID;
	}
	
	
}
