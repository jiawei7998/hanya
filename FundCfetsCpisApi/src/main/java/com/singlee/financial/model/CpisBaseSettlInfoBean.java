//package com.singlee.financial.model;
//
//import java.sql.Clob;
//
//public class CpisBaseSettlInfoBean {
//	private static final long serialVersionUID = 1L;
//	
//	private String execID;//成交编号
//	private String tradeID;//SUMMIT编号
//	private Clob settleInfo;//SUMMIT清算信息文本
//	private String effDate;//起息日期  掉期近端起息日
//	private String matDate;//到期日取
//	private String swiftmessagetype; //报文类型
//	private String ccy1;//成交货币 -近端买入币种
//	private String ccy2;//对应货币-近端卖出币种
//	private String tradetype;//交易类型
//	private String side;    //交易方向
//	private String subtype;  //区分IRS CRS IRS取IRSWAP CRS取CCYSWAP
//	private String sendflag; //0未发送 1已发送
//	private String optccy; //期权费币种
//	private String marketindicator;  //市场表示
//	
//	private String   institution_id;     //本方机构号      
//	private String   trade_id;     //交易员名称               
//	private String   trade_name;  //交易员名称                
//	private String   short_name;  //机构简称                 
//	private String   full_name;     //机构全称              
//	private String   marker_taker_role ;          
//	private String   settccy;           //本方清算币种           
//	private String   bankname ;       //本方近端 开户行名称-110
//	private String   bankopenno ;     //本方近端 开户行SWIFTCODE/开户行行号-138
//	private String   bankacctno ;     //   本方近端 开户行在中间行的资金账号-207          
//	private String   acctname  ;     //本方近端 账户名称             
//	private String   acctopenno ;    //本方近端资 金账户行SWIFT CODE/资金账户行-16             
//	private String   acctno    ;     //本方近端 资金账户-15             
//	private String   intermediarybankname  ;  //本方近端 中间行名称-209   
//	private String   intermediarybankbiccode   ; //本方近端 中间行SWIFT CODE/中间行行号-211
//	private String   intermediarybankacctno ;     //本方近端 中间行在开户行的资金账号-213
//	private String   remark     ;            //本方近端 付言-139     
//	private String   farbankname  ;            //本方远端 开户行名称   
//	private String   farbankopenno  ;          //本方远端 开户行SWIFTCODE/开户行行号-138   
//	private String   farbankacctno   ;          //本方远端 开户行在中间行的资金账号-207  
//	private String   faracctname   ;             //本方远端 账户名称-23 
//	private String   faracctopenno   ;           //本方远端资 金账户行SWIFT CODE/资金账户行-16 
//	private String   faracctno ;                  //本方远端 资金账户-15
//	private String   farintermediarybankname    ;  //本方远端 中间行名称-209
//	private String   farintermediarybankbiccode  ; //本方远端 中间行SWIFT CODE/中间行行号-211
//	private String   farintermediarybankacctno  ;  //本方远端 中间行在开户行的资金账号-213
//	private String   farremark       ;             //本方远端 付言-139
//	private String   c_institution_id   ;          //对手机构号
//	private String   c_trade_id      ;            //交易员名称
//	private String   c_trade_name   ;             //交易员名称
//	private String   c_short_name ;               //机构简称
//	private String   c_full_name   ;              //机构全称
//	private String   c_marker_taker_role;         
//	private String   c_settccy ;                  //对手清算币种
//	private String   c_bankname ;                 //对手方近端 开户行名称-110
//	private String   c_bankopenno ;               //对手方近端 开户行SWIFTCODE/开户行行号-138'
//	private String   c_bankacctno  ;              //对手方近端 开户行在中间行的资金账号-207
//	private String   c_acctname   ;               //对手方近端 账户名称-23
//	private String   c_acctopenno  ;              //对手方近端 资金账户行SWIFT CODE/资金账户行-16
//	private String   c_acctno    ;                //对手方近端 资金账户-15
//	private String   c_intermediarybankname ;     //对手方近端 中间行名称-209
//	private String   c_intermediarybankbiccode  ;  //对手方近端 中间行SWIFT CODE/中间行行号-211
//	private String   c_intermediarybankacctno  ;   //对手方近端 中间行在开户行的资金账号-213
//	private String   c_remark      ;               //对手方近端 付言-139
//	private String   c_farbankname   ;             //对手方远端 开户行名称-110
//	private String   c_farbankopenno ;             //对手方远端 开户行SWIFTCODE/开户行行号-138
//	private String   c_farbankacctno  ;            //对手方远端 开户行在中间行的资金账号-207
//	private String   c_faracctname  ;              //对手方远端 账户名称-23
//	private String   c_faracctopenno  ;            //对手方远端 资金账户行SWIFT CODE/资金账户行-16
//	private String   c_faracctno   ;               //对手方远端 资金账户-15
//	private String   c_farintermediarybankname ;   //对手方远端 中间行名称-209
//	private String   c_farintermediarybankbiccode;  //对手方远端 中间行SWIFT CODE/中间行行号-211
//	private String   c_farintermediarybankacctno ; //对手方远端 中间行在开户行的资金账号-213
//	private String   c_farremark ;                  //对手方远端 付言-139
//
//	public String getExecID() {
//		return execID;
//	}
//
//	public void setExecID(String execID) {
//		this.execID = execID;
//	}
//
//	public String getTradeID() {
//		return tradeID;
//	}
//
//	public void setTradeID(String tradeID) {
//		this.tradeID = tradeID;
//	}
//
//	public Clob getSettleInfo() {
//		return settleInfo;
//	}
//
//	public void setSettleInfo(Clob settleInfo) {
//		this.settleInfo = settleInfo;
//	}
//
//	public String getEffDate() {
//		return effDate;
//	}
//
//	public void setEffDate(String effDate) {
//		this.effDate = effDate;
//	}
//
//	public String getMatDate() {
//		return matDate;
//	}
//
//	public void setMatDate(String matDate) {
//		this.matDate = matDate;
//	}
//
//	public String getSwiftmessagetype() {
//		return swiftmessagetype;
//	}
//
//	public void setSwiftmessagetype(String swiftmessagetype) {
//		this.swiftmessagetype = swiftmessagetype;
//	}
//
//	public String getCcy1() {
//		return ccy1;
//	}
//
//	public void setCcy1(String ccy1) {
//		this.ccy1 = ccy1;
//	}
//
//	public String getCcy2() {
//		return ccy2;
//	}
//
//	public void setCcy2(String ccy2) {
//		this.ccy2 = ccy2;
//	}
//
//	public String getTradetype() {
//		return tradetype;
//	}
//
//	public void setTradetype(String tradetype) {
//		this.tradetype = tradetype;
//	}
//
//	public String getInstitution_id() {
//		return institution_id;
//	}
//
//	public void setInstitution_id(String institution_id) {
//		this.institution_id = institution_id;
//	}
//
//	public String getTrade_id() {
//		return trade_id;
//	}
//
//	public void setTrade_id(String trade_id) {
//		this.trade_id = trade_id;
//	}
//
//	public String getTrade_name() {
//		return trade_name;
//	}
//
//	public void setTrade_name(String trade_name) {
//		this.trade_name = trade_name;
//	}
//
//	public String getShort_name() {
//		return short_name;
//	}
//
//	public void setShort_name(String short_name) {
//		this.short_name = short_name;
//	}
//
//	public String getFull_name() {
//		return full_name;
//	}
//
//	public void setFull_name(String full_name) {
//		this.full_name = full_name;
//	}
//
//	public String getMarker_taker_role() {
//		return marker_taker_role;
//	}
//
//	public void setMarker_taker_role(String marker_taker_role) {
//		this.marker_taker_role = marker_taker_role;
//	}
//
//	public String getSettccy() {
//		return settccy;
//	}
//
//	public void setSettccy(String settccy) {
//		this.settccy = settccy;
//	}
//
//	public String getBankname() {
//		return bankname;
//	}
//
//	public void setBankname(String bankname) {
//		this.bankname = bankname;
//	}
//
//	public String getBankopenno() {
//		return bankopenno;
//	}
//
//	public void setBankopenno(String bankopenno) {
//		this.bankopenno = bankopenno;
//	}
//
//	public String getBankacctno() {
//		return bankacctno;
//	}
//
//	public void setBankacctno(String bankacctno) {
//		this.bankacctno = bankacctno;
//	}
//
//	public String getAcctname() {
//		return acctname;
//	}
//
//	public void setAcctname(String acctname) {
//		this.acctname = acctname;
//	}
//
//	public String getAcctopenno() {
//		return acctopenno;
//	}
//
//	public void setAcctopenno(String acctopenno) {
//		this.acctopenno = acctopenno;
//	}
//
//	public String getAcctno() {
//		return acctno;
//	}
//
//	public void setAcctno(String acctno) {
//		this.acctno = acctno;
//	}
//
//	public String getIntermediarybankname() {
//		return intermediarybankname;
//	}
//
//	public void setIntermediarybankname(String intermediarybankname) {
//		this.intermediarybankname = intermediarybankname;
//	}
//
//	public String getIntermediarybankbiccode() {
//		return intermediarybankbiccode;
//	}
//
//	public void setIntermediarybankbiccode(String intermediarybankbiccode) {
//		this.intermediarybankbiccode = intermediarybankbiccode;
//	}
//
//	public String getIntermediarybankacctno() {
//		return intermediarybankacctno;
//	}
//
//	public void setIntermediarybankacctno(String intermediarybankacctno) {
//		this.intermediarybankacctno = intermediarybankacctno;
//	}
//
//	public String getRemark() {
//		return remark;
//	}
//
//	public void setRemark(String remark) {
//		this.remark = remark;
//	}
//
//	public String getFarbankname() {
//		return farbankname;
//	}
//
//	public void setFarbankname(String farbankname) {
//		this.farbankname = farbankname;
//	}
//
//	public String getFarbankopenno() {
//		return farbankopenno;
//	}
//
//	public void setFarbankopenno(String farbankopenno) {
//		this.farbankopenno = farbankopenno;
//	}
//
//	public String getFarbankacctno() {
//		return farbankacctno;
//	}
//
//	public void setFarbankacctno(String farbankacctno) {
//		this.farbankacctno = farbankacctno;
//	}
//
//	public String getFaracctname() {
//		return faracctname;
//	}
//
//	public void setFaracctname(String faracctname) {
//		this.faracctname = faracctname;
//	}
//
//	public String getFaracctopenno() {
//		return faracctopenno;
//	}
//
//	public void setFaracctopenno(String faracctopenno) {
//		this.faracctopenno = faracctopenno;
//	}
//
//	public String getFaracctno() {
//		return faracctno;
//	}
//
//	public void setFaracctno(String faracctno) {
//		this.faracctno = faracctno;
//	}
//
//	public String getFarintermediarybankname() {
//		return farintermediarybankname;
//	}
//
//	public void setFarintermediarybankname(String farintermediarybankname) {
//		this.farintermediarybankname = farintermediarybankname;
//	}
//
//	public String getFarintermediarybankbiccode() {
//		return farintermediarybankbiccode;
//	}
//
//	public void setFarintermediarybankbiccode(String farintermediarybankbiccode) {
//		this.farintermediarybankbiccode = farintermediarybankbiccode;
//	}
//
//	public String getFarintermediarybankacctno() {
//		return farintermediarybankacctno;
//	}
//
//	public void setFarintermediarybankacctno(String farintermediarybankacctno) {
//		this.farintermediarybankacctno = farintermediarybankacctno;
//	}
//
//	public String getFarremark() {
//		return farremark;
//	}
//
//	public void setFarremark(String farremark) {
//		this.farremark = farremark;
//	}
//
//	public String getC_institution_id() {
//		return c_institution_id;
//	}
//
//	public void setC_institution_id(String c_institution_id) {
//		this.c_institution_id = c_institution_id;
//	}
//
//	public String getC_trade_id() {
//		return c_trade_id;
//	}
//
//	public void setC_trade_id(String c_trade_id) {
//		this.c_trade_id = c_trade_id;
//	}
//
//	public String getC_trade_name() {
//		return c_trade_name;
//	}
//
//	public void setC_trade_name(String c_trade_name) {
//		this.c_trade_name = c_trade_name;
//	}
//
//	public String getC_short_name() {
//		return c_short_name;
//	}
//
//	public void setC_short_name(String c_short_name) {
//		this.c_short_name = c_short_name;
//	}
//
//	public String getC_full_name() {
//		return c_full_name;
//	}
//
//	public void setC_full_name(String c_full_name) {
//		this.c_full_name = c_full_name;
//	}
//
//	public String getC_marker_taker_role() {
//		return c_marker_taker_role;
//	}
//
//	public void setC_marker_taker_role(String c_marker_taker_role) {
//		this.c_marker_taker_role = c_marker_taker_role;
//	}
//
//	public String getC_settccy() {
//		return c_settccy;
//	}
//
//	public void setC_settccy(String c_settccy) {
//		this.c_settccy = c_settccy;
//	}
//
//	public String getC_bankname() {
//		return c_bankname;
//	}
//
//	public void setC_bankname(String c_bankname) {
//		this.c_bankname = c_bankname;
//	}
//
//	public String getC_bankopenno() {
//		return c_bankopenno;
//	}
//
//	public void setC_bankopenno(String c_bankopenno) {
//		this.c_bankopenno = c_bankopenno;
//	}
//
//	public String getC_bankacctno() {
//		return c_bankacctno;
//	}
//
//	public void setC_bankacctno(String c_bankacctno) {
//		this.c_bankacctno = c_bankacctno;
//	}
//
//	public String getC_acctname() {
//		return c_acctname;
//	}
//
//	public void setC_acctname(String c_acctname) {
//		this.c_acctname = c_acctname;
//	}
//
//	public String getC_acctopenno() {
//		return c_acctopenno;
//	}
//
//	public void setC_acctopenno(String c_acctopenno) {
//		this.c_acctopenno = c_acctopenno;
//	}
//
//	public String getC_acctno() {
//		return c_acctno;
//	}
//
//	public void setC_acctno(String c_acctno) {
//		this.c_acctno = c_acctno;
//	}
//
//	public String getC_intermediarybankname() {
//		return c_intermediarybankname;
//	}
//
//	public void setC_intermediarybankname(String c_intermediarybankname) {
//		this.c_intermediarybankname = c_intermediarybankname;
//	}
//
//	public String getC_intermediarybankbiccode() {
//		return c_intermediarybankbiccode;
//	}
//
//	public void setC_intermediarybankbiccode(String c_intermediarybankbiccode) {
//		this.c_intermediarybankbiccode = c_intermediarybankbiccode;
//	}
//
//	public String getC_intermediarybankacctno() {
//		return c_intermediarybankacctno;
//	}
//
//	public void setC_intermediarybankacctno(String c_intermediarybankacctno) {
//		this.c_intermediarybankacctno = c_intermediarybankacctno;
//	}
//
//	public String getC_remark() {
//		return c_remark;
//	}
//
//	public void setC_remark(String c_remark) {
//		this.c_remark = c_remark;
//	}
//
//	public String getC_farbankname() {
//		return c_farbankname;
//	}
//
//	public void setC_farbankname(String c_farbankname) {
//		this.c_farbankname = c_farbankname;
//	}
//
//	public String getC_farbankopenno() {
//		return c_farbankopenno;
//	}
//
//	public void setC_farbankopenno(String c_farbankopenno) {
//		this.c_farbankopenno = c_farbankopenno;
//	}
//
//	public String getC_farbankacctno() {
//		return c_farbankacctno;
//	}
//
//	public void setC_farbankacctno(String c_farbankacctno) {
//		this.c_farbankacctno = c_farbankacctno;
//	}
//
//	public String getC_faracctname() {
//		return c_faracctname;
//	}
//
//	public void setC_faracctname(String c_faracctname) {
//		this.c_faracctname = c_faracctname;
//	}
//
//	public String getC_faracctopenno() {
//		return c_faracctopenno;
//	}
//
//	public void setC_faracctopenno(String c_faracctopenno) {
//		this.c_faracctopenno = c_faracctopenno;
//	}
//
//	public String getC_faracctno() {
//		return c_faracctno;
//	}
//
//	public void setC_faracctno(String c_faracctno) {
//		this.c_faracctno = c_faracctno;
//	}
//
//	public String getC_farintermediarybankname() {
//		return c_farintermediarybankname;
//	}
//
//	public void setC_farintermediarybankname(String c_farintermediarybankname) {
//		this.c_farintermediarybankname = c_farintermediarybankname;
//	}
//
//	public String getC_farintermediarybankbiccode() {
//		return c_farintermediarybankbiccode;
//	}
//
//	public void setC_farintermediarybankbiccode(String c_farintermediarybankbiccode) {
//		this.c_farintermediarybankbiccode = c_farintermediarybankbiccode;
//	}
//
//	public String getC_farintermediarybankacctno() {
//		return c_farintermediarybankacctno;
//	}
//
//	public void setC_farintermediarybankacctno(String c_farintermediarybankacctno) {
//		this.c_farintermediarybankacctno = c_farintermediarybankacctno;
//	}
//
//	public String getC_farremark() {
//		return c_farremark;
//	}
//
//	public void setC_farremark(String c_farremark) {
//		this.c_farremark = c_farremark;
//	}
//
//	public String getSide() {
//		return side;
//	}
//
//	public void setSide(String side) {
//		this.side = side;
//	}
//
//	public String getSubtype() {
//		return subtype;
//	}
//
//	public void setSubtype(String subtype) {
//		this.subtype = subtype;
//	}
//
//	public String getSendflag() {
//		return sendflag;
//	}
//
//	public void setSendflag(String sendflag) {
//		this.sendflag = sendflag;
//	}
//
//	public String getOptccy() {
//		return optccy;
//	}
//
//	public void setOptccy(String optccy) {
//		this.optccy = optccy;
//	}
//
//	public String getMarketindicator() {
//		return marketindicator;
//	}
//
//	public void setMarketindicator(String marketindicator) {
//		this.marketindicator = marketindicator;
//	}
//	
//	
//	
//	
//	
//	
//
//}
