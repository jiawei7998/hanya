package com.singlee.financial.model;

import java.io.Serializable;

/**
 * null
 * 
 * @author lisc
 * 
 * @date 2019-04-29
 */
public class IfsSwiftMatching implements Serializable {
    private String dealno;

    private String queryrequestid;

    private String matchstatus;

    private String transactime;

    private String msg;
    
    private String text;
    
    private String msgtype;
    
    private String prodtype;
    
    private String confirmID;
    
	private String matchMsgPartyid_1;
	private String matchMsgPartyid_2;
	private String matchMsgpartyRole_1;
	private String matchMsgpartyRole_2;
	private String matchMsgSubid_1;
	private String matchMsgSubid_2;
	private String matchMsgSubid_3;
	private String matchMsgSubid_4;
	private String matchMsgSubid_5;
	private String matchMsgSubid_6;
	
	private String matchMsgType;//报文类型
	
	private String field20;
	
	private String cdate;//处理日期
    
    

    private static final long serialVersionUID = 1L;
    
    public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getDealno() {
        return dealno;
    }

    public void setDealno(String dealno) {
        this.dealno = dealno == null ? null : dealno.trim();
    }

    public String getQueryrequestid() {
        return queryrequestid;
    }

    public void setQueryrequestid(String queryrequestid) {
        this.queryrequestid = queryrequestid == null ? null : queryrequestid.trim();
    }

    public String getMatchstatus() {
        return matchstatus;
    }

    public void setMatchstatus(String matchstatus) {
        this.matchstatus = matchstatus == null ? null : matchstatus.trim();
    }

    public String getTransactime() {
        return transactime;
    }

    public void setTransactime(String transactime) {
        this.transactime = transactime == null ? null : transactime.trim();
    }

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getMatchMsgPartyid_1() {
		return matchMsgPartyid_1;
	}

	public void setMatchMsgPartyid_1(String matchMsgPartyid_1) {
		this.matchMsgPartyid_1 = matchMsgPartyid_1;
	}

	public String getMatchMsgPartyid_2() {
		return matchMsgPartyid_2;
	}

	public void setMatchMsgPartyid_2(String matchMsgPartyid_2) {
		this.matchMsgPartyid_2 = matchMsgPartyid_2;
	}

	public String getMatchMsgpartyRole_1() {
		return matchMsgpartyRole_1;
	}

	public void setMatchMsgpartyRole_1(String matchMsgpartyRole_1) {
		this.matchMsgpartyRole_1 = matchMsgpartyRole_1;
	}

	public String getMatchMsgpartyRole_2() {
		return matchMsgpartyRole_2;
	}

	public void setMatchMsgpartyRole_2(String matchMsgpartyRole_2) {
		this.matchMsgpartyRole_2 = matchMsgpartyRole_2;
	}

	public String getMatchMsgSubid_1() {
		return matchMsgSubid_1;
	}

	public void setMatchMsgSubid_1(String matchMsgSubid_1) {
		this.matchMsgSubid_1 = matchMsgSubid_1;
	}

	public String getMatchMsgSubid_2() {
		return matchMsgSubid_2;
	}

	public void setMatchMsgSubid_2(String matchMsgSubid_2) {
		this.matchMsgSubid_2 = matchMsgSubid_2;
	}

	public String getMatchMsgSubid_3() {
		return matchMsgSubid_3;
	}

	public void setMatchMsgSubid_3(String matchMsgSubid_3) {
		this.matchMsgSubid_3 = matchMsgSubid_3;
	}

	public String getMatchMsgSubid_4() {
		return matchMsgSubid_4;
	}

	public void setMatchMsgSubid_4(String matchMsgSubid_4) {
		this.matchMsgSubid_4 = matchMsgSubid_4;
	}

	public String getMatchMsgSubid_5() {
		return matchMsgSubid_5;
	}

	public void setMatchMsgSubid_5(String matchMsgSubid_5) {
		this.matchMsgSubid_5 = matchMsgSubid_5;
	}

	public String getMatchMsgSubid_6() {
		return matchMsgSubid_6;
	}

	public void setMatchMsgSubid_6(String matchMsgSubid_6) {
		this.matchMsgSubid_6 = matchMsgSubid_6;
	}

	public String getConfirmID() {
		return confirmID;
	}

	public void setConfirmID(String confirmID) {
		this.confirmID = confirmID;
	}

	public String getMatchMsgType() {
		return matchMsgType;
	}

	public void setMatchMsgType(String matchMsgType) {
		this.matchMsgType = matchMsgType;
	}

	public String getField20() {
		return field20;
	}

	public void setField20(String field20) {
		this.field20 = field20;
	}

	public String getCdate() {
		return cdate;
	}

	public void setCdate(String cdate) {
		this.cdate = cdate;
	}
	
	
    
    

}