package com.singlee.financial.model;

import java.io.Serializable;

/**
 * 摊销信息
 * @author 程梦杰
 *
 */
public class IntPayModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String execID;//成交编号
	
	private String legSide;//支付方向
	
	private String seq;//序列号
	
	private String amorPaymentAmt;//摊销金额
	
	private String amorPaymentCcy;//摊销币种
	
	private String intPayDate;//利息支付日期
	
	private String intSettlDate;//计算利息开始日期
	
	private String intSettlDate2;//计算利息结束日期

	public String getExecID() {
		return execID;
	}

	public void setExecID(String execID) {
		this.execID = execID;
	}

	public String getLegSide() {
		return legSide;
	}

	public void setLegSide(String legSide) {
		this.legSide = legSide;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getAmorPaymentAmt() {
		return amorPaymentAmt;
	}

	public void setAmorPaymentAmt(String amorPaymentAmt) {
		this.amorPaymentAmt = amorPaymentAmt;
	}

	public String getAmorPaymentCcy() {
		return amorPaymentCcy;
	}

	public void setAmorPaymentCcy(String amorPaymentCcy) {
		this.amorPaymentCcy = amorPaymentCcy;
	}

	public String getIntPayDate() {
		return intPayDate;
	}

	public void setIntPayDate(String intPayDate) {
		this.intPayDate = intPayDate;
	}

	public String getIntSettlDate() {
		return intSettlDate;
	}

	public void setIntSettlDate(String intSettlDate) {
		this.intSettlDate = intSettlDate;
	}

	public String getIntSettlDate2() {
		return intSettlDate2;
	}

	public void setIntSettlDate2(String intSettlDate2) {
		this.intSettlDate2 = intSettlDate2;
	}
	
	
}
