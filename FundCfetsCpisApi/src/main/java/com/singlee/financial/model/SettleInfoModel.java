package com.singlee.financial.model;

public class SettleInfoModel {

	private String payBankID;//付款行行号
	
	private String payBankName;//付款行行名
	
	private String payUserID;//付款行账号
	
	private String payUserName;//付款行账户名
	
	private String recBankID;//收款行行号
	
	private String recBankName;//收款行行名
	
	private String recUserID;//收款行账号
	
	private String recUserName;//收款行账户名
	
	private String partyInBIC;//对手方中间行BICCODE
	
	private String partyAwBIC;//对手方开户行BICCODE;
	
	private String partyBeAcc;//对手方收款行账户
	
	private String partyBeBIC;//对手方收款行BICCODE
	
	private String fwdPartyInBIC;//对手方中间行BICCODE
	
	private String fwdPartyAwBIC;//对手方开户行BICCODE;
	
	private String fwdPartyBeAcc;//对手方收款行账户
	
	private String fwdPartyBeBIC;//对手方收款行BICCODE
	
	private String dealNo;//交易单号
	
	private String br;//部门

	public String getPayBankID() {
		return payBankID;
	}

	public void setPayBankID(String payBankID) {
		this.payBankID = payBankID;
	}

	public String getPayBankName() {
		return payBankName;
	}

	public void setPayBankName(String payBankName) {
		this.payBankName = payBankName;
	}

	public String getPayUserID() {
		return payUserID;
	}

	public void setPayUserID(String payUserID) {
		this.payUserID = payUserID;
	}

	public String getPayUserName() {
		return payUserName;
	}

	public void setPayUserName(String payUserName) {
		this.payUserName = payUserName;
	}

	public String getRecBankID() {
		return recBankID;
	}

	public void setRecBankID(String recBankID) {
		this.recBankID = recBankID;
	}

	public String getRecBankName() {
		return recBankName;
	}

	public void setRecBankName(String recBankName) {
		this.recBankName = recBankName;
	}

	public String getRecUserID() {
		return recUserID;
	}

	public void setRecUserID(String recUserID) {
		this.recUserID = recUserID;
	}

	public String getRecUserName() {
		return recUserName;
	}

	public void setRecUserName(String recUserName) {
		this.recUserName = recUserName;
	}

	public String getPartyInBIC() {
		return partyInBIC;
	}

	public void setPartyInBIC(String partyInBIC) {
		this.partyInBIC = partyInBIC;
	}

	public String getPartyAwBIC() {
		return partyAwBIC;
	}

	public void setPartyAwBIC(String partyAwBIC) {
		this.partyAwBIC = partyAwBIC;
	}

	public String getPartyBeAcc() {
		return partyBeAcc;
	}

	public void setPartyBeAcc(String partyBeAcc) {
		this.partyBeAcc = partyBeAcc;
	}

	public String getPartyBeBIC() {
		return partyBeBIC;
	}

	public void setPartyBeBIC(String partyBeBIC) {
		this.partyBeBIC = partyBeBIC;
	}

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getFwdPartyInBIC() {
		return fwdPartyInBIC;
	}

	public void setFwdPartyInBIC(String fwdPartyInBIC) {
		this.fwdPartyInBIC = fwdPartyInBIC;
	}

	public String getFwdPartyAwBIC() {
		return fwdPartyAwBIC;
	}

	public void setFwdPartyAwBIC(String fwdPartyAwBIC) {
		this.fwdPartyAwBIC = fwdPartyAwBIC;
	}

	public String getFwdPartyBeAcc() {
		return fwdPartyBeAcc;
	}

	public void setFwdPartyBeAcc(String fwdPartyBeAcc) {
		this.fwdPartyBeAcc = fwdPartyBeAcc;
	}

	public String getFwdPartyBeBIC() {
		return fwdPartyBeBIC;
	}

	public void setFwdPartyBeBIC(String fwdPartyBeBIC) {
		this.fwdPartyBeBIC = fwdPartyBeBIC;
	}
	
	
}
