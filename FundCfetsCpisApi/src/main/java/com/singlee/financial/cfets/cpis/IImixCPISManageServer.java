package com.singlee.financial.cfets.cpis;

import java.util.List;

import com.singlee.financial.model.MatchingModel;
import com.singlee.financial.model.QueryModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.hessian.annotation.CfetsCPIS;
import com.singlee.hessian.annotation.CfetsCPISContext;

/**
 * 交易后业务处理类,发送交易后业务对象到Capital Web资金前置系统中
 * 
 * @author chengmj
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
@CfetsCPIS(context = CfetsCPISContext.SINGLEE_API, uri = "/ImixCPISManageServer")
public interface IImixCPISManageServer {

	/**
	 * CFETS登入
	 * 
	 * @return
	 */
	boolean startImixSession();

	/**
	 * CFETS登出
	 * 
	 * @return
	 */
	boolean stopImixSession();
	
	/**
	 * 交易确认接口
	 * @param <T>
	 * 
	 * @param xml
	 * @return
	 */
	<T> boolean confirmCPISTransaction(T mes)throws Exception;
	
	/**
	 * 交易确认接口
	 * @param <T>
	 * 
	 * @param xml
	 * @return
	 */
	boolean confirmCPISTransaction(TradeConfirmBean confirmBean)throws Exception;
	
	List<Object> getRecMessage();
	
	boolean confirmCPISDetail(QueryModel qry);
	
	boolean matchingCPISDetail(MatchingModel qry);
	
	List<Object> getRecDetailMessage();
	
	boolean confirmCPISTransactionOPT(List<SendForeignOptionModel> confirmBean)throws Exception;
	
	boolean getImixSessionState();
	 /**
     * 会话状态
     * @return
     */
    boolean isSessionStarted();

    /**
     * 执行cstp重启脚本
     * @return
     */
    String runShell(String shfile);
}
