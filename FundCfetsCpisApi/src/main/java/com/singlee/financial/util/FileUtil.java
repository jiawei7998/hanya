package com.singlee.financial.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 
 * <P>FileUtil.java</P>
 * <P>文件处理工具</P>
 * <P>Copyright: Copyright (c) 2016</P>
 * <P>Company: 新利信息科技公司</P>
 * @author 彭前
 */
public class FileUtil {
	/**
	 * 
	 * @param ssrcFile
	 * @param sdestFile
	 * @return
	 */
	public static boolean isSameSize(String ssrcFile, String sdestFile) {
		File fsrcFile = new File(ssrcFile);
		File fdestFile = new File(sdestFile);
		return isSameSize(fsrcFile, fdestFile);
	}

	/**
	 * 比较两个文件大小是否相同
	 * 
	 * @param fsrcFile
	 * @param fdestFile
	 * @return
	 */
	public static boolean isSameSize(File fsrcFile, File fdestFile) {
		if (!fsrcFile.isFile() || !fdestFile.isFile()) {
			return false;
		}
		return fsrcFile.length() == fdestFile.length();
	}

	/**
	 * 
	 * @param ssrcFileName
	 * @param sdestFileName
	 * @return
	 */
	public static boolean setModifyTime(String ssrcFileName,
			String sdestFileName) {
		File fsrcFile = new File(ssrcFileName);
		File fdestFile = new File(sdestFileName);
		return fsrcFile.setLastModified(fdestFile.lastModified());
	}

	/**
	 * 
	 * @param sfile
	 * @param lfileTime
	 */
	public static void setModifyTime(String sfile, long lfileTime) {
		File ffile = new File(sfile);
		ffile.setLastModified(lfileTime);
	}

	/**
	 * 比较两个文件的最后修改日期
	 * 
	 * @param fsrcFile
	 * @param fdestFile
	 * @return
	 */
	public static boolean isSameModifyTime(String ssrcFile, String sdestFile) {
		File fsrcFile = new File(ssrcFile);
		File fdestFile = new File(sdestFile);
		return isSameModifyTime(fsrcFile, fdestFile);
	}

	/**
	 * 比较两个文件的最后修改日期
	 * 
	 * @param fsrcFile
	 * @param fdestFile
	 * @return
	 */
	public static boolean isSameModifyTime(File fsrcFile, File fdestFile) {
		if (!fsrcFile.isFile() || !fdestFile.isFile()) {
			return false;
		}
		return fsrcFile.lastModified() == fdestFile.lastModified();
	}

	/**
	 * 新建目录
	 * 
	 * @param folderPath
	 *            String
	 * @return boolean
	 */
	public static void newFolder(String folderPath) {
		String filePath = folderPath;
		filePath = filePath.toString();
		java.io.File myFilePath = new java.io.File(filePath);
		if (!myFilePath.exists()) {
			myFilePath.mkdir();
		}
	}

	/**
	 * 新建文件
	 * 
	 * @param filePathAndName
	 *            String 文件路径及名�?
	 * @param fileContent
	 *            String 文件内容
	 * @return boolean
	 */
	public static void newFile(String filePathAndName, String fileContent) {

		try {
			String filePath = filePathAndName;
			filePath = filePath.toString();
			File myFilePath = new File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.createNewFile();
			}
			FileWriter resultFile = new FileWriter(myFilePath);
			PrintWriter myFile = new PrintWriter(resultFile);
			String strContent = fileContent;
			myFile.println(strContent);
			resultFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 删除文件
	 * 
	 * @param filePathAndName
	 *            String 文件路径及名�?
	 * @param fileContent
	 *            String
	 * @return boolean
	 */
	public static void delFile(String filePathAndName) {
		String filePath = filePathAndName;
		filePath = filePath.toString();
		java.io.File f = new java.io.File(filePath);
		
		boolean result = false;  
		int tryCount = 0;  
		result = f.delete();  
		while(!result && tryCount++ <10)  
		{  
			System.out.println(("try to delete file "+ f.getName() +" cnt:"+tryCount));  
			System.gc();  
			result = f.delete();  
		}  

		

	}

	/**
	 * 删除文件�?
	 * 
	 * @param filePathAndName
	 *            String 文件夹路径及名称
	 * @param fileContent
	 *            String
	 * @return boolean
	 */
	public static void delFolder(String folderPath) {
		delAllFile(folderPath); // 删除完里面所有内�?
		String filePath = folderPath;
		filePath = filePath.toString();
		java.io.File myFilePath = new java.io.File(filePath);
		myFilePath.delete(); // 删除空文件夹

	}

	/**
	 * 删除文件夹里面的�?��文件
	 * 
	 * @param path
	 *            String 文件夹路径
	 */
	public static void delAllFile(String path) {
		File file = new File(path);
		if (!file.exists()) {
			return;
		}
		if (!file.isDirectory()) {
			return;
		}
		String[] tempList = file.list();
		File temp = null;
		for (int i = 0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			} else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件
				delFolder(path + "/" + tempList[i]);// 再删除空文件
			}
		}
	}

	/**
	 * 复制单个文件
	 * 
	 * @param oldPath
	 *            String 原文件路径
	 * @param newPath
	 *            String 复制后路径
	 * @return boolean
	 * @throws IOException 
	 */
	public static void copyFile(String oldPath, String newPath) throws IOException {
		int byteread = 0;
		FileInputStream inStream = null;
		FileOutputStream fs = null;
		try {
			File oldfile = new File(oldPath);
			if (oldfile.exists()) { // 文件存在�?
				inStream = new FileInputStream(oldPath); // 读入原文�?
				fs = new FileOutputStream(newPath);
				byte[] buffer = new byte[32];
				while ((byteread = inStream.read(buffer)) != -1) {
					// System.out.println(bytesum);
					fs.write(buffer, 0, byteread);
				}
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			inStream.close();
			fs.close();
		}

	}

	/**
	 * 复制整个文件夹内�?
	 * 
	 * @param oldPath
	 *            String 原文件路径
	 * @param newPath
	 *            String 复制后路径
	 * @return boolean
	 */
	public static void copyFolder(String oldPath, String newPath) {

		try {
			(new File(newPath)).mkdirs(); // 如果文件夹不存在 则建立新文件�?
			File a = new File(oldPath);
			String[] file = a.list();
			File temp = null;
			for (int i = 0; i < file.length; i++) {
				if (oldPath.endsWith(File.separator)) {
					temp = new File(oldPath + file[i]);
				} else {
					temp = new File(oldPath + File.separator + file[i]);
				}

				if (temp.isFile()) {
					FileInputStream input = new FileInputStream(temp);
					FileOutputStream output = new FileOutputStream(newPath
							+ "/" + (temp.getName()).toString());
					byte[] b = new byte[1024 * 5];
					int len;
					while ((len = input.read(b)) != -1) {
						output.write(b, 0, len);
					}
					output.flush();
					output.close();
					input.close();
				}
				if (temp.isDirectory()) {// 如果是子文件�?
					copyFolder(oldPath + "/" + file[i], newPath + "/" + file[i]);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 移动文件到指定目录
	 * 
	 * @param oldPath
	 *            String
	 * @param newPath
	 *            String
	 */
	public static void moveFile(String oldPath, String newPath) {
		try {
			copyFile(oldPath, newPath);
			delFile(oldPath);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 移动文件到指定目�?
	 * 
	 * @param oldPath
	 *            String
	 * @param newPath
	 *            String
	 */
	public static void moveFolder(String oldPath, String newPath) {
		copyFolder(oldPath, newPath);
		delFolder(oldPath);

	}

	public static void copyFileName(String path) {

		try {

			File a = new File(path);
			String[] file = a.list();
			StringBuffer sb = new StringBuffer();
			String aPath = "prt\\";
			for (int i = 0; i < file.length; i++) {
				if (!path.endsWith(File.separator)) {
					sb.append(aPath + file[i] + "\n");
				}
			}
			System.out.println(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
