package com.singlee.financial.util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * 
 * <P>DateUtil.java</P>
 * <P>时间处理工具类</P>
 * <P>Company: 新利</P>
 * @version 1.0  2016-5-16
 * <P>          修改者姓名 修改内容说明</P>
 * @see     参考类1
 * @author pengqian
 */
public class DateUtil {
	/**
	 * 当前操作系统日期 Calendar
	 */
	private static Calendar calendar = null;
	/**
	 * 日期格式 默认：yyyyMMdd
	 */
	private static String pattern = "yyyyMMdd";
	/**
	 * 时间格式 默认：HH:mm:ss
	 */
	private static String timePattern = "HH:mm:ss";

	/**
	 * 功能描述：获取系统时间 格式：yyyymmdd hh:mm:ss
	 * @return String
	 * @author 
	 * @date 
	 */
	public static String getDateTime() {
		calendar = new GregorianCalendar(TimeZone.getDefault());
		SimpleDateFormat format = new SimpleDateFormat(pattern + "-"
				+ timePattern);
		Date date = calendar.getTime();
		return format.format(date);
	}
	
	/**
	 * 功能描述：获取系统时间 格式： hh:mm:ss
	 * @return String
	 * @author 
	 * @date 
	 */
	public static String getTime() {
		calendar = new GregorianCalendar(TimeZone.getDefault());
		SimpleDateFormat format = new SimpleDateFormat(timePattern);
		Date date = calendar.getTime();
		return format.format(date);
	}
	
	/**
	 * 功能描述：获取系统日期 格式：yyyymmdd
	 * @return String
	 * @author 
	 * @date 
	 */
	public static String getDate() {
		calendar = new GregorianCalendar(TimeZone.getDefault());
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date date = calendar.getTime();
		return format.format(date);
	}
	/**
	 * 功能描述：时间戳转化为Date
	 * @return String
	 * @author 
	 * @date 
	 */
	public static String timestampToString(String timestamp) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Long time = new Long(timestamp);
		return format.format(time);
	}
	/**
	 * 功能描述：时间戳转化为String
	 * @return String
	 * @author 
	 * @throws ParseException 
	 * @date 
	 */
	public static Date timestampToDate(String timestamp) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Long time = new Long(timestamp);
		String d = format.format(time);
		return format.parse(d);
	}

	/**
	 * 按长度把字符串前补0
	 * 
	 * @param s
	 *            需要前补0的字符串
	 * @param len
	 *            生成后的字符串长度
	 * @return len长度前补0的字符串
	 */
	public static String fix0BeforeString(String s, int len) {
		if (null==s) {
			s = "";}
		for (int i = len - s.length(); i > 0; i--) {
			s = "0" + s;
		}
		return s;
	}
	
	/**
	 * 计算两个日期相隔天数。 <br>
	 * 计算结果统一成正数
	 * 
	 * @param date1
	 *            - 指定日期(yyyyMMdd)的其中一个
	 * @param date2
	 *            - 指定日期(yyyyMMdd)的另外一个
	 * @return int - 返回计算后的天数 失败返回-1
	 */
	public static int diffDate(String date1, String date2) {
		try {

			SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyyMMdd");
			Date d1 = simpledateformat.parse(date1);
			Date d2 = simpledateformat.parse(date2);

			long tmp = d1.getTime() - d2.getTime();
			if (tmp < 0) {
				tmp = -tmp;
			}
			int diff = (int) (tmp / (24 * 60 * 60 * 1000));

			return diff;
		} catch (Exception e) {
			// e.printStackTrace();
			return -1;
		}

	}
	
	/**
	 * 返回日历的年字符串
	 * 
	 * @param cal
	 *            日历
	 * @return 日历的年字符串
	 */
	public static String getYear(Calendar cal) {
		return String.valueOf(cal.get(Calendar.YEAR));
	}

	/**
	 * 返回日历的月字符串(两位)
	 * 
	 * @param cal
	 *            日历
	 * @return 返回日历的月字符串(两位)
	 */
	public static String getMonth(Calendar cal) {
		return fix0BeforeString(String
				.valueOf(cal.get(Calendar.MONTH) + 1), 2);
	}

	/**
	 * 返回日历的日字符串(两位)
	 * 
	 * @param cal
	 *            日历
	 * @return 返回日历的日字符串(两位)
	 */
	public static String getDay(Calendar cal) {
		return fix0BeforeString(String.valueOf(cal
				.get(Calendar.DAY_OF_MONTH)), 2);
	}
	
	/**
	 * 返回年
	 * @param date
	 * @return 年
	 * @throws ParseException
	 */
	public static int getYear(String date) throws ParseException {
		return (int)(Integer.parseInt(date) / 10000);
	}

	/**
	 * 返回月
	 * @param date
	 * @return 月
	 * @throws ParseException
	 */
	public static int getMonth(String date) throws ParseException {
		return (int)(Integer.parseInt(date) / 100) % 100;
	}
	/**
	 * 返回日
	 * @param date
	 * @return 日
	 * @throws ParseException
	 */
	public static int getDay(String date) throws ParseException {
		return (int)Integer.parseInt(date) % 100;
	}
	
	/**
	 * 计算两个日期相隔月数。 <br>
	 * 计算结果统一成正数
	 * 
	 * @param date1
	 *            - 指定日期(yyyyMMdd)的其中一个
	 * @param date2
	 *            - 指定日期(yyyyMMdd)的另外一个
	 * @return int - 返回计算后的月数，失败返回-1
	 * 		如：起始日期Y1M1D1   结束日期Y2M2D2
	 *				1、计算Y2M2和Y1M1的间隔月份M0
	 *				2、如果 D2>D1  M=M0+1,否则M=M0
	 */
	public static int diffMonth(int date1, int date2) {
		int diff = 0;
		try {
			if(date2 < date1) {
				int tmp = date2;
				date2 = date1;
				date1 = tmp;
			}
			diff = getYear(date1+"")*12+getMonth(date1+"")-getYear(date2+"")*12-getMonth(date2+"");
			if(diff < 0) {
				diff = -diff;
			}
			if(getDay(date2+"") > getDay(date1+"")) {
				diff = diff+1;
			}
		} catch (ParseException e) {
			return -1;
		}
	    return diff;
	}
	
	
	
	public static String getTradeLimitDays(String startDate, String endDate) {
		int year;
		int month;
		int day;
		if (startDate!=null&&startDate.length()==8&&endDate!=null&&endDate.length()==8) {
			if (startDate.substring(4, 8).equals(endDate.substring(4, 8))) {
				year = Integer.parseInt(endDate.substring(0, 4)) - Integer.parseInt(startDate.substring(0, 4));
				return year+"000000";
			}else if (!startDate.substring(4, 6).equals(endDate.substring(4, 6))&&startDate.substring(6, 8).equals(endDate.substring(6, 8))) {
				month  = diffMonth(Integer.parseInt(startDate),Integer.parseInt(endDate));
				return month+"000";
			}else {
				day = diffDate(startDate,endDate);
				
				if (day>365) {
					year = day%365;
					day = day - 365 * year;
					return year+"000"+ fix0BeforeString(day+"",3);
				}
				
				return day+"";
			} 
		}
		return null;
	}
	
}
