package com.singlee.financial.util;

import java.util.HashMap;

/**
 * 
 * <P>SSSS
 * SequenceUtil.java
 * </P>
 * <P>
 * 序号生成工具类
 * </P>
 * <P>
 * Copyright: Copyright (c) 2016
 * </P>
 * <P>
 * Company: 新利信息科技有限公司
 * @author 彭前
 * </P>
 * 
 */
public class SequenceUtil {

	private static HashMap<String, Integer> seqNumMap = new HashMap<String, Integer>();

	/**
	 * 获取序列号
	 * 
	 * @param name
	 * @return
	 * 
	 */
	public static int getSeqInt(String name) {
		int msgSeqNum = 1000000000;
		if (seqNumMap.get(name) != null) {
			msgSeqNum = seqNumMap.get(name);
		}
		msgSeqNum = msgSeqNum + 1;
		synchronized (seqNumMap) {
			seqNumMap.put(name, msgSeqNum);
		}

		return msgSeqNum;

	}

	/**
	 * 获取序列号
	 * 
	 * @param name
	 * @return
	 */
	public static String getFileSeq(String name) {
		int msgSeqNum = 0;
		if (seqNumMap.get(name) != null) {
			msgSeqNum = seqNumMap.get(name);
		}
		msgSeqNum = msgSeqNum + 1;
		synchronized (seqNumMap) {
			seqNumMap.put(name, msgSeqNum);
		}
		return "" + msgSeqNum;
	}

	public static HashMap<String, Integer> getSeqNumMap() {
		return seqNumMap;
	}

	public static void setSeqNumMap(HashMap<String, Integer> seqNuMap) {
		SequenceUtil.seqNumMap = seqNuMap;
	}

}
