package com.singlee.financial.util;

import java.io.*;
import java.util.*;

/**
 * 
 * <P>PropertiesUtil.java</P>
 * <P>配置文件处理工具类</P>
 * <P>Copyright: Copyright (c) 2016</P>
 * <P>Company: 新利科技有限公司</P>
 * @author 彭前
 */
public class PropertiesUtil extends ArrayList<Object> 
{
	private static final long serialVersionUID = 1L; //序列号
	private String encoding = "UTF-8"; //字符编码
	private String fileName; //文件名
	
	/**
	 * Properties构造方法
	 */
	public PropertiesUtil(){
		try {
			this.setCharacterEncoding(encoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Properties构造方法
	 * @param fileName 文件名
	 * @param encoding 编码方式
	 */
	public PropertiesUtil(String fileName, String encoding) 
	{
		try 
		{
			//super(Arrays.asList(read(fileName, encoding).split("\n")));
			this.setFileName(fileName);
			this.setCharacterEncoding(encoding);
			if (!isFileExist(fileName))
			{
				this.write("");
			}
			this.addAll(Arrays.asList(read(fileName, encoding).split("\n")));
		} 
		catch (Exception ex) 
		{ 
			ex.printStackTrace();
		} 
	}
	/**
	 * Properties构造方法
	 * @param fileName 文件名
	 * @param encoding 编码方式
	 */
	public PropertiesUtil(String fileName) 
	{
		try 
		{
			//super(Arrays.asList(read(fileName, encoding).split("\n")));
			this.setFileName(fileName);
			this.setCharacterEncoding(encoding);
			if (!isFileExist(fileName))
			{
				this.write("");
			}
			this.addAll(Arrays.asList(read(fileName, encoding).split("\n")));
		} 
		catch (Exception ex) 
		{ 
			ex.printStackTrace();
		} 
	}
	/**
	 * 设置文件编码方式
	 * @param encoding 编码方式
	 * @throws UnsupportedEncodingException
	 */
	private void setCharacterEncoding(String encoding) throws UnsupportedEncodingException 
	{ 
		new String("".getBytes("iso-8859-1"), encoding);  
		this.encoding = encoding;  
	}
	/**
	 * 判断文件是否存在
	 * @param fileName 文件名
	 * return boolean
	 */
	public static boolean isFileExist(String fileName) 
	{
		return new File(fileName).isFile();  
	}
	/**
	 * 读取文件内容
	 * @param fileName 文件名
	 * @param encoding 编码方式
	 * @return String
	 * @throws IOException
	 */
 /*public static String read(String fileName, String encoding) throws IOException 
	{
		StringBuffer sb = new StringBuffer(); 
//		FileReader fr = new FileReader(fileName);
		BufferedReader in = null;
		FileInputStream fr = null;
		fr = new FileInputStream(fileName);
//	    brd = new BufferedReader( new InputStreamReader(fr, "UTF-8") );
		
//		BufferedReader in = new BufferedReader(fr);
		in =  new BufferedReader( new InputStreamReader(fr, encoding) );
		String s;
		while ((s = in.readLine()) != null) 
		{
			sb.append(s);
			// new String(s.getBytes("iso8859_1"), encoding));
			sb.append("\n");
		}  
	    fr.close();
		in.close();
		return sb.toString();
	}
	*/
	/**
	 * 写入文件内容
	 * @param text 内容
	 */
	public void write(String text) throws IOException 
	{
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));  
		out.print(text);
		out.close();  
	}
	/**
	 * 保存文件内容
	 * @throws IOException
	 */
	public void save() throws IOException 
	{
				
		OutputStreamWriter ow = new OutputStreamWriter(new FileOutputStream(fileName),encoding);
		String tmp;
		for (int i = 0; i < size(); i++) 
		{  
			tmp = get(i) + ""; 
			ow.write(tmp);
			ow.write("\n");
		}
		ow.close();
	}
	/**
	 * 设置properties文件的指定内容的值
	 * @param key 键
	 * @param val 值
	 */
	public void setProperties(String key, String val) 
	{
		int ipos = findKey(key);  
		if (ipos >= 0)  
		{
			this.set(ipos, key + "=" + val);
		}
		else
		{
			this.add(key + "=" + val);
		}
	}
	/**
	 * 设置properties文件的指定内容的值
	 * @param key 键
	 * @param val 值
	 */
	public void setProperties(String key, int val) 
	{
		int ipos = findKey(key);  
		if (ipos >= 0)  
		{
			this.set(ipos, key + "=" + val);
		}
		else
		{
			this.add(key + "=" + val);
		}
	}
	
	/**
	 * 查找properties文件的指定的值
	 * @param key 键
	 * @return int
	 */
	public int findKey(String key) 
	{
		try 
		{  
		  String tmp;  
		  for (int i = 0; i < size(); i++) 
		  {  
			  tmp = get(i) + "";  
//			  tmp = new String(tmp.getBytes("iso8859_1"), encoding);  
			  if (tmp.indexOf(key) == 0) 
			  {  
				  return i;  
			  }  
		  }  
		} 
		catch (Exception e) 
		{  
		}  
		return -1;  
	}
	/**
	 * 查找properties文件的指定的值
	 * @return ArrayList
	 */
	public ArrayList<String> getKeys() 
	{
		ArrayList<String> ret=new ArrayList<String>();
		try 
		{  
		  String tmp;  
		  for (int i = 0; i < size(); i++) 
		  {  
			  tmp = get(i) + "";  
//			  tmp = new String(tmp.getBytes("iso8859_1"), encoding);  
			  if (tmp.startsWith("#"))
			  {  
				  
			  } 
			  else
			  {
				  if(tmp.indexOf("=")>0)
				  {
					  ret.add(tmp.substring(0,tmp.indexOf("=")));
				  }
			  }
		  }  
		} 
		catch (Exception e) 
		{  
		}  
		return ret;  
	}
	/**
	 * 设置properties文件的指定的值
	 * @param key
	 * @param memo
	 */
	public void setMemo(String key, String memo) 
	{
		if ("".equals(key)) 
		{  
		  this.add("#" + memo);  
		  return; 
		}
		  
		String tmp;  
		int ret = findKey(key);  
		if (ret == -1)
		{
			this.add("#" + memo);  
			this.add(key + "=");  
		} 
		else 
		{
			int ipos = ret - 1;  
			if (ipos < 0)  
			{
				this.add(ipos, "#" + memo);
			}
			else
			{
				tmp = this.get(ipos) + "";
				if(tmp==null|| "".equals(tmp.trim()))
				{
					
				}
				else
				{
					if ("#".equals(tmp.substring(0, 1)))
					{
						this.set(ipos, "#" + memo);
					}
					else
					{
						this.add(ipos + 1, "#" + memo);
					}
				}
			}
		}
	}  
	/**
	 * 读取 properties文件的指定的值
	 * @param key
	 * @return String
	 */
	public String getMemo(String key) 
	{
		if ("".equals(key)) 
		{  
			return "";
		}
		  
		String tmp;  
		int ret = findKey(key);  
		if (ret == -1)
		{
			return "";
		} 
		else 
		{
			int ipos = ret - 1;  
			if (ipos < 0)  
			{
				return "";
			}
			else
			{
				tmp = this.get(ipos) + "";
				if(tmp==null|| "".equals(tmp.trim()))
				{
					return "";
				}
				else
				{
					if ("#".equals(tmp.substring(0, 1)))
					{
						return tmp.substring(1, tmp.length());
					}
					else
					{
						return "";
					}
				}
			}
		}
	}
	/**
	 * 设置 properties文件的头
	 * @param title
	 */
	public void setTitle(String title) 
	{
		String tmp = this.get(0) + "";  
		if (tmp==null || tmp.length()==0) { 
			tmp ="";  }
		else  {
			tmp = tmp.substring(0, 1);
		}
		if ("#".equals(tmp))
		{
			this.set(0, "#" + title);
		}
		else
		{  
			this.add(0,"");  
			this.add(0, "#" + title);  
		}  
	}
	/**
	 * 读取  properties文件的指定属性
	 * @param key
	 * @return String
	 */
	public String getProperties(String key) 
	{
		return getProperties(key, "");  
	}
	/**
	 * 读取  properties文件的指定属性
	 * @param key
	 * @param defaultStr
	 * @return String
	 */
	/*public String getProperties(String key, String defaultStr) 
	{  
		String tmp, ret;  
		try 
		{  
			for (int i = 0; i < size(); i++) 
			{  
				tmp = get(i) + "";
				
//				tmp = new String(encoding);  
				if (tmp.indexOf(key) == 0) 
				{  
					ret = tmp.substring(key.length() + 1);  
					return ret.trim().replace("\"", "").replace(";", "");  
				}  
			}  
		} 
		catch (Exception e) 
		{  
		}  
		return defaultStr;  
	}*/
	/**
	 * 获取文件名
	 * @return String
	 */
	public String getFileName() 
	{  
		return fileName;  
	}
	/**
	 * 设置文件名
	 * @param fileName
	 * @return String
	 */
	public void setFileName(String fileName) 
	{  
		this.fileName = fileName;  
	}
	
	/**
	 * 读取文件内容
	 * @param fileName 文件名
	 * @param encoding 编码方式
	 * @return String
	 * @throws IOException
	 */
	public static String read(String fileName, String encoding) throws IOException 
	{
		StringBuffer sb = new StringBuffer(); 
//		FileReader fr = new FileReader(fileName);
		BufferedReader in = null;
		//FileInputStream fr = null;
		//fr = new FileInputStream(fileName);
//	    brd = new BufferedReader( new InputStreamReader(fr, "UTF-8") );
		
//		BufferedReader in = new BufferedReader(fr);
		InputStream ins = PropertiesUtil.class.getResourceAsStream("/" + fileName);
		in =  new BufferedReader( new InputStreamReader(ins, encoding) );
		String s;
		while ((s = in.readLine()) != null) 
		{
			sb.append(s);
			// new String(s.getBytes("iso8859_1"), encoding));
			sb.append("\n");
		}  
	    //fr.close();
		ins.close();
		in.close();
		return sb.toString();
	}
	
	public String getProperties(String key, String defaultStr) 
	{  
		String tmp, ret;  
		try 
		{  
			for (int i = 0; i < size(); i++) 
			{  
				tmp = get(i) + "";
				
//				tmp = new String(encoding);  
				if (tmp.indexOf(key) == 0) 
				{  
					ret = tmp.substring(key.length() + 1);  
					return ret.trim().replace("\"", "").replace(";", "");  
				}  
			}  
		} 
		catch (Exception e) 
		{  
		}  
		return defaultStr;  
	}
} 

