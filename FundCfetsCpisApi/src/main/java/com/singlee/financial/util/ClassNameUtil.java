package com.singlee.financial.util;
/**
 * <p>获取非限定性类名的工具类  </p>
 * 
 * @param Object
 * <p>新利信息科技公司 </p>
 * @author 彭前
 *
 */
public class ClassNameUtil {
	public static String getClassName(Object object){
		String name = object.getClass().getName();
		int i = name.lastIndexOf(".");
		String serviceType = name.substring((i+1),name.length());
		return serviceType;
		
	}

}
