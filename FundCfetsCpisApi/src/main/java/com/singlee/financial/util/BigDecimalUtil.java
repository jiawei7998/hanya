package com.singlee.financial.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
/**
 * 
 * <P>DateUtil.java</P>
 * <P>精确度处理工具类</P>
 * <P>Copyright: Copyright (c) 2018</P>
 * <P>Company:新利信息科技有限公司</P>
 * @author  彭前
 * 
 */

public class BigDecimalUtil {
	/**
	 * 按照特定精度取值
	 * 
	 * @param v1
	 *            String number
	 * @param v2
	 *            String 精度(如 0.00)
	 * 
	 * @return 按照精度取值之后的结果
	 */

	public static String decimalFormat(String number, String format) {
		DecimalFormat decformat = new DecimalFormat(format);
		return decformat.format(new BigDecimal(number));
	}

	/**
	 * 把一个double类型的数 转成String类型，要求小数点后有decimalCount个数字
	 * 
	 * @param 
	 * 			number 要精确的数
	 * 
	 * @param 
	 * 			小数点后面精确的位数
	 *         
	 * @return 按照精度取值之后的结果
	 */
	public static String decimalFormat(String number, int decimalCount) {
		BigDecimal bd = new BigDecimal(number);
		String format = "0";
		// 小数点后 位数达不到指定位数，补0
		if (decimalCount > 0) {
			format = "0.";
			for (int i = 0; i < decimalCount; i++) {
				format += "0";
			}
		}
		DecimalFormat df = new DecimalFormat(format);
		String result = df.format(bd);
		return result;
	}

	/**
	 * 提供精确的小数位四舍五入处理。
	 * 
	 * @param v
	 *            需要四舍五入的数字
	 * @param scale
	 *            小数点后保留几位
	 * @return 四舍五入后的结果
	 */
	public static String round(double v, int scale) {

		if (scale < 0) {
			throw new IllegalArgumentException(
			"The scale must be a positive integer or zero");
		}
		
		DecimalFormat format = new DecimalFormat(fixZero("##.#",scale,"#"));
		
		BigDecimal b = new BigDecimal(Double.toString(v));
		BigDecimal one = new BigDecimal("1");
		return format.format(b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue());
		
	}
	
	/**
	 * 小数点后补0
	 * 
	 * @param v
	 *            需要补0的数字
	 * @param scale
	 *            小数点后补几位
	 * @return 补的结果
	 */
	public static String fixZero(String fixStr ,int scale,String str){
		
		 //获取小数点的位置
   	  	 int num = 0;
   	  	 num = fixStr.indexOf(".");
   	  	 //获取小数点后面的是否补足
   	  	 if (num != -1) {
   	  		 String dianAfter = fixStr.substring(0,num+1);
   	  		 String afterData = fixStr.replace(dianAfter, "");
   	  		 int length = afterData.length();
   	  		 if( length < scale ){
   	  			 for (int i = 0; i < scale-length ; i++) {
   	  				 afterData = afterData + str ;
   	  			 }
   	  		 }
   	  		 return fixStr.substring(0,num) + "." + afterData;
		}else {
   	  		 String afterData = "";
   	  		 int length = afterData.length();
   	  		 if( 0 < scale ){
   	  			 for (int i = 0; i < scale-length ; i++) {
   	  				 afterData = afterData + str ;
   	  			 }
   	  		 }
   	  		 return fixStr + "." + afterData;	
		}
	}
}




