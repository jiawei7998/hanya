package com.singlee.financial.util;

import java.util.List;

import com.singlee.financial.model.AgencyBasicInformationModel;


/**
 * 基础信息处理类
 * 
 * @param list
 * @param PartySubIDType
 * @return 根据传入PartySubIDType返回PartySubID
 * 
 * @author 彭前
 * 
 */
public class AgencyBasicInformationUtil {
	public static String getPartySubID(List<AgencyBasicInformationModel> list,
			String PartySubIDType) {
		if (list != null && list.size() > 0) {
			for (AgencyBasicInformationModel agencyBasicInformationModel : list) {
				if (PartySubIDType.equals(agencyBasicInformationModel.getPartySubIDType())) {
					return agencyBasicInformationModel.getPartySubID();
				}
			}
		}
		return "";
	}

}
