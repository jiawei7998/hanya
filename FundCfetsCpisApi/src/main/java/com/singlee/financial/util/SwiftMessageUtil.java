package com.singlee.financial.util;

import org.apache.commons.lang.StringUtils;

/**
 * <p>拆借Swift报文处理类  </p>
 * @param 需要被切割的报文
 * @param 要切割的
 * <p>新利信息科技公司 </p>
 * @author 彭前
 * 
 */

public class SwiftMessageUtil {

	public static String fun(String data, String r) {
		// 解析中间行账户
		if (data.contains(r)) {
			if (data.contains(r + "/")) {
				int i = data.indexOf(r);
				String s = data.substring(i + 6, data.length() - 1);
				// 截取到余下字符串到第二个空格之间的字符
				String result = s.substring(0,
				s.indexOf("\n", s.indexOf("\n") + 1));
				System.out.println(result);
				return result;
			} else {
				int i = data.indexOf(r);
				String substring = data.substring(i + 5, data.length() - 1);
				String result = StringUtils.substringBefore(substring, "\n");
				System.out.println(result);
				return result;
			}
		}
		return "";
	}
}