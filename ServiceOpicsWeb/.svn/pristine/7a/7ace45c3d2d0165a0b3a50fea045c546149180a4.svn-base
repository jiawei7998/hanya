package com.singlee.financial.opics;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlBondPositionBalanceBean;
import com.singlee.financial.bean.SlCdIssueBalanceBean;
import com.singlee.financial.bean.SlCommonBean;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProfitLossBean;
import com.singlee.financial.bean.SlSdCdInvestHistoryBean;
import com.singlee.financial.bean.SlSdCdProfitAndLossBean;
import com.singlee.financial.bean.SlSposBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.ExternalMapper;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.RetStatusEnum;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BaseServer implements IBaseServer {

	@Autowired
	ExternalMapper externalMapper;
	@Autowired
	CommonMapper commonMapper;

	@Override
	public SlInthBean getInthStatus(SlInthBean inthBean) {
		return externalMapper.getSlFundInth(inthBean);
	}

	@Override
	public List<SlInthBean> getInthStatusList(SlInthBean inthBean) {
		return externalMapper.getSlFundInthList(inthBean);
	}

	@Override
	public Date getOpicsSysDate(String br) {
		return commonMapper.getOpicsSysDate(br);
	}
	
	@Override
	public Date getOpicsPrevbranprcDate(String br)
			throws RemoteConnectFailureException, Exception {
		return commonMapper.getOpicsPrevbranprcDate(br);
	}

	@Override
	public Date getOpicsNextbranprcdate(String br) throws RemoteConnectFailureException, Exception {
		return commonMapper.getOpicsNextbranprcdate(br);
	}

	@Override
	public SlSposBean getSpos(Map<String, Object> arg0)
			throws RemoteConnectFailureException, Exception {
		return commonMapper.getSpos(arg0);
	}

	@Override
	public SlCommonBean queryMarketData(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		
		return commonMapper.queryMarketData(map);
	}

	@Override
	public PageInfo<SlBondPositionBalanceBean> getBondPositionBalance(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlBondPositionBalanceBean> page=commonMapper.getBondPositionBalance(map,rb);
		return new PageInfo<SlBondPositionBalanceBean>(page);
	}

	@Override
	public PageInfo<SlSdCdInvestHistoryBean> getCdInvestHistory(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlSdCdInvestHistoryBean> page=commonMapper.getCdInvestHistory(map,rb);
		return new PageInfo<SlSdCdInvestHistoryBean>(page);
	}

	@Override
	public PageInfo<SlSdCdInvestHistoryBean> getSdInvestHistory(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlSdCdInvestHistoryBean> page=commonMapper.getSdInvestHistory(map,rb);
		return new PageInfo<SlSdCdInvestHistoryBean>(page);
	}
	
	@Override
	public List<SlSdCdProfitAndLossBean> getCdProfitAndLoss(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		return commonMapper.getCdProfitAndLoss(map);
	}

	@Override
	public List<SlSdCdProfitAndLossBean> getSdProfitAndLoss(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		return commonMapper.getSdProfitAndLoss(map);
	}

	@Override
	public SlOutBean callCdInvestHistory(Map<String, String> arg0)
			throws RemoteConnectFailureException, Exception {
		return commonMapper.callCdInvestHistory(arg0);
	}

	@Override
	public SlOutBean callCdProfitLoss(Map<String, String> arg0)
			throws RemoteConnectFailureException, Exception {
		return commonMapper.callCdProfitLoss(arg0);
	}

	@Override
	public SlOutBean callSdInvestHistory(Map<String, String> arg0)
			throws RemoteConnectFailureException, Exception {
		return commonMapper.callSdInvestHistory(arg0);
	}

	@Override
	public SlOutBean callSdProfitLoss(Map<String, String> arg0)
			throws RemoteConnectFailureException, Exception {
		return commonMapper.callSdProfitLoss(arg0);
	}

	@Override
	public PageInfo<SlCdIssueBalanceBean> getCdIssueBalance(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlCdIssueBalanceBean> page=commonMapper.getCdIssueBalance(map,rb);
		return new PageInfo<SlCdIssueBalanceBean>(page);
	}

	@Override
	public List<SlBondPositionBalanceBean> getBondPositionBalanceExport(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		return commonMapper.getBondPositionBalanceExport(map);
	}

	@Override
	public List<SlSdCdInvestHistoryBean> getCdInvestHistoryExport(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		return commonMapper.getCdInvestHistoryExport(map);
	}

	@Override
	public List<SlCdIssueBalanceBean> getCdIssueBalanceExport(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		return commonMapper.getCdIssueBalanceExport(map);
	}

	@Override
	public List<SlSdCdInvestHistoryBean> getSdInvestHistoryExport(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		return commonMapper.getSdInvestHistoryExport(map);
	}

	@Override
	public List<SlCustBean> getListOpicsCust(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return commonMapper.getListOpicsCust(map);
		
	}

	@Override
	public List<String> getTermsFromCcyp(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return commonMapper.getTermsFromCcyp(map);
	}

	
	@Override
    public String getProfitLoss(Map<String, Object> map)
              throws RemoteConnectFailureException, Exception {
         String profitLoss="";
         String prodtype=(String)map.get("prodtype");
         String invtype=(String)map.get("invtype");
         SlProfitLossBean bean=commonMapper.getProfitLoss();
         if("SD".equals(prodtype)&& "T".equals(invtype)){
              profitLoss=String.valueOf(bean==null?"":bean.gettProfitLossSd());
         }else if("SD".equals(prodtype)&& "A".equals(invtype)){
              profitLoss=String.valueOf(bean==null?"":bean.getaProfitLossSd());
         }else if("CD".equals(prodtype)&& "T".equals(invtype)){
              profitLoss=String.valueOf(bean==null?"":bean.gettProfitLossCd());
         }else if("CD".equals(prodtype)&& "A".equals(invtype)){
              profitLoss=String.valueOf(bean==null?"":bean.getaProfitLossCd());
         }else if("FX".equals(prodtype)){
              profitLoss=String.valueOf(bean==null?"":bean.getFxProfitLoss());
         }
         return profitLoss;
    }

	@Override
    public SlOutBean checkProfitLossH() throws RemoteConnectFailureException, Exception {
         SlOutBean outbean=new SlOutBean();
         int count=commonMapper.checkProfitLossH();
         if(count>0) {//已有数据
              outbean.setRetStatus(RetStatusEnum.F);
              outbean.setRetCode("888");
         }else {
              outbean.setRetStatus(RetStatusEnum.S);
              outbean.setRetCode("999");
         }
         return outbean;
    }

	@Override
    public SlOutBean callProfitLossInit(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
         SlOutBean outbean=new SlOutBean(RetStatusEnum.S, "999", "生成数据成功");
         commonMapper.callProfitLossInit(map);
         if(map.get("RETCODE")== null||!"999".equals(map.get("RETCODE"))){ //调用存储过程失败
              outbean.setRetCode((String)map.get("RETCODE"));
              outbean.setRetMsg((String)map.get("RETMSG"));
         }
         return outbean;
    }

	@Override
    public SlOutBean callProfitLossH(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
         SlOutBean outbean=new SlOutBean(RetStatusEnum.S, "999", "生成数据成功");
         commonMapper.callProfitLossH(map);
         if(map.get("RETCODE")== null||!"999".equals(map.get("RETCODE"))){ //调用存储过程失败
              outbean.setRetCode((String)map.get("RETCODE"));
              outbean.setRetMsg((String)map.get("RETMSG"));
         }
         return outbean;
    }


	

}
