<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.singlee.financial.mapper.SlQuotedPriceMapper" >

    <resultMap id="BaseResultMap" type="com.singlee.financial.bean.SlQuotedPrice" >
        <id column="CCY_CODE" property="ccyCode" jdbcType="VARCHAR" />
        <id column="CTR_CCY_CODE" property="ctrCcyCode" jdbcType="VARCHAR" />
        <result column="EFF_DATE" property="effDate" jdbcType="VARCHAR" />
        <result column="EFF_TIME" property="effTime" jdbcType="VARCHAR" />
        <result column="CCY" property="ccy" jdbcType="VARCHAR" />
        <result column="CTR_CCY" property="ctrCcy" jdbcType="VARCHAR" />
        <result column="PRICE" property="price" jdbcType="DECIMAL" />
        <result column="MIN_PRICE" property="minPrice" jdbcType="DECIMAL" />
        <result column="MAX_PRICE" property="maxPrice" jdbcType="DECIMAL" />
        <result column="MID_PRICE" property="midPrice" jdbcType="DECIMAL" />
        <result column="HOT_BUY_PRICE" property="hotBuyPrice" jdbcType="DECIMAL" />
        <result column="HOT_SELL_PRICE" property="hotSellPrice" jdbcType="DECIMAL" />
        <result column="BUY_PRICE" property="buyPrice" jdbcType="DECIMAL" />
        <result column="SELL_PRICE" property="sellPrice" jdbcType="DECIMAL" />
        <result column="TRADE_AREA_FLAG" property="tradeAreaFlag" jdbcType="VARCHAR" />
        <result column="PRICE_TYPE" property="priceType" jdbcType="VARCHAR" />
        <result column="post_date" property="postDate" jdbcType="VARCHAR" />
    </resultMap>

    <sql id="Base_Column_List" >
        CCY_CODE, CTR_CCY_CODE, EFF_DATE, EFF_TIME, CCY, CTR_CCY, PRICE, MIN_PRICE, MAX_PRICE, 
        MID_PRICE, HOT_BUY_PRICE, HOT_SELL_PRICE, BUY_PRICE, SELL_PRICE, TRADE_AREA_FLAG, 
        PRICE_TYPE,post_date
    </sql>
	<sql id="Column_List" >
        formula, rate_8, INPUT_DATE, errorcode
    </sql>
    <resultMap id="ResultMap" type="com.singlee.financial.bean.TRtrate" >
        <id column="formula" property="formula"/>
        <id column="INPUT_DATE" property="inputDate" />
        <id column="errorcode" property="errorcode"/>
        <result column="rate_8" property="rate_8"/>
    </resultMap>
    
    <select id="selectByPrimaryKey" resultMap="BaseResultMap" parameterType="com.singlee.financial.bean.SlQuotedPrice" >
        select 
        <include refid="Base_Column_List" />
        from SL_QUOTED_PRICE
        where CCY_CODE = #{ccyCode,jdbcType=VARCHAR}
          and CTR_CCY_CODE = #{ctrCcyCode,jdbcType=VARCHAR}
    </select>

    <select id="getQuotedPriceByPostDate" resultMap="BaseResultMap" parameterType="String" >
        select 
        <include refid="Base_Column_List" />
        from SL_QUOTED_PRICE
        where post_date = #{postDate,jdbcType=VARCHAR}
    </select>
    <delete id="deleteByPrimaryKey" parameterType="com.singlee.financial.bean.SlQuotedPrice" >
        delete from SL_QUOTED_PRICE
        where CCY_CODE = #{ccyCode,jdbcType=VARCHAR}
          and CTR_CCY_CODE = #{ctrCcyCode,jdbcType=VARCHAR}
    </delete>
	<select id="selectTRtrate" resultMap="ResultMap" parameterType="com.singlee.financial.bean.TRtrate" >
        select 
        <include refid="Column_List" /> 
        from T_RTrate
        where RTRIM(formula) = #{formula,jdbcType=VARCHAR}
          and RTRIM(INPUT_DATE) = to_date(#{inputDate,jdbcType=TIMESTAMP},'yyyy/mm/dd')
          and RTRIM(errorCode) = #{errorcode,jdbcType=VARCHAR}
    </select>
	<insert id="insertTRtrate" parameterType="com.singlee.financial.bean.TRtrate" >
        insert into T_RTrate (formula, rate_8, INPUT_DATE,errorCode)
        values (#{formula,jdbcType=VARCHAR},#{rate_8,jdbcType=DECIMAL}, to_date(#{inputDate,jdbcType=TIMESTAMP},'yyyy/mm/dd'), #{errorcode,jdbcType=VARCHAR})
    </insert>
    <update id="updateTRtrate" parameterType="com.singlee.financial.bean.TRtrate" >
        update T_RTrate
        set rate_8 = #{rate_8,jdbcType=DECIMAL}
        where formula = #{formula,jdbcType=VARCHAR}
          and INPUT_DATE = to_date(#{inputDate,jdbcType=TIMESTAMP},'yyyy/mm/dd')
          and errorCode = #{errorcode,jdbcType=VARCHAR}
    </update>
    <insert id="insert" parameterType="com.singlee.financial.bean.SlQuotedPrice" >
        insert into SL_QUOTED_PRICE (CCY_CODE, CTR_CCY_CODE, EFF_DATE, 
            EFF_TIME, CCY, CTR_CCY, 
            PRICE, MIN_PRICE, MAX_PRICE, 
            MID_PRICE, HOT_BUY_PRICE, HOT_SELL_PRICE, 
            BUY_PRICE, SELL_PRICE, TRADE_AREA_FLAG, 
            PRICE_TYPE,post_date)
        values (#{ccyCode,jdbcType=VARCHAR}, #{ctrCcyCode,jdbcType=VARCHAR}, #{effDate,jdbcType=VARCHAR}, 
            #{effTime,jdbcType=VARCHAR}, #{ccy,jdbcType=VARCHAR}, #{ctrCcy,jdbcType=VARCHAR}, 
            #{price,jdbcType=DECIMAL}, #{minPrice,jdbcType=DECIMAL}, #{maxPrice,jdbcType=DECIMAL}, 
            #{midPrice,jdbcType=DECIMAL}, #{hotBuyPrice,jdbcType=DECIMAL}, #{hotSellPrice,jdbcType=DECIMAL}, 
            #{buyPrice,jdbcType=DECIMAL}, #{sellPrice,jdbcType=DECIMAL}, #{tradeAreaFlag,jdbcType=VARCHAR}, 
            #{priceType,jdbcType=VARCHAR},#{postDate,jdbcType=VARCHAR})
    </insert>
    <update id="updateByPrimaryKey" parameterType="com.singlee.financial.bean.SlQuotedPrice" >
        update SL_QUOTED_PRICE
        set EFF_DATE = #{effDate,jdbcType=VARCHAR},
            EFF_TIME = #{effTime,jdbcType=VARCHAR},
            CCY = #{ccy,jdbcType=VARCHAR},
            CTR_CCY = #{ctrCcy,jdbcType=VARCHAR},
            PRICE = #{price,jdbcType=DECIMAL},
            MIN_PRICE = #{minPrice,jdbcType=DECIMAL},
            MAX_PRICE = #{maxPrice,jdbcType=DECIMAL},
            MID_PRICE = #{midPrice,jdbcType=DECIMAL},
            HOT_BUY_PRICE = #{hotBuyPrice,jdbcType=DECIMAL},
            HOT_SELL_PRICE = #{hotSellPrice,jdbcType=DECIMAL},
            BUY_PRICE = #{buyPrice,jdbcType=DECIMAL},
            SELL_PRICE = #{sellPrice,jdbcType=DECIMAL},
            TRADE_AREA_FLAG = #{tradeAreaFlag,jdbcType=VARCHAR},
            PRICE_TYPE = #{priceType,jdbcType=VARCHAR},
            post_date = #{postDate,jdbcType=VARCHAR}
        where CCY_CODE = #{ccyCode,jdbcType=VARCHAR}
          and CTR_CCY_CODE = #{ctrCcyCode,jdbcType=VARCHAR}
    </update>
    <!-- 查询价格（债券上日中债估值净价）-->
   <select id="getQuotedPriceList" parameterType="map" resultMap="BaseResultMap">
	 	select * from SL_QUOTED_PRICE T
	 	<where>
		1=1
		<if test="ccy != null and ccy != ''">
				AND T.ccy = #{ccy}
		</if>
		<if test="ctrCcy != null and ctrCcy != ''">
			AND I.CTR_CCY = #{ctrCcy}
		</if>
		</where>
   </select>
</mapper>