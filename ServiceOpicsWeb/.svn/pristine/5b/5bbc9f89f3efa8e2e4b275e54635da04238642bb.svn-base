package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.SwiftMethod;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public interface PmtqService {

    int deleteByPrimaryKey(String br, String type, String dealno, String seq, String product, Date cdate, String ctime,
        String payrecind);
    int deletePmtqByVdate(String br, String type, String dealno, String product, Date vdate);
    int deleteReverseByPrimaryKey(String br, String type, String dealno, String product, Date vdate);

    int insert(Pmtq record);

    int insertSelective(Pmtq record);

    Pmtq selectByPrimaryKey(String br, String type, String dealno, String seq, String product, Date cdate, String ctime,
        String payrecind);
    List<Pmtq> selectReverseByPrimaryKey(String br, String type, String dealno, String product);

    int updateByPrimaryKeySelective(Pmtq record);

    int updateByPrimaryKey(Pmtq record);
    int updateReverseByPrimaryKey(String br,String product,String type,String dealno,String seq,Date postdate);

    void updateBatch(List<Pmtq> list);

    void batchInsert(@Param("list") List<Pmtq> list);

    void addDefSi(@Param("list") List<Pmtq> list);

    /**
     * 方法已重载.根据拆借交易数据、现金流数据构建支付指令对象(PMTQ)
     * 
     * @param dl
     * @param schd
     * @return
     */
    List<Pmtq> builder(Dldt dl, List<Schd> schd);

    /**
     * 
     * @param fx
     * @return
     */
    List<Pmtq> builder(Fxdh fx,SwiftMethod swiftMethod);

    /**
     * 
     * @param otc
     * @return
     */
    Pmtq builder(Otdt otc);

    /**
     * 
     * @param swdh
     * @param swdt
     * @param swds
     * @return
     */
    List<Pmtq> builder(Swdh swdh, List<Swds> swdts,Prir prir,Psix psix);

    Pmtq builder(Swdh swdh,Swds swds);
    /**
     * 
     * @param spsh
     * @return
     */
    List<Pmtq> builder(Spsh spsh);
    /**
     * 
     * @param rdfh
     * @return
     */
    List<Pmtq> builder(Rdfh rdfh);
    
    /**
     * 
     * @param spsh
     * @return
     */
    List<Pmtq> builder(Rprh rprh,List<Rpdt> rpdt);
    
    List<Pmtq> builder(Sldh sldh);
    
    /**
     * 删除大于新到期日的记录
     * @param br
     * @param dealno
     * @param seq
     * @param product
     * @param type
     * @param amenddate
     * @param amendtime
     * @param suppressccy
     * @param suppresssec
     * @param swiftfmtNotin
     * @param statusIn
     * @param vdateIn
     * @param convrev
     */
    void callSpUpdrecPmtqSwiftfmt(String br,String dealno,String seq,String product,String type,
    		Date amenddate,String amendtime,String suppressccy,String suppresssec,String swiftfmtNotin,
    		String statusIn,Date vdateIn,BigDecimal convrev);
    
    /**
     * 方法已重载.利率互换交易冲销时,生成取消报文记录
     * @param list
     * @param swdh
     * @return
     */
    List<Pmtq> builder(Date revdate,List<Pmtq> list);
}
