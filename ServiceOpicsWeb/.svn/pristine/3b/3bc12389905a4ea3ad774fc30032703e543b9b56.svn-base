package com.singlee.financial.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

/**
 * FTP上传下载
 * 
 * @author shenzl
 * 
 */
public class SingleeFTPClient {

	private static Logger logger = Logger.getLogger(SingleeFTPClient.class);

	/**
	 * Description: 向FTP服务器上传文件
	 * 
	 * @Version1.0
	 * 
	 * @param url      FTP服务器hostname
	 * @param port     FTP服务器端口
	 * @param username FTP登录账号
	 * @param password FTP登录密码
	 * @param path     FTP服务器保存目录
	 * @param filename 上传到FTP服务器上的文件名
	 * @param input    输入流
	 * @return 成功返回true，否则返回false
	 */
	public static boolean uploadFile(String url, int port, String username, String password, String path,
			String filename, InputStream input) {
		boolean success = false;
		FTPClient ftp = new FTPClient();
		try {
			int reply;
			ftp.connect(url, port);// 连接FTP服务器
			// 如果采用默认端口，可以使用ftp.connect(url)的方式直接连接FTP服务器
			ftp.login(username, password);// 登录
			ftp.enterLocalPassiveMode();// 设置被动模式
			// ftp.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);// 设置以二进制方式传输
			reply = ftp.getReplyCode(); // FTP响应码
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return success;
			}
			// 没有目录则新建目录
			FTPFile[] fs = ftp.listFiles(path);
			if (fs.length == 0) {// 判断目录是否存在 --创建ftp服务器保存文件目录
				ftp.makeDirectory(path);
			}
			if (ftp.cwd(path) == 250) { // 改变保存目录
				ftp.storeFile(filename, input);// ftp服务端保存文件
				ftp.logout();
				success = true;
			}
		} catch (IOException e) {
			logger.info(e);
		} finally {
			if (ftp.isConnected()) {
				try {
					if (input != null) {
						input.close();
					}
					ftp.disconnect();
				} catch (IOException ioe) {
					logger.info(ioe);
				}
			}
		}
		return success;
	}

	/**
	 * 
	 * Description: 从FTP服务器下载文件
	 * 
	 * @Version1.0
	 * 
	 * @param url        FTP服务器hostname
	 * @param port       FTP服务器端口
	 * @param username   FTP登录账号
	 * @param password   FTP登录密码
	 * @param remotePath FTP服务器上的相对路径
	 * @param fileName   要下载的文件名
	 * @param localPath  下载后保存到本地的路径
	 * @return
	 */
	public static String downFile(String url, // FTP服务器hostname
			int port, // FTP服务器端口
			String username, // FTP登录账号
			String password, // FTP登录密码
			String remotePath, // FTP服务器上的相对路径
			String localPath, // 下载后保存到本地的路径
			String pathbak, // swift处理日期（读取ack后文件备份目录）
			String dealServer// 传入处理文件的service
	) {
		FTPClient ftp = new FTPClient();
		String filePath = "";
		OutputStream is = null;
		InputStream in = null;
		try {
			int reply;
			ftp.connect(url, port);
			// 如果采用默认端口，可以使用ftp.connect(url)的方式直接连接FTP服务器
			ftp.login(username, password);// 登录
			ftp.enterLocalPassiveMode();// 设置被动模式
			// ftp.setFileTransferMode(FTPClient.BINARY_FILE_TYPE);// 设置以二进制方式传输
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				return "";
			}
			// 本地没有目录则新建目录（备份目录） 路径
			String localStore = MessageFormat.format("{0}/{1}", localPath, pathbak);
			File file = new File(localStore);
			if (!file.exists()) {
				file.mkdirs();
			}
			if (ftp.changeWorkingDirectory(remotePath)) {// 转移到FTP服务器目录
				FTPFile[] fs = ftp.listFiles();
				for (FTPFile ff : fs) {
					if (!ff.isFile()) {// 判断是否文件
						continue;
					}
					File localFile = new File(localStore, ff.getName());
					is = new FileOutputStream(localFile);
					ftp.retrieveFile(ff.getName(), is);// 获得远程文件并保存在本地
					FTPFile[] fbak = ftp.listFiles(pathbak);
					if (fbak.length == 0) {
						ftp.makeDirectory(pathbak);
					}
					filePath = localFile.getPath();
					if (ftp.cwd(pathbak) == 250) {// 改变保存目录 远程文件备份
						in = new FileInputStream(localFile);// ftp.retrieveFileStream(ff.getName())
						ftp.storeFile(ff.getName(), in);
						ftp.changeToParentDirectory();// 返回原文件工作路径
						ftp.cwd(remotePath);
						ftp.deleteFile(ff.getName());// 删除已备份文件
					}
				}
			}
			ftp.logout();
		} catch (IOException e) {
			logger.info(e);
		} finally {
			if (ftp.isConnected()) {
				try {
					if (is != null) {
						is.close();
					}
					if (in != null) {
						in.close();
					}
					ftp.disconnect();
				} catch (IOException ioe) {
					logger.info(ioe);
				}
			}
		}
		return filePath;
	}

	public static void main(String[] args) {
	}
}