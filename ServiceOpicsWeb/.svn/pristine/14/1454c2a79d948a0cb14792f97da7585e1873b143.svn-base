package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.FxdWksBean;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.cashflow.HolidayBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.FieldMapType;
import com.singlee.financial.wks.expand.FxdItemWksBean;
import com.singlee.financial.wks.expand.RevpRow;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.cashflow.FxdFactory;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.util.FinancialCalculationModel;
import com.singlee.financial.wks.util.FinancialFieldMap;
import com.singlee.financial.wks.util.FinancialMarketTransUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 外汇交易录入转换方法
 *
 * @author shenzl
 * @date 2020/02/19
 */
@Component
public class FxdEntry {
    @Autowired
    private BrpsService brpsService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private PortService portService;
    @Autowired
    private CostService costService;
    @Autowired
    private TradService tradService;
    @Autowired
    private CustService custService;
    @Autowired
    private CcypService ccypService;
    @SuppressWarnings("unused")
	@Autowired
    private NostService nostService;
	@Autowired
	private FinancialFieldMap financialFieldMap;
	@Autowired
	private CusiService cusiService;
	@Autowired
	private SetaService setaService;
	@Autowired
	private FxdhService fxdhService;
	@Autowired
	private RevpService revpService;
	@Autowired
	private HldyService hldyService;
	@Autowired
	private CcodService ccodService;
	
	private Logger logger = Logger.getLogger(FxdEntry.class);
	
    /**
     * 外汇即期/远期校验
     *
     * @param fxdWksBean
     */
	public void DataCheck(FxdItemWksBean fxd) {
		// 1.判断债券对象是否存在
		if (null == fxd) {
			JY.raise("%s:%s,请补充完整FxdWksBean对象,或存在循环组", WksErrorCode.FXD_SPTFWD_NULL.getErrCode(), WksErrorCode.FXD_SPTFWD_NULL.getErrMsg());
		}
		// 2.判断交易机构是否存在
		if (StringUtils.isEmpty(fxd.getInstId())) {
			JY.raise("%s:%s,请填写FxdWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NULL.getErrCode(), WksErrorCode.BR_NULL.getErrMsg());
		}
		// 3.查询分支/机构是否存在
		Brps brps = brpsService.selectByPrimaryKey(fxd.getInstId());
		if (StringUtils.isEmpty(fxd.getInstId()) || null == brps) {
			JY.raise("%s:%s,请正确输入FxdWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(), WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 4.检查产品类型DESK字段拆分PROD_TYPE_PORT
		String[] book = StringUtils.split(fxd.getBook(), "_");
		if (StringUtils.isEmpty(fxd.getBook()) || book.length != 3) {
			JY.raise("%s:%s,请输入正确格式FxdWksBean.desk,For.[desk=FXD_FX_100]", WksErrorCode.DESK_FORM_ERR.getErrCode(), WksErrorCode.DESK_FORM_ERR.getErrMsg());
		}
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		if (StringUtils.isEmpty(book[0]) || StringUtils.isEmpty(book[1]) || null == type) {
			JY.raise("%s:%s,请输入正确格式FxdWksBean.desk,For.[TYPE=FX OR SW]", WksErrorCode.DESK_NOT_FOUND.getErrCode(), WksErrorCode.DESK_NOT_FOUND.getErrMsg());
		}
		Port port = portService.selectByPrimaryKey(fxd.getInstId(), fxd.getDesk());// 投组组合
		if (StringUtils.isEmpty(fxd.getBook()) || null == port) {
			JY.raise("%s:%s,请输入正确格式FxdWksBean.desk,For.[desk=MM_BR_100]", WksErrorCode.DESK_PORT_ERR.getErrCode(), WksErrorCode.DESK_PORT_ERR.getErrMsg());
		}
		// 5. 成本中心
		Cost cost = costService.selectByPrimaryKey(book[2]);
		if (StringUtils.isEmpty(book[2]) || null == cost) {
			JY.raise("%s:%s,请输入正确格式FxdWksBean.book,For.[COST为空或者不存在]", WksErrorCode.BOOK_NOT_FOUND.getErrCode(), WksErrorCode.BOOK_NOT_FOUND.getErrMsg());
		}
		// 6.检查交易对手
		Cust cust = custService.selectByPrimaryKey(fxd.getPartyId());
		if (StringUtils.isEmpty(fxd.getPartyId()) || null == cust) {
			JY.raise("%s:%s,请输入正确格式FxdWksBean.PartyId,For.[PartyId=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(), WksErrorCode.CUST_NOT_FOUND.getErrMsg());
		}
		// 7.检查交易员
		Trad trad = tradService.selectByPrimaryKey(fxd.getInstId(), fxd.getTrad());
		if (StringUtils.isNotEmpty(fxd.getTrad()) && null == trad) {
			JY.raise("%s:%s,请输入正确格式FxdWksBean.trad,For.[trad=TRAD]", WksErrorCode.TRAD_NOT_FOUND.getErrCode(), WksErrorCode.TRAD_NOT_FOUND.getErrMsg());
		}
		// 8.检查货币对是否存在
		Ccyp ccyp = ccypService.selectByPrimaryKey(fxd.getInstId(), fxd.getCcy(), fxd.getCtrCcy());
		if (StringUtils.isEmpty(fxd.getCcy()) || StringUtils.isEmpty(fxd.getCtrCcy()) || null == ccyp) {
			JY.raise("%s:%s,请输入正确格式FxdWksBean.CCY/CTRCCY,For.[ccy=USD,ctrccy=CNY]", WksErrorCode.CCYP_NOT_FOUND.getErrCode(), WksErrorCode.CCYP_NOT_FOUND.getErrMsg());
		}

		// 9.检查清算路径
		Seta seta = setaService.selectBySettacct(fxd.getInstId(),fxd.getCurrencySettlementAccount());
		if (null == seta || StringUtils.isEmpty(seta.getSacct())) {
			JY.raise("交易货币清算路径[" + fxd.getCurrencySettlementAccount() + "]不存在");
		}
		seta = setaService.selectBySettacct(fxd.getInstId(),fxd.getCounterCurrencySettlementAccount());
		if (null == seta || StringUtils.isEmpty(seta.getSacct())) {
			JY.raise("对应货币清算路径[" + fxd.getCounterCurrencySettlementAccount() + "]不存在");
		}

		//检查交易日期
		if(brps.getBranprcdate().compareTo(DateUtil.parse(fxd.getDealDate())) < 0) {
			JY.raise("交易日不能大于当前批量日期:"+DateUtil.format(brps.getBranprcdate()));
		}
		//起息日不能小于交易日
		if(DateUtil.parse(fxd.getVDate()).compareTo(DateUtil.parse(fxd.getDealDate())) < 0) {
			JY.raise("起息日不能小于交易日");
		}
		
	}

    /**
     * 账务日期
     *
     * @param fxdh
     * @param brpsService
     * @return
     */
    public Fxdh DataTrans(Fxdh fxdh, BrpsService brpsService) {
        // 获取当前账务日期
        Brps brps = brpsService.selectByPrimaryKey(fxdh.getBr());
        fxdh.setBrprcindte(brps.getBranprcdate());// 当前账务日期
        fxdh.setPayauthdte(brps.getBranprcdate());
        fxdh.setRecauthdte(brps.getBranprcdate());
        fxdh.setVerdate(brps.getBranprcdate());
        fxdh.setAmenddate(brps.getBranprcdate());
        fxdh.setLstmntdate(brps.getBranprcdate());
        return fxdh;
    }
    
    /**
     * 即/远期添加交易单号
     *
     * @param fxdh
     * @param mcfpService
     * @return
     */
    public void DataTrans(String br,String dealType,List<String> dealnos,McfpService mcfpService) {
    	 Mcfp mcfp = mcfpService.selectByPrimaryKey(br, OpicsConstant.FX.PROD_CODE,
                 OpicsConstant.FX.PROD_TYPE);// 获取DEALNO
         String dealNo = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";// 近端dealno
         dealnos.add(dealNo);
         if ("FXSWAP".equalsIgnoreCase(dealType)) {// 掉期
             // 远端dealno
             dealnos.add(Integer.parseInt(StringUtils.trimToEmpty(dealNo)) + 1 + "");
         }
    };

    /**
     * 更新交易单号
     *
     * @param fxdhLst
     * @param mcfpService
     * @param mcfp
     */
	public void DataTrans(String br,List<String> dealnos,McfpService mcfpService, Mcfp mcfp) {
		mcfp.setBr(br);
		mcfp.setProdcode(OpicsConstant.FX.PROD_CODE);// 产品代码 去掉空格
		mcfp.setType(OpicsConstant.FX.PROD_TYPE);// 产品类型
		mcfp.setTraddno(StringUtils.trimToEmpty(dealnos.get(dealnos.size()-1)));
		mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
	}

    /**
     * 静态常量
     *
     * @param fxdh
     * @return
     */
    public Fxdh DataTrans(Fxdh fxdh) {
        fxdh.setSeq(OpicsConstant.FX.SEQ_ZERO);// 序号
        fxdh.setBrokccy(OpicsConstant.BRRD.LOCAL_CCY);// 经纪人费用币种
        fxdh.setBrok("");// 经纪人费用币种
        fxdh.setBrokamt(BigDecimal.ZERO);// 经纪人费用
        fxdh.setPhonci(OpicsConstant.FX.PHONE_IND);// 电话确认标志
        fxdh.setIoper(OpicsConstant.FX.TRAD_NO);// 输入操作员
        fxdh.setVoper(OpicsConstant.FX.TRAD_NO);// 复核操作员
        fxdh.setVerind(OpicsConstant.FX.VER_IND);// 复核标志
        fxdh.setPayauthind(OpicsConstant.FX.PAY_AUTH_IND);// 付款授权标志
        fxdh.setDealsrce(OpicsConstant.FX.DEAL_SRCE);// 交易源代码
        fxdh.setPayoper(OpicsConstant.FX.TRAD_NO);// 付款操作员
        fxdh.setRecoper(OpicsConstant.FX.TRAD_NO);// 收款操作员
        fxdh.setCcyrevalamt(BigDecimal.ZERO);// 货币1重估
        fxdh.setCtrrevalamt(BigDecimal.ZERO);// 货币2重估
        fxdh.setCcynpvamt(BigDecimal.ZERO);// 货币1净现值
        fxdh.setCtrnpvamt(BigDecimal.ZERO);// 货币2净现值
        fxdh.setRecauthoper(OpicsConstant.FX.TRAD_NO);// 收款授权员
        fxdh.setPayauthoper(OpicsConstant.FX.TRAD_NO);// 付款授权操作员
        fxdh.setSiind("1");// 标志指令
        fxdh.setInternalrate8(BigDecimal.ZERO);// 内部汇率
        fxdh.setCorpspread8(BigDecimal.ZERO);// 交易所汇率差
        fxdh.setBrspread8(BigDecimal.ZERO);// 分行汇率差
        fxdh.setCustenteredind(OpicsConstant.FX.CUST_ENTERED_IND);// 客户输入标志
        fxdh.setCommamt(BigDecimal.ZERO);// 佣金
        fxdh.setExcoveralllim(OpicsConstant.FX.EXCOVERALL_LIM);// 超过总限额标志
        fxdh.setExccustlim(OpicsConstant.FX.EXCCUST_LIM);// 超过客户额度标志
        fxdh.setOptionind("N");// 可选交易标志
        fxdh.setMarginamt(BigDecimal.ZERO);// 保证金金额
        fxdh.setCorpspreadamt(BigDecimal.ZERO);// 默认交易金额
        fxdh.setSpotind(OpicsConstant.FX.SPOT_IND);// 即期标志
        fxdh.setMarginfeeamt(BigDecimal.ZERO);// 保证金金额
        fxdh.setMarginfeerebamt(BigDecimal.ZERO);// 保证金折扣
        fxdh.setSwiftmatchind(OpicsConstant.FX.SWIFT_MATCH_IND);// Swift匹配标志
        fxdh.setSiindpay(OpicsConstant.FX.SIIND_PAY);// 标准付款指令标志
        fxdh.setRexratetolind(OpicsConstant.FX.REXRATETOL_IND);// 超过利率限额标志
        fxdh.setFixrateind(OpicsConstant.FX.FIX_RATE_IND);// 固定利率标志
        fxdh.setBlockind(OpicsConstant.FX.BLOCK_IND);
        fxdh.setPhonetext("");
        fxdh.setRevtext("");
        fxdh.setRevreason("");
        fxdh.setTracecnt("");
        fxdh.setNetsi("0");
        fxdh.setNetdeal("");
        fxdh.setNsrnoccy("");
        fxdh.setNsrccyseq("");
        fxdh.setNsrnoctr("");
        fxdh.setNsrctrseq("");
        fxdh.setCorpcost("");
        fxdh.setBrcost("");
        fxdh.setLinkdealno("");
        fxdh.setLinkproduct("");
        fxdh.setLinkprodtype("");
        fxdh.setFincntr1("");
        fxdh.setFincntr2("");
        fxdh.setCorpccy("");
        fxdh.setCorpport("");
        fxdh.setCorptrad("");
        fxdh.setRevdealno("");
        fxdh.setMarginccy("");
        fxdh.setTenorexceedind("");
        fxdh.setCcysettstatus("");
        fxdh.setCtrsettstatus("");
        fxdh.setCustrefno("");
        fxdh.setCcyfailoper("");
        fxdh.setCcyclearoper("");
        fxdh.setCtrfailoper("");
        fxdh.setCtrclearoper("");
        fxdh.setMarginfeeccy("");
        fxdh.setMarginfeesettmeans("");
        fxdh.setMarginfeesettacct("");
        fxdh.setMarginfeerebccy("");
        fxdh.setMarginfeerebsettmeans("");
        fxdh.setMarginfeerebsettacct("");
        fxdh.setPhoneconfoper("");
        fxdh.setPhoneconftime("");
        fxdh.setCommccy("");
        fxdh.setDealnoexcess("");
        fxdh.setFixratecode("");
        fxdh.setRevoper("");
        fxdh.setCtrpartydealno("");
        fxdh.setCtrpartyphoneno("");
        fxdh.setCtrpartyphoneoper("");
        fxdh.setConfcreatedind("");
        fxdh.setDisaggccy("");
        fxdh.setDisaggspottrad("");
        fxdh.setDisaggspotport("");
        fxdh.setDisaggspotcost("");
        fxdh.setDisaggfwdtrad("");
        fxdh.setDisaggfwdport("");
        fxdh.setDisaggfwdcost("");
        fxdh.setFarnearind("");
        
        return fxdh;
    }

    /**
     * 即/远期交易要素
     *
     * @param fxdh
     * @param fxdItemWksBean
     * @return
     */
	public Fxdh DataTrans(Fxdh fxdh, FxdItemWksBean fxdItemWksBean, List<String> dealnos, String dealType) {
		fxdh.setBr(fxdItemWksBean.getInstId());// 核算机构
		String[] book = StringUtils.split(fxdItemWksBean.getBook(), "_");
		fxdh.setProdcode(book[0]);// 产品代码
		fxdh.setProdtype(book[1]);// 产品类型代码
		fxdh.setPort(fxdItemWksBean.getDesk());// 投资组合
		fxdh.setCost(book[2]);// 成本中心
		fxdh.setTrad(fxdItemWksBean.getTrad());// 交易员

		fxdh.setVdate(DateUtil.parse(fxdItemWksBean.getVDate()));// 交割日
		fxdh.setCust(fxdItemWksBean.getPartyId());// 交易对手编号
		fxdh.setDealdate(DateUtil.parse(fxdItemWksBean.getDealDate()));// 交易日期
		fxdh.setDealtime(fxdItemWksBean.getDealTime());// 交易时间
		fxdh.setPs(fxdItemWksBean.getPs());// 买卖标识
		fxdh.setInputdate(DateUtil.parse(DateUtil.getCurrentDateAsString()));
		fxdh.setInputtime(DateUtil.getCurrentTimeAsString());// 输入系统时间
		fxdh.setOrigocamt(fxdItemWksBean.getCcyAmt().abs());// 初始货币金额
		fxdh.setCcy(fxdItemWksBean.getCcy());// 成交货币
		fxdh.setCcyamt(fxdItemWksBean.getCcyAmt());// 成交货币金额
		fxdh.setCcyterms(fxdItemWksBean.getCcyTerms());// 成交货币与对应货币关系
		fxdh.setCcyrate8(fxdItemWksBean.getCcyRate());// 成交货币汇率
		fxdh.setCcypd8(fxdItemWksBean.getCcyPd());// 成交货币升贴水
		fxdh.setCcybamt(fxdItemWksBean.getCcyBAmt());// 成交货币基础金额
		fxdh.setCcybrate8(fxdItemWksBean.getCcyBRate());// 成交货币基础汇率
		fxdh.setCcybpd8(fxdItemWksBean.getCcyBPd());
		fxdh.setBaseterms(fxdItemWksBean.getCcyBTerms());// 基础货币汇率乘除形式
		fxdh.setCtrccy(fxdItemWksBean.getCtrCcy());// 对应货币
		fxdh.setCtramt(fxdItemWksBean.getCtrAmt());// 对应货币金额
		fxdh.setCtrrate8(fxdItemWksBean.getCtrRate());// 对应货币利率
		fxdh.setCtrpd8(fxdItemWksBean.getCtrPd().setScale(8, RoundingMode.HALF_UP));// 对应货币汇率
		fxdh.setCtrbamt(fxdItemWksBean.getCtrBAmt());// 对应货币基础金额
		fxdh.setCtrbrate8(fxdItemWksBean.getCtrBRate());// 对应货币基础汇率
		fxdh.setVerdate(fxdh.getBrprcindte());// 复核日期
		fxdh.setPayauthdte(fxdh.getBrprcindte());// 付款授权日期
		fxdh.setRecauthdte(fxdh.getBrprcindte());// 收款授权日期
		fxdh.setCcysettdate(fxdh.getVdate());// 成交货币结算日期
		fxdh.setCtrsettdate(fxdh.getVdate());// 对应货币结算日期
		fxdh.setSpotccy(fxdItemWksBean.getCtrCcy().equalsIgnoreCase(OpicsConstant.BRRD.LOCAL_CCY) ? fxdItemWksBean.getCcy() : fxdItemWksBean.getCtrCcy());// 强势货币,待确认？？？
		fxdh.setSpotdate(DateUtil.parse(fxdItemWksBean.getSpotDate()));// 即期日期
		fxdh.setSwapdeal("0");// 即/远期默认编号
		fxdh.setDealtext(fxdItemWksBean.getRemark());
		fxdh.setAmenddate(fxdh.getBrprcindte());
		fxdh.setSpottrad(fxdh.getTrad());// 即期交易交易员,即期空掉
		fxdh.setSpotport(fxdh.getPort());// 即期投资组合,即期空掉
		fxdh.setSpotcost(fxdh.getCost());// 即期成本中心,即期空掉
		fxdh.setFwdpremamt(fxdItemWksBean.getFwdRealPlAmt());//到期日损益
		
		DataTrans(fxdh, brpsService);// 设置账务日期

		if (!StringUtils.isEmpty(fxdItemWksBean.getCurrencySettlementAccount())) {
			fxdh.setCcysacct(fxdItemWksBean.getCurrencySettlementAccount());
			Seta seta = setaService.selectBySettacct(fxdItemWksBean.getInstId(),fxdItemWksBean.getCurrencySettlementAccount());
			if (null == seta || StringUtils.trim(seta.getSmeans()).length() == 0) {
				throw new RuntimeException("清算方式OPICS中不在");
			}
			fxdh.setCcysmeans(seta.getSmeans());
		} else {
			// 资金工作站处理
			fxdh.setCcysmeans(StringUtils.equals(OpicsConstant.BRRD.LOCAL_CCY, fxdh.getCcy()) ? OpicsConstant.PCIX.CANPS_MEANS : OpicsConstant.PCIX.NOSTRO_MEANS);
		}

		if (!StringUtils.isEmpty(fxdItemWksBean.getCounterCurrencySettlementAccount())) {
			fxdh.setCtrsacct(fxdItemWksBean.getCounterCurrencySettlementAccount());
			Seta seta = setaService.selectBySettacct(fxdItemWksBean.getInstId(),fxdItemWksBean.getCounterCurrencySettlementAccount());
			if (null == seta || StringUtils.trim(seta.getSmeans()).length() == 0) {
				throw new RuntimeException("对应货币清算方式[ "+fxdItemWksBean.getCounterCurrencySettlementAccount()+" ]OPICS中不在");
			}
			fxdh.setCtrsmeans(seta.getSmeans());
		} else {
			// 资金工作站处理
			fxdh.setCtrsmeans(StringUtils.equals(OpicsConstant.BRRD.LOCAL_CCY, fxdh.getCtrccy()) ? OpicsConstant.PCIX.CANPS_MEANS : OpicsConstant.PCIX.NOSTRO_MEANS);
		}

		if (isSpotDeal(fxdh)) {// 即期
			fxdh.setSpotfwdind("S");
			fxdh.setTenor("S");
		} else {
			fxdh.setSpotfwdind("F");// 远期
			fxdh.setTenor("F");
		}

		if ("SPOTFORWARD".equalsIgnoreCase(dealType)) {// 即期/远期
			fxdh.setDealno(dealnos.get(0));
		} else if ("FXSWAP".equalsIgnoreCase(dealType)) {// 掉期
			fxdh.setFarnearind(fxdItemWksBean.getFarNearind());// 掉期远端
			DataTransFsp(fxdItemWksBean, fxdh, dealnos);
		} else {
			throw new RuntimeException("交易类型[" + dealType + "]不支持");
		}

		return fxdh;
	}
    
    /**
     * 设置交易DEALNO
     * @param fxdh
     * @param dealnos
     */
    private void setDealno(Fxdh fxdh,List<String> dealnos) {
    	fxdh.setDealno(dealnos.get(null == fxdh.getFarnearind() || StringUtils.trim(fxdh.getFarnearind()).length()==0 || "N".equalsIgnoreCase(fxdh.getFarnearind())  ? 0 : 1));
    }
    
    /**
     * 获取另外一端的DEALNO
     * @param farNearind
     * @param dealnos
     * @return
     */
    private String getSwapDealno(String farNearind,List<String> dealnos) {
    	String swapDeal = null;
    	if("F".equalsIgnoreCase(farNearind)) {
    		swapDeal = dealnos.get(0);
    	}else if("N".equalsIgnoreCase(farNearind)) {
    		swapDeal = dealnos.get(1);
    	}else {
            throw new RuntimeException("近远端标识["+farNearind+"]不支持");
        }
    	return swapDeal;
    }
    
    /**
     * 计算当前交易是即期还是远期
     * @param fxdItemWksBean
     * @param fxdh
     * @return
     */
    public int getSettDays(String ccy,String ctrccy,Date dealDate,Date vdate) {
    	List<String> ccys = new ArrayList<String>();
    	int hldyDays = 0;
    	List<Date> dates = null;
    	int days = 0;
    	
    	ccys.add(ccy);
    	ccys.add(ctrccy);
    	ccys.add(OpicsConstant.BRRD.LOCAL_CCY);
    	
    	dates = hldyService.selectHldyListDate(ccys,dealDate,vdate);
    	
    	hldyDays = null == dates ? 0 : dates.size();
    	days = DateUtil.daysBetween(dealDate, vdate) - hldyDays;
    	return days;
    }
    
    /**
     * 根据CPPR中的SPOTDATE计算当前交易即、远、掉
     * @param fxdItemWksBean
     * @param fxdh
     * @return
     */
    private boolean isSpotDeal(Fxdh fxdh) {
    	boolean isSpotDeal = false;
    	int days = 0;
    	
    	if(fxdh.getDealdate().equals(fxdh.getBrprcindte())) {
    		//交易日与批量日期相同的交易
    		if(fxdh.getVdate().compareTo(fxdh.getSpotdate()) > 0) {
        		isSpotDeal = false;
        	}else {
        		isSpotDeal = true;
        	}
    	}else {
    		//交易日小于批量日期的倒起息交易
    		days = getSettDays(fxdh.getCcy(),fxdh.getCtrccy(),fxdh.getDealdate(),fxdh.getVdate());
    		if(days > 2) {
    			isSpotDeal = false;
    		}else {
    			isSpotDeal = true;
    		}
    	}//end if else
    	logger.info(String.format("计算是否为即期交易[%s],系统日期[%tF],交易日[%tF],起息日[%tF],交易日与起息日天数[%d]", 
    			isSpotDeal,fxdh.getBrprcindte(),fxdh.getDealdate(),fxdh.getVdate(),days));
    	return isSpotDeal;
    }
    
    
    /**
     * 处理掉期交易近端远端信息
     * @param fxdItemWksBean
     * @param fxdh
     */
    private void DataTransFsp(FxdItemWksBean fxdItemWksBean,Fxdh fxdh,List<String> dealnos) {
        /*
         * 先判断掉期的近端与远端
         */
        if (StringUtils.equals(fxdItemWksBean.getFarNearind(), "F")) {// 掉期远端
            fxdh.setSwapdeal(getSwapDealno(fxdItemWksBean.getFarNearind(),dealnos));// 近端dealno
            setDealno(fxdh,dealnos);// 交易编号
        } else if (StringUtils.equals(fxdItemWksBean.getFarNearind(), "N")) {
            fxdh.setSwapdeal(getSwapDealno(fxdItemWksBean.getFarNearind(),dealnos));// 近端dealno
            setDealno(fxdh,dealnos);// 交易编号
        }
        
        fxdh.setSwapvdate(DateUtil.parse(fxdItemWksBean.getSwapVdate()));
    }


    /**
     * 外汇掉期FXDT表数据
     *
     * @param fxdhLst
     * @return
     */
	public Fxdt DataTrans(Fxdt fxdt,Fxdh fxdh,String dealType) {
		List<Fxdt> fxdtLst = new ArrayList<Fxdt>();
		if (null == fxdh) {
			return null;
		}
		fxdt.setBr(fxdh.getBr());// 部门
		fxdt.setDealno(fxdh.getDealno());// 交易单号
		fxdt.setSeq(OpicsConstant.FX.SEQ_ZERO);// 顺序号
		fxdt.setDisaggccyrate8(BigDecimal.ZERO);
		fxdt.setDisaggccypd8(BigDecimal.ZERO);
		fxdt.setDisaggccyspread8(BigDecimal.ZERO);
		fxdt.setLstmntdte(fxdh.getBrprcindte());// 修改时间
		fxdt.setAmount1(BigDecimal.ZERO);
		fxdt.setAmount2(BigDecimal.ZERO);
		fxdt.setCcyintrate8(BigDecimal.ZERO);
		fxdt.setCtrintrate8(BigDecimal.ZERO);
		fxdt.setRate38(BigDecimal.ZERO);
		fxdt.setRate48(BigDecimal.ZERO);
		fxdt.setUpdatecounter(OpicsConstant.FX.UPDATE_COUNTER);// 修改次数
		fxdt.setDealccynpvamt(BigDecimal.ZERO);//
		fxdt.setDealctrnpvamt(BigDecimal.ZERO);
		fxdt.setSpotctrbamt(BigDecimal.ZERO);//
		fxdt.setSpotctramt(BigDecimal.ZERO);
		fxdt.setCcyfwdpremamt(BigDecimal.ZERO);
		fxdt.setDraftind(BigDecimal.ZERO);
		fxdt.setDraftblockind(BigDecimal.ZERO);
		fxdt.setDisaggccyterms("");
		fxdt.setDisaggccy("");
		fxdt.setDisaggctr("");
		fxdt.setInterbranchno("");
		fxdt.setInterbranchspottrad("");
		fxdt.setInterbranchfwdtrad("");
		fxdt.setInterbranchdealno("");
		fxdt.setInterbranchcorpdealno("");
		fxdt.setTrad2("");
		fxdt.setTrad3("");
		fxdt.setTrad4("");
		fxdt.setCcylinkdealno("");
		fxdt.setCtrlinkdealno("");
		fxdt.setInterbranchpldealno("");
		fxdt.setInterbranchcorppldealno("");
		fxdt.setFedealno("");
		fxdt.setFecorpdealno("");
		fxdt.setFepldealno("");
		fxdt.setFecorppldealno("");
		fxdt.setInterfacedealno("");
		fxdt.setRevtime("");
		fxdt.setDisaggcorpspottrad("");
		fxdt.setDisaggcorpspotport("");
		fxdt.setDisaggcorpspotcost("");
		fxdt.setDisaggcorpfwdtrad("");
		fxdt.setDisaggcorpfwdport("");
		fxdt.setDisaggcorpfwdcost("");
		fxdt.setInterfaceserver("");
		fxdt.setFwdpremccy("");
		fxdt.setDraftno("");
		fxdt.setDrafttext("");
		fxdtLst.add(fxdt);
		
		if ("SPOTFORWARD".equalsIgnoreCase(dealType)) {// 即期/远期
			fxdt.setTrad1(fxdh.getSpotfwdind());
		} else if ("FXSWAP".equalsIgnoreCase(dealType)) {// 掉期
			fxdt.setTrad1(fxdh.getProdtype());
		} else {
			throw new RuntimeException("交易类型[" + dealType + "]不支持");
		}
		
		return fxdt;
	}
    
    /**
     * 计算外汇远掉期交易相关字段值
     * @param fxdWksBean
     */
    public void DataCheck(FxdWksBean fxdWksBean) {
    	List<Fxdh> fxdhs = null;
    	
    	if(null == fxdWksBean) {
    		throw new RuntimeException("外汇远期/掉期交易对象不能为空");
    	}
    	
    	System.out.println("FxdWksBean:"+fxdWksBean);
    	
    	fxdhs = fxdhService.selectByTradeNo(null, fxdWksBean.getApproveNo(),"N");
    	
    	if(null != fxdhs && fxdhs.size() > 0) {
    		throw new RuntimeException("外汇远期/掉期交易["+fxdWksBean.getApproveNo()+"]在OPICS中已存在");
    	}
		
		if(null == fxdWksBean.getItemBeanLst() || fxdWksBean.getItemBeanLst().size() == 0) {
			throw new RuntimeException("外汇远期/掉期交易明细对象不能为空");
		}
		
    }
    
    /**
     * 计算FXDH表中,baserate,ctrbp等字段值
     * @param sn
     * @param fxdItemWksBean
     * @param dealType
     */
	public void DataTrans(String sn,FxdItemWksBean fxdItemWksBean,String dealType) {
		Trad trad = null;
		Cusi cusi = null;
		Ccyp ccyp = null;
		Revp ccyRevp = null;
		BigDecimal ccybRate = null;
		BigDecimal ccybpd = null;
		//货币对应的假日信息
		Map<String, HolidayBean> holidayDates = null;
		//CCY货币兑人民币汇率
		List<RevpRow> revpRows = null;
		BigDecimal bpUnit = null;
		int ctrScaleLength;
		int ccyScaleLength;
		
		financialFieldMap.convertFieldvalue(fxdItemWksBean);
		
		if("P".equalsIgnoreCase(fxdItemWksBean.getPs())) {
			fxdItemWksBean.setCtrAmt(fxdItemWksBean.getCtrAmt().negate());
		}else if("S".equalsIgnoreCase(fxdItemWksBean.getPs())) {
			fxdItemWksBean.setCcyAmt(fxdItemWksBean.getCcyAmt().negate());
		}else {
			throw new RuntimeException("交易方向不支持");
		}
		
		// 将CFETS交易员转换为OPICS中的交易员
		trad = tradService.selectByCfetsTradId(fxdItemWksBean.getInstId(), fxdItemWksBean.getTrad());
		if (null != trad) {
			fxdItemWksBean.setTrad(trad.getTrad());
		} // end if
			// 将CFETS交易员转换为OPICS中的交易员
		cusi = cusiService.selectByPrimaryKey(FieldMapType.CFETS.name(), fxdItemWksBean.getPartyId());
		if (null != cusi) {
			fxdItemWksBean.setPartyId(cusi.getCno());
		} // end if
		
		fxdItemWksBean.setDealTime(DateUtil.getCurrentTimeAsString());
		
		if("3".contains(fxdItemWksBean.getSettlementMeans())){
			fxdItemWksBean.setRemark(sn+"|CCP|SL_FXD");
		}else if("9".contains(fxdItemWksBean.getSettlementMeans())){
			//在接口中处理,组装remark字段
		}else {
			fxdItemWksBean.setRemark(sn+"|SL_FXD");
		}
		
		Brps brps = brpsService.selectByPrimaryKey(fxdItemWksBean.getInstId());
		if (null == brps) {
			throw new RuntimeException("机构代码["+fxdItemWksBean.getInstId()+"]在不存在");
		}
		
		ccyp = ccypService.selectByPrimaryKey(fxdItemWksBean.getInstId(), fxdItemWksBean.getCcy(), fxdItemWksBean.getCtrCcy());
		if(null == ccyp) {
			throw new RuntimeException("查询交易货币对为空!");
		}
		fxdItemWksBean.setSpotDate(DateUtil.format(ccyp.getPairspotdate()));
		
		fxdItemWksBean.setCcyTerms(calTerms(fxdItemWksBean.getCcy(),ccyp.getCcy1(),ccyp.getTerms()));
		
		fxdItemWksBean.setCcyBTerms(calBaseTerms(fxdItemWksBean.getInstId(),fxdItemWksBean.getCcy(),null));
		// 获取货币的小数位
		ctrScaleLength = Integer.parseInt(StringUtils.trim(ccodService.selectByPrimaryKey(fxdItemWksBean.getCtrCcy()).getDecs()));
		ccyScaleLength = Integer.parseInt(StringUtils.trim(ccodService.selectByPrimaryKey(fxdItemWksBean.getCcy()).getDecs()));
		// 检查货币1金额、货币2金额、成交汇率之间的运算关系
		BigDecimal ctrAmt = FxdFactory.calculationCtramt(calTerms(fxdItemWksBean.getCcy(), ccyp.getCcy1(), ccyp.getTerms()), fxdItemWksBean.getCcyAmt(), fxdItemWksBean.getCcyRate(), ctrScaleLength);
		if (ctrAmt.compareTo(fxdItemWksBean.getCtrAmt()) != 0) {
			JY.raise("货币1金额、成交汇率计算金额与货币2金额不相等,对应货币金额:" + fxdItemWksBean.getCtrAmt() + ",计算金额：" + ctrAmt);
		}
		// 小货币点差除以100,其它货币除以1000
		if (ctrScaleLength == 0 || ccyScaleLength == 0) {
			bpUnit = FinancialCalculationModel.ONE_HUNDRED;
		} else {
			
			bpUnit = FinancialCalculationModel.TEN_THOUSAND;
		}
		fxdItemWksBean.setCcyPd(fxdItemWksBean.getCcyPd().divide(bpUnit, 8, RoundingMode.HALF_UP));// 成交货币基础点差
		ccyRevp = revpService.selectByPrimaryKey(fxdItemWksBean.getInstId(), fxdItemWksBean.getCcy());
		if(fxdItemWksBean.getCtrCcy().equalsIgnoreCase(OpicsConstant.BRRD.LOCAL_CCY)) {
			ccybRate = fxdItemWksBean.getCcyRate();
			ccybpd = fxdItemWksBean.getCcyPd();
		}else {
			//要根据两条腿的币种来获取币种,货币互换是两个币种
			holidayDates = FinancialMarketTransUtils.createHoliday(ccyRevp.getCcy(),hldyService.selectHldyListStr(ccyRevp.getCcy()));
			//将OPICS的一条REVP记录转换为LIST
			revpRows = FxdFactory.convertRevpFieldToRow(ccyRevp, ccyp.getPairspotdate(), holidayDates.get(ccyRevp.getCcy()).getHolidayDates());
			//计算拆人民币点差,插值掉期点计算
			ccybpd = FxdFactory.calculationCcyBaseSpread(DateUtil.parse(fxdItemWksBean.getVDate()),ccyp.getPairspotdate(), revpRows);
			if("FXSWAP".equalsIgnoreCase(dealType)) {
				ccybRate = ccyRevp.getSpotrate8().add(ccybpd);
			}else if("SPOTFORWARD".equalsIgnoreCase(dealType)) {
				ccybRate = ccyRevp.getSpotrate8();
			}else {
				throw new RuntimeException("交易类型不支持");
			}//end if .... else
		}//end if else
		fxdItemWksBean.setCcyBRate(ccybRate);
		fxdItemWksBean.setCcyBAmt(FxdFactory.calculationBaseAmount(fxdItemWksBean.getCcyAmt(), fxdItemWksBean.getCcyBRate(), 2));
		fxdItemWksBean.setCcyBPd(ccybpd);
		fxdItemWksBean.setCtrRate(FxdFactory.calculationCounterCurrencyRate(fxdItemWksBean.getCcyAmt(), fxdItemWksBean.getCcyRate(), fxdItemWksBean.getCcyTerms(), ctrScaleLength));
		fxdItemWksBean.setCtrBAmt(fxdItemWksBean.getCcyBAmt().negate());
		fxdItemWksBean.setCtrBRate(FxdFactory.calculationCounterCurrencyBaseRate(fxdItemWksBean.getCcyAmt(), fxdItemWksBean.getCcyRate(), ccybRate, OpicsConstant.BRRD.LOCAL_CCY,
				fxdItemWksBean.getCcy(), fxdItemWksBean.getCcyTerms(), calBaseTerms(fxdItemWksBean.getInstId(), fxdItemWksBean.getCtrCcy(), fxdItemWksBean.getCcyBTerms()), ctrScaleLength));
		fxdItemWksBean.setCtrPd(
				FxdFactory.calculationCounterCurrencySpread(fxdItemWksBean.getCcyAmt(), fxdItemWksBean.getCcyRate(), fxdItemWksBean.getCcyPd(), fxdItemWksBean.getCcyTerms(), ctrScaleLength));
	}
	
	/**
	 * 计算并设置FWDPREMAMT金额
	 * @param list
	 * @param dealType
	 */
	public void DataTrans(List<FxdItemWksBean> list,String dealType) {
		FxdItemWksBean item = null;
		FxdItemWksBean itemFar = null;
		Optional<FxdItemWksBean> op = null;
		BigDecimal spotRate = null;
		int ndays = 0;
		int fdays = 0;
		Revp ctrRevp = null;
		Ccyp ccyp  = null;
		int ctramtScaleLength = 2;
		int ccyamtScaleLength = 2;
		
		if("FXSWAP".equalsIgnoreCase(dealType)) {
			op = list.stream().filter(fxd -> "N".equalsIgnoreCase(fxd.getFarNearind())).findFirst();
			if(!op.isPresent()) {
				throw new RuntimeException("获取掉期近端交易明细异常");
			}
			item = op.get();
			ctrRevp = revpService.selectByPrimaryKey(item.getInstId(), item.getCtrCcy());
			ccyp = ccypService.selectByPrimaryKey(item.getInstId(), item.getCcy(), item.getCtrCcy());
			ctramtScaleLength = Integer.parseInt(StringUtils.trim(ccodService.selectByPrimaryKey(item.getCtrCcy()).getDecs()));
			ccyamtScaleLength = Integer.parseInt(StringUtils.trim(ccodService.selectByPrimaryKey(item.getCcy()).getDecs()));
			//计算掉期近端
			item.setFwdRealPlAmt(FxdFactory.calculationFwdpremamt(calFwdpremamtMethod(item.getCcy(), item.getCtrCcy(),dealType,ccyp),
					ctramtScaleLength,item.getCcyAmt(), item.getCcyRate(), item.getCcyPd(), item.getCcyBRate().add(item.getCcyBPd()),item.getCcyTerms(),
					ctrRevp.getSpotrate8(),ccyamtScaleLength));
			
			op = list.stream().filter(fxd -> "F".equalsIgnoreCase(fxd.getFarNearind())).findFirst();
			if(!op.isPresent()) {
				throw new RuntimeException("获取掉期远期交易明细异常");
			}
			itemFar = op.get();
			//计算掉期远端
			itemFar.setFwdRealPlAmt(FxdFactory.calculationFwdpremamt(calFwdpremamtMethod(item.getCcy(), item.getCtrCcy(),dealType,ccyp),
					ctramtScaleLength,itemFar.getCcyAmt(), itemFar.getCcyRate(), itemFar.getCcyPd(),item.getCcyBRate().add(item.getCcyBPd()),itemFar.getCcyTerms(),
					ctrRevp.getSpotrate8(),ccyamtScaleLength));
			
			//检查近端起息日与远端起息日
			if(DateUtil.parse(itemFar.getVDate()).compareTo(DateUtil.parse(item.getVDate())) < 0) {
				throw new RuntimeException("远端起息日必须大于近端起息日");
			}//end if
			
			//检查交易近端远端汇率及金额是否正确
			ndays = getSettDays(item.getCcy(),item.getCtrCcy(),DateUtil.parse(item.getDealDate()),DateUtil.parse(item.getVDate()));
			fdays = getSettDays(itemFar.getCcy(),itemFar.getCtrCcy(),DateUtil.parse(itemFar.getDealDate()),DateUtil.parse(itemFar.getVDate()));
			logger.debug("近端计算天数："+ndays+",远端计算天数："+fdays);
			//检查T+2的交易不能有点差
			if(2 == ndays && BigDecimal.ZERO.compareTo(item.getCcyPd()) != 0) {
				throw new RuntimeException("掉期近端是SPOT交易,不能输入点差,该字段请填0");
			}//end if
			if(2 == fdays && BigDecimal.ZERO.compareTo(itemFar.getCcyPd()) != 0) {
				throw new RuntimeException("掉期远端是SPOT交易,不能输入点差,该字段请填0");
			}//end if
			if(1 == ndays && fdays == 2) {
				//T+1掉T+2的交易，近远端都是即期交易
				spotRate = itemFar.getCcyRate().add(item.getCcyPd()).setScale(8, RoundingMode.HALF_UP);
				logger.debug("计算近端汇率："+spotRate+",近端成交汇率："+item.getCcyRate()+",远端成交汇率:"+itemFar.getCcyRate()+",近端汇率点差:"+item.getCcyPd());
				if(spotRate.compareTo(item.getCcyRate()) != 0) {
					throw new RuntimeException("近端与远端即期汇率不相等,请检近端汇率、近端点差与远端汇率");
				}//if
			}//end if
		}else if("SPOTFORWARD".equalsIgnoreCase(dealType)) {
			item = list.get(0);
			ctrRevp = revpService.selectByPrimaryKey(item.getInstId(), item.getCtrCcy());
			ccyp = ccypService.selectByPrimaryKey(item.getInstId(), item.getCcy(), item.getCtrCcy());
			ctramtScaleLength = Integer.parseInt(StringUtils.trim(ccodService.selectByPrimaryKey(item.getCtrCcy()).getDecs()));
			ccyamtScaleLength = Integer.parseInt(StringUtils.trim(ccodService.selectByPrimaryKey(item.getCcy()).getDecs()));
			//计算远期交易
			item.setFwdRealPlAmt(FxdFactory.calculationFwdpremamt(calFwdpremamtMethod(item.getCcy(), item.getCtrCcy(),dealType,ccyp),
					ctramtScaleLength,item.getCcyAmt(),item.getCcyRate(), item.getCcyPd(),item.getCcyBRate().subtract(item.getCcyBPd()),item.getCcyTerms(),
					ctrRevp.getSpotrate8(),ccyamtScaleLength));
		}else {
			throw new RuntimeException("交易类型不支持");
		}//end if .... else
	}
	
	/**
	 * 计算CTRCCY端的BPD,通过插值法计算
	 * @param ctrRevp
	 * @param ccyp
	 * @param vdate
	 * @return
	 */
	@SuppressWarnings("unused")
	private BigDecimal calCtrBpd(Revp ctrRevp, Ccyp ccyp, String vdate) {
		// 货币对应的假日信息
		Map<String, HolidayBean> holidayDates = null;
		// CCY货币兑人民币汇率
		List<RevpRow> revpRows = null;
		BigDecimal ctrbpd = null;
		
		/*
		 * 非人民币时才需求计算
		 */
		if (!OpicsConstant.BRRD.LOCAL_CCY.equalsIgnoreCase(ctrRevp.getCcy())) {
			// 要根据两条腿的币种来获取币种,货币互换是两个币种
			holidayDates = FinancialMarketTransUtils.createHoliday(ctrRevp.getCcy(), hldyService.selectHldyListStr(ctrRevp.getCcy()));
			// 将OPICS的一条REVP记录转换为LIST
			revpRows = FxdFactory.convertRevpFieldToRow(ctrRevp, ccyp.getPairspotdate(), holidayDates.get(ctrRevp.getCcy()).getHolidayDates());
			// 计算拆人民币点差,插值掉期点计算
			ctrbpd = FxdFactory.calculationCcyBaseSpread(DateUtil.parse(vdate), ccyp.getPairspotdate(), revpRows);
		}else {
			ctrbpd = BigDecimal.ZERO;
		}
		
		return ctrbpd;
	}
	
	/**
	 * 获取货币对乘除关系
	 * @param ccy
	 * @param ccypCcy
	 * @param terms
	 * @return
	 */
	private String calTerms(String ccy,String ccypCcy1,String terms) {
		String strTerms = null;
			
		if(ccy.equalsIgnoreCase(ccypCcy1)) {
			strTerms = terms;
		}else {
			strTerms = "D".equalsIgnoreCase(terms) ? "M" : "M".equalsIgnoreCase(terms) ? "D" : "";
		}
		
		return strTerms;
	}
	
	/**
	 * 获取当前货币对本国货币的方向
	 * @param br
	 * @param ccy
	 * @return
	 */
	private String calBaseTerms(String br,String ccy,String ccybTerms) {
		Ccyp baseCcy = null;
		String strTerms = null;
		
		if(OpicsConstant.BRRD.LOCAL_CCY.equalsIgnoreCase(ccy)) {
			strTerms = ccybTerms;
		}else {
			baseCcy = ccypService.selectByPrimaryKey(br,ccy,OpicsConstant.BRRD.LOCAL_CCY);
			strTerms = calTerms(ccy,baseCcy.getCcy1(),baseCcy.getTerms());
		}
		
		return strTerms;
	}
	
	/**
	 * 组装FWDPREMAMT字段的计算方法
	 * @param ccy
	 * @param ctrccy
	 * @return
	 */
	private String calFwdpremamtMethod(String ccy,String ctrccy,String dealType,Ccyp ccyp) {
		String fwdpremamtMethod;
		if(OpicsConstant.BRRD.LOCAL_CCY.equalsIgnoreCase(ccy) || OpicsConstant.BRRD.LOCAL_CCY.equalsIgnoreCase(ctrccy)) {
			fwdpremamtMethod = "JSH";
		}else if(ccy.equalsIgnoreCase(ccyp.getTradedccy())) {
			//CCY货币和CCYP表中的TRADEDCCY
			fwdpremamtMethod = "FX";
		}else {
			//CCYP表中的TRADEDCCY和CCY不等时的计算逻辑
			fwdpremamtMethod = "FX_OTHER";
		}
		
		return fwdpremamtMethod;
	}
}
