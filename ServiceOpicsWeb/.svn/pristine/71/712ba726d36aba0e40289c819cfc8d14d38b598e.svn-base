package com.singlee.financial.mapper;

import java.math.BigDecimal;
import java.util.Map;

import com.singlee.financial.bean.SlFxSptFwdBean;
import com.singlee.financial.bean.SlFxSwapBean;

/**
 * OPICS外汇接口调用
 * 
 * @author shenzl
 * 
 */
public interface FxdMapper {

	/**
	 * 调用opics存储过程
	 * 
	 * @param map
	 * @return
	 */
	String cfestFxProc(Map<String, String> map);

	String cfestSwapProc(Map<String, String> map);

	BigDecimal getCcyRate(String ccy);

	/**
	 * 外汇即期远期添加到OPICS中的ifxd表中
	 */
	void addFxSptFwd(SlFxSptFwdBean fxSptFwdBean);

	/**
	 * 外汇即期远期冲销添加
	 */
	void addFxSptFwdRev(SlFxSptFwdBean fxSptFwdBean);

	/**
	 * 外汇掉期添加
	 */
	void addFxSwap(SlFxSwapBean fxSwapBean);

	/**
	 * 外汇掉期冲销添加
	 */
	void addFxSwapRev(SlFxSwapBean fxSwapBean);

	/**
	 * 检查外汇即期、远期是否存在
	 * 
	 * @param dealNo
	 */
	int getSlFundIfxd(String dealNo);

	/***
	 * 判断对方账户是否与nost表中的相同
	 * 
	 * @param fxSptFwdBean
	 * @return
	 */
	Integer isCustAcctSameInNost(SlFxSptFwdBean fxSptFwdBean);

	/***
	 * 判断对方账户是否与nost表中的相同
	 * 
	 * @param fxSwapBean
	 * @return
	 */
	Integer isCustAcctSameInNostSwap(SlFxSwapBean fxSwapBean);

	/**
	 * 根据fedealno查询inth表中的dealno
	 */
	String queryDealno(Map<String,Object> map);
	/**
	 * 掉期根据DEALNO 查询FXDH 数据的远端dealno(SWAPDEAL)
	 * @param nearDealno
	 * @return
	 */
	String getFxdhSwapDeal(String nearDealno);
	
	String fxFwdChangeSpt(Map<String, String> map);

	String fxSwapChange(Map<String, String> map);

	
}