SELECT TRIM(SUBSTR(A.DESCR,1,20)) AS SECID,MAX(M.ACCTNGTYPE),
       TRIM(SUBSTR(A.DESCR,38,1)) AS INVTYPE,
       SUM(CASE
             WHEN A.DRCRIND = 'CR' THEN
              A.AMOUNT
             ELSE
              0
           END) AS cramount,
       SUM(CASE
             WHEN A.DRCRIND = 'DR' THEN
              A.AMOUNT
             ELSE
              0
           END) AS dramount,
       SUM(CASE
             WHEN A.DRCRIND = 'DR' THEN
              A.AMOUNT
             ELSE
              - (A.AMOUNT)
           END) AS BALANCEAMT
  FROM SL_ACUP A LEFT JOIN SECM M ON TRIM(SUBSTR(A.DESCR,1,20))=TRIM(M.SECID) WHERE TRIM(A.INTGLNO) LIKE '%13302%' AND M.ACCTNGTYPE<>'LOANREC'
  AND A.PRODUCT='SECUR'
 GROUP BY TRIM(SUBSTR(A.DESCR,1,20)),TRIM(SUBSTR(A.DESCR,38,1))
 ORDER BY TRIM(SUBSTR(A.DESCR,38,1))
