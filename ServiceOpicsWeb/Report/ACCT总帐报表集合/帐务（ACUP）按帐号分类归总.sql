
SELECT SUBJECT,SUM(CASE
             WHEN DRCRIND = 'CR' THEN
              AMOUNT
             ELSE
              0
           END) AS cramount,
       SUM(CASE
             WHEN DRCRIND = 'DR' THEN
              AMOUNT
             ELSE
              0
           END) AS dramount,
       SUM(CASE
             WHEN DRCRIND = 'DR' THEN
              AMOUNT
             ELSE
              - (AMOUNT)
           END) AS BALANCEAMT FROM 
(SELECT DISTINCT * FROM SL_ACUP
 UNION
 SELECT DISTINCT * FROM SL_ACUP_BAK) ACUPUNION 
 WHERE POSTDATE BETWEEN TO_DATE('2009-11-09','YYYY-MM-DD') AND  TO_DATE('2009-11-09','YYYY-MM-DD')
 GROUP BY SUBJECT
 ORDER BY SUBJECT
