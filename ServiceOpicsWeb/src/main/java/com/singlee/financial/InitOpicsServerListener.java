package com.singlee.financial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.singlee.financial.common.spring.InitListener;

@Service
public class InitOpicsServerListener extends InitListener {

	private static final Logger logger = LoggerFactory.getLogger("ESB");

	@Override
	protected void init(ApplicationContext applicationContext) {
		logger.info("--------------OPICS ESB Listener init success ------------");
		/**
		 * 在该地方写需要容器启动后加载的服务监听
		 */
//		EsbTransferSocketServer esbTransferSocketServer =applicationContext.getBean("esbTransferSocketServer",EsbTransferSocketServer.class);
//		esbTransferSocketServer.doListen();
		logger.info("--------------OPICS ESB Listener init success ------------");
	}

	@Override
	protected String sysname() {
		return "--------------OPICS ESB Listener--------------";
	}

}
