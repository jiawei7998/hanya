package com.singlee.financial.marketdata;

import com.alibaba.fastjson.JSONObject;
import com.singlee.financial.bean.*;
import com.singlee.financial.common.spring.ExternalIntfc;
import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.mapper.*;
import com.singlee.financial.marketdata.bean.*;
import com.singlee.financial.marketdata.hscb.bean.SlFiMarketBean;
import com.singlee.financial.marketdata.intfc.MarketDataTransServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.mapper.opics.*;
import com.singlee.financial.wks.service.opics.RsrmService;
import com.singlee.financial.wks.util.FinancialCalculationModel;
import com.singlee.financial.wks.util.FinancialDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class MarketDataTransServerImpl implements MarketDataTransServer {
	@Autowired
	private RsrmService rsrmService;
	@Autowired
	IrevMapper irevMapper;
	@Autowired
	RevpMapper revpMapper;
	@Autowired
	IrhsMapper irhsMapper;
	@Autowired
	RateMapper rateMapper;
	@Autowired
	IycrMapper iycrMapper;
	@Autowired
	YcrtMapper ycrtMapper;
	@Autowired
	YcimMapper ycimMapper;
	@Autowired
	IvolMapper ivolMapper;
	@Autowired
	VoldMapper voldMapper;
	@Autowired
	IselMapper IselMapper;
	@Autowired
	SecmMapper secmMapper;
	@Autowired
	CustMapper custMapper;
	@Autowired
	SeclMapper seclMapper;
	@Resource
	BatchDao batchDao;
	@Resource
	IselMapper iselMapper;
	@Resource
	CommonMapper commonMapper;
	@Autowired
	ExternalIntfc externalIntfc;
	@Autowired
	ISlFiMarketMapper slFiMarketMapper;
	@Autowired
	BrpsMapper brpsMapper;
	@Autowired
	HldyMapper hldyMapper;

	private Logger logger = Logger.getLogger(MarketDataTransServerImpl.class);
	
	@Override
	@Deprecated
	public SlOutBean saveFxRateData(List<FxRevaluationRates> frrLst, int repeatNum) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlIrevBean> irevBeans = new ArrayList<SlIrevBean>();
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		try {
			// 获取外汇曲线中对应的标准期限点
			List<Revp> revps = revpMapper.selectAll();
			for (int i = 0; i < revps.size(); i++) {// 查看货币对应的标准期限点
				SlIrevBean irevBean = new SlIrevBean(revps.get(i).getBr());
				irevBean.setCcy(revps.get(i).getCcy());
				irevBean.setXccy("CNY");
				SlInthBean inthBean = irevBean.getInthBean();
				inthBean.setPriority("1");
				// 初始化接口对象
				inthBean.setFedealno(String.format("%1$ty%1$tm%1$td%1$tH%1$tM%1$tS%2$03d", new Date(), i));
				boolean haveSameCCY = false;
				for (FxRevaluationRates frr : frrLst) {// 开始处理每个币种的
					if (StringUtils.equals(revps.get(i).getCcy().trim(), frr.getCcy().trim())) {
						haveSameCCY = true;
						irevBean.setSpotrate(frr.getSpotRate().stripTrailingZeros().toPlainString());// 即期汇率
						if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod1()),
								frr.getDesignatedMaturity())) { // 第1个期限点
							irevBean.setPeriod1Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod2()),
								frr.getDesignatedMaturity())) {// 第2个期限点
							irevBean.setPeriod2Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod3()),
								frr.getDesignatedMaturity())) {// 第3个期限点
							irevBean.setPeriod3Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod4()),
								frr.getDesignatedMaturity())) {// 第4个期限点
							irevBean.setPeriod4Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod5()),
								frr.getDesignatedMaturity())) {// 第5个期限点
							irevBean.setPeriod5Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod6()),
								frr.getDesignatedMaturity())) {// 第6个期限点
							irevBean.setPeriod6Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod7()),
								frr.getDesignatedMaturity())) {// 第7个期限点
							irevBean.setPeriod7Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod8()),
								frr.getDesignatedMaturity())) {// 第8个期限点
							irevBean.setPeriod8Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod9()),
								frr.getDesignatedMaturity())) {// 第9个期限点
							irevBean.setPeriod9Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod10()),
								frr.getDesignatedMaturity())) {// 第10个期限点
							irevBean.setPeriod10Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod11()),
								frr.getDesignatedMaturity())) {// 第11个期限点
							irevBean.setPeriod11Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod12()),
								frr.getDesignatedMaturity())) {// 第12个期限点
							irevBean.setPeriod12Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod13()),
								frr.getDesignatedMaturity())) {// 第13个期限点
							irevBean.setPeriod13Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod14()),
								frr.getDesignatedMaturity())) {// 第14个期限点
							irevBean.setPeriod14Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod15()),
								frr.getDesignatedMaturity())) {// 第15个期限点
							irevBean.setPeriod15Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod16()),
								frr.getDesignatedMaturity())) {// 第16个期限点
							irevBean.setPeriod16Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod17()),
								frr.getDesignatedMaturity())) {// 第17个期限点
							irevBean.setPeriod17Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod18()),
								frr.getDesignatedMaturity())) {// 第18个期限点
							irevBean.setPeriod18Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod19()),
								frr.getDesignatedMaturity())) {// 第19个期限点
							irevBean.setPeriod19Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod20()),
								frr.getDesignatedMaturity())) {// 第20个期限点
							irevBean.setPeriod20Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod21()),
								frr.getDesignatedMaturity())) {// 第21个期限点
							irevBean.setPeriod21Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod22()),
								frr.getDesignatedMaturity())) {// 第22个期限点
							irevBean.setPeriod22Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod23()),
								frr.getDesignatedMaturity())) {// 第23个期限点
							irevBean.setPeriod23Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod24()),
								frr.getDesignatedMaturity())) {// 第24个期限点
							irevBean.setPeriod24Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod25()),
								frr.getDesignatedMaturity())) {// 第25个期限点
							irevBean.setPeriod25Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod26()),
								frr.getDesignatedMaturity())) {// 第26个期限点
							irevBean.setPeriod26Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod27()),
								frr.getDesignatedMaturity())) {// 第27个期限点
							irevBean.setPeriod27Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod28()),
								frr.getDesignatedMaturity())) {// 第28个期限点
							irevBean.setPeriod28Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod29()),
								frr.getDesignatedMaturity())) {// 第29个期限点
							irevBean.setPeriod29Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod30()),
								frr.getDesignatedMaturity())) {// 第30个期限点
							irevBean.setPeriod30Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod31()),
								frr.getDesignatedMaturity())) {// 第31个期限点
							irevBean.setPeriod31Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod32()),
								frr.getDesignatedMaturity())) {// 第32个期限点
							irevBean.setPeriod32Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod33()),
								frr.getDesignatedMaturity())) {// 第33个期限点
							irevBean.setPeriod33Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod34()),
								frr.getDesignatedMaturity())) {// 第34个期限点
							irevBean.setPeriod34Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod35()),
								frr.getDesignatedMaturity())) {// 第35个期限点
							irevBean.setPeriod35Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod36()),
								frr.getDesignatedMaturity())) {// 第36个期限点
							irevBean.setPeriod36Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod37()),
								frr.getDesignatedMaturity())) {// 第37个期限点
							irevBean.setPeriod37Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod38()),
								frr.getDesignatedMaturity())) {// 第38个期限点
							irevBean.setPeriod38Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						} else if (StringUtils.equals(StringUtils.trim(revps.get(i).getPeriod39()),
								frr.getDesignatedMaturity())) {// 第39个期限点
							irevBean.setPeriod39Rate(frr.getBaseRatePoint().stripTrailingZeros().toPlainString());
						}
						continue;
					}
				}
				if (haveSameCCY) {
					irevBean.setLstmntdate(DateUtil.format(frrLst.get(0).getLstmntdate()));
					inthBeans.add(inthBean);
					irevBeans.add(irevBean);
				}
			}
			// 批量提交
			if (irevBeans.size() > 0) {
				logger.error("上传参数--》"+JSONObject.toJSONString(irevBeans.get(0)));
				logger.error("IREV 插入数据库条数--》"+JSONObject.toJSONString(irevBeans.size()));
				batchDao.batch("com.singlee.financial.mapper.IrevMapper.addIrev", irevBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IREV操作成功,第%s次导入%s条数据!", repeatNum, irevBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IREV操作失败,失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("外汇市场数据处理异常!", e);
		}
		return outBean;
	}

	@Override
	@Deprecated
	public SlOutBean saveRateData(List<RateHistoryInformation> rhiLst, int repeatNum) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIrhsBean> irhsBeans = new ArrayList<SlIrhsBean>();
		try {
			// 获取OPICS中定义的RATECODE
			List<Rate> rates = rateMapper.selectAll();
			for (int i = 0; i < rates.size(); i++) {
				SlIrhsBean irhsBean = new SlIrhsBean(rates.get(i).getBr());
				SlInthBean inthBean = irhsBean.getInthBean();
				inthBean.setFedealno(String.format("%1$ty%1$tm%1$td%1$tH%1$tM%1$tS%2$03d", new Date(), i));
				for (RateHistoryInformation rhi : rhiLst) {
					if (StringUtils.equals(StringUtils.trim(rates.get(i).getRatecode()), rhi.getRateCode())) {// 利率代码相等则导入
						irhsBean.setEffdate(DateUtil.format(rhiLst.get(0).getSpotDate()));
						irhsBean.setRatecode(rates.get(i).getRatecode());
						irhsBean.setIntrate8(rhi.getIntRate().doubleValue());// 利率
						irhsBean.setLstmntdate(DateUtil.format(rhiLst.get(0).getLstmntdate()));
						inthBeans.add(inthBean);
						irhsBeans.add(irhsBean);
						break;
					}
				}
			}
			// 批量提交
			if (irhsBeans.size() > 0) {
				logger.error("上传参数--》"+JSONObject.toJSONString(irhsBeans.get(0)));
				logger.error("IRHS 插入数据库条数--》"+JSONObject.toJSONString(irhsBeans.size()));
				batchDao.batch("com.singlee.financial.mapper.IrhsMapper.addIrhs", irhsBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IRHS操作成功,第%s次导入%s条数据!", repeatNum, irhsBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IRHS操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("重定利率数据处理异常!", e);
		}

		return outBean;
	}

	@Override
	@Deprecated
	public SlOutBean saveYieldData(List<YieldCurveIndex> yciLst, int repeatNum) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIycrBean> iycrBeans = new ArrayList<SlIycrBean>();
		try {
			// 获取OPICS中定义的CONTRIBRATE曲线
			List<Ycrt> ycrts = ycrtMapper.selectAll();
			for (int i = 0; i < ycrts.size(); i++) {
				SlIycrBean iycrBean = new SlIycrBean(ycrts.get(i).getBr()); //
				for (YieldCurveIndex yci : yciLst) {
					SlInthBean inthBean = iycrBean.getInthBean();
					//inthBean.setFedealno(String.format("%1$ty%1$tm%1$td%1$tH%1$tM%1$tS%2$03d", new Date(), i));
					inthBean.setFedealno(String.format("%1$ty%1$tm%1$tH%1$tM%1$tS%2$03d", new Date(),i));
					if (StringUtils.equals(ycrts.get(i).getCcy().trim(), yci.getCcy().trim())
							&& StringUtils.equals(ycrts.get(i).getContribrate().trim(), yci.getYieldCurveName().trim())
							&& StringUtils.equals(ycrts.get(i).getMtyend().trim(),
									yci.getDesignatedMaturity().trim())) {// 币种+曲线名称+期限点相等则导入
						String rateType;
						if(yci.getDesignatedMaturity().endsWith("Y") && !"1Y".equalsIgnoreCase(yci.getDesignatedMaturity())) {
							rateType = "S";
						}else {
							rateType = "C";
						}
						iycrBean.setRatetype(rateType);
						iycrBean.setCcy(yci.getCcy());
						iycrBean.setYieldcurve(yci.getYieldCurveName());
						iycrBean.setMtystart("0M");// 默认值
						iycrBean.setMtyend(yci.getDesignatedMaturity());
						iycrBean.setBidrate8(yci.getRate().doubleValue());
						iycrBean.setMidrate8(yci.getRate().doubleValue());
						iycrBean.setOfferrate8(yci.getRate().doubleValue());
						iycrBean.setLstmntdate(DateUtil.format(yci.getLstmntdate()));
						inthBeans.add(inthBean);
						iycrBeans.add(iycrBean);
						break;
					}
				}
			}
			// 批量提交
			if (iycrBeans.size() > 0) {
				batchDao.batch("com.singlee.financial.mapper.IycrMapper.addIycr", iycrBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IYCR操作成功,第%s次导入%s条数据!", repeatNum, iycrBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IYCR操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("收益率曲线数据处理异常!", e);
		}
		return outBean;
	}

	@Override
	@Deprecated
	public SlOutBean saveVoldData(List<OptionsVolatility> ovLst, int repeatNum) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIvolBean> ivolBeans = new ArrayList<SlIvolBean>();
		try {
			Map<String, BigDecimal> ovMap = new HashMap<String, BigDecimal>();// 标准价格波动率
			for (OptionsVolatility ov : ovLst) {// 数据清洗
				if (ov.getStrikePrice().compareTo(new BigDecimal("50")) == 0) {// 取出所有标准点数据FX+CNY/USD+2M --> 0.1
					ovMap.put(ov.getOptionType().concat(ov.getCcyParis()).concat(ov.getDesignatedMaturity()),
							ov.getVolatility());
				} else {// FX+CNY/USD+10+P+2M --> 0.1
					ovMap.put(ov.getOptionType().concat(ov.getCcyParis())
							.concat(String.valueOf(ov.getStrikePrice().intValue())).concat(ov.getCallputInd())
							.concat(ov.getDesignatedMaturity()), ov.getVolatility());
				}
			}
			// 获取OPICS中定义的波动率
			List<Vold> volds = voldMapper.selectAll();// 包含CALL/PUT记录
			for (int i = 0; i < volds.size(); i++) {
				Vold vold = volds.get(i);
				SlIvolBean ivolBean = new SlIvolBean(vold.getBr());
				SlInthBean inthBean = ivolBean.getInthBean();
				inthBean.setFedealno(String.format("%1$ty%1$tm%1$td%1$tH%1$tM%1$tS%2$03d", new Date(), i));
				String optype = StringUtils.trimToEmpty(vold.getOptiontype());// 类型
				String ccypair = StringUtils.trimToEmpty(vold.getOptiontypekey());// 货币对
				String strike = String.valueOf(vold.getStrikeprice8().intValue());// 执行价格
				String desmat = StringUtils.trimToEmpty(vold.getDesmat());// 标准期限
//				String cp = StringUtils.trimToEmpty(vold.getCallput());// 涨跌标识
				String key50 = optype.concat(ccypair).concat(desmat);
				String keyCall = optype.concat(ccypair).concat(strike).concat("C").concat(desmat);// map键
				String keyPut = optype.concat(ccypair).concat(strike).concat("P").concat(desmat);// map键
				// 根据记录查询需要的call和对于的put值,没找到则舍去FI+CCY/USD+10+C+2M
				ivolBean.setOptiontype(optype);
				ivolBean.setOptiontypekey(ccypair);
				ivolBean.setDesmat(desmat);
				ivolBean.setAtmoneyind("50".equals(strike) ? "1" : "0");
				ivolBean.setTerm("0");
				ivolBean.setRiskrev8(BigDecimal.ZERO);
				ivolBean.setStranglemgn8(BigDecimal.ZERO);
				logger.debug("strike:" + strike);
				ivolBean.setStrikeprice8(vold.getStrikeprice8());
				if ("50".equals(strike) && ovMap.get(key50) != null) {
					ivolBean.setCallaskvolatility8(ovMap.get(key50));
					ivolBean.setPutaskvolatility8(ivolBean.getCallaskvolatility8());
					ivolBean.setCallbidvolatility8(ivolBean.getCallaskvolatility8());
					ivolBean.setPutbidvolatility8(ivolBean.getCallaskvolatility8());
					ivolBeans.add(ivolBean);
					inthBeans.add(inthBean);
				} else {
					if ((ovMap.get(keyCall) != null && ovMap.get(keyPut) != null && ovMap.get(key50) != null)) {
						ivolBean.setCallaskvolatility8(ovMap.get(keyCall).subtract(ovMap.get(key50)));
						ivolBean.setPutaskvolatility8(ovMap.get(keyPut).subtract(ovMap.get(key50)));
						ivolBean.setCallbidvolatility8(ivolBean.getCallaskvolatility8());
						ivolBean.setPutbidvolatility8(ivolBean.getPutaskvolatility8());
						ivolBeans.add(ivolBean);
						inthBeans.add(inthBean);
					}
				}
			}
			// 批量提交
			if (ivolBeans.size() > 0) {
				batchDao.batch("com.singlee.financial.mapper.IvolMapper.addIvol", ivolBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IVOL操作成功,第%s次导入%s条数据!", repeatNum, ivolBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IVOL操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("波动率数据处理异常!", e);
		}
		return outBean;
	}
	
	
	
	@Override
	@Deprecated
	public SlOutBean savePriceData(List<BondMarketPrices> bmpLst, int repeatNum) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIselBean> iselBeans = new ArrayList<SlIselBean>();
		
		try {
			HashMap<String,Secm>  BondCode_Secm_MAP = new HashMap<String,Secm>();
			if(BondCode_Secm_MAP.size()==0){
				List<Secm> secmList=secmMapper.selectAll();
				for(int i=0;i<secmList.size();i++){
					Secm secmTemp = secmList.get(i);
					BondCode_Secm_MAP.put(StringUtils.trim(secmTemp.getSecid()), secmTemp);
				}
			}

			// 开始正常逻辑处理
			List<Brps> brs = brpsMapper.selectAll();
			int i = 0;
			for (BondMarketPrices bondMarketPrices : bmpLst) {// 数据清洗
				// 判断债券是否存在
//				Secm secm = secmMapper.selectByPrimaryKey(bondMarketPrices.getBondCode());
				Secm secm = BondCode_Secm_MAP.get(StringUtils.trim(bondMarketPrices.getBondCode()));
				if (null == secm || secm.getMdate().compareTo(bondMarketPrices.getSpotDate()) <= 0) {// 对于不存在的券和已经到期的券则不再进行导入
					continue;
				}
				
				for (Brps br : brs) {
					if("00".equalsIgnoreCase(br.getBr())) {
						continue;
					}
					i++;// 自增加数据
					SlIselBean iselBean = new SlIselBean(br.getBr());
					SlInthBean inthBean = iselBean.getInthBean();
					inthBean.setFedealno(String.format("%1$ty%1$tm%1$td%1$tH%1$tM%2$03d", new Date(),i));
					iselBean.setClsgprice8(bondMarketPrices.getCleanPrice());// 收盘价
					iselBean.setSecid(StringUtils.trim(secm.getSecid().trim()));
					iselBeans.add(iselBean);
					inthBeans.add(inthBean);
				}
			}
			// 批量提交
			if (iselBeans.size() > 0) {
				batchDao.batch("com.singlee.financial.mapper.IselMapper.addIsel", iselBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IVOL操作成功,第%s次导入%s条数据!", repeatNum, iselBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("ISEL操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("债券收盘价数据处理异常!", e);
		}
		return outBean;
	}
	
	@Override
	public List<SubscriptionCode> queryRsrmByGroupId(String groupId) {
		return rsrmService.queryByGroupID(groupId);
	}
	
	@Override
	public SlOutBean saveBondData(List<SlSecmBean> sibLst, int repeatNum) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlSecmBean> isecBeans = new ArrayList<SlSecmBean>();
		List<Secm> secmList = secmMapper.selectAll();
		List<Cust> custList = custMapper.selectAll();
		try {
			for (int i = 0; i < sibLst.size(); i++) {// 数据清洗
				SlSecmBean slSecmBean = sibLst.get(i);
				// 判断债券是否存在  对于存在的券则不再进行导入
				Boolean flag1 = false;
				for (Secm secm : secmList) {
					if(secm.getSecid().trim().equals(slSecmBean.getSecid().trim())) {
						flag1 = true;
						continue;
					}
				}
				if(flag1) {
					continue;
				}
				// 根据发行人  匹配行业代码和债券类型
				for (Cust cust : custList) {
					if(cust.getCno().trim().equals(slSecmBean.getIssuer().trim())) {
						slSecmBean.setAcctngtype(cust.getAcctngtype());
						slSecmBean.setSecmsic(cust.getSic());
						continue;
					}
				}
				SlInthBean inthBean = slSecmBean.getInthBean();
				inthBean.setFedealno(String.format("%1$ty%1$tm%1$td%1$tH%1$tM%1$tS%2$03d", new Date(), i));
				inthBeans.add(inthBean);
				isecBeans.add(slSecmBean);
			}
			// 批量提交
			if (isecBeans.size() > 0) {
				batchDao.batch("com.singlee.financial.mapper.ExternalMapper.addSlFundSemm", isecBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IVOL操作成功,第%s次导入%s条数据!", repeatNum, isecBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("ISEC操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("债券信息数据处理异常!", e);
		}
		return outBean;
	}
	

	
	/**
	 * 获取OPICS 市场数据  涉及到表 SECL SEFM SECM BRPS
	 * @return
	 */
	@Deprecated
	@Override
	public List<OpicsSmclBean> getSmclOpicsList() {
		List<OpicsSmclBean> list = new ArrayList<OpicsSmclBean>();
		
		List<Secl> selcList = seclMapper.selectSelAndSefmAll();
		if(selcList.size()>0){
			for(Secl bean : selcList){
				OpicsSmclBean opicsBean = new OpicsSmclBean();
				opicsBean.setBr(bean.getBr());
				opicsBean.setSecid(bean.getSecid());
//				opicsBean.setFactor(bean.getFactor());
//				opicsBean.setIntcalcrule(bean.getIntcalcrule());
				opicsBean.setSecsacct(bean.getSecsacct());
				opicsBean.setPrice(bean.getClsgprice8());
//				opicsBean.setsNumber(bean.getSn());
				list.add(opicsBean);
			}
		}
		return list;
	}

	@Override
	public SlOutBean saveFxRateData(Map<String, FxRevaluationRates> frRateMap,FxRateTypes fxRateType) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlIrevBean> irevBeans = new ArrayList<SlIrevBean>();
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		Date postdate = null;
		int count = 0;
		try {
			// 当前系统日期
			postdate = Calendar.getInstance().getTime();

			// ISEL表最指定日期最大流水号
			count = getMaxFedealNo(postdate, RateFeedTypes.FXRATE);
			// 获取外汇曲线中对应的标准期限点
			List<Revp> revps = revpMapper.selectImportData();
			for (Revp revp : revps) {
				SlIrevBean irevBean = new SlIrevBean(revp.getBr());
				irevBean.setCcy(revp.getCcy());
				irevBean.setXccy("CNY");
				SlInthBean inthBean = irevBean.getInthBean();
				inthBean.setPriority("1");
				count++;
				// 初始化接口对象
				inthBean.setFedealno(createFedealno(postdate, count));
				
				logger.debug("外汇汇率OPICS待导入数据:"+revp);
				
				irevBean.setSpotrate(getRate(revp.getCcy(),"SPOT",frRateMap,fxRateType));// 即期汇率
				irevBean.setPeriod1Rate(getRate(revp.getCcy(),revp.getPeriod1(),frRateMap,fxRateType));
				irevBean.setPeriod2Rate(getRate(revp.getCcy(),revp.getPeriod2(),frRateMap,fxRateType));
				irevBean.setPeriod3Rate(getRate(revp.getCcy(),revp.getPeriod3(),frRateMap,fxRateType));
				irevBean.setPeriod4Rate(getRate(revp.getCcy(),revp.getPeriod4(),frRateMap,fxRateType));
				irevBean.setPeriod5Rate(getRate(revp.getCcy(),revp.getPeriod5(),frRateMap,fxRateType));
				irevBean.setPeriod6Rate(getRate(revp.getCcy(),revp.getPeriod6(),frRateMap,fxRateType));
				irevBean.setPeriod7Rate(getRate(revp.getCcy(),revp.getPeriod7(),frRateMap,fxRateType));
				irevBean.setPeriod8Rate(getRate(revp.getCcy(),revp.getPeriod8(),frRateMap,fxRateType));
				irevBean.setPeriod9Rate(getRate(revp.getCcy(),revp.getPeriod9(),frRateMap,fxRateType));
				irevBean.setPeriod10Rate(getRate(revp.getCcy(),revp.getPeriod10(),frRateMap,fxRateType));
				irevBean.setPeriod11Rate(getRate(revp.getCcy(),revp.getPeriod11(),frRateMap,fxRateType));
				irevBean.setPeriod12Rate(getRate(revp.getCcy(),revp.getPeriod12(),frRateMap,fxRateType));
				irevBean.setPeriod13Rate(getRate(revp.getCcy(),revp.getPeriod13(),frRateMap,fxRateType));
				irevBean.setPeriod14Rate(getRate(revp.getCcy(),revp.getPeriod14(),frRateMap,fxRateType));
				irevBean.setPeriod15Rate(getRate(revp.getCcy(),revp.getPeriod15(),frRateMap,fxRateType));
				irevBean.setPeriod16Rate(getRate(revp.getCcy(),revp.getPeriod16(),frRateMap,fxRateType));
				irevBean.setPeriod17Rate(getRate(revp.getCcy(),revp.getPeriod17(),frRateMap,fxRateType));
				irevBean.setPeriod18Rate(getRate(revp.getCcy(),revp.getPeriod18(),frRateMap,fxRateType));
				irevBean.setPeriod19Rate(getRate(revp.getCcy(),revp.getPeriod19(),frRateMap,fxRateType));
				irevBean.setPeriod20Rate(getRate(revp.getCcy(),revp.getPeriod20(),frRateMap,fxRateType));
				irevBean.setPeriod21Rate(getRate(revp.getCcy(),revp.getPeriod21(),frRateMap,fxRateType));
				irevBean.setPeriod22Rate(getRate(revp.getCcy(),revp.getPeriod22(),frRateMap,fxRateType));
				irevBean.setPeriod23Rate(getRate(revp.getCcy(),revp.getPeriod23(),frRateMap,fxRateType));
				irevBean.setPeriod24Rate(getRate(revp.getCcy(),revp.getPeriod24(),frRateMap,fxRateType));
				irevBean.setPeriod25Rate(getRate(revp.getCcy(),revp.getPeriod25(),frRateMap,fxRateType));
				irevBean.setPeriod26Rate(getRate(revp.getCcy(),revp.getPeriod26(),frRateMap,fxRateType));
				irevBean.setPeriod27Rate(getRate(revp.getCcy(),revp.getPeriod27(),frRateMap,fxRateType));
				irevBean.setPeriod28Rate(getRate(revp.getCcy(),revp.getPeriod28(),frRateMap,fxRateType));
				irevBean.setPeriod29Rate(getRate(revp.getCcy(),revp.getPeriod29(),frRateMap,fxRateType));
				irevBean.setPeriod30Rate(getRate(revp.getCcy(),revp.getPeriod30(),frRateMap,fxRateType));
				irevBean.setPeriod31Rate(getRate(revp.getCcy(),revp.getPeriod31(),frRateMap,fxRateType));
				irevBean.setPeriod32Rate(getRate(revp.getCcy(),revp.getPeriod32(),frRateMap,fxRateType));
				irevBean.setPeriod33Rate(getRate(revp.getCcy(),revp.getPeriod33(),frRateMap,fxRateType));
				irevBean.setPeriod34Rate(getRate(revp.getCcy(),revp.getPeriod34(),frRateMap,fxRateType));
				irevBean.setPeriod35Rate(getRate(revp.getCcy(),revp.getPeriod35(),frRateMap,fxRateType));
				irevBean.setPeriod36Rate(getRate(revp.getCcy(),revp.getPeriod36(),frRateMap,fxRateType));
				irevBean.setPeriod37Rate(getRate(revp.getCcy(),revp.getPeriod37(),frRateMap,fxRateType));
				irevBean.setPeriod38Rate(getRate(revp.getCcy(),revp.getPeriod38(),frRateMap,fxRateType));
				irevBean.setPeriod39Rate(getRate(revp.getCcy(),revp.getPeriod39(),frRateMap,fxRateType));
				irevBean.setLstmntdate(DateUtil.format(postdate,"yyyyMMdd"));
				if(!org.springframework.util.StringUtils.hasText(irevBean.getSpotrate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod1Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod2Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod3Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod4Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod5Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod6Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod7Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod8Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod9Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod10Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod11Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod12Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod13Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod14Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod15Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod16Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod17Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod18Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod19Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod20Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod21Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod22Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod23Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod24Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod25Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod26Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod27Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod28Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod29Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod30Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod31Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod32Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod33Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod34Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod35Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod36Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod37Rate())
						&& !org.springframework.util.StringUtils.hasText(irevBean.getPeriod38Rate()) && !org.springframework.util.StringUtils.hasText(irevBean.getPeriod39Rate())
						){
					//如果所有数据都为0或者为空则不导入,系统中保存的则是上一工作日的数据
					continue;
				}
				
				logger.debug("外汇汇率导入对象值:"+irevBean);

				inthBeans.add(inthBean);
				irevBeans.add(irevBean);
			}
			// 批量提交
			batchDao.batch("com.singlee.financial.mapper.IrevMapper.addIrev", irevBeans);
			batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			outBean.setRetMsg(String.format("IREV操作成功,导入%s条数据!", irevBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IREV操作失败,失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("外汇市场数据处理异常!", e);
			throw new RuntimeException(e);
		}
		return outBean;
	}

	@Override
	public SlOutBean saveRateData(Map<String, RateHistoryInformation> rhiMap) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIrhsBean> irhsBeans = new ArrayList<SlIrhsBean>();
		Date postdate = null;
		int count = 0;
		Brps brps = null;
		HashMap<String,Brps> brpsMap = new HashMap<String,Brps>();
		try {
			// 当前系统日期
			postdate = Calendar.getInstance().getTime();

			// ISEL表最指定日期最大流水号
			count = getMaxFedealNo(postdate, RateFeedTypes.INTRATE);
			// 获取OPICS中定义的RATECODE
			List<Rate> rates = rateMapper.selectImportData();
			for (Rate rate : rates) {
				SlIrhsBean irhsBean = new SlIrhsBean(rate.getBr());
				SlInthBean inthBean = irhsBean.getInthBean();
				count++;
				logger.debug("重定利率待处理OPICS记录:"+rate);
				RateHistoryInformation excel = rhiMap.get(StringUtils.trim(rate.getRatecode()));
				logger.debug("重定利率待导入EXCEL记录:"+excel);
				if(null == excel) {
					//如果EXCLE中没有导入数据,则该条记录不进处理
					continue;
				}
				//根据BR查询批量日期
				brps = brpsMap.get(rate.getBr());
				if(null == brps) {
					brps = brpsMapper.selectByPrimaryKey(rate.getBr());
					brpsMap.put(rate.getBr(), brps);
				}
				inthBean.setFedealno(createFedealno(postdate, count));
				//根据rate表中的RATEFIXDAY生成对应的日期yyyy/mm/dd
				irhsBean.setEffdate(createEffdate(rate,brps.getBranprcdate()));
				irhsBean.setRatecode(StringUtils.trim(rate.getRatecode()));
				irhsBean.setIntrate8(getRate(excel.getIntRateAsk(), excel.getIntRateBid(), excel.getIntRate()).doubleValue());// 利率
				
				//EXCEL中没有值时该条记录不添加
				if(0.0 == irhsBean.getIntrate8()) {
					continue;
				}
				
				irhsBean.setLstmntdate(DateUtil.format(brps.getBranprcdate()));
				
				inthBeans.add(inthBean);
				irhsBeans.add(irhsBean);
			}
			// 批量提交
			if (irhsBeans.size() > 0) {
				batchDao.batch("com.singlee.financial.mapper.IrhsMapper.addIrhs", irhsBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IRHS操作成功,导入%s条数据!", irhsBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IRHS操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("重定利率数据处理异常!", e);
			throw new RuntimeException(e);
		}

		return outBean;
	}
	
	/**
	 * 根据利率参数计算利息生效日期
	 * @param rate
	 * @param postdate
	 * @return
	 */
	private String createEffdate(Rate rate,Date postdate) {
		Date effdate = null;
		int refixDays = 0;
		Calendar valueDate = null;
		int count = 0;
		List<Date> hldyDates = null;
		
		refixDays = Integer.parseInt(StringUtils.trim(rate.getRatefixday()));
		
		logger.debug("重定利率OPICS重定天数配置:"+rate);
		//查询批量日期开始一个月内的假日信息
		valueDate = Calendar.getInstance();
		valueDate.setTime(postdate);
		valueDate.add(Calendar.MONTH, 1);
		
		logger.debug("重定利率当前批量日期:"+String.format("%1$tY/%1$tm/%1$td", postdate));
		
		//查询系统本国货币一个月之类的假日信息
		hldyDates = hldyMapper.selectHldyListDate(Arrays.asList("CNY"), postdate, valueDate.getTime());
		if(null == hldyDates) {
			hldyDates = new ArrayList<Date>();
		}
		logger.debug("重定利率一个月内的假日信息:"+hldyDates);
		effdate = postdate;
		while (count < refixDays ) {
			effdate = FinancialDateUtils.calculateNextWorkDate(effdate, hldyDates);
			count ++;
		}
		
		logger.debug("重定利率下一个计息开始日期:"+String.format("%1$tY/%1$tm/%1$td", effdate));
		
		return String.format("%1$tY/%1$tm/%1$td", effdate);
	}

	@Override
	public SlOutBean saveYieldData(Map<String,YieldCurveIndex> ycrtMap) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIycrBean> iycrBeans = new ArrayList<SlIycrBean>();
		Date  postdate = null;
		int count = 0;
		try {
			// 获取OPICS中定义的CONTRIBRATE曲线
			List<ImportYieldCurveBean> yield = ycrtMapper.selectOpicsYcrtData();
			// 当前系统日期
			postdate = Calendar.getInstance().getTime();
			// IYCR表最指定日期最大流水号
			count = getMaxFedealNo(postdate, RateFeedTypes.YCRATE);
			
			for (ImportYieldCurveBean opics : yield) {
					SlIycrBean iycrBean = new SlIycrBean(opics.getBr()); 
					SlInthBean inthBean = iycrBean.getInthBean();
					logger.debug("收益率曲线待导入数据："+ opics);
					count++;
					inthBean.setFedealno(createFedealno(postdate, count));
					YieldCurveIndex excel = ycrtMap.get(StringUtils.trim(opics.getCcy())+StringUtils.trim(opics.getYieldCurveName())
						+StringUtils.trim(opics.getDesignatedMaturity()));
					logger.debug("收益率曲线获取EXCEL数据KEY："+ StringUtils.trim(opics.getCcy())+StringUtils.trim(opics.getYieldCurveName())
						+StringUtils.trim(opics.getDesignatedMaturity()));
					logger.debug("收益率曲线EXCEL导入数据："+excel);
					
					//EXCEL中没有数据则不导入,系统中的值就是上一工作日的值
					if(null == excel) {
						continue;
					}
					
					iycrBean.setRatetype(opics.getRatetype());
					iycrBean.setCcy(opics.getCcy());
					iycrBean.setYieldcurve(opics.getYieldCurveName());
					iycrBean.setMtystart(opics.getMtystart());// 默认值
					iycrBean.setMtyend(opics.getDesignatedMaturity());
					iycrBean.setBidrate8(excel.getRateBid().doubleValue());
					iycrBean.setOfferrate8(excel.getRateAsk().doubleValue());
					
					//如果EXCEL中的值有一个为0则也不导入
					if(0.0 == iycrBean.getBidrate8() || 0.0 == iycrBean.getOfferrate8()) {
						continue;
					}
					
					iycrBean.setLstmntdate(DateUtil.format(postdate));
					
					inthBeans.add(inthBean);
					iycrBeans.add(iycrBean);
						
			}
			// 批量提交
			batchDao.batch("com.singlee.financial.mapper.IycrMapper.addIycr", iycrBeans);
			batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			outBean.setRetMsg("IYCR操作成功!");
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IYCR操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("收益率曲线数据处理异常!", e);
			throw new RuntimeException(e);
		}
		return outBean;
	}
	
	/**
	 * 计算中间价格
	 * @param rateAsk
	 * @param rateBid
	 * @return
	 */
	private BigDecimal getRate(BigDecimal rateAsk,BigDecimal rateBid,BigDecimal rateMid) {
		BigDecimal big = null;
		BigDecimal bigRateAsk = null;
		BigDecimal bigRateBid = null;
		BigDecimal bigRateMid = null;
		
		if(null == rateAsk) {
			bigRateAsk = BigDecimal.ZERO;
		}else {
			bigRateAsk = rateAsk;
		}
		if(null == rateBid) {
			bigRateBid = BigDecimal.ZERO;
		}else {
			bigRateBid = rateBid;
		}
		if(null == rateMid) {
			bigRateMid = BigDecimal.ZERO;
		}else {
			bigRateMid = rateMid;
		}
		
		logger.debug("EXCEL数据,ASK："+rateAsk+",BID:"+rateBid+",MID:"+rateMid);
		
		if(BigDecimal.ZERO.compareTo(bigRateAsk) != 0 && BigDecimal.ZERO.compareTo(bigRateBid) != 0 && BigDecimal.ZERO.compareTo(bigRateMid) == 0){
			big = bigRateBid.add(bigRateAsk).divide(new BigDecimal("2"), 8, RoundingMode.HALF_UP);
		}else if(BigDecimal.ZERO.compareTo(bigRateAsk) == 0 && BigDecimal.ZERO.compareTo(bigRateBid) == 0 && BigDecimal.ZERO.compareTo(bigRateMid) != 0){
			big = bigRateMid;
		}else {
			big = BigDecimal.ZERO;
		}
		
		logger.debug("EXCEL数据,计算后的利率值："+big);
		
		return big;
	}
	
	/**
	 * 
	 * @param spotRateOpics
	 * @param fxRateOpics
	 * @param mty
	 * @param ccy
	 * @param spotRate
	 * @param fxRateAsk
	 * @param fxRateBid
	 * @param fxRate
	 * @return
	 */
	private String getRate(String ccy, String mty,Map<String, FxRevaluationRates> frRateMap, FxRateTypes fxRateType) {
		BigDecimal big = null;
		FxRevaluationRates excel = null;

		excel = frRateMap.get(StringUtils.trim(ccy) + StringUtils.trim(mty));

		logger.debug("外汇汇率EXCEL数据[" + StringUtils.trim(ccy) + StringUtils.trim(mty) + "]:" + excel);

		// 期限点为空,则不用赋值
		if (!org.springframework.util.StringUtils.hasText(mty)) {
			return null;
		}

		// excel值为空时使用上一日的值
		if (excel == null) {
			return null;
		}
		
		//获取汇率值
		big = getRate(excel.getRateAsk(), excel.getRateBid(), excel.getRate());

		if("SPOT".equalsIgnoreCase(mty)) {
			//即期汇率为实际值,不在处理
		}else if (fxRateType.equals(FxRateTypes.Pips) || fxRateType.equals(FxRateTypes.Points)) {
			big = big.divide(fxRateType.getValue(), 8, RoundingMode.HALF_UP);
			// 掉期点都是基于SPOTRATE为当前批量日期加2天,TOM为当前批量日期加1天,1D为SPOTDATE加1天
			if ("TOM".equalsIgnoreCase(StringUtils.trim(mty))) {
				big = big.negate();
			}
		} else if (fxRateType.equals(FxRateTypes.Rate)) {
			FxRevaluationRates excelSpotRate = frRateMap.get(StringUtils.trim(ccy) + "SPOT");
			if (null == excelSpotRate) {
				big = null;
			} else {
				big = big.subtract(getRate(excelSpotRate.getRateAsk(), excelSpotRate.getRateBid(), excelSpotRate.getRate()));
				if ("TOM".equalsIgnoreCase(StringUtils.trim(mty))) {
					big = big.negate();
				}//end if
			} // end if else
		} // end if else if ....

		logger.debug("外汇汇率最终导入值[" + StringUtils.trim(ccy) + StringUtils.trim(mty) + "]：" + big);

		return null == big || BigDecimal.ZERO.compareTo(big) == 0 ? null : big.stripTrailingZeros().toPlainString();
	}
	

	@Override
	public SlOutBean saveVoldData(Map<String,OptionsVolatility> ovlMap,VolatilityTypes volatilityType) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIvolBean> ivolBeans = new ArrayList<SlIvolBean>();
		int count = 0;
		Date postdate = null;
		Map<String,ImportVolBean> opicsDetal50 = null;
		try {
			//当前系统日期
			postdate = Calendar.getInstance().getTime();
			
			//ISEL表最指定日期最大流水号
			count = getMaxFedealNo(postdate,RateFeedTypes.VOLRATE);
			
			// 获取OPICS中定义的波动率
			//徽商 目前只导入02网点
			List<ImportVolBean> volds = ivolMapper.selectOpicsVoldData();
			
			//如果传入的波动率为Volatility时需要保存detal50进行计算
			if(VolatilityTypes.Volatility.equals(volatilityType)) {
				//获取OPICSdetal50的数据,填充excel对象detal50数据
				createDetal50Data(ovlMap, volds);
			}//end if
			
			logger.debug("波动率读取EXCEL数据:"+ovlMap);
			
			logger.debug("Opics波动率Detal50数据:"+opicsDetal50);
			
			for (ImportVolBean vold : volds) {
				SlIvolBean ivolBean = new SlIvolBean(vold.getBr());
				SlInthBean inthBean = ivolBean.getInthBean();
				count++;
				inthBean.setFedealno(createFedealno(postdate, count));
				logger.debug("波动率待导入对象："+vold);
				// 根据记录查询需要的call和对于的put值,没找到则舍去FI+CCY/USD+10+C+2M
				String putKey = StringUtils.trim(vold.getOptiontype())+StringUtils.trim(vold.getOptiontypekey())+StringUtils.trim(vold.getDesmat())+"P"+vold.getStrikeprice().intValue();
				String callKey = StringUtils.trim(vold.getOptiontype())+StringUtils.trim(vold.getOptiontypekey())+StringUtils.trim(vold.getDesmat())+"C"+vold.getStrikeprice().intValue();
				logger.debug("波动率callKey："+callKey+",波动率putKey："+putKey);
				OptionsVolatility excelPut = ovlMap.get(putKey);
				OptionsVolatility excelCall = ovlMap.get(callKey);
				
				if(null == excelCall && null == excelPut) {
					//EXCEL中无数据不插入接口表
					continue;
				}
				ivolBean.setOptiontype(vold.getOptiontype());
				ivolBean.setOptiontypekey(vold.getOptiontypekey());
				ivolBean.setDesmat(vold.getDesmat());
				ivolBean.setAtmoneyind(vold.getAtmoneyind());
				ivolBean.setTerm(vold.getTerm());
				ivolBean.setRiskrev8(BigDecimal.ZERO);
				ivolBean.setStranglemgn8(BigDecimal.ZERO);
				ivolBean.setStrikeprice8(vold.getStrikeprice());
				
				//设置波动率数据
				setVolatility(ivolBean,vold,excelPut,excelCall,volatilityType);
				
				if((null == ivolBean.getCallaskvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getCallaskvolatility8())  == 0) &&
						(null == ivolBean.getCallbidvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getCallbidvolatility8())  == 0) &&
						(null == ivolBean.getPutaskvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getPutaskvolatility8())  == 0) &&
						(null == ivolBean.getPutbidvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getPutbidvolatility8())  == 0)) {
					//EXCEL波动率数据都为空,不导入系统
					continue;
				}
					
				ivolBeans.add(ivolBean);
				inthBeans.add(inthBean);
			}
			// 批量提交
			if (ivolBeans.size() > 0) {
				batchDao.batch("com.singlee.financial.mapper.IvolMapper.addIvol", ivolBeans);
				batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			}
			outBean.setRetMsg(String.format("IVOL操作成功,导入%s条数据!", ivolBeans.size()));
		} catch (Exception e) {
			outBean.setRetMsg(String.format("IVOL操作失败失败原因%s!", e));
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("波动率数据处理异常!", e);
			throw new RuntimeException(e);
		}
		return outBean;
	}
	
	/**
	 * 设置期权波动率数据
	 * 
	 * @param ivolBean
	 * @param opics
	 * @param excelPut
	 * @param excelCall
	 * @param opicsDetal50
	 * @param excelDetal50
	 */
	private void setVolatility(SlIvolBean ivolBean, ImportVolBean opics, OptionsVolatility excelPut, OptionsVolatility excelCall, VolatilityTypes volatilityType) {
		BigDecimal volDetal50 = null;
		BigDecimal vol = null;
		logger.debug("Excel PUT波动率 ：" + excelPut);
		logger.debug("Excel CALL波动率 ：" + excelCall);

		/*
		 * EXCEL数据为Volatility时需要计算,该期限点值减去DETAL50的值
		 */
		if (null != excelPut) {
			if (VolatilityTypes.Volatility.equals(volatilityType)) {
				vol = excelPut.getVolatilityAsk();
				volDetal50 = excelPut.getVolatilityAmtRate();
				if(null != vol && BigDecimal.ZERO.compareTo(vol) !=0 && null != volDetal50 && BigDecimal.ZERO.compareTo(volDetal50) !=0) {
					// 计算put ask波动率数据
					ivolBean.setPutaskvolatility8(vol.subtract(volDetal50));
				}
				vol = excelPut.getVolatilityBid();
				if(null != vol && BigDecimal.ZERO.compareTo(vol) !=0 && null != volDetal50 && BigDecimal.ZERO.compareTo(volDetal50) !=0) {
					// 计算put bid波动率数据
					ivolBean.setPutbidvolatility8(vol.subtract(volDetal50));
				}
			} else {
				ivolBean.setPutaskvolatility8(excelPut.getVolatilityAsk());
				ivolBean.setPutbidvolatility8(excelPut.getVolatilityBid());
			}//end if else
		}//end if
		if (null != excelCall) {
			if (VolatilityTypes.Volatility.equals(volatilityType)) {
				vol = excelCall.getVolatilityAsk();
				volDetal50 = excelCall.getVolatilityAmtRate();
				if(null != vol && BigDecimal.ZERO.compareTo(vol) !=0 && null != volDetal50 && BigDecimal.ZERO.compareTo(volDetal50) !=0) {
					// 计算call ask波动率数据
					ivolBean.setCallaskvolatility8(vol.subtract(volDetal50));
				}
				vol = excelCall.getVolatilityBid();
				if(null != vol && BigDecimal.ZERO.compareTo(vol) !=0 && null != volDetal50 && BigDecimal.ZERO.compareTo(volDetal50) !=0) {
					// 计算call bid波动率数据
					ivolBean.setCallbidvolatility8(vol.subtract(volDetal50));
				}
			} else {
				ivolBean.setCallaskvolatility8(excelCall.getVolatilityAsk());
				ivolBean.setCallbidvolatility8(excelCall.getVolatilityBid());
			}//end if else
		}//end if
		
		//call中的数据,如果ask和bid有一个为空则都不导入
		if((null == ivolBean.getCallaskvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getCallaskvolatility8())  == 0) ||
				(null == ivolBean.getCallbidvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getCallbidvolatility8())  == 0)) {
			ivolBean.setCallaskvolatility8(null);
			ivolBean.setCallbidvolatility8(null);
		}
		//PUT中的数据,如果ask和bid有一个为空则都不导入
		if((null == ivolBean.getPutaskvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getPutaskvolatility8())  == 0) ||
				(null == ivolBean.getPutbidvolatility8() || BigDecimal.ZERO.compareTo(ivolBean.getPutbidvolatility8())  == 0)) {
			ivolBean.setPutaskvolatility8(null);
			ivolBean.setPutbidvolatility8(null);
		}
		logger.debug("设置SlIvolBean对象值 ：" + ivolBean);
	}
	
	/**
	 * 获取OPICSdetal50的数据,填充excel对象detal50数据
	 * @param ovlMap
	 * @param volds
	 * @return
	 */
	private void createDetal50Data(Map<String,OptionsVolatility> ovlMap,List<ImportVolBean> volds){
		Map<String,OptionsVolatility> excelDetal50 = new HashMap<String,OptionsVolatility>();
		
		//保存EXCEL导入数据DETAL50的数据
		for (OptionsVolatility var : ovlMap.values()) {
			if(var.getStrikePrice().intValue() != 50) {
				continue;
			}//end if
			excelDetal50.put(var.getOptionType()+var.getCcyParis()+var.getDesignatedMaturity()+var.getCallputInd(), var);
		}//end for
		//填充EXCEL中每条记录中DETAL50的数据
		for (OptionsVolatility var : ovlMap.values()) {
			OptionsVolatility detal50 = excelDetal50.get(var.getOptionType()+var.getCcyParis()+var.getDesignatedMaturity()+var.getCallputInd());
			var.setVolatilityAmtRate(getRate(detal50.getVolatilityAsk(), detal50.getVolatilityBid(), detal50.getVolatility()));
		}//end for
		
	}
	
	@Override
	public HashMap<String, String> queryImportSecurData() {
		HashMap<String, String> map = new HashMap<String, String>();
		//查询OPICS中待导入债券ID及托管账号信息
		List<ImportSeclPriceBean> list = seclMapper.selectImportSecurData();
		//将list转换成MAP对象
		for (ImportSeclPriceBean importSeclPriceBean : list) {
			map.put(StringUtils.trim(importSeclPriceBean.getSecid()), StringUtils.trim(importSeclPriceBean.getSecsAcct()));
		}
		return map;
	}

	@Override
	public SlOutBean savePriceData(Map<String,BondMarketPrices> excelMap) {
		SlOutBean outBean = new SlOutBean();
		
		try {
			//查询opics中未到期的债券信息
			List<ImportSeclPriceBean> list = seclMapper.selectOpicsSeclData();
			//调用保存数据方法
			outBean = savePriceData(excelMap,list);
		} catch (Exception e) {
			outBean.setRetMsg(e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("债券收盘价数据处理异常!", e);
			throw new RuntimeException(e);
		}
		
		return outBean;
	}
	
	/**
	 * 根据OPICS数据及EXCEL组装ISEL、INTH表数据,并导入数据库
	 * @param excelMap
	 * @param list
	 * @return
	 */
	public SlOutBean savePriceData(Map<String,BondMarketPrices> excelMap,List<ImportSeclPriceBean> list) {
		List<SlInthBean> inthBeans = new ArrayList<SlInthBean>();
		List<SlIselBean> iselBeans = new ArrayList<SlIselBean>();
		SlOutBean outBean = new SlOutBean();
		int count = 0;
		Date postdate = null;
		
		try {
			if (null == excelMap || excelMap.size() == 0) {
				logger.info("外部债券收盘价信息为空");
	            throw new RuntimeException("外部债券收盘价信息为空");
	        }
			
			if (null == list || list.size() == 0) {
				logger.info("Opics待导入债券信息为空");
	            throw new RuntimeException("Opics待导入债券信息为空");
	        }
			//当前系统日期
			postdate = Calendar.getInstance().getTime();
			
			//ISEL表最指定日期最大流水号
			count = getMaxFedealNo(postdate,RateFeedTypes.SCML);
			
			//根据EXCLE以及OPICS中的数据生成ISEL、INTH接口数据
			for (ImportSeclPriceBean importSeclPriceBean : list) {
				SlIselBean iselBean = new SlIselBean(importSeclPriceBean.getBr());
				SlInthBean inthBean = iselBean.getInthBean();
				
				//流水号加1
				count++;
				
				if(org.springframework.util.StringUtils.hasText(importSeclPriceBean.getFedealno())) {
					inthBean.setFedealno(importSeclPriceBean.getFedealno());
				}else {
					inthBean.setFedealno(createFedealno(postdate, count));
				}
				iselBean.setClsgprice8(getPrice(importSeclPriceBean, excelMap.get(importSeclPriceBean.getSecid())));// 收盘价
				iselBean.setSecid(StringUtils.trim(importSeclPriceBean.getSecid()));
				iselBeans.add(iselBean);
				inthBeans.add(inthBean);
			}
			
			//批量提交ISEL表
			batchDao.batch("com.singlee.financial.mapper.IselMapper.addIsel", iselBeans);
			
			//批量提交INTH表
			batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
			
			//处理成功
			outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
			outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.S);
		} catch (Exception e) {
			outBean.setRetMsg(e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("债券收盘价数据处理异常!", e);
			throw new RuntimeException(e);
		}
		
		return outBean;
	}
	
	/**
	 * 获取ISEL表指定日期最大流水号
	 * @param postdate
	 * @return
	 */
	@Override
	public int getMaxFedealNo(Date postdate,RateFeedTypes rateFeedType) {
		int count = 0;
		String fedealno = null;
		String srtPostdate = null;
		String seq = null;
		
		srtPostdate = String.format("%1$tY%1$tm%1$td%2$s",postdate,"%");
		
		if(rateFeedType.equals(RateFeedTypes.INTRATE)) {
			fedealno = irhsMapper.selectIrhsMaxFedealno(null,SlDealModule.IRHS.SERVER,srtPostdate);
		}else if(rateFeedType.equals(RateFeedTypes.FXRATE)) {
			fedealno = irevMapper.selectIrevMaxFedealno(null,SlDealModule.IREV.SERVER,srtPostdate);
		}else if(rateFeedType.equals(RateFeedTypes.VOLRATE)) {
			fedealno = ivolMapper.selectIvolMaxFedealno(null,SlDealModule.IVOL.SERVER,srtPostdate);
		}else if(rateFeedType.equals(RateFeedTypes.YCRATE)) {
			fedealno = iycrMapper.selectIycrMaxFedealno(null,SlDealModule.IYCR.SERVER,srtPostdate);
		}else if(rateFeedType.equals(RateFeedTypes.SCML)) {
			fedealno = iselMapper.selectIselMaxFedealno(null,SlDealModule.ISEL.SERVER,srtPostdate);
		}else {
			throw new RuntimeException("市场数据类型["+rateFeedType.name()+"]不支持");
		}
		
		logger.info(String.format("市场数据[%s]接口表最大流水号:%s", rateFeedType.name(),fedealno));
		
		if(org.springframework.util.StringUtils.hasText(fedealno)) {
			seq = StringUtils.trim(fedealno.substring(8));
			logger.info(String.format("市场数据[%s]接口表最大序号:%s", rateFeedType.name(),seq));
			//查询ISEL表最大流水号
			count = Integer.parseInt(seq);
		}else {
			count = 0;
		}
		
		return count;
	}
	
	/**
	 * 创建OPICS接口表FEDEALNO
	 * @param postdate
	 * @param count
	 * @return
	 */
	@Override
	public String createFedealno(Date postdate,int count) {
		if(count > 9999999) {
			throw new RuntimeException("Opics接口表FEDEALNO字段序号最大只支持9999999!");
		}
		return String.format("%1$tY%1$tm%1$td%2$07d",postdate,count);
	}

	@Override
	public Boolean insertToIrev(List<SlIrevBean> irevBeans, List<SlInthBean> inthBeans) {
		Boolean flag=true;
		try {
			batchDao.batch("com.singlee.financial.mapper.IrevMapper.addIrev", irevBeans);
			batchDao.batch("com.singlee.financial.mapper.CommonMapper.addSlFundInth", inthBeans);
		}catch (Exception e){

			flag=false;
			logger.error("批量插入irev异常", e);
			throw new RuntimeException(e);
		}


		return flag;
	}

	/**
	 * 贴现债券和MBS债券估值进行特殊处理 贴现债券使用全价,MBS债券要转换为以100计价的估值
	 * 
	 * @param opics
	 * @param excel
	 * @param excleConfigBean
	 * @return
	 */
	public BigDecimal getPrice(ImportSeclPriceBean opics, BondMarketPrices excel) {
		BigDecimal price = null;
		
		if(null == excel) {
			// 如果EXCEL中的价格为0或者没有数据,则设置默认价格为100
			//如果Opics中的价格有值,则沿用上一工作日的数值
			if ( null != opics.getPrice() && opics.getPrice().doubleValue() > 0.0) {
				logger.debug("OpicsSecmPrice Excel Price is Null Or 0,Set Securities[" + opics.getSecid() + "] Previous value:"+opics.getPrice().doubleValue());
				price = opics.getPrice();
			}else {
				logger.debug("OpicsSecmPrice Excel、Price、Opics is Null Or 0,Set Securities[" + opics.getSecid() + "] Default value 100");
				price = FinancialCalculationModel.ONE_HUNDRED;
			}
			return price;
		}

		//贴现债券使用全价,其它债券用净价
		if (Boolean.parseBoolean(externalIntfc.getIsSeclExcelDisSecIsUseDirtyPrice()) && "DIS".equalsIgnoreCase(opics.getIntcalcrule())) {
			logger.debug("OpicsSecmPrice DIS securities Is Use Dirty Price");
			price = excel.getDirtyPrice();
		} else {
			logger.debug("OpicsSecmPrice Other securities Default Clean Price");
			price = excel.getCleanPrice();
		}

		//MBS债券,需要将中债的价格转换为百比例
		if ("MBS".equalsIgnoreCase(opics.getIntcalcrule()) && Boolean.parseBoolean(externalIntfc.getIsSeclExcelMbsSecPriceIsConvert())) {
			logger.debug("OpicsSecmPrice MBS securities[" + opics.getSecid() + "] Convert Price To Percentage");
			//如果中债的剩余本金比例和OPICS的FACTOR相同,则使用opics的FACTOR进行计算
			//否则使用中债的剩余比例进行计算
			if (null != opics.getFactor() && 0 != opics.getFactor().compareTo(BigDecimal.ZERO) && 0 == opics.getFactor().multiply(FinancialCalculationModel.ONE_HUNDRED).compareTo(excel.getParValue())) {
				logger.debug("OpicsSecmPric MBS Use Opics Factor Calculate");
				price = price.divide(opics.getFactor(), 8, RoundingMode.HALF_UP);
			} else {
				logger.debug("OpicsSecmPrice MBS Use CDTC ParValue Calculate");
				price = price.divide(excel.getParValue().divide(FinancialCalculationModel.ONE_HUNDRED), 8, RoundingMode.HALF_UP);
			}
			logger.debug(
					"OpicsSecmPrice MBS securities excel Price:" + excel.getCleanPrice().doubleValue() + ",Convert price:" + price.doubleValue() + ",Opics Factor:" + opics.getFactor()
							+ ",Excel ParValue:" + excel.getParValue());
		}

		return price;
	}

	@Override
	public SlOutBean savePriceDataHscb(Map<String, BondMarketPrices> excelMap) {
		SlOutBean outBean = new SlOutBean();
		List<SlFiMarketBean> list = new ArrayList<SlFiMarketBean>();
		int count = 0;
		Date postdate = null;
		try {

			if (null == excelMap || excelMap.size() == 0) {
				logger.info("没有要导入的债券收盘价信息");
				throw new RuntimeException("没有要导入的债券收盘价信息");
			}

			// 当前系统日期
			postdate = Calendar.getInstance().getTime();

			// ISEL表最指定日期最大流水号
			count = getMaxFedealNo(postdate, RateFeedTypes.SCML);
			
			// 删除当前批量数据
			slFiMarketMapper.deleteFiMarket();

			// 查询opics中需要导入的债券信息
			List<ImportSeclPriceBean> Opicslist = seclMapper.selectOpicsSeclData();
			logger.info("待导入OPICS系统的债券信息[ISEL]记录数:"+(null == Opicslist ? 0 : Opicslist.size()));
			// 组装SlFiMarketBean对象数据
			for (ImportSeclPriceBean importSeclPriceBean : Opicslist) {
				count++;
				importSeclPriceBean.setFedealno(createFedealno(postdate, count));
				list.add(createSlFiMarketBean(importSeclPriceBean, excelMap.get(importSeclPriceBean.getSecid())));
			}

			logger.debug("待导入的市场数据信息[SlFiMarketBean]记录数:" + (null == list ? 0 : list.size()));
			
			// 批量提交SL_FI_MARKET表
			batchDao.batch("com.singlee.financial.mapper.ISlFiMarketMapper.insertSlFiMarket", list);
			// 添加ISEL、INTH表数据
			outBean = savePriceData(excelMap,Opicslist);

		} catch (Exception e) {
			outBean.setRetMsg(e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode(SlErrors.FAILED.getErrCode());
			logger.error("徽商银行债券收盘价数据处理异常!", e);
			throw new RuntimeException(e);
		}
		return outBean;
	}
	
	/**
	 * 设置SeclPriceBean对象属性值
	 * @param opics
	 * @param excel
	 * @param configMap
	 */
	private SlFiMarketBean createSlFiMarketBean(ImportSeclPriceBean opics, BondMarketPrices excel){
		SlFiMarketBean slFiMarket = new  SlFiMarketBean();
		
		slFiMarket.setPrice(getPrice(opics,excel));
		slFiMarket.setSecid(opics.getSecid());
		slFiMarket.setFedealno(opics.getFedealno());
		slFiMarket.setBr(opics.getBr());
		
		if(null != excel) {
			slFiMarket.setMarket(excel.getMarket()+"-"+excel.getSecsacct());
			slFiMarket.setCondition(excel.getCondition());
			slFiMarket.setDuration(excel.getDuration());
			slFiMarket.setCleanPrice(excel.getCleanPrice());
			slFiMarket.setDirtyPrice(excel.getDirtyPrice());
		}
		
		logger.debug("SlFiMarketBean Set Value:"+slFiMarket);
		
		return slFiMarket;
	}
	
}
