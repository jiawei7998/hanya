package com.singlee.financial.opics;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProperties;
import com.singlee.financial.common.util.SafeProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 主要操作配置文件的增删改
 * 
 * @author shenzl
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PropertiesServer implements IPropertiesServer {

	private SafeProperties safe;
	private File file;
	private FileOutputStream fos;

	private void init() throws IOException {
		if (null == safe) {
			safe = new SafeProperties();
			String configFilePath = PropertiesServer.class.getResource("/").getFile();
			file = new File(configFilePath + "external.properties");
			// 操作对象加载file,用于后续操作
			safe.load(file);
		}
	}

	@Override
	public SlOutBean operateProperties(SlProperties properties) {
		try {
			init();
			// 设置新的追加属性
			safe.setProperty(properties.getKey(), properties.getValue(), properties.getCommont());
			// 获取新的流
			fos = new FileOutputStream(file);
			// 保存
			safe.store(fos);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	@Override
	public List<SlProperties> getProperties() {
		List<SlProperties> array = null;
		try {
			init();
			array = new ArrayList<SlProperties>();
			for (String key : safe.propertyNames()) {
				SlProperties properties = new SlProperties();
				properties.setKey(key);
				properties.setValue(safe.getProperty(key));
				array.add(properties);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return array;
	}

}
