package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCustSettleAcct;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.SlCustSettleAcctMapper;
import com.singlee.financial.page.PageInfo;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlCustSettleAcctServer implements ISlCustSettleAcctServer{
	@Autowired
	SlCustSettleAcctMapper custSettleAcctMapper;

	@Override
	public PageInfo<SlCustSettleAcct> getCSList(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlCustSettleAcct> page = custSettleAcctMapper.getCSList(map, rb);
		return new PageInfo<SlCustSettleAcct>(page);
	}

	@Override
	public void updateByCno(Map<String, Object> map) {
		custSettleAcctMapper.updateByCno(map);
	}

	@Override
	public SlCustSettleAcct selectByCno(Map<String, Object> map) {
		return custSettleAcctMapper.selectByCno(map);
	}

	@Override
	public void addCustSettle(Map<String, Object> map) {
		SlCustSettleAcct acct=custSettleAcctMapper.selectMaxSeqByCno(map);
		if(acct==null){
			map.put("seq", "0");
			if(map.get("dealFlag")==null|| "".equals(map.get("dealFlag"))) {
				map.put("dealFlag", "1");
			}
			
			
		}else{
			int num=Integer.valueOf(acct.getSeq());
			map.put("seq", String.valueOf(num+1));
		}
		custSettleAcctMapper.addCustSettle(map);
		
	}

	@Override
	public void deleteSettle(Map<String, Object> map) {
		custSettleAcctMapper.deleteSettle(map);
	}

	@Override
	public SlCustSettleAcct selectById(Map<String, Object> map) {
		return custSettleAcctMapper.selectById(map);
	}

	@Override
	public List<SlCustSettleAcct> getListByCno(Map<String, Object> map) {
		return custSettleAcctMapper.getListByCno(map);
	}

	@Override
	public void updateIsdefaultById(Map<String, Object> map) {
		custSettleAcctMapper.updateIsdefaultById(map);
	}

	@Override
	public void updateDealFlagById(Map<String, Object> map) {
		if("1".equals(map.get("dealFlag"))){//经办
			custSettleAcctMapper.updateManageDealFlagById(map);
		}else{//复核或退回经办
			custSettleAcctMapper.updateDealFlagById(map);
		}
	}

}
