package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSwapCrsBean;
import com.singlee.financial.bean.SlSwapCurBean;
import com.singlee.financial.bean.SlSwapIrsBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.opics.ISwapServer;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.SwapMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SwapServer implements ISwapServer {
	private static Logger logger = LoggerFactory.getLogger(SwapServer.class);
	@Autowired
	SwapMapper swapMapper;
	@Autowired
	CommonMapper commonMapper;
	

	@Override
	public SlOutBean crsSwapProc(Map<String, String> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlOutBean irsSwapProc(Map<String, String> arg0) {
		System.out.println(arg0);
		SlOutBean SlOutBean = new SlOutBean();
		arg0.put("RETCODE", "");
		arg0.put("RETMSG", "");
		arg0.put("br", "01");
		System.out.println(arg0.get("contractId") + "|" + arg0.get("tradeDate") + "|" + arg0.get("floatTrader") + "|" + arg0.get("floatInst") + "|" + arg0.get("myDir") + "|" + arg0.get("lastQty")
				+ "|" + arg0.get("startDate") + "|" + arg0.get("endDate") + "|" + arg0.get("legPrice") + "|" + arg0.get("fixedPaymentFrequency") + "|" + arg0.get("fixedLegDayCount") + "|"
				+ arg0.get("benchmarkCurveName") + "|" + arg0.get("benchmarkSpread") + "|" + arg0.get("floatPaymentFrequency") + "|" + arg0.get("interestResetFrequency") + "|"
				+ arg0.get("floatLegDayCount") + "|" + arg0.get("br") + "|" + arg0.get("product") + "|" + arg0.get("prodType") + "|" + arg0.get("cost") + "|" + arg0.get("port") + "|"
				+ arg0.get("grantSign") + "|" + arg0.get("verifySign"));
		try {
			swapMapper.cfestSwapProc(arg0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SlOutBean.setRetCode(arg0.get("RETCODE").substring(0, 3));
		SlOutBean.setRetMsg(arg0.get("RETMSG"));
		System.out.println(SlOutBean.getRetCode() + SlOutBean.getRetMsg());
		return SlOutBean;
	}

	@Override
	public SlOutBean swapCrs(SlSwapCrsBean crsBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入ISWH表
		 */
		try {
			swapMapper.addSwapCrs(crsBean);
		} catch (Exception e) {
			logger.error("货币互换导入接口>>>>>>>>>>>>>>>" + SlErrors.CRS_ADD_ISWH, e);
			outBean.setRetCode(SlErrors.CRS_ADD_ISWH.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ADD_ISWH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(crsBean.getInthBean());
		} catch (Exception e) {
			logger.error("货币互换导入接口>>>>>>>>>>>>>>>" + SlErrors.CRS_ADD_INTH, e);
			outBean.setRetCode(SlErrors.CRS_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	/***
	 * 货币互换、利率互换 冲销
	 */
	@Override
	public SlOutBean swapCrsRev(SlSwapCrsBean crsBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		try {
			// 冲销之前需要验证,看INTH表里是否有已冲销过的数据
			int c = commonMapper.getSlFundInthRev(crsBean.getInthBean());
			if (c >= 1) {
				outBean.setRetCode(SlErrors.SWAPREV_ADD_INTH_EXIST.getErrCode());
				outBean.setRetMsg(SlErrors.SWAPREV_ADD_INTH_EXIST.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
		} catch (Exception e) {
			logger.error("互换接口>>>>>>>>>>>>>>>" + SlErrors.CRSREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.CRSREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.CRSREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入ISWH表
		 */
		try {
			swapMapper.addSwapCrsRev(crsBean);
		} catch (Exception e) {
			logger.error("货币互换冲销接口>>>>>>>>>>>>>>>" + SlErrors.CRSREV_ADD_ISWH, e);
			outBean.setRetCode(SlErrors.CRSREV_ADD_ISWH.getErrCode());
			outBean.setRetMsg(SlErrors.CRSREV_ADD_ISWH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInthRev(crsBean.getInthBean());
		} catch (Exception e) {
			logger.error("货币互换冲销接口>>>>>>>>>>>>>>>" + SlErrors.CRSREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.CRSREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.CRSREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean swapIrs(SlSwapIrsBean irsBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入ISWH表
		 */
		try {
			swapMapper.addSwapIrs(irsBean);
		} catch (Exception e) {
			logger.error("利率互换导入接口>>>>>>>>>>>>>>>" + SlErrors.IRS_ADD_ISWH, e);
			outBean.setRetCode(SlErrors.IRS_ADD_ISWH.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ADD_ISWH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(irsBean.getInthBean());
		} catch (Exception e) {
			logger.error("利率互换导入接口>>>>>>>>>>>>>>>" + SlErrors.IRS_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IRS_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean swapIrsRev(SlSwapIrsBean irsBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入ISWH表
		 */
		try {
			swapMapper.addSwapIrsRev(irsBean);
		} catch (Exception e) {
			logger.error("利率互换冲销接口>>>>>>>>>>>>>>>" + SlErrors.IRSREV_ADD_ISWH, e);
			outBean.setRetCode(SlErrors.IRSREV_ADD_ISWH.getErrCode());
			outBean.setRetMsg(SlErrors.IRSREV_ADD_ISWH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(irsBean.getInthBean());
		} catch (Exception e) {
			logger.error("利率互换冲销接口>>>>>>>>>>>>>>>" + SlErrors.IRSREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IRSREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IRSREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean getInthStatus(TradeBaseBean baseBean) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlOutBean verifiedSwapCrs(SlSwapCrsBean crsBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == crsBean) {
			logger.error("货币互换导入接口>>>>>>>>>>>>>>>" + SlErrors.CRS_ISWH_NOT_EXIST);
			outBean.setRetCode(SlErrors.CRS_ISWH_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断execId是否存在 */
		if (!StringUtils.isNotEmpty(crsBean.getExecId())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断IDLD表中是否已经存在
		if (swapMapper.getSlFundCrsAndIRS(crsBean.getExecId()) >= 1) {
			logger.error("货币互换导入接口>>>>>>>>>>>>>>>" + SlErrors.CRS_ISWH_EXIST);
			outBean.setRetCode(SlErrors.CRS_ISWH_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空
		if (!StringUtils.isNotEmpty(crsBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(crsBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(crsBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("货币互换导入接口>>>>>>>>>>>>>>>" + SlErrors.CRS_ISWH_EXIST);
			outBean.setRetCode(SlErrors.CRS_ISWH_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId是否为空
		if (!StringUtils.isNotEmpty(crsBean.getContraPatryInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId在CUST表中是否存在
		if (commonMapper.getCno(crsBean.getContraPatryInstitutionInfo().getTradeId()) <= 0) {
			logger.error("货币互换导入接口>>>>>>>>>>>>>>>" + SlErrors.CRS_ISWH_EXIST);
			outBean.setRetCode(SlErrors.CRS_ISWH_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断custrefno（放inth的fedealno）是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getCustrefno())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算路径是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getCcysmeans())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_CCYSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CCYSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算账户是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getCcysacct())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_CCYSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CCYSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算路径是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getCtrsmeans())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_CTRSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CTRSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算账户是否为空
		if (!StringUtils.isNotEmpty(crsBean.getExternalBean().getCtrsacct())) {
			outBean.setRetCode(SlErrors.CRS_ISWH_CTRSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CTRSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手清算账户与NOST表中是否相同
		if (swapMapper.isCustAcctSameInNostCrs(crsBean) <= 0) {
			logger.error("货币互换导入接口>>>>>>>>>>>>>>>" + SlErrors.CRS_ISWH_CTRSACCT_NOT_SAME_EXIST);
			outBean.setRetCode(SlErrors.CRS_ISWH_CTRSACCT_NOT_SAME_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_CTRSACCT_NOT_SAME_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断买卖方向是否为空
		if (crsBean.getPs() == null) {
			outBean.setRetCode(SlErrors.CRS_ISWH_PS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CRS_ISWH_PS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	@Override
	public SlOutBean verifiedSwapIrs(SlSwapIrsBean irsBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == irsBean) {
			logger.error("利率互换导入接口>>>>>>>>>>>>>>>" + SlErrors.IRS_ISWH_NOT_EXIST);
			outBean.setRetCode(SlErrors.IRS_ISWH_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断execId是否存在 */
		if (!StringUtils.isNotEmpty(irsBean.getExecId())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断IDLD表中是否已经存在
		if (swapMapper.getSlFundCrsAndIRS(irsBean.getExecId()) >= 1) {
			logger.error("利率互换导入接口>>>>>>>>>>>>>>>" + SlErrors.IRS_ISWH_EXIST);
			outBean.setRetCode(SlErrors.IRS_ISWH_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空
		if (!StringUtils.isNotEmpty(irsBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(irsBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(irsBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("利率互换导入接口>>>>>>>>>>>>>>>" + SlErrors.IRS_ISWH_EXIST);
			outBean.setRetCode(SlErrors.IRS_ISWH_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId是否为空
		if (!StringUtils.isNotEmpty(irsBean.getContraPatryInstitutionInfo().getInstitutionId())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手在CUST表中是否存在
		if (commonMapper.getCno(irsBean.getContraPatryInstitutionInfo().getInstitutionId()) <= 0) {
			logger.error("利率互换导入接口>>>>>>>>>>>>>>>" + SlErrors.IRS_ISWH_EXIST);
			outBean.setRetCode(SlErrors.IRS_ISWH_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断custrefno（放inth的fedealno）是否为空
		if (!StringUtils.isNotEmpty(irsBean.getExternalBean().getCustrefno())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/*
		 * //判断本方清算路径是否为空 if(!StringUtils.isNotEmpty(irsBean.getExternalBean().getCcysmeans())) { outBean.setRetCode(SlErrors.IRS_ISWH_CCYSMEANS_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors.IRS_ISWH_CCYSMEANS_EXIST.getErrMsg()); outBean.setRetStatus(RetStatusEnum.F); return outBean; } //判断本方清算账户是否为空
		 * if(!StringUtils.isNotEmpty(irsBean.getExternalBean().getCcysacct())) { outBean.setRetCode(SlErrors.IRS_ISWH_CCYSACCT_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors.IRS_ISWH_CCYSACCT_EXIST.getErrMsg()); outBean.setRetStatus(RetStatusEnum.F); return outBean; } //判断对方清算路径是否为空
		 * if(!StringUtils.isNotEmpty(irsBean.getExternalBean().getCtrsmeans())) { outBean.setRetCode(SlErrors.IRS_ISWH_CTRSMEANS_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors.IRS_ISWH_CTRSMEANS_EXIST.getErrMsg()); outBean.setRetStatus(RetStatusEnum.F); return outBean; } //判断对方清算账户是否为空
		 * if(!StringUtils.isNotEmpty(irsBean.getExternalBean().getCtrsacct())) { outBean.setRetCode(SlErrors.IRS_ISWH_CTRSACCT_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors.IRS_ISWH_CTRSACCT_EXIST.getErrMsg()); outBean.setRetStatus(RetStatusEnum.F); return outBean; } //判断对手清算账户与NOST表中是否相同 if
		 * (swapMapper.isCustAcctSameInNostIrs(irsBean) <= 0) { logger.error("利率互换导入接口>>>>>>>>>>>>>>>"+ SlErrors.IRS_ISWH_CTRSACCT_NOT_SAME_EXIST);
		 * outBean.setRetCode(SlErrors.IRS_ISWH_CTRSACCT_NOT_SAME_EXIST.getErrCode()); outBean.setRetMsg(SlErrors.IRS_ISWH_CTRSACCT_NOT_SAME_EXIST.getErrMsg()); outBean.setRetStatus(RetStatusEnum.F);
		 * return outBean; }
		 */

		// 判断买卖方向是否为空
		/*
		 * if (irsBean.getPs() == null) { outBean.setRetCode(SlErrors.IRS_ISWH_PS_EXIST.getErrCode()); outBean.setRetMsg(SlErrors.IRS_ISWH_PS_EXIST.getErrMsg()); outBean.setRetStatus(RetStatusEnum.F);
		 * return outBean; }
		 */

		// 判断利率曲线1是否为空

		if (!StringUtils.isNotEmpty(irsBean.getPayYieldcurve())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断利率曲线1在YCHD表中是否存在

		// 判断利率曲线2是否为空
		if (!StringUtils.isNotEmpty(irsBean.getRecYieldcurve())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断利率曲线2在YCHD表中是否存在

		// 判断利率代码1是否为空
		if (!StringUtils.isNotEmpty(irsBean.getRateCode())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断利率代码1在RATE表中是否存在

		// 判断利率代码2是否为空
		if (!StringUtils.isNotEmpty(irsBean.getRecRateCode())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断利率代码2在RATE表中是否存在

		// 判断损益是否为空
		if (!StringUtils.isNotEmpty(irsBean.getPlmethod())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断固定/浮动方向是否为空
		if (!StringUtils.isNotEmpty(irsBean.getSwapType())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断左端币种是否为空
		if (!StringUtils.isNotEmpty(irsBean.getPayNotCcy())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断右端币种是否为空
		if (!StringUtils.isNotEmpty(irsBean.getRecNotCcy())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断左端金额是否为空
		if (irsBean.getPayNotCcyAmt() == null) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断右端金额是否为空
		if (irsBean.getRecNotCcyAmt() == null) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断到期日是否为空
		if (!StringUtils.isNotEmpty(irsBean.getMaturityDate())) {
			outBean.setRetCode(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IRS_ISWH_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	@Override
	public PageInfo<SlSwapCurBean> getSwapCrsExpose(Map<String, Object> map) {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlSwapCurBean> page=swapMapper.searchCurSwapExposureCount(map, rb);
		return new PageInfo<SlSwapCurBean>(page);
	}

	@Override
	public List<SlSwapCurBean> getSwapCrsCount(Map<String, Object> map) {
		return swapMapper.searchCurSwapExposureCount(map);
	}

}
