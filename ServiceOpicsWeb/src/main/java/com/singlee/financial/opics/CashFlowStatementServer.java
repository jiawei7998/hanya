package com.singlee.financial.opics;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCashFlowBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.ExternalMapper;
import com.singlee.financial.page.PageInfo;

/**
 * 现金流查询
 * 
 * @author fengll
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CashFlowStatementServer implements ICashFlowServer {
	@Autowired
	ExternalMapper externalMapper;
	@Override
	public PageInfo<SlCashFlowBean> getCashFlowList(Map<String, String> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlCashFlowBean> page = externalMapper.getCashFlowList(map,rb);
		return new PageInfo<SlCashFlowBean>(page);
	}
	

	
}