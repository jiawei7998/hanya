package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.mapper.SlGentMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class EdServer implements IEdServer {
	
	@Autowired
	SlGentMapper slGentMapper;

	/**
	 * 查汇率
	 */
	@Override
	public String queryRate(Map<String,String> map) {
		return slGentMapper.queryRate(map);
	}

	/**
	 * 查估值
	 */
	@Override
	public List<SlAcupBean> queryValue(Map<String, String> map) {
		return slGentMapper.queryValue(map);
	}
}