package com.singlee.financial.opics;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.*;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.PmtqFxMapper;
import com.singlee.financial.mapper.PmtqTotalMapper;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.wks.bean.opics.Pcix;
import com.singlee.financial.wks.bean.opics.Psix;
import com.singlee.financial.wks.mapper.opics.PcixMapper;
import com.singlee.financial.wks.mapper.opics.PsixMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class PmtqServer implements IPmtqServer {
	@Autowired
	PmtqTotalMapper pmtqTotalMapper;
	@Autowired
	PmtqFxMapper pmtqFxMapper;
	@Autowired
	PsixMapper psixMapper;
	@Autowired
	PcixMapper pcixMapper;
	

	@Override
	public List<SlPmtqBean> getPmtq(Map<String, Object> queryMap) {
		return pmtqTotalMapper.getPmtq(queryMap);
	}

	@Override
	public List<SlPmtqBean> getPmtqBySendflag(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		return pmtqTotalMapper.getPmtqBySendflag(map);
	}


	@Override
	public List<SlPmtqBean> getPmtqByMap(Map<String, Object> map) 
			throws RemoteConnectFailureException, Exception {
		return pmtqTotalMapper.getPmtqByMap(map);
	}

	@Override
	public SlPmtqAccBean selectRepoAmt(Map<String, String> map) {
		return pmtqTotalMapper.selectRepoAmt(map);
	}


	@Override
	public SlPmtqAccBean selectSecurAmt(Map<String, String> map) {
		return pmtqTotalMapper.selectSecurAmt(map);
	}

	@Override
	public void updateSendflag(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
	    pmtqTotalMapper.updateSendflag(map);
	}
	
	
	/**
	 * 外币记账
	 */
	@Override
	public PageInfo<SlPmtqFxBean> searchPagePmtqFx(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlPmtqFxBean> page=pmtqFxMapper.searchPagePmtqFx(map, rb);
		return new PageInfo<SlPmtqFxBean>(page);
	}

	@Override
	public SlPmtqFxBean selectById(Map<String, Object> map) {
		return pmtqFxMapper.selectById(map);
	}

	@Override
	public void handleFxbl(String sn, String oper, Date date, String note,
			String verind) {
		if("A".equals(verind)|| "1".equals(verind)){//复核或退回经办
			pmtqFxMapper.verifyFxbl(sn,oper,date,note,verind);
	    }else{
	    	pmtqFxMapper.handleFxbl(sn,oper,date,note,verind);
	    }
	}

	@Override
	public List<SlPmtqFxBean> getPmtqFxList(Map<String, Object> param) {
		
		return pmtqFxMapper.getPmtqFxList(param);
	}

	@Override
	public void fxblAdd(SlPmtqFxBean entity) {
		pmtqFxMapper.fxblAdd(entity);
	}

	@Override
	public void fxblEdit(SlPmtqFxBean entity) {
		pmtqFxMapper.fxblEdit(entity);
	}

	@Override
	public void deleteFxbl(SlPmtqFxBean entity) {
		pmtqFxMapper.deleteFxbl(entity);
	}

	@Override
	public List<PsixBean> selectPsix(PsixBean psixBean) {
		List<PsixBean> pss =new ArrayList<PsixBean>();
		Psix psix = new Psix();
		try {
			BeanUtils.copyProperties(psixBean,psix);
			List<Psix> selectPsixList = psixMapper.selectPsixList(psix);
			for (Psix p : selectPsixList) {
				PsixBean ps = new PsixBean();
				BeanUtils.copyProperties(p, ps);
				pss.add(ps);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return  pss;
	}

	@Override
	public List<PcixBean> selectPcix(PcixBean pcixBean) {
		List<PcixBean> pcs =new ArrayList<PcixBean>();
		Pcix pcix = new Pcix();
		
		try {
			BeanUtils.copyProperties(pcixBean,pcix);
			List<Pcix> selectPcixList = pcixMapper.selectPcixList(pcix);
			for (Pcix p : selectPcixList) {
				PcixBean pc = new PcixBean();
				BeanUtils.copyProperties(p, pc);
				pcs.add(pc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return pcs;
	}

	@Override
	public List<SlPmtqBean> selectBySetmeansAndProduct(String cdate) {
		return pmtqTotalMapper.selectBySetmeansAndProduct(cdate);
	}

	@Override
	public List<String> queryInthBydealno(String dealno) {
		return pmtqTotalMapper.queryInthBydealno(dealno);
	}


}
