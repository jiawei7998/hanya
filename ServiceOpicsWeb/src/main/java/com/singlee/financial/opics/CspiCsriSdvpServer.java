package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlCspiCsriBean;
import com.singlee.financial.bean.SlCspiCsriDetailBean;
import com.singlee.financial.bean.SlSdvpBean;
import com.singlee.financial.bean.SlSdvpDetailBean;
import com.singlee.financial.mapper.CspiCsriSdvpMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CspiCsriSdvpServer implements ICspiCsriSdvpServer {

	private static Logger logger = Logger.getLogger(CspiCsriSdvpServer.class);

	@Autowired
	CspiCsriSdvpMapper cspiCsriSdvpMapper;

	@Override
	public List<SlCspiCsriBean> getCspiCsri(SlCspiCsriBean cspiCsriBean) {
		logger.info("CSPICSRI收款付款输入参数：" + cspiCsriBean);
		return cspiCsriSdvpMapper.getCspiCsri(cspiCsriBean);
	}

	@Override
	public List<SlSdvpBean> getSdvp(SlSdvpBean sdvpBean) {
		logger.info("SDVP收款付款输入参数：" + sdvpBean);
		return cspiCsriSdvpMapper.getSdvp(sdvpBean);
	}

	@Override
	public List<SlCspiCsriDetailBean> searchcspicsridetail(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return cspiCsriSdvpMapper.searchcspicsridetail(map);
	}

	@Override
	public SlCspiCsriBean searchpaycspicsrihead(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return cspiCsriSdvpMapper.searchpaycspicsrihead(map);
	}
	
	@Override
	public SlCspiCsriBean searchreccspicsrihead(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return cspiCsriSdvpMapper.searchreccspicsrihead(map);
	}

	@Override
	public List<SlSdvpDetailBean> searchsdvpdetail(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return cspiCsriSdvpMapper.searchsdvpdetail(map);
	}

	@Override
	public SlSdvpBean searchpaysdvphead(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return cspiCsriSdvpMapper.searchpaysdvphead(map);
	}
	
	@Override
	public SlSdvpBean searchrecsdvphead(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return cspiCsriSdvpMapper.searchrecsdvphead(map);
	}

	@Override
	public List<SlCspiCsriDetailBean> searchcsridetail(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		
		// TODO Auto-generated method stub
		return cspiCsriSdvpMapper.searchcsridetail(map);
	}
}