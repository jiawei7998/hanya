package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlFxCashFlowBean;
import com.singlee.financial.bean.SlFxdhBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.FxdhTotalMapper;
import com.singlee.financial.page.PageInfo;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FxdhServer implements IFxdhServer{
	@Autowired
	FxdhTotalMapper fxdhMapper;

	@Override
	public SlFxdhBean getFxdhBean(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		return fxdhMapper.getFxdhBean(map);
	}

	@Override
	public PageInfo<SlFxCashFlowBean> searchFxCashFlowPage( Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlFxCashFlowBean> page=fxdhMapper.searchFxCashFlowPage(map,rb);
		return new PageInfo<SlFxCashFlowBean>(page);
	}

	@Override
	public List<SlFxdhBean> getFxdhList(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlFxdhBean> list=fxdhMapper.getFxdhList(map);
		return list;
	}

}
