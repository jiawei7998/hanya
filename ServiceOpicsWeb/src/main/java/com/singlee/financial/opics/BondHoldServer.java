package com.singlee.financial.opics;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlBondHoldBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.BondHoldMapper;
import com.singlee.financial.page.PageInfo;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 债券
 * 
 * @author wangzt
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BondHoldServer implements IBondHoldServer {
	
	@Autowired
	BondHoldMapper bondHoldMapper;

	
	@Override
	public PageInfo<SlBondHoldBean> searchBondHoldCount(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlBondHoldBean> page = bondHoldMapper.searchBondHoldCount(map, rb);
		return new PageInfo<SlBondHoldBean>(page);
	}

	@Override
	public List<String> tposList(Map<String, Object> map) {
		List<String> list =bondHoldMapper.TposList(map);
		return list	;
	}

}
