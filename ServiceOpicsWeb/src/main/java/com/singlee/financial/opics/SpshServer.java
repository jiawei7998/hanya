package com.singlee.financial.opics;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlRprhDldtBean;
import com.singlee.financial.bean.SlSpshBean;
import com.singlee.financial.mapper.SpshTotalMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SpshServer implements ISpshServer{
	@Autowired
	SpshTotalMapper spshMapper;

	@Override
	public SlSpshBean getSpshBean(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		return spshMapper.getSpshBean(map);
	}

	@Override
	public SlRprhDldtBean getRprhDldtBean(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		return spshMapper.getRprhDldtBean(map);
	}

}
