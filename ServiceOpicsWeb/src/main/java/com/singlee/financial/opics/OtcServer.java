package com.singlee.financial.opics;


import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlIotdBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Ccyp;
import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.CcypService;
import com.singlee.financial.wks.service.opics.DdftService;
import com.singlee.financial.wks.service.opics.HldyService;
import com.singlee.financial.wks.service.opics.OtdtService;
import com.singlee.financial.wks.util.FinancialDateUtils;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.OtcMapper;

/***
 * 期权
 * 
 * @author lij
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OtcServer implements IOtcServer {
	private static Logger logger = LoggerFactory.getLogger(OtcServer.class);
	@Autowired
	OtcMapper otcMapper;
	@Autowired
	CommonMapper commonMapper;
	@Autowired
	BrpsService brpsService;
	@Autowired
	OtdtService otdtService;
	@Autowired
	CcypService ccypService;
	@Autowired
	HldyService hldyService;
	@Autowired
	DdftService ddftService;

	@Override
	public SlOutBean verifiedOtc(SlIotdBean slIotdBean) {
		return null;
	}

	@Override
	public SlOutBean otc(SlIotdBean slIotdBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		
		checkEntry(slIotdBean);

		/**
		 * 插入IOTD表
		 */
		try {
			otcMapper.addOtc(slIotdBean);
		} catch (Exception e) {
			logger.error("期权导入接口>>>>>>>>>>>>>>>" + SlErrors.OTC_ADD_IOTD, e);
			outBean.setRetCode(SlErrors.OTC_ADD_IOTD.getErrCode());
			outBean.setRetMsg(SlErrors.OTC_ADD_IOTD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(slIotdBean.getInthBean());
		} catch (Exception e) {
			logger.error("期权导入接口>>>>>>>>>>>>>>>" + SlErrors.OTC_ADD_INTH, e);
			outBean.setRetCode(SlErrors.OTC_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.OTC_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	@Override
	public SlOutBean otcRev(SlIotdBean slIotdBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		
		if(brpsService.isBatchRunning(slIotdBean.getInthBean().getBr())) {
			throw new RuntimeException("系统跑批中请稍后操作!");
		}

		try {
			
			// 冲销之前需要验证,看INTH表里是否有已冲销过的数据
			int c = commonMapper.getSlFundInthRev(slIotdBean.getInthBean());
			if (c >= 1) {
				outBean.setRetCode(SlErrors.OTCREV_ADD_INTH_EXIST.getErrCode());
				outBean.setRetMsg(SlErrors.OTCREV_ADD_INTH_EXIST.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
		} catch (Exception e) {
			logger.error("期权冲销接口>>>>>>>>>>>>>>>" + SlErrors.OTC_REV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.OTC_REV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.OTC_REV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		/**
		 * 插入IOTD表
		 */
		try {
			otcMapper.addOtcRev(slIotdBean);
		} catch (Exception e) {

			logger.error("期权冲销接口>>>>>>>>>>>>>>>" + SlErrors.OTC_REV_ADD_IOTD, e);
			outBean.setRetCode(SlErrors.OTC_REV_ADD_IOTD.getErrCode());
			outBean.setRetMsg(SlErrors.OTC_REV_ADD_IOTD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInthRev(slIotdBean.getInthBean());
		} catch (Exception e) {

			logger.error("期权冲销接口>>>>>>>>>>>>>>>" + SlErrors.OTC_REV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.OTC_REV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.OTC_REV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;

	}

	/**
	 * 校验期权冲销
	 * 
	 * @param slIotdBean
	 * @return
	 */
	@Override
	public SlOutBean verifiedOtcRev(SlIotdBean slIotdBean) {
		return null;
	}

	@Override
	public SlOutBean otcExecute(SlIotdBean slIotdBean) throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		
		checkExecute(slIotdBean);

		/**
		 * 插入IOTD表
		 */
		try {
			otcMapper.addExeute(slIotdBean);
		} catch (Exception e) {
			logger.error("期权导入接口>>>>>>>>>>>>>>>" + SlErrors.OTC_ADD_IOTD, e);
			outBean.setRetCode(SlErrors.OTC_ADD_IOTD.getErrCode());
			outBean.setRetMsg(SlErrors.OTC_ADD_IOTD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(slIotdBean.getInthBean());
		} catch (Exception e) {
			logger.error("期权导入接口>>>>>>>>>>>>>>>" + SlErrors.OTC_ADD_INTH, e);
			outBean.setRetCode(SlErrors.OTC_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.OTC_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	/**
	 * 检查部分字段逻辑
	 * @param slIotdBean
	 */
	public void checkEntry(SlIotdBean slIotdBean) {
		Brps brps = brpsService.selectByPrimaryKey(slIotdBean.getInthBean().getBr());
		Ccyp ccyp = ccypService.selectByPrimaryKey(slIotdBean.getInthBean().getBr(), slIotdBean.getCcy(), slIotdBean.getCtrCcy()); 
		Date onDate = null;
		int count = 0;
		
		if(brpsService.isBatchRunning(slIotdBean.getInthBean().getBr())) {
			throw new RuntimeException("系统跑批中请稍后操作!");
		}
		if(slIotdBean.getExpDate().compareTo(brps.getBranprcdate()) <= 0 ) {
			JY.raise("交易到期日必须大于系统批量日期,OPICS批量日期："+DateUtil.format(brps.getBranprcdate()));
		}
		if(slIotdBean.getSettleDte().compareTo(slIotdBean.getExpDate()) < 0 ) {
			JY.raise("结算日期必须大于等于到期日期");
		}
		if(slIotdBean.getDealVDate().compareTo(slIotdBean.getDealDate()) < 0 ) {
			JY.raise("起息日期必须大于等于交易日期");
		}
		if(slIotdBean.getDealDate().compareTo(brps.getBranprcdate()) > 0 ) {
			JY.raise("交易日期必须小于等于系统批量日期");
		}
		if(null == ccyp) {
			JY.raise("货币对信息不存在");
		}
		onDate = FinancialDateUtils.calculateSpotDate(slIotdBean.getDealDate(), hldyService.selectHldyListStr(OpicsConstant.BRRD.LOCAL_CCY), 2, 0);
		if(slIotdBean.getExpDate().compareTo(onDate) <= 0) {
			JY.raise("交易期限必须大于1D");
		}
		//检查CCY币种是否有曲线数据
		count = ddftService.selectCountByBpdate(slIotdBean.getInthBean().getBr(),slIotdBean.getDealDate(),slIotdBean.getCcy(),slIotdBean.getCtrCcy());
		if(count < 2) {
			JY.raise("未匹配到货币曲线对应的贴现因子数据");
		}
	}
	
	public void checkExecute(SlIotdBean slIotdBean) {
		Otdt otdt = otdtService.selectByPrimaryKey(slIotdBean.getInthBean().getBr(), slIotdBean.getInthBean().getDealno(), "000");
		
		if(brpsService.isBatchRunning(slIotdBean.getInthBean().getBr())) {
			throw new RuntimeException("系统跑批中请稍后操作!");
		}
		if(null == otdt) {
			JY.raise("未找到原交易");
		}
		if(!"1".equalsIgnoreCase(StringUtils.trim(otdt.getVerind()))) {
			JY.raise("原交易未复核");
		}
		if(otdt.getRevdate() != null) {
			JY.raise("原交易已冲销");
		}
		if("1".equalsIgnoreCase(StringUtils.trim(otdt.getExerind())) || "Y".equalsIgnoreCase(otdt.getExerind())) {
			JY.raise("原交易已行权");
		}
		if("1".equalsIgnoreCase(StringUtils.trim(otdt.getTrigind())) || "3".equalsIgnoreCase(StringUtils.trim(otdt.getTrigind()))) {
			JY.raise("原交易已触碰");
		}
		if(slIotdBean.getExerdate().compareTo(otdt.getExpdate()) != 0 ) {
			JY.raise("行权日期必须等于期权到期日");
		}
	}
}
