package com.singlee.financial.opics;

import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCoreAcctBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.SlCoreAcctMapper;
import com.singlee.financial.page.PageInfo;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlCoreAcctServer implements ISlCoreAcctServer {
	
	@Autowired
	SlCoreAcctMapper coreAcctMapper;

	@Override
	public PageInfo<SlCoreAcctBean> searchPageCoreAcct(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlCoreAcctBean> page = coreAcctMapper.searchPageCoreAcct(map,rb);
		return new PageInfo<SlCoreAcctBean>(page);
	}

	@Override
	public void coreAcctAdd(Map<String, Object> map) {
		coreAcctMapper.coreAcctAdd(map);
	}

	@Override
	public void updateById(SlCoreAcctBean entity) {
		coreAcctMapper.updateById(entity);
	}

	@Override
	public void deleteCoreAcct(Map<String, String> map) {
		coreAcctMapper.deleteCoreAcct(map);
	}
}