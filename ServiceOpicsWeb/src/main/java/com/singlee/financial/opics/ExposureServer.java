package com.singlee.financial.opics;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlBondCashBean;
import com.singlee.financial.bean.SlExposureBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.ExposureMapper;
import com.singlee.financial.page.PageInfo;

/**
 * 敞口
 * 
 * @author zhengfl
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ExposureServer implements IExposureServer {
	@Autowired
	ExposureMapper exposureMapper;

	@Override
	public PageInfo<SlExposureBean> getFxExposure(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxExposure(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}
	@Override
	public PageInfo<SlExposureBean> getFxExposureHistory(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxExposureHistory(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}

	@Override
	public PageInfo<SlExposureBean> getFxSettleExposure(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxSettleExposure(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}
	@Override
	public PageInfo<SlExposureBean> getFxSettleExposureHistory(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxSettleExposureHistory(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}

	@Override
	public PageInfo<SlBondCashBean> searchBondCashCount(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlBondCashBean> page = exposureMapper.searchBondCashCount(map, rb);
		return new PageInfo<SlBondCashBean>(page);
	}

	@Override
	public PageInfo<SlExposureBean> getFxForwardExposure(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxForwardExposure(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}

	@Override
	public PageInfo<SlExposureBean> getFxForwardSettleExposure(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxForwardSettleExposure(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}

	@Override
	public PageInfo<SlExposureBean> getFxSwapSettleExposure(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxSwapSettleExposure(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}

	@Override
	public PageInfo<SlExposureBean> getFxSwapExposure(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlExposureBean> page = exposureMapper.getFxSwapExposure(map, rb);
		return new PageInfo<SlExposureBean>(page);
	}

}
