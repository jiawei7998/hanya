/**
 * Project Name:ServiceOpicsWeb
 * File Name:FeesServer.java
 * Package Name:com.singlee.financial.opics
 * Date:2018-10-17下午02:53:50
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.opics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlIfeeBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.FeesTotalMapper;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FeesServer implements IFeesServer{
	private static Logger logger = LoggerFactory.getLogger(FeesServer.class);
	@Autowired
	FeesTotalMapper feesMapper;
	@Autowired
	CommonMapper commonMapper;

	@Override
	public SlOutBean fees(SlIfeeBean ifeeBean)
			throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IFEE表
		 */
		try {
			feesMapper.addFees(ifeeBean);
		} catch (Exception e) {
			logger.error("费用导入接口>>>>>>>>>>>>>>>" + SlErrors.FEES_ADD_IFEE, e);
			outBean.setRetCode(SlErrors.FEES_ADD_IFEE.getErrCode());
			outBean.setRetMsg(SlErrors.FEES_ADD_IFEE.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(ifeeBean.getInthBean());
		} catch (Exception e) {
			logger.error("费用导入接口>>>>>>>>>>>>>>>" + SlErrors.FEES_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FEES_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FEES_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}
	
	

}

