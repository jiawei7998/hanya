package com.singlee.financial.opics;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlBondTradeBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.BondTradeMapper;
import com.singlee.financial.page.PageInfo;

/**
 * 债券
 * 
 * @author wangzt
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BondTradeServer implements IBondTradeServer {
	
	@Autowired
	BondTradeMapper bondTradeMapper;

	
	@Override
	public PageInfo<SlBondTradeBean> searchBondTradeCount(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlBondTradeBean> page = bondTradeMapper.searchBondTradeCount(map, rb);
		return new PageInfo<SlBondTradeBean>(page);
	}

}
