package com.singlee.financial.opics;

import com.singlee.financial.bean.SlHldyInfo;
import com.singlee.financial.common.spring.ExternalIntfc;
import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.RemoteAccessFile;
import com.singlee.financial.wks.bean.opics.Hldy;
import com.singlee.financial.wks.mapper.opics.HldyMapper;
import com.singlee.financial.wks.service.opics.CcodService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class HldyDbAndFileServer implements IHldyDbAndFileServer {
    @Autowired
    ExternalIntfc eic;
    @Autowired
    HldyMapper hldyMapper;
    @Autowired
    CcodService ccodService;
    @Resource
    BatchDao batchDao;

    private static Logger logger = Logger.getLogger(RemoteAccessFile.class);

    @Override
    public void addHldyProcess(List<SlHldyInfo> slHldyInfoList) {
        //循环处理数据
        for (SlHldyInfo hldyInfo : slHldyInfoList) {
            String ccy = hldyInfo.getCcy();
            List<Date> hildy = hldyInfo.getHilDayLst();
            List<Date> adj = hldyInfo.getAdjDayLst();
            //获取50年的周六周末
            Calendar cal = Calendar.getInstance();
            cal.setTime(hildy.get(0));
            int year = cal.get(Calendar.YEAR);
            List<Date> weekday = DateUtil.getWeekDay(year, year + 50, ccy);// 默认取传入年数据
            //开始处理日期[hldy表中只存假日，如果本来就是假日，则需要剔除为工作日]
            HashSet<Date> hildySet = new HashSet<>();
            //复制周末假日
            hildySet.addAll(weekday);
            //复制特殊假日
            hildySet.addAll(hildy);
            //移除调休日
            hildySet.removeAll(adj);
            //封装假日
            List<Date> hildyDt = new ArrayList<>(hildySet);
            //排序
            Collections.sort(hildyDt);
            //声明对象
            List<Hldy> hildyLst = new ArrayList<Hldy>();
            //循环封装对象
            for (Date date : hildyDt) {
                Hldy hldy = new Hldy();
                hldy.setCalendarid(ccy);// 设置币种
                hldy.setHolidate(date);// 设置假日
                hldy.setHolidaytype("1");// 设置标志
                hildyLst.add(hldy);
            }
            // 批量提交
            hldyMapper.deleteByCcy(ccy);// 删除币种所有假日
            if (hildySet.size() > 0) {
                batchDao.batch("com.singlee.financial.wks.mapper.opics.HldyMapper.insert", hildyLst, 500);
            }
            logger.info("开始批量处理" + ccy + "数据库，总计处理假日数:" + hildySet.size());
            // 开始处理文件
            logger.info("开始批量处理假日文件，开始处理假日文件，移除调休日······ =>" + adj);
            boolean retStatus = RemoteAccessFile.sshHolidayFile(ccy, hildyDt, weekday, year,
                    eic.getFileName(), eic.getUserName(), eic.getPassword(), eic.getShareIp(),
                    Integer.parseInt(eic.getSharePort()), eic.getShareDir());
            logger.info("处理假日文件，状态:" + retStatus + "文件，总计处理假日数：" + hildy.size());
        }
    }
}
