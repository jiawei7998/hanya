package com.singlee.financial.opics;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlIdriBean;
import com.singlee.financial.bean.SlIinsBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.SettleManagerMapper;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SettleManagerServer implements ISettleManagerServer {
	private static Logger logger = LoggerFactory.getLogger(SettleManagerServer.class);
	@Autowired
	SettleManagerMapper settleManagerMapper;

	@Autowired
	CommonMapper commonMapper;

	@Override
	public SlOutBean addSlIinsBean(SlIinsBean iinsBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入BRED表
		 */
		try {
			settleManagerMapper.addSlIinsBean(iinsBean);
		} catch (Exception e) {
			logger.error("清算信息导入接口>>>>>>>>>>>>>>>" + SlErrors.ADD_IINS, e);
			outBean.setRetCode(SlErrors.ADD_IINS.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_IINS.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		
			try {
				if("0".equals(iinsBean.getInthBean().getSeq())){
					commonMapper.addSlFundInth(iinsBean.getInthBean());
				}
			} catch (Exception e) {
				logger.error("清算信息导入接口>>>>>>>>>>>>>>>" + SlErrors.ADD_INTH, e);
				outBean.setRetCode(SlErrors.ADD_INTH.getErrCode());
				outBean.setRetMsg(SlErrors.ADD_INTH.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
			return outBean;
		

	}

	@Override
	public SlOutBean addSlIdriBean(SlIdriBean idriBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入idri表
		 */
		try {
			settleManagerMapper.addSlIdriBean(idriBean);
		} catch (Exception e) {
			logger.error("清算信息导入接口>>>>>>>>>>>>>>>" + SlErrors.ADD_IDRI, e);
			outBean.setRetCode(SlErrors.ADD_IDRI.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_IDRI.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			if("0".equals(idriBean.getInthBean().getSeq())){
				commonMapper.addSlFundInth(idriBean.getInthBean());
			}
		} catch (Exception e) {
			logger.error("清算信息导入接口>>>>>>>>>>>>>>>" + SlErrors.ADD_INTH, e);
			outBean.setRetCode(SlErrors.ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public int searchCspiById(Map<String, Object> arg0)
			throws RemoteConnectFailureException, Exception {
		return settleManagerMapper.searchCspiById(arg0);
	}

	@Override
	public int searchCsriById(Map<String, Object> arg0)
			throws RemoteConnectFailureException, Exception {
		return settleManagerMapper.searchCsriById(arg0);
	}

	@Override
	public int searchSdvpById(Map<String, Object> arg0)
			throws RemoteConnectFailureException, Exception {
		return settleManagerMapper.searchSdvpById(arg0);
	}

}
