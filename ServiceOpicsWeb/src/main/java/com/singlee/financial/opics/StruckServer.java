package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlDealModule;
import com.singlee.financial.bean.SlDepositsAndLoansBean;
import com.singlee.financial.bean.SlIfeeBean;
import com.singlee.financial.bean.SlIotdBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.FeesTotalMapper;
import com.singlee.financial.mapper.MmMapper;
import com.singlee.financial.mapper.OtcMapper;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;

/***
 * 结构衍生
 * v1.0国民银行
 * @author xuqq
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class StruckServer implements IStruckServer{

	private static Logger logger = LoggerFactory.getLogger(OtcServer.class);
	@Autowired
	OtcMapper otcMapper;
	@Autowired
	MmMapper mmMapper;
	@Autowired
	FeesTotalMapper feesMapper;
	@Autowired
	CommonMapper commonMapper;
	
	@Override
	public SlOutBean verifiedStruck(Map<String,List<Object>> map) throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public SlOutBean struck(Map<String,List<Object>> map) throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		
//		为了事务回退  throw new RuntimeException();
		
//		插入IOTD表start
		/**
		 * 插入IOTD表
		 */
		try {
			List<Object> iotdList = map.get(SlDealModule.OTC.DETAIL);
			for(int i=0;i<iotdList.size();i++) {
				otcMapper.addOtc((SlIotdBean)iotdList.get(i));
			}
		} catch (Exception e) {
			logger.error("期权导入接口>>>>>>>>>>>>>>>" + SlErrors.OTC_ADD_IOTD, e);
			throw new RuntimeException();
		}
		/**
		 * 插入IOTD对应的INTH表
		 */
		try {
			List<Object> iotdList = map.get(SlDealModule.OTC.DETAIL);
			for(int i=0;i<iotdList.size();i++) {
				SlIotdBean slIotdBean=(SlIotdBean)iotdList.get(i);
				commonMapper.addSlFundInth(slIotdBean.getInthBean());
			}
		} catch (Exception e) {
			logger.error("期权导入接口>>>>>>>>>>>>>>>" + SlErrors.OTC_ADD_INTH, e);
			throw new RuntimeException();
		}
//		插入IOTD表end
		
		
//		插入IDLD表start
		/**
		 * 插入IDLD表
		 */
		try {
			List<Object> idldList = map.get(SlDealModule.DL.DETAIL);
			for(int i=0;i<idldList.size();i++) {
				mmMapper.addMm((SlDepositsAndLoansBean)idldList.get(i));
			}
		} catch (Exception e) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_ADD_IDLD, e);
			throw new RuntimeException();
		}
		/**
		 * 插入INTH表
		 */
		try {
			List<Object> idldList = map.get(SlDealModule.DL.DETAIL);
			for(int i=0;i<idldList.size();i++) {
				SlDepositsAndLoansBean slDepositsAndLoansBean =(SlDepositsAndLoansBean)idldList.get(i);
				commonMapper.addSlFundInth(slDepositsAndLoansBean.getInthBean());
			}
		} catch (Exception e) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_ADD_INTH, e);
			throw new RuntimeException();
		}
//		插入IDLD表end
		
		
//		插入IFEE表start
		/**
		 * 插入IFEE表
		 */
		try {
			List<Object> ifeeList = map.get(SlDealModule.FEES.DETAIL);
			if(ifeeList!=null) {
				for(int i=0;i<ifeeList.size();i++) {
					feesMapper.addFees((SlIfeeBean)ifeeList.get(i));
				}
			}
		} catch (Exception e) {
			logger.error("费用导入接口>>>>>>>>>>>>>>>" + SlErrors.FEES_ADD_IFEE, e);
			throw new RuntimeException();
		}
		/**
		 * 插入INTH表
		 */
		try {
			List<Object> ifeeList = map.get(SlDealModule.FEES.DETAIL);
			if(ifeeList!=null) {
				for(int i=0;i<ifeeList.size();i++) {
					SlIfeeBean slIfeeBean = (SlIfeeBean)ifeeList.get(i);
					commonMapper.addSlFundInth(slIfeeBean.getInthBean());
				}
			}
			
		} catch (Exception e) {
			logger.error("费用导入接口>>>>>>>>>>>>>>>" + SlErrors.FEES_ADD_INTH, e);
			throw new RuntimeException();
		}
//		插入IFEE表end
		
		return outBean;
	}
	@Override
	public SlOutBean struckRev(Map<String,List<Object>> map) throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public SlOutBean verifiedStruckRev(Map<String,List<Object>> map) throws RemoteConnectFailureException, Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
}
