package com.singlee.financial.opics;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlRepoCrBean;
import com.singlee.financial.bean.SlRepoOrBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.RepoMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RepoServer implements IRepoServer {

	private static Logger logger = LoggerFactory.getLogger(RepoServer.class);

	@Autowired
	RepoMapper repoMapper;
	@Autowired
	CommonMapper commonMapper;

	@Override
	public SlOutBean ircaProc(Map<String, String> arg0) {
		System.out.println(arg0);
		SlOutBean SlOutBean = new SlOutBean();
		arg0.put("RETCODE", "");
		arg0.put("RETMSG", "");
		arg0.put("br", "01");
		arg0.put("SEQ", "1");
		arg0.put("FLAG", "1");
		arg0.put("sttlTyp", "0");
		arg0.put("sttlTyp2", "0");
		arg0.put("MarketIndicator", "10");
		arg0.put("SIIND", "0");
		arg0.put("VERIND", "0");
		System.out.println(arg0.get("contractId") + "," + arg0.get("tradeDate") + "," + arg0.get("inputTime") + "," + arg0.get("myDir") + "," + arg0.get("reverseInst") + ","
				+ arg0.get("positiveTrader") + "," + arg0.get("firstSettlementAmount") + "," + arg0.get("firstDirtyPrice") + "," + arg0.get("firstSettlementDate") + ","
				+ arg0.get("secondSettlementDate") + "," + arg0.get("bondCode") + "," + arg0.get("repoRate") + "," + arg0.get("firstSettlementAmount") + "," + arg0.get("secondSettlementAmount") + ","
				+ arg0.get("FLAG") + "," + arg0.get("SEQ") + "," + arg0.get("sttlTyp") + "," + arg0.get("sttlTyp2") + "," + arg0.get("MarketIndicator") + "," + arg0.get("br") + ","
				+ arg0.get("invtype") + "," + arg0.get("product") + "," + arg0.get("prodType") + "," + arg0.get("cost") + "," + arg0.get("port") + "," + arg0.get("SIIND") + "," + arg0.get("VERIND"));
		try {
			repoMapper.ircaProc(arg0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(SlOutBean);
		SlOutBean.setRetCode(arg0.get("RETCODE").substring(0, 3));
		SlOutBean.setRetMsg(arg0.get("RETMSG"));
		System.out.println(SlOutBean.getRetCode() + SlOutBean.getRetMsg());
		return SlOutBean;
	}

	@Override
	public SlOutBean repoOr(SlRepoOrBean orBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IRCA表
		 */
		try {
			repoMapper.addRepoOr(orBean);
		} catch (Exception e) {
			logger.error("买断式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.OR_ADD_IRCA, e);
			outBean.setRetCode(SlErrors.OR_ADD_IRCA.getErrCode());
			outBean.setRetMsg(SlErrors.OR_ADD_IRCA.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(orBean.getInthBean());
		} catch (Exception e) {
			logger.error("买断式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.OR_ADD_INTH, e);
			outBean.setRetCode(SlErrors.OR_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.OR_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean repoOrRev(SlRepoOrBean orBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		try {
			// 冲销之前需要验证,看INTH表里是否有已冲销过的数据
			int c = commonMapper.getSlFundInthRev(orBean.getInthBean());
			if (c >= 1) {
				outBean.setRetCode(SlErrors.ORREV_ADD_INTH_EXIST.getErrCode());
				outBean.setRetMsg(SlErrors.ORREV_ADD_INTH_EXIST.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
		} catch (Exception e) {
			logger.error("回购冲销接口>>>>>>>>>>>>>>>" + SlErrors.ORREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.ORREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.ORREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入IRCA表
		 */
		try {
			repoMapper.addRepoOrRev(orBean);
		} catch (Exception e) {
			logger.error("买断式回购冲销接口>>>>>>>>>>>>>>>" + SlErrors.ORREV_ADD_IRCA, e);
			outBean.setRetCode(SlErrors.ORREV_ADD_IRCA.getErrCode());
			outBean.setRetMsg(SlErrors.ORREV_ADD_IRCA.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInthRev(orBean.getInthBean());
		} catch (Exception e) {
			logger.error("买断式回购冲销接口>>>>>>>>>>>>>>>" + SlErrors.ORREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.ORREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.ORREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean verifiedRepoCr(SlRepoCrBean crBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == crBean) {
			logger.error("质押式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.CR_IRCA_NOT_EXIST);
			outBean.setRetCode(SlErrors.CR_IRCA_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断execId是否存在 */
		if (!StringUtils.isNotEmpty(crBean.getExecId())) {
			outBean.setRetCode(SlErrors.CR_IRCA_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断IRCA表中是否已经存在
		if (repoMapper.getSlFundCrAndOr(crBean.getExecId()) >= 1) {
			logger.error("质押式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.CR_IRCA_EXIST);
			outBean.setRetCode(SlErrors.CR_IRCA_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空
		if (!StringUtils.isNotEmpty(crBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.CR_IRCA_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(crBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.CR_IRCA_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(crBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("质押式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.CR_IRCA_EXIST);
			outBean.setRetCode(SlErrors.CR_IRCA_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId是否为空
		if (!StringUtils.isNotEmpty(crBean.getContraPatryInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.CR_IRCA_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId在CUST表中是否存在
		if (commonMapper.getCno(crBean.getContraPatryInstitutionInfo().getTradeId()) <= 0) {
			logger.error("质押式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.CR_IRCA_EXIST);
			outBean.setRetCode(SlErrors.CR_IRCA_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.CR_IRCA_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.CR_IRCA_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.CR_IRCA_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.CR_IRCA_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.CR_IRCA_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.CR_IRCA_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.CR_IRCA_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.CR_IRCA_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.CR_IRCA_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.CR_IRCA_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断custrefno（放inth的fedealno）是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getCustrefno())) {
			outBean.setRetCode(SlErrors.CR_IRCA_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算路径是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getCcysmeans())) {
			outBean.setRetCode(SlErrors.CR_IRCA_CCYSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CCYSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算账户是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getCcysacct())) {
			outBean.setRetCode(SlErrors.CR_IRCA_CCYSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CCYSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算路径是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getCtrsmeans())) {
			outBean.setRetCode(SlErrors.CR_IRCA_CTRSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CTRSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算账户是否为空
		if (!StringUtils.isNotEmpty(crBean.getExternalBean().getCtrsacct())) {
			outBean.setRetCode(SlErrors.CR_IRCA_CTRSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CTRSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手清算账户与NOST表中是否相同
		if (repoMapper.isCustAcctSameInNostCr(crBean) <= 0) {
			logger.error("质押式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.CR_IRCA_CTRSACCT_NOT_SAME_EXIST);
			outBean.setRetCode(SlErrors.CR_IRCA_CTRSACCT_NOT_SAME_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_CTRSACCT_NOT_SAME_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断买卖方向是否为空
		if (crBean.getPs() == null) {
			outBean.setRetCode(SlErrors.CR_IRCA_PS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CR_IRCA_PS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	@Override
	public SlOutBean repoCr(SlRepoCrBean crBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IRCA表
		 */
		try {
			repoMapper.addRepoCr(crBean);
		} catch (Exception e) {
			logger.error("质押式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.CR_ADD_IRCA, e);
			outBean.setRetCode(SlErrors.CR_ADD_IRCA.getErrCode());
			outBean.setRetMsg(SlErrors.CR_ADD_IRCA.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(crBean.getInthBean());
		} catch (Exception e) {
			logger.error("质押式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.CR_ADD_INTH, e);
			outBean.setRetCode(SlErrors.CR_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.CR_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean repoCrRev(SlRepoCrBean crBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IRCA表
		 */
		try {
			repoMapper.addRepoCrRev(crBean);
		} catch (Exception e) {
			logger.error("质押式回购冲销接口>>>>>>>>>>>>>>>" + SlErrors.CRREV_ADD_IRCA, e);
			outBean.setRetCode(SlErrors.CRREV_ADD_IRCA.getErrCode());
			outBean.setRetMsg(SlErrors.CRREV_ADD_IRCA.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(crBean.getInthBean());
		} catch (Exception e) {
			logger.error("质押式回购冲销接口>>>>>>>>>>>>>>>" + SlErrors.CR_ADD_INTH, e);
			outBean.setRetCode(SlErrors.CR_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.CR_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean verifiedRepoOr(SlRepoOrBean orBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == orBean) {
			logger.error("买断式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.OR_IRCA_NOT_EXIST);
			outBean.setRetCode(SlErrors.OR_IRCA_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断execId是否存在 */
		if (!StringUtils.isNotEmpty(orBean.getExecId())) {
			outBean.setRetCode(SlErrors.OR_IRCA_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断IRCA表中是否已经存在
		if (repoMapper.getSlFundCrAndOr(orBean.getExecId()) >= 1) {
			logger.error("买断式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.OR_IRCA_EXIST);
			outBean.setRetCode(SlErrors.OR_IRCA_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空
		if (!StringUtils.isNotEmpty(orBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.OR_IRCA_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(orBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.OR_IRCA_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(orBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("买断式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.OR_IRCA_EXIST);
			outBean.setRetCode(SlErrors.OR_IRCA_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId是否为空
		if (!StringUtils.isNotEmpty(orBean.getContraPatryInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.OR_IRCA_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId在CUST表中是否存在
		if (commonMapper.getCno(orBean.getContraPatryInstitutionInfo().getTradeId()) <= 0) {
			logger.error("买断式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.OR_IRCA_EXIST);
			outBean.setRetCode(SlErrors.OR_IRCA_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.OR_IRCA_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.OR_IRCA_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.OR_IRCA_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.OR_IRCA_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.OR_IRCA_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.OR_IRCA_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.OR_IRCA_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.OR_IRCA_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.OR_IRCA_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.OR_IRCA_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断custrefno（放inth的fedealno）是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getCustrefno())) {
			outBean.setRetCode(SlErrors.OR_IRCA_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算路径是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getCcysmeans())) {
			outBean.setRetCode(SlErrors.OR_IRCA_CCYSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CCYSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算账户是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getCcysacct())) {
			outBean.setRetCode(SlErrors.OR_IRCA_CCYSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CCYSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算路径是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getCtrsmeans())) {
			outBean.setRetCode(SlErrors.OR_IRCA_CTRSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CTRSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算账户是否为空
		if (!StringUtils.isNotEmpty(orBean.getExternalBean().getCtrsacct())) {
			outBean.setRetCode(SlErrors.OR_IRCA_CTRSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CTRSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手清算账户与NOST表中是否相同
		if (repoMapper.isCustAcctSameInNostOr(orBean) <= 0) {
			logger.error("买断式回购导入接口>>>>>>>>>>>>>>>" + SlErrors.OR_IRCA_CTRSACCT_NOT_SAME_EXIST);
			outBean.setRetCode(SlErrors.OR_IRCA_CTRSACCT_NOT_SAME_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_CTRSACCT_NOT_SAME_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断买卖方向是否为空
		if (orBean.getPs() == null) {
			outBean.setRetCode(SlErrors.OR_IRCA_PS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.OR_IRCA_PS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	/**
	 * 校验回购类冲销
	 * 
	 * @param orBean
	 * @return
	 */
	@Override
	public SlOutBean verifiedRepoOrRev(SlRepoOrBean orBean) {
		// TODO Auto-generated method stub
		return null;
	}

}
