package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlFifoBean;
import com.singlee.financial.bean.SlSpshViewBean;
import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.mapper.FifoMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FifoServer implements IFifoServer {

	@Autowired
	FifoMapper fifoMapper;
	@Resource
	BatchDao batchDao;

	@Override
	public void deleteFifoByBr(String br) {
		fifoMapper.deleteFifoByBr(br);
	}

	@Override
	public List<SlSpshViewBean> selectSpshViewListGroup(String br) {
		return fifoMapper.selectSpshViewListGroup(br);
	}

	@Override
	public List<SlSpshViewBean> selectSpshViewListP(Map<String, Object> map) {
		return fifoMapper.selectSpshViewListP(map);
	}

	@Override
	public SlSpshViewBean selectSpshViewS(Map<String, Object> map) {
		return fifoMapper.selectSpshViewS(map);
	}

	@Override
	public void bathInsertFifo(List<SlFifoBean> fbList) {
		batchDao.batch("com.singlee.financial.mapper.FifoMapper.insertFifo", fbList);
	}

    @Override
    public List<SlFifoBean> getAllList(Map<String, Object> map) {
        List<SlFifoBean> list=   fifoMapper.getListAll(map);
        return list;
    }

}
