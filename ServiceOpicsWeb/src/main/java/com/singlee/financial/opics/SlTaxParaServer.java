package com.singlee.financial.opics;

import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlTaxParaBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.SlTaxParaMapper;
import com.singlee.financial.page.PageInfo;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlTaxParaServer implements ISlTaxParaServer {
	
	@Autowired
	SlTaxParaMapper taxParaMapper;

	@Override
	public SlOutBean addSlTaxPara(SlTaxParaBean taxParaBean) {
		taxParaMapper.insert(taxParaBean);
		return null;
	}

	@Override
	public SlOutBean updateSlTaxPara(SlTaxParaBean taxParaBean) {
		taxParaMapper.updateById(taxParaBean);
		return null;
	}

	@Override
	public SlOutBean deleteSlTaxPara(SlTaxParaBean taxParaBean) {
		taxParaMapper.deleteSlTaxPara(taxParaBean);
		return null;
	}

	@Override
	public SlTaxParaBean getSlTaxPara(Map<String, String> map) {
		return taxParaMapper.getSlTaxPara(map);
	}

	@Override
	public PageInfo<SlTaxParaBean> searchPageTaxPara(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlTaxParaBean> page =taxParaMapper.searchPageTaxPara(map, rb);
		return new PageInfo<SlTaxParaBean>(page);
	}
}