package com.singlee.financial.opics;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.*;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.SecurMapper;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SecurServer implements ISecurServer {
	private static Logger logger = LoggerFactory.getLogger(SecurServer.class);
	@Autowired
	SecurMapper securMapper;
	@Autowired
	CommonMapper commonMapper;

	@Override
	public SlOutBean bredProc(Map<String, String> arg0) {
		System.out.println(arg0);
		SlOutBean SlOutBean = new SlOutBean();
		arg0.put("RETCODE", "");
		arg0.put("RETMSG", "");
		arg0.put("br", "01");
		System.out.println(arg0.get("contractId") + "," + arg0.get("tradeDate") + "," + arg0.get("inputTime") + "," + arg0.get("myDir") + "," + arg0.get("cno") + "," + arg0.get("buyTrader") + ","
				+ arg0.get("settlementAmount") + "," + arg0.get("dirtyPrice") + "," + arg0.get("settlementDate") + "," + arg0.get("bondCode") + "," + arg0.get("tradeAmount") + ","
				+ arg0.get("settlementMethod") + "," + arg0.get("br") + "," + arg0.get("invtype") + "," + arg0.get("product") + "," + arg0.get("prodType") + "," + arg0.get("cost") + ","
				+ arg0.get("port"));
		try {
			securMapper.bredProc(arg0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SlOutBean.setRetCode(arg0.get("RETCODE").substring(0, 3));
		SlOutBean.setRetMsg(arg0.get("RETMSG"));
		System.out.println(SlOutBean.getRetCode() + SlOutBean.getRetMsg());
		return SlOutBean;
	}

	@Override
	public SlOutBean isldProc(Map<String, String> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlOutBean fi(SlFixedIncomeBean fiBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入BRED表
		 */
		try {
			securMapper.addFi(fiBean);
		} catch (Exception e) {
			logger.error("债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.FI_ADD_BRED, e);
			outBean.setRetCode(SlErrors.FI_ADD_BRED.getErrCode());
			outBean.setRetMsg(SlErrors.FI_ADD_BRED.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(fiBean.getInthBean());
		} catch (Exception e) {
			logger.error("债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.FI_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FI_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FI_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean fiRev(SlFixedIncomeBean fiBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		try {
			// 冲销之前需要验证,看INTH表里是否有已冲销过的数据
			int c = commonMapper.getSlFundInthRev(fiBean.getInthBean());
			if (c >= 1) {
				outBean.setRetCode(SlErrors.FIREV_ADD_INTH_EXIST.getErrCode());
				outBean.setRetMsg(SlErrors.FIREV_ADD_INTH_EXIST.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
		} catch (Exception e) {
			logger.error("现券买卖冲销接口>>>>>>>>>>>>>>>" + SlErrors.FIREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FIREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FIREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入BRED表
		 */
		try {
			securMapper.addFiRev(fiBean);
		} catch (Exception e) {
			logger.error("债券买卖冲销接口>>>>>>>>>>>>>>>" + SlErrors.FIREV_ADD_BRED, e);
			outBean.setRetCode(SlErrors.FIREV_ADD_BRED.getErrCode());
			outBean.setRetMsg(SlErrors.FIREV_ADD_BRED.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInthRev(fiBean.getInthBean());
		} catch (Exception e) {
			logger.error("债券买卖冲销接口>>>>>>>>>>>>>>>" + SlErrors.FIREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FIREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FIREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean verifiedFi(SlFixedIncomeBean fiBean) {

		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == fiBean) {
			logger.error("债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.FI_BRED_NOT_EXIST);
			outBean.setRetCode(SlErrors.FI_BRED_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断execId是否存在 */
		if (!StringUtils.isNotEmpty(fiBean.getExecId())) {
			outBean.setRetCode(SlErrors.FI_BRED_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断BRED表中是否已经存在
		if (securMapper.getSlFundFi(fiBean.getExecId()) >= 1) {
			logger.error("债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.FI_BRED_EXIST);
			outBean.setRetCode(SlErrors.FI_BRED_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空
		if (!StringUtils.isNotEmpty(fiBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.FI_BRED_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(fiBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.FI_BRED_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(fiBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.FI_BRED_EXIST);
			outBean.setRetCode(SlErrors.FI_BRED_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId是否为空
		if (!StringUtils.isNotEmpty(fiBean.getContraPatryInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.FI_BRED_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId在CUST表中是否存在
		if (commonMapper.getCno(fiBean.getContraPatryInstitutionInfo().getTradeId()) <= 0) {
			logger.error("债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.FI_BRED_EXIST);
			outBean.setRetCode(SlErrors.FI_BRED_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.FI_BRED_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.FI_BRED_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.FI_BRED_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.FI_BRED_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.FI_BRED_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.FI_BRED_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.FI_BRED_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.FI_BRED_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.FI_BRED_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.FI_BRED_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断custrefno（放inth的fedealno）是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getCustrefno())) {
			outBean.setRetCode(SlErrors.FI_BRED_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算路径是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getCcysmeans())) {
			outBean.setRetCode(SlErrors.FI_BRED_CCYSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CCYSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算账户是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getCcysacct())) {
			outBean.setRetCode(SlErrors.FI_BRED_CCYSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CCYSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算路径是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getCtrsmeans())) {
			outBean.setRetCode(SlErrors.FI_BRED_CTRSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CTRSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算账户是否为空
		if (!StringUtils.isNotEmpty(fiBean.getExternalBean().getCtrsacct())) {
			outBean.setRetCode(SlErrors.FI_BRED_CTRSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CTRSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手清算账户与NOST表中是否相同
		if (securMapper.isCustAcctSameInNost(fiBean) <= 0) {
			logger.error("债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.FI_BRED_CTRSACCT_NOT_SAME_EXIST);
			outBean.setRetCode(SlErrors.FI_BRED_CTRSACCT_NOT_SAME_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_CTRSACCT_NOT_SAME_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断买卖方向是否为空
		if (fiBean.getPs() == null) {
			outBean.setRetCode(SlErrors.FI_BRED_PS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FI_BRED_PS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	@Override
	public SlOutBean seclend(SlSecurityLendingBean securityLendingBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入ISLD表
		 */
		try {
			securMapper.addSeclend(securityLendingBean);
		} catch (Exception e) {
			logger.error("债券借贷导入接口>>>>>>>>>>>>>>>" + SlErrors.SECLEND_ADD_BRED, e);
			outBean.setRetCode(SlErrors.SECLEND_ADD_BRED.getErrCode());
			outBean.setRetMsg(SlErrors.SECLEND_ADD_BRED.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(securityLendingBean.getInthBean());
		} catch (Exception e) {
			logger.error("债券借贷导入接口>>>>>>>>>>>>>>>" + SlErrors.SECLEND_ADD_INTH, e);
			outBean.setRetCode(SlErrors.SECLEND_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.SECLEND_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	/***
	 * 债券借贷 冲销
	 */
	@Override
	public SlOutBean seclendRev(SlSecurityLendingBean securityLendingBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		try {
			// 冲销之前需要验证,看INTH表里是否有已冲销过的数据
			int c = commonMapper.getSlFundInthRev(securityLendingBean.getInthBean());
			if (c >= 1) {
				outBean.setRetCode(SlErrors.SECLENDREV_ADD_INTH_EXIST.getErrCode());
				outBean.setRetMsg(SlErrors.SECLENDREV_ADD_INTH_EXIST.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
		} catch (Exception e) {
			logger.error("债券借贷冲销接口>>>>>>>>>>>>>>>" + SlErrors.SECLENDREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.SECLENDREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.SECLENDREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入ISLD表
		 */
		try {
			securMapper.addSeclendRev(securityLendingBean);
		} catch (Exception e) {
			logger.error("债券借贷冲销接口>>>>>>>>>>>>>>>" + SlErrors.SECLENDREV_ADD_BRED, e);
			outBean.setRetCode(SlErrors.SECLENDREV_ADD_BRED.getErrCode());
			outBean.setRetMsg(SlErrors.SECLENDREV_ADD_BRED.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInthRev(securityLendingBean.getInthBean());
		} catch (Exception e) {
			logger.error("债券借贷冲销接口>>>>>>>>>>>>>>>" + SlErrors.SECLENDREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.SECLENDREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.SECLENDREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	/**
	 * 校验OPICS现券买卖冲销
	 * 
	 */
	@Override
	public SlOutBean verifiedFiRev(SlFixedIncomeBean fixedIncomeBean) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 校验OPICS债券借贷冲销
	 * 
	 * @param securityLendingBean
	 * @return
	 */
	@Override
	public SlOutBean verifiedSeclendRev(SlSecurityLendingBean securityLendingBean) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public SlOutBean imbd(SlImbdBean imbdBean)
			throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入BRED表
		 */
		try {
			securMapper.addImbd(imbdBean);
		} catch (Exception e) {
			logger.error("MBS债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.IMBD_ADD_BRED, e);
			outBean.setRetCode(SlErrors.IMBD_ADD_BRED.getErrCode());
			outBean.setRetMsg(SlErrors.IMBD_ADD_BRED.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(imbdBean.getInthBean());
		} catch (Exception e) {
			logger.error("MBS债券买卖导入接口>>>>>>>>>>>>>>>" + SlErrors.IMBD_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IMBD_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IMBD_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean rdfe() throws Exception {
		return null;
	}


	@Override
	public PageInfo<SlSecmRedemption> getRedemption(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlSecmRedemption> result = securMapper.getRedemption(map, rb);
		return new PageInfo<SlSecmRedemption>(result);
	}

	@Override
	public List<SlSecmRedemption> getRedemptdWithList(Map<String, Object> map){
		return securMapper.getRedemptdWithList(map);
	}

	@Override
	public PageInfo<SlSecmRevaluation> getPageSecmRevaluation(
			Map<String, Object> map) throws RemoteConnectFailureException,
			Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlSecmRevaluation> result = securMapper.getPageSecmRevaluation(map, rb);
		return new PageInfo<SlSecmRevaluation>(result);
	}

	@Override
	public SlSecmBean getSlSecmOne(String secid)
			throws RemoteConnectFailureException, Exception {
		SlSecmBean data=securMapper.searchSlSecmOne(secid);
		return data;
	}
}
