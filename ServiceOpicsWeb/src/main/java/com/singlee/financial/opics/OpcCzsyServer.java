package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.singlee.financial.bean.SlOpcCzsyBean;
import com.singlee.financial.mapper.OpcCzsyMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OpcCzsyServer implements IOpcCzsyServer {

	@Autowired
	OpcCzsyMapper opcCzsyMapper;

	@Override
	public List<SlOpcCzsyBean> selectOpcCzsy(Map<String, Object> map) {
		return opcCzsyMapper.selectOpcCzsy(map);
	}

	@Override
	public List<SlOpcCzsyBean> selectGlno(Map<String, Object> map) {
		return opcCzsyMapper.selectGlno(map);
	}

}
