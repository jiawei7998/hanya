package com.singlee.financial.opics;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.bean.SlFxSptFwdBean;
import com.singlee.financial.bean.SlFxSwapBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IFxdServer;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.FxdMapper;

import org.apache.commons.lang3.StringUtils;

import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FxdServer implements IFxdServer {

	private static Logger logger = LoggerFactory.getLogger(FxdServer.class);
	
	@Autowired
	FxdMapper fxdMapper;
	
	@Autowired
	CommonMapper commonMapper;

	@Override
	public SlOutBean fxSptAndFwdProc(Map<String, String> arg0) {
		System.out.println(arg0);
		SlOutBean SlOutBean = new SlOutBean();
		arg0.put("RETCODE", "");
		arg0.put("RETMSG", "");
		arg0.put("br", "01");
		System.out.println(arg0);
		System.out.println(arg0.get("contractId") + "|" + arg0.get("dealer") + "|" + arg0.get("valueDate") + "|" + arg0.get("counterparty") + "|" + arg0.get("forDate")
				+ "|" + arg0.get("forTime") + "|" + arg0.get("buyDirection") + "|" + arg0.get("currencyPair") + "|" + arg0.get("buyAmount") + "|"
				+ arg0.get("currencyPair") + "|" + arg0.get("sellAmount") + "|" + arg0.get("price") + "|" + arg0.get("br") + "|" + arg0.get("product") + "|"
				+ arg0.get("prodType") + "|" + arg0.get("cost") + "|" + arg0.get("port") + "|" + arg0.get("verifySign") + "|" + arg0.get("grantSign"));
		try {
			fxdMapper.cfestFxProc(arg0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SlOutBean.setRetCode(arg0.get("RETCODE").substring(0, 3));
		SlOutBean.setRetMsg(arg0.get("RETMSG"));
		System.out.println(SlOutBean.getRetCode() + SlOutBean.getRetMsg());
		return SlOutBean;
	}

	@Override
	public SlOutBean fxSwapProc(Map<String, String> arg0) {
		System.out.println(arg0);
		SlOutBean SlOutBean = new SlOutBean();
		arg0.put("RETCODE", "");
		arg0.put("RETMSG", "");
		arg0.put("br", "01");
		try {
			fxdMapper.cfestSwapProc(arg0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SlOutBean.setRetCode(arg0.get("RETCODE").substring(0, 3));
		SlOutBean.setRetMsg(arg0.get("RETMSG"));
		System.out.println(SlOutBean.getRetCode() + SlOutBean.getRetMsg());
		return SlOutBean;
	}

	@Override
	public SlOutBean fxSptFwd(SlFxSptFwdBean fxSptFwdBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IFXD表
		 */
		try {
			fxdMapper.addFxSptFwd(fxSptFwdBean);
		} catch (Exception e) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_ADD_IFXD, e);
			outBean.setRetCode(SlErrors.FX_SPTFWD_ADD_IFXD.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_ADD_IFXD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(fxSptFwdBean.getInthBean());
		} catch (Exception e) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FX_SPTFWD_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	/***
	 * 外汇 即期 远期 冲销
	 */
	@Override
	public SlOutBean fxSptFwdRev(SlFxSptFwdBean fxSptFwdBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		try {
			// 冲销之前需要验证,看INTH表里是否有已冲销过的数据
			int c = commonMapper.getSlFundInthRev(fxSptFwdBean.getInthBean());
			if (c >= 1) {
				outBean.setRetCode(SlErrors.FXREV_ADD_INTH_EXIST.getErrCode());
				outBean.setRetMsg(SlErrors.FXREV_ADD_INTH_EXIST.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
		} catch (Exception e) {
			logger.error("外汇冲销接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWDREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FX_SPTFWDREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWDREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		/**
		 * 插入IFXD表
		 */
		try {
			fxdMapper.addFxSptFwdRev(fxSptFwdBean);
		} catch (Exception e) {

			logger.error("外汇即期、远期冲销接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWDREV_ADD_IFXD, e);
			outBean.setRetCode(SlErrors.FX_SPTFWDREV_ADD_IFXD.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWDREV_ADD_IFXD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInthRev(fxSptFwdBean.getInthBean());
		} catch (Exception e) {

			logger.error("外汇即期、远期冲销接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWDREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FX_SPTFWDREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWDREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean fxSwap(SlFxSwapBean fxSwapBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IFXD表
		 */
		try {
			fxdMapper.addFxSwap(fxSwapBean);
		} catch (Exception e) {
			logger.error("外汇掉期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_ADD_IFXD, e);
			outBean.setRetCode(SlErrors.FX_SWAP_ADD_IFXD.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_ADD_IFXD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(fxSwapBean.getInthBean());
		} catch (Exception e) {
			logger.error("外汇掉期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FX_SWAP_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean fxSwapRev(SlFxSwapBean fxSwapBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IFXD表
		 */
		try {
			fxdMapper.addFxSwapRev(fxSwapBean);
		} catch (Exception e) {
			logger.error("外汇掉期冲销接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAPREV_ADD_IFXD, e);
			outBean.setRetCode(SlErrors.FX_SWAPREV_ADD_IFXD.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAPREV_ADD_IFXD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(fxSwapBean.getInthBean());
		} catch (Exception e) {
			logger.error("外汇掉期冲销接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAPREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.FX_SWAPREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAPREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	/***
	 * @author lijing 20180612 校验数据
	 */
	@Override
	public SlOutBean verifiedfxSptFwd(SlFxSptFwdBean fxSptFwdBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == fxSptFwdBean) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_NOT_EXIST);
			outBean.setRetCode(SlErrors.FX_SPTFWD_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断execId是否存在 */
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExecId())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断IFXD表中是否已经存在
		if (fxdMapper.getSlFundIfxd(fxSptFwdBean.getExecId()) >= 1) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_EXIST);
			outBean.setRetCode(SlErrors.FX_SPTFWD_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空

		if (!StringUtils.isNotEmpty(fxSptFwdBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(fxSptFwdBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_EXIST);
			outBean.setRetCode(SlErrors.FX_SPTFWD_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getContraPatryInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手InstitutionId在CUST表中是否存在
		if (commonMapper.getCno(fxSptFwdBean.getContraPatryInstitutionInfo().getInstitutionId()) <= 0) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_EXIST);
			outBean.setRetCode(SlErrors.FX_SPTFWD_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断投资组合port在PORT表中是否存在（根据br、port查）
		Map<String, Object> portMap = new HashMap<String, Object>(2);
		portMap.put("br", fxSptFwdBean.getInthBean().getBr());
		portMap.put("port", fxSptFwdBean.getExternalBean().getPort());
		if (commonMapper.getPort(portMap) <= 0) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_EXIST);
			outBean.setRetCode(SlErrors.FX_SPTFWD_PORT_NOEXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_PORT_NOEXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心cost在COST表中是否存在
		if (commonMapper.getCost(fxSptFwdBean.getExternalBean().getCost()) <= 0) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SPTFWD_EXIST);
			outBean.setRetCode(SlErrors.FX_SPTFWD_COST_NOEXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_COST_NOEXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断custrefno（放inth的fedealno）是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getCustrefno())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算路径是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getCcysmeans())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CCYSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CCYSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算账户是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getCcysacct())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CCYSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CCYSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算路径是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getCtrsmeans())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CTRSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CTRSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算账户是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getExternalBean().getCtrsacct())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CTRSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CTRSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手清算账户与NOST表中是否相同
		/*
		 * if (fxdMapper.isCustAcctSameInNost(fxSptFwdBean) <= 0) {
		 * logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>"+
		 * SlErrors.FX_SPTFWD_CTRSACCT_NOT_SAME_EXIST);
		 * outBean.setRetCode(SlErrors
		 * .FX_SPTFWD_CTRSACCT_NOT_SAME_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors
		 * .FX_SPTFWD_CTRSACCT_NOT_SAME_EXIST.getErrMsg());
		 * outBean.setRetStatus(RetStatusEnum.F); return outBean; }
		 */
		// 判断本方币种是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getCurrencyInfo().getCcy())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CCY_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CCY_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断本方金额是否为空
		if (fxSptFwdBean.getCurrencyInfo().getAmt() == null) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_AMT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_AMT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断对手方币种是否为空
		if (!StringUtils.isNotEmpty(fxSptFwdBean.getCurrencyInfo().getContraCcy())) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_CTRCCY_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_CTRCCY_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手方金额是否为空
		/*
		 * if (fxSptFwdBean.getCurrencyInfo().getContraAmt() == null) {
		 * outBean.setRetCode(SlErrors.FX_SPTFWD_CTRAMT_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors.FX_SPTFWD_CTRAMT_EXIST.getErrMsg());
		 * outBean.setRetStatus(RetStatusEnum.F); return outBean; }
		 */

		// 判断买卖方向是否为空
		if (fxSptFwdBean.getPs() == null) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_PS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_PS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易日期是否<=系统日期
		Date dealDate = fxSptFwdBean.getDealDate();
		// 1.获取opics系统日期
		Date sysDate = commonMapper.getOpicsSysDate(fxSptFwdBean.getInthBean().getBr());

		int sub = DateUtil.daysBetween(dealDate, sysDate);
		if (sub < 0) {
			outBean.setRetCode(SlErrors.FX_SPTFWD_DEALDATE.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SPTFWD_DEALDATE.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;

		}

		return outBean;
	}

	@Override
	public SlOutBean verifiedfxSwap(SlFxSwapBean fxSwapBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == fxSwapBean) {
			logger.error("外汇掉期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_NOT_EXIST);
			outBean.setRetCode(SlErrors.FX_SWAP_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断execId是否存在 */
		if (!StringUtils.isNotEmpty(fxSwapBean.getExecId())) {
			outBean.setRetCode(SlErrors.FX_SWAP_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断IFXD表中是否已经存在
		if (fxdMapper.getSlFundIfxd(fxSwapBean.getExecId()) >= 1) {
			logger.error("外汇掉期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_EXIST);
			outBean.setRetCode(SlErrors.FX_SWAP_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空

		if (!StringUtils.isNotEmpty(fxSwapBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.FX_SWAP_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.FX_SWAP_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(fxSwapBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("外汇掉期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_EXIST);
			outBean.setRetCode(SlErrors.FX_SWAP_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手tradeId是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getContraPatryInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手InstitutionId在CUST表中是否存在
		if (commonMapper.getCno(fxSwapBean.getContraPatryInstitutionInfo().getInstitutionId()) <= 0) {
			logger.error("外汇掉期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_EXIST);
			outBean.setRetCode(SlErrors.FX_SWAP_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.FX_SWAP_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.FX_SWAP_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断投资组合port是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.FX_SWAP_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合port在PORT表中是否存在（根据br、port查）
		Map<String, Object> portMap = new HashMap<String, Object>();
		portMap.put("br", fxSwapBean.getInthBean().getBr());
		portMap.put("port", fxSwapBean.getExternalBean().getPort());
		if (commonMapper.getPort(portMap) <= 0) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_EXIST);
			outBean.setRetCode(SlErrors.FX_SWAP_PORT_NOEXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_PORT_NOEXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.FX_SWAP_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心cost在COST表中是否存在
		if (commonMapper.getCost(fxSwapBean.getExternalBean().getCost()) <= 0) {
			logger.error("外汇即期、远期导入接口>>>>>>>>>>>>>>>" + SlErrors.FX_SWAP_EXIST);
			outBean.setRetCode(SlErrors.FX_SWAP_COST_NOEXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_COST_NOEXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.FX_SWAP_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.FX_SWAP_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.FX_SWAP_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.FX_SWAP_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.FX_SWAP_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.FX_SWAP_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断custrefno（放inth的fedealno）是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getCustrefno())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CUSTREFNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CUSTREFNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算路径是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getCcysmeans())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CCYSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CCYSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算账户是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getCcysacct())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CCYSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CCYSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算路径是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getCtrsmeans())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CTRSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CTRSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算账户是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getExternalBean().getCtrsacct())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CTRSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CTRSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手清算账户与NOST表中是否相同
		/*
		 * if (fxdMapper.isCustAcctSameInNostSwap(fxSwapBean) <= 0) {
		 * logger.error("外汇掉期导入接口>>>>>>>>>>>>>>>"+
		 * SlErrors.FX_SWAP_CTRSACCT_NOT_SAME_EXIST);
		 * outBean.setRetCode(SlErrors
		 * .FX_SWAP_CTRSACCT_NOT_SAME_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors
		 * .FX_SWAP_CTRSACCT_NOT_SAME_EXIST.getErrMsg());
		 * outBean.setRetStatus(RetStatusEnum.F); return outBean; }
		 */

		// 判断本方币种是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getCurrencyInfo().getCcy())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CCY_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CCY_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断本方金额是否为空
		if (fxSwapBean.getCurrencyInfo().getAmt() == null) {
			outBean.setRetCode(SlErrors.FX_SWAP_AMT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_AMT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断对手方币种是否为空
		if (!StringUtils.isNotEmpty(fxSwapBean.getCurrencyInfo().getContraCcy())) {
			outBean.setRetCode(SlErrors.FX_SWAP_CTRCCY_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_CTRCCY_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手方金额是否为空
		// 对手方金额不必上送至Opics，如果存在点差，Opics计算与上送金额冲突
		/*
		 * if (fxSwapBean.getCurrencyInfo().getContraAmt() == null) {
		 * outBean.setRetCode(SlErrors.FX_SWAP_CTRAMT_EXIST.getErrCode());
		 * outBean.setRetMsg(SlErrors.FX_SWAP_CTRAMT_EXIST.getErrMsg());
		 * outBean.setRetStatus(RetStatusEnum.F); return outBean; }
		 */

		// 判断买卖方向是否为空
		if (fxSwapBean.getPs() == null) {
			outBean.setRetCode(SlErrors.FX_SWAP_PS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_PS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易日期是否<=系统日期
		Date dealDate = fxSwapBean.getDealDate();
		// 1.获取opics系统日期
		Date sysDate = commonMapper.getOpicsSysDate(fxSwapBean.getInthBean().getBr());

		int sub = DateUtil.daysBetween(dealDate, sysDate);
		if (sub < 0) {
			outBean.setRetCode(SlErrors.FX_SWAP_DEALDATE.getErrCode());
			outBean.setRetMsg(SlErrors.FX_SWAP_DEALDATE.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;

		}
		return outBean;
	}

	/**
	 * 校验外汇即期/远期/掉期冲销
	 * 
	 * @param fxSptFwdBean
	 * @return
	 */
	@Override
	public SlOutBean verifiedFxSptFwdRev(SlFxSptFwdBean fxSptFwdBean) {
		return null;
	}

	@Override
	public SlOutBean fxdCheckedTenor(Date dealdate, Date vdate, String ccy, String ctrccy) throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		String deralDate=new SimpleDateFormat("yyyy-MM-dd").format(dealdate);
		String valuelDate=new SimpleDateFormat("yyyy-MM-dd").format(vdate);
		Map<String, String> map = new HashMap<String, String>();
		map.put("deralDate", deralDate);
		map.put("valuelDate", valuelDate);
		map.put("ccy1", "USD");
		map.put("ccy2", "CNY");
		String t=fxdMapper.fxFwdChangeSpt(map);
		outBean.setRetCode(t);
		outBean.setRetMsg("");
		outBean.setRetStatus(RetStatusEnum.S);
		return outBean;
	}

	/**
	 * 根据fedealno查询inth表中的dealno
	 */
	@Override
	public String queryDealno(Map<String,Object> map) throws RemoteConnectFailureException, Exception {
		String dealno = fxdMapper.queryDealno(map);
		return dealno;
	}
	
	@Override
	public String getFxdhSwapDeal(String nearDealno) {
		return fxdMapper.getFxdhSwapDeal(nearDealno);
	}
	
	
	@Override
	public String fxdCheckedTdate(String arg0, String arg1)
			throws RemoteConnectFailureException, Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("ccy1", arg0);
		map.put("ccy2", arg1);
		String pariSpotDate = fxdMapper.fxSwapChange(map);
		return pariSpotDate;
	}
}