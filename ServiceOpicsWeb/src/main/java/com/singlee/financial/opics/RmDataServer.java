package com.singlee.financial.opics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlIrevBean;
import com.singlee.financial.bean.SlIrhsBean;
import com.singlee.financial.bean.SlIvolBean;
import com.singlee.financial.bean.SlIycrBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.IrevMapper;
import com.singlee.financial.mapper.IrhsMapper;
import com.singlee.financial.mapper.IvolMapper;
import com.singlee.financial.mapper.IycrMapper;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RmDataServer implements IRmDataServer{
	private static Logger logger = LoggerFactory.getLogger(RmDataServer.class);
	@Autowired
	IrevMapper irevMapper;
	@Autowired
	IrhsMapper irhsMapper;
	@Autowired
	IvolMapper ivolMapper;
	@Autowired
	IycrMapper iycrMapper;
	@Autowired
	CommonMapper commonMapper;
	@Override
	public SlOutBean addIrev(SlIrevBean irev)
			throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IREV表
		 */
		try {
			irevMapper.addIrev(irev);
		} catch (Exception e) {
			logger.error("汇率导入接口>>>>>>>>>>>>>>>" + SlErrors.IRE_ADD_IREV,e);
			outBean.setRetCode(SlErrors.IRE_ADD_IREV.getErrCode());
			outBean.setRetMsg(SlErrors.IRE_ADD_IREV.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(irev.getInthBean());
		} catch (Exception e) {
			logger.error("汇率导入接口>>>>>>>>>>>>>>>" + SlErrors.IRE_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IRE_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IRE_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean addIrhs(SlIrhsBean irhs)
			throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IRHS表
		 */
		try {
			irhsMapper.addIrhs(irhs);
		} catch (Exception e) {
			logger.error("利率导入接口>>>>>>>>>>>>>>>" + SlErrors.IRH_ADD_IRHS, e);
			outBean.setRetCode(SlErrors.IRH_ADD_IRHS.getErrCode());
			outBean.setRetMsg(SlErrors.IRH_ADD_IRHS.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(irhs.getInthBean());
		} catch (Exception e) {
			logger.error("利率导入接口>>>>>>>>>>>>>>>" + SlErrors.IRH_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IRH_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IRH_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean addIvol(SlIvolBean ivol)
			throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IVOL表
		 */
		try {
			ivolMapper.addIvol(ivol);
		} catch (Exception e) {
			logger.error("波动率导入接口>>>>>>>>>>>>>>>" + SlErrors.IVO_ADD_IVOL, e);
			outBean.setRetCode(SlErrors.IVO_ADD_IVOL.getErrCode());
			outBean.setRetMsg(SlErrors.IVO_ADD_IVOL.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(ivol.getInthBean());
		} catch (Exception e) {
			logger.error("波动率导入接口>>>>>>>>>>>>>>>" + SlErrors.IVO_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IVO_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IVO_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean addIycr(SlIycrBean iycr)
			throws RemoteConnectFailureException, Exception {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IYCR表
		 */
		try {
			iycrMapper.addIycr(iycr);
		} catch (Exception e) {
			logger.error("收益率导入接口>>>>>>>>>>>>>>>" + SlErrors.IYC_ADD_IYCR, e);
			outBean.setRetCode(SlErrors.IYC_ADD_IYCR.getErrCode());
			outBean.setRetMsg(SlErrors.IYC_ADD_IYCR.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(iycr.getInthBean());
		} catch (Exception e) {
			logger.error("收益率导入接口>>>>>>>>>>>>>>>" + SlErrors.IYC_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IYC_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IYC_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

}
