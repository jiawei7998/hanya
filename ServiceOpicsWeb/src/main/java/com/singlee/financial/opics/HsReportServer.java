package com.singlee.financial.opics;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.bean.SlHsReportBean;
import com.singlee.financial.bean.SlOutSonBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.mapper.HsReportMapper;

@SuppressWarnings("rawtypes")
@Service
public class HsReportServer implements IHsReportServer {

	@Autowired
	private HsReportMapper mapper;

	private static Logger logger = LoggerFactory.getLogger(HsReportServer.class);

	@Override
	public SlOutSonBean getHsReportData(SlHsReportBean hsReport) {
		try {
			logger.info("hsReportServer begin，报表数据生成中...");
			mapper.generateData(hsReport);
			if (!"999".equals(hsReport.getRetcode())) {
				logger.info("数据生成失败.." + hsReport.getRetcode() + ":" + hsReport.getRetmsg());
				return new SlOutSonBean(RetStatusEnum.F, "error.hsReport.0001", "报表数据生成失败..");
			}
			logger.info("报表数据生成完毕..");

			String methodName = hsReport.getMethodname();
			if (!checkMethodName(methodName)) {
				logger.info("报表数据查询方法不存在...");
				return new SlOutSonBean(RetStatusEnum.F, "error.hsReport.0002", "报表数据查询方法不存在..");
			}

			logger.info("数据查询中...");
			List list = callMethod(methodName, hsReport);
			logger.info("数据查询完成...");
			if (null == list || list.size() == 0) {
				return new SlOutSonBean(RetStatusEnum.F, "error.hsReport.0003", "生成报表数据为空..");
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("data", list);
			return new SlOutSonBean(RetStatusEnum.S, "error.common.0000", "报表数据处理成功..", map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("数据生成发生异常：" + e);
			return new SlOutSonBean(RetStatusEnum.F, "error.hsReport.0004", "报表数据生成时发生异常.." + e);
		}
	}

	public boolean checkMethodName(String methodName) {
		List<String> list = new ArrayList<String>();
		list.add("queryG01");
		return list.contains(methodName);
	}

	public List callMethod(String methodName, SlHsReportBean hsReport) throws Exception {
		Method m = HsReportMapper.class.getDeclaredMethod(methodName, SlHsReportBean.class);
		return (List) m.invoke(mapper, hsReport);
	}

	/*
	 * @Override public PageInfo<Gage> getGageList(Map<String, String> map) { RowBounds rb = ParameterUtil.getRowBounds(map); Page<Gage> page = mapper.getGagePage(map, rb); return new
	 * PageInfo<Gage>(page); }
	 */

}
