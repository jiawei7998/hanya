package com.singlee.financial.opics;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlDepositsAndLoansBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IMmServer;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.MmMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class MmServer implements IMmServer {
	private static Logger logger = LoggerFactory.getLogger(MmServer.class);
	@Autowired
	MmMapper mmMapper;
	@Autowired
	CommonMapper commonMapper;

	@Override
	public SlOutBean idldProc(Map<String, String> arg0) {
		System.out.println(arg0);
		SlOutBean SlOutBean = new SlOutBean();
		arg0.put("RETCODE", "");
		arg0.put("RETMSG", "");
		arg0.put("br", "01");
		System.out.println(arg0.get("contractId") + "|" + arg0.get("tradeDate") + "|" + arg0.get("inputTime") + "|" + arg0.get("myDir") + "|" + arg0.get("removeTrader") + "|"
				+ arg0.get("borrowTrader") + "|" + arg0.get("amt") + "|" + arg0.get("rate") + "|" + arg0.get("firstSettlementDate") + "|" + arg0.get("secondSettlementDate") + "|" + arg0.get("br")
				+ "|" + arg0.get("product") + "|" + arg0.get("prodType") + "|" + arg0.get("cost") + "|" + arg0.get("port") + "|" + arg0.get("verifySign") + "|" + arg0.get("grantSign"));
		try {
			mmMapper.idldProc(arg0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SlOutBean.setRetCode(arg0.get("RETCODE").substring(0, 3));
		SlOutBean.setRetMsg(arg0.get("RETMSG"));
		System.out.println(SlOutBean.getRetCode() + SlOutBean.getRetMsg());
		return SlOutBean;
	}

	@Override
	public SlOutBean mm(SlDepositsAndLoansBean mmBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IDLD表
		 */
		try {
			mmMapper.addMm(mmBean);
		} catch (Exception e) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_ADD_IDLD, e);
			outBean.setRetCode(SlErrors.IBO_ADD_IDLD.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_ADD_IDLD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(mmBean.getInthBean());
		} catch (Exception e) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IBO_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean mmAdv(SlDepositsAndLoansBean mmAdvBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入IDLV表
		 */
		try {
			mmMapper.addMmmAdv(mmAdvBean);
		} catch (Exception e) {
			logger.error("信用拆借提前还款导入接口>>>>>>>>>>>>>>>" + SlErrors.IBOADV_ADD_IDLV, e);
			outBean.setRetCode(SlErrors.IBOADV_ADD_IDLV.getErrCode());
			outBean.setRetMsg(SlErrors.IBOADV_ADD_IDLV.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(mmAdvBean.getInthBean());
		} catch (Exception e) {
			logger.error("信用拆借提前还款导入接口>>>>>>>>>>>>>>>" + SlErrors.IBOADV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IBOADV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IBOADV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	/**
	 * 插入拆借冲销表
	 */
	@Override
	public SlOutBean mmRev(SlDepositsAndLoansBean mmBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		try {
			// 冲销之前需要验证,看INTH表里是否有已冲销过的数据
			int c = commonMapper.getSlFundInthRev(mmBean.getInthBean());
			if (c >= 1) {
				outBean.setRetCode(SlErrors.IBOREV_ADD_INTH_EXIST.getErrCode());
				outBean.setRetMsg(SlErrors.IBOREV_ADD_INTH_EXIST.getErrMsg());
				outBean.setRetStatus(RetStatusEnum.F);
				return outBean;
			}
		} catch (Exception e) {
			logger.error("拆借冲销接口>>>>>>>>>>>>>>>" + SlErrors.IBOREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IBOREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IBOREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		/**
		 * 插入IRVV表
		 */
		try {
			mmMapper.addMmRev(mmBean);
		} catch (Exception e) {
			logger.error("拆借冲销接口>>>>>>>>>>>>>>>" + SlErrors.IBOREV_ADD_IDLD, e);
			outBean.setRetCode(SlErrors.IBOREV_ADD_IDLD.getErrCode());
			outBean.setRetMsg(SlErrors.IBOREV_ADD_IDLD.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInthRev(mmBean.getInthBean());
		} catch (Exception e) {
			logger.error("拆借冲销接口>>>>>>>>>>>>>>>" + SlErrors.IBOREV_ADD_INTH, e);
			outBean.setRetCode(SlErrors.IBOREV_ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.IBOREV_ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean verifiedMm(SlDepositsAndLoansBean mmBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);

		if (null == mmBean) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_IDLD_NOT_EXIST);
			outBean.setRetCode(SlErrors.IBO_IDLD_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/* 判断fedealno是否存在 */
		if (!StringUtils.isNotEmpty(mmBean.getInthBean().getFedealno())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断IDLD表中是否已经存在
		if (mmMapper.getSlFundMm(mmBean.getInthBean().getFedealno()) >= 1) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_IDLD_EXIST);
			outBean.setRetCode(SlErrors.IBO_IDLD_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手是否为空
		if (!StringUtils.isNotEmpty(mmBean.getContraPatryInstitutionInfo().getInstitutionId())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_CNO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CNO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易对手在CUST表中是否存在
		if (commonMapper.getCno(mmBean.getContraPatryInstitutionInfo().getInstitutionId()) <= 0) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_IDLD_EXIST);
			outBean.setRetCode(SlErrors.IBO_IDLD_CNO_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CNO_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断br是否为空
		if (!StringUtils.isNotEmpty(mmBean.getInthBean().getBr())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_BR_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_BR_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易员tradeId是否为空
		if (!StringUtils.isNotEmpty(mmBean.getInstitutionInfo().getTradeId())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_TRADID_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_TRADID_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易员tradeId在OPER表中是否存在
		if (commonMapper.getTradeId(mmBean.getInstitutionInfo().getTradeId()) <= 0) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_IDLD_EXIST);
			outBean.setRetCode(SlErrors.IBO_IDLD_TRADID_OPER_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_TRADID_OPER_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断产品代码是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getProdcode())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_PRODCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_PRODCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断产品类型是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getProdtype())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_PRODTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_PRODTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断投资组合是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getPort())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_PORT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_PORT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断成本中心是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getCost())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_COST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_COST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断授权标识siind是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getSiind())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_SIIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_SIIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断授权标识authsi是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getAuthsi())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_AUTHSI_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_AUTHSI_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断复核标识verind是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getVerind())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_VERIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_VERIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断付款报文标识suppayind是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getSuppayind())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_SUPPAYIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_SUPPAYIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断收款报文标识suprecind是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getSuprecind())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_SUPRECIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_SUPRECIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断确认款报文标识supconfind是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getSupconfind())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_SUPCONFIND_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_SUPCONFIND_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算路径是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getCcysmeans())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_CCYSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CCYSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断本方清算账户是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getCcysacct())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_CCYSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CCYSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算路径是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getCtrsmeans())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_CTRSMEANS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CTRSMEANS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对方清算账户是否为空
		if (!StringUtils.isNotEmpty(mmBean.getExternalBean().getCtrsacct())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_CTRSACCT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CTRSACCT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断对手清算账户与NOST表中是否相同
		if (mmMapper.isCustAcctSameInNost(mmBean) <= 0) {
			logger.error("信用拆借导入接口>>>>>>>>>>>>>>>" + SlErrors.IBO_IDLD_CTRSACCT_NOT_SAME_EXIST);
			outBean.setRetCode(SlErrors.IBO_IDLD_CTRSACCT_NOT_SAME_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CTRSACCT_NOT_SAME_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断买卖方向是否为空
		if (mmBean.getPs() == null) {
			outBean.setRetCode(SlErrors.IBO_IDLD_PS_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_PS_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		// 判断交易币种是否为空
		if (!StringUtils.isNotEmpty(mmBean.getCcy())) {
			outBean.setRetCode(SlErrors.IBO_IDLD_CCY_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_CCY_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断交易币种金额是否为空
		if (mmBean.getAmt() == null) {
			outBean.setRetCode(SlErrors.IBO_IDLD_AMT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.IBO_IDLD_AMT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		return outBean;
	}

	/***
	 * 校验拆借冲销
	 */
	@Override
	public SlOutBean verifiedMmRev(SlDepositsAndLoansBean depositsAndLoansBean) {
		// TODO Auto-generated method stub
		return null;
	}
}
