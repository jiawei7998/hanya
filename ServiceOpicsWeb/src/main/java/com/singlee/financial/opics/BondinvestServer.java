package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.BondinvestBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.BondinvestMapper;
import com.singlee.financial.page.PageInfo;

/**
 * 带参数查询 债券投资业务表
 * @author wangzhao
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BondinvestServer implements IBondinvestServer {

	@Autowired
	BondinvestMapper bondinvestMapper;
	@Override
	public PageInfo<BondinvestBean> getBondinvestReport(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<BondinvestBean> page=bondinvestMapper.getBondinvestBeanList(map,rb);
		return new PageInfo<BondinvestBean>(page);
	}
	@Override
	public List<BondinvestBean> getBondinvestReportList(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<BondinvestBean> page=bondinvestMapper.getBondinvestBeanListByDate(map);
		return page;
	}

}
