package com.singlee.financial.opics;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSecmBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.ExternalMapper;
import com.singlee.financial.mapper.SemmMapper;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.wks.bean.opics.Seci;
import com.singlee.financial.wks.mapper.opics.SeciMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ExternalServer implements IExternalServer {

	private static Logger logger = Logger.getLogger(ExternalServer.class);

	@Autowired
	ExternalMapper externalMapper;
	@Autowired
	SemmMapper semmMapper;
	@Autowired
	CommonMapper commonMapper;
	@Autowired
	SeciMapper seciMapper;

	@Override
	public SlOutBean secmProc(Map<String, String> arg0) {
		System.out.println(arg0);
		SlOutBean retBean = new SlOutBean();
		arg0.put("RETCODE", "");
		arg0.put("RETMSG", "");
		arg0.put("br", "01");
		try {
			semmMapper.semmProc(arg0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		retBean.setRetCode(arg0.get("RETCODE").substring(0, 3));
		retBean.setRetMsg(arg0.get("RETMSG"));
		System.out.println(retBean.getRetCode() + retBean.getRetMsg());
		return retBean;
	}

	@Override
	public SlOutBean addCust(SlCustBean custBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入表
		 */
		try {
			externalMapper.addSlFundCust(custBean);
		} catch (Exception e) {

			logger.error("OPICS CUST INSERT TABLE ICUS " + SlErrors.ADD_ICUS, e);
			outBean.setRetCode(SlErrors.ADD_ICUS.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_ICUS.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		/*try {
			externalMapper.addCust(custBean); // 同步到CUST表
		} catch (Exception e) {

			logger.error("OPICS CUST INSERT TABLE CUST " + SlErrors.CUST_EXIST, e);
			outBean.setRetCode(SlErrors.CUST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}*/

		try {
			commonMapper.addSlFundInth(custBean.getInthBean());
		} catch (Exception e) {

			logger.error("OPICS CUST INSERT TABLE INTH " + SlErrors.ADD_INTH, e);
			outBean.setRetCode(SlErrors.ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean addSemm(SlSecmBean secmBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入表
		 */
		try {
			externalMapper.addSlFundSemm(secmBean);
		} catch (Exception e) {

			logger.error("OPICS ISEC INSERT TABLE ISEC " + SlErrors.ADD_ISEC, e);
			outBean.setRetCode(SlErrors.ADD_ISEC.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_ISEC.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		try {
			commonMapper.addSlFundInth(secmBean.getInthBean());
		} catch (Exception e) {

			logger.error("OPICS ISEC INSERT TABLE INTH " + SlErrors.ADD_INTH, e);
			outBean.setRetCode(SlErrors.ADD_ISEC.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_ISEC.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		try {
			//保存发行价格
			Seci seci = new Seci();
			seci.setSecid(secmBean.getSecid());
			seci.setAmtissued(BigDecimal.ZERO);
			seci.setSeasonind("0");
			seci.setIssueprice8(secmBean.getIssueprice());
			seci.setFinanctype("0");
			seci.setTaxcallind("0");
			seci.setWithholdtax8(BigDecimal.ZERO);
			seci.setLaunchspread8(BigDecimal.ZERO);
			seci.setSwapissueind("0");
			seci.setLstmntdte(Calendar.getInstance().getTime());
			//判断是否存在
			Seci si =seciMapper.selectByPrimaryKey(seci.getSecid());
			if(null == si){
				seciMapper.insertSelective(seci);
			}else {
				seciMapper.updateByPrimaryKey(seci);
			}
		} catch (Exception e) {
			logger.error("OPICS SECI INSERT TABLE INTH " + SlErrors.ADD_INTH, e);
			outBean.setRetCode(SlErrors.ADD_ISEC.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_ISEC.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean verifiedCust(SlCustBean custBean) {
		SlOutBean outBean = new SlOutBean();
		/**
		 * 成功状态
		 */
		outBean.setRetCode(SlErrors.SUCCESS_VER.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS_VER.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		if (null == custBean) {
			logger.error("OPICS CHECK CUST MSG " + SlErrors.CUST_NOT_EXIST);
			outBean.setRetCode(SlErrors.CUST_NOT_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_NOT_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}

		if (StringUtils.isNotEmpty(custBean.getCno())) {
			outBean.setRetCode(SlErrors.CUST_NO_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_NO_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断CUST表中是否已经存在
		if (externalMapper.getSlFundCust(custBean.getCno()) >= 1) {
			logger.error("OPICS CHECK CUST EXIST" + SlErrors.CUST_EXIST);
			outBean.setRetCode(SlErrors.CUST_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		if (StringUtils.isNotEmpty(custBean.getBic())) {
			outBean.setRetCode(SlErrors.CUST_BIC_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_BIC_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断是否存在BIC码
		if (externalMapper.getSlFundBic(custBean.getBic()) < 1) {
			logger.error("OPICS CHECK CUST BIC EXIST " + SlErrors.CUST_BIC_EXIST);
			outBean.setRetCode(SlErrors.CUST_BIC_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_BIC_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		if (StringUtils.isNotEmpty(custBean.getUccode())) {
			outBean.setRetCode(SlErrors.CUST_UCCOUN_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_UCCOUN_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断是否存在uccode码
		if (externalMapper.getSlFundUccode(custBean.getUccode()) < 1) {
			logger.error("OPICS CHECK CUST UCCOUN EXIST " + SlErrors.CUST_UCCOUN_EXIST);
			outBean.setRetCode(SlErrors.CUST_UCCOUN_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_UCCOUN_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		if (StringUtils.isNotEmpty(custBean.getCcode())) {
			outBean.setRetCode(SlErrors.CUST_CCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_CCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断是否存在ccode码
		if (externalMapper.getSlFundCcode(custBean.getCcode()) < 1) {
			logger.error("OPICS CHECK CUST CCODE EXIST " + SlErrors.CUST_CCODE_EXIST);
			outBean.setRetCode(SlErrors.CUST_CCODE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_CCODE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;

		}
		if (StringUtils.isNotEmpty(custBean.getAcctngtype())) {
			outBean.setRetCode(SlErrors.CUST_ACCTNGTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_ACCTNGTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断是否存在acctngtype码
		if (externalMapper.getSlFundAcctngtype(custBean.getAcctngtype()) < 1) {
			logger.error("OPICS CHECK CUST ACCTNGTYPE EXIST " + SlErrors.CUST_ACCTNGTYPE_EXIST);
			outBean.setRetCode(SlErrors.CUST_ACCTNGTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_ACCTNGTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		if (StringUtils.isNotEmpty(custBean.getSic())) {
			outBean.setRetCode(SlErrors.CUST_SIC_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_SIC_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断是否存在sic码
		if (externalMapper.getSlFundSic(custBean.getSic()) < 1) {
			logger.error("OPICS CHECK CUST SIC EXIST " + SlErrors.CUST_SIC_EXIST);
			outBean.setRetCode(SlErrors.CUST_SIC_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_SIC_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		if (StringUtils.isNotEmpty(custBean.getCtype())) {
			outBean.setRetCode(SlErrors.CUST_CTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_CTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		// 判断是否存在ctype码
		if (externalMapper.getSlFundCtype(custBean.getCtype()) < 1) {
			logger.error("OPICS CHECK CUST CTYPE EXIST " + SlErrors.CUST_CTYPE_EXIST);
			outBean.setRetCode(SlErrors.CUST_CTYPE_EXIST.getErrCode());
			outBean.setRetMsg(SlErrors.CUST_CTYPE_EXIST.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public SlOutBean verifiedSemm(SlSecmBean secmBean) {
		return null;
	}

	/**
	 * 获取OPICS的所有交易对手
	 */
	@Override
	public List<SlCustBean> synchroCust() {
		List<SlCustBean> list = externalMapper.getCustByAll();
		return list;
	}

	@Override
	public SlSecmBean getSemmById(String secid) {
		return externalMapper.getSemmById(secid);
	}

	@Override
	public List<SlSecmBean> synchroSemm() {
		return externalMapper.getSemmByAll();
	}

	@Override
	public SlCustBean getCust(Map<String, String> map)
			throws RemoteConnectFailureException, Exception {
		return externalMapper.getCust(map);
	}

	@Override
	public PageInfo<SlCustBean> searchPageCust(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlCustBean> page = externalMapper.searchPageCust(map,rb);
		return new PageInfo<SlCustBean>(page);
	}

}
