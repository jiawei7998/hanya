package com.singlee.financial.opics;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIrs9Bean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.RefluxFileMapper;
import com.singlee.financial.page.PageInfo;

/**
 * 回流文件
 * 
 * @author wangzt
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RefluxFileServer implements IRefluxFileServer {
	
	@Autowired
	RefluxFileMapper refluxFileMapper;

	
	@Override
	public PageInfo<SlIrs9Bean> searchRefluxFileCount(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlIrs9Bean> page = refluxFileMapper.searchRefluxFileCount(map, rb);
		return new PageInfo<SlIrs9Bean>(page);
	}

}
