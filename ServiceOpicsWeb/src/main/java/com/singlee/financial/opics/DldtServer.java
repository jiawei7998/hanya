package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.DldtRevaluation;
import com.singlee.financial.bean.SlSecmRevaluation;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlDldtBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.DldtTotalMapper;
import com.singlee.financial.page.PageInfo;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DldtServer implements IDldtCountServer{
    @Autowired
    DldtTotalMapper dldtTotalMapper;
	@Override
	public PageInfo<SlDldtBean> searchDlCount(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlDldtBean> page = dldtTotalMapper.dldtSearch(map, rb);
		return new PageInfo<SlDldtBean>(page);
	}

	@Override
	public List<SlDldtBean> searchDlCountlis(Map<String, Object> map) {
		return  dldtTotalMapper.dldtSearch(map);
	}

	@Override
	public SlDldtBean searchSlDldtBean(Map<String, Object> map) {
		return dldtTotalMapper.dldtSrearchOne(map);
	}

	@Override
	public PageInfo<DldtRevaluation> searchDldtRev(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<DldtRevaluation> result = dldtTotalMapper.searchDldtRev(map,rb);
		return new PageInfo<DldtRevaluation>(result);
	}
}
