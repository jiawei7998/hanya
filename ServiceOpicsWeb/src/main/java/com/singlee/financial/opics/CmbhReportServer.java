package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlReportG14Bean;
import com.singlee.financial.bean.SlReportG18Bean;
import com.singlee.financial.bean.SlReportG21Bean;
import com.singlee.financial.bean.SlReportG22Bean;
import com.singlee.financial.bean.SlReportG24Bean;
import com.singlee.financial.bean.SlReportG31Bean;
import com.singlee.financial.bean.SlReportG33Bean;
import com.singlee.financial.bean.SlReportY0085Bean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.CmbhReportMapper;
import com.singlee.financial.page.PageInfo;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CmbhReportServer implements ICmbhReportServer {
	
	@Autowired
	CmbhReportMapper cmbhReportMapper;

	
	@Override
	public PageInfo<SlReportG14Bean> getReportG14List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportG14Bean> page = cmbhReportMapper.getSlReportG14List(map, rb);
		return new PageInfo<SlReportG14Bean>(page);
	}
	
	@Override
	public List<SlReportG14Bean> searchReportG14List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportG14Bean> list = cmbhReportMapper.getSlReportG14List(map);
		return list;
	}
	
	@Override
	public PageInfo<SlReportG18Bean> getReportG18List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportG18Bean> page = cmbhReportMapper.getSlReportG18List(map, rb);
		return new PageInfo<SlReportG18Bean>(page);
	}

	@Override
	public List<SlReportG18Bean> searchReportG18List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportG18Bean> list = cmbhReportMapper.searchSlReportG18List(map);
		return list;
	}

	@Override
	public PageInfo<SlReportG21Bean> getReportG21List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportG21Bean> page = cmbhReportMapper.getSlReportG21List(map, rb);
		return new PageInfo<SlReportG21Bean>(page);
	}

	@Override
	public List<SlReportG21Bean> searchReportG21List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportG21Bean> list = cmbhReportMapper.searchSlReportG21List(map);
		return list;
	}

	@Override
	public PageInfo<SlReportG22Bean> getReportG22List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportG22Bean> page = cmbhReportMapper.getSlReportG22List(map, rb);
		return new PageInfo<SlReportG22Bean>(page);
	}

	@Override
	public List<SlReportG22Bean> searchReportG22List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportG22Bean> list = cmbhReportMapper.searchSlReportG22List(map);
		return list;
	}

	@Override
	public PageInfo<SlReportG24Bean> getReportG24List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportG24Bean> page = cmbhReportMapper.getSlReportG24List(map, rb);
		return new PageInfo<SlReportG24Bean>(page);
	}

	@Override
	public List<SlReportG24Bean> searchReportG24List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportG24Bean> list = cmbhReportMapper.getSlReportG24List(map);
		return list;
	}
	
	@Override
	public PageInfo<SlReportG31Bean> getReportG31List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportG31Bean> page = cmbhReportMapper.getSlReportG31List(map, rb);
		return new PageInfo<SlReportG31Bean>(page);
	}

	@Override
	public List<SlReportG31Bean> searchReportG31List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportG31Bean> list = cmbhReportMapper.searchSlReportG31List(map);
		return list;
	}

	@Override
	public PageInfo<SlReportG33Bean> getReportG33List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportG33Bean> page = cmbhReportMapper.getSlReportG33List(map, rb);
		return new PageInfo<SlReportG33Bean>(page);
	}

	@Override
	public List<SlReportG33Bean> searchReportG33List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportG33Bean> list = cmbhReportMapper.getSlReportG33List(map);
		return list;
	}

	@Override
	public PageInfo<SlReportY0085Bean> getReportY0085List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportY0085Bean> page = cmbhReportMapper.getSlReportY0085List(map, rb);
		return new PageInfo<SlReportY0085Bean>(page);
	}

	@Override
	public List<SlReportY0085Bean> searchReportY0085List(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlReportY0085Bean> list = cmbhReportMapper.getSlReportY0085List(map);
		return list;
	}




}
