package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlOpcNssbBean;
import com.singlee.financial.mapper.OpcNssbMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OpcNssbServer implements IOpcNssbServer {
	
	@Autowired
	OpcNssbMapper opcNssbMapper;

	@Override
	public List<SlOpcNssbBean> selectOpcNssbPayTaxes(Map<String, Object> map) {
		return opcNssbMapper.selectOpcNssbPayTaxes(map);
	}

	@Override
	public List<SlOpcNssbBean> selectOpcNssbCreditorRight(Map<String, Object> map) {
		return opcNssbMapper.selectOpcNssbCreditorRight(map);
	}
	

}
