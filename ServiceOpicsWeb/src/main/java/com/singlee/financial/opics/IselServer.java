package com.singlee.financial.opics;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.IselBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.mapper.IselMapper;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class IselServer {
	private static Logger logger = LoggerFactory.getLogger(IselServer.class);
	@Autowired
	IselMapper iselMapper;
	@Autowired
	CommonMapper commonMapper;
	
	public SlOutBean addIsel(List<IselBean> iselBean) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		
		String Code="OP00502";
		String ErrorName1="ERR_ADD_ISEL";
		String Messge1=" 添加债券收盘价接口表表出错（IRHS）";
		String ErrorName2="ERR_ADD_SL_ISEL";
		String Messge2=" 添加债券收盘价接口历史表出错（SL_IRHS）";
		
		for(int i=0;i<iselBean.size();i++){
		/**
		 * 插入ISEL表
		 */
		try {
		int n=iselMapper.selectBykey(iselBean.get(i));
		if(n>0){
			iselMapper.updateBykey(iselBean.get(i));
		}else{
			iselMapper.addIsel(iselBean.get(i));
		}
		}catch(Exception e) {
			logger.error("债券收盘价接口>>>>>>>>>>>>>>>" + ErrorName1, e);
			outBean.setRetCode(Code);
			outBean.setRetMsg(Messge1);
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		
		/**
		 * 插入SL_ISEL表
		 */
		try {
		String date=sdf.format(new Date());
		iselBean.get(i).setLstmntDate(date);
		iselMapper.addSlIsel(iselBean.get(i));
		}catch (Exception e) {
			logger.error("债券收盘价接口历史表>>>>>>>>>>>>>>>" + ErrorName2, e);
			outBean.setRetCode(Code);
			outBean.setRetMsg(Messge2);
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		/**
		 * 插入INTH表
		 */
		try {
			commonMapper.addSlFundInth(iselBean.get(i).getInthBean());
		} catch (Exception e) {
			logger.error("债券收盘价接口>>>>>>>>>>>>>>>" + SlErrors.ADD_INTH, e);
			outBean.setRetCode(SlErrors.ADD_INTH.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_INTH.getErrMsg());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		}
		return outBean;
	}

}
