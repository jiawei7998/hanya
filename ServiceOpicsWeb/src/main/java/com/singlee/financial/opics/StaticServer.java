package com.singlee.financial.opics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlActyBean;
import com.singlee.financial.bean.SlBicoBean;
import com.singlee.financial.bean.SlCostBean;
import com.singlee.financial.bean.SlCounBean;
import com.singlee.financial.bean.SlHldyBean;
import com.singlee.financial.bean.SlNostBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlPortBean;
import com.singlee.financial.bean.SlProdBean;
import com.singlee.financial.bean.SlProdTypeBean;
import com.singlee.financial.bean.SlRateBean;
import com.singlee.financial.bean.SlSaccBean;
import com.singlee.financial.bean.SlSetaBean;
import com.singlee.financial.bean.SlSetmBean;
import com.singlee.financial.bean.SlSicoBean;
import com.singlee.financial.bean.SlYchdBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.trade.SlErrors;
import com.singlee.financial.mapper.StaticMapper;
import com.singlee.financial.page.PageInfo;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class StaticServer implements IStaticServer {

	@Autowired
	StaticMapper staticMapper;

	private static Logger logger = LoggerFactory.getLogger(StaticServer.class);

	@Override
	public SlOutBean addProd(SlProdBean prodBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());

		try {
			SlProdBean slProdBean = staticMapper.getProdById(prodBean);
			if (slProdBean == null || "".equals(slProdBean)) {
				staticMapper.insertProd(prodBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	@Override
	public SlOutBean addProdType(SlProdTypeBean prodTypeBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());

		try {
			SlProdTypeBean slProdTypeBean = staticMapper.getProdTypeById(prodTypeBean);
			if (slProdTypeBean == null || "".equals(slProdTypeBean)) {
				staticMapper.insertProdType(prodTypeBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	@Override
	public SlOutBean addCost(SlCostBean costBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());

		try {
			SlCostBean slCostBean = staticMapper.getCostById(costBean);
			if (slCostBean == null || "".equals(slCostBean)) {
				staticMapper.insertCost(costBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	@Override
	public SlOutBean addPort(SlPortBean portBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());

		try {
			SlPortBean slPortBean = staticMapper.getPortById(portBean);
			if (slPortBean == null || "".equals(slPortBean)) {
				staticMapper.insertPort(portBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	/***
	 * 插入opics系统一条国家代码记录
	 * 
	 * @author lij
	 */
	@Override
	public SlOutBean addCoun(SlCounBean counBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());

		try {
			SlCounBean slCounBean = staticMapper.getCounById(counBean);
			if (slCounBean == null || "".equals(slCounBean)) {
				staticMapper.insertCoun(counBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	/***
	 * 插入opics系统一条工业代码记录
	 * 
	 * @author lij
	 */
	@Override
	public SlOutBean addBico(SlBicoBean bicoBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());

		try {
			SlBicoBean slBicoBean = staticMapper.getBicoById(bicoBean);
			if (slBicoBean == null || "".equals(slBicoBean)) {
				staticMapper.insertBico(bicoBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	/***
	 * 插入opics系统一条SWIFT码记录
	 * 
	 * @author lij
	 */
	@Override
	public SlOutBean addSico(SlSicoBean sicoBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		
		try {
			SlSicoBean slSicoBean = staticMapper.getSicoById(sicoBean);
			if (slSicoBean == null || "".equals(slSicoBean)) {
				staticMapper.insertSico(sicoBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	@Override
	public SlOutBean addActy(SlActyBean actyBean) {
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		/**
		 * 插入表
		 */
		try {
			staticMapper.insertActy(actyBean);
		} catch (Exception e) {
			logger.error("OPICS CUST INSERT TABLE ACTY " + SlErrors.ADD_ACTY, e);
			outBean.setRetCode(SlErrors.ADD_ACTY.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_ACTY.getErrMsg()+":"+e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		return outBean;
	}

	@Override
	public List<SlProdBean> synchroProd() {
		List<SlProdBean> list = staticMapper.getProdByAll();
		return list;
	}

	@Override
	public List<SlProdTypeBean> synchroProdType() {
		List<SlProdTypeBean> list = staticMapper.getProdTypeByAll();
		return list;
	}

	@Override
	public List<SlCostBean> synchroCost() {
		List<SlCostBean> list = staticMapper.getCostByAll();
		return list;
	}

	@Override
	public List<SlPortBean> synchroPort() {
		List<SlPortBean> list = staticMapper.getPortByAll();
		return list;
	}

	/***
	 * 获取opics系统所有的国家代码记录
	 * 
	 * @author lij
	 */
	@Override
	public List<SlCounBean> synchroCoun() {
		List<SlCounBean> list = staticMapper.getCounByAll();
		return list;
	}

	/***
	 * 获取opics系统所有的工业代码记录
	 * 
	 * @author lij
	 */
	@Override
	public List<SlBicoBean> synchroBico() {
		List<SlBicoBean> list = staticMapper.getBicoByAll();
		return list;
	}

	/***
	 * 获取opics系统所有的SWIFT码记录
	 * 
	 * @author lij
	 */
	@Override
	public List<SlSicoBean> synchroSico() {
		List<SlSicoBean> list = staticMapper.getSicoByAll();
		return list;
	}

	/**
	 * 将查询到的acty数据进行校准
	 */
	@Override
	public List<SlActyBean> synchroActy() {
		List<SlActyBean> slActyBean = new ArrayList<SlActyBean>();
		SlOutBean outBean = new SlOutBean();
		outBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		outBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		outBean.setRetStatus(RetStatusEnum.S);
		try {
			slActyBean = staticMapper.getActyByAll();
		} catch (Exception e) {
			logger.error("OPICS CUST INSERT TABLE ACTY " + SlErrors.ADD_ACTY, e);
			outBean.setRetCode(SlErrors.ADD_ACTY.getErrCode());
			outBean.setRetMsg(SlErrors.ADD_ACTY.getErrMsg()+":"+e.getMessage());
			outBean.setRetStatus(RetStatusEnum.F);
		}
		return slActyBean;
	}

	@Override
	public SlOutBean addSeta(SlSetaBean setaBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		try {
			SlSetaBean slSetaBean = staticMapper.getSetaById(setaBean);
			if (slSetaBean == null || ("").equals(slSetaBean)) {
				staticMapper.insertSeta(setaBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	@Override
	public SlOutBean addNost(SlNostBean nostBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		try {
			SlNostBean slNostBean = staticMapper.getNostById(nostBean);
			if (slNostBean == null || ("").equals(slNostBean)) {
				staticMapper.insertNost(nostBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	@Override
	public List<SlSetaBean> synchroSeta() {
		List<SlSetaBean> list = staticMapper.getSetaByAll();
		return list;
	}

	@Override
	public List<SlNostBean> synchroNost() {
		List<SlNostBean> list = staticMapper.getNostByAll();
		return list;
	}
	
	@Override
	public PageInfo<SlNostBean> searchPageNos(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlNostBean> page = staticMapper.searchPageNos(map,rb);
		return new PageInfo<SlNostBean>(page);
	}

	/***
	 * 插入opics系统一条清算方式记录
	 * 
	 * @author rongliu
	 */
	@Override
	public SlOutBean addSetm(SlSetmBean setmBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		
		try {
			SlSetmBean slSetmBean = staticMapper.getSetmById(setmBean);
			if (slSetmBean == null || ("").equals(slSetmBean)) {
				staticMapper.insertSetm(setmBean);
			} else {
				slOutBean.setRetStatus(RetStatusEnum.F);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	@Override
	public SlOutBean addSacc(SlSaccBean saccBean) {
		SlOutBean slOutBean = new SlOutBean();
		slOutBean.setRetStatus(RetStatusEnum.S);
		slOutBean.setRetCode(SlErrors.SUCCESS.getErrCode());
		slOutBean.setRetMsg(SlErrors.SUCCESS.getErrMsg());
		
		try {
			SlSaccBean slSaccBean = staticMapper.getSaccById(saccBean);
			if (slSaccBean == null || ("").equals(slSaccBean)) {
				staticMapper.insertSacc(saccBean);
			} 
		} catch (Exception e) {
			logger.error(SlErrors.FAILED.getErrMsg()+":"+e.getMessage(),e);
			slOutBean.setRetStatus(RetStatusEnum.F);
			slOutBean.setRetCode(SlErrors.FAILED.getErrCode());
			slOutBean.setRetMsg(SlErrors.FAILED.getErrMsg()+":"+e.getMessage());
			return slOutBean;
		}
		return slOutBean;
	}

	/***
	 * 查询所有清算方式记录
	 * 
	 * @author rongliu
	 */
	@Override
	public List<SlSetmBean> synchroSetm() {
		List<SlSetmBean> list = staticMapper.getSetmByAll();
		return list;
	}

	@Override
	public List<SlSaccBean> synchroSacc() {
		List<SlSaccBean> list = staticMapper.getSaccByAll();
		return list;
	}

	/***
	 * 插入利率代码
	 */
	@Override
	public SlOutBean addRate(SlRateBean rateBean) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlOutBean addYchd(SlYchdBean ychdBean) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SlRateBean> synchroRate() {
		List<SlRateBean> list = staticMapper.getRateByAll();
		return list;
	}

	@Override
	public List<SlYchdBean> synchroYchd() {
		List<SlYchdBean> list = staticMapper.getYchdByAll();
		return list;
	}

	/***
	 * 插入节假日表
	 */
	@Override
	public SlOutBean addHldy(SlHldyBean arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/***
	 * 查询所有的节假日数据
	 */
	@Override
	public List<SlHldyBean> synchroHldy() {
		List<SlHldyBean> list = staticMapper.getHldyByAll();
		return list;
	}

	@Override
	public List<SlProdTypeBean> synchroProdTypeByProd(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		return staticMapper.synchroProdTypeByProd(map);
	}

	
}