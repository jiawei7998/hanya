package com.singlee.financial.opics;

import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlGentBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.SlGentMapper;
import com.singlee.financial.page.PageInfo;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlGentServer implements ISlGentServer {
	
	@Autowired
	SlGentMapper slGentMapper;

	/**
	 * 新增时保存
	 */
	@Override
	public SlOutBean addSlGent(SlGentBean gentBean) {
		SlOutBean bean = new SlOutBean();
		slGentMapper.insert(gentBean);
		return bean;
	}

	@Override
	public SlOutBean updateSlGent(SlGentBean gentBean) {
		return null;
	}

	/**
	 * 删除选中记录
	 */
	@Override
	public void deleteSlGent(SlGentBean gentBean) {
		slGentMapper.deleteSlGent(gentBean);
	}
    /**
     * 根据成本中心查询本方交易员
     */
	@Override
	public SlGentBean getSlGent(SlGentBean gentBean) {
		return slGentMapper.getSlGent(gentBean);
	}

	/**
	 * 查询所有的gent信息，返回page页
	 */
	@Override
	public PageInfo<SlGentBean> searchPageGent(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlGentBean> page = slGentMapper.searchPageGent(map,rb);
		return new PageInfo<SlGentBean>(page);
	}

	/**
	 * 修改时保存
	 */
	@Override
	public void updateById(SlGentBean gentBean) {
		slGentMapper.updateById(gentBean);
	}
	
	/**
	 * 根据主键取查询有无此数据
	 */
	@Override
	public SlGentBean searchIfsOpicsGent(Map<String, String> map) {
		SlGentBean entity = slGentMapper.searchIfsOpicsGent(map);
		return entity;
	}
}