package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlReportZjgyjz;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.SlReportZjgyjzMapper;
import com.singlee.financial.page.PageInfo;

/***
 * 资金交易公允价值监测报表
 * @author LIJ
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlReportZjgyjzServer implements ISlReportZjgyjzServer {
	@Autowired
	SlReportZjgyjzMapper slReportZjgyjzMapper;

	@Override
	public PageInfo<SlReportZjgyjz> getPageZjgyjzReport(Map<String, Object> map)throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportZjgyjz> page=slReportZjgyjzMapper.getPageZjgyjzReport(map,rb);
		return new PageInfo<SlReportZjgyjz>(page);
	}

	@Override
	public List<SlReportZjgyjz> getListZjgyjzReport(Map<String, Object> map)throws RemoteConnectFailureException, Exception {
		return slReportZjgyjzMapper.getPageZjgyjzReport(map);
	}

}
