package com.singlee.financial.opics;

import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.mapper.SubjectEndOfDayCheckMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 主要用于查询sl_acup   科目余额
 * @author copysun
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SubjectEndOfDayCheckServer implements ISubjectEndOfDayCheckServer {

	@Autowired
	private SubjectEndOfDayCheckMapper subjectEndOfDayCheckMapper;


	@Override
	public List<SlAcupBean> getList(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		//从opics获取所有科目的余额
		List<SlAcupBean> opicsAcupList = subjectEndOfDayCheckMapper.getList(map);
		return opicsAcupList;
	}


}
