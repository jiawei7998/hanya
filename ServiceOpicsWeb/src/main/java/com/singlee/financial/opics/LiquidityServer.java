package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.LiquidityBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.LiquidityMapper;
import com.singlee.financial.page.PageInfo;

/**
 * Liquidity-表一
 * @author wangzhao
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class LiquidityServer implements ILiquidityServer {

	@Autowired
	LiquidityMapper liquidityMapper;
	@Override
	public PageInfo<LiquidityBean> getLiquidityReport(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<LiquidityBean> page=liquidityMapper.getLiquidityList(map,rb);
		return new PageInfo<LiquidityBean>(page);
	}
	@Override
	public List<LiquidityBean> getLiquidityListReport(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
	
		List<LiquidityBean> page =liquidityMapper.getLiquidityListByDate(map);
		return page;
	}

}
