package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.ZjsyjcBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.ZjsyjcMapper;
import com.singlee.financial.page.PageInfo;

/**
 * 带参数查询 Zjsyjc-资金业务收益检测表
 * @author wangzhao
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class ZjsyjcServer implements IZjsyjcServer {

	@Autowired
	ZjsyjcMapper zjsyjcMapper;
	@Override
	public PageInfo<ZjsyjcBean> getZjsyjcReport(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<ZjsyjcBean> page=zjsyjcMapper.getZjsyjcBeanList(map,rb);
		return new PageInfo<ZjsyjcBean>(page);
	}
	@Override
	public List<ZjsyjcBean> getZjsyjcReportList(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<ZjsyjcBean> page=zjsyjcMapper.getZjsyjcBeanListByDate(map);
		return page;
	}

}
