package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.A1403Bean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.A1403Mapper;
import com.singlee.financial.page.PageInfo;

/**
 * 带参数查询 A1403-外资银行资产负债项目月报表数据
 * @author wangzhao
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class A1403Server implements IA1403Server {

	@Autowired
	A1403Mapper a1403Mapper;
	@Override
	public PageInfo<A1403Bean> getA1403Report(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<A1403Bean> page=a1403Mapper.getA1403BeanList(map,rb);
		return new PageInfo<A1403Bean>(page);
	}
	@Override
	public List<A1403Bean> getA1403ReportList(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<A1403Bean> list =a1403Mapper.getA1403ReportList(map);
		return list;
	}
	@Override
	public List<String> getA1403ReportAllBr()
			throws RemoteConnectFailureException, Exception {
		List<String> list= a1403Mapper.getA1403ReportAllBr();
		return list;
	}

}
