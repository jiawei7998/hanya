package com.singlee.financial.opics;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.BatchFlag;
import com.singlee.financial.bean.SendInfo;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlAcupErrBean;
import com.singlee.financial.bean.SlAcupRecord;
import com.singlee.financial.bean.SlAcupTaxBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlOutSonBean;
import com.singlee.financial.common.spring.ExternalIntfc;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.common.util.SingleeTelnetUtils;
import com.singlee.financial.mapper.AcupTotalMapper;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.Ccyp;
import com.singlee.financial.wks.service.opics.CcypService;
import com.singlee.financial.wks.service.trans.FxdEntry;

/**
 * OPICS 总账处理
 * 
 * 1.调用总账处理
 * 
 * 2.检查账务生成结果
 * 
 * 3.查询正常、错误账务情况
 * 
 * @author shenzl
 * 
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class AcupServer implements IAcupServer {

	private static Logger logger = Logger.getLogger(AcupServer.class);

	@Autowired
	AcupTotalMapper acupMapper;

    @Autowired
    CcypService ccypService;

	@Autowired
	ExternalIntfc interfaceAddress;

	/**
	 * 总账数据生成
	 */
	@Override
	public SlOutBean createAcupProc(Map<String, String> map) {
		SlOutBean msg = null;
		logger.info("正在生成.........");
		String postdate = map.get("postdate");
		String opicsflag = acupMapper.getOPICSFlag(map);
		// 加上这个限制之后,只能生成,opics上一个账务日期的账务,而且只有OPICS,brps为'online'才能进行后续步骤
		if ("1".equals(opicsflag)) {
			String flag = acupMapper.getFlag(postdate);
			// 如果总账sendFlag为空或者为0 可以进行生成,否则都是已经生成
			if ("".equals(flag) || flag == null || "0".equals(flag) || "1".equals(flag)) {
				logger.info("账务部门："+map.get("br")+"账务日期："+postdate);
				acupMapper.acupCreate(map);
				// 如果总账生成成功,还需要判断sl_acup是否有数据,否则也不算生成成功
				if ("999".equals(map.get("RETCODE").trim())) {
					Integer createFlag = acupMapper.getCreateFlag(postdate);
					if (createFlag > 0) {
						msg = new SlOutBean(RetStatusEnum.S, "AAAAAAAAAA", "总账生成成功!" + "\n");
						logger.info("生成成功!");
					} else {
						// acupMapper.updateAcupSend(postdate);//根据日期(postdate)更新SL_ACUP_SENDINFO表的sendflag,修改为0--未生成
						//acupMapper.deleteAcupSendInfo(postdate);// 根据日期(postdate)删除SL_ACUP_SENDINFO表的对应记录
						msg = new SlOutBean(RetStatusEnum.F, "ProcSButNoData",
								" [code]: " + map.get("RETCODE").trim() + " [msg]: " + map.get("RETMSG").trim() + "\n");
						logger.info(" [code]: " + map.get("RETCODE").trim() + "  [msg]: " + map.get("RETMSG").trim()
								+ " ProcSuccessButNoData  sl_acup未发现符合条件数据!");


					}

				} else { // 生成失败的原因 "570"有很多种情况,目前不能具体到什么原因
					//acupMapper.deleteAcupSendInfo(postdate);// 根据日期(postdate)删除SL_ACUP_SENDINFO表的对应记录
					msg = new SlOutBean(RetStatusEnum.F, "singleeInfo",
							" [code]: " + map.get("RETCODE").trim() + " [msg]: " + map.get("RETMSG").trim() + "\n");
					logger.info(" [code]: " + map.get("RETCODE").trim() + "  [msg]: " + map.get("RETMSG").trim()
							+ "   总账生成失败,请检查!");
				}
			} else {
				// 改状态码 singleeInfo要改 ledgerAutoSendManage.jsp-IfsOpicsAcupServiceImpl
				msg = new SlOutBean(RetStatusEnum.F, "singleeInfo", "总账已发送,不能再次生成!" + "\n");
				logger.info("总账已发送,不能再次生成!");
			}
		} else {
			msg = new SlOutBean(RetStatusEnum.F, "singleeInfo",
					"singleeInfo: " + "OPICS BATCH RUNNING! 总账正在生成! " + "\n");
			logger.info("singleeInfo: " + "OPICS BATCH RUNNING 总账正在生成!");
		}

		return msg;
	}

	@Override
	public SlOutBean callAcupProc(Map<String, String> map) {
		String br = map.get("br");
		String msg = "";
		logger.info("正在为机构： " + br + "生成总账数据....");
		String[] arr = br.split(",");
		for (int i = 0; i < arr.length; i++) {
			logger.info("机构：" + arr[i] + "总账数据开始生成...");

			Map<String, String> param = new HashMap<String, String>();
			param.put("br", arr[i]);
			param.put("postdate", map.get("postdate"));
			param.put("status", RetStatusEnum.S.toString());
			List<SlAcupRecord> records = queryOperaRec(param);
			if (null != records && records.size() > 0) {
				logger.info("机构：" + arr[i] + "总账数据已生成...");
				msg += "机构：" + arr[i] + "总账数据已生成...\n";
				continue;
			}

			acupMapper.acupCreate(param);
			String retCode = param.get("RETCODE").trim();
			logger.info("总账过程执行完毕。。" + retCode);

			SlAcupRecord record = new SlAcupRecord();
			record.setBr(param.get("br"));
			record.setPostDate(param.get("postdate"));
			record.setCurrStep("1"); // 设置步骤数
			record.setRetCode(retCode);
			record.setRetMsg(null != param.get("RETMSG") ? param.get("RETMSG").trim() : "");
			record.setLastDate(DateUtil.getCurrentDateTimeAsString());
			record.setSuccCount(acupMapper.getSuccAcupCount(param) + ""); // 设置账务生成成功条数
			record.setFailCount(acupMapper.getFailAcupCount(param) + ""); // 设置账务生成失败条数
			record.setSetNo(acupMapper.getSetNoCount(param) + ""); // 根据机构查询总账套数

			if (!"999".equals(retCode)) {
				record.setStatus(RetStatusEnum.F.toString());
				handleRecord(record);
				msg = "机构： " + arr[i] + "总账生成失败  retCode:" + retCode + "\n";
				return new SlOutBean(RetStatusEnum.F, retCode, msg);
			}

			record.setStatus("S");
			record.setRetMsg("总账数据生成完毕...");
			handleRecord(record);

			logger.info("机构：" + arr[i] + "总账数据生成完毕...");
			msg += "机构：" + arr[i] + "总账数据生成完毕...\n";
		}
		return new SlOutBean(RetStatusEnum.S, "error.common.0000", msg);
	}

	/**
	 * flag: 1:已生成待发送; 2:已发送待取回; 3:取回并处理成功 检查是否已经送账
	 */
	@Override
	public SlOutBean checkSendFlag(String date) {
		SlOutBean rtb = new SlOutBean();
		String flag = acupMapper.getFlag(date);
		Integer createFlag = acupMapper.getCreateFlag(date);
		if (flag == null) {
			rtb.setRetMsg("empty");
			return rtb;
		} else if (createFlag < 1 || null == createFlag) { // 如果数据为空,不允许发送
			rtb.setRetMsg("dataEmpty");
			return rtb;
		} else if (!"1".equals(flag)) { // 已发送待取回 和 取回并处理成功时
			rtb = new SlOutBean(RetStatusEnum.S, "error.common.0000", "repeat");
		} else {
			rtb = new SlOutBean(RetStatusEnum.S, "error.common.0000", "first");
		}
		return rtb;
	}

	@Override
	public PageInfo<SlAcupBean> getAcupList(Map<String, String> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		if ("1".equals(map.get("seetstatus"))) { // 成功
			Page<SlAcupBean> page = acupMapper.getAcupPage(map, rb);
			return new PageInfo<SlAcupBean>(page);
		}
		// 失败
		Page<SlAcupBean> page = acupMapper.getAcupErrPage(map, rb);
		return new PageInfo<SlAcupBean>(page);
	}

	@Override
	public List<SlAcupBean> getAcupById(Map<String, String> map) {
		return acupMapper.getAcupById(map);
	}

	@Override
	public PageInfo<SlAcupErrBean> getAcupErrList(Map<String, String> map) {
		return null;
	}

	@Override
	public List<SlAcupErrBean> getAcupErrById(Map<String, String> map) {
		return null;
	}

	/**
	 * 总账查询
	 */
	@Override
	public PageInfo<SlAcupBean> getLedgerList(Map<String, String> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		String queryFlag = map.get("queryFlag");
		Page<SlAcupBean> page = null;
		if ("setnoQuery".equals(queryFlag)) {
			// 根据条件查询只显示套号的总账
			page = acupMapper.getSetnoAcupNew(map, rb);
		} else if ("setnoDetailQuery".equals(queryFlag)) {
			// 根据套号查询总账
			page = acupMapper.getSetnoDetailAcup(map, rb);
		} else {
			// 根据条件查询某一套账务
			page = acupMapper.getSortAcup(map, rb);
		}

		return new PageInfo<SlAcupBean>(page);
	}

	/**
	 * 按条件查询总账(返回list)
	 */
	@Override
	public List<SlAcupBean> getAllData(Map<String, String> map) {
		List<SlAcupBean> acupList = acupMapper.getAllData(map);
		return acupList;
	}

	@Override
	public String getAcupDate(Map<String, String> map) {
		return acupMapper.getOpicsDate(map);
	}

	@Override
	public List<Map<String, String>> getOpicsBr(Map<String, String> map) {
		return acupMapper.getOpicsBr(map);
	}

	@Override
	public List<SlAcupRecord> queryOperaRec(Map<String, String> map) {
		SlAcupRecord record = new SlAcupRecord();
		record.setPostDate(map.get("postdate"));
		record.setCurrStep(map.get("currstep"));
		record.setStatus(map.get("status"));
		record.setBr(map.get("br"));
		return acupMapper.queryOperaRec(record);
	}

	@Override
	public SlOutSonBean sendValid(Map<String, String> map) {

		Map<String, Object> result = new HashMap<String, Object>();

		List<Map<String, String>> brs = acupMapper.getOpicsBr(map);
		map.put("status", "S");
		List<SlAcupRecord> records = queryOperaRec(map);

		if (null == records || records.size() == 0) {
			return new SlOutSonBean(RetStatusEnum.F, "error.acupserver.0002", "请先执行<<生成总账数据>>操作");
		}

		List<String> recordBrs = new ArrayList<String>();
		for (SlAcupRecord record : records) {
			if ("1".equals(record.getCurrStep())) {
				recordBrs.add(record.getBr());
			}

			if ("2".equals(record.getCurrStep())) { // 是否重复送账
				result.put("repeat", true);
			}
		}

		for (int i = 0; i < brs.size(); i++) {
			String br = brs.get(i).get("BR");
			if (!recordBrs.contains(br)) {
				return new SlOutSonBean(RetStatusEnum.F, "error.acupserver.0002", "机构： " + br + "总账数据未生成");
			}
		}

		result.put("records", records);

		return new SlOutSonBean(RetStatusEnum.S, "error.common.0000", "数据生成成功", result);
	}

	public void handleRecord(SlAcupRecord record) {
		SlAcupRecord query = new SlAcupRecord();
		query.setPostDate(record.getPostDate());
		query.setCurrStep(record.getCurrStep());
		query.setBr(record.getBr());
		List<SlAcupRecord> records = acupMapper.queryOperaRec(query);
		if (null == record || records.size() == 0) {
			acupMapper.insertRecord(record);
		} else {
			acupMapper.updateRecord(record);
		}
	}

	@Override
	public SlOutBean checkSendFlag() throws RemoteConnectFailureException, Exception {
		return null;
	}

	/**
	 * 查询估值
	 * 
	 * @param map里传dealno和postdate格式为 (yyyy-MM-dd)的字符串
	 */
	@Override
	public BigDecimal queryValue(Map<String, String> map) {
		String code = "";
		String prdNo = ParameterUtil.getString(map, "prdNo", "");
		if ("391".equals(prdNo) || "432".equals(prdNo)) {// 人民币远期，人民币掉期
			code = "0111";
		} else if ("433".equals(prdNo)) {// 外汇期权
			code = "6112";
		} else if ("441".equals(prdNo)) {// 利率互换
			code = "7090";
		}
		map.put("code", code);
		BigDecimal value = acupMapper.queryValue(map);
		if (value == null) {
			value = BigDecimal.valueOf(0);
		}
		if ("441".equals(prdNo)) {
			value = value.divide(new BigDecimal(2));
		}
		return value;
	}

	/**
	 * 查询汇率
	 * 
	 * @param map里传br和ccy
	 */
	@Override
	public BigDecimal queryRate(Map<String, String> map) {
		BigDecimal rate = acupMapper.queryRate(map);
		if (rate == null) {
			rate = BigDecimal.valueOf(1);
		}
		String currency = ParameterUtil.getString(map, "ccy", "");
		if ("JPY".equalsIgnoreCase(currency)) {
			int num = rate.compareTo(new BigDecimal(1));
			if (num > 0) {
				rate = rate.divide(new BigDecimal(100));
			}
		}
		return rate;
	}
	
	/**
	 * 获取当前货币对本国货币的方向
	 */
	@Override
	public String queryRateCCType(String br,String ccy,String ccybTerms) {
		Ccyp baseCcy = null;
		String strTerms = null;
		
		if(OpicsConstant.BRRD.LOCAL_CCY.equalsIgnoreCase(ccy)) {
			strTerms = ccybTerms;
		}else {
			baseCcy = ccypService.selectByPrimaryKey(br,ccy,OpicsConstant.BRRD.LOCAL_CCY);
			if(null == baseCcy) {
				return strTerms;
			}
			strTerms = calTerms(ccy,baseCcy.getCcy1(),baseCcy.getTerms());
		}
		
		return strTerms;
	}
	
	/**
	 * 获取货币对乘除关系
	 * @param ccy
	 * @param ccypCcy1
	 * @param terms
	 * @return
	 */
	private String calTerms(String ccy,String ccypCcy1,String terms) {
		String strTerms = null;
			
		if(ccy.equalsIgnoreCase(ccypCcy1)) {
			strTerms = terms;
		}else {
			strTerms = "D".equalsIgnoreCase(terms) ? "M" : "M".equalsIgnoreCase(terms) ? "D" : "";
		}
		
		return strTerms;
	}

	/**
	 * 查询债券市场价格
	 * 
	 * @param map里传br和secid
	 */
	@Override
	public BigDecimal queryPrice(Map<String, String> map) {
		BigDecimal price = acupMapper.queryPrice(map);
		if (price == null) {
			price = BigDecimal.valueOf(0);
		}
		return price;
	}

	/**
	 * 查询债券上次付息日
	 * 
	 * @param map里传secid和ipaydate
	 */
	@Override
	public Date queryDate(Map<String, Object> map) {
		Date date = acupMapper.queryDate(map);
		return date;
	}

	/**
	 * 查询价格（价格异常检测偏离度使用）
	 * 
	 * @param map里传br ,ratecode,effdate(yyyy-MM-dd) 对应opics RHIS.INTRATE
	 */
	@Override
	public BigDecimal queryRhisIntrate(Map<String, Object> map) {
		BigDecimal intRate = acupMapper.queryRhisIntrate(map);
		return intRate;
	}

	// 债券上日中债估值净价
	@Override
	public BigDecimal querySeclPrice(Map<String, Object> map) {
		BigDecimal bidPrice = acupMapper.querySeclPrice(map);
		return bidPrice;
	}

	// 同评级同期限存单收益率曲线
	@Override
	public BigDecimal querySeclRate(Map<String, Object> map) {
		BigDecimal bidRate = acupMapper.querySeclRate(map);
		return bidRate;
	}

	/**
	 * 按条件查询历史总账
	 */
	@Override
	public List<SlAcupBean> getAllHistoryData(Map<String, String> map) throws RemoteConnectFailureException, Exception {
		List<SlAcupBean> acuplist = acupMapper.getAllHistoryData(map);
		return acuplist;
	}

	/**
	 * 查询外汇交易所有损益总和
	 */
	@Override
	public BigDecimal queryFxdSum(Map<String, Object> map) {
		BigDecimal bidRate = acupMapper.queryFxdSum(map);
		return bidRate;
	}

	/**
	 * 查询节假日 交易后确认功能定时登录登出使用
	 */
	@Override
	public int checkHldy(Map<String, Object> map) {
		int hldyState = acupMapper.checkHldy(map);
		return hldyState;
	}

	@Override
	public String updateAcup(AcupSendBean acupTempBean) {
		Integer updateAcupNum = 0;
		// 需要有一个去掉空格的操作
		String retCode = acupTempBean.getRetcode();
		String retMsg = acupTempBean.getRetmsg();
		String voucher = acupTempBean.getVoucher();
		retCode = StringUtils.deleteWhitespace(retCode);
		retMsg = StringUtils.deleteWhitespace(retMsg);
		voucher = StringUtils.deleteWhitespace(voucher);
		acupTempBean.setRetcode(retCode);
		acupTempBean.setRetmsg(retMsg);
		acupTempBean.setVoucher(voucher);
		try {

			acupMapper.updateRetryAcup(acupTempBean);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return updateAcupNum + "";

	}

	@Override
	public String callOpicsBsys() throws RemoteConnectFailureException, Exception {
		String ip = interfaceAddress.getTelnetIp();
		int port = Integer.valueOf(interfaceAddress.getTelnetPort());
		String username = interfaceAddress.getTelnetUser();
		String password = interfaceAddress.getTelnetPassword();
		String telnetHomeDir = interfaceAddress.getTelnetHomeDir();
		String telnetStartBsys = interfaceAddress.getTelnetStartBsys();
		SingleeTelnetUtils telnetUtils = new SingleeTelnetUtils("VT220");//解决window乱码
		String str = "";
		logger.info("登录:[" + ip + "]主机telnet...");
		telnetUtils.login(ip, port, username, password);
		try {
			String ret = telnetUtils.sendCommand(telnetHomeDir);
			logger.info("成功切换至:[" + telnetHomeDir + "]目录");
			logger.info("执行结果切换结果" + ret);
			logger.info("开始调用OPICS批量...");
			str = telnetUtils.sendCommand(telnetStartBsys);
			logger.info("==============调用批量执行结果如下=====================");
			logger.info(str);
			logger.info("==============================================");
		} catch (UnsupportedEncodingException e) {
			logger.error("OPICS批量方法处理异常", e);
		} finally {
			telnetUtils.distinct();
		}

		return str;
	}
	/********************************
	 * 
	   *   华商总账
	 * 
	 * 
	 * *****************************/
	
	
	/**
	   *    总账生成
	 * @param map
	 * @return
	 */
	@Override
	public SlOutBean createAcupProcCmbh(Map<String, Object> map) {
		SlOutBean msg = new SlOutBean(RetStatusEnum.S, "error.common.0000", "总账生成成功");
		Map<String, String> smap=new HashMap<String, String>();
		logger.info("正在生成.........");
		//部门列表
		List<String> brList = (List<String>)map.get("brList"); 
		//判断总账是否发送
		logger.info("判断总账是否已发送.........");
		for (String br : brList) {
			smap.put("postdate", (String)map.get("postdate"));
			smap.put("br", br);
			String sendflag = acupMapper.getSendFlagCmbh(smap);
			if (sendflag!=null&&sendflag.equals(BatchFlag.SUCCEED.name())) {
				logger.info("机构： " + br + "总账已发送,不能再次生成!");
				return new SlOutBean(RetStatusEnum.F, "0001", "机构： " + br + "总账已发送,不能再次生成!");
			}
		}
		for (String br : brList) {
			smap.put("postdate", (String)map.get("postdate"));
			smap.put("br", br);
			acupMapper.acupCreate(smap);
			String RETCODE=smap.get("RETCODE");
			if (!"999".equals(RETCODE)) {
				logger.info("机构： " + br + "总账生成失败!");
				return new SlOutBean(RetStatusEnum.F, "0001", "机构： " + br + "总账生成失败!");
			}
		}
		msg.setRetCode("999");
		msg.setRetMsg("总账生成成功！");
		logger.info("总账生成成功！");
		return msg;
	}
	
	/**
	   *    检查总账是否已发送
	 */
	@Override
	public SlOutBean checkSendFlagCmbh(Map<String, Object> map) {
		SlOutBean rtb = new SlOutBean();
		Map<String, String> smap=new HashMap<String, String>();
		List<String> brList = (List<String>)map.get("brList"); 
		String postdate=(String)map.get("postdate");
		//判断总账是否发送
		logger.info("判断总账是否已发送.........");
		for (String br : brList) {
			smap.put("postdate", postdate);
			smap.put("br", br);
			String sendflag = acupMapper.getSendFlagCmbh(smap);
			if (sendflag!=null&&sendflag.equals(BatchFlag.SUCCEED.name())) {
					logger.info("总账已发送！");
					return rtb = new SlOutBean(RetStatusEnum.S, "error.common.0000", "repeat");
			}
		}
		map.put("postdate", postdate);
		map.put("brList", brList);
		Integer createFlag = acupMapper.getCreateFlagCmbh(map);
		if (createFlag < 1 || null == createFlag) { // 如果数据为空,不允许发送
			rtb.setRetMsg("dataEmpty");
			return rtb;
		} else {
			rtb = new SlOutBean(RetStatusEnum.S, "error.common.0000", "first");
		}
		return rtb;
	}
	
	/**
	   *   总账查询
	 */
	@Override
	public PageInfo<SlAcupTaxBean> queryAcupPage(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlAcupTaxBean> page = acupMapper.queryAcupPage(map, rb);
		return new PageInfo<SlAcupTaxBean>(page);
	}

	@Override
	public List<SlAcupTaxBean> queryAcupList(Map<String, String> map)
			throws RemoteConnectFailureException, Exception {
		List<SlAcupTaxBean> acupList = acupMapper.queryAcupList(map);
		return acupList;
	}
	
	@Override
	public List<String> getAllOpicsBr() {
		return acupMapper.getAllOpicsBr();
	}

	@Override
	public SlOutBean checkAcupSendInfo(Map<String, Object> map) {
		String jobId=(String)map.get("jobId");
		map.put("sendtype", jobId);
		List<SendInfo> list=acupMapper.getSendInfoList(map);
		if(list!=null&&list.size()>0) {
			int suss=0;
			for (SendInfo sendInfo : list) {
				if(sendInfo.getSendflag().equals(BatchFlag.ERROR.name())) {
					return new SlOutBean(RetStatusEnum.F, "ERROR", sendInfo.getRetmsg());
				}
				if(sendInfo.getSendflag().equals(BatchFlag.SUCCEED.name())) {
					suss+=1;
				}
			}
			if(suss==list.size()) {
				return new SlOutBean(RetStatusEnum.S, "00000000", "执行成功");
			}else {
				return new SlOutBean(RetStatusEnum.S, "PROCESSING", "执行中");
			}
		}else {
			return new SlOutBean(RetStatusEnum.S, "UNPROCESSED", "未执行");
		}
		
	}

	@Override
	public PageInfo<SlAcupTaxBean> queryErrorAcupPage(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlAcupTaxBean> page = acupMapper.queryErrorAcupPage(map, rb);
		return new PageInfo<SlAcupTaxBean>(page);
	}

	@Override
	public List<SlAcupTaxBean> queryErrorAcuplist(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlAcupTaxBean> list = acupMapper.queryErrorAcupList(map);
		return list;
	}
	@Override
	public PageInfo<SlAcupTaxBean> queryTodayPage(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlAcupTaxBean> page = acupMapper.queryTodayPage(map, rb);
		return new PageInfo<SlAcupTaxBean>(page);
	}

	@Override
	public List<SlAcupTaxBean> queryTodaylist(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		List<SlAcupTaxBean> list = acupMapper.queryTodayList(map);
		return list;
	}

	@Override
	public Map<String, Object> callProfitLoss(Map<String, Object> qMap) {
		 Map<String, Object> rmap=new HashMap<String, Object>();
         acupMapper.callProfitLoss(qMap);
         rmap.put("RETCODE", qMap.get("RETCODE"));
         rmap.put("RETMSG", qMap.get("RETMSG"));
         return rmap;
	}

	@Override
	public Map<String, Object> callAcupProfitLoss(Map<String, Object> qMap) {
		Map<String, Object> rmap=new HashMap<String, Object>();
        acupMapper.callAcupProfitLoss(qMap);
        rmap.put("RETCODE", qMap.get("RETCODE"));
        rmap.put("RETMSG", qMap.get("RETMSG"));
        return rmap;
	}


}