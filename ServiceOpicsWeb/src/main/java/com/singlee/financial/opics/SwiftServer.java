package com.singlee.financial.opics;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSwiftBean;
import com.singlee.financial.common.spring.ExternalIntfc;
import com.singlee.financial.common.util.RemoteAccessFile;
import com.singlee.financial.mapper.SwiftMapper;

@Service
public class SwiftServer implements ISwiftServer {

	@Autowired
	ExternalIntfc externalIntfc;
	
	@Autowired
	SwiftMapper swiftMapper;
	
	@Override
	public InputStream getSwiftSmbFileInputStream() {

		try {
			// 获取smb文件输出流
			return RemoteAccessFile.smbGet(externalIntfc.getSourcePath(), externalIntfc.getFileName(), externalIntfc.getUserName(), externalIntfc.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public SlOutBean sendSwiftFile() {

		return null;
	}

	@Override
	public void getSwiftFile(String localPath) throws RemoteConnectFailureException, Exception {
		RemoteAccessFile.smbGet(externalIntfc.getSourcePath(), localPath, externalIntfc.getFileName(), externalIntfc.getUserName(), externalIntfc.getPassword());
	}

	/**
	 * 根据
	 */
	@Override
	public List<SlSwiftBean> searchPageSwift(Map<String, Object> map) {
		List<SlSwiftBean> list = swiftMapper.searchPageSwift(map);
		return list;
	}

	@Override
	public List<SlSwiftBean> searchSwiftBySendflag(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		return swiftMapper.searchSwiftBySendflag(map);
	}

	@Override
	public void updateSendflag(Map<String, Object> map)
			throws RemoteConnectFailureException, Exception {
		swiftMapper.updateSendflag(map);
	}
}