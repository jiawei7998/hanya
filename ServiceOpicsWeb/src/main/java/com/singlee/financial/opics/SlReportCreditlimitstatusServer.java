package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlReportCreditlimitstatus;
import com.singlee.financial.bean.SlReportCreditrate;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.SlReportCreditlimitstatusMapper;
import com.singlee.financial.page.PageInfo;
/***
 * 同业额度使用情况报告周报
 * @author LIJ
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlReportCreditlimitstatusServer implements ISlReportCreditlimitstatusServer {
	@Autowired
	SlReportCreditlimitstatusMapper slReportCreditlimitstatusMapper;
	
	@Override
	public PageInfo<SlReportCreditlimitstatus> getPageCreditlimitstatusReport(Map<String, Object> map) throws RemoteConnectFailureException,Exception {
		RowBounds rb=ParameterUtil.getRowBounds(map);
		Page<SlReportCreditlimitstatus> page=slReportCreditlimitstatusMapper.getPageCreditlimitstatusReport(map, rb);
		return new PageInfo<SlReportCreditlimitstatus>(page);
	}

	@Override
	public List<SlReportCreditlimitstatus> getListCreditlimitstatusReport(Map<String, Object> map) throws RemoteConnectFailureException,Exception {
		return slReportCreditlimitstatusMapper.getPageCreditlimitstatusReport(map);
	}

	@Override
	public List<String> selectDistinctSheet()throws RemoteConnectFailureException, Exception {
		return slReportCreditlimitstatusMapper.selectDistinctSheet();
	}

	@Override
	public List<SlReportCreditrate> getListCreditrate(Map<String, Object> map)throws RemoteConnectFailureException, Exception {
		return slReportCreditlimitstatusMapper.getListCreditrate(map);
	}

}
