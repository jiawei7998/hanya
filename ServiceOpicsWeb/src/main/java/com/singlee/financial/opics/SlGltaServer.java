package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlGltaBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.ParameterUtil;
import com.singlee.financial.mapper.SlGltaMapper;
import com.singlee.financial.page.PageInfo;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlGltaServer implements ISlGltaServer {
	
	@Autowired
	SlGltaMapper slGentMapper;

	/**
	 * 新增时保存
	 */
	@Override
	public SlOutBean addSlGent(SlGltaBean gentBean) {
		SlOutBean bean = new SlOutBean();
		slGentMapper.insert(gentBean);
		return bean;
	}

	@Override
	public SlOutBean updateSlGent(SlGltaBean gentBean) {
		return null;
	}

	/**
	 * 删除选中记录
	 */
	@Override
	public void deleteSlGent(SlGltaBean gentBean) {
		slGentMapper.deleteSlGent(gentBean);
	}
    /**
     * 根据成本中心查询本方交易员
     */
	@Override
	public SlGltaBean getSlGent(SlGltaBean gentBean) {
		return slGentMapper.getSlGent(gentBean);
	}

	/**
	 * 查询所有的gent信息，返回page页
	 */
	@Override
	public PageInfo<SlGltaBean> searchPageGent(Map<String, Object> map) {
		RowBounds rb = ParameterUtil.getRowBounds(map);
		Page<SlGltaBean> page = slGentMapper.searchPageGent(map,rb);
		return new PageInfo<SlGltaBean>(page);
	}

	/**
	 * 修改时保存
	 */
	@Override
	public void updateById(SlGltaBean gentBean) {
		slGentMapper.updateById(gentBean);
	}
	
	/**
	 * 根据主键取查询有无此数据
	 */
	@Override
	public SlGltaBean searchIfsOpicsGent(Map<String, String> map) {
		SlGltaBean entity = slGentMapper.searchIfsOpicsGent(map);
		return entity;
	}

	@Override
	public List<SlGltaBean> searchAllTableid() {
		return slGentMapper.searchAllTableid();
	}
}