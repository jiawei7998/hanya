package com.singlee.financial.opics;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.BatchFlag;
import com.singlee.financial.bean.SendInfo;
import com.singlee.financial.bean.SlAcupTaxBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.spring.ExternalIntfc;
import com.singlee.financial.common.util.CallSpringBeanUtils;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.SingleeJschUtil;
import com.singlee.financial.common.util.SingleeTelnetUtils;
import com.singlee.financial.mapper.AcupTotalMapper;
import com.singlee.financial.mapper.CommonMapper;
import com.singlee.financial.pojo.component.RetStatusEnum;

/**
 * 用于OPICS自动日终批量处理
 * 
 * @author shenzl
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BsysAutoServer implements IBsysAutoServer {

	@Autowired
	CommonMapper commonMapper;
	@Autowired
	AcupTotalMapper acupMapper;
	@Autowired
	ExternalIntfc config;

	private static Logger logger = Logger.getLogger(BsysAutoServer.class);

	@Override
	public SlOutBean callAutoBsys(String br, String postdate, String jobId)
			throws RemoteConnectFailureException, Exception {
		SendInfo bean = null;
		SlOutBean retBean = new SlOutBean();
		Map<String, Object> map = null;
		BatchFlag batchFlag = null;
		try {
			logger.debug("自动跑批OPICS0001服务配置项:" + config);
			// 设置任务重复检查查询条件
			bean = createSlAcupSendInfo(br, postdate, jobId);
			// 批量日期
			logger.debug("自动跑批批量日期:" + DateUtil.parse(bean.getPostdate()));
			// 任务ID
			logger.debug("自动跑批作业标识:" + bean.getSendtype());
			// 判断当前日期是否为假日
			map = new HashMap<String, Object>();
			map.put("holidate", DateUtil.parse(bean.getPostdate()));
			map.put("calendarid", "CNY");
			if (acupMapper.checkHldy(map) > 1) {
				logger.info("自动跑批当前日期为假日,系统不做处理,请求日期:" + postdate);
				retBean.setRetCode("00000000");
				retBean.setRetMsg("节假日空跑成功");
				retBean.setRetStatus(RetStatusEnum.S);
				return retBean;
			}
			// 如果系统未找到日终记录则不允许执行其它任务
			// 查询当前任务状态
			SendInfo sendInfo = new SendInfo();
			sendInfo.setBr(bean.getBr());
			sendInfo.setPostdate(bean.getPostdate());
			sendInfo.setSendtype(bean.getSendtype());
			logger.debug("自动跑批日终处理" + bean.getSendtype() + "查询条件:" + bean);
			sendInfo = acupMapper.getAcupInfo(sendInfo);
			batchFlag = BatchFlag.getByDbValue(sendInfo == null ? null : sendInfo.getSendflag());
			if (batchFlag.equals(BatchFlag.SUCCEED)) {// 检查状态
				retBean.setRetCode("OPICSE02");
				retBean.setRetMsg("任务[" + bean.getSendtype() + "]已执行,系统不支持续跑、重跑");
				retBean.setRetStatus(RetStatusEnum.F);
				return retBean;
			}
			logger.debug("自动跑批日终处理" + bean.getSendtype() + "当前状态:" + batchFlag);

			if (("101").equalsIgnoreCase(bean.getSendtype())) {
				// 开始插入日终标识
				retBean = batchRun101(bean, batchFlag);
			} else if (("102").equalsIgnoreCase(bean.getSendtype())) {
				// 开始调OPICS批量
				retBean = batchRun102(bean, batchFlag);
			} else if (("103").equalsIgnoreCase(bean.getSendtype())) {
				// 开始生成总账
				retBean = batchRun103(bean, batchFlag);
			} else if (("104").equalsIgnoreCase(bean.getSendtype())) {
				// 开始发送总账到核心记账
				retBean = batchRun104(bean, batchFlag);
			} else {
				retBean.setRetCode("OPICSE03");
				retBean.setRetMsg("不支持作业标识:" + bean.getSendtype());
				retBean.setRetStatus(RetStatusEnum.F);
				return retBean;
			} // end if ..... else
				// 设置成功状态
			/*retBean.setRetCode("00000000");
			retBean.setRetMsg("任务调度处理成功");*/
			logger.info("自动跑批OPICS0001服务处理完成");
		} catch (Exception e) {
			logger.error("自动跑批处理异常", e);
			retBean.setRetCode("OPICSE04");
			retBean.setRetMsg("自动跑批处理异常");
		}

		return retBean;
	}

	/**
	 * 将系统更新为日终状态
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private SlOutBean batchRun101(SendInfo bean, BatchFlag batchFlag) throws Exception {

		Date workDate = null;

		SlOutBean outBean = new SlOutBean();
		// 获取系统当前日期
		//workDate = commonMapper.getOpicsPrevbranprcDate("01");
		workDate = commonMapper.getOpicsSysDate(bean.getBr());

		if (BatchFlag.NONE.equals(batchFlag) && !bean.getPostdate().equals(DateUtil.format(workDate))) {
			outBean.setRetCode("OPICSE05");
			outBean.setRetMsg("批量日期与系统日期不一致,系统日期:" + DateUtil.format(workDate));
			outBean.setRetStatus(RetStatusEnum.F);
			return outBean;
		}
		if (BatchFlag.NONE.equals(batchFlag)) {// 开始日终标识
			acupMapper.addAcupInfo(createSlAcupSendInfo("101", BatchFlag.SUCCEED, "日终处理完成", bean));

		}
		return new SlOutBean(RetStatusEnum.S, "00000000", "调用日终任务成功");
	}

	/**
	 * 开始执行OPICS BSYS批量
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private SlOutBean batchRun102(SendInfo bean, BatchFlag batchFlag) throws Exception {

		final SendInfo batchInfo = createSlAcupSendInfo("102", BatchFlag.UNPROCESSED, "Opics BSYS等待跑批", bean);

		// 任务未执行,插入待跑批记录
		if (BatchFlag.NONE.equals(batchFlag)) {
			// 插入开始跑批记录
			acupMapper.addAcupInfo(batchInfo);
		}

		// 未执行或者待执行状态，可以再次调用OPICS BSYS批量
		if (BatchFlag.NONE.equals(batchFlag) || BatchFlag.UNPROCESSED.equals(batchFlag)) {
			// 初始化TELNET工具类
//			final SingleeTelnetUtils telnet = new SingleeTelnetUtils("VT220");
			// 使用TELNET登录OPICS服务器
//			telnet.login(config.getTelnetIp(), Integer.valueOf(config.getTelnetPort()), config.getTelnetUser(),
//					config.getTelnetPassword());

			final SingleeJschUtil sshd = new SingleeJschUtil();
			sshd.login(config.getTelnetIp(), Integer.valueOf(config.getTelnetPort()), config.getTelnetUser(),
					config.getTelnetPassword());

			logger.info("自动跑批Telnet登录服务器成功!");
			// 开起一个线程掉用OPICS批量
			new Thread(new Runnable() {
				@Override
				public void run() {
					batchRun102(batchInfo, sshd);
				}
			}).start();
//			batchRun102(batchInfo, telnet);
		}
		// 如果有则进行状态更新
		// 设置BSYS跑批任务调度成功
		return new SlOutBean(RetStatusEnum.S, "00000000", "调用日终任务成功");
	}

	/**
	 * 异步处理OPICS BSYS跑批
	 * 
	 * @param telnet
	 * @throws Exception
	 */
	private void batchRun102(SendInfo bean, SingleeJschUtil sshd) {
		String ret = null;
		BatchFlag batchFlag = null;
		try {

			logger.info("自动跑批SSHD进入自动跑批目录");

			// 查询当前任务状态
			SendInfo sendInfo = new SendInfo();
			sendInfo.setBr(bean.getBr());
			sendInfo.setPostdate(bean.getPostdate());
			sendInfo=acupMapper.getAcupInfo(sendInfo);
			batchFlag = BatchFlag.getByDbValue(sendInfo == null ? null : sendInfo.getSendflag());

			// 如果OPICS BSYS批量在处理中、处理成功、处理失败状态则不允许再次调BSYS，无记录和待处理状态可以再次执行
			if (BatchFlag.PROCESSING.equals(batchFlag) || BatchFlag.SUCCEED.equals(batchFlag)
					|| BatchFlag.ERROR.equals(batchFlag)) {
				logger.warn("自动跑批Opics BSYS批量已开起,不能重复执行!");
				return;
			}

			// 接收调用OPICS BSYS批量的返回日志
			// 切换致OPICS客户端所在的目录
			String commd = config.getTelnetHomeDir() + " && " ;
			if("01".equals(bean.getBr())) {
				commd += config.getTelnetStartBsys();
			}else if("02".equals(bean.getBr())) {
				commd += config.getTelnetStartBsys2();
			}
			
			logger.info("自动跑批SSHD执行命令:" + commd);
			ret = sshd.sendCommand(commd);
			logger.info("自动跑批SSHD执行状态:" + ret);
			String[] str = ret.split("\n");
			StringBuffer sb = new StringBuffer();
			if(str.length>0){
				for(String s : str){
					if(s.contains("failed")){
						sb.append(str).append(" ");
					}
				}
			}
			// 报错处理结果
			bean.setRetmsg(sb.toString());
			// 处理成功
			if (ret.indexOf("successful") != -1) {
				// 如果有则进行状态更新
				acupMapper.updateAcupInfo(createSlAcupSendInfo("102", BatchFlag.SUCCEED, "Opics BSYS跑批成功", bean));
			} else {
				acupMapper.updateAcupInfo(createSlAcupSendInfo("102", BatchFlag.ERROR, "Opics BSYS跑批错误", bean));
			}

		} catch (Exception e) {
			logger.error("自动跑批batchRun102方法异常!", e);
			// 如果有则进行状态更新
			acupMapper.updateAcupInfo(
					createSlAcupSendInfo("102", BatchFlag.ERROR, "Opics BSYS跑批异常" + e.getLocalizedMessage(), bean));
		} finally {
			try {
				if (null != sshd) {
					sshd.distinct();
				}
			} catch (Exception e) {
				logger.error("自动跑批关闭SSHD异常", e);
			}
		} // end try catch
	}

	/**
	 * 异步处理OPICS BSYS跑批
	 * 
	 * @param telnet
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private void batchRun102(SendInfo bean, SingleeTelnetUtils telnet) {
		String ret = null;
		BatchFlag batchFlag = null;
		try {

			logger.info("自动跑批Telnet进入自动跑批目录");
			// 切换致OPICS客户端所在的目录
			ret = telnet.sendCommand(config.getTelnetHomeDir());
			logger.debug("自动跑批Telnet切换目录状态:" + ret);

			logger.info("自动跑批Telnet开始调用Opics BSYS批量");
			logger.debug("自动跑批Telnet开始调用Opics BSYS命令:" + config.getTelnetStartBsys());

			// 查询当前任务状态
			SendInfo sendInfo = new SendInfo();
			sendInfo.setBr(bean.getBr());
			sendInfo.setPostdate(bean.getPostdate());
			sendInfo.setSendtype(bean.getSendtype());
			sendInfo=acupMapper.getAcupInfo(sendInfo);
			batchFlag = BatchFlag.getByDbValue(sendInfo == null ? null : sendInfo.getSendflag());

			// 如果OPICS BSYS批量在处理中、处理成功、处理失败状态则不允许再次调BSYS，无记录和待处理状态可以再次执行
			if (BatchFlag.PROCESSING.equals(batchFlag) || BatchFlag.SUCCEED.equals(batchFlag)
					|| BatchFlag.ERROR.equals(batchFlag)) {
				logger.warn("自动跑批Opics BSYS批量已开起,不能重复执行!");
				return;
			}

			// 接收调用OPICS BSYS批量的返回日志
			ret = telnet.sendCommand(config.getTelnetStartBsys());
			logger.debug("自动跑批Telnet执行状态:" + ret);
			// 报错处理结果
			bean.setRetmsg(ret);
			// 处理成功
			if (ret.indexOf("successful") != -1) {
				// 如果有则进行状态更新
				acupMapper.updateAcupInfo(createSlAcupSendInfo("102", BatchFlag.SUCCEED, "Opics BSYS跑批成功", bean));
			} else {
				acupMapper.updateAcupInfo(createSlAcupSendInfo("102", BatchFlag.ERROR, "Opics BSYS跑批错误", bean));
			}

		} catch (Exception e) {
			logger.error("自动跑批batchRun102方法异常!", e);
			// 如果有则进行状态更新
			acupMapper.updateAcupInfo(
					createSlAcupSendInfo("102", BatchFlag.ERROR, "Opics BSYS跑批异常" + e.getLocalizedMessage(), bean));
		} finally {
			try {
				if (null != telnet) {
					telnet.distinct();
				}
			} catch (Exception e) {
				logger.error("自动跑批关闭TELNET异常", e);
			}
		} // end try catch
	}

	/**
	 * 等OPICS批量执行完成后,开始调用总账存储过程,生成当日总账信息
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private SlOutBean batchRun103(SendInfo bean, BatchFlag batchFlag) throws Exception {
		final SendInfo batchInfo = createSlAcupSendInfo("103", BatchFlag.UNPROCESSED, "准备生成总账信息", bean);

		// 总账生成未找到记录时,开始调用总账生成程序
		if (BatchFlag.NONE.equals(batchFlag)) {
			// 插入总账生成程序为待处理状态
			acupMapper.updateAcupInfo(batchInfo);
		} // enf if

		// 无记录、待处理、异常状态允许调度平台重跑续跑操作
		if (BatchFlag.NONE.equals(batchFlag) || BatchFlag.UNPROCESSED.equals(batchFlag)
				|| BatchFlag.ERROR.equals(batchFlag)) {
			// 开起一个线程掉用总账生成程序
			new Thread(new Runnable() {
				@Override
				public void run() {
					batchRun103(batchInfo);
				}
			}).start();
			//batchRun103(batchInfo);
		}

		// 设置生成总账处理成功
		return new SlOutBean(RetStatusEnum.S, "00000000", "调用日终任务成功");
	}

	/**
	 * 线程处理总账存储过程
	 * 
	 * @param map
	 * @param bean
	 */
	private void batchRun103(SendInfo bean) {
		SlOutBean retMap = null;
		Map<String, String> map = null;
		try {
			map = new HashMap<String, String>();

			// 设置总账生成所需要的参数
			map.put("br", bean.getBr());
			logger.info("自动跑批生成总账日期:" + DateUtil.parse(bean.getPostdate()));
			map.put("acupDate", bean.getPostdate());

			// 创建SL_ACUP总账并接收返回信息
			acupMapper.acupCreate(map);

			logger.debug("自动跑批生成账信息返回值:" + map);

			/*
			 * 如果总账处理失败,更新表状态为失败状态 999处理成功,其它状态为失败
			 */
			if (null != map && "999".equalsIgnoreCase(map.get("RETCODE"))) {
				SendInfo sendInfo = new SendInfo();
				sendInfo.setBr(bean.getBr());
				sendInfo.setPostdate(bean.getPostdate());
				sendInfo.setSendtype(bean.getSendtype());
				if (BatchFlag.NONE.equals(BatchFlag.getByDbValue(acupMapper.getAcupInfo(sendInfo).getSendflag()))) {
					// 如果没有则添加失败状态
					SendInfo batchInfo = createSlAcupSendInfo("103", BatchFlag.ERROR,
							"总账生成失败,RETCODE:" + map.get("RETCODE") + ",RETMSG:" + map.get("RETMSG"), bean);
					acupMapper.updateAcupInfo(batchInfo);
				} else {
					// 如果有则进行状态更新
					SendInfo batchInfo = createSlAcupSendInfo("103", BatchFlag.SUCCEED,
							"总账生成成功,RETCODE:" + map.get("RETCODE") + ",RETMSG:" + map.get("RETMSG"), bean);
					acupMapper.updateAcupInfo(batchInfo);
				}
			} else{
				SendInfo batchInfo = createSlAcupSendInfo("103", BatchFlag.ERROR,
						"总账生成失败,RETCODE:" + map.get("RETCODE") + ",RETMSG:" + map.get("RETMSG"), bean);
				acupMapper.updateAcupInfo(batchInfo);
			}// end if
		} catch (Exception e) {
			logger.error("自动跑批创建总账信息异常", e);
			// 总账处理异常更新状态
			acupMapper.updateAcupInfo(
					createSlAcupSendInfo("103", BatchFlag.ERROR, "创建总账信息异常," + e.getLocalizedMessage(), bean));
		}
	}

	/**
	 * 待总账信息生成完成后,发始逐笔发送到核心进行记账
	 * 
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	private SlOutBean batchRun104(SendInfo bean, BatchFlag batchFlag) throws Exception {

		final SendInfo batchInfo = createSlAcupSendInfo("104", BatchFlag.PROCESSING, "开始发送总账", bean);

		// 查询总账发送状态,无记录状态时调用总账发送程序
		if (BatchFlag.NONE.equals(batchFlag)) {
			acupMapper.updateAcupInfo(batchInfo);
		}

		// 无记录、待处理、异常状态允许调度平台重跑续跑操作
		if (BatchFlag.NONE.equals(batchFlag) || BatchFlag.UNPROCESSED.equals(batchFlag)
				|| BatchFlag.ERROR.equals(batchFlag)) {
			// 开起一个线程掉用OPICS批量
			new Thread(new Runnable() {
				@Override
				public void run() {
					batchRun104(batchInfo);
				}
			}).start();
		}

		return new SlOutBean(RetStatusEnum.S, "00000000", "调用衍生品日终任务成功");

	}

	/**
	 * 线程运行总账记账
	 * 
	 * @param map
	 * @param bean
	 */
	private void batchRun104(SendInfo bean) {
		Map<String, String> map = new HashMap<String, String>();
		SendInfo batchInfo = null;
		try {
			logger.debug("开始像核心发送总账");
			map.put("deal_flag", "0");
			map.put("br", bean.getBr());
			logger.info("自动跑批当前批量日期:" + DateUtil.parse(bean.getPostdate()));
			map.put("acupDate", bean.getPostdate());

			// 发送SL_ACUP总账
			SlAcupTaxBean acupBean = new SlAcupTaxBean();
			acupBean.setPostdate(bean.getPostdate());
			SlOutBean retMsg = CallSpringBeanUtils.call("iSendAcupServer", "sendAcup", SlOutBean.class, acupBean);

			logger.info("核心记账处理完成");

			if (RetStatusEnum.S.name().equalsIgnoreCase(retMsg.getRetCode())) {
				// 总账记录发送完成
				batchInfo = createSlAcupSendInfo("104", BatchFlag.SUCCEED, "总账记账完成", bean);
			} else {
				// 总账记录发送异常
				batchInfo = createSlAcupSendInfo("104", BatchFlag.ERROR, "总账记账失败," + retMsg.getRetMsg(), bean);
			}
			// 更新总账状 态
			acupMapper.updateAcupInfo(batchInfo);

			logger.info("更新核心记账状态完成");
		} catch (Exception e) {
			logger.error("发送总账线程运行异常", e);
			// 更新总账状 态
			acupMapper.updateAcupInfo(
					createSlAcupSendInfo("104", BatchFlag.ERROR, "发送总账线程运行异常," + e.getLocalizedMessage(), bean));
		}
	}

	/**
	 * 创建SL_ACUP_SENDINFO对象
	 * 
	 * @throws Exception
	 */
	private SendInfo createSlAcupSendInfo(String name, BatchFlag batchFalg, String note, SendInfo bean) {
		bean.setTypename(name);
		bean.setSendflag(batchFalg.name());
		bean.setNote(note);
		bean.setRetmsg(note);
		bean.setInputdate(DateUtil.getCurrentDateTimeAsString());
		bean.setLastdate(DateUtil.getCurrentDateTimeAsString());
		return bean;
	}

	/**
	 * 创建SL_ACUP_SENDINFO对象
	 * 
	 * @throws Exception
	 */
	private SendInfo createSlAcupSendInfo(String br, String postdate, String sendtype) {
		SendInfo bean = new SendInfo();
		bean.setBr(br);
		bean.setPostdate(postdate);
		bean.setSendtype(sendtype);
		return bean;
	}

	@Override
	public SlOutBean callAutoBsys(List<String> brs, String postdate, String jobId)
			throws RemoteConnectFailureException, Exception {
		SlOutBean outBean=new SlOutBean();
		if (("101").equalsIgnoreCase(jobId)||("102").equalsIgnoreCase(jobId)||("103").equalsIgnoreCase(jobId))  {
			for (String br : brs) {
				outBean=callAutoBsys(br, postdate, jobId);
				if(!"00000000".equals(outBean.getRetCode())) {
					return outBean;
				}
			}
		}else if(("104").equalsIgnoreCase(jobId)) {
			List<SendInfo> list =new ArrayList<SendInfo>();
			SendInfo bean=null;
			BatchFlag batchFlag=null;
			for (String br : brs) {
				bean = createSlAcupSendInfo(br, postdate, jobId);
				// 查询当前任务状态
				SendInfo sendInfo = new SendInfo();
				sendInfo.setBr(bean.getBr());
				sendInfo.setPostdate(bean.getPostdate());
				sendInfo.setSendtype(bean.getSendtype());
				logger.debug("自动跑批日终处理" + bean.getSendtype() + "查询条件:" + bean);
				sendInfo = acupMapper.getAcupInfo(sendInfo);
				batchFlag = BatchFlag.getByDbValue(sendInfo == null ? null : sendInfo.getSendflag());
				if (batchFlag.equals(BatchFlag.SUCCEED)) {// 检查状态
					outBean.setRetCode("OPICSE02");
					outBean.setRetMsg("任务[" + bean.getSendtype() + "]已执行,系统不支持续跑、重跑");
					outBean.setRetStatus(RetStatusEnum.F);
					return outBean;
				}
				logger.debug("自动跑批日终处理" + bean.getSendtype() + "当前状态:" + batchFlag);
				if(sendInfo==null) {
					SendInfo sendBean = new SendInfo();
					sendBean.setBr(bean.getBr());
					sendBean.setPostdate(bean.getPostdate());
					sendBean.setSendtype(bean.getSendtype());
					list.add(sendBean);
				}else {
					sendInfo.setPostdate(sendInfo.getPostdate().substring(0,10));
					list.add(sendInfo);
				}
				
			}
			outBean=batchRun104(list, batchFlag);
			if(!"00000000".equals(outBean.getRetCode())) {
				return outBean;
			}
		}
		return outBean;
	}
	
	private SlOutBean batchRun104(final List<SendInfo> list, BatchFlag batchFlag) throws Exception {
		for (SendInfo bean : list) {
			final SendInfo batchInfo = createSlAcupSendInfo("104", BatchFlag.PROCESSING, "开始发送总账", bean);

			// 查询总账发送状态,无记录状态时调用总账发送程序
			if (BatchFlag.NONE.equals(batchFlag)) {
				acupMapper.updateAcupInfo(batchInfo);
			}
			// 无记录、待处理、异常状态允许调度平台重跑续跑操作
			if (!(BatchFlag.NONE.equals(batchFlag) || BatchFlag.UNPROCESSED.equals(batchFlag)
					|| BatchFlag.ERROR.equals(batchFlag))) {
				return new SlOutBean(RetStatusEnum.F, "singleeinfo", "调用日终任务失败");
			}
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				batchRun104(list);
			}
		}).start();
		
		return new SlOutBean(RetStatusEnum.S, "00000000", "调用日终任务成功");

	}
	private void batchRun104(List<SendInfo> list) {
		SendInfo batchInfo = null;
		try {
			logger.debug("开始发送总账");
			logger.info("自动跑批当前批量日期:" + DateUtil.parse(list.get(0).getPostdate()));
			// 发送SL_ACUP_TAX总账
			SlAcupTaxBean acupBean = new SlAcupTaxBean();
			acupBean.setPostdate(list.get(0).getPostdate());
			List<String> brList=new ArrayList<String>();
			for (SendInfo sendInfo : list) {
				brList.add(sendInfo.getBr());
			}
			acupBean.setBrList(brList);
			logger.info("acup send ing");
			SlOutBean retMsg = CallSpringBeanUtils.call("iSendAcupServer", "sendAcup", SlOutBean.class, acupBean);
			logger.info("acup send ok:"+retMsg);
			if ("0000".equalsIgnoreCase(retMsg.getRetCode())) {
				logger.info("发送总账完成");
				// 总账记录发送完成
				for (SendInfo bean : list) {
					batchInfo = createSlAcupSendInfo("104", BatchFlag.SUCCEED, "总账发送完成", bean);
					// 更新总账状 态
					acupMapper.updateAcupInfo(batchInfo);
				}
				
			} else {
				// 总账记录发送异常
				for (SendInfo bean : list) {
					batchInfo = createSlAcupSendInfo("104", BatchFlag.ERROR, "总账记账失败," + retMsg.getRetMsg(), bean);
					// 更新总账状 态
					acupMapper.updateAcupInfo(batchInfo);
				}
				
			}
			

			logger.info("更新核心记账状态完成");
		} catch (Exception e) {
			logger.error("发送总账线程运行异常", e);
			// 更新总账状 态
			for (SendInfo bean : list) {
				acupMapper.updateAcupInfo(
						createSlAcupSendInfo("104", BatchFlag.ERROR, "发送总账线程运行异常," + e.getLocalizedMessage(), bean));
			}
			
		}
	}
	

}
