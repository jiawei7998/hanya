package com.singlee.financial.opics;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.singlee.financial.bean.SlOpcKpsjBean;
import com.singlee.financial.mapper.OpcKpsjMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OpcKpsjServer implements IOpcKpsjServer {
	@Autowired
	OpcKpsjMapper opcMapper;
	
	@Override
	public List<SlOpcKpsjBean> selectOpcKpsjDldt(String datetime) {
		return opcMapper.selectOpcKpsjDldt(datetime);
	}

	@Override
	public List<SlOpcKpsjBean> selectOpcKpsjRepo(String datetime) {
		return opcMapper.selectOpcKpsjRepo(datetime);
	}

	@Override
	public List<SlOpcKpsjBean> selectOpcKpsjSPsh(String datetime) {
		return opcMapper.selectOpcKpsjRepo(datetime);
	}

	@Override
	public List<SlOpcKpsjBean> selectOpcKpsjSPshGain(String datetime) {
		return opcMapper.selectOpcKpsjSPshGain(datetime);
	}

	@Override
	public SlOpcKpsjBean selectOpcKpsjCountGain(Map<String,Object> map) {
		return opcMapper.selectOpcKpsjCountGain(map);
	}

	@Override
	public List<SlOpcKpsjBean> selectOpcKpsjByDelno(String dealno) {
		return opcMapper.selectOpcKpsjByDelno(dealno);
	}

}
