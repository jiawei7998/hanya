package com.singlee.financial.bean;

public enum BatchFlag {

	/**
	 * I-未执行
	 */
	UNPROCESSED(73,"未执行"),
	
	/**
	 * N-无记录
	 */
	NONE(78,"无记录"),
	
	/**
	 * R-正在执行
	 */
	PROCESSING(82,"正在执行"),	
	
	/**
	 * E-错误
	 */
	ERROR(69,"错误"),
	
	/**
	 * S-成功
	 */
	SUCCEED(83,"成功");

	/**
	 * 枚举值
	 */
	private int value;
	
	/**
	 * 枚举值描述
	 */
	private String descr;
	
	private BatchFlag(int value,String descr){
		this.value = value;
		this.descr = descr;
	}
	
	public static BatchFlag getByDbValue(String dbValue) {
		BatchFlag em = null;
		if(null == dbValue || dbValue.trim().length() == 0){
			return BatchFlag.NONE;
		}
		for (BatchFlag var : BatchFlag.values()) {
			if(dbValue.equalsIgnoreCase(var.name())){
				em = var;
			}//end if
		}//end for
		return em;
	}

	public String getStrValue() {
		return String.valueOf(((char)value));
	}
	
	public char getValue() {
		return (char)value;
	}

	public String getDescr() {
		return descr;
	}
	
	@Override  
    public String toString() {  
        return getStrValue()+"-"+getDescr();  
    } 
}
