package com.singlee.financial.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class IselBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SlInthBean inthBean;
	private String secid;
	private BigDecimal clsgprice8;
	private BigDecimal askprice8;
	private BigDecimal bidprice8;
	private BigDecimal clsgyield8;
	private BigDecimal askyield8;
	private BigDecimal bidyield8;
	private BigDecimal offclsgprice8;
	private BigDecimal offclsgpriceyst8;
	private String lstmntDate;
	//市场数据查询所需字段
	private String feidate;
	private String statcode;
	private String errorcode;
	
	
	public BigDecimal getBidyield8() {
		return bidyield8;
	}
	public void setBidyield8(BigDecimal bidyield8) {
		this.bidyield8 = bidyield8;
	}
	public SlInthBean getInthBean() {
		return inthBean;
	}
	public void setInthBean(SlInthBean inthBean) {
		this.inthBean = inthBean;
	}
	public String getLstmntDate() {
		return lstmntDate;
	}
	public void setLstmntDate(String lstmntDate) {
		this.lstmntDate = lstmntDate;
	}
	
	public String getSecid() {
		return secid;
	}
	public void setSecid(String secid) {
		this.secid = secid;
	}
	public BigDecimal getClsgprice8() {
		return clsgprice8;
	}
	public void setClsgprice8(BigDecimal clsgprice8) {
		this.clsgprice8 = clsgprice8;
	}
	public BigDecimal getAskprice8() {
		return askprice8;
	}
	public void setAskprice8(BigDecimal askprice8) {
		this.askprice8 = askprice8;
	}
	public BigDecimal getBidprice8() {
		return bidprice8;
	}
	public void setBidprice8(BigDecimal bidprice8) {
		this.bidprice8 = bidprice8;
	}
	public BigDecimal getClsgyield8() {
		return clsgyield8;
	}
	public void setClsgyield8(BigDecimal clsgyield8) {
		this.clsgyield8 = clsgyield8;
	}
	public BigDecimal getAskyield8() {
		return askyield8;
	}
	public void setAskyield8(BigDecimal askyield8) {
		this.askyield8 = askyield8;
	}
	public BigDecimal getOffclsgprice8() {
		return offclsgprice8;
	}
	public void setOffclsgprice8(BigDecimal offclsgprice8) {
		this.offclsgprice8 = offclsgprice8;
	}
	public BigDecimal getOffclsgpriceyst8() {
		return offclsgpriceyst8;
	}
	public void setOffclsgpriceyst8(BigDecimal offclsgpriceyst8) {
		this.offclsgpriceyst8 = offclsgpriceyst8;
	}
	public String getFeidate() {
		return feidate;
	}
	public void setFeidate(String feidate) {
		this.feidate = feidate;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
}
