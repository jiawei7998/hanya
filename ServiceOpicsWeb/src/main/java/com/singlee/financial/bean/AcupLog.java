package com.singlee.financial.bean;

import java.io.Serializable;

public class AcupLog implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5902927615667660372L;
	private String module;
	private String msgtype;
	private String direction;
	private String message;
	private String serseqno;
	private String systime;
	private String filename;
	private String opicsdate;
	private String postdate;
	private String opername;

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSerseqno() {
		return serseqno;
	}

	public void setSerseqno(String serseqno) {
		this.serseqno = serseqno;
	}

	public String getSystime() {
		return systime;
	}

	public void setSystime(String systime) {
		this.systime = systime;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getOpicsdate() {
		return opicsdate;
	}

	public void setOpicsdate(String opicsdate) {
		this.opicsdate = opicsdate;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	public String getOpername() {
		return opername;
	}

	public void setOpername(String opername) {
		this.opername = opername;
	}

}
