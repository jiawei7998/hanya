package com.singlee.financial.common.spring.mybatis;

import java.io.ByteArrayInputStream;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;
import org.apache.ibatis.session.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.singlee.financial.common.util.JY;

public class SlBatisLanguageDriver extends XMLLanguageDriver{


	private ScriptEngineManager manager = new ScriptEngineManager(); 
	private ScriptEngine engine;
	
	@Override
	public SqlSource createSqlSource(Configuration configuration, XNode script,
			Class<?> parameterType) {
		if(engine == null){
			engine =  manager.getEngineByName("js");
		}
		parse(script);
        return super.createSqlSource(configuration,script,parameterType);
	}
	
	public void parse(XNode script){
		if(script.getNode().getNodeName().contains("if") && script.getNode().getTextContent().contains("##")){
			String scriptScript=script.getNode().getTextContent();
			scriptScript = scriptScript.substring(2);
			String domString = null;
			try {
				domString = engine.eval(scriptScript).toString();
				//得到DOM解析器的工厂实例
	            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	            //从DOM工厂中获得DOM解析器
	            DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
	            ByteArrayInputStream is = new ByteArrayInputStream(domString.getBytes());
	            Document doc = dbBuilder.parse(is);
	            NodeList nList = doc.getChildNodes().item(0).getChildNodes();
	            for(int i = 0; i< nList.getLength() ; i ++){
	            	Node node = (Node)nList.item(i);
		            Node newNode = script.getNode().getOwnerDocument().importNode(node, true);
		            script.getNode().getParentNode().insertBefore(newNode, script.getNode());//.appendChild(newNode);
	            } 
			} catch (ScriptException e) {
				JY.raise("脚本解析异常："+script.getNode().getTextContent(),e);
			}catch (Exception e) {
				JY.raise("Node解析异常："+script.getNode().getTextContent()+"="+domString,e);
	        }
		}
		for(XNode node:script.getChildren()){
			parse(node);
		}
	}
}
