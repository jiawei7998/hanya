package com.singlee.financial.common.util;

public class FundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1321421012707161321L;

	public FundException(Object object) {
		super(object.toString());
	}

}
