package com.singlee.financial.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class FileReadAndWriteUtil {
	/**
	 * 递归删除文件夹
	 * 
	 * @param path
	 */
	public static void delDir(String path) {
		File dir = new File(path);

		if (dir.exists()) {

			File[] tmp = dir.listFiles();

			for (int i = 0; i < tmp.length; i++) {

				if (tmp[i].isDirectory()) {

					delDir(path + "/" + tmp[i].getName());

				} else {

					tmp[i].delete();

				}
			}
			dir.delete();
		}
	}

	/**
	 * 删除文件
	 * 
	 * @param path
	 *            目录
	 * @param filename
	 *            文件名
	 */
	public static void delFile(String path) {

		File file = new File(path);

		if (file.exists() && file.isFile()) {
			file.delete();
		}
		else if (file.isDirectory()) {
			file.delete();
		}
	}

	/**
	 * 创建新文件
	 * 
	 * @param path
	 *            目录
	 * @param filename
	 *            文件名
	 * @throws IOException
	 */
	public static void createFile(String path, String filename) throws IOException {

		File file = new File(path + "/" + filename);

		if (!file.exists()) {
			file.createNewFile();
		}
	}

	/**
	 * 创建文件夹
	 * 
	 * @param path
	 *            目录
	 */
	public static void createDir(String path) {

		File dir = new File(path);

		if (!dir.exists()) {
			dir.mkdir();
		}
	}

	/**
	 * 读取文件内容
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public static String readFileFromPath(String path) throws IOException {
		File file = new File(path);

		if (!file.exists() || file.isDirectory()) {
			throw new FileNotFoundException();
		}
		if (file.length() <= 0) {
			return null;
		}
		BufferedReader br = new BufferedReader(new FileReader(file));

		String temp = null;

		StringBuffer sb = new StringBuffer();

		temp = br.readLine();

		while (temp != null) {

			sb.append(temp + "\n");

			temp = br.readLine();
		}
		br.close();
		return sb.toString();

	}

	/**
	 * 文件重命名
	 * 
	 * @param path
	 *            文件目录
	 * @param oldname
	 *            原来的文件名
	 * @param newname
	 *            新文件名
	 */
	public void renameFile(String path, String oldname, String newname) {

		if (!oldname.equals(newname)) {// 新的文件名和以前文件名不同时,才有必要进行重命名

			File oldfile = new File(path + "/" + oldname);

			File newfile = new File(path + "/" + newname);

			if (newfile.exists())// 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
            {
                return;
            } else {
				oldfile.renameTo(newfile);
			}
		}
	}

	/**
	 * 保存内容到指定的path下的文件中
	 * 
	 * @param str
	 *            需要保存的内容
	 * @param path
	 *            文件路径
	 * @throws Exception
	 */
	public static void saveSqlStrToFile(String str, String path) throws Exception {
		FileOutputStream fos = new FileOutputStream(new File(path));

		OutputStreamWriter osw = new OutputStreamWriter(fos);

		osw.write(str);

		osw.flush();

		osw.close();

	}

	/**
	 * 获取递归文件路径
	 * 
	 * @param paramFile
	 * @param fileJson
	 * @return
	 */
	public static void getFilePath(File paramFile, StringBuffer fileJson) {
		if (paramFile.isFile()) {// 是文件，添加到文件列表中，本次调用结束，返回文件列表
			File parentFile = new File(paramFile.getParent());

			fileJson.append("{id:'" + paramFile.getName() + "',text:'" + paramFile.getName() + "',pid:'" + parentFile.getName() + "'},");
		} else {// 是目录
			fileJson.append("{id:'" + paramFile.getName() + "',text:'" + paramFile.getName() + "'");
			if (!"Report".equals(paramFile.getName())) {
				fileJson.append(",pid:'Report'},");
			} else {
				fileJson.append("},");
			}
			File[] localFiles = paramFile.listFiles();// 得到目录下的子文件数组
			if (localFiles != null) {// 目录不为空
				for (File localFile : localFiles) {// 遍历子文件数组
					getFilePath(localFile, fileJson);// 调用该函数本身
				}
			}
			// 为空目录，本次调用结束，返回文件列表
		}
	}
}
