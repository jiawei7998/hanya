package com.singlee.financial.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;


/**
 * 字符串工具类
 * 
 * @author Gaolb
 */

public class StringUtil extends StringUtils {

	private static Pattern SUB4_PATTERN = Pattern.compile("\\$\\{\\w+\\}");
	private static Pattern SUB3_PATTERN = Pattern.compile("#\\{\\w+\\}");
	private static Pattern LOWER_PATTERN = Pattern.compile("_[a-z]");
	/**
	 * t1>t2返回true
	 * 
	 * @param t1
	 *            "yyyy-MM-dd"
	 * @param t2
	 *            "yyyy-MM-dd"
	 * @return
	 */
	public static boolean compareTime(String t1, String t2) {
		boolean flag = false;
		if (t1 != null && !"".equals(t1) && t2 != null && !"".equals(t2)) {
			int date1 = Integer.parseInt(t1.replace("-", ""));
			int date2 = Integer.parseInt(t2.replace("-", ""));

			if (date1 > date2) {
				flag = true;
			} else {
				flag = false;
			}
			return flag;

		} else {
			return flag;
		}

	}

	public static String leftPad(int value, int size, char padChar) {
		String str = String.valueOf(value);
		str = leftPad(str, size, padChar);
		return str;
	}

	public static String rightPad(int value, int size, char padChar) {
		String str = String.valueOf(value);
		str = rightPad(str, size, padChar);
		return str;
	}

	/**
	 * @param src
	 * @param len
	 * @param fillWith
	 * @return 把src串用fillwith左补足到len长 如果用空格补的话，"" 等同于 " "
	 * 
	 * @author Gaolb
	 * @date 2014-7-8
	 */
	public static String leftFillStr(String src, int len, String fillWith) {
		if ("".equals(fillWith)) {
			fillWith = " ";}
		byte fill = fillWith.getBytes()[0];
		return leftFillStr(src, len, fill);
	}

	/**
	 * @param src
	 * @param len
	 * @param fillWith
	 * @return 把src串用fillwith左补足到len长
	 * 
	 * @author Gaolb
	 * @date 2014-7-8
	 */
	public static String leftFillStr(String src, int len, byte fillWith) {
		try {
			byte[] srcbyte;
			if (src != null) {
				srcbyte = src.getBytes("GBK");
			} else {
				srcbyte = new byte[0];
			}

			if (srcbyte.length >= len) {
				return src;
			}
			byte[] tmp = new byte[len];
			int fulllen = len - srcbyte.length;
			for (int i = 0; i < fulllen; i++) {
				tmp[i] = fillWith;
			}
			System.arraycopy(srcbyte, 0, tmp, fulllen, srcbyte.length);
			return new String(tmp, "GBK");
		} catch (UnsupportedEncodingException e) {
			return "";
		}
	}

	/**
	 * 字符串连接
	 * 
	 * @param ss
	 * @return
	 * @author Gaolb
	 * @date 2014-7-8
	 */
	public static String connect(String... ss) {
		StringBuffer sb = new StringBuffer();
		for (String s : ss) {
			sb.append(s);
		}
		return sb.toString();
	}

	public static String addQuote(String str) {
		if (!"NULL".equalsIgnoreCase(str)) {
			str = "'" + encodeSingleQuotedString(str) + "'";
		}
		return str;
	}

	public static String leftQuote(String str) {
		str = "'" + encodeSingleQuotedString(str);

		return str;
	}

	public static String rightQuote(String str) {
		str = encodeSingleQuotedString(str) + "'";

		return str;
	}

	public static String encodeSingleQuotedString(String str) {
		if (isNotEmpty(str)) {
			StringBuffer sb = new StringBuffer(64);
			for (int i = 0; i < str.length(); i++) {
				char c = str.charAt(i);
				if (c == '\'') {
					sb.append("''");
				}
				else {
					sb.append(c);
				}
			}
			return sb.toString();
		}

		return str;
	}

	public static String formatDouble(double value) {
		String str = "0.0";
		if (BigDecimal.valueOf(value).signum() != 0) {
			DecimalFormat nf = new DecimalFormat("#.##");
			nf.setParseIntegerOnly(false);
			nf.setDecimalSeparatorAlwaysShown(false);
			str = nf.format(value);
		}
		return str;
	}

	public static String formatDouble(double value, int digitNum) {
		String str = "0.0";

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < digitNum; i++) {
			sb.append("0");
		}
		DecimalFormat nf = new DecimalFormat("#." + sb.toString());
		nf.setParseIntegerOnly(false);
		nf.setDecimalSeparatorAlwaysShown(false);
		str = nf.format(value);
		if (isEmpty(str.split("\\.")[0])) {
			str = "0" + str;}
		return str;
	}

	@SuppressWarnings( { "rawtypes", "unchecked" })
	public static String[] tokenize(String str, String delimiter) {
		ArrayList v = new ArrayList();

		StringTokenizer t = new StringTokenizer(str, delimiter);

		while (t.hasMoreTokens()) {
			String s = t.nextToken();
			if (isNotEmpty(s)) {
				v.add(s.trim());
			}
		}

		String[] pro = new String[v.size()];
		for (int i = 0; i < pro.length; i++) {
			pro[i] = ((String) (String) v.get(i));
		}
		return pro;
	}

	public static void byte2hex(byte b, StringBuffer buf) {
		char[] hexChars = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };

		int high = (b & 0xF0) >> 4;
		int low = b & 0xF;

		buf.append(hexChars[high]);
		buf.append(hexChars[low]);
	}

	public static String toHexString(byte[] block) {
		StringBuffer buf = new StringBuffer(64);

		int len = block.length;

		for (int i = 0; i < len; i++) {
			byte2hex(block[i], buf);
		}

		return buf.toString();
	}

	public static String addFlagQuote(String str) {
		StringBuffer sbFlag = new StringBuffer(16);

		String[] flags = tokenize(str, ",");

		int count = flags.length;
		for (int i = 0; i < count; i++) {
			if (i != 0) {
				sbFlag.append(",");
			}

			sbFlag.append("'" + flags[i] + "'");
		}

		return sbFlag.toString();
	}

	public static String getData(String resouce, String label) {
		String result = "";
		String labelB = "";
		String labelE = "";

		int site1 = 0;
		int site2 = 0;

		if ((resouce == null) || (label == null)) {
			return result;
		}

		resouce = resouce.trim();
		labelB = "<" + label + ">";
		labelE = "</" + label + ">";

		site1 = resouce.indexOf(labelB) + labelB.length();
		site2 = resouce.indexOf(labelE);

		if ((site1 < 0) || (site2 < 0)) {
			return "";
		}

		result = resouce.substring(site1, site2);

		return result;
	}

	public static String convertToGB(String str) {
		String result = str;
		String charCodeOld = "8859_1";
		String charCodeNew = "GBK";

		if (isNotEmpty(result)) {
			try {
				byte[] bytes = result.getBytes(charCodeOld);
				result = new String(bytes, charCodeNew);
			} catch (Exception ex) {
				throw new RException(ex);
			}
		}

		return result;
	}

	public static String convertToISO(String str) {
		String result = str;
		String charCodeOld = "GBK";
		String charCodeNew = "8859_1";

		if (isNotEmpty(result)) {
			try {
				byte[] bytes = result.getBytes(charCodeOld);
				result = new String(bytes, charCodeNew);
			} catch (Exception ex) {
				throw new RException(ex);
			}
		}
		return result;
	}

	public static String fomatHtmlText(String reCeiv) {
		String cMesg = "";
		String sMesg = "";

		for (int ii = 0; ii < reCeiv.length(); ii++) {
			cMesg = reCeiv.substring(ii, ii + 1);
			if ("\n".compareTo(cMesg) == 0) {
				sMesg = sMesg + "<br>";
			} else if (" ".compareTo(cMesg) == 0) {
				sMesg = sMesg + "&nbsp";
			} else {
				sMesg = sMesg + cMesg;
			}
		}
		return sMesg;
	}

	public static String toUtf8String(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if ((c >= 0) && (c <= 'ÿ')) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					System.out.println(ex);
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0) {
						k += 256;
					}
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}

		return sb.toString();
	}

	public static String getFormatDate(String s) {
		String s_date = "";
		if (s.trim().length() == 14) {
			s_date = s.substring(0, 4) + "-" + s.substring(4, 6) + "-"
					+ s.substring(6, 8);}
		else {
			s_date = s;}
		return s_date;
	}

	public static String getPriorDay(int offset) {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

		Calendar theday = Calendar.getInstance();
		theday.add(5, offset);

		df.applyPattern("yyyyMMdd");
		return df.format(theday.getTime());
	}

	public static String trim(String s) {
		if ((s == null) || ("".equalsIgnoreCase(s))
				|| ("null".equalsIgnoreCase(s))) {
			return "";
		}
		return s.trim();
	}

	public static String getStrSysYear() {
		Calendar getTime = Calendar.getInstance(Locale.CHINA);
		return Integer.toString(getTime.get(1));
	}

	public static int getIntSysYear() {
		Calendar getTime = Calendar.getInstance(Locale.CHINA);
		return getTime.get(1);
	}

	public static int getIntSysMonth() {
		Calendar getTime = Calendar.getInstance(Locale.CHINA);
		return getTime.get(2) + 1;
	}

	public static String getStrSysDay() {
		Calendar getTime = Calendar.getInstance(Locale.CHINA);
		return Integer.toString(getTime.get(5));
	}

	public static int getIntSysDay() {
		Calendar getTime = Calendar.getInstance(Locale.CHINA);
		return getTime.get(5);
	}

	public static String getFileModifyTime(String fileName) {
		File fn = new File(fileName);

		Long.toString(fn.lastModified());
		SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar theday = Calendar.getInstance();
		theday.setTimeInMillis(fn.lastModified());
		df.applyPattern("yyyy/MM/dd HH:mm:ss");
		return df.format(theday.getTime());
	}

	public static String getLikeString(String colName, String colValue) {
		String out = "";
		out = colName + " Like '%" + colValue + "%'";
		return out;
	}

	public static String getUpperInteger(int in) {
		String out = "";
		int tmp = in;
		while (true) {
			int tmp1 = tmp % 10;
			switch (tmp1) {
			case 1:
				out = "一" + out;
				break;
			case 2:
				out = "二" + out;
				break;
			case 3:
				out = "三" + out;
				break;
			case 4:
				out = "四" + out;
				break;
			case 5:
				out = "五" + out;
				break;
			case 6:
				out = "六" + out;
				break;
			case 7:
				out = "七" + out;
				break;
			case 8:
				out = "八" + out;
				break;
			case 9:
				out = "九" + out;
			}
			tmp /= 10;
			if (tmp == 0) {
				break;}
		}
		return out;
	}

	public static String changeNullToEmpty(String str) {
		if (isEmpty(str)) {
			return "";
		}

		return str;
	}

	public static String convertURL(String URL) {
		if (isEmpty(URL)) {
			return "";
		}

		return URL.replace('\\', '/');
	}

	public static String byteToString(byte b) {
		byte maskHigh = -16;
		byte maskLow = 15;

		byte high = (byte) ((b & maskHigh) >> 4);
		byte low = (byte) (b & maskLow);

		StringBuffer buf = new StringBuffer();
		buf.append(findHex(high));
		buf.append(findHex(low));

		return buf.toString();
	}

	private static char findHex(byte b) {
		int t = new Byte(b).intValue();
		t = t < 0 ? t + 16 : t;

		if ((t >= 0) && (t <= 9)) {
			return (char) (t + 48);
		}

		return (char) (t - 10 + 65);
	}

	public static int stringToByte(String in, byte[] b) {
		if (b.length < in.length() / 2) {
			return 0;
		}

		int j = 0;
		StringBuffer buf = new StringBuffer(2);
		for (int i = 0; i < in.length(); j++) {
			buf.insert(0, in.charAt(i));
			buf.insert(1, in.charAt(i + 1));
			int t = Integer.parseInt(buf.toString(), 16);

			b[j] = ((byte) t);
			i++;
			buf.delete(0, 2);

			i++;
		}

		return j;
	}

	public static byte[] hex2Bytes(String hexString) {
		if (hexString == null) {
			return null;
		}

		if (hexString.length() % 2 != 0) {
			hexString = '0' + hexString;
		}

		byte[] result = new byte[hexString.length() / 2];

		for (int i = 0; i < result.length; i++) {
			result[i] = ((byte) Integer.parseInt(hexString.substring(i * 2,
					(i + 1) * 2), 16));
		}

		return result;
	}

	public static String bytes2Hex(byte[] bytes) {
		StringBuffer result = new StringBuffer("");
		if (bytes == null) {
			return "";
		}

		for (int i = 0; i < bytes.length; i++) {
			result.append(padding2Head(Integer.toHexString(bytes[i] & 0xFF),
					'0', 2));
		}
		return result.toString();
	}

	public static String padding2Head(String s, char ch, int destLength) {
		StringBuffer str = null;
		if (destLength < 0) {
			return "";
		}
		if (s == null) {
			str = new StringBuffer();
			for (int i = 0; i < destLength; i++) {
				str.append(ch);
			}
		} else {
			if (s.length() > destLength) {
				return "";
			}

			str = new StringBuffer();
			for (int i = 0; i < destLength - s.length(); i++) {
				str.append(ch);
			}
			str.append(s);
		}

		return str.toString();
	}

	/**
	 * 通过HttpServletRequest返回IP地址
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return ip String
	 * @throws Exception
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 通过IP地址获取MAC地址
	 * 
	 * @param ip
	 *            String,127.0.0.1格式
	 * @return mac String
	 * @throws Exception
	 */
	public static String getMACAddress(String ip) {
		String line = "";
		String macAddress = "";
		final String MAC_ADDRESS_PREFIX = "MAC Address = ";
		final String LOOPBACK_ADDRESS = "127.0.0.1";
		try {
			// 如果为127.0.0.1,则获取本地MAC地址。
			if (LOOPBACK_ADDRESS.equals(ip)) {
				InetAddress inetAddress = InetAddress.getLocalHost();
				// 貌似此方法需要JDK1.6。
				byte[] mac = NetworkInterface.getByInetAddress(inetAddress)
						.getHardwareAddress();
				// 下面代码是把mac地址拼装成String
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < mac.length; i++) {
					if (i != 0) {
						sb.append("-");
					}
					// mac[i] & 0xFF 是为了把byte转化为正整数
					String s = Integer.toHexString(mac[i] & 0xFF);
					sb.append(s.length() == 1 ? 0 + s : s);
				}
				// 把字符串所有小写字母改为大写成为正规的mac地址并返回
				macAddress = sb.toString().trim().toUpperCase();
				return macAddress;
			}
			// 获取非本地IP的MAC地址

			String os = System.getProperty("os.name");
			Process p;
			if (os != null && os.startsWith("Windows")) {
				p = Runtime.getRuntime().exec("nbtstat -A " + ip);
			} else {
				p = Runtime.getRuntime().exec("nmblookup -A " + ip);
			}
			InputStreamReader isr = new InputStreamReader(p.getInputStream());
			BufferedReader br = new BufferedReader(isr);
			while ((line = br.readLine()) != null) {
				if (line != null) {
					int index = line.indexOf(MAC_ADDRESS_PREFIX);
					if (index != -1) {
						macAddress = line.substring(
								index + MAC_ADDRESS_PREFIX.length()).trim()
								.toUpperCase();
					}
				}
			}
			br.close();
		} catch (IOException e) {
		}
		return macAddress;
	}
	
	//以下部分从IMS拷贝过来
	/**
	 * 将对象转为int类型
	 * 
	 * @param obj
	 * @return
	 */

	public static final int toInt(String obj, int def) {
		if (obj == null) {
			return def;
		} else {
			try {
				return Integer.parseInt(obj);
			} catch (Exception e) {
				return def;
			}
		}
	}

	public static final int toInt(String obj) {
		return toInt(obj, -1);
	}

	/**
	 * 将对象转为double类型
	 * 
	 * @param obj
	 * @return
	 */
	public static final double toDouble(String obj, int def) {
		if (obj == null) {
			return def;
		} else {
			try {
				return Double.parseDouble(obj);
			} catch (Exception e) {
				return def;
			}
		}
	}

	public static final double toDouble(String obj) {
		return toInt(obj, -1);
	}

	/**
	 * 检查字符串是否为空 或 null
	 * 
	 * @param s
	 * @return
	 */
	public static final boolean checkEmptyNull(Object s) {
		return s == null || "".equals(s);
	}

	/**
	 * 检查字符串是否为空 或 null
	 * 
	 * @param s
	 * @return
	 */
	public static final boolean isNullOrEmpty(String s) {
		return s == null || "".equals(s);
	}

	/***
	 * 
	 * @date 2015-6-4
	 * @Description TODO
	 * @author shm
	 * @returnType boolean
	 */
	public static final boolean IsEqual(String str1, String str2) {
		boolean isEqual = false;
		if (str1 == null && str2 == null) {
			isEqual = true;
		} else {
			if (str1 != null && str1.equals(str2)) {
				isEqual = true;
			}
		}
		return isEqual;
	}

	/**
	 * 检查字符串是否不为空
	 * 
	 * @param s
	 * @return
	 */
	public static final boolean notNullOrEmpty(String s) {
		return s != null && !"".equals(s);
	}

	/**
	 * 检查字符串是否为数字
	 * 
	 * @param s
	 * @return
	 */
	public static final boolean isDigital(String s) {
		if (notNullOrEmpty(s)) {
			int index = s.indexOf(".");
			if (index < 0) {
				return StringUtils.isNumeric(s);
			} else {
				String num1 = s.substring(0, index);
				String num2 = s.substring(index + 1);
				return StringUtils.isNumeric(num1)
						&& StringUtils.isNumeric(num2);
			}
		} else {
			return false;
		}
	}

	/**
	 * 将对象转成String字符串
	 * 
	 * @param obj
	 * @return
	 */
	public static String toString(Object obj, String def) {
		if (obj == null) {
			return def;
		} else {
			return obj.toString();
		}
	}

	public static String toString(Object obj) {
		return toString(obj, "");
	}
	

	public static String join(String delimiter, Object... args) {
		if (args == null) {
			return null;}
		StringBuffer buffer = new StringBuffer();
		for (Object obj : args) {
			buffer.append(obj == null ? "" : obj);
			buffer.append(delimiter);
		}
		return buffer.toString();
	}

	/**
	 * 读取输入流为字符串
	 * 
	 * @param reader
	 * @return
	 */

	public static String readerToString(Reader reader) {
		try {
			return IOUtils.toString(reader);
		} catch (IOException e) {
			return "reader流出现异常" + e.getMessage();
		}
	}

	// public static String reader2String(Reader reader){
	// StringBuilder sb = new StringBuilder();
	// BufferedReader bufreader = new BufferedReader(reader);
	// try {
	// String line;
	// while ((line = bufreader.readLine()) != null) {
	// sb.append(line).append('\n');
	// }
	// } catch (IOException e) {
	// sb.append("\nreader流出现异常").append(e.getMessage());
	// } finally {
	// try {
	// bufreader.close();
	// } catch (IOException e) {
	// }
	// }
	// return sb.toString();
	// }

	/**
	 * 
	 * 正则表达式数组分割(过滤重复的值)
	 * 
	 * @param str
	 * @param regular
	 * @return "aaaaaaaaaaa${aa}aaaa#{aa}aaaaaa#{bbb}aaaa" aa,bb
	 */
	public static List<String> regularSplit$(String str) {
		List<String> strList = new ArrayList<String>();
		Matcher matcher = SUB4_PATTERN.matcher(str);
		while (matcher.find()) {
			String string = matcher.group();
			string = string.substring(2, string.length() - 1);// 仅读取#{XXX}中间的结果集
			if (!strList.contains(string)) {
				strList.add(string);
			}
		}
		return strList;
	}

	/**
	 * 正则表达式数组分割(过滤重复的值)
	 * 
	 * @param str
	 * @param regular
	 * @return "aaaaaaaaaaa#{aa}aaaa#{aa}aaaaaa#{bbb}aaaa" aa,bb
	 */
	public static List<String> regularSplit(String str) {
		List<String> strList = new ArrayList<String>();
		
		Matcher matcher = SUB3_PATTERN.matcher(str);
		while (matcher.find()) {
			String string = matcher.group();
			string = string.substring(2, string.length() - 1);// 仅读取#{XXX}中间的结果集
			if (!strList.contains(string)) {
				strList.add(string);
			}
		}
		return strList;
	}

	/**
	 * s 左补 p，按size大小
	 */
	public static String leftPad(String s, int size, char p) {
		if (s == null || s.length() > size) {
			return s;
		} else {
			int ps = size - s.length();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < ps; i++) {
				sb.append(p);
			}
			sb.append(s);
			return sb.toString();
		}
	}

	/**
	 */
	public static String replace(String src, String s, String t) {
		if (src == null || src.length() == 0) {
			return src;
		} else {
			return src.replace(s, t);
		}
	}

	/**
	 * double 转 string 防止出现科学计数法
	 * 
	 * @param dob
	 * @return
	 */
	public static String doubleToString(double dob) {
		BigDecimal bd = new BigDecimal(dob);
		return bd.toPlainString();
	}

	/**
	 * 正则替换，替换template中${}的参数
	 * 
	 * @param template
	 * @param tokens
	 * @return
	 */
	public static String replaceMapToString(String template,
			HashMap<String, Object> tokens) {
		// 生成匹配模式的正则表达式
		String patternString = "\\$\\{("
				+ StringUtils.join(tokens.keySet(), "|") + ")\\}";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(template);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, (String) tokens.get(matcher.group(1)));
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	/**
	 * 转义keyword like字句中需要调用
	 * 
	 * @param keyword
	 * @return
	 */
	public static final String transferLikeCaulse(String keyword) {
		if (keyword == null) {
			return keyword;}
		if (keyword.contains("%") || keyword.contains("_")) {
			keyword = keyword.replaceAll("\\\\", "\\\\\\\\")
					.replaceAll("\\%", "\\\\%").replaceAll("\\_", "\\\\_");
		}
		return "%" + keyword + "%";
	}

	/**
	 * String,String[] =-->String,String
	 * 
	 * @throws UnsupportedEncodingException
	 */
	public static final Map<String, String> formatMapString(
			Map<String, String[]> map, String delim)
			throws UnsupportedEncodingException {
		JY.require(map != null, "参数map为空");
		Map<String, String> ret = new HashMap<String, String>();
		for (Entry<String, String[]> entry : map.entrySet()) {
			ret.put(entry.getKey(), URLDecoder.decode(
					org.springframework.util.StringUtils
							.arrayToDelimitedString(entry.getValue(), delim),
					"UTF-8"));
		}
		return ret;
	}
	/**
	 * 先匹配 第一个参数，如果为空，则取b
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static final String firstMatch(String a, String b) {
		return StringUtil.checkEmptyNull(a) ? b : a;
	}

	/**
	 * 数字转换为汉语中人民币的大写<br>
	 * 
	 * @ClassName: MoneyToCNY
	 * @Description: TODO( )
	 * 
	 */
	public static class MoneyToCNY {
		/**
		 * 汉语中数字大写
		 */
		private static final String[] CN_UPPER_NUMBER = { "零", "壹", "贰", "叁",
				"肆", "伍", "陆", "柒", "捌", "玖" };
		/**
		 * 汉语中货币单位大写，这样的设计类似于占位符
		 */
		private static final String[] CN_UPPER_MONETRAY_UNIT = { "分", "角", "元",
				"拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟", "兆",
				"拾", "佰", "仟" };
		/**
		 * 特殊字符：整
		 */
		private static final String CN_FULL = "整";
		/**
		 * 特殊字符：负
		 */
		private static final String CN_NEGATIVE = "负";
		/**
		 * 金额的精度，默认值为2
		 */
		private static final int MONEY_PRECISION = 2;
		/**
		 * 特殊字符：零元整
		 */
		private static final String CN_ZEOR_FULL = "零元" + CN_FULL;

		/**
		 * 把输入的金额转换为汉语中人民币的大写
		 * 
		 * @param numberOfMoney
		 *            输入的金额
		 * @return 对应的汉语大写
		 */
		public static String number2CNMontrayUnit(double numberOfMoney) {
			return number2CNMontrayUnit(new BigDecimal(numberOfMoney));
		}

		/**
		 * 把输入的金额转换为汉语中人民币的大写
		 * 
		 * @param numberOfMoney
		 *            输入的金额
		 * @return 对应的汉语大写
		 */
		public static String number2CNMontrayUnit(BigDecimal numberOfMoney) {
			StringBuffer sb = new StringBuffer();
			// -1, 0, or 1 as the value of this BigDecimal is negative, zero, or
			// positive.
			int signum = numberOfMoney.signum();
			// 零元整的情况
			if (signum == 0) {
				return CN_ZEOR_FULL;
			}
			// 这里会进行金额的四舍五入
			long number = numberOfMoney.movePointRight(MONEY_PRECISION)
					.setScale(0, 4).abs().longValue();
			// 得到小数点后两位值
			long scale = number % 100;
			int numUnit = 0;
			int numIndex = 0;
			boolean getZero = false;
			// 判断最后两位数，一共有四中情况：00 = 0, 01 = 1, 10, 11
			if (!(scale > 0)) {
				numIndex = 2;
				number = number / 100;
				getZero = true;
			}
			if ((scale > 0) && (!(scale % 10 > 0))) {
				numIndex = 1;
				number = number / 10;
				getZero = true;
			}
			int zeroSize = 0;
			while (true) {
				if (number <= 0) {
					break;
				}
				// 每次获取到最后一个数
				numUnit = (int) (number % 10);
				if (numUnit > 0) {
					if ((numIndex == 9) && (zeroSize >= 3)) {
						sb.insert(0, CN_UPPER_MONETRAY_UNIT[6]);
					}
					if ((numIndex == 13) && (zeroSize >= 3)) {
						sb.insert(0, CN_UPPER_MONETRAY_UNIT[10]);
					}
					sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
					sb.insert(0, CN_UPPER_NUMBER[numUnit]);
					getZero = false;
					zeroSize = 0;
				} else {
					++zeroSize;
					if (!(getZero)) {
						sb.insert(0, CN_UPPER_NUMBER[numUnit]);
					}
					if (numIndex == 2) {
						if (number > 0) {
							sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
						}
					} else if (((numIndex - 2) % 4 == 0) && (number % 1000 > 0)) {
						sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
					}
					getZero = true;
				}
				// 让number每次都去掉最后一个数
				number = number / 10;
				++numIndex;
			}
			// 如果signum == -1，则说明输入的数字为负数，就在最前面追加特殊字符：负
			if (signum == -1) {
				sb.insert(0, CN_NEGATIVE);
			}
			// 输入的数字小数点后两位为"00"的情况，则要在最后追加特殊字符：整
			if (!(scale > 0)) {
				sb.append(CN_FULL);
			}
			return sb.toString();
		}
	}
	
	/**
	 * 转小写驼峰
	 * @param str
	 * @return
	 */
	@SuppressWarnings("unused")
	public static String underLineToCamel(String str){
		if(StringUtil.isNotEmpty(str)){
	        Matcher matcher = LOWER_PATTERN.matcher(str.toLowerCase());
	        StringBuilder builder = new StringBuilder(str.toLowerCase());
	        for (int i = 0; matcher.find(); i++) {
	        	String b =matcher.group();
	            builder.replace(matcher.start() - i, matcher.end() - i, matcher.group().substring(1).toUpperCase());
	        }
	        if (Character.isUpperCase(builder.charAt(0))) {
	            builder.replace(0, 1, String.valueOf(Character.toLowerCase(builder.charAt(0))));
	        }
	        return builder.toString();
		}
		return "";
	}
}