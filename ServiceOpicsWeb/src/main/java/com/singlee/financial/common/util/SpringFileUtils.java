package com.singlee.financial.common.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

/**
 * 动态获取SPRING XML或者properties资源文件
 * @author chenxh
 *
 */
public class SpringFileUtils {
	
	/**
	 * 根据资源文件目录,加载指文件名前缀以及后缀的文件
	 * @param res
	 * @param filterPrefix
	 * @param filterSuffix
	 * @return
	 * @throws IOException
	 */
	public static List<String> getFiles(Resource res, String filterPrefix, String... filterSuffix) throws IOException {
		// 获取classpath根目录
		String dir = res.getURI().getPath().replaceAll(res.getFilename(), "");
		LogFactory.getLog(SpringFileUtils.class).info("ClassPath dir:" + dir);
		return getFiles(new File(dir), filterPrefix, filterSuffix);
	}
	
	/**
	 * 获取目录下指定文件名前缀以及后缀的文件
	 * @param fileDir
	 * @param filterPrefix
	 * @param filterSuffix
	 * @return
	 * @throws IOException
	 */
	public static List<String> getFiles(File fileDir,String filterPrefix,String...filterSuffix) throws IOException{
		List<String> listFile = new ArrayList<String>();
		//扫描根目录是否有apdater开头的配置文件
		String [] files = fileDir.list(new FilenameFilter() {
			//根据文件名前缀以及文件名后缀过滤文件
			@Override
			public boolean accept(File dir, String name) {
				boolean ret = false;
				if(StringUtils.hasText(filterPrefix) && null != filterSuffix && filterSuffix.length > 0) {
					ret = name.startsWith(filterPrefix) && endsWith(name,filterSuffix) ? true : false;
				}else if(StringUtils.hasText(filterPrefix) && (null == filterSuffix || filterSuffix.length == 0)) {
					ret = name.startsWith(filterPrefix) ? true : false;
				}else if(!StringUtils.hasText(filterPrefix) && null != filterSuffix && filterSuffix.length > 0) {
					ret = endsWith(name,filterSuffix) ? true : false;
				}
				return ret;
			}
		});
		//没有文件程序结束
		if(null != files && files.length > 0) {
			listFile = Arrays.asList(files);
		}
		LogFactory.getLog(SpringFileUtils.class).info("wait loading config file:" + listFile);
		return listFile;
	}
	
	/**
	 * 检查数姐中是否包含指定字符串
	 * @param value
	 * @param otherValue
	 * @return
	 */
	public static boolean endsWith(String value,String...otherValue) {
		for (String var : otherValue) {
			if(value.endsWith(var)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 方法已重载.组装需要加载的BEANID
	 * @param springXml
	 * @param filterPrefix
	 * @return
	 * @throws IOException
	 */
	public static List<String> getClassPathFiles(String filterPrefix,String filterSuffix) throws IOException{
		return getFiles(new ClassPathResource("main.spring.xml"),filterPrefix,filterSuffix);
	}
}
