package com.singlee.financial.common.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 静态参数配置类
 *
 * @author shenzhaola335
 */
@Component
public class ExternalIntfc implements Serializable {

    private static final long serialVersionUID = 2124080238083026781L;

    /****************** SWIFT FTP配置 *****************************/
    // #swiftFTP IP 地址
    @Value("${swift.ftp.ip:}")
    private String swiftIp;
    // #swiftFTP 端口
    @Value("${swift.ftp.port:}")
    private String swiftPort;
    // #swiftFTP 用户名
    @Value("${swift.ftp.userName:}")
    private String swiftUserName;
    // #swiftFTP 密码
    @Value("${swift.ftp.password:}")
    private String swiftPassword;
    // #swiftFTP 本地路径
    @Value("${swift.ftp.beanpath:}")
    private String swiftLocalpath;
    // #swiftFTP 备份地址
    @Value("${swift.ftp.bakuppath:}")
    private String swiftBakuppath;
    // #swiftFTP 目标地址
    @Value("${swift.ftp.remotepath:}")
    private String swiftRemotepath;
    // #swiftFTP 文件名称
    @Value("${swift.ftp.imtf9:}")
    private String imtf9;
    // #swiftFTP 传输方式
    @Value("${swift.ftp.ftptype:}")
    private String swiftFtptype;
    // #swiftFTP 文件过滤正则表达式
    @Value("${swift.ftp.regular:}")
    private String swiftFtpRegular;

    /****************** OPICS共享盘 *****************************/
    // OPICS服务器共享盘路径
    @Value("${share.ip:}")
    private String shareIp;
    @Value("${share.port:}")
    private String sharePort;
    // OPICS服务器共享盘路径
    @Value("${share.dir:}")
    private String shareDir;
    // OPICS服务器共享盘路径
    @Value("${share.sourcepath:}")
    private String sourcePath;
    // OPICS共享盘SWIFT文件名称
    @Value("${share.filename:}")
    private String fileName;
    // OPICS共享盘登录名称
    @Value("${share.username:}")
    private String userName;
    // OPICS共享盘登录密码
    @Value("${share.password:}")
    private String password;

    /****************** OPICS批量自动调度 *****************************/
    // telnet 地址
    @Value("${telnet.Ip:}")
    private String telnetIp;
    @Value("${telnet.Port:}")
    private String telnetPort;
    @Value("${telnet.User:}")
    private String telnetUser;
    @Value("${telnet.Password:}")
    private String telnetPassword;
    // OPICS 安装客户端路径
    @Value("${telnet.HomeDir:}")
    private String telnetHomeDir;
    // opics 调用BSYS命令
    @Value("${telnet.StartBsys:}")
    private String telnetStartBsys;//用户1
    @Value("${telnet.StartBsys2:}")
    private String telnetStartBsys2;//用户2
    //是否转换msb收盘价格导入
    @Value("${seclExcelMbsSecPriceIsConvert:}")
    private String isSeclExcelMbsSecPriceIsConvert;
    //贴现债券是否使用全价导入
    @Value("${seclExcelDisSecIsUseDirtyPrice:}")
    private String isSeclExcelDisSecIsUseDirtyPrice;

    public String getSwiftIp() {
        return swiftIp;
    }

    public void setSwiftIp(String swiftIp) {
        this.swiftIp = swiftIp;
    }

    public String getSwiftPort() {
        return swiftPort;
    }

    public void setSwiftPort(String swiftPort) {
        this.swiftPort = swiftPort;
    }

    public String getSwiftUserName() {
        return swiftUserName;
    }

    public void setSwiftUserName(String swiftUserName) {
        this.swiftUserName = swiftUserName;
    }

    public String getSwiftPassword() {
        return swiftPassword;
    }

    public void setSwiftPassword(String swiftPassword) {
        this.swiftPassword = swiftPassword;
    }

    public String getSwiftLocalpath() {
        return swiftLocalpath;
    }

    public void setSwiftLocalpath(String swiftLocalpath) {
        this.swiftLocalpath = swiftLocalpath;
    }

    public String getSwiftBakuppath() {
        return swiftBakuppath;
    }

    public void setSwiftBakuppath(String swiftBakuppath) {
        this.swiftBakuppath = swiftBakuppath;
    }

    public String getSwiftRemotepath() {
        return swiftRemotepath;
    }

    public void setSwiftRemotepath(String swiftRemotepath) {
        this.swiftRemotepath = swiftRemotepath;
    }

    public String getImtf9() {
        return imtf9;
    }

    public void setImtf9(String imtf9) {
        this.imtf9 = imtf9;
    }

    public String getSwiftFtptype() {
        return swiftFtptype;
    }

    public void setSwiftFtptype(String swiftFtptype) {
        this.swiftFtptype = swiftFtptype;
    }

    public String getSwiftFtpRegular() {
        return swiftFtpRegular;
    }

    public void setSwiftFtpRegular(String swiftFtpRegular) {
        this.swiftFtpRegular = swiftFtpRegular;
    }

    public String getShareIp() {
        return shareIp;
    }

    public void setShareIp(String shareIp) {
        this.shareIp = shareIp;
    }

    public String getShareDir() {
        return shareDir;
    }

    public void setShareDir(String shareDir) {
        this.shareDir = shareDir;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelnetIp() {
        return telnetIp;
    }

    public void setTelnetIp(String telnetIp) {
        this.telnetIp = telnetIp;
    }

    public String getTelnetPort() {
        return telnetPort;
    }

    public void setTelnetPort(String telnetPort) {
        this.telnetPort = telnetPort;
    }

    public String getTelnetUser() {
        return telnetUser;
    }

    public void setTelnetUser(String telnetUser) {
        this.telnetUser = telnetUser;
    }

    public String getTelnetPassword() {
        return telnetPassword;
    }

    public void setTelnetPassword(String telnetPassword) {
        this.telnetPassword = telnetPassword;
    }

    public String getTelnetHomeDir() {
        return telnetHomeDir;
    }

    public void setTelnetHomeDir(String telnetHomeDir) {
        this.telnetHomeDir = telnetHomeDir;
    }

    public String getTelnetStartBsys() {
        return telnetStartBsys;
    }

    public void setTelnetStartBsys(String telnetStartBsys) {
        this.telnetStartBsys = telnetStartBsys;
    }

    public String getTelnetStartBsys2() {
        return telnetStartBsys2;
    }

    public void setTelnetStartBsys2(String telnetStartBsys2) {
        this.telnetStartBsys2 = telnetStartBsys2;
    }

    public String getIsSeclExcelMbsSecPriceIsConvert() {
        return isSeclExcelMbsSecPriceIsConvert;
    }

    public void setIsSeclExcelMbsSecPriceIsConvert(String isSeclExcelMbsSecPriceIsConvert) {
        this.isSeclExcelMbsSecPriceIsConvert = isSeclExcelMbsSecPriceIsConvert;
    }

    public String getIsSeclExcelDisSecIsUseDirtyPrice() {
        return isSeclExcelDisSecIsUseDirtyPrice;
    }

    public void setIsSeclExcelDisSecIsUseDirtyPrice(String isSeclExcelDisSecIsUseDirtyPrice) {
        this.isSeclExcelDisSecIsUseDirtyPrice = isSeclExcelDisSecIsUseDirtyPrice;
    }

    public String getSharePort() {
        return sharePort;
    }

    public void setSharePort(String sharePort) {
        this.sharePort = sharePort;
    }

    @Override
    public String toString() {
        return "ExternalIntfc{" +
                "swiftIp='" + swiftIp + '\'' +
                ", swiftPort='" + swiftPort + '\'' +
                ", swiftUserName='" + swiftUserName + '\'' +
                ", swiftPassword='" + swiftPassword + '\'' +
                ", swiftLocalpath='" + swiftLocalpath + '\'' +
                ", swiftBakuppath='" + swiftBakuppath + '\'' +
                ", swiftRemotepath='" + swiftRemotepath + '\'' +
                ", imtf9='" + imtf9 + '\'' +
                ", swiftFtptype='" + swiftFtptype + '\'' +
                ", swiftFtpRegular='" + swiftFtpRegular + '\'' +
                ", shareIp='" + shareIp + '\'' +
                ", sharePort='" + sharePort + '\'' +
                ", shareDir='" + shareDir + '\'' +
                ", sourcePath='" + sourcePath + '\'' +
                ", fileName='" + fileName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", telnetIp='" + telnetIp + '\'' +
                ", telnetPort='" + telnetPort + '\'' +
                ", telnetUser='" + telnetUser + '\'' +
                ", telnetPassword='" + telnetPassword + '\'' +
                ", telnetHomeDir='" + telnetHomeDir + '\'' +
                ", telnetStartBsys='" + telnetStartBsys + '\'' +
                ", telnetStartBsys2='" + telnetStartBsys2 + '\'' +
                ", isSeclExcelMbsSecPriceIsConvert='" + isSeclExcelMbsSecPriceIsConvert + '\'' +
                ", isSeclExcelDisSecIsUseDirtyPrice='" + isSeclExcelDisSecIsUseDirtyPrice + '\'' +
                '}';
    }
}