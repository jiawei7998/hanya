package com.singlee.financial.common.spring.mybatis;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import tk.mybatis.mapper.common.Mapper;




public interface MyMapper<T> extends Mapper<T> {
	/**
     * 支持参数按map传入 Key==value
     *
     * @param recordList
     * @return
     */
    @SelectProvider(type = MyMapperProvider.class, method = "dynamicSQL")
    List<T> select2(T t);
    
    

    @InsertProvider(type = MyMapperProvider.class, method = "dynamicSQL")
    int insertListWithId(List<T> l);
    
    
    /**
     * 支持map中的key对应于T类型的属性，value都是字符串
     * map中必须保护有T的@ID属性，
     * map中多余的属性不会进入sql，value=null的不会进入sql
     * 但是map中value=''的，将进入sql并更新成空字符串。
     * 
     * @param map
     * @return
     */
    @UpdateProvider(type = MyMapperProvider.class, method = "dynamicSQL")
    int updateByMap(Map<String,String> map);

}