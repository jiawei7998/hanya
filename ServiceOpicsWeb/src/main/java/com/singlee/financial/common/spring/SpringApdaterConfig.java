package com.singlee.financial.common.spring;

import java.io.Serializable;
import java.util.List;

/**
 * 加载自定义properties配置实体类
 * @author chenxh
 *
 */
public class SpringApdaterConfig implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7836302197854632204L;

	private List<String> encryptKeys;
	
	private String encryptMethod;
	
	private String filterPrefix;
	
	private String filterSuffix = ".properties";
	
	private boolean isEncrypt;
	
	public SpringApdaterConfig(String filterPrefix, String encryptMethod,boolean isEncrypt,List<String> encryptKeys) {
		super();
		this.encryptKeys = encryptKeys;
		this.encryptMethod = encryptMethod;
		this.filterPrefix = filterPrefix;
		this.isEncrypt = isEncrypt;
	}

	public List<String> getEncryptKeys() {
		return encryptKeys;
	}

	public void setEncryptKeys(List<String> encryptKeys) {
		this.encryptKeys = encryptKeys;
	}

	public String getEncryptMethod() {
		return encryptMethod;
	}

	public void setEncryptMethod(String encryptMethod) {
		this.encryptMethod = encryptMethod;
	}

	public String getFilterPrefix() {
		return filterPrefix;
	}

	public void setFilterPrefix(String filterPrefix) {
		this.filterPrefix = filterPrefix;
	}

	public String getFilterSuffix() {
		return filterSuffix;
	}

	public void setFilterSuffix(String filterSuffix) {
		this.filterSuffix = filterSuffix;
	}

	public boolean isEncrypt() {
		return isEncrypt;
	}

	public void setEncrypt(boolean isEncrypt) {
		this.isEncrypt = isEncrypt;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SpringApdaterConfig [encryptKeys=");
		builder.append(encryptKeys);
		builder.append(", encryptMethod=");
		builder.append(encryptMethod);
		builder.append(", filterPrefix=");
		builder.append(filterPrefix);
		builder.append(", filterSuffix=");
		builder.append(filterSuffix);
		builder.append(", isEncrypt=");
		builder.append(isEncrypt);
		builder.append("]");
		return builder.toString();
	}
	
}
