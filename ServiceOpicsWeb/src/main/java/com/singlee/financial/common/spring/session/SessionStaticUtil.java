package com.singlee.financial.common.spring.session;

import java.util.Locale;

/**
 * session的静态方法工具集合
 * 
 * 线程中自由获取定义session
 * 
 * @author x230i
 * 
 */
public class SessionStaticUtil {

	/**
	 * ***********************************************************************
	 */

	/**
	 * 存放自定义session，由httpsession转过来
	 */
	private static final ThreadLocal<SlSession> threadLocal = new ThreadLocal<SlSession>();

	public static final void holdSlSessionThreadLocal(SlSession session) {
		threadLocal.set(session);
	}

	public static final void clearSlSessionThreadLocal() {
		threadLocal.remove();
	}

	public static final SlSession getSlSessionThreadLocal() {
		return threadLocal.get();
	}

	/** 提供一个获得当前会话线程 的locale */
	public static final Locale getSlSessionLocale() {
		SlSession s = getSlSessionThreadLocal();
		if (s != null) {
			return s.getLocaleDefault();
		} else {
			return Locale.getDefault();
		}
	}
}
