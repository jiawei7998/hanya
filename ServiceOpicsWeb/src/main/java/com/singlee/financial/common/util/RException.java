package com.singlee.financial.common.util;

import java.io.Serializable;




/**
 * 运行期异常
 * @author
 */
public class RException extends RuntimeException implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new runtime exception with null as its detail message.
	 */
	public RException() {
		this("UnknownException");
	}

	/**
	 * Constructs a new runtime exception with the specified detail message.
	 * 
	 * @param message
	 */
	public RException(final String message) {
		super(message);
	}


	/**
	 * Constructs a new runtime exception with the specified detail message and
	 * cause.
	 * 
	 * @param message
	 * @param cause
	 */
	public RException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new runtime exception with the specified cause and a detail
	 * message of (cause==null ? null : cause.toString()) (which typically
	 * contains the class and detail message of cause).
	 * 
	 * @param cause
	 */
	public RException(final Throwable cause) {
		super(cause);
	}

	
}
