package com.singlee.financial.common.spring;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;


/**
 * 系统初始化 动作， 可以再init方法中顺序增加 实现者应该 声明 @Service @Scope("singleton")
 * 
 * @author LyonChen
 * 
 */
public abstract class InitListener implements ApplicationListener<ContextRefreshedEvent> {

	private Logger logger = Logger.getLogger(InitListener.class);
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			logger.info("--------------------------------------------------------------------");
			logger.info(sysname() + "系统init工作开始");
			logger.info("--------------------------------------------------------------------");
			// 需要执行的逻辑代码，当spring容器初始化完成后就会执行该方法。
			init(event.getApplicationContext());
			logger.info("--------------------------------------------------------------------");
			logger.info(sysname() + "系统init工作结束");
			logger.info("--------------------------------------------------------------------");
		}
	}

	/**
	 * 
	 */
	protected abstract void init(ApplicationContext ac);

	protected abstract String sysname();

}
