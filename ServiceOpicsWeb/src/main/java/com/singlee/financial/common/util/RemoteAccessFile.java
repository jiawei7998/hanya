package com.singlee.financial.common.util;

import java.io.*;
import java.net.MalformedURLException;
import java.util.*;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.singlee.financial.common.spring.ExternalIntfc;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileFilter;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

/**
 * 操作共享盘文件 通过SMB上传、下载、读取、复制、删除远程共享文件
 *
 * @author shenzl
 */
public class RemoteAccessFile {

    private static Logger logger = Logger.getLogger(RemoteAccessFile.class);
    private static String holnotuse = "not used";
    private static String holAnnual = "Rem - annual holidays";
    private static String holSpecific = "Rem - specific holidays";
    private static String holEnd = "Rem - end of holiday file";
    private static String holSplitStr = "\r\n";

    /**
     * 主要利用类 SmbFile 去实现读取共享文件夹 shareFile 下的所有文件(文件夹)的名称
     *
     * @param sourcePath
     * @param userName
     * @param password
     * @return
     * @throws MalformedURLException
     */
    public static SmbFile[] smbRead(String sourcePath, String userName, String password) throws MalformedURLException {
        SmbFile sourceFile = new SmbFile("smb://" + userName + ":" + password + "@" + sourcePath);
        try {
            if (!sourceFile.exists()) {
                return null;
            }
            SmbFile[] files = sourceFile.listFiles();
            for (SmbFile f : files) {
                System.out.println(f.getName());
            }

            return sourceFile.listFiles();
        } catch (SmbException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 把共享目录指定文件夹下的文件拷贝到另外一个共享目录指定文件夹下
     *
     * @param sourcePath 远端共享盘路径
     * @param targetPath 远端共享盘目标路径
     * @param fileName   文件名称
     * @param userName   远端共享盘登录用户名
     * @param password   远端共享盘登录密码
     * @throws IOException
     */
    public static void smbCopy(String sourcePath, String targetPath, String fileName, String userName, String password)
            throws IOException {
        SmbFile sourceFile = new SmbFile("smb://" + userName + ":" + password + "@" + sourcePath + fileName);
        SmbFile targetFile = new SmbFile("smb://" + userName + ":" + password + "@" + targetPath);
        if (!targetFile.exists()) { // 目录文件夹不存在，就创建
            targetFile.mkdirs();
        }
        if (sourceFile.exists() && sourceFile.isFile()) {
            SmbFile cpFile = new SmbFile(
                    "smb://" + userName + ":" + password + "@" + targetPath + sourceFile.getName());
            sourceFile.copyTo(cpFile);
        }
    }

    /**
     * 删除共享目录中指定文件
     *
     * @param sourcePath 远端共享盘路径
     * @param fileName   文件名称
     * @param userName   远端共享盘登录用户名
     * @param password   远端共享盘登录密码
     * @throws Exception
     */
    public static void smbDelete(String sourcePath, String fileName, String userName, String password)
            throws Exception {
        SmbFile sourceFile = new SmbFile("smb://" + userName + ":" + password + "@" + sourcePath + fileName);
        if (sourceFile.exists()) {
            sourceFile.delete();
        }
    }

    public static SmbFileInputStream smbGet(String sourcePath, String fileName, String userName, String password)
            throws Exception {
        SmbFile sourceFile = new SmbFile("smb://" + userName + ":" + password + "@" + sourcePath + fileName);
        SmbFileInputStream sfis = new SmbFileInputStream(sourceFile);
        return sfis;
    }

    /**
     * 从共享目录下载文件
     *
     * @param sourcePath 远端共享盘路径
     * @param targetPath 本地临时路径
     * @param fileName   文件名称
     * @param userName   远端共享盘登录用户名
     * @param password   远端共享盘登录密码
     */
    public static void smbGet(String sourcePath, String targetPath, String fileName, String userName, String password) {
        // 输入流
        InputStream in = null;
        // 输出流
        OutputStream out = null;

        try {
            // 初始化远端共享盘路径
            SmbFile sourceFile = new SmbFile("smb://" + userName + ":" + password + "@" + sourcePath + fileName);
            if (!sourceFile.exists()) {
                return;
            }
            // 获取远端file名称
            String sourceFileName = sourceFile.getName();
            // 初始化本地文件对象
            File targetFile = new File(targetPath + File.separator + sourceFileName);
            // 创建本地路径
            if (!targetFile.exists()) {
                targetFile.getParentFile().mkdirs();
            }
            // 输入流
            in = new BufferedInputStream(new SmbFileInputStream(sourceFile));
            // 输出流
            out = new BufferedOutputStream(new FileOutputStream(targetFile));
            byte[] buffer = new byte[1024];
            while (in.read(buffer) != -1) {
                out.write(buffer);
                buffer = new byte[1024];
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 向共享目录上传文件
     *
     * @param sourcePath 本地文件路径
     * @param targetPath 远端共享目录路径
     * @param fileName   本地文件名称
     * @param userName   远端共享盘登录用户名
     * @param password   远端共享盘登录密码
     * @throws Exception
     */

    @SuppressWarnings("resource")
    public static void smbPut(String sourcePath, String targetPath, String fileName, String userName, String password)
            throws Exception {
        OutputStream out = null;
        // 本地文件
        File sourceFile = new File(sourcePath + File.separator + fileName);
        InputStream ins = new FileInputStream(sourceFile);
        // 远程共享地址
        SmbFile smbfile = new SmbFile("smb://" + userName + ":" + password + "@" + targetPath + fileName);
        try {
            if (smbfile.exists()) {
                System.out.println("file is exists");
            } else {
                smbfile.connect();
                out = new SmbFileOutputStream(smbfile);
                byte[] buffer = new byte[4096];
                int len = 0; // 读取长度
                while ((len = ins.read(buffer, 0, buffer.length)) != -1) {
                    out.write(buffer, 0, len);
                }
                out.flush(); // 刷新缓冲的输出流
                System.out.println("写入成功");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public RemoteAccessFile() {
    }

    /**
     * 通过OpenSsh访问opics的共享盘文件
     *
     * @param ccy
     * @param ccySpecHldy
     * @param weekday
     * @param year
     * @param suffix
     * @param userName
     * @param password
     * @param ip
     * @param port
     * @param dir
     * @return
     */
    public static boolean sshHolidayFile(String ccy, List<Date> ccySpecHildy, List<Date> weekday, Integer year,
                                         String suffix, String userName, String password, String ip, int port, String dir) {
        boolean retStatus = true;
        BufferedReader bufReader = null;
        BufferedWriter bufWrite = null;

        // 特殊周末
        List<String> annual = new ArrayList<String>();
        List<Integer> specific = new ArrayList<Integer>();
        String weekendRegex = "(\\s[1-7])+";
        String annualRegex = "([0-9]|[0-9][0-9])\\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)";
        String specificRegex = "\\d{5}";
        //登录ssh
        SingleeJschUtil singleeJschUtil = new SingleeJschUtil();
        try {
            //登录远程
            Session session = singleeJschUtil.login(ip, port, userName, password);
            //打开一个sftp通道
            ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
            //连接
            sftpChannel.connect();
            //进入当前目录
            sftpChannel.cd(dir);
            try {
                //处理单独文件
                logger.info("==========开始处理【" + ccy + "】货币日历=====================");
                bufReader = new BufferedReader(new InputStreamReader(sftpChannel.get(ccy + ".hol"), "UTF-8"));
            } catch (SftpException e) {
                if (null != bufReader) {
                    bufReader.close();
                }
                logger.error("假日文件不存在，异常信息SftpException:" + e.getMessage(), e);
                return false;
            }
            String weekend = "";
            String line = "";
            int i = 1;
            while ((line = bufReader.readLine()) != null) {
                logger.debug("开始读取" + i + "行，处理当前==>" + line);
                if (StringUtils.equals(line, holnotuse)) {
                    continue;
                } else if (line.matches(weekendRegex)) {
                    weekend = line;
                } else if (line.equalsIgnoreCase(holAnnual)) {
                    continue;
                } else if (line.matches(annualRegex)) {//
                    annual.add(line);
                } else if (line.equalsIgnoreCase(holSpecific)) {
                    continue;
                } else if (line.matches(specificRegex)) {
                    specific.add(Integer.parseInt(line));
                }
                i++;
            }
            // ================预处理日期=================
            // 转换位5位假日数据处理
            List<Integer> ccyHildy = DateUtil.getDayFromOriginDate(ccySpecHildy, 2);
            // 获取最大最小日期
            Integer maxHildy = Collections.max(ccyHildy);
            logger.debug("获取特殊假日最大日期==>" + maxHildy);
            Integer minHildy = Collections.min(ccyHildy);
            logger.debug("获取特殊假日最小日期==>" + minHildy);
            logger.debug("更改前假日文件中特殊假日长度==>" + specific.size());
            // 删除旧的假日数据
            for (int n = specific.size() - 1; n >= 0; n--) {
                //在传入的数据范围之间的全部移除;大于当前最大的也需要移除;防止未来数据异常
                if (minHildy <= specific.get(n)) {
                    logger.info("删除特殊假日中已有假日(小于当前最小的日期全部移除)... ==>" + specific.get(n));
                    specific.remove(n);
                }
            }
            logger.debug("删除最大最小后特殊假日长度==>" + specific.size());
            specific.removeAll(ccyHildy);
            specific.addAll(ccyHildy);
            // 判断周六周末设置,
            if (StringUtils.isNotBlank(weekend)) {
                List<Integer> weekdayInt = DateUtil.getDayFromOriginDate(weekday, 2);
                specific.removeAll(weekdayInt);
                logger.info("检测假日文件周末假日设置" + weekend + "非空，删除周末设置... ==> " + weekdayInt);
            }
            // 判断年度加入
            if (annual.size() > 0) {
                List<Date> annualDay = DateUtil.getAnnelDay(year, annual);
                List<Integer> annualInt = DateUtil.getDayFromOriginDate(annualDay, 2);
                specific.removeAll(annualInt);
                logger.info("检测假日文件年度假日设置," + annual.size() + "非空,删除年度假日设置... ==> " + annualInt);
            }
            //重新排序
            Collections.sort(specific);
            logger.debug("特殊假日增加周末后长度==>" + specific.size());
            // ================预处理日期=================
            // 开始处理文件
            bufWrite = new BufferedWriter(new OutputStreamWriter(sftpChannel.put(ccy + ".hol"), "UTF-8"));
            // 开始逐行写入文件
            // 第一行
            bufWrite.write(holnotuse + holSplitStr);
            // 第二行
            bufWrite.write(holnotuse + holSplitStr);
            // 第三行
            bufWrite.write(weekend + holSplitStr);
            // 第四行年度假日备注
            bufWrite.write(holAnnual + holSplitStr);
            // 循环增加年度假日
            for (String anl : annual) {
                bufWrite.write(anl + holSplitStr);
            }
            // 第五特殊假日备注
            bufWrite.write(holSpecific + holSplitStr);
            // 写如新的数据>>防止历史数据丢失
            for (Integer spc : specific) {
                // 写入新日期
                bufWrite.write(spc + holSplitStr);
            }
            // 添加结尾数据
            bufWrite.write(holEnd + holSplitStr);
            bufWrite.flush();
        } catch (JSchException e) {
            retStatus = false;
            logger.error("JSchException:" + e.getMessage(), e);
        } catch (SftpException e) {
            retStatus = false;
            logger.error("SftpException:" + e.getMessage(), e);
        } catch (IOException e) {
            retStatus = false;
            logger.error("IOException:" + e.getMessage(), e);
        } finally {
            try {
                if (null != bufReader) {
                    bufReader.close();
                }
                if (null != bufWrite) {
                    bufWrite.close();
                }
            } catch (IOException e) {
                retStatus = false;
                logger.error("IOException:" + e.getMessage(), e);
            }
        }
        return retStatus;
    }


    /**
     * 处理OPICS共享盘OPXDB下的HOL文件
     *
     * @param ccy
     * @param ccySpecHldy
     * @param weekday
     * @param year
     * @param targetPath
     * @param suffix
     * @param userName
     * @param password
     * @return
     */
    @Deprecated
    public static boolean smbHolidayFile(String ccy, List<Date> ccySpecHldy, List<Date> weekday, Integer year,
                                         String targetPath, String suffix, String userName, String password) {
        boolean retStatus = true;
        OutputStream out = null;
        InputStream in = null;
        // 特殊周末
        String weekend = "";
        List<String> annual = new ArrayList<String>();
        List<Integer> specific = new ArrayList<Integer>();
        String weekendRegix = "(\\s[1-7])+";
        String annualRegix = "([0-9]|[0-9][0-9])\\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)";
        String specificRegix = "\\d{5}";
        // 远程共享地址
        SmbFile smbfile;
        try {
            // 连接共享盘
            smbfile = new SmbFile("smb://" + userName + ":" + password + "@" + targetPath);
            // 获取共享盘下的路径&文件
            List<SmbFile> smbFileLst = Arrays.asList(smbfile.listFiles(new SmbFileFilter() {
                @Override
                public boolean accept(SmbFile smbFile) throws SmbException {// 根据后缀筛选过滤
                    return smbFile.getName().endsWith(suffix.split(",")[0])
                            || smbFile.getName().endsWith(suffix.split(",")[1]);
                }
            }));
            for (SmbFile sf : smbFileLst) {// 循环处理文件
                if (StringUtils.equalsIgnoreCase(sf.getName().split("\\.")[0], ccy)) {// 判断是否包含当前货币的假日文件
                    logger.info("开始处理" + sf.getName() + "假日文件!!!");
                    sf.connect();// 连接当前假日文件
                    in = new SmbFileInputStream(sf);
                    InputStreamReader inReader = new InputStreamReader(in, "UTF-8");
                    BufferedReader bufReader = new BufferedReader(inReader);
                    String line = null;
                    int i = 1;
                    while ((line = bufReader.readLine()) != null) {
                        logger.debug("开始读取" + i + "行，处理当前==>" + line);
                        if (StringUtils.equals(line, holnotuse)) {
                            continue;
                        } else if (line.matches(weekendRegix)) {
                            weekend = line;
                        } else if (line.equalsIgnoreCase(holAnnual)) {
                            continue;
                        } else if (line.matches(annualRegix)) {//
                            annual.add(line);
                        } else if (line.equalsIgnoreCase(holSpecific)) {
                            continue;
                        } else if (line.matches(specificRegix)) {
                            specific.add(Integer.parseInt(line));
                        }
                        i++;
                    }
                    bufReader.close();
                    inReader.close();
                    in.close();
                    // ================预处理日期=================
                    // 转换位5位假日数据处理
                    List<Integer> ccyHldy = DateUtil.getDayFromOriginDate(ccySpecHldy, 2);
                    // 获取最大最小日期
                    Integer maxHldy = Collections.max(ccyHldy);
                    logger.debug("获取特殊假日最大日期==>" + maxHldy);
                    Integer minHldy = Collections.min(ccyHldy);
                    logger.debug("获取特殊假日最小日期==>" + minHldy);
                    logger.debug("更改前假日文件中特殊假日长度==>" + specific.size());
                    // 删除旧的假日数据
                    for (int n = specific.size() - 1; n >= 0; n--) {
                        if (specific.get(n) <= maxHldy && minHldy <= specific.get(n)) {
                            logger.debug("删除特殊假日中已有假日(根据最大日期和最小日期)==>" + specific.get(n));
                            specific.remove(n);
                        }
                    }
                    logger.debug("删除最大最小后特殊假日长度==>" + specific.size());
                    specific.removeAll(ccyHldy);
                    specific.addAll(ccyHldy);
                    // 判断周六周末设置,
                    if (StringUtils.isNotBlank(weekend)) {
                        logger.debug("检测周末假日设置" + weekend + "非空，删除周末设置...");
                        List<Integer> weekdayInt = DateUtil.getDayFromOriginDate(weekday, 2);
                        specific.removeAll(weekdayInt);
                    }
                    // 判断年度加入
                    if (!annual.isEmpty()) {
                        logger.debug("检测年度假日设置," + annual.size() + "非空,删除年度假日设置...");
                        List<Date> annualDay = DateUtil.getAnnelDay(year, annual);
                        List<Integer> annualInt = DateUtil.getDayFromOriginDate(annualDay, 2);
                        specific.removeAll(annualInt);
                    }

                    Collections.sort(specific);
                    logger.debug("特殊假日增加周末后长度==>" + specific.size());
                    // ================预处理日期=================
                    // 开始处理文件
                    out = new SmbFileOutputStream(sf);
                    OutputStreamWriter outWriter = new OutputStreamWriter(out, "UTF-8");
                    BufferedWriter bufWrite = new BufferedWriter(outWriter);
                    // 开始逐行写入文件
                    // 第一行
                    bufWrite.write(holnotuse + holSplitStr);
                    // 第二行
                    bufWrite.write(holnotuse + holSplitStr);
                    // 第三行
                    bufWrite.write(weekend + holSplitStr);
                    // 第四行年度假日备注
                    bufWrite.write(holAnnual + holSplitStr);
                    // 循环增加年度假日
                    for (String anl : annual) {
                        bufWrite.write(anl + holSplitStr);
                    }
                    // 第五特殊假日备注
                    bufWrite.write(holSpecific + holSplitStr);
                    // 写如新的数据>>防止历史数据丢失
                    for (Integer spc : specific) {
                        // 写入新日期
                        bufWrite.write(spc + holSplitStr);
                    }
                    // 添加结尾数据
                    bufWrite.write(holEnd + holSplitStr);
                    bufWrite.close();
                    outWriter.close();
                    out.close();
                }
            }
        } catch (MalformedURLException e) {
            retStatus = false;
        } catch (IOException e) {
            retStatus = false;
        } finally {
            try {
                if (null != in) {
                    in.close();
                }
                if (null != out) {
                    out.close();
                }
            } catch (IOException e) {
                retStatus = false;
            }
        }
        return retStatus;

    }

    public static void main(String[] args) throws Exception {
        // 查看
        // smbRead("10.4.146.51/OPICS45/SWIFT/", "OPICS", "Master10!");
        // 下載
        // smbGet("10.4.146.51/OPICS45/SWIFT/", "c:/swift/", "imtf9.b01", "OPICS",
        // "Master10!");
        // 拷貝
//		smbCopy("10.4.146.51/OPICS45/SWIFT/", "10.4.146.51/OPICS45/SWIFT/BACKUP_TEST/", "imtf9.b01", "OPICS",
//				"Master10!");
        // 刪除
//		smbDelete("10.4.146.51/OPICS45/SWIFT/", "imtf9.b01", "OPICS", "Master10!");
    }
}
