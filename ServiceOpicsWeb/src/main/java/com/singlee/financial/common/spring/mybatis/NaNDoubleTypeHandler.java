package com.singlee.financial.common.spring.mybatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.ibatis.type.DoubleTypeHandler;
import org.apache.ibatis.type.JdbcType;


/**
 * 对double类型数据，进行处理，NaN数据作为null存入数据库
 * 从数据库中null数据作为NaN返回
 * @author LyonChen
 *
 */
public class NaNDoubleTypeHandler extends DoubleTypeHandler {
	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, Double v, JdbcType type) throws SQLException {
		 if(Double.isNaN(v)){
			 ps.setObject(i, null);
		 }else{
			 ps.setDouble(i, v.doubleValue());
		 }
	}
	@Override
	public void setParameter(PreparedStatement ps, int i, Double v, JdbcType type) throws SQLException {
		 if(v == null || Double.isNaN(v)){
			 ps.setObject(i, null);
		 }else{
			 ps.setDouble(i, v.doubleValue());
		 }
	}
}
