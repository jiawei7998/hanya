package com.singlee.financial.common.util;

import java.lang.reflect.Field;
import java.util.Locale;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import com.singlee.financial.common.spring.session.SessionStaticUtil;
import com.singlee.financial.common.spring.SpringContextHolder;

/**
 * 
 * @author x230i
 *
 */
public class MsgUtils {
	
	
	private static final ReloadableResourceBundleMessageSource mr = SpringContextHolder.getBean(ReloadableResourceBundleMessageSource.class);
	
	public static final String getOkMessage(Locale l){ 
		return getMessage("error.common.0000", null, "", l);
	}
	
	
	public static final String getOkMessage(){ 
		return getMessage("error.common.0000", null,  "ok");
	}
	
	public static String getMessage(String key, String[] params, String def){
		return getMessage( key, params, def,
				SessionStaticUtil.getSlSessionLocale());
	}

	public static String getMessage(String key, String... params){
		return getMessage( key, params, key,
				SessionStaticUtil.getSlSessionLocale());
	}

	public static String getMessage(String key){
		return getMessage( key, null, key,
				SessionStaticUtil.getSlSessionLocale());
	}
	protected static String getMessage(String key, String[] params, String def, Locale l){
		JY.require(mr !=null, "") ;
		return mr.getMessage(key, params, def, l);
	}

	public static String getDictDesc(String dict){
		return getMessage("dictionary."+dict);
	}

	public static String getDictItemDesc(Class<?> dictClz, String key){
		return getMessage("dictionary."+dictClz.getSimpleName()+"."+key);
	}

	public static String getDictItemText(String dictId, String value){
		return getMessage("dictionary."+dictId+"."+value);
	}
	
	public static String getDictItemValue(Class<?> dictClz, String key){
		Field[] file = dictClz.getDeclaredFields();
		String val = null;
		for(int i=0;i<file.length;i++) {
			file[i].setAccessible(true);
			try {
				Object o = file[i].get(dictClz);
				String value = getMessage("dictionary."+dictClz.getSimpleName()+"."+o);
				if(key.equals(value)){
					val = (String) o;
				}
			} catch (Exception e) {
				throw new RException(e);
			} 
		   }
		return val;
	}
}
