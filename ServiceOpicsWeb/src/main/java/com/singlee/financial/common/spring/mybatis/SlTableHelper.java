package com.singlee.financial.common.spring.mybatis;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import tk.mybatis.mapper.code.Style;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.SqlHelper;

/**
 * Mybatis的tableHelper类
 * @author Libo
 *
 */
public class SlTableHelper {
	private static final tk.mybatis.mapper.entity.Config config;
	static {
		config = new tk.mybatis.mapper.entity.Config();
		config.setProperties(null);
		config.setStyle(Style.camelhumpAndUppercase);
		config.setOrder("BEFORE");
		config.setNotEmpty(true);
	}

	/**
	 * 根据表的属性获取或有的IF语句
	 * @param tableName
	 * @param exclude
	 * @return
	 */
	public static String getAllIf(String tableClass) throws ClassNotFoundException{
		Class<?> clz = Class.forName(tableClass);
		EntityHelper.initEntityNameMap(clz, config);
		String whereStr = SqlHelper.whereAllIfColumns(clz, true);
		return whereStr;
	}
	/**
	 * 获取所有非数字字段的IF语句
	 * @param tableClass
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static String getAllIfNoNumber(String tableClass) throws ClassNotFoundException{
		Class<?> clz = Class.forName(tableClass);
		EntityHelper.initEntityNameMap(clz, config);
		Set<EntityColumn> cs=EntityHelper.getColumns(clz);
		StringBuilder sql=new StringBuilder();
		sql.append("<where>");
		for(EntityColumn c:cs){
			if(c.getJavaType().equals(double.class)) {continue;}
			if(c.getJavaType().equals(int.class)) {continue;}
			if(c.getJavaType().equals(float.class)) {continue;}
			if(c.getJavaType().equals(boolean.class)) {continue;}
			sql.append("<if test=\""+c.getProperty()+" != null and "+c.getProperty()+" != '' \"> AND "+c.getColumn()+" = #{"+c.getProperty()+"}</if>");
		}
		sql.append("</where>");
		return sql.toString();
	}
	
	/**
	 * 获取表的所有列,如：
	 * 	 ${alias}.EVENT_ID as eventId,${alias}.EVENT_ID as eventId,
	 * @param tableName
	 * @return
	 */
	public static String getAllColumn(String tableClass) throws ClassNotFoundException{
		return getAllColumn(tableClass,"");
	}
	/**
	 * 获取表的所有列,如：
	 * 	 ${alias}.EVENT_ID as eventId,${alias}.EVENT_ID as eventId,
	 * @param tableName
	 * @return
	 */
	public static String getAllColumn(String tableClass,String alias) throws ClassNotFoundException{
		Class<?> clz = Class.forName(tableClass);
	
		EntityHelper.initEntityNameMap(clz, config);
		String columnStr = EntityHelper.getSelectColumns(clz);
		if(StringUtils.isNotEmpty(alias)){
			columnStr = alias+"."+columnStr.replace(",", ","+alias+".");
		}
		return "<c>"+columnStr+"</c>";
	}
}
