package com.singlee.financial.common.util;

import java.io.*;
import java.util.Vector;

import com.jcraft.jsch.*;
import org.apache.commons.lang3.StringUtils;

/**
 * Ssh2 操作类
 */
public class SingleeJschUtil {

    org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SingleeJschUtil.class);

    Session session = null;
    Channel channel = null;

    public Session login(String ip, int port, String username, String password) throws JSchException {
        JSch jsch = new JSch();
        Logger log = new com.jcraft.jsch.Logger() {
            @Override
            public boolean isEnabled(int i) {
                // 开启、关闭调试
                return false;
            }

            @Override
            public void log(int i, String s) {
                // 打印日志
                System.out.println(s);
            }
        };
        JSch.setLogger(log);
        // getSession()只是创建一个session，需要设置必要的认证信息之后，调用connect()才能建立连接。
        session = jsch.getSession(username, ip, port);
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.setPassword(password);
//		session.setTimeout(60000);
        session.connect();
        return session;
    }

    /**
     * 调用openChannel(String type)
     * 可以在session上打开指定类型的channel。该channel只是被初始化，使用前需要先调用connect()进行连接。
     * Channel的类型可以为如下类型： shell - ChannelShell exec - ChannelExec direct-tcpip -
     * ChannelDirectTCPIP sftp - ChannelSftp subsystem - ChannelSubsystem
     * 其中，ChannelShell和ChannelExec比较类似，都可以作为执行Shell脚本的Channel类型。它们有一个比较重要的区别：ChannelShell可以看作是执行一个交互式的Shell，而ChannelExec是执行一个Shell脚本。
     *
     * @param command
     * @return
     * @throws JSchException
     * @throws IOException
     */
    public String sendCommand(String command) throws JSchException, IOException {
        String result = "";
        channel = session.openChannel("exec");
        ChannelExec execChannel = (ChannelExec) channel;
        execChannel.setCommand(command);
        InputStream in = channel.getInputStream();
        channel.connect();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String buf = null;
            while ((buf = reader.readLine()) != null) {
                result += " \n " + buf;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String updateFile(String dir, String suffix, String commond) throws JSchException, SftpException, IOException {
        String result = "";
        channel = session.openChannel("sftp");
        ChannelSftp sftpChannel = (ChannelSftp) channel;
        channel.connect();

        //进入目录
        sftpChannel.cd(dir);
        //获取文件列表
        Vector<ChannelSftp.LsEntry> vector = sftpChannel.ls(dir);
        //循环处理
        for (ChannelSftp.LsEntry entry : vector) {
            //读取文件流程
            InputStream in = sftpChannel.get(entry.getFilename());
            InputStreamReader inReader = new InputStreamReader(in, "UTF-8");
            BufferedReader bufReader = new BufferedReader(inReader);
            String line = null;
            int i = 1;
            while ((line = bufReader.readLine()) != null) {
                log.debug("开始读取" + i + "行，处理当前==>" + line);
                System.out.println(entry.getFilename() + "==>开始读取" + i + "行，处理当前==>" + line);
                i++;
            }
            //关闭流通道
            bufReader.close();
            inReader.close();
            in.close();
        }
        return "false";
    }

    /**
     * 断开连接
     */
    public void distinct() {
        if (channel != null && !channel.isConnected()) {
            channel.disconnect();
        }
        if (session != null && !session.isConnected()) {
            session.disconnect();
        }
    }


    public static void main(String[] args) throws JSchException, IOException, SftpException {
        SingleeJschUtil jschUtil = new SingleeJschUtil();
        jschUtil.login("192.168.2.21", 22, "OPICS46", "Singlee1234");
        jschUtil.updateFile("/E:/FusionOpics4.6.1.0/OPXDB", "", "");
//		jschUtil.login("183.129.146.22", 2022, "root", "SINGLEE@8848");
//		String ret1 = jschUtil.sendCommand("cd C:\\Program Files (x86)\\Finastra\\FusionOpicsClient");
//		System.out.println(ret1 + ">>>");
//        String ret2 = jschUtil.sendCommand("dir");
//        System.out.println(ret2);
//		jschUtil.distinct();
//        String ret = jschUtil.sendCommand(
//                "Misys.OpicsPlus.Application.Core.Auto.exe -A RUNJOB -U BATH -J D01D0001 -T BSYS -S ORACLEDP30D");
//        System.out.println(ret + "-->");
    }
}
