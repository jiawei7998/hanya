package com.singlee.financial.common.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

import com.singlee.financial.common.spring.SpringApdaterConfig;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.SpringFileUtils;

/**
 * 加载自定义properties配置文件
 * @author chenxh
 *
 */
public class SpringApdaterConfigUtils{
	
	private static Properties properties = new Properties();
	
	public static Properties getProperties() {
		return properties;
	}

	public static String getStrValue(String key) {
		return properties.getProperty(key);
	}
	
	public static int getIntValue(String key) {
		String value = getStrValue(key);
		if(!StringUtils.hasText(key) || !StringUtils.hasText(value)) {
			value = "0";
		}
		return Integer.parseInt(value);
	}
	
	public static BigDecimal getBigValue(String key) {
		String value = getStrValue(key);
		if(!StringUtils.hasText(key) || !StringUtils.hasText(value)) {
			value = "0";
		}
		return new BigDecimal(value);
	}
	
	public static Date getDateValue(String key) {
		String value = getStrValue(key);
		if(!StringUtils.hasText(key) || !StringUtils.hasText(value)) {
			throw new RuntimeException("string date Value is null");
		}
		return DateUtil.parse(value);
	}
	
	/**
	 * 添加键值对数据
	 * @param propertyName
	 * @param propertyValue
	 * @param springApdaterConfig
	 */
	public static void addProperty(String propertyName, String propertyValue,SpringApdaterConfig springApdaterConfig) {
		String value = null;		
		//解密相应的配置项
		if(isDecode(springApdaterConfig.getEncryptKeys(),propertyName,springApdaterConfig.isEncrypt())) {
			value = deCode(propertyValue,springApdaterConfig.getEncryptMethod());
		}else {
			value = propertyValue;
		}
		properties.put(propertyName, value);
	}

	
	/**
	 * 加载自定义配置文件
	 * @param locations
	 * @return
	 */
	public static Resource [] addLocations(String filterPrefix,String filterSuffix,Resource... locations) {
		List<Resource> list = new ArrayList<Resource>();
		Collections.addAll(list, locations);
		List<String> files = null;
		try {
			LogFactory.getLog(SpringApdaterConfigUtils.class).debug(String.format("filter file prefix[%s],suffix[%s]", filterPrefix,filterSuffix));
			if(list.size() ==0 ) {
				files = SpringFileUtils.getClassPathFiles(filterPrefix,filterSuffix);
			}else {
				files = SpringFileUtils.getFiles(list.get(0),filterPrefix,filterSuffix);
			}
			for (String file : files) {
				list.add(new ClassPathResource(file));
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return list.toArray(new Resource[list.size()]);
	}
	
	/**
	 * 解密配置的值
	 * @param value
	 * @param encryptMethod
	 * @return
	 */
	public static String deCode(String value,String encryptMethod) {
		String deCodeValue = null;
		
		switch (encryptMethod.toLowerCase()) {
		case "base64":
			deCodeValue = new String(Base64.getDecoder().decode(value));
			break;
		default:
			deCodeValue = value;
			break;
		}
		return deCodeValue;
	}
	
	/**
	 * 检查该配置是否需要进行解密
	 * @param name
	 * @param encryptName
	 * @return
	 */
	public static boolean isDecode(List<String> encryptKeys,String encryptName,boolean isEncrypt) {
		Optional<String> op = encryptKeys.stream().filter(key -> encryptName.toLowerCase().contains(key.toLowerCase())).findFirst();
		if(op.isPresent() && isEncrypt) {
			return true;
		}else {
			return false;
		}
	}
}
