package com.singlee.financial.report;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.common.util.FileReadAndWriteUtil;
import com.singlee.financial.bean.ReportBean;
import com.singlee.financial.report.IReportServer;
import com.singlee.financial.mapper.ReportMapper;

@Service
public class ReportServer implements IReportServer {

	@Autowired
	ReportMapper reportMapper;

	@Override
	public String getGrssFile(File paramFile) {
		StringBuffer fileJson = new StringBuffer();
		// 获取json 数据层级结构
		FileReadAndWriteUtil.getFilePath(paramFile, fileJson);
		return fileJson.deleteCharAt(fileJson.length() - 1).toString();

	}

	@Override
	public String addGrssFile(String paramFile, String filename) {

		try {
			if (filename != null) {
				FileReadAndWriteUtil.createFile(paramFile, filename);
			} else {
				FileReadAndWriteUtil.createDir(paramFile);
			}
		} catch (IOException e) {
			return e.getMessage();
		}
		return "";
	}

	@Override
	public String delGrssFile(String paramFile, String filename) {
		if (filename != null) {
			FileReadAndWriteUtil.delFile(paramFile + "/" + filename);
		} else {
			FileReadAndWriteUtil.delDir(paramFile);
		}
		return filename;
	}

	@Override
	public String addSqltoFile(String paramSql, String path) {
		try {
			FileReadAndWriteUtil.saveSqlStrToFile(paramSql, path);
		} catch (Exception e) {
			return e.getMessage();
		}
		return "";
	}

	@Override
	public String getSqlfromFile(String path) {
		File file = new File(path);
		try {
			if (!file.exists() || file.isDirectory()) {
				return "";
			}
			if (file.length() <= 0) {
				return null;}
			BufferedReader br = new BufferedReader(new FileReader(file));

			String temp = null;

			StringBuffer sb = new StringBuffer();

			temp = br.readLine();
			while (temp != null) {

				sb.append(temp + "\n");

				temp = br.readLine();
			}
			br.close();
			return sb.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";

	}

	@Override
	public List<LinkedHashMap<String, Object>> runSql(String paramSql) {
		if (paramSql.toUpperCase().contains("UPDATE") || paramSql.toUpperCase().contains("DELETE") || paramSql.toUpperCase().contains("INSERT")) {
			return null;
		}
		if (paramSql == null || "".equals(paramSql)) {
			return null;
		}
		return reportMapper.superManagerSelect(paramSql);
	}

	@Override
	public List<String> getUserTabComments(String paramTabId) {
		return reportMapper.getUserTabComments(paramTabId);
	}

	@Override
	public List<String> getUserTabColums(String paramTableId) {
		return reportMapper.getUserTabColums(paramTableId);
	}

	/**
	 * 导出报表G03_1
	 * 
	 * */
	public List<ReportBean> expG03_1(Map<String, String> params) {
		// 调用存储过程
		reportMapper.reportG03_1(params);
		if ("999".equals(params.get("retcode"))) {
			// 成功，开始获取数据
			ReportBean bean = new ReportBean();
			bean.setOper(params.get("oper"));
			bean.setRpdate(params.get("rpdate"));
			bean.setExcelname("SL_REPORT_G01_3");
			// 获取报表数据
			return reportMapper.serach(bean);
		}
		return null;
	}

	/**
	 * 导出报表G03_2
	 * 
	 * */
	public List<ReportBean> expG03_2(Map<String, String> params) {
		// 调用存储过程
		params = reportMapper.reportG03_2(params);
		if ("999".equals(params.get("retcode"))) {
			// 成功，开始获取数据
			ReportBean bean = new ReportBean();
			bean.setOper(params.get("oper"));
			bean.setRpdate(params.get("rpdate"));
			bean.setExcelname("SL_REPORT_G02_3");
			// 获取报表数据
			return reportMapper.serach(bean);
		}
		return null;
	}

	/**
	 * 导出报表G03_3
	 * 
	 * */
	public List<ReportBean> expG03_3(Map<String, String> params) {
		// 调用存储过程
		params = reportMapper.reportG03_3(params);
		if ("999".equals(params.get("retcode"))) {
			// 成功，开始获取数据
			ReportBean bean = new ReportBean();
			bean.setOper(params.get("oper"));
			bean.setRpdate(params.get("rpdate"));
			bean.setExcelname("SL_REPORT_G25_3");
			// 获取报表数据
			return reportMapper.serach(bean);
		}
		return null;
	}

	/**
	 * 导出报表G03_4
	 * 
	 * */
	public List<ReportBean> expG03_4(Map<String, String> params) {
		// 调用存储过程
		params = reportMapper.reportG03_4(params);
		if ("999".equals(params.get("retcode"))) {
			// 成功，开始获取数据
			ReportBean bean = new ReportBean();
			bean.setOper(params.get("oper"));
			bean.setRpdate(params.get("rpdate"));
			bean.setExcelname("SL_REPORT_G44_3");
			// 获取报表数据
			return reportMapper.serach(bean);
		}
		return null;
	}

	/**
	 * 导出报表G03_5
	 * 
	 * */
	public List<ReportBean> expG03_5(Map<String, String> params) {
		// 调用存储过程
		params = reportMapper.reportG03_5(params);
		if ("999".equals(params.get("retcode"))) {
			// 成功，开始获取数据
			ReportBean bean = new ReportBean();
			bean.setOper(params.get("oper"));
			bean.setRpdate(params.get("rpdate"));
			bean.setExcelname("SL_REPORT_G4B3A_3");
			// 获取报表数据
			return reportMapper.serach(bean);
		}
		return null;
	}

	/**
	 * 导出报表G03_6
	 * 
	 * */
	public List<ReportBean> expG03_6(Map<String, String> params) {
		// 调用存储过程
		params = reportMapper.reportG03_6(params);
		if ("999".equals(params.get("retcode"))) {
			// 成功，开始获取数据
			ReportBean bean = new ReportBean();
			bean.setOper(params.get("oper"));
			bean.setRpdate(params.get("rpdate"));
			bean.setExcelname("SL_REPORT_G01I_3");
			// 获取报表数据
			return reportMapper.serach(bean);
		}
		return null;
	}

}
