package com.singlee.financial.esb.hrbcb;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.SendInfo;
import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.mapper.AcupTotalMapper;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlAcupServer implements ISlAcupServer {

	@Autowired
	AcupTotalMapper acupTotalMapper;
	@Autowired
	BatchDao batchDao;

	@Override
	public List<AcupSendBean> getSlAcupList(Map<String, Object> map) {
		return acupTotalMapper.getSlAcupList(map);
	}

	@Override
	public List<AcupSendBean> getSlAcupBySetNo(Map<String, Object> map) {
		return acupTotalMapper.getRetrySendAcupNew(map);
	}

	@Override
	public void updateBatchSlAcupHendle(List<AcupSendBean> list) {
		batchDao.batch("com.singlee.financial.mapper.AcupTotalMapper.updateAcupHendle", list,100);
	}

	@Override
	public void updateSLAcupSendinfo(SendInfo sendInfo) {
		acupTotalMapper.upAcupInfo(sendInfo);
	}

	@Override
	public SendInfo getSLAcupSendinfo() {
		return acupTotalMapper.getSendFlag();
	}

}
