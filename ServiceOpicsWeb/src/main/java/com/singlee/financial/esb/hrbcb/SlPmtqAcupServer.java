package com.singlee.financial.esb.hrbcb;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.SlPmtqAcupBean;
import com.singlee.financial.mapper.SlPmtqAcupMapper;

import tk.mybatis.mapper.entity.Example;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SlPmtqAcupServer implements ISlPmtqAcupServer{

	@Autowired
	SlPmtqAcupMapper slPmtqAcupMapper;
	
	
	
	@Override
	public List<SlPmtqAcupBean> getSlPmtqAcupList(SlPmtqAcupBean slPmtqAcupBean) {
		return slPmtqAcupMapper.select(slPmtqAcupBean);
	}

	@Override
	public List<SlPmtqAcupBean> getSlPmtqAcupListAndTrim(SlPmtqAcupBean slPmtqAcupBean) {
		return slPmtqAcupMapper.getSlPmtqAcupListAndTrim(slPmtqAcupBean);
	}

	@Override
	public SlPmtqAcupBean getSlPmtqAcup(SlPmtqAcupBean slPmtqAcupBean) {
		return slPmtqAcupMapper.selectOne(slPmtqAcupBean);
	}

	  
	
	@Override
	public void updateSlPmtqAcup(SlPmtqAcupBean slPmtqAcupBean) {
		// 修改状态
		Example example = new Example(SlPmtqAcupBean.class);
		example.createCriteria().andEqualTo("dealno", slPmtqAcupBean.getDealno());
		slPmtqAcupMapper.updateByExampleSelective(slPmtqAcupBean, example);
	}

 
}
