package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Rate;
import com.singlee.financial.wks.mapper.opics.RateMapper;
import com.singlee.financial.wks.service.opics.RateService;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

@Service
public class RateServiceImpl implements RateService {

	@Resource
	private RateMapper rateMapper;

	@Override
	public int deleteByPrimaryKey(String br, String ratecode) {
		return rateMapper.deleteByPrimaryKey(br, ratecode);
	}

	@Override
	public int insert(Rate record) {
		return rateMapper.insert(record);
	}

	@Override
	public int insertSelective(Rate record) {
		return rateMapper.insertSelective(record);
	}

	@Override
	public Rate selectByPrimaryKey(String br, String ratecode) {
		return rateMapper.selectByPrimaryKey(br, ratecode);
	}

	@Override
	public Rate selectByCcy(String br, String ccy, String varfix) {
		List<Rate> rateLst = rateMapper.selectByCcy(br, ccy, varfix);
		if (CollectionUtils.isNotEmpty(rateLst)) {
			return rateLst.get(0);
		}
		return null;
	}

	@Override
	public Rate selectByIsda(String br, String ccy, String ratesourceIsda) {
		List<Rate> rateLst = rateMapper.selectByIsda(br, ccy, ratesourceIsda);
		if (CollectionUtils.isNotEmpty(rateLst)) {
			return rateLst.get(0);
		}
		return null;
	}

	@Override
	public int updateByPrimaryKeySelective(Rate record) {
		return rateMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Rate record) {
		return rateMapper.updateByPrimaryKey(record);
	}

}
