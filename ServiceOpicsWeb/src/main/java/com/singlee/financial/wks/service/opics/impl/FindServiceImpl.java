package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Fees;
import com.singlee.financial.wks.bean.opics.Find;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.bean.opics.Swdt;
import com.singlee.financial.wks.mapper.opics.FindMapper;
import com.singlee.financial.wks.service.opics.FindService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FindServiceImpl implements FindService {

    @Resource
    private FindMapper findMapper;
    
    @Resource
    private BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br,String dealno,String seq,String product,String prodtype,String incexptype) {
        return findMapper.deleteByPrimaryKey(br,dealno,seq,product,prodtype,incexptype);
    }

    @Override
    public int insert(Find record) {
        return findMapper.insert(record);
    }

    @Override
    public int insertSelective(Find record) {
        return findMapper.insertSelective(record);
    }

    @Override
    public Find selectByPrimaryKey(String br,String dealno,String seq,String product,String prodtype,String incexptype) {
        return findMapper.selectByPrimaryKey(br,dealno,seq,product,prodtype,incexptype);
    }

    @Override
    public int updateByPrimaryKeySelective(Find record) {
        return findMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Find record) {
        return findMapper.updateByPrimaryKey(record);
    }
    
    private Find bulider(String br,String dealno,String seq,String product,String prodtype,String incexptype,String ccy,BigDecimal tdyincexp,
    		BigDecimal ystincexp,BigDecimal mtdincexp,BigDecimal ytdincexp,BigDecimal mtdpincexp,BigDecimal ytdpincexp,BigDecimal accroutst,
    		BigDecimal intpaid,Date lstmntdate) {
    	Find find = new Find();
    	
    	find.setBr(br);
    	find.setDealno(dealno);
    	find.setSeq(seq);
    	find.setProduct(product);
    	find.setProdtype(prodtype);
    	find.setIncexptype(incexptype);
    	find.setCcy(ccy);
    	find.setTdyincexp(tdyincexp);
    	find.setYstincexp(ystincexp);
    	find.setMtdincexp(mtdincexp);
    	find.setYtdincexp(ytdincexp);
    	find.setMtdpincexp(mtdpincexp);
    	find.setYtdpincexp(ytdpincexp);
    	find.setAccroutst(accroutst);
    	find.setIntpaid(intpaid);
    	find.setLstmntdate(lstmntdate);
    	
    	return find;
    }

    @Override
    public void batchInsert(List<Find> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.FindMapper.insert", list);
    }
    @Override
    public Find bulider(String br,String dealno,String seq,String product,String prodtype,String incexptype,String ccy) {
    	return bulider(br,dealno,seq,product,prodtype,incexptype,ccy,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,
    			BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO,new Date());
    }

	@Override
	public List<Find> bulider(Swdh swdh,List<Swdt> swdhLeg) {
		List<Find> finds = new ArrayList<Find>();
		String ccy = null;
		

		// 添加每条腿的信息
		for (Swdt swdt : swdhLeg) {
			//添加估值
			finds.add(bulider(swdt.getBr(),swdt.getDealno(),swdt.getSeq(),swdt.getProduct(),swdt.getProdtype(),"M",swdt.getIntccy()));
			
			//添加计提
			finds.add(bulider(swdt.getBr(),swdt.getDealno(),swdt.getSeq(),swdt.getProduct(),swdt.getProdtype(),"A",swdt.getIntccy()));
			
			ccy = swdt.getIntccy();
		}
		
		// 添加头信息
		finds.add(bulider(swdh.getBr(),swdh.getDealno(),swdh.getSeq(),swdh.getProduct(),swdh.getProdtype(),"M",ccy));

		return finds;
	}

    @Override
    public Find bulider(Fees fees) {
        return bulider(fees.getBr(),fees.getFeeno(),fees.getSeq(),fees.getFeeproduct(),fees.getFeeprodtype()
                ,"D",fees.getCcy());
    }

}
