package com.singlee.financial.wks.util;

import com.singlee.financial.wks.expand.FieldMapType;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.Map.Entry;

public class FinancialFieldMap extends PropertyPlaceholderConfigurer {

	private Map<String,Map<String,List<String>>> propertyMap = new HashMap<String,Map<String,List<String>>>();
	
	private Map<String,Map<String,Map<String,String>>> fieldMaps = new HashMap<String,Map<String,Map<String,String>>>();

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess, Properties props)
			throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		getProperty(propertyMap,fieldMaps,props);
	}

	/**
	 * 将Properties文件内容转换为Map<String, String>对象
	 * @param fieldName
	 * @param fieldValue
	 * @param props
	 */
	private void getProperty(Map<String,Map<String,List<String>>> fieldName,Map<String,Map<String,Map<String,String>>> fieldValue,Properties props) {
		Map<String,List<String>> fieldNames = null;
		Map<String,Map<String,String>> fieldValues = null;
		String key = null;
		String [] keys = null;
		String [] values = null;
		String value = null;
		List<String> fieldList= null;
		Map<String,String> fieldMap = null;
		int size;
		for (Entry<Object, Object> properties : props.entrySet()) {
			key = properties.getKey().toString();
			value = properties.getValue().toString();
			size = 0;
			
			keys = key.split("\\.");
			
			if(null == keys) {
				throw new RuntimeException("fieldmap.properties配置文件中配置项数据不合法");
			}
			size = keys.length;
			if(size != 2 && size != 4) {
				throw new RuntimeException("fieldmap.properties配置文件中配置项数据不合法,格式只能为[A.B.C.D=C]或者是[A.B=C]");
			}
			
			if(2 == size) {
				//添加实体对象的配置信息
				fieldNames = fieldName.get(keys[0]);
				if(null == fieldNames) {
					fieldNames = new HashMap<String,List<String>>();
					fieldName.put(keys[0], fieldNames);
				}
				
				//添加实体对象需要转换的字段
				fieldList = fieldNames.get(keys[1]);
				if(null == fieldList) {
					fieldList = new ArrayList<String>();
					fieldNames.put(keys[1], fieldList);
				}
				//添加转换字段对应对象的所有属性
				values = value.split(",");
				for (String value_key : values) {
					fieldList.add(value_key);
				}//end for
			}else {
				fieldValues = fieldValue.get(keys[0]);
				
				if(null == fieldValues) {
					fieldValues = new HashMap<String,Map<String,String>>();
					fieldValue.put(keys[0], fieldValues);
				}
				fieldMap = fieldValues.get(keys[1]);
				
				if(null == fieldMap) {
					fieldMap = new HashMap<String,String>();
					fieldValues.put(keys[1], fieldMap);
				}
				
				fieldMap.put(keys[2]+"."+keys[3], value);
			}//end if else			
		}//end for
	}
	
	/**
	 * 根据实体对象自动处理需要转换的枚举值
	 * @param obj
	 * @return
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 */
	public void convertFieldvalue(Object obj){
		String fieldMapKey = null;
		Map<String,List<String>> fieldNames = null;
		Map<String,String> fieldValues = null;
		String beanName = null;
		String fieldMapvalue = null;
		String [] keys = null;
		List<String> field_list = null;
		FieldMapType fieldMapType = null;
		
		try {
			if(null == obj) {
				throw new RuntimeException("FieldMap中需要转换的实体对象为空");
			}
			
			//获取对象类名
			beanName = obj.getClass().getSimpleName();
			
			//获取当对象属性名称转换配置
			fieldNames = propertyMap.get(beanName);
			
			//获取字段转换对应的系统
			fieldMapType = getFieldMapType(obj);
			
			//OPICS系统枚举值不进行转换
			if(fieldMapType.equals(FieldMapType.OPICS)) {
				return;
			}
			
			//获取当前对象属性值转换配置
			fieldValues = fieldMaps.get(beanName).get(fieldMapType.name());
			
			//遍历需要转换的枚举进行替换
			for (Entry<String, String> var : fieldValues.entrySet()) {
				keys = var.getKey().split("\\.");
				fieldMapKey = keys[0];
				fieldMapvalue = var.getValue();
				
				//获取属性名称转换后的值
				field_list = fieldNames.get(fieldMapKey);
				
				//字段没有转换时,直接获取对象属性
				if(null == field_list || field_list.size() == 0) {
					convertFieldvalue(obj,fieldMapKey,fieldMapvalue);
				}else {
					//当对象属性进行了转换,则循环处理被转换的属性
					for (String fieldName_str : field_list) {
						convertFieldvalue(fieldValues,obj,fieldName_str,fieldMapKey);
					}//end for
				}//end if else
			}//end for
		} catch (SecurityException e) {
			throw new RuntimeException("FieldMap.SecurityException",e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("FieldMap.IllegalArgumentException",e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("FieldMap.IllegalAccessException",e);
		} catch (NoSuchFieldException e) {
			throw new RuntimeException("FieldMap.NoSuchFieldException",e);
		}
	}
	
	/**
	 * 获取枚举值字段的匹配值
	 * @param obj
	 * @return
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	private FieldMapType getFieldMapType(Object obj) throws SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = null;
		FieldMapType fieldMapType = null;
		// 获取对象字段
		try {
			field = obj.getClass().getDeclaredField("fieldMapType");
		} catch (NoSuchFieldException e) {
			//继续获取父类
			try {
				field = obj.getClass().getSuperclass().getDeclaredField("fieldMapType");
			} catch (NoSuchFieldException | SecurityException e1) {
				throw new RuntimeException("自动转换枚举值处理方法,对象[ "+obj.getClass().getSimpleName()+" ]中必须包含com.singlee.financial.wks.bean.FieldMapType枚举类型的属性[ fieldMapType ]");
			}
		}

		// 设置是否允许访问，不是修改原来的访问权限修饰词。
		field.setAccessible(true);
		
		// 属性进行转换后需组装键值
		fieldMapType = (FieldMapType) field.get(obj);
		
		field.setAccessible(false);
		
		if(null == fieldMapType) {
			throw new RuntimeException("自动转换枚举值处理方法,对象[ "+obj.getClass().getSimpleName()+" ]的属性[ fieldMapType ]字段未赋值");
		}

		// 属性进行转换后需组装键值
		return fieldMapType;
	}
	
	/**
	 * 方法已重载.转换对象指定字段的值
	 * 
	 * @param fieldValues
	 * @param obj
	 * @param field
	 * @param fieldName
	 * @param value
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	private void convertFieldvalue(Map<String, String> fieldValues, Object obj, String fieldName, String fieldMapKey)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		String key = null;
		String temp;
		String old_value = null;
		Field field = null;

		// 获取对象字段
		field = obj.getClass().getDeclaredField(fieldName);

		// 设置是否允许访问，不是修改原来的访问权限修饰词。
		field.setAccessible(true);

		// 属性进行转换后需组装键值
		old_value = Objects.toString(field.get(obj));
		// 拼接映射KEY
		key = fieldMapKey + "." + old_value;

		//获取KEY对象的值
		temp = fieldValues.get(key);

		if (null != temp && temp.trim().length() > 0) {
			// 转换对应枚举值
			field.set(obj, temp);
		} // end if

		// 恢复访问权限
		field.setAccessible(false);
	}

	/**
	 * 方法已重载.转换对象指定字段的值
	 * @param obj
	 * @param fieldName
	 * @param fieldMapValue
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 */
	private void convertFieldvalue(Object obj, String fieldName, String fieldMapValue)
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		Field field = null;

		// 获取对象字段
		field = obj.getClass().getDeclaredField(fieldName);

		// 设置是否允许访问，不是修改原来的访问权限修饰词。
		field.setAccessible(true);

		// 转换对应枚举值
		field.set(obj, fieldMapValue);

		// 恢复访问权限
		field.setAccessible(false);
	}
}