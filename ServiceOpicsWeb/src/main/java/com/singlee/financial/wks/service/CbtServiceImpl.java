package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.CbtWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.intfc.CbtService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.CbtEntry;
import com.singlee.financial.wks.service.trans.CbtReverse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CbtServiceImpl implements CbtService {

    @Autowired
    private SpshService spshService;
    @Autowired
    private DrpiService drpiService;
    @Autowired
    private McfpService mcfpService;
    @Autowired
    private CnfqService cnfqService;
    @Autowired
    private NupdService nupdService;
    @Autowired
    private PmtqService pmtqService;
    @Autowired
    private PcixService pcixService;
    @Autowired
    private PsixService psixService;
    @Autowired
    private PrirService prirService;
    @Autowired
    private TposService tposService;
    @Autowired
    private SposService sposService;
    @Autowired
    private BrpsService brpsService;
    @Autowired
    private CbtEntry cbtEntry;// 首期交易录入
    @Autowired
    private CbtReverse cbtReverse;// 冲销转换

    private static Logger logger = Logger.getLogger(CbtServiceImpl.class);

    @Override
    public SlOutBean saveEntry(CbtWksBean cbt) {
        SlOutBean outBean =
            new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(), WksErrorCode.SUCCESS.getErrMsg());
        Spsh spsh = null;//债券交易表
        Drpi drpi = null;//债券
        Tpos tpos = null;//日终头寸
        Spos spos = null;//日间头寸
        try {
            // 1.交易要素检查
            cbtEntry.DataCheck(cbt);
            // 2.转换交易主体数据
            spsh = cbtEntry.DataTrans(new Spsh());// 静态参数转换
            cbtEntry.DataTrans(spsh, cbt);// 设置交易信息
            cbtEntry.DataTrans(spsh, mcfpService);// 交易编号
            cbtEntry.DataTrans(spsh, brpsService);// 设置账务日期

            spshService.insert(spsh);
            // 转换drpi数据
            drpi = new Drpi();
            cbtEntry.DataTrans(drpi, spsh);
            drpiService.insert(drpi);

            // 往来帐信息nupd
            List<Nupd> nupdLst = nupdService.builder(spsh);
            if (nupdLst.size() > 0) {
                nupdService.batchInsert(nupdLst);
            }
            // 5.收付款信息
            List<Pmtq> pmtqLst = pmtqService.builder(spsh);
            if (pmtqLst.size() > 0) {
                pmtqService.batchInsert(pmtqLst);
            }
            // 6.对手方清算信息
            List<Pcix> pcixLst = pcixService.builder(spsh, cbt.getSettleAcct());
            if (pcixLst.size() > 0) {
                pcixService.batchInsert(pcixLst);
            }
            // 7.收款信息
            prirService.insert(prirService.builder(spsh));
            // 8.付款信息
            psixService.insert(psixService.builder(spsh));
            // 9.确认信息
            cnfqService.insert(cnfqService.bulider(spsh));
            // 10.回填dealno
            cbtEntry.DataTrans(spsh, mcfpService, new Mcfp());

            // 更新SPOS:SPOS当日无记录INSERT;有记录执行如下存储过程，更新语句
            spos = sposService.selectByPrimaryKey(spsh.getBr(), spsh.getCcy(), spsh.getPort(), spsh.getSecid(),
                spsh.getSettdate(), spsh.getInvtype(), spsh.getCost());
            if (spos == null) {
                sposService.insert(sposService.builder(spsh));
            } else {
                // 调用存储过程SP_UPDREC_SPOS_INCR
                sposService.callSpUpdrecSposIncr(spsh.getBr(), spsh.getCcy(), spsh.getPort(), spsh.getSecid(),
                    spsh.getSettdate(), spsh.getInvtype(), spsh.getCost(), spsh.getSettamt(),
                    Calendar.getInstance().getTime(),
                    "P".equalsIgnoreCase(spsh.getPs()) ? spsh.getQty() : spsh.getQty().negate());
            }

            // 更新TPOS：TPOS里面当前投组记录为空才插入，有记录不插入
            tpos = tposService.selectByPrimaryKeySpsh(spsh.getBr(), spsh.getCost(), spsh.getCcy(), spsh.getInvtype(),
                spsh.getPort(), spsh.getSecid(), spsh.getProduct(), spsh.getProdtype());
            if (tpos == null) {
                tposService.insert(tposService.builder(spsh));
            }

        } catch (Exception e) {
            outBean.setRetStatus(RetStatusEnum.F);
            outBean.setRetMsg(e.getMessage());
            logger.error("现券买卖交易录入错误", e);
            throw e;
        }
        outBean.setClientNo(spsh.getDealno());
        logger.info("现券买卖交易处理完成,返回消息:" + JSON.toJSONString(outBean));
        return outBean;
    }

    @Override
    public SlOutBean reverseTrading(CbtWksBean cbt) {
        SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "CBT-00000", "现券买卖交易冲销成功!");
        Spsh spsh = null;
        try {
            // cbt.getTradeNo()为opics dealno
            cbtReverse.DataCheck(cbt);
            // 1.查询交易
            spsh = spshService.selectByTradeNo(cbt.getInstId(), cbt.getTradeNo());
            // 1.1修改SPSH
            cbtReverse.DataTrans(spsh, brpsService);
            spshService.updateByPrimaryKey(spsh);

            // 3.1删除NUPD的记录
            nupdService.callSpDelrecNupdVdate(spsh.getBr(), spsh.getDealno(), spsh.getSeq(), spsh.getProduct(),
                spsh.getProdtype(), spsh.getSettdate(), spsh.getCcysacct());
            // 3.2根据冲销时间节点新增NUPD
            nupdService.callSpAddrecNupd(spsh.getBr(), spsh.getCcysacct(), spsh.getDealno(), spsh.getSeq(),
                spsh.getProduct(), spsh.getProdtype(), spsh.getCno(), spsh.getSettdate(), spsh.getRevdate(),
                spsh.getSettamt().negate());

            // 4.1删除大于新到期日的记录
            pmtqService.callSpUpdrecPmtqSwiftfmt(spsh.getBr(), spsh.getDealno(), spsh.getSeq(), spsh.getProduct(),
                spsh.getProdtype(), spsh.getRevdate(), DateUtil.getCurrentTimeAsString(), "", "", "", "", null,
                BigDecimal.ZERO);

            // 更新SPOS头寸
            sposService.callSpUpdrecSposIncr(spsh.getBr(), spsh.getCcy(), spsh.getPort(), spsh.getSecid(),
                spsh.getSettdate(), spsh.getInvtype(), spsh.getCost(), spsh.getSettamt().negate(),
                Calendar.getInstance().getTime(),
                "P".equalsIgnoreCase(spsh.getPs()) ? spsh.getQty().negate() : spsh.getQty());
        } catch (Exception e) {
            outBean.setRetStatus(RetStatusEnum.F);
            outBean.setRetMsg(e.getMessage());
            logger.error("现券买卖交易冲销错误", e);
            throw e;
        }
        outBean.setClientNo(spsh.getDealno());
        logger.info("现券买卖交易冲销处理完成,返回消息:" + JSON.toJSONString(outBean));
        return outBean;
    }

}
