package com.singlee.financial.wks.service.opics.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.singlee.financial.wks.bean.opics.Ycim;
import com.singlee.financial.wks.mapper.opics.YcimMapper;
import com.singlee.financial.wks.service.opics.YcimService;
@Service
public class YcimServiceImpl implements YcimService{

    @Resource
    private YcimMapper ycimMapper;

    @Override
    public int deleteByPrimaryKey(String br,String ccy,String contribrate) {
        return ycimMapper.deleteByPrimaryKey(br,ccy,contribrate);
    }

    @Override
    public int insert(Ycim record) {
        return ycimMapper.insert(record);
    }

    @Override
    public int insertSelective(Ycim record) {
        return ycimMapper.insertSelective(record);
    }

    @Override
    public Ycim selectByPrimaryKey(String br,String ccy,String contribrate) {
        return ycimMapper.selectByPrimaryKey(br,ccy,contribrate);
    }

    @Override
    public int updateByPrimaryKeySelective(Ycim record) {
        return ycimMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Ycim record) {
        return ycimMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateBatch(List<Ycim> list) {
        return ycimMapper.updateBatch(list);
    }

    @Override
    public int updateBatchSelective(List<Ycim> list) {
        return ycimMapper.updateBatchSelective(list);
    }

}
