package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Schd implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String product;

    private String prodtype;

    private Date intenddte;

    private String schdseq;

    private String schdtype;

    private String ccy;

    private String compound;

    private BigDecimal totpayamt;

    private BigDecimal prinamt;

    private BigDecimal intamt;

    private BigDecimal posnintamt;

    private Date intstrtdte;

    private Date ipaydate;

    private String automanpay;

    private Date raterevdte;

    private Date ratefixdte;

    private String smeans;

    private String sacct;

    private String ratecode;

    private BigDecimal intrate8;

    private String basis;

    private BigDecimal spread8;

    private Date revdate;

    private Date verdate;

    private Date manpaydte;

    private BigDecimal ppayamt;

    private String al;

    private Date lstmntdate;

    private Date brprcindte;

    private BigDecimal notprin;

    private BigDecimal whtrate8;

    private String whtbasis;

    private BigDecimal whtamt;

    private String whtind;

    private BigDecimal vatamt;

    private BigDecimal vatrate8;

    private BigDecimal fundrate8;

    private String fundbasis;

    private BigDecimal fundamt;

    private Date ppaydate;

    private Date payauthdte;

    private String payauthind;

    private String intccy;

    private BigDecimal intexcharate8;

    private String terms;

    private BigDecimal posnfundamt;

    private String intsmeans;

    private String intsacct;

    private String intauthind;

    private String intauthoper;

    private Date intauthdate;

    private String capintind;

    private BigDecimal capintamt;

    private BigDecimal captaxamt;

    private BigDecimal appliedmonthall;

    private BigDecimal posnprinamt;

    private String rateamtint;

    private String rateamtfund;

    private String rateamttax1;

    private String rateamttax2;

    private String rateamttax3;

    private String text1;

    private String text2;

    private Date date1;

    private Date date2;

    private BigDecimal nextappmonthall;

    private BigDecimal amount2;

    private String intoper;

    private String prinoper;

    private String prinauthoper;

    private String netsiind;

    private String netdealno;

    private String netseq;

    private String corpnetind;

    private String prinusualid;

    private String intusualid;

    private BigDecimal amount1;

    private BigDecimal rate1;

    private BigDecimal rate2;

    private String taxstatusind;

    private BigDecimal stepupcorrrate8;

    private BigDecimal stepupcorrintamt;

    private BigDecimal intadjamt;

    private Date faildate;

    private Date origpaydate;

    private BigDecimal origppayamt;

    private BigDecimal origintpayamt;

    private Date failpaiddate;

    private BigDecimal failintpaidamt;

    private BigDecimal failprinpaidamt;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public Date getIntenddte() {
		return intenddte;
	}


	public void setIntenddte(Date intenddte) {
		this.intenddte = intenddte;
	}


	public String getSchdseq() {
		return schdseq;
	}


	public void setSchdseq(String schdseq) {
		this.schdseq = schdseq;
	}


	public String getSchdtype() {
		return schdtype;
	}


	public void setSchdtype(String schdtype) {
		this.schdtype = schdtype;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getCompound() {
		return compound;
	}


	public void setCompound(String compound) {
		this.compound = compound;
	}


	public BigDecimal getTotpayamt() {
		return totpayamt;
	}


	public void setTotpayamt(BigDecimal totpayamt) {
		this.totpayamt = totpayamt;
	}


	public BigDecimal getPrinamt() {
		return prinamt;
	}


	public void setPrinamt(BigDecimal prinamt) {
		this.prinamt = prinamt;
	}


	public BigDecimal getIntamt() {
		return intamt;
	}


	public void setIntamt(BigDecimal intamt) {
		this.intamt = intamt;
	}


	public BigDecimal getPosnintamt() {
		return posnintamt;
	}


	public void setPosnintamt(BigDecimal posnintamt) {
		this.posnintamt = posnintamt;
	}


	public Date getIntstrtdte() {
		return intstrtdte;
	}


	public void setIntstrtdte(Date intstrtdte) {
		this.intstrtdte = intstrtdte;
	}


	public Date getIpaydate() {
		return ipaydate;
	}


	public void setIpaydate(Date ipaydate) {
		this.ipaydate = ipaydate;
	}


	public String getAutomanpay() {
		return automanpay;
	}


	public void setAutomanpay(String automanpay) {
		this.automanpay = automanpay;
	}


	public Date getRaterevdte() {
		return raterevdte;
	}


	public void setRaterevdte(Date raterevdte) {
		this.raterevdte = raterevdte;
	}


	public Date getRatefixdte() {
		return ratefixdte;
	}


	public void setRatefixdte(Date ratefixdte) {
		this.ratefixdte = ratefixdte;
	}


	public String getSmeans() {
		return smeans;
	}


	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}


	public String getSacct() {
		return sacct;
	}


	public void setSacct(String sacct) {
		this.sacct = sacct;
	}


	public String getRatecode() {
		return ratecode;
	}


	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}


	public BigDecimal getIntrate8() {
		return intrate8;
	}


	public void setIntrate8(BigDecimal intrate8) {
		this.intrate8 = intrate8;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public BigDecimal getSpread8() {
		return spread8;
	}


	public void setSpread8(BigDecimal spread8) {
		this.spread8 = spread8;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public Date getManpaydte() {
		return manpaydte;
	}


	public void setManpaydte(Date manpaydte) {
		this.manpaydte = manpaydte;
	}


	public BigDecimal getPpayamt() {
		return ppayamt;
	}


	public void setPpayamt(BigDecimal ppayamt) {
		this.ppayamt = ppayamt;
	}


	public String getAl() {
		return al;
	}


	public void setAl(String al) {
		this.al = al;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public BigDecimal getNotprin() {
		return notprin;
	}


	public void setNotprin(BigDecimal notprin) {
		this.notprin = notprin;
	}


	public BigDecimal getWhtrate8() {
		return whtrate8;
	}


	public void setWhtrate8(BigDecimal whtrate8) {
		this.whtrate8 = whtrate8;
	}


	public String getWhtbasis() {
		return whtbasis;
	}


	public void setWhtbasis(String whtbasis) {
		this.whtbasis = whtbasis;
	}


	public BigDecimal getWhtamt() {
		return whtamt;
	}


	public void setWhtamt(BigDecimal whtamt) {
		this.whtamt = whtamt;
	}


	public String getWhtind() {
		return whtind;
	}


	public void setWhtind(String whtind) {
		this.whtind = whtind;
	}


	public BigDecimal getVatamt() {
		return vatamt;
	}


	public void setVatamt(BigDecimal vatamt) {
		this.vatamt = vatamt;
	}


	public BigDecimal getVatrate8() {
		return vatrate8;
	}


	public void setVatrate8(BigDecimal vatrate8) {
		this.vatrate8 = vatrate8;
	}


	public BigDecimal getFundrate8() {
		return fundrate8;
	}


	public void setFundrate8(BigDecimal fundrate8) {
		this.fundrate8 = fundrate8;
	}


	public String getFundbasis() {
		return fundbasis;
	}


	public void setFundbasis(String fundbasis) {
		this.fundbasis = fundbasis;
	}


	public BigDecimal getFundamt() {
		return fundamt;
	}


	public void setFundamt(BigDecimal fundamt) {
		this.fundamt = fundamt;
	}


	public Date getPpaydate() {
		return ppaydate;
	}


	public void setPpaydate(Date ppaydate) {
		this.ppaydate = ppaydate;
	}


	public Date getPayauthdte() {
		return payauthdte;
	}


	public void setPayauthdte(Date payauthdte) {
		this.payauthdte = payauthdte;
	}


	public String getPayauthind() {
		return payauthind;
	}


	public void setPayauthind(String payauthind) {
		this.payauthind = payauthind;
	}


	public String getIntccy() {
		return intccy;
	}


	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}


	public BigDecimal getIntexcharate8() {
		return intexcharate8;
	}


	public void setIntexcharate8(BigDecimal intexcharate8) {
		this.intexcharate8 = intexcharate8;
	}


	public String getTerms() {
		return terms;
	}


	public void setTerms(String terms) {
		this.terms = terms;
	}


	public BigDecimal getPosnfundamt() {
		return posnfundamt;
	}


	public void setPosnfundamt(BigDecimal posnfundamt) {
		this.posnfundamt = posnfundamt;
	}


	public String getIntsmeans() {
		return intsmeans;
	}


	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}


	public String getIntsacct() {
		return intsacct;
	}


	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}


	public String getIntauthind() {
		return intauthind;
	}


	public void setIntauthind(String intauthind) {
		this.intauthind = intauthind;
	}


	public String getIntauthoper() {
		return intauthoper;
	}


	public void setIntauthoper(String intauthoper) {
		this.intauthoper = intauthoper;
	}


	public Date getIntauthdate() {
		return intauthdate;
	}


	public void setIntauthdate(Date intauthdate) {
		this.intauthdate = intauthdate;
	}


	public String getCapintind() {
		return capintind;
	}


	public void setCapintind(String capintind) {
		this.capintind = capintind;
	}


	public BigDecimal getCapintamt() {
		return capintamt;
	}


	public void setCapintamt(BigDecimal capintamt) {
		this.capintamt = capintamt;
	}


	public BigDecimal getCaptaxamt() {
		return captaxamt;
	}


	public void setCaptaxamt(BigDecimal captaxamt) {
		this.captaxamt = captaxamt;
	}


	public BigDecimal getAppliedmonthall() {
		return appliedmonthall;
	}


	public void setAppliedmonthall(BigDecimal appliedmonthall) {
		this.appliedmonthall = appliedmonthall;
	}


	public BigDecimal getPosnprinamt() {
		return posnprinamt;
	}


	public void setPosnprinamt(BigDecimal posnprinamt) {
		this.posnprinamt = posnprinamt;
	}


	public String getRateamtint() {
		return rateamtint;
	}


	public void setRateamtint(String rateamtint) {
		this.rateamtint = rateamtint;
	}


	public String getRateamtfund() {
		return rateamtfund;
	}


	public void setRateamtfund(String rateamtfund) {
		this.rateamtfund = rateamtfund;
	}


	public String getRateamttax1() {
		return rateamttax1;
	}


	public void setRateamttax1(String rateamttax1) {
		this.rateamttax1 = rateamttax1;
	}


	public String getRateamttax2() {
		return rateamttax2;
	}


	public void setRateamttax2(String rateamttax2) {
		this.rateamttax2 = rateamttax2;
	}


	public String getRateamttax3() {
		return rateamttax3;
	}


	public void setRateamttax3(String rateamttax3) {
		this.rateamttax3 = rateamttax3;
	}


	public String getText1() {
		return text1;
	}


	public void setText1(String text1) {
		this.text1 = text1;
	}


	public String getText2() {
		return text2;
	}


	public void setText2(String text2) {
		this.text2 = text2;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public BigDecimal getNextappmonthall() {
		return nextappmonthall;
	}


	public void setNextappmonthall(BigDecimal nextappmonthall) {
		this.nextappmonthall = nextappmonthall;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public String getIntoper() {
		return intoper;
	}


	public void setIntoper(String intoper) {
		this.intoper = intoper;
	}


	public String getPrinoper() {
		return prinoper;
	}


	public void setPrinoper(String prinoper) {
		this.prinoper = prinoper;
	}


	public String getPrinauthoper() {
		return prinauthoper;
	}


	public void setPrinauthoper(String prinauthoper) {
		this.prinauthoper = prinauthoper;
	}


	public String getNetsiind() {
		return netsiind;
	}


	public void setNetsiind(String netsiind) {
		this.netsiind = netsiind;
	}


	public String getNetdealno() {
		return netdealno;
	}


	public void setNetdealno(String netdealno) {
		this.netdealno = netdealno;
	}


	public String getNetseq() {
		return netseq;
	}


	public void setNetseq(String netseq) {
		this.netseq = netseq;
	}


	public String getCorpnetind() {
		return corpnetind;
	}


	public void setCorpnetind(String corpnetind) {
		this.corpnetind = corpnetind;
	}


	public String getPrinusualid() {
		return prinusualid;
	}


	public void setPrinusualid(String prinusualid) {
		this.prinusualid = prinusualid;
	}


	public String getIntusualid() {
		return intusualid;
	}


	public void setIntusualid(String intusualid) {
		this.intusualid = intusualid;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public BigDecimal getRate1() {
		return rate1;
	}


	public void setRate1(BigDecimal rate1) {
		this.rate1 = rate1;
	}


	public BigDecimal getRate2() {
		return rate2;
	}


	public void setRate2(BigDecimal rate2) {
		this.rate2 = rate2;
	}


	public String getTaxstatusind() {
		return taxstatusind;
	}


	public void setTaxstatusind(String taxstatusind) {
		this.taxstatusind = taxstatusind;
	}


	public BigDecimal getStepupcorrrate8() {
		return stepupcorrrate8;
	}


	public void setStepupcorrrate8(BigDecimal stepupcorrrate8) {
		this.stepupcorrrate8 = stepupcorrrate8;
	}


	public BigDecimal getStepupcorrintamt() {
		return stepupcorrintamt;
	}


	public void setStepupcorrintamt(BigDecimal stepupcorrintamt) {
		this.stepupcorrintamt = stepupcorrintamt;
	}


	public BigDecimal getIntadjamt() {
		return intadjamt;
	}


	public void setIntadjamt(BigDecimal intadjamt) {
		this.intadjamt = intadjamt;
	}


	public Date getFaildate() {
		return faildate;
	}


	public void setFaildate(Date faildate) {
		this.faildate = faildate;
	}


	public Date getOrigpaydate() {
		return origpaydate;
	}


	public void setOrigpaydate(Date origpaydate) {
		this.origpaydate = origpaydate;
	}


	public BigDecimal getOrigppayamt() {
		return origppayamt;
	}


	public void setOrigppayamt(BigDecimal origppayamt) {
		this.origppayamt = origppayamt;
	}


	public BigDecimal getOrigintpayamt() {
		return origintpayamt;
	}


	public void setOrigintpayamt(BigDecimal origintpayamt) {
		this.origintpayamt = origintpayamt;
	}


	public Date getFailpaiddate() {
		return failpaiddate;
	}


	public void setFailpaiddate(Date failpaiddate) {
		this.failpaiddate = failpaiddate;
	}


	public BigDecimal getFailintpaidamt() {
		return failintpaidamt;
	}


	public void setFailintpaidamt(BigDecimal failintpaidamt) {
		this.failintpaidamt = failintpaidamt;
	}


	public BigDecimal getFailprinpaidamt() {
		return failprinpaidamt;
	}


	public void setFailprinpaidamt(BigDecimal failprinpaidamt) {
		this.failprinpaidamt = failprinpaidamt;
	}


	private static final long serialVersionUID = 1L;
}