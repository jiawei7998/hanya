package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Ycrs implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String br;

    private String ccy;

    private String yieldcurve;

    private String contribrate;

    private String mtystart;

    private String mtyend;

    private String delvcode;

    private String ratetype;

    private Date lstmntdte;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getYieldcurve() {
		return yieldcurve;
	}

	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}

	public String getContribrate() {
		return contribrate;
	}

	public void setContribrate(String contribrate) {
		this.contribrate = contribrate;
	}

	public String getMtystart() {
		return mtystart;
	}

	public void setMtystart(String mtystart) {
		this.mtystart = mtystart;
	}

	public String getMtyend() {
		return mtyend;
	}

	public void setMtyend(String mtyend) {
		this.mtyend = mtyend;
	}

	public String getDelvcode() {
		return delvcode;
	}

	public void setDelvcode(String delvcode) {
		this.delvcode = delvcode;
	}

	public String getRatetype() {
		return ratetype;
	}

	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
    
    
}