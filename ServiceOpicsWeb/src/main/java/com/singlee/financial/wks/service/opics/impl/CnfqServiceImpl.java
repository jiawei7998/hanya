package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.SwiftMethod;
import com.singlee.financial.wks.mapper.opics.CnfqMapper;
import com.singlee.financial.wks.service.opics.CnfqService;
import com.singlee.financial.wks.service.opics.CustService;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class CnfqServiceImpl implements CnfqService {

    @Resource
    private CnfqMapper cnfqMapper;

    @Resource
    private BatchDao batchDao;
    
    @Autowired
	private CustService custService;
    
    private Logger logger = Logger.getLogger(CnfqServiceImpl.class);

    @Override
    public int deleteByPrimaryKey(String br, String type, String dealno, String seq, String prodcode, Date cdate,
        String ctime) {
        return cnfqMapper.deleteByPrimaryKey(br, type, dealno, seq, prodcode, cdate, ctime);
    }
    
    @Override
	public int deleteReverseByPrimaryKey(String br, String type, String dealno, String seq, String prodcode) {
    	return cnfqMapper.deleteReverseByPrimaryKey(br, type, dealno, seq, prodcode);
	}

    @Override
    public int insert(Cnfq record) {
    	if(null == record) {
    		return 0;
    	}
        return cnfqMapper.insert(record);
    }

    @Override
    public int insertSelective(Cnfq record) {
        return cnfqMapper.insertSelective(record);
    }

    @Override
    public Cnfq selectByPrimaryKey(String br, String type, String dealno, String seq, String prodcode, Date cdate,
        String ctime) {
        return cnfqMapper.selectByPrimaryKey(br, type, dealno, seq, prodcode, cdate, ctime);
    }

    @Override
    public int updateByPrimaryKeySelective(Cnfq record) {
        return cnfqMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Cnfq record) {
        return cnfqMapper.updateByPrimaryKey(record);
    }

    @Override
    public void updateBatch(List<Cnfq> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.CnfqMapper.updateByPrimaryKeySelective", list);
    }

    @Override
    public void batchInsert(List<Cnfq> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.CnfqMapper.insert", list);
    }

    /**
     * 方法已重载.构建确认报文对象(CNFQ),设置默认值 ctime:当前系统时间 textstat:报文处理状态,0-待处理,1-处理成功 formind:报文格式标识,T-TEXT文本,S-SWIFT,F-FAX
     * conftype:报文类型,C-外汇交易确认报文,N-利率互换首期,F-利率互换利率重定 
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param prodcode
     * @param cdate
     * @param ctime
     * @param textstat
     * @param formind
     * @param conftype
     * @return
     */
    private Cnfq bulider(String br, String type, String dealno, String seq, String prodcode, Date cdate, String ctime,
        String textstat, String formind, String conftype) {
        Cnfq cnfq = new Cnfq();
        cnfq.setBr(br);// 机构
        cnfq.setType(type);// 产品类型
        cnfq.setDealno(dealno);// 交易单号
        cnfq.setSeq(seq);// 序号默认0
        cnfq.setProdcode(prodcode);// 产品代码
        cnfq.setCdate(cdate);// 创建日期 创建时间
        cnfq.setCtime(ctime);
        cnfq.setTextstat(textstat);// 发送标识 0：未发送 1：已发送
        cnfq.setFormind(formind);// 格式
        cnfq.setConftype(conftype);// 银行
        return cnfq;
    }

    /**
     * 方法已重载.构建确认报文对象(CNFQ)
     * 
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param prodcode
     * @param cdate
     * @return
     */
    private Cnfq bulider(String br, String type, String dealno, String seq, String prodcode, Date cdate,String cno, String conftype,String textStat) {
    	String formind = null;
		Cust cust = null;
		
		//查询交易对手信息
		cust = custService.selectByPrimaryKey(cno);
		logger.info("交易对["+cno+"]-[SWIFTCODE="+cust.getBic()+"]");
		/*
		 * 交易对手未添加SWIFTCODE时该值为T,否则值为S
		 */
		if(StringUtils.isEmpty(StringUtils.trim(cust.getBic()))) {
			formind = "T";
		}else {
			formind = "S";
		}
        return bulider(br, type, dealno, seq, prodcode, cdate, DateUtil.getCurrentTimeAsString(), textStat, formind,conftype);
    }
    
    /**
     * 方法已重载.构建确认报文对象(CNFQ)
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param prodcode
     * @param cdate
     * @param cno
     * @return
     */
    private Cnfq bulider(String br, String type, String dealno, String seq, String prodcode, Date cdate,String cno,SwiftMethod swiftMethod) {
    	String textStat = null;
    	if(!SwiftMethod.ENABLE.equals(swiftMethod) && !SwiftMethod.CONFIRMATION.equals(swiftMethod)) {
    		textStat = "1";
    	}else {
    		textStat = "0";
    	}
    	
    	return bulider(br, type, dealno, seq, prodcode, cdate, cno,"C",textStat);
    }
    
    /**
     * 方法已重载.构建确认报文对象(CNFQ)，默认textStat为1,不出确认报文
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param prodcode
     * @param cdate
     * @param cno
     * @return
     */
    private Cnfq bulider(String br, String type, String dealno, String seq, String prodcode, Date cdate,String cno) {
    	return bulider(br, type, dealno, seq, prodcode, cdate, cno,"C","1");
    }
    
    /**
     * 方法已重载.构建确认报文对象(CNFQ)，指定contype字段,默认textStat为1,不出确认报文
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param prodcode
     * @param cdate
     * @param cno
     * @param contype
     * @return
     */
    private Cnfq bulider(String br, String type, String dealno, String seq, String prodcode, Date cdate,String cno,String contype) {
    	return bulider(br, type, dealno, seq, prodcode, cdate, cno,contype,"1");
    }

    @Override
    public Cnfq bulider(Dldt dldt) {
        return bulider(dldt.getBr(), dldt.getProdtype(), dldt.getDealno(), dldt.getSeq(), dldt.getProduct(),
            dldt.getBrprcindte(),dldt.getCno());
    }
    
    @Override
	public Cnfq bulider(Fxdh fx,SwiftMethod swiftMethod) {
		if (null == fx) {
			JY.raise("Cnfq输入外汇对象为空!");
		}
		return bulider(fx.getBr(), fx.getProdtype(), fx.getDealno(), fx.getSeq(), fx.getProdcode(), fx.getBrprcindte(), fx.getCust(),swiftMethod);
	}

    @Override
    public Cnfq bulider(Otdt otc) {
        return bulider(otc.getBr(), otc.getProdtype(), otc.getDealno(), otc.getSeq(), otc.getProduct(),
            otc.getBrprcindte(),otc.getCno());
    }

    @Override
    public Cnfq bulider(Swdh swdh,Prir prir,Psix psix) {
    	
    	/*
    	 * 当禁发确认报文时,状态为1不生成MT360报文
    	 */
    	if("Y".equalsIgnoreCase(prir.getSupconfind()) && "Y".equalsIgnoreCase(psix.getSupconfind())) {
    		logger.info("利率互换交易["+swdh.getDealno()+"]禁用确认报文,将不插入CNFQ记录");
    		return null;
    	}
    	
        return bulider(swdh.getBr(), swdh.getProdtype(), swdh.getDealno(), swdh.getSeq(), swdh.getProduct(),
            swdh.getBrprcindte(),swdh.getCno(),"N");
    }

	@Override
	public Cnfq bulider(Spsh spsh) {
		return bulider(spsh.getBr(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(), spsh.getProduct(),
				spsh.getBrprcindte(),spsh.getCno());
	}

	@Override
	public Cnfq bulider(Rprh rprh) {
		return bulider(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
				rprh.getBrprcindte(),rprh.getCno());
	}
	
	@Override
	public Cnfq bulider(Sldh sldh) {
		return bulider(sldh.getBr(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(), sldh.getProduct(),
				sldh.getBrprcindte(),sldh.getCno());
	}

}
