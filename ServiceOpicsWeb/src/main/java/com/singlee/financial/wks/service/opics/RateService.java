package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Rate;

public interface RateService {

    int deleteByPrimaryKey(String br, String ratecode);

    int insert(Rate record);

    int insertSelective(Rate record);

    Rate selectByPrimaryKey(String br, String ratecode);

    Rate selectByCcy(String br,String ccy, String varfix);
    
    Rate selectByIsda(String br, String ccy,String ratesourceIsda);

    int updateByPrimaryKeySelective(Rate record);

    int updateByPrimaryKey(Rate record);

}
