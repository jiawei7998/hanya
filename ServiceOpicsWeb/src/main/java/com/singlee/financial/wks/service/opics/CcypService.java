package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Ccyp;
public interface CcypService{


    int deleteByPrimaryKey(String br,String ccy1,String ccy2);

    int insert(Ccyp record);

    int insertSelective(Ccyp record);

    Ccyp selectByPrimaryKey(String br,String ccy1,String ccy2);

    int updateByPrimaryKeySelective(Ccyp record);

    int updateByPrimaryKey(Ccyp record);

}
