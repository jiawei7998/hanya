package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Sfdc;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.expand.SettleInfoWksBean;
public interface SfdcService{


    int deleteByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct,String dcc);

    int insert(Sfdc record);

    int insertSelective(Sfdc record);

    Sfdc selectByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct,String dcc);

    int updateByPrimaryKeySelective(Sfdc record);

    int updateByPrimaryKey(Sfdc record);

    void batchInsert(List<Sfdc> list);
    /**
     * 方法已重载.根据现券买卖业务数据构建交易对手付款清算对象(PCIX),MT202、MT300中使用
     * @param spsh
     * @param acct
     * @return
     */
    List<Sfdc> builder(Spsh spsh, SettleInfoWksBean acct);
}
