package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Port;
public interface PortService{


    int deleteByPrimaryKey(String br,String portfolio);

    int insert(Port record);

    int insertSelective(Port record);

    Port selectByPrimaryKey(String br,String portfolio);

    int updateByPrimaryKeySelective(Port record);

    int updateByPrimaryKey(Port record);

}
