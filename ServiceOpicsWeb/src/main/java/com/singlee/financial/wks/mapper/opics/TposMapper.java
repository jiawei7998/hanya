package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Tpos;
import org.apache.ibatis.annotations.Param;

public interface TposMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("cost") String cost, @Param("ccy") String ccy, @Param("invtype") String invtype, @Param("port") String port, @Param("secid") String secid);

    int insert(Tpos record);

    int insertSelective(Tpos record);

    Tpos selectByPrimaryKey(@Param("br") String br, @Param("cost") String cost, @Param("ccy") String ccy, @Param("invtype") String invtype, @Param("port") String port, @Param("secid") String secid);
    Tpos selectByPrimaryKeySpsh(@Param("br") String br, @Param("cost") String cost, @Param("ccy") String ccy, @Param("invtype") String invtype, @Param("port") String port,
    		@Param("secid") String secid,@Param("product") String product, @Param("prodtype") String prodtype);

    int updateByPrimaryKeySelective(Tpos record);

    int updateByPrimaryKey(Tpos record);
}