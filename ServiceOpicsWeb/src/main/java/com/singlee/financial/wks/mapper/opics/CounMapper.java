package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Coun;

public interface CounMapper {
    /**
     * delete by primary key
     * @param ccode primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String ccode);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Coun record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Coun record);

    /**
     * select by primary key
     * @param ccode primary key
     * @return object by primary key
     */
    Coun selectByPrimaryKey(String ccode);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Coun record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Coun record);
}