package com.singlee.financial.wks.service.opics;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.bean.opics.Nupd;
import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.bean.opics.Rdfh;
import com.singlee.financial.wks.bean.opics.Rprh;
import com.singlee.financial.wks.bean.opics.Schd;
import com.singlee.financial.wks.bean.opics.Sldh;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.bean.opics.Swds;

public interface NupdService {

    int deleteByPrimaryKey(String br, String nos, String dealno, String seq, String product, String type, Date vdate);
    /**
     * 提前终止
     */
    int deleteNupdByVdate(String br, String dealno, String product, String type, Date vdate);
    /**
     * 交易冲销
     */
    int deleteReverseByPrimaryKey(String br, String dealno, String product, String type,Date postdate, Date mdate);
    /**
     * 交易冲销:外汇
     */
    int deleteReverseFxdhByPrimaryKey(String br, String dealno, String product, String type,Date postdate);

    int insert(Nupd record);

    int insertSelective(Nupd record);

    Nupd selectByPrimaryKey(String br, String nos, String dealno, String seq, String product, String type, Date vdate);

    List<Nupd> selectReverseByPrimaryKey(String br, String dealno, String product, String type);

    int updateByPrimaryKeySelective(Nupd record);

    int updateByPrimaryKey(Nupd record);

    void updateBatch(List<Nupd> list);

    void batchInsert(List<Nupd> list);

    /**
     * 方法已重载.根据拆借业务、现金流数据构建往来账对象(NUPD)
     * 
     * @param dl
     * @param schd
     * @return
     */
    List<Nupd> builder(Dldt dl, List<Schd> schd);

    /**
     * 方法已重载.根据外汇业务数据构建往来账对象(NUPD)
     * 
     * @param fxdh
     * @return
     */
    List<Nupd> builder(Fxdh fxdh);

    /**
     * 方法已重载.根据外汇期权业务数据构建往来账对象(NUPD)
     * 
     * @param otdt
     * @return
     */
    List<Nupd> builder(Otdt otdt);

    /**
     * 方法已重载.根据利率互换/货币互换业务数据构建往来账对象(NUPD)
     * 
     * @param swdh
     * @param swdt
     * @return
     */
    List<Nupd> builder(Swdh swdh, List<Swds> swdts);
    /**
     * 方法已重载.根据现券买卖数据构建往来账对象(NUPD)
     * 
     * @param spsh
     * @return
     */
    List<Nupd> builder(Spsh spsh);
    /**
     * 方法已重载.根据收息数据构建往来账对象(NUPD)
     * @param rdfh
     * @return
     */
    List<Nupd> builder(Rdfh rdfh);
    /**
     * 回购处理
     * @param rprh
     * @return
     */
    List<Nupd> builder(Rprh rprh);
    /**
     * 债券借贷
     * @param sldh
     * @return
     */
    List<Nupd> builder(Sldh sldh);
    /**
     * 删除NUPD的记录
     */
    void callSpDelrecNupdVdate(String br,String dealno,String seq,String product,String type,Date vdate,String nos);
    /**
     * 根据冲销时间节点新增NUPD
     */
    void callSpAddrecNupd(String br,String nos,String dealno,String seq,String product,String type,String cmne,Date vdate,Date postdate,BigDecimal amtupd);
}
