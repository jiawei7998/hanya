package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Ddft implements Serializable {
    private String br;

    private String ccy;

    private String yieldcurve;

    private String shiftseq;

    private Date brprocdate;

    private Date matdate;

    private String quotetype;

    private BigDecimal discfact10;

    private BigDecimal zerocoup10;

    private BigDecimal zeroyield10;

    private BigDecimal zeroccyield10;

    private BigDecimal discfact1bp10;

    private BigDecimal zerocoup1bp10;

    private BigDecimal zeroyield1bp10;

    private BigDecimal zeroccyield1bp10;

    private BigDecimal parrate8;

    private String interpolated;

    private String mty;

    private static final long serialVersionUID = 1L;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy == null ? null : ccy.trim();
    }

    public String getYieldcurve() {
        return yieldcurve;
    }

    public void setYieldcurve(String yieldcurve) {
        this.yieldcurve = yieldcurve == null ? null : yieldcurve.trim();
    }

    public String getShiftseq() {
        return shiftseq;
    }

    public void setShiftseq(String shiftseq) {
        this.shiftseq = shiftseq == null ? null : shiftseq.trim();
    }

    public Date getBrprocdate() {
        return brprocdate;
    }

    public void setBrprocdate(Date brprocdate) {
        this.brprocdate = brprocdate;
    }

    public Date getMatdate() {
        return matdate;
    }

    public void setMatdate(Date matdate) {
        this.matdate = matdate;
    }

    public String getQuotetype() {
        return quotetype;
    }

    public void setQuotetype(String quotetype) {
        this.quotetype = quotetype == null ? null : quotetype.trim();
    }

    public BigDecimal getDiscfact10() {
        return discfact10;
    }

    public void setDiscfact10(BigDecimal discfact10) {
        this.discfact10 = discfact10;
    }

    public BigDecimal getZerocoup10() {
        return zerocoup10;
    }

    public void setZerocoup10(BigDecimal zerocoup10) {
        this.zerocoup10 = zerocoup10;
    }

    public BigDecimal getZeroyield10() {
        return zeroyield10;
    }

    public void setZeroyield10(BigDecimal zeroyield10) {
        this.zeroyield10 = zeroyield10;
    }

    public BigDecimal getZeroccyield10() {
        return zeroccyield10;
    }

    public void setZeroccyield10(BigDecimal zeroccyield10) {
        this.zeroccyield10 = zeroccyield10;
    }

    public BigDecimal getDiscfact1bp10() {
        return discfact1bp10;
    }

    public void setDiscfact1bp10(BigDecimal discfact1bp10) {
        this.discfact1bp10 = discfact1bp10;
    }

    public BigDecimal getZerocoup1bp10() {
        return zerocoup1bp10;
    }

    public void setZerocoup1bp10(BigDecimal zerocoup1bp10) {
        this.zerocoup1bp10 = zerocoup1bp10;
    }

    public BigDecimal getZeroyield1bp10() {
        return zeroyield1bp10;
    }

    public void setZeroyield1bp10(BigDecimal zeroyield1bp10) {
        this.zeroyield1bp10 = zeroyield1bp10;
    }

    public BigDecimal getZeroccyield1bp10() {
        return zeroccyield1bp10;
    }

    public void setZeroccyield1bp10(BigDecimal zeroccyield1bp10) {
        this.zeroccyield1bp10 = zeroccyield1bp10;
    }

    public BigDecimal getParrate8() {
        return parrate8;
    }

    public void setParrate8(BigDecimal parrate8) {
        this.parrate8 = parrate8;
    }

    public String getInterpolated() {
        return interpolated;
    }

    public void setInterpolated(String interpolated) {
        this.interpolated = interpolated == null ? null : interpolated.trim();
    }

    public String getMty() {
        return mty;
    }

    public void setMty(String mty) {
        this.mty = mty == null ? null : mty.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", br=").append(br);
        sb.append(", ccy=").append(ccy);
        sb.append(", yieldcurve=").append(yieldcurve);
        sb.append(", shiftseq=").append(shiftseq);
        sb.append(", brprocdate=").append(brprocdate);
        sb.append(", matdate=").append(matdate);
        sb.append(", quotetype=").append(quotetype);
        sb.append(", discfact10=").append(discfact10);
        sb.append(", zerocoup10=").append(zerocoup10);
        sb.append(", zeroyield10=").append(zeroyield10);
        sb.append(", zeroccyield10=").append(zeroccyield10);
        sb.append(", discfact1bp10=").append(discfact1bp10);
        sb.append(", zerocoup1bp10=").append(zerocoup1bp10);
        sb.append(", zeroyield1bp10=").append(zeroyield1bp10);
        sb.append(", zeroccyield1bp10=").append(zeroccyield1bp10);
        sb.append(", parrate8=").append(parrate8);
        sb.append(", interpolated=").append(interpolated);
        sb.append(", mty=").append(mty);
        sb.append("]");
        return sb.toString();
    }

    @Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;}
		if (obj == null) {
			return false;}
		if (getClass() != obj.getClass()) {
			return false;}
		Ddft other = (Ddft) obj;
		if (br == null) {
			if (other.br != null) {
				return false;}
		} else if (!br.equals(other.br)) {
			return false;}
		if (brprocdate == null) {
			if (other.brprocdate != null) {
				return false;}
		} else if (!brprocdate.equals(other.brprocdate)) {
			return false;}
		if (ccy == null) {
			if (other.ccy != null) {
				return false;}
		} else if (!ccy.equals(other.ccy)) {
			return false;}
		if (matdate == null) {
			if (other.matdate != null) {
				return false;}
		} else if (!matdate.equals(other.matdate)) {
			return false;}
		if (quotetype == null) {
			if (other.quotetype != null) {
				return false;}
		} else if (!quotetype.equals(other.quotetype)) {
			return false;}
		if (shiftseq == null) {
			if (other.shiftseq != null) {
				return false;}
		} else if (!shiftseq.equals(other.shiftseq)) {
			return false;}
		if (yieldcurve == null) {
			if (other.yieldcurve != null) {
				return false;}
		} else if (!yieldcurve.equals(other.yieldcurve)) {
			return false;}
		return true;
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((br == null) ? 0 : br.hashCode());
		result = prime * result + ((brprocdate == null) ? 0 : brprocdate.hashCode());
		result = prime * result + ((ccy == null) ? 0 : ccy.hashCode());
		result = prime * result + ((matdate == null) ? 0 : matdate.hashCode());
		result = prime * result + ((quotetype == null) ? 0 : quotetype.hashCode());
		result = prime * result + ((shiftseq == null) ? 0 : shiftseq.hashCode());
		result = prime * result + ((yieldcurve == null) ? 0 : yieldcurve.hashCode());
		return result;
	}
}