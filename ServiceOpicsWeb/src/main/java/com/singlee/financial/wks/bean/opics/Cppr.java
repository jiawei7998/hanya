package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Cppr implements Serializable {
    private String br;

    private String ccy1;

    private String ccy2;

    private String cost;

    private String port;

    private String trad;

    private Date vdate;

    private BigDecimal ccy1amt;

    private BigDecimal ccy2amt;

    private BigDecimal ccy1mktamt;

    private BigDecimal ccy2mktamt;

    private BigDecimal tdyplamt;

    private BigDecimal ystplamt;

    private BigDecimal pmeplamt;

    private BigDecimal pyeplamt;

    private BigDecimal ccy1amtyst;

    private BigDecimal ccy2amtyst;

    private BigDecimal tdynpvamt;

    private BigDecimal ystnpvamt;

    private BigDecimal pmenpvamt;

    private BigDecimal pyenpvamt;

    private BigDecimal ccy1spotbaseamtyst;

    private BigDecimal ccy2spotbaseamtyst;

    private BigDecimal ccy1spotbaseamt;

    private BigDecimal ccy2spotbaseamt;

    private static final long serialVersionUID = 1L;

    public String getBr() {
        return br;
    }

    public void setBr(String br) {
        this.br = br == null ? null : br.trim();
    }

    public String getCcy1() {
        return ccy1;
    }

    public void setCcy1(String ccy1) {
        this.ccy1 = ccy1 == null ? null : ccy1.trim();
    }

    public String getCcy2() {
        return ccy2;
    }

    public void setCcy2(String ccy2) {
        this.ccy2 = ccy2 == null ? null : ccy2.trim();
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost == null ? null : cost.trim();
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port == null ? null : port.trim();
    }

    public String getTrad() {
        return trad;
    }

    public void setTrad(String trad) {
        this.trad = trad == null ? null : trad.trim();
    }

    public Date getVdate() {
        return vdate;
    }

    public void setVdate(Date vdate) {
        this.vdate = vdate;
    }

    public BigDecimal getCcy1amt() {
        return ccy1amt;
    }

    public void setCcy1amt(BigDecimal ccy1amt) {
        this.ccy1amt = ccy1amt;
    }

    public BigDecimal getCcy2amt() {
        return ccy2amt;
    }

    public void setCcy2amt(BigDecimal ccy2amt) {
        this.ccy2amt = ccy2amt;
    }

    public BigDecimal getCcy1mktamt() {
        return ccy1mktamt;
    }

    public void setCcy1mktamt(BigDecimal ccy1mktamt) {
        this.ccy1mktamt = ccy1mktamt;
    }

    public BigDecimal getCcy2mktamt() {
        return ccy2mktamt;
    }

    public void setCcy2mktamt(BigDecimal ccy2mktamt) {
        this.ccy2mktamt = ccy2mktamt;
    }

    public BigDecimal getTdyplamt() {
        return tdyplamt;
    }

    public void setTdyplamt(BigDecimal tdyplamt) {
        this.tdyplamt = tdyplamt;
    }

    public BigDecimal getYstplamt() {
        return ystplamt;
    }

    public void setYstplamt(BigDecimal ystplamt) {
        this.ystplamt = ystplamt;
    }

    public BigDecimal getPmeplamt() {
        return pmeplamt;
    }

    public void setPmeplamt(BigDecimal pmeplamt) {
        this.pmeplamt = pmeplamt;
    }

    public BigDecimal getPyeplamt() {
        return pyeplamt;
    }

    public void setPyeplamt(BigDecimal pyeplamt) {
        this.pyeplamt = pyeplamt;
    }

    public BigDecimal getCcy1amtyst() {
        return ccy1amtyst;
    }

    public void setCcy1amtyst(BigDecimal ccy1amtyst) {
        this.ccy1amtyst = ccy1amtyst;
    }

    public BigDecimal getCcy2amtyst() {
        return ccy2amtyst;
    }

    public void setCcy2amtyst(BigDecimal ccy2amtyst) {
        this.ccy2amtyst = ccy2amtyst;
    }

    public BigDecimal getTdynpvamt() {
        return tdynpvamt;
    }

    public void setTdynpvamt(BigDecimal tdynpvamt) {
        this.tdynpvamt = tdynpvamt;
    }

    public BigDecimal getYstnpvamt() {
        return ystnpvamt;
    }

    public void setYstnpvamt(BigDecimal ystnpvamt) {
        this.ystnpvamt = ystnpvamt;
    }

    public BigDecimal getPmenpvamt() {
        return pmenpvamt;
    }

    public void setPmenpvamt(BigDecimal pmenpvamt) {
        this.pmenpvamt = pmenpvamt;
    }

    public BigDecimal getPyenpvamt() {
        return pyenpvamt;
    }

    public void setPyenpvamt(BigDecimal pyenpvamt) {
        this.pyenpvamt = pyenpvamt;
    }

    public BigDecimal getCcy1spotbaseamtyst() {
        return ccy1spotbaseamtyst;
    }

    public void setCcy1spotbaseamtyst(BigDecimal ccy1spotbaseamtyst) {
        this.ccy1spotbaseamtyst = ccy1spotbaseamtyst;
    }

    public BigDecimal getCcy2spotbaseamtyst() {
        return ccy2spotbaseamtyst;
    }

    public void setCcy2spotbaseamtyst(BigDecimal ccy2spotbaseamtyst) {
        this.ccy2spotbaseamtyst = ccy2spotbaseamtyst;
    }

    public BigDecimal getCcy1spotbaseamt() {
        return ccy1spotbaseamt;
    }

    public void setCcy1spotbaseamt(BigDecimal ccy1spotbaseamt) {
        this.ccy1spotbaseamt = ccy1spotbaseamt;
    }

    public BigDecimal getCcy2spotbaseamt() {
        return ccy2spotbaseamt;
    }

    public void setCcy2spotbaseamt(BigDecimal ccy2spotbaseamt) {
        this.ccy2spotbaseamt = ccy2spotbaseamt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", br=").append(br);
        sb.append(", ccy1=").append(ccy1);
        sb.append(", ccy2=").append(ccy2);
        sb.append(", cost=").append(cost);
        sb.append(", port=").append(port);
        sb.append(", trad=").append(trad);
        sb.append(", vdate=").append(vdate);
        sb.append(", ccy1amt=").append(ccy1amt);
        sb.append(", ccy2amt=").append(ccy2amt);
        sb.append(", ccy1mktamt=").append(ccy1mktamt);
        sb.append(", ccy2mktamt=").append(ccy2mktamt);
        sb.append(", tdyplamt=").append(tdyplamt);
        sb.append(", ystplamt=").append(ystplamt);
        sb.append(", pmeplamt=").append(pmeplamt);
        sb.append(", pyeplamt=").append(pyeplamt);
        sb.append(", ccy1amtyst=").append(ccy1amtyst);
        sb.append(", ccy2amtyst=").append(ccy2amtyst);
        sb.append(", tdynpvamt=").append(tdynpvamt);
        sb.append(", ystnpvamt=").append(ystnpvamt);
        sb.append(", pmenpvamt=").append(pmenpvamt);
        sb.append(", pyenpvamt=").append(pyenpvamt);
        sb.append(", ccy1spotbaseamtyst=").append(ccy1spotbaseamtyst);
        sb.append(", ccy2spotbaseamtyst=").append(ccy2spotbaseamtyst);
        sb.append(", ccy1spotbaseamt=").append(ccy1spotbaseamt);
        sb.append(", ccy2spotbaseamt=").append(ccy2spotbaseamt);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Cppr other = (Cppr) that;
        return (this.getBr() == null ? other.getBr() == null : this.getBr().equals(other.getBr()))
            && (this.getCcy1() == null ? other.getCcy1() == null : this.getCcy1().equals(other.getCcy1()))
            && (this.getCcy2() == null ? other.getCcy2() == null : this.getCcy2().equals(other.getCcy2()))
            && (this.getCost() == null ? other.getCost() == null : this.getCost().equals(other.getCost()))
            && (this.getPort() == null ? other.getPort() == null : this.getPort().equals(other.getPort()))
            && (this.getTrad() == null ? other.getTrad() == null : this.getTrad().equals(other.getTrad()))
            && (this.getVdate() == null ? other.getVdate() == null : this.getVdate().equals(other.getVdate()))
            && (this.getCcy1amt() == null ? other.getCcy1amt() == null : this.getCcy1amt().equals(other.getCcy1amt()))
            && (this.getCcy2amt() == null ? other.getCcy2amt() == null : this.getCcy2amt().equals(other.getCcy2amt()))
            && (this.getCcy1mktamt() == null ? other.getCcy1mktamt() == null : this.getCcy1mktamt().equals(other.getCcy1mktamt()))
            && (this.getCcy2mktamt() == null ? other.getCcy2mktamt() == null : this.getCcy2mktamt().equals(other.getCcy2mktamt()))
            && (this.getTdyplamt() == null ? other.getTdyplamt() == null : this.getTdyplamt().equals(other.getTdyplamt()))
            && (this.getYstplamt() == null ? other.getYstplamt() == null : this.getYstplamt().equals(other.getYstplamt()))
            && (this.getPmeplamt() == null ? other.getPmeplamt() == null : this.getPmeplamt().equals(other.getPmeplamt()))
            && (this.getPyeplamt() == null ? other.getPyeplamt() == null : this.getPyeplamt().equals(other.getPyeplamt()))
            && (this.getCcy1amtyst() == null ? other.getCcy1amtyst() == null : this.getCcy1amtyst().equals(other.getCcy1amtyst()))
            && (this.getCcy2amtyst() == null ? other.getCcy2amtyst() == null : this.getCcy2amtyst().equals(other.getCcy2amtyst()))
            && (this.getTdynpvamt() == null ? other.getTdynpvamt() == null : this.getTdynpvamt().equals(other.getTdynpvamt()))
            && (this.getYstnpvamt() == null ? other.getYstnpvamt() == null : this.getYstnpvamt().equals(other.getYstnpvamt()))
            && (this.getPmenpvamt() == null ? other.getPmenpvamt() == null : this.getPmenpvamt().equals(other.getPmenpvamt()))
            && (this.getPyenpvamt() == null ? other.getPyenpvamt() == null : this.getPyenpvamt().equals(other.getPyenpvamt()))
            && (this.getCcy1spotbaseamtyst() == null ? other.getCcy1spotbaseamtyst() == null : this.getCcy1spotbaseamtyst().equals(other.getCcy1spotbaseamtyst()))
            && (this.getCcy2spotbaseamtyst() == null ? other.getCcy2spotbaseamtyst() == null : this.getCcy2spotbaseamtyst().equals(other.getCcy2spotbaseamtyst()))
            && (this.getCcy1spotbaseamt() == null ? other.getCcy1spotbaseamt() == null : this.getCcy1spotbaseamt().equals(other.getCcy1spotbaseamt()))
            && (this.getCcy2spotbaseamt() == null ? other.getCcy2spotbaseamt() == null : this.getCcy2spotbaseamt().equals(other.getCcy2spotbaseamt()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getBr() == null) ? 0 : getBr().hashCode());
        result = prime * result + ((getCcy1() == null) ? 0 : getCcy1().hashCode());
        result = prime * result + ((getCcy2() == null) ? 0 : getCcy2().hashCode());
        result = prime * result + ((getCost() == null) ? 0 : getCost().hashCode());
        result = prime * result + ((getPort() == null) ? 0 : getPort().hashCode());
        result = prime * result + ((getTrad() == null) ? 0 : getTrad().hashCode());
        result = prime * result + ((getVdate() == null) ? 0 : getVdate().hashCode());
        result = prime * result + ((getCcy1amt() == null) ? 0 : getCcy1amt().hashCode());
        result = prime * result + ((getCcy2amt() == null) ? 0 : getCcy2amt().hashCode());
        result = prime * result + ((getCcy1mktamt() == null) ? 0 : getCcy1mktamt().hashCode());
        result = prime * result + ((getCcy2mktamt() == null) ? 0 : getCcy2mktamt().hashCode());
        result = prime * result + ((getTdyplamt() == null) ? 0 : getTdyplamt().hashCode());
        result = prime * result + ((getYstplamt() == null) ? 0 : getYstplamt().hashCode());
        result = prime * result + ((getPmeplamt() == null) ? 0 : getPmeplamt().hashCode());
        result = prime * result + ((getPyeplamt() == null) ? 0 : getPyeplamt().hashCode());
        result = prime * result + ((getCcy1amtyst() == null) ? 0 : getCcy1amtyst().hashCode());
        result = prime * result + ((getCcy2amtyst() == null) ? 0 : getCcy2amtyst().hashCode());
        result = prime * result + ((getTdynpvamt() == null) ? 0 : getTdynpvamt().hashCode());
        result = prime * result + ((getYstnpvamt() == null) ? 0 : getYstnpvamt().hashCode());
        result = prime * result + ((getPmenpvamt() == null) ? 0 : getPmenpvamt().hashCode());
        result = prime * result + ((getPyenpvamt() == null) ? 0 : getPyenpvamt().hashCode());
        result = prime * result + ((getCcy1spotbaseamtyst() == null) ? 0 : getCcy1spotbaseamtyst().hashCode());
        result = prime * result + ((getCcy2spotbaseamtyst() == null) ? 0 : getCcy2spotbaseamtyst().hashCode());
        result = prime * result + ((getCcy1spotbaseamt() == null) ? 0 : getCcy1spotbaseamt().hashCode());
        result = prime * result + ((getCcy2spotbaseamt() == null) ? 0 : getCcy2spotbaseamt().hashCode());
        return result;
    }
}