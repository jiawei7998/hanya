package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Secm;
import com.singlee.financial.wks.mapper.opics.SecmMapper;
import com.singlee.financial.wks.service.opics.SecmService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SecmServiceImpl implements SecmService {

    @Resource
    private SecmMapper secmMapper;

    @Override
    public int deleteByPrimaryKey(String secid) {
        return secmMapper.deleteByPrimaryKey(secid);
    }

    @Override
    public int insert(Secm record) {
        return secmMapper.insert(record);
    }

    @Override
    public int insertSelective(Secm record) {
        return secmMapper.insertSelective(record);
    }

    @Override
    public Secm selectByPrimaryKey(String secid) {
        return secmMapper.selectByPrimaryKey(secid);
    }

    @Override
    public int updateByPrimaryKeySelective(Secm record) {
        return secmMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Secm record) {
        return secmMapper.updateByPrimaryKey(record);
    }

}
