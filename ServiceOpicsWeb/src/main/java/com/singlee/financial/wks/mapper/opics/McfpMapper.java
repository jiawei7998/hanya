package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Mcfp;
import org.apache.ibatis.annotations.Param;

public interface McfpMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("prodcode") String prodcode, @Param("type") String type);

    int insert(Mcfp record);

    int insertSelective(Mcfp record);
    /**
     * With Oracle Lock
     * @param br
     * @param prodcode
     * @param type
     * @return
     */
    Mcfp selectByPrimaryKey(@Param("br") String br, @Param("prodcode") String prodcode, @Param("type") String type);

    int updateByPrimaryKeySelective(Mcfp record);

    int updateByPrimaryKey(Mcfp record);
}