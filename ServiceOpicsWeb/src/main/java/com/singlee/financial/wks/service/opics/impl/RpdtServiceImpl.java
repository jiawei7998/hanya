package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Rpdt;
import com.singlee.financial.wks.mapper.opics.RpdtMapper;
import com.singlee.financial.wks.service.opics.RpdtService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class RpdtServiceImpl implements RpdtService {

    @Resource
    private RpdtMapper rpdtMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br,String dealno,String seq,String assignseq) {
        return rpdtMapper.deleteByPrimaryKey(br,dealno,seq,assignseq);
    }

    @Override
    public int insert(Rpdt record) {
        return rpdtMapper.insert(record);
    }

    @Override
    public int insertSelective(Rpdt record) {
        return rpdtMapper.insertSelective(record);
    }

    @Override
    public Rpdt selectByPrimaryKey(String br,String dealno,String seq,String assignseq) {
        return rpdtMapper.selectByPrimaryKey(br,dealno,seq,assignseq);
    }
    @Override
    public List<Rpdt> selectByDealno(String br,String dealno) {
        return rpdtMapper.selectByDealno(br,dealno);
    }

    @Override
    public int updateByPrimaryKeySelective(Rpdt record) {
        return rpdtMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Rpdt record) {
        return rpdtMapper.updateByPrimaryKey(record);
    }

	@Override
	public void batchInsert(List<Rpdt> list) {
		if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.RpdtMapper.insert", list);
	}

}
