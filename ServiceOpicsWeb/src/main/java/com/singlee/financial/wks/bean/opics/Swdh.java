package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Swdh implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String dealind;

    private String product;

    private String prodtype;

    private String brok;

    private BigDecimal brokamt;

    private String brokccy;

    private Date brokcdate;

    private BigDecimal brokrate8;

    private Date brprcindte;

    private String cno;

    private String cost;

    private String creditcode;

    private Date custcdate;

    private Date dealdate;

    private String deallinkno;

    private String dealsrce;

    private String dealtext;

    private String dealtime;

    private Date etinputdate;

    private String etoper;

    private String imm;

    private Date inputdate;

    private String inputtime;

    private String ioper;

    private String legcnt;

    private Date lstmntdte;

    private Date matdate;

    private String netpayind;

    private BigDecimal npvbamt;

    private Date origtermdate;

    private String plmethod;

    private String port;

    private Date revdate;

    private String revoper;

    private String revreason;

    private String revtext;

    private String setoff;

    private Date startdate;

    private String swaptype;

    private String tenor;

    private String tracecnt;

    private Date tracedate;

    private String trad;

    private String undacctngtype;

    private String unddealno;

    private String undproduct;

    private String undsecid;

    private String undtenor;

    private String undtype;

    private String undhedgeseq;

    private Date verdate;

    private String verind;

    private String voper;

    private String etassigncust;

    private BigDecimal etamt;

    private Date etauthdte;

    private String etauthind;

    private String etauthoper;

    private String etccy;

    private String etpayrecind;

    private String etsacct;

    private String etsmeans;

    private String etfeeno;

    private BigDecimal npvbamtpay;

    private BigDecimal npvbamtrec;

    private String minlegseq;

    private String maxlegseq;

    private String activeind;

    private String optiontype;

    private String exeract;

    private String collardealno;

    private BigDecimal intrinsicvalue;

    private String etfeeintind;

    private String guarantor;

    private BigDecimal amount1;

    private String char1;

    private String flag1;

    private BigDecimal rate18;

    private Long updatecounter;

    private Date settledate;

    private String counterrefid;

    private String vanillaswapind;

    private String btbbr;

    private String btbdealno;

    private String btbseq;

    private String btbprod;

    private String btbprodtype;

    private String btbcno;

    private String btbcost;

    private String btbport;

    private String btbtrad;

    private String btbind;

    private String mbsdealtype;

    private String intdeal;

    private BigDecimal notccyamt;

    private String notccy;

    private String taxflg;

    private String corptrad;

    private String corpport;

    private BigDecimal bcreditamt;

    private BigDecimal bmtmamt;

    private BigDecimal bmktamt;

    private String flag2;

    private String flag3;

    private BigDecimal rate28;

    private BigDecimal rate38;

    private Date date1;

    private Date date2;

    private BigDecimal amount2;

    private BigDecimal amount3;

    private String corpcost;

    private String ptyguarantee;

    private String cntrptyguarantee;

    private BigDecimal clragntamt;

    private String clragntname;

    private String clragntccy;

    private BigDecimal bmfamt;

    private String bmfname;

    private String bmfccy;

    private String invtype;

    private String notexchrateind;

    private String multiccyind;

    private String varnotionalind;

    private BigDecimal etamt2;

    private Date etauthdte2;

    private String etauthind2;

    private String etauthoper2;

    private String etccy2;

    private String etpayrecind2;

    private String etsacct2;

    private String etsmeans2;

    private String etfeeno2;

    private String etfeeintind2;

    private String etlegseq;

    private String etlegseq2;

    private String creditdefaultind;

    private String mbslegind;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getDealind() {
		return dealind;
	}


	public void setDealind(String dealind) {
		this.dealind = dealind;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getBrok() {
		return brok;
	}


	public void setBrok(String brok) {
		this.brok = brok;
	}


	public BigDecimal getBrokamt() {
		return brokamt;
	}


	public void setBrokamt(BigDecimal brokamt) {
		this.brokamt = brokamt;
	}


	public String getBrokccy() {
		return brokccy;
	}


	public void setBrokccy(String brokccy) {
		this.brokccy = brokccy;
	}


	public Date getBrokcdate() {
		return brokcdate;
	}


	public void setBrokcdate(Date brokcdate) {
		this.brokcdate = brokcdate;
	}


	public BigDecimal getBrokrate8() {
		return brokrate8;
	}


	public void setBrokrate8(BigDecimal brokrate8) {
		this.brokrate8 = brokrate8;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getCreditcode() {
		return creditcode;
	}


	public void setCreditcode(String creditcode) {
		this.creditcode = creditcode;
	}


	public Date getCustcdate() {
		return custcdate;
	}


	public void setCustcdate(Date custcdate) {
		this.custcdate = custcdate;
	}


	public Date getDealdate() {
		return dealdate;
	}


	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}


	public String getDeallinkno() {
		return deallinkno;
	}


	public void setDeallinkno(String deallinkno) {
		this.deallinkno = deallinkno;
	}


	public String getDealsrce() {
		return dealsrce;
	}


	public void setDealsrce(String dealsrce) {
		this.dealsrce = dealsrce;
	}


	public String getDealtext() {
		return dealtext;
	}


	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}


	public String getDealtime() {
		return dealtime;
	}


	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}


	public Date getEtinputdate() {
		return etinputdate;
	}


	public void setEtinputdate(Date etinputdate) {
		this.etinputdate = etinputdate;
	}


	public String getEtoper() {
		return etoper;
	}


	public void setEtoper(String etoper) {
		this.etoper = etoper;
	}


	public String getImm() {
		return imm;
	}


	public void setImm(String imm) {
		this.imm = imm;
	}


	public Date getInputdate() {
		return inputdate;
	}


	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}


	public String getInputtime() {
		return inputtime;
	}


	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}


	public String getIoper() {
		return ioper;
	}


	public void setIoper(String ioper) {
		this.ioper = ioper;
	}


	public String getLegcnt() {
		return legcnt;
	}


	public void setLegcnt(String legcnt) {
		this.legcnt = legcnt;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public Date getMatdate() {
		return matdate;
	}


	public void setMatdate(Date matdate) {
		this.matdate = matdate;
	}


	public String getNetpayind() {
		return netpayind;
	}


	public void setNetpayind(String netpayind) {
		this.netpayind = netpayind;
	}


	public BigDecimal getNpvbamt() {
		return npvbamt;
	}


	public void setNpvbamt(BigDecimal npvbamt) {
		this.npvbamt = npvbamt;
	}


	public Date getOrigtermdate() {
		return origtermdate;
	}


	public void setOrigtermdate(Date origtermdate) {
		this.origtermdate = origtermdate;
	}


	public String getPlmethod() {
		return plmethod;
	}


	public void setPlmethod(String plmethod) {
		this.plmethod = plmethod;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public String getRevoper() {
		return revoper;
	}


	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}


	public String getRevreason() {
		return revreason;
	}


	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}


	public String getRevtext() {
		return revtext;
	}


	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}


	public String getSetoff() {
		return setoff;
	}


	public void setSetoff(String setoff) {
		this.setoff = setoff;
	}


	public Date getStartdate() {
		return startdate;
	}


	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}


	public String getSwaptype() {
		return swaptype;
	}


	public void setSwaptype(String swaptype) {
		this.swaptype = swaptype;
	}


	public String getTenor() {
		return tenor;
	}


	public void setTenor(String tenor) {
		this.tenor = tenor;
	}


	public String getTracecnt() {
		return tracecnt;
	}


	public void setTracecnt(String tracecnt) {
		this.tracecnt = tracecnt;
	}


	public Date getTracedate() {
		return tracedate;
	}


	public void setTracedate(Date tracedate) {
		this.tracedate = tracedate;
	}


	public String getTrad() {
		return trad;
	}


	public void setTrad(String trad) {
		this.trad = trad;
	}


	public String getUndacctngtype() {
		return undacctngtype;
	}


	public void setUndacctngtype(String undacctngtype) {
		this.undacctngtype = undacctngtype;
	}


	public String getUnddealno() {
		return unddealno;
	}


	public void setUnddealno(String unddealno) {
		this.unddealno = unddealno;
	}


	public String getUndproduct() {
		return undproduct;
	}


	public void setUndproduct(String undproduct) {
		this.undproduct = undproduct;
	}


	public String getUndsecid() {
		return undsecid;
	}


	public void setUndsecid(String undsecid) {
		this.undsecid = undsecid;
	}


	public String getUndtenor() {
		return undtenor;
	}


	public void setUndtenor(String undtenor) {
		this.undtenor = undtenor;
	}


	public String getUndtype() {
		return undtype;
	}


	public void setUndtype(String undtype) {
		this.undtype = undtype;
	}


	public String getUndhedgeseq() {
		return undhedgeseq;
	}


	public void setUndhedgeseq(String undhedgeseq) {
		this.undhedgeseq = undhedgeseq;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public String getVerind() {
		return verind;
	}


	public void setVerind(String verind) {
		this.verind = verind;
	}


	public String getVoper() {
		return voper;
	}


	public void setVoper(String voper) {
		this.voper = voper;
	}


	public String getEtassigncust() {
		return etassigncust;
	}


	public void setEtassigncust(String etassigncust) {
		this.etassigncust = etassigncust;
	}


	public BigDecimal getEtamt() {
		return etamt;
	}


	public void setEtamt(BigDecimal etamt) {
		this.etamt = etamt;
	}


	public Date getEtauthdte() {
		return etauthdte;
	}


	public void setEtauthdte(Date etauthdte) {
		this.etauthdte = etauthdte;
	}


	public String getEtauthind() {
		return etauthind;
	}


	public void setEtauthind(String etauthind) {
		this.etauthind = etauthind;
	}


	public String getEtauthoper() {
		return etauthoper;
	}


	public void setEtauthoper(String etauthoper) {
		this.etauthoper = etauthoper;
	}


	public String getEtccy() {
		return etccy;
	}


	public void setEtccy(String etccy) {
		this.etccy = etccy;
	}


	public String getEtpayrecind() {
		return etpayrecind;
	}


	public void setEtpayrecind(String etpayrecind) {
		this.etpayrecind = etpayrecind;
	}


	public String getEtsacct() {
		return etsacct;
	}


	public void setEtsacct(String etsacct) {
		this.etsacct = etsacct;
	}


	public String getEtsmeans() {
		return etsmeans;
	}


	public void setEtsmeans(String etsmeans) {
		this.etsmeans = etsmeans;
	}


	public String getEtfeeno() {
		return etfeeno;
	}


	public void setEtfeeno(String etfeeno) {
		this.etfeeno = etfeeno;
	}


	public BigDecimal getNpvbamtpay() {
		return npvbamtpay;
	}


	public void setNpvbamtpay(BigDecimal npvbamtpay) {
		this.npvbamtpay = npvbamtpay;
	}


	public BigDecimal getNpvbamtrec() {
		return npvbamtrec;
	}


	public void setNpvbamtrec(BigDecimal npvbamtrec) {
		this.npvbamtrec = npvbamtrec;
	}


	public String getMinlegseq() {
		return minlegseq;
	}


	public void setMinlegseq(String minlegseq) {
		this.minlegseq = minlegseq;
	}


	public String getMaxlegseq() {
		return maxlegseq;
	}


	public void setMaxlegseq(String maxlegseq) {
		this.maxlegseq = maxlegseq;
	}


	public String getActiveind() {
		return activeind;
	}


	public void setActiveind(String activeind) {
		this.activeind = activeind;
	}


	public String getOptiontype() {
		return optiontype;
	}


	public void setOptiontype(String optiontype) {
		this.optiontype = optiontype;
	}


	public String getExeract() {
		return exeract;
	}


	public void setExeract(String exeract) {
		this.exeract = exeract;
	}


	public String getCollardealno() {
		return collardealno;
	}


	public void setCollardealno(String collardealno) {
		this.collardealno = collardealno;
	}


	public BigDecimal getIntrinsicvalue() {
		return intrinsicvalue;
	}


	public void setIntrinsicvalue(BigDecimal intrinsicvalue) {
		this.intrinsicvalue = intrinsicvalue;
	}


	public String getEtfeeintind() {
		return etfeeintind;
	}


	public void setEtfeeintind(String etfeeintind) {
		this.etfeeintind = etfeeintind;
	}


	public String getGuarantor() {
		return guarantor;
	}


	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public String getChar1() {
		return char1;
	}


	public void setChar1(String char1) {
		this.char1 = char1;
	}


	public String getFlag1() {
		return flag1;
	}


	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}


	public BigDecimal getRate18() {
		return rate18;
	}


	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}


	public Long getUpdatecounter() {
		return updatecounter;
	}


	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}


	public Date getSettledate() {
		return settledate;
	}


	public void setSettledate(Date settledate) {
		this.settledate = settledate;
	}


	public String getCounterrefid() {
		return counterrefid;
	}


	public void setCounterrefid(String counterrefid) {
		this.counterrefid = counterrefid;
	}


	public String getVanillaswapind() {
		return vanillaswapind;
	}


	public void setVanillaswapind(String vanillaswapind) {
		this.vanillaswapind = vanillaswapind;
	}


	public String getBtbbr() {
		return btbbr;
	}


	public void setBtbbr(String btbbr) {
		this.btbbr = btbbr;
	}


	public String getBtbdealno() {
		return btbdealno;
	}


	public void setBtbdealno(String btbdealno) {
		this.btbdealno = btbdealno;
	}


	public String getBtbseq() {
		return btbseq;
	}


	public void setBtbseq(String btbseq) {
		this.btbseq = btbseq;
	}


	public String getBtbprod() {
		return btbprod;
	}


	public void setBtbprod(String btbprod) {
		this.btbprod = btbprod;
	}


	public String getBtbprodtype() {
		return btbprodtype;
	}


	public void setBtbprodtype(String btbprodtype) {
		this.btbprodtype = btbprodtype;
	}


	public String getBtbcno() {
		return btbcno;
	}


	public void setBtbcno(String btbcno) {
		this.btbcno = btbcno;
	}


	public String getBtbcost() {
		return btbcost;
	}


	public void setBtbcost(String btbcost) {
		this.btbcost = btbcost;
	}


	public String getBtbport() {
		return btbport;
	}


	public void setBtbport(String btbport) {
		this.btbport = btbport;
	}


	public String getBtbtrad() {
		return btbtrad;
	}


	public void setBtbtrad(String btbtrad) {
		this.btbtrad = btbtrad;
	}


	public String getBtbind() {
		return btbind;
	}


	public void setBtbind(String btbind) {
		this.btbind = btbind;
	}


	public String getMbsdealtype() {
		return mbsdealtype;
	}


	public void setMbsdealtype(String mbsdealtype) {
		this.mbsdealtype = mbsdealtype;
	}


	public String getIntdeal() {
		return intdeal;
	}


	public void setIntdeal(String intdeal) {
		this.intdeal = intdeal;
	}


	public BigDecimal getNotccyamt() {
		return notccyamt;
	}


	public void setNotccyamt(BigDecimal notccyamt) {
		this.notccyamt = notccyamt;
	}


	public String getNotccy() {
		return notccy;
	}


	public void setNotccy(String notccy) {
		this.notccy = notccy;
	}


	public String getTaxflg() {
		return taxflg;
	}


	public void setTaxflg(String taxflg) {
		this.taxflg = taxflg;
	}


	public String getCorptrad() {
		return corptrad;
	}


	public void setCorptrad(String corptrad) {
		this.corptrad = corptrad;
	}


	public String getCorpport() {
		return corpport;
	}


	public void setCorpport(String corpport) {
		this.corpport = corpport;
	}


	public BigDecimal getBcreditamt() {
		return bcreditamt;
	}


	public void setBcreditamt(BigDecimal bcreditamt) {
		this.bcreditamt = bcreditamt;
	}


	public BigDecimal getBmtmamt() {
		return bmtmamt;
	}


	public void setBmtmamt(BigDecimal bmtmamt) {
		this.bmtmamt = bmtmamt;
	}


	public BigDecimal getBmktamt() {
		return bmktamt;
	}


	public void setBmktamt(BigDecimal bmktamt) {
		this.bmktamt = bmktamt;
	}


	public String getFlag2() {
		return flag2;
	}


	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}


	public String getFlag3() {
		return flag3;
	}


	public void setFlag3(String flag3) {
		this.flag3 = flag3;
	}


	public BigDecimal getRate28() {
		return rate28;
	}


	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}


	public BigDecimal getRate38() {
		return rate38;
	}


	public void setRate38(BigDecimal rate38) {
		this.rate38 = rate38;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public BigDecimal getAmount3() {
		return amount3;
	}


	public void setAmount3(BigDecimal amount3) {
		this.amount3 = amount3;
	}


	public String getCorpcost() {
		return corpcost;
	}


	public void setCorpcost(String corpcost) {
		this.corpcost = corpcost;
	}


	public String getPtyguarantee() {
		return ptyguarantee;
	}


	public void setPtyguarantee(String ptyguarantee) {
		this.ptyguarantee = ptyguarantee;
	}


	public String getCntrptyguarantee() {
		return cntrptyguarantee;
	}


	public void setCntrptyguarantee(String cntrptyguarantee) {
		this.cntrptyguarantee = cntrptyguarantee;
	}


	public BigDecimal getClragntamt() {
		return clragntamt;
	}


	public void setClragntamt(BigDecimal clragntamt) {
		this.clragntamt = clragntamt;
	}


	public String getClragntname() {
		return clragntname;
	}


	public void setClragntname(String clragntname) {
		this.clragntname = clragntname;
	}


	public String getClragntccy() {
		return clragntccy;
	}


	public void setClragntccy(String clragntccy) {
		this.clragntccy = clragntccy;
	}


	public BigDecimal getBmfamt() {
		return bmfamt;
	}


	public void setBmfamt(BigDecimal bmfamt) {
		this.bmfamt = bmfamt;
	}


	public String getBmfname() {
		return bmfname;
	}


	public void setBmfname(String bmfname) {
		this.bmfname = bmfname;
	}


	public String getBmfccy() {
		return bmfccy;
	}


	public void setBmfccy(String bmfccy) {
		this.bmfccy = bmfccy;
	}


	public String getInvtype() {
		return invtype;
	}


	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}


	public String getNotexchrateind() {
		return notexchrateind;
	}


	public void setNotexchrateind(String notexchrateind) {
		this.notexchrateind = notexchrateind;
	}


	public String getMulticcyind() {
		return multiccyind;
	}


	public void setMulticcyind(String multiccyind) {
		this.multiccyind = multiccyind;
	}


	public String getVarnotionalind() {
		return varnotionalind;
	}


	public void setVarnotionalind(String varnotionalind) {
		this.varnotionalind = varnotionalind;
	}


	public BigDecimal getEtamt2() {
		return etamt2;
	}


	public void setEtamt2(BigDecimal etamt2) {
		this.etamt2 = etamt2;
	}


	public Date getEtauthdte2() {
		return etauthdte2;
	}


	public void setEtauthdte2(Date etauthdte2) {
		this.etauthdte2 = etauthdte2;
	}


	public String getEtauthind2() {
		return etauthind2;
	}


	public void setEtauthind2(String etauthind2) {
		this.etauthind2 = etauthind2;
	}


	public String getEtauthoper2() {
		return etauthoper2;
	}


	public void setEtauthoper2(String etauthoper2) {
		this.etauthoper2 = etauthoper2;
	}


	public String getEtccy2() {
		return etccy2;
	}


	public void setEtccy2(String etccy2) {
		this.etccy2 = etccy2;
	}


	public String getEtpayrecind2() {
		return etpayrecind2;
	}


	public void setEtpayrecind2(String etpayrecind2) {
		this.etpayrecind2 = etpayrecind2;
	}


	public String getEtsacct2() {
		return etsacct2;
	}


	public void setEtsacct2(String etsacct2) {
		this.etsacct2 = etsacct2;
	}


	public String getEtsmeans2() {
		return etsmeans2;
	}


	public void setEtsmeans2(String etsmeans2) {
		this.etsmeans2 = etsmeans2;
	}


	public String getEtfeeno2() {
		return etfeeno2;
	}


	public void setEtfeeno2(String etfeeno2) {
		this.etfeeno2 = etfeeno2;
	}


	public String getEtfeeintind2() {
		return etfeeintind2;
	}


	public void setEtfeeintind2(String etfeeintind2) {
		this.etfeeintind2 = etfeeintind2;
	}


	public String getEtlegseq() {
		return etlegseq;
	}


	public void setEtlegseq(String etlegseq) {
		this.etlegseq = etlegseq;
	}


	public String getEtlegseq2() {
		return etlegseq2;
	}


	public void setEtlegseq2(String etlegseq2) {
		this.etlegseq2 = etlegseq2;
	}


	public String getCreditdefaultind() {
		return creditdefaultind;
	}


	public void setCreditdefaultind(String creditdefaultind) {
		this.creditdefaultind = creditdefaultind;
	}


	public String getMbslegind() {
		return mbslegind;
	}


	public void setMbslegind(String mbslegind) {
		this.mbslegind = mbslegind;
	}


	private static final long serialVersionUID = 1L;
}