package com.singlee.financial.wks.mapper.opics;

import java.util.List;

import com.singlee.financial.marketdata.bean.SubscriptionCode;

public interface RsrmMapper {
	List<SubscriptionCode> selectByGroupId(String groupId);
}
