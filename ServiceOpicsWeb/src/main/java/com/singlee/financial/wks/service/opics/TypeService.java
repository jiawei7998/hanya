package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Type;
public interface TypeService{


    int deleteByPrimaryKey(String prodcode,String type);

    int insert(Type record);

    int insertSelective(Type record);

    Type selectByPrimaryKey(String prodcode,String type);
    
    Type selectByProdAl(String prodcode,String al);

    int updateByPrimaryKeySelective(Type record);

    int updateByPrimaryKey(Type record);

}
