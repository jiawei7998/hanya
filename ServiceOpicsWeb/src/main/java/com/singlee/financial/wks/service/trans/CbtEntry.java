package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.CbtWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * 现券买卖首期转换方法
 *
 * @author xuqq
 */
@Component
public class CbtEntry {
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private PortService portService;
	@Autowired
	private CostService costService;
	@Autowired
	private TradService tradService;
	@Autowired
	private CustService custService;
	@Autowired
	private SecmService secmService;

	/**
	 * 交易信息检查
	 *
	 * @param cbt
	 */
	public void DataCheck(CbtWksBean cbt) {
		// 1.判断债券对象是否存在
		if (null == cbt) {
			JY.raise("%s:%s,请补充完整CbtWksBean对象", WksErrorCode.CBT_NULL.getErrCode(), WksErrorCode.CBT_NULL.getErrMsg());
		}
		// 2.判断交易机构是否存在
		if (StringUtils.isEmpty(cbt.getInstId())) {
			JY.raise("%s:%s,请填写CbtWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NULL.getErrCode(),
					WksErrorCode.BR_NULL.getErrMsg());
		}
		// 3.查询分支/机构是否存在
		Brps brps = brpsService.selectByPrimaryKey(cbt.getInstId());
		if (StringUtils.isEmpty(cbt.getInstId()) || null == brps) {
			JY.raise("%s:%s,请正确输入CbtWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 4.检查产品类型DESK字段拆分PROD_TYPE_PORT
		String[] book = StringUtils.split(cbt.getBook(), "_");
		if (StringUtils.isEmpty(cbt.getBook()) || book.length != 3) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.desk,For.[book=SECUR_SD_100]", WksErrorCode.DESK_FORM_ERR.getErrCode(),
					WksErrorCode.DESK_FORM_ERR.getErrMsg());
		}
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		if (StringUtils.isEmpty(book[0]) || StringUtils.isEmpty(book[1]) || null == type) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.desk,For.[book=SECUR_SD_100]", WksErrorCode.DESK_NOT_FOUND.getErrCode(),
					WksErrorCode.DESK_NOT_FOUND.getErrMsg());
		}
		Port port = portService.selectByPrimaryKey(cbt.getInstId(), cbt.getDesk());// 投组组合
		if (StringUtils.isEmpty(cbt.getDesk()) || null == port) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.desk,For.[desk=PORT]", WksErrorCode.DESK_PORT_ERR.getErrCode(),
					WksErrorCode.DESK_PORT_ERR.getErrMsg());
		}
		// 5.检查成本中心
		Cost cost = costService.selectByPrimaryKey(book[2]);// 成本中心
		if (StringUtils.isEmpty(book[2]) || null == cost) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.book,For.[book=SECUR_SD_100,或查询RATE表]",
					WksErrorCode.BOOK_NOT_FOUND.getErrCode(), WksErrorCode.BOOK_NOT_FOUND.getErrMsg());
		}
		// 6.检查交易对手
		Cust cust = custService.selectByPrimaryKey(cbt.getCno());
		if (StringUtils.isEmpty(cbt.getCno()) || null == cust) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.cno,For.[cno=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(),
					WksErrorCode.CUST_NOT_FOUND.getErrMsg());
		}
		// 7.检查交易员
		Trad trad = tradService.selectByPrimaryKey(cbt.getInstId(), cbt.getTrad());
		if (StringUtils.isNotEmpty(cbt.getTrad()) && null == trad) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.trad,For.[trad=TRAD]", WksErrorCode.TRAD_NOT_FOUND.getErrCode(),
					WksErrorCode.TRAD_NOT_FOUND.getErrMsg());
		}
		// 10.检查债券是否存在
		Secm secm = secmService.selectByPrimaryKey(cbt.getBondCode());
		if (StringUtils.isEmpty(cbt.getBondCode()) || null == secm) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.trad,For.[trad=TRAD]", WksErrorCode.CBT_BOND_NULL.getErrCode(),
					WksErrorCode.CBT_BOND_NULL.getErrMsg());
		}
		// 11.托管账户,暂时不校验

	}

	/**
	 * 静态默认数据,该处禁止查询数据库进行转换值,会出现空指针
	 *
	 * @param spsh
	 * @return
	 */
	public Spsh DataTrans(Spsh spsh) {
		spsh.setSeq(OpicsConstant.SPSH.SEQ);// 序号:默认 0
		spsh.setFixincind(OpicsConstant.SPSH.FIXINCIND);// 固定收入标志:默认1
		spsh.setAssignind(OpicsConstant.SPSH.ASSIGNIND);// 分配标志:默认1
		spsh.setBvamortamt(BigDecimal.ZERO);// 抵押品分期付款金额:默认0.0000
		spsh.setBvintamt(BigDecimal.ZERO);// 抵押品利息:默认0.0000
		spsh.setBrok(OpicsConstant.SPSH.BROK);// 经纪公司编号:默认D
		spsh.setBrokfeeamt(BigDecimal.ZERO);// 经纪人费用:默认0.0000
		spsh.setChargeamt(BigDecimal.ZERO);// 费用:
		spsh.setCommamt(BigDecimal.ZERO);// 佣金:默认0.0000
		spsh.setCommrate8(BigDecimal.ZERO);// 佣金率:默认0.0000
		spsh.setCcyauthind(OpicsConstant.SPSH.CCYAUTHIND);// 流通授权标志:默认1
		spsh.setCcyauthoper(OpicsConstant.SPSH.CCYAUTHOPER);// 流通授权操作员:
		spsh.setFailintamt(BigDecimal.ZERO);// 交易失败利息:默认0.0000
		spsh.setFeeamt(BigDecimal.ZERO);// 费用:默认0.0000
		spsh.setGainamt(BigDecimal.ZERO);// 收益:默认0.0000
		spsh.setInputdate(Calendar.getInstance().getTime());// 系统输入日期:录入时间
		spsh.setIoper(OpicsConstant.SPSH.TRAD_NO);// 输入操作员:管理要素—交易员
		spsh.setInputtime(DateUtil.getCurrentTimeAsString());// 系统输入时间:time
		spsh.setLossamt(BigDecimal.ZERO);// 损失金额:默认0.0000
		spsh.setOrigamt(BigDecimal.ZERO);// 初始金额:默认0.0000
		spsh.setPremamt(BigDecimal.ZERO);// 溢价:
		spsh.setVatamt(BigDecimal.ZERO);// 增值税:默认0.0000
		spsh.setVerind(OpicsConstant.SPSH.VERIND);// 复核标志:
		spsh.setVoper(OpicsConstant.SPSH.VOPER);// 复核操作员:
		spsh.setExchcommamt(BigDecimal.ZERO);// 交易佣金:默认0.0000
		spsh.setWhtamt(BigDecimal.ZERO);// 计提税金额:
		spsh.setExchind(OpicsConstant.SPSH.EXCHIND);// 兑换标志:
		spsh.setOffassamt(BigDecimal.ZERO);// 官方分配数量:
		spsh.setEstamt(BigDecimal.ZERO);// EST金额:
		spsh.setSettprocexchrate8(new BigDecimal("1"));// 结算币种对收益币种汇率:
		spsh.setSettprocterms(OpicsConstant.SPSH.SETTPROCTERMS);// 结算币种对收益币种汇率乘除形式:
		spsh.setSettprocpremdisc8(BigDecimal.ZERO);// 结算币种对收益币种汇率升/贴水:
		spsh.setIntsettexchrate8(new BigDecimal("1"));// 利息币种对结算币种汇率:
		spsh.setIntsettterms(OpicsConstant.SPSH.INTSETTTERMS);// 利息币种对结算币种汇率乘除形式:
		spsh.setIntsettpremdisc8(BigDecimal.ZERO);// 利息币种对结算币种汇率升/贴水:
		spsh.setSettbaseexchrate8(new BigDecimal("1"));// 结算币种对基础币种汇率:
		spsh.setSettbaseterms(OpicsConstant.SPSH.SETTBASETERMS);// 结算币种对基础币种汇率乘除形式:
		spsh.setSettbasepremdisc8(BigDecimal.ZERO);// 结算币种对基础币种汇率升/贴水:
		spsh.setIntbaseexchrate8(new BigDecimal("1"));// 利息币种对基础币种汇率:
		spsh.setIntbaseterms(OpicsConstant.SPSH.INTBASETERMS);// 利息币种对基础币种汇率乘除形是:
		spsh.setIntbasepremdisc8(BigDecimal.ZERO);// 利息币种对基础币种汇率升/贴水:
		spsh.setInternalyield8(BigDecimal.ZERO);// 收益率:
		spsh.setInternaldiscrate8(BigDecimal.ZERO);// 内部折扣率:
		spsh.setInternalprice8(BigDecimal.ZERO);// 内部价格:
		spsh.setInternalproceedamt(BigDecimal.ZERO);// 内部收益:
		spsh.setInternalspreadrate8(BigDecimal.ZERO);// 内部利率差:
		spsh.setMarkupcommamt(BigDecimal.ZERO);// 估值:
		spsh.setAmortyield8(BigDecimal.ZERO);// 分期偿还收益:
		spsh.setAmortind(OpicsConstant.SPSH.AMORTIND);// 分期偿还标志:
		spsh.setPairedfaceamt(BigDecimal.ZERO);// 抵押物面值:
		spsh.setPairedamt(BigDecimal.ZERO);// 抵押物金额:
		spsh.setPairedind(OpicsConstant.SPSH.PAIREDIND);// 抵押物标识:
		spsh.setCurrsplitseq(OpicsConstant.SPSH.CURRSPLITSEQ);// 序号:
		spsh.setUpdatecounter(OpicsConstant.SPSH.UPDATE_COUNTER);// 更新次数:
		spsh.setSplitind(OpicsConstant.SPSH.SPLITIND);// :
		spsh.setIndexratio8(BigDecimal.ZERO);// :
		spsh.setSwiftbothind(OpicsConstant.SPSH.SWIFTBOTHIND);// :
		spsh.setNewissueind(OpicsConstant.SPSH.NEWISSUEIND);// :
		spsh.setCcpind(OpicsConstant.SPSH.CCPIND);// :
		spsh.setExtcompare(OpicsConstant.SPSH.EXTCOMPARE);// :
		spsh.setNovfaceamt(BigDecimal.ZERO);// :
		spsh.setPaymentholdind(OpicsConstant.SPSH.PAYMENTHOLDIND);// :
		spsh.setExciseamt(BigDecimal.ZERO);// :
		spsh.setIntraday(OpicsConstant.SPSH.INTRADAY);// :
		// 4.5版本有这个字段
		// spsh.setContype(OpicsConstant.SPSH.CONTYPE);//:

		spsh.setDealtime(DateUtil.getCurrentTimeAsString());// 交易时间:
		spsh.setDiscrate8(BigDecimal.ZERO);// 折扣率:

		spsh.setSecauthind(OpicsConstant.SPSH.SECAUTHIND);// 债券授权标志:
		spsh.setSecauthoper(OpicsConstant.SPSH.TRAD_NO);// 债券授权操作员:
		spsh.setSpreadrate8(BigDecimal.ZERO);// 利率差:
		spsh.setSuppconfind(OpicsConstant.SPSH.SUPPCONFIND);// 复核确认:
		spsh.setPrinagind(OpicsConstant.SPSH.PRINAGIND);// 自营代理标志:

		return spsh;
	}

	/**
	 * 交易数据
	 *
	 * @param spsh
	 * @param cbt
	 * @return
	 */
	public Spsh DataTrans(Spsh spsh, CbtWksBean cbt) {
		Secm secm = secmService.selectByPrimaryKey(cbt.getBondCode());// 查询债券信息
		BigDecimal faceamt = cbt.getTotalFaceValue();// 票面金额:
		BigDecimal discamt = faceamt
				.multiply((cbt.getNetPrice().subtract(new BigDecimal(100))).divide(new BigDecimal(100)));// 折价金额
		BigDecimal disprice8 = cbt.getNetPrice().multiply(new BigDecimal(secm.getDenom().trim()));// 牌价
		spsh.setBr(cbt.getInstId());// 分行号:机构号
		String[] book = StringUtils.split(cbt.getBook(), "_");// 产品+类型+COST
		spsh.setProduct(book[0]);// 产品代码:
		spsh.setProdtype(book[1]);// 产品类型:
		spsh.setPort(cbt.getDesk());// 投资组合编号:管理要素—投组
		spsh.setCost(book[2]);// 成本中心:管理要素—COST
		spsh.setCno(cbt.getCno());// 交易对手编号:交易对手；CUST里面维护
		spsh.setTrad(StringUtils.defaultIfEmpty(cbt.getTrad(), OpicsConstant.SPSH.TRAD_NO));// 交易员
		spsh.setDealtext(cbt.getTradeNo());// 将交易编号放入,用于冲销等存续期动作
		// spsh.setCcyauthind("");
		spsh.setBrokfeeccy(secm.getCcy().trim());// 经纪人费用币种:默认基础信息债券币种
		spsh.setCcyauthdate(DateUtil.parse(cbt.getTransDate()));// 授权日期:
		spsh.setCcy(secm.getCcy().trim());// 币种代码:默认基础信息债券币种
		spsh.setDealdate(DateUtil.parse(cbt.getTransDate()));// 交易日期:交易日
		spsh.setDiscamt(discamt);// 折价金额:=面值*(净价-100)/100
		spsh.setDisprice8(disprice8);// 牌价:=SECM.denom*净价
		spsh.setFaceamt(faceamt);// 票面金额:
		spsh.setInvtype(cbt.getInvtype());// 投资类型:管理要素—A/T/H/S/I 可供出售
		spsh.setOrigqty(faceamt);// 初始数量:
		spsh.setPrice8(cbt.getNetPrice());// 价格:净价
		spsh.setPrinamt(getPlusNegateAmt(cbt.getTrdType(), faceamt));// 本金金额:本金
		spsh.setProceedamt(getPlusNegateAmt(cbt.getTrdType(), cbt.getFullAmt()));// 收入:交割金额
		spsh.setPs(getPs(cbt.getTrdType()));// 买卖标志:
		spsh.setPurchintamt(getPlusNegateAmt(cbt.getTrdType(), cbt.getTotalInterestAmount()));// 买入利息金额:应计利息
		spsh.setQty(cbt.getTradeAmount());// 数量:手数
		spsh.setSecsacct(cbt.getSecsacct());// 托管帐户:托管账户
		spsh.setSecauthdate(DateUtil.parse(cbt.getTransDate()));// 债券授权日期:
		spsh.setSecid(cbt.getBondCode());// 债券编号:债券ID
		spsh.setCcysacct(cbt.getCcysacct());// 资金结算账号:清算账号
		spsh.setSettdate(DateUtil.parse(cbt.getSettlementAmount()));// 结算日期:交割日
		spsh.setCcysmeans(cbt.getSettlementMethod());// 资金结算方式:清算路径
		spsh.setVerdate(DateUtil.parse(cbt.getTransDate()));// 复核日期:
		spsh.setYield8(cbt.getYield());// 收益率:
		spsh.setAssignedqty(cbt.getTradeAmount());// 分配数量:
		spsh.setConvintamt(getPlusNegateAmt(cbt.getTrdType(), cbt.getTotalInterestAmount()));// 兑换后利息金额:
		spsh.setConvintbamt(getPlusNegateAmt(cbt.getTrdType(), cbt.getTotalInterestAmount()));// 兑换后利息基数:
		spsh.setSettamt(getPlusNegateAmt(cbt.getTrdType(), cbt.getFullAmt()));// 清算金额:交割金额
		spsh.setSettbaseamt(getPlusNegateAmt(cbt.getTrdType(), cbt.getFullAmt()));// 结算金额兑换成基础币种金额:折本币金额
		// spsh.setCostamt(getPlusNegateAmt(cbt.getTrdType(), disprice8));//
		// 成本金额:面值*净价/100
		spsh.setCostamt(
				getPlusNegateAmt(cbt.getTrdType(), faceamt.multiply(cbt.getNetPrice().divide(new BigDecimal(100)))));
		// 成本基本币种金额:
		spsh.setCostbamt(
				getPlusNegateAmt(cbt.getTrdType(), faceamt.multiply(cbt.getNetPrice().divide(new BigDecimal(100)))));
		spsh.setIntccy(secm.getCcy().trim());// 利息币种:默认基础信息债券币种
		spsh.setSettccy(secm.getCcy().trim());// 清算币种:默认基础信息债券币种

		String settday = secm.getSettdays().trim();
		int days = DateUtil.daysBetween(spsh.getDealdate(), spsh.getSettdate());
		if (settday.equals(String.valueOf(days))) {
			spsh.setDelaydelivind("N");// 延期交货标志:与基础信息SETT DAY匹配；交易交割日满足条件Y/不满足N
		} else {
			spsh.setDelaydelivind("Y");// 延期交货标志:与基础信息SETT DAY匹配；交易交割日满足条件Y/不满足N
		}

		return spsh;
	}

	public Drpi DataTrans(Drpi drpi, Spsh spsh) {
		drpi.setBr(spsh.getBr());// 机构:
		drpi.setProduct(spsh.getProduct());// 产品:
		drpi.setProdtype(spsh.getProdtype());// 产品类型:
		drpi.setDealno(spsh.getDealno());// 单号:
		drpi.setSeq(spsh.getSeq());// 序号:
		drpi.setDelrecind("P".equals(spsh.getPs()) ? "R" : "D");// 债的收付方向:
		drpi.setSupsecmoveind(OpicsConstant.SPSH.SUPSECMOVEIND);// 默认N :
		drpi.setSupccymoveind(OpicsConstant.SPSH.SUPCCYMOVEIND);// 默认N:
		drpi.setLstmntdate(Calendar.getInstance().getTime());// :

		return drpi;
	}

	/**
	 * 获取dealno
	 * <p>
	 * 现券买卖的单号比较特殊,需要进行特殊处理
	 *
	 * @param spsh
	 * @param mcfpService
	 * @return
	 */
	public Spsh DataTrans(Spsh spsh, McfpService mcfpService) {
		// 获取DEALNO
		Mcfp mcfp = mcfpService.selectByPrimaryKey(spsh.getBr(), spsh.getProduct(), spsh.getProdtype());// 设置交易编号
		String dealno = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";// 获取
		spsh.setDealno(dealno);// 设置交易编号
		return spsh;
	}

	/**
	 * 账务日期
	 *
	 * @param spsh
	 * @param brpsService
	 * @return
	 */
	public Spsh DataTrans(Spsh spsh, BrpsService brpsService) {
		// 获取当前账务日期
		Brps brps = brpsService.selectByPrimaryKey(spsh.getBr());
		spsh.setBrprcindte(brps.getBranprcdate());// 当前账务日期
		return spsh;
	}

	/**
	 * 更新交易单号
	 * <p>
	 * 拆借业务的单号比较特殊,需要进行特殊处理
	 *
	 * @param spsh
	 * @param mcfpService
	 * @param mcfp
	 */
	public void DataTrans(Spsh spsh, McfpService mcfpService, Mcfp mcfp) {
		mcfp.setBr(spsh.getBr());
		mcfp.setTraddno(StringUtils.trimToEmpty(spsh.getDealno()));
		mcfp.setProdcode(spsh.getProduct());// 产品代码 去掉空格
		mcfp.setType(spsh.getProdtype());// 产品类型
		mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
	}

	/**
	 * 根据收付方向判断金额正负
	 */
	private BigDecimal getPlusNegateAmt(String drcrind, BigDecimal amt) {
		if (StringUtils.equals(drcrind, OpicsConstant.SPSH.PAY_IND)
				|| StringUtils.equals(drcrind, OpicsConstant.SPSH.LIAB_IND)) {
			return amt.negate();// 取反
		} else {
			return amt.plus();// 取正
		}
	}

	private String getPs(String str) {
		if ("CBT001".equalsIgnoreCase(str)) {
			return "P";
		} else if ("CBT002".equalsIgnoreCase(str)) {
			return "S";
		}
		return "";
	}

}
