package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.FxdItemWksBean;
import com.singlee.financial.wks.expand.SwiftMethod;
import com.singlee.financial.wks.mapper.opics.PsixMapper;
import com.singlee.financial.wks.service.opics.PsixService;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PsixServiceImpl implements PsixService {

    @Resource
    private PsixMapper psixMapper;
    @Resource
    private BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br, String product, String type, String dealno, String seq) {
        return psixMapper.deleteByPrimaryKey(br, product, type, dealno, seq);
    }

    @Override
    public int insert(Psix record) {
        return psixMapper.insert(record);
    }

    @Override
    public int insertSelective(Psix record) {
        return psixMapper.insertSelective(record);
    }

    @Override
    public Psix selectByPrimaryKey(String br, String product, String type, String dealno, String seq) {
        return psixMapper.selectByPrimaryKey(br, product, type, dealno, seq);
    }

    @Override
    public int updateByPrimaryKeySelective(Psix record) {
        return psixMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Psix record) {
        return psixMapper.updateByPrimaryKey(record);
    }

    @Override
    public void updateBatch(List<Psix> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.PsixMapper.updateByPrimaryKeySelective", list);
    }

    @Override
    public void batchInsert(List<Psix> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.PsixMapper.insert", list);
    }

    /**
     * 方法已重载.构建付款确认、付款报文状态对象(PSIX)
     * 
     * @param br
     * @param product
     * @param type
     * @param dealno
     * @param seq
     * @param p1
     * @param p2
     * @param p3
     * @param p4
     * @param det
     * @param r1
     * @param r2
     * @param r3
     * @param r4
     * @param r5
     * @param r6
     * @param bif
     * @param suppayind
     * @param supconfind
     * @param lstmntdte
     * @return
     */
    private Psix builder(String br, String product, String type, String dealno, String seq, String p1, String p2,
        String p3, String p4, String det, String r1, String r2, String r3, String r4, String r5, String r6, String bif,
        String suppayind, String supconfind, Date lstmntdte) {
        Psix psix = new Psix();
        // 部门
        psix.setBr(br);
        // 产品代码
        psix.setProduct(product);
        // 业务类型
        psix.setType(type);
        // 交易编号
        psix.setDealno(dealno);
        // 序号
        psix.setSeq(seq);
        //
        psix.setP1(p1);
        //
        psix.setP2(p2);
        //
        psix.setP3(p3);
        //
        psix.setP4(p4);
        //
        psix.setDet(det);
        //
        psix.setR1(r1);
        //
        psix.setR2(r2);
        //
        psix.setR3(r3);
        //
        psix.setR4(r4);
        //
        psix.setR5(r5);
        //
        psix.setR6(r6);
        //
        psix.setBif(bif);
        // 是否禁用确认报文
        psix.setSupconfind(supconfind);
        // 是否禁用付款报文
        psix.setSuppayind(suppayind);
        // 最后更新日期
        psix.setLstmntdte(lstmntdte);

        return psix;
    }

    /**
     * 方法已重载.构建付款确认、付款报文状态对象(PSIX),设置默认值
     * 
     * @param br
     * @param product
     * @param type
     * @param dealno
     * @param seq
     * @return
     */
    private Psix builder(String br, String product, String type, String dealno, String seq,Date bpdate) {
        return builder(br, product, type, dealno, seq,bpdate,OpicsConstant.PCIX.SUP_CONF_IND, OpicsConstant.PCIX.SUP_PAY_IND);
    }
    
    /**
     * 方法已重载.构建付款确认、付款报文状态对象(PSIX),设置默认值
     * @param br
     * @param product
     * @param type
     * @param dealno
     * @param seq
     * @param supConfInd
     * @param supPayInd
     * @return
     */
    private Psix builder(String br, String product, String type, String dealno, String seq,Date bpdate,String supConfInd,String supPayInd) {
    	return builder(br, product, type, dealno, seq, "", "", "", "", "", "", "", "", "", "", "",
                OpicsConstant.PCIX.BIF_FLAG,supPayInd,supConfInd,bpdate);
    }

    @Override
    public Psix builder(Dldt dl) {
        return builder(dl.getBr(), dl.getProduct(), dl.getProdtype(), dl.getDealno(), dl.getSeq(),dl.getBrprcindte());
    }

    @Override
    public Psix builder(Otdt otc) {
        return builder(otc.getBr(), otc.getProduct(), otc.getProdtype(), otc.getDealno(), otc.getSeq(),otc.getBrprcindte());
    }

    @Override
	public Psix builder(Fxdh fxdh, FxdItemWksBean itemBean) {
		if (null == fxdh || null == itemBean) {
			JY.raise("Psix外汇对象输入错误");
		}

		/*
		 * 集中清算时不发送确认报文和付款报文
		 */
		String supPayind = null;
		String supConfInd = null;

		if (SwiftMethod.DISABLE.equals(itemBean.getSwiftMethod())) {
			supPayind = "Y";
			supConfInd = "Y";
		} else if (SwiftMethod.CONFIRMATION.equals(itemBean.getSwiftMethod())) {
			supPayind = "Y";
			supConfInd = "N";
		} else if (SwiftMethod.PAYANDREC.equals(itemBean.getSwiftMethod())) {
			supPayind = "N";
			supConfInd = "Y";
		} else if (SwiftMethod.ENABLE.equals(itemBean.getSwiftMethod())) {
			supPayind = "N";
			supConfInd = "N";
		} else {
			throw new RuntimeException("FxdItemWksBean.swiftMethod处理方法暂不支持或者为空");
		}
		return builder(fxdh.getBr(), fxdh.getProdcode(), fxdh.getProdtype(), fxdh.getDealno(), fxdh.getSeq(), fxdh.getBrprcindte(), supConfInd, supPayind);
	}

    @Override
    public Psix builder(Swdh swdh, List<Swdt> swdtLst,SwapWksBean swap) {
        List<Psix> psixLst = new ArrayList<Psix>();
        if (null == swdtLst || swdtLst.size() == 0) {
            JY.raise("利率互换/货币互换明细对象为空!");
        }
        
        LogFactory.getLog(PsixServiceImpl.class).debug("利率互换交易[" + swdh.getDealno() + "],Psix.SwiftMethod:" + swap.getSwiftMethod());
        
        /*
         * 集中清算时不发送确认报文和付款报文
         */
        swdtLst.stream().filter(swdt -> "P".equalsIgnoreCase(StringUtils.trim(swdt.getPayrecind()))).forEach(swdt -> {
        	String supPayind = null;
        	String supConfInd = null;
			switch (swap.getSwiftMethod()) {
			case DISABLE:
				supPayind = "Y";
				supConfInd = "Y";
				break;
			case CONFIRMATION:
				supPayind = "Y";
				supConfInd = "N";
				break;
			case PAYANDREC:
				supPayind = "N";
				supConfInd = "Y";
				break;
			case ENABLE:
				supPayind = "N";
				supConfInd = "N";
				break;
			default:
				throw new RuntimeException("SwapWksBean.swiftMethod处理方法[ " + swap.getSwiftMethod() + " ]暂不支持或者为空");
			}
        	psixLst.add(builder(swdt.getBr(), swdt.getProduct(), swdt.getProdtype(), swdt.getDealno(), swdt.getSeq(),swdh.getBrprcindte(),supConfInd,supPayind));
        });
        return psixLst.get(0);
    }

	@Override
	public Psix builder(Spsh spsh) {
		return builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(),spsh.getBrprcindte());
	}
	@Override
	public Psix builder(Sldh sldh) {
		return builder(sldh.getBr(), sldh.getProduct(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(),sldh.getBrprcindte());
	}

	@Override
	public Psix selectByDealno(String br, String product, String type, String dealno) {
		return psixMapper.selectByDealno(br, product, type, dealno);
	}
}
