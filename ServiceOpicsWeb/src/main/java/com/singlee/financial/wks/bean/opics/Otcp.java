package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Otcp implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String product;

    private String prodtype;

    private String otctype;

    private BigDecimal delta8;

    private BigDecimal theta8;

    private BigDecimal gamma8;

    private BigDecimal vega8;

    private BigDecimal mktpremamt;

    private BigDecimal mktpremperc8;

    private BigDecimal volatility8;

    private BigDecimal uaprice8;

    private String interfacenum;

    private BigDecimal rho8;

    private BigDecimal vanna8;

    private BigDecimal phi8;

    private BigDecimal hedge;

    private BigDecimal forward8;

    private BigDecimal depo8;

    private BigDecimal ctrdepo8;

    private BigDecimal histspot8;

    private BigDecimal histforward8;

    private BigDecimal mktpremval;

    private BigDecimal profit;

    private String text1;

    private String text2;

    private String text3;

    private BigDecimal rate18;

    private BigDecimal rate28;

    private BigDecimal rate38;

    private BigDecimal amt1;

    private BigDecimal amt2;

    private Date date1;

    private Date date2;

    private BigDecimal impliedqty;

    private BigDecimal marketpremamt;

    private BigDecimal ystmktpremamt;

    private BigDecimal tdyintrinsicval;

    private BigDecimal ystintrinsicval;

    private BigDecimal ccydeltaamt;

    private BigDecimal ctrdeltaamt;

    private BigDecimal basemktpremamt;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getOtctype() {
		return otctype;
	}


	public void setOtctype(String otctype) {
		this.otctype = otctype;
	}


	public BigDecimal getDelta8() {
		return delta8;
	}


	public void setDelta8(BigDecimal delta8) {
		this.delta8 = delta8;
	}


	public BigDecimal getTheta8() {
		return theta8;
	}


	public void setTheta8(BigDecimal theta8) {
		this.theta8 = theta8;
	}


	public BigDecimal getGamma8() {
		return gamma8;
	}


	public void setGamma8(BigDecimal gamma8) {
		this.gamma8 = gamma8;
	}


	public BigDecimal getVega8() {
		return vega8;
	}


	public void setVega8(BigDecimal vega8) {
		this.vega8 = vega8;
	}


	public BigDecimal getMktpremamt() {
		return mktpremamt;
	}


	public void setMktpremamt(BigDecimal mktpremamt) {
		this.mktpremamt = mktpremamt;
	}


	public BigDecimal getMktpremperc8() {
		return mktpremperc8;
	}


	public void setMktpremperc8(BigDecimal mktpremperc8) {
		this.mktpremperc8 = mktpremperc8;
	}


	public BigDecimal getVolatility8() {
		return volatility8;
	}


	public void setVolatility8(BigDecimal volatility8) {
		this.volatility8 = volatility8;
	}


	public BigDecimal getUaprice8() {
		return uaprice8;
	}


	public void setUaprice8(BigDecimal uaprice8) {
		this.uaprice8 = uaprice8;
	}


	public String getInterfacenum() {
		return interfacenum;
	}


	public void setInterfacenum(String interfacenum) {
		this.interfacenum = interfacenum;
	}


	public BigDecimal getRho8() {
		return rho8;
	}


	public void setRho8(BigDecimal rho8) {
		this.rho8 = rho8;
	}


	public BigDecimal getVanna8() {
		return vanna8;
	}


	public void setVanna8(BigDecimal vanna8) {
		this.vanna8 = vanna8;
	}


	public BigDecimal getPhi8() {
		return phi8;
	}


	public void setPhi8(BigDecimal phi8) {
		this.phi8 = phi8;
	}


	public BigDecimal getHedge() {
		return hedge;
	}


	public void setHedge(BigDecimal hedge) {
		this.hedge = hedge;
	}


	public BigDecimal getForward8() {
		return forward8;
	}


	public void setForward8(BigDecimal forward8) {
		this.forward8 = forward8;
	}


	public BigDecimal getDepo8() {
		return depo8;
	}


	public void setDepo8(BigDecimal depo8) {
		this.depo8 = depo8;
	}


	public BigDecimal getCtrdepo8() {
		return ctrdepo8;
	}


	public void setCtrdepo8(BigDecimal ctrdepo8) {
		this.ctrdepo8 = ctrdepo8;
	}


	public BigDecimal getHistspot8() {
		return histspot8;
	}


	public void setHistspot8(BigDecimal histspot8) {
		this.histspot8 = histspot8;
	}


	public BigDecimal getHistforward8() {
		return histforward8;
	}


	public void setHistforward8(BigDecimal histforward8) {
		this.histforward8 = histforward8;
	}


	public BigDecimal getMktpremval() {
		return mktpremval;
	}


	public void setMktpremval(BigDecimal mktpremval) {
		this.mktpremval = mktpremval;
	}


	public BigDecimal getProfit() {
		return profit;
	}


	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}


	public String getText1() {
		return text1;
	}


	public void setText1(String text1) {
		this.text1 = text1;
	}


	public String getText2() {
		return text2;
	}


	public void setText2(String text2) {
		this.text2 = text2;
	}


	public String getText3() {
		return text3;
	}


	public void setText3(String text3) {
		this.text3 = text3;
	}


	public BigDecimal getRate18() {
		return rate18;
	}


	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}


	public BigDecimal getRate28() {
		return rate28;
	}


	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}


	public BigDecimal getRate38() {
		return rate38;
	}


	public void setRate38(BigDecimal rate38) {
		this.rate38 = rate38;
	}


	public BigDecimal getAmt1() {
		return amt1;
	}


	public void setAmt1(BigDecimal amt1) {
		this.amt1 = amt1;
	}


	public BigDecimal getAmt2() {
		return amt2;
	}


	public void setAmt2(BigDecimal amt2) {
		this.amt2 = amt2;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public BigDecimal getImpliedqty() {
		return impliedqty;
	}


	public void setImpliedqty(BigDecimal impliedqty) {
		this.impliedqty = impliedqty;
	}


	public BigDecimal getMarketpremamt() {
		return marketpremamt;
	}


	public void setMarketpremamt(BigDecimal marketpremamt) {
		this.marketpremamt = marketpremamt;
	}


	public BigDecimal getYstmktpremamt() {
		return ystmktpremamt;
	}


	public void setYstmktpremamt(BigDecimal ystmktpremamt) {
		this.ystmktpremamt = ystmktpremamt;
	}


	public BigDecimal getTdyintrinsicval() {
		return tdyintrinsicval;
	}


	public void setTdyintrinsicval(BigDecimal tdyintrinsicval) {
		this.tdyintrinsicval = tdyintrinsicval;
	}


	public BigDecimal getYstintrinsicval() {
		return ystintrinsicval;
	}


	public void setYstintrinsicval(BigDecimal ystintrinsicval) {
		this.ystintrinsicval = ystintrinsicval;
	}


	public BigDecimal getCcydeltaamt() {
		return ccydeltaamt;
	}


	public void setCcydeltaamt(BigDecimal ccydeltaamt) {
		this.ccydeltaamt = ccydeltaamt;
	}


	public BigDecimal getCtrdeltaamt() {
		return ctrdeltaamt;
	}


	public void setCtrdeltaamt(BigDecimal ctrdeltaamt) {
		this.ctrdeltaamt = ctrdeltaamt;
	}


	public BigDecimal getBasemktpremamt() {
		return basemktpremamt;
	}


	public void setBasemktpremamt(BigDecimal basemktpremamt) {
		this.basemktpremamt = basemktpremamt;
	}


	private static final long serialVersionUID = 1L;
}