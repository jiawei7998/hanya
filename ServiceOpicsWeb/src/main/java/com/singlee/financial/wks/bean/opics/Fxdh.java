package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Fxdh implements Serializable {
	/**
	 * 核算机构编号
	 */
	private String br;

	/**
	 * 交易号
	 */
	private String dealno;

	/**
	 * 序号
	 */
	private String seq;

	/**
	 * 交易员
	 */
	private String trad;

	/**
	 * 生效日期
	 */
	private Date vdate;

	/**
	 * 交易对手编号
	 */
	private String cust;

	/**
	 * 经纪人编号
	 */
	private String brok;

	/**
	 * 经纪费用币种
	 */
	private String brokccy;

	/**
	 * 经纪费用
	 */
	private BigDecimal brokamt;

	/**
	 * 电话确认标志
	 */
	private String phonci;

	/**
	 * 投资组合
	 */
	private String port;

	/**
	 * 成本中心
	 */
	private String cost;

	/**
	 * 期权开始日期
	 */
	private Date odate;

	/**
	 * 交易日期
	 */
	private Date dealdate;

	/**
	 * 交易时间
	 */
	private String dealtime;

	/**
	 * 输入操作员
	 */
	private String ioper;

	/**
	 * 复核操作员
	 */
	private String voper;

	/**
	 * 经纪人确认日期
	 */
	private Date brokcdate;

	/**
	 * 客户确认日期
	 */
	private Date custcdate;

	/**
	 * 电话确认日期
	 */
	private Date phonecdate;

	/**
	 * 交易说明
	 */
	private String dealtext;

	/**
	 * 电话确认内容
	 */
	private String phonetext;

	/**
	 * 买卖标志
	 */
	private String ps;

	/**
	 * 产品代码
	 */
	private String prodcode;

	/**
	 * 确认标志
	 */
	private String verind;

	/**
	 * 撤销原因
	 */
	private String revtext;

	/**
	 * 掉期交易编号
	 */
	private String swapdeal;

	/**
	 * 远期\近期交易标志
	 */
	private String farnearind;

	/**
	 * 付款授权标志
	 */
	private String payauthind;

	/**
	 * 掉期交易生效日期
	 */
	private Date swapvdate;

	/**
	 * 撤销原因代码
	 */
	private String revreason;

	/**
	 * 输入系统日期
	 */
	private Date inputdate;

	/**
	 * 输入系统时间
	 */
	private String inputtime;

	/**
	 * 交易源代码
	 */
	private String dealsrce;

	/**
	 * 初始货币金额
	 */
	private BigDecimal origocamt;

	/**
	 * 跟踪日期
	 */
	private Date trcedate;

	/**
	 * TRACECNT
	 */
	private String tracecnt;

	/**
	 * 付款操作员
	 */
	private String payoper;

	/**
	 * 上次更新日期
	 */
	private Date lstmntdate;

	/**
	 * 货币１
	 */
	private String ccy;

	/**
	 * 货币１金额
	 */
	private BigDecimal ccyamt;

	/**
	 * 货币１汇率乘除形式
	 */
	private String ccyterms;

	/**
	 * 货币１汇率
	 */
	private BigDecimal ccyrate8;

	/**
	 * 货币１升水/贴水
	 */
	private BigDecimal ccypd8;

	/**
	 * 货币１相应的基础货币金额
	 */
	private BigDecimal ccybamt;

	/**
	 * 货币１对基础货币汇率
	 */
	private BigDecimal ccybrate8;

	/**
	 * 货币１对基础货币升水/贴水
	 */
	private BigDecimal ccybpd8;

	/**
	 * 基础货币汇率乘除形式
	 */
	private String baseterms;

	/**
	 * 清算标志
	 */
	private String netsi;

	/**
	 * 即期/远期标志
	 */
	private String spotfwdind;

	/**
	 * 清算交易码（未用）
	 */
	private String netdeal;

	/**
	 * 货币1结算方式
	 */
	private String ccysmeans;

	/**
	 * 货币1结算帐户
	 */
	private String ccysacct;

	/**
	 * 货币2结算方式
	 */
	private String ctrsmeans;

	/**
	 * 货币2结算帐户
	 */
	private String ctrsacct;

	/**
	 * 货币2
	 */
	private String ctrccy;

	/**
	 * 货币2金额
	 */
	private BigDecimal ctramt;

	/**
	 * 货币2汇率
	 */
	private BigDecimal ctrrate8;

	/**
	 * 货币2对基础货币升水/贴水
	 */
	private BigDecimal ctrpd8;

	/**
	 * 货币2相应的基础货币金额
	 */
	private BigDecimal ctrbamt;

	/**
	 * 货币2对基础货币汇率
	 */
	private BigDecimal ctrbrate8;

	/**
	 * 货币1清算编号
	 */
	private String nsrnoccy;

	/**
	 * 货币1清算流水号
	 */
	private String nsrccyseq;

	/**
	 * 货币2清算编号
	 */
	private String nsrnoctr;

	/**
	 * 货币2清算流水号
	 */
	private String nsrctrseq;

	/**
	 * 分行处理输入日期
	 */
	private Date brprcindte;

	/**
	 * 收款操作员
	 */
	private String recoper;

	/**
	 * 付款授权日期
	 */
	private Date payauthdte;

	/**
	 * 收款授权日期
	 */
	private Date recauthdte;

	/**
	 * 确认日期
	 */
	private Date verdate;

	/**
	 * 撤销日期
	 */
	private Date revdate;

	/**
	 * 货币1重估
	 */
	private BigDecimal ccyrevalamt;

	/**
	 * 货币2重估
	 */
	private BigDecimal ctrrevalamt;

	/**
	 * 货币1净现值
	 */
	private BigDecimal ccynpvamt;

	/**
	 * 货币2净现值
	 */
	private BigDecimal ctrnpvamt;

	/**
	 * 收款授权
	 */
	private String recauthoper;

	/**
	 * 付款授权操作员
	 */
	private String payauthoper;

	/**
	 * 标志指令
	 */
	private String siind;

	/**
	 * 产品类型代码
	 */
	private String prodtype;

	/**
	 * 即期交易交易员
	 */
	private String spottrad;

	/**
	 * 即期投资组合
	 */
	private String spotport;

	/**
	 * 即期成本中心
	 */
	private String spotcost;

	/**
	 * 内部汇率
	 */
	private BigDecimal internalrate8;

	/**
	 * 交易所汇率差
	 */
	private BigDecimal corpspread8;

	/**
	 * 交易所成本中心
	 */
	private String corpcost;

	/**
	 * 分行汇率差
	 */
	private BigDecimal brspread8;

	/**
	 * 分行成本中心
	 */
	private String brcost;

	/**
	 * 期限
	 */
	private String tenor;

	/**
	 * 货币1结算日期
	 */
	private Date ccysettdate;

	/**
	 * 货币2结算日期
	 */
	private Date ctrsettdate;

	/**
	 * 客户输入标志
	 */
	private String custenteredind;

	/**
	 * 相关交易码
	 */
	private String linkdealno;

	/**
	 * 相关产品代码
	 */
	private String linkproduct;

	/**
	 * 相关产品类型代码
	 */
	private String linkprodtype;

	/**
	 * 金融中心日历1
	 */
	private String fincntr1;

	/**
	 * 金融中心日历2
	 */
	private String fincntr2;

	/**
	 * 佣金
	 */
	private BigDecimal commamt;

	/**
	 * 超过总限额标志
	 */
	private String excoveralllim;

	/**
	 * 超过客户额度标志
	 */
	private String exccustlim;

	/**
	 * 默认币种
	 */
	private String corpccy;

	/**
	 * 默认组合
	 */
	private String corpport;

	/**
	 * 默认交易员
	 */
	private String corptrad;

	/**
	 * 撤销交易码
	 */
	private String revdealno;

	/**
	 * 可选交易标志
	 */
	private String optionind;

	/**
	 * 保证金金额
	 */
	private BigDecimal marginamt;

	/**
	 * 保证金币种
	 */
	private String marginccy;

	/**
	 * 默认交易金额
	 */
	private BigDecimal corpspreadamt;

	/**
	 * 即期币种
	 */
	private String spotccy;

	/**
	 * 即期标志
	 */
	private String spotind;

	/**
	 * 过期标志
	 */
	private String tenorexceedind;

	/**
	 * 货币1结算状态
	 */
	private String ccysettstatus;

	/**
	 * 货币2结算状态
	 */
	private String ctrsettstatus;

	/**
	 * 客户号
	 */
	private String custrefno;

	/**
	 * 货币1失败日期
	 */
	private Date ccyfaildate;

	/**
	 * 货币1清算日期
	 */
	private Date ccycleardate;

	/**
	 * 货币1失败操作员
	 */
	private String ccyfailoper;

	/**
	 * 货币1清算操作员
	 */
	private String ccyclearoper;

	/**
	 * 货币2交易失败日期
	 */
	private Date ctrfaildate;

	/**
	 * 货币2清算日期
	 */
	private Date ctrcleardate;

	/**
	 * 货币2交易失败操作员
	 */
	private String ctrfailoper;

	/**
	 * 货币2清算操作员
	 */
	private String ctrclearoper;

	/**
	 * 保证金金额
	 */
	private BigDecimal marginfeeamt;

	/**
	 * 保证金币种
	 */
	private String marginfeeccy;

	/**
	 * 保证金结算方式
	 */
	private String marginfeesettmeans;

	/**
	 * 保证金结算帐户
	 */
	private String marginfeesettacct;

	/**
	 * 保证金折扣
	 */
	private BigDecimal marginfeerebamt;

	/**
	 * 保证金折扣币种
	 */
	private String marginfeerebccy;

	/**
	 * 保证金折扣结算方式
	 */
	private String marginfeerebsettmeans;

	/**
	 * 保证金折扣结算账户
	 */
	private String marginfeerebsettacct;

	/**
	 * 电话确认操作员
	 */
	private String phoneconfoper;

	/**
	 * 电话确认时间
	 */
	private String phoneconftime;

	/**
	 * Swift匹配标志
	 */
	private String swiftmatchind;

	/**
	 * 标准付款指令标志
	 */
	private String siindpay;

	/**
	 * 佣金币种
	 */
	private String commccy;

	/**
	 * 超过利率限额标志
	 */
	private String rexratetolind;

	/**
	 * 超额交易码
	 */
	private String dealnoexcess;

	/**
	 * 固定利率标志
	 */
	private String fixrateind;

	/**
	 * 固定利率代码
	 */
	private String fixratecode;

	/**
	 * 撤销交易操作员
	 */
	private String revoper;

	/**
	 * 交易对手交易代码
	 */
	private String ctrpartydealno;

	/**
	 * 交易对手电话
	 */
	private String ctrpartyphoneno;

	/**
	 * 交易对手电话操作员
	 */
	private String ctrpartyphoneoper;

	/**
	 * 大宗交易标志
	 */
	private String blockind;

	/**
	 * 已确认标志
	 */
	private String confcreatedind;

	/**
	 * 即期日期
	 */
	private Date spotdate;

	/**
	 * 分解币种
	 */
	private String disaggccy;

	/**
	 * 即期货币2交易分解交易员
	 */
	private String disaggspottrad;

	/**
	 * 即期货币2交易分解投资组合
	 */
	private String disaggspotport;

	/**
	 * 即期货币2交易分解成本中心
	 */
	private String disaggspotcost;

	/**
	 * 远期货币2交易分解交易员
	 */
	private String disaggfwdtrad;

	/**
	 * 远期货币2交易分解投资组合
	 */
	private String disaggfwdport;

	/**
	 * 远期货币2交易分解成本中心
	 */
	private String disaggfwdcost;

	/**
	 * 远期升水／贴水
	 */
	private BigDecimal fwdpremamt;

	/**
	 * 更改日期
	 */
	private Date amenddate;

	private static final long serialVersionUID = 1L;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br == null ? null : br.trim();
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno == null ? null : dealno.trim();
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq == null ? null : seq.trim();
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad == null ? null : trad.trim();
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public String getCust() {
		return cust;
	}

	public void setCust(String cust) {
		this.cust = cust == null ? null : cust.trim();
	}

	public String getBrok() {
		return brok;
	}

	public void setBrok(String brok) {
		this.brok = brok == null ? null : brok.trim();
	}

	public String getBrokccy() {
		return brokccy;
	}

	public void setBrokccy(String brokccy) {
		this.brokccy = brokccy == null ? null : brokccy.trim();
	}

	public BigDecimal getBrokamt() {
		return brokamt;
	}

	public void setBrokamt(BigDecimal brokamt) {
		this.brokamt = brokamt;
	}

	public String getPhonci() {
		return phonci;
	}

	public void setPhonci(String phonci) {
		this.phonci = phonci == null ? null : phonci.trim();
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port == null ? null : port.trim();
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost == null ? null : cost.trim();
	}

	public Date getOdate() {
		return odate;
	}

	public void setOdate(Date odate) {
		this.odate = odate;
	}

	public Date getDealdate() {
		return dealdate;
	}

	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}

	public String getDealtime() {
		return dealtime;
	}

	public void setDealtime(String dealtime) {
		this.dealtime = dealtime == null ? null : dealtime.trim();
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper == null ? null : ioper.trim();
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper == null ? null : voper.trim();
	}

	public Date getBrokcdate() {
		return brokcdate;
	}

	public void setBrokcdate(Date brokcdate) {
		this.brokcdate = brokcdate;
	}

	public Date getCustcdate() {
		return custcdate;
	}

	public void setCustcdate(Date custcdate) {
		this.custcdate = custcdate;
	}

	public Date getPhonecdate() {
		return phonecdate;
	}

	public void setPhonecdate(Date phonecdate) {
		this.phonecdate = phonecdate;
	}

	public String getDealtext() {
		return dealtext;
	}

	public void setDealtext(String dealtext) {
		this.dealtext = dealtext == null ? null : dealtext.trim();
	}

	public String getPhonetext() {
		return phonetext;
	}

	public void setPhonetext(String phonetext) {
		this.phonetext = phonetext == null ? null : phonetext.trim();
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps == null ? null : ps.trim();
	}

	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode == null ? null : prodcode.trim();
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind == null ? null : verind.trim();
	}

	public String getRevtext() {
		return revtext;
	}

	public void setRevtext(String revtext) {
		this.revtext = revtext == null ? null : revtext.trim();
	}

	public String getSwapdeal() {
		return swapdeal;
	}

	public void setSwapdeal(String swapdeal) {
		this.swapdeal = swapdeal == null ? null : swapdeal.trim();
	}

	public String getFarnearind() {
		return farnearind;
	}

	public void setFarnearind(String farnearind) {
		this.farnearind = farnearind == null ? null : farnearind.trim();
	}

	public String getPayauthind() {
		return payauthind;
	}

	public void setPayauthind(String payauthind) {
		this.payauthind = payauthind == null ? null : payauthind.trim();
	}

	public Date getSwapvdate() {
		return swapvdate;
	}

	public void setSwapvdate(Date swapvdate) {
		this.swapvdate = swapvdate;
	}

	public String getRevreason() {
		return revreason;
	}

	public void setRevreason(String revreason) {
		this.revreason = revreason == null ? null : revreason.trim();
	}

	public Date getInputdate() {
		return inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime == null ? null : inputtime.trim();
	}

	public String getDealsrce() {
		return dealsrce;
	}

	public void setDealsrce(String dealsrce) {
		this.dealsrce = dealsrce == null ? null : dealsrce.trim();
	}

	public BigDecimal getOrigocamt() {
		return origocamt;
	}

	public void setOrigocamt(BigDecimal origocamt) {
		this.origocamt = origocamt;
	}

	public Date getTrcedate() {
		return trcedate;
	}

	public void setTrcedate(Date trcedate) {
		this.trcedate = trcedate;
	}

	public String getTracecnt() {
		return tracecnt;
	}

	public void setTracecnt(String tracecnt) {
		this.tracecnt = tracecnt == null ? null : tracecnt.trim();
	}

	public String getPayoper() {
		return payoper;
	}

	public void setPayoper(String payoper) {
		this.payoper = payoper == null ? null : payoper.trim();
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy == null ? null : ccy.trim();
	}

	public BigDecimal getCcyamt() {
		return ccyamt;
	}

	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}

	public String getCcyterms() {
		return ccyterms;
	}

	public void setCcyterms(String ccyterms) {
		this.ccyterms = ccyterms == null ? null : ccyterms.trim();
	}

	public BigDecimal getCcyrate8() {
		return ccyrate8;
	}

	public void setCcyrate8(BigDecimal ccyrate8) {
		this.ccyrate8 = ccyrate8;
	}

	public BigDecimal getCcypd8() {
		return ccypd8;
	}

	public void setCcypd8(BigDecimal ccypd8) {
		this.ccypd8 = ccypd8;
	}

	public BigDecimal getCcybamt() {
		return ccybamt;
	}

	public void setCcybamt(BigDecimal ccybamt) {
		this.ccybamt = ccybamt;
	}

	public BigDecimal getCcybrate8() {
		return ccybrate8;
	}

	public void setCcybrate8(BigDecimal ccybrate8) {
		this.ccybrate8 = ccybrate8;
	}

	public BigDecimal getCcybpd8() {
		return ccybpd8;
	}

	public void setCcybpd8(BigDecimal ccybpd8) {
		this.ccybpd8 = ccybpd8;
	}

	public String getBaseterms() {
		return baseterms;
	}

	public void setBaseterms(String baseterms) {
		this.baseterms = baseterms == null ? null : baseterms.trim();
	}

	public String getNetsi() {
		return netsi;
	}

	public void setNetsi(String netsi) {
		this.netsi = netsi == null ? null : netsi.trim();
	}

	public String getSpotfwdind() {
		return spotfwdind;
	}

	public void setSpotfwdind(String spotfwdind) {
		this.spotfwdind = spotfwdind == null ? null : spotfwdind.trim();
	}

	public String getNetdeal() {
		return netdeal;
	}

	public void setNetdeal(String netdeal) {
		this.netdeal = netdeal == null ? null : netdeal.trim();
	}

	public String getCcysmeans() {
		return ccysmeans;
	}

	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans == null ? null : ccysmeans.trim();
	}

	public String getCcysacct() {
		return ccysacct;
	}

	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct == null ? null : ccysacct.trim();
	}

	public String getCtrsmeans() {
		return ctrsmeans;
	}

	public void setCtrsmeans(String ctrsmeans) {
		this.ctrsmeans = ctrsmeans == null ? null : ctrsmeans.trim();
	}

	public String getCtrsacct() {
		return ctrsacct;
	}

	public void setCtrsacct(String ctrsacct) {
		this.ctrsacct = ctrsacct == null ? null : ctrsacct.trim();
	}

	public String getCtrccy() {
		return ctrccy;
	}

	public void setCtrccy(String ctrccy) {
		this.ctrccy = ctrccy == null ? null : ctrccy.trim();
	}

	public BigDecimal getCtramt() {
		return ctramt;
	}

	public void setCtramt(BigDecimal ctramt) {
		this.ctramt = ctramt;
	}

	public BigDecimal getCtrrate8() {
		return ctrrate8;
	}

	public void setCtrrate8(BigDecimal ctrrate8) {
		this.ctrrate8 = ctrrate8;
	}

	public BigDecimal getCtrpd8() {
		return ctrpd8;
	}

	public void setCtrpd8(BigDecimal ctrpd8) {
		this.ctrpd8 = ctrpd8;
	}

	public BigDecimal getCtrbamt() {
		return ctrbamt;
	}

	public void setCtrbamt(BigDecimal ctrbamt) {
		this.ctrbamt = ctrbamt;
	}

	public BigDecimal getCtrbrate8() {
		return ctrbrate8;
	}

	public void setCtrbrate8(BigDecimal ctrbrate8) {
		this.ctrbrate8 = ctrbrate8;
	}

	public String getNsrnoccy() {
		return nsrnoccy;
	}

	public void setNsrnoccy(String nsrnoccy) {
		this.nsrnoccy = nsrnoccy == null ? null : nsrnoccy.trim();
	}

	public String getNsrccyseq() {
		return nsrccyseq;
	}

	public void setNsrccyseq(String nsrccyseq) {
		this.nsrccyseq = nsrccyseq == null ? null : nsrccyseq.trim();
	}

	public String getNsrnoctr() {
		return nsrnoctr;
	}

	public void setNsrnoctr(String nsrnoctr) {
		this.nsrnoctr = nsrnoctr == null ? null : nsrnoctr.trim();
	}

	public String getNsrctrseq() {
		return nsrctrseq;
	}

	public void setNsrctrseq(String nsrctrseq) {
		this.nsrctrseq = nsrctrseq == null ? null : nsrctrseq.trim();
	}

	public Date getBrprcindte() {
		return brprcindte;
	}

	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}

	public String getRecoper() {
		return recoper;
	}

	public void setRecoper(String recoper) {
		this.recoper = recoper == null ? null : recoper.trim();
	}

	public Date getPayauthdte() {
		return payauthdte;
	}

	public void setPayauthdte(Date payauthdte) {
		this.payauthdte = payauthdte;
	}

	public Date getRecauthdte() {
		return recauthdte;
	}

	public void setRecauthdte(Date recauthdte) {
		this.recauthdte = recauthdte;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public Date getRevdate() {
		return revdate;
	}

	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}

	public BigDecimal getCcyrevalamt() {
		return ccyrevalamt;
	}

	public void setCcyrevalamt(BigDecimal ccyrevalamt) {
		this.ccyrevalamt = ccyrevalamt;
	}

	public BigDecimal getCtrrevalamt() {
		return ctrrevalamt;
	}

	public void setCtrrevalamt(BigDecimal ctrrevalamt) {
		this.ctrrevalamt = ctrrevalamt;
	}

	public BigDecimal getCcynpvamt() {
		return ccynpvamt;
	}

	public void setCcynpvamt(BigDecimal ccynpvamt) {
		this.ccynpvamt = ccynpvamt;
	}

	public BigDecimal getCtrnpvamt() {
		return ctrnpvamt;
	}

	public void setCtrnpvamt(BigDecimal ctrnpvamt) {
		this.ctrnpvamt = ctrnpvamt;
	}

	public String getRecauthoper() {
		return recauthoper;
	}

	public void setRecauthoper(String recauthoper) {
		this.recauthoper = recauthoper == null ? null : recauthoper.trim();
	}

	public String getPayauthoper() {
		return payauthoper;
	}

	public void setPayauthoper(String payauthoper) {
		this.payauthoper = payauthoper == null ? null : payauthoper.trim();
	}

	public String getSiind() {
		return siind;
	}

	public void setSiind(String siind) {
		this.siind = siind == null ? null : siind.trim();
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype == null ? null : prodtype.trim();
	}

	public String getSpottrad() {
		return spottrad;
	}

	public void setSpottrad(String spottrad) {
		this.spottrad = spottrad == null ? null : spottrad.trim();
	}

	public String getSpotport() {
		return spotport;
	}

	public void setSpotport(String spotport) {
		this.spotport = spotport == null ? null : spotport.trim();
	}

	public String getSpotcost() {
		return spotcost;
	}

	public void setSpotcost(String spotcost) {
		this.spotcost = spotcost == null ? null : spotcost.trim();
	}

	public BigDecimal getInternalrate8() {
		return internalrate8;
	}

	public void setInternalrate8(BigDecimal internalrate8) {
		this.internalrate8 = internalrate8;
	}

	public BigDecimal getCorpspread8() {
		return corpspread8;
	}

	public void setCorpspread8(BigDecimal corpspread8) {
		this.corpspread8 = corpspread8;
	}

	public String getCorpcost() {
		return corpcost;
	}

	public void setCorpcost(String corpcost) {
		this.corpcost = corpcost == null ? null : corpcost.trim();
	}

	public BigDecimal getBrspread8() {
		return brspread8;
	}

	public void setBrspread8(BigDecimal brspread8) {
		this.brspread8 = brspread8;
	}

	public String getBrcost() {
		return brcost;
	}

	public void setBrcost(String brcost) {
		this.brcost = brcost == null ? null : brcost.trim();
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor == null ? null : tenor.trim();
	}

	public Date getCcysettdate() {
		return ccysettdate;
	}

	public void setCcysettdate(Date ccysettdate) {
		this.ccysettdate = ccysettdate;
	}

	public Date getCtrsettdate() {
		return ctrsettdate;
	}

	public void setCtrsettdate(Date ctrsettdate) {
		this.ctrsettdate = ctrsettdate;
	}

	public String getCustenteredind() {
		return custenteredind;
	}

	public void setCustenteredind(String custenteredind) {
		this.custenteredind = custenteredind == null ? null : custenteredind.trim();
	}

	public String getLinkdealno() {
		return linkdealno;
	}

	public void setLinkdealno(String linkdealno) {
		this.linkdealno = linkdealno == null ? null : linkdealno.trim();
	}

	public String getLinkproduct() {
		return linkproduct;
	}

	public void setLinkproduct(String linkproduct) {
		this.linkproduct = linkproduct == null ? null : linkproduct.trim();
	}

	public String getLinkprodtype() {
		return linkprodtype;
	}

	public void setLinkprodtype(String linkprodtype) {
		this.linkprodtype = linkprodtype == null ? null : linkprodtype.trim();
	}

	public String getFincntr1() {
		return fincntr1;
	}

	public void setFincntr1(String fincntr1) {
		this.fincntr1 = fincntr1 == null ? null : fincntr1.trim();
	}

	public String getFincntr2() {
		return fincntr2;
	}

	public void setFincntr2(String fincntr2) {
		this.fincntr2 = fincntr2 == null ? null : fincntr2.trim();
	}

	public BigDecimal getCommamt() {
		return commamt;
	}

	public void setCommamt(BigDecimal commamt) {
		this.commamt = commamt;
	}

	public String getExcoveralllim() {
		return excoveralllim;
	}

	public void setExcoveralllim(String excoveralllim) {
		this.excoveralllim = excoveralllim == null ? null : excoveralllim.trim();
	}

	public String getExccustlim() {
		return exccustlim;
	}

	public void setExccustlim(String exccustlim) {
		this.exccustlim = exccustlim == null ? null : exccustlim.trim();
	}

	public String getCorpccy() {
		return corpccy;
	}

	public void setCorpccy(String corpccy) {
		this.corpccy = corpccy == null ? null : corpccy.trim();
	}

	public String getCorpport() {
		return corpport;
	}

	public void setCorpport(String corpport) {
		this.corpport = corpport == null ? null : corpport.trim();
	}

	public String getCorptrad() {
		return corptrad;
	}

	public void setCorptrad(String corptrad) {
		this.corptrad = corptrad == null ? null : corptrad.trim();
	}

	public String getRevdealno() {
		return revdealno;
	}

	public void setRevdealno(String revdealno) {
		this.revdealno = revdealno == null ? null : revdealno.trim();
	}

	public String getOptionind() {
		return optionind;
	}

	public void setOptionind(String optionind) {
		this.optionind = optionind == null ? null : optionind.trim();
	}

	public BigDecimal getMarginamt() {
		return marginamt;
	}

	public void setMarginamt(BigDecimal marginamt) {
		this.marginamt = marginamt;
	}

	public String getMarginccy() {
		return marginccy;
	}

	public void setMarginccy(String marginccy) {
		this.marginccy = marginccy == null ? null : marginccy.trim();
	}

	public BigDecimal getCorpspreadamt() {
		return corpspreadamt;
	}

	public void setCorpspreadamt(BigDecimal corpspreadamt) {
		this.corpspreadamt = corpspreadamt;
	}

	public String getSpotccy() {
		return spotccy;
	}

	public void setSpotccy(String spotccy) {
		this.spotccy = spotccy == null ? null : spotccy.trim();
	}

	public String getSpotind() {
		return spotind;
	}

	public void setSpotind(String spotind) {
		this.spotind = spotind == null ? null : spotind.trim();
	}

	public String getTenorexceedind() {
		return tenorexceedind;
	}

	public void setTenorexceedind(String tenorexceedind) {
		this.tenorexceedind = tenorexceedind == null ? null : tenorexceedind.trim();
	}

	public String getCcysettstatus() {
		return ccysettstatus;
	}

	public void setCcysettstatus(String ccysettstatus) {
		this.ccysettstatus = ccysettstatus == null ? null : ccysettstatus.trim();
	}

	public String getCtrsettstatus() {
		return ctrsettstatus;
	}

	public void setCtrsettstatus(String ctrsettstatus) {
		this.ctrsettstatus = ctrsettstatus == null ? null : ctrsettstatus.trim();
	}

	public String getCustrefno() {
		return custrefno;
	}

	public void setCustrefno(String custrefno) {
		this.custrefno = custrefno == null ? null : custrefno.trim();
	}

	public Date getCcyfaildate() {
		return ccyfaildate;
	}

	public void setCcyfaildate(Date ccyfaildate) {
		this.ccyfaildate = ccyfaildate;
	}

	public Date getCcycleardate() {
		return ccycleardate;
	}

	public void setCcycleardate(Date ccycleardate) {
		this.ccycleardate = ccycleardate;
	}

	public String getCcyfailoper() {
		return ccyfailoper;
	}

	public void setCcyfailoper(String ccyfailoper) {
		this.ccyfailoper = ccyfailoper == null ? null : ccyfailoper.trim();
	}

	public String getCcyclearoper() {
		return ccyclearoper;
	}

	public void setCcyclearoper(String ccyclearoper) {
		this.ccyclearoper = ccyclearoper == null ? null : ccyclearoper.trim();
	}

	public Date getCtrfaildate() {
		return ctrfaildate;
	}

	public void setCtrfaildate(Date ctrfaildate) {
		this.ctrfaildate = ctrfaildate;
	}

	public Date getCtrcleardate() {
		return ctrcleardate;
	}

	public void setCtrcleardate(Date ctrcleardate) {
		this.ctrcleardate = ctrcleardate;
	}

	public String getCtrfailoper() {
		return ctrfailoper;
	}

	public void setCtrfailoper(String ctrfailoper) {
		this.ctrfailoper = ctrfailoper == null ? null : ctrfailoper.trim();
	}

	public String getCtrclearoper() {
		return ctrclearoper;
	}

	public void setCtrclearoper(String ctrclearoper) {
		this.ctrclearoper = ctrclearoper == null ? null : ctrclearoper.trim();
	}

	public BigDecimal getMarginfeeamt() {
		return marginfeeamt;
	}

	public void setMarginfeeamt(BigDecimal marginfeeamt) {
		this.marginfeeamt = marginfeeamt;
	}

	public String getMarginfeeccy() {
		return marginfeeccy;
	}

	public void setMarginfeeccy(String marginfeeccy) {
		this.marginfeeccy = marginfeeccy == null ? null : marginfeeccy.trim();
	}

	public String getMarginfeesettmeans() {
		return marginfeesettmeans;
	}

	public void setMarginfeesettmeans(String marginfeesettmeans) {
		this.marginfeesettmeans = marginfeesettmeans == null ? null : marginfeesettmeans.trim();
	}

	public String getMarginfeesettacct() {
		return marginfeesettacct;
	}

	public void setMarginfeesettacct(String marginfeesettacct) {
		this.marginfeesettacct = marginfeesettacct == null ? null : marginfeesettacct.trim();
	}

	public BigDecimal getMarginfeerebamt() {
		return marginfeerebamt;
	}

	public void setMarginfeerebamt(BigDecimal marginfeerebamt) {
		this.marginfeerebamt = marginfeerebamt;
	}

	public String getMarginfeerebccy() {
		return marginfeerebccy;
	}

	public void setMarginfeerebccy(String marginfeerebccy) {
		this.marginfeerebccy = marginfeerebccy == null ? null : marginfeerebccy.trim();
	}

	public String getMarginfeerebsettmeans() {
		return marginfeerebsettmeans;
	}

	public void setMarginfeerebsettmeans(String marginfeerebsettmeans) {
		this.marginfeerebsettmeans = marginfeerebsettmeans == null ? null : marginfeerebsettmeans.trim();
	}

	public String getMarginfeerebsettacct() {
		return marginfeerebsettacct;
	}

	public void setMarginfeerebsettacct(String marginfeerebsettacct) {
		this.marginfeerebsettacct = marginfeerebsettacct == null ? null : marginfeerebsettacct.trim();
	}

	public String getPhoneconfoper() {
		return phoneconfoper;
	}

	public void setPhoneconfoper(String phoneconfoper) {
		this.phoneconfoper = phoneconfoper == null ? null : phoneconfoper.trim();
	}

	public String getPhoneconftime() {
		return phoneconftime;
	}

	public void setPhoneconftime(String phoneconftime) {
		this.phoneconftime = phoneconftime == null ? null : phoneconftime.trim();
	}

	public String getSwiftmatchind() {
		return swiftmatchind;
	}

	public void setSwiftmatchind(String swiftmatchind) {
		this.swiftmatchind = swiftmatchind == null ? null : swiftmatchind.trim();
	}

	public String getSiindpay() {
		return siindpay;
	}

	public void setSiindpay(String siindpay) {
		this.siindpay = siindpay == null ? null : siindpay.trim();
	}

	public String getCommccy() {
		return commccy;
	}

	public void setCommccy(String commccy) {
		this.commccy = commccy == null ? null : commccy.trim();
	}

	public String getRexratetolind() {
		return rexratetolind;
	}

	public void setRexratetolind(String rexratetolind) {
		this.rexratetolind = rexratetolind == null ? null : rexratetolind.trim();
	}

	public String getDealnoexcess() {
		return dealnoexcess;
	}

	public void setDealnoexcess(String dealnoexcess) {
		this.dealnoexcess = dealnoexcess == null ? null : dealnoexcess.trim();
	}

	public String getFixrateind() {
		return fixrateind;
	}

	public void setFixrateind(String fixrateind) {
		this.fixrateind = fixrateind == null ? null : fixrateind.trim();
	}

	public String getFixratecode() {
		return fixratecode;
	}

	public void setFixratecode(String fixratecode) {
		this.fixratecode = fixratecode == null ? null : fixratecode.trim();
	}

	public String getRevoper() {
		return revoper;
	}

	public void setRevoper(String revoper) {
		this.revoper = revoper == null ? null : revoper.trim();
	}

	public String getCtrpartydealno() {
		return ctrpartydealno;
	}

	public void setCtrpartydealno(String ctrpartydealno) {
		this.ctrpartydealno = ctrpartydealno == null ? null : ctrpartydealno.trim();
	}

	public String getCtrpartyphoneno() {
		return ctrpartyphoneno;
	}

	public void setCtrpartyphoneno(String ctrpartyphoneno) {
		this.ctrpartyphoneno = ctrpartyphoneno == null ? null : ctrpartyphoneno.trim();
	}

	public String getCtrpartyphoneoper() {
		return ctrpartyphoneoper;
	}

	public void setCtrpartyphoneoper(String ctrpartyphoneoper) {
		this.ctrpartyphoneoper = ctrpartyphoneoper == null ? null : ctrpartyphoneoper.trim();
	}

	public String getBlockind() {
		return blockind;
	}

	public void setBlockind(String blockind) {
		this.blockind = blockind == null ? null : blockind.trim();
	}

	public String getConfcreatedind() {
		return confcreatedind;
	}

	public void setConfcreatedind(String confcreatedind) {
		this.confcreatedind = confcreatedind == null ? null : confcreatedind.trim();
	}

	public Date getSpotdate() {
		return spotdate;
	}

	public void setSpotdate(Date spotdate) {
		this.spotdate = spotdate;
	}

	public String getDisaggccy() {
		return disaggccy;
	}

	public void setDisaggccy(String disaggccy) {
		this.disaggccy = disaggccy == null ? null : disaggccy.trim();
	}

	public String getDisaggspottrad() {
		return disaggspottrad;
	}

	public void setDisaggspottrad(String disaggspottrad) {
		this.disaggspottrad = disaggspottrad == null ? null : disaggspottrad.trim();
	}

	public String getDisaggspotport() {
		return disaggspotport;
	}

	public void setDisaggspotport(String disaggspotport) {
		this.disaggspotport = disaggspotport == null ? null : disaggspotport.trim();
	}

	public String getDisaggspotcost() {
		return disaggspotcost;
	}

	public void setDisaggspotcost(String disaggspotcost) {
		this.disaggspotcost = disaggspotcost == null ? null : disaggspotcost.trim();
	}

	public String getDisaggfwdtrad() {
		return disaggfwdtrad;
	}

	public void setDisaggfwdtrad(String disaggfwdtrad) {
		this.disaggfwdtrad = disaggfwdtrad == null ? null : disaggfwdtrad.trim();
	}

	public String getDisaggfwdport() {
		return disaggfwdport;
	}

	public void setDisaggfwdport(String disaggfwdport) {
		this.disaggfwdport = disaggfwdport == null ? null : disaggfwdport.trim();
	}

	public String getDisaggfwdcost() {
		return disaggfwdcost;
	}

	public void setDisaggfwdcost(String disaggfwdcost) {
		this.disaggfwdcost = disaggfwdcost == null ? null : disaggfwdcost.trim();
	}

	public BigDecimal getFwdpremamt() {
		return fwdpremamt;
	}

	public void setFwdpremamt(BigDecimal fwdpremamt) {
		this.fwdpremamt = fwdpremamt;
	}

	public Date getAmenddate() {
		return amenddate;
	}

	public void setAmenddate(Date amenddate) {
		this.amenddate = amenddate;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", br=").append(br);
		sb.append(", dealno=").append(dealno);
		sb.append(", seq=").append(seq);
		sb.append(", trad=").append(trad);
		sb.append(", vdate=").append(vdate);
		sb.append(", cust=").append(cust);
		sb.append(", brok=").append(brok);
		sb.append(", brokccy=").append(brokccy);
		sb.append(", brokamt=").append(brokamt);
		sb.append(", phonci=").append(phonci);
		sb.append(", port=").append(port);
		sb.append(", cost=").append(cost);
		sb.append(", odate=").append(odate);
		sb.append(", dealdate=").append(dealdate);
		sb.append(", dealtime=").append(dealtime);
		sb.append(", ioper=").append(ioper);
		sb.append(", voper=").append(voper);
		sb.append(", brokcdate=").append(brokcdate);
		sb.append(", custcdate=").append(custcdate);
		sb.append(", phonecdate=").append(phonecdate);
		sb.append(", dealtext=").append(dealtext);
		sb.append(", phonetext=").append(phonetext);
		sb.append(", ps=").append(ps);
		sb.append(", prodcode=").append(prodcode);
		sb.append(", verind=").append(verind);
		sb.append(", revtext=").append(revtext);
		sb.append(", swapdeal=").append(swapdeal);
		sb.append(", farnearind=").append(farnearind);
		sb.append(", payauthind=").append(payauthind);
		sb.append(", swapvdate=").append(swapvdate);
		sb.append(", revreason=").append(revreason);
		sb.append(", inputdate=").append(inputdate);
		sb.append(", inputtime=").append(inputtime);
		sb.append(", dealsrce=").append(dealsrce);
		sb.append(", origocamt=").append(origocamt);
		sb.append(", trcedate=").append(trcedate);
		sb.append(", tracecnt=").append(tracecnt);
		sb.append(", payoper=").append(payoper);
		sb.append(", lstmntdate=").append(lstmntdate);
		sb.append(", ccy=").append(ccy);
		sb.append(", ccyamt=").append(ccyamt);
		sb.append(", ccyterms=").append(ccyterms);
		sb.append(", ccyrate8=").append(ccyrate8);
		sb.append(", ccypd8=").append(ccypd8);
		sb.append(", ccybamt=").append(ccybamt);
		sb.append(", ccybrate8=").append(ccybrate8);
		sb.append(", ccybpd8=").append(ccybpd8);
		sb.append(", baseterms=").append(baseterms);
		sb.append(", netsi=").append(netsi);
		sb.append(", spotfwdind=").append(spotfwdind);
		sb.append(", netdeal=").append(netdeal);
		sb.append(", ccysmeans=").append(ccysmeans);
		sb.append(", ccysacct=").append(ccysacct);
		sb.append(", ctrsmeans=").append(ctrsmeans);
		sb.append(", ctrsacct=").append(ctrsacct);
		sb.append(", ctrccy=").append(ctrccy);
		sb.append(", ctramt=").append(ctramt);
		sb.append(", ctrrate8=").append(ctrrate8);
		sb.append(", ctrpd8=").append(ctrpd8);
		sb.append(", ctrbamt=").append(ctrbamt);
		sb.append(", ctrbrate8=").append(ctrbrate8);
		sb.append(", nsrnoccy=").append(nsrnoccy);
		sb.append(", nsrccyseq=").append(nsrccyseq);
		sb.append(", nsrnoctr=").append(nsrnoctr);
		sb.append(", nsrctrseq=").append(nsrctrseq);
		sb.append(", brprcindte=").append(brprcindte);
		sb.append(", recoper=").append(recoper);
		sb.append(", payauthdte=").append(payauthdte);
		sb.append(", recauthdte=").append(recauthdte);
		sb.append(", verdate=").append(verdate);
		sb.append(", revdate=").append(revdate);
		sb.append(", ccyrevalamt=").append(ccyrevalamt);
		sb.append(", ctrrevalamt=").append(ctrrevalamt);
		sb.append(", ccynpvamt=").append(ccynpvamt);
		sb.append(", ctrnpvamt=").append(ctrnpvamt);
		sb.append(", recauthoper=").append(recauthoper);
		sb.append(", payauthoper=").append(payauthoper);
		sb.append(", siind=").append(siind);
		sb.append(", prodtype=").append(prodtype);
		sb.append(", spottrad=").append(spottrad);
		sb.append(", spotport=").append(spotport);
		sb.append(", spotcost=").append(spotcost);
		sb.append(", internalrate8=").append(internalrate8);
		sb.append(", corpspread8=").append(corpspread8);
		sb.append(", corpcost=").append(corpcost);
		sb.append(", brspread8=").append(brspread8);
		sb.append(", brcost=").append(brcost);
		sb.append(", tenor=").append(tenor);
		sb.append(", ccysettdate=").append(ccysettdate);
		sb.append(", ctrsettdate=").append(ctrsettdate);
		sb.append(", custenteredind=").append(custenteredind);
		sb.append(", linkdealno=").append(linkdealno);
		sb.append(", linkproduct=").append(linkproduct);
		sb.append(", linkprodtype=").append(linkprodtype);
		sb.append(", fincntr1=").append(fincntr1);
		sb.append(", fincntr2=").append(fincntr2);
		sb.append(", commamt=").append(commamt);
		sb.append(", excoveralllim=").append(excoveralllim);
		sb.append(", exccustlim=").append(exccustlim);
		sb.append(", corpccy=").append(corpccy);
		sb.append(", corpport=").append(corpport);
		sb.append(", corptrad=").append(corptrad);
		sb.append(", revdealno=").append(revdealno);
		sb.append(", optionind=").append(optionind);
		sb.append(", marginamt=").append(marginamt);
		sb.append(", marginccy=").append(marginccy);
		sb.append(", corpspreadamt=").append(corpspreadamt);
		sb.append(", spotccy=").append(spotccy);
		sb.append(", spotind=").append(spotind);
		sb.append(", tenorexceedind=").append(tenorexceedind);
		sb.append(", ccysettstatus=").append(ccysettstatus);
		sb.append(", ctrsettstatus=").append(ctrsettstatus);
		sb.append(", custrefno=").append(custrefno);
		sb.append(", ccyfaildate=").append(ccyfaildate);
		sb.append(", ccycleardate=").append(ccycleardate);
		sb.append(", ccyfailoper=").append(ccyfailoper);
		sb.append(", ccyclearoper=").append(ccyclearoper);
		sb.append(", ctrfaildate=").append(ctrfaildate);
		sb.append(", ctrcleardate=").append(ctrcleardate);
		sb.append(", ctrfailoper=").append(ctrfailoper);
		sb.append(", ctrclearoper=").append(ctrclearoper);
		sb.append(", marginfeeamt=").append(marginfeeamt);
		sb.append(", marginfeeccy=").append(marginfeeccy);
		sb.append(", marginfeesettmeans=").append(marginfeesettmeans);
		sb.append(", marginfeesettacct=").append(marginfeesettacct);
		sb.append(", marginfeerebamt=").append(marginfeerebamt);
		sb.append(", marginfeerebccy=").append(marginfeerebccy);
		sb.append(", marginfeerebsettmeans=").append(marginfeerebsettmeans);
		sb.append(", marginfeerebsettacct=").append(marginfeerebsettacct);
		sb.append(", phoneconfoper=").append(phoneconfoper);
		sb.append(", phoneconftime=").append(phoneconftime);
		sb.append(", swiftmatchind=").append(swiftmatchind);
		sb.append(", siindpay=").append(siindpay);
		sb.append(", commccy=").append(commccy);
		sb.append(", rexratetolind=").append(rexratetolind);
		sb.append(", dealnoexcess=").append(dealnoexcess);
		sb.append(", fixrateind=").append(fixrateind);
		sb.append(", fixratecode=").append(fixratecode);
		sb.append(", revoper=").append(revoper);
		sb.append(", ctrpartydealno=").append(ctrpartydealno);
		sb.append(", ctrpartyphoneno=").append(ctrpartyphoneno);
		sb.append(", ctrpartyphoneoper=").append(ctrpartyphoneoper);
		sb.append(", blockind=").append(blockind);
		sb.append(", confcreatedind=").append(confcreatedind);
		sb.append(", spotdate=").append(spotdate);
		sb.append(", disaggccy=").append(disaggccy);
		sb.append(", disaggspottrad=").append(disaggspottrad);
		sb.append(", disaggspotport=").append(disaggspotport);
		sb.append(", disaggspotcost=").append(disaggspotcost);
		sb.append(", disaggfwdtrad=").append(disaggfwdtrad);
		sb.append(", disaggfwdport=").append(disaggfwdport);
		sb.append(", disaggfwdcost=").append(disaggfwdcost);
		sb.append(", fwdpremamt=").append(fwdpremamt);
		sb.append(", amenddate=").append(amenddate);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		Fxdh other = (Fxdh) that;
		return (this.getBr() == null ? other.getBr() == null : this.getBr().equals(other.getBr()))
				&& (this.getDealno() == null ? other.getDealno() == null : this.getDealno().equals(other.getDealno()))
				&& (this.getSeq() == null ? other.getSeq() == null : this.getSeq().equals(other.getSeq()))
				&& (this.getTrad() == null ? other.getTrad() == null : this.getTrad().equals(other.getTrad()))
				&& (this.getVdate() == null ? other.getVdate() == null : this.getVdate().equals(other.getVdate()))
				&& (this.getCust() == null ? other.getCust() == null : this.getCust().equals(other.getCust()))
				&& (this.getBrok() == null ? other.getBrok() == null : this.getBrok().equals(other.getBrok()))
				&& (this.getBrokccy() == null ? other.getBrokccy() == null
						: this.getBrokccy().equals(other.getBrokccy()))
				&& (this.getBrokamt() == null ? other.getBrokamt() == null
						: this.getBrokamt().equals(other.getBrokamt()))
				&& (this.getPhonci() == null ? other.getPhonci() == null : this.getPhonci().equals(other.getPhonci()))
				&& (this.getPort() == null ? other.getPort() == null : this.getPort().equals(other.getPort()))
				&& (this.getCost() == null ? other.getCost() == null : this.getCost().equals(other.getCost()))
				&& (this.getOdate() == null ? other.getOdate() == null : this.getOdate().equals(other.getOdate()))
				&& (this.getDealdate() == null ? other.getDealdate() == null
						: this.getDealdate().equals(other.getDealdate()))
				&& (this.getDealtime() == null ? other.getDealtime() == null
						: this.getDealtime().equals(other.getDealtime()))
				&& (this.getIoper() == null ? other.getIoper() == null : this.getIoper().equals(other.getIoper()))
				&& (this.getVoper() == null ? other.getVoper() == null : this.getVoper().equals(other.getVoper()))
				&& (this.getBrokcdate() == null ? other.getBrokcdate() == null
						: this.getBrokcdate().equals(other.getBrokcdate()))
				&& (this.getCustcdate() == null ? other.getCustcdate() == null
						: this.getCustcdate().equals(other.getCustcdate()))
				&& (this.getPhonecdate() == null ? other.getPhonecdate() == null
						: this.getPhonecdate().equals(other.getPhonecdate()))
				&& (this.getDealtext() == null ? other.getDealtext() == null
						: this.getDealtext().equals(other.getDealtext()))
				&& (this.getPhonetext() == null ? other.getPhonetext() == null
						: this.getPhonetext().equals(other.getPhonetext()))
				&& (this.getPs() == null ? other.getPs() == null : this.getPs().equals(other.getPs()))
				&& (this.getProdcode() == null ? other.getProdcode() == null
						: this.getProdcode().equals(other.getProdcode()))
				&& (this.getVerind() == null ? other.getVerind() == null : this.getVerind().equals(other.getVerind()))
				&& (this.getRevtext() == null ? other.getRevtext() == null
						: this.getRevtext().equals(other.getRevtext()))
				&& (this.getSwapdeal() == null ? other.getSwapdeal() == null
						: this.getSwapdeal().equals(other.getSwapdeal()))
				&& (this.getFarnearind() == null ? other.getFarnearind() == null
						: this.getFarnearind().equals(other.getFarnearind()))
				&& (this.getPayauthind() == null ? other.getPayauthind() == null
						: this.getPayauthind().equals(other.getPayauthind()))
				&& (this.getSwapvdate() == null ? other.getSwapvdate() == null
						: this.getSwapvdate().equals(other.getSwapvdate()))
				&& (this.getRevreason() == null ? other.getRevreason() == null
						: this.getRevreason().equals(other.getRevreason()))
				&& (this.getInputdate() == null ? other.getInputdate() == null
						: this.getInputdate().equals(other.getInputdate()))
				&& (this.getInputtime() == null ? other.getInputtime() == null
						: this.getInputtime().equals(other.getInputtime()))
				&& (this.getDealsrce() == null ? other.getDealsrce() == null
						: this.getDealsrce().equals(other.getDealsrce()))
				&& (this.getOrigocamt() == null ? other.getOrigocamt() == null
						: this.getOrigocamt().equals(other.getOrigocamt()))
				&& (this.getTrcedate() == null ? other.getTrcedate() == null
						: this.getTrcedate().equals(other.getTrcedate()))
				&& (this.getTracecnt() == null ? other.getTracecnt() == null
						: this.getTracecnt().equals(other.getTracecnt()))
				&& (this.getPayoper() == null ? other.getPayoper() == null
						: this.getPayoper().equals(other.getPayoper()))
				&& (this.getLstmntdate() == null ? other.getLstmntdate() == null
						: this.getLstmntdate().equals(other.getLstmntdate()))
				&& (this.getCcy() == null ? other.getCcy() == null : this.getCcy().equals(other.getCcy()))
				&& (this.getCcyamt() == null ? other.getCcyamt() == null : this.getCcyamt().equals(other.getCcyamt()))
				&& (this.getCcyterms() == null ? other.getCcyterms() == null
						: this.getCcyterms().equals(other.getCcyterms()))
				&& (this.getCcyrate8() == null ? other.getCcyrate8() == null
						: this.getCcyrate8().equals(other.getCcyrate8()))
				&& (this.getCcypd8() == null ? other.getCcypd8() == null : this.getCcypd8().equals(other.getCcypd8()))
				&& (this.getCcybamt() == null ? other.getCcybamt() == null
						: this.getCcybamt().equals(other.getCcybamt()))
				&& (this.getCcybrate8() == null ? other.getCcybrate8() == null
						: this.getCcybrate8().equals(other.getCcybrate8()))
				&& (this.getCcybpd8() == null ? other.getCcybpd8() == null
						: this.getCcybpd8().equals(other.getCcybpd8()))
				&& (this.getBaseterms() == null ? other.getBaseterms() == null
						: this.getBaseterms().equals(other.getBaseterms()))
				&& (this.getNetsi() == null ? other.getNetsi() == null : this.getNetsi().equals(other.getNetsi()))
				&& (this.getSpotfwdind() == null ? other.getSpotfwdind() == null
						: this.getSpotfwdind().equals(other.getSpotfwdind()))
				&& (this.getNetdeal() == null ? other.getNetdeal() == null
						: this.getNetdeal().equals(other.getNetdeal()))
				&& (this.getCcysmeans() == null ? other.getCcysmeans() == null
						: this.getCcysmeans().equals(other.getCcysmeans()))
				&& (this.getCcysacct() == null ? other.getCcysacct() == null
						: this.getCcysacct().equals(other.getCcysacct()))
				&& (this.getCtrsmeans() == null ? other.getCtrsmeans() == null
						: this.getCtrsmeans().equals(other.getCtrsmeans()))
				&& (this.getCtrsacct() == null ? other.getCtrsacct() == null
						: this.getCtrsacct().equals(other.getCtrsacct()))
				&& (this.getCtrccy() == null ? other.getCtrccy() == null : this.getCtrccy().equals(other.getCtrccy()))
				&& (this.getCtramt() == null ? other.getCtramt() == null : this.getCtramt().equals(other.getCtramt()))
				&& (this.getCtrrate8() == null ? other.getCtrrate8() == null
						: this.getCtrrate8().equals(other.getCtrrate8()))
				&& (this.getCtrpd8() == null ? other.getCtrpd8() == null : this.getCtrpd8().equals(other.getCtrpd8()))
				&& (this.getCtrbamt() == null ? other.getCtrbamt() == null
						: this.getCtrbamt().equals(other.getCtrbamt()))
				&& (this.getCtrbrate8() == null ? other.getCtrbrate8() == null
						: this.getCtrbrate8().equals(other.getCtrbrate8()))
				&& (this.getNsrnoccy() == null ? other.getNsrnoccy() == null
						: this.getNsrnoccy().equals(other.getNsrnoccy()))
				&& (this.getNsrccyseq() == null ? other.getNsrccyseq() == null
						: this.getNsrccyseq().equals(other.getNsrccyseq()))
				&& (this.getNsrnoctr() == null ? other.getNsrnoctr() == null
						: this.getNsrnoctr().equals(other.getNsrnoctr()))
				&& (this.getNsrctrseq() == null ? other.getNsrctrseq() == null
						: this.getNsrctrseq().equals(other.getNsrctrseq()))
				&& (this.getBrprcindte() == null ? other.getBrprcindte() == null
						: this.getBrprcindte().equals(other.getBrprcindte()))
				&& (this.getRecoper() == null ? other.getRecoper() == null
						: this.getRecoper().equals(other.getRecoper()))
				&& (this.getPayauthdte() == null ? other.getPayauthdte() == null
						: this.getPayauthdte().equals(other.getPayauthdte()))
				&& (this.getRecauthdte() == null ? other.getRecauthdte() == null
						: this.getRecauthdte().equals(other.getRecauthdte()))
				&& (this.getVerdate() == null ? other.getVerdate() == null
						: this.getVerdate().equals(other.getVerdate()))
				&& (this.getRevdate() == null ? other.getRevdate() == null
						: this.getRevdate().equals(other.getRevdate()))
				&& (this.getCcyrevalamt() == null ? other.getCcyrevalamt() == null
						: this.getCcyrevalamt().equals(other.getCcyrevalamt()))
				&& (this.getCtrrevalamt() == null ? other.getCtrrevalamt() == null
						: this.getCtrrevalamt().equals(other.getCtrrevalamt()))
				&& (this.getCcynpvamt() == null ? other.getCcynpvamt() == null
						: this.getCcynpvamt().equals(other.getCcynpvamt()))
				&& (this.getCtrnpvamt() == null ? other.getCtrnpvamt() == null
						: this.getCtrnpvamt().equals(other.getCtrnpvamt()))
				&& (this.getRecauthoper() == null ? other.getRecauthoper() == null
						: this.getRecauthoper().equals(other.getRecauthoper()))
				&& (this.getPayauthoper() == null ? other.getPayauthoper() == null
						: this.getPayauthoper().equals(other.getPayauthoper()))
				&& (this.getSiind() == null ? other.getSiind() == null : this.getSiind().equals(other.getSiind()))
				&& (this.getProdtype() == null ? other.getProdtype() == null
						: this.getProdtype().equals(other.getProdtype()))
				&& (this.getSpottrad() == null ? other.getSpottrad() == null
						: this.getSpottrad().equals(other.getSpottrad()))
				&& (this.getSpotport() == null ? other.getSpotport() == null
						: this.getSpotport().equals(other.getSpotport()))
				&& (this.getSpotcost() == null ? other.getSpotcost() == null
						: this.getSpotcost().equals(other.getSpotcost()))
				&& (this.getInternalrate8() == null ? other.getInternalrate8() == null
						: this.getInternalrate8().equals(other.getInternalrate8()))
				&& (this.getCorpspread8() == null ? other.getCorpspread8() == null
						: this.getCorpspread8().equals(other.getCorpspread8()))
				&& (this.getCorpcost() == null ? other.getCorpcost() == null
						: this.getCorpcost().equals(other.getCorpcost()))
				&& (this.getBrspread8() == null ? other.getBrspread8() == null
						: this.getBrspread8().equals(other.getBrspread8()))
				&& (this.getBrcost() == null ? other.getBrcost() == null : this.getBrcost().equals(other.getBrcost()))
				&& (this.getTenor() == null ? other.getTenor() == null : this.getTenor().equals(other.getTenor()))
				&& (this.getCcysettdate() == null ? other.getCcysettdate() == null
						: this.getCcysettdate().equals(other.getCcysettdate()))
				&& (this.getCtrsettdate() == null ? other.getCtrsettdate() == null
						: this.getCtrsettdate().equals(other.getCtrsettdate()))
				&& (this.getCustenteredind() == null ? other.getCustenteredind() == null
						: this.getCustenteredind().equals(other.getCustenteredind()))
				&& (this.getLinkdealno() == null ? other.getLinkdealno() == null
						: this.getLinkdealno().equals(other.getLinkdealno()))
				&& (this.getLinkproduct() == null ? other.getLinkproduct() == null
						: this.getLinkproduct().equals(other.getLinkproduct()))
				&& (this.getLinkprodtype() == null ? other.getLinkprodtype() == null
						: this.getLinkprodtype().equals(other.getLinkprodtype()))
				&& (this.getFincntr1() == null ? other.getFincntr1() == null
						: this.getFincntr1().equals(other.getFincntr1()))
				&& (this.getFincntr2() == null ? other.getFincntr2() == null
						: this.getFincntr2().equals(other.getFincntr2()))
				&& (this.getCommamt() == null ? other.getCommamt() == null
						: this.getCommamt().equals(other.getCommamt()))
				&& (this.getExcoveralllim() == null ? other.getExcoveralllim() == null
						: this.getExcoveralllim().equals(other.getExcoveralllim()))
				&& (this.getExccustlim() == null ? other.getExccustlim() == null
						: this.getExccustlim().equals(other.getExccustlim()))
				&& (this.getCorpccy() == null ? other.getCorpccy() == null
						: this.getCorpccy().equals(other.getCorpccy()))
				&& (this.getCorpport() == null ? other.getCorpport() == null
						: this.getCorpport().equals(other.getCorpport()))
				&& (this.getCorptrad() == null ? other.getCorptrad() == null
						: this.getCorptrad().equals(other.getCorptrad()))
				&& (this.getRevdealno() == null ? other.getRevdealno() == null
						: this.getRevdealno().equals(other.getRevdealno()))
				&& (this.getOptionind() == null ? other.getOptionind() == null
						: this.getOptionind().equals(other.getOptionind()))
				&& (this.getMarginamt() == null ? other.getMarginamt() == null
						: this.getMarginamt().equals(other.getMarginamt()))
				&& (this.getMarginccy() == null ? other.getMarginccy() == null
						: this.getMarginccy().equals(other.getMarginccy()))
				&& (this.getCorpspreadamt() == null ? other.getCorpspreadamt() == null
						: this.getCorpspreadamt().equals(other.getCorpspreadamt()))
				&& (this.getSpotccy() == null ? other.getSpotccy() == null
						: this.getSpotccy().equals(other.getSpotccy()))
				&& (this.getSpotind() == null ? other.getSpotind() == null
						: this.getSpotind().equals(other.getSpotind()))
				&& (this.getTenorexceedind() == null ? other.getTenorexceedind() == null
						: this.getTenorexceedind().equals(other.getTenorexceedind()))
				&& (this.getCcysettstatus() == null ? other.getCcysettstatus() == null
						: this.getCcysettstatus().equals(other.getCcysettstatus()))
				&& (this.getCtrsettstatus() == null ? other.getCtrsettstatus() == null
						: this.getCtrsettstatus().equals(other.getCtrsettstatus()))
				&& (this.getCustrefno() == null ? other.getCustrefno() == null
						: this.getCustrefno().equals(other.getCustrefno()))
				&& (this.getCcyfaildate() == null ? other.getCcyfaildate() == null
						: this.getCcyfaildate().equals(other.getCcyfaildate()))
				&& (this.getCcycleardate() == null ? other.getCcycleardate() == null
						: this.getCcycleardate().equals(other.getCcycleardate()))
				&& (this.getCcyfailoper() == null ? other.getCcyfailoper() == null
						: this.getCcyfailoper().equals(other.getCcyfailoper()))
				&& (this.getCcyclearoper() == null ? other.getCcyclearoper() == null
						: this.getCcyclearoper().equals(other.getCcyclearoper()))
				&& (this.getCtrfaildate() == null ? other.getCtrfaildate() == null
						: this.getCtrfaildate().equals(other.getCtrfaildate()))
				&& (this.getCtrcleardate() == null ? other.getCtrcleardate() == null
						: this.getCtrcleardate().equals(other.getCtrcleardate()))
				&& (this.getCtrfailoper() == null ? other.getCtrfailoper() == null
						: this.getCtrfailoper().equals(other.getCtrfailoper()))
				&& (this.getCtrclearoper() == null ? other.getCtrclearoper() == null
						: this.getCtrclearoper().equals(other.getCtrclearoper()))
				&& (this.getMarginfeeamt() == null ? other.getMarginfeeamt() == null
						: this.getMarginfeeamt().equals(other.getMarginfeeamt()))
				&& (this.getMarginfeeccy() == null ? other.getMarginfeeccy() == null
						: this.getMarginfeeccy().equals(other.getMarginfeeccy()))
				&& (this.getMarginfeesettmeans() == null ? other.getMarginfeesettmeans() == null
						: this.getMarginfeesettmeans().equals(other.getMarginfeesettmeans()))
				&& (this.getMarginfeesettacct() == null ? other.getMarginfeesettacct() == null
						: this.getMarginfeesettacct().equals(other.getMarginfeesettacct()))
				&& (this.getMarginfeerebamt() == null ? other.getMarginfeerebamt() == null
						: this.getMarginfeerebamt().equals(other.getMarginfeerebamt()))
				&& (this.getMarginfeerebccy() == null ? other.getMarginfeerebccy() == null
						: this.getMarginfeerebccy().equals(other.getMarginfeerebccy()))
				&& (this.getMarginfeerebsettmeans() == null ? other.getMarginfeerebsettmeans() == null
						: this.getMarginfeerebsettmeans().equals(other.getMarginfeerebsettmeans()))
				&& (this.getMarginfeerebsettacct() == null ? other.getMarginfeerebsettacct() == null
						: this.getMarginfeerebsettacct().equals(other.getMarginfeerebsettacct()))
				&& (this.getPhoneconfoper() == null ? other.getPhoneconfoper() == null
						: this.getPhoneconfoper().equals(other.getPhoneconfoper()))
				&& (this.getPhoneconftime() == null ? other.getPhoneconftime() == null
						: this.getPhoneconftime().equals(other.getPhoneconftime()))
				&& (this.getSwiftmatchind() == null ? other.getSwiftmatchind() == null
						: this.getSwiftmatchind().equals(other.getSwiftmatchind()))
				&& (this.getSiindpay() == null ? other.getSiindpay() == null
						: this.getSiindpay().equals(other.getSiindpay()))
				&& (this.getCommccy() == null ? other.getCommccy() == null
						: this.getCommccy().equals(other.getCommccy()))
				&& (this.getRexratetolind() == null ? other.getRexratetolind() == null
						: this.getRexratetolind().equals(other.getRexratetolind()))
				&& (this.getDealnoexcess() == null ? other.getDealnoexcess() == null
						: this.getDealnoexcess().equals(other.getDealnoexcess()))
				&& (this.getFixrateind() == null ? other.getFixrateind() == null
						: this.getFixrateind().equals(other.getFixrateind()))
				&& (this.getFixratecode() == null ? other.getFixratecode() == null
						: this.getFixratecode().equals(other.getFixratecode()))
				&& (this.getRevoper() == null ? other.getRevoper() == null
						: this.getRevoper().equals(other.getRevoper()))
				&& (this.getCtrpartydealno() == null ? other.getCtrpartydealno() == null
						: this.getCtrpartydealno().equals(other.getCtrpartydealno()))
				&& (this.getCtrpartyphoneno() == null ? other.getCtrpartyphoneno() == null
						: this.getCtrpartyphoneno().equals(other.getCtrpartyphoneno()))
				&& (this.getCtrpartyphoneoper() == null ? other.getCtrpartyphoneoper() == null
						: this.getCtrpartyphoneoper().equals(other.getCtrpartyphoneoper()))
				&& (this.getBlockind() == null ? other.getBlockind() == null
						: this.getBlockind().equals(other.getBlockind()))
				&& (this.getConfcreatedind() == null ? other.getConfcreatedind() == null
						: this.getConfcreatedind().equals(other.getConfcreatedind()))
				&& (this.getSpotdate() == null ? other.getSpotdate() == null
						: this.getSpotdate().equals(other.getSpotdate()))
				&& (this.getDisaggccy() == null ? other.getDisaggccy() == null
						: this.getDisaggccy().equals(other.getDisaggccy()))
				&& (this.getDisaggspottrad() == null ? other.getDisaggspottrad() == null
						: this.getDisaggspottrad().equals(other.getDisaggspottrad()))
				&& (this.getDisaggspotport() == null ? other.getDisaggspotport() == null
						: this.getDisaggspotport().equals(other.getDisaggspotport()))
				&& (this.getDisaggspotcost() == null ? other.getDisaggspotcost() == null
						: this.getDisaggspotcost().equals(other.getDisaggspotcost()))
				&& (this.getDisaggfwdtrad() == null ? other.getDisaggfwdtrad() == null
						: this.getDisaggfwdtrad().equals(other.getDisaggfwdtrad()))
				&& (this.getDisaggfwdport() == null ? other.getDisaggfwdport() == null
						: this.getDisaggfwdport().equals(other.getDisaggfwdport()))
				&& (this.getDisaggfwdcost() == null ? other.getDisaggfwdcost() == null
						: this.getDisaggfwdcost().equals(other.getDisaggfwdcost()))
				&& (this.getFwdpremamt() == null ? other.getFwdpremamt() == null
						: this.getFwdpremamt().equals(other.getFwdpremamt()))
				&& (this.getAmenddate() == null ? other.getAmenddate() == null
						: this.getAmenddate().equals(other.getAmenddate()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getBr() == null) ? 0 : getBr().hashCode());
		result = prime * result + ((getDealno() == null) ? 0 : getDealno().hashCode());
		result = prime * result + ((getSeq() == null) ? 0 : getSeq().hashCode());
		result = prime * result + ((getTrad() == null) ? 0 : getTrad().hashCode());
		result = prime * result + ((getVdate() == null) ? 0 : getVdate().hashCode());
		result = prime * result + ((getCust() == null) ? 0 : getCust().hashCode());
		result = prime * result + ((getBrok() == null) ? 0 : getBrok().hashCode());
		result = prime * result + ((getBrokccy() == null) ? 0 : getBrokccy().hashCode());
		result = prime * result + ((getBrokamt() == null) ? 0 : getBrokamt().hashCode());
		result = prime * result + ((getPhonci() == null) ? 0 : getPhonci().hashCode());
		result = prime * result + ((getPort() == null) ? 0 : getPort().hashCode());
		result = prime * result + ((getCost() == null) ? 0 : getCost().hashCode());
		result = prime * result + ((getOdate() == null) ? 0 : getOdate().hashCode());
		result = prime * result + ((getDealdate() == null) ? 0 : getDealdate().hashCode());
		result = prime * result + ((getDealtime() == null) ? 0 : getDealtime().hashCode());
		result = prime * result + ((getIoper() == null) ? 0 : getIoper().hashCode());
		result = prime * result + ((getVoper() == null) ? 0 : getVoper().hashCode());
		result = prime * result + ((getBrokcdate() == null) ? 0 : getBrokcdate().hashCode());
		result = prime * result + ((getCustcdate() == null) ? 0 : getCustcdate().hashCode());
		result = prime * result + ((getPhonecdate() == null) ? 0 : getPhonecdate().hashCode());
		result = prime * result + ((getDealtext() == null) ? 0 : getDealtext().hashCode());
		result = prime * result + ((getPhonetext() == null) ? 0 : getPhonetext().hashCode());
		result = prime * result + ((getPs() == null) ? 0 : getPs().hashCode());
		result = prime * result + ((getProdcode() == null) ? 0 : getProdcode().hashCode());
		result = prime * result + ((getVerind() == null) ? 0 : getVerind().hashCode());
		result = prime * result + ((getRevtext() == null) ? 0 : getRevtext().hashCode());
		result = prime * result + ((getSwapdeal() == null) ? 0 : getSwapdeal().hashCode());
		result = prime * result + ((getFarnearind() == null) ? 0 : getFarnearind().hashCode());
		result = prime * result + ((getPayauthind() == null) ? 0 : getPayauthind().hashCode());
		result = prime * result + ((getSwapvdate() == null) ? 0 : getSwapvdate().hashCode());
		result = prime * result + ((getRevreason() == null) ? 0 : getRevreason().hashCode());
		result = prime * result + ((getInputdate() == null) ? 0 : getInputdate().hashCode());
		result = prime * result + ((getInputtime() == null) ? 0 : getInputtime().hashCode());
		result = prime * result + ((getDealsrce() == null) ? 0 : getDealsrce().hashCode());
		result = prime * result + ((getOrigocamt() == null) ? 0 : getOrigocamt().hashCode());
		result = prime * result + ((getTrcedate() == null) ? 0 : getTrcedate().hashCode());
		result = prime * result + ((getTracecnt() == null) ? 0 : getTracecnt().hashCode());
		result = prime * result + ((getPayoper() == null) ? 0 : getPayoper().hashCode());
		result = prime * result + ((getLstmntdate() == null) ? 0 : getLstmntdate().hashCode());
		result = prime * result + ((getCcy() == null) ? 0 : getCcy().hashCode());
		result = prime * result + ((getCcyamt() == null) ? 0 : getCcyamt().hashCode());
		result = prime * result + ((getCcyterms() == null) ? 0 : getCcyterms().hashCode());
		result = prime * result + ((getCcyrate8() == null) ? 0 : getCcyrate8().hashCode());
		result = prime * result + ((getCcypd8() == null) ? 0 : getCcypd8().hashCode());
		result = prime * result + ((getCcybamt() == null) ? 0 : getCcybamt().hashCode());
		result = prime * result + ((getCcybrate8() == null) ? 0 : getCcybrate8().hashCode());
		result = prime * result + ((getCcybpd8() == null) ? 0 : getCcybpd8().hashCode());
		result = prime * result + ((getBaseterms() == null) ? 0 : getBaseterms().hashCode());
		result = prime * result + ((getNetsi() == null) ? 0 : getNetsi().hashCode());
		result = prime * result + ((getSpotfwdind() == null) ? 0 : getSpotfwdind().hashCode());
		result = prime * result + ((getNetdeal() == null) ? 0 : getNetdeal().hashCode());
		result = prime * result + ((getCcysmeans() == null) ? 0 : getCcysmeans().hashCode());
		result = prime * result + ((getCcysacct() == null) ? 0 : getCcysacct().hashCode());
		result = prime * result + ((getCtrsmeans() == null) ? 0 : getCtrsmeans().hashCode());
		result = prime * result + ((getCtrsacct() == null) ? 0 : getCtrsacct().hashCode());
		result = prime * result + ((getCtrccy() == null) ? 0 : getCtrccy().hashCode());
		result = prime * result + ((getCtramt() == null) ? 0 : getCtramt().hashCode());
		result = prime * result + ((getCtrrate8() == null) ? 0 : getCtrrate8().hashCode());
		result = prime * result + ((getCtrpd8() == null) ? 0 : getCtrpd8().hashCode());
		result = prime * result + ((getCtrbamt() == null) ? 0 : getCtrbamt().hashCode());
		result = prime * result + ((getCtrbrate8() == null) ? 0 : getCtrbrate8().hashCode());
		result = prime * result + ((getNsrnoccy() == null) ? 0 : getNsrnoccy().hashCode());
		result = prime * result + ((getNsrccyseq() == null) ? 0 : getNsrccyseq().hashCode());
		result = prime * result + ((getNsrnoctr() == null) ? 0 : getNsrnoctr().hashCode());
		result = prime * result + ((getNsrctrseq() == null) ? 0 : getNsrctrseq().hashCode());
		result = prime * result + ((getBrprcindte() == null) ? 0 : getBrprcindte().hashCode());
		result = prime * result + ((getRecoper() == null) ? 0 : getRecoper().hashCode());
		result = prime * result + ((getPayauthdte() == null) ? 0 : getPayauthdte().hashCode());
		result = prime * result + ((getRecauthdte() == null) ? 0 : getRecauthdte().hashCode());
		result = prime * result + ((getVerdate() == null) ? 0 : getVerdate().hashCode());
		result = prime * result + ((getRevdate() == null) ? 0 : getRevdate().hashCode());
		result = prime * result + ((getCcyrevalamt() == null) ? 0 : getCcyrevalamt().hashCode());
		result = prime * result + ((getCtrrevalamt() == null) ? 0 : getCtrrevalamt().hashCode());
		result = prime * result + ((getCcynpvamt() == null) ? 0 : getCcynpvamt().hashCode());
		result = prime * result + ((getCtrnpvamt() == null) ? 0 : getCtrnpvamt().hashCode());
		result = prime * result + ((getRecauthoper() == null) ? 0 : getRecauthoper().hashCode());
		result = prime * result + ((getPayauthoper() == null) ? 0 : getPayauthoper().hashCode());
		result = prime * result + ((getSiind() == null) ? 0 : getSiind().hashCode());
		result = prime * result + ((getProdtype() == null) ? 0 : getProdtype().hashCode());
		result = prime * result + ((getSpottrad() == null) ? 0 : getSpottrad().hashCode());
		result = prime * result + ((getSpotport() == null) ? 0 : getSpotport().hashCode());
		result = prime * result + ((getSpotcost() == null) ? 0 : getSpotcost().hashCode());
		result = prime * result + ((getInternalrate8() == null) ? 0 : getInternalrate8().hashCode());
		result = prime * result + ((getCorpspread8() == null) ? 0 : getCorpspread8().hashCode());
		result = prime * result + ((getCorpcost() == null) ? 0 : getCorpcost().hashCode());
		result = prime * result + ((getBrspread8() == null) ? 0 : getBrspread8().hashCode());
		result = prime * result + ((getBrcost() == null) ? 0 : getBrcost().hashCode());
		result = prime * result + ((getTenor() == null) ? 0 : getTenor().hashCode());
		result = prime * result + ((getCcysettdate() == null) ? 0 : getCcysettdate().hashCode());
		result = prime * result + ((getCtrsettdate() == null) ? 0 : getCtrsettdate().hashCode());
		result = prime * result + ((getCustenteredind() == null) ? 0 : getCustenteredind().hashCode());
		result = prime * result + ((getLinkdealno() == null) ? 0 : getLinkdealno().hashCode());
		result = prime * result + ((getLinkproduct() == null) ? 0 : getLinkproduct().hashCode());
		result = prime * result + ((getLinkprodtype() == null) ? 0 : getLinkprodtype().hashCode());
		result = prime * result + ((getFincntr1() == null) ? 0 : getFincntr1().hashCode());
		result = prime * result + ((getFincntr2() == null) ? 0 : getFincntr2().hashCode());
		result = prime * result + ((getCommamt() == null) ? 0 : getCommamt().hashCode());
		result = prime * result + ((getExcoveralllim() == null) ? 0 : getExcoveralllim().hashCode());
		result = prime * result + ((getExccustlim() == null) ? 0 : getExccustlim().hashCode());
		result = prime * result + ((getCorpccy() == null) ? 0 : getCorpccy().hashCode());
		result = prime * result + ((getCorpport() == null) ? 0 : getCorpport().hashCode());
		result = prime * result + ((getCorptrad() == null) ? 0 : getCorptrad().hashCode());
		result = prime * result + ((getRevdealno() == null) ? 0 : getRevdealno().hashCode());
		result = prime * result + ((getOptionind() == null) ? 0 : getOptionind().hashCode());
		result = prime * result + ((getMarginamt() == null) ? 0 : getMarginamt().hashCode());
		result = prime * result + ((getMarginccy() == null) ? 0 : getMarginccy().hashCode());
		result = prime * result + ((getCorpspreadamt() == null) ? 0 : getCorpspreadamt().hashCode());
		result = prime * result + ((getSpotccy() == null) ? 0 : getSpotccy().hashCode());
		result = prime * result + ((getSpotind() == null) ? 0 : getSpotind().hashCode());
		result = prime * result + ((getTenorexceedind() == null) ? 0 : getTenorexceedind().hashCode());
		result = prime * result + ((getCcysettstatus() == null) ? 0 : getCcysettstatus().hashCode());
		result = prime * result + ((getCtrsettstatus() == null) ? 0 : getCtrsettstatus().hashCode());
		result = prime * result + ((getCustrefno() == null) ? 0 : getCustrefno().hashCode());
		result = prime * result + ((getCcyfaildate() == null) ? 0 : getCcyfaildate().hashCode());
		result = prime * result + ((getCcycleardate() == null) ? 0 : getCcycleardate().hashCode());
		result = prime * result + ((getCcyfailoper() == null) ? 0 : getCcyfailoper().hashCode());
		result = prime * result + ((getCcyclearoper() == null) ? 0 : getCcyclearoper().hashCode());
		result = prime * result + ((getCtrfaildate() == null) ? 0 : getCtrfaildate().hashCode());
		result = prime * result + ((getCtrcleardate() == null) ? 0 : getCtrcleardate().hashCode());
		result = prime * result + ((getCtrfailoper() == null) ? 0 : getCtrfailoper().hashCode());
		result = prime * result + ((getCtrclearoper() == null) ? 0 : getCtrclearoper().hashCode());
		result = prime * result + ((getMarginfeeamt() == null) ? 0 : getMarginfeeamt().hashCode());
		result = prime * result + ((getMarginfeeccy() == null) ? 0 : getMarginfeeccy().hashCode());
		result = prime * result + ((getMarginfeesettmeans() == null) ? 0 : getMarginfeesettmeans().hashCode());
		result = prime * result + ((getMarginfeesettacct() == null) ? 0 : getMarginfeesettacct().hashCode());
		result = prime * result + ((getMarginfeerebamt() == null) ? 0 : getMarginfeerebamt().hashCode());
		result = prime * result + ((getMarginfeerebccy() == null) ? 0 : getMarginfeerebccy().hashCode());
		result = prime * result + ((getMarginfeerebsettmeans() == null) ? 0 : getMarginfeerebsettmeans().hashCode());
		result = prime * result + ((getMarginfeerebsettacct() == null) ? 0 : getMarginfeerebsettacct().hashCode());
		result = prime * result + ((getPhoneconfoper() == null) ? 0 : getPhoneconfoper().hashCode());
		result = prime * result + ((getPhoneconftime() == null) ? 0 : getPhoneconftime().hashCode());
		result = prime * result + ((getSwiftmatchind() == null) ? 0 : getSwiftmatchind().hashCode());
		result = prime * result + ((getSiindpay() == null) ? 0 : getSiindpay().hashCode());
		result = prime * result + ((getCommccy() == null) ? 0 : getCommccy().hashCode());
		result = prime * result + ((getRexratetolind() == null) ? 0 : getRexratetolind().hashCode());
		result = prime * result + ((getDealnoexcess() == null) ? 0 : getDealnoexcess().hashCode());
		result = prime * result + ((getFixrateind() == null) ? 0 : getFixrateind().hashCode());
		result = prime * result + ((getFixratecode() == null) ? 0 : getFixratecode().hashCode());
		result = prime * result + ((getRevoper() == null) ? 0 : getRevoper().hashCode());
		result = prime * result + ((getCtrpartydealno() == null) ? 0 : getCtrpartydealno().hashCode());
		result = prime * result + ((getCtrpartyphoneno() == null) ? 0 : getCtrpartyphoneno().hashCode());
		result = prime * result + ((getCtrpartyphoneoper() == null) ? 0 : getCtrpartyphoneoper().hashCode());
		result = prime * result + ((getBlockind() == null) ? 0 : getBlockind().hashCode());
		result = prime * result + ((getConfcreatedind() == null) ? 0 : getConfcreatedind().hashCode());
		result = prime * result + ((getSpotdate() == null) ? 0 : getSpotdate().hashCode());
		result = prime * result + ((getDisaggccy() == null) ? 0 : getDisaggccy().hashCode());
		result = prime * result + ((getDisaggspottrad() == null) ? 0 : getDisaggspottrad().hashCode());
		result = prime * result + ((getDisaggspotport() == null) ? 0 : getDisaggspotport().hashCode());
		result = prime * result + ((getDisaggspotcost() == null) ? 0 : getDisaggspotcost().hashCode());
		result = prime * result + ((getDisaggfwdtrad() == null) ? 0 : getDisaggfwdtrad().hashCode());
		result = prime * result + ((getDisaggfwdport() == null) ? 0 : getDisaggfwdport().hashCode());
		result = prime * result + ((getDisaggfwdcost() == null) ? 0 : getDisaggfwdcost().hashCode());
		result = prime * result + ((getFwdpremamt() == null) ? 0 : getFwdpremamt().hashCode());
		result = prime * result + ((getAmenddate() == null) ? 0 : getAmenddate().hashCode());
		return result;
	}
}