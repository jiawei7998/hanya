package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Cust;
public interface CustService{


    int deleteByPrimaryKey(String cno);

    int insert(Cust record);

    int insertSelective(Cust record);

    Cust selectByPrimaryKey(String cno);

    int updateByPrimaryKeySelective(Cust record);

    int updateByPrimaryKey(Cust record);

    Cust selectByCustaltid(String cfetsId);
    
    Cust selectByCustCmne(String cmne);
    
    Cust selectByBr(String br);

}
