package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Cost;
public interface CostService{


    int deleteByPrimaryKey(String costcent);

    int insert(Cost record);

    int insertSelective(Cost record);

    Cost selectByPrimaryKey(String costcent);

    int updateByPrimaryKeySelective(Cost record);

    int updateByPrimaryKey(Cost record);

}
