package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Port;
import org.apache.ibatis.annotations.Param;

public interface PortMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("portfolio") String portfolio);

    int insert(Port record);

    int insertSelective(Port record);

    Port selectByPrimaryKey(@Param("br") String br, @Param("portfolio") String portfolio);

    int updateByPrimaryKeySelective(Port record);

    int updateByPrimaryKey(Port record);
}