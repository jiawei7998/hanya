package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Sfdc;
import org.apache.ibatis.annotations.Param;

public interface SfdcMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("cno") String cno, @Param("delrecind") String delrecind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("ccy") String ccy, @Param("safekeepacct") String safekeepacct, @Param("dcc") String dcc);

    int insert(Sfdc record);

    int insertSelective(Sfdc record);

    Sfdc selectByPrimaryKey(@Param("br") String br, @Param("cno") String cno, @Param("delrecind") String delrecind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("ccy") String ccy, @Param("safekeepacct") String safekeepacct, @Param("dcc") String dcc);

    int updateByPrimaryKeySelective(Sfdc record);

    int updateByPrimaryKey(Sfdc record);
}