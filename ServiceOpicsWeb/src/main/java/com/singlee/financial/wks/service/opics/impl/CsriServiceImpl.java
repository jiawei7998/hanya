package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Csri;
import com.singlee.financial.wks.mapper.opics.CsriMapper;
import com.singlee.financial.wks.service.opics.CsriService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CsriServiceImpl implements CsriService {

    @Resource
    private CsriMapper csriMapper;

    @Override
    public int deleteByPrimaryKey(String br,String cust,String product,String type,String ccy) {
        return csriMapper.deleteByPrimaryKey(br,cust,product,type,ccy);
    }

    @Override
    public int insert(Csri record) {
        return csriMapper.insert(record);
    }

    @Override
    public int insertSelective(Csri record) {
        return csriMapper.insertSelective(record);
    }

    @Override
    public Csri selectByPrimaryKey(String br,String cust,String product,String type,String ccy) {
        return csriMapper.selectByPrimaryKey(br,cust,product,type,ccy);
    }

    @Override
    public int updateByPrimaryKeySelective(Csri record) {
        return csriMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Csri record) {
        return csriMapper.updateByPrimaryKey(record);
    }

}
