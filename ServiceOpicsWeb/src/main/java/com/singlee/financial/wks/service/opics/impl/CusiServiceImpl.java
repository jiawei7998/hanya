package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Cusi;
import com.singlee.financial.wks.mapper.opics.CusiMapper;
import com.singlee.financial.wks.service.opics.CusiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class CusiServiceImpl implements CusiService{

    @Resource
    private CusiMapper cusiMapper;

    @Override
    public int deleteByPrimaryKey(String custidtype,String custaltid) {
        return cusiMapper.deleteByPrimaryKey(custidtype,custaltid);
    }

    @Override
    public int insert(Cusi record) {
        return cusiMapper.insert(record);
    }

    @Override
    public int insertSelective(Cusi record) {
        return cusiMapper.insertSelective(record);
    }

    @Override
    public Cusi selectByPrimaryKey(String custidtype,String custaltid) {
        return cusiMapper.selectByPrimaryKey(custidtype,custaltid);
    }

    @Override
    public int updateByPrimaryKeySelective(Cusi record) {
        return cusiMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Cusi record) {
        return cusiMapper.updateByPrimaryKey(record);
    }


}
