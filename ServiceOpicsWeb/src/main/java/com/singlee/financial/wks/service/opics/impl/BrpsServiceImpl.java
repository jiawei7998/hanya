package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.mapper.opics.BrpsMapper;
import com.singlee.financial.wks.service.opics.BrpsService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class BrpsServiceImpl implements BrpsService {

    @Resource
    private BrpsMapper brpsMapper;

    @Override
    public int deleteByPrimaryKey(String br) {
        return brpsMapper.deleteByPrimaryKey(br);
    }

    @Override
    public int insert(Brps record) {
        return brpsMapper.insert(record);
    }

    @Override
    public int insertSelective(Brps record) {
        return brpsMapper.insertSelective(record);
    }

    @Override
    public Brps selectByPrimaryKey(String br) {
        return brpsMapper.selectByPrimaryKey(br);
    }

    @Override
    public int updateByPrimaryKeySelective(Brps record) {
        return brpsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Brps record) {
        return brpsMapper.updateByPrimaryKey(record);
    }

	@Override
	public boolean isBatchRunning(String br) {
		
		Brps brps = selectByPrimaryKey(br);
		
		if("ONLINE".equalsIgnoreCase(StringUtils.trim(brps.getBrstatus()))) {
			return false;
		}
		
		return true;
	}

    
}
