package com.singlee.financial.wks.bean.opics;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Mcfp implements Serializable {
    private String br;

    private String prodcode;

    private String type;

    private String daysreq;

    private String daysadv;

    private String spotci;

    private String fwdci;

    private Date lstmntdte;

    private String traddno;

    private String backdno;

    private String confind;

    private String rfu1;

    private String rfu2;

    private String rfu3;

    private String rfu4;

    private String rfu5;

    private String rfu6;

    private String rfu7;

    private String rfu8;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getProdcode() {
		return prodcode;
	}


	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getDaysreq() {
		return daysreq;
	}


	public void setDaysreq(String daysreq) {
		this.daysreq = daysreq;
	}


	public String getDaysadv() {
		return daysadv;
	}


	public void setDaysadv(String daysadv) {
		this.daysadv = daysadv;
	}


	public String getSpotci() {
		return spotci;
	}


	public void setSpotci(String spotci) {
		this.spotci = spotci;
	}


	public String getFwdci() {
		return fwdci;
	}


	public void setFwdci(String fwdci) {
		this.fwdci = fwdci;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public String getTraddno() {
		return traddno;
	}


	public void setTraddno(String traddno) {
		this.traddno = traddno;
	}


	public String getBackdno() {
		return backdno;
	}


	public void setBackdno(String backdno) {
		this.backdno = backdno;
	}


	public String getConfind() {
		return confind;
	}


	public void setConfind(String confind) {
		this.confind = confind;
	}


	public String getRfu1() {
		return rfu1;
	}


	public void setRfu1(String rfu1) {
		this.rfu1 = rfu1;
	}


	public String getRfu2() {
		return rfu2;
	}


	public void setRfu2(String rfu2) {
		this.rfu2 = rfu2;
	}


	public String getRfu3() {
		return rfu3;
	}


	public void setRfu3(String rfu3) {
		this.rfu3 = rfu3;
	}


	public String getRfu4() {
		return rfu4;
	}


	public void setRfu4(String rfu4) {
		this.rfu4 = rfu4;
	}


	public String getRfu5() {
		return rfu5;
	}


	public void setRfu5(String rfu5) {
		this.rfu5 = rfu5;
	}


	public String getRfu6() {
		return rfu6;
	}


	public void setRfu6(String rfu6) {
		this.rfu6 = rfu6;
	}


	public String getRfu7() {
		return rfu7;
	}


	public void setRfu7(String rfu7) {
		this.rfu7 = rfu7;
	}


	public String getRfu8() {
		return rfu8;
	}


	public void setRfu8(String rfu8) {
		this.rfu8 = rfu8;
	}


	private static final long serialVersionUID = 1L;
    
   
}