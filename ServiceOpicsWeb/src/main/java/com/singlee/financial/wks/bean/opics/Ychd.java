package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Ychd implements Serializable {
    private String br;

    private String ccy;

    private String yieldcurve;

    private String curvepurpose;

    private String descr;

    private String curvetype;

    private String interpmethod;

    private String spotdays;

    private String payrule;

    private String defaultflag;

    private String quotetype;

    private String gennightly;

    private String importedcurve;

    private String fincent;

    private Date lstmntdte;

    private String riskfreecurve;

    private String sourcesystem;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getYieldcurve() {
		return yieldcurve;
	}


	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}


	public String getCurvepurpose() {
		return curvepurpose;
	}


	public void setCurvepurpose(String curvepurpose) {
		this.curvepurpose = curvepurpose;
	}


	public String getDescr() {
		return descr;
	}


	public void setDescr(String descr) {
		this.descr = descr;
	}


	public String getCurvetype() {
		return curvetype;
	}


	public void setCurvetype(String curvetype) {
		this.curvetype = curvetype;
	}


	public String getInterpmethod() {
		return interpmethod;
	}


	public void setInterpmethod(String interpmethod) {
		this.interpmethod = interpmethod;
	}


	public String getSpotdays() {
		return spotdays;
	}


	public void setSpotdays(String spotdays) {
		this.spotdays = spotdays;
	}


	public String getPayrule() {
		return payrule;
	}


	public void setPayrule(String payrule) {
		this.payrule = payrule;
	}


	public String getDefaultflag() {
		return defaultflag;
	}


	public void setDefaultflag(String defaultflag) {
		this.defaultflag = defaultflag;
	}


	public String getQuotetype() {
		return quotetype;
	}


	public void setQuotetype(String quotetype) {
		this.quotetype = quotetype;
	}


	public String getGennightly() {
		return gennightly;
	}


	public void setGennightly(String gennightly) {
		this.gennightly = gennightly;
	}


	public String getImportedcurve() {
		return importedcurve;
	}


	public void setImportedcurve(String importedcurve) {
		this.importedcurve = importedcurve;
	}


	public String getFincent() {
		return fincent;
	}


	public void setFincent(String fincent) {
		this.fincent = fincent;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public String getRiskfreecurve() {
		return riskfreecurve;
	}


	public void setRiskfreecurve(String riskfreecurve) {
		this.riskfreecurve = riskfreecurve;
	}


	public String getSourcesystem() {
		return sourcesystem;
	}


	public void setSourcesystem(String sourcesystem) {
		this.sourcesystem = sourcesystem;
	}


	private static final long serialVersionUID = 1L;

}