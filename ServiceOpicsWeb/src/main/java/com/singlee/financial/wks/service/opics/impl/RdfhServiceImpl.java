package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.wks.bean.opics.Rdfh;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.mapper.opics.RdfhMapper;
import com.singlee.financial.wks.service.opics.RdfhService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Calendar;

@Service
public class RdfhServiceImpl implements RdfhService {

    @Resource
    private RdfhMapper rdfhMapper;

    @Override
    public int deleteByPrimaryKey(String br,String transno,String seq) {
        return rdfhMapper.deleteByPrimaryKey(br,transno,seq);
    }

    @Override
    public int insert(Rdfh record) {
        return rdfhMapper.insert(record);
    }

    @Override
    public int insertSelective(Rdfh record) {
        return rdfhMapper.insertSelective(record);
    }

    @Override
    public Rdfh selectByPrimaryKey(String br,String transno,String seq) {
        return rdfhMapper.selectByPrimaryKey(br,transno,seq);
    }

    @Override
    public int updateByPrimaryKeySelective(Rdfh record) {
        return rdfhMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Rdfh record) {
        return rdfhMapper.updateByPrimaryKey(record);
    }
    @Override
	public int updateByPrimaryKeyRec(Rdfh record) {
    	return rdfhMapper.updateByPrimaryKeyRec(record);
	}

	@Override
	public int updateRevInterest(Rdfh record) {
		return rdfhMapper.updateRevInterest(record);
	}
    

	@Override
	public Rdfh builder(Spsh spsh) {
		Rdfh rdfh = new Rdfh();
		rdfh.setBr(spsh.getBr());//机构:
		rdfh.setTransno(spsh.getDealno());//单号:
		rdfh.setSeq(spsh.getSeq());//序号:
		rdfh.setBrprcindte(spsh.getBrprcindte());//账务日期:
		rdfh.setCcy(spsh.getCcy());//币种:
		rdfh.setCno(spsh.getCno());//交易对手:
		rdfh.setDr(null);//收息:
		rdfh.setInitcost(BigDecimal.ZERO);//利息COST:
		rdfh.setInputdate(Calendar.getInstance().getTime());//录入日期:
		rdfh.setIoper(null);//录入人员:
		rdfh.setInputtime(DateUtil.getCurrentTimeAsString());//:
		rdfh.setProduct(spsh.getProduct());//产品:
		rdfh.setProdtype(spsh.getProdtype());//:
		rdfh.setQty(BigDecimal.ZERO);//产品类型:
		rdfh.setSecauthind("0");//:默认值:
		rdfh.setSecauthoper(null);//:
		rdfh.setSecid(spsh.getSecid());//债券号:
		rdfh.setSettdate(spsh.getSettdate());//交割日期:
		rdfh.setTransdate(Calendar.getInstance().getTime());//创立日期:
		rdfh.setTranstime(DateUtil.getCurrentTimeAsString());//时间:
		rdfh.setVerdate(spsh.getDealdate());//
		rdfh.setVeroper(null);//:
		rdfh.setVerind("1");//:
		rdfh.setSuppconfind("1");//:
		rdfh.setFreepaycash("C");//:
		rdfh.setCcyamt(null);//:
		rdfh.setVatamt(BigDecimal.ZERO);//收息金额:
		rdfh.setWhtamt(BigDecimal.ZERO);//:
		rdfh.setProceeds(null);//:
		rdfh.setFeeamt1(BigDecimal.ZERO);//:
		rdfh.setFeeamt2(BigDecimal.ZERO);//:
		rdfh.setFeeamt3(BigDecimal.ZERO);//:
		rdfh.setCcysmeans(null);//:
		rdfh.setCcysacct(null);//:
		rdfh.setDealdate(null);//:
		rdfh.setDealtext(null);//:
		rdfh.setCost(spsh.getCost());//:
		rdfh.setRfuprice8(BigDecimal.ZERO);//:
		rdfh.setPort(spsh.getPort());//:
		rdfh.setInvtype(null);//:
		rdfh.setCcyauthdate(null);//:
		rdfh.setCcyauthind("1");//:
		rdfh.setCcyauthoper(null);//:
		rdfh.setSettccy(null);//:
		rdfh.setSettexchrate8(null);//:
		rdfh.setSettamt(null);//:
		rdfh.setSettbaseexchrate8(null);//:
		rdfh.setSettbaseamt(null);//:
		rdfh.setCcybaseamt(null);//:

		return rdfh;
	}

}
