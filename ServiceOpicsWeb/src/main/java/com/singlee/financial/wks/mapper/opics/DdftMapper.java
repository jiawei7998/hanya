package com.singlee.financial.wks.mapper.opics;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.singlee.financial.wks.bean.opics.Ddft;

public interface DdftMapper {

    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("yieldcurve") String yieldcurve, @Param("shiftseq") String shiftseq, @Param("brprocdate") Date brprocdate, @Param("matdate") Date matdate, @Param("quotetype") String quotetype);

    int insert(Ddft record);

    int insertSelective(Ddft record);

    Ddft selectByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("yieldcurve") String yieldcurve, @Param("shiftseq") String shiftseq, @Param("brprocdate") Date brprocdate, @Param("matdate") Date matdate, @Param("quotetype") String quotetype);

    int updateByPrimaryKeySelective(Ddft record);

    int updateByPrimaryKey(Ddft record);

    int updateBatch(List<Ddft> list);

    int updateBatchSelective(List<Ddft> list);
    
    int selectCountByBpdate(@Param("br") String br, @Param("ccys") List<String> ccys, @Param("shiftseq") String shiftseq, @Param("brprocdate") Date brprocdate, @Param("quotetype") String quotetype, @Param("mty") String mty);
}