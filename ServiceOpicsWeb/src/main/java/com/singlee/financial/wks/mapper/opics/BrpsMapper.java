package com.singlee.financial.wks.mapper.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Brps;

public interface BrpsMapper {
    int deleteByPrimaryKey(String br);

    int insert(Brps record);

    int insertSelective(Brps record);

    Brps selectByPrimaryKey(String br);
    
    List<Brps> selectAll();

    int updateByPrimaryKeySelective(Brps record);

    int updateByPrimaryKey(Brps record);
}