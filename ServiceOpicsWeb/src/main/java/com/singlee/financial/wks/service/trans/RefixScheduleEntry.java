package com.singlee.financial.wks.service.trans;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.bean.opics.Swds;
import com.singlee.financial.wks.bean.opics.Swdt;
import com.singlee.financial.wks.expand.SwapItemSchdWksBean;
import com.singlee.financial.wks.service.opics.SwdsService;
import com.singlee.financial.wks.util.FinancialBeanHelper;
import com.singlee.financial.wks.util.FinancialDateUtils;

@Component
public class RefixScheduleEntry {

	private Logger logger = Logger.getLogger(RefixScheduleEntry.class);

	/**
	 * 根据条件生成现金流
	 * 
	 * @param irs
	 * @param swdh
	 * @param swdtLst
	 */
	public void DataTrans(SwapWksBean irs, Swdh swdh, List<Swdt> swdtLst) {
		FinancialBeanHelper.TrimBeanAttrValue(swdh);// 去空格
		irs.setInstId(swdh.getBr());
		irs.setDealDate(FinancialDateUtils.format(swdh.getDealdate()));
		irs.setVDate(FinancialDateUtils.format(swdh.getStartdate()));
		irs.setMDate(FinancialDateUtils.format(swdh.getMatdate()));
		for (Swdt swdt : swdtLst) {
			FinancialBeanHelper.TrimBeanAttrValue(swdt);// 去空格
			if (StringUtils.equals(swdt.getPayrecind(), "P")) {// 付
				irs.setAmt(swdt.getNotccyamt());// 本金
				irs.setCcy(swdt.getNotccy());// 货币代码
				irs.setPayNominalAmount(swdt.getNotccyamt());// 本金
				irs.setPayCcy(swdt.getNotccy());// 货币
				irs.setPayRateCode(swdt.getRatecode());// 利率代码
				irs.setPayCalIntType(swdt.getCompound());// 复利
				irs.setPayCalIntBasic(swdt.getBasis());// 计息基础
				irs.setPayFloatBps(BigDecimal.ZERO);// 点差
				irs.setPayFixedRate(swdt.getIntrate8());// 利率
				irs.setPayPaymentFrequency(swdt.getIntpaycycle());// 重置频率
				irs.setIntPayAdjMod(swdt.getIntpaycycle());// 利息调整规则
				irs.setPayCashFlowMethod(swdt.getSchdconv());// 现金流计算方法
				irs.setPayInterestResetFrequency(
						StringUtils.equals(swdt.getFixfloatind(), "X") ? "" : swdt.getRaterevcycle());// 重置频率
				irs.setPayRaterevpayrule(swdt.getIntpaypayrule());// 利率调整策略
				irs.setPayRateType(swdt.getFixfloatind());// 固定-浮动标志
				irs.setPayYieldcurve(swdt.getYieldcurve());// 收益率曲线
				irs.setPaySeq(swdt.getSeq());// 序号
				irs.setPayAcctType(swdt.getIntsmeans());// 清算方式
				irs.setPayAcctNo(swdt.getIntsacct());// 清算帐户
			} else {
				irs.setAmt(swdt.getNotccyamt());// 本金
				irs.setCcy(swdt.getNotccy());// 货币代码
				irs.setRecNominalAmount(swdt.getNotccyamt());// 本金
				irs.setRecCcy(swdt.getNotccy());// 货币
				irs.setRecRateCode(swdt.getRatecode());// 利率代码
				irs.setRecCalIntType(swdt.getCompound());// 复利
				irs.setRecCalIntBasic(swdt.getBasis());// 计息基础
				irs.setRecFloatBps(BigDecimal.ZERO);// 点差
				irs.setRecFixedRate(swdt.getIntrate8());// 利率
				irs.setRecPaymentFrequency(swdt.getIntpaycycle());// 重置频率
				irs.setIntPayAdjMod(swdt.getIntpaypayrule());// 利息调整规则
				irs.setRecCashFlowMethod(swdt.getSchdconv());// 现金流计算方法
				irs.setRecRaterevpayrule(swdt.getIntpaypayrule());// 利率调整策略
				irs.setRecInterestResetFrequency(
						StringUtils.equals(swdt.getFixfloatind(), "X") ? "" : swdt.getRaterevcycle());// 重置频率
				irs.setRecRateType(swdt.getFixfloatind());// 固定-浮动标志
				irs.setRecYieldcurve(swdt.getYieldcurve());// 收益率曲线
				irs.setRecSeq(swdt.getSeq());// 序号
				irs.setRecAcctType(swdt.getIntsmeans());// 清算方式
				irs.setRecAcctNo(swdt.getIntsacct());// 清算帐户
			}
		}
	}

	/**
	 * 转换更新schd
	 * 
	 * @param irs
	 * @param swdh
	 * @param swdsService
	 */
	public void DataTrans(SwapWksBean irs, Swdh swdh, SwdsService swdsService) {
		List<SwapItemSchdWksBean> itemSchdWksBeansLst = irs.getItemBeanSwds();
		Map<String, Swds> swdsMap = new HashMap<String, Swds>();
		for (SwapItemSchdWksBean siswb : itemSchdWksBeansLst) {// 循环处理
			String br = swdh.getBr();
			String dealno = swdh.getDealno();
			String seq = siswb.getSeq();
			String dealind = swdh.getDealind();
			String product = swdh.getProduct();
			String prodtype = swdh.getProdtype();
			String schdseq = siswb.getSchdSeq();
			String schdtype = siswb.getSchdType();
			Swds swds = swdsService.selectBySeq(br, dealno, seq, dealind, product, prodtype, schdseq, schdtype);
			FinancialBeanHelper.TrimBeanAttrValue(swds);// 去空格
			logger.info("开始处理,交易单号:" + dealno + "固定浮动:" + siswb.getRateType() + "计划类型:" + schdtype + "计划所属腿编号:" + seq
					+ "计划编号:" + schdseq + "利息开始日期:" + siswb.getBeginDate() + "利息结束日期:" + siswb.getEndDate() + "利息支付日期:"
					+ siswb.getPayDate() + "利息重定日期:" + siswb.getFixingDate() + "利息金额:" + siswb.getInterestAmt());
			if (StringUtils.equals(siswb.getSchdType(), "M")) {// 如果是最后一条,则删除
				// 获取最后一期
				Swds swdsM = swdsService.selectBySchdtype(br, dealno, seq, dealind, product, prodtype, schdtype);
				// 删除大于最后一期所有记录
				swdsService.deleteByGtSeq(br, dealno, seq, dealind, product, prodtype, schdseq, schdtype);
				swdsM.setIntenddte(FinancialDateUtils.parse(siswb.getEndDate()));// 利息区间结束日期
				swdsM.setIntstrtdte(FinancialDateUtils.parse(siswb.getBeginDate()));// 利息区间开始日期
				swdsM.setIpaydate(FinancialDateUtils.parse(siswb.getPayDate()));// 利息支付日期
				swdsM.setIntamt(siswb.getInterestAmt());// 利息金额
				swdsM.setPrinccyintamt(siswb.getInterestAmt());
				swdsM.setLstmntdte(Calendar.getInstance().getTime());// 修改日期
				swdsM.setSchdseq(siswb.getSchdSeq());// 重新设定
				swdsService.insertSelective(swdsM);
			} else {// 中间收-付息
				if (null != swds) {// 不为空,则更新
					logger.debug("找到记录,进行更新处理....");
					swds.setIntenddte(FinancialDateUtils.parse(siswb.getEndDate()));// 利息区间结束日期
					swds.setIntstrtdte(FinancialDateUtils.parse(siswb.getBeginDate()));// 利息区间开始日期
					if ("L".equalsIgnoreCase(swds.getFixfloatind())
							&& ("P".equalsIgnoreCase(swds.getSchdtype()) || "R".equalsIgnoreCase(swds.getSchdtype()))) {
						swds.setIpaydate(FinancialDateUtils.parse(siswb.getPayDate()));// 利息支付日期
						swds.setRatefixdte(FinancialDateUtils.parse(siswb.getFixingDate()));// 重定日期
						swds.setRaterevdte(FinancialDateUtils.parse(siswb.getEndDate()));
					}
					swds.setIntamt(siswb.getInterestAmt());// 利息金额
					swds.setPrinccyintamt(siswb.getInterestAmt());
					swds.setLstmntdte(Calendar.getInstance().getTime());// 修改日期
					swdsService.updateBySeqSelective(swds);
					swdsMap.put(seq + schdseq, swds);// 临时存放
				} else {// 不存在,则记录下来
					// 查询上一条作为模板
					logger.debug("未找到记录,进行新增处理....");
					Swds swdsPre = swdsMap.get(seq + String.format("%03d", Integer.parseInt(siswb.getSchdSeq()) - 1));
					logger.debug("获取前一条记录交易单号:" + dealno + "固定浮动:" + siswb.getRateType() + "计划类型:" + schdtype
							+ "计划所属腿编号" + seq + "计划编号:" + schdseq);
					swdsPre.setSchdseq(schdseq);
					swdsPre.setIntenddte(FinancialDateUtils.parse(siswb.getEndDate()));// 利息区间结束日期
					swdsPre.setIntstrtdte(FinancialDateUtils.parse(siswb.getBeginDate()));// 利息区间开始日期
					swdsPre.setIpaydate(FinancialDateUtils.parse(siswb.getPayDate()));// 利息支付日期
					if ("L".equalsIgnoreCase(swdsPre.getFixfloatind()) && ("P".equalsIgnoreCase(swdsPre.getSchdtype())
							|| "R".equalsIgnoreCase(swdsPre.getSchdtype()))) {
						swdsPre.setRatefixdte(FinancialDateUtils.parse(siswb.getFixingDate()));// 重定日期
						swdsPre.setRaterevdte(FinancialDateUtils.parse(siswb.getEndDate()));
						swdsPre.setIntamt(siswb.getInterestAmt());// 利息金额
					}
					swdsPre.setPrinccyintamt(siswb.getInterestAmt());
					swdsPre.setLstmntdte(Calendar.getInstance().getTime());// 修改日期
					swdsService.insertSelective(swdsPre);
					swdsMap.put(seq + schdseq, swdsPre);// 临时存放
				}
			}
		}
	}
}
