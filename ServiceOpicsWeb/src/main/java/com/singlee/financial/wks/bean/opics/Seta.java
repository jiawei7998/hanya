package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Seta implements Serializable {
    private String br;

    private String smeans;

    private String sacct;

    private String ccy;

    private String costcent;

    private String genledgno;

    private String cno;

    private BigDecimal acctbal;

    private Date lstmntdte;
    
    

    public String getBr() {
		return br;
	}



	public void setBr(String br) {
		this.br = br;
	}



	public String getSmeans() {
		return smeans;
	}



	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}



	public String getSacct() {
		return sacct;
	}



	public void setSacct(String sacct) {
		this.sacct = sacct;
	}



	public String getCcy() {
		return ccy;
	}



	public void setCcy(String ccy) {
		this.ccy = ccy;
	}



	public String getCostcent() {
		return costcent;
	}



	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}



	public String getGenledgno() {
		return genledgno;
	}



	public void setGenledgno(String genledgno) {
		this.genledgno = genledgno;
	}



	public String getCno() {
		return cno;
	}



	public void setCno(String cno) {
		this.cno = cno;
	}



	public BigDecimal getAcctbal() {
		return acctbal;
	}



	public void setAcctbal(BigDecimal acctbal) {
		this.acctbal = acctbal;
	}



	public Date getLstmntdte() {
		return lstmntdte;
	}



	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}



	private static final long serialVersionUID = 1L;
}