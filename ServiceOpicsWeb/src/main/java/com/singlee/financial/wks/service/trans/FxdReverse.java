package com.singlee.financial.wks.service.trans;


import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.bean.opics.Fxdt;
import com.singlee.financial.wks.service.opics.BrpsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;



/**
 * 交易冲销转换方法
 * 
 * @author xuqq
 * @date 2020/02/19
 */
@Service
public class FxdReverse {
	
	public List<Fxdh> DataTrans(List<Fxdh> fxdhs, BrpsService brpsService) {
		List<Fxdh> fxdhList = new ArrayList<Fxdh>();
		fxdhs.stream().forEach(fxdh -> {
			fxdhList.add(DataTrans(fxdh,brpsService));
		});
		return fxdhList;
	}

	public Fxdh DataTrans(Fxdh fxdh, BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(fxdh.getBr());
		fxdh.setRevreason("99");// 冲销参数:默认99
		fxdh.setRevdate(brps.getBranprcdate());// 冲销日期
		fxdh.setRevtext("");// 冲销描述
		fxdh.setRevoper(OpicsConstant.FX.TRAD_NO);// 冲销复核人员
//		fxdh.setUpdatecounter(fxdh.getUpdatecounter() + 1);// 修改次数
		fxdh.setLstmntdate(Calendar.getInstance().getTime());// 最后修改日期
		return fxdh;
	}

	public Fxdt DataTrans(Fxdt fxdt, String br, String dealno, String seq) {
		fxdt.setBr(br);
		fxdt.setDealno(dealno);
		fxdt.setSeq(seq);
//		fxdt.setUpdatecounter(fxdt.getUpdatecounter() + 1);// 修改次数
		fxdt.setLstmntdte(Calendar.getInstance().getTime());// 最后修改日期
		return fxdt;
	}
}
