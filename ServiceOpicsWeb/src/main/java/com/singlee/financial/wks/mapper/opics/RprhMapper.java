package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Rprh;
import org.apache.ibatis.annotations.Param;

public interface RprhMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    int insert(Rprh record);

    int insertSelective(Rprh record);

    Rprh selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    Rprh selectByTradeNo(@Param("br") String br, @Param("dealtext") String tradeNo);

    int updateByPrimaryKeySelective(Rprh record);

    int updateByPrimaryKey(Rprh record);
}