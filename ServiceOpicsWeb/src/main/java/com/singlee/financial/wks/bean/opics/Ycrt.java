package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Ycrt implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String br;

    private String ccy;

    private String contribrate;

    private String mtystart;

    private String mtyend;

    private BigDecimal bidrate8;

    private BigDecimal bidprice8;

    private BigDecimal bidspread8;

    private BigDecimal midrate8;

    private BigDecimal midprice8;

    private BigDecimal midspread8;

    private BigDecimal offerrate8;

    private BigDecimal offerprice8;

    private BigDecimal offerspread8;

    private String secid;

    private Date lstmntdte;

    private String bidsourceDde;

    private String midsourceDde;

    private String offersourceDde;
    
    private String ratetype;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getContribrate() {
		return contribrate;
	}

	public void setContribrate(String contribrate) {
		this.contribrate = contribrate;
	}

	public String getMtystart() {
		return mtystart;
	}

	public void setMtystart(String mtystart) {
		this.mtystart = mtystart;
	}

	public String getMtyend() {
		return mtyend;
	}

	public void setMtyend(String mtyend) {
		this.mtyend = mtyend;
	}

	public BigDecimal getBidrate8() {
		return bidrate8;
	}

	public void setBidrate8(BigDecimal bidrate8) {
		this.bidrate8 = bidrate8;
	}

	public BigDecimal getBidprice8() {
		return bidprice8;
	}

	public void setBidprice8(BigDecimal bidprice8) {
		this.bidprice8 = bidprice8;
	}

	public BigDecimal getBidspread8() {
		return bidspread8;
	}

	public void setBidspread8(BigDecimal bidspread8) {
		this.bidspread8 = bidspread8;
	}

	public BigDecimal getMidrate8() {
		return midrate8;
	}

	public void setMidrate8(BigDecimal midrate8) {
		this.midrate8 = midrate8;
	}

	public BigDecimal getMidprice8() {
		return midprice8;
	}

	public void setMidprice8(BigDecimal midprice8) {
		this.midprice8 = midprice8;
	}

	public BigDecimal getMidspread8() {
		return midspread8;
	}

	public void setMidspread8(BigDecimal midspread8) {
		this.midspread8 = midspread8;
	}

	public BigDecimal getOfferrate8() {
		return offerrate8;
	}

	public void setOfferrate8(BigDecimal offerrate8) {
		this.offerrate8 = offerrate8;
	}

	public BigDecimal getOfferprice8() {
		return offerprice8;
	}

	public void setOfferprice8(BigDecimal offerprice8) {
		this.offerprice8 = offerprice8;
	}

	public BigDecimal getOfferspread8() {
		return offerspread8;
	}

	public void setOfferspread8(BigDecimal offerspread8) {
		this.offerspread8 = offerspread8;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getBidsourceDde() {
		return bidsourceDde;
	}

	public void setBidsourceDde(String bidsourceDde) {
		this.bidsourceDde = bidsourceDde;
	}

	public String getMidsourceDde() {
		return midsourceDde;
	}

	public void setMidsourceDde(String midsourceDde) {
		this.midsourceDde = midsourceDde;
	}

	public String getOffersourceDde() {
		return offersourceDde;
	}

	public void setOffersourceDde(String offersourceDde) {
		this.offersourceDde = offersourceDde;
	}

	public String getRatetype() {
		return ratetype;
	}

	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}
    
}