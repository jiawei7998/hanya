package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Bico;
public interface BicoService{


    int deleteByPrimaryKey(String bic);

    int insert(Bico record);

    int insertSelective(Bico record);

    Bico selectByPrimaryKey(String bic);

    int updateByPrimaryKeySelective(Bico record);

    int updateByPrimaryKey(Bico record);

}
