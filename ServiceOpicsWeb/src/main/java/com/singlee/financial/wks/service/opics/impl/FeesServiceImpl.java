package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Fees;
import com.singlee.financial.wks.mapper.opics.FeesMapper;
import com.singlee.financial.wks.service.opics.FeesService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class FeesServiceImpl implements FeesService {

	@Resource
	private FeesMapper feesMapper;

	@Override
	public int deleteByPrimaryKey(String br, String feeno, String feeseq) {
		return feesMapper.deleteByPrimaryKey(br, feeno, feeseq);
	}

	@Override
	public int insert(Fees record) {
		return feesMapper.insert(record);
	}

	@Override
	public int insertSelective(Fees record) {
		return feesMapper.insertSelective(record);
	}

	@Override
	public Fees selectByPrimaryKey(String br, String feeno, String feeseq) {
		return feesMapper.selectByPrimaryKey(br, feeno, feeseq);
	}

	@Override
	public int updateByPrimaryKeySelective(Fees record) {
		return feesMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Fees record) {
		return feesMapper.updateByPrimaryKey(record);
	}

	@Override
	public Fees selectByTradeNo(String br, String dealtext) {
		return feesMapper.selectByTradeNo(br, dealtext);
	}

}
