package com.singlee.financial.wks.bean.cashflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class YieldCurveBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1750858287442754179L;

	/**
	 * 曲线名称
	 */
	private String yieldCurveName;
	
	/**
	 * 曲线币种
	 */
	private String currency;
	
	/**
	 * 曲线使用场景,MTM、Analytics
	 */
	private String scene;
	
	/**
	 * 曲线类型,Bond、Swap、Spread
	 */
	private String curveType;
	
	
	/**
	 * 插值方法,Linear、Logarithmic、Cubic、CubicSpline
	 */
	private String interpolationMethod;
	
	/**
	 * 日期处理规则
	 */
	private String dateConvention;
	
	/**
	 * 即期汇率对应的天数
	 */
	private int spotDays;
	
	/**
	 * 曲线贡献点信息
	 */
	private List<YieldCurveSubBean> yieldCurveSubBeans = new ArrayList<YieldCurveSubBean>();

	public String getYieldCurveName() {
		return yieldCurveName;
	}

	public void setYieldCurveName(String yieldCurveName) {
		this.yieldCurveName = yieldCurveName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getCurveType() {
		return curveType;
	}

	public void setCurveType(String curveType) {
		this.curveType = curveType;
	}

	public String getInterpolationMethod() {
		return interpolationMethod;
	}

	public void setInterpolationMethod(String interpolationMethod) {
		this.interpolationMethod = interpolationMethod;
	}

	public String getDateConvention() {
		return dateConvention;
	}

	public void setDateConvention(String dateConvention) {
		this.dateConvention = dateConvention;
	}

	public int getSpotDays() {
		return spotDays;
	}

	public void setSpotDays(int spotDays) {
		this.spotDays = spotDays;
	}

	public List<YieldCurveSubBean> getYieldCurveSubBeans() {
		return yieldCurveSubBeans;
	}

	public void addYieldCurveSubBeans(YieldCurveSubBean yieldCurveSubBean) {
		this.yieldCurveSubBeans.add(yieldCurveSubBean);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("YieldCurveBean [yieldCurveName=");
		builder.append(yieldCurveName);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", scene=");
		builder.append(scene);
		builder.append(", curveType=");
		builder.append(curveType);
		builder.append(", interpolationMethod=");
		builder.append(interpolationMethod);
		builder.append(", dateConvention=");
		builder.append(dateConvention);
		builder.append(", spotDays=");
		builder.append(spotDays);
		builder.append(", yieldCurveSubBeans=");
		builder.append(yieldCurveSubBeans);
		builder.append("]");
		return builder.toString();
	}
	
	
}
