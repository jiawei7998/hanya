package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Slda implements Serializable{
    private static final long serialVersionUID = 1L;
    private String br;

    private String dealno;

    private String seq;

    private String dealind;

    private String assignseq;

    private String prodtype;

    private String product;

    private String assigntype;

    private Date brprcindte;

    private String ccy;

    private BigDecimal chargeamt;

    private BigDecimal clprice8;

    private BigDecimal comprocamt;

    private String cost;

    private String dealtime;

    private String delivtype;

    private BigDecimal discamt;

    private BigDecimal discrate8;

    private BigDecimal dispricerate8;

    private String dispricerateind;

    private BigDecimal drprice8;

    private BigDecimal extretqty;

    private BigDecimal extretamt;

    private BigDecimal faceamt;

    private BigDecimal fillprice8;

    private BigDecimal haircut8;

    private String haircutterms;

    private BigDecimal indexrate8;

    private Date inputdate;

    private String inputtime;

    private String invtype;

    private String ioper;

    private Short linkassignseq;

    private BigDecimal matprocamt;

    private BigDecimal matprocbaseamt;

    private BigDecimal matprocorigamt;

    private BigDecimal matprocorigbaseamt;

    private Date mdate;

    private Date mdcleardate;

    private String mdclearoper;

    private Date mdfaildate;

    private String mdfailoper;

    private BigDecimal mdmsgqty;

    private BigDecimal mdmsgamt;

    private String notccy;

    private BigDecimal origfaceamt;

    private String port;

    private BigDecimal premamt;

    private BigDecimal prinamt;

    private BigDecimal proceedamt;

    private BigDecimal proceedamthc;

    private String ps;

    private BigDecimal purchintamt;

    private BigDecimal pvmatprocamt;

    private BigDecimal qty;

    private String reinvestind;

    private String reinvestlinkseqno;

    private BigDecimal remamt;

    private BigDecimal remfaceamt;

    private Date revdate;

    private String revoper;

    private String revreason;

    private String revtext;

    private String revtime;

    private Date rposmdate;

    private String secid;

    private String sectypeind;

    private BigDecimal swaprate8;

    private String settccy;

    private Long updatecounter;

    private BigDecimal vatamt;

    private Date vdate;

    private Date vdcleardate;

    private String vdclearoper;

    private Date vdfaildate;

    private String vdfailoper;

    private BigDecimal vdmsgqty;

    private BigDecimal vdmsgamt;

    private Date verdate;

    private String verind;

    private String voper;

    private BigDecimal whtamt;

    private BigDecimal yield8;

    private BigDecimal currfillprice8;

    private BigDecimal prinexchrate8;

    private String prinexchterms;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getDealind() {
		return dealind;
	}

	public void setDealind(String dealind) {
		this.dealind = dealind;
	}

	public String getAssignseq() {
		return assignseq;
	}

	public void setAssignseq(String assignseq) {
		this.assignseq = assignseq;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getAssigntype() {
		return assigntype;
	}

	public void setAssigntype(String assigntype) {
		this.assigntype = assigntype;
	}

	public Date getBrprcindte() {
		return brprcindte;
	}

	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getChargeamt() {
		return chargeamt;
	}

	public void setChargeamt(BigDecimal chargeamt) {
		this.chargeamt = chargeamt;
	}

	public BigDecimal getClprice8() {
		return clprice8;
	}

	public void setClprice8(BigDecimal clprice8) {
		this.clprice8 = clprice8;
	}

	public BigDecimal getComprocamt() {
		return comprocamt;
	}

	public void setComprocamt(BigDecimal comprocamt) {
		this.comprocamt = comprocamt;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getDealtime() {
		return dealtime;
	}

	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}

	public String getDelivtype() {
		return delivtype;
	}

	public void setDelivtype(String delivtype) {
		this.delivtype = delivtype;
	}

	public BigDecimal getDiscamt() {
		return discamt;
	}

	public void setDiscamt(BigDecimal discamt) {
		this.discamt = discamt;
	}

	public BigDecimal getDiscrate8() {
		return discrate8;
	}

	public void setDiscrate8(BigDecimal discrate8) {
		this.discrate8 = discrate8;
	}

	public BigDecimal getDispricerate8() {
		return dispricerate8;
	}

	public void setDispricerate8(BigDecimal dispricerate8) {
		this.dispricerate8 = dispricerate8;
	}

	public String getDispricerateind() {
		return dispricerateind;
	}

	public void setDispricerateind(String dispricerateind) {
		this.dispricerateind = dispricerateind;
	}

	public BigDecimal getDrprice8() {
		return drprice8;
	}

	public void setDrprice8(BigDecimal drprice8) {
		this.drprice8 = drprice8;
	}

	public BigDecimal getExtretqty() {
		return extretqty;
	}

	public void setExtretqty(BigDecimal extretqty) {
		this.extretqty = extretqty;
	}

	public BigDecimal getExtretamt() {
		return extretamt;
	}

	public void setExtretamt(BigDecimal extretamt) {
		this.extretamt = extretamt;
	}

	public BigDecimal getFaceamt() {
		return faceamt;
	}

	public void setFaceamt(BigDecimal faceamt) {
		this.faceamt = faceamt;
	}

	public BigDecimal getFillprice8() {
		return fillprice8;
	}

	public void setFillprice8(BigDecimal fillprice8) {
		this.fillprice8 = fillprice8;
	}

	public BigDecimal getHaircut8() {
		return haircut8;
	}

	public void setHaircut8(BigDecimal haircut8) {
		this.haircut8 = haircut8;
	}

	public String getHaircutterms() {
		return haircutterms;
	}

	public void setHaircutterms(String haircutterms) {
		this.haircutterms = haircutterms;
	}

	public BigDecimal getIndexrate8() {
		return indexrate8;
	}

	public void setIndexrate8(BigDecimal indexrate8) {
		this.indexrate8 = indexrate8;
	}

	public Date getInputdate() {
		return inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public Short getLinkassignseq() {
		return linkassignseq;
	}

	public void setLinkassignseq(Short linkassignseq) {
		this.linkassignseq = linkassignseq;
	}

	public BigDecimal getMatprocamt() {
		return matprocamt;
	}

	public void setMatprocamt(BigDecimal matprocamt) {
		this.matprocamt = matprocamt;
	}

	public BigDecimal getMatprocbaseamt() {
		return matprocbaseamt;
	}

	public void setMatprocbaseamt(BigDecimal matprocbaseamt) {
		this.matprocbaseamt = matprocbaseamt;
	}

	public BigDecimal getMatprocorigamt() {
		return matprocorigamt;
	}

	public void setMatprocorigamt(BigDecimal matprocorigamt) {
		this.matprocorigamt = matprocorigamt;
	}

	public BigDecimal getMatprocorigbaseamt() {
		return matprocorigbaseamt;
	}

	public void setMatprocorigbaseamt(BigDecimal matprocorigbaseamt) {
		this.matprocorigbaseamt = matprocorigbaseamt;
	}

	public Date getMdate() {
		return mdate;
	}

	public void setMdate(Date mdate) {
		this.mdate = mdate;
	}

	public Date getMdcleardate() {
		return mdcleardate;
	}

	public void setMdcleardate(Date mdcleardate) {
		this.mdcleardate = mdcleardate;
	}

	public String getMdclearoper() {
		return mdclearoper;
	}

	public void setMdclearoper(String mdclearoper) {
		this.mdclearoper = mdclearoper;
	}

	public Date getMdfaildate() {
		return mdfaildate;
	}

	public void setMdfaildate(Date mdfaildate) {
		this.mdfaildate = mdfaildate;
	}

	public String getMdfailoper() {
		return mdfailoper;
	}

	public void setMdfailoper(String mdfailoper) {
		this.mdfailoper = mdfailoper;
	}

	public BigDecimal getMdmsgqty() {
		return mdmsgqty;
	}

	public void setMdmsgqty(BigDecimal mdmsgqty) {
		this.mdmsgqty = mdmsgqty;
	}

	public BigDecimal getMdmsgamt() {
		return mdmsgamt;
	}

	public void setMdmsgamt(BigDecimal mdmsgamt) {
		this.mdmsgamt = mdmsgamt;
	}

	public String getNotccy() {
		return notccy;
	}

	public void setNotccy(String notccy) {
		this.notccy = notccy;
	}

	public BigDecimal getOrigfaceamt() {
		return origfaceamt;
	}

	public void setOrigfaceamt(BigDecimal origfaceamt) {
		this.origfaceamt = origfaceamt;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public BigDecimal getPremamt() {
		return premamt;
	}

	public void setPremamt(BigDecimal premamt) {
		this.premamt = premamt;
	}

	public BigDecimal getPrinamt() {
		return prinamt;
	}

	public void setPrinamt(BigDecimal prinamt) {
		this.prinamt = prinamt;
	}

	public BigDecimal getProceedamt() {
		return proceedamt;
	}

	public void setProceedamt(BigDecimal proceedamt) {
		this.proceedamt = proceedamt;
	}

	public BigDecimal getProceedamthc() {
		return proceedamthc;
	}

	public void setProceedamthc(BigDecimal proceedamthc) {
		this.proceedamthc = proceedamthc;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public BigDecimal getPurchintamt() {
		return purchintamt;
	}

	public void setPurchintamt(BigDecimal purchintamt) {
		this.purchintamt = purchintamt;
	}

	public BigDecimal getPvmatprocamt() {
		return pvmatprocamt;
	}

	public void setPvmatprocamt(BigDecimal pvmatprocamt) {
		this.pvmatprocamt = pvmatprocamt;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public String getReinvestind() {
		return reinvestind;
	}

	public void setReinvestind(String reinvestind) {
		this.reinvestind = reinvestind;
	}

	public String getReinvestlinkseqno() {
		return reinvestlinkseqno;
	}

	public void setReinvestlinkseqno(String reinvestlinkseqno) {
		this.reinvestlinkseqno = reinvestlinkseqno;
	}

	public BigDecimal getRemamt() {
		return remamt;
	}

	public void setRemamt(BigDecimal remamt) {
		this.remamt = remamt;
	}

	public BigDecimal getRemfaceamt() {
		return remfaceamt;
	}

	public void setRemfaceamt(BigDecimal remfaceamt) {
		this.remfaceamt = remfaceamt;
	}

	public Date getRevdate() {
		return revdate;
	}

	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}

	public String getRevoper() {
		return revoper;
	}

	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}

	public String getRevreason() {
		return revreason;
	}

	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}

	public String getRevtext() {
		return revtext;
	}

	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}

	public String getRevtime() {
		return revtime;
	}

	public void setRevtime(String revtime) {
		this.revtime = revtime;
	}

	public Date getRposmdate() {
		return rposmdate;
	}

	public void setRposmdate(Date rposmdate) {
		this.rposmdate = rposmdate;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getSectypeind() {
		return sectypeind;
	}

	public void setSectypeind(String sectypeind) {
		this.sectypeind = sectypeind;
	}

	public BigDecimal getSwaprate8() {
		return swaprate8;
	}

	public void setSwaprate8(BigDecimal swaprate8) {
		this.swaprate8 = swaprate8;
	}

	public String getSettccy() {
		return settccy;
	}

	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}

	public Long getUpdatecounter() {
		return updatecounter;
	}

	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}

	public BigDecimal getVatamt() {
		return vatamt;
	}

	public void setVatamt(BigDecimal vatamt) {
		this.vatamt = vatamt;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public Date getVdcleardate() {
		return vdcleardate;
	}

	public void setVdcleardate(Date vdcleardate) {
		this.vdcleardate = vdcleardate;
	}

	public String getVdclearoper() {
		return vdclearoper;
	}

	public void setVdclearoper(String vdclearoper) {
		this.vdclearoper = vdclearoper;
	}

	public Date getVdfaildate() {
		return vdfaildate;
	}

	public void setVdfaildate(Date vdfaildate) {
		this.vdfaildate = vdfaildate;
	}

	public String getVdfailoper() {
		return vdfailoper;
	}

	public void setVdfailoper(String vdfailoper) {
		this.vdfailoper = vdfailoper;
	}

	public BigDecimal getVdmsgqty() {
		return vdmsgqty;
	}

	public void setVdmsgqty(BigDecimal vdmsgqty) {
		this.vdmsgqty = vdmsgqty;
	}

	public BigDecimal getVdmsgamt() {
		return vdmsgamt;
	}

	public void setVdmsgamt(BigDecimal vdmsgamt) {
		this.vdmsgamt = vdmsgamt;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper;
	}

	public BigDecimal getWhtamt() {
		return whtamt;
	}

	public void setWhtamt(BigDecimal whtamt) {
		this.whtamt = whtamt;
	}

	public BigDecimal getYield8() {
		return yield8;
	}

	public void setYield8(BigDecimal yield8) {
		this.yield8 = yield8;
	}

	public BigDecimal getCurrfillprice8() {
		return currfillprice8;
	}

	public void setCurrfillprice8(BigDecimal currfillprice8) {
		this.currfillprice8 = currfillprice8;
	}

	public BigDecimal getPrinexchrate8() {
		return prinexchrate8;
	}

	public void setPrinexchrate8(BigDecimal prinexchrate8) {
		this.prinexchrate8 = prinexchrate8;
	}

	public String getPrinexchterms() {
		return prinexchterms;
	}

	public void setPrinexchterms(String prinexchterms) {
		this.prinexchterms = prinexchterms;
	}
    
    
}