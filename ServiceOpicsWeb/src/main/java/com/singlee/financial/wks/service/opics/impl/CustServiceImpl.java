package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Cust;
import com.singlee.financial.wks.mapper.opics.CustMapper;
import com.singlee.financial.wks.service.opics.CustService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CustServiceImpl implements CustService {

    @Resource
    private CustMapper custMapper;

    @Override
    public int deleteByPrimaryKey(String cno) {
        return custMapper.deleteByPrimaryKey(cno);
    }

    @Override
    public int insert(Cust record) {
        return custMapper.insert(record);
    }

    @Override
    public int insertSelective(Cust record) {
        return custMapper.insertSelective(record);
    }

    @Override
    public Cust selectByPrimaryKey(String cno) {
        return custMapper.selectByPrimaryKey(cno);
    }

    @Override
    public int updateByPrimaryKeySelective(Cust record) {
        return custMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Cust record) {
        return custMapper.updateByPrimaryKey(record);
    }

	@Override
	public Cust selectByCustaltid(String cfetsId) {
		return custMapper.selectByCustaltid(cfetsId);
	}

	@Override
	public Cust selectByCustCmne(String cmne) {
		return custMapper.selectByCustCmne(cmne);
	}

	@Override
	public Cust selectByBr(String br) {
		return custMapper.selectByBr(br);
	}

}
