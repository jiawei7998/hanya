package com.singlee.financial.wks.mapper.opics;


import com.singlee.financial.wks.bean.opics.Seci;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SeciMapper {
    /**
     * delete by primary key
     * @param secid primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("secid") String secid);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Seci record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Seci record);

    /**
     * select by primary key
     * @param secid primary key
     * @return object by primary key
     */
    Seci selectByPrimaryKey(@Param("secid") String secid);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Seci record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Seci record);

    int updateBatch(List<Seci> list);

    int updateBatchSelective(List<Seci> list);
}