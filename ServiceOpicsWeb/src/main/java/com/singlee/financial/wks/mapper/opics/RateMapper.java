package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Rate;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface RateMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("ratecode") String ratecode);

    int insert(Rate record);

    int insertSelective(Rate record);

    Rate selectByPrimaryKey(@Param("br") String br, @Param("ratecode") String ratecode);

    List<Rate> selectByCcy(@Param("br") String br,@Param("ccy") String ccy, @Param("varfix") String varfix);
    
    List<Rate> selectByIsda(@Param("br") String br,@Param("ccy") String ccy, @Param("ratesourceIsda") String ratesourceIsda);

    List<Rate> selectAll();
    
    int updateByPrimaryKeySelective(Rate record);

    int updateByPrimaryKey(Rate record);
    
    /**
     * opics rate data
     * @return
     */
    List<Rate> selectImportData();
}