package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Drpi implements Serializable {
    private String br;

    private String product;

    private String prodtype;

    private String dealno;

    private String seq;

    private String delrecind;

    private String sr1;

    private String sr2;

    private String sr3;

    private String sr4;

    private String sr5;

    private String sr6;

    private String ac1;

    private String ac2;

    private String ac3;

    private String ac4;

    private String ac5;

    private String ac6;

    private String supsecmoveind;

    private String supccymoveind;

    private Date lstmntdate;

    public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getDelrecind() {
		return delrecind;
	}

	public void setDelrecind(String delrecind) {
		this.delrecind = delrecind;
	}

	public String getSr1() {
		return sr1;
	}

	public void setSr1(String sr1) {
		this.sr1 = sr1;
	}

	public String getSr2() {
		return sr2;
	}

	public void setSr2(String sr2) {
		this.sr2 = sr2;
	}

	public String getSr3() {
		return sr3;
	}

	public void setSr3(String sr3) {
		this.sr3 = sr3;
	}

	public String getSr4() {
		return sr4;
	}

	public void setSr4(String sr4) {
		this.sr4 = sr4;
	}

	public String getSr5() {
		return sr5;
	}

	public void setSr5(String sr5) {
		this.sr5 = sr5;
	}

	public String getSr6() {
		return sr6;
	}

	public void setSr6(String sr6) {
		this.sr6 = sr6;
	}

	public String getAc1() {
		return ac1;
	}

	public void setAc1(String ac1) {
		this.ac1 = ac1;
	}

	public String getAc2() {
		return ac2;
	}

	public void setAc2(String ac2) {
		this.ac2 = ac2;
	}

	public String getAc3() {
		return ac3;
	}

	public void setAc3(String ac3) {
		this.ac3 = ac3;
	}

	public String getAc4() {
		return ac4;
	}

	public void setAc4(String ac4) {
		this.ac4 = ac4;
	}

	public String getAc5() {
		return ac5;
	}

	public void setAc5(String ac5) {
		this.ac5 = ac5;
	}

	public String getAc6() {
		return ac6;
	}

	public void setAc6(String ac6) {
		this.ac6 = ac6;
	}

	public String getSupsecmoveind() {
		return supsecmoveind;
	}

	public void setSupsecmoveind(String supsecmoveind) {
		this.supsecmoveind = supsecmoveind;
	}

	public String getSupccymoveind() {
		return supccymoveind;
	}

	public void setSupccymoveind(String supccymoveind) {
		this.supccymoveind = supccymoveind;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	private static final long serialVersionUID = 1L;
}