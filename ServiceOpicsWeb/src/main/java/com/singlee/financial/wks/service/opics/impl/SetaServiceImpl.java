package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Seta;
import com.singlee.financial.wks.mapper.opics.SetaMapper;
import com.singlee.financial.wks.service.opics.SetaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SetaServiceImpl implements SetaService {

    @Resource
    private SetaMapper setaMapper;

    @Override
    public int deleteByPrimaryKey(String br,String smeans,String sacct) {
        return setaMapper.deleteByPrimaryKey(br,smeans,sacct);
    }

    @Override
    public int insert(Seta record) {
        return setaMapper.insert(record);
    }

    @Override
    public int insertSelective(Seta record) {
        return setaMapper.insertSelective(record);
    }

    @Override
    public Seta selectByPrimaryKey(String br,String smeans,String sacct) {
        return setaMapper.selectByPrimaryKey(br,smeans,sacct);
    }
    @Override
	public Seta selectByPrimaryKeyAndCno(String br, String smeans, String sacct, String cno) {
    	return setaMapper.selectByPrimaryKeyAndCno(br,smeans,sacct,cno);
	}

    @Override
    public int updateByPrimaryKeySelective(Seta record) {
        return setaMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Seta record) {
        return setaMapper.updateByPrimaryKey(record);
    }

	@Override
	public Seta selectBySettacct(String br,String sacct) {
		 return setaMapper.selectBySettacct(br,sacct);
	}

	@Override
	public Seta selectBySettmeans(String br, String smeans) {
		return setaMapper.selectBySettmeans(br, smeans);
	}


}
