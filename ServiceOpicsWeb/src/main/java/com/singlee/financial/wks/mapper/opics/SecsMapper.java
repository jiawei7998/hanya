package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Secs;
import java.util.Date;
import org.apache.ibatis.annotations.Param;

public interface SecsMapper {
    int deleteByPrimaryKey(@Param("secid") String secid, @Param("seq") String seq, @Param("ipaydate") Date ipaydate);

    int insert(Secs record);

    int insertSelective(Secs record);

    Secs selectByPrimaryKey(@Param("secid") String secid, @Param("seq") String seq, @Param("ipaydate") Date ipaydate);

    int updateByPrimaryKeySelective(Secs record);

    int updateByPrimaryKey(Secs record);
}