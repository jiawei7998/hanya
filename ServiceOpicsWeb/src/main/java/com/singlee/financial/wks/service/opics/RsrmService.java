package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.marketdata.bean.SubscriptionCode;


public interface RsrmService {
	List<SubscriptionCode> queryByGroupID(String groupId);
}
