package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Rprh;
import com.singlee.financial.wks.mapper.opics.RprhMapper;
import com.singlee.financial.wks.service.opics.RprhService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RprhServiceImpl implements RprhService {

    @Resource
    private RprhMapper rprhMapper;

    @Override
    public int deleteByPrimaryKey(String br,String dealno,String seq) {
        return rprhMapper.deleteByPrimaryKey(br,dealno,seq);
    }

    @Override
    public int insert(Rprh record) {
        return rprhMapper.insert(record);
    }

    @Override
    public int insertSelective(Rprh record) {
        return rprhMapper.insertSelective(record);
    }

    @Override
    public Rprh selectByPrimaryKey(String br,String dealno,String seq) {
        return rprhMapper.selectByPrimaryKey(br,dealno,seq);
    }

    @Override
    public Rprh selectByTradeNo(String br, String dealtext) {
        return rprhMapper.selectByTradeNo(br, dealtext);
    }

    @Override
    public int updateByPrimaryKeySelective(Rprh record) {
        return rprhMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Rprh record) {
        return rprhMapper.updateByPrimaryKey(record);
    }

}
