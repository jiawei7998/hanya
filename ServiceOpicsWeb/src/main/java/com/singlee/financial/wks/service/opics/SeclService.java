package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Secl;import java.util.List;

public interface SeclService {


    int deleteByPrimaryKey(String br, String secid);

    int insert(Secl record);

    int insertSelective(Secl record);

    Secl selectByPrimaryKey(String br, String secid);

    int updateByPrimaryKeySelective(Secl record);

    int updateByPrimaryKey(Secl record);

    int updateBatch(List<Secl> list);

    int updateBatchSelective(List<Secl> list);
}

