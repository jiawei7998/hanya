package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Seci;

import java.util.List;

public interface SeciService{


    int deleteByPrimaryKey(String secid);

    int insert(Seci record);

    int insertSelective(Seci record);

    Seci selectByPrimaryKey(String secid);

    int updateByPrimaryKeySelective(Seci record);

    int updateByPrimaryKey(Seci record);

    int updateBatch(List<Seci> list);

    int updateBatchSelective(List<Seci> list);

}
