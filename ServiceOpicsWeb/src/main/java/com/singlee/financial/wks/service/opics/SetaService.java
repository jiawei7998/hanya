package com.singlee.financial.wks.service.opics;



import com.singlee.financial.wks.bean.opics.Seta;
public interface SetaService{


    int deleteByPrimaryKey(String br,String smeans,String sacct);

    int insert(Seta record);

    int insertSelective(Seta record);

    Seta selectByPrimaryKey(String br,String smeans,String sacct);
    Seta selectByPrimaryKeyAndCno (String br,String smeans,String sacct,String cno);

    int updateByPrimaryKeySelective(Seta record);

    int updateByPrimaryKey(Seta record);
    
    Seta selectBySettacct(String br,String sacct);
    
    Seta selectBySettmeans(String br,String smeans);

}
