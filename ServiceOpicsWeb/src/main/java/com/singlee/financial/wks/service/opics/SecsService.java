package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Secs;
import java.util.Date;
import java.util.List;
public interface SecsService{


    int deleteByPrimaryKey(String secid,String seq,Date ipaydate);

    int insert(Secs record);

    int insertSelective(Secs record);

    Secs selectByPrimaryKey(String secid,String seq,Date ipaydate);

    int updateByPrimaryKeySelective(Secs record);

    int updateByPrimaryKey(Secs record);
    
    void batchInsert(List<Secs> list);

}
