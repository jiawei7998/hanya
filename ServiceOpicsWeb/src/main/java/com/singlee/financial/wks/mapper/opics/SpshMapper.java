package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Spsh;
import org.apache.ibatis.annotations.Param;

public interface SpshMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("fixincind") String fixincind);

    int insert(Spsh record);

    int insertSelective(Spsh record);

    Spsh selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("fixincind") String fixincind);

    int updateByPrimaryKeySelective(Spsh record);

    Spsh selectByTradeNo(@Param("br") String br, @Param("dealtext") String tradeNo);

    int updateByPrimaryKey(Spsh record);
}