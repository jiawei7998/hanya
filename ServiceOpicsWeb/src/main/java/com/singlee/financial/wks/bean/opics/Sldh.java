package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Sldh implements Serializable{
    private static final long serialVersionUID = 1L;
    private String br;

    private String dealno;

    private String seq;

    private String dealind;

    private String prodtype;

    private String product;

    private Date amenddate;

    private String amendtime;

    private String amendoper;

    private BigDecimal assignqty;

    private Date assigndate;

    private String assignind;

    private String basis;

    private String brok;

    private BigDecimal brokfeeamt;

    private String brokfeeccy;

    private Date brprcindte;

    private String ccy;

    private BigDecimal chargeamt;

    private String closeind;

    private String cno;

    private BigDecimal collamt;

    private String collateralcode;

    private String collrequiredind;

    private Date comauthdate;

    private String comauthind;

    private String comauthoper;

    private String comccysacct;

    private String comccysmeans;

    private BigDecimal comprocbaseamt;

    private BigDecimal comprocamt;

    private String confcode;

    private Date confdate;

    private String confoper;

    private BigDecimal convintamt;

    private BigDecimal convintbamt;

    private String cashcost;

    private String coupreinvestind;

    private BigDecimal coupreinvrate8;

    private String custrefno;

    private Date dealdate;

    private String dealscre;

    private String dealtext;

    private String dealtime;

    private String delivtype;

    private Date eminputdate;

    private String emioper;

    private BigDecimal empenamt;

    private String extind;

    private String fedealno;

    private BigDecimal feeamt;

    private String feecalctype;

    private String feeno;

    private BigDecimal feeper8;

    private String initterms;

    private Date inputdate;

    private String inputtime;

    private String ioper;

    private String linkdealno;

    private BigDecimal loanamt;

    private Date lstmntdte;

    private BigDecimal margcallpct8;

    private Date matauthdate;

    private String matauthind;

    private String matauthoper;

    private String matccysacct;

    private String matccysmeans;

    private Date matdate;

    private BigDecimal matprocbaseamt;

    private BigDecimal matprocamt;

    private BigDecimal notfeeamt;

    private String openlendind;

    private String openlendmatdays;

    private Date origmdate;

    private BigDecimal origprocdamt;

    private String overnightind;

    private String overridewxtaxind;

    private String pmvind;

    private String processind;

    private String ps;

    private BigDecimal quotamt;

    private BigDecimal quotqty;

    private String ratecode;

    private Date revdate;

    private String revoper;

    private String revreason;

    private String revtext;

    private String revtime;

    private String safekeepacct;

    private BigDecimal spreadrate8;

    private String substitute;

    private BigDecimal subcashoutst;

    private String suppcashind;

    private String suppconfind;

    private String suppfeeind;

    private String supsecmoveind;

    private String tenor;

    private String trad;

    private BigDecimal uncollatamt;

    private Long updatecounter;

    private BigDecimal vatamt;

    private Date vdate;

    private Date verdate;

    private String verind;

    private String voper;

    private BigDecimal whtamt;

    private String unddealno;

    private String unddealseq;

    private String undprodtype;

    private String undproduct;

    private BigDecimal cashcollatamt;

    private BigDecimal cashcollathcamt;

    private BigDecimal cashhaircut8;

    private String cashhaircutterms;

    private BigDecimal cashinterestrate8;

    private String cashinterestfeeid;

    private String onbehalfcno;

    private String loanreason;

    private String remainingcashind;

    private Date feesettdate;

    private String feepaycycle;

    private String feepayday;

    private String feepayrule;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getDealind() {
		return dealind;
	}

	public void setDealind(String dealind) {
		this.dealind = dealind;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Date getAmenddate() {
		return amenddate;
	}

	public void setAmenddate(Date amenddate) {
		this.amenddate = amenddate;
	}

	public String getAmendtime() {
		return amendtime;
	}

	public void setAmendtime(String amendtime) {
		this.amendtime = amendtime;
	}

	public String getAmendoper() {
		return amendoper;
	}

	public void setAmendoper(String amendoper) {
		this.amendoper = amendoper;
	}

	public BigDecimal getAssignqty() {
		return assignqty;
	}

	public void setAssignqty(BigDecimal assignqty) {
		this.assignqty = assignqty;
	}

	public Date getAssigndate() {
		return assigndate;
	}

	public void setAssigndate(Date assigndate) {
		this.assigndate = assigndate;
	}

	public String getAssignind() {
		return assignind;
	}

	public void setAssignind(String assignind) {
		this.assignind = assignind;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getBrok() {
		return brok;
	}

	public void setBrok(String brok) {
		this.brok = brok;
	}

	public BigDecimal getBrokfeeamt() {
		return brokfeeamt;
	}

	public void setBrokfeeamt(BigDecimal brokfeeamt) {
		this.brokfeeamt = brokfeeamt;
	}

	public String getBrokfeeccy() {
		return brokfeeccy;
	}

	public void setBrokfeeccy(String brokfeeccy) {
		this.brokfeeccy = brokfeeccy;
	}

	public Date getBrprcindte() {
		return brprcindte;
	}

	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getChargeamt() {
		return chargeamt;
	}

	public void setChargeamt(BigDecimal chargeamt) {
		this.chargeamt = chargeamt;
	}

	public String getCloseind() {
		return closeind;
	}

	public void setCloseind(String closeind) {
		this.closeind = closeind;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public BigDecimal getCollamt() {
		return collamt;
	}

	public void setCollamt(BigDecimal collamt) {
		this.collamt = collamt;
	}

	public String getCollateralcode() {
		return collateralcode;
	}

	public void setCollateralcode(String collateralcode) {
		this.collateralcode = collateralcode;
	}

	public String getCollrequiredind() {
		return collrequiredind;
	}

	public void setCollrequiredind(String collrequiredind) {
		this.collrequiredind = collrequiredind;
	}

	public Date getComauthdate() {
		return comauthdate;
	}

	public void setComauthdate(Date comauthdate) {
		this.comauthdate = comauthdate;
	}

	public String getComauthind() {
		return comauthind;
	}

	public void setComauthind(String comauthind) {
		this.comauthind = comauthind;
	}

	public String getComauthoper() {
		return comauthoper;
	}

	public void setComauthoper(String comauthoper) {
		this.comauthoper = comauthoper;
	}

	public String getComccysacct() {
		return comccysacct;
	}

	public void setComccysacct(String comccysacct) {
		this.comccysacct = comccysacct;
	}

	public String getComccysmeans() {
		return comccysmeans;
	}

	public void setComccysmeans(String comccysmeans) {
		this.comccysmeans = comccysmeans;
	}

	public BigDecimal getComprocbaseamt() {
		return comprocbaseamt;
	}

	public void setComprocbaseamt(BigDecimal comprocbaseamt) {
		this.comprocbaseamt = comprocbaseamt;
	}

	public BigDecimal getComprocamt() {
		return comprocamt;
	}

	public void setComprocamt(BigDecimal comprocamt) {
		this.comprocamt = comprocamt;
	}

	public String getConfcode() {
		return confcode;
	}

	public void setConfcode(String confcode) {
		this.confcode = confcode;
	}

	public Date getConfdate() {
		return confdate;
	}

	public void setConfdate(Date confdate) {
		this.confdate = confdate;
	}

	public String getConfoper() {
		return confoper;
	}

	public void setConfoper(String confoper) {
		this.confoper = confoper;
	}

	public BigDecimal getConvintamt() {
		return convintamt;
	}

	public void setConvintamt(BigDecimal convintamt) {
		this.convintamt = convintamt;
	}

	public BigDecimal getConvintbamt() {
		return convintbamt;
	}

	public void setConvintbamt(BigDecimal convintbamt) {
		this.convintbamt = convintbamt;
	}

	public String getCashcost() {
		return cashcost;
	}

	public void setCashcost(String cashcost) {
		this.cashcost = cashcost;
	}

	public String getCoupreinvestind() {
		return coupreinvestind;
	}

	public void setCoupreinvestind(String coupreinvestind) {
		this.coupreinvestind = coupreinvestind;
	}

	public BigDecimal getCoupreinvrate8() {
		return coupreinvrate8;
	}

	public void setCoupreinvrate8(BigDecimal coupreinvrate8) {
		this.coupreinvrate8 = coupreinvrate8;
	}

	public String getCustrefno() {
		return custrefno;
	}

	public void setCustrefno(String custrefno) {
		this.custrefno = custrefno;
	}

	public Date getDealdate() {
		return dealdate;
	}

	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}

	public String getDealscre() {
		return dealscre;
	}

	public void setDealscre(String dealscre) {
		this.dealscre = dealscre;
	}

	public String getDealtext() {
		return dealtext;
	}

	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}

	public String getDealtime() {
		return dealtime;
	}

	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}

	public String getDelivtype() {
		return delivtype;
	}

	public void setDelivtype(String delivtype) {
		this.delivtype = delivtype;
	}

	public Date getEminputdate() {
		return eminputdate;
	}

	public void setEminputdate(Date eminputdate) {
		this.eminputdate = eminputdate;
	}

	public String getEmioper() {
		return emioper;
	}

	public void setEmioper(String emioper) {
		this.emioper = emioper;
	}

	public BigDecimal getEmpenamt() {
		return empenamt;
	}

	public void setEmpenamt(BigDecimal empenamt) {
		this.empenamt = empenamt;
	}

	public String getExtind() {
		return extind;
	}

	public void setExtind(String extind) {
		this.extind = extind;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public BigDecimal getFeeamt() {
		return feeamt;
	}

	public void setFeeamt(BigDecimal feeamt) {
		this.feeamt = feeamt;
	}

	public String getFeecalctype() {
		return feecalctype;
	}

	public void setFeecalctype(String feecalctype) {
		this.feecalctype = feecalctype;
	}

	public String getFeeno() {
		return feeno;
	}

	public void setFeeno(String feeno) {
		this.feeno = feeno;
	}

	public BigDecimal getFeeper8() {
		return feeper8;
	}

	public void setFeeper8(BigDecimal feeper8) {
		this.feeper8 = feeper8;
	}

	public String getInitterms() {
		return initterms;
	}

	public void setInitterms(String initterms) {
		this.initterms = initterms;
	}

	public Date getInputdate() {
		return inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getLinkdealno() {
		return linkdealno;
	}

	public void setLinkdealno(String linkdealno) {
		this.linkdealno = linkdealno;
	}

	public BigDecimal getLoanamt() {
		return loanamt;
	}

	public void setLoanamt(BigDecimal loanamt) {
		this.loanamt = loanamt;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public BigDecimal getMargcallpct8() {
		return margcallpct8;
	}

	public void setMargcallpct8(BigDecimal margcallpct8) {
		this.margcallpct8 = margcallpct8;
	}

	public Date getMatauthdate() {
		return matauthdate;
	}

	public void setMatauthdate(Date matauthdate) {
		this.matauthdate = matauthdate;
	}

	public String getMatauthind() {
		return matauthind;
	}

	public void setMatauthind(String matauthind) {
		this.matauthind = matauthind;
	}

	public String getMatauthoper() {
		return matauthoper;
	}

	public void setMatauthoper(String matauthoper) {
		this.matauthoper = matauthoper;
	}

	public String getMatccysacct() {
		return matccysacct;
	}

	public void setMatccysacct(String matccysacct) {
		this.matccysacct = matccysacct;
	}

	public String getMatccysmeans() {
		return matccysmeans;
	}

	public void setMatccysmeans(String matccysmeans) {
		this.matccysmeans = matccysmeans;
	}

	public Date getMatdate() {
		return matdate;
	}

	public void setMatdate(Date matdate) {
		this.matdate = matdate;
	}

	public BigDecimal getMatprocbaseamt() {
		return matprocbaseamt;
	}

	public void setMatprocbaseamt(BigDecimal matprocbaseamt) {
		this.matprocbaseamt = matprocbaseamt;
	}

	public BigDecimal getMatprocamt() {
		return matprocamt;
	}

	public void setMatprocamt(BigDecimal matprocamt) {
		this.matprocamt = matprocamt;
	}

	public BigDecimal getNotfeeamt() {
		return notfeeamt;
	}

	public void setNotfeeamt(BigDecimal notfeeamt) {
		this.notfeeamt = notfeeamt;
	}

	public String getOpenlendind() {
		return openlendind;
	}

	public void setOpenlendind(String openlendind) {
		this.openlendind = openlendind;
	}

	public String getOpenlendmatdays() {
		return openlendmatdays;
	}

	public void setOpenlendmatdays(String openlendmatdays) {
		this.openlendmatdays = openlendmatdays;
	}

	public Date getOrigmdate() {
		return origmdate;
	}

	public void setOrigmdate(Date origmdate) {
		this.origmdate = origmdate;
	}

	public BigDecimal getOrigprocdamt() {
		return origprocdamt;
	}

	public void setOrigprocdamt(BigDecimal origprocdamt) {
		this.origprocdamt = origprocdamt;
	}

	public String getOvernightind() {
		return overnightind;
	}

	public void setOvernightind(String overnightind) {
		this.overnightind = overnightind;
	}

	public String getOverridewxtaxind() {
		return overridewxtaxind;
	}

	public void setOverridewxtaxind(String overridewxtaxind) {
		this.overridewxtaxind = overridewxtaxind;
	}

	public String getPmvind() {
		return pmvind;
	}

	public void setPmvind(String pmvind) {
		this.pmvind = pmvind;
	}

	public String getProcessind() {
		return processind;
	}

	public void setProcessind(String processind) {
		this.processind = processind;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public BigDecimal getQuotamt() {
		return quotamt;
	}

	public void setQuotamt(BigDecimal quotamt) {
		this.quotamt = quotamt;
	}

	public BigDecimal getQuotqty() {
		return quotqty;
	}

	public void setQuotqty(BigDecimal quotqty) {
		this.quotqty = quotqty;
	}

	public String getRatecode() {
		return ratecode;
	}

	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}

	public Date getRevdate() {
		return revdate;
	}

	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}

	public String getRevoper() {
		return revoper;
	}

	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}

	public String getRevreason() {
		return revreason;
	}

	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}

	public String getRevtext() {
		return revtext;
	}

	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}

	public String getRevtime() {
		return revtime;
	}

	public void setRevtime(String revtime) {
		this.revtime = revtime;
	}

	public String getSafekeepacct() {
		return safekeepacct;
	}

	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}

	public BigDecimal getSpreadrate8() {
		return spreadrate8;
	}

	public void setSpreadrate8(BigDecimal spreadrate8) {
		this.spreadrate8 = spreadrate8;
	}

	public String getSubstitute() {
		return substitute;
	}

	public void setSubstitute(String substitute) {
		this.substitute = substitute;
	}

	public BigDecimal getSubcashoutst() {
		return subcashoutst;
	}

	public void setSubcashoutst(BigDecimal subcashoutst) {
		this.subcashoutst = subcashoutst;
	}

	public String getSuppcashind() {
		return suppcashind;
	}

	public void setSuppcashind(String suppcashind) {
		this.suppcashind = suppcashind;
	}

	public String getSuppconfind() {
		return suppconfind;
	}

	public void setSuppconfind(String suppconfind) {
		this.suppconfind = suppconfind;
	}

	public String getSuppfeeind() {
		return suppfeeind;
	}

	public void setSuppfeeind(String suppfeeind) {
		this.suppfeeind = suppfeeind;
	}

	public String getSupsecmoveind() {
		return supsecmoveind;
	}

	public void setSupsecmoveind(String supsecmoveind) {
		this.supsecmoveind = supsecmoveind;
	}

	public String getTenor() {
		return tenor;
	}

	public void setTenor(String tenor) {
		this.tenor = tenor;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public BigDecimal getUncollatamt() {
		return uncollatamt;
	}

	public void setUncollatamt(BigDecimal uncollatamt) {
		this.uncollatamt = uncollatamt;
	}

	public Long getUpdatecounter() {
		return updatecounter;
	}

	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}

	public BigDecimal getVatamt() {
		return vatamt;
	}

	public void setVatamt(BigDecimal vatamt) {
		this.vatamt = vatamt;
	}

	public Date getVdate() {
		return vdate;
	}

	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper;
	}

	public BigDecimal getWhtamt() {
		return whtamt;
	}

	public void setWhtamt(BigDecimal whtamt) {
		this.whtamt = whtamt;
	}

	public String getUnddealno() {
		return unddealno;
	}

	public void setUnddealno(String unddealno) {
		this.unddealno = unddealno;
	}

	public String getUnddealseq() {
		return unddealseq;
	}

	public void setUnddealseq(String unddealseq) {
		this.unddealseq = unddealseq;
	}

	public String getUndprodtype() {
		return undprodtype;
	}

	public void setUndprodtype(String undprodtype) {
		this.undprodtype = undprodtype;
	}

	public String getUndproduct() {
		return undproduct;
	}

	public void setUndproduct(String undproduct) {
		this.undproduct = undproduct;
	}

	public BigDecimal getCashcollatamt() {
		return cashcollatamt;
	}

	public void setCashcollatamt(BigDecimal cashcollatamt) {
		this.cashcollatamt = cashcollatamt;
	}

	public BigDecimal getCashcollathcamt() {
		return cashcollathcamt;
	}

	public void setCashcollathcamt(BigDecimal cashcollathcamt) {
		this.cashcollathcamt = cashcollathcamt;
	}

	public BigDecimal getCashhaircut8() {
		return cashhaircut8;
	}

	public void setCashhaircut8(BigDecimal cashhaircut8) {
		this.cashhaircut8 = cashhaircut8;
	}

	public String getCashhaircutterms() {
		return cashhaircutterms;
	}

	public void setCashhaircutterms(String cashhaircutterms) {
		this.cashhaircutterms = cashhaircutterms;
	}

	public BigDecimal getCashinterestrate8() {
		return cashinterestrate8;
	}

	public void setCashinterestrate8(BigDecimal cashinterestrate8) {
		this.cashinterestrate8 = cashinterestrate8;
	}

	public String getCashinterestfeeid() {
		return cashinterestfeeid;
	}

	public void setCashinterestfeeid(String cashinterestfeeid) {
		this.cashinterestfeeid = cashinterestfeeid;
	}

	public String getOnbehalfcno() {
		return onbehalfcno;
	}

	public void setOnbehalfcno(String onbehalfcno) {
		this.onbehalfcno = onbehalfcno;
	}

	public String getLoanreason() {
		return loanreason;
	}

	public void setLoanreason(String loanreason) {
		this.loanreason = loanreason;
	}

	public String getRemainingcashind() {
		return remainingcashind;
	}

	public void setRemainingcashind(String remainingcashind) {
		this.remainingcashind = remainingcashind;
	}

	public Date getFeesettdate() {
		return feesettdate;
	}

	public void setFeesettdate(Date feesettdate) {
		this.feesettdate = feesettdate;
	}

	public String getFeepaycycle() {
		return feepaycycle;
	}

	public void setFeepaycycle(String feepaycycle) {
		this.feepaycycle = feepaycycle;
	}

	public String getFeepayday() {
		return feepayday;
	}

	public void setFeepayday(String feepayday) {
		this.feepayday = feepayday;
	}

	public String getFeepayrule() {
		return feepayrule;
	}

	public void setFeepayrule(String feepayrule) {
		this.feepayrule = feepayrule;
	}
    
    
}