package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.CustWksBean;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.FieldMapType;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.intfc.CustomService;
import com.singlee.financial.wks.service.opics.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CustomServiceImpl implements CustomService {

	@Autowired
	CustService custService;
	@Autowired
	BicoService bicoService;
	@Autowired
	SicoService sicoService;
	@Autowired
	CounService counService;
	@Autowired
	ActyService actyService;
	@Autowired
	private AdnmService adnmService;
	@Autowired
	private CusiService cusiService;


	private Logger logger = Logger.getLogger(CustomService.class);

	@Override
	public SlOutBean saveEntry(CustWksBean custWksBean) {
		Cust cust = null;
		Adnm adnm = null;
		String cno = null;
		Cust cust_cmne = null;

		logger.debug("交易对手信息导入参数对象:" + custWksBean);

		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		// 检查传入对象
		check(custWksBean);
		try {
			// 初始化对象
//			cust = custService.selectByCustaltid(StringUtils.upperCase(custWksBean.getCustId()));
			cust = custService.selectByPrimaryKey(StringUtils.upperCase(custWksBean.getCustId()));
			cust_cmne = custService.selectByCustCmne(StringUtils.upperCase(custWksBean.getCustCode()));

			logger.debug("交易对手信息:" + cust);
			logger.debug("交易对手信息CNME:" + cust_cmne);

			if (null == cust && null != cust_cmne) {
				JY.raise("交易对手简称[" + custWksBean.getCustCode() + "]在系统中已存在");
			}
			if (null != cust && (null == cust_cmne || (null != cust_cmne && !StringUtils.trim(cust_cmne.getCmne()).equalsIgnoreCase(StringUtils.trim(cust.getCmne()))))) {
				throw new RuntimeException("交易对手[" + custWksBean.getCustId() + "]在系统中已存在,OPICS系统中维护的英文简称是["+StringUtils.trim(cust.getCmne())+"]");
			}

			// 添加BICO信息
			insertBico(custWksBean);

			if (null == cust && null == cust_cmne) {
				// 新增交易对手
				cust = createCust(new Cust(), custWksBean);
//				adnm = adnmService.selectByPrimaryKey("00","C","CUSTNO","CN");// 获取DEALNO
//				if (null == adnm) {// 不存在则进行初始化
//					adnm =new Adnm();
//					adnmService.insert(createAdnm(adnm,"00","C","CUSTNO","CN","1000000000",""));
//				}
//				cno = Integer.parseInt(StringUtils.trimToEmpty(adnm.getFonumber())) + 1 + "";
//				cust.setCno(cno);

				// 插入交易对手到OPICS系统
				custService.insert(cust);

				// 插入CFETS与OPICS交易对手映射关系
//				cusiService.insert(createCusi(cno, custWksBean.getCustId()));
				
				// 更新CUST CNO
//				adnm.setFonumber(cno);
//				adnmService.updateByPrimaryKeySelective(adnm);

				outBean.setRetMsg("交易对手信息新增成功");
			} else {
				// 修改交易对手信息,交易对手会计类型,英文简称缩写不允许修改
				cno = cust.getCno();
				if (!cust.getAcctngtype().equalsIgnoreCase(custWksBean.getAcctngtype())) {
					throw new RuntimeException("交易对手类型不允许更改");
				}

				cust = createCust(cust, custWksBean);
				custService.updateByPrimaryKeySelective(cust);

				outBean.setRetMsg("交易对手信息更新成功");
			}
			outBean.setClientNo(cno);
			outBean.setTellSeqNo(custWksBean.getCustId());
			outBean.setRetStatus(RetStatusEnum.S);
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("交易对手录入错误", e);
			throw e;
		}
		logger.info("交易对手处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	private void insertBico(CustWksBean custWksBean) {
		Bico bico = null;
		// 判断bic码是否存在
		bico = bicoService.selectByPrimaryKey(StringUtils.upperCase(custWksBean.getSwiftCode()));
		if (null == bico) {// 不存在则以外部为准
			bico = new Bico();
			bico.setBic(StringUtils.upperCase(custWksBean.getSwiftCode()));
			bico.setSn(custWksBean.getCustCode());
			bico.setBa1(custWksBean.getCustEshortname());
			bico.setBa2(custWksBean.getCustArea());
			bico.setLstmntdte(Calendar.getInstance().getTime());
			bicoService.insert(bico);
			logger.debug("创建SWIFTCODE信息:" + bico);
		} // end if
	}

	private Cust createCust(Cust cust, CustWksBean custWksBean) {

//		cust.setCno(cust.getCno());// 对手编码
		cust.setCno(custWksBean.getCustId());// 对手编码
		// 简称转换为大写字母
		cust.setCmne(StringUtils.upperCase(custWksBean.getCustCode()));// 对手简称
		cust.setBic(StringUtils.upperCase(custWksBean.getSwiftCode()));// swift Code
		cust.setCcode(custWksBean.getCustArea());// 地域代码
		cust.setSic(custWksBean.getCustSmallKind());// 工业代码
		cust.setSn(custWksBean.getCustEshortname());// 简称
		cust.setCfn1(StringUtils.substring(custWksBean.getCustEfullname(), 0, 35));// 公司全称
		cust.setCfn2(StringUtils.substring(custWksBean.getCustEfullname(), 35, 70));// 公司全称
		cust.setCa4(custWksBean.getCustArea());// 国家代码
		cust.setCtype(custWksBean.getCustCategory());// 客户类型
		cust.setUccode(custWksBean.getCustCountry());// 国家代码
		cust.setAcctngtype(custWksBean.getAcctngtype());// 会计类型
		cust.setLstmntdte(Calendar.getInstance().getTime());// 更新时间
		cust.setCcode(custWksBean.getCustCountry());

		cust.setCa1(StringUtils.substring(custWksBean.getCustAddress(), 0, 35));// 公司地址
		cust.setCa2(StringUtils.substring(custWksBean.getCustAddress(), 35, 70));// 公司地址
		cust.setCa3(StringUtils.substring(custWksBean.getCustAddress(), 70, 105));// 公司地址
		
		cust.setCa5("");
		cust.setCpost("");
		cust.setTaxid("");

		logger.debug("创建交易对手信息:" + cust);

		return cust;
	}

	private Cusi createCusi(String cno, String custAltId) {
		Cusi cusi = new Cusi();
		cusi.setCno(cno);// 对手编码
		cusi.setCustidtype(FieldMapType.CFETS.name());
		cusi.setCustaltid(StringUtils.upperCase(custAltId));
		logger.debug(String.format("创建交易对手映射信息,CNO=%s,CUSTIDTYPE=%s,CUSTALTID=%s", cusi.getCno(), cusi.getCustidtype(),
				cusi.getCustaltid()));
		return cusi;
	}
	
	private Adnm createAdnm(Adnm adnm,String br,String datatype,String product,String prodtype,String fonumber, String bonumber) {
		adnm.setBr(br);
		adnm.setDatatype(datatype);
		adnm.setProduct(product);
		adnm.setProdtype(prodtype);
		adnm.setFonumber(fonumber);
		adnm.setBonumber(bonumber);
		adnm.setLstmntdate(DateUtil.parse(DateUtil.getCurrentDateAsString(), "yyyy-MM-dd"));
		return adnm;
	}

	private void check(CustWksBean custWksBean) {
		boolean bool = false;
		Coun coun = null;
		Sico sico = null;
		Acty acty = null;

		// 判断
		if (null == custWksBean) {
			JY.raise("%s:%s,请补充CustWksBean对象", WksErrorCode.CUST_NULL.getErrCode(), WksErrorCode.CUST_NULL.getErrMsg());
		}
		// 判断交易对手不能为空并且是数字
		if (StringUtils.isEmpty(custWksBean.getCustId()) || StringUtils.length(custWksBean.getCustId()) > 10) {//
			JY.raise("%s:%s,请填写CustWksBean.Cno,For.[Cno=1234567890]", WksErrorCode.CUST_CNO_NUMERIC.getErrCode(),
					WksErrorCode.CUST_CNO_NUMERIC.getErrMsg());
		}
		// 判断代码简称不能为空
		if (StringUtils.isEmpty(custWksBean.getCustCode()) || StringUtils.length(custWksBean.getCustCode()) > 10) {
			JY.raise("%s:%s,请填写CustWksBean.CustCode,For.[CustCode=ABCI]", WksErrorCode.CUST_CMNE_UPPER.getErrCode(),
					WksErrorCode.CUST_CMNE_UPPER.getErrMsg());
		}

		// 判断CCODE是否存在COUN表
		if (StringUtils.isEmpty(custWksBean.getCustCountry()) || StringUtils.length(custWksBean.getCustCountry()) > 2) {
			JY.raise("%s:%s,请填写CustWksBean.CustCountry,For.[CustCountry=CN]", WksErrorCode.CUST_CCODE_NULL.getErrCode(),
					WksErrorCode.CUST_CCODE_NULL.getErrMsg());
		}
		coun = counService.selectByPrimaryKey(custWksBean.getCustCountry());
		if (null == coun) {
			JY.raise("%s:%s,请填写CustWksBean.SwiftCode,For.[getCustArea=CN]",
					WksErrorCode.CUST_CCODE_NOT_FOUND.getErrCode(), WksErrorCode.CUST_CCODE_NOT_FOUND.getErrMsg());
		}
		// 判断SIC码是否存在
		if (StringUtils.isEmpty(custWksBean.getCustSmallKind())
				|| StringUtils.length(custWksBean.getCustSmallKind()) > 10) {
			JY.raise("%s:%s,请填写CustWksBean.CustSmallKind,For.[CustSmallKind=1204]",
					WksErrorCode.CUST_SIC_NULL.getErrCode(), WksErrorCode.CUST_SIC_NULL.getErrMsg());
		}
		sico = sicoService.selectByPrimaryKey(custWksBean.getCustSmallKind());
		if (null == sico) {
			JY.raise("%s:%s,请填写CustWksBean.CustSmallKind,For.[CustSmallKind=BANK-COM]",
					WksErrorCode.CUST_SIC_NOT_FOUND.getErrCode(), WksErrorCode.CUST_SIC_NOT_FOUND.getErrMsg());
		}
		// 判断简称是否存在
		if (StringUtils.isEmpty(custWksBean.getCustEshortname())
				|| StringUtils.length(custWksBean.getCustEshortname()) > 40) {
			JY.raise("%s:%s,请填写CustWksBean.CustShortname,For.[CustShortname=AGRICULTURAL BANK OF CHINA]",
					WksErrorCode.CUST_SN_NULL.getErrCode(), WksErrorCode.CUST_SN_NULL.getErrMsg());
		}
		// 判断对手全称
		if (StringUtils.isEmpty(custWksBean.getCustEfullname())
				|| StringUtils.length(custWksBean.getCustEshortname()) > 70) {
			JY.raise("%s:%s,请填写CustWksBean.CustEfullname,For.[CustEfullname=ZHONG GOU YING HANG LIMITED COMPANY]",
					WksErrorCode.CUST_CFN_NULL.getErrCode(), WksErrorCode.CUST_CFN_NULL.getErrMsg());
		}
//		// 判断对手地址
//		if (StringUtils.isEmpty(custWksBean.getCustAddress()) || StringUtils.length(custWksBean.getCustAddress()) > 100) {
//			JY.raise("%s:%s,请填写CustWksBean.CustAddress,For.[CustAddress=BEIJING JIN RONG DAJIE 56 STREET]", WksErrorCode.CUST_CFN_NULL.getErrCode(), WksErrorCode.CUST_CFN_NULL.getErrMsg());
//		}
		// 判断对手所在城市
//		if (StringUtils.isEmpty(custWksBean.getCustArea())) {
//			JY.raise("%s:%s,请填写CustWksBean.CustArea,For.[CustArea=BEI JING]", WksErrorCode.CUST_CA4_NULL.getErrCode(),
//					WksErrorCode.CUST_CA4_NULL.getErrMsg());
//		}
		// 客户类型
		if (StringUtils.isEmpty(custWksBean.getCustCategory())
				|| StringUtils.length(custWksBean.getCustCategory()) > 1) {
			JY.raise("%s:%s,请填写CustWksBean.CustCategory,For.[CustCategory=B]",
					WksErrorCode.CUST_UCCODE_NULL.getErrCode(), WksErrorCode.CUST_UCCODE_NULL.getErrMsg());
		}
		// 判断会计类型
		if (StringUtils.isEmpty(custWksBean.getAcctngtype()) || StringUtils.length(custWksBean.getAcctngtype()) > 10) {
			JY.raise("%s:%s,请填写CustWksBean.Acctngtype,For.[Acctngtype=D-CCOM-BNK]",
					WksErrorCode.CUST_ACTNGTYPE_NULL.getErrCode(), WksErrorCode.CUST_ACTNGTYPE_NULL.getErrMsg());
		}
		acty = actyService.selectByPrimaryKey(custWksBean.getAcctngtype());
		if (null == acty) {
			JY.raise("%s:%s,请填写CustWksBean.Acctngtype,For.[Acctngtype=D-CCOM-BNK]",
					WksErrorCode.CUST_ACTNGTYPE_NOT_FOUND.getErrCode(),
					WksErrorCode.CUST_ACTNGTYPE_NOT_FOUND.getErrMsg());
		} // end if

		bool = OpicsConstant.CUST.CUST_TYPE.contains(custWksBean.getCustCategory().toUpperCase());
		if (!bool) {
			JY.raise("%s:%s,请填写CustWksBean.CustCategory,For.[CustCategory=B]",
					WksErrorCode.CUST_TYPE_NOT_FOUND.getErrCode(), WksErrorCode.CUST_TYPE_NOT_FOUND.getErrMsg());
		}

		// 判断swift code 不为空并且最大11位
		if ((StringUtils.isEmpty(custWksBean.getSwiftCode())
				&& custWksBean.getCustCategory().equalsIgnoreCase(OpicsConstant.CUST.CUST_TYPE.get(0)))
				|| StringUtils.length(custWksBean.getSwiftCode()) > 11) {
			JY.raise("%s:%s,请填写CustWksBean.SwiftCode,For.[SwiftCode=ADBNCNBJ]", WksErrorCode.CUST_BIC_NULL.getErrCode(),
					WksErrorCode.CUST_BIC_NULL.getErrMsg());
		}
	}

}
