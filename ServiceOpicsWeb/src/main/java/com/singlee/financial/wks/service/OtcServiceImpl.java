package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.OtcWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.intfc.OtcService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.OtcEntry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 期权模块服务实现类
 *
 * @author shenzl
 * @date 2020/02/10
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class OtcServiceImpl implements OtcService {

    @Autowired
    private OtdtService otdtService;
    @Autowired
    private OtcpService otcpService;
    @Autowired
    private McfpService mcfpService;
    @Autowired
    private PcixService pcixService;
    @Autowired
    private PrirService prirService;
    @Autowired
    private PsixService psixService;
    @Autowired
    private CnfqService cnfqService;
    @Autowired
    private NupdService nupdService;
    @Autowired
    private PmtqService pmtqSerice;
    @Autowired
    private BrpsService brpsService;
    @Autowired
    private RevpService revpService;
    @Autowired
    private OtcEntry otcEntry;
    private static Logger logger = Logger.getLogger(OtcServiceImpl.class);

    @Override
    public SlOutBean saveEntry(OtcWksBean otc) {
        SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "IBO-00000", "外汇期权录入成功!");
        Otdt otdt = null; // 期权交易对手
        Otcp otcp = null; // 期权定价对象
        try {
            // 1.交易要素检查
            otcEntry.DataCheck(otc);
            // 2.交易主体数据转换
            otdt = otcEntry.DataTrans(new Otdt());// 静态参数转换
            otcEntry.DataTrans(otdt, mcfpService);// 交易编号
            otcEntry.DataTrans(otdt, brpsService);// 设置账务日期
            otcEntry.DataTrans(otdt, otc, revpService);// 设置交易信息
            otdtService.insert(otdt);// 保存信息
            // 3.转换定价数据
            otcp = otcEntry.DataTrans(new Otcp());
            otcEntry.DataTrans(otdt, otcp);
            otcpService.insert(otcp);
            // 4.往来帐信息
            List<Nupd> nupdLst = nupdService.builder(otdt);
            if (nupdLst.size() > 0) {
                nupdService.batchInsert(nupdLst);
            }
            // 5.收付款信息
            Pmtq pmtq = pmtqSerice.builder(otdt);
            pmtqSerice.insert(pmtq);
            // 6.对手方清算信息
            List<Pcix> pcix = pcixService.builder(otdt, otc.getSettleAcct());
            pcixService.batchInsert(pcix);
            // 7.收款信息
            prirService.insert(prirService.builder(otdt));
            // 8.付款信息
            psixService.insert(psixService.builder(otdt));
            // 9.确认信息
            cnfqService.insert(cnfqService.bulider(otdt));
            // 10.回填dealno
            otcEntry.DataTrans(otdt, mcfpService, new Mcfp());
        } catch (Exception e) {
            outBean.setRetStatus(RetStatusEnum.F);
            outBean.setRetMsg(e.getMessage());
            logger.error("外汇期权交易录入错误", e);
            throw e;
        }
        outBean.setClientNo(otdt.getDealno());
        logger.info("外汇期权交易处理完成,返回消息:" + JSON.toJSONString(outBean));
        return outBean;
    }

    @Override
    public SlOutBean exercise(OtcWksBean otc) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public SlOutBean reverseTrading(OtcWksBean otc) {
        // TODO Auto-generated method stub
        return null;
    }

}
