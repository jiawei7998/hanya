package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Csri implements Serializable {
    private String br;

    private String cust;

    private String product;

    private String type;

    private String ccy;

    private String smeans;

    private String sacct;

    private String bic;

    private String c1;

    private String c2;

    private String c3;

    private String c4;

    private Date lstmntdte;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getCust() {
		return cust;
	}


	public void setCust(String cust) {
		this.cust = cust;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getSmeans() {
		return smeans;
	}


	public void setSmeans(String smeans) {
		this.smeans = smeans;
	}


	public String getSacct() {
		return sacct;
	}


	public void setSacct(String sacct) {
		this.sacct = sacct;
	}


	public String getBic() {
		return bic;
	}


	public void setBic(String bic) {
		this.bic = bic;
	}


	public String getC1() {
		return c1;
	}


	public void setC1(String c1) {
		this.c1 = c1;
	}


	public String getC2() {
		return c2;
	}


	public void setC2(String c2) {
		this.c2 = c2;
	}


	public String getC3() {
		return c3;
	}


	public void setC3(String c3) {
		this.c3 = c3;
	}


	public String getC4() {
		return c4;
	}


	public void setC4(String c4) {
		this.c4 = c4;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	private static final long serialVersionUID = 1L;
}