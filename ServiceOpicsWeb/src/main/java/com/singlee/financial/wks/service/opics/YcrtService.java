package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Ycrt;

import java.util.List;

public interface YcrtService{


    int deleteByPrimaryKey(String br,String ccy,String contribrate,String mtystart,String mtyend);

    int insert(Ycrt record);

    int insertSelective(Ycrt record);

    Ycrt selectByPrimaryKey(String br,String ccy,String contribrate,String mtystart,String mtyend);

    int updateByPrimaryKeySelective(Ycrt record);

    int updateByPrimaryKey(Ycrt record);

    List<Ycrt> selectByAll(Ycrt record);

}
