package com.singlee.financial.wks.service.trans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.IboWksBean;
import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.bean.opics.Schd;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.DldtService;

/**
 * 本金调整转换方法
 *
 * @author shenzl
 * @date 2020/02/19
 */
@Component
public class MmAdaptPrin {

	@Autowired
	private DldtService dldtService;

	public void DataCheck(IboWksBean ibo) {
		// 1.判断拆借交易对象是否存在
		if (null == ibo) {
			JY.raise("%s:%s,请补充完整IboWksBean对象", WksErrorCode.IBO_NULL.getErrCode(), WksErrorCode.IBO_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(ibo.getInstId())) {
			JY.raise("%s:%s,请填写IboWksBean.InstId,For.[InstId=01]", WksErrorCode.PARAM_NULL.getErrCode(),
					WksErrorCode.PARAM_NULL.getErrMsg());
		}
		// 2.判断提前终止日期是否存在
		if (StringUtils.isEmpty(ibo.getEmdate())) {
			JY.raise("%s:%s,请填写IboWksBean.Emdate,For.[Emdate=2020-01-01]", WksErrorCode.PARAM_NULL.getErrCode(),
					WksErrorCode.PARAM_NULL.getErrMsg());
		}
		// 3.判断业务编号是否存在
		Dldt dldt = dldtService.selectByTradeNo(ibo.getInstId(), ibo.getTradeNo());
		if (StringUtils.isEmpty(ibo.getTradeNo()) || null == dldt) {
			JY.raise("%s:%s,请填写IboWksBean.TradeNo,For.[TradeNo=TRD20200101000001]",
					WksErrorCode.IBO_NOT_FOUND.getErrCode(), WksErrorCode.IBO_NOT_FOUND.getErrMsg());
		}
	}

	/**
	 * 本金调整交易数据
	 *
	 * @param dldt
	 * @param ibo
	 * @return
	 */
	public Dldt DataTrans(Dldt dldt, IboWksBean ibo) {
		dldt.setCcyamt(ibo.getCcyamt());// 调整后的
		dldt.setLstmntdate(Calendar.getInstance().getTime());// 最后修改日期
		dldt.setUpdatecounter(dldt.getUpdatecounter() + 1);// 修改次数
		return dldt;
	}

	/**
	 * SCHD本金调整交易数据
	 *
	 * @param schd
	 * @param dldt
	 * @param ibo
	 * @return
	 */
	public List<Schd> DataTrans(List<Schd> schdLst, Dldt dldt, IboWksBean ibo) {

		Iterator<Schd> schdIt = schdLst.iterator();
		while (schdIt.hasNext()) {// 移除提前之前的数据
			Schd schd = schdIt.next();
			if (schd.getIntenddte().before(DateUtil.parse(ibo.getEmdate()))) {
				schdIt.remove();
			}
		}
		for (Schd schd : schdLst) {// 处理未移除的数据
			schd.setVerdate(dldt.getBrprcindte());// 账务日期
			schd.setLstmntdate(Calendar.getInstance().getTime());// 最后修改日期
			schd.setBrprcindte(dldt.getMdate());// 账务日期
			schd.setFundamt(dldt.getEmpenamt());// 新的利息-违约金
			schd.setPosnfundamt(BigDecimal.ZERO);// 默认填0
		}
		// 排序
		List<Schd> list = new ArrayList<Schd>(schdLst);
		Collections.sort(list, new Comparator<Schd>() {
			@Override
			public int compare(Schd o1, Schd o2) {
				return Integer.parseInt(o1.getSchdseq()) - Integer.parseInt(o2.getSchdseq());
			}
		});
		return list;
	}
}
