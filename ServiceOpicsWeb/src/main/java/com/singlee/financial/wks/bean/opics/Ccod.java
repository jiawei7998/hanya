package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Ccod implements Serializable {
    private String ccy;

    private String ccydesc;

    private String decs;

    private String wday1;

    private String wday2;

    private Date lstmntdte;

    private String roundind;

    private Short roundno;

    private String roundrule;

    private String isocurrcode;

    public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCcydesc() {
		return ccydesc;
	}

	public void setCcydesc(String ccydesc) {
		this.ccydesc = ccydesc;
	}

	public String getDecs() {
		return decs;
	}

	public void setDecs(String decs) {
		this.decs = decs;
	}

	public String getWday1() {
		return wday1;
	}

	public void setWday1(String wday1) {
		this.wday1 = wday1;
	}

	public String getWday2() {
		return wday2;
	}

	public void setWday2(String wday2) {
		this.wday2 = wday2;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getRoundind() {
		return roundind;
	}

	public void setRoundind(String roundind) {
		this.roundind = roundind;
	}

	public Short getRoundno() {
		return roundno;
	}

	public void setRoundno(Short roundno) {
		this.roundno = roundno;
	}

	public String getRoundrule() {
		return roundrule;
	}

	public void setRoundrule(String roundrule) {
		this.roundrule = roundrule;
	}

	public String getIsocurrcode() {
		return isocurrcode;
	}

	public void setIsocurrcode(String isocurrcode) {
		this.isocurrcode = isocurrcode;
	}

	private static final long serialVersionUID = 1L;
}