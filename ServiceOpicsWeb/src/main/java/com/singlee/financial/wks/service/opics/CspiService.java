package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Cspi;
public interface CspiService{


    int deleteByPrimaryKey(String br,String cno,String product,String type,String ccy);

    int insert(Cspi record);

    int insertSelective(Cspi record);

    Cspi selectByPrimaryKey(String br,String cno,String product,String type,String ccy);

    int updateByPrimaryKeySelective(Cspi record);

    int updateByPrimaryKey(Cspi record);

}
