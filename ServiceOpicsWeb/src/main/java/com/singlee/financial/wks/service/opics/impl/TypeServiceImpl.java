package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.wks.bean.opics.Type;
import com.singlee.financial.wks.mapper.opics.TypeMapper;
import com.singlee.financial.wks.service.opics.TypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TypeServiceImpl implements TypeService {

    @Resource
    private TypeMapper typeMapper;

    @Override
    public int deleteByPrimaryKey(String prodcode,String type) {
        return typeMapper.deleteByPrimaryKey(prodcode,type);
    }

    @Override
    public int insert(Type record) {
        return typeMapper.insert(record);
    }

    @Override
    public int insertSelective(Type record) {
        return typeMapper.insertSelective(record);
    }

    @Override
    public Type selectByPrimaryKey(String prodcode,String type) {
        return typeMapper.selectByPrimaryKey(prodcode,type);
    }

    @Override
    public int updateByPrimaryKeySelective(Type record) {
        return typeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Type record) {
        return typeMapper.updateByPrimaryKey(record);
    }

	@Override
	public Type selectByProdAl(String prodcode, String al) {
		return typeMapper.selectByProdAl(prodcode, al);
	}

}
