package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Swds;
import java.util.Date;
import java.util.List;
public interface SwdsService{

    int deleteByPrimaryKey(String br,String dealno,String seq,String dealind,String product,String prodtype,Date intenddte,String schdseq,String schdtype);
    
    int deleteByGtSeq(String br,String dealno,String seq,String dealind,String product,String prodtype,String schdseq,String schdtype);

    int insert(Swds record);

    int insertSelective(Swds record);

    Swds selectByPrimaryKey(String br,String dealno,String seq,String dealind,String product,String prodtype,Date intenddte,String schdseq,String schdtype);
    
    Swds selectBySeq(String br,String dealno,String seq,String dealind,String product,String prodtype,String schdseq,String schdtype);
    
    Swds selectBySchdtype(String br,String dealno,String seq,String dealind,String product,String prodtype,String schdtype);

    int updateByPrimaryKeySelective(Swds record);
    
    int updateBySeqSelective(Swds record);

    int updateByPrimaryKey(Swds record);
    
    void batchInsert(List<Swds> list);
    
    /**
     * 根据交易编号查询所有的现金流信息
     * @param br
     * @param dealno
     * @return
     */
    List<Swds> selectByDealno(String br,String dealno);
    
    /**
     * 根据交易编号删除现金流明细记录
     * @param br
     * @param dealno
     * @return
     */
    int deleteBySchdSeq(Swds record);
    
}
