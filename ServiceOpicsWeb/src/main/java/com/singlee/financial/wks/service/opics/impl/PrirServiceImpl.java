package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.FxdItemWksBean;
import com.singlee.financial.wks.expand.SwiftMethod;
import com.singlee.financial.wks.mapper.opics.PrirMapper;
import com.singlee.financial.wks.service.opics.PrirService;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class PrirServiceImpl implements PrirService {

    @Resource
    private PrirMapper prirMapper;
    @Resource
    private BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br, String product, String type, String dealno, String seq) {
        return prirMapper.deleteByPrimaryKey(br, product, type, dealno, seq);
    }

    @Override
    public int insert(Prir record) {
        return prirMapper.insert(record);
    }

    @Override
    public int insertSelective(Prir record) {
        return prirMapper.insertSelective(record);
    }

    @Override
    public Prir selectByPrimaryKey(String br, String product, String type, String dealno, String seq) {
        return prirMapper.selectByPrimaryKey(br, product, type, dealno, seq);
    }

    @Override
    public int updateByPrimaryKeySelective(Prir record) {
        return prirMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Prir record) {
        return prirMapper.updateByPrimaryKey(record);
    }

    @Override
    public void updateBatch(List<Prir> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.PrirMapper.updateByPrimaryKeySelective", list);
    }

    @Override
    public void batchInsert(List<Prir> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.PrirMapper.insert", list);
    }

    /**
     * 方法已重载.构建PRIR收款报文
     * 
     * @param br
     * @param product
     * @param type
     * @param dealno
     * @param seq
     * @param bic
     * @param c1
     * @param c2
     * @param c3
     * @param c4
     * @param cnarr
     * @param suprecind
     * @param supconfind
     * @param lstmntdte
     * @return
     */
    private Prir builder(String br, String product, String type, String dealno, String seq, String bic, String c1,
        String c2, String c3, String c4, String cnarr, String suprecind, String supconfind, Date lstmntdte,String c5) {
        Prir prir = new Prir();
        // 部门
        prir.setBr(br);
        // 产品代码
        prir.setProduct(product);
        // 业务类型
        prir.setType(type);
        // 交易编号
        prir.setDealno(dealno);
        // 交易序号
        prir.setSeq(seq);
        // SWIFT CODE
        prir.setBic(bic);
        // CONFIRMATION NARRATIVE
        prir.setCnarr(cnarr);
        // 报文第一行信息35个字符
        prir.setC1(c1);
        // 报文第二行信息35个字符
        prir.setC2(c2);
        // 报文第三行信息35个字符
        prir.setC3(c3);
        // 报文第四行信息35个字符
        prir.setC4(c4);
        // 是否禁用确认报文
        prir.setSupconfind(supconfind);
        // 是否禁用收款报文
        prir.setSuprecind(suprecind);
        // 最后更新日期
        prir.setLstmntdte(lstmntdte);
        //报文第五行信息35个字符
        prir.setC5(c5);

        return prir;
    }

    /**
     * 方法已重载.构建PRIR收款报文,设置默认值
     * 
     * @param br
     * @param product
     * @param type
     * @param dealno
     * @param seq
     * @param bic
     * @param c1
     * @param c2
     * @param c3
     * @param c4
     * @return
     */
    private Prir builder(String br, String product, String type, String dealno, String seq,Date bpdate,String supRecInd,String supConfInd) {

    	return builder(br, product, type, dealno, seq, "", "", "", "", "", "", supRecInd,supConfInd,bpdate,"");
    }

    /**
     * 方法已重载.构建PRIR收款报文,设置默认值
     * 
     * @param br
     * @param product
     * @param type
     * @param dealno
     * @param seq
     * @return
     */
    private Prir builder(String br, String product, String type, String dealno, String seq,Date bpdate) {
          //针对4.4版本BIC不允许NULL情况进行处理
        return builder(br, product, type, dealno, seq, bpdate,OpicsConstant.PCIX.SUP_CONF_IND,OpicsConstant.PCIX.SUP_CONF_IND);
    }

    @Override
    public Prir builder(Dldt dl) {
        return builder(dl.getBr(), dl.getProduct(), dl.getProdtype(), dl.getDealno(), dl.getSeq(),dl.getBrprcindte());
    }

    @Override
    public Prir builder(Otdt otc) {
        return builder(otc.getBr(), otc.getProduct(), otc.getProdtype(), otc.getDealno(), otc.getSeq(),otc.getBrprcindte());
    }

    @Override
	public Prir builder(Fxdh fxdh,FxdItemWksBean itemBean) {
		if (null == fxdh || null == itemBean) {
			JY.raise("外汇对象为空!");
		}
		/*
		 * 集中清算时不发送确认报文和收款报文
		 */
		String supRecind = null;
		String supConfInd = null;

		if (SwiftMethod.DISABLE.equals(itemBean.getSwiftMethod())) {
			supRecind = "Y";
			supConfInd = "Y";
		} else if (SwiftMethod.CONFIRMATION.equals(itemBean.getSwiftMethod())) {
			supRecind = "Y";
			supConfInd = "N";
		} else if (SwiftMethod.PAYANDREC.equals(itemBean.getSwiftMethod())) {
			supRecind = "N";
			supConfInd = "Y";
		} else if (SwiftMethod.ENABLE.equals(itemBean.getSwiftMethod())) {
			supRecind = "N";
			supConfInd = "N";
		} else {
			throw new RuntimeException("FxdItemWksBean.swiftMethod处理方法[ "+itemBean.getSwiftMethod()+" ]暂不支持或者为空");
		}
		return builder(fxdh.getBr(), fxdh.getProdcode(), fxdh.getProdtype(), fxdh.getDealno(), fxdh.getSeq(), fxdh.getBrprcindte(), supRecind, supConfInd);
	}

    @Override
	public Prir builder(Swdh swdh, List<Swdt> swdtLst, SwapWksBean swap) {
		List<Prir> prirLst = new ArrayList<Prir>();
		if (null == swdtLst || swdtLst.size() == 0) {
			JY.raise("利率互换/货币互换明细对象为空!");
		}

		LogFactory.getLog(PrirServiceImpl.class).debug("利率互换交易[" + swdh.getDealno() + "],Prir.SwiftMethod:" + swap.getSwiftMethod());
		/*
		 * 集中清算时不发送确认报文和收款报文
		 */
		swdtLst.stream().filter(swdt -> "R".equalsIgnoreCase(StringUtils.trim(swdt.getPayrecind()))).forEach(swdt -> {
			String supRecind = null;
			String supConfInd = null;
			switch (swap.getSwiftMethod()) {
			case DISABLE:
				supRecind = "Y";
				supConfInd = "Y";
				break;
			case CONFIRMATION:
				supRecind = "Y";
				supConfInd = "N";
				break;
			case PAYANDREC:
				supRecind = "N";
				supConfInd = "Y";
				break;
			case ENABLE:
				supRecind = "N";
				supConfInd = "N";
				break;
			default:
				throw new RuntimeException("SwapWksBean.swiftMethod处理方法[ " + swap.getSwiftMethod() + " ]暂不支持或者为空");
			}
			prirLst.add(builder(swdt.getBr(), swdt.getProduct(), swdt.getProdtype(), swdt.getDealno(), swdt.getSeq(), swdh.getBrprcindte(), supRecind, supConfInd));
		});
		return prirLst.get(0);
	}

	@Override
	public Prir builder(Spsh spsh) {
		 return builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(),spsh.getBrprcindte());
	}

	@Override
	public Prir builder(Rdfh rdfh) {
		return builder(rdfh.getBr(), rdfh.getProduct(), rdfh.getProdtype(), rdfh.getTransno(), rdfh.getSeq(),rdfh.getBrprcindte());
	}
	@Override
	public Prir builder(Sldh sldh) {
		 return builder(sldh.getBr(), sldh.getProduct(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(),sldh.getBrprcindte());
	}

	@Override
	public Prir selectByDealno(String br, String product, String type, String dealno) {
		return prirMapper.selectByDealno(br, product, type, dealno);
	}
}
