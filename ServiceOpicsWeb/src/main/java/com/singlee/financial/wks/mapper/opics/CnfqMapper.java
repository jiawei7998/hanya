package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Cnfq;
import java.util.Date;
import org.apache.ibatis.annotations.Param;

public interface CnfqMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq, @Param("prodcode") String prodcode, @Param("cdate") Date cdate, @Param("ctime") String ctime);

    int deleteReverseByPrimaryKey(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq, @Param("prodcode") String prodcode);
    
    int insert(Cnfq record);

    int insertSelective(Cnfq record);

    Cnfq selectByPrimaryKey(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq, @Param("prodcode") String prodcode, @Param("cdate") Date cdate, @Param("ctime") String ctime);

    int updateByPrimaryKeySelective(Cnfq record);

    int updateByPrimaryKey(Cnfq record);
}