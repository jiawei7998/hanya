package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SldhWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.SldaWksBean;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.util.FinancialFieldMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 
 * 债券借贷交易处理
 * 
 * @author tuzh
 *
 */
@Component
public class SlteEntry {

	@Autowired
	private BrpsService brpsService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private PortService portService;
	@Autowired
	private CostService costService;
	@Autowired
	private TradService tradService;
	@Autowired
	private CustService custService;
	@Autowired
	private McfpService mcfpService;
	@Autowired
	private SecmService secmService;
	@Autowired
	private FinancialFieldMap financialFieldMap;

	private static BigDecimal ONE_HUNDRED = new BigDecimal("100");

	/**
	 * 交易信息检查
	 *
	 * @param sldh
	 */
	public void DataCheck(SldhWksBean sldh) {
		// 1.判断债券对象是否存在
		if (null == sldh) {
			JY.raise("%s:%s,请补充完整CbtWksBean对象", WksErrorCode.CBT_NULL.getErrCode(), WksErrorCode.CBT_NULL.getErrMsg());
		}
		// 2.判断交易机构是否存在
		if (StringUtils.isEmpty(sldh.getInstId())) {
			JY.raise("%s:%s,请填写CbtWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NULL.getErrCode(),
					WksErrorCode.BR_NULL.getErrMsg());
		}
		// 3.查询分支/机构是否存在
		Brps brps = brpsService.selectByPrimaryKey(sldh.getInstId());
		if (StringUtils.isEmpty(sldh.getInstId()) || null == brps) {
			JY.raise("%s:%s,请正确输入CbtWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 4.检查产品类型DESK字段拆分PROD_TYPE_PORT
		String[] book = StringUtils.split(sldh.getBook(), "_");
		if (StringUtils.isEmpty(sldh.getBook()) || book.length != 3) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.desk,For.[book=SECUR_SD_100]", WksErrorCode.DESK_FORM_ERR.getErrCode(),
					WksErrorCode.DESK_FORM_ERR.getErrMsg());
		}
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		if (StringUtils.isEmpty(book[0]) || StringUtils.isEmpty(book[1]) || null == type) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.desk,For.[book=SECUR_SD_100]", WksErrorCode.DESK_NOT_FOUND.getErrCode(),
					WksErrorCode.DESK_NOT_FOUND.getErrMsg());
		}
		Port port = portService.selectByPrimaryKey(sldh.getInstId(), sldh.getDesk());// 投组组合
		if (StringUtils.isEmpty(sldh.getDesk()) || null == port) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.desk,For.[desk=PORT]", WksErrorCode.DESK_PORT_ERR.getErrCode(),
					WksErrorCode.DESK_PORT_ERR.getErrMsg());
		}
		// 5.检查成本中心
		Cost cost = costService.selectByPrimaryKey(book[2]);// 成本中心
		if (StringUtils.isEmpty(book[2]) || null == cost) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.book,For.[book=SECUR_SD_100,或查询RATE表]",
					WksErrorCode.BOOK_NOT_FOUND.getErrCode(), WksErrorCode.BOOK_NOT_FOUND.getErrMsg());
		}
		// 6.检查交易对手
		Cust cust = custService.selectByPrimaryKey(sldh.getCno());
		if (StringUtils.isEmpty(sldh.getCno()) || null == cust) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.cno,For.[cno=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(),
					WksErrorCode.CUST_NOT_FOUND.getErrMsg());
		}
		// 7.检查交易员
		Trad trad = tradService.selectByPrimaryKey(sldh.getInstId(), sldh.getTrad());
		if (StringUtils.isNotEmpty(sldh.getTrad()) && null == trad) {
			JY.raise("%s:%s,请输入正确格式CbtWksBean.trad,For.[trad=TRAD]", WksErrorCode.TRAD_NOT_FOUND.getErrCode(),
					WksErrorCode.TRAD_NOT_FOUND.getErrMsg());
		}

	}

	/**
	 * 静态默认数据,该处禁止查询数据库进行转换值,会出现空指针 交易信息
	 * 
	 * @param sldh
	 * @return
	 */
	private Sldh DataTrans(Sldh sldh) {
		sldh.setAssignind(OpicsConstant.SLDH.ASSIGN_IND);// 抵押标志
		sldh.setBrokfeeamt(BigDecimal.ZERO);// 中间费用
		sldh.setChargeamt(BigDecimal.ZERO);// 小费
		sldh.setCloseind(OpicsConstant.SLDH.CLOSE_IND);// 关闭标志
		sldh.setCollrequiredind(OpicsConstant.SLDH.COLL_REQUIRED_IND);// 抵押必须标志
		sldh.setComauthind(OpicsConstant.SLDH.COM_AUTH_IND);// 授权标志
		sldh.setComauthoper(OpicsConstant.SLDH.COM_AUTH_OPER);// 授权操作员
		sldh.setConvintamt(BigDecimal.ZERO);
		sldh.setConvintbamt(BigDecimal.ZERO);
		sldh.setCoupreinvestind(OpicsConstant.SLDH.COUPRE_INVEST_IND);
		sldh.setCoupreinvrate8(BigDecimal.ZERO);
		// sldh.setDealscre(OpicsConstant.SLDH.DEAL_SCRE);
		sldh.setEmpenamt(BigDecimal.ZERO);
		sldh.setExtind(OpicsConstant.SLDH.EXT_IND);
		sldh.setInitterms(OpicsConstant.SLDH.INIT_TERMS);// 初始计算关系
		sldh.setIoper(StringUtils.defaultIfEmpty(sldh.getTrad(), OpicsConstant.SLDH.IOPER));// 操作员
		sldh.setMargcallpct8(BigDecimal.ZERO);
		sldh.setMatauthind(OpicsConstant.SLDH.MAT_AUTH_IND);// 到期标志
		sldh.setMatauthoper(OpicsConstant.SLDH.IOPER);// 授权标志
		sldh.setOpenlendind(OpicsConstant.SLDH.OPEN_LEND_IND);
		sldh.setOrigprocdamt(BigDecimal.ZERO);
		sldh.setOvernightind(OpicsConstant.SLDH.OVER_NIGHT_IND);
		sldh.setOverridewxtaxind(OpicsConstant.SLDH.OVER_RIDEWXTAX_IND);
		sldh.setPmvind(OpicsConstant.SLDH.PMVIND);
		sldh.setProcessind(OpicsConstant.SLDH.PROCESS_SIND);
		sldh.setSpreadrate8(BigDecimal.ZERO);
		sldh.setSubstitute(OpicsConstant.SLDH.SUB_STITUTE);
		sldh.setSubcashoutst(BigDecimal.ZERO);
		sldh.setSuppcashind(OpicsConstant.SLDH.SUPP_CASH_IND);
		sldh.setSuppconfind(OpicsConstant.SLDH.SUPP_CONF_IND);
		sldh.setSuppfeeind(OpicsConstant.SLDH.SUPP_FEE_IND);
		sldh.setSupsecmoveind(OpicsConstant.SLDH.SUPSECOMVE_IND);
		sldh.setTrad(StringUtils.defaultIfEmpty(sldh.getTrad(), OpicsConstant.SLDH.TRAD_NO));
		sldh.setUncollatamt(BigDecimal.ZERO);
		sldh.setUpdatecounter(1L);
		sldh.setVatamt(BigDecimal.ZERO);
		sldh.setVerind(OpicsConstant.SLDH.VER_IND);
		sldh.setVoper(OpicsConstant.SLDH.IOPER);
		sldh.setWhtamt(BigDecimal.ZERO);
		sldh.setCashcollatamt(BigDecimal.ZERO);
		sldh.setCashcollathcamt(BigDecimal.ZERO);
		sldh.setCashhaircut8(BigDecimal.ZERO);
		sldh.setCashinterestrate8(BigDecimal.ZERO);
		sldh.setRemainingcashind(OpicsConstant.SLDH.REMAINING_CASH_IND);
		sldh.setBrok(OpicsConstant.SLDH.DEAL_SCRE);
		sldh.setSeq("0");
		return sldh;
	}

	/**
	 * 组装交易数据
	 * 
	 * 结算金额 = 费用金额/借贷费率/占款期限/计息基础*100
	 * 
	 * 全价价格 = 结算金额/券面总额*100
	 * 
	 * 应计利息 = 券面总额*占款期限*借贷费率
	 * 
	 * 净价价格 = (结算金额-应计利息)/券面总额
	 * 
	 * @param sldh
	 * @param slte
	 * @return
	 */
	private Sldh DataTrans(Sldh sldh, SldhWksBean slte) {
		DataTrans(sldh);
		// 转换资金工作站SldhWksBean对象中的枚举值
		financialFieldMap.convertFieldvalue(slte);
		// 获取当前账务日期
		Brps brps = brpsService.selectByPrimaryKey(sldh.getBr());
		sldh.setBrprcindte(brps.getBranprcdate());// 当前账务日期
		// 买卖方向
		if ("SLDH".equals(sldh.getDealind())) {
			sldh.setPs(slte.getPs());
		} else {
			sldh.setPs("P".equals(slte.getPs()) ? "S" : "P");
		}
		sldh.setAssigndate(sldh.getBrprcindte());// 抵押日期
		sldh.setBasis(slte.getBasis());// 计息基础
		sldh.setCcy(slte.getCcy());// 交易货币代码
		sldh.setBrokfeeccy(sldh.getCcy());// 中间费货币代码
		sldh.setCno(slte.getCno());// 交易对手
		sldh.setCollateralcode(StringUtils.defaultIfEmpty(slte.getCollCode(), "ALL"));// 担保代码[不填写,系统默认ALL]
		sldh.setComauthdate(sldh.getBrprcindte());// 授权日期
		//集中处理金额
		sldh.setComprocbaseamt(OpicsConstant.SLDH.DEALIND.equals(sldh.getDealind())?sldh.getLoanamt():sldh.getLoanamt().negate());
		sldh.setComprocamt(sldh.getComprocbaseamt());
		sldh.setCollamt(OpicsConstant.SLDH.DEALIND.equals(sldh.getDealind())?BigDecimal.ZERO:sldh.getComprocamt());//抵押/担保金额，存在误差
		//sldh.setFeeamt();在外部已处理 
		//sldh.setLoanamt();在外部已处理 
		sldh.setMatprocbaseamt(sldh.getComprocbaseamt().negate());
		sldh.setMatprocamt(sldh.getMatprocbaseamt());
		//sldh.setNotfeeamt();在外部已处理
		sldh.setQuotqty(OpicsConstant.SLDH.DEALIND.equals(sldh.getDealind())?sldh.getAssignqty():BigDecimal.ZERO);
		sldh.setQuotamt(OpicsConstant.SLDH.DEALIND.equals(sldh.getDealind())?sldh.getCollamt():sldh.getCollamt().negate());
		//清算信息处理
		sldh.setComccysmeans(StringUtils.defaultIfEmpty(slte.getSettmeans(),OpicsConstant.PCIX.DVP_MEANS));// 清算账户
		sldh.setMatccysmeans(StringUtils.defaultIfEmpty(slte.getSettmeans(),OpicsConstant.PCIX.DVP_MEANS));// 质押债
		sldh.setComccysacct("P".equals(sldh.getPs()) ? OpicsConstant.PCIX.DVP_ACCT_P : OpicsConstant.PCIX.DVP_ACCT_R);// 清算方式
		sldh.setMatccysacct("P".equals(sldh.getPs()) ? OpicsConstant.PCIX.DVP_ACCT_P : OpicsConstant.PCIX.DVP_ACCT_R);// 清算账户
		sldh.setDealdate(DateUtil.parse(slte.getDealDate()));// 交易日期
		sldh.setDealtime(DateUtil.getCurrentTimeAsString());// 交易日期
		sldh.setDelivtype(sldh.getComccysmeans());// 交割方式
		sldh.setFeecalctype(
				sldh.getDealind().equals(OpicsConstant.SLDH.DEALIND) ? OpicsConstant.SLDH.FEE_CALC_TYPE : "");// 费用计算类型
		sldh.setInputdate(Calendar.getInstance().getTime()); // 放入日期
		sldh.setInputtime(DateUtil.getCurrentTimeAsString());// 放入时间
		sldh.setLstmntdte(sldh.getBrprcindte());// 最后修改时间
		sldh.setVdate(DateUtil.parse(slte.getVdate()));// 起息日期
		sldh.setMatdate(DateUtil.parse(slte.getMatDate()));// 到期日
		sldh.setMatauthdate(sldh.getBrprcindte());
		sldh.setVerdate(sldh.getBrprcindte());// 复核日期
		sldh.setDealtext(slte.getTradeNo());// 外部交易编号,冲销使用
		sldh.setSafekeepacct(OpicsConstant.SLDH.SAFE);
		return sldh;
	}

	/**
	 * 组装交易数据
	 * 
	 * @param fees
	 * @param lend
	 * @param slte
	 * @return
	 */
	public List<Sldh> DataTrans(Fees fees, Sldh lend, SldhWksBean slte) {
		List<Sldh> retList = new ArrayList<Sldh>();
		// 标的券处理[借入/借出]
		String[] book = StringUtils.split(slte.getBook(), "_");// 债券借贷产品+类型+COST
		lend.setProduct(book[0]);// 产品类型
		lend.setProdtype(book[1]);// 产品代码
		lend.setCashcost(book[2]);// 成本中心
		lend.setBr(slte.getInstId());// 分行号:机构号
		lend.setFeeno(fees.getFeeno());// 费用编号
		lend.setFeeamt(fees.getCcyamt());
		lend.setFeeper8(fees.getFeeper8());
		lend.setDealind(OpicsConstant.SLDH.DEALIND);// 交易标志
		lend.setAssignqty(slte.getSlda().getQty());//获取标的券的数量
		lend.setLoanamt(getSettleAmt(slte.getSlda().getSecid(),slte.getPs(),lend.getAssignqty()));//标的券金额
		lend.setNotfeeamt(OpicsConstant.SLDH.DEALIND.equals(lend.getDealind())?fees.getCcyamt():fees.getCcyamt().negate());//费用金额
		lend.setFeeper8(fees.getFeeper8());//费率
		lend.setTenor(OpicsConstant.SLDH.TENOR);//期限
		Type lendType = typeService.selectByPrimaryKey(lend.getProduct(), lend.getProdtype());
		DataTrans(lend, slte);// 转换
		retList.add(lend);// 添加
		// 抵押券信息
		Sldh collat = new Sldh();
		collat.setProduct(OpicsConstant.SLDH.COLL_PROD);// 产品代码
		Type collatType = typeService.selectByProdAl(collat.getProduct(),
				lendType.getAl().equals(OpicsConstant.SLDH.ASST_IND) ? OpicsConstant.SLDH.LIAB_IND
						: OpicsConstant.SLDH.ASST_IND);
		collat.setProdtype(collatType.getType());// 产品类型
		collat.setCashcost(book[2]);// 成本中心
		collat.setBr(lend.getBr());// br
		collat.setDealno(lend.getDealno());// 交易编号
		// collat.setFeeno(fees.getFeeno());// 费用编号 质押债不用填
		collat.setFeeamt(BigDecimal.ZERO);
		collat.setDealind(OpicsConstant.SLDH.DEALIND_COLL);// 交易标志
		collat.setSeq(OpicsConstant.SLDH.SEQ);
		collat.setAssignqty(calcCollQty(slte.getSldaList()));//获取抵押权的数量
		collat.setLoanamt(lend.getLoanamt());
		collat.setNotfeeamt(BigDecimal.ZERO);
		collat.setFeeper8(BigDecimal.ZERO);
		collat.setTrad(StringUtils.defaultIfEmpty(slte.getTrad(), OpicsConstant.SLDH.TRAD_NO));
		collat.setTenor(OpicsConstant.SLDH.TENOR);//期限
		DataTrans(collat, slte);// 转换
		retList.add(collat);// 添加
		// 3.更新dealno
		DataTrans(lend, mcfpService, new Mcfp());

		return retList;
	}

	/**
	 * 静态默认数据,该处禁止查询数据库进行转换值,会出现空指针 交易详细
	 * 
	 * @param slda
	 * @return
	 */
	private Slda DataTrans(Slda slda) {
		// 4.5版本有这个字段
		slda.setSeq("0");
		slda.setChargeamt(BigDecimal.ZERO);
		slda.setDiscrate8(BigDecimal.ZERO);
		slda.setExtretqty(BigDecimal.ZERO);
		slda.setExtretamt(BigDecimal.ZERO);
		slda.setHaircut8(BigDecimal.ZERO);
		slda.setIndexrate8(BigDecimal.ZERO);
		slda.setLinkassignseq(Short.valueOf("0"));
		slda.setMatprocorigamt(BigDecimal.ZERO);
		slda.setMatprocorigbaseamt(BigDecimal.ZERO);
		slda.setPremamt(BigDecimal.ZERO);
		slda.setPvmatprocamt(BigDecimal.ZERO);
		slda.setReinvestind("N");
		slda.setSwaprate8(BigDecimal.ZERO);
		slda.setUpdatecounter(1L);
		slda.setVatamt(BigDecimal.ZERO);
		slda.setVerind("1");
		slda.setVoper("SYS1");
		slda.setWhtamt(BigDecimal.ZERO);
		slda.setPrinexchrate8(new BigDecimal("1"));
		slda.setPrinexchterms("M");
		slda.setHaircut8(BigDecimal.ZERO);
		slda.setHaircutterms(OpicsConstant.SLDH.HAIRCUT_TERMS);
		slda.setSectypeind(OpicsConstant.SLDH.SEC_TYPEIND);
		return slda;
	}

	/**
	 * 交易数据
	 *
	 *已废弃 结算金额 = 费用金额/借贷费率/占款期限/计息基础*100
	 * 
	 * 全价价格 = 结算金额/券面总额*100
	 * 
	 * 应计利息 = 券面总额*占款期限*借贷费率
	 * 
	 * 净价价格 = (结算金额-应计利息)/券面总额
	 * 
	 * @param sldhLst
	 * @param slte
	 * @return
	 */
	public List<Slda> DataTrans(List<Sldh> sldhLst, SldhWksBean slte) {
		List<Slda> result = new ArrayList<Slda>();
		int i = 1;
		Sldh sldh = sldhLst.get(0);
		// 标的债录入
		SldaWksBean sldaL = slte.getSlda();
		Slda sldaB = DataTrans(new Slda());
		sldaB.setPs(sldh.getPs());
		sldaB.setDealind(sldh.getDealind());
		sldaB.setProdtype(sldh.getProdtype());
		sldaB.setProduct(sldh.getProduct());
		sldaB.setAssigntype("P".equals(sldaB.getPs()) ? "1" : "0");
		sldaB.setBr(slte.getInstId());
		sldaB.setDealno(sldh.getDealno());
		sldaB.setAssignseq(String.format("%03d", i));
		sldaB.setBrprcindte(sldh.getBrprcindte());
		sldaB.setDealtime(sldh.getDealtime());
		sldaB.setDelivtype(sldh.getDelivtype());
		sldaB.setSecid(sldaL.getSecid());
		sldaB.setQty(sldaL.getQty());// 手数
		//处理金额
		sldaB.setFaceamt(getSettleAmt(sldaB.getSecid(), sldaB.getPs(), sldaB.getQty()).abs());//面值
		sldaB.setComprocamt(getPlusNegateAmt(sldaB.getPs(), sldaB.getFaceamt()));//处理金额
		sldaB.setMatprocbaseamt(sldaB.getFaceamt());
		sldaB.setMatprocamt(sldaB.getFaceamt());
		sldaB.setMdmsgqty(sldaB.getFaceamt());
		sldaB.setMdmsgamt(sldaB.getFaceamt());
		sldaB.setOrigfaceamt(sldaB.getFaceamt());
		sldaB.setPrinamt(sldaB.getComprocamt());
		sldaB.setRemamt(sldaB.getComprocamt());
		sldaB.setRemfaceamt(sldaB.getFaceamt());
		sldaB.setVdmsgqty(sldaB.getFaceamt());
		sldaB.setVdmsgamt(sldaB.getComprocamt());
		sldaB.setProceedamt(sldaB.getComprocamt());//使用faceamt 存在误差
		sldaB.setProceedamthc(sldaB.getComprocamt());//使用faceamt 存在误差
		// 净价 全价标识 D
		sldaB.setDispricerateind(sldaL.getDispricerateind());
		BigDecimal drPriceB = sldh.getComprocamt().divide(sldaL.getFaceamt(), 16, RoundingMode.HALF_UP)
				.multiply(ONE_HUNDRED).setScale(8, RoundingMode.HALF_UP).abs();
		sldaB.setDrprice8(drPriceB);// 全价
		BigDecimal clPriceB = sldh.getComprocamt().subtract(getAcctualAmt(slte, sldaB))
				.divide(sldaL.getFaceamt(), 16, RoundingMode.HALF_UP).multiply(ONE_HUNDRED)
				.setScale(8, RoundingMode.HALF_UP).abs();
		sldaB.setClprice8(clPriceB);// 净价
		sldaB.setDispricerate8(sldaB.getDrprice8());// 全价
		sldaB.setCurrfillprice8(sldaB.getDrprice8());// 全价
		sldaB.setFillprice8(sldaB.getDrprice8());// 全价
		sldaB.setDiscamt(getPlusNegateAmt(sldh.getPs(), getAcctualAmt(slte, sldaB)));// 应计利息
		sldaB.setPurchintamt(getPlusNegateAmt(sldh.getPs(), getAcctualAmt(slte, sldaB)));
		sldaB.setIoper(StringUtils.defaultIfEmpty(sldh.getIoper(), OpicsConstant.SLDH.IOPER));
		// 暂时写死
		String[] book = StringUtils.split(sldaL.getBook(), "_");
		sldaB.setCost(book[2]);
		sldaB.setPort(sldaL.getDesk());
		/*
		 * sldaB.setCost("1360100002"); sldaB.setPort("SLBS");
		 */
		sldaB.setInvtype(StringUtils.isBlank(sldaL.getInvtype()) ? "T" : sldaL.getInvtype());
		sldaB.setCcy(sldh.getCcy());
		sldaB.setSettccy(sldh.getCcy());
		sldaB.setNotccy(sldh.getCcy());
		sldaB.setVdate(sldh.getVdate());
		sldaB.setInputdate(sldh.getInputdate());
		sldaB.setInputtime(sldh.getInputtime());
		sldaB.setMdate(sldh.getMatdate());
		sldaB.setVerdate(sldh.getBrprcindte());
		Secm secm = secmService.selectByPrimaryKey(sldaL.getSecid());
		sldaB.setYield8(getYield(sldaB.getDrprice8(), ONE_HUNDRED, secm.getCouprate8()));
		result.add(sldaB);
		i++;
		// 质押债信息
		List<SldaWksBean> sldaWksLst = slte.getSldaList();
		for (SldaWksBean sldaColl : sldaWksLst) {
			Slda slda = DataTrans(new Slda());
			slda.setPs("P".equals(sldh.getPs()) ? "S" : "P");
			slda.setDealind(OpicsConstant.SLDH.DEALIND_COLL);
			Type lendType = typeService.selectByPrimaryKey(sldh.getProduct(), sldh.getProdtype());
			Type collatType = typeService.selectByProdAl(OpicsConstant.SLDH.PROD_CODE_COLLAT,
					lendType.getAl().equals(OpicsConstant.SLDH.ASST_IND) ? OpicsConstant.SLDH.LIAB_IND
							: OpicsConstant.SLDH.ASST_IND);
			slda.setProdtype(collatType.getType());
			slda.setProduct(collatType.getProdcode());
			slda.setAssigntype("2");// 序号
			slda.setBr(slte.getInstId());// 机构
			slda.setDealno(sldh.getDealno());// 单号
			slda.setAssignseq(String.format("%03d", i));// 腿序号
			slda.setBrprcindte(sldh.getBrprcindte());
			slda.setDealtime(sldh.getDealtime());
			slda.setDelivtype(sldh.getDelivtype());
			// 暂时写死
			String[] book2 = StringUtils.split(sldaColl.getBook(), "_");
			slda.setCost(book2[2]);
			slda.setPort(sldaColl.getDesk());
			/*
			 * slda.setCost("1360100002");slda.setPort("FIZY");
			 */
			slda.setInvtype(StringUtils.isBlank(sldaColl.getInvtype()) ? "T" : sldaColl.getInvtype());
			slda.setSecid(sldaColl.getSecid());
			slda.setQty(sldaColl.getQty());
			//处理金额
			slda.setFaceamt(getSettleAmt(slda.getSecid(), slda.getPs(), slda.getQty()).abs());//面值
			slda.setComprocamt(getPlusNegateAmt(slda.getPs(), sldaB.getFaceamt()));//处理金额
			slda.setMatprocbaseamt(slda.getComprocamt().negate());
			slda.setMatprocamt(slda.getMatprocbaseamt());
			slda.setMdmsgqty(slda.getFaceamt());
			slda.setMdmsgamt(slda.getMatprocbaseamt());
			slda.setOrigfaceamt(slda.getFaceamt());
			slda.setPrinamt(slda.getFaceamt());
			slda.setRemamt(slda.getComprocamt());
			slda.setRemfaceamt(slda.getFaceamt());
			slda.setVdmsgqty(slda.getFaceamt());
			slda.setVdmsgamt(slda.getComprocamt());
			slda.setProceedamt(slda.getFaceamt());//使用faceamt 存在误差
			slda.setProceedamthc(slda.getFaceamt());//使用faceamt 存在误差
			BigDecimal drPrice = slda.getPrinamt().divide(slda.getFaceamt(), 16, RoundingMode.HALF_UP)
					.multiply(ONE_HUNDRED).setScale(8, RoundingMode.HALF_UP).abs();
			BigDecimal clPrice = slda.getPrinamt().subtract(getAcctualAmt(slte, slda))
					.divide(slda.getFaceamt(), 16, RoundingMode.HALF_UP).multiply(ONE_HUNDRED)
					.setScale(8, RoundingMode.HALF_UP).abs();
			// 净价 全价标识 D
			slda.setDispricerateind(sldaColl.getDispricerateind());
			slda.setClprice8(clPrice);/// 净价
			slda.setDrprice8(drPrice);// 全价
			slda.setDispricerate8(sldaColl.getDrprice_8());// 全价
			slda.setFillprice8(sldaColl.getDrprice_8());// 全价
			slda.setCurrfillprice8(sldaColl.getDrprice_8());// 全价
			slda.setIoper(StringUtils.defaultIfEmpty(sldh.getIoper(), OpicsConstant.SLDH.IOPER));// 录入用户
			
			slda.setDiscamt(getPlusNegateAmt(slda.getPs(), BigDecimal.ZERO));// 应收利息
			slda.setPurchintamt(getPlusNegateAmt(slda.getPs(), BigDecimal.ZERO));
			slda.setCcy(sldh.getCcy());
			slda.setSettccy(sldh.getCcy());
			slda.setNotccy(sldh.getCcy());
			slda.setVdate(sldh.getVdate());
			slda.setInputdate(sldh.getInputdate());
			slda.setInputtime(sldh.getInputtime());
			slda.setMdate(sldh.getMatdate());
			slda.setVerdate(sldh.getBrprcindte());
			Secm secmPledge = secmService.selectByPrimaryKey(slda.getSecid());
			slda.setYield8(getYield(sldaB.getDrprice8(), ONE_HUNDRED, secmPledge.getCouprate8()));
			result.add(slda);
			i++;
		}
		return result;
	}

	/**
	 * 债券路径
	 * 
	 * @param drpi
	 * @param sldhLst
	 * @return
	 */
	public Drpi DataTrans(Drpi drpi, List<Sldh> sldhLst) {
		Sldh sldh = sldhLst.get(0);
		drpi.setBr(sldh.getBr());// 机构:
		drpi.setProduct(sldh.getProduct());// 产品:
		drpi.setProdtype(sldh.getProdtype());// 产品类型:
		drpi.setDealno(sldh.getDealno());// 单号:
		drpi.setSeq(sldh.getSeq());// 序号:
		drpi.setDelrecind("P".equals(sldh.getPs()) ? "R" : "D");// 债的收付方向:
		drpi.setSupsecmoveind(OpicsConstant.SLDH.SUPSECMOVEIND);// 默认N :
		drpi.setSupccymoveind(OpicsConstant.SLDH.SUPCCYMOVEIND);// 默认N:
		drpi.setLstmntdate(Calendar.getInstance().getTime());// :

		return drpi;
	}

	/**
	 * 获取dealno
	 *
	 * @param sldh
	 * @param mcfpService
	 * @return
	 */
	public Sldh DataTrans(Sldh sldh, String br, McfpService mcfpService) {
		// 获取DEALNO
		Mcfp mcfp = mcfpService.selectByPrimaryKey(br, OpicsConstant.SLDH.PROD_CODE, OpicsConstant.SLDH.PROD_TYPE);// 设置交易编号
		String dealno = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";// 获取
		sldh.setDealno(dealno);// 设置交易编号
		return sldh;
	}

	/**
	 * 更新交易单号
	 * <p>
	 * 债券借贷单号更新
	 *
	 * @param sldh
	 * @param mcfpService
	 * @param mcfp
	 */
	public void DataTrans(Sldh sldh, McfpService mcfpService, Mcfp mcfp) {
		mcfp.setBr(sldh.getBr());
		mcfp.setTraddno(StringUtils.trimToEmpty(sldh.getDealno()));
		mcfp.setProdcode(sldh.getProduct());// 产品代码 去掉空格
		mcfp.setType(OpicsConstant.SLDH.PROD_TYPE);// 产品类型
		mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
	}

	/**
	 * 根据收付方向判断金额正负
	 */
	private BigDecimal getPlusNegateAmt(String drcrind, BigDecimal amt) {
		if (StringUtils.equals(drcrind, OpicsConstant.SLDH.PAY_IND)
				|| StringUtils.equals(drcrind, OpicsConstant.SLDH.PS_IND)
				|| StringUtils.equals(drcrind, OpicsConstant.SLDH.LIAB_IND)) {
			return amt.negate();// 取反
		} else {
			return amt.plus();// 取正
		}
	}

	/**
	 * 结算金额
	 * 
	 * @param slte
	 * @return
	 */
	private BigDecimal getSettleAmt(String secid,String ps,BigDecimal assignQty) {
		Secm secm = secmService.selectByPrimaryKey(secid);
		BigDecimal qty = assignQty.multiply(new BigDecimal(secm.getDenom().trim()));
		return "P".equals(ps)?qty.negate():qty;
	}

	/**
	 * 应计利息
	 * 
	 * @param slte
	 * @return
	 */
	private BigDecimal getAcctualAmt(SldhWksBean slte, Slda slda) {
		if (!StringUtils.isNoneBlank(slte.getPayIntDate())) {
			return BigDecimal.ZERO;
		}
		if (DateUtil.isEffectiveDate(DateUtil.parse(slte.getPayIntDate()), DateUtil.parse(slte.getVdate()),
				DateUtil.parse(slte.getMatDate()))) {
			return BigDecimal.ZERO;
		}
		BigDecimal days = BigDecimal.valueOf(DateUtil.daysBetween(slte.getPayIntDate(), slte.getMatDate()));
		BigDecimal basis = slte.getBasis().contains("360") ? new BigDecimal("360") : new BigDecimal("365");
		return slda.getFaceamt().multiply(slte.getFeePer8()).multiply(days).divide(basis, 16, RoundingMode.HALF_UP)
				.divide(ONE_HUNDRED).setScale(2, RoundingMode.HALF_UP).abs();
	}

	/**
	 * 计算收益率
	 * 
	 * @param buyamt
	 * @param paramt
	 * @param couprate
	 * @return
	 */
	private BigDecimal getYield(BigDecimal buyamt, BigDecimal paramt, BigDecimal couprate) {
		return (paramt.multiply(couprate).divide(ONE_HUNDRED).add(paramt).subtract(buyamt))
				.divide(buyamt, 16, RoundingMode.HALF_UP).multiply(ONE_HUNDRED).setScale(8, RoundingMode.HALF_UP);
	}
	
	/**
	 * 获取抵押权总数量
	 * @param sldaList
	 * @return
	 */
	private BigDecimal calcCollQty(List<SldaWksBean> sldaList) {
		BigDecimal decimal = new BigDecimal("0");
		for (SldaWksBean sldaWksBean : sldaList) {
			decimal= decimal.add(sldaWksBean.getQty());
		}
		return decimal;
	}

	public static void main(String[] args) {
		System.out.println(new BigDecimal("9493.15").divide(new BigDecimal("0.45"), 16, RoundingMode.HALF_UP)
				.divide(new BigDecimal("7"), 16, RoundingMode.HALF_UP).multiply(new BigDecimal("365"))
				.multiply(ONE_HUNDRED).setScale(2, RoundingMode.HALF_UP));
	}
}
