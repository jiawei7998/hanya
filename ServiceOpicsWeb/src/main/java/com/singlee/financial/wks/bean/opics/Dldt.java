package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Dldt implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String product;

    private String prodtype;

    private String trad;

    private Date vdate;

    private Date mdate;

    private String cno;

    private String ccy;

    private BigDecimal ccyamt;

    private BigDecimal origocamt;

    private String al;

    private String ratecode;

    private String basis;

    private String intrate;

    private BigDecimal spread8;

    private String acrfrstlst;

    private String brok;

    private String brokccy;

    private BigDecimal brokamt;

    private BigDecimal brokrate8;

    private String port;

    private String cost;

    private Date dealdate;

    private String dealtime;

    private String ioper;

    private String voper;

    private String guarantor;

    private Date custcdate;

    private String verind;

    private Date verdate;

    private String revreason;

    private Date revdate;

    private String revtext;

    private String payauthind;

    private String payoper;

    private Date payauthdte;

    private String recoper;

    private Date recauthdte;

    private Date inputdate;

    private String inputtime;

    private String dealsrce;

    private String arbdealno;

    private Date trcedate;

    private String tracecnt;

    private String commmeans;

    private String commacct;

    private String matmeans;

    private String matacct;

    private Date brprcindte;

    private String dealtext;

    private Date lstmntdate;

    private Date raterevdte;

    private Date nxtraterev;

    private String schdflag;

    private String newdealno;

    private String origdealno;

    private String intcapflag;

    private BigDecimal intcapamt;

    private BigDecimal lienamt;

    private String lienoffcr;

    private Date liendate;

    private BigDecimal prmdscamrtd;

    private BigDecimal empenamt;

    private String emioper;

    private Date eminputdate;

    private Date origmdate;

    private String tenor;

    private BigDecimal fundrate8;

    private String fundbasis;

    private String fundcost;

    private BigDecimal discrate8;

    private String firstdealno;

    private String fixrule;

    private String intcalcrule;

    private Date firstipaydate;

    private Date lastipaydate;

    private String intpaycycle;

    private String prinpaycycle;

    private String intpayday;

    private String prinpayday;

    private BigDecimal yield8;

    private String intsmeans;

    private String fincal;

    private String siind;

    private String intsettauthind;

    private String intauthoper;

    private Date intauthdate;

    private String secid;

    private String corptrad;

    private String corpport;

    private String agentind;

    private Long updatecounter;

    private String text1;

    private String text2;

    private Date date1;

    private Date date2;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private String matstatus;

    private String revdealno;

    private String btbdealno;

    private Date amenddate;

    private String custrefno;

    private BigDecimal intrate8;

    private String revoper;

    private String payauthoper;

    private String recauthoper;

    private String btbtrad;

    private String btbport;

    private String btbcost;

    private String linkdealno;

    private String linkproduct;

    private String linkprodtype;

    private String linkseq;

    private String sipayind;

    private String swiftmatchind;

    private String intsacct;

    private String commusualid;

    private String matusualid;

    private String commtypecode;

    private String mattypecode;

    private String scheduletype;

    private String prindaterule;

    private String prindatedir;

    private BigDecimal printotpayments;

    private String intdaterule;

    private String intdatedir;

    private BigDecimal lastamount;

    private String btbind;

    private String btbbranch;

    private String btbseq;

    private String btbproduct;

    private String btbprodtype;

    private String btbfundcenter;

    private String delayedintind;

    private Date delayedintdate;

    private String earlymatind;

    private BigDecimal brokexchrate8;

    private String prinsipayind;

    private String prinsirecind;

    private String intsipayind;

    private String intsirecind;

    private String netsi;

    private String netdealno;

    private String netseq;

    private String corpnetind;

    private String taxchgdesc;

    private BigDecimal taxamt;

    private String taxauthority;

    private String taxproduct;

    private String taxprodtype;

    private String taxtype;

    private String acctgmethod;

    private String fundport;

    private String fundtrad;

    private String btbcustomer;

    private String btbcommusualid;

    private String btbmatusualid;

    private String corpdealno;

    private String corpseq;

    private BigDecimal stepuprate8;

    private String fedealno;

    private String server;

    private BigDecimal profitrate8;

    private BigDecimal templatetype;

    private String ucparty;

    private String contactinfo;

    private String spinst;

    private String cusipisin;

    private String allownegint;

    private Date effectivestatusdate;

    private String dealrebooked;

    private String rebookeddeal;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getTrad() {
		return trad;
	}


	public void setTrad(String trad) {
		this.trad = trad;
	}


	public Date getVdate() {
		return vdate;
	}


	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}


	public Date getMdate() {
		return mdate;
	}


	public void setMdate(Date mdate) {
		this.mdate = mdate;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public BigDecimal getCcyamt() {
		return ccyamt;
	}


	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}


	public BigDecimal getOrigocamt() {
		return origocamt;
	}


	public void setOrigocamt(BigDecimal origocamt) {
		this.origocamt = origocamt;
	}


	public String getAl() {
		return al;
	}


	public void setAl(String al) {
		this.al = al;
	}


	public String getRatecode() {
		return ratecode;
	}


	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public String getIntrate() {
		return intrate;
	}


	public void setIntrate(String intrate) {
		this.intrate = intrate;
	}


	public BigDecimal getSpread8() {
		return spread8;
	}


	public void setSpread8(BigDecimal spread8) {
		this.spread8 = spread8;
	}


	public String getAcrfrstlst() {
		return acrfrstlst;
	}


	public void setAcrfrstlst(String acrfrstlst) {
		this.acrfrstlst = acrfrstlst;
	}


	public String getBrok() {
		return brok;
	}


	public void setBrok(String brok) {
		this.brok = brok;
	}


	public String getBrokccy() {
		return brokccy;
	}


	public void setBrokccy(String brokccy) {
		this.brokccy = brokccy;
	}


	public BigDecimal getBrokamt() {
		return brokamt;
	}


	public void setBrokamt(BigDecimal brokamt) {
		this.brokamt = brokamt;
	}


	public BigDecimal getBrokrate8() {
		return brokrate8;
	}


	public void setBrokrate8(BigDecimal brokrate8) {
		this.brokrate8 = brokrate8;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public Date getDealdate() {
		return dealdate;
	}


	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}


	public String getDealtime() {
		return dealtime;
	}


	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}


	public String getIoper() {
		return ioper;
	}


	public void setIoper(String ioper) {
		this.ioper = ioper;
	}


	public String getVoper() {
		return voper;
	}


	public void setVoper(String voper) {
		this.voper = voper;
	}


	public String getGuarantor() {
		return guarantor;
	}


	public void setGuarantor(String guarantor) {
		this.guarantor = guarantor;
	}


	public Date getCustcdate() {
		return custcdate;
	}


	public void setCustcdate(Date custcdate) {
		this.custcdate = custcdate;
	}


	public String getVerind() {
		return verind;
	}


	public void setVerind(String verind) {
		this.verind = verind;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public String getRevreason() {
		return revreason;
	}


	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public String getRevtext() {
		return revtext;
	}


	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}


	public String getPayauthind() {
		return payauthind;
	}


	public void setPayauthind(String payauthind) {
		this.payauthind = payauthind;
	}


	public String getPayoper() {
		return payoper;
	}


	public void setPayoper(String payoper) {
		this.payoper = payoper;
	}


	public Date getPayauthdte() {
		return payauthdte;
	}


	public void setPayauthdte(Date payauthdte) {
		this.payauthdte = payauthdte;
	}


	public String getRecoper() {
		return recoper;
	}


	public void setRecoper(String recoper) {
		this.recoper = recoper;
	}


	public Date getRecauthdte() {
		return recauthdte;
	}


	public void setRecauthdte(Date recauthdte) {
		this.recauthdte = recauthdte;
	}


	public Date getInputdate() {
		return inputdate;
	}


	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}


	public String getInputtime() {
		return inputtime;
	}


	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}


	public String getDealsrce() {
		return dealsrce;
	}


	public void setDealsrce(String dealsrce) {
		this.dealsrce = dealsrce;
	}


	public String getArbdealno() {
		return arbdealno;
	}


	public void setArbdealno(String arbdealno) {
		this.arbdealno = arbdealno;
	}


	public Date getTrcedate() {
		return trcedate;
	}


	public void setTrcedate(Date trcedate) {
		this.trcedate = trcedate;
	}


	public String getTracecnt() {
		return tracecnt;
	}


	public void setTracecnt(String tracecnt) {
		this.tracecnt = tracecnt;
	}


	public String getCommmeans() {
		return commmeans;
	}


	public void setCommmeans(String commmeans) {
		this.commmeans = commmeans;
	}


	public String getCommacct() {
		return commacct;
	}


	public void setCommacct(String commacct) {
		this.commacct = commacct;
	}


	public String getMatmeans() {
		return matmeans;
	}


	public void setMatmeans(String matmeans) {
		this.matmeans = matmeans;
	}


	public String getMatacct() {
		return matacct;
	}


	public void setMatacct(String matacct) {
		this.matacct = matacct;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public String getDealtext() {
		return dealtext;
	}


	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public Date getRaterevdte() {
		return raterevdte;
	}


	public void setRaterevdte(Date raterevdte) {
		this.raterevdte = raterevdte;
	}


	public Date getNxtraterev() {
		return nxtraterev;
	}


	public void setNxtraterev(Date nxtraterev) {
		this.nxtraterev = nxtraterev;
	}


	public String getSchdflag() {
		return schdflag;
	}


	public void setSchdflag(String schdflag) {
		this.schdflag = schdflag;
	}


	public String getNewdealno() {
		return newdealno;
	}


	public void setNewdealno(String newdealno) {
		this.newdealno = newdealno;
	}


	public String getOrigdealno() {
		return origdealno;
	}


	public void setOrigdealno(String origdealno) {
		this.origdealno = origdealno;
	}


	public String getIntcapflag() {
		return intcapflag;
	}


	public void setIntcapflag(String intcapflag) {
		this.intcapflag = intcapflag;
	}


	public BigDecimal getIntcapamt() {
		return intcapamt;
	}


	public void setIntcapamt(BigDecimal intcapamt) {
		this.intcapamt = intcapamt;
	}


	public BigDecimal getLienamt() {
		return lienamt;
	}


	public void setLienamt(BigDecimal lienamt) {
		this.lienamt = lienamt;
	}


	public String getLienoffcr() {
		return lienoffcr;
	}


	public void setLienoffcr(String lienoffcr) {
		this.lienoffcr = lienoffcr;
	}


	public Date getLiendate() {
		return liendate;
	}


	public void setLiendate(Date liendate) {
		this.liendate = liendate;
	}


	public BigDecimal getPrmdscamrtd() {
		return prmdscamrtd;
	}


	public void setPrmdscamrtd(BigDecimal prmdscamrtd) {
		this.prmdscamrtd = prmdscamrtd;
	}


	public BigDecimal getEmpenamt() {
		return empenamt;
	}


	public void setEmpenamt(BigDecimal empenamt) {
		this.empenamt = empenamt;
	}


	public String getEmioper() {
		return emioper;
	}


	public void setEmioper(String emioper) {
		this.emioper = emioper;
	}


	public Date getEminputdate() {
		return eminputdate;
	}


	public void setEminputdate(Date eminputdate) {
		this.eminputdate = eminputdate;
	}


	public Date getOrigmdate() {
		return origmdate;
	}


	public void setOrigmdate(Date origmdate) {
		this.origmdate = origmdate;
	}


	public String getTenor() {
		return tenor;
	}


	public void setTenor(String tenor) {
		this.tenor = tenor;
	}


	public BigDecimal getFundrate8() {
		return fundrate8;
	}


	public void setFundrate8(BigDecimal fundrate8) {
		this.fundrate8 = fundrate8;
	}


	public String getFundbasis() {
		return fundbasis;
	}


	public void setFundbasis(String fundbasis) {
		this.fundbasis = fundbasis;
	}


	public String getFundcost() {
		return fundcost;
	}


	public void setFundcost(String fundcost) {
		this.fundcost = fundcost;
	}


	public BigDecimal getDiscrate8() {
		return discrate8;
	}


	public void setDiscrate8(BigDecimal discrate8) {
		this.discrate8 = discrate8;
	}


	public String getFirstdealno() {
		return firstdealno;
	}


	public void setFirstdealno(String firstdealno) {
		this.firstdealno = firstdealno;
	}


	public String getFixrule() {
		return fixrule;
	}


	public void setFixrule(String fixrule) {
		this.fixrule = fixrule;
	}


	public String getIntcalcrule() {
		return intcalcrule;
	}


	public void setIntcalcrule(String intcalcrule) {
		this.intcalcrule = intcalcrule;
	}


	public Date getFirstipaydate() {
		return firstipaydate;
	}


	public void setFirstipaydate(Date firstipaydate) {
		this.firstipaydate = firstipaydate;
	}


	public Date getLastipaydate() {
		return lastipaydate;
	}


	public void setLastipaydate(Date lastipaydate) {
		this.lastipaydate = lastipaydate;
	}


	public String getIntpaycycle() {
		return intpaycycle;
	}


	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}


	public String getPrinpaycycle() {
		return prinpaycycle;
	}


	public void setPrinpaycycle(String prinpaycycle) {
		this.prinpaycycle = prinpaycycle;
	}


	public String getIntpayday() {
		return intpayday;
	}


	public void setIntpayday(String intpayday) {
		this.intpayday = intpayday;
	}


	public String getPrinpayday() {
		return prinpayday;
	}


	public void setPrinpayday(String prinpayday) {
		this.prinpayday = prinpayday;
	}


	public BigDecimal getYield8() {
		return yield8;
	}


	public void setYield8(BigDecimal yield8) {
		this.yield8 = yield8;
	}


	public String getIntsmeans() {
		return intsmeans;
	}


	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}


	public String getFincal() {
		return fincal;
	}


	public void setFincal(String fincal) {
		this.fincal = fincal;
	}


	public String getSiind() {
		return siind;
	}


	public void setSiind(String siind) {
		this.siind = siind;
	}


	public String getIntsettauthind() {
		return intsettauthind;
	}


	public void setIntsettauthind(String intsettauthind) {
		this.intsettauthind = intsettauthind;
	}


	public String getIntauthoper() {
		return intauthoper;
	}


	public void setIntauthoper(String intauthoper) {
		this.intauthoper = intauthoper;
	}


	public Date getIntauthdate() {
		return intauthdate;
	}


	public void setIntauthdate(Date intauthdate) {
		this.intauthdate = intauthdate;
	}


	public String getSecid() {
		return secid;
	}


	public void setSecid(String secid) {
		this.secid = secid;
	}


	public String getCorptrad() {
		return corptrad;
	}


	public void setCorptrad(String corptrad) {
		this.corptrad = corptrad;
	}


	public String getCorpport() {
		return corpport;
	}


	public void setCorpport(String corpport) {
		this.corpport = corpport;
	}


	public String getAgentind() {
		return agentind;
	}


	public void setAgentind(String agentind) {
		this.agentind = agentind;
	}


	public Long getUpdatecounter() {
		return updatecounter;
	}


	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}


	public String getText1() {
		return text1;
	}


	public void setText1(String text1) {
		this.text1 = text1;
	}


	public String getText2() {
		return text2;
	}


	public void setText2(String text2) {
		this.text2 = text2;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public String getMatstatus() {
		return matstatus;
	}


	public void setMatstatus(String matstatus) {
		this.matstatus = matstatus;
	}


	public String getRevdealno() {
		return revdealno;
	}


	public void setRevdealno(String revdealno) {
		this.revdealno = revdealno;
	}


	public String getBtbdealno() {
		return btbdealno;
	}


	public void setBtbdealno(String btbdealno) {
		this.btbdealno = btbdealno;
	}


	public Date getAmenddate() {
		return amenddate;
	}


	public void setAmenddate(Date amenddate) {
		this.amenddate = amenddate;
	}


	public String getCustrefno() {
		return custrefno;
	}


	public void setCustrefno(String custrefno) {
		this.custrefno = custrefno;
	}


	public BigDecimal getIntrate8() {
		return intrate8;
	}


	public void setIntrate8(BigDecimal intrate8) {
		this.intrate8 = intrate8;
	}


	public String getRevoper() {
		return revoper;
	}


	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}


	public String getPayauthoper() {
		return payauthoper;
	}


	public void setPayauthoper(String payauthoper) {
		this.payauthoper = payauthoper;
	}


	public String getRecauthoper() {
		return recauthoper;
	}


	public void setRecauthoper(String recauthoper) {
		this.recauthoper = recauthoper;
	}


	public String getBtbtrad() {
		return btbtrad;
	}


	public void setBtbtrad(String btbtrad) {
		this.btbtrad = btbtrad;
	}


	public String getBtbport() {
		return btbport;
	}


	public void setBtbport(String btbport) {
		this.btbport = btbport;
	}


	public String getBtbcost() {
		return btbcost;
	}


	public void setBtbcost(String btbcost) {
		this.btbcost = btbcost;
	}


	public String getLinkdealno() {
		return linkdealno;
	}


	public void setLinkdealno(String linkdealno) {
		this.linkdealno = linkdealno;
	}


	public String getLinkproduct() {
		return linkproduct;
	}


	public void setLinkproduct(String linkproduct) {
		this.linkproduct = linkproduct;
	}


	public String getLinkprodtype() {
		return linkprodtype;
	}


	public void setLinkprodtype(String linkprodtype) {
		this.linkprodtype = linkprodtype;
	}


	public String getLinkseq() {
		return linkseq;
	}


	public void setLinkseq(String linkseq) {
		this.linkseq = linkseq;
	}


	public String getSipayind() {
		return sipayind;
	}


	public void setSipayind(String sipayind) {
		this.sipayind = sipayind;
	}


	public String getSwiftmatchind() {
		return swiftmatchind;
	}


	public void setSwiftmatchind(String swiftmatchind) {
		this.swiftmatchind = swiftmatchind;
	}


	public String getIntsacct() {
		return intsacct;
	}


	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}


	public String getCommusualid() {
		return commusualid;
	}


	public void setCommusualid(String commusualid) {
		this.commusualid = commusualid;
	}


	public String getMatusualid() {
		return matusualid;
	}


	public void setMatusualid(String matusualid) {
		this.matusualid = matusualid;
	}


	public String getCommtypecode() {
		return commtypecode;
	}


	public void setCommtypecode(String commtypecode) {
		this.commtypecode = commtypecode;
	}


	public String getMattypecode() {
		return mattypecode;
	}


	public void setMattypecode(String mattypecode) {
		this.mattypecode = mattypecode;
	}


	public String getScheduletype() {
		return scheduletype;
	}


	public void setScheduletype(String scheduletype) {
		this.scheduletype = scheduletype;
	}


	public String getPrindaterule() {
		return prindaterule;
	}


	public void setPrindaterule(String prindaterule) {
		this.prindaterule = prindaterule;
	}


	public String getPrindatedir() {
		return prindatedir;
	}


	public void setPrindatedir(String prindatedir) {
		this.prindatedir = prindatedir;
	}


	public BigDecimal getPrintotpayments() {
		return printotpayments;
	}


	public void setPrintotpayments(BigDecimal printotpayments) {
		this.printotpayments = printotpayments;
	}


	public String getIntdaterule() {
		return intdaterule;
	}


	public void setIntdaterule(String intdaterule) {
		this.intdaterule = intdaterule;
	}


	public String getIntdatedir() {
		return intdatedir;
	}


	public void setIntdatedir(String intdatedir) {
		this.intdatedir = intdatedir;
	}


	public BigDecimal getLastamount() {
		return lastamount;
	}


	public void setLastamount(BigDecimal lastamount) {
		this.lastamount = lastamount;
	}


	public String getBtbind() {
		return btbind;
	}


	public void setBtbind(String btbind) {
		this.btbind = btbind;
	}


	public String getBtbbranch() {
		return btbbranch;
	}


	public void setBtbbranch(String btbbranch) {
		this.btbbranch = btbbranch;
	}


	public String getBtbseq() {
		return btbseq;
	}


	public void setBtbseq(String btbseq) {
		this.btbseq = btbseq;
	}


	public String getBtbproduct() {
		return btbproduct;
	}


	public void setBtbproduct(String btbproduct) {
		this.btbproduct = btbproduct;
	}


	public String getBtbprodtype() {
		return btbprodtype;
	}


	public void setBtbprodtype(String btbprodtype) {
		this.btbprodtype = btbprodtype;
	}


	public String getBtbfundcenter() {
		return btbfundcenter;
	}


	public void setBtbfundcenter(String btbfundcenter) {
		this.btbfundcenter = btbfundcenter;
	}


	public String getDelayedintind() {
		return delayedintind;
	}


	public void setDelayedintind(String delayedintind) {
		this.delayedintind = delayedintind;
	}


	public Date getDelayedintdate() {
		return delayedintdate;
	}


	public void setDelayedintdate(Date delayedintdate) {
		this.delayedintdate = delayedintdate;
	}


	public String getEarlymatind() {
		return earlymatind;
	}


	public void setEarlymatind(String earlymatind) {
		this.earlymatind = earlymatind;
	}


	public BigDecimal getBrokexchrate8() {
		return brokexchrate8;
	}


	public void setBrokexchrate8(BigDecimal brokexchrate8) {
		this.brokexchrate8 = brokexchrate8;
	}


	public String getPrinsipayind() {
		return prinsipayind;
	}


	public void setPrinsipayind(String prinsipayind) {
		this.prinsipayind = prinsipayind;
	}


	public String getPrinsirecind() {
		return prinsirecind;
	}


	public void setPrinsirecind(String prinsirecind) {
		this.prinsirecind = prinsirecind;
	}


	public String getIntsipayind() {
		return intsipayind;
	}


	public void setIntsipayind(String intsipayind) {
		this.intsipayind = intsipayind;
	}


	public String getIntsirecind() {
		return intsirecind;
	}


	public void setIntsirecind(String intsirecind) {
		this.intsirecind = intsirecind;
	}


	public String getNetsi() {
		return netsi;
	}


	public void setNetsi(String netsi) {
		this.netsi = netsi;
	}


	public String getNetdealno() {
		return netdealno;
	}


	public void setNetdealno(String netdealno) {
		this.netdealno = netdealno;
	}


	public String getNetseq() {
		return netseq;
	}


	public void setNetseq(String netseq) {
		this.netseq = netseq;
	}


	public String getCorpnetind() {
		return corpnetind;
	}


	public void setCorpnetind(String corpnetind) {
		this.corpnetind = corpnetind;
	}


	public String getTaxchgdesc() {
		return taxchgdesc;
	}


	public void setTaxchgdesc(String taxchgdesc) {
		this.taxchgdesc = taxchgdesc;
	}


	public BigDecimal getTaxamt() {
		return taxamt;
	}


	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}


	public String getTaxauthority() {
		return taxauthority;
	}


	public void setTaxauthority(String taxauthority) {
		this.taxauthority = taxauthority;
	}


	public String getTaxproduct() {
		return taxproduct;
	}


	public void setTaxproduct(String taxproduct) {
		this.taxproduct = taxproduct;
	}


	public String getTaxprodtype() {
		return taxprodtype;
	}


	public void setTaxprodtype(String taxprodtype) {
		this.taxprodtype = taxprodtype;
	}


	public String getTaxtype() {
		return taxtype;
	}


	public void setTaxtype(String taxtype) {
		this.taxtype = taxtype;
	}


	public String getAcctgmethod() {
		return acctgmethod;
	}


	public void setAcctgmethod(String acctgmethod) {
		this.acctgmethod = acctgmethod;
	}


	public String getFundport() {
		return fundport;
	}


	public void setFundport(String fundport) {
		this.fundport = fundport;
	}


	public String getFundtrad() {
		return fundtrad;
	}


	public void setFundtrad(String fundtrad) {
		this.fundtrad = fundtrad;
	}


	public String getBtbcustomer() {
		return btbcustomer;
	}


	public void setBtbcustomer(String btbcustomer) {
		this.btbcustomer = btbcustomer;
	}


	public String getBtbcommusualid() {
		return btbcommusualid;
	}


	public void setBtbcommusualid(String btbcommusualid) {
		this.btbcommusualid = btbcommusualid;
	}


	public String getBtbmatusualid() {
		return btbmatusualid;
	}


	public void setBtbmatusualid(String btbmatusualid) {
		this.btbmatusualid = btbmatusualid;
	}


	public String getCorpdealno() {
		return corpdealno;
	}


	public void setCorpdealno(String corpdealno) {
		this.corpdealno = corpdealno;
	}


	public String getCorpseq() {
		return corpseq;
	}


	public void setCorpseq(String corpseq) {
		this.corpseq = corpseq;
	}


	public BigDecimal getStepuprate8() {
		return stepuprate8;
	}


	public void setStepuprate8(BigDecimal stepuprate8) {
		this.stepuprate8 = stepuprate8;
	}


	public String getFedealno() {
		return fedealno;
	}


	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}


	public String getServer() {
		return server;
	}


	public void setServer(String server) {
		this.server = server;
	}


	public BigDecimal getProfitrate8() {
		return profitrate8;
	}


	public void setProfitrate8(BigDecimal profitrate8) {
		this.profitrate8 = profitrate8;
	}


	public BigDecimal getTemplatetype() {
		return templatetype;
	}


	public void setTemplatetype(BigDecimal templatetype) {
		this.templatetype = templatetype;
	}


	public String getUcparty() {
		return ucparty;
	}


	public void setUcparty(String ucparty) {
		this.ucparty = ucparty;
	}


	public String getContactinfo() {
		return contactinfo;
	}


	public void setContactinfo(String contactinfo) {
		this.contactinfo = contactinfo;
	}


	public String getSpinst() {
		return spinst;
	}


	public void setSpinst(String spinst) {
		this.spinst = spinst;
	}


	public String getCusipisin() {
		return cusipisin;
	}


	public void setCusipisin(String cusipisin) {
		this.cusipisin = cusipisin;
	}


	public String getAllownegint() {
		return allownegint;
	}


	public void setAllownegint(String allownegint) {
		this.allownegint = allownegint;
	}


	public Date getEffectivestatusdate() {
		return effectivestatusdate;
	}


	public void setEffectivestatusdate(Date effectivestatusdate) {
		this.effectivestatusdate = effectivestatusdate;
	}


	public String getDealrebooked() {
		return dealrebooked;
	}


	public void setDealrebooked(String dealrebooked) {
		this.dealrebooked = dealrebooked;
	}


	public String getRebookeddeal() {
		return rebookeddeal;
	}


	public void setRebookeddeal(String rebookeddeal) {
		this.rebookeddeal = rebookeddeal;
	}


	private static final long serialVersionUID = 1L;
}