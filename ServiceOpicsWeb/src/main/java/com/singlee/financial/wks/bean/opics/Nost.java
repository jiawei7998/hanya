package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Nost implements Serializable {
    private String br;

    private String nos;

    private String ccy;

    private String cost;

    private String genledgno;

    private String cust;

    private BigDecimal acctbal;

    private BigDecimal amtbalprev;

    private Date lstmntdte;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getNos() {
		return nos;
	}


	public void setNos(String nos) {
		this.nos = nos;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getGenledgno() {
		return genledgno;
	}


	public void setGenledgno(String genledgno) {
		this.genledgno = genledgno;
	}


	public String getCust() {
		return cust;
	}


	public void setCust(String cust) {
		this.cust = cust;
	}


	public BigDecimal getAcctbal() {
		return acctbal;
	}


	public void setAcctbal(BigDecimal acctbal) {
		this.acctbal = acctbal;
	}


	public BigDecimal getAmtbalprev() {
		return amtbalprev;
	}


	public void setAmtbalprev(BigDecimal amtbalprev) {
		this.amtbalprev = amtbalprev;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	private static final long serialVersionUID = 1L;
}