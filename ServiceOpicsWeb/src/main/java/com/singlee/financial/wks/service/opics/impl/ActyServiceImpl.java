package com.singlee.financial.wks.service.opics.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.financial.wks.bean.opics.Acty;
import com.singlee.financial.wks.mapper.opics.ActyMapper;
import com.singlee.financial.wks.service.opics.ActyService;
@Service
public class ActyServiceImpl implements ActyService{

    @Resource
    private ActyMapper actyMapper;

    @Override
    public int deleteByPrimaryKey(String acctngtype) {
        return actyMapper.deleteByPrimaryKey(acctngtype);
    }

    @Override
    public int insert(Acty record) {
        return actyMapper.insert(record);
    }

    @Override
    public int insertSelective(Acty record) {
        return actyMapper.insertSelective(record);
    }

    @Override
    public Acty selectByPrimaryKey(String acctngtype) {
        return actyMapper.selectByPrimaryKey(acctngtype);
    }

    @Override
    public int updateByPrimaryKeySelective(Acty record) {
        return actyMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Acty record) {
        return actyMapper.updateByPrimaryKey(record);
    }

}
