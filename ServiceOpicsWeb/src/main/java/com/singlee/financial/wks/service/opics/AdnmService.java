package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Adnm;
import com.singlee.hessian.annotation.Opics;
import com.singlee.hessian.annotation.OpicsContext;

public interface AdnmService{


    int deleteByPrimaryKey(String br,String datatype,String product,String prodtype);

    int insert(Adnm record);

    int insertSelective(Adnm record);

    Adnm selectByPrimaryKey(String br,String datatype,String product,String prodtype);

    int updateByPrimaryKeySelective(Adnm record);

    int updateByPrimaryKey(Adnm record);

}
