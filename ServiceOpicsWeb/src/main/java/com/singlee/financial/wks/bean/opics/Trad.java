package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Trad implements Serializable {
    private String br;

    private String trad;

    private String port;

    private String tradname;

    private String portdesc;

    private Date lstmntdte;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getTrad() {
		return trad;
	}


	public void setTrad(String trad) {
		this.trad = trad;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getTradname() {
		return tradname;
	}


	public void setTradname(String tradname) {
		this.tradname = tradname;
	}


	public String getPortdesc() {
		return portdesc;
	}


	public void setPortdesc(String portdesc) {
		this.portdesc = portdesc;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	private static final long serialVersionUID = 1L;
}