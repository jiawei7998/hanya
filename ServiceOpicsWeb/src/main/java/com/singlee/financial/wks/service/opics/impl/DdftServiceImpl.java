package com.singlee.financial.wks.service.opics.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import java.util.Arrays;
import java.util.Date;

import com.singlee.financial.wks.bean.opics.Ddft;
import com.singlee.financial.wks.mapper.opics.DdftMapper;
import com.singlee.financial.wks.service.opics.DdftService;
@Service
public class DdftServiceImpl implements DdftService{

    @Resource
    private DdftMapper ddftMapper;

    @Override
    public int deleteByPrimaryKey(String br,String ccy,String yieldcurve,String shiftseq,Date brprocdate,Date matdate,String quotetype) {
        return ddftMapper.deleteByPrimaryKey(br,ccy,yieldcurve,shiftseq,brprocdate,matdate,quotetype);
    }

    @Override
    public int insert(Ddft record) {
        return ddftMapper.insert(record);
    }

    @Override
    public int insertSelective(Ddft record) {
        return ddftMapper.insertSelective(record);
    }

    @Override
    public Ddft selectByPrimaryKey(String br,String ccy,String yieldcurve,String shiftseq,Date brprocdate,Date matdate,String quotetype) {
        return ddftMapper.selectByPrimaryKey(br,ccy,yieldcurve,shiftseq,brprocdate,matdate,quotetype);
    }

    @Override
    public int updateByPrimaryKeySelective(Ddft record) {
        return ddftMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Ddft record) {
        return ddftMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateBatch(List<Ddft> list) {
        return ddftMapper.updateBatch(list);
    }

    @Override
    public int updateBatchSelective(List<Ddft> list) {
        return ddftMapper.updateBatchSelective(list);
    }

	@Override
	public int selectCountByBpdate(String br,Date brprocdate,String...ccy) {
		return ddftMapper.selectCountByBpdate(br,Arrays.asList(ccy),"000", brprocdate, "M","O/N");
	}

}
