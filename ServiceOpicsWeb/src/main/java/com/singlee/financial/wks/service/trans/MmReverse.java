package com.singlee.financial.wks.service.trans;


import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.IboWksBean;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.DldtService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

/**
 * 交易冲销转换方法
 *
 * @author shenzl
 * @date 2020/02/19
 */
@Service
public class MmReverse {
    @Autowired
    private DldtService dldtService;

    public void DataCheck(IboWksBean ibo) {
        // 1.判断拆借交易对象是否存在
        if (null == ibo) {
            JY.raise("%s:%s,请补充完整IboWksBean对象", WksErrorCode.IBO_NULL.getErrCode(), WksErrorCode.IBO_NULL.getErrMsg());
        }
        // 2.判断拆借交易机构是否存在
        if (StringUtils.isEmpty(ibo.getInstId())) {
            JY.raise("%s:%s,请填写IboWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
                    WksErrorCode.BR_NOT_FOUND.getErrMsg());
        }

        // 3.判断业务编号是否存在
        Dldt dldt = dldtService.selectByTradeNo(ibo.getInstId(), ibo.getTradeNo());
        if (StringUtils.isEmpty(ibo.getTradeNo()) || null == dldt) {
            JY.raise("%s:%s,请填写IboWksBean.TradeNo,For.[TradeNo=TRD20200101000001]",
                    WksErrorCode.IBO_NOT_FOUND.getErrCode(), WksErrorCode.IBO_NOT_FOUND.getErrMsg());
        }
    }

    /**
     * 冲销交易数据
     *
     * @param dldt
     * @param ibo
     * @return
     */
    public Dldt DataTrans(Dldt dldt, IboWksBean ibo, BrpsService brpsService) {
        Brps brps = brpsService.selectByPrimaryKey(dldt.getBr());
        dldt.setRevreason("99");// 冲销参数:默认99
        dldt.setRevdate(brps.getBranprcdate());// 冲销日期
        dldt.setRevtext("");// 冲销描述
        dldt.setRevoper(OpicsConstant.DL.TRAD_NO);// 冲销复核人员
        dldt.setUpdatecounter(dldt.getUpdatecounter() + 1);// 修改次数
        dldt.setLstmntdate(Calendar.getInstance().getTime());// 最后修改日期
        return dldt;
    }
}
