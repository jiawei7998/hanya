package com.singlee.financial.wks.service.opics.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.singlee.financial.marketdata.bean.SubscriptionCode;
import com.singlee.financial.wks.mapper.opics.RsrmMapper;
import com.singlee.financial.wks.service.opics.RsrmService;

@Service
public class RsrmServiceImpl implements RsrmService {
	@Autowired
	private RsrmMapper  rsrmMapper;

	@Override
	public List<SubscriptionCode> queryByGroupID(String groupId) {
		return rsrmMapper.selectByGroupId(groupId);
	}

}
