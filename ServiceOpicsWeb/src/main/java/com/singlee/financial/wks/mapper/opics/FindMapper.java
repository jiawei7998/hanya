package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Find;
import org.apache.ibatis.annotations.Param;

public interface FindMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("product") String product, @Param("prodtype") String prodtype, @Param("incexptype") String incexptype);

    int insert(Find record);

    int insertSelective(Find record);

    Find selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("product") String product, @Param("prodtype") String prodtype, @Param("incexptype") String incexptype);

    int updateByPrimaryKeySelective(Find record);

    int updateByPrimaryKey(Find record);
}