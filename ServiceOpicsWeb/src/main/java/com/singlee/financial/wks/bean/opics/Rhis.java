package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Rhis implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4692524835277924566L;

	private String br;

    private String ratecode;

    private Date effdate;

    private String intrate;

    private Date lstmntdate;

    private String ioper;

    private String voper;

    private String verind;

    private Date verdate;

    private Date amenddate;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getRatecode() {
		return ratecode;
	}

	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}

	public Date getEffdate() {
		return effdate;
	}

	public void setEffdate(Date effdate) {
		this.effdate = effdate;
	}

	public String getIntrate() {
		return intrate;
	}

	public void setIntrate(String intrate) {
		this.intrate = intrate;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public Date getAmenddate() {
		return amenddate;
	}

	public void setAmenddate(Date amenddate) {
		this.amenddate = amenddate;
	}
    
    
}