package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Otdt;
import org.apache.ibatis.annotations.Param;

public interface OtdtMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    int insert(Otdt record);

    int insertSelective(Otdt record);

    Otdt selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    Otdt selectByTradeNo(@Param("br") String br, @Param("dealtext") String tradeNo,@Param("status") String status);

    int updateByPrimaryKeySelective(Otdt record);

    int updateByPrimaryKey(Otdt record);
    
    Otdt selectTriggerByTradeNo(@Param("br") String br, @Param("dealtext") String tradeNo,@Param("status") String status);
}