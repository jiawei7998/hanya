package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Csri;
import org.apache.ibatis.annotations.Param;

public interface CsriMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("cust") String cust, @Param("product") String product, @Param("type") String type, @Param("ccy") String ccy);

    int insert(Csri record);

    int insertSelective(Csri record);

    Csri selectByPrimaryKey(@Param("br") String br, @Param("cust") String cust, @Param("product") String product, @Param("type") String type, @Param("ccy") String ccy);

    int updateByPrimaryKeySelective(Csri record);

    int updateByPrimaryKey(Csri record);
}