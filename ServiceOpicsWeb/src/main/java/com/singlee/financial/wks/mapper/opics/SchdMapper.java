package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Schd;
import java.util.Date;

import org.apache.ibatis.annotations.Param;

public interface SchdMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq,
        @Param("product") String product, @Param("prodtype") String prodtype, @Param("intenddte") Date intenddte,
        @Param("schdseq") String schdseq, @Param("schdtype") String schdtype);
    int deleteSchdByIntEndDte(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq,
            @Param("product") String product, @Param("prodtype") String prodtype, @Param("intenddte") Date intenddte);

    int insert(Schd record);

    int insertSelective(Schd record);

    Schd selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq,
        @Param("product") String product, @Param("prodtype") String prodtype, @Param("intenddte") Date intenddte,
        @Param("schdseq") String schdseq, @Param("schdtype") String schdtype);

    int updateByPrimaryKeySelective(Schd record);

    int updateByPrimaryKey(Schd record);
    int updateReverseByPrimaryKey(Schd record);
}