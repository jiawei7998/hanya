package com.singlee.financial.wks.service.opics.impl;

import org.springframework.stereotype.Service;

import com.singlee.financial.wks.bean.opics.Ychd;
import com.singlee.financial.wks.mapper.opics.YchdMapper;
import com.singlee.financial.wks.service.opics.YchdService;
import javax.annotation.Resource;
import java.util.List;

@Service
public class YchdServiceImpl implements YchdService {

	@Resource
	private YchdMapper ychdMapper;

	@Override
	public int deleteByPrimaryKey(String br, String ccy, String yieldcurve) {
		return ychdMapper.deleteByPrimaryKey(br, ccy, yieldcurve);
	}

	@Override
	public int insert(Ychd record) {
		return ychdMapper.insert(record);
	}

	@Override
	public int insertSelective(Ychd record) {
		return ychdMapper.insertSelective(record);
	}

	@Override
	public Ychd selectByPrimaryKey(String br, String ccy, String yieldcurve) {
		return ychdMapper.selectByPrimaryKey(br, ccy, yieldcurve);
	}

	@Override
	public int updateByPrimaryKeySelective(Ychd record) {
		return ychdMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Ychd record) {
		return ychdMapper.updateByPrimaryKey(record);
	}

	@Override
	public int updateBatch(List<Ychd> list) {
		return ychdMapper.updateBatch(list);
	}

	@Override
	public int updateBatchSelective(List<Ychd> list) {
		return ychdMapper.updateBatchSelective(list);
	}

}
