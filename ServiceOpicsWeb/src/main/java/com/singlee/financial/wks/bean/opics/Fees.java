package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Fees implements Serializable {
    private String br;

    private String feeno;

    private String feeseq;

    private String feeproduct;

    private String feeprodtype;

    private String dealno;

    private String seq;

    private String product;

    private String prodtype;

    private String ccy;

    private BigDecimal ccyamt;

    private String cost;

    private String cno;

    private String tenor;

    private String al;

    private String acctgmethod;

    private Date startdate;

    private Date enddate;

    private Date vdate;

    private String ioper;

    private Date inputdate;

    private String inputtime;

    private Date brprcindte;

    private String verind;

    private String veroper;

    private Date verdate;

    private String settmeans;

    private String settacct;

    private String settauthind;

    private Date settauthdte;

    private String settoper;

    private String feetext;

    private String revreason;

    private Date revdate;

    private String revtext;

    private Date lstmntdate;

    private String secid;

    private String feesind;

    private BigDecimal feeper8;

    private Date dealmdate;

    private BigDecimal dealccyamt;

    private Date dealvdate;

    private String port;

    private String trad;

    private Date dealdate;

    private String fincent;

    private String coverbyfx;

    private String settccy;

    private BigDecimal settexchrate8;

    private String settexchterms;

    private BigDecimal settamtperiod;

    private BigDecimal dealamtperiod;

    private BigDecimal baseamtperiod;

    private String netdeal;

    private String netfeeno;

    private String origcno;

    private Date lstpaiddate;

    private Long updatecounter;

    private String amortmethod;

    private String feetype;

    private String basis;

    private BigDecimal baseccyamt;
    
    

    public String getBr() {
		return br;
	}



	public void setBr(String br) {
		this.br = br;
	}



	public String getFeeno() {
		return feeno;
	}



	public void setFeeno(String feeno) {
		this.feeno = feeno;
	}



	public String getFeeseq() {
		return feeseq;
	}



	public void setFeeseq(String feeseq) {
		this.feeseq = feeseq;
	}



	public String getFeeproduct() {
		return feeproduct;
	}



	public void setFeeproduct(String feeproduct) {
		this.feeproduct = feeproduct;
	}



	public String getFeeprodtype() {
		return feeprodtype;
	}



	public void setFeeprodtype(String feeprodtype) {
		this.feeprodtype = feeprodtype;
	}



	public String getDealno() {
		return dealno;
	}



	public void setDealno(String dealno) {
		this.dealno = dealno;
	}



	public String getSeq() {
		return seq;
	}



	public void setSeq(String seq) {
		this.seq = seq;
	}



	public String getProduct() {
		return product;
	}



	public void setProduct(String product) {
		this.product = product;
	}



	public String getProdtype() {
		return prodtype;
	}



	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}



	public String getCcy() {
		return ccy;
	}



	public void setCcy(String ccy) {
		this.ccy = ccy;
	}



	public BigDecimal getCcyamt() {
		return ccyamt;
	}



	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}



	public String getCost() {
		return cost;
	}



	public void setCost(String cost) {
		this.cost = cost;
	}



	public String getCno() {
		return cno;
	}



	public void setCno(String cno) {
		this.cno = cno;
	}



	public String getTenor() {
		return tenor;
	}



	public void setTenor(String tenor) {
		this.tenor = tenor;
	}



	public String getAl() {
		return al;
	}



	public void setAl(String al) {
		this.al = al;
	}



	public String getAcctgmethod() {
		return acctgmethod;
	}



	public void setAcctgmethod(String acctgmethod) {
		this.acctgmethod = acctgmethod;
	}



	public Date getStartdate() {
		return startdate;
	}



	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}



	public Date getEnddate() {
		return enddate;
	}



	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}



	public Date getVdate() {
		return vdate;
	}



	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}



	public String getIoper() {
		return ioper;
	}



	public void setIoper(String ioper) {
		this.ioper = ioper;
	}



	public Date getInputdate() {
		return inputdate;
	}



	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}



	public String getInputtime() {
		return inputtime;
	}



	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}



	public Date getBrprcindte() {
		return brprcindte;
	}



	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}



	public String getVerind() {
		return verind;
	}



	public void setVerind(String verind) {
		this.verind = verind;
	}



	public String getVeroper() {
		return veroper;
	}



	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}



	public Date getVerdate() {
		return verdate;
	}



	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}



	public String getSettmeans() {
		return settmeans;
	}



	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}



	public String getSettacct() {
		return settacct;
	}



	public void setSettacct(String settacct) {
		this.settacct = settacct;
	}



	public String getSettauthind() {
		return settauthind;
	}



	public void setSettauthind(String settauthind) {
		this.settauthind = settauthind;
	}



	public Date getSettauthdte() {
		return settauthdte;
	}



	public void setSettauthdte(Date settauthdte) {
		this.settauthdte = settauthdte;
	}



	public String getSettoper() {
		return settoper;
	}



	public void setSettoper(String settoper) {
		this.settoper = settoper;
	}



	public String getFeetext() {
		return feetext;
	}



	public void setFeetext(String feetext) {
		this.feetext = feetext;
	}



	public String getRevreason() {
		return revreason;
	}



	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}



	public Date getRevdate() {
		return revdate;
	}



	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}



	public String getRevtext() {
		return revtext;
	}



	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}



	public Date getLstmntdate() {
		return lstmntdate;
	}



	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}



	public String getSecid() {
		return secid;
	}



	public void setSecid(String secid) {
		this.secid = secid;
	}



	public String getFeesind() {
		return feesind;
	}



	public void setFeesind(String feesind) {
		this.feesind = feesind;
	}



	public BigDecimal getFeeper8() {
		return feeper8;
	}



	public void setFeeper8(BigDecimal feeper8) {
		this.feeper8 = feeper8;
	}



	public Date getDealmdate() {
		return dealmdate;
	}



	public void setDealmdate(Date dealmdate) {
		this.dealmdate = dealmdate;
	}



	public BigDecimal getDealccyamt() {
		return dealccyamt;
	}



	public void setDealccyamt(BigDecimal dealccyamt) {
		this.dealccyamt = dealccyamt;
	}



	public Date getDealvdate() {
		return dealvdate;
	}



	public void setDealvdate(Date dealvdate) {
		this.dealvdate = dealvdate;
	}



	public String getPort() {
		return port;
	}



	public void setPort(String port) {
		this.port = port;
	}



	public String getTrad() {
		return trad;
	}



	public void setTrad(String trad) {
		this.trad = trad;
	}



	public Date getDealdate() {
		return dealdate;
	}



	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}



	public String getFincent() {
		return fincent;
	}



	public void setFincent(String fincent) {
		this.fincent = fincent;
	}



	public String getCoverbyfx() {
		return coverbyfx;
	}



	public void setCoverbyfx(String coverbyfx) {
		this.coverbyfx = coverbyfx;
	}



	public String getSettccy() {
		return settccy;
	}



	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}



	public BigDecimal getSettexchrate8() {
		return settexchrate8;
	}



	public void setSettexchrate8(BigDecimal settexchrate8) {
		this.settexchrate8 = settexchrate8;
	}



	public String getSettexchterms() {
		return settexchterms;
	}



	public void setSettexchterms(String settexchterms) {
		this.settexchterms = settexchterms;
	}



	public BigDecimal getSettamtperiod() {
		return settamtperiod;
	}



	public void setSettamtperiod(BigDecimal settamtperiod) {
		this.settamtperiod = settamtperiod;
	}



	public BigDecimal getDealamtperiod() {
		return dealamtperiod;
	}



	public void setDealamtperiod(BigDecimal dealamtperiod) {
		this.dealamtperiod = dealamtperiod;
	}



	public BigDecimal getBaseamtperiod() {
		return baseamtperiod;
	}



	public void setBaseamtperiod(BigDecimal baseamtperiod) {
		this.baseamtperiod = baseamtperiod;
	}



	public String getNetdeal() {
		return netdeal;
	}



	public void setNetdeal(String netdeal) {
		this.netdeal = netdeal;
	}



	public String getNetfeeno() {
		return netfeeno;
	}



	public void setNetfeeno(String netfeeno) {
		this.netfeeno = netfeeno;
	}



	public String getOrigcno() {
		return origcno;
	}



	public void setOrigcno(String origcno) {
		this.origcno = origcno;
	}



	public Date getLstpaiddate() {
		return lstpaiddate;
	}



	public void setLstpaiddate(Date lstpaiddate) {
		this.lstpaiddate = lstpaiddate;
	}



	public Long getUpdatecounter() {
		return updatecounter;
	}



	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}



	public String getAmortmethod() {
		return amortmethod;
	}



	public void setAmortmethod(String amortmethod) {
		this.amortmethod = amortmethod;
	}



	public String getFeetype() {
		return feetype;
	}



	public void setFeetype(String feetype) {
		this.feetype = feetype;
	}



	public String getBasis() {
		return basis;
	}



	public void setBasis(String basis) {
		this.basis = basis;
	}



	public BigDecimal getBaseccyamt() {
		return baseccyamt;
	}



	public void setBaseccyamt(BigDecimal baseccyamt) {
		this.baseccyamt = baseccyamt;
	}



	private static final long serialVersionUID = 1L;
}