package com.singlee.financial.wks.service.opics;

import java.util.List;
import com.singlee.financial.wks.bean.opics.Ycim;
public interface YcimService{


    int deleteByPrimaryKey(String br,String ccy,String contribrate);

    int insert(Ycim record);

    int insertSelective(Ycim record);

    Ycim selectByPrimaryKey(String br,String ccy,String contribrate);

    int updateByPrimaryKeySelective(Ycim record);

    int updateByPrimaryKey(Ycim record);

    int updateBatch(List<Ycim> list);

    int updateBatchSelective(List<Ycim> list);

}
