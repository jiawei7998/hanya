package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.IboWksBean;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.IboIntSchdWksBean;
import com.singlee.financial.wks.expand.IboPrinSchdWksBean;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.util.FinancialFieldMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * 拆借交易首期转换方法
 *
 * @author shenzl
 * @date 2020/02/19
 */
@Component
public class MmEntry {

	@Autowired
	private BrpsService brpsService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private PortService portService;
	@Autowired
	private CcodService ccodService;
	@Autowired
	private CostService costService;
	@Autowired
	private CustService custService;
	@Autowired
	private RateService rateService;
	@Autowired
	private TradService tradService;
	@Autowired
	private NostService nostService;
	@Autowired
	private FinancialFieldMap financialFieldMap;

	/**
	 * 交易信息检查
	 *
	 * @param ibo
	 */
	public void DataCheck(IboWksBean ibo) {
		// 1.判断拆借交易对象是否存在
		if (null == ibo) {
			JY.raise("%s:%s,请补充完整IboWksBean对象", WksErrorCode.IBO_NULL.getErrCode(), WksErrorCode.IBO_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(ibo.getInstId())) {
			JY.raise("%s:%s,请填写IboWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NULL.getErrCode(),
					WksErrorCode.BR_NULL.getErrMsg());
		}
		// 3.查询分支/机构是否存在
		Brps brps = brpsService.selectByPrimaryKey(ibo.getInstId());
		if (StringUtils.isEmpty(ibo.getInstId()) || null == brps) {
			JY.raise("%s:%s,请正确输入IboWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 4.检查产品类型DESK字段拆分PROD_TYPE_PORT
		String[] book = StringUtils.split(ibo.getBook(), "_");
		if (StringUtils.isEmpty(ibo.getBook()) || book.length != 3) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.book,For.[book=MM_BR_100]", WksErrorCode.DESK_FORM_ERR.getErrCode(),
					WksErrorCode.DESK_FORM_ERR.getErrMsg());
		}
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		if (StringUtils.isEmpty(book[0]) || StringUtils.isEmpty(book[1]) || null == type) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.book,For.[book=MM_BR_100]", WksErrorCode.DESK_NOT_FOUND.getErrCode(),
					WksErrorCode.DESK_NOT_FOUND.getErrMsg());
		}
		Port port = portService.selectByPrimaryKey(ibo.getInstId(), ibo.getDesk());// 投组组合
		if (StringUtils.isEmpty(ibo.getDesk()) || null == port) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.desk,For.[desk=PORT]", WksErrorCode.DESK_PORT_ERR.getErrCode(),
					WksErrorCode.DESK_PORT_ERR.getErrMsg());
		}
		// 5.检查成本中心
		Cost cost = costService.selectByPrimaryKey(book[2]);
		if (StringUtils.isEmpty(book[2]) || null == cost) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.book,For.[book=MM_BR_100,或查询RATE表]",
					WksErrorCode.BOOK_NOT_FOUND.getErrCode(), WksErrorCode.BOOK_NOT_FOUND.getErrMsg());
		}
		// 6.检查交易对手
		Cust cust = custService.selectByPrimaryKey(ibo.getCno());
		if (StringUtils.isEmpty(ibo.getCno()) || null == cust) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.cno,For.[cno=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(),
					WksErrorCode.CUST_NOT_FOUND.getErrMsg());
		}
		// 7.检查交易员
		Trad trad = tradService.selectByPrimaryKey(ibo.getInstId(), ibo.getTrad());
		if (StringUtils.isNotEmpty(ibo.getTrad()) && null == trad) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.trad,For.[trad=TRAD]", WksErrorCode.TRAD_NOT_FOUND.getErrCode(),
					WksErrorCode.TRAD_NOT_FOUND.getErrMsg());
		}
		// 8.校验币种是否存在
		Ccod ccod = ccodService.selectByPrimaryKey(ibo.getCcy());
		if (StringUtils.isEmpty(ibo.getCcy()) || null == ccod) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.ccy,For.[ccy=CNY]", WksErrorCode.CCY_NOT_FOUND.getErrCode(),
					WksErrorCode.CCY_NOT_FOUND.getErrMsg());
		}
		// 9.检查利率代码是否存在
		Rate rate = rateService.selectByCcy(ibo.getInstId(), ibo.getCcy(), "F");
		if (StringUtils.isEmpty(ibo.getCcy()) || null == rate) {
			JY.raise("%s:%s,请输入正确格式IboWksBean.ratecode,For.[ratecode=FIXED,或查询RATE表]",
					WksErrorCode.RATE_CODE_NOT_FOUND.getErrCode(), WksErrorCode.RATE_CODE_NOT_FOUND.getErrMsg());
		}
		// 10.检查本方清算账户是否存在
		if (!StringUtils.equals(ibo.getCcy(), OpicsConstant.BRRD.LOCAL_CCY)) {
			String intHostBic = ibo.getIntSchdList().get(0).getSettleAcct().getHostCcyAwBiccode();
			String prinHostBic = ibo.getPrinSchdList().get(0).getSettleAcct().getHostCcyAwBiccode();
			List<String> intAccts = nostService.selectNosByCust(ibo.getInstId(), ibo.getCcy(), intHostBic);
			if (StringUtils.isEmpty(intHostBic) || null == intAccts || intAccts.size() == 0) {
				JY.raise(
						"%s:%s,请输入正确格式IboWksBean.intSchdList.settleAcct.hostCcyAwBiccode,For.[hostCcyAwBiccode=BKCHCNBJXXX]",
						WksErrorCode.HOST_BIC_NOT_FOUND.getErrCode(), WksErrorCode.HOST_BIC_NOT_FOUND.getErrMsg());
			}
			List<String> prinAccts = nostService.selectNosByCust(ibo.getInstId(), ibo.getCcy(), prinHostBic);
			if (StringUtils.isEmpty(prinHostBic) || null == prinAccts || prinAccts.size() == 0) {
				JY.raise(
						"%s:%s,请输入正确格式IboWksBean.prinSchdList.settleAcct.hostCcyAwBiccode,For.[hostCcyAwBiccode=BKCHCNBJXXX]",
						WksErrorCode.HOST_BIC_NOT_FOUND.getErrCode(), WksErrorCode.HOST_BIC_NOT_FOUND.getErrMsg());
			}
		}
	}

	/**
	 * 静态默认数据,该处禁止查询数据库进行转换值,会出现空指针
	 *
	 * @param dldt
	 * @return
	 */
	public Dldt DataTrans(Dldt dldt) {
		dldt.setSeq(OpicsConstant.DL.SEQ_ZERO);// 默认值
		dldt.setAcrfrstlst(OpicsConstant.DL.ACR_FRST_LST);// 默认第一次/最后一次结息标志
		dldt.setVerind(OpicsConstant.DL.VER_IND);// 复核标志 1-已复核
		dldt.setDealsrce(OpicsConstant.DL.DEAL_SRCE);// 交易来源
		dldt.setCommmeans(OpicsConstant.DL.COMM_MEANS);// 未到期结算方式
		dldt.setSchdflag(OpicsConstant.DL.SCHD_FLAG);
		dldt.setIntcapflag(OpicsConstant.DL.INT_CAP_FLAG);
		dldt.setTenor(OpicsConstant.DL.TENOR);// 期限
		dldt.setSiind(OpicsConstant.DL.SI_IND);// 标准指令
		dldt.setAgentind(OpicsConstant.DL.AGENT_IND);// 代理标志
		dldt.setBtbind(OpicsConstant.DL.BTB_IND);// 背对背交易标志
		dldt.setDelayedintind(OpicsConstant.DL.DELAYED_INT_IND);// 延期利息标志
		dldt.setEarlymatind(OpicsConstant.DL.EARLY_MAT_IND);// 提前还款标志
		dldt.setCorpnetind(OpicsConstant.DL.CORP_NET_IND);// CORPNTIND
		dldt.setAllownegint(OpicsConstant.DL.ALLOW_NEGINT);//
		dldt.setDealrebooked(OpicsConstant.DL.DEAL_REBOOKED);// 交易重新启动
		dldt.setRebookeddeal(OpicsConstant.DL.REBOOKED_DEAL);// 重新预订的交易
		dldt.setUpdatecounter(OpicsConstant.DL.UPDATE_COUNTER);// 更新次数
		dldt.setIntcapamt(BigDecimal.ZERO);// 利息转为本金金额
		dldt.setLienamt(BigDecimal.ZERO);// 抵押品利息
		dldt.setPrmdscamrtd(BigDecimal.ZERO);// 升水/贴水
		dldt.setEmpenamt(BigDecimal.ZERO);// 提前还款罚款
		dldt.setDiscrate8(BigDecimal.ZERO);// 贴现率
		dldt.setAmount1(BigDecimal.ZERO);// 备用5
		dldt.setAmount2(BigDecimal.ZERO);// 备用6
		dldt.setPrintotpayments(BigDecimal.ZERO);// 本金总额
		dldt.setLastamount(BigDecimal.ZERO);// 上次本金金额
		dldt.setBrokexchrate8(BigDecimal.ZERO);// 经纪人汇率
		dldt.setTaxamt(BigDecimal.ZERO);// 税金金额
		dldt.setStepuprate8(BigDecimal.ZERO);// 利率调幅
		dldt.setTemplatetype(BigDecimal.ZERO);//
		dldt.setSpread8(BigDecimal.ZERO);// 点差
		dldt.setBrokamt(BigDecimal.ZERO);// 经纪人费用
		dldt.setBrokrate8(BigDecimal.ZERO);// 经纪人费率
		return dldt;
	}

	/**
	 * 获取dealno 拆借业务的单号比较特殊,需要进行特殊处理
	 *
	 * @param dldt
	 * @param mcfpService
	 * @return
	 */
	public Dldt DataTrans(Dldt dldt, McfpService mcfpService) {
		// 获取DEALNO
		Mcfp mcfp = mcfpService.selectByPrimaryKey(dldt.getBr(), OpicsConstant.DL.PROD_CODE,
				OpicsConstant.DL.PROD_TYPE);// 设置交易编号
		String dealno = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";// 获取
		dldt.setDealno(dealno);// 设置交易编号
		return dldt;
	}

	/**
	 * 更新交易单号 拆借业务的单号比较特殊,需要进行特殊处理
	 *
	 * @param dldt
	 * @param mcfpService
	 * @param mcfp
	 */
	public void DataTrans(Dldt dldt, McfpService mcfpService, Mcfp mcfp) {
		mcfp.setBr(dldt.getBr());
		mcfp.setTraddno(StringUtils.trimToEmpty(dldt.getDealno()));
		mcfp.setProdcode(OpicsConstant.DL.PROD_CODE);// 产品代码 去掉空格
		mcfp.setType(OpicsConstant.DL.PROD_TYPE);// 产品类型
		mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
	}

	/**
	 * 交易数据
	 *
	 * @param dldt
	 * @param ibo
	 * @return
	 */
	public Dldt DataTrans(Dldt dldt, IboWksBean ibo) {
		// 转换资金工作站IboWksBean对象中的枚举值
		financialFieldMap.convertFieldvalue(ibo);
		dldt.setBr(ibo.getInstId());// branch
		// 获取当前账务日期
		Brps brps = brpsService.selectByPrimaryKey(dldt.getBr());
		dldt.setBrprcindte(brps.getBranprcdate());// 当前账务日期
		// 设置BOOK
		String[] book = StringUtils.split(ibo.getBook(), "_");
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		dldt.setProduct(book[0]);// 产品
		dldt.setProdtype(book[1]);// 类型
		dldt.setPort(ibo.getDesk());// 投资组合
		dldt.setCost(book[2]);// 成本中心
		dldt.setTrad(StringUtils.defaultIfEmpty(ibo.getTrad(), OpicsConstant.DL.TRAD_NO));// 交易员
		dldt.setCno(ibo.getCno());// 交易对手
		dldt.setDealdate(DateUtil.parse(ibo.getDealDate()));// 交易日期
		dldt.setVerdate(dldt.getBrprcindte());// 复核日期
		dldt.setVdate(DateUtil.parse(ibo.getVdate()));// 交割日期
		dldt.setMdate(DateUtil.parse(ibo.getMdate()));// 到日期
		dldt.setAl(type.getAl());// 资产负债标识
		dldt.setCcy(ibo.getCcy());// 货币代码
		Rate rate = rateService.selectByCcy(dldt.getBr(), dldt.getCcy(), "F");// 获取00网点币种的FIXED
		dldt.setRatecode(StringUtils.stripToEmpty(rate.getRatecode()));// 后续转换
		dldt.setCcyamt(getPlusNegateAmt(dldt.getAl(), ibo.getCcyamt()));// 资金金额
		dldt.setOrigocamt(getPlusNegateAmt(dldt.getAl(), ibo.getCcyamt()));// 初始资金金额
		dldt.setBasis(ibo.getBasis());// 计息基础
		dldt.setIntrate8(ibo.getIntrate8());// 利率
		dldt.setIntrate(String.valueOf(ibo.getIntrate8()));// 利率
		dldt.setFundrate8(ibo.getIntrate8());// 融资利率
		dldt.setFundcost(dldt.getCost());
		dldt.setFundbasis(dldt.getBasis());// 融资计息规则
		dldt.setFirstipaydate(DateUtil.parse(ibo.getVdate()));// 第一次付息日
		dldt.setLastipaydate(DateUtil.parse(ibo.getMdate()));// 上一次付息日
		dldt.setYield8(ibo.getIntrate8());// 收益率
		dldt.setProfitrate8(ibo.getIntrate8());// 投组利率
		dldt.setDealtime(DateUtil.getCurrentTimeAsString());// 交易时间
		dldt.setInputdate(Calendar.getInstance().getTime());// 输入日期
		dldt.setInputtime(DateUtil.getCurrentTimeAsString());// 输入时间
		dldt.setLstmntdate(Calendar.getInstance().getTime());// 上次更新日期
		dldt.setDealtext(ibo.getTradeNo());// 外部交易流水唯一编号
		dldt.setIoper(StringUtils.defaultIfEmpty(ibo.getTrad(), OpicsConstant.DL.TRAD_NO));// 操作员
		dldt.setVoper(StringUtils.defaultIfEmpty(ibo.getTrad(), OpicsConstant.DL.TRAD_NO));// 复核人

		if (ibo.getPrinSchdList().size() > 2 && ibo.getIntSchdList().size() > 1) {// 本金利息进行调整了
			dldt.setScheduletype(OpicsConstant.DL.SCHEDULE_TYPE_PRIN);
			dldt.setPrinpaycycle(ibo.getSechuleType());// 还本频率
			dldt.setIntdaterule("M");//
			dldt.setIntdatedir("MB");
			dldt.setIntpaycycle(ibo.getSechuleType());// 付息频率
			dldt.setIntdaterule("M");
			dldt.setIntdatedir("MB");
		} else if (ibo.getIntSchdList().size() > 1) {// 利息进行调整了
			dldt.setScheduletype(OpicsConstant.DL.SCHEDULE_TYPE_INT);// D -不调整 I-调整利息 P-本金和利息都调整
			dldt.setIntpaycycle(ibo.getSechuleType()); // 付息频率
			dldt.setIntdaterule("M");
			dldt.setIntdatedir("MB");
		} else {// 不调整
			dldt.setScheduletype(OpicsConstant.DL.SCHEDULE_TYPE_DEF);// D -不调整 I-调整利息 P-本金和利息都调整
		}
		return dldt;
	}

	/**
	 * 组装SCHD对象
	 *
	 * @param ibo
	 * @param dldt
	 * @return
	 */
	public List<Schd> DataTrans(IboWksBean ibo, Dldt dldt) {
		// 转换资金工作站IboWksBean对象中的枚举值
		financialFieldMap.convertFieldvalue(ibo);
		Map<Date, Schd> schdMap = Collections.synchronizedMap(new HashMap<Date, Schd>());
		// 预处理
		List<IboPrinSchdWksBean> prinSchdList = ibo.getPrinSchdList();
		Map<Date, Schd> intSchdMap = Collections.synchronizedMap(new HashMap<Date, Schd>());
		// 先把所有的利息放入map
		for (IboIntSchdWksBean iiswb : ibo.getIntSchdList()) {
			Schd intschd = DataTrans(iiswb, new Schd(), dldt);
			intSchdMap.put(intschd.getIpaydate(), intschd);
		}
		// 根据本金循环获取
		for (IboPrinSchdWksBean prin : prinSchdList) {// 说明有值
			Schd prinSchd = DataTrans(prin, new Schd(), dldt);
			// 查找intmap是否有相等的,有则取出
			Schd intSchd = intSchdMap.get(prinSchd.getIpaydate());
			if (null != intSchd) {
				// 合并相加处理
				System.out.println(String.format("开始合并第%s利息与本金第%s期的信息匹配,匹配日期%s", intSchd.getSchdseq(),
						prinSchd.getSchdseq(), intSchd.getIpaydate()));
				prinSchd.setSchdseq(StringUtils.leftPad(intSchd.getSchdseq(), 3, "0"));// 序号重新格式化
				prinSchd.setTotpayamt(prinSchd.getTotpayamt().add(intSchd.getTotpayamt()));// 利息金额相加
				prinSchd.setIntamt(intSchd.getIntamt());// 利息
				prinSchd.setPosnintamt(intSchd.getIntamt());// 利息
				prinSchd.setFundamt(intSchd.getIntamt());// 融资金额
				prinSchd.setPosnfundamt(intSchd.getIntamt());// 货币头寸
				prinSchd.setIntstrtdte(intSchd.getIntstrtdte());// 开始日期
				prinSchd.setIntenddte(intSchd.getIntenddte());// 结束日期
				intSchdMap.remove(prinSchd.getIpaydate()); // 移除已被合并的
			}
			schdMap.put(prinSchd.getIpaydate(), prinSchd);// 处理完成放入map
		}
		schdMap.putAll(intSchdMap);// 将利息与本金的剩余条数完全合并
		// 排序
		List<Schd> list = new ArrayList<Schd>(schdMap.values());
		Collections.sort(list, new Comparator<Schd>() {
			@Override
			public int compare(Schd o1, Schd o2) {
				return Integer.parseInt(o1.getSchdseq()) - Integer.parseInt(o2.getSchdseq());
			}
		});
		return list;
	}

	/**
	 * 组装SCHD静态数据,该处禁止查询数据库进行转换值,会出现空指针
	 *
	 * @param schd
	 * @return
	 */
	private Schd DataTrans(Schd schd, Dldt dldt) {
		schd.setBr(dldt.getBr());// 代码
		schd.setDealno(dldt.getDealno());// 交易流水号
		schd.setProduct(dldt.getProduct());// 产品代码
		schd.setProdtype(dldt.getProdtype());// 产品类型
		schd.setCcy(dldt.getCcy());// 币种
		schd.setRatecode(dldt.getRatecode());// 利率代码
		schd.setIntrate8(dldt.getIntrate8());// 利率
		schd.setBasis(dldt.getBasis());// 计息规则
		schd.setVerdate(dldt.getVerdate());// 复核日期
		schd.setAl(dldt.getAl());// 资产负债标志
		schd.setBrprcindte(dldt.getBrprcindte());// 分行处理输入日期
		schd.setFundrate8(dldt.getFundrate8());// 融资利率
		schd.setFundbasis(dldt.getBasis());// 融资计息规则
		schd.setPayauthdte(dldt.getVdate());// 付款授权日期
		schd.setIntauthdate(dldt.getVdate());// 利息结算指令授权日期
		schd.setText2(dldt.getCno());// 备用2
		schd.setSpread8(BigDecimal.ZERO);// 利率差
		schd.setPpayamt(BigDecimal.ZERO);// 本金还款金额
		schd.setNotprin(BigDecimal.ZERO);// 名义本金
		schd.setWhtrate8(BigDecimal.ZERO);// 计提税率
		schd.setWhtamt(BigDecimal.ZERO);// 计提税金
		schd.setVatamt(BigDecimal.ZERO);// 增值税金额
		schd.setIntexcharate8(BigDecimal.ZERO);// 利息汇率
		schd.setCapintamt(BigDecimal.ZERO);// 利息转为本金金额
		schd.setCaptaxamt(BigDecimal.ZERO);// 税费转为本金金额
		schd.setAppliedmonthall(BigDecimal.ZERO);// APPLIEDMONTHALL
		schd.setPosnprinamt(BigDecimal.ZERO);// 货币头寸本金金额
		schd.setNextappmonthall(BigDecimal.ZERO);// NEXTAPPMONTHALL
		schd.setAmount2(BigDecimal.ZERO);// 备用5
		schd.setAmount1(BigDecimal.ZERO);// 备用6
		schd.setRate1(BigDecimal.ZERO);// 备用7
		schd.setRate2(BigDecimal.ZERO);// 备用8
		schd.setStepupcorrrate8(BigDecimal.ZERO);// 利息增幅\减幅
		schd.setStepupcorrintamt(BigDecimal.ZERO);// 利息增量\减量
		schd.setIntadjamt(BigDecimal.ZERO);//
		schd.setOrigppayamt(BigDecimal.ZERO);//
		schd.setOrigintpayamt(BigDecimal.ZERO);//
		schd.setFailintpaidamt(BigDecimal.ZERO);//
		schd.setFailprinpaidamt(BigDecimal.ZERO);//
		schd.setPrinamt(BigDecimal.ZERO);// 本金
		schd.setIntamt(BigDecimal.ZERO);// 利息金融为零
		schd.setVatrate8(BigDecimal.ZERO);
		schd.setFundamt(BigDecimal.ZERO);
		schd.setPosnfundamt(BigDecimal.ZERO);
		schd.setPosnintamt(BigDecimal.ZERO);
		schd.setPayauthind(OpicsConstant.PCIX.AUTH_IND);// 付款授权标志
		schd.setIntauthind(OpicsConstant.PCIX.AUTH_IND);// 利息结算指令授权
		schd.setTerms(OpicsConstant.DL.TERMS);// 期限
		schd.setCapintind(OpicsConstant.DL.INT_CAP_FLAG);// 复利标志
		schd.setRateamtint(OpicsConstant.DL.RATE_AMT_IND);// 利率\利息标志
		schd.setRateamtfund(OpicsConstant.DL.RATE_AMT_IND);// 利率\利息标志（对融资）
		schd.setRateamttax1(OpicsConstant.DL.RATE_AMT_IND);// 税率\税金标志（对税金1）
		schd.setRateamttax2(OpicsConstant.DL.RATE_AMT_IND);// 税率\税金标志（对税金2）
		schd.setRateamttax3(OpicsConstant.DL.RATE_AMT_IND);// 税率\税金标志（对税金3）
		schd.setSeq(OpicsConstant.DL.SEQ_ZERO);// 默认
		schd.setAutomanpay(OpicsConstant.DL.AUTO_MAN_PAY);// 自动手动付款标志
		return schd;
	}

	/**
	 * SCHD 本金交易信息组装
	 *
	 * @param ipswb
	 * @param schd
	 * @param dldt
	 * @return
	 */
	private Schd DataTrans(IboPrinSchdWksBean ipswb, Schd schd, Dldt dldt) {
		DataTrans(schd, dldt);// 静态数据转换
		schd.setIntenddte(DateUtil.parse(ipswb.getPaymentDate()));// 利息到期日
		schd.setSchdseq(StringUtils.leftPad(ipswb.getSeqNumber(), 3, "0"));// 计划顺序号
		schd.setTotpayamt(getPlusNegateAmt(ipswb.getPayDirection(), ipswb.getPrincipal()));// 还款总额
		schd.setPrinamt(getPlusNegateAmt(ipswb.getPayDirection(), ipswb.getPrincipal()));// 本金
		schd.setIntstrtdte(DateUtil.parse(ipswb.getPaymentDate()));// 利息起始日期
		schd.setIpaydate(DateUtil.parse(ipswb.getPaymentDate()));// 付息日期
		schd.setIntsmeans(schd.getSmeans());
		schd.setIntsacct(schd.getSacct());
		// 清算信息
		if (!StringUtils.equals(schd.getCcy(), OpicsConstant.BRRD.LOCAL_CCY)) { // 非人民币是查询NOS ACCT
			List<String> accts = nostService.selectNosByCust(schd.getBr(), schd.getCcy(),
					ipswb.getSettleAcct().getHostCcyAwBiccode());
			schd.setSmeans(OpicsConstant.PCIX.NOSTRO_MEANS);
			schd.setSacct(StringUtils.trimToEmpty(accts.get(0)));// 结算方式
			schd.setIntsmeans(OpicsConstant.PCIX.NOSTRO_MEANS);
			schd.setIntsacct(StringUtils.trimToEmpty(accts.get(0)));// 结算方式
		} else {
			schd.setSmeans(OpicsConstant.PCIX.CANPS_MEANS);
			schd.setSacct(OpicsConstant.PCIX.CANPS_MEANS);// 结算方式
			schd.setIntsmeans(OpicsConstant.PCIX.CANPS_MEANS);
			schd.setIntsacct(OpicsConstant.PCIX.CANPS_MEANS);// 结算方式
		}
		// SchdType处理
		if (schd.getIpaydate().compareTo(dldt.getVdate()) == 0) {// 利息开始日期<=交易起息日
			schd.setSchdtype(OpicsConstant.DL.SCHD_TYPE_V);// 起息日
		} else if (schd.getIpaydate().compareTo(dldt.getMdate()) == 0) {
			schd.setSchdtype(OpicsConstant.DL.SCHD_TYPE_M);// 到息日
		} else {
			schd.setSchdtype(OpicsConstant.DL.SCHD_TYPE_P);// 中间记录
		}

		schd.setPpayamt(getPlusNegateAmt(ipswb.getPayDirection(), ipswb.getPrincipal()));// 本金还款金额
		schd.setLstmntdate(Calendar.getInstance().getTime());// 上次更新日期
		schd.setFundamt(schd.getIntamt());// 融资金额
		schd.setPosnfundamt(schd.getIntamt());// 货币头寸融资金额
		schd.setIntauthoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 利息结算指令授权操作员
		schd.setIntoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 利息收付款指令操作员
		schd.setPrinoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 本金收付款指令操作员
		schd.setPrinauthoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 利息收付款指令操作员
		return schd;
	}

	/**
	 * SCHD 利息交易信息组装
	 *
	 * @param iiswb
	 * @param schd
	 * @param dldt
	 * @return
	 */
	private Schd DataTrans(IboIntSchdWksBean iiswb, Schd schd, Dldt dldt) {
		DataTrans(schd, dldt);// 静态数据转换
		schd.setIntenddte(DateUtil.parse(iiswb.getRefEndDate()));// 利息到期日
		schd.setSchdseq(StringUtils.leftPad(String.valueOf(NumberUtils.toInt(iiswb.getSeqNumber()) + 1), 3, "0"));// 计划顺序号
		schd.setTotpayamt(getPlusNegateAmt(iiswb.getPayDirection(), iiswb.getInterest()));// 还款总额
		schd.setPrinamt(getPlusNegateAmt(iiswb.getPayDirection(), dldt.getCcyamt()));
		schd.setIntamt(getPlusNegateAmt(iiswb.getPayDirection(), iiswb.getInterest()));// 利息金额
		schd.setPosnintamt(getPlusNegateAmt(iiswb.getPayDirection(), iiswb.getInterest()));// 利息金额
		schd.setPosnfundamt(getPlusNegateAmt(iiswb.getPayDirection(), iiswb.getInterest()));
		schd.setIntstrtdte(DateUtil.parse(iiswb.getRefBeginDate()));// 利息起始金额
		schd.setIpaydate(DateUtil.parse(iiswb.getTheoryPaymentDate()));// 付息日期
		schd.setFundamt(schd.getIntamt());// 融资金额
		schd.setPosnfundamt(schd.getIntamt());// 货币头寸融资金额
		schd.setLstmntdate(Calendar.getInstance().getTime());// 上次更新日期
		schd.setIntsmeans(schd.getSmeans());// 利息结算方式
		schd.setIntsacct(schd.getSacct());
		// SchdType处理
		if (schd.getIpaydate().compareTo(dldt.getVdate()) == 0) {// 利息开始日期<=交易起息日
			schd.setSchdtype(OpicsConstant.DL.SCHD_TYPE_V);// 起息日
		} else if (schd.getIpaydate().compareTo(dldt.getMdate()) == 0) {
			schd.setSchdtype(OpicsConstant.DL.SCHD_TYPE_M);// 到息日
		} else {
			schd.setSchdtype(OpicsConstant.DL.SCHD_TYPE_P);// 中间记录
		}
		// 清算信息
		if (!StringUtils.equals(schd.getCcy(), OpicsConstant.BRRD.LOCAL_CCY)) { // 非人民币是查询NOS ACCT
			List<String> accts = nostService.selectNosByCust(schd.getBr(), schd.getCcy(),
					iiswb.getSettleAcct().getHostCcyAwBiccode());
			schd.setSmeans(OpicsConstant.PCIX.NOSTRO_MEANS);
			schd.setSacct(StringUtils.trimToEmpty(accts.get(0)));// 结算方式
			schd.setIntsmeans(OpicsConstant.PCIX.NOSTRO_MEANS);
			schd.setIntsacct(StringUtils.trimToEmpty(accts.get(0)));// 结算方式
		} else {
			schd.setSmeans(OpicsConstant.PCIX.CANPS_MEANS);
			schd.setSacct(OpicsConstant.PCIX.CANPS_MEANS);// 结算方式
			schd.setIntsmeans(OpicsConstant.PCIX.CANPS_MEANS);
			schd.setIntsacct(OpicsConstant.PCIX.CANPS_MEANS);// 结算方式
		}
		schd.setLstmntdate(Calendar.getInstance().getTime());// 上次更新日期
		schd.setFundamt(schd.getIntamt());// 融资金额
		schd.setPosnfundamt(schd.getIntamt());// 货币头寸融资金额
		schd.setIntauthoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 利息结算指令授权操作员
		schd.setIntoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 利息收付款指令操作员
		schd.setPrinoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 本金收付款指令操作员
		schd.setPrinauthoper(StringUtils.defaultIfEmpty(dldt.getIoper(), OpicsConstant.DL.TRAD_NO));// 利息收付款指令操作员
		return schd;
	}

	/**
	 * 根据收付方向判断金额正负
	 */
	private BigDecimal getPlusNegateAmt(String drcrind, BigDecimal amt) {
		if (StringUtils.equals(drcrind, OpicsConstant.DL.PAY_IND)
				|| StringUtils.equals(drcrind, OpicsConstant.DL.LIAB_IND)) {
			return amt.negate();// 取反
		} else {
			return amt.plus();// 取正
		}
	}
}
