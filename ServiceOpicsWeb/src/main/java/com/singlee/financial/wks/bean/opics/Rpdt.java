package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Rpdt implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String assignseq;

    private Date brprcindte;

    private BigDecimal chargeamt;

    private BigDecimal clprice8;

    private String ccy;

    private String dealtime;

    private BigDecimal drprice8;

    private BigDecimal discamt;

    private BigDecimal discrate8;

    private BigDecimal disprice8;

    private String exchcno;

    private BigDecimal exchcommamt;

    private String exchccy;

    private BigDecimal faceamt;

    private Date inputdate;

    private String ioper;

    private String inputtime;

    private String invtype;

    private Date mdate;

    private String port;

    private BigDecimal premamt;

    private BigDecimal prinamt;

    private BigDecimal proceedamt;

    private String product;

    private String prodtype;

    private String ps;

    private BigDecimal purchintamt;

    private BigDecimal repointamt;

    private Date rposmdate;

    private BigDecimal qty;

    private String secid;

    private BigDecimal vatamt;

    private Date vdate;

    private BigDecimal yield8;

    private BigDecimal whtamt;

    private BigDecimal cpnpaidamt;

    private BigDecimal nxtcpnamt;

    private BigDecimal tdymtmamt;

    private BigDecimal ystmtmamt;

    private BigDecimal pmemtmamt;

    private BigDecimal pyemtmamt;

    private BigDecimal tdyincexpamt;

    private BigDecimal ystincexpamt;

    private BigDecimal pmeincexpamt;

    private BigDecimal pyeincexpamt;

    private BigDecimal tdydscamt;

    private BigDecimal ystdscamt;

    private BigDecimal pmedscamt;

    private BigDecimal pyedscamt;

    private BigDecimal tdypremamt;

    private BigDecimal ystpremamt;

    private BigDecimal coupaccroutstamt;

    private BigDecimal coupaccrnextmonth;

    private BigDecimal settavgcost;

    private BigDecimal settavgcostyst;

    private BigDecimal bookvalamt;

    private BigDecimal bookvalamtyst;

    private String delivtype;

    private String cno;

    private String repocost;

    private String btbport;

    private Date verdate;

    private Date revdate;

    private BigDecimal prinexchrate8;

    private BigDecimal intexchrate8;

    private BigDecimal prinbaserate8;

    private BigDecimal intbaserate8;

    private String prinexchterms;

    private String intexchterms;

    private String prinbaseterms;

    private String intbaseterms;

    private BigDecimal haircut8;

    private Date vdfaildate;

    private Date mdfaildate;

    private Date vdcleardate;

    private Date mdcleardate;

    private String vdfailoper;

    private String mdfailoper;

    private String vdclearoper;

    private String mdclearoper;

    private String vdfailrefno;

    private String mdfailrefno;

    private String collunit;

    private BigDecimal vdcleanprice8;

    private BigDecimal vddirtyprice8;

    private BigDecimal mdcleanprice8;

    private BigDecimal mddirtyprice8;

    private String notccy;

    private BigDecimal commprocamt;

    private BigDecimal matprocamt;

    private BigDecimal matpurchintamt;

    private String reinvestind;

    private String reinvestlinkseqno;

    private BigDecimal mktvalue;

    private BigDecimal unamortamt;

    private BigDecimal tdyrepointamt;

    private BigDecimal ystrepointamt;

    private BigDecimal pmerepointamt;

    private BigDecimal pyerepointamt;

    private BigDecimal tdyrepomtmamt;

    private BigDecimal ystrepomtmamt;

    private BigDecimal pmerepomtmamt;

    private BigDecimal pyerepomtmamt;

    private BigDecimal pvmatprocamt;

    private BigDecimal reporate8;

    private BigDecimal spreadrate8;

    private BigDecimal hcpurchintamt;

    private BigDecimal hcmatpurchintamt;

    private BigDecimal repoprinamt;

    private String cost;

    private BigDecimal convintamt;

    private BigDecimal convintbamt;

    private String settccy;

    private String borrlendind;

    private BigDecimal tdypurchintamt;

    private BigDecimal ystpurchintamt;

    private String text1;

    private String text2;

    private Date date1;

    private Date date2;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private BigDecimal rate18;

    private BigDecimal rate28;

    private Long updatecounter;

    private BigDecimal vdpairedfaceamt;

    private BigDecimal vdpairedamt;

    private String vdpairedind;

    private String vdcurrsplitseq;

    private BigDecimal mdpairedfaceamt;

    private BigDecimal mdpairedamt;

    private String mdpairedind;

    private String mdcurrsplitseq;

    private BigDecimal indexratio8;

    private BigDecimal novfaceamt;

    private BigDecimal secondprice8;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getAssignseq() {
		return assignseq;
	}


	public void setAssignseq(String assignseq) {
		this.assignseq = assignseq;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public BigDecimal getChargeamt() {
		return chargeamt;
	}


	public void setChargeamt(BigDecimal chargeamt) {
		this.chargeamt = chargeamt;
	}


	public BigDecimal getClprice8() {
		return clprice8;
	}


	public void setClprice8(BigDecimal clprice8) {
		this.clprice8 = clprice8;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getDealtime() {
		return dealtime;
	}


	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}


	public BigDecimal getDrprice8() {
		return drprice8;
	}


	public void setDrprice8(BigDecimal drprice8) {
		this.drprice8 = drprice8;
	}


	public BigDecimal getDiscamt() {
		return discamt;
	}


	public void setDiscamt(BigDecimal discamt) {
		this.discamt = discamt;
	}


	public BigDecimal getDiscrate8() {
		return discrate8;
	}


	public void setDiscrate8(BigDecimal discrate8) {
		this.discrate8 = discrate8;
	}


	public BigDecimal getDisprice8() {
		return disprice8;
	}


	public void setDisprice8(BigDecimal disprice8) {
		this.disprice8 = disprice8;
	}


	public String getExchcno() {
		return exchcno;
	}


	public void setExchcno(String exchcno) {
		this.exchcno = exchcno;
	}


	public BigDecimal getExchcommamt() {
		return exchcommamt;
	}


	public void setExchcommamt(BigDecimal exchcommamt) {
		this.exchcommamt = exchcommamt;
	}


	public String getExchccy() {
		return exchccy;
	}


	public void setExchccy(String exchccy) {
		this.exchccy = exchccy;
	}


	public BigDecimal getFaceamt() {
		return faceamt;
	}


	public void setFaceamt(BigDecimal faceamt) {
		this.faceamt = faceamt;
	}


	public Date getInputdate() {
		return inputdate;
	}


	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}


	public String getIoper() {
		return ioper;
	}


	public void setIoper(String ioper) {
		this.ioper = ioper;
	}


	public String getInputtime() {
		return inputtime;
	}


	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}


	public String getInvtype() {
		return invtype;
	}


	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}


	public Date getMdate() {
		return mdate;
	}


	public void setMdate(Date mdate) {
		this.mdate = mdate;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public BigDecimal getPremamt() {
		return premamt;
	}


	public void setPremamt(BigDecimal premamt) {
		this.premamt = premamt;
	}


	public BigDecimal getPrinamt() {
		return prinamt;
	}


	public void setPrinamt(BigDecimal prinamt) {
		this.prinamt = prinamt;
	}


	public BigDecimal getProceedamt() {
		return proceedamt;
	}


	public void setProceedamt(BigDecimal proceedamt) {
		this.proceedamt = proceedamt;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getPs() {
		return ps;
	}


	public void setPs(String ps) {
		this.ps = ps;
	}


	public BigDecimal getPurchintamt() {
		return purchintamt;
	}


	public void setPurchintamt(BigDecimal purchintamt) {
		this.purchintamt = purchintamt;
	}


	public BigDecimal getRepointamt() {
		return repointamt;
	}


	public void setRepointamt(BigDecimal repointamt) {
		this.repointamt = repointamt;
	}


	public Date getRposmdate() {
		return rposmdate;
	}


	public void setRposmdate(Date rposmdate) {
		this.rposmdate = rposmdate;
	}


	public BigDecimal getQty() {
		return qty;
	}


	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}


	public String getSecid() {
		return secid;
	}


	public void setSecid(String secid) {
		this.secid = secid;
	}


	public BigDecimal getVatamt() {
		return vatamt;
	}


	public void setVatamt(BigDecimal vatamt) {
		this.vatamt = vatamt;
	}


	public Date getVdate() {
		return vdate;
	}


	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}


	public BigDecimal getYield8() {
		return yield8;
	}


	public void setYield8(BigDecimal yield8) {
		this.yield8 = yield8;
	}


	public BigDecimal getWhtamt() {
		return whtamt;
	}


	public void setWhtamt(BigDecimal whtamt) {
		this.whtamt = whtamt;
	}


	public BigDecimal getCpnpaidamt() {
		return cpnpaidamt;
	}


	public void setCpnpaidamt(BigDecimal cpnpaidamt) {
		this.cpnpaidamt = cpnpaidamt;
	}


	public BigDecimal getNxtcpnamt() {
		return nxtcpnamt;
	}


	public void setNxtcpnamt(BigDecimal nxtcpnamt) {
		this.nxtcpnamt = nxtcpnamt;
	}


	public BigDecimal getTdymtmamt() {
		return tdymtmamt;
	}


	public void setTdymtmamt(BigDecimal tdymtmamt) {
		this.tdymtmamt = tdymtmamt;
	}


	public BigDecimal getYstmtmamt() {
		return ystmtmamt;
	}


	public void setYstmtmamt(BigDecimal ystmtmamt) {
		this.ystmtmamt = ystmtmamt;
	}


	public BigDecimal getPmemtmamt() {
		return pmemtmamt;
	}


	public void setPmemtmamt(BigDecimal pmemtmamt) {
		this.pmemtmamt = pmemtmamt;
	}


	public BigDecimal getPyemtmamt() {
		return pyemtmamt;
	}


	public void setPyemtmamt(BigDecimal pyemtmamt) {
		this.pyemtmamt = pyemtmamt;
	}


	public BigDecimal getTdyincexpamt() {
		return tdyincexpamt;
	}


	public void setTdyincexpamt(BigDecimal tdyincexpamt) {
		this.tdyincexpamt = tdyincexpamt;
	}


	public BigDecimal getYstincexpamt() {
		return ystincexpamt;
	}


	public void setYstincexpamt(BigDecimal ystincexpamt) {
		this.ystincexpamt = ystincexpamt;
	}


	public BigDecimal getPmeincexpamt() {
		return pmeincexpamt;
	}


	public void setPmeincexpamt(BigDecimal pmeincexpamt) {
		this.pmeincexpamt = pmeincexpamt;
	}


	public BigDecimal getPyeincexpamt() {
		return pyeincexpamt;
	}


	public void setPyeincexpamt(BigDecimal pyeincexpamt) {
		this.pyeincexpamt = pyeincexpamt;
	}


	public BigDecimal getTdydscamt() {
		return tdydscamt;
	}


	public void setTdydscamt(BigDecimal tdydscamt) {
		this.tdydscamt = tdydscamt;
	}


	public BigDecimal getYstdscamt() {
		return ystdscamt;
	}


	public void setYstdscamt(BigDecimal ystdscamt) {
		this.ystdscamt = ystdscamt;
	}


	public BigDecimal getPmedscamt() {
		return pmedscamt;
	}


	public void setPmedscamt(BigDecimal pmedscamt) {
		this.pmedscamt = pmedscamt;
	}


	public BigDecimal getPyedscamt() {
		return pyedscamt;
	}


	public void setPyedscamt(BigDecimal pyedscamt) {
		this.pyedscamt = pyedscamt;
	}


	public BigDecimal getTdypremamt() {
		return tdypremamt;
	}


	public void setTdypremamt(BigDecimal tdypremamt) {
		this.tdypremamt = tdypremamt;
	}


	public BigDecimal getYstpremamt() {
		return ystpremamt;
	}


	public void setYstpremamt(BigDecimal ystpremamt) {
		this.ystpremamt = ystpremamt;
	}


	public BigDecimal getCoupaccroutstamt() {
		return coupaccroutstamt;
	}


	public void setCoupaccroutstamt(BigDecimal coupaccroutstamt) {
		this.coupaccroutstamt = coupaccroutstamt;
	}


	public BigDecimal getCoupaccrnextmonth() {
		return coupaccrnextmonth;
	}


	public void setCoupaccrnextmonth(BigDecimal coupaccrnextmonth) {
		this.coupaccrnextmonth = coupaccrnextmonth;
	}


	public BigDecimal getSettavgcost() {
		return settavgcost;
	}


	public void setSettavgcost(BigDecimal settavgcost) {
		this.settavgcost = settavgcost;
	}


	public BigDecimal getSettavgcostyst() {
		return settavgcostyst;
	}


	public void setSettavgcostyst(BigDecimal settavgcostyst) {
		this.settavgcostyst = settavgcostyst;
	}


	public BigDecimal getBookvalamt() {
		return bookvalamt;
	}


	public void setBookvalamt(BigDecimal bookvalamt) {
		this.bookvalamt = bookvalamt;
	}


	public BigDecimal getBookvalamtyst() {
		return bookvalamtyst;
	}


	public void setBookvalamtyst(BigDecimal bookvalamtyst) {
		this.bookvalamtyst = bookvalamtyst;
	}


	public String getDelivtype() {
		return delivtype;
	}


	public void setDelivtype(String delivtype) {
		this.delivtype = delivtype;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getRepocost() {
		return repocost;
	}


	public void setRepocost(String repocost) {
		this.repocost = repocost;
	}


	public String getBtbport() {
		return btbport;
	}


	public void setBtbport(String btbport) {
		this.btbport = btbport;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public BigDecimal getPrinexchrate8() {
		return prinexchrate8;
	}


	public void setPrinexchrate8(BigDecimal prinexchrate8) {
		this.prinexchrate8 = prinexchrate8;
	}


	public BigDecimal getIntexchrate8() {
		return intexchrate8;
	}


	public void setIntexchrate8(BigDecimal intexchrate8) {
		this.intexchrate8 = intexchrate8;
	}


	public BigDecimal getPrinbaserate8() {
		return prinbaserate8;
	}


	public void setPrinbaserate8(BigDecimal prinbaserate8) {
		this.prinbaserate8 = prinbaserate8;
	}


	public BigDecimal getIntbaserate8() {
		return intbaserate8;
	}


	public void setIntbaserate8(BigDecimal intbaserate8) {
		this.intbaserate8 = intbaserate8;
	}


	public String getPrinexchterms() {
		return prinexchterms;
	}


	public void setPrinexchterms(String prinexchterms) {
		this.prinexchterms = prinexchterms;
	}


	public String getIntexchterms() {
		return intexchterms;
	}


	public void setIntexchterms(String intexchterms) {
		this.intexchterms = intexchterms;
	}


	public String getPrinbaseterms() {
		return prinbaseterms;
	}


	public void setPrinbaseterms(String prinbaseterms) {
		this.prinbaseterms = prinbaseterms;
	}


	public String getIntbaseterms() {
		return intbaseterms;
	}


	public void setIntbaseterms(String intbaseterms) {
		this.intbaseterms = intbaseterms;
	}


	public BigDecimal getHaircut8() {
		return haircut8;
	}


	public void setHaircut8(BigDecimal haircut8) {
		this.haircut8 = haircut8;
	}


	public Date getVdfaildate() {
		return vdfaildate;
	}


	public void setVdfaildate(Date vdfaildate) {
		this.vdfaildate = vdfaildate;
	}


	public Date getMdfaildate() {
		return mdfaildate;
	}


	public void setMdfaildate(Date mdfaildate) {
		this.mdfaildate = mdfaildate;
	}


	public Date getVdcleardate() {
		return vdcleardate;
	}


	public void setVdcleardate(Date vdcleardate) {
		this.vdcleardate = vdcleardate;
	}


	public Date getMdcleardate() {
		return mdcleardate;
	}


	public void setMdcleardate(Date mdcleardate) {
		this.mdcleardate = mdcleardate;
	}


	public String getVdfailoper() {
		return vdfailoper;
	}


	public void setVdfailoper(String vdfailoper) {
		this.vdfailoper = vdfailoper;
	}


	public String getMdfailoper() {
		return mdfailoper;
	}


	public void setMdfailoper(String mdfailoper) {
		this.mdfailoper = mdfailoper;
	}


	public String getVdclearoper() {
		return vdclearoper;
	}


	public void setVdclearoper(String vdclearoper) {
		this.vdclearoper = vdclearoper;
	}


	public String getMdclearoper() {
		return mdclearoper;
	}


	public void setMdclearoper(String mdclearoper) {
		this.mdclearoper = mdclearoper;
	}


	public String getVdfailrefno() {
		return vdfailrefno;
	}


	public void setVdfailrefno(String vdfailrefno) {
		this.vdfailrefno = vdfailrefno;
	}


	public String getMdfailrefno() {
		return mdfailrefno;
	}


	public void setMdfailrefno(String mdfailrefno) {
		this.mdfailrefno = mdfailrefno;
	}


	public String getCollunit() {
		return collunit;
	}


	public void setCollunit(String collunit) {
		this.collunit = collunit;
	}


	public BigDecimal getVdcleanprice8() {
		return vdcleanprice8;
	}


	public void setVdcleanprice8(BigDecimal vdcleanprice8) {
		this.vdcleanprice8 = vdcleanprice8;
	}


	public BigDecimal getVddirtyprice8() {
		return vddirtyprice8;
	}


	public void setVddirtyprice8(BigDecimal vddirtyprice8) {
		this.vddirtyprice8 = vddirtyprice8;
	}


	public BigDecimal getMdcleanprice8() {
		return mdcleanprice8;
	}


	public void setMdcleanprice8(BigDecimal mdcleanprice8) {
		this.mdcleanprice8 = mdcleanprice8;
	}


	public BigDecimal getMddirtyprice8() {
		return mddirtyprice8;
	}


	public void setMddirtyprice8(BigDecimal mddirtyprice8) {
		this.mddirtyprice8 = mddirtyprice8;
	}


	public String getNotccy() {
		return notccy;
	}


	public void setNotccy(String notccy) {
		this.notccy = notccy;
	}


	public BigDecimal getCommprocamt() {
		return commprocamt;
	}


	public void setCommprocamt(BigDecimal commprocamt) {
		this.commprocamt = commprocamt;
	}


	public BigDecimal getMatprocamt() {
		return matprocamt;
	}


	public void setMatprocamt(BigDecimal matprocamt) {
		this.matprocamt = matprocamt;
	}


	public BigDecimal getMatpurchintamt() {
		return matpurchintamt;
	}


	public void setMatpurchintamt(BigDecimal matpurchintamt) {
		this.matpurchintamt = matpurchintamt;
	}


	public String getReinvestind() {
		return reinvestind;
	}


	public void setReinvestind(String reinvestind) {
		this.reinvestind = reinvestind;
	}


	public String getReinvestlinkseqno() {
		return reinvestlinkseqno;
	}


	public void setReinvestlinkseqno(String reinvestlinkseqno) {
		this.reinvestlinkseqno = reinvestlinkseqno;
	}


	public BigDecimal getMktvalue() {
		return mktvalue;
	}


	public void setMktvalue(BigDecimal mktvalue) {
		this.mktvalue = mktvalue;
	}


	public BigDecimal getUnamortamt() {
		return unamortamt;
	}


	public void setUnamortamt(BigDecimal unamortamt) {
		this.unamortamt = unamortamt;
	}


	public BigDecimal getTdyrepointamt() {
		return tdyrepointamt;
	}


	public void setTdyrepointamt(BigDecimal tdyrepointamt) {
		this.tdyrepointamt = tdyrepointamt;
	}


	public BigDecimal getYstrepointamt() {
		return ystrepointamt;
	}


	public void setYstrepointamt(BigDecimal ystrepointamt) {
		this.ystrepointamt = ystrepointamt;
	}


	public BigDecimal getPmerepointamt() {
		return pmerepointamt;
	}


	public void setPmerepointamt(BigDecimal pmerepointamt) {
		this.pmerepointamt = pmerepointamt;
	}


	public BigDecimal getPyerepointamt() {
		return pyerepointamt;
	}


	public void setPyerepointamt(BigDecimal pyerepointamt) {
		this.pyerepointamt = pyerepointamt;
	}


	public BigDecimal getTdyrepomtmamt() {
		return tdyrepomtmamt;
	}


	public void setTdyrepomtmamt(BigDecimal tdyrepomtmamt) {
		this.tdyrepomtmamt = tdyrepomtmamt;
	}


	public BigDecimal getYstrepomtmamt() {
		return ystrepomtmamt;
	}


	public void setYstrepomtmamt(BigDecimal ystrepomtmamt) {
		this.ystrepomtmamt = ystrepomtmamt;
	}


	public BigDecimal getPmerepomtmamt() {
		return pmerepomtmamt;
	}


	public void setPmerepomtmamt(BigDecimal pmerepomtmamt) {
		this.pmerepomtmamt = pmerepomtmamt;
	}


	public BigDecimal getPyerepomtmamt() {
		return pyerepomtmamt;
	}


	public void setPyerepomtmamt(BigDecimal pyerepomtmamt) {
		this.pyerepomtmamt = pyerepomtmamt;
	}


	public BigDecimal getPvmatprocamt() {
		return pvmatprocamt;
	}


	public void setPvmatprocamt(BigDecimal pvmatprocamt) {
		this.pvmatprocamt = pvmatprocamt;
	}


	public BigDecimal getReporate8() {
		return reporate8;
	}


	public void setReporate8(BigDecimal reporate8) {
		this.reporate8 = reporate8;
	}


	public BigDecimal getSpreadrate8() {
		return spreadrate8;
	}


	public void setSpreadrate8(BigDecimal spreadrate8) {
		this.spreadrate8 = spreadrate8;
	}


	public BigDecimal getHcpurchintamt() {
		return hcpurchintamt;
	}


	public void setHcpurchintamt(BigDecimal hcpurchintamt) {
		this.hcpurchintamt = hcpurchintamt;
	}


	public BigDecimal getHcmatpurchintamt() {
		return hcmatpurchintamt;
	}


	public void setHcmatpurchintamt(BigDecimal hcmatpurchintamt) {
		this.hcmatpurchintamt = hcmatpurchintamt;
	}


	public BigDecimal getRepoprinamt() {
		return repoprinamt;
	}


	public void setRepoprinamt(BigDecimal repoprinamt) {
		this.repoprinamt = repoprinamt;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public BigDecimal getConvintamt() {
		return convintamt;
	}


	public void setConvintamt(BigDecimal convintamt) {
		this.convintamt = convintamt;
	}


	public BigDecimal getConvintbamt() {
		return convintbamt;
	}


	public void setConvintbamt(BigDecimal convintbamt) {
		this.convintbamt = convintbamt;
	}


	public String getSettccy() {
		return settccy;
	}


	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}


	public String getBorrlendind() {
		return borrlendind;
	}


	public void setBorrlendind(String borrlendind) {
		this.borrlendind = borrlendind;
	}


	public BigDecimal getTdypurchintamt() {
		return tdypurchintamt;
	}


	public void setTdypurchintamt(BigDecimal tdypurchintamt) {
		this.tdypurchintamt = tdypurchintamt;
	}


	public BigDecimal getYstpurchintamt() {
		return ystpurchintamt;
	}


	public void setYstpurchintamt(BigDecimal ystpurchintamt) {
		this.ystpurchintamt = ystpurchintamt;
	}


	public String getText1() {
		return text1;
	}


	public void setText1(String text1) {
		this.text1 = text1;
	}


	public String getText2() {
		return text2;
	}


	public void setText2(String text2) {
		this.text2 = text2;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public BigDecimal getRate18() {
		return rate18;
	}


	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}


	public BigDecimal getRate28() {
		return rate28;
	}


	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}


	public Long getUpdatecounter() {
		return updatecounter;
	}


	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}


	public BigDecimal getVdpairedfaceamt() {
		return vdpairedfaceamt;
	}


	public void setVdpairedfaceamt(BigDecimal vdpairedfaceamt) {
		this.vdpairedfaceamt = vdpairedfaceamt;
	}


	public BigDecimal getVdpairedamt() {
		return vdpairedamt;
	}


	public void setVdpairedamt(BigDecimal vdpairedamt) {
		this.vdpairedamt = vdpairedamt;
	}


	public String getVdpairedind() {
		return vdpairedind;
	}


	public void setVdpairedind(String vdpairedind) {
		this.vdpairedind = vdpairedind;
	}


	public String getVdcurrsplitseq() {
		return vdcurrsplitseq;
	}


	public void setVdcurrsplitseq(String vdcurrsplitseq) {
		this.vdcurrsplitseq = vdcurrsplitseq;
	}


	public BigDecimal getMdpairedfaceamt() {
		return mdpairedfaceamt;
	}


	public void setMdpairedfaceamt(BigDecimal mdpairedfaceamt) {
		this.mdpairedfaceamt = mdpairedfaceamt;
	}


	public BigDecimal getMdpairedamt() {
		return mdpairedamt;
	}


	public void setMdpairedamt(BigDecimal mdpairedamt) {
		this.mdpairedamt = mdpairedamt;
	}


	public String getMdpairedind() {
		return mdpairedind;
	}


	public void setMdpairedind(String mdpairedind) {
		this.mdpairedind = mdpairedind;
	}


	public String getMdcurrsplitseq() {
		return mdcurrsplitseq;
	}


	public void setMdcurrsplitseq(String mdcurrsplitseq) {
		this.mdcurrsplitseq = mdcurrsplitseq;
	}


	public BigDecimal getIndexratio8() {
		return indexratio8;
	}


	public void setIndexratio8(BigDecimal indexratio8) {
		this.indexratio8 = indexratio8;
	}


	public BigDecimal getNovfaceamt() {
		return novfaceamt;
	}


	public void setNovfaceamt(BigDecimal novfaceamt) {
		this.novfaceamt = novfaceamt;
	}


	public BigDecimal getSecondprice8() {
		return secondprice8;
	}


	public void setSecondprice8(BigDecimal secondprice8) {
		this.secondprice8 = secondprice8;
	}


	private static final long serialVersionUID = 1L;
}