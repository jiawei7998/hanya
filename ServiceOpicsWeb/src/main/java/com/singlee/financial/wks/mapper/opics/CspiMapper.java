package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Cspi;
import org.apache.ibatis.annotations.Param;

public interface CspiMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("cno") String cno, @Param("product") String product, @Param("type") String type, @Param("ccy") String ccy);

    int insert(Cspi record);

    int insertSelective(Cspi record);

    Cspi selectByPrimaryKey(@Param("br") String br, @Param("cno") String cno, @Param("product") String product, @Param("type") String type, @Param("ccy") String ccy);

    int updateByPrimaryKeySelective(Cspi record);

    int updateByPrimaryKey(Cspi record);
}