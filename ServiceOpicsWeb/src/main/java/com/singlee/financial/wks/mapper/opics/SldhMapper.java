package com.singlee.financial.wks.mapper.opics;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.singlee.financial.wks.bean.opics.Sldh;

public interface SldhMapper {
    /**
     * delete by primary key
     *
     * @param br primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(Sldh record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(Sldh record);

    /**
     * select by primary key
     *
     * @param br primary key
     * @return object by primary key
     */
    Sldh selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Sldh record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Sldh record);
    
    List<Sldh> selectByTradeNo(@Param("br") String br, @Param("dealtext") String tradeNo);
}