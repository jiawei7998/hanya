package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Sico;

public interface SicoMapper {
    /**
     * delete by primary key
     * @param sic primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String sic);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Sico record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Sico record);

    /**
     * select by primary key
     * @param sic primary key
     * @return object by primary key
     */
    Sico selectByPrimaryKey(String sic);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Sico record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Sico record);
}