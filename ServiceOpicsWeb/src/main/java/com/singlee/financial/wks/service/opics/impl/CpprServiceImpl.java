package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.Ccyp;
import com.singlee.financial.wks.bean.opics.Cppr;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.mapper.opics.CcypMapper;
import com.singlee.financial.wks.mapper.opics.CpprMapper;
import com.singlee.financial.wks.service.cashflow.FxdFactory;
import com.singlee.financial.wks.service.opics.CpprService;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

/**
 * 更新外汇头寸
 * 
 * @author shenzl
 * @date 2020/02/20
 */
@Service
public class CpprServiceImpl implements CpprService {

	@Resource
	private CpprMapper cpprMapper;
	@Resource
	private CcypMapper ccypMapper;

	@Override
	public int deleteByPrimaryKey(String br, String ccy1, String ccy2, String cost, String port, String trad,
			Date vdate) {
		return cpprMapper.deleteByPrimaryKey(br, ccy1, ccy2, cost, port, trad, vdate);
	}

	@Override
	public int insert(Cppr record) {
		return cpprMapper.insert(record);
	}

	@Override
	public int insertSelective(Cppr record) {
		return cpprMapper.insertSelective(record);
	}

	@Override
	public Cppr selectByPrimaryKey(String br, String ccy1, String ccy2, String cost, String port, String trad,
			Date vdate) {
		return cpprMapper.selectByPrimaryKey(br, ccy1, ccy2, cost, port, trad, vdate);
	}

	@Override
	public int updateByPrimaryKeySelective(Cppr record) {
		return cpprMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Cppr record) {
		return cpprMapper.updateByPrimaryKey(record);
	}

	@Override
	public int updateBatch(List<Cppr> list) {
		return cpprMapper.updateBatch(list);
	}

	@Override
	public int updateBatchSelective(List<Cppr> list) {
		return cpprMapper.updateBatchSelective(list);
	}

	@Override
	public int updateCpprByFxdh(Cppr cppr) {
		return cpprMapper.updateCpprAmtByDeal(cppr);
	}

	@Override
	public Cppr builder(Fxdh fxdh,Cppr orgCppr) {
		boolean isCurrencyPairsEqual;
		boolean isCcyBaseCurrency;
		boolean isCtrccyBaseCurrency;
		boolean isReverse;
		Cppr cppr = null;
		BigDecimal ccy1Amt = null;
		BigDecimal ccy2Amt = null;
		
		if (null == fxdh) {
			return null;
		}
		
		//判断当前交易是否为冲销操作 
		if(fxdh.getRevdate() == null) {
			isReverse = false;
		}else{
			isReverse = true;
		}
		
		// 判读货币对
		Ccyp ccyp = ccypMapper.selectByPrimaryKey(fxdh.getBr(), fxdh.getCcy(), fxdh.getCtrccy());
		
		// 如果CCPR没有记录则创建,如果有则累加
		if (null == orgCppr) {
			cppr = createCppr(ccyp,fxdh);
		} else {
			cppr = orgCppr;
		}
		
		// 判断兑换方向
		if (StringUtils.equals(fxdh.getCcy(), ccyp.getCcy1())) {// 交易录入ccy/ctr与CPPR定义CCY1/CCY2一致
			ccy1Amt = fxdh.getCcyamt();
			ccy2Amt = fxdh.getCtramt();
			isCurrencyPairsEqual = true;
		} else {// 不一致,需要反向
			ccy1Amt = fxdh.getCtramt();
			ccy2Amt = fxdh.getCcyamt();
			isCurrencyPairsEqual = false;
		}
		//设置CCPR金额
		cppr.setCcy1amt(cppr.getCcy1amt().add(getReverseAmt(ccy1Amt,isReverse)));
		cppr.setCcy2amt(cppr.getCcy2amt().add(getReverseAmt(ccy2Amt,isReverse)));
		
		// 判断CCY是否为本国货币
		isCcyBaseCurrency = StringUtils.equals(fxdh.getCcy(),OpicsConstant.BRRD.BASE_CCY) ? true : false;
		// 判断CTRCCY是否为本国货币
		isCtrccyBaseCurrency = StringUtils.equals(fxdh.getCtrccy(),OpicsConstant.BRRD.BASE_CCY) ? true : false;

		//判断是结售汇交易或者是外汇买卖
		if (isCcyBaseCurrency || isCtrccyBaseCurrency) {// 结售汇
			//结售汇交易处理
			builder(cppr,fxdh,isCurrencyPairsEqual,isCcyBaseCurrency,isReverse);
		} else {
			//外汇买卖交易处理
			builder(cppr,fxdh,isCurrencyPairsEqual,isReverse,ccyp);
		}
		
		
		return cppr;
	}
	
	/**
	 * 方法已重载.组装结售汇交易CPPR对应的字段
	 * @param cppr
	 * @param fxdh
	 * @param isCurrencyPairsEqual
	 * @param isCcyBaseCurrency
	 * @param isReverse
	 * @return
	 */
	private Cppr builder(Cppr cppr, Fxdh fxdh,boolean isCurrencyPairsEqual,boolean isCcyBaseCurrency,boolean isReverse) {
		BigDecimal ccy1spotbaseamt = null;
		BigDecimal ccy2spotbaseamt = null;
		//交易货币对与CCYP相同
		if (isCurrencyPairsEqual) {
			if(isCcyBaseCurrency) {
				// 货币1是本币
				ccy1spotbaseamt = fxdh.getCcybamt().add(fxdh.getFwdpremamt()).setScale(2, RoundingMode.HALF_UP);
				ccy2spotbaseamt = fxdh.getCtrbamt().subtract(fxdh.getFwdpremamt()).setScale(2, RoundingMode.HALF_UP);
			}else {
				//货币2是本币
				ccy2spotbaseamt = fxdh.getCtrbamt();
				ccy1spotbaseamt = fxdh.getCcybamt().subtract(fxdh.getFwdpremamt()).setScale(2, RoundingMode.HALF_UP);
			}
		} else {//交易货币对与CCYP不同
			if(isCcyBaseCurrency) {
				// 货币1是本币
				ccy1spotbaseamt = fxdh.getCtrbamt().subtract(fxdh.getFwdpremamt()).setScale(2, RoundingMode.HALF_UP);
				ccy2spotbaseamt = fxdh.getCcybamt().add(fxdh.getFwdpremamt()).setScale(2, RoundingMode.HALF_UP);
			}else {
				//货币2是本币
				ccy1spotbaseamt = fxdh.getCtrbamt();
				ccy2spotbaseamt = fxdh.getCcybamt().subtract(fxdh.getFwdpremamt()).setScale(2, RoundingMode.HALF_UP);
			}//end if else
		}//end if else
		cppr.setCcy1spotbaseamt(cppr.getCcy1spotbaseamt().add(getReverseAmt(ccy1spotbaseamt,isReverse)));
		cppr.setCcy2spotbaseamt(cppr.getCcy2spotbaseamt().add(getReverseAmt(ccy2spotbaseamt,isReverse)));
		return cppr;
	}

	/**
	 * 方法已重载.组装外汇买卖交易CPPR对应字段
	 * @param cppr
	 * @param fxdh
	 * @param isCurrencyPairsEqual
	 * @param isReverse
	 * @return
	 */
	private Cppr builder(Cppr cppr, Fxdh fxdh,boolean isCurrencyPairsEqual,boolean isReverse,Ccyp ccyp) {
		BigDecimal ccy1spotbaseamt = null;
		BigDecimal ccy2spotbaseamt = null;
		
		BigDecimal spotBaseRate = fxdh.getCcybrate8().subtract(fxdh.getCcybpd8()).setScale(15, RoundingMode.HALF_UP);
		
		LogFactory.getLog(CpprServiceImpl.class).info("CpprServiceImpl builderFx spotBaseRate:"+spotBaseRate);
		LogFactory.getLog(CpprServiceImpl.class).debug("CpprServiceImpl builderFx Fxdh:"+fxdh);
		
		//交易货币对与CCYP相同
		if (isCurrencyPairsEqual) {
			ccy1spotbaseamt = FxdFactory.calculationBaseAmount(fxdh.getCcyamt(), spotBaseRate,2);
			if("0".equalsIgnoreCase(StringUtils.trim(fxdh.getSwapdeal()))) {
				//即远期交易
				ccy2spotbaseamt = ccy1spotbaseamt.negate().subtract(fxdh.getFwdpremamt());
			}else {
				//掉期交易
				if(ccyp.getTradedccy().equalsIgnoreCase(ccyp.getCcy1())) {
					ccy2spotbaseamt = ccy1spotbaseamt.negate().subtract(fxdh.getFwdpremamt());
				}else {
					ccy2spotbaseamt = ccy1spotbaseamt.negate().add(fxdh.getFwdpremamt());
				}//end if else
			}//end if else
		} else {//交易货币对与CCYP不同
			ccy2spotbaseamt = FxdFactory.calculationBaseAmount(fxdh.getCcyamt(), spotBaseRate,2);
			
			if("0".equalsIgnoreCase(StringUtils.trim(fxdh.getSwapdeal()))) {
				//即远期交易
				ccy1spotbaseamt = ccy2spotbaseamt.negate().add(fxdh.getFwdpremamt());
			}else {
				//掉期交易
				if(ccyp.getTradedccy().equalsIgnoreCase(ccyp.getCcy1())) {
					ccy1spotbaseamt = ccy2spotbaseamt.negate().add(fxdh.getFwdpremamt());
				}else {
					ccy1spotbaseamt = ccy2spotbaseamt.negate().subtract(fxdh.getFwdpremamt());
				}//end if else
			}//end if else
			
		}//end if else
		cppr.setCcy1spotbaseamt(cppr.getCcy1spotbaseamt().add(getReverseAmt(ccy1spotbaseamt,isReverse)));
		cppr.setCcy2spotbaseamt(cppr.getCcy2spotbaseamt().add(getReverseAmt(ccy2spotbaseamt,isReverse)));
		return cppr;
	}
	
	/**
	 * 根据冲销状态,设置交易金额方向
	 * @param amt
	 * @param isReverse
	 * @return
	 */
	private BigDecimal getReverseAmt(BigDecimal amt,boolean isReverse) {
		if(isReverse) {
			return amt.negate();
		}else {
			return amt;
		}
	}
	
	private Cppr createCppr(Ccyp ccyp,Fxdh fxdh) {
		Cppr cppr = new Cppr();

		cppr.setBr(fxdh.getBr());
		cppr.setCcy1(ccyp.getCcy1());
		cppr.setCcy2(ccyp.getCcy2());
		cppr.setCost(fxdh.getCost());
		cppr.setPort(fxdh.getPort());
		cppr.setTrad(fxdh.getTrad());
		cppr.setVdate(fxdh.getVdate().compareTo(fxdh.getBrprcindte()) >= 0 ? fxdh.getVdate() : fxdh.getBrprcindte());

		// 以下为0
		cppr.setCcy1mktamt(BigDecimal.ZERO);
		cppr.setCcy2mktamt(BigDecimal.ZERO);
		cppr.setTdyplamt(BigDecimal.ZERO);
		cppr.setYstplamt(BigDecimal.ZERO);
		cppr.setPmeplamt(BigDecimal.ZERO);
		cppr.setPyeplamt(BigDecimal.ZERO);
		cppr.setCcy1amtyst(BigDecimal.ZERO);
		cppr.setCcy2amtyst(BigDecimal.ZERO);
		cppr.setTdynpvamt(BigDecimal.ZERO);
		cppr.setYstnpvamt(BigDecimal.ZERO);
		cppr.setPmenpvamt(BigDecimal.ZERO);
		cppr.setPyenpvamt(BigDecimal.ZERO);
		cppr.setCcy1spotbaseamtyst(BigDecimal.ZERO);
		cppr.setCcy2spotbaseamtyst(BigDecimal.ZERO);
		cppr.setCcy1amt(BigDecimal.ZERO);
		cppr.setCcy2amt(BigDecimal.ZERO);
		cppr.setCcy1spotbaseamt(BigDecimal.ZERO);
		cppr.setCcy2spotbaseamt(BigDecimal.ZERO);
		
		return cppr;
	}

}
