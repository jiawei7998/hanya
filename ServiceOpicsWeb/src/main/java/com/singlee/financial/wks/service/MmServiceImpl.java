package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.RException;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.IboWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.intfc.MmService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.MmAdaptDate;
import com.singlee.financial.wks.service.trans.MmAdaptPrin;
import com.singlee.financial.wks.service.trans.MmAdjustRate;
import com.singlee.financial.wks.service.trans.MmEarly;
import com.singlee.financial.wks.service.trans.MmEntry;
import com.singlee.financial.wks.service.trans.MmReverse;
import com.singlee.financial.wks.util.FinancialBeanHelper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

/**
 * 拆借模块服务实现类
 *
 * @author shenzl
 * @date 2020年1月6日 19:19:24
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class MmServiceImpl implements MmService {

	@Autowired
	private DldtService dldtService;
	@Autowired
	private SchdService schdService;
	@Autowired
	private McfpService mcfpService;
	@Autowired
	private PcixService pcixService;
	@Autowired
	private PrirService prirService;
	@Autowired
	private PsixService psixService;
	@Autowired
	private CnfqService cnfqService;
	@Autowired
	private NupdService nupdService;
	@Autowired
	private PmtqService pmtqSerice;
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private MmEntry mmEntry;// 首期交易录入
	@Autowired
	private MmEarly mmEarly;// 提前终止
	@Autowired
	private MmAdaptPrin mmAdaptPrin;// 本金调整
	@Autowired
	private MmAdaptDate mmAdaptDate;// 日期调整
	@Autowired
	private MmAdjustRate mmAdjustRate;// 利率调整
	@Autowired
	private MmReverse mmReverse; // 交易冲销

	private static Logger logger = Logger.getLogger(MmServiceImpl.class);

	@Override
	public SlOutBean saveEntry(IboWksBean ibo) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		logger.info("拆借模块交易录入,输入消息:" + JSON.toJSONString(ibo));
		Dldt dldt = null;
		List<Schd> schdLst = null;
		try {
			// 1.交易要素检查
			mmEntry.DataCheck(ibo);
			// 2.转换交易主体数据
			dldt = mmEntry.DataTrans(new Dldt());// 静态参数转换
			mmEntry.DataTrans(dldt, ibo);// 设置交易信息
			mmEntry.DataTrans(dldt, mcfpService);// 交易编号
			dldtService.insert(dldt);
			// 3.转换现金流数据
			schdLst = mmEntry.DataTrans(ibo, dldt);// 设置现金流信息
			schdService.batchInsert(schdLst);
			// 4.往来帐信息
			List<Nupd> nupdLst = nupdService.builder(dldt, schdLst);
			if (nupdLst.size() > 0) {
				nupdService.batchInsert(nupdLst);
			}
			// 5.收付款信息
			List<Pmtq> pmtqLst = pmtqSerice.builder(dldt, schdLst);
			if (pmtqLst.size() > 0) {
				pmtqSerice.batchInsert(pmtqLst);
				// pmtqSerice.addDefSi(pmtqLst);
			}
			// 6.对手方清算信息
			List<Pcix> pcixLst = pcixService.builder(dldt, schdLst, ibo.getPrinSchdList().get(0).getSettleAcct());
			if (pcixLst.size() > 0) {
				pcixService.batchInsert(pcixLst);
			}
			// 7.收款信息
			prirService.insert(prirService.builder(dldt));
			// 8.付款信息
			psixService.insert(psixService.builder(dldt));
			// 9.确认信息
			cnfqService.insert(cnfqService.bulider(dldt));
			// 10.回填dealno
			mmEntry.DataTrans(dldt, mcfpService, new Mcfp());
		} catch (RException e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("拆借模块交易录入错误", e);
			throw e;
		}
		outBean.setClientNo(dldt.getDealno());
		logger.info("拆借模块交易录入,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 提前终止
	 */
	@Override
	public SlOutBean earlyTermination(IboWksBean ibo) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		Dldt dldt = null;
		logger.info("拆借模块交易提前终止,输入消息:" + JSON.toJSONString(ibo));
		try {
			// 1.交易要素检查
			mmEarly.DataCheck(ibo);
			// 查询交易信息
			dldt = dldtService.selectByTradeNo(ibo.getInstId(), ibo.getTradeNo());
			// 去掉类空格
			FinancialBeanHelper.TrimBeanAttrValue(dldt);
			// 2.1修改DLDT,对象的mdate是当前账务日期
			mmEarly.DataTrans(dldt, ibo);
			dldtService.updateByPrimaryKey(dldt);
			// 2.2删掉大于新到日期的记录:SCHD
			schdService.deleteSchdByIntEndDte(dldt.getBr(), dldt.getDealno(), dldt.getSeq(), dldt.getProduct(),
					dldt.getProdtype(), dldt.getMdate());
			// 2.3转换合并新的现金流
			List<Schd> schdLst = mmEntry.DataTrans(ibo, dldt);// 设置现金流信息
			// 新的
			List<Schd> earlyschdLst = mmEarly.DataTrans(schdLst, dldt, ibo);
			schdService.batchInsert(earlyschdLst);
			// 3.1删除大于新到期日的记录NUPD
			nupdService.deleteNupdByVdate(dldt.getBr(), dldt.getDealno(), dldt.getProduct(), dldt.getProdtype(),
					dldt.getMdate());
			// 3.2插入新的NUPD
			List<Nupd> nupdLst = nupdService.builder(dldt, schdLst);
			if (nupdLst.size() > 0) {
				Nupd nupd = nupdLst.get(0);
				nupd.setVdate(dldt.getMdate());// 新的到日期
				nupd.setPostdate(dldt.getMdate());// 账务日期
				nupd.setAmtupd(dldt.getCcyamt());// 新的到期还款金额-违约金
				nupdService.insert(nupd);
			}
			// 4.1删除大于新到期日的记录PMTQ
			pmtqSerice.deletePmtqByVdate(dldt.getBr(), dldt.getProdtype(), dldt.getDealno(), dldt.getProduct(),
					dldt.getMdate());
			// 4.2插入新的PMTQ记录
			List<Pmtq> pmtqLst = pmtqSerice.builder(dldt, schdLst);
			if (pmtqLst.size() > 0) {
				Pmtq pmtq = pmtqLst.get(0);
				pmtq.setCdate(dldt.getMdate());// 账务日期
				pmtq.setStatus("H");// 状态:默认H
				pmtq.setVdate(dldt.getMdate());// 新的到日期
				pmtq.setAmount(dldt.getCcyamt());// 新的到期还款金额-违约金
				pmtq.setLstmntdte(Calendar.getInstance().getTime());// 最后修改日期
				pmtqSerice.insert(pmtq);
			}
		} catch (RException e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("拆借模块交易录入错误", e);
			throw e;
		}
		outBean.setClientNo(dldt.getDealno());
		logger.info("同业拆借提前终止交易处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 交易冲销
	 */
	@Override
	public SlOutBean reverseTrading(IboWksBean ibo) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		Dldt dldt = null;
		Schd schd = null;
		logger.info("拆借模块交易交易冲销,输入消息:" + JSON.toJSONString(ibo));
		try {
			// 1.交易要素检查
			mmReverse.DataCheck(ibo);
			// 1.1修改DLDT
			// 查询交易信息
			dldt = dldtService.selectByTradeNo(ibo.getInstId(), ibo.getTradeNo());
			// 去掉类空格
			FinancialBeanHelper.TrimBeanAttrValue(dldt);
			// 1.1修改DLDT：dldt对象的Revdate是当前账务日期
			mmReverse.DataTrans(dldt, ibo, brpsService);
			dldtService.updateByPrimaryKey(dldt);
			// 2.1修改字段SCHD
			schd = new Schd();
			schd.setBr(dldt.getBr());
			schd.setDealno(dldt.getDealno());
			schd.setProduct(dldt.getProduct());
			schd.setProdtype(dldt.getProdtype());
			schd.setRevdate(dldt.getRevdate());// 当日账务日期
			schd.setLstmntdate(Calendar.getInstance().getTime());// 最后修改日期
			schdService.updateReverseByPrimaryKey(schd);
			// 3.1删除大于新到期日的记录NUPD
			nupdService.deleteReverseByPrimaryKey(dldt.getBr(), dldt.getDealno(), dldt.getProduct(), dldt.getProdtype(),
					dldt.getRevdate(), dldt.getMdate());
			// 3.2根据冲销时间节点新增NUPD
			List<Nupd> nupdLst = nupdService.selectReverseByPrimaryKey(dldt.getBr(), dldt.getDealno(),
					dldt.getProduct(), dldt.getProdtype());
			if (nupdLst.size() > 0) {
				for (int i = 0; i < nupdLst.size(); i++) {
					nupdLst.get(i).setSeq("R0");// 序号:默认R0
					nupdLst.get(i).setPostdate(dldt.getRevdate());// 当前账务日期
					nupdLst.get(i).setAmtupd(nupdLst.get(i).getAmtupd().negate());// 金额相反
				}
				nupdService.batchInsert(nupdLst);
			}
			// 4.1删除大于新到期日的记录PMTQ
			pmtqSerice.deleteReverseByPrimaryKey(dldt.getBr(), dldt.getProdtype(), dldt.getDealno(), dldt.getProduct(),
					dldt.getRevdate());
			// 4.2插入新的PMTQ记录
			List<Pmtq> pmtqLst = pmtqSerice.selectReverseByPrimaryKey(dldt.getBr(), dldt.getProdtype(),
					dldt.getDealno(), dldt.getProduct());
			if (pmtqLst.size() > 0) {
				for (int i = 0; i < pmtqLst.size(); i++) {
					pmtqLst.get(i).setCdate(dldt.getRevdate());// 账务日期
					pmtqLst.get(i).setStatus("AR");// 状态
					pmtqLst.get(i).setSwiftfmt("292");// 报文类型
					pmtqLst.get(i).setLstmntdte(Calendar.getInstance().getTime());// 最后修改日期
					pmtqLst.get(i).setAmount(pmtqLst.get(i).getAmount().negate());// 金额相反
				}
				pmtqSerice.batchInsert(pmtqLst);
			}
			// 5.1新增一条数据CNFQ
			Cnfq cnfq = cnfqService.bulider(dldt);
			if (cnfq != null) {
				cnfq.setCdate(dldt.getRevdate());//
				cnfq.setCtime(DateUtil.getCurrentTimeAsString());//
				cnfq.setTextstat("0");// 状态:0
				cnfq.setConftype("R");// 类型:R
				cnfqService.insert(cnfq);
			}
		} catch (RException e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("拆借模块交易录入错误", e);
			throw e;
		}
		outBean.setClientNo(dldt.getDealno());
		logger.info("同业拆借交易冲销处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 调整本金金额，追加减少
	 */
	@Override
	public SlOutBean adaptPrinAmt(IboWksBean ibo) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		Dldt dldt = null;
		logger.info("拆借模块交易调整本金金额,输入消息:" + JSON.toJSONString(ibo));
		// 1.查询交易信息
		dldt = dldtService.selectByTradeNo(ibo.getInstId(), ibo.getTradeNo());
		// 去掉类空格
		FinancialBeanHelper.TrimBeanAttrValue(dldt);
		// 2.1修改DLDT
		mmAdaptPrin.DataTrans(dldt, ibo);
		dldtService.updateByPrimaryKey(dldt);
		// 2.2删掉大于新到日期的记录:SCHD
		schdService.deleteSchdByIntEndDte(dldt.getBr(), dldt.getDealno(), dldt.getSeq(), dldt.getProduct(),
				dldt.getProdtype(), DateUtil.parse(ibo.getEmdate()));
		// 2.3转换合并新的现金流
		List<Schd> schdLst = mmEntry.DataTrans(ibo, dldt);// 设置现金流信息
		// 新的
		List<Schd> adaptPrinSchdLst = mmAdaptPrin.DataTrans(schdLst, dldt, ibo);
		schdService.batchInsert(adaptPrinSchdLst);
		// 3.1删除大于新到期日的记录NUPD
		nupdService.deleteNupdByVdate(dldt.getBr(), dldt.getDealno(), dldt.getProduct(), dldt.getProdtype(),
				DateUtil.parse(ibo.getEmdate()));
		// 3.2插入新的NUPD
		List<Nupd> nupdLst = nupdService.builder(dldt, schdLst);
		nupdService.batchInsert(nupdLst);
		// 4.1删除大于新到期日的记录PMTQ
		pmtqSerice.deletePmtqByVdate(dldt.getBr(), dldt.getProdtype(), dldt.getDealno(), dldt.getProduct(),
				DateUtil.parse(ibo.getEmdate()));
		// 4.2插入新的PMTQ记录
		List<Pmtq> pmtqLst = pmtqSerice.builder(dldt, schdLst);
		pmtqSerice.batchInsert(pmtqLst);

		outBean.setClientNo(dldt.getDealno());
		logger.info("同业拆借交易调整本金金额,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 展期
	 */
	@Override
	public SlOutBean adaptDueDate(IboWksBean ibo) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		Dldt dldt = null;
		logger.info("拆借模块交易展期,输入消息:" + JSON.toJSONString(ibo));
		// 查询交易信息
		dldt = dldtService.selectByTradeNo(ibo.getInstId(), ibo.getTradeNo());
		// 去掉类空格
		FinancialBeanHelper.TrimBeanAttrValue(dldt);
		// 2.1修改DLDT
		mmAdaptDate.DataTrans(dldt, ibo);
		dldtService.updateByPrimaryKey(dldt);
		// 2.2删掉大于新到日期的记录:SCHD
		schdService.deleteSchdByIntEndDte(dldt.getBr(), dldt.getDealno(), dldt.getSeq(), dldt.getProduct(),
				dldt.getProdtype(), DateUtil.parse(ibo.getEmdate()));
		// 2.3转换合并新的现金流
		List<Schd> schdLst = mmEntry.DataTrans(ibo, dldt);// 设置现金流信息
		// 新的
		List<Schd> adaptDateSchdLst = mmAdaptDate.DataTrans(schdLst, dldt, ibo);
		schdService.batchInsert(adaptDateSchdLst);
		// 3.1删除大于新到期日的记录NUPD
		nupdService.deleteNupdByVdate(dldt.getBr(), dldt.getDealno(), dldt.getProduct(), dldt.getProdtype(),
				DateUtil.parse(ibo.getEmdate()));
		// 3.2插入新的NUPD
		List<Nupd> nupdLst = nupdService.builder(dldt, schdLst);
		nupdService.batchInsert(nupdLst);
		// 4.1删除大于新到期日的记录PMTQ
		pmtqSerice.deletePmtqByVdate(dldt.getBr(), dldt.getProdtype(), dldt.getDealno(), dldt.getProduct(),
				DateUtil.parse(ibo.getEmdate()));
		// 4.2插入新的PMTQ记录
		List<Pmtq> pmtqLst = pmtqSerice.builder(dldt, schdLst);
		pmtqSerice.batchInsert(pmtqLst);

		outBean.setClientNo(dldt.getDealno());
		logger.info("同业拆借交易展期处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 利率调整
	 */
	@Override
	public SlOutBean adjustRate(IboWksBean ibo) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		Dldt dldt = null;
		logger.info("拆借模块交易利率调整,输入消息:" + JSON.toJSONString(ibo));
		// 查询交易信息
		dldt = dldtService.selectByTradeNo(ibo.getInstId(), ibo.getTradeNo());
		// 去掉类空格
		FinancialBeanHelper.TrimBeanAttrValue(dldt);
		// 2.1修改DLDT
		mmAdjustRate.DataTrans(dldt, ibo);
		dldtService.updateByPrimaryKey(dldt);
		// 2.2删掉大于新到日期的记录:SCHD
		schdService.deleteSchdByIntEndDte(dldt.getBr(), dldt.getDealno(), dldt.getSeq(), dldt.getProduct(),
				dldt.getProdtype(), DateUtil.parse(ibo.getEmdate()));
		// 2.3转换合并新的现金流
		List<Schd> schdLst = mmEntry.DataTrans(ibo, dldt);// 设置现金流信息
		// 新的
		List<Schd> adjustRateSchdLst = mmAdjustRate.DataTrans(schdLst, dldt, ibo);
		schdService.batchInsert(adjustRateSchdLst);
		// 3.1删除大于新到期日的记录NUPD
		nupdService.deleteNupdByVdate(dldt.getBr(), dldt.getDealno(), dldt.getProduct(), dldt.getProdtype(),
				DateUtil.parse(ibo.getEmdate()));
		// 3.2插入新的NUPD
		List<Nupd> nupdLst = nupdService.builder(dldt, schdLst);
		nupdService.batchInsert(nupdLst);
		// 4.1删除大于新到期日的记录PMTQ
		pmtqSerice.deletePmtqByVdate(dldt.getBr(), dldt.getProdtype(), dldt.getDealno(), dldt.getProduct(),
				DateUtil.parse(ibo.getEmdate()));
		// 4.2插入新的PMTQ记录
		List<Pmtq> pmtqLst = pmtqSerice.builder(dldt, schdLst);
		pmtqSerice.batchInsert(pmtqLst);

		outBean.setClientNo(dldt.getDealno());
		logger.info("同业拆借交易利率调整处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	public static void main(String[] args) {
		System.out.println(DateUtil.parse("2019-02-02").compareTo(DateUtil.parse("2019-10-0")));
	}
}
