package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Hldy;
import com.singlee.financial.wks.mapper.opics.HldyMapper;
import com.singlee.financial.wks.service.opics.HldyService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@Service
public class HldyServiceImpl implements HldyService {

    @Resource
    private HldyMapper hldyMapper;

    @Override
    public int deleteByPrimaryKey(String calendarid, String holidaytype, Date holidate) {
        return hldyMapper.deleteByPrimaryKey(calendarid, holidaytype, holidate);
    }

    @Override
    public int insert(Hldy record) {
        return hldyMapper.insert(record);
    }

    @Override
    public int insertSelective(Hldy record) {
        return hldyMapper.insertSelective(record);
    }

    @Override
    public Hldy selectByPrimaryKey(String calendarid, String holidaytype, Date holidate) {
        return hldyMapper.selectByPrimaryKey(calendarid, holidaytype, holidate);
    }

    @Override
    public int updateByPrimaryKeySelective(Hldy record) {
        return hldyMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Hldy record) {
        return hldyMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<Date> selectHldyListStr(String ccy) {
		return hldyMapper.selectHldyListStr(ccy);
    }

	@Override
	public List<Date> selectHldyListDate(List<String> ccy, Date dealDate, Date valueDate) {
		return hldyMapper.selectHldyListDate(ccy, dealDate, valueDate);
	}

}






