package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.mapper.opics.OtdtMapper;
import com.singlee.financial.wks.service.opics.OtdtService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class OtdtServiceImpl implements OtdtService {

    @Resource
    private OtdtMapper otdtMapper;

    @Override
    public int deleteByPrimaryKey(String br,String dealno,String seq) {
        return otdtMapper.deleteByPrimaryKey(br,dealno,seq);
    }

    @Override
    public int insert(Otdt record) {
        return otdtMapper.insert(record);
    }

    @Override
    public int insertSelective(Otdt record) {
        return otdtMapper.insertSelective(record);
    }

    @Override
    public Otdt selectByPrimaryKey(String br,String dealno,String seq) {
        return otdtMapper.selectByPrimaryKey(br,dealno,seq);
    }
    @Override
    public Otdt selectByTradeNo(String br, String dealtext,String dealStatus) {
        return otdtMapper.selectByTradeNo(br, dealtext,dealStatus);
    }

    @Override
    public int updateByPrimaryKeySelective(Otdt record) {
        return otdtMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Otdt record) {
        return otdtMapper.updateByPrimaryKey(record);
    }

	@Override
	public Otdt selectTriggerByTradeNo(String br, String dealtext, String dealStatus) {
		return otdtMapper.selectTriggerByTradeNo(br, dealtext, dealStatus);
	}

}
