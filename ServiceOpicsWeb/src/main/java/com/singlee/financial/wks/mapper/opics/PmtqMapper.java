package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Pmtq;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface PmtqMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq, @Param("product") String product, @Param("cdate") Date cdate, @Param("ctime") String ctime, @Param("payrecind") String payrecind);
    int deletePmtqByVdate(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("product") String product, @Param("vdate") Date vdate);
    int deleteReverseByPrimaryKey(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("product") String product, @Param("vdate") Date vdate);

    int insert(Pmtq record);

    int insertSelective(Pmtq record);

    Pmtq selectByPrimaryKey(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq, @Param("product") String product, @Param("cdate") Date cdate, @Param("ctime") String ctime, @Param("payrecind") String payrecind);

    List<Pmtq> selectReverseByPrimaryKey(@Param("br") String br, @Param("type") String type, @Param("dealno") String dealno, @Param("product") String product);

    int updateByPrimaryKeySelective(Pmtq record);

    int updateByPrimaryKey(Pmtq record);
    int updateReverseByPrimaryKey(@Param("br") String br,@Param("product") String product,@Param("type") String type, @Param("dealno") String dealno,@Param("seq") String seq,@Param("postdate") Date postdate);
    
    int updateBatch(List<Pmtq> list);

    int batchInsert(@Param("list") List<Pmtq> list);
    
    void callSpUpdrecPmtqSwiftfmt(@Param("br") String br, @Param("dealno") String dealno,
    		@Param("seq") String seq,@Param("product") String product,@Param("type") String type,
    		@Param("amenddate") Date amenddate,@Param("amendtime") String amendtime,@Param("suppressccy") String suppressccy,
    		@Param("suppresssec") String suppresssec,@Param("swiftfmtNotin") String swiftfmtNotin,
    		@Param("statusIn") String statusIn,@Param("vdateIn") Date vdateIn,@Param("convrev") BigDecimal convrev);
}