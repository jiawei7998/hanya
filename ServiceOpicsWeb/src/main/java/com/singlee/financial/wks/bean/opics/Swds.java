package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Swds implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String dealind;

    private String product;

    private String prodtype;

    private Date intenddte;

    private String schdseq;

    private String schdtype;

    private String basis;

    private Date brprcindte;

    private String calcrule;

    private String compound;

    private BigDecimal fixedintamt;

    private String fixfloatind;

    private BigDecimal implintamt;

    private BigDecimal implintrate8;

    private BigDecimal intamt;

    private Date intauthdate;

    private String intauthind;

    private String intauthoper;

    private String intccy;

    private BigDecimal intexchrate8;

    private String intexchterms;

    private BigDecimal intrate8;

    private BigDecimal intratecap8;

    private BigDecimal intratefloor8;

    private String intsacct;

    private String intsmeans;

    private Date intstrtdte;

    private Date ipaydate;

    private Date lstmntdte;

    private BigDecimal netipay;

    private String payrecind;

    private BigDecimal posnintamt;

    private BigDecimal ppayamt;

    private Date ppayauthdte;

    private String ppayauthind;

    private String ppayauthoper;

    private String ppayccy;

    private Date ppaydate;

    private BigDecimal prinadjamt;

    private BigDecimal prinamt;

    private String princcy;

    private BigDecimal princcyintamt;

    private String prinratecode;

    private String ratecode;

    private Date ratefixdte;

    private Date raterevdte;

    private Date revdate;

    private String raterevfrstlst;

    private BigDecimal spread8;

    private BigDecimal steprate8;

    private Date verdate;

    private BigDecimal pvamt;

    private BigDecimal spreadamt;

    private BigDecimal pvbamt;

    private String ps;

    private BigDecimal strike8;

    private BigDecimal index8;

    private BigDecimal particperc8;

    private String collarind;

    private BigDecimal exchrate8;

    private BigDecimal exchamt;

    private String prinsettmeans;

    private String prinsettacct;

    private Date lastcoupondate;

    private Date exchraterevdate;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private BigDecimal amount3;

    private String char1;

    private String char2;

    private String char3;

    private Date date1;

    private Date date2;

    private String formulaind;

    private BigDecimal rate18;

    private BigDecimal rate28;

    private BigDecimal factor12;

    private Date factordate;

    private Date factorfixdate;

    private BigDecimal flatintcompamt;

    private BigDecimal sumcompoundamt;

    private BigDecimal remainingpay;

    private String settle;

    private Date factorrevdate;

    private String mbspayday;

    private String intdeal;

    private String taxddealno;

    private String taxdproduct;

    private String taxdprodtype;

    private String taxdseq;

    private BigDecimal taxamt;

    private BigDecimal internalrate8;

    private BigDecimal corpspread8;

    private BigDecimal corpspreadamt;

    private String exchterms;

    private String flag2;

    private String flag3;

    private BigDecimal rate38;

    private BigDecimal rate48;

    private Date date3;

    private Date date4;

    private BigDecimal amount4;

    private BigDecimal amount5;

    private BigDecimal accrint;

    private String ccytax;

    private BigDecimal convprice12;

    private BigDecimal price12;

    private BigDecimal priceexchrate8;

    private String priceexchterms;

    private Date intnegauthdte;

    private String intnegauthind;

    private String intnegauthoper;

    private String intnegsacct;

    private String intnegsmeans;

    private Date repricedate;

    private BigDecimal rateofreturn12;

    private BigDecimal divmktmktamt;

    private BigDecimal divpayamt;

    private BigDecimal divpercent8;

    private String divpaydateind;

    private String equitytype;

    private Date breakdate;

    private BigDecimal undintpayamt;

    private Date unadjintenddte;

    private Date unadjintstrtdte;

    private BigDecimal groupid;

    private BigDecimal formulaid;

    private BigDecimal fixingoverride;

    private String shareadjaccroptind;

    private BigDecimal shareadjrate8;

    private Date ppayverdate;

    private BigDecimal ppayexchrate8;

    private String ppayexchterms;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getDealind() {
		return dealind;
	}


	public void setDealind(String dealind) {
		this.dealind = dealind;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public Date getIntenddte() {
		return intenddte;
	}


	public void setIntenddte(Date intenddte) {
		this.intenddte = intenddte;
	}


	public String getSchdseq() {
		return schdseq;
	}


	public void setSchdseq(String schdseq) {
		this.schdseq = schdseq;
	}


	public String getSchdtype() {
		return schdtype;
	}


	public void setSchdtype(String schdtype) {
		this.schdtype = schdtype;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public String getCalcrule() {
		return calcrule;
	}


	public void setCalcrule(String calcrule) {
		this.calcrule = calcrule;
	}


	public String getCompound() {
		return compound;
	}


	public void setCompound(String compound) {
		this.compound = compound;
	}


	public BigDecimal getFixedintamt() {
		return fixedintamt;
	}


	public void setFixedintamt(BigDecimal fixedintamt) {
		this.fixedintamt = fixedintamt;
	}


	public String getFixfloatind() {
		return fixfloatind;
	}


	public void setFixfloatind(String fixfloatind) {
		this.fixfloatind = fixfloatind;
	}


	public BigDecimal getImplintamt() {
		return implintamt;
	}


	public void setImplintamt(BigDecimal implintamt) {
		this.implintamt = implintamt;
	}


	public BigDecimal getImplintrate8() {
		return implintrate8;
	}


	public void setImplintrate8(BigDecimal implintrate8) {
		this.implintrate8 = implintrate8;
	}


	public BigDecimal getIntamt() {
		return intamt;
	}


	public void setIntamt(BigDecimal intamt) {
		this.intamt = intamt;
	}


	public Date getIntauthdate() {
		return intauthdate;
	}


	public void setIntauthdate(Date intauthdate) {
		this.intauthdate = intauthdate;
	}


	public String getIntauthind() {
		return intauthind;
	}


	public void setIntauthind(String intauthind) {
		this.intauthind = intauthind;
	}


	public String getIntauthoper() {
		return intauthoper;
	}


	public void setIntauthoper(String intauthoper) {
		this.intauthoper = intauthoper;
	}


	public String getIntccy() {
		return intccy;
	}


	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}


	public BigDecimal getIntexchrate8() {
		return intexchrate8;
	}


	public void setIntexchrate8(BigDecimal intexchrate8) {
		this.intexchrate8 = intexchrate8;
	}


	public String getIntexchterms() {
		return intexchterms;
	}


	public void setIntexchterms(String intexchterms) {
		this.intexchterms = intexchterms;
	}


	public BigDecimal getIntrate8() {
		return intrate8;
	}


	public void setIntrate8(BigDecimal intrate8) {
		this.intrate8 = intrate8;
	}


	public BigDecimal getIntratecap8() {
		return intratecap8;
	}


	public void setIntratecap8(BigDecimal intratecap8) {
		this.intratecap8 = intratecap8;
	}


	public BigDecimal getIntratefloor8() {
		return intratefloor8;
	}


	public void setIntratefloor8(BigDecimal intratefloor8) {
		this.intratefloor8 = intratefloor8;
	}


	public String getIntsacct() {
		return intsacct;
	}


	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}


	public String getIntsmeans() {
		return intsmeans;
	}


	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}


	public Date getIntstrtdte() {
		return intstrtdte;
	}


	public void setIntstrtdte(Date intstrtdte) {
		this.intstrtdte = intstrtdte;
	}


	public Date getIpaydate() {
		return ipaydate;
	}


	public void setIpaydate(Date ipaydate) {
		this.ipaydate = ipaydate;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public BigDecimal getNetipay() {
		return netipay;
	}


	public void setNetipay(BigDecimal netipay) {
		this.netipay = netipay;
	}


	public String getPayrecind() {
		return payrecind;
	}


	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}


	public BigDecimal getPosnintamt() {
		return posnintamt;
	}


	public void setPosnintamt(BigDecimal posnintamt) {
		this.posnintamt = posnintamt;
	}


	public BigDecimal getPpayamt() {
		return ppayamt;
	}


	public void setPpayamt(BigDecimal ppayamt) {
		this.ppayamt = ppayamt;
	}


	public Date getPpayauthdte() {
		return ppayauthdte;
	}


	public void setPpayauthdte(Date ppayauthdte) {
		this.ppayauthdte = ppayauthdte;
	}


	public String getPpayauthind() {
		return ppayauthind;
	}


	public void setPpayauthind(String ppayauthind) {
		this.ppayauthind = ppayauthind;
	}


	public String getPpayauthoper() {
		return ppayauthoper;
	}


	public void setPpayauthoper(String ppayauthoper) {
		this.ppayauthoper = ppayauthoper;
	}


	public String getPpayccy() {
		return ppayccy;
	}


	public void setPpayccy(String ppayccy) {
		this.ppayccy = ppayccy;
	}


	public Date getPpaydate() {
		return ppaydate;
	}


	public void setPpaydate(Date ppaydate) {
		this.ppaydate = ppaydate;
	}


	public BigDecimal getPrinadjamt() {
		return prinadjamt;
	}


	public void setPrinadjamt(BigDecimal prinadjamt) {
		this.prinadjamt = prinadjamt;
	}


	public BigDecimal getPrinamt() {
		return prinamt;
	}


	public void setPrinamt(BigDecimal prinamt) {
		this.prinamt = prinamt;
	}


	public String getPrinccy() {
		return princcy;
	}


	public void setPrinccy(String princcy) {
		this.princcy = princcy;
	}


	public BigDecimal getPrinccyintamt() {
		return princcyintamt;
	}


	public void setPrinccyintamt(BigDecimal princcyintamt) {
		this.princcyintamt = princcyintamt;
	}


	public String getPrinratecode() {
		return prinratecode;
	}


	public void setPrinratecode(String prinratecode) {
		this.prinratecode = prinratecode;
	}


	public String getRatecode() {
		return ratecode;
	}


	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}


	public Date getRatefixdte() {
		return ratefixdte;
	}


	public void setRatefixdte(Date ratefixdte) {
		this.ratefixdte = ratefixdte;
	}


	public Date getRaterevdte() {
		return raterevdte;
	}


	public void setRaterevdte(Date raterevdte) {
		this.raterevdte = raterevdte;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public String getRaterevfrstlst() {
		return raterevfrstlst;
	}


	public void setRaterevfrstlst(String raterevfrstlst) {
		this.raterevfrstlst = raterevfrstlst;
	}


	public BigDecimal getSpread8() {
		return spread8;
	}


	public void setSpread8(BigDecimal spread8) {
		this.spread8 = spread8;
	}


	public BigDecimal getSteprate8() {
		return steprate8;
	}


	public void setSteprate8(BigDecimal steprate8) {
		this.steprate8 = steprate8;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public BigDecimal getPvamt() {
		return pvamt;
	}


	public void setPvamt(BigDecimal pvamt) {
		this.pvamt = pvamt;
	}


	public BigDecimal getSpreadamt() {
		return spreadamt;
	}


	public void setSpreadamt(BigDecimal spreadamt) {
		this.spreadamt = spreadamt;
	}


	public BigDecimal getPvbamt() {
		return pvbamt;
	}


	public void setPvbamt(BigDecimal pvbamt) {
		this.pvbamt = pvbamt;
	}


	public String getPs() {
		return ps;
	}


	public void setPs(String ps) {
		this.ps = ps;
	}


	public BigDecimal getStrike8() {
		return strike8;
	}


	public void setStrike8(BigDecimal strike8) {
		this.strike8 = strike8;
	}


	public BigDecimal getIndex8() {
		return index8;
	}


	public void setIndex8(BigDecimal index8) {
		this.index8 = index8;
	}


	public BigDecimal getParticperc8() {
		return particperc8;
	}


	public void setParticperc8(BigDecimal particperc8) {
		this.particperc8 = particperc8;
	}


	public String getCollarind() {
		return collarind;
	}


	public void setCollarind(String collarind) {
		this.collarind = collarind;
	}


	public BigDecimal getExchrate8() {
		return exchrate8;
	}


	public void setExchrate8(BigDecimal exchrate8) {
		this.exchrate8 = exchrate8;
	}


	public BigDecimal getExchamt() {
		return exchamt;
	}


	public void setExchamt(BigDecimal exchamt) {
		this.exchamt = exchamt;
	}


	public String getPrinsettmeans() {
		return prinsettmeans;
	}


	public void setPrinsettmeans(String prinsettmeans) {
		this.prinsettmeans = prinsettmeans;
	}


	public String getPrinsettacct() {
		return prinsettacct;
	}


	public void setPrinsettacct(String prinsettacct) {
		this.prinsettacct = prinsettacct;
	}


	public Date getLastcoupondate() {
		return lastcoupondate;
	}


	public void setLastcoupondate(Date lastcoupondate) {
		this.lastcoupondate = lastcoupondate;
	}


	public Date getExchraterevdate() {
		return exchraterevdate;
	}


	public void setExchraterevdate(Date exchraterevdate) {
		this.exchraterevdate = exchraterevdate;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public BigDecimal getAmount3() {
		return amount3;
	}


	public void setAmount3(BigDecimal amount3) {
		this.amount3 = amount3;
	}


	public String getChar1() {
		return char1;
	}


	public void setChar1(String char1) {
		this.char1 = char1;
	}


	public String getChar2() {
		return char2;
	}


	public void setChar2(String char2) {
		this.char2 = char2;
	}


	public String getChar3() {
		return char3;
	}


	public void setChar3(String char3) {
		this.char3 = char3;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public String getFormulaind() {
		return formulaind;
	}


	public void setFormulaind(String formulaind) {
		this.formulaind = formulaind;
	}


	public BigDecimal getRate18() {
		return rate18;
	}


	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}


	public BigDecimal getRate28() {
		return rate28;
	}


	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}


	public BigDecimal getFactor12() {
		return factor12;
	}


	public void setFactor12(BigDecimal factor12) {
		this.factor12 = factor12;
	}


	public Date getFactordate() {
		return factordate;
	}


	public void setFactordate(Date factordate) {
		this.factordate = factordate;
	}


	public Date getFactorfixdate() {
		return factorfixdate;
	}


	public void setFactorfixdate(Date factorfixdate) {
		this.factorfixdate = factorfixdate;
	}


	public BigDecimal getFlatintcompamt() {
		return flatintcompamt;
	}


	public void setFlatintcompamt(BigDecimal flatintcompamt) {
		this.flatintcompamt = flatintcompamt;
	}


	public BigDecimal getSumcompoundamt() {
		return sumcompoundamt;
	}


	public void setSumcompoundamt(BigDecimal sumcompoundamt) {
		this.sumcompoundamt = sumcompoundamt;
	}


	public BigDecimal getRemainingpay() {
		return remainingpay;
	}


	public void setRemainingpay(BigDecimal remainingpay) {
		this.remainingpay = remainingpay;
	}


	public String getSettle() {
		return settle;
	}


	public void setSettle(String settle) {
		this.settle = settle;
	}


	public Date getFactorrevdate() {
		return factorrevdate;
	}


	public void setFactorrevdate(Date factorrevdate) {
		this.factorrevdate = factorrevdate;
	}


	public String getMbspayday() {
		return mbspayday;
	}


	public void setMbspayday(String mbspayday) {
		this.mbspayday = mbspayday;
	}


	public String getIntdeal() {
		return intdeal;
	}


	public void setIntdeal(String intdeal) {
		this.intdeal = intdeal;
	}


	public String getTaxddealno() {
		return taxddealno;
	}


	public void setTaxddealno(String taxddealno) {
		this.taxddealno = taxddealno;
	}


	public String getTaxdproduct() {
		return taxdproduct;
	}


	public void setTaxdproduct(String taxdproduct) {
		this.taxdproduct = taxdproduct;
	}


	public String getTaxdprodtype() {
		return taxdprodtype;
	}


	public void setTaxdprodtype(String taxdprodtype) {
		this.taxdprodtype = taxdprodtype;
	}


	public String getTaxdseq() {
		return taxdseq;
	}


	public void setTaxdseq(String taxdseq) {
		this.taxdseq = taxdseq;
	}


	public BigDecimal getTaxamt() {
		return taxamt;
	}


	public void setTaxamt(BigDecimal taxamt) {
		this.taxamt = taxamt;
	}


	public BigDecimal getInternalrate8() {
		return internalrate8;
	}


	public void setInternalrate8(BigDecimal internalrate8) {
		this.internalrate8 = internalrate8;
	}


	public BigDecimal getCorpspread8() {
		return corpspread8;
	}


	public void setCorpspread8(BigDecimal corpspread8) {
		this.corpspread8 = corpspread8;
	}


	public BigDecimal getCorpspreadamt() {
		return corpspreadamt;
	}


	public void setCorpspreadamt(BigDecimal corpspreadamt) {
		this.corpspreadamt = corpspreadamt;
	}


	public String getExchterms() {
		return exchterms;
	}


	public void setExchterms(String exchterms) {
		this.exchterms = exchterms;
	}


	public String getFlag2() {
		return flag2;
	}


	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}


	public String getFlag3() {
		return flag3;
	}


	public void setFlag3(String flag3) {
		this.flag3 = flag3;
	}


	public BigDecimal getRate38() {
		return rate38;
	}


	public void setRate38(BigDecimal rate38) {
		this.rate38 = rate38;
	}


	public BigDecimal getRate48() {
		return rate48;
	}


	public void setRate48(BigDecimal rate48) {
		this.rate48 = rate48;
	}


	public Date getDate3() {
		return date3;
	}


	public void setDate3(Date date3) {
		this.date3 = date3;
	}


	public Date getDate4() {
		return date4;
	}


	public void setDate4(Date date4) {
		this.date4 = date4;
	}


	public BigDecimal getAmount4() {
		return amount4;
	}


	public void setAmount4(BigDecimal amount4) {
		this.amount4 = amount4;
	}


	public BigDecimal getAmount5() {
		return amount5;
	}


	public void setAmount5(BigDecimal amount5) {
		this.amount5 = amount5;
	}


	public BigDecimal getAccrint() {
		return accrint;
	}


	public void setAccrint(BigDecimal accrint) {
		this.accrint = accrint;
	}


	public String getCcytax() {
		return ccytax;
	}


	public void setCcytax(String ccytax) {
		this.ccytax = ccytax;
	}


	public BigDecimal getConvprice12() {
		return convprice12;
	}


	public void setConvprice12(BigDecimal convprice12) {
		this.convprice12 = convprice12;
	}


	public BigDecimal getPrice12() {
		return price12;
	}


	public void setPrice12(BigDecimal price12) {
		this.price12 = price12;
	}


	public BigDecimal getPriceexchrate8() {
		return priceexchrate8;
	}


	public void setPriceexchrate8(BigDecimal priceexchrate8) {
		this.priceexchrate8 = priceexchrate8;
	}


	public String getPriceexchterms() {
		return priceexchterms;
	}


	public void setPriceexchterms(String priceexchterms) {
		this.priceexchterms = priceexchterms;
	}


	public Date getIntnegauthdte() {
		return intnegauthdte;
	}


	public void setIntnegauthdte(Date intnegauthdte) {
		this.intnegauthdte = intnegauthdte;
	}


	public String getIntnegauthind() {
		return intnegauthind;
	}


	public void setIntnegauthind(String intnegauthind) {
		this.intnegauthind = intnegauthind;
	}


	public String getIntnegauthoper() {
		return intnegauthoper;
	}


	public void setIntnegauthoper(String intnegauthoper) {
		this.intnegauthoper = intnegauthoper;
	}


	public String getIntnegsacct() {
		return intnegsacct;
	}


	public void setIntnegsacct(String intnegsacct) {
		this.intnegsacct = intnegsacct;
	}


	public String getIntnegsmeans() {
		return intnegsmeans;
	}


	public void setIntnegsmeans(String intnegsmeans) {
		this.intnegsmeans = intnegsmeans;
	}


	public Date getRepricedate() {
		return repricedate;
	}


	public void setRepricedate(Date repricedate) {
		this.repricedate = repricedate;
	}


	public BigDecimal getRateofreturn12() {
		return rateofreturn12;
	}


	public void setRateofreturn12(BigDecimal rateofreturn12) {
		this.rateofreturn12 = rateofreturn12;
	}


	public BigDecimal getDivmktmktamt() {
		return divmktmktamt;
	}


	public void setDivmktmktamt(BigDecimal divmktmktamt) {
		this.divmktmktamt = divmktmktamt;
	}


	public BigDecimal getDivpayamt() {
		return divpayamt;
	}


	public void setDivpayamt(BigDecimal divpayamt) {
		this.divpayamt = divpayamt;
	}


	public BigDecimal getDivpercent8() {
		return divpercent8;
	}


	public void setDivpercent8(BigDecimal divpercent8) {
		this.divpercent8 = divpercent8;
	}


	public String getDivpaydateind() {
		return divpaydateind;
	}


	public void setDivpaydateind(String divpaydateind) {
		this.divpaydateind = divpaydateind;
	}


	public String getEquitytype() {
		return equitytype;
	}


	public void setEquitytype(String equitytype) {
		this.equitytype = equitytype;
	}


	public Date getBreakdate() {
		return breakdate;
	}


	public void setBreakdate(Date breakdate) {
		this.breakdate = breakdate;
	}


	public BigDecimal getUndintpayamt() {
		return undintpayamt;
	}


	public void setUndintpayamt(BigDecimal undintpayamt) {
		this.undintpayamt = undintpayamt;
	}


	public Date getUnadjintenddte() {
		return unadjintenddte;
	}


	public void setUnadjintenddte(Date unadjintenddte) {
		this.unadjintenddte = unadjintenddte;
	}


	public Date getUnadjintstrtdte() {
		return unadjintstrtdte;
	}


	public void setUnadjintstrtdte(Date unadjintstrtdte) {
		this.unadjintstrtdte = unadjintstrtdte;
	}


	public BigDecimal getGroupid() {
		return groupid;
	}


	public void setGroupid(BigDecimal groupid) {
		this.groupid = groupid;
	}


	public BigDecimal getFormulaid() {
		return formulaid;
	}


	public void setFormulaid(BigDecimal formulaid) {
		this.formulaid = formulaid;
	}


	public BigDecimal getFixingoverride() {
		return fixingoverride;
	}


	public void setFixingoverride(BigDecimal fixingoverride) {
		this.fixingoverride = fixingoverride;
	}


	public String getShareadjaccroptind() {
		return shareadjaccroptind;
	}


	public void setShareadjaccroptind(String shareadjaccroptind) {
		this.shareadjaccroptind = shareadjaccroptind;
	}


	public BigDecimal getShareadjrate8() {
		return shareadjrate8;
	}


	public void setShareadjrate8(BigDecimal shareadjrate8) {
		this.shareadjrate8 = shareadjrate8;
	}


	public Date getPpayverdate() {
		return ppayverdate;
	}


	public void setPpayverdate(Date ppayverdate) {
		this.ppayverdate = ppayverdate;
	}


	public BigDecimal getPpayexchrate8() {
		return ppayexchrate8;
	}


	public void setPpayexchrate8(BigDecimal ppayexchrate8) {
		this.ppayexchrate8 = ppayexchrate8;
	}


	public String getPpayexchterms() {
		return ppayexchterms;
	}


	public void setPpayexchterms(String ppayexchterms) {
		this.ppayexchterms = ppayexchterms;
	}


	private static final long serialVersionUID = 1L;
}