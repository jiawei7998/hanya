package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Psix;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface PsixMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq);

    int insert(Psix record);

    int insertSelective(Psix record);

    Psix selectByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq);

    List<Psix>  selectPsixList(Psix record);
    
    int updateByPrimaryKeySelective(Psix record);

    int updateByPrimaryKey(Psix record);
    
    Psix selectByDealno(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno);
}