package com.singlee.financial.wks.bean.opics;

import lombok.Data;

@Data
public class Cusi {
    private String custidtype;

    private String custaltid;

    private String cno;

	public String getCustidtype() {
		return custidtype;
	}

	public void setCustidtype(String custidtype) {
		this.custidtype = custidtype;
	}

	public String getCustaltid() {
		return custaltid;
	}

	public void setCustaltid(String custaltid) {
		this.custaltid = custaltid;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}
    
    
}