package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Cspi;
import com.singlee.financial.wks.mapper.opics.CspiMapper;
import com.singlee.financial.wks.service.opics.CspiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CspiServiceImpl implements CspiService {

    @Resource
    private CspiMapper cspiMapper;

    @Override
    public int deleteByPrimaryKey(String br,String cno,String product,String type,String ccy) {
        return cspiMapper.deleteByPrimaryKey(br,cno,product,type,ccy);
    }

    @Override
    public int insert(Cspi record) {
        return cspiMapper.insert(record);
    }

    @Override
    public int insertSelective(Cspi record) {
        return cspiMapper.insertSelective(record);
    }

    @Override
    public Cspi selectByPrimaryKey(String br,String cno,String product,String type,String ccy) {
        return cspiMapper.selectByPrimaryKey(br,cno,product,type,ccy);
    }

    @Override
    public int updateByPrimaryKeySelective(Cspi record) {
        return cspiMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Cspi record) {
        return cspiMapper.updateByPrimaryKey(record);
    }

}
