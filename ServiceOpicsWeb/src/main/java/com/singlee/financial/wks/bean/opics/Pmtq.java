package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Pmtq implements Serializable {
    private String br;

    private String type;

    private String dealno;

    private String seq;

    private String product;

    private Date cdate;

    private String ctime;

    private String payrecind;

    private Date vdate;

    private String paytype;

    private String status;

    private String ccy;

    private BigDecimal amount;

    private String cno;

    private String msgprty;

    private String swiftfmt;

    private Date reldte;

    private String reltime;

    private Date lstmntdte;

    private String setmeans;

    private String setacct;

    private String comrefno;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public Date getCdate() {
		return cdate;
	}


	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}


	public String getCtime() {
		return ctime;
	}


	public void setCtime(String ctime) {
		this.ctime = ctime;
	}


	public String getPayrecind() {
		return payrecind;
	}


	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}


	public Date getVdate() {
		return vdate;
	}


	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}


	public String getPaytype() {
		return paytype;
	}


	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getMsgprty() {
		return msgprty;
	}


	public void setMsgprty(String msgprty) {
		this.msgprty = msgprty;
	}


	public String getSwiftfmt() {
		return swiftfmt;
	}


	public void setSwiftfmt(String swiftfmt) {
		this.swiftfmt = swiftfmt;
	}


	public Date getReldte() {
		return reldte;
	}


	public void setReldte(Date reldte) {
		this.reldte = reldte;
	}


	public String getReltime() {
		return reltime;
	}


	public void setReltime(String reltime) {
		this.reltime = reltime;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public String getSetmeans() {
		return setmeans;
	}


	public void setSetmeans(String setmeans) {
		this.setmeans = setmeans;
	}


	public String getSetacct() {
		return setacct;
	}


	public void setSetacct(String setacct) {
		this.setacct = setacct;
	}


	public String getComrefno() {
		return comrefno;
	}


	public void setComrefno(String comrefno) {
		this.comrefno = comrefno;
	}


	private static final long serialVersionUID = 1L;
}