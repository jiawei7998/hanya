package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Swdh;
import org.apache.ibatis.annotations.Param;

public interface SwdhMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind);

    int insert(Swdh record);

    int insertSelective(Swdh record);

    Swdh selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind);

    Swdh selectByTradeNo(@Param("br") String br, @Param("dealtext") String tradeNo,@Param("status") String status);

    int updateByPrimaryKeySelective(Swdh record);

    int updateByPrimaryKey(Swdh record);
    
    int updateByTerminate(Swdh record); 
}