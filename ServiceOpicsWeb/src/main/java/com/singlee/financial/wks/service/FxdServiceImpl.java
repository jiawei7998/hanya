package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.FxdWksBean;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.FxdItemWksBean;
import com.singlee.financial.wks.expand.SwiftMethod;
import com.singlee.financial.wks.intfc.FxdService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.FxdEntry;
import com.singlee.financial.wks.service.trans.FxdReverse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * 外汇模块服务实现类
 *
 * @author shenzl
 * @date 2020/01/09
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FxdServiceImpl implements FxdService {

	@Autowired
	private FxdhService fxdhService;
	@Autowired
	private FxdtService fxdtService;
	@Autowired
	private McfpService mcfpService;
	@Autowired
	private PcixService pcixService;
	@Autowired
	private PrirService prirService;
	@Autowired
	private PsixService psixService;
	@Autowired
	private CnfqService cnfqService;
	@Autowired
	private NupdService nupdService;
	@Autowired
	private PmtqService pmtqSerice;
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private CpprService cpprService;
	@Autowired
	private FxdEntry fxdEntry;// 外汇交易录入转换
	@Autowired
	private FxdReverse fxdReverse;// 外汇冲销录入转换

	private Logger logger = Logger.getLogger(FxdServiceImpl.class);

	// 获取外汇交易信息
	@Override
	public SlOutBean saveStopFwdEntry(FxdWksBean fxdWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "FXD-00000", "外汇即/远期录入成功!");
		FxdItemWksBean itemBean = null;
		List<String> dealnos = null;
		String dealType = "SPOTFORWARD";
		try {
			fxdEntry.DataCheck(fxdWksBean);
			// 保存交易DEALNO
			dealnos = new ArrayList<String>();
			itemBean = fxdWksBean.getItemBeanLst().get(0);
			// 生成dealo
			fxdEntry.DataTrans(itemBean.getInstId(), dealType, dealnos, mcfpService);
			addFxdDeal(itemBean, dealnos, dealType);
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("外汇/远期交易录入错误", e);
			throw e;
		}
		outBean.setClientNo(createClientNo(dealnos));
		logger.info("外汇/远期交易处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 添加外汇交易
	 * 
	 * @param fxdWksBean
	 * @return
	 */
	private void addFxdDeal(FxdItemWksBean itemBean, List<String> dealnos, String dealType) {
		Fxdh fxdh = null;
		// 1.交易要素检查
		fxdEntry.DataCheck(itemBean);
		// 2.转换交易主体数据
		fxdh = fxdEntry.DataTrans(new Fxdh());// 静态数据
		fxdEntry.DataTrans(fxdh, itemBean, dealnos, dealType);// 设置dealno
		logger.debug("请求的FxdItemWksBean对象信息:" + itemBean);
		logger.debug("转换后的FXDH对象信息:" + fxdh);
		if(brpsService.isBatchRunning(fxdh.getBr())) {
			throw new RuntimeException("系统跑批中请稍后操作!");
		}
		fxdhService.insert(fxdh);// 保存交易信息
		// 3.转换拆分交易信息
		fxdtService.insert(fxdEntry.DataTrans(new Fxdt(), fxdh, dealType));
		// 4.往来帐信息
		List<Nupd> nupdLst = nupdService.builder(fxdh);
		if (nupdLst.size() > 0) {
			nupdService.batchInsert(nupdLst);
		}
		// 5.收付款信息
		List<Pmtq> pmtqLst = pmtqSerice.builder(fxdh,itemBean.getSwiftMethod());
		if (pmtqLst.size() > 0) {
			pmtqSerice.batchInsert(pmtqLst);
		}
		// 6.对手方清算信息
		List<Pcix> pcixLst = pcixService.builder(fxdh, itemBean);
		if (pcixLst.size() > 0) {
			pcixService.batchInsert(pcixLst);
		}
		// 7.新增CPPR
		Cppr cppr = cpprService.selectByPrimaryKey(fxdh.getBr(), fxdh.getCcy(), fxdh.getCtrccy(), StringUtils.trim(fxdh.getCost()),
				StringUtils.trim(fxdh.getPort()), StringUtils.trim(fxdh.getTrad()),
				fxdh.getVdate().compareTo(fxdh.getBrprcindte()) >= 0 ? fxdh.getVdate() : fxdh.getBrprcindte());
		boolean isCpprExist = null == cppr ? false : true;
		cppr = cpprService.builder(fxdh,cppr);
		if (!isCpprExist) {// 空
			cpprService.insert(cppr);
		} else {
			cpprService.updateCpprByFxdh(cppr);
		}
		// 8.收款信息
		prirService.insert(prirService.builder(fxdh, itemBean));
		// 9.付款信息
		psixService.insert(psixService.builder(fxdh, itemBean));
		// 10.确认信息
		cnfqService.insert(cnfqService.bulider(fxdh,itemBean.getSwiftMethod()));
		// 11.回填dealno
		fxdEntry.DataTrans(itemBean.getInstId(), dealnos, mcfpService, new Mcfp());
	}

	@Override
	public SlOutBean reverseTrading(FxdWksBean fxdWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "FXD-00000", "外汇即/远期冲销成功!");
		List<Fxdh> fxdhLst = null;// 声明
		// 保存交易DEALNO
		List<String> dealnos = new ArrayList<String>();
		try {
			// 查询交易信息
			fxdhLst = fxdhService.selectByTradeNo(null, fxdWksBean.getApproveNo(), "R");
			if (null == fxdhLst || fxdhLst.size() == 0) {
				throw new RuntimeException("交易[" + fxdWksBean.getApproveNo() + "]不存在");
			}
			// 1.1修改FXDH
			fxdReverse.DataTrans(fxdhLst, brpsService);
			fxdhService.updateBatch(fxdhLst);

			fxdhLst.stream().forEach(fxdh -> {
				if(brpsService.isBatchRunning(fxdh.getBr())) {
					throw new RuntimeException("系统跑批中请稍后操作!");
				}
				// 1.2修改FXDT表字段
				Fxdt fxdt = new Fxdt();
				fxdReverse.DataTrans(fxdt, fxdh.getBr(), fxdh.getDealno(), OpicsConstant.FX.SEQ_ZERO);
				fxdtService.updateReverseByPrimaryKey(fxdt);
				// 3.1删除大于新到期日的记录NUPD
				nupdService.deleteReverseFxdhByPrimaryKey(fxdh.getBr(), fxdh.getDealno(), fxdh.getProdcode(),
						fxdh.getProdtype(), fxdh.getRevdate());

				// 3.2根据冲销时间节点新增NUPD
				List<Nupd> nupdLst = nupdService.selectReverseByPrimaryKey(fxdh.getBr(), fxdh.getDealno(),
						fxdh.getProdcode(), fxdh.getProdtype());
				if (nupdLst.size() > 0) {
					for (int i = 0; i < nupdLst.size(); i++) {
						nupdLst.get(i).setSeq("R0");// 序号:默认R0
						nupdLst.get(i).setPostdate(fxdh.getRevdate());// 当前账务日期
						nupdLst.get(i).setAmtupd(nupdLst.get(i).getAmtupd().negate());// 金额相反
					}
					nupdService.batchInsert(nupdLst);
				}
				// 4.1修改状态--未发报文PMTQ
				pmtqSerice.updateReverseByPrimaryKey(fxdh.getBr(), fxdh.getProdcode(), fxdh.getProdtype(),
						fxdh.getDealno(), fxdh.getSeq(), fxdh.getRevdate());
				// 4.2删除大于新到期日的记录-已到期PMTQ
				pmtqSerice.deleteReverseByPrimaryKey(fxdh.getBr(), fxdh.getProdtype(), fxdh.getDealno(),
						fxdh.getProdcode(), fxdh.getRevdate());
				// 4.2插入新的PMTQ记录
				List<Pmtq> pmtqLst = pmtqSerice.selectReverseByPrimaryKey(fxdh.getBr(), fxdh.getProdtype(),
						fxdh.getDealno(), fxdh.getProdcode());
				if (pmtqLst.size() > 0) {
					for (int i = 0; i < pmtqLst.size(); i++) {
						pmtqLst.get(i).setCdate(fxdh.getRevdate());// 账务日期
						pmtqLst.get(i).setCtime(DateUtil.getTimeAsStringByCount(i));
						pmtqLst.get(i).setStatus("AR");// 状态
						pmtqLst.get(i).setSwiftfmt("292");// 报文类型
						pmtqLst.get(i).setLstmntdte(Calendar.getInstance().getTime());// 最后修改日期
						pmtqLst.get(i).setAmount(pmtqLst.get(i).getAmount());// 金额相反
					}
					pmtqSerice.batchInsert(pmtqLst);
				}
				// 5.1删除CNFQ数据
				cnfqService.deleteReverseByPrimaryKey(fxdh.getBr(), fxdh.getProdtype(), fxdh.getDealno(), fxdh.getSeq(),
						fxdh.getProdcode());

				// 5.2冲销后需要新增一笔CNFQ
				Cnfq cnfq = cnfqService.bulider(fxdh,SwiftMethod.ENABLE);
				cnfq.setCdate(fxdh.getRevdate());//
				cnfq.setCtime(DateUtil.getCurrentTimeAsString());//
				cnfq.setTextstat("0");// 状态:0
				cnfq.setConftype("R");// 类型:R
				cnfqService.insert(cnfq);
				
				// 2.1更新头寸CPPR
				Cppr cppr = cpprService.selectByPrimaryKey(fxdh.getBr(), fxdh.getCcy(), fxdh.getCtrccy(), StringUtils.trim(fxdh.getCost()),
						StringUtils.trim(fxdh.getPort()), StringUtils.trim(fxdh.getTrad()),
						fxdh.getVdate().compareTo(fxdh.getBrprcindte()) >= 0 ? fxdh.getVdate() : fxdh.getBrprcindte());
				cppr = cpprService.builder(fxdh,cppr);
				cpprService.updateCpprByFxdh(cppr);				

				dealnos.add(fxdh.getDealno());
			});

		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("外汇/远期交易冲销错误", e);
			throw e;
		}
		outBean.setClientNo(createClientNo(dealnos));
		logger.info("外汇交易冲销处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 组装外汇交易dealno
	 * 
	 * @param dealnos
	 * @return
	 */
	private String createClientNo(List<String> dealnos) {
		String clientNo = null;
		if (null == dealnos || dealnos.size() == 0) {
			clientNo = "";
		}
		switch (dealnos.size()) {
		case 1:
			clientNo = StringUtils.trim(dealnos.get(0));
			break;
		case 2:
			clientNo = StringUtils.trim(dealnos.get(0)) + "/" + StringUtils.trim(dealnos.get(1));
			break;
		}

		return StringUtils.trim(clientNo);
	}

	@Override
	public SlOutBean saveSwapEntry(FxdWksBean fxdWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "FXD-00000", "外汇掉期录入成功!");
		// 保存交易DEALNO
		List<String> dealnos = new ArrayList<String>();
		String dealType = "FXSWAP";
		try {
			fxdEntry.DataCheck(fxdWksBean);
			// 生成dealo
			fxdEntry.DataTrans(fxdWksBean.getItemBeanLst().get(0).getInstId(), dealType, dealnos, mcfpService);
			fxdWksBean.getItemBeanLst().stream().forEach(item -> {
				addFxdDeal(item, dealnos, dealType);
			});
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("外汇掉期交易录入错误", e);
			throw e;
		}
		outBean.setClientNo(createClientNo(dealnos));
		outBean.setTellSeqNo(fxdWksBean.getApproveNo());
		logger.info("外汇掉期交易录入处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	@Override
	public SlOutBean saveStopFwdByDealInfo(FxdWksBean fxdWksBean) {
		String dealType = "SPOTFORWARD";
		fxdEntry.DataCheck(fxdWksBean);
		fxdEntry.DataTrans(fxdWksBean.getApproveNo(), fxdWksBean.getItemBeanLst().get(0), dealType);
		fxdEntry.DataTrans(fxdWksBean.getItemBeanLst(), dealType);
		return saveStopFwdEntry(fxdWksBean);
	}

	@Override
	public SlOutBean saveSwapByDealInfo(FxdWksBean fxdWksBean) {
		String dealType = "FXSWAP";
		fxdEntry.DataCheck(fxdWksBean);

		fxdWksBean.getItemBeanLst().stream().forEach(item -> {
			FxdItemWksBean other = null;
			fxdEntry.DataTrans(fxdWksBean.getApproveNo(), item, dealType);
			Optional<FxdItemWksBean> op = fxdWksBean.getItemBeanLst().stream()
					.filter(fxd -> fxd.getFarNearind().equalsIgnoreCase("F".equalsIgnoreCase(item.getFarNearind()) ? "N"
							: "N".equalsIgnoreCase(item.getFarNearind()) ? "F" : ""))
					.findFirst();
			if (!op.isPresent()) {
				throw new RuntimeException("获取掉期交易另外一端异常");
			}
			other = op.get();
			item.setSwapVdate(other.getVDate());
		});
		System.out.println("测试查询转换后的对象值:" + fxdWksBean);
		fxdEntry.DataTrans(fxdWksBean.getItemBeanLst(), dealType);
		return saveSwapEntry(fxdWksBean);
	}
}
