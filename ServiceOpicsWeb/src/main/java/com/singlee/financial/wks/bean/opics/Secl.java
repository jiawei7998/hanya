package com.singlee.financial.wks.bean.opics;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Secl {
    private static final long serialVersionUID = 1L;
    private String br;

    private String secid;

    private BigDecimal askprice8;

    private BigDecimal askpriceyst8;

    private BigDecimal askrate8;

    private BigDecimal askrateyst8;

    private BigDecimal bidprice8;

    private BigDecimal bidpriceyst8;

    private BigDecimal bidrate8;

    private BigDecimal bidrateyst8;

    private BigDecimal clsgprice8;

    private BigDecimal clsgpriceyst8;

    private BigDecimal clsgrate8;

    private BigDecimal clsgrateyst8;

    private BigDecimal coupremainamt;

    private BigDecimal currfixrate8;

    private String efflagday;

    private BigDecimal fixbaserate8;

    private String fixbaseterms;

    private String fixcycle;

    private String fixfreq;

    private String fixday;

    private String fixlagday;

    private BigDecimal fixspotrate8;

    private String fixspotdays;

    private String fixspotterms;

    private Date lstprcemntdate;

    private String markmakind;

    private Date nexteffdate;

    private Date nextfixdate;

    private BigDecimal nextfixrate8;

    private Date nextipaydate;

    private BigDecimal offclsgprice8;

    private BigDecimal offclsgpriceyst8;

    private BigDecimal offclsgrate8;

    private BigDecimal offclsgrateyst8;

    private Date prevcoupdate;

    private BigDecimal prevcouprate8;

    private String paycno;

    private String secsacct;

    private String settmeans;

    private String settacct;

    private String taxind;

    private Date lstmntdate;

    private Date effdate;

    private Date fixdate;

    private Date excldate;

    private BigDecimal tdyaccint8;

    private BigDecimal ystaccint8;

    private String pricesourceDde;

    private String yieldsourceDde;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public BigDecimal getAskprice8() {
		return askprice8;
	}

	public void setAskprice8(BigDecimal askprice8) {
		this.askprice8 = askprice8;
	}

	public BigDecimal getAskpriceyst8() {
		return askpriceyst8;
	}

	public void setAskpriceyst8(BigDecimal askpriceyst8) {
		this.askpriceyst8 = askpriceyst8;
	}

	public BigDecimal getAskrate8() {
		return askrate8;
	}

	public void setAskrate8(BigDecimal askrate8) {
		this.askrate8 = askrate8;
	}

	public BigDecimal getAskrateyst8() {
		return askrateyst8;
	}

	public void setAskrateyst8(BigDecimal askrateyst8) {
		this.askrateyst8 = askrateyst8;
	}

	public BigDecimal getBidprice8() {
		return bidprice8;
	}

	public void setBidprice8(BigDecimal bidprice8) {
		this.bidprice8 = bidprice8;
	}

	public BigDecimal getBidpriceyst8() {
		return bidpriceyst8;
	}

	public void setBidpriceyst8(BigDecimal bidpriceyst8) {
		this.bidpriceyst8 = bidpriceyst8;
	}

	public BigDecimal getBidrate8() {
		return bidrate8;
	}

	public void setBidrate8(BigDecimal bidrate8) {
		this.bidrate8 = bidrate8;
	}

	public BigDecimal getBidrateyst8() {
		return bidrateyst8;
	}

	public void setBidrateyst8(BigDecimal bidrateyst8) {
		this.bidrateyst8 = bidrateyst8;
	}

	public BigDecimal getClsgprice8() {
		return clsgprice8;
	}

	public void setClsgprice8(BigDecimal clsgprice8) {
		this.clsgprice8 = clsgprice8;
	}

	public BigDecimal getClsgpriceyst8() {
		return clsgpriceyst8;
	}

	public void setClsgpriceyst8(BigDecimal clsgpriceyst8) {
		this.clsgpriceyst8 = clsgpriceyst8;
	}

	public BigDecimal getClsgrate8() {
		return clsgrate8;
	}

	public void setClsgrate8(BigDecimal clsgrate8) {
		this.clsgrate8 = clsgrate8;
	}

	public BigDecimal getClsgrateyst8() {
		return clsgrateyst8;
	}

	public void setClsgrateyst8(BigDecimal clsgrateyst8) {
		this.clsgrateyst8 = clsgrateyst8;
	}

	public BigDecimal getCoupremainamt() {
		return coupremainamt;
	}

	public void setCoupremainamt(BigDecimal coupremainamt) {
		this.coupremainamt = coupremainamt;
	}

	public BigDecimal getCurrfixrate8() {
		return currfixrate8;
	}

	public void setCurrfixrate8(BigDecimal currfixrate8) {
		this.currfixrate8 = currfixrate8;
	}

	public String getEfflagday() {
		return efflagday;
	}

	public void setEfflagday(String efflagday) {
		this.efflagday = efflagday;
	}

	public BigDecimal getFixbaserate8() {
		return fixbaserate8;
	}

	public void setFixbaserate8(BigDecimal fixbaserate8) {
		this.fixbaserate8 = fixbaserate8;
	}

	public String getFixbaseterms() {
		return fixbaseterms;
	}

	public void setFixbaseterms(String fixbaseterms) {
		this.fixbaseterms = fixbaseterms;
	}

	public String getFixcycle() {
		return fixcycle;
	}

	public void setFixcycle(String fixcycle) {
		this.fixcycle = fixcycle;
	}

	public String getFixfreq() {
		return fixfreq;
	}

	public void setFixfreq(String fixfreq) {
		this.fixfreq = fixfreq;
	}

	public String getFixday() {
		return fixday;
	}

	public void setFixday(String fixday) {
		this.fixday = fixday;
	}

	public String getFixlagday() {
		return fixlagday;
	}

	public void setFixlagday(String fixlagday) {
		this.fixlagday = fixlagday;
	}

	public BigDecimal getFixspotrate8() {
		return fixspotrate8;
	}

	public void setFixspotrate8(BigDecimal fixspotrate8) {
		this.fixspotrate8 = fixspotrate8;
	}

	public String getFixspotdays() {
		return fixspotdays;
	}

	public void setFixspotdays(String fixspotdays) {
		this.fixspotdays = fixspotdays;
	}

	public String getFixspotterms() {
		return fixspotterms;
	}

	public void setFixspotterms(String fixspotterms) {
		this.fixspotterms = fixspotterms;
	}

	public Date getLstprcemntdate() {
		return lstprcemntdate;
	}

	public void setLstprcemntdate(Date lstprcemntdate) {
		this.lstprcemntdate = lstprcemntdate;
	}

	public String getMarkmakind() {
		return markmakind;
	}

	public void setMarkmakind(String markmakind) {
		this.markmakind = markmakind;
	}

	public Date getNexteffdate() {
		return nexteffdate;
	}

	public void setNexteffdate(Date nexteffdate) {
		this.nexteffdate = nexteffdate;
	}

	public Date getNextfixdate() {
		return nextfixdate;
	}

	public void setNextfixdate(Date nextfixdate) {
		this.nextfixdate = nextfixdate;
	}

	public BigDecimal getNextfixrate8() {
		return nextfixrate8;
	}

	public void setNextfixrate8(BigDecimal nextfixrate8) {
		this.nextfixrate8 = nextfixrate8;
	}

	public Date getNextipaydate() {
		return nextipaydate;
	}

	public void setNextipaydate(Date nextipaydate) {
		this.nextipaydate = nextipaydate;
	}

	public BigDecimal getOffclsgprice8() {
		return offclsgprice8;
	}

	public void setOffclsgprice8(BigDecimal offclsgprice8) {
		this.offclsgprice8 = offclsgprice8;
	}

	public BigDecimal getOffclsgpriceyst8() {
		return offclsgpriceyst8;
	}

	public void setOffclsgpriceyst8(BigDecimal offclsgpriceyst8) {
		this.offclsgpriceyst8 = offclsgpriceyst8;
	}

	public BigDecimal getOffclsgrate8() {
		return offclsgrate8;
	}

	public void setOffclsgrate8(BigDecimal offclsgrate8) {
		this.offclsgrate8 = offclsgrate8;
	}

	public BigDecimal getOffclsgrateyst8() {
		return offclsgrateyst8;
	}

	public void setOffclsgrateyst8(BigDecimal offclsgrateyst8) {
		this.offclsgrateyst8 = offclsgrateyst8;
	}

	public Date getPrevcoupdate() {
		return prevcoupdate;
	}

	public void setPrevcoupdate(Date prevcoupdate) {
		this.prevcoupdate = prevcoupdate;
	}

	public BigDecimal getPrevcouprate8() {
		return prevcouprate8;
	}

	public void setPrevcouprate8(BigDecimal prevcouprate8) {
		this.prevcouprate8 = prevcouprate8;
	}

	public String getPaycno() {
		return paycno;
	}

	public void setPaycno(String paycno) {
		this.paycno = paycno;
	}

	public String getSecsacct() {
		return secsacct;
	}

	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}

	public String getSettmeans() {
		return settmeans;
	}

	public void setSettmeans(String settmeans) {
		this.settmeans = settmeans;
	}

	public String getSettacct() {
		return settacct;
	}

	public void setSettacct(String settacct) {
		this.settacct = settacct;
	}

	public String getTaxind() {
		return taxind;
	}

	public void setTaxind(String taxind) {
		this.taxind = taxind;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public Date getEffdate() {
		return effdate;
	}

	public void setEffdate(Date effdate) {
		this.effdate = effdate;
	}

	public Date getFixdate() {
		return fixdate;
	}

	public void setFixdate(Date fixdate) {
		this.fixdate = fixdate;
	}

	public Date getExcldate() {
		return excldate;
	}

	public void setExcldate(Date excldate) {
		this.excldate = excldate;
	}

	public BigDecimal getTdyaccint8() {
		return tdyaccint8;
	}

	public void setTdyaccint8(BigDecimal tdyaccint8) {
		this.tdyaccint8 = tdyaccint8;
	}

	public BigDecimal getYstaccint8() {
		return ystaccint8;
	}

	public void setYstaccint8(BigDecimal ystaccint8) {
		this.ystaccint8 = ystaccint8;
	}

	public String getPricesourceDde() {
		return pricesourceDde;
	}

	public void setPricesourceDde(String pricesourceDde) {
		this.pricesourceDde = pricesourceDde;
	}

	public String getYieldsourceDde() {
		return yieldsourceDde;
	}

	public void setYieldsourceDde(String yieldsourceDde) {
		this.yieldsourceDde = yieldsourceDde;
	}
    
}