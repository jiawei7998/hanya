package com.singlee.financial.wks.service.opics;

import java.util.List;
import com.singlee.financial.wks.bean.opics.Ychd;
public interface YchdService{


    int deleteByPrimaryKey(String br,String ccy,String yieldcurve);

    int insert(Ychd record);

    int insertSelective(Ychd record);

    Ychd selectByPrimaryKey(String br,String ccy,String yieldcurve);

    int updateByPrimaryKeySelective(Ychd record);

    int updateByPrimaryKey(Ychd record);

    int updateBatch(List<Ychd> list);

    int updateBatchSelective(List<Ychd> list);

}
