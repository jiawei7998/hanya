package com.singlee.financial.wks.service.opics.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.financial.wks.mapper.opics.VoldMapper;
import java.math.BigDecimal;
import java.util.List;
import com.singlee.financial.wks.bean.opics.Vold;
import com.singlee.financial.wks.service.opics.VoldService;
@Service
public class VoldServiceImpl implements VoldService{

    @Resource
    private VoldMapper voldMapper;

    @Override
    public int deleteByPrimaryKey(String br,String optiontype,String optiontypekey,String desmat,String callput,BigDecimal strikeprice8,String term) {
        return voldMapper.deleteByPrimaryKey(br,optiontype,optiontypekey,desmat,callput,strikeprice8,term);
    }

    @Override
    public int insert(Vold record) {
        return voldMapper.insert(record);
    }

    @Override
    public int insertSelective(Vold record) {
        return voldMapper.insertSelective(record);
    }

    @Override
    public Vold selectByPrimaryKey(String br,String optiontype,String optiontypekey,String desmat,String callput,BigDecimal strikeprice8,String term) {
        return voldMapper.selectByPrimaryKey(br,optiontype,optiontypekey,desmat,callput,strikeprice8,term);
    }

    @Override
    public int updateByPrimaryKeySelective(Vold record) {
        return voldMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Vold record) {
        return voldMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateBatch(List<Vold> list) {
        return voldMapper.updateBatch(list);
    }

    @Override
    public int updateBatchSelective(List<Vold> list) {
        return voldMapper.updateBatchSelective(list);
    }

}
