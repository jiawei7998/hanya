package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SecmWksBean;
import com.singlee.financial.wks.bean.opics.Cust;
import com.singlee.financial.wks.bean.opics.Secl;
import com.singlee.financial.wks.bean.opics.Secm;
import com.singlee.financial.wks.bean.opics.Secs;
import com.singlee.financial.wks.expand.SecsWksBean;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.CustService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 债券信息转换方法
 *
 * @author xuqq
 */
@Component
public class SecmEntry {


    @Autowired
    private CustService custService;

    /**
     * 债券信息检查
     *
     * @param secmWksBean
     * @param custService
     */
    public void DataCheck(SecmWksBean secm) {
        // 1.判断对象是否存在
        if (null == secm) {
            JY.raise("%s:%s,请补充完整IboWksBean对象", WksErrorCode.BOND_NULL.getErrCode(), WksErrorCode.BOND_NULL.getErrMsg());
        }
        // 2.检查交易对手
        Cust cust = custService.selectByPrimaryKey(secm.getBIssuer());
        if (StringUtils.isEmpty(secm.getBIssuer()) || null == cust) {
            JY.raise("%s:%s,请输入正确格式SecmWksBean.BIssuer,For.[BIssuer=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(),
                    WksErrorCode.CUST_NOT_FOUND.getErrMsg());
        }
    }

    /**
     * 静态默认数据,该处禁止查询数据库进行转换值,会出现空指针
     *
     * @param secm
     * @return
     */
    public Secm DataTrans(Secm secm) {
        //管理要素
        secm.setRatedecs(OpicsConstant.SECM.RATEDECS);//利率小数位数:
        //默认不填
        secm.setRatetol(OpicsConstant.SECM.RATETOL);//利率允许浮动百分比:
        secm.setPricedecs(OpicsConstant.SECM.PRICEDECS);//价格小数位数:8/12位选择
        secm.setCashdecs(OpicsConstant.SECM.CASHDECS);//现金流小数位数:
        secm.setAidecs(OpicsConstant.SECM.AIDECS);//AI小数位数:
        //静态数据
        secm.setCallprice8(BigDecimal.ZERO);//赎回价格:
        secm.setClsgindxrate8(BigDecimal.ZERO);//收盘价:
        secm.setConvcouprate8(BigDecimal.ZERO);//更新后息票利率:
        secm.setInitindexrate8(BigDecimal.ZERO);//初始的指数利率:
        secm.setLstmntdate(Calendar.getInstance().getTime());//上次更新日期:
        secm.setPricerndrule(OpicsConstant.SECM.PRICERNDRULE);//价格取整规则:U是四舍五入方式
        secm.setPutprice8(BigDecimal.ZERO);//卖出价格:
        secm.setSpreadrate8(BigDecimal.ZERO);//利率差:
        secm.setTenor(OpicsConstant.SECM.TENOR);//期限:默认99
        //默认不填
        secm.setPricetol(OpicsConstant.SECM.PRICETOL);//价格允许浮动百分比:
        
        secm.setMindenom(OpicsConstant.SECM.MINDENOM);//最少单位量:
        secm.setIntexchrate8(new BigDecimal("1"));//利息汇率:
        secm.setIntexchterms(OpicsConstant.SECM.INTEXCHTERMS);//利息汇率乘除形式:
        secm.setRepoflag(OpicsConstant.SECM.REPOFLAG);//回购标志:
        secm.setEmtaind(OpicsConstant.SECM.EMTAIND);//EMT选项:
        secm.setFixrate8(BigDecimal.ZERO);//固定利率:
        secm.setMaxmfix8(BigDecimal.ZERO);//固定:
        secm.setMaxmlife8(BigDecimal.ZERO);//最大腿值:
        secm.setLastamort8(BigDecimal.ZERO);//上一次偿付金额:
        secm.setUpdatecounter(OpicsConstant.SECM.UPDATECOUNTER);//更新次数:
        secm.setPhysdelind(OpicsConstant.SECM.PHYSDELIND);//实际交货标志:
        secm.setPerratefloor12(BigDecimal.ZERO);//期限范围内利率下限:
        secm.setLiferatefloor12(BigDecimal.ZERO);//全生命周期的利率下限:
        secm.setPmtcap12(BigDecimal.ZERO);//付款上限:
        secm.setWithtaxrate12(BigDecimal.ZERO);//计提税率:
        secm.setFactsecind(OpicsConstant.SECM.FACTSECIND);//面值债券选项:
        secm.setAmt1(BigDecimal.ZERO);//备用1:
        secm.setAmt2(BigDecimal.ZERO);//备用2:
        secm.setRate112(BigDecimal.ZERO);//备用3:
        secm.setRate212(BigDecimal.ZERO);//备用4:
        secm.setIlind(OpicsConstant.SECM.ILIND);//:
        secm.setDeflationind(OpicsConstant.SECM.DEFLATIONIND);//:
        secm.setCpidaycycle(OpicsConstant.SECM.CPIDAYCYCLE);//:
        secm.setSchedid(BigDecimal.ZERO);//:
        secm.setProdid(BigDecimal.ZERO);//:
        secm.setStructid(OpicsConstant.SECM.STRUCTID);//:
        secm.setFormulaid(BigDecimal.ZERO);//:
        secm.setDtcind(OpicsConstant.SECM.DTCIND);//:
        secm.setFedind(OpicsConstant.SECM.FEDIND);//:
        secm.setEuroclearind(OpicsConstant.SECM.EUROCLEARIND);//:
        secm.setClearstreamind(OpicsConstant.SECM.CLEARSTREAMIND);//:
        secm.setRepotrackingind(OpicsConstant.SECM.REPOTRACKINGIND);//:
        secm.setEndofmonthind(OpicsConstant.SECM.ENDOFMONTHIND);//:
        secm.setAllownegativeint(OpicsConstant.SECM.ALLOWNEGATIVEINT);//:
        return secm;
    }

    public Secl DataTrans(Secl secl) {
        secl.setBr(OpicsConstant.SECM.BRANCH);//机构:
        secl.setAskprice8(new BigDecimal("100"));//买入价:默认100
        secl.setAskpriceyst8(new BigDecimal("0.0000"));//昨日买入价:默认100
        secl.setBidprice8(new BigDecimal("100"));//卖出价:默认100
        secl.setBidpriceyst8(new BigDecimal("0.0000"));//昨日卖出价:默认100
        secl.setClsgprice8(new BigDecimal("100"));//收盘价:默认100
        secl.setClsgpriceyst8(new BigDecimal("0.0000"));//昨日收盘价:默认100
        secl.setCoupremainamt(BigDecimal.ZERO);//:
        secl.setCurrfixrate8(BigDecimal.ZERO);//:
        secl.setFixcycle(OpicsConstant.SECM.FIXCYCLE);//:
        secl.setFixfreq(OpicsConstant.SECM.FIXFREQ);//:
        secl.setFixday(OpicsConstant.SECM.FIXDAY);//:
        secl.setFixbaserate8(BigDecimal.ZERO);//:
        secl.setFixspotrate8(BigDecimal.ZERO);//:
        secl.setFixspotdays(OpicsConstant.SECM.FIXSPOTDAYS);//:
        secl.setMarkmakind(OpicsConstant.SECM.MARKMAKIND);//:
        secl.setNextfixrate8(BigDecimal.ZERO);//:
        secl.setOffclsgprice8(BigDecimal.ZERO);//:
        secl.setOffclsgpriceyst8(BigDecimal.ZERO);//:
        secl.setOffclsgrate8(BigDecimal.ZERO);//:
        secl.setOffclsgrateyst8(BigDecimal.ZERO);//:
        secl.setPrevcouprate8(BigDecimal.ZERO);//:
        secl.setPaycno(OpicsConstant.SECM.PAYCNO);//:
        secl.setSecsacct(OpicsConstant.SECM.SECSACCT);//托管账户:
        secl.setTaxind(OpicsConstant.SECM.TAXIND);//:
        secl.setLstmntdate(Calendar.getInstance().getTime());//:
        secl.setTdyaccint8(BigDecimal.ZERO);//:
        secl.setYstaccint8(BigDecimal.ZERO);//:

        return secl;
    }

    /**
     * 债券信息数据
     *
     * @param secm
     * @param secmWksBean
     * @return
     */
    public Secm DataTrans(Secm secm, SecmWksBean secmWksBean, CustService custService) {
        //交易要素
        secm.setSecid(secmWksBean.getICode());//债券编号:机构号; 总行=01
        secm.setDescr(secmWksBean.getBName());//债券描述:
        secm.setIssdate(DateUtil.parse(secmWksBean.getBIssueDate()));//发行日期:发行日期大于等于起息日
        secm.setIssuer(secmWksBean.getBIssuer());//发行人:CUST表里面取
        secm.setCouprate8(secmWksBean.getBCouponRate());//债券票面利率:票面利率
        String ccy = secmWksBean.getCny();
        secm.setCcy(ccy);//币种代码:
        secm.setBasis(getBasisTrans(secmWksBean.getBDaycount()));//计息基数代码:BOND/EBOND/A360/A365/ACTUAL
        secm.setVdate(DateUtil.parse(secmWksBean.getBValueDate()));//生息日:
        secm.setMdate(DateUtil.parse(secmWksBean.getBMaturityDate()));//到期日:
        secm.setFirstipaydate(DateUtil.parse(secmWksBean.getFirstipaydate()));//首次付息日:标黄四个日期灵活组合:起息日/首次付息日/最后一次付息日/到期日
        secm.setLastipaydate(DateUtil.parse(secmWksBean.getLastipaydate()));//最后一次付息日期:
        secm.setIntpaycycle(secmWksBean.getIntpaycycle());//付息周期代码:A按年/S半年/Q季度/M月
        secm.setPmtfreq(secmWksBean.getIntpaycycle());//付息频率:A按年/S半年/Q季度/M月
        secm.setIntenddaterule(secmWksBean.getIntenddaterule());//利息结束日期规则:D默认/S向前/M向后
        secm.setIntpayrule(secmWksBean.getIntpayrule());//付息日期规则:D默认/S向前/M向后
        secm.setDenom(secmWksBean.getDenom());//计量单位:一手的量DENOM
        //管理要素
        secm.setIntcalcrule(secmWksBean.getIntcalcrule());//利息计算规则:NPV按期付息/IAM到期换本/DIS贴现
        secm.setProduct(secmWksBean.getProduct());//产品代码:
        secm.setProdtype(secmWksBean.getProdtype());//产品类型代码:根据实际业务模式选择SD/FI/CD/MBS
        Cust cust = custService.selectByPrimaryKey(secmWksBean.getBIssuer());
        
        //会计类型 跟着信息传进来
        //secm.setAcctngtype(cust.getAcctngtype());//发行人会计类型:根据债券属性确定ACTY值
        secm.setAcctngtype(secmWksBean.getAcctngtype());//发行人会计类型:根据债券属性确定ACTY值
        secm.setSecunit(secmWksBean.getSecunit());//单位:FMT/BON
        secm.setSecmsic("");//工业标准代码:行业标准码:SICO里面取
        secm.setDcpriceind("C");//全价/净价标志:C净价
        secm.setRaterndrule("U");//利率取整规则:四舍五入
        secm.setParamt(new BigDecimal("100"));//面值:默认都是100
        secm.setRedempamt(new BigDecimal("100"));//赎回价值:到期都是100

        secm.setSettccy(ccy);//结算币种:
        secm.setIntccy(ccy);//利息币种:

        secm.setSettdays(secmWksBean.getSettdays());//债券交易日与起息日之间标准天数:
        return secm;
    }

    public Secl DataTrans(Secl secl, Secm secm) {
        secl.setSecid(secm.getSecid());//债券号:
        secl.setAskrate8(secm.getCouprate8());//买入汇率:票面利率
        secl.setAskrateyst8(new BigDecimal("0.0000"));//昨日买入汇率:票面利率
        secl.setBidrate8(secm.getCouprate8());//卖出汇率:票面利率
        secl.setBidrateyst8(new BigDecimal("0.0000"));//昨日卖出汇率:票面利率
        secl.setClsgrate8(secm.getCouprate8());//票面利率:票面利率
        secl.setClsgrateyst8(new BigDecimal("0.0000"));//票面利率:票面利率
        return secl;
    }

    public List<Secs> DataTrans(List<Secs> secsLst, Secm secm, SecmWksBean secmWksBean) {
        List<SecsWksBean> secsSchdList = secmWksBean.getSecsSchdList();

        Collections.sort(secsSchdList, new Comparator<SecsWksBean>() {
            @Override
            public int compare(SecsWksBean o1, SecsWksBean o2) {
                return o1.getPaymentdate().compareTo(o2.getPaymentdate());
            }
        });
        for (int i = 0; i < secsSchdList.size(); i++) {
            Secs secs = DataTrans(new Secs());
            secs = DataTrans(secs, secm, String.valueOf(i + 1), secsSchdList.get(i));
            secsLst.add(secs);
        }
        return secsLst;
    }

    public Secs DataTrans(Secs secs) {
        secs.setCallprice8(BigDecimal.ZERO);//买回价格:
        secs.setCaprate8(BigDecimal.ZERO);//利率上限:
        secs.setFloorrate8(BigDecimal.ZERO);//利率下限:
        secs.setSpreadrate8(BigDecimal.ZERO);//利率点差:
        secs.setLstmntdate(Calendar.getInstance().getTime());//上次更新日期:
        secs.setPutprice8(BigDecimal.ZERO);//卖价:
        secs.setSteprate8(BigDecimal.ZERO);//利率增幅:
        secs.setUpdatecounter(OpicsConstant.SECM.UPDATECOUNTER);//更新次数:
        secs.setGroupid(BigDecimal.ZERO);//:
        secs.setFormulaid(BigDecimal.ZERO);//:
        secs.setFixingoverride(BigDecimal.ZERO);//:
        secs.setRaterefixind(OpicsConstant.SECM.RATEREFIXIND);//:
        return secs;
    }

    public Secs DataTrans(Secs secs, Secm secm, String seq, SecsWksBean secsWksBean) {
        secs.setSecid(secsWksBean.getICode());//债券编号:
        secs.setSeq(seq);//序号:序号，依次递增
        secs.setIpaydate(DateUtil.parse(secsWksBean.getPaymentdate()));//付息日:
        secs.setCashflow8(secsWksBean.getPaymentsum());//现金流金额:收付息期间=每手的利息收入，到期=所有现金流+本金
        secs.setIntrate8(secsWksBean.getCouponrate());//利率:
        secs.setExdivdate(DateUtil.parse(secsWksBean.getPaymentdate()));//除息日:
        secs.setIntpayamt8(secsWksBean.getPaymentinterest());//付息金额:37.5
        secs.setPrinpayamt8(secsWksBean.getPaymentparvalue().negate());//本金付款金额:
        secs.setIntenddte(DateUtil.parse(secsWksBean.getEnddate()));//利息结束日期:
        secs.setIntstrtdte(DateUtil.parse(secsWksBean.getCarrydate()));//利息起始日期:
        secs.setRatefixdate(DateUtil.parse(secsWksBean.getCarrydate()));//利率重置日期:
        secs.setPrinamt8(new BigDecimal(secm.getDenom()));//本金金额:
        secs.setBasis(secm.getBasis());//计息基数代码:
        secs.setNotprinamt8(new BigDecimal(secm.getDenom()));//名义本金金额:
        secs.setNotprinpayamt8(secsWksBean.getPaymentparvalue().negate());//名义本金付款金额:
        secs.setNotintpayamt8(secsWksBean.getPaymentinterest());//名义利息付款金额:
        return secs;
    }

    /**
     * 转换计息基础
     *
     * @param str
     * @return
     */
    private String getBasisTrans(String str) {
        if (StringUtils.equals(str, "Actual/360")) {
            return "A360";
        } else if (StringUtils.equals(str, "Actual/365")) {
            return "A365";
        } else if (StringUtils.equals(str, "Actual/Actual")) {
            return "ACTUAL";
        }
        return "";
    }
}
