package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.OpicsConstant.PCIX;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.SwiftMethod;
import com.singlee.financial.wks.mapper.opics.PmtqMapper;
import com.singlee.financial.wks.service.opics.*;
import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class PmtqServiceImpl implements PmtqService {

    @Resource
    private PmtqMapper pmtqMapper;
    @Resource
    BatchDao batchDao;
    @Autowired
    private CsriService csriService;
    @Autowired
    private CspiService cspiService;
    @Autowired
    private SaccService saccService;
    @Autowired
    private SetaService setaService;
    @Autowired
    private CustService custService;

    private Logger logger = Logger.getLogger(PmtqServiceImpl.class);

    @Override
    public int deleteByPrimaryKey(String br, String type, String dealno, String seq, String product, Date cdate,
                                  String ctime, String payrecind) {
        return pmtqMapper.deleteByPrimaryKey(br, type, dealno, seq, product, cdate, ctime, payrecind);
    }

    @Override
    public int deletePmtqByVdate(String br, String type, String dealno, String product, Date vdate) {
        return pmtqMapper.deletePmtqByVdate(br, type, dealno, product, vdate);
    }

    @Override
    public int deleteReverseByPrimaryKey(String br, String type, String dealno, String product, Date vdate) {
        return pmtqMapper.deleteReverseByPrimaryKey(br, type, dealno, product, vdate);
    }

    @Override
    public int insert(Pmtq record) {
        return pmtqMapper.insert(record);
    }

    @Override
    public int insertSelective(Pmtq record) {
        return pmtqMapper.insertSelective(record);
    }

    @Override
    public Pmtq selectByPrimaryKey(String br, String type, String dealno, String seq, String product, Date cdate,
                                   String ctime, String payrecind) {
        return pmtqMapper.selectByPrimaryKey(br, type, dealno, seq, product, cdate, ctime, payrecind);
    }

    @Override
    public List<Pmtq> selectReverseByPrimaryKey(String br, String type, String dealno, String product) {
        return pmtqMapper.selectReverseByPrimaryKey(br, type, dealno, product);
    }

    @Override
    public int updateByPrimaryKeySelective(Pmtq record) {
        return pmtqMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Pmtq record) {
        return pmtqMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateReverseByPrimaryKey(String br, String product, String type, String dealno, String seq, Date postdate) {
        return pmtqMapper.updateReverseByPrimaryKey(br, product, type, dealno, seq, postdate);
    }

    @Override
    public void updateBatch(List<Pmtq> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.PmtqMapper.updateByPrimaryKeySelective", list);
    }

    @Override
    public void batchInsert(@Param("list") List<Pmtq> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.PmtqMapper.insert", list);
    }

    /**
     * 添加默认清算路径
     *
     * @param list
     */
    @Override
    public void addDefSi(@Param("list") List<Pmtq> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        for (Pmtq pmtq : list) {
            if (StringUtils.equalsIgnoreCase(pmtq.getPayrecind(), PCIX.PMTQ_PAYIND)) {// 付款信息
                cspiService.deleteByPrimaryKey(pmtq.getBr(), pmtq.getCno(), pmtq.getProduct(), pmtq.getType(),
                        pmtq.getCcy());
                Cspi cspi = new Cspi();
                cspi.setBr(pmtq.getBr());
                cspi.setCno(pmtq.getCno());
                cspi.setProduct(pmtq.getProduct());
                cspi.setType(pmtq.getType());
                cspi.setCcy(pmtq.getCcy());
                cspi.setSmeans(pmtq.getSetmeans());
                cspi.setSacct(pmtq.getSetacct());
                cspi.setLstmntdate(Calendar.getInstance().getTime());
                cspiService.insert(cspi);
            } else {// 收款信息
                csriService.deleteByPrimaryKey(pmtq.getBr(), pmtq.getCno(), pmtq.getProduct(), pmtq.getType(),
                        pmtq.getCcy());
                Csri csri = new Csri();
                csri.setBr(pmtq.getBr());
                csri.setCust(pmtq.getCno());
                csri.setProduct(pmtq.getProduct());
                csri.setType(pmtq.getType());
                csri.setCcy(pmtq.getCcy());
                csri.setSmeans(pmtq.getSetmeans());
                csri.setSacct(pmtq.getSetacct());
                csri.setLstmntdte(Calendar.getInstance().getTime());
                csriService.insert(csri);
            }
        }
    }

    /**
     * 方法已重载.设置支付指今对象(PMTQ)
     *
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param product
     * @param cdate
     * @param ctime
     * @param payrecind
     * @param vdate
     * @param paytype
     * @param status
     * @param ccy
     * @param amount
     * @param cno
     * @param msgprty
     * @param swiftfmt
     * @param reldte
     * @param reltime
     * @param lstmntdte
     * @param setmeans
     * @param setacct
     * @param comrefno
     * @return
     */
    private Pmtq builder(String br, String type, String dealno, String seq, String product, Date cdate, String ctime,
                         String payrecind, Date vdate, String paytype, String status, String ccy, BigDecimal amount, String cno,
                         String msgprty, String swiftfmt, Date reldte, String reltime, Date lstmntdte, String setmeans, String setacct,
                         String comrefno) {
        Pmtq pmtq = new Pmtq();
        // 部门
        pmtq.setBr(br);
        // 业务类型
        pmtq.setType(type);
        // 交易编号
        pmtq.setDealno(dealno);
        // 交易序号
        pmtq.setSeq(seq);
        // 产品代码
        pmtq.setProduct(product);
        // 创建日期
        pmtq.setCdate(cdate);
        pmtq.setCtime(ctime);
        // 收付款标识
        pmtq.setPayrecind(payrecind);
        // 付款日期
        pmtq.setVdate(vdate);
        // 付款类型
        pmtq.setPaytype(paytype);
        // 处理状态
        pmtq.setStatus(status);
        // 货币
        pmtq.setCcy(ccy);
        // 金额
        pmtq.setAmount(amount);
        // 交易对手编号
        pmtq.setCno(cno);
        // 报文属性
        pmtq.setMsgprty(msgprty);
        // 揶文类型
        pmtq.setSwiftfmt(swiftfmt);
        // 真实日期
        pmtq.setReldte(reldte);
        // 真实时间
        pmtq.setReltime(reltime);
        // 最后更新时间
        pmtq.setLstmntdte(lstmntdte);
        // 清算方式
        pmtq.setSetmeans(setmeans);
        // 清算路径
        pmtq.setSetacct(setacct);
        // 外部流水号
        pmtq.setComrefno(comrefno);

        return pmtq;
    }

    /**
     * 方法已重载.设置支付指今对象(PMTQ),填充默认值. ctime payrecind paytype status msgprty swiftfmt reldte reltime comrefno
     *
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param product
     * @param cdate
     * @param vdate
     * @param ccy
     * @param amount
     * @param cno
     * @param setmeans
     * @param setacct
     * @return
     */
    private Pmtq builder(String br, String type, String dealno, String seq, String product, Date cdate, String ctime, Date vdate,
                         String ccy, BigDecimal amount, String cno, String setmeans, String setacct, String comrefnoSeq, String ctrSetacct) {
        String comrefno = "";
        // 当前系统时间
        //String time = DateUtil.getCurrentTimeAsString();

        // 收付标识
        String payrecind = amount.doubleValue() > 0 ? OpicsConstant.PCIX.PMTQ_RECIND : OpicsConstant.PCIX.PMTQ_PAYIND;

        Cust cust = custService.selectByPrimaryKey(cno);
        Cust MyCust = custService.selectByBr(br);

        if (null == cust || null == MyCust) {
            throw new RuntimeException("构建PMTQ信息时交易对手信息不能为空");
        }

        logger.info("交易对手[" + cno + "]-[SWIFTCODE=" + cust.getBic() + "]");
        logger.info("本方[" + MyCust.getCno() + "]-[SWIFTCODE=" + MyCust.getBic() + "]");

        //生成comrefno流水号
        comrefno = createComrefno(br, cust, MyCust, setacct, comrefnoSeq, ctrSetacct);

        logger.debug("21域引用PMTQ流水号comrefno=" + comrefno);

        return builder(br, type, dealno, seq, product, cdate, ctime, payrecind, vdate, "",
                OpicsConstant.PCIX.PMTQ_STATUS, ccy, amount, cno, "",
                builderMsgType(payrecind, product, type, ccy, setmeans, cust.getCtype()), null, "", cdate, setmeans, setacct,
                comrefno);
    }

    /**
     * 根据清算机构与交易对手信息生成Comrefno
     *
     * @param br
     * @param ccy
     * @param cust
     * @param MyCust
     * @param setacct
     * @param comrefnoSeq
     * @return
     */
    private String createComrefno(String br, Cust cust, Cust MyCust, String setacct, String comrefnoSeq, String ctrSetacct) {
        String comrefno = "";
        Seta seta = null;
        Seta ctrSeta = null;
        String custBic = null;
        String myBic = null;
        Cust custSeta = null;
        String setaBic = null;
        String ctrSetaBic = null;

        if (StringUtils.isEmpty(StringUtils.trim(cust.getBic())) || StringUtils.isEmpty(StringUtils.trim(MyCust.getBic()))) {
            logger.warn("交易对手或者自己的BIC为空,系统无法生成Comrefno");
            return comrefno;
        }

        //查询自己的清算机构
        seta = setaService.selectBySettacct(br, StringUtils.trim(setacct));
        custSeta = custService.selectByPrimaryKey(StringUtils.trim(null == seta ? "" : seta.getCno()));
        setaBic = custSeta == null ? "" : custSeta.getBic();
        logger.info(String.format("PMTQ.CreateComrefno,交易货币清算信息:%s,清算路径:%s,清算机构CNO%s,清算机构BIC:%s", seta, setacct, cust.getCno(), setaBic));
        ctrSeta = setaService.selectBySettacct(br, StringUtils.trim(ctrSetacct));
        custSeta = custService.selectByPrimaryKey(StringUtils.trim(null == ctrSeta ? "" : ctrSeta.getCno()));
        ctrSetaBic = custSeta == null ? "" : custSeta.getBic();
        logger.info(String.format("PMTQ.CreateComrefno,对应货币清算信息:%s,清算路径:%s,清算机构CNO：%s,清算机构BIC:%s", ctrSeta, ctrSetacct, cust.getCno(), ctrSetaBic));

        if (null == cust.getBic()) {
            return comrefno;
        }

        custBic = StringUtils.trim(cust.getBic().replaceAll("X", ""));
        myBic = StringUtils.trim(MyCust.getBic().replaceAll("X", ""));

        if (!org.springframework.util.StringUtils.hasText(custBic) || !org.springframework.util.StringUtils.hasText(myBic)) {
            return comrefno;
        }

        if (StringUtils.trim(cust.getCno()).equals(StringUtils.trim(seta.getCno())) || (null != ctrSeta && StringUtils.trim(cust.getCno()).equals(StringUtils.trim(ctrSeta.getCno())))
                || custBic.substring(0, 4).equalsIgnoreCase(setaBic.substring(0, 4)) || custBic.substring(0, 4).equalsIgnoreCase(ctrSetaBic.substring(0, 4))) {
            comrefno = String.format("%s%s%s%s%s", custBic.substring(0, 4), custBic.substring(custBic.length() - 2),
                    comrefnoSeq, myBic.substring(0, 4), myBic.substring(myBic.length() - 2));
        } else {
            comrefno = String.format("%s%s%s%s%s", myBic.substring(0, 4), myBic.substring(myBic.length() - 2),
                    comrefnoSeq, custBic.substring(0, 4), custBic.substring(custBic.length() - 2));
        }

        return comrefno;
    }

    /**
     * 方法已重载.设置支付指今对象(PMTQ),填充默认值. ctime payrecind paytype status msgprty swiftfmt reldte reltime comrefnoSeq
     * 根据ctrSetacct、setacct字段创建comrefno
     *
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param product
     * @param cdate
     * @param ctime
     * @param vdate
     * @param ccy
     * @param amount
     * @param cno
     * @param setmeans
     * @param setacct
     * @return
     */
    private Pmtq builder(String br, String type, String dealno, String seq, String product, Date cdate, String ctime, Date vdate,
                         String ccy, BigDecimal amount, String cno, String setmeans, String setacct) {
        return builder(br, type, dealno, seq, product, cdate, ctime, vdate, ccy, amount, cno, setmeans, setacct, FastDateFormat.getInstance("mmss").format(new Date()), null);
    }

    /**
     * 方法已重载.设置支付指今对象(PMTQ),填充默认值. ctime payrecind paytype status msgprty swiftfmt reldte reltime,
     * 字段comrefno由参数传入
     *
     * @param br
     * @param type
     * @param dealno
     * @param seq
     * @param product
     * @param cdate
     * @param ctime
     * @param vdate
     * @param ccy
     * @param amount
     * @param cno
     * @param setmeans
     * @param setacct
     * @param comrefno
     * @return
     */
    private Pmtq builder(String br, String type, String dealno, String seq, String product, Date cdate, String ctime, Date vdate,
                         String ccy, BigDecimal amount, String cno, String setmeans, String setacct, String comrefno) {
        return builder(br, type, dealno, seq, product, cdate, ctime, vdate, ccy, amount, cno, setmeans, setacct, comrefno, null);
    }

    /**
     * 现券买卖收付标识区分重载
     *
     * @return xuqq
     */
    private Pmtq builder(String br, String type, String dealno, String seq, String product, Date cdate, Date vdate,
                         String ccy, BigDecimal amount, String cno, String setmeans, String setacct, String comrefno, String payrecind, String swftfmt) {
        // 当前系统时间
        String time = DateUtil.getCurrentTimeAsString();
        // 当前系统日期
        Date nowDate = new Date();
        // 收付标识
        return builder(br, type, dealno, seq, product, cdate, time, payrecind, vdate, "",
                OpicsConstant.PCIX.PMTQ_STATUS, ccy, amount, cno, "",
                swftfmt, nowDate, time, nowDate, setmeans, setacct,
                comrefno);
    }

    /**
     * 根根收付标识及业务类型生成报文类型
     *
     * @param prodcut
     * @param prodType
     * @return
     */
    private String builderMsgType(String payrecind, String prodcut, String prodType, String ccy, String setmeans, String custType) {
        String msgType = null;

        switch (StringUtils.trim(prodcut)) {
            case "FXD":
            case "OTC":
            case "MM":
                if ("CNY|RMB|CNH".contains(ccy.toUpperCase())) {
                    //人民币在PMTQ只有202D类型，没有210D
                    //msgType = "P".equalsIgnoreCase(payrecind) ? "202D" : "210D";
                    msgType = "202D";
                } else {
                    //外币在PMTQ不管收款还是付款都是202，是否出报文系统会根据msgType+payrecind+status
                    //msgType = "P".equalsIgnoreCase(payrecind) ? "202" : "210";
                    msgType = "202";
                }
                break;
            case "SWAP":
                if ("CNY|RMB|CNH".contains(ccy.toUpperCase())) {
                    msgType = "F".equalsIgnoreCase(custType) ? "100D" : "202D";
                } else {
                    //msgType = "P".equalsIgnoreCase(payrecind) ? "202" : "210";
                    msgType = "202";
                }
                break;
            case "FI":
            case "SECUR":
                // 资金、债券在同一机构托管
                msgType = "P".equalsIgnoreCase(payrecind) ? "541" : "543";
                // 资金、债券在不同机构托管,540+202,542+210
                break;
            default:
                throw new RuntimeException("PMTQ新增:产品类型不支持！");
        }

        return msgType;
    }

    @Override
    public List<Pmtq> builder(Dldt dl, List<Schd> schds) {
        List<Pmtq> pmtqs = new ArrayList<Pmtq>();
        /*
         * 根据现金流生成PMTQ数据
         */
        for (int i = 0; i < schds.size(); i++) {
            pmtqs.add(builder(dl.getBr(), dl.getProdtype(), dl.getDealno(), dl.getSeq(), dl.getProduct(),
                    dl.getBrprcindte(), DateUtil.getTimeAsStringByCount(i), schds.get(i).getIpaydate(), dl.getCcy(),
                    schds.get(i).getTotpayamt(), dl.getCno(), schds.get(i).getSmeans(), schds.get(i).getSacct()));
        }
        return pmtqs;
    }

    @Override
    public List<Pmtq> builder(Fxdh fx, SwiftMethod swiftMethod) {
        List<Pmtq> pmtqs = new ArrayList<Pmtq>();
        String comrefnoSeq = null;
        int dealNoLength = 0;

        /*
         * 禁用收付款报文时将不生成PMTQ记录
         */
        // 当报文处理方法不是启用或者收付款报文启用状态,PCIX表不处理
        if (!SwiftMethod.ENABLE.equals(swiftMethod) && !SwiftMethod.PAYANDREC.equals(swiftMethod)) {
            logger.info("外汇交易[" + fx.getDealno() + "]禁用收付款信息,将不插入PMTQ记录");
            return pmtqs;
        }
        dealNoLength = StringUtils.trim(fx.getDealno()).length();
        comrefnoSeq = StringUtils.trim(fx.getDealno()).substring(dealNoLength - 4, dealNoLength);
        // 添加货币1收付款信息
        pmtqs.add(builder(fx.getBr(), fx.getProdtype(), fx.getDealno(), fx.getSeq(), fx.getProdcode(), fx.getBrprcindte(), DateUtil.getTimeAsStringByCount(1), fx.getVdate(), fx.getCcy(),
                fx.getCcyamt(), fx.getCust(), fx.getCcysmeans(), fx.getCcysacct(), comrefnoSeq, fx.getCtrsacct()));
        // 添加货币2收付款信息
        pmtqs.add(builder(fx.getBr(), fx.getProdtype(), fx.getDealno(), fx.getSeq(), fx.getProdcode(), fx.getBrprcindte(), DateUtil.getTimeAsStringByCount(0), fx.getVdate(), fx.getCtrccy(),
                fx.getCtramt(), fx.getCust(), fx.getCtrsmeans(), fx.getCtrsacct(), comrefnoSeq, fx.getCcysacct()));
        return pmtqs;
    }

    @Override
    public Pmtq builder(Otdt otc) {
        return builder(otc.getBr(), otc.getProdtype(), otc.getDealno(), otc.getSeq(), otc.getProduct(),
                otc.getBrprcindte(), DateUtil.getTimeAsStringByCount(0), otc.getDealvdate(), otc.getCcy(),
                otc.getCcyprin(), otc.getCno(), otc.getPremsmeans(), otc.getPremsacct());
    }

    @Override
    public List<Pmtq> builder(Swdh swdh, List<Swds> swdts, Prir prir, Psix psix) {
        List<Pmtq> pmtqs = new ArrayList<Pmtq>();
        if (swdts == null || swdts.size() == 0) {
            return pmtqs;
        }

        logger.info("利率互换交易[" + swdh.getDealno() + "],psix.getSuppayind():" + psix.getSuppayind() + ",prir.getSuprecind():" + prir.getSuprecind());

        /*
         * 禁用收付款报文时将不生成PMTQ记录
         */
        if ("Y".equalsIgnoreCase(prir.getSuprecind()) && "Y".equalsIgnoreCase(psix.getSuppayind())) {
            logger.info("利率互换交易[" + swdh.getDealno() + "]禁用收付款报文,将不插入PMTQ记录");
            return pmtqs;
        }

        // 添加收付款信息
        for (int i = 0; i < swdts.size(); i++) {
            if (swdts.get(i).getNetipay().compareTo(BigDecimal.ZERO) != 0) {
                pmtqs.add(builder(swdh.getBr(), swdh.getProdtype(), swdh.getDealno(), swdts.get(i).getSeq(), swdh.getProduct(),
                                swdh.getBrprcindte(), DateUtil.getTimeAsStringByCount(i), swdts.get(i).getIntenddte(), swdts.get(i).getIntccy(),
                                swdts.get(i).getNetipay(), swdh.getCno(), swdts.get(i).getIntsmeans(), swdts.get(i).getIntsacct(),
                                String.format("%d%03d", Integer.valueOf(StringUtils.trim(swdts.get(i).getSeq())), Integer.valueOf(StringUtils.trim(swdts.get(i).getSchdseq())))
                        )
                );
            }//end if
        }

        return pmtqs;
    }

    @Override
    public Pmtq builder(Swdh swdh,Swds swds) {
        Pmtq pmtq = new Pmtq();
        pmtq.setBr(swdh.getBr());
        pmtq.setProduct(swdh.getProduct());
        pmtq.setDealno(swdh.getDealno());
        pmtq.setSeq("001");
        pmtq.setCdate(swdh.getDealdate());
        pmtq.setCtime(swdh.getDealtime());
        pmtq.setVdate(swdh.getVerdate());
        pmtq.setStatus("AR");
        pmtq.setCcy(swdh.getEtccy());
        pmtq.setAmount(swdh.getEtamt().add(swds.getNetipay()));//罚息金额+利息金额
        pmtq.setCno(swdh.getCno());
        pmtq.setSwiftfmt("100D");
        pmtq.setLstmntdte(swdh.getDealdate());
        pmtq.setSetmeans(swdh.getEtsmeans());
        pmtq.setSetacct(swdh.getEtsacct());
        Cust cust = custService.selectByPrimaryKey(swdh.getCno());
        pmtq.setComrefno(cust.getBic());
        return pmtq;
    }

    @Override
    public List<Pmtq> builder(Spsh spsh) {
        List<Pmtq> pmtqs = new ArrayList<Pmtq>();
        if (spsh == null) {
            return pmtqs;
        }

        //处理债券pmtq数据
        boolean flag = false;
        Sacc sacc = saccService.selectByPrimaryKeyAndCno(spsh.getBr(), spsh.getSecsacct(), spsh.getCno());
        Seta seta = setaService.selectByPrimaryKeyAndCno(spsh.getBr(), spsh.getCcysmeans(), spsh.getCcysacct(), spsh.getCno());
        if (sacc != null && seta != null) {
            flag = true;
        }
        List<Map<String, String>> mapList = builderMsgType(flag, spsh.getPs(), spsh.getCcy(), spsh.getSettamt().toString(),
                spsh.getCcysmeans(), spsh.getCcysacct(), spsh.getSecsacct());
        // 添加收付款信息
        for (int i = 0; i < mapList.size(); i++) {
            Map<String, String> map = mapList.get(i);
            pmtqs.add(builder(spsh.getBr(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(), spsh.getProduct(),
                    spsh.getBrprcindte(), spsh.getSettdate(), spsh.getCcy(), new BigDecimal(map.get("AMOUNT")), spsh.getCno(),
                    map.get("SETMEANS"), map.get("SETACCT"), spsh.getDealno(), map.get("PAYRECIND"), map.get("SWIFTFMT")));
        }
        return pmtqs;
    }

    @Override
    public List<Pmtq> builder(Rprh rprh, List<Rpdt> rpdt) {
        List<Pmtq> pmtqs = new ArrayList<Pmtq>();
        if (rprh == null) {
            return pmtqs;
        }
        if ("P".equalsIgnoreCase(rprh.getPs())) {
            //逆回购:先付钱收券，再收钱付券
            pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
                    rprh.getBrprcindte(), rprh.getVdate(), rprh.getCcy(), rprh.getComprocdamt(), rprh.getCno(),
                    rprh.getComccysmeans(), rprh.getComccysacct(), rprh.getDealno(), "P",
                    "CNY".equalsIgnoreCase(rprh.getCcy()) ? "202D" : "202"));

            pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
                    rprh.getBrprcindte(), rprh.getMatdate(), rprh.getCcy(), rprh.getMatprocdamt(), rprh.getCno(),
                    rprh.getComccysmeans(), rprh.getComccysacct(), rprh.getDealno(), "R",
                    "CNY".equalsIgnoreCase(rprh.getCcy()) ? "202D" : "210"));

            for (int i = 0; i < rpdt.size(); i++) {
                pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), String.format("%03d", i + 1), rprh.getProduct(),
                        rprh.getBrprcindte(), rprh.getVdate(), rprh.getCcy(), BigDecimal.ZERO, rprh.getCno(),
                        "", rprh.getSafekeepacct(), rprh.getDealno(), "R", "520"));

                pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), String.format("%03d", i + 1), rprh.getProduct(),
                        rprh.getBrprcindte(), rprh.getMatdate(), rprh.getCcy(), BigDecimal.ZERO, rprh.getCno(),
                        "", rprh.getSafekeepacct(), rprh.getDealno(), "D", "522"));
            }
        } else {

            //正回购:先收钱付券，再付钱收券
            pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
                    rprh.getBrprcindte(), rprh.getVdate(), rprh.getCcy(), rprh.getComprocdamt(), rprh.getCno(),
                    rprh.getComccysmeans(), rprh.getComccysacct(), rprh.getDealno(), "R",
                    "CNY".equalsIgnoreCase(rprh.getCcy()) ? "202D" : "210"));

            pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
                    rprh.getBrprcindte(), rprh.getMatdate(), rprh.getCcy(), rprh.getMatprocdamt(), rprh.getCno(),
                    rprh.getComccysmeans(), rprh.getComccysacct(), rprh.getDealno(), "P",
                    "CNY".equalsIgnoreCase(rprh.getCcy()) ? "202D" : "202"));

            for (int i = 0; i < rpdt.size(); i++) {
                pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), String.format("%03d", i + 1), rprh.getProduct(),
                        rprh.getBrprcindte(), rprh.getVdate(), rprh.getCcy(), BigDecimal.ZERO, rprh.getCno(),
                        "", rprh.getSafekeepacct(), rprh.getDealno(), "D", "522"));

                pmtqs.add(builder(rprh.getBr(), rprh.getProdtype(), rprh.getDealno(), String.format("%03d", i + 1), rprh.getProduct(),
                        rprh.getBrprcindte(), rprh.getMatdate(), rprh.getCcy(), BigDecimal.ZERO, rprh.getCno(),
                        "", rprh.getSafekeepacct(), rprh.getDealno(), "R", "520"));
            }

        }

        return pmtqs;
    }

    /**
     * 处理债券pmtq信息
     *
     * @param flag:是否托管行
     * @param ps:买卖方向
     * @param ccy：币种
     * @param amount：金额
     * @param smeans：资金结算方式：如DVP
     * @param sacct:资金结算账号:如DVP-P
     * @param secsacct:托管帐户:如CDTC
     * @return by xuqq 20200302
     */
    private List<Map<String, String>> builderMsgType(boolean flag, String ps, String ccy, String amount, String smeans, String sacct, String secsacct) {
		/* 现券
    	MT540	券款不在同一个托管行：收券
    	MT202	券款不在同一个托管行：付钱
    	MT541	券款同一个托管行：收券付钱
    	MT542	券款不在同一个托管行：付券
    	MT210	券款不在同一个托管行：收钱
    	MT543	券款同一个托管行：付券收钱
    	对应pmtq表：MT540<==>520,MT541<==>521,MT542<==>522,MT543<==>523
    				MT202:外币：202,本币202D
    				MT210:外币：210,本币202D
    	
    	同一个托管行：根据sacc（券）,seta（钱）表判断
    	PAYRECIND：付券：D,收钱：R
    				付钱：P，收券：R
    	清算：钱：dvp,dvp-p;券：cdtc;同一个托管行:是dvp,dvp-p
    	同一个托管行:一条数据，不同托管行两条数据
    	*/

        List<Map<String, String>> mapList = new ArrayList<Map<String, String>>();
        if (flag) {
            //同一托管行
            if ("P".equalsIgnoreCase(ps)) {
                //收券付钱
                Map<String, String> map = new HashMap<String, String>();
                map.put("PAYRECIND", "P");
                map.put("AMOUNT", amount);
                map.put("SETMEANS", smeans);
                map.put("SETACCT", sacct);
                map.put("SWIFTFMT", "521");
                mapList.add(map);
            } else {
                //付券收钱
                Map<String, String> map = new HashMap<String, String>();
                map.put("PAYRECIND", "R");
                map.put("AMOUNT", amount);
                map.put("SETMEANS", smeans);
                map.put("SETACCT", sacct);
                map.put("SWIFTFMT", "523");
                mapList.add(map);
            }
        } else {
            //不同托管行
            if ("P".equalsIgnoreCase(ps)) {
                //收券付钱
                Map<String, String> map = new HashMap<String, String>();
                map.put("PAYRECIND", "P");
                map.put("AMOUNT", amount);
                map.put("SETMEANS", smeans);
                map.put("SETACCT", sacct);
                if ("CNY".equalsIgnoreCase(ccy)) {
                    map.put("SWIFTFMT", "202D");
                } else {
                    map.put("SWIFTFMT", "202");
                }
                mapList.add(map);
                Map<String, String> map1 = new HashMap<String, String>();
                map1.put("PAYRECIND", "R");
                map1.put("AMOUNT", "0");
                map1.put("SETMEANS", "");
                map1.put("SETACCT", secsacct);
                map1.put("SWIFTFMT", "520");
                mapList.add(map1);
            } else {
                //付券收钱
                Map<String, String> map = new HashMap<String, String>();
                map.put("PAYRECIND", "D");
                map.put("AMOUNT", "0");
                map.put("SETMEANS", "");
                map.put("SETACCT", secsacct);
                map.put("SWIFTFMT", "522");

                mapList.add(map);
                Map<String, String> map1 = new HashMap<String, String>();
                map1.put("PAYRECIND", "R");
                map1.put("AMOUNT", amount);
                map1.put("SETMEANS", smeans);
                map1.put("SETACCT", sacct);
                if ("CNY".equalsIgnoreCase(ccy)) {
                    map1.put("SWIFTFMT", "202D");
                } else {
                    map1.put("SWIFTFMT", "210");
                }
                mapList.add(map1);
            }
        }
        return mapList;
    }

    @Override
    public List<Pmtq> builder(Rdfh rdfh) {
        List<Pmtq> pmtqs = new ArrayList<Pmtq>();
        if (rdfh == null) {
            return pmtqs;
        }
        String dr = rdfh.getDr();
        if (!"R".equals(dr)) {
            dr = "P";
        }
        //冲销报文
        String MTxx = "292";
        if ("CNY".equalsIgnoreCase(rdfh.getCcy())) {
            MTxx = "202D";
        } else {
            MTxx = "292";
        }
        pmtqs.add(builder(rdfh.getBr(), rdfh.getProdtype(), rdfh.getTransno(), rdfh.getSeq(), rdfh.getProduct(),
                rdfh.getBrprcindte(), rdfh.getSettdate(), rdfh.getCcy(), rdfh.getSettamt(), rdfh.getCno(),
                rdfh.getCcysmeans(), rdfh.getCcysacct(), rdfh.getTransno(), dr, MTxx));
        return pmtqs;
    }

    @Override
    public List<Pmtq> builder(Sldh sldh) {
        List<Pmtq> pmtqs = new ArrayList<Pmtq>();
        if (sldh == null) {
            return pmtqs;
        }

        //处理债券pmtq数据
        boolean flag = false;
        Sacc sacc = saccService.selectByPrimaryKeyAndCno(sldh.getBr(), sldh.getComccysacct(), sldh.getCno());
        Seta seta = setaService.selectByPrimaryKeyAndCno(sldh.getBr(), sldh.getComccysmeans(), sldh.getComccysacct(), sldh.getCno());
        if (sacc != null && seta != null) {
            flag = true;
        }
        List<Map<String, String>> mapList = builderMsgType(flag, sldh.getPs(), sldh.getCcy(), sldh.getComprocamt().toString(),
                sldh.getComccysmeans(), sldh.getComccysacct(), sldh.getComccysacct());
        // 添加收付款信息
        for (int i = 0; i < mapList.size(); i++) {
            Map<String, String> map = mapList.get(i);
            pmtqs.add(builder(sldh.getBr(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(), sldh.getProduct(),
                    sldh.getBrprcindte(), sldh.getVdate(), sldh.getCcy(), new BigDecimal(map.get("AMOUNT")), sldh.getCno(),
                    map.get("SETMEANS"), map.get("SETACCT"), sldh.getDealno(), map.get("PAYRECIND"), map.get("SWIFTFMT")));
        }
        return pmtqs;
    }

    @Override
    public void callSpUpdrecPmtqSwiftfmt(String br, String dealno, String seq, String product, String type,
                                         Date amenddate, String amendtime, String suppressccy, String suppresssec, String swiftfmtNotin,
                                         String statusIn, Date vdateIn, BigDecimal convrev) {
//		pmtqMapper.callSpUpdrecPmtqSwiftfmt(br, dealno, seq, product, type, amenddate, amendtime, suppressccy,
//				suppresssec, swiftfmtNotin, statusIn, vdateIn, convrev);
        //某些值写死
        pmtqMapper.callSpUpdrecPmtqSwiftfmt(br, dealno, seq, product, type, amenddate, amendtime, "0",
                "0", "192,192D,192I,292,292D,292I,592", "AR,H", null, BigDecimal.ZERO);
    }

    @Override
    public List<Pmtq> builder(Date revdate, List<Pmtq> list) {
        if (null == list || list.size() == 0) {
            logger.info("PMTQ待冲销的记录为空！");
            return list;
        }

        list.stream().forEach(pmtq -> {
            String swiftFmt = null;
            switch (StringUtils.trim(pmtq.getSwiftfmt())) {
                case "D202":
                    swiftFmt = "D292";
                    break;
                case "202":
                    swiftFmt = "292";
                    break;
                case "202D":
                    swiftFmt = "292D";
                    break;
                case "100D":
                    swiftFmt = "192D";
                    break;
                case "D100":
                    swiftFmt = "D192";
                    break;
                case "100":
                    swiftFmt = "192";
                    break;
                default:
                    throw new RuntimeException("PMTQ冲销:报文类型不支持！");
            }
            pmtq.setCdate(revdate);
            pmtq.setCtime(DateUtil.getTimeAsStringByCount(Integer.valueOf(StringUtils.trim(pmtq.getSeq()))));
            pmtq.setStatus("AR");
            pmtq.setSwiftfmt(swiftFmt);
            pmtq.setLstmntdte(revdate);
        });
        return list;
    }

}
