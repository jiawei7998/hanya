package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.mapper.opics.SpshMapper;
import com.singlee.financial.wks.service.opics.SpshService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SpshServiceImpl implements SpshService {

    @Resource
    private SpshMapper spshMapper;

    @Override
    public int deleteByPrimaryKey(String br,String dealno,String seq,String fixincind) {
        return spshMapper.deleteByPrimaryKey(br,dealno,seq,fixincind);
    }

    @Override
    public int insert(Spsh record) {
        return spshMapper.insert(record);
    }

    @Override
    public int insertSelective(Spsh record) {
        return spshMapper.insertSelective(record);
    }

    @Override
    public Spsh selectByPrimaryKey(String br,String dealno,String seq,String fixincind) {
        return spshMapper.selectByPrimaryKey(br,dealno,seq,fixincind);
    }

    @Override
    public Spsh selectByTradeNo(String br, String dealtext) {
        return spshMapper.selectByTradeNo(br,dealtext);
    }

    @Override
    public int updateByPrimaryKeySelective(Spsh record) {
        return spshMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Spsh record) {
        return spshMapper.updateByPrimaryKey(record);
    }

}
