package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Rsrm implements Serializable{
	private String rateType;
	private String groupId;
	private String keys;
	private String bidSource;
	private String bidFormula;
	private String bidRule;
	private String offerSource;
	private String offerFormula;
	private String offerRule;
	private Date lstmntdte;
	private BigDecimal updateCounter;
	private BigDecimal tolerance_8;
	private BigDecimal toleranceCap_8;
	private String minRates;
	private String retryAttempts;
	private String retryInterval;
	private String manualPriceInd;
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getKeys() {
		return keys;
	}
	public void setKeys(String keys) {
		this.keys = keys;
	}
	public String getBidSource() {
		return bidSource;
	}
	public void setBidSource(String bidSource) {
		this.bidSource = bidSource;
	}
	public String getBidFormula() {
		return bidFormula;
	}
	public void setBidFormula(String bidFormula) {
		this.bidFormula = bidFormula;
	}
	public String getBidRule() {
		return bidRule;
	}
	public void setBidRule(String bidRule) {
		this.bidRule = bidRule;
	}
	public String getOfferSource() {
		return offerSource;
	}
	public void setOfferSource(String offerSource) {
		this.offerSource = offerSource;
	}
	public String getOfferFormula() {
		return offerFormula;
	}
	public void setOfferFormula(String offerFormula) {
		this.offerFormula = offerFormula;
	}
	public String getOfferRule() {
		return offerRule;
	}
	public void setOfferRule(String offerRule) {
		this.offerRule = offerRule;
	}
	public Date getLstmntdte() {
		return lstmntdte;
	}
	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
	public BigDecimal getUpdateCounter() {
		return updateCounter;
	}
	public void setUpdateCounter(BigDecimal updateCounter) {
		this.updateCounter = updateCounter;
	}
	public BigDecimal getTolerance_8() {
		return tolerance_8;
	}
	public void setTolerance_8(BigDecimal tolerance_8) {
		this.tolerance_8 = tolerance_8;
	}
	public BigDecimal getToleranceCap_8() {
		return toleranceCap_8;
	}
	public void setToleranceCap_8(BigDecimal toleranceCap_8) {
		this.toleranceCap_8 = toleranceCap_8;
	}
	public String getMinRates() {
		return minRates;
	}
	public void setMinRates(String minRates) {
		this.minRates = minRates;
	}
	public String getRetryAttempts() {
		return retryAttempts;
	}
	public void setRetryAttempts(String retryAttempts) {
		this.retryAttempts = retryAttempts;
	}
	public String getRetryInterval() {
		return retryInterval;
	}
	public void setRetryInterval(String retryInterval) {
		this.retryInterval = retryInterval;
	}
	public String getManualPriceInd() {
		return manualPriceInd;
	}
	public void setManualPriceInd(String manualPriceInd) {
		this.manualPriceInd = manualPriceInd;
	}
	
	
}
