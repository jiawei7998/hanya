package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Rprh implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String aetrad;

    private String assignind;

    private BigDecimal assgnqty;

    private Date brprcindte;

    private String brok;

    private BigDecimal brokfeeamt;

    private String brokfeeccy;

    private BigDecimal chargeamt;

    private Date comauthdate;

    private String comauthind;

    private String comauthoper;

    private String comccysacct;

    private String comccysmeans;

    private String comrefno;

    private Date confdate;

    private String confoper;

    private String cnarr;

    private String cost;

    private String ccy;

    private String cno;

    private Date custconfdate;

    private String custrefno;

    private Date dealdate;

    private String dealscre;

    private String dealtext;

    private String dealtime;

    private BigDecimal feeamt;

    private BigDecimal finfxrate8;

    private String finterms;

    private BigDecimal initfxrate8;

    private String initterms;

    private Date inputdate;

    private String ioper;

    private String inputtime;

    private String basis;

    private String intcalctype;

    private BigDecimal margcallpct8;

    private String marginpct;

    private Date matauthdate;

    private String matauthind;

    private String matauthoper;

    private Date matdate;

    private String matccysacct;

    private String matccysmeans;

    private BigDecimal notamt;

    private String notccy;

    private BigDecimal nottotlint;

    private String openrepoind;

    private String product;

    private String prodtype;

    private String ps;

    private BigDecimal mqty;

    private BigDecimal quotqty;

    private BigDecimal repoamt;

    private BigDecimal reporate8;

    private Date revdate;

    private String revoper;

    private String revreason;

    private String revtext;

    private String revtime;

    private BigDecimal spreadrate8;

    private String indexratecode;

    private String substitute;

    private String suppconfind;

    private String tenor;

    private BigDecimal totalint;

    private String trad;

    private Date vdate;

    private BigDecimal vatamt;

    private String verind;

    private Date verdate;

    private String voper;

    private BigDecimal whtamt;

    private BigDecimal yield8;

    private String safekeepacct;

    private String rpind;

    private BigDecimal totprocdamt;

    private BigDecimal comprocdamt;

    private BigDecimal matprocdamt;

    private BigDecimal origprocdamt;

    private String closerepoind;

    private BigDecimal empenamt;

    private String emioper;

    private Date eminputdate;

    private Date origmdate;

    private String intcalcdind;

    private String port;

    private String ratecode;

    private String collateralcode;

    private String collunit;

    private String delivtype;

    private String coupreinvest;

    private BigDecimal coupreinvrate8;

    private String confcode;

    private BigDecimal initbaserate8;

    private String initbaseterms;

    private BigDecimal finbaserate8;

    private String finbaseterms;

    private String openrepomatdays;

    private BigDecimal initindexrate8;

    private BigDecimal finindexrate8;

    private Date finexchratefixdate;

    private Date finindxratefixdate;

    private Date intratefixdate;

    private Date finexchrateentdate;

    private Date finindxrateentdate;

    private Date intrateenddate;

    private Long updatecounter;

    private BigDecimal comprocbaseamt;

    private BigDecimal matprocbaseamt;

    private BigDecimal convintamt;

    private BigDecimal convintbamt;

    private Date assigndate;

    private Date prinbaseratefixdate;

    private Date prinbaserateentdate;

    private String overridewxtaxind;

    private String borrlendind;

    private String text1;

    private String text2;

    private Date date1;

    private Date date2;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private BigDecimal rate18;

    private BigDecimal rate28;

    private BigDecimal commamt;

    private BigDecimal exchcommamt;

    private BigDecimal stampdutyamt;

    private String exchcno;

    private String exchind;

    private String btbdealno;

    private String commusualid;

    private String matusualid;

    private String swiftbothind;

    private String linkdealno;

    private String linkind;

    private String blockind;

    private String splitind;

    private String ccpind;

    private String extcompare;

    private String vdatenet;

    private String vdpaymentholdind;

    private String mdpaymentholdind;

    private String newdealno;

    private String origdealno;

    private String rollind;

    private Date rollinputdate;

    private String rollcapintind;

    private BigDecimal rollprinout;

    private BigDecimal rollprinin;

    private BigDecimal rolldiffamt;

    private Date rollrevdate;

    private String mergeind;

    private String assignmethod;

    private String autoassign;

    private BigDecimal statedreporate8;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getAetrad() {
		return aetrad;
	}


	public void setAetrad(String aetrad) {
		this.aetrad = aetrad;
	}


	public String getAssignind() {
		return assignind;
	}


	public void setAssignind(String assignind) {
		this.assignind = assignind;
	}


	public BigDecimal getAssgnqty() {
		return assgnqty;
	}


	public void setAssgnqty(BigDecimal assgnqty) {
		this.assgnqty = assgnqty;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public String getBrok() {
		return brok;
	}


	public void setBrok(String brok) {
		this.brok = brok;
	}


	public BigDecimal getBrokfeeamt() {
		return brokfeeamt;
	}


	public void setBrokfeeamt(BigDecimal brokfeeamt) {
		this.brokfeeamt = brokfeeamt;
	}


	public String getBrokfeeccy() {
		return brokfeeccy;
	}


	public void setBrokfeeccy(String brokfeeccy) {
		this.brokfeeccy = brokfeeccy;
	}


	public BigDecimal getChargeamt() {
		return chargeamt;
	}


	public void setChargeamt(BigDecimal chargeamt) {
		this.chargeamt = chargeamt;
	}


	public Date getComauthdate() {
		return comauthdate;
	}


	public void setComauthdate(Date comauthdate) {
		this.comauthdate = comauthdate;
	}


	public String getComauthind() {
		return comauthind;
	}


	public void setComauthind(String comauthind) {
		this.comauthind = comauthind;
	}


	public String getComauthoper() {
		return comauthoper;
	}


	public void setComauthoper(String comauthoper) {
		this.comauthoper = comauthoper;
	}


	public String getComccysacct() {
		return comccysacct;
	}


	public void setComccysacct(String comccysacct) {
		this.comccysacct = comccysacct;
	}


	public String getComccysmeans() {
		return comccysmeans;
	}


	public void setComccysmeans(String comccysmeans) {
		this.comccysmeans = comccysmeans;
	}


	public String getComrefno() {
		return comrefno;
	}


	public void setComrefno(String comrefno) {
		this.comrefno = comrefno;
	}


	public Date getConfdate() {
		return confdate;
	}


	public void setConfdate(Date confdate) {
		this.confdate = confdate;
	}


	public String getConfoper() {
		return confoper;
	}


	public void setConfoper(String confoper) {
		this.confoper = confoper;
	}


	public String getCnarr() {
		return cnarr;
	}


	public void setCnarr(String cnarr) {
		this.cnarr = cnarr;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public Date getCustconfdate() {
		return custconfdate;
	}


	public void setCustconfdate(Date custconfdate) {
		this.custconfdate = custconfdate;
	}


	public String getCustrefno() {
		return custrefno;
	}


	public void setCustrefno(String custrefno) {
		this.custrefno = custrefno;
	}


	public Date getDealdate() {
		return dealdate;
	}


	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}


	public String getDealscre() {
		return dealscre;
	}


	public void setDealscre(String dealscre) {
		this.dealscre = dealscre;
	}


	public String getDealtext() {
		return dealtext;
	}


	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}


	public String getDealtime() {
		return dealtime;
	}


	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}


	public BigDecimal getFeeamt() {
		return feeamt;
	}


	public void setFeeamt(BigDecimal feeamt) {
		this.feeamt = feeamt;
	}


	public BigDecimal getFinfxrate8() {
		return finfxrate8;
	}


	public void setFinfxrate8(BigDecimal finfxrate8) {
		this.finfxrate8 = finfxrate8;
	}


	public String getFinterms() {
		return finterms;
	}


	public void setFinterms(String finterms) {
		this.finterms = finterms;
	}


	public BigDecimal getInitfxrate8() {
		return initfxrate8;
	}


	public void setInitfxrate8(BigDecimal initfxrate8) {
		this.initfxrate8 = initfxrate8;
	}


	public String getInitterms() {
		return initterms;
	}


	public void setInitterms(String initterms) {
		this.initterms = initterms;
	}


	public Date getInputdate() {
		return inputdate;
	}


	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}


	public String getIoper() {
		return ioper;
	}


	public void setIoper(String ioper) {
		this.ioper = ioper;
	}


	public String getInputtime() {
		return inputtime;
	}


	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public String getIntcalctype() {
		return intcalctype;
	}


	public void setIntcalctype(String intcalctype) {
		this.intcalctype = intcalctype;
	}


	public BigDecimal getMargcallpct8() {
		return margcallpct8;
	}


	public void setMargcallpct8(BigDecimal margcallpct8) {
		this.margcallpct8 = margcallpct8;
	}


	public String getMarginpct() {
		return marginpct;
	}


	public void setMarginpct(String marginpct) {
		this.marginpct = marginpct;
	}


	public Date getMatauthdate() {
		return matauthdate;
	}


	public void setMatauthdate(Date matauthdate) {
		this.matauthdate = matauthdate;
	}


	public String getMatauthind() {
		return matauthind;
	}


	public void setMatauthind(String matauthind) {
		this.matauthind = matauthind;
	}


	public String getMatauthoper() {
		return matauthoper;
	}


	public void setMatauthoper(String matauthoper) {
		this.matauthoper = matauthoper;
	}


	public Date getMatdate() {
		return matdate;
	}


	public void setMatdate(Date matdate) {
		this.matdate = matdate;
	}


	public String getMatccysacct() {
		return matccysacct;
	}


	public void setMatccysacct(String matccysacct) {
		this.matccysacct = matccysacct;
	}


	public String getMatccysmeans() {
		return matccysmeans;
	}


	public void setMatccysmeans(String matccysmeans) {
		this.matccysmeans = matccysmeans;
	}


	public BigDecimal getNotamt() {
		return notamt;
	}


	public void setNotamt(BigDecimal notamt) {
		this.notamt = notamt;
	}


	public String getNotccy() {
		return notccy;
	}


	public void setNotccy(String notccy) {
		this.notccy = notccy;
	}


	public BigDecimal getNottotlint() {
		return nottotlint;
	}


	public void setNottotlint(BigDecimal nottotlint) {
		this.nottotlint = nottotlint;
	}


	public String getOpenrepoind() {
		return openrepoind;
	}


	public void setOpenrepoind(String openrepoind) {
		this.openrepoind = openrepoind;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getPs() {
		return ps;
	}


	public void setPs(String ps) {
		this.ps = ps;
	}


	public BigDecimal getMqty() {
		return mqty;
	}


	public void setMqty(BigDecimal mqty) {
		this.mqty = mqty;
	}


	public BigDecimal getQuotqty() {
		return quotqty;
	}


	public void setQuotqty(BigDecimal quotqty) {
		this.quotqty = quotqty;
	}


	public BigDecimal getRepoamt() {
		return repoamt;
	}


	public void setRepoamt(BigDecimal repoamt) {
		this.repoamt = repoamt;
	}


	public BigDecimal getReporate8() {
		return reporate8;
	}


	public void setReporate8(BigDecimal reporate8) {
		this.reporate8 = reporate8;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public String getRevoper() {
		return revoper;
	}


	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}


	public String getRevreason() {
		return revreason;
	}


	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}


	public String getRevtext() {
		return revtext;
	}


	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}


	public String getRevtime() {
		return revtime;
	}


	public void setRevtime(String revtime) {
		this.revtime = revtime;
	}


	public BigDecimal getSpreadrate8() {
		return spreadrate8;
	}


	public void setSpreadrate8(BigDecimal spreadrate8) {
		this.spreadrate8 = spreadrate8;
	}


	public String getIndexratecode() {
		return indexratecode;
	}


	public void setIndexratecode(String indexratecode) {
		this.indexratecode = indexratecode;
	}


	public String getSubstitute() {
		return substitute;
	}


	public void setSubstitute(String substitute) {
		this.substitute = substitute;
	}


	public String getSuppconfind() {
		return suppconfind;
	}


	public void setSuppconfind(String suppconfind) {
		this.suppconfind = suppconfind;
	}


	public String getTenor() {
		return tenor;
	}


	public void setTenor(String tenor) {
		this.tenor = tenor;
	}


	public BigDecimal getTotalint() {
		return totalint;
	}


	public void setTotalint(BigDecimal totalint) {
		this.totalint = totalint;
	}


	public String getTrad() {
		return trad;
	}


	public void setTrad(String trad) {
		this.trad = trad;
	}


	public Date getVdate() {
		return vdate;
	}


	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}


	public BigDecimal getVatamt() {
		return vatamt;
	}


	public void setVatamt(BigDecimal vatamt) {
		this.vatamt = vatamt;
	}


	public String getVerind() {
		return verind;
	}


	public void setVerind(String verind) {
		this.verind = verind;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public String getVoper() {
		return voper;
	}


	public void setVoper(String voper) {
		this.voper = voper;
	}


	public BigDecimal getWhtamt() {
		return whtamt;
	}


	public void setWhtamt(BigDecimal whtamt) {
		this.whtamt = whtamt;
	}


	public BigDecimal getYield8() {
		return yield8;
	}


	public void setYield8(BigDecimal yield8) {
		this.yield8 = yield8;
	}


	public String getSafekeepacct() {
		return safekeepacct;
	}


	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}


	public String getRpind() {
		return rpind;
	}


	public void setRpind(String rpind) {
		this.rpind = rpind;
	}


	public BigDecimal getTotprocdamt() {
		return totprocdamt;
	}


	public void setTotprocdamt(BigDecimal totprocdamt) {
		this.totprocdamt = totprocdamt;
	}


	public BigDecimal getComprocdamt() {
		return comprocdamt;
	}


	public void setComprocdamt(BigDecimal comprocdamt) {
		this.comprocdamt = comprocdamt;
	}


	public BigDecimal getMatprocdamt() {
		return matprocdamt;
	}


	public void setMatprocdamt(BigDecimal matprocdamt) {
		this.matprocdamt = matprocdamt;
	}


	public BigDecimal getOrigprocdamt() {
		return origprocdamt;
	}


	public void setOrigprocdamt(BigDecimal origprocdamt) {
		this.origprocdamt = origprocdamt;
	}


	public String getCloserepoind() {
		return closerepoind;
	}


	public void setCloserepoind(String closerepoind) {
		this.closerepoind = closerepoind;
	}


	public BigDecimal getEmpenamt() {
		return empenamt;
	}


	public void setEmpenamt(BigDecimal empenamt) {
		this.empenamt = empenamt;
	}


	public String getEmioper() {
		return emioper;
	}


	public void setEmioper(String emioper) {
		this.emioper = emioper;
	}


	public Date getEminputdate() {
		return eminputdate;
	}


	public void setEminputdate(Date eminputdate) {
		this.eminputdate = eminputdate;
	}


	public Date getOrigmdate() {
		return origmdate;
	}


	public void setOrigmdate(Date origmdate) {
		this.origmdate = origmdate;
	}


	public String getIntcalcdind() {
		return intcalcdind;
	}


	public void setIntcalcdind(String intcalcdind) {
		this.intcalcdind = intcalcdind;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getRatecode() {
		return ratecode;
	}


	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}


	public String getCollateralcode() {
		return collateralcode;
	}


	public void setCollateralcode(String collateralcode) {
		this.collateralcode = collateralcode;
	}


	public String getCollunit() {
		return collunit;
	}


	public void setCollunit(String collunit) {
		this.collunit = collunit;
	}


	public String getDelivtype() {
		return delivtype;
	}


	public void setDelivtype(String delivtype) {
		this.delivtype = delivtype;
	}


	public String getCoupreinvest() {
		return coupreinvest;
	}


	public void setCoupreinvest(String coupreinvest) {
		this.coupreinvest = coupreinvest;
	}


	public BigDecimal getCoupreinvrate8() {
		return coupreinvrate8;
	}


	public void setCoupreinvrate8(BigDecimal coupreinvrate8) {
		this.coupreinvrate8 = coupreinvrate8;
	}


	public String getConfcode() {
		return confcode;
	}


	public void setConfcode(String confcode) {
		this.confcode = confcode;
	}


	public BigDecimal getInitbaserate8() {
		return initbaserate8;
	}


	public void setInitbaserate8(BigDecimal initbaserate8) {
		this.initbaserate8 = initbaserate8;
	}


	public String getInitbaseterms() {
		return initbaseterms;
	}


	public void setInitbaseterms(String initbaseterms) {
		this.initbaseterms = initbaseterms;
	}


	public BigDecimal getFinbaserate8() {
		return finbaserate8;
	}


	public void setFinbaserate8(BigDecimal finbaserate8) {
		this.finbaserate8 = finbaserate8;
	}


	public String getFinbaseterms() {
		return finbaseterms;
	}


	public void setFinbaseterms(String finbaseterms) {
		this.finbaseterms = finbaseterms;
	}


	public String getOpenrepomatdays() {
		return openrepomatdays;
	}


	public void setOpenrepomatdays(String openrepomatdays) {
		this.openrepomatdays = openrepomatdays;
	}


	public BigDecimal getInitindexrate8() {
		return initindexrate8;
	}


	public void setInitindexrate8(BigDecimal initindexrate8) {
		this.initindexrate8 = initindexrate8;
	}


	public BigDecimal getFinindexrate8() {
		return finindexrate8;
	}


	public void setFinindexrate8(BigDecimal finindexrate8) {
		this.finindexrate8 = finindexrate8;
	}


	public Date getFinexchratefixdate() {
		return finexchratefixdate;
	}


	public void setFinexchratefixdate(Date finexchratefixdate) {
		this.finexchratefixdate = finexchratefixdate;
	}


	public Date getFinindxratefixdate() {
		return finindxratefixdate;
	}


	public void setFinindxratefixdate(Date finindxratefixdate) {
		this.finindxratefixdate = finindxratefixdate;
	}


	public Date getIntratefixdate() {
		return intratefixdate;
	}


	public void setIntratefixdate(Date intratefixdate) {
		this.intratefixdate = intratefixdate;
	}


	public Date getFinexchrateentdate() {
		return finexchrateentdate;
	}


	public void setFinexchrateentdate(Date finexchrateentdate) {
		this.finexchrateentdate = finexchrateentdate;
	}


	public Date getFinindxrateentdate() {
		return finindxrateentdate;
	}


	public void setFinindxrateentdate(Date finindxrateentdate) {
		this.finindxrateentdate = finindxrateentdate;
	}


	public Date getIntrateenddate() {
		return intrateenddate;
	}


	public void setIntrateenddate(Date intrateenddate) {
		this.intrateenddate = intrateenddate;
	}


	public Long getUpdatecounter() {
		return updatecounter;
	}


	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}


	public BigDecimal getComprocbaseamt() {
		return comprocbaseamt;
	}


	public void setComprocbaseamt(BigDecimal comprocbaseamt) {
		this.comprocbaseamt = comprocbaseamt;
	}


	public BigDecimal getMatprocbaseamt() {
		return matprocbaseamt;
	}


	public void setMatprocbaseamt(BigDecimal matprocbaseamt) {
		this.matprocbaseamt = matprocbaseamt;
	}


	public BigDecimal getConvintamt() {
		return convintamt;
	}


	public void setConvintamt(BigDecimal convintamt) {
		this.convintamt = convintamt;
	}


	public BigDecimal getConvintbamt() {
		return convintbamt;
	}


	public void setConvintbamt(BigDecimal convintbamt) {
		this.convintbamt = convintbamt;
	}


	public Date getAssigndate() {
		return assigndate;
	}


	public void setAssigndate(Date assigndate) {
		this.assigndate = assigndate;
	}


	public Date getPrinbaseratefixdate() {
		return prinbaseratefixdate;
	}


	public void setPrinbaseratefixdate(Date prinbaseratefixdate) {
		this.prinbaseratefixdate = prinbaseratefixdate;
	}


	public Date getPrinbaserateentdate() {
		return prinbaserateentdate;
	}


	public void setPrinbaserateentdate(Date prinbaserateentdate) {
		this.prinbaserateentdate = prinbaserateentdate;
	}


	public String getOverridewxtaxind() {
		return overridewxtaxind;
	}


	public void setOverridewxtaxind(String overridewxtaxind) {
		this.overridewxtaxind = overridewxtaxind;
	}


	public String getBorrlendind() {
		return borrlendind;
	}


	public void setBorrlendind(String borrlendind) {
		this.borrlendind = borrlendind;
	}


	public String getText1() {
		return text1;
	}


	public void setText1(String text1) {
		this.text1 = text1;
	}


	public String getText2() {
		return text2;
	}


	public void setText2(String text2) {
		this.text2 = text2;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public BigDecimal getRate18() {
		return rate18;
	}


	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}


	public BigDecimal getRate28() {
		return rate28;
	}


	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}


	public BigDecimal getCommamt() {
		return commamt;
	}


	public void setCommamt(BigDecimal commamt) {
		this.commamt = commamt;
	}


	public BigDecimal getExchcommamt() {
		return exchcommamt;
	}


	public void setExchcommamt(BigDecimal exchcommamt) {
		this.exchcommamt = exchcommamt;
	}


	public BigDecimal getStampdutyamt() {
		return stampdutyamt;
	}


	public void setStampdutyamt(BigDecimal stampdutyamt) {
		this.stampdutyamt = stampdutyamt;
	}


	public String getExchcno() {
		return exchcno;
	}


	public void setExchcno(String exchcno) {
		this.exchcno = exchcno;
	}


	public String getExchind() {
		return exchind;
	}


	public void setExchind(String exchind) {
		this.exchind = exchind;
	}


	public String getBtbdealno() {
		return btbdealno;
	}


	public void setBtbdealno(String btbdealno) {
		this.btbdealno = btbdealno;
	}


	public String getCommusualid() {
		return commusualid;
	}


	public void setCommusualid(String commusualid) {
		this.commusualid = commusualid;
	}


	public String getMatusualid() {
		return matusualid;
	}


	public void setMatusualid(String matusualid) {
		this.matusualid = matusualid;
	}


	public String getSwiftbothind() {
		return swiftbothind;
	}


	public void setSwiftbothind(String swiftbothind) {
		this.swiftbothind = swiftbothind;
	}


	public String getLinkdealno() {
		return linkdealno;
	}


	public void setLinkdealno(String linkdealno) {
		this.linkdealno = linkdealno;
	}


	public String getLinkind() {
		return linkind;
	}


	public void setLinkind(String linkind) {
		this.linkind = linkind;
	}


	public String getBlockind() {
		return blockind;
	}


	public void setBlockind(String blockind) {
		this.blockind = blockind;
	}


	public String getSplitind() {
		return splitind;
	}


	public void setSplitind(String splitind) {
		this.splitind = splitind;
	}


	public String getCcpind() {
		return ccpind;
	}


	public void setCcpind(String ccpind) {
		this.ccpind = ccpind;
	}


	public String getExtcompare() {
		return extcompare;
	}


	public void setExtcompare(String extcompare) {
		this.extcompare = extcompare;
	}


	public String getVdatenet() {
		return vdatenet;
	}


	public void setVdatenet(String vdatenet) {
		this.vdatenet = vdatenet;
	}


	public String getVdpaymentholdind() {
		return vdpaymentholdind;
	}


	public void setVdpaymentholdind(String vdpaymentholdind) {
		this.vdpaymentholdind = vdpaymentholdind;
	}


	public String getMdpaymentholdind() {
		return mdpaymentholdind;
	}


	public void setMdpaymentholdind(String mdpaymentholdind) {
		this.mdpaymentholdind = mdpaymentholdind;
	}


	public String getNewdealno() {
		return newdealno;
	}


	public void setNewdealno(String newdealno) {
		this.newdealno = newdealno;
	}


	public String getOrigdealno() {
		return origdealno;
	}


	public void setOrigdealno(String origdealno) {
		this.origdealno = origdealno;
	}


	public String getRollind() {
		return rollind;
	}


	public void setRollind(String rollind) {
		this.rollind = rollind;
	}


	public Date getRollinputdate() {
		return rollinputdate;
	}


	public void setRollinputdate(Date rollinputdate) {
		this.rollinputdate = rollinputdate;
	}


	public String getRollcapintind() {
		return rollcapintind;
	}


	public void setRollcapintind(String rollcapintind) {
		this.rollcapintind = rollcapintind;
	}


	public BigDecimal getRollprinout() {
		return rollprinout;
	}


	public void setRollprinout(BigDecimal rollprinout) {
		this.rollprinout = rollprinout;
	}


	public BigDecimal getRollprinin() {
		return rollprinin;
	}


	public void setRollprinin(BigDecimal rollprinin) {
		this.rollprinin = rollprinin;
	}


	public BigDecimal getRolldiffamt() {
		return rolldiffamt;
	}


	public void setRolldiffamt(BigDecimal rolldiffamt) {
		this.rolldiffamt = rolldiffamt;
	}


	public Date getRollrevdate() {
		return rollrevdate;
	}


	public void setRollrevdate(Date rollrevdate) {
		this.rollrevdate = rollrevdate;
	}


	public String getMergeind() {
		return mergeind;
	}


	public void setMergeind(String mergeind) {
		this.mergeind = mergeind;
	}


	public String getAssignmethod() {
		return assignmethod;
	}


	public void setAssignmethod(String assignmethod) {
		this.assignmethod = assignmethod;
	}


	public String getAutoassign() {
		return autoassign;
	}


	public void setAutoassign(String autoassign) {
		this.autoassign = autoassign;
	}


	public BigDecimal getStatedreporate8() {
		return statedreporate8;
	}


	public void setStatedreporate8(BigDecimal statedreporate8) {
		this.statedreporate8 = statedreporate8;
	}


	private static final long serialVersionUID = 1L;
}