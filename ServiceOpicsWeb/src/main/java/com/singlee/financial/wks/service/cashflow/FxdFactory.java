package com.singlee.financial.wks.service.cashflow;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.singlee.financial.wks.bean.opics.Revp;
import com.singlee.financial.wks.expand.RevpRow;
import com.singlee.financial.wks.util.FinancialCalculationModel;
import com.singlee.financial.wks.util.FinancialDateUtils;

public class FxdFactory {
	
	private static Logger logger = Logger.getLogger(FxdFactory.class);
	
	/**
	 * 计算对应货币金额
	 * @param terms
	 * @param ccyAmt
	 * @param ccyRate
	 * @return
	 */
	public static BigDecimal calculationCtramt(String terms,BigDecimal ccyAmt, BigDecimal ccyRate,int scale) {
		BigDecimal ctramt = null;
		logger.debug(String.format("FxdFactory.calculationCtramt,terms:%s,ccyAmt:%15.4f,ccyRate,%15.8f,", terms,ccyAmt,ccyRate));
		switch (terms) {
		case "M":
			ctramt = ccyAmt.multiply(ccyRate).setScale(scale, RoundingMode.HALF_UP).negate();
			break;
		case "D":
			ctramt = ccyAmt.divide(ccyRate,scale, RoundingMode.HALF_UP).negate();
			break;
		default:
			throw new RuntimeException("货币对运算关系[ "+terms+" ]不支持");
		}
		
		return ctramt;
	}
	
	
	/**
	 * 方法已重载.计算对应货币拆本国货币的总收益
	 * @param baseCcy
	 * @param ccy
	 * @param ctrCcy
	 * @param ccyAmt
	 * @param ccyRate
	 * @param ccyBp
	 * @param ccySpotBaseRate
	 * @return
	 */
	public static BigDecimal calculationFwdpremamt(String fwdpremamtMethod, int ctramtScaleLength, BigDecimal ccyAmt, BigDecimal ccyRate, BigDecimal ccyBp, BigDecimal ccySpotBaseRate, String terms,
			BigDecimal revpCtrSpotRate,  int ccyamtScaleLength) {
		BigDecimal amt = null;
		
		logger.info(String.format("FxdFactory.calculationFwdpremamt,fwdpremamtMethod:%s",fwdpremamtMethod));

		// 根据不同场景调用不同的计算方法
		switch (fwdpremamtMethod) {
		case "JSH":
			// 结售汇交易处理
			amt = calculationFwdpremamt(ctramtScaleLength,ccyAmt, ccyRate, ccyBp, revpCtrSpotRate, terms);
			break;
		case "FX":
			// 外汇买卖CCY货币与CCYP.TRADEDCCY相同时的计算
			amt = calculationFwdpremamt(ctramtScaleLength,terms, ccyAmt, ccyRate, ccyBp, ccySpotBaseRate);
			break;
		case "FX_OTHER":
			// 外汇买卖CCY货币与CCYP.TRADEDCCY不同时的计算
			amt = calculationFwdpremamt(terms, ccyAmt, ccyRate, ccyBp, ccySpotBaseRate,ctramtScaleLength,ccyamtScaleLength);
			break;
		default:
			throw new RuntimeException("Fwdpremamt字段计算方法[ "+fwdpremamtMethod+" ]不支持");
		}// end switch

		return amt;
	}
	
	/**
	 * 方法已重载.计算Fwdpremamt字段方法三,外汇买卖CCY货币与CCYP.TRADEDCCY不一致时的处理,规则为(CCYSPOTAMT-CCYAMT) * CCYSPOTBASERATE
	 * @param terms
	 * @param ccyAmt
	 * @param ccyRate
	 * @param ccyBp
	 * @param ccySpotBaseRate
	 * @param ctramtScaleLength
	 * @param ccyamtScaleLength
	 * @return
	 */
	private static BigDecimal calculationFwdpremamt(String terms,BigDecimal ccyAmt,BigDecimal ccyRate,BigDecimal ccyBp,BigDecimal ccySpotBaseRate,int ctramtScaleLength,int ccyamtScaleLength) {
		BigDecimal ctrPd = null;
		BigDecimal ccySpotAmt = null;
		BigDecimal ctrRate = null;
		BigDecimal ctrSpotRate = null;
		BigDecimal ctrAmt = null;
		BigDecimal fwdpremamt = null;

		//计算对应货币金额
		ctrAmt = calculationCtramt(terms, ccyAmt, ccyRate, ctramtScaleLength);
		//计算对应货币即期汇率金额
		ctrSpotRate = calculationCounterCurrencyRate(ccyAmt,ccyRate.subtract(ccyBp),terms, 4).abs();
		//交易即期汇率金额
		ccySpotAmt = calculationCtramt(terms,ctrAmt,ctrSpotRate,4);
		//损益
		fwdpremamt = ccySpotAmt.subtract(ccyAmt).multiply(ccySpotBaseRate).setScale(2, RoundingMode.HALF_UP);
		
		logger.info(String.format("FxdFactory.calculationFwdpremamtFun3,plAmt:%.2f,ccyAmt:%.2f,crtAmt：%.8f,terms：%s,ccySpotAmt：%.8f,ccySpotBaseRate:%.15f,"
				+ "ctrSpotRate:%.15f,ctrRate:%.15f,ctrPd:%.15f",fwdpremamt,ccyAmt,ctrAmt,terms,ccySpotAmt,ccySpotBaseRate,ctrSpotRate,ctrRate,ctrPd));
		
		return fwdpremamt;
	}
	
	/**
	 * 方法已重载.计算Fwdpremamt字段方法二,规则为CTRAMT*CTRPD*CCYSPOTBASERATE
	 * @param ccyAmt
	 * @param ccyRate
	 * @param ccyBp
	 * @param ccySpotBaseRate
	 * @param terms
	 * @return
	 */
	private static BigDecimal calculationFwdpremamt(int ctramtScaleLength,String terms,BigDecimal ccyAmt,BigDecimal ccyRate,BigDecimal ccyBp,BigDecimal ccySpotBaseRate) {
		BigDecimal amt = null;
		BigDecimal ctrAmt = null;
		BigDecimal ctrPd = null;
		
		//计算对应货币金额
		ctrAmt = calculationCtramt(terms,ccyAmt,ccyRate,ctramtScaleLength);
		//计算对应货币点差
		ctrPd = calculationCounterCurrencySpread(ccyAmt,ccyRate,ccyBp,terms,4);
		
		amt = ctrAmt.negate().multiply(ctrPd).multiply(ccySpotBaseRate).setScale(2, RoundingMode.HALF_UP);
		
		logger.info(String.format("FxdFactory.calculationFwdpremamtFun2,plAmt:%.2f,crtAmt：%.2f,ctrpd:%.8f,ccySpotBaseRate:%.15f,", 
				amt,ctrAmt,ctrPd,ccySpotBaseRate));
		
		return amt;
	}
	
	/**
	 * 方法已重载.计算Fwdpremamt字段方法一,规则为(CTRAMT-CTRSPOTAMT)*CCYSPOTRATE
	 * @param ccyAmt
	 * @param ccyRate
	 * @param ccyBp
	 * @param ctrccySpotRate
	 * @param terms
	 * @return
	 */
	private static BigDecimal calculationFwdpremamt(int ctramtScaleLength,BigDecimal ccyAmt, BigDecimal ccyRate,BigDecimal ccyBp,BigDecimal ctrccySpotRate,String terms) {
		BigDecimal ccySpotRate = null;
		BigDecimal crtccySpotAmt = null;
		BigDecimal amt = null;
		BigDecimal ctrAmt = null;
		
		
		//计算对应货币金额
		ctrAmt = calculationCtramt(terms,ccyAmt,ccyRate,ctramtScaleLength);
		//计算CCY端即期汇率
		ccySpotRate = ccyRate.subtract(ccyBp).setScale(12, RoundingMode.HALF_UP);
		//计算CTRCCY端即期金额
		crtccySpotAmt = calculationCtramt(terms, ccyAmt, ccySpotRate, 4);
		
		amt = ctrAmt.subtract(crtccySpotAmt).multiply(ctrccySpotRate).setScale(2, RoundingMode.HALF_UP);		
		
		logger.info(String.format("FxdFactory.calculationFwdpremamtFun1,plAmt:%.2f,ccyAmt:%.2f,crtAmt：%.2f,terms：%s,crtccySpotAmt：%.4f,ctrccySpotRate:%.15f,"
				+ "ccySpotRate:%.15f",amt,ccyAmt,ctrAmt,terms,crtccySpotAmt,ctrccySpotRate,ccySpotRate));
		
		return amt;
	}
	
	/**
	 * 计算对应货币点差
	 * @param ccyAmt
	 * @param ccyRate
	 * @param ccyBp
	 * @return
	 */
	public static BigDecimal calculationCounterCurrencySpread(BigDecimal ccyAmt,BigDecimal ccyRate,BigDecimal ccyBp,String terms,int ctrScaleLength) {
		BigDecimal ctrBp = null;
		BigDecimal ctrRate = null;
		BigDecimal ctrSpotRate = null;
		
		//计算对应货币成交汇率
		ctrRate = calculationCounterCurrencyRate(ccyAmt,ccyRate,terms,ctrScaleLength,12);
		//计算对应货币即期汇率
		ctrSpotRate = calculationCounterCurrencyRate(ccyAmt,ccyRate.subtract(ccyBp).setScale(15, RoundingMode.HALF_UP),terms,ctrScaleLength);
		//计算对应货币点差
		ctrBp = ctrRate.subtract(ctrSpotRate).setScale(15, RoundingMode.HALF_UP);
		
		return ctrBp;
	}
	
	/**
	 * 计算对应货币拆本国货币汇率
	 * @param ccyAmt
	 * @param ccyRate
	 * @param ccyBaseRate
	 * @return
	 */
	public static BigDecimal calculationCounterCurrencyBaseRate(BigDecimal ccyAmt,BigDecimal ccyRate,BigDecimal ccyBaseRate,String baseCcy,String ccy,String terms,String baseTerms,int ctrScaleLength) {
		BigDecimal ctrBaseRate = null;
		BigDecimal ctrAmt = null;
		BigDecimal ctrBAmt = null;
		
		logger.info(String.format("FxdFactory.calculationCounterCurrencyBaseRate,dealTerms:%s,baseTerms:%s", terms,baseTerms));
		
		if(baseCcy.equalsIgnoreCase(ccy)) {
			ctrBaseRate = ccyRate;
		}else {
			//对应货币金额
			ctrAmt = calculationCtramt(terms,ccyAmt,ccyRate,ctrScaleLength);
			//对应货币拆人民币金额
			ctrBAmt = ccyAmt.multiply(ccyBaseRate).setScale(4, RoundingMode.HALF_UP);
			
			//计算对应货币汇率
			switch (baseTerms) {
			case "D":
				ctrBaseRate = ctrAmt.divide(ctrBAmt, 15, RoundingMode.HALF_UP).abs();
				break;
			case "M":
				ctrBaseRate = ctrBAmt.divide(ctrAmt, 15, RoundingMode.HALF_UP).abs();
				break;
			default:
				throw new RuntimeException("货币对运算关系[ "+terms+" ]不支持");
			}
			
			logger.info(String.format("FxdFactory.calculationCounterCurrencyBaseRate,ctrAmt:%.2f,ctrBAmt:%.4f,ctrBaseRate:%.8f", ctrAmt,ctrBAmt,ctrBaseRate));
		}

		return ctrBaseRate;
	}
	
	/**
	 * 方法已重载.计算对应货币汇率,默认汇率小数位保留15位
	 * @param ccyAmt
	 * @param ccyRate
	 * @param terms
	 * @param ctrScaleLength
	 * @return
	 */
	public static BigDecimal calculationCounterCurrencyRate(BigDecimal ccyAmt,BigDecimal ccyRate,String terms,int ctrScaleLength) {
		return calculationCounterCurrencyRate(ccyAmt,ccyRate,terms,ctrScaleLength,15);
	}
	
	/**
	 * 方法已重载.计算对应货币汇率,指定汇率小数位
	 * @return
	 */
	public static BigDecimal calculationCounterCurrencyRate(BigDecimal ccyAmt,BigDecimal ccyRate,String terms,int ctrScaleLength,int rateScaleLength) {
		BigDecimal ctrRate = null;
		BigDecimal ctrAmt = null;
		
		//计算对应货币金额
		ctrAmt = calculationCtramt(terms,ccyAmt,ccyRate,ctrScaleLength);
		switch (terms) {
		case "D":
			ctrRate = ctrAmt.divide(ccyAmt, rateScaleLength, RoundingMode.HALF_UP).abs();
			break;
		case "M":
			ctrRate = ccyAmt.divide(ctrAmt, rateScaleLength, RoundingMode.HALF_UP).abs();
			break;
		default:
			throw new RuntimeException("货币对运算关系[ "+terms+" ]不支持");
		}
		return ctrRate;
	}
	

	/**
	 * 计算拆本国货币金额
	 * @param amount
	 * @param rate
	 * @return
	 */
	public static BigDecimal calculationBaseAmount(BigDecimal amount,BigDecimal baseRate,int scaleLength) {
		return amount.multiply(baseRate).setScale(scaleLength, RoundingMode.HALF_UP).negate();
	}
	
	/**
	 * 计算外汇交易远端交割日对应的CCY BASE SPOT RATE BP
	 * @param vdate
	 * @param revpRows
	 * @return
	 */
	public static BigDecimal calculationCcyBaseSpread(Date vdate,Date spotDate,List<RevpRow> revpRows) {
		BigDecimal ccyBpd = null;
		Optional<RevpRow> op = null;
		RevpRow revpB = null;
		RevpRow revpD = null;
		
		if(null == vdate) {
			throw new RuntimeException("交易起息日不能为空");
		}
		
		if(null == revpRows || revpRows.size() == 0) {
			throw new RuntimeException("掉期点不能为空");
		}
		
		if(revpRows.size() == 1 ) {
			return BigDecimal.ZERO;
		}
		
		//找到小于等于起息日的最大的日期
		if(vdate.compareTo(spotDate) > 0) {
			op = revpRows.stream().sorted(Comparator.comparing(RevpRow::getMtyDate)).filter(var -> vdate.compareTo(var.getMtyDate()) >= 0).max(Comparator.comparing(RevpRow::getMtyDate));
		}else {
			op = revpRows.stream().filter(var -> "SPOT".equalsIgnoreCase(var.getMty())).findFirst();
		}
		if(!op.isPresent()) {
			throw new RuntimeException("找不到对应的掉期点");
		}
		revpB = op.get();
		
		//找到大于等于起息日的最小的日期
		if(vdate.compareTo(spotDate) > 0) {
			op = revpRows.stream().sorted(Comparator.comparing(RevpRow::getMtyDate, Comparator.reverseOrder())).filter(var -> vdate.compareTo(var.getMtyDate()) <= 0)
					.min(Comparator.comparing(RevpRow::getMtyDate));
		}else {
			op = revpRows.stream().filter(var -> "SPOT".equalsIgnoreCase(var.getMty())).findFirst();
		}
		if (!op.isPresent()) {
			throw new RuntimeException("找不到对应的掉期点");
		}
		revpD = op.get();
		
		if(revpB.getMtyDate().equals(revpD.getMtyDate())) {
			ccyBpd = revpB.getRateBp();
		}else {
			ccyBpd = FinancialCalculationModel.calculateInterpolationRate(revpB.getRateBp(),revpD.getRateBp(),revpB.getMtyDate(),revpD.getMtyDate(),vdate).setScale(15, RoundingMode.HALF_UP);
		}
		
		logger.info(String.format("交易起息日期[%tF],计算CCYBPD[%.15f],REVP上一个日期[%tF]掉期点[%.15f],REVP下一个日期[%tF]掉期点[%.15f]",vdate,ccyBpd,revpB.getMtyDate(),revpB.getRateBp(),revpD.getMtyDate(),revpD.getRateBp()));
		return ccyBpd;
	}
	
	/**
	 * 方法已重载.将一条REVP记录的掉期点转换为list
	 * @param revp
	 * @param spotDate
	 * @param holyDates
	 * @return
	 */
	public static List<RevpRow> convertRevpFieldToRow(Revp revp,Date spotDate,List<Date> holyDates){
		List<RevpRow> rows = new ArrayList<RevpRow>();
		convertRevpFieldToRow(rows,revp,spotDate,holyDates);
		return rows;
	}

	/**
	 * 方法已重载.将一条REVP记录的掉期点转换为list
	 * @param revpRows
	 * @param revp
	 * @param spotDate
	 * @param holyDates
	 */
	public static void convertRevpFieldToRow(List<RevpRow> revpRows,Revp revp,Date spotDate,List<Date> holyDates){
		Object period = null;
		Object rate = null;
		String mty = null;
		Date mtyDate = null;
		BigDecimal mtyRate = null;
		Date mtyModifyDate = null;
		if (null == revp) {
			throw new RuntimeException("需要转换的REVP实体对象为空");
		}

		try {
			// 添加即期汇率
			revpRows.add(createRevpRow(revp.getBr(), revp.getCcy(), revp.getTerms(), spotDate, "SPOT", spotDate, revp.getSpotrate8(), revp.getLstmntdate(),revp.getSpotrate8(),"CNY",spotDate));
			// 添加掉期点
			for (int i = 1; i < 40; i++) {
				period = getObjectValue(revp, "period" + i);
				rate = getObjectValue(revp, "rate" + i + "8");
				if (null == period || null == rate || StringUtils.trim(period.toString()).length() == 0) {
					continue;
				}
				mty = period.toString();
				mtyDate = FinancialDateUtils.calculateTenorDate(spotDate, mty);
				//当前日期如果是假日则顺延到下一工作日
				if(holyDates.contains(mtyDate)) {
					mtyModifyDate = FinancialDateUtils.calculateNextWorkDate(mtyDate, holyDates);
				}else {
					mtyModifyDate = mtyDate;
				}
				mtyRate = new BigDecimal(rate.toString());
				revpRows.add(createRevpRow(revp.getBr(), revp.getCcy(), revp.getTerms(), spotDate, mty, mtyDate, revp.getSpotrate8(), revp.getLstmntdate(),mtyRate,"CNY",mtyModifyDate));
			} // end for
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 创建汇率对象并赋值
	 * @param br
	 * @param ccy
	 * @param terms
	 * @param spotDate
	 * @param mty
	 * @param mtyDate
	 * @param rate
	 * @param lstmntdate
	 * @return
	 */
	public static RevpRow createRevpRow(String br,String ccy,String terms,Date spotDate,String mty,Date mtyDate,BigDecimal rate,Date lstmntdate,BigDecimal mytRate,String baseCcy,Date mtyModifyDate) {
		RevpRow revpRow = new RevpRow();
		
		revpRow.setBr(br);
		revpRow.setCcy(ccy);
		revpRow.setBaseCcy(baseCcy);
		revpRow.setTerms(terms);
		revpRow.setLstmntdate(lstmntdate);
		revpRow.setSpotDate(spotDate);
		revpRow.setMty(mty);
		revpRow.setMtyDate(mtyDate);
		revpRow.setMtyModifyDate(mtyModifyDate);
		revpRow.setSpotRate(rate);
		revpRow.setRate(rate);
		revpRow.setRateBp(mytRate.subtract(rate).setScale(8, RoundingMode.HALF_UP));
		
		return revpRow;
	}
	
	/**
	 * 获取对象属性值
	 * @param obj
	 * @param fieldName
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static Object getObjectValue(Object obj,String fieldName) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = null;
		Object value = null;
		field = obj.getClass().getDeclaredField(fieldName);
		// 设置是否允许访问，不是修改原来的访问权限修饰词。
		field.setAccessible(true);
		value = field.get(obj);
		field.setAccessible(false);
		return value;
	}
}
