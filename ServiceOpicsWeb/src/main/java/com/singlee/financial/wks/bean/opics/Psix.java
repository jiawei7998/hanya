package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Psix implements Serializable {
    private String br;

    private String product;

    private String type;

    private String dealno;

    private String seq;

    private String p1;

    private String p2;

    private String p3;

    private String p4;

    private String det;

    private String r1;

    private String r2;

    private String r3;

    private String r4;

    private String r5;

    private String r6;

    private String bif;

    private String suppayind;

    private String supconfind;

    private Date lstmntdte;
    
    

    public String getBr() {
		return br;
	}



	public void setBr(String br) {
		this.br = br;
	}



	public String getProduct() {
		return product;
	}



	public void setProduct(String product) {
		this.product = product;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getDealno() {
		return dealno;
	}



	public void setDealno(String dealno) {
		this.dealno = dealno;
	}



	public String getSeq() {
		return seq;
	}



	public void setSeq(String seq) {
		this.seq = seq;
	}



	public String getP1() {
		return p1;
	}



	public void setP1(String p1) {
		this.p1 = p1;
	}



	public String getP2() {
		return p2;
	}



	public void setP2(String p2) {
		this.p2 = p2;
	}



	public String getP3() {
		return p3;
	}



	public void setP3(String p3) {
		this.p3 = p3;
	}



	public String getP4() {
		return p4;
	}



	public void setP4(String p4) {
		this.p4 = p4;
	}



	public String getDet() {
		return det;
	}



	public void setDet(String det) {
		this.det = det;
	}



	public String getR1() {
		return r1;
	}



	public void setR1(String r1) {
		this.r1 = r1;
	}



	public String getR2() {
		return r2;
	}



	public void setR2(String r2) {
		this.r2 = r2;
	}



	public String getR3() {
		return r3;
	}



	public void setR3(String r3) {
		this.r3 = r3;
	}



	public String getR4() {
		return r4;
	}



	public void setR4(String r4) {
		this.r4 = r4;
	}



	public String getR5() {
		return r5;
	}



	public void setR5(String r5) {
		this.r5 = r5;
	}



	public String getR6() {
		return r6;
	}



	public void setR6(String r6) {
		this.r6 = r6;
	}



	public String getBif() {
		return bif;
	}



	public void setBif(String bif) {
		this.bif = bif;
	}



	public String getSuppayind() {
		return suppayind;
	}



	public void setSuppayind(String suppayind) {
		this.suppayind = suppayind;
	}



	public String getSupconfind() {
		return supconfind;
	}



	public void setSupconfind(String supconfind) {
		this.supconfind = supconfind;
	}



	public Date getLstmntdte() {
		return lstmntdte;
	}



	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}



	private static final long serialVersionUID = 1L;
}