package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Ccyp;
import com.singlee.financial.wks.mapper.opics.CcypMapper;
import com.singlee.financial.wks.service.opics.CcypService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CcypServiceImpl implements CcypService {

    @Resource
    private CcypMapper ccypMapper;

    @Override
    public int deleteByPrimaryKey(String br,String ccy1,String ccy2) {
        return ccypMapper.deleteByPrimaryKey(br,ccy1,ccy2);
    }

    @Override
    public int insert(Ccyp record) {
        return ccypMapper.insert(record);
    }

    @Override
    public int insertSelective(Ccyp record) {
        return ccypMapper.insertSelective(record);
    }

    @Override
    public Ccyp selectByPrimaryKey(String br,String ccy1,String ccy2) {
        return ccypMapper.selectByPrimaryKey(br,ccy1,ccy2);
    }

    @Override
    public int updateByPrimaryKeySelective(Ccyp record) {
        return ccypMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Ccyp record) {
        return ccypMapper.updateByPrimaryKey(record);
    }

}
