package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.cashflow.HolidayBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveContributeBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.FieldMapType;
import com.singlee.financial.wks.expand.SchduleRefix;
import com.singlee.financial.wks.expand.SwapCashFlowMethod;
import com.singlee.financial.wks.intfc.SwapService;
import com.singlee.financial.wks.service.cashflow.DiscountFactorFactory;
import com.singlee.financial.wks.service.cashflow.DiscountFactorFactory.DiscountFactorDateMethod;
import com.singlee.financial.wks.service.cashflow.SwapFactory;
import com.singlee.financial.wks.service.cashflow.YieldCurveContributeService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.FeeEntry;
import com.singlee.financial.wks.service.trans.RefixScheduleEntry;
import com.singlee.financial.wks.service.trans.SwapEntry;
import com.singlee.financial.wks.service.trans.SwapReverse;
import com.singlee.financial.wks.util.FinancialBeanHelper;
import com.singlee.financial.wks.util.FinancialDateUtils;
import com.singlee.financial.wks.util.FinancialMarketTransUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;

/**
 * 互换模块服务实现类
 *
 * @author xuqq 20200204
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SwapServiceImpl implements SwapService {

	@Autowired
	private SwdhService swdhService;
	@Autowired
	private SwdtService swdtService;
	@Autowired
	private SwdsService swdsService;
	@Autowired
	private McfpService mcfpService;
	@Autowired
	private PcixService pcixService;
	@Autowired
	private PrirService prirService;
	@Autowired
	private PsixService psixService;
	@Autowired
	private CnfqService cnfqService;
	@Autowired
	private NupdService nupdService;
	@Autowired
	private PmtqService pmtqSerice;
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private SwapEntry swapEntry;
	@Autowired
	private SwapReverse swapReverse;
	@Autowired
	private HldyService hldyService;
	@Autowired
	private YieldCurveContributeService yieldCurveContributeService;
	@Autowired
	private RhisService rhisService;
	@Autowired
	private FindService findService;
	@Autowired
	private FeeEntry feeEntry;
	@Autowired
	private RefixScheduleEntry refixScheduleEntry;

	private Logger logger = Logger.getLogger(SwapServiceImpl.class);

	// 获取利率互换交易信息
	@Override
	public SlOutBean saveIrsEntry(SwapWksBean irs) {
		// 更新SCHD对象中的首期,到期SCHDTYPE
		swapEntry.DataTrans(irs, irs.getItemBeanSwds());
		return addSwap(irs);
	}

	/**
	 * 增加Swap交易到OPICS系统
	 * 
	 * @param swap
	 * @return
	 */
	private SlOutBean addSwap(SwapWksBean swap) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SWAP-00000", "处理成功!");
		List<Swdt> swdhLeg = new ArrayList<Swdt>();// 声明
		List<Swds> swdsLst = new ArrayList<Swds>();// 收付息计划
		Swdh swdh = null;
		try {
			if (brpsService.isBatchRunning(swap.getInstId())) {
				throw new RuntimeException("系统跑批中请稍后操作!");
			}
			// 检查交易是否已存在
			swapEntry.DataIrsSwapCheck(swdhService, swap.getInstId(), swap.getTradeNo());
			// 1.交易要素检查
			swapEntry.DataIrsSwapCheck(swap);
			// 2.转换交易主体数据
			swdh = swapEntry.DataTrans(new Swdh());// 静态数据
			swapEntry.DataTrans(swdh, swap);// 互换交易信息
			swapEntry.DataTrans(swdh, brpsService);
			swapEntry.DataTrans(swdh, mcfpService);// 交易编号
			swdhService.insert(swdh);// 保存交易信息
			// 3.转换互换腿交易信息
			swapEntry.DataTrans(swdhLeg, swdh, swap);
			swdtService.batchInsert(swdhLeg);
			swapEntry.DataTrans(swdsLst, swdh, swdhLeg, swap);
			swdsService.batchInsert(swdsLst);
			// 4.往来帐信息
			List<Nupd> nupdLst = nupdService.builder(swdh, swdsLst);
			if (nupdLst.size() > 0) {
				nupdService.batchInsert(nupdLst);
			}
			// 6.对手方清算信息
			List<Pcix> pcixLst = pcixService.builder(swdh, swdhLeg, swap.getSettleAcct());
			pcixService.batchInsert(pcixLst);
			// 7.收款信息
			Prir prir = prirService.builder(swdh, swdhLeg, swap);
			prirService.insert(prir);
			// 8.付款信息
			Psix psix = psixService.builder(swdh, swdhLeg, swap);
			psixService.insert(psix);
			// 9.确认信息
			cnfqService.insert(cnfqService.bulider(swdh, prir, psix));
			// 10.FIND表信息
			findService.batchInsert(findService.bulider(swdh, swdhLeg));
			// 5.收付款信息
			pmtqSerice.batchInsert(pmtqSerice.builder(swdh, swdsLst, prir, psix));

			// 11.回填dealno
			swapEntry.DataTrans(swdh, mcfpService, new Mcfp());
			outBean.setClientNo(swdh.getDealno());
			outBean.setTellSeqNo(swap.getTradeNo());
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("互换模块交易录入错误", e);
			throw e;
		}
		logger.info("互换模块处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	/**
	 * 生成收益率曲线信息
	 * 
	 * @param yieldCurves
	 * @param br
	 * @param ccy
	 * @param yieldcurveName
	 */
	private void createYieldCurveBean(Map<String, YieldCurveBean> yieldCurves, String br, String ccy,
			String yieldcurveName) {
		// 收益率曲线对象
		YieldCurveBean yieldCurve = null;
		// OPICS曲线对象
		YieldCurveContributeBean contributeBean = null;
		// 曲线信息
		List<YieldCurveContributeBean> list = null;
		// 获取opics曲线
		contributeBean = new YieldCurveContributeBean();

		contributeBean.setBr(br);
		// 币种需要修改,要支持两条脚腿的币种,货币互换是两个币种
		contributeBean.setCcy(ccy);
		contributeBean.setYieldcurve(yieldcurveName);

		// 查询OPICS中的曲线信息
		list = yieldCurveContributeService.selectYieldCurve(contributeBean);

		if (null == list || list.size() == 0) {
			throw new RuntimeException(String.format("找不到曲线信息,币种[%s],曲线名称[%s]", ccy, yieldcurveName));
		}

		// 根据IRS对象中的曲线名称获取相应的信息
		yieldCurve = FinancialMarketTransUtils.createYieldCurve(list);
		// 添加两条腿的曲线信息
		yieldCurves.put(StringUtils.trimToEmpty(yieldCurve.getCurrency())
				+ StringUtils.trimToEmpty(yieldCurve.getYieldCurveName()), yieldCurve);
	}

	@Override
	public SlOutBean saveIrsWithSchedule(SwapWksBean irs) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SWAP-00000", "利率互换录入成功!");

		try {
			irs.setSwapType(OpicsConstant.SWAP.SWAPTYPE_I);
			// 创建现金流信息
			createCashFlow(irs);
			// 将SwapWksBean对象转换成OPICS对象并插库
			outBean = addSwap(irs);
			logger.debug(JSON.toJSON("输出=====>" + outBean));
		} catch (ParseException | IOException e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("利率互换交易录入错误", e);
		}
		return outBean;
	}

	@Override
	public SlOutBean RefixScheduleByHlyd(List<SchduleRefix> srLst) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SWAP-00009", "计划重订处理成功!");
		for (SchduleRefix refix : srLst) {
			logger.debug(JSON.toJSON("输出=====>" + refix));
			// 创建现金流信息
			try {
				// 查询OPICS需要调整的计划
				SwapWksBean irs = new SwapWksBean();
				irs.setFieldMapType(FieldMapType.OPICS);// 不转换值
				Swdh swdh = swdhService.selectByPrimaryKey(refix.getBr(), refix.getDealno(), "0", "S");
				List<Swdt> swdtLst = swdtService.selectByDealno(refix.getBr(), refix.getDealno());
				// 组装转换
				refixScheduleEntry.DataTrans(irs, swdh, swdtLst);
				// 产生现金流
				createCashFlow(irs);
				// 获取自己产生的现金流
				refixScheduleEntry.DataTrans(irs, swdh, swdsService);
			} catch (ParseException | IOException e) {
				outBean.setRetStatus(RetStatusEnum.F);
				outBean.setRetMsg(e.getMessage());
				logger.error("互换模块交易计划重订错误", e);
			}
		}
		return outBean;
	}

	@Override
	public SlOutBean saveCcsEntry(SwapWksBean ccs) {

		return null;
	}

	@Override
	public SlOutBean reverseIrs(SwapWksBean irs) {
		// 1：录入未跑批就冲销。2：录入跑批后冲销，2.1：未收息冲销，2.2：收过息冲销。3：到期冲销
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SWAP-REV-00000", "利率互换冲销成功!");
		Swdh swdh = null;
		try {
			// 1：录入未跑批就冲销
			// 1.修改swdh表
			swdh = swdhService.selectByTradeNo(irs.getInstId(), irs.getTradeNo(), "R");
			if (null == swdh) {
				throw new RuntimeException("成交编号[" + irs.getTradeNo() + "]未在OPICS中找到对应的交易");
			}

			// 去掉类空格
			FinancialBeanHelper.TrimBeanAttrValue(swdh);

			swapReverse.DataTrans(swdh, brpsService);
			swdhService.updateByPrimaryKey(swdh);
			// 删除大于新到期日的记录NUPD
			nupdService.deleteReverseByPrimaryKey(swdh.getBr(), swdh.getDealno(), swdh.getProduct(), swdh.getProdtype(),
					swdh.getRevdate(), swdh.getMatdate());
			// 3.2根据冲销时间节点新增NUPD
			// 去掉类空格
			List<Nupd> nupdLst = nupdService.selectReverseByPrimaryKey(swdh.getBr(), swdh.getDealno(),
					swdh.getProduct(), swdh.getProdtype());
			if (nupdLst.size() > 0) {
				for (int i = 0; i < nupdLst.size(); i++) {
					nupdLst.get(i).setSeq("R0");// 序号:默认R0
					nupdLst.get(i).setPostdate(swdh.getRevdate());// 当前账务日期
					nupdLst.get(i).setAmtupd(nupdLst.get(i).getAmtupd().negate());// 金额相反
				}
				nupdService.batchInsert(nupdLst);
			}
			// 7.收款信息
			Prir prir = prirService.selectByDealno(swdh.getBr(), swdh.getProduct(), swdh.getProdtype(),
					swdh.getDealno());
			// 8.付款信息
			Psix psix = psixService.selectByDealno(swdh.getBr(), swdh.getProduct(), swdh.getProdtype(),
					swdh.getDealno());
			// 4.2插入新的PMTQ记录
			List<Pmtq> pmtsList = pmtqSerice.builder(swdh, swdsService.selectByDealno(swdh.getBr(), swdh.getDealno()),
					prir, psix);
			pmtqSerice.batchInsert(pmtqSerice.builder(swdh.getRevdate(), pmtsList));
			// cnfq插入了一条数据
			Cnfq cnfq = cnfqService.bulider(swdh, prir, psix);
			if (cnfq != null) {
				cnfq.setCdate(swdh.getRevdate());//
				cnfq.setCtime(DateUtil.getCurrentTimeAsString());//
				cnfq.setTextstat("0");// 状态:0
				cnfq.setConftype("R");// 类型:R
				cnfqService.insert(cnfq);
			}
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("互换交易冲销错误", e);
			throw e;
		}
		outBean.setClientNo(swdh.getDealno());
		logger.info("互换冲销处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	@Override
	public SlOutBean reverseCcs(SwapWksBean ccs) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SWAP-00000", "货币互换冲销成功!");
		try {
			SlOutBean tmp = reverseIrs(ccs);
			if (null != tmp && !RetStatusEnum.S.equals(tmp.getRetStatus())) {
				throw new RuntimeException(tmp.getRetCode() + tmp.getRetMsg());
			}
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("货币互换交易冲销错误", e);
			throw e;
		}
		return outBean;
	}

	/**
	 * 创建交易现金流信息
	 * 
	 * @param swap
	 * @throws IOException
	 * @throws ParseException
	 */
	private void createCashFlow(SwapWksBean swap) throws ParseException, IOException {
		// 利率互换业务逻辑处理工厂
		SwapFactory swapFactory = new SwapFactory();
		// 计算贴现因子工厂对象
		DiscountFactorFactory discountFactorFactory = null;
		// 保存贴现因子工厂计算所需要的利率曲线
		Map<String, YieldCurveBean> yieldCurves = new HashMap<String, YieldCurveBean>();
		// 历史利率对象
		Rhis rhis = new Rhis();
		// 货币对应的假日信息
		Map<String, HolidayBean> holidayDates = null;
		// 计算现金流时所需要的历史利率数据
		Map<Date, String> resetRates = null;
		// 现金流计算方法
		SwapCashFlowMethod cashFlowMethod = null;

		// 根据枚举值到OPICS中查询相关表进行赋值
		swapEntry.DataTrans(swap, brpsService);

		// 节假日获取
		// 要根据两条腿的币种来获取币种,货币互换是两个币种
		if (OpicsConstant.SWAP.SWAPTYPE_I.equalsIgnoreCase(swap.getSwapType())) {
			holidayDates = FinancialMarketTransUtils.createHoliday(swap.getCcy(),
					hldyService.selectHldyListStr(swap.getCcy()));
		} else {
			holidayDates = FinancialMarketTransUtils.createHoliday(swap.getPayCcy(),
					hldyService.selectHldyListStr(swap.getPayCcy()));
			holidayDates.putAll(FinancialMarketTransUtils.createHoliday(swap.getRecCcy(),
					hldyService.selectHldyListStr(swap.getRecCcy())));
		}
		/**
		 * 进入生成现金流list之前，先进行到期日判断，如果在节假日之中，则向后顺延，如果顺延至下月，则改为当月最后一个工作日。
		 */
		Date mdate = FinancialDateUtils.calculateNextWorkDate(FinancialDateUtils.parse(swap.getMDate()), holidayDates.get(swap.getCcy()), "M");
		swap.setMDate(FinancialDateUtils.format(mdate,"yyyyMMdd"));
		logger.debug("holidayDates:" + holidayDates);

		// 获取浮动端市场利率
		rhis.setRatecode("L".equalsIgnoreCase(swap.getPayRateType()) ? swap.getPayRateCode() : swap.getRecRateCode());

		// 浮动端历史利率数据
		resetRates = FinancialMarketTransUtils.createRateHistory(rhisService.selectAllByRate(rhis));

		logger.debug("resetRates:" + resetRates);

		if (null == swap.getCashFlowMethod()) {
			cashFlowMethod = SwapCashFlowMethod.ResetAccrual;
		} else {
			cashFlowMethod = swap.getCashFlowMethod();
		}

		switch (cashFlowMethod) {
		case First:
			// 生成现金流数据和计算每一期的利息金额
			swapFactory.createCashFlow(swap, holidayDates, resetRates.get(DateUtil.parse(swap.getVDate())));
			break;
		case ResetAccrual:
			// 生成现金流数据和计算已确认的利息金额
			swapFactory.createCashFlow(swap, holidayDates, resetRates);
			break;
		case All:
			// 生成现金流数据和计算已确认的利息金额,并计算未重定部分现金流利息金额
			// 添加付端的曲线信息
			createYieldCurveBean(yieldCurves, swap.getInstId(), swap.getPayCcy(), swap.getPayYieldcurve());

			// 如果收端和付端的贴线因子不同则分别进行计算
			if (!swap.getPayYieldcurve().equalsIgnoreCase(swap.getRecYieldcurve())) {
				// 添加收端的曲线信息
				createYieldCurveBean(yieldCurves, swap.getInstId(), swap.getRecCcy(), swap.getRecYieldcurve());
			}

			// 计算贴现因子
			discountFactorFactory = DiscountFactorFactory.createDiscountFactorFactory(yieldCurves, swap.getBatchDate(),
					DiscountFactorDateMethod.SPOTDATE, holidayDates);
			// 生成现金流数据和计算已确认的利息金额,并计算未来现金流的预估金额
			swapFactory.createCashFlow(swap, discountFactorFactory, holidayDates, resetRates);
			break;
		default:
			throw new RuntimeException("利率互换/货币互换现金流计算方法[ " + cashFlowMethod + " ]不支持");
		}

		// 计算已过收付息日的轧差利息金额
		swapFactory.createCashFlow(swap);

		logger.debug("创建现金信息后的SwapWksBean对象:" + JSON.toJSON(swap));
		// 如果日期规则为M,到期日遇节假日顺延
		swapEntry.DataTrans(swap, holidayDates);
	}

	/**
	 * 1.先给定提前终止的要素派生提前终止现金流
	 * 2.cfets冲销估值=earlyAmount-netipay 金额
	 * 3.
	 * @param irs
	 * @return
	 */
	@Override
	public SlOutBean terminate(SwapWksBean irs) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SWAP-TERM-00000", "利率互换提前终止成功!");
		Swdh swdh = null;
		List<Swdt> swdts = null;
		List<Swds> swds = new ArrayList<Swds>();
		List<Swds> orsSwds = null;
		SwapWksBean etIrs = new SwapWksBean();

		try {
			if (brpsService.isBatchRunning(irs.getInstId())) {
				throw new RuntimeException("系统跑批中请稍后操作!");
			}
			// 转换终止交易对象
			swapEntry.DataTrans(irs, etIrs);
			// 根据新的到期日生成现金流信息
			createCashFlow(etIrs);
			// 查询头信息
			swdh = swdhService.selectByTradeNo(irs.getInstId(), irs.getOrgTradeNo(), "N");
			if (null == swdh) {
				throw new RuntimeException("未找到原交易流水[ " + irs.getTradeNo() + " ]记录");
			}
			// 查询原始交易明细
			swdts = swdtService.selectByDealno(StringUtils.trim(swdh.getBr()), StringUtils.trim(swdh.getDealno()));
			// 查询原始收付息计划
			orsSwds = swdsService.selectByDealno(swdh.getBr(), StringUtils.trim(swdh.getDealno()));
			// 转换处理
			swapEntry.DataTrans(swdh, swdts, orsSwds, irs, swds, etIrs);
			// 更新明细表
			swdtService.updateByTerminate(swdts);
			// 删除大于等于提前终止日期的现金流信息
			swdsService.deleteBySchdSeq(swds.get(0));
			swdsService.deleteBySchdSeq(swds.get(1));
			// 插入最后一期现金流信息
			swdsService.batchInsert(swds);
			// 添加罚金信息--费用信息
			feeEntry.saveEntry(swdh, swdts, swds);
			// 更新头表
			swdhService.updateByTerminate(swdh);
			// 更新pmtq数据
			pmtqSerice.insert(pmtqSerice.builder(swdh,swds.get(0)));
			// 返回信息
			outBean.setClientNo(StringUtils.trim(swdh.getDealno()));
			outBean.setTellSeqNo(StringUtils.trim(irs.getTradeNo()));
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("利率互换交易提前终止错误", e);
			throw new RuntimeException(e);
		}

		return outBean;
	}

	@Override
	public SlOutBean saveCcsWithSchedule(SwapWksBean ccs) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SWAP-00000", "货币互换录入成功!");

		try {
			ccs.setSwapType(OpicsConstant.SWAP.SWAPTYPE_C);

			// 创建现金流信息
			createCashFlow(ccs);
			// 将SwapWksBean对象转换成OPICS对象并插库
			SlOutBean tmp = addSwap(ccs);

			if (null == tmp) {
				outBean.setRetCode("SWAP-90001");
				throw new RuntimeException("货币互换录入返回信息为空");
			}

			outBean.setTellSeqNo(tmp.getTellSeqNo());

			// 处理货币互换本金清算信息
			if (null != tmp && !RetStatusEnum.S.equals(tmp.getRetStatus())) {
				outBean.setRetCode(tmp.getRetCode());
				throw new RuntimeException(tmp.getRetMsg());
			}
			outBean.setClientNo(tmp.getClientNo());
			outBean.setRetStatus(RetStatusEnum.S);
			logger.debug(JSON.toJSON("输出=====>" + outBean));
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetMsg(e.getMessage());
			logger.error("货币互换交易录入错误", e);
			throw new RuntimeException(e);
		}
		return outBean;
	}

}
