package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Revp;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface RevpMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy);

    int insert(Revp record);

    int insertSelective(Revp record);

    Revp selectByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy);
    
    List<Revp> selectAll();

    int updateByPrimaryKeySelective(Revp record);

    int updateByPrimaryKey(Revp record);
    
    /**
     * opics revp data
     * @return
     */
    List<Revp> selectImportData();
}