package com.singlee.financial.wks.mapper.cashflow;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Swdt;

public interface SchduleRefixMapper {

	/**
	 * 查询SWDT表
	 * 
	 * @param swdt
	 * @return
	 */
	List<Swdt> selectSwdtLst(Swdt swdt);
}
