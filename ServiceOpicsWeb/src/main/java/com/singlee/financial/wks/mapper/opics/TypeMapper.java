package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Type;
import org.apache.ibatis.annotations.Param;

public interface TypeMapper {
    int deleteByPrimaryKey(@Param("prodcode") String prodcode, @Param("type") String type);

    int insert(Type record);

    int insertSelective(Type record);

    Type selectByPrimaryKey(@Param("prodcode") String prodcode, @Param("type") String type);
    
    Type selectByProdAl(@Param("prodcode") String prodcode, @Param("al") String al);

    int updateByPrimaryKeySelective(Type record);

    int updateByPrimaryKey(Type record);
}