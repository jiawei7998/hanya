package com.singlee.financial.wks.service.cashflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.singlee.financial.wks.bean.cashflow.DiscountFactorBean;
import com.singlee.financial.wks.bean.cashflow.HolidayBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveSubBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveSubClosingRateBean;
import com.singlee.financial.wks.util.FinancialCalculationModel;
import com.singlee.financial.wks.util.FinancialDateUtils;

public class DiscountFactorFactory {
	
	private Logger log = Logger.getLogger(DiscountFactorFactory.class);
	
	/**
	 * 贴现开始日期
	 * @author chenxh
	 *
	 */
	public enum DiscountFactorDateMethod {

		/**
		 * 贴现到SPOTDATE
		 */
		SPOTDATE,
		/**
		 * 贴现到批量日期
		 */
		BATCHDATE
	}
	
	
	/**
	 * 存储由曲线构建的贴现因子信息
	 */
	private Map<String,List<DiscountFactorBean>> discountFactors = new HashMap<String,List<DiscountFactorBean>>();
	
	public Map<String,List<DiscountFactorBean>> getDiscountFactors() {
		return discountFactors;
	}
	
	/**
	 * 私有构造方法
	 */
	private DiscountFactorFactory() {
		
	}

	
	/**
	 * 创建贴现因子工厂对象
	 * @param yieldCurves
	 * @param batchDate
	 * @param disMethod
	 * @param holidayDates
	 * @return
	 */
	public static DiscountFactorFactory createDiscountFactorFactory(Map<String,YieldCurveBean> yieldCurves,Date batchDate,DiscountFactorDateMethod disMethod,Map<String, HolidayBean> holidayDates) {
		DiscountFactorFactory factory = new DiscountFactorFactory();
		factory.build(yieldCurves,batchDate,disMethod,holidayDates);
		return factory;
	}
	
	/**
	 * 方法已重载.构建相应币的的曲线及贴现因子
	 * @param yields
	 * @param batchDate
	 * @param spotDate
	 * @param disMethod
	 * @param holidayDates
	 */
	public void build(Map<String,YieldCurveBean> yields,Date batchDate,DiscountFactorDateMethod disMethod,Map<String, HolidayBean> holidayDates) {
		for (YieldCurveBean yieldCurve : yields.values()) {
			build(yieldCurve,batchDate,disMethod,holidayDates.get(yieldCurve.getCurrency()));
		}//end for
	}


	/**
	 * 根据曲线信息构建标准期限对应的贴现因子
	 */
	public void build(YieldCurveBean yieldCure,Date batchDate,DiscountFactorDateMethod disMethod,HolidayBean hldy) {
		Hashtable<String,BigDecimal> tenors_useCompoundDis = new Hashtable<String,BigDecimal>();
		Hashtable<String,List<String>> tenors_CompoundTenors =  new Hashtable<String,List<String>>();
		Date disDate = getStartCalculateDate(batchDate,disMethod,hldy.getHolidayDates());
		int size = 0;
		//清空贴现因子记录
		List<DiscountFactorBean> discountFactor = this.discountFactors.get(yieldCure.getCurrency().trim()+yieldCure.getYieldCurveName().trim());
		
		if(null == discountFactor) {
			discountFactor = new ArrayList<DiscountFactorBean>();
			this.discountFactors.put(yieldCure.getCurrency().trim()+yieldCure.getYieldCurveName().trim(), discountFactor);
		}else {
			discountFactor.clear();
		}
		
		//将曲线贡献率拼成一条完整曲线,并补充复利频率中缺失的期限点
		build(discountFactor,yieldCure,disDate,batchDate,hldy,tenors_CompoundTenors);
		
		//按期限进行排序
		sort(discountFactor);
		
		DiscountFactorBean dis = discountFactor.get(0);
		
		//添加批量日期的贴线因子记录
		discountFactor.add(createDiscountFactorBean(yieldCure.getCurrency(),yieldCure.getYieldCurveName(),batchDate,"O/N",dis.getRate(),disDate,hldy,dis.getBasis(),dis.getCompoudingFrequency(),"N"));
	
		//添加复利标准期限上需要的期限点
		
		
		//按期限进行排序
		sort(discountFactor);
		
		size = discountFactor.size();
		//按顺序从第一期开始计算贴现因子
		for (int i = 0; i < size; i++) {
			DiscountFactorBean c_dis = discountFactor.get(i);
			DiscountFactorBean pre_dis = null;
			DiscountFactorBean next_dis = null;
			
			//计算隐含标准复利期点利率
			if("Y".equalsIgnoreCase(c_dis.getIsInterpolation())) {
				pre_dis = getPreRate(discountFactor,i-1);
				next_dis = getNextRate(discountFactor, i+1);
				log.debug("当前期限点："+c_dis.getMaturity()+"上一个标准期限点："+pre_dis+"下一个标准期限点："+next_dis);
				c_dis.setRate(FinancialCalculationModel.calculateInterpolationRate(pre_dis.getRate(), next_dis.getRate(), pre_dis.getMaturityDate(), next_dis.getMaturityDate(), c_dis.getMaturityDate()));
			}
			
			if(isCompoundDiscountFactor(tenors_CompoundTenors,c_dis.getMaturity())) {
				//计算复利贴现因子
				//获取当前期限对应的上一个复利日期
				Date CompoundPrdDate = getCompoundPrdDate(discountFactor,c_dis,yieldCure);
				//调用复利贴现因子计算方法
				c_dis.setDiscountFactor(FinancialCalculationModel.calculateDiscountFactor(tenors_useCompoundDis,c_dis.getMaturity(),CompoundPrdDate, c_dis.getMaturityDate(), c_dis.getRate(),c_dis.getBasis(),tenors_CompoundTenors.get(c_dis.getMaturity()),disDate));
			}else {
				//计算单利贴现因子
				c_dis.setDiscountFactor(FinancialCalculationModel.calculateDiscountFactor(tenors_useCompoundDis,c_dis.getMaturity(),c_dis.getBatchDate(), c_dis.getMaturityDate(), c_dis.getRate(),c_dis.getBasis()));
			}//end if else
		}//end for
	}
	
	/**
	 * 获取上一个标准期限点对应的利率
	 * @param discountFactors
	 * @param index
	 * @return
	 */
	private DiscountFactorBean getPreRate(List<DiscountFactorBean> discountFactors,int index) {
		DiscountFactorBean pre = null;
		if(index<0) {
			return null;
		}
		
		pre = discountFactors.get(index);
		if("N".equalsIgnoreCase(pre.getIsInterpolation())) {
			return pre;
		}
		
		return getPreRate(discountFactors,index-1);
	}
	
	/**
	 * 获取下一个标准期限点对应的利率
	 * @param discountFactors
	 * @param index
	 * @return
	 */
	private DiscountFactorBean getNextRate(List<DiscountFactorBean> discountFactors,int index) {
		DiscountFactorBean next = null;
		
		if(index>discountFactors.size()) {
			return null;
		}
		next = discountFactors.get(index);
		
		if("N".equalsIgnoreCase(next.getIsInterpolation())) {
			return next;
		}
		return getNextRate(discountFactors,index+1);
	}
	
	/**
	 * 根据曲线构建所有标准期限点
	 * @param tenorsSet
	 * @param yieldCure
	 */
	private void build(List<DiscountFactorBean> discountFactors,YieldCurveBean yieldCure,Date disDate,Date batchDate,HolidayBean hldy,Hashtable<String,List<String>> tenors_CompoundTenors) {
		DiscountFactorBean discountFactorBean;
		
		//增加曲线中未定义的复利标准期限点
		for (YieldCurveSubBean subBean : yieldCure.getYieldCurveSubBeans()) {
			build(yieldCure.getCurrency(),yieldCure.getYieldCurveName(),subBean.getBasis(),subBean.getCompoudingFrequency(),batchDate,disDate,hldy,FinancialDateUtils.calculateTenorDays(getMaxTenor(subBean.getClosingRates())),discountFactors,0);
		}//end for
		
		//将系统中的曲线值添加到贴现因子对象中
		for (YieldCurveSubBean var : yieldCure.getYieldCurveSubBeans()) {
			for (Entry<String, YieldCurveSubClosingRateBean> subVar : var.getClosingRates().entrySet()) {
				discountFactorBean = createDiscountFactorBean(yieldCure.getCurrency(), yieldCure.getYieldCurveName(), batchDate,subVar.getKey(),subVar.getValue().getMidRate(), disDate, hldy,var.getBasis(),var.getCompoudingFrequency(),"N");
				int index = discountFactors.indexOf(discountFactorBean);
				if(index >=0 ) {
					discountFactorBean = discountFactors.get(index);
					discountFactorBean.setIsInterpolation("N");
					discountFactorBean.setRate(subVar.getValue().getMidRate());
				}else {
					discountFactors.add(discountFactorBean);
				}
			}//end for
		}//end for
		
		//计算每个复利期限点所需要的期限点
		for (DiscountFactorBean var : discountFactors) {
			//先计算当期对应的需要复利的期限点
			createCompoundTenors(tenors_CompoundTenors,yieldCure,var,discountFactors);
		}
	}
	
	/**
	 * 方法已重载.补齐曲线中未定义的复利期限点
	 * @param basis
	 * @param CompoudingFrequency
	 * @param closingRate
	 */
	private void build(String ccy,String yieldName,String basis,String compoudingFrequency,Date batchDate,Date beginDate,HolidayBean hldy,int maxTenorDays,List<DiscountFactorBean> discountFactors,int currentCompoudingFrequencyDays) {
		int compoudingFrequencyDays;
		int firstCompoudingFrequencyDays;
		DiscountFactorBean discountFactorBean;
		
		
		firstCompoudingFrequencyDays = FinancialDateUtils.calculateFirstCompoundDays(compoudingFrequency);
		
		if(0==currentCompoudingFrequencyDays) {
			compoudingFrequencyDays = firstCompoudingFrequencyDays;
		}else {
			compoudingFrequencyDays = currentCompoudingFrequencyDays;
		}
		
		log.debug("当前期限点对应的复利天数:"+compoudingFrequencyDays+",对应该条线上最大天数:"+maxTenorDays);
		
		//大于最大期限时退出
		if(compoudingFrequencyDays > maxTenorDays) {
			return;
		}
		
		//创建贴现因子对象
		discountFactorBean = createDiscountFactorBean(ccy, yieldName, batchDate, FinancialDateUtils.calculateCompoundTenor(compoudingFrequencyDays, compoudingFrequency),FinancialCalculationModel.ZERO, beginDate, hldy,basis,compoudingFrequency,"Y");
		log.debug("构建曲线中末设值的标准复利期限点:"+discountFactorBean);
		//添加不存在的期限点
		if(!discountFactors.contains(discountFactorBean)) {
			discountFactors.add(discountFactorBean);
		}

		build(ccy,yieldName,basis,compoudingFrequency,batchDate,beginDate,hldy,maxTenorDays,discountFactors,compoudingFrequencyDays+firstCompoudingFrequencyDays);
	}
	
	
	/**
	 * 获取最大的期限点
	 * @param closingRate
	 */
	public String getMaxTenor(Map<String, YieldCurveSubClosingRateBean> closingRate) {
        List<Map.Entry<String,YieldCurveSubClosingRateBean>> list = new ArrayList<Map.Entry<String,YieldCurveSubClosingRateBean>>(closingRate.entrySet());
        Collections.sort(list,new Comparator<Map.Entry<String,YieldCurveSubClosingRateBean>>() {
			@Override
			public int compare(Entry<String, YieldCurveSubClosingRateBean> o1, Entry<String, YieldCurveSubClosingRateBean> o2) {
				int result = 0;				
				int o1days = FinancialDateUtils.calculateTenorDays(o1.getKey());
				int o2days = FinancialDateUtils.calculateTenorDays(o2.getKey());
				result = (o1days == o2days ? 0 : o1days > o2days ? 1 : -1);
				return result;
			}
        });
        return list.get(list.size()-1).getKey();
	}
	
	/**
	 * 获取贴现开始日期
	 * @param batchDate
	 * @param spotDate
	 * @param disMethod
	 * @return
	 */
	private Date getStartCalculateDate(Date batchDate,DiscountFactorDateMethod disMethod,List<Date> hldys) {
		if(DiscountFactorDateMethod.SPOTDATE.equals(disMethod)) {
			return FinancialDateUtils.calculateSpotDate(batchDate, hldys, 2, 1);
		}else if(DiscountFactorDateMethod.BATCHDATE.equals(disMethod)) {
			return batchDate;
		}else {
			throw new RuntimeException("贴现日期方法不支持");
		}
	}
	
	/**
	 * 获取当前利率期限对应的上一个复利时间
	 * @param discountFactors
	 * @param tenor
	 * @param yieldCure
	 * @return
	 */
	private Date getCompoundPrdDate(List<DiscountFactorBean> discountFactors, DiscountFactorBean discountFactor, YieldCurveBean yieldCure) {
		// 计算复利频率对应的天数
		int firstCompoundDays = FinancialDateUtils.calculateFirstCompoundDays(discountFactor.getCompoudingFrequency());
		
		for (DiscountFactorBean discountFactorBean : discountFactors) {
			int tmp = FinancialDateUtils.calculateTenorDays(discountFactorBean.getMaturity());
			if(FinancialDateUtils.calculateTenorDays(discountFactor.getMaturity())-firstCompoundDays < firstCompoundDays) {
				if("A".equalsIgnoreCase(discountFactor.getCompoudingFrequency())) {
					log.debug(">>>>>>期限："+discountFactor.getMaturity()+",上一个复利期限1Y");
					return getFirstCompoundDate("1Y",discountFactors);
				}else if("S".equalsIgnoreCase(discountFactor.getCompoudingFrequency())) {
					log.debug(">>>>>>期限："+discountFactor.getMaturity()+",上一个复利期限6M");
					return getFirstCompoundDate("6M",discountFactors);
				}
			}else if(FinancialDateUtils.calculateTenorDays(discountFactor.getMaturity())-firstCompoundDays == tmp) {
				log.debug(">>>>>>期限："+discountFactor.getMaturity()+",上一个复利日期："+String.format("%1$tY-%1$tm-%1$td", discountFactorBean.getMaturityDate())+",上一个复利期限："+discountFactorBean.getMaturity());
				return discountFactorBean.getMaturityDate();
			}
		}//end for
		
		return null;
	}
	
	private Date getFirstCompoundDate(String tenor,List<DiscountFactorBean> discountFactors) {
		for (DiscountFactorBean discountFactorBean : discountFactors) {
			if(tenor.equalsIgnoreCase(discountFactorBean.getMaturity())) {
				return discountFactorBean.getMaturityDate();
			}//end if
		}//end for
		return null;
	}
	
	/**
	 * 生成当前期限所使用到的复利贴现
	 */
	private void createCompoundTenors(Hashtable<String,List<String>> tenors_CompoundTenors,YieldCurveBean yieldCure,DiscountFactorBean bean,List<DiscountFactorBean> discountFactors) {
		List<String> compoundTenors = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		
		//计算复利频率对应的天数
		int firstCompoundDays =FinancialDateUtils.calculateFirstCompoundDays(bean.getCompoudingFrequency());
		int tenorDays = FinancialDateUtils.calculateTenorDays(bean.getMaturity());
		sb.append("首期复利天数："+firstCompoundDays+",复利频率："+bean.getCompoudingFrequency()+",当前期限："+bean.getMaturity()+",当前期限天数："+tenorDays+",");
		
		//当前期限天数小于等于第一次复利天数时不处理
		if(tenorDays <= firstCompoundDays) {
			return;
		}
		
		//循环处理,添加当前期限点会使用的复利期限点
		for (DiscountFactorBean var : discountFactors) {
			int tmp = FinancialDateUtils.calculateTenorDays(var.getMaturity());
			if(tmp%firstCompoundDays == 0  && tmp < tenorDays) {
				sb.append("复利计算期限:"+var.getMaturity()+"="+tmp+"天");
				compoundTenors.add(var.getMaturity());
			}//end if
		}//end for
		
		log.debug("createCompoundTenors:"+sb.toString());
		
		tenors_CompoundTenors.put(bean.getMaturity(), compoundTenors);
	}
	

	/**
	 * 判断当前期限是否需要进行复利计算贴现因子
	 * @param yieldCure
	 * @param tenor
	 * @return
	 */
	private boolean isCompoundDiscountFactor(Hashtable<String,List<String>> tenors_CompoundTenors,String tenor) {
		if(null !=tenors_CompoundTenors.get(tenor) ) {
			return true;
		}
		return false;
	}
	
	/**
	 * 对标准贴现因子记录进行排序,按期限由短到长进行排序
	 * @param discountFactors
	 */
	public void sort(List<DiscountFactorBean> discountFactors) {
		Collections.sort(discountFactors,new Comparator<DiscountFactorBean>() {
			@Override
			public int compare(DiscountFactorBean o1, DiscountFactorBean o2) {
				int result = 0;				
				int o1days = FinancialDateUtils.calculateTenorDate(o1.getBatchDate(), o1.getMaturityDate());
				int o2days = FinancialDateUtils.calculateTenorDate(o2.getBatchDate(), o2.getMaturityDate());
				result = (o1days == o2days ? 0 : o1days > o2days ? 1 : -1);
				return result;
			}//end method
		});
	}
	
	/**
	 * 创建贴现因子对象
	 * @param ccy
	 * @param name
	 * @param batchDate
	 * @param tenor
	 * @param rate
	 * @param beginDate
	 * @param hldy
	 * @param basis
	 * @param compoudingFrequency
	 * @param isInterpolation
	 * @return
	 */
	private DiscountFactorBean createDiscountFactorBean(String ccy,String name,Date batchDate,String tenor,BigDecimal rate,Date beginDate,HolidayBean hldy,String basis,String compoudingFrequency,String isInterpolation) {
		Date calDate = beginDate;
		
		DiscountFactorBean bean = new DiscountFactorBean();
		bean.setCurrency(ccy);
		bean.setBatchDate(batchDate);
		bean.setMaturity(tenor);
		bean.setBasis(basis);
		bean.setCompoudingFrequency(compoudingFrequency);
		bean.setRate(rate);
		bean.setYieldCurveName(name);
		bean.setIsInterpolation(isInterpolation);
		
		if("O/N".equalsIgnoreCase(tenor)) {
			calDate = batchDate;
		}
		
		bean.setMaturityDate(FinancialDateUtils.calculateTenorDate(calDate,tenor));
		
		
		boolean isHas = hldy.getHolidayDates().contains(bean.getMaturityDate());
		
		//遇节假日顺延
		if(isHas) {
			bean.setMaturityDate(FinancialDateUtils.calculateNextWorkDate(bean.getMaturityDate(), hldy.getHolidayDates()));
		}//end if
		
		return bean;
	}
	
	
	/**
	 * 返回指定计息日期对应的远期贴现利率
	 * @param preStartDate
	 * @param preEndDate
	 * @param startDate
	 * @param endDate
	 * @param basisDays
	 * @param discountFactorKey 货币+收益率曲线
	 * @return
	 */
	public BigDecimal build(Date preStartDate,Date preEndDate,Date prePayDate,BigDecimal preRate,Date startDate,Date endDate,BigDecimal basisDays,String discountFactorKey) {
		BigDecimal dftc = null;		
		BigDecimal dftb = null;
		DiscountFactorBean preDis = null;
		DiscountFactorBean nextDis = null;
		
		preDis = getPreMaxDiscountFactor(endDate,discountFactorKey);
		
		log.debug("当期计划的上一期,getPreMaxDiscountFactor:" + preDis);
		
		nextDis = getNextMinDiscountFactor(endDate,discountFactorKey);
		
		log.debug("当期计划的下一期,getPreMaxDiscountFactor:" + nextDis);
		
		if(null == preDis) {
			preDis = nextDis;
		}
		
		if(endDate.equals(nextDis.getMaturityDate())) {
			return nextDis.getRate();
		}
		
		if(preDis.getMaturityDate().equals(nextDis.getMaturityDate())) {
			dftc = FinancialCalculationModel.ONE;
		}else {
			dftc = FinancialCalculationModel.calculateInterpolationDiscountFactor(preDis.getDiscountFactor(), nextDis.getDiscountFactor(), preDis.getBatchDate(), preDis.getMaturityDate(), nextDis.getBatchDate(), endDate, nextDis.getBatchDate(), nextDis.getMaturityDate());
		}
		
		
		log.debug("dftc:"+dftc);
		
		preDis = getPreMaxDiscountFactor(preEndDate,discountFactorKey);
		
		log.debug("上一期计划的上一个曲线期限点,getPreMaxDiscountFactor:" + preDis);
		
		nextDis = getNextMinDiscountFactor(preEndDate,discountFactorKey);
		
		log.debug("上一期计划的下一个曲线期限点,getPreMaxDiscountFactor:" + nextDis);
		
		if(null == preDis) {
			preDis = nextDis;
		}
		
		if(preDis.getMaturityDate().equals(nextDis.getMaturityDate())) {
			dftb = FinancialCalculationModel.ONE;
		}else {
			dftb = FinancialCalculationModel.calculateInterpolationDiscountFactor(preDis.getDiscountFactor(), nextDis.getDiscountFactor(), preDis.getBatchDate(), preDis.getMaturityDate(), preDis.getBatchDate(), preEndDate, nextDis.getBatchDate(), nextDis.getMaturityDate());
		}
		
		log.debug("dftb:"+dftb);
		
		if(dftb.equals(dftc) || (dftb.equals(FinancialCalculationModel.ONE) && dftc.equals(FinancialCalculationModel.ONE))) {
			return nextDis.getRate();
		}
		
		return FinancialCalculationModel.calculateForwardRate(dftb, dftc, basisDays, new BigDecimal(FinancialDateUtils.calculateTenorDate(startDate, endDate)));
	}
	
	/**
	 * 获取接近指定日期的最大一期贴现因子对象
	 * @param endDate
	 * @param discountFactorKey
	 * @return
	 */
	private DiscountFactorBean getPreMaxDiscountFactor(Date endDate,String discountFactorKey) {
		List<DiscountFactorBean> list = this.discountFactors.get(discountFactorKey);
		int size = list.size();
		DiscountFactorBean tmp = null;
		for (int i = size - 1; i >= 0; i--) {
			tmp = list.get(i);
			if (tmp.getMaturityDate().getTime() <= endDate.getTime()) {
				break;
			} // end if
		} // end for
		return tmp;
	}

	/**
	 * 获取接近指定日期的最小一期贴现因子对象
	 * @param intDate
	 * @param discountFactorKey
	 * @return
	 */
	private DiscountFactorBean getNextMinDiscountFactor(Date intDate,String discountFactorKey) {
		DiscountFactorBean tmp = null;
		for (DiscountFactorBean discountFactorBean : this.discountFactors.get(discountFactorKey)) {
			if (discountFactorBean.getMaturityDate().getTime() >= intDate.getTime()) {
				tmp = discountFactorBean;
				break;
			} // end if
		} // end for
		return tmp;
	}
}
