package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Drpi;
import org.apache.ibatis.annotations.Param;

public interface DrpiMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("prodtype") String prodtype, @Param("dealno") String dealno, @Param("seq") String seq, @Param("delrecind") String delrecind);

    int insert(Drpi record);

    int insertSelective(Drpi record);

    Drpi selectByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("prodtype") String prodtype, @Param("dealno") String dealno, @Param("seq") String seq, @Param("delrecind") String delrecind);

    int updateByPrimaryKeySelective(Drpi record);

    int updateByPrimaryKey(Drpi record);
}