package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.mapper.opics.NupdMapper;
import com.singlee.financial.wks.service.opics.NupdService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class NupdServiceImpl implements NupdService {

    @Resource
    private NupdMapper nupdMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br, String nos, String dealno, String seq, String product, String type,
        Date vdate) {
        return nupdMapper.deleteByPrimaryKey(br, nos, dealno, seq, product, type, vdate);
    }
    @Override
	public int deleteNupdByVdate(String br, String dealno, String product, String type, Date vdate) {
    	return nupdMapper.deleteNupdByVdate(br, dealno, product, type, vdate);
	}
    @Override
	public int deleteReverseByPrimaryKey(String br, String dealno, String product, String type, Date postdate,Date mdate) {
    	return nupdMapper.deleteReverseByPrimaryKey(br, dealno, product, type, postdate,mdate);
	}
    @Override
	public int deleteReverseFxdhByPrimaryKey(String br, String dealno, String product, String type, Date postdate) {
    	return nupdMapper.deleteReverseFxdhByPrimaryKey(br, dealno, product, type, postdate);
	}

    @Override
    public int insert(Nupd record) {
        return nupdMapper.insert(record);
    }

    @Override
    public int insertSelective(Nupd record) {
        return nupdMapper.insertSelective(record);
    }

    @Override
    public Nupd selectByPrimaryKey(String br, String nos, String dealno, String seq, String product, String type,
        Date vdate) {
        return nupdMapper.selectByPrimaryKey(br, nos, dealno, seq, product, type, vdate);
    }
    @Override
	public List<Nupd> selectReverseByPrimaryKey(String br, String dealno, String product, String type) {
    	return nupdMapper.selectReverseByPrimaryKey(br, dealno, product, type);
	}

    @Override
    public int updateByPrimaryKeySelective(Nupd record) {
        return nupdMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Nupd record) {
        return nupdMapper.updateByPrimaryKey(record);
    }

    @Override
    public void updateBatch(List<Nupd> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.NupdMapper.updateByPrimaryKeySelective", list);
    }

    @Override
    public void batchInsert(List<Nupd> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.NupdMapper.insert", list);
    }

    /**
     * 
     * @param br
     * @param nos
     * @param dealno
     * @param seq
     * @param product
     * @param type
     * @param vdate
     * @param postdate
     * @param cmne
     * @param amtupd
     * @param smeans
     * @return
     */
    private Nupd builder(String br, String nos, String dealno, String seq, String product, String type, Date vdate,
        Date postdate, String cmne, BigDecimal amtupd, String smeans) {

        if (!"NOS".equalsIgnoreCase(smeans)) {
            // 不是NOST清算路径,不插入NUPD表数据
            return null;
        }

        // 往来账对象
        Nupd nupd = new Nupd();
        // 部门
        nupd.setBr(br);
        // 清算路径
        nupd.setNos(nos);
        // 交易编号
        nupd.setDealno(dealno);
        // 交易序号
        nupd.setSeq(seq);
        // 产品代码
        nupd.setProduct(product);
        // 业务类型
        nupd.setType(type);
        // 收付款日期
        nupd.setVdate(vdate);
        // 处理日期
        nupd.setPostdate(postdate);
        // 交易对手编号
        nupd.setCmne(cmne);
        // 交易金额
        nupd.setAmtupd(amtupd);

        return nupd;
    }

    /**
     * 添加NUPD记录
     * 
     * @param nupds
     * @param nupd
     */
    public void addList(List<Nupd> nupds, Nupd nupd) {

        // 如果NUPD数据为空,则不进行处理
        if (null == nupd) {
            return;
        }

        nupds.add(nupd);

    }

    @Override
    public List<Nupd> builder(Dldt dl, List<Schd> schds) {
        List<Nupd> nupds = new ArrayList<Nupd>();
        /*
         * 根据现金流生成NUPD数据
         */
        for (Schd schd : schds) {
            addList(nupds,
                builder(dl.getBr(), schd.getSacct(), dl.getDealno(), schd.getSeq(), schd.getProduct(),
                    schd.getProdtype(), schd.getIpaydate(), dl.getBrprcindte(), dl.getCno(), schd.getTotpayamt(),
                    schd.getSmeans()));
        }
        return nupds;
    }

    @Override
	public List<Nupd> builder(Fxdh fxdh) {
		// 循环处理
		List<Nupd> nupds = new ArrayList<Nupd>();
		// 添加货币1往来账信息
		addList(nupds, builder(fxdh.getBr(), fxdh.getCcysacct(), fxdh.getDealno(), fxdh.getSeq(), fxdh.getProdcode(), fxdh.getProdtype(), fxdh.getVdate(), fxdh.getBrprcindte(), fxdh.getCust(),
				fxdh.getCcyamt(), fxdh.getCcysmeans()));
		// 添加货币2往来账信息
		addList(nupds, builder(fxdh.getBr(), fxdh.getCtrsacct(), fxdh.getDealno(), fxdh.getSeq(), fxdh.getProdcode(), fxdh.getProdtype(), fxdh.getVdate(), fxdh.getBrprcindte(), fxdh.getCust(),
				fxdh.getCtramt(), fxdh.getCtrsmeans()));

		return nupds;
	}

    @Override
    public List<Nupd> builder(Otdt otdt) {
        List<Nupd> nupds = new ArrayList<Nupd>();

        // 添加期权费
        addList(nupds,
            builder(otdt.getBr(), otdt.getPremsacct(), otdt.getDealno(), otdt.getSeq(), otdt.getProduct(),
                otdt.getProdtype(), otdt.getDealvdate(), otdt.getBrprcindte(), otdt.getCno(), otdt.getPremamt(),
                otdt.getPremsmeans()));

        // 行期时的现金流
        if ("1".equalsIgnoreCase(otdt.getExerind())) {
            // 添加投次收益,行权时生的的FEES交易

        }

        return nupds;
    }

    @Override
    public List<Nupd> builder(Swdh swdh, List<Swds> swdts) {
        List<Nupd> nupds = new ArrayList<Nupd>();
        /*
         * 根据现金流生成NUPD数据
         */
        if(swdts==null || swdts.size()==0) {
        	return nupds;
        }
        for (Swds swds : swdts) {
        	if(swds.getNetipay().compareTo(BigDecimal.ZERO)!=0) {
        		addList(nupds,
                        builder(swdh.getBr(), swdts.get(0).getIntccy(), swdh.getDealno(), swdh.getSeq(), swdh.getProduct(),
                        		swdh.getProdtype(), swds.getIntenddte(), swds.getIntstrtdte(), swdh.getCno(), swds.getNetipay(),
                        		swdts.get(0).getIntsmeans()));
        	}
            // 添加SWDT表中每条退所对应的SWDS对应计录,只添加固定端和已重定的?暂没分析出具体的逻辑,目前看可能是在批量中添加得,固息时需要添加
//            if ("B".equalsIgnoreCase(swdt.getCalcrule())) {
                // 添加每条腿的现金流信息,SWDT,SWDS对象引用关系没有定义好暂无法处理
//            } // END IF
        } // END FOR
        return nupds;
    }
	@Override
	public List<Nupd> builder(Spsh spsh) {
		List<Nupd> nupds = new ArrayList<Nupd>();

        // 添加现券买卖
        addList(nupds,
            builder(spsh.getBr(), spsh.getCcysacct(), spsh.getDealno(), spsh.getSeq(), spsh.getProduct(),
            		spsh.getProdtype(), spsh.getSettdate(), spsh.getBrprcindte(), spsh.getCno(), spsh.getSettamt(),
            		spsh.getCcysmeans()));

        return nupds;
	}
	@Override
	public List<Nupd> builder(Rdfh rdfh) {
		List<Nupd> nupds = new ArrayList<Nupd>();

        // 添加现券买卖
        addList(nupds,
            builder(rdfh.getBr(), rdfh.getCcysacct(), rdfh.getTransno(), rdfh.getSeq(), rdfh.getProduct(),
            		rdfh.getProdtype(), rdfh.getSettdate(), rdfh.getBrprcindte(), rdfh.getCno(), rdfh.getSettamt(),
            		rdfh.getCcysmeans()));

        return nupds;
	}
	@Override
	public List<Nupd> builder(Rprh rprh) {
		List<Nupd> nupds = new ArrayList<Nupd>();

        // 添加回购
        addList(nupds,
            builder(rprh.getBr(), rprh.getComccysacct(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
            		rprh.getProdtype(), rprh.getVdate(), rprh.getBrprcindte(), rprh.getCno(), rprh.getComprocdamt(),
            		rprh.getComccysmeans()));

        return nupds;
	}
	@Override
	public List<Nupd> builder(Sldh sldh) {
		List<Nupd> nupds = new ArrayList<Nupd>();

        // 添加债券借贷
        addList(nupds,
            builder(sldh.getBr(), sldh.getComccysacct(), sldh.getDealno(), sldh.getSeq(), sldh.getProduct(),
            		sldh.getProdtype(), sldh.getVdate(), sldh.getBrprcindte(), sldh.getCno(), sldh.getComprocamt(),
            		sldh.getComccysmeans()));

        return nupds;
	}
	@Override
	public void callSpDelrecNupdVdate(String br, String dealno, String seq, String product, String type, Date vdate,
			String nos) {
		nupdMapper.callSpDelrecNupdVdate(br, dealno, seq, product, type, vdate, nos);
	}
	@Override
	public void callSpAddrecNupd(String br, String nos, String dealno, String seq, String product, String type,
			String cmne, Date vdate, Date postdate, BigDecimal amtupd) {
		nupdMapper.callSpAddrecNupd(br, nos, dealno, seq, product, type, cmne, vdate, postdate, amtupd);
	}

}
