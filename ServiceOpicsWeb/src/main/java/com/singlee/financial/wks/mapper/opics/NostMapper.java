package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Nost;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface NostMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("nos") String nos);

    int insert(Nost record);

    int insertSelective(Nost record);

    Nost selectByPrimaryKey(@Param("br") String br, @Param("nos") String nos);

    List<String> selectNosByCust(@Param("br") String br, @Param("ccy") String ccy, @Param("bic") String bic);

    int updateByPrimaryKeySelective(Nost record);

    int updateByPrimaryKey(Nost record);

}