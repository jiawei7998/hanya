package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Acty;
public interface ActyService{


    int deleteByPrimaryKey(String acctngtype);

    int insert(Acty record);

    int insertSelective(Acty record);

    Acty selectByPrimaryKey(String acctngtype);

    int updateByPrimaryKeySelective(Acty record);

    int updateByPrimaryKey(Acty record);

}
