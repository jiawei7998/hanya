package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Port;
import com.singlee.financial.wks.mapper.opics.PortMapper;
import com.singlee.financial.wks.service.opics.PortService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PortServiceImpl implements PortService {

    @Resource
    private PortMapper portMapper;

    @Override
    public int deleteByPrimaryKey(String br,String portfolio) {
        return portMapper.deleteByPrimaryKey(br,portfolio);
    }

    @Override
    public int insert(Port record) {
        return portMapper.insert(record);
    }

    @Override
    public int insertSelective(Port record) {
        return portMapper.insertSelective(record);
    }

    @Override
    public Port selectByPrimaryKey(String br,String portfolio) {
        return portMapper.selectByPrimaryKey(br,portfolio);
    }

    @Override
    public int updateByPrimaryKeySelective(Port record) {
        return portMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Port record) {
        return portMapper.updateByPrimaryKey(record);
    }

}
