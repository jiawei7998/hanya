package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Seci implements Serializable {
    private String secid;

    private Date launchdate;

    private BigDecimal amtissued;

    private String marketissue;

    private String mtyreftype;

    private String seasonind;

    private String oid;

    private BigDecimal issueprice8;

    private String governlaw;

    private String sellrestrict;

    private String otherlegal;

    private String misc;

    private String financtype;

    private Date closepaydate;

    private String issuetype;

    private String taxcallind;

    private String taxcall;

    private String mindenom;

    private Date startinvdate;

    private Date syndcompleted;

    private BigDecimal withholdtax8;

    private String collateral;

    private Date fungdate;

    private String docustatus;

    private BigDecimal launchspread8;

    private String swapissueind;

    private String benchmark;

    private Date lstmntdte;

    private String bondform;

    private String drawerid;

    private static final long serialVersionUID = 1L;

    public String getSecid() {
        return secid;
    }

    public void setSecid(String secid) {
        this.secid = secid == null ? null : secid.trim();
    }

    public Date getLaunchdate() {
        return launchdate;
    }

    public void setLaunchdate(Date launchdate) {
        this.launchdate = launchdate;
    }

    public BigDecimal getAmtissued() {
        return amtissued;
    }

    public void setAmtissued(BigDecimal amtissued) {
        this.amtissued = amtissued;
    }

    public String getMarketissue() {
        return marketissue;
    }

    public void setMarketissue(String marketissue) {
        this.marketissue = marketissue == null ? null : marketissue.trim();
    }

    public String getMtyreftype() {
        return mtyreftype;
    }

    public void setMtyreftype(String mtyreftype) {
        this.mtyreftype = mtyreftype == null ? null : mtyreftype.trim();
    }

    public String getSeasonind() {
        return seasonind;
    }

    public void setSeasonind(String seasonind) {
        this.seasonind = seasonind == null ? null : seasonind.trim();
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public BigDecimal getIssueprice8() {
        return issueprice8;
    }

    public void setIssueprice8(BigDecimal issueprice8) {
        this.issueprice8 = issueprice8;
    }

    public String getGovernlaw() {
        return governlaw;
    }

    public void setGovernlaw(String governlaw) {
        this.governlaw = governlaw == null ? null : governlaw.trim();
    }

    public String getSellrestrict() {
        return sellrestrict;
    }

    public void setSellrestrict(String sellrestrict) {
        this.sellrestrict = sellrestrict == null ? null : sellrestrict.trim();
    }

    public String getOtherlegal() {
        return otherlegal;
    }

    public void setOtherlegal(String otherlegal) {
        this.otherlegal = otherlegal == null ? null : otherlegal.trim();
    }

    public String getMisc() {
        return misc;
    }

    public void setMisc(String misc) {
        this.misc = misc == null ? null : misc.trim();
    }

    public String getFinanctype() {
        return financtype;
    }

    public void setFinanctype(String financtype) {
        this.financtype = financtype == null ? null : financtype.trim();
    }

    public Date getClosepaydate() {
        return closepaydate;
    }

    public void setClosepaydate(Date closepaydate) {
        this.closepaydate = closepaydate;
    }

    public String getIssuetype() {
        return issuetype;
    }

    public void setIssuetype(String issuetype) {
        this.issuetype = issuetype == null ? null : issuetype.trim();
    }

    public String getTaxcallind() {
        return taxcallind;
    }

    public void setTaxcallind(String taxcallind) {
        this.taxcallind = taxcallind == null ? null : taxcallind.trim();
    }

    public String getTaxcall() {
        return taxcall;
    }

    public void setTaxcall(String taxcall) {
        this.taxcall = taxcall == null ? null : taxcall.trim();
    }

    public String getMindenom() {
        return mindenom;
    }

    public void setMindenom(String mindenom) {
        this.mindenom = mindenom == null ? null : mindenom.trim();
    }

    public Date getStartinvdate() {
        return startinvdate;
    }

    public void setStartinvdate(Date startinvdate) {
        this.startinvdate = startinvdate;
    }

    public Date getSyndcompleted() {
        return syndcompleted;
    }

    public void setSyndcompleted(Date syndcompleted) {
        this.syndcompleted = syndcompleted;
    }

    public BigDecimal getWithholdtax8() {
        return withholdtax8;
    }

    public void setWithholdtax8(BigDecimal withholdtax8) {
        this.withholdtax8 = withholdtax8;
    }

    public String getCollateral() {
        return collateral;
    }

    public void setCollateral(String collateral) {
        this.collateral = collateral == null ? null : collateral.trim();
    }

    public Date getFungdate() {
        return fungdate;
    }

    public void setFungdate(Date fungdate) {
        this.fungdate = fungdate;
    }

    public String getDocustatus() {
        return docustatus;
    }

    public void setDocustatus(String docustatus) {
        this.docustatus = docustatus == null ? null : docustatus.trim();
    }

    public BigDecimal getLaunchspread8() {
        return launchspread8;
    }

    public void setLaunchspread8(BigDecimal launchspread8) {
        this.launchspread8 = launchspread8;
    }

    public String getSwapissueind() {
        return swapissueind;
    }

    public void setSwapissueind(String swapissueind) {
        this.swapissueind = swapissueind == null ? null : swapissueind.trim();
    }

    public String getBenchmark() {
        return benchmark;
    }

    public void setBenchmark(String benchmark) {
        this.benchmark = benchmark == null ? null : benchmark.trim();
    }

    public Date getLstmntdte() {
        return lstmntdte;
    }

    public void setLstmntdte(Date lstmntdte) {
        this.lstmntdte = lstmntdte;
    }

    public String getBondform() {
        return bondform;
    }

    public void setBondform(String bondform) {
        this.bondform = bondform == null ? null : bondform.trim();
    }

    public String getDrawerid() {
        return drawerid;
    }

    public void setDrawerid(String drawerid) {
        this.drawerid = drawerid == null ? null : drawerid.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", secid=").append(secid);
        sb.append(", launchdate=").append(launchdate);
        sb.append(", amtissued=").append(amtissued);
        sb.append(", marketissue=").append(marketissue);
        sb.append(", mtyreftype=").append(mtyreftype);
        sb.append(", seasonind=").append(seasonind);
        sb.append(", oid=").append(oid);
        sb.append(", issueprice8=").append(issueprice8);
        sb.append(", governlaw=").append(governlaw);
        sb.append(", sellrestrict=").append(sellrestrict);
        sb.append(", otherlegal=").append(otherlegal);
        sb.append(", misc=").append(misc);
        sb.append(", financtype=").append(financtype);
        sb.append(", closepaydate=").append(closepaydate);
        sb.append(", issuetype=").append(issuetype);
        sb.append(", taxcallind=").append(taxcallind);
        sb.append(", taxcall=").append(taxcall);
        sb.append(", mindenom=").append(mindenom);
        sb.append(", startinvdate=").append(startinvdate);
        sb.append(", syndcompleted=").append(syndcompleted);
        sb.append(", withholdtax8=").append(withholdtax8);
        sb.append(", collateral=").append(collateral);
        sb.append(", fungdate=").append(fungdate);
        sb.append(", docustatus=").append(docustatus);
        sb.append(", launchspread8=").append(launchspread8);
        sb.append(", swapissueind=").append(swapissueind);
        sb.append(", benchmark=").append(benchmark);
        sb.append(", lstmntdte=").append(lstmntdte);
        sb.append(", bondform=").append(bondform);
        sb.append(", drawerid=").append(drawerid);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Seci other = (Seci) that;
        return (this.getSecid() == null ? other.getSecid() == null : this.getSecid().equals(other.getSecid()))
            && (this.getLaunchdate() == null ? other.getLaunchdate() == null : this.getLaunchdate().equals(other.getLaunchdate()))
            && (this.getAmtissued() == null ? other.getAmtissued() == null : this.getAmtissued().equals(other.getAmtissued()))
            && (this.getMarketissue() == null ? other.getMarketissue() == null : this.getMarketissue().equals(other.getMarketissue()))
            && (this.getMtyreftype() == null ? other.getMtyreftype() == null : this.getMtyreftype().equals(other.getMtyreftype()))
            && (this.getSeasonind() == null ? other.getSeasonind() == null : this.getSeasonind().equals(other.getSeasonind()))
            && (this.getOid() == null ? other.getOid() == null : this.getOid().equals(other.getOid()))
            && (this.getIssueprice8() == null ? other.getIssueprice8() == null : this.getIssueprice8().equals(other.getIssueprice8()))
            && (this.getGovernlaw() == null ? other.getGovernlaw() == null : this.getGovernlaw().equals(other.getGovernlaw()))
            && (this.getSellrestrict() == null ? other.getSellrestrict() == null : this.getSellrestrict().equals(other.getSellrestrict()))
            && (this.getOtherlegal() == null ? other.getOtherlegal() == null : this.getOtherlegal().equals(other.getOtherlegal()))
            && (this.getMisc() == null ? other.getMisc() == null : this.getMisc().equals(other.getMisc()))
            && (this.getFinanctype() == null ? other.getFinanctype() == null : this.getFinanctype().equals(other.getFinanctype()))
            && (this.getClosepaydate() == null ? other.getClosepaydate() == null : this.getClosepaydate().equals(other.getClosepaydate()))
            && (this.getIssuetype() == null ? other.getIssuetype() == null : this.getIssuetype().equals(other.getIssuetype()))
            && (this.getTaxcallind() == null ? other.getTaxcallind() == null : this.getTaxcallind().equals(other.getTaxcallind()))
            && (this.getTaxcall() == null ? other.getTaxcall() == null : this.getTaxcall().equals(other.getTaxcall()))
            && (this.getMindenom() == null ? other.getMindenom() == null : this.getMindenom().equals(other.getMindenom()))
            && (this.getStartinvdate() == null ? other.getStartinvdate() == null : this.getStartinvdate().equals(other.getStartinvdate()))
            && (this.getSyndcompleted() == null ? other.getSyndcompleted() == null : this.getSyndcompleted().equals(other.getSyndcompleted()))
            && (this.getWithholdtax8() == null ? other.getWithholdtax8() == null : this.getWithholdtax8().equals(other.getWithholdtax8()))
            && (this.getCollateral() == null ? other.getCollateral() == null : this.getCollateral().equals(other.getCollateral()))
            && (this.getFungdate() == null ? other.getFungdate() == null : this.getFungdate().equals(other.getFungdate()))
            && (this.getDocustatus() == null ? other.getDocustatus() == null : this.getDocustatus().equals(other.getDocustatus()))
            && (this.getLaunchspread8() == null ? other.getLaunchspread8() == null : this.getLaunchspread8().equals(other.getLaunchspread8()))
            && (this.getSwapissueind() == null ? other.getSwapissueind() == null : this.getSwapissueind().equals(other.getSwapissueind()))
            && (this.getBenchmark() == null ? other.getBenchmark() == null : this.getBenchmark().equals(other.getBenchmark()))
            && (this.getLstmntdte() == null ? other.getLstmntdte() == null : this.getLstmntdte().equals(other.getLstmntdte()))
            && (this.getBondform() == null ? other.getBondform() == null : this.getBondform().equals(other.getBondform()))
            && (this.getDrawerid() == null ? other.getDrawerid() == null : this.getDrawerid().equals(other.getDrawerid()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getSecid() == null) ? 0 : getSecid().hashCode());
        result = prime * result + ((getLaunchdate() == null) ? 0 : getLaunchdate().hashCode());
        result = prime * result + ((getAmtissued() == null) ? 0 : getAmtissued().hashCode());
        result = prime * result + ((getMarketissue() == null) ? 0 : getMarketissue().hashCode());
        result = prime * result + ((getMtyreftype() == null) ? 0 : getMtyreftype().hashCode());
        result = prime * result + ((getSeasonind() == null) ? 0 : getSeasonind().hashCode());
        result = prime * result + ((getOid() == null) ? 0 : getOid().hashCode());
        result = prime * result + ((getIssueprice8() == null) ? 0 : getIssueprice8().hashCode());
        result = prime * result + ((getGovernlaw() == null) ? 0 : getGovernlaw().hashCode());
        result = prime * result + ((getSellrestrict() == null) ? 0 : getSellrestrict().hashCode());
        result = prime * result + ((getOtherlegal() == null) ? 0 : getOtherlegal().hashCode());
        result = prime * result + ((getMisc() == null) ? 0 : getMisc().hashCode());
        result = prime * result + ((getFinanctype() == null) ? 0 : getFinanctype().hashCode());
        result = prime * result + ((getClosepaydate() == null) ? 0 : getClosepaydate().hashCode());
        result = prime * result + ((getIssuetype() == null) ? 0 : getIssuetype().hashCode());
        result = prime * result + ((getTaxcallind() == null) ? 0 : getTaxcallind().hashCode());
        result = prime * result + ((getTaxcall() == null) ? 0 : getTaxcall().hashCode());
        result = prime * result + ((getMindenom() == null) ? 0 : getMindenom().hashCode());
        result = prime * result + ((getStartinvdate() == null) ? 0 : getStartinvdate().hashCode());
        result = prime * result + ((getSyndcompleted() == null) ? 0 : getSyndcompleted().hashCode());
        result = prime * result + ((getWithholdtax8() == null) ? 0 : getWithholdtax8().hashCode());
        result = prime * result + ((getCollateral() == null) ? 0 : getCollateral().hashCode());
        result = prime * result + ((getFungdate() == null) ? 0 : getFungdate().hashCode());
        result = prime * result + ((getDocustatus() == null) ? 0 : getDocustatus().hashCode());
        result = prime * result + ((getLaunchspread8() == null) ? 0 : getLaunchspread8().hashCode());
        result = prime * result + ((getSwapissueind() == null) ? 0 : getSwapissueind().hashCode());
        result = prime * result + ((getBenchmark() == null) ? 0 : getBenchmark().hashCode());
        result = prime * result + ((getLstmntdte() == null) ? 0 : getLstmntdte().hashCode());
        result = prime * result + ((getBondform() == null) ? 0 : getBondform().hashCode());
        result = prime * result + ((getDrawerid() == null) ? 0 : getDrawerid().hashCode());
        return result;
    }
}