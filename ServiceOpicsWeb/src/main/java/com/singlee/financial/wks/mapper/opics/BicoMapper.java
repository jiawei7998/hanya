package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Bico;

public interface BicoMapper {
    /**
     * delete by primary key
     * @param bic primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String bic);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Bico record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Bico record);

    /**
     * select by primary key
     * @param bic primary key
     * @return object by primary key
     */
    Bico selectByPrimaryKey(String bic);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Bico record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Bico record);
}