package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Slda;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.bean.opics.Tpos;
import com.singlee.financial.wks.mapper.opics.TposMapper;
import com.singlee.financial.wks.service.opics.TposService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;


@Service
public class TposServiceImpl implements TposService {

    @Resource
    private TposMapper tposMapper;

    @Override
    public int deleteByPrimaryKey(String br,String cost,String ccy,String invtype,String port,String secid) {
        return tposMapper.deleteByPrimaryKey(br,cost,ccy,invtype,port,secid);
    }

    @Override
    public int insert(Tpos record) {
        return tposMapper.insert(record);
    }

    @Override
    public int insertSelective(Tpos record) {
        return tposMapper.insertSelective(record);
    }

    @Override
    public Tpos selectByPrimaryKey(String br,String cost,String ccy,String invtype,String port,String secid) {
        return tposMapper.selectByPrimaryKey(br,cost,ccy,invtype,port,secid);
    }
    @Override
	public Tpos selectByPrimaryKeySpsh(String br, String cost, String ccy, String invtype, String port, String secid,
			String product, String prodtype) {
    	return tposMapper.selectByPrimaryKeySpsh(br,cost,ccy,invtype,port,secid,product,prodtype);
	}

    @Override
    public int updateByPrimaryKeySelective(Tpos record) {
        return tposMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Tpos record) {
        return tposMapper.updateByPrimaryKey(record);
    }

	@Override
	public Tpos builder(Spsh spsh) {
		Tpos tpos = DataTrans(new Tpos());
		tpos.setBr(spsh.getBr());
		tpos.setCost(spsh.getCost());
		tpos.setCcy(spsh.getCcy());
		tpos.setInvtype(spsh.getInvtype());//投资类型
		tpos.setPort(spsh.getPort());
		tpos.setSecid(spsh.getSecid());
		tpos.setProduct(spsh.getProduct());//产品
		tpos.setProdtype(spsh.getProdtype());//产品类型
		return tpos;
	}
	@Override
	public Tpos builder(Slda slda) {
		Tpos tpos = DataTrans(new Tpos());
		tpos.setBr(slda.getBr());
		tpos.setCost(slda.getCost());
		tpos.setCcy(slda.getCcy());
		tpos.setInvtype(slda.getInvtype());//投资类型
		tpos.setPort(slda.getPort());
		tpos.setSecid(slda.getSecid());
		tpos.setProduct(slda.getProduct());//产品
		tpos.setProdtype(slda.getProdtype());//产品类型
		return tpos;
	}
	
	private Tpos DataTrans(Tpos tpos) {
		tpos.setAccroutst(BigDecimal.ZERO);//上个付息日到当前日期的利息总收入
		tpos.setAccroutstyst(BigDecimal.ZERO);//昨日上付息日截至当前的利息收入
		tpos.setAmortnextmnth(BigDecimal.ZERO);//下个月摊销
		tpos.setPmedscincexp(BigDecimal.ZERO);//上个月末已摊销累计金额
		tpos.setPyedscincexp(BigDecimal.ZERO);//上年末的已摊销累计金额
		tpos.setTdydscincexp(BigDecimal.ZERO);//已摊销累计金额
		tpos.setYstdscincexp(BigDecimal.ZERO);//昨天的已摊销累计金额
		tpos.setAvgcost(BigDecimal.ZERO);//平均成本
		tpos.setAvgcostyst(BigDecimal.ZERO);//昨天平均成本
		tpos.setIntnextmonth(BigDecimal.ZERO);//月利息金额
		tpos.setPmeintincexp(BigDecimal.ZERO);//上个月末已计利息累计金额
		tpos.setPyeintincexp(BigDecimal.ZERO);//上年末已计利息累计金额
		tpos.setTdyintincexp(BigDecimal.ZERO);//已计利息累计金额
		tpos.setYstintincexp(BigDecimal.ZERO);//昨日已计利息累计金额
		tpos.setInvadyamt(BigDecimal.ZERO);//库存调整
		tpos.setMktval(BigDecimal.ZERO);//市场价值
		tpos.setMktvalyst(BigDecimal.ZERO);//昨天市场价值
		tpos.setPrinamt(BigDecimal.ZERO);//面值金额
		tpos.setPrinamtyst(BigDecimal.ZERO);//昨日面值金额
		tpos.setPurchavgcost(BigDecimal.ZERO);//购买平均成本
		tpos.setPurchqty(BigDecimal.ZERO);//购买数量
		tpos.setQty(BigDecimal.ZERO);//数量
		tpos.setQtyyst(BigDecimal.ZERO);//昨天数量
		tpos.setSaleavgcost(BigDecimal.ZERO);//销售平均成本
		tpos.setSaleqty(BigDecimal.ZERO);//销售数量
		tpos.setSettavgcost(BigDecimal.ZERO);//摊余成本
		tpos.setSettavgcostyst(BigDecimal.ZERO);//昨日摊余成本
		tpos.setSettqty(BigDecimal.ZERO);//结算数量
		tpos.setSettqtyyst(BigDecimal.ZERO);//昨日结算数量
		tpos.setUnamortamt(BigDecimal.ZERO);//未摊销金额
		tpos.setUnamortamtyst(BigDecimal.ZERO);//未摊销金额
		tpos.setTdysalegain(BigDecimal.ZERO);//今日销售/收益
		tpos.setYstsalegain(BigDecimal.ZERO);//昨天销售/收益
		tpos.setPmesalegain(BigDecimal.ZERO);//销售收入上个月
		tpos.setPyesalegain(BigDecimal.ZERO);//销售收入上一年度结束
		tpos.setTdymtm(BigDecimal.ZERO);//今天去市场
		tpos.setYstmtm(BigDecimal.ZERO);//昨天上市
		tpos.setPmemtm(BigDecimal.ZERO);//标注市场上个月底
		tpos.setPyemtm(BigDecimal.ZERO);//上一年度市值金额
		tpos.setClsgprice8(BigDecimal.ZERO);//实际利率
		tpos.setIntpayamt(BigDecimal.ZERO);//利息支付金额
		tpos.setTdyuglamt(BigDecimal.ZERO);//今天未实现的增益/损失金额
		tpos.setYstuglamt(BigDecimal.ZERO);//昨天未实现的收益/损失金额
		tpos.setPmeuglamt(BigDecimal.ZERO);//上个月结束未实现的增益/损失
		tpos.setPyeuglamt(BigDecimal.ZERO);//上一年度未实现的收益/亏损
		tpos.setTdaccroutst(BigDecimal.ZERO);//交易日应计未偿还金额
		tpos.setTdaccroutstyst(BigDecimal.ZERO);//交易日应计未偿还金额
		tpos.setSettpurchintamt(BigDecimal.ZERO);//结算购置利息金额
		tpos.setSettpurchintamtyst(BigDecimal.ZERO);//结算购买利息金额昨天
		tpos.setTdsettpurchintamt(BigDecimal.ZERO);//交易日购买的利息金额
		tpos.setTdsettpurchintamtyst(BigDecimal.ZERO);//交易日期采购金额昨天
		tpos.setTdyinfadjamt(BigDecimal.ZERO);//今日通胀调整数
		tpos.setYstinfadjamt(BigDecimal.ZERO);//昨天通胀调整金额
		tpos.setPmeinfadjamt(BigDecimal.ZERO);//上个月通胀调整结束
		tpos.setPyeinfadjamt(BigDecimal.ZERO);//去年通货膨胀调整数
		tpos.setAdjnomamt(BigDecimal.ZERO);//调整名义金额
		tpos.setAdjnomamtyst(BigDecimal.ZERO);//调整昨天名义金额
		tpos.setTdprinamt(BigDecimal.ZERO);//交易本日金额
		tpos.setTdprinamtyst(BigDecimal.ZERO);//昨天交易本日金额
		tpos.setTdunamortamt(BigDecimal.ZERO);//交易日期未摊销
		tpos.setTdunamortamtyst(BigDecimal.ZERO);//昨天交易日期未平摊金额
		tpos.setTdbookvalue(BigDecimal.ZERO);//交易日期账面价值
		tpos.setTdbookvalueyst(BigDecimal.ZERO);//昨天交易日期数值
		tpos.setTdmarketvalue(BigDecimal.ZERO);//交易日期市场价值
		tpos.setTdmarketvalueyst(BigDecimal.ZERO);//昨日交易日市值金额
		tpos.setAdjtdbookval(BigDecimal.ZERO);//调整交易日期数值
		tpos.setAdjtdbookvalyst(BigDecimal.ZERO);//调整昨天交易日期
		tpos.setAdjtdmtmpye(BigDecimal.ZERO);//ADJ估值年
		tpos.setAdjtdmtmpme(BigDecimal.ZERO);//ADJ估值月
		tpos.setAdjtdmtm(BigDecimal.ZERO);//ADJ估值
		tpos.setAdjtdmtmyst(BigDecimal.ZERO);//ADJ估值昨日
		tpos.setTdrgladjpye(BigDecimal.ZERO);//实现上年度交易日期收益调整
		tpos.setTdrgladjpme(BigDecimal.ZERO);//实现上个月交易日期收益调整
		tpos.setTdrgladj(BigDecimal.ZERO);//实现交易日收益调整
		tpos.setTdrgladjyst(BigDecimal.ZERO);//实现昨天交易日期收益调整
		tpos.setAdjtdmktval(BigDecimal.ZERO);//调整交易日期市值
		tpos.setAdjtdmktvalyst(BigDecimal.ZERO);//调整昨天交易日市值

		return tpos;
	}

	

}
