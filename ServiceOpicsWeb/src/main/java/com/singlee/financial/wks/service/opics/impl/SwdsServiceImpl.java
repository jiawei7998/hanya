package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Swds;
import com.singlee.financial.wks.mapper.opics.SwdsMapper;
import com.singlee.financial.wks.service.opics.SwdsService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class SwdsServiceImpl implements SwdsService {

    @Resource
    BatchDao batchDao;
    @Resource
    private SwdsMapper swdsMapper;

    @Override
    public int deleteByPrimaryKey(String br, String dealno, String seq, String dealind, String product, String prodtype, Date intenddte, String schdseq, String schdtype) {
        return swdsMapper.deleteByPrimaryKey(br,dealno,seq,dealind,product,prodtype,intenddte,schdseq,schdtype);
    }
    
    @Override
    public int deleteByGtSeq(String br, String dealno, String seq, String dealind, String product, String prodtype, String schdseq, String schdtype) {
        return swdsMapper.deleteByGtSeq(br,dealno,seq,dealind,product,prodtype,schdseq,schdtype);
    }
    

    @Override
    public int insert(Swds record) {
        return swdsMapper.insert(record);
    }

    @Override
    public int insertSelective(Swds record) {
        return swdsMapper.insertSelective(record);
    }

    @Override
    public Swds selectByPrimaryKey(String br,String dealno,String seq,String dealind,String product,String prodtype,Date intenddte,String schdseq,String schdtype) {
        return swdsMapper.selectByPrimaryKey(br,dealno,seq,dealind,product,prodtype,intenddte,schdseq,schdtype);
    }
    
    @Override
    public Swds selectBySeq(String br,String dealno,String seq,String dealind,String product,String prodtype,String schdseq,String schdtype) {
        return swdsMapper.selectBySeq(br,dealno,seq,dealind,product,prodtype,schdseq,schdtype);
    }
    
    @Override
    public Swds selectBySchdtype(String br, String dealno, String seq, String dealind, String product, String prodtype, String schdtype) {
        return swdsMapper.selectBySchdtype(br,dealno,seq,dealind,product,prodtype,schdtype);
    }

    @Override
    public int updateByPrimaryKeySelective(Swds record) {
        return swdsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateBySeqSelective(Swds record) {
        return swdsMapper.updateBySeqSelective(record);
    }
    @Override
    public int updateByPrimaryKey(Swds record) {
        return swdsMapper.updateByPrimaryKey(record);
    }
    
    @Override
	public void batchInsert(List<Swds> list) {
		if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.SwdsMapper.insert", list);
	}

	@Override
	public List<Swds> selectByDealno(String br, String dealno) {
		return swdsMapper.selectByDealno(br, dealno);
	}

	@Override
	public int deleteBySchdSeq(Swds record) {
		return swdsMapper.deleteBySchdSeq(StringUtils.trim(record.getBr()), StringUtils.trim(record.getDealno()),StringUtils.trim(record.getFixfloatind())
				, StringUtils.trim(record.getSchdseq()));
	}

}
