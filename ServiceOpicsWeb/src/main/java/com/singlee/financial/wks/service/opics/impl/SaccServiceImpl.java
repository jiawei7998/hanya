package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Sacc;
import com.singlee.financial.wks.mapper.opics.SaccMapper;
import com.singlee.financial.wks.service.opics.SaccService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SaccServiceImpl implements SaccService {

    @Resource
    private SaccMapper saccMapper;

    @Override
    public int deleteByPrimaryKey(String br,String accountno) {
        return saccMapper.deleteByPrimaryKey(br,accountno);
    }

    @Override
    public int insert(Sacc record) {
        return saccMapper.insert(record);
    }

    @Override
    public int insertSelective(Sacc record) {
        return saccMapper.insertSelective(record);
    }

    @Override
    public Sacc selectByPrimaryKey(String br,String accountno) {
        return saccMapper.selectByPrimaryKey(br,accountno);
    }
    @Override
	public Sacc selectByPrimaryKeyAndCno(String br, String accountno, String cno) {
    	return saccMapper.selectByPrimaryKeyAndCno(br,accountno,cno);
	}

    @Override
    public int updateByPrimaryKeySelective(Sacc record) {
        return saccMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Sacc record) {
        return saccMapper.updateByPrimaryKey(record);
    }

}
