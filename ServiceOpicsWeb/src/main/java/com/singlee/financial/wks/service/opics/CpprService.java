package com.singlee.financial.wks.service.opics;

import java.util.Date;
import java.util.List;

import com.singlee.financial.wks.bean.opics.Cppr;
import com.singlee.financial.wks.bean.opics.Fxdh;

public interface CpprService {

	int deleteByPrimaryKey(String br, String ccy1, String ccy2, String cost, String port, String trad, Date vdate);

	int insert(Cppr record);

	int insertSelective(Cppr record);

	Cppr selectByPrimaryKey(String br, String ccy1, String ccy2, String cost, String port, String trad, Date vdate);

	int updateByPrimaryKeySelective(Cppr record);

	int updateByPrimaryKey(Cppr record);

	int updateBatch(List<Cppr> list);

	int updateBatchSelective(List<Cppr> list);

	int updateCpprByFxdh(Cppr cppr);

	/**
	 * 组装外汇即、远期交易CPPR对象
	 * @param fxdh
	 * @return
	 */
	Cppr builder(Fxdh fxdh,Cppr cppr);
}
