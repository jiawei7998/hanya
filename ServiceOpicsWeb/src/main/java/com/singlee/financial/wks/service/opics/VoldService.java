package com.singlee.financial.wks.service.opics;

import java.math.BigDecimal;
import java.util.List;
import com.singlee.financial.wks.bean.opics.Vold;
public interface VoldService{


    int deleteByPrimaryKey(String br,String optiontype,String optiontypekey,String desmat,String callput,BigDecimal strikeprice8,String term);

    int insert(Vold record);

    int insertSelective(Vold record);

    Vold selectByPrimaryKey(String br,String optiontype,String optiontypekey,String desmat,String callput,BigDecimal strikeprice8,String term);

    int updateByPrimaryKeySelective(Vold record);

    int updateByPrimaryKey(Vold record);

    int updateBatch(List<Vold> list);

    int updateBatchSelective(List<Vold> list);

}
