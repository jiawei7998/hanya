package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.mapper.opics.SwdhMapper;
import com.singlee.financial.wks.service.opics.SwdhService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SwdhServiceImpl implements SwdhService {

    @Resource
    private SwdhMapper swdhMapper;

    @Override
    public int deleteByPrimaryKey(String br,String dealno,String seq,String dealind) {
        return swdhMapper.deleteByPrimaryKey(br,dealno,seq,dealind);
    }

    @Override
    public int insert(Swdh record) {
        return swdhMapper.insert(record);
    }

    @Override
    public int insertSelective(Swdh record) {
        return swdhMapper.insertSelective(record);
    }

    @Override
    public Swdh selectByPrimaryKey(String br,String dealno,String seq,String dealind) {
        return swdhMapper.selectByPrimaryKey(br,dealno,seq,dealind);
    }

    @Override
    public Swdh selectByTradeNo(String br, String dealtext,String status) {
        return swdhMapper.selectByTradeNo(br, dealtext,status);
    }

    @Override
    public int updateByPrimaryKeySelective(Swdh record) {
        return swdhMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Swdh record) {
        return swdhMapper.updateByPrimaryKey(record);
    }

	@Override
	public int updateByTerminate(Swdh record) {
		return swdhMapper.updateByTerminate(record);
	}

}
