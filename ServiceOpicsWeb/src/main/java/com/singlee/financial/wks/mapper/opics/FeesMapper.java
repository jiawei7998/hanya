package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Fees;
import org.apache.ibatis.annotations.Param;

public interface FeesMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("feeno") String feeno, @Param("feeseq") String feeseq);

    int insert(Fees record);

    int insertSelective(Fees record);

    Fees selectByPrimaryKey(@Param("br") String br, @Param("feeno") String feeno, @Param("feeseq") String feeseq);

    Fees selectByTradeNo(@Param("br") String br, @Param("dealtext") String dealtext);
    
    int updateByPrimaryKeySelective(Fees record);

    int updateByPrimaryKey(Fees record);
}