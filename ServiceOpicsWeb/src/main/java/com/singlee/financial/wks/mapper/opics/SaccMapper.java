package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Sacc;
import org.apache.ibatis.annotations.Param;

public interface SaccMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("accountno") String accountno);

    int insert(Sacc record);

    int insertSelective(Sacc record);

    Sacc selectByPrimaryKey(@Param("br") String br, @Param("accountno") String accountno);
    Sacc selectByPrimaryKeyAndCno(@Param("br") String br, @Param("accountno") String accountno,@Param("cno") String cno);

    int updateByPrimaryKeySelective(Sacc record);

    int updateByPrimaryKey(Sacc record);
}