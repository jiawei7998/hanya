package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Find implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String product;

    private String prodtype;

    private String incexptype;

    private String ccy;

    private BigDecimal tdyincexp;

    private BigDecimal ystincexp;

    private BigDecimal mtdincexp;

    private BigDecimal ytdincexp;

    private BigDecimal mtdpincexp;

    private BigDecimal ytdpincexp;

    private BigDecimal accroutst;

    private BigDecimal intpaid;

    private Date lstmntdate;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getIncexptype() {
		return incexptype;
	}


	public void setIncexptype(String incexptype) {
		this.incexptype = incexptype;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public BigDecimal getTdyincexp() {
		return tdyincexp;
	}


	public void setTdyincexp(BigDecimal tdyincexp) {
		this.tdyincexp = tdyincexp;
	}


	public BigDecimal getYstincexp() {
		return ystincexp;
	}


	public void setYstincexp(BigDecimal ystincexp) {
		this.ystincexp = ystincexp;
	}


	public BigDecimal getMtdincexp() {
		return mtdincexp;
	}


	public void setMtdincexp(BigDecimal mtdincexp) {
		this.mtdincexp = mtdincexp;
	}


	public BigDecimal getYtdincexp() {
		return ytdincexp;
	}


	public void setYtdincexp(BigDecimal ytdincexp) {
		this.ytdincexp = ytdincexp;
	}


	public BigDecimal getMtdpincexp() {
		return mtdpincexp;
	}


	public void setMtdpincexp(BigDecimal mtdpincexp) {
		this.mtdpincexp = mtdpincexp;
	}


	public BigDecimal getYtdpincexp() {
		return ytdpincexp;
	}


	public void setYtdpincexp(BigDecimal ytdpincexp) {
		this.ytdpincexp = ytdpincexp;
	}


	public BigDecimal getAccroutst() {
		return accroutst;
	}


	public void setAccroutst(BigDecimal accroutst) {
		this.accroutst = accroutst;
	}


	public BigDecimal getIntpaid() {
		return intpaid;
	}


	public void setIntpaid(BigDecimal intpaid) {
		this.intpaid = intpaid;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	private static final long serialVersionUID = 1L;
}