package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Coun implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -184956261298759397L;

	private String ccode;

    private String coun;

    private Date lstmntdte;

    private String sensitiveind;

    private String treatyind;

	public String getCcode() {
		return ccode;
	}

	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	public String getCoun() {
		return coun;
	}

	public void setCoun(String coun) {
		this.coun = coun;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getSensitiveind() {
		return sensitiveind;
	}

	public void setSensitiveind(String sensitiveind) {
		this.sensitiveind = sensitiveind;
	}

	public String getTreatyind() {
		return treatyind;
	}

	public void setTreatyind(String treatyind) {
		this.treatyind = treatyind;
	}
    
    
}