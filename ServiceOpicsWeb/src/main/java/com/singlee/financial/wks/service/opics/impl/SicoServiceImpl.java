package com.singlee.financial.wks.service.opics.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.financial.wks.mapper.opics.SicoMapper;
import com.singlee.financial.wks.bean.opics.Sico;
import com.singlee.financial.wks.service.opics.SicoService;
@Service
public class SicoServiceImpl implements SicoService{

    @Resource
    private SicoMapper sicoMapper;

    @Override
    public int deleteByPrimaryKey(String sic) {
        return sicoMapper.deleteByPrimaryKey(sic);
    }

    @Override
    public int insert(Sico record) {
        return sicoMapper.insert(record);
    }

    @Override
    public int insertSelective(Sico record) {
        return sicoMapper.insertSelective(record);
    }

    @Override
    public Sico selectByPrimaryKey(String sic) {
        return sicoMapper.selectByPrimaryKey(sic);
    }

    @Override
    public int updateByPrimaryKeySelective(Sico record) {
        return sicoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Sico record) {
        return sicoMapper.updateByPrimaryKey(record);
    }

}
