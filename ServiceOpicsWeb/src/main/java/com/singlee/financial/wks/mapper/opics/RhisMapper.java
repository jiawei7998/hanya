package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Rhis;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface RhisMapper {
    /**
     * delete by primary key
     * @param br primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("br") String br, @Param("ratecode") String ratecode, @Param("effdate") Date effdate);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Rhis record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Rhis record);

    /**
     * select by primary key
     * @param br primary key
     * @return object by primary key
     */
    Rhis selectByPrimaryKey(@Param("br") String br, @Param("ratecode") String ratecode, @Param("effdate") Date effdate);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Rhis record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Rhis record);

    /**
     * seelct List
     * @param rhis
     * @return
     */
    List<Rhis> selectAllByRate(Rhis rhis);
}