package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Sfdi;
import org.apache.ibatis.annotations.Param;

public interface SfdiMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("cno") String cno, @Param("delrecind") String delrecind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("ccy") String ccy, @Param("safekeepacct") String safekeepacct);

    int insert(Sfdi record);

    int insertSelective(Sfdi record);

    Sfdi selectByPrimaryKey(@Param("br") String br, @Param("cno") String cno, @Param("delrecind") String delrecind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("ccy") String ccy, @Param("safekeepacct") String safekeepacct);

    int updateByPrimaryKeySelective(Sfdi record);

    int updateByPrimaryKey(Sfdi record);
}