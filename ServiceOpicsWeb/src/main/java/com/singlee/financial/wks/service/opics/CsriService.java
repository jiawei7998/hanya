package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Csri;
public interface CsriService{


    int deleteByPrimaryKey(String br,String cust,String product,String type,String ccy);

    int insert(Csri record);

    int insertSelective(Csri record);

    Csri selectByPrimaryKey(String br,String cust,String product,String type,String ccy);

    int updateByPrimaryKeySelective(Csri record);

    int updateByPrimaryKey(Csri record);
    
}
