package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Ycrs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YcrsMapper {
    /**
     * delete by primary key
     * @param br primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("yieldcurve") String yieldcurve, @Param("contribrate") String contribrate, @Param("mtystart") String mtystart, @Param("mtyend") String mtyend, @Param("delvcode") String delvcode);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Ycrs record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Ycrs record);

    /**
     * select by primary key
     * @param br primary key
     * @return object by primary key
     */
    Ycrs selectByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("yieldcurve") String yieldcurve, @Param("contribrate") String contribrate, @Param("mtystart") String mtystart, @Param("mtyend") String mtyend, @Param("delvcode") String delvcode);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Ycrs record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Ycrs record);

    /**
     * select record
     * @return
     */
    List<Ycrs> selectByAll(Ycrs record);
}