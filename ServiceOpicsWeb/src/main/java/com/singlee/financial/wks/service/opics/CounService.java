package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Coun;
public interface CounService{


    int deleteByPrimaryKey(String ccode);

    int insert(Coun record);

    int insertSelective(Coun record);

    Coun selectByPrimaryKey(String ccode);

    int updateByPrimaryKeySelective(Coun record);

    int updateByPrimaryKey(Coun record);

}
