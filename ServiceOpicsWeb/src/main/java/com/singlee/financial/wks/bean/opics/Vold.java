package com.singlee.financial.wks.bean.opics;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Vold {
    private String br;

    private String optiontype;

    private String optiontypekey;

    private String desmat;

    private String callput;

    private BigDecimal strikeprice8;

    private String term;

    private BigDecimal volatility8;

    private String atmoneyind;

    private String volsourceDde;

    private BigDecimal bidvolatility8;

    private BigDecimal askvolatility8;

    private String bidvolsourceDde;

    private String askvolsourceDde;

    private Date lstmntdate;

    private BigDecimal riskrev8;

    private BigDecimal stranglemgn8;

    private String riskrevsourceDde;

    private String stranglemgnsourceDde;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getOptiontype() {
		return optiontype;
	}

	public void setOptiontype(String optiontype) {
		this.optiontype = optiontype;
	}

	public String getOptiontypekey() {
		return optiontypekey;
	}

	public void setOptiontypekey(String optiontypekey) {
		this.optiontypekey = optiontypekey;
	}

	public String getDesmat() {
		return desmat;
	}

	public void setDesmat(String desmat) {
		this.desmat = desmat;
	}

	public String getCallput() {
		return callput;
	}

	public void setCallput(String callput) {
		this.callput = callput;
	}

	public BigDecimal getStrikeprice8() {
		return strikeprice8;
	}

	public void setStrikeprice8(BigDecimal strikeprice8) {
		this.strikeprice8 = strikeprice8;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public BigDecimal getVolatility8() {
		return volatility8;
	}

	public void setVolatility8(BigDecimal volatility8) {
		this.volatility8 = volatility8;
	}

	public String getAtmoneyind() {
		return atmoneyind;
	}

	public void setAtmoneyind(String atmoneyind) {
		this.atmoneyind = atmoneyind;
	}

	public String getVolsourceDde() {
		return volsourceDde;
	}

	public void setVolsourceDde(String volsourceDde) {
		this.volsourceDde = volsourceDde;
	}

	public BigDecimal getBidvolatility8() {
		return bidvolatility8;
	}

	public void setBidvolatility8(BigDecimal bidvolatility8) {
		this.bidvolatility8 = bidvolatility8;
	}

	public BigDecimal getAskvolatility8() {
		return askvolatility8;
	}

	public void setAskvolatility8(BigDecimal askvolatility8) {
		this.askvolatility8 = askvolatility8;
	}

	public String getBidvolsourceDde() {
		return bidvolsourceDde;
	}

	public void setBidvolsourceDde(String bidvolsourceDde) {
		this.bidvolsourceDde = bidvolsourceDde;
	}

	public String getAskvolsourceDde() {
		return askvolsourceDde;
	}

	public void setAskvolsourceDde(String askvolsourceDde) {
		this.askvolsourceDde = askvolsourceDde;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public BigDecimal getRiskrev8() {
		return riskrev8;
	}

	public void setRiskrev8(BigDecimal riskrev8) {
		this.riskrev8 = riskrev8;
	}

	public BigDecimal getStranglemgn8() {
		return stranglemgn8;
	}

	public void setStranglemgn8(BigDecimal stranglemgn8) {
		this.stranglemgn8 = stranglemgn8;
	}

	public String getRiskrevsourceDde() {
		return riskrevsourceDde;
	}

	public void setRiskrevsourceDde(String riskrevsourceDde) {
		this.riskrevsourceDde = riskrevsourceDde;
	}

	public String getStranglemgnsourceDde() {
		return stranglemgnsourceDde;
	}

	public void setStranglemgnsourceDde(String stranglemgnsourceDde) {
		this.stranglemgnsourceDde = stranglemgnsourceDde;
	}
    
}