package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.RepoWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.intfc.RepoService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.RepoEntry;
import com.singlee.financial.wks.service.trans.RepoReverse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RepoServiceImpl implements RepoService{

	private static Logger logger = Logger.getLogger(RepoServiceImpl.class);
	
	@Autowired
    private DrpiService drpiService;
	@Autowired
    private RprhService rprhService;
	@Autowired
	private RpdtService rpdtService;
	@Autowired
    private McfpService mcfpService;
	@Autowired
    private CnfqService cnfqService;
	@Autowired
    private NupdService nupdService;
	@Autowired
    private PmtqService pmtqService;
	@Autowired
	private SposService sposService;
	@Autowired
    private BrpsService brpsService;
	@Autowired
    private CustService custService;
	@Autowired
    private SecmService secmService;
	@Autowired
    private RepoEntry repoEntry;// 首期交易录入
	@Autowired
    private RepoReverse repoReverse;// 冲销转换
	
	@Override
	public SlOutBean saveCrEntry(RepoWksBean repoWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "CBT-00000", "质押回购录入成功!");
		Rprh rprh =null;
		List<Rpdt> rpdtLst = new ArrayList<Rpdt>();// 声明
		Spos spos = null;//日间头寸
		Drpi drpi = new Drpi();//债券路径
		try {
			// 1.交易要素检查
			repoEntry.DataCheck(repoWksBean, custService,secmService);
			// 2.转换交易主体数据
			rprh = repoEntry.DataTrans(new Rprh());//静态数据
			repoEntry.DataTrans(rprh, mcfpService);// 交易编号
			repoEntry.DataTrans(rprh, brpsService);// 设置账务日期
			repoEntry.DataTrans(rprh, repoWksBean);// 设置交易信息
            rprhService.insert(rprh);
            
            //转换RPDT数据
            rpdtLst = repoEntry.DataTrans(rpdtLst,rprh,repoWksBean,secmService);
			rpdtService.batchInsert(rpdtLst);
			
			// 转换drpi数据
	        repoEntry.DataTrans(drpi, rprh);
	        drpiService.insert(drpi);
			
			//往来帐信息nupd
            List<Nupd> nupdLst = nupdService.builder(rprh);
            if (nupdLst.size() > 0) {
                nupdService.batchInsert(nupdLst);
            }
            // 5.收付款信息 
            List<Pmtq> pmtqLst = pmtqService.builder(rprh,rpdtLst);
            if (pmtqLst.size() > 0) {
                pmtqService.batchInsert(pmtqLst);
            }
            // 9.确认信息
            cnfqService.insert(cnfqService.bulider(rprh));
            // 10.回填dealno
            repoEntry.DataTrans(rprh, mcfpService, new Mcfp());
            
            //更新SPOS
            for(Rpdt rpdt:rpdtLst) {
            	spos = sposService.selectByPrimaryKey(rpdt.getBr(), rpdt.getCcy(), rpdt.getPort(), rpdt.getSecid(),
            			rpdt.getMdate(), rpdt.getInvtype(), rpdt.getCost());
                    if (spos == null) {
                        sposService.insert(sposService.builder(rpdt));
                    } else {
                        // 调用存储过程SP_UPDREC_SPOS_INCR
                    	//计算头寸:票面金额×起息日净价÷100
                    	BigDecimal sposAmt = rpdt.getFaceamt().multiply(rpdt.getVdcleanprice8()).divide(new BigDecimal("100"));
                        sposService.callSpUpdrecSposIncr(rpdt.getBr(), rpdt.getCcy(), rpdt.getPort(), rpdt.getSecid(),
                        		rpdt.getMdate(), rpdt.getInvtype(), rpdt.getCost(),
                        		"P".equalsIgnoreCase(rpdt.getPs()) ? sposAmt : sposAmt.negate(),
                            Calendar.getInstance().getTime(),
                            "P".equalsIgnoreCase(rpdt.getPs()) ? rpdt.getQty().negate() : rpdt.getQty());
                    }
            }
            
            
		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
            outBean.setRetCode("CBT-10001");
            outBean.setRetMsg("质押回购交易处理错误<请检查输入要素完整性>" + e.getMessage());
            logger.error("质押回购交易处理错误<请检查输入要素完整性>", e);
            JY.raise("CBT-10001", e);
		}
		outBean.setClientNo(rprh.getDealno());
        logger.info("质押回购交易处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	@Override
	public SlOutBean saveOrEntry() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SlOutBean reverseTrading(RepoWksBean repoWksBean) {
		// TODO Auto-generated method stub
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "REPO-00000", "回购交易冲销成功!");
		Rprh rprh = null;
        try {
            // sldhWksBean.getTradeNo()为opics dealno
        	repoReverse.DataCheck(repoWksBean);
            // 1.查询交易
        	rprh = rprhService.selectByTradeNo(repoWksBean.getInstId(), repoWksBean.getTradeNo());
//            // 1.1修改RPRH
        	repoReverse.DataTrans(rprh, brpsService);
        	rprhService.updateByPrimaryKey(rprh);
        	
            // 3.1删除NUPD的记录
            nupdService.callSpDelrecNupdVdate(rprh.getBr(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
            		rprh.getProdtype(), rprh.getVdate(), rprh.getComccysacct());
            // 3.2根据冲销时间节点新增NUPD
            nupdService.callSpAddrecNupd(rprh.getBr(), rprh.getComccysacct(), rprh.getDealno(), rprh.getSeq(),
            		rprh.getProduct(), rprh.getProdtype(), rprh.getCno(), rprh.getVdate(), rprh.getRevdate(),
                rprh.getComprocdamt().negate());

            // 4.1删除大于新到期日的记录
            pmtqService.callSpUpdrecPmtqSwiftfmt(rprh.getBr(), rprh.getDealno(), rprh.getSeq(), rprh.getProduct(),
                rprh.getProdtype(), rprh.getRevdate(), DateUtil.getCurrentTimeAsString(), "", "", "", "", null,
                BigDecimal.ZERO);
            List<Rpdt> rpdtLst = rpdtService.selectByDealno(rprh.getBr(), rprh.getDealno());
            for(Rpdt rpdt:rpdtLst) {
            	// 更新SPOS头寸
            	//计算头寸:票面金额×起息日净价÷100
            	BigDecimal sposAmt = rpdt.getFaceamt().multiply(rpdt.getVdcleanprice8()).divide(new BigDecimal("100"));
                sposService.callSpUpdrecSposIncr(rpdt.getBr(), rpdt.getCcy(), rpdt.getPort(), rpdt.getSecid(),
                		rpdt.getMdate(), rpdt.getInvtype(), rpdt.getCost(),
                		"P".equalsIgnoreCase(rpdt.getPs()) ? sposAmt.negate() : sposAmt,
                    Calendar.getInstance().getTime(),
                    "P".equalsIgnoreCase(rpdt.getPs()) ? rpdt.getQty() : rpdt.getQty().negate());
            }
            
        } catch (Exception e) {
            outBean.setRetStatus(RetStatusEnum.F);
            outBean.setRetMsg(e.getMessage());
            logger.error("回购交易冲销错误", e);
            throw e;
        }
        outBean.setClientNo(rprh.getDealno());
        logger.info("回购交易冲销处理完成,返回消息:" + JSON.toJSONString(outBean));
        return outBean;
	}

}
