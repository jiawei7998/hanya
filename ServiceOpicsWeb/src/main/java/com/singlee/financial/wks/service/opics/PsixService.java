package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.bean.opics.Psix;
import com.singlee.financial.wks.bean.opics.Sldh;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.bean.opics.Swdt;
import com.singlee.financial.wks.expand.FxdItemWksBean;

public interface PsixService {

    int deleteByPrimaryKey(String br, String product, String type, String dealno, String seq);

    int insert(Psix record);

    int insertSelective(Psix record);

    Psix selectByPrimaryKey(String br, String product, String type, String dealno, String seq);
    
    Psix selectByDealno(String br, String product, String type, String dealno);

    int updateByPrimaryKeySelective(Psix record);

    int updateByPrimaryKey(Psix record);

    void updateBatch(List<Psix> list);

    void batchInsert(List<Psix> list);

    /**
     * 方法已重载.根据拆借业务数据构建PSIX对象
     * 
     * @param dl
     * @return
     */
    Psix builder(Dldt dl);

    /**
     * 方法已重载.根据期权业务数据构建PSIX对象
     * 
     * @param otc
     * @return
     */
    Psix builder(Otdt otc);

    /**
     * 方法已重载.根据外汇业务数据构建PSIX对象
     * 
     * @param fxLst
     * @return
     */
    Psix builder(Fxdh fxdh,FxdItemWksBean itemBean);

    /**
     * 方法已重载.根据互换业务数据构建PSIX对象
     * 
     * @param swdtLst
     * @return
     */
    Psix builder(Swdh swdh, List<Swdt> swdtLst,SwapWksBean swap);
    
    /**
     * 方法已重载.根据现券买卖数据构建PSIX对象
     * 
     * @param dl
     * @return
     */
    Psix builder(Spsh spsh);
    Psix builder(Sldh sldh);
}
