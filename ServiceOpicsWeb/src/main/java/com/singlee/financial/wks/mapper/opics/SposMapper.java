package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Spos;

import java.math.BigDecimal;
import java.util.Date;
import org.apache.ibatis.annotations.Param;

public interface SposMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("port") String port, @Param("secid") String secid, @Param("settdate") Date settdate, @Param("invtype") String invtype, @Param("cost") String cost);

    int insert(Spos record);

    int insertSelective(Spos record);

    Spos selectByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("port") String port, @Param("secid") String secid, @Param("settdate") Date settdate, @Param("invtype") String invtype, @Param("cost") String cost);

    int updateByPrimaryKeySelective(Spos record);

    int updateByPrimaryKey(Spos record);
    
    void callSpUpdrecSposIncr(@Param("br") String br,@Param("ccy") String ccy,@Param("port") String port,
    		@Param("secid") String secid,@Param("settdate") Date settdate,
    		@Param("invtype") String invtype,@Param("cost") String cost,@Param("amt") BigDecimal amt,
    		@Param("lstmntdate") Date lstmntdate,@Param("qty") BigDecimal qty);
}