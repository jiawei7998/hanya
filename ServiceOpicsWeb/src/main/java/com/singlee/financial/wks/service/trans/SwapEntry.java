package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.OpicsConstant.SWAP;
import com.singlee.financial.wks.bean.OpicsConstant.SWAP.ExchPrin;
import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.cashflow.HolidayBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.DateConventionType;
import com.singlee.financial.wks.expand.FieldMapType;
import com.singlee.financial.wks.expand.SwapItemSchdWksBean;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.cashflow.SwapFactory;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.util.FinancialCalculationModel;
import com.singlee.financial.wks.util.FinancialDateUtils;
import com.singlee.financial.wks.util.FinancialFieldMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 互换交易录入转换方法
 *
 * @author shenzl
 * @date 2020/02/19
 */
@Component
public class SwapEntry {
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private PortService portService;
	@Autowired
	private CusiService cusiService;
	@Autowired
	private CustService custService;
	@Autowired
	private CostService costService;
	@Autowired
	private TradService tradService;
	@Autowired
	private SetaService setaService;
	@Autowired
	private RateService rateService;
	@Autowired
	private FinancialFieldMap financialFieldMap;
	@Autowired
	private RevpService revpService;
	@Autowired
	private CcodService ccodService;
	@Autowired
	private YchdService ychdService;
	
	public void DataIrsSwapCheck(SwapWksBean irs) {

		// 1.判断拆借交易对象是否存在
		if (null == irs) {
			JY.raise("%s:%s,请补充完整SwapWksBean对象", WksErrorCode.IRS_NULL.getErrCode(), WksErrorCode.IRS_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(irs.getInstId())) {
			JY.raise("%s:%s,请填写SwapWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NULL.getErrCode(), WksErrorCode.BR_NULL.getErrMsg());
		}
		// 3.查询分支/机构是否存在
		Brps brps = brpsService.selectByPrimaryKey(irs.getInstId());
		if (null == brps || StringUtils.isEmpty(irs.getInstId())) {
			JY.raise("%s:%s,请正确输入SwapWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(), WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 4.检查产品类型DESK字段拆分PROD_TYPE_PORT
		String[] book = StringUtils.split(irs.getBook(), "_");
		if (StringUtils.isEmpty(irs.getBook()) || book.length != 3) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.desk,For.[book=SWAP_SP_100]", WksErrorCode.DESK_FORM_ERR.getErrCode(), WksErrorCode.DESK_FORM_ERR.getErrMsg());
		}
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		if (null == type || StringUtils.isEmpty(book[0]) || StringUtils.isEmpty(book[1])) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.desk,For.[book=SWAP_SP_100]", WksErrorCode.DESK_NOT_FOUND.getErrCode(), WksErrorCode.DESK_NOT_FOUND.getErrMsg());
		}
		Port port = portService.selectByPrimaryKey(irs.getInstId(), irs.getDesk());// 投组组合
		if (null == port || StringUtils.isEmpty(irs.getDesk())) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.desk,For.[desk=PORT]", WksErrorCode.DESK_PORT_ERR.getErrCode(), WksErrorCode.DESK_PORT_ERR.getErrMsg());
		}
		// 7.检查成本中心
		Cost cost = costService.selectByPrimaryKey(book[2]);
		if (null == cost || StringUtils.isEmpty(book[2])) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.book,For.[book=SWAP_SP_100,或查询RATE表]", WksErrorCode.BOOK_NOT_FOUND.getErrCode(), WksErrorCode.BOOK_NOT_FOUND.getErrMsg());
		}
		// 8.检查交易对手
		Cust cust = custService.selectByPrimaryKey(irs.getCno());
		if (null == cust || StringUtils.isEmpty(irs.getCno())) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.cno,For.[cno=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(), WksErrorCode.CUST_NOT_FOUND.getErrMsg());
		}
		// 9.检查交易员
		Trad trad = tradService.selectByPrimaryKey(irs.getInstId(), irs.getTrad());
		if (null == trad || StringUtils.isEmpty(irs.getTrad())) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.trad,For.[trad=TRAD]", WksErrorCode.TRAD_NOT_FOUND.getErrCode(), WksErrorCode.TRAD_NOT_FOUND.getErrMsg());
		}

		// 10.检查清算路径
		Seta seta = setaService.selectBySettacct(irs.getInstId(),irs.getPayAcctNo());
		if (null == seta || StringUtils.isEmpty(seta.getSacct())) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.payAcctNo,For.[PayAcctNo=Sacct]", WksErrorCode.SETA_SACCT_NOT_FOUND.getErrCode(), WksErrorCode.SETA_SACCT_NOT_FOUND.getErrMsg());
		}
		seta = setaService.selectBySettacct(irs.getInstId(),irs.getRecAcctNo());
		if (null == seta || StringUtils.isEmpty(seta.getSacct())) {
			JY.raise("%s:%s,请输入正确格式SwapWksBean.recAcctNo,For.[recAcctNo=Sacct]", WksErrorCode.SETA_SACCT_NOT_FOUND.getErrCode(), WksErrorCode.SETA_SACCT_NOT_FOUND.getErrMsg());
		}
		if (StringUtils.isEmpty(irs.getSwapType())) {
			JY.raise("SWAP模块SWAPTYPE不能为空,枚举值为I、C");
		}
	}
	
	/**
	 * 检查交易是否已存在
	 * @param sn
	 * @return
	 */
	public void DataIrsSwapCheck(SwdhService swdhService,String br,String sn) {
		
		Swdh swdh = swdhService.selectByTradeNo(br, sn,"N");
		
		if(null != swdh && swdh.getDealtext().contains(sn)) {
			throw new RuntimeException("成交编号["+sn+"]已存在,对应OPICS交易编号["+swdh.getDealno()+"]");
		}
		
	}

	/**
	 * 主表静态常量
	 *
	 * @param swdh
	 * @return
	 */
	public Swdh DataTrans(Swdh swdh) {
		swdh.setSeq(OpicsConstant.SWAP.SEQ_ZERO);// 序号
		swdh.setDealsrce(OpicsConstant.SWAP.DEAL_SRCE);// 交易源代码
		swdh.setBrok("");//经纪人
		swdh.setBrokccy("");// 经纪人费用币种
		swdh.setBrokamt(BigDecimal.ZERO);// 经纪人费用
		swdh.setDealind(OpicsConstant.SWAP.DEALIND);// 交易标识
		swdh.setBrokrate8(BigDecimal.ZERO);// 经纪人利率
		swdh.setNetpayind(OpicsConstant.SWAP.NETPAYIND);// 内部付款标识
		swdh.setPlmethod(OpicsConstant.SWAP.PLMETHOD);// 损益方法
		swdh.setIoper(OpicsConstant.SWAP.TRAD_NO);// 输入操作员
		swdh.setVoper(OpicsConstant.SWAP.TRAD_NO);// 复核操作员
		swdh.setVerind(OpicsConstant.SWAP.VER_IND);// 复核标志
		swdh.setNpvbamt(BigDecimal.ZERO);// 折本币金额
		swdh.setSetoff(OpicsConstant.SWAP.SETOFF);// 设置启用标识
		swdh.setSwaptype(OpicsConstant.SWAP.SWAPTYPE_I);// 掉期类型
		swdh.setTenor(OpicsConstant.SWAP.TENOR);// 期限
		swdh.setNpvbamtpay(BigDecimal.ZERO);// 付款金额
		swdh.setNpvbamtrec(BigDecimal.ZERO);// 收款金额
		swdh.setTracecnt(BigDecimal.ZERO.toString());// 交易成本
		swdh.setEtamt(BigDecimal.ZERO);// ET金额
		swdh.setEtauthind(OpicsConstant.SWAP.ETAUTHIND);// 提前结束指令授权标志
		swdh.setActiveind(OpicsConstant.SWAP.ACTIVEIND);// 掉期期权标志
		swdh.setIntrinsicvalue(BigDecimal.ZERO);// 利息付款规则
		swdh.setAmount1(BigDecimal.ZERO);// 金额1
		swdh.setRate18(BigDecimal.ZERO);// 利率1
		swdh.setUpdatecounter(OpicsConstant.SWAP.UPDATE_COUNTER);// 修改次数
		swdh.setVanillaswapind(OpicsConstant.SWAP.VANILLASWAPIND);// 掉期规则
		swdh.setBtbind(OpicsConstant.SWAP.BTBIND);// 背对背交易标志
		swdh.setNotccyamt(BigDecimal.ZERO);//
		swdh.setBcreditamt(BigDecimal.ZERO);// 金额1
		swdh.setBmtmamt(BigDecimal.ZERO);// 单条腿金额
		swdh.setBmktamt(BigDecimal.ZERO);// 另条腿金额
		swdh.setFlag2(OpicsConstant.SWAP.FLAG2);// 备用
		swdh.setRate28(BigDecimal.ZERO);// 利率2
		swdh.setRate38(BigDecimal.ZERO);// 利率3
		swdh.setAmount2(BigDecimal.ZERO);// 金额2
		swdh.setAmount3(BigDecimal.ZERO);// 金额3
		swdh.setClragntamt(BigDecimal.ZERO);
		swdh.setBmfamt(BigDecimal.ZERO);
		swdh.setEtamt2(BigDecimal.ZERO);// 提前结束金额
		swdh.setDealtime(DateUtil.getCurrentTimeAsString());// 交易时间
		swdh.setInputdate(DateUtil.parse(DateUtil.getCurrentDateAsString()));// 输入系统日期
		swdh.setInputtime(DateUtil.getCurrentTimeAsString());// 输入系统时间
		swdh.setLegcnt(OpicsConstant.SWAP.LEGCNT);// 互换交易腿个数
		swdh.setInvtype(OpicsConstant.SWAP.INVTYPE);// 投资类型
		swdh.setCreditcode("");
		swdh.setEtoper("");
		swdh.setImm("");
		swdh.setEtassigncust("");
		swdh.setEtauthoper("");
		swdh.setEtccy("");
		swdh.setEtpayrecind("");
		swdh.setEtsacct("");
		swdh.setEtsmeans("");
		swdh.setEtfeeno("");
		swdh.setCollardealno("");
		swdh.setEtfeeintind("");
		swdh.setIntdeal("");
		swdh.setFlag3("");
		swdh.setPtyguarantee("");
		swdh.setClragntname("");
		swdh.setClragntccy("");
		swdh.setBmfname("");
		swdh.setBmfccy("");
		swdh.setEtauthind2("");
		swdh.setEtauthoper2("");
		swdh.setEtccy2("");
		swdh.setEtpayrecind2("");
		swdh.setEtsacct2("");
		swdh.setEtsmeans2("");
		swdh.setEtfeeno2("");
		swdh.setEtfeeintind2("");
		swdh.setEtlegseq("");
		swdh.setEtlegseq2("");
		swdh.setDeallinkno("");
		swdh.setRevoper("");
		swdh.setRevreason("");
		swdh.setRevtext("");
		swdh.setUndacctngtype("");
		swdh.setUnddealno("");
		swdh.setUndproduct("");
		swdh.setUndsecid("");
		swdh.setUndtenor("");
		swdh.setUndtype("");
		swdh.setUndhedgeseq("");
		swdh.setOptiontype("");
		swdh.setExeract("");
		swdh.setGuarantor("");
		swdh.setChar1("");
		swdh.setFlag1("");
		swdh.setCounterrefid("");
		swdh.setBtbbr("");
		swdh.setBtbdealno("");
		swdh.setBtbseq("");
		swdh.setBtbprod("");
		swdh.setBtbprodtype("");
		swdh.setBtbcno("");
		swdh.setBtbcost("");
		swdh.setBtbport("");
		swdh.setBtbtrad("");
		swdh.setMbsdealtype("");
		swdh.setNotccy("");
		swdh.setTaxflg("");
		swdh.setCorptrad("");
		swdh.setCorpport("");
		swdh.setCorpcost("");
		swdh.setCntrptyguarantee("");
		swdh.setNotexchrateind("");
		swdh.setMulticcyind("");
		swdh.setVarnotionalind("");
		swdh.setCreditdefaultind("");
		swdh.setMbslegind("");
		
		return swdh;
	}

	/**
	 * 固定浮动对应的腿信息
	 *
	 * @param swdt
	 * @return
	 */
	public Swdt DataTrans(Swdt swdt) {
		swdt.setFinexchamt(BigDecimal.ZERO);// 最后资金交换金额
		swdt.setFinexchbamt(BigDecimal.ZERO);// 最后资金交换基础币种金额
		swdt.setFinexchauthind(OpicsConstant.SWAP.FINEXCHAUTHIND);// 最后资金交换指令授权标志
		swdt.setFixdintamt(BigDecimal.ZERO);// 固定利息金额
		swdt.setInitexchamt(BigDecimal.ZERO);// 开始资金交换金额
		swdt.setInitexchbamt(BigDecimal.ZERO);// 开始资金交换基础币种金额
		swdt.setInitexchauthind(OpicsConstant.SWAP.INITEXCHAUTHIND);// 开始资金交换指令授权标志
		swdt.setIntauthind(OpicsConstant.SWAP.INTAUTHIND);// 利息结算指令授权标志
		swdt.setIntauthoper(OpicsConstant.SWAP.INTAUTHOPER);// 利息结算指令授权操作员
		swdt.setIntconvrate8(BigDecimal.ZERO);// 利率转换率
		swdt.setIntexchrate8(BigDecimal.ZERO);// 利息货币汇率
		swdt.setIntratecap8(BigDecimal.ZERO);// 利率上限
		swdt.setIntratefloor8(BigDecimal.ZERO);// 利率下限
		swdt.setNpvamt(BigDecimal.ZERO);// 净现值
		swdt.setRaterevfrstlst(OpicsConstant.SWAP.RATEREVFRSTLST);// 第一次/最后一次利率重置标志
		swdt.setAcrfrstlst(OpicsConstant.SWAP.RATEREVFRSTLST);// 第一次利息/最后一次利息标志
		swdt.setNpvamtinitexch(BigDecimal.ZERO);// 起始交换金额净现值
		swdt.setNpvamtfinexch(BigDecimal.ZERO);// 最终交换金额净现值
		swdt.setStrike8(BigDecimal.ZERO);// 执行利率
		swdt.setIndex8(BigDecimal.ZERO);// 利率
		swdt.setParticperc8(BigDecimal.ZERO);// 参与百分比
		swdt.setDelta8(BigDecimal.ZERO);// 希腊字母
		swdt.setVolatility8(BigDecimal.ZERO);// 流动率
		swdt.setCollarind(OpicsConstant.SWAP.COLLARIND);// SET
		swdt.setNpvbamt(BigDecimal.ZERO);// 净现值基数
		swdt.setTdyaccrintamt(BigDecimal.ZERO);// 今日利息
		swdt.setTdyaccrintbamt(BigDecimal.ZERO);// 今日应计利息基础货币金额
		swdt.setYstaccrintamt(BigDecimal.ZERO);// 昨日应计利息金额
		swdt.setYstaccrintbamt(BigDecimal.ZERO);// 昨日应计利息基础货币金额
		swdt.setNpvbamtinitexch(BigDecimal.ZERO);// 起始交换金额净现值
		swdt.setNpvbamtfinexch(BigDecimal.ZERO);// 最终交换金额净现值a
		swdt.setTdycomprate8(BigDecimal.ZERO);// 今日复利率
		swdt.setTdycompamt(BigDecimal.ZERO);// 今日复利
		swdt.setYstcomprate8(BigDecimal.ZERO);// 今日复利率
		swdt.setYstcompamt(BigDecimal.ZERO);// 今日复利
		swdt.setDatedirection(OpicsConstant.SWAP.DATEDIRECTION);// 日期方向规则
		swdt.setPurchint(BigDecimal.ZERO);// 买入利息
		swdt.setAmount1(BigDecimal.ZERO);// 备用1
		swdt.setAmount2(BigDecimal.ZERO);// 备用2
		swdt.setAmount3(BigDecimal.ZERO);// 备用3
		swdt.setRate18(BigDecimal.ZERO);// 备用10
		swdt.setRate28(BigDecimal.ZERO);// 备用11
		swdt.setMbsleg(OpicsConstant.SWAP.MBSLEG);//
		swdt.setSpeed12(BigDecimal.ZERO);//
		swdt.setServicing12(BigDecimal.ZERO);//
		swdt.setFactor12(BigDecimal.ZERO);// 因数
		swdt.setOrigprinadjamt(BigDecimal.ZERO);// 初始本金调整金额
		swdt.setInternalrate8(BigDecimal.ZERO);// 内部利率
		swdt.setCorpspread8(BigDecimal.ZERO);//
		swdt.setBcreditamt(BigDecimal.ZERO);//
		swdt.setBmtmamt(BigDecimal.ZERO);//
		swdt.setBmktamt(BigDecimal.ZERO);//
		swdt.setExchrate8(BigDecimal.ZERO);// 汇率
		swdt.setInitbaseamt(BigDecimal.ZERO);//
		swdt.setInitfinamt(BigDecimal.ZERO);//
		swdt.setInitfinbaseamt(BigDecimal.ZERO);//
		swdt.setInitfinexchrate8(BigDecimal.ZERO);//
		swdt.setInitbaseexchrate8(BigDecimal.ZERO);//
		swdt.setFlag2(OpicsConstant.SWAP.FLAG2);// 备用12
		swdt.setRate38(BigDecimal.ZERO);// 备用12
		swdt.setRate48(BigDecimal.ZERO);// 备用12
		swdt.setAmount4(BigDecimal.ZERO);// 备用12
		swdt.setAmount5(BigDecimal.ZERO);// 备用12
		swdt.setCorpspreadamt(BigDecimal.ZERO);//
		swdt.setPostnotional(OpicsConstant.SWAP.POSTNOTIONAL);//
		swdt.setConvprice12(BigDecimal.ZERO);//
		swdt.setPrice12(BigDecimal.ZERO);//
		swdt.setPriceexchrate8(BigDecimal.ZERO);//
		swdt.setQuantity(BigDecimal.ZERO);// 股票份额
		swdt.setRateofreturn12(BigDecimal.ZERO);//
		swdt.setDivpercent8(BigDecimal.ZERO);// 股息比率
		swdt.setMultiplier8(BigDecimal.ZERO);//
		swdt.setDealrate8(BigDecimal.ZERO);// 交易利率
		swdt.setMulticcyacctind(OpicsConstant.SWAP.MULTICCYACCTIND);// 多币种清算账目标志
		swdt.setSchedid(BigDecimal.ZERO);//
		swdt.setProdid(BigDecimal.ZERO);//
		swdt.setFormulaid(BigDecimal.ZERO);//
		swdt.setGrossufpayamt(BigDecimal.ZERO);//
		swdt.setGrossufpayrate8(BigDecimal.ZERO);//
		swdt.setRedemptionccyind(OpicsConstant.SWAP.REDEMPTIONCCYIND);//
		swdt.setDeffactor(BigDecimal.ZERO);//
		swdt.setIntnegauthind("0");
		swdt.setCalcrule("");
		swdt.setFinexchccy("");
		swdt.setFinexchfincent("");
		swdt.setFinexchoper("");
		swdt.setFinexchsacct("");
		swdt.setFinexchsmeans("");
		swdt.setInitexchccy("");
		swdt.setInitexchfincent("");
		swdt.setInitexchoper("");
		swdt.setInitexchsmeans("");
		swdt.setInitexchsacct("");
		swdt.setIntexchterms("");
		swdt.setIntpayfincent("");
		swdt.setRaterevfincent("");
		swdt.setUpfrontfeeno("");
		swdt.setPs("");
		swdt.setSettfirst("");
		swdt.setUpfrontfeefincent("");
		swdt.setUpfrontfeeind("");
		swdt.setChar1("");
		swdt.setChar2("");
		swdt.setFormulaind("");
		swdt.setFlag1("");
		swdt.setUsualid("");
		swdt.setBtbusualid("");
		swdt.setPrepaytype("");
		swdt.setSettnotional("");
		swdt.setIntdeal("");
		swdt.setExchterms("");
		swdt.setInitfinexchterms("");
		swdt.setInitbaseterms("");
		swdt.setFlag3("");
		swdt.setBasketdescr("");
		swdt.setEquitytype("");
		swdt.setIntnegauthoper("");
		swdt.setIntnegsacct("");
		swdt.setIntnegsmeans("");
		swdt.setPriceexchterms("");
		swdt.setRepriceind("");
		swdt.setSecid("");
		swdt.setSettdays("");
		swdt.setSecccy("");
		swdt.setDivpaydateind("");
		swdt.setDivreinvestind("");
		swdt.setDivpayccyind("");
		swdt.setReturntype("");
		swdt.setValuationtime("");
		swdt.setRepricedays("");
		swdt.setExternalvalind("");
		swdt.setNotifdays("");
		swdt.setNotiftime("");
		swdt.setDealterms("");
		swdt.setFxratecode("");
		swdt.setExchmethod("");
		swdt.setAccrualccy("");
		swdt.setMktvalsource("");
		swdt.setAccrualmethod("");
		swdt.setGrossufpayrecind("");
		swdt.setRaterevday("");

		return swdt;
	}

	/**
	 * 
	 * 固定浮动腿对应的计划
	 * 
	 * @param swds
	 * @return
	 */
	public Swds DataTrans(Swds swds) {
		swds.setFixedintamt(BigDecimal.ZERO);// 固定利率金额
		swds.setIntauthind(OpicsConstant.SWAP.INTAUTHIND);// 利息授权标识
		swds.setIntauthoper(OpicsConstant.SWAP.INTAUTHOPER);// 利息授权用户
		swds.setIntexchrate8(BigDecimal.ZERO);//
		swds.setIntratecap8(BigDecimal.ZERO);// 利率上限
		swds.setIntratefloor8(BigDecimal.ZERO);// 利率下限
		swds.setPpayamt(BigDecimal.ZERO);// 付款金额
		swds.setPrinadjamt(BigDecimal.ZERO);// 本金递减金额
		swds.setRaterevfrstlst(OpicsConstant.SWAP.RATEREVFRSTLST);//
		swds.setSpread8(BigDecimal.ZERO);// 点差利率
		swds.setSteprate8(BigDecimal.ZERO);// 步骤利率
		swds.setPvamt(BigDecimal.ZERO);// 目前价值金额
		swds.setSpreadamt(BigDecimal.ZERO);// 点差金额
		swds.setPvbamt(BigDecimal.ZERO);// 价值折本币金额
		swds.setStrike8(BigDecimal.ZERO);// 行权价
		swds.setIndex8(BigDecimal.ZERO);// 指数利率
		swds.setParticperc8(BigDecimal.ZERO);// 百分比价格
		swds.setCollarind(OpicsConstant.SWAP.COLLARIND);// COLL计算标识
		swds.setExchrate8(BigDecimal.ZERO);// 汇率
		swds.setExchamt(BigDecimal.ZERO);// 汇率金额
		swds.setAmount1(BigDecimal.ZERO);// 备注1
		swds.setAmount2(BigDecimal.ZERO);// 备注2
		swds.setAmount3(BigDecimal.ZERO);// 备注3
		swds.setRate18(BigDecimal.ZERO);//
		swds.setRate28(BigDecimal.ZERO);//
		swds.setFactor12(BigDecimal.ZERO);//
		swds.setFlatintcompamt(BigDecimal.ZERO);//
		swds.setSumcompoundamt(BigDecimal.ZERO);//
		swds.setRemainingpay(BigDecimal.ZERO);//
		swds.setSettle(OpicsConstant.SWAP.SETTLE);//
		swds.setTaxamt(BigDecimal.ZERO);//
		swds.setInternalrate8(BigDecimal.ZERO);//
		swds.setCorpspread8(BigDecimal.ZERO);//
		swds.setCorpspreadamt(BigDecimal.ZERO);//
		swds.setRate38(BigDecimal.ZERO);// 备用4
		swds.setRate48(BigDecimal.ZERO);// 备用5
		swds.setAmount4(BigDecimal.ZERO);// 备用8
		swds.setAmount5(BigDecimal.ZERO);// 备用9
		swds.setAccrint(BigDecimal.ZERO);// 应计利息
		swds.setConvprice12(BigDecimal.ZERO);//
		swds.setPrice12(BigDecimal.ZERO);//
		swds.setPriceexchrate8(BigDecimal.ZERO);//
		swds.setRateofreturn12(BigDecimal.ZERO);//
		swds.setDivmktmktamt(BigDecimal.ZERO);//
		swds.setDivpayamt(BigDecimal.ZERO);//
		swds.setDivpercent8(BigDecimal.ZERO);//
		swds.setUndintpayamt(BigDecimal.ZERO);//
		swds.setGroupid(BigDecimal.ZERO);//
		swds.setFormulaid(BigDecimal.ZERO);//
		swds.setFixingoverride(BigDecimal.ZERO);//
		swds.setShareadjrate8(BigDecimal.ZERO);//
		swds.setPpayexchrate8(BigDecimal.ZERO);//
		swds.setCalcrule("");
		swds.setIntexchterms("");
		swds.setPpayauthind("");
		swds.setPpayauthoper("");
		swds.setPpayccy("");
		swds.setPrinratecode("");
		swds.setPs("");
		swds.setPrinsettmeans("");
		swds.setPrinsettacct("");
		swds.setChar1("");
		swds.setChar2("");
		swds.setChar3("");
		swds.setFormulaind("");
		swds.setMbspayday("");
		swds.setIntdeal("");
		swds.setTaxddealno("");
		swds.setTaxdproduct("");
		swds.setTaxdprodtype("");
		swds.setTaxdseq("");
		swds.setExchterms("");
		swds.setFlag2("");
		swds.setFlag3("");
		swds.setCcytax("");
		swds.setPriceexchterms("");
		swds.setIntnegauthoper("");
		swds.setIntnegsacct("");
		swds.setIntnegsmeans("");
		swds.setDivpaydateind("");
		swds.setEquitytype("");
		swds.setShareadjaccroptind("");
		swds.setPpayexchterms("");
		
		return swds;
	}

	/**
	 * 互换添加交易单号
	 *
	 * @param swdh
	 * @param mcfpService
	 * @return
	 */
	public Swdh DataTrans(Swdh swdh, McfpService mcfpService) {
		Mcfp mcfp = mcfpService.selectByPrimaryKey(swdh.getBr(), OpicsConstant.SWAP.PROD_CODE, OpicsConstant.SWAP.PROD_TYPE_SP);// 获取DEALNO
		String dealNo = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";
		swdh.setDealno(dealNo);
		return swdh;
	}

	/**
	 * 更新交易单号
	 *
	 * @param swdh
	 * @param mcfpService
	 * @param mcfp
	 */
	public void DataTrans(Swdh swdh, McfpService mcfpService, Mcfp mcfp) {
		mcfp.setBr(swdh.getBr());
		mcfp.setTraddno(StringUtils.trimToEmpty(swdh.getDealno()));
		mcfp.setProdcode(OpicsConstant.SWAP.PROD_CODE);// 产品代码 去掉空格
		mcfp.setType(OpicsConstant.SWAP.PROD_TYPE_SP);// 产品类型
		mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
	}

	/**
	 * 账务日期
	 *
	 * @param swdh
	 * @param brpsService
	 * @return
	 */
	public Swdh DataTrans(Swdh swdh, BrpsService brpsService) {
		if (null != swdh.getBrprcindte() && null != swdh.getLstmntdte()) {
			return swdh;
		}
		// 获取当前账务日期
		Brps brps = brpsService.selectByPrimaryKey(swdh.getBr());
		swdh.setBrprcindte(brps.getBranprcdate());// 当前账务日期
		swdh.setLstmntdte(brps.getBranprcdate());// 上次更新日期
		return swdh;
	}

	public void DataTrans(SwapWksBean irs, BrpsService brpsService) {
		Trad trad = null;
		Cusi cusi = null;

		// 获取当前账务日期
		Brps brps = brpsService.selectByPrimaryKey(irs.getInstId());

		irs.setBatchDate(brps.getBranprcdate());

		irs.setItemBeanSwds(new ArrayList<SwapItemSchdWksBean>());

		// 转换资金工作站SwapWksBean对象中的枚举值
		financialFieldMap.convertFieldvalue(irs);

		// 7天回购相关业务品种使用CNY SWAP,其它的使用ISDA
		if ("FR007".equalsIgnoreCase(irs.getPayRateCode()) || "FR007".equalsIgnoreCase(irs.getRecRateCode())) {
			irs.setPayCashFlowMethod("China");
			irs.setRecCashFlowMethod("China");
		} else {
			irs.setPayCashFlowMethod("ISDA");
			irs.setRecCashFlowMethod("ISDA");
		} // end if else

		// 将CFETS交易员转换为OPICS中的交易员
		trad = tradService.selectByCfetsTradId(irs.getInstId(), irs.getTrad());
		if (null != trad) {
			irs.setTrad(trad.getTrad());
		} // end if
			// 将CFETS交易员转换为OPICS中的交易员
		cusi = cusiService.selectByPrimaryKey(FieldMapType.CFETS.name(),irs.getPartyId());
		if (null != cusi) {
			irs.setCno(cusi.getCno());
		} // end if

		//转换设置默认值
		DataTrans(irs);
	}
	
	/**
	 * 转换利差
	 * @param br
	 * @param ccy
	 * @param floatBps
	 * @return
	 */
	private BigDecimal convertBps(String br,String ccy,BigDecimal floatBps) {
		Ccod ccod = null;
		int scaleLength;
		BigDecimal bpUnit;
		ccod = ccodService.selectByPrimaryKey(ccy);
		if(null == ccod) {
			throw new RuntimeException(ccy+"货币不支持!");
		}
		scaleLength = Integer.parseInt(StringUtils.trim(ccodService.selectByPrimaryKey(ccy).getDecs()));
		// 小货币点差除以100,其它货币除以1000
		if (scaleLength == 0 || scaleLength == 0) {
			bpUnit = FinancialCalculationModel.ONE_HUNDRED;
		} else {
			bpUnit = FinancialCalculationModel.TEN_THOUSAND;
		}
		return null != floatBps && BigDecimal.ZERO.compareTo(floatBps) != 0 ? floatBps.divide(bpUnit, 8, RoundingMode.HALF_UP) : floatBps;// BP点差

	}

	/**
	 * 获取利率代码
	 * @param br
	 * @param ccy
	 * @param rateType
	 * @param ratesourceIsda
	 * @return
	 */
	private String getRateCode(String br,String ccy,String rateType,String ratesourceIsdaOrRateCode) {
		
		String varfix = null;
		String rateCode = null;
		Rate rate = null;
		
		LogFactory.getLog(SwapEntry.class).info(String.format("网点：%s,货币：%s,利率类型：%s,接口利率代码参数：%s", br,ccy,rateType,ratesourceIsdaOrRateCode));
		
		if("L".equalsIgnoreCase(rateType)) {
			
			//先查询RATE表主键
			rate = rateService.selectByPrimaryKey(br, ratesourceIsdaOrRateCode);
			
			//如果为空再查ratesourceIsda字段
			if(null == rate) {
				rate = rateService.selectByIsda(br, ccy, ratesourceIsdaOrRateCode);
			}
			
			if(null == rate) {
				throw new RuntimeException("浮动利率代码不存在!");
			}
			rateCode = StringUtils.trim(rate.getRatecode());
			
		}else if("X".equalsIgnoreCase(rateType)){
			
			varfix = "F";
			rate = rateService.selectByCcy(br,ccy, varfix);
			
			if(null == rate) {
				throw new RuntimeException("固定利率代码不存在!");
			}
			rateCode = StringUtils.trim(rate.getRatecode());
		}else {
			throw new RuntimeException("利率类型不支持!");
		}
		
		if(null == rateCode ||rateCode.trim().length() == 0) {
			throw new RuntimeException("参考利率映射数据为空");
		}
		
		return rateCode;
	}
	
	/**
	 * 转换对象中的首期,到期SCHDTYPE对应的值
	 * @param listItem
	 */
	public void DataTrans(SwapWksBean irs, List<SwapItemSchdWksBean> listItem) {

		// 转换资金工作站SwapWksBean对象中的枚举值
		financialFieldMap.convertFieldvalue(irs);

		for (SwapItemSchdWksBean swapItemSchdWksBean : listItem) {
			if (swapItemSchdWksBean.getEndDate().equalsIgnoreCase(irs.getVDate())) {
				swapItemSchdWksBean.setSchdType("V");
			} else if (swapItemSchdWksBean.getEndDate().equalsIgnoreCase(irs.getMDate())) {
				swapItemSchdWksBean.setSchdType("M");
			} // end if else if
		} // for
		
		//转换设置默认值
		DataTrans(irs);
	}

	/**
	 * 方法已重载.在调生成现金流方法之前,初始化计算现金流时所需要的字段值,设置系统当前日期
	 * 
	 * @param irs
	 */
	public void DataTrans(SwapWksBean irs) {
		
		if ("X".equalsIgnoreCase(irs.getPayRateType())) {
			irs.setPaySeq("001");
			irs.setRecSeq("002");
			if("B".equalsIgnoreCase(irs.getRecPaymentFrequency())) {
				irs.setRecInterestResetFrequency(irs.getRecPaymentFrequency());
			}
		} else if ("X".equalsIgnoreCase(irs.getRecRateType())) {
			irs.setRecSeq("001");
			irs.setPaySeq("002");
			if("B".equalsIgnoreCase(irs.getPayPaymentFrequency())) {
				irs.setPayInterestResetFrequency(irs.getPayPaymentFrequency());
			}
		} else {
			throw new RuntimeException("利率类型枚举不支持!");
		} // end if elseif else

		if (null == irs.getPayNominalAmount() || BigDecimal.ZERO.equals(irs.getPayNominalAmount())) {
			irs.setPayNominalAmount(new BigDecimal("-" + irs.getAmt()));
		} // end if

		if (null == irs.getRecNominalAmount() || BigDecimal.ZERO.equals(irs.getRecNominalAmount())) {
			irs.setRecNominalAmount(irs.getAmt());
		} // end if
		
		if(StringUtils.isEmpty(irs.getPayCcy())) {
			irs.setPayCcy(irs.getCcy());
		}
		
		if(StringUtils.isEmpty(irs.getRecCcy())) {
			irs.setRecCcy(irs.getCcy());
		}
		
		// 11.检查交易日
		if(FinancialDateUtils.parse(irs.getDealDate()).compareTo(irs.getBatchDate()) > 0) {
			throw new RuntimeException("交易日期必须小于OPICS系统日期,OPICS当前系统日期为:"+FinancialDateUtils.format(irs.getBatchDate()));
		}
		
		// 12.检查起息日
		if(FinancialDateUtils.parse(irs.getVDate()).compareTo(FinancialDateUtils.parse(irs.getDealDate())) < 0) {
			throw new RuntimeException("起息日期必须大于等于交易日期!");
		}
		
		// 13.检查到期日期
		if(FinancialDateUtils.parse(irs.getMDate()).compareTo(FinancialDateUtils.parse(irs.getVDate())) <= 0) {
			throw new RuntimeException("到期日期必须大于起息日期!");
		}
		
		// 14.检查起息日必须大于付息周期
		Date endDate = FinancialDateUtils.calculateNextDate(FinancialDateUtils.parse(irs.getVDate()),FinancialDateUtils.parse(irs.getMDate()), irs.getRecPaymentFrequency());
		if (!"B".equalsIgnoreCase(irs.getRecPaymentFrequency()) && FinancialDateUtils.parse(irs.getMDate()).compareTo(endDate) < 0) {
			throw new RuntimeException("收取端到期日必须大于等于首次付息日!");
		}
		endDate = FinancialDateUtils.calculateNextDate(FinancialDateUtils.parse(irs.getVDate()),FinancialDateUtils.parse(irs.getMDate()),  irs.getPayPaymentFrequency());
		if (!"B".equalsIgnoreCase(irs.getPayPaymentFrequency()) && FinancialDateUtils.parse(irs.getMDate()).compareTo(endDate) < 0) {
			throw new RuntimeException("支付端到期日必须大于等于首次付息日!");
		}
		// 获取付端利率代码
		irs.setPayRateCode(getRateCode(irs.getInstId(), irs.getPayCcy(), irs.getPayRateType(), irs.getPayRateCode()));

		// 获取收端利率代码
		irs.setRecRateCode(getRateCode(irs.getInstId(), irs.getRecCcy(), irs.getRecRateType(), irs.getRecRateCode()));
		
		//BP除以1000
		irs.setPayFloatBps(convertBps(irs.getInstId(), irs.getPayCcy(), irs.getPayFloatBps()));
		//BP除以1000
		irs.setRecFloatBps(convertBps(irs.getInstId(), irs.getRecCcy(), irs.getRecFloatBps()));

	}

	/**
	 * 方法已重载.转换成OPICS数据库对象之前,初始化或者转换相关字段,处理到期日是否需要顺延
	 * 
	 * @param irs
	 * @param holidayDates
	 */
	public void DataTrans(SwapWksBean irs, Map<String, HolidayBean> holidayDates) {
		Date mdate = FinancialDateUtils.parse(irs.getMDate());

		if ("China".equalsIgnoreCase(irs.getPayCashFlowMethod()) || "China".equalsIgnoreCase(irs.getRecCashFlowMethod())) {
			// 获取人民币对应的假日
			HolidayBean holiday = holidayDates.get("CNY");
			mdate = FinancialDateUtils.calculateNextWorkDate(mdate, holiday, FinancialDateUtils.dateConvertMapping(DateConventionType.PAYMENT, irs.getIntPayAdjMod()));
		}
		// 设置顺延后的到期日
		irs.setMDate(FinancialDateUtils.format(mdate));

	}

	/**
	 * 互换交易数据转换
	 *
	 * @param swdh
	 * @param irs
	 * @return
	 */
	public Swdh DataTrans(Swdh swdh, SwapWksBean irs) {
		swdh.setBr(irs.getInstId());// 核算机构
		String[] book = StringUtils.split(irs.getBook(), "_");
		swdh.setProduct(book[0]);// 产品代码
		swdh.setProdtype(book[1]);// 产品类型代码
		swdh.setPort(irs.getDesk());// 投资组合
		swdh.setCost(book[2]);// 成本中心
		swdh.setCno(irs.getCno());// 交易对手编号
		swdh.setTrad(StringUtils.defaultIfEmpty(irs.getTrad(), OpicsConstant.SWAP.TRAD_NO));// 交易员
		swdh.setDealdate(DateUtil.parse(irs.getDealDate()));// 交易日期
		swdh.setMatdate(DateUtil.parse(irs.getMDate()));// 到期日
		swdh.setStartdate(DateUtil.parse(irs.getVDate()));// 开始日期
		swdh.setVerdate(irs.getBatchDate());// 复核日期
		swdh.setSettledate(DateUtil.parse(irs.getVDate()));// 结算日期：起息日
		swdh.setBrprcindte(irs.getBatchDate());
		
		/**
		 * 6为集中清算,其它的为全额清算
		 */
		if("6".equalsIgnoreCase(irs.getPayAcctType()) || "6".equalsIgnoreCase(irs.getRecAcctType())) {
			swdh.setDealtext(String.format("%s|%s|SL_%s_IRS", StringUtils.trim(irs.getTradeNo()),"CCP", irs.getFieldMapType().name()));
		}else if("9".equalsIgnoreCase(irs.getPayAcctType()) || "9".equalsIgnoreCase(irs.getRecAcctType())) {
			//代客交易由接口拼接
			swdh.setDealtext(irs.getRemark());
		}else {
			swdh.setDealtext(String.format("%s|SL_%s_IRS", StringUtils.trim(irs.getTradeNo()), irs.getFieldMapType().name()));
		}
		
		if ("X".equalsIgnoreCase(irs.getPayRateType())) {
			swdh.setMinlegseq(irs.getPaySeq());// 最小腿序号
			swdh.setMaxlegseq(irs.getRecSeq());// 最大腿序号
		} else if ("X".equalsIgnoreCase(irs.getRecRateType())) {
			swdh.setMinlegseq(irs.getRecSeq());// 最小腿序号
			swdh.setMaxlegseq(irs.getPaySeq());// 最大腿序号
		} else {
			throw new RuntimeException("利率类型枚举不支持!");
		}
		
		swdh.setSwaptype(irs.getSwapType());

		return swdh;
	}

	private void DataTrans(Swdt swdtleg, Swdh swdh, SwapWksBean swapWksBean) {

		swdtleg.setBr(swdh.getBr());// 部门
		swdtleg.setDealno(swdh.getDealno());// 交易单号
		swdtleg.setDealind(swdh.getDealind());// 标识
		swdtleg.setProduct(swdh.getProduct());// 产品类型代码
		swdtleg.setProdtype(swdh.getProdtype());// 产品类型代码
		swdtleg.setIntauthdte(swdh.getBrprcindte());// 利息结算指令授权日期
		swdtleg.setStartdate(swdh.getStartdate());// 开始日期
		swdtleg.setMatdate(swdh.getMatdate());// 到期日

		swdtleg.setIntpayadjust("Y");// 付款本金规则暂时用Y,情况复杂后会改的
		swdtleg.setSchdflag(FinancialDateUtils.dateConvertMapping(DateConventionType.INTEREST, swapWksBean.getIntPayAdjMod()));// 计划标志
		swdtleg.setIntccy(swapWksBean.getCcy());// 利息币种
		swdtleg.setNotccy(swapWksBean.getCcy());// 名义币种
		swdtleg.setIntpaypayrule(swapWksBean.getIntPayAdjMod());// 付息日确定规则:付息周期规则调整 D不调整/S延后/P向前/M延后，遇月末向前
		swdtleg.setUpfrontfeenettype(OpicsConstant.SWAP.UPFRONTFEENETTYPE);// 费用类型
	}

	private void DataTrans(Swdh swdh, Swdt swdt, String calIntBasic, String calIntType, String rateType, String paymentFrequency, BigDecimal fixedRate, String acctNO, String AcctType, String rateCode,
			String raterevpayrule, BigDecimal floatBps, String cashFlowMethod, String yieldcurve, String payRecInd, BigDecimal nosCcyAmt, String seq, String interestResetFrequency,String discountYieldcurve,
			String ccy,BigDecimal bp) {
		
		Ychd ychd = null;
		
		swdt.setBasis(calIntBasic);// 计息基数代码
		swdt.setCompound(calIntType);// 复利标志:M=复利/N=单利
		swdt.setFixfloatind(rateType);// 固定利率浮动利率标志
		swdt.setIntpaycycle(paymentFrequency);// 付息周期代码
		swdt.setIntrate8(fixedRate);// 利率
		swdt.setIntsacct(acctNO);// 利息结算帐户
		swdt.setIntsmeans(AcctType);// 利息结算方式
		swdt.setRatecode(rateCode);// 利率代码
		swdt.setRaterevpayrule("L".equalsIgnoreCase(rateType) ? "D" : "");// 利率重置规则
		swdt.setSpread8(floatBps);
		swdt.setPremdisc(FinancialCalculationModel.ZERO);// 溢价/折价
		swdt.setLegtype(rateType);//
		swdt.setSchdconv(cashFlowMethod);// 调整规则
		
		if(!org.springframework.util.StringUtils.hasText(yieldcurve)) {
			throw new RuntimeException("收益率曲线不能为空!");
		}
		swdt.setYieldcurve(org.springframework.util.StringUtils.hasText(discountYieldcurve) ? discountYieldcurve : yieldcurve);// 曲线
		ychd = ychdService.selectByPrimaryKey(swdt.getBr(), ccy, swdt.getYieldcurve());
		if(null == ychd) {
			throw new RuntimeException(swdt.getYieldcurve()+"曲线不支持!");
		}
		swdt.setFwdfwdyieldcurve(yieldcurve);
		ychd = ychdService.selectByPrimaryKey(swdt.getBr(), ccy, swdt.getFwdfwdyieldcurve());
		if(null == ychd) {
			throw new RuntimeException(swdt.getFwdfwdyieldcurve()+"曲线不支持!");
		}
		swdt.setNotccyamt(nosCcyAmt);
		swdt.setSeq(seq);
		swdt.setPayrecind(payRecInd);
		swdt.setRaterevcycle(interestResetFrequency);
		
		swdt.setNotccy(ccy);
		swdt.setIntccy(ccy);
	}
	
	

	/**
	 * 获取重定日期
	 * 
	 * @param itemBeanSwds
	 * @return
	 */
	private void setRaterevdte(List<SwapItemSchdWksBean> itemBeanSwds, Swdt swdt, String rateType,Date vdate) {

		if ("X".equalsIgnoreCase(rateType)) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(vdate);
			// 固定端有，浮动端为空
			swdt.setIntpayday(String.valueOf("W".equalsIgnoreCase(swdt.getIntpaycycle()) ? cal.get(Calendar.DAY_OF_WEEK)-1 : cal.get(Calendar.DAY_OF_MONTH)));// 付息日
			swdt.setNxtraterev(swdt.getMatdate());
			return;
		}
		
		swdt.setIntpayday("");// 付息日

		Collections.sort(itemBeanSwds, new Comparator<SwapItemSchdWksBean>() {
			@Override
			public int compare(SwapItemSchdWksBean o1, SwapItemSchdWksBean o2) {
				int v1 = 0;
				int v2 = 0;
				if (o1.getRateType().equalsIgnoreCase(o2.getRateType())) {
					v1 = Integer.parseInt(o1.getSchdSeq());
					v2 = Integer.parseInt(o2.getSchdSeq());
				}
				return v1 == v2 ? 0 : v1 > v2 ? -1 : 1;
			}
		});
		for (SwapItemSchdWksBean schd : itemBeanSwds) {
			if ("L".equalsIgnoreCase(schd.getRateType()) && schd.getInterestRate().compareTo(FinancialCalculationModel.ZERO) != 0) {
				swdt.setRaterevdte(FinancialDateUtils.parse(schd.getActualFixingDate()));// 利率重置日期
				break;
			} else if ("L".equalsIgnoreCase(schd.getRateType()) && schd.getInterestRate().compareTo(FinancialCalculationModel.ZERO) == 0
					&& "002".equalsIgnoreCase(schd.getSchdSeq())) {
				swdt.setRaterevdte(vdate);// 利率重置日期
				break;
			} // end if
		} // end for
	}

	/**
	 * 互换交易浮动/固定端对应的数据转换
	 * 
	 * @param swdtLst
	 * @param swdh
	 * @param swapWksBean
	 * @return
	 */
	public void DataTrans(List<Swdt> swdtLst, Swdh swdh, SwapWksBean swapWksBean) {
		Swdt swdt = null;
		
		
		// 添加付端
		swdt = DataTrans(new Swdt());// 静态数据端
		DataTrans(swdt, swdh, swapWksBean);
		DataTrans(swdh, swdt, swapWksBean.getPayCalIntBasic(), swapWksBean.getPayCalIntType(), swapWksBean.getPayRateType(), 
				swapWksBean.getPayPaymentFrequency(), swapWksBean.getPayFixedRate(),swapWksBean.getPayAcctNo(), 
				getSettmeans(swapWksBean.getInstId(),swapWksBean.getCcy(),swapWksBean.getPayAcctNo()), swapWksBean.getPayRateCode(), 
				swapWksBean.getPayRaterevpayrule(), swapWksBean.getPayFloatBps(),swapWksBean.getPayCashFlowMethod(), 
				swapWksBean.getPayYieldcurve(), SWAP.PAYRECIND_P, swapWksBean.getPayNominalAmount(), swapWksBean.getPaySeq(),
				swapWksBean.getPayInterestResetFrequency(),swapWksBean.getPayDiscountYieldcurve(),swapWksBean.getPayCcy(),
				swapWksBean.getPayFloatBps());
		setRaterevdte(swapWksBean.getItemBeanSwds(), swdt, swapWksBean.getPayRateType(),swdh.getStartdate());
		if(OpicsConstant.SWAP.SWAPTYPE_C.equalsIgnoreCase(swdh.getSwaptype())) {
			LogFactory.getLog(SwapEntry.class).debug(String.format("支付端本金互换信息,PayFinExchAmt=%19.2f,PayFinExchCcy=%s,PayFinExchDate=%s,PayInitExchAmt=%19.2f,PayInitExchCcy=%s,PayInitExchDate=%s", 
					swapWksBean.getPayFinExchAmt(),swapWksBean.getPayFinExchCcy(),swapWksBean.getPayFinExchDate(),
					swapWksBean.getPayInitExchAmt(),swapWksBean.getPayInitExchCcy(),swapWksBean.getPayInitExchDate()));
			//添加货币互换付端字段
			DataTrans(swdh,swdt,swapWksBean.getExchPrin(),swapWksBean.getPayFinExchAmt(),swapWksBean.getPayFinExchCcy(),swapWksBean.getPayFinExchDate(),
					swapWksBean.getPayInitExchAmt(),swapWksBean.getPayInitExchCcy(),swapWksBean.getPayInitExchDate());
		}
		swdtLst.add(swdt);
		
		LogFactory.getLog(SwapEntry.class).debug("SWDT PAY:"+swdt);

		// 添加收端
		swdt = DataTrans(new Swdt());
		DataTrans(swdt, swdh, swapWksBean);
		DataTrans(swdh, swdt, swapWksBean.getRecCalIntBasic(), swapWksBean.getRecCalIntType(), swapWksBean.getRecRateType(), 
				swapWksBean.getRecPaymentFrequency(), swapWksBean.getRecFixedRate(),swapWksBean.getRecAcctNo(), 
				getSettmeans(swapWksBean.getInstId(),swapWksBean.getCcy(),swapWksBean.getRecAcctNo()), swapWksBean.getRecRateCode(), 
				swapWksBean.getRecRaterevpayrule(), swapWksBean.getRecFloatBps(),swapWksBean.getRecCashFlowMethod(), 
				swapWksBean.getRecYieldcurve(), SWAP.PAYRECIND_R, swapWksBean.getRecNominalAmount(), swapWksBean.getRecSeq(),
				swapWksBean.getRecInterestResetFrequency(),swapWksBean.getRecDiscountYieldcurve(),swapWksBean.getRecCcy(),
				swapWksBean.getRecFloatBps());
		setRaterevdte(swapWksBean.getItemBeanSwds(), swdt, swapWksBean.getRecRateType(),swdh.getStartdate());
		if(OpicsConstant.SWAP.SWAPTYPE_C.equalsIgnoreCase(swdh.getSwaptype())) {
			LogFactory.getLog(SwapFactory.class).debug(String.format("收取端本金互换信息,RecFinExchAmt=%19.2f,RecFinExchCcy=%s,RecFinExchDate=%s,RecInitExchAmt=%19.2f,RecInitExchCcy=%s,RecInitExchDate=%s", 
					swapWksBean.getRecFinExchAmt(),swapWksBean.getRecFinExchCcy(),swapWksBean.getRecFinExchDate(),
					swapWksBean.getRecInitExchAmt(),swapWksBean.getRecInitExchCcy(),swapWksBean.getRecInitExchDate()));
			//添加货币互换收端字段
			DataTrans(swdh,swdt,swapWksBean.getExchPrin(),swapWksBean.getRecFinExchAmt(),swapWksBean.getRecFinExchCcy(),swapWksBean.getRecFinExchDate(),
					swapWksBean.getRecInitExchAmt(),swapWksBean.getRecInitExchCcy(),swapWksBean.getRecInitExchDate());
		}
		LogFactory.getLog(SwapEntry.class).debug("SWDT REC:"+swdt);
		swdtLst.add(swdt);
	}
	
	/**
	 * 方法已重载.处理货币互换SWDT表字段
	 * @param swdh
	 * @param swdt
	 * @param ccyp
	 * @param exchPrin
	 * @param finexchamt
	 * @param finexchccy
	 * @param finexchdate
	 * @param intexchamt
	 * @param intexchccy
	 * @param intexchdate
	 */
	private void DataTrans(Swdh swdh,Swdt swdt,String exchPrin,BigDecimal finexchamt,String finexchccy,String finexchdate,BigDecimal intexchamt,String intexchccy,String intexchdate) {
		
		if(!org.springframework.util.StringUtils.hasText(exchPrin)) {
			throw new RuntimeException("本金交换方式[ "+exchPrin+" ]不能为空");
		}
		ExchPrin emExchPrin = ExchPrin.valueOf(exchPrin);
		if(null == emExchPrin) {
			throw new RuntimeException("本金交换方式枚举值[ "+exchPrin+" ]不支持");
		}
		//本金交换方式
		swdt.setFlag2(emExchPrin.name());
		
		switch (emExchPrin) {
		case V:
			setInitInfo(swdh,swdt,intexchamt,intexchccy,intexchdate);
			break;
		case M:
			setFinInfo(swdh,swdt,finexchamt,finexchccy,finexchdate);
			break;
		case B:
			setInitInfo(swdh,swdt,intexchamt,intexchccy,intexchdate);
			setFinInfo(swdh,swdt,finexchamt,finexchccy,finexchdate);
			break;
		case N:
			break;
		default:
			break;
		}
	}
	
	/**
	 * 设置期初本金交易相关字段
	 * @param swdh
	 * @param swdt
	 * @param intexchamt
	 * @param intexchccy
	 * @param intexchdate
	 */
	private void setInitInfo(Swdh swdh,Swdt swdt,BigDecimal intexchamt,String intexchccy,String intexchdate) {
		Revp revp = null;
		Ccod ccod = null;
		int scaleLength = 2;
		BigDecimal neg_one = new BigDecimal("-1");
		
		swdt.setInitexchamt("P".equalsIgnoreCase(swdt.getPayrecind()) ? intexchamt.abs() : "R".equalsIgnoreCase(swdt.getPayrecind()) ? neg_one.multiply(intexchamt.abs()) : BigDecimal.ZERO);
		revp = revpService.selectByPrimaryKey(swdh.getBr(), intexchccy);
		if(null == revp) {
			throw new RuntimeException("未找到期初本金交换货币对应的市场汇率");
		}
		ccod = ccodService.selectByPrimaryKey(intexchccy);
		if(null == ccod) {
			throw new RuntimeException("期初交割货币不支持");
		}
		scaleLength = Integer.parseInt(StringUtils.trim(ccod.getDecs()));
		swdt.setInitexchbamt(swdt.getInitexchamt().multiply(revp.getSpotrate8()).setScale(scaleLength, RoundingMode.HALF_UP));
		swdt.setInitexchdate(DateUtil.parse(intexchdate));
		swdt.setInitexchauthdte(swdh.getBrprcindte());
		swdt.setInitexchauthind("1");
		swdt.setInitexchccy(intexchccy);
		swdt.setInitexchsmeans(swdt.getIntsmeans());
		swdt.setInitexchsacct(swdt.getIntsacct());
		swdt.setInitexchoper("SYS1");
	}
	
	/**
	 * 设置到期本金交换字段
	 * @param swdh
	 * @param swdt
	 * @param finexchamt
	 * @param finexchccy
	 * @param finexchdate
	 */
	private void setFinInfo(Swdh swdh,Swdt swdt,BigDecimal finexchamt,String finexchccy,String finexchdate) {
		Revp revp = null;
		Ccod ccod = null;
		int scaleLength = 2;
		BigDecimal neg_one = new BigDecimal("-1");
		
		swdt.setFinexchamt("P".equalsIgnoreCase(swdt.getPayrecind()) ? neg_one.multiply(finexchamt.abs()) : "R".equalsIgnoreCase(swdt.getPayrecind()) ? finexchamt.abs() : BigDecimal.ZERO);
		revp = revpService.selectByPrimaryKey(swdh.getBr(), finexchccy);
		if(null == revp) {
			throw new RuntimeException("未找到到期本金交换货币对应的市场汇率");
		}
		ccod = ccodService.selectByPrimaryKey(finexchccy);
		if(null == ccod) {
			throw new RuntimeException("到期交割货币不支持");
		}
		scaleLength = Integer.parseInt(StringUtils.trim(ccod.getDecs()));
		swdt.setFinexchbamt(swdt.getFinexchamt().multiply(revp.getSpotrate8()).setScale(scaleLength, RoundingMode.HALF_UP));
		swdt.setFinexchdate(DateUtil.parse(finexchdate));
		swdt.setFinexchauthdte(swdh.getBrprcindte());
		swdt.setFinexchauthind("1");
		swdt.setFinexchccy(finexchccy);
		swdt.setFinexchsmeans(swdt.getIntsmeans());
		swdt.setFinexchsacct(swdt.getIntsacct());
		swdt.setFinexchoper("SYS1");

		swdt.setAmount4(swdt.getFinexchamt());
	}
	
	
	/**
	 * 根据清算路径查询清算方式
	 * @param br
	 * @param ccy
	 * @param settacct
	 * @return
	 */
	private String getSettmeans(String br, String ccy, String settacct) {
		String settmeans = null;

		//查询SETA、NOST中是否有清算信息
		Seta seta = setaService.selectBySettacct(br,settacct);
		if (null == seta || StringUtils.isEmpty(seta.getSmeans())) {
			throw new RuntimeException("清算方式不能为空!");
		}

		//设置清算方式
		settmeans = seta.getSmeans();

		return settmeans;
	}

	/**
	 * 方法已重载.转换OPICS现金流表SWDS信息
	 * 
	 * @param swdsLst
	 * @param swdh
	 * @param swdhLeg
	 * @param swapWksBean
	 * @return
	 */
	public List<Swds> DataTrans(List<Swds> swdsLst, Swdh swdh, List<Swdt> swdhLeg, SwapWksBean swapWksBean) {
		
		if (swapWksBean.getItemBeanSwds() == null) {
			return null;
		}
		for (SwapItemSchdWksBean item : swapWksBean.getItemBeanSwds()) {
			Swds swds = DataTrans(new Swds());
			swds.setBr(swdh.getBr());// 部门
			swds.setDealno(swdh.getDealno());// 交易单号
			swds.setSeq(item.getSeq());// 顺序号
			swds.setDealind(swdh.getDealind());// 标识
			swds.setProduct(swdh.getProduct());// 产品类型代码
			swds.setProdtype(swdh.getProdtype());// 产品类型代码
			swds.setIntenddte(DateUtil.parse(item.getEndDate()));// 利息截至日期
			swds.setSchdseq(item.getSchdSeq());// 腿序号下条数
			swds.setSchdtype(item.getSchdType());// 类型标识
			swds.setBasis(item.getBasis());// 计息基础
			swds.setBrprcindte(swdh.getBrprcindte());// 账务日期
			swds.setCompound(item.getCompound());// 复利标志:M=复利/N=单利
			swds.setFixfloatind(item.getRateType());// 固定/浮动
			swds.setImplintamt(item.getImplintAmt());// 远期利率利息金额
			swds.setImplintrate8(item.getImplintRate8());// 远期利率
			swds.setIntamt(item.getInterestAmt());// 利息金额
			swds.setIntauthdate(swdh.getBrprcindte());// 利息授权时间
			swds.setIntccy(item.getCcy());// 利息币种
			swds.setIntrate8(item.getInterestRate());// 利率
			swds.setIntsacct(item.getIntsacct());// 清算账号
			swds.setIntsmeans(swdhLeg.stream().filter(swdt -> swdt.getSeq().equalsIgnoreCase(item.getSeq())).findFirst().get().getIntsmeans());// 清算方式
			swds.setIntstrtdte(DateUtil.parse(item.getBeginDate()));// 利息开始日期
			swds.setIpaydate(DateUtil.parse(item.getPayDate()));// 付款日期
			swds.setLstmntdte(swdh.getBrprcindte());// 最后修改日期
			swds.setNetipay(item.getNetipayAmt() == null ? BigDecimal.ZERO : item.getNetipayAmt());// 收付利息差额
			swds.setPayrecind(item.getTradeDirection());// 收/付款标识
			swds.setPosnintamt(item.getPosnintamt());// 货币头寸利息金额
			swds.setPrinamt(item.getPrinAmt());// 名义本金
			swds.setPrinccy(item.getCcy());// 名义币种
			swds.setPrinccyintamt(item.getInterestAmt());// 名义币种利息金额
			swds.setRatecode(item.getRateCode());// 利率代码
			swds.setIntnegauthind("0");
			swds.setVerdate(FinancialDateUtils.parse(item.getVerdate()));// 复核日期
			swds.setFlatintcompamt(item.getFlatintcompamt());//复利与单利差额
			swds.setSumcompoundamt(item.getSumcompoundamt());//复利汇总金额

			// X-固定 L-浮动
			if ("L".equals(item.getRateType())) {
				// 利率重定日期
				swds.setRatefixdte(DateUtil.parse(item.getFixingDate()));
				// 利率确认日期
				swds.setRaterevdte(DateUtil.parse(item.getActualFixingDate()));
				// 当利息金额有值时重定字段填1,通过SWRF界面重定的才需要赋值
//				if (null != swds.getIntamt() && !swds.getIntamt().equals(BigDecimal.ZERO)) {
//					swds.setFixingoverride(BigDecimal.ONE);
//				} // end if
			} // end if else
			
			//设置起息日的复核日期为当前批量日期
			if("V".equalsIgnoreCase(swds.getSchdtype())) {
				swds.setVerdate(swdh.getBrprcindte());
			}

			swdsLst.add(swds);
		}

		return swdsLst;
	}
	
	/**
	 * 转换提前到期对象
	 * @param swap
	 * @param etSwap
	 */
	public void DataTrans(SwapWksBean swap,SwapWksBean etSwap) {
		BeanUtils.copyProperties(swap, etSwap);
		//转换到期日为提前终止日
		etSwap.setMDate(swap.getEarlyDate());
		//收付端货币相同时为利率互换,不同时为货币互换
		etSwap.setSwapType(swap.getPayCcy().equalsIgnoreCase(swap.getRecCcy()) ? OpicsConstant.SWAP.SWAPTYPE_I : OpicsConstant.SWAP.SWAPTYPE_C);
		LogFactory.getLog(SwapEntry.class).info("利率互换提前终止SwapWksBean:"+etSwap);
	}
	
	/**
	 * 转换并组装提前终止操作相关的字段
	 * @param swdh
	 * @param orgSwds
	 * @param swdhLeg
	 * @param swapWksBean
	 * @param swdtLegs
	 */
	public void DataTrans(Swdh swdh, List<Swdt> swdhLeg, List<Swds> orgSwds, SwapWksBean swap, List<Swds> swdtLegs, SwapWksBean etSwap) {
		Optional<Swds> opSwds = null;
		// 处理SWDT
		swdhLeg.stream().forEach(swdt -> {
			swdt.setMatdate(DateUtil.parse(swap.getEarlyDate()));
			if("X".equalsIgnoreCase(swdt.getFixfloatind())) {
				swdt.setNxtraterev(null);
			}else if("L".equalsIgnoreCase(swdt.getFixfloatind())) {
				swdt.setRaterevdte(null);
			}else {
				throw new RuntimeException("Fixfloatind类型不支持");
			}
			swdt.setDate2(swdh.getMatdate());
		});
		
		// 处理SWDS
		//更新固定端
		Optional<SwapItemSchdWksBean> opFixedItem = etSwap.getItemBeanSwds().stream().filter(item -> "X".equalsIgnoreCase(item.getRateType()) && "M".equalsIgnoreCase(item.getSchdType())).findFirst();
		DataTrans(orgSwds,swdtLegs,opFixedItem);
		//更新浮动端
		Optional<SwapItemSchdWksBean> opFloatItem = etSwap.getItemBeanSwds().stream().filter(item -> "L".equalsIgnoreCase(item.getRateType()) && "M".equalsIgnoreCase(item.getSchdType())).findFirst();
		DataTrans(orgSwds,swdtLegs,opFloatItem);

		// 处理SWDH
		swdh.setEtinputdate(etSwap.getBatchDate());
		swdh.setEtoper("SYS1");
		swdh.setLstmntdte(etSwap.getBatchDate());
		swdh.setOrigtermdate(swdh.getMatdate());
		swdh.setMatdate(DateUtil.parse(swap.getEarlyDate()));
		//减去利息累计金额
		swdh.setEtamt(etSwap.getEarlyAmount().subtract(opFixedItem.get().getNetipayAmt()));
		swdh.setEtccy(etSwap.getEarlyCcy());
		swdh.setEtpayrecind(swdh.getEtamt().doubleValue() > 0 ? "R" : "P");
		swdh.setEtauthdte(etSwap.getBatchDate());
		swdh.setEtauthind("1");
		swdh.setEtauthoper("SYS1");
		Seta seta = setaService.selectBySettacct(swap.getInstId(),swap.getPayAcctNo());
		if(null == seta) {
			throw new RuntimeException("罚金清算路径不能为空");
		}
		swdh.setEtsmeans(StringUtils.trim(seta.getSmeans()));
		swdh.setEtsacct(StringUtils.trim(seta.getSacct()));
		swdh.setEtfeeintind("N");
		opSwds = swdtLegs.stream().filter(swds -> "M".equalsIgnoreCase(swds.getSchdtype()) && swds.getNetipay() != null || BigDecimal.ZERO.compareTo(swds.getNetipay()) != 0).findFirst();
		if (!opSwds.isPresent()) {
			throw new RuntimeException("获取利率互提前终止利息金异常");
		}
		swdh.setEtlegseq(opSwds.get().getSeq());
		swdh.setDealtext(StringUtils.trim(swdh.getDealtext())+swap.getRemark());

	}
	
	/**
	 * 更新到期现金流信息
	 * @param opOrgSwds
	 * @param opSwdsLeg
	 */
	private void DataTrans(List<Swds> orgSwdsList,List<Swds> swdtLegs,Optional<SwapItemSchdWksBean> opItem) {
		Swds orgSwds = null;
		Optional<Swds> opOrgSwds = null;
		
		if (!opItem.isPresent()) {
			throw new RuntimeException("获取利率互换到期现金流异常");
		}
		SwapItemSchdWksBean itemM = opItem.get();
		
		opOrgSwds = orgSwdsList.stream().filter(swds -> StringUtils.trim(itemM.getRateType()).equalsIgnoreCase(StringUtils.trim(swds.getFixfloatind())) && StringUtils.trim(itemM.getSchdSeq()).equalsIgnoreCase(StringUtils.trim(swds.getSchdseq()))).findFirst();
		if (!opOrgSwds.isPresent()) {
			throw new RuntimeException("获取利率互提前终止现金流异常");
		}
		orgSwds = opOrgSwds.get();
		orgSwds.setSchdtype(itemM.getSchdType());
		orgSwds.setIntenddte(DateUtil.parse(itemM.getEndDate()));
		orgSwds.setIntamt(itemM.getInterestAmt());
		orgSwds.setIpaydate(DateUtil.parse(itemM.getPayDate()));
		orgSwds.setNetipay(itemM.getNetipayAmt());
		orgSwds.setPrinccyintamt(itemM.getInterestAmt());
		orgSwds.setRatefixdte(null);
		orgSwds.setRaterevdte(null);
		
		//添加需要更新的记录
		swdtLegs.add(orgSwds);
		
	}

}
