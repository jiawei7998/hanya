package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.Sfdc;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.expand.SettleInfoWksBean;
import com.singlee.financial.wks.mapper.opics.SfdcMapper;
import com.singlee.financial.wks.service.opics.SfdcService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
@Service
public class SfdcServiceImpl implements SfdcService {

    @Resource
    private SfdcMapper sfdcMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct,String dcc) {
        return sfdcMapper.deleteByPrimaryKey(br,cno,delrecind,product,prodtype,ccy,safekeepacct,dcc);
    }

    @Override
    public int insert(Sfdc record) {
        return sfdcMapper.insert(record);
    }

    @Override
    public int insertSelective(Sfdc record) {
        return sfdcMapper.insertSelective(record);
    }

    @Override
    public Sfdc selectByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct,String dcc) {
        return sfdcMapper.selectByPrimaryKey(br,cno,delrecind,product,prodtype,ccy,safekeepacct,dcc);
    }

    @Override
    public int updateByPrimaryKeySelective(Sfdc record) {
        return sfdcMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Sfdc record) {
        return sfdcMapper.updateByPrimaryKey(record);
    }

	@Override
	public void batchInsert(List<Sfdc> list) {
		if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.SfdcMapper.insert", list);
	}
	
	private Sfdc builder(String br, String product, String type, String cno,String ccy,
			String safekeepacct,  String dcc, String acctno,String delrecind,
	        String bic, String c1, String c2, String c3, String c4) {
	        // 付款报文对手方清算信息
			Sfdc sfdc = new Sfdc();
			sfdc.setBr(br);//机构:
			sfdc.setCno(cno);//交易对手:
			sfdc.setDelrecind(delrecind);//D/R:D付/R收
			sfdc.setProduct(product);//产品:
			sfdc.setProdtype(type);//产品类型:
			sfdc.setCcy(ccy);//币种:
			sfdc.setSafekeepacct(safekeepacct);//托管账户:
			sfdc.setDcc(dcc);//清算行区分:AW=账户行/BE=收款行/IN=中间行
			sfdc.setAccountno(acctno);//:
			sfdc.setBic(bic);//:
			sfdc.setC1(c1);//:
			sfdc.setC2(c2);//:
			sfdc.setC3(c3);//:
			sfdc.setC4(c4);//:
			sfdc.setLstmntdate(Calendar.getInstance().getTime());//:

	        return sfdc;
	    }
	
	private Sfdc builder(String br, String product, String type, String cno, String ccy) {
        return builder(br, product, type, cno, ccy,null, null, null,null, null, null, null, null, null);
    }
	
	private Sfdc addSfdc(Sfdc sfdc,String delrecind,String safekeepacct, String dcc, String bicCode, String bicType, String smeans) {
        if (null == bicCode || bicCode.trim().length() == 0) {
            // JY.raise("SWIFT CODE信息为空!");
            return sfdc;
        }
        if (null == smeans || smeans.trim().length() == 0) {
            JY.raise("清算方式为空!");
        }
        sfdc.setDcc(dcc);
        sfdc.setDelrecind(delrecind);
        sfdc.setSafekeepacct(safekeepacct);
        if ("NOS".equalsIgnoreCase(smeans)) {
        	sfdc.setBic(bicCode);
        } else {
        	sfdc.setC1("/" + bicType + "/" + bicCode);
        }
        return sfdc;
    }
	
	private void addSfdc(List<Sfdc> sfdcs, Sfdc sfdc, SettleInfoWksBean acct) {

        if (OpicsConstant.SFDC.PAYMENT_AW.equalsIgnoreCase(sfdc.getDcc())
            && (acct.getGuestCcyAwBiccode() == null || acct.getGuestCcyAwBiccode().trim().length() == 0)) {
            return;
        } else if (OpicsConstant.SFDC.PAYMENT_BE.equalsIgnoreCase(sfdc.getDcc())
            && (acct.getGuestCcyBeBiccode() == null || acct.getGuestCcyBeBiccode().trim().length() == 0)) {
            return;
        } else if (OpicsConstant.SFDC.PAYMENT_IN.equalsIgnoreCase(sfdc.getDcc())
            && (acct.getGuestCcyInBiccode() == null || acct.getGuestCcyInBiccode().trim().length() == 0)) {
            return;
        } else if (StringUtils.isEmpty(sfdc.getDcc())) {// 异常数据
            return;
        }

        sfdcs.add(sfdc);
    }

	@Override
	public List<Sfdc> builder(Spsh spsh, SettleInfoWksBean acct) {
		List<Sfdc> sfdcs = new ArrayList<Sfdc>();
        if (null == spsh) {
            JY.raise("现券买卖交易对象为空!");
        }

        if (null == acct) {
            JY.raise("现券买卖交易清算对象为空!");
        }
        // 添加交易对手账户行
        addSfdc(sfdcs,
        		addSfdc(builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getCno(), spsh.getCcy()),
        				"",spsh.getSecsacct(),
                OpicsConstant.SFDC.PAYMENT_AW, acct.getGuestCcyAwBiccode(), OpicsConstant.SFDC.FIELD_57J,
                spsh.getCcysmeans()),
            acct);
        // 添加交易对手信息
        addSfdc(sfdcs,
        		addSfdc(builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getCno(), spsh.getCcy()),
        				"",spsh.getSecsacct(),
                OpicsConstant.SFDC.PAYMENT_BE, acct.getGuestCcyBeBiccode(), OpicsConstant.SFDC.FIELD_58J,
                spsh.getCcysmeans()),
            acct);
        // 添加交易对手中间行
        addSfdc(sfdcs,
        		addSfdc(builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getCno(), spsh.getCcy()),
        				"",spsh.getSecsacct(),
                OpicsConstant.SFDC.PAYMENT_IN, acct.getGuestCcyInBiccode(), OpicsConstant.SFDC.FIELD_56J,
                spsh.getCcysmeans()),
            acct);

        return sfdcs;
	}

}
