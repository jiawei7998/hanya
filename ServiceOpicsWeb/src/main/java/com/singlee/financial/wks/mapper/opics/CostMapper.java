package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Cost;

public interface CostMapper {
    int deleteByPrimaryKey(String costcent);

    int insert(Cost record);

    int insertSelective(Cost record);

    Cost selectByPrimaryKey(String costcent);

    int updateByPrimaryKeySelective(Cost record);

    int updateByPrimaryKey(Cost record);
}