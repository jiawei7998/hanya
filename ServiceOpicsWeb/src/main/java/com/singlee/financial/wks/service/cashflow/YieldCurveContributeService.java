package com.singlee.financial.wks.service.cashflow;

import java.util.List;

import com.singlee.financial.wks.bean.cashflow.YieldCurveContributeBean;

public interface YieldCurveContributeService {

	List<YieldCurveContributeBean> selectYieldCurve(YieldCurveContributeBean contributeBean);
}
