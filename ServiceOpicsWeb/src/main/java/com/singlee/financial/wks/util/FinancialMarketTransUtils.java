package com.singlee.financial.wks.util;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.singlee.financial.wks.bean.cashflow.HolidayBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveContributeBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveSubBean;
import com.singlee.financial.wks.bean.cashflow.YieldCurveSubClosingRateBean;
import com.singlee.financial.wks.bean.opics.Rhis;

public class FinancialMarketTransUtils {

	/**
	 * 构建历史汇率
	 * 
	 * @param rhisLst
	 * @return
	 */
	public static Map<Date, String> createRateHistory(List<Rhis> rhisLst) {
		Map<Date, String> rates = new HashMap<Date, String>();
		for (Rhis rhis : rhisLst) {
			rates.put(rhis.getEffdate(), rhis.getIntrate());
		}
		return rates;
	}

	/**
	 * 构建节假日
	 * 
	 * @param hldyLst
	 * @return
	 */
	public static Map<String, HolidayBean> createHoliday(String currency, List<Date> hldyLst) {
		Map<String, HolidayBean> hldys = new HashMap<String, HolidayBean>();
		HolidayBean bean = new HolidayBean();
		bean.setCurrency(currency);
		for (Date holidayDate : hldyLst) {
			bean.addHolidayDate(holidayDate);
		}
		hldys.put(currency, bean);
		return hldys;
	}

	/**
	 * 构建曲线
	 * 
	 * @param yieldCurveList
	 * @return
	 */
	public static YieldCurveBean createYieldCurve(List<YieldCurveContributeBean> yieldCurveList) {
		YieldCurveBean yieldCurve = new YieldCurveBean();
		
		if(null == yieldCurveList || yieldCurveList.size() ==0) {
			throw new RuntimeException("找不到曲线信息");
		}
		
		// 曲线基础构建
		yieldCurve.setCurrency(yieldCurveList.get(0).getCcy());
		yieldCurve.setCurveType(StringUtils.trimToEmpty(yieldCurveList.get(0).getCurvetype()));
		yieldCurve.setDateConvention("M");
		yieldCurve.setInterpolationMethod(yieldCurveList.get(0).getInterpmethod() == "L" ? "Linear" : "");
		yieldCurve.setScene("MTM");
		yieldCurve.setSpotDays(0);
		yieldCurve.setYieldCurveName(StringUtils.trimToEmpty(yieldCurveList.get(0).getYieldcurve()));
		// 循环构建曲线对象
		for (YieldCurveContributeBean contributeBean : yieldCurveList) {
			// 去空格判断是否是相同的贡献曲线，如果不是则需要新创建对象
			if (StringUtils.equals(StringUtils.trim(contributeBean.getRatetype()), "C")) {// 短线
				YieldCurveSubBean yieldCurveSubCcy = new YieldCurveSubBean();
				yieldCurveSubCcy.setYieldCurveSubName(StringUtils.trimToEmpty(contributeBean.getContribrate()));// 贡献曲线
				yieldCurveSubCcy.setBasis(StringUtils.trimToEmpty(contributeBean.getBasis()));
				yieldCurveSubCcy.setCompoudingFrequency(StringUtils.trimToEmpty(contributeBean.getFreq()));// 规则
				yieldCurveSubCcy.setRateType("MMKT");
				// 汇率点
				YieldCurveSubClosingRateBean yieldCurveSubClosingRate = new YieldCurveSubClosingRateBean();
				yieldCurveSubClosingRate.setMaturity(StringUtils.trimToEmpty(contributeBean.getMtyend()));
				yieldCurveSubClosingRate.setMidRate(contributeBean.getMidrate8());
				yieldCurveSubCcy.addClosingRates(yieldCurveSubClosingRate.getMaturity(), yieldCurveSubClosingRate);
				// 添加曲线对象
				yieldCurve.addYieldCurveSubBeans(yieldCurveSubCcy);
			} else if (StringUtils.equals(StringUtils.trim(contributeBean.getRatetype()), "S")) {// 长线
				YieldCurveSubBean yieldCurveSubSwap = new YieldCurveSubBean();
				yieldCurveSubSwap.setYieldCurveSubName(StringUtils.trimToEmpty(contributeBean.getContribrate()));// 贡献曲线
				yieldCurveSubSwap.setBasis(StringUtils.trimToEmpty(contributeBean.getBasis()));
				yieldCurveSubSwap.setCompoudingFrequency(StringUtils.trimToEmpty(contributeBean.getFreq()));// 规则
				yieldCurveSubSwap.setRateType("MMKT");
				// 汇率点
				YieldCurveSubClosingRateBean yieldCurveSubClosingRate = new YieldCurveSubClosingRateBean();
				yieldCurveSubClosingRate.setMaturity(StringUtils.trimToEmpty(contributeBean.getMtyend()));
				yieldCurveSubClosingRate.setMidRate(contributeBean.getMidrate8());
				yieldCurveSubSwap.addClosingRates(yieldCurveSubClosingRate.getMaturity(), yieldCurveSubClosingRate);
				// 添加曲线对象
				yieldCurve.addYieldCurveSubBeans(yieldCurveSubSwap);
			}
		}

		return yieldCurve;
	}
}
