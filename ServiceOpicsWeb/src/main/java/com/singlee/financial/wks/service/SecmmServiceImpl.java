package com.singlee.financial.wks.service;


import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.SecmWksBean;
import com.singlee.financial.wks.bean.opics.Secl;
import com.singlee.financial.wks.bean.opics.Secm;
import com.singlee.financial.wks.bean.opics.Secs;
import com.singlee.financial.wks.intfc.SecmmService;
import com.singlee.financial.wks.service.opics.CustService;
import com.singlee.financial.wks.service.opics.SeclService;
import com.singlee.financial.wks.service.opics.SecmService;
import com.singlee.financial.wks.service.opics.SecsService;
import com.singlee.financial.wks.service.trans.SecmEntry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class SecmmServiceImpl implements SecmmService {

    @Autowired
    private SecmService secmService;
    @Autowired
    private SeclService seclService;
    @Autowired
    private SecsService secsService;
    @Autowired
    private CustService custService;
    @Autowired
    private SecmEntry secmEntry;// 首期交易录入

    private static Logger logger = Logger.getLogger(SecmmServiceImpl.class);

    @Override
    public SlOutBean saveEntry(SecmWksBean secmWksBean) {
        SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SECM-00000", "债券信息录入成功!");
        Secm secm = null;
        Secl secl = null;
        List<Secs> secsLst = null;
        try {
            // 1.交易要素检查
            secmEntry.DataCheck(secmWksBean);
            // 2.转换债券主体数据
            secm = secmEntry.DataTrans(new Secm());// 静态参数转换
            secmEntry.DataTrans(secm, secmWksBean, custService);// 设置债券信息
            secmService.insert(secm);
            //3.转换初始收盘价数据
            secl = secmEntry.DataTrans(new Secl());//静态参数转换
            secmEntry.DataTrans(secl, secm);//设置secl信息
            seclService.insert(secl);
            //4.收付息计划secs
            secsLst = new ArrayList<Secs>();
            secmEntry.DataTrans(secsLst, secm, secmWksBean);
            secsService.batchInsert(secsLst);
        } catch (Exception e) {
            outBean.setRetStatus(RetStatusEnum.F);
            outBean.setRetMsg(e.getMessage());
            logger.error("债券信息交易处理录入错误", e);
            throw e;
        }
        outBean.setClientNo(secm.getSecid());
        logger.info("债券信息处理完成,返回消息:" + JSON.toJSONString(outBean));
        return outBean;
    }

}
