package com.singlee.financial.wks.service.trans;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SldhWksBean;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Fees;
import com.singlee.financial.wks.bean.opics.Slda;
import com.singlee.financial.wks.bean.opics.Sldh;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.SldhService;

/**
 * 交易冲销转换方法
 * @author xuqq
 *
 */
@Service
public class SlteReverse {

	@Autowired
	private SldhService sldhService;
	
	public void DataCheck(SldhWksBean sldhWksBean) {
		// 1.判断拆借交易对象是否存在
		if (null == sldhWksBean) {
			JY.raise("%s:%s,请补充完整SldhWksBean对象", WksErrorCode.REVERSE_NULL.getErrCode(), WksErrorCode.REVERSE_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(sldhWksBean.getInstId())) {
			JY.raise("%s:%s,请填写RepoWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 3.判断业务编号是否存在
		List<Sldh> sldh = sldhService.selectByTradeNo(sldhWksBean.getInstId(), sldhWksBean.getTradeNo());
		if (StringUtils.isEmpty(sldhWksBean.getTradeNo()) || null == sldh) {
			JY.raise("%s:%s,请填写SldhWksBean.TradeNo,For.[" + sldhWksBean.getTradeNo() + "]",
					WksErrorCode.CBT_NOT_FOUND.getErrCode(), WksErrorCode.CBT_NOT_FOUND.getErrMsg());
		}
	}
	
	public Sldh DataTrans(Sldh sldh, BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(sldh.getBr());
		sldh.setRevreason("99");// 冲销参数:默认99
		sldh.setRevdate(brps.getBranprcdate());// 冲销日期
		sldh.setRevtext("99");// 冲销描述
		sldh.setRevoper(OpicsConstant.SPSH.TRAD_NO);// 冲销复核人员
		sldh.setUpdatecounter(sldh.getUpdatecounter() + 1);// 修改次数
		return sldh;
	}
	public Slda DataTrans(Slda slda, BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(slda.getBr());
		slda.setRevreason("99");// 冲销参数:默认99
		slda.setRevdate(brps.getBranprcdate());// 冲销日期
		slda.setRevtext("99");// 冲销描述
		slda.setRevoper(OpicsConstant.SPSH.TRAD_NO);// 冲销复核人员
		slda.setUpdatecounter(slda.getUpdatecounter() + 1);// 修改次数
		return slda;
	}
	public Fees DataTrans(Fees fees, BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(fees.getBr());
		fees.setRevreason("99");// 冲销参数:默认99
		fees.setRevdate(brps.getBranprcdate());// 冲销日期
		fees.setRevtext("99");// 冲销描述
//		fees.setRevoper(OpicsConstant.SPSH.TRAD_NO);// 冲销复核人员
		fees.setUpdatecounter(fees.getUpdatecounter() + 1);// 修改次数
		return fees;
	}
}
