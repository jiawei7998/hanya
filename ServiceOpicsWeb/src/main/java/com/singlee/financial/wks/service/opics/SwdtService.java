package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Swdt;
public interface SwdtService{


    int deleteByPrimaryKey(String br,String dealno,String seq,String dealind);

    int insert(Swdt record);

    int insertSelective(Swdt record);

    Swdt selectByPrimaryKey(String br,String dealno,String seq,String dealind);

    int updateByPrimaryKeySelective(Swdt record);

    int updateByPrimaryKey(Swdt record);

    void batchInsert(List<Swdt> list);
    
    /**
     * 根据固定、浮动标识查询记录
     * @param br
     * @param dealno
     * @param fixFloatInd
     * @return
     */
    List<Swdt> selectByDealno(String br,String dealno);
    
    /**
     * 更新提前终止相关字段
     * @param record
     * @return
     */
    int updateByTerminate(List<Swdt> list);

}
