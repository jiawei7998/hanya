package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.bean.opics.Prir;
import com.singlee.financial.wks.bean.opics.Rdfh;
import com.singlee.financial.wks.bean.opics.Sldh;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.bean.opics.Swdt;
import com.singlee.financial.wks.expand.FxdItemWksBean;

public interface PrirService {

    int deleteByPrimaryKey(String br, String product, String type, String dealno, String seq);

    int insert(Prir record);

    int insertSelective(Prir record);

    Prir selectByPrimaryKey(String br, String product, String type, String dealno, String seq);
    
    Prir selectByDealno(String br, String product, String type, String dealno);

    int updateByPrimaryKeySelective(Prir record);

    int updateByPrimaryKey(Prir record);

    void updateBatch(List<Prir> list);

    void batchInsert(List<Prir> list);

    /**
     * 方法已重载.根据拆借业务数据构建PRIR对象
     * 
     * @param dl
     * @return
     */
    Prir builder(Dldt dl);

    /**
     * 方法已重载,根据期权业务对象构建PRIR对象
     * 
     * @param otc
     * @return
     */
    Prir builder(Otdt otc);

    /**
     * 方法已重载.根据外汇业务数据构建PRIR对象
     * 
     * @param fxdhLst
     * @return
     */
    Prir builder(Fxdh fxdhLst,FxdItemWksBean itemBean);

    /**
     * 方法已重载.根据互换业务数据构建PRIR对象
     * 
     * @param Lst
     * @return
     */
    Prir builder(Swdh swdh, List<Swdt> swdtLst,SwapWksBean swap);
    
    /**
     * 方法已重载.根据现券买卖数据构建PRIR对象
     * 
     * @param dl
     * @return
     */
    Prir builder(Spsh spsh);
    /**
     * 方法已重载.根据收息数据构建PRIR对象
     * 
     * @param dl
     * @return
     */
    Prir builder(Rdfh rdfh);
    
    Prir builder(Sldh sldh);
}
