package com.singlee.financial.wks.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

import com.singlee.financial.wks.bean.cashflow.HolidayBean;
import com.singlee.financial.wks.expand.DateConventionType;
import com.singlee.financial.common.util.DateUtil;

public class FinancialDateUtils extends DateUtil{

	
	public static HashMap<String,Integer> tenor_mapping = new HashMap<String,Integer>();
	
	private static HashMap<String,String> dateConvert_mapping = new HashMap<String,String>();
	
	static {
		tenor_mapping.put("M", 30);
		tenor_mapping.put("Y", 360);
		tenor_mapping.put("A", 360);
		tenor_mapping.put("S", 180);
		tenor_mapping.put("Q", 90);
		tenor_mapping.put("W", 7);
		tenor_mapping.put("D", 1);
		
		
		dateConvert_mapping.put("M_"+DateConventionType.INTEREST.name(), "M");
		dateConvert_mapping.put("S_"+DateConventionType.INTEREST.name(), "S");
		dateConvert_mapping.put("P_"+DateConventionType.INTEREST.name(), "P");
		dateConvert_mapping.put("M_"+DateConventionType.PAYMENT.name(), "M");
		dateConvert_mapping.put("S_"+DateConventionType.PAYMENT.name(), "S");
		dateConvert_mapping.put("P_"+DateConventionType.PAYMENT.name(), "P");
		dateConvert_mapping.put("M_"+DateConventionType.RESET.name(), "M");
		dateConvert_mapping.put("S_"+DateConventionType.RESET.name(), "S");
		dateConvert_mapping.put("P_"+DateConventionType.RESET.name(), "P");
		dateConvert_mapping.put("DIM_"+DateConventionType.INTEREST.name(), "D");
		dateConvert_mapping.put("DIS_"+DateConventionType.INTEREST.name(), "D");
		dateConvert_mapping.put("DIP_"+DateConventionType.INTEREST.name(), "D");
		dateConvert_mapping.put("DIM_"+DateConventionType.PAYMENT.name(), "M");
		dateConvert_mapping.put("DIS_"+DateConventionType.PAYMENT.name(), "S");
		dateConvert_mapping.put("DIP_"+DateConventionType.PAYMENT.name(), "P");
		dateConvert_mapping.put("DIM_"+DateConventionType.RESET.name(), "M");
		dateConvert_mapping.put("DIS_"+DateConventionType.RESET.name(), "S");
		dateConvert_mapping.put("DIP_"+DateConventionType.RESET.name(), "P");
		
	}
	
	public static void main(String [] args) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(FinancialDateUtils.parse("2020/2/3", "yyyy/MM/dd"));
		
		cal.add(Calendar.DATE, -2);
		
		cal.add(Calendar.MONTH, 3);
		
		System.out.println(FinancialDateUtils.format(cal.getTime(), "yyyy/MM/dd"));
	}
	
	/**
	 * 根据当前需要调整的业务类型,获取当前日期调整规则
	 * @param dateConventionType
	 * @param intPayAdjMod
	 * @return
	 */
	public static String dateConvertMapping(DateConventionType dateConventionType,String intPayAdjMod) {
		return dateConvert_mapping.get(intPayAdjMod+"_"+dateConventionType.name());
	}
	
	/**
	 * 方法已重载.计算首期开始日期
	 * @param startDate
	 * @param adjDays
	 * @return
	 */
	public static Date calculateStartDate(Date startDate,int adjDays) {
		if(null == startDate) {
			throw new RuntimeException("计算开始日时,参考日期不能为空!");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		return calculateStartDate(cal,adjDays);
	}
	
	/**
	 * 方法已重载.计算首期开始日期
	 * @param calDate
	 * @param adjDays
	 * @return
	 */
	public static Date calculateStartDate(Calendar calDate,int adjDays) {
		calDate.add(Calendar.DATE, adjDays);
		return calDate.getTime();
	}
	
	/**
	 * 方法已重载.计算并返回下一个工作日期
	 * @param calDate
	 * @param holidayDate
	 * @param dateAdjMod
	 * @return
	 */
	public static Date calculateNextWorkDate(Date date,HolidayBean holidayDate,String dateAdjMod) {
		if(null == date) {
			throw new RuntimeException("计算指定日期下一工作日时,参考日期不能为空!");
		}
		if(null == holidayDate || null ==holidayDate.getHolidayDates() || holidayDate.getHolidayDates().size() ==0) {
			throw new RuntimeException("计算指定日期下一工作日时,假日信息不能为空!");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return calculateNextWorkDate(cal,holidayDate,dateAdjMod);
	}
	
	/**
	 * 方法已重载.获取指定日期的下一工作日
	 * @param calDate
	 * @param holidayDate
	 * @param dateAdjMod
	 * @return
	 */
	public static Date calculateNextWorkDate(Calendar calDate,HolidayBean holidayDate,String dateAdjMod) {
		Date nextDate = null;
		Calendar orgDate = Calendar.getInstance();
		orgDate.setTime(calDate.getTime());
		if(null == holidayDate || null ==holidayDate.getHolidayDates() || holidayDate.getHolidayDates().size() ==0) {
			throw new RuntimeException("计算指定日期下一工作日时,假日信息不能为空!");
		}
		if(null == dateAdjMod || dateAdjMod.trim().length() ==0) {
			throw new RuntimeException("计算指定日期下一工作日时,日期调整规则不能为空!");
		}
		if ("M".equalsIgnoreCase(dateAdjMod)) {
			if(null == holidayDate || null == holidayDate.getHolidayDates()) {
				throw new RuntimeException("节假信息为空");
			}
			// 校验当期结束日期是否为假日
			boolean isHas = holidayDate.getHolidayDates().contains(calDate.getTime());
			// 如果是假日根据付息日调整规则计算下一个工作日
			if (isHas) {
				nextDate = calculateNextWorkDate(calDate, holidayDate.getHolidayDates());
				//如果下一工作日跨月则提前至上一工作日
				if(orgDate.get(Calendar.MONTH) != calDate.get(Calendar.MONTH)) {
					nextDate = calculatePreWorkDate(orgDate, holidayDate.getHolidayDates());
				}
			}else {
				nextDate = calDate.getTime();
			}//end if else			
		} else if ("P".equalsIgnoreCase(dateAdjMod)) {
			nextDate = FinancialDateUtils.calculatePreWorkDate(calDate.getTime(), holidayDate.getHolidayDates());
			//如果上一工作日跨月则顺延至当月第一个工作日
			if(orgDate.get(Calendar.MONTH) != calDate.get(Calendar.MONTH)) {
				nextDate = calculateNextWorkDate(orgDate, holidayDate.getHolidayDates());
			}
		} else if ("S".equalsIgnoreCase(dateAdjMod)) {
			nextDate = FinancialDateUtils.calculateNextWorkDate(calDate.getTime(), holidayDate.getHolidayDates());
			//如果下一工作日跨月则提前至当月最后一个工作日
			if(orgDate.get(Calendar.MONTH) != calDate.get(Calendar.MONTH)) {
				nextDate = calculatePreWorkDate(orgDate, holidayDate.getHolidayDates());
			}
		} else if ("D".equalsIgnoreCase(dateAdjMod)) {
			nextDate = calDate.getTime();
		} else {
			throw new RuntimeException("付息日顺延规则不支持");
		} // end if else if ... else
		return nextDate;
	}
	
	/**
	 * 方法已重载.计算下一个日期,如果是假日则递归至下一个工作日为止
	 * @param calDate
	 * @param list
	 * @return
	 */
	public static Date calculateNextWorkDate(Calendar calDate, List<Date> list) {
		if(null == list) {
			throw new RuntimeException("计算下一工作日时,假日信息不能为空!");
		}
		calDate.add(Calendar.DATE, 1);
		boolean isHas = list.contains(calDate.getTime());
		if (!isHas) {
			return calDate.getTime();
		} // end if
		return calculateNextWorkDate(calDate,list);
	}
	
	/**
	 * 方法已重载.计算下一个日期,如果是假日则递归至下一个工作日为止
	 * @param calDate
	 * @param list
	 * @return
	 */
	public static Date calculateNextWorkDate(Date date, List<Date> list) {
		if(null == date) {
			throw new RuntimeException("计算下一工作日时,参数日期对象不能为空!");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return calculateNextWorkDate(cal,list);
	}
	
	/**
	 * 计算当前货币的SPOTDATE日期
	 * @param date
	 * @param list
	 * @return
	 */
	public static Date calculateSpotDate(Date date, List<Date> list,int spotDays,int count) {
		Date tmp = null;
		
		if(null == date) {
			throw new RuntimeException("计算SPOTDATE时,参数日期对象不能为空!");
		}
		if(null == list || list.size() ==0) {
			throw new RuntimeException("计算SPOTDATE时,假日信息不能为空!");
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		tmp = calculateNextWorkDate(cal,list);
		
		if(count >= spotDays) {
			return tmp;
		}

		return calculateSpotDate(tmp,list,spotDays,++count);
	}
	
	
	/**
	 * 计算上一个工作日,如果是假日则递归至上一个工作日为止
	 * @param calDate
	 * @param list
	 * @return
	 */
	public static Date calculatePreWorkDate(Calendar calDate, List<Date> list) {
		if(null == list || list.size() ==0) {
			throw new RuntimeException("计算上一工作日时,假日信息不能为空!");
		}
		calDate.add(Calendar.DATE, -1);
		boolean isHas = list.contains(calDate.getTime());
		if (!isHas) {
			return calDate.getTime();
		} // end if
		return calculatePreWorkDate(calDate,list);
	}
	
	/**
	 * 方法已重载.计算上一个自然日
	 * @param calDate
	 * @return
	 */
	public static Date calculatePreDate(Calendar calDate) {
		calDate.add(Calendar.DATE, -1);
		
		return calDate.getTime();
	}
	
	
	/**
	 * 方法已重载.计算下一个自然日
	 * @param date
	 * @return
	 */
	public static Date calculatePreDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		return calculatePreDate(cal);
	}
	
	/**
	 * 方法已重载.计算下一个自然日
	 * @param calDate
	 * @return
	 */
	public static Date calculateNextDate(Calendar calDate) {
		calDate.add(Calendar.DATE, -1);
		
		return calDate.getTime();
	}
	
	
	/**
	 * 方法已重载.计算上一个自然日
	 * @param date
	 * @return
	 */
	public static Date calculateNextDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		return calculatePreDate(cal);
	}
	
	/**
	 * 计算上一个工作日,如果是假日则递归至上一个工作日为止
	 * @param calDate
	 * @param list
	 * @return
	 */
	public static Date calculatePreWorkDate(Date date, List<Date> list) {
		if(null == date) {
			throw new RuntimeException("计算上一工作日时,参数日期对象不能为空!");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return calculatePreWorkDate(cal,list);
	}
	
	/**
	 * 方法已重载.按付息/利率重定频率计算当前计息期间的结束日期,如果当前日期日历天与起息日不同则设置与起息日相同,如果大于了当前月份的日历天数则设置为当月最后一天
	 * @param days
	 * @param cal
	 * @param endDate
	 * @param frequency
	 */
	public static void calculateNextDate(int dayOfMonth,Calendar cal,Date endDate, String frequency) {
		calculateNextDate(cal,endDate,frequency,0);
		int days = cal.get(Calendar.DAY_OF_MONTH);
		int maxDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		//如果当期的结束日与起息日不同,则使用起息日的日历日
		if(days != dayOfMonth && maxDayOfMonth >= dayOfMonth) {
			cal.set(Calendar.DATE, dayOfMonth);
		}//end if
		LogFactory.getLog(FinancialDateUtils.class).info(String.format("设置与起息日所在月份中的天数:%1$tY-%1$tm-%1$td,起期日日历天数:%2$d", cal.getTime(),dayOfMonth));
	}

	
	/**
	 * 方法已重载.按付息/利率重定频率计算当前计息期间的结束日期
	 * @param cal
	 * @param frequency
	 */
	public static void calculateNextDate(Calendar cal,Date endDate, String frequency,int preAdjDays) {
		if(null == frequency || frequency.trim().length() == 0) {
			throw new RuntimeException("计算下一个付息结束日时,复息频率不能为空");
		}
		if(null == cal) {
			throw new RuntimeException("计算日期时,参考日期对象不能为空");
		}
		
		// 计算下一个日期
		if ("A".equalsIgnoreCase(frequency)) {
			cal.add(Calendar.YEAR, 1);
		} else if ("S".equalsIgnoreCase(frequency)) {
			cal.add(Calendar.MONTH, 6);
		} else if ("Q".equalsIgnoreCase(frequency)) {
			cal.add(Calendar.MONTH, 3);
		} else if ("M".equalsIgnoreCase(frequency)) {
			cal.add(Calendar.MONTH, 1);
		} else if ("14".equalsIgnoreCase(frequency)) {
			if(preAdjDays > 0 && preAdjDays < 14) {
				cal.add(Calendar.DATE, -preAdjDays);
			}
			cal.add(Calendar.DATE, 14);
		} else if ("W".equalsIgnoreCase(frequency)) {
			if(preAdjDays > 0 && preAdjDays < 7) {
				cal.add(Calendar.DATE, -preAdjDays);
			}
			cal.add(Calendar.DATE, 7);
		} else if ("D".equalsIgnoreCase(frequency)) {
			cal.add(Calendar.DATE, 1);
		} else if ("B".equalsIgnoreCase(frequency)) {
			if(null == endDate) {
				throw new RuntimeException("到期还本付息时,到期日不能为空!");
			}
			cal.setTime(endDate);
		} else {
			throw new RuntimeException("付息/浮动重置频率不支持");
		} // end if else if ... else
		
	}
	
	/**
	 * 方法已重载.计算当前期限对应的天数
	 * @param cal
	 * @param frequency
	 */
	public static int calculateTenorDays(String tenor) {
		int days = 0;
		int frequencyDays;
		
		if(tenor == null || tenor.trim().length() == 0) {
			throw new RuntimeException("期限定义为空");
		}
		
		// 计算下一个日期
		if (tenor.contains("Y")) {
			frequencyDays = Integer.parseInt(tenor.substring(0, tenor.trim().length() -1));
			days = frequencyDays*tenor_mapping.get("Y");
		} else if (tenor.contains("M")) {
			frequencyDays = Integer.parseInt(tenor.substring(0, tenor.trim().length() -1));
			days = frequencyDays*tenor_mapping.get("M");
		} else if (tenor.contains("W")) {
			frequencyDays = Integer.parseInt(tenor.substring(0, tenor.trim().length() -1));
			days = frequencyDays*tenor_mapping.get("W");
		} else if (tenor.contains("D")) {
			frequencyDays = Integer.parseInt(tenor.substring(0, tenor.trim().length() -1));
			days = frequencyDays*tenor_mapping.get("D");
		} else if (tenor.contains("O/N")) {
			days = FinancialCalculationModel.ZERO.intValue();
		} else if (tenor.contains("TOM")) {
			days = FinancialCalculationModel.THREE.intValue();
		} else {
			throw new RuntimeException("期限定义不支持");
		} // end if else if ... else
		
		return days;
	}
	
	/**
	 * 计算复利频率对应的天数
	 * @param frequency
	 * @return
	 */
	public static int calculateFirstCompoundDays(String compoundFrequency) {
		
		int firstCompoundDays = 0;
		
		if(compoundFrequency == null || compoundFrequency.isEmpty() == true) {
			throw new RuntimeException("复利频率为空");
		}
		
		if("A".equalsIgnoreCase(compoundFrequency)) {
			firstCompoundDays = tenor_mapping.get("A");
		}else if("S".equalsIgnoreCase(compoundFrequency)) {
			firstCompoundDays = tenor_mapping.get("S");
		}else if("Q".equalsIgnoreCase(compoundFrequency)) {
			firstCompoundDays = tenor_mapping.get("Q");
		}else if("M".equalsIgnoreCase(compoundFrequency)) {
			firstCompoundDays = tenor_mapping.get("M");
		}else {
			throw new RuntimeException("复利频率不支持");
		}
		
		return firstCompoundDays;
	}
	
	public static String calculateCompoundTenor(int compoundDays,String compoundFrequency) {
		String  tenor = null;
		
		if(compoundDays == 0) {
			throw new RuntimeException("复利频率天数不能为零");
		}
		
		if(null == compoundFrequency || compoundFrequency.trim().length() == 0) {
			throw new RuntimeException("复利频率不能为空");
		}
		
		if(compoundDays%tenor_mapping.get("A") == 0 && "A".equalsIgnoreCase(compoundFrequency)) {
			tenor = compoundDays/tenor_mapping.get("A")+"Y";
		}else if(compoundDays%tenor_mapping.get("S") == 0 && "S".equalsIgnoreCase(compoundFrequency)) {
			tenor = compoundDays/tenor_mapping.get("M")+"M";
		}else if(compoundDays%tenor_mapping.get("Q") == 0 && "Q".equalsIgnoreCase(compoundFrequency)) {
			tenor = compoundDays/tenor_mapping.get("M")+"M";
		}else if(compoundDays%tenor_mapping.get("M") == 0 && "M".equalsIgnoreCase(compoundFrequency)) {
			tenor = compoundDays/tenor_mapping.get("M")+"M";
		}else {
			throw new RuntimeException("复利频率不支持");
		}
		
		if("12M".equalsIgnoreCase(tenor)) {
			tenor = "1Y";
		}
		
		return tenor;
	}
	
	
	/**
	 * 方法已重载.根据步率获取下一个日期
	 * @param date
	 * @param frequency
	 * @param preAdjDays
	 */
	public static Date calculateNextDate(Date date, String frequency,int preAdjDays) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		calculateNextDate(cal,null,frequency,preAdjDays);
		
		return cal.getTime();
	}
	
	/**
	 * 方法已重载.按付息/利率重定频率计算当前计息期间的结束日期
	 * @param cal
	 * @param frequency
	 * @return 
	 */
	public static void calculateNextDate(Calendar cal, String frequency) {
		calculateNextDate(cal,null,frequency,0);
	}
	
	/**
	 * 方法已重载.按付息/利率重定频率计算当前计息期间的结束日期
	 * @param cal
	 * @param frequency
	 * @param preAdjDays
	 */
	public static void calculateNextDate(Calendar cal,String frequency,int preAdjDays) {
		calculateNextDate(cal,null,frequency,preAdjDays);
	}
	
	/**
	 * 方法已重载.按付息/利率重定频率计算当前计息期间的结束日期,支持到期还本付息
	 * @param startDate
	 * @param endDate
	 * @param frequency
	 */
	public static void calculateNextDate(Calendar startDate, Date endDate,String frequency) {
		calculateNextDate(startDate,endDate,frequency,0);
	}
	
	/**
	 * 方法已重载.按付息/利率重定频率计算当前计息期间的结束日期,支持到期还本付息
	 * @param startDate
	 * @param endDate
	 * @param frequency
	 */
	public static Date calculateNextDate(Date startDate, Date endDate,String frequency) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		calculateNextDate(cal,endDate,frequency);
		
		return cal.getTime();
	}
	
	/**
	 * 方法已重载.按付息/利率重定频率计算当前计息期间的结束日期
	 * @param cal
	 * @param frequency
	 * @return 
	 */
	public static Date calculateNextDate(Date date, String frequency) {
		return calculateNextDate(date,frequency,0);
	}

	public static Date calculateTenorDate(Date batchDate,String tenor) {
		Calendar cal = null;
		
		String tenorType = null;
		int days = 0;
		tenor = StringUtils.trimToEmpty(tenor);
		
		if(null == tenor || tenor.trim().length() == 0) {
			throw new RuntimeException("计算两天日期之前天数时,期阴不能为空");
		}
		if(null == batchDate) {
			throw new RuntimeException("计算两天日期之前天数时,开始日期不能为空");
		}
		
		cal = Calendar.getInstance();
		cal.setTime(batchDate);
		
		if("SN".equalsIgnoreCase(tenor) || "TN".equalsIgnoreCase(tenor)) {
			tenorType = "D";
			days = 3;
		}else if("TOM".equalsIgnoreCase(tenor)){
			tenorType = "D";
			days = 1;
		}else if("O/N".equalsIgnoreCase(tenor) || "ON".equalsIgnoreCase(tenor)){
			tenorType = "D";
			days = 0;
		}else {
			tenorType = StringUtils.trim(tenor.substring(tenor.trim().length()-1));
			days = Integer.parseInt(StringUtils.trim(tenor.substring(0, tenor.trim().length()-1)));
		}
		//计算期限对应的日期
		calculateTenorDate(cal,tenorType,days);
		
		return cal.getTime();
	}
	
	/**
	 * 计算当前日期对应期阴的到期日
	 * @param cal
	 * @param tenorType
	 * @param days
	 */
	public static void calculateTenorDate(Calendar cal,String tenorType,int days) {
		
		if(null == tenorType || tenorType.trim().length() == 0) {
			throw new RuntimeException("计算参考日期对应限期到限日时,期阴类型不能为空");
		}
		
		// 计算期限相对于批量日期的到期日
		if ("M".equalsIgnoreCase(tenorType)) {
			cal.add(Calendar.MONTH, days);
		} else if ("D".equalsIgnoreCase(tenorType)) {
			cal.add(Calendar.DATE, days);
		} else if ("Y".equalsIgnoreCase(tenorType)) {
			cal.add(Calendar.YEAR, days);
		} else if ("W".equalsIgnoreCase(tenorType)) {
			cal.add(Calendar.DATE, days*7);
		} else {
			throw new RuntimeException("收益率曲线期限类型不支持");
		} // end if else if ... else
	}
	
	/**
	 * 返回两个日期之前的天数
	 * @param batchDate
	 * @param maturityDate
	 * @return
	 */
	public static int calculateTenorDate(Date batchDate,Date maturityDate) {
		return DateUtil.daysBetween(batchDate, maturityDate);
	}

	/**
	 * 返回当前期限相对于批量日期之前的天数
	 * @param maturity
	 * @param bacthDate
	 * @return
	 */
	public static int calculateTenorDate(String maturity,Date bacthDate) {
		Date maturityDate = calculateTenorDate(bacthDate,maturity);
		return calculateTenorDate(bacthDate,maturityDate);
	}
	
	/**
	 * 方法已重载.判断当前日期是否为月末
	 * @param cal
	 * @return
	 */
	public static boolean isLastDayOfMonth(Calendar  cal) {
		boolean flag = false;
		
		if(cal.get(Calendar.DAY_OF_MONTH) == cal.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			flag = true;
		}
		
		return flag;
	}
	
	/**
	 * 方法已重载.判断当前日期是否为月末
	 * @param date
	 * @return
	 */
	public static boolean isLastDayOfMonth(Date  date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return isLastDayOfMonth(cal);
	}
	
}
