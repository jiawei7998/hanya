package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Drpi;
import com.singlee.financial.wks.mapper.opics.DrpiMapper;
import com.singlee.financial.wks.service.opics.DrpiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DrpiServiceImpl implements DrpiService {

    @Resource
    private DrpiMapper drpiMapper;

    @Override
    public int deleteByPrimaryKey(String br,String product,String prodtype,String dealno,String seq,String delrecind) {
        return drpiMapper.deleteByPrimaryKey(br,product,prodtype,dealno,seq,delrecind);
    }

    @Override
    public int insert(Drpi record) {
        return drpiMapper.insert(record);
    }

    @Override
    public int insertSelective(Drpi record) {
        return drpiMapper.insertSelective(record);
    }

    @Override
    public Drpi selectByPrimaryKey(String br,String product,String prodtype,String dealno,String seq,String delrecind) {
        return drpiMapper.selectByPrimaryKey(br,product,prodtype,dealno,seq,delrecind);
    }

    @Override
    public int updateByPrimaryKeySelective(Drpi record) {
        return drpiMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Drpi record) {
        return drpiMapper.updateByPrimaryKey(record);
    }

}
