package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Rpdt;
import com.singlee.financial.wks.bean.opics.Slda;
import com.singlee.financial.wks.bean.opics.Spos;
import com.singlee.financial.wks.bean.opics.Spsh;

import java.math.BigDecimal;
import java.util.Date;
public interface SposService{


    int deleteByPrimaryKey(String br,String ccy,String port,String secid,Date settdate,String invtype,String cost);

    int insert(Spos record);

    int insertSelective(Spos record);

    Spos selectByPrimaryKey(String br,String ccy,String port,String secid,Date settdate,String invtype,String cost);

    int updateByPrimaryKeySelective(Spos record);

    int updateByPrimaryKey(Spos record);

    Spos builder(Spsh spsh);

    Spos builder(Slda slda);

    Spos builder(Rpdt rpdt);
    
    void callSpUpdrecSposIncr(String br,String ccy,String port,String secid,Date settdate,String invtype,String cost,
    		BigDecimal amt,Date lstmntdate,BigDecimal qty);
}
