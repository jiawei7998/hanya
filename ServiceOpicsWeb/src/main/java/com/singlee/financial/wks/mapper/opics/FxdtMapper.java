package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Fxdt;
import org.apache.ibatis.annotations.Param;

public interface FxdtMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    int insert(Fxdt record);

    int insertSelective(Fxdt record);

    Fxdt selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    int updateByPrimaryKeySelective(Fxdt record);

    int updateByPrimaryKey(Fxdt record);
    
    int updateReverseByPrimaryKey(Fxdt record);
}