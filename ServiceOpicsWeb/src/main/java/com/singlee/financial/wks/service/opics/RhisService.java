package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Rhis;

import java.util.Date;
import java.util.List;
public interface RhisService{


    int deleteByPrimaryKey(String br,String ratecode,Date effdate);

    int insert(Rhis record);

    int insertSelective(Rhis record);

    Rhis selectByPrimaryKey(String br,String ratecode,Date effdate);

    int updateByPrimaryKeySelective(Rhis record);

    int updateByPrimaryKey(Rhis record);

    List<Rhis> selectAllByRate(Rhis rhis);
}
