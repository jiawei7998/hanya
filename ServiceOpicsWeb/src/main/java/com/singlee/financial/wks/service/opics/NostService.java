package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Nost;
public interface NostService{


    int deleteByPrimaryKey(String br,String nos);

    int insert(Nost record);

    int insertSelective(Nost record);

    Nost selectByPrimaryKey(String br,String nos);

    int updateByPrimaryKeySelective(Nost record);

    int updateByPrimaryKey(Nost record);

    List<String> selectNosByCust(String br, String ccy, String bic);

}
