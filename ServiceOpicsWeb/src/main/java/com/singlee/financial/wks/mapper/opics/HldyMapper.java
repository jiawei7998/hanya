package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Hldy;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface HldyMapper {
	/**
	 * delete by primary key
	 *
	 * @param calendarid primaryKey
	 * @return deleteCount
	 */
	int deleteByPrimaryKey(@Param("calendarid") String calendarid, @Param("holidaytype") String holidaytype,
			@Param("holidate") Date holidate);

	/**
	 * 根据货币标识删除假日
	 * 
	 * @param calendarid
	 * @return
	 */
	int deleteByCcy(@Param("calendarid") String calendarid);

	/**
	 * insert record to table
	 *
	 * @param record the record
	 * @return insert count
	 */
	int insert(Hldy record);

	/**
	 * insert record to table selective
	 *
	 * @param record the record
	 * @return insert count
	 */
	int insertSelective(Hldy record);

	Hldy selectByPrimaryKey(@Param("calendarid") String calendarid, @Param("holidaytype") String holidaytype,
			@Param("holidate") Date holidate);

	int updateByPrimaryKeySelective(Hldy record);

	int updateByPrimaryKey(Hldy record);

	List<Date> selectHldyListStr(String ccy);

	/**
	 * 查询当期货币指定时间内的假日信息
	 * 
	 * @param ccy
	 * @param dealDate
	 * @param valueDate
	 * @return
	 */
	List<Date> selectHldyListDate(@Param("ccy") List<String> ccy, @Param("dealDate") Date dealDate,
			@Param("valueDate") Date valueDate);
}