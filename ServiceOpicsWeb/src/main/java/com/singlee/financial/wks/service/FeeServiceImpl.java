package com.singlee.financial.wks.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.FeeWksBean;
import com.singlee.financial.wks.bean.opics.Fees;
import com.singlee.financial.wks.bean.opics.Mcfp;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.intfc.FeeService;
import com.singlee.financial.wks.service.opics.FeesService;
import com.singlee.financial.wks.service.opics.McfpService;
import com.singlee.financial.wks.service.trans.FeeEntry;
import com.singlee.financial.wks.service.trans.FeeReverse;
import com.singlee.financial.wks.util.FinancialBeanHelper;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class FeeServiceImpl implements FeeService {

	@Autowired
	private McfpService mcfpService;
	@Autowired
	private FeesService feesService;
	@Autowired
	private FeeEntry feeEntry;
	@Autowired
	private FeeReverse feeReverse;

	private static Logger logger = Logger.getLogger(FeeServiceImpl.class);

	@Override
	public SlOutBean saveEntry(FeeWksBean feeWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrCode(),
				WksErrorCode.SUCCESS.getErrMsg());
		logger.info("费用交易录入,输入消息:" + JSON.toJSONString(feeWksBean));
		// 1.交易要素检查
		feeEntry.DataCheck(feeWksBean);
		// 2.交易转换
		Fees fees = feeEntry.DataTrans(new Fees());
		feeEntry.DataTrans(fees, feeWksBean);// 交易转换,默认不填写
		feeEntry.DataTrans(fees, mcfpService);// 交易编号
		feesService.insert(fees);// 保存数据
		// 3.更新dealno
		feeEntry.DataTrans(fees, mcfpService, new Mcfp());
		outBean.setClientNo(fees.getFeeno());// 费用dealno
		logger.info("费用交易录入,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	@Override
	public SlOutBean reverseTrading(FeeWksBean feeWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
				WksErrorCode.SUCCESS.getErrMsg());
		logger.info("费用冲销交易录入,输入消息:" + JSON.toJSONString(feeWksBean));
		// 1.交易检查
		feeReverse.DataCheck(feeWksBean);
		// 2.查询原始交易,查询交易信息
		Fees fees = feesService.selectByTradeNo(feeWksBean.getInstId(), feeWksBean.getTradeNo());
		// 去掉类空格
		FinancialBeanHelper.TrimBeanAttrValue(fees);
		// 2.交易更新
		feeReverse.DataTrans(fees, feeWksBean);
		// 3.更新要素
		feesService.updateByPrimaryKeySelective(fees);
		// 设置feesNo
		outBean.setClientNo(fees.getFeeno());
		logger.info("费用冲销交易录入,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

}
