package com.singlee.financial.wks.mapper.opics;


import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.singlee.financial.wks.bean.opics.Cppr;

public interface CpprMapper {
    /**
     * delete by primary key
     * @param br primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy1") String ccy1, @Param("ccy2") String ccy2, @Param("cost") String cost, @Param("port") String port, @Param("trad") String trad, @Param("vdate") Date vdate);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Cppr record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Cppr record);

    /**
     * select by primary key
     * @param br primary key
     * @return object by primary key
     */
    Cppr selectByPrimaryKey(@Param("br") String br, @Param("ccy1") String ccy1, @Param("ccy2") String ccy2, @Param("cost") String cost, @Param("port") String port, @Param("trad") String trad, @Param("vdate") Date vdate);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Cppr record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Cppr record);

    int updateBatch(List<Cppr> list);

    int updateBatchSelective(List<Cppr> list);
    
    int updateCpprAmtByDeal(Cppr cppr);
}