package com.singlee.financial.wks.service.opics;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.singlee.financial.wks.bean.opics.Sldh;

public interface SldhService {

	int deleteByPrimaryKey(String br, String dealno, String seq, String dealind);

	int insert(Sldh record);

	int insertSelective(Sldh record);

	Sldh selectByPrimaryKey(String br, String dealno, String seq, String dealind);

	int updateByPrimaryKeySelective(Sldh record);

	int updateByPrimaryKey(Sldh record);

	List<Sldh> selectByTradeNo(String br, String dealtext);

	void updateBatch(List<Sldh> list);

	void batchInsert(@Param("list") List<Sldh> list);
}
