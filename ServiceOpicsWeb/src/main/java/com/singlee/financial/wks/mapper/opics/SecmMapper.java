package com.singlee.financial.wks.mapper.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Secm;

public interface SecmMapper {
    int deleteByPrimaryKey(String secid);

    int insert(Secm record);

    int insertSelective(Secm record);

    Secm selectByPrimaryKey(String secid);
    
    List<Secm> selectAll();

    int updateByPrimaryKeySelective(Secm record);

    int updateByPrimaryKey(Secm record);
}