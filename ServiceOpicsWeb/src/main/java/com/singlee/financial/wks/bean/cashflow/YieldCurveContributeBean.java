package com.singlee.financial.wks.bean.cashflow;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 利率曲线查询数据库表
 * 
 * @author shenzl
 *
 */
public class YieldCurveContributeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 部门
	 */
	private String br;
	/**
	 * 货币
	 */
	private String ccy;

	/**
	 * 曲线名称
	 */
	private String yieldcurve;

	/**
	 * 曲线类型
	 */
	private String curvetype;

	/**
	 * 计算方法
	 */
	private String interpmethod;

	/**
	 * 曲线名称
	 */
	private String contribrate;

	/**
	 * 曲线点
	 */
	private String mtyend;

	/**
	 * 利率类型
	 */
	private String ratetype;

	/**
	 * 基准
	 */
	private String basis;

	/**
	 * 频率
	 */
	private String freq;

	/**
	 * 买价利率
	 */
	private BigDecimal bidrate8;
	/**
	 * 卖价利率
	 */
	private BigDecimal offerrate8;
	/**
	 * 中间利率
	 */
	private BigDecimal midrate8;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getYieldcurve() {
		return yieldcurve;
	}

	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}

	public String getCurvetype() {
		return curvetype;
	}

	public void setCurvetype(String curvetype) {
		this.curvetype = curvetype;
	}

	public String getInterpmethod() {
		return interpmethod;
	}

	public void setInterpmethod(String interpmethod) {
		this.interpmethod = interpmethod;
	}

	public String getContribrate() {
		return contribrate;
	}

	public void setContribrate(String contribrate) {
		this.contribrate = contribrate;
	}

	public String getMtyend() {
		return mtyend;
	}

	public void setMtyend(String mtyend) {
		this.mtyend = mtyend;
	}

	public String getRatetype() {
		return ratetype;
	}

	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public BigDecimal getBidrate8() {
		return bidrate8;
	}

	public void setBidrate8(BigDecimal bidrate8) {
		this.bidrate8 = bidrate8;
	}

	public BigDecimal getOfferrate8() {
		return offerrate8;
	}

	public void setOfferrate8(BigDecimal offerrate8) {
		this.offerrate8 = offerrate8;
	}

	public BigDecimal getMidrate8() {
		return midrate8;
	}

	public void setMidrate8(BigDecimal midrate8) {
		this.midrate8 = midrate8;
	}

}
