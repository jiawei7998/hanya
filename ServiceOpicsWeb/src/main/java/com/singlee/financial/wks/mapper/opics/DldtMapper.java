package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Dldt;
import org.apache.ibatis.annotations.Param;

public interface DldtMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    int insert(Dldt record);

    int insertSelective(Dldt record);

    Dldt selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq);

    Dldt selectByTradeNo(@Param("br") String br, @Param("dealtext") String tradeNo);

    int updateByPrimaryKeySelective(Dldt record);

    int updateByPrimaryKey(Dldt record);

}