package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Sldh;
import com.singlee.financial.wks.mapper.opics.SldhMapper;
import com.singlee.financial.wks.service.opics.SldhService;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

@Service
public class SldhServiceImpl implements SldhService {
	@Resource
	BatchDao batchDao;
	@Resource
	private SldhMapper sldhMapper;

	@Override
	public int deleteByPrimaryKey(String br, String dealno, String seq, String dealind) {
		return sldhMapper.deleteByPrimaryKey(br, dealno, seq, dealind);
	}

	@Override
	public int insert(Sldh record) {
		return sldhMapper.insert(record);
	}

	@Override
	public int insertSelective(Sldh record) {
		return sldhMapper.insertSelective(record);
	}

	@Override
	public Sldh selectByPrimaryKey(String br, String dealno, String seq, String dealind) {
		return sldhMapper.selectByPrimaryKey(br, dealno, seq, dealind);
	}

	@Override
	public int updateByPrimaryKeySelective(Sldh record) {
		return sldhMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Sldh record) {
		return sldhMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<Sldh> selectByTradeNo(String br, String dealtext) {
		return sldhMapper.selectByTradeNo(br, dealtext);
	}

	@Override
	public void updateBatch(List<Sldh> list) {
		if (null == list || list.size() == 0) {
			return;
		}
		batchDao.batch("com.singlee.financial.wks.mapper.opics.SldhMapper.updateByPrimaryKeySelective", list);
	}

	@Override
	public void batchInsert(List<Sldh> list) {
		if (null == list || list.size() == 0) {
			return;
		}
		batchDao.batch("com.singlee.financial.wks.mapper.opics.SldhMapper.insert", list);
	}

}
