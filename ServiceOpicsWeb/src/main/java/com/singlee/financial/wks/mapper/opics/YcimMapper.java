package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Ycim;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface YcimMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("contribrate") String contribrate);

    int insert(Ycim record);

    int insertSelective(Ycim record);

    Ycim selectByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("contribrate") String contribrate);

    int updateByPrimaryKeySelective(Ycim record);

    int updateByPrimaryKey(Ycim record);

    int updateBatch(List<Ycim> list);

    int updateBatchSelective(List<Ycim> list);
}