package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Fxdh;

public interface FxdhService {

    int deleteByPrimaryKey(String br, String dealno, String seq);

    int insert(Fxdh fxdh);

    int insertSelective(Fxdh fxdh);

    Fxdh selectByPrimaryKey(String br, String dealno, String seq);
    
    List<Fxdh> selectByTradeNo(String br, String dealtext,String dealStatus);

    int updateByPrimaryKeySelective(Fxdh fxdh);

    int updateByPrimaryKey(Fxdh fxdh);

    void batchInsert(List<Fxdh> list);

    void updateBatch(List<Fxdh> list);

}
