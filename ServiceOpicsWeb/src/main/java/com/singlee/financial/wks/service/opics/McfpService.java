package com.singlee.financial.wks.service.opics;


import com.singlee.financial.wks.bean.opics.Mcfp;

public interface McfpService {

    int deleteByPrimaryKey(String br, String prodcode, String type);

    int insert(Mcfp record);

    int insertSelective(Mcfp record);

    Mcfp selectByPrimaryKey(String br, String prodcode, String type);

    int updateByPrimaryKeySelective(Mcfp record);

    int updateByPrimaryKey(Mcfp record);

}
