package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Seci;
import com.singlee.financial.wks.mapper.opics.SeciMapper;
import com.singlee.financial.wks.service.opics.SeciService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SeciServiceImpl implements SeciService {

    @Resource
    private SeciMapper seciMapper;

    @Override
    public int deleteByPrimaryKey(String secid) {
        return seciMapper.deleteByPrimaryKey(secid);
    }

    @Override
    public int insert(Seci record) {
        return seciMapper.insert(record);
    }

    @Override
    public int insertSelective(Seci record) {
        return seciMapper.insertSelective(record);
    }

    @Override
    public Seci selectByPrimaryKey(String secid) {
        return seciMapper.selectByPrimaryKey(secid);
    }

    @Override
    public int updateByPrimaryKeySelective(Seci record) {
        return seciMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Seci record) {
        return seciMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateBatch(List<Seci> list) {
        return seciMapper.updateBatch(list);
    }

    @Override
    public int updateBatchSelective(List<Seci> list) {
        return seciMapper.updateBatchSelective(list);
    }

}
