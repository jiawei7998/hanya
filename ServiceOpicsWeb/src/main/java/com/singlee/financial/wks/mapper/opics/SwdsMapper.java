package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Swds;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SwdsMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("intenddte") Date intenddte, @Param("schdseq") String schdseq, @Param("schdtype") String schdtype);
    
    int deleteByGtSeq(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("schdseq") String schdseq, @Param("schdtype") String schdtype);

    int insert(Swds record);

    int insertSelective(Swds record);

    Swds selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("intenddte") Date intenddte, @Param("schdseq") String schdseq, @Param("schdtype") String schdtype);

    Swds selectBySeq(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("schdseq") String schdseq, @Param("schdtype") String schdtype);
    
    Swds selectBySchdtype(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind, @Param("product") String product, @Param("prodtype") String prodtype, @Param("schdtype") String schdtype);
    
    int updateByPrimaryKeySelective(Swds record);
    
    int updateBySeqSelective(Swds record);

    int updateByPrimaryKey(Swds record);
    
    List<Swds> selectByDealno(@Param("br") String br, @Param("dealno") String dealno);
    
    /**
     * 根据DEALNO删除现金流明细表
     * @param br
     * @param dealno
     * @return
     */
    int deleteBySchdSeq(@Param("br") String br, @Param("dealno") String dealno,@Param("fixFloatInd") String fixFloatInd, @Param("schdseq") String schdSeq);
}