package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Sacc implements Serializable {
    private String br;

    private String accountno;

    private String trad;

    private Date opendate;

    private String accttitle;

    private String cost;

    private String cno;

    private String discretind;

    private String glno;

    private Date lstmntdate;

    private Date laststmtdate;

    private Date nextstmtdate;

    private String nostrovostro;

    private String stmtcycle;

    private String stmtday;

    private String stmtdayrule;

    private String timelimit;

    private String port;

    private String shortposind;

    private String autosettind;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getAccountno() {
		return accountno;
	}


	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}


	public String getTrad() {
		return trad;
	}


	public void setTrad(String trad) {
		this.trad = trad;
	}


	public Date getOpendate() {
		return opendate;
	}


	public void setOpendate(Date opendate) {
		this.opendate = opendate;
	}


	public String getAccttitle() {
		return accttitle;
	}


	public void setAccttitle(String accttitle) {
		this.accttitle = accttitle;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getDiscretind() {
		return discretind;
	}


	public void setDiscretind(String discretind) {
		this.discretind = discretind;
	}


	public String getGlno() {
		return glno;
	}


	public void setGlno(String glno) {
		this.glno = glno;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public Date getLaststmtdate() {
		return laststmtdate;
	}


	public void setLaststmtdate(Date laststmtdate) {
		this.laststmtdate = laststmtdate;
	}


	public Date getNextstmtdate() {
		return nextstmtdate;
	}


	public void setNextstmtdate(Date nextstmtdate) {
		this.nextstmtdate = nextstmtdate;
	}


	public String getNostrovostro() {
		return nostrovostro;
	}


	public void setNostrovostro(String nostrovostro) {
		this.nostrovostro = nostrovostro;
	}


	public String getStmtcycle() {
		return stmtcycle;
	}


	public void setStmtcycle(String stmtcycle) {
		this.stmtcycle = stmtcycle;
	}


	public String getStmtday() {
		return stmtday;
	}


	public void setStmtday(String stmtday) {
		this.stmtday = stmtday;
	}


	public String getStmtdayrule() {
		return stmtdayrule;
	}


	public void setStmtdayrule(String stmtdayrule) {
		this.stmtdayrule = stmtdayrule;
	}


	public String getTimelimit() {
		return timelimit;
	}


	public void setTimelimit(String timelimit) {
		this.timelimit = timelimit;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getShortposind() {
		return shortposind;
	}


	public void setShortposind(String shortposind) {
		this.shortposind = shortposind;
	}


	public String getAutosettind() {
		return autosettind;
	}


	public void setAutosettind(String autosettind) {
		this.autosettind = autosettind;
	}


	private static final long serialVersionUID = 1L;
}