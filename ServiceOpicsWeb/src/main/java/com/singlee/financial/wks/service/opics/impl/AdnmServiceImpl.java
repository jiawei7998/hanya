package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.wks.bean.opics.Adnm;
import com.singlee.financial.wks.intfc.AdnmMidService;
import com.singlee.financial.wks.mapper.opics.AdnmMapper;
import com.singlee.financial.wks.service.opics.AdnmService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class AdnmServiceImpl implements AdnmService, AdnmMidService {

    @Resource
    private AdnmMapper adnmMapper;

    @Override
    public int deleteByPrimaryKey(String br,String datatype,String product,String prodtype) {
        return adnmMapper.deleteByPrimaryKey(br,datatype,product,prodtype);
    }

    @Override
    public int insert(Adnm record) {
        return adnmMapper.insert(record);
    }

    @Override
    public int insertSelective(Adnm record) {
        return adnmMapper.insertSelective(record);
    }

    @Override
    public Adnm selectByPrimaryKey(String br,String datatype,String product,String prodtype) {
        return adnmMapper.selectByPrimaryKey(br,datatype,product,prodtype);
    }

    @Override
    public int updateByPrimaryKeySelective(Adnm record) {
        return adnmMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Adnm record) {
        return adnmMapper.updateByPrimaryKey(record);
    }

    @Override
    public String createCustNo() {
        Adnm adnm = null;
        String cno = null;
        adnm = selectByPrimaryKey("00","C","CUSTNO","CN");// 获取DEALNO
        if (null == adnm) {// 不存在则进行初始化
            adnm =new Adnm();
            insert(createAdnm(adnm,"00","C","CUSTNO","CN","1000000000",""));
        }
        cno = Integer.parseInt(StringUtils.trimToEmpty(adnm.getFonumber())) + 1 + "";

        // 更新CUST CNO
        adnm.setFonumber(cno);
        updateByPrimaryKeySelective(adnm);

        return cno;
    }

    private Adnm createAdnm(Adnm adnm,String br,String datatype,String product,String prodtype,String fonumber, String bonumber) {
        adnm.setBr(br);
        adnm.setDatatype(datatype);
        adnm.setProduct(product);
        adnm.setProdtype(prodtype);
        adnm.setFonumber(fonumber);
        adnm.setBonumber(bonumber);
        adnm.setLstmntdate(DateUtil.parse(DateUtil.getCurrentDateAsString(), "yyyy-MM-dd"));
        return adnm;
    }
}
