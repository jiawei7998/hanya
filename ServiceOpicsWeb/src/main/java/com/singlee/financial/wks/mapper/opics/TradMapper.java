package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Trad;
import org.apache.ibatis.annotations.Param;

public interface TradMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("trad") String trad);

    int insert(Trad record);

    int insertSelective(Trad record);

    Trad selectByPrimaryKey(@Param("br") String br, @Param("trad") String trad);

    int updateByPrimaryKeySelective(Trad record);

    int updateByPrimaryKey(Trad record);
    
    Trad selectByCfetsTradId(@Param("br") String br, @Param("trad") String trad);
}