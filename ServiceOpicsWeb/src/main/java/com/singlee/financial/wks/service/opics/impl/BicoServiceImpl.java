package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Bico;
import com.singlee.financial.wks.mapper.opics.BicoMapper;
import com.singlee.financial.wks.service.opics.BicoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class BicoServiceImpl implements BicoService {

    @Resource
    private BicoMapper bicoMapper;

    @Override
    public int deleteByPrimaryKey(String bic) {
        return bicoMapper.deleteByPrimaryKey(bic);
    }

    @Override
    public int insert(Bico record) {
        return bicoMapper.insert(record);
    }

    @Override
    public int insertSelective(Bico record) {
        return bicoMapper.insertSelective(record);
    }

    @Override
    public Bico selectByPrimaryKey(String bic) {
        return bicoMapper.selectByPrimaryKey(bic);
    }

    @Override
    public int updateByPrimaryKeySelective(Bico record) {
        return bicoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Bico record) {
        return bicoMapper.updateByPrimaryKey(record);
    }

}
