package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Pcix;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface PcixMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq, @Param("pcc") String pcc);

    int insert(Pcix record);

    int insertSelective(Pcix record);

    Pcix selectByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq, @Param("pcc") String pcc);

    List<Pcix>  selectPcixList(Pcix record);
    
    int updateByPrimaryKeySelective(Pcix record);

    int updateByPrimaryKey(Pcix record);
    
    int updateBatch(List<Pcix> list);

    int batchInsert(@Param("list") List<Pcix> list);
}