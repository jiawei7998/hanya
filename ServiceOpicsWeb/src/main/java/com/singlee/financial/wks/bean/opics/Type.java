package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Type implements Serializable {
    private String prodcode;

    private String type;

    private String descr;

    private Date lstmntdte;

    private String al;

    
    public String getProdcode() {
		return prodcode;
	}


	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getDescr() {
		return descr;
	}


	public void setDescr(String descr) {
		this.descr = descr;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public String getAl() {
		return al;
	}


	public void setAl(String al) {
		this.al = al;
	}


	private static final long serialVersionUID = 1L;
}