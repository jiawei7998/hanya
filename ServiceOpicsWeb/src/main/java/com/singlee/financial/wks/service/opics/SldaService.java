package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Slda;

public interface SldaService {


    int deleteByPrimaryKey(String br, String dealno, String seq, String dealind, String assignseq);

    int insert(Slda record);

    int insertSelective(Slda record);

    Slda selectByPrimaryKey(String br, String dealno, String seq, String dealind, String assignseq);
    List<Slda> selectByDealno(String br, String dealno);

    int updateByPrimaryKeySelective(Slda record);

    int updateByPrimaryKey(Slda record);
    
    void batchInsert(List<Slda> list);

}

