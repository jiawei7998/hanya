package com.singlee.financial.wks.service.opics.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.singlee.financial.wks.bean.opics.Coun;
import com.singlee.financial.wks.mapper.opics.CounMapper;
import com.singlee.financial.wks.service.opics.CounService;
@Service
public class CounServiceImpl implements CounService{

    @Resource
    private CounMapper counMapper;

    @Override
    public int deleteByPrimaryKey(String ccode) {
        return counMapper.deleteByPrimaryKey(ccode);
    }

    @Override
    public int insert(Coun record) {
        return counMapper.insert(record);
    }

    @Override
    public int insertSelective(Coun record) {
        return counMapper.insertSelective(record);
    }

    @Override
    public Coun selectByPrimaryKey(String ccode) {
        return counMapper.selectByPrimaryKey(ccode);
    }

    @Override
    public int updateByPrimaryKeySelective(Coun record) {
        return counMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Coun record) {
        return counMapper.updateByPrimaryKey(record);
    }

}
