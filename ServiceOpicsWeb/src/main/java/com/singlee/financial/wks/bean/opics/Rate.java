package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Rate implements Serializable {
    private String br;

    private String ratecode;

    private String ccy;

    private String ratetype;

    private String varfix;

    private String source;

    private String desmat;

    private BigDecimal spread8;

    private String basis;

    private String descr;

    private String ratefixday;

    private Date lstmntdate;

    private String fincal;

    private String ratesourceDde;

    private String ratesourceIsda;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getRatecode() {
		return ratecode;
	}


	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getRatetype() {
		return ratetype;
	}


	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}


	public String getVarfix() {
		return varfix;
	}


	public void setVarfix(String varfix) {
		this.varfix = varfix;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getDesmat() {
		return desmat;
	}


	public void setDesmat(String desmat) {
		this.desmat = desmat;
	}


	public BigDecimal getSpread8() {
		return spread8;
	}


	public void setSpread8(BigDecimal spread8) {
		this.spread8 = spread8;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public String getDescr() {
		return descr;
	}


	public void setDescr(String descr) {
		this.descr = descr;
	}


	public String getRatefixday() {
		return ratefixday;
	}


	public void setRatefixday(String ratefixday) {
		this.ratefixday = ratefixday;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public String getFincal() {
		return fincal;
	}


	public void setFincal(String fincal) {
		this.fincal = fincal;
	}


	public String getRatesourceDde() {
		return ratesourceDde;
	}


	public void setRatesourceDde(String ratesourceDde) {
		this.ratesourceDde = ratesourceDde;
	}


	public String getRatesourceIsda() {
		return ratesourceIsda;
	}


	public void setRatesourceIsda(String ratesourceIsda) {
		this.ratesourceIsda = ratesourceIsda;
	}


	private static final long serialVersionUID = 1L;
}