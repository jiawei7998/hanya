package com.singlee.financial.wks.service.cashflow;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.cashflow.HolidayBean;
import com.singlee.financial.wks.expand.DateConventionType;
import com.singlee.financial.wks.expand.SwapItemSchdWksBean;
import com.singlee.financial.wks.util.FinancialBasisUtils;
import com.singlee.financial.wks.util.FinancialCalculationModel;
import com.singlee.financial.wks.util.FinancialDateUtils;

public class SwapFactory {
	
	/**
	 * 方法已重载.创建利率互交易现金信息,计算已确认的利息金额,计算末实现的远期利率及利息金额
	 * 
	 * @param irs
	 * @param factory
	 * @param holidayDates
	 * @param resetRates
	 * @throws ParseException
	 * @throws IOException
	 */
	public void createCashFlow(SwapWksBean irs, DiscountFactorFactory factory, Map<String, HolidayBean> holidayDates, Map<Date, String> resetRates) throws ParseException, IOException {
		String yieldName = null;
		String ccy = null;
		String paymentFrequency = null;
		BigDecimal spreadRate = null;
		// 创建利率互交易现金信息,计算已确认的利息金额
		createCashFlow(irs, holidayDates, resetRates);
		
		
		if("L".equalsIgnoreCase(irs.getRecRateType())) {
			yieldName = irs.getRecYieldcurve();
			ccy = irs.getRecCcy();
			paymentFrequency = irs.getRecPaymentFrequency();
			spreadRate = irs.getRecFloatBps();
		}else if("L".equalsIgnoreCase(irs.getPayRateType())) {
			yieldName = irs.getPayYieldcurve();
			ccy = irs.getPayCcy();
			paymentFrequency = irs.getPayPaymentFrequency();
			spreadRate = irs.getPayFloatBps();
		}else {
			throw new RuntimeException("利率类型不支持");
		}

		// 计算末实现的远期利率及利息金额
		createCashFlow(irs.getItemBeanSwds(),irs.getItemBeanSwds().get(0),factory,holidayDates.get(ccy),0,irs.getAmt(), 
				paymentFrequency,yieldName,ccy,irs.getBatchDate(),FinancialCalculationModel.ZERO,spreadRate);
		
		//设置浮动端第一期利率
		setFloatRate(irs,resetRates);
	}

	/**
	 * 方法已重载.创建利率互交易现金信息,计算已确认的利息金额
	 * 
	 * @param irs
	 * @param holidayDates
	 * @param resetRates
	 * @throws ParseException
	 * @throws IOException
	 */
	public void createCashFlow(SwapWksBean irs, Map<String, HolidayBean> holidayDates, Map<Date, String> resetRates) throws ParseException, IOException {

		// 创建现金流信息并计算固定端利息金额
		createCashFlow(irs, holidayDates);

		// 计算浮动端已确定利息金额
		createCashFlow(irs.getItemBeanSwds(), irs.getItemBeanSwds().get(0), holidayDates.get("L".equalsIgnoreCase(irs.getRecRateType()) ? irs.getRecCcy(): ("L".equalsIgnoreCase(irs.getPayRateType()) ? irs.getPayCcy() : "")), 
				resetRates, 0, irs.getAmt(), FinancialDateUtils.parse(irs.getVDate()),
				"L".equalsIgnoreCase(irs.getRecRateType()) ? irs.getRecPaymentFrequency(): ("L".equalsIgnoreCase(irs.getPayRateType()) ? irs.getPayPaymentFrequency() : ""), 
				irs.getBatchDate(),
				"L".equalsIgnoreCase(irs.getRecRateType()) ? irs.getRecFloatBps() : ("L".equalsIgnoreCase(irs.getPayRateType()) ? irs.getPayFloatBps() : FinancialCalculationModel.ZERO),
				FinancialCalculationModel.ZERO);
		
		//设置浮动端第一期利率
		setFloatRate(irs,resetRates);
	}
	
	/**
	 * 方法已重载.根据业务对象、节假日信息创建现金流数据
	 * @param irs
	 * @param holidayDates
	 * @throws ParseException
	 * @throws IOException
	 */
	private void createCashFlow(SwapWksBean irs, Map<String, HolidayBean> holidayDates) throws ParseException, IOException {
		if (null == irs.getItemBeanSwds()) {
			irs.setItemBeanSwds(new ArrayList<SwapItemSchdWksBean>());
		}

		LogFactory.getLog(SwapFactory.class).debug("利率互换工作站对象:" + irs);

		// 根据利率类型创建现金流
		if ("X".equalsIgnoreCase(irs.getPayRateType()) && "L".equalsIgnoreCase(irs.getRecRateType())) {

			// 创建支付固定,收取浮动交易的现金流
			// 创建支付固定端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getPayNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getPayPaymentFrequency(),
					irs.getPayCalIntBasic(), irs.getPayFloatBps(), irs.getPayFixedRate(), irs.getPayRateType(), irs.getPaySeq(), OpicsConstant.SWAP.PAYRECIND_P, irs.getPayCalIntType(),
					irs.getPayYieldcurve(), irs.getPayCcy(), irs.getPayCashFlowMethod(), irs.getPayRateCode(), irs.getPayAcctNo(), irs.getPayAcctType(), FinancialDateUtils.format(irs.getBatchDate()));
			// 创建收取浮动端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getRecNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getRecPaymentFrequency(),
					irs.getRecCalIntBasic(), irs.getRecCalIntType(), irs.getRecFloatBps(), irs.getRecFixedRate(), irs.getRecInterestResetFrequency(), irs.getRecRateType(), irs.getRecCashFlowMethod(),
					irs.getRecSeq(), OpicsConstant.SWAP.PAYRECIND_R, irs.getRecYieldcurve(), irs.getRecCcy(), irs.getBatchDate(), irs.getRecRateCode(), irs.getRecAcctNo(), irs.getRecAcctType());

		} else if ("X".equalsIgnoreCase(irs.getRecRateType()) && "L".equalsIgnoreCase(irs.getPayRateType())) {

			// 创建收取固定,支付浮动交易的现金流
			// 创建收取固定端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getRecNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getRecPaymentFrequency(),
					irs.getRecCalIntBasic(), irs.getRecFloatBps(), irs.getRecFixedRate(), irs.getRecRateType(), irs.getRecSeq(), OpicsConstant.SWAP.PAYRECIND_R, irs.getRecCalIntType(),
					irs.getRecYieldcurve(), irs.getRecCcy(), irs.getRecCashFlowMethod(), irs.getRecRateCode(), irs.getRecAcctNo(), irs.getRecAcctType(), FinancialDateUtils.format(irs.getBatchDate()));
			// 创建支付浮动端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getPayNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getPayPaymentFrequency(),
					irs.getPayCalIntBasic(), irs.getPayCalIntType(), irs.getPayFloatBps(), irs.getPayFixedRate(), irs.getPayInterestResetFrequency(), irs.getPayRateType(), irs.getPayCashFlowMethod(),
					irs.getPaySeq(), OpicsConstant.SWAP.PAYRECIND_P, irs.getPayYieldcurve(), irs.getPayCcy(), irs.getBatchDate(), irs.getPayRateCode(), irs.getPayAcctNo(), irs.getPayAcctType());

		} else if ("X".equalsIgnoreCase(irs.getRecRateType()) && "X".equalsIgnoreCase(irs.getPayRateType())) {

			// 创建收取固定,支付固定交易的现金流
			// 创建收取固定端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getRecNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getRecPaymentFrequency(),
					irs.getRecCalIntBasic(), irs.getRecFloatBps(), irs.getRecFixedRate(), irs.getRecRateType(), irs.getRecSeq(), OpicsConstant.SWAP.PAYRECIND_R, irs.getRecCalIntType(),
					irs.getRecYieldcurve(), irs.getRecCcy(), irs.getRecCashFlowMethod(), irs.getRecRateCode(), irs.getRecAcctNo(), irs.getRecAcctType(), FinancialDateUtils.format(irs.getBatchDate()));
			// 创建支付固定端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getPayNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getPayPaymentFrequency(),
					irs.getPayCalIntBasic(), irs.getPayFloatBps(), irs.getPayFixedRate(), irs.getPayRateType(), irs.getPaySeq(), OpicsConstant.SWAP.PAYRECIND_P, irs.getPayCalIntType(),
					irs.getPayYieldcurve(), irs.getPayCcy(), irs.getPayCashFlowMethod(), irs.getPayRateCode(), irs.getPayAcctNo(), irs.getPayAcctType(), FinancialDateUtils.format(irs.getBatchDate()));

		} else if ("L".equalsIgnoreCase(irs.getRecRateType()) && "L".equalsIgnoreCase(irs.getPayRateType())) {

			// 创建收取浮动,支付浮动交易的现金流
			// 创建收取浮动端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getRecNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getRecPaymentFrequency(),
					irs.getRecCalIntBasic(), irs.getRecCalIntType(), irs.getRecFloatBps(), irs.getRecFixedRate(), irs.getRecInterestResetFrequency(), irs.getRecRateType(), irs.getRecCashFlowMethod(),
					irs.getRecSeq(), OpicsConstant.SWAP.PAYRECIND_R, irs.getRecYieldcurve(), irs.getRecCcy(), irs.getBatchDate(), irs.getRecRateCode(), irs.getRecAcctNo(), irs.getRecAcctType());
			// 创建支付浮动端现金流
			createCashFlow(irs.getItemBeanSwds(), holidayDates, irs.getPayNominalAmount(), irs.getIntPayAdjMod(), irs.getDealDate(), irs.getVDate(), irs.getMDate(), irs.getPayPaymentFrequency(),
					irs.getPayCalIntBasic(), irs.getPayCalIntType(), irs.getPayFloatBps(), irs.getPayFixedRate(), irs.getPayInterestResetFrequency(), irs.getPayRateType(), irs.getPayCashFlowMethod(),
					irs.getPaySeq(), OpicsConstant.SWAP.PAYRECIND_P, irs.getPayYieldcurve(), irs.getPayCcy(), irs.getBatchDate(), irs.getPayRateCode(), irs.getPayAcctNo(), irs.getPayAcctType());

		} else {
			throw new RuntimeException("利率类型不支持");
		} // if else if ....else
		
		// 计算固定端每一期对应的利息金额
		irs.getItemBeanSwds().stream().filter(swds -> "X".equalsIgnoreCase(swds.getRateType())).forEach(swds -> createCashFlow(swds,irs));
	}

	/**
	 * 方法已重载.创建利率互交易现金信息
	 * 
	 * @param irs
	 * @param holidayDates
	 * @throws ParseException
	 * @throws IOException
	 */
	public void createCashFlow(SwapWksBean irs, Map<String, HolidayBean> holidayDates,String firstRate) throws ParseException, IOException {

		//创建现金流并计算固定端利息金额
		createCashFlow(irs,holidayDates);
		
		if(StringUtils.isEmpty(firstRate)) {
			throw new RuntimeException("请到OPICS的RIHS界面中维护起息日对应的利率");
		}
		
		// 计算浮动端第一期对应的利息金额
		irs.getItemBeanSwds().stream().filter(swds -> "L".equalsIgnoreCase(swds.getRateType()) && "002".equalsIgnoreCase(swds.getSchdSeq())).forEach(swds -> {
				BigDecimal rate = new BigDecimal(StringUtils.trim(firstRate));
				BigDecimal spread = "L".equalsIgnoreCase(irs.getRecRateType()) ? irs.getRecFloatBps() : ("L".equalsIgnoreCase(irs.getPayRateType()) ? irs.getPayFloatBps() : FinancialCalculationModel.ZERO);
				BigDecimal intAmt = FinancialCalculationModel.calculateInterestAmount(swds.getPrinAmt(), 
						FinancialDateUtils.parse(swds.getBeginDate()),FinancialDateUtils.parse(swds.getEndDate()), rate.add(spread), swds.getBasis(), 
						"L".equalsIgnoreCase(irs.getRecRateType()) ? irs.getRecPaymentFrequency(): ("L".equalsIgnoreCase(irs.getPayRateType()) ? irs.getPayPaymentFrequency() : ""));
				swds.setInterestRate(rate);
				swds.setInterestAmt(intAmt);
				swds.setSumcompoundamt(intAmt);
				swds.setFlatintcompamt(FinancialCalculationModel.ZERO);
				swds.setVerdate(FinancialDateUtils.format(irs.getBatchDate()));
				swds.setPosnintamt(rate.multiply(swds.getPrinAmt()).divide(FinancialCalculationModel.ONE_HUNDRED, 10,RoundingMode.HALF_UP));
		});
		
		//设置浮动端第一期利率
		setFloatRate(irs, firstRate);
	}

	/**
	 * 方法已重载.计算小于当前批量日期并且已重定的记录,设置轧差后的付息金额
	 * 
	 * @param irs
	 */
	public void createCashFlow(SwapWksBean irs) {
		if(null == irs) {
			throw new RuntimeException("利率互换业务对象为空");
		}
		if(null == irs.getItemBeanSwds()) {
			throw new RuntimeException("利率互换现金流对象对象为空");
		}
		if(irs.getItemBeanSwds().size() == 0) {
			throw new RuntimeException("计算收付息金额前请先创建现金流信息");
		}
		
		// 计算已重定部分利息
		irs.getItemBeanSwds().stream().filter(swds -> Pattern.matches("[RPM]", swds.getSchdType())).collect(Collectors.groupingBy(swds -> swds.getPayDate())).keySet()
				.forEach(payDate -> createCashFlow(irs, payDate));
	}

	/**
	 * 方法已重载.利率重定后计算每个收付息日的轧差金额
	 * 
	 * @param list
	 */
	private void createCashFlow(SwapWksBean irs, String payDate) {
		SwapItemSchdWksBean recSchd = null;
		SwapItemSchdWksBean paySchd = null;
		BigDecimal recIntAmt = null;
		BigDecimal payIntAmt = null;
		BigDecimal netIntAmt = null;

		LogFactory.getLog(SwapFactory.class).debug("createCashFlow(SwapWksBean irs, String payDate)->payDate:" + payDate);

		if (FinancialDateUtils.parse(payDate).getTime() > irs.getBatchDate().getTime()) {
			LogFactory.getLog(SwapFactory.class).info("当前计息日期已大于批量日期计算结束");
			return;
		}

		// 获取收端利息总金额
		recSchd = irs.getItemBeanSwds().stream()
				.filter(swds -> Pattern.matches("[RPM]", swds.getSchdType()) && irs.getRecRateType().equalsIgnoreCase(swds.getRateType())
						&& (FinancialDateUtils.parse(swds.getPayDate()).equals(FinancialDateUtils.parse(payDate))
								|| FinancialDateUtils.parse(payDate).equals(FinancialDateUtils.parse(swds.getActualFixingDate())) || "M".equalsIgnoreCase(swds.getSchdType()))
					)
				.findFirst().get();

		LogFactory.getLog(SwapFactory.class).debug("createCashFlow(SwapWksBean irs, String payDate)->recSchd:" + recSchd);
		if (null == recSchd) {
			throw new RuntimeException(String.format("未找到付息日[%s]对应收取端记录", payDate));
		}

		// 获取付端利息总金额
		paySchd = irs.getItemBeanSwds().stream()
				.filter(swds -> Pattern.matches("[RPM]", swds.getSchdType()) && irs.getPayRateType().equalsIgnoreCase(swds.getRateType())
						&& (FinancialDateUtils.parse(swds.getPayDate()).equals(FinancialDateUtils.parse(payDate))
								|| FinancialDateUtils.parse(payDate).equals(FinancialDateUtils.parse(swds.getActualFixingDate())) || "M".equalsIgnoreCase(swds.getSchdType()))
					)
				.findFirst().get();

		LogFactory.getLog(SwapFactory.class).debug("createCashFlow(SwapWksBean irs, String payDate)->paySchd:" + paySchd);

		if (null == paySchd) {
			throw new RuntimeException(String.format("未找到付息日[%s]对应支付端记录", payDate));
		}

		// 根据利率类型创建现金流
		if ("X".equalsIgnoreCase(irs.getPayRateType()) && "L".equalsIgnoreCase(irs.getRecRateType())) {
			recIntAmt = recSchd.getSumcompoundamt();
			payIntAmt = paySchd.getInterestAmt();
		} else if ("X".equalsIgnoreCase(irs.getRecRateType()) && "L".equalsIgnoreCase(irs.getPayRateType())) {
			recIntAmt = recSchd.getInterestAmt();
			payIntAmt = paySchd.getSumcompoundamt();
		} else if ("X".equalsIgnoreCase(irs.getRecRateType()) && "X".equalsIgnoreCase(irs.getPayRateType())) {
			recIntAmt = recSchd.getInterestAmt();
			payIntAmt = paySchd.getInterestAmt();
		} else if ("L".equalsIgnoreCase(irs.getRecRateType()) && "L".equalsIgnoreCase(irs.getPayRateType())) {
			recIntAmt = recSchd.getSumcompoundamt();
			payIntAmt = paySchd.getSumcompoundamt();
		} else {
			throw new RuntimeException("利率类型不支持");
		} // if else if ....else
		
		if(FinancialCalculationModel.ZERO.equals(recIntAmt) || FinancialCalculationModel.ZERO.equals(payIntAmt)) {
			return;
		}

		// 计算收付金额
		netIntAmt = recIntAmt.add(payIntAmt);
		
		if(FinancialCalculationModel.ZERO.equals(netIntAmt)) {
			return;
		}

		// 设置收付息金额
		if (netIntAmt.doubleValue() < 0 && "P".equalsIgnoreCase(recSchd.getTradeDirection())) {
			recSchd.setNetipayAmt(netIntAmt);
		} else if (netIntAmt.doubleValue() < 0 && "R".equalsIgnoreCase(recSchd.getTradeDirection())) {
			paySchd.setNetipayAmt(netIntAmt);
		} else if (netIntAmt.doubleValue() > 0 && "P".equalsIgnoreCase(recSchd.getTradeDirection())) {
			paySchd.setNetipayAmt(netIntAmt);
		} else if (netIntAmt.doubleValue() > 0 && "R".equalsIgnoreCase(recSchd.getTradeDirection())) {
			recSchd.setNetipayAmt(netIntAmt);
		} else {
			throw new RuntimeException("收付款标识不支持");
		} // if else if ....else
	}

	/**
	 * 方法已重载.创建固定端现金流信息，生成第一期信息并调用生成期开始日期、结束日期、付款日期的方法
	 * 
	 * @param list
	 * @param holidayDates
	 * @param amt
	 * @param intPayAdjMod
	 * @param dealDate
	 * @param vdate
	 * @param mdate
	 * @param paymentFrequency
	 * @param basis
	 * @param floatBps
	 * @param rate
	 * @param rateType
	 * @param leg
	 * @param payRecInd
	 * @param compound
	 * @throws ParseException
	 */
	private void createCashFlow(List<SwapItemSchdWksBean> list, Map<String, HolidayBean> holidayDates, BigDecimal amt, String intPayAdjMod, String dealDate, String vdate, String mdate,
			String paymentFrequency, String basis, BigDecimal floatBps, BigDecimal rate, String rateType, String leg, String payRecInd, String compound, String yieldCurve, String ccy,
			String cashFlowMethod, String rateCode, String intSacc, String intMeans, String postdate) throws ParseException {
		Hashtable<String,Integer> preDays = new Hashtable<String,Integer>();
		// 根据付息日期及交易信息生成每一期现金流数据

		Calendar date = Calendar.getInstance();
		date.setTime(FinancialDateUtils.parse(vdate));
		
		LogFactory.getLog(SwapFactory.class).debug("交易起息日期:" + String.format("%1$tY-%1$tm-%1$td", date.getTime()));

		// 添加固定端首期
		list.add(createSwapItemSchdWksBean(date.getTime(), date.getTime(), rateType, null, null, null, 1, leg, payRecInd, payRecInd, FinancialDateUtils.parse(mdate), rateCode, ccy, amt, basis,
				compound, intSacc, intMeans));
		
		//获取起息日的日历日
		int dayOfMonth = date.get(Calendar.DAY_OF_MONTH);

		// 计算现金流信息
		createCashFlow(list, holidayDates.get(ccy), leg, payRecInd, date, FinancialDateUtils.parse(mdate), paymentFrequency, 
				FinancialDateUtils.dateConvertMapping(DateConventionType.PAYMENT,intPayAdjMod), rateType, paymentFrequency,
				FinancialDateUtils.dateConvertMapping(DateConventionType.INTEREST,intPayAdjMod), cashFlowMethod, 2, rateCode, ccy, amt, basis, compound, intSacc, intMeans,dayOfMonth,preDays);

	}

	/**
	 * 方法已重载.计算固定端每一个付息周期的利息金额
	 * 
	 * @param bean
	 * @param rate
	 * @param basis
	 * @param floatBps
	 * @param amt
	 * @param paymentFrequency
	 * @param compound
	 * @throws ParseException
	 */
	private void createCashFlow(SwapItemSchdWksBean bean,SwapWksBean irs) {
		String compound = bean.getCompound();
		String basis = bean.getBasis();
		BigDecimal amt = bean.getPrinAmt();
		BigDecimal rate = null;
		BigDecimal floatBps = null;
		String paymentFrequency = null;
		try {
			// 首期不需要计算
			if ("V".equalsIgnoreCase(bean.getSchdType())) {
				return;
			}
			
			if(bean.getSeq().equalsIgnoreCase(irs.getRecSeq())) {
				rate = irs.getRecFixedRate();
				floatBps = irs.getRecFloatBps();
				paymentFrequency = irs.getRecPaymentFrequency();
			}else if(bean.getSeq().equalsIgnoreCase(irs.getPaySeq())) {
				rate = irs.getPayFixedRate();
				floatBps = irs.getPayFloatBps();
				paymentFrequency = irs.getPayPaymentFrequency();
			}else {
				throw new RuntimeException("未匹配到现金流信息");
			}

			bean.setCompound(compound);

			if ("N".equalsIgnoreCase(compound)) {
				BigDecimal intAmt = FinancialCalculationModel.calculateInterestAmount(amt, FinancialDateUtils.parse(bean.getBeginDate()), FinancialDateUtils.parse(bean.getEndDate()),
						rate.add(floatBps), basis, paymentFrequency);

				bean.setInterestAmt(intAmt);
				bean.setImplintAmt(intAmt);
				bean.setInterestRate(rate);
				bean.setImplintRate8(rate);
				bean.setPosnintamt(rate.divide(FinancialCalculationModel.ONE_HUNDRED, 10, RoundingMode.HALF_UP).multiply(amt));
				bean.setVerdate(FinancialDateUtils.format(irs.getBatchDate()));
				bean.setInterestRate(rate);
			} else {
				throw new RuntimeException("固定端付复利计算暂不支持");
			} // end if else
		} catch (Exception e) {
			throw new RuntimeException("计算固定端利息金额异常", e);
		}
	}

	/**
	 * 方法已重载.创建浮动端的每个付息周期,调用每个周期生成现金流的方法，生成当前付息周期的明细现金流
	 * 
	 * @param factory
	 * @param list
	 * @param holidayDate
	 * @param resetRates
	 * @param amt
	 * @param intPayAdjMod
	 * @param dealDate
	 * @param vdate
	 * @param mdate
	 * @param paymentFrequency
	 * @param basis
	 * @param compound
	 * @param floatBps
	 * @param rate
	 * @param interestResetFrequency
	 * @param rateType
	 * @param cashFlowMethod
	 * @throws ParseException
	 * @throws IOException
	 */
	private void createCashFlow(List<SwapItemSchdWksBean> list, Map<String, HolidayBean> holidayDate, BigDecimal amt, String intPayAdjMod, String dealDate, String vdate, String mdate,
			String paymentFrequency, String basis, String compound, BigDecimal floatBps, BigDecimal rate, String interestResetFrequency, String rateType, String cashFlowMethod, String leg,
			String recPayInd, String yieldCurve, String ccy, Date batchDate, String rateCode, String intSacc, String intMeans) throws ParseException, IOException {
		// 根据付息日期及交易信息生成每一期现金流数据
		Hashtable<String,Integer> preDays = new Hashtable<String,Integer>();
		Calendar date = Calendar.getInstance();
		date.setTime(FinancialDateUtils.parse(vdate));
		
		Date nextResetDate = FinancialDateUtils.calculatePreWorkDate(FinancialDateUtils.parse(vdate), holidayDate.get(ccy).getHolidayDates());
		
		LogFactory.getLog(SwapFactory.class).info("交易起息日期:" + String.format("%1$tY-%1$tm-%1$td", date.getTime())+",首期重定日期:" + String.format("%1$tY-%1$tm-%1$td", nextResetDate));
		
		// 添加浮动端首期
		list.add(createSwapItemSchdWksBean(date.getTime(), date.getTime(), rateType, null, nextResetDate, FinancialDateUtils.parse(vdate), 1, leg, recPayInd, recPayInd, FinancialDateUtils.parse(mdate), rateCode, ccy, amt, basis,
				compound, intSacc, intMeans));
		
		//获取起息日的日历日
		int dayOfMonth = date.get(Calendar.DAY_OF_MONTH);

		// 计算现金流信息
		createCashFlow(list, holidayDate.get(ccy), leg, recPayInd, date, FinancialDateUtils.parse(mdate), paymentFrequency, 
				FinancialDateUtils.dateConvertMapping(DateConventionType.PAYMENT,intPayAdjMod), rateType, interestResetFrequency,
				FinancialDateUtils.dateConvertMapping(DateConventionType.INTEREST,intPayAdjMod), cashFlowMethod, 2, rateCode, ccy, amt, basis, compound, intSacc, intMeans,dayOfMonth,preDays);

	}

	/**
	 * 方法已重载.生成每个付息日信息,并调用生成每个计息周期的开始日期、结束日期、利率重定日期、利率重定生效日期信息方法
	 * 
	 * @param list
	 * @param holidayDate
	 * @param calDate
	 * @param mdate
	 * @param PaymentFrequency
	 * @param intPayAdjMod
	 * @throws ParseException
	 */
	private void createCashFlow(List<SwapItemSchdWksBean> list, HolidayBean holidayDate, String leg, String payRecInd, Calendar calDate, Date mdate, String paymentFrequency, String intPayAdjMod,
			String rateType, String reSetFrequency, String intCalAdjMod, String cashFlowMethod,int scheduleSeq, String rateCode, String ccy, BigDecimal amt, String basis,
			String compound, String intSacc, String intMeans,int dayOfMonth,Hashtable<String,Integer> preDays) throws ParseException {

		Date payDate = null;
		Date endDate = null;
		Date adjEndDate = null;
		int adjDays = 0;
		int seq;

		// 每一个付周期对应的开始日期
		Date startDate = calDate.getTime();

		// 设置浮动端现金流明细日历对象
		Calendar scheduleDate = Calendar.getInstance();
		scheduleDate.setTime(startDate);

		// 计算当前付息周期的结束日期
		FinancialDateUtils.calculateNextDate(dayOfMonth,calDate, mdate,paymentFrequency);
		// 当付息日大于到期日时则为到期日
		if (calDate.getTime().compareTo(mdate) == 0 || calDate.getTime().compareTo(mdate) > 0) {
			calDate.setTime(mdate);
		} // end if
		endDate = calDate.getTime();
		
		payDate = FinancialDateUtils.calculateNextWorkDate(endDate, holidayDate, intPayAdjMod);
		adjEndDate = FinancialDateUtils.calculateNextWorkDate(endDate, holidayDate, intCalAdjMod);
		// 计算顺延天数
		adjDays = FinancialDateUtils.calculateTenorDate(endDate, adjEndDate);
		if(adjDays != 0) {
			calDate.setTime(adjEndDate);
		}

		LogFactory.getLog(SwapFactory.class).info("当期开始日期：" + String.format("%1$tY-%1$tm-%1$td", startDate) +",当期结束日期：" + String.format("%1$tY-%1$tm-%1$td", endDate) + ",结束日期调整日期:"
				+ String.format("%1$tY-%1$tm-%1$td", adjEndDate) + ",是否为假日：" + holidayDate.getHolidayDates().contains(calDate.getTime()) + ",付息频率：" + paymentFrequency + ",重置频率：" + reSetFrequency
				+ ",付息调整规则：" + intPayAdjMod + ",计息调整规则：" + intCalAdjMod+ ",到期日：" + String.format("%1$tY-%1$tm-%1$td", mdate)
				+ ",利率类型：" + rateType);

		// 创建现金流信息
		if (paymentFrequency.equalsIgnoreCase(reSetFrequency)) {
			// 添加收付息计划
			list.add(createSwapItemSchdWksBean(startDate, adjEndDate, rateType, payDate, FinancialDateUtils.calculatePreWorkDate(adjEndDate, holidayDate.getHolidayDates()), adjEndDate, scheduleSeq,
					leg, payRecInd, payRecInd, mdate, rateCode, ccy, amt, basis, compound, intSacc, intMeans));
			// 付息频率与重置频率相同处理
			seq = ++scheduleSeq;
		} else {
			// 付息频率与重置频率不相同时处理
			seq = createCashFlow(list, holidayDate, leg, payRecInd, scheduleDate, startDate, endDate, paymentFrequency, intPayAdjMod, rateType, reSetFrequency, intCalAdjMod, payDate, cashFlowMethod,
					mdate, preDays, scheduleSeq, adjEndDate, rateCode, ccy, amt, basis, compound, intSacc, intMeans);
		}

		// 当日期等于或大于到期日时退出循环
		if (calDate.getTime().compareTo(mdate) == 0 || calDate.getTime().compareTo(mdate) > 0) {
			return;
		} // end if
		createCashFlow(list, holidayDate, leg, payRecInd, calDate, mdate, paymentFrequency, intPayAdjMod, rateType, reSetFrequency, intCalAdjMod, cashFlowMethod,seq, rateCode, ccy, amt,
				basis, compound, intSacc, intMeans,dayOfMonth,preDays);
	}

	/**
	 * 方法已重载.处理付息频率与重置频率不一致的场景,生成每个付息周期的开始日期、结束日期、利率重定日期、利率重定生效日期信息
	 * 
	 * @param list
	 * @param holidayDate
	 * @param scheduleDate
	 * @param intStartDate
	 * @param intEndDate
	 * @param paymentFrequency
	 * @param intPayAdjMod
	 * @param reSetFrequency
	 * @param intCalAdjMod
	 * @param paymentDate
	 * @param cashFlowMethod
	 *            CHINA-中国FR007标准、ISDA-国际标准
	 * @throws ParseException
	 */
	private int createCashFlow(List<SwapItemSchdWksBean> list, HolidayBean holidayDate, String leg, String payRecInd, Calendar scheduleDate, Date intStartDate, Date intEndDate,
			String paymentFrequency, String intPayAdjMod, String rateType, String reSetFrequency, String intCalAdjMod, Date paymentDate, String cashFlowMethod, Date mdate, Hashtable<String,Integer> preDays,
			int scheduleSeq, Date intEndAdjDate, String rateCode, String ccy, BigDecimal amt, String basis, String compound, String intSacc, String intMeans) throws ParseException {

		Date calcIntEndDate = null;
		Date calcStartDate = null;
		int startAdjDays = 0;
		int nextAdjDays = 0;

		// 首期残断处理
		if (scheduleDate.getTime().compareTo(intStartDate) == 0) {
			Integer nPreDays = preDays.get(leg+(scheduleSeq-1));
			startAdjDays = null == nPreDays ? 0 : nPreDays.intValue();
		} else {
			startAdjDays = 0;
		}

		calcStartDate = scheduleDate.getTime();

		// 计算当前计息周期的结束日期
		FinancialDateUtils.calculateNextDate(scheduleDate, reSetFrequency, startAdjDays);

		// 最后一期残断处理
		if (scheduleDate.getTime().compareTo(intEndAdjDate) == 0 || scheduleDate.getTime().compareTo(intEndAdjDate) > 0) {
			calcIntEndDate = intEndAdjDate;
			nextAdjDays = AdjustIntStartFirstDays(calcIntEndDate, calcStartDate, cashFlowMethod);
			preDays.put(leg+scheduleSeq, nextAdjDays);
		} else {
			calcIntEndDate = scheduleDate.getTime();
		} // end if else

		LogFactory.getLog(SwapFactory.class).info("第" + scheduleSeq + "期,计息开始日期：" + String.format("%1$tY-%1$tm-%1$td", calcStartDate) + ",计息结束日期：" + String.format("%1$tY-%1$tm-%1$td", scheduleDate.getTime()) + ",调整后的结束日期："
				+ String.format("%1$tY-%1$tm-%1$td", calcIntEndDate) + ",付息日期:" + String.format("%1$tY-%1$tm-%1$td", paymentDate) + ",当期调整天数：" + startAdjDays + ",现金流规则方法:" + cashFlowMethod);
		
		// 添加收付息计划
		list.add(createSwapItemSchdWksBean(calcStartDate, calcIntEndDate, rateType, paymentDate, FinancialDateUtils.calculatePreWorkDate(calcIntEndDate, holidayDate.getHolidayDates()), calcIntEndDate,
				scheduleSeq, leg, payRecInd, payRecInd, mdate, rateCode, ccy, amt, basis, compound, intSacc, intMeans));

		// 当日期等于或当期付息周期付息日
		if (calcIntEndDate.compareTo(intEndAdjDate) == 0 || calcIntEndDate.compareTo(intEndAdjDate) > 0) {
			return ++scheduleSeq;
		} // end if
		return createCashFlow(list, holidayDate, leg, payRecInd, scheduleDate, intStartDate, intEndDate, paymentFrequency, intPayAdjMod, rateType, reSetFrequency, intCalAdjMod, paymentDate,
				cashFlowMethod, mdate, preDays, ++scheduleSeq, intEndAdjDate, rateCode, ccy, amt, basis, compound, intSacc, intMeans);
	}

	/**
	 * 方法已重载.创建收付息计划明细对应
	 * 
	 * @param startDate
	 * @param endDate
	 * @param rateType
	 * @param payDate
	 * @param fixingDate
	 * @param actualFixingDate
	 * @param schdSeq
	 * @param leg
	 * @param schdType
	 * @param tradeDirection
	 * @param payRecInd
	 * @return
	 */
	private SwapItemSchdWksBean createSwapItemSchdWksBean(Date startDate, Date endDate, String rateType, Date payDate, Date fixingDate, Date actualFixingDate, int schdSeq, String leg,
			String tradeDirection, String payRecInd, Date mdate, String rateCode, String ccy, BigDecimal amt, String basis, String compound, String intSacc, String intMeans) {
		String schdType = null;
		SwapItemSchdWksBean bean = new SwapItemSchdWksBean();

		bean.setRateType(rateType);
		// 设置付息日
		bean.setPayDate(FinancialDateUtils.format(payDate));
		bean.setBeginDate(FinancialDateUtils.format(startDate));
		bean.setEndDate(FinancialDateUtils.format(endDate));
		bean.setSchdSeq(createSeq(schdSeq));
		bean.setSeq(leg);

		if (endDate.equals(startDate)) {
			schdType = "V";
		} else {
			schdType = "X".equals(rateType) ? "P" : "L".equals(rateType) ? "R" :  "";
			if(StringUtils.isEmpty(schdType)) {
				throw new RuntimeException("收付息计划类型[SCHDTYPE]为空!");
			}
		}

		// 当日期等于或大于到期日时为最后一期
		if (endDate.compareTo(mdate) == 0 || endDate.compareTo(mdate) > 0) {
			schdType = "M";
		} // end if

		if (!"M".equalsIgnoreCase(schdType)) {
			// 利率重定日期
			bean.setFixingDate(FinancialDateUtils.format(fixingDate));
			// 利率重定生效日期
			bean.setActualFixingDate(FinancialDateUtils.format(actualFixingDate));
		}

		bean.setSchdType(schdType);
		bean.setTradeDirection(payRecInd);
		bean.setRateCode(rateCode);
		bean.setCcy(ccy);
		bean.setPrinAmt(amt);
		bean.setBasis(basis);
		bean.setCompound(compound);

		bean.setImplintAmt(FinancialCalculationModel.ZERO);
		bean.setInterestRate(FinancialCalculationModel.ZERO);
		bean.setImplintRate8(FinancialCalculationModel.ZERO);
		bean.setInterestAmt(FinancialCalculationModel.ZERO);
		bean.setPosnintamt(FinancialCalculationModel.ZERO);
		bean.setIntsacct(intSacc);
		bean.setIntsmeans(intMeans);
		bean.setFlatintcompamt(FinancialCalculationModel.ZERO);
		bean.setSumcompoundamt(FinancialCalculationModel.ZERO);
		bean.setNetipayAmt(FinancialCalculationModel.ZERO);
		return bean;
	}

	/**
	 * 方法已重载.计算浮动端已确定的利息金额
	 * 
	 * @param list
	 * @param bean
	 * @param holidayDate
	 * @param resetRates
	 * @param index
	 * @param compoundPrinAmt
	 * @param nextResetDate
	 * @param paymentFrequency
	 * @param batchDate
	 * @param firstRate
	 * @param sumCompoundIntAmt
	 * @throws IOException
	 * @throws ParseException
	 */
	private void createCashFlow(List<SwapItemSchdWksBean> list, SwapItemSchdWksBean bean, HolidayBean holidayDate, Map<Date, String> resetRates, int index, BigDecimal compoundPrinAmt,
			Date nextResetDate, String paymentFrequency, Date batchDate,BigDecimal floatBps,BigDecimal sumCompoundIntAmt) throws IOException, ParseException {

		if (null == bean) {
			return;
		}

		BigDecimal intAmt = null;
		BigDecimal compoundAmt = null;
		Date reSetDate = nextResetDate;
		BigDecimal amt = bean.getPrinAmt();
		String basis = bean.getBasis();
		String compound = bean.getCompound();
		BigDecimal sumIntAmt = null;
		BigDecimal nIntAmt = null;
		BigDecimal rate = null;
		
		LogFactory.getLog(SwapFactory.class).debug(String.format("利率重定日期:%1$tY-%1$tm-%1$td,当前批量日期:%2$tY-%2$tm-%2$td", reSetDate,batchDate));

		if ("L".equalsIgnoreCase(bean.getRateType()) && ("P".equalsIgnoreCase(bean.getSchdType()) || 
				"R".equalsIgnoreCase(bean.getSchdType()) || "M".equalsIgnoreCase(bean.getSchdType()))
			&& reSetDate.getTime() < batchDate.getTime() && FinancialCalculationModel.ZERO.equals(bean.getInterestRate())) {


			// 获取利率，如果没有就用曲线计算一个，这里用已存在的静态数据
			String str_rate = getFloatRate(resetRates, reSetDate, holidayDate.getHolidayDates());

			if (null == str_rate || str_rate.trim().length() == 0) {
				throw new RuntimeException(String.format("找不到定盘利率数据,利率代码[%s],重定日期[%s],请到RHIS界面中维护定盘利率", bean.getRateCode(), FinancialDateUtils.format(reSetDate)));
			} else {
				
				rate = new BigDecimal(StringUtils.trimToEmpty(str_rate));
				intAmt = FinancialCalculationModel.calculateInterestAmount(compoundPrinAmt, FinancialDateUtils.parse(bean.getBeginDate()), FinancialDateUtils.parse(bean.getEndDate()),
						rate.add(floatBps), basis, paymentFrequency);
				
				LogFactory.getLog(SwapFactory.class).info("复利利息金额：" + intAmt);

				bean.setInterestAmt(intAmt);
				bean.setInterestRate(rate);
				bean.setImplintAmt(intAmt);
				bean.setImplintRate8(rate);
				bean.setVerdate(FinancialDateUtils.format(batchDate));
			} // end if else
			bean.setPosnintamt(rate.divide(FinancialCalculationModel.ONE_HUNDRED, 10, RoundingMode.HALF_UP).multiply(amt));
			// 单利利息金额
			nIntAmt = FinancialCalculationModel.calculateInterestAmount(amt, FinancialDateUtils.parse(bean.getBeginDate()), FinancialDateUtils.parse(bean.getEndDate()), rate.add(floatBps), basis,
					paymentFrequency);
			LogFactory.getLog(SwapFactory.class).info("单利利息金额：" + intAmt+",复利利息金额累计：" + sumCompoundIntAmt);
			if ("F".equalsIgnoreCase(compound)) {
				compoundAmt = compoundPrinAmt.add(intAmt);
				sumIntAmt = sumCompoundIntAmt.add(intAmt);
				bean.setSumcompoundamt(sumIntAmt);
				Date nextWorkDate = FinancialDateUtils.calculateNextWorkDate(FinancialDateUtils.parse(bean.getEndDate()), holidayDate.getHolidayDates());
				if (bean.getEndDate().equalsIgnoreCase(bean.getPayDate()) || FinancialDateUtils.parse(bean.getEndDate()).equals(nextWorkDate)) {
					compoundAmt = amt;
					sumIntAmt = FinancialCalculationModel.ZERO;
				}
			} else {
				compoundAmt = amt;
				sumIntAmt = sumCompoundIntAmt;
			}
			// 下一个重定生效日期,当期的结束日期为下一期的开始日期,计息规则为算头不算尾
			reSetDate = FinancialDateUtils.parse(bean.getActualFixingDate());
			bean.setFlatintcompamt(nIntAmt.subtract(intAmt));
		} else {
			compoundAmt = amt;
			sumIntAmt = sumCompoundIntAmt;
		} // end if

		if (index + 1 >= list.size()) {
			return;
		}
		createCashFlow(list, list.get(index + 1), holidayDate, resetRates, index + 1, compoundAmt, reSetDate, paymentFrequency, batchDate,floatBps,sumIntAmt);
	}

	/**
	 * 方法已重载.计算浮动端末来隐含利息金额
	 * 
	 * @param list
	 * @param bean
	 * @param factory
	 * @param holidayDate
	 * @param resetRates
	 * @param basis
	 * @param compound
	 * @param floatBps
	 * @param amt
	 * @param index
	 * @param compoundPrinAmt
	 * @param nextResetDate
	 * @param paymentFrequency
	 * @throws IOException
	 * @throws ParseException
	 */
	private void createCashFlow(List<SwapItemSchdWksBean> list, SwapItemSchdWksBean bean, DiscountFactorFactory factory, HolidayBean holidayDate, 
			int index, BigDecimal compoundPrinAmt,String paymentFrequency, String yieldCurve, String ccy, Date batchDate, 
			BigDecimal sumCompoundIntAmt,BigDecimal floatBps) throws IOException, ParseException {

		if (null == bean) {
			return;
		}

		BigDecimal intAmt = null;
		BigDecimal compoundAmt = null;
		BigDecimal amt = bean.getPrinAmt();
		String basis = bean.getBasis();
		String compound = bean.getCompound();
		BigDecimal sumIntAmt = null;
		BigDecimal nIntAmt = null;
		if ("L".equalsIgnoreCase(bean.getRateType()) && ("P".equalsIgnoreCase(bean.getSchdType()) || 
				"R".equalsIgnoreCase(bean.getSchdType()) || "M".equalsIgnoreCase(bean.getSchdType()))
			&& FinancialDateUtils.parse(bean.getBeginDate()).getTime() >= batchDate.getTime()) {

			BigDecimal rate = null;

			Date startDate = FinancialDateUtils.parse(bean.getBeginDate());
			Date endDate = FinancialDateUtils.parse(bean.getEndDate());
			SwapItemSchdWksBean preSchd = getPreSchdBean(list, bean);
			LogFactory.getLog(SwapFactory.class).info("上一期收付息计划信息" + preSchd+",当期收付息计划信息" + bean);
			Date preStartDate = FinancialDateUtils.parse(preSchd.getBeginDate());
			Date preEndDate = FinancialDateUtils.parse(preSchd.getEndDate());
			Date prePayDate = FinancialDateUtils.parse(preSchd.getPayDate());
			// 使用曲线计算一个远期利率
			rate = factory.build(preStartDate, preEndDate, prePayDate, preSchd.getInterestRate(), startDate, endDate,
					FinancialBasisUtils.createBasis(basis, paymentFrequency, FinancialDateUtils.calculateTenorDate(startDate, endDate)), ccy.trim() + yieldCurve.trim());
			LogFactory.getLog(SwapFactory.class).info("隐含利率:" + rate.doubleValue() + ",开始日期:" + FinancialDateUtils.format(startDate) + ",结束日期:" + FinancialDateUtils.format(endDate));
			intAmt = FinancialCalculationModel.calculateInterestAmount(compoundPrinAmt, FinancialDateUtils.parse(bean.getBeginDate()), 
					FinancialDateUtils.parse(bean.getEndDate()), rate.add(floatBps),
					basis, paymentFrequency);

			bean.setImplintAmt(intAmt);
			bean.setImplintRate8(rate);
			bean.setInterestRate(FinancialCalculationModel.ZERO);
			bean.setInterestAmt(FinancialCalculationModel.ZERO);
			LogFactory.getLog(SwapFactory.class).info("隐含利息金额：" + intAmt);
			bean.setPosnintamt(rate.divide(FinancialCalculationModel.ONE_HUNDRED, 10, RoundingMode.HALF_UP).multiply(amt));
			// 单利利息金额
			nIntAmt = FinancialCalculationModel.calculateInterestAmount(amt, FinancialDateUtils.parse(bean.getBeginDate()), FinancialDateUtils.parse(bean.getEndDate()), rate.add(floatBps), basis,
					paymentFrequency);
			LogFactory.getLog(SwapFactory.class).info("单利利息金额：" + intAmt+",复利利息金额累计：" + sumCompoundIntAmt);
			if ("F".equalsIgnoreCase(compound)) {
				compoundAmt = compoundPrinAmt.add(intAmt);
				sumIntAmt = sumCompoundIntAmt.add(intAmt);
				bean.setSumcompoundamt(sumIntAmt);
				Date nextWorkDate = FinancialDateUtils.calculateNextWorkDate(FinancialDateUtils.parse(bean.getEndDate()), holidayDate.getHolidayDates());
				if (bean.getEndDate().equalsIgnoreCase(bean.getPayDate()) || FinancialDateUtils.parse(bean.getEndDate()).equals(nextWorkDate)) {
					compoundAmt = amt;
					sumIntAmt = FinancialCalculationModel.ZERO;
				}
			} else {
				compoundAmt = amt;
				sumIntAmt = sumCompoundIntAmt;
			}
			bean.setFlatintcompamt(nIntAmt.subtract(intAmt));
		} else {
			compoundAmt = amt;
			sumIntAmt = sumCompoundIntAmt;
		} // end if

		if (index + 1 >= list.size()) {
			return;
		}
		createCashFlow(list, list.get(index + 1), factory, holidayDate,index + 1, compoundAmt, paymentFrequency, yieldCurve, ccy, batchDate,sumIntAmt,floatBps);
	}

	/**
	 * 获取下一期的重定利率值
	 * 
	 * @param resetRates
	 * @param nextStartDate
	 * @param startDate
	 * @return
	 */
	private String getFloatRate(Map<Date, String> resetRates, Date nextStartDate, List<Date> list) {
		String rate = null;
		Date nextWorkDate = nextStartDate;

		// 获取计息开始日期利率
		rate = resetRates.get(nextWorkDate);

		// 十一或者过年放长假时需要特殊处理,如果还是找不到记录则说明没有维护市场利率
		if (null == rate) {
			nextWorkDate = FinancialDateUtils.calculateNextWorkDate(nextStartDate, list);
			// 如果下一个工作日是下一个自然日说明中间没有假日,应该为利率数据维护问题,则不进行赋值
			if (!nextWorkDate.equals(FinancialDateUtils.calculateNextDate(nextStartDate))) {
				rate = resetRates.get(nextWorkDate);
			} // end if
		} // end if

		LogFactory.getLog(SwapFactory.class).info("重定生效日期:" + FinancialDateUtils.format(nextStartDate) + ",重定利率：" + rate + ",遇假日取下一个工作日的数据,对应日期:" + FinancialDateUtils.format(nextWorkDate));
		return rate;
	}

	// 获取上一个收付息计划
	private SwapItemSchdWksBean getPreSchdBean(List<SwapItemSchdWksBean> list, SwapItemSchdWksBean bean) {
		SwapItemSchdWksBean swapItemSchdWksBean = null;

		for (SwapItemSchdWksBean var : list) {
			if (var.getEndDate().equalsIgnoreCase(bean.getBeginDate()) && var.getSeq().equalsIgnoreCase(bean.getSeq()) && var.getRateType().equalsIgnoreCase(bean.getRateType())) {
				swapItemSchdWksBean = var;
				break;
			} // end if
		} // end for

		return swapItemSchdWksBean;
	}

	/**
	 * 浮动端残断处理
	 * 
	 * @param holidayDate
	 * @param scheduleDate
	 * @param calIntEndDate
	 * @param reSetFrequency
	 * @param intCalAdjMod
	 * @param paymentDate
	 * @param cashFlowMethod
	 * @return
	 */
	private Integer AdjustIntStartFirstDays(Date calcIntEndDate, Date lastStartDate, String cashFlowMethod) {
		int adjDays = 0;

		if ("CHINA".equalsIgnoreCase(cashFlowMethod)) {
			adjDays = 0;
		} else if ("ISDA".equalsIgnoreCase(cashFlowMethod)) {
			adjDays = FinancialDateUtils.calculateTenorDate(lastStartDate, calcIntEndDate);
		} else {
			throw new RuntimeException("现金残断方法不支持");
		} // end if else if ... else

		return adjDays;
	}

	/**
	 * 格式化收付息计划的序号,返回格式为3位字符,如:001
	 * 
	 * @param vlaue
	 * @return
	 */
	private String createSeq(int vlaue) {
		return String.format("%03d", vlaue);
	}

	/**
	 * 方法已重载.设置浮动端第一期利息金额
	 * @param swap
	 * @param rate
	 */
	private void setFloatRate(SwapWksBean swap,String rate) {
		BigDecimal bigRate = null;
		if(org.springframework.util.StringUtils.hasText(rate)) {
			bigRate = new BigDecimal(StringUtils.trim(rate));
		}//end if
		
		if("L".equalsIgnoreCase(swap.getPayRateType())) {
			swap.setPayFixedRate(bigRate);
		}else if("L".equalsIgnoreCase(swap.getRecRateType())) {
			swap.setRecFixedRate(bigRate);
		}
	}
	
	/**
	 * 方法已重载.设置浮动端第一期利息金额
	 * @param swap
	 * @param rate
	 */
	private void setFloatRate(SwapWksBean swap,Map<Date, String> resetRates) {
		String value = null;
		Date vdate = null;
		
		if(!org.springframework.util.StringUtils.hasText(swap.getVDate())) {
			throw new RuntimeException("交易起息日不能为空");
		}//end if
		vdate = DateUtil.parse(swap.getVDate());
		if(null != resetRates) {
			value = resetRates.get(vdate);
		}//end if
		if(org.springframework.util.StringUtils.hasText(value)) {
			setFloatRate(swap,value);
		}//end if
	}
}
