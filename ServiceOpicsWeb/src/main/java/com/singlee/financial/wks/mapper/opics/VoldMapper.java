package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Vold;
import java.math.BigDecimal;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface VoldMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("optiontype") String optiontype, @Param("optiontypekey") String optiontypekey, @Param("desmat") String desmat, @Param("callput") String callput, @Param("strikeprice8") BigDecimal strikeprice8, @Param("term") String term);

    int insert(Vold record);

    int insertSelective(Vold record);

    Vold selectByPrimaryKey(@Param("br") String br, @Param("optiontype") String optiontype, @Param("optiontypekey") String optiontypekey, @Param("desmat") String desmat, @Param("callput") String callput, @Param("strikeprice8") BigDecimal strikeprice8, @Param("term") String term);

    int updateByPrimaryKeySelective(Vold record);

    int updateByPrimaryKey(Vold record);

    int updateBatch(List<Vold> list);

    int updateBatchSelective(List<Vold> list);
    
    List<Vold> selectAll();
}