package com.singlee.financial.wks.service.cashflow.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.wks.bean.cashflow.YieldCurveContributeBean;
import com.singlee.financial.wks.mapper.cashflow.YieldCurveContributeMapper;
import com.singlee.financial.wks.service.cashflow.YieldCurveContributeService;

@Service
public class YieldCurveContributeServiceImpl implements YieldCurveContributeService{

	@Autowired
	YieldCurveContributeMapper yieldCurveContributeMapper;
	
	@Override
	public List<YieldCurveContributeBean> selectYieldCurve(YieldCurveContributeBean contributeBean) {
		return yieldCurveContributeMapper.selectYieldCurve(contributeBean);
	}

}
