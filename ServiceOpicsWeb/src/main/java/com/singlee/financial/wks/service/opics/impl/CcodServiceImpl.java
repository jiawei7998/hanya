package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Ccod;
import com.singlee.financial.wks.mapper.opics.CcodMapper;
import com.singlee.financial.wks.service.opics.CcodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CcodServiceImpl implements CcodService {

    @Resource
    private CcodMapper ccodMapper;

    @Override
    public int deleteByPrimaryKey(String ccy) {
        return ccodMapper.deleteByPrimaryKey(ccy);
    }

    @Override
    public int insert(Ccod record) {
        return ccodMapper.insert(record);
    }

    @Override
    public int insertSelective(Ccod record) {
        return ccodMapper.insertSelective(record);
    }

    @Override
    public Ccod selectByPrimaryKey(String ccy) {
        return ccodMapper.selectByPrimaryKey(ccy);
    }

    @Override
    public int updateByPrimaryKeySelective(Ccod record) {
        return ccodMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Ccod record) {
        return ccodMapper.updateByPrimaryKey(record);
    }

}
