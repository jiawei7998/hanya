package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.opics.Sfdi;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.mapper.opics.SfdiMapper;
import com.singlee.financial.wks.service.opics.SfdiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
@Service
public class SfdiServiceImpl implements SfdiService {

    @Resource
    private SfdiMapper sfdiMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct) {
        return sfdiMapper.deleteByPrimaryKey(br,cno,delrecind,product,prodtype,ccy,safekeepacct);
    }

    @Override
    public int insert(Sfdi record) {
        return sfdiMapper.insert(record);
    }

    @Override
    public int insertSelective(Sfdi record) {
        return sfdiMapper.insertSelective(record);
    }

    @Override
    public Sfdi selectByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct) {
        return sfdiMapper.selectByPrimaryKey(br,cno,delrecind,product,prodtype,ccy,safekeepacct);
    }

    @Override
    public int updateByPrimaryKeySelective(Sfdi record) {
        return sfdiMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Sfdi record) {
        return sfdiMapper.updateByPrimaryKey(record);
    }

	@Override
	public void batchInsert(List<Sfdi> list) {
		if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.SfdiMapper.insert", list);
	}
	
	private Sfdi builder(String br, String product, String type, String cno, String delrecind, String ccy, String safekeepacct,
	        String settacct, String settmeans, String swiftbothind) {
		Sfdi sfdi = new Sfdi();
		sfdi.setBr(br);//机构:
		sfdi.setProduct(product);//产品:
		sfdi.setProdtype(type);//产品类型:
		sfdi.setCno(cno);//交易对手:
		sfdi.setDelrecind(delrecind);//D/R:D付/R收
		sfdi.setCcy(ccy);//币种:
		sfdi.setSafekeepacct(safekeepacct);//托管账户:
		sfdi.setLstmntdate(Calendar.getInstance().getTime());//:
		sfdi.setSettacct(settacct);//账号:
		sfdi.setSettmeans(settmeans);//帐户行:
		sfdi.setSwiftbothind(swiftbothind);//:
		return sfdi;
	}

	@Override
	public List<Sfdi> builder(Spsh spsh) {
		List<Sfdi> sfdis = new ArrayList<Sfdi>();
		if (null == spsh) {
            JY.raise("现券买卖交易对象为空!");
        }
		sfdis.add(builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getCno(),
				"D", spsh.getCcy(), spsh.getSecsacct(), spsh.getCcysacct(), spsh.getCcysmeans(), "0"));
		sfdis.add(builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getCno(),
				"R", spsh.getCcy(), spsh.getSecsacct(), spsh.getCcysacct(), spsh.getCcysmeans(), "0"));
		return null;
	}

}
