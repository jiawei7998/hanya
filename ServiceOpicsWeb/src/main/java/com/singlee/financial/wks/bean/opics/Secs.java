package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Secs implements Serializable {
    private String secid;

    private String seq;

    private Date ipaydate;

    private BigDecimal callprice8;

    private BigDecimal cashflow8;

    private BigDecimal intrate8;

    private Date exdivdate;

    private BigDecimal intpayamt8;

    private Date intenddte;

    private Date intstrtdte;

    private BigDecimal caprate8;

    private String ratecode;

    private Date ratefixdate;

    private BigDecimal floorrate8;

    private BigDecimal spreadrate8;

    private Date lstmntdate;

    private BigDecimal prinamt8;

    private BigDecimal prinpayamt8;

    private BigDecimal putprice8;

    private BigDecimal steprate8;

    private Date exerdate;

    private Date publishdate;

    private String basis;

    private BigDecimal notprinamt8;

    private BigDecimal notprinpayamt8;

    private BigDecimal notintpayamt8;

    private Long updatecounter;

    private BigDecimal groupid;

    private BigDecimal formulaid;

    private BigDecimal fixingoverride;

    private Date ratesetdate;

    private String intsettccy;

    private String capfloorind;

    private String raterefixind;

    
    public String getSecid() {
		return secid;
	}


	public void setSecid(String secid) {
		this.secid = secid;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public Date getIpaydate() {
		return ipaydate;
	}


	public void setIpaydate(Date ipaydate) {
		this.ipaydate = ipaydate;
	}


	public BigDecimal getCallprice8() {
		return callprice8;
	}


	public void setCallprice8(BigDecimal callprice8) {
		this.callprice8 = callprice8;
	}


	public BigDecimal getCashflow8() {
		return cashflow8;
	}


	public void setCashflow8(BigDecimal cashflow8) {
		this.cashflow8 = cashflow8;
	}


	public BigDecimal getIntrate8() {
		return intrate8;
	}


	public void setIntrate8(BigDecimal intrate8) {
		this.intrate8 = intrate8;
	}


	public Date getExdivdate() {
		return exdivdate;
	}


	public void setExdivdate(Date exdivdate) {
		this.exdivdate = exdivdate;
	}


	public BigDecimal getIntpayamt8() {
		return intpayamt8;
	}


	public void setIntpayamt8(BigDecimal intpayamt8) {
		this.intpayamt8 = intpayamt8;
	}


	public Date getIntenddte() {
		return intenddte;
	}


	public void setIntenddte(Date intenddte) {
		this.intenddte = intenddte;
	}


	public Date getIntstrtdte() {
		return intstrtdte;
	}


	public void setIntstrtdte(Date intstrtdte) {
		this.intstrtdte = intstrtdte;
	}


	public BigDecimal getCaprate8() {
		return caprate8;
	}


	public void setCaprate8(BigDecimal caprate8) {
		this.caprate8 = caprate8;
	}


	public String getRatecode() {
		return ratecode;
	}


	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}


	public Date getRatefixdate() {
		return ratefixdate;
	}


	public void setRatefixdate(Date ratefixdate) {
		this.ratefixdate = ratefixdate;
	}


	public BigDecimal getFloorrate8() {
		return floorrate8;
	}


	public void setFloorrate8(BigDecimal floorrate8) {
		this.floorrate8 = floorrate8;
	}


	public BigDecimal getSpreadrate8() {
		return spreadrate8;
	}


	public void setSpreadrate8(BigDecimal spreadrate8) {
		this.spreadrate8 = spreadrate8;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public BigDecimal getPrinamt8() {
		return prinamt8;
	}


	public void setPrinamt8(BigDecimal prinamt8) {
		this.prinamt8 = prinamt8;
	}


	public BigDecimal getPrinpayamt8() {
		return prinpayamt8;
	}


	public void setPrinpayamt8(BigDecimal prinpayamt8) {
		this.prinpayamt8 = prinpayamt8;
	}


	public BigDecimal getPutprice8() {
		return putprice8;
	}


	public void setPutprice8(BigDecimal putprice8) {
		this.putprice8 = putprice8;
	}


	public BigDecimal getSteprate8() {
		return steprate8;
	}


	public void setSteprate8(BigDecimal steprate8) {
		this.steprate8 = steprate8;
	}


	public Date getExerdate() {
		return exerdate;
	}


	public void setExerdate(Date exerdate) {
		this.exerdate = exerdate;
	}


	public Date getPublishdate() {
		return publishdate;
	}


	public void setPublishdate(Date publishdate) {
		this.publishdate = publishdate;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public BigDecimal getNotprinamt8() {
		return notprinamt8;
	}


	public void setNotprinamt8(BigDecimal notprinamt8) {
		this.notprinamt8 = notprinamt8;
	}


	public BigDecimal getNotprinpayamt8() {
		return notprinpayamt8;
	}


	public void setNotprinpayamt8(BigDecimal notprinpayamt8) {
		this.notprinpayamt8 = notprinpayamt8;
	}


	public BigDecimal getNotintpayamt8() {
		return notintpayamt8;
	}


	public void setNotintpayamt8(BigDecimal notintpayamt8) {
		this.notintpayamt8 = notintpayamt8;
	}


	public Long getUpdatecounter() {
		return updatecounter;
	}


	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}


	public BigDecimal getGroupid() {
		return groupid;
	}


	public void setGroupid(BigDecimal groupid) {
		this.groupid = groupid;
	}


	public BigDecimal getFormulaid() {
		return formulaid;
	}


	public void setFormulaid(BigDecimal formulaid) {
		this.formulaid = formulaid;
	}


	public BigDecimal getFixingoverride() {
		return fixingoverride;
	}


	public void setFixingoverride(BigDecimal fixingoverride) {
		this.fixingoverride = fixingoverride;
	}


	public Date getRatesetdate() {
		return ratesetdate;
	}


	public void setRatesetdate(Date ratesetdate) {
		this.ratesetdate = ratesetdate;
	}


	public String getIntsettccy() {
		return intsettccy;
	}


	public void setIntsettccy(String intsettccy) {
		this.intsettccy = intsettccy;
	}


	public String getCapfloorind() {
		return capfloorind;
	}


	public void setCapfloorind(String capfloorind) {
		this.capfloorind = capfloorind;
	}


	public String getRaterefixind() {
		return raterefixind;
	}


	public void setRaterefixind(String raterefixind) {
		this.raterefixind = raterefixind;
	}


	private static final long serialVersionUID = 1L;
}