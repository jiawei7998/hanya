package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Rhis;
import com.singlee.financial.wks.mapper.opics.RhisMapper;
import com.singlee.financial.wks.service.opics.RhisService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
@Service
public class RhisServiceImpl implements RhisService{

    @Resource
    private RhisMapper rhisMapper;

    @Override
    public int deleteByPrimaryKey(String br,String ratecode,Date effdate) {
        return rhisMapper.deleteByPrimaryKey(br,ratecode,effdate);
    }

    @Override
    public int insert(Rhis record) {
        return rhisMapper.insert(record);
    }

    @Override
    public int insertSelective(Rhis record) {
        return rhisMapper.insertSelective(record);
    }

    @Override
    public Rhis selectByPrimaryKey(String br,String ratecode,Date effdate) {
        return rhisMapper.selectByPrimaryKey(br,ratecode,effdate);
    }

    @Override
    public int updateByPrimaryKeySelective(Rhis record) {
        return rhisMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Rhis record) {
        return rhisMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<Rhis> selectAllByRate(Rhis rhis) {
        return rhisMapper.selectAllByRate(rhis);
    }
}
