package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Dldt;

public interface DldtService {

    int deleteByPrimaryKey(String br, String dealno, String seq);

    int insert(Dldt record);

    int insertSelective(Dldt record);

    Dldt selectByPrimaryKey(String br, String dealno, String seq);

    Dldt selectByTradeNo(String br, String dealtext);

    int updateByPrimaryKeySelective(Dldt record);

    int updateByPrimaryKey(Dldt record);

}
