package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Otdt implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String product;

    private String prodtype;

    private BigDecimal avgrate8;

    private BigDecimal avgstke8;

    private Date avgstkeedate;

    private Date avgstkesdate;

    private BigDecimal ccyprembamt;

    private BigDecimal binary;

    private String brok;

    private String ccy;

    private String ccycp;

    private BigDecimal ccyprem;

    private BigDecimal ccypremamt;

    private BigDecimal ccyrevalamt;

    private BigDecimal ccypremrate8;

    private BigDecimal ccyprin;

    private BigDecimal contsize;

    private String cost;

    private String ctrccy;

    private String ctrcp;

    private BigDecimal ctrprem;

    private BigDecimal ctrpremamt;

    private BigDecimal ctrrevalamt;

    private BigDecimal ctrpremrate8;

    private BigDecimal ctrprin;

    private String cno;

    private String cutoff;

    private Date dealdate;

    private Date dealvdate;

    private String dealtext;

    private String delivery;

    private BigDecimal delta8;

    private BigDecimal dkitrig18;

    private BigDecimal dkitrig28;

    private BigDecimal dkotrig18;

    private BigDecimal dkotrig28;

    private Date exerdate;

    private String exerind;

    private String exotic;

    private Date expdate;

    private BigDecimal feeamt;

    private String feeccy;

    private Date inputdate;

    private String inputtime;

    private String ioper;

    private BigDecimal kitrig8;

    private BigDecimal kotrig8;

    private String linkdealno;

    private String linkproduct;

    private String linkprodtype;

    private Date lstmntdate;

    private String otctype;

    private Date premauthdte;

    private String premauthind;

    private String premauthoper;

    private String premsmeans;

    private String premsacct;

    private String premsoper;

    private String plmethod;

    private String port;

    private String ps;

    private BigDecimal qty;

    private BigDecimal quanto8;

    private Date revdate;

    private String revreason;

    private String revtext;

    private String secid;

    private Date settledte;

    private String shperopt;

    private String siind;

    private BigDecimal strike8;

    private String style;

    private String symbol;

    private String tenor;

    private String terms;

    private String trad;

    private String unit;

    private Date verdate;

    private String verind;

    private BigDecimal volatility8;

    private String voper;

    private String dealsrce;

    private String fincntr1;

    private String fincntr2;

    private String undtenor;

    private String undacctngtype;

    private Date startdate;

    private Date enddate;

    private BigDecimal spotrate8;

    private BigDecimal ccybrate8;

    private String ccybterms;

    private BigDecimal ctrbrate8;

    private String ctrbterms;

    private BigDecimal ccyintrate8;

    private BigDecimal ctrintrate8;

    private String premccy;

    private BigDecimal premamt;

    private BigDecimal premrate8;

    private String revind;

    private String binaryccy;

    private String undseq;

    private String interfacenum;

    private String context;

    private String context1;

    private String context2;

    private String location;

    private String frequency;

    private String source;

    private String revalsource;

    private String phonci;

    private Date phonecdate;

    private String phonetext;

    private String text1;

    private String masteragreement;

    private String text3;

    private BigDecimal rate18;

    private BigDecimal rate28;

    private BigDecimal rate38;

    private BigDecimal amt1;

    private BigDecimal amt2;

    private Date date1;

    private Date date2;

    private BigDecimal salescredamt;

    private String salescredccy;

    private String trigind;

    private Date trigdate;

    private String trigoper;

    private BigDecimal rebateamt;

    private BigDecimal closingrate8;

    private String feeno;

    private String feeseq;

    private String cashsettleind;

    private String exeroper;

    private String underlyingind;

    private Date amenddate;

    private String psdealno;

    private String psseq;

    private String psoper;

    private Date psdate;

    private String psind;

    private Date revpsdate;

    private BigDecimal revqty;

    private BigDecimal revprem;

    private BigDecimal revprincipal;

    private String marketvalind;

    private BigDecimal undprice8;

    private BigDecimal capprice8;

    private BigDecimal discprice8;

    private BigDecimal particper8;

    private BigDecimal linkprinamt;

    private BigDecimal exponent;

    private BigDecimal discper8;

    private Long updatecounter;

    private Date trigstartdate;

    private Date trigenddate;

    private BigDecimal origprincipal;

    private BigDecimal origqty;

    private String reporatecode;

    private String avgmethod;

    private String mathsource;

    private String model;

    private String swiftmatchind;

    private Date custcdate;

    private BigDecimal origpremamt;

    private BigDecimal origpremrate8;

    private Date brprcindte;

    private String salescreditind;

    private String allocateind;

    private Date allocatedate;

    private String assignind;

    private String assigncno;

    private String custstatus;

    private String assigndealno;

    private Date assigndate;

    private String assigndealnoseq;

    private String assigncuststatus;

    private BigDecimal schedid;

    private BigDecimal basketid;

    private BigDecimal wtaxamt;

    private BigDecimal taxnetpremamt;

    private String taxstatus;

    private String calcagent;

    private String corptrad;

    private String corpport;

    private String corpcost;

    private BigDecimal corpspread8;

    private BigDecimal corpspreadamt;

    private String ccyyieldcurve;

    private String ctryieldcurve;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public BigDecimal getAvgrate8() {
		return avgrate8;
	}


	public void setAvgrate8(BigDecimal avgrate8) {
		this.avgrate8 = avgrate8;
	}


	public BigDecimal getAvgstke8() {
		return avgstke8;
	}


	public void setAvgstke8(BigDecimal avgstke8) {
		this.avgstke8 = avgstke8;
	}


	public Date getAvgstkeedate() {
		return avgstkeedate;
	}


	public void setAvgstkeedate(Date avgstkeedate) {
		this.avgstkeedate = avgstkeedate;
	}


	public Date getAvgstkesdate() {
		return avgstkesdate;
	}


	public void setAvgstkesdate(Date avgstkesdate) {
		this.avgstkesdate = avgstkesdate;
	}


	public BigDecimal getCcyprembamt() {
		return ccyprembamt;
	}


	public void setCcyprembamt(BigDecimal ccyprembamt) {
		this.ccyprembamt = ccyprembamt;
	}


	public BigDecimal getBinary() {
		return binary;
	}


	public void setBinary(BigDecimal binary) {
		this.binary = binary;
	}


	public String getBrok() {
		return brok;
	}


	public void setBrok(String brok) {
		this.brok = brok;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getCcycp() {
		return ccycp;
	}


	public void setCcycp(String ccycp) {
		this.ccycp = ccycp;
	}


	public BigDecimal getCcyprem() {
		return ccyprem;
	}


	public void setCcyprem(BigDecimal ccyprem) {
		this.ccyprem = ccyprem;
	}


	public BigDecimal getCcypremamt() {
		return ccypremamt;
	}


	public void setCcypremamt(BigDecimal ccypremamt) {
		this.ccypremamt = ccypremamt;
	}


	public BigDecimal getCcyrevalamt() {
		return ccyrevalamt;
	}


	public void setCcyrevalamt(BigDecimal ccyrevalamt) {
		this.ccyrevalamt = ccyrevalamt;
	}


	public BigDecimal getCcypremrate8() {
		return ccypremrate8;
	}


	public void setCcypremrate8(BigDecimal ccypremrate8) {
		this.ccypremrate8 = ccypremrate8;
	}


	public BigDecimal getCcyprin() {
		return ccyprin;
	}


	public void setCcyprin(BigDecimal ccyprin) {
		this.ccyprin = ccyprin;
	}


	public BigDecimal getContsize() {
		return contsize;
	}


	public void setContsize(BigDecimal contsize) {
		this.contsize = contsize;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getCtrccy() {
		return ctrccy;
	}


	public void setCtrccy(String ctrccy) {
		this.ctrccy = ctrccy;
	}


	public String getCtrcp() {
		return ctrcp;
	}


	public void setCtrcp(String ctrcp) {
		this.ctrcp = ctrcp;
	}


	public BigDecimal getCtrprem() {
		return ctrprem;
	}


	public void setCtrprem(BigDecimal ctrprem) {
		this.ctrprem = ctrprem;
	}


	public BigDecimal getCtrpremamt() {
		return ctrpremamt;
	}


	public void setCtrpremamt(BigDecimal ctrpremamt) {
		this.ctrpremamt = ctrpremamt;
	}


	public BigDecimal getCtrrevalamt() {
		return ctrrevalamt;
	}


	public void setCtrrevalamt(BigDecimal ctrrevalamt) {
		this.ctrrevalamt = ctrrevalamt;
	}


	public BigDecimal getCtrpremrate8() {
		return ctrpremrate8;
	}


	public void setCtrpremrate8(BigDecimal ctrpremrate8) {
		this.ctrpremrate8 = ctrpremrate8;
	}


	public BigDecimal getCtrprin() {
		return ctrprin;
	}


	public void setCtrprin(BigDecimal ctrprin) {
		this.ctrprin = ctrprin;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getCutoff() {
		return cutoff;
	}


	public void setCutoff(String cutoff) {
		this.cutoff = cutoff;
	}


	public Date getDealdate() {
		return dealdate;
	}


	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}


	public Date getDealvdate() {
		return dealvdate;
	}


	public void setDealvdate(Date dealvdate) {
		this.dealvdate = dealvdate;
	}


	public String getDealtext() {
		return dealtext;
	}


	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}


	public String getDelivery() {
		return delivery;
	}


	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}


	public BigDecimal getDelta8() {
		return delta8;
	}


	public void setDelta8(BigDecimal delta8) {
		this.delta8 = delta8;
	}


	public BigDecimal getDkitrig18() {
		return dkitrig18;
	}


	public void setDkitrig18(BigDecimal dkitrig18) {
		this.dkitrig18 = dkitrig18;
	}


	public BigDecimal getDkitrig28() {
		return dkitrig28;
	}


	public void setDkitrig28(BigDecimal dkitrig28) {
		this.dkitrig28 = dkitrig28;
	}


	public BigDecimal getDkotrig18() {
		return dkotrig18;
	}


	public void setDkotrig18(BigDecimal dkotrig18) {
		this.dkotrig18 = dkotrig18;
	}


	public BigDecimal getDkotrig28() {
		return dkotrig28;
	}


	public void setDkotrig28(BigDecimal dkotrig28) {
		this.dkotrig28 = dkotrig28;
	}


	public Date getExerdate() {
		return exerdate;
	}


	public void setExerdate(Date exerdate) {
		this.exerdate = exerdate;
	}


	public String getExerind() {
		return exerind;
	}


	public void setExerind(String exerind) {
		this.exerind = exerind;
	}


	public String getExotic() {
		return exotic;
	}


	public void setExotic(String exotic) {
		this.exotic = exotic;
	}


	public Date getExpdate() {
		return expdate;
	}


	public void setExpdate(Date expdate) {
		this.expdate = expdate;
	}


	public BigDecimal getFeeamt() {
		return feeamt;
	}


	public void setFeeamt(BigDecimal feeamt) {
		this.feeamt = feeamt;
	}


	public String getFeeccy() {
		return feeccy;
	}


	public void setFeeccy(String feeccy) {
		this.feeccy = feeccy;
	}


	public Date getInputdate() {
		return inputdate;
	}


	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}


	public String getInputtime() {
		return inputtime;
	}


	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}


	public String getIoper() {
		return ioper;
	}


	public void setIoper(String ioper) {
		this.ioper = ioper;
	}


	public BigDecimal getKitrig8() {
		return kitrig8;
	}


	public void setKitrig8(BigDecimal kitrig8) {
		this.kitrig8 = kitrig8;
	}


	public BigDecimal getKotrig8() {
		return kotrig8;
	}


	public void setKotrig8(BigDecimal kotrig8) {
		this.kotrig8 = kotrig8;
	}


	public String getLinkdealno() {
		return linkdealno;
	}


	public void setLinkdealno(String linkdealno) {
		this.linkdealno = linkdealno;
	}


	public String getLinkproduct() {
		return linkproduct;
	}


	public void setLinkproduct(String linkproduct) {
		this.linkproduct = linkproduct;
	}


	public String getLinkprodtype() {
		return linkprodtype;
	}


	public void setLinkprodtype(String linkprodtype) {
		this.linkprodtype = linkprodtype;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public String getOtctype() {
		return otctype;
	}


	public void setOtctype(String otctype) {
		this.otctype = otctype;
	}


	public Date getPremauthdte() {
		return premauthdte;
	}


	public void setPremauthdte(Date premauthdte) {
		this.premauthdte = premauthdte;
	}


	public String getPremauthind() {
		return premauthind;
	}


	public void setPremauthind(String premauthind) {
		this.premauthind = premauthind;
	}


	public String getPremauthoper() {
		return premauthoper;
	}


	public void setPremauthoper(String premauthoper) {
		this.premauthoper = premauthoper;
	}


	public String getPremsmeans() {
		return premsmeans;
	}


	public void setPremsmeans(String premsmeans) {
		this.premsmeans = premsmeans;
	}


	public String getPremsacct() {
		return premsacct;
	}


	public void setPremsacct(String premsacct) {
		this.premsacct = premsacct;
	}


	public String getPremsoper() {
		return premsoper;
	}


	public void setPremsoper(String premsoper) {
		this.premsoper = premsoper;
	}


	public String getPlmethod() {
		return plmethod;
	}


	public void setPlmethod(String plmethod) {
		this.plmethod = plmethod;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getPs() {
		return ps;
	}


	public void setPs(String ps) {
		this.ps = ps;
	}


	public BigDecimal getQty() {
		return qty;
	}


	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}


	public BigDecimal getQuanto8() {
		return quanto8;
	}


	public void setQuanto8(BigDecimal quanto8) {
		this.quanto8 = quanto8;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public String getRevreason() {
		return revreason;
	}


	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}


	public String getRevtext() {
		return revtext;
	}


	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}


	public String getSecid() {
		return secid;
	}


	public void setSecid(String secid) {
		this.secid = secid;
	}


	public Date getSettledte() {
		return settledte;
	}


	public void setSettledte(Date settledte) {
		this.settledte = settledte;
	}


	public String getShperopt() {
		return shperopt;
	}


	public void setShperopt(String shperopt) {
		this.shperopt = shperopt;
	}


	public String getSiind() {
		return siind;
	}


	public void setSiind(String siind) {
		this.siind = siind;
	}


	public BigDecimal getStrike8() {
		return strike8;
	}


	public void setStrike8(BigDecimal strike8) {
		this.strike8 = strike8;
	}


	public String getStyle() {
		return style;
	}


	public void setStyle(String style) {
		this.style = style;
	}


	public String getSymbol() {
		return symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getTenor() {
		return tenor;
	}


	public void setTenor(String tenor) {
		this.tenor = tenor;
	}


	public String getTerms() {
		return terms;
	}


	public void setTerms(String terms) {
		this.terms = terms;
	}


	public String getTrad() {
		return trad;
	}


	public void setTrad(String trad) {
		this.trad = trad;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public String getVerind() {
		return verind;
	}


	public void setVerind(String verind) {
		this.verind = verind;
	}


	public BigDecimal getVolatility8() {
		return volatility8;
	}


	public void setVolatility8(BigDecimal volatility8) {
		this.volatility8 = volatility8;
	}


	public String getVoper() {
		return voper;
	}


	public void setVoper(String voper) {
		this.voper = voper;
	}


	public String getDealsrce() {
		return dealsrce;
	}


	public void setDealsrce(String dealsrce) {
		this.dealsrce = dealsrce;
	}


	public String getFincntr1() {
		return fincntr1;
	}


	public void setFincntr1(String fincntr1) {
		this.fincntr1 = fincntr1;
	}


	public String getFincntr2() {
		return fincntr2;
	}


	public void setFincntr2(String fincntr2) {
		this.fincntr2 = fincntr2;
	}


	public String getUndtenor() {
		return undtenor;
	}


	public void setUndtenor(String undtenor) {
		this.undtenor = undtenor;
	}


	public String getUndacctngtype() {
		return undacctngtype;
	}


	public void setUndacctngtype(String undacctngtype) {
		this.undacctngtype = undacctngtype;
	}


	public Date getStartdate() {
		return startdate;
	}


	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}


	public Date getEnddate() {
		return enddate;
	}


	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}


	public BigDecimal getSpotrate8() {
		return spotrate8;
	}


	public void setSpotrate8(BigDecimal spotrate8) {
		this.spotrate8 = spotrate8;
	}


	public BigDecimal getCcybrate8() {
		return ccybrate8;
	}


	public void setCcybrate8(BigDecimal ccybrate8) {
		this.ccybrate8 = ccybrate8;
	}


	public String getCcybterms() {
		return ccybterms;
	}


	public void setCcybterms(String ccybterms) {
		this.ccybterms = ccybterms;
	}


	public BigDecimal getCtrbrate8() {
		return ctrbrate8;
	}


	public void setCtrbrate8(BigDecimal ctrbrate8) {
		this.ctrbrate8 = ctrbrate8;
	}


	public String getCtrbterms() {
		return ctrbterms;
	}


	public void setCtrbterms(String ctrbterms) {
		this.ctrbterms = ctrbterms;
	}


	public BigDecimal getCcyintrate8() {
		return ccyintrate8;
	}


	public void setCcyintrate8(BigDecimal ccyintrate8) {
		this.ccyintrate8 = ccyintrate8;
	}


	public BigDecimal getCtrintrate8() {
		return ctrintrate8;
	}


	public void setCtrintrate8(BigDecimal ctrintrate8) {
		this.ctrintrate8 = ctrintrate8;
	}


	public String getPremccy() {
		return premccy;
	}


	public void setPremccy(String premccy) {
		this.premccy = premccy;
	}


	public BigDecimal getPremamt() {
		return premamt;
	}


	public void setPremamt(BigDecimal premamt) {
		this.premamt = premamt;
	}


	public BigDecimal getPremrate8() {
		return premrate8;
	}


	public void setPremrate8(BigDecimal premrate8) {
		this.premrate8 = premrate8;
	}


	public String getRevind() {
		return revind;
	}


	public void setRevind(String revind) {
		this.revind = revind;
	}


	public String getBinaryccy() {
		return binaryccy;
	}


	public void setBinaryccy(String binaryccy) {
		this.binaryccy = binaryccy;
	}


	public String getUndseq() {
		return undseq;
	}


	public void setUndseq(String undseq) {
		this.undseq = undseq;
	}


	public String getInterfacenum() {
		return interfacenum;
	}


	public void setInterfacenum(String interfacenum) {
		this.interfacenum = interfacenum;
	}


	public String getContext() {
		return context;
	}


	public void setContext(String context) {
		this.context = context;
	}


	public String getContext1() {
		return context1;
	}


	public void setContext1(String context1) {
		this.context1 = context1;
	}


	public String getContext2() {
		return context2;
	}


	public void setContext2(String context2) {
		this.context2 = context2;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getFrequency() {
		return frequency;
	}


	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getRevalsource() {
		return revalsource;
	}


	public void setRevalsource(String revalsource) {
		this.revalsource = revalsource;
	}


	public String getPhonci() {
		return phonci;
	}


	public void setPhonci(String phonci) {
		this.phonci = phonci;
	}


	public Date getPhonecdate() {
		return phonecdate;
	}


	public void setPhonecdate(Date phonecdate) {
		this.phonecdate = phonecdate;
	}


	public String getPhonetext() {
		return phonetext;
	}


	public void setPhonetext(String phonetext) {
		this.phonetext = phonetext;
	}


	public String getText1() {
		return text1;
	}


	public void setText1(String text1) {
		this.text1 = text1;
	}


	public String getMasteragreement() {
		return masteragreement;
	}


	public void setMasteragreement(String masteragreement) {
		this.masteragreement = masteragreement;
	}


	public String getText3() {
		return text3;
	}


	public void setText3(String text3) {
		this.text3 = text3;
	}


	public BigDecimal getRate18() {
		return rate18;
	}


	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}


	public BigDecimal getRate28() {
		return rate28;
	}


	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}


	public BigDecimal getRate38() {
		return rate38;
	}


	public void setRate38(BigDecimal rate38) {
		this.rate38 = rate38;
	}


	public BigDecimal getAmt1() {
		return amt1;
	}


	public void setAmt1(BigDecimal amt1) {
		this.amt1 = amt1;
	}


	public BigDecimal getAmt2() {
		return amt2;
	}


	public void setAmt2(BigDecimal amt2) {
		this.amt2 = amt2;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public BigDecimal getSalescredamt() {
		return salescredamt;
	}


	public void setSalescredamt(BigDecimal salescredamt) {
		this.salescredamt = salescredamt;
	}


	public String getSalescredccy() {
		return salescredccy;
	}


	public void setSalescredccy(String salescredccy) {
		this.salescredccy = salescredccy;
	}


	public String getTrigind() {
		return trigind;
	}


	public void setTrigind(String trigind) {
		this.trigind = trigind;
	}


	public Date getTrigdate() {
		return trigdate;
	}


	public void setTrigdate(Date trigdate) {
		this.trigdate = trigdate;
	}


	public String getTrigoper() {
		return trigoper;
	}


	public void setTrigoper(String trigoper) {
		this.trigoper = trigoper;
	}


	public BigDecimal getRebateamt() {
		return rebateamt;
	}


	public void setRebateamt(BigDecimal rebateamt) {
		this.rebateamt = rebateamt;
	}


	public BigDecimal getClosingrate8() {
		return closingrate8;
	}


	public void setClosingrate8(BigDecimal closingrate8) {
		this.closingrate8 = closingrate8;
	}


	public String getFeeno() {
		return feeno;
	}


	public void setFeeno(String feeno) {
		this.feeno = feeno;
	}


	public String getFeeseq() {
		return feeseq;
	}


	public void setFeeseq(String feeseq) {
		this.feeseq = feeseq;
	}


	public String getCashsettleind() {
		return cashsettleind;
	}


	public void setCashsettleind(String cashsettleind) {
		this.cashsettleind = cashsettleind;
	}


	public String getExeroper() {
		return exeroper;
	}


	public void setExeroper(String exeroper) {
		this.exeroper = exeroper;
	}


	public String getUnderlyingind() {
		return underlyingind;
	}


	public void setUnderlyingind(String underlyingind) {
		this.underlyingind = underlyingind;
	}


	public Date getAmenddate() {
		return amenddate;
	}


	public void setAmenddate(Date amenddate) {
		this.amenddate = amenddate;
	}


	public String getPsdealno() {
		return psdealno;
	}


	public void setPsdealno(String psdealno) {
		this.psdealno = psdealno;
	}


	public String getPsseq() {
		return psseq;
	}


	public void setPsseq(String psseq) {
		this.psseq = psseq;
	}


	public String getPsoper() {
		return psoper;
	}


	public void setPsoper(String psoper) {
		this.psoper = psoper;
	}


	public Date getPsdate() {
		return psdate;
	}


	public void setPsdate(Date psdate) {
		this.psdate = psdate;
	}


	public String getPsind() {
		return psind;
	}


	public void setPsind(String psind) {
		this.psind = psind;
	}


	public Date getRevpsdate() {
		return revpsdate;
	}


	public void setRevpsdate(Date revpsdate) {
		this.revpsdate = revpsdate;
	}


	public BigDecimal getRevqty() {
		return revqty;
	}


	public void setRevqty(BigDecimal revqty) {
		this.revqty = revqty;
	}


	public BigDecimal getRevprem() {
		return revprem;
	}


	public void setRevprem(BigDecimal revprem) {
		this.revprem = revprem;
	}


	public BigDecimal getRevprincipal() {
		return revprincipal;
	}


	public void setRevprincipal(BigDecimal revprincipal) {
		this.revprincipal = revprincipal;
	}


	public String getMarketvalind() {
		return marketvalind;
	}


	public void setMarketvalind(String marketvalind) {
		this.marketvalind = marketvalind;
	}


	public BigDecimal getUndprice8() {
		return undprice8;
	}


	public void setUndprice8(BigDecimal undprice8) {
		this.undprice8 = undprice8;
	}


	public BigDecimal getCapprice8() {
		return capprice8;
	}


	public void setCapprice8(BigDecimal capprice8) {
		this.capprice8 = capprice8;
	}


	public BigDecimal getDiscprice8() {
		return discprice8;
	}


	public void setDiscprice8(BigDecimal discprice8) {
		this.discprice8 = discprice8;
	}


	public BigDecimal getParticper8() {
		return particper8;
	}


	public void setParticper8(BigDecimal particper8) {
		this.particper8 = particper8;
	}


	public BigDecimal getLinkprinamt() {
		return linkprinamt;
	}


	public void setLinkprinamt(BigDecimal linkprinamt) {
		this.linkprinamt = linkprinamt;
	}


	public BigDecimal getExponent() {
		return exponent;
	}


	public void setExponent(BigDecimal exponent) {
		this.exponent = exponent;
	}


	public BigDecimal getDiscper8() {
		return discper8;
	}


	public void setDiscper8(BigDecimal discper8) {
		this.discper8 = discper8;
	}


	public Long getUpdatecounter() {
		return updatecounter;
	}


	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}


	public Date getTrigstartdate() {
		return trigstartdate;
	}


	public void setTrigstartdate(Date trigstartdate) {
		this.trigstartdate = trigstartdate;
	}


	public Date getTrigenddate() {
		return trigenddate;
	}


	public void setTrigenddate(Date trigenddate) {
		this.trigenddate = trigenddate;
	}


	public BigDecimal getOrigprincipal() {
		return origprincipal;
	}


	public void setOrigprincipal(BigDecimal origprincipal) {
		this.origprincipal = origprincipal;
	}


	public BigDecimal getOrigqty() {
		return origqty;
	}


	public void setOrigqty(BigDecimal origqty) {
		this.origqty = origqty;
	}


	public String getReporatecode() {
		return reporatecode;
	}


	public void setReporatecode(String reporatecode) {
		this.reporatecode = reporatecode;
	}


	public String getAvgmethod() {
		return avgmethod;
	}


	public void setAvgmethod(String avgmethod) {
		this.avgmethod = avgmethod;
	}


	public String getMathsource() {
		return mathsource;
	}


	public void setMathsource(String mathsource) {
		this.mathsource = mathsource;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public String getSwiftmatchind() {
		return swiftmatchind;
	}


	public void setSwiftmatchind(String swiftmatchind) {
		this.swiftmatchind = swiftmatchind;
	}


	public Date getCustcdate() {
		return custcdate;
	}


	public void setCustcdate(Date custcdate) {
		this.custcdate = custcdate;
	}


	public BigDecimal getOrigpremamt() {
		return origpremamt;
	}


	public void setOrigpremamt(BigDecimal origpremamt) {
		this.origpremamt = origpremamt;
	}


	public BigDecimal getOrigpremrate8() {
		return origpremrate8;
	}


	public void setOrigpremrate8(BigDecimal origpremrate8) {
		this.origpremrate8 = origpremrate8;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public String getSalescreditind() {
		return salescreditind;
	}


	public void setSalescreditind(String salescreditind) {
		this.salescreditind = salescreditind;
	}


	public String getAllocateind() {
		return allocateind;
	}


	public void setAllocateind(String allocateind) {
		this.allocateind = allocateind;
	}


	public Date getAllocatedate() {
		return allocatedate;
	}


	public void setAllocatedate(Date allocatedate) {
		this.allocatedate = allocatedate;
	}


	public String getAssignind() {
		return assignind;
	}


	public void setAssignind(String assignind) {
		this.assignind = assignind;
	}


	public String getAssigncno() {
		return assigncno;
	}


	public void setAssigncno(String assigncno) {
		this.assigncno = assigncno;
	}


	public String getCuststatus() {
		return custstatus;
	}


	public void setCuststatus(String custstatus) {
		this.custstatus = custstatus;
	}


	public String getAssigndealno() {
		return assigndealno;
	}


	public void setAssigndealno(String assigndealno) {
		this.assigndealno = assigndealno;
	}


	public Date getAssigndate() {
		return assigndate;
	}


	public void setAssigndate(Date assigndate) {
		this.assigndate = assigndate;
	}


	public String getAssigndealnoseq() {
		return assigndealnoseq;
	}


	public void setAssigndealnoseq(String assigndealnoseq) {
		this.assigndealnoseq = assigndealnoseq;
	}


	public String getAssigncuststatus() {
		return assigncuststatus;
	}


	public void setAssigncuststatus(String assigncuststatus) {
		this.assigncuststatus = assigncuststatus;
	}


	public BigDecimal getSchedid() {
		return schedid;
	}


	public void setSchedid(BigDecimal schedid) {
		this.schedid = schedid;
	}


	public BigDecimal getBasketid() {
		return basketid;
	}


	public void setBasketid(BigDecimal basketid) {
		this.basketid = basketid;
	}


	public BigDecimal getWtaxamt() {
		return wtaxamt;
	}


	public void setWtaxamt(BigDecimal wtaxamt) {
		this.wtaxamt = wtaxamt;
	}


	public BigDecimal getTaxnetpremamt() {
		return taxnetpremamt;
	}


	public void setTaxnetpremamt(BigDecimal taxnetpremamt) {
		this.taxnetpremamt = taxnetpremamt;
	}


	public String getTaxstatus() {
		return taxstatus;
	}


	public void setTaxstatus(String taxstatus) {
		this.taxstatus = taxstatus;
	}


	public String getCalcagent() {
		return calcagent;
	}


	public void setCalcagent(String calcagent) {
		this.calcagent = calcagent;
	}


	public String getCorptrad() {
		return corptrad;
	}


	public void setCorptrad(String corptrad) {
		this.corptrad = corptrad;
	}


	public String getCorpport() {
		return corpport;
	}


	public void setCorpport(String corpport) {
		this.corpport = corpport;
	}


	public String getCorpcost() {
		return corpcost;
	}


	public void setCorpcost(String corpcost) {
		this.corpcost = corpcost;
	}


	public BigDecimal getCorpspread8() {
		return corpspread8;
	}


	public void setCorpspread8(BigDecimal corpspread8) {
		this.corpspread8 = corpspread8;
	}


	public BigDecimal getCorpspreadamt() {
		return corpspreadamt;
	}


	public void setCorpspreadamt(BigDecimal corpspreadamt) {
		this.corpspreadamt = corpspreadamt;
	}


	public String getCcyyieldcurve() {
		return ccyyieldcurve;
	}


	public void setCcyyieldcurve(String ccyyieldcurve) {
		this.ccyyieldcurve = ccyyieldcurve;
	}


	public String getCtryieldcurve() {
		return ctryieldcurve;
	}


	public void setCtryieldcurve(String ctryieldcurve) {
		this.ctryieldcurve = ctryieldcurve;
	}


	private static final long serialVersionUID = 1L;
}