package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Ycrs;

import java.util.List;

public interface YcrsService{


    int deleteByPrimaryKey(String br,String ccy,String yieldcurve,String contribrate,String mtystart,String mtyend,String delvcode);

    int insert(Ycrs record);

    int insertSelective(Ycrs record);

    Ycrs selectByPrimaryKey(String br,String ccy,String yieldcurve,String contribrate,String mtystart,String mtyend,String delvcode);

    int updateByPrimaryKeySelective(Ycrs record);

    int updateByPrimaryKey(Ycrs record);

    List<Ycrs> selectByAll(Ycrs record);
}
