package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Cost;
import com.singlee.financial.wks.mapper.opics.CostMapper;
import com.singlee.financial.wks.service.opics.CostService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CostServiceImpl implements CostService {

    @Resource
    private CostMapper costMapper;

    @Override
    public int deleteByPrimaryKey(String costcent) {
        return costMapper.deleteByPrimaryKey(costcent);
    }

    @Override
    public int insert(Cost record) {
        return costMapper.insert(record);
    }

    @Override
    public int insertSelective(Cost record) {
        return costMapper.insertSelective(record);
    }

    @Override
    public Cost selectByPrimaryKey(String costcent) {
        return costMapper.selectByPrimaryKey(costcent);
    }

    @Override
    public int updateByPrimaryKeySelective(Cost record) {
        return costMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Cost record) {
        return costMapper.updateByPrimaryKey(record);
    }

}
