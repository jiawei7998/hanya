package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.RepoWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.RpdtWksBean;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.CustService;
import com.singlee.financial.wks.service.opics.McfpService;
import com.singlee.financial.wks.service.opics.SecmService;
import com.singlee.financial.wks.util.FinancialFieldMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 * 回购首期转换方法
 *
 * @author xuqq
 */
@Component
public class RepoEntry {

	@Autowired
	private FinancialFieldMap financialFieldMap;

	/**
	 * 交易信息检查
	 *
	 * @param cbt
	 * @param custService
	 */
	public void DataCheck(RepoWksBean repoWksBean, CustService custService, SecmService secmService) {
		if (null == repoWksBean) {
			JY.raise("输入对象为空,请检查!!!");
		}
		if (StringUtils.isEmpty(repoWksBean.getCno())) {
			JY.raise("交易对手为空,请检查!!!");
		}
		// 交易对手检查
		Cust cust = custService.selectByPrimaryKey(repoWksBean.getCno());
		if (null == cust) {
			JY.raise("交易对手错误或者不存在,请检查!!!");
		}
		// 债券检查
//        Secm secm = secmService.selectByPrimaryKey(repoWksBean.getBondCode());
//        if (null == secm) {
//            JY.raise("债券信息错误或者不存在,请检查!!!");
//        }
	}

	/**
	 * 交易静态数据
	 * 
	 * @param rprh
	 * @return
	 */
	public Rprh DataTrans(Rprh rprh) {
		rprh.setSeq(OpicsConstant.RPRH.SEQ_ZERO);// 流水号:序号
		rprh.setAssignind(OpicsConstant.RPRH.ASSIGNIND);// 分配标志:默认1
		rprh.setBrok(OpicsConstant.RPRH.ASSIGNIND);// 经纪公司比编号:D
		rprh.setBrokfeeamt(BigDecimal.ZERO);// 经纪人佣金:
		rprh.setChargeamt(BigDecimal.ZERO);// CHARGEAMT:
		rprh.setComauthind(OpicsConstant.RPRH.COMAUTHIND);// 授权标志:
		rprh.setFeeamt(BigDecimal.ZERO);// 费用:
		rprh.setFinfxrate8(new BigDecimal("1"));// 最终汇率:
		rprh.setFinterms(OpicsConstant.RPRH.FINTERMS);// FINTERMS:
		rprh.setInitfxrate8(new BigDecimal("1"));// 初始汇率:
		rprh.setInitterms(OpicsConstant.RPRH.INITTERMS);// INITTERMS:
		rprh.setMargcallpct8(BigDecimal.ZERO);// MARGCALLPCT8:
		rprh.setMarginpct(OpicsConstant.RPRH.MARGINPCT);// 保证金比率:
		rprh.setNottotlint(BigDecimal.ZERO);// 名义利息:
		rprh.setOpenrepoind(OpicsConstant.RPRH.OPENREPOIND);// 开通回购标志:
		rprh.setMqty(BigDecimal.ZERO);// MQTY:
		rprh.setRepoamt(BigDecimal.ZERO);// 回购数量:
		rprh.setSpreadrate8(BigDecimal.ZERO);// 利率差:
		rprh.setSubstitute(OpicsConstant.RPRH.SUBSTITUTE);// 置换标志:
		rprh.setSuppconfind(OpicsConstant.RPRH.SUPPCONFIND);// 复核确认:
		rprh.setTenor(OpicsConstant.RPRH.TENOR);// 期限:
		rprh.setVatamt(BigDecimal.ZERO);// 增值税金额:
		rprh.setVerind(OpicsConstant.RPRH.VERIND);// 复核标志:
		rprh.setWhtamt(BigDecimal.ZERO);// 计提税金额:
		rprh.setYield8(BigDecimal.ZERO);// YIELD8:
		rprh.setRpind(OpicsConstant.RPRH.RPIND);// RPIND:
		rprh.setTotprocdamt(BigDecimal.ZERO);// 总收入:
		rprh.setOrigprocdamt(BigDecimal.ZERO);// ORIGPROCDAMT:
		rprh.setCloserepoind(OpicsConstant.RPRH.CLOSEREPOIND);// 回购关闭标志:
		rprh.setEmpenamt(BigDecimal.ZERO);// 提前回购罚款:
		rprh.setIntcalcdind(OpicsConstant.RPRH.INTCALCDIND);// 利息计算标志:
		rprh.setCoupreinvest(OpicsConstant.RPRH.COUPREINVEST);// 息票再投资标志:
		rprh.setCoupreinvrate8(BigDecimal.ZERO);// 息票再投资利率:
		rprh.setInitbaserate8(new BigDecimal("1"));// 初始基准利率:
		rprh.setInitbaseterms(OpicsConstant.RPRH.INITBASETERMS);// 初始基本利率代码:
		rprh.setFinbaserate8(new BigDecimal("1"));// 最终基准利率:
		rprh.setFinbaseterms(OpicsConstant.RPRH.FINBASETERMS);// 最终基本利率代码:
		rprh.setInitindexrate8(new BigDecimal("1"));// 初始利率:
		rprh.setFinindexrate8(new BigDecimal("1"));// 最终利率:
		rprh.setOverridewxtaxind(OpicsConstant.RPRH.OVERRIDEWXTAXIND);// OVERRIDEWXTAXIND:
		rprh.setAmount1(BigDecimal.ZERO);// 备用5:
		rprh.setAmount2(BigDecimal.ZERO);// 备用6:
		rprh.setRate18(BigDecimal.ZERO);// 备用7:
		rprh.setRate28(BigDecimal.ZERO);// 备用8:
		rprh.setCommamt(BigDecimal.ZERO);// 佣金:
		rprh.setExchcommamt(BigDecimal.ZERO);// 兑换佣金:
		rprh.setStampdutyamt(BigDecimal.ZERO);// 印花税:
		rprh.setExchind(OpicsConstant.RPRH.EXCHIND);// 兑换标志:
		rprh.setSplitind(OpicsConstant.RPRH.SPLITIND);// :
		rprh.setCcpind(OpicsConstant.RPRH.CCPIND);// :
		rprh.setExtcompare(OpicsConstant.RPRH.EXTCOMPARE);// :
		rprh.setVdatenet(OpicsConstant.RPRH.VDATENET);// :
		rprh.setVdpaymentholdind(OpicsConstant.RPRH.VDPAYMENTHOLDIND);// :
		rprh.setMdpaymentholdind(OpicsConstant.RPRH.MDPAYMENTHOLDIND);// :
		rprh.setRollprinout(BigDecimal.ZERO);// :
		rprh.setRollprinin(BigDecimal.ZERO);// :
		rprh.setRolldiffamt(BigDecimal.ZERO);// :
		rprh.setAssignmethod(OpicsConstant.RPRH.ASSIGNMETHOD);// :
		rprh.setAutoassign(OpicsConstant.RPRH.AUTOASSIGN);// :
		rprh.setStatedreporate8(BigDecimal.ZERO);// :

		return rprh;
	}

	/**
	 * 交易数据
	 * 
	 * @param rprh
	 * @param repoWksBean
	 * @return
	 */
	public Rprh DataTrans(Rprh rprh, RepoWksBean repoWksBean) {
		financialFieldMap.convertFieldvalue(repoWksBean);
		List<RpdtWksBean> rpdtWksBeanList = repoWksBean.getRpdtWksBeanList();
		BigDecimal assgnqty = BigDecimal.ZERO;
		for (RpdtWksBean rpdtWksBean : rpdtWksBeanList) {
			assgnqty = assgnqty.add(rpdtWksBean.getQty());
		}
		String[] book = StringUtils.split(repoWksBean.getBook(), "_");
		String ccy = StringUtils.defaultIfEmpty(repoWksBean.getCcy(), "CNY");

		rprh.setPs(repoWksBean.getTrdType());// 买/卖标志:
		rprh.setBr(repoWksBean.getInstId());// 分行号:机构
		rprh.setAssgnqty(assgnqty);// 分配数量:手数
		rprh.setComauthdate(DateUtil.parse(repoWksBean.getDealDate()));// 授权日期:
		rprh.setComauthoper(OpicsConstant.RPRH.TRAD_NO);// 授权操作员:
		rprh.setComccysacct(repoWksBean.getCcysacct());// 清算账户:清算账号
		rprh.setComccysmeans(repoWksBean.getSettlementMethod());// 清算方式:
		rprh.setCost(book[2]);// 成本中心:
		rprh.setCcy(ccy);// 币种:回购币种
		rprh.setCno(repoWksBean.getCno());// 交易对手:CUST里面维护
		rprh.setDealdate(DateUtil.parse(repoWksBean.getDealDate()));// 交易日期:交易日期
		rprh.setDealtime(DateUtil.getCurrentTimeAsString());// 交易时间:
		rprh.setInputdate(DateUtil.parse(DateUtil.getCurrentDateAsString()));// 输入系统日期:
		rprh.setIoper(OpicsConstant.RPRH.TRAD_NO);// 输入操作员:
		rprh.setInputtime(DateUtil.getCurrentTimeAsString());// 输入系统时间:
		rprh.setBasis(getBasisTrans(repoWksBean.getBasis()));// BASIS:计息基础
		rprh.setIntcalctype(OpicsConstant.RPRH.INTCALCTYPE);// 利息计算类型:默认SIM/
		rprh.setMatauthdate(rprh.getBrprcindte());// 保证金清算授权日期:
		rprh.setMatauthind(OpicsConstant.RPRH.MATAUTHIND);// 保证金清算授权标志:
		rprh.setMatauthoper(OpicsConstant.RPRH.TRAD_NO);// MATAUTHOPER:
		rprh.setMatdate(DateUtil.parse(repoWksBean.getMdate()));// 到期日期:
		rprh.setMatccysacct(repoWksBean.getCcysacct());// 到期清算账户:
		rprh.setMatccysmeans(repoWksBean.getSettlementMethod());// 到期清算方式:
		rprh.setNotamt(getPlusNegateAmt(rprh.getPs(), repoWksBean.getCcyamt()));// 名义金额:
		rprh.setNotccy(ccy);// 名义币种:
		rprh.setProduct(book[0]);// 产品代码:
		rprh.setProdtype(book[1]);// 产品类型代码:
		rprh.setQuotqty(assgnqty);// 报价数量:
		rprh.setReporate8(repoWksBean.getRepoRate());// 回购利率:
		rprh.setTrad(StringUtils.defaultIfEmpty(repoWksBean.getTrad(), OpicsConstant.RPRH.TRAD_NO));// 交易员:
		rprh.setVdate(DateUtil.parse(repoWksBean.getVdate()));// 生息日期:
		rprh.setVerdate(rprh.getBrprcindte());// 复核日期:
		rprh.setVoper(OpicsConstant.RPRH.TRAD_NO);// 复核操作员:
		rprh.setSafekeepacct(repoWksBean.getSecsacct());// 托管帐户:
		rprh.setComprocdamt(getPlusNegateAmt(rprh.getPs(), repoWksBean.getCcyamt()));// 开始收入金额:
		rprh.setMatprocdamt(getPlusNegateAmt(rprh.getPs(), repoWksBean.getMccyamt()).negate());// 到期收入金额:
		rprh.setTotalint(rprh.getComprocdamt().add(rprh.getMatprocdamt()));// 利息总额:17.5024305555556
		rprh.setPort(repoWksBean.getDesk());// 投资组合:
		rprh.setCollateralcode(StringUtils.defaultIfEmpty(repoWksBean.getCollateralcode(), "ALL"));// 担保品编码:
		rprh.setCollunit(repoWksBean.getCollunit());// 担保品单位:
		rprh.setDelivtype(StringUtils.defaultIfEmpty(repoWksBean.getDelivtype(), "DVP"));//
		rprh.setUpdatecounter(OpicsConstant.RPRH.UPDATE_COUNTER);// 更新次数:
		rprh.setComprocbaseamt(repoWksBean.getCcyamt());// 初始收益基数:
		rprh.setMatprocbaseamt(repoWksBean.getMccyamt());// 最终收益基数:
		rprh.setConvintamt(repoWksBean.getConvintamt());// 兑换利息:
		rprh.setConvintbamt(repoWksBean.getConvintamt());// 兑换利息基数:
		rprh.setAssigndate(rprh.getBrprcindte());// 分配日期:
//		rprh.setText1(null);//备用1:

		rprh.setDealtext(repoWksBean.getTradeNo());// 交易摘要:
		return rprh;
	}

	public List<Rpdt> DataTrans(List<Rpdt> rpdtLst, Rprh rprh, RepoWksBean repoWksBean, SecmService secmService) {
		List<RpdtWksBean> rpdtWksBeanList = repoWksBean.getRpdtWksBeanList();
		BigDecimal hcmatpurchintamt = BigDecimal.ZERO;// 多个债券利息汇总金额
		for (RpdtWksBean rpdtWksBean : rpdtWksBeanList) {
			hcmatpurchintamt = hcmatpurchintamt.add(rpdtWksBean.getHcpurchintamt());
		}
		int i = 1;
		for (RpdtWksBean rpdtWksBean : rpdtWksBeanList) {
			Rpdt rpdt = DataTrans(new Rpdt());
			Secm secm = secmService.selectByPrimaryKey(rpdtWksBean.getSecid());
			String denom = secm.getDenom().trim();
			rpdt.setBr(rprh.getBr());// 01:分行号
			rpdt.setDealno(rprh.getDealno());// 2030903 :交易码
			rpdt.setAssignseq(String.format("%03d", i));// 002:分配流水号
			rpdt.setBrprcindte(rprh.getBrprcindte());// 2019/4/26:分行处理日期
			rpdt.setClprice8(rpdtWksBean.getVdcleanprice8());// 90.00000000:净价
			rpdt.setCcy(rprh.getCcy());// CNY:币种代码
			rpdt.setDrprice8(rpdtWksBean.getVddirtyprice8());// 90.01250000:全价
			rpdt.setDiscamt(new BigDecimal(denom));// 10000.0000:折扣
			rpdt.setDisprice8(rpdtWksBean.getDisprice8());// 90.00000000:票面价格
			rpdt.setFaceamt(rpdtWksBean.getQty().multiply(new BigDecimal(denom)));// 100000.0000:票面金额
			rpdt.setInvtype(repoWksBean.getInvtype());// A:投资类型
			rpdt.setMdate(rprh.getMatdate());// 2019/4/25:到期日
			rpdt.setPort(rprh.getPort());// FIZY:投资组合编号
			rpdt.setPrinamt(rpdtWksBean.getCcyamt());// 100000.0000:本金金额
			rpdt.setProceedamt(rpdtWksBean.getCommprocamt());// 90012.5000:回购结算金额
			rpdt.setProduct(rprh.getProduct());// REPO :产品代码
			rpdt.setProdtype(rprh.getProdtype());// RP:产品类型代码
			rpdt.setPs(rprh.getPs());// S:买卖标志
			rpdt.setPurchintamt(getPlusNegateAmt(rprh.getPs(), rpdtWksBean.getPurchintamt()));// 12.5000:买回应计利息金额
			rpdt.setRepointamt(getPlusNegateAmt(rprh.getPs(), rpdtWksBean.getRepointamt()).negate());// -8.7500:回购利息1
			rpdt.setQty(rpdtWksBean.getQty());// 10.0000:数量
			rpdt.setSecid(rpdtWksBean.getSecid());// TEST003:证券编号
			rpdt.setVdate(rprh.getVdate());// 2019/4/24:起息日期
			rpdt.setYield8(rpdtWksBean.getYield8());// 15.20000000:收益
			rpdt.setDelivtype(rprh.getDelivtype());// DVP:DELIVTYPE
			rpdt.setCno(rprh.getCno());// 120200002 :交易对手
			rpdt.setRepocost(rprh.getCost());// 1000000000:回购成本中心
			rpdt.setVerdate(rprh.getBrprcindte());// 2019/4/26:复核日期
			rpdt.setCollunit(rprh.getCollunit());// BON:抵押单位
			rpdt.setVdcleanprice8(rpdtWksBean.getVdcleanprice8());// 90.00000000:起息日净价
			rpdt.setVddirtyprice8(rpdtWksBean.getVddirtyprice8());// 90.01250000:起息日全价
			rpdt.setMdcleanprice8(rpdtWksBean.getMdcleanprice8());// 89.99625122:到期日净价
			rpdt.setMddirtyprice8(rpdtWksBean.getMddirtyprice8());// 90.02125122:到期日全价
			rpdt.setNotccy(rprh.getCcy());// CNY:名义币种
			rpdt.setCommprocamt(getPlusNegateAmt(rprh.getPs(), rpdtWksBean.getCommprocamt()));// 90012.5000:开始收入金额
			rpdt.setMatprocamt(getPlusNegateAmt(rprh.getPs(), rpdtWksBean.getMatprocamt()).negate());// -90021.2500:到期收入金额
			rpdt.setMatpurchintamt(hcmatpurchintamt);// 25.0000:到期购买利息金额
			rpdt.setUnamortamt(BigDecimal.ZERO);// 0.0000:未摊销金额
			rpdt.setReporate8(rprh.getReporate8());// 3.50000000:回购利率
			rpdt.setSpreadrate8(BigDecimal.ZERO);// 0.00000000:利率差
			rpdt.setHcpurchintamt(getPlusNegateAmt(rprh.getPs(), rpdtWksBean.getHcpurchintamt()));// 12.5000:初始基本利率
			rpdt.setHcmatpurchintamt(getPlusNegateAmt(rprh.getPs(), hcmatpurchintamt));// 25.0000:HCMATPURCHINTAMT
			rpdt.setRepoprinamt(getPlusNegateAmt(rprh.getPs(), rpdtWksBean.getMatprocamt()).negate());// -90012.5000:回购本金金额
			rpdt.setCost(rpdtWksBean.getCost());// 1360300001:成本中心
			rpdt.setConvintamt(rpdtWksBean.getConvintamt());// 12.5000:兑换利息
			rpdt.setConvintbamt(rpdtWksBean.getConvintamt());// 12.5000:交货类型:利息折本币金额
			rpdt.setSettccy(rprh.getCcy());// CNY:清算币种
//    		rpdt.setText1(null);//C:备用1
			rpdt.setAmount1(rpdt.getPrinamt());// 100000.0000:备用5
			rpdt.setRate18(rpdt.getDisprice8());// 90.00000000:备用7
			i++;
			rpdtLst.add(rpdt);
		}
		return rpdtLst;
	}

	public Rpdt DataTrans(Rpdt rpdt) {
		rpdt.setSeq(OpicsConstant.RPDT.SEQ);// 0 :流水号
		rpdt.setChargeamt(BigDecimal.ZERO);// 0.0000:费用
		rpdt.setDealtime(DateUtil.getCurrentTimeAsString());// 10:42:41:交易时间
		rpdt.setDiscrate8(BigDecimal.ZERO);// 0.00000000:折扣率
		rpdt.setExchcommamt(BigDecimal.ZERO);// 0.0000:兑换佣金
		rpdt.setInputdate(DateUtil.parse(DateUtil.getCurrentDateAsString()));// 2020/2/28:系统输入日期
		rpdt.setIoper(OpicsConstant.RPDT.TRAD_NO);// KJJ :输入操作员
		rpdt.setInputtime(DateUtil.getCurrentTimeAsString());// 10:42:41:系统输入时间
		rpdt.setPremamt(BigDecimal.ZERO);// 0.0000:赎回溢价
		rpdt.setVatamt(BigDecimal.ZERO);// 0.0000:增值税
		rpdt.setWhtamt(BigDecimal.ZERO);// 0.0000:计提税金额
		rpdt.setCpnpaidamt(BigDecimal.ZERO);// 0.0000:已付息票总额
		rpdt.setNxtcpnamt(BigDecimal.ZERO);// 0.0000:下一个息票付款金额
		rpdt.setTdymtmamt(BigDecimal.ZERO);// 0.0000:今日市场价
		rpdt.setYstmtmamt(BigDecimal.ZERO);// 0.0000:昨日市场价
		rpdt.setPmemtmamt(BigDecimal.ZERO);// 0.0000:上月市场价
		rpdt.setPyemtmamt(BigDecimal.ZERO);// 0.0000:上一年市场价
		rpdt.setTdyincexpamt(BigDecimal.ZERO);// 0.0000:今日损益
		rpdt.setYstincexpamt(BigDecimal.ZERO);// 0.0000:昨日损益
		rpdt.setPmeincexpamt(BigDecimal.ZERO);// 0.0000:上月损益
		rpdt.setPyeincexpamt(BigDecimal.ZERO);// 0.0000:上一年损益
		rpdt.setTdydscamt(BigDecimal.ZERO);// 0.0000:TDYDSCAMT
		rpdt.setYstdscamt(BigDecimal.ZERO);// 0.0000:YSTDSCAMT
		rpdt.setPmedscamt(BigDecimal.ZERO);// 0.0000:PMEDSCAMT
		rpdt.setPyedscamt(BigDecimal.ZERO);// 0.0000:PYEDSCAMT
		rpdt.setTdypremamt(BigDecimal.ZERO);// 0.0000:今日溢价
		rpdt.setYstpremamt(BigDecimal.ZERO);// 0.0000:昨日溢价
		rpdt.setCoupaccroutstamt(BigDecimal.ZERO);// 0.0000:息票利息
		rpdt.setCoupaccrnextmonth(BigDecimal.ZERO);// 0.0000:下月息票利息
		rpdt.setSettavgcost(BigDecimal.ZERO);// 0.0000:固定平均成本
		rpdt.setSettavgcostyst(BigDecimal.ZERO);// 0.0000:昨日清算平均成本
		rpdt.setBookvalamt(BigDecimal.ZERO);// 0.0000:帐面价值
		rpdt.setBookvalamtyst(BigDecimal.ZERO);// 0.0000:昨日帐面价值
		rpdt.setPrinexchrate8(new BigDecimal("1"));// 1.00000000:本金汇率
		rpdt.setIntexchrate8(new BigDecimal("1"));// 1.00000000:利息汇率
		rpdt.setPrinbaserate8(new BigDecimal("1"));// 1.00000000:本金基本利率
		rpdt.setIntbaserate8(new BigDecimal("1"));// 1.00000000:初始基本利率
		rpdt.setPrinexchterms(OpicsConstant.RPDT.PRINEXCHTERMS);// M:本金汇率规则
		rpdt.setIntexchterms(OpicsConstant.RPDT.INTEXCHTERMS);// M:利息兑换规则
		rpdt.setPrinbaseterms(OpicsConstant.RPDT.PRINBASETERMS);// M:本金基本利率规则
		rpdt.setIntbaseterms(OpicsConstant.RPDT.INTBASETERMS);// M:利息汇率
		rpdt.setHaircut8(BigDecimal.ZERO);// 0.00000000:HAIRCUT8
		rpdt.setReinvestind(OpicsConstant.RPDT.REINVESTIND);// N:息票重新投资标志
		rpdt.setMktvalue(BigDecimal.ZERO);// 0.0000:市场价值
		rpdt.setTdyrepointamt(BigDecimal.ZERO);// 0.0000:今日回购利息
		rpdt.setYstrepointamt(BigDecimal.ZERO);// 0.0000:昨日回购利息
		rpdt.setPmerepointamt(BigDecimal.ZERO);// 0.0000:上月末回购利息
		rpdt.setPyerepointamt(BigDecimal.ZERO);// 0.0000:上年末回购利息
		rpdt.setTdyrepomtmamt(BigDecimal.ZERO);// 0.0000:今日回购市场价
		rpdt.setYstrepomtmamt(BigDecimal.ZERO);// 0.0000:昨日回购市场价
		rpdt.setPmerepomtmamt(BigDecimal.ZERO);// 0.0000:上月末回购市场价
		rpdt.setPyerepomtmamt(BigDecimal.ZERO);// 0.0000:上年末回购市场价
		rpdt.setPvmatprocamt(BigDecimal.ZERO);// 0.0000:到期收益现值
		rpdt.setTdypurchintamt(BigDecimal.ZERO);// 0.0000:今日买入利息金额
		rpdt.setYstpurchintamt(BigDecimal.ZERO);// 0.0000:昨天的买入利息金额
		rpdt.setAmount2(BigDecimal.ZERO);// 0.0000:备用6
		rpdt.setRate28(BigDecimal.ZERO);// 0.00000000:备用8
		rpdt.setUpdatecounter(OpicsConstant.RPDT.UPDATE_COUNTER);// 2:更新次数
		rpdt.setVdpairedfaceamt(BigDecimal.ZERO);// 0.0000:VDPAIREDFACEAMT
		rpdt.setVdpairedamt(BigDecimal.ZERO);// 0.0000:VDPAIREDAMT
		rpdt.setVdpairedind(OpicsConstant.RPDT.VDPAIREDIND);// 0:VDPAIREDIND
		rpdt.setVdcurrsplitseq(OpicsConstant.RPDT.VDCURRSPLITSEQ);// 0 :VDCURRSPLITSEQ
		rpdt.setMdpairedfaceamt(BigDecimal.ZERO);// 0.0000:MDPAIREDFACEAMT
		rpdt.setMdpairedamt(BigDecimal.ZERO);// 0.0000:MDPAIREDAMT
		rpdt.setMdpairedind(OpicsConstant.RPDT.MDPAIREDIND);// 0:MDPAIREDIND
		rpdt.setMdcurrsplitseq(OpicsConstant.RPDT.MDCURRSPLITSEQ);// 0 :MDCURRSPLITSEQ
		rpdt.setIndexratio8(BigDecimal.ZERO);// 0.00000000:
		rpdt.setNovfaceamt(BigDecimal.ZERO);// 0.0000:
		rpdt.setSecondprice8(BigDecimal.ZERO);// 0.00000000:
		return rpdt;
	}

	public Drpi DataTrans(Drpi drpi, Rprh rprh) {
		drpi.setBr(rprh.getBr());// 机构:
		drpi.setProduct(rprh.getProduct());// 产品:
		drpi.setProdtype(rprh.getProdtype());// 产品类型:
		drpi.setDealno(rprh.getDealno());// 单号:
		drpi.setSeq(rprh.getSeq());// 序号:
		drpi.setDelrecind("P".equals(rprh.getPs()) ? "R" : "D");// 债的收付方向:
		drpi.setSupsecmoveind(OpicsConstant.RPRH.SUPSECMOVEIND);// 默认N :
		drpi.setSupccymoveind(OpicsConstant.RPRH.SUPCCYMOVEIND);// 默认N:
		drpi.setLstmntdate(Calendar.getInstance().getTime());// :

		return drpi;
	}

	/**
	 * 获取dealno
	 * <p>
	 * 回购的单号比较特殊,需要进行特殊处理
	 *
	 * @param rprh
	 * @param mcfpService
	 * @return
	 */
	public Rprh DataTrans(Rprh rprh, McfpService mcfpService) {
		// 获取DEALNO
		Mcfp mcfp = mcfpService.selectByPrimaryKey(OpicsConstant.RPRH.BRANCH, OpicsConstant.RPRH.PROD_CODE,
				OpicsConstant.RPRH.PROD_TYPE);// 设置交易编号
		String dealno = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";// 获取
		rprh.setDealno(dealno);// 设置交易编号
		return rprh;
	}

	/**
	 * 更新交易单号
	 * <p>
	 * 回购的单号比较特殊,需要进行特殊处理
	 *
	 * @param rprh
	 * @param mcfpService
	 * @param mcfp
	 */
	public void DataTrans(Rprh rprh, McfpService mcfpService, Mcfp mcfp) {
		mcfp.setBr(rprh.getBr());
		mcfp.setTraddno(StringUtils.trimToEmpty(rprh.getDealno()));
		mcfp.setProdcode(rprh.getProduct());// 产品代码 去掉空格
		mcfp.setType(rprh.getProdtype());// 产品类型
		mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
	}

	/**
	 * 账务日期
	 * @param rprh
	 * @param brpsService
	 * @return rprh
	 */
	public Rprh DataTrans(Rprh rprh, BrpsService brpsService) {
		// 获取当前账务日期
		Brps brps = brpsService.selectByPrimaryKey(OpicsConstant.RPRH.BRANCH);
		rprh.setBrprcindte(brps.getBranprcdate());// 当前账务日期
		return rprh;
	}

	/**
	 * 转换计息基础
	 * @param str x
	 * @return str
	 */
	private String getBasisTrans(String str) {
		if (StringUtils.equals(str, "Actual/360")) {
			return "A360";
		} else if (StringUtils.equals(str, "Actual/365")) {
			return "A365";
		} else if (StringUtils.equals(str, "Actual/Actual")) {
			return "ACTUAL";
		}
		return "";
	}

	/**
	 * 根据收付方向判断金额正负
	 */
	private BigDecimal getPlusNegateAmt(String drcrind, BigDecimal amt) {
		if (StringUtils.equals(drcrind, OpicsConstant.RPRH.PAY_IND)
				|| StringUtils.equals(drcrind, OpicsConstant.RPRH.PS_IND)) {
			return amt.negate();// 取反
		} else {
			return amt.plus();// 取正
		}
	}
}
