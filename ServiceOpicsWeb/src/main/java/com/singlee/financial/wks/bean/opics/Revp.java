package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Revp implements Serializable {
    private String br;

    private String ccy;

    private BigDecimal spotrate8;

    private String terms;

    private String period1;

    private BigDecimal rate18;

    private String period2;

    private BigDecimal rate28;

    private String period3;

    private BigDecimal rate38;

    private String period4;

    private BigDecimal rate48;

    private String period5;

    private BigDecimal rate58;

    private String period6;

    private BigDecimal rate68;

    private String period7;

    private BigDecimal rate78;

    private String period8;

    private BigDecimal rate88;

    private String period9;

    private BigDecimal rate98;

    private String period10;

    private BigDecimal rate108;

    private String period11;

    private BigDecimal rate118;

    private String period12;

    private BigDecimal rate128;

    private String period13;

    private BigDecimal rate138;

    private String period14;

    private BigDecimal rate148;

    private String period15;

    private BigDecimal rate158;

    private String period16;

    private BigDecimal rate168;

    private String period17;

    private BigDecimal rate178;

    private String period18;

    private BigDecimal rate188;

    private String period19;

    private BigDecimal rate198;

    private String period20;

    private BigDecimal rate208;

    private String period21;

    private BigDecimal rate218;

    private String period22;

    private BigDecimal rate228;

    private String period23;

    private BigDecimal rate238;

    private String period24;

    private BigDecimal rate248;

    private String period25;

    private BigDecimal rate258;

    private String period26;

    private BigDecimal rate268;

    private String period27;

    private BigDecimal rate278;

    private String period28;

    private BigDecimal rate288;

    private String period29;

    private BigDecimal rate298;

    private String period30;

    private BigDecimal rate308;

    private String period31;

    private BigDecimal rate318;

    private String period32;

    private BigDecimal rate328;

    private String period33;

    private BigDecimal rate338;

    private String period34;

    private BigDecimal rate348;

    private String period35;

    private BigDecimal rate358;

    private String period36;

    private BigDecimal rate368;

    private String period37;

    private BigDecimal rate378;

    private String period38;

    private BigDecimal rate388;

    private String period39;

    private BigDecimal rate398;

    private Date lstmntdate;

    public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public BigDecimal getSpotrate8() {
		return spotrate8;
	}

	public void setSpotrate8(BigDecimal spotrate8) {
		this.spotrate8 = spotrate8;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getPeriod1() {
		return period1;
	}

	public void setPeriod1(String period1) {
		this.period1 = period1;
	}

	public BigDecimal getRate18() {
		return rate18;
	}

	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}

	public String getPeriod2() {
		return period2;
	}

	public void setPeriod2(String period2) {
		this.period2 = period2;
	}

	public BigDecimal getRate28() {
		return rate28;
	}

	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}

	public String getPeriod3() {
		return period3;
	}

	public void setPeriod3(String period3) {
		this.period3 = period3;
	}

	public BigDecimal getRate38() {
		return rate38;
	}

	public void setRate38(BigDecimal rate38) {
		this.rate38 = rate38;
	}

	public String getPeriod4() {
		return period4;
	}

	public void setPeriod4(String period4) {
		this.period4 = period4;
	}

	public BigDecimal getRate48() {
		return rate48;
	}

	public void setRate48(BigDecimal rate48) {
		this.rate48 = rate48;
	}

	public String getPeriod5() {
		return period5;
	}

	public void setPeriod5(String period5) {
		this.period5 = period5;
	}

	public BigDecimal getRate58() {
		return rate58;
	}

	public void setRate58(BigDecimal rate58) {
		this.rate58 = rate58;
	}

	public String getPeriod6() {
		return period6;
	}

	public void setPeriod6(String period6) {
		this.period6 = period6;
	}

	public BigDecimal getRate68() {
		return rate68;
	}

	public void setRate68(BigDecimal rate68) {
		this.rate68 = rate68;
	}

	public String getPeriod7() {
		return period7;
	}

	public void setPeriod7(String period7) {
		this.period7 = period7;
	}

	public BigDecimal getRate78() {
		return rate78;
	}

	public void setRate78(BigDecimal rate78) {
		this.rate78 = rate78;
	}

	public String getPeriod8() {
		return period8;
	}

	public void setPeriod8(String period8) {
		this.period8 = period8;
	}

	public BigDecimal getRate88() {
		return rate88;
	}

	public void setRate88(BigDecimal rate88) {
		this.rate88 = rate88;
	}

	public String getPeriod9() {
		return period9;
	}

	public void setPeriod9(String period9) {
		this.period9 = period9;
	}

	public BigDecimal getRate98() {
		return rate98;
	}

	public void setRate98(BigDecimal rate98) {
		this.rate98 = rate98;
	}

	public String getPeriod10() {
		return period10;
	}

	public void setPeriod10(String period10) {
		this.period10 = period10;
	}

	public BigDecimal getRate108() {
		return rate108;
	}

	public void setRate108(BigDecimal rate108) {
		this.rate108 = rate108;
	}

	public String getPeriod11() {
		return period11;
	}

	public void setPeriod11(String period11) {
		this.period11 = period11;
	}

	public BigDecimal getRate118() {
		return rate118;
	}

	public void setRate118(BigDecimal rate118) {
		this.rate118 = rate118;
	}

	public String getPeriod12() {
		return period12;
	}

	public void setPeriod12(String period12) {
		this.period12 = period12;
	}

	public BigDecimal getRate128() {
		return rate128;
	}

	public void setRate128(BigDecimal rate128) {
		this.rate128 = rate128;
	}

	public String getPeriod13() {
		return period13;
	}

	public void setPeriod13(String period13) {
		this.period13 = period13;
	}

	public BigDecimal getRate138() {
		return rate138;
	}

	public void setRate138(BigDecimal rate138) {
		this.rate138 = rate138;
	}

	public String getPeriod14() {
		return period14;
	}

	public void setPeriod14(String period14) {
		this.period14 = period14;
	}

	public BigDecimal getRate148() {
		return rate148;
	}

	public void setRate148(BigDecimal rate148) {
		this.rate148 = rate148;
	}

	public String getPeriod15() {
		return period15;
	}

	public void setPeriod15(String period15) {
		this.period15 = period15;
	}

	public BigDecimal getRate158() {
		return rate158;
	}

	public void setRate158(BigDecimal rate158) {
		this.rate158 = rate158;
	}

	public String getPeriod16() {
		return period16;
	}

	public void setPeriod16(String period16) {
		this.period16 = period16;
	}

	public BigDecimal getRate168() {
		return rate168;
	}

	public void setRate168(BigDecimal rate168) {
		this.rate168 = rate168;
	}

	public String getPeriod17() {
		return period17;
	}

	public void setPeriod17(String period17) {
		this.period17 = period17;
	}

	public BigDecimal getRate178() {
		return rate178;
	}

	public void setRate178(BigDecimal rate178) {
		this.rate178 = rate178;
	}

	public String getPeriod18() {
		return period18;
	}

	public void setPeriod18(String period18) {
		this.period18 = period18;
	}

	public BigDecimal getRate188() {
		return rate188;
	}

	public void setRate188(BigDecimal rate188) {
		this.rate188 = rate188;
	}

	public String getPeriod19() {
		return period19;
	}

	public void setPeriod19(String period19) {
		this.period19 = period19;
	}

	public BigDecimal getRate198() {
		return rate198;
	}

	public void setRate198(BigDecimal rate198) {
		this.rate198 = rate198;
	}

	public String getPeriod20() {
		return period20;
	}

	public void setPeriod20(String period20) {
		this.period20 = period20;
	}

	public BigDecimal getRate208() {
		return rate208;
	}

	public void setRate208(BigDecimal rate208) {
		this.rate208 = rate208;
	}

	public String getPeriod21() {
		return period21;
	}

	public void setPeriod21(String period21) {
		this.period21 = period21;
	}

	public BigDecimal getRate218() {
		return rate218;
	}

	public void setRate218(BigDecimal rate218) {
		this.rate218 = rate218;
	}

	public String getPeriod22() {
		return period22;
	}

	public void setPeriod22(String period22) {
		this.period22 = period22;
	}

	public BigDecimal getRate228() {
		return rate228;
	}

	public void setRate228(BigDecimal rate228) {
		this.rate228 = rate228;
	}

	public String getPeriod23() {
		return period23;
	}

	public void setPeriod23(String period23) {
		this.period23 = period23;
	}

	public BigDecimal getRate238() {
		return rate238;
	}

	public void setRate238(BigDecimal rate238) {
		this.rate238 = rate238;
	}

	public String getPeriod24() {
		return period24;
	}

	public void setPeriod24(String period24) {
		this.period24 = period24;
	}

	public BigDecimal getRate248() {
		return rate248;
	}

	public void setRate248(BigDecimal rate248) {
		this.rate248 = rate248;
	}

	public String getPeriod25() {
		return period25;
	}

	public void setPeriod25(String period25) {
		this.period25 = period25;
	}

	public BigDecimal getRate258() {
		return rate258;
	}

	public void setRate258(BigDecimal rate258) {
		this.rate258 = rate258;
	}

	public String getPeriod26() {
		return period26;
	}

	public void setPeriod26(String period26) {
		this.period26 = period26;
	}

	public BigDecimal getRate268() {
		return rate268;
	}

	public void setRate268(BigDecimal rate268) {
		this.rate268 = rate268;
	}

	public String getPeriod27() {
		return period27;
	}

	public void setPeriod27(String period27) {
		this.period27 = period27;
	}

	public BigDecimal getRate278() {
		return rate278;
	}

	public void setRate278(BigDecimal rate278) {
		this.rate278 = rate278;
	}

	public String getPeriod28() {
		return period28;
	}

	public void setPeriod28(String period28) {
		this.period28 = period28;
	}

	public BigDecimal getRate288() {
		return rate288;
	}

	public void setRate288(BigDecimal rate288) {
		this.rate288 = rate288;
	}

	public String getPeriod29() {
		return period29;
	}

	public void setPeriod29(String period29) {
		this.period29 = period29;
	}

	public BigDecimal getRate298() {
		return rate298;
	}

	public void setRate298(BigDecimal rate298) {
		this.rate298 = rate298;
	}

	public String getPeriod30() {
		return period30;
	}

	public void setPeriod30(String period30) {
		this.period30 = period30;
	}

	public BigDecimal getRate308() {
		return rate308;
	}

	public void setRate308(BigDecimal rate308) {
		this.rate308 = rate308;
	}

	public String getPeriod31() {
		return period31;
	}

	public void setPeriod31(String period31) {
		this.period31 = period31;
	}

	public BigDecimal getRate318() {
		return rate318;
	}

	public void setRate318(BigDecimal rate318) {
		this.rate318 = rate318;
	}

	public String getPeriod32() {
		return period32;
	}

	public void setPeriod32(String period32) {
		this.period32 = period32;
	}

	public BigDecimal getRate328() {
		return rate328;
	}

	public void setRate328(BigDecimal rate328) {
		this.rate328 = rate328;
	}

	public String getPeriod33() {
		return period33;
	}

	public void setPeriod33(String period33) {
		this.period33 = period33;
	}

	public BigDecimal getRate338() {
		return rate338;
	}

	public void setRate338(BigDecimal rate338) {
		this.rate338 = rate338;
	}

	public String getPeriod34() {
		return period34;
	}

	public void setPeriod34(String period34) {
		this.period34 = period34;
	}

	public BigDecimal getRate348() {
		return rate348;
	}

	public void setRate348(BigDecimal rate348) {
		this.rate348 = rate348;
	}

	public String getPeriod35() {
		return period35;
	}

	public void setPeriod35(String period35) {
		this.period35 = period35;
	}

	public BigDecimal getRate358() {
		return rate358;
	}

	public void setRate358(BigDecimal rate358) {
		this.rate358 = rate358;
	}

	public String getPeriod36() {
		return period36;
	}

	public void setPeriod36(String period36) {
		this.period36 = period36;
	}

	public BigDecimal getRate368() {
		return rate368;
	}

	public void setRate368(BigDecimal rate368) {
		this.rate368 = rate368;
	}

	public String getPeriod37() {
		return period37;
	}

	public void setPeriod37(String period37) {
		this.period37 = period37;
	}

	public BigDecimal getRate378() {
		return rate378;
	}

	public void setRate378(BigDecimal rate378) {
		this.rate378 = rate378;
	}

	public String getPeriod38() {
		return period38;
	}

	public void setPeriod38(String period38) {
		this.period38 = period38;
	}

	public BigDecimal getRate388() {
		return rate388;
	}

	public void setRate388(BigDecimal rate388) {
		this.rate388 = rate388;
	}

	public String getPeriod39() {
		return period39;
	}

	public void setPeriod39(String period39) {
		this.period39 = period39;
	}

	public BigDecimal getRate398() {
		return rate398;
	}

	public void setRate398(BigDecimal rate398) {
		this.rate398 = rate398;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	private static final long serialVersionUID = 1L;
}