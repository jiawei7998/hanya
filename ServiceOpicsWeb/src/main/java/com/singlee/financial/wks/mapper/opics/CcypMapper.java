package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Ccyp;
import org.apache.ibatis.annotations.Param;

public interface CcypMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy1") String ccy1, @Param("ccy2") String ccy2);

    int insert(Ccyp record);

    int insertSelective(Ccyp record);

    Ccyp selectByPrimaryKey(@Param("br") String br, @Param("ccy1") String ccy1, @Param("ccy2") String ccy2);

    int updateByPrimaryKeySelective(Ccyp record);

    int updateByPrimaryKey(Ccyp record);
}