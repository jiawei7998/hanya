package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Mcfp;
import com.singlee.financial.wks.mapper.opics.McfpMapper;
import com.singlee.financial.wks.service.opics.McfpService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class McfpServiceImpl implements McfpService {

    @Resource
    private McfpMapper mcfpMapper;

    @Override
    public int deleteByPrimaryKey(String br, String prodcode, String type) {
        return mcfpMapper.deleteByPrimaryKey(br, prodcode, type);
    }

    @Override
    public int insert(Mcfp record) {
        return mcfpMapper.insert(record);
    }

    @Override
    public int insertSelective(Mcfp record) {
        return mcfpMapper.insertSelective(record);
    }

    @Override
    public Mcfp selectByPrimaryKey(String br, String prodcode, String type) {
        return mcfpMapper.selectByPrimaryKey(br, prodcode, type);
    }

    @Override
    public int updateByPrimaryKeySelective(Mcfp record) {
        return mcfpMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Mcfp record) {
        return mcfpMapper.updateByPrimaryKey(record);
    }

}
