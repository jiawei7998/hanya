package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.page.PageInfo;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.RdfhWksBean;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Nupd;
import com.singlee.financial.wks.bean.opics.Pmtq;
import com.singlee.financial.wks.bean.opics.Rdfh;
import com.singlee.financial.wks.intfc.RdfhhService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.RdfhEntry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 收息服务，和交易无关
 * 
 * @author xuqq
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class RdfhhServiceImpl implements RdfhhService {

	@Autowired
	private NupdService nupdService;
	@Autowired
	private PmtqService pmtqService;
	@Autowired
	private PrirService prirService;
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private RdfhService rdfhService;
	@Autowired
	private RdfhEntry rdfhEntry;// 收息转换方法
	private static Logger logger = Logger.getLogger(RdfhhServiceImpl.class);

	@Override
	public PageInfo<RdfhWksBean> searchDlCount(Map<String, Object> map) {
		return null;
	}

	@Override
	public SlOutBean receiveInterestTrading(RdfhWksBean rdfhWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "RDFH-00000", "债券收息成功!");
		Rdfh rdfh = null;
		try {
			// 查询rdfh
			rdfh = rdfhService.selectByPrimaryKey(rdfhWksBean.getInstId(), rdfhWksBean.getTradeNo(),
					OpicsConstant.SPSH.SEQ);
			// 复核:修改RDFH,经办/授权:1.更新RDFH表
			if (rdfh.getRevdate() != null) {
				logger.info("RDFH-10001:单号[" + rdfhWksBean.getTradeNo() + "]是已冲销的收息不能收息");
				JY.raise("RDFH-10001:单号[" + rdfhWksBean.getTradeNo() + "]是已冲销的收息不能收息");
			}
			rdfh = rdfhEntry.DataTrans(rdfh, rdfhWksBean, brpsService);
			rdfhService.updateByPrimaryKeyRec(rdfh);

			// 插入PRIR表
			// 7.收款信息
			prirService.insert(prirService.builder(rdfh));
			// 3.1插入NUPD表数据
			List<Nupd> nupdLst = nupdService.builder(rdfh);
			if (nupdLst.size() > 0) {
				nupdService.batchInsert(nupdLst);
			}
			// 4.2插入新的PMTQ记录
			List<Pmtq> pmtqLst = pmtqService.builder(rdfh);
			if (pmtqLst.size() > 0) {
				pmtqService.batchInsert(pmtqLst);
			}

		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode("RDFH-10001");
			outBean.setRetMsg("债券收息处理错误<请检查输入要素完整性>" + e.getMessage());
			logger.error("债券收息处理错误<请检查输入要素完整性>", e);
			JY.raise("RDFH-10001", e);
		}
		outBean.setClientNo(rdfh.getTransno());
		logger.info("债券收息处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

	@Override
	public SlOutBean reverseInterestTrading(RdfhWksBean rdfhWksBean) {
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "RDFH-00000", "债券收息冲销成功!");
		Rdfh rdfh = null;
		try {
			// 查询rdfh
			rdfh = rdfhService.selectByPrimaryKey(rdfhWksBean.getInstId(), rdfhWksBean.getTradeNo(),
					OpicsConstant.SPSH.SEQ);
			if (rdfh.getRevdate() != null) {
				logger.info("RDFH-10001:单号[" + rdfhWksBean.getTradeNo() + "]是已冲销的收息不能再次冲销");
				JY.raise("RDFH-10001:单号[" + rdfhWksBean.getTradeNo() + "]是已冲销的收息不能再次冲销");
			}
			// 获取当前账务日期
			Brps brps = brpsService.selectByPrimaryKey(rdfh.getBr());
			// 修改RDFH
			rdfh = rdfhEntry.DataTrans(rdfh, brpsService);
			rdfhService.updateRevInterest(rdfh);

			// pmtq解决未到期，已到期等处理
			pmtqService.callSpUpdrecPmtqSwiftfmt(rdfh.getBr(), rdfh.getTransno(), rdfh.getSeq(), rdfh.getProduct(),
					rdfh.getProdtype(), brps.getBranprcdate(), DateUtil.getCurrentTimeAsString(), "", "", "", "", null,
					BigDecimal.ZERO);

			// nupd解决未到期，已到期等处理
			nupdService.callSpDelrecNupdVdate(rdfh.getBr(), rdfh.getTransno(), rdfh.getSeq(), rdfh.getProduct(),
					rdfh.getProdtype(), rdfh.getSettdate(), rdfh.getCcysacct());

		} catch (Exception e) {
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setRetCode("RDFH-10001");
			outBean.setRetMsg("债券收息冲销处理错误<请检查输入要素完整性>" + e.getMessage());
			logger.error("债券收息冲销处理错误<请检查输入要素完整性>", e);
			JY.raise("RDFH-10001", e);
		}
		outBean.setClientNo(rdfh.getTransno());
		logger.info("债券收息冲销处理完成,返回消息:" + JSON.toJSONString(outBean));
		return outBean;
	}

}
