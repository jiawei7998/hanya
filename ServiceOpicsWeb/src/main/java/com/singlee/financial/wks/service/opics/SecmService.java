package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Secm;
public interface SecmService{


    int deleteByPrimaryKey(String secid);

    int insert(Secm record);

    int insertSelective(Secm record);

    Secm selectByPrimaryKey(String secid);

    int updateByPrimaryKeySelective(Secm record);

    int updateByPrimaryKey(Secm record);

}
