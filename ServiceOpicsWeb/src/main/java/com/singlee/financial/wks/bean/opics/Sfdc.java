package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Sfdc implements Serializable {
    private String br;

    private String cno;

    private String delrecind;

    private String product;

    private String prodtype;

    private String ccy;

    private String safekeepacct;

    private String dcc;

    private String accountno;

    private String bic;

    private String c1;

    private String c2;

    private String c3;

    private String c4;

    private Date lstmntdate;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getDelrecind() {
		return delrecind;
	}


	public void setDelrecind(String delrecind) {
		this.delrecind = delrecind;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getSafekeepacct() {
		return safekeepacct;
	}


	public void setSafekeepacct(String safekeepacct) {
		this.safekeepacct = safekeepacct;
	}


	public String getDcc() {
		return dcc;
	}


	public void setDcc(String dcc) {
		this.dcc = dcc;
	}


	public String getAccountno() {
		return accountno;
	}


	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}


	public String getBic() {
		return bic;
	}


	public void setBic(String bic) {
		this.bic = bic;
	}


	public String getC1() {
		return c1;
	}


	public void setC1(String c1) {
		this.c1 = c1;
	}


	public String getC2() {
		return c2;
	}


	public void setC2(String c2) {
		this.c2 = c2;
	}


	public String getC3() {
		return c3;
	}


	public void setC3(String c3) {
		this.c3 = c3;
	}


	public String getC4() {
		return c4;
	}


	public void setC4(String c4) {
		this.c4 = c4;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	private static final long serialVersionUID = 1L;
}