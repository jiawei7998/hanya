package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Ycrt;
import com.singlee.financial.wks.mapper.opics.YcrtMapper;
import com.singlee.financial.wks.service.opics.YcrtService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class YcrtServiceImpl implements YcrtService {

    @Resource
    private YcrtMapper ycrtMapper;

    @Override
    public int deleteByPrimaryKey(String br,String ccy,String contribrate,String mtystart,String mtyend) {
        return ycrtMapper.deleteByPrimaryKey(br,ccy,contribrate,mtystart,mtyend);
    }

    @Override
    public int insert(Ycrt record) {
        return ycrtMapper.insert(record);
    }

    @Override
    public int insertSelective(Ycrt record) {
        return ycrtMapper.insertSelective(record);
    }

    @Override
    public Ycrt selectByPrimaryKey(String br,String ccy,String contribrate,String mtystart,String mtyend) {
        return ycrtMapper.selectByPrimaryKey(br,ccy,contribrate,mtystart,mtyend);
    }

    @Override
    public int updateByPrimaryKeySelective(Ycrt record) {
        return ycrtMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Ycrt record) {
        return ycrtMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<Ycrt> selectByAll(Ycrt record) {
        return ycrtMapper.selectByAll(record);
    }

}
