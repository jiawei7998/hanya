package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Secl;
import com.singlee.financial.wks.mapper.opics.SeclMapper;
import com.singlee.financial.wks.service.opics.SeclService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;import java.util.List;

@Service
public class SeclServiceImpl implements SeclService {

    @Resource
    private SeclMapper seclMapper;

    @Override
    public int deleteByPrimaryKey(String br, String secid) {
        return seclMapper.deleteByPrimaryKey(br, secid);
    }

    @Override
    public int insert(Secl record) {
        return seclMapper.insert(record);
    }

    @Override
    public int insertSelective(Secl record) {
        return seclMapper.insertSelective(record);
    }

    @Override
    public Secl selectByPrimaryKey(String br, String secid) {
        return seclMapper.selectByPrimaryKey(br, secid);
    }

    @Override
    public int updateByPrimaryKeySelective(Secl record) {
        return seclMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Secl record) {
        return seclMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updateBatch(List<Secl> list) {
        return seclMapper.updateBatch(list);
    }

    @Override
    public int updateBatchSelective(List<Secl> list) {
        return seclMapper.updateBatchSelective(list);
    }
}

