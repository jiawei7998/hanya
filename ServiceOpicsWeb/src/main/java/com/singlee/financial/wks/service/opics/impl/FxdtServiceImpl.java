package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Fxdt;
import com.singlee.financial.wks.mapper.opics.FxdtMapper;
import com.singlee.financial.wks.service.opics.FxdtService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FxdtServiceImpl implements FxdtService {

    @Resource
    private FxdtMapper fxdtMapper;
    @Resource
    private BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br, String dealno, String seq) {
        return fxdtMapper.deleteByPrimaryKey(br, dealno, seq);
    }

    @Override
    public int insert(Fxdt record) {
        return fxdtMapper.insert(record);
    }

    @Override
    public int insertSelective(Fxdt record) {
        return fxdtMapper.insertSelective(record);
    }

    @Override
    public Fxdt selectByPrimaryKey(String br, String dealno, String seq) {
        return fxdtMapper.selectByPrimaryKey(br, dealno, seq);
    }

    @Override
    public int updateByPrimaryKeySelective(Fxdt record) {
        return fxdtMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Fxdt record) {
        return fxdtMapper.updateByPrimaryKey(record);
    }
    @Override
	public int updateReverseByPrimaryKey(Fxdt record) {
    	return fxdtMapper.updateReverseByPrimaryKey(record);
	}

    @Override
    public void updateBatch(List<Fxdt> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.FxdtMapper.updateByPrimaryKeySelective", list);
    }

    @Override
    public void batchInsert(List<Fxdt> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.FxdtMapper.insert", list);
    }

}
