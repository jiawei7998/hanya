package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SwapWksBean;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.SwdhService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;


/**
 * 互换交易冲销转换方法
 *
 * @author shenzl
 * @date 2020/02/19
 */
@Component
public class SwapReverse {

    @Autowired
    private SwdhService swdhService;

    void DataCheck(SwapWksBean swap) {
        // 1.判断拆借交易对象是否存在
        if (null == swap) {
            JY.raise("%s:%s,请补充完整SwapWksBean对象", WksErrorCode.IBO_NULL.getErrCode(), WksErrorCode.IBO_NULL.getErrMsg());
        }
        // 2.判断拆借交易机构是否存在
        if (StringUtils.isEmpty(swap.getInstId())) {
            JY.raise("%s:%s,请填写SwapWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
                    WksErrorCode.BR_NOT_FOUND.getErrMsg());
        }
        // 3.判断业务编号是否存在
        Swdh swdh = swdhService.selectByTradeNo(swap.getInstId(), swap.getTradeNo(),"R");
        if (StringUtils.isEmpty(swap.getTradeNo()) || null == swdh) {
            JY.raise("%s:%s,请填写SwapWksBean.TradeNo,For.[TradeNo=TRD20200101000001]",
                    WksErrorCode.IRS_NULL.getErrCode(), WksErrorCode.IRS_NULL.getErrMsg());
        }
    }

    /**
     * 冲销
     *
     * @param swdh
     * @return
     */
    public Swdh DataTrans(Swdh swdh, BrpsService brpsService) {
        Brps brps = brpsService.selectByPrimaryKey(swdh.getBr());
        swdh.setRevdate(brps.getBranprcdate());// 撤销日期
        swdh.setRevoper(OpicsConstant.SWAP.TRAD_NO);// 撤销操作员
        swdh.setRevreason("99");// 撤销原因代码
        swdh.setRevtext("");// 撤销原因
        swdh.setUpdatecounter(swdh.getUpdatecounter() + 1);// 修改次数
        swdh.setLstmntdte(Calendar.getInstance().getTime());// 最后修改日期
        return swdh;
    }
}
