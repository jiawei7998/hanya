package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Secs;
import com.singlee.financial.wks.mapper.opics.SecsMapper;
import com.singlee.financial.wks.service.opics.SecsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
@Service
public class SecsServiceImpl implements SecsService {

    @Resource
    private SecsMapper secsMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String secid,String seq,Date ipaydate) {
        return secsMapper.deleteByPrimaryKey(secid,seq,ipaydate);
    }

    @Override
    public int insert(Secs record) {
        return secsMapper.insert(record);
    }

    @Override
    public int insertSelective(Secs record) {
        return secsMapper.insertSelective(record);
    }

    @Override
    public Secs selectByPrimaryKey(String secid,String seq,Date ipaydate) {
        return secsMapper.selectByPrimaryKey(secid,seq,ipaydate);
    }

    @Override
    public int updateByPrimaryKeySelective(Secs record) {
        return secsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Secs record) {
        return secsMapper.updateByPrimaryKey(record);
    }

	@Override
	public void batchInsert(List<Secs> list) {
		if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.SecsMapper.insert", list);
	}

}
