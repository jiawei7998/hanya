package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Cusi;
public interface CusiService{


    int deleteByPrimaryKey(String custidtype,String custaltid);

    int insert(Cusi record);

    int insertSelective(Cusi record);

    Cusi selectByPrimaryKey(String custidtype,String custaltid);

    int updateByPrimaryKeySelective(Cusi record);

    int updateByPrimaryKey(Cusi record);

}
