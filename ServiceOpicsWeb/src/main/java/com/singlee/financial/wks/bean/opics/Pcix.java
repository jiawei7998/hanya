package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import lombok.Data;

@Data
public class Pcix implements Serializable {
    private String br;

    private String product;

    private String type;

    private String dealno;

    private String seq;

    private String pcc;

    private String acctno;

    private String bic;

    private String c1;

    private String c2;

    private String c3;

    private String c4;
    
    private String c5;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getPcc() {
		return pcc;
	}


	public void setPcc(String pcc) {
		this.pcc = pcc;
	}


	public String getAcctno() {
		return acctno;
	}


	public void setAcctno(String acctno) {
		this.acctno = acctno;
	}


	public String getBic() {
		return bic;
	}


	public void setBic(String bic) {
		this.bic = bic;
	}


	public String getC1() {
		return c1;
	}


	public void setC1(String c1) {
		this.c1 = c1;
	}


	public String getC2() {
		return c2;
	}


	public void setC2(String c2) {
		this.c2 = c2;
	}


	public String getC3() {
		return c3;
	}


	public void setC3(String c3) {
		this.c3 = c3;
	}


	public String getC4() {
		return c4;
	}


	public void setC4(String c4) {
		this.c4 = c4;
	}


	public String getC5() {
		return c5;
	}


	public void setC5(String c5) {
		this.c5 = c5;
	}


	private static final long serialVersionUID = 1L;
}