package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.FxdItemWksBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;
import com.singlee.financial.wks.expand.SwiftMethod;
import com.singlee.financial.wks.mapper.opics.PcixMapper;
import com.singlee.financial.wks.service.opics.BicoService;
import com.singlee.financial.wks.service.opics.CustService;
import com.singlee.financial.wks.service.opics.PcixService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class PcixServiceImpl implements PcixService {

	@Resource
	private PcixMapper pcixMapper;
	@Resource
	BatchDao batchDao;
	@Autowired
    private CustService custService;
	@Autowired
	private BicoService bicoService;
	
	private Logger logger = Logger.getLogger(PcixServiceImpl.class);
	
	@Override
	public int deleteByPrimaryKey(String br, String product, String type, String dealno, String seq, String pcc) {
		return pcixMapper.deleteByPrimaryKey(br, product, type, dealno, seq, pcc);
	}

	@Override
	public int insert(Pcix record) {
		return pcixMapper.insert(record);
	}

	@Override
	public int insertSelective(Pcix record) {
		return pcixMapper.insertSelective(record);
	}

	@Override
	public Pcix selectByPrimaryKey(String br, String product, String type, String dealno, String seq, String pcc) {
		return pcixMapper.selectByPrimaryKey(br, product, type, dealno, seq, pcc);
	}

	@Override
	public int updateByPrimaryKeySelective(Pcix record) {
		return pcixMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(Pcix record) {
		return pcixMapper.updateByPrimaryKey(record);
	}

	@Override
	public void updateBatch(List<Pcix> list) {
		if (null == list || list.size() == 0) {
			return;
		}
		batchDao.batch("com.singlee.financial.wks.mapper.opics.PcixMapper.updateByPrimaryKeySelective", list);
	}

	@Override
	public void batchInsert(List<Pcix> list) {
		if (null == list || list.size() == 0) {
			return;
		}
		batchDao.batch("com.singlee.financial.wks.mapper.opics.PcixMapper.insert", list);
	}

	/**
	 * 方法重载.构建付款报文对手方清算信息(PCIX)
	 * 
	 * @param br
	 * @param product
	 * @param type
	 * @param dealno
	 * @param seq
	 * @param pcc
	 * @param acctno
	 * @param bic
	 * @param c1
	 * @param c2
	 * @param c3
	 * @param c4
	 * @return
	 */
	private Pcix builder(String br, String product, String type, String dealno, String seq, String pcc, String acctno,
			String bic, String c1, String c2, String c3, String c4,String c5) {
		// 付款报文对手方清算信息
		Pcix pcix = new Pcix();
		// 部门
		pcix.setBr(br);
		// 产品代码
		pcix.setProduct(product);
		// 业务类型
		pcix.setType(type);
		// 交易编号
		pcix.setDealno(dealno);
		// 交易序号
		pcix.setSeq(seq);
		// 报文域场景
		pcix.setPcc(pcc);
		// 账号或者行号
		pcix.setAcctno(acctno);
		// SWIFTCODE
		pcix.setBic(bic);
		// 报文第一行信息35个字符
		pcix.setC1(c1);
		// 报文第二行信息35个字符
		pcix.setC2(c2);
		// 报文第三行信息35个字符
		pcix.setC3(c3);
		// 报文第四行信息35个字符
		pcix.setC4(c4);

		return pcix;
	}
	
	/**
	 * 方法重载.初始化PCIX对象,根据业务对象设置57A、58A、56A的字段值
	 * @param pcix
	 * @param pcc
	 * @param bicCode
	 * @param bicType
	 * @param smeans
	 * @return
	 */
	private Pcix builder(String br, String product, String type, String dealno, String seq,String pcc, String acctno,String bicCode, String bicType, String smeans) {
		String bicSwfitCode = null;
		String c1 = null;
		String beAcctNo= null;
		
		if ("NOS".equalsIgnoreCase(smeans)) {
			bicSwfitCode = bicCode;
			c1 = "";
			Bico bico = bicoService.selectByPrimaryKey(bicCode);
			if(null == bico && null != bicCode && StringUtils.trim(bicCode).length() > 0) {
				throw new RuntimeException(String.format("报文%s项对应的SWIFTCODE[%s]不存在", pcc,bicCode));
			}
		} else {
			bicSwfitCode = "";
			if(null != bicCode && StringUtils.trim(bicCode).length() > 0) {
				c1 = "/" + bicType + "/" + bicCode;
			}
		}
		
		if(null != acctno && StringUtils.trim(acctno).length() > 0) {
			beAcctNo = "/"+acctno;
		}else {
			beAcctNo = "";
		}
		
		return builder(br,product,type,dealno,seq,pcc,beAcctNo,bicSwfitCode,c1, "","","","");
	}
	
	/**
	 * 方法重载.初始化PCIX对象,根据业务对象设置57A、58A、56A的字段值
	 * @param br
	 * @param product
	 * @param type
	 * @param dealno
	 * @param seq
	 * @param pcc
	 * @param bicCode
	 * @param bicType
	 * @param smeans
	 * @return
	 */
	private Pcix builder(String br, String product, String type, String dealno, String seq,String pcc,String bicCode, String bicType, String smeans) {
		return builder(br, product,type,dealno,seq,pcc, "",bicCode,bicType,smeans);
	}

	@Override
	public List<Pcix> builder(Dldt dl, List<Schd> schds, SettleInfoWksBean acct) {
		List<Pcix> pcixs = new ArrayList<Pcix>();
		if (null == dl) {
			JY.raise("拆借交易头信息为空!");
		}

		if (null == schds || schds.size() < 2) {
			JY.raise("拆借交易现金流对象不能小于两条!");
		}

		if (null == acct) {
			JY.raise("拆借交易清算信息为空!");
		}
		if(StringUtils.equals(dl.getCcy(), OpicsConstant.BRRD.LOCAL_CCY)) {//cny直接跳出
			return pcixs;
		}

		for (Schd schd : schds) {
			// 收款无记录
			if (!(schd.getTotpayamt().doubleValue() > 0 && dl.getVdate().compareTo(schd.getIpaydate()) == 0)) {
				continue;
			}
			// 添加付款路径账户行57A/57J
			addPcix(pcixs,builder(schd.getBr(), schd.getProduct(), schd.getProdtype(), schd.getDealno(),
							schd.getSeq(), OpicsConstant.PCIX.PAYMENT_AW,acct.getGuestCcyAwBiccode(),
							OpicsConstant.PCIX.FIELD_57J, schd.getSmeans()));

			// 添加付款路径受益人578/58J
			addPcix(pcixs,builder(schd.getBr(), schd.getProduct(), schd.getProdtype(), schd.getDealno(),
							schd.getSeq(), OpicsConstant.PCIX.PAYMENT_BE, acct.getGuestCcyBeAcctno(),acct.getGuestCcyBeBiccode(),
							OpicsConstant.PCIX.FIELD_58J, schd.getSmeans()));

			// 添加付款路径中间行56A/56J
			addPcix(pcixs,builder(schd.getBr(), schd.getProduct(), schd.getProdtype(), schd.getDealno(),
							schd.getSeq(), OpicsConstant.PCIX.PAYMENT_IN,acct.getGuestCcyInBiccode(),
							OpicsConstant.PCIX.FIELD_56J, schd.getSmeans()));
		}
		return pcixs;
	}
	

	/**
	 * 判断是否有数据,有数据则添加信息,没有数据则不添加
	 * 
	 * @param pcixs
	 * @param pcix
	 */
	private void addPcix(List<Pcix> pcixs, Pcix pcix) {

		if ((pcix.getBic() == null || pcix.getBic().trim().length() == 0) && 
				(pcix.getC1() == null || pcix.getC1().trim().length() == 0)) {
			return;
		} 

		pcixs.add(pcix);
	}

	@Override
	public List<Pcix> builder(Fxdh fxdh, FxdItemWksBean item) {
		List<Pcix> pcixs = new ArrayList<Pcix>();
		String beBic = null;
		String awBic = null;
		String beAcctNo = null;
		String inBic = null;
		String smeans = null;
		String ccy = null;
		
		if (null == fxdh || null == item) {
			JY.raise("外汇交易集合对象为空!");
		}
		
		// 当报文处理方法不是启用或者收付款报文启用状态,PCIX表不处理
		if (!SwiftMethod.ENABLE.equals(item.getSwiftMethod()) && !SwiftMethod.PAYANDREC.equals(item.getSwiftMethod())) {
			logger.info("报文处理方法为:[ " + item.getSwiftMethod() + " ]系统不生成MT202报文");
			return pcixs;
		}

		// 循环交易处理
		SettleInfoWksBean sett = item.getSettleAcct();
		if (null == item.getSettleAcct()) {
			JY.raise("外汇交易清算对象为空!");
		}
		logger.debug("组装PCIX的付款清算信息：" + sett);

		Cust cust = custService.selectByPrimaryKey(StringUtils.trim(fxdh.getCust()));
		if (null == cust || StringUtils.trim(cust.getBic()).length() == 0) {
			throw new RuntimeException("交易对手[" + fxdh.getCust() + "]的SWIFTCODE为空");
		}
		if ("P".equalsIgnoreCase(fxdh.getPs())) {
			ccy = fxdh.getCtrccy();
			smeans = fxdh.getCtrsmeans();
			if ("CNY".equalsIgnoreCase(ccy)) {
				beBic = cust.getBic();
				awBic = sett.getGuestCtrBeAcctno();
				beAcctNo = "";
			} else {
				beBic = sett.getGuestCtrBeBiccode();
				awBic = sett.getGuestCtrAwBiccode();
				beAcctNo = sett.getGuestCtrBeAcctno();
				inBic = sett.getGuestCtrInBiccode();
			}
		} else if ("S".equalsIgnoreCase(fxdh.getPs())) {
			ccy = fxdh.getCcy();
			smeans = fxdh.getCcysmeans();
			if ("CNY".equalsIgnoreCase(ccy)) {
				beBic = cust.getBic();
				awBic = sett.getGuestCcyBeAcctno();
				beAcctNo = "";
			} else {
				beBic = sett.getGuestCcyBeBiccode();
				awBic = sett.getGuestCcyAwBiccode();
				beAcctNo = sett.getGuestCcyBeAcctno();
				inBic = sett.getGuestCcyInBiccode();
			}
		} else {
			throw new RuntimeException("交易方向[" + fxdh.getPs() + "]不支持");
		} // end if
		
		builder(pcixs, fxdh, beBic, beAcctNo, awBic,inBic,smeans);
		return pcixs;
	}
	

	/**
	 * 方法已重载.添加外汇交易外币付款信息
	 * @param fxdh
	 * @param acct
	 * @return
	 */
	private void builder(List<Pcix> pcixs,Fxdh fxdh,String beBicCode,String beActtno,String awBicCode,String inBicCdoe,String smeans) {
		
		if (null == fxdh) {
			JY.raise("外汇交易对象为空!");
		}
		
		logger.debug(String.format("外汇交易组装PCIX信息,beBicCode=%s,beActtno=%s,awBicCode=%s,inBicCdoe=%s", beBicCode,beActtno,awBicCode,inBicCdoe));

		if (null == beBicCode || StringUtils.trim(beBicCode).length() == 0) {
			JY.raise("外汇交易Beneficiary SwiftCode为空!");
		}
		
		if (null == awBicCode || StringUtils.trim(awBicCode).length() == 0) {
			JY.raise("外汇交易Account with SwiftCode为空!");
		}
		
		if (null == smeans || StringUtils.trim(smeans).length() == 0) {
			JY.raise("外汇交易清算方式为空!");
		}
		
		// 添加交易对手账户行
		addPcix(pcixs, builder(fxdh.getBr(), fxdh.getProdcode(), fxdh.getProdtype(), fxdh.getDealno(), fxdh.getSeq(), OpicsConstant.PCIX.PAYMENT_AW, 
				awBicCode, OpicsConstant.PCIX.FIELD_57J, smeans));

		// 添加交易对手信息
		addPcix(pcixs, builder(fxdh.getBr(), fxdh.getProdcode(), fxdh.getProdtype(), fxdh.getDealno(), fxdh.getSeq(), OpicsConstant.PCIX.PAYMENT_BE, 
				beActtno, beBicCode, OpicsConstant.PCIX.FIELD_58J,
				smeans));

		// 添加交易对手中间行
		addPcix(pcixs, builder(fxdh.getBr(), fxdh.getProdcode(), fxdh.getProdtype(), fxdh.getDealno(), fxdh.getSeq(), OpicsConstant.PCIX.PAYMENT_IN, 
				inBicCdoe, OpicsConstant.PCIX.FIELD_56J, smeans));

	}

	@Override
	public List<Pcix> builder(Otdt otdt, SettleInfoWksBean acct) {
		List<Pcix> pcixs = new ArrayList<Pcix>();
		if (null == otdt) {
			JY.raise("期权交易对象为空!");
		}

		if (null == acct) {
			JY.raise("期权交易清算对象为空!");
		}
		// 添加交易对手账户行
		addPcix(pcixs,builder(otdt.getBr(), otdt.getProduct(), otdt.getProdtype(), otdt.getDealno(), otdt.getSeq(),
						OpicsConstant.PCIX.PAYMENT_AW, acct.getGuestCcyAwBiccode(), OpicsConstant.PCIX.FIELD_57J,
						otdt.getPremsmeans()));
		// 添加交易对手信息
		addPcix(pcixs,builder(otdt.getBr(), otdt.getProduct(), otdt.getProdtype(), otdt.getDealno(), otdt.getSeq(),
						OpicsConstant.PCIX.PAYMENT_BE,acct.getGuestCcyBeAcctno(), acct.getGuestCcyBeBiccode(), OpicsConstant.PCIX.FIELD_58J,
						otdt.getPremsmeans()));
		// 添加交易对手中间行
		addPcix(pcixs,builder(otdt.getBr(), otdt.getProduct(), otdt.getProdtype(), otdt.getDealno(), otdt.getSeq(),
						OpicsConstant.PCIX.PAYMENT_IN, acct.getGuestCcyInBiccode(), OpicsConstant.PCIX.FIELD_56J,
						otdt.getPremsmeans()));

		return pcixs;
	}

	@Override
	public List<Pcix> builder(Swdh swdh, List<Swdt> swdts, SettleInfoWksBean acct) {
		List<Pcix> pcixs = new ArrayList<Pcix>();

		if (null == swdh) {
			JY.raise("利率互换/货币互换对象为空!");
		}

		if (null == swdts || swdts.size() == 0) {
			JY.raise("利率互换/货币互换对象为空!");
		}

		// 根据收付方向判断,添加付款路径
		for (Swdt swdt : swdts) {
			
			if("INT".equalsIgnoreCase(swdt.getIntsmeans())) {
				//代客或者内部交易不出报文
				continue;
			}
			
			// 001的LEG为本方方向
			if (!"001".equalsIgnoreCase(swdt.getSeq())) {
				continue;
			}

			// 清算方式对象结构未定义好,暂无法处理
			if ("P".equalsIgnoreCase(swdt.getPayrecind())) {
				if (null == acct) {
					continue;// 不处理当前数据
				}
				// 添加交易对手账户行
				if (!(null == acct.getGuestCcyAwBiccode() || acct.getGuestCcyAwBiccode().trim().length() == 0)) {
					addPcix(pcixs,builder(swdh.getBr(), swdh.getProduct(), swdh.getProdtype(), swdh.getDealno(),
									swdh.getSeq(), OpicsConstant.PCIX.PAYMENT_AW, acct.getGuestCcyAwBiccode(),
									OpicsConstant.PCIX.FIELD_57J, swdt.getIntsmeans()));
				}

				// 添加交易对手信息
				if (!(null == acct.getGuestCcyBeBiccode() || acct.getGuestCcyBeBiccode().trim().length() == 0)) {
					addPcix(pcixs,builder(swdh.getBr(), swdh.getProduct(), swdh.getProdtype(), swdh.getDealno(),
									swdh.getSeq(), OpicsConstant.PCIX.PAYMENT_BE, acct.getGuestCcyBeBiccode(),
									OpicsConstant.PCIX.FIELD_58J, swdt.getIntsmeans()));
				}

				// 添加交易对手中间行
				if (!(null == acct.getGuestCcyInBiccode() || acct.getGuestCcyInBiccode().trim().length() == 0)) {
					addPcix(pcixs,builder(swdh.getBr(), swdh.getProduct(), swdh.getProdtype(), swdh.getDealno(),
									swdh.getSeq(), OpicsConstant.PCIX.PAYMENT_IN, acct.getGuestCcyInBiccode(),
									OpicsConstant.PCIX.FIELD_56J, swdt.getIntsmeans()));
				}

			}
		} // end for

		return pcixs;
	}

	@Override
	public List<Pcix> builder(Spsh spsh, SettleInfoWksBean acct) {
		List<Pcix> pcixs = new ArrayList<Pcix>();
		if (null == spsh) {
			JY.raise("现券买卖交易对象为空!");
		}

		if (null == acct) {
			JY.raise("现券买卖交易清算对象为空!");
		}
		// 添加交易对手账户行
		addPcix(pcixs,builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_AW, acct.getGuestCcyAwBiccode(), OpicsConstant.PCIX.FIELD_57J,
						spsh.getCcysmeans()));
		// 添加交易对手信息
		addPcix(pcixs,builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_BE, acct.getGuestCcyBeBiccode(), OpicsConstant.PCIX.FIELD_58J,
						spsh.getCcysmeans()));
		// 添加交易对手中间行
		addPcix(pcixs,builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_IN, acct.getGuestCcyInBiccode(), OpicsConstant.PCIX.FIELD_56J,
						spsh.getCcysmeans()));
		// DE
		addPcix(pcixs,builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_DE, "", "", ""));
		// OC
		addPcix(pcixs,builder(spsh.getBr(), spsh.getProduct(), spsh.getProdtype(), spsh.getDealno(), spsh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_OC, "", "", ""));

		return pcixs;
	}

	@Override
	public List<Pcix> builder(Sldh sldh, SettleInfoWksBean acct) {
		List<Pcix> pcixs = new ArrayList<Pcix>();
		if (null == sldh) {
			JY.raise("现券买卖交易对象为空!");
		}

		if (null == acct) {
			JY.raise("现券买卖交易清算对象为空!");
		}
		// 添加交易对手账户行
		addPcix(pcixs,builder(sldh.getBr(), sldh.getProduct(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_AW, acct.getGuestCcyAwBiccode(), OpicsConstant.PCIX.FIELD_57J,
						sldh.getComccysmeans()));
		// 添加交易对手信息
		addPcix(pcixs,builder(sldh.getBr(), sldh.getProduct(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_BE, acct.getGuestCcyBeBiccode(), OpicsConstant.PCIX.FIELD_58J,
						sldh.getComccysmeans()));
		// 添加交易对手中间行
		addPcix(pcixs,builder(sldh.getBr(), sldh.getProduct(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_IN, acct.getGuestCcyInBiccode(), OpicsConstant.PCIX.FIELD_56J,
						sldh.getComccysmeans()));
		// DE
		addPcix(pcixs,builder(sldh.getBr(), sldh.getProduct(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_DE, "", "", ""));
		// OC
		addPcix(pcixs,builder(sldh.getBr(), sldh.getProduct(), sldh.getProdtype(), sldh.getDealno(), sldh.getSeq(),
						OpicsConstant.PCIX.PAYMENT_OC, "", "", ""));

		return pcixs;
	}
}
