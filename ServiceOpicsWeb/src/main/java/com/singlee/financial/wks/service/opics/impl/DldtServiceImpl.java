package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.mapper.opics.DldtMapper;
import com.singlee.financial.wks.service.opics.DldtService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class DldtServiceImpl implements DldtService {

    @Resource
    private DldtMapper dldtMapper;

    @Override
    public int deleteByPrimaryKey(String br, String dealno, String seq) {
        return dldtMapper.deleteByPrimaryKey(br, dealno, seq);
    }

    @Override
    public int insert(Dldt record) {
        return dldtMapper.insert(record);
    }

    @Override
    public int insertSelective(Dldt record) {
        return dldtMapper.insertSelective(record);
    }

    @Override
    public Dldt selectByPrimaryKey(String br, String dealno, String seq) {
        return dldtMapper.selectByPrimaryKey(br, dealno, seq);
    }

    @Override
    public Dldt selectByTradeNo(String br, String dealtext) {
        return dldtMapper.selectByTradeNo(br, dealtext);
    }

    @Override
    public int updateByPrimaryKeySelective(Dldt record) {
        return dldtMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Dldt record) {
        return dldtMapper.updateByPrimaryKey(record);
    }

}
