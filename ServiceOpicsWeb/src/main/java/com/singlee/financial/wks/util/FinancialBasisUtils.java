package com.singlee.financial.wks.util;

import java.math.BigDecimal;

import com.singlee.financial.wks.expand.Basis;
import com.singlee.financial.wks.expand.Frequency;

/**
 * 计息基础工具类
 * @author chenxh
 *
 */
public class FinancialBasisUtils {

    
    public static String OPICS;
    
    static {
        OPICS="OPICS47";
    }
	/**
	 * 计算计息基础对应的天数
	 * @param bassis
	 * @param paymentFrequency
	 * @param intDays
	 * @return
	 */
	public static BigDecimal createBasis(String bassis,String paymentFrequency,int intDays) {
		String strBassis = null;
		
		if(null == bassis || bassis.trim().length() == 0) {
			throw new RuntimeException("计算计基础时,计息基础不能为空");
		}
		
		
		if(Basis.A360.name().equalsIgnoreCase(bassis)) {
			strBassis = "360";
		}else if(Basis.A365.name().equalsIgnoreCase(bassis)) {
			strBassis = "365";
		}else if(Basis.ACTUAL.name().equalsIgnoreCase(bassis)) {
			if(null == paymentFrequency || paymentFrequency.trim().length() == 0) {
				throw new RuntimeException("计算ACTUAL计息基础时,复利频率不能为空");
			}
			if(intDays == 0) {
				throw new RuntimeException("计算ACTUAL计息基础时,当期计息天数不能为零");
			}
			int frequency = 0;
			if(Frequency.A.name().equalsIgnoreCase(paymentFrequency)) {
				frequency = 1;
			}else if(Frequency.S.name().equalsIgnoreCase(paymentFrequency)) {
				frequency = 2;
			}else if(Frequency.Q.name().equalsIgnoreCase(paymentFrequency)) {
				frequency = 4;
			}else if(Frequency.M.name().equalsIgnoreCase(paymentFrequency)) {
				frequency = 12;
			}else {
				throw new RuntimeException("计息基础"+Basis.ACTUAL.name()+"不支持"+paymentFrequency+"付息频率");
			}
			strBassis = String.valueOf(intDays*frequency);
			
		}else if(Basis.ACT365.name().equalsIgnoreCase(bassis)) {
			throw new RuntimeException("暂不支持A365F计息基础的操作");
		}else if(Basis.BOND.name().equalsIgnoreCase(bassis)) {
			throw new RuntimeException("暂不支持EBOND计息基础的操作");
		}else {
			throw new RuntimeException("计息基础不支持");
		}
		
		return new BigDecimal(strBassis);
	}
}
