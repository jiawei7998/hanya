package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.bean.opics.Pcix;
import com.singlee.financial.wks.bean.opics.Schd;
import com.singlee.financial.wks.bean.opics.Sldh;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.bean.opics.Swdt;
import com.singlee.financial.wks.expand.FxdItemWksBean;
import com.singlee.financial.wks.expand.SettleInfoWksBean;

public interface PcixService {

    int deleteByPrimaryKey(String br, String product, String type, String dealno, String seq, String pcc);

    int insert(Pcix record);

    int insertSelective(Pcix record);

    Pcix selectByPrimaryKey(String br, String product, String type, String dealno, String seq, String pcc);

    int updateByPrimaryKeySelective(Pcix record);

    int updateByPrimaryKey(Pcix record);

    void updateBatch(List<Pcix> list);

    void batchInsert(List<Pcix> list);

    /**
     * 方法已重载.根据拆借业务、现金流数据构建交易对手付款清算对象(PCIX),MT202、MT320中使用
     * 
     * @param dl
     * @param schds
     * @return
     */
    List<Pcix> builder(Dldt dl, List<Schd> schds, SettleInfoWksBean acct);

    /**
     * 方法已重载.根据外汇业务数据集合构建交易对手付款清算对象(PCIX),MT202、MT300中使用
     * 
     * @param fxdh
     * @param acct
     * @return
     */
    List<Pcix> builder(Fxdh fxdh,FxdItemWksBean item);

    /**
     * 方法已重载.根据外汇期权业务数据构建交易对手付款清算对象(PCIX),MT202、MT300中使用
     * 
     * @param otdt
     * @param acct
     * @return
     */
    List<Pcix> builder(Otdt otdt, SettleInfoWksBean acct);

    /**
     * 方法已重载.根据利率互换/货币互换业务数据构建交易对手付款清算对象(PCIX),MT202、MT300中使用
     * 
     * @param swdh
     * @param swdts
     * @return
     */
    List<Pcix> builder(Swdh swdh, List<Swdt> swdts, SettleInfoWksBean acct);
    /**
     * 方法已重载.根据现券买卖业务数据构建交易对手付款清算对象(PCIX),MT202、MT300中使用
     * @param spsh
     * @param acct
     * @return
     */
    List<Pcix> builder(Spsh spsh, SettleInfoWksBean acct);
    
    List<Pcix> builder(Sldh sldh, SettleInfoWksBean acct);
}
