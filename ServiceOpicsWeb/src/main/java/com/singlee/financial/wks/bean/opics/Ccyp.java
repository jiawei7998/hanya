package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Ccyp implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -4210567778092558977L;

	private String br;

    private String ccy1;

    private String ccy2;

    private String fincntr1;

    private String fincntr2;

    private String terms;

    private String carryport;

    private String carrytrad;

    private String carrycost;

    private String quotccy;

    private String tradedccy;

    private String sortccypair;

    private String majorccyflag;

    private BigDecimal spotriskfactor8;

    private String fwdriskperiod1;

    private BigDecimal fwdriskfactor18;

    private String fwdriskperiod2;

    private BigDecimal fwdriskfactor28;

    private String fwdriskperiod3;

    private BigDecimal fwdriskfactor38;

    private String fwdriskperiod4;

    private BigDecimal fwdriskfactor48;

    private String fwdriskperiod5;

    private BigDecimal fwdriskfactor58;

    private String spotind;

    private Date lstmntdte;

    private String spotport;

    private String spottrad;

    private String spotcost;

    private String corpport;

    private String corptrad;

    private String corpcost;

    private String forwardport;

    private String forwardtrad;

    private String forwardcost;

    private String intdealport;

    private String intdealtrad;

    private String intdealcost;

    private String tellerport;

    private String tellertrad;

    private String tellercost;

    private String draftport;

    private String drafttrad;

    private String draftcost;

    private String mthendport;

    private String mthendtrad;

    private String mthendcost;

    private String bigfigdec;

    private String disaggspotind;

    private String disaggfwdind;

    private BigDecimal disaggamt;

    private String fundbaseind;

    private String spotcalccy;

    private Date pairspotdate;

    private Date date1;

    private Date date2;

    private String text1;

    private String text2;

    private BigDecimal rate110;

    private BigDecimal rate210;

    private String spdtecutofftime;

    private String deriveccy;

    private BigDecimal mult8;

    private String multterms;

    private String disaggspotcorpind;

    private String disaggfwdcorpind;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy1() {
		return ccy1;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public String getCcy2() {
		return ccy2;
	}

	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}

	public String getFincntr1() {
		return fincntr1;
	}

	public void setFincntr1(String fincntr1) {
		this.fincntr1 = fincntr1;
	}

	public String getFincntr2() {
		return fincntr2;
	}

	public void setFincntr2(String fincntr2) {
		this.fincntr2 = fincntr2;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getCarryport() {
		return carryport;
	}

	public void setCarryport(String carryport) {
		this.carryport = carryport;
	}

	public String getCarrytrad() {
		return carrytrad;
	}

	public void setCarrytrad(String carrytrad) {
		this.carrytrad = carrytrad;
	}

	public String getCarrycost() {
		return carrycost;
	}

	public void setCarrycost(String carrycost) {
		this.carrycost = carrycost;
	}

	public String getQuotccy() {
		return quotccy;
	}

	public void setQuotccy(String quotccy) {
		this.quotccy = quotccy;
	}

	public String getTradedccy() {
		return tradedccy;
	}

	public void setTradedccy(String tradedccy) {
		this.tradedccy = tradedccy;
	}

	public String getSortccypair() {
		return sortccypair;
	}

	public void setSortccypair(String sortccypair) {
		this.sortccypair = sortccypair;
	}

	public String getMajorccyflag() {
		return majorccyflag;
	}

	public void setMajorccyflag(String majorccyflag) {
		this.majorccyflag = majorccyflag;
	}

	public BigDecimal getSpotriskfactor8() {
		return spotriskfactor8;
	}

	public void setSpotriskfactor8(BigDecimal spotriskfactor8) {
		this.spotriskfactor8 = spotriskfactor8;
	}

	public String getFwdriskperiod1() {
		return fwdriskperiod1;
	}

	public void setFwdriskperiod1(String fwdriskperiod1) {
		this.fwdriskperiod1 = fwdriskperiod1;
	}

	public BigDecimal getFwdriskfactor18() {
		return fwdriskfactor18;
	}

	public void setFwdriskfactor18(BigDecimal fwdriskfactor18) {
		this.fwdriskfactor18 = fwdriskfactor18;
	}

	public String getFwdriskperiod2() {
		return fwdriskperiod2;
	}

	public void setFwdriskperiod2(String fwdriskperiod2) {
		this.fwdriskperiod2 = fwdriskperiod2;
	}

	public BigDecimal getFwdriskfactor28() {
		return fwdriskfactor28;
	}

	public void setFwdriskfactor28(BigDecimal fwdriskfactor28) {
		this.fwdriskfactor28 = fwdriskfactor28;
	}

	public String getFwdriskperiod3() {
		return fwdriskperiod3;
	}

	public void setFwdriskperiod3(String fwdriskperiod3) {
		this.fwdriskperiod3 = fwdriskperiod3;
	}

	public BigDecimal getFwdriskfactor38() {
		return fwdriskfactor38;
	}

	public void setFwdriskfactor38(BigDecimal fwdriskfactor38) {
		this.fwdriskfactor38 = fwdriskfactor38;
	}

	public String getFwdriskperiod4() {
		return fwdriskperiod4;
	}

	public void setFwdriskperiod4(String fwdriskperiod4) {
		this.fwdriskperiod4 = fwdriskperiod4;
	}

	public BigDecimal getFwdriskfactor48() {
		return fwdriskfactor48;
	}

	public void setFwdriskfactor48(BigDecimal fwdriskfactor48) {
		this.fwdriskfactor48 = fwdriskfactor48;
	}

	public String getFwdriskperiod5() {
		return fwdriskperiod5;
	}

	public void setFwdriskperiod5(String fwdriskperiod5) {
		this.fwdriskperiod5 = fwdriskperiod5;
	}

	public BigDecimal getFwdriskfactor58() {
		return fwdriskfactor58;
	}

	public void setFwdriskfactor58(BigDecimal fwdriskfactor58) {
		this.fwdriskfactor58 = fwdriskfactor58;
	}

	public String getSpotind() {
		return spotind;
	}

	public void setSpotind(String spotind) {
		this.spotind = spotind;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getSpotport() {
		return spotport;
	}

	public void setSpotport(String spotport) {
		this.spotport = spotport;
	}

	public String getSpottrad() {
		return spottrad;
	}

	public void setSpottrad(String spottrad) {
		this.spottrad = spottrad;
	}

	public String getSpotcost() {
		return spotcost;
	}

	public void setSpotcost(String spotcost) {
		this.spotcost = spotcost;
	}

	public String getCorpport() {
		return corpport;
	}

	public void setCorpport(String corpport) {
		this.corpport = corpport;
	}

	public String getCorptrad() {
		return corptrad;
	}

	public void setCorptrad(String corptrad) {
		this.corptrad = corptrad;
	}

	public String getCorpcost() {
		return corpcost;
	}

	public void setCorpcost(String corpcost) {
		this.corpcost = corpcost;
	}

	public String getForwardport() {
		return forwardport;
	}

	public void setForwardport(String forwardport) {
		this.forwardport = forwardport;
	}

	public String getForwardtrad() {
		return forwardtrad;
	}

	public void setForwardtrad(String forwardtrad) {
		this.forwardtrad = forwardtrad;
	}

	public String getForwardcost() {
		return forwardcost;
	}

	public void setForwardcost(String forwardcost) {
		this.forwardcost = forwardcost;
	}

	public String getIntdealport() {
		return intdealport;
	}

	public void setIntdealport(String intdealport) {
		this.intdealport = intdealport;
	}

	public String getIntdealtrad() {
		return intdealtrad;
	}

	public void setIntdealtrad(String intdealtrad) {
		this.intdealtrad = intdealtrad;
	}

	public String getIntdealcost() {
		return intdealcost;
	}

	public void setIntdealcost(String intdealcost) {
		this.intdealcost = intdealcost;
	}

	public String getTellerport() {
		return tellerport;
	}

	public void setTellerport(String tellerport) {
		this.tellerport = tellerport;
	}

	public String getTellertrad() {
		return tellertrad;
	}

	public void setTellertrad(String tellertrad) {
		this.tellertrad = tellertrad;
	}

	public String getTellercost() {
		return tellercost;
	}

	public void setTellercost(String tellercost) {
		this.tellercost = tellercost;
	}

	public String getDraftport() {
		return draftport;
	}

	public void setDraftport(String draftport) {
		this.draftport = draftport;
	}

	public String getDrafttrad() {
		return drafttrad;
	}

	public void setDrafttrad(String drafttrad) {
		this.drafttrad = drafttrad;
	}

	public String getDraftcost() {
		return draftcost;
	}

	public void setDraftcost(String draftcost) {
		this.draftcost = draftcost;
	}

	public String getMthendport() {
		return mthendport;
	}

	public void setMthendport(String mthendport) {
		this.mthendport = mthendport;
	}

	public String getMthendtrad() {
		return mthendtrad;
	}

	public void setMthendtrad(String mthendtrad) {
		this.mthendtrad = mthendtrad;
	}

	public String getMthendcost() {
		return mthendcost;
	}

	public void setMthendcost(String mthendcost) {
		this.mthendcost = mthendcost;
	}

	public String getBigfigdec() {
		return bigfigdec;
	}

	public void setBigfigdec(String bigfigdec) {
		this.bigfigdec = bigfigdec;
	}

	public String getDisaggspotind() {
		return disaggspotind;
	}

	public void setDisaggspotind(String disaggspotind) {
		this.disaggspotind = disaggspotind;
	}

	public String getDisaggfwdind() {
		return disaggfwdind;
	}

	public void setDisaggfwdind(String disaggfwdind) {
		this.disaggfwdind = disaggfwdind;
	}

	public BigDecimal getDisaggamt() {
		return disaggamt;
	}

	public void setDisaggamt(BigDecimal disaggamt) {
		this.disaggamt = disaggamt;
	}

	public String getFundbaseind() {
		return fundbaseind;
	}

	public void setFundbaseind(String fundbaseind) {
		this.fundbaseind = fundbaseind;
	}

	public String getSpotcalccy() {
		return spotcalccy;
	}

	public void setSpotcalccy(String spotcalccy) {
		this.spotcalccy = spotcalccy;
	}

	public Date getPairspotdate() {
		return pairspotdate;
	}

	public void setPairspotdate(Date pairspotdate) {
		this.pairspotdate = pairspotdate;
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	public String getText1() {
		return text1;
	}

	public void setText1(String text1) {
		this.text1 = text1;
	}

	public String getText2() {
		return text2;
	}

	public void setText2(String text2) {
		this.text2 = text2;
	}

	public BigDecimal getRate110() {
		return rate110;
	}

	public void setRate110(BigDecimal rate110) {
		this.rate110 = rate110;
	}

	public BigDecimal getRate210() {
		return rate210;
	}

	public void setRate210(BigDecimal rate210) {
		this.rate210 = rate210;
	}

	public String getSpdtecutofftime() {
		return spdtecutofftime;
	}

	public void setSpdtecutofftime(String spdtecutofftime) {
		this.spdtecutofftime = spdtecutofftime;
	}

	public String getDeriveccy() {
		return deriveccy;
	}

	public void setDeriveccy(String deriveccy) {
		this.deriveccy = deriveccy;
	}

	public BigDecimal getMult8() {
		return mult8;
	}

	public void setMult8(BigDecimal mult8) {
		this.mult8 = mult8;
	}

	public String getMultterms() {
		return multterms;
	}

	public void setMultterms(String multterms) {
		this.multterms = multterms;
	}

	public String getDisaggspotcorpind() {
		return disaggspotcorpind;
	}

	public void setDisaggspotcorpind(String disaggspotcorpind) {
		this.disaggspotcorpind = disaggspotcorpind;
	}

	public String getDisaggfwdcorpind() {
		return disaggfwdcorpind;
	}

	public void setDisaggfwdcorpind(String disaggfwdcorpind) {
		this.disaggfwdcorpind = disaggfwdcorpind;
	}
    
    
}