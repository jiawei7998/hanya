package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Swdt;
import com.singlee.financial.wks.mapper.opics.SwdtMapper;
import com.singlee.financial.wks.service.opics.SwdtService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class SwdtServiceImpl implements SwdtService {

    @Resource
    private SwdtMapper swdtMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br,String dealno,String seq,String dealind) {
        return swdtMapper.deleteByPrimaryKey(br,dealno,seq,dealind);
    }

    @Override
    public int insert(Swdt record) {
        return swdtMapper.insert(record);
    }

    @Override
    public int insertSelective(Swdt record) {
        return swdtMapper.insertSelective(record);
    }

    @Override
    public Swdt selectByPrimaryKey(String br,String dealno,String seq,String dealind) {
        return swdtMapper.selectByPrimaryKey(br,dealno,seq,dealind);
    }

    @Override
    public int updateByPrimaryKeySelective(Swdt record) {
        return swdtMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Swdt record) {
        return swdtMapper.updateByPrimaryKey(record);
    }

	@Override
	public void batchInsert(List<Swdt> list) {
		// TODO Auto-generated method stub
		if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.SwdtMapper.insert", list);
	}

	@Override
	public List<Swdt> selectByDealno(String br, String dealno) {
		return swdtMapper.selectByDealno(br, dealno);
	}

	@Override
	public int updateByTerminate(List<Swdt> list) {
		list.stream().forEach(swdt -> {
			swdtMapper.updateByTerminate(swdt);
		});
		
		return 0;
	}

}
