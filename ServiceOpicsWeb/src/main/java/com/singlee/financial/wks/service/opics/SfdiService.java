package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Sfdi;
import com.singlee.financial.wks.bean.opics.Spsh;
public interface SfdiService{


    int deleteByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct);

    int insert(Sfdi record);

    int insertSelective(Sfdi record);

    Sfdi selectByPrimaryKey(String br,String cno,String delrecind,String product,String prodtype,String ccy,String safekeepacct);

    int updateByPrimaryKeySelective(Sfdi record);

    int updateByPrimaryKey(Sfdi record);

    void batchInsert(List<Sfdi> list);
    
    List<Sfdi> builder(Spsh spsh);
}
