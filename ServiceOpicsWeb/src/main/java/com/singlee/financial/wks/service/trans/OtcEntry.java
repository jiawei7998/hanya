package com.singlee.financial.wks.service.trans;


import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.OtcWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

/**
 * 期权录入交易转换方法
 *
 * @author shenzl
 * @date 2020/02/19
 */
@Component
public class OtcEntry {
    @Autowired
    private BrpsService brpsService;
    @Autowired
    private TypeService typeService;
    @Autowired
    private PortService portService;
    @Autowired
    private CostService costService;
    @Autowired
    private CustService custService;
    @Autowired
    private TradService tradService;
    @Autowired
    private NostService nostService;
    @Autowired
    private CcodService ccodService;

    /**
     * 期权交易校验
     *
     * @param otc
     */
    public void DataCheck(OtcWksBean otc) {
        // 1.判断拆借交易对象是否存在
        if (null == otc) {
            JY.raise("%s:%s,请补充完整OtcWksBean对象", WksErrorCode.IBO_NULL.getErrCode(), WksErrorCode.IBO_NULL.getErrMsg());
        }
        // 2.判断拆借交易机构是否存在
        if (StringUtils.isEmpty(otc.getInstId())) {
            JY.raise("%s:%s,请填写OtcWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NULL.getErrCode(),
                    WksErrorCode.BR_NULL.getErrMsg());
        }
        // 3.查询分支/机构是否存在
        Brps brps = brpsService.selectByPrimaryKey(otc.getInstId());
        if (StringUtils.isEmpty(otc.getInstId()) || null == brps) {
            JY.raise("%s:%s,请正确输入OtcWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
                    WksErrorCode.BR_NOT_FOUND.getErrMsg());
        }
        // 4.检查产品类型DESK字段拆分PROD_TYPE_PORT
        String[] book = StringUtils.split(otc.getBook(), "_");
        if (StringUtils.isEmpty(otc.getBook()) || book.length != 3) {
            JY.raise("%s:%s,请输入正确格式OtcWksBean.desk,For.[desk=MM_BR_100]", WksErrorCode.DESK_FORM_ERR.getErrCode(),
                    WksErrorCode.DESK_FORM_ERR.getErrMsg());
        }
        Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
        if (StringUtils.isEmpty(book[0]) || StringUtils.isEmpty(book[1]) || null == type) {
            JY.raise("%s:%s,请输入正确格式OtcWksBean.desk,For.[desk=MM_BR_100]", WksErrorCode.DESK_NOT_FOUND.getErrCode(),
                    WksErrorCode.DESK_NOT_FOUND.getErrMsg());
        }
        Port port = portService.selectByPrimaryKey(otc.getInstId(), otc.getDesk());// 投组组合
        if (StringUtils.isEmpty(otc.getDesk()) || null == port) {
            JY.raise("%s:%s,请输入正确格式OtcWksBean.desk,For.[desk=PORT]", WksErrorCode.DESK_PORT_ERR.getErrCode(),
                    WksErrorCode.DESK_PORT_ERR.getErrMsg());
        }
        // 5.检查成本中心
        Cost cost = costService.selectByPrimaryKey(book[2]);
        if (StringUtils.isEmpty(book[2]) || null == cost) {
            JY.raise("%s:%s,请输入正确格式OtcWksBean.book,For.[desk=MM_BR_100,或查询RATE表]", WksErrorCode.BOOK_NOT_FOUND.getErrCode(),
                    WksErrorCode.BOOK_NOT_FOUND.getErrMsg());
        }
        // 6.检查交易对手
        Cust cust = custService.selectByPrimaryKey(otc.getCno());
        if (StringUtils.isEmpty(otc.getCno()) || null == cust) {
            JY.raise("%s:%s,请输入正确格式OtcWksBean.cno,For.[cno=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(),
                    WksErrorCode.CUST_NOT_FOUND.getErrMsg());
        }
        // 7.检查交易员
        Trad trad = tradService.selectByPrimaryKey(otc.getInstId(), otc.getTrad());
        if (StringUtils.isNotEmpty(otc.getTrad()) && null == trad) {
            JY.raise("%s:%s,请输入正确格式OtcWksBean.trader,For.[trader=TRAD]", WksErrorCode.TRAD_NOT_FOUND.getErrCode(),
                    WksErrorCode.TRAD_NOT_FOUND.getErrMsg());
        }
        // 8.校验费用币种是否存在
        Ccod ccod = ccodService.selectByPrimaryKey(otc.getFeesCcy());
        if (StringUtils.isEmpty(otc.getCcy()) || null == ccod) {
            JY.raise("%s:%s,请输入正确格式OtcWksBean.feeCcy,For.[feeCcy=CNY]", WksErrorCode.CCY_NOT_FOUND.getErrCode(),
                    WksErrorCode.CCY_NOT_FOUND.getErrMsg());
        }
        //7.检查本方清算账户
        String feeBic = otc.getSettleAcct().getHostCcyAwBiccode();
        List<String> intAccts = nostService.selectNosByCust(otc.getInstId(), otc.getFeesCcy(), feeBic);
        if (StringUtils.isEmpty(feeBic) || null == intAccts || intAccts.size() == 0) {
            JY.raise(
                    "%s:%s,请输入正确格式OtcWksBean.settleAcct.hostCcyAwBiccode,For.[hostCcyAwBiccode=BKCHCNBJXXX]",
                    WksErrorCode.HOST_BIC_NOT_FOUND.getErrCode(), WksErrorCode.HOST_BIC_NOT_FOUND.getErrMsg());
        }
    }

    /**
     * 静态数据组装
     *
     * @param otdt
     * @return
     */
    public Otdt DataTrans(Otdt otdt) {
        otdt.setSeq(OpicsConstant.OTC.SEQ_ZERO);// 默认
        otdt.setVerind(OpicsConstant.OTC.VER_IND);// 复核状态：1
        otdt.setIoper(OpicsConstant.OTC.TRAD_NO);// 经办用户：SYS1
        otdt.setVoper(OpicsConstant.OTC.TRAD_NO);// 复核用户：SYS1
        otdt.setPremauthind(OpicsConstant.PCIX.AUTH_IND);// 期权费复核标识：1
        otdt.setPremauthoper(OpicsConstant.OTC.TRAD_NO);// 期权费复核用户：SYS1
        otdt.setPremsoper(OpicsConstant.OTC.TRAD_NO);// 期权费经办用户：SYS1
        // OPICS默认值
        otdt.setCutoff(OpicsConstant.OTC.CUT_OFF);// 行权市场时间：固定值3:00 PM
        otdt.setLocation(OpicsConstant.OTC.LOCATION);// 行权市场：固定值BEIJING
        otdt.setLstmntdate(Calendar.getInstance().getTime());// BRPS. BRANPRCDATE
        otdt.setTerms(null);// 根据CCYP表中当前货币对的信息查询TERMS的值，乘除关系
        otdt.setCcybterms(null);// 根据CCYP表中当前货币对的信息查询TERMS的值，乘除关系
        otdt.setBrprcindte(null);// BRPS. BRANPRCDATE
        otdt.setOrigprincipal(null);// CCYPRIN
        otdt.setInputtime(DateUtil.getCurrentTimeAsString());// 当前系统时间16:15:48
        otdt.setPlmethod(OpicsConstant.OTC.PLMETHOD);// 损益法
        otdt.setPsind(OpicsConstant.OTC.PSIND);// N 配对标志
        otdt.setMathsource(OpicsConstant.OTC.MATHSOURCE);// FINCAD
        otdt.setTrigind(OpicsConstant.OTC.TRIGIND);// 触碰标志
        otdt.setExerind(OpicsConstant.OTC.EXERIND);// 行权标志
        otdt.setSiind(OpicsConstant.OTC.SIIND);// 清算指令标志
        otdt.setTenor(OpicsConstant.OTC.TENOR);// 与账务有关
        otdt.setDealsrce(OpicsConstant.OTC.DEALSRCE);// 交易标志
        otdt.setSalescredccy(OpicsConstant.OTC.SALESCREDCCY);// 销售信用货币
        otdt.setFeeseq(OpicsConstant.OTC.FEESEQ);// 费用序号
        otdt.setMarketvalind(OpicsConstant.OTC.MARKETVALIND);// 市值指标
        otdt.setTaxstatus(OpicsConstant.OTC.TAXSTATUS);// 税务状况
        otdt.setOrigpremrate8(BigDecimal.ZERO);// 原始保险费率
        otdt.setCtrbrate8(BigDecimal.ZERO);// 对应币种基准汇率
        otdt.setAvgrate8(BigDecimal.ZERO);// 平均费率
        otdt.setAvgstke8(BigDecimal.ZERO);// 平均执行价格
        otdt.setBinary(BigDecimal.ZERO);// 二元支付金额
        otdt.setCcyprem(BigDecimal.ZERO);// 货币溢价
        otdt.setCcypremamt(BigDecimal.ZERO);// 货币溢价金额
        otdt.setCcyrevalamt(BigDecimal.ZERO);// 货币重估
        otdt.setCcypremrate8(BigDecimal.ZERO);// 货币溢价率
        otdt.setContsize(BigDecimal.ZERO);// 合同规模
        otdt.setCtrprem(BigDecimal.ZERO);// 对应货币溢价
        otdt.setCtrrevalamt(BigDecimal.ZERO);// 对应货币重估
        otdt.setDkitrig18(BigDecimal.ZERO);// Double Knock In Trigger 1 下限
        otdt.setDkitrig28(BigDecimal.ZERO);// Double Knock In Trigger 2 上限
        otdt.setDkotrig18(BigDecimal.ZERO);// Double Knockout Trigger 1 下线
        otdt.setDkotrig28(BigDecimal.ZERO);// Double Knockout Trigger 2 上线
        otdt.setFeeamt(BigDecimal.ZERO);// 费用金额
        otdt.setKitrig8(BigDecimal.ZERO);// Knock In Trigger
        otdt.setKotrig8(BigDecimal.ZERO);// Knock Out Trigger
        otdt.setQty(BigDecimal.ZERO);// 数额
        otdt.setQuanto8(BigDecimal.ZERO);// 外汇兑换率是多少
        otdt.setCcyintrate8(BigDecimal.ZERO);// 货币汇率
        otdt.setCtrintrate8(BigDecimal.ZERO);// 对应货币汇率
        otdt.setRate28(BigDecimal.ZERO);// Rate 2
        otdt.setRate38(BigDecimal.ZERO);// Rate 3
        otdt.setAmt1(BigDecimal.ZERO);// Amount 1
        otdt.setAmt2(BigDecimal.ZERO);// Amount 2
        otdt.setSalescredamt(BigDecimal.ZERO);// 销售信用额
        otdt.setRebateamt(BigDecimal.ZERO);// 返利金额
        otdt.setClosingrate8(BigDecimal.ZERO);// 收盘价
        otdt.setRevqty(BigDecimal.ZERO);// 冲销数量
        otdt.setRevprem(BigDecimal.ZERO);// 冲销溢价
        otdt.setRevprincipal(BigDecimal.ZERO);// 冲销本金
        otdt.setUndprice8(BigDecimal.ZERO);// 标的价格
        otdt.setCapprice8(BigDecimal.ZERO);// 最高限价
        otdt.setDiscprice8(BigDecimal.ZERO);// 折扣价
        otdt.setParticper8(BigDecimal.ZERO);// 参与百分比
        otdt.setLinkprinamt(BigDecimal.ZERO);// 链接本金金额
        otdt.setExponent(BigDecimal.ZERO);// 指数
        otdt.setDiscper8(BigDecimal.ZERO);// 贴现率
        otdt.setOrigqty(BigDecimal.ZERO);// 原始数额
        otdt.setSchedid(BigDecimal.ZERO);// 计划标识符
        otdt.setBasketid(BigDecimal.ZERO);// Basket Identifier
        otdt.setWtaxamt(BigDecimal.ZERO);// 预缴税金金额
        otdt.setTaxnetpremamt(BigDecimal.ZERO);// 净免税额
        otdt.setCorpspread8(BigDecimal.ZERO);// 公司利差率
        otdt.setCorpspreadamt(BigDecimal.ZERO);// 公司价差金额
        otdt.setCashsettleind(OpicsConstant.OTC.ZERO);
        otdt.setUnderlyingind(OpicsConstant.OTC.ZERO);
        otdt.setUpdatecounter(OpicsConstant.OTC.UPDATE_COUNTER);
        otdt.setSalescreditind(OpicsConstant.OTC.ZERO);
        return otdt;
    }

    /**
     * 账务日期
     *
     * @param otdt
     * @param brpsService
     * @return
     */
    public Otdt DataTrans(Otdt otdt, BrpsService brpsService) {
        // 获取当前账务日期
        Brps brps = brpsService.selectByPrimaryKey(otdt.getBr());
        otdt.setBrprcindte(brps.getBranprcdate());// 当前账务日期
        otdt.setInputdate(brps.getBranprcdate());// 经办日期：BRPS. BRANPRCDATE
        otdt.setVerdate(brps.getBranprcdate());// 复核日期：BRPS. BRANPRCDATE
        otdt.setPremauthdte(brps.getBranprcdate());// 期权费复核日期：BRPS. BRANPRCDATE
        return otdt;
    }

    /**
     * 获取dealno
     *
     * @param otdt
     * @param mcfpService
     * @return
     */
    public Otdt DataTrans(Otdt otdt, McfpService mcfpService) {
        // 获取DEALNO
        Mcfp mcfp = mcfpService.selectByPrimaryKey(otdt.getBr(), OpicsConstant.OTC.PROD_CODE,
                OpicsConstant.OTC.PROD_TYPE);// 设置交易编号
        String dealno = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";// 获取
        otdt.setDealno(dealno);// 设置交易编号
        return otdt;
    }

    /**
     * 更新交易单号
     *
     * @param otdt
     * @param mcfpService
     * @param mcfp
     */
    public  void DataTrans(Otdt otdt, McfpService mcfpService, Mcfp mcfp) {
        mcfp.setBr(otdt.getBr());
        mcfp.setTraddno(StringUtils.trimToEmpty(otdt.getDealno()));
        mcfp.setProdcode(StringUtils.trimToEmpty(otdt.getProduct()));// 产品代码 去掉空格
        mcfp.setType(otdt.getProdtype());// 产品类型
        mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
    }

    /**
     * 交易数据
     *
     * @param otdt
     * @param otc
     * @param revpService
     * @return
     */
    public Otdt DataTrans(Otdt otdt, OtcWksBean otc, RevpService revpService) {
        otdt.setBr(otc.getInstId());// 机构
        String[] book = StringUtils.split(otc.getBook(), "_");
        otdt.setProduct(book[0]);// 产品代码：OTC
        otdt.setProdtype(book[1]);// 产品类型：OT
        otdt.setPort(otc.getDesk());// 投资组合：PORT
        otdt.setCost(book[2]);// 成本中心：1000000000
        otdt.setTrad(StringUtils.defaultIfEmpty(otc.getTrad(),OpicsConstant.OTC.TRAD_NO));// 交易员
        otdt.setDealdate(DateUtil.parse(otc.getDealDate()));// 交易日：
        otdt.setDealvdate(DateUtil.parse(otc.getFDDate()));// 期权费交割日：一般为DEALDATE +2个工作日
        otdt.setExpdate(DateUtil.parse(otc.getMDate()));// 到期日：
        otdt.setSettledte(DateUtil.parse(otc.getDDate()));// 行权交割日：一般为EXPDATE +2个工作日
        otdt.setCcycp(otc.getCcyCp());// 看涨/看跌：CALL-看涨；PUT-看跌
        otdt.setCtrcp(StringUtils.equals(otc.getCcyCp(), OpicsConstant.OTC.CALL_IND) ? OpicsConstant.OTC.PUT_IND
                : OpicsConstant.OTC.CALL_IND);
        otdt.setPs(otc.getPs());// 交易方向：P-买入；S-卖出
        otdt.setCcy(otc.getCcy());// 交易货币1：
        otdt.setCcyprin(otc.getCcyAmt());// 交易货币1金额：
        otdt.setStrike8(otc.getStrikeRate());// 执行价格：
        otdt.setCtrccy(otc.getCtrCcy());// 交易货币2：
        otdt.setCtrprin(otc.getCtrAmt());// 交易货币2金额：CCYPRIN* STRIKE_8
        otdt.setPremccy(otc.getFeesCcy());// 期权费货币：只能是CCY或者CTRCCY，不支持第三币种
        // 期权费金额：如果值为空或者0并且PREMCCY=CCY则为PREMRATE_8* CCYPRIN，如果PREMCCY= CTRCCY则为
        otdt.setPremamt(getPlusNegateAmt(otdt, otc.getFeesAmt()));
        otdt.setOrigpremamt(otdt.getPremamt());// 原始保险金额
        otdt.setPremauthdte(otdt.getBrprcindte());// 期权费授权日期
        otdt.setTerms("M");
        otdt.setCcybterms("M");
        otdt.setOrigprincipal(otc.getCcyAmt());
        // PREMRATE_8*CCYPRIN，如果PREMCCY= CTRCCY则为 PREMRATE_8* CTRPRIN
        // 期权费费率：如果值为空或者0并且PREMCCY=CCY则为PREMAMT/CCYPRIN；如果PREMCCY=CTRCCY则为PREMAMT/CTRPRIN；
        otdt.setPremrate8(otc.getFeesRate());
        // 期权费折本国货币汇率
        otdt.setCcybrate8(revpService.selectByPrimaryKey(otdt.getBr(), otdt.getPremccy()).getSpotrate8());
        // 期权费拆本国货币金额：PREMAMT* CCYBRATE_8
        otdt.setCcyprembamt(otdt.getPremamt().multiply(otdt.getCcybrate8()));//
        otdt.setCcypremrate8(otdt.getPremccy().equals(otdt.getCcy()) ? otdt.getPremrate8() : BigDecimal.ZERO);// 货币1期权费率;如果PREMCCY=CCY则为PREMRATE_8否则为0
        // 货币1期权费金额;如果PREMCCY=CCY则为PREMAMT否则为0
        otdt.setCcypremamt(otdt.getPremccy().equals(otdt.getCcy()) ? otdt.getPremamt() : BigDecimal.ZERO);
        // 货币2期权费率;如果PREMCCY=CTRCCY则为PREMRATE_8否则为0
        otdt.setCtrpremrate8(otdt.getPremccy().equals(otdt.getCtrccy()) ? otdt.getPremrate8() : BigDecimal.ZERO);
        // 货币2期权费金额;如果值PREMCCY=CTRCCY则为PREMAMT否则为0
        otdt.setCtrpremamt(otdt.getPremccy().equals(otdt.getCtrccy()) ? otdt.getPremamt() : BigDecimal.ZERO);
        otdt.setOtctype(otc.getOtcType());// 期权类型：FX-外汇期权
        otdt.setStyle(otc.getStyle());// 期权模式：EURO-欧式期权；AMER-美式期权
        // CCY兑 CTRCCY的汇率，如果CCY或者CTRCCY=人民币，取REVP.SPOTRATE_8
        // REVP.CCY=(CCY或者是CTRCCY不等于人民币的币种)，否则分别取CCY、CTRCCY货币汇率进行计算，CCY SPOTRATE / CTRCCY SPOTRATE
        if (otc.getCcy().equals(OpicsConstant.BRRD.BASE_CCY)) {// 判断币种
            Revp revp = revpService.selectByPrimaryKey(otdt.getBr(), otdt.getCtrccy());
            otdt.setSpotrate8(revp.getSpotrate8());// 即期汇率
        } else if (otc.getCtrCcy().equals(OpicsConstant.BRRD.BASE_CCY)) {
            Revp revp = revpService.selectByPrimaryKey(otdt.getBr(), otdt.getCcy());
            otdt.setSpotrate8(revp.getSpotrate8());// 即期汇率
        } else {
            Revp revpCtrCcy = revpService.selectByPrimaryKey(otdt.getBr(), otdt.getCtrccy());
            Revp revpCcy = revpService.selectByPrimaryKey(otdt.getBr(), otdt.getCcy());
            otdt.setSpotrate8(revpCcy.getSpotrate8().divide(revpCtrCcy.getSpotrate8(), 8, BigDecimal.ROUND_HALF_UP));// 即期汇率
        }
        otdt.setDelta8(otc.getCcyDelta());// 货币1 DELTA：
        otdt.setRate18(otc.getCtrDelta());// 货币2 DELTA：
        otdt.setVolatility8(otc.getVolatility());// 隐含波动率：将DELTA_8计算出来后，代入插值法中计算对应期限的波动率
        otdt.setCno(otc.getCno());// 交易对手：
        if (!StringUtils.equals(otdt.getPremccy(), OpicsConstant.BRRD.LOCAL_CCY)) { // 非人民币是查询NOS ACCT
            List<String> accts =
                    nostService.selectNosByCust(otdt.getBr(), otdt.getPremccy(), otc.getSettleAcct().getHostCcyAwBiccode());
            otdt.setPremsmeans(OpicsConstant.PCIX.NOSTRO_MEANS);// 期权费清算方式：信息来源于SETM表
            otdt.setPremsacct(StringUtils.trimToEmpty(accts.get(0)));// 期权费清算路径：信息来源于SETA、NOST表
        } else {
            otdt.setPremsmeans(OpicsConstant.PCIX.CANPS_MEANS);// 期权费清算方式：信息来源于SETM表
            otdt.setPremsacct(OpicsConstant.PCIX.CANPS_MEANS);// 期权费清算路径：信息来源于SETA、NOST表
        }
        return otdt;
    }

    /**
     * OTCP静态数据
     *
     * @param otcp
     * @return
     */
    public Otcp DataTrans(Otcp otcp) {
        otcp.setTheta8(BigDecimal.ZERO);
        otcp.setGamma8(BigDecimal.ZERO);
        otcp.setVega8(BigDecimal.ZERO);
        otcp.setMktpremamt(BigDecimal.ZERO);
        otcp.setMktpremperc8(BigDecimal.ZERO);
        otcp.setVolatility8(BigDecimal.ZERO);
        otcp.setRho8(BigDecimal.ZERO);
        otcp.setVanna8(BigDecimal.ZERO);
        otcp.setPhi8(BigDecimal.ZERO);
        otcp.setHedge(BigDecimal.ZERO);
        otcp.setForward8(BigDecimal.ZERO);
        otcp.setDepo8(BigDecimal.ZERO);
        otcp.setCtrdepo8(BigDecimal.ZERO);
        otcp.setHistspot8(BigDecimal.ZERO);
        otcp.setHistforward8(BigDecimal.ZERO);
        otcp.setMktpremval(BigDecimal.ZERO);
        otcp.setProfit(BigDecimal.ZERO);
        otcp.setText1(OpicsConstant.OTC.ZERO);
        otcp.setText2(OpicsConstant.OTC.ZERO);
        otcp.setText3(OpicsConstant.OTC.ZERO);
        otcp.setRate28(BigDecimal.ZERO);
        otcp.setRate38(BigDecimal.ZERO);
        otcp.setAmt1(BigDecimal.ZERO);
        otcp.setAmt2(BigDecimal.ZERO);
        otcp.setImpliedqty(BigDecimal.ZERO);
        otcp.setMarketpremamt(BigDecimal.ZERO);
        otcp.setYstmktpremamt(BigDecimal.ZERO);
        otcp.setTdyintrinsicval(BigDecimal.ZERO);
        otcp.setYstintrinsicval(BigDecimal.ZERO);
        otcp.setBasemktpremamt(BigDecimal.ZERO);
        return otcp;
    }

    /**
     * 转换OTCP数据
     *
     * @param otdt
     * @param otcp
     * @return
     */
    public Otcp DataTrans(Otdt otdt, Otcp otcp) {
        otcp.setBr(otdt.getBr());// 机构
        otcp.setDealno(otdt.getDealno());// 交易号
        otcp.setSeq(otdt.getSeq());// 默认
        otcp.setProduct(otdt.getProduct());// 产品代码
        otcp.setProdtype(otdt.getProdtype());// 产品代码
        otcp.setOtctype(otdt.getOtctype());// 期权类型
        otcp.setDelta8(otdt.getDelta8().multiply(new BigDecimal("100")));// 货币1 DELTA OTDT. DELTA_8*100
        otcp.setRate18(otdt.getRate18().multiply(new BigDecimal("100")));// 货币2 DELTA OTDT. RATE1_8*100
        otcp.setUaprice8(otdt.getSpotrate8());// 即期汇率 OTDT. SPOTRATE_8
        otcp.setCcydeltaamt(otdt.getDelta8().multiply(otdt.getCcyprin()));// 货币1 DELTA敞口 OTDT. DELTA_8*OTDT.CCYPRIN
        otcp.setCtrdeltaamt(otdt.getRate18().multiply(otdt.getCtrprin()));// 货币2 DELTA敞口 OTDT. RATE1_8*OTDT.CTRPRIN
        return otcp;
    }

    /**
     * 根据期权的交易方向判断费用的金额正负
     * <p>
     * P -买入期权 pay费用 负金额
     */
    private BigDecimal getPlusNegateAmt(Otdt otdt, BigDecimal amt) {
        if (StringUtils.equals(otdt.getPs(), OpicsConstant.PCIX.BUYIND)) {
            return amt.negate();// 取反
        } else {
            return amt.plus();// 取正
        }
    }
}
