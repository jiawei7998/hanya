package com.singlee.financial.wks.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.StateBean;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.intfc.StateService;
import com.singlee.financial.wks.service.opics.FxdhService;
import com.singlee.financial.wks.service.opics.OtdtService;
import com.singlee.financial.wks.service.opics.SwdhService;

@Service
public class StateServiceImpl implements StateService {

	@Autowired
	SwdhService swdhService;
	@Autowired
	FxdhService fxdhService;
	@Autowired
	OtdtService otdtService;

	@Override
	public SlOutBean queryEntry(StateBean sb) {
		
		// 1.判断冲销对象是否存在
		if (null == sb) {
			JY.raise("%s:%s,请补充完整StateBean对象", WksErrorCode.REVERSE_NULL.getErrCode(), WksErrorCode.REVERSE_NULL.getErrMsg());
		}
		
		// 2.判断冲销流水号是否存在
		if (StringUtils.isEmpty(sb.getTradeNo())) {
			JY.raise("%s:%s,请填写StateBean.tradeNo,For.[tradeNo=IRS******]", WksErrorCode.REVERSE_NULL.getErrCode(), WksErrorCode.REVERSE_NULL.getErrMsg());
		}

		if("N".equalsIgnoreCase(sb.getTradeAction())) {
			return queryEntryStatus(sb);
		}else if("R".equalsIgnoreCase(sb.getTradeAction())) {
			return queryReverseStatus(sb);
		}else {
			throw new RuntimeException("状态查询事件类型不支持");
		}
	}
	
	/**
	 * 查询冲销状态
	 * @param sb
	 * @return
	 */
	private SlOutBean queryReverseStatus(StateBean sb){
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "QUERY-00000", "冲销状态查询成功!");
		outBean.setTellSeqNo(sb.getTradeNo());
		switch (StringUtils.defaultIfBlank(sb.getTradeType(), "")) {
		case "IRS":
			Swdh irs = swdhService.selectByTradeNo(null, sb.getTradeNo(),"R");
			if(null == irs) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (irs.getRevdate() == null) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]冲销处理中");
				outBean.setRetStatus(RetStatusEnum.F);
			}else {
				outBean.setClientNo(irs.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}
			break;
		case "CCS":
			Swdh ccs = swdhService.selectByTradeNo(null, sb.getTradeNo(),"R");
			if(null == ccs) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (ccs.getRevdate() == null) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]冲销处理中");
				outBean.setRetStatus(RetStatusEnum.F);
			}else {
				outBean.setClientNo(ccs.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}
		case "FWD":
			List<Fxdh> fwd = fxdhService.selectByTradeNo(null, sb.getTradeNo(),"R");
			if(null == fwd || fwd.size() == 0) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (null == fwd.get(0).getRevdate()) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]冲销处理中");
				outBean.setRetStatus(RetStatusEnum.F);
			}else {
				outBean.setClientNo(fwd.get(0).getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}
			break;
		case "FSP":
			List<Fxdh> fsp = fxdhService.selectByTradeNo(null, sb.getTradeNo(),"R");
			if(null == fsp || fsp.size() != 2) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (null == fsp.get(0).getRevdate() && null == fsp.get(1).getRevdate()) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]冲销处理中");
				outBean.setRetStatus(RetStatusEnum.F);
			}else {
				outBean.setClientNo(fsp.get(0).getDealno().trim()+"/"+fsp.get(1).getDealno().trim());
				outBean.setRetStatus(RetStatusEnum.S);
			}
			break;
		case "OTC":
			Otdt otc = otdtService.selectByTradeNo(null, sb.getTradeNo(),"R");
			if(null == otc) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (otc.getRevdate() == null) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]冲销处理中");
				outBean.setRetStatus(RetStatusEnum.F);
			}else {
				outBean.setClientNo(otc.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}
			break;
		case "FXD":
			int count = 0;
			List<Fxdh> fxd = fxdhService.selectByTradeNo(null, sb.getTradeNo(),"R");
			count =  null == fxd ? 0 : fxd.size();
			if(null == fxd || fxd.size() == 0) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (count == 2 && null != fxd.get(0).getRevdate() && null != fxd.get(1).getRevdate()) {
					outBean.setClientNo(fxd.get(0).getDealno().trim()+"/"+fxd.get(1).getDealno().trim());
					outBean.setRetStatus(RetStatusEnum.S);
			}else if (count == 1 && null != fxd.get(0).getRevdate()) {
				outBean.setClientNo(fxd.get(0).getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]冲销处理中");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		case "OTC_TRIGGER":
			Otdt otcTrigger = otdtService.selectTriggerByTradeNo(null, sb.getTradeNo(),"R");
			if(null == otcTrigger) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (otcTrigger.getRevdate() == null) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]冲销处理中");
				outBean.setRetStatus(RetStatusEnum.F);
			}else {
				outBean.setClientNo(otcTrigger.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}
			break;
		default:
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setClientNo("暂时不支持该类型查询!");
			break;
		}
		return outBean;
	}
	
	/**
	 * 查询交易录入状态
	 * @param sb
	 * @return
	 */
	private SlOutBean queryEntryStatus(StateBean sb){
		SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "QUERY-00000", "交易录入状诚查询成功!");
		outBean.setTellSeqNo(sb.getTradeNo());
		switch (StringUtils.defaultIfBlank(sb.getTradeType(), "")) {
		case "IRS":
			Swdh irs = swdhService.selectByTradeNo(null, sb.getTradeNo(),"N");
			if(null == irs) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]不存在");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (irs.getRevdate() == null) {
				outBean.setClientNo(irs.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		case "CCS":
			Swdh ccs = swdhService.selectByTradeNo(null, sb.getTradeNo(),"N");
			if(null == ccs) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]不存在");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (ccs.getRevdate() == null) {
				outBean.setClientNo(ccs.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		case "FWD":
			List<Fxdh> fwd = fxdhService.selectByTradeNo(null, sb.getTradeNo(),"N");
			if(null == fwd || fwd.size() == 0) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]不存在");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (fwd.get(0).getRevdate() == null) {
				outBean.setClientNo(fwd.get(0).getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		case "FSP":
			List<Fxdh> fsp = fxdhService.selectByTradeNo(null, sb.getTradeNo(),"N");
			if(null == fsp || fsp.size() != 2) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]不存在");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (fsp.get(0).getRevdate() == null && fsp.get(1).getRevdate() == null) {
				outBean.setClientNo(fsp.get(0).getDealno().trim()+"/"+fsp.get(1).getDealno().trim());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		case "OTC":
			Otdt otc = otdtService.selectByTradeNo(null, sb.getTradeNo(),"N");
			if(null == otc) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]不存在");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (otc.getRevdate() == null) {
				outBean.setClientNo(otc.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		case "FXD":
			int count = 0;
			List<Fxdh> fxd = fxdhService.selectByTradeNo(null, sb.getTradeNo(),"N");
			count =  null == fxd ? 0 : fxd.size();
			if(null == fxd || fxd.size() == 0) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]不存在");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (count == 1 && fxd.get(0).getRevdate() == null) {
				outBean.setClientNo(fxd.get(0).getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}else if (count == 2 && fxd.get(0).getRevdate() == null && fxd.get(1).getRevdate() == null) {
				outBean.setClientNo(fxd.get(0).getDealno().trim()+"/"+fxd.get(1).getDealno().trim());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		case "OTC_TRIGGER":
			Otdt otcTrigger = otdtService.selectTriggerByTradeNo(null, sb.getTradeNo(),"N");
			if(null == otcTrigger) {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]不存在");
				outBean.setRetStatus(RetStatusEnum.F);
			}else if (otcTrigger.getRevdate() == null) {
				outBean.setClientNo(otcTrigger.getDealno());
				outBean.setRetStatus(RetStatusEnum.S);
			}else {
				outBean.setRetMsg("交易["+outBean.getTellSeqNo()+"]已冲销");
				outBean.setRetStatus(RetStatusEnum.F);
			}
			break;
		default:
			outBean.setRetStatus(RetStatusEnum.F);
			outBean.setClientNo("暂时不支持该类型查询!");
			break;
		}
		return outBean;
	}

}
