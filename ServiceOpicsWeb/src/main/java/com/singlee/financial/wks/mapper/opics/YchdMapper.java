package com.singlee.financial.wks.mapper.opics;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.singlee.financial.wks.bean.opics.Ychd;

public interface YchdMapper {
    /**
     * delete by primary key
     * @param br primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("yieldcurve") String yieldcurve);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Ychd record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Ychd record);

    /**
     * select by primary key
     * @param br primary key
     * @return object by primary key
     */
    Ychd selectByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("yieldcurve") String yieldcurve);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Ychd record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Ychd record);

    int updateBatch(List<Ychd> list);

    int updateBatchSelective(List<Ychd> list);
}