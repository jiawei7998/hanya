package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Rdfh;
import org.apache.ibatis.annotations.Param;

public interface RdfhMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("transno") String transno, @Param("seq") String seq);

    int insert(Rdfh record);

    int insertSelective(Rdfh record);

    Rdfh selectByPrimaryKey(@Param("br") String br, @Param("transno") String transno, @Param("seq") String seq);

    int updateByPrimaryKeySelective(Rdfh record);

    int updateByPrimaryKey(Rdfh record);
    
    /**
     * 收息
     * @param record
     * @return
     */
    int updateByPrimaryKeyRec(Rdfh record);
    /**
     * 冲销收息
     * @param record
     * @return
     */
    int updateRevInterest(Rdfh record);
}