package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Rdfh;
import com.singlee.financial.wks.bean.opics.Spsh;
public interface RdfhService{


    int deleteByPrimaryKey(String br,String transno,String seq);

    int insert(Rdfh record);

    int insertSelective(Rdfh record);

    Rdfh selectByPrimaryKey(String br,String transno,String seq);

    int updateByPrimaryKeySelective(Rdfh record);

    int updateByPrimaryKey(Rdfh record);
    /**
     * 收息
     * @param record
     * @return
     */
    int updateByPrimaryKeyRec(Rdfh record);
    /**
     * 冲销收息
     * @param record
     * @return
     */
    int updateRevInterest(Rdfh record);
    /**
     * 方法已重载.根据现券买卖数据构建PRIR对象
     * 
     * @param dl
     * @return
     */
    Rdfh builder(Spsh spsh);
    
}
