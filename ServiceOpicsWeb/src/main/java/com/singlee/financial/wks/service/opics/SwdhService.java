package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Swdh;
public interface SwdhService{


    int deleteByPrimaryKey(String br,String dealno,String seq,String dealind);

    int insert(Swdh record);

    int insertSelective(Swdh record);

    Swdh selectByPrimaryKey(String br,String dealno,String seq,String dealind);
    Swdh selectByTradeNo(String br, String dealtext,String status);
    int updateByPrimaryKeySelective(Swdh record);

    int updateByPrimaryKey(Swdh record);

    /**
     * 提前终更新头表信息
     * @param record
     * @return
     */
    int updateByTerminate(Swdh record);
}
