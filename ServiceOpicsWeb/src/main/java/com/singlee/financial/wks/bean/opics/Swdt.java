package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Swdt implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String dealind;

    private String product;

    private String prodtype;

    private String basis;

    private String calcrule;

    private String compound;

    private BigDecimal finexchamt;

    private BigDecimal finexchbamt;

    private Date finexchauthdte;

    private String finexchauthind;

    private String finexchccy;

    private Date finexchdate;

    private String finexchfincent;

    private String finexchoper;

    private String finexchsacct;

    private String finexchsmeans;

    private Date firstipaydate;

    private BigDecimal fixdintamt;

    private String fixfloatind;

    private BigDecimal initexchamt;

    private BigDecimal initexchbamt;

    private Date initexchauthdte;

    private String initexchauthind;

    private String initexchccy;

    private Date initexchdate;

    private String initexchfincent;

    private String initexchoper;

    private String initexchsmeans;

    private String initexchsacct;

    private Date intauthdte;

    private String intauthind;

    private String intauthoper;

    private Date intconvdte;

    private BigDecimal intconvrate8;

    private String intccy;

    private BigDecimal intexchrate8;

    private String intexchterms;

    private String intpayadjust;

    private String intpaycycle;

    private String intpayday;

    private String intpayfincent;

    private String intpaypayrule;

    private BigDecimal intrate8;

    private BigDecimal intratecap8;

    private BigDecimal intratefloor8;

    private String intsacct;

    private String intsmeans;

    private Date lastipaydate;

    private Date matdate;

    private String notccy;

    private BigDecimal notccyamt;

    private BigDecimal npvamt;

    private Date nxtraterev;

    private String payrecind;

    private String ratecode;

    private String raterevcycle;

    private String raterevday;

    private Date raterevdte;

    private String raterevfincent;

    private String raterevfrstlst;

    private String raterevpayrule;

    private String schdflag;

    private BigDecimal spread8;

    private Date startdate;

    private String upfrontfeeno;

    private String upfrontfeenettype;

    private String yieldcurve;

    private String acrfrstlst;

    private BigDecimal npvamtinitexch;

    private BigDecimal npvamtfinexch;

    private String ps;

    private BigDecimal strike8;

    private BigDecimal index8;

    private BigDecimal particperc8;

    private BigDecimal delta8;

    private BigDecimal volatility8;

    private String settfirst;

    private String collarind;

    private BigDecimal npvbamt;

    private BigDecimal tdyaccrintamt;

    private BigDecimal tdyaccrintbamt;

    private BigDecimal ystaccrintamt;

    private BigDecimal ystaccrintbamt;

    private BigDecimal npvbamtinitexch;

    private BigDecimal npvbamtfinexch;

    private String upfrontfeefincent;

    private String upfrontfeeind;

    private BigDecimal tdycomprate8;

    private BigDecimal tdycompamt;

    private BigDecimal ystcomprate8;

    private BigDecimal ystcompamt;

    private Date exchrevdate;

    private String datedirection;

    private BigDecimal purchint;

    private Date lastcoupondate;

    private BigDecimal premdisc;

    private Date exchraterevdate;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private BigDecimal amount3;

    private String char1;

    private String char2;

    private String formulaind;

    private Date date1;

    private Date date2;

    private String flag1;

    private BigDecimal rate18;

    private BigDecimal rate28;

    private String usualid;

    private String btbusualid;

    private String mbsleg;

    private BigDecimal speed12;

    private BigDecimal servicing12;

    private String prepaytype;

    private String settnotional;

    private BigDecimal factor12;

    private Date factorrevdate;

    private BigDecimal origprinadjamt;

    private String intdeal;

    private BigDecimal internalrate8;

    private BigDecimal corpspread8;

    private BigDecimal bcreditamt;

    private BigDecimal bmtmamt;

    private BigDecimal bmktamt;

    private BigDecimal exchrate8;

    private String exchterms;

    private BigDecimal initbaseamt;

    private BigDecimal initfinamt;

    private BigDecimal initfinbaseamt;

    private BigDecimal initfinexchrate8;

    private BigDecimal initbaseexchrate8;

    private String initfinexchterms;

    private String initbaseterms;

    private String flag2;

    private String flag3;

    private BigDecimal rate38;

    private BigDecimal rate48;

    private Date date3;

    private Date date4;

    private BigDecimal amount4;

    private BigDecimal amount5;

    private BigDecimal corpspreadamt;

    private String postnotional;

    private String basketdescr;

    private BigDecimal convprice12;

    private String fwdfwdyieldcurve;

    private String equitytype;

    private Date intnegauthdte;

    private String intnegauthind;

    private String intnegauthoper;

    private String intnegsacct;

    private String intnegsmeans;

    private BigDecimal price12;

    private BigDecimal priceexchrate8;

    private String priceexchterms;

    private String repriceind;

    private BigDecimal quantity;

    private String secid;

    private String settdays;

    private String secccy;

    private Date repricedate;

    private BigDecimal rateofreturn12;

    private BigDecimal divpercent8;

    private String divpaydateind;

    private String divreinvestind;

    private String divpayccyind;

    private BigDecimal multiplier8;

    private String returntype;

    private String valuationtime;

    private Date nxtrepricedate;

    private String repricedays;

    private String externalvalind;

    private String notifdays;

    private String notiftime;

    private BigDecimal dealrate8;

    private String dealterms;

    private String fxratecode;

    private Date nextfxdate;

    private String exchmethod;

    private String multiccyacctind;

    private String legtype;

    private BigDecimal schedid;

    private BigDecimal prodid;

    private BigDecimal formulaid;

    private String accrualccy;

    private String mktvalsource;

    private String accrualmethod;

    private String grossufpayrecind;

    private BigDecimal grossufpayamt;

    private BigDecimal grossufpayrate8;

    private String schdconv;

    private String redemptionccyind;

    private Date nextfxprindate;

    private BigDecimal deffactor;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getDealind() {
		return dealind;
	}


	public void setDealind(String dealind) {
		this.dealind = dealind;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public String getBasis() {
		return basis;
	}


	public void setBasis(String basis) {
		this.basis = basis;
	}


	public String getCalcrule() {
		return calcrule;
	}


	public void setCalcrule(String calcrule) {
		this.calcrule = calcrule;
	}


	public String getCompound() {
		return compound;
	}


	public void setCompound(String compound) {
		this.compound = compound;
	}


	public BigDecimal getFinexchamt() {
		return finexchamt;
	}


	public void setFinexchamt(BigDecimal finexchamt) {
		this.finexchamt = finexchamt;
	}


	public BigDecimal getFinexchbamt() {
		return finexchbamt;
	}


	public void setFinexchbamt(BigDecimal finexchbamt) {
		this.finexchbamt = finexchbamt;
	}


	public Date getFinexchauthdte() {
		return finexchauthdte;
	}


	public void setFinexchauthdte(Date finexchauthdte) {
		this.finexchauthdte = finexchauthdte;
	}


	public String getFinexchauthind() {
		return finexchauthind;
	}


	public void setFinexchauthind(String finexchauthind) {
		this.finexchauthind = finexchauthind;
	}


	public String getFinexchccy() {
		return finexchccy;
	}


	public void setFinexchccy(String finexchccy) {
		this.finexchccy = finexchccy;
	}


	public Date getFinexchdate() {
		return finexchdate;
	}


	public void setFinexchdate(Date finexchdate) {
		this.finexchdate = finexchdate;
	}


	public String getFinexchfincent() {
		return finexchfincent;
	}


	public void setFinexchfincent(String finexchfincent) {
		this.finexchfincent = finexchfincent;
	}


	public String getFinexchoper() {
		return finexchoper;
	}


	public void setFinexchoper(String finexchoper) {
		this.finexchoper = finexchoper;
	}


	public String getFinexchsacct() {
		return finexchsacct;
	}


	public void setFinexchsacct(String finexchsacct) {
		this.finexchsacct = finexchsacct;
	}


	public String getFinexchsmeans() {
		return finexchsmeans;
	}


	public void setFinexchsmeans(String finexchsmeans) {
		this.finexchsmeans = finexchsmeans;
	}


	public Date getFirstipaydate() {
		return firstipaydate;
	}


	public void setFirstipaydate(Date firstipaydate) {
		this.firstipaydate = firstipaydate;
	}


	public BigDecimal getFixdintamt() {
		return fixdintamt;
	}


	public void setFixdintamt(BigDecimal fixdintamt) {
		this.fixdintamt = fixdintamt;
	}


	public String getFixfloatind() {
		return fixfloatind;
	}


	public void setFixfloatind(String fixfloatind) {
		this.fixfloatind = fixfloatind;
	}


	public BigDecimal getInitexchamt() {
		return initexchamt;
	}


	public void setInitexchamt(BigDecimal initexchamt) {
		this.initexchamt = initexchamt;
	}


	public BigDecimal getInitexchbamt() {
		return initexchbamt;
	}


	public void setInitexchbamt(BigDecimal initexchbamt) {
		this.initexchbamt = initexchbamt;
	}


	public Date getInitexchauthdte() {
		return initexchauthdte;
	}


	public void setInitexchauthdte(Date initexchauthdte) {
		this.initexchauthdte = initexchauthdte;
	}


	public String getInitexchauthind() {
		return initexchauthind;
	}


	public void setInitexchauthind(String initexchauthind) {
		this.initexchauthind = initexchauthind;
	}


	public String getInitexchccy() {
		return initexchccy;
	}


	public void setInitexchccy(String initexchccy) {
		this.initexchccy = initexchccy;
	}


	public Date getInitexchdate() {
		return initexchdate;
	}


	public void setInitexchdate(Date initexchdate) {
		this.initexchdate = initexchdate;
	}


	public String getInitexchfincent() {
		return initexchfincent;
	}


	public void setInitexchfincent(String initexchfincent) {
		this.initexchfincent = initexchfincent;
	}


	public String getInitexchoper() {
		return initexchoper;
	}


	public void setInitexchoper(String initexchoper) {
		this.initexchoper = initexchoper;
	}


	public String getInitexchsmeans() {
		return initexchsmeans;
	}


	public void setInitexchsmeans(String initexchsmeans) {
		this.initexchsmeans = initexchsmeans;
	}


	public String getInitexchsacct() {
		return initexchsacct;
	}


	public void setInitexchsacct(String initexchsacct) {
		this.initexchsacct = initexchsacct;
	}


	public Date getIntauthdte() {
		return intauthdte;
	}


	public void setIntauthdte(Date intauthdte) {
		this.intauthdte = intauthdte;
	}


	public String getIntauthind() {
		return intauthind;
	}


	public void setIntauthind(String intauthind) {
		this.intauthind = intauthind;
	}


	public String getIntauthoper() {
		return intauthoper;
	}


	public void setIntauthoper(String intauthoper) {
		this.intauthoper = intauthoper;
	}


	public Date getIntconvdte() {
		return intconvdte;
	}


	public void setIntconvdte(Date intconvdte) {
		this.intconvdte = intconvdte;
	}


	public BigDecimal getIntconvrate8() {
		return intconvrate8;
	}


	public void setIntconvrate8(BigDecimal intconvrate8) {
		this.intconvrate8 = intconvrate8;
	}


	public String getIntccy() {
		return intccy;
	}


	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}


	public BigDecimal getIntexchrate8() {
		return intexchrate8;
	}


	public void setIntexchrate8(BigDecimal intexchrate8) {
		this.intexchrate8 = intexchrate8;
	}


	public String getIntexchterms() {
		return intexchterms;
	}


	public void setIntexchterms(String intexchterms) {
		this.intexchterms = intexchterms;
	}


	public String getIntpayadjust() {
		return intpayadjust;
	}


	public void setIntpayadjust(String intpayadjust) {
		this.intpayadjust = intpayadjust;
	}


	public String getIntpaycycle() {
		return intpaycycle;
	}


	public void setIntpaycycle(String intpaycycle) {
		this.intpaycycle = intpaycycle;
	}


	public String getIntpayday() {
		return intpayday;
	}


	public void setIntpayday(String intpayday) {
		this.intpayday = intpayday;
	}


	public String getIntpayfincent() {
		return intpayfincent;
	}


	public void setIntpayfincent(String intpayfincent) {
		this.intpayfincent = intpayfincent;
	}


	public String getIntpaypayrule() {
		return intpaypayrule;
	}


	public void setIntpaypayrule(String intpaypayrule) {
		this.intpaypayrule = intpaypayrule;
	}


	public BigDecimal getIntrate8() {
		return intrate8;
	}


	public void setIntrate8(BigDecimal intrate8) {
		this.intrate8 = intrate8;
	}


	public BigDecimal getIntratecap8() {
		return intratecap8;
	}


	public void setIntratecap8(BigDecimal intratecap8) {
		this.intratecap8 = intratecap8;
	}


	public BigDecimal getIntratefloor8() {
		return intratefloor8;
	}


	public void setIntratefloor8(BigDecimal intratefloor8) {
		this.intratefloor8 = intratefloor8;
	}


	public String getIntsacct() {
		return intsacct;
	}


	public void setIntsacct(String intsacct) {
		this.intsacct = intsacct;
	}


	public String getIntsmeans() {
		return intsmeans;
	}


	public void setIntsmeans(String intsmeans) {
		this.intsmeans = intsmeans;
	}


	public Date getLastipaydate() {
		return lastipaydate;
	}


	public void setLastipaydate(Date lastipaydate) {
		this.lastipaydate = lastipaydate;
	}


	public Date getMatdate() {
		return matdate;
	}


	public void setMatdate(Date matdate) {
		this.matdate = matdate;
	}


	public String getNotccy() {
		return notccy;
	}


	public void setNotccy(String notccy) {
		this.notccy = notccy;
	}


	public BigDecimal getNotccyamt() {
		return notccyamt;
	}


	public void setNotccyamt(BigDecimal notccyamt) {
		this.notccyamt = notccyamt;
	}


	public BigDecimal getNpvamt() {
		return npvamt;
	}


	public void setNpvamt(BigDecimal npvamt) {
		this.npvamt = npvamt;
	}


	public Date getNxtraterev() {
		return nxtraterev;
	}


	public void setNxtraterev(Date nxtraterev) {
		this.nxtraterev = nxtraterev;
	}


	public String getPayrecind() {
		return payrecind;
	}


	public void setPayrecind(String payrecind) {
		this.payrecind = payrecind;
	}


	public String getRatecode() {
		return ratecode;
	}


	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}


	public String getRaterevcycle() {
		return raterevcycle;
	}


	public void setRaterevcycle(String raterevcycle) {
		this.raterevcycle = raterevcycle;
	}


	public String getRaterevday() {
		return raterevday;
	}


	public void setRaterevday(String raterevday) {
		this.raterevday = raterevday;
	}


	public Date getRaterevdte() {
		return raterevdte;
	}


	public void setRaterevdte(Date raterevdte) {
		this.raterevdte = raterevdte;
	}


	public String getRaterevfincent() {
		return raterevfincent;
	}


	public void setRaterevfincent(String raterevfincent) {
		this.raterevfincent = raterevfincent;
	}


	public String getRaterevfrstlst() {
		return raterevfrstlst;
	}


	public void setRaterevfrstlst(String raterevfrstlst) {
		this.raterevfrstlst = raterevfrstlst;
	}


	public String getRaterevpayrule() {
		return raterevpayrule;
	}


	public void setRaterevpayrule(String raterevpayrule) {
		this.raterevpayrule = raterevpayrule;
	}


	public String getSchdflag() {
		return schdflag;
	}


	public void setSchdflag(String schdflag) {
		this.schdflag = schdflag;
	}


	public BigDecimal getSpread8() {
		return spread8;
	}


	public void setSpread8(BigDecimal spread8) {
		this.spread8 = spread8;
	}


	public Date getStartdate() {
		return startdate;
	}


	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}


	public String getUpfrontfeeno() {
		return upfrontfeeno;
	}


	public void setUpfrontfeeno(String upfrontfeeno) {
		this.upfrontfeeno = upfrontfeeno;
	}


	public String getUpfrontfeenettype() {
		return upfrontfeenettype;
	}


	public void setUpfrontfeenettype(String upfrontfeenettype) {
		this.upfrontfeenettype = upfrontfeenettype;
	}


	public String getYieldcurve() {
		return yieldcurve;
	}


	public void setYieldcurve(String yieldcurve) {
		this.yieldcurve = yieldcurve;
	}


	public String getAcrfrstlst() {
		return acrfrstlst;
	}


	public void setAcrfrstlst(String acrfrstlst) {
		this.acrfrstlst = acrfrstlst;
	}


	public BigDecimal getNpvamtinitexch() {
		return npvamtinitexch;
	}


	public void setNpvamtinitexch(BigDecimal npvamtinitexch) {
		this.npvamtinitexch = npvamtinitexch;
	}


	public BigDecimal getNpvamtfinexch() {
		return npvamtfinexch;
	}


	public void setNpvamtfinexch(BigDecimal npvamtfinexch) {
		this.npvamtfinexch = npvamtfinexch;
	}


	public String getPs() {
		return ps;
	}


	public void setPs(String ps) {
		this.ps = ps;
	}


	public BigDecimal getStrike8() {
		return strike8;
	}


	public void setStrike8(BigDecimal strike8) {
		this.strike8 = strike8;
	}


	public BigDecimal getIndex8() {
		return index8;
	}


	public void setIndex8(BigDecimal index8) {
		this.index8 = index8;
	}


	public BigDecimal getParticperc8() {
		return particperc8;
	}


	public void setParticperc8(BigDecimal particperc8) {
		this.particperc8 = particperc8;
	}


	public BigDecimal getDelta8() {
		return delta8;
	}


	public void setDelta8(BigDecimal delta8) {
		this.delta8 = delta8;
	}


	public BigDecimal getVolatility8() {
		return volatility8;
	}


	public void setVolatility8(BigDecimal volatility8) {
		this.volatility8 = volatility8;
	}


	public String getSettfirst() {
		return settfirst;
	}


	public void setSettfirst(String settfirst) {
		this.settfirst = settfirst;
	}


	public String getCollarind() {
		return collarind;
	}


	public void setCollarind(String collarind) {
		this.collarind = collarind;
	}


	public BigDecimal getNpvbamt() {
		return npvbamt;
	}


	public void setNpvbamt(BigDecimal npvbamt) {
		this.npvbamt = npvbamt;
	}


	public BigDecimal getTdyaccrintamt() {
		return tdyaccrintamt;
	}


	public void setTdyaccrintamt(BigDecimal tdyaccrintamt) {
		this.tdyaccrintamt = tdyaccrintamt;
	}


	public BigDecimal getTdyaccrintbamt() {
		return tdyaccrintbamt;
	}


	public void setTdyaccrintbamt(BigDecimal tdyaccrintbamt) {
		this.tdyaccrintbamt = tdyaccrintbamt;
	}


	public BigDecimal getYstaccrintamt() {
		return ystaccrintamt;
	}


	public void setYstaccrintamt(BigDecimal ystaccrintamt) {
		this.ystaccrintamt = ystaccrintamt;
	}


	public BigDecimal getYstaccrintbamt() {
		return ystaccrintbamt;
	}


	public void setYstaccrintbamt(BigDecimal ystaccrintbamt) {
		this.ystaccrintbamt = ystaccrintbamt;
	}


	public BigDecimal getNpvbamtinitexch() {
		return npvbamtinitexch;
	}


	public void setNpvbamtinitexch(BigDecimal npvbamtinitexch) {
		this.npvbamtinitexch = npvbamtinitexch;
	}


	public BigDecimal getNpvbamtfinexch() {
		return npvbamtfinexch;
	}


	public void setNpvbamtfinexch(BigDecimal npvbamtfinexch) {
		this.npvbamtfinexch = npvbamtfinexch;
	}


	public String getUpfrontfeefincent() {
		return upfrontfeefincent;
	}


	public void setUpfrontfeefincent(String upfrontfeefincent) {
		this.upfrontfeefincent = upfrontfeefincent;
	}


	public String getUpfrontfeeind() {
		return upfrontfeeind;
	}


	public void setUpfrontfeeind(String upfrontfeeind) {
		this.upfrontfeeind = upfrontfeeind;
	}


	public BigDecimal getTdycomprate8() {
		return tdycomprate8;
	}


	public void setTdycomprate8(BigDecimal tdycomprate8) {
		this.tdycomprate8 = tdycomprate8;
	}


	public BigDecimal getTdycompamt() {
		return tdycompamt;
	}


	public void setTdycompamt(BigDecimal tdycompamt) {
		this.tdycompamt = tdycompamt;
	}


	public BigDecimal getYstcomprate8() {
		return ystcomprate8;
	}


	public void setYstcomprate8(BigDecimal ystcomprate8) {
		this.ystcomprate8 = ystcomprate8;
	}


	public BigDecimal getYstcompamt() {
		return ystcompamt;
	}


	public void setYstcompamt(BigDecimal ystcompamt) {
		this.ystcompamt = ystcompamt;
	}


	public Date getExchrevdate() {
		return exchrevdate;
	}


	public void setExchrevdate(Date exchrevdate) {
		this.exchrevdate = exchrevdate;
	}


	public String getDatedirection() {
		return datedirection;
	}


	public void setDatedirection(String datedirection) {
		this.datedirection = datedirection;
	}


	public BigDecimal getPurchint() {
		return purchint;
	}


	public void setPurchint(BigDecimal purchint) {
		this.purchint = purchint;
	}


	public Date getLastcoupondate() {
		return lastcoupondate;
	}


	public void setLastcoupondate(Date lastcoupondate) {
		this.lastcoupondate = lastcoupondate;
	}


	public BigDecimal getPremdisc() {
		return premdisc;
	}


	public void setPremdisc(BigDecimal premdisc) {
		this.premdisc = premdisc;
	}


	public Date getExchraterevdate() {
		return exchraterevdate;
	}


	public void setExchraterevdate(Date exchraterevdate) {
		this.exchraterevdate = exchraterevdate;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public BigDecimal getAmount3() {
		return amount3;
	}


	public void setAmount3(BigDecimal amount3) {
		this.amount3 = amount3;
	}


	public String getChar1() {
		return char1;
	}


	public void setChar1(String char1) {
		this.char1 = char1;
	}


	public String getChar2() {
		return char2;
	}


	public void setChar2(String char2) {
		this.char2 = char2;
	}


	public String getFormulaind() {
		return formulaind;
	}


	public void setFormulaind(String formulaind) {
		this.formulaind = formulaind;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public String getFlag1() {
		return flag1;
	}


	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}


	public BigDecimal getRate18() {
		return rate18;
	}


	public void setRate18(BigDecimal rate18) {
		this.rate18 = rate18;
	}


	public BigDecimal getRate28() {
		return rate28;
	}


	public void setRate28(BigDecimal rate28) {
		this.rate28 = rate28;
	}


	public String getUsualid() {
		return usualid;
	}


	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}


	public String getBtbusualid() {
		return btbusualid;
	}


	public void setBtbusualid(String btbusualid) {
		this.btbusualid = btbusualid;
	}


	public String getMbsleg() {
		return mbsleg;
	}


	public void setMbsleg(String mbsleg) {
		this.mbsleg = mbsleg;
	}


	public BigDecimal getSpeed12() {
		return speed12;
	}


	public void setSpeed12(BigDecimal speed12) {
		this.speed12 = speed12;
	}


	public BigDecimal getServicing12() {
		return servicing12;
	}


	public void setServicing12(BigDecimal servicing12) {
		this.servicing12 = servicing12;
	}


	public String getPrepaytype() {
		return prepaytype;
	}


	public void setPrepaytype(String prepaytype) {
		this.prepaytype = prepaytype;
	}


	public String getSettnotional() {
		return settnotional;
	}


	public void setSettnotional(String settnotional) {
		this.settnotional = settnotional;
	}


	public BigDecimal getFactor12() {
		return factor12;
	}


	public void setFactor12(BigDecimal factor12) {
		this.factor12 = factor12;
	}


	public Date getFactorrevdate() {
		return factorrevdate;
	}


	public void setFactorrevdate(Date factorrevdate) {
		this.factorrevdate = factorrevdate;
	}


	public BigDecimal getOrigprinadjamt() {
		return origprinadjamt;
	}


	public void setOrigprinadjamt(BigDecimal origprinadjamt) {
		this.origprinadjamt = origprinadjamt;
	}


	public String getIntdeal() {
		return intdeal;
	}


	public void setIntdeal(String intdeal) {
		this.intdeal = intdeal;
	}


	public BigDecimal getInternalrate8() {
		return internalrate8;
	}


	public void setInternalrate8(BigDecimal internalrate8) {
		this.internalrate8 = internalrate8;
	}


	public BigDecimal getCorpspread8() {
		return corpspread8;
	}


	public void setCorpspread8(BigDecimal corpspread8) {
		this.corpspread8 = corpspread8;
	}


	public BigDecimal getBcreditamt() {
		return bcreditamt;
	}


	public void setBcreditamt(BigDecimal bcreditamt) {
		this.bcreditamt = bcreditamt;
	}


	public BigDecimal getBmtmamt() {
		return bmtmamt;
	}


	public void setBmtmamt(BigDecimal bmtmamt) {
		this.bmtmamt = bmtmamt;
	}


	public BigDecimal getBmktamt() {
		return bmktamt;
	}


	public void setBmktamt(BigDecimal bmktamt) {
		this.bmktamt = bmktamt;
	}


	public BigDecimal getExchrate8() {
		return exchrate8;
	}


	public void setExchrate8(BigDecimal exchrate8) {
		this.exchrate8 = exchrate8;
	}


	public String getExchterms() {
		return exchterms;
	}


	public void setExchterms(String exchterms) {
		this.exchterms = exchterms;
	}


	public BigDecimal getInitbaseamt() {
		return initbaseamt;
	}


	public void setInitbaseamt(BigDecimal initbaseamt) {
		this.initbaseamt = initbaseamt;
	}


	public BigDecimal getInitfinamt() {
		return initfinamt;
	}


	public void setInitfinamt(BigDecimal initfinamt) {
		this.initfinamt = initfinamt;
	}


	public BigDecimal getInitfinbaseamt() {
		return initfinbaseamt;
	}


	public void setInitfinbaseamt(BigDecimal initfinbaseamt) {
		this.initfinbaseamt = initfinbaseamt;
	}


	public BigDecimal getInitfinexchrate8() {
		return initfinexchrate8;
	}


	public void setInitfinexchrate8(BigDecimal initfinexchrate8) {
		this.initfinexchrate8 = initfinexchrate8;
	}


	public BigDecimal getInitbaseexchrate8() {
		return initbaseexchrate8;
	}


	public void setInitbaseexchrate8(BigDecimal initbaseexchrate8) {
		this.initbaseexchrate8 = initbaseexchrate8;
	}


	public String getInitfinexchterms() {
		return initfinexchterms;
	}


	public void setInitfinexchterms(String initfinexchterms) {
		this.initfinexchterms = initfinexchterms;
	}


	public String getInitbaseterms() {
		return initbaseterms;
	}


	public void setInitbaseterms(String initbaseterms) {
		this.initbaseterms = initbaseterms;
	}


	public String getFlag2() {
		return flag2;
	}


	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}


	public String getFlag3() {
		return flag3;
	}


	public void setFlag3(String flag3) {
		this.flag3 = flag3;
	}


	public BigDecimal getRate38() {
		return rate38;
	}


	public void setRate38(BigDecimal rate38) {
		this.rate38 = rate38;
	}


	public BigDecimal getRate48() {
		return rate48;
	}


	public void setRate48(BigDecimal rate48) {
		this.rate48 = rate48;
	}


	public Date getDate3() {
		return date3;
	}


	public void setDate3(Date date3) {
		this.date3 = date3;
	}


	public Date getDate4() {
		return date4;
	}


	public void setDate4(Date date4) {
		this.date4 = date4;
	}


	public BigDecimal getAmount4() {
		return amount4;
	}


	public void setAmount4(BigDecimal amount4) {
		this.amount4 = amount4;
	}


	public BigDecimal getAmount5() {
		return amount5;
	}


	public void setAmount5(BigDecimal amount5) {
		this.amount5 = amount5;
	}


	public BigDecimal getCorpspreadamt() {
		return corpspreadamt;
	}


	public void setCorpspreadamt(BigDecimal corpspreadamt) {
		this.corpspreadamt = corpspreadamt;
	}


	public String getPostnotional() {
		return postnotional;
	}


	public void setPostnotional(String postnotional) {
		this.postnotional = postnotional;
	}


	public String getBasketdescr() {
		return basketdescr;
	}


	public void setBasketdescr(String basketdescr) {
		this.basketdescr = basketdescr;
	}


	public BigDecimal getConvprice12() {
		return convprice12;
	}


	public void setConvprice12(BigDecimal convprice12) {
		this.convprice12 = convprice12;
	}


	public String getFwdfwdyieldcurve() {
		return fwdfwdyieldcurve;
	}


	public void setFwdfwdyieldcurve(String fwdfwdyieldcurve) {
		this.fwdfwdyieldcurve = fwdfwdyieldcurve;
	}


	public String getEquitytype() {
		return equitytype;
	}


	public void setEquitytype(String equitytype) {
		this.equitytype = equitytype;
	}


	public Date getIntnegauthdte() {
		return intnegauthdte;
	}


	public void setIntnegauthdte(Date intnegauthdte) {
		this.intnegauthdte = intnegauthdte;
	}


	public String getIntnegauthind() {
		return intnegauthind;
	}


	public void setIntnegauthind(String intnegauthind) {
		this.intnegauthind = intnegauthind;
	}


	public String getIntnegauthoper() {
		return intnegauthoper;
	}


	public void setIntnegauthoper(String intnegauthoper) {
		this.intnegauthoper = intnegauthoper;
	}


	public String getIntnegsacct() {
		return intnegsacct;
	}


	public void setIntnegsacct(String intnegsacct) {
		this.intnegsacct = intnegsacct;
	}


	public String getIntnegsmeans() {
		return intnegsmeans;
	}


	public void setIntnegsmeans(String intnegsmeans) {
		this.intnegsmeans = intnegsmeans;
	}


	public BigDecimal getPrice12() {
		return price12;
	}


	public void setPrice12(BigDecimal price12) {
		this.price12 = price12;
	}


	public BigDecimal getPriceexchrate8() {
		return priceexchrate8;
	}


	public void setPriceexchrate8(BigDecimal priceexchrate8) {
		this.priceexchrate8 = priceexchrate8;
	}


	public String getPriceexchterms() {
		return priceexchterms;
	}


	public void setPriceexchterms(String priceexchterms) {
		this.priceexchterms = priceexchterms;
	}


	public String getRepriceind() {
		return repriceind;
	}


	public void setRepriceind(String repriceind) {
		this.repriceind = repriceind;
	}


	public BigDecimal getQuantity() {
		return quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public String getSecid() {
		return secid;
	}


	public void setSecid(String secid) {
		this.secid = secid;
	}


	public String getSettdays() {
		return settdays;
	}


	public void setSettdays(String settdays) {
		this.settdays = settdays;
	}


	public String getSecccy() {
		return secccy;
	}


	public void setSecccy(String secccy) {
		this.secccy = secccy;
	}


	public Date getRepricedate() {
		return repricedate;
	}


	public void setRepricedate(Date repricedate) {
		this.repricedate = repricedate;
	}


	public BigDecimal getRateofreturn12() {
		return rateofreturn12;
	}


	public void setRateofreturn12(BigDecimal rateofreturn12) {
		this.rateofreturn12 = rateofreturn12;
	}


	public BigDecimal getDivpercent8() {
		return divpercent8;
	}


	public void setDivpercent8(BigDecimal divpercent8) {
		this.divpercent8 = divpercent8;
	}


	public String getDivpaydateind() {
		return divpaydateind;
	}


	public void setDivpaydateind(String divpaydateind) {
		this.divpaydateind = divpaydateind;
	}


	public String getDivreinvestind() {
		return divreinvestind;
	}


	public void setDivreinvestind(String divreinvestind) {
		this.divreinvestind = divreinvestind;
	}


	public String getDivpayccyind() {
		return divpayccyind;
	}


	public void setDivpayccyind(String divpayccyind) {
		this.divpayccyind = divpayccyind;
	}


	public BigDecimal getMultiplier8() {
		return multiplier8;
	}


	public void setMultiplier8(BigDecimal multiplier8) {
		this.multiplier8 = multiplier8;
	}


	public String getReturntype() {
		return returntype;
	}


	public void setReturntype(String returntype) {
		this.returntype = returntype;
	}


	public String getValuationtime() {
		return valuationtime;
	}


	public void setValuationtime(String valuationtime) {
		this.valuationtime = valuationtime;
	}


	public Date getNxtrepricedate() {
		return nxtrepricedate;
	}


	public void setNxtrepricedate(Date nxtrepricedate) {
		this.nxtrepricedate = nxtrepricedate;
	}


	public String getRepricedays() {
		return repricedays;
	}


	public void setRepricedays(String repricedays) {
		this.repricedays = repricedays;
	}


	public String getExternalvalind() {
		return externalvalind;
	}


	public void setExternalvalind(String externalvalind) {
		this.externalvalind = externalvalind;
	}


	public String getNotifdays() {
		return notifdays;
	}


	public void setNotifdays(String notifdays) {
		this.notifdays = notifdays;
	}


	public String getNotiftime() {
		return notiftime;
	}


	public void setNotiftime(String notiftime) {
		this.notiftime = notiftime;
	}


	public BigDecimal getDealrate8() {
		return dealrate8;
	}


	public void setDealrate8(BigDecimal dealrate8) {
		this.dealrate8 = dealrate8;
	}


	public String getDealterms() {
		return dealterms;
	}


	public void setDealterms(String dealterms) {
		this.dealterms = dealterms;
	}


	public String getFxratecode() {
		return fxratecode;
	}


	public void setFxratecode(String fxratecode) {
		this.fxratecode = fxratecode;
	}


	public Date getNextfxdate() {
		return nextfxdate;
	}


	public void setNextfxdate(Date nextfxdate) {
		this.nextfxdate = nextfxdate;
	}


	public String getExchmethod() {
		return exchmethod;
	}


	public void setExchmethod(String exchmethod) {
		this.exchmethod = exchmethod;
	}


	public String getMulticcyacctind() {
		return multiccyacctind;
	}


	public void setMulticcyacctind(String multiccyacctind) {
		this.multiccyacctind = multiccyacctind;
	}


	public String getLegtype() {
		return legtype;
	}


	public void setLegtype(String legtype) {
		this.legtype = legtype;
	}


	public BigDecimal getSchedid() {
		return schedid;
	}


	public void setSchedid(BigDecimal schedid) {
		this.schedid = schedid;
	}


	public BigDecimal getProdid() {
		return prodid;
	}


	public void setProdid(BigDecimal prodid) {
		this.prodid = prodid;
	}


	public BigDecimal getFormulaid() {
		return formulaid;
	}


	public void setFormulaid(BigDecimal formulaid) {
		this.formulaid = formulaid;
	}


	public String getAccrualccy() {
		return accrualccy;
	}


	public void setAccrualccy(String accrualccy) {
		this.accrualccy = accrualccy;
	}


	public String getMktvalsource() {
		return mktvalsource;
	}


	public void setMktvalsource(String mktvalsource) {
		this.mktvalsource = mktvalsource;
	}


	public String getAccrualmethod() {
		return accrualmethod;
	}


	public void setAccrualmethod(String accrualmethod) {
		this.accrualmethod = accrualmethod;
	}


	public String getGrossufpayrecind() {
		return grossufpayrecind;
	}


	public void setGrossufpayrecind(String grossufpayrecind) {
		this.grossufpayrecind = grossufpayrecind;
	}


	public BigDecimal getGrossufpayamt() {
		return grossufpayamt;
	}


	public void setGrossufpayamt(BigDecimal grossufpayamt) {
		this.grossufpayamt = grossufpayamt;
	}


	public BigDecimal getGrossufpayrate8() {
		return grossufpayrate8;
	}


	public void setGrossufpayrate8(BigDecimal grossufpayrate8) {
		this.grossufpayrate8 = grossufpayrate8;
	}


	public String getSchdconv() {
		return schdconv;
	}


	public void setSchdconv(String schdconv) {
		this.schdconv = schdconv;
	}


	public String getRedemptionccyind() {
		return redemptionccyind;
	}


	public void setRedemptionccyind(String redemptionccyind) {
		this.redemptionccyind = redemptionccyind;
	}


	public Date getNextfxprindate() {
		return nextfxprindate;
	}


	public void setNextfxprindate(Date nextfxprindate) {
		this.nextfxprindate = nextfxprindate;
	}


	public BigDecimal getDeffactor() {
		return deffactor;
	}


	public void setDeffactor(BigDecimal deffactor) {
		this.deffactor = deffactor;
	}


	private static final long serialVersionUID = 1L;
}