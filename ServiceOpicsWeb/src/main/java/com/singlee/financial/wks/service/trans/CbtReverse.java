package com.singlee.financial.wks.service.trans;


import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.CbtWksBean;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.SpshService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 交易冲销转换方法
 * @author xuqq
 *
 */
@Component
public class CbtReverse {

	@Autowired
	private SpshService spshService;

	public void DataCheck(CbtWksBean cbt) {
		// 1.判断拆借交易对象是否存在
		if (null == cbt) {
			JY.raise("%s:%s,请补充完整IboWksBean对象", WksErrorCode.CBT_NULL.getErrCode(), WksErrorCode.CBT_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(cbt.getInstId())) {
			JY.raise("%s:%s,请填写IboWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 3.判断业务编号是否存在
		Spsh spsh = spshService.selectByTradeNo(cbt.getInstId(), cbt.getTradeNo());
		if (StringUtils.isEmpty(cbt.getTradeNo()) || null == spsh) {
			JY.raise("%s:%s,请填写CbtWksBean.TradeNo,For.[TradeNo=TRD20200101000001]",
					WksErrorCode.CBT_NOT_FOUND.getErrCode(), WksErrorCode.CBT_NOT_FOUND.getErrMsg());
		}
	}

	public Spsh DataTrans(Spsh spsh, BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(spsh.getBr());
		spsh.setRevreason("99");// 冲销参数:默认99
		spsh.setRevdate(brps.getBranprcdate());// 冲销日期
		spsh.setRevtext("99");// 冲销描述
		spsh.setRevoper(OpicsConstant.SPSH.TRAD_NO);// 冲销复核人员
		spsh.setUpdatecounter(spsh.getUpdatecounter() + 1);// 修改次数
//		spsh.setLstmntdate(Calendar.getInstance().getTime());// 最后修改日期
		return spsh;
	}
}
