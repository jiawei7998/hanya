package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Rprh;
public interface RprhService{


    int deleteByPrimaryKey(String br,String dealno,String seq);

    int insert(Rprh record);

    int insertSelective(Rprh record);

    Rprh selectByPrimaryKey(String br,String dealno,String seq);
    Rprh selectByTradeNo(String br, String dealtext);

    int updateByPrimaryKeySelective(Rprh record);

    int updateByPrimaryKey(Rprh record);

}
