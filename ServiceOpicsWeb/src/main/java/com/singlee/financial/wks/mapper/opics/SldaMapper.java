package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Slda;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SldaMapper {
    /**
     * delete by primary key
     *
     * @param br primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind, @Param("assignseq") String assignseq);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(Slda record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(Slda record);

    /**
     * select by primary key
     *
     * @param br primary key
     * @return object by primary key
     */
    Slda selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("dealind") String dealind, @Param("assignseq") String assignseq);
    List<Slda> selectByDealno(@Param("br") String br, @Param("dealno") String dealno);
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Slda record);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Slda record);
}