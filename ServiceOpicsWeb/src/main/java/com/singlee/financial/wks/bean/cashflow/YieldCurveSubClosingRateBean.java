package com.singlee.financial.wks.bean.cashflow;

import java.io.Serializable;
import java.math.BigDecimal;

public class YieldCurveSubClosingRateBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4561142226672335277L;

	/**
	 * 利率对应的期限
	 */
	private String maturity;
	
	/**
	 * 买入价格
	 */
	private BigDecimal bidRate;
	
	/**
	 * 卖出价格
	 */
	private BigDecimal offerRate;
	
	/**
	 * 中间价格
	 */
	private BigDecimal midRate;
	

	public String getMaturity() {
		return maturity;
	}

	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}

	public BigDecimal getBidRate() {
		return bidRate;
	}

	public void setBidRate(BigDecimal bidRate) {
		this.bidRate = bidRate;
	}

	public BigDecimal getOfferRate() {
		return offerRate;
	}

	public void setOfferRate(BigDecimal offerRate) {
		this.offerRate = offerRate;
	}

	public BigDecimal getMidRate() {
		return midRate;
	}

	public void setMidRate(BigDecimal midRate) {
		this.midRate = midRate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("YieldCurveSubClosingRateBean [maturity=");
		builder.append(maturity);
		builder.append(", bidRate=");
		builder.append(bidRate);
		builder.append(", offerRate=");
		builder.append(offerRate);
		builder.append(", midRate=");
		builder.append(midRate);
		builder.append("]");
		return builder.toString();
	}
	
	
}
