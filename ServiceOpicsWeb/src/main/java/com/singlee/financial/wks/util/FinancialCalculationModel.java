package com.singlee.financial.wks.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.logging.LogFactory;

public class FinancialCalculationModel {

	public static BigDecimal ONE_HUNDRED = new BigDecimal("100");

	public static BigDecimal ZERO = BigDecimal.ZERO;

	public static BigDecimal ONE = BigDecimal.ONE;
	
	public static BigDecimal THREE = new BigDecimal("3");
	
	public static BigDecimal TEN = new BigDecimal("10");
	
	public static BigDecimal TEN_THOUSAND = new BigDecimal("10000");

	public static void main(String[] args) {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 计算利息金额
	 * 
	 * @param prinAmt
	 * @param beginDate
	 * @param endDate
	 * @param rate
	 * @param basis
	 * @param frequency
	 * @return
	 */
	public static BigDecimal calculateInterestAmount(BigDecimal prinAmt, Date beginDate, Date endDate, BigDecimal rate,
			String basis, String frequency) {
		if(null == prinAmt) {
			throw new RuntimeException("计算利息金额时,本金不能为空!");
		}
		if(null == beginDate) {
			throw new RuntimeException("计算利息金额时,开始日期不能为空!");
		}
		if(null == endDate) {
			throw new RuntimeException("计算利息金额时,结束日期不能为空!");
		}
		if(null == rate) {
			throw new RuntimeException("计算利息金额时,利率不能为空!");
		}
		if(null == basis || basis.trim().length() == 0) {
			throw new RuntimeException("计算利息金额时,计息基础不能为空!");
		}

		BigDecimal intDays = new BigDecimal(FinancialDateUtils.calculateTenorDate(beginDate, endDate));
		BigDecimal intBasis = FinancialBasisUtils.createBasis(basis, frequency, intDays.intValue());
		LogFactory.getLog(FinancialCalculationModel.class).info("名义本金：" + prinAmt+",计息基础：" + intBasis+",计息天数：" + intDays+",计率：" + rate);
		return prinAmt.multiply(intDays).multiply(rate).divide(intBasis, 2, RoundingMode.HALF_UP)
				.divide(FinancialCalculationModel.ONE_HUNDRED, 2, RoundingMode.HALF_UP);
	}

	/**
	 * 方法已重载.计算贴现因子,分子值做为参数传入,单利、复利按统一方式计算
	 * 
	 * @param beginDate
	 * @param endDate
	 * @param rate
	 * @param basis
	 * @return
	 */
	private static BigDecimal calculateDiscountFactor(Hashtable<String,BigDecimal> tenors_DiscountFactor,String tenor,Date beginDate, Date endDate, BigDecimal rate, String basis,BigDecimal disMolecular,Date batchDate) {
		LogFactory.getLog(FinancialCalculationModel.class).info("开始日期:"+String.format("%1$tY-%1$tm-%1$td",beginDate)+",结束日期"+String.format("%1$tY-%1$tm-%1$td",endDate));
		BigDecimal days = new BigDecimal(FinancialDateUtils.calculateTenorDate(beginDate, endDate));
		BigDecimal intBasis = FinancialBasisUtils.createBasis(basis, "", days.intValue());
		BigDecimal denominator = FinancialCalculationModel.ONE.add((rate.multiply(days).divide(intBasis,FinancialCalculationModel.TEN.intValue(),RoundingMode.HALF_UP).divide(FinancialCalculationModel.ONE_HUNDRED,FinancialCalculationModel.TEN.intValue(),RoundingMode.HALF_UP)));
		BigDecimal dis = disMolecular.divide(denominator,FinancialCalculationModel.TEN.intValue(),RoundingMode.HALF_UP);
		BigDecimal disDays = null;
		
		if("1Y".equalsIgnoreCase(tenor)) {
			disDays = new BigDecimal(FinancialDateUtils.calculateTenorDate(batchDate, endDate));
		}else {
			disDays = days;
		}
		
		tenors_DiscountFactor.put(tenor, dis.multiply(disDays).divide(intBasis, BigDecimal.TEN.intValue(),RoundingMode.HALF_UP));
		
		LogFactory.getLog(FinancialCalculationModel.class).info("计算期限："+tenor+",贴现因子值："+dis.doubleValue()+",计息天数："+days+",利率："+rate+",分子值："+disMolecular+",分母值："+denominator+"，复利值："+tenors_DiscountFactor.get(tenor)+",复利天数："+disDays);
		return dis;
	}
	
	/**
	 * 方法已重载.计算单利贴现因子
	 * 
	 * @param beginDate
	 * @param endDate
	 * @param rate
	 * @param basis
	 * @return
	 */
	public static BigDecimal calculateDiscountFactor(Hashtable<String,BigDecimal> tenors_DiscountFactor,String tenor,Date beginDate, Date endDate, BigDecimal rate, String basis) {
		return calculateDiscountFactor(tenors_DiscountFactor,tenor,beginDate,endDate,rate,basis,FinancialCalculationModel.ONE,beginDate);
	}
	
	/**
	 * 方法已重载.计算复利贴现因子
	 * 
	 * @param beginDate
	 * @param endDate
	 * @param rate
	 * @param basis
	 * @param frequency
	 * @return
	 */
	public static BigDecimal calculateDiscountFactor(Hashtable<String,BigDecimal> tenors_DiscountFactor,String tenor,Date beginDate, Date endDate, BigDecimal rate, String basis, List<String> compoundFrequencys,Date batchDate) {
		BigDecimal molecular = FinancialCalculationModel.ZERO;
		StringBuilder sb = new StringBuilder();
		
		if(null == tenor || tenor.trim().length() == 0) {
			throw new RuntimeException("计算贴现因子时,期限不能为空!");
		}
		if(null == beginDate) {
			throw new RuntimeException("计算贴现因子时,开始日期不能为空!");
		}
		if(null == endDate) {
			throw new RuntimeException("计算贴现因子时,结束日期不能为空!");
		}
		if(null == rate) {
			throw new RuntimeException("计算贴现因子时,利率不能为空!");
		}
		if(null == basis || basis.trim().length() == 0) {
			throw new RuntimeException("计算贴现因子时,计息基础不能为空!");
		}
		if(null == basis || basis.trim().length() == 0) {
			throw new RuntimeException("计算贴现因子时,计息基础不能为空!");
		}
		if(null == batchDate) {
			throw new RuntimeException("计算贴现因子时,批量日期不能为空!");
		}
		if(null == tenors_DiscountFactor) {
			throw new RuntimeException("计算贴现因子时,保存对应期限复利贴现因子MAP对象不能为空!");
		}
		
		//计算复利贴现因子分子值
		for (String key : compoundFrequencys) {
			sb.append("复利期限:"+key+"="+tenors_DiscountFactor.get(key)+",");
			molecular = molecular.add(tenors_DiscountFactor.get(key));
		}
		
		LogFactory.getLog(FinancialCalculationModel.class).debug("当前期限:"+tenor+",复利频率集合："+compoundFrequencys+",复利贴现因子值:"+molecular.doubleValue()+",复利期限点复利因子值:"+sb.toString());
		
		molecular = FinancialCalculationModel.ONE.subtract((rate.divide(FinancialCalculationModel.ONE_HUNDRED, 10,RoundingMode.HALF_UP).multiply(molecular)));
		
		LogFactory.getLog(FinancialCalculationModel.class).debug("复利贴现因子分子值："+molecular);
		
		//调用通用贴现因子计算方法,返回对应的贴现因子值
		return calculateDiscountFactor(tenors_DiscountFactor,tenor,beginDate,endDate,rate,basis,molecular,batchDate);
	}
	
	/**
	 * 直线插值计算利率
	 * @param rb
	 * @param rd
	 * @param tb
	 * @param td
	 * @param tc
	 * @return
	 */
	public static BigDecimal calculateInterpolationRate(BigDecimal rb,BigDecimal rd,Date tb,Date td,Date tc) {
		if(tb.compareTo(td) == 0 || tb.compareTo(tc) == 0 || rd.equals(FinancialCalculationModel.ZERO)) {
			return FinancialCalculationModel.ZERO;
		}
		return rb.add((rd.subtract(rb)).multiply(new BigDecimal(FinancialDateUtils.calculateTenorDate(tb, tc)).divide(new BigDecimal(FinancialDateUtils.calculateTenorDate(tb, td)),15,RoundingMode.HALF_UP)));
	}
	
	/**
	 * 方法已重载.贴现因子插值公式
	 * @param dfb
	 * @param dfd
	 * @param tb
	 * @param td
	 * @param tc
	 * @return
	 */
	public static BigDecimal calculateInterpolationDiscountFactor(BigDecimal dfb,BigDecimal dfd,BigDecimal tb,BigDecimal td,BigDecimal tc) {
		LogFactory.getLog(FinancialCalculationModel.class).debug("dfb:"+dfb+", dfd:"+dfd+", tb:"+tb+", td:"+td+", tc:"+tc);
		if(null == dfb || ZERO.equals(dfb)) {
			throw new RuntimeException("计算直线插值贴现因子时,上一个期限对应的贴现因子值不能为零!");
		}
		if(null == dfd || ZERO.equals(dfd)) {
			throw new RuntimeException("计算直线插值贴现因子时,下一个期限点对应的贴现因子值不能为零!");
		}
		if(null == tc || ZERO.equals(tc)) {
			throw new RuntimeException("计算直线插值贴现因子时,当期计息天数不能为零!");
		}
		if(null == td || ZERO.equals(td)) {
			throw new RuntimeException("计算直线插值贴现因子时,下一个期限计息天数不能为零!");
		}
		BigDecimal tb_tmp = null;
		if(ZERO.equals(tb)) {
			tb_tmp = ONE;
		}else {
			tb_tmp = tb;
		}
		return new BigDecimal(Math.pow(dfb.doubleValue(), (tc.divide(tb_tmp, 10,RoundingMode.HALF_UP)).multiply(((td.subtract(tc)).divide((td.subtract(tb_tmp)),10,RoundingMode.HALF_UP))).doubleValue()) * Math.pow(dfd.doubleValue(),(tc.divide(td, 10,RoundingMode.HALF_UP)).multiply(((tc.subtract(tb)).divide((td.subtract(tb_tmp)),10,RoundingMode.HALF_UP))).doubleValue()));
	}
	
	/**
	 * 方法已重载.根据前一期、后一期的标准期限、当期现金流计息结束日到批量日期的时间，计算对应的天数，调用贴现因子插值公工计算指定日期的贴现因子
	 * @param dfb
	 * @param dfd
	 * @param tb_start
	 * @param tb_end
	 * @param td
	 * @param tc_batchDate
	 * @param tc_intEndDate
	 * @param td_start
	 * @param td_end
	 * @return
	 */
	public static BigDecimal calculateInterpolationDiscountFactor(BigDecimal dfb,BigDecimal dfd,Date tb_start,Date tb_end,Date tc_batchDate,Date tc_intEndDate,Date td_start,Date td_end) {
		return calculateInterpolationDiscountFactor(dfb,dfd,new BigDecimal(FinancialDateUtils.calculateTenorDate(tb_start, tb_end)),new BigDecimal(FinancialDateUtils.calculateTenorDate(td_start, td_end)),new BigDecimal(FinancialDateUtils.calculateTenorDate(tc_batchDate, tc_intEndDate)));
	}
	

	/**
	 * 计算利率互换浮动端远期利率
	 * @param dfb
	 * @param dfc
	 * @param basisDays
	 * @param intDays
	 * @return
	 */
	public static BigDecimal calculateForwardRate(BigDecimal dfb,BigDecimal dfc,BigDecimal basisDays,BigDecimal intDays) {
		BigDecimal forwardRate = null;
		
		if(null == dfb || ZERO.equals(dfb)) {
			throw new RuntimeException("计算远期贴现利率时,上一个期限对应的贴现因子值不能为零!");
		}
		if(null == dfc || ZERO.equals(dfc)) {
			throw new RuntimeException("计算远期贴现利率时,当期对应的贴现因子值不能为零!");
		}
		if(null == basisDays || ZERO.equals(basisDays)) {
			throw new RuntimeException("计算远期贴现利率时,计息基础不能为零!");
		}
		if(null == intDays || ZERO.equals(intDays)) {
			throw new RuntimeException("计算远期贴现利率时,计息天数不能为零!");
		}
		
		forwardRate = ((dfb.divide(dfc, 10,RoundingMode.HALF_UP)).subtract(ONE)).multiply(((basisDays.multiply(ONE_HUNDRED)).divide(intDays,10,RoundingMode.HALF_UP)));
		
		return forwardRate;
	}
}
