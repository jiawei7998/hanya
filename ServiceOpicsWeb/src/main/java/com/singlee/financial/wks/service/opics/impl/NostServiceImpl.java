package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Nost;
import com.singlee.financial.wks.mapper.opics.NostMapper;
import com.singlee.financial.wks.service.opics.NostService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class NostServiceImpl implements NostService {

    @Resource
    private NostMapper nostMapper;

    @Override
    public int deleteByPrimaryKey(String br, String nos) {
        return nostMapper.deleteByPrimaryKey(br, nos);
    }

    @Override
    public int insert(Nost record) {
        return nostMapper.insert(record);
    }

    @Override
    public int insertSelective(Nost record) {
        return nostMapper.insertSelective(record);
    }

    @Override
    public Nost selectByPrimaryKey(String br, String nos) {
        return nostMapper.selectByPrimaryKey(br, nos);
    }

    @Override
    public List<String> selectNosByCust(String br, String ccy, String bic) {
       return nostMapper.selectNosByCust(br, ccy, bic);
    }

    @Override
    public int updateByPrimaryKeySelective(Nost record) {
        return nostMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Nost record) {
        return nostMapper.updateByPrimaryKey(record);
    }

}
