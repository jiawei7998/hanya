package com.singlee.financial.wks.service.opics;

import java.util.Date;
import java.util.List;

import com.singlee.financial.wks.bean.opics.Hldy;

public interface HldyService {

    int deleteByPrimaryKey(String calendarid, String holidaytype, Date holidate);

    int insert(Hldy record);

    int insertSelective(Hldy record);

    Hldy selectByPrimaryKey(String calendarid, String holidaytype, Date holidate);

    int updateByPrimaryKeySelective(Hldy record);

    int updateByPrimaryKey(Hldy record);

    List<Date> selectHldyListStr(String ccy);
    
    /**
	 * 查询当期货币指定时间内的假日信息
	 * @param ccy
	 * @param dealDate
	 * @param valueDate
	 * @return
	 */
	List<Date> selectHldyListDate(List<String> ccy,Date dealDate,Date valueDate);
}






