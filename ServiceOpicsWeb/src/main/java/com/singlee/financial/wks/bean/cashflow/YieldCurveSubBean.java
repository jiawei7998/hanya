package com.singlee.financial.wks.bean.cashflow;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class YieldCurveSubBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4952428264309635811L;

	/**
	 * 曲线贡献点名称
	 */
	private String YieldCurveSubName;

	/**
	 * 利率类型,MMkt、Bond、Swap、Spread、Future、FRA、Exchange
	 */
	private String rateType;

	/**
	 * 插值方法,Linear、Logarithmic、Cubic、CubicSpline
	 */
	private String interpolationMethod;

	/**
	 * 计息基础
	 */
	private String basis;

	/**
	 * 曲线复利步率,A、S、Q、M
	 */
	private String CompoudingFrequency;

	/**
	 * 收盘价信息
	 */
	private Map<String, YieldCurveSubClosingRateBean> closingRates = new HashMap<String, YieldCurveSubClosingRateBean>();

	public String getYieldCurveSubName() {
		return YieldCurveSubName;
	}

	public void setYieldCurveSubName(String yieldCurveSubName) {
		YieldCurveSubName = yieldCurveSubName;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getInterpolationMethod() {
		return interpolationMethod;
	}

	public void setInterpolationMethod(String interpolationMethod) {
		this.interpolationMethod = interpolationMethod;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getCompoudingFrequency() {
		return CompoudingFrequency;
	}

	public void setCompoudingFrequency(String compoudingFrequency) {
		CompoudingFrequency = compoudingFrequency;
	}

	public Map<String, YieldCurveSubClosingRateBean> getClosingRates() {
		return closingRates;
	}

	public void addClosingRates(String maturityDays, YieldCurveSubClosingRateBean closingRate) {
		this.closingRates.put(maturityDays, closingRate);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("YieldCurveSubBean [YieldCurveSubName=");
		builder.append(YieldCurveSubName);
		builder.append(", rateType=");
		builder.append(rateType);
		builder.append(", interpolationMethod=");
		builder.append(interpolationMethod);
		builder.append(", basis=");
		builder.append(basis);
		builder.append(", CompoudingFrequency=");
		builder.append(CompoudingFrequency);
		builder.append(", closingRates=");
		builder.append(closingRates);
		builder.append("]");
		return builder.toString();
	}

}
