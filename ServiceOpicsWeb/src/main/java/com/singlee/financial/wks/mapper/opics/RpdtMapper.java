package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Rpdt;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface RpdtMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("assignseq") String assignseq);

    int insert(Rpdt record);

    int insertSelective(Rpdt record);

    Rpdt selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("assignseq") String assignseq);
    List<Rpdt> selectByDealno(@Param("br") String br, @Param("dealno") String dealno);
    
    int updateByPrimaryKeySelective(Rpdt record);

    int updateByPrimaryKey(Rpdt record);
}