package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Rdfh implements Serializable {
    private String br;

    private String transno;

    private String seq;

    private String betext1;

    private String betext2;

    private String betext3;

    private String betext4;

    private String besacct;

    private String besbic;

    private Date brprcindte;

    private String ccy;

    private String cno;

    private String dr;

    private BigDecimal initcost;

    private Date inputdate;

    private String ioper;

    private String inputtime;

    private String linkdtransno;

    private String product;

    private String prodtype;

    private BigDecimal qty;

    private Date revdate;

    private String revoper;

    private String revreason;

    private String revtext;

    private String secsacct;

    private Date secauthdate;

    private String secauthind;

    private String secauthoper;

    private String secid;

    private Date settdate;

    private Date transdate;

    private String transtime;

    private Date verdate;

    private String veroper;

    private String verind;

    private Date cleardate;

    private Date clearinputdate;

    private String clearoper;

    private Date faildate;

    private Date failinputdate;

    private String failinputoper;

    private String suppconfind;

    private String freepaycash;

    private BigDecimal ccyamt;

    private BigDecimal vatamt;

    private BigDecimal whtamt;

    private BigDecimal proceeds;

    private String feecode1;

    private BigDecimal feeamt1;

    private String feecode2;

    private BigDecimal feeamt2;

    private String feecode3;

    private BigDecimal feeamt3;

    private String custccysmeans;

    private String custccysacct;

    private String ccysmeans;

    private String ccysacct;

    private String brok;

    private Date dealdate;

    private String dealtext;

    private String cost;

    private String tranref;

    private BigDecimal rfuprice8;

    private String port;

    private String invtype;

    private String sendrecno;

    private String instrreqind;

    private Date ccyauthdate;

    private String ccyauthind;

    private String ccyauthoper;

    private String usualid;

    private String ccpind;

    private String settccy;

    private BigDecimal settexchrate8;

    private String settterms;

    private BigDecimal settamt;

    private BigDecimal settbaseexchrate8;

    private String settbaseterms;

    private BigDecimal settbaseamt;

    private BigDecimal ccybaseamt;

    private String divno;

    private String vsecsacct;

    private Date initpurchdate;

    private String convind;

    private String extbsktid;

    private String extconvid;

    private String agreeno;

    private String plgtype;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getTransno() {
		return transno;
	}


	public void setTransno(String transno) {
		this.transno = transno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getBetext1() {
		return betext1;
	}


	public void setBetext1(String betext1) {
		this.betext1 = betext1;
	}


	public String getBetext2() {
		return betext2;
	}


	public void setBetext2(String betext2) {
		this.betext2 = betext2;
	}


	public String getBetext3() {
		return betext3;
	}


	public void setBetext3(String betext3) {
		this.betext3 = betext3;
	}


	public String getBetext4() {
		return betext4;
	}


	public void setBetext4(String betext4) {
		this.betext4 = betext4;
	}


	public String getBesacct() {
		return besacct;
	}


	public void setBesacct(String besacct) {
		this.besacct = besacct;
	}


	public String getBesbic() {
		return besbic;
	}


	public void setBesbic(String besbic) {
		this.besbic = besbic;
	}


	public Date getBrprcindte() {
		return brprcindte;
	}


	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getDr() {
		return dr;
	}


	public void setDr(String dr) {
		this.dr = dr;
	}


	public BigDecimal getInitcost() {
		return initcost;
	}


	public void setInitcost(BigDecimal initcost) {
		this.initcost = initcost;
	}


	public Date getInputdate() {
		return inputdate;
	}


	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}


	public String getIoper() {
		return ioper;
	}


	public void setIoper(String ioper) {
		this.ioper = ioper;
	}


	public String getInputtime() {
		return inputtime;
	}


	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}


	public String getLinkdtransno() {
		return linkdtransno;
	}


	public void setLinkdtransno(String linkdtransno) {
		this.linkdtransno = linkdtransno;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getProdtype() {
		return prodtype;
	}


	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}


	public BigDecimal getQty() {
		return qty;
	}


	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}


	public Date getRevdate() {
		return revdate;
	}


	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}


	public String getRevoper() {
		return revoper;
	}


	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}


	public String getRevreason() {
		return revreason;
	}


	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}


	public String getRevtext() {
		return revtext;
	}


	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}


	public String getSecsacct() {
		return secsacct;
	}


	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}


	public Date getSecauthdate() {
		return secauthdate;
	}


	public void setSecauthdate(Date secauthdate) {
		this.secauthdate = secauthdate;
	}


	public String getSecauthind() {
		return secauthind;
	}


	public void setSecauthind(String secauthind) {
		this.secauthind = secauthind;
	}


	public String getSecauthoper() {
		return secauthoper;
	}


	public void setSecauthoper(String secauthoper) {
		this.secauthoper = secauthoper;
	}


	public String getSecid() {
		return secid;
	}


	public void setSecid(String secid) {
		this.secid = secid;
	}


	public Date getSettdate() {
		return settdate;
	}


	public void setSettdate(Date settdate) {
		this.settdate = settdate;
	}


	public Date getTransdate() {
		return transdate;
	}


	public void setTransdate(Date transdate) {
		this.transdate = transdate;
	}


	public String getTranstime() {
		return transtime;
	}


	public void setTranstime(String transtime) {
		this.transtime = transtime;
	}


	public Date getVerdate() {
		return verdate;
	}


	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}


	public String getVeroper() {
		return veroper;
	}


	public void setVeroper(String veroper) {
		this.veroper = veroper;
	}


	public String getVerind() {
		return verind;
	}


	public void setVerind(String verind) {
		this.verind = verind;
	}


	public Date getCleardate() {
		return cleardate;
	}


	public void setCleardate(Date cleardate) {
		this.cleardate = cleardate;
	}


	public Date getClearinputdate() {
		return clearinputdate;
	}


	public void setClearinputdate(Date clearinputdate) {
		this.clearinputdate = clearinputdate;
	}


	public String getClearoper() {
		return clearoper;
	}


	public void setClearoper(String clearoper) {
		this.clearoper = clearoper;
	}


	public Date getFaildate() {
		return faildate;
	}


	public void setFaildate(Date faildate) {
		this.faildate = faildate;
	}


	public Date getFailinputdate() {
		return failinputdate;
	}


	public void setFailinputdate(Date failinputdate) {
		this.failinputdate = failinputdate;
	}


	public String getFailinputoper() {
		return failinputoper;
	}


	public void setFailinputoper(String failinputoper) {
		this.failinputoper = failinputoper;
	}


	public String getSuppconfind() {
		return suppconfind;
	}


	public void setSuppconfind(String suppconfind) {
		this.suppconfind = suppconfind;
	}


	public String getFreepaycash() {
		return freepaycash;
	}


	public void setFreepaycash(String freepaycash) {
		this.freepaycash = freepaycash;
	}


	public BigDecimal getCcyamt() {
		return ccyamt;
	}


	public void setCcyamt(BigDecimal ccyamt) {
		this.ccyamt = ccyamt;
	}


	public BigDecimal getVatamt() {
		return vatamt;
	}


	public void setVatamt(BigDecimal vatamt) {
		this.vatamt = vatamt;
	}


	public BigDecimal getWhtamt() {
		return whtamt;
	}


	public void setWhtamt(BigDecimal whtamt) {
		this.whtamt = whtamt;
	}


	public BigDecimal getProceeds() {
		return proceeds;
	}


	public void setProceeds(BigDecimal proceeds) {
		this.proceeds = proceeds;
	}


	public String getFeecode1() {
		return feecode1;
	}


	public void setFeecode1(String feecode1) {
		this.feecode1 = feecode1;
	}


	public BigDecimal getFeeamt1() {
		return feeamt1;
	}


	public void setFeeamt1(BigDecimal feeamt1) {
		this.feeamt1 = feeamt1;
	}


	public String getFeecode2() {
		return feecode2;
	}


	public void setFeecode2(String feecode2) {
		this.feecode2 = feecode2;
	}


	public BigDecimal getFeeamt2() {
		return feeamt2;
	}


	public void setFeeamt2(BigDecimal feeamt2) {
		this.feeamt2 = feeamt2;
	}


	public String getFeecode3() {
		return feecode3;
	}


	public void setFeecode3(String feecode3) {
		this.feecode3 = feecode3;
	}


	public BigDecimal getFeeamt3() {
		return feeamt3;
	}


	public void setFeeamt3(BigDecimal feeamt3) {
		this.feeamt3 = feeamt3;
	}


	public String getCustccysmeans() {
		return custccysmeans;
	}


	public void setCustccysmeans(String custccysmeans) {
		this.custccysmeans = custccysmeans;
	}


	public String getCustccysacct() {
		return custccysacct;
	}


	public void setCustccysacct(String custccysacct) {
		this.custccysacct = custccysacct;
	}


	public String getCcysmeans() {
		return ccysmeans;
	}


	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans;
	}


	public String getCcysacct() {
		return ccysacct;
	}


	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}


	public String getBrok() {
		return brok;
	}


	public void setBrok(String brok) {
		this.brok = brok;
	}


	public Date getDealdate() {
		return dealdate;
	}


	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}


	public String getDealtext() {
		return dealtext;
	}


	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public String getTranref() {
		return tranref;
	}


	public void setTranref(String tranref) {
		this.tranref = tranref;
	}


	public BigDecimal getRfuprice8() {
		return rfuprice8;
	}


	public void setRfuprice8(BigDecimal rfuprice8) {
		this.rfuprice8 = rfuprice8;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getInvtype() {
		return invtype;
	}


	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}


	public String getSendrecno() {
		return sendrecno;
	}


	public void setSendrecno(String sendrecno) {
		this.sendrecno = sendrecno;
	}


	public String getInstrreqind() {
		return instrreqind;
	}


	public void setInstrreqind(String instrreqind) {
		this.instrreqind = instrreqind;
	}


	public Date getCcyauthdate() {
		return ccyauthdate;
	}


	public void setCcyauthdate(Date ccyauthdate) {
		this.ccyauthdate = ccyauthdate;
	}


	public String getCcyauthind() {
		return ccyauthind;
	}


	public void setCcyauthind(String ccyauthind) {
		this.ccyauthind = ccyauthind;
	}


	public String getCcyauthoper() {
		return ccyauthoper;
	}


	public void setCcyauthoper(String ccyauthoper) {
		this.ccyauthoper = ccyauthoper;
	}


	public String getUsualid() {
		return usualid;
	}


	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}


	public String getCcpind() {
		return ccpind;
	}


	public void setCcpind(String ccpind) {
		this.ccpind = ccpind;
	}


	public String getSettccy() {
		return settccy;
	}


	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}


	public BigDecimal getSettexchrate8() {
		return settexchrate8;
	}


	public void setSettexchrate8(BigDecimal settexchrate8) {
		this.settexchrate8 = settexchrate8;
	}


	public String getSettterms() {
		return settterms;
	}


	public void setSettterms(String settterms) {
		this.settterms = settterms;
	}


	public BigDecimal getSettamt() {
		return settamt;
	}


	public void setSettamt(BigDecimal settamt) {
		this.settamt = settamt;
	}


	public BigDecimal getSettbaseexchrate8() {
		return settbaseexchrate8;
	}


	public void setSettbaseexchrate8(BigDecimal settbaseexchrate8) {
		this.settbaseexchrate8 = settbaseexchrate8;
	}


	public String getSettbaseterms() {
		return settbaseterms;
	}


	public void setSettbaseterms(String settbaseterms) {
		this.settbaseterms = settbaseterms;
	}


	public BigDecimal getSettbaseamt() {
		return settbaseamt;
	}


	public void setSettbaseamt(BigDecimal settbaseamt) {
		this.settbaseamt = settbaseamt;
	}


	public BigDecimal getCcybaseamt() {
		return ccybaseamt;
	}


	public void setCcybaseamt(BigDecimal ccybaseamt) {
		this.ccybaseamt = ccybaseamt;
	}


	public String getDivno() {
		return divno;
	}


	public void setDivno(String divno) {
		this.divno = divno;
	}


	public String getVsecsacct() {
		return vsecsacct;
	}


	public void setVsecsacct(String vsecsacct) {
		this.vsecsacct = vsecsacct;
	}


	public Date getInitpurchdate() {
		return initpurchdate;
	}


	public void setInitpurchdate(Date initpurchdate) {
		this.initpurchdate = initpurchdate;
	}


	public String getConvind() {
		return convind;
	}


	public void setConvind(String convind) {
		this.convind = convind;
	}


	public String getExtbsktid() {
		return extbsktid;
	}


	public void setExtbsktid(String extbsktid) {
		this.extbsktid = extbsktid;
	}


	public String getExtconvid() {
		return extconvid;
	}


	public void setExtconvid(String extconvid) {
		this.extconvid = extconvid;
	}


	public String getAgreeno() {
		return agreeno;
	}


	public void setAgreeno(String agreeno) {
		this.agreeno = agreeno;
	}


	public String getPlgtype() {
		return plgtype;
	}


	public void setPlgtype(String plgtype) {
		this.plgtype = plgtype;
	}


	private static final long serialVersionUID = 1L;
}