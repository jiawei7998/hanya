package com.singlee.financial.wks.bean.opics;

import java.util.Date;
import lombok.Data;

@Data
public class Ycim {
    private String br;

    private String ccy;

    private String contribrate;

    private String product;

    private String prodtype;

    private String contcode;

    private String descr;

    private String ratetype;

    private String basis;

    private String freq;

    private String defaultflag;

    private Date lstmntdte;

    private String matvalind;

    private String interpmethod;

    private String derivefromfx;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getContribrate() {
		return contribrate;
	}

	public void setContribrate(String contribrate) {
		this.contribrate = contribrate;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getContcode() {
		return contcode;
	}

	public void setContcode(String contcode) {
		this.contcode = contcode;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getRatetype() {
		return ratetype;
	}

	public void setRatetype(String ratetype) {
		this.ratetype = ratetype;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getDefaultflag() {
		return defaultflag;
	}

	public void setDefaultflag(String defaultflag) {
		this.defaultflag = defaultflag;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}

	public String getMatvalind() {
		return matvalind;
	}

	public void setMatvalind(String matvalind) {
		this.matvalind = matvalind;
	}

	public String getInterpmethod() {
		return interpmethod;
	}

	public void setInterpmethod(String interpmethod) {
		this.interpmethod = interpmethod;
	}

	public String getDerivefromfx() {
		return derivefromfx;
	}

	public void setDerivefromfx(String derivefromfx) {
		this.derivefromfx = derivefromfx;
	}
    
    
}