package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Otcp;
import org.apache.ibatis.annotations.Param;

public interface OtcpMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("product") String product, @Param("prodtype") String prodtype);

    int insert(Otcp record);

    int insertSelective(Otcp record);

    Otcp selectByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno, @Param("seq") String seq, @Param("product") String product, @Param("prodtype") String prodtype);

    int updateByPrimaryKeySelective(Otcp record);

    int updateByPrimaryKey(Otcp record);
}