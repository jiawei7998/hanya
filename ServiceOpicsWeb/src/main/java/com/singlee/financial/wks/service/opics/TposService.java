package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Slda;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.bean.opics.Tpos;
public interface TposService{


    int deleteByPrimaryKey(String br,String cost,String ccy,String invtype,String port,String secid);

    int insert(Tpos record);

    int insertSelective(Tpos record);

    Tpos selectByPrimaryKey(String br,String cost,String ccy,String invtype,String port,String secid);
    Tpos selectByPrimaryKeySpsh(String br,String cost,String ccy,String invtype,String port,String secid,String product,String prodtype);

    int updateByPrimaryKeySelective(Tpos record);

    int updateByPrimaryKey(Tpos record);
    

    Tpos builder(Spsh spsh);
    Tpos builder(Slda slda);
}
