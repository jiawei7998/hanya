package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.wks.bean.opics.Trad;
import com.singlee.financial.wks.mapper.opics.TradMapper;
import com.singlee.financial.wks.service.opics.TradService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TradServiceImpl implements TradService {

    @Resource
    private TradMapper tradMapper;

    @Override
    public int deleteByPrimaryKey(String br,String trad) {
        return tradMapper.deleteByPrimaryKey(br,trad);
    }

    @Override
    public int insert(Trad record) {
        return tradMapper.insert(record);
    }

    @Override
    public int insertSelective(Trad record) {
        return tradMapper.insertSelective(record);
    }

    @Override
    public Trad selectByPrimaryKey(String br,String trad) {
        return tradMapper.selectByPrimaryKey(br,trad);
    }

    @Override
    public int updateByPrimaryKeySelective(Trad record) {
        return tradMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Trad record) {
        return tradMapper.updateByPrimaryKey(record);
    }

	@Override
	public Trad selectByCfetsTradId(String br, String trad) {
		return tradMapper.selectByCfetsTradId(br,trad);
	}

}
