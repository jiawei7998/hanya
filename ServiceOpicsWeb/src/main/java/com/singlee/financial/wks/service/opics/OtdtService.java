package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Otdt;
public interface OtdtService{


    int deleteByPrimaryKey(String br,String dealno,String seq);

    int insert(Otdt record);

    int insertSelective(Otdt record);

    Otdt selectByPrimaryKey(String br,String dealno,String seq);
    Otdt selectByTradeNo(String br, String dealtext,String dealStatus);

    int updateByPrimaryKeySelective(Otdt record);

    int updateByPrimaryKey(Otdt record);
    
    Otdt selectTriggerByTradeNo(String br, String dealtext,String dealStatus);

}
