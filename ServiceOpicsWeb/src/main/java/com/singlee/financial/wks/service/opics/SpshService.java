package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Spsh;
public interface SpshService{


    int deleteByPrimaryKey(String br,String dealno,String seq,String fixincind);

    int insert(Spsh record);

    int insertSelective(Spsh record);

    Spsh selectByPrimaryKey(String br,String dealno,String seq,String fixincind);

    Spsh selectByTradeNo(String br ,String dealtext);

    int updateByPrimaryKeySelective(Spsh record);

    int updateByPrimaryKey(Spsh record);

}
