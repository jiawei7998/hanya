package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Slda;
import com.singlee.financial.wks.mapper.opics.SldaMapper;
import com.singlee.financial.wks.service.opics.SldaService;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

@Service
public class SldaServiceImpl implements SldaService {

    @Resource
    private SldaMapper sldaMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br, String dealno, String seq, String dealind, String assignseq) {
        return sldaMapper.deleteByPrimaryKey(br, dealno, seq, dealind, assignseq);
    }

    @Override
    public int insert(Slda record) {
        return sldaMapper.insert(record);
    }

    @Override
    public int insertSelective(Slda record) {
        return sldaMapper.insertSelective(record);
    }

    @Override
    public Slda selectByPrimaryKey(String br, String dealno, String seq, String dealind, String assignseq) {
        return sldaMapper.selectByPrimaryKey(br, dealno, seq, dealind, assignseq);
    }
    @Override
    public List<Slda> selectByDealno(String br, String dealno) {
        return sldaMapper.selectByDealno(br, dealno);
    }

    @Override
    public int updateByPrimaryKeySelective(Slda record) {
        return sldaMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Slda record) {
        return sldaMapper.updateByPrimaryKey(record);
    }
    @Override
	public void batchInsert(List<Slda> list) {
		if (null == list || list.size() == 0) {
            return;
        }				
        batchDao.batch("com.singlee.financial.wks.mapper.opics.SldaMapper.insert", list);
	}
}

