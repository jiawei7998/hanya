package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Nupd;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface NupdMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("nos") String nos, @Param("dealno") String dealno,
        @Param("seq") String seq, @Param("product") String product, @Param("type") String type,
        @Param("vdate") Date vdate);
    int deleteNupdByVdate(@Param("br") String br, @Param("dealno") String dealno,
            @Param("product") String product, @Param("type") String type, @Param("vdate") Date vdate);
    int deleteReverseByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno,
            @Param("product") String product, @Param("type") String type, 
            @Param("postdate") Date postdate,@Param("mdate") Date mdate);
    int deleteReverseFxdhByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno,
            @Param("product") String product, @Param("type") String type, 
            @Param("postdate") Date postdate);
    int insert(Nupd record);

    int insertSelective(Nupd record);

    Nupd selectByPrimaryKey(@Param("br") String br, @Param("nos") String nos, @Param("dealno") String dealno,
        @Param("seq") String seq, @Param("product") String product, @Param("type") String type,
        @Param("vdate") Date vdate);
    List<Nupd> selectReverseByPrimaryKey(@Param("br") String br, @Param("dealno") String dealno,
            @Param("product") String product, @Param("type") String type);

    int updateByPrimaryKeySelective(Nupd record);

    int updateByPrimaryKey(Nupd record);
    
    void callSpDelrecNupdVdate(@Param("br") String br,@Param("dealno") String dealno,@Param("seq") String seq,@Param("product") String product,
    		@Param("type") String type,@Param("vdate") Date vdate,@Param("nos") String nos);
    
    void callSpAddrecNupd(@Param("br") String br, @Param("nos") String nos, @Param("dealno") String dealno, @Param("seq") String seq,
    		@Param("product") String product,@Param("type") String type,
    		@Param("cmne") String cmne, @Param("vdate") Date vdate, @Param("postdate") Date postdate,@Param("amtupd") BigDecimal amtupd);
}