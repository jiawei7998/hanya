package com.singlee.financial.wks.bean.cashflow;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.singlee.financial.wks.util.FinancialDateUtils;

public class DiscountFactorBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7767983779298580125L;

	/**
	 * 曲线名称
	 */
	private String yieldCurveName;
	
	/**
	 * 曲线币种
	 */
	private String currency;
	
	/**
	 * 批量日期
	 */
	private Date batchDate;
	
	/**
	 * 利率对应的期限日期
	 */
	private Date maturityDate;
 
	/**
	 * 利率对应的期限
	 */
	private String maturity;
	
	/**
	 * 利率
	 */
	private BigDecimal rate;
	
	/**
	 * 贴现因子
	 */
	private BigDecimal discountFactor;
	
	/**
	 * 是否为插值点,Y-插值期限点;N-市场标准点
	 */
	private String isInterpolation;
	
	/**
	 * 计息基础
	 */
	private String basis;
	
	/**
	 * 复利频率
	 */
	private String compoudingFrequency;
	

	public String getYieldCurveName() {
		return yieldCurveName;
	}

	public void setYieldCurveName(String yieldCurveName) {
		this.yieldCurveName = yieldCurveName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}

	public Date getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}

	public String getMaturity() {
		return maturity;
	}

	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}

	public BigDecimal getDiscountFactor() {
		return discountFactor;
	}

	public void setDiscountFactor(BigDecimal discountFactor) {
		this.discountFactor = discountFactor;
	}

	public String getIsInterpolation() {
		return isInterpolation;
	}

	public void setIsInterpolation(String isInterpolation) {
		this.isInterpolation = isInterpolation;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getCompoudingFrequency() {
		return compoudingFrequency;
	}

	public void setCompoudingFrequency(String compoudingFrequency) {
		this.compoudingFrequency = compoudingFrequency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((maturity == null) ? 0 : maturity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;}
		if (obj == null) {
			return false;}
		if (getClass() != obj.getClass()) {
			return false;}
		DiscountFactorBean other = (DiscountFactorBean) obj;
		if (maturity == null) {
			if (other.maturity != null) {
				return false;}
		} else if (!maturity.equals(other.maturity)) {
			return false;}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DiscountFactorBean [yieldCurveName=");
		builder.append(yieldCurveName);
		builder.append(", currency=");
		builder.append(currency);
		builder.append(", batchDate=");
		builder.append(FinancialDateUtils.format(batchDate, "yyyy-MM-dd"));
		builder.append(", maturityDate=");
		builder.append(FinancialDateUtils.format(maturityDate, "yyyy-MM-dd"));
		builder.append(", maturity=");
		builder.append(maturity);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", discountFactor=");
		builder.append(discountFactor);
		builder.append(", isInterpolation=");
		builder.append(isInterpolation);
		builder.append(", basis=");
		builder.append(basis);
		builder.append(", compoudingFrequency=");
		builder.append(compoudingFrequency);
		builder.append("]");
		
		return builder.toString();
	}
	
	
}
