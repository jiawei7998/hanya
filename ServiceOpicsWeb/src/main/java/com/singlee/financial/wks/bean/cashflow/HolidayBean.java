package com.singlee.financial.wks.bean.cashflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HolidayBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2944023048019938305L;

	/**
	 * 假日币种
	 */
	private String currency;
	
	private String type;
	
	private List<Date> holidayDates = new ArrayList<Date>();

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Date> getHolidayDates() {
		return this.holidayDates;
	}

	public void setHolidayDates(List<Date> holidayDates) {
		this.holidayDates = holidayDates;
	}

	public void addHolidayDate(Date holidayDate) {
		this.holidayDates.add(holidayDate);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HolidayBean [currency=");
		builder.append(currency);
		builder.append(", type=");
		builder.append(type);
		builder.append(", holidayDates=");
		builder.append(holidayDates);
		builder.append("]");
		return builder.toString();
	}
}
