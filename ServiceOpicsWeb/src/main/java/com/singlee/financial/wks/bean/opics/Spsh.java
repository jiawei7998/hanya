package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Spsh implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private String fixincind;

    private String aetrad;

    private String assignind;

    private BigDecimal bvamortamt;

    private BigDecimal bvintamt;

    private Date brprcindte;

    private String brok;

    private BigDecimal brokfeeamt;

    private String brokfeeccy;

    private BigDecimal chargeamt;

    private Date cleardate;

    private Date clearinputdate;

    private String clearoper;

    private BigDecimal commamt;

    private BigDecimal commrate8;

    private String comrefno;

    private Date confdate;

    private String confoper;

    private String cost;

    private String couponno;

    private Date ccyauthdate;

    private String ccyauthind;

    private String ccyauthoper;

    private String ccy;

    private String cno;

    private Date custconfdate;

    private String custrefno;

    private Date dealdate;

    private String dealsrce;

    private String dealtext;

    private String dealtime;

    private BigDecimal discamt;

    private BigDecimal discrate8;

    private BigDecimal disprice8;

    private BigDecimal faceamt;

    private Date faildate;

    private Date failinputdate;

    private String failoper;

    private BigDecimal failintamt;

    private BigDecimal feeamt;

    private BigDecimal gainamt;

    private Date inputdate;

    private String ioper;

    private String inputtime;

    private String invtype;

    private String linkorderno;

    private String linkseq;

    private BigDecimal lossamt;

    private String mutfundind;

    private BigDecimal origamt;

    private BigDecimal origqty;

    private String port;

    private BigDecimal premamt;

    private BigDecimal price8;

    private BigDecimal prinamt;

    private String prinagind;

    private BigDecimal proceedamt;

    private String product;

    private String prodtype;

    private String ps;

    private BigDecimal purchintamt;

    private BigDecimal qty;

    private Date revdate;

    private String revoper;

    private String revreason;

    private String revtext;

    private String revtime;

    private String secsacct;

    private Date secauthdate;

    private String secauthind;

    private String secauthoper;

    private String secid;

    private String series;

    private String ccysacct;

    private Date settdate;

    private String ccysmeans;

    private BigDecimal spreadrate8;

    private String suppconfind;

    private String symbolid;

    private BigDecimal vatamt;

    private String trad;

    private String verind;

    private Date verdate;

    private String voper;

    private String whenissind;

    private BigDecimal yield8;

    private String exchcno;

    private BigDecimal exchcommamt;

    private String exchccy;

    private String cnarr;

    private String fromdays;

    private String todays;

    private String delaydelivind;

    private String issuer;

    private String ratcno;

    private String crating;

    private String ratecode;

    private String agency;

    private BigDecimal whtamt;

    private String exchind;

    private BigDecimal offassamt;

    private BigDecimal estamt;

    private BigDecimal assignedqty;

    private BigDecimal convintamt;

    private BigDecimal convintbamt;

    private BigDecimal settamt;

    private BigDecimal settbaseamt;

    private BigDecimal costamt;

    private BigDecimal costbamt;

    private String intccy;

    private String settccy;

    private BigDecimal settprocexchrate8;

    private String settprocterms;

    private BigDecimal settprocpremdisc8;

    private BigDecimal intsettexchrate8;

    private String intsettterms;

    private BigDecimal intsettpremdisc8;

    private BigDecimal settbaseexchrate8;

    private String settbaseterms;

    private BigDecimal settbasepremdisc8;

    private BigDecimal intbaseexchrate8;

    private String intbaseterms;

    private BigDecimal intbasepremdisc8;

    private Date amenddate;

    private String amendtime;

    private String amendoper;

    private String fedealno;

    private BigDecimal internalyield8;

    private BigDecimal internaldiscrate8;

    private BigDecimal internalprice8;

    private BigDecimal internalproceedamt;

    private BigDecimal internalspreadrate8;

    private BigDecimal markupcommamt;

    private BigDecimal amortyield8;

    private String amortind;

    private String pledgeid;

    private BigDecimal pairedfaceamt;

    private BigDecimal pairedamt;

    private String pairedind;

    private String currsplitseq;

    private Long updatecounter;

    private String btbdealno;

    private String btbport;

    private String usualid;

    private String linkdealno;

    private String linkind;

    private String blockind;

    private String splitind;

    private BigDecimal indexratio8;

    private String swiftbothind;

    private String newissueind;

    private String ccpind;

    private String extcompare;

    private BigDecimal novfaceamt;

    private String paymentholdind;

    private String convind;

    private String extbsktid;

    private String extconvid;

    private String srind;

    private BigDecimal exciseamt;

    private String intraday;

    private static final long serialVersionUID = 1L;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDealno() {
		return dealno;
	}

	public void setDealno(String dealno) {
		this.dealno = dealno;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getFixincind() {
		return fixincind;
	}

	public void setFixincind(String fixincind) {
		this.fixincind = fixincind;
	}

	public String getAetrad() {
		return aetrad;
	}

	public void setAetrad(String aetrad) {
		this.aetrad = aetrad;
	}

	public String getAssignind() {
		return assignind;
	}

	public void setAssignind(String assignind) {
		this.assignind = assignind;
	}

	public BigDecimal getBvamortamt() {
		return bvamortamt;
	}

	public void setBvamortamt(BigDecimal bvamortamt) {
		this.bvamortamt = bvamortamt;
	}

	public BigDecimal getBvintamt() {
		return bvintamt;
	}

	public void setBvintamt(BigDecimal bvintamt) {
		this.bvintamt = bvintamt;
	}

	public Date getBrprcindte() {
		return brprcindte;
	}

	public void setBrprcindte(Date brprcindte) {
		this.brprcindte = brprcindte;
	}

	public String getBrok() {
		return brok;
	}

	public void setBrok(String brok) {
		this.brok = brok;
	}

	public BigDecimal getBrokfeeamt() {
		return brokfeeamt;
	}

	public void setBrokfeeamt(BigDecimal brokfeeamt) {
		this.brokfeeamt = brokfeeamt;
	}

	public String getBrokfeeccy() {
		return brokfeeccy;
	}

	public void setBrokfeeccy(String brokfeeccy) {
		this.brokfeeccy = brokfeeccy;
	}

	public BigDecimal getChargeamt() {
		return chargeamt;
	}

	public void setChargeamt(BigDecimal chargeamt) {
		this.chargeamt = chargeamt;
	}

	public Date getCleardate() {
		return cleardate;
	}

	public void setCleardate(Date cleardate) {
		this.cleardate = cleardate;
	}

	public Date getClearinputdate() {
		return clearinputdate;
	}

	public void setClearinputdate(Date clearinputdate) {
		this.clearinputdate = clearinputdate;
	}

	public String getClearoper() {
		return clearoper;
	}

	public void setClearoper(String clearoper) {
		this.clearoper = clearoper;
	}

	public BigDecimal getCommamt() {
		return commamt;
	}

	public void setCommamt(BigDecimal commamt) {
		this.commamt = commamt;
	}

	public BigDecimal getCommrate8() {
		return commrate8;
	}

	public void setCommrate8(BigDecimal commrate8) {
		this.commrate8 = commrate8;
	}

	public String getComrefno() {
		return comrefno;
	}

	public void setComrefno(String comrefno) {
		this.comrefno = comrefno;
	}

	public Date getConfdate() {
		return confdate;
	}

	public void setConfdate(Date confdate) {
		this.confdate = confdate;
	}

	public String getConfoper() {
		return confoper;
	}

	public void setConfoper(String confoper) {
		this.confoper = confoper;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getCouponno() {
		return couponno;
	}

	public void setCouponno(String couponno) {
		this.couponno = couponno;
	}

	public Date getCcyauthdate() {
		return ccyauthdate;
	}

	public void setCcyauthdate(Date ccyauthdate) {
		this.ccyauthdate = ccyauthdate;
	}

	public String getCcyauthind() {
		return ccyauthind;
	}

	public void setCcyauthind(String ccyauthind) {
		this.ccyauthind = ccyauthind;
	}

	public String getCcyauthoper() {
		return ccyauthoper;
	}

	public void setCcyauthoper(String ccyauthoper) {
		this.ccyauthoper = ccyauthoper;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getCno() {
		return cno;
	}

	public void setCno(String cno) {
		this.cno = cno;
	}

	public Date getCustconfdate() {
		return custconfdate;
	}

	public void setCustconfdate(Date custconfdate) {
		this.custconfdate = custconfdate;
	}

	public String getCustrefno() {
		return custrefno;
	}

	public void setCustrefno(String custrefno) {
		this.custrefno = custrefno;
	}

	public Date getDealdate() {
		return dealdate;
	}

	public void setDealdate(Date dealdate) {
		this.dealdate = dealdate;
	}

	public String getDealsrce() {
		return dealsrce;
	}

	public void setDealsrce(String dealsrce) {
		this.dealsrce = dealsrce;
	}

	public String getDealtext() {
		return dealtext;
	}

	public void setDealtext(String dealtext) {
		this.dealtext = dealtext;
	}

	public String getDealtime() {
		return dealtime;
	}

	public void setDealtime(String dealtime) {
		this.dealtime = dealtime;
	}

	public BigDecimal getDiscamt() {
		return discamt;
	}

	public void setDiscamt(BigDecimal discamt) {
		this.discamt = discamt;
	}

	public BigDecimal getDiscrate8() {
		return discrate8;
	}

	public void setDiscrate8(BigDecimal discrate8) {
		this.discrate8 = discrate8;
	}

	public BigDecimal getDisprice8() {
		return disprice8;
	}

	public void setDisprice8(BigDecimal disprice8) {
		this.disprice8 = disprice8;
	}

	public BigDecimal getFaceamt() {
		return faceamt;
	}

	public void setFaceamt(BigDecimal faceamt) {
		this.faceamt = faceamt;
	}

	public Date getFaildate() {
		return faildate;
	}

	public void setFaildate(Date faildate) {
		this.faildate = faildate;
	}

	public Date getFailinputdate() {
		return failinputdate;
	}

	public void setFailinputdate(Date failinputdate) {
		this.failinputdate = failinputdate;
	}

	public String getFailoper() {
		return failoper;
	}

	public void setFailoper(String failoper) {
		this.failoper = failoper;
	}

	public BigDecimal getFailintamt() {
		return failintamt;
	}

	public void setFailintamt(BigDecimal failintamt) {
		this.failintamt = failintamt;
	}

	public BigDecimal getFeeamt() {
		return feeamt;
	}

	public void setFeeamt(BigDecimal feeamt) {
		this.feeamt = feeamt;
	}

	public BigDecimal getGainamt() {
		return gainamt;
	}

	public void setGainamt(BigDecimal gainamt) {
		this.gainamt = gainamt;
	}

	public Date getInputdate() {
		return inputdate;
	}

	public void setInputdate(Date inputdate) {
		this.inputdate = inputdate;
	}

	public String getIoper() {
		return ioper;
	}

	public void setIoper(String ioper) {
		this.ioper = ioper;
	}

	public String getInputtime() {
		return inputtime;
	}

	public void setInputtime(String inputtime) {
		this.inputtime = inputtime;
	}

	public String getInvtype() {
		return invtype;
	}

	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}

	public String getLinkorderno() {
		return linkorderno;
	}

	public void setLinkorderno(String linkorderno) {
		this.linkorderno = linkorderno;
	}

	public String getLinkseq() {
		return linkseq;
	}

	public void setLinkseq(String linkseq) {
		this.linkseq = linkseq;
	}

	public BigDecimal getLossamt() {
		return lossamt;
	}

	public void setLossamt(BigDecimal lossamt) {
		this.lossamt = lossamt;
	}

	public String getMutfundind() {
		return mutfundind;
	}

	public void setMutfundind(String mutfundind) {
		this.mutfundind = mutfundind;
	}

	public BigDecimal getOrigamt() {
		return origamt;
	}

	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}

	public BigDecimal getOrigqty() {
		return origqty;
	}

	public void setOrigqty(BigDecimal origqty) {
		this.origqty = origqty;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public BigDecimal getPremamt() {
		return premamt;
	}

	public void setPremamt(BigDecimal premamt) {
		this.premamt = premamt;
	}

	public BigDecimal getPrice8() {
		return price8;
	}

	public void setPrice8(BigDecimal price8) {
		this.price8 = price8;
	}

	public BigDecimal getPrinamt() {
		return prinamt;
	}

	public void setPrinamt(BigDecimal prinamt) {
		this.prinamt = prinamt;
	}

	public String getPrinagind() {
		return prinagind;
	}

	public void setPrinagind(String prinagind) {
		this.prinagind = prinagind;
	}

	public BigDecimal getProceedamt() {
		return proceedamt;
	}

	public void setProceedamt(BigDecimal proceedamt) {
		this.proceedamt = proceedamt;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getPs() {
		return ps;
	}

	public void setPs(String ps) {
		this.ps = ps;
	}

	public BigDecimal getPurchintamt() {
		return purchintamt;
	}

	public void setPurchintamt(BigDecimal purchintamt) {
		this.purchintamt = purchintamt;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public Date getRevdate() {
		return revdate;
	}

	public void setRevdate(Date revdate) {
		this.revdate = revdate;
	}

	public String getRevoper() {
		return revoper;
	}

	public void setRevoper(String revoper) {
		this.revoper = revoper;
	}

	public String getRevreason() {
		return revreason;
	}

	public void setRevreason(String revreason) {
		this.revreason = revreason;
	}

	public String getRevtext() {
		return revtext;
	}

	public void setRevtext(String revtext) {
		this.revtext = revtext;
	}

	public String getRevtime() {
		return revtime;
	}

	public void setRevtime(String revtime) {
		this.revtime = revtime;
	}

	public String getSecsacct() {
		return secsacct;
	}

	public void setSecsacct(String secsacct) {
		this.secsacct = secsacct;
	}

	public Date getSecauthdate() {
		return secauthdate;
	}

	public void setSecauthdate(Date secauthdate) {
		this.secauthdate = secauthdate;
	}

	public String getSecauthind() {
		return secauthind;
	}

	public void setSecauthind(String secauthind) {
		this.secauthind = secauthind;
	}

	public String getSecauthoper() {
		return secauthoper;
	}

	public void setSecauthoper(String secauthoper) {
		this.secauthoper = secauthoper;
	}

	public String getSecid() {
		return secid;
	}

	public void setSecid(String secid) {
		this.secid = secid;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getCcysacct() {
		return ccysacct;
	}

	public void setCcysacct(String ccysacct) {
		this.ccysacct = ccysacct;
	}

	public Date getSettdate() {
		return settdate;
	}

	public void setSettdate(Date settdate) {
		this.settdate = settdate;
	}

	public String getCcysmeans() {
		return ccysmeans;
	}

	public void setCcysmeans(String ccysmeans) {
		this.ccysmeans = ccysmeans;
	}

	public BigDecimal getSpreadrate8() {
		return spreadrate8;
	}

	public void setSpreadrate8(BigDecimal spreadrate8) {
		this.spreadrate8 = spreadrate8;
	}

	public String getSuppconfind() {
		return suppconfind;
	}

	public void setSuppconfind(String suppconfind) {
		this.suppconfind = suppconfind;
	}

	public String getSymbolid() {
		return symbolid;
	}

	public void setSymbolid(String symbolid) {
		this.symbolid = symbolid;
	}

	public BigDecimal getVatamt() {
		return vatamt;
	}

	public void setVatamt(BigDecimal vatamt) {
		this.vatamt = vatamt;
	}

	public String getTrad() {
		return trad;
	}

	public void setTrad(String trad) {
		this.trad = trad;
	}

	public String getVerind() {
		return verind;
	}

	public void setVerind(String verind) {
		this.verind = verind;
	}

	public Date getVerdate() {
		return verdate;
	}

	public void setVerdate(Date verdate) {
		this.verdate = verdate;
	}

	public String getVoper() {
		return voper;
	}

	public void setVoper(String voper) {
		this.voper = voper;
	}

	public String getWhenissind() {
		return whenissind;
	}

	public void setWhenissind(String whenissind) {
		this.whenissind = whenissind;
	}

	public BigDecimal getYield8() {
		return yield8;
	}

	public void setYield8(BigDecimal yield8) {
		this.yield8 = yield8;
	}

	public String getExchcno() {
		return exchcno;
	}

	public void setExchcno(String exchcno) {
		this.exchcno = exchcno;
	}

	public BigDecimal getExchcommamt() {
		return exchcommamt;
	}

	public void setExchcommamt(BigDecimal exchcommamt) {
		this.exchcommamt = exchcommamt;
	}

	public String getExchccy() {
		return exchccy;
	}

	public void setExchccy(String exchccy) {
		this.exchccy = exchccy;
	}

	public String getCnarr() {
		return cnarr;
	}

	public void setCnarr(String cnarr) {
		this.cnarr = cnarr;
	}

	public String getFromdays() {
		return fromdays;
	}

	public void setFromdays(String fromdays) {
		this.fromdays = fromdays;
	}

	public String getTodays() {
		return todays;
	}

	public void setTodays(String todays) {
		this.todays = todays;
	}

	public String getDelaydelivind() {
		return delaydelivind;
	}

	public void setDelaydelivind(String delaydelivind) {
		this.delaydelivind = delaydelivind;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getRatcno() {
		return ratcno;
	}

	public void setRatcno(String ratcno) {
		this.ratcno = ratcno;
	}

	public String getCrating() {
		return crating;
	}

	public void setCrating(String crating) {
		this.crating = crating;
	}

	public String getRatecode() {
		return ratecode;
	}

	public void setRatecode(String ratecode) {
		this.ratecode = ratecode;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public BigDecimal getWhtamt() {
		return whtamt;
	}

	public void setWhtamt(BigDecimal whtamt) {
		this.whtamt = whtamt;
	}

	public String getExchind() {
		return exchind;
	}

	public void setExchind(String exchind) {
		this.exchind = exchind;
	}

	public BigDecimal getOffassamt() {
		return offassamt;
	}

	public void setOffassamt(BigDecimal offassamt) {
		this.offassamt = offassamt;
	}

	public BigDecimal getEstamt() {
		return estamt;
	}

	public void setEstamt(BigDecimal estamt) {
		this.estamt = estamt;
	}

	public BigDecimal getAssignedqty() {
		return assignedqty;
	}

	public void setAssignedqty(BigDecimal assignedqty) {
		this.assignedqty = assignedqty;
	}

	public BigDecimal getConvintamt() {
		return convintamt;
	}

	public void setConvintamt(BigDecimal convintamt) {
		this.convintamt = convintamt;
	}

	public BigDecimal getConvintbamt() {
		return convintbamt;
	}

	public void setConvintbamt(BigDecimal convintbamt) {
		this.convintbamt = convintbamt;
	}

	public BigDecimal getSettamt() {
		return settamt;
	}

	public void setSettamt(BigDecimal settamt) {
		this.settamt = settamt;
	}

	public BigDecimal getSettbaseamt() {
		return settbaseamt;
	}

	public void setSettbaseamt(BigDecimal settbaseamt) {
		this.settbaseamt = settbaseamt;
	}

	public BigDecimal getCostamt() {
		return costamt;
	}

	public void setCostamt(BigDecimal costamt) {
		this.costamt = costamt;
	}

	public BigDecimal getCostbamt() {
		return costbamt;
	}

	public void setCostbamt(BigDecimal costbamt) {
		this.costbamt = costbamt;
	}

	public String getIntccy() {
		return intccy;
	}

	public void setIntccy(String intccy) {
		this.intccy = intccy;
	}

	public String getSettccy() {
		return settccy;
	}

	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}

	public BigDecimal getSettprocexchrate8() {
		return settprocexchrate8;
	}

	public void setSettprocexchrate8(BigDecimal settprocexchrate8) {
		this.settprocexchrate8 = settprocexchrate8;
	}

	public String getSettprocterms() {
		return settprocterms;
	}

	public void setSettprocterms(String settprocterms) {
		this.settprocterms = settprocterms;
	}

	public BigDecimal getSettprocpremdisc8() {
		return settprocpremdisc8;
	}

	public void setSettprocpremdisc8(BigDecimal settprocpremdisc8) {
		this.settprocpremdisc8 = settprocpremdisc8;
	}

	public BigDecimal getIntsettexchrate8() {
		return intsettexchrate8;
	}

	public void setIntsettexchrate8(BigDecimal intsettexchrate8) {
		this.intsettexchrate8 = intsettexchrate8;
	}

	public String getIntsettterms() {
		return intsettterms;
	}

	public void setIntsettterms(String intsettterms) {
		this.intsettterms = intsettterms;
	}

	public BigDecimal getIntsettpremdisc8() {
		return intsettpremdisc8;
	}

	public void setIntsettpremdisc8(BigDecimal intsettpremdisc8) {
		this.intsettpremdisc8 = intsettpremdisc8;
	}

	public BigDecimal getSettbaseexchrate8() {
		return settbaseexchrate8;
	}

	public void setSettbaseexchrate8(BigDecimal settbaseexchrate8) {
		this.settbaseexchrate8 = settbaseexchrate8;
	}

	public String getSettbaseterms() {
		return settbaseterms;
	}

	public void setSettbaseterms(String settbaseterms) {
		this.settbaseterms = settbaseterms;
	}

	public BigDecimal getSettbasepremdisc8() {
		return settbasepremdisc8;
	}

	public void setSettbasepremdisc8(BigDecimal settbasepremdisc8) {
		this.settbasepremdisc8 = settbasepremdisc8;
	}

	public BigDecimal getIntbaseexchrate8() {
		return intbaseexchrate8;
	}

	public void setIntbaseexchrate8(BigDecimal intbaseexchrate8) {
		this.intbaseexchrate8 = intbaseexchrate8;
	}

	public String getIntbaseterms() {
		return intbaseterms;
	}

	public void setIntbaseterms(String intbaseterms) {
		this.intbaseterms = intbaseterms;
	}

	public BigDecimal getIntbasepremdisc8() {
		return intbasepremdisc8;
	}

	public void setIntbasepremdisc8(BigDecimal intbasepremdisc8) {
		this.intbasepremdisc8 = intbasepremdisc8;
	}

	public Date getAmenddate() {
		return amenddate;
	}

	public void setAmenddate(Date amenddate) {
		this.amenddate = amenddate;
	}

	public String getAmendtime() {
		return amendtime;
	}

	public void setAmendtime(String amendtime) {
		this.amendtime = amendtime;
	}

	public String getAmendoper() {
		return amendoper;
	}

	public void setAmendoper(String amendoper) {
		this.amendoper = amendoper;
	}

	public String getFedealno() {
		return fedealno;
	}

	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}

	public BigDecimal getInternalyield8() {
		return internalyield8;
	}

	public void setInternalyield8(BigDecimal internalyield8) {
		this.internalyield8 = internalyield8;
	}

	public BigDecimal getInternaldiscrate8() {
		return internaldiscrate8;
	}

	public void setInternaldiscrate8(BigDecimal internaldiscrate8) {
		this.internaldiscrate8 = internaldiscrate8;
	}

	public BigDecimal getInternalprice8() {
		return internalprice8;
	}

	public void setInternalprice8(BigDecimal internalprice8) {
		this.internalprice8 = internalprice8;
	}

	public BigDecimal getInternalproceedamt() {
		return internalproceedamt;
	}

	public void setInternalproceedamt(BigDecimal internalproceedamt) {
		this.internalproceedamt = internalproceedamt;
	}

	public BigDecimal getInternalspreadrate8() {
		return internalspreadrate8;
	}

	public void setInternalspreadrate8(BigDecimal internalspreadrate8) {
		this.internalspreadrate8 = internalspreadrate8;
	}

	public BigDecimal getMarkupcommamt() {
		return markupcommamt;
	}

	public void setMarkupcommamt(BigDecimal markupcommamt) {
		this.markupcommamt = markupcommamt;
	}

	public BigDecimal getAmortyield8() {
		return amortyield8;
	}

	public void setAmortyield8(BigDecimal amortyield8) {
		this.amortyield8 = amortyield8;
	}

	public String getAmortind() {
		return amortind;
	}

	public void setAmortind(String amortind) {
		this.amortind = amortind;
	}

	public String getPledgeid() {
		return pledgeid;
	}

	public void setPledgeid(String pledgeid) {
		this.pledgeid = pledgeid;
	}

	public BigDecimal getPairedfaceamt() {
		return pairedfaceamt;
	}

	public void setPairedfaceamt(BigDecimal pairedfaceamt) {
		this.pairedfaceamt = pairedfaceamt;
	}

	public BigDecimal getPairedamt() {
		return pairedamt;
	}

	public void setPairedamt(BigDecimal pairedamt) {
		this.pairedamt = pairedamt;
	}

	public String getPairedind() {
		return pairedind;
	}

	public void setPairedind(String pairedind) {
		this.pairedind = pairedind;
	}

	public String getCurrsplitseq() {
		return currsplitseq;
	}

	public void setCurrsplitseq(String currsplitseq) {
		this.currsplitseq = currsplitseq;
	}

	public Long getUpdatecounter() {
		return updatecounter;
	}

	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}

	public String getBtbdealno() {
		return btbdealno;
	}

	public void setBtbdealno(String btbdealno) {
		this.btbdealno = btbdealno;
	}

	public String getBtbport() {
		return btbport;
	}

	public void setBtbport(String btbport) {
		this.btbport = btbport;
	}

	public String getUsualid() {
		return usualid;
	}

	public void setUsualid(String usualid) {
		this.usualid = usualid;
	}

	public String getLinkdealno() {
		return linkdealno;
	}

	public void setLinkdealno(String linkdealno) {
		this.linkdealno = linkdealno;
	}

	public String getLinkind() {
		return linkind;
	}

	public void setLinkind(String linkind) {
		this.linkind = linkind;
	}

	public String getBlockind() {
		return blockind;
	}

	public void setBlockind(String blockind) {
		this.blockind = blockind;
	}

	public String getSplitind() {
		return splitind;
	}

	public void setSplitind(String splitind) {
		this.splitind = splitind;
	}

	public BigDecimal getIndexratio8() {
		return indexratio8;
	}

	public void setIndexratio8(BigDecimal indexratio8) {
		this.indexratio8 = indexratio8;
	}

	public String getSwiftbothind() {
		return swiftbothind;
	}

	public void setSwiftbothind(String swiftbothind) {
		this.swiftbothind = swiftbothind;
	}

	public String getNewissueind() {
		return newissueind;
	}

	public void setNewissueind(String newissueind) {
		this.newissueind = newissueind;
	}

	public String getCcpind() {
		return ccpind;
	}

	public void setCcpind(String ccpind) {
		this.ccpind = ccpind;
	}

	public String getExtcompare() {
		return extcompare;
	}

	public void setExtcompare(String extcompare) {
		this.extcompare = extcompare;
	}

	public BigDecimal getNovfaceamt() {
		return novfaceamt;
	}

	public void setNovfaceamt(BigDecimal novfaceamt) {
		this.novfaceamt = novfaceamt;
	}

	public String getPaymentholdind() {
		return paymentholdind;
	}

	public void setPaymentholdind(String paymentholdind) {
		this.paymentholdind = paymentholdind;
	}

	public String getConvind() {
		return convind;
	}

	public void setConvind(String convind) {
		this.convind = convind;
	}

	public String getExtbsktid() {
		return extbsktid;
	}

	public void setExtbsktid(String extbsktid) {
		this.extbsktid = extbsktid;
	}

	public String getExtconvid() {
		return extconvid;
	}

	public void setExtconvid(String extconvid) {
		this.extconvid = extconvid;
	}

	public String getSrind() {
		return srind;
	}

	public void setSrind(String srind) {
		this.srind = srind;
	}

	public BigDecimal getExciseamt() {
		return exciseamt;
	}

	public void setExciseamt(BigDecimal exciseamt) {
		this.exciseamt = exciseamt;
	}

	public String getIntraday() {
		return intraday;
	}

	public void setIntraday(String intraday) {
		this.intraday = intraday;
	}
    
    
}