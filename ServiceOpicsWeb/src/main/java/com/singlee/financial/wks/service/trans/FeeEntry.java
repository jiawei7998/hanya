package com.singlee.financial.wks.service.trans;

import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.FeeWksBean;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.util.FinancialFieldMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class FeeEntry {

	@Autowired
	private McfpService mcfpService;
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private PortService portService;
	@Autowired
	private CcodService ccodService;
	@Autowired
	private CostService costService;
	@Autowired
	private CustService custService;
	@Autowired
	private TradService tradService;
	@Autowired
	private FeesService feesService;
	@Autowired
	private FinancialFieldMap financialFieldMap;

	/**
	 * 交易信息检查
	 * @param feeWksBean
	 */
	public void DataCheck(FeeWksBean feeWksBean) {
		// 1.判断债券对象是否存在
		if (null == feeWksBean) {
			JY.raise("%s:%s,请补充完整FeeWksBean对象", WksErrorCode.FEES_NULL.getErrCode(),
					WksErrorCode.FEES_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(feeWksBean.getInstId())) {
			JY.raise("%s:%s,请填写feeWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NULL.getErrCode(),
					WksErrorCode.BR_NULL.getErrMsg());
		}
		// 3.查询分支/机构是否存在
		Brps brps = brpsService.selectByPrimaryKey(feeWksBean.getInstId());
		if (StringUtils.isEmpty(feeWksBean.getInstId()) || null == brps) {
			JY.raise("%s:%s,请正确输入feeWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 4.检查产品类型DESK字段拆分PROD_TYPE_PORT
		String[] book = StringUtils.split(feeWksBean.getBook(), "_");
		if (StringUtils.isEmpty(feeWksBean.getBook()) || book.length != 3 ) {
//			if (StringUtils.isEmpty(feeWksBean.getBook()) || book.length != 3 || book[0] != "SECLEN" ) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.book,For.[book=MM_BR_100]", WksErrorCode.DESK_FORM_ERR.getErrCode(),
					WksErrorCode.DESK_FORM_ERR.getErrMsg());
		}
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		if (StringUtils.isEmpty(book[0]) || StringUtils.isEmpty(book[1]) || null == type) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.book,For.[book=MM_BR_100]", WksErrorCode.DESK_NOT_FOUND.getErrCode(),
					WksErrorCode.DESK_NOT_FOUND.getErrMsg());
		}
		Port port = portService.selectByPrimaryKey(feeWksBean.getInstId(), feeWksBean.getDesk());// 投组组合
		if (StringUtils.isEmpty(feeWksBean.getDesk()) || null == port) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.desk,For.[desk=PORT]", WksErrorCode.DESK_PORT_ERR.getErrCode(),
					WksErrorCode.DESK_PORT_ERR.getErrMsg());
		}
		// 5.检查成本中心
		Cost cost = costService.selectByPrimaryKey(book[2]);
		if (StringUtils.isEmpty(book[2]) || null == cost) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.book,For.[book=MM_BR_100,或查询RATE表]",
					WksErrorCode.BOOK_NOT_FOUND.getErrCode(), WksErrorCode.BOOK_NOT_FOUND.getErrMsg());
		}
		// 6.检查交易对手
		Cust cust = custService.selectByPrimaryKey(feeWksBean.getCno());
		if (StringUtils.isEmpty(feeWksBean.getCno()) || null == cust) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.cno,For.[cno=1,或查询CUST表]", WksErrorCode.CUST_NOT_FOUND.getErrCode(),
					WksErrorCode.CUST_NOT_FOUND.getErrMsg());
		}
		// 7.检查交易员
		Trad trad = tradService.selectByPrimaryKey(feeWksBean.getInstId(), feeWksBean.getTrad());
		if (StringUtils.isNotEmpty(feeWksBean.getTrad()) && null == trad) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.trad,For.[trad=TRAD]", WksErrorCode.TRAD_NOT_FOUND.getErrCode(),
					WksErrorCode.TRAD_NOT_FOUND.getErrMsg());
		}
		// 8.校验币种是否存在
		Ccod ccod = ccodService.selectByPrimaryKey(feeWksBean.getCcy());
		if (StringUtils.isEmpty(feeWksBean.getCcy()) || null == ccod) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.ccy,For.[ccy=CNY]", WksErrorCode.CCY_NOT_FOUND.getErrCode(),
					WksErrorCode.CCY_NOT_FOUND.getErrMsg());
		}
		// 9.账户方法校验
		if (StringUtils.isEmpty(feeWksBean.getAcctgMethod())) {
			JY.raise("%s:%s,请输入正确格式feeWksBean.acctgMethod,For.[AcctgMethod=1]", WksErrorCode.PARAM_NULL.getErrCode(),
					WksErrorCode.PARAM_NULL.getErrMsg());
		}

	}

	/**
	 * 静态变量赋值
	 *
	 * @param fees
	 * @return
	 */
	public Fees DataTrans(Fees fees) {
		fees.setFeeseq(OpicsConstant.FEES.FEESEQ);// 费用序号
		fees.setSeq(OpicsConstant.FEES.SEQ);// 序号
		fees.setTenor(OpicsConstant.FEES.TENOR);// 期限,账务有关系
		fees.setVerind(OpicsConstant.FEES.VERIND);
		fees.setSettauthind(OpicsConstant.FEES.AUTHINGD);
		fees.setFeesind(OpicsConstant.FEES.FEESIND);
		fees.setVeroper(OpicsConstant.FEES.TRAD_NO);
		fees.setSettoper(OpicsConstant.FEES.TRAD_NO);
		fees.setUpdatecounter(OpicsConstant.FEES.UPDATE_COUNTER);// 更新次数
		fees.setAmortmethod(OpicsConstant.FEES.AMORT_METHOD);// 摊销方法
		fees.setDealccyamt(BigDecimal.ZERO);
		fees.setSettexchrate8(BigDecimal.ZERO);
		fees.setDealamtperiod(BigDecimal.ZERO);
		fees.setBaseamtperiod(BigDecimal.ZERO);
		fees.setBaseccyamt(BigDecimal.ZERO);
		return fees;
	}

	/**
	 * 费用交易数据转换
	 *
	 * @param fees
	 * @param feeWksBean
	 * @return
	 */
	public Fees DataTrans(Fees fees, FeeWksBean feeWksBean) {
		// 类型转换
		financialFieldMap.convertFieldvalue(feeWksBean);
		fees.setBr(feeWksBean.getInstId());// 机构编号
		// 获取当前账务日期
		Brps brps = brpsService.selectByPrimaryKey(fees.getBr());
		fees.setBrprcindte(brps.getBranprcdate());// 当前账务日期
		// 产品+类型
		String[] feeDir = StringUtils.split(feeWksBean.getFeeDir(), "|");
		fees.setFeeproduct(feeDir[0]);// 费用的产品
		fees.setFeeprodtype(feeDir[1]);// 费用的产品类型
		String[] book = StringUtils.split(feeWksBean.getBook(), "_");
		fees.setProduct(book[0]);//
		fees.setProdtype(book[1]);//
		Type type = typeService.selectByPrimaryKey(book[0], book[1]);// 查询产品类型
		fees.setCost(book[2]);// 交易的成本
		fees.setCcy(feeWksBean.getCcy());// 交易的货币
		fees.setCcyamt(getPlusNegateAmt(book[1],feeWksBean.getCcyAmt()));// 对应的费用金额

		fees.setCno(feeWksBean.getCno());// 交易对手
		fees.setAl(type.getAl());//根据SB SL获取资产负债标识
		fees.setDealno(feeWksBean.getTradeNo());

		if (StringUtils.equals(feeWksBean.getAcctgMethod(), "C")) {// 现金处理
			fees.setAcctgmethod(feeWksBean.getAcctgMethod());// 摊销计提方法
		} else {// 摊销处理方式
			fees.setAcctgmethod(feeWksBean.getAcctgMethod());
			fees.setAmortmethod("");// 摊销方式置为空
		}

		// 判断费用类型
		/*if (StringUtils.equals(feeWksBean.getAcctgMethod(), "C")) {// 现金处理
			fees.setAcctgmethod(feeWksBean.getAcctgMethod());// 摊销计提方法
		} else {// 摊销处理方式
			fees.setAmortmethod(feeWksBean.getAmortMethod());// 摊销方式
			if (fees.getAmortmethod().equals("C")) {// 判断是否时摊余固定收益
				fees.setBasis(feeWksBean.getBasis());// 计息基础
			}
		}*/

		fees.setStartdate(DateUtil.parse(feeWksBean.getStartDate()));// 费用开始
		fees.setEnddate(DateUtil.parse(feeWksBean.getEndDate()));// 费用结束时间
		fees.setVdate(DateUtil.parse(feeWksBean.getEndDate()));// 费用Vdate共用结束日期
		fees.setIoper(StringUtils.defaultIfEmpty(feeWksBean.getTrad(), OpicsConstant.FEES.TRAD_NO));// 操作员
		fees.setVerdate(fees.getBrprcindte());// 复核日期
		fees.setInputdate(fees.getBrprcindte());// 日期
		fees.setInputtime(DateUtil.getCurrentTimeAsString());// 时间
		fees.setSettmeans(feeWksBean.getSettmeans());// 清算方式
		fees.setSettacct(feeWksBean.getSettacct());// 清算账户
		fees.setSettauthdte(fees.getBrprcindte());// 授权日期
		fees.setSettoper(StringUtils.defaultIfEmpty(feeWksBean.getTrad(), OpicsConstant.FEES.TRAD_NO));
		fees.setLstmntdate(brps.getBranprcdate());
		fees.setFeeper8(feeWksBean.getFeePer8());// 费率
		//fees.setDealmdate(fees.getBrprcindte());// 处理时间
		fees.setPort(feeWksBean.getDesk());// 投资组合
		fees.setTrad(StringUtils.defaultIfEmpty(feeWksBean.getTrad(), OpicsConstant.FEES.TRAD_NO));
		fees.setDealdate(fees.getBrprcindte());// 交易日期
		fees.setSettamtperiod(fees.getCcyamt());
		return fees;
	}

	/**
	 * 获取dealno
	 *
	 * @param fees
	 * @param mcfpService
	 * @return
	 */
	public Fees DataTrans(Fees fees, McfpService mcfpService) {
		// 获取DEALNO
		Mcfp mcfp = mcfpService.selectByPrimaryKey(fees.getBr(), OpicsConstant.FEES.PROD_CODE,
				OpicsConstant.FEES.PROD_TYPE);// 设置交易编号
		String dealno = Integer.parseInt(StringUtils.trimToEmpty(mcfp.getTraddno())) + 1 + "";// 获取
		fees.setFeeno(dealno);// 设置交易编号
		return fees;
	}

	/**
	 * 更新交易单号 交易单号比较特殊,需要进行特殊处理
	 *
	 * @param fees
	 * @param mcfpService
	 * @param mcfp
	 */
	public void DataTrans(Fees fees, McfpService mcfpService, Mcfp mcfp) {
		mcfp.setBr(fees.getBr());
		mcfp.setTraddno(StringUtils.trimToEmpty(fees.getFeeno()));
		mcfp.setProdcode(OpicsConstant.FEES.PROD_CODE);// 产品代码 去掉空格
		mcfp.setType(OpicsConstant.FEES.PROD_TYPE);// 产品类型
		mcfpService.updateByPrimaryKeySelective(mcfp);// 更新数据库表状态
	}

	/**
	 * 其他产品调用增加费用
	 * @param feeWksBean
	 * @return
	 */
	public Fees saveEntry(FeeWksBean feeWksBean) {
		// 1.交易要素检查
		DataCheck(feeWksBean);
		// 2.交易转换
		Fees fees = DataTrans(new Fees());
		DataTrans(fees, feeWksBean);// 交易转换
		DataTrans(fees, mcfpService);// 交易编号
		feesService.insert(fees);// 保存数据
		// 3.更新dealno
		DataTrans(fees, mcfpService, new Mcfp());
		return fees;
	}


	/**
	 * 根据收付方向判断金额正负
	 */
	private BigDecimal getPlusNegateAmt(String drcrind, BigDecimal amt) {
		if (StringUtils.equals(drcrind, OpicsConstant.SLDH.PAY_IND)
				|| StringUtils.equals(drcrind, OpicsConstant.SLDH.PS_IND)
				|| StringUtils.equals(drcrind, OpicsConstant.SLDH.LIAB_IND)
			    || StringUtils.equals(drcrind, OpicsConstant.SLDH.PROD_TYPE_SB)) {
			return amt.setScale(2,BigDecimal.ROUND_HALF_UP).negate();// 取反
		} else {
			return amt.setScale(2,BigDecimal.ROUND_HALF_UP).plus();// 取正
		}
	}

	/**
	 * 添加利率互换交易费用记录
	 * @param swap
	 * @return
	 */
	public void saveEntry(Swdh swdh,List<Swdt> swdtList,List<Swds> swdsList) {
		int ret = 0;
		Fees fees = new Fees();
		Brps brps = brpsService.selectByPrimaryKey(StringUtils.trim(swdh.getBr()));

		fees.setBr(StringUtils.trim(swdh.getBr()));
		fees.setFeeseq("0");
		fees.setFeeproduct("FEES");
		fees.setFeeprodtype(swdh.getEtamt().doubleValue() > 0 ? "FC" : "FE");
		fees.setDealno(StringUtils.trim(swdh.getDealno()));
		fees.setSeq(StringUtils.trim(swdh.getEtlegseq()));
		fees.setProduct(StringUtils.trim(swdh.getProduct()));
		fees.setProdtype(StringUtils.trim(swdh.getProdtype()));
		fees.setCcy(StringUtils.trim(swdh.getEtccy()));
		fees.setCcyamt(swdh.getEtamt());
		fees.setCost(StringUtils.trim(swdh.getCost()));
		fees.setCno(StringUtils.trim(swdh.getCno()));
		fees.setTenor("99");
		fees.setAl(swdh.getEtamt().doubleValue() > 0 ? "L" : "A");
		fees.setAcctgmethod("C");
		fees.setVdate(swdh.getMatdate());
		fees.setIoper("SYS1");
		fees.setInputdate(DateUtil.parse(DateUtil.getCurrentDateAsString()));
		fees.setInputtime(DateUtil.getCurrentTimeAsString());
		fees.setBrprcindte(brps.getBranprcdate());
		fees.setVerind("1");
		fees.setVeroper("SYS1");
		fees.setVerdate(brps.getBranprcdate());
		fees.setSettmeans(StringUtils.trim(swdtList.get(0).getIntsmeans()));
		fees.setSettacct(StringUtils.trim(swdtList.get(0).getIntsacct()));
		fees.setSettauthind("1");
		fees.setSettauthdte(brps.getBranprcdate());
		fees.setLstmntdate(brps.getBranprcdate());
		fees.setFeesind("N");
		fees.setFeeper8(BigDecimal.ZERO);
		fees.setDealmdate(swdh.getMatdate());
		fees.setPort(StringUtils.trim(swdh.getPort()));
		fees.setTrad(StringUtils.trim(swdh.getTrad()));
		fees.setDealdate(swdh.getMatdate());
		fees.setSettexchrate8(BigDecimal.ZERO);
		fees.setSettamtperiod(BigDecimal.ZERO);
		fees.setDealamtperiod(BigDecimal.ZERO);
		fees.setBaseamtperiod(BigDecimal.ZERO);
		fees.setUpdatecounter(BigDecimal.ONE.longValue());
		fees.setFeetype("ET");
		fees.setBaseccyamt(BigDecimal.ZERO);
		fees.setDealvdate(swdh.getMatdate());
		fees.setDealccyamt(BigDecimal.ZERO);
		fees.setSettoper("SYS1");

		DataTrans(fees, mcfpService);// 交易编号
		ret = feesService.insert(fees);// 保存数据
		// 3.更新dealno
		DataTrans(fees, mcfpService, new Mcfp());
		if(ret == 0) {
			throw new RuntimeException("FEES信息添加失败");
		}//end if

		swdh.setEtfeeno(fees.getFeeno());
	}

}
