package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Cnfq;
import com.singlee.financial.wks.bean.opics.Dldt;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.bean.opics.Otdt;
import com.singlee.financial.wks.bean.opics.Prir;
import com.singlee.financial.wks.bean.opics.Psix;
import com.singlee.financial.wks.bean.opics.Rprh;
import com.singlee.financial.wks.bean.opics.Sldh;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.bean.opics.Swdh;
import com.singlee.financial.wks.expand.SwiftMethod;

import java.util.Date;
import java.util.List;

public interface CnfqService {

    int deleteByPrimaryKey(String br, String type, String dealno, String seq, String prodcode, Date cdate,
        String ctime);

    int deleteReverseByPrimaryKey(String br, String type, String dealno, String seq, String prodcode);
    
    int insert(Cnfq record);

    int insertSelective(Cnfq record);

    Cnfq selectByPrimaryKey(String br, String type, String dealno, String seq, String prodcode, Date cdate,
        String ctime);

    int updateByPrimaryKeySelective(Cnfq record);

    int updateByPrimaryKey(Cnfq record);

    void updateBatch(List<Cnfq> list);

    void batchInsert(List<Cnfq> list);

    /**
     * 方法已重载.根据拆借业务数据构建CNFQ对象
     * 
     * @param dldt
     *            拆借业务对象
     * @return
     */
    Cnfq bulider(Dldt dl);
    
    /**
     * 方法已重载.根据外汇业务数据构建CNFQ对象
     * 
     * @param fx
     * @return
     */
    Cnfq bulider(Fxdh fxdh,SwiftMethod swiftMethod);

    /**
     * 方法已重载.根据期权业务数据构建CNFQ对象
     * 
     * @param otc
     * @return
     */
    Cnfq bulider(Otdt otc);

    /**
     * 方法已重载.根据利率互换/货币互换业务数据构建CNFQ对象
     * 
     * @param swdh
     * @return
     */
    Cnfq bulider(Swdh swdh,Prir prir,Psix psix);
    /**
     * 方法已重载.根据现券买卖数据构建CNFQ对象
     * 
     * @param spsh
     *            现券买卖对象
     * @return
     */
    Cnfq bulider(Spsh spsh);
    /**
     * 方法已重载.根据回购数据构建CNFQ对象
     * @param rprh
     * @return
     */
    Cnfq bulider(Rprh rprh);
    
    Cnfq bulider(Sldh sldh);
}
