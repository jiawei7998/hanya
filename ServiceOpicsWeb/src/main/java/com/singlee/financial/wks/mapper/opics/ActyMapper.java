package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Acty;

public interface ActyMapper {
    /**
     * delete by primary key
     * @param acctngtype primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String acctngtype);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Acty record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Acty record);

    /**
     * select by primary key
     * @param acctngtype primary key
     * @return object by primary key
     */
    Acty selectByPrimaryKey(String acctngtype);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Acty record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Acty record);
}