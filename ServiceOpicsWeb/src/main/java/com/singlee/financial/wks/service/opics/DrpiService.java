package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Drpi;
public interface DrpiService{


    int deleteByPrimaryKey(String br,String product,String prodtype,String dealno,String seq,String delrecind);

    int insert(Drpi record);

    int insertSelective(Drpi record);

    Drpi selectByPrimaryKey(String br,String product,String prodtype,String dealno,String seq,String delrecind);

    int updateByPrimaryKeySelective(Drpi record);

    int updateByPrimaryKey(Drpi record);

}
