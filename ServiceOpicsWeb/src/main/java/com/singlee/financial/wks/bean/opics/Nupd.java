package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Nupd implements Serializable {
    private String br;

    private String nos;

    private String dealno;

    private String seq;

    private String product;

    private String type;

    private Date vdate;

    private Date postdate;

    private String cmne;

    private BigDecimal amtupd;
    

    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getNos() {
		return nos;
	}


	public void setNos(String nos) {
		this.nos = nos;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Date getVdate() {
		return vdate;
	}


	public void setVdate(Date vdate) {
		this.vdate = vdate;
	}


	public Date getPostdate() {
		return postdate;
	}


	public void setPostdate(Date postdate) {
		this.postdate = postdate;
	}


	public String getCmne() {
		return cmne;
	}


	public void setCmne(String cmne) {
		this.cmne = cmne;
	}


	public BigDecimal getAmtupd() {
		return amtupd;
	}


	public void setAmtupd(BigDecimal amtupd) {
		this.amtupd = amtupd;
	}


	private static final long serialVersionUID = 1L;
}