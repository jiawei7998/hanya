package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Prir;
import org.apache.ibatis.annotations.Param;

public interface PrirMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq);

    int insert(Prir record);

    int insertSelective(Prir record);

    Prir selectByPrimaryKey(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno, @Param("seq") String seq);

    int updateByPrimaryKeySelective(Prir record);

    int updateByPrimaryKey(Prir record);
    
    Prir selectByDealno(@Param("br") String br, @Param("product") String product, @Param("type") String type, @Param("dealno") String dealno);
}