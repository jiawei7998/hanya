package com.singlee.financial.wks.service.opics;

import java.util.Date;
import java.util.List;

import com.singlee.financial.wks.bean.opics.Schd;

public interface SchdService {

	int deleteByPrimaryKey(String br, String dealno, String seq, String product, String prodtype, Date intenddte,
			String schdseq, String schdtype);
	/**
	 * 计划删除
	 */
	int deleteSchdByIntEndDte(String br, String dealno, String seq, String product, String prodtype, Date intenddte);

	int insert(Schd record);

	int insertSelective(Schd record);

	Schd selectByPrimaryKey(String br, String dealno, String seq, String product, String prodtype, Date intenddte,
			String schdseq, String schdtype);

	int updateByPrimaryKeySelective(Schd record);

	int updateByPrimaryKey(Schd record);

	int updateReverseByPrimaryKey(Schd record);

	void updateBatch(List<Schd> list);

	void batchInsert(List<Schd> list);
}
