package com.singlee.financial.wks.service.opics.impl;


import com.singlee.financial.common.spring.mybatis.BatchDao;
import com.singlee.financial.wks.bean.opics.Fxdh;
import com.singlee.financial.wks.mapper.opics.FxdhMapper;
import com.singlee.financial.wks.service.opics.FxdhService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FxdhServiceImpl implements FxdhService {

    @Resource
    private FxdhMapper fxdhMapper;
    @Resource
    BatchDao batchDao;

    @Override
    public int deleteByPrimaryKey(String br, String dealno, String seq) {
        return fxdhMapper.deleteByPrimaryKey(br, dealno, seq);
    }

    @Override
    public int insert(Fxdh record) {
        return fxdhMapper.insert(record);
    }

    @Override
    public int insertSelective(Fxdh record) {
        return fxdhMapper.insertSelective(record);
    }

    @Override
    public Fxdh selectByPrimaryKey(String br, String dealno, String seq) {
        return fxdhMapper.selectByPrimaryKey(br, dealno, seq);
    }

    @Override
    public List<Fxdh> selectByTradeNo(String br, String dealtext,String dealStatus) {
        return fxdhMapper.selectByTradeNo(br, dealtext,dealStatus);
    }

    @Override
    public int updateByPrimaryKeySelective(Fxdh record) {
        return fxdhMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Fxdh record) {
        return fxdhMapper.updateByPrimaryKey(record);
    }

    @Override
    public void batchInsert(List<Fxdh> list) {
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.FxdhMapper.insert", list);

    }

    @Override
    public void updateBatch(List<Fxdh> list) {
        // TODO Auto-generated method stub
        if (null == list || list.size() == 0) {
            return;
        }
        batchDao.batch("com.singlee.financial.wks.mapper.opics.FxdhMapper.updateByPrimaryKeySelective", list);
    }

}
