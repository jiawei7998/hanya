package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Cust implements Serializable {
    private String cno;

    private String cmne;

    private String bic;

    private String ccode;

    private String sic;

    private String sn;

    private String cfn1;

    private String cfn2;

    private String ca1;

    private String ca2;

    private String ca3;

    private String ca4;

    private String ca5;

    private String ctype;

    private String cpost;

    private Date lstmntdte;

    private String uccode;

    private Date birthdate;

    private String taxid;

    private String acctngtype;

    
    public String getCno() {
		return cno;
	}


	public void setCno(String cno) {
		this.cno = cno;
	}


	public String getCmne() {
		return cmne;
	}


	public void setCmne(String cmne) {
		this.cmne = cmne;
	}


	public String getBic() {
		return bic;
	}


	public void setBic(String bic) {
		this.bic = bic;
	}


	public String getCcode() {
		return ccode;
	}


	public void setCcode(String ccode) {
		this.ccode = ccode;
	}


	public String getSic() {
		return sic;
	}


	public void setSic(String sic) {
		this.sic = sic;
	}


	public String getSn() {
		return sn;
	}


	public void setSn(String sn) {
		this.sn = sn;
	}


	public String getCfn1() {
		return cfn1;
	}


	public void setCfn1(String cfn1) {
		this.cfn1 = cfn1;
	}


	public String getCfn2() {
		return cfn2;
	}


	public void setCfn2(String cfn2) {
		this.cfn2 = cfn2;
	}


	public String getCa1() {
		return ca1;
	}


	public void setCa1(String ca1) {
		this.ca1 = ca1;
	}


	public String getCa2() {
		return ca2;
	}


	public void setCa2(String ca2) {
		this.ca2 = ca2;
	}


	public String getCa3() {
		return ca3;
	}


	public void setCa3(String ca3) {
		this.ca3 = ca3;
	}


	public String getCa4() {
		return ca4;
	}


	public void setCa4(String ca4) {
		this.ca4 = ca4;
	}


	public String getCa5() {
		return ca5;
	}


	public void setCa5(String ca5) {
		this.ca5 = ca5;
	}


	public String getCtype() {
		return ctype;
	}


	public void setCtype(String ctype) {
		this.ctype = ctype;
	}


	public String getCpost() {
		return cpost;
	}


	public void setCpost(String cpost) {
		this.cpost = cpost;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public String getUccode() {
		return uccode;
	}


	public void setUccode(String uccode) {
		this.uccode = uccode;
	}


	public Date getBirthdate() {
		return birthdate;
	}


	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}


	public String getTaxid() {
		return taxid;
	}


	public void setTaxid(String taxid) {
		this.taxid = taxid;
	}


	public String getAcctngtype() {
		return acctngtype;
	}


	public void setAcctngtype(String acctngtype) {
		this.acctngtype = acctngtype;
	}


	private static final long serialVersionUID = 1L;
}