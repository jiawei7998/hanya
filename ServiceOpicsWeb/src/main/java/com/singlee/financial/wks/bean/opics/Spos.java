package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Spos implements Serializable {
    private String br;

    private String ccy;

    private String port;

    private String secid;

    private Date settdate;

    private String invtype;

    private String cost;

    private BigDecimal amt;

    private Date lstmntdate;

    private BigDecimal qty;

    private BigDecimal purchqty;

    private BigDecimal purchavgcost;

    private BigDecimal saleqty;

    private BigDecimal saleavgcost;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getCcy() {
		return ccy;
	}


	public void setCcy(String ccy) {
		this.ccy = ccy;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getSecid() {
		return secid;
	}


	public void setSecid(String secid) {
		this.secid = secid;
	}


	public Date getSettdate() {
		return settdate;
	}


	public void setSettdate(Date settdate) {
		this.settdate = settdate;
	}


	public String getInvtype() {
		return invtype;
	}


	public void setInvtype(String invtype) {
		this.invtype = invtype;
	}


	public String getCost() {
		return cost;
	}


	public void setCost(String cost) {
		this.cost = cost;
	}


	public BigDecimal getAmt() {
		return amt;
	}


	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}


	public Date getLstmntdate() {
		return lstmntdate;
	}


	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}


	public BigDecimal getQty() {
		return qty;
	}


	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}


	public BigDecimal getPurchqty() {
		return purchqty;
	}


	public void setPurchqty(BigDecimal purchqty) {
		this.purchqty = purchqty;
	}


	public BigDecimal getPurchavgcost() {
		return purchavgcost;
	}


	public void setPurchavgcost(BigDecimal purchavgcost) {
		this.purchavgcost = purchavgcost;
	}


	public BigDecimal getSaleqty() {
		return saleqty;
	}


	public void setSaleqty(BigDecimal saleqty) {
		this.saleqty = saleqty;
	}


	public BigDecimal getSaleavgcost() {
		return saleavgcost;
	}


	public void setSaleavgcost(BigDecimal saleavgcost) {
		this.saleavgcost = saleavgcost;
	}


	private static final long serialVersionUID = 1L;
}