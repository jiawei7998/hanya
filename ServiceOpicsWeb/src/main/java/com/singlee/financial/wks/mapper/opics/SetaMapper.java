package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Seta;
import org.apache.ibatis.annotations.Param;

public interface SetaMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("smeans") String smeans, @Param("sacct") String sacct);

    int insert(Seta record);

    int insertSelective(Seta record);

    Seta selectByPrimaryKey(@Param("br") String br, @Param("smeans") String smeans, @Param("sacct") String sacct);
    
    Seta selectByPrimaryKeyAndCno(@Param("br") String br, @Param("smeans") String smeans, @Param("sacct") String sacct, @Param("cno") String cno);

    int updateByPrimaryKeySelective(Seta record);

    int updateByPrimaryKey(Seta record);
    
    Seta selectBySettacct(@Param("br") String br,@Param("sacct") String sacct);
    
    Seta selectBySettmeans(@Param("br") String br,@Param("smeans") String smeans);
}