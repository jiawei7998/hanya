package com.singlee.financial.wks.service.trans;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.RepoWksBean;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Rprh;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.RprhService;

/**
 * 交易冲销转换方法
 * @author xuqq
 *
 */
@Service
public class RepoReverse {

	@Autowired
	private RprhService rprhService;
	
	public void DataCheck(RepoWksBean repoWksBean) {
		// 1.判断拆借交易对象是否存在
		if (null == repoWksBean) {
			JY.raise("%s:%s,请补充完整SldhWksBean对象", WksErrorCode.REVERSE_NULL.getErrCode(), WksErrorCode.REVERSE_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(repoWksBean.getInstId())) {
			JY.raise("%s:%s,请填写RepoWksBean.InstId,For.[InstId=01]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		// 3.判断业务编号是否存在
		Rprh rprh = rprhService.selectByTradeNo(repoWksBean.getInstId(), repoWksBean.getTradeNo());
		if (StringUtils.isEmpty(repoWksBean.getTradeNo()) || null == rprh) {
			JY.raise("%s:%s,请填写RepoWksBean.TradeNo,For.[TradeNo=TRD20200101000001]",
					WksErrorCode.CBT_NOT_FOUND.getErrCode(), WksErrorCode.CBT_NOT_FOUND.getErrMsg());
		}
	}
	
	public Rprh DataTrans(Rprh rprh, BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(rprh.getBr());
		rprh.setRevreason("99");// 冲销参数:默认99
		rprh.setRevdate(brps.getBranprcdate());// 冲销日期
		rprh.setRevtext("99");// 冲销描述
		rprh.setRevoper(OpicsConstant.SPSH.TRAD_NO);// 冲销复核人员
		rprh.setUpdatecounter(rprh.getUpdatecounter() + 1);// 修改次数
		return rprh;
	}
}
