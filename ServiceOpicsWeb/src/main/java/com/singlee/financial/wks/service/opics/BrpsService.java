package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Brps;
public interface BrpsService{


    int deleteByPrimaryKey(String br);

    int insert(Brps record);

    int insertSelective(Brps record);

    Brps selectByPrimaryKey(String br);

    int updateByPrimaryKeySelective(Brps record);

    int updateByPrimaryKey(Brps record);

    boolean isBatchRunning(String br);
}
