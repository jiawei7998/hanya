package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Cusi;
import org.apache.ibatis.annotations.Param;

public interface CusiMapper {
    int deleteByPrimaryKey(@Param("custidtype") String custidtype, @Param("custaltid") String custaltid);

    int insert(Cusi record);

    int insertSelective(Cusi record);

    Cusi selectByPrimaryKey(@Param("custidtype") String custidtype, @Param("custaltid") String custaltid);

    int updateByPrimaryKeySelective(Cusi record);

    int updateByPrimaryKey(Cusi record);

}