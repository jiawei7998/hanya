package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Ddft;

import java.util.Date;

public interface DdftService{


    int deleteByPrimaryKey(String br,String ccy,String yieldcurve,String shiftseq,Date brprocdate,Date matdate,String quotetype);

    int insert(Ddft record);

    int insertSelective(Ddft record);

    Ddft selectByPrimaryKey(String br,String ccy,String yieldcurve,String shiftseq,Date brprocdate,Date matdate,String quotetype);

    int updateByPrimaryKeySelective(Ddft record);

    int updateByPrimaryKey(Ddft record);

    int updateBatch(List<Ddft> list);

    int updateBatchSelective(List<Ddft> list);

    /**
     * 查询当前批量日期是否有曲线数据
     * @param br
     * @param ccy
     * @param yieldcurve
     * @param shiftseq
     * @param brprocdate
     * @param quotetype
     * @return
     */
    int selectCountByBpdate(String br,Date brprocdate,String...ccy);
}
