package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Hldy implements Serializable {
    private String calendarid;

    private String holidaytype;

    private Date holidate;

    
    public String getCalendarid() {
		return calendarid;
	}


	public void setCalendarid(String calendarid) {
		this.calendarid = calendarid;
	}


	public String getHolidaytype() {
		return holidaytype;
	}


	public void setHolidaytype(String holidaytype) {
		this.holidaytype = holidaytype;
	}


	public Date getHolidate() {
		return holidate;
	}


	public void setHolidate(Date holidate) {
		this.holidate = holidate;
	}


	private static final long serialVersionUID = 1L;
}