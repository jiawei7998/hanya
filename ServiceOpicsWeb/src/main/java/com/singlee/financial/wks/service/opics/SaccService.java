package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Sacc;
public interface SaccService{


    int deleteByPrimaryKey(String br,String accountno);

    int insert(Sacc record);

    int insertSelective(Sacc record);

    Sacc selectByPrimaryKey(String br,String accountno);
    Sacc selectByPrimaryKeyAndCno(String br,String accountno,String cno);

    int updateByPrimaryKeySelective(Sacc record);

    int updateByPrimaryKey(Sacc record);

}
