package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Cost implements Serializable {
    private String costcent;

    private String costdesc;

    private Date lstmntdte;

    private String busunit;

    private String orgunit;

    
    
    public String getCostcent() {
		return costcent;
	}



	public void setCostcent(String costcent) {
		this.costcent = costcent;
	}



	public String getCostdesc() {
		return costdesc;
	}



	public void setCostdesc(String costdesc) {
		this.costdesc = costdesc;
	}



	public Date getLstmntdte() {
		return lstmntdte;
	}



	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}



	public String getBusunit() {
		return busunit;
	}



	public void setBusunit(String busunit) {
		this.busunit = busunit;
	}



	public String getOrgunit() {
		return orgunit;
	}



	public void setOrgunit(String orgunit) {
		this.orgunit = orgunit;
	}



	private static final long serialVersionUID = 1L;
}