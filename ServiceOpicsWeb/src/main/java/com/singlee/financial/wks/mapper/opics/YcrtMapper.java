package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.marketdata.bean.ImportYieldCurveBean;
import com.singlee.financial.wks.bean.opics.Ycrt;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YcrtMapper {
    /**
     * delete by primary key
     * @param br primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("contribrate") String contribrate, @Param("mtystart") String mtystart, @Param("mtyend") String mtyend);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Ycrt record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Ycrt record);

    /**
     * select by primary key
     * @param br primary key
     * @return object by primary key
     */
    Ycrt selectByPrimaryKey(@Param("br") String br, @Param("ccy") String ccy, @Param("contribrate") String contribrate, @Param("mtystart") String mtystart, @Param("mtyend") String mtyend);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Ycrt record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Ycrt record);

    /**
     *  search by record
     * @param record
     * @return
     */
    List<Ycrt> selectByAll(Ycrt record);
    /**
     * join with table YCIM
     * @return
     */
    List<Ycrt> selectAll();
    
    /**
     * 查询Opics中需要导入的收益率曲线信息
     * @return
     */
    List<ImportYieldCurveBean> selectOpicsYcrtData();

}