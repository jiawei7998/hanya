package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Ccod;
public interface CcodService{


    int deleteByPrimaryKey(String ccy);

    int insert(Ccod record);

    int insertSelective(Ccod record);

    Ccod selectByPrimaryKey(String ccy);

    int updateByPrimaryKeySelective(Ccod record);

    int updateByPrimaryKey(Ccod record);

}
