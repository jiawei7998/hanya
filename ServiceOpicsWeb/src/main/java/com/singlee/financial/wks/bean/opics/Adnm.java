package com.singlee.financial.wks.bean.opics;

import java.util.Date;
import lombok.Data;

@Data
public class Adnm {
    private String br;

    private String datatype;

    private String product;

    private String prodtype;

    private String fonumber;

    private String bonumber;

    private Date lstmntdate;

    private Long updatecounter;

    private String rfu1;

    private String rfu2;

    private String rfu3;

    private String rfu4;

    private String rfu5;

    private String rfu6;

    private String rfu7;

    private String rfu8;

	public String getBr() {
		return br;
	}

	public void setBr(String br) {
		this.br = br;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProdtype() {
		return prodtype;
	}

	public void setProdtype(String prodtype) {
		this.prodtype = prodtype;
	}

	public String getFonumber() {
		return fonumber;
	}

	public void setFonumber(String fonumber) {
		this.fonumber = fonumber;
	}

	public String getBonumber() {
		return bonumber;
	}

	public void setBonumber(String bonumber) {
		this.bonumber = bonumber;
	}

	public Date getLstmntdate() {
		return lstmntdate;
	}

	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}

	public Long getUpdatecounter() {
		return updatecounter;
	}

	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}

	public String getRfu1() {
		return rfu1;
	}

	public void setRfu1(String rfu1) {
		this.rfu1 = rfu1;
	}

	public String getRfu2() {
		return rfu2;
	}

	public void setRfu2(String rfu2) {
		this.rfu2 = rfu2;
	}

	public String getRfu3() {
		return rfu3;
	}

	public void setRfu3(String rfu3) {
		this.rfu3 = rfu3;
	}

	public String getRfu4() {
		return rfu4;
	}

	public void setRfu4(String rfu4) {
		this.rfu4 = rfu4;
	}

	public String getRfu5() {
		return rfu5;
	}

	public void setRfu5(String rfu5) {
		this.rfu5 = rfu5;
	}

	public String getRfu6() {
		return rfu6;
	}

	public void setRfu6(String rfu6) {
		this.rfu6 = rfu6;
	}

	public String getRfu7() {
		return rfu7;
	}

	public void setRfu7(String rfu7) {
		this.rfu7 = rfu7;
	}

	public String getRfu8() {
		return rfu8;
	}

	public void setRfu8(String rfu8) {
		this.rfu8 = rfu8;
	}
    
    
}