package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Cnfq implements Serializable {
    private String br;

    private String type;

    private String dealno;

    private String seq;

    private String prodcode;

    private Date cdate;

    private String ctime;

    private String textstat;

    private String formind;

    private String conftype;
    
    

    public String getBr() {
		return br;
	}



	public void setBr(String br) {
		this.br = br;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getDealno() {
		return dealno;
	}



	public void setDealno(String dealno) {
		this.dealno = dealno;
	}



	public String getSeq() {
		return seq;
	}



	public void setSeq(String seq) {
		this.seq = seq;
	}



	public String getProdcode() {
		return prodcode;
	}



	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}



	public Date getCdate() {
		return cdate;
	}



	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}



	public String getCtime() {
		return ctime;
	}



	public void setCtime(String ctime) {
		this.ctime = ctime;
	}



	public String getTextstat() {
		return textstat;
	}



	public void setTextstat(String textstat) {
		this.textstat = textstat;
	}



	public String getFormind() {
		return formind;
	}



	public void setFormind(String formind) {
		this.formind = formind;
	}



	public String getConftype() {
		return conftype;
	}



	public void setConftype(String conftype) {
		this.conftype = conftype;
	}



	private static final long serialVersionUID = 1L;
}