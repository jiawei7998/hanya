package com.singlee.financial.wks.mapper.cashflow;

import java.util.List;

import com.singlee.financial.wks.bean.cashflow.YieldCurveContributeBean;

public interface YieldCurveContributeMapper {

	/**
	 * 获取集合
	 * 
	 * @return
	 */
	List<YieldCurveContributeBean> selectYieldCurve(YieldCurveContributeBean contributeBean);
}
