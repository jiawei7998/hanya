package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Fxdt;

public interface FxdtService {

    int deleteByPrimaryKey(String br, String dealno, String seq);

    int insert(Fxdt record);

    int insertSelective(Fxdt record);

    Fxdt selectByPrimaryKey(String br, String dealno, String seq);

    int updateByPrimaryKeySelective(Fxdt record);

    int updateByPrimaryKey(Fxdt record);
    
    int updateReverseByPrimaryKey(Fxdt record);

    void updateBatch(List<Fxdt> list);

    void batchInsert(List<Fxdt> list);

}
