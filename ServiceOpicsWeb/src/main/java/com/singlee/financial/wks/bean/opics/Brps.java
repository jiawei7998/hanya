package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Brps implements Serializable {
    private String br;

    private String brstatus;

    private String brprocdate;

    private String prevbrproc;

    private Date lstmntdte;

    private String prvmenddte;

    private Date branprcdate;

    private Date prevbranprcdate;

    private Date prevmnthenddate;

    private String monthendday;

    private String yearendmonth;

    private Date monthenddate;

    private Date yearenddate;

    private Date nextbranprcdate;

    private Date prevprevbranprcdate;

    private Date nextmonthenddate;

    private Date prevyearenddate;

    private Date nextyearenddate;

    
    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getBrstatus() {
		return brstatus;
	}


	public void setBrstatus(String brstatus) {
		this.brstatus = brstatus;
	}


	public String getBrprocdate() {
		return brprocdate;
	}


	public void setBrprocdate(String brprocdate) {
		this.brprocdate = brprocdate;
	}


	public String getPrevbrproc() {
		return prevbrproc;
	}


	public void setPrevbrproc(String prevbrproc) {
		this.prevbrproc = prevbrproc;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public String getPrvmenddte() {
		return prvmenddte;
	}


	public void setPrvmenddte(String prvmenddte) {
		this.prvmenddte = prvmenddte;
	}


	public Date getBranprcdate() {
		return branprcdate;
	}


	public void setBranprcdate(Date branprcdate) {
		this.branprcdate = branprcdate;
	}


	public Date getPrevbranprcdate() {
		return prevbranprcdate;
	}


	public void setPrevbranprcdate(Date prevbranprcdate) {
		this.prevbranprcdate = prevbranprcdate;
	}


	public Date getPrevmnthenddate() {
		return prevmnthenddate;
	}


	public void setPrevmnthenddate(Date prevmnthenddate) {
		this.prevmnthenddate = prevmnthenddate;
	}


	public String getMonthendday() {
		return monthendday;
	}


	public void setMonthendday(String monthendday) {
		this.monthendday = monthendday;
	}


	public String getYearendmonth() {
		return yearendmonth;
	}


	public void setYearendmonth(String yearendmonth) {
		this.yearendmonth = yearendmonth;
	}


	public Date getMonthenddate() {
		return monthenddate;
	}


	public void setMonthenddate(Date monthenddate) {
		this.monthenddate = monthenddate;
	}


	public Date getYearenddate() {
		return yearenddate;
	}


	public void setYearenddate(Date yearenddate) {
		this.yearenddate = yearenddate;
	}


	public Date getNextbranprcdate() {
		return nextbranprcdate;
	}


	public void setNextbranprcdate(Date nextbranprcdate) {
		this.nextbranprcdate = nextbranprcdate;
	}


	public Date getPrevprevbranprcdate() {
		return prevprevbranprcdate;
	}


	public void setPrevprevbranprcdate(Date prevprevbranprcdate) {
		this.prevprevbranprcdate = prevprevbranprcdate;
	}


	public Date getNextmonthenddate() {
		return nextmonthenddate;
	}


	public void setNextmonthenddate(Date nextmonthenddate) {
		this.nextmonthenddate = nextmonthenddate;
	}


	public Date getPrevyearenddate() {
		return prevyearenddate;
	}


	public void setPrevyearenddate(Date prevyearenddate) {
		this.prevyearenddate = prevyearenddate;
	}


	public Date getNextyearenddate() {
		return nextyearenddate;
	}


	public void setNextyearenddate(Date nextyearenddate) {
		this.nextyearenddate = nextyearenddate;
	}


	private static final long serialVersionUID = 1L;
}