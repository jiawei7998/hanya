package com.singlee.financial.wks.service.opics.impl;

import com.singlee.financial.wks.bean.opics.Rpdt;
import com.singlee.financial.wks.bean.opics.Slda;
import com.singlee.financial.wks.bean.opics.Spos;
import com.singlee.financial.wks.bean.opics.Spsh;
import com.singlee.financial.wks.mapper.opics.SposMapper;
import com.singlee.financial.wks.service.opics.SposService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

@Service
public class SposServiceImpl implements SposService {

    @Resource
    private SposMapper sposMapper;

    @Override
    public int deleteByPrimaryKey(String br, String ccy, String port, String secid, Date settdate, String invtype, String cost) {
        return sposMapper.deleteByPrimaryKey(br, ccy, port, secid, settdate, invtype, cost);
    }

    @Override
    public int insert(Spos record) {
        return sposMapper.insert(record);
    }

    @Override
    public int insertSelective(Spos record) {
        return sposMapper.insertSelective(record);
    }

    @Override
    public Spos selectByPrimaryKey(String br, String ccy, String port, String secid, Date settdate, String invtype, String cost) {
        return sposMapper.selectByPrimaryKey(br, ccy, port, secid, settdate, invtype, cost);
    }

    @Override
    public int updateByPrimaryKeySelective(Spos record) {
        return sposMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Spos record) {
        return sposMapper.updateByPrimaryKey(record);
    }

    @Override
    public Spos builder(Spsh spsh) {
        Spos spos = DataTrans(new Spos());
        spos.setBr(spsh.getBr());//机构
        spos.setCcy(spsh.getCcy());//币种
        spos.setPort(spsh.getPort());//管理要素-
        spos.setSecid(spsh.getSecid());//债券号
        spos.setSettdate(spsh.getSettdate());//交割日
        spos.setInvtype(spsh.getInvtype());//管理要素-
        spos.setCost(spsh.getCost());//管理要素-
        spos.setAmt(spsh.getCostamt());//交易金额
        spos.setQty("P".equalsIgnoreCase(spsh.getPs()) ? spsh.getQty() : spsh.getQty().negate());//手数
        return spos;
    }


    @Override
    public Spos builder(Slda slda) {
        Spos spos = DataTrans(new Spos());
        spos.setBr(slda.getBr());//机构
        spos.setCcy(slda.getCcy());//币种
        spos.setPort(slda.getPort());//管理要素-
        spos.setSecid(slda.getSecid());//债券号
        spos.setInvtype(slda.getInvtype());//管理要素-
        spos.setCost(slda.getCost());//管理要素-
        return spos;
    }

    private Spos DataTrans(Spos spos) {
        spos.setLstmntdate(Calendar.getInstance().getTime());//修改日期
        spos.setPurchqty(BigDecimal.ZERO);//
        spos.setPurchavgcost(BigDecimal.ZERO);//
        spos.setSaleqty(BigDecimal.ZERO);//
        spos.setSaleavgcost(BigDecimal.ZERO);//
        return spos;
    }

    @Override
    public Spos builder(Rpdt rpdt) {
        Spos spos = DataTrans(new Spos());
        spos.setBr(rpdt.getBr());//机构
        spos.setCcy(rpdt.getCcy());//币种
        spos.setPort(rpdt.getPort());//管理要素-
        spos.setSecid(rpdt.getSecid());//债券号
        spos.setSettdate(rpdt.getMdate());//交割日
        spos.setInvtype(rpdt.getInvtype());//管理要素-
        spos.setCost(rpdt.getCost());//管理要素-
        spos.setAmt(rpdt.getMatprocamt());//交易金额
        spos.setQty("P".equalsIgnoreCase(rpdt.getPs()) ? rpdt.getQty() : rpdt.getQty().negate());//手数

        return spos;
    }

    @Override
    public void callSpUpdrecSposIncr(String br, String ccy, String port, String secid, Date settdate, String invtype,
                                     String cost, BigDecimal amt, Date lstmntdate, BigDecimal qty) {
        sposMapper.callSpUpdrecSposIncr(br, ccy, port, secid, settdate, invtype, cost, amt, lstmntdate, qty);
    }
}
