package com.singlee.financial.wks.service.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Rpdt;
public interface RpdtService{


    int deleteByPrimaryKey(String br,String dealno,String seq,String assignseq);

    int insert(Rpdt record);

    int insertSelective(Rpdt record);

    Rpdt selectByPrimaryKey(String br,String dealno,String seq,String assignseq);
    List<Rpdt> selectByDealno(String br,String dealno);

    int updateByPrimaryKeySelective(Rpdt record);

    int updateByPrimaryKey(Rpdt record);

    void batchInsert(List<Rpdt> list);
}
