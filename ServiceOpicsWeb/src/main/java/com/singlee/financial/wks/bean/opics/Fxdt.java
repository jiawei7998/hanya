package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class Fxdt implements Serializable {
    private String br;

    private String dealno;

    private String seq;

    private BigDecimal disaggccyrate8;

    private BigDecimal disaggccypd8;

    private BigDecimal disaggccyspread8;

    private String disaggccyterms;

    private String disaggccy;

    private String disaggctr;

    private String interbranchno;

    private String interbranchspottrad;

    private String interbranchfwdtrad;

    private String interbranchdealno;

    private String interbranchcorpdealno;

    private Date lstmntdte;

    private BigDecimal amount1;

    private BigDecimal amount2;

    private String trad1;

    private String trad2;

    private String trad3;

    private String trad4;

    private BigDecimal ccyintrate8;

    private BigDecimal ctrintrate8;

    private BigDecimal rate38;

    private BigDecimal rate48;

    private String ccylinkdealno;

    private String ctrlinkdealno;

    private Date date1;

    private Date date2;

    private String interbranchpldealno;

    private String interbranchcorppldealno;

    private String fedealno;

    private String fecorpdealno;

    private String fepldealno;

    private String fecorppldealno;

    private String interfacedealno;

    private Date ndffixdate;

    private String revtime;

    private String disaggcorpspottrad;

    private String disaggcorpspotport;

    private String disaggcorpspotcost;

    private String disaggcorpfwdtrad;

    private String disaggcorpfwdport;

    private String disaggcorpfwdcost;

    private Long updatecounter;

    private String interfaceserver;

    private BigDecimal dealccynpvamt;

    private BigDecimal dealctrnpvamt;

    private BigDecimal spotctrbamt;

    private BigDecimal spotctramt;

    private BigDecimal ccyfwdpremamt;

    private String fwdpremccy;

    private BigDecimal draftind;

    private String draftno;

    private BigDecimal draftblockind;

    private Date draftpaiddate;

    private String drafttext;
    

    public String getBr() {
		return br;
	}


	public void setBr(String br) {
		this.br = br;
	}


	public String getDealno() {
		return dealno;
	}


	public void setDealno(String dealno) {
		this.dealno = dealno;
	}


	public String getSeq() {
		return seq;
	}


	public void setSeq(String seq) {
		this.seq = seq;
	}


	public BigDecimal getDisaggccyrate8() {
		return disaggccyrate8;
	}


	public void setDisaggccyrate8(BigDecimal disaggccyrate8) {
		this.disaggccyrate8 = disaggccyrate8;
	}


	public BigDecimal getDisaggccypd8() {
		return disaggccypd8;
	}


	public void setDisaggccypd8(BigDecimal disaggccypd8) {
		this.disaggccypd8 = disaggccypd8;
	}


	public BigDecimal getDisaggccyspread8() {
		return disaggccyspread8;
	}


	public void setDisaggccyspread8(BigDecimal disaggccyspread8) {
		this.disaggccyspread8 = disaggccyspread8;
	}


	public String getDisaggccyterms() {
		return disaggccyterms;
	}


	public void setDisaggccyterms(String disaggccyterms) {
		this.disaggccyterms = disaggccyterms;
	}


	public String getDisaggccy() {
		return disaggccy;
	}


	public void setDisaggccy(String disaggccy) {
		this.disaggccy = disaggccy;
	}


	public String getDisaggctr() {
		return disaggctr;
	}


	public void setDisaggctr(String disaggctr) {
		this.disaggctr = disaggctr;
	}


	public String getInterbranchno() {
		return interbranchno;
	}


	public void setInterbranchno(String interbranchno) {
		this.interbranchno = interbranchno;
	}


	public String getInterbranchspottrad() {
		return interbranchspottrad;
	}


	public void setInterbranchspottrad(String interbranchspottrad) {
		this.interbranchspottrad = interbranchspottrad;
	}


	public String getInterbranchfwdtrad() {
		return interbranchfwdtrad;
	}


	public void setInterbranchfwdtrad(String interbranchfwdtrad) {
		this.interbranchfwdtrad = interbranchfwdtrad;
	}


	public String getInterbranchdealno() {
		return interbranchdealno;
	}


	public void setInterbranchdealno(String interbranchdealno) {
		this.interbranchdealno = interbranchdealno;
	}


	public String getInterbranchcorpdealno() {
		return interbranchcorpdealno;
	}


	public void setInterbranchcorpdealno(String interbranchcorpdealno) {
		this.interbranchcorpdealno = interbranchcorpdealno;
	}


	public Date getLstmntdte() {
		return lstmntdte;
	}


	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}


	public BigDecimal getAmount1() {
		return amount1;
	}


	public void setAmount1(BigDecimal amount1) {
		this.amount1 = amount1;
	}


	public BigDecimal getAmount2() {
		return amount2;
	}


	public void setAmount2(BigDecimal amount2) {
		this.amount2 = amount2;
	}


	public String getTrad1() {
		return trad1;
	}


	public void setTrad1(String trad1) {
		this.trad1 = trad1;
	}


	public String getTrad2() {
		return trad2;
	}


	public void setTrad2(String trad2) {
		this.trad2 = trad2;
	}


	public String getTrad3() {
		return trad3;
	}


	public void setTrad3(String trad3) {
		this.trad3 = trad3;
	}


	public String getTrad4() {
		return trad4;
	}


	public void setTrad4(String trad4) {
		this.trad4 = trad4;
	}


	public BigDecimal getCcyintrate8() {
		return ccyintrate8;
	}


	public void setCcyintrate8(BigDecimal ccyintrate8) {
		this.ccyintrate8 = ccyintrate8;
	}


	public BigDecimal getCtrintrate8() {
		return ctrintrate8;
	}


	public void setCtrintrate8(BigDecimal ctrintrate8) {
		this.ctrintrate8 = ctrintrate8;
	}


	public BigDecimal getRate38() {
		return rate38;
	}


	public void setRate38(BigDecimal rate38) {
		this.rate38 = rate38;
	}


	public BigDecimal getRate48() {
		return rate48;
	}


	public void setRate48(BigDecimal rate48) {
		this.rate48 = rate48;
	}


	public String getCcylinkdealno() {
		return ccylinkdealno;
	}


	public void setCcylinkdealno(String ccylinkdealno) {
		this.ccylinkdealno = ccylinkdealno;
	}


	public String getCtrlinkdealno() {
		return ctrlinkdealno;
	}


	public void setCtrlinkdealno(String ctrlinkdealno) {
		this.ctrlinkdealno = ctrlinkdealno;
	}


	public Date getDate1() {
		return date1;
	}


	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	public Date getDate2() {
		return date2;
	}


	public void setDate2(Date date2) {
		this.date2 = date2;
	}


	public String getInterbranchpldealno() {
		return interbranchpldealno;
	}


	public void setInterbranchpldealno(String interbranchpldealno) {
		this.interbranchpldealno = interbranchpldealno;
	}


	public String getInterbranchcorppldealno() {
		return interbranchcorppldealno;
	}


	public void setInterbranchcorppldealno(String interbranchcorppldealno) {
		this.interbranchcorppldealno = interbranchcorppldealno;
	}


	public String getFedealno() {
		return fedealno;
	}


	public void setFedealno(String fedealno) {
		this.fedealno = fedealno;
	}


	public String getFecorpdealno() {
		return fecorpdealno;
	}


	public void setFecorpdealno(String fecorpdealno) {
		this.fecorpdealno = fecorpdealno;
	}


	public String getFepldealno() {
		return fepldealno;
	}


	public void setFepldealno(String fepldealno) {
		this.fepldealno = fepldealno;
	}


	public String getFecorppldealno() {
		return fecorppldealno;
	}


	public void setFecorppldealno(String fecorppldealno) {
		this.fecorppldealno = fecorppldealno;
	}


	public String getInterfacedealno() {
		return interfacedealno;
	}


	public void setInterfacedealno(String interfacedealno) {
		this.interfacedealno = interfacedealno;
	}


	public Date getNdffixdate() {
		return ndffixdate;
	}


	public void setNdffixdate(Date ndffixdate) {
		this.ndffixdate = ndffixdate;
	}


	public String getRevtime() {
		return revtime;
	}


	public void setRevtime(String revtime) {
		this.revtime = revtime;
	}


	public String getDisaggcorpspottrad() {
		return disaggcorpspottrad;
	}


	public void setDisaggcorpspottrad(String disaggcorpspottrad) {
		this.disaggcorpspottrad = disaggcorpspottrad;
	}


	public String getDisaggcorpspotport() {
		return disaggcorpspotport;
	}


	public void setDisaggcorpspotport(String disaggcorpspotport) {
		this.disaggcorpspotport = disaggcorpspotport;
	}


	public String getDisaggcorpspotcost() {
		return disaggcorpspotcost;
	}


	public void setDisaggcorpspotcost(String disaggcorpspotcost) {
		this.disaggcorpspotcost = disaggcorpspotcost;
	}


	public String getDisaggcorpfwdtrad() {
		return disaggcorpfwdtrad;
	}


	public void setDisaggcorpfwdtrad(String disaggcorpfwdtrad) {
		this.disaggcorpfwdtrad = disaggcorpfwdtrad;
	}


	public String getDisaggcorpfwdport() {
		return disaggcorpfwdport;
	}


	public void setDisaggcorpfwdport(String disaggcorpfwdport) {
		this.disaggcorpfwdport = disaggcorpfwdport;
	}


	public String getDisaggcorpfwdcost() {
		return disaggcorpfwdcost;
	}


	public void setDisaggcorpfwdcost(String disaggcorpfwdcost) {
		this.disaggcorpfwdcost = disaggcorpfwdcost;
	}


	public Long getUpdatecounter() {
		return updatecounter;
	}


	public void setUpdatecounter(Long updatecounter) {
		this.updatecounter = updatecounter;
	}


	public String getInterfaceserver() {
		return interfaceserver;
	}


	public void setInterfaceserver(String interfaceserver) {
		this.interfaceserver = interfaceserver;
	}


	public BigDecimal getDealccynpvamt() {
		return dealccynpvamt;
	}


	public void setDealccynpvamt(BigDecimal dealccynpvamt) {
		this.dealccynpvamt = dealccynpvamt;
	}


	public BigDecimal getDealctrnpvamt() {
		return dealctrnpvamt;
	}


	public void setDealctrnpvamt(BigDecimal dealctrnpvamt) {
		this.dealctrnpvamt = dealctrnpvamt;
	}


	public BigDecimal getSpotctrbamt() {
		return spotctrbamt;
	}


	public void setSpotctrbamt(BigDecimal spotctrbamt) {
		this.spotctrbamt = spotctrbamt;
	}


	public BigDecimal getSpotctramt() {
		return spotctramt;
	}


	public void setSpotctramt(BigDecimal spotctramt) {
		this.spotctramt = spotctramt;
	}


	public BigDecimal getCcyfwdpremamt() {
		return ccyfwdpremamt;
	}


	public void setCcyfwdpremamt(BigDecimal ccyfwdpremamt) {
		this.ccyfwdpremamt = ccyfwdpremamt;
	}


	public String getFwdpremccy() {
		return fwdpremccy;
	}


	public void setFwdpremccy(String fwdpremccy) {
		this.fwdpremccy = fwdpremccy;
	}


	public BigDecimal getDraftind() {
		return draftind;
	}


	public void setDraftind(BigDecimal draftind) {
		this.draftind = draftind;
	}


	public String getDraftno() {
		return draftno;
	}


	public void setDraftno(String draftno) {
		this.draftno = draftno;
	}


	public BigDecimal getDraftblockind() {
		return draftblockind;
	}


	public void setDraftblockind(BigDecimal draftblockind) {
		this.draftblockind = draftblockind;
	}


	public Date getDraftpaiddate() {
		return draftpaiddate;
	}


	public void setDraftpaiddate(Date draftpaiddate) {
		this.draftpaiddate = draftpaiddate;
	}


	public String getDrafttext() {
		return drafttext;
	}


	public void setDrafttext(String drafttext) {
		this.drafttext = drafttext;
	}


	private static final long serialVersionUID = 1L;
}