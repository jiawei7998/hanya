package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Fees;
public interface FeesService{


    int deleteByPrimaryKey(String br,String feeno,String feeseq);

    int insert(Fees record);

    int insertSelective(Fees record);

    Fees selectByPrimaryKey(String br,String feeno,String feeseq);
    
    Fees selectByTradeNo(String br, String dealtext);

    int updateByPrimaryKeySelective(Fees record);

    int updateByPrimaryKey(Fees record);

}
