package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Otcp;
public interface OtcpService{


    int deleteByPrimaryKey(String br,String dealno,String seq,String product,String prodtype);

    int insert(Otcp record);

    int insertSelective(Otcp record);

    Otcp selectByPrimaryKey(String br,String dealno,String seq,String product,String prodtype);

    int updateByPrimaryKeySelective(Otcp record);

    int updateByPrimaryKey(Otcp record);

}
