package com.singlee.financial.wks.bean.opics;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

@Data
public class Bico implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String bic;

    private String sn;

    private String ba1;

    private String ba2;

    private String ba3;

    private String ba4;

    private String ba5;

    private Date lstmntdte;

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getBa1() {
		return ba1;
	}

	public void setBa1(String ba1) {
		this.ba1 = ba1;
	}

	public String getBa2() {
		return ba2;
	}

	public void setBa2(String ba2) {
		this.ba2 = ba2;
	}

	public String getBa3() {
		return ba3;
	}

	public void setBa3(String ba3) {
		this.ba3 = ba3;
	}

	public String getBa4() {
		return ba4;
	}

	public void setBa4(String ba4) {
		this.ba4 = ba4;
	}

	public String getBa5() {
		return ba5;
	}

	public void setBa5(String ba5) {
		this.ba5 = ba5;
	}

	public Date getLstmntdte() {
		return lstmntdte;
	}

	public void setLstmntdte(Date lstmntdte) {
		this.lstmntdte = lstmntdte;
	}
    
    
}