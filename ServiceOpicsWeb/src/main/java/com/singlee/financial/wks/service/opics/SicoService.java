package com.singlee.financial.wks.service.opics;

import com.singlee.financial.wks.bean.opics.Sico;
public interface SicoService{


    int deleteByPrimaryKey(String sic);

    int insert(Sico record);

    int insertSelective(Sico record);

    Sico selectByPrimaryKey(String sic);

    int updateByPrimaryKeySelective(Sico record);

    int updateByPrimaryKey(Sico record);

}
