package com.singlee.financial.wks.mapper.opics;

import com.singlee.financial.wks.bean.opics.Secl;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.singlee.financial.marketdata.bean.ImportSeclPriceBean;

public interface SeclMapper {
    int deleteByPrimaryKey(@Param("br") String br, @Param("secid") String secid);

    int insert(Secl record);

    int insertSelective(Secl record);

    Secl selectByPrimaryKey(@Param("br") String br, @Param("secid") String secid);

    int updateByPrimaryKeySelective(Secl record);

    int updateByPrimaryKey(Secl record);

    int updateBatch(List<Secl> list);

    int updateBatchSelective(List<Secl> list);
    
    List<Secl> selectAll();
    //市场数据
    List<Secl> selectSelAndSefmAll();
    
    /**
     * 查询OPICS中的债券收盘价信息
     * @return
     */
    List<ImportSeclPriceBean> selectOpicsSeclData();
    
    /**
     * 查询需要导入的债券信息
     * @return
     */
    List<ImportSeclPriceBean> selectImportSecurData();
}