package com.singlee.financial.wks.mapper.opics;

import java.util.List;

import com.singlee.financial.wks.bean.opics.Cust;

public interface CustMapper {
    int deleteByPrimaryKey(String cno);

    int insert(Cust record);

    int insertSelective(Cust record);

    Cust selectByPrimaryKey(String cno);
    
    List<Cust> selectAll();

    int updateByPrimaryKeySelective(Cust record);

    int updateByPrimaryKey(Cust record);

    Cust selectByCustaltid(String cno);
    
    Cust selectByCustCmne(String cmne);
    
    Cust selectByBr(String br);
}