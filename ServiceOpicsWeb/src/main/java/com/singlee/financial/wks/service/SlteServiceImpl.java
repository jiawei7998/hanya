package com.singlee.financial.wks.service;

import com.alibaba.fastjson.JSON;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.wks.bean.FeeWksBean;
import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.SldhWksBean;
import com.singlee.financial.wks.bean.opics.*;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.intfc.SeclenService;
import com.singlee.financial.wks.service.opics.*;
import com.singlee.financial.wks.service.trans.FeeEntry;
import com.singlee.financial.wks.service.trans.FeeReverse;
import com.singlee.financial.wks.service.trans.SlteEntry;
import com.singlee.financial.wks.service.trans.SlteReverse;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional(value = "transactionManager", rollbackFor = Exception.class)
public class SlteServiceImpl implements SeclenService {

    @Autowired
    private DrpiService drpiService;
    @Autowired
    private McfpService mcfpService;
    @Autowired
    private CnfqService cnfqService;
    @Autowired
    private NupdService nupdService;
    @Autowired
    private PmtqService pmtqService;
    @Autowired
    private PcixService pcixService;
    @Autowired
    private PsixService psixService;
    @Autowired
    private PrirService prirService;
    @Autowired
    private SposService sposService;
    @Autowired
    private BrpsService brpsService;
    @Autowired
    private SldhService sldhService;
    @Autowired
    private SldaService sldaService;
    @Autowired
    private SlteEntry slteEntry;// 债券借贷交易录入
    @Autowired
    private SlteReverse slteReverse;// 冲销转换
    @Autowired
    private FeeEntry feeEntry;
    @Autowired
    private FeeReverse feeReverse;
    @Autowired
    private FeesService feesService;
    @Autowired
    private FindService findService;

    private static Logger logger = Logger.getLogger(SlteServiceImpl.class);

    @Override
    public SlOutBean saveEntry(SldhWksBean slte) {
        SlOutBean outBean = new SlOutBean(RetStatusEnum.S, WksErrorCode.SUCCESS.getErrMsg(),
                WksErrorCode.SUCCESS.getErrMsg());
        Spos spos = null;// 日间头寸

        // 1.交易要素检查
        slteEntry.DataCheck(slte);
        Sldh sldh = slteEntry.DataTrans(new Sldh(), slte.getInstId(), mcfpService);
        // 2.处理处理 共用标的债信息映射，费用中需要关联交易数据
        FeeWksBean feeWksBean = slte.getFeesWksBean();
        feeWksBean.setTradeNo(sldh.getDealno());// 设置dealno
        feeWksBean.setSettmeans(StringUtils.defaultIfEmpty(slte.getSettmeans(), OpicsConstant.PCIX.DVP_MEANS));//债券借贷首次的
        feeWksBean.setSettacct(StringUtils.defaultIfEmpty(slte.getSettacct(), OpicsConstant.PCIX.DVP_MEANS));//
        Fees fees = feeEntry.saveEntry(feeWksBean);// 保存方法
        // 3.转换交易头表主体数据
        List<Sldh> sldhLst = slteEntry.DataTrans(fees, sldh, slte);
        sldhService.batchInsert(sldhLst);// 保存头表数据
        // 3.1 FIND表信息
        findService.insert(findService.bulider(fees));
        // 4.1 标的债 质押债信息填写
        List<Slda> sldaList = slteEntry.DataTrans(sldhLst, slte);// 设置标的债/质押债 信息
        sldaService.batchInsert(sldaList);
        // 5.1 转换drpi数据 债券路径
        Drpi drpi = slteEntry.DataTrans(new Drpi(), sldhLst);
        drpiService.insert(drpi);
        // 往来帐信息nupd
        List<Nupd> nupdLst = nupdService.builder(sldhLst.get(0));
        if (nupdLst.size() > 0) {
            nupdService.batchInsert(nupdLst);
        }
        // 5.收付款信息
        List<Pmtq> pmtqLst = pmtqService.builder(sldhLst.get(0));
        if (pmtqLst.size() > 0) {
            pmtqService.batchInsert(pmtqLst);
        }
        // 6.对手方清算信息
        List<Pcix> pcixLst = pcixService.builder(sldhLst.get(0), slte.getSettleAcct());
        if (pcixLst.size() > 0) {
            pcixService.batchInsert(pcixLst);
        }
        // 7.收款信息
        prirService.insert(prirService.builder(sldhLst.get(0)));
        // 8.付款信息
        psixService.insert(psixService.builder(sldhLst.get(0)));
        // 9.确认信息
        cnfqService.insert(cnfqService.bulider(sldhLst.get(0)));
        // 10.回填dealno
        slteEntry.DataTrans(sldhLst.get(0), mcfpService, new Mcfp());

        // 更新SPOS:SPOS当日无记录INSERT;有记录执行如下存储过程，更新语句
        for (Slda slda : sldaList) {
            //获取 brps
            Brps brps = brpsService.selectByPrimaryKey(slda.getBr());
            // 起息日倒期日
            spos = sposService.selectByPrimaryKey(slda.getBr(), slda.getCcy(), slda.getPort(), slda.getSecid(),
                    slda.getVdate().compareTo(brps.getPrevbranprcdate()) < 0 ? brps.getPrevbranprcdate() : slda.getVdate(), slda.getInvtype(), slda.getCost());

            //数量与方向有关
            BigDecimal qty = "P".equalsIgnoreCase(slda.getPs()) ? slda.getQty() : slda.getQty().negate();
            Spos tmp = sposService.builder(slda);
            if (spos == null) {
                tmp.setSettdate(slda.getVdate().compareTo(brps.getPrevbranprcdate()) < 0 ? brps.getPrevbranprcdate() : slda.getVdate());
                tmp.setAmt(slda.getComprocamt());
                tmp.setQty(qty);
                sposService.insert(tmp);
            } else {
                // 调用存储过程SP_UPDREC_SPOS_INCR
                sposService.callSpUpdrecSposIncr(slda.getBr(), slda.getCcy(), slda.getPort(), slda.getSecid(),
                        slda.getVdate().compareTo(brps.getPrevbranprcdate()) < 0 ? brps.getPrevbranprcdate() : slda.getVdate(),
                        slda.getInvtype(), slda.getCost(),
                        slda.getComprocamt(),
                        Calendar.getInstance().getTime(),
                        qty);
            }
            // 完全倒起息日
            spos = sposService.selectByPrimaryKey(slda.getBr(), slda.getCcy(), slda.getPort(), slda.getSecid(),
                    slda.getMdate().compareTo(brps.getPrevbranprcdate()) < 0 ? brps.getPrevbranprcdate() : slda.getMdate(), slda.getInvtype(), slda.getCost());
            if (spos == null) {
                tmp.setSettdate(slda.getMdate().compareTo(brps.getPrevbranprcdate()) < 0 ? brps.getPrevbranprcdate() : slda.getMdate());
                tmp.setAmt(slda.getComprocamt().negate());
                tmp.setQty(qty.negate());
                sposService.insert(tmp);
            } else {
                // 调用存储过程SP_UPDREC_SPOS_INCR
                sposService.callSpUpdrecSposIncr(slda.getBr(), slda.getCcy(), slda.getPort(), slda.getSecid(),
                        slda.getMdate().compareTo(brps.getPrevbranprcdate()) < 0 ? brps.getPrevbranprcdate() : slda.getMdate(),
                        slda.getInvtype(), slda.getCost(),
                        slda.getComprocamt().negate(),
                        Calendar.getInstance().getTime(),
                        qty.negate());
            }
        }
        outBean.setClientNo(sldhLst.get(0).getDealno());
        logger.info("债券借贷录入处理完成,返回消息:" + JSON.toJSONString(outBean));
        return outBean;
    }

    @Override
    public SlOutBean reverseTrading(SldhWksBean sldhWksBean) {
        // TODO Auto-generated method stub、
        SlOutBean outBean = new SlOutBean(RetStatusEnum.S, "SLTE-00000", "债券借贷交易冲销成功!");
        List<Sldh> sldhList = null;
        Sldh sldh = null;
        try {
            // sldhWksBean.getTradeNo()为opics dealno
            slteReverse.DataCheck(sldhWksBean);
            // 1.查询交易
            sldhList = sldhService.selectByTradeNo(sldhWksBean.getInstId(), sldhWksBean.getTradeNo());
            // 2.修改SLDH
            for (Sldh sl : sldhList) {
                slteReverse.DataTrans(sl, brpsService);
                sldhService.updateByPrimaryKey(sl);
            }
            sldh = sldhList.get(0);
            // 3.冲销SLDA
            List<Slda> sldaList = sldaService.selectByDealno(sldhList.get(0).getBr(), sldhList.get(0).getDealno());
            for (Slda slda : sldaList) {
                slteReverse.DataTrans(slda, brpsService);
                sldaService.updateByPrimaryKey(slda);
            }
            // 4.冲销fees费用
            Fees fees = feeReverse.reverseTrading(sldhWksBean.getFeesWksBean());
            feesService.updateByPrimaryKeySelective(fees);// 更新费用
            // 5.1删除NUPD的记录
            nupdService.callSpDelrecNupdVdate(sldh.getBr(), sldh.getDealno(), sldh.getSeq(), sldh.getProduct(),
                    sldh.getProdtype(), sldh.getVdate(), sldh.getComccysacct());
            // 5.2根据冲销时间节点新增NUPD
            nupdService.callSpAddrecNupd(sldh.getBr(), sldh.getComccysacct(), sldh.getDealno(), sldh.getSeq(),
                    sldh.getProduct(), sldh.getProdtype(), sldh.getCno().trim(), sldh.getVdate(), sldh.getRevdate(),
                    sldh.getComprocamt().negate());

            // 6.1删除大于新到期日的记录
            pmtqService.callSpUpdrecPmtqSwiftfmt(sldh.getBr(), sldh.getDealno(), sldh.getSeq(), sldh.getProduct(),
                    sldh.getProdtype(), sldh.getRevdate(), DateUtil.getCurrentTimeAsString(), "", "", "", "", null,
                    BigDecimal.ZERO);
            // 7.更新日间头寸编号
            for (Slda slda : sldaList) {
                // 更新SPOS头寸
                sposService.callSpUpdrecSposIncr(slda.getBr(), slda.getCcy(), slda.getPort(), slda.getSecid(),
                        slda.getVdate(), slda.getInvtype(), slda.getCost(),
                        "P".equalsIgnoreCase(slda.getPs()) ? slda.getComprocamt() : slda.getComprocamt().negate(),
                        Calendar.getInstance().getTime(),
                        "P".equalsIgnoreCase(slda.getPs()) ? slda.getQty().negate() : slda.getQty());
            }

        } catch (Exception e) {
            outBean.setRetStatus(RetStatusEnum.F);
            outBean.setRetMsg(e.getMessage());
            logger.error("债券借贷交易冲销错误", e);
            throw e;
        }
        outBean.setClientNo(sldh.getDealno());
        logger.info("债券借贷交易冲销处理完成,返回消息:" + JSON.toJSONString(outBean));
        return outBean;
    }

}
