package com.singlee.financial.wks.service.trans;

import com.singlee.financial.wks.bean.OpicsConstant;
import com.singlee.financial.wks.bean.RdfhWksBean;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Rdfh;
import com.singlee.financial.wks.service.opics.BrpsService;
import org.springframework.stereotype.Component;

/**
 * 债券收息转换方法
 * @author xuqq
 *
 */
@Component
public class RdfhEntry {

	/**
	 * 收息转换
	 * @param rdfh
	 * @param cbt
	 * @param spsh
	 * @param brpsService
	 * @return
	 */
	public Rdfh DataTrans(Rdfh rdfh, RdfhWksBean rdfhWksBean, BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(rdfh.getBr());
		
		rdfh.setVeroper(OpicsConstant.SPSH.VOPER);
		rdfh.setVerind(OpicsConstant.SPSH.VERIND);
		rdfh.setVerdate(brps.getBranprcdate());
		rdfh.setCcyauthdate(brps.getBranprcdate());
		rdfh.setCcyauthind(OpicsConstant.SPSH.CCYAUTHIND);
		rdfh.setCcyauthoper(OpicsConstant.SPSH.VOPER);
		
		if(!"".equals(rdfhWksBean.getCcysmeans())) {
			rdfh.setCcysmeans(rdfhWksBean.getCcysmeans());
		}
		if(!"".equals(rdfhWksBean.getCcysacct())) {
			rdfh.setCcysacct(rdfhWksBean.getCcysacct());
		}
		return rdfh;
	}
	
	/**
	 * 冲销收息转换
	 * @param rdfh
	 * @param spsh
	 * @param brpsService
	 * @return
	 */
	public Rdfh DataTrans(Rdfh rdfh,BrpsService brpsService) {
		Brps brps = brpsService.selectByPrimaryKey(rdfh.getBr());
		
		rdfh.setRevdate(brps.getBranprcdate());
		rdfh.setRevoper(OpicsConstant.SPSH.VOPER);
		rdfh.setRevreason(OpicsConstant.SPSH.REVREASON);
		rdfh.setRevtext(OpicsConstant.SPSH.REVTEXT);
		
		return rdfh;
	}
}
