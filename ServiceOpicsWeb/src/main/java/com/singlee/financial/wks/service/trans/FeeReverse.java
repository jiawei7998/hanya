package com.singlee.financial.wks.service.trans;

import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.singlee.financial.common.util.JY;
import com.singlee.financial.wks.bean.FeeWksBean;
import com.singlee.financial.wks.bean.opics.Brps;
import com.singlee.financial.wks.bean.opics.Fees;
import com.singlee.financial.wks.expand.WksErrorCode;
import com.singlee.financial.wks.service.opics.BrpsService;
import com.singlee.financial.wks.service.opics.FeesService;
import com.singlee.financial.wks.util.FinancialBeanHelper;

@Component
public class FeeReverse {
	@Autowired
	private BrpsService brpsService;
	@Autowired
	private FeesService feesService;

	/**
	 * 交易信息检查
	 *
	 * @param cbt
	 */
	public void DataCheck(FeeWksBean feeWksBean) {
		// 1.判断拆借交易对象是否存在
		if (null == feeWksBean) {
			JY.raise("%s:%s,请补充完整feeWksBean对象", WksErrorCode.IBO_NULL.getErrCode(), WksErrorCode.IBO_NULL.getErrMsg());
		}
		// 2.判断拆借交易机构是否存在
		if (StringUtils.isEmpty(feeWksBean.getInstId())) {
			JY.raise("%s:%s,请填写feeWksBean.InstId,For.["+feeWksBean.getInstId()+"]", WksErrorCode.BR_NOT_FOUND.getErrCode(),
					WksErrorCode.BR_NOT_FOUND.getErrMsg());
		}
		Fees fees = feesService.selectByTradeNo(feeWksBean.getInstId(), feeWksBean.getTradeNo());
		if (StringUtils.isEmpty(feeWksBean.getTradeNo()) || null == fees) {
			JY.raise("%s:%s,请填写IboWksBean.TradeNo,For.["+feeWksBean.getTradeNo()+"]",
					WksErrorCode.FEES_NOT_FOUND.getErrCode(), WksErrorCode.FEES_NOT_FOUND.getErrMsg());
		}
	}

	/**
	 * 费用交易数据转换
	 * 
	 * @param feeWksBean
	 * @return
	 */
	public Fees DataTrans(Fees fees, FeeWksBean feeWksBean) {
		Brps brps = brpsService.selectByPrimaryKey(fees.getBr());
		fees.setRevreason("99");
		fees.setRevdate(brps.getBranprcdate());
		fees.setRevtext(feeWksBean.getRevtext());
		fees.setUpdatecounter(fees.getUpdatecounter() + 1);// 修改次数
		fees.setLstmntdate(Calendar.getInstance().getTime());// 最后修改日期
		return fees;
	}

	/**
	 * 其他产品调用费用冲销
	 * 
	 * @param feeWksBean
	 * @return
	 */
	public Fees reverseTrading(FeeWksBean feeWksBean) {
		// 1.检查要素
		DataCheck(feeWksBean);
		// 2.查询原始交易
		Fees fees = feesService.selectByTradeNo(feeWksBean.getInstId(), feeWksBean.getTradeNo());
		// 去掉类空格
		FinancialBeanHelper.TrimBeanAttrValue(fees);
		// 2.交易更新
		DataTrans(fees, feeWksBean);
		return fees;
	}
}
