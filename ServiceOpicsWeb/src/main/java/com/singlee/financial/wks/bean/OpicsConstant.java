package com.singlee.financial.wks.bean;

import java.util.Arrays;
import java.util.List;

public class OpicsConstant {

	public interface BRRD {
		public static final String BASE_CCY = "CNY";
		public static final String LOCAL_CCY = "CNY";
	}

	/**
	 * 清算信息
	 * 
	 * @author shenzl
	 * @date 2020/01/11
	 */
	public interface PCIX {
		public static final String PAYMENT_AW = "AW";// 账户行
		public static final String FIELD_57J = "ACCT";// 账户格式MT300,MT320
		public static final String PAYMENT_BE = "BE";// 收款行
		public static final String FIELD_58J = "NAME";// 名称格式MT300,MT320
		public static final String PAYMENT_IN = "IN";// 中间行
		public static final String FIELD_56J = "INAC";// 名称格式MT300,MT320
		public static final String SUP_PAY_IND = "N";// 禁止付款标志
		public static final String SUP_CONF_IND = "N";// 禁止确认标志
		public static final String AUTH_IND = "1";// 指令标识
		public static final String TEXT_STAT = "0";// 确认标志
		public static final String FORMAT_IND = "S";// SWIFT
		public static final String CONF_TYPE = "C";// 确认类型-银行
		public static final String BIF_FLAG = "1";// 标志
		public static final String PMTQ_STATUS = "AR";// AR:等待发送/H :中间状态/R:已发送/C:取消
		public static final String PMTQ_SWIFT_FMT = "202";// 默认报文类型
		public static final String PMTQ_PAYIND = "P";// 付款
		public static final String PMTQ_RECIND = "R";// 付款
		public static final String CANPS_MEANS = "CNAPS";// 大额清算方式,针对RMB
		public static final String INNER_MEANS = "INNER";// 内部调拨,针对RMB
		public static final String NOSTRO_MEANS = "NOS";// 往来帐,针对外币
		public static final String DVP_MEANS = "DVP";// 针对债券清算方式
		public static final String DVP_ACCT_P = "DVP-P";// 针对债券清算方式
		public static final String DVP_ACCT_R = "DVP-R";// 针对债券清算方式
		public static final String BUYIND = "P";// 买
		public static final String SALEIND = "S";// 卖
		// 债券需要：xuqq
		public static final String PAYMENT_DE = "DE";//
		public static final String PAYMENT_OC = "OC";//
	}

	/**
	 * SFDC
	 * 
	 * @author xuqq
	 *
	 */
	public interface SFDC {
		public static final String PAYMENT_AW = "AW";// 账户行
		public static final String FIELD_57J = "ACCT";// 账户格式MT300,MT320
		public static final String PAYMENT_BE = "BE";// 收款行
		public static final String FIELD_58J = "NAME";// 名称格式MT300,MT320
		public static final String PAYMENT_IN = "IN";// 中间行
		public static final String FIELD_56J = "INAC";// 名称格式MT300,MT320
	}

	/**
	 * 拆借
	 * 
	 * @author shenzl
	 * @date 2020/01/11
	 */
	public interface DL {
		public static final String PROD_CODE = "DPNL";// MCFP中的产品代码
		public static final String PROD_TYPE = "DL";// MCFP中的产品类型
		public static final String RATE_CODE_FIXED = "FIXED";// 固定利率代码
		public static final String SEQ_ZERO = "0";// 默认0的SEQ
		public static final String TRAD_NO = "TRAD";// 交易员TRAD
		public static final String RATE_AMT_IND = "R";// 利率\利息标志
		public static final String ZERO = "0";// 适用于标志
		public static final String TERMS = "M";// 期限
		public static final String VER_IND = "1";// 复核标志
		public static final String SI_IND = "3";// 标准指令
		public static final String AGENT_IND = "0";// 代理标志
		public static final String DEAL_SRCE = "D";// 交易来源
		public static final String COMM_MEANS = "B";// 未到期结算方式
		public static final String SCHD_FLAG = "D";// 默认计划标识
		public static final String SCHEDULE_TYPE_DEF = "D"; // 计划类型
		public static final String SCHEDULE_TYPE_INT = "I"; // 计划类型
		public static final String SCHEDULE_TYPE_PRIN = "P"; // 计划类型
		public static final String SCHD_TYPE_V = "V";// 首期
		public static final String SCHD_TYPE_P = "P";// 中间
		public static final String SCHD_TYPE_M = "M";// 到期
		public static final String INT_CAP_FLAG = "0";// 利息转为本金标志
		public static final String TENOR = "99";// 期限
		public static final String ACR_FRST_LST = "F";// 默认第一次/最后一次结息标志
		public static final String BTB_IND = "0";// 背对背交易标志
		public static final String DELAYED_INT_IND = "0";// 延期利息标志
		public static final String EARLY_MAT_IND = "0";// 提前还款标志
		public static final String CORP_NET_IND = "0";
		public static final String ALLOW_NEGINT = "D";// 允许
		public static final String DEAL_REBOOKED = "0";// 交易重新启动
		public static final String REBOOKED_DEAL = "0";// 重新预订的交易
		public static final String AUTO_MAN_PAY = "A";// 自动手动付款标志
		public static final Long UPDATE_COUNTER = 1L;// 修改次数
		public static final String PAY_IND = "Pay";// 付款标志
		public static final String LIAB_IND = "L";// 负债
	}

	/**
	 * 外汇
	 * 
	 * @author shenzl
	 * @date 2020/01/11
	 */
	public interface FX {
		public static final String PROD_CODE = "FXD";
		public static final String PROD_TYPE = "FX";
		public static final String SEQ_ZERO = "0";
		public static final String TRAD_NO = "TRAD";
		public static final String PHONE_IND = "0";// 电话标识
		public static final String VER_IND = "1";// 复核标识
		public static final String PAY_AUTH_IND = "3";// 付款授权标志
		public static final String DEAL_SRCE = "D";// 交易源代码
		public static final String SI_IND = "3";// 标准指令
		public static final String TENOR_F = "F";// 远期期限
		public static final String TENOR_S = "S";// 即期期限
		public static final String CUST_ENTERED_IND = "Y";// 客户输入标志
		public static final String EXCOVERALL_LIM = "N"; // 超过总限额标志
		public static final String EXCCUST_LIM = "N"; // 超过客户额度标志
		public static final String OPTION_IND = "Y";// 可选交易标志
		public static final String SWIFT_MATCH_IND = "N";// Swift匹配标志
		public static final String SIIND_PAY = "N";// 标准付款指令标志
		public static final String REXRATETOL_IND = "N";// 超过利率限额标志
		public static final String SPOT_IND = "0";// 即期标志
		public static final String FIX_RATE_IND = "0";// 固定利率标志
		public static final String BLOCK_IND = "N";// 大宗交易标志
		public static final Long UPDATE_COUNTER = 1L;// 修改次数
	}

	/**
	 * 期权
	 * 
	 * @author shenzl
	 * @date 2020/01/11
	 */
	public interface OTC {
		public static final String PROD_CODE = "OTC";
		public static final String PROD_TYPE = "OT";
		public static final String SEQ_ZERO = "000";
		public static final String TRAD_NO = "TRAD";
		public static final String ZERO = "0";
		public static final String VER_IND = "1";// 复核标识
		public static final String CUT_OFF = "3:00 PM"; // 行权市场时间
		public static final String LOCATION = "BEIJING"; // 行权市场
		public static final String PLMETHOD = "M"; // 损益法
		public static final String PSIND = "N";// 是否配对
		public static final String MATHSOURCE = "FINCAD";// 计算方法
		public static final String TRIGIND = "0";// 触碰标志
		public static final String EXERIND = "0";// 行权标志
		public static final String SIIND = "0";// 清算指令标志
		public static final String TENOR = "99";// 与账务有关
		public static final String DEALSRCE = "D";// 交易标志
		public static final String SALESCREDCCY = "0";// 销售信用货币
		public static final String FEESEQ = "0";// 费用序号
		public static final String MARKETVALIND = "0";// 市值指标
		public static final String TAXSTATUS = "0";// 税务状况
		public static final String CALL_IND = "CALL";// 看涨期权
		public static final String PUT_IND = "PUT";// 看跌期权
		public static final Long UPDATE_COUNTER = 1L;// 修改次数

	}

	/**
	 * 互换
	 * 
	 * @author xuqq
	 * @date 2020/01/11
	 */
	public interface SWAP {
		public static final String PROD_CODE = "SWAP";
		public static final String PROD_TYPE_SP = "SP";// 产品类型代码：利率互换
		public static final String PROD_TYPE_CS = "CS";// 产品类型代码：货币互换
		public static final String DEAL_SRCE = "D";// 交易源代码
		public static final String VER_IND = "1";// 复核标识
		public static final String SEQ_ZERO = "0";
		public static final String TRAD_NO = "TRAD";
		public static final String DEALIND = "S";// 交易标识
		public static final String NETPAYIND = "Y";// 内部付款标识
		public static final String PLMETHOD = "T";// 损益方法
		public static final String SETOFF = "Y";// 设置启用标识
		public static final String SWAPTYPE_I = "I";// 掉期类型:利率互换
		public static final String SWAPTYPE_C = "C";// 掉期类型:货币互换
		public static final String TENOR = "99";//
		public static final String ETAUTHIND = "0";// 提前结束指令授权标志
		public static final String ACTIVEIND = "Y";// 掉期期权标志
		public static final Long UPDATE_COUNTER = 1L;// 修改次数
		public static final String VANILLASWAPIND = "T";// 掉期规则
		public static final String BTBIND = "0";// 背对背交易标志
		public static final String FLAG2 = "N";// 备用
		public static final String LEGCNT = "2";// 互换交易腿个数
		public static final String INVTYPE = "0";// 投资类型
		// swdt
		public static final String PAYRECIND_P = "P";// 付款/收款标志
		public static final String PAYRECIND_PAY = "Pay";// 付款/收款标志
		public static final String PAYRECIND_R = "R";// 付款/收款标志
		public static final String FINEXCHAUTHIND = "0";// 最后资金交换指令授权标志
		public static final String INITEXCHAUTHIND = "0";// 开始资金交换指令授权标志
		public static final String INTAUTHIND = "1";// 利息结算指令授权标志
		public static final String INTAUTHOPER = "SYS1";// 利息结算指令授权标志
		public static final String COLLARIND = "0";// SET
		public static final String DATEDIRECTION = "VF";// 日期方向规则
		public static final String MBSLEG = "0";//
		public static final String POSTNOTIONAL = "1";//
		public static final String INTNEGAUTHIND = "1";// 逆利率指令授权标志
		public static final String MULTICCYACCTIND = "0";// 多币种清算账目标志
		public static final String SCHEDID = "0";//
		public static final String PRODID = "0";//
		public static final String FORMULAID = "0";//
		public static final String REDEMPTIONCCYIND = "0";//
		public static final String RATEREVFRSTLST = "F";//
		public static final String UPFRONTFEENETTYPE = "I";// 费用类型
		public static final String YIELDCURVE = "SWAP";// 收益率曲线
		public static final String FWDFWDYIELDCURVE = "SWAP";//
		public static final String INTNEGAUTHOPER = "SYS1";// 逆利率指令授权操作员
		// swds
		public static final String SETTLE = "0";//
		
		public enum ExchPrin {
			/**
			 * 期初到期都交易
			 */
			B,
			/**
			 * 不交易本金
			 */
			N,
			/**
			 * 只交换期初本金
			 */
			V,
			/**
			 * 只交易到期本金
			 */
			M,
			/**
			 * 
			 */
			R
		}
	}

	/**
	 * 债券信息
	 * 
	 * @author xuqq
	 *
	 */
	public interface SECM {
		// 交易要素
		// 管理要素
		public static final String RATEDECS = "8";// 利率小数位数:
		public static final String RATETOL = "2";// 利率允许浮动百分比:
		public static final String PRICEDECS = "12";// 价格小数位数:8/12位选择
		public static final String CASHDECS = "8";// 现金流小数位数:
		public static final String AIDECS = "8";// AI小数位数:

		// 静态数据
		public static final String PRICERNDRULE = "U";// 价格取整规则:U是四舍五入方式
		public static final String TENOR = "99";//
		public static final String PRICETOL = "12";// 价格允许浮动百分比
		public static final String MINDENOM = "0";// 最少单位量
		public static final String INTEXCHTERMS = "M";// 利息汇率乘除形式
		public static final String REPOFLAG = "0";// 回购标志
		public static final String EMTAIND = "0";// EMT选项:
		public static final Long UPDATECOUNTER = 1L;// 修改次数
		public static final String PHYSDELIND = "0";// 实际交货标志
		public static final String FACTSECIND = "0";// 面值债券选项
		public static final String ILIND = "0";//
		public static final String DEFLATIONIND = "0";//
		public static final String CPIDAYCYCLE = "0";//
		public static final String STRUCTID = "0";//
		public static final String DTCIND = "0";//
		public static final String FEDIND = "0";//
		public static final String EUROCLEARIND = "0";//
		public static final String CLEARSTREAMIND = "0";//
		public static final String REPOTRACKINGIND = "0";//
		public static final String ENDOFMONTHIND = "N";//
		public static final String ALLOWNEGATIVEINT = "N";//
		// secl
		public static final String BRANCH = "01";
		public static final String FIXCYCLE = "3";
		public static final String FIXFREQ = "1";
		public static final String FIXDAY = "N";
		public static final String FIXSPOTDAYS = "0";
		public static final String MARKMAKIND = "0";
		public static final String PAYCNO = "0";
		public static final String SECSACCT = "CDTC";
		public static final String TAXIND = "E";
		// secs
		public static final String RATEREFIXIND = "0";
	}

	/**
	 * 现券买卖
	 * 
	 * @author xuqq
	 *
	 */
	public interface SPSH {
		public static final String PROD_CODE = "SECUR";// 产品代码:
		public static final String PROD_TYPE = "SD";// 产品类型:
		public static final String TRAD_NO = "TRAD";// 输入操作员:管理要素—交易员
		public static final String SEQ = "0";// 序号:默认 0
		public static final String FIXINCIND = "1";// 固定收入标志:默认1
		public static final String ASSIGNIND = "1";// 分配标志:默认1
		public static final String BROK = "D";// 经纪公司编号:默认D
		public static final String CCYAUTHIND = "1";// 流通授权标志:默认1 授权
		public static final String CCYAUTHOPER = "SYS1";// 流通授权操作员:
		public static final String VERIND = "1";// 复核标志:
		public static final String VOPER = "SYS1";// 复核操作员:
		public static final String EXCHIND = "N";// 兑换标志
		public static final String SETTPROCTERMS = "M";// 结算币种对收益币种汇率乘除形式:
		public static final String INTSETTTERMS = "M";// 利息币种对结算币种汇率乘除形式:
		public static final String SETTBASETERMS = "M";// 结算币种对基础币种汇率乘除形式:
		public static final String INTBASETERMS = "M";// 利息币种对基础币种汇率乘除形是:
		public static final String AMORTIND = "0";// 分期偿还标志:
		public static final String PAIREDIND = "0";// 抵押物标识:
		public static final String CURRSPLITSEQ = "0";// 序号:
		public static final Long UPDATE_COUNTER = 1L;// 修改次数
		public static final String SPLITIND = "0";//
		public static final String SWIFTBOTHIND = "0";//
		public static final String NEWISSUEIND = "N";//
		public static final String CCPIND = "N";//
		public static final String EXTCOMPARE = "N";//
		public static final String PAYMENTHOLDIND = "0";//
		public static final String INTRADAY = "0";//
		public static final String CONTYPE = "0";//
		public static final String SECAUTHIND = "1";// 债券授权标志 默认1
		public static final String SUPPCONFIND = "2";// 复核确认:
		public static final String PRINAGIND = "P";// 自营代理标志:
		public static final String PAY_IND = "CBT001";// 付款标志
		public static final String LIAB_IND = "L";// 负债
		public static final String REVREASON = "99";// 冲销原因
		public static final String REVTEXT = "99";// 冲销描述
		public static final String SUPSECMOVEIND = "N";//
		public static final String SUPCCYMOVEIND = "N";//
	}

	/**
	 * 债券借贷默认参数定义
	 * 
	 * @author tuzh
	 *
	 */
	public interface SLDH {
		public static final String BRANCH = "01";
		public static final String SUPSECMOVEIND = "N";//
		public static final String SUPCCYMOVEIND = "N";//
		public static final String DEALIND = "SLDH";// 交易标志
		public static final String DEALIND_COLL = "CLDH";// 交易标志
		public static final String PROD_CODE = "SECLEN";// 产品代码
		public static final String PROD_CODE_COLLAT = "COLLAT";// 产品代码
		public static final String TRAD_NO="TRAD";//交易员TRAD
		public static final String PROD_TYPE = "SL";// 产品类型
		public static final String PROD_TYPE_SB = "SB";// 产品类型
		public static final String PAY_IND = "SL001";// 付款标志
		public static final String PS_IND = "P";// 交易方向
		public static final String LIAB_IND = "L";// 负债
		public static final String ASST_IND = "A";// 资产
		public static final String COLL_PROD = "COLLAT";// 抵押券产品代码
		public static final String ASSIGN_IND = "1";// 抵押标志
		public static final String CLOSE_IND = "1";// 关闭标志
		public static final String COLL_REQUIRED_IND = "1";// 抵押必须标志
		public static final String COM_AUTH_IND = "1";// 授权标志
		public static final String COM_AUTH_OPER = "SYS1";// 授权操作员
		public static final String COUPRE_INVEST_IND = "N";//
		public static final String DEAL_SCRE = "D";// 交易来源
		public static final String EXT_IND = "0";
		public static final String FEE_CALC_TYPE = "SIM";// 费用计算类型
		public static final String INIT_TERMS = "M";// 初始计算关系
		public static final String IOPER = "SYS1";// 操作员
		public static final String MAT_AUTH_IND = "1";// 到期授权标志
		public static final String OPEN_LEND_IND = "0";// 打开借出标志
		public static final String OVER_NIGHT_IND = "0";// 隔夜标志
		public static final String OVER_RIDEWXTAX_IND = "0";
		public static final String SEQ = "0";//序号
		public static final String PMVIND = "1";
		public static final String PROCESS_SIND = "1";
		public static final String SUB_STITUTE = "1";
		public static final String SUPP_CASH_IND = "0";
		public static final String SUPP_CONF_IND = "2";
		public static final String SUPP_FEE_IND = "0";
		public static final String SUPSECOMVE_IND = "0";
		public static final String TENOR = "99";
		public static final String VER_IND = "1";
		public static final String REMAINING_CASH_IND = "0";
		public static final String SAFE = "CDTC";
		public static final String SEC_TYPEIND = "F";
		public static final String HAIRCUT_TERMS = "M";
		public static final String HAIRCUT_PRICS8 = "0";
		
		
		
	}

	/**
	 * 回购交易表数据
	 * 
	 * @author shenzl
	 *
	 */
	public interface RPRH {
		public static final String BRANCH = "01";
		public static final String PROD_CODE = "SECUR";// MCFP使用
		public static final String PROD_TYPE = "SD";// MCFP使用
		public static final String SEQ_ZERO = "0";// 默认0的SEQ
		public static final String ASSIGNIND = "1";// 分配标志:默认1
		public static final String BROK = "D";// 经纪公司比编号:D
		public static final String COMAUTHIND = "1";// 授权标志:
		public static final String FINTERMS = "M";// FINTERMS:
		public static final String INITTERMS = "M";// INITTERMS:
		public static final String MARGINPCT = "M";// 保证金比率:
		public static final String OPENREPOIND = "N";// 开通回购标志:
		public static final String SUBSTITUTE = "N";// 置换标志:
		public static final String SUPPCONFIND = "2";// 复核确认:
		public static final String TENOR = "99";// 期限:
		public static final String VERIND = "1";// 复核标志:
		public static final String RPIND = "N";// RPIND:
		public static final String CLOSEREPOIND = "N";// 回购关闭标志:
		public static final String INTCALCDIND = "1";// 利息计算标志:
		public static final String COUPREINVEST = "N";// 息票再投资标志:
		public static final String INITBASETERMS = "M";// 初始基本利率代码:
		public static final String FINBASETERMS = "M";// 最终基本利率代码:
		public static final String OVERRIDEWXTAXIND = "0";// OVERRIDEWXTAXIND:
		public static final String EXCHIND = "0";// 兑换标志:
		public static final String SPLITIND = "0";// :
		public static final String CCPIND = "N";// :
		public static final String EXTCOMPARE = "N";// :
		public static final String VDATENET = "N";// :
		public static final String VDPAYMENTHOLDIND = "0";// :
		public static final String MDPAYMENTHOLDIND = "0";// :
		public static final String ASSIGNMETHOD = "D";// :
		public static final String AUTOASSIGN = "N";// :
		public static final String SUPSECMOVEIND = "N";//
		public static final String SUPCCYMOVEIND = "N";//
		public static final String TRAD_NO = "TRAD";// 交易员TRAD
		public static final String INTCALCTYPE = "SIM";// 利息计算类型:默认SIM/
		public static final String MATAUTHIND = "1";// 保证金清算授权标志:
		public static final Long UPDATE_COUNTER = 1L;// 修改次数
		public static final String PAY_IND = "REPO001";// 付款标志
		public static final String PS_IND = "P";// 交易方向
	}

	/**
	 * 回购交易
	 * 
	 * @author shenzl
	 *
	 */
	public interface RPDT {
		public static final String SEQ = "0";// 0 :流水号
		public static final String TRAD_NO = "TRAD";// 交易员TRAD
		public static final String PRINEXCHTERMS = "M";// M:本金汇率规则
		public static final String INTEXCHTERMS = "M";// M:利息兑换规则
		public static final String PRINBASETERMS = "M";// M:本金基本利率规则
		public static final String INTBASETERMS = "M";// M:利息汇率
		public static final String REINVESTIND = "N";// N:息票重新投资标志
		public static final Long UPDATE_COUNTER = 1L;// 修改次数
		public static final String VDPAIREDIND = "0";// 0:VDPAIREDIND
		public static final String VDCURRSPLITSEQ = "0";// 0 :VDCURRSPLITSEQ
		public static final String MDPAIREDIND = "0";// 0:MDPAIREDIND
		public static final String MDCURRSPLITSEQ = "0";// 0 :MDCURRSPLITSEQ

	}

	/**
	 * 费用表
	 * 
	 * @author shenzl
	 *
	 */
	public interface FEES {
		public static final String PROD_CODE = "FEES";// MCFP中的产品代码
		public static final String PROD_CODE_SB = "SECLEN|FE";// 债券借贷费用CODE
		public static final String PROD_TYPE = "FE";// MCFP中的产品类型
		public static final String FEESEQ = "0";// 序号
		public static final String SEQ = "0";// 序号
		public static final String TENOR = "99";// 期限-账务相关
		public static final String TRAD_NO = "TRAD";// 操作员
		public static final String VERIND = "1"; // 复核标志
		public static final String AUTHINGD = "1"; // 授权标志
		public static final String FEESIND = "N";// 费用编制
		public static final Long UPDATE_COUNTER = 1L;// 修改次数
		public static final String AMORT_METHOD = "S";// 摊销方法
		
	}

	public interface CUST {
		// B-银行；C-公司；O-其它；F-金融公司；I-个人；K-经纪人/代理人
		public static final List<String> CUST_TYPE = Arrays.asList("B", "C", "O", "F", "I", "K");

	}
}
