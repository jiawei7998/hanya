package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.ColDefBean;
import com.singlee.financial.bean.OpicsDealResultBean;
import com.singlee.financial.bean.ResultBean;
import com.singlee.financial.bean.SlImportResultQueryBean;
import com.singlee.financial.bean.SlMmktIrevBean;
import com.singlee.financial.bean.SlMmktIrhsBean;
import com.singlee.financial.bean.SlMmktIselBean;
import com.singlee.financial.bean.SlMmktIselForBean;
import com.singlee.financial.bean.SlMmktIselTmpBean;
import com.singlee.financial.bean.SlMmktIvolBean;
import com.singlee.financial.bean.SlMmktIycrBean;

public interface MmktMapper {
	
	List<ColDefBean> getColDefList(String paramString);


	int getIrevFilNextImpSeq(String intFlag);
	int getIrhsFilNextImpSeq();
	int getIvolFilNextImpSeq();
	int getIycrFilNextImpSeq();
	int getIselFilNextImpSeq();
	int getIselForFilNextImpSeq();


	void updateIrevStatus(Map<String,Object> map);
	void updateIselStatus(Map<String,Object> map);
	void updateIycrStatus(Map<String,Object> map);
	void updateIselForStatus(Map<String,Object> map);
	void updateIvolStatus(Map<String,Object> map);
	void updateIrhsStatus(Map<String,Object> map);


	Map<String,Object> getFedealNo(Map<String,Object> map);
	
	int getIrevAwaitDealImpSeq(Map<String,Object> map);
	int getIrhsAwaitDealImpSeq(Map<String,Object> map);
	int getIvolAwaitDealImpSeq(Map<String,Object> map);
	int getIycrAwaitDealImpSeq(Map<String,Object> map);
	int getIselAwaitDealImpSeq(Map<String,Object> map);
	int getIselForAwaitDealImpSeq(Map<String,Object> map);


	Map<String,Object> callIrevInth(Map<String, Object> map);
	Map<String,Object> callIrevPrvrInth(Map<String, Object> map);
	Map<String,Object> callIrhsInth(Map<String, Object> map);
	Map<String,Object> callIvolInth(Map<String, Object> map);
	Map<String,Object> callIycrInth(Map<String, Object> map);
	Map<String,Object> callIselInth(Map<String, Object> map);
	Map<String,Object> callIselForInth(Map<String, Object> map);


	int saveSlIrev(SlMmktIrevBean bean);
	int saveSlIselTmp(SlMmktIselTmpBean bean);
	int saveSlIycr(SlMmktIycrBean bean);
	int saveSlIvol(SlMmktIvolBean bean);
	int saveSlIselFor(SlMmktIselForBean bean);
	int saveSlIrhs(SlMmktIrhsBean bean);

	ResultBean callIselInput(Map<String, Object> map);

	void callIselCheck(Map<String, Object> map);
	
	void truncateSlIselTemp();


	Page<SlImportResultQueryBean> queryMmktPage(Map<String, Object> map, RowBounds rb);


	Page<SlMmktIrevBean> queryMmktIrevPage(Map<String, Object> map, RowBounds rb);
	Page<SlMmktIrhsBean> queryMmktIrhsPage(Map<String, Object> map, RowBounds rb);
	Page<SlMmktIvolBean> queryMmktIvolPage(Map<String, Object> map, RowBounds rb);
	Page<SlMmktIycrBean> queryMmktIycrPage(Map<String, Object> map, RowBounds rb);
	Page<SlMmktIselBean> queryMmktIselPage(Map<String, Object> map, RowBounds rb);
	Page<SlMmktIselForBean> queryMmktIselForPage(Map<String, Object> map, RowBounds rb);


	Page<OpicsDealResultBean> queryMmktOpicsPage(Map<String, Object> map, RowBounds rb);


	Page<SlMmktIselTmpBean> queryMmktIselTmpPage(Map<String, Object> map, RowBounds rb);




	

}
