package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;


import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlFxCashFlowBean;
import com.singlee.financial.bean.SlFxdhBean;

public interface FxdhTotalMapper {
	
	public SlFxdhBean getFxdhBean(Map<String, Object> map);

	public Page<SlFxCashFlowBean> searchFxCashFlowPage(Map<String, Object> map,
			RowBounds rb);

	public List<SlFxdhBean> getFxdhList(Map<String, Object> map);

}
