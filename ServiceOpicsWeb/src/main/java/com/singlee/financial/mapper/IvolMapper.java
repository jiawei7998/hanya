package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIvolBean;
import com.singlee.financial.marketdata.bean.ImportVolBean;
/**
 * 波动率曲线
 * @author zhengfl
 *
 */
public interface IvolMapper {
	
	void addIvol(SlIvolBean ivolBean);
	
	Page<SlIvolBean> queryIvolPage(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询Ivol表指定SERVER最大流水号
	 * @param br
	 * @param server
	 * @param postdate
	 * @return
	 */
	String selectIvolMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
	
	/**
	 * 查询OPICS待导入的波动率数据
	 * @return
	 */
	List<ImportVolBean> selectOpicsVoldData();
}
