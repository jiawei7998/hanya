package com.singlee.financial.mapper;

import com.singlee.financial.bean.SlIotdBean;

/**
 * 期权
 * 
 * @author lij
 *
 */
public interface OtcMapper {
	/**
	 * 期权 添加到OPICS中的IOTD表中
	 */
	void addOtc(SlIotdBean slIotdBean);

	/**
	 * 期权 - 冲销 添加
	 */
	void addOtcRev(SlIotdBean slIotdBean);

	/**
	 * 行权/到期/触碰
	 */
	void addExeute(SlIotdBean slIotdBean);

	/**
	 * 查询原始交易状态是否行权
	 * 
	 * @param slIotdBean
	 * @return
	 */
	SlIotdBean getIotdByFedealno(SlIotdBean slIotdBean);
	
	/**
	 * 期权 - 冲销   判断
	 */
	public String getOtcRevDate(SlIotdBean slIotdBean);
}
