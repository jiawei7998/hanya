package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIrhsBean;

public interface IrhsMapper {

	void addIrhs(SlIrhsBean irhsBean);
	
	Page<SlIrhsBean> queryIrhsPage(Map<String, Object> map, RowBounds rb);

	/**
	 * 查询Irhs表指定SERVER最大流水号
	 * @param br
	 * @param server
	 * @param postdate
	 * @return
	 */
	String selectIrhsMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
}
