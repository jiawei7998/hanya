package com.singlee.financial.mapper;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlBondHoldBean;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public interface BondHoldMapper {
	/**
	 * 债券持仓查询
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlBondHoldBean> searchBondHoldCount(Map<String, Object> map,RowBounds rb);

	List<String> TposList(Map<String, Object> map);
	
}
