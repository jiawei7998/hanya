package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlTaxParaBean;

public interface SlTaxParaMapper {
	/**
	 * 新增时保存
	 */
	void insert(SlTaxParaBean taxParaBean);
	
	/**
	 * 查询所有的gent信息，返回page页
	 */
	Page<SlTaxParaBean> searchPageTaxPara(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 修改时保存
	 */
	void updateById(SlTaxParaBean taxParaBean);
	
	/**
	 * 删除某一条记录
	 */
	void deleteSlTaxPara(SlTaxParaBean taxParaBean);
	
	/**
	 * 查询
	 */
	SlTaxParaBean getSlTaxPara(Map<String, String> map);


}
