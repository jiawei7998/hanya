package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.NupdSendBean;
import com.singlee.financial.bean.SlNupdBean;

/**
 * OPICS往来账
 * 
 * @author zc
 * 
 */
public interface NupdTotalMapper {

	/**
	 * 调用往来账查询
	 */
	List<NupdSendBean> nupdSearch(String valDate);
	Integer updateResult(NupdSendBean nupd);
	
	Page<SlNupdBean> getNupdPage(Map<String, String> params, RowBounds rb);
	
	List<SlNupdBean> getNupdById(Map<String, String> map);
}
