package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlRateBean;

public interface RateTotalMapper {
	
	public List<SlRateBean> queryRateList(Map<String, Object> map);
}
