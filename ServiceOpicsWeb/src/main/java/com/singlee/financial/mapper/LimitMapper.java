package com.singlee.financial.mapper;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.HsLimiltBean;
import com.singlee.financial.bean.OpicsBankChackBean;
import com.singlee.financial.bean.SlCashBean;
import com.singlee.financial.bean.SlCnapsBean;
import com.singlee.financial.bean.SlInthBean;
/**
 * 非信贷额度占用
 * @author fll
 *
 */
public  interface LimitMapper {
	
	/**
	 * 将信贷推送报文信息插入数据库
	 * @param 
	 * @return
	 */
	int insertHsLimit(HsLimiltBean limilt);
	/**
	 * 交易页面展示
	 * @param BalanceDate
	 * @return
	 */
	Page<HsLimiltBean> searchPageHslimit(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 交易进OPICS接口表时，更新状态
	 * @param BalanceDate
	 * @return
	 */
	void updateLimitStatus(HsLimiltBean limitBean);
	/**
	 * 调用额度释放接口时，更新额度表状态
	 * @param BalanceDate
	 * @return
	 */
	void releaseLimitStatus(HsLimiltBean limitBean);
	
	/**
	 * 调用额度释放接口时，更新额度表状态
	 * @param BalanceDate
	 * @return
	 */
	void releaseLimitDetail(HsLimiltBean limitBean);
	
	/**
	 * 额度页面展示
	 * @param BalanceDate
	 * @return
	 */
	Page<HsLimiltBean> searchPageOpicsHslimit(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 额度释放明细展示
	 * @param BalanceDate
	 * @return
	 */
	Page<HsLimiltBean> searchPageLimitReverseDetail(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 额度表修改
	 * @param BalanceDate
	 * @return
	 */
	void updateHslimit(HsLimiltBean limitBean);
	/**
	 * 额度表新增
	 * @param cnapsBean
	 * @return
	 */
	void insertHslimit(HsLimiltBean limitBean);
	/**
	 * 额度表删除
	 * @return
	 */
	void deleteHslimit(Map<String, Object> map);
	/**
	 * 根据信贷额度编号查询
	 * @param arg0
	 * @param rb
	 * @return
	 */	
	HsLimiltBean selectByLimitNumber(HsLimiltBean limitBean);
	
	
	int insertCnaps(SlCnapsBean cnaps);
	/**
	 * 更新废止状态
	 * @param cnaps
	 * @return
	 */
	int updateCnapsList(SlCnapsBean cnapsBean);
	/**
	 * 退回交易修改
	 * @param cnapsBean
	 * @return
	 */
	int updateCnapsEdit(SlCnapsBean cnapsBean);
	Page<SlCnapsBean> getCnapsListInst(Map<String, Object> arg0, RowBounds rb);
	Page<SlCnapsBean> getSLCnapsList(Map<String, Object> arg0, RowBounds rb);
	SlCnapsBean updateCnaps(Map<String, Object> arg0);
	int updateSLCnaps(SlCnapsBean slCnapsBean);
	/**
	 * 将锁定状态更新为已发送
	 * @param cnaps
	 * @return
	 */
	int updateCnapsStatus(SlCnapsBean cnapsBean);
	
	/**
	 * OPICS交易回查
	 * @param BalanceDate
	 * @return
	 */
	Page<OpicsBankChackBean> searchOpicsBankChack(OpicsBankChackBean bankChackBean);
	/**
	 * OPICS交易回查占用额度
	 * @param BalanceDate
	 * @return
	 */
	void updateOpicsLimitStatus(HsLimiltBean limitBean);
	
	/**
	 * 定时任务dealno号回查，根据tickedid更新
	 * @param BalanceDate
	 * @return
	 */
	void updateDealnolimit(SlInthBean slInthBean);
	/**
	 * 定时任务FTP批量文件上传
	 * @param BalanceDate
	 * @return
	 */
	public List<HsLimiltBean> FTPFilelimitList();
	/**
	 * 额度文件上传成功后更新额度状态“已释放”
	 * @param BalanceDate
	 * @return
	 */
	void updateStatusTolimit(HsLimiltBean limitBean);
	/**
	 * 额度释放批量释放后更新明细状态
	 * @param BalanceDate
	 * @return
	 */
	void updateReleaseLimitDetail(HsLimiltBean limitBean);
	
}
