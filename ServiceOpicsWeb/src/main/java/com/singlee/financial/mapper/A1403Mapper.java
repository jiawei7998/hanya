package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.A1403Bean;

public interface A1403Mapper {
 
	Page<A1403Bean> getA1403BeanList(Map<String, Object> map, RowBounds rb);
	
	List<A1403Bean> getA1403ReportList(Map<String, Object> map);
	
	List<String> getA1403ReportAllBr();
}
