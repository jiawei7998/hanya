package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;


/***
 * 
 * @author LIJ
 *
 */
public interface GmrmBondInfoMapper {
	/***
	 * 调用债券信息存储过程：
	 * 生成当天数据于SL_GMRM_BONDINFO表中
	 * @param map
	 * @return
	 */
	Map<String, Object> bondInfoProc(Map<String, Object> map);
	
	/***
	 * 获取债券信息list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmBondInfo(Map<String, Object> map);
	
	/***
	 * 调用债券即期存储过程：
	 * 生成当天数据于SL_GMRM_BONDSPOT表中
	 * @param map
	 * @return
	 */
	Map<String, Object> bondSpotProc(Map<String, Object> map);
	
	/***
	 * 获取债券即期list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmBondSpot(Map<String, Object> map);
	
	/***
	 * 调用债券等级存储过程：
	 * 生成当天数据于SL_GMRM_BONDRATING表中
	 * @param map
	 * @return
	 */
	Map<String, Object> bondRatingProc(Map<String, Object> map);
	
	/***
	 * 获取债券评级list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmBondRating(Map<String, Object> map);
	
	/***
	 * 调用债券重置利率存储过程：
	 * 生成当天数据于SL_GMRM_BONDRESETRATE表中
	 * @param map
	 * @return
	 */
	Map<String, Object> bondResetProc(Map<String, Object> map);
	
	/***
	 * 获取债券重置利率list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmBondResetRate(Map<String, Object> map);
	
	
	/***
	 * 调用外汇即期存储过程：
	 * 生成当天数据于SL_GMRM_FXSPOT表中
	 * @param map
	 * @return
	 */
	Map<String, Object> FxSpotProc(Map<String, Object> map);
	/***
	 * 获取外汇即期list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmFxSpot(Map<String, Object> map);
	
	/***
	 * 调用外汇远期存储过程：
	 * 生成当天数据于SL_GMRM_FXFORWARD表中
	 * @param map
	 * @return
	 */
	Map<String, Object> FxForwardProc(Map<String, Object> map);
	/***
	 * 获取外汇远期list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmFxForward(Map<String, Object> map);
	
	
	/***
	 * 调用利率互换POS存储过程：
	 * 生成当天数据于SL_GMRM_IRSPOS表中
	 * @param map
	 * @return
	 */
	Map<String, Object> IrsPosProc(Map<String, Object> map);
	/***
	 * 获取利率互换POS   list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmIrsPos(Map<String, Object> map);
	
	/***
	 * 调用利率互换Pay存储过程：
	 * 生成当天数据于SL_GMRM_IRSPAY表中
	 * @param map
	 * @return
	 */
	Map<String, Object> IrsPayProc(Map<String, Object> map);
	/***
	 * 获取利率互换PAY   list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmIrsPay(Map<String, Object> map);
	
	/***
	 * 调用利率互换Refix存储过程：
	 * 生成当天数据于SL_GMRM_IRSREFIX表中
	 * @param map
	 * @return
	 */
	Map<String, Object> IrsRefixProc(Map<String, Object> map);
	/***
	 * 获取利率互换REFIX   list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmIrsRefix(Map<String, Object> map);
	
	/***
	 * 调用利率互换Lump存储过程：
	 * 生成当天数据于SL_GMRM_IRSLUMP表中
	 * @param map
	 * @return
	 */
	Map<String, Object> IrsLumpProc(Map<String, Object> map);
	/***
	 * 获取利率互换LUMP   list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmIrsLump(Map<String, Object> map);
	
	
	/***
	 * 调用贵金属Physical存储过程：
	 * 生成当天数据于SL_GMRM_PMPHYSICAL表中
	 * @param map
	 * @return
	 */
	Map<String, Object> PmPhysicalProc(Map<String, Object> map);
	/***
	 * 获取贵金属Physical   list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmPmPhysical(Map<String, Object> map);
	
	/***
	 * 调用贵金属Forward存储过程：
	 * 生成当天数据于SL_GMRM_PMFORWARD表中
	 * @param map
	 * @return
	 */
	Map<String, Object> PmForwardProc(Map<String, Object> map);
	/***
	 * 获取贵金属远期   list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmPmForward(Map<String, Object> map);
	
	/***
	 * 调用贵金属Swap存储过程：
	 * 生成当天数据于SL_SP_GMRM_PMSWAP表中
	 * @param map
	 * @return
	 */
	Map<String, Object> PmSwapProc(Map<String, Object> map);
	/***
	 * 获取贵金属掉期   list
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getListGmrmPmSwap(Map<String, Object> map);
	
	
	
	

}
