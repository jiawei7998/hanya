package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlCspiCsriBean;
import com.singlee.financial.bean.SlCspiCsriDetailBean;
import com.singlee.financial.bean.SlSdvpBean;
import com.singlee.financial.bean.SlSdvpDetailBean;

public interface CspiCsriSdvpMapper {

	/**
	 * 检查收付款标识
	 * 
	 * @param cspiCsriBean
	 * @return
	 */
	public List<SlCspiCsriBean> getCspiCsri(SlCspiCsriBean cspiCsriBean);

	/**
	 * 检查是否SDVP路径
	 * 
	 * @param sdvpBean
	 * @return
	 */
	public List<SlSdvpBean> getSdvp(SlSdvpBean sdvpBean);

	/**
	 * 查询cspicsri明细
	 * 
	 * @param map
	 * @return
	 */
	List<SlCspiCsriDetailBean> searchcspicsridetail(Map<String, Object> map);

	/**
	 * 查询付款cspicsri
	 * 
	 * @param map
	 * @return
	 */
	SlCspiCsriBean searchpaycspicsrihead(Map<String, Object> map);
	
	/**
	 * 查询收款cspicsri
	 * 
	 * @param map
	 * @return
	 */
	SlCspiCsriBean searchreccspicsrihead(Map<String, Object> map);

	/**
	 * 
	 * @param map
	 * @return
	 */
	List<SlSdvpDetailBean> searchsdvpdetail(Map<String, Object> map);

	/**
	 * 
	 * @param map
	 * @return
	 */
	SlSdvpBean searchpaysdvphead(Map<String, Object> map);
	
	/**
	 * 
	 * @param map
	 * @return
	 */
	SlSdvpBean searchrecsdvphead(Map<String, Object> map);
	
	/**
	 * 查询csri明细
	 * 
	 * @param map
	 * @return
	 */
	List<SlCspiCsriDetailBean> searchcsridetail(Map<String, Object> map);
}