package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIntfcGent;

/**
 * 映射关系
 * 
 * @author QXJ
 * 
 */
public interface SlIntfcGentMapper {

	/**
	 * 新增时保存
	 */
	void insert(SlIntfcGent gentBean);
	
	/**
	 * 查询所有的gent信息，返回page页
	 */
	Page<SlIntfcGent> searchPageGent(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 修改时保存
	 */
	void updateById(SlIntfcGent gentBean);
	
	/**
	 * 删除某一条记录
	 */
	void deleteSlGent(SlIntfcGent gentBean);
	
	/**
	 * 根据主键取查询有无此数据
	 */
	SlIntfcGent searchSlIntfcGent(Map<String, Object> map);

	String getObject(String paramod, String br, String paraid, String part1,
			String part2);

	String getPart2(String paramod, String br, String paraid, String part1,
			String object);
	
	/***
	 * 获取参数列表
	 * @param map
	 * @return
	 */
	List<SlIntfcGent>  getListSlIntfcGent(Map<String,Object> map);
	
}