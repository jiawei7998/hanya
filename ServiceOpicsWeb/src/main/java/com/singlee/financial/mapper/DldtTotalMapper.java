package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.DldtRevaluation;
import com.singlee.financial.page.PageInfo;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlDldtBean;

public interface DldtTotalMapper {
	/**
	 * 获取拆出外币统计
	 * @param map
	 * @return
	 */
	public Page<SlDldtBean> dldtSearch(Map<String, Object> map, RowBounds rb);
	
	List<SlDldtBean> dldtSearch(Map<String, Object> map);
	
	SlDldtBean dldtSrearchOne(Map<String, Object> map);

	Page<DldtRevaluation>  searchDldtRev(Map<String, Object> map,RowBounds rb);
}
