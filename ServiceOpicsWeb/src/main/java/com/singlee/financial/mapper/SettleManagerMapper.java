package com.singlee.financial.mapper;

import java.util.Map;

import com.singlee.financial.bean.SlIdriBean;
import com.singlee.financial.bean.SlIinsBean;

/**
 * opics CSPICSRI相关查询
 * 
 * @author shenzl
 *
 */
public interface SettleManagerMapper {

	/**
	 * 添加IINS表数据
	 * 
	 * @return
	 */
	int addSlIinsBean(SlIinsBean iinsBean);

	/**
	 * 添加IDRI表数据
	 * 
	 * @return
	 */
	int addSlIdriBean(SlIdriBean idriBean);

	/**
	 * 查询付款路径
	 * 
	 * @return
	 */
	SlIinsBean queryCspi(SlIinsBean iinsBean);
	/**
	 * 判断CSPI是否有数据
	 * @param map
	 * @return
	 */
	int searchCspiById(Map<String, Object> map);
	
	/**
	 * 判断CSRI是否有数据
	 * @param map
	 * @return
	 */
	int searchCsriById(Map<String, Object> map);
	/**
	 * 判断SDVP是否有数据
	 * @param map
	 * @return
	 */
	int searchSdvpById(Map<String, Object> map);
	

}
