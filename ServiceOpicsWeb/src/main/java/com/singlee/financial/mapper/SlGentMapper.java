package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlGentBean;

/**
 * 映射关系
 * 
 * @author QXJ
 * 
 */
public interface SlGentMapper {

	/**
	 * 新增时保存
	 */
	void insert(SlGentBean gentBean);
	
	/**
	 * 查询所有的gent信息，返回page页
	 */
	Page<SlGentBean> searchPageGent(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 修改时保存
	 */
	void updateById(SlGentBean gentBean);
	
	/**
	 * 删除某一条记录
	 */
	void deleteSlGent(SlGentBean gentBean);
	
	/**
	 * 根据主键取查询有无此数据
	 */
	SlGentBean searchIfsOpicsGent(Map<String, String> map);
	
	/**
	 * 根据币种查询汇率
	 */
	String queryRate(Map<String, String> map);
	
	/**
	 * 查询估值
	 */
	List<SlAcupBean> queryValue(Map<String, String> map);
	
	/**
	 * 根据成本中心查询 本方交易员
	 */
	SlGentBean getSlGent(SlGentBean gentBean);
}