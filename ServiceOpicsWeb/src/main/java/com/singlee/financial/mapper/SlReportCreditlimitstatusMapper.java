package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlReportCreditlimitstatus;
import com.singlee.financial.bean.SlReportCreditrate;
/***
 * 同业额度使用情况报告周报
 * @author LIJ
 *
 */
public interface SlReportCreditlimitstatusMapper {
	
	/**
	 * 分页查询-同业额度使用情况报告周报
	 * @param map
	 * @return
	 */
	Page<SlReportCreditlimitstatus> getPageCreditlimitstatusReport(Map<String, Object> map,RowBounds rb) ;
	
	/**
	 * 列表查询-同业额度使用情况报告周报
	 * @param map
	 * @return
	 */
	List<SlReportCreditlimitstatus> getPageCreditlimitstatusReport(Map<String, Object> map) ;
	
	/***
	 * 获取不重复的sheet集合
	 * @return
	 */
	List<String> selectDistinctSheet();
	
	
	/**
	 * 列表查询-汇率
	 * @param map
	 * @return
	 */
	List<SlReportCreditrate> getListCreditrate(Map<String, Object> map) ;

}
