package com.singlee.financial.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlPmtqFxBean;

/**
 * 	外币记账
 * @author zhengfl
 *
 */
public interface PmtqFxMapper {
	/**
	  * 外币记账分页查询
	 * @param params
	 * @param rb
	 * @return
	 */
	Page<SlPmtqFxBean> searchPagePmtqFx(Map<String, Object> params, RowBounds rb);

	List<SlPmtqFxBean> getPmtqFxList(Map<String, Object> param);

	void fxblAdd(SlPmtqFxBean entity);

	void fxblEdit(SlPmtqFxBean entity);

	void deleteFxbl(SlPmtqFxBean entity);

	SlPmtqFxBean selectById(Map<String, Object> map);
	//复核
	void verifyFxbl(String sn, String oper, Date date, String note, String verind);
	//经办
	void handleFxbl(String sn, String oper, Date date, String note, String verind);

	void doUpdatePmtqMsg(Map<String, Object> map);
	
}
