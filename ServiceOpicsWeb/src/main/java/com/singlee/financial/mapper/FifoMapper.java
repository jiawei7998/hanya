package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlFifoBean;
import com.singlee.financial.bean.SlSpshViewBean;

public interface FifoMapper{

	public void deleteFifoByBr(String br);
	
	public List<SlSpshViewBean> selectSpshViewListGroup(String br);
	
	public List<SlSpshViewBean> selectSpshViewListP(Map<String,Object> map);
	
	public SlSpshViewBean selectSpshViewS(Map<String,Object> map);

	public void insertFifo(SlFifoBean fifo);
	
	List<SlFifoBean>  getListAll(Map<String,Object> map);
}
