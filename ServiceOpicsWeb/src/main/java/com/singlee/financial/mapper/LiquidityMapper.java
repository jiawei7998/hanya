package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.LiquidityBean;

public interface LiquidityMapper {
 
	Page<LiquidityBean> getLiquidityList(Map<String, Object> map, RowBounds rb);

	List<LiquidityBean> getLiquidityListByDate(Map<String, Object> map); 
	
}
