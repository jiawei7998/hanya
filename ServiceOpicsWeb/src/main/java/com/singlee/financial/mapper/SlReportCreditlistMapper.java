package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.remoting.RemoteConnectFailureException;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlReportCreditlist;

/***
 * 同业额度明细报表
 * @author LIJ
 *
 */
public interface SlReportCreditlistMapper {
	
	/**
	 * 分页查询-同业额度明细报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	Page<SlReportCreditlist> getPageCreditlistReport(Map<String, Object> map,RowBounds rb) ;
	
	/**
	 * 列表查询-同业额度明细报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlReportCreditlist> getPageCreditlistReport(Map<String, Object> map) ;

}
