package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlQuotedPrice;
import com.singlee.financial.bean.TRtrate;


public interface SlQuotedPriceMapper {
    int deleteByPrimaryKey(SlQuotedPrice key);

    int insert(SlQuotedPrice record);

    SlQuotedPrice selectByPrimaryKey(SlQuotedPrice key);

    int updateByPrimaryKey(SlQuotedPrice record);

	Page<SlQuotedPrice> getQuotedPriceList(Map<String, Object> paramMap,
			RowBounds rb);

	List<SlQuotedPrice> getQuotedPriceByPostDate(String postDate);

	void insertTRtrate(TRtrate tRtrate);

	TRtrate selectTRtrate(TRtrate tRtrate1);
	
	void updateTRtrate(TRtrate tRtrate);
}