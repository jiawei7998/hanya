package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCustSettleAcct;

public interface SlCustSettleAcctMapper {
	
	Page<SlCustSettleAcct> getCSList(Map<String, Object> map, RowBounds rowBounds);

	void updateByCno(Map<String, Object> map);

	SlCustSettleAcct selectByCno(Map<String, Object> map);

	void addCustSettle(Map<String, Object> map);

	void deleteSettle(Map<String, Object> map);

	SlCustSettleAcct selectById(Map<String, Object> map);
	
	SlCustSettleAcct selectMaxSeqByCno(Map<String, Object> map);
	
	List<SlCustSettleAcct> getListByCno(Map<String, Object> map);
	//修改是否默认账户
	void updateIsdefaultById(Map<String, Object> map);
	//复核或退回经办修改处理状态
	void updateDealFlagById(Map<String, Object> map);
	//经办修改处理状态
	void updateManageDealFlagById(Map<String, Object> map);

}
