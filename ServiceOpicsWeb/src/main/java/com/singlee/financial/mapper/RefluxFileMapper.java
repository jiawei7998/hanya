package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIrs9Bean;

public interface RefluxFileMapper {
	/**
	 * 债券持仓查询
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlIrs9Bean> searchRefluxFileCount(Map<String, Object> map,RowBounds rb);
	
}
