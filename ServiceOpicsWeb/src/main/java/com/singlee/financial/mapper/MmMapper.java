package com.singlee.financial.mapper;

import java.util.Map;

import com.singlee.financial.bean.SlDepositsAndLoansBean;

/**
 * OPICS外汇接口调用
 * 
 * @author shenzl
 * 
 */
public interface MmMapper {

	/**
	 * 调用opics存储过程
	 * 
	 * @param map
	 * @return
	 */
	Map<String, String> idldProc(Map<String, String> map);

	/**
	 * 添加信用拆借
	 */
	void addMm(SlDepositsAndLoansBean mmBean);
	/**
	 * 添加信用拆借提前还款
	 * @param mmBean
	 */
	void addMmmAdv(SlDepositsAndLoansBean mmBean);
	/**
	 * 添加信用拆借冲销
	 */
	void addMmRev(SlDepositsAndLoansBean mmBean);
	/**
	 * 检查信用拆借是否存在
	 * 
	 * @param dealNo
	 */
	int getSlFundMm(String dealNo);
	/***
	 * 判断对方账户是否与nost表中的相同
	 * @param fxSptFwdBean
	 * @return
	 */
	Integer isCustAcctSameInNost(SlDepositsAndLoansBean mmBean);
	
}
