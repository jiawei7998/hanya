package com.singlee.financial.mapper;

import java.util.Map;

import com.singlee.financial.bean.SlRepoCrBean;
import com.singlee.financial.bean.SlRepoOrBean;

/**
 * OPICS外汇接口调用
 * 
 * @author shenzl
 * 
 */
public interface RepoMapper {

	/**
	 * 调用opics存储过程
	 * @param mapResult 
	 * 
	 * @param map
	 * @return
	 */
	Map<String, String> ircaProc(Map<String, String> mapResult);
	
	/**
	 * 添加买断式回购
	 */
	void addRepoOr(SlRepoOrBean orBean);
	/**
	 * 添加买断式回购冲销
	 */
	void addRepoOrRev(SlRepoOrBean orBean);
	
	/***
	 * 判断对方账户是否与nost表中的相同
	 * @param orBean
	 * @return
	 */
	Integer isCustAcctSameInNostOr(SlRepoOrBean orBean);
	
	/**
	 * 添加质押式回购
	 */
	void addRepoCr(SlRepoCrBean crBean);
	
	/**
	 * 添加质押式回购冲销
	 */
	void addRepoCrRev(SlRepoCrBean crBean);
	
	/**
	 * 检查质押式回购/买断式回购是否存在
	 * 
	 * @param dealNo
	 */
	int getSlFundCrAndOr(String dealNo);
	
	/***
	 * 判断对方账户是否与nost表中的相同
	 * @param orBean
	 * @return
	 */
	Integer isCustAcctSameInNostCr(SlRepoCrBean crBean);
	
}
