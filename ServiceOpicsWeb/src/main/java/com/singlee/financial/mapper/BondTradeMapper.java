package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlBondTradeBean;

public interface BondTradeMapper {
	/**
	 * 债券交易查询
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlBondTradeBean> searchBondTradeCount(Map<String, Object> map,RowBounds rb);
	
}
