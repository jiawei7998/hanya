package com.singlee.financial.mapper;

import java.util.List;

import com.singlee.financial.bean.SlHsReportBean;

public interface HsReportMapper {

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void generateData(SlHsReportBean hsReport);

	List queryG01(SlHsReportBean hsReport);

	// Page<Gage> getGagePage(Map<String, String> params, RowBounds rb);

}
