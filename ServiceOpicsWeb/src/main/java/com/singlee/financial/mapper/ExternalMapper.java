package com.singlee.financial.mapper;


import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCashFlowBean;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlSecmBean;
import com.singlee.financial.bean.SlSwiftBean;

public interface ExternalMapper {

	/**
	 * 根据TAG,SERVER,FEDEALNO 查询INTH 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	SlInthBean getSlFundInth(SlInthBean inthBean);

	/**
	 * 根据TAG,SERVER,FEDEALNO 查询INTH 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	List<SlInthBean> getSlFundInthList(SlInthBean inthBean);

	/**
	 * 插入OPICS的INTH表数据
	 * 
	 * @param inthBean
	 */
	void addSlFundInth(SlInthBean inthBean);

	/**
	 * 插入OPICS的CUST交易对手信息
	 * 
	 * @param icusBean
	 */
	void addSlFundCust(SlCustBean custBean);
	/**
	 * 检查交易对手是否存在
	 * 
	 * @param icusBean
	 */
	int getSlFundCust(String custNo);

	/**
	 * 检查是否存在bic码
	 * 
	 * @param bic
	 * @return
	 */
	int getSlFundBic(String bic);

	/**
	 * 判断对手母公司国家代码是否存在
	 * 
	 * @param uccode
	 * @return
	 */
	int getSlFundUccode(String uccode);

	/**
	 * 判断交易对手国家代码是否存在
	 * 
	 * @param ccode
	 * @return
	 */
	int getSlFundCcode(String ccode);

	/**
	 * 判断会计类型是否存在
	 * 
	 * @param acctngtype
	 * @return
	 */
	int getSlFundAcctngtype(String acctngtype);

	/**
	 * 判断行业代码SIC是否存在
	 * 
	 * @param sic
	 * @return
	 */
	int getSlFundSic(String sic);

	/**
	 * 判断客户类型是否存在
	 * 
	 * @param ctype
	 * @return
	 */
	int getSlFundCtype(String ctype);
	
	/**
	 * 添加债券信息
	 */
	void addSlFundSemm(SlSecmBean secmBean);
	/**
	 * 通过secid查询债券基本信息
	 * 
	 * @param secid
	 */
	SlSecmBean getSemmById(String secid);
	
	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlSecmBean> getSemmByAll();
	
	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlCustBean> getCustByAll();

	/**
	 * 交易对手信息插入OPICS的CUST表中
	 */
	void addCust(SlCustBean custBean);
	/**
	 * 查询现金流
	 */
	Page<SlCashFlowBean> getCashFlowList(Map<String, String> map,RowBounds rb);

	SlCustBean getCust(Map<String, String> map);

	Page<SlCustBean> searchPageCust(Map<String, Object> map, RowBounds rb);
	
		/**
	 * 生成SL_SWIFT表数据
	 */
	void addSlSwift();
	/**
	 * 查询SL_SWIFT表数据
	 */
	List<SlSwiftBean> getSlSwift();
	
	/**
	 * 更新SL_SWIFT表是否生成状态
	 */
	void updateSlSwift(SlSwiftBean slSwiftBean);
	
	/**
	 * 根据TAG,SERVER,FEDEALNO 查询FXDH表冲销 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	SlInthBean getSlFundFxdhReverse(SlInthBean reverseBean);
	/**
	 * 根据FEDEALNO 查询DLDT表冲销 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	SlInthBean getSlFundDldtReverse(SlInthBean reverseBean);
	/**
	 * 根据FEDEALNO 查询SPSH表冲销 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	SlInthBean getSlFundSpshReverse(SlInthBean reverseBean);
	/**
	 * 根据DEALNO 查询SWDH表冲销 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	SlInthBean getSlFundSwdhReverse(SlInthBean reverseBean);
	/**
	 * 根据DEALNO 查询RPRH表冲销 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	SlInthBean getSlFundRprhReverse(SlInthBean reverseBean);
	
	/**
	 * 将数据插入到SECI表中
	 */
	void addSeci(SlSecmBean slSecmBean);
	
	/**
	 * 将数据插入到CRAT表中
	 */
	void addCrat(SlSecmBean slSecmBean);
	
	/**
	 * 将数据插入到SL_SICYIELD表中
	 */
	void addSicYield(SlSecmBean slSecmBean);
	/**
	 * 根据券号删除SECI表数据
	 */
	void deleteSeciBySecId(String seid);
	
	/**
	 * 根据SERVER,SECID 查询ISEC 数据
	 * 
	 * @param inthBean
	 * @return
	 */
	SlInthBean getFedealNo(SlInthBean inthBean);
	
	/**
	 * 利率重订存储调用
	 * 
	 * @param inthBean
	 * @return
	 */
	SlOutBean interestData(Map<String, String> map);
	/**
	 * 质押式回购查询OPICS库是否有该只券
	 * 
	 * @param seci
	 * @return
	 */
	public SlSecmBean getSeciById(String secid);
	
	/**
	 * 结构性存款冲销交易回查
	 * 
	 */
	public SlInthBean getOtcReverse(SlInthBean reverseBean);
	
	
	
	/**
	 * 结构性存款冲销交易回查
	 * 
	 */
	public SlInthBean getFeesReverse(SlInthBean reverseBean);
	
}