package com.singlee.financial.mapper;

import java.util.List;

import com.singlee.financial.marketdata.hscb.bean.SlFiMarketBean;

/**
 * 徽商银行债券收盘价处理
 * @author chenxh
 *
 */
public interface ISlFiMarketMapper {
	
	/**
	 * 添加收盘价数据到中间处理表
	 * @param secls
	 */
	void insertSlFiMarket(List<SlFiMarketBean> secls);
	
	/**
	 * 删除当前批量日期市场数据
	 */
	void deleteFiMarket();
	
}