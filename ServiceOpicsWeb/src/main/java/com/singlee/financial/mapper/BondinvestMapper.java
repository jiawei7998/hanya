package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.BondinvestBean;

public interface BondinvestMapper {
 
	Page<BondinvestBean> getBondinvestBeanList(Map<String, Object> map, RowBounds rb);
	
	List<BondinvestBean> getBondinvestBeanListByDate(Map<String, Object> map);
}
