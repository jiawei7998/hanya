package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCoreSubject;

public interface SlCoreSubjectMapper {
    int deleteByPrimaryKey(String acctno);

    int insert(SlCoreSubject record);

	// 分页查询
	Page<SlCoreSubject> searchPageOpicsSubject(Map<String, Object> map, RowBounds rb);

    int insertSelective(SlCoreSubject record);

    SlCoreSubject selectByPrimaryKey(String acctno);

    int updateByPrimaryKeySelective(SlCoreSubject record);

    int updateByPrimaryKey(SlCoreSubject record);
}