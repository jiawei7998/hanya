package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlOpcKpsjBean;

public interface OpcKpsjMapper {

	/**
	 * 拆借交易
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjDldt(String datetime);

	/**
	 * 回购交易
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjRepo(String datetime);

	/**
	 * 债券交易
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjSPsh(String datetime);

	/**
	 * 债券交易处置收益
	 * 
	 * @param datetime
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjSPshGain(String datetime);

	/**
	 * 通过卖单交易号算出处置收益
	 * 
	 * @param datetime
	 * @return
	 */
	public SlOpcKpsjBean selectOpcKpsjCountGain(Map<String,Object> map);
	
	/**
	 * 
	 * @param dealno
	 * @return
	 */
	public List<SlOpcKpsjBean> selectOpcKpsjByDelno(String dealno);
}
