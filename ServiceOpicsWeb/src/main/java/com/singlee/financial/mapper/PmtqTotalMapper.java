package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlPmtqAccBean;
import com.singlee.financial.bean.SlPmtqBean;

/**
 * 支付款信息
 * 
 * @author shenzl
 * 
 */
public interface PmtqTotalMapper {
	/**
	 * 获取支付信息
	 * 
	 * @param map
	 * @return
	 */
	List<SlPmtqBean> getPmtq(Map<String, Object> map);

	List<SlPmtqBean> selectBySetmeansAndProduct(String cdate);

	List<SlPmtqBean> getPmtqBySendflag(Map<String, Object> map);

	List<SlPmtqBean> getPmtqByMap(Map<String, Object> map);

	void updateSendflag(Map<String, Object> map);

	List<String> queryInthBydealno(String dealno);

	SlPmtqAccBean selectRepoAmt(Map<String, String> map);

	SlPmtqAccBean selectSecurAmt(Map<String, String> map);
}
