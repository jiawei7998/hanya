package com.singlee.financial.mapper;

import com.singlee.financial.bean.SlAcupBean;

import java.util.List;
import java.util.Map;

/**
 * @author copysun
 */
public interface SubjectEndOfDayCheckMapper {

  List<SlAcupBean> getList(Map<String,Object> map);

}
