package com.singlee.financial.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.ReportBean;

public interface ReportMapper {
	/**
	 * 查询sql语句
	 * 
	 * @param sql
	 * @return
	 */
	List<LinkedHashMap<String, Object>> superManagerSelect(String sql);

	/**
	 * 查询表名称
	 * 
	 * @return
	 */
	List<String> getUserTabComments(String paramTabId);

	/**
	 * 根据tableId查询具体表字段
	 * 
	 * @param paramTableId
	 * @return
	 */
	List<String> getUserTabColums(String paramTableId);
	
	Map<String, String> reportG03_1(Map<String, String> map);

	Map<String, String> reportG03_2(Map<String, String> map);

	Map<String, String> reportG03_3(Map<String, String> map);

	Map<String, String> reportG03_4(Map<String, String> map);

	Map<String, String> reportG03_5(Map<String, String> map);

	Map<String, String> reportG03_6(Map<String, String> map);

	List<ReportBean> serach(ReportBean bean);

}
