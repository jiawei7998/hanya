package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIrevBean;

public interface IrevMapper {
	
	void addIrev(SlIrevBean irevBean);
	
	Page<SlIrevBean> queryIrevPage(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询Irev表指定SERVER最大流水号
	 * @param br
	 * @param server
	 * @param postdate
	 * @return
	 */
	String selectIrevMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
}
