package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.KBStruckBean;

public interface IStruckMapper {

	/**
	 * 国民银行核心反馈数据后需要修改PMTQ_INF表
	 */
	void updatePmtqInf(Map<String,Object> map);
	void updatePmtqInfErr(Map<String,Object> map);
	/**
	 * 国民银行获取录入后待发送数据
	 */
	//a发送交易数据（日间）
	List<String> getOtcPmtqInf(Map<String,Object> map);
	//a发送行权交易（日间，行权操作）
	List<String> getOtcExpPmtqInf(Map<String,Object> map);
	//a发送冲销交易（日渐，冲销操作）
	List<String> getOtcCancelPmtqInf(Map<String,Object> map);
	//a批前发送当天录入交易与行权交易
	List<String> getOtcPreApprovalPmtqInf(Map<String,Object> map);
	//a晚间发送未到期的交易
	List<String> getOtcNightPmtqInf(Map<String,Object> map);
	/**
	 * 获取交易数据：期权和拆借
	 * @param map
	 * @return
	 */
	KBStruckBean getOtcDldtDataKb(Map<String,Object> map);
	/**
	 * 获取交易数据：费用
	 * @param map
	 * @return
	 */
	List<KBStruckBean> getFeeDataKb(Map<String,Object> map);
	/**
	 * 国民银行
	 * 获取期权交易的估值和交易状态：正常，行权，冲销
	 */
	KBStruckBean getOtcDataKb(Map<String,Object> map);
	/**
	 * 国民银行
	 * 获取期权交易的交易对手核心科目号
	 */
	String getOPicsCustCa1(Map<String,Object> map);
	/**
	 * 国民银行
	 * 清除费用没有done的数据
	 */
	void updateFeesInth(Map<String,Object> map);
}
