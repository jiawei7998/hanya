/**
 * Project Name:ServiceOpicsWeb
 * File Name:CommonMapper.java
 * Package Name:com.singlee.mapper
 * Date:2018-6-25上午09:20:20
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
 */
package com.singlee.financial.mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.EsbLogBean;
import com.singlee.financial.bean.SlBondPositionBalanceBean;
import com.singlee.financial.bean.SlCdIssueBalanceBean;
import com.singlee.financial.bean.SlCommonBean;
import com.singlee.financial.bean.SlCustBean;
import com.singlee.financial.bean.SlInthBean;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.bean.SlProfitLossBean;
import com.singlee.financial.bean.SlSdCdInvestHistoryBean;
import com.singlee.financial.bean.SlSdCdProfitAndLossBean;
import com.singlee.financial.bean.SlSposBean;

/**
 * ClassName:CommonMapper <br/>
 * Date: 2018-6-25 上午09:20:20 <br/>
 * 
 * @author 10093
 * @version
 * @since JDK 1.6
 * @see
 */
public interface CommonMapper {
	/**
	 * 添加INTH表
	 */
	void addSlFundInth(SlInthBean inthBean);
	/**
	 * 批量添加INTH表
	 */
	void addBatchSlFundInth(List<SlInthBean> list);
	/**
	 * 添加INTH表 用于冲销
	 */
	void addSlFundInthRev(SlInthBean inthBean);

    /**
     * 判断 inth表里是否有已存在的冲销数据
     */
    int getSlFundInthRev(SlInthBean inthBean);

    /**
     * 判断交易员tradeId在OPER表中是否存在
     * 
     * @param tradeId
     */
    Integer getTradeId(String tradeId);

    /**
     * 判断交易对手tradeId在CUST表中是否存在
     * 
     * @param tradeId
     */
    int getCno(String tradeId);

    /**
     * 判断投资组合port在PORT表中是否存在（根据br、port查）
     * 
     * @param map
     */
    int getPort(Map<String, Object> map);

    /**
     * 判断成本中心cost在COST表中是否存在
     * 
     * @param cost
     */
    int getCost(String cost);

    /**
     * 根据br获取brps表中的系统日期
     * 
     * @param br
     */
    Date getOpicsSysDate(String br);
    
    /**
     * 根据br获取brps表中的上一跑批日期(prevbranprcdate)
     * 
     * @param br
     */
    Date getOpicsPrevbranprcDate(String br);

	/**
	 * 根据br获取brps表中的下一跑批日期(nextbranprcdate)
	 * @param br
	 * @return
	 */
	Date getOpicsNextbranprcdate(String br);

    /**
     * 记录接口日志
     */
    void addEsbLog(EsbLogBean esbLogBean);

    /**
     * 根据部门代码查询用信部门
     */
    String getInstName(String instID);
    
    /**
     * 判断债券是否短头寸
     */
    SlSposBean getSpos(Map<String, Object> map);
    
    /**
     * 查询久期限，止损，DV01
     */
    SlCommonBean queryMarketData(Map<String, Object> map);
    /**
     * 查询债券持仓余额
     * @param rb 
     */
    Page<SlBondPositionBalanceBean> getBondPositionBalance(Map<String, Object> map, RowBounds rb);

	Page<SlSdCdInvestHistoryBean> getCdInvestHistory(Map<String, Object> map,
			RowBounds rb);

	Page<SlSdCdInvestHistoryBean> getSdInvestHistory(Map<String, Object> map,
			RowBounds rb);

	List<SlSdCdProfitAndLossBean> getCdProfitAndLoss(Map<String, Object> map);

	List<SlSdCdProfitAndLossBean> getSdProfitAndLoss(Map<String, Object> map);
	//调用债券投资历史存储过程
	SlOutBean callSdInvestHistory(Map<String, String> map);
	//调用存单投资历史存储过程
	SlOutBean callCdInvestHistory(Map<String, String> map);
	//调用债券损益存储过程
	SlOutBean callSdProfitLoss(Map<String, String> map);
	//调用存单损益存储过程
	SlOutBean callCdProfitLoss(Map<String, String> map);

	Page<SlCdIssueBalanceBean> getCdIssueBalance(Map<String, Object> map,
			RowBounds rb);

	List<SlBondPositionBalanceBean> getBondPositionBalanceExport(
			Map<String, Object> map);

	List<SlCdIssueBalanceBean> getCdIssueBalanceExport(Map<String, Object> map);

	List<SlSdCdInvestHistoryBean> getCdInvestHistoryExport(
			Map<String, Object> map);

	List<SlSdCdInvestHistoryBean> getSdInvestHistoryExport(
			Map<String, Object> map);
	
	/***
	 * 获取OPICS客户列表
	 * @param map
	 * @return
	 */
	List<SlCustBean> getListOpicsCust(Map<String, Object> map);
	
	/***
	 * 根据BR  CCY1  CCY2获取 TERMS(乘除关系)
	 * @param map
	 * @return
	 */
	List<String> getTermsFromCcyp(Map<String, Object> map);
	
	
	SlProfitLossBean getProfitLoss();

	int checkProfitLossH();

	void callProfitLossInit(Map<String, Object> map);

	void callProfitLossH(Map<String, Object> map);
	
	
	
	
	
	
	
	
	
	
    
}