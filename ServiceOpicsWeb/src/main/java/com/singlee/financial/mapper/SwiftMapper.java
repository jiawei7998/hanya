package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlSwiftBean;

/**
 * SWIFT接口调用
 * 
 * @author Qxj
 */
public interface SwiftMapper {

	/**
	 * 从OPICS库中查询IFS_OPICS_SWIFT表中需要的数据
	 */
	List<SlSwiftBean> searchPageSwift(Map<String, Object> map);

	List<SlSwiftBean> searchSwiftBySendflag(Map<String, Object> map);

	void updateSendflag(Map<String, Object> map);
}
