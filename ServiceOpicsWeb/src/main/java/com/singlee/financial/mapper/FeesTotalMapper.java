/**
 * Project Name:ServiceOpicsWeb
 * File Name:FeesMapper.java
 * Package Name:com.singlee.financial.mapper
 * Date:2018-10-17下午02:26:01
 * Copyright (c) 2018, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.singlee.financial.mapper;

import com.singlee.financial.bean.SlIfeeBean;

/**
 * OPICS费用接口调用
 * @author   zhengfl
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public interface FeesTotalMapper {
	
	/**
	 * 费用添加到OPICS中的ifee表中
	 */
	void addFees(SlIfeeBean ifeeBean);
}

