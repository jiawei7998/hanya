package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.singlee.financial.bean.SlBrBranchRef;
import com.singlee.financial.bean.SlHldyBean;

public interface BrBranchRefMapper {
	
	SlBrBranchRef selectByBr(@Param(value = "br")String br);
	
	
	//hldy查询
	SlHldyBean queryHldy(Map<String, Object> map);
}
