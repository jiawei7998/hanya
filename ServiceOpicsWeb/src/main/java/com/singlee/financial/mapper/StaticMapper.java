package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlActyBean;
import com.singlee.financial.bean.SlBicoBean;
import com.singlee.financial.bean.SlCostBean;
import com.singlee.financial.bean.SlCounBean;
import com.singlee.financial.bean.SlHldyBean;
import com.singlee.financial.bean.SlNostBean;
import com.singlee.financial.bean.SlPortBean;
import com.singlee.financial.bean.SlProdBean;
import com.singlee.financial.bean.SlProdTypeBean;
import com.singlee.financial.bean.SlRateBean;
import com.singlee.financial.bean.SlSaccBean;
import com.singlee.financial.bean.SlSetaBean;
import com.singlee.financial.bean.SlSetmBean;
import com.singlee.financial.bean.SlSicoBean;
import com.singlee.financial.bean.SlYchdBean;

public interface StaticMapper {

	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlProdBean getProdById(SlProdBean prodBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlProdBean> getProdByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertProd(SlProdBean prodBean);

	// ++++++++++++++++++++++++PRODTYPE++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlProdTypeBean getProdTypeById(SlProdTypeBean prodTypeBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlProdTypeBean> getProdTypeByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertProdType(SlProdTypeBean prodTypeBean);

	// +++++++++++++++++++++++++COSTCENT+++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlCostBean getCostById(SlCostBean costBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlCostBean> getCostByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertCost(SlCostBean costBean);

	// ++++++++++++++++++++++++++PORTFILIO++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlPortBean getPortById(SlPortBean portBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlPortBean> getPortByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertPort(SlPortBean portBean);

	// ++++++++++++++++++++++++++COUN++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlCounBean getCounById(SlCounBean counBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlCounBean> getCounByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertCoun(SlCounBean counBean);

	// ++++++++++++++++++++++++++BICO++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlBicoBean getBicoById(SlBicoBean bicoBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlBicoBean> getBicoByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertBico(SlBicoBean bicoBean);

	// ++++++++++++++++++++++++++SICO++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlSicoBean getSicoById(SlSicoBean sicoBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlSicoBean> getSicoByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertSico(SlSicoBean sicoBean);

	// ++++++++++++++++++++++++++ACTY++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param prodBean
	 * @return
	 */
	SlActyBean getActyById(SlActyBean actyBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlActyBean> getActyByAll();

	/**
	 * 插入数据
	 * 
	 * @param prodBean
	 */
	void insertActy(SlActyBean actyBean);
	
	// ++++++++++++++++++++++++++SETA++++++++++++++++++++++++++++++++++++++++++++++
	SlSetaBean getSetaById(SlSetaBean slSetaBean);
	
	List<SlSetaBean> getSetaByAll();
	
	void insertSeta(SlSetaBean setaBean);
	
	// ++++++++++++++++++++++++++NOST++++++++++++++++++++++++++++++++++++++++++++++
	SlNostBean getNostById(SlNostBean slNostBean);
	
	List<SlNostBean> getNostByAll();
	
	Page<SlNostBean> searchPageNos(Map<String, Object> map, RowBounds rb);
	
	void insertNost(SlNostBean nostBean);
	
	// ++++++++++++++++++++++++++SACC++++++++++++++++++++++++++++++++++++++++++++++
	SlSaccBean getSaccById(SlSaccBean slSaccBean);
	
	List<SlSaccBean> getSaccByAll();
	
	void insertSacc(SlSaccBean saccBean);
	
	// ++++++++++++++++++++++++++RATE   利率代码++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param rateBean
	 * @return
	 */
	SlRateBean getRateById(SlRateBean rateBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlRateBean> getRateByAll();

	/**
	 * 插入数据
	 * 
	 * @param rateBean
	 */
	void insertRate(SlRateBean rateBean);
	
	
	// ++++++++++++++++++++++++++YCHD  利率曲线++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param ychdBean
	 * @return
	 */
	SlRateBean getYchdById(SlYchdBean ychdBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlYchdBean> getYchdByAll();

	/**
	 * 插入数据
	 * 
	 * @param ychdBean
	 */
	void insertYchd(SlYchdBean ychdBean);
	
	
	// ++++++++++++++++++++++++++SETM  清算方式++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param setmBean
	 * @return
	 */
	SlSetmBean getSetmById(SlSetmBean setmBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlSetmBean> getSetmByAll();

	/**
	 * 插入数据
	 * 
	 * @param setmBean
	 */
	void insertSetm(SlSetmBean setmBean);
	
	// ++++++++++++++++++++++++++  HLDY  节假日   ++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * 通过主键获取数据主要用于检测是否重复
	 * 
	 * @param ychdBean
	 * @return
	 */
	SlHldyBean getHldyById(SlHldyBean hldyBean);

	/**
	 * 获取所有数据
	 * 
	 * @return
	 */
	List<SlHldyBean> getHldyByAll();

	/**
	 * 插入数据
	 * 
	 * @param ychdBean
	 */
	void insertHldy(SlHldyBean hldyBean);

	List<SlProdTypeBean> synchroProdTypeByProd(Map<String, Object> map);

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
