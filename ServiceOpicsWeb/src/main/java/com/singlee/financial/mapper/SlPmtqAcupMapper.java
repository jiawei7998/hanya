package com.singlee.financial.mapper;

import com.singlee.financial.bean.SlPmtqAcupBean;

import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SlPmtqAcupMapper extends Mapper<SlPmtqAcupBean>{

    public List<SlPmtqAcupBean> getSlPmtqAcupListAndTrim(SlPmtqAcupBean slPmtqAcupBean);
}
