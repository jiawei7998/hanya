package com.singlee.financial.mapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.AcupSendBean;
import com.singlee.financial.bean.SendInfo;
import com.singlee.financial.bean.SlAcupBean;
import com.singlee.financial.bean.SlAcupRecord;
import com.singlee.financial.bean.SlAcupTaxBean;
import com.singlee.financial.bean.SlOutBean;

/**
 * OPICS外汇接口调用
 * 
 * @author zc
 * 
 */
public interface AcupTotalMapper {

	/**
	 * 调用opics存储过程
	 * 
	 * @param map
	 * @return
	 */
	SlOutBean acupCreate(Map<String, String> map);

	AcupSendBean getAcupHead();

	List<AcupSendBean> getSendAcup();

	Integer updateResult(AcupSendBean acup);

	Page<SlAcupBean> getAcupPage(Map<String, String> params, RowBounds rb);

	// 查询转化失败账务
	Page<SlAcupBean> getAcupErrPage(Map<String, String> params, RowBounds rb);

	Integer getAcupCount();

	//List<SendInfo> getAcupInfo(String sendflag);

	void upAcupInfo(SendInfo sendInfo);

	void upAcupInfoQuery(SendInfo sendInfo);

	List<SlAcupBean> getAcupById(Map<String, String> map);

	// 查询日期
	String getOpicsDate(Map<String, String> map);

	// 获取opics机构
	List<Map<String, String>> getOpicsBr(Map<String, String> map);

	// 获取请求报文和下载文件需要的委托号
	List<SendInfo> getSendInfo(Date date);

	/**
	 * 根据日期更新Sl_ACUP_SENDINFO
	 */
	void updateAcupInfo(SendInfo sendInfo);

	/**
	 * 查询当前日期下的所有日终总账
	 */
	List<AcupSendBean> getTlSendAcup(String postDate);

	/**
	 * 查询指定套号的所有日终总账(用于重发)
	 */
	List<AcupSendBean> getRetrySendAcup(Map<String, String> map);
	

	List<AcupSendBean> getRetrySendAcupNew(Map<String, Object> map);

	/**
	 * 从SL_SERIALNO表中取流水号
	 */
	String getSerialNo(String tableId);

	/**
	 * 更新SL_SERIALNO表中的流水号
	 */
	void updateSerialNo(String tableId);

	/**
	 * 获取SL_ESBLOG表中的最大seq,作为流水号的一部分
	 */
	String getSeqNo(String tableId);

	/**
	 * 更新SL_ACUP
	 */
	void updateAcupTemp(AcupSendBean acupTempBean);

	/**
	 * 总账重发更新SL_ACUP
	 */
	void updateRetryAcup(AcupSendBean acupTempBean);

	/**
	 * 从SL_ACUP表根据日期(postdate)查询当日有多少套记账失败
	 */
	String getFailSetAcup(String date);

	/**
	 * 从SL_ACUP表根据日期(postdate)查询当日总共有多少套
	 */
	String getTotalSetAcup(String date);

	/**
	 * 查询账务状态
	 */
	String getFlag(String date);

	/**
	 * 判断查询的日期OPICS是否生成了账务BRPS
	 */
	String getOPICSFlag(Map<String, String> map);

	/**
	 * 查询委托号
	 */
	String getCoreseq(String date);

	/**
	 * 从SL_ACUP表根据日期(postdate)查询当日是否有总账生成
	 */
	Integer getCreateFlag(String date);

	/**
	 * 根据日期(postdate)更新SL_ACUP_SENDINFO表的sendflag,修改为0未生成
	 */
	void updateAcupSend(String date);

	/**
	 * 根据日期(postdate)删除SL_ACUP_SENDINFO表的对应记录
	 */
	void deleteAcupSendInfo(String date);
	
	/**
	 * 新增sendinfo数据
	 * @param sendInfo
	 */
	void addAcupInfo(SendInfo sendInfo);
	/**
	 * 根据BR .POSTDATE .SENDTYPE 查询
	 * @param sendInfo
	 * @return
	 */
	SendInfo getAcupInfo(SendInfo sendInfo);

	/**
	 * 获取所有总账数据(返回list)
	 */
	List<SlAcupBean> getAllData(Map<String, String> map);

	/**
	 * 根据条件查询数据
	 */
	Page<SlAcupBean> getSortAcup(Map<String, String> params, RowBounds rb);

	/**
	 * 根据条件查询只显示套号的总账
	 */
	Page<SlAcupBean> getSetnoAcup(Map<String, String> params, RowBounds rb);
	
	/**
	 * 根据条件查询只显示套号的错账
	 */
	Page<SlAcupBean> getSetnoAcupNew(Map<String, String> params, RowBounds rb);

	/**
	 * 根据套号查询总账
	 */
	Page<SlAcupBean> getSetnoDetailAcup(Map<String, String> params, RowBounds rb);

	/**
	 * 根据机构号查询总账数据（记录操作）
	 */
	int getSuccAcupCount(Map<String, String> map);

	int getFailAcupCount(Map<String, String> map);

	int getSetNoCount(Map<String, String> map);

	/**
	 * 记录操作详情
	 */
	void insertRecord(SlAcupRecord record);

	List<SlAcupRecord> queryOperaRec(SlAcupRecord record);

	void updateRecord(SlAcupRecord record);

	/**
	 * 查询估值
	 */
	BigDecimal queryValue(Map<String, String> map);

	/**
	 * 查询汇率
	 */
	BigDecimal queryRate(Map<String, String> map);

	/**
	 * 查询价格
	 */
	BigDecimal queryPrice(Map<String, String> map);

	/**
	 * 查询债券上次付息日
	 */
	Date queryDate(Map<String, Object> map);

	/**
	 * 查询价格（价格异常检测偏离度使用）
	 */
	BigDecimal queryRhisIntrate(Map<String, Object> map);

	/**
	 * 查询价格（债券上日中债估值净价）
	 */
	BigDecimal querySeclPrice(Map<String, Object> map);

	/**
	 * 查询价格（同评级同期限存单收益率曲线）
	 */
	BigDecimal querySeclRate(Map<String, Object> map);

	/**
	 * 获取所有历史总账数据
	 */
	List<SlAcupBean> getAllHistoryData(Map<String, String> map);

	/**
	 * 获取外汇所有交易止损总额
	 */
	BigDecimal queryFxdSum(Map<String, Object> map);

	/**
	 * 获取节假日
	 */
	int checkHldy(Map<String, Object> map);
	
	
	/******************************
	 * 
	 * 华商银行
	 * 
	 *********************************/
	/**
	   *   分页查询总账
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlAcupTaxBean> queryAcupPage(Map<String, Object> map, RowBounds rb);
	
	/**
	   *    查询待发送的总账，从SL_ACUP_TAX取数
	 * @param map
	 * @return
	 */
	List<SlAcupTaxBean> getSendAcupTax(Map<String, Object> map);
	/**
	   *    查询SL_ACUP
	 * @param map
	 * @return
	 */
	List<SlAcupTaxBean> queryAcupList(Map<String, String> map);
	 /**
	      *     从SL_ACUP_TAX表根据日期(postdate)、部门查询当日是否有总账生成
	  * @param map
	  * @return
	  */
	Integer getCreateFlagCmbh(Map<String, Object> map);
	
	/**
	 * 查询账务状态
	 */
	String getFlagCmbh(Map<String, String> map);

	
	/**
	   * 总账是否已经发送
	 * @param map
	 * @return
	 */
	String getSendFlagCmbh(Map<String, String> map);
	
	/**
	 * 查看总账
	 */
	SendInfo getSendFlag();
	
	/**
	   *    查询所有部门
	 * @return
	 */
	List<String> getAllOpicsBr();
	/**
	   * 更新核心返回结果
	 */
	void updateAcupResult(SlAcupTaxBean bean);
	
	List<SendInfo> getSendInfoList(Map<String, Object> map);
	
	/**
	 * 	外汇异常账务列表查询-分页
	 */
	Page<SlAcupTaxBean> queryErrorAcupPage(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 	列表查询
	 */
	List<SlAcupTaxBean> queryErrorAcupList(Map<String, Object> map);
	
	
	/**
	 * 	全天交易列表查询-分页
	 */
	Page<SlAcupTaxBean> queryTodayPage(Map<String, Object> map, RowBounds rb);
	
	/**
	 *	 列表查询
	 */
	List<SlAcupTaxBean> queryTodayList(Map<String, Object> map);

	void callAcupProfitLoss(Map<String, Object> qMap);

	void callProfitLoss(Map<String, Object> qMap);
	
	/**
	 * 查询当前日期下的所有日终总账
	 */
	List<AcupSendBean> getSlAcupList(Map<String, Object> map);
	
	/**
	 * 修改处理状态
	 */
	void updateAcupHendle(AcupSendBean acupSendBean);
}