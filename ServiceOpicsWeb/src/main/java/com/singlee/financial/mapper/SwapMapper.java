package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlSwapCrsBean;
import com.singlee.financial.bean.SlSwapCurBean;
import com.singlee.financial.bean.SlSwapIrsBean;

/**
 * OPICS外汇接口调用
 * 
 * @author shenzl
 * 
 */
public interface SwapMapper {

	/**
	 * 调用opics存储过程
	 * @param mapResult 
	 * 
	 * @param map
	 * @return
	 */
	Map<String, String> cfestSwapProc(Map<String, String> mapResult);
	
	/**
	 * 添加货币互换
	 */
	void addSwapCrs(SlSwapCrsBean crsBean);
	/**
	 * 添加货币互换冲销
	 */
	void addSwapCrsRev(SlSwapCrsBean crsBean);
	
	/**
	 * 检查货币互换/利率互换是否存在
	 * 
	 * @param dealNo
	 */
	int getSlFundCrsAndIRS(String dealNo);
	
	/***
	 * 判断对方账户是否与nost表中的相同
	 * @param orBean
	 * @return
	 */
	Integer isCustAcctSameInNostCrs(SlSwapCrsBean crsBean);
	
	/**
	 * 添加利率互换
	 */
	void addSwapIrs(SlSwapIrsBean irsBean);
	
	/**
	 * 添加利率互换冲销
	 */
	void addSwapIrsRev(SlSwapIrsBean irsBean);
	
	/***
	 * 判断对方账户是否与nost表中的相同
	 * @param orBean
	 * @return
	 */
	Integer isCustAcctSameInNostIrs(SlSwapIrsBean irsBean);
	/***
	 * 货币互换头寸敞口
	 * @param map
	 * @param rb
	 * @return
	 */
    Page<SlSwapCurBean> searchCurSwapExposureCount(Map<String, Object> map, RowBounds rb);
	
	List<SlSwapCurBean> searchCurSwapExposureCount(Map<String, Object> map);
}
