package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;
import com.singlee.financial.bean.SlOpcNssbBean;

public interface OpcNssbMapper {

	/**
	 * 纳税申报申请取数
	 * 
	 * @param map
	 * @return
	 */
	public List<SlOpcNssbBean> selectOpcNssbPayTaxes(Map<String, Object> map);

	/**
	 * 根据转让债权计算表
	 * 
	 * @param map
	 * @return
	 */
	public List<SlOpcNssbBean> selectOpcNssbCreditorRight(Map<String, Object> map);
}
