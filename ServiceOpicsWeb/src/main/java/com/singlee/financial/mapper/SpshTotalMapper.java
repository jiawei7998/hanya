package com.singlee.financial.mapper;

import java.util.Map;

import com.singlee.financial.bean.SlRprhDldtBean;
import com.singlee.financial.bean.SlSpshBean;

public interface SpshTotalMapper {
	
	public SlSpshBean getSpshBean(Map<String, Object> map);

	public SlRprhDldtBean getRprhDldtBean(Map<String, Object> map);

}
