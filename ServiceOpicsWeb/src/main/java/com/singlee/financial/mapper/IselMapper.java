package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.IselBean;
import com.singlee.financial.bean.SlIselBean;

public interface IselMapper {

	int selectBykey(IselBean iselBean);

	void updateBykey(IselBean iselBean);

	void addIsel(IselBean iselBean);

	void addSlIsel(IselBean iselBean);
	
	/**
	 * add by SUNYH
	 * @param iselBean
	 */
	void addBatchSlIsel(List<SlIselBean> iselBean);
	
	Page<IselBean> queryIselPage(Map<String, Object> map, RowBounds rb);

	/**
	 * 查询ISEL表指定SERVER最大流水号
	 * @param br
	 * @param server
	 * @param postdate
	 * @return
	 */
	String selectIselMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
}
