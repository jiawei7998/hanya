package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.SlOpcCzsyBean;

public interface OpcCzsyMapper {

	public List<SlOpcCzsyBean> selectOpcCzsy(Map<String, Object> map);
	
	/**
	 * 获取科目号
	 * @param map
	 * @return
	 */
	public List<SlOpcCzsyBean> selectGlno(Map<String, Object> map);
}
