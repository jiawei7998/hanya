package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;

public interface OdsFileMapper {
	
	
	Page<Map<String, Object>> getListOdsInfo(Map<String, Object> map, RowBounds rb);
	
	int getCount(Map<String, Object> map);
	
	

}
