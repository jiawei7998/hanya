package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlNupdBean;

public interface SlNupdAcupMapper {
	/**
	 * 获取日间往来账
	 * @param map
	 * @return
	 */
	public Page<SlNupdBean> acupSearch(Map<String, String> map, RowBounds rb);
	/**
	 * 账务发送后更新发送状态
	 * @param map
	 * @return
	 */
	public void updateAcup(Map<String, String> map);
	/**
	 * 根据交易流水查询日间账
	 * @param map
	 * @return
	 */
	public List<SlNupdBean> getNupdById(Map<String,String> map);
	/**
	 * 获取往来账
	 * @param valDate
	 * @return
	 */
	List<SlNupdBean> acupSearch(Map<String, String> paramMap);
}
