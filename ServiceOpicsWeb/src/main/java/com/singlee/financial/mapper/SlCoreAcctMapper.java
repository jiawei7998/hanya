package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlCoreAcctBean;

/**
 * 核心账户
 * 
 * 
 */
public interface SlCoreAcctMapper {

	Page<SlCoreAcctBean> searchPageCoreAcct(Map<String, Object> map, RowBounds rb);

	void coreAcctAdd(Map<String, Object> map);

	void updateById(SlCoreAcctBean entity);

	void deleteCoreAcct(Map<String, String> map);
	
	
}