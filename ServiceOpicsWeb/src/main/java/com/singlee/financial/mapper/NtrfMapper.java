package com.singlee.financial.mapper;


import com.singlee.financial.bean.SlNtrfBean;

/**
 * 头寸调拨
 */
public interface NtrfMapper {
    /**
     * Ntrf 新增
     */
    void addNtrf(SlNtrfBean slNtrfBean);
    
}
