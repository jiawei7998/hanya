package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlBondCashBean;
import com.singlee.financial.bean.SlExposureBean;

public interface ExposureMapper {
	/**
	 * 即期外汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxExposure(Map<String, Object> map,RowBounds rb);
	/**
	 * 历史即期外汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxExposureHistory(Map<String, Object> map,RowBounds rb);
	/**
	 * 即期结售汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxSettleExposure(Map<String, Object> map,RowBounds rb);
	/**
	 * 历史即期结售汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxSettleExposureHistory(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 债券头寸查询
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlBondCashBean> searchBondCashCount(Map<String, Object> map,RowBounds rb);
	
	/**
	 * 远期外汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxForwardExposure(Map<String, Object> map,RowBounds rb);
	/**
	 * 远期结售汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxForwardSettleExposure(Map<String, Object> map,RowBounds rb);
	/**
	 * 掉期外汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxSwapExposure(Map<String, Object> map,RowBounds rb);
	/**
	 * 掉期结售汇敞口
	 * @param map
	 * @param rb
	 * @return
	 */
	Page<SlExposureBean> getFxSwapSettleExposure(Map<String, Object> map,RowBounds rb);
	

}
