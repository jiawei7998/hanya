package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlReportG14Bean;
import com.singlee.financial.bean.SlReportG18Bean;
import com.singlee.financial.bean.SlReportG21Bean;
import com.singlee.financial.bean.SlReportG22Bean;
import com.singlee.financial.bean.SlReportG24Bean;
import com.singlee.financial.bean.SlReportG31Bean;
import com.singlee.financial.bean.SlReportG33Bean;
import com.singlee.financial.bean.SlReportY0085Bean;

public interface CmbhReportMapper {
	//G14
	Page<SlReportG14Bean> getSlReportG14List(Map<String, Object> map, RowBounds rb);
	List<SlReportG14Bean> getSlReportG14List(Map<String, Object> map);
	//G18
	Page<SlReportG18Bean> getSlReportG18List(Map<String, Object> map, RowBounds rb);
	List<SlReportG18Bean> searchSlReportG18List(Map<String, Object> map);
	//G21
	Page<SlReportG21Bean> getSlReportG21List(Map<String, Object> map, RowBounds rb);
	List<SlReportG21Bean> searchSlReportG21List(Map<String, Object> map);
	//G22
	Page<SlReportG22Bean> getSlReportG22List(Map<String, Object> map, RowBounds rb);
	List<SlReportG22Bean> searchSlReportG22List(Map<String, Object> map);
	//G24
	Page<SlReportG24Bean> getSlReportG24List(Map<String, Object> map, RowBounds rb);
	List<SlReportG24Bean> getSlReportG24List(Map<String, Object> map);
	//G31
	Page<SlReportG31Bean> getSlReportG31List(Map<String, Object> map, RowBounds rb);
	List<SlReportG31Bean> searchSlReportG31List(Map<String, Object> map);
	//G33
	Page<SlReportG33Bean> getSlReportG33List(Map<String, Object> map, RowBounds rb);
	List<SlReportG33Bean> getSlReportG33List(Map<String, Object> map);
	//Y0085
	Page<SlReportY0085Bean> getSlReportY0085List(Map<String, Object> map, RowBounds rb);
	List<SlReportY0085Bean> getSlReportY0085List(Map<String, Object> map);
}
