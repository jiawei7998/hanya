package com.singlee.financial.mapper;

import java.util.Map;

/**
 * OPICS外汇接口调用
 * 
 * @author zhoukx
 * 
 */
public interface SemmMapper {

	/**
	 * 调用opics存储过程
	 * 
	 * @param map
	 * @return
	 */
	String semmProc(Map<String, String> map);
}
