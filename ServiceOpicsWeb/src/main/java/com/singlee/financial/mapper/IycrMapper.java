package com.singlee.financial.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlIycrBean;

/**
 * 收益率曲线
 * @author zhengfl
 *
 */
public interface IycrMapper {

	void addIycr(SlIycrBean iycrBean);
	
	Page<SlIycrBean> queryIycrPage(Map<String, Object> map, RowBounds rb);
	
	/**
	 * 查询Iycr表指定SERVER最大流水号
	 * @param br
	 * @param server
	 * @param postdate
	 * @return
	 */
	String selectIycrMaxFedealno(@Param("br") String br,@Param("server")String server,@Param("postdate") String postdate);
}
