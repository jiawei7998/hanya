package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.remoting.RemoteConnectFailureException;
import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlReportZjgyjz;
/***
 * 资金交易公允价值监测报表
 * @author LIJ
 *
 */
public interface SlReportZjgyjzMapper {
	
	/**
	 * 分页查询-资金交易公允价值监测报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	Page<SlReportZjgyjz> getPageZjgyjzReport(Map<String, Object> map,RowBounds rb) ;
	
	/**
	 * 列表查询-资金交易公允价值监测报表
	 * @param map
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	List<SlReportZjgyjz> getPageZjgyjzReport(Map<String, Object> map) ;

}
