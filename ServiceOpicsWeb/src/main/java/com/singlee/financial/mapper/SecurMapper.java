package com.singlee.financial.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.github.pagehelper.Page;
import com.singlee.financial.bean.SlFixedIncomeBean;
import com.singlee.financial.bean.SlImbdBean;
import com.singlee.financial.bean.SlSecmBean;
import com.singlee.financial.bean.SlSecmRedemption;
import com.singlee.financial.bean.SlSecmRevaluation;
import com.singlee.financial.bean.SlSecurityLendingBean;

/**
 * OPICS外汇接口调用
 * 
 * @author shenzl
 * 
 */
public interface SecurMapper {

	/**
	 * 调用opics存储过程
	 * 
	 * @param mapResult
	 * 
	 * @param map
	 * @return
	 */
	Map<String, String> bredProc(Map<String, String> mapResult);

	/**
	 * 添加债券买卖
	 */
	void addFi(SlFixedIncomeBean fiBean);

	/**
	 * 添加债券买卖冲销
	 */
	void addFiRev(SlFixedIncomeBean fiBean);

	/**
	 * 检查债券买卖是否存在
	 * 
	 * @param dealNo
	 */
	int getSlFundFi(String dealNo);

	/***
	 * 判断对方账户是否与nost表中的相同
	 * 
	 * @param fiBean
	 * @return
	 */
	Integer isCustAcctSameInNost(SlFixedIncomeBean fiBean);

	/**
	 * 添加债券借贷
	 */
	void addSeclend(SlSecurityLendingBean securityLendingBean);

	/**
	 * 添加债券借贷冲销
	 */
	void addSeclendRev(SlSecurityLendingBean securityLendingBean);

	/**
	 * 获取当前需要提醒的还本收息
	 * 
	 * @param params br:当前分支,brprcindte当前账务日期
	 * @param rb
	 * @return
	 */
	Page<SlSecmRedemption> getRedemption(Map<String, Object> params, RowBounds rb);
	
	void addImbd(SlImbdBean imbdBean);
	
	/**
	 * 获取当前需要提醒的还本收息
	 * 
	 * @param params br:当前分支,brprcindte当前账务日期
	 * @param rb
	 * @return
	 */
	List<SlSecmRedemption> getRedemptdWithList(Map<String, Object> params);

	Page<SlSecmRevaluation> getPageSecmRevaluation(Map<String, Object> map,
			RowBounds rb);
	/**
	 * 查询secm 数据
	 */
	SlSecmBean searchSlSecmOne(String secid);
}
