package com.singlee.financial.dbcp;

import org.apache.commons.dbcp.BasicDataSource;

public class SingleeDBCPBasicDataSource extends BasicDataSource {

	@Override
	public synchronized void setPassword(String password) {
		super.setPassword(Base64Creator.decode(password));
	}
}
