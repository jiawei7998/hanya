package com.singlee.financial.dbcp;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;

import com.singlee.financial.common.spring.SpringApdaterConfig;
import com.singlee.financial.common.util.JY;
import com.singlee.financial.common.util.MsgUtils;
import com.singlee.financial.common.util.SpringApdaterConfigUtils;

public class DecryptPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

	/** 需要替换密码明文的key */
	private static final String[] passwdKeyLaws = new String[] { "jdbc.password", "system.sshPass",
			"mail.smtp.password" };

	private static Pattern encryptedPattern = Pattern.compile("Encrypted:\\{(\\S*)\\}"); // 加密属性特征正则
	
	private SpringApdaterConfig springApdaterConfig;
	
	public DecryptPropertyPlaceholderConfigurer(SpringApdaterConfig springApdaterConfig){
		super();
		this.springApdaterConfig = springApdaterConfig;
	}

	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
			throws BeansException {
		super.processProperties(beanFactory, props);
		properties = props;
		if (needSave) {
			save(props);
		}
	}

	@Override
	protected String convertProperty(String propertyName, String propertyValue) {
		SpringApdaterConfigUtils.addProperty(propertyName, propertyValue, springApdaterConfig);
		for (String pkd : passwdKeyLaws) {
			if (propertyName.equalsIgnoreCase(pkd)) {
				final Matcher matcher = encryptedPattern.matcher(propertyValue); // 判断该属性是否已经加密
				if (matcher.matches()) { // 已经加密，进行解密
					String encryptedString = matcher.group(1); // 获得加密值
					String decryptedPropValue = CodecDES.decrypt(encryptedString);
					if (decryptedPropValue != null) { // !=null说明正常
						propertyValue = decryptedPropValue; // 设置解决后的值
					} else {// 说明解密失败
						String msg = MsgUtils.getMessage("error.common.0004", propertyName, propertyValue);
						JY.raise(msg);
					}
				} else {
					// 标记数据需要被加密保存
					needSave = true;
				}
			}
		}
		return super.convertProperty(propertyName, propertyValue); // 将处理过的值传给父类继续处理
	}

	/**
	 * 获得所有的配置
	 * 
	 * @return
	 * @throws IOException
	 */
	public Properties getProperties() throws IOException {
		return properties;
	}

	protected Resource[] locations;
	protected boolean needSave = false;
	protected Properties properties;

	@Override
	public void setLocations(Resource... locations) { // 由于location是父类私有，所以需要记录到本类的locations中
		Resource [] res = SpringApdaterConfigUtils.addLocations(springApdaterConfig.getFilterPrefix(), springApdaterConfig.getFilterSuffix(), locations);
		super.setLocations(res);
		this.locations = res;
	}
	
	@Override
	public void setLocation(Resource location) { // 由于location是父类私有，所以需要记录到本类的locations中
		super.setLocation(location);
		this.locations = new Resource[] { location };
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		super.postProcessBeanFactory(beanFactory); // 正常执行属性文件加载
	}

	/**
	 * 
	 * @param props
	 */
	private void save(Properties props) {
		for (Resource location : locations) { // 加载完后，遍历location，对properties进行加密
			try {
				final File file = location.getFile();
				if (file.isFile()) { // 如果是一个普通文件
					if (file.canWrite()) { // 如果有写权限
						encrypt(file, props); // 调用文件加密方法
					} else {
						LogFactory.getLog(DecryptPropertyPlaceholderConfigurer.class).warn("File '" + location + "' can not be write!");
					}
				} else {
					LogFactory.getLog(DecryptPropertyPlaceholderConfigurer.class).warn("File '" + location + "' is not a normal file!");
				}
			} catch (IOException e) {
				LogFactory.getLog(DecryptPropertyPlaceholderConfigurer.class).warn("File '" + location + "' is not a normal file!");
			}
		}
	}

	/**
	 * 
	 * @param file
	 * @param localProperties
	 */
	private void encrypt(File file, Properties properties) {
		try {
			PropertiesConfiguration config = new PropertiesConfiguration(file);
			Iterator<String> iter = config.getKeys();
			while (iter.hasNext()) {
				String key = iter.next();
				for (String pkd : passwdKeyLaws) {
					if (key.equalsIgnoreCase(pkd)) {
						String value = properties.getProperty(key);
						String newValue = "Encrypted:{" + StringUtils.trim(CodecDES.encrypt(value)) + "}";
						config.setProperty(key, newValue);
					}
				}
			}
			config.save();
		} catch (ConfigurationException e) {
			LogFactory.getLog(DecryptPropertyPlaceholderConfigurer.class).warn("保存属性文件" + file.getName() + "出错！" + e.getLocalizedMessage());
		}

	}

	/**
	 * 
	 * @param map          参数列表
	 * @param paramName    参数名称
	 * @param defaultValue 取值错误时的默认返回值
	 * @return
	 */
	public String getString(String paramName, String defaultValue) {
		String obj = properties.getProperty(paramName);
		if (obj != null) {
			return obj;
		} else {
			return defaultValue;
		}
	}

	/**
	 * 
	 * @param map          参数列表
	 * @param paramName    参数名称
	 * @param defaultValue 取值错误时的默认返回值
	 * @return
	 */
	public int getInt(String paramName, int defaultValue) {
		try {
			return Integer.parseInt(properties.getProperty(paramName));
		} catch (Exception ex) {
			return defaultValue;
		}
	}

}
