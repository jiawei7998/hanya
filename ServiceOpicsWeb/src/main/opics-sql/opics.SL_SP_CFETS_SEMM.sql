CREATE OR REPLACE 
procedure SL_SP_CFETS_SEMM(
                        I_dealno              IN CHAR,
                        I_status              IN CHAR,
                        I_opicsdealno         IN CHAR,
                        I_br                  IN CHAR,
                        I_ccy                 IN CHAR,
                        I_secunit             IN CHAR,
                        I_denom               IN CHAR,
                        I_descr               IN CHAR,
                        I_spreadrate_8        IN CHAR,
                        I_intenddaterule      IN CHAR,
                        I_bndnm_cn            IN CHAR,
                        I_bndnm_en            IN CHAR,
                        I_bndcd               IN CHAR,
                        I_accountno           IN CHAR,
                        I_pubdt               IN CHAR,
                        I_issuevol            IN CHAR,
                        I_issprice            IN CHAR,
                        I_intpaymethod        IN CHAR,
                        I_intpaycircle        IN CHAR,
                        I_couponrate          IN CHAR,
                        I_issudt              IN CHAR,
                        I_matdt               IN CHAR,
                        I_bondperiod          IN CHAR,
                        I_basicspread         IN CHAR,
                        I_paragraphfirstplan  IN CHAR,
                        I_bondproperties      IN CHAR,
                        I_issuer              IN CHAR,
                        I_issuername          IN CHAR,
                        I_floatratemark       IN CHAR,
                        I_bondratingclass     IN CHAR,
                        I_bondratingagency    IN CHAR,
                        I_bondrating          IN CHAR,
                        I_product             IN CHAR,
                        I_prodtype            IN CHAR,
                        I_settccy             IN CHAR,
                        I_paramt              IN CHAR,
                        I_basis               IN CHAR,
                        I_intcalcrule         IN CHAR,
                        I_note                IN CHAR,
                        I_userid              IN CHAR,
                        I_checkUserId         IN CHAR,
                        I_element             IN CHAR,
                        I_inputdate           IN CHAR,
                        O_RETCODE    OUT CHAR,
                        O_RETMSG     OUT CHAR) is
 -- SECM 现券交易表
  VS_secid   CHAR(10);
  VS_ccy      CHAR(3);
  VS_secunit  CHAR(3);
  VS_denom     CHAR(15);
  VS_descr       CHAR(70);
  VS_spreadrate_8   CHAR(20);
  VS_intenddaterule   CHAR(1);
 -- VS_mdate         DATE;
  VS_issuer        CHAR(10);
  VS_product        CHAR(6);
  VS_prodtype        CHAR(2);
  VS_settccy         CHAR(3);
  VS_paramt          CHAR(20);
  VS_basis           CHAR(6);
  VS_intcalcrule       CHAR(3);
  --VS_putdate           DATE;

Begin
 VS_secid           :='5588';
 VS_ccy             := I_ccy;
 VS_secunit         := I_secunit;
 VS_denom           := I_denom;
 VS_descr           := I_descr;
 VS_spreadrate_8    := I_spreadrate_8;
 VS_intenddaterule  := I_intenddaterule;
 --VS_mdate         :=   I_matdt ;
 VS_issuer        :=   I_issuer;
 VS_product       :=   I_product;
 VS_prodtype      :=   I_prodtype;
 VS_settccy       :=   I_settccy;
 VS_paramt        :=   I_paramt;
 VS_basis         :=   I_basis;
 VS_intcalcrule   :=   I_intcalcrule;
--VS_putdate       :=  to_date  I_inputdate, 'YYYY-MM-DD');
  BEGIN
    INSERT INTO SECM
      (secid,
       ccy,
       secunit,
       denom,
       descr,
       spreadrate_8,
       intenddaterule,
       issuer,
       product,
       prodtype,
       settccy,
       paramt,
       basis,
       intcalcrule     
       )
VALUES
      (VS_secid,
       VS_ccy,
       VS_secunit,
       VS_denom,
       VS_descr,
       VS_spreadrate_8,
       VS_intenddaterule,
       VS_issuer,
       VS_product,
       VS_prodtype,
       VS_settccy,
       VS_paramt,
       VS_basis,
       VS_intcalcrule    
       );
  EXCEPTION
    WHEN OTHERS THEN
      O_RETCODE := '105';
      O_RETMSG  :='插入SECM报错！'||substr(sqlerrm,1,200);
      RETURN;
  end;
  O_RETCODE := '999';
  O_RETMSG  :='成功！';
end SL_SP_CFETS_SEMM;
