
insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, null, 'USD/CNY', 'USD/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, null, 'AUY/CNY', 'AUY/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'AUD/USD', 'AUD/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CAD/CNY', 'CAD/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CAD/USD', 'CAD/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CHF/CNY', 'CHF/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CHF/USD', 'CHF/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'EUR/CNY', 'EUR/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'GBP/CNY', 'GBP/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'HKD/CNY', 'HKD/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'JPY/CNY', 'JPY/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'EUR/USD', 'EUR/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/ADU', 'USD/ADU', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/CAD', 'CNY/CAD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/CAD', 'USD/CAD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/CHF', 'CNY/CHF', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/UHF', 'USD/UHF', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/EUR', 'CNY/EUR', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/GBP', 'CNY/GBP', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/HKD', 'CNY/HKD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/JPY', 'CNY/JPY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/CNY', 'CNY/CNY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/USD', 'CNY/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/EUR', 'USD/EUR', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/GBP', 'USD/GBP', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/HKD', 'USD/HKD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/JPY', 'USD/JPY', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'USD/SGD', 'USD/SGD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'CNY/AUD', 'CNY/AUD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'GBP/USD', 'GBP/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'HKD/USD', 'HKD/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'JPY/USD', 'JPY/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'SGD/USD', 'SGD/USD', '1', null, 1);

insert into TA_DICT (DICT_ID, DICT_NAME, DICT_MODULE, DICT_DESC, DICT_PARENTID, DICT_KEY, DICT_VALUE, DICT_LEVEL, DICT_SORT, IS_ACTIVE)
values ('CurrencyPair', '货币对', '公共', null, '-', 'AUD/CNY', 'AUD/CNY', '1', null, 1);


