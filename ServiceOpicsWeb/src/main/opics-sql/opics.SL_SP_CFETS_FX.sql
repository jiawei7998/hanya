CREATE OR REPLACE 
procedure SL_SP_CFETS_FX(I_FEDEALNO   IN CHAR,--原交易单号
                         I_TRAD       IN CHAR,--交易员
                         I_VDATE     IN CHAR,--交易日
                         I_CUST       IN CHAR,--交易对手
                         I_DEALDATE   IN CHAR,--交易日期
                         I_DEALTIME   IN CHAR,--交易时间
                         I_PS         IN CHAR,--交易方向
                         I_CCY        IN CHAR,--货币1
                         I_CCYAMT     IN CHAR,--货币1金额
                         I_CTRCCY     IN CHAR,--货币2
                         I_CTRAMT     IN CHAR,--货币2金额
                         I_CCYRATE_8  IN CHAR,--成交价格
												 I_BROK		 		IN CHAR,
                         I_CCYPD_8    IN CHAR,--货币1升水/贴水
                         --I_SERVERNAME IN CHAR,
                         --I_NOTETEXT   IN CHAR,

                         --I_DEALTYPE   IN CHAR,--交易种类

                         I_BR         IN CHAR,--BR
                         I_PRODCODE   IN CHAR,--PRODCODE
                         I_PRODTYPE   IN CHAR,--PRODTYPE
                         I_COST       IN CHAR,--COST
                         I_PORT       IN CHAR,--PORT
                         I_SIIND      IN CHAR,--标志指令0/3
                         I_VERIND     IN CHAR,--复核标志0/1                
                         O_RETCODE    OUT CHAR,
                         O_RETMSG     OUT CHAR) is
  -- IFXD 外汇即期、远期
  VS_BR         CHAR(2);--BR
  VS_SERVER     CHAR(15);
  VS_FEDEALNO   CHAR(15);--原交易单号
  VS_SEQ        CHAR(4);
  VS_INOUTIND   CHAR(1);
  VS_TRAD       CHAR(4);
  VS_VDATE      CHAR(10);
  VS_NVDATE     CHAR(25);
  VS_FVDATE     CHAR(25);
  VS_CUST       CUSI.CNO%TYPE;
  VS_BROK       CHAR(11);
  VS_COST       CHAR(11);
  VS_PORT       CHAR(4);
  VS_DEALDATE   CHAR(10);
  VS_PS         CHAR(1);
  VS_CCY        CHAR(3);
  VS_CCYAMT     CHAR(20);
  --VS_CCYRATE_8  CHAR(20);
  --VS_CCYPD_8    CHAR(20);
  VS_CTRCCY     CHAR(3);
  VS_CTRAMT     CHAR(20);
  VS_SIIND      CHAR(1);
  VS_VERIND     CHAR(1);
  VS_SUPPAYIND  CHAR(1);
  VS_SUPRECIND  CHAR(1);
  VS_SUPCONFIND CHAR(1);
  VS_AUTHSI     CHAR(1);
  VS_PRODCODE   CHAR(6);
  VS_NPRODCODE  CHAR(6);
  VS_FPRODCODE  CHAR(6);
  VS_PRODTYPE   CHAR(2);
  VS_NPRODTYPE  CHAR(2);
  VS_FPRODTYPE  CHAR(2);
  VS_DEALTEXT   CHAR(50);
  VS_NOTETEXT   CHAR(100);
  VS_FCCYRATE_8 CHAR(20);
  VS_NCCYRATE_8 CHAR(20);
	VS_ZERORATEIND IFXD.ZERORATEIND%TYPE;
	VS_CCYRATE_8   IFXD.CCYRATE_8%TYPE;
	VS_CCYPD_8     IFXD.CCYPD_8%TYPE;
  -- INTH 
  VS_PRIORITY   CHAR(2);
  VS_TAG        CHAR(4);
  VS_DETAIL     CHAR(4);
  VS_STATCODE   CHAR(4);
  VS_SYST       CHAR(15);
  VS_IOPER      CHAR(4);
  VS_LSTMNTDATE DATE;
  VS_ERRORCODE  CHAR(4);
  VS_COUNT      INTEGER;
  VS_DEALNO     CHAR(15);
begin
  VS_BR     := I_BR;
  VS_PRODCODE    := I_PRODCODE;
  VS_PRODTYPE    := I_PRODTYPE;
  VS_PORT        := I_PORT;
  VS_COST        := I_COST;
  VS_SIIND       := I_SIIND;--标志指令0/3
  VS_VERIND      := I_VERIND;--复核标志0/1
  VS_CUST        := I_CUST;
  VS_SUPPAYIND   :='Y';
  VS_SUPRECIND   :='Y';
  VS_SUPCONFIND  :='Y';
  VS_AUTHSI      :='0';
  VS_CCYPD_8     :=I_CCYPD_8;
  VS_FEDEALNO := SUBSTR(TRIM(I_FEDEALNO),0,15);
  VS_SEQ      := '0';
  VS_INOUTIND := 'I';
  VS_VDATE    := I_VDATE;
  VS_DEALDATE := I_DEALDATE;
  VS_DEALTEXT := TRIM(I_FEDEALNO) || '|' || VS_SERVER;
	if I_PS='1' THEN
		VS_PS     := 'P';
	ELSIF I_PS='4' THEN
		VS_PS     := 'S';
	END IF;
  
  VS_CCY    := SUBSTR(I_CCY,0,3);
  VS_CCYAMT := I_CCYAMT;
  VS_CTRCCY := SUBSTR(I_CTRCCY,5,3);     
  VS_CTRAMT := I_CTRAMT;
  If VS_CCYPD_8 Is Not Null Then
  VS_CCYRATE_8 := I_CCYRATE_8+I_CCYPD_8;
  Else
  VS_CCYRATE_8 := I_CCYRATE_8;
  END IF;    
  VS_BROK :=I_BROK;
	VS_TRAD :=I_TRAD;   
  If VS_CCYPD_8 Is Not Null Then
  VS_SERVER   := 'SL_CFETS_FWD';
  Else
  VS_SERVER   := 'SL_CFETS_SPOT';
  END IF;    
/*  If To_Date(VS_VDATE, 'YYYY-MM-DD') -To_Date(VS_DEALDATE, 'YYYY-MM-DD') >=3 THEN
  VS_SERVER   := 'SL_CFETS_FWD';
  Else
  VS_SERVER   := 'SL_CFETS_SPOT';
  END IF;*/
  BEGIN
    INSERT INTO IFXD
      (BR,
				SERVER,
				FEDEALNO,
				SEQ,
				INOUTIND,
				TRAD,
				VDATE,
				CUST,
				BROK,
				PORT,
				COST,
				DEALDATE,
				DEALTEXT,
				PS,
				CCY,
				CCYAMT,
				CTRCCY,
				CTRAMT,
				PRODCODE,
				PRODTYPE,
				SIIND,
				VERIND,
				SUPPAYIND,
				SUPRECIND,
				SUPCONFIND,
				AUTHSI,
				NOTETEXT,
				CUSTREFNO,
				ZERORATEIND,
				CCYRATE_8,
				CCYPD_8)
VALUES
      (VS_BR,
       VS_SERVER,
       VS_FEDEALNO,
       VS_SEQ,
       VS_INOUTIND,
       VS_TRAD,
       TO_DATE(VS_VDATE, 'YYYY-MM-DD'),
       VS_CUST,
       VS_BROK,
       VS_PORT,
       VS_COST,
       TO_DATE(VS_DEALDATE, 'YYYY-MM-DD'),
       VS_DEALTEXT,
       VS_PS,
       VS_CCY,
       VS_CCYAMT,
       VS_CTRCCY,
       VS_CTRAMT,
       VS_PRODCODE,
       VS_PRODTYPE,
       VS_SIIND,
       VS_VERIND,
       VS_SUPPAYIND,
       VS_SUPRECIND,
       VS_SUPCONFIND,
       VS_AUTHSI,
       VS_NOTETEXT,
       VS_FEDEALNO,
			 VS_ZERORATEIND,
			 VS_CCYRATE_8,
			 VS_CCYPD_8);
  EXCEPTION
    WHEN OTHERS THEN
      O_RETCODE := '105';
      O_RETMSG  :='插入IFXD报错！'||substr(sqlerrm,1,200);
      RETURN;
  end;
  
  VS_PRIORITY := '1';
  VS_TAG      := 'FXDE';
  VS_DETAIL   := 'IFXD'; 
  VS_STATCODE := '-1';
  VS_SYST     := 'SING'; 
  VS_IOPER    := ''; 
  --alter session set nls_date_format = 'YYYYMMDD   HH24:MI:SS';
  VS_LSTMNTDATE := TO_DATE(TO_CHAR(SYSDATE, 'YYYY-MM-DD'), 'YYYY-MM-DD');
  BEGIN
    INSERT INTO INTH
      (BR,
       SERVER,
       FEDEALNO,
       SEQ,
       INOUTIND,
       PRIORITY,
       TAG,
       DETAIL,
       STATCODE,
       SYST,
       IOPER,
       LSTMNTDATE)
    VALUES
      (VS_BR,
       VS_SERVER,
       VS_FEDEALNO,
       VS_SEQ,
       VS_INOUTIND,
       VS_PRIORITY,
       VS_TAG,
       VS_DETAIL,
       VS_STATCODE,
       VS_SYST,
       '',
       VS_LSTMNTDATE);
  EXCEPTION
    WHEN OTHERS THEN
      O_RETCODE := '110';
      O_RETMSG  :='插入INTH报错！'||substr(sqlerrm,1,200);
      RETURN;
  end;
  O_RETCODE := '999';
  O_RETMSG  :='成功！';
end SL_SP_CFETS_FX;
