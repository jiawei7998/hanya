CREATE OR REPLACE 
PROCEDURE SL_SP_ISWHIRS(I_SN                   IN CHAR, --成交编号(17)
												 I_DLDT                  IN CHAR, --成交日期(38)
												 I_TRAD                  IN CHAR, --本方交易员
												 I_CNO                   IN CHAR, --对手方编号
												 I_PS                    IN CHAR, --交易方向(FIX: 本方是固定,FLT：本方是浮动)
												 I_NOTIONALAMT           IN CHAR, --名义本金金额
												 I_STARTDATE             IN CHAR, --起息日
												 I_ENDDATE               IN CHAR, --到期日
												 I_FIXEDRATE             IN CHAR, --FIX固定利率
												 I_FIXPAYMENTFREQUENCY   IN CHAR, --FIX付息频率
												 I_FIXBASIS              IN CHAR, -- FIX计息基准
												 I_FLTCURVENAME          IN CHAR, --FLT参考利率
												 I_FLTSPREAD             IN CHAR, --FLT利差
												 I_FLTPAYMENTFREQUENCY   IN CHAR, --FLT支付周期
												 I_FLTINTRESETFREQUENCY  IN CHAR, --FLT重置频率
												 I_FLTBASIS              IN CHAR, --FLT计息基准
												 
												 I_BR					IN CHAR,--BR
												 I_PRODCODE    IN CHAR,--PRODCODE
												 I_PRODTYPE   IN CHAR,--PRODTYPE
												 I_COST       IN CHAR,--COST
												 I_PORT       IN CHAR,--PORT
												 I_SIIND 			IN CHAR,--标志指令0/3
												 I_VERIND      IN CHAR,--复核标志0/1
                         O_RETCODE    OUT CHAR,
                         O_RETMSG     OUT CHAR
                         )IS
  VS_FEDEALNO           ISWH.FEDEALNO%TYPE; --流水号
  VS_INPUTDATE          DATE; --录入日期
  VS_NUM                INT;
  --ISWH IRS利率互换
  VS_BR                 ISWH.BR%TYPE; --网点
  VS_SERVER             ISWH.SERVER%TYPE; --服务名
  VS_SEQ                ISWH.SEQ%TYPE; --序列
  VS_INOUTIND           ISWH.INOUTIND%TYPE; --IN/OUT标志
  VS_PRODCODE           ISWH.PRODUCT%TYPE; --产品代码
  VS_PRODTYPE           ISWH.PRODTYPE%TYPE; --产品类型
  VS_DEALIND            ISWH.DEALIND%TYPE;--处理标志
  VS_BROK               ISWH.BROK%TYPE; --代理人编号
  VS_CNO                ISWH.CNO%TYPE; --客户号
  VS_COST               ISWH.COST%TYPE; --成本中心
  VS_DEALDATE           ISWH.DEALDATE%TYPE; --交易日期
  VS_DEALTEXT           ISWH.DEALTEXT%TYPE; --交易说明
  VS_DEALTYPE           ISWH.DEALTYPE%TYPE; --交易类型
  VS_TENOR              ISWH.TENOR%TYPE; --期限
  VS_PORT               ISWH.PORT%TYPE; --PORT
  VS_ADDSIIND           ISWH.ADDSIIND%TYPE; --添加指令标志
  VS_VERIND             ISWH.VERIND%TYPE; --复核标志
  VS_SUPPAYIND          ISWH.SUPPAYIND%TYPE; --SUP支付标志
  VS_SUPRECIND          ISWH.SUPRECIND%TYPE; --SUP接受标志
  VS_SUPCONFIND         ISWH.SUPCONFIND%TYPE; --废除指令标志
  VS_AUTHSIIND          ISWH.AUTHSIIND%TYPE; --授权标志
  VS_MATDATE            ISWH.MATDATE%TYPE; --到期日
  VS_PLMETHOD           ISWH.PLMETHOD%TYPE; --利润/亏损
  VS_RATECODE           ISWH.RATECODE%TYPE; --利率代码
  VS_SPREAD_8           ISWH.SPREAD_8%TYPE; --浮动利率
  VS_PAYRATECODE        ISWH.RATECODE%TYPE; --
  VS_PAYSPREAD_8        ISWH.SPREAD_8%TYPE; --
  VS_SETTLEDATE         ISWH.SETTLEDATE%TYPE; --清算日期
  VS_STARTDATE          ISWH.STARTDATE%TYPE; --开始日期
  VS_SWAPTYPE           ISWH.SWAPTYPE%TYPE; --SWAP类型（固定利率支付方/浮动利率收取方）
  VS_TRAD               ISWH.TRAD%TYPE; --交易员
  VS_PAYBASIS           ISWH.PAYBASIS%TYPE; --支付方计息基准
  VS_RECBASIS           ISWH.RECBASIS%TYPE; --收取放计息基准
  VS_PAYDATEDIRECTION   ISWH.PAYDATEDIRECTION%TYPE; --支付方日期方向
  VS_RECDATEDIRECTION   ISWH.RECDATEDIRECTION%TYPE; --收取方日期方向
  VS_PAYINTPAYCYCLE     ISWH.PAYINTPAYCYCLE%TYPE; --支付方利率支付周期
  VS_RECINTPAYCYCLE     ISWH.RECINTPAYCYCLE%TYPE; --收取方利率支付周期
  VS_PAYINTPAYPAYRULE   ISWH.PAYINTPAYPAYRULE%TYPE; --支付方利率支付规则
  VS_RECINTPAYPAYRULE   ISWH.RECINTPAYPAYRULE%TYPE; --收取方利率支付规则
  VS_PAYINTRATE_8       ISWH.PAYINTRATE_8%TYPE; --支付方利率
  VS_RECINTRATE_8       ISWH.RECINTRATE_8%TYPE; --收取方利率
  VS_PAYNOTCCY          ISWH.PAYNOTCCY%TYPE; --支付方名义币种
  VS_RECNOTCCY          ISWH.RECNOTCCY%TYPE; --收取方名义币种
  VS_PAYNOTCCYAMT       ISWH.PAYNOTCCYAMT%TYPE; --支付方名义金额
  VS_RECNOTCCYAMT       ISWH.RECNOTCCYAMT%TYPE; --收取方名义金额
  VS_PAYPRINADJAMT      ISWH.PAYPRINADJAMT%TYPE; --支付方名义本金重置金额
  VS_RECPRINADJAMT      ISWH.RECPRINADJAMT%TYPE; --收取方名义本金重置金额
  VS_PAYYIELDCURVE      ISWH.PAYYIELDCURVE%TYPE; --支付方利率曲线
  VS_RECYIELDCURVE      ISWH.RECYIELDCURVE%TYPE; --收取方利率曲线
  VS_RECRATECODE        ISWH.RECRATECODE%TYPE; --收取方利率代码
  VS_RECSPREAD_8        ISWH.RECSPREAD_8%TYPE; --收取方利差
  VS_RATEREVCYCLE       ISWH.RATEREVCYCLE%TYPE; --利率修正周期
  VS_RECRATEREVCYCLE    ISWH.RECRATEREVCYCLE%TYPE; --收取方利率修正周期
  VS_RATEREVFRSTLST     ISWH.RATEREVFRSTLST%TYPE; --首次/最后利率修正标志
  VS_CALCRULE           ISWH.CALCRULE%TYPE; --浮动利率计算规则
  VS_RECRATEREVFRSTLST  ISWH.RECRATEREVFRSTLST%TYPE; --收取方首次/最后利率修正标志
  VS_RECCALCRULE        ISWH.RECCALCRULE%TYPE; --收取方浮动利率计算规则
  VS_INVTYPE            ISWH.INVTYPE%TYPE; --投资类型
  VS_PAYFIRSTIPAYDATE   ISWH.PAYFIRSTIPAYDATE%TYPE; --支付方首期支付日
  VS_RECFIRSTIPAYDATE   ISWH.RECFIRSTIPAYDATE%TYPE; --收取方首期支付日
  VS_NETPAYIND          ISWH.NETPAYIND%TYPE; --轧差支付标志
  VS_STRATID            ISWH.STRATID%TYPE; --策略号
  VS_PAYPOSTNOTIONAL    ISWH.PAYPOSTNOTIONAL%TYPE; --支付方国有标志
  VS_RECPOSTNOTIONAL    ISWH.RECPOSTNOTIONAL%TYPE; --收取方国有标志
  VS_PAYCOMPOUND ISWH.COMPOUND%TYPE;--付款复利标识
  VS_RECCOMPOUND ISWH.RECCOMPOUND%TYPE;--收方复利标识
  VS_PAYSCHDCONV ISWH.PAYSCHDCONV%TYPE;--付方计息方式
  VS_RECSCHDCONV ISWH.RECSCHDCONV%TYPE;--收方计息方式
  --INTH FIELDS
  VS_PRIORITY   INTH.PRIORITY%TYPE;--优先级
  VS_TAG        INTH.TAG%TYPE;--业务名称
  VS_DETAIL     INTH.DETAIL%TYPE;--接口详细
  VS_STATCODE   INTH.STATCODE%TYPE;--状态码
  VS_SYST       INTH.SYST%TYPE;--系统描述
  VS_IOPER      INTH.IOPER%TYPE;--录入人
  VS_LSTMNTDATE INTH.LSTMNTDATE%TYPE;--最后更新日期

BEGIN
  VS_INPUTDATE := SYSDATE;
  VS_NUM := 0;
  VS_FEDEALNO := 'F' || SUBSTR(TRIM(I_SN),-14,14);
  /*********************INSERT接口表END**************************************/
  /*********************发送数据到OPICS的处理*******************************/
  VS_SERVER      := 'SL_CFETS_IRS';
  VS_INOUTIND    := 'I';
  VS_SEQ         := '0';
  VS_TENOR       := '99';
  VS_BROK        := 'X';
  VS_DEALIND     := 'S';
  VS_ADDSIIND    := '1';
  VS_INVTYPE     := '';
  VS_VERIND      := '1';
  VS_AUTHSIIND   := '1';
  VS_SUPPAYIND   := 'Y';
  VS_SUPRECIND   := 'Y';
  VS_SUPCONFIND  := 'Y';
  VS_DEALTEXT    := TRIM(VS_SERVER)||'|'||TRIM(I_SN)||'|CCP';
  VS_DEALDATE    := TO_DATE(I_DLDT, 'YYYY-MM-DD');
  VS_DEALTYPE    := 'I';
  VS_MATDATE     := TO_DATE(I_ENDDATE, 'YYYY-MM-DD');
  VS_PLMETHOD    := 'T';
  VS_SETTLEDATE  := '';--SYSDATE
  VS_STARTDATE   := TO_DATE(I_STARTDATE, 'YYYY-MM-DD');
  VS_NETPAYIND   := 'Y';
  VS_STRATID     := '';
  VS_PAYPOSTNOTIONAL  := 'Y';
  VS_RECPOSTNOTIONAL  := 'Y';

  --IF I_PS='PAYFIX' THEN
  IF SUBSTR(I_PS,0,6)='PAYFIX' THEN
     BEGIN
        VS_SWAPTYPE     :='P'; --P:for Pay Fixed/Rec Floating,
        VS_PAYRATECODE  := 'FIXCNY';
        VS_PAYSPREAD_8  := NULL;
        VS_PAYBASIS      :=CASE WHEN I_FIXBASIS='0' THEN 'ACTUAL' --0-Actual divide Actual
                           WHEN I_FIXBASIS='1' THEN 'A360'  --1-ACT DIVIDE 360
                           WHEN I_FIXBASIS='2' THEN 'BOND'   --2-Thirty divide 360
                           WHEN I_FIXBASIS='3' THEN 'A365'   --3-A DIVIDE 365
                           ELSE I_FIXBASIS END;
        VS_PAYDATEDIRECTION     :='VF';
        VS_PAYINTPAYCYCLE :=CASE WHEN I_FIXPAYMENTFREQUENCY='0' THEN 'S'--0 - (half year) S Semi-annually,
                            WHEN I_FIXPAYMENTFREQUENCY='1' THEN 'A'--1 - (year)   A Annually,
                            WHEN I_FIXPAYMENTFREQUENCY='2' THEN 'B'--2 - (maturity) B Bullet, at maturity only
                            WHEN I_FIXPAYMENTFREQUENCY='3' THEN 'M'--3 - (monthly)   M Monthly,
                            WHEN I_FIXPAYMENTFREQUENCY='4' THEN 'Q'--4 - (quarterly)  Q Quarterly
                            WHEN I_FIXPAYMENTFREQUENCY='5' THEN '14'--5 - (tow week)
                            WHEN I_FIXPAYMENTFREQUENCY='6' THEN 'W'--6 - (week)   W Weekly,
                            WHEN I_FIXPAYMENTFREQUENCY='7' THEN 'D'--7 - (daily) D Daily
                            ELSE I_FIXPAYMENTFREQUENCY END;
        VS_PAYINTPAYPAYRULE  :='M';
        VS_PAYINTRATE_8      :=I_FIXEDRATE;
        VS_PAYNOTCCY         :='CNY';
        VS_PAYNOTCCYAMT      :=-I_NOTIONALAMT;
        VS_PAYPRINADJAMT     :=0;
        VS_PAYYIELDCURVE     :='SWAP' ;
        VS_PAYFIRSTIPAYDATE  :='';--TO_DATE(I_FIXFIRSTPAYDATE, 'YYYY-MM-DD');
        VS_RATEREVCYCLE      :=NULL;
        IF  UPPER(I_FLTCURVENAME) =UPPER('Shibor_3M') THEN
            VS_RECRATECODE:='3MDPO';
        ELSIF UPPER(I_FLTCURVENAME) =UPPER('shibor_1w')OR UPPER(I_FLTCURVENAME) =UPPER('FR007') THEN
            VS_RECRATECODE:='FR007';
        ELSE
            VS_RECRATECODE:='1YDPO';
        END IF;
        VS_RECSPREAD_8       :=I_FLTSPREAD;
            VS_RECBASIS     :=CASE WHEN I_FLTBASIS='0' THEN 'ACTUAL' --0-Actual divide Actual
                            WHEN I_FLTBASIS='1' THEN 'A360' --1-ACT DIVIDE 360
                            WHEN I_FLTBASIS='2' THEN 'BOND' --2-Thirty divide 360
                            WHEN I_FLTBASIS='3' THEN 'A365' --3-A DIVIDE 365
                            ELSE I_FLTBASIS END;
        VS_RECDATEDIRECTION   :='VF';
        VS_RECINTPAYCYCLE :=CASE  WHEN I_FLTPAYMENTFREQUENCY='0' THEN 'S'--0 - (half year) S Semi-annually,
                            WHEN I_FLTPAYMENTFREQUENCY='1' THEN 'A'--1 - (year)   A Annually,
                            WHEN I_FLTPAYMENTFREQUENCY='2' THEN 'B'--2 - (maturity) B Bullet, at maturity only
                            WHEN I_FLTPAYMENTFREQUENCY='3' THEN 'M'--3 - (monthly)   M Monthly,
                            WHEN I_FLTPAYMENTFREQUENCY='4' THEN 'Q'--4 - (quarterly)  Q Quarterly
                            WHEN I_FLTPAYMENTFREQUENCY='5' THEN '14'--5 - (tow week)
                            WHEN I_FLTPAYMENTFREQUENCY='6' THEN 'W'--6 - (week)   W Weekly,
                            WHEN I_FLTPAYMENTFREQUENCY='7' THEN 'D'--7 - (daily) D Daily
                            ELSE I_FLTPAYMENTFREQUENCY END;

        VS_RECINTPAYPAYRULE  :='M';
        VS_RECINTRATE_8      :=NULL;
        VS_RECNOTCCY         :='CNY';
        VS_RECNOTCCYAMT      :=I_NOTIONALAMT;
        VS_RECPRINADJAMT     :=0;
        VS_RECYIELDCURVE     := 'SWAP' ;
        VS_RECFIRSTIPAYDATE  :='';--TO_DATE(I_FLTFIRSTPAYDATE, 'YYYY-MM-DD');

        VS_RECRATEREVCYCLE :=CASE WHEN I_FLTINTRESETFREQUENCY='0' THEN 'S'--0 - (half year) S Semi-annually,
                            WHEN I_FLTINTRESETFREQUENCY='1' THEN 'A'--1 - (year)   A Annually,
                            WHEN I_FLTINTRESETFREQUENCY='2' THEN 'B'--2 - (maturity) B Bullet, at maturity only
                            WHEN I_FLTINTRESETFREQUENCY='3' THEN 'M'--3 - (monthly)   M Monthly,
                            WHEN I_FLTINTRESETFREQUENCY='4' THEN 'Q'--4 - (quarterly)  Q Quarterly
                            WHEN I_FLTINTRESETFREQUENCY='5' THEN '14'--5 - (tow week)
                            WHEN I_FLTINTRESETFREQUENCY='6' THEN 'W'--6 - (week)   W Weekly,
                            WHEN I_FLTINTRESETFREQUENCY='7' THEN 'D'--7 - (daily) D Daily
                            ELSE I_FLTINTRESETFREQUENCY END;
     END;

  ELSE
     BEGIN
        VS_SWAPTYPE     :='R'; --R:for Rec Fixed/Pay Floating
        IF  UPPER(I_FLTCURVENAME) =UPPER('Shibor_3M') THEN
            VS_PAYRATECODE:='3MDPO';
        ELSIF UPPER(I_FLTCURVENAME) =UPPER('shibor_1w')OR UPPER(I_FLTCURVENAME) =UPPER('FR007') THEN
            VS_PAYRATECODE:='FR007';
        ELSE
            VS_PAYRATECODE:='1YDPO';
        END IF;
        VS_PAYSPREAD_8  := I_FLTSPREAD;
        VS_PAYBASIS :=CASE WHEN I_FLTBASIS='0' THEN 'ACTUAL' --0-Actual divide Actual
                           WHEN I_FLTBASIS='1' THEN 'A360'   --1-ACT DIVIDE 360
                           WHEN I_FLTBASIS='2' THEN 'BOND'   --2-Thirty divide 360
                           WHEN I_FLTBASIS='3' THEN 'A365'   --3-A DIVIDE 365
                           ELSE I_FLTBASIS END;
        VS_PAYDATEDIRECTION     :='VF';
        VS_PAYINTPAYCYCLE :=CASE  WHEN I_FLTPAYMENTFREQUENCY='0' THEN 'S'--0 - (half year) S Semi-annually,
                            WHEN I_FLTPAYMENTFREQUENCY='1' THEN 'A'--1 - (year)   A Annually,
                            WHEN I_FLTPAYMENTFREQUENCY='2' THEN 'B'--2 - (maturity) B Bullet, at maturity only
                            WHEN I_FLTPAYMENTFREQUENCY='3' THEN 'M'--3 - (monthly)   M Monthly,
                            WHEN I_FLTPAYMENTFREQUENCY='4' THEN 'Q'--4 - (quarterly)  Q Quarterly
                            WHEN I_FLTPAYMENTFREQUENCY='5' THEN '14'--5 - (tow week)
                            WHEN I_FLTPAYMENTFREQUENCY='6' THEN 'W'--6 - (week)   W Weekly,
                            WHEN I_FLTPAYMENTFREQUENCY='7' THEN 'D'--7 - (daily) D Daily
                            ELSE I_FLTPAYMENTFREQUENCY END;
        VS_PAYINTPAYPAYRULE  :='M';
        VS_PAYINTRATE_8      :=NULL;
        VS_PAYNOTCCY         :='CNY';
        VS_PAYNOTCCYAMT      :=-I_NOTIONALAMT;
        VS_PAYPRINADJAMT     :=0;
        VS_PAYYIELDCURVE     := 'SWAP' ;
        VS_PAYFIRSTIPAYDATE  :='';-- TO_DATE(I_FLTFIRSTPAYDATE, 'YYYY-MM-DD');
        VS_RATEREVCYCLE   :=CASE  WHEN I_FLTINTRESETFREQUENCY='0' THEN 'S'--0 - (half year) S Semi-annually,
                            WHEN I_FLTINTRESETFREQUENCY='1' THEN 'A'--1 - (year)   A Annually,
                            WHEN I_FLTINTRESETFREQUENCY='2' THEN 'B'--2 - (maturity) B Bullet, at maturity only
                            WHEN I_FLTINTRESETFREQUENCY='3' THEN 'M'--3 - (monthly)   M Monthly,
                            WHEN I_FLTINTRESETFREQUENCY='4' THEN 'Q'--4 - (quarterly)  Q Quarterly
                            WHEN I_FLTINTRESETFREQUENCY='5' THEN '14'--5 - (tow week)
                            WHEN I_FLTINTRESETFREQUENCY='6' THEN 'W'--6 - (week)   W Weekly,
                            WHEN I_FLTINTRESETFREQUENCY='7' THEN 'D'--7 - (daily) D Daily
                          ELSE I_FLTINTRESETFREQUENCY END;
         VS_RECRATECODE       := 'FIXCNY';
         VS_RECSPREAD_8       := NULL;
         VS_RECBASIS      :=CASE  WHEN I_FIXBASIS='0' THEN 'ACTUAL' --0-Actual divide Actual
                            WHEN I_FIXBASIS='1' THEN 'A360' --1-ACT DIVIDE 360
                            WHEN I_FIXBASIS='2' THEN 'BOND' --2-Thirty divide 360
                            WHEN I_FIXBASIS='3' THEN 'A365' --3-A DIVIDE 365
                            ELSE I_FIXBASIS END;
         VS_RECDATEDIRECTION     :='VF';
         VS_RECINTPAYCYCLE   :=CASE  WHEN I_FIXPAYMENTFREQUENCY='0' THEN 'S'--0 - (half year) S Semi-annually,
                            WHEN I_FIXPAYMENTFREQUENCY='1' THEN 'A'--1 - (year)   A Annually,
                            WHEN I_FIXPAYMENTFREQUENCY='2' THEN 'B'--2 - (maturity) B Bullet, at maturity only
                            WHEN I_FIXPAYMENTFREQUENCY='3' THEN 'M'--3 - (monthly)   M Monthly,
                            WHEN I_FIXPAYMENTFREQUENCY='4' THEN 'Q'--4 - (quarterly)  Q Quarterly
                            WHEN I_FIXPAYMENTFREQUENCY='5' THEN '14'--5 - (tow week)
                            WHEN I_FIXPAYMENTFREQUENCY='6' THEN 'W'--6 - (week)   W Weekly,
                            WHEN I_FIXPAYMENTFREQUENCY='7' THEN 'D'--7 - (daily) D Daily
                            ELSE I_FIXPAYMENTFREQUENCY END;
        VS_RECINTPAYPAYRULE  :='M';
        VS_RECINTRATE_8      :=I_FIXEDRATE;
        VS_RECNOTCCY         :='CNY';
        VS_RECNOTCCYAMT      :=I_NOTIONALAMT;
        VS_RECPRINADJAMT     :=0;
        VS_RECYIELDCURVE     := 'SWAP' ;
        VS_RECFIRSTIPAYDATE  :=''; --TO_DATE(I_FIXFIRSTPAYDATE, 'YYYY-MM-DD');
        VS_RECRATEREVCYCLE   :=NULL;
     END;
  END IF;

  VS_RATECODE   :=VS_PAYRATECODE;
  VS_SPREAD_8   :=VS_PAYSPREAD_8;

  BEGIN
    --将拆借中心的利率类型转换为OPICS的
    IF  UPPER(I_FLTCURVENAME) =UPPER('Shibor_3M') THEN
        VS_PAYYIELDCURVE :='SW3MREPO';
        VS_RECYIELDCURVE :='SW3MREPO';
    ELSIF I_FLTCURVENAME='一年定存利率' OR I_FLTCURVENAME='贷款利率_1年' THEN
        VS_PAYYIELDCURVE :='SW1YREPO';
        VS_RECYIELDCURVE :='SW1YREPO';
    ELSIF UPPER(I_FLTCURVENAME) =UPPER('shibor_1w')OR UPPER(I_FLTCURVENAME) =UPPER('FR007') THEN
        VS_PAYYIELDCURVE :='SW7DREPO';
        VS_RECYIELDCURVE :='SW7DREPO';
    ELSE
      VS_PAYYIELDCURVE :='SWAP';
      VS_RECYIELDCURVE :='SWAP';
    END IF;
  END;

   --ADD 2015-11-03 BY CHENXH,设置付款复利及计息方式
   IF(VS_PAYRATECODE ='FR007')THEN
        VS_PAYCOMPOUND := 'F';
        VS_PAYSCHDCONV := 'CHINA';
   END IF;

   --ADD 2015-11-03 BY CHENXH,设置收款复利及计息方式
   IF(VS_RECRATECODE ='FR007')THEN
        VS_RECCOMPOUND := 'F';
        VS_RECSCHDCONV := 'CHINA';
   END IF;
  VS_BR := I_BR;
  VS_TRAD := I_TRAD;
  VS_COST := I_COST;
  VS_PORT := I_PORT;
  VS_CNO := I_CNO;
  VS_PRODCODE := I_PRODCODE;
  VS_PRODTYPE := I_PRODTYPE;
  VS_BROK := 'X';
  -- INSERT INTO ISWH
  BEGIN
    INSERT INTO ISWH
        (BR,
        SERVER,
        FEDEALNO,
        SEQ,
        INOUTIND,
        DEALIND,
        PRODUCT,
        PRODTYPE,
        BROK,CNO,
        COST,
        DEALTEXT,
        PORT,
        MATDATE,
        PLMETHOD,
        SETTLEDATE,
        STARTDATE,
        SWAPTYPE,
        RATECODE,
        SPREAD_8,
        DEALDATE,
        DEALTYPE,
        TENOR,
        TRAD,
        PAYBASIS,
        RECBASIS,
        PAYDATEDIRECTION,
        RECDATEDIRECTION,
        PAYINTPAYCYCLE,
        RECINTPAYCYCLE,
        PAYINTPAYPAYRULE,
        RECINTPAYPAYRULE,
        PAYINTRATE_8,
        RECINTRATE_8,
        PAYNOTCCY,
        RECNOTCCY,
        PAYNOTCCYAMT,
        RECNOTCCYAMT,
        PAYPRINADJAMT,
        RECPRINADJAMT,
        PAYYIELDCURVE,
        RECYIELDCURVE,
        RECRATECODE,
        RECSPREAD_8,
        ADDSIIND,
        VERIND,
        SUPPAYIND,
        SUPRECIND,
        AUTHSIIND,
        SUPCONFIND,
        NETPAYIND,
        STRATID,
        PAYPOSTNOTIONAL,
        RECPOSTNOTIONAL,
        INVTYPE,
        PAYFIRSTIPAYDATE,
        RECFIRSTIPAYDATE,
        RATEREVCYCLE,
        RECRATEREVCYCLE,
        RATEREVFRSTLST,
        RECRATEREVFRSTLST,
        CALCRULE,
        RECCALCRULE,
        COMPOUND,
        PAYSCHDCONV,
        RECCOMPOUND,
        RECSCHDCONV
        )
        VALUES
        (VS_BR,
        VS_SERVER,
        VS_FEDEALNO,
        VS_SEQ,
        VS_INOUTIND,
        VS_DEALIND,
        VS_PRODCODE,
        VS_PRODTYPE,
        VS_BROK,
        VS_CNO,
        VS_COST,
        VS_DEALTEXT,
        VS_PORT,
        VS_MATDATE,
        VS_PLMETHOD,
        VS_SETTLEDATE,
        VS_STARTDATE,
        VS_SWAPTYPE,
        VS_RATECODE,
        VS_SPREAD_8,
        VS_DEALDATE,
        VS_DEALTYPE,
        VS_TENOR,
        VS_TRAD,
        VS_PAYBASIS,
        VS_RECBASIS,
        VS_PAYDATEDIRECTION,
        VS_RECDATEDIRECTION,
        VS_PAYINTPAYCYCLE,
        VS_RECINTPAYCYCLE,
        VS_PAYINTPAYPAYRULE,
        VS_RECINTPAYPAYRULE,
        VS_PAYINTRATE_8,
        VS_RECINTRATE_8,
        VS_PAYNOTCCY,
        VS_RECNOTCCY,
        VS_PAYNOTCCYAMT,
        VS_RECNOTCCYAMT,
        VS_PAYPRINADJAMT,
        VS_RECPRINADJAMT,
        VS_PAYYIELDCURVE,
        VS_RECYIELDCURVE,
        VS_RECRATECODE,
        VS_RECSPREAD_8,
        VS_ADDSIIND,
        VS_VERIND,
        VS_SUPPAYIND,
        VS_SUPRECIND,
        VS_AUTHSIIND,
        VS_SUPCONFIND,
        VS_NETPAYIND,
        VS_STRATID,
        VS_PAYPOSTNOTIONAL,
        VS_RECPOSTNOTIONAL,
        VS_INVTYPE,
        VS_PAYFIRSTIPAYDATE,
        VS_RECFIRSTIPAYDATE,
        VS_RATEREVCYCLE,
        VS_RECRATEREVCYCLE,
        VS_RATEREVFRSTLST,
        VS_RECRATEREVFRSTLST,
        VS_CALCRULE,
        VS_RECCALCRULE,
        VS_PAYCOMPOUND,
        VS_PAYSCHDCONV,
        VS_RECCOMPOUND,
        VS_RECSCHDCONV);
    EXCEPTION
      WHEN OTHERS THEN
        O_RETCODE := '202';
        O_RETMSG := 'INSERT ISWH ERROR!'||SQLERRM;
        ROLLBACK;
        RETURN;
 END;
   VS_PRIORITY := '7';
   VS_TAG      := 'SWAP';
   VS_DETAIL   := 'ISWH';
   VS_STATCODE := '-1';
   VS_SYST     := '';
   VS_IOPER    := '';
   VS_LSTMNTDATE := SYSDATE;
  -- INSERT INTO INTH
  BEGIN
   INSERT INTO INTH
      (BR,
      SERVER,
      FEDEALNO,
      SEQ,
      INOUTIND,
      PRIORITY,
      TAG,
      DETAIL,
      STATCODE,
      SYST,
      IOPER,
      LSTMNTDATE)
      VALUES
      (VS_BR,
      VS_SERVER,
      VS_FEDEALNO,
      VS_SEQ,
      VS_INOUTIND,
      VS_PRIORITY,
      VS_TAG,
      VS_DETAIL,
      VS_STATCODE,
      VS_SYST,
      VS_IOPER,
      VS_LSTMNTDATE);
  EXCEPTION
    WHEN OTHERS THEN
      O_RETCODE := '203';
      O_RETMSG := 'INSERT INTH ERROR!';
      ROLLBACK;
      RETURN;
  END;
 /*********************处理OPICS表END*******************************/
 COMMIT;
 O_RETCODE := '999';
 O_RETMSG := 'DEAL SUCCESS!';
END SL_SP_ISWHIRS;
