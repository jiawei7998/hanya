package com.singlee.opics.bsys;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.bean.SlOutBean;
import com.singlee.financial.opics.IBsysAutoServer;

import java.net.MalformedURLException;

public class BsysAutoServerTest {

	public static void main(String[] args) {
		IBsysAutoServer autoServer = null;
		try {
			autoServer = (IBsysAutoServer) new HessianProxyFactory().create(IBsysAutoServer.class,
					"http://127.0.0.1:8080/fund-opics/api/service/bsysAutoExporter");
			SlOutBean out = autoServer.callAutoBsys("01", "2018-11-29", "102");
			System.out.println(String.format("AN%s030288", "IBO001"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
