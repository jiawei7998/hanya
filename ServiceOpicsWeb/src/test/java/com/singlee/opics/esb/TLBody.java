package com.singlee.opics.esb;

import java.util.List;

import com.singlee.esb.client.packet.EsbBodyPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

public class TLBody extends EsbBodyPacket {
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -5933334919162982489L;
	
	private String sn;
	
	private List<TLBodyItem> tLBodyItem;

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public List<TLBodyItem> gettLBodyItem() {
		return tLBodyItem;
	}

	public void settLBodyItem(List<TLBodyItem> tLBodyItem) {
		this.tLBodyItem = tLBodyItem;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		
		info.setSubPacketAliasName("BODY");
		info.setSubPacketClassType(TLBody.class);
		info.addAlias(TLBody.class, "tLBodyItem", "BodyList", TLBodyItem.class);
		return info;
	}

}
