package com.singlee.opics.esb;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;


public class SocketServerTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SocketServerTest socketServer = new SocketServerTest();
		socketServer.doListen();
	}

	public void doListen(){
		BufferedInputStream in = null;
		BufferedOutputStream output = null;
		String msg = null;
		Socket client = null;
		ServerSocket server = null;
		String headerMsg = null;
		try {
			server = new ServerSocket(8888);
			System.out.println("启动SOCKET服务端");
			while(true){
				try {
					System.out.println("等侍客户端请求");
					client = server.accept();
					client.setSoTimeout(120*10000);
					System.out.println("开始接收客户端请求报文");
					in = new BufferedInputStream(client.getInputStream());
					output = new BufferedOutputStream(client.getOutputStream());
					
					byte[] tmpbuff = null;
					
					// 创建报文头字节数组
					byte[] header = new byte[8];
					// 获取报文头信息
					in.read(header);
					
					headerMsg = new String(header);
					System.out.println("客户端请求报文头:"+headerMsg);
					
					//获取报文长度
					int bodylength = Integer.parseInt(headerMsg.trim());
					
					if(bodylength == 0){
						throw new RuntimeException("socket receive header data is null");
					}
					
					// 分配存储报文内容字节数组
					byte[] recvbuff = new byte[bodylength];
					
					//根据报文长度设置缓冲数组大小
					if(bodylength < 8192){
						//-次读取所有字节数
						tmpbuff = new byte[bodylength];
					}else{
						//每次读取的字节数
						tmpbuff = new byte[8192];
					}
					
					//循环读取服务端返回的报文信息并保存到recvbuff数组中
					int count = 0;
					int desPos = 0;
					while (-1 != (count = in.read(tmpbuff))) {
						System.arraycopy(tmpbuff, 0, recvbuff, desPos, count);
						desPos = desPos + count;
						if (desPos >= bodylength) {
							break;
						}//end if
					}//end while
					
					System.out.println("接收客户端报文结束");
					
					System.out.println("客户端请求报文:"+new String(recvbuff,"UTF-8"));
					
					byte [] ret = getRetMsg();
					
					String retMsg = new String(ret,"UTF-8");
					
					System.out.println("服务端返回报文体:"+retMsg);
					
					headerMsg = String.format("%08d", ret.length);
					
					System.out.println("服务端返回报文头:"+headerMsg);
					
					msg = headerMsg+retMsg;
					
					System.out.println("服务端返回报文体:"+msg);
					
					//写入请求报文到服务端
					output.write(msg.getBytes("UTF-8"));
					output.flush();
					
					System.out.println("发送返回报文到客户端");
				} catch (IOException e) {
					e.printStackTrace();
				}finally{
					if(null != output){
						try {
							output.close();
						} catch (IOException e) {
						}
					}//end if
					if(null != in){
						try {
							in.close();
						} catch (IOException e) {
						}
					}//end if
					if(null != client){
						try {
							client.shutdownOutput();
						} catch (IOException e1) {
						}
						try {
							client.shutdownInput();
						} catch (IOException e1) {
						}
						try {
							client.close();
						} catch (IOException e) {
						}
					}//end if
				}//end try catch
			}//end while
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(null != server){
				try {
					server.close();
				} catch (IOException e) {
				}
			}//end if
		}//end try catch
		
	}
	
	public byte [] getRetMsg() throws UnsupportedEncodingException{
		System.out.println("服务端开始组装返回报文");
		String warpLine = "\r\n";
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+warpLine);
		sb.append("<service>"+warpLine);
		sb.append("  <SYS_HEADER>"+warpLine);
		sb.append("    <ServiceCode>080808808</ServiceCode>"+warpLine);
		sb.append("    <array>"+warpLine);
		sb.append("       <Ret>"+warpLine);
		sb.append("          <ReturnCode>000000</ReturnCode>"+warpLine);
		sb.append("          <ReturnMsg>成功</ReturnMsg>"+warpLine);
		sb.append("       </Ret>"+warpLine);
		sb.append("    </array>"+warpLine);
		sb.append("  </SYS_HEADER>"+warpLine);
		sb.append("  <APP_HEADER>"+warpLine);
		sb.append("    <AppCode>87878787</AppCode>"+warpLine);
		sb.append("    <array>"+warpLine);
		sb.append("        <ApprTellerArray>"+warpLine);
		sb.append("           <ApprTellerNo>1</ApprTellerNo>"+warpLine);
		sb.append("        </ApprTellerArray>"+warpLine);
		sb.append("    </array>"+warpLine);
		sb.append("    <AuthFlag>OK</AuthFlag>"+warpLine);
		sb.append("    <array>"+warpLine);
		sb.append("        <AuthTellerArray>"+warpLine);
		sb.append("           <branchCode>9998</branchCode>"+warpLine);
		sb.append("        </AuthTellerArray>"+warpLine);
		sb.append("    </array>"+warpLine);
		sb.append("    <dealDate>2018-04-28</dealDate>"+warpLine);
		sb.append("  </APP_HEADER>"+warpLine);
		sb.append("  <BODY>"+warpLine);
		sb.append("     <cmitem>01</cmitem>"+warpLine);
		sb.append("     <cmsqno>11</cmsqno>"+warpLine);
		sb.append("     <ARRAY>"+warpLine);
		sb.append("        <BodyList>"+warpLine);
		sb.append("           <id>1</id>"+warpLine);
		sb.append("           <name>中文1</name>"+warpLine);
		sb.append("        </BodyList>"+warpLine);
		sb.append("        <BodyList>"+warpLine);
		sb.append("           <id>2</id>"+warpLine);
		sb.append("           <name>测试2</name>"+warpLine);
		sb.append("        </BodyList>"+warpLine);
		sb.append("     </ARRAY>"+warpLine);
		sb.append(" </BODY>"+warpLine);
		sb.append("</service>");
		
		return sb.toString().getBytes("UTF-8");
	}
}
