package com.singlee.opics.esb;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.esb.utils.TcpConfig;
//import com.singlee.financial.esb.hsbank.bean.AppHeadDto;
//import com.singlee.financial.esb.hsbank.bean.BodyDto0359988;
//import com.singlee.financial.esb.hsbank.bean.BodyDto0359989;
//import com.singlee.financial.esb.hsbank.bean.BodyDto0359990;
//import com.singlee.financial.esb.hsbank.bean.BodyListDto0359988;
//import com.singlee.financial.esb.hsbank.bean.LocalHeadDto;
//import com.singlee.financial.esb.hsbank.bean.SysHeadDto;
//import com.singlee.financial.esb.tlcb.bean.AppHead;
//import com.singlee.financial.esb.tlcb.bean.LocalHead;
//import com.singlee.financial.esb.tlcb.bean.SysHead;
//import com.singlee.financial.esb.tlcb.bean.SysHeadRet;
import com.singlee.xstream.utils.XmlUtils;


public class EsbTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			System.out.println("系统时间日期:"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
			System.out.println("系统时间;"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()).substring(8, 14));
			//对象转XML然后再XML转对象测试
//			fromXml(toXml());
			
//			发送对象到ESB测试
//			sendToEsb();
//			EsbCnapsReviceImpl0359990();
//			EsbCnapsReviceImpl0359988();
//			EsbCnapsReviceImpl0359989();
//			toXml();
			//组建ESB请求报文对象
			//输入
//	        EsbPacket<BodyDto> requestDto = test();
	        //输出
//	        EsbPacket<BodyDto> dto = EsbPacket.initEsbPacket(BodyDto.class,SysHeaderDto.class,AppHeaderDto.class);
			
//			EsbClient.send("D:\\workspace\\ServiceOpicsWeb\\src\\main\\resources\\mqapi.xml", "client", requestDto, dto, "UTF-8");
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static  EsbPacket<BodyDto> test() throws InstantiationException, IllegalAccessException{
		
		//组建ESB请求报文对象
				EsbPacket<BodyDto> requestDto = EsbPacket.initEsbPacket(BodyDto.class,SysHeaderDto.class,AppHeaderDto.class,LocalHeaderDto.class,"service");
				BodyDto body = requestDto.getBody();
				body.setCmitem("01");
				body.setCmsqno("11");
				
				List<BodyListDto> bodyList = new ArrayList<BodyListDto>();
				
				BodyListDto dodyListDto = new BodyListDto();
				dodyListDto.setId("1");
				dodyListDto.setName("中文1");
				bodyList.add(dodyListDto);
				
				dodyListDto = new BodyListDto();
				dodyListDto.setId("2");
				dodyListDto.setName("测试2");
				bodyList.add(dodyListDto);
				
				body.setARRAY(bodyList);
				
				SysHeaderDto sysHeader = requestDto.getSysHeader(SysHeaderDto.class);
				System.out.println("sysHeader:"+sysHeader);
				sysHeader.setServiceCode("080808808");
				sysHeader.setServiceScene("61");
				
				List<SysHeaderRetDto> sysHeaderRetDtos = new ArrayList<SysHeaderRetDto>();
				SysHeaderRetDto sysHeaderRetDto = new SysHeaderRetDto();
				sysHeaderRetDto.setReturnCode("000000");
				sysHeaderRetDto.setReturnMsg("成功");
				
				sysHeaderRetDtos.add(sysHeaderRetDto);
				
				sysHeader.setArray(sysHeaderRetDtos);
				
				AppHeaderDto appHeader = requestDto.getAppHeader(AppHeaderDto.class);
				appHeader.setAppCode("87878787");
				
				List<ApprTellerDto> apprTellerDtos = new ArrayList<ApprTellerDto>();
				ApprTellerDto apprTellerDto = new ApprTellerDto();
				apprTellerDto.setApprTellerNo("1");
				ApprTellerDto apprTellerDto1 = new ApprTellerDto();
				apprTellerDto1.setApprTellerNo("2");
				apprTellerDtos.add(apprTellerDto);
				apprTellerDtos.add(apprTellerDto1);
				appHeader.setApprTellers(apprTellerDtos);
				
				List<AuthTellerDto> authTellerDtos = new ArrayList<AuthTellerDto>();
				AuthTellerDto authTellerDto = new AuthTellerDto();
				authTellerDto.setBranchCode("9998");
				authTellerDtos.add(authTellerDto);
				appHeader.setAuthTellers(authTellerDtos);
				
				appHeader.setAuthFlag("OK");
				
				LocalHeaderDto localHeader = requestDto.getLocalHeader(LocalHeaderDto.class);
				localHeader.setBranchId("01");
				
				System.out.println("RequestDto:"+requestDto);
				
				return requestDto;
	}
	
	public static void sendToEsb() throws Exception{
		//组建ESB请求报文对象
		EsbPacket<BodyDto> requestDto = test();
		
		//实例化ESB返回报文对象
		EsbPacket<BodyDto> responseDto = EsbPacket.initEsbPacket(BodyDto.class,SysHeaderDto.class,"service");
		
		TcpConfig tcpConfig = new TcpConfig("38.62.24.188",10010);
		
		responseDto = EsbClient.send(tcpConfig, requestDto, responseDto);
		
		System.out.println(responseDto.getSysHeader(SysHeaderDto.class).getServiceCode());
		
		System.out.println("ResponseDto:"+responseDto);
	}
	
	
	public static void fromXml(String xml) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		
		EsbPacket<BodyDto> dto = EsbPacket.initEsbPacket(BodyDto.class,SysHeaderDto.class,AppHeaderDto.class);
		
		EsbPacket<BodyDto> retDto = XmlUtils.fromXML(xml, dto);
		
		System.out.println("返回对象:"+retDto);
		
		System.out.println("返回对象转XML："+XmlUtils.toXml(retDto));
		
	}
	
//	public static String toXml() throws IllegalArgumentException, IllegalAccessException, InstantiationException, ClassNotFoundException{
//		EsbPacket<TLBody> dto = EsbPacket.initEsbPacket( TLBody.class, SysHead.class, AppHead.class, LocalHead.class, "SERVICE");
//		BodyDto body = new BodyDto();
//		body.setCmitem("01");
//		body.setCmsqno("11");
//		
//		List<BodyListDto> bodyList = new ArrayList<BodyListDto>();
//		
//		BodyListDto dodyListDto = new BodyListDto();
//		dodyListDto.setId("1");
//		dodyListDto.setName("Name1");
//		bodyList.add(dodyListDto);
//		
//		dodyListDto = new BodyListDto();
//		dodyListDto.setId("2");
//		dodyListDto.setName("Name2");
//		bodyList.add(dodyListDto);
//		
//		body.setARRAY(bodyList);
//		
//		SysHead sysHeader = dto.getSysHeader(SysHead.class);
//		sysHeader.setSERVICE_CODE("123456789");
//		SysHeadRet ret = new SysHeadRet();
//		ret.setRET_CODE("1111");
//		ret.setRET_MSG("2222");
//		sysHeader.setRET(ret);
//		
//		AppHead appHeader = dto.getAppHeader(AppHead.class);
//		appHeader.setACCOUNT_SYS_DATE("xxx");
//		
//		String xml = XmlUtils.toXml(dto);
//		
//		System.out.println("请求的XML报文"+xml);
//		
//		return xml;
//	}
	
//	public static void EsbCnapsReviceImpl0359988() throws Exception{
//		EsbPacket<BodyDto0359988> requestDto = EsbPacket.initEsbPacket(BodyDto0359988.class, SysHeadDto.class,"service");
//		requestDto.getBody().setClrDt("20180731");
//		requestDto.getBody().setBusiSts("9");
//		requestDto.getSysHeader(SysHeadDto.class).setServiceCode("03003000019");
//		requestDto.getSysHeader(SysHeadDto.class).setServiceScene("01");
//		
//		TcpConfig tcpConfig = new TcpConfig("127.0.0.1",10010);
//		
//		EsbPacket<BodyListDto0359988> retDto = EsbPacket.initEsbPacket(BodyListDto0359988.class, SysHeadDto.class,AppHeadDto.class,LocalHeadDto.class,"service");
//		
//		retDto = EsbClient.send(tcpConfig, requestDto, retDto);
//		
//		System.out.println(retDto.getSysHeader(SysHeaderDto.class).getServiceCode());
//		
////		System.out.println("ResponseDto:"+retDto);
//	}
//	
//	public static void EsbCnapsReviceImpl0359989() throws Exception{
//		EsbPacket<BodyDto0359989> requestDto = EsbPacket.initEsbPacket(BodyDto0359989.class, SysHeadDto.class,"service");
//		requestDto.getBody().setClrDt("20180730");
//		requestDto.getBody().setBsnId("002");
//		requestDto.getSysHeader(SysHeadDto.class).setServiceCode("03003000019");
//		requestDto.getSysHeader(SysHeadDto.class).setServiceScene("02");
//		
//		TcpConfig tcpConfig = new TcpConfig("127.0.0.1",10010);
//		
//		EsbPacket<BodyDto0359989> retDto = EsbPacket.initEsbPacket(BodyDto0359989.class, SysHeadDto.class,AppHeadDto.class,LocalHeadDto.class,"service");
//		
//		retDto = EsbClient.send(tcpConfig, requestDto, retDto);
//		
//		System.out.println(retDto.getSysHeader(SysHeaderDto.class).getServiceCode());
//		
//		System.out.println("ResponseDto:"+retDto);
//	}
//	
//	public static void EsbCnapsReviceImpl0359990() throws Exception{
//		EsbPacket<BodyDto0359990> requestDto = EsbPacket.initEsbPacket(BodyDto0359990.class, SysHeadDto.class,"service");
//		requestDto.getBody().setClrDt("20180730");
//		requestDto.getBody().setBsnId("002");
//		requestDto.getBody().setBusiSts("1");
//		requestDto.getSysHeader(SysHeadDto.class).setServiceCode("03002000026");
//		requestDto.getSysHeader(SysHeadDto.class).setServiceScene("01");
//		
//		TcpConfig tcpConfig = new TcpConfig("127.0.0.1",10010);
//		
//		EsbPacket<BodyDto0359990> retDto = EsbPacket.initEsbPacket(BodyDto0359990.class, SysHeadDto.class,AppHeadDto.class,LocalHeadDto.class,"service");
//		
//		retDto = EsbClient.send(tcpConfig, requestDto, retDto);
//		
//		System.out.println(retDto.getSysHeader(SysHeaderDto.class).getServiceCode());
//		
//		System.out.println("ResponseDto:"+retDto);
//	}
	
}
