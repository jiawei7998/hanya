package com.singlee.opics.esb;

import java.util.ArrayList;
import java.util.List;

import com.singlee.esb.client.EsbClient;
import com.singlee.esb.client.packet.EsbPacket;
import com.singlee.financial.common.util.DateUtil;
//import com.singlee.financial.esb.dlcb.bean.CommReq;
//import com.singlee.financial.esb.dlcb.bean.CommRes;
//import com.singlee.financial.esb.dlcb.bean.IB1243BodyReqDto;
//import com.singlee.financial.esb.dlcb.bean.IB1243BodyRespDto;
//import com.singlee.financial.esb.dlcb.bean.IctrlDto;
//import com.singlee.financial.esb.dlcb.bean.LstAcctIn;
//import com.singlee.financial.esb.dlcb.bean.LstAcctOut;
//import com.singlee.financial.esb.dlcb.bean.OctrlDto;
//import com.singlee.financial.esb.dlcb.bean.Printer;

public class MqTest {

    private static String SERVICE_CODE = "41012345";// 服务码
    private static String NODE_ID = "410";// 本方系统编码
    private static String SVR_NODE_ID = "108";// 系统三位码,服务方的
    private static String BRANCH_ID = "0019901";// 机构代号
    private static String TELLER_ID = "80037";// 交易柜员
    private static String CORP_COD = "9999";// 法人代码,有核心提供

    public static void main(String[] args) throws Exception {

//        testIB1243Req();

    }

//    public static EsbPacket<IB1243BodyRespDto> testIB1243Response(EsbPacket<IB1243BodyRespDto> respDto) throws Exception {
//        
//
//        /******************** 公共输出信息 **************************/
//        OctrlDto octrlDto = respDto.getSysHeader(OctrlDto.class);
//        octrlDto.setSERVICE_CODE("41012345");
//        octrlDto.setTX_STATUS("0");
//        octrlDto.setHOST_BUS_DT(DateUtil.getCurrentDateAsString("yyyyMMdd"));
//        octrlDto.setHOST_CPU_DT(DateUtil.getCurrentDateAsString("yyyyMMdd"));
//        octrlDto.setHOST_PROC_TIME(DateUtil.getCurrentTimeAsString("HHmmssSSS"));
//        octrlDto.setTX_LOG_NO("000000000000000");
//        octrlDto.setFRT_DATE(DateUtil.getCurrentDateAsString("yyyyMMdd"));
//        octrlDto.setFRT_LOG_NO("000000000000000");
//        octrlDto.setSVR_NODE_ID("000000000000000");
//        octrlDto.setMSG_CODE("000000");
//        octrlDto.setMSG_TXT("交易成功");
//        /**************************** ib1243反馈报文体信息 ******************************/
//        IB1243BodyRespDto bodyRespDto = respDto.getBody();
//        bodyRespDto.setBencfkje("000000000000000");
//        bodyRespDto.setDkzhangh("000000000000000");
//        // 重复组
//        List<LstAcctOut> acctOutList = new ArrayList<LstAcctOut>();
//        LstAcctOut acctOut = new LstAcctOut();
//        acctOut.setBeiyzd01("备注1");
//        acctOut.setBeiyzd02("备注2");
//        acctOut.setBeiyzd03("备注3");
//        acctOutList.add(acctOut);
//        LstAcctOut acctOut1 = new LstAcctOut();
//        acctOut1.setBeiyzd01("备注11");
//        acctOut1.setBeiyzd02("备注22");
//        acctOut1.setBeiyzd03("备注33");
//        acctOutList.add(acctOut1);
//        bodyRespDto.setLstAcctOut_count("2");
//        bodyRespDto.setLstAcctOut_array(acctOutList);
//
//        return respDto;
//    }

//    public static EsbPacket<IB1243BodyReqDto> testIB1243Req() throws Exception {
//        // 初始化请求消息,用于拼装请求消息
//        EsbPacket<IB1243BodyReqDto> requestDto =
//            EsbPacket.initEsbPacket(IB1243BodyReqDto.class, IctrlDto.class, CommReq.class, Printer.class, "Message");
//        // 初始化接收消息对象,用于接收对象
//        EsbPacket<IB1243BodyRespDto> respDto =
//            EsbPacket.initEsbPacket(IB1243BodyRespDto.class, OctrlDto.class, CommRes.class, Printer.class, "Message");
//
//        /*********************** 公共输入信息 **************************************/
//        IctrlDto ictrlDto = requestDto.getSysHeader(IctrlDto.class);
//        ictrlDto.setSERVICE_CODE(SERVICE_CODE);// 每个接口确定值
//        ictrlDto.setORG_NODE_ID(NODE_ID);// 原始业务发起方系统编号
//        ictrlDto.setORG_BRANCH_ID(BRANCH_ID);// 原始方机构代号
//        ictrlDto.setORG_TELLER_ID(TELLER_ID);// 原始方交易柜员
//        ictrlDto.setORG_TX_DATE(DateUtil.getCurrentDateAsString("yyyyMMdd"));// 营业日期
//        ictrlDto.setORG_TX_TIME(DateUtil.getCurrentTimeAsString("HHmmssSSS"));// 营业时间
//        ictrlDto.setORG_TX_LOG_NO("42020171214902007");// 与业务唯一编码保持一致。
//        ictrlDto.setORG_TRADE_CODE("1001");// 交易码（8字节）， Comm_req中的发起方交易码
//        ictrlDto.setSND_NODE_ID(NODE_ID);// 发送方系统编号（3字节），外币资金系统为410
//        ictrlDto.setSND_BRANCH_ID(BRANCH_ID);// 发送方业务机构代号 与原始的一致。
//        ictrlDto.setSND_TX_DATE(DateUtil.getCurrentDateAsString("yyyyMMdd"));// 发送方交易日期
//        ictrlDto.setSND_TX_TIME(DateUtil.getCurrentTimeAsString("HHmmssSSS"));// 发送方交易时间
//        ictrlDto.setSND_TX_LOG_NO("42020171214902007");// 发送方交易流水号
//        ictrlDto.setGLO_SNO("42020171214902007");// 原始业务唯一编码 系统编号（3位） + 8位日期(yyyymmdd) + 11位序号（在指定日期内唯一）
//        ictrlDto.setSVR_NODE_ID(SVR_NODE_ID);// 应答系统标识(3字节)服务提供方的系统标识,如核心
//        ictrlDto.setCORP_COD(CORP_COD);// 法人代码-由核心提供
//        /*********************** 核心公共输入信息 **************************************/
//        // CommReq commReq = requestDto.getAppHeader(CommReq.class);
//
//        /**************************** ib1243请求报文体信息 ******************************/
//        IB1243BodyReqDto body = requestDto.getBody();
//        body.setYwzlbhao("DLYH_420_001");
//        List<LstAcctIn> lstAcctIns = new ArrayList<LstAcctIn>();
//        LstAcctIn acctIn = new LstAcctIn();
//        acctIn.setZhkonzbz("00000000000000000000");// 账户控制标志
//        acctIn.setZhaolaiy("1");// 账号来源0--固定账号 1--拼账号 2--现金 3--待销账 4--票据账号 5--代理业务号查询
//        acctIn.setKehuzhao("13089901");// 客户账号
//        acctIn.setHuobdaih("156");// 货币代号
//        acctIn.setChaohubz("1");// 账户钞汇标志0--现钞 1--现汇 N--无
//        acctIn.setYanmbzhi("0");// 密码校验方式 0--不校验 1--校验查询密码 2--校验交易密码 3--校验对公账户查询密码 4--效验账户级查询密码
//        acctIn.setJiedaibz("0");// 借贷标志 0--借 1--贷
//        acctIn.setJiaoyije("200000000.00");// 交易金额
//        acctIn.setZhaiyodm("780");// 摘要代码
//        acctIn.setZhaiyoms("非标投资业务认购");// 摘要描述
//        acctIn.setSfzdbhzh("0");// 是否指定保护账号标志 1--是 0--否
//        acctIn.setJigolaiy("0");// 机构来源 0--当前帐务机构 1--分行帐务机构 2--总行帐务机构 3--固定机构号 4--指定机构分行账务机构
//        acctIn.setJigouhao("0019901");// 机构号
//        acctIn.setYewubima("130899001");// 业务编码
//        acctIn.setZhhaxhao("000001");// 账号序号
//        lstAcctIns.add(acctIn);
//        LstAcctIn acctIn1 = new LstAcctIn();
//        acctIn1.setZhkonzbz("00000000000000000000");// 账户控制标志
//        acctIn1.setZhaolaiy("1");// 账号来源0--固定账号 1--拼账号 2--现金 3--待销账 4--票据账号 5--代理业务号查询
//        acctIn1.setKehuzhao("261106");// 客户账号
//        acctIn1.setHuobdaih("156");// 货币代号
//        acctIn1.setChaohubz("1");// 账户钞汇标志0--现钞 1--现汇 N--无
//        acctIn1.setYanmbzhi("0");// 密码校验方式 0--不校验 1--校验查询密码 2--校验交易密码 3--校验对公账户查询密码 4--效验账户级查询密码
//        acctIn1.setJiedaibz("1");// 借贷标志 0--借 1--贷
//        acctIn1.setJiaoyije("200000000.00");// 交易金额
//        acctIn1.setZhaiyodm("780");// 摘要代码
//        acctIn1.setZhaiyoms("非标投资业务认购");// 摘要描述
//        acctIn1.setSfzdbhzh("0");// 是否指定保护账号标志 1--是 0--否
//        acctIn1.setJigolaiy("0");// 机构来源 0--当前帐务机构 1--分行帐务机构 2--总行帐务机构 3--固定机构号 4--指定机构分行账务机构
//        acctIn1.setJigouhao("0019901");// 机构号
//        acctIn1.setYewubima("130899001");// 业务编码
//        acctIn1.setZhhaxhao("000001");// 账号序号
//        lstAcctIns.add(acctIn1);
//        body.setLstAcctIn_array(lstAcctIns);
//        body.setLstAcctIn_count(lstAcctIns.size() + "");// 借贷記錄
//        // 发送请求对象到MQ综合前置
//        respDto = EsbClient.send(requestDto, respDto, "GBK");
//        // 解析返回对象组装返回需要相关信息
//        /********************** 解析返回报文 ***************************/
//        // 头信息
//        OctrlDto octrlDto = respDto.getSysHeader(OctrlDto.class);
//        // 判断返回码
//        String txStatus = octrlDto.getTX_STATUS();
//        String retCode = octrlDto.getMSG_CODE();
//        String retMsg = octrlDto.getMSG_TXT();
//
//        // 代表0－前置处理成功，应用系统通讯处理成功；1－前置处理失败，或前置与应用系统通讯错；
//        if (null != txStatus && txStatus.equals("0")) {
//            if (null != retCode && retCode.equals("000000")) {
//
//                System.out.println("返回对象：" + octrlDto.getSERVICE_CODE());
//
//            } else {
//                System.out.println("交易系统处理失败，或前置与应用系统通讯错");
//            }
//        } else {
//            System.out.println("前置处理失败，或前置与应用系统通讯错");
//        }
//
//        return requestDto;
//    }
}
