package com.singlee.opics.esb;

import com.singlee.esb.client.packet.EsbLocalHeaderPacket;
import com.singlee.esb.client.packet.EsbSubPacketConvert;

public class LocalHeaderDto extends EsbLocalHeaderPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7897457584149497939L;
	
	private String branchId;

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public EsbSubPacketConvert getLocalHeaderConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		
		info.setSubPacketAliasName("LOCAL_HEADER");
		info.setSubPacketClassType(LocalHeaderDto.class);
		
		
		return info;
	}

}
