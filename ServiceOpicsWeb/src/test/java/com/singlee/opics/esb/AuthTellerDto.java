package com.singlee.opics.esb;

import java.io.Serializable;

public class AuthTellerDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2292974097520540696L;

	private String branchCode;

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AuthTellerDto [branchCode=");
		builder.append(branchCode);
		builder.append("]");
		return builder.toString();
	}
	
}
