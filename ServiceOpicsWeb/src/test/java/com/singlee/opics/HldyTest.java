package com.singlee.opics;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.remoting.RemoteConnectFailureException;

import com.caucho.hessian.client.HessianProxyFactory;
import com.singlee.financial.common.util.DateUtil;
import com.singlee.financial.opics.IHldyDbAndFileServer;

public class HldyTest {

	public static void main(String[] args) {
		IHldyDbAndFileServer dbAndFileServer = null;
		try {
			dbAndFileServer = (IHldyDbAndFileServer) new HessianProxyFactory().create(IHldyDbAndFileServer.class,
					"http://127.0.0.1:8080/fund-opics/api/service/hldyDbAndFileServer");
			Map<String, List<Date>> map = new HashMap<String, List<Date>>();
			List<Date> dates = new ArrayList<Date>();
			dates.add(DateUtil.parse("2018-1-1"));
			dates.add(DateUtil.parse("2018-2-15"));
			dates.add(DateUtil.parse("2018-2-16"));
			dates.add(DateUtil.parse("2018-2-17"));
			dates.add(DateUtil.parse("2018-2-18"));
			dates.add(DateUtil.parse("2018-2-19"));
			dates.add(DateUtil.parse("2018-2-20"));
			dates.add(DateUtil.parse("2018-2-21"));
			dates.add(DateUtil.parse("2018-4-5"));
			dates.add(DateUtil.parse("2018-4-6"));
			dates.add(DateUtil.parse("2018-4-7"));
			dates.add(DateUtil.parse("2018-4-29"));
			dates.add(DateUtil.parse("2018-4-30"));
			dates.add(DateUtil.parse("2018-5-1"));
			dates.add(DateUtil.parse("2018-6-18"));
			dates.add(DateUtil.parse("2018-9-24"));
			dates.add(DateUtil.parse("2018-10-1"));
			dates.add(DateUtil.parse("2018-10-2"));
			dates.add(DateUtil.parse("2018-10-3"));
			dates.add(DateUtil.parse("2018-10-4"));
			dates.add(DateUtil.parse("2018-10-5"));
			dates.add(DateUtil.parse("2018-10-6"));
			dates.add(DateUtil.parse("2018-10-7"));

			dates.add(DateUtil.parse("2019-1-1"));
			dates.add(DateUtil.parse("2019-2-4"));
			dates.add(DateUtil.parse("2019-2-5"));
			dates.add(DateUtil.parse("2019-2-6"));
			dates.add(DateUtil.parse("2019-2-7"));
			dates.add(DateUtil.parse("2019-2-8"));
			dates.add(DateUtil.parse("2019-2-9"));
			dates.add(DateUtil.parse("2019-2-10"));
			dates.add(DateUtil.parse("2019-4-5"));
			dates.add(DateUtil.parse("2019-5-1"));
			dates.add(DateUtil.parse("2019-9-13"));
			dates.add(DateUtil.parse("2019-10-1"));
			dates.add(DateUtil.parse("2019-10-2"));
			dates.add(DateUtil.parse("2019-10-3"));
			dates.add(DateUtil.parse("2019-10-4"));
			dates.add(DateUtil.parse("2019-10-5"));
			dates.add(DateUtil.parse("2019-10-6"));
			dates.add(DateUtil.parse("2019-10-7"));

			dates.add(DateUtil.parse("2020-1-1"));
			dates.add(DateUtil.parse("2020-1-24"));
			dates.add(DateUtil.parse("2020-1-27"));
			dates.add(DateUtil.parse("2020-1-28"));
			dates.add(DateUtil.parse("2020-1-29"));
			dates.add(DateUtil.parse("2020-1-30"));
			dates.add(DateUtil.parse("2020-4-6"));
			dates.add(DateUtil.parse("2020-5-1"));
			dates.add(DateUtil.parse("2020-5-4"));
			dates.add(DateUtil.parse("2020-5-5"));
			dates.add(DateUtil.parse("2020-6-25"));
			dates.add(DateUtil.parse("2020-6-26"));
			dates.add(DateUtil.parse("2020-10-1"));
			dates.add(DateUtil.parse("2020-10-2"));
			dates.add(DateUtil.parse("2020-10-3"));
			dates.add(DateUtil.parse("2020-10-4"));
			dates.add(DateUtil.parse("2020-10-5"));
			dates.add(DateUtil.parse("2020-10-6"));
			dates.add(DateUtil.parse("2020-10-7"));
			dates.add(DateUtil.parse("2020-10-8"));

			dates.add(DateUtil.parse("2021-1-1"));
			dates.add(DateUtil.parse("2021-2-11"));
			dates.add(DateUtil.parse("2021-2-12"));
			dates.add(DateUtil.parse("2021-2-15"));
			dates.add(DateUtil.parse("2021-2-16"));
			dates.add(DateUtil.parse("2021-2-17"));
			dates.add(DateUtil.parse("2021-4-5"));
			dates.add(DateUtil.parse("2021-5-3"));
			dates.add(DateUtil.parse("2021-5-4"));
			dates.add(DateUtil.parse("2021-5-5"));
			dates.add(DateUtil.parse("2021-6-14"));
			dates.add(DateUtil.parse("2021-9-20"));
			dates.add(DateUtil.parse("2021-9-21"));
			dates.add(DateUtil.parse("2021-10-1"));
			dates.add(DateUtil.parse("2021-10-4"));
			dates.add(DateUtil.parse("2021-10-5"));
			dates.add(DateUtil.parse("2021-10-6"));
			dates.add(DateUtil.parse("2021-10-7"));
			map.put("CNY", dates);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteConnectFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
