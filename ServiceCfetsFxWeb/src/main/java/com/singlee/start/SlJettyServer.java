package com.singlee.start;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class SlJettyServer {

	private static final int HTTP_PORT = 8083;
	private static final String CONTEXT = "/fund-cfetsFx";

	public static void main(String[] args) {
		// 创建jetty web容器
		Server server = new Server(HTTP_PORT);
		// 在退出程序是关闭服务
		server.setStopAtShutdown(true);

		ServletContextHandler handler = new ServletContextHandler();
		handler.setContextPath(CONTEXT);
		handler.setAttribute("webAppRootKey", "cfetsFx.root");
		XmlWebApplicationContext context = new XmlWebApplicationContext();
		// 加载spring配置文件
		context.setConfigLocation("classpath:main.spring.xml");
		// 相当于web.xml中配置的ContextLoaderListener
		handler.addEventListener(new ContextLoaderListener(context));
		// 添加一个Servlet服务
		// +++++++++++++++++++发布Hessian服务Start+++++++++++++++++++++++++++++
		// 声明Spring DispatcherServlet
		DispatcherServlet hessianDs = new DispatcherServlet();
		hessianDs.setContextConfigLocation("classpath*:cfetsFx.dispatcher.xml");
		ServletHolder hessianSh = new ServletHolder(hessianDs);
		hessianSh.setName("CfetsFxServer");
		// 设置启动顺序
		hessianSh.setInitOrder(2);
		// springmvc拦截规则 相当于web.xml中配置的DispatcherServlet
		handler.addServlet(hessianSh, "/api/service/*");
		// ++++++++++++++++++++发布Hessian服务End+++++++++++++++++++++++++++++

		server.setHandler(handler);
		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
