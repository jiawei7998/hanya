package com.singlee.financial.cfets.confirm;

import java.util.Date;

import imix.field.AccountCurrency;
import imix.field.AccruedInterestTotalAmt;
import imix.field.BeginString;
import imix.field.BodyLength;
import imix.field.CalculateAgency;
import imix.field.CalculatedCcyLastQty;
import imix.field.ConfirmID;
import imix.field.ConfirmType;
import imix.field.ContactInfoID;
import imix.field.ContactInfoIDType;
import imix.field.CouponPaymentDateReset;
import imix.field.Currency;
import imix.field.Currency1;
import imix.field.Currency2;
import imix.field.DayCount;
import imix.field.DeliverToCompID;
import imix.field.DeliveryType;
import imix.field.EndDate;
import imix.field.ExecID;
import imix.field.FirstPeriodStartDate;
import imix.field.InterestAccuralDaysAdjustment;
import imix.field.LastPx;
import imix.field.LastQty;
import imix.field.LastSpotRate;
import imix.field.LegBenchmarkCurveName;
import imix.field.LegBenchmarkSpread;
import imix.field.LegBenchmarkTenor;
import imix.field.LegCalculatedCcyLastQty;
import imix.field.LegCouponPaymentDate;
import imix.field.LegCouponPaymentDateReset;
import imix.field.LegCouponPaymentFrequency;
import imix.field.LegCurrency;
import imix.field.LegCurrency1;
import imix.field.LegCurrency2;
import imix.field.LegDayCount;
import imix.field.LegInterestAccrualDate;
import imix.field.LegInterestAccrualMethod;
import imix.field.LegInterestAccrualResetFrequency;
import imix.field.LegInterestFixDateAdjustment;
import imix.field.LegLastPx;
import imix.field.LegLastQty;
import imix.field.LegOrderQty;
import imix.field.LegPrice;
import imix.field.LegPriceType;
import imix.field.LegSettlDate;
import imix.field.LegSide;
import imix.field.LegSign;
import imix.field.LegStubIndicator;
import imix.field.MarketID;
import imix.field.MarketIndicator;
import imix.field.MaturityDate;
import imix.field.MsgSeqNum;
import imix.field.MsgType;
import imix.field.NetGrossInd;
import imix.field.NotionalExchangeType;
import imix.field.OnBehalfOfCompID;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.PayDate;
import imix.field.PaymentAmt;
import imix.field.PaymentCurrency;
import imix.field.SenderCompID;
import imix.field.SenderSubID;
import imix.field.SendingTime;
import imix.field.SettlCurrAmt2;
import imix.field.SettlDate;
import imix.field.SettlDate2;
import imix.field.Side;
import imix.field.StartDate;
import imix.field.Symbol;
import imix.field.TargetCompID;
import imix.field.Text;
import imix.field.TradeDate;
import imix.field.TradeLimitDays;
import imix.imix10.Confirmation;
import imix.imix10.ExecutionReport;
import imix.imix10.Message.Header;
import org.apache.log4j.Logger;
import com.singlee.financial.cfets.confirm.ImixFxConfirmMessageProcess;

public class ImixFxConfirmMessageProcess {

	Logger logger = Logger.getLogger(ImixFxConfirmMessageProcess.class);

	public void swapConvert(ExecutionReport message, Confirmation remessage) throws Exception {

		// Cfets交易类型标识
		String marketindicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
		// Cfets流水号
		String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
		// 报文协议版本
		String beginstring = message.getHeader().getString(BeginString.FIELD);
		// 交易状态
		// String dealtranstype = message.getDealTransType().getValue() + "";
		// 消息类型
		// String msttype = message.getHeader().getString(MsgType.FIELD);
		// 成交日期
		Date tradeDate = message.getTradeDate().getDateValue();

		// header
		Header header = remessage.getHeader();
		header.setField(new BeginString(beginstring));
		header.setField(new BodyLength(1));
		header.setField(new MsgType("AK"));
		header.setField(new SenderCompID("303784"));
		header.setField(new SenderSubID("gzybapi"));
		header.setField(new TargetCompID("CFETS-RMB-TPP"));
		header.setField(new MsgSeqNum(1));
		header.setField(new SendingTime("20111113-10:21:10"));
		// body
		if (message.isSetExecID()) {
			remessage.setField(new ExecID(execid));
		}
		if (message.isSetTradeDate()) {
			remessage.setField(new TradeDate(tradeDate));
		}
		if (message.isSetMarketID()) {
			remessage.setField(new MarketID(message.getMarketID().getValue()));
		}
		remessage.setField(new ConfirmID("66666666"));
		if (message.isSetNetGrossInd()) {
			remessage.setField(new NetGrossInd(message.getNetGrossInd().getValue()));
		}
		if (message.isSetMarketIndicator()) {
			remessage.setField(new MarketIndicator(marketindicator));
		}
		if (message.isSetDeliveryType()) {
			remessage.setField(new DeliveryType(message.getDeliveryType().getValue()));
		}
		// 重复组
		imix.imix10.ExecutionReport.NoLegs noLegs = new imix.imix10.ExecutionReport.NoLegs();
		int NoLegs = message.isSetNoLegs() ? message.getNoLegs().getValue() : 0;
		for (int i = 1; i <= NoLegs; i++) {
			message.getGroup(i, noLegs);
			if (noLegs.isSetLegSide()) {
				noLegs.set(new LegSide(noLegs.getLegSide().getValue()));
			}
			if (noLegs.isSetLegSettlDate()) {
				noLegs.set(new LegSettlDate(noLegs.getLegSettlDate().getValue()));
			}
			if (noLegs.isSetLegLastPx()) {
				noLegs.set(new LegLastPx(noLegs.getLegLastPx().getValue()));
			}
			if (noLegs.isSetLegLastQty()) {
				noLegs.set(new LegLastQty(noLegs.getLegLastQty().getValue()));
			}
			if (noLegs.isSetLegCalculatedCcyLastQty()) {
				noLegs.set(new LegCalculatedCcyLastQty(noLegs.getLegCalculatedCcyLastQty().getValue()));
			}
			if (noLegs.isSetLegCurrency1()) {
				noLegs.set(new LegCurrency1(noLegs.getLegCurrency1().getValue()));
			}
			if (noLegs.isSetLegCurrency2()) {
				noLegs.set(new LegCurrency2(noLegs.getLegCurrency2().getValue()));
			}
			remessage.addGroup(noLegs);
		}
		imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs = new imix.imix10.ExecutionReport.NoPartyIDs();
		PartyID partyID = new PartyID();
		PartyRole partyRole = new PartyRole();
		imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs();
		PartySubID partySubID = new PartySubID();
		PartySubIDType partySubIDType = new PartySubIDType();
		int NoPartyIDs = message.isSetNoPartyIDs() ? message.getNoPartyIDs().getValue() : 0;
		for (int j = 1; j <= NoPartyIDs; j++) {
			message.getGroup(j, noPartyIDs);
			partyID = noPartyIDs.get(partyID);
			partyRole = noPartyIDs.get(partyRole);
			if (noPartyIDs.isSetPartyID()) {
				noPartyIDs.set(new PartyID(partyID.getValue()));
			}
			if (noPartyIDs.isSetPartyRole()) {
				noPartyIDs.set(new PartyRole(partyRole.getValue()));
			}
			remessage.addGroup(noPartyIDs);
			int NoPartySubIDs = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.getNoPartySubIDs().getValue() : 0;
			for (int l = 1; l <= NoPartySubIDs; l++) {
				noPartyIDs.getGroup(l, noPartySubIDs);
				partySubID = noPartySubIDs.getPartySubID();
				partySubIDType = noPartySubIDs.getPartySubIDType();
				if (noPartySubIDs.isSetPartySubID()) {
					noPartySubIDs.set(new PartySubID(partySubID.getValue()));
				}
				if (noPartySubIDs.isSetPartySubIDType()) {
					noPartySubIDs.set(new PartySubIDType(partySubIDType.getValue()));
				}
				noPartyIDs.addGroup(noPartySubIDs);
			}

		}
	}

	public void sptConvert(ExecutionReport message, Confirmation remessage) throws Exception {

		// Cfets交易类型标识
		String marketindicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
		// Cfets流水号
		String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
		// 报文协议版本
		String beginstring = message.getHeader().getString(BeginString.FIELD);
		// 交易状态
		// String dealtranstype = message.getDealTransType().getValue() + "";
		// 消息类型
		// String msttype = message.getHeader().getString(MsgType.FIELD);
		// 成交日期
		Date tradeDate = message.getTradeDate().getDateValue();

		// header
		Header header = remessage.getHeader();
		header.setField(new BeginString(beginstring));
		header.setField(new BodyLength(1));
		header.setField(new MsgType("AK"));
		header.setField(new SenderCompID("303784"));
		header.setField(new SenderSubID("gzybapi"));
		header.setField(new TargetCompID("CFETS-RMB-TPP"));
		header.setField(new MsgSeqNum(1));
		header.setField(new SendingTime("20111113-10:21:10"));

		// body
		if (message.isSetExecID()) {
			remessage.setField(new ExecID(execid));
		}
		if (message.isSetTradeDate()) {
			remessage.setField(new TradeDate(tradeDate));
		}
		remessage.setField(new ConfirmID("66666666"));
		if (message.isSetMarketID()) {
			remessage.setField(new MarketID(message.getMarketID().getValue()));
		}
		if (message.isSetNetGrossInd()) {
			remessage.setField(new NetGrossInd(message.getNetGrossInd().getValue()));
		}
		if (message.isSetMarketIndicator()) {
			remessage.setField(new MarketIndicator(marketindicator));
		}
		if (message.isSetDeliveryType()) {
			remessage.setField(new DeliveryType(message.getDeliveryType().getValue()));
		}
		if (message.isSetSettlDate()) {
			remessage.setField(new SettlDate(message.getSettlDate().getValue()));
		}
		if (message.isSetLastPx()) {
			remessage.setField(new LastPx(message.getLastPx().getValue()));
		}
		if (message.isSetCurrency1()) {
			remessage.setField(new Currency1(message.getCurrency1().getValue()));
		}
		if (message.isSetLastQty()) {
			remessage.setField(new LastQty(message.getLastQty().getValue()));
		}
		if (message.isSetCurrency2()) {
			remessage.setField(new Currency2(message.getCurrency2().getValue()));
		}
		if (message.isSetCalculatedCcyLastQty()) {
			remessage.setField(new CalculatedCcyLastQty(message.getCalculatedCcyLeavesQty().getValue()));
		}
		// 重复组
		imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs = new imix.imix10.ExecutionReport.NoPartyIDs();
		PartyID partyID = new PartyID();
		PartyRole partyRole = new PartyRole();
		imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs();
		PartySubID partySubID = new PartySubID();
		PartySubIDType partySubIDType = new PartySubIDType();
		int NoPartyIDs = message.isSetNoPartyIDs() ? message.getNoPartyIDs().getValue() : 0;
		for (int j = 1; j <= NoPartyIDs; j++) {
			message.getGroup(j, noPartyIDs);
			partyID = noPartyIDs.get(partyID);
			partyRole = noPartyIDs.get(partyRole);
			if (noPartyIDs.isSetPartyID()) {
				noPartyIDs.set(new PartyID(partyID.getValue()));
			}
			if (noPartyIDs.isSetPartyRole()) {
				noPartyIDs.set(new PartyRole(partyRole.getValue()));
			}
			remessage.addGroup(noPartyIDs);
			int NoPartySubIDs = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.getNoPartySubIDs().getValue() : 0;
			for (int l = 1; l <= NoPartySubIDs; l++) {
				noPartyIDs.getGroup(l, noPartySubIDs);
				partySubID = noPartySubIDs.getPartySubID();
				partySubIDType = noPartySubIDs.getPartySubIDType();
				if (noPartySubIDs.isSetPartySubID()) {
					noPartySubIDs.set(new PartySubID(partySubID.getValue()));
				}
				if (noPartySubIDs.isSetPartySubIDType()) {
					noPartySubIDs.set(new PartySubIDType(partySubIDType.getValue()));
				}
				noPartyIDs.addGroup(noPartySubIDs);
			}
		}

	}

	public void fwdConvert(ExecutionReport message, Confirmation remessage) throws Exception {

		// Cfets交易类型标识
		String marketindicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
		// Cfets流水号
		String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
		// 报文协议版本
		String beginstring = message.getHeader().getString(BeginString.FIELD);
		// 交易状态
		// String dealtranstype = message.getDealTransType().getValue() + "";
		// 消息类型
		// String msttype = message.getHeader().getString(MsgType.FIELD);
		// 成交日期
		Date tradeDate = message.getTradeDate().getDateValue();

		// header
		Header header = remessage.getHeader();
		header.setField(new BeginString(beginstring));
		header.setField(new BodyLength(1));
		header.setField(new MsgType("AK"));
		header.setField(new SenderCompID("303784"));
		header.setField(new SenderSubID("gzybapi"));
		header.setField(new TargetCompID("CFETS-RMB-TPP"));
		header.setField(new MsgSeqNum(1));
		header.setField(new SendingTime("20111113-10:21:10"));
		// body
		if (message.isSetExecID()) {
			remessage.setField(new ExecID(execid));
		}
		if (message.isSetTradeDate()) {
			remessage.setField(new TradeDate(tradeDate));
		}
		if (message.isSetMarketID()) {
			remessage.setField(new MarketID(message.getMarketID().getValue()));
		}
		remessage.setField(new ConfirmID("66666666"));
		if (message.isSetNetGrossInd()) {
			remessage.setField(new NetGrossInd(message.getNetGrossInd().getValue()));
		}
		if (message.isSetMarketIndicator()) {
			remessage.setField(new MarketIndicator(marketindicator));
		}
		if (message.isSetDeliveryType()) {
			remessage.setField(new DeliveryType(message.getDeliveryType().getValue()));
		}
		if (message.isSetSettlDate()) {
			remessage.setField(new SettlDate(message.getSettlDate().getValue()));
		}
		if (message.isSetLastPx()) {
			remessage.setField(new LastPx(message.getLastPx().getValue()));
		}
		if (message.isSetCurrency1()) {
			remessage.setField(new Currency1(message.getCurrency1().getValue()));
		}
		if (message.isSetLastQty()) {
			remessage.setField(new LastQty(message.getLastQty().getValue()));
		}
		if (message.isSetCurrency2()) {
			remessage.setField(new Currency2(message.getCurrency2().getValue()));
		}
		if (message.isSetCalculatedCcyLastQty()) {
			remessage.setField(new CalculatedCcyLastQty(message.getCalculatedCcyLastQty().getValue()));
		}
		// 重复组
		imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs = new imix.imix10.ExecutionReport.NoPartyIDs();
		PartyID partyID = new PartyID();
		PartyRole partyRole = new PartyRole();
		imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs();
		PartySubID partySubID = new PartySubID();
		PartySubIDType partySubIDType = new PartySubIDType();
		int NoPartyIDs = message.isSetNoPartyIDs() ? message.getNoPartyIDs().getValue() : 0;
		for (int j = 1; j <= NoPartyIDs; j++) {
			message.getGroup(j, noPartyIDs);
			partyID = noPartyIDs.get(partyID);
			partyRole = noPartyIDs.get(partyRole);
			if (noPartyIDs.isSetPartyID()) {
				noPartyIDs.set(new PartyID(partyID.getValue()));
			}
			if (noPartyIDs.isSetPartyRole()) {
				noPartyIDs.set(new PartyRole(partyRole.getValue()));
			}
			remessage.addGroup(noPartyIDs);
			int NoPartySubIDs = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.getNoPartySubIDs().getValue() : 0;
			for (int l = 1; l <= NoPartySubIDs; l++) {
				noPartyIDs.getGroup(l, noPartySubIDs);
				partySubID = noPartySubIDs.getPartySubID();
				partySubIDType = noPartySubIDs.getPartySubIDType();
				if (noPartySubIDs.isSetPartySubID()) {
					noPartySubIDs.set(new PartySubID(partySubID.getValue()));
				}
				if (noPartySubIDs.isSetPartySubIDType()) {
					noPartySubIDs.set(new PartySubIDType(partySubIDType.getValue()));
				}
				noPartyIDs.addGroup(noPartySubIDs);
			}

		}
	}

	public void crsConvert(ExecutionReport message, Confirmation remessage) throws Exception {

		// Cfets交易类型标识
		String marketindicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
		// Cfets流水号
		String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
		// 报文协议版本
		String beginstring = message.getHeader().getString(BeginString.FIELD);
		// 交易状态
		// String dealtranstype = message.getDealTransType().getValue() + "";
		// 消息类型
		// String msttype = message.getHeader().getString(MsgType.FIELD);
		// 成交日期
		Date tradeDate = message.getTradeDate().getDateValue();

		// header
		Header header = remessage.getHeader();
		header.setField(new BeginString(beginstring));
		header.setField(new BodyLength(1));
		header.setField(new MsgType("AK"));
		header.setField(new SenderCompID("303784"));
		header.setField(new SenderSubID("gzybapi"));
		header.setField(new TargetCompID("CFETS-RMB-TPP"));
		header.setField(new MsgSeqNum(2));
		header.setField(new SendingTime("20111113-10:21:10"));
		// body
		if (message.isSetExecID()) {
			remessage.setField(new ExecID(execid));
		}
		if (message.isSetTradeDate()) {
			remessage.setField(new TradeDate(tradeDate));
		}
		remessage.setField(new ConfirmID("66666666"));
		if (message.isSetMarketID()) {
			remessage.setField(new MarketID(message.getMarketID().getValue()));
		}
		if (message.isSetNetGrossInd()) {
			remessage.setField(new NetGrossInd(message.getNetGrossInd().getValue()));
		}
		if (message.isSetMarketIndicator()) {
			remessage.setField(new MarketIndicator(marketindicator));
		}
		if (message.isSetMaturityDate()) {
			remessage.setField(new MaturityDate(message.getMaturityDate().getValue()));
		}
		// remessage.setField(new DateAdjustmentIndic());
		// remessage.setField(new IniExDate());
		// remessage.setField(new FinalExDate());
		if (message.isSetCalculateAgency()) {
			remessage.setField(new CalculateAgency(message.getCalculateAgency().getValue()));
		}
		if (message.isSetNotionalExchangeType()) {
			remessage.setField(new NotionalExchangeType(message.getNotionalExchangeType().getValue()));
		}
		if (message.isSetLastSpotRate()) {
			remessage.setField(new LastSpotRate(message.getLastSpotRate().getValue()));
		}
		if (message.isSetText()) {
			remessage.setField(new Text(message.getText().getValue()));
		}
		// 重复组
		imix.imix10.ExecutionReport.NoLegs noLegs = new imix.imix10.ExecutionReport.NoLegs();
		imix.imix10.ExecutionReport.NoLegs.NoPayment noPayment = new imix.imix10.ExecutionReport.NoLegs.NoPayment();
		int NoLegs = message.isSetNoLegs() ? message.getNoLegs().getValue() : 0;
		for (int i = 1; i <= NoLegs; i++) {
			message.getGroup(i, noLegs);
			if (noLegs.isSetLegSide()) {
				noLegs.set(new LegSide(noLegs.getLegSide().getValue()));
			}
			if (noLegs.isSetLegSign()) {
				noLegs.set(new LegSign(noLegs.getLegSign().getValue()));
			}
			if (noLegs.isSetLegCurrency()) {
				noLegs.set(new LegCurrency(noLegs.getLegCurrency().getValue()));
			}
			if (noLegs.isSetLegCouponPaymentDate()) {
				noLegs.set(new LegCouponPaymentDate(noLegs.getLegCouponPaymentDate().getValue()));
			}
			if (noLegs.isSetLegOrderQty()) {
				noLegs.set(new LegOrderQty(noLegs.getLegOrderQty().getValue()));
			}
			if (noLegs.isSetLegPriceType()) {
				noLegs.set(new LegPriceType(noLegs.getLegPriceType().getValue()));
			}
			if (noLegs.isSetLegBenchmarkCurveName()) {
				noLegs.set(new LegBenchmarkCurveName(noLegs.getLegBenchmarkCurveName().getValue()));
			}
			if (noLegs.isSetLegBenchmarkTenor()) {
				noLegs.set(new LegBenchmarkTenor(noLegs.getLegBenchmarkTenor().getValue()));
			}
			if (noLegs.isSetLegBenchmarkSpread()) {
				noLegs.set(new LegBenchmarkSpread(noLegs.getLegBenchmarkSpread().getValue()));
			}
			if (noLegs.isSetLegPrice()) {
				noLegs.set(new LegPrice(noLegs.getLegPrice().getValue()));
			}
			if (noLegs.isSetLegInterestAccrualResetFrequency()) {
				noLegs.set(
						new LegInterestAccrualResetFrequency(noLegs.getLegInterestAccrualResetFrequency().getValue()));
			}
			if (noLegs.isSetLegInterestFixDateAdjustment()) {
				noLegs.set(new LegInterestFixDateAdjustment(noLegs.getLegInterestFixDateAdjustment().getValue()));
			}
			if (noLegs.isSetLegDayCount()) {
				noLegs.set(new LegDayCount(noLegs.getLegDayCount().getValue()));
			}
			if (noLegs.isSetLegCouponPaymentFrequency()) {
				noLegs.set(new LegCouponPaymentFrequency(noLegs.getLegCouponPaymentFrequency().getValue()));
			}
			if (noLegs.isSetLegCouponPaymentDateReset()) {
				noLegs.set(new LegCouponPaymentDateReset(noLegs.getLegCouponPaymentDateReset().getValue()));
			}
			if (noLegs.isSetLegStubIndicator()) {
				noLegs.set(new LegStubIndicator(noLegs.getLegStubIndicator().getValue()));
			}
			remessage.addGroup(noLegs);
			int NoPayment = noLegs.isSetNoPayment() ? noLegs.getNoPayment().getValue() : 0;
			for (int j = 1; j <= NoPayment; j++) {
				noLegs.getGroup(j, noPayment);
				if (noPayment.isSetPaymentAmt()) {
					noPayment.set(new PaymentAmt(noPayment.getPaymentAmt().getValue()));
				}
				if (noPayment.isSetPaymentCurrency()) {
					noPayment.set(new PaymentCurrency(noPayment.getPaymentCurrency().getValue()));
				}
				if (noPayment.isSetPayDate()) {
					noPayment.set(new PayDate(noPayment.getPayDate().getValue()));
				}
			}
		}
		imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs = new imix.imix10.ExecutionReport.NoPartyIDs();
		PartyID partyID = new PartyID();
		PartyRole partyRole = new PartyRole();
		imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs();
		PartySubID partySubID = new PartySubID();
		PartySubIDType partySubIDType = new PartySubIDType();
		AccountCurrency accountCurrency = new AccountCurrency();
		int NoPartyIDs = message.isSetNoPartyIDs() ? message.getNoPartyIDs().getValue() : 0;
		for (int j = 1; j <= NoPartyIDs; j++) {
			message.getGroup(j, noPartyIDs);
			partyID = noPartyIDs.get(partyID);
			partyRole = noPartyIDs.get(partyRole);
			accountCurrency = noPartyIDs.get(accountCurrency);
			if (noPartyIDs.isSetPartyID()) {
				noPartyIDs.set(new PartyID(partyID.getValue()));
			}
			if (noPartyIDs.isSetPartyRole()) {
				noPartyIDs.set(new PartyRole(partyRole.getValue()));
			}
			if (noPartyIDs.isSetAccountCurrency()) {
				noPartyIDs.set(new AccountCurrency(accountCurrency.getValue()));
			}
			remessage.addGroup(noPartyIDs);
			int NoPartySubIDs = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.getNoPartySubIDs().getValue() : 0;
			for (int l = 1; l <= NoPartySubIDs; l++) {
				noPartyIDs.getGroup(l, noPartySubIDs);
				partySubID = noPartySubIDs.getPartySubID();
				partySubIDType = noPartySubIDs.getPartySubIDType();
				if (noPartySubIDs.isSetPartySubID()) {
					noPartySubIDs.set(new PartySubID(partySubID.getValue()));
				}
				if (noPartySubIDs.isSetPartySubIDType()) {
					noPartySubIDs.set(new PartySubIDType(partySubIDType.getValue()));
				}
				noPartyIDs.addGroup(noPartySubIDs);
			}

		}
	}

	public void mmConvert(ExecutionReport message, Confirmation remessage) throws Exception {

		// Cfets交易类型标识
		String marketindicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
		// Cfets流水号
		String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
		// 报文协议版本
		String beginstring = message.getHeader().getString(BeginString.FIELD);
		// 交易状态
		// String dealtranstype = message.getDealTransType().getValue() + "";
		// 消息类型
		// String msttype = message.getHeader().getString(MsgType.FIELD);
		// 成交日期
		Date tradeDate = message.getTradeDate().getDateValue();

		// header
		Header header = remessage.getHeader();
		header.setField(new BeginString(beginstring));
		header.setField(new BodyLength(1));
		header.setField(new MsgType("AK"));
		header.setField(new SenderCompID("303784"));
		header.setField(new SenderSubID("gzybapi"));
		header.setField(new TargetCompID("CFETS-RMB-TPP"));
		header.setField(new MsgSeqNum(1));
		header.setField(new SendingTime("20111113-10:21:10"));
		// body
		if (message.isSetExecID()) {
			remessage.setField(new ExecID(execid));
		}
		if (message.isSetTradeDate()) {
			remessage.setField(new TradeDate(tradeDate));
		}
		remessage.setField(new ConfirmID("XXX"));
		if (message.isSetNetGrossInd()) {
			remessage.setField(new NetGrossInd(message.getNetGrossInd().getValue()));
		}
		if (message.isSetMarketIndicator()) {
			remessage.setField(new MarketIndicator(marketindicator));
		}
		if (message.isSetSettlDate()) {
			remessage.setField(new SettlDate(message.getSettlDate().getValue()));
		}
		if (message.isSetSettlDate2()) {
			remessage.setField(new SettlDate2(message.getSettlDate2().getValue()));
		}
		if (message.isSetCurrency()) {
			remessage.setField(new Currency(message.getCurrency().getValue()));
		}
		if (message.isSetLastPx()) {
			remessage.setField(new LastPx(message.getLastPx().getValue()));
		}
		if (message.isSetDayCount()) {
			remessage.setField(new DayCount(message.getDayCount().getValue()));
		}
		if (message.isSetSide()) {
			remessage.setField(new Side(message.getSide().getValue()));
		}
		if (message.isSetAccruedInterestTotalAmt()) {
			remessage.setField(new AccruedInterestTotalAmt(message.getAccruedInterestTotalAmt().getValue()));
		}
		if (message.isSetLastQty()) {
			remessage.setField(new LastQty(message.getLastQty().getValue()));
		}
		if (message.isSetSettlCurrAmt2()) {
			remessage.setField(new SettlCurrAmt2(message.getSettlCurrAmt2().getValue()));
		}
		imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs = new imix.imix10.ExecutionReport.NoPartyIDs();
		PartyID partyID = new PartyID();
		PartyRole partyRole = new PartyRole();
		imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs();
		PartySubID partySubID = new PartySubID();
		PartySubIDType partySubIDType = new PartySubIDType();
		int NoPartyIDs = message.isSetNoPartyIDs() ? message.getNoPartyIDs().getValue() : 0;
		for (int j = 1; j <= NoPartyIDs; j++) {
			message.getGroup(j, noPartyIDs);
			partyID = noPartyIDs.get(partyID);
			partyRole = noPartyIDs.get(partyRole);
			if (noPartyIDs.isSetPartyID()) {
				noPartyIDs.set(new PartyID(partyID.getValue()));
			}
			if (noPartyIDs.isSetPartyRole()) {
				noPartyIDs.set(new PartyRole(partyRole.getValue()));
			}
			remessage.addGroup(noPartyIDs);
			int NoPartySubIDs = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.getNoPartySubIDs().getValue() : 0;
			for (int l = 1; l <= NoPartySubIDs; l++) {
				noPartyIDs.getGroup(l, noPartySubIDs);
				partySubID = noPartySubIDs.getPartySubID();
				partySubIDType = noPartySubIDs.getPartySubIDType();
				if (noPartySubIDs.isSetPartySubID()) {
					noPartySubIDs.set(new PartySubID(partySubID.getValue()));
				}
				if (noPartySubIDs.isSetPartySubIDType()) {
					noPartySubIDs.set(new PartySubIDType(partySubIDType.getValue()));
				}
				noPartyIDs.addGroup(noPartySubIDs);
			}

		}
	}

	public void irsConvert(ExecutionReport message, Confirmation remessage) throws Exception {

		// Cfets交易类型标识
		String marketindicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
		// Cfets流水号
		String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
		// 报文协议版本
		String beginstring = message.getHeader().getString(BeginString.FIELD);
		// 交易状态
		// String dealtranstype = message.getDealTransType().getValue() + "";
		// 消息类型
		// String msttype = message.getHeader().getString(MsgType.FIELD);
		// 成交日期
		Date tradeDate = message.getTradeDate().getDateValue();

		// header
		Header header = remessage.getHeader();
		header.setField(new BeginString(beginstring));
		header.setField(new BodyLength(20));
		header.setField(new MsgType("AK"));
		header.setField(new SenderCompID("303784"));
		header.setField(new SenderSubID("gzybapi"));
		header.setField(new TargetCompID("CFETS-RMB-TPP"));
		header.setField(new OnBehalfOfCompID("303784"));
		header.setField(new DeliverToCompID("CFETS-RMB-TPP"));
		header.setField(new MsgSeqNum(26));
		header.setField(new SendingTime("20111113-10:21:10"));

		// body
		remessage.setField(new ConfirmID("XXX"));
		remessage.setField(new ConfirmType(106));
		if (message.isSetMarketID()) {
			remessage.setField(new MarketID(message.getMarketID().getValue()));
		}
		if (message.isSetExecID()) {
			remessage.setField(new ExecID(execid));
		}
		if (message.isSetLastQty()) {
			remessage.setField(new LastQty(message.getLastQty().getValue()));
		}
		if (message.isSetMarketIndicator()) {
			remessage.setField(new MarketIndicator(marketindicator));
		}
		if (message.isSetSymbol()) {
			remessage.setField(new Symbol(message.getSymbol().getValue()));
		}
		remessage.setField(new Text("aaaaa"));
		if (message.isSetTradeDate()) {
			remessage.setField(new TradeDate(tradeDate));
		}
		if (message.isSetStartDate()) {
			remessage.setField(new StartDate(message.getStartDate().getValue()));
		}
		if (message.isSetEndDate()) {
			remessage.setField(new EndDate(message.getEndDate().getValue()));
		}
		if (message.isSetCouponPaymentDateReset()) {
			remessage.setField(new CouponPaymentDateReset(message.getCouponPaymentDateReset().getValue()));
		}
		if (message.isSetInterestAccuralDaysAdjustment()) {
			remessage
					.setField(new InterestAccuralDaysAdjustment(message.getInterestAccuralDaysAdjustment().getValue()));
		}
		if (message.isSetFirstPeriodStartDate()) {
			remessage.setField(new FirstPeriodStartDate(message.getFirstPeriodStartDate().getValue()));
		}
		if (message.isSetTradeLimitDays()) {
			remessage.setField(new TradeLimitDays(message.getTradeLimitDays().getValue()));
		}
		if (message.isSetCalculateAgency()) {
			remessage.setField(new CalculateAgency(message.getCalculateAgency().getValue()));
		}
		if (message.isSetNetGrossInd()) {
			remessage.setField(new NetGrossInd(message.getNetGrossInd().getValue()));
		}
		if (message.isSetSide()) {
			remessage.setField(new Side(message.getSide().getValue()));
		}

		// 重复组
		imix.imix10.ExecutionReport.NoLegs noLegs = new imix.imix10.ExecutionReport.NoLegs();
		int NoLegs = message.isSetNoLegs() ? message.getNoLegs().getValue() : 0;
		for (int i = 1; i <= NoLegs; i++) {
			message.getGroup(i, noLegs);
			if (noLegs.isSetLegSide()) {
				noLegs.set(new LegSide(noLegs.getLegSide().getValue()));
			}
			if (noLegs.isSetLegPriceType()) {
				noLegs.set(new LegPriceType(noLegs.getLegPriceType().getValue()));
			}
			if (noLegs.isSetLegPrice()) {
				noLegs.set(new LegPrice(noLegs.getLegPrice().getValue()));
			}
			if (noLegs.isSetLegCouponPaymentFrequency()) {
				noLegs.set(new LegCouponPaymentFrequency(noLegs.getLegCouponPaymentFrequency().getValue()));
			}
			if (noLegs.isSetLegCouponPaymentDate()) {
				noLegs.set(new LegCouponPaymentDate(noLegs.getLegCouponPaymentDate().getValue()));
			}
			if (noLegs.isSetLegDayCount()) {
				noLegs.set(new LegDayCount(noLegs.getLegDayCount().getValue()));
			}
			if (noLegs.isSetLegBenchmarkCurveName()) {
				noLegs.set(new LegBenchmarkCurveName(noLegs.getLegBenchmarkCurveName().getValue()));
			}
			if (noLegs.isSetLegBenchmarkSpread()) {
				noLegs.set(new LegBenchmarkSpread(noLegs.getLegBenchmarkSpread().getValue()));
			}
			if (noLegs.isSetLegInterestAccrualDate()) {
				noLegs.set(new LegInterestAccrualDate(noLegs.getLegInterestAccrualDate().getValue()));
			}
			if (noLegs.isSetLegInterestAccrualResetFrequency()) {
				noLegs.set(
						new LegInterestAccrualResetFrequency(noLegs.getLegInterestAccrualResetFrequency().getValue()));
			}
			if (noLegs.isSetLegInterestAccrualMethod()) {
				noLegs.set(new LegInterestAccrualMethod(noLegs.getLegInterestAccrualMethod().getValue()));
			}
			remessage.addGroup(noLegs);
		}
		imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs = new imix.imix10.ExecutionReport.NoPartyIDs();
		PartyID partyID = new PartyID();
		PartyRole partyRole = new PartyRole();
		imix.imix10.ExecutionReport.NoPartyIDs.NoContactInfos noContactInfos = new imix.imix10.ExecutionReport.NoPartyIDs.NoContactInfos();
		imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs();
		PartySubID partySubID = new PartySubID();
		PartySubIDType partySubIDType = new PartySubIDType();
		int NoPartyIDs = message.isSetNoPartyIDs() ? message.getNoPartyIDs().getValue() : 0;
		for (int j = 1; j <= NoPartyIDs; j++) {
			message.getGroup(j, noPartyIDs);
			partyID = noPartyIDs.get(partyID);
			partyRole = noPartyIDs.get(partyRole);
			if (noPartyIDs.isSetPartyID()) {
				noPartyIDs.set(new PartyID(partyID.getValue()));
			}
			if (noPartyIDs.isSetPartyRole()) {
				noPartyIDs.set(new PartyRole(partyRole.getValue()));
			}
			remessage.addGroup(noPartyIDs);
			int NoContactInfos = noPartyIDs.isSetNoContactInfos() ? noPartyIDs.getNoContactInfos().getValue() : 0;
			for (int k = 1; k <= NoContactInfos; k++) {
				noPartyIDs.getGroup(k, noContactInfos);
				if (noContactInfos.isSetContactInfoID()) {
					noContactInfos.set(new ContactInfoID(noContactInfos.getContactInfoID().getValue()));
				}
				if (noContactInfos.isSetContactInfoIDType()) {
					noContactInfos.set(new ContactInfoIDType(noContactInfos.getContactInfoIDType().getValue()));
				}
				noPartyIDs.addGroup(noContactInfos);
			}

			int NoPartySubIDs = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.getNoPartySubIDs().getValue() : 0;
			for (int l = 1; l <= NoPartySubIDs; l++) {
				noPartyIDs.getGroup(l, noPartySubIDs);
				partySubID = noPartySubIDs.getPartySubID();
				partySubIDType = noPartySubIDs.getPartySubIDType();
				if (noPartySubIDs.isSetPartySubID()) {
					noPartySubIDs.set(new PartySubID(partySubID.getValue()));
				}
				if (noPartySubIDs.isSetPartySubIDType()) {
					noPartySubIDs.set(new PartySubIDType(partySubIDType.getValue()));
				}
				noPartyIDs.addGroup(noPartySubIDs);
			}
		}
	}

}
