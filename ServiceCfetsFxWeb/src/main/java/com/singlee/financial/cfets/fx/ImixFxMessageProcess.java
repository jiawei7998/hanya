package com.singlee.financial.cfets.fx;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.field.ExecType;
import imix.field.MsgType;
import imix.field.SendingTime;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Date;

/**
 * CFETS消息线程序处理
 *
 * @author chenxh
 */
public class ImixFxMessageProcess extends Thread {

    // Cfets消息对象
    private ExecutionReport message;
    // Cfets接口服务对象
    private ImixFxCfetsApiService imixFxCfetsApiService;
    // Cfets消息线程处理实现类
    private ImixFxMessageProcessImpl imixFxMessageProcessImpl;
    // CFETS配置信息
    private ConfigBean configBean;

    private static Logger LogManager = Logger.getLogger(ImixFxMessageProcess.class);

    public ImixFxMessageProcess() {
    }

    public ImixFxMessageProcess(ExecutionReport message, ImixFxCfetsApiService imixFxCfetsApiService,
                                ConfigBean configBean) {
        this.message = message;
        this.imixFxCfetsApiService = imixFxCfetsApiService;
        this.configBean = configBean;
        imixFxMessageProcessImpl = new ImixFxMessageProcessImpl(configBean);
    }

    @Override
    public void run() {
        String filePath = null;
        CfetsPackMsgBean cfetsPackMsgBean = builder(message);
        try {
            //解析报文并发送前置【CSTP】=>【前置】
            TradeBaseBean tradeBean = imixFxMessageProcessImpl.parseMsg(message, cfetsPackMsgBean.getMarketindicator());
            imixFxMessageProcessImpl.sendToCapital(cfetsPackMsgBean, tradeBean, imixFxCfetsApiService);
            // 保存CFETS消息到XML文件中
            if (configBean.isSaveFile()) {
                LogManager.debug("开始保存文件.............");
                filePath = configBean.getSaveFilePath() + File.separator + ConfigBean.dataPath + File.separator
                        + String.format("%tF", new Date()) + File.separator + cfetsPackMsgBean.getExecid() + ConfigBean.txtSuffix;
                CfetsUtils.saveFile(cfetsPackMsgBean.getPackmsg(), filePath, configBean.getCharset());
            }
            // 发送确认消息到CFETS
            ImixFxClient.sendMessage(message, cfetsPackMsgBean.getExecid(), ConfigBean.ExecAckStatus_Accept_Confirm);
        } catch (Exception e) {
            LogManager.error("CFETS 线程处理异常!", e);
            try {
                ImixFxClient.sendMessage(message, cfetsPackMsgBean.getExecid(), ConfigBean.ExecAckStatus_Reject_Confirm);
            } catch (Exception e1) {
                LogManager.error("发送拒收交易[" + cfetsPackMsgBean.getExecid() + "]到CFETS服务器异常", e);
            }
        }
    }

    /**
     * 报文报文
     *
     * @param message
     * @return
     */
    CfetsPackMsgBean builder(ExecutionReport message) {
        // 将CFETS消息转换为字符串
        CfetsPackMsgBean cfetsPackMsg = null;
        try {
            //申明对象
            cfetsPackMsg = new CfetsPackMsgBean();
            // 成交单号
            String execID = message.isSetExecID() ? message.getExecID().getValue() : "";
            cfetsPackMsg.setExecid(execID);
            // 成交日期+时间
            String tradeDate = message.isSetField(TradeDate.FIELD) ? message.getField(new TradeDate()).getValue() : message.getHeader().getField(new SendingTime()).getValue();
            cfetsPackMsg.setTradedate(tradeDate);
            // 发送时间
            cfetsPackMsg.setBeginstring(message.getHeader().getString(SendingTime.FIELD));
            // 报文类型
            String dealStatus = message.isSetDealTransType() ? String.valueOf(message.getDealTransType().getValue()) : ExecType.TRADE;
            cfetsPackMsg.setDealtranstype(dealStatus);
            // 市场标识
            String marketIndicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
            cfetsPackMsg.setMarketindicator(marketIndicator);
            // 消息类型
            String msgType = message.getHeader().getString(MsgType.FIELD);
            cfetsPackMsg.setMsgtype(msgType);
            // 报文
            cfetsPackMsg.setPackmsg(message.toString());
            LogManager.info("ExecutionReport Received marketIndicator:" + marketIndicator + ", execID:" + execID);
            return cfetsPackMsg;
        } catch (Exception e) {
            LogManager.error("转换保存对象失败异常", e);
        }
        return cfetsPackMsg;
    }
}
