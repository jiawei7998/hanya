package com.singlee.financial.cfets.Util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
@Component("springGetBeanByClass")
public class SpringGetBeanByClass implements ApplicationContextAware{

	private  static ApplicationContext applicationContext = null;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringGetBeanByClass.applicationContext = applicationContext;
	}
	
	 public static ApplicationContext getApplicationContext(){
	        return applicationContext;
	 }
	 
	 public static <T> T getBean(Class<T> clazz){
	      return applicationContext.getBean(clazz);
	 }
}
