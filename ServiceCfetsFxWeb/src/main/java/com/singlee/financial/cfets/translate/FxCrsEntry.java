package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.fx.ImixFxMessageHandler;
import com.singlee.financial.pojo.SwapCrsBean;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.trade.CcsDetailBean;
import com.singlee.financial.pojo.trade.CcsInfoBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.field.LegPriceType;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;

/**
 * 外币货币互换下行
 */
public class FxCrsEntry extends FxSettleEntry implements ImixFxMessageHandler<SwapCrsBean> {


    /**
     * 方法已重载.转换外币利率互换业务
     *
     * @param msg         CFETS报文对象
     * @param swapCrsBean 货币互换交易业务对象
     */
    @Override
    public void convert(ExecutionReport msg, SwapCrsBean swapCrsBean, String bankId) throws Exception {
        // 设置本方机构及清算信息 近端和远端清算信息
        convert(msg, bankId, SettPartyEnum.TheParty, swapCrsBean.getInstitutionInfo(), swapCrsBean.getSettlement(), swapCrsBean.getFarSettlement());
        // 设置对手方机构及清算信息 近端和远端清算信息
        convert(msg, bankId, SettPartyEnum.CounterParty, swapCrsBean.getContraPatryInstitutionInfo(), swapCrsBean.getCounterPartySettlement(), swapCrsBean.getFarCounterPartySettlement());
        // 基本信息
        CcsInfoBean crs = new CcsInfoBean();
        swapCrsBean.setCcsInfoBean(crs);
        //交易号
        swapCrsBean.setExecId(msg.isSetExecID() ? msg.getExecID().getValue() : "");
        // 设置本方nolegs信息
        convert(msg, 1, swapCrsBean.getCcsDetailBean(), swapCrsBean.getSettlement().getMarkerTakerRole());
        // 设置对方nolegs信息
        convert(msg, 2, swapCrsBean.getContraPatryCcsDetailBean(), swapCrsBean.getSettlement().getMarkerTakerRole());
        //买卖方向 需要先解析清算信息
        swapCrsBean.setPs(convert(msg.isSetSide() ? msg.getSide() : null, swapCrsBean.getSettlement().getMarkerTakerRole()));
        //交易货币15
        crs.setCurrency(msg.isSetCurrency() ? msg.getCurrency().getValue() : "");
        //成交编号17
        crs.setExecId(msg.isSetExecID() ? msg.getExecID().getValue() : "");
        //成交状态10105
        crs.setDealTransType(String.valueOf(msg.isSetDealTransType() ? msg.getDealTransType().getValue() : ""));
        //交易货币金额32
        crs.setLastQty(msg.isSetLastQty() ? msg.getLastQty().getValue() : "");
        //交易方向B-固定值
        crs.setSide(msg.isSetSide() ? msg.getSide().getUnionTypeValue() : null);
        //报文生成时间60
        crs.setTransactTime(msg.isSetTransactTime() ? msg.getTransactTime().getValue() : "");
        //交易期限63
        crs.setSettlType(msg.isSetSettlType() ? msg.getSettlType().getValue() : "");
        //交易生效日64
        crs.setSettlDate(msg.isSetSettlDate() ? msg.getSettlDate().getValue() : "");
        //交易日75
        crs.setTradeDate(msg.isSetTradeDate() ? msg.getTradeDate().getValue() : "");
        //结算币种120
        crs.setSettlCurrency(msg.isSetSettlCurrency() ? msg.getSettlCurrency().getValue() : "");
        //即期汇率194
        crs.setLastSpotRate(msg.isSetLastSpotRate() ? msg.getLastSpotRate().getValue() : "");
        //清算方式430
        crs.setNetGrossInd(String.valueOf(msg.isSetNetGrossInd() ? msg.getNetGrossInd().getValue() : ""));
        //终止日541
        crs.setMaturityDate(msg.isSetMaturityDate() ? msg.getMaturityDate().getValue() : "");
        //货币交易量1056
        crs.setCalculatedCcyLastQty(msg.isSetCalculatedCcyLastQty() ? msg.getCalculatedCcyLastQty().getValue() : "");
        //10064
        crs.setCurrency1(msg.isSetCurrency1() ? msg.getCurrency1().getValue() : "");
        //10065
        crs.setCurrency2(msg.isSetCurrency2() ? msg.getCurrency2().getValue() : "");
        crs.setRiskLastQty(msg.isSetRiskLastQty() ? msg.getRiskLastQty().getValue() : "");
        crs.setTradeInstrument(String.valueOf(msg.isSetTradeInstrument() ? msg.getTradeInstrument().getValue() : ""));
        crs.setTradeMethod(String.valueOf(msg.isSetTradeMethod() ? msg.getTradeMethod().getValue() : ""));
        crs.setTradeTime(msg.isSetTradeTime() ? msg.getTradeTime().getValue() : "");
        crs.setMarketIndicator(msg.isSetMarketIndicator() ? msg.getMarketIndicator().getValue() : "");
        crs.setNotionalExchangeType(String.valueOf(msg.isSetNotionalExchangeType() ? msg.getNotionalExchangeType().getValue() : ""));
        crs.setMaturityDateType(msg.isSetMaturityDateType() ? msg.getMaturityDateType().getValue() : "");
        crs.setCfetsCnfmIndicator(String.valueOf(msg.isSetCFETSCnfmIndicator() ? msg.getCFETSCnfmIndicator().getValue() : ""));
        crs.setDateAdjustmentIndic(msg.isSetField(11882) ? msg.getString(11882) : "");
        crs.setIniExDate(msg.isSetIniExDate() ? msg.getIniExDate().getValue() : "");
        crs.setFinalExDate(msg.isSetFinalExDate() ? msg.getFinalExDate().getValue() : "");
    }

    /**
     * 货币互换 nolegs处理
     *
     * @param message
     * @param i
     * @param ccsDetailBean
     * @param markerTakerRole
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, int i, CcsDetailBean ccsDetailBean, MarkerTakerEnum markerTakerRole) throws FieldNotFound, FieldConvertError, Exception {
        ExecutionReport.NoLegs nolegs = new ExecutionReport.NoLegs();
        message.getGroup(i, nolegs);
        // 3-固定值
        int legPriceType = nolegs.getLegPriceType().getValue();
        ccsDetailBean.setLegPriceType(legPriceType);
        /***
         * legPriceType 取 3 表示交易端为固定端，取 6 表示交易端为浮动端
         */
        ccsDetailBean.setLegPriceType(new Integer(nolegs.isSetLegPriceType() ? String.valueOf(nolegs.getLegPriceType().getValue()) : "0"));
        //交易方向1 卖出并收取利息 4 买入并支付利息
        ccsDetailBean.setLegSide(nolegs.isSetLegSide() ? String.valueOf(nolegs.getLegSide().getValue()) : "");
        //转换买卖方向
        PsEnum ps = convert(message.isSetSide() ? message.getSide() : null, nolegs.getLegSide(), markerTakerRole);
        ccsDetailBean.setLegPs(ps == null ? "" : ps.toString());
        //浮动利率lastpx表示利差 BP 固定利率表示利率 %
        ccsDetailBean.setLegLastPx(nolegs.isSetLegLastPx() ? new BigDecimal(String.valueOf(nolegs.getLegLastPx().getValue())) : null);
        //参考利率曲线
        ccsDetailBean.setLegBenchmarkCurveName(nolegs.isSetLegBenchmarkCurveName() ? String.valueOf(nolegs.getLegBenchmarkCurveName().getValue()) : "");
        if (LegPriceType.SPREAD == ccsDetailBean.getLegPriceType()) {
            ccsDetailBean.setLegPrice(null);
            ccsDetailBean.setLegBenchmarkSpread(ccsDetailBean.getLegLastPx());
        } else {
            ccsDetailBean.setLegPrice(ccsDetailBean.getLegLastPx());
            ccsDetailBean.setLegBenchmarkSpread(null);
        }
        //名义本金
        ccsDetailBean.setLegOrderQty(new BigDecimal(nolegs.isSetLegOrderQty() ? String.valueOf(nolegs.getLegOrderQty().getValue()) : "0"));
        //币种
        ccsDetailBean.setLegCurrency(nolegs.isSetLegCurrency() ? String.valueOf(nolegs.getLegCurrency().getValue()) : "");
        //询价关联
        ccsDetailBean.setLegSolveForIndicator(nolegs.isSetLegSolveForIndicator() ? String.valueOf(nolegs.getLegSolveForIndicator().getValue()) : "");
        //计息基准
        ccsDetailBean.setLegDayCount(nolegs.isSetLegDayCount() ? String.valueOf(nolegs.getLegDayCount().getValue()) : "");
        //付息频率
        ccsDetailBean.setLegCouponPaymentFrequency(nolegs.isSetLegCouponPaymentFrequency() ? String.valueOf(nolegs.getLegCouponPaymentFrequency().getValue()) : "");
        //付息日调整规则
        ccsDetailBean.setLegCouponPaymentDateReset(nolegs.isSetLegCouponPaymentDateReset() ? String.valueOf(nolegs.getLegCouponPaymentDateReset().getValue()) : "");
        //定息日调整规则
        ccsDetailBean.setLegInterestAccrualResetDateAdjust(nolegs.isSetLegInterestAccrualResetDateAdjust() ? String.valueOf(nolegs.getLegInterestAccrualResetDateAdjust().getValue()) : "");
        //定息周期
        ccsDetailBean.setLegInterestAccrualResetFrequency(nolegs.isSetLegInterestAccrualResetFrequency() ? String.valueOf(nolegs.getLegInterestAccrualResetFrequency().getValue()) : "");
        //定息规则
        ccsDetailBean.setLegInterestFixDateAdjustment(nolegs.isSetLegInterestFixDateAdjustment() ? String.valueOf(nolegs.getLegInterestFixDateAdjustment().getValue()) : "");
        //参考利率期限
        ccsDetailBean.setLegBenchmarkTenor(nolegs.isSetLegBenchmarkTenor() ? String.valueOf(nolegs.getLegBenchmarkTenor().getValue()) : "");
        // 首期支付日
        ccsDetailBean.setLegFirstPayDate(nolegs.isSetLegCouponPaymentDate() ? nolegs.getLegCouponPaymentDate().getValue() : null);
        // 残端标识
        ccsDetailBean.setLegStubIndicator(nolegs.isSetLegStubIndicator() ? nolegs.getLegStubIndicator().getValue() : 0);
        //付息计划
        ccsDetailBean.setIntPaymentPlans(convert(message, nolegs));
        //如果获取不到首次付息日则从付息计划中获取
        if (ccsDetailBean.getLegFirstPayDate() == null && ccsDetailBean.getIntPaymentPlans() != null && ccsDetailBean.getIntPaymentPlans().size() > 0) {
            ccsDetailBean.setLegFirstPayDate(ccsDetailBean.getIntPaymentPlans().get(0).getIntPayDate());
        }
    }
}
