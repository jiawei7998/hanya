package com.singlee.financial.cfets.translate;

import com.singlee.financial.pojo.component.*;
import com.singlee.financial.pojo.trade.IntPaymentPlan;
import imix.field.*;
import imix.imix10.ExecutionReport;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 外汇市场基础转换方法
 */
public class FxBaseEntry {

    static Logger logger = Logger.getLogger(FxBaseEntry.class);

    /***
     * 将期权币种组件的看涨看跌方向枚举值  转换为期权交易的的看涨看跌方向的枚举值
     * @param currencyAmtType
     * @return
     */
    public String convert(int currencyAmtType) {
        switch (currencyAmtType) {
            case CurrencyAmtType.PUT:
                return String.valueOf(PutOrCall.PUT);
            case CurrencyAmtType.CALL:
                return String.valueOf(PutOrCall.CALL);
            default:
                return "";
        }
    }

    /**
     * 方法已重载.转换清算方式信息
     *
     * @param netGrossInd Cfets清算方式
     * @return
     */
    public SettMode convert(NetGrossInd netGrossInd) {
        SettMode settMode = null;
        if (null == netGrossInd) {
            return SettMode.Netting;//xqq修改默认净额清算，防止前置平台转换出现空指针
//			return null;
        }
        switch (netGrossInd.getValue()) {
            case NetGrossInd.NET_NEGOTIATION:
                // 1-双边净额清算
                settMode = SettMode.Netting;
                break;
            case NetGrossInd.GROSS:
                // 2-双边全额清算
                settMode = SettMode.Normal;
                break;
            case NetGrossInd.NET_MATCH:
                // 3-集中清算
                settMode = SettMode.Central;
                break;
            case NetGrossInd.NET_AND_GROSS:
                // 4-应急录入
                settMode = SettMode.SGE;
                break;
            default:
                break;
        }

        return settMode;
    }

    /**
     * 方法已重载.转换交易模式信息
     *
     * @param dayCount Cfets交易模式
     * @return
     */
    public BasisEnum convert(DayCount dayCount) {
        BasisEnum basisEnum = null;
        if (null == dayCount) {
            return null;
        }
        if ("0".equalsIgnoreCase(dayCount.getValue() + "")) {
            // 1-询价
            basisEnum = BasisEnum.ACTUAL;
        } else if ("1".equalsIgnoreCase(dayCount.getValue() + "")) {
            // 2-竟价
            basisEnum = BasisEnum.A360;
        } else if ("3".equalsIgnoreCase(dayCount.getValue() + "")) {
            // 9-摄合
            basisEnum = BasisEnum.A365;
        }
        return basisEnum;
    }

    /**
     * 方法已重载.转换交易模式信息
     *
     * @param tradingMode Cfets交易模式
     * @return
     */
    public DealModeEnum convert(TradingMode tradingMode) {
        DealModeEnum dealMode = null;
        if (null == tradingMode) {
            return null;
        }
        if ("1".equalsIgnoreCase(tradingMode.getValue())) {
            // 1-询价
            dealMode = DealModeEnum.Enquiry;
        } else if ("2".equalsIgnoreCase(tradingMode.getValue())) {
            // 2-竟价
            dealMode = DealModeEnum.Competition;
        } else if ("9".equalsIgnoreCase(tradingMode.getValue())) {
            // 9-摄合
            dealMode = DealModeEnum.Married;
        }
        return dealMode;
    }

    /**
     * 方法已重载.转换交易方式信息
     *
     * @param tradeMethod Cfets交易模式
     * @return
     */
    public TradeMethodEnum convert(TradeMethod tradeMethod) {
        TradeMethodEnum tradeType = null;

        if (null == tradeMethod) {
            return null;
        }
        switch (tradeMethod.getValue()) {
            case 1:
                tradeType = TradeMethodEnum.Negotiate;
                break;
            case 3:
                tradeType = TradeMethodEnum.Matching;
                break;
            case 4:
                tradeType = TradeMethodEnum.RFQ;
                break;
            case 8:
                tradeType = TradeMethodEnum.ESP;
                break;
            case 10:
                tradeType = TradeMethodEnum.RFQ_NDM;
                break;
            default:
                break;
        }

        return tradeType;
    }

    /**
     * 方法已重载.转换交易方式信息
     *
     * @param tradeInstrument Cfets交易模型
     * @return
     */
    public TradeInstrumentEnum convert(TradeInstrument tradeInstrument) {
        TradeInstrumentEnum tradeType = null;
        if (null == tradeInstrument) {
            return null;
        }
        switch (tradeInstrument.getValue()) {
            case 'Q':
                tradeType = TradeInstrumentEnum.QDM;
                break;
            case 'O':
                tradeType = TradeInstrumentEnum.ODM;
                break;
            case 'N':
                tradeType = TradeInstrumentEnum.NDM;
                break;
            case (char) 49:
                //1
                tradeType = TradeInstrumentEnum.FX_BILATERAL;
                break;
            case (char) 50:
                //2
                tradeType = TradeInstrumentEnum.FX_CROSS;
                break;
            case (char) 54:
                //6
                tradeType = TradeInstrumentEnum.GOLD_BILATERAL;
                break;
            case (char) 57:
                //9
                tradeType = TradeInstrumentEnum.FX_Matching;
                break;
            default:
                break;
        }

        return tradeType;
    }

    /**
     * 方法已重载.转换交易状态信息
     *
     * @param dealTransType Cfets交易状态
     * @return
     */
    public TransactionStatusEnum convert(DealTransType dealTransType) {
        TransactionStatusEnum transactionStatus = null;

        if (null == dealTransType) {
            return null;
        }

        switch (dealTransType.getValue()) {
            case (char) 48:
                // 0-录入
                transactionStatus = TransactionStatusEnum.Entry;
                break;
            case (char) 49:
                // 1-修改
                transactionStatus = TransactionStatusEnum.Modify;
                break;
            case (char) 70:
                // F-应急录入
                transactionStatus = TransactionStatusEnum.EmergencyEntry;
                break;
            case (char) 71:
                // G-应急删除
                transactionStatus = TransactionStatusEnum.EmergencyDelete;
                break;
            default:
                break;
        }

        return transactionStatus;
    }

    /**
     * 方法已重载.转换买卖方向
     *
     * @param side        Cfets买卖方向
     * @param markerTaker 发起方/接收方标识
     * @return
     */
    public PsEnum convert(Side side, MarkerTakerEnum markerTaker) {

        if (null == side) {
            return null;
        }
        // 买卖方向,根据taker、maker是否是银行自已,再加上Cfets中的买卖方向,转换成买卖方向的枚举值
        if ("1".equals(side.getUnionTypeValue()) || "H".equalsIgnoreCase(side.getUnionTypeValue()) || "B".equalsIgnoreCase(side.getUnionTypeValue())) {
            return MarkerTakerEnum.Taker.equals(markerTaker) ? PsEnum.P : PsEnum.S;
        } else if ("4".equals(side.getUnionTypeValue()) || "I".equalsIgnoreCase(side.getUnionTypeValue()) || "B".equalsIgnoreCase(side.getUnionTypeValue())) {
            return MarkerTakerEnum.Taker.equals(markerTaker) ? PsEnum.S : PsEnum.P;
        } else {
            return PsEnum.X;
        }
    }

    /**
     * 方法已重载.转换买卖方向
     *
     * @param side        Cfets买卖方向
     * @param markerTaker 发起方/接收方标识
     * @return
     */
    public PsEnum convert(Side side, LegSide legSide, MarkerTakerEnum markerTaker) {

        if (null == side) {
            return null;
        }
        // 买卖方向,根据taker、maker是否是银行自已,再加上Cfets中的买卖方向,转换成买卖方向的枚举值
        if ("1".equals(side.getUnionTypeValue()) || "H".equalsIgnoreCase(side.getUnionTypeValue()) || "B".equalsIgnoreCase(side.getUnionTypeValue())) {
            if (null != legSide && "1".equalsIgnoreCase(legSide.getUnionTypeValue())) {
                return MarkerTakerEnum.Taker.equals(markerTaker) ? PsEnum.P : PsEnum.S;
            } else if (null != legSide && "4".equalsIgnoreCase(legSide.getUnionTypeValue())) {
                return MarkerTakerEnum.Taker.equals(markerTaker) ? PsEnum.S : PsEnum.P;
            }
            return MarkerTakerEnum.Taker.equals(markerTaker) ? PsEnum.P : PsEnum.S;
        } else if ("4".equals(side.getUnionTypeValue()) || "I".equalsIgnoreCase(side.getUnionTypeValue())) {
            return MarkerTakerEnum.Taker.equals(markerTaker) ? PsEnum.S : PsEnum.P;
        } else {
            return PsEnum.X;
        }
    }

    /**
     * xqq:根据基准货币与交易货币比较判断判断清算信息。
     *
     * @param psEnum    方向
     * @param currency  市场类型
     * @param currency1 交易货币
     * @param side      基准货币
     * @param taker
     * @return
     */
    public PsEnum convert(PsEnum psEnum, String currency, String currency1, Side side, MarkerTakerEnum taker) {
        if (psEnum == null) {
            return null;
        }
        if (currency == null || currency1 == null) {
            return psEnum;
        }
        if (side == null) {
            return psEnum;
        }
        if (MarkerTakerEnum.Taker.equals(taker)) {
            if (currency.equals(currency1)) {
                if ("1".equals(side.getUnionTypeValue()) || "H".equals(side.getUnionTypeValue())) {
                    psEnum = PsEnum.P;
                } else {
                    psEnum = PsEnum.S;
                }
            } else {
                if ("1".equals(side.getUnionTypeValue()) || "H".equals(side.getUnionTypeValue())) {
                    psEnum = PsEnum.S;
                } else {
                    psEnum = PsEnum.P;
                }
            }
        } else {
            if (currency.equals(currency1)) {
                if ("1".equals(side.getUnionTypeValue()) || "H".equals(side.getUnionTypeValue())) {
                    psEnum = PsEnum.S;
                } else {
                    psEnum = PsEnum.P;
                }
            } else {
                if ("1".equals(side.getUnionTypeValue()) || "H".equals(side.getUnionTypeValue())) {
                    psEnum = PsEnum.P;
                } else {
                    psEnum = PsEnum.S;
                }
            }
        }
        return psEnum;
    }

    /**
     * 根据交易货币与leg的方向和leg的币种判断判断清算信息。
     *
     * @param psEnum   方向
     * @param currency 市场类型
     * @param legCcy   交易货币
     * @param legSide  基准货币
     * @param taker
     * @return
     */
    public PsEnum convert(PsEnum psEnum, String currency, String legCcy, String legSide, MarkerTakerEnum taker) {
        if (psEnum == null) {
            return null;
        }
        if (currency == null) {
            return psEnum;
        }
        if (legSide == null) {
            return psEnum;
        }
        if (MarkerTakerEnum.Taker.equals(taker)) {//本方是发起方
            if (currency.equals(legCcy)) {
                if ("4".equals(legSide)) {//支付该货币利息
                    psEnum = PsEnum.S;
                } else {
                    psEnum = PsEnum.P;
                }
            } else {
                if ("4".equals(legSide)) {//收取利息
                    psEnum = PsEnum.P;
                } else {
                    psEnum = PsEnum.S;
                }
            }
        } else {//本方不是发起方
            if (currency.equals(legCcy)) {
                if ("4".equals(legSide)) {
                    psEnum = PsEnum.P;
                } else {
                    psEnum = PsEnum.S;
                }
            } else {
                if ("4".equals(legSide)) {
                    psEnum = PsEnum.S;
                } else {
                    psEnum = PsEnum.P;
                }
            }
        }
        return psEnum;
    }

    /**
     * 转换excel类型
     *
     * @param execType
     * @return
     */
    public ExecStatus convert(ExecType execType) {
        if (null == execType) {
            return null;
        }
        if ("F".equals(execType.getValue())) {
            return ExecStatus.Finish;
        } else if ("4".equals(execType.getValue())) {
            return ExecStatus.Revoke;
        } else if ("5".equals(execType.getValue())) {
            return ExecStatus.Modify;
        } else if ("101".equals(execType.getValue())) {
            return ExecStatus.Emergency;
        }
        return null;
    }

    /**
     * 转换即远掉
     *
     * @param settlType
     * @return
     */
    public String convert(String settlType) {
        if (null == settlType) {
            return "-1";
        }
        if (StringUtils.equals(StringUtils.trimToEmpty(settlType), SettlType.LEGSETTLTYPE_VALUE_TOM)) {
            return "1";
        } else if (StringUtils.equals(StringUtils.trimToEmpty(settlType), SettlType.LEGSETTLTYPE_VALUE_TODAY)) {
            return "0";
        } else {
            return "2";
        }
    }

    /**
     * 处理互换交易的计划
     *
     * @param message
     * @param nolegs
     * @return
     * @throws Exception
     */
    List<IntPaymentPlan> convert(ExecutionReport message, imix.imix10.ExecutionReport.NoLegs nolegs) throws Exception {
        List<IntPaymentPlan> list = new ArrayList<IntPaymentPlan>();
        //自定义付息组件
        int noPayment = nolegs.isSetNoPayment() ? nolegs.getNoPayment().getValue() : 0;
        IntPaymentPlan plan = null;
        ExecutionReport.NoLegs.NoPayment legPayments = null;
        for (int j = 1; j <= noPayment; j++) {
            legPayments = new ExecutionReport.NoLegs.NoPayment();
            nolegs.getGroup(j, legPayments);

            plan = new IntPaymentPlan();
            //交易号
            plan.setExecId(message.isSetExecID() ? message.getExecID().getValue() : "");
            //支付方向
            plan.setLegSide(nolegs.isSetLegSide() ? String.valueOf(nolegs.getLegSide().getValue()) : "");
            //序列号
            plan.setSeq(j);
            //本金摊销金额
            plan.setAmorPaymentAmt(legPayments.isSetPaymentAmt() ? new BigDecimal(legPayments.getPaymentAmt().getValue()) : null);
            //币种
            plan.setAmorPaymentCcy(legPayments.isSetPaymentCurrency() ? legPayments.getPaymentCurrency().getValue() : null);
            //付息日期
            plan.setIntPayDate(legPayments.isSetPayDate() ? legPayments.getPayDate().getValue() : null);
            //计息开始日期
            plan.setIntSettlDate(legPayments.isSetSettlDate() ? legPayments.getSettlDate().getValue() : null);

            list.add(plan);
        }// end for
        Collections.sort(list);//升序
        return list;
    }
}