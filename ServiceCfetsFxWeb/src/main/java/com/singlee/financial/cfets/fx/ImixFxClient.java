package com.singlee.financial.cfets.fx;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.util.CfetsUtils;
import imix.DataDictionary;
import imix.Message;
import imix.client.core.ImixApplication;
import imix.client.core.ImixSession;
import imix.imix10.Confirmation;
import imix.imix10.ExecutionAcknowledgement;
import imix.imix10.ExecutionReport;
import imix.protocol.version.Version;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * CFETS监听客户端管理类
 *
 * @author chenxh
 */
public class ImixFxClient {

    // CFETS配置类
    private ConfigBean configBean;

    // CFETS监听事件类
    private ImixFxClientListener listener;

    // CFETS 初始化
    private ImixSession imixSession = null;

    private static Logger LogManager = Logger.getLogger(ImixFxClient.class);

    /**
     * 初始化CFETS服务,登录CFETS服务器
     *
     * @return
     * @throws Exception
     */
    public boolean initLogin() throws Exception {
        // 是否登录
        boolean isLogin = false;
        try {
            LogManager.debug("configBean:" + configBean);
            String configFilePath = ImixFxClient.class.getResource("/").getFile();
            LogManager.debug("init configfile:" + configFilePath);

            configFilePath = CfetsUtils.updateConfigFile(configBean.getUpdatePathKey(), configFilePath,
                    ConfigBean.cfgClientPath, configBean.getCharset());
            LogManager.debug("update configfile:" + configFilePath);

            ImixApplication.initialize(listener, configFilePath);

            LogManager.info("USERNAME[" + this.configBean.getUserName() + "]PASSWORD[" + this.configBean.getPassword() + "]MARKET[" + this.configBean.getMarket() + "]");
            imixSession = new ImixSession(this.configBean.getUserName(), this.configBean.getPassword(), this.configBean.getMarket());
            isLogin = imixSession.start();
            LogManager.info("CFETS FX CSTP LOGIN:[ isLogin:" + isLogin + "]***********************************");
            LogManager.info("***************IMIXProtocol****************************");
            LogManager.info("The Version of IMIX Protocol is :" + Version.getVersion());
            LogManager.info("Building Time is :" + Version.getBuildingTime());
            LogManager.info("*********************************************************");
            // 本地测试
            // testMessage();
        } catch (Exception e) {
            LogManager.error("CFETS FX CSTP LOGIN Exception:[" + e.getMessage() + "]", e);
        }
        return isLogin;
    }

    /**
     * 方法已重载.发送信息到CFETS服务器
     *
     * @param execID
     * @param execAckStatus
     * @return
     */
    public static boolean sendMessage(ExecutionReport executionReport, String execID, int execAckStatus) {
        return sendMessage(executionReport, generateMsg(execID, null, execAckStatus));
    }

    /**
     * 方法已重载.发送信息到CFETS服务器
     *
     * @param message
     * @return
     */
    public static boolean sendMessage(ExecutionReport executionReport, Message message) {
        return ImixSession.lookupIMIXSession(executionReport).send(message);
    }

    public boolean sendMessage(Confirmation message) {
        return imixSession.send(message);
    }

    /**
     * 创建CFETS消息类
     *
     * @param execID
     * @param messageEncoding
     * @param execAckStatus
     * @return
     */
    private static Message generateMsg(String execID, String messageEncoding, int execAckStatus) {
        ExecutionAcknowledgement msg = new ExecutionAcknowledgement();
        msg.setString(17, execID);
        if (messageEncoding == null) {
            messageEncoding = "UTF-8";
        }
        msg.getHeader().setString(347, messageEncoding);
        msg.setChar(1036, (char) (execAckStatus + 49));
        return msg;
    }

    public ConfigBean getConfigBean() {
        return configBean;
    }

    public void setConfigBean(ConfigBean configBean) {
        this.configBean = configBean;
    }

    public ImixFxClientListener getListener() {
        return listener;
    }

    public void setListener(ImixFxClientListener listener) {
        this.listener = listener;
    }

    public ImixSession getImixSession() {
        return imixSession;
    }

    /**
     * 本地测试
     *
     * @throws Exception
     */
    public void testMessage() throws Exception {

        BufferedReader reader = null;
        String msgFilePath = "C:\\Singlee\\cfets\\fx\\Data\\2018-05-29\\0000-0000-0762-10.txt";
        // msgFilePath = "C:\\Users\\chenxh\\Desktop\\0000-0000-0761-F2.txt";
        StringBuffer sbf = null;
        try {
            sbf = new StringBuffer();
            // log.debug("read msgFilePath :" + msgFilePath);
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(msgFilePath)), "UTF-8"));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sbf.append(line);
            }
            reader.close();

            ExecutionReport e = new ExecutionReport();
            e.fromString(sbf.toString(), new DataDictionary("IMIX10.xml"), false);
            listener.onMessage(e);

        } catch (Exception e) {
            throw e;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

    }

}
