package com.singlee.financial.cfets.fx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.ICfetsImixFx;
import com.singlee.financial.cfets.ICfetsImixDepositsAndLoans;
import com.singlee.financial.cfets.ICfetsImixOption;
import com.singlee.financial.cfets.ICfetsImixSwap;

/**
 * CFETS接口服务
 * 
 * @author chenxh
 * 
 */
@Service
public class ImixFxCfetsApiService {

	@Autowired
	ICfetsImixFx cfetsImixFx;

	@Autowired
	ICfetsImixDepositsAndLoans cfetsImixDepositsAndLoans;

	@Autowired
	ICfetsImixSwap cfetsImixSwap;

	@Autowired
	ICfetsImixOption cfetsImixOtc;

	@Autowired
	ICfetsImixOption cfetsImixOption;

	public ICfetsImixFx getCfetsImixFx() {
		return cfetsImixFx;
	}

	public void setCfetsImixFx(ICfetsImixFx cfetsImixFx) {
		this.cfetsImixFx = cfetsImixFx;
	}

	public ICfetsImixDepositsAndLoans getCfetsImixMM() {
		return cfetsImixDepositsAndLoans;
	}

	public void setCfetsImixMM(ICfetsImixDepositsAndLoans cfetsImixDepositsAndLoans) {
		this.cfetsImixDepositsAndLoans = cfetsImixDepositsAndLoans;
	}

	public ICfetsImixSwap getCfetsImixSwap() {
		return cfetsImixSwap;
	}

	public void setCfetsImixSwap(ICfetsImixSwap cfetsImixSwap) {
		this.cfetsImixSwap = cfetsImixSwap;
	}

	public ICfetsImixOption getCfetsImixOtc() {
		return cfetsImixOtc;
	}

	public void setCfetsImixOtc(ICfetsImixOption cfetsImixOtc) {
		this.cfetsImixOtc = cfetsImixOtc;
	}

}
