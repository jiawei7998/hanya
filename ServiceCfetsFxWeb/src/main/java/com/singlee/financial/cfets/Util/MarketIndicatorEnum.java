package com.singlee.financial.cfets.Util;

import imix.field.MarketIndicator;

/**
 * 
 * @author 代号_47
 *
 */
public enum MarketIndicatorEnum {
	
	FxSpt("外汇即期",MarketIndicator.FXSPT),
	
	FxFwd("外汇远期",MarketIndicator.FXFOW),
	
	FxSwap("外汇掉期",MarketIndicator.FXSWP),
	
	FxGlodSpt("贵金属即期",MarketIndicator.GOLDSPT),
	
	FxGlodFwd("贵金属远期",MarketIndicator.GOLDFWD),
	
	FxGlodSwap("贵金属掉期",MarketIndicator.GOLDSWP),
	
	FXMm("外币拆借",MarketIndicator.INTER_BANK_OFFERING),
	
	FxOpt("外汇期权",MarketIndicator.FXOPT),
	
	FxIrs("外币利率互换",MarketIndicator.FXIRS),
	
	FxCcs("货币互换",MarketIndicator.FXCRS),
	
	FxNdf("外汇NDF交易",MarketIndicator.FXNDF);
	
	MarketIndicatorEnum(String name,String value){
        this.name=name;
        this.value=value;
    }
	
	private String name;
	
	private String value;
	/**
	 * 根据value值获取名字
	 * @param index
	 * @return
	 */
	public static String getName(String index){
        for(MarketIndicatorEnum market :MarketIndicatorEnum.values()){
            if(market.getValue().equals(index)){
                return market.name;
            }
        }
        return  "";
    }
	/**
	 * 根据名字获取value值
	 * @param name
	 * @return
	 */
	public static String getIndexByName(String name){
        for(MarketIndicatorEnum market :MarketIndicatorEnum.values()){
            if(name.equals(market.getName())){
                return market.getValue();
            }
        }
        return "";
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	

}
