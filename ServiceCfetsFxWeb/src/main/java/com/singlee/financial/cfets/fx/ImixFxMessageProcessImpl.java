package com.singlee.financial.cfets.fx;

import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.translate.*;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.*;
import com.singlee.financial.pojo.component.RetStatusEnum;
import com.singlee.financial.pojo.component.SptFwdTypeEnum;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.field.MarketIndicator;
import imix.imix10.ExecutionReport;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * CFETS报文对象处理实体类
 *
 * @author chenxh
 */
public class ImixFxMessageProcessImpl {

    //申明配置文件
    private ConfigBean configBean;
    //日志申明
    private static Logger logger = Logger.getLogger(ImixFxMessageProcessImpl.class);

    //初始化方法
    public ImixFxMessageProcessImpl(ConfigBean configBean) {
        this.configBean = configBean;
    }

    /***
     * 解析方法 可以被其他类调用
     * @param message
     * @param marketIndicator
     * @return
     * @throws Exception
     */
    public TradeBaseBean parseMsg(ExecutionReport message, String marketIndicator) throws Exception {
        TradeBaseBean tradeBaseBean = null;
        ImixFxMessageHandler imixFxMessageHandler = null;
        switch (marketIndicator) {
            //外汇即远期
            case MarketIndicator.FXSPT:
            case MarketIndicator.GOLDSPT:
            case MarketIndicator.FXFOW:
            case MarketIndicator.FXNDF:
            case MarketIndicator.GOLDFWD:
                tradeBaseBean = new FxSptFwdBean();
                imixFxMessageHandler = new FxSptFwdEntry();
                break;
            //外汇掉期
            case MarketIndicator.FXSWP:
            case MarketIndicator.GOLDSWP:
                tradeBaseBean = new FxSwapBean();
                imixFxMessageHandler = new FxSwapEntry();
                break;
            //外币拆借
            case MarketIndicator.INTER_BANK_OFFERING:
                tradeBaseBean = new DepositsAndLoansFxBean();
                imixFxMessageHandler = new FxIboEntry();
                break;
            //期权交易
            case MarketIndicator.FXOPT:
                tradeBaseBean = new FxOptionBean();
                imixFxMessageHandler = new FxOptionEntry();
                break;
            //货币互换
            case MarketIndicator.FXCRS:
                tradeBaseBean = new SwapCrsBean();
                imixFxMessageHandler = new FxCrsEntry();
                break;
            //利率互换
            case MarketIndicator.FXIRS:
                tradeBaseBean = new SwapIrsBean();
                imixFxMessageHandler = new FxIrsEntry();
                break;
            default:
                logger.warn("系统暂未支持的市场标识(marketIndicator):" + marketIndicator + ",不解析此报文");
                String cstpFxFlag = configBean.getCstpFxFlag() == null ? "" : configBean.getCstpFxFlag();
                if ("Y".equals(cstpFxFlag)) {
                    //验收时打开
                    String execid = message.isSetExecID() ? message.getExecID().getValue() : "";
                    Map<String, String> map1 = new HashMap<String, String>();
                    map1.put("marketIndicator", marketIndicator);
                    map1.put("execId", execid);
                    map1.put("msg", "系统暂未支持的市场标识(marketIndicator):" + marketIndicator + ",不解析此报文");
                    saveExcelError(map1);
                }
                throw new Exception("系统暂未支持的市场标识(marketIndicator):" + marketIndicator + ",不解析此报文");
        }
        //解析报文
        imixFxMessageHandler.convert(message, tradeBaseBean, configBean.getBankId());
        logger.debug("返回报文对象：" + tradeBaseBean);

        return tradeBaseBean;
    }

    /**
     * 发送CFETS业务对象对资金前置系统
     */
    void sendToCapital(final CfetsPackMsgBean cfetsPackMsg, final TradeBaseBean tradeBean, ImixFxCfetsApiService imixFxCfetsApiService) {
        new Thread() {
            @Override
            public void run() {
                // 资金前置返回对象
                RetBean retBean = null;
                String saveFilePath = null;
                try {
                    logger.debug("开始发送CFETS交易[" + tradeBean.getExecId() + "]到资金前置系统!");

                    retBean = sendMsg(tradeBean, cfetsPackMsg, imixFxCfetsApiService);

                    logger.debug("资金前置系统返回对象:" + retBean);

                } catch (Exception e) {
                    // 异常时设置反回状态为失败
                    retBean = new RetBean();
                    retBean.setRetStatus(RetStatusEnum.F);
                    logger.error("发送CFETS交易[" + tradeBean.getExecId() + "]到资金前置系统异常!", e);
                } // end try catch
                try {
                    // 以txt形式保存CFETS报文对象
                    if (null == retBean || retBean.getRetStatus().equals(RetStatusEnum.F)) {
                        // 处理失败的记录保存路径
                        saveFilePath = configBean.getSaveFilePath() + File.separator + ConfigBean.errDataPath
                                + File.separator + String.format("%tF", new Date()) + File.separator
                                + tradeBean.getExecId() + ConfigBean.txtSuffix;
                        // 如果处理失败保存到失败目录
                        CfetsUtils.saveFile(cfetsPackMsg.getPackmsg(), saveFilePath, configBean.getCharset());
                    }
                } catch (Exception e) {
                    logger.error("保存发送异常交易[" + tradeBean.getExecId() + "]到交易异常目录!", e);
                } // end try catch
            }// end run
        }.start();
    }

    /****
     * 发送交易方法，可以被其他类调用
     * @param tradeBean
     * @param cfetsPackMsg
     * @return
     * @throws Exception
     */
    public RetBean sendMsg(TradeBaseBean tradeBean, CfetsPackMsgBean cfetsPackMsg, ImixFxCfetsApiService imixFxCfetsApiService) throws Exception {
        RetBean retBean = null;
        if (tradeBean instanceof FxSptFwdBean) {
            retBean = imixFxCfetsApiService.getCfetsImixFx().send((FxSptFwdBean) tradeBean, cfetsPackMsg);
            FxSptFwdBean bean = (FxSptFwdBean) tradeBean;
            if (SptFwdTypeEnum.SPOT.equals(bean.getSptFwdType())) {
                saveExcel(MarketIndicator.FXSPT, tradeBean);
            } else {
                saveExcel(MarketIndicator.FXFOW, tradeBean);
            }
        } else if (tradeBean instanceof FxSwapBean) {
            retBean = imixFxCfetsApiService.getCfetsImixFx().send((FxSwapBean) tradeBean, cfetsPackMsg);
            saveExcel(MarketIndicator.FXSWP, tradeBean);
        } else if (tradeBean instanceof SwapCrsBean) {
            retBean = imixFxCfetsApiService.getCfetsImixSwap().send((SwapCrsBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof DepositsAndLoansFxBean) {
            retBean = imixFxCfetsApiService.getCfetsImixMM().send((DepositsAndLoansFxBean) tradeBean, cfetsPackMsg);
            saveExcel(MarketIndicator.INTER_BANK_OFFERING, tradeBean);
        } else if (tradeBean instanceof FxOptionBean) {
            retBean = imixFxCfetsApiService.getCfetsImixOtc().send((FxOptionBean) tradeBean, cfetsPackMsg);
        } else if (tradeBean instanceof SwapIrsBean) {
            retBean = imixFxCfetsApiService.getCfetsImixSwap().sendFxIrs((SwapIrsBean) tradeBean, cfetsPackMsg);
        } else {
            logger.warn("参数类型不支持!");
            throw new Exception("参数类型不支持!");
        }
        return retBean;
    }

    /**
     * 验收保存
     */
    private void saveExcel(String marketIndicator, TradeBaseBean tradeBean) {
        //验收时打开
        String cstpFxFlag = configBean.getCstpFxFlag() == null ? "" : configBean.getCstpFxFlag();
        if ("Y".equals(cstpFxFlag)) {
            try {
                String basePath = configBean.getCstpFxUrl() == null ? "" : configBean.getCstpFxUrl();
                URL weburl = ImixFxMessageProcess.class.getClassLoader().getResource("WEB-INF/classes");
                URL url = ImixFxMessageProcess.class.getClassLoader().getResource("");
                String path = (null == weburl ? url.getPath() : weburl.getPath());
                ImixFxFeedBack.addDataExcelAll(marketIndicator, tradeBean, path + basePath);
            } catch (Exception e) {
                logger.error("保存excel失败，单号为【" + tradeBean.getExecId() + "】");
            }
        }
    }

    /**
     * 验收前校验部分阈值
     */
    public void beforeCheck(ExecutionReport message, String marketIndicator, String execid) {
        //验收时打开
        String cstpFxFlag = configBean.getCstpFxFlag() == null ? "" : configBean.getCstpFxFlag();
        if ("Y".equals(cstpFxFlag)) {
            try {
                ImixFxMessageProcessCheck imixFxMessageProcessCheck = new ImixFxMessageProcessCheck();
                Map<String, String> map = new HashMap<String, String>();
                map.put("marketIndicator", marketIndicator);
                Map<String, String> checkMap = imixFxMessageProcessCheck.checkFieldAll(message, map);
                if ("1".equals(checkMap.get("controlFlag")) && "1".equals(checkMap.get("promptFlag"))) {
                    //即控制校验和提示校验全部通过，不用打印日志
                } else {
                    Map<String, String> map1 = new HashMap<String, String>();
                    map1.put("marketIndicator", marketIndicator);
                    map1.put("execId", execid);
                    map1.put("msg", checkMap.get("msg"));
                    saveExcelError(map1);
                    logger.debug("外汇即期业务对象：execid=[" + execid + "]的交易报文存在以下问题：" + checkMap.get("msg"));
                }
            } catch (Exception e) {
                logger.error("校验失败，单号为【" + execid + "】");
            }
        }
    }

    /**
     * 下行转换前校验有问题时保存
     *
     * @param map 包含市场类型，交易编号，错误信息
     */
    public void saveExcelError(Map<String, String> map) {
        //验收时打开
        String cstpFxFlag = configBean.getCstpFxFlag() == null ? "" : configBean.getCstpFxFlag();
        if ("Y".equals(cstpFxFlag)) {
            try {
                String basePath = configBean.getCstpFxUrl() == null ? "" : configBean.getCstpFxUrl();
                URL weburl = ImixFxMessageProcess.class.getClassLoader().getResource("WEB-INF/classes");
                URL url = ImixFxMessageProcess.class.getClassLoader().getResource("");
                String path = (null == weburl ? url.getPath() : weburl.getPath());
                map.put("basePath", path + basePath);
                ImixFxFeedBack.addDataExcelAllError(map);
            } catch (Exception e) {
                logger.error("保存excel失败，单号为【" + map.get("execId") + "】");
            }
        }
    }
}
