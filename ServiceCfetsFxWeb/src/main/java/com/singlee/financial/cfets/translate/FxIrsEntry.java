package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.fx.ImixFxMessageHandler;
import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.SwapIrsBean;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.trade.IrsDetailBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.field.LegPriceType;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 外币利率互换解析
 */
public class FxIrsEntry extends FxSettleEntry implements ImixFxMessageHandler<SwapIrsBean> {

    /**
     * 转换外币利率互换业务 marketIndicator=15
     *
     * @param message
     * @param swapIrsBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     * @throws ParseException
     */
    public void convert(ExecutionReport message, SwapIrsBean swapIrsBean, String bankId) throws Exception {
        swapIrsBean.setExecId(message.isSetExecID() ? message.getExecID().getValue() : null);
        //交易日
        swapIrsBean.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);
        // 交易状态1
        swapIrsBean.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));
        //清算方式
        swapIrsBean.setNetGrossInd(message.isSetNetGrossInd() ? message.getNetGrossInd().getUnionTypeValue() : CfetsUtils.EMPTY);
        //交易确认标识
        swapIrsBean.setCfetsCnfmIndicator(message.isSetCFETSCnfmIndicator() ? (message.getCFETSCnfmIndicator() == null ? "" : String.valueOf(message.getCFETSCnfmIndicator().getValue())) : "");
        //清算方式
        swapIrsBean.setMarketIndicator(message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : CfetsUtils.EMPTY);
        //生效日
        swapIrsBean.getIrsInfo().setStartDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null);
        // 到期日
        swapIrsBean.getIrsInfo().setEndDate(message.isSetMaturityDate() ? message.getMaturityDate().getDateValue() : null);
        //近端期限
        swapIrsBean.getIrsInfo().setSettType(message.isSetSettlType() ? message.getSettlType().getValue() : null);
        //期限
        swapIrsBean.getIrsInfo().setmDateType(message.isSetMaturityDateType() ? message.getMaturityDateType().getValue() : null);
        //交易价格
        swapIrsBean.getIrsInfo().setPrice(message.isSetLastPx() ? new BigDecimal(message.getLastPx().getValue()) : null);
        //交易货币
        swapIrsBean.getIrsInfo().setCcy(message.isSetCurrency() ? message.getCurrency().getValue() : null);
        //交易货币金额
        swapIrsBean.getIrsInfo().setNotCcyAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null);
        //交易方向
        swapIrsBean.setSide(message.isSetSide() ? message.getSide().getUnionTypeValue() : null);

        // 设置本方机构及清算信息
        convert(message, bankId, SettPartyEnum.TheParty, swapIrsBean.getInstitutionInfo(), swapIrsBean.getSettlement());
        // 设置对手方机构及清算信息  外币债券特殊处理  没有对应货币,故上送本方清算路径即可
        convert(message, bankId, SettPartyEnum.CounterParty, swapIrsBean.getContraPatryInstitutionInfo(), swapIrsBean.getCounterPartySettlement());

        // 设置本方nolegs信息
        convert(message, 1, swapIrsBean.getIrsDetail(), swapIrsBean.getSettlement().getMarkerTakerRole());
        // 设置对方nolegs信息
        convert(message, 2, swapIrsBean.getContraPatryIrsDetail(), swapIrsBean.getSettlement().getMarkerTakerRole());

        swapIrsBean.getSettlement().setSettCcy(swapIrsBean.getIrsInfo().getCcy());
        swapIrsBean.getCounterPartySettlement().setSettCcy(swapIrsBean.getIrsInfo().getCcy());
    }

    /**
     * 利率互换 nolegs处理
     *
     * @param message
     * @param i
     * @param irsDetail
     * @param markerTakerRole
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    void convert(ExecutionReport message, int i, IrsDetailBean irsDetail, MarkerTakerEnum markerTakerRole) throws FieldNotFound, FieldConvertError, Exception {
        ExecutionReport.NoLegs nolegs = new ExecutionReport.NoLegs();
        message.getGroup(i, nolegs);
        // 3-固定值
        int legPriceType = nolegs.isSetLegPriceType() ? nolegs.getLegPriceType().getValue() : 0;
        irsDetail.setLegPriceType(legPriceType);

        PsEnum ps = convert(message.getSide(), nolegs.getLegSide(), markerTakerRole);
        irsDetail.setLegPs(ps == null ? "" : ps.toString());

        if (legPriceType == LegPriceType.SPREAD) {
            // 1-固定利率支付 B-基准端
            irsDetail.setLegSide(nolegs.isSetLegSide() ? nolegs.getLegSide().getUnionTypeValue() : CfetsUtils.EMPTY);
            // L左端 R右端
            irsDetail.setLegSign(nolegs.isSetLegSign() ? nolegs.getLegSign().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 货币代码
            irsDetail.setLegCurrency(nolegs.isSetLegCurrency() ? nolegs.getLegCurrency().getValue() : CfetsUtils.EMPTY);
            // 是否关联询价关联端
            irsDetail.setLegSolveForIndicator(nolegs.isSetLegSolveForIndicator() ? nolegs.getLegSolveForIndicator().getValue() : false);
            // 名义本金
            irsDetail.setLegQty(nolegs.isSetLegOrderQty() ? new BigDecimal(nolegs.getLegOrderQty().getValue()) : null);
            // 残端标识
            irsDetail.setLegStub(nolegs.isSetLegStubIndicator() ? nolegs.getLegStubIndicator().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 利率明细为浮动利率时
            irsDetail.setLegCurveName(nolegs.isSetLegBenchmarkCurveName() ? nolegs.getLegBenchmarkCurveName().getValue() : CfetsUtils.EMPTY);
            // 利率明细为浮动利率时
            irsDetail.setLegCurveTenor(nolegs.isSetLegBenchmarkTenor() ? nolegs.getLegBenchmarkTenor().getValue() : CfetsUtils.EMPTY);
            // 利差
            irsDetail.setRateDiff(nolegs.isSetLegLastPx() ? nolegs.getLegLastPx().getDoubleValue() : 0);
            // 付息频率
            irsDetail.setPaycycle(nolegs.isSetLegCouponPaymentFrequency() ? nolegs.getLegCouponPaymentFrequency().getValue() : CfetsUtils.EMPTY);
            // 付息日调整规则
            irsDetail.setLegCouponPaymentDateReset(nolegs.isSetLegCouponPaymentDateReset() ? nolegs.getLegCouponPaymentDateReset().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 重置频率
            irsDetail.setRaterevcycle(nolegs.isSetLegInterestAccrualResetFrequency() ? nolegs.getLegInterestAccrualResetFrequency().getValue() : CfetsUtils.EMPTY);
            //定息规则
            irsDetail.setRateFixDateAdjustment(nolegs.isSetLegInterestFixDateAdjustment() ? String.valueOf(nolegs.getLegInterestFixDateAdjustment().getValue()) : CfetsUtils.EMPTY);
            // 重置日（定息日）调整规则 0 上一营业日
            irsDetail.setRaterevDateAdjust(nolegs.isSetLegInterestAccrualResetDateAdjust() ? nolegs.getLegInterestAccrualResetDateAdjust().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 计息基准
            irsDetail.setBasis(nolegs.isSetLegDayCount() ? nolegs.getLegDayCount().getUnionTypeValue() : CfetsUtils.EMPTY);
            //首次付息日
            irsDetail.setFirstpaydate(nolegs.isSetLegCouponPaymentDate() ? nolegs.getLegCouponPaymentDate().getDateValue() : null);
            //付息计划
            irsDetail.setIntPaymentPlans(convert(message, nolegs));

        } else {
            // 1-固定利率支付 B-基准端
            irsDetail.setLegSide(nolegs.isSetLegSide() ? nolegs.getLegSide().getUnionTypeValue() : CfetsUtils.EMPTY);
            // L左端 R右端
            irsDetail.setLegSign(nolegs.isSetLegSign() ? nolegs.getLegSign().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 货币代码
            irsDetail.setLegCurrency(nolegs.isSetLegCurrency() ? nolegs.getLegCurrency().getValue() : CfetsUtils.EMPTY);
            // 是否关联询价关联端
            irsDetail.setLegSolveForIndicator(nolegs.isSetLegSolveForIndicator() ? nolegs.getLegSolveForIndicator().getValue() : false);
            // 名义本金
            irsDetail.setLegQty(nolegs.isSetLegOrderQty() ? new BigDecimal(nolegs.getLegOrderQty().getValue()) : null);
            // 残端标识
            irsDetail.setLegStub(nolegs.isSetLegStubIndicator() ? nolegs.getLegStubIndicator().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 利率明细为固定利率时
            irsDetail.setIntRate(nolegs.isSetLegLastPx() ? new BigDecimal(nolegs.getLegLastPx().getValue()) : null);
            // 付息频率
            irsDetail.setPaycycle(nolegs.isSetLegCouponPaymentFrequency() ? nolegs.getLegCouponPaymentFrequency().getValue() : CfetsUtils.EMPTY);
            // 付息日调整规则
            irsDetail.setLegCouponPaymentDateReset(nolegs.isSetLegCouponPaymentDateReset() ? nolegs.getLegCouponPaymentDateReset().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 重置频率
            irsDetail.setRaterevcycle(nolegs.isSetLegInterestAccrualResetFrequency() ? nolegs.getLegInterestAccrualResetFrequency().getValue() : CfetsUtils.EMPTY);
            //定息规则
            irsDetail.setRateFixDateAdjustment(nolegs.isSetLegInterestFixDateAdjustment() ? String.valueOf(nolegs.getLegInterestFixDateAdjustment().getValue()) : CfetsUtils.EMPTY);
            // 重置日（定息日）调整规则 0 上一营业日
            irsDetail.setRaterevDateAdjust(nolegs.isSetLegInterestAccrualResetDateAdjust() ? nolegs.getLegInterestAccrualResetDateAdjust().getUnionTypeValue() : CfetsUtils.EMPTY);
            // 计息基准
            irsDetail.setBasis(nolegs.isSetLegDayCount() ? nolegs.getLegDayCount().getUnionTypeValue() : CfetsUtils.EMPTY);
            //首次付息日
            irsDetail.setFirstpaydate(nolegs.isSetLegCouponPaymentDate() ? nolegs.getLegCouponPaymentDate().getDateValue() : null);
            //付息计划
            irsDetail.setIntPaymentPlans(convert(message, nolegs));
        }

        //如果获取不到首次付息日则从付息计划中获取
        if (irsDetail.getFirstpaydate() == null && irsDetail.getIntPaymentPlans() != null && irsDetail.getIntPaymentPlans().size() > 0) {
            if (irsDetail.getIntPaymentPlans().get(0).getIntPayDate() != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                irsDetail.setFirstpaydate(sdf.parse(irsDetail.getIntPaymentPlans().get(0).getIntPayDate()));
            }
        }
    }
}
