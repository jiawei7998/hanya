package com.singlee.financial.cfets.fx;

import com.singlee.financial.cfets.Util.MarketIndicatorEnum;
import com.singlee.financial.cfets.Util.SpringGetBeanByClass;
import com.singlee.financial.cfets.bean.CfetsPackMsgBean;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.spring.SpringContextHolder;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.TradingFailureBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.ConfigError;
import imix.DataDictionary;
import imix.InvalidMessage;
import imix.client.core.ImixApplication;
import imix.client.core.ImixSession;
import imix.field.*;
import imix.imix10.ExecutionReport;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ImixFxManageServer implements IImixFxManageServer {

    private static Logger LogManager = Logger.getLogger(ImixFxManageServer.class);
    public final static char SPLIT_CHAR = 0X01;
    @Autowired
    private ImixFxClient imixFxClient;

    @Autowired
    private ConfigBean configBean;
    @Autowired
    ImixFxCfetsApiService imixFxCfetsApiService;

    @Override
    public boolean startImixSession() {
        // 是否登录
        boolean isLogin = false;
        // fxImixClient名称是配置在main.spring.xml中bean的别名，这个需要注意
        ImixFxClient fxClient = SpringContextHolder.getBean("fxImixClient");
        // 先停止应用服务,再进行开启
        ImixApplication.stop();
        // 重新初始化程序
        try {
            isLogin = fxClient.initLogin();
        } catch (Exception e) {
            LogManager.error("CFETS RESTART FX LOGIN ConfigError:[" + e.getMessage() + "]", e);
        }
        return isLogin;

    }

    @Override
    public boolean stopImixSession() {
        ImixSession imixSession = imixFxClient.getImixSession();
        if (imixSession.isStarted()) {
            //先登出单个会话
            imixSession.stop();
        }
        //登出所有会话
        ImixApplication.stop();
        return false;
    }

    @Override
    public String confirmFxTransaction(String xml) {
        if (null == xml) {
            return "";
        }
        ExecutionReport report = new ExecutionReport();
        try {
            report.fromString(xml.toString(), new DataDictionary("IMIX10.xml"), false);
        } catch (InvalidMessage e) {
            e.printStackTrace();
        } catch (ConfigError e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 读取发送失败的交易记录
     */
    @Override
    public List<TradingFailureBean> getFailureSendFxByDate(String date) {
        //自动加载日志文件的路径位置
        LogManager.info("*************************外币读取发送失败日志开始**********************");
        ArrayList<TradingFailureBean> list = new ArrayList<TradingFailureBean>();
        ArrayList<String> arr = new ArrayList<String>();
        TradingFailureBean bean = null;
        ConfigBean configBean = SpringGetBeanByClass.getBean(ConfigBean.class);
        //获取date日期对应的交易报文
        Map<String, File> map = getFileByDate(date, getFileNames(getResourceAsFilePath(configBean.getLogPath())));

        if (map == null || map.size() == 0) {
            return list;
        }
        //处理报文
        InputStream instream = null;
        BufferedReader buffreader = null;
        InputStreamReader inputreader = null;
        for (Map.Entry<String, File> entry : map.entrySet()) {
            try {
                instream = new FileInputStream(entry.getValue());
                if (instream != null) {
                    inputreader = new InputStreamReader(instream, configBean.getCharset());
                    buffreader = new BufferedReader(inputreader);
                    String line;
                    while ((line = buffreader.readLine()) != null) {
                        if (line.contains("INFO") && line.contains("8=IMIX.1.0") && (line.contains("35=8") || line.contains("35=d"))) {
                            arr.add(line.substring(line.indexOf("8=IMIX.1.0")));
                        }
                    }
                }
            } catch (java.io.FileNotFoundException e) {
                LogManager.error(e.getMessage(), e);
            } catch (IOException e) {
                LogManager.error(e.getMessage(), e);
            } finally {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e) {
                        LogManager.debug(e.getMessage());
                    }
                }
                if (buffreader != null) {
                    try {
                        buffreader.close();
                    } catch (IOException e) {
                        LogManager.debug(e.getMessage());
                    }
                }
            }
        }// end for
        Map<String, String> map1 = null;
        Map<String, String> tMap = new HashMap<String, String>();
        //解析报文封装成对象
        for (String str : arr) {
            map1 = new HashMap<String, String>();
            //根据这个东西进行分解  
            String[] strTemp = str.split(String.valueOf(SPLIT_CHAR));
            if (strTemp != null && strTemp.length > 0) {
                for (String strRecord : strTemp) {
                    if (strRecord.startsWith(ExecID.FIELD + "=")
                            || strRecord.startsWith(SecurityID.FIELD + "=")
                            || strRecord.startsWith(MarketIndicator.FIELD + "=")
                            || strRecord.startsWith(TradeDate.FIELD + "=")) {
                        System.out.println(strRecord);
                        String[] strLength = strRecord.split("=");
                        map1.put(strLength[0], strLength[1]);
                    }
                }

                if (tMap.get(map1.get(String.valueOf(ExecID.FIELD))) == null) {
                    //封装数据
                    bean = new TradingFailureBean();
                    bean.setDealNo(map1.get(String.valueOf(ExecID.FIELD)));
                    bean.setMarketIndicator(map1.get(String.valueOf(MarketIndicator.FIELD)) == null ?
                            map1.get(String.valueOf(SecurityID.FIELD)) :
                            MarketIndicatorEnum.getName(map1.get(String.valueOf(MarketIndicator.FIELD))));
                    bean.setTradeDate(map1.get(String.valueOf(TradeDate.FIELD)));
                    bean.setXml(str);

                    tMap.put(bean.getDealNo(), bean.getDealNo());
                    list.add(bean);
                    System.out.println(str);
                    LogManager.info(str);
                }// end if

            }// end if
        }// end for
        LogManager.info("*************************外币读取发送失败日志结束**********************");
        return list;
    }

    /**
     * 获取某一个文件夹下的所有文件
     */
    public static File[] getResourceAsFilePath(String path) {
        File[] fileList = null;
        if (path == null) {
            throw new RuntimeException("路径不能为空，请检查");
        }
        File files = new File(path);
        if (files.exists()) {
            fileList = files.listFiles(new FileFilter() {

                @Override
                public boolean accept(File f) {
                    return f.isFile();
                }
            });
        } else {
            throw new RuntimeException("文件不存在");
        }
        return fileList;
    }

    /**
     * 获取文件名称和文件内容  map<key(文件名),value(对应的文件)>;
     *
     * @param files
     * @return
     */
    public static Map<String, File> getFileNames(File[] files) {
        Map<String, File> map = new HashMap<String, File>();
        if (files.length > 0) {
            for (File file : files) {
                map.put(file.getName(), file);
            }
        }
        return map;
    }

    /**
     * 获取指定格式的路径
     *
     * @param map
     */
    public static Map<String, File> getFileByDate(String str, Map<String, File> map) {
        Map<String, File> mapTemp = new HashMap<String, File>();
        if (str == null) {
            throw new RuntimeException("过滤条件不能为空");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        for (Map.Entry<String, File> entry : map.entrySet()) {
            if (entry.getKey().toString().contains(str)) {
                mapTemp.put(entry.getKey(), entry.getValue());
            } else {
                c.setTimeInMillis(entry.getValue().lastModified());
                if (sdf.format(c.getTime()).equals(str)) {
                    mapTemp.put(entry.getKey(), entry.getValue());
                }
            }
        }
        return mapTemp;
    }

    /**
     * 外币重新导入
     */
    @Override
    public RetBean resendFxRecord(TradingFailureBean bean) throws Exception {
        LogManager.info("**************重发开始***************");
        try {
            ExecutionReport message = new ExecutionReport();
            message.fromString(reBuilderMsg(bean.getXml()), new DataDictionary("IMIX10.xml"), false);

            ImixFxMessageProcessImpl process = new ImixFxMessageProcessImpl(configBean);
            // 市场标识
            String marketIndicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
            // 消息类型
            String msgType = message.getHeader().getString(MsgType.FIELD);
            // Cfets流水号
            String execID = message.isSetExecID() ? message.getExecID().getValue() : "";
            // 成交日期
            String tradeDate = DateFormatUtils.format(message.getTradeDate().getDateValue(), "yyyy/MM/dd HH:mm:ss.SSS");
            // 交易状态
            String dealStatus = message.isSetDealTransType() ? message.getDealTransType().getValue() + "" : message.getExecType().getValue();
            //报文协议
            String beginstring = message.getHeader().getString(BeginString.FIELD);

            CfetsPackMsgBean cfetsPackMsg = new CfetsPackMsgBean();
            cfetsPackMsg.setExecid(execID);
            cfetsPackMsg.setTradedate(tradeDate);
            cfetsPackMsg.setBeginstring(beginstring);
            cfetsPackMsg.setDealtranstype(dealStatus);
            cfetsPackMsg.setMarketindicator(marketIndicator);
            cfetsPackMsg.setMsgtype(ConfigBean.reSendFlag);
            cfetsPackMsg.setPackmsg(message.toString());
            TradeBaseBean tradeBaseBean = process.parseMsg(message, marketIndicator);
            LogManager.info(tradeBaseBean.getExecId() + "解析完成:" + bean.getXml());

            return process.sendMsg(tradeBaseBean, cfetsPackMsg, imixFxCfetsApiService);

        } catch (Exception e) {
            LogManager.error("**************重发时存在错误******************：" + e.getMessage());
            throw e;
        }
    }

    public static String reBuilderMsg(String msg) throws Exception {
        //93=7489=XXX10=191
        //获取93域验证字符串的长度
        if (msg.indexOf(SPLIT_CHAR + "93=") != -1 && msg.indexOf(SPLIT_CHAR + "89=") != -1) {
            int index = msg.indexOf(SPLIT_CHAR + "93=") + 4;
            String validStr = msg.substring(index);
            int validLen = Integer.parseInt(validStr.substring(0, validStr.indexOf(SPLIT_CHAR)));

            //赋值给89域,长度为validLen
            int i = msg.indexOf(SPLIT_CHAR + "89=") + 4;
            String s1 = msg.substring(0, i);
            String tmp = msg.substring(i);
            String s2 = tmp.substring(tmp.indexOf(SPLIT_CHAR));

            msg = s1 + String.format("%0" + validLen + "d", 0) + s2;
        }
        return msg;
    }

    @Override
    public boolean getImixSessionState() {
        ImixSession imixSession = imixFxClient.getImixSession();
        return imixSession.isStarted();
    }

    @Override
    public boolean isSessionStarted() {
        // TODO Auto-generated method stub
        return false;
    }


}
