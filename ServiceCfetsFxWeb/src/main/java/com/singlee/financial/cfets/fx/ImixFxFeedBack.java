package com.singlee.financial.cfets.fx;

import com.singlee.financial.cfets.Util.ExcelUtil;
import com.singlee.financial.cfets.Util.TrdSocketListener;
import com.singlee.financial.pojo.*;
import com.singlee.financial.pojo.component.BasisEnum;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.SptFwdTypeEnum;
import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.financial.pojo.trade.FxOptionCurrencyAmtBean;
import com.singlee.financial.pojo.trade.FxOptionPremiumBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.field.LegPriceType;
import imix.field.MarketIndicator;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.DocumentFactoryHelper;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 验收反馈excel文件生成
 * @author Administrator
 *
 */
public class ImixFxFeedBack {
	
	private static Logger LogManager = Logger.getLogger(TrdSocketListener.class);
	
	
	//保存数据到excel  (new)
	/**
	 * 
	 * @param marketIndicator 由于即期远期数据交叉，所以需要区分
	 * @param tradeBean
	 * @throws IOException
	 */
	public static synchronized  void addDataExcelAll(String marketIndicator,TradeBaseBean tradeBean,String basePath)throws IOException{
		if("".equals(basePath)){
			File file = new File("feedbackFile");
			basePath=file.getCanonicalPath().replace("\\", "/")+"/";
		}
		String excelPath=basePath+"FXCSTP.xlsx";//交易保存excel
		if(marketIndicator.equals(MarketIndicator.FXSPT)){//即期交易
			try {
				addExcelSPTFWD(marketIndicator+"-外汇即期",excelPath,(FxSptFwdBean) tradeBean);
			} catch (Exception e) {
				LogManager.info(e.getCause().getMessage());
				e.printStackTrace();
			}
	    }else if(marketIndicator.equals(MarketIndicator.FXNDF)|| marketIndicator.equals(MarketIndicator.GOLDFWD)){//黄金即期
              //		addDataExcel("黄金即期",excelPath,(FxSptFwdBean) tradeBean);
	    }else if(marketIndicator.equals(MarketIndicator.FXFOW) || marketIndicator.equals(MarketIndicator.FXNDF)
			|| marketIndicator.equals(MarketIndicator.GOLDFWD)){//远期交易
			try {
				addExcelSPTFWD(marketIndicator+"-外汇远期",excelPath,(FxSptFwdBean) tradeBean);
			} catch (Exception e) {
				e.printStackTrace();
			}
	  } else if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {//外币拆借
			try {
				addExcelLEND(marketIndicator+"-信用拆借",excelPath,(DepositsAndLoansFxBean) tradeBean);
			} catch (Exception e) {
				LogManager.info(e.getCause().getMessage());
				e.printStackTrace();
			}
	 }else if (marketIndicator.equals(MarketIndicator.FXSWP)) {//外币掉期
			try {
				addExcelSWAP(marketIndicator+"-外汇掉期",excelPath,(FxSwapBean) tradeBean);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}else if (marketIndicator.equals(MarketIndicator.FXOPT)) {//外汇期权
		try {
			addExcelOTC(marketIndicator+"-外币期权",excelPath,(FxOptionBean) tradeBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}else{//市场类型为空或者不对的默认保存在即期里//即期交易
		try {
			addExcelSPTFWD(marketIndicator,excelPath,(FxSptFwdBean) tradeBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
	
	public static synchronized  void addDataExcelAllError(Map<String,String> map)throws IOException{
		/**
		 * 外币验收正常交易和异常交易分开下行，所以文件应该创建双份
		 * 注意：excel文件名称目前写死，必须有，不判断。
		 */
		//TODO 文件路径
		String basePath=map.get("basePath")==null?"":map.get("basePath");
		if("".equals(basePath)){
			File file = new File("feedbackFile");
			basePath=file.getCanonicalPath().replace("\\", "/")+"/";
		}
		String excelPath=basePath+"FXCSTP.xlsx";//分开写，提高速度
		String marketIndicator = map.get("marketIndicator")==null?MarketIndicator.FXSPT:map.get("marketIndicator");//错误信息默认保存到即期文件中
		if(marketIndicator.equals(MarketIndicator.FXSPT)){
			try {
				map.put("sheetName","SPOT");
				addExcelError(excelPath,map);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if(marketIndicator.equals(MarketIndicator.FXNDF)
				|| marketIndicator.equals(MarketIndicator.GOLDFWD)){//黄金即期
//			addDataExcel("黄金即期",excelPath,(FxSptFwdBean) tradeBean);
		}else if(marketIndicator.equals(MarketIndicator.FXFOW) || marketIndicator.equals(MarketIndicator.FXNDF)
				|| marketIndicator.equals(MarketIndicator.GOLDFWD)){//远期交易
			try {
				 map.put("sheetName","FORWARD");
				addExcelError(excelPath,map);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {//外币拆借
			try {
				 map.put("sheetName","FCL");
				addExcelError(excelPath,map);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if (marketIndicator.equals(MarketIndicator.FXSWP)) {//外币掉期
			try {
				 map.put("sheetName","SWAP");
				addExcelError(excelPath,map);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else if (marketIndicator.equals(MarketIndicator.FXOPT)) {//外汇期权
			try {
				 map.put("sheetName","FXOPTION");
				addExcelError(excelPath,map);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{//市场类型为空或者不对的默认保存在即期里
			try {
				map.put("sheetName","SPOT");
				addExcelError(excelPath,map);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			
	}
	public static  void addExcelError(String excelPath,Map<String,String> map)throws IOException{
		Workbook wb = null;
		FileOutputStream out=null;
		try {
			FileInputStream fs = new FileInputStream(excelPath);//获取excel
			wb = getWorkbook(fs);//获取excel
			String sheetName=map.get("sheetName");
			Sheet sheet = wb.getSheet(sheetName);//获取工作表
			int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
			out = new FileOutputStream(excelPath);//向excel中添加数据
			Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
			//设置单元格的数据
		    row.createCell(0).setCellValue(map.get("execId")==null?"":map.get("execId"));//成交编号17
		    row.createCell(1).setCellValue(map.get("msg")==null?"":map.get("msg"));//错误内容
			out.flush();
			wb.write(out);
//			wb.close();
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(out!=null){
				out.close();
			}
			if(wb!=null){
				wb.close();
			}
		}
	}
	/**
	 * 外汇期权转换
	 * @param marketIndicator
	 * @param excelPath
	 * @param tradeBean
	 * @throws IOException
	 */
	public static void addExcelOTC(String marketIndicator,String excelPath,FxOptionBean tradeBean)throws IOException{
		Workbook wb = null;
		FileOutputStream out=null;
		try {
		FileInputStream fs = new FileInputStream(excelPath);//获取excel
		wb = getWorkbook(fs);//获取excel
		Sheet sheet = wb.getSheet("FXOPTION");//获取工作表
		int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
		out = new FileOutputStream(excelPath);//向excel中添加数据
		Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//设置单元格的数据
	    row.createCell(0).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
	    row.createCell(1).setCellValue(marketIndicator);//市场标识10176
	    row.createCell(3).setCellValue(tradeBean.getCfetsCnfmIndicator()==null?"":tradeBean.getCfetsCnfmIndicator());//交易确认标识11049
	    row.createCell(4).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
	    row.createCell(5).setCellValue(tradeBean.getOptPayoutType()==null?"":tradeBean.getOptPayoutType());//期权类型1482
	    row.createCell(8).setCellValue(tradeBean.getExpireTimeZone()==null?"":tradeBean.getExpireTimeZone());//行权截止时区10741
	    row.createCell(9).setCellValue(tradeBean.getSettlType()==null?"":tradeBean.getSettlType());//行权期限63
	    row.createCell(10).setCellValue(tradeBean.getDerivativeExerciseStyle()==null?"":tradeBean.getDerivativeExerciseStyle());//行权方式1299
	    row.createCell(15).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
	    row.createCell(16).setCellValue(tradeBean.getSymbol()==null?"":tradeBean.getSymbol());//货币对55
	    //TODO 此转换有问题
//	    row.createCell(15).setCellValue(tradeBean.getSide()==null?"":tradeBean.getSide());//54子期权方向Self
	    row.createCell(19).setCellValue(tradeBean.getPutOrCall()==null?"":tradeBean.getPutOrCall());//Call/Put201
	    row.createCell(20).setCellValue(tradeBean.getOptDataType()==null?"":tradeBean.getOptDataType());//期权报文类型10738
	    //putorcall详细信息
	    setPutAndCallInfo(row,tradeBean.getCcy1());
	    setPutAndCallInfo(row,tradeBean.getCcy2());
	    row.createCell(27).setCellValue(tradeBean.getRiskLastQty()==null?"":tradeBean.getRiskLastQty().toString());//折美元金额10284
	    row.createCell(28).setCellValue(tradeBean.getSettlType()==null?"":tradeBean.getSettlType());//行权期限63
	    row.createCell(30).setCellValue(tradeBean.getMaturityDate()==null?"":tradeBean.getMaturityDate());//行权日541
	    row.createCell(32).setCellValue(tradeBean.getStrikePrice()==null?"":tradeBean.getStrikePrice());//行权价202
	    //子期权
	    setPremium(row,tradeBean.getPremium());
	    row.createCell(38).setCellValue(tradeBean.getSide()==null?"":tradeBean.getSide());//54组合期权方向Taker
	    row.createCell(44).setCellValue(tradeBean.getVolatility()==null?"":tradeBean.getVolatility());//子期权波动率1188
	    row.createCell(47).setCellValue(tradeBean.getExpireTime()==null?"":tradeBean.getExpireTime());//行权截止时间126
	    
	    row.createCell(51).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
	    row.createCell(52).setCellValue(tradeBean.getInstitutionInfo().getFullName()==null?"":tradeBean.getInstitutionInfo().getFullName());//本方机构全称
	    row.createCell(54).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方21位机构码
	    row.createCell(56).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
	    row.createCell(57).setCellValue(tradeBean.getContraPatryInstitutionInfo().getFullName()==null?"":tradeBean.getContraPatryInstitutionInfo().getFullName());//对方机构全称
	    row.createCell(59).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方21位机构码
	    
	    row.createCell(70).setCellValue(tradeBean.getDealTransType()==null?"":tradeBean.getDealTransType());//状态10105
	    row.createCell(71).setCellValue(tradeBean.getDerivativeExerciseStatus()==null?"":tradeBean.getDerivativeExerciseStatus());//行权状态10739
	    row.createCell(72).setCellValue(tradeBean.getTradeInstrument()==null?"":tradeBean.getTradeInstrument().toString());//交易模型10315
	    row.createCell(73).setCellValue(tradeBean.getTradeMethod()==null?"":tradeBean.getTradeMethod().toString());//交易方式10317
	    row.createCell(74).setCellValue(tradeBean.getDealMode()==null?"":tradeBean.getDealMode().toString());//交易模式11657
	    row.createCell(75).setCellValue(tradeBean.getNetGrossInd()==null?"":tradeBean.getNetGrossInd());//清算方式430
	    row.createCell(79).setCellValue(tradeBean.getRelatedReference()==null?"":tradeBean.getRelatedReference());//EX编号10535
	    
	  //清算信息:期权交易只有人民币清算信息(期权只是买卖权利，没有交易)
	  MarkerTakerEnum markerTaker= tradeBean.getSettlement().getMarkerTakerRole();//本方（泰隆）角色
		 if(MarkerTakerEnum.Taker.equals(markerTaker)){
			 returnRowOtc(row,81,tradeBean.getSettlement());//本方
			 returnRowOtc(row,91,tradeBean.getCounterPartySettlement());//对手方
		 }else{
			//本方为受市方
			 returnRowOtc(row,91,tradeBean.getSettlement());//本方
			 returnRowOtc(row,81,tradeBean.getCounterPartySettlement());//对手方
		 }
		out.flush();
		wb.write(out);
//		wb.close();
		
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(out!=null){
				out.close();
			}
			if(wb!=null){
				wb.close();
			}
		}
	}
	public static void setPutAndCallInfo(Row row,FxOptionCurrencyAmtBean ccy){
	    if("CALL".equals(ccy.getCurrencyAmtType())){
	    	//call
	    	row.createCell(23).setCellValue(ccy.getCurrency());//Call CCY
	    	row.createCell(24).setCellValue(ccy.getPrincipal());//Call Amount
	    }else if("PUT".equals(ccy.getCurrencyAmtType())){
	    	//put
	    	row.createCell(25).setCellValue(ccy.getCurrency());//PUT CCY
	    	row.createCell(26).setCellValue(ccy.getPrincipal());//PUT Amount
	    }
	}
	public static void setPremium(Row row,FxOptionPremiumBean premiun){
		row.createCell(33).setCellValue(premiun.getOptPremiumValue());//子期权期权费率10779
		row.createCell(34).setCellValue(premiun.getOptPremiumBasis());//子期权期权费类型10778
		row.createCell(35).setCellValue(premiun.getOptPremiumCurrency());//子期权期权费币种10780
		row.createCell(36).setCellValue(premiun.getOptPremiumAmt());//子期权期权费总额10781
		row.createCell(37).setCellValue(premiun.getDecimalPlaces());//小数位数10429
	}
	
	/**
	 * 外汇掉期转换
	 * @param marketIndicator
	 * @param excelPath
	 * @param tradeBean
	 * @throws IOException
	 */
	public static void addExcelSWAP(String marketIndicator,String excelPath,FxSwapBean tradeBean)throws IOException{
		Workbook wb=null;
		FileOutputStream out=null;
		try {
		FileInputStream fs = new FileInputStream(excelPath);//获取excel
		wb = getWorkbook(fs);//获取excel
		Sheet sheet = wb.getSheet("SWAP");//获取工作表
		int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
		out = new FileOutputStream(excelPath);//向excel中添加数据
		Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//设置单元格的数据
	    row.createCell(0).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
		row.createCell(1).setCellValue(tradeBean.getDealDateTime()==null?"":sdf.format(tradeBean.getDealDateTime()));//成交编号17

	    row.createCell(3).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
	    row.createCell(4).setCellValue(tradeBean.getTradeTime()==null?"":tradeBean.getTradeTime());//成交时间10318
	    row.createCell(5).setCellValue(marketIndicator);//市场标识10176
	    row.createCell(7).setCellValue(tradeBean.getCcypair()==null?"":tradeBean.getCcypair());//货币对55
	    row.createCell(8).setCellValue(tradeBean.getCcy1()==null?"":tradeBean.getCcy1());//基准货币10064
	    row.createCell(9).setCellValue(tradeBean.getCcy2()==null?"":tradeBean.getCcy2());//非基准货币10065
	    row.createCell(10).setCellValue(tradeBean.getCurrencyInfo().getCcy()==null?"":tradeBean.getCurrencyInfo().getCcy());//交易货币15
	    row.createCell(11).setCellValue(tradeBean.getCurrencyInfo().getContraCcy()==null?"":tradeBean.getCurrencyInfo().getContraCcy());//对应货币11514
	    //近端
	    row.createCell(12).setCellValue(tradeBean.getCurrencyInfo().getLegSide()==null?"":tradeBean.getCurrencyInfo().getLegSide());//发起方方向624
	    row.createCell(13).setCellValue(tradeBean.getCurrencyInfo().getLegSide()==null?"":tradeBean.getCurrencyInfo().getLegSide());//发起方方向624
	    row.createCell(14).setCellValue(tradeBean.getCurrencyInfo().getCcy1Amt()==null?"":tradeBean.getCurrencyInfo().getCcy1Amt().toString());//基准货币金额11599
	    row.createCell(15).setCellValue(tradeBean.getCurrencyInfo().getCcy2Amt()==null?"":tradeBean.getCurrencyInfo().getCcy2Amt().toString());//非基准货币金额11600
	    row.createCell(16).setCellValue(tradeBean.getCurrencyInfo().getAmt()==null?"":tradeBean.getCurrencyInfo().getAmt().toString());//交易量685
	    row.createCell(17).setCellValue(tradeBean.getCurrencyInfo().getContraAmt()==null?"":tradeBean.getCurrencyInfo().getContraAmt().toString());//反方交易量1074
	    row.createCell(18).setCellValue(tradeBean.getCurrencyInfo().getLegRiskOrderQty()==null?"":tradeBean.getCurrencyInfo().getLegRiskOrderQty().toString());//折美元交易量10132
	    row.createCell(19).setCellValue(tradeBean.getCurrencyInfo().getLegSettlType()==null?"":tradeBean.getCurrencyInfo().getLegSettlType());//期限587
	    row.createCell(20).setCellValue(tradeBean.getValueDate()==null?"":sdf.format(tradeBean.getValueDate()));//起息日588
	    row.createCell(21).setCellValue(tradeBean.getCurrencyInfo().getSpotRate()==null?"":tradeBean.getCurrencyInfo().getSpotRate().toString());//即期汇率10112
	    row.createCell(22).setCellValue(tradeBean.getCurrencyInfo().getPoints()==null?"":tradeBean.getCurrencyInfo().getPoints().toString());//远期点1073
	    row.createCell(23).setCellValue(tradeBean.getCurrencyInfo().getRate()==null?"":tradeBean.getCurrencyInfo().getRate().toString());//交易价格637
	    //远端
	    row.createCell(24).setCellValue(tradeBean.getFarCurrencyInfo().getLegSide()==null?"":tradeBean.getFarCurrencyInfo().getLegSide());//发起方方向624
	    row.createCell(25).setCellValue(tradeBean.getFarCurrencyInfo().getLegSide()==null?"":tradeBean.getFarCurrencyInfo().getLegSide());//发起方方向624
	    row.createCell(26).setCellValue(tradeBean.getFarCurrencyInfo().getCcy1Amt()==null?"":tradeBean.getFarCurrencyInfo().getCcy1Amt().toString());//基准货币金额11599
	    row.createCell(27).setCellValue(tradeBean.getFarCurrencyInfo().getCcy2Amt()==null?"":tradeBean.getFarCurrencyInfo().getCcy2Amt().toString());//非基准货币金额11600
	    row.createCell(28).setCellValue(tradeBean.getFarCurrencyInfo().getAmt()==null?"":tradeBean.getFarCurrencyInfo().getAmt().toString());//交易量685
	    row.createCell(29).setCellValue(tradeBean.getFarCurrencyInfo().getContraAmt()==null?"":tradeBean.getFarCurrencyInfo().getContraAmt().toString());//反方交易量1074
	    row.createCell(30).setCellValue(tradeBean.getFarCurrencyInfo().getLegRiskOrderQty()==null?"":tradeBean.getFarCurrencyInfo().getLegRiskOrderQty().toString());//折美元交易量10132
	    row.createCell(31).setCellValue(tradeBean.getFarCurrencyInfo().getLegSettlType()==null?"":tradeBean.getFarCurrencyInfo().getLegSettlType());//期限587
	    row.createCell(32).setCellValue(tradeBean.getFarValueDate()==null?"":sdf.format(tradeBean.getFarValueDate()));//起息日588
	    row.createCell(33).setCellValue(tradeBean.getFarCurrencyInfo().getSpotRate()==null?"":tradeBean.getFarCurrencyInfo().getSpotRate().toString());//即期汇率10112
	    row.createCell(34).setCellValue(tradeBean.getFarCurrencyInfo().getPoints()==null?"":tradeBean.getFarCurrencyInfo().getPoints().toString());//远期点1073
	    row.createCell(35).setCellValue(tradeBean.getFarCurrencyInfo().getRate()==null?"":tradeBean.getFarCurrencyInfo().getRate().toString());//交易价格637
	    row.createCell(38).setCellValue(tradeBean.getInstitutionInfo().getShortName()==null?"":tradeBean.getInstitutionInfo().getShortName());//self
	    row.createCell(39).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
	    row.createCell(40).setCellValue(tradeBean.getInstitutionInfo().getFullName()==null?"":tradeBean.getInstitutionInfo().getFullName());//本方机构全称
	    row.createCell(42).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方21位机构码
	    row.createCell(43).setCellValue(tradeBean.getContraPatryInstitutionInfo().getShortName()==null?"":tradeBean.getContraPatryInstitutionInfo().getShortName());//对方
	    row.createCell(44).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
	    row.createCell(45).setCellValue(tradeBean.getContraPatryInstitutionInfo().getFullName()==null?"":tradeBean.getContraPatryInstitutionInfo().getFullName());//对方机构全称
	    row.createCell(47).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方21位机构码
	    
	    row.createCell(58).setCellValue(tradeBean.getDealStatus()==null?"":tradeBean.getDealStatus().toString());//状态10105
	    row.createCell(59).setCellValue(tradeBean.getTradeInstrument()==null?"":tradeBean.getTradeInstrument().toString());//交易模型10315
	    row.createCell(60).setCellValue(tradeBean.getTradeMethod()==null?"":tradeBean.getTradeMethod().toString());//交易方式10317
	    row.createCell(61).setCellValue(tradeBean.getDealMode()==null?"":tradeBean.getDealMode().toString());//交易模式11657
	    //row.createCell(63).setCellValue(tradeBean.getSettMode()==null?"":tradeBean.getSettMode().toString());//清算方式430
	    
	  //清算信息:需要根据发起方，买卖方向，成交货币，基准货币，综合判断
	     //四种：TAKER_DEALT_*:发起方买入交易货币；TAKER_CONTRA_*：发起方买入对应货币
	     	//Maker_DEALT_*:受市方买入交易货币；Maker_CONTRA_*：受市方买入对应货币
	     MarkerTakerEnum markerTaker= tradeBean.getSettlement().getMarkerTakerRole();//本方（泰隆）角色
	     String ps =tradeBean.getPs()==null?"":tradeBean.getPs().toString();//本方（泰隆）买卖方向（买卖基准货币的方向）
	     String ccy =tradeBean.getCurrencyInfo().getCcy()==null?"":tradeBean.getCurrencyInfo().getCcy();//交易货币
		 String ccy1=tradeBean.getCcy1()==null?"":tradeBean.getCcy1();//基准货币
		 boolean ccyFlag = ccy.equals(ccy1);//判断基准货币是否等于交易货币
		 if(MarkerTakerEnum.Taker.equals(markerTaker)){
			 //本方为发起方
			 if(("P".equals(ps)&&ccyFlag)||("S".equals(ps)&&!ccyFlag)){
				 //买交易币种，本方为TAKER_DEALT_，对手方为Maker_CONTRA_
				 returnRow(row,66,tradeBean.getSettlement());//本方
				 returnRow(row,96,tradeBean.getCounterPartySettlement());//对手方
				 returnRow(row,76,tradeBean.getFarSettlement());//本方
				 returnRow(row,86,tradeBean.getFarCounterPartySettlement());//对手方
			 }else{
				//买对应币种，本方为TAKER_CONTRA_，对手方为Maker_DEALT_
				 returnRow(row,76,tradeBean.getSettlement());//本方
				 returnRow(row,86,tradeBean.getCounterPartySettlement());//对手方
				 returnRow(row,66,tradeBean.getFarSettlement());//本方
				 returnRow(row,96,tradeBean.getFarCounterPartySettlement());//对手方
			 }
		 }else{
			 //本方为受市方
			 if(("P".equals(ps)&&ccyFlag)||("S".equals(ps)&&!ccyFlag)){
				 //买交易币种，本方为Maker_DEALT_，对手方为TAKER_CONTRA_
				 returnRow(row,86,tradeBean.getSettlement());//本方
				 returnRow(row,76,tradeBean.getCounterPartySettlement());//对手方
				 returnRow(row,96,tradeBean.getFarSettlement());//本方
				 returnRow(row,66,tradeBean.getFarCounterPartySettlement());//对手方
				 
			 }else{
				//买对应币种，本方为Maker_CONTRA_，对手方为TAKER_DEALT_
				 returnRow(row,96,tradeBean.getSettlement());//本方
				 returnRow(row,66,tradeBean.getCounterPartySettlement());//对手方
				 returnRow(row,86,tradeBean.getFarSettlement());//本方
				 returnRow(row,76,tradeBean.getFarCounterPartySettlement());//对手方
			 }
		 }
	    
	    row.createCell(107).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
	    out.flush();
		wb.write(out);
		LogManager.info("掉期交易保存EXCEL:"+tradeBean.getExecId());


		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(out!=null){
				out.close();
			}
			if(wb!=null){
				wb.close();
			}
		}
	}
	/**
	 * 外币拆借转换
	 * @param marketIndicator
	 * @param excelPath
	 * @param tradeBean
	 * @throws IOException
	 */
	public static void addExcelLEND(String marketIndicator,String excelPath,DepositsAndLoansFxBean tradeBean)throws IOException{
		Workbook wb=null;
		FileOutputStream out=null;
		try {
		FileInputStream fs = new FileInputStream(excelPath);//获取excel
		wb = getWorkbook(fs);//获取excel
		Sheet sheet = wb.getSheet("FCL");//获取工作表
		int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
		out = new FileOutputStream(excelPath);//向excel中添加数据
		Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//设置单元格的数据
	    row.createCell(0).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
	    row.createCell(1).setCellValue(tradeBean.getTradeTime()==null?"":tradeBean.getTradeTime());//成交时间10318
	    row.createCell(2).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75
	    row.createCell(3).setCellValue(tradeBean.getCurrencyInfo().getCcy()==null?"":tradeBean.getCurrencyInfo().getCcy());//交易货币15
	    row.createCell(4).setCellValue(tradeBean.getCurrencyInfo().getRate()==null?"":tradeBean.getCurrencyInfo().getRate().divide(new BigDecimal(100), 2).toString());//拆借利率31
	    row.createCell(5).setCellValue(tradeBean.getCurrencyInfo().getAmt()==null?"":tradeBean.getCurrencyInfo().getAmt().toString());//拆借金额32
	    row.createCell(6).setCellValue(tradeBean.getPs()==null?"":tradeBean.getPs().toString());//本方交易方向54
	    row.createCell(8).setCellValue(tradeBean.getBasis()==null?"":tradeBean.getBasis().toString());//计息基础10040
	    row.createCell(9).setCellValue(tradeBean.getValueDate()==null?"":sdf.format(tradeBean.getValueDate()));//起息日 64
	    row.createCell(10).setCellValue(tradeBean.getSettlType()==null?"":tradeBean.getSettlType());//期限63
	    row.createCell(11).setCellValue(tradeBean.getMatuitydate()==null?"":sdf.format(tradeBean.getMatuitydate()));//还款日193
	    row.createCell(12).setCellValue(tradeBean.getSettlCurrAmt2()==null?"":tradeBean.getSettlCurrAmt2().toString());//到期还款金额10289
	    row.createCell(13).setCellValue(tradeBean.getRiskLastQty()==null?"":tradeBean.getRiskLastQty().toString());//折美元金额10284
	    row.createCell(14).setCellValue(tradeBean.getAccruedInterestTotal()==null?"":tradeBean.getAccruedInterestTotal().toString());//应计利息10002
	    row.createCell(15).setCellValue(tradeBean.getCashHoldingDays()==null?"":tradeBean.getCashHoldingDays());//实际占款天数10014
	    row.createCell(16).setCellValue(tradeBean.getInstitutionInfo().getShortName()==null?"":tradeBean.getInstitutionInfo().getShortName());//本方交易员
	    row.createCell(17).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
	    row.createCell(18).setCellValue(tradeBean.getInstitutionInfo().getFullName()==null?"":tradeBean.getInstitutionInfo().getFullName());//本方交易员
	    row.createCell(20).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方21位机构码
	    row.createCell(21).setCellValue(tradeBean.getContraPatryInstitutionInfo().getShortName()==null?"":tradeBean.getContraPatryInstitutionInfo().getShortName());//对方
	    row.createCell(22).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
	    row.createCell(23).setCellValue(tradeBean.getContraPatryInstitutionInfo().getFullName()==null?"":tradeBean.getContraPatryInstitutionInfo().getFullName());//对方交易员
	    row.createCell(25).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方21位机构码
	    row.createCell(36).setCellValue(tradeBean.getDealStatus()==null?"":tradeBean.getDealStatus().toString());//状态10105
	    row.createCell(37).setCellValue(tradeBean.getTradeInstrument()==null?"":tradeBean.getTradeInstrument().toString());//交易模型10315
	    row.createCell(38).setCellValue(tradeBean.getTradeMethod()==null?"":tradeBean.getTradeMethod().toString());//交易方式10317
	    row.createCell(39).setCellValue(tradeBean.getDealMode()==null?"":tradeBean.getDealMode().toString());//交易模式11657
	    row.createCell(40).setCellValue(tradeBean.getSettMode()==null?"":tradeBean.getSettMode().toString());//清算方式430
	    MarkerTakerEnum markerTaker= tradeBean.getSettlement().getMarkerTakerRole();//本方（泰隆）角色
	    if(MarkerTakerEnum.Taker.equals(markerTaker)){
			 //本方为发起方TAKER_,对手方为Maker_
			 returnRow(row,42,tradeBean.getSettlement());//本方
			 returnRow(row,52,tradeBean.getCounterPartySettlement());//对手方
		 }else{
			 //本方为受市方Maker_,对手方为TAKER_
			 returnRow(row,52,tradeBean.getSettlement());//本方
			 returnRow(row,42,tradeBean.getCounterPartySettlement());//对手方
		 }
	    row.createCell(63).setCellValue(tradeBean.getKindId()==null?"":tradeBean.getKindId());//交易品种48
	    row.createCell(64).setCellValue(marketIndicator);//市场标识10176
		out.flush();
		wb.write(out);
		LogManager.info("拆借交易保存EXCEL:"+tradeBean.getExecId());

//		wb.close();
		
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(out!=null){
				out.close();
			}
			if(wb!=null){
				wb.close();
			}
		}
	}
	/**
	 * 即远期转换
	 * @param marketIndicator
	 * @param excelPath
	 * @param tradeBean
	 * @throws IOException
	 */
	public static void addExcelSPTFWD(String marketIndicator,String excelPath,FxSptFwdBean tradeBean)throws IOException{
		Workbook wb=null;
		FileOutputStream out=null;
		try {
		FileInputStream fs = new FileInputStream(excelPath);//获取excel
		wb = getWorkbook(fs);//获取excel 
		String sheetName=(tradeBean.getSptFwdType().compareTo(SptFwdTypeEnum.SPOT)==0)?"SPOT":"FORWARD";
		Sheet sheet = wb.getSheet(sheetName);//获取工作表
		//Sheet[] list=wb.getSheet(arg0);
		int newRowNum = sheet.getLastRowNum()+1;//添加数据新行号
		out = new FileOutputStream(excelPath);//向excel中添加数据
		Row row = sheet.createRow(newRowNum);//在现有行号后追加数据
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//设置单元格的数据
	     row.createCell(0).setCellValue(tradeBean.getExecId()==null?"":tradeBean.getExecId());//成交编号17
	     if(tradeBean.getSptFwdType().compareTo(SptFwdTypeEnum.SPOT)==0){
	    	 row.createCell(4).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75

	     }else{
	    	 row.createCell(3).setCellValue(tradeBean.getDealDate()==null?"":sdf.format(tradeBean.getDealDate()));//交易日期75

	     }
	  //   marketIndicator=tradeBean.getSptFwdType().SPOT;
	     row.createCell(5).setCellValue(marketIndicator);//市场标识10176
	     row.createCell(7).setCellValue(tradeBean.getCcypair()==null?"":tradeBean.getCcypair());//货币对55
	     row.createCell(8).setCellValue(tradeBean.getPs()==null?"":tradeBean.getPs().toString());//本方交易方向54
	     row.createCell(10).setCellValue(tradeBean.getCurrencyInfo().getCcy1()==null?"":tradeBean.getCurrencyInfo().getCcy1());//基准货币10064
	     row.createCell(11).setCellValue(tradeBean.getCurrencyInfo().getCcy2()==null?"":tradeBean.getCurrencyInfo().getCcy2());//非基准货币10065
	     row.createCell(12).setCellValue(tradeBean.getCurrencyInfo().getCcy1Amt()==null?"":tradeBean.getCurrencyInfo().getCcy1Amt().toString());//基准货币金额11515
	     row.createCell(13).setCellValue(tradeBean.getCurrencyInfo().getCcy2Amt()==null?"":tradeBean.getCurrencyInfo().getCcy2Amt().toString());//非基准货币金额11516
	     row.createCell(14).setCellValue(tradeBean.getCurrencyInfo().getCcy()==null?"":tradeBean.getCurrencyInfo().getCcy());//交易货币15
	     row.createCell(15).setCellValue(tradeBean.getCurrencyInfo().getContraCcy()==null?"":tradeBean.getCurrencyInfo().getContraCcy());//对应货币11514
	     row.createCell(16).setCellValue(tradeBean.getCurrencyInfo().getAmt()==null?"":tradeBean.getCurrencyInfo().getAmt().toString());//交易量32
	     row.createCell(17).setCellValue(tradeBean.getCurrencyInfo().getContraAmt()==null?"":tradeBean.getCurrencyInfo().getContraAmt().toString());//对应货币交易量1056
	     row.createCell(18).setCellValue(tradeBean.getRiskLastQty()==null?"":tradeBean.getRiskLastQty().toString());//折美元交易量 10284
	    if(tradeBean.getSptFwdType().compareTo(SptFwdTypeEnum.SPOT)==0){
	    	row.createCell(19).setCellValue(tradeBean.getSettlType()==null?"":(tradeBean.getSettlType()=="0"?"TODAY":(tradeBean.getSettlType()=="1"?"TOM":(tradeBean.getSettlType()=="2"?"SPOT":tradeBean.getSettlType()))));//期限 63 
	    }else{
	    	row.createCell(19).setCellValue(tradeBean.getSettlType()==null?"":tradeBean.getSettlType());
	    }
	     row.createCell(20).setCellValue(tradeBean.getValueDate()==null?"":sdf.format(tradeBean.getValueDate()));//起息日 64
    	 row.createCell(22).setCellValue(tradeBean.getCurrencyInfo().getPoints()==null?"":tradeBean.getCurrencyInfo().getPoints().multiply(new BigDecimal(10000)).setScale(2, BigDecimal.ROUND_HALF_UP).toString());//点差195
	     row.createCell(21).setCellValue(tradeBean.getCurrencyInfo().getSpotRate()==null?"":tradeBean.getCurrencyInfo().getSpotRate().toString());//即期汇率 194
//	     if(tradeBean.getSptFwdType().compareTo(SptFwdTypeEnum.SPOT)!=0){
	    	 //远期交易,新版本即期也会有
		     row.createCell(23).setCellValue(tradeBean.getCurrencyInfo().getRate()==null?"":tradeBean.getCurrencyInfo().getRate().toString());//交易价格31=即期汇率+点差
//	     }
		 row.createCell(26).setCellValue(tradeBean.getInstitutionInfo().getShortName()==null?"":tradeBean.getInstitutionInfo().getShortName());//本方
	     row.createCell(27).setCellValue(tradeBean.getInstitutionInfo().getTradeName()==null?"":tradeBean.getInstitutionInfo().getTradeName());//本方交易员
	     row.createCell(28).setCellValue(tradeBean.getInstitutionInfo().getFullName()==null?"":tradeBean.getInstitutionInfo().getFullName());//fullname 
	     row.createCell(30).setCellValue(tradeBean.getInstitutionInfo().getInstitutionId()==null?"":tradeBean.getInstitutionInfo().getInstitutionId());//本方21位机构码
	     row.createCell(31).setCellValue(tradeBean.getContraPatryInstitutionInfo().getShortName()==null?"":tradeBean.getContraPatryInstitutionInfo().getShortName());//对方
	     row.createCell(32).setCellValue(tradeBean.getContraPatryInstitutionInfo().getTradeName()==null?"":tradeBean.getContraPatryInstitutionInfo().getTradeName());//对方交易员
	     row.createCell(33).setCellValue(tradeBean.getContraPatryInstitutionInfo().getFullName()==null?"":tradeBean.getContraPatryInstitutionInfo().getFullName());//fullnae
	     row.createCell(35).setCellValue(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()==null?"":tradeBean.getContraPatryInstitutionInfo().getInstitutionId());//对方21位机构码
	     row.createCell(46).setCellValue(tradeBean.getDealStatus()==null?"":tradeBean.getDealStatus().toString());//状态10105
	     row.createCell(47).setCellValue(tradeBean.getTradeInstrument()==null?"":tradeBean.getTradeInstrument().toString());//交易模型10315
	     row.createCell(48).setCellValue(tradeBean.getTradeMethod()==null?"":tradeBean.getTradeMethod().toString());//交易方式10317
	     row.createCell(49).setCellValue(tradeBean.getDealMode()==null?"":tradeBean.getDealMode().toString());//交易模式11657
	    // row.createCell(51).setCellValue(tradeBean.getSettMode()==null?"":tradeBean.getSettMode().toString());//清算方式430
	     //清算信息:需要根据发起方，买卖方向，成交货币，基准货币，综合判断
	     //四种：TAKER_DEALT_*:发起方买入交易货币；TAKER_CONTRA_*：发起方买入对应货币
	     	//Maker_DEALT_*:受市方买入交易货币；Maker_CONTRA_*：受市方买入对应货币
	     MarkerTakerEnum markerTaker= tradeBean.getSettlement().getMarkerTakerRole();//本方（泰隆）角色
	     String ps =tradeBean.getPs()==null?"":tradeBean.getPs().toString();//本方（泰隆）买卖方向（买卖基准货币的方向）
	     String ccy =tradeBean.getCurrencyInfo().getCcy()==null?"":tradeBean.getCurrencyInfo().getCcy();//交易货币
		 String ccy1=tradeBean.getCurrencyInfo().getCcy1()==null?"":tradeBean.getCurrencyInfo().getCcy1();//基准货币
		 boolean ccyFlag = ccy.equals(ccy1);//判断基准货币是否等于交易货币
		 if(MarkerTakerEnum.Taker.equals(markerTaker)){
			 //本方为发起方
			 if(("P".equals(ps)&&ccyFlag)||("S".equals(ps)&&!ccyFlag)){
				 //买交易币种，本方为TAKER_DEALT_，对手方为Maker_CONTRA_
				 returnRow(row,54,tradeBean.getSettlement());//本方
				 returnRow(row,84,tradeBean.getCounterPartySettlement());//对手方
				 
			 }else{
				//买对应币种，本方为TAKER_CONTRA_，对手方为Maker_DEALT_
				 returnRow(row,64,tradeBean.getSettlement());//本方
				 returnRow(row,74,tradeBean.getCounterPartySettlement());//对手方
				 
			 }
		 }else{
			 //本方为受市方
			 if(("P".equals(ps)&&ccyFlag)||("S".equals(ps)&&!ccyFlag)){
				 //买交易币种，本方为Maker_DEALT_，对手方为TAKER_CONTRA_
				 returnRow(row,74,tradeBean.getSettlement());//本方
				 returnRow(row,64,tradeBean.getCounterPartySettlement());//对手方
				 
			 }else{
				//买对应币种，本方为Maker_CONTRA_，对手方为TAKER_DEALT_
				 returnRow(row,84,tradeBean.getSettlement());//本方
				 returnRow(row,54,tradeBean.getCounterPartySettlement());//对手方
				 
			 }
		 }
		 if(tradeBean.getSptFwdType().compareTo(SptFwdTypeEnum.SPOT)==0){
			 //即期交易
			 row.createCell(96).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
		 }else{
			 //远期交易
			 row.createCell(95).setCellValue(tradeBean.getStatus()==null?"":tradeBean.getStatus().toString());//成交状态150
		 }
		 out.flush();
		 wb.write(out);
		 LogManager.info("即远期交易保存EXCEL:"+tradeBean.getExecId());
//		 wb.close();
		 
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			if(out!=null){
				out.close();
			}
			if(wb!=null){
				wb.close();
			}
		}
	}
	
	/**
	 * 赋值清算信息
	 * @param row
	 * @param index
	 * @param fxSettBean
	 */
	public static void returnRow(Row row,int index,FxSettBean fxSettBean){
		if(fxSettBean!=null){
			row.createCell(index).setCellValue(fxSettBean.getBankName()==null?"":fxSettBean.getBankName());//开户行名称/资金开户行，110/140
		    row.createCell(index+1).setCellValue(fxSettBean.getBankOpenNo()==null?"":fxSettBean.getBankOpenNo());//开户行biccode 138/144
		    row.createCell(index+2).setCellValue(fxSettBean.getBankAcctNo()==null?"":fxSettBean.getBankAcctNo());//开户行账号 207/208
		    row.createCell(index+3).setCellValue(fxSettBean.getAcctName()==null?"":fxSettBean.getAcctName());//收款行名称/资金账户户名 23/142
		    row.createCell(index+4).setCellValue(fxSettBean.getAcctOpenNo()==null?"":fxSettBean.getAcctOpenNo());//收款行biccode/支付系统行号 16/143
		    row.createCell(index+5).setCellValue(fxSettBean.getAcctNo()==null?"":fxSettBean.getAcctNo());//收款行账户/资金账号 15/173
		    row.createCell(index+6).setCellValue(fxSettBean.getIntermediaryBankName()==null?"":fxSettBean.getIntermediaryBankName());//中间行名称 209/210
		    row.createCell(index+7).setCellValue(fxSettBean.getIntermediaryBankBicCode()==null?"":fxSettBean.getIntermediaryBankBicCode());//中间行biccode 211/212
		    row.createCell(index+8).setCellValue(fxSettBean.getIntermediaryBankAcctNo()==null?"":fxSettBean.getIntermediaryBankAcctNo());//中间行账号 213/214
		    row.createCell(index+9).setCellValue(fxSettBean.getRemark()==null?"":fxSettBean.getRemark());//附言
		}
	}
	//外汇期权有不同
	public static void returnRowOtc(Row row,int index,FxSettBean fxSettBean){
		if(fxSettBean!=null){
			row.createCell(index).setCellValue(fxSettBean.getBankName()==null?"":fxSettBean.getBankName());//资金开户行，110
		    row.createCell(index+1).setCellValue(fxSettBean.getAcctName()==null?"":fxSettBean.getAcctName());//资金账户户名 23
		    row.createCell(index+2).setCellValue(fxSettBean.getAcctOpenNo()==null?"":fxSettBean.getAcctOpenNo());//支付系统行号 16
		    row.createCell(index+3).setCellValue(fxSettBean.getAcctNo()==null?"":fxSettBean.getAcctNo());//资金账号 15
		    row.createCell(index+4).setCellValue(fxSettBean.getRemark()==null?"":fxSettBean.getRemark());//附言
		}
	}
		
	@SuppressWarnings("resource")
	public static Workbook getWorkbook(InputStream is) throws IOException{
		Workbook wb = null;
		 if(!is.markSupported()){
			 is = new PushbackInputStream(is,8);
		 }
		 if (POIFSFileSystem.hasPOIFSHeader(is)) {//Excel2003及以下版本  
			 wb = (Workbook) new HSSFWorkbook(is);
		 }else if (DocumentFactoryHelper.hasOOXMLHeader(is)) {//Excel2007及以上版本  
			 wb = new XSSFWorkbook(is);
		 }else{
			 throw new IllegalArgumentException("你的Excel版本目前poi无法解析！");
		 }
		return wb;
	}
	
	public static String nullToEmpty(String s){
		return s == null ? "" : s;
	}
	
	public static String nullToEmpty(Object s){
		return s == null ? "" : s.toString();
	}
	
	public static String nullToEmpty(Date s){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		return s == null ? "" : sdf.format(s);
	}
	
	public static String nullToEmpty(BigDecimal s){
		return s == null ? "" : s.toPlainString();
	}
	
	public static String transSide(PsEnum ps){
		if(ps == null)
		{
			return "";
		}
		String side = "";
		switch (ps) {
		case P:
			side = "1-买入";
			break;
		
		case S:
			side = "4-卖出";
			break;

		default:
			break;
		} 
		return side;
	}
	
	public static String transOptSptSide(PsEnum ps){
		if(ps == null)
		{
			return "";
		}
		String side = "";
		switch (ps) {
		case P:
			side = "1-买";
			break;
		
		case S:
			side = "4-卖";
			break;

		default:
			break;
		} 
		return side;
	}
	
	
	public static String transMMSide(PsEnum ps){
		if(ps == null)
		{
			return "";
		}
		String side = "";
		switch (ps) {
		case P:
			side = "1-拆入";
			break;
		
		case S:
			side = "4-拆出";
			break;

		default:
			break;
		} 
		return side;
	}
	
	
	public static String transSide(String ps){
		String side = "";
		if("1".equals(ps)){
			side = "1-买";
			
		}else if("4".equals(ps)){
			side = "4-卖";	
		} 
		return side;
	}
	
	public static String transLegSide(String ps){
		String side = "";
		if("P".equals(ps)){
			side = "1-卖出货币并收取该货币利息";
		}else if("S".equals(ps)){
			side = "4-买入货币并支付该货币利息";
		}
		return side;
	}
	
	public static String transBasisMM(BasisEnum basisEnum){
		if(basisEnum == null)
		{
			return "";
		}
		String basis = "";
		switch (basisEnum) {
		case A360:
			basis = "1-实际/360";
			break;
		
		case A365:
			basis = "3-实际/365";
			break;
		
		default:
			break;
		}
		return basis;		
	}
	
	/***
	 * 即期
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxSpotTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		FxSptFwdBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("SPOT");
		int row = e.getWb().getSheet("SPOT").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			tradeBean = (FxSptFwdBean)trds.get(i);
			try {
				
			    e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
			    //交易日期
			    e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
			    //货币对55
			    e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcypair()));
			    //本方交易方向54
			    e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), transSide(tradeBean.getPs()));
			    //基准货币10064
			    e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy1()));
			    //非基准货币10065
			    e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy2()));
			    //基准金额
			    e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy1Amt()));
			    //非基准金额
			    e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy2Amt()));
			    //起息日 64
			    e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
			    //即期汇率 194
			    e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getSpotRate()));
			    //本方
			    e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
			    
			} catch (Exception e2) {
				e2.printStackTrace();
				LogManager.error("写入外汇即期交易出错:"+tradeBean.getExecId(), e2);
			}
			
		    row++;
		}
	}
	
	/***
	 * 远期
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxFwdTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		FxSptFwdBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("FORWARD");
		int row = e.getWb().getSheet("FORWARD").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			try {
				tradeBean = (FxSptFwdBean)trds.get(i);
				
			    e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
			    //交易日期
			    e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
			    //货币对55
			    e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcypair()));
			    //本方交易方向54
			    e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), transSide(tradeBean.getPs()));
			    //基准货币10064
			    e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy1()));
			    //非基准货币10065
			    e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy2()));
			    //基准金额
			    e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy1Amt()));
			    //非基准金额
			    e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy2Amt()));
			    //起息日 64
			    e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
			    //即期汇率 194
			    e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getSpotRate()));
			    //价格
			    e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getRate()));
			    //本方
			    e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
			    
			} catch (Exception e2) {
				e2.printStackTrace();
				LogManager.error("写入外汇远期交易出错:"+tradeBean.getExecId(), e2);
			}
		    row++;
		}
	}
	
	public static String transFxSwapSide(PsEnum ps){
		if(ps == null)
		{
			return "";
		}
		String side = "";
		switch (ps) {
		case P:
			side = "1-买";
			break;
		
		case S:
			side = "4-卖";
			break;

		default:
			break;
		} 
		return side;
	}
	
	/***
	 * 掉期
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxSwpTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		FxSwapBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("SWAP");
		int row = e.getWb().getSheet("SWAP").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			try {
				tradeBean = (FxSwapBean)trds.get(i);
				
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
			    //交易日期
			    e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
			    //货币对55
			    e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcypair()));
			    //Base CCY
			    e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcy1()));
			    //Term CCY
			    e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcy2()));
			    
			    //近端
			    //发起方方向624
			    e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transFxSwapSide(tradeBean.getPs()));
			    //基准金额
			    e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy1Amt()));
			    //非基准金额
			    e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy2Amt()));
			    //起息日588
			    e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
			    //即期汇率10112
			    e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getSpotRate()));
			    //远期点1073
			    e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getPoints()));
			    //交易价格637
			    e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getRate()));
		
			    //基准金额
			    e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFarCurrencyInfo().getCcy1Amt()));
			    //非基准金额
			    e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFarCurrencyInfo().getCcy2Amt()));
			    //起息日588
			    e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFarValueDate()));
			    //即期汇率10112
			    e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFarCurrencyInfo().getSpotRate()));
			    //远期点1073
			    e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFarCurrencyInfo().getPoints()));
			    //交易价格637
			    e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getFarCurrencyInfo().getRate()));

			    //本方
			    e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
			    
			} catch (Exception e2) {
				e2.printStackTrace();
				LogManager.error("写入外汇掉期交易出错:"+tradeBean.getExecId(), e2);
			}
		    row++;
		}
	}
	
	/***
	 * 拆借
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxMMTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		DepositsAndLoansFxBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("FCL");
		int row = e.getWb().getSheet("FCL").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			tradeBean = (DepositsAndLoansFxBean)trds.get(i);
			try {
				//成交编号17
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//交易日期75
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//交易货币15
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy()));
				//拆借利率31
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), tradeBean.getCurrencyInfo().getRate()==null?"":tradeBean.getCurrencyInfo().getRate().divide(new BigDecimal(100), 2).toString());
				//拆借金额32
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getAmt()));
				//本方交易方向54
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), transMMSide((tradeBean.getPs())));
				//计息基础10040
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transBasisMM(tradeBean.getBasis()));
				//起息日 64
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
				//还款日193
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMatuitydate()));
				//到期还款金额10289
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSettlCurrAmt2()));
				//实际占款天数10014
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCashHoldingDays()));
			    //本方
			    e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
			    
			} catch (Exception e2) {
				e2.printStackTrace();
				LogManager.error("写入外汇拆借交易出错:"+tradeBean.getExecId(), e2);
			}
		    row++;
		}
	}
	
	/***
	 * 
	 * 1-Vanilla,
	 * 100-Payer Swaption,
	 * 101-Receiver Swaption,
	 * 2-Capped,
	 * 102-Floor,
	 * 103-Call Spread,
	 * 104-Put Spread,
	 * 105-Straddle,
	 * 106-Strangle,
	 * 107-Risk Reversal,
	 * 108-Butterfly,
	 * 109-Custom Stratery
	 * @param optPayoutType
	 * @return
	 */
	public static String transOptPayoutType(String optPayoutType){
		if("1".equals(optPayoutType)){
			return "1=Vanilla";
			
		}else if("100".equals(optPayoutType)){
			return "100=Payer Swaption";
			
		}else if("101".equals(optPayoutType)){
			return "101=Receiver Swaption";
			
		}else if("2".equals(optPayoutType)){
			return "2=Capped";
			
		}else if("102".equals(optPayoutType)){
			return "102=Floor";
			
		}else if("103".equals(optPayoutType)){
			return "103=Call Spread";
			
		}else if("104".equals(optPayoutType)){
			return "104=Put Spread";
			
		}else if("105".equals(optPayoutType)){
			return "105=Straddle";
			
		}else if("106".equals(optPayoutType)){
			return "106=Strangle";
			
		}else if("107".equals(optPayoutType)){
			return "107=Risk Reversal";
			
		}else if("108".equals(optPayoutType)){
			return "108=Butterfly";
			
		}else if("109".equals(optPayoutType)){
			return "109=Custom Stratery";
		}
		return "";
	}
	
	/***
	 * 0-European,1-American
	 * @param derivativeExerciseStyle
	 * @return
	 */
	public static String transDerivativeExerciseStyle(String derivativeExerciseStyle){
		if("0".equals(derivativeExerciseStyle)){
			return "0=European";
		}else{
			return "1=American";
		}
	}
	
	public static String transSideOpt(PsEnum ps){
		if(ps == null) {return "";}
		String side = "";
		switch (ps) {
		case P:
			side = "1-买";
			break;
		
		case S:
			side = "4-卖";
			break;

		default:
			break;
		} 
		return side;
	}
	
	/***
	 * 0-Put,1-Call,2-Convertible,3-Put and Call,8-Other,9-None
	 * @param optPayoutType
	 * @return
	 */
	public static String transPutOrCall(String optPayoutType){
		if("0".equals(optPayoutType)){
			return "0-Put";
		}else if("1".equals(optPayoutType)){
			return "1-Call";
		}else if("2".equals(optPayoutType)){
			return "2-Convertible";
		}else if("3".equals(optPayoutType)){
			return "3-Put and Call";
		}else if("8".equals(optPayoutType)){
			return "8-Other";
		}else if("9".equals(optPayoutType)){
			return "9-None";
		}
		return "";
	}
	
	/***
	 * 1-Pips,2-Term Percentage,3-Base Percentage
	 * @param OptPremiumBasis
	 * @return
	 */
	public static String transOptPremiumBasis(String OptPremiumBasis){
		if("Pips".equals(OptPremiumBasis)){
			return "1-Pips";
			
		}else if("Term".equals(OptPremiumBasis)){
			return "2-%";
		}
		return "";
	}
	
	/***
	 * 期权
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxOptTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		FxOptionBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("FXOPTION");
		int row = e.getWb().getSheet("FXOPTION").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			tradeBean = (FxOptionBean)trds.get(i);
			try {
				//成交编号17
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//期权类型1482
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), transOptPayoutType(tradeBean.getOptPayoutType()));
				//行权截止时区10741
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExpireTimeZone()));
				//行权方式1299
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), transDerivativeExerciseStyle(tradeBean.getDerivativeExerciseStyle()));
				//交易日期75
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getTradeDate()));
				//货币对55
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getSymbol()));
				//买卖方向
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transSideOpt(tradeBean.getPs()));
				//CALL PUT
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transPutOrCall(tradeBean.getPutOrCall()));
				
				FxOptionCurrencyAmtBean callCcy = null;
				FxOptionCurrencyAmtBean putCcy = null;
				if("CALL".equals(tradeBean.getCcy1().getCurrencyAmtType())){
					callCcy = tradeBean.getCcy1();
					putCcy = tradeBean.getCcy2();
				}else{
					callCcy = tradeBean.getCcy2();
					putCcy = tradeBean.getCcy1();
				}
				//call
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(callCcy.getCurrency()));
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(callCcy.getPrincipal()));
				//put
		    	e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(putCcy.getCurrency()));
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(putCcy.getPrincipal())); 
				//行权日541
				e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getMaturityDate())); 
				//行权价202
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getStrikePrice())); 
				
				//子期权期权费率10779
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getPremium().getOptPremiumValue())); 
				//子期权期权费类型10778
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), transOptPremiumBasis(tradeBean.getPremium().getOptPremiumBasis())); 
				//子期权期权费币种10780
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getPremium().getOptPremiumCurrency())); 
				//子期权期权费总额10781
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getPremium().getOptPremiumAmt())); 
				//小数位数10429
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getPremium().getDecimalPlaces())); 
				//子期权波动率1188
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getVolatility())); 

				//本方
			    e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
			 
			} catch (Exception e2) {
				e2.printStackTrace();
				LogManager.error("写入外汇期权交易出错:"+tradeBean.getExecId(), e2);
			}
		    row++;
		}
	}
	
	/***
	 * 期权行权交易
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxOptSpotTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		FxSptFwdBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("EXOP");
		int row = e.getWb().getSheet("EXOP").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			tradeBean = (FxSptFwdBean)trds.get(i);
			try {
				
			    e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
			    //交易日期
			    e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
			    //货币对55
			    e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcypair()));
			    //本方交易方向54
			    e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), transOptSptSide(tradeBean.getPs()));
			    //基准货币10064
			    e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy1()));
			    //非基准货币10065
			    e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy2()));
			    //基准金额
			    e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy1Amt()));
			    //非基准金额
			    e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getCcy2Amt()));
			    //起息日 64
			    e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getValueDate()));
			    //即期汇率 194
			    e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCurrencyInfo().getSpotRate()));
			    //本方
			    e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
			    
			} catch (Exception e2) {
				e2.printStackTrace();
				LogManager.error("写入外汇即期交易出错:"+tradeBean.getExecId(), e2);
			}
			
		    row++;
		}
	}
	
	/***
	 *  0-Start and End;
		1-Start Only;
		2-End Only;
		3-None
	 * @param NotionalExchangeType
	 * @return
	 */
	public static String transNotionalExchangeType(String NotionalExchangeType){
		if("0".equals(NotionalExchangeType)){
			return "0-StartandEnd";
			
		}else if("1".equals(NotionalExchangeType)){
			return "1-StartOnly";
			
		}else if("2".equals(NotionalExchangeType)){
			return "2-EndOnly";
			
		}else if("3".equals(NotionalExchangeType)){
			return "3-None";
			
		}
		return "";
	}
	
	/**
	 * 
	 * @param NetGrossInd
	 * @return
	 */
	public static String transNetGrossInd(String NetGrossInd){
		if("1".equals(NetGrossInd)){
			return "1-双边净额清算";
			
		}else if("2".equals(NetGrossInd)){
			return "2-双边全额清算";
			
		}else if("3".equals(NetGrossInd)){
			return "3-集中净额清算";
			
		}
		return "";
	}
	
	public static String transLegPriceType(int LegPriceType){
		if(6 == LegPriceType){
			return "6-Spread(对应Floating)";
			
		}else if(3 == LegPriceType){
			return "3-FixedAmount(对应Fixed)";
			
		}
		return "";
	}
	
	/***
	 * 
	 * 0-Semi annually,1-Annually,2-Maturity,3-Monthly,4-Quarterly,5-BiWeekly,6-Weekly,7-Daily,7D-LEGSETTLTYPE_VALUE_7D,1W-LEGSETTLTYPE_VALUE_1W,2W-LEGSETTLTYPE_VALUE_2W,3W-LEGSETTLTYPE_VALUE_3W,1M-LEGSETTLTYPE_VALUE_1M,2M-LEGSETTLTYPE_VALUE_2M,3M-LEGSETTLTYPE_VALUE_3M,4M-LEGSETTLTYPE_VALUE_4M,5M-LEGSETTLTYPE_VALUE_5M,6M-LEGSETTLTYPE_VALUE_6M,7M-LEGSETTLTYPE_VALUE_7M,8M-LEGSETTLTYPE_VALUE_8M,9M-LEGSETTLTYPE_VALUE_9M,10M-LEGSETTLTYPE_VALUE_10M,11M-LEGSETTLTYPE_VALUE_11M,1Y-LEGSETTLTYPE_VALUE_1Y,2Y-LEGSETTLTYPE_VALUE_2Y,3Y-LEGSETTLTYPE_VALUE_3Y,4Y-LEGSETTLTYPE_VALUE_4Y,5Y-LEGSETTLTYPE_VALUE_5Y,6Y-LEGSETTLTYPE_VALUE_6Y,7Y-LEGSETTLTYPE_VALUE_7Y,8Y-LEGSETTLTYPE_VALUE_8Y,9Y-LEGSETTLTYPE_VALUE_9Y,10Y-LEGSETTLTYPE_VALUE_10Y,11Y-LEGSETTLTYPE_VALUE_11Y,12Y-LEGSETTLTYPE_VALUE_12Y,13Y-LEGSETTLTYPE_VALUE_13Y,14Y-LEGSETTLTYPE_VALUE_14Y,15Y-LEGSETTLTYPE_VALUE_15Y,20Y-LEGSETTLTYPE_VALUE_20Y,1D-LEGSETTLTYPE_VALUE_1D,10D-LEGSETTLTYPE_VALUE_10D,8-Ten Days,9-Nine Months,10-In Advance

	 * @param PaymentFrequency
	 * @return
	 */
	public static String transPaymentFrequency(String PaymentFrequency){
		if("0".equals(PaymentFrequency)){
			return "0-Semi annually";
			
		}else if("1".equals(PaymentFrequency)){
			return "1-Annually";
			
		}else if("2".equals(PaymentFrequency)){
			return "2-Maturity";
			
		}else if("3".equals(PaymentFrequency)){
			return "3-Monthly";
			
		}else if("4".equals(PaymentFrequency)){
			return "4-Quarterly";
			
		}else if("5".equals(PaymentFrequency)){
			return "5-BiWeekly";
			
		}else if("6".equals(PaymentFrequency)){
			return "6-Weekly";
			
		}else if("7".equals(PaymentFrequency)){
			return "7-Daily";
			
		}
		return PaymentFrequency;
	}
	
	/**
	 * 
	 * 0- Preceding Day,1- Following Day,2- Modified Following,3- Modified Proceeding
	 * @param LegCouponPaymentDateReset
	 * @return
	 */
	public static String transLegCouponPaymentDateReset(String LegCouponPaymentDateReset){
		if("0".equals(LegCouponPaymentDateReset)){
			return "0-Preceding Day";
			
		}else if("1".equals(LegCouponPaymentDateReset)){
			return "1-Following Day";
			
		}else if("2".equals(LegCouponPaymentDateReset)){
			return "2-Modified Following";
			
		}else if("3".equals(LegCouponPaymentDateReset)){
			return "3-Modified Proceeding";
			
		}
		return "";
	}
	
	
	public static String transLegDayCount(String LegDayCount){
		if("0".equals(LegDayCount)){
			return "0-A/A";
			
		}else if("1".equals(LegDayCount)){
			return "1-A/360";
			
		}else if("2".equals(LegDayCount)){
			return "2-30/360";
			
		}else if("3".equals(LegDayCount)){
			return "3-A/365";
			
		}else if("5".equals(LegDayCount)){
			return "5-A/365F";
			
		}
		return "";
	}
	
	
	/***
	 * 货币互换
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxCcsTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		SwapCrsBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("CCS");
		int row = e.getWb().getSheet("CCS").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			tradeBean = (SwapCrsBean)trds.get(i);
			try {
				//ExecID
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//Deal Date		
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsInfoBean().getTradeDate()));
				//Base CCY	
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsInfoBean().getCurrency1()));
				//Term CCY	
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsInfoBean().getCurrency2()));
				//NotionalExchangeType	
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), transNotionalExchangeType(tradeBean.getCcsInfoBean().getNotionalExchangeType()));
				//Value Date	
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsInfoBean().getSettlDate()));
				//Maturity Date
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsInfoBean().getMaturityDate()));
				
				//本方
			    e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
			    
				//Net Gross	
			    e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), transNetGrossInd(tradeBean.getCcsInfoBean().getNetGrossInd()));
			    
				//Interest Direction Self 1st leg	
			    e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), transLegSide(tradeBean.getCcsDetailBean().getLegPs()));
				//Order Qty 1st leg	
			    e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsDetailBean().getLegOrderQty()));
				//CCY 1st leg	
			    e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsDetailBean().getLegCurrency()));
				//Rate 1st leg	
			    e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsDetailBean().getLegLastPx()));
				//BenchmarkCurveName 1st leg	
			    e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsDetailBean().getLegBenchmarkCurveName()));
				//PriceType 1st Leg	
			    e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), transLegPriceType(tradeBean.getCcsDetailBean().getLegPriceType()));
				//CouponPaymentDateReset 1st Leg	
			    e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), transLegCouponPaymentDateReset(tradeBean.getCcsDetailBean().getLegCouponPaymentDateReset()));
				//Paymt Frequency 1st leg
			    e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getCcsDetailBean().getLegCouponPaymentFrequency()));
				//Fixing Frequency 1st leg	
			    e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getCcsDetailBean().getLegInterestAccrualResetFrequency()));
				//BenchmarkTenor 1st Leg	
			    e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getCcsDetailBean().getLegBenchmarkTenor()));
				//Daycount Rule 1st leg	
			    e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), transLegDayCount(tradeBean.getCcsDetailBean().getLegDayCount()));
				
				//Interest Direction Self 2nd Leg	
			    e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), transLegSide(tradeBean.getContraPatryCcsDetailBean().getLegPs()));
				//Order Qty 2nd Leg	
			    e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryCcsDetailBean().getLegOrderQty()));
				//CCY 2nd Leg	
			    e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryCcsDetailBean().getLegCurrency()));
				//Rate 2nd Leg	
			    e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryCcsDetailBean().getLegLastPx()));
				//BenchmarkCurveName 2nd Leg	
			    e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryCcsDetailBean().getLegBenchmarkCurveName()));
				//PriceType 2nd Leg	
			    e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), transLegPriceType(tradeBean.getContraPatryCcsDetailBean().getLegPriceType()));
				//CouponPaymentDateReset 2nd Leg	
			    e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), transLegCouponPaymentDateReset(tradeBean.getContraPatryCcsDetailBean().getLegCouponPaymentDateReset()));
				//Paymt Frequency 2nd Leg	
			    e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getContraPatryCcsDetailBean().getLegCouponPaymentFrequency()));
				//Fixing Frequency 2nd Leg	
			    e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getContraPatryCcsDetailBean().getLegInterestAccrualResetFrequency()));
				//BenchmarkTenor 2nd Leg
			    e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryCcsDetailBean().getLegBenchmarkTenor()));
				//Daycount Rule 2nd Leg
			    e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), transLegDayCount(tradeBean.getContraPatryCcsDetailBean().getLegDayCount()));

			} catch (Exception e2) {
				e2.printStackTrace();
				LogManager.error("写入货币互换交易出错:"+tradeBean.getExecId(), e2);
			}
		    row++;
		}
	}
	
	public static String transIrsLegSide(String ps){
		String side = "";
		if("P".equals(ps)){
			side = "1-卖出=收取";
		}else if("S".equals(ps)){
			side = "4-买入=支付";
		}
		return side;
	}
	
	public static String transIrsLegPriceType(int LegPriceType){
		if(6 == LegPriceType){
			return "6-Floating-浮动利率";
			
		}else if(3 == LegPriceType){
			return "3-Fixed-固定利率";
			
		}
		return "";
	}
	
	public static String transInterestFixDateAdjustment(String InterestFixDateAdjustment){
		if("0".equals(InterestFixDateAdjustment)){
			return "0-V-0";
			
		}else if("1".equals(InterestFixDateAdjustment)){
			return "1-V-1";
			
		}else if("2".equals(InterestFixDateAdjustment)){
			return "2-V-2";
			
		}
		return "";
	}
	
	/**
	 * 
	 * 0- Preceding Day,1- Following Day,2- Modified Following,3- Modified Proceeding
	 * @param LegCouponPaymentDateReset
	 * @return
	 */
	public static String transIrsLegCouponPaymentDateReset(String LegCouponPaymentDateReset){
		if("0".equals(LegCouponPaymentDateReset)){
			return "0-Preceding(上一营业日)";
			
		}else if("1".equals(LegCouponPaymentDateReset)){
			return "1-Following(下一营业日)";
			
		}else if("2".equals(LegCouponPaymentDateReset)){
			return "2-Modified Following(经调整的下一营业日)";
			
		}else if("3".equals(LegCouponPaymentDateReset)){
			return "3-Modified Proceeding(经调整的上一营业日)";
			
		}
		return "";
	}
	
	/***
	 * 外币利率互换
	 * @param e
	 * @param trds
	 * @param marketIndicator
	 * @throws Exception
	 */
	public static void writeFxIrsTrader(ExcelUtil e,List<TradeBaseBean> trds,String marketIndicator)throws Exception
	{
		SwapIrsBean tradeBean = null;
		int sheetIdx = e.getWb().getSheetIndex("IRS");
		int row = e.getWb().getSheet("IRS").getLastRowNum() + 1;
		for (int i = 0; i < trds.size(); i++) 
		{
			try {
				tradeBean = (SwapIrsBean)trds.get(i);
				//ExecID
				e.writeStr(sheetIdx, row, "A", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getExecId()));
				//TradeDate	
				e.writeStr(sheetIdx, row, "B", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getDealDate()));
				//ValueDate	
				e.writeStr(sheetIdx, row, "C", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getStartDate()));
				//MaturityDate	
				e.writeStr(sheetIdx, row, "D", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getEndDate()));
				//DealtCurrency	
				e.writeStr(sheetIdx, row, "E", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getCcy()));
				//DealtCurrencyAmount	
				e.writeStr(sheetIdx, row, "F", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsInfo().getNotCcyAmt()));
				//Net Gross	
				e.writeStr(sheetIdx, row, "G", e.getDefaultLeftStrCellStyle(), transNetGrossInd(tradeBean.getNetGrossInd()));
				
				//FirstLegInterestDirectionSelf	
				e.writeStr(sheetIdx, row, "H", e.getDefaultLeftStrCellStyle(), transIrsLegSide(tradeBean.getIrsDetail().getLegPs()));
				//1st leg CCY		
				e.writeStr(sheetIdx, row, "I", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsDetail().getLegCurrency()));
				//FirstLegNotionalAmount	
				e.writeStr(sheetIdx, row, "J", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsDetail().getLegQty()));
				//FirstLegPriceType	
				e.writeStr(sheetIdx, row, "K", e.getDefaultLeftStrCellStyle(), transIrsLegPriceType(tradeBean.getIrsDetail().getLegPriceType()));
				//FirstLegBenchmarkCurveName	
				e.writeStr(sheetIdx, row, "L", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsDetail().getLegCurveName()));
				//FirstLegLastPx	
				if(tradeBean.getIrsDetail().getLegPriceType() == LegPriceType.SPREAD){
					e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsDetail().getRateDiff()));
				}else{
					e.writeStr(sheetIdx, row, "M", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsDetail().getIntRate()));
				}
				//FirstLegBenchmarkTenor	
				e.writeStr(sheetIdx, row, "N", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getIrsDetail().getLegCurveTenor()));
				//FirstLegInterestAccrualResetFrequency	
				e.writeStr(sheetIdx, row, "O", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getIrsDetail().getRaterevcycle()));
				//FirstLegInterestFixDateAdjustment	
				e.writeStr(sheetIdx, row, "P", e.getDefaultLeftStrCellStyle(), transInterestFixDateAdjustment(tradeBean.getIrsDetail().getRateFixDateAdjustment()));
				//FirstLegDayCount	
				e.writeStr(sheetIdx, row, "Q", e.getDefaultLeftStrCellStyle(), transLegDayCount(tradeBean.getIrsDetail().getBasis()));
				//FirstLegCouponPaymentFrequency	
				e.writeStr(sheetIdx, row, "R", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getIrsDetail().getPaycycle()));
				
				//FirstLegCouponPaymentDateReset	
				e.writeStr(sheetIdx, row, "S", e.getDefaultLeftStrCellStyle(), transIrsLegCouponPaymentDateReset(tradeBean.getIrsDetail().getLegCouponPaymentDateReset()));
				//FirstLegInterestAccrualResetDateAdjust
				e.writeStr(sheetIdx, row, "T", e.getDefaultLeftStrCellStyle(), transIrsLegCouponPaymentDateReset(tradeBean.getIrsDetail().getRaterevDateAdjust()));
				
				
				//SecondLegInterestDirectionSelf	
				e.writeStr(sheetIdx, row, "U", e.getDefaultLeftStrCellStyle(), transIrsLegSide(tradeBean.getContraPatryIrsDetail().getLegPs()));
				//2nd leg CCY	
				e.writeStr(sheetIdx, row, "V", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryIrsDetail().getLegCurrency()));
				//SecondLegNotionalAmount
				e.writeStr(sheetIdx, row, "W", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryIrsDetail().getLegQty()));
				//SecondLegPriceType	
				e.writeStr(sheetIdx, row, "X", e.getDefaultLeftStrCellStyle(), transIrsLegPriceType(tradeBean.getContraPatryIrsDetail().getLegPriceType()));
				//SecondLegBenchmarkCurveName	
				e.writeStr(sheetIdx, row, "Y", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryIrsDetail().getLegCurveName()));
				//SecondLegLastPx	
				if(tradeBean.getContraPatryIrsDetail().getLegPriceType() == LegPriceType.SPREAD){
					e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryIrsDetail().getRateDiff()));
				}else{
					e.writeStr(sheetIdx, row, "Z", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryIrsDetail().getIntRate()));
				}
				//SecondLegBenchmarkTenor	
				e.writeStr(sheetIdx, row, "AA", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryIrsDetail().getLegCurveTenor()));
				//SecondLegInterestAccrualResetFrequency	
				e.writeStr(sheetIdx, row, "AB", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getContraPatryIrsDetail().getRaterevcycle()));
				//SecondLegInterestFixDateAdjustment	
				e.writeStr(sheetIdx, row, "AC", e.getDefaultLeftStrCellStyle(), transInterestFixDateAdjustment(tradeBean.getContraPatryIrsDetail().getRateFixDateAdjustment()));
				//SecondLegDayCount	
				e.writeStr(sheetIdx, row, "AD", e.getDefaultLeftStrCellStyle(), transLegDayCount(tradeBean.getContraPatryIrsDetail().getBasis()));
				//SecondLegCouponPaymentFrequency	
				e.writeStr(sheetIdx, row, "AE", e.getDefaultLeftStrCellStyle(), transPaymentFrequency(tradeBean.getContraPatryIrsDetail().getPaycycle()));
				//SecondLegCouponPaymentDateReset	
				e.writeStr(sheetIdx, row, "AF", e.getDefaultLeftStrCellStyle(), transIrsLegCouponPaymentDateReset(tradeBean.getContraPatryIrsDetail().getLegCouponPaymentDateReset()));
				//SecondLegInterestAccrualResetDateAdjust
				e.writeStr(sheetIdx, row, "AG", e.getDefaultLeftStrCellStyle(), transIrsLegCouponPaymentDateReset(tradeBean.getContraPatryIrsDetail().getRaterevDateAdjust()));
				
				//本方
			    e.writeStr(sheetIdx, row, "AH", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getShortName()));
			    //本方交易员
			    e.writeStr(sheetIdx, row, "AI", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "AJ", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getFullName()));
			    //本方21位机构码
			    e.writeStr(sheetIdx, row, "AK", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getInstitutionInfo().getInstitutionId()));
			    //对方 
			    e.writeStr(sheetIdx, row, "AL", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getShortName()));
			    //对方交易员
			    e.writeStr(sheetIdx, row, "AM", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getTradeName()));
			    //fullname
			    e.writeStr(sheetIdx, row, "AN", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getFullName()));
			    //对方21位机构码
			    e.writeStr(sheetIdx, row, "AO", e.getDefaultLeftStrCellStyle(), nullToEmpty(tradeBean.getContraPatryInstitutionInfo().getInstitutionId()));
				
			} catch (Exception e2) {
				LogManager.error("写入外币利率互换交易出错:"+tradeBean.getExecId(), e2);
			}
		    row++;
		}
	}
	
}
