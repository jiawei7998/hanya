package com.singlee.financial.cfets.fx;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.singlee.financial.cfets.config.ConfigBean;

import imix.ConfigError;
import imix.FieldNotFound;
import imix.IncorrectDataFormat;
import imix.IncorrectTagValue;
import imix.Message;
import imix.RuntimeError;
import imix.UnsupportedMessageType;
import imix.client.core.ImixSession;
import imix.client.core.Listener;
import imix.client.core.MessageCracker;
import imix.field.MsgType;
import imix.imix10.ExecutionReport;
import imix.imix10.Logout;

/**
 * CFETS消息监听服务
 * 
 * @author chenxh
 * 
 */
public class ImixFxClientListener extends MessageCracker implements Listener {

	/**
	 * 登录次数
	 */
	public static int count = 0;

	/**
	 * CFETS配置信息
	 */
	private ConfigBean configBean;

	@Autowired
	ImixFxCfetsApiService imixFxCfetsApiService;

	/**
	 * 收报线程队列管理
	 */
	private ThreadPoolExecutor socketServerThreadPool;

	private static Logger LogManager = Logger.getLogger(ImixFxClientListener.class);
	
	

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}
	@Override
	public void fromAdmin(Message message, ImixSession imixSession)
			throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue {
		LogManager.info("******* fromAdmin() **************");
		LogManager.debug((new StringBuilder("imixSession:")).append(imixSession).toString());
		LogManager.info("*******pre connection:" + count + "**********");
		try {
			LogManager.info("*******String(35):" + message.getHeader().getString(35).toString() + "**********");
		} catch (FieldNotFound e) {
			LogManager.error("fromAdmin:", e);
		}
		LogManager.info("**********************************");
		try {
			if ("5".equals(message.getHeader().getString(MsgType.FIELD))) {
				// 登出
				Logout logout = (Logout) message;
				// 5表示登录失败
				if (logout.isSetLogoutStatus()) {
					LogManager.info("用户登录失败,失败代码:" + logout.getLogoutStatus() + "[17-密码过期]");
				} else if (logout.isSetSessionStatus()) {
					LogManager.info("用户注销成功,代码:" + logout.getSessionStatus() + "[4-注销成功]");
				} else if (logout.isSetText()) {
					LogManager.info("登录失败,失败代码:" + logout.getText()+ "[2-用户信息验证失败]");
				} else {
					LogManager.info("登录失败,原因未知");
				}
			} else {
				count = 0;
				LogManager.info("***********************************");
				LogManager.info("******* 登录 成功 !************");
				LogManager.info("******* count reset to zero ! [" + count + "]******");
				LogManager.info("***********************************");
			}
		} catch (FieldNotFound e) {
			LogManager.error("fromAdmin:", e);
			return;
		}
	}
	@Override
	public void fromApp(Message message, ImixSession imixSession)
			throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
		LogManager.info("********fromApp:" + message);
		//用于解析
		LogManager.info(message.toString());
		messageCrack(message);
	}
	@Override
	public void onError(ImixSession imixSession, int type) {
		LogManager.info("connection failure:" + (new StringBuilder("imixSession:")).append(imixSession).toString());
		// 断开 重连
		if (1 == type) {
			try {
				LogManager.info("*******系统捕获FxClient错误消息,立即进行重新连接!**********");
				imixSession.start();
			} catch (ConfigError e) {
				LogManager.error("onError exception", e);
			}
		}

	}
	@Override
	public void onLogon(ImixSession imixSession) {
		LogManager.info("********onLogon********" + (new StringBuilder("imixSession:")).append(imixSession).toString());

	}
	@Override
	public void onLogout(ImixSession imixSession) {
		LogManager.info("onLogonOut:" + (new StringBuilder("imixSession:")).append(imixSession).toString());
		count++;
		LogManager.info("*******开始累计登录次数:" + count + "次**********");
		if (count > configBean.getReconCount()) {
			count = 0;
			LogManager.info("*******当前重连累计大于" + configBean.getReconCount() + "次,断开连接!**********");
			try {
				imixSession.stop();
			} catch (Exception e) {
				LogManager.error("停止服务异常", e);
			}
		}
	}
	@Override
	public void toApp(Message message, ImixSession imixSession) {
		LogManager.info("********toApp:" + message);
		LogManager.info("********" + (new StringBuilder("imixSession:")).append(imixSession).toString());
	}
	@Override
	public void onMessage(ExecutionReport message) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {
		// 报文类型
		String msgType = null;

		// 消息为空,程序结束
		if (message.isEmpty()) {
			LogManager.error("Cfets push message is empty", new RuntimeError());
			return;
		}

		// 获取报文类型
		msgType = message.getHeader().getString(MsgType.FIELD);

		// 如果消息类型不是8的,不进行处理
		if (!MsgType.EXECUTION_REPORT.equals(msgType)) {
			LogManager.error("Cfets packet type[" + msgType + "] is unsupporte!", new RuntimeError());
			return;
		}

		// 创建客户羰线程池管理对象
		if (null == socketServerThreadPool) {
			socketServerThreadPool = new ThreadPoolExecutor(configBean.getCorePoolSize(),
					configBean.getMaximumPoolSize(), configBean.getKeepAliveTime(), TimeUnit.SECONDS,
					new ArrayBlockingQueue<Runnable>(configBean.getArrayBlockingQueueSize()),
					new ThreadPoolExecutor.CallerRunsPolicy());
		}

		// 接收到消息后,开启一个线程
		socketServerThreadPool.execute(new ImixFxMessageProcess(message, imixFxCfetsApiService, configBean));
	}
}
