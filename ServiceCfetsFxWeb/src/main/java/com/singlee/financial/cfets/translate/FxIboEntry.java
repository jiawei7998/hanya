package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.fx.ImixFxMessageHandler;
import com.singlee.financial.pojo.DepositsAndLoansFxBean;
import com.singlee.financial.pojo.component.ExecStatus;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;

/**
 * 外币拆借
 */
public class FxIboEntry extends FxSettleEntry implements ImixFxMessageHandler<DepositsAndLoansFxBean> {

    /**
     * 方法已重载.转换外币拆借业务
     *
     * @param message CFETS报文对象
     * @param cfetsMm 外币拆借交易业务对象
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, DepositsAndLoansFxBean cfetsMm, String bankId) throws Exception {
        // 设置本方机构及清算信息
        convert(message, bankId, SettPartyEnum.TheParty, cfetsMm.getInstitutionInfo(), cfetsMm.getSettlement());
        // 设置对手方机构及清算信息  外币债券特殊处理  没有对应货币,故上送本方清算路径即可
        convert(message, bankId, SettPartyEnum.CounterParty, cfetsMm.getContraPatryInstitutionInfo(), cfetsMm.getCounterPartySettlement());
        // 币种基础
        FxCurrencyBean currencyInfo = new FxCurrencyBean();
        // 外币拆借
        //币种15
        currencyInfo.setCcy(message.isSetCurrency() ? message.getCurrency().getValue() : null);
        //拆借利率31
        currencyInfo.setRate(message.isSetLastPx() ? BigDecimal.valueOf(message.getLastPx().getDoubleValue() * 100) : null);
        //拆借金额32
        currencyInfo.setAmt(message.isSetLastQty() ? BigDecimal.valueOf(message.getLastQty().getDoubleValue()) : null);
        cfetsMm.setCurrencyInfo(currencyInfo);
        // 交易模式11657
        cfetsMm.setDealMode(convert(message.isSetTradingMode() ? message.getTradingMode() : null));
        //交易方式10317
        cfetsMm.setTradeMethod(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        //交易方式(现阶段CSTP没有)
        cfetsMm.setStatus(ExecStatus.Finish);

        //交易品种48
        cfetsMm.setKindId(message.isSetSecurityID() ? message.getSecurityID().getValue() : null);
        //拆借期限63
        cfetsMm.setSettlType(message.isSetSettlType() ? message.getSettlType().getValue() : null);
        //应计利息10002
        cfetsMm.setAccruedInterestTotal(message.isSetAccruedInterestTotalAmt() ? BigDecimal.valueOf(message.getAccruedInterestTotalAmt().getDoubleValue()) : null);
        //到期还款金额10289
        cfetsMm.setSettlCurrAmt2(message.isSetSettlCurrAmt2() ? BigDecimal.valueOf(message.getSettlCurrAmt2().getDoubleValue()) : null);
        //实际占款天数10014
        cfetsMm.setCashHoldingDays(message.isSetCashHoldingDays() ? message.getCashHoldingDays().getUnionTypeValue() : null);
        //计息基准10040
        cfetsMm.setBasis(convert(message.isSetDayCount() ? message.getDayCount() : null));
        // 非美元货币折美元价格10284
        cfetsMm.setRiskLastQty(message.isSetRiskLastQty() ? BigDecimal.valueOf(message.getRiskLastQty().getDoubleValue()) : null);
        //交易日75
        cfetsMm.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);
        //起息日64
        cfetsMm.setValueDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : null);
        //到日期193
        cfetsMm.setMatuitydate(message.isSetSettlDate2() ? message.getSettlDate2().getDateValue() : null);
        //成交编号17
        cfetsMm.setExecId(message.isSetExecID() ? message.getExecID().getValue() : null);
        //成交单状态10105
        cfetsMm.setDealStatus(convert(message.isSetDealTransType() ? message.getDealTransType() : null));
        // 买卖方向54
        cfetsMm.setPs(convert(message.isSetSide() ? message.getSide() : null, cfetsMm.getSettlement().getMarkerTakerRole()));

        // 清算类型430:默认净额清算
        cfetsMm.setSettMode(convert(message.isSetNetGrossInd() ? message.getNetGrossInd() : null));
        //交易模型10315
        cfetsMm.setTradeInstrument(convert(message.isSetTradeInstrument() ? message.getTradeInstrument() : null));
        //交易确认标识
        cfetsMm.setCfetsCnfmIndicator(message.isSetCFETSCnfmIndicator() ? String.valueOf(message.getCFETSCnfmIndicator().getValue()) : "");
    }
}
