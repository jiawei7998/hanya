package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.fx.ImixFxMessageHandler;
import com.singlee.financial.pojo.FxSwapBean;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.RuntimeError;
import imix.field.NoLegs;
import imix.imix10.ExecutionReport;

import java.math.BigDecimal;

/**
 * 外汇掉期
 */
public class FxSwapEntry extends FxSettleEntry implements ImixFxMessageHandler<FxSwapBean> {
    /**
     * 方法已重载.转换外汇掉期业务
     *
     * @param message     CFETS报文对象
     * @param cfetsFxSwap 外汇掉期交易业务对象
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, FxSwapBean cfetsFxSwap, String bankId) throws Exception {
        // 外汇掉期交易近端币种对应的金额及汇率信息
        FxCurrencyBean currencyInfo = null;
        // 外汇掉期交易远端币种对应的金额及汇率信息
        FxCurrencyBean farCurrencyInfo = null;
        // 外汇掉期金额信息列表个数
        int noLegsCount = -1;

        // 实例化外汇掉期交易近端币种对应的金额及汇率信息
        currencyInfo = new FxCurrencyBean();
        // 实例化外汇掉期交易远端币种对应的金额及汇率信息
        farCurrencyInfo = new FxCurrencyBean();

        // 设置本方机构及清算信息[成交货币/对应货币]
        convert(message, bankId, SettPartyEnum.TheParty, cfetsFxSwap.getInstitutionInfo(), cfetsFxSwap.getSettlement(),cfetsFxSwap.getFarSettlement());
        // 设置对手方机构及清算信息[成交货币/对应货币]
        convert(message, bankId, SettPartyEnum.CounterParty, cfetsFxSwap.getContraPatryInstitutionInfo(),cfetsFxSwap.getCounterPartySettlement(), cfetsFxSwap.getFarCounterPartySettlement());
        //交易状态150
        cfetsFxSwap.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));
        /*
         * 掉期基础信息
         */
        // CFETS成交编号17
        cfetsFxSwap.setExecId(message.isSetExecID() ? message.getExecID().getValue() : null);
        // 成交单状态10105
        cfetsFxSwap.setDealStatus(convert(message.isSetDealTransType() ? message.getDealTransType() : null));
        // 交易成交时间10318
        cfetsFxSwap.setDealDateTime(message.isSetTradeTime() ? message.getTradeTime().getDateValue() : null);
        // 交易日期75
        cfetsFxSwap.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : null);
        // 买卖方向54
        cfetsFxSwap.setPs(convert(message.isSetSide() ? message.getSide() : null, cfetsFxSwap.getSettlement().getMarkerTakerRole()));
        // 交易模式11657
        cfetsFxSwap.setDealMode(convert(message.isSetTradingMode() ? message.getTradingMode() : null));
        //交易方式10317
        cfetsFxSwap.setTradeMethod(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        //交易模型10315
        cfetsFxSwap.setTradeInstrument(convert(message.isSetTradeInstrument() ? message.getTradeInstrument() : null));
        // 交易时间10318
        cfetsFxSwap.setTradeTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : null);
        // 清算类型430
        cfetsFxSwap.setSettMode(convert(message.isSetNetGrossInd() ? message.getNetGrossInd() : null));
        //交易类型（市场标识）10176
        cfetsFxSwap.setMarketIndicator(message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : null);

        // 获取金额列表个数
        noLegsCount = message.isSetNoLegs() ? message.get(new NoLegs()).getValue() : 0;

        logger.debug("Fx Swap NoLegs Value:" + noLegsCount);

        // 如果金额列表没有2个,则表示该笔交易不完整
        if (noLegsCount != 2) {
            throw new RuntimeError("Fx Swap NoLegs Count Is Not Set Or Value Is Not Equals 2!");
        }

        // 近端交易币种
        currencyInfo.setCcy(message.isSetCurrency() ? message.getCurrency().getValue() : null);
        // 近端交易对应货币
        currencyInfo.setContraCcy(message.isSetContraCurrency() ? message.getContraCurrency().getValue() : null);
        // 设置近端交易金额汇率等信息
        convert(message, 1, currencyInfo, cfetsFxSwap);
        // 设置近端金额等信息
        cfetsFxSwap.setCurrencyInfo(currencyInfo);

        // 近端根据方向判断币种
//		cfetsFxSwap.getSettlement().setSettCcy(
//				cfetsFxSwap.getPs().equals(PsEnum.P) ? currencyInfo.getCcy() : currencyInfo.getContraCcy());
//		cfetsFxSwap.getCounterPartySettlement().setSettCcy(
//				cfetsFxSwap.getPs().equals(PsEnum.P) ? currencyInfo.getContraCcy() : currencyInfo.getCcy());

        // 远端交易币种
        farCurrencyInfo.setCcy(message.isSetCurrency() ? message.getCurrency().getValue() : null);
        // 远端交易对应货币
        farCurrencyInfo.setContraCcy(message.isSetContraCurrency() ? message.getContraCurrency().getValue() : null);
        // 设置远端交易金额汇率等信息
        convert(message, 2, farCurrencyInfo, cfetsFxSwap);
        // 设置远端金额等信息
        cfetsFxSwap.setFarCurrencyInfo(farCurrencyInfo);

        // 远端根据方向判断币种
//		cfetsFxSwap.getFarSettlement().setSettCcy(
//				PsEnum.P.equals(cfetsFxSwap.getPs()) ? farCurrencyInfo.getContraCcy() : farCurrencyInfo.getCcy());
//		cfetsFxSwap.getFarCounterPartySettlement().setSettCcy(
//				PsEnum.P.equals(cfetsFxSwap.getPs()) ? farCurrencyInfo.getCcy() : farCurrencyInfo.getContraCcy());

        //货币对55
        cfetsFxSwap.setCcypair(message.isSetSymbol() ? message.getSymbol().getValue() : null);
        //基准货币10064
        cfetsFxSwap.setCcy1(message.isSetCurrency1() ? message.getCurrency1().getValue() : null);
        //非基准货币10065
        cfetsFxSwap.setCcy2(message.isSetCurrency2() ? message.getCurrency2().getValue() : null);

        //交易确认标识11049：是Y,即此交易需要交易后确认，为N 不需要;默认值为Y,防止需要确认的交易漏掉，不需要的发送确认也没关系的。
        cfetsFxSwap.setCfetsCnfmIndicator(message.isSetCFETSCnfmIndicator() ? String.valueOf(message.getCFETSCnfmIndicator().getValue()) : "");

        //交收方式
        cfetsFxSwap.setDeliveryType(message.isSetDeliveryType() ? message.getDeliveryType().getUnionTypeValue() : "");
    }

    /**
     * 方法已重载.转换外汇掉期交易金额汇率等信息
     *
     * @param message      CFETS报文对象
     * @param currencyBean
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    void convert(ExecutionReport message, int index, FxCurrencyBean currencyBean, FxSwapBean cfetsFxSwap) throws FieldConvertError, FieldNotFound {
        // 外汇掉期金额信息列表
        imix.imix10.ExecutionReport.NoLegs noLegs = null;
        // 获取近端金额信息
        noLegs = new imix.imix10.ExecutionReport.NoLegs();
        message.getGroup(index, noLegs);
        //624
        currencyBean.setLegSide(noLegs.isSetLegSide() ? String.valueOf(noLegs.getLegSide().getValue()) : null);
        //期限587
        currencyBean.setLegSettlType(noLegs.isSetLegSettlType() ? noLegs.getLegSettlType().getValue() : null);
        // 交易货币金额685
        currencyBean.setAmt(noLegs.isSetLegOrderQty() ? new BigDecimal(noLegs.getLegOrderQty().getValue()) : null);
        //折美元金额10132
        currencyBean.setLegRiskOrderQty(noLegs.isSetLegRiskOrderQty() ? new BigDecimal(noLegs.getLegRiskOrderQty().getValue()) : null);
        // 交易对应货币金额1074
        currencyBean.setContraAmt(noLegs.isSetLegCalculatedCcyLastQty() ? new BigDecimal(noLegs.getLegCalculatedCcyLastQty().getValue()) : null);
        // 交易成交价格637
        currencyBean.setRate(noLegs.isSetLegLastPx() ? new BigDecimal(noLegs.getLegLastPx().getValue()) : null);
        // 交易汇率升水点，近端远期点1073
        currencyBean.setPoints(noLegs.isSetLegLastForwardPoints() ? new BigDecimal(noLegs.getLegLastForwardPoints().getValue()) : null);
        // 交易即期参考价格,近端即期汇率10112
        currencyBean.setSpotRate(noLegs.isSetLegLastSpotRate() ? new BigDecimal(noLegs.getLegLastSpotRate().getValue()) : null);
        //TODO xuqq本人建议还是放到currencyBean里面
        if (1 == index) {
            // 近端起息日588
            cfetsFxSwap.setValueDate(noLegs.isSetLegSettlDate() ? noLegs.getLegSettlDate().getDateValue() : null);
        } else {
            // 远端起息日588
            cfetsFxSwap.setFarValueDate(noLegs.isSetLegSettlDate() ? noLegs.getLegSettlDate().getDateValue() : null);
        }
        //基准货币
        currencyBean.setCcy1(message.isSetCurrency1() ? message.getCurrency1().getValue() : null);
        //基准货币金额11599
        currencyBean.setCcy1Amt(noLegs.isSetlegCurrency1Amt() ? new BigDecimal(noLegs.getlegCurrency1Amt().getValue()) : null);
        //非基准货币
        currencyBean.setCcy2(message.isSetCurrency2() ? message.getCurrency2().getValue() : null);
        //非基准货币金额11600
        currencyBean.setCcy2Amt(noLegs.isSetlegCurrency2Amt() ? new BigDecimal(noLegs.getlegCurrency2Amt().getValue()) : null);
    }

}
