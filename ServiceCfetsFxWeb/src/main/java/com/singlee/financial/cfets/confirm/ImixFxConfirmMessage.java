package com.singlee.financial.cfets.confirm;

import imix.field.MarketIndicator;
import imix.imix10.Confirmation;
import imix.imix10.ExecutionReport;

import org.apache.log4j.Logger;

import com.singlee.financial.cfets.fx.ImixFxClient;

/**
 * CFETS消息线程序处理
 * 
 * @author ym
 * 
 */

public class ImixFxConfirmMessage {
	// Cfets消息线程处理实现类
	private ImixFxConfirmMessageProcess confirmMessageProcessImpl;

	private static Logger LogManager = Logger.getLogger(ImixFxConfirmMessage.class);

	public ImixFxConfirmMessage(ExecutionReport message) {
		confirmMessageProcessImpl = new ImixFxConfirmMessageProcess();
		// Cfets交易类型标识
		String marketindicator = null;
		Confirmation remessage = new Confirmation();
		ImixFxClient imixFxClient = new ImixFxClient();
		try {

			marketindicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";

			LogManager.info("ExecutionReport Received marketIndicator:" + marketindicator);

			if (marketindicator.equals(MarketIndicator.FXSWP) || marketindicator.equals(MarketIndicator.GOLDSWP)) {
				// FX SWAP掉期交易
				// FxSwapBean cfetsFxSwap = new FxSwapBean();
				// 组装交易确认报文要素
				confirmMessageProcessImpl.swapConvert(message, remessage);
				imixFxClient.sendMessage(remessage);
				LogManager.debug("外汇掉期业务报文对象：" + remessage);
			} else if (marketindicator.equals(MarketIndicator.FXSPT)
					|| marketindicator.equals(MarketIndicator.GOLDSPT)) {
				// SPT即期交易
				// FxSptFwdBean sptFwd = new FxSptFwdBean();
				// 组装交易确认报文要素
				confirmMessageProcessImpl.sptConvert(message, remessage);
				imixFxClient.sendMessage(remessage);
				LogManager.debug("外汇即期业务报文对象：" + remessage);
			} else if (marketindicator.equals(MarketIndicator.FXFOW) || marketindicator.equals(MarketIndicator.FXNDF)
					|| marketindicator.equals(MarketIndicator.GOLDFWD)) {
				// FWD远期交易
				// FxSptFwdBean sptFwd = new FxSptFwdBean();
				// 组装交易确认报文要素
				confirmMessageProcessImpl.fwdConvert(message, remessage);
				imixFxClient.sendMessage(remessage);
				LogManager.debug("外汇远期业务报文对象：" + remessage);
			} else if (marketindicator.equals(MarketIndicator.FXOPT)) {
				// FX OTC外汇期交易

			} else if (marketindicator.equals(MarketIndicator.FXCRS)) {
				// CRS货币互换
				// SwapCrsBean crsBean = new SwapCrsBean();
				// 组装交易确认报文要素
				confirmMessageProcessImpl.crsConvert(message, remessage);
				imixFxClient.sendMessage(remessage);
				LogManager.debug("货币互换业务报文对象：" + remessage);
			} else if (marketindicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {
				// MM外币拆借
				// DepositsAndLoansBean cfetsMm = new DepositsAndLoansBean();
				// 组装交易确认报文要素
				confirmMessageProcessImpl.mmConvert(message, remessage);
				imixFxClient.sendMessage(remessage);
				LogManager.debug("外币拆借业务报文对象：" + remessage);
			} else if (marketindicator.equals(MarketIndicator.FXIRS)) {
				// IRS利率互换
				// SwapIrsBean irsBean = new SwapIrsBean();
				// 组装交易确认报文要素
				confirmMessageProcessImpl.irsConvert(message, remessage);
				imixFxClient.sendMessage(remessage);
				LogManager.debug("外币利率互换业务报文对象：" + remessage);
			}

		} catch (Exception e) {
			LogManager.error("CFETS 线程处理异常!", e);
		}

	}
}
