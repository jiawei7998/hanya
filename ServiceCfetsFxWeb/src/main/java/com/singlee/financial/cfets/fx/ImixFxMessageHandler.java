package com.singlee.financial.cfets.fx;

import imix.imix10.ExecutionReport;

/**
 * 转换消息入口
 *
 * @param <T>
 * @author shenzl
 */
public interface ImixFxMessageHandler<T> {
    /**
     * 消息转换类
     *
     * @param message
     * @param tradeBase
     * @param bankId
     * @return
     * @throws Exception
     */
    void convert(ExecutionReport message, T tradeBase, String bankId) throws Exception;

}
