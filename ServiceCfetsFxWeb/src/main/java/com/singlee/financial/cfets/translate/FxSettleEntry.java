package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.util.CfetsUtils;
import com.singlee.financial.pojo.component.MarkerTakerEnum;
import com.singlee.financial.pojo.component.PsEnum;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.financial.pojo.trade.InstitutionBean;
import imix.FieldNotFound;
import imix.field.*;
import imix.imix10.ExecutionReport;
import org.springframework.beans.BeanUtils;

/**
 * 用于解析清算信息及相关机构信息
 *
 * @author shenzl
 * @date 2021/10/26
 */
public class FxSettleEntry extends FxBaseEntry {

    /**
     * 方法已重载.转换清算信息
     *
     * @param message     Cfets报文对象
     * @param bankId      机构ID银行唯一标识号
     * @param marketTaker 本方/对手方标识
     * @param institution
     * @param settBeans
     * @throws FieldNotFound
     */
    public void convert(ExecutionReport message, String bankId, SettPartyEnum marketTaker, InstitutionBean institution,SettBaseBean... settBeans) throws FieldNotFound {
        // 做市商/受市商清算信息列表
        imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs = null;
        ExecutionReport.NoLegs nolegs = null;
        // 清算信息列表个数
        int noPartyIdsCount = -1;
        // 做市商/受市商机构编号
        String partyID = null;
        // 做市商/受市商角色
        int partyRole = -1;
        // 获取清算信息列表个数
        noPartyIdsCount = message.isSetNoPartyIDs() ? message.get(new NoPartyIDs()).getValue() : 0;
        // 循环处理清算信息列表
        for (int i = 1; i <= noPartyIdsCount; i++) {
            // 实例做市商/受市商清算信息列表对象
            noPartyIDs = new imix.imix10.ExecutionReport.NoPartyIDs();
            // 获取做市商/受市商清算信息列表数据
            message.getGroup(i, noPartyIDs);
            // 获取机构编号
            partyID = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : CfetsUtils.EMPTY;
            // 获取做市商/受市商角色
            partyRole = noPartyIDs.isSetPartyRole() ? noPartyIDs.getPartyRole().getValue() : 0;

            logger.debug("partyID:" + partyID + ",partyRole:" + partyRole);
            /*
             * 判断当前清算信息是本方还是对手方
             */
            // 如果不是当前要取的信息,则继续读取下条记录
            if (!(partyID.equalsIgnoreCase(bankId) ? SettPartyEnum.TheParty : SettPartyEnum.CounterParty).equals(marketTaker)) {
                continue;
            }
            // 获取做市商/受市商清算信息列表数据
            message.getGroup(i, noPartyIDs);
            // 针对货币互换交易
            nolegs = message.isSetNoLegs() ? new ExecutionReport.NoLegs() : null;
            if (null != nolegs) {
                message.getGroup(i, nolegs);
            }
            // 转换清算信息
            convert(message, noPartyIDs, nolegs, institution, settBeans);
        } // end for
    }

    /**
     * 方法已重载.转换清算明细信息到清算对象中
     *
     * @param noPartyIDs
     * @param institution
     * @param settBeans
     * @throws FieldNotFound
     */
    public void convert(ExecutionReport message, imix.imix10.ExecutionReport.NoPartyIDs noPartyIDs, ExecutionReport.NoLegs nolegs,InstitutionBean institution, SettBaseBean... settBeans) throws FieldNotFound {
        // 做市商/受市商机构编号
        String partyID = null;
        // 做市商/受市商角色
        int partyRole = -1;
        // 清算信息明细信息
        imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = null;
        imix.imix10.ExecutionReport.NoFxOptions noFxOption = null;
        // 清算信息类型
        int partySubIdType = -1;
        // 清算信息类型对应的值
        String partySubIdValue = null;
        // 清算信息明细个数
        int noPartySubIdsCount = -1;
        // 交易货币清算信息
        FxSettBean settInfo = null;
        // 交易对应货几币清算信息
        FxSettBean contraSettInfo = null;
        // 获取机构编号
        partyID = noPartyIDs.isSetPartyID() ? noPartyIDs.getPartyID().getValue() : CfetsUtils.EMPTY;
        // 获取做市商/受市商角色
        partyRole = noPartyIDs.isSetPartyRole() ? noPartyIDs.getPartyRole().getValue() : 0;

        logger.debug("partyID:" + partyID + ",partyRole:" + partyRole);
        // 交易货币清算信息
        settInfo = new FxSettBean();
        // 交易对应货币清算信息
        contraSettInfo = new FxSettBean();

        // 机构唯一编号
        institution.setInstitutionId(partyID);

        // 设置当前清算信息受市商/做市商角色
        if (114 == partyRole) {
            settInfo.setMarkerTakerRole(MarkerTakerEnum.Taker);
            contraSettInfo.setMarkerTakerRole(MarkerTakerEnum.Taker);
        } else {
            settInfo.setMarkerTakerRole(MarkerTakerEnum.Marker);
            contraSettInfo.setMarkerTakerRole(MarkerTakerEnum.Marker);
        }

        settInfo.setSettCcy(message.isSetCurrency() ? message.getCurrency().getValue() : "");
        contraSettInfo.setSettCcy(message.isSetContraCurrency() ? message.getContraCurrency().getValue() : "");
        // 设置清算信息明细个数
        noPartySubIdsCount = noPartyIDs.isSetNoPartySubIDs() ? noPartyIDs.get(new NoPartySubIDs()).getValue() : 0;

        // 循环读取清算信息明细
        for (int j = 1; j <= noPartySubIdsCount; j++) {
            // 实例清算信息明细信息对象
            noPartySubIDs = new imix.imix10.ExecutionReport.NoPartyIDs.NoPartySubIDs();
            // 获取清算信息明细信息对象数据
            noPartyIDs.getGroup(j, noPartySubIDs);
            // 获取清算信息类型对应的值
            partySubIdValue = noPartySubIDs.isSetPartySubID() ? noPartySubIDs.getPartySubID().getValue()
                    : CfetsUtils.EMPTY;
            // 获取清算信息类型
            partySubIdType = noPartySubIDs.isSetPartySubIDType() ? noPartySubIDs.getPartySubIDType().getValue() : 0;

            // 根据清算信息类型设置清算信息实体对象
            switch (partySubIdType) {
                case PartySubIDType.TRADER_NAME:
                    // 交易员名称-101
                    institution.setTradeId(partySubIdValue);
                    institution.setTradeName(partySubIdValue);
                    break;
                case PartySubIDType.SHORT_LEGAL_NAME_OF_FIRM:
                    // 机构简称-102
                    institution.setShortName(partySubIdValue);
                    break;
                case PartySubIDType.FULL_LEGAL_NAME_OF_FIRM:
                    // 机构全称-5
                    institution.setFullName(partySubIdValue);
                    break;
                case PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM:
                    // 机构中文全称-124
                    institution.setFullNameCn(partySubIdValue);
                    break;
                case PartySubIDType.SETTLEMENT_BANK_NAME:
                    // 开户行名称-110
                    settInfo.setBankName(partySubIdValue);
                    break;
                case PartySubIDType.SETTLEMENT_CURRENCY_NAME:
                    // 开户行SWIFTCODE/开户行行号-138
                    settInfo.setBankOpenNo(partySubIdValue);
                    break;
                case PartySubIDType.CORRESPONDENT_BANK_ACCOUNT:
                    // 开户行在中间行的资金账号-207
                    settInfo.setBankAcctNo(partySubIdValue);
                    break;
                case PartySubIDType.CASH_ACCOUNT_NAME:
                    // 账户名称-23
                    settInfo.setAcctName(partySubIdValue);
                    break;
                case PartySubIDType.SWIFT_BIC_CODE:
                    // 资金账户行SWIFT CODE/资金账户行-16
                    settInfo.setAcctOpenNo(partySubIdValue);
                    break;
                case PartySubIDType.CASH_ACCOUNT_NUMBER:
                    // 资金账户-15
                    settInfo.setAcctNo(partySubIdValue);
                    break;
                case PartySubIDType.INTERMEDIARY_BANK:
                    // 中间行名称-209
                    settInfo.setIntermediaryBankName(partySubIdValue);
                    break;
                case PartySubIDType.INTERMEDIARY_BANK_BIC_CODE:
                    // 中间行SWIFT CODE/中间行行号-211
                    settInfo.setIntermediaryBankBicCode(partySubIdValue);
                    break;
                case PartySubIDType.INTERMEDIARY_BANK_ACCOUNT:
                    // 中间行在开户行的资金账号-213
                    settInfo.setIntermediaryBankAcctNo(partySubIdValue);
                    break;
                case PartySubIDType.SETTLEMENT_CURRENCY_REMARK:
                    // 付言-139
                    settInfo.setRemark(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_SETTLEMENT_BANK_NAME:
                    // 交易对应货币开户行名称-140
                    contraSettInfo.setBankName(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_NAME:
                    // 交易对应货币开户行SWIFTCODE/开户行行号-144
                    contraSettInfo.setBankOpenNo(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_CORRESPONDENT_BANK_ACCOUNT:
                    // 交易对应货币开户行在中间行的资金账号-208
                    contraSettInfo.setBankAcctNo(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_ENGLISH_NAME_OF_CASH_ACCOUNT:
                    // 交易对应货币账户名称-142
                    contraSettInfo.setAcctName(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_SWIFT_CODE:
                    // 交易对应货币资金账户行SWIFT CODE/资金账户行-143
                    contraSettInfo.setAcctOpenNo(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_CASH_ACCOUNT_NUMBER:
                    // 交易对应货币资金账户-173
                    contraSettInfo.setAcctNo(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_INTERMEDIARY_BANK:
                    // 中间行名称-210
                    contraSettInfo.setIntermediaryBankName(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_INTERMEDIARY_BANK_BIC_CODE:
                    // 中间行SWIFT CODE/中间行行号-212
                    contraSettInfo.setIntermediaryBankBicCode(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_INTERMEDIARY_BANK_ACCOUNT:
                    // 中间行在开户行的资金账号-214
                    contraSettInfo.setIntermediaryBankAcctNo(partySubIdValue);
                    break;
                case PartySubIDType.CONTRA_CCY_REMARK:
                    // 付言-145
                    contraSettInfo.setRemark(partySubIdValue);
                    break;
                default:
                    logger.warn("清算明细项未进行处理! partySubIdType:" + partySubIdType + ",partySubIdValue:" + partySubIdValue);
                    break;
            }// end switch
        } // end for
        // 根据方向设置清算货币
        Side side = null;
        if (message.isSetNoFxOptions()) {//期权交易
            noFxOption = new imix.imix10.ExecutionReport.NoFxOptions();
            message.getGroup(1, noFxOption);
            side = noFxOption.isSetSide() ? noFxOption.getSide() : null;

        } else if (message.isSetNoOptionExercise()) { //期权到期行权或放弃行权
            imix.imix10.ExecutionReport.NoOptionExercise exercise = new imix.imix10.ExecutionReport.NoOptionExercise();
            message.getGroup(1, exercise);
            side = exercise.isSetSide() ? exercise.getSide() : null;

        } else {
            side = message.isSetSide() ? message.getSide() : null;
        }
        //市场标识
        String marketIndicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
        // 将清算信息设置到交易业务对象中x
        //TODO 20190321,xqq
        //买卖方向是针对基准货币【11064】的，而非成交货币【15】，所以对于带有货币对的交易需要再根据基准货币与交易货币比较，转换一次才可以。
        //此时，即使交易本方交易方向是买，我会转换成相反的，然后去获取对应的清算信息（注意：前台接收此类数据时也要判断，将对应的金额1余与金额2交换赋值，清算币种也要交换）
        //或者解析数据时交换
        //TODO 20190321,xqq
        PsEnum psEnum = convert(side, message.isSetNoLegs() ? nolegs.getLegSide() : null, settInfo.getMarkerTakerRole());
        if (MarketIndicator.FXSPT.equals(marketIndicator) || MarketIndicator.FXSWP.equals(marketIndicator)||MarketIndicator.FXFOW.equals(marketIndicator)) {
            String currency = message.isSetCurrency() ? message.getCurrency().getValue() : null;
            String currency1 = message.isSetCurrency1() ? message.getCurrency1().getValue() : null;
            Side side1 = message.isSetSide() ? message.getSide() : null;
            psEnum = convert(psEnum, currency, currency1, side1, settInfo.getMarkerTakerRole());

        } else if (MarketIndicator.FXCRS.equals(marketIndicator)) {
            //货币互换，判断买入币种是否是成交货币，如果是成交货币则取成交货币，如果是对应货币则取对应货币当做近端清算信息
            String currency = message.isSetCurrency() ? message.getCurrency().getValue() : null;
            String contraCurrency = message.isSetContraCurrency() ? message.getContraCurrency().getValue() : null;
            String legCcy = nolegs.isSetLegCurrency() ? nolegs.getLegCurrency().getValue() : null;
            psEnum = convert(psEnum, currency, legCcy, nolegs.getLegSide().getUnionTypeValue(), settInfo.getMarkerTakerRole());

            settInfo.setSettCcy(currency);
            contraSettInfo.setSettCcy(contraCurrency);

        } else if (MarketIndicator.FXOPT.equals(marketIndicator)) {
            if (message.isSetNoFxOptions()) {
                noFxOption = new imix.imix10.ExecutionReport.NoFxOptions();
                message.getGroup(1, noFxOption);
                String ccyPair = noFxOption.isSetSymbol() ? noFxOption.getSymbol().getValue() : "";
                if (ccyPair.trim().length() >= 7) {
                    settInfo.setSettCcy(ccyPair.substring(0, ccyPair.indexOf(".")));
                    contraSettInfo.setSettCcy(ccyPair.substring(ccyPair.indexOf(".") + 1, ccyPair.length()));
                }

            } else if (message.isSetNoOptionExercise()) {
                imix.imix10.ExecutionReport.NoOptionExercise exercise = new imix.imix10.ExecutionReport.NoOptionExercise();
                message.getGroup(1, exercise);
                String ccyPair = exercise.isSetSymbol() ? exercise.getSymbol().getValue() : "";
                if (ccyPair.trim().length() >= 7) {
                    settInfo.setSettCcy(ccyPair.substring(0, ccyPair.indexOf(".")));
                    contraSettInfo.setSettCcy(ccyPair.substring(ccyPair.indexOf(".") + 1, ccyPair.length()));
                }
            }

        } else if (MarketIndicator.INTER_BANK_OFFERING.equals(marketIndicator)) {
            settInfo.setSettCcy(message.isSetCurrency() ? message.getCurrency().getValue() : null);

        }
        convert(marketIndicator, psEnum, settInfo, contraSettInfo, settBeans);
    }


    public void convert(String marketIndicator, PsEnum psEnum, SettBaseBean settInfo, SettBaseBean contraSettInfo, SettBaseBean... settBeans) {
        if (null == settBeans) {
            return;
        }
        if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {
            if (null == settInfo) {// 处理外币拆借特殊
                BeanUtils.copyProperties(contraSettInfo, settInfo);
            } else {
                BeanUtils.copyProperties(settInfo, contraSettInfo);
            }

        } else if (marketIndicator.equals(MarketIndicator.FXIRS)) {//外币利率互换只有一个币种
            BeanUtils.copyProperties(settInfo, contraSettInfo);
        }

        if (settBeans.length >= 1) {
            BeanUtils.copyProperties(PsEnum.P.equals(psEnum) ? settInfo : contraSettInfo, settBeans[0]);
        }
        if (settBeans.length >= 2) {
            BeanUtils.copyProperties(PsEnum.P.equals(psEnum) ? contraSettInfo : settInfo, settBeans[1]);
        }
    }

}
