package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.fx.ImixFxMessageHandler;
import com.singlee.financial.pojo.FxOptionBean;
import com.singlee.financial.pojo.component.ExecStatus;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.trade.FxOptionCurrencyAmtBean;
import com.singlee.financial.pojo.trade.FxOptionPremiumBean;
import com.singlee.financial.pojo.trade.FxOptionSettlAmtBean;
import com.singlee.financial.pojo.trade.FxOptionsType;
import imix.Group;
import imix.field.CurrencyAmtType;
import imix.field.NoCurrencyAmt;
import imix.imix10.ExecutionReport;
import imix.imix10.component.SpreadOrBenchmarkCurveData;

import java.math.BigDecimal;

/**
 * 外汇期权[普通欧式]
 */
public class FxOptionEntry extends FxSettleEntry implements ImixFxMessageHandler<FxOptionBean> {

    /**
     * 方法已重载.转换外汇期权
     *
     * @param message      CFETS报文对象
     * @param fxOptionBean 期权交易业务对象
     */
    public void convert(ExecutionReport message, FxOptionBean fxOptionBean, String bankId) throws Exception {
        // 设置本方机构及清算信息
        convert(message, bankId, SettPartyEnum.TheParty, fxOptionBean.getInstitutionInfo(),fxOptionBean.getSettlement(), fxOptionBean.getFarSettlement());
        // 设置对手方机构及清算信息  外币债券特殊处理  没有对应货币,故上送本方清算路径即可
        convert(message, bankId, SettPartyEnum.CounterParty, fxOptionBean.getContraPatryInstitutionInfo(),fxOptionBean.getCounterPartySettlement(), fxOptionBean.getFarCounterPartySettlement());

        // 基本信息
        fxOptionBean.setOptSettlAmtBean(new FxOptionSettlAmtBean());
        //期权交易编号17
        fxOptionBean.setExecId(message.isSetExecID() ? (message.getExecID() == null ? "" : message.getExecID().getValue()) : "");
        //组合期权编号
        fxOptionBean.setCombinationId(message.isSetCombinationID() ? message.getCombinationID().getValue() : "");
        //期权类型:1-Vanilla
        fxOptionBean.setOptPayoutType(message.isSetOptPayoutType() ? String.valueOf(message.getOptPayoutType().getValue()) : "");
        //交易确认标识
        fxOptionBean.setCfetsCnfmIndicator(message.isSetCFETSCnfmIndicator() ? (message.getCFETSCnfmIndicator() == null ? "" : String.valueOf(message.getCFETSCnfmIndicator().getValue())) : "");
        //市场标识
        fxOptionBean.setMarketIndicator(message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "");
        //买卖方向
        fxOptionBean.setSide(message.isSetSide() ? message.getSide().getUnionTypeValue() : null);
        //taker交易方向
        fxOptionBean.setPs(convert(message.isSetSide() ? message.getSide() : null, fxOptionBean.getSettlement().getMarkerTakerRole()));
        //交易日期75
        fxOptionBean.setTradeDate(message.isSetTradeDate() ? (message.getTradeDate() == null ? "" : message.getTradeDate().getValue()) : "");
        //交易时间10318
        fxOptionBean.setTradeTime(message.isSetTradeTime() ? (message.getTradeTime() == null ? "" : message.getTradeTime().getValue()) : "");
        //交易状态
        fxOptionBean.setExecType(message.isSetExecType() ? (message.getExecType() == null ? "" : message.getExecType().getValue()) : "");
        //执行方式：4-RFQ
        // 交易模式
        fxOptionBean.setDealMode(convert(message.isSetTradingMode() ? message.getTradingMode() : null));
        //交易方式
        fxOptionBean.setTradeMethod(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        //交易模型10315
        fxOptionBean.setTradeInstrument(convert(message.isSetTradeInstrument() ? message.getTradeInstrument() : null));
        //交易方式(现阶段CSTP没有)
        fxOptionBean.setStatus(ExecStatus.Finish);
        //风险货币量
        fxOptionBean.setRiskLastQty(message.isSetRiskLastQty() ? new BigDecimal(message.getRiskLastQty().getDoubleValue()) : null);

        //delta exchange交易编号
        fxOptionBean.setRelatedReference(message.isSetRelatedReference() ? (message.getRelatedReference() == null ? "" : message.getRelatedReference().getValue()) : "");
        //期权交易成交消息类型:1-期权交易数据
        fxOptionBean.setOptDataType(message.isSetOPTDataType() ? (message.getOPTDataType() == null ? "" : String.valueOf(message.getOPTDataType().getValue())) : "");
        //行权状态:1-valid,2-exercised,3-expired
        fxOptionBean.setDerivativeExerciseStatus(message.isSetDerivativeExerciseStatus() ? (message.getDerivativeExerciseStatus() == null ? "" : String.valueOf(message.getDerivativeExerciseStatus().getValue())) : "");
        //清算方式
        fxOptionBean.setNetGrossInd(String.valueOf(message.isSetNetGrossInd() ? message.getNetGrossInd().getValue() : ""));
        //参考价
        SpreadOrBenchmarkCurveData data = message.getSpreadOrBenchmarkCurveData();
        if (data != null) {
            fxOptionBean.setBenchmarkCurveName(data.isSetBenchmarkCurveName() ? data.getBenchmarkCurveName().getValue() : "");
        }

        //**********************************
        if ("1".equals(fxOptionBean.getOptDataType()) && "1".equals(fxOptionBean.getDerivativeExerciseStatus())) {
            // 期权成交时
            fxOptionBean.setFxOptionsType(FxOptionsType.DealType.Valid.getValue());
        } else if ("2".equals(fxOptionBean.getOptDataType()) && "2".equals(fxOptionBean.getDerivativeExerciseStatus())) {
            // 期权到期正常行权时
            fxOptionBean.setFxOptionsType(FxOptionsType.DealType.Exercised.getValue());
        } else if ("2".equals(fxOptionBean.getOptDataType()) && "3".equals(fxOptionBean.getDerivativeExerciseStatus())) {
            // 期权到期放弃行权时
            fxOptionBean.setFxOptionsType(FxOptionsType.DealType.Expired.getValue());
        } else {
            // 默认为期权成交时
            fxOptionBean.setFxOptionsType(FxOptionsType.DealType.Valid.getValue());
        }

        imix.imix10.ExecutionReport.NoFxOptions fxOptions = null;
        imix.imix10.ExecutionReport.NoOptionExercise optionExercise = null;
        Group group = null;
        //交割类型:1-净额交割
        fxOptionBean.setNetGrossInd(message.isSetNetGrossInd() ? (message.getNetGrossInd() == null ? "" : String.valueOf(message.getNetGrossInd().getValue())) : "");
        if (FxOptionsType.DealType.Valid.getValue() == fxOptionBean.getFxOptionsType()) {
            //期权成交时
            fxOptions = new imix.imix10.ExecutionReport.NoFxOptions();
            message.getGroup(1, fxOptions);
            setNoOptionsInfo(message, fxOptions, fxOptionBean);
            group = fxOptions;

            int num = message.isSetNoFxOptions() ? message.getNoFxOptions().getValue() : 0;
            if (num > 1) {//组合期权
                fxOptions = new imix.imix10.ExecutionReport.NoFxOptions();
                message.getGroup(2, fxOptions);//获取组合期权类型
                //期权类型:1-Vanilla
                fxOptionBean.setOptPayoutType(fxOptions.isSetOptPayoutType() ? fxOptions.getOptPayoutType().getUnionTypeValue() : "");
            }

        } else if (FxOptionsType.DealType.Exercised.getValue() == fxOptionBean.getFxOptionsType()) {
            //期权到期正常行权时
            optionExercise = new imix.imix10.ExecutionReport.NoOptionExercise();
            message.getGroup(1, optionExercise);
            setNoOptionExerciseInfo(message, optionExercise, fxOptionBean);
            group = optionExercise;

            int num = message.isSetNoOptionExercise() ? message.getNoOptionExercise().getValue() : 0;
            if (num > 1) {//组合期权
                optionExercise = new imix.imix10.ExecutionReport.NoOptionExercise();
                message.getGroup(2, optionExercise);//获取组合期权类型
                //期权类型:1-Vanilla
                fxOptionBean.setOptPayoutType(optionExercise.isSetOptPayoutType() ? optionExercise.getOptPayoutType().getUnionTypeValue() : "");
            }

        } else if (FxOptionsType.DealType.Expired.getValue() == fxOptionBean.getFxOptionsType()) {
            //期权到期放弃行权时
            optionExercise = new imix.imix10.ExecutionReport.NoOptionExercise();
            message.getGroup(1, optionExercise);
            setNoOptionExerciseInfo(message, optionExercise, fxOptionBean);
            group = optionExercise;

            int num = message.isSetNoOptionExercise() ? message.getNoOptionExercise().getValue() : 0;
            if (num > 1) {//组合期权
                optionExercise = new imix.imix10.ExecutionReport.NoOptionExercise();
                message.getGroup(2, optionExercise);//获取组合期权类型
                //期权类型:1-Vanilla
                fxOptionBean.setOptPayoutType(optionExercise.isSetOptPayoutType() ? optionExercise.getOptPayoutType().getUnionTypeValue() : "");
            }
        }
        //期权为 Call 则币种类型为call的为基准货币
        //币种1信息 基准货币
        fxOptionBean.setCcy1(getCurrencyInfo(group, FxOptionsType.CurrencyType.Ccy, fxOptionBean.getPutOrCall()));
        //币种1信息 非基准货币
        fxOptionBean.setCcy2(getCurrencyInfo(group, FxOptionsType.CurrencyType.Ctrccy, fxOptionBean.getPutOrCall()));
        //期权费
        fxOptionBean.setPremium(getPremiumInfo(group));
    }

    /**
     * 外汇期权交易成交信息解析
     *
     * @param message
     * @param fxOptions
     * @param fxOptionBean
     * @throws Exception
     */
    private void setNoOptionsInfo(ExecutionReport message, imix.imix10.ExecutionReport.NoFxOptions fxOptions, FxOptionBean fxOptionBean) throws Exception {
        //成交单状态0-录入 1-修改 2-撤销
        fxOptionBean.setDealTransType(message.isSetDealTransType() ? (message.getDealTransType() == null ? "" : String.valueOf(message.getDealTransType().getValue())) : "");
        //行权方式：0-欧式期权
        fxOptionBean.setDerivativeExerciseStyle(fxOptions.isSetDerivativeExerciseStyle() ? (fxOptions.getDerivativeExerciseStyle() == null ? "" : String.valueOf(fxOptions.getDerivativeExerciseStyle().getValue())) : "");

        //子期权或者普通期权的交易方向
        fxOptionBean.setPs(convert(fxOptions.isSetSide() ? fxOptions.getSide() : null, fxOptionBean.getSettlement().getMarkerTakerRole()));
        //看涨看跌标识：0-Put,1-Call,2-Convertible,3-Put and Call,8-Other,9-None
        fxOptionBean.setPutOrCall(fxOptions.isSetPutOrCall() ? (fxOptions.getPutOrCall() == null ? "" : (String.valueOf(fxOptions.getPutOrCall().getValue()))) : "");
        //交易报价类型：0-ATM;(ATM,PUT_10D,PUT_25D,CALL_25D)
        fxOptionBean.setDerivativeOptAttribute(fxOptions.isSetDerivativeOptAttribute() ? (fxOptions.getDerivativeOptAttribute() == null ? "" : String.valueOf(fxOptions.getDerivativeOptAttribute().getValue())) : "");
        //期权类型:1-Vanilla
        fxOptionBean.setOptPayoutType(fxOptions.isSetOptPayoutType() ? (fxOptions.getOptPayoutType() == null ? "" : String.valueOf(fxOptions.getOptPayoutType().getValue())) : "");
        //交易货币对
        fxOptionBean.setSymbol(fxOptions.isSetSymbol() ? (fxOptions.getSymbol() == null ? "" : fxOptions.getSymbol().getValue()) : "");
        //即期价格
        fxOptionBean.setLastSpotRate(fxOptions.isSetLastSpotRate() ? (fxOptions.getLastSpotRate() == null ? "" : fxOptions.getLastSpotRate().getValue()) : "");
        //行权价
        fxOptionBean.setStrikePrice(fxOptions.isSetStrikePrice() ? (fxOptions.getStrikePrice() == null ? "" : fxOptions.getStrikePrice().getValue()) : "");
        //波动率
        fxOptionBean.setVolatility(fxOptions.isSetVolatility() ? (fxOptions.getVolatility() == null ? "" : fxOptions.getVolatility().getValue()) : "");
        //风险货币量
        fxOptionBean.setRiskLastQty(new BigDecimal(fxOptions.isSetRiskLastQty() ? (fxOptions.getRiskLastQty() == null ? "0" : fxOptions.getRiskLastQty().getValue()) : "0"));
        //行权截止日
        fxOptionBean.setMaturityDate(fxOptions.isSetMaturityDate() ? (fxOptions.getMaturityDate() == null ? "" : fxOptions.getMaturityDate().getValue()) : "");
        //行权截止时间
        fxOptionBean.setExpireTime(fxOptions.isSetExpireTime() ? (fxOptions.getExpireTime() == null ? "" : fxOptions.getExpireTime().getValue()) : "");
        //行权截止时间时区
        fxOptionBean.setExpireTimeZone(fxOptions.isSetExpireTimeZone() ? (fxOptions.getExpireTimeZone() == null ? "" : fxOptions.getExpireTimeZone().getValue()) : "");
        //行权期限
        fxOptionBean.setSettlType(fxOptions.isSetSettlType() ? (fxOptions.getSettlType() == null ? "" : fxOptions.getSettlType().getValue()) : "");
        if (FxOptionsType.DealType.Valid.getValue() == fxOptionBean.getFxOptionsType()) {
            imix.imix10.ExecutionReport.NoOptionExercise noOptionExercise = new imix.imix10.ExecutionReport.NoOptionExercise();
            message.getGroup(1, noOptionExercise);
            imix.imix10.ExecutionReport.NoOptionExercise.NoOptSettlAmt optSettlAmt = new imix.imix10.ExecutionReport.NoOptionExercise.NoOptSettlAmt();
            noOptionExercise.getGroup(1, optSettlAmt);
            // 净额/全额
            fxOptionBean.getOptSettlAmtBean().setOptSettlAmtType(optSettlAmt.isSetOptSettlAmtType() ? (optSettlAmt.getOptSettlAmtType() == null ? "" : String.valueOf(optSettlAmt.getOptSettlAmtType().getValue())) : "");
            //结算货币
            fxOptionBean.getOptSettlAmtBean().setStrikeCurrency(optSettlAmt.isSetStrikeCurrency() ? (optSettlAmt.getStrikeCurrency() == null ? "" : optSettlAmt.getStrikeCurrency().getValue()) : "");
            //结算金额交割日
            fxOptionBean.getOptSettlAmtBean().setDeliveryDate(optSettlAmt.isSetDeliveryDate() ? (optSettlAmt.getDeliveryDate() == null ? "" : optSettlAmt.getDeliveryDate().getValue()) : "");
        }
    }

    /**
     * 外汇期权行权或弃权交易解析
     *
     * @param message
     * @param optionExercise
     * @param fxOptionBean
     * @throws Exception
     */
    void setNoOptionExerciseInfo(ExecutionReport message, imix.imix10.ExecutionReport.NoOptionExercise optionExercise, FxOptionBean fxOptionBean) throws Exception {
        //行权后对应的EXOP交易id
        fxOptionBean.setReference(message.isSetReference() ? (message.getReference() == null ? "" : message.getReference().getValue()) : "");
        //行权方式：0-欧式期权
        fxOptionBean.setDerivativeExerciseStyle(optionExercise.isSetDerivativeExerciseStyle() ? (optionExercise.getDerivativeExerciseStyle() == null ? "" : String.valueOf(optionExercise.getDerivativeExerciseStyle().getValue())) : "");
        //看涨看跌标识：0-Put,1-Call,2-Convertible,3-Put and Call,8-Other,9-None
        fxOptionBean.setPutOrCall(optionExercise.isSetPutOrCall() ? (optionExercise.getPutOrCall() == null ? "" : String.valueOf(optionExercise.getPutOrCall().getValue())) : "");
        //子期权或者普通期权的交易方向
        fxOptionBean.setPs(convert(optionExercise.isSetSide() ? optionExercise.getSide() : null, fxOptionBean.getSettlement().getMarkerTakerRole()));
        //交易报价类型：0-ATM;(ATM,PUT_10D,PUT_25D,CALL_25D)
        fxOptionBean.setDerivativeOptAttribute(optionExercise.isSetDerivativeOptAttribute() ? (optionExercise.getDerivativeOptAttribute() == null ? "" : String.valueOf(optionExercise.getDerivativeOptAttribute().getValue())) : "");
        //期权类型:1-Vanilla
        fxOptionBean.setOptPayoutType(optionExercise.isSetOptPayoutType() ? (optionExercise.getOptPayoutType() == null ? "" : String.valueOf(optionExercise.getOptPayoutType().getValue())) : "");
        //交易货币对
        fxOptionBean.setSymbol(optionExercise.isSetSymbol() ? (optionExercise.getSymbol() == null ? "" : optionExercise.getSymbol().getValue()) : "");
        //即期价格
        fxOptionBean.setLastSpotRate(optionExercise.isSetLastSpotRate() ? (optionExercise.getLastSpotRate() == null ? "" : optionExercise.getLastSpotRate().getValue()) : "");
        //行权价
        fxOptionBean.setStrikePrice(optionExercise.isSetStrikePrice() ? (optionExercise.getStrikePrice() == null ? "" : optionExercise.getStrikePrice().getValue()) : "");
        //波动率
        fxOptionBean.setVolatility(optionExercise.isSetVolatility() ? (optionExercise.getVolatility() == null ? "" : optionExercise.getVolatility().getValue()) : "");
        //风险货币量
        fxOptionBean.setRiskLastQty(new BigDecimal(optionExercise.isSetRiskLastQty() ? (optionExercise.getRiskLastQty() == null ? "0" : optionExercise.getRiskLastQty().getValue()) : "0"));
        //行权截止日
        fxOptionBean.setMaturityDate(optionExercise.isSetMaturityDate() ? (optionExercise.getMaturityDate() == null ? "" : optionExercise.getMaturityDate().getValue()) : "");
        //行权截止时间
        fxOptionBean.setExpireTime(optionExercise.isSetExpireTime() ? (optionExercise.getExpireTime() == null ? "" : optionExercise.getExpireTime().getValue()) : "");
        //行权截止时间时区
        fxOptionBean.setExpireTimeZone(optionExercise.isSetExpireTimeZone() ? (optionExercise.getExpireTimeZone() == null ? "" : optionExercise.getExpireTimeZone().getValue()) : "");
        //行权期限
        fxOptionBean.setSettlType(optionExercise.isSetSettlType() ? (optionExercise.getSettlType() == null ? "" : optionExercise.getSettlType().getValue()) : "");
        //行权时间
        fxOptionBean.setStrikeTime(optionExercise.isSetStrikeTime() ? (optionExercise.getStrikeTime() == null ? "" : optionExercise.getStrikeTime().getValue()) : "");
        //行权用户
        fxOptionBean.setStrikeUser(optionExercise.isSetStrikeUser() ? (optionExercise.getStrikeUser() == null ? "" : optionExercise.getStrikeUser().getValue()) : "");
        //行权市价
        fxOptionBean.setStrikeDayPrice(optionExercise.isSetStrikeDayPrice() ? (optionExercise.getStrikeDayPrice() == null ? "" : optionExercise.getStrikeDayPrice().getValue()) : "");
        if (FxOptionsType.DealType.Exercised.getValue() == fxOptionBean.getFxOptionsType()) {
            imix.imix10.ExecutionReport.NoOptionExercise.NoOptSettlAmt optSettlAmt = new imix.imix10.ExecutionReport.NoOptionExercise.NoOptSettlAmt();
            optionExercise.getGroup(1, optSettlAmt);
            fxOptionBean.getOptSettlAmtBean().setOptSettlAmtType(optSettlAmt.isSetOptSettlAmtType() ? (optSettlAmt.getOptSettlAmtType() == null ? "" : String.valueOf(optSettlAmt.getOptSettlAmtType().getValue())) : "");
            //轧差结算金额
            fxOptionBean.getOptSettlAmtBean().setSettlCurrAmt(optSettlAmt.isSetSettlCurrAmt() ? (optSettlAmt.getSettlCurrAmt() == null ? "" : optSettlAmt.getSettlCurrAmt().getValue()) : "");
            //结算货币
            fxOptionBean.getOptSettlAmtBean().setStrikeCurrency(optSettlAmt.isSetStrikeCurrency() ? (optSettlAmt.getStrikeCurrency() == null ? "" : optSettlAmt.getStrikeCurrency().getValue()) : "");
            //结算金额支付方
            fxOptionBean.getOptSettlAmtBean().setPayerPartyReference(optSettlAmt.isSetPayerPartyReference() ? (optSettlAmt.getPayerPartyReference() == null ? "" : optSettlAmt.getPayerPartyReference().getValue()) : "");
            //结算金额收取方
            fxOptionBean.getOptSettlAmtBean().setReceiverPartyReference(optSettlAmt.isSetReceiverPartyReference() ? (optSettlAmt.getReceiverPartyReference() == null ? "" : optSettlAmt.getReceiverPartyReference().getValue()) : "");
            //结算金额交割日
            fxOptionBean.getOptSettlAmtBean().setDeliveryDate(optSettlAmt.isSetDeliveryDate() ? (optSettlAmt.getDeliveryDate() == null ? "" : optSettlAmt.getDeliveryDate().getValue()) : "");
        }
    }

    /**
     * 外汇期权币种、金额信息解析
     *
     * @param group
     * @param currencyType
     * @return
     * @throws Exception
     */
    FxOptionCurrencyAmtBean getCurrencyInfo(Group group, FxOptionsType.CurrencyType currencyType, String putCall) throws Exception {
        FxOptionCurrencyAmtBean bean = null;
        int currencyAmtType;
        imix.imix10.ExecutionReport.NoFxOptions.NoCurrencyAmt currencyAmt = null;
        int num = group.isSetField(NoCurrencyAmt.FIELD) ? group.getInt(NoCurrencyAmt.FIELD) : 0;
        for (int i = 1; i <= num; i++) {
            currencyAmt = new imix.imix10.ExecutionReport.NoFxOptions.NoCurrencyAmt();
            group.getGroup(i, currencyAmt);
            currencyAmtType = currencyAmt.isSetCurrencyAmtType() ? currencyAmt.getCurrencyAmtType().getValue() : 0;
            if (FxOptionsType.CurrencyType.Ccy.equals(currencyType)) {//基准货币
                if (putCall.equals(convert(currencyAmtType))) {
                    bean = new FxOptionCurrencyAmtBean();
                    // 币种
                    bean.setCurrency(currencyAmt.isSetCurrency() ? (currencyAmt.getCurrency() == null ? "" : currencyAmt.getCurrency().getValue()) : "");
                    // 金额类型：1-call,2-put
                    bean.setCurrencyAmtType(CurrencyAmtType.CALL == currencyAmtType ? "CALL" : "PUT");
                    // 本金
                    bean.setPrincipal(currencyAmt.isSetPrincipal() ? (currencyAmt.getPrincipal() == null ? "" : currencyAmt.getPrincipal().getValue()) : "");

                    break;
                }

            } else {//非基准货币
                if (!putCall.equals(convert(currencyAmtType))) {
                    bean = new FxOptionCurrencyAmtBean();
                    // 币种
                    bean.setCurrency(currencyAmt.isSetCurrency() ? (currencyAmt.getCurrency() == null ? "" : currencyAmt.getCurrency().getValue()) : "");
                    // 金额类型：1-call,2-put
                    bean.setCurrencyAmtType(CurrencyAmtType.CALL == currencyAmtType ? "CALL" : "PUT");
                    // 本金
                    bean.setPrincipal(currencyAmt.isSetPrincipal() ? (currencyAmt.getPrincipal() == null ? "" : currencyAmt.getPrincipal().getValue()) : "");

                    break;
                }
            }
        }// end for
        return bean;
    }

    /**
     * 外汇期权期权费解析
     *
     * @param group
     * @return
     * @throws Exception
     */
    FxOptionPremiumBean getPremiumInfo(Group group) throws Exception {
        FxOptionPremiumBean bean = new FxOptionPremiumBean();
        imix.imix10.ExecutionReport.NoFxOptions.NoPremium premium = new imix.imix10.ExecutionReport.NoFxOptions.NoPremium();
        group.getGroup(1, premium);
        // 期权费类型:Pips/Term%
        bean.setOptPremiumBasis(premium.isSetOptPremiumBasis() ? (premium.getOptPremiumBasis() == null ? "" : (premium.getOptPremiumBasis().getValue() == 1 ? "Pips" : "Term")) : "");
        // 期权费率
        bean.setOptPremiumValue(premium.isSetOptPremiumValue() ? (premium.getOptPremiumValue() == null ? "" : premium.getOptPremiumValue().getValue()) : "");
        // 期权费币种
        bean.setOptPremiumCurrency(premium.isSetOptPremiumCurrency() ? (premium.getOptPremiumCurrency() == null ? "" : premium.getOptPremiumCurrency().getValue()) : "");
        // 期权费总额
        bean.setOptPremiumAmt(premium.isSetOptPremiumAmt() ? (premium.getOptPremiumAmt() == null ? "" : premium.getOptPremiumAmt().getValue()) : "");
        // 小数位数
        bean.setDecimalPlaces(premium.isSetDecimalPlaces() ? (premium.getDecimalPlaces() == null ? "" : String.valueOf(premium.getDecimalPlaces().getValue())) : "");
        // 期权费交割日
        bean.setDeliveryDate(premium.isSetDeliveryDate() ? (premium.getDeliveryDate() == null ? "" : premium.getDeliveryDate().getValue()) : "");
        // 期权费支付方
        bean.setPayerPartyReference(premium.isSetPayerPartyReference() ? (premium.getPayerPartyReference() == null ? "" : premium.getPayerPartyReference().getValue()) : "");
        // 期权费收取方
        bean.setReceiverPartyReference(premium.isSetReceiverPartyReference() ? (premium.getReceiverPartyReference() == null ? "" : premium.getReceiverPartyReference().getValue()) : "");
        return bean;
    }
}
