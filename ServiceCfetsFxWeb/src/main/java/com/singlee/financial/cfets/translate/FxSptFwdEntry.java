package com.singlee.financial.cfets.translate;

import com.singlee.financial.cfets.fx.ImixFxMessageHandler;
import com.singlee.financial.pojo.FxSptFwdBean;
import com.singlee.financial.pojo.component.SettPartyEnum;
import com.singlee.financial.pojo.component.SptFwdTypeEnum;
import com.singlee.financial.pojo.trade.FxCurrencyBean;
import imix.FieldConvertError;
import imix.FieldNotFound;
import imix.field.MarketIndicator;
import imix.imix10.ExecutionReport;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 主要用于处理外汇即、远期交易的转换
 *
 * @author shenzl
 * @date 2021年10月26日
 */
public class FxSptFwdEntry extends FxSettleEntry implements ImixFxMessageHandler<FxSptFwdBean> {

    /**
     * 方法已重载.转换外汇即远期交易
     *
     * @param message       CFETS报文对象
     * @param cfetsFxSptFwd 即远期交易业务对象
     * @throws FieldNotFound
     * @throws FieldConvertError
     */
    public void convert(ExecutionReport message, FxSptFwdBean cfetsFxSptFwd, String bankId) throws Exception {
        // 交易币种对应的金额及汇率信息
        FxCurrencyBean currencyInfo = null;
        // 实例化金额实体对象
        currencyInfo = new FxCurrencyBean();
        // 设置本方机构及清算信息[成交货币清算信息 / 对应货币清算信息]
        convert(message, bankId, SettPartyEnum.TheParty, cfetsFxSptFwd.getInstitutionInfo(),cfetsFxSptFwd.getSettlement(), cfetsFxSptFwd.getContraSettlement());
        // 设置对手方机构及清算信息[成交货币清算信息 / 对应货币清算信息]
        convert(message, bankId, SettPartyEnum.CounterParty, cfetsFxSptFwd.getContraPatryInstitutionInfo(),cfetsFxSptFwd.getCounterPartySettlement(), cfetsFxSptFwd.getCounterPartyContraSettlement());
        //交易状态150
        cfetsFxSptFwd.setStatus(convert(message.isSetExecType() ? message.getExecType() : null));
        /*
         * 即远期基础信息
         */
        // 是否为期权行权交易10776
        cfetsFxSptFwd.setIsexdeltaindic(message.isSetIsExDeltaIndic() ? message.getIsExDeltaIndic().getValue() : false);
        //是否为行权的EXOP交易10787
        cfetsFxSptFwd.setOpSpotIndic(message.isSetIsOpSpotIndic() ? message.getIsOpSpotIndic().getValue() : false);
        // CFETS成交编号17域
        cfetsFxSptFwd.setExecId(message.isSetExecID() ? message.getExecID().getValue() : null);
        // 成交单状态10105
        cfetsFxSptFwd.setDealStatus(convert(message.isSetDealTransType() ? message.getDealTransType() : null));
        // 交易成交时间10318有重复
        cfetsFxSptFwd.setDealDateTime(message.isSetTradeTime() ? message.getTradeTime().getDateValue() : null);
        // 交易时间t,10318成交时间 ,默认当前时间
        cfetsFxSptFwd.setTradeTime(message.isSetTradeTime() ? message.getTradeTime().getValue() : new SimpleDateFormat("HH:mm:ss").format(new Date()));
        // 交易日期,成交日期75,默认当前时间
        cfetsFxSptFwd.setDealDate(message.isSetTradeDate() ? message.getTradeDate().getDateValue() : new Date());
        // 起息日，当交易为现券买卖时为交割日期,	64默认当前日期
        cfetsFxSptFwd.setValueDate(message.isSetSettlDate() ? message.getSettlDate().getDateValue() : new Date());
        // 买卖方向：54
        cfetsFxSptFwd.setPs(convert(message.isSetSide() ? message.getSide() : null, cfetsFxSptFwd.getSettlement().getMarkerTakerRole()));
        // 交易模式11657
        cfetsFxSptFwd.setDealMode(convert(message.isSetTradingMode() ? message.getTradingMode() : null));
        //交易方式10317
        cfetsFxSptFwd.setTradeMethod(convert(message.isSetTradeMethod() ? message.getTradeMethod() : null));
        //交易模型10315
        cfetsFxSptFwd.setTradeInstrument(convert(message.isSetTradeInstrument() ? message.getTradeInstrument() : null));
        //结算方式919,目前只有12:PVP
        cfetsFxSptFwd.setDeliveryType(message.isSetDeliveryType() ? String.valueOf(message.getDeliveryType().getValue()) : null);
        //交易类型（市场标识）10176
        cfetsFxSptFwd.setMarketIndicator(message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : null);
        // 清算类型430:默认净额清算
        cfetsFxSptFwd.setSettMode(convert(message.isSetNetGrossInd() ? message.getNetGrossInd() : null));
        //设置即期/远期标志
        String marketIndicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
        cfetsFxSptFwd.setSptFwdType((marketIndicator.equals(MarketIndicator.FXSPT) || marketIndicator.equals(MarketIndicator.GOLDSPT)) ? SptFwdTypeEnum.SPOT : SptFwdTypeEnum.FORWARD);
        //外汇期限63
        String  settleType = message.isSetSettlType() ? message.getSettlType().getValue() : null;
        cfetsFxSptFwd.setSettlType(convert(settleType));
        cfetsFxSptFwd.setSettlTypeCode(settleType);
        // 交易币种15
        currencyInfo.setCcy(message.isSetCurrency() ? message.getCurrency().getValue() : null);
        // 交易币种金额32
        currencyInfo.setAmt(message.isSetLastQty() ? new BigDecimal(message.getLastQty().getValue()) : null);
        // 对应货币币种11514
        currencyInfo.setContraCcy(message.isSetContraCurrency() ? message.getContraCurrency().getValue() : null);
        // 对应币种金额1056
        currencyInfo.setContraAmt(message.isSetCalculatedCcyLastQty() ? new BigDecimal(message.getCalculatedCcyLastQty().getValue()) : null);
        // 交易即期参考汇率194：即期价格
        currencyInfo.setSpotRate(message.isSetLastSpotRate() ? new BigDecimal(message.getLastSpotRate().getValue()) : null);
        //点差；成交价格
        if (cfetsFxSptFwd.getSptFwdType().equals(SptFwdTypeEnum.FORWARD) || "TOM".equals(StringUtils.trimToEmpty(settleType)) || "TODAY".equals(StringUtils.trimToEmpty(settleType))) {
            // 汇率点差,195,默认0
            BigDecimal points = message.isSetLastForwardPoints() ? new BigDecimal(message.getLastForwardPoints().getValue()) : new BigDecimal(0);
            BigDecimal spread = points.divide(new BigDecimal(10000));
            currencyInfo.setPoints(spread);
        }
        if (cfetsFxSptFwd.getSptFwdType().equals(SptFwdTypeEnum.FORWARD)  || "TOM".equals(StringUtils.trimToEmpty(settleType)) || "TODAY".equals(StringUtils.trimToEmpty(settleType))) {
            // 利率,汇率,净价价格31：远端交易价格
            currencyInfo.setRate(message.isSetLastPx() ? new BigDecimal(message.getLastPx().getValue()) : null);
        } else {
            //即期成交价格，即期没有点差，所以填充的是即期汇率
            currencyInfo.setRate(message.isSetLastSpotRate() ? new BigDecimal(message.getLastSpotRate().getValue()) : null);
        }
        // 根据方向判断币种
        //xqq  清算信息币种均是根据此币种，原理：清算币种为买入的币种，买：即买入基准货币，清算币种为基准货币，卖：即卖出基准货币，清算币种为对应货币
        //TODO 注意交易币种金额与对应币种金额，这里不转换，前置平台接收数据时需要判断转换，保存到数据库，因为前置平台金额1不是交易币种金额，而是货币对的前面货币金额。
        String currency1 = message.isSetCurrency1() ? message.getCurrency1().getValue() : null;//基准货币10064
        String currency2 = message.isSetCurrency2() ? message.getCurrency2().getValue() : null;//对应货币货币10065
        //cfetsFxSptFwd.getSettlement().setSettCcy(PsEnum.P.equals(cfetsFxSptFwd.getPs()) ? currency1 : currency2);
        //cfetsFxSptFwd.getCounterPartySettlement().setSettCcy(PsEnum.P.equals(cfetsFxSptFwd.getPs()) ? currency2 : currency1);
        //cfetsFxSptFwd.getSettlement().setSettCcy(cfetsFxSptFwd.getPs().equals(PsEnum.P) ? currencyInfo.getCcy() : currencyInfo.getContraCcy());
 		//cfetsFxSptFwd.getCounterPartySettlement().setSettCcy(cfetsFxSptFwd.getPs().equals(PsEnum.P) ? currencyInfo.getContraCcy() : currencyInfo.getCcy());

        //基准货币10064
        currencyInfo.setCcy1(currency1);
        //非基准货币10065
        currencyInfo.setCcy2(currency2);

        if (cfetsFxSptFwd.getSptFwdType().equals(SptFwdTypeEnum.FORWARD) || "TOM".equals(StringUtils.trimToEmpty(settleType)) || "TODAY".equals(StringUtils.trimToEmpty(settleType))) {
            //基准货币金额11519
            currencyInfo.setCcy1Amt(message.isSetCurrency1Amt2() ? new BigDecimal(message.getCurrency1Amt2().getValue()) : null);
            //非基准货币金额11520
            currencyInfo.setCcy2Amt(message.isSetCurrency2Amt2() ? new BigDecimal(message.getCurrency2Amt2().getValue()) : null);
        } else {
            //基准货币金额11515
            currencyInfo.setCcy1Amt(message.isSetCurrency1Amt() ? new BigDecimal(message.getCurrency1Amt().getValue()) : null);
            //非基准货币金额11516
            currencyInfo.setCcy2Amt(message.isSetCurrency2Amt() ? new BigDecimal(message.getCurrency2Amt().getValue()) : null);
        }

        // 设置金额实体对像
        cfetsFxSptFwd.setCurrencyInfo(currencyInfo);
        // 风险资本-折合美元10284
        cfetsFxSptFwd.setRiskLastQty(message.isSetRiskLastQty() ? new BigDecimal(message.getRiskLastQty().getDoubleValue()) : null);
        //货币对55
        cfetsFxSptFwd.setCcypair(message.isSetSymbol() ? message.getSymbol().getValue() : null);
        //交易确认标识11049：是Y,即此交易需要交易后确认，为N 不需要;默认值为Y,防止需要确认的交易漏掉，不需要的发送确认也没关系的。
        cfetsFxSptFwd.setCfetsCnfmIndicator(message.isSetCFETSCnfmIndicator() ? String.valueOf(message.getCFETSCnfmIndicator().getValue()) : "");
    }
}
