package com.singlee.cfets;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import imix.*;
import imix.Message.Header;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FIX2Json {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private final Map<Integer, String> tagToNameMap;

    public FIX2Json() {
        tagToNameMap = new HashMap<>();
        tagToNameMap.putAll(getTagToNameMap("imix.field"));
    }

    private static Map<Integer, String> getTagToNameMap(String packageName) {
        Map<Integer, String> tagToNameMap = new HashMap<>();
        try {
            ClassPath classPath = ClassPath.from(ClassLoader.getSystemClassLoader());
            ImmutableSet<ClassPath.ClassInfo> classes = classPath.getTopLevelClasses(packageName);
            for (ClassPath.ClassInfo classInfo : classes) {
                Field field;
                try {
                    field = classInfo.load().getField("FIELD");
                } catch (NoSuchFieldException e) {
                    continue;
                }
                int fieldTag = field.getInt(null);
                String fieldName = classInfo.getSimpleName();
                tagToNameMap.put(fieldTag, fieldName);
            }
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return tagToNameMap;
    }

    public String fix2Json(Message message) throws FieldNotFound, ConfigError {
        JsonObject jsonObject = new JsonObject();
        Header header = message.getHeader();
        DataDictionary dd = new DataDictionary("IMIX10.xml");
        //解析报文头
        Iterator<imix.Field<?>> headerIterator = header.iterator();
        while (headerIterator.hasNext()) {
            try {
                imix.Field<?> field = headerIterator.next();
                String name = tagToNameMap.get(field.getTag());
                if (name == null) {
                    name = String.valueOf(field.getTag());
                }
                jsonObject.addProperty(name, header.getString(field.getTag()));
            } catch (FieldNotFound e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        //解析报文体
        Iterator<imix.Field<?>> bodyIterator = message.iterator();
        while (bodyIterator.hasNext()) {
            try {
                imix.Field<?> field = bodyIterator.next();
                String name = tagToNameMap.get(field.getTag());
                printFieldMap("SL===>", dd, "8", message);
                //判断一级数组
//                if (isGroupCountField(dd, field)) {
//                    JsonObject object = this.fieldMapParse(field, message, dd);
//                    System.out.println(object);
//                }
//                //判断是否是数组
//                jsonObject.addProperty(name, message.getString(field.getTag()));
            } catch (FieldNotFound e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return gson.toJson(jsonObject);
    }

    private JsonObject jsonObject = new JsonObject();
    private JsonObject grpObj = new JsonObject();
    private JsonArray jsonArray = new JsonArray();

    private JsonObject fieldMapParse(imix.Field<?> field, FieldMap message, DataDictionary dd) throws FieldNotFound {
        String tagName = tagToNameMap.get(field.getTag());//从map取出字典名称
        String tagValue = message.getString(field.getTag());//从报文中取值
        //放入集合开头
        if (isGroupCountField(dd, field)) {
            jsonObject.add(tagName, jsonArray);

            for (int i = 1; i <= message.getInt(field.getTag()); i++) {
                Group group = message.getGroup(i, field.getField());
                Iterator<imix.Field<?>> g1fieldIterator = group.iterator();
                while (g1fieldIterator.hasNext()) {
                    imix.Field<?> g1field = g1fieldIterator.next();
                    if (isGroupCountField(dd, g1field)) {
                        // jsonObject.add(tagToNameMap.get(field.getTag()), jsonArray);
                        //fieldMapParse(g1field, group, dd);
                    } else {
                        grpObj.addProperty(tagToNameMap.get(g1field.getTag()), group.getString(g1field.getTag()));
                        jsonArray.add(grpObj);//添加数组1信息
                        System.out.println(tagToNameMap.get(g1field.getTag()) + "===>" + group.getString(g1field.getTag()));
                    }
                }
            }
        }
        return jsonObject;
    }


    private void printFieldMap(String prefix, DataDictionary dd, String msgType, FieldMap fieldMap) throws FieldNotFound {
        Iterator fieldIterator = fieldMap.iterator();
        while (fieldIterator.hasNext()) {
            imix.Field<?> field = (imix.Field<?>) fieldIterator.next();
            if (!isGroupCountField(dd, field)) {
                String value = fieldMap.getString(field.getTag());
                if (dd.hasFieldValue(field.getTag())) {
                    value = dd.getValueName(field.getTag(), fieldMap.getString(field.getTag())) + "_" + value;
                }
                System.out.println(prefix + dd.getFieldName(field.getTag()) + ": " + value);

            }
        }
        Iterator groupsKeys = fieldMap.groupKeyIterator();
        while (groupsKeys.hasNext()) {
            int groupCountTag = ((Integer) groupsKeys.next()).intValue();
            System.out.println(prefix + dd.getFieldName(groupCountTag) + ": count = " + fieldMap.getInt(groupCountTag));
            Group g = new Group(groupCountTag, 0);
            //Group g = Group.createGroup("iMIX.1.0","s",groupCountTag);
            int i = 1;
            while (fieldMap.hasGroup(i, groupCountTag)) {
                if (i > 1) {
                    System.out.println(prefix + "  ----");
                }
                fieldMap.getGroup(i, g);
                printFieldMap(prefix + "  ", dd, msgType, g);
                i++;
            }
        }
    }

    private boolean isGroupCountField(DataDictionary dd, imix.Field<?> field) {
        return dd.getFieldTypeEnum(field.getTag()) == FieldType.NumInGroup;
    }
}
