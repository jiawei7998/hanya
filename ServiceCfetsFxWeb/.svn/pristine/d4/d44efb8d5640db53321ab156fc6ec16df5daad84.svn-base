package com.singlee.financial.cfets.Util;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.fx.*;
import com.singlee.financial.pojo.FxSptFwdBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;
import imix.DataDictionary;
import imix.field.MarketIndicator;
import imix.field.MsgType;
import imix.imix10.ExecutionReport;
import org.apache.log4j.Logger;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContextEvent;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

/***
 *
 * 1、交易自动验收生成数据服务,验收配置为Y后 启动项目,该服务会启动
 * 2、client.cfg文件的以下两个配置为日志读取的路径和服务端口
 *  LogPath=D:/cfets/
 Port=8009
 * 3、将交易.log的日志文件放到以上配置的目录，文件要包含此次验收所有的交易报文，必须以.log结尾，可以是多个文件
 * 4、运行项目根目录下的 ParseTrades.cmd文件，服务会自动生成验收需要的数据到固定的目录下，生成的文件中没有多余的列，不需要手动在界面勾选
 * 5、直接上传文件进行匹配
 * @author lijie
 *
 */
public class TrdSocketListener extends ContextLoaderListener {

    private ConfigBean configBean;

    private ImixFxCfetsApiService imixFxCfetsApiService;

    private ServerSocket server = null;

    private String webRootPath = null;

    private static Logger LogManager = Logger.getLogger(TrdSocketListener.class);

    public TrdSocketListener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        webRootPath = arg0.getServletContext().getRealPath("");
        configBean = SpringGetBeanByClass.getBean(ConfigBean.class);
        imixFxCfetsApiService = SpringGetBeanByClass.getBean(ImixFxCfetsApiService.class);
        if (!"Y".equals(configBean.getCstpFxFlag())) {
            return;
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Properties p = new Properties();
                    p.load(TrdSocketClient.class.getClassLoader().getResourceAsStream("cfg//client.cfg"));
                    int port = Integer.parseInt(p.getProperty("CheckAndAcceptPort"));
                    server = new ServerSocket(port);
                    //b)指定绑定的端口，并监听此端口。
                    System.out.println("服务器启动成功");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Socket socket = null;
                while (true) {
                    try {
                        socket = server.accept();
                        doProcess(socket);
                    } catch (Exception e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }).start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if (!"Y".equals(configBean.getCstpFxFlag())) {
            return;
        }
        try {
            if (server != null && !server.isClosed()) {
                server.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /***
     *
     * 解析日志中的报文
     * 解析报文到Map中
     * 将交易写入文件中
     * @param socket
     * @throws Exception
     */
    public void doProcess(final Socket socket) throws Exception {
        new Thread(new Runnable() {

            @Override
            public void run() {
                BufferedReader in = null;
                PrintWriter w = null;
                try {
                    in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    w = new PrintWriter(socket.getOutputStream());
                    String line = in.readLine();
                    System.out.println(line);
                    if ("TRADES".equals(line)) {
                        try {
                            readerMsgs();

                            w.println("执行成功,请到目录:[" + webRootPath + "/result/]下查看结果");
                            w.flush();
                        } catch (Exception e) {
                            e.printStackTrace();

                            w.println("执行失败:" + e.getMessage());
                            w.flush();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (in != null) {
                            in.close();
                        }

                        if (w != null) {
                            w.close();
                        }

                        if (socket != null) {
                            socket.close();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void readerMsgs() throws Exception {
        String path = configBean.getLogPath();
        File[] fileList = null;
        File file = new File(path);
        if (!file.exists()) {
            throw new RuntimeException("文件不存在");
        }
        fileList = file.listFiles(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".log");
            }
        });
        ArrayList<String> arr = new ArrayList<String>();
        InputStream instream = null;
        for (File f : fileList) {
            try {
                instream = new FileInputStream(f);
                if (instream != null) {
                    InputStreamReader inputreader = new InputStreamReader(instream, configBean.getCharset());
                    BufferedReader buffreader = new BufferedReader(inputreader);
                    String line;
                    while ((line = buffreader.readLine()) != null) {
                        if (line.contains("8=IMIX.1.0") && (line.contains("35=8") || line.contains("35=d"))) {
                            arr.add(line.substring(line.indexOf("8=IMIX.1.0")));
                        }
                    }
                    buffreader.close();
                }
            } catch (Exception e) {
                throw e;
            } finally {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        TradeBaseBean tradeBean = null;
        String marketIndicator = null;
        List<TradeBaseBean> beans = null;
        String tmp = null;
        Map<String, String> map_1 = new HashMap<String, String>();
        Map<String, List<TradeBaseBean>> map = new HashMap<String, List<TradeBaseBean>>();
        for (String s : arr) {
            try {
                tmp = s.substring(s.indexOf(MarketIndicator.FIELD + "="));
                marketIndicator = tmp.substring(0, tmp.indexOf(ImixFxManageServer.SPLIT_CHAR)).split("=")[1];
                tradeBean = parseTrade(s);

                LogManager.info("解析完成：marketIndicator ===>" + marketIndicator + ", execId===>" + tradeBean.getExecId());

                if (map_1.get(tradeBean.getExecId()) != null) {
                    //重复交易
                    marketIndicator = "RESEND_" + marketIndicator;
                } else {
                    map_1.put(tradeBean.getExecId(), tradeBean.getExecId());
                }

                if (marketIndicator.equals(MarketIndicator.FXSPT) || marketIndicator.equals(MarketIndicator.GOLDSPT)) {
                    FxSptFwdBean tt = (FxSptFwdBean) tradeBean;
                    if (tt.isOpSpotIndic()) {
                        marketIndicator = "FXOPT_" + marketIndicator;
                    }
                }

                beans = map.get(marketIndicator);
                if (beans == null) {
                    beans = new ArrayList<TradeBaseBean>();
                    map.put(marketIndicator, beans);
                }
                beans.add(tradeBean);
            } catch (Exception e) {
                e.printStackTrace();
                LogManager.error(s);
                LogManager.error("解析报错：", e);
            }
        }

        URL weburl = ImixFxMessageProcess.class.getClassLoader().getResource("WEB-INF/classes");
        URL url = ImixFxMessageProcess.class.getClassLoader().getResource("");
        String path1 = (null == weburl ? url.getPath() : weburl.getPath()) + configBean.getCstpFxUrl();

        StringBuffer sdf = new StringBuffer();

        ExcelUtil eUtil = new ExcelUtil(path1 + "FXCSTP_1.xlsx");
        for (Entry<String, List<TradeBaseBean>> e : map.entrySet()) {
            marketIndicator = e.getKey();
            LogManager.info("==============>marketIndicator: " + marketIndicator + "," + e.getValue().size());
            sdf = new StringBuffer();
            for (TradeBaseBean tradeBaseBean : e.getValue()) {
                sdf.append(tradeBaseBean.getExecId()).append(" ");
            }

            LogManager.info("==============>" + sdf.toString());

            if (marketIndicator.equals(MarketIndicator.FXSWP) || marketIndicator.equals(MarketIndicator.GOLDSWP)) {
                ImixFxFeedBack.writeFxSwpTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.equals(MarketIndicator.FXSPT) || marketIndicator.equals(MarketIndicator.GOLDSPT)) {
                ImixFxFeedBack.writeFxSpotTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.equals(MarketIndicator.FXFOW) || marketIndicator.equals(MarketIndicator.FXNDF)
                    || marketIndicator.equals(MarketIndicator.GOLDFWD)) {
                ImixFxFeedBack.writeFxFwdTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.equals(MarketIndicator.FXOPT)) {
                ImixFxFeedBack.writeFxOptTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.equals(MarketIndicator.FXCRS)) {
                ImixFxFeedBack.writeFxCcsTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.equals(MarketIndicator.INTER_BANK_OFFERING)) {
                ImixFxFeedBack.writeFxMMTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.equals(MarketIndicator.FXIRS)) {
                ImixFxFeedBack.writeFxIrsTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.startsWith("FXOPT_")) {//期权相关交易
                ImixFxFeedBack.writeFxOptSpotTrader(eUtil, e.getValue(), marketIndicator);

            } else if (marketIndicator.startsWith("RESEND_")) {//重复交易
                writeReSendTrades(e.getValue(), marketIndicator);

            }
        }
        String res = webRootPath + "/result/";
        File dir = new File(res);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        System.out.println(res);
        eUtil.writeExcel(res + "FXCSTP_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".xlsx");
    }

    private void writeReSendTrades(List<TradeBaseBean> value, String marketIndicator) {
        PrintWriter p = null;
        try {
            p = new PrintWriter(new File(webRootPath + "/result/RESEND_" + marketIndicator + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".txt"));
            Map<String, String> map_1 = new HashMap<String, String>();
            for (TradeBaseBean tradeBaseBean : value) {
                if (map_1.get(tradeBaseBean.getExecId()) != null) {
                    continue;
                } else {
                    p.println(tradeBaseBean.getExecId());

                    map_1.put(tradeBaseBean.getExecId(), tradeBaseBean.getExecId());
                }
            }
            p.flush();
        } catch (Exception e) {
            LogManager.error("写入重复交易出错:", e);
        } finally {
            if (p != null) {
                p.close();
            }
        }
    }

    public TradeBaseBean parseTrade(String msg) throws Exception {
        ExecutionReport message = new ExecutionReport();
        message.fromString(ImixFxManageServer.reBuilderMsg(msg), new DataDictionary("IMIX10.xml"), false);
        // 市场标识
        String marketIndicator = message.isSetMarketIndicator() ? message.getMarketIndicator().getValue() : "";
        // 消息类型
        String msgType = message.getHeader().getString(MsgType.FIELD);

        return new ImixFxMessageProcessImpl(configBean).parseMsg(message, marketIndicator);
    }
}
