#!/bin/sh
######################################################
# Author -- John Liu.
# Copyright 2002,2003 Reuters SH.

AWKCMD="/bin/awk"
CURRENT_DIR=`pwd`
SERVICE_FILE="services"

if [ ! -f $SERVICE_FILE ]; then
	echo "Error: File '$SERVICE_FILE' not exits!"
	exit 1
fi

SERVICE_LIST=`cat $SERVICE_FILE | grep -v \!`
for ITEM in $SERVICE_LIST
do
	DISPLAY_NAME=`echo $ITEM | awk 'BEGIN{FS="|"}{print $1}'`
	PROCESS_NAME=`echo $ITEM | awk 'BEGIN{FS="|"}{print $2}'`
	SCRIPT_NAME=`echo $ITEM | awk 'BEGIN{FS="|"}{print $3}'`
	OWNER_USER=`echo $ITEM | awk 'BEGIN{FS="|"}{print $4}'`

	PID_FILE=$DISPLAY_NAME.pid
	
	tem=`ps -ef |grep $PROCESS_NAME | grep -v grep | grep $OWNER_USER | $AWKCMD '{ print $2 } ' | tr -d '\012'`
	tem=${tem:-"y"}
	if [ ! $tem = y ] ; then
		ps -ef |grep $PROCESS_NAME | grep -v grep | grep $OWNER_USER | $AWKCMD '{ print $2 }' | xargs pwdx | grep $CURRENT_DIR | $AWKCMD '{ print substr($1,1,length($1)-1)}' >$PID_FILE
	fi
	if [ ! -s $PID_FILE ] ; then
		echo "$DISPLAY_NAME  WARNING: $PROCESS_NAME not running ... Nothing done "
	else
		PID=`cat $PID_FILE`
		NOW_DIR=`pwdx $PID | $AWKCMD '{ print $2 }'`
		if [ "$CURRENT_DIR" = "$NOW_DIR" ] ; then
			rm $PID_FILE
			kill $PID
			echo "$DISPLAY_NAME  $PROCESS_NAME has been stoped ..."
		else
			rm $PID_FILE
			echo "$DISPLAY_NAME  WARNING: $PROCESS_NAME not running ... Nothing done "
		fi
	fi

done


