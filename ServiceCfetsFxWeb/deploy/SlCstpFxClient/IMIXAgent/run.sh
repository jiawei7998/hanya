#!/bin/bash
export agentcp=`find lib -name "*.jar" |xargs|sed "s/ /:/g"`
java -Xms256m -Xmx1024m -Djava.library.path=/usr/lib -classpath ${agentcp} agent.Agent ./cfg/agent.cfg
