package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 债券础基信息实体对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SecurityInfoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4369821601542263122L;

	//债券ISNB代码
	private String securityId;
	
	//债券简称
	private String securityName;
	
	//折算比例(%)
	private BigDecimal haircut;
	
	//面额
	private BigDecimal faceAmt;
	
	//净价
	private BigDecimal cPrice;
	
	//全价
	private BigDecimal dPrice;
	
	//应计利息
	private BigDecimal accruedInt;
	
	//远期行权收益率
	private BigDecimal strikeYield;
	
	//远期收益率
	private BigDecimal yield2;
	
	private BigDecimal rate;
	
	private String intPayDate;

	//债券托管机构名称
	private String securityAccName;

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getIntPayDate() {
		return intPayDate;
	}

	public void setIntPayDate(String intPayDate) {
		this.intPayDate = intPayDate;
	}

	public BigDecimal getStrikeYield() {
		return strikeYield;
	}

	public void setStrikeYield(BigDecimal strikeYield) {
		this.strikeYield = strikeYield;
	}

	public BigDecimal getYield2() {
		return yield2;
	}

	public void setYield2(BigDecimal yield2) {
		this.yield2 = yield2;
	}

	/**
	 * 获取债券代码(ISBN)
	 * @return
	 */
	public String getSecurityId() {
		return securityId;
	}

	/**
	 * 设置债券代码(ISBN)
	 * @param securityId
	 */
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}

	/**
	 * 获取债券名称
	 * @return
	 */
	public String getSecurityName() {
		return securityName;
	}

	/**
	 * 设置债券名称
	 * @param securityName
	 */
	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	/**
	 * 获取拆算比例
	 * @return
	 */
	public BigDecimal getHaircut() {
		return haircut;
	}

	/**
	 * 设置拆算比例
	 * @param haircut
	 */
	public void setHaircut(BigDecimal haircut) {
		this.haircut = haircut;
	}

	/**
	 * 获取券面总额
	 * @return
	 */
	public BigDecimal getFaceAmt() {
		return faceAmt;
	}

	/**
	 * 设置券面总额
	 * @param faceAmt
	 */
	public void setFaceAmt(BigDecimal faceAmt) {
		this.faceAmt = faceAmt;
	}


	public BigDecimal getcPrice() {
		return cPrice;
	}

	public void setcPrice(BigDecimal cPrice) {
		this.cPrice = cPrice;
	}

	public BigDecimal getdPrice() {
		return dPrice;
	}

	public void setdPrice(BigDecimal dPrice) {
		this.dPrice = dPrice;
	}

	public BigDecimal getAccruedInt() {
		return accruedInt;
	}

	public void setAccruedInt(BigDecimal accruedInt) {
		this.accruedInt = accruedInt;
	}

	public String getSecurityAccName() {
		return securityAccName;
	}

	public void setSecurityAccName(String securityAccName) {
		this.securityAccName = securityAccName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SecurityInfoBean [faceAmt=");
		builder.append(faceAmt);
		builder.append(", haircut=");
		builder.append(haircut);
		builder.append(", securityId=");
		builder.append(securityId);
		builder.append(", securityName=");
		builder.append(securityName);
		builder.append("]");
		return builder.toString();
	}
	
	

}
