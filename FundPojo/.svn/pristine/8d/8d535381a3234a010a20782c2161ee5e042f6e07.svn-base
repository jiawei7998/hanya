package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 债券结算金额组件,包含清算方式,结算金额,应计利息,结算日期
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SecuritySettAmtBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2122722633568070359L;

	//清算方式
	private String settType;
	
	//结算金额
	private BigDecimal settAmt;
	
	//应计利息
	private BigDecimal interestAmt;
	
	//结算日期
	private Date settDate;
	
	//结算币种
	private String settCCy;

	/**
	 * 获取清算方式
	 * @return
	 */
	public String getSettType() {
		return settType;
	}

	/**
	 * 设置清算方式
	 * @param settType
	 */
	public void setSettType(String settType) {
		this.settType = settType;
	}

	/**
	 * 获取结算金额
	 * @return
	 */
	public BigDecimal getSettAmt() {
		return settAmt;
	}

	/**
	 * 设置结算金额
	 * @param settAmt
	 */
	public void setSettAmt(BigDecimal settAmt) {
		this.settAmt = settAmt;
	}

	/**
	 * 获取应计利息金额
	 * @return
	 */
	public BigDecimal getInterestAmt() {
		return interestAmt;
	}

	/**
	 * 设置应计利息金额
	 * @param interestAmt
	 */
	public void setInterestAmt(BigDecimal interestAmt) {
		this.interestAmt = interestAmt;
	}

	/**
	 * 获取结算金额
	 * @return
	 */
	public Date getSettDate() {
		return settDate;
	}

	/**
	 * 设置结算金额
	 * @param settDate
	 */
	public void setSettDate(Date settDate) {
		this.settDate = settDate;
	}

	public String getSettCCy() {
		return settCCy;
	}

	public void setSettCCy(String settCCy) {
		this.settCCy = settCCy;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SecuritySettAmtBean [interestAmt=");
		builder.append(interestAmt);
		builder.append(", settAmt=");
		builder.append(settAmt);
		builder.append(", settDate=");
		builder.append(settDate);
		builder.append(", settType=");
		builder.append(settType);
		builder.append("]");
		return builder.toString();
	}

}
