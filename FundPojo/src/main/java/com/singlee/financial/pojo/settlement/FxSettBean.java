package com.singlee.financial.pojo.settlement;

/**
 * 外汇业务清算信息实体对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class FxSettBean extends SettBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3663926953051921315L;
	
	//清算货币
	private String settCcy;
	
	//开户行SWIFT CODE/开户行行号
	private String bankOpenNo;
	
	//账户行SWIFT CODE/支付系统行号
	private String acctOpenNo;

	//中间行名称
	private String intermediaryBankName;
	
	//中间行SWIFT CODE
	private String intermediaryBankBicCode;
	
	//中间行在开户行的资金账号
	private String intermediaryBankAcctNo;
	
	/**
	 * 获取开户行SWIFT CODE(外币)/开户行行号(人民币)
	 * @return
	 */
	public String getBankOpenNo() {
		return bankOpenNo;
	}

	/**
	 * 获取开户行SWIFT CODE(外币)/开户行行号(人民币)
	 * @param bankBicCode
	 */
	public void setBankOpenNo(String bankOpenNo) {
		this.bankOpenNo = bankOpenNo;
	}

	/**
	 * 获取账户行SWIFT CODE(外币)/账户行行号(人民币)
	 * @return
	 */
	public String getAcctOpenNo() {
		return acctOpenNo;
	}

	/**
	 * 设置账户行SWIFT CODE/账户行行号
	 * @param acctBicCode
	 */
	public void setAcctOpenNo(String acctOpenNo) {
		this.acctOpenNo = acctOpenNo;
	}

	/**
	 * 获取中间行名称
	 * @return
	 */
	public String getIntermediaryBankName() {
		return intermediaryBankName;
	}

	/**
	 * 设置中间行名称
	 * @param intermediaryBankName
	 */
	public void setIntermediaryBankName(String intermediaryBankName) {
		this.intermediaryBankName = intermediaryBankName;
	}

	/**
	 * 获取中间行SWIFT CODE/中间行行号
	 * @return
	 */
	public String getIntermediaryBankBicCode() {
		return intermediaryBankBicCode;
	}

	/**
	 * 设置中间行SWIFT CODE/中间行行号
	 * @param intermediaryBankBicCode
	 */
	public void setIntermediaryBankBicCode(String intermediaryBankBicCode) {
		this.intermediaryBankBicCode = intermediaryBankBicCode;
	}

	/**
	 * 获取中间行在开户行的资金账户
	 * @return
	 */
	public String getIntermediaryBankAcctNo() {
		return intermediaryBankAcctNo;
	}

	/**
	 * 设置中间行在开户行的资金账户
	 * @param intermediaryBankAcctNo
	 */
	public void setIntermediaryBankAcctNo(String intermediaryBankAcctNo) {
		this.intermediaryBankAcctNo = intermediaryBankAcctNo;
	}
	
	/**
	 * 获取清算币种
	 * @return
	 */
	public String getSettCcy() {
		return settCcy;
	}

	/**
	 * 设置清算币种
	 * @param settCcy
	 */
	public void setSettCcy(String settCcy) {
		this.settCcy = settCcy;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FxSettBean [acctOpenNo=");
		builder.append(acctOpenNo);
		builder.append(", bankOpenNo=");
		builder.append(bankOpenNo);
		builder.append(", intermediaryBankAcctNo=");
		builder.append(intermediaryBankAcctNo);
		builder.append(", intermediaryBankBicCode=");
		builder.append(intermediaryBankBicCode);
		builder.append(", intermediaryBankName=");
		builder.append(intermediaryBankName);
		builder.append(", settCcy=");
		builder.append(settCcy);
		builder.append(", getAcctName()=");
		builder.append(getAcctName());
		builder.append(", getAcctNo()=");
		builder.append(getAcctNo());
		builder.append(", getBankAcctNo()=");
		builder.append(getBankAcctNo());
		builder.append(", getBankName()=");
		builder.append(getBankName());
		builder.append(", getMarkerTakerRole()=");
		builder.append(getMarkerTakerRole());
		builder.append(", getRemark()=");
		builder.append(getRemark());
		builder.append("]");
		return builder.toString();
	}
}
