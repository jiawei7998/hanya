package com.singlee.financial.pojo.trade;

import com.singlee.financial.pojo.settlement.SecurSettBean;

import java.math.BigDecimal;

/**
 * 回购业务父对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class RepoBaseBean extends TradeBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7916424202215942785L;


	//回购利率
	private BigDecimal rate;
	
	//本方清算信息
	private SecurSettBean settInfo;
	
	//对手方清算信息
	private SecurSettBean contraPartySettInfo;
	
	private String term;//cstp下行期限接收
	
	private String occupancyDays;//cstp下行实际占款天数接收
	

	/**
	 * 获取回购利率
	 * @return
	 */
	public BigDecimal getRate() {
		return rate;
	}

	/**
	 * 设置回购利率
	 * @param rate
	 */
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	/**
	 * 获取本方清算信息
	 * @return
	 */
	public SecurSettBean getSettInfo() {
		if(null == settInfo){
			settInfo = new SecurSettBean();
		}
		return settInfo;
	}

	/**
	 * 设置本方清算信息
	 * @param settInfo
	 */
	public void setSettInfo(SecurSettBean settInfo) {
		this.settInfo = settInfo;
	}

	/**
	 * 获取对手方清算信息
	 * @return
	 */
	public SecurSettBean getContraPartySettInfo() {
		if(null == contraPartySettInfo){
			contraPartySettInfo = new SecurSettBean();
		}
		return contraPartySettInfo;
	}

	/**
	 * 设置对手方清算信息
	 * @param contraPartySettInfo
	 */
	public void setContraPartySettInfo(SecurSettBean contraPartySettInfo) {
		this.contraPartySettInfo = contraPartySettInfo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RepoBaseBean [contraPartySettInfo=");
		builder.append(contraPartySettInfo);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", settInfo=");
		builder.append(settInfo);
		builder.append("]");
		return builder.toString();
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public void setOccupancyDays(String occupancyDays) {
		this.occupancyDays = occupancyDays;
	}

	public String getOccupancyDays() {
		return occupancyDays;
	}
}
