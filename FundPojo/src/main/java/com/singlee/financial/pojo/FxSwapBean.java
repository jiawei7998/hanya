package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.financial.pojo.trade.FxBaseBean;
import com.singlee.financial.pojo.trade.FxCurrencyBean;

import java.util.Date;

/**
 * 外汇掉期业务实体对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class FxSwapBean extends FxBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1278092510598094980L;


	//远端交易货币金额及汇率信息
	private FxCurrencyBean farCurrencyInfo;

	//远端起息日
	private Date farValueDate;
	
	//远端本方清算信息
	private FxSettBean farSettlement;

	//远端对手方清算信息
	private FxSettBean farCounterPartySettlement;
	
	//交易类型（市场标识）
	private String marketIndicator;
	
	//货币对
	private String ccypair;
	
	//基准货币
	private String ccy1;
	//非基准货币
	private String ccy2;

	public String getCcy1() {
		return ccy1;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public String getCcy2() {
		return ccy2;
	}

	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	/**
	 * 获取远端交易货币金额及汇率信息
	 * @return
	 */
	public FxCurrencyBean getFarCurrencyInfo() {
		if(null == farCurrencyInfo){
			farCurrencyInfo = new FxCurrencyBean();
		}
		return farCurrencyInfo;
	}

	/**
	 * 设置远端交易货币金额及汇率信息
	 * @param farCurrencyInfo
	 */
	public void setFarCurrencyInfo(FxCurrencyBean farCurrencyInfo) {
		this.farCurrencyInfo = farCurrencyInfo;
	}

	/**
	 * 获取远端起息日
	 * @return
	 */
	public Date getFarValueDate() {
		return farValueDate;
	}

	/**
	 * 设置远端起息日
	 * @param farValueDate
	 */
	public void setFarValueDate(Date farValueDate) {
		this.farValueDate = farValueDate;
	}

	/**
	 * 获取远端本方清算信息
	 * @return
	 */
	public FxSettBean getFarSettlement() {
		if(null == farSettlement){
			farSettlement = new FxSettBean();
		}
		return farSettlement;
	}

	/**
	 * 设置远端本方清算信息
	 * @param farSettlement
	 */
	public void setFarSettlement(FxSettBean farSettlement) {
		this.farSettlement = farSettlement;
	}

	/**
	 * 获取远端对手方清算信息
	 * @return
	 */
	public FxSettBean getFarCounterPartySettlement() {
		if(null == farCounterPartySettlement){
			farCounterPartySettlement = new FxSettBean();
		}
		return farCounterPartySettlement;
	}

	/**
	 * 设置远端对手方清算信息
	 * @param farCounterPartySettlement
	 */
	public void setFarCounterPartySettlement(FxSettBean farCounterPartySettlement) {
		this.farCounterPartySettlement = farCounterPartySettlement;
	}

	public String getCcypair() {
		return ccypair;
	}

	public void setCcypair(String ccypair) {
		this.ccypair = ccypair;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FxSwapBean [farCounterPartySettlement=");
		builder.append(farCounterPartySettlement);
		builder.append(", farCurrencyInfo=");
		builder.append(farCurrencyInfo);
		builder.append(", farSettlement=");
		builder.append(farSettlement);
		builder.append(", farValueDate=");
		builder.append(farValueDate);
		builder.append(", getCounterPartySettlement()=");
		builder.append(getCounterPartySettlement());
		builder.append(", getCurrencyInfo()=");
		builder.append(getCurrencyInfo());
		builder.append(", getSettlement()=");
		builder.append(getSettlement());
		builder.append(", getContraPatryInstitutionInfo()=");
		builder.append(getContraPatryInstitutionInfo());
		builder.append(", getDealDate()=");
		builder.append(getDealDate());
		builder.append(", getDealDateTime()=");
		builder.append(getDealDateTime());
		builder.append(", getDealMode()=");
		builder.append(getDealMode());
		builder.append(", getDealStatus()=");
		builder.append(getDealStatus());
		builder.append(", getExecId()=");
		builder.append(getExecId());
		builder.append(", getExecIdToFedealno()=");
		builder.append(getExecIdToFedealno());
		builder.append(", getInstitutionInfo()=");
		builder.append(getInstitutionInfo());
		builder.append(", getPs()=");
		builder.append(getPs());
		builder.append(", getSettMode()=");
		builder.append(getSettMode());
		builder.append(", getTradeTime()=");
		builder.append(getTradeTime());
		builder.append(", getValueDate()=");
		builder.append(getValueDate());
		builder.append("]");
		return builder.toString();
	}

}
