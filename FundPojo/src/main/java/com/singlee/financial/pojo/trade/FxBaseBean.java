package com.singlee.financial.pojo.trade;

import com.singlee.financial.pojo.settlement.FxSettBean;

import java.math.BigDecimal;

/**
 * 外汇业务基础父对象
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class FxBaseBean extends TradeBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3543233526905592268L;

	// 交易币种金额及汇率信息
	private FxCurrencyBean currencyInfo;

	// 本方清算信息
	private FxSettBean settlement;

	// 对手方清算信息
	private FxSettBean counterPartySettlement;

	// 折合美元量(风险资本)
	private BigDecimal riskLastQty;

	/**
	 * 获取对手方清算信息
	 * 
	 * @return
	 */
	public FxSettBean getCounterPartySettlement() {
		if (null == this.counterPartySettlement) {
			this.counterPartySettlement = new FxSettBean();
		}
		return counterPartySettlement;
	}

	/**
	 * 设置对手方清算信息
	 * 
	 * @param counterPartySettlement
	 */
	public void setCounterPartySettlement(FxSettBean counterPartySettlement) {
		this.counterPartySettlement = counterPartySettlement;
	}

	/**
	 * 获取交易币种金额及汇率信息对象
	 * 
	 * @return
	 */
	public FxCurrencyBean getCurrencyInfo() {
		return currencyInfo;
	}

	/**
	 * 设置交易币种金额及汇率信息对象
	 * 
	 * @param currencyInfo
	 */
	public void setCurrencyInfo(FxCurrencyBean currencyInfo) {
		this.currencyInfo = currencyInfo;
	}

	/**
	 * 获取本方清算信息
	 * 
	 * @return
	 */
	public FxSettBean getSettlement() {
		if (null == this.settlement) {
			this.settlement = new FxSettBean();
		}
		return settlement;
	}

	/**
	 * 设置本方清算信息
	 * 
	 * @param settlement
	 */
	public void setSettlement(FxSettBean settlement) {
		this.settlement = settlement;
	}

	public BigDecimal getRiskLastQty() {
		return riskLastQty;
	}

	public void setRiskLastQty(BigDecimal riskLastQty) {
		this.riskLastQty = riskLastQty;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FxBaseBean [counterPartySettlement=");
		builder.append(counterPartySettlement);
		builder.append(", currencyInfo=");
		builder.append(currencyInfo);
		builder.append(", settlement=");
		builder.append(settlement);
		builder.append(", getContraPatryInstitutionInfo()=");
		builder.append(getContraPatryInstitutionInfo());
		builder.append(", getDealDate()=");
		builder.append(getDealDate());
		builder.append(", getDealDateTime()=");
		builder.append(getDealDateTime());
		builder.append(", getDealMode()=");
		builder.append(getDealMode());
		builder.append(", getDealStatus()=");
		builder.append(getDealStatus());
		builder.append(", getExecId()=");
		builder.append(getExecId());
		//builder.append(", getExecIdToFedealno()=");
		//builder.append(getExecIdToFedealno());
		builder.append(", getInstitutionInfo()=");
		builder.append(getInstitutionInfo());
		builder.append(", getPs()=");
		builder.append(getPs());
		builder.append(", getSettMode()=");
		builder.append(getSettMode());
		builder.append(", getTradeTime()=");
		builder.append(getTradeTime());
		builder.append(", getValueDate()=");
		builder.append(getValueDate());
		builder.append("]");
		return builder.toString();
	}

}
