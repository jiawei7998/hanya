package com.singlee.financial.pojo.component;

/**
 * 交易模型(10315域)
 * @author xuqq
 *
 */
public enum TradeInstrumentEnum {

	/**
	 * NTP2改造
	 */
	//Q:QDM
	QDM,
	//O:ODM
	ODM,
	//N:NDM
	NDM,
	/**
	 * NTP1改造
	 *	包含上面的 Q,N项
	 */
	//1:FX-BILATERAL
	FX_BILATERAL,
	//2:FX-CROSS
	FX_CROSS,
	//6:GOLD_BILATERAL
	GOLD_BILATERAL,
	//9:FX_Matching
	FX_Matching
}
