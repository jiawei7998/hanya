package com.singlee.financial.pojo.trade;

import java.io.Serializable;


/**
 * 常备借贷便利资金账号和托管账号
 * @author
 * @version 
 * @since 
 */
public class SLFAccountBean implements Serializable {

    //资金账号
    private String partySubIDType;
    //托管账号
    private String partySubIDType2;
    @Override
    public String toString() {
        return "SLFAccountBean [partySubIDType=" + partySubIDType + ", partySubIDType2=" + partySubIDType2 + "]";
    }
    public String getPartySubIDType() {
        return partySubIDType;
    }
    public void setPartySubIDType(String partySubIDType) {
        this.partySubIDType = partySubIDType;
    }
    public String getPartySubIDType2() {
        return partySubIDType2;
    }
    public void setPartySubIDType2(String partySubIDType2) {
        this.partySubIDType2 = partySubIDType2;
    }
    

    
    

}
