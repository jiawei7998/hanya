package com.singlee.financial.pojo.trade;

import java.io.Serializable;

/**
 * 货币互换交易信息
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class CcsInfoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6203127764249579748L;

	//交易货币
	private String currency                                ;
	//成交编号
	private String execId                               ;
	//成交单状态 0-录入 1-修改 2-撤销
	private String dealTransType                           ;
	//本金
	private String lastQty                                 ;
	//方向
	private String side                                    ;
	//报文生成时间
	private String transactTime                            ;
	//交易期限
	private String settlType                               ;
	//结算日期
	private String settlDate                               ;
	//交易日期
	private String tradeDate                               ;
	//交易确认标识
	private String cfetsCnfmIndicator                      ;
	//清算货币
	private String settlCurrency                           ;
	//即期价格
	private String lastSpotRate                            ;
	//清算方式
	private String netGrossInd                             ;
	//到期日
	private String maturityDate                            ;
	//交易反方货币交易量
	private String calculatedCcyLastQty                    ;
	//交易货币1
	private String currency1                               ;
	//交易货币2
	private String currency2                               ;
	//折美元交易量
	private String riskLastQty                             ;
	//交易方式
	private String tradeInstrument                         ;
	//TradeMethod
	private String tradeMethod                             ;
	//成交时间
	private String tradeTime                               ;
	//市场标识
	private String marketIndicator                         ;
	//本金交换方式 0-Start and End; 1-Start Only; 2-End Only; 3-None
	private String notionalExchangeType                    ;
	//期初本金交换日
	private String iniExDate                    ;
	//期末本金交换日
	private String finalExDate                    ;
	//交易到期日
	private String maturityDateType                        ;
	//残端标识  如果 StubRequired 值 为 Y，则表明这笔交易有残段信息，则使用< StubGrp>组件来表示具体的残端信息
	private String stubRequired                            ;
	//残段信息
	private String noStub                                  ;
	//前后端残段标识
	private String stubIndicator                           ;
	//残段信息说明
	private String stubDescription                         ;
	//起息日调整 Y-是 N-否
	private String dateAdjustmentIndic;
	//备注
	private String text;
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getExecId() {
		return execId;
	}
	public void setExecId(String execId) {
		this.execId = execId;
	}
	public String getDealTransType() {
		return dealTransType;
	}
	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}
	public String getLastQty() {
		return lastQty;
	}
	public void setLastQty(String lastQty) {
		this.lastQty = lastQty;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public String getTransactTime() {
		return transactTime;
	}
	public void setTransactTime(String transactTime) {
		this.transactTime = transactTime;
	}
	public String getSettlType() {
		return settlType;
	}
	public void setSettlType(String settlType) {
		this.settlType = settlType;
	}
	public String getSettlDate() {
		return settlDate;
	}
	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}
	public String getTradeDate() {
		return tradeDate;
	}
	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}
	public String getCfetsCnfmIndicator() {
		return cfetsCnfmIndicator;
	}
	public void setCfetsCnfmIndicator(String cfetsCnfmIndicator) {
		this.cfetsCnfmIndicator = cfetsCnfmIndicator;
	}
	public String getSettlCurrency() {
		return settlCurrency;
	}
	public void setSettlCurrency(String settlCurrency) {
		this.settlCurrency = settlCurrency;
	}
	public String getLastSpotRate() {
		return lastSpotRate;
	}
	public void setLastSpotRate(String lastSpotRate) {
		this.lastSpotRate = lastSpotRate;
	}
	public String getNetGrossInd() {
		return netGrossInd;
	}
	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}
	public String getMaturityDate() {
		return maturityDate;
	}
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	public String getCalculatedCcyLastQty() {
		return calculatedCcyLastQty;
	}
	public void setCalculatedCcyLastQty(String calculatedCcyLastQty) {
		this.calculatedCcyLastQty = calculatedCcyLastQty;
	}
	public String getCurrency1() {
		return currency1;
	}
	public void setCurrency1(String currency1) {
		this.currency1 = currency1;
	}
	public String getCurrency2() {
		return currency2;
	}
	public void setCurrency2(String currency2) {
		this.currency2 = currency2;
	}
	public String getRiskLastQty() {
		return riskLastQty;
	}
	public void setRiskLastQty(String riskLastQty) {
		this.riskLastQty = riskLastQty;
	}
	public String getTradeInstrument() {
		return tradeInstrument;
	}
	public void setTradeInstrument(String tradeInstrument) {
		this.tradeInstrument = tradeInstrument;
	}
	public String getTradeMethod() {
		return tradeMethod;
	}
	public void setTradeMethod(String tradeMethod) {
		this.tradeMethod = tradeMethod;
	}
	public String getTradeTime() {
		return tradeTime;
	}
	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}
	public String getMarketIndicator() {
		return marketIndicator;
	}
	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}
	public String getNotionalExchangeType() {
		return notionalExchangeType;
	}
	public void setNotionalExchangeType(String notionalExchangeType) {
		this.notionalExchangeType = notionalExchangeType;
	}
	public String getMaturityDateType() {
		return maturityDateType;
	}
	public void setMaturityDateType(String maturityDateType) {
		this.maturityDateType = maturityDateType;
	}
	public String getStubRequired() {
		return stubRequired;
	}
	public void setStubRequired(String stubRequired) {
		this.stubRequired = stubRequired;
	}
	public String getNoStub() {
		return noStub;
	}
	public void setNoStub(String noStub) {
		this.noStub = noStub;
	}
	public String getStubIndicator() {
		return stubIndicator;
	}
	public void setStubIndicator(String stubIndicator) {
		this.stubIndicator = stubIndicator;
	}
	public String getStubDescription() {
		return stubDescription;
	}
	public void setStubDescription(String stubDescription) {
		this.stubDescription = stubDescription;
	}
	public String getDateAdjustmentIndic() {
		return dateAdjustmentIndic;
	}
	public void setDateAdjustmentIndic(String dateAdjustmentIndic) {
		this.dateAdjustmentIndic = dateAdjustmentIndic;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getIniExDate() {
		return iniExDate;
	}
	public void setIniExDate(String iniExDate) {
		this.iniExDate = iniExDate;
	}
	public String getFinalExDate() {
		return finalExDate;
	}
	public void setFinalExDate(String finalExDate) {
		this.finalExDate = finalExDate;
	}

}
