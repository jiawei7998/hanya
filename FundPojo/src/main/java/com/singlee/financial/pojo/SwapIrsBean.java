package com.singlee.financial.pojo;

import com.singlee.financial.pojo.trade.FxBaseBean;
import com.singlee.financial.pojo.trade.IrsDetailBean;
import com.singlee.financial.pojo.trade.IrsInfoBean;

/**
 * 利率互换
 * 
 * @author chenxh
 * 
 */
public class SwapIrsBean extends FxBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4644645822494144385L;

	// 利率互换信息
	private IrsInfoBean irsInfo;

	// 本方利率详细信息
	private IrsDetailBean irsDetail;

	// 对收费利率信息
	private IrsDetailBean contraPatryIrsDetail;
	
	private String term;//cstp下行期限接收
	
	private String side;//报文交易方向
	
	private String clearingMethod;
	
	private String netGrossInd;
	
	private String text;
	
	//市场标识
	private String marketIndicator;

	public IrsInfoBean getIrsInfo() {
		if (null == irsInfo) {
			irsInfo = new IrsInfoBean();
		}
		return irsInfo;
	}

	public void setIrsInfo(IrsInfoBean irsInfo) {
		this.irsInfo = irsInfo;
	}

	public IrsDetailBean getIrsDetail() {
		if (null == irsDetail) {
			irsDetail = new IrsDetailBean();
		}
		return irsDetail;
	}

	public void setIrsDetail(IrsDetailBean irsDetail) {
		this.irsDetail = irsDetail;
	}

	public IrsDetailBean getContraPatryIrsDetail() {
		if (null == contraPatryIrsDetail) {
			contraPatryIrsDetail = new IrsDetailBean();
		}
		return contraPatryIrsDetail;
	}

	public void setContraPatryIrsDetail(IrsDetailBean contraPatryIrsDetail) {
		this.contraPatryIrsDetail = contraPatryIrsDetail;
	}

	public String getClearingMethod() {
		return clearingMethod;
	}

	public void setClearingMethod(String clearingMethod) {
		this.clearingMethod = clearingMethod;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getNetGrossInd() {
		return netGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	
}
