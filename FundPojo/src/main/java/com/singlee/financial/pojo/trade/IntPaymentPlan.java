package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;

public class IntPaymentPlan implements Serializable,Comparable<IntPaymentPlan>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//交易号
	private String execId;
	//leg的方向
	private String legSide;
	//序号
	private int seq;
	//本金摊销金额
	private BigDecimal amorPaymentAmt;
	//本金摊销币种
	private String amorPaymentCcy;
	//付息日期
	private String intPayDate;
	//计息开始日期
	private String intSettlDate;
	//计息结束日期
	private String intSettlDate2;
	public BigDecimal getAmorPaymentAmt() {
		return amorPaymentAmt;
	}
	public void setAmorPaymentAmt(BigDecimal amorPaymentAmt) {
		this.amorPaymentAmt = amorPaymentAmt;
	}
	public String getAmorPaymentCcy() {
		return amorPaymentCcy;
	}
	public void setAmorPaymentCcy(String amorPaymentCcy) {
		this.amorPaymentCcy = amorPaymentCcy;
	}
	public String getIntPayDate() {
		return intPayDate;
	}
	public void setIntPayDate(String intPayDate) {
		this.intPayDate = intPayDate;
	}
	public String getIntSettlDate() {
		return intSettlDate;
	}
	public void setIntSettlDate(String intSettlDate) {
		this.intSettlDate = intSettlDate;
	}
	public String getIntSettlDate2() {
		return intSettlDate2;
	}
	public void setIntSettlDate2(String intSettlDate2) {
		this.intSettlDate2 = intSettlDate2;
	}
	public String getExecId() {
		return execId;
	}
	public void setExecId(String execId) {
		this.execId = execId;
	}
	public String getLegSide() {
		return legSide;
	}
	public void setLegSide(String legSide) {
		this.legSide = legSide;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	
	@Override
	public int compareTo(IntPaymentPlan o) {
		if(this.intPayDate != null && o.intPayDate != null){
			return this.intPayDate.compareTo(o.intPayDate);
		}
		return 0;
	}
	
	
}
