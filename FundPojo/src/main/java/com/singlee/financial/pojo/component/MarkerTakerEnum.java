package com.singlee.financial.pojo.component;

/**
 * 发起方/坐市商方向
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public enum MarkerTakerEnum {

	/**
	 * 发起方/本方
	 * CFETS外币交易时对应114-发起方
	 * CFETS本币现券交易时119-买入方
	 * CFETS本币回购交易时119-逆回购方
	 * CFETS本币拆借交易时119-拆入方
	 * CFETS本币债券借贷119-融入方
	 * 同业存单 投资人
	 */
	Taker,
	
	/**
	 * 做市商/对手方
	 * CFETS外币交易时对应113-发起方
	 * CFETS本币现券交易时120-卖出方
	 * CFETS本币回购交易时120-正回购方
	 * CFETS本币拆借交易时119-拆出方方
	 * CFETS本币债券借贷120-融出方
	 * 同业存单 发行人
	 */
	Marker;
	
}
