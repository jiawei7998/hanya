package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.financial.pojo.trade.FxBaseBean;
import com.singlee.financial.pojo.trade.FxOptionCurrencyAmtBean;
import com.singlee.financial.pojo.trade.FxOptionPremiumBean;
import com.singlee.financial.pojo.trade.FxOptionSettlAmtBean;

import java.math.BigDecimal;

/**
 * 期权交易业务对象
 * 
 * @author shenzl
 *
 */
public class FxOptionBean extends FxBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String dealTransType;// 成交单状态
	private String side;// 交易方向
	private String tradeDate;// 交易日期
	private String tradeTime;// 交易时间
	private String execType;// 交易状态
	private String relatedReference;// delta exchange交易编号5.6.1234567
	private String reference;// 行权后对应的EXOP交易代码5.4.1234567
	private String optDataType;// 期权交易成交消息类型:取值1-期权交易数据，2-期权交易行权数据
	private String derivativeExerciseStatus;// 行权状态：1-valid(期权成交时),2-exercised(期权到期正常行权时),3-expired(期权到期放弃行权时)
	private String derivativeExerciseStyle;// 行权方式：0-欧式期权
	private String putOrCall;// 看涨看跌标识：1-call
	private String derivativeOptAttribute;// 交易报价类型：0-ATM
	private String optPayoutType;// 期权类型:1-Vanilla
	private String combinationId;
	private String symbol;// 交易货币对
	private String lastSpotRate;// 即期价格
	private String strikePrice;// 行权价
	private String volatility;// 波动率
	private String netGrossInd;// 交割类型:1-净额交割
	private BigDecimal riskLastQty;// 风险货币量
	private String maturityDate;// 行权截止日
	private String expireTime;// 行权截止时间
	private String expireTimeZone;// 行权截止时间时区
	private String settlType;// 行权期限
	private String strikeTime;// 行权时间
	private String strikeUser;// 行权用户
	private String strikeDayPrice;// 行权日，当天的价格
	private String marketIndicator;// 市场标识
	private FxOptionCurrencyAmtBean ccy1;// 币种1
	private FxOptionCurrencyAmtBean ccy2;// 币种2
	private FxOptionPremiumBean premium;// 期权费
	private FxOptionSettlAmtBean optSettlAmtBean;// 执行行权清算信息
	private int fxOptionsType;// 期权交易类型
	private String benchmarkCurveName;
	
	//远端本方清算信息
	private FxSettBean farSettlement;

	//远端对手方清算信息
	private FxSettBean farCounterPartySettlement;
	
	/**
	 * 获取远端本方清算信息
	 * @return
	 */
	public FxSettBean getFarSettlement() {
		if(null == farSettlement){
			farSettlement = new FxSettBean();
		}
		return farSettlement;
	}

	/**
	 * 设置远端本方清算信息
	 * @param farSettlement
	 */
	public void setFarSettlement(FxSettBean farSettlement) {
		this.farSettlement = farSettlement;
	}

	/**
	 * 获取远端对手方清算信息
	 * @return
	 */
	public FxSettBean getFarCounterPartySettlement() {
		if(null == farCounterPartySettlement){
			farCounterPartySettlement = new FxSettBean();
		}
		return farCounterPartySettlement;
	}

	/**
	 * 设置远端对手方清算信息
	 * @param farCounterPartySettlement
	 */
	public void setFarCounterPartySettlement(FxSettBean farCounterPartySettlement) {
		this.farCounterPartySettlement = farCounterPartySettlement;
	}

	public int getFxOptionsType() {
		return fxOptionsType;
	}

	public void setFxOptionsType(int fxOptionsType) {
		this.fxOptionsType = fxOptionsType;
	}

	public String getOptDataType() {
		return optDataType;
	}

	public void setOptDataType(String optDataType) {
		this.optDataType = optDataType;
	}

	public String getNetGrossInd() {
		return netGrossInd;
	}

	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}

	public String getExpireTimeZone() {
		return expireTimeZone;
	}

	public void setExpireTimeZone(String expireTimeZone) {
		this.expireTimeZone = expireTimeZone;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	public FxOptionSettlAmtBean getOptSettlAmtBean() {
		return optSettlAmtBean;
	}

	public void setOptSettlAmtBean(FxOptionSettlAmtBean optSettlAmtBean) {
		this.optSettlAmtBean = optSettlAmtBean;
	}

	public String getStrikeTime() {
		return strikeTime;
	}

	public void setStrikeTime(String strikeTime) {
		this.strikeTime = strikeTime;
	}

	public String getStrikeUser() {
		return strikeUser;
	}

	public void setStrikeUser(String strikeUser) {
		this.strikeUser = strikeUser;
	}

	public String getStrikeDayPrice() {
		return strikeDayPrice;
	}

	public void setStrikeDayPrice(String strikeDayPrice) {
		this.strikeDayPrice = strikeDayPrice;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public FxOptionPremiumBean getPremium() {
		if(premium == null){
			premium = new FxOptionPremiumBean();
		}
		return premium;
	}

	public void setPremium(FxOptionPremiumBean premium) {
		this.premium = premium;
	}

	public String getDealTransType() {
		return dealTransType;
	}

	public void setDealTransType(String dealTransType) {
		this.dealTransType = dealTransType;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}
	@Override
	public String getTradeTime() {
		return tradeTime;
	}
	@Override
	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getRelatedReference() {
		return relatedReference;
	}

	public void setRelatedReference(String relatedReference) {
		this.relatedReference = relatedReference;
	}

	

	public String getDerivativeExerciseStatus() {
		return derivativeExerciseStatus;
	}

	public void setDerivativeExerciseStatus(String derivativeExerciseStatus) {
		this.derivativeExerciseStatus = derivativeExerciseStatus;
	}

	public String getDerivativeExerciseStyle() {
		return derivativeExerciseStyle;
	}

	public void setDerivativeExerciseStyle(String derivativeExerciseStyle) {
		this.derivativeExerciseStyle = derivativeExerciseStyle;
	}

	public String getPutOrCall() {
		return putOrCall;
	}

	public void setPutOrCall(String putOrCall) {
		this.putOrCall = putOrCall;
	}

	public String getDerivativeOptAttribute() {
		return derivativeOptAttribute;
	}

	public void setDerivativeOptAttribute(String derivativeOptAttribute) {
		this.derivativeOptAttribute = derivativeOptAttribute;
	}

	public String getOptPayoutType() {
		return optPayoutType;
	}

	public void setOptPayoutType(String optPayoutType) {
		this.optPayoutType = optPayoutType;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getLastSpotRate() {
		return lastSpotRate;
	}

	public void setLastSpotRate(String lastSpotRate) {
		this.lastSpotRate = lastSpotRate;
	}

	public String getStrikePrice() {
		return strikePrice;
	}

	public void setStrikePrice(String strikePrice) {
		this.strikePrice = strikePrice;
	}

	public String getVolatility() {
		return volatility;
	}

	public void setVolatility(String volatility) {
		this.volatility = volatility;
	}
	@Override
	public void setRiskLastQty(BigDecimal riskLastQty) {
		this.riskLastQty = riskLastQty;
	}

	public String getMaturityDate() {
		return maturityDate;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}

	public String getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(String expireTime) {
		this.expireTime = expireTime;
	}

	public String getSettlType() {
		return settlType;
	}

	public void setSettlType(String settlType) {
		this.settlType = settlType;
	}

	public FxOptionCurrencyAmtBean getCcy1() {
		return ccy1;
	}

	public void setCcy1(FxOptionCurrencyAmtBean ccy1) {
		this.ccy1 = ccy1;
	}

	public FxOptionCurrencyAmtBean getCcy2() {
		return ccy2;
	}

	public void setCcy2(FxOptionCurrencyAmtBean ccy2) {
		this.ccy2 = ccy2;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(String combinationId) {
		this.combinationId = combinationId;
	}
	@Override
	public BigDecimal getRiskLastQty() {
		return riskLastQty;
	}

	public String getBenchmarkCurveName() {
		return benchmarkCurveName;
	}

	public void setBenchmarkCurveName(String benchmarkCurveName) {
		this.benchmarkCurveName = benchmarkCurveName;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FxOpitionBean [CFETSCnfmIndicator=");
		builder.append(getCfetsCnfmIndicator());
		builder.append(", execID=");
		builder.append(getExecId());
		builder.append(", ExpireTimeZone=");
		builder.append(expireTimeZone);
		builder.append(", NetGrossInd=");
		builder.append(netGrossInd);
		builder.append(", OPTDataType=");
		builder.append(optDataType);
		builder.append(", ccy1=");
		builder.append(ccy1);
		builder.append(", ccy2=");
		builder.append(ccy2);
		builder.append(", dealTransType=");
		builder.append(dealTransType);
		builder.append(", derivativeExerciseStatus=");
		builder.append(derivativeExerciseStatus);
		builder.append(", derivativeExerciseStyle=");
		builder.append(derivativeExerciseStyle);
		builder.append(", derivativeOptAttribute=");
		builder.append(derivativeOptAttribute);
		builder.append(", execType=");
		builder.append(execType);
		builder.append(", expireTime=");
		builder.append(expireTime);
		builder.append(", lastSpotRate=");
		builder.append(lastSpotRate);
		builder.append(", maturityDate=");
		builder.append(maturityDate);
		builder.append(", optPayoutType=");
		builder.append(optPayoutType);
		builder.append(", optSettlAmtBean=");
		builder.append(optSettlAmtBean);
		builder.append(", premium=");
		builder.append(premium);
		builder.append(", putOrCall=");
		builder.append(putOrCall);
		builder.append(", reference=");
		builder.append(reference);
		builder.append(", relatedReference=");
		builder.append(relatedReference);
		builder.append(", riskLastQty=");
		builder.append(riskLastQty);
		builder.append(", settlType=");
		builder.append(settlType);
		builder.append(", side=");
		builder.append(side);
		builder.append(", strikeDayPrice=");
		builder.append(strikeDayPrice);
		builder.append(", strikePrice=");
		builder.append(strikePrice);
		builder.append(", strikeTime=");
		builder.append(strikeTime);
		builder.append(", strikeUser=");
		builder.append(strikeUser);
		builder.append(", symbol=");
		builder.append(symbol);
		builder.append(", tradeDate=");
		builder.append(tradeDate);
		builder.append(", tradeTime=");
		builder.append(tradeTime);
		builder.append(", volatility=");
		builder.append(volatility);
		builder.append("]");
		return builder.toString();
	}
	
	
}
