package com.singlee.financial.pojo;

import java.io.Serializable;

public class TradeSettlsBean implements Serializable {
    /**
     * 交易单号
     */
    private String dealNo;

    /**
     * 交易类型
     */
    private String tradeType;

    /**
     * 清算方式
     */
    private String netGrossInd;

    /**
     * 市场标识
     */
    private String marketIndicator;

    /**
     * 成交单号
     */
    private String execId;

    /**
     * 本方机构21机构码
     */
    private String institutionId;

    /**
     * 本方交易员
     */
    private String tradeId;

    /**
     * 本方交易员名称
     */
    private String tradeName;

    /**
     * 本方机构简称
     */
    private String shortName;

    /**
     * 本方机构全称
     */
    private String fullName;

    /**
     * 本方机构角色
     */
    private String markerTakerTole;

    /**
     * 近端清算货币
     */
    private String settccy;

    /**
     * 近端本方开户行名称
     */
    private String bankname;

    /**
     * 近端本方开户行SWIFT CODE(外币)/开户行行号(人民币)
     */
    private String bankopenno;

    /**
     * 近端本方支付系统行号
     */
    private String bankacctno;

    /**
     * 近端本方账户名称
     */
    private String acctname;

    /**
     * 近端本方账户行SWIFT CODE/支付系统行号
     */
    private String acctopenno;

    /**
     * 近端本方资金账号
     */
    private String acctno;

    /**
     * 近端本方中间行名称
     */
    private String intermediarybankname;

    /**
     * 近端本方中间行SWIFT CODE
     */
    private String intermediarybankbiccode;

    /**
     * 近端本方中间行在开户行的资金账户
     */
    private String intermediarybankacctno;

    /**
     * 近端本方附言
     */
    private String remark;

    /**
     * 远端本方开户行名称
     */
    private String farBankname;

    /**
     * 远端本方开户行SWIFT CODE(外币)/开户行行号(人民币)
     */
    private String farBankopenno;

    /**
     * 远端本方支付系统行号
     */
    private String farBankacctno;

    /**
     * 远端本方账户名称
     */
    private String farAcctname;

    /**
     * 远端本方账户行SWIFT CODE/支付系统行号
     */
    private String farAcctopenno;

    /**
     * 远端本方资金账号
     */
    private String farAcctno;

    /**
     * 远端本方中间行名称
     */
    private String farIntermediarybankname;

    /**
     * 远端本方中间行SWIFT CODE
     */
    private String farIntermediarybankbiccode;

    /**
     * 远端本方中间行在开户行的资金账户
     */
    private String farIntermediarybankacctno;

    /**
     * 远端本方附言
     */
    private String farRemark;

    /**
     * 对手方21位机构码
     */
    private String cInstitutionId;

    /**
     * 对手方交易员
     */
    private String cIradeId;

    /**
     * 对手方交易员名称
     */
    private String cIradeName;

    /**
     * 对手方机构简称
     */
    private String cShortName;

    /**
     * 对手方机构全称
     */
    private String cFullName;

    /**
     * 对手方机构角色
     */
    private String cMarkerTakerTole;

    /**
     * 远端清算货币
     */
    private String cSettccy;

    /**
     * 近端对手方开户行名称
     */
    private String cBankname;

    /**
     * 近端对手方开户行SWIFT CODE(外币)/开户行行号(人民币)
     */
    private String cBankopenno;

    /**
     * 近端对手方支付系统行号
     */
    private String cBankacctno;

    /**
     * 近端对手方账户名称
     */
    private String cAcctname;

    /**
     * 近端对手方账户行SWIFT CODE/支付系统行号
     */
    private String cAcctopenno;

    /**
     * 近端对手方资金账号
     */
    private String cAcctno;

    /**
     * 近端对手方中间行名称
     */
    private String cIntermediarybankname;

    /**
     * 近端对手方中间行SWIFT CODE
     */
    private String cIntermediarybankbiccode;

    /**
     * 近端对手方中间行在开户行的资金账户
     */
    private String cIntermediarybankacctno;

    /**
     * 近端对手方附言
     */
    private String cRemark;

    /**
     * 远端对手方开户行名称
     */
    private String cFarBankname;

    /**
     * 远端对手方开户行SWIFT CODE(外币)/开户行行号(人民币)
     */
    private String cFarBankopenno;

    /**
     * 远端对手方支付系统行号
     */
    private String cFarBankacctno;

    /**
     * 远端对手方账户名称
     */
    private String cFarAcctname;

    /**
     * 远端对手方账户行SWIFT CODE/支付系统行号
     */
    private String cFarAcctopenno;

    /**
     * 远端对手方资金账号
     */
    private String cFarAcctno;

    /**
     * 远端对手方中间行名称
     */
    private String cFarIntermediarybankname;

    /**
     * 远端对手方中间行SWIFT CODE
     */
    private String cFarIntermediarybankbiccode;

    /**
     * 远端对手方中间行在开户行的资金账户
     */
    private String cFarIntermediarybankacctno;

    /**
     * 远端对手方附言
     */
    private String cFarRemark;

    public String getDealNo() {
        return dealNo;
    }

    public void setDealNo(String dealNo) {
        this.dealNo = dealNo;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getNetGrossInd() {
        return netGrossInd;
    }

    public void setNetGrossInd(String netGrossInd) {
        this.netGrossInd = netGrossInd;
    }

    public String getMarketIndicator() {
        return marketIndicator;
    }

    public void setMarketIndicator(String marketIndicator) {
        this.marketIndicator = marketIndicator;
    }

    public String getExecId() {
        return execId;
    }

    public void setExecId(String execId) {
        this.execId = execId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMarkerTakerTole() {
        return markerTakerTole;
    }

    public void setMarkerTakerTole(String markerTakerTole) {
        this.markerTakerTole = markerTakerTole;
    }

    public String getSettccy() {
        return settccy;
    }

    public void setSettccy(String settccy) {
        this.settccy = settccy;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBankopenno() {
        return bankopenno;
    }

    public void setBankopenno(String bankopenno) {
        this.bankopenno = bankopenno;
    }

    public String getBankacctno() {
        return bankacctno;
    }

    public void setBankacctno(String bankacctno) {
        this.bankacctno = bankacctno;
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }

    public String getAcctopenno() {
        return acctopenno;
    }

    public void setAcctopenno(String acctopenno) {
        this.acctopenno = acctopenno;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getIntermediarybankname() {
        return intermediarybankname;
    }

    public void setIntermediarybankname(String intermediarybankname) {
        this.intermediarybankname = intermediarybankname;
    }

    public String getIntermediarybankbiccode() {
        return intermediarybankbiccode;
    }

    public void setIntermediarybankbiccode(String intermediarybankbiccode) {
        this.intermediarybankbiccode = intermediarybankbiccode;
    }

    public String getIntermediarybankacctno() {
        return intermediarybankacctno;
    }

    public void setIntermediarybankacctno(String intermediarybankacctno) {
        this.intermediarybankacctno = intermediarybankacctno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFarBankname() {
        return farBankname;
    }

    public void setFarBankname(String farBankname) {
        this.farBankname = farBankname;
    }

    public String getFarBankopenno() {
        return farBankopenno;
    }

    public void setFarBankopenno(String farBankopenno) {
        this.farBankopenno = farBankopenno;
    }

    public String getFarBankacctno() {
        return farBankacctno;
    }

    public void setFarBankacctno(String farBankacctno) {
        this.farBankacctno = farBankacctno;
    }

    public String getFarAcctname() {
        return farAcctname;
    }

    public void setFarAcctname(String farAcctname) {
        this.farAcctname = farAcctname;
    }

    public String getFarAcctopenno() {
        return farAcctopenno;
    }

    public void setFarAcctopenno(String farAcctopenno) {
        this.farAcctopenno = farAcctopenno;
    }

    public String getFarAcctno() {
        return farAcctno;
    }

    public void setFarAcctno(String farAcctno) {
        this.farAcctno = farAcctno;
    }

    public String getFarIntermediarybankname() {
        return farIntermediarybankname;
    }

    public void setFarIntermediarybankname(String farIntermediarybankname) {
        this.farIntermediarybankname = farIntermediarybankname;
    }

    public String getFarIntermediarybankbiccode() {
        return farIntermediarybankbiccode;
    }

    public void setFarIntermediarybankbiccode(String farIntermediarybankbiccode) {
        this.farIntermediarybankbiccode = farIntermediarybankbiccode;
    }

    public String getFarIntermediarybankacctno() {
        return farIntermediarybankacctno;
    }

    public void setFarIntermediarybankacctno(String farIntermediarybankacctno) {
        this.farIntermediarybankacctno = farIntermediarybankacctno;
    }

    public String getFarRemark() {
        return farRemark;
    }

    public void setFarRemark(String farRemark) {
        this.farRemark = farRemark;
    }

    public String getcInstitutionId() {
        return cInstitutionId;
    }

    public void setcInstitutionId(String cInstitutionId) {
        this.cInstitutionId = cInstitutionId;
    }

    public String getcIradeId() {
        return cIradeId;
    }

    public void setcIradeId(String cIradeId) {
        this.cIradeId = cIradeId;
    }

    public String getcIradeName() {
        return cIradeName;
    }

    public void setcIradeName(String cIradeName) {
        this.cIradeName = cIradeName;
    }

    public String getcShortName() {
        return cShortName;
    }

    public void setcShortName(String cShortName) {
        this.cShortName = cShortName;
    }

    public String getcFullName() {
        return cFullName;
    }

    public void setcFullName(String cFullName) {
        this.cFullName = cFullName;
    }

    public String getcMarkerTakerTole() {
        return cMarkerTakerTole;
    }

    public void setcMarkerTakerTole(String cMarkerTakerTole) {
        this.cMarkerTakerTole = cMarkerTakerTole;
    }

    public String getcSettccy() {
        return cSettccy;
    }

    public void setcSettccy(String cSettccy) {
        this.cSettccy = cSettccy;
    }

    public String getcBankname() {
        return cBankname;
    }

    public void setcBankname(String cBankname) {
        this.cBankname = cBankname;
    }

    public String getcBankopenno() {
        return cBankopenno;
    }

    public void setcBankopenno(String cBankopenno) {
        this.cBankopenno = cBankopenno;
    }

    public String getcBankacctno() {
        return cBankacctno;
    }

    public void setcBankacctno(String cBankacctno) {
        this.cBankacctno = cBankacctno;
    }

    public String getcAcctname() {
        return cAcctname;
    }

    public void setcAcctname(String cAcctname) {
        this.cAcctname = cAcctname;
    }

    public String getcAcctopenno() {
        return cAcctopenno;
    }

    public void setcAcctopenno(String cAcctopenno) {
        this.cAcctopenno = cAcctopenno;
    }

    public String getcAcctno() {
        return cAcctno;
    }

    public void setcAcctno(String cAcctno) {
        this.cAcctno = cAcctno;
    }

    public String getcIntermediarybankname() {
        return cIntermediarybankname;
    }

    public void setcIntermediarybankname(String cIntermediarybankname) {
        this.cIntermediarybankname = cIntermediarybankname;
    }

    public String getcIntermediarybankbiccode() {
        return cIntermediarybankbiccode;
    }

    public void setcIntermediarybankbiccode(String cIntermediarybankbiccode) {
        this.cIntermediarybankbiccode = cIntermediarybankbiccode;
    }

    public String getcIntermediarybankacctno() {
        return cIntermediarybankacctno;
    }

    public void setcIntermediarybankacctno(String cIntermediarybankacctno) {
        this.cIntermediarybankacctno = cIntermediarybankacctno;
    }

    public String getcRemark() {
        return cRemark;
    }

    public void setcRemark(String cRemark) {
        this.cRemark = cRemark;
    }

    public String getcFarBankname() {
        return cFarBankname;
    }

    public void setcFarBankname(String cFarBankname) {
        this.cFarBankname = cFarBankname;
    }

    public String getcFarBankopenno() {
        return cFarBankopenno;
    }

    public void setcFarBankopenno(String cFarBankopenno) {
        this.cFarBankopenno = cFarBankopenno;
    }

    public String getcFarBankacctno() {
        return cFarBankacctno;
    }

    public void setcFarBankacctno(String cFarBankacctno) {
        this.cFarBankacctno = cFarBankacctno;
    }

    public String getcFarAcctname() {
        return cFarAcctname;
    }

    public void setcFarAcctname(String cFarAcctname) {
        this.cFarAcctname = cFarAcctname;
    }

    public String getcFarAcctopenno() {
        return cFarAcctopenno;
    }

    public void setcFarAcctopenno(String cFarAcctopenno) {
        this.cFarAcctopenno = cFarAcctopenno;
    }

    public String getcFarAcctno() {
        return cFarAcctno;
    }

    public void setcFarAcctno(String cFarAcctno) {
        this.cFarAcctno = cFarAcctno;
    }

    public String getcFarIntermediarybankname() {
        return cFarIntermediarybankname;
    }

    public void setcFarIntermediarybankname(String cFarIntermediarybankname) {
        this.cFarIntermediarybankname = cFarIntermediarybankname;
    }

    public String getcFarIntermediarybankbiccode() {
        return cFarIntermediarybankbiccode;
    }

    public void setcFarIntermediarybankbiccode(String cFarIntermediarybankbiccode) {
        this.cFarIntermediarybankbiccode = cFarIntermediarybankbiccode;
    }

    public String getcFarIntermediarybankacctno() {
        return cFarIntermediarybankacctno;
    }

    public void setcFarIntermediarybankacctno(String cFarIntermediarybankacctno) {
        this.cFarIntermediarybankacctno = cFarIntermediarybankacctno;
    }

    public String getcFarRemark() {
        return cFarRemark;
    }

    public void setcFarRemark(String cFarRemark) {
        this.cFarRemark = cFarRemark;
    }
}