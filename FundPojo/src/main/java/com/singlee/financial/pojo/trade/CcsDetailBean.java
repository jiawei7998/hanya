package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 利率互换明细,包含利率、利差、首次支付日、支付周期、首次利率确认日、利率修正周期、计息方式、计息基础
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class CcsDetailBean implements Serializable {

	private static final long serialVersionUID = -6203127764249579748L;

	//1.固定利率支付  B.基准端
	private String legSide;	
	// leg Ps
	private String legPs;	
	//6.浮动值   3.固定值
	private int legPriceType;
	//参考利率  浮动利率时有值
	private String legBenchmarkCurveName;
	//利差
	private BigDecimal legBenchmarkSpread;
	//固定利率
	private BigDecimal legPrice;
	//Benchmark对应的期限
	private String legBenchmarkTenor;
	//legPriceType 为固定式表示利率  浮动时表示利差
	private BigDecimal legLastPx;
	//交易金额
	private BigDecimal legOrderQty;
	//币种
	private String legCurrency;
	//IRS 交易端是否是询价关联端
	private String legSolveForIndicator;
	//计息基准
	private String legDayCount;
	//双边支付频率
	private String legCouponPaymentFrequency;
	//双边支付日调整
	private String legCouponPaymentDateReset;
	//重置日调整
	private String legInterestAccrualResetDateAdjust;
	//重置频率
	private String legInterestAccrualResetFrequency;
	//定息规则 0-V-0;1-V-1;2-V-2
	private String legInterestFixDateAdjustment;
	/**首次支付日*/
	private String legFirstPayDate;
	//残端标识
	private int legStubIndicator; 
	
	private List<IntPaymentPlan> intPaymentPlans;

	public List<IntPaymentPlan> getIntPaymentPlans() {
		if(intPaymentPlans == null){
			intPaymentPlans = new ArrayList<IntPaymentPlan>();
		}
		return intPaymentPlans;
	}

	public void setIntPaymentPlans(List<IntPaymentPlan> intPaymentPlans) {
		this.intPaymentPlans = intPaymentPlans;
	}


	public BigDecimal getLegPrice() {
		return legPrice;
	}


	public void setLegPrice(BigDecimal legPrice) {
		this.legPrice = legPrice;
	}


	public int getLegStubIndicator() {
		return legStubIndicator;
	}


	public void setLegStubIndicator(int legStubIndicator) {
		this.legStubIndicator = legStubIndicator;
	}


	public String getLegSide() {
		return legSide;
	}


	public void setLegSide(String legSide) {
		this.legSide = legSide;
	}

	public String getLegFirstPayDate() {
		return legFirstPayDate;
	}


	public void setLegFirstPayDate(String legFirstPayDate) {
		this.legFirstPayDate = legFirstPayDate;
	}


	public int getLegPriceType() {
		return legPriceType;
	}


	public void setLegPriceType(int legPriceType) {
		this.legPriceType = legPriceType;
	}

	public String getLegBenchmarkCurveName() {
		return legBenchmarkCurveName;
	}

	public void setLegBenchmarkCurveName(String legBenchmarkCurveName) {
		this.legBenchmarkCurveName = legBenchmarkCurveName;
	}

	public BigDecimal getLegBenchmarkSpread() {
		return legBenchmarkSpread;
	}


	public void setLegBenchmarkSpread(BigDecimal legBenchmarkSpread) {
		this.legBenchmarkSpread = legBenchmarkSpread;
	}


	public String getLegBenchmarkTenor() {
		return legBenchmarkTenor;
	}


	public void setLegBenchmarkTenor(String legBenchmarkTenor) {
		this.legBenchmarkTenor = legBenchmarkTenor;
	}


	public BigDecimal getLegLastPx() {
		return legLastPx;
	}


	public void setLegLastPx(BigDecimal legLastPx) {
		this.legLastPx = legLastPx;
	}


	public BigDecimal getLegOrderQty() {
		return legOrderQty;
	}


	public void setLegOrderQty(BigDecimal legOrderQty) {
		this.legOrderQty = legOrderQty;
	}


	public String getLegCurrency() {
		return legCurrency;
	}


	public void setLegCurrency(String legCurrency) {
		this.legCurrency = legCurrency;
	}


	public String getLegSolveForIndicator() {
		return legSolveForIndicator;
	}


	public void setLegSolveForIndicator(String legSolveForIndicator) {
		this.legSolveForIndicator = legSolveForIndicator;
	}


	public String getLegDayCount() {
		return legDayCount;
	}


	public void setLegDayCount(String legDayCount) {
		this.legDayCount = legDayCount;
	}


	public String getLegCouponPaymentFrequency() {
		return legCouponPaymentFrequency;
	}


	public void setLegCouponPaymentFrequency(String legCouponPaymentFrequency) {
		this.legCouponPaymentFrequency = legCouponPaymentFrequency;
	}


	public String getLegCouponPaymentDateReset() {
		return legCouponPaymentDateReset;
	}


	public void setLegCouponPaymentDateReset(String legCouponPaymentDateReset) {
		this.legCouponPaymentDateReset = legCouponPaymentDateReset;
	}


	public String getLegInterestAccrualResetDateAdjust() {
		return legInterestAccrualResetDateAdjust;
	}


	public void setLegInterestAccrualResetDateAdjust(
			String legInterestAccrualResetDateAdjust) {
		this.legInterestAccrualResetDateAdjust = legInterestAccrualResetDateAdjust;
	}


	public String getLegInterestAccrualResetFrequency() {
		return legInterestAccrualResetFrequency;
	}


	public void setLegInterestAccrualResetFrequency(
			String legInterestAccrualResetFrequency) {
		this.legInterestAccrualResetFrequency = legInterestAccrualResetFrequency;
	}


	public String getLegInterestFixDateAdjustment() {
		return legInterestFixDateAdjustment;
	}


	public void setLegInterestFixDateAdjustment(String legInterestFixDateAdjustment) {
		this.legInterestFixDateAdjustment = legInterestFixDateAdjustment;
	}


	public String getLegPs() {
		return legPs;
	}

	public void setLegPs(String legPs) {
		this.legPs = legPs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CcsDetailBean");
		builder.append("[ legSide = " +  legSide	                        );  
		builder.append(", legPriceType = " +  legPriceType                     );  
		builder.append(", legBenchmarkCurveName = " +  legBenchmarkCurveName            );  
		builder.append(", legBenchmarkTenor = " +  legBenchmarkTenor                );  
		builder.append(", legLastPx = " +  legLastPx                        );  
		builder.append(", legOrderQty = " +  legOrderQty                      );  
		builder.append(", legCurrency = " +  legCurrency                      );  
		builder.append(", legSolveForIndicator = " +  legSolveForIndicator             );  
		builder.append(", legDayCount = " +  legDayCount                      );  
		builder.append(", legCouponPaymentFrequency = " +  legCouponPaymentFrequency        );  
		builder.append(", legCouponPaymentDateReset = " +  legCouponPaymentDateReset        );  
		builder.append(", legInterestAccrualResetDateAdjust = " +  legInterestAccrualResetDateAdjust);  
		builder.append(", legInterestAccrualResetFrequency = " +  legInterestAccrualResetFrequency );  
		builder.append(", legInterestFixDateAdjustment = " +  legInterestFixDateAdjustment     );  
		builder.append("]");
		return builder.toString();
	}

}
