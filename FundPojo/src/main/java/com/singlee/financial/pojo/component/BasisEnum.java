package com.singlee.financial.pojo.component;

/**
 * 计息基础转换
 * 
 * @author shenzl
 *
 */
public enum BasisEnum {
	
	/**
	 * Actual/365-3
	 */
	A365,
	/**
	 * Actual/Actual-0
	 */
	ACTUAL,
	/**
	 * Actual/360-1
	 */
	A360
}
