package com.singlee.financial.pojo.component;

public enum SptFwdTypeEnum {

	/**
	 * 即期交易
	 */
	SPOT,
	
	/**
	 * 远期交易
	 */
	FORWARD
}
