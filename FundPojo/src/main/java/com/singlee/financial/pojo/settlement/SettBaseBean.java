package com.singlee.financial.pojo.settlement;

import com.singlee.financial.pojo.component.MarkerTakerEnum;

import java.io.Serializable;

/**
 * 机构清算账号信息,保存资金交割时使用的行号、账号等信息
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SettBaseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4819449779002414581L;
	
	//发起方/做市商标识
	private MarkerTakerEnum markerTakerRole;

	//资金开户行
	private String bankName;
	
	//支付系统行号
	private String bankAcctNo;
	
	//资金账号户名
	private String acctName;
	
	//资金账号
	private String acctNo;	
	
	//附言、备注
	private String remark;

	/**
	 * 获取开户行名称
	 * @return
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * 设置开户行名称
	 * @param bankName
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * 获取账户名称
	 * @return
	 */
	public String getAcctName() {
		return acctName;
	}

	/**
	 * 设置账户名称
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	/**
	 * 获取资金账号
	 * @return
	 */
	public String getAcctNo() {
		return acctNo;
	}

	/**
	 * 设置资金账号
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	/**
	 * 获取发起方/做市商标识
	 * @return
	 */
	public MarkerTakerEnum getMarkerTakerRole() {
		return markerTakerRole;
	}

	/**
	 * 设置发起方/做市商标识
	 * @param markerTakerRole
	 */
	public void setMarkerTakerRole(MarkerTakerEnum markerTakerRole) {
		this.markerTakerRole = markerTakerRole;
	}

	/**
	 * 获取支付系统行号
	 * @return
	 */
	public String getBankAcctNo() {
		return bankAcctNo;
	}

	/**
	 * 设置支付系统行号
	 * @param bankAcctNo
	 */
	public void setBankAcctNo(String bankAcctNo) {
		this.bankAcctNo = bankAcctNo;
	}

	/**
	 * 获取附言、备注信息
	 * @return
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * 设置附言、备注信息
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SettBaseBean [acctName=");
		builder.append(acctName);
		builder.append(", acctNo=");
		builder.append(acctNo);
		builder.append(", bankAcctNo=");
		builder.append(bankAcctNo);
		builder.append(", bankName=");
		builder.append(bankName);
		builder.append(", markerTakerRole=");
		builder.append(markerTakerRole);
		builder.append(", remark=");
		builder.append(remark);
		builder.append("]");
		return builder.toString();
	}
	
}
