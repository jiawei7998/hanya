package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

import java.math.BigDecimal;

/***
 * 
 * 标准债券远期
 * @author lee
 *
 */
public class StandardBfdBean extends TradeBaseBean{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4799646628212957165L;
	//交易日期
	private String tradeDate;
	//本方清算信息
	private SecurSettBean settInfo;
	//对手方清算信息
	private SecurSettBean contraPartySettInfo;
	//合约代码
	private String securityId;
	//品种名称
	private String securityGroup;
	//交割日
	private String settlDate;
	//价格
	private BigDecimal price;
	//金额
	private BigDecimal amt;
	//清算场所 6 上海清算所  13 双边自行清算
	private Integer clearingMethod;
	//是否备注
	private Boolean remarkIndicator;
	//备注信息
	private String text;
	
	/**
	 * 获取本方债券清算信息
	 * @return
	 */
	public SecurSettBean getSettInfo() {
		if(null == settInfo){
			settInfo = new SecurSettBean();
		}
		return settInfo;
	}

	/**
	 * 设置本方债券清算信息
	 * @param settInfo
	 */
	public void setSettInfo(SecurSettBean settInfo) {
		this.settInfo = settInfo;
	}

	/**
	 * 获取对手方债券清算信息
	 * @return
	 */
	public SecurSettBean getContraPartySettInfo() {
		if(null == contraPartySettInfo){
			contraPartySettInfo = new SecurSettBean();
		}
		return contraPartySettInfo;
	}

	/**
	 * 设置对手方债券清算信息
	 * @param contraPartySettInfo
	 */
	public void setContraPartySettInfo(SecurSettBean contraPartySettInfo) {
		this.contraPartySettInfo = contraPartySettInfo;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getSecurityId() {
		return securityId;
	}

	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}

	public String getSecurityGroup() {
		return securityGroup;
	}

	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

	public String getSettlDate() {
		return settlDate;
	}

	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public Integer getClearingMethod() {
		return clearingMethod;
	}

	public void setClearingMethod(Integer clearingMethod) {
		this.clearingMethod = clearingMethod;
	}

	public Boolean getRemarkIndicator() {
		return remarkIndicator;
	}

	public void setRemarkIndicator(Boolean remarkIndicator) {
		this.remarkIndicator = remarkIndicator;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
