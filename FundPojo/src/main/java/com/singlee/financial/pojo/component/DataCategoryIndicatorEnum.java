package com.singlee.financial.pojo.component;

/**
 * 交易方式
 * @author chenxh
 *
 */
public enum DataCategoryIndicatorEnum {

	/**
	 * 本方数据
	 */
	THIS,
	
	/**
	 * 关联机构数据
	 */
	AI,
	
	/**
	 * 债券关联机构数据
	 */
	BAI
}
