package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 债券远期 对象
 * 
 */
public class BfdBean extends TradeBaseBean{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4799646628212957165L;
	//结算日期
	private String settlDate;
	//应计利息总额
	private BigDecimal accruedIntAmt;  
	//交易金额
	private BigDecimal tradeAmt;  
	//结算币种
	private String settlCurrency; 
	//结算金额
	private BigDecimal settlCurrAmt; 
	//合约期限
	private BigDecimal tradeLimitDays;
	//代偿期
	private String termToMaturity;
	
	//债券信息
	private SecurityInfoBean securityInfo;
	
	//债券交易金额
	private SecurityAmtDetailBean securityAmt;
	
	//本方清算信息
	private SecurSettBean settInfo;
	
	//对手方清算信息
	private SecurSettBean contraPartySettInfo;

	//更新日
	private Date transactTime;
	
	public BigDecimal getTradeLimitDays() {
		return tradeLimitDays;
	}

	public void setTradeLimitDays(BigDecimal tradeLimitDays) {
		this.tradeLimitDays = tradeLimitDays;
	}

	public String getTermToMaturity() {
		return termToMaturity;
	}

	public void setTermToMaturity(String termToMaturity) {
		this.termToMaturity = termToMaturity;
	}

	/**
	 * 获取债券信息
	 * @return
	 */
	public SecurityInfoBean getSecurityInfo() {
		if(null == securityInfo){
			securityInfo = new SecurityInfoBean();
		}
		return securityInfo;
	}

	/**
	 * 设置债券信息
	 * @param securityInfo
	 */
	public void setSecurityInfo(SecurityInfoBean securityInfo) {
		this.securityInfo = securityInfo;
	}
	
	/**
	 * 获取债券交易金额组件,包含清算方式,结算金额,应计利息,结算日期
	 * @return
	 */
	public SecurityAmtDetailBean getSecurityAmt() {
		if(null == securityAmt){
			securityAmt = new SecurityAmtDetailBean();
		}
		return securityAmt;
	}

	/**
	 * 设置债券交易金额组件,包含清算方式,结算金额,应计利息,结算日期
	 * @param securityAmt
	 */
	public void setSecurityAmt(SecurityAmtDetailBean securityAmt) {
		this.securityAmt = securityAmt;
	}

	/**
	 * 获取本方债券清算信息
	 * @return
	 */
	public SecurSettBean getSettInfo() {
		if(null == settInfo){
			settInfo = new SecurSettBean();
		}
		return settInfo;
	}

	/**
	 * 设置本方债券清算信息
	 * @param settInfo
	 */
	public void setSettInfo(SecurSettBean settInfo) {
		this.settInfo = settInfo;
	}
	
	/**
	 * 获取对手方债券清算信息
	 * @return
	 */
	public SecurSettBean getContraPartySettInfo() {
		if(null == contraPartySettInfo){
			contraPartySettInfo = new SecurSettBean();
		}
		return contraPartySettInfo;
	}

	/**
	 * 设置对手方债券清算信息
	 * @param contraPartySettInfo
	 */
	public void setContraPartySettInfo(SecurSettBean contraPartySettInfo) {
		this.contraPartySettInfo = contraPartySettInfo;
	}

	public String getSettlDate() {
		return settlDate;
	}

	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}

	public BigDecimal getSettlCurrAmt() {
		return settlCurrAmt;
	}

	public void setSettlCurrAmt(BigDecimal settlCurrAmt) {
		this.settlCurrAmt = settlCurrAmt;
	}

	public BigDecimal getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(BigDecimal tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getSettlCurrency() {
		return settlCurrency;
	}

	public void setSettlCurrency(String settlCurrency) {
		this.settlCurrency = settlCurrency;
	}

	public BigDecimal getAccruedIntAmt() {
		return accruedIntAmt;
	}

	public void setAccruedIntAmt(BigDecimal accruedIntAmt) {
		this.accruedIntAmt = accruedIntAmt;
	}

	public Date getTransactTime() {
		return transactTime;
	}

	public void setTransactTime(Date transactTime) {
		this.transactTime = transactTime;
	}
}
