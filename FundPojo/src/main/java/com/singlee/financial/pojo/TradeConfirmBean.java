package com.singlee.financial.pojo;

import java.io.Serializable;

public class TradeConfirmBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  dealNo                   ;
	private String  tradeType;
	private String  netGrossInd;
	private String  marketIndicator;
	private String  execId                   ;
	private TradeSettlsBean tradeSettlsBean;
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public String getNetGrossInd() {
		return netGrossInd;
	}
	public void setNetGrossInd(String netGrossInd) {
		this.netGrossInd = netGrossInd;
	}
	public String getMarketIndicator() {
		return marketIndicator;
	}
	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}
	public String getExecId() {
		return execId;
	}
	public void setExecId(String execId) {
		this.execId = execId;
	}
	public TradeSettlsBean getTradeSettlsBean() {
		return tradeSettlsBean;
	}
	public void setTradeSettlsBean(TradeSettlsBean tradeSettlsBean) {
		this.tradeSettlsBean = tradeSettlsBean;
	}
	
	
}
