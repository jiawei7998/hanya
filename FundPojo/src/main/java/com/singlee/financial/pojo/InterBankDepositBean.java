package com.singlee.financial.pojo;

import com.singlee.financial.pojo.component.PayFrequency;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/****
 * 
 * 同业存款
 * @author lee
 *
 */
public class InterBankDepositBean extends TradeBaseBean {
	
	/**
    *
    */
   private static final long serialVersionUID = 1L;
   
   //53 - 同业存款
   private String marketIndicator;
   
   //交易品种代码
   private String securityId;
   
   // 期限
   private String tenor;
   
   //实际占款天数
   private String holdingDays;

   //交易币种
   private String ccy;
   
   //利率
   private BigDecimal rate;

   //交易币种金额
   private BigDecimal amt;
   
   //收付息频率
   private PayFrequency payFrequency;
    //收付息频率 Cfets
    private int couponPaymentFrequency;
   
   //首次付息日
   private String firstPayDate;

    //首次结息日
    private String firstSettleDate;

    //结息日约定
    private String settlDayAgreement;

    //付息日约定
    private String payDayAgreement;
   
   //应计利息总金额
   private BigDecimal interestAmt;
   
   //到期还款金额
   private BigDecimal maturityAmt;
   
   //首次结算日
   private Date settlDate;
   
   //到期日
   private Date maturityDate;
   
   //提前支取约定 1不允许提前支取 2允许全额 3 允许全额或部分
   private String advanceWithDrawAgreement;
   
   private String text;
   
   //存出方经办行
   private String managingEntity2;
   
   //存入方经办行
   private String managingEntity1;
   
   //计息基础
   private String basis;

   //本方清算信息
   private SettBaseBean settInfo;

   //对手方清算信息
   private SettBaseBean contraPartySettInfo;

   //数据类型标识
   private Integer dataCategoryIndicator;

   //付息现金流
   private List<InterestCashFlowBean> icfbList;
   
   //提前支取预期利率
   private BigDecimal expYield;

   public BigDecimal getExpYield() {
	return expYield;
   }
	
	public void setExpYield(BigDecimal expYield) {
		this.expYield = expYield;
	}

    public List<InterestCashFlowBean> getIcfbList() {
        return icfbList;
    }

    public void setIcfbList(List<InterestCashFlowBean> icfbList) {
        this.icfbList = icfbList;
    }

    public String getFirstSettleDate() {
        return firstSettleDate;
    }

    public void setFirstSettleDate(String firstSettleDate) {
        this.firstSettleDate = firstSettleDate;
    }

    public String getSettlDayAgreement() {
        return settlDayAgreement;
    }

    public void setSettlDayAgreement(String settlDayAgreement) {
        this.settlDayAgreement = settlDayAgreement;
    }

    public String getPayDayAgreement() {
        return payDayAgreement;
    }

    public void setPayDayAgreement(String payDayAgreement) {
        this.payDayAgreement = payDayAgreement;
    }

    public String getTenor() {
       return tenor;
    }

    public void setTenor(String tenor) {
       this.tenor = tenor;
    }

    public String getHoldingDays() {
       return holdingDays;
    }

   public void setHoldingDays(String holdingDays) {
       this.holdingDays = holdingDays;
   }

   /**
    * 获取交易币种
    */
   public String getCcy() {
       return ccy;
   }

   /**
    * 获取交易币种
    *
    * @param ccy
    */
   public void setCcy(String ccy) {
       this.ccy = ccy;
   }

   /**
    * 获取交易币种金额
    *
    * @return
    */
   public BigDecimal getAmt() {
       return amt;
   }

   /**
    * 设置交易币种金额
    *
    * @param amt
    */
   public void setAmt(BigDecimal amt) {
       this.amt = amt;
   }

   /**
    * 获取利率
    *
    * @return
    */
   public BigDecimal getRate() {
       return rate;
   }

   /**
    * 设置利率
    *
    * @param rate
    */
   public void setRate(BigDecimal rate) {
       this.rate = rate;
   }

   /**
    * 获取到期日
    *
    * @return
    */
   public Date getMaturityDate() {
       return maturityDate;
   }

   /**
    * 设置到期日
    *
    * @param maturityDate
    */
   public void setMaturityDate(Date maturityDate) {
       this.maturityDate = maturityDate;
   }

   /**
    * 获取应计利息总金额
    *
    * @return
    */
   public BigDecimal getInterestAmt() {
       return interestAmt;
   }

   /**
    * 设置应计利息总金额
    *
    * @param interestAmt
    */
   public void setInterestAmt(BigDecimal interestAmt) {
       this.interestAmt = interestAmt;
   }

   /**
    * 获取计息基础
    *
    * @return
    */
   public String getBasis() {
       return basis;
   }

   /**
    * 设置计息基础
    *
    * @param basis
    */
   public void setBasis(String basis) {
       this.basis = basis;
   }

   /**
    * 获取到期还款金额
    *
    * @return
    */
   public BigDecimal getMaturityAmt() {
       return maturityAmt;
   }

   /**
    * 设置到期还款金额
    *
    * @param maturityAmt
    */
   public void setMaturityAmt(BigDecimal maturityAmt) {
       this.maturityAmt = maturityAmt;
   }

   /**
    * 获取本方清算信息
    *
    * @return
    */
	public SettBaseBean getSettInfo() {
		if (null == settInfo) {
			settInfo = new SettBaseBean();
		}
		return settInfo;
	}

   /**
    * 设置本方清算信息
    *
    * @param settInfo
    */
   public void setSettInfo(SettBaseBean settInfo) {
       this.settInfo = settInfo;
   }

   /**
    * 获取对手方清算信息
    *
    * @return
    */
	public SettBaseBean getContraPartySettInfo() {
		if (null == contraPartySettInfo) {
			contraPartySettInfo = new SettBaseBean();
		}
		return contraPartySettInfo;
	}

   /**
    * 设置对手方清算信息
    *
    * @param contraPartySettInfo
    */
   public void setContraPartySettInfo(SettBaseBean contraPartySettInfo) {
       this.contraPartySettInfo = contraPartySettInfo;
   }

   /**
    * 获取收付息频率
    *
    * @return
    */
   public PayFrequency getPayFrequency() {
       return payFrequency;
   }

   /**
    * 设置收付息频率
    *
    * @param payFrequency
    */
   public void setPayFrequency(PayFrequency payFrequency) {
       this.payFrequency = payFrequency;
   }

	public String getMarketIndicator() {
		return marketIndicator;
	}
	
	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}
	
	public String getSecurityId() {
		return securityId;
	}
	
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}
	
	public String getFirstPayDate() {
		return firstPayDate;
	}
	
	public void setFirstPayDate(String firstPayDate) {
		this.firstPayDate = firstPayDate;
	}
	
	public Date getSettlDate() {
		return settlDate;
	}
	
	public void setSettlDate(Date settlDate) {
		this.settlDate = settlDate;
	}
	
	public String getAdvanceWithDrawAgreement() {
		return advanceWithDrawAgreement;
	}
	
	public void setAdvanceWithDrawAgreement(String advanceWithDrawAgreement) {
		this.advanceWithDrawAgreement = advanceWithDrawAgreement;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getManagingEntity2() {
		return managingEntity2;
	}
	
	public void setManagingEntity2(String managingEntity2) {
		this.managingEntity2 = managingEntity2;
	}
	
	public String getManagingEntity1() {
		return managingEntity1;
	}
	
	public void setManagingEntity1(String managingEntity1) {
		this.managingEntity1 = managingEntity1;
	}

	public Integer getDataCategoryIndicator() {
		return dataCategoryIndicator;
	}

	public void setDataCategoryIndicator(Integer dataCategoryIndicator) {
		this.dataCategoryIndicator = dataCategoryIndicator;
	}

    public int getCouponPaymentFrequency() {
        return couponPaymentFrequency;
    }

    public void setCouponPaymentFrequency(int couponPaymentFrequency) {
        this.couponPaymentFrequency = couponPaymentFrequency;
    }
}
