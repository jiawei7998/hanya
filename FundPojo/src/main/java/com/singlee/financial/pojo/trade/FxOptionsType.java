package com.singlee.financial.pojo.trade;

public class FxOptionsType {
	public enum DealType {
		/**
		 * 期权成交时
		 */
		Valid(1),

		/**
		 * 期权到期正常行权时
		 */
		Exercised(2),

		/**
		 * 期权到期放弃行权时
		 */
		Expired(3);

		private int value;

		private DealType(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}
	
	public enum CurrencyType {
		/**
		 * 币种1
		 */
		Ccy(1),

		/**
		 * 币种2
		 */
		Ctrccy(2);

		private int value;

		private CurrencyType(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}
}
