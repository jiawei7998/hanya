package com.singlee.financial.pojo.component;

/**
 * 回购方式模式
 * @author chenxh
 *
 */
public enum RepoMethodEnum {

	/**
	 * 双边回购
	 */
	Enquiry,
	
	/**
	 * 三方回购
	 */
	Competition,
	
	/**
	 * 通用回购
	 */
	Married
}
