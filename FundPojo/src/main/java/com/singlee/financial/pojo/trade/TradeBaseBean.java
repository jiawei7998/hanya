package com.singlee.financial.pojo.trade;

import com.singlee.financial.pojo.component.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 资金交易业务父对象
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class TradeBaseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4132722822437896184L;

	// 成交编号
	private String execId;

	// 交易成交时间
	private Date dealDateTime;

	// 交易日期
	private Date dealDate;

	// 起息日 当交易为现券买卖时为交割日期
	private Date valueDate;

	// 买卖方向
	private PsEnum ps;

	// 交易时间
	private String tradeTime;

	// 本方机构信息
	private InstitutionBean institutionInfo;

	// 对手方机构信息
	private InstitutionBean contraPatryInstitutionInfo;

	// 交易处理状态
	private TransactionStatusEnum dealStatus;

	// 交易成交模式
	private DealModeEnum dealMode;

	// 交易清算模式
	private SettMode settMode;

	// 品种代码
	private String kindId;

	// 交易方式
	private TradeMethodEnum TradeMethod;
	
	//交易模式
	private TradeInstrumentEnum TradeInstrument;
	//结算方式：PVP才有值
	private String deliveryType;

	private ExecStatus status;
	//Cfets
	private String execType;
	
	//交易确认标识
	private String cfetsCnfmIndicator;

	//数据类型标识
	private DataCategoryIndicatorEnum dcie;

	//报文类型
	private String msgType;

	/**
	 * 将CFETS ExecID转换为15位的流水号
	 * 
	 * @return
	 */
	public String getExecIdToFedealno() {
		return this.execId.isEmpty() ? "" : this.execId.replace("-", "");
	}

	/**
	 * 获取交易处理状态
	 * 
	 * @return
	 */
	public TransactionStatusEnum getDealStatus() {
		return dealStatus;
	}

	/**
	 * 设置交易处理状态
	 * 
	 * @param dealStatus
	 */
	public void setDealStatus(TransactionStatusEnum dealStatus) {
		this.dealStatus = dealStatus;
	}

	/**
	 * 获取交易成交模式
	 * 
	 * @return
	 */
	public DealModeEnum getDealMode() {
		return dealMode;
	}

	/**
	 * 设置交易成交模式
	 * 
	 * @param dealMode
	 */
	public void setDealMode(DealModeEnum dealMode) {
		this.dealMode = dealMode;
	}

	/**
	 * 获取交易清算模式
	 * 
	 * @return
	 */
	public SettMode getSettMode() {
		return settMode;
	}

	/**
	 * 设置交易清算模式
	 * 
	 * @param settMode
	 */
	public void setSettMode(SettMode settMode) {
		this.settMode = settMode;
	}

	/**
	 * 获取本方机构信息
	 * 
	 * @return
	 */
	public InstitutionBean getInstitutionInfo() {
		if (null == this.institutionInfo) {
			this.institutionInfo = new InstitutionBean();
		}
		return institutionInfo;
	}

	/**
	 * 设置本方机构信息
	 * 
	 * @param institutionInfo
	 */
	public void setInstitutionInfo(InstitutionBean institutionInfo) {
		this.institutionInfo = institutionInfo;
	}

	/**
	 * 获取对手方机构信息
	 * 
	 * @return
	 */
	public InstitutionBean getContraPatryInstitutionInfo() {
		if (null == this.contraPatryInstitutionInfo) {
			this.contraPatryInstitutionInfo = new InstitutionBean();
		}
		return contraPatryInstitutionInfo;
	}

	/**
	 * 设置对手方机构信息
	 * 
	 * @param contraPatryInstitutionInfo
	 */
	public void setContraPatryInstitutionInfo(InstitutionBean contraPatryInstitutionInfo) {
		this.contraPatryInstitutionInfo = contraPatryInstitutionInfo;
	}

	/**
	 * 获取交易成交编号
	 * 
	 * @return
	 */
	public String getExecId() {
		return execId;
	}

	/**
	 * 设置交易成交编号
	 * 
	 * @param execId
	 */
	public void setExecId(String execId) {
		this.execId = execId;
	}

	/**
	 * 获取交易成交日期
	 * 
	 * @return
	 */
	public Date getDealDateTime() {
		return dealDateTime;
	}

	/**
	 * 设置交易成交日期
	 * 
	 * @param dealDateTime
	 */
	public void setDealDateTime(Date dealDateTime) {
		this.dealDateTime = dealDateTime;
	}

	/**
	 * 获取交易日
	 * 
	 * @return
	 */
	public Date getDealDate() {
		return dealDate;
	}

	/**
	 * 设置交易日
	 * 
	 * @param dealDate
	 */
	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	/**
	 * 获取起息日期
	 * 
	 * @return
	 */
	public Date getValueDate() {
		return valueDate;
	}

	/**
	 * 设置起息日期
	 * 
	 * @param valueDate
	 */
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	/**
	 * 获取买卖方向
	 * 
	 * @return
	 */
	public PsEnum getPs() {
		return ps;
	}

	/**
	 * 设置买卖方向
	 * 
	 * @param ps
	 */
	public void setPs(PsEnum ps) {
		this.ps = ps;
	}

	/**
	 * 获取交易成交时间
	 * 
	 * @return
	 */
	public String getTradeTime() {
		return tradeTime;
	}

	/**
	 * 设置交易成交时间
	 * 
	 * @param tradeTime
	 */
	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TradeBaseBean [contraPatryInstitutionInfo=");
		builder.append(contraPatryInstitutionInfo);
		builder.append(", dealDate=");
		builder.append(dealDate);
		builder.append(", dealDateTime=");
		builder.append(dealDateTime);
		builder.append(", execId=");
		builder.append(execId);
		builder.append(", institutionInfo=");
		builder.append(institutionInfo);
		builder.append(", ps=");
		builder.append(ps);
		builder.append(", tradeTime=");
		builder.append(tradeTime);
		builder.append(", valueDate=");
		builder.append(valueDate);
		builder.append("]");
		return builder.toString();
	}

	public void setKindId(String kindId) {
		this.kindId = kindId;
	}

	public String getKindId() {
		return kindId;
	}

	public void setTradeMethod(TradeMethodEnum tradeMethod) {
		TradeMethod = tradeMethod;
	}

	public TradeMethodEnum getTradeMethod() {
		return TradeMethod;
	}
	
	public TradeInstrumentEnum getTradeInstrument() {
		return TradeInstrument;
	}

	public void setTradeInstrument(TradeInstrumentEnum tradeInstrument) {
		TradeInstrument = tradeInstrument;
	}
	
	public String getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	
	public ExecStatus getStatus() {
		return status;
	}

	public void setStatus(ExecStatus status) {
		this.status = status;
	}

	public String getCfetsCnfmIndicator() {
		return cfetsCnfmIndicator;
	}

	public void setCfetsCnfmIndicator(String cfetsCnfmIndicator) {
		this.cfetsCnfmIndicator = cfetsCnfmIndicator;
	}

	public DataCategoryIndicatorEnum getDcie() {
		return dcie;
	}

	public void setDcie(DataCategoryIndicatorEnum dcie) {
		this.dcie = dcie;
	}

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
}
