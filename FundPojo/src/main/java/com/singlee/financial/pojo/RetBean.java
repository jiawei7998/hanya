package com.singlee.financial.pojo;

import com.singlee.financial.pojo.component.RetStatusEnum;

import java.io.Serializable;

/**
 * 报文发送方法返回实体父类
 * 
 * @author chenxh
 * 
 */
public class RetBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5214334334567177010L;

	/**
	 * 返回状态
	 */
	private RetStatusEnum retStatus;

	/**
	 * 返回代码
	 */
	private String retCode;

	/**
	 * 返回描述信息
	 */
	private String retMsg;
	
	/**
	 * 返回错误代码
	 */
	private String errorCode;
	
	/**
	 * 返回错误信息
	 */
	private String errorMsg;
	

	/**
	 * 构造方法.已重载,无参方法
	 */
	public RetBean() {
		super();
	}

	/**
	 * 构造方法.已重载,有参方法
	 * 
	 * @param retStatus
	 *            处理状态
	 * @param retCode
	 *            处理状态代码
	 * @param retMsg
	 *            处理返回描述信息
	 */
	public RetBean(RetStatusEnum retStatus, String retCode, String retMsg) {
		super();
		this.retStatus = retStatus;
		this.retCode = retCode;
		this.retMsg = retMsg;
	}

	public RetStatusEnum getRetStatus() {
		return retStatus;
	}

	public void setRetStatus(RetStatusEnum retStatus) {
		this.retStatus = retStatus;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		super.toString();
		StringBuilder builder = new StringBuilder();
		builder.append("PacketRetBean [retCode=");
		builder.append(retCode);
		builder.append(", retMsg=");
		builder.append(retMsg);
		builder.append(", retStatus=");
		builder.append(retStatus);
		builder.append(", errorCode=");
		builder.append(errorCode);
		builder.append(", errorMsg=");
		builder.append(errorMsg);
		builder.append("]");
		return builder.toString();
	}

}
