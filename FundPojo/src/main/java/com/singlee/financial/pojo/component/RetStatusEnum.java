package com.singlee.financial.pojo.component;

/**
 * ESB返回状态枚举
 * @author chenxh
 *
 */
public enum RetStatusEnum {

	/**
	 * 处理成功SUCCESS
	 */
	S,
	/**
	 * 处理失败FAILURE
	 */
	F
}
