package com.singlee.financial.pojo;

import com.singlee.financial.pojo.trade.RepoBaseBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;

/**
 * 买断式回购业务实体对象
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class RepoOrBean extends RepoBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6388560026914639394L;

	// 债券信息
	private SecurityInfoBean securityInfo;

	// 首期债券交易金额
	private SecurityAmtDetailBean comSecurityAmt;

	// 到期债券交易金额
	private SecurityAmtDetailBean matSecurityAmt;

	//有无补充条款
	private boolean remarkIndicator;
	/**
	 * 获取债券信息
	 * 
	 * @return
	 */
	public SecurityInfoBean getSecurityInfo() {
		if (null == securityInfo) {
			securityInfo = new SecurityInfoBean();
		}
		return securityInfo;
	}

	/**
	 * 设置债券信息
	 * 
	 * @param securityInfo
	 */
	public void setSecurityInfo(SecurityInfoBean securityInfo) {
		this.securityInfo = securityInfo;
	}

	/**
	 * 获取首期债券交易金额
	 * 
	 * @return
	 */
	public SecurityAmtDetailBean getComSecurityAmt() {
		if (null == comSecurityAmt) {
			comSecurityAmt = new SecurityAmtDetailBean();
		}
		return comSecurityAmt;
	}

	/**
	 * 设置首期债券交易金额
	 * 
	 * @param comSecurityAmt
	 */
	public void setComSecurityAmt(SecurityAmtDetailBean comSecurityAmt) {
		this.comSecurityAmt = comSecurityAmt;
	}

	/**
	 * 获取债券到期交易金额
	 * 
	 * @return
	 */
	public SecurityAmtDetailBean getMatSecurityAmt() {
		if (null == matSecurityAmt) {
			matSecurityAmt = new SecurityAmtDetailBean();
		}
		return matSecurityAmt;
	}

	/**
	 * 设置债券到期交易金额
	 * 
	 * @param matSecurityAmt
	 */
	public void setMatSecurityAmt(SecurityAmtDetailBean matSecurityAmt) {
		this.matSecurityAmt = matSecurityAmt;
	}

	public boolean isRemarkIndicator() {
		return remarkIndicator;
	}

	public void setRemarkIndicator(boolean remarkIndicator) {
		this.remarkIndicator = remarkIndicator;
	}
}
