package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 常备借贷便利质押券组件
 * @author
 * @version 
 * @since 
 */
public class SLFSettAmtBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2122722633568070359L;

  
    //质押券代码
    private String underlyingSecurityID;
    //质押券券面金额
    private BigDecimal underlyingQty;
    //折算率
    private BigDecimal underlyingStipType;
    
    
  
    public String getUnderlyingSecurityID() {
        return underlyingSecurityID;
    }
    public void setUnderlyingSecurityID(String underlyingSecurityID) {
        this.underlyingSecurityID = underlyingSecurityID;
    }
    public BigDecimal getUnderlyingQty() {
        return underlyingQty;
    }
    public void setUnderlyingQty(BigDecimal underlyingQty) {
        this.underlyingQty = underlyingQty;
    }
    public BigDecimal getUnderlyingStipType() {
        return underlyingStipType;
    }
    public void setUnderlyingStipType(BigDecimal underlyingStipType) {
        this.underlyingStipType = underlyingStipType;
    }
    public String getUnderlyingStipValue() {
        return underlyingStipValue;
    }
    public void setUnderlyingStipValue(String underlyingStipValue) {
        this.underlyingStipValue = underlyingStipValue;
    }
    //单位
    private String underlyingStipValue;



    @Override
    public String toString() {
        return "SLFSettAmtBean [underlyingSecurityID=" + underlyingSecurityID + ", underlyingQty=" + underlyingQty
                + ", underlyingStipType=" + underlyingStipType + ", underlyingStipValue=" + underlyingStipValue + "]";
    }
 
    

    
    
    
    
    
}
