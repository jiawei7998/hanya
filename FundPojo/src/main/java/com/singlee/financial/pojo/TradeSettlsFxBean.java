package com.singlee.financial.pojo;

import java.io.Serializable;

public class TradeSettlsFxBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  execId                   ;
	private String  institutionId            ;
	private String  tradeId                  ;
	private String  tradeName                ;
	private String  shortName                ;
	private String  fullName                 ;
	private String  markerTakerTole          ;
	private String  settccy                  ;
	private String  bankname                 ;
	private String  bankopenno               ;
	private String  bankacctno               ;
	private String  acctname                 ;
	private String  acctopenno               ;
	private String  acctno                   ;
	private String  intermediarybankname     ;
	private String  intermediarybankbiccode  ;
	private String  intermediarybankacctno   ;
	private String  remark                   ;
	private String  farSettCcy;
	private String  farBankname               ; 
	private String  farBankopenno             ; 
	private String  farBankacctno             ; 
	private String  farAcctname               ; 
	private String  farAcctopenno             ; 
	private String  farAcctno                 ; 
	private String  farIntermediarybankname   ; 
	private String  farIntermediarybankbiccode; 
	private String  farIntermediarybankacctno ; 
	private String  farRemark                 ; 

	private String  cInstitutionId           ;
	private String  cIradeId                 ;
	private String  cIradeName               ;
	private String  cShortName               ;
	private String  cFullName                ;
	private String  cMarkerTakerTole         ;
	private String  cSettccy                 ;
	private String  cBankname                 ;
	private String  cBankopenno               ;
	private String  cBankacctno               ;
	private String  cAcctname                 ;
	private String  cAcctopenno               ;
	private String  cAcctno                   ;
	private String  cIntermediarybankname    ;
	private String  cIntermediarybankbiccode ;
	private String  cIntermediarybankacctno  ;
	private String  cRemark                  ;
	private String  cFarSettCcy;
	private String  cFarBankname               ;
	private String  cFarBankopenno             ;
	private String  cFarBankacctno             ;
	private String  cFarAcctname               ;
	private String  cFarAcctopenno             ;
	private String  cFarAcctno                 ;
	private String  cFarIntermediarybankname   ;
	private String  cFarIntermediarybankbiccode;
	private String  cFarIntermediarybankacctno ;
	private String  cFarRemark                 ;
	
	public String getExecId() {
		return execId;
	}
	public void setExecId(String execId) {
		this.execId = execId;
	}
	public String getInstitutionId() {
		return institutionId;
	}
	public void setInstitutionId(String institutionId) {
		this.institutionId = institutionId;
	}
	public String getTradeId() {
		return tradeId;
	}
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getMarkerTakerTole() {
		return markerTakerTole;
	}
	public void setMarkerTakerTole(String markerTakerTole) {
		this.markerTakerTole = markerTakerTole;
	}
	public String getSettccy() {
		return settccy;
	}
	public void setSettccy(String settccy) {
		this.settccy = settccy;
	}
	public String getBankopenno() {
		return bankopenno;
	}
	public void setBankopenno(String bankopenno) {
		this.bankopenno = bankopenno;
	}
	public String getAcctopenno() {
		return acctopenno;
	}
	public void setAcctopenno(String acctopenno) {
		this.acctopenno = acctopenno;
	}
	public String getIntermediarybankname() {
		return intermediarybankname;
	}
	public void setIntermediarybankname(String intermediarybankname) {
		this.intermediarybankname = intermediarybankname;
	}
	public String getIntermediarybankbiccode() {
		return intermediarybankbiccode;
	}
	public void setIntermediarybankbiccode(String intermediarybankbiccode) {
		this.intermediarybankbiccode = intermediarybankbiccode;
	}
	public String getIntermediarybankacctno() {
		return intermediarybankacctno;
	}
	public void setIntermediarybankacctno(String intermediarybankacctno) {
		this.intermediarybankacctno = intermediarybankacctno;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getcInstitutionId() {
		return cInstitutionId;
	}
	public void setcInstitutionId(String cInstitutionId) {
		this.cInstitutionId = cInstitutionId;
	}
	public String getcIradeId() {
		return cIradeId;
	}
	public void setcIradeId(String cIradeId) {
		this.cIradeId = cIradeId;
	}
	public String getcIradeName() {
		return cIradeName;
	}
	public void setcIradeName(String cIradeName) {
		this.cIradeName = cIradeName;
	}
	public String getcShortName() {
		return cShortName;
	}
	public void setcShortName(String cShortName) {
		this.cShortName = cShortName;
	}
	public String getcFullName() {
		return cFullName;
	}
	public void setcFullName(String cFullName) {
		this.cFullName = cFullName;
	}
	public String getcMarkerTakerTole() {
		return cMarkerTakerTole;
	}
	public void setcMarkerTakerTole(String cMarkerTakerTole) {
		this.cMarkerTakerTole = cMarkerTakerTole;
	}
	public String getcSettccy() {
		return cSettccy;
	}
	public void setcSettccy(String cSettccy) {
		this.cSettccy = cSettccy;
	}
	public String getcBankopenno() {
		return cBankopenno;
	}
	public void setcBankopenno(String cBankopenno) {
		this.cBankopenno = cBankopenno;
	}
	public String getcAcctopenno() {
		return cAcctopenno;
	}
	public void setcAcctopenno(String cAcctopenno) {
		this.cAcctopenno = cAcctopenno;
	}
	public String getcIntermediarybankname() {
		return cIntermediarybankname;
	}
	public void setcIntermediarybankname(String cIntermediarybankname) {
		this.cIntermediarybankname = cIntermediarybankname;
	}
	public String getcIntermediarybankbiccode() {
		return cIntermediarybankbiccode;
	}
	public void setcIntermediarybankbiccode(String cIntermediarybankbiccode) {
		this.cIntermediarybankbiccode = cIntermediarybankbiccode;
	}
	public String getcIntermediarybankacctno() {
		return cIntermediarybankacctno;
	}
	public void setcIntermediarybankacctno(String cIntermediarybankacctno) {
		this.cIntermediarybankacctno = cIntermediarybankacctno;
	}
	public String getcRemark() {
		return cRemark;
	}
	public void setcRemark(String cRemark) {
		this.cRemark = cRemark;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getBankacctno() {
		return bankacctno;
	}
	public void setBankacctno(String bankacctno) {
		this.bankacctno = bankacctno;
	}
	public String getAcctname() {
		return acctname;
	}
	public void setAcctname(String acctname) {
		this.acctname = acctname;
	}
	public String getAcctno() {
		return acctno;
	}
	public void setAcctno(String acctno) {
		this.acctno = acctno;
	}
	public String getcBankname() {
		return cBankname;
	}
	public void setcBankname(String cBankname) {
		this.cBankname = cBankname;
	}
	public String getcBankacctno() {
		return cBankacctno;
	}
	public void setcBankacctno(String cBankacctno) {
		this.cBankacctno = cBankacctno;
	}
	public String getcAcctname() {
		return cAcctname;
	}
	public void setcAcctname(String cAcctname) {
		this.cAcctname = cAcctname;
	}
	public String getcAcctno() {
		return cAcctno;
	}
	public void setcAcctno(String cAcctno) {
		this.cAcctno = cAcctno;
	}
	public String getFarBankname() {
		return farBankname;
	}
	public void setFarBankname(String farBankname) {
		this.farBankname = farBankname;
	}
	public String getFarBankopenno() {
		return farBankopenno;
	}
	public void setFarBankopenno(String farBankopenno) {
		this.farBankopenno = farBankopenno;
	}
	public String getFarBankacctno() {
		return farBankacctno;
	}
	public void setFarBankacctno(String farBankacctno) {
		this.farBankacctno = farBankacctno;
	}
	public String getFarAcctname() {
		return farAcctname;
	}
	public void setFarAcctname(String farAcctname) {
		this.farAcctname = farAcctname;
	}
	public String getFarAcctopenno() {
		return farAcctopenno;
	}
	public void setFarAcctopenno(String farAcctopenno) {
		this.farAcctopenno = farAcctopenno;
	}
	public String getFarAcctno() {
		return farAcctno;
	}
	public void setFarAcctno(String farAcctno) {
		this.farAcctno = farAcctno;
	}
	public String getFarIntermediarybankname() {
		return farIntermediarybankname;
	}
	public void setFarIntermediarybankname(String farIntermediarybankname) {
		this.farIntermediarybankname = farIntermediarybankname;
	}
	public String getFarIntermediarybankbiccode() {
		return farIntermediarybankbiccode;
	}
	public void setFarIntermediarybankbiccode(String farIntermediarybankbiccode) {
		this.farIntermediarybankbiccode = farIntermediarybankbiccode;
	}
	public String getFarIntermediarybankacctno() {
		return farIntermediarybankacctno;
	}
	public void setFarIntermediarybankacctno(String farIntermediarybankacctno) {
		this.farIntermediarybankacctno = farIntermediarybankacctno;
	}
	public String getFarRemark() {
		return farRemark;
	}
	public void setFarRemark(String farRemark) {
		this.farRemark = farRemark;
	}
	public String getcFarBankname() {
		return cFarBankname;
	}
	public void setcFarBankname(String cFarBankname) {
		this.cFarBankname = cFarBankname;
	}
	public String getcFarBankopenno() {
		return cFarBankopenno;
	}
	public void setcFarBankopenno(String cFarBankopenno) {
		this.cFarBankopenno = cFarBankopenno;
	}
	public String getcFarBankacctno() {
		return cFarBankacctno;
	}
	public void setcFarBankacctno(String cFarBankacctno) {
		this.cFarBankacctno = cFarBankacctno;
	}
	public String getcFarAcctname() {
		return cFarAcctname;
	}
	public void setcFarAcctname(String cFarAcctname) {
		this.cFarAcctname = cFarAcctname;
	}
	public String getcFarAcctopenno() {
		return cFarAcctopenno;
	}
	public void setcFarAcctopenno(String cFarAcctopenno) {
		this.cFarAcctopenno = cFarAcctopenno;
	}
	public String getcFarAcctno() {
		return cFarAcctno;
	}
	public void setcFarAcctno(String cFarAcctno) {
		this.cFarAcctno = cFarAcctno;
	}
	public String getcFarIntermediarybankname() {
		return cFarIntermediarybankname;
	}
	public void setcFarIntermediarybankname(String cFarIntermediarybankname) {
		this.cFarIntermediarybankname = cFarIntermediarybankname;
	}
	public String getcFarIntermediarybankbiccode() {
		return cFarIntermediarybankbiccode;
	}
	public void setcFarIntermediarybankbiccode(String cFarIntermediarybankbiccode) {
		this.cFarIntermediarybankbiccode = cFarIntermediarybankbiccode;
	}
	public String getcFarIntermediarybankacctno() {
		return cFarIntermediarybankacctno;
	}
	public void setcFarIntermediarybankacctno(String cFarIntermediarybankacctno) {
		this.cFarIntermediarybankacctno = cFarIntermediarybankacctno;
	}
	public String getcFarRemark() {
		return cFarRemark;
	}
	public void setcFarRemark(String cFarRemark) {
		this.cFarRemark = cFarRemark;
	}
	public String getFarSettCcy() {
		return farSettCcy;
	}
	public void setFarSettCcy(String farSettCcy) {
		this.farSettCcy = farSettCcy;
	}
	public String getcFarSettCcy() {
		return cFarSettCcy;
	}
	public void setcFarSettCcy(String cFarSettCcy) {
		this.cFarSettCcy = cFarSettCcy;
	}


}
