package com.singlee.financial.pojo.trade;

import java.io.Serializable;


public class FxOptionCurrencyAmtBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String currency;// 币种
	private String principal;// 本金
	private String currencyAmtType;// 金额类型：1-call

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getCurrencyAmtType() {
		return currencyAmtType;
	}

	public void setCurrencyAmtType(String currencyAmtType) {
		this.currencyAmtType = currencyAmtType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CurrencyAmtBean [currency=");
		builder.append(currency);
		builder.append(", currencyAmtType=");
		builder.append(currencyAmtType);
		builder.append(", principal=");
		builder.append(principal);
		builder.append("]");
		return builder.toString();
	}
}
