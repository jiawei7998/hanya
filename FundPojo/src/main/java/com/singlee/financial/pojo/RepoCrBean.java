package com.singlee.financial.pojo;

import com.singlee.financial.pojo.component.RepoMethodEnum;
import com.singlee.financial.pojo.trade.*;

import java.math.BigDecimal;
import java.util.List;


/**
 * 质押式回购业务实体对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class RepoCrBean extends RepoBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2088067375693250727L;

	private SecurityAmtDetailBean amtDeail;

	//回购方式
	private RepoMethodEnum repoMethod;

	//首期交割日期,金额等信息
	private SecuritySettAmtBean comSecuritySettAmt;
	
	//到期交割日期,金额等信息
	private SecuritySettAmtBean matSecuritySettAmt;
	
	//券面总额合计
	private BigDecimal totalFaceAmt;
	
	//质押债券信息
	List<SecurityInfoBean> secursInfo;

	//跨托管机构信息
	List<SecurityCustodianBean> secursCustodian;

	public SecurityAmtDetailBean getAmtDeail() {
		if (null == amtDeail) {
			amtDeail = new SecurityAmtDetailBean();
		}
		return amtDeail;
	}

	public void setAmtDeail(SecurityAmtDetailBean amtDeail) {
		this.amtDeail = amtDeail;
	}

	public RepoMethodEnum getRepoMethod() {
		return repoMethod;
	}

	public void setRepoMethod(RepoMethodEnum repoMethod) {
		this.repoMethod = repoMethod;
	}

	/**
	 * 获取债券信息列表
	 * @return
	 */
	public List<SecurityInfoBean> getSecursInfo() {
		return secursInfo;
	}

	/**
	 * 设置债券信息列表
	 * @param secursInfo
	 */
	public void setSecursInfo(List<SecurityInfoBean> secursInfo) {
		this.secursInfo = secursInfo;
	}

	/**
	 * 获取首期交割日,结算金额等信息
	 * @return
	 */
	public SecuritySettAmtBean getComSecuritySettAmt() {
		if (null == comSecuritySettAmt) {
			comSecuritySettAmt = new SecuritySettAmtBean();
		}
		return comSecuritySettAmt;
	}

	/**
	 * 设置首期交割日,结算金额等信息
	 * @param comSecuritySettAmt
	 */
	public void setComSecuritySettAmt(SecuritySettAmtBean comSecuritySettAmt) {
		this.comSecuritySettAmt = comSecuritySettAmt;
	}

	/**
	 * 获取到期交割日,结算金额等信息
	 * @return
	 */
	public SecuritySettAmtBean getMatSecuritySettAmt() {
		if(null == matSecuritySettAmt){
			matSecuritySettAmt = new SecuritySettAmtBean();
		}
		return matSecuritySettAmt;
	}

	/**
	 * 设置到期交割日,结算金额等信息
	 * @param matSecuritySettAmt
	 */
	public void setMatSecuritySettAmt(SecuritySettAmtBean matSecuritySettAmt) {
		this.matSecuritySettAmt = matSecuritySettAmt;
	}

	/**
	 * 获取券面总额合计
	 * @return
	 */
	public BigDecimal getTotalFaceAmt() {
		return totalFaceAmt;
	}

	/**
	 * 设置券面总额合计
	 * @param totalFaceAmt
	 */
	public void setTotalFaceAmt(BigDecimal totalFaceAmt) {
		this.totalFaceAmt = totalFaceAmt;
	}


	public List<SecurityCustodianBean> getSecursCustodian() {
		return secursCustodian;
	}

	public void setSecursCustodian(List<SecurityCustodianBean> secursCustodian) {
		this.secursCustodian = secursCustodian;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RepoCrBean [comSecuritySettAmt=");
		builder.append(comSecuritySettAmt);
		builder.append(", matSecuritySettAmt=");
		builder.append(matSecuritySettAmt);
		builder.append(", secursInfo=");
		builder.append(secursInfo);
		builder.append(", getContraPartySettInfo()=");
		builder.append(getContraPartySettInfo());
		builder.append(", getRate()=");
		builder.append(getRate());
		builder.append(", getSettInfo()=");
		builder.append(getSettInfo());
		builder.append(", getContraPatryInstitutionInfo()=");
		builder.append(getContraPatryInstitutionInfo());
		builder.append(", getDealDate()=");
		builder.append(getDealDate());
		builder.append(", getDealDateTime()=");
		builder.append(getDealDateTime());
		builder.append(", getDealMode()=");
		builder.append(getDealMode());
		builder.append(", getDealStatus()=");
		builder.append(getDealStatus());
		builder.append(", getExecId()=");
		builder.append(getExecId());
		builder.append(", getExecIdToFedealno()=");
		builder.append(getExecIdToFedealno());
		builder.append(", getInstitutionInfo()=");
		builder.append(getInstitutionInfo());
		builder.append(", getPs()=");
		builder.append(getPs());
		builder.append(", getSettMode()=");
		builder.append(getSettMode());
		builder.append(", getTradeTime()=");
		builder.append(getTradeTime());
		builder.append(", getValueDate()=");
		builder.append(getValueDate());
		builder.append("]");
		return builder.toString();
	}

}
