package com.singlee.financial.pojo.component;

/**
 * 利率重置频率
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public enum ResetFrequency{

	/**
	 * Annually每年重定
	 */
	A,
	
	/**
	 * Semiannually每半年重定
	 */
	S,
	
	/**
	 * Quarter每季重定
	 */
	Q,
	
	/**
	 * Monthly每月重定
	 */
	M,
	
	/**
	 * Weekly每周重定
	 */
	W,
	
	/**
	 * 到期一次
	 */
	ONCE
	
}
