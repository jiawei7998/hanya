package com.singlee.financial.pojo.trade;

import java.io.Serializable;

/**
 * 机构信息,保存本方或者交易对手的机构ID、简称、全称、交易员等信息
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class InstitutionBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8572335166844227657L;

	// 机构ID
	private String institutionId;

	// 交易员ID
	private String tradeId;

	// 交易员名称
	private String tradeName;

	// 机构简称
	private String shortName;

	// 机构全称
	private String fullName;

	// 机构SWIFT CODE
	private String swiftCode;
	
	// 中文全称 交易后确认需要此字段
	private String fullNameCn;
	
	//联系人
	private String contactName;
	
	//联系电话
	private String contactPhone;
	
	//联系传真
	private String contactFax;

	//交易成员群组
	private String tradGroup;

	//地址
	private String address;

	/**
	 * 获取机构ID
	 * 
	 * @return
	 */
	public String getInstitutionId() {
		return institutionId;
	}

	/**
	 * 设置机构ID
	 * 
	 * @param institutionId
	 */
	public void setInstitutionId(String institutionId) {
		this.institutionId = institutionId;
	}

	/**
	 * 获取交易员ID
	 * 
	 * @return
	 */
	public String getTradeId() {
		return tradeId;
	}

	/**
	 * 设置交易员ID
	 * 
	 * @param tradeId
	 */
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}

	/**
	 * 获取交易员名称
	 * 
	 * @return
	 */
	public String getTradeName() {
		return tradeName;
	}

	/**
	 * 设置交易员名称
	 * 
	 * @param tradeName
	 */
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	/**
	 * 获取机构简称
	 * 
	 * @return
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * 设置机构简称
	 * 
	 * @param shortName
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * 获取机构全称
	 * 
	 * @return
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * 设置机构全称
	 * 
	 * @param fullName
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * 获取机构SWIFT CODE
	 * 
	 * @return
	 */
	public String getSwiftCode() {
		return swiftCode;
	}

	/**
	 * 设置机构SWIFT CODE
	 * 
	 * @param swiftCode
	 */
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getFullNameCn() {
		return fullNameCn;
	}

	public void setFullNameCn(String fullNameCn) {
		this.fullNameCn = fullNameCn;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactFax() {
		return contactFax;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	public String getTradGroup() {
		return tradGroup;
	}

	public void setTradGroup(String tradGroup) {
		this.tradGroup = tradGroup;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InstitutionBean [fullName=");
		builder.append(fullName);
		builder.append(", institutionId=");
		builder.append(institutionId);
		builder.append(", shortName=");
		builder.append(shortName);
		builder.append(", swiftCode=");
		builder.append(swiftCode);
		builder.append(", tradeId=");
		builder.append(tradeId);
		builder.append(", tradeName=");
		builder.append(tradeName);
		builder.append("]");
		return builder.toString();
	}

}
