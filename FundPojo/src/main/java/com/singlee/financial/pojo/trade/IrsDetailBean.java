package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 利率互换明细,包含利率、利差、首次支付日、支付周期、首次利率确认日、利率修正周期、计息方式、计息基础
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class IrsDetailBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6203127764249579748L;

	/**
	 * 1.固定利率支付  B.基准端
	 * */
	private String legSide;	
	
	/**
	 * L 左端 R右端
	 * */
	private String legSign;	
	
	/***
	 * 币种
	 */
	private String legCurrency;
	
	/***
	 * 是否询价关联端
	 */
	private boolean legSolveForIndicator;
	
	/***
	 * 名义本金
	 */
	private BigDecimal legQty;
	
	/***
	 * 残端
	 */
	private String legStub;
	
	/**
	 * 6.浮动值   3.固定值
	 * */
	private int legPriceType;
	
	/**参考利率  浮动利率时有值*/
	private String legCurveName;
	
	/**  固定利率 */
	private BigDecimal intRate;;

	/** 利差 */
	private double rateDiff; //
	
	/**参考利率  浮动利率时有值*/
	private String legCurveTenor;

	/**首次支付日*/
	private Date firstpaydate;

	/** 支付周期*/
	private String paycycle;
	
	/****
	 * 付息日调整规则 2-MF 下一工作日
	 * @return
	 */
	private String legCouponPaymentDateReset;

	/** 首次利率确认日*/
	private Date firstconfirmdate;

	/** 计息方式 */
	private String rateType;

	/**计息基准*/
	private String basis;
	
	/** 利率修正周期*/
	private String raterevcycle;
	
	/** 息率修正调整规则 定息日调整规则 0- 上一营业日*/
	private String raterevDateAdjust;
	
	/***
	 * 定息规则 0-v-0 1-v-1 2-v-2
	 */
	private String rateFixDateAdjustment;
	
	//自定义计息周期标识
	private String userPaymentIndicator; 
	
	//付息日期
	private List<IntPaymentPlan> intPaymentPlans;
	
	private String LegPs;

	public List<IntPaymentPlan> getIntPaymentPlans() {
		if(intPaymentPlans == null){
			intPaymentPlans = new ArrayList<IntPaymentPlan>();
		}
		return intPaymentPlans;
	}

	public void setIntPaymentPlans(List<IntPaymentPlan> intPaymentPlans) {
		this.intPaymentPlans = intPaymentPlans;
	}
	public String getLegSign() {
		return legSign;
	}

	public void setLegSign(String legSign) {
		this.legSign = legSign;
	}
	
	public String getLegPs() {
		return LegPs;
	}

	public void setLegPs(String legPs) {
		LegPs = legPs;
	}

	public String getLegCurrency() {
		return legCurrency;
	}

	public void setLegCurrency(String legCurrency) {
		this.legCurrency = legCurrency;
	}

	public boolean getLegSolveForIndicator() {
		return legSolveForIndicator;
	}

	public void setLegSolveForIndicator(boolean legSolveForIndicator) {
		this.legSolveForIndicator = legSolveForIndicator;
	}

	public BigDecimal getLegQty() {
		return legQty;
	}

	public void setLegQty(BigDecimal legQty) {
		this.legQty = legQty;
	}

	public String getLegStub() {
		return legStub;
	}

	public void setLegStub(String legStub) {
		this.legStub = legStub;
	}

	public String getLegCurveTenor() {
		return legCurveTenor;
	}

	public void setLegCurveTenor(String legCurveTenor) {
		this.legCurveTenor = legCurveTenor;
	}

	public String getLegCouponPaymentDateReset() {
		return legCouponPaymentDateReset;
	}

	public void setLegCouponPaymentDateReset(String legCouponPaymentDateReset) {
		this.legCouponPaymentDateReset = legCouponPaymentDateReset;
	}

	public String getRaterevDateAdjust() {
		return raterevDateAdjust;
	}

	public void setRaterevDateAdjust(String raterevDateAdjust) {
		this.raterevDateAdjust = raterevDateAdjust;
	}

	public String getLegSide() {
		return legSide;
	}

	public void setLegSide(String legSide) {
		this.legSide = legSide;
	}

	public int getLegPriceType() {
		return legPriceType;
	}

	public void setLegPriceType(int legPriceType) {
		this.legPriceType = legPriceType;
	}
	
	public String getLegCurveName() {
		return legCurveName;
	}

	public void setLegCurveName(String legCurveName) {
		this.legCurveName = legCurveName;
	}

	public BigDecimal getIntRate() {
		return intRate;
	}

	public void setIntRate(BigDecimal intRate) {
		this.intRate = intRate;
	}


	public double getRateDiff() {
		return rateDiff;
	}

	public void setRateDiff(double rateDiff) {
		this.rateDiff = rateDiff;
	}

	public Date getFirstpaydate() {
		return firstpaydate;
	}

	public void setFirstpaydate(Date firstpaydate) {
		this.firstpaydate = firstpaydate;
	}

	public String getPaycycle() {
		return paycycle;
	}

	public void setPaycycle(String paycycle) {
		this.paycycle = paycycle;
	}

	public Date getFirstconfirmdate() {
		return firstconfirmdate;
	}

	public void setFirstconfirmdate(Date firstconfirmdate) {
		this.firstconfirmdate = firstconfirmdate;
	}

	public String getRaterevcycle() {
		return raterevcycle;
	}

	public void setRaterevcycle(String raterevcycle) {
		this.raterevcycle = raterevcycle;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getBasis() {
		return basis;
	}

	public void setBasis(String basis) {
		this.basis = basis;
	}

	public String getUserPaymentIndicator() {
		return userPaymentIndicator;
	}

	public void setUserPaymentIndicator(String userPaymentIndicator) {
		this.userPaymentIndicator = userPaymentIndicator;
	}

	public String getRateFixDateAdjustment() {
		return rateFixDateAdjustment;
	}

	public void setRateFixDateAdjustment(String rateFixDateAdjustment) {
		this.rateFixDateAdjustment = rateFixDateAdjustment;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrsDetailBean [intRate=");
		builder.append(intRate);
		builder.append(", rateDiff=");
		builder.append(rateDiff);
		builder.append(", firstpaydate=");
		builder.append(firstpaydate);
		builder.append(", paycycle=");
		builder.append(paycycle);
		builder.append(", firstconfirmdate=");
		builder.append(firstconfirmdate);
		builder.append(", raterevcycle=");
		builder.append(raterevcycle);
		builder.append(", rateType");
		builder.append(rateType);
		builder.append(", basis");
		builder.append(basis);
		builder.append("]");
		return builder.toString();
	}

}
