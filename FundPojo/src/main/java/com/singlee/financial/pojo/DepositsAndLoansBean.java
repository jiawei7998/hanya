package com.singlee.financial.pojo;

import com.singlee.financial.pojo.component.PayFrequency;
import com.singlee.financial.pojo.component.ResetFrequency;
import com.singlee.financial.pojo.settlement.SettBaseBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 拆借业务实体对象,本、外币共用
 *
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6, Hessian4.0.37
 */
public class DepositsAndLoansBean extends TradeBaseBean {
    /**
     *
     */
    private static final long serialVersionUID = -3790282879572567955L;

    //交易币种
    private String ccy;

    //交易币种金额
    private BigDecimal amt;

    // 期限
    private String tenor;

    //利率
    private BigDecimal rate;

    //到期日
    private Date maturityDate;

    //应计利息总金额
    private BigDecimal interestAmt;

    //计息基础
    private String basis;

    //到期还款金额
    private BigDecimal maturityAmt;

    //本方清算信息
    private SettBaseBean settInfo;

    //对手方清算信息
    private SettBaseBean contraPartySettInfo;

    //收付息频率
    private PayFrequency payFrequency;

    //利率重置频率
    private ResetFrequency resetFrequency;

    //实际占款天数
    private String holdingDays;

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getHoldingDays() {
        return holdingDays;
    }

    public void setHoldingDays(String holdingDays) {
        this.holdingDays = holdingDays;
    }

    /**
     * 获取交易币种
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * 获取交易币种
     *
     * @param ccy
     */
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    /**
     * 获取交易币种金额
     *
     * @return
     */
    public BigDecimal getAmt() {
        return amt;
    }

    /**
     * 设置交易币种金额
     *
     * @param amt
     */
    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    /**
     * 获取利率
     *
     * @return
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * 设置利率
     *
     * @param rate
     */
    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    /**
     * 获取到期日
     *
     * @return
     */
    public Date getMaturityDate() {
        return maturityDate;
    }

    /**
     * 设置到期日
     *
     * @param maturityDate
     */
    public void setMaturityDate(Date maturityDate) {
        this.maturityDate = maturityDate;
    }

    /**
     * 获取应计利息总金额
     *
     * @return
     */
    public BigDecimal getInterestAmt() {
        return interestAmt;
    }

    /**
     * 设置应计利息总金额
     *
     * @param interestAmt
     */
    public void setInterestAmt(BigDecimal interestAmt) {
        this.interestAmt = interestAmt;
    }

    /**
     * 获取计息基础
     *
     * @return
     */
    public String getBasis() {
        return basis;
    }

    /**
     * 设置计息基础
     *
     * @param basis
     */
    public void setBasis(String basis) {
        this.basis = basis;
    }

    /**
     * 获取到期还款金额
     *
     * @return
     */
    public BigDecimal getMaturityAmt() {
        return maturityAmt;
    }

    /**
     * 设置到期还款金额
     *
     * @param maturityAmt
     */
    public void setMaturityAmt(BigDecimal maturityAmt) {
        this.maturityAmt = maturityAmt;
    }

    /**
     * 获取本方清算信息
     *
     * @return
     */
	public SettBaseBean getSettInfo() {
		if (null == settInfo) {
			settInfo = new SettBaseBean();
		}
		return settInfo;
	}

    /**
     * 设置本方清算信息
     *
     * @param settInfo
     */
    public void setSettInfo(SettBaseBean settInfo) {
        this.settInfo = settInfo;
    }

    /**
     * 获取对手方清算信息
     *
     * @return
     */
	public SettBaseBean getContraPartySettInfo() {
		if (null == contraPartySettInfo) {
			contraPartySettInfo = new SettBaseBean();
		}
		return contraPartySettInfo;
	}

    /**
     * 设置对手方清算信息
     *
     * @param contraPartySettInfo
     */
    public void setContraPartySettInfo(SettBaseBean contraPartySettInfo) {
        this.contraPartySettInfo = contraPartySettInfo;
    }

    /**
     * 获取收付息频率
     *
     * @return
     */
    public PayFrequency getPayFrequency() {
        return payFrequency;
    }

    /**
     * 设置收付息频率
     *
     * @param payFrequency
     */
    public void setPayFrequency(PayFrequency payFrequency) {
        this.payFrequency = payFrequency;
    }

    /**
     * 获取利率重置频率
     *
     * @return
     */
    public ResetFrequency getResetFrequency() {
        return resetFrequency;
    }

    public void setResetFrequency(ResetFrequency resetFrequency) {
        this.resetFrequency = resetFrequency;
    }

}
