package com.singlee.financial.pojo;


/****
 * 存单发行人缴款信息
 * 
 * @author lee
 *
 */
public class IbncdCommissionBean extends IbncdBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4799646628212957165L;

	//缴款状态
	private int paymentStatus;
	
	//发行人缴款信息
	private String headine;

	public int getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getHeadine() {
		return headine;
	}

	public void setHeadine(String headine) {
		this.headine = headine;
	}
	
}
