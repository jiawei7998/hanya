package com.singlee.financial.pojo.component;

/**
 * 本方/对手方枚举值
 * @author chenxh
 *
 */
public enum SettPartyEnum {

	/**
	 * 本方
	 */
	TheParty,
	
	/**
	 * 对手方
	 */
	CounterParty
	
}
