package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.financial.pojo.trade.SecuritySettAmtBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 债券借贷业务实体对象
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SecurityLendingBean extends TradeBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8998060796389052381L;

	// 借贷费率
	private BigDecimal lendingFeeRate;

	// 借贷费用（元）
	private BigDecimal lengingFeeCost;

	// 争议解决方式
	private String disputeWay;

	// 交易品种
	private String productName;

	// 标的债券信息
	private SecurityInfoBean securityInfo;

	// 债券借贷信息
	private SecurityAmtDetailBean securityAmtInfo;

	// 首期结算信息
	private SecuritySettAmtBean comSecurityAmt;

	// 到期结算信息
	private SecuritySettAmtBean matSecurityAmt;

	// 质押债券总额
	private BigDecimal maginTotalAmt;

	// 质押债券置换标识
	private boolean marginReplacement;

	// 质押债券list
	private List<SecurityInfoBean> lendingInfos;

	// 本方清算信息
	private SecurSettBean settInfo;

	// 对手方清算信息
	private SecurSettBean contraPartySettInfo;
	
	private String term;//下行数据期限接收字段
	
	private String occupancyDays;//下行数据实际占款天数字段

	public BigDecimal getLendingFeeRate() {
		return lendingFeeRate;
	}

	public void setLendingFeeRate(BigDecimal lendingFeeRate) {
		this.lendingFeeRate = lendingFeeRate;
	}

	public BigDecimal getLengingFeeCost() {
		return lengingFeeCost;
	}

	public void setLengingFeeCost(BigDecimal lengingFeeCost) {
		this.lengingFeeCost = lengingFeeCost;
	}

	public String getDisputeWay() {
		return disputeWay;
	}

	public void setDisputeWay(String disputeWay) {
		this.disputeWay = disputeWay;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getMaginTotalAmt() {
		return maginTotalAmt;
	}

	public void setMaginTotalAmt(BigDecimal maginTotalAmt) {
		this.maginTotalAmt = maginTotalAmt;
	}

	public boolean getMarginReplacement() {
		return marginReplacement;
	}

	public void setMarginReplacement(boolean marginReplacement) {
		this.marginReplacement = marginReplacement;
	}

	public SecurityInfoBean getSecurityInfo() {
		if (null == securityInfo) {
			securityInfo = new SecurityInfoBean();
		}
		return securityInfo;
	}

	public void setSecurityInfo(SecurityInfoBean securityInfo) {
		this.securityInfo = securityInfo;
	}

	public SecurityAmtDetailBean getSecurityAmtInfo() {
		if (null == securityAmtInfo) {
			securityAmtInfo = new SecurityAmtDetailBean();
		}
		return securityAmtInfo;
	}

	public void setSecurityAmtInfo(SecurityAmtDetailBean securityAmtInfo) {
		this.securityAmtInfo = securityAmtInfo;
	}

	public SecuritySettAmtBean getComSecurityAmt() {
		if (null == comSecurityAmt) {
			comSecurityAmt = new SecuritySettAmtBean();
		}
		return comSecurityAmt;
	}

	public void setComSecurityAmt(SecuritySettAmtBean comSecurityAmt) {
		this.comSecurityAmt = comSecurityAmt;
	}

	public SecuritySettAmtBean getMatSecurityAmt() {
		if (null == matSecurityAmt) {
			matSecurityAmt = new SecuritySettAmtBean();
		}
		return matSecurityAmt;
	}

	public void setMatSecurityAmt(SecuritySettAmtBean matSecurityAmt) {
		this.matSecurityAmt = matSecurityAmt;
	}

	public List<SecurityInfoBean> getLendingInfos() {
		if(null==lendingInfos){
			lendingInfos = new ArrayList<SecurityInfoBean>();
		}
		return lendingInfos;
	}

	public void setLendingInfos(List<SecurityInfoBean> lendingInfos) {
		this.lendingInfos = lendingInfos;
	}

	public SecurSettBean getSettInfo() {
		if (null == settInfo) {
			settInfo = new SecurSettBean();
		}
		return settInfo;
	}

	public void setSettInfo(SecurSettBean settInfo) {
		this.settInfo = settInfo;
	}

	public SecurSettBean getContraPartySettInfo() {
		if (null == contraPartySettInfo) {
			contraPartySettInfo = new SecurSettBean();
		}
		return contraPartySettInfo;
	}

	public void setContraPartySettInfo(SecurSettBean contraPartySettInfo) {
		this.contraPartySettInfo = contraPartySettInfo;
	}

	
	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public void setOccupancyDays(String occupancyDays) {
		this.occupancyDays = occupancyDays;
	}

	public String getOccupancyDays() {
		return occupancyDays;
	}
}
