package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 利率互换交易信息
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class IrsInfoBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6203127764249579748L;

	/** 产品代码 */
	private String productCode;
	/** 产品名称 */
	private String productName;

	/** 名义金额 */
	private BigDecimal notCcyAmt;
	
	/**起息日*/
	private Date startDate;

	/** 首期起息日 */
	private Date firstValueDate;

	/** 到期日 */
	private Date endDate;

	/**计息天数调整*/
	private String basisRest;
	
	/** 支付日调整 */
	private String payDateReset;

	/** 清算方式 近端期限 */
	private String settType;

	/** 计算机构 */
	private String calculateAgency;
	
	/***
	 * 交易价格
	 */
	private BigDecimal price;
	
	/***
	 * 期限
	 */
	private String mDateType;
	
	/***
	 * 币种
	 */
	private String ccy;
	
	/***
	 * 自定义备注
	 */
	private String textType;
	
	/***
	 * 备注内容
	 */
	private String text;
	
	
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getmDateType() {
		return mDateType;
	}

	public void setmDateType(String mDateType) {
		this.mDateType = mDateType;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getNotCcyAmt() {
		return notCcyAmt;
	}

	public void setNotCcyAmt(BigDecimal notCcyAmt) {
		this.notCcyAmt = notCcyAmt;
	}

	public Date getFirstValueDate() {
		return firstValueDate;
	}

	public void setFirstValueDate(Date firstValueDate) {
		this.firstValueDate = firstValueDate;
	}

	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getBasisRest() {
		return basisRest;
	}

	public void setBasisRest(String basisRest) {
		this.basisRest = basisRest;
	}

	public String getPayDateReset() {
		return payDateReset;
	}

	public void setPayDateReset(String payDateReset) {
		this.payDateReset = payDateReset;
	}

	public String getSettType() {
		return settType;
	}

	public void setSettType(String settType) {
		this.settType = settType;
	}

	public String getCalculateAgency() {
		return calculateAgency;
	}

	public void setCalculateAgency(String calculateAgency) {
		this.calculateAgency = calculateAgency;
	}

	public String getTextType() {
		return textType;
	}

	public void setTextType(String textType) {
		this.textType = textType;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("IrsInfoBean [productName=");
		builder.append(productName);
		builder.append(", notCcyAmt=");
		builder.append(notCcyAmt);
		builder.append(", startDate=");
		builder.append(startDate);
		builder.append(", firstValueDate=");
		builder.append(firstValueDate);
		builder.append(", endDate=");
		builder.append(endDate);
		builder.append(", payDateReset=");
		builder.append(payDateReset);
		builder.append(", settType=");
		builder.append(settType);
		builder.append(", calculateAgency");
		builder.append(calculateAgency);
		builder.append("]");
		return builder.toString();
	}

}
