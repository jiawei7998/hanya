package com.singlee.financial.pojo.component;

/**
 * 交易处理模式
 * @author chenxh
 *
 */
public enum DealModeEnum {

	/**
	 * 询价
	 */
	Enquiry,
	
	/**
	 * 竟价
	 */
	Competition,
	
	/**
	 * 撮合
	 */
	Married
}
