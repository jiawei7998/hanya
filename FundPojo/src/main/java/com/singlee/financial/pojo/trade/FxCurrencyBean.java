package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 外汇交易金额实体对象,保存交易货币、金额、汇率、点差等信息
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class FxCurrencyBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2257334169059510612L;

	
	//交易货币
	private String ccy;
	
	//交易币种金额
	private BigDecimal amt;
	
	//汇率
	private BigDecimal rate;
	
	//汇率点差
	private BigDecimal points;
	
	//交易对应货币
	private String contraCcy;
	
	//交易对应货币金额
	private BigDecimal contraAmt;
	
	//即期参考汇率
	private BigDecimal spotRate;
	
	//基准货币
	private String ccy1;
	//非基准货币
	private String ccy2;
	//基准货币金额
	private BigDecimal ccy1Amt;
	//非基准货币金额
	private BigDecimal ccy2Amt;
	//掉期交易报文出现
	private String legSide;
	//期限
	private String legSettlType;
	//折美元金额交易量
	private BigDecimal legRiskOrderQty;
	/**
	 * 获取即期参考汇率
	 * @return
	 */
	public BigDecimal getSpotRate() {
		return spotRate;
	}

	/**
	 * 设置即期参考汇率
	 * @param spotRate
	 */
	public void setSpotRate(BigDecimal spotRate) {
		this.spotRate = spotRate;
	}

	/**
	 * 获取远期升水点
	 * @return
	 */
	public BigDecimal getPoints() {
		return points;
	}

	/**
	 * 设置远期升水点
	 * @param points
	 */
	public void setPoints(BigDecimal points) {
		this.points = points;
	}

	/**
	 * 获取交易对应货币
	 * @return
	 */
	public String getContraCcy() {
		return contraCcy;
	}

	/**
	 * 设置交易对应货币
	 * @param contraCcy
	 */
	public void setContraCcy(String contraCcy) {
		this.contraCcy = contraCcy;
	}

	/**
	 * 获取交易对应货币金额
	 * @return
	 */
	public BigDecimal getContraAmt() {
		return contraAmt;
	}

	/**
	 * 设置交易对应货币金额
	 * @param contraAmt
	 */
	public void setContraAmt(BigDecimal contraAmt) {
		this.contraAmt = contraAmt;
	}

	/**
	 * 获取交易货币
	 * @return
	 */
	public String getCcy() {
		return ccy;
	}

	/**
	 * 设置交易货币
	 * @param ccy
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	/**
	 * 获取交易货币金额
	 * @return
	 */
	public BigDecimal getAmt() {
		return amt;
	}

	/**
	 * 设置交易货币金额
	 * @param amt
	 */
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	/**
	 * 获取成交汇率
	 * @return
	 */
	public BigDecimal getRate() {
		return rate;
	}

	/**
	 * 设置成交汇率
	 * @param rate
	 */
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public String getCcy1() {
		return ccy1;
	}

	public void setCcy1(String ccy1) {
		this.ccy1 = ccy1;
	}

	public String getCcy2() {
		return ccy2;
	}

	public void setCcy2(String ccy2) {
		this.ccy2 = ccy2;
	}

	public BigDecimal getCcy1Amt() {
		return ccy1Amt;
	}

	public void setCcy1Amt(BigDecimal ccy1Amt) {
		this.ccy1Amt = ccy1Amt;
	}

	public BigDecimal getCcy2Amt() {
		return ccy2Amt;
	}

	public void setCcy2Amt(BigDecimal ccy2Amt) {
		this.ccy2Amt = ccy2Amt;
	}

	public String getLegSide() {
		return legSide;
	}

	public void setLegSide(String legSide) {
		this.legSide = legSide;
	}

	public String getLegSettlType() {
		return legSettlType;
	}

	public void setLegSettlType(String legSettlType) {
		this.legSettlType = legSettlType;
	}

	public BigDecimal getLegRiskOrderQty() {
		return legRiskOrderQty;
	}

	public void setLegRiskOrderQty(BigDecimal legRiskOrderQty) {
		this.legRiskOrderQty = legRiskOrderQty;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FxCurrencyBean [amt=");
		builder.append(amt);
		builder.append(", ccy=");
		builder.append(ccy);
		builder.append(", contraAmt=");
		builder.append(contraAmt);
		builder.append(", contraCcy=");
		builder.append(contraCcy);
		builder.append(", points=");
		builder.append(points);
		builder.append(", rate=");
		builder.append(rate);
		builder.append(", spotRate=");
		builder.append(spotRate);
		builder.append("]");
		return builder.toString();
	}
	
}
