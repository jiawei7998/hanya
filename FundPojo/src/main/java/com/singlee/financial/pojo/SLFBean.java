package com.singlee.financial.pojo;

import com.singlee.financial.pojo.trade.RepoBaseBean;
import com.singlee.financial.pojo.trade.SLFAccountBean;
import com.singlee.financial.pojo.trade.SLFPledegBean;
import com.singlee.financial.pojo.trade.SLFSettAmtBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;



/**
 * 常备借贷便利业务实体对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SLFBean extends RepoBaseBean {

	/**
     * 
     */
    private static final long serialVersionUID = -6451505976289328625L;
    //常备借贷便标识 6
    private String marketCode;

    //日期
	private String tradeDate;
	//申请编号
	private String execID;
	//申请金额
	private BigDecimal tradeCashAmt;
	//期限
    private String tradeLimitDays;
	//利率
	private BigDecimal price;
	//起息日
	private Date settlDate;
	//到期日
	private Date settlDate2;
	//到期结算金额
	private BigDecimal settlAmt2;
	//利息
	private BigDecimal accruedInterestAmt;
	//数据类型 0-本方数据
	private String DataCategoryIndicator;
	//法人资金账号
	private String partySubIDType;
	//法人托管账号
//	private String partySubIDType;
	//流程状态
	private String processingStatusString;
	//更新时间
	private String transactTime;
	//抵押品种类
	private Integer CollateralType;

    //结算方式
//    private BigDecimal deliveryType;
    //质押券券面总额
    private BigDecimal lastQty;
      
    //质押券信息
    private List<SLFSettAmtBean> sLFSettAmtBean;
      
    //质押合同签署方信息
    private  SLFPledegBean sLFPledegBean;
      
    //资金账号和托管账号
    private List<SLFAccountBean> sLFAccountBean;

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public String getTradeDate() {
        return tradeDate;
    }
    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }
    public String getExecID() {
        return execID;
    }
    public void setExecID(String execID) {
        this.execID = execID;
    }
    public BigDecimal getTradeCashAmt() {
        return tradeCashAmt;
    }
    public void setTradeCashAmt(BigDecimal tradeCashAmt) {
        this.tradeCashAmt = tradeCashAmt;
    }
    public String getTradeLimitDays() {
        return tradeLimitDays;
    }
    public void setTradeLimitDays(String tradeLimitDays) {
        this.tradeLimitDays = tradeLimitDays;
    }
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public Date getSettlDate() {
        return settlDate;
    }
    public void setSettlDate(Date settlDate) {
        this.settlDate = settlDate;
    }
    public Date getSettlDate2() {
        return settlDate2;
    }
    public void setSettlDate2(Date settlDate2) {
        this.settlDate2 = settlDate2;
    }
    public BigDecimal getSettlAmt2() {
        return settlAmt2;
    }
    public void setSettlAmt2(BigDecimal settlAmt2) {
        this.settlAmt2 = settlAmt2;
    }
    public BigDecimal getAccruedInterestAmt() {
        return accruedInterestAmt;
    }
    public void setAccruedInterestAmt(BigDecimal accruedInterestAmt) {
        this.accruedInterestAmt = accruedInterestAmt;
    }
    public String getDataCategoryIndicator() {
        return DataCategoryIndicator;
    }
    public void setDataCategoryIndicator(String dataCategoryIndicator) {
        DataCategoryIndicator = dataCategoryIndicator;
    }
    public String getPartySubIDType() {
        return partySubIDType;
    }
    public void setPartySubIDType(String partySubIDType) {
        this.partySubIDType = partySubIDType;
    }
    public String getProcessingStatusString() {
        return processingStatusString;
    }
    public void setProcessingStatusString(String processingStatusString) {
        this.processingStatusString = processingStatusString;
    }
    public String getTransactTime() {
        return transactTime;
    }
    public void setTransactTime(String transactTime) {
        this.transactTime = transactTime;
    }
    public Integer getCollateralType() {
        return CollateralType;
    }
    public void setCollateralType(Integer collateralType) {
        CollateralType = collateralType;
    }
    public BigDecimal getLastQty() {
        return lastQty;
    }
    public void setLastQty(BigDecimal lastQty) {
        this.lastQty = lastQty;
    }
    public List<SLFSettAmtBean> getsLFSettAmtBean() {
        return sLFSettAmtBean;
    }
    public void setsLFSettAmtBean(List<SLFSettAmtBean> sLFSettAmtBean) {
        this.sLFSettAmtBean = sLFSettAmtBean;
    }
    public SLFPledegBean getsLFPledegBean() {
        return sLFPledegBean;
    }
    public void setsLFPledegBean(SLFPledegBean sLFPledegBean) {
        this.sLFPledegBean = sLFPledegBean;
    }

    public List<SLFAccountBean> getsLFAccountBean() {
        return sLFAccountBean;
    }
    public void setsLFAccountBean(List<SLFAccountBean> sLFAccountBean) {
        this.sLFAccountBean = sLFAccountBean;
    }
    @Override
    public String toString() {
        return "SLFBean [tradeDate=" + tradeDate + ", execID=" + execID + ", tradeCashAmt=" + tradeCashAmt
                + ", tradeLimitDays=" + tradeLimitDays + ", price=" + price + ", settlDate=" + settlDate
                + ", settlDate2=" + settlDate2 + ", settlAmt2=" + settlAmt2 + ", accruedInterestAmt="
                + accruedInterestAmt + ", DataCategoryIndicator=" + DataCategoryIndicator + ", partySubIDType="
                + partySubIDType + ", processingStatusString=" + processingStatusString + ", transactTime="
                + transactTime + ", CollateralType=" + CollateralType + ", lastQty=" + lastQty + ", sLFSettAmtBean="
                + sLFSettAmtBean + ", sLFPledegBean=" + sLFPledegBean + ", sLFAccountBean=" + sLFAccountBean + "]";
    }
 
    
    

}
