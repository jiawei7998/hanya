package com.singlee.financial.pojo.component;

public enum ExecStatus {

	/**
	 * 已完成
	 */
	Finish("F"),
	/**
	 * 正常
	 */
	Normal("0"),
	/**
	 * 撤销
	 */
	Revoke("4"),
	/**
	 * 更新
	 */
	Modify("5"),
	/**
	 * 停止
	 */
	Stop("7"),
	/**
	 * 拒绝
	 */
	Refuse("8"),
	/**
	 * 交易撤销
	 */
	Cancel("H"),
	/**
	 * 应急
	 */
	Emergency("101");

	private String index;

	ExecStatus(String index) {
		this.index = index;
	}

	@Override
	public String toString() {
		return this.index;
	}
}
