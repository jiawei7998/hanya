package com.singlee.financial.pojo.trade;

import java.io.Serializable;

public class FxOptionPremiumBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String optPremiumBasis;// 期权费类型：1-pips
	private String optPremiumValue;// 期权费率
	private String optPremiumCurrency;// 期权费币种
	private String optPremiumAmt;// 期权费总额
	private String decimalPlaces;// 小数位数
	private String deliveryDate;// 期权费交割日
	private String payerPartyReference;// 期权费支付方
	private String receiverPartyReference;// 期权费收取方

	public String getOptPremiumBasis() {
		return optPremiumBasis;
	}

	public void setOptPremiumBasis(String optPremiumBasis) {
		this.optPremiumBasis = optPremiumBasis;
	}

	public String getOptPremiumValue() {
		return optPremiumValue;
	}

	public void setOptPremiumValue(String optPremiumValue) {
		this.optPremiumValue = optPremiumValue;
	}

	public String getOptPremiumCurrency() {
		return optPremiumCurrency;
	}

	public void setOptPremiumCurrency(String optPremiumCurrency) {
		this.optPremiumCurrency = optPremiumCurrency;
	}

	public String getOptPremiumAmt() {
		return optPremiumAmt;
	}

	public void setOptPremiumAmt(String optPremiumAmt) {
		this.optPremiumAmt = optPremiumAmt;
	}

	public String getDecimalPlaces() {
		return decimalPlaces;
	}

	public void setDecimalPlaces(String decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getPayerPartyReference() {
		return payerPartyReference;
	}

	public void setPayerPartyReference(String payerPartyReference) {
		this.payerPartyReference = payerPartyReference;
	}

	public String getReceiverPartyReference() {
		return receiverPartyReference;
	}

	public void setReceiverPartyReference(String receiverPartyReference) {
		this.receiverPartyReference = receiverPartyReference;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PremiumBean [decimalPlaces=");
		builder.append(decimalPlaces);
		builder.append(", deliveryDate=");
		builder.append(deliveryDate);
		builder.append(", optPremiumAmt=");
		builder.append(optPremiumAmt);
		builder.append(", optPremiumBasis=");
		builder.append(optPremiumBasis);
		builder.append(", optPremiumCurrency=");
		builder.append(optPremiumCurrency);
		builder.append(", optPremiumValue=");
		builder.append(optPremiumValue);
		builder.append(", payerPartyReference=");
		builder.append(payerPartyReference);
		builder.append(", receiverPartyReference=");
		builder.append(receiverPartyReference);
		builder.append("]");
		return builder.toString();
	}
}
