package com.singlee.financial.pojo;

import java.math.BigDecimal;

/*
 *
 * 同业借款附加协议
 *
 */
public class InterBankLoansAddBean extends InterBankLoansBean {

    private static final long serialVersionUID = 1L;

    //附加协议类型
    private String supplType;
    //操作类型
    private String supplTransType;

    //有无补充条款
    private String supplementAttachmentIndicator;

    //提前还款编号
    private String advanceId;
    //
    private String advanceInfoType;
    //提前还款状态
    private String advanceStatus;
    //提前还款申请业务发生时间
    private String advanveBusinessTime;
    //提前还款日
    private String advanceDate;
    //提前还款本金
    private BigDecimal advanceAmt;
    //提前还款补偿类型
    private String advanceCompensationType;
    //提前还款利率
    private BigDecimal advancePrice;
    //提前还款补偿金额
    private BigDecimal advanceLastQty;
    //提前还款应计利息
    private BigDecimal advanceInterestAmt;
    //提前还款结算金额
    private BigDecimal advanceSettlAmt;
    //提前还款后剩余存款本金
    private BigDecimal advanceLeavesAmt;
    //提前还款后到期还款金额
    private BigDecimal advanceExpireAmt;
    //提前还款条款
    private String advanceClause;
    //
    private String advanceAttachmentIndicator;
    //提前还款借入方交易员
    private String advanceTraderName1;
    //提前还款借出方交易员
    private String advanceTraderName2;

    public String getSupplType() {
        return supplType;
    }

    public void setSupplType(String supplType) {
        this.supplType = supplType;
    }

    public String getSupplTransType() {
        return supplTransType;
    }

    public void setSupplTransType(String supplTransType) {
        this.supplTransType = supplTransType;
    }

    public String getSupplementAttachmentIndicator() {
        return supplementAttachmentIndicator;
    }

    public void setSupplementAttachmentIndicator(String supplementAttachmentIndicator) {
        this.supplementAttachmentIndicator = supplementAttachmentIndicator;
    }

    public String getAdvanceId() {
        return advanceId;
    }

    public void setAdvanceId(String advanceId) {
        this.advanceId = advanceId;
    }

    public String getAdvanceInfoType() {
        return advanceInfoType;
    }

    public void setAdvanceInfoType(String advanceInfoType) {
        this.advanceInfoType = advanceInfoType;
    }

    public String getAdvanceStatus() {
        return advanceStatus;
    }

    public void setAdvanceStatus(String advanceStatus) {
        this.advanceStatus = advanceStatus;
    }

    public String getAdvanveBusinessTime() {
        return advanveBusinessTime;
    }

    public void setAdvanveBusinessTime(String advanveBusinessTime) {
        this.advanveBusinessTime = advanveBusinessTime;
    }

    public String getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(String advanceDate) {
        this.advanceDate = advanceDate;
    }

    public BigDecimal getAdvanceAmt() {
        return advanceAmt;
    }

    public void setAdvanceAmt(BigDecimal advanceAmt) {
        this.advanceAmt = advanceAmt;
    }

    public String getAdvanceCompensationType() {
        return advanceCompensationType;
    }

    public void setAdvanceCompensationType(String advanceCompensationType) {
        this.advanceCompensationType = advanceCompensationType;
    }

    public BigDecimal getAdvancePrice() {
        return advancePrice;
    }

    public void setAdvancePrice(BigDecimal advancePrice) {
        this.advancePrice = advancePrice;
    }

    public BigDecimal getAdvanceLastQty() {
        return advanceLastQty;
    }

    public void setAdvanceLastQty(BigDecimal advanceLastQty) {
        this.advanceLastQty = advanceLastQty;
    }

    public BigDecimal getAdvanceInterestAmt() {
        return advanceInterestAmt;
    }

    public void setAdvanceInterestAmt(BigDecimal advanceInterestAmt) {
        this.advanceInterestAmt = advanceInterestAmt;
    }

    public BigDecimal getAdvanceSettlAmt() {
        return advanceSettlAmt;
    }

    public void setAdvanceSettlAmt(BigDecimal advanceSettlAmt) {
        this.advanceSettlAmt = advanceSettlAmt;
    }

    public BigDecimal getAdvanceLeavesAmt() {
        return advanceLeavesAmt;
    }

    public void setAdvanceLeavesAmt(BigDecimal advanceLeavesAmt) {
        this.advanceLeavesAmt = advanceLeavesAmt;
    }

    public BigDecimal getAdvanceExpireAmt() {
        return advanceExpireAmt;
    }

    public void setAdvanceExpireAmt(BigDecimal advanceExpireAmt) {
        this.advanceExpireAmt = advanceExpireAmt;
    }

    public String getAdvanceClause() {
        return advanceClause;
    }

    public void setAdvanceClause(String advanceClause) {
        this.advanceClause = advanceClause;
    }

    public String getAdvanceAttachmentIndicator() {
        return advanceAttachmentIndicator;
    }

    public void setAdvanceAttachmentIndicator(String advanceAttachmentIndicator) {
        this.advanceAttachmentIndicator = advanceAttachmentIndicator;
    }

    public String getAdvanceTraderName1() {
        return advanceTraderName1;
    }

    public void setAdvanceTraderName1(String advanceTraderName1) {
        this.advanceTraderName1 = advanceTraderName1;
    }

    public String getAdvanceTraderName2() {
        return advanceTraderName2;
    }

    public void setAdvanceTraderName2(String advanceTraderName2) {
        this.advanceTraderName2 = advanceTraderName2;
    }
}
