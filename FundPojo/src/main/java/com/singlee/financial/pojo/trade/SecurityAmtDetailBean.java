package com.singlee.financial.pojo.trade;

import java.math.BigDecimal;

/**
 * 债券金额组件,包含收益率曲线,净价,应计利息总金额,净价,全价
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SecurityAmtDetailBean extends SecuritySettAmtBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5081304739336419974L;

	//收益率曲线
	private BigDecimal yeld;
	
	//应计利息总金额
	private BigDecimal interestTotalAmt;
	
	//净价
	private BigDecimal price;
	
	//全价
	private BigDecimal dirtyPrice;

	//交易金额
	private BigDecimal amt;
	
	//结算币种
	private String settlCcy;
	
	public String getSettlCcy() {
		return settlCcy;
	}

	public void setSettlCcy(String settlCcy) {
		this.settlCcy = settlCcy;
	}

	/**
	 * 获取收益率曲线
	 * @return
	 */
	public BigDecimal getYeld() {
		return yeld;
	}

	/**
	 * 设置收益率曲线
	 * @param yeld
	 */
	public void setYeld(BigDecimal yeld) {
		this.yeld = yeld;
	}

	/**
	 * 获取应计利息总金额
	 * @return
	 */
	public BigDecimal getInterestTotalAmt() {
		return interestTotalAmt;
	}

	/**
	 * 设置应计利息总金额
	 * @param interestTotalAmt
	 */
	public void setInterestTotalAmt(BigDecimal interestTotalAmt) {
		this.interestTotalAmt = interestTotalAmt;
	}

	/**
	 * 获取交易净价
	 * @return
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * 设置交易净价
	 * @param price
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * 获取交易全价
	 * @return
	 */
	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}

	/**
	 * 设置交易全价
	 * @param dirtyPrice
	 */
	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SecurityAmtDetailBean [dirtyPrice=");
		builder.append(dirtyPrice);
		builder.append(", interestTotalAmt=");
		builder.append(interestTotalAmt);
		builder.append(", price=");
		builder.append(price);
		builder.append(", yeld=");
		builder.append(yeld);
		builder.append(", getInterestAmt()=");
		builder.append(getInterestAmt());
		builder.append(", getSettAmt()=");
		builder.append(getSettAmt());
		builder.append(", getSettDate()=");
		builder.append(getSettDate());
		builder.append(", getSettType()=");
		builder.append(getSettType());
		builder.append("]");
		return builder.toString();
	}
	
	
}
