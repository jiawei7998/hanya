package com.singlee.financial.pojo.trade;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 质押式回购 跨托管机构对象
 */
public class SecurityCustodianBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4369821601542263122L;

    //跨托管机构名称
    private String custodianInstitutionName;
    //跨托管交易金额
    private BigDecimal custodianTradeAmt;
    //跨托管回购利息
    private BigDecimal custodianAccruedInterestAmt;
    //跨托管到期结算金额
    private BigDecimal custodianSettlCurrAmt2;
    //跨托管券面总额
    private BigDecimal custodianQty;


    public String getCustodianInstitutionName() {
        return custodianInstitutionName;
    }

    public void setCustodianInstitutionName(String custodianInstitutionName) {
        this.custodianInstitutionName = custodianInstitutionName;
    }

    public BigDecimal getCustodianTradeAmt() {
        return custodianTradeAmt;
    }

    public void setCustodianTradeAmt(BigDecimal custodianTradeAmt) {
        this.custodianTradeAmt = custodianTradeAmt;
    }

    public BigDecimal getCustodianAccruedInterestAmt() {
        return custodianAccruedInterestAmt;
    }

    public void setCustodianAccruedInterestAmt(BigDecimal custodianAccruedInterestAmt) {
        this.custodianAccruedInterestAmt = custodianAccruedInterestAmt;
    }

    public BigDecimal getCustodianSettlCurrAmt2() {
        return custodianSettlCurrAmt2;
    }

    public void setCustodianSettlCurrAmt2(BigDecimal custodianSettlCurrAmt2) {
        this.custodianSettlCurrAmt2 = custodianSettlCurrAmt2;
    }

    public BigDecimal getCustodianQty() {
        return custodianQty;
    }

    public void setCustodianQty(BigDecimal custodianQty) {
        this.custodianQty = custodianQty;
    }
}
