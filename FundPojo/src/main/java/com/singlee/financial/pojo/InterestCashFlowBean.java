package com.singlee.financial.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 付息现金流
 *
 * @author chenxh
 */
public class InterestCashFlowBean implements Serializable {

    private static final long serialVersionUID = 5214334334567177010L;

    //付息日 按期付息的时候有值
    private String intPayDate;

    //应计利息 按期付息的时候有值
    private BigDecimal accruedIntAmt;

    public String getIntPayDate() {
        return intPayDate;
    }

    public void setIntPayDate(String intPayDate) {
        this.intPayDate = intPayDate;
    }

    public BigDecimal getAccruedIntAmt() {
        return accruedIntAmt;
    }

    public void setAccruedIntAmt(BigDecimal accruedIntAmt) {
        this.accruedIntAmt = accruedIntAmt;
    }
}
