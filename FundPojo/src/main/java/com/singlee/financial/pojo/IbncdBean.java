package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SubScriptionInstBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/****
 * 存单发行
 * 
 * @author lee
 *
 */
public class IbncdBean extends TradeBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4799646628212957165L;

	// 交易币种
	private String ccy;

	// 工具类型 CD-同业存单
	private String secType;

	// 消息类型 2-发行结果信息 3投标结果信息
	private String secDefType;

	// 工具全称
	private String fullName;

	// 工具简称
	private String shortName;

	// 数据类型标识
	private String dataCategoryIndicator;

	// 工具代码
	private String secId;

	// 发行方式、招标方式
	private String issMethod;

	// 工具状态
	private String secStatus;

	// 失败原因(当965号域取值2时，传输该域)
	private String text;

	// 实际发行量(亿元)
	private BigDecimal issSize;

	// 计划发行量(亿元)
	private BigDecimal issSizeForPlan;

	// 利差
	private BigDecimal spread;

	// 票面利率
	private BigDecimal couponRate;

	//息票类型
	private String rateType;

	// 发行价格(元)
	private BigDecimal issPrice;
	
	// 本方清算信息
	private SecurSettBean settInfo;

	// 对手方清算信息
	private SecurSettBean contraPartySettInfo;

	// 认购人信息
	private List<SubScriptionInstBean> subScriptions;
	
	/***
	 * 招标标的
		仅发行方式为“单一价格招标发行”时
		传输；
		1-价格
		2-利率
		3-利差
	 */
	private String tenderSubject;
	//投标总量（亿元）
	private BigDecimal totalTenderAmt;
	//中标总量（亿元）
	private BigDecimal totalTenderSuccessAmt;
	//投标倍数
	private BigDecimal tenderMultiples;
	//TenderParties
	private int tenderParties;
    //中标家数
	private int tenderSuccessParties;
	//投标笔数
	private int tenderNumber;
	//中标笔数
	private int tenderSuccessNumber;
	
	public SecurSettBean getSettInfo() {
		if (null == settInfo) {
			settInfo = new SecurSettBean();
		}
		return settInfo;
	}

	public void setSettInfo(SecurSettBean settInfo) {
		this.settInfo = settInfo;
	}

	public void setContraPartySettInfo(SecurSettBean contraPartySettInfo) {
		this.contraPartySettInfo = contraPartySettInfo;
	}

	public SecurSettBean getContraPartySettInfo() {
		if (null == contraPartySettInfo) {
			contraPartySettInfo = new SecurSettBean();
		}
		return contraPartySettInfo;
	}

	public List<SubScriptionInstBean> getSubScriptions() {
		if (null == subScriptions) {
			subScriptions = new ArrayList<SubScriptionInstBean>();
		}
		return subScriptions;
	}

	public void setSubScriptions(List<SubScriptionInstBean> subScriptions) {
		this.subScriptions = subScriptions;
	}

	public String getCcy() {
		return ccy;
	}

	public void setCcy(String ccy) {
		this.ccy = ccy;
	}

	public String getSecType() {
		return secType;
	}

	public void setSecType(String secType) {
		this.secType = secType;
	}

	public String getSecDefType() {
		return secDefType;
	}

	public void setSecDefType(String secDefType) {
		this.secDefType = secDefType;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDataCategoryIndicator() {
		return dataCategoryIndicator;
	}

	public void setDataCategoryIndicator(String dataCategoryIndicator) {
		this.dataCategoryIndicator = dataCategoryIndicator;
	}

	public String getSecId() {
		return secId;
	}

	public void setSecId(String secId) {
		this.secId = secId;
	}

	public String getRateType() {
		return rateType;
	}

	public void setRateType(String rateType) {
		this.rateType = rateType;
	}

	public String getSecStatus() {
		return secStatus;
	}

	public void setSecStatus(String secStatus) {
		this.secStatus = secStatus;
	}

	public String getIssMethod() {
		return issMethod;
	}

	public void setIssMethod(String issMethod) {
		this.issMethod = issMethod;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public BigDecimal getIssPrice() {
		return issPrice;
	}

	public void setIssPrice(BigDecimal issPrice) {
		this.issPrice = issPrice;
	}

	public BigDecimal getIssSize() {
		return issSize;
	}

	public void setIssSize(BigDecimal issSize) {
		this.issSize = issSize;
	}

	public BigDecimal getSpread() {
		return spread;
	}

	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}

	public BigDecimal getCouponRate() {
		return couponRate;
	}

	public void setCouponRate(BigDecimal couponRate) {
		this.couponRate = couponRate;
	}

	public String getTenderSubject() {
		return tenderSubject;
	}

	public void setTenderSubject(String tenderSubject) {
		this.tenderSubject = tenderSubject;
	}

	public BigDecimal getTotalTenderAmt() {
		return totalTenderAmt;
	}

	public void setTotalTenderAmt(BigDecimal totalTenderAmt) {
		this.totalTenderAmt = totalTenderAmt;
	}

	public BigDecimal getTotalTenderSuccessAmt() {
		return totalTenderSuccessAmt;
	}

	public void setTotalTenderSuccessAmt(BigDecimal totalTenderSuccessAmt) {
		this.totalTenderSuccessAmt = totalTenderSuccessAmt;
	}

	public BigDecimal getTenderMultiples() {
		return tenderMultiples;
	}

	public void setTenderMultiples(BigDecimal tenderMultiples) {
		this.tenderMultiples = tenderMultiples;
	}

	public int getTenderParties() {
		return tenderParties;
	}

	public void setTenderParties(int tenderParties) {
		this.tenderParties = tenderParties;
	}

	public int getTenderSuccessParties() {
		return tenderSuccessParties;
	}

	public void setTenderSuccessParties(int tenderSuccessParties) {
		this.tenderSuccessParties = tenderSuccessParties;
	}

	public int getTenderNumber() {
		return tenderNumber;
	}

	public void setTenderNumber(int tenderNumber) {
		this.tenderNumber = tenderNumber;
	}

	public int getTenderSuccessNumber() {
		return tenderSuccessNumber;
	}

	public void setTenderSuccessNumber(int tenderSuccessNumber) {
		this.tenderSuccessNumber = tenderSuccessNumber;
	}

	public BigDecimal getIssSizeForPlan() {
		return issSizeForPlan;
	}

	public void setIssSizeForPlan(BigDecimal issSizeForPlan) {
		this.issSizeForPlan = issSizeForPlan;
	}
}
