package com.singlee.financial.pojo;

import java.io.Serializable;

public class TradeSettlsRmbBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String execId             ; //  成交单号
	private String institutionId      ; //  本方机构号
	private String tradeId            ; //  交易员编号
	private String tradeName          ; //  交易员名称
	private String shortName          ; //  机构简称
	private String fullName           ; //  机构全称
	private String contactPhone;//联系电话
	private String contactFax;//联系传真
	private String markerTakerRole    ; // 是否发起方
	private String bankName           ; //  开户行名
	private String bankAcctNo         ; // 开户行号
	private String acctName           ; //  账号名
	private String acctNo             ; //  账号
	private String remark             ; //   备注
	private String trustAcctName      ; // 托管机构名称
	private String trustAcctNo        ; // 托管机构号
	private String trustInstitutionId ; // 托管机构ID
	
	private String cInstitutionId     ; // 对手机构号
	private String cTradeId           ; // 对手交易员编号
	private String cTradeName         ; // 对手交易员名称
	private String cShortName         ; // 对手机构简称
	private String cFullName          ; // 对手机构全称
	private String cContactPhone;//联系电话
	private String cContactFax;//联系传真
	private String cMarkerTakerRole   ; //对手是否发起方
	private String cBankName          ; // 对手开户行名
	private String cBankAcctNo        ; //对手开户行号
	private String cAcctName          ; // 对手账号名
	private String cAcctNo            ; // 对手账号
	private String cRemark            ; //  对手备注
	private String cTrustAcctName     ; //对手托管机构名称
	private String cTrustAcctNo       ; //对手托管机构号
	private String cTrustInstitutionId; //对手托管机构ID
	public String getExecId() {
		return execId;
	}
	public void setExecId(String execId) {
		this.execId = execId;
	}
	public String getInstitutionId() {
		return institutionId;
	}
	public void setInstitutionId(String institutionId) {
		this.institutionId = institutionId;
	}
	public String getTradeId() {
		return tradeId;
	}
	public void setTradeId(String tradeId) {
		this.tradeId = tradeId;
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getMarkerTakerRole() {
		return markerTakerRole;
	}
	public void setMarkerTakerRole(String markerTakerRole) {
		this.markerTakerRole = markerTakerRole;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAcctNo() {
		return bankAcctNo;
	}
	public void setBankAcctNo(String bankAcctNo) {
		this.bankAcctNo = bankAcctNo;
	}
	public String getAcctName() {
		return acctName;
	}
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getTrustAcctName() {
		return trustAcctName;
	}
	public void setTrustAcctName(String trustAcctName) {
		this.trustAcctName = trustAcctName;
	}
	public String getTrustAcctNo() {
		return trustAcctNo;
	}
	public void setTrustAcctNo(String trustAcctNo) {
		this.trustAcctNo = trustAcctNo;
	}
	public String getTrustInstitutionId() {
		return trustInstitutionId;
	}
	public void setTrustInstitutionId(String trustInstitutionId) {
		this.trustInstitutionId = trustInstitutionId;
	}
	public String getcInstitutionId() {
		return cInstitutionId;
	}
	public void setcInstitutionId(String cInstitutionId) {
		this.cInstitutionId = cInstitutionId;
	}
	public String getcTradeId() {
		return cTradeId;
	}
	public void setcTradeId(String cTradeId) {
		this.cTradeId = cTradeId;
	}
	public String getcTradeName() {
		return cTradeName;
	}
	public void setcTradeName(String cTradeName) {
		this.cTradeName = cTradeName;
	}
	public String getcShortName() {
		return cShortName;
	}
	public void setcShortName(String cShortName) {
		this.cShortName = cShortName;
	}
	public String getcFullName() {
		return cFullName;
	}
	public void setcFullName(String cFullName) {
		this.cFullName = cFullName;
	}
	public String getcMarkerTakerRole() {
		return cMarkerTakerRole;
	}
	public void setcMarkerTakerRole(String cMarkerTakerRole) {
		this.cMarkerTakerRole = cMarkerTakerRole;
	}
	public String getcBankName() {
		return cBankName;
	}
	public void setcBankName(String cBankName) {
		this.cBankName = cBankName;
	}
	public String getcBankAcctNo() {
		return cBankAcctNo;
	}
	public void setcBankAcctNo(String cBankAcctNo) {
		this.cBankAcctNo = cBankAcctNo;
	}
	public String getcAcctName() {
		return cAcctName;
	}
	public void setcAcctName(String cAcctName) {
		this.cAcctName = cAcctName;
	}
	public String getcAcctNo() {
		return cAcctNo;
	}
	public void setcAcctNo(String cAcctNo) {
		this.cAcctNo = cAcctNo;
	}
	public String getcRemark() {
		return cRemark;
	}
	public void setcRemark(String cRemark) {
		this.cRemark = cRemark;
	}
	public String getcTrustAcctName() {
		return cTrustAcctName;
	}
	public void setcTrustAcctName(String cTrustAcctName) {
		this.cTrustAcctName = cTrustAcctName;
	}
	public String getcTrustAcctNo() {
		return cTrustAcctNo;
	}
	public void setcTrustAcctNo(String cTrustAcctNo) {
		this.cTrustAcctNo = cTrustAcctNo;
	}
	public String getcTrustInstitutionId() {
		return cTrustInstitutionId;
	}
	public void setcTrustInstitutionId(String cTrustInstitutionId) {
		this.cTrustInstitutionId = cTrustInstitutionId;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getContactFax() {
		return contactFax;
	}
	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}
	public String getcContactPhone() {
		return cContactPhone;
	}
	public void setcContactPhone(String cContactPhone) {
		this.cContactPhone = cContactPhone;
	}
	public String getcContactFax() {
		return cContactFax;
	}
	public void setcContactFax(String cContactFax) {
		this.cContactFax = cContactFax;
	}

	
	
}
