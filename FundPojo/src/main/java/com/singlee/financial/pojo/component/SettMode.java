package com.singlee.financial.pojo.component;

/**
 * 清算模式
 */
public enum SettMode {

	/**
	 * 双边全额
	 */
	Normal, 
	
	/**
	 * 集中清算
	 */
	Central,
	
	/**
	 * 双边净额
	 */
	Netting,
	
	/**
	 * SGE
	 */
	SGE
}
