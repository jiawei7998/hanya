package com.singlee.financial.pojo;

import java.io.Serializable;

/**
 * 
 * @author 代号_47
 *
 */
public class TradingFailureBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//交易号
	private String dealNo;
	
	//交易类型
	private String MarketIndicator;
	
	//交易日期
	private String TradeDate;
	
	//报文内容
	private String xml;

	public String getDealNo() {
		return dealNo;
	}

	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}

	public String getMarketIndicator() {
		return MarketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		MarketIndicator = marketIndicator;
	}

	public String getTradeDate() {
		return TradeDate;
	}

	public void setTradeDate(String tradeDate) {
		TradeDate = tradeDate;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}
	
	
	
	
	

}
