package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.SecurSettBean;
import com.singlee.financial.pojo.trade.SecurityAmtDetailBean;
import com.singlee.financial.pojo.trade.SecurityInfoBean;
import com.singlee.financial.pojo.trade.TradeBaseBean;

/**
 * 现券业务实体对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class FixedIncomeBean extends TradeBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2143416021409927611L;

	//债券信息
	private SecurityInfoBean securityInfo;
	
	//债券交易金额
	private SecurityAmtDetailBean securityAmt;
	
	//本方清算信息
	private SecurSettBean settInfo;
	
	//对手方清算信息
	private SecurSettBean contraPartySettInfo;

	/**
	 * 获取债券信息
	 * @return
	 */
	public SecurityInfoBean getSecurityInfo() {
		if(null == securityInfo){
			securityInfo = new SecurityInfoBean();
		}
		return securityInfo;
	}

	/**
	 * 设置债券信息
	 * @param securityInfo
	 */
	public void setSecurityInfo(SecurityInfoBean securityInfo) {
		this.securityInfo = securityInfo;
	}

	/**
	 * 获取债券交易金额组件,包含清算方式,结算金额,应计利息,结算日期
	 * @return
	 */
	public SecurityAmtDetailBean getSecurityAmt() {
		if(null == securityAmt){
			securityAmt = new SecurityAmtDetailBean();
		}
		return securityAmt;
	}

	/**
	 * 设置债券交易金额组件,包含清算方式,结算金额,应计利息,结算日期
	 * @param securityAmt
	 */
	public void setSecurityAmt(SecurityAmtDetailBean securityAmt) {
		this.securityAmt = securityAmt;
	}

	/**
	 * 获取本方债券清算信息
	 * @return
	 */
	public SecurSettBean getSettInfo() {
		if(null == settInfo){
			settInfo = new SecurSettBean();
		}
		return settInfo;
	}

	/**
	 * 设置本方债券清算信息
	 * @param settInfo
	 */
	public void setSettInfo(SecurSettBean settInfo) {
		this.settInfo = settInfo;
	}

	/**
	 * 获取对手方债券清算信息
	 * @return
	 */
	public SecurSettBean getContraPartySettInfo() {
		if(null == contraPartySettInfo){
			contraPartySettInfo = new SecurSettBean();
		}
		return contraPartySettInfo;
	}

	/**
	 * 设置对手方债券清算信息
	 * @param contraPartySettInfo
	 */
	public void setContraPartySettInfo(SecurSettBean contraPartySettInfo) {
		this.contraPartySettInfo = contraPartySettInfo;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FixedIncomeBean [contraPartySettInfo=");
		builder.append(contraPartySettInfo);
		builder.append(", securityAmt=");
		builder.append(securityAmt);
		builder.append(", securityInfo=");
		builder.append(securityInfo);
		builder.append(", settInfo=");
		builder.append(settInfo);
		builder.append("]");
		return builder.toString();
	}
	
	
}
