package com.singlee.financial.pojo.trade;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 机构信息,保存本方或者交易对手的机构ID、简称、全称、交易员等信息
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SubScriptionInstBean extends InstitutionBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8572335166844227657L;
	
	private String execId;
	
	//存单发行时 应缴纳金额  
	private BigDecimal feeActualAmt;
	
	//缴款信息 实际缴款金额
	private BigDecimal netCommissionAmt;
	
	//持有量 （万元）
	private BigDecimal qty;
	
	//存单发行时 认购量
	private BigDecimal subScriptionAmt;

	//存单发行时 认购时间
	private Date subScriptionTime;
	
	//投标价位
	private BigDecimal tenderPx;

	//投标量（亿元）
	private BigDecimal tenderAmt;
	
	//中标价位
	private BigDecimal tenderSuccessPx;
	
	//中标量（亿元）
	private BigDecimal tenderSuccessAmt;

	private String trustAcctNo;
	
	public String getTrustAcctNo() {
		return trustAcctNo;
	}

	public void setTrustAcctNo(String trustAcctNo) {
		this.trustAcctNo = trustAcctNo;
	}

	public BigDecimal getTenderPx() {
		return tenderPx;
	}

	public void setTenderPx(BigDecimal tenderPx) {
		this.tenderPx = tenderPx;
	}

	public BigDecimal getTenderAmt() {
		return tenderAmt;
	}

	public void setTenderAmt(BigDecimal tenderAmt) {
		this.tenderAmt = tenderAmt;
	}

	public BigDecimal getTenderSuccessPx() {
		return tenderSuccessPx;
	}

	public void setTenderSuccessPx(BigDecimal tenderSuccessPx) {
		this.tenderSuccessPx = tenderSuccessPx;
	}

	public BigDecimal getTenderSuccessAmt() {
		return tenderSuccessAmt;
	}

	public void setTenderSuccessAmt(BigDecimal tenderSuccessAmt) {
		this.tenderSuccessAmt = tenderSuccessAmt;
	}

	public String getExecId() {
		return execId;
	}

	public void setExecId(String execId) {
		this.execId = execId;
	}

	public BigDecimal getFeeActualAmt() {
		return feeActualAmt;
	}

	public void setFeeActualAmt(BigDecimal feeActualAmt) {
		this.feeActualAmt = feeActualAmt;
	}

	public BigDecimal getSubScriptionAmt() {
		return subScriptionAmt;
	}

	public void setSubScriptionAmt(BigDecimal subScriptionAmt) {
		this.subScriptionAmt = subScriptionAmt;
	}

	public Date getSubScriptionTime() {
		return subScriptionTime;
	}

	public void setSubScriptionTime(Date subScriptionTime) {
		this.subScriptionTime = subScriptionTime;
	}

	public BigDecimal getNetCommissionAmt() {
		return netCommissionAmt;
	}

	public void setNetCommissionAmt(BigDecimal netCommissionAmt) {
		this.netCommissionAmt = netCommissionAmt;
	}

	public BigDecimal getQty() {
		return qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InstitutionBean [fullName=");
		builder.append(getFullName());
		builder.append(", institutionId=");
		builder.append(getInstitutionId());
		builder.append(", shortName=");
		builder.append(getShortName());
		builder.append(", swiftCode=");
		builder.append(getSwiftCode());
		builder.append(", tradeId=");
		builder.append(getTradeId());
		builder.append(", tradeName=");
		builder.append(getTradeName());
		builder.append("]");
		return builder.toString();
	}

}
