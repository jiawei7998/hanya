package com.singlee.financial.pojo.component;

/**
 * 交易方向
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public enum PsEnum {
	/**
	 * 买入
	 * 拆借交易时为拆入
	 */
	P,
	
	/**
	 * 卖出
	 * 拆出交易时为拆出
	 */
	S,
	
	/**
	 * 固定-浮动
	 */
	J,
	
	/**
	 * 浮动-浮动
	 */
	K,
	/**
	 * cfets下行时未知方向
	 */
	X
}
