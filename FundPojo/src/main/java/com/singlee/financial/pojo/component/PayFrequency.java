package com.singlee.financial.pojo.component;

/**
 * 付息频率
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public enum PayFrequency {

	/**
	 * Annually按年付息
	 */
	A,
	
	/**
	 * Semiannually按半年付息
	 */
	S,
	
	/**
	 * Quarter按季付付息
	 */
	Q,
	
	/**
	 * Monthly按月付息
	 */
	M,
	
	/**
	 * 到期一次
	 */
	ONCE
}
