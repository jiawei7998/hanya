package com.singlee.financial.pojo;

import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.financial.pojo.trade.CcsDetailBean;
import com.singlee.financial.pojo.trade.CcsInfoBean;
import com.singlee.financial.pojo.trade.FxBaseBean;

/**
 * 货币互换
 * 
 * @author chenxh
 * 
 */
public class SwapCrsBean extends FxBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4644645822494144385L;

	// 货币互换信息
	private CcsInfoBean ccsInfoBean;

	// 本方利率详细信息
	private CcsDetailBean ccsDetailBean;

	// 对收费利率信息
	private CcsDetailBean contraPatryCcsDetailBean;
	
	//远端本方清算信息
	private FxSettBean farSettlement;

	//远端对手方清算信息
	private FxSettBean farCounterPartySettlement;

	public CcsInfoBean getCcsInfo() {
		if (null == ccsInfoBean) {
			ccsInfoBean = new CcsInfoBean();
		}
		return ccsInfoBean;
	}

	public CcsInfoBean getCcsInfoBean() {
		return ccsInfoBean;
	}

	public void setCcsInfoBean(CcsInfoBean ccsInfoBean) {
		this.ccsInfoBean = ccsInfoBean;
	}

	public CcsDetailBean getCcsDetailBean() {
		if(ccsDetailBean == null){
			ccsDetailBean = new CcsDetailBean();
		}
		return ccsDetailBean;
	}
	
	/**
	 * 获取远端本方清算信息
	 * @return
	 */
	public FxSettBean getFarSettlement() {
		if(null == farSettlement){
			farSettlement = new FxSettBean();
		}
		return farSettlement;
	}

	/**
	 * 设置远端本方清算信息
	 * @param farSettlement
	 */
	public void setFarSettlement(FxSettBean farSettlement) {
		this.farSettlement = farSettlement;
	}

	/**
	 * 获取远端对手方清算信息
	 * @return
	 */
	public FxSettBean getFarCounterPartySettlement() {
		if(null == farCounterPartySettlement){
			farCounterPartySettlement = new FxSettBean();
		}
		return farCounterPartySettlement;
	}

	/**
	 * 设置远端对手方清算信息
	 * @param farCounterPartySettlement
	 */
	public void setFarCounterPartySettlement(FxSettBean farCounterPartySettlement) {
		this.farCounterPartySettlement = farCounterPartySettlement;
	}

	public void setCcsDetailBean(CcsDetailBean ccsDetailBean) {
		this.ccsDetailBean = ccsDetailBean;
	}

	public CcsDetailBean getContraPatryCcsDetailBean() {
		if(contraPatryCcsDetailBean == null){
			contraPatryCcsDetailBean = new CcsDetailBean();
		}
		return contraPatryCcsDetailBean;
	}

	public void setContraPatryCcsDetailBean(CcsDetailBean contraPatryCcsDetailBean) {
		this.contraPatryCcsDetailBean = contraPatryCcsDetailBean;
	}
	

}
