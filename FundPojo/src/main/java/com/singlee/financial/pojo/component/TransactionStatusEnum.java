package com.singlee.financial.pojo.component;

/**
 * 交易处理状态
 * 
 * @author chenxh
 * 
 *         0-Deal entry,1-Deal modification,2-deal cancellation,3-Supplementary Contract Modification,4-Supplementary Contract Cancellation,5-Supplementary Contract Cancellation by MSC,6-Supplementary
 *         Contract Cancellation by Trader,7-Deal Modification on CDC Rejection,8-Limit Info Check,9-Limit Info Check Response,A-Execution Query Response,B-Recorded Deal Modification,C-Recorded Deal
 *         Cancellation,D-Recorded Deal Passed,E-Deal Stopped,R-Opposite Direction,M-Settled,F-EmergencyInsert,G-EmergencyCancel
 */
public enum TransactionStatusEnum {

	/**
	 * 录入
	 */
	Entry("0"),

	/**
	 * 修改
	 */
	Modify("1"),
	/**
	 * 撤销
	 */
	Back("2"),

	/**
	 * 应急录入
	 */
	EmergencyEntry("F"),

	/**
	 * 应急删除
	 */
	EmergencyDelete("G"),
	/**
	 * 补录
	 */
	Insert("M");

	private String index;

	TransactionStatusEnum(String index) {
		this.index = index;
	}
	@Override
	public String toString() {
		return this.index;
	}
}
