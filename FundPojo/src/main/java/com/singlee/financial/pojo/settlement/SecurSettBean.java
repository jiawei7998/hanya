package com.singlee.financial.pojo.settlement;

import java.util.List;

/**
 * 债券清算信息实体对象
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class SecurSettBean extends SettBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7780197610369647383L;

	/**
	 * 托管账户名称
	 */
	private String trustAcctName;
	
	/**
	 * 托管账户号
	 */
	private String trustAcctNo;
	
	/**
	 * 托管机构Id
	 */
	private String trustInstitutionId;

	/**
	 *质押式跨托管机构 list
	 */
	private List<SecurSettBean> SecurSettList;
	
	/**
	 * 获取债券托管账户名称
	 * @return
	 */
	public String getTrustAcctName() {
		return trustAcctName;
	}

	/**
	 * 设置债券托管账户名称
	 * @param trustAcctName
	 */
	public void setTrustAcctName(String trustAcctName) {
		this.trustAcctName = trustAcctName;
	}

	/**
	 * 获取债券托管账户号
	 * @return
	 */
	public String getTrustAcctNo() {
		return trustAcctNo;
	}

	/**
	 * 设置债券托管账户号
	 * @param trustAcctNo
	 */
	public void setTrustAcctNo(String trustAcctNo) {
		this.trustAcctNo = trustAcctNo;
	}

	/**
	 * 获取债券托管机构ID
	 * @return
	 */
	public String getTrustInstitutionId() {
		return trustInstitutionId;
	}

	/**
	 * 设置债券托管机构ID
	 * @param trustInstitutionId
	 */
	public void setTrustInstitutionId(String trustInstitutionId) {
		this.trustInstitutionId = trustInstitutionId;
	}

	public List<SecurSettBean> getSecurSettList() {
		return SecurSettList;
	}

	public void setSecurSettList(List<SecurSettBean> securSettList) {
		SecurSettList = securSettList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SecurSettBean [trustAcctName=");
		builder.append(trustAcctName);
		builder.append(", trustAcctNo=");
		builder.append(trustAcctNo);
		builder.append(", trustInstitutionId=");
		builder.append(trustInstitutionId);
		builder.append(", getAcctName()=");
		builder.append(getAcctName());
		builder.append(", getAcctNo()=");
		builder.append(getAcctNo());
		builder.append(", getBankAcctNo()=");
		builder.append(getBankAcctNo());
		builder.append(", getBankName()=");
		builder.append(getBankName());
		builder.append(", getMarkerTakerRole()=");
		builder.append(getMarkerTakerRole());
		builder.append(", getRemark()=");
		builder.append(getRemark());
		builder.append("]");
		return builder.toString();
	}
	
}
