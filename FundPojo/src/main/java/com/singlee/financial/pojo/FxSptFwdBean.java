package com.singlee.financial.pojo;

import com.singlee.financial.pojo.component.SptFwdTypeEnum;
import com.singlee.financial.pojo.settlement.FxSettBean;
import com.singlee.financial.pojo.trade.FxBaseBean;

/**
 * 外汇即、远期业务实体对象
 * 
 * @author chenxh
 * @version V2018-05-27
 * @since JDK1.6,Hessian4.0.37
 */
public class FxSptFwdBean extends FxBaseBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3782494589030959393L;

	// 
	private boolean isexdeltaindic;
	//是否为期权行权交易 false-否 ture-是
	private boolean isOpSpotIndic;
	//期权编号
	private String reference;
	//货币对
	private String ccypair;
	// 即远期标识
	private SptFwdTypeEnum sptFwdType;
	// 交易期限
	private String settlType;
	// 交易期限代码
	private String settlTypeCode;
	
	//交易类型（市场标识）
	private String marketIndicator;
	
	//本方对应货币清算信息
	private FxSettBean contraSettlement;

	//对手方对应货币清算信息
	private FxSettBean counterPartyContraSettlement;
	
	private String serverName;
	
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public FxSettBean getContraSettlement() {
		if(contraSettlement == null){
			contraSettlement = new FxSettBean();
		}
		return contraSettlement;
	}

	public void setContraSettlement(FxSettBean contraSettlement) {
		this.contraSettlement = contraSettlement;
	}

	public FxSettBean getCounterPartyContraSettlement() {
		if(counterPartyContraSettlement == null){
			counterPartyContraSettlement = new FxSettBean();
		}
		return counterPartyContraSettlement;
	}

	public void setCounterPartyContraSettlement(
			FxSettBean counterPartyContraSettlement) {
		this.counterPartyContraSettlement = counterPartyContraSettlement;
	}

	public String getMarketIndicator() {
		return marketIndicator;
	}

	public void setMarketIndicator(String marketIndicator) {
		this.marketIndicator = marketIndicator;
	}

	/**
	 * 获取即远期交易标识
	 * 
	 * @return
	 */
	public SptFwdTypeEnum getSptFwdType() {
		return sptFwdType;
	}

	/**
	 * 设置即远期交易标识
	 * 
	 * @param sptFwdType
	 */
	public void setSptFwdType(SptFwdTypeEnum sptFwdType) {
		this.sptFwdType = sptFwdType;
	}

	/**
	 * 获取期权行权交易示识,期权行权交易标识 false-否 ture-是
	 * 
	 * @return
	 */
	public boolean isIsexdeltaindic() {
		return isexdeltaindic;
	}

	/**
	 * 设置期权行权交易示识,期权行权交易标识 false-否 ture-是
	 * 
	 * @param isexdeltaindic
	 */
	public void setIsexdeltaindic(boolean isexdeltaindic) {
		this.isexdeltaindic = isexdeltaindic;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSettlType() {
		return settlType;
	}

	public void setSettlType(String settlType) {
		this.settlType = settlType;
	}

	public boolean isOpSpotIndic() {
		return isOpSpotIndic;
	}

	public void setOpSpotIndic(boolean isOpSpotIndic) {
		this.isOpSpotIndic = isOpSpotIndic;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getCcypair() {
		return ccypair;
	}

	public void setCcypair(String ccypair) {
		this.ccypair = ccypair;
	}

	public String getSettlTypeCode() {
		return settlTypeCode;
	}

	public void setSettlTypeCode(String settlTypeCode) {
		this.settlTypeCode = settlTypeCode;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FxSptFwdBean [isexdeltaindic=");
		builder.append(isexdeltaindic);
		builder.append(", sptFwdType=");
		builder.append(sptFwdType);
		builder.append(", getCounterPartySettlement()=");
		builder.append(getCounterPartySettlement());
		builder.append(", getCurrencyInfo()=");
		builder.append(getCurrencyInfo());
		builder.append(", getSettlement()=");
		builder.append(getSettlement());
		builder.append(", getContraPatryInstitutionInfo()=");
		builder.append(getContraPatryInstitutionInfo());
		builder.append(", getDealDate()=");
		builder.append(getDealDate());
		builder.append(", getDealDateTime()=");
		builder.append(getDealDateTime());
		builder.append(", getDealMode()=");
		builder.append(getDealMode());
		builder.append(", getDealStatus()=");
		builder.append(getDealStatus());
		builder.append(", getExecId()=");
		builder.append(getExecId());
		builder.append(", getExecIdToFedealno()=");
		builder.append(getExecIdToFedealno());
		builder.append(", getInstitutionInfo()=");
		builder.append(getInstitutionInfo());
		builder.append(", getPs()=");
		builder.append(getPs());
		builder.append(", getSettMode()=");
		builder.append(getSettMode());
		builder.append(", getTradeTime()=");
		builder.append(getTradeTime());
		builder.append(", getValueDate()=");
		builder.append(getValueDate());
		builder.append("]");
		return builder.toString();
	}

}
