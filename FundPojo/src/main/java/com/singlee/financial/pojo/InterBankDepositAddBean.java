package com.singlee.financial.pojo;


import java.math.BigDecimal;

/*
 *
 * 同业存款
 *
 */
public class InterBankDepositAddBean extends InterBankDepositBean {

    private static final long serialVersionUID = 1L;

    //附加协议类型
    private String supplType;
    //操作类型
    private String supplTransType;

    //提前支取编号
    private String advanceId;
    //
    private String advanceInfoType;
    //提前支取状态
    private String advanceStatus;
    //提前支取申请业务发生时间
    private String advanveBusinessTime;
    //提前支取日
    private String advanceDate;
    //提前支取本金
    private BigDecimal advanceAmt;
    //提前支取补偿类型
    private String advanceCompensationType;
    //提前支取利率
    private BigDecimal advancePrice;
    //提前支取补偿金额
    private BigDecimal advanceLastQty;
    //提前支取应计利息
    private BigDecimal advanceInterestAmt;
    //提前支取结算金额
    private BigDecimal advanceSettlAmt;
    //提前支取后剩余存款本金
    private BigDecimal advanceLeavesAmt;
    //提前支取后到期支取金额
    private BigDecimal advanceExpireAmt;
    //提前支取条款
    private String advanceClause;
    //
    private String advanceAttachmentIndicator;
    //提前支取存出方交易员
    private String advanceTraderName2;
    //提前支取存入方交易员
    private String advanceTraderName1;


    //补充定期账户编号
    private String supId;
    //补充定期账户状态
    private String supStatus;
    //补充定期账户业务发生时间
    private String supBusinessTime;
    //补充定期账户交易员姓名
    private String supTradeName;
    //补充定期账户资金开户行名称
    private String supCapitalBankName;
    //补充定期账户资金账户户名
    private String supCapitalAccountName;
    //补充定期账户资金账号
    private String supCapitalAccountNumber;
    //补充定期账户支付系统行号
    private String supPaymentSystemCode;


    public String getSupplType() {
        return supplType;
    }

    public void setSupplType(String supplType) {
        this.supplType = supplType;
    }

    public String getSupplTransType() {
        return supplTransType;
    }

    public void setSupplTransType(String supplTransType) {
        this.supplTransType = supplTransType;
    }

    public String getAdvanceId() {
        return advanceId;
    }

    public void setAdvanceId(String advanceId) {
        this.advanceId = advanceId;
    }

    public String getAdvanceInfoType() {
        return advanceInfoType;
    }

    public void setAdvanceInfoType(String advanceInfoType) {
        this.advanceInfoType = advanceInfoType;
    }

    public String getAdvanceStatus() {
        return advanceStatus;
    }

    public void setAdvanceStatus(String advanceStatus) {
        this.advanceStatus = advanceStatus;
    }

    public String getAdvanveBusinessTime() {
        return advanveBusinessTime;
    }

    public void setAdvanveBusinessTime(String advanveBusinessTime) {
        this.advanveBusinessTime = advanveBusinessTime;
    }

    public String getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(String advanceDate) {
        this.advanceDate = advanceDate;
    }

    public BigDecimal getAdvanceAmt() {
        return advanceAmt;
    }

    public void setAdvanceAmt(BigDecimal advanceAmt) {
        this.advanceAmt = advanceAmt;
    }

    public String getAdvanceCompensationType() {
        return advanceCompensationType;
    }

    public void setAdvanceCompensationType(String advanceCompensationType) {
        this.advanceCompensationType = advanceCompensationType;
    }

    public BigDecimal getAdvancePrice() {
        return advancePrice;
    }

    public void setAdvancePrice(BigDecimal advancePrice) {
        this.advancePrice = advancePrice;
    }

    public BigDecimal getAdvanceLastQty() {
        return advanceLastQty;
    }

    public void setAdvanceLastQty(BigDecimal advanceLastQty) {
        this.advanceLastQty = advanceLastQty;
    }

    public BigDecimal getAdvanceInterestAmt() {
        return advanceInterestAmt;
    }

    public void setAdvanceInterestAmt(BigDecimal advanceInterestAmt) {
        this.advanceInterestAmt = advanceInterestAmt;
    }

    public BigDecimal getAdvanceSettlAmt() {
        return advanceSettlAmt;
    }

    public void setAdvanceSettlAmt(BigDecimal advanceSettlAmt) {
        this.advanceSettlAmt = advanceSettlAmt;
    }

    public BigDecimal getAdvanceLeavesAmt() {
        return advanceLeavesAmt;
    }

    public void setAdvanceLeavesAmt(BigDecimal advanceLeavesAmt) {
        this.advanceLeavesAmt = advanceLeavesAmt;
    }

    public BigDecimal getAdvanceExpireAmt() {
        return advanceExpireAmt;
    }

    public void setAdvanceExpireAmt(BigDecimal advanceExpireAmt) {
        this.advanceExpireAmt = advanceExpireAmt;
    }

    public String getAdvanceClause() {
        return advanceClause;
    }

    public void setAdvanceClause(String advanceClause) {
        this.advanceClause = advanceClause;
    }

    public String getAdvanceAttachmentIndicator() {
        return advanceAttachmentIndicator;
    }

    public void setAdvanceAttachmentIndicator(String advanceAttachmentIndicator) {
        this.advanceAttachmentIndicator = advanceAttachmentIndicator;
    }

    public String getAdvanceTraderName2() {
        return advanceTraderName2;
    }

    public void setAdvanceTraderName2(String advanceTraderName2) {
        this.advanceTraderName2 = advanceTraderName2;
    }

    public String getAdvanceTraderName1() {
        return advanceTraderName1;
    }

    public void setAdvanceTraderName1(String advanceTraderName1) {
        this.advanceTraderName1 = advanceTraderName1;
    }

    public String getSupId() {
        return supId;
    }

    public void setSupId(String supId) {
        this.supId = supId;
    }

    public String getSupStatus() {
        return supStatus;
    }

    public void setSupStatus(String supStatus) {
        this.supStatus = supStatus;
    }

    public String getSupBusinessTime() {
        return supBusinessTime;
    }

    public void setSupBusinessTime(String supBusinessTime) {
        this.supBusinessTime = supBusinessTime;
    }

    public String getSupTradeName() {
        return supTradeName;
    }

    public void setSupTradeName(String supTradeName) {
        this.supTradeName = supTradeName;
    }

    public String getSupCapitalBankName() {
        return supCapitalBankName;
    }

    public void setSupCapitalBankName(String supCapitalBankName) {
        this.supCapitalBankName = supCapitalBankName;
    }

    public String getSupCapitalAccountName() {
        return supCapitalAccountName;
    }

    public void setSupCapitalAccountName(String supCapitalAccountName) {
        this.supCapitalAccountName = supCapitalAccountName;
    }

    public String getSupCapitalAccountNumber() {
        return supCapitalAccountNumber;
    }

    public void setSupCapitalAccountNumber(String supCapitalAccountNumber) {
        this.supCapitalAccountNumber = supCapitalAccountNumber;
    }

    public String getSupPaymentSystemCode() {
        return supPaymentSystemCode;
    }

    public void setSupPaymentSystemCode(String supPaymentSystemCode) {
        this.supPaymentSystemCode = supPaymentSystemCode;
    }
}
