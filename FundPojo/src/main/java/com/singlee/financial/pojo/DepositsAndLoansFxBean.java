package com.singlee.financial.pojo;

import com.singlee.financial.pojo.component.BasisEnum;
import com.singlee.financial.pojo.trade.FxBaseBean;

import java.math.BigDecimal;
import java.util.Date;

public class DepositsAndLoansFxBean extends FxBaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1081120236919435344L;
	// 到日期
	private Date Matuitydate;
	// 拆借期限
	private String settlType;
	// 应计利息
	private BigDecimal accruedInterestTotal;
	// 实际占款天数
	private String cashHoldingDays;
	/**
	 * 计息基准 0-Actual divide Actual, 1-ACT DIVIDE 360, 2-Thirty divide 360, 3-A
	 * DIVIDE 365, 4-ISMA30 DIVIDE 360, 5-A DIVIDE 365F, 6-Thirty DIVIDE 365,
	 * 7-Thirty DIVIDE ACT, 8-Thirty DIVIDE 366, 9-Rang ACT DIVIDE ACT, A-Rang
	 */
	private BasisEnum basis;
	// 到期还款金额
	private BigDecimal settlCurrAmt2;

	public String getSettlType() {
		return settlType;
	}

	public void setSettlType(String settlType) {
		this.settlType = settlType;
	}

	public Date getMatuitydate() {
		return Matuitydate;
	}

	public void setMatuitydate(Date matuitydate) {
		Matuitydate = matuitydate;
	}

	public BigDecimal getAccruedInterestTotal() {
		return accruedInterestTotal;
	}

	public void setAccruedInterestTotal(BigDecimal accruedInterestTotal) {
		this.accruedInterestTotal = accruedInterestTotal;
	}

	public String getCashHoldingDays() {
		return cashHoldingDays;
	}

	public void setCashHoldingDays(String cashHoldingDays) {
		this.cashHoldingDays = cashHoldingDays;
	}

	public BasisEnum getBasis() {
		return basis;
	}

	public void setBasis(BasisEnum basis) {
		this.basis = basis;
	}

	public BigDecimal getSettlCurrAmt2() {
		return settlCurrAmt2;
	}

	public void setSettlCurrAmt2(BigDecimal settlCurrAmt2) {
		this.settlCurrAmt2 = settlCurrAmt2;
	}

}
