package com.singlee.financial.disruptor;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;

/**
 * 消费者抽象类
 * 
 * @author shenzl
 *
 * @param <T>
 */
public abstract class DisruptorConsumer<T> implements EventHandler<ObjectEvent<T>>, WorkHandler<ObjectEvent<T>> {
	public DisruptorConsumer() {
	}
	@Override
	public void onEvent(ObjectEvent<T> event, long sequence, boolean endOfBatch) throws Exception {
		this.onEvent(event);
	}
	@Override
	public void onEvent(ObjectEvent<T> event) throws Exception {
		this.consume(event.getObj());
	}

	public abstract void consume(T var1);
}
