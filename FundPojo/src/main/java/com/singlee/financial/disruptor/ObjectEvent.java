package com.singlee.financial.disruptor;

/**
 * 事件对象
 * 
 * @author shenzl
 *
 * @param <T>
 */
public class ObjectEvent<T> {
	private T obj;

	public ObjectEvent() {
	}

	public T getObj() {
		return this.obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}
}
