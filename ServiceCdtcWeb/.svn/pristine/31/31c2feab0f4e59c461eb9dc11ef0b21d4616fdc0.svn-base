package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UtcTimeStampField extends DateField
{
	  private static final long serialVersionUID = 1L;
	  private boolean includeMilliseconds = true;

	  private static TimeZone timezone = TimeZone.getTimeZone("UTC");
	  private static Calendar calendar;

	  public UtcTimeStampField(String field)
	  {
	    super(field, UtcTimestampConverter.convert(createDate(), true));
	  }

	  protected UtcTimeStampField(String field, Date data)
	  {
	    super(field, UtcTimestampConverter.convert(data, true));
	  }

	  protected UtcTimeStampField(String field, String data)
	  {
	    super(field, data);
	  }

	  public UtcTimeStampField(String field, boolean includeMilliseconds)
	  {
	    super(field, UtcTimestampConverter.convert(createDate(), includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected UtcTimeStampField(String field, Date data, boolean includeMilliseconds)
	  {
	    super(field, UtcTimestampConverter.convert(data, includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected UtcTimeStampField(String field, String data, boolean includeMilliseconds)
	  {
	    super(field, data);
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  boolean showMilliseconds() {
	    return this.includeMilliseconds;
	  }

	  public void setValue(Date value) {
	    setValue(UtcTimestampConverter.convert(value, true));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(UtcTimestampConverter.convert(value, true));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }
	}