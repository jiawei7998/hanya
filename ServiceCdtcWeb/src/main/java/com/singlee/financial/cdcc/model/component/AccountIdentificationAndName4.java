package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 帐户4组件
 */
public class AccountIdentificationAndName4 implements Serializable {
    /**
     * 账户名称
     */
    private String Nm;
    /**
     * 账户编号
     */
    private String Id;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
