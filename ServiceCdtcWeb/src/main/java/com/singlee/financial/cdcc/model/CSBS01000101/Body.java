package com.singlee.financial.cdcc.model.CSBS01000101;

import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.Extension;

import java.util.List;


/**
 * 请求报文体
 * @author copysun
 */
public class Body extends EsbBodyPacket {

	/**
	 * 报文标识1.0
	 */
	private Msg Msg;

	/**
	 * 业务消息通知2.0
	 */
	private BizInfNtce BizInfNtce;

	/**
	 * 扩展项3.0
	 */
	private List<Extension> Xtnsn;


	public Msg getMsg() {
		return Msg;
	}

	public void setMsg(Msg msg) {
		Msg = msg;
	}

	public BizInfNtce getBizInfNtce() {
		return BizInfNtce;
	}

	public void setBizInfNtce(BizInfNtce bizInfNtce) {
		BizInfNtce = bizInfNtce;
	}

	public List<Extension> getXtnsn() {
		return Xtnsn;
	}

	public void setXtnsn(List<Extension> xtnsn) {
		Xtnsn = xtnsn;
	}

	@Override
	public EsbSubPacketConvert getBodyConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		info.setSubPacketAliasName("Document");
		info.setSubPacketClassType(Body.class);
		// 处理子标签包含集合的内容
		info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
		return info;
	}
}
