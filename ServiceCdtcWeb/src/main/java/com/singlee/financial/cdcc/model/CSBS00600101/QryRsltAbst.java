package com.singlee.financial.cdcc.model.CSBS00600101;

import java.io.Serializable;

/**
 * 查询结果摘要
 *
 * @Auther:zk
 */
public class QryRsltAbst implements Serializable {

    /**
     *查询处理结果返回码
     */
    private String TxRsltCd;

    /**
     *查询结果计数
     */
    private String QryRsltCnt;

    public String getTxRsltCd() {
        return TxRsltCd;
    }

    public void setTxRsltCd(String txRsltCd) {
        TxRsltCd = txRsltCd;
    }

    public String getQryRsltCnt() {
        return QryRsltCnt;
    }

    public void setQryRsltCnt(String qryRsltCnt) {
        QryRsltCnt = qryRsltCnt;
    }
}
