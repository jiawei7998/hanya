package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;
import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 15:07
 * =======================
 */
public class SummaryStatement implements Serializable {
    /**
     * 日期
     */
    private String Dt;

    /**
     * 科目余额
     */
    private List<AccountLedger> AcctLdg;

    public String getDt() {
        return Dt;
    }

    public void setDt(String dt) {
        Dt = dt;
    }

    public List<AccountLedger> getAcctLdg() {
        return AcctLdg;
    }

    public void setAcctLdg(List<AccountLedger> acctLdg) {
        AcctLdg = acctLdg;
    }
}
