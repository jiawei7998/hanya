package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * @author Liang
 * @date 2021/11/27 15:08
 * =======================
 */
public class AccountLedger implements Serializable {
    /**
     * 科目类型
     */
    private String LdgTp;
    /**
     * 科目余额
     */
    private String Bal;

    public String getLdgTp() {
        return LdgTp;
    }

    public void setLdgTp(String ldgTp) {
        LdgTp = ldgTp;
    }

    public String getBal() {
        return Bal;
    }

    public void setBal(String bal) {
        Bal = bal;
    }
}
