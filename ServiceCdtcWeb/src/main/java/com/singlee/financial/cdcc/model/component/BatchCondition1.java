package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 批量条件1
 * 适用的批量下载功能编号：
 * BF00
 * BF01
 * BF02
 * BF03
 * BF04
 * BF05
 * BF06
 * BF07
 * BF08
 * BF09
 */
public class BatchCondition1 implements Serializable {
    /**
     * 帐户编号
     */
    private String Id;
    /**
     * 开始日期
     */
    private String StrtDt;
    /**
     * 结束日期
     */
    private String EndDt;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getStrtDt() {
        return StrtDt;
    }

    public void setStrtDt(String strtDt) {
        StrtDt = strtDt;
    }

    public String getEndDt() {
        return EndDt;
    }

    public void setEndDt(String endDt) {
        EndDt = endDt;
    }
}
