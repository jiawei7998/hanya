package com.singlee.financial.cdcc.model.CSBS01100101;

import com.singlee.financial.cdcc.model.component.GenericIdentification4;

import java.io.Serializable;

/**
 * 查询方
 *
 * @author copysun
 */
public class QryPty implements Serializable {

    /**
     * 名称
     */
    private String Nm;

    /**
     * 自定义标识
     */
    private GenericIdentification4 PrtryId;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public GenericIdentification4 getPrtryId() {
        return PrtryId;
    }

    public void setPrtryId(GenericIdentification4 prtryId) {
        PrtryId = prtryId;
    }
}
