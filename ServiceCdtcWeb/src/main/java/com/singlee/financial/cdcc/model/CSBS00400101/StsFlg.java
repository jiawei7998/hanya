package com.singlee.financial.cdcc.model.CSBS00400101;

import com.singlee.financial.cdcc.model.component.ContractStatusInfo;
import com.singlee.financial.cdcc.model.component.InstructionStatusInfo;
import com.singlee.financial.cdcc.model.component.PartyIdentification;

import java.io.Serializable;
import java.util.List;

/**
 * 状态标识
 */
public class StsFlg implements Serializable {

    /**
     * 指令状态信息
     */
    private InstructionStatusInfo InstrStsInf;
    /**
     * 合同状态信息
     */
    private List<ContractStatusInfo> CtrctStsInf;

    public InstructionStatusInfo getInstrStsInf() {
        return InstrStsInf;
    }

    public void setInstrStsInf(InstructionStatusInfo instrStsInf) {
        InstrStsInf = instrStsInf;
    }

    public List<ContractStatusInfo> getCtrctStsInf() {
        return CtrctStsInf;
    }

    public void setCtrctStsInf(List<ContractStatusInfo> ctrctStsInf) {
        CtrctStsInf = ctrctStsInf;
    }
}
