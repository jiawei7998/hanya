package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 批量查询组件
 */
public class BatchCondition implements Serializable {
    /**
     * 批量查询条件1
     */
    private BatchCondition1 BthCond1;

    /**
     * 批量查询条件2
     */
    private BatchCondition2 BthCond2;

    /**
     * 批量查询条件3
     */
    private BatchCondition3 BthCond3;

    /**
     * 批量查询条件1
     */
    private BatchCondition4 BthCond4;

    /**
     * 批量查询条件1
     */
    private BatchCondition5 BthCond5;

    public BatchCondition1 getBthCond1() {
        return BthCond1;
    }

    public void setBthCond1(BatchCondition1 bthCond1) {
        BthCond1 = bthCond1;
    }

    public BatchCondition2 getBthCond2() {
        return BthCond2;
    }

    public void setBthCond2(BatchCondition2 bthCond2) {
        BthCond2 = bthCond2;
    }

    public BatchCondition3 getBthCond3() {
        return BthCond3;
    }

    public void setBthCond3(BatchCondition3 bthCond3) {
        BthCond3 = bthCond3;
    }

    public BatchCondition4 getBthCond4() {
        return BthCond4;
    }

    public void setBthCond4(BatchCondition4 bthCond4) {
        BthCond4 = bthCond4;
    }

    public BatchCondition5 getBthCond5() {
        return BthCond5;
    }

    public void setBthCond5(BatchCondition5 bthCond5) {
        BthCond5 = bthCond5;
    }
}
