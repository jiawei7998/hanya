package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 拓展组件
 */
public class Extension implements Serializable {
    /**
     * 扩展字段
     */
    private String XtnsnTxt;

    public String getXtnsnTxt() {
        return XtnsnTxt;
    }

    public void setXtnsnTxt(String xtnsnTxt) {
        XtnsnTxt = xtnsnTxt;
    }
}
