package com.singlee.financial.cdcc.model.component;


import java.io.Serializable;
import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 15:00
 * =======================
 */
public class AccountIdentificationAndName5 implements Serializable {
    /**
     * 账户名称
     */
    private String Nm;
    /**
     * 账户编号
     */
    private String Id;
    /**
     * 债券数目
     */
    private String BdCnt;
    /**
     * 债券对账单
     */
    private List<Bond2> Bd;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getBdCnt() {
        return BdCnt;
    }

    public void setBdCnt(String bdCnt) {
        BdCnt = bdCnt;
    }

    public List<Bond2> getBd() {
        return Bd;
    }

    public void setBd(List<Bond2> bd) {
        Bd = bd;
    }
}
