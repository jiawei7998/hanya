package com.singlee.financial.cdcc.model.CSBS00800101;


import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.*;

import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 14:04
 * =======================
 */
public class Body extends EsbBodyPacket {
    /**
     * 1.0  报文标识
     */
    private Msg Msg;
    /**
     * 2.0 对账报告
     */
    private StmtRpt StmtRpt;

    /**
     * 3.0 查询方
     */
    private QryPty QryPty;

    /**
     * 4.0 扩展项
     */
    private List<Extension> Xtnsn;

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg msg) {
        Msg = msg;
    }

    public StmtRpt getStmtRpt() {
        return StmtRpt;
    }

    public void setStmtRpt(StmtRpt stmtRpt) {
        StmtRpt = stmtRpt;
    }

    public QryPty getQryPty() {
        return QryPty;
    }

    public void setQryPty(QryPty qryPty) {
        QryPty = qryPty;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getBodyConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("Document");
        info.setSubPacketClassType(Body.class);
        // 处理子标签包含集合的内容
        info.addAlias(StmtRpt.class, "SfkpgAcctStmtInf", "SfkpgAcctStmtInf", AccountIdentificationAndName5.class);
        info.addAlias(AccountIdentificationAndName5.class, "Bd", "Bd", Bond2.class);

        info.addAlias(SummaryStatement.class, "AcctLdg", "AcctLdg", AccountLedger.class);

        info.addAlias(Bond2.class, "DtldStmt", "DtldStmt", DetailedStatement.class);
        info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
        return info;
    }
}
