package com.singlee.financial.cdcc.model.CSBS00600101;


import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.CSBS00100101.SttlmDtl;
import com.singlee.financial.cdcc.model.component.Bond3;
import com.singlee.financial.cdcc.model.component.Extension;

import java.io.Serializable;
import java.util.List;

/**
 * 请求体报文
 *
 * @Auther:zk
 */
public class Body extends EsbBodyPacket {
    /**
     *报文标识1.0
     */
    private Msg Msg;

    /**
     *查询结果摘要2.0
     */
    private QryRsltAbst QryRsltAbst;

    /**
     *批量查询结果3.0
     */
    private List<BtchQryRslt> BtchQryRslt;

    /**
     *查询方4.0
     */
    private QryPty QryPty;

    /**
     *扩展项5.0
     */
    private List<Extension> Xtnsn;

    public com.singlee.financial.cdcc.model.CSBS00600101.Msg getMsg() {
        return Msg;
    }

    public void setMsg(com.singlee.financial.cdcc.model.CSBS00600101.Msg msg) {
        Msg = msg;
    }

    public com.singlee.financial.cdcc.model.CSBS00600101.QryRsltAbst getQryRsltAbst() {
        return QryRsltAbst;
    }

    public void setQryRsltAbst(com.singlee.financial.cdcc.model.CSBS00600101.QryRsltAbst qryRsltAbst) {
        QryRsltAbst = qryRsltAbst;
    }

    public List<com.singlee.financial.cdcc.model.CSBS00600101.BtchQryRslt> getBtchQryRslt() {
        return BtchQryRslt;
    }

    public void setBtchQryRslt(List<com.singlee.financial.cdcc.model.CSBS00600101.BtchQryRslt> btchQryRslt) {
        BtchQryRslt = btchQryRslt;
    }

    public com.singlee.financial.cdcc.model.CSBS00600101.QryPty getQryPty() {
        return QryPty;
    }

    public void setQryPty(com.singlee.financial.cdcc.model.CSBS00600101.QryPty qryPty) {
        QryPty = qryPty;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getBodyConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("Document");
        info.setSubPacketClassType(Body.class);
        // 处理子标签包含集合的内容
        info.addAlias(Body.class, "BtchQryRslt", "BtchQryRslt", BtchQryRslt.class);
        info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
        return info;
    }
}
