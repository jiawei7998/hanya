package com.singlee.financial.cdcc.model.CSBS00500101;

import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.Condition;
import com.singlee.financial.cdcc.model.component.Extension;

import java.util.List;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/27 14:34
 * @description：报文
 * @modified By：
 * @version:
 */
public class Body extends EsbBodyPacket {
    //报文标识
    private Msg Msg;
    //操作标识
    private OprtrFlg OprtrFlg;
    //查询账户
    private AccountIdentificationAndName4 AcctQry;
    //查询条件
    private Condition QryCond;
    //操作标识
    private QryPty QryPty;
    //扩展项
    private List<Extension> Xtnsn;

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg msg) {
        Msg = msg;
    }

    public OprtrFlg getOprtrFlg() {
        return OprtrFlg;
    }

    public void setOprtrFlg(OprtrFlg oprtrFlg) {
        OprtrFlg = oprtrFlg;
    }

    public AccountIdentificationAndName4 getAcctQry() {
        return AcctQry;
    }

    public Condition getQryCond() {
        return QryCond;
    }

    public void setQryCond(Condition qryCond) {
        QryCond = qryCond;
    }

    public void setAcctQry(AccountIdentificationAndName4 acctQry) {
        AcctQry = acctQry;
    }

    public QryPty getQryPty() {
        return QryPty;
    }

    public void setQryPty(QryPty qryPty) {
        QryPty = qryPty;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getBodyConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("Document");
        info.setSubPacketClassType(Body.class);
        info.addAlias(Body.class, "Xtnsn", "Xtnsn",Extension.class );
        return info;
    }
}
