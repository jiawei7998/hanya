package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 应计利息
 */
public class AccruedInterestInformation implements Serializable {

    /**
     * 应计利息
     */
    private String AcrdIntrst;
    /**
     * 计息日
     */
    private String AcrdIntrstDt;

    public String getAcrdIntrst() {
        return AcrdIntrst;
    }

    public void setAcrdIntrst(String acrdIntrst) {
        AcrdIntrst = acrdIntrst;
    }

    public String getAcrdIntrstDt() {
        return AcrdIntrstDt;
    }

    public void setAcrdIntrstDt(String acrdIntrstDt) {
        AcrdIntrstDt = acrdIntrstDt;
    }
}
