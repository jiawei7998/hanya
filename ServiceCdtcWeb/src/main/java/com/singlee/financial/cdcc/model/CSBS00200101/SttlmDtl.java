package com.singlee.financial.cdcc.model.CSBS00200101;

import com.singlee.financial.cdcc.model.component.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/27 14:26
 * @description：结算详情
 * @modified By：
 * @version:
 */
public class SttlmDtl implements Serializable {
    /**
     * 结算指令来源
     */
    private String InstrOrgn;
    /**
     * 业务类别
     */
    private String BizTp;
    /**
     * 结算指令标识
     */
    private String InstrId;
    /**
     * 付券方账户
     */
    private AccountIdentificationAndName4 GivAcct;
    /**
     * 收券方账户
     */
    private AccountIdentificationAndName4 TakAcct;
    /**
     * 结算方式1
     */
    private String SttlmTp1;
    /**
     * 结算方式2
     */
    private String SttlmTp2;
    /**
     * 债券1
     */
    private List<Bond3> Bd1;
    /**
     * 债券2
     */
    private List<Bond3> Bd2;
    /**
     * 金额1
     */
    private String Val1;
    /**
     * 金额2
     */
    private String Val2;
    /**
     * 金额3
     */
    private String Val3;
    /**
     * 金额4
     */
    private String Val4;
    /**
     * 券面总额合计
     */
    private String AggtFaceAmt;
    /**
     * 利率
     */
    private String Rate;
    /**
     * 期限
     */
    private String Terms;
    /**
     * 日期1
     */
    private String Dt1;
    /**
     * 日期2
     */
    private String Dt2;
    /**
     * 结算担保1
     */
    private SettlementGuarantee SttlmGrte1;
    /**
     * 结算担保2
     */
    private SettlementGuarantee SttlmGrte2;
    /**
     * 原结算指令标识
     */
    private String OrgnlInstrId;
    /**
     * 原结算合同标识
     */
    private String OrgnlCtrctId;
    /**
     * 转托管申请人
     */
    private PartyIdentification34 CrossTrfAplt;
    /**
     * 业务标识号
     */
    private String TxId;
    /**
     * 经办人
     */
    private PartyIdentification Oprtr;
    /**
     * 复核人
     */
    private PartyIdentification Chckr;
    /**
     * 确认人
     */
    private PartyIdentification Cnfrmr;

    public String getInstrOrgn() {
        return InstrOrgn;
    }

    public void setInstrOrgn(String instrOrgn) {
        InstrOrgn = instrOrgn;
    }

    public String getBizTp() {
        return BizTp;
    }

    public void setBizTp(String bizTp) {
        BizTp = bizTp;
    }

    public String getInstrId() {
        return InstrId;
    }

    public void setInstrId(String instrId) {
        InstrId = instrId;
    }

    public AccountIdentificationAndName4 getGivAcct() {
        return GivAcct;
    }

    public void setGivAcct(AccountIdentificationAndName4 givAcct) {
        GivAcct = givAcct;
    }

    public AccountIdentificationAndName4 getTakAcct() {
        return TakAcct;
    }

    public void setTakAcct(AccountIdentificationAndName4 takAcct) {
        TakAcct = takAcct;
    }

    public String getSttlmTp1() {
        return SttlmTp1;
    }

    public void setSttlmTp1(String sttlmTp1) {
        SttlmTp1 = sttlmTp1;
    }

    public String getSttlmTp2() {
        return SttlmTp2;
    }

    public void setSttlmTp2(String sttlmTp2) {
        SttlmTp2 = sttlmTp2;
    }

    public List<Bond3> getBd1() {
        return Bd1;
    }

    public void setBd1(List<Bond3> bd1) {
        Bd1 = bd1;
    }

    public List<Bond3> getBd2() {
        return Bd2;
    }

    public void setBd2(List<Bond3> bd2) {
        Bd2 = bd2;
    }

    public String getVal1() {
        return Val1;
    }

    public void setVal1(String val1) {
        Val1 = val1;
    }

    public String getVal2() {
        return Val2;
    }

    public void setVal2(String val2) {
        Val2 = val2;
    }

    public String getVal3() {
        return Val3;
    }

    public void setVal3(String val3) {
        Val3 = val3;
    }

    public String getVal4() {
        return Val4;
    }

    public void setVal4(String val4) {
        Val4 = val4;
    }

    public String getAggtFaceAmt() {
        return AggtFaceAmt;
    }

    public void setAggtFaceAmt(String aggtFaceAmt) {
        AggtFaceAmt = aggtFaceAmt;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public String getTerms() {
        return Terms;
    }

    public void setTerms(String terms) {
        Terms = terms;
    }

    public String getDt1() {
        return Dt1;
    }

    public void setDt1(String dt1) {
        Dt1 = dt1;
    }

    public String getDt2() {
        return Dt2;
    }

    public void setDt2(String dt2) {
        Dt2 = dt2;
    }

    public SettlementGuarantee getSttlmGrte1() {
        return SttlmGrte1;
    }

    public void setSttlmGrte1(SettlementGuarantee sttlmGrte1) {
        SttlmGrte1 = sttlmGrte1;
    }

    public SettlementGuarantee getSttlmGrte2() {
        return SttlmGrte2;
    }

    public void setSttlmGrte2(SettlementGuarantee sttlmGrte2) {
        SttlmGrte2 = sttlmGrte2;
    }

    public String getOrgnlInstrId() {
        return OrgnlInstrId;
    }

    public void setOrgnlInstrId(String orgnlInstrId) {
        OrgnlInstrId = orgnlInstrId;
    }

    public String getOrgnlCtrctId() {
        return OrgnlCtrctId;
    }

    public void setOrgnlCtrctId(String orgnlCtrctId) {
        OrgnlCtrctId = orgnlCtrctId;
    }

    public PartyIdentification34 getCrossTrfAplt() {
        return CrossTrfAplt;
    }

    public void setCrossTrfAplt(PartyIdentification34 crossTrfAplt) {
        CrossTrfAplt = crossTrfAplt;
    }

    public String getTxId() {
        return TxId;
    }

    public void setTxId(String txId) {
        TxId = txId;
    }

    public PartyIdentification getOprtr() {
        return Oprtr;
    }

    public void setOprtr(PartyIdentification oprtr) {
        Oprtr = oprtr;
    }

    public PartyIdentification getChckr() {
        return Chckr;
    }

    public void setChckr(PartyIdentification chckr) {
        Chckr = chckr;
    }

    public PartyIdentification getCnfrmr() {
        return Cnfrmr;
    }

    public void setCnfrmr(PartyIdentification cnfrmr) {
        Cnfrmr = cnfrmr;
    }

}