package com.singlee.financial.cdcc.model.header;

import java.io.Serializable;

/**
 * 报文接收方
 */
public class Rcvr implements Serializable {
    /**
     * 接收方标识
     */
    private String RcvrId;
    /**
     * 接收系统标识号
     */
    private String RcvrSysId;

    public String getRcvrId() {
        return RcvrId;
    }

    public void setRcvrId(String rcvrId) {
        RcvrId = rcvrId;
    }

    public String getRcvrSysId() {
        return RcvrSysId;
    }

    public void setRcvrSysId(String rcvrSysId) {
        RcvrSysId = rcvrSysId;
    }
}
