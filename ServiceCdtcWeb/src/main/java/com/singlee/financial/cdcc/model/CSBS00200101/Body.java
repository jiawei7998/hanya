package com.singlee.financial.cdcc.model.CSBS00200101;

import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.AgentParty;
import com.singlee.financial.cdcc.model.component.Bond3;
import com.singlee.financial.cdcc.model.component.ContractStatusInfo;
import com.singlee.financial.cdcc.model.component.Extension;

import java.util.List;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/27 14:07
 * @description：
 * @modified By：
 * @version:
 */
    public class Body extends EsbBodyPacket {
        /**
         * 报文标识1.0
         */
        private Msg Msg;
        /**
         * 操作标识2.0
         */
        private StsFlg StsFlg;

        /**
         * 结算详情3.0
         */
        private SttlmDtl SttlmDtl;
        /**
         * 付方信息4.0
         */
        private GivInf GivInf;
        /**
         * 收方信息5.0
         */
        private TakInf TakInf;
        /**
         * 扩展项6.0
         */
        private List<Extension> Xtnsn;

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg msg) {
        Msg = msg;
    }

    public StsFlg getStsFlg() {
        return StsFlg;
    }

    public void setStsFlg(StsFlg stsFlg) {
        StsFlg = stsFlg;
    }

    public SttlmDtl getSttlmDtl() {
        return SttlmDtl;
    }

    public void setSttlmDtl(SttlmDtl sttlmDtl) {
        SttlmDtl = sttlmDtl;
    }

    public GivInf getGivInf() {
        return GivInf;
    }

    public void setGivInf(GivInf givInf) {
        GivInf = givInf;
    }

    public TakInf getTakInf() {
        return TakInf;
    }

    public void setTakInf(TakInf takInf) {
        TakInf = takInf;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
        public EsbSubPacketConvert getBodyConvert() {
            EsbSubPacketConvert info = new EsbSubPacketConvert();
            info.setSubPacketAliasName("Document");
            info.setSubPacketClassType(Body.class);
            info.addAlias(SttlmDtl.class, "Bd1", "Bd1", Bond3.class);
            info.addAlias(SttlmDtl.class, "Bd2", "Bd2", Bond3.class);
            info.addAlias(StsFlg.class, "CtrctStsInf", "CtrctStsInf", ContractStatusInfo.class);
            info.addAlias(GivInf.class, "AgtPty", "AgtPty", AgentParty.class);
            info.addAlias(TakInf.class, "AgtPty", "AgtPty", AgentParty.class);
            info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
            return info;
        }
    }


