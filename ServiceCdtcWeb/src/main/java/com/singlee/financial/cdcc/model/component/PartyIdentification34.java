package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 转入转出组件
 */
public class PartyIdentification34 implements Serializable {

    /**
     * 名称
     */
    private String Nm;
    /**
     * 在转出方债券账号
     */
    private SafekeepingAccount AcctInTrfOut;
    /**
     * 在转入方债券账号
     */
    private SafekeepingAccount AcctInTrfIn;
    /**
     * 在转出方代理机构
     */
    private AgentParty AgtPtyInTrfOut;
    /**
     * 在转入方代理机构
     */
    private AgentParty AgtPtyInTrfIn;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public SafekeepingAccount getAcctInTrfOut() {
        return AcctInTrfOut;
    }

    public void setAcctInTrfOut(SafekeepingAccount acctInTrfOut) {
        AcctInTrfOut = acctInTrfOut;
    }

    public SafekeepingAccount getAcctInTrfIn() {
        return AcctInTrfIn;
    }

    public void setAcctInTrfIn(SafekeepingAccount acctInTrfIn) {
        AcctInTrfIn = acctInTrfIn;
    }

    public AgentParty getAgtPtyInTrfOut() {
        return AgtPtyInTrfOut;
    }

    public void setAgtPtyInTrfOut(AgentParty agtPtyInTrfOut) {
        AgtPtyInTrfOut = agtPtyInTrfOut;
    }

    public AgentParty getAgtPtyInTrfIn() {
        return AgtPtyInTrfIn;
    }

    public void setAgtPtyInTrfIn(AgentParty agtPtyInTrfIn) {
        AgtPtyInTrfIn = agtPtyInTrfIn;
    }
}
