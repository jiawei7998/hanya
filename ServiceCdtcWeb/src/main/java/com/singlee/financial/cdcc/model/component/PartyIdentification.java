package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 机构或角色的名称
 */
public class PartyIdentification implements Serializable {
    /**
     * 名称
     */
    private String Nm;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }
}
