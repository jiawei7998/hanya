package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 自定义标识
 */
public class GenericIdentification4 implements Serializable {
    /**
     * 标识号
     */
    private String Id;
    /**
     * 标识类型
     */
    private String IdTp;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getIdTp() {
        return IdTp;
    }

    public void setIdTp(String idTp) {
        IdTp = idTp;
    }
}
