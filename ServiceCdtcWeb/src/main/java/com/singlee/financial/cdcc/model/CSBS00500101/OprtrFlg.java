package com.singlee.financial.cdcc.model.CSBS00500101;

import java.io.Serializable;

/**
 * 操作标识
 */
public class OprtrFlg implements Serializable {
    /**
     * 请求类别码
     */
    private String ReqTp;
    /**
     * 操作码
     */
    private String OprtrSts;

    public String getReqTp() {
        return ReqTp;
    }

    public void setReqTp(String reqTp) {
        ReqTp = reqTp;
    }

    public String getOprtrSts() {
        return OprtrSts;
    }

    public void setOprtrSts(String oprtrSts) {
        OprtrSts = oprtrSts;
    }
}
