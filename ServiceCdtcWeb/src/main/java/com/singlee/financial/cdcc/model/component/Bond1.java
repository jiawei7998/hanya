package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

public class Bond1 implements Serializable {
    /**
     * 债券代码
     */
    private String BdId;
    /**
     * ISIN编码
     */
    private String ISIN;
    /**
     * 债券简称
     */
    private String BdShrtNm;

    public String getBdId() {
        return BdId;
    }

    public void setBdId(String bdId) {
        BdId = bdId;
    }

    public String getISIN() {
        return ISIN;
    }

    public void setISIN(String ISIN) {
        this.ISIN = ISIN;
    }

    public String getBdShrtNm() {
        return BdShrtNm;
    }

    public void setBdShrtNm(String bdShrtNm) {
        BdShrtNm = bdShrtNm;
    }
}
