package com.singlee.financial.cdcc.model.header;

import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.convert.EsbSysHeaderPacket;
import com.singlee.financial.cdcc.model.component.Extension;

import java.util.List;

/**
 * 定义中债报文头
 */
public class MsgHeader extends EsbSysHeaderPacket {
    /**
     * 报文版本标识
     */
    private Vrsn Vrsn;
    /**
     * 报文发起方
     */
    private Sndr Sndr;
    /**
     * 报文接收方
     */
    private Rcvr Rcvr;
    /**
     * 报文描述
     */
    private MsgDesc MsgDesc;
    /**
     * 报文签名
     */
    private Sgntr Sgntr;
    /**
     * 扩展项
     */
    private List<Extension> Xtnsn;

    public Vrsn getVrsn() {
        return Vrsn;
    }

    public void setVrsn(Vrsn vrsn) {
        Vrsn = vrsn;
    }

    public Sndr getSndr() {
        return Sndr;
    }

    public void setSndr(Sndr sndr) {
        Sndr = sndr;
    }

    public Rcvr getRcvr() {
        return Rcvr;
    }

    public void setRcvr(Rcvr rcvr) {
        Rcvr = rcvr;
    }

    public MsgDesc getMsgDesc() {
        return MsgDesc;
    }

    public void setMsgDesc(MsgDesc msgDesc) {
        MsgDesc = msgDesc;
    }

    public Sgntr getSgntr() {
        return Sgntr;
    }

    public void setSgntr(Sgntr sgntr) {
        Sgntr = sgntr;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getSysHeaderConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("MsgHeader");
        info.setSubPacketClassType(MsgHeader.class);
        // 处理子标签包含集合的内容
        info.addAlias(Sgntr.class, "SttlmMmbSgntr", "SttlmMmbSgntr", String.class);
        info.addAlias(Sgntr.class, "CSBSSgntr", "CSBSSgntr", String.class);
        info.addAlias(MsgHeader.class, "Xtnsn", "Xtnsn", Extension.class);
        return info;
    }
}
