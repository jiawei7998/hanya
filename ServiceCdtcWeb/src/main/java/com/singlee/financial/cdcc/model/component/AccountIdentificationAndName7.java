package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

public class AccountIdentificationAndName7 implements Serializable {

    /**
     * 账户名称
     */
    private String Nm;

    /**
     * 账户编号
     */
    private String Id;

    /**
     * 账户对账单类型
     */
    private String AccStTp;


    /**
     * 查询债券
     */
    private Bond1 QryBd;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getAccStTp() {
        return AccStTp;
    }

    public void setAccStTp(String accStTp) {
        AccStTp = accStTp;
    }

    public Bond1 getQryBd() {
        return QryBd;
    }

    public void setQryBd(Bond1 qryBd) {
        QryBd = qryBd;
    }
}
