package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 抵押品组件
 */
public class CollateralBond1 implements Serializable {
    /**
     * 债券代码
     */
    private String BdId;
    /**
     * ISIN编码
     */
    private String ISIN;
    /**
     * 债券简称
     */
    private String BdShrtNm;
    /**
     * 债券质押率
     */
    private String BdCollRate;
    /**
     * 券面总额
     */
    private String BdAmt;

    public String getBdId() {
        return BdId;
    }

    public void setBdId(String bdId) {
        BdId = bdId;
    }

    public String getISIN() {
        return ISIN;
    }

    public void setISIN(String ISIN) {
        this.ISIN = ISIN;
    }

    public String getBdShrtNm() {
        return BdShrtNm;
    }

    public void setBdShrtNm(String bdShrtNm) {
        BdShrtNm = bdShrtNm;
    }

    public String getBdCollRate() {
        return BdCollRate;
    }

    public void setBdCollRate(String bdCollRate) {
        BdCollRate = bdCollRate;
    }

    public String getBdAmt() {
        return BdAmt;
    }

    public void setBdAmt(String bdAmt) {
        BdAmt = bdAmt;
    }
}
