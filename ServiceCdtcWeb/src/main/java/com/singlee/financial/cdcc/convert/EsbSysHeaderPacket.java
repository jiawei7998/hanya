package com.singlee.financial.cdcc.convert;

import java.io.Serializable;

/**
 * Esb系统头报文抽象类
 * @author chenxh
 *
 */
public abstract class EsbSysHeaderPacket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 793201452739313148L;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * 组装系统头的节点转换信息
	 * @return
	 */
	public abstract EsbSubPacketConvert getSysHeaderConvert();
}
