package com.singlee.financial.cdcc.model.CSBS00900101;

import java.io.Serializable;

/**
 * @author Liang
 * @date 2021/11/27 14:13
 * =======================
 */
public class SysXcptn implements Serializable {

    /**
     * 异常返回代码
     */
    private String XcptnRtrCd;

    /**
     * 异常返回描述
     */
    private String XcptnRtrTx;


    public String getXcptnRtrCd() {
        return XcptnRtrCd;
    }

    public void setXcptnRtrCd(String xcptnRtrCd) {
        XcptnRtrCd = xcptnRtrCd;
    }

    public String getXcptnRtrTx() {
        return XcptnRtrTx;
    }

    public void setXcptnRtrTx(String xcptnRtrTx) {
        XcptnRtrTx = xcptnRtrTx;
    }
}
