package com.singlee.financial.cdcc.model.CSBS00400101;

import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.CashSettlementDelivery;
import com.singlee.financial.cdcc.model.component.PartyIdentification;

import java.io.Serializable;
import java.util.List;

/**
 * 报文标识
 */
public class CmonSbsdryItm implements Serializable {
    /**
     * 结算指令来源
     */
    private String InstrOrgn;
    /**
     * 辅助类型
     */
    private String SbryTp;
    /**
     * 结算指令标识
     */
    private String InstrId;
    /**
     * 结算合同标识
     */
    private List<String> CtrctId;
    /**
     * 付券方账户
     */
    private AccountIdentificationAndName4 GivAcct;
    /**
     * 收券方账户
     */
    private AccountIdentificationAndName4 TakAcct;
    /**
     * 原结算指令标识
     */
    private String OrgnlInstrId;
    /**
     * 原结算合同标识
     */
    private String OrgnlCtrctId;
    /**
     * 执行日期
     */
    private String DlvryDt;
    /**
     * 现金了结交割
     */
    private CashSettlementDelivery CshSttlmDlvry;
    /**
     * 经办人
     */
    private PartyIdentification Oprtr;
    /**
     * 复核人
     */
    private PartyIdentification Chckr;
    /**
     * 确认人
     */
    private PartyIdentification Cnfrmr;


    public String getInstrOrgn() {
        return InstrOrgn;
    }

    public void setInstrOrgn(String instrOrgn) {
        InstrOrgn = instrOrgn;
    }

    public String getSbryTp() {
        return SbryTp;
    }

    public void setSbryTp(String sbryTp) {
        SbryTp = sbryTp;
    }

    public String getInstrId() {
        return InstrId;
    }

    public void setInstrId(String instrId) {
        InstrId = instrId;
    }

    public List<String> getCtrctId() {
        return CtrctId;
    }

    public void setCtrctId(List<String> ctrctId) {
        CtrctId = ctrctId;
    }

    public AccountIdentificationAndName4 getGivAcct() {
        return GivAcct;
    }

    public void setGivAcct(AccountIdentificationAndName4 givAcct) {
        GivAcct = givAcct;
    }

    public AccountIdentificationAndName4 getTakAcct() {
        return TakAcct;
    }

    public void setTakAcct(AccountIdentificationAndName4 takAcct) {
        TakAcct = takAcct;
    }

    public String getOrgnlInstrId() {
        return OrgnlInstrId;
    }

    public void setOrgnlInstrId(String orgnlInstrId) {
        OrgnlInstrId = orgnlInstrId;
    }

    public String getOrgnlCtrctId() {
        return OrgnlCtrctId;
    }

    public void setOrgnlCtrctId(String orgnlCtrctId) {
        OrgnlCtrctId = orgnlCtrctId;
    }

    public String getDlvryDt() {
        return DlvryDt;
    }

    public void setDlvryDt(String dlvryDt) {
        DlvryDt = dlvryDt;
    }

    public CashSettlementDelivery getCshSttlmDlvry() {
        return CshSttlmDlvry;
    }

    public void setCshSttlmDlvry(CashSettlementDelivery cshSttlmDlvry) {
        CshSttlmDlvry = cshSttlmDlvry;
    }

    public PartyIdentification getOprtr() {
        return Oprtr;
    }

    public void setOprtr(PartyIdentification oprtr) {
        Oprtr = oprtr;
    }

    public PartyIdentification getChckr() {
        return Chckr;
    }

    public void setChckr(PartyIdentification chckr) {
        Chckr = chckr;
    }

    public PartyIdentification getCnfrmr() {
        return Cnfrmr;
    }

    public void setCnfrmr(PartyIdentification cnfrmr) {
        Cnfrmr = cnfrmr;
    }
}
