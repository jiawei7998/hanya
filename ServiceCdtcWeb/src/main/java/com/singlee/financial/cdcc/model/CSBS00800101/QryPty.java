package com.singlee.financial.cdcc.model.CSBS00800101;

import com.singlee.financial.cdcc.model.component.AgentParty;
import com.singlee.financial.cdcc.model.component.GenericIdentification4;

import java.io.Serializable;

/**
 * @author Liang
 * @date 2021/11/27 14:20
 * =======================
 */
public class QryPty implements Serializable {


    // 名称
    private String Nm;

    // 自定义标识
    private GenericIdentification4 PrtryId;

    //   代理机构
    private AgentParty AgtPty;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public GenericIdentification4 getPrtryId() {
        return PrtryId;
    }

    public void setPrtryId(GenericIdentification4 prtryId) {
        PrtryId = prtryId;
    }

    public AgentParty getAgtPty() {
        return AgtPty;
    }

    public void setAgtPty(AgentParty agtPty) {
        AgtPty = agtPty;
    }
}
