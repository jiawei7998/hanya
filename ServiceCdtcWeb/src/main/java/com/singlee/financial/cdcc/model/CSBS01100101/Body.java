package com.singlee.financial.cdcc.model.CSBS01100101;

import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.BatchCondition;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.component.PartyIdentification;

import java.util.List;

/**
 * 请求报文体
 *
 * @author copysun
 */
public class Body extends EsbBodyPacket {

    /**
     * 报文标识1.0
     */
    private Msg Msg;

    /**
     * 查询功能点2.0
     */
    private String QryFunc;


    /**
     * 查询条件3.0
     */
    private BatchCondition QryCond;

    /**
     * 查询方4.0
     */
    private QryPty QryPty;


    /**
     * 经办员5.0
     */
    private PartyIdentification Oprtr;

    /**
     * 扩展项6.0
     */
    private List<Extension> Xtnsn;

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg msg) {
        Msg = msg;
    }

    public String getQryFunc() {
        return QryFunc;
    }

    public void setQryFunc(String qryFunc) {
        QryFunc = qryFunc;
    }

    public BatchCondition getQryCond() {
        return QryCond;
    }

    public void setQryCond(BatchCondition qryCond) {
        QryCond = qryCond;
    }

    public QryPty getQryPty() {
        return QryPty;
    }

    public void setQryPty(QryPty qryPty) {
        QryPty = qryPty;
    }

    public PartyIdentification getOprtr() {
        return Oprtr;
    }

    public void setOprtr(PartyIdentification oprtr) {
        Oprtr = oprtr;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getBodyConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("Document");
        info.setSubPacketClassType(com.singlee.financial.cdcc.model.CSBS01000101.Body.class);
        // 处理子标签包含集合的内容
        info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
        return info;
    }
}
