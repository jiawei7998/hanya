package com.singlee.financial.cdcc.model.header;

import java.io.Serializable;

/**
 * 报文发起方
 */
public class Sndr implements Serializable {
    /**
     * 发送方标识
     */
    private String SndrId;
    /**
     * 发送系统标识号
     */
    private String SndrSysId;

    public String getSndrId() {
        return SndrId;
    }

    public void setSndrId(String sndrId) {
        SndrId = sndrId;
    }

    public String getSndrSysId() {
        return SndrSysId;
    }

    public void setSndrSysId(String sndrSysId) {
        SndrSysId = sndrSysId;
    }
}
