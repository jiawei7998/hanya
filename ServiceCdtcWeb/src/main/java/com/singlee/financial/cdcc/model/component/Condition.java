package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 查询条件组件
 */
public class Condition implements Serializable {
    /**
     * 编号
     */
    private String Id;
    /**
     * 查询日期
     */
    private String QryDt;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getQryDt() {
        return QryDt;
    }

    public void setQryDt(String qryDt) {
        QryDt = qryDt;
    }
}
