package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/27 14:12
 * @description：指令状态信息
 * @modified By：
 * @version:
 */
public class InstructionStatusInfo implements Serializable {
    //指令处理状态
    private  String  InstrSts;
    //指令处理结果返回码
    private  String  TxRsltCd;
    //指令返回信息描述
    private  String  InsErrInf;
    //发令方确认标识
    private  String  OrgtrCnfrmInd;
    //对手方确认标识
    private  String  CtrCnfrmInd;

    public String getInstrSts() {
        return InstrSts;
    }

    public void setInstrSts(String instrSts) {
        InstrSts = instrSts;
    }

    public String getTxRsltCd() {
        return TxRsltCd;
    }

    public void setTxRsltCd(String txRsltCd) {
        TxRsltCd = txRsltCd;
    }

    public String getInsErrInf() {
        return InsErrInf;
    }

    public void setInsErrInf(String insErrInf) {
        InsErrInf = insErrInf;
    }

    public String getOrgtrCnfrmInd() {
        return OrgtrCnfrmInd;
    }

    public void setOrgtrCnfrmInd(String orgtrCnfrmInd) {
        OrgtrCnfrmInd = orgtrCnfrmInd;
    }

    public String getCtrCnfrmInd() {
        return CtrCnfrmInd;
    }

    public void setCtrCnfrmInd(String ctrCnfrmInd) {
        CtrCnfrmInd = ctrCnfrmInd;
    }
}
