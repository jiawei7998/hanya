package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 机构组件
 */
public class PartyIdentification30 implements Serializable {
    /**
     * 名称
     */
    private String Nm;
    /**
     * 自定义标识
     */
    private GenericIdentification4 PrtryId;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public GenericIdentification4 getPrtryId() {
        return PrtryId;
    }

    public void setPrtryId(GenericIdentification4 prtryId) {
        PrtryId = prtryId;
    }
}
