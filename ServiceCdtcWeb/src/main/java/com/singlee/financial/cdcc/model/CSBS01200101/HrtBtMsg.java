package com.singlee.financial.cdcc.model.CSBS01200101;

import java.io.Serializable;

/**
 * 心跳消息
 */
public class HrtBtMsg implements Serializable {
    /**
     * 心跳序号
     */
    private String HrtBtId;
    /**
     * 心跳参考号
     */
    private String HrtBtRefId;

    public String getHrtBtId() {
        return HrtBtId;
    }

    public void setHrtBtId(String hrtBtId) {
        HrtBtId = hrtBtId;
    }

    public String getHrtBtRefId() {
        return HrtBtRefId;
    }

    public void setHrtBtRefId(String hrtBtRefId) {
        HrtBtRefId = hrtBtRefId;
    }
}
