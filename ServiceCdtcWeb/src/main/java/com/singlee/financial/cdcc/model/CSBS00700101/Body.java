package com.singlee.financial.cdcc.model.CSBS00700101;

import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.AgentParty;
import com.singlee.financial.cdcc.model.component.Extension;

import java.util.List;

/**
 * 债券账户对账单查询请求体
 */
public class Body extends EsbBodyPacket {
    /**
     * 报文标识
     */
    private Msg Msg;
    /**
     * 操作标识
     */
    private OprtrFlg OprtrFlg;
    /**
     * 查询条目
     */
    private QryItm QryItm;
    /**
     * 查询方
     */
    private QryPty QryPty;
    /**
     * 扩展项
     */
    private List<Extension> Xtnsn;

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg msg) {
        Msg = msg;
    }

    public OprtrFlg getOprtrFlg() {
        return OprtrFlg;
    }

    public void setOprtrFlg(OprtrFlg oprtrFlg) {
        OprtrFlg = oprtrFlg;
    }

    public QryItm getQryItm() {
        return QryItm;
    }

    public void setQryItm(QryItm qryItm) {
        QryItm = qryItm;
    }

    public QryPty getQryPty() {
        return QryPty;
    }

    public void setQryPty(QryPty qryPty) {
        QryPty = qryPty;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getBodyConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("Document");
        info.setSubPacketClassType(Body.class);
        // 处理子标签包含集合的内容
        info.addAlias(QryPty.class, "AgtPty", "AgtPty", AgentParty.class);
        info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
        return info;
    }
}
