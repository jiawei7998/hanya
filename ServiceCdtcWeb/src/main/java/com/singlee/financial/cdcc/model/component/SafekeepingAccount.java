package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 帐户组件
 */
public class SafekeepingAccount implements Serializable {

    /**
     * 账户名称
     */
    private String Nm;
    /**
     * 账户编号
     */
    private String AcctId;

    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public String getAcctId() {
        return AcctId;
    }

    public void setAcctId(String acctId) {
        AcctId = acctId;
    }
}
