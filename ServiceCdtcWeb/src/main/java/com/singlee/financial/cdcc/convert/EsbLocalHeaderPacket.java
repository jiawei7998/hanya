package com.singlee.financial.cdcc.convert;

import java.io.Serializable;

/**
 * Esb本地头报文抽象类
 * @author chenxh
 *
 */
public abstract class EsbLocalHeaderPacket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 793201452739313148L;

	/**
	 * 组装本地头的节点转换信息
	 * @return
	 */
	public abstract EsbSubPacketConvert getLocalHeaderConvert();
}
