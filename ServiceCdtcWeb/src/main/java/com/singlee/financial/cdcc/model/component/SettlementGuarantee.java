package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 保证金组件
 */
public class SettlementGuarantee implements Serializable {

    /**
     * 保证方式
     */
    private String GrteTp;
    /**
     * 保证券
     */
    private CollateralBond1 GrteBd;
    /**
     * 保证金保管地
     */
    private String CshLctn;
    /**
     * 保证金金额
     */
    private String GrtePricAmt;

    public String getGrteTp() {
        return GrteTp;
    }

    public void setGrteTp(String grteTp) {
        GrteTp = grteTp;
    }

    public CollateralBond1 getGrteBd() {
        return GrteBd;
    }

    public void setGrteBd(CollateralBond1 grteBd) {
        GrteBd = grteBd;
    }

    public String getCshLctn() {
        return CshLctn;
    }

    public void setCshLctn(String cshLctn) {
        CshLctn = cshLctn;
    }

    public String getGrtePricAmt() {
        return GrtePricAmt;
    }

    public void setGrtePricAmt(String grtePricAmt) {
        GrtePricAmt = grtePricAmt;
    }
}
