package com.singlee.financial.cdcc.model.CSBS01200101;

import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.Extension;

import java.util.List;

/**
 * 心跳报文体
 */
public class Body extends EsbBodyPacket {
    /**
     * 报文标识
     */
    private Msg Msg;
    /**
     * 心跳消息
     */
    private HrtBtMsg HrtBtMsg;
    /**
     * 扩展项
     */
    private List<Extension> Xtnsn;

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg msg) {
        Msg = msg;
    }

    public HrtBtMsg getHrtBtMsg() {
        return HrtBtMsg;
    }

    public void setHrtBtMsg(HrtBtMsg hrtBtMsg) {
        HrtBtMsg = hrtBtMsg;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getBodyConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("Document");
        info.setSubPacketClassType(Body.class);
        // 处理子标签包含集合的内容
        info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
        return info;
    }
}
