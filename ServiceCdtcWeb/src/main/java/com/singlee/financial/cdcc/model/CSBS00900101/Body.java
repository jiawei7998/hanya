package com.singlee.financial.cdcc.model.CSBS00900101;

import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.component.Extension;

import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 14:29
 * =======================
 */
public class Body extends EsbBodyPacket {
    /**
     * 1.0 报文标识
     */
    private Msg Msg;
    /**
     * 2.0 系统异常信息
     */
    private SysXcptn SysXcptn;
    /**
     * 3.0 扩展项
     */
    private List<Extension> Xtnsn;

    public Msg getMsg() {
        return Msg;
    }

    public void setMsg(Msg msg) {
        Msg = msg;
    }

    public SysXcptn getSysXcptn() {
        return SysXcptn;
    }

    public void setSysXcptn(SysXcptn sysXcptn) {
        SysXcptn = sysXcptn;
    }

    public List<Extension> getXtnsn() {
        return Xtnsn;
    }

    public void setXtnsn(List<Extension> xtnsn) {
        Xtnsn = xtnsn;
    }

    @Override
    public EsbSubPacketConvert getBodyConvert() {
        EsbSubPacketConvert info = new EsbSubPacketConvert();
        info.setSubPacketAliasName("Document");
        info.setSubPacketClassType(Body.class);
        // 处理子标签包含集合的内容
        info.addAlias(Body.class, "Xtnsn", "Xtnsn", Extension.class);
        return info;
    }
}
