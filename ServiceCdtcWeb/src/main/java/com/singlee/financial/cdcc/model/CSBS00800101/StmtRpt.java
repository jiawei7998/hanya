package com.singlee.financial.cdcc.model.CSBS00800101;


import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName5;

import java.io.Serializable;
import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 14:13
 * =======================
 */
public class StmtRpt implements Serializable {

    // 起始日期
    private String StartDt;

    // 截止日期
    private String EndDt;

    // 债券账户计数
    private String  SfkpgAcctCnt;

    //债券账户对账单信息
    private List<AccountIdentificationAndName5> SfkpgAcctStmtInf;

    public String getStartDt() {
        return StartDt;
    }

    public void setStartDt(String startDt) {
        StartDt = startDt;
    }

    public String getEndDt() {
        return EndDt;
    }

    public void setEndDt(String endDt) {
        EndDt = endDt;
    }

    public String getSfkpgAcctCnt() {
        return SfkpgAcctCnt;
    }

    public void setSfkpgAcctCnt(String sfkpgAcctCnt) {
        SfkpgAcctCnt = sfkpgAcctCnt;
    }

    public List<AccountIdentificationAndName5> getSfkpgAcctStmtInf() {
        return SfkpgAcctStmtInf;
    }

    public void setSfkpgAcctStmtInf(List<AccountIdentificationAndName5> sfkpgAcctStmtInf) {
        SfkpgAcctStmtInf = sfkpgAcctStmtInf;
    }
}
