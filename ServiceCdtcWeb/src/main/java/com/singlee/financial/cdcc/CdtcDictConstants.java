package com.singlee.financial.cdcc;

/**
 * 中债注释
 */
public class CdtcDictConstants {
	/**
	 * 债券算结类型代码
	 */
	public final static class BondSettlementTypeCode{
		/**
		 * 纯券过户（FOP）
		 */
		public final static String  ST00 = "ST00";
		/**
		 * 见款付券（DAP）
		 */
		public final static String  ST01 = "ST01";
		/**
		 * 见券付款（PAD）
		 */
		public final static String  ST02 = "ST02";
		/**
		 * 券款对付（DVP）
		 */
		public final static String  ST03 = "ST03";
		/**
		 * 券券对付（DVD）
		 */
		public final static String  ST04 = "ST04";
		/**
		 * 返券付费解券（BLDAP）
		 */
		public final static String  ST05 = "ST05";
		/**
		 * 券费对付（BLDVP）
		 */
		public final static String  ST06 = "ST06";
		/**
		 * 空
		 */
		public final static String  ST07 = "ST07";
	}

	/**
	 * 保证金保管地代码
	 */
	public final static class CashLocationCode{
		/**
		 * 中央结算公司
		 */
		public final static String  CL00 = "CL00";
		/**
		 * 外汇交易中心
		 */
		public final static String  CL01 = "CL01";
		/**
		 * 其他
		 */
		public final static String  CL02 = "CL02";
	}

	/**
	 * 保证方式代码
	 */
	public final static class GuaranteeTypeCode{
		/**
		 * 保证金（CashColateral）
		 */
		public final static String  DT01 = "DT01";
		/**
		 * 保证券（BondColateral）
		 */
		public final static String  DT02 = "DT02";
	}

	/**
	 * 优先级4代码
	 */
	public final static class Priority4Code{
		/**
		 * 高级
		 */
		public final static String  PT01 = "PT01";
		/**
		 * 普通
		 */
		public final static String  PT02 = "PT02";
		/**
		 * 低级
		 */
		public final static String  PT03 = "PT03";
	}

	/**
	 * 查询类型4代码
	 */
	public final static class QueryType4Code{
		/**
		 * 托管总对账单
		 */
		public final static String  QT01 = "QT01";
		/**
		 * 托管明细对账单
		 */
		public final static String  QT02 = "QT02";
	}

	/**
	 * 操作码代码
	 */
	public final static class OperatorStatusCode{
		/**
		 * 创建
		 */
		public final static String  OS00 = "OS00";
		/**
		 * 确认
		 */
		public final static String  OS01 = "OS01";
		/**
		 * 拒绝
		 */
		public final static String  OS02 = "OS02";
		/**
		 * 批量
		 */
		public final static String  OS03 = "OS03";
		/**
		 * 详情
		 */
		public final static String  OS04 = "OS04";
		/**
		 * 付券方确认
		 */
		public final static String  OS05 = "OS05";
		/**
		 * 收券方确认
		 */
		public final static String  OS06 = "OS06";
		/**
		 * 总对账单
		 */
		public final static String  OS07 = "OS07";
		/**
		 * 明细最账单
		 */
		public final static String  OS08 = "OS08";
		/**
		 * 结算代理批量查询
		 */
		public final static String  OS20 = "OS20";
		/**
		 * 托管代理批量查询
		 */
		public final static String  OS21 = "OS21";
		/**
		 * 产品代理批量查询
		 */
		public final static String  OS22 = "OS22";
	}

	/**
	 * 指令处理状态代码
	 */
	public final static class InstructionStatusCode{
		/**
		 * 成功
		 */
		public final static String  IS00 = "IS00";
		/**
		 * 待复核
		 */
		public final static String  IS01 = "IS01";
		/**
		 * 已复核
		 */
		public final static String  IS02 = "IS02";
		/**
		 * 待确认
		 */
		public final static String  IS03 = "IS03";
		/**
		 * 已确认
		 */
		public final static String  IS04 = "IS04";
		/**
		 * 合法
		 */
		public final static String  IS05 = "IS05";
		/**
		 * 非法
		 */
		public final static String  IS06 = "IS06";
		/**
		 * 作废
		 */
		public final static String  IS07 = "IS07";
		/**
		 * 撤销
		 */
		public final static String  IS08 = "IS08";
	}

	/**
	 * 合同处理状态代码
	 */
	public final static class ContractStatusCode{
		/**
		 * 成功
		 */
		public final static String  CS00 = "CS00";
		/**
		 * 等券
		 */
		public final static String  CS01 = "CS01";
		/**
		 * 等款
		 */
		public final static String  CS02 = "CS02";
		/**
		 * 待生效
		 */
		public final static String  CS03 = "CS03";
		/**
		 * 待履行
		 */
		public final static String  CS04 = "CS04";
		/**
		 * 应履行
		 */
		public final static String  CS05 = "CS05";
		/**
		 * 履行中
		 */
		public final static String  CS06 = "CS06";
		/**
		 * 部分过户
		 */
		public final static String  CS07 = "CS07";
		/**
		 * 现金了结
		 */
		public final static String  CS08 = "CS08";
		/**
		 * 逾期完成
		 */
		public final static String  CS09 = "CS09";
		/**
		 * 已清偿
		 */
		public final static String  CS10 = "CS10";
		/**
		 * 失败
		 */
		public final static String  CS11 = "CS11";
		/**
		 * 取消
		 */
		public final static String  CS12 = "CS12";
		/**
		 * 撤消
		 */
		public final static String  CS13 = "CS13";
		/**
		 * 作废
		 */
		public final static String  CS14 = "CS14";
	}

	/**
	 * 业务类型代码
	 */
	public final static class BusinessTypeCode{
		/**
		 * 普通分销
		 */
		public final static String  BT00 = "BT00";
		/**
		 * 现券
		 */
		public final static String  BT01 = "BT01";
		/**
		 * 质押式回购
		 */
		public final static String  BT02 = "BT02";
		/**
		 * 买断式回购
		 */
		public final static String  BT03 = "BT03";
		/**
		 * 债券远期
		 */
		public final static String  BT04 = "BT04";
		/**
		 * 债券借贷
		 */
		public final static String  BT05 = "BT05";
		/**
		 * 质押券置换
		 */
		public final static String  BT06 = "BT06";
		/**
		 * BEPS质押
		 */
		public final static String  BT07 = "BT07";
		/**
		 * BEPS解押
		 */
		public final static String  BT08 = "BT08";
		/**
		 * BEPS质押券置换
		 */
		public final static String  BT09 = "BT09";
		/**
		 * 投资人选择提前赎回
		 */
		public final static String  BT10 = "BT10";
		/**
		 * 转托管
		 */
		public final static String  BT11 = "BT11";
		/**
		 * 柜台专项结算
		 */
		public final static String  BT12 = "BT12";
		/**
		 * 柜台分销结算
		 */
		public final static String  BT13 = "BT13";
		/**
		 * 公开市场操作现券
		 */
		public final static String  BT14 = "BT14";
		/**
		 * 公开市场操作质押式回购
		 */
		public final static String  BT15 = "BT15";
		/**
		 * 国库现金商业银行定期存款
		 */
		public final static String  BT16 = "BT16";
		/**
		 * 收款确认
		 */
		public final static String  BT17 = "BT17";
		/**
		 * 付款确认
		 */
		public final static String  BT18 = "BT18";
		/**
		 * 质押式回购逾期返售
		 */
		public final static String  BT19 = "BT19";
		/**
		 * 现金了结交割
		 */
		public final static String  BT20 = "BT20";
		/**
		 * 撤销合同
		 */
		public final static String  BT21 = "BT21";
		/**
		 * 撤销指令
		 */
		public final static String  BT22 = "BT22";
		/**
		 * 质押式回购到期
		 */
		public final static String  BT23 = "BT23";
		/**
		 * 买断式回购到期
		 */
		public final static String  BT24 = "BT24";
		/**
		 * 债券借贷到期
		 */
		public final static String  BT25 = "BT25";
		/**
		 * 公开市场操作质押式回购到期
		 */
		public final static String  BT26 = "BT26";
		/**
		 * 国库现金商业银行定期存款到期
		 */
		public final static String  BT27 = "BT27";
	}

	/**
	 * 合同冻结状态代码
	 */
	public final static class ContractBlockStatusCode{
		/**
		 * 未冻结
		 */
		public final static String  CB00 = "CB00";
		/**
		 * 已冻结
		 */
		public final static String  CB01 = "CB01";
	}

	/**
	 * 代理机构类型代码
	 */
	public final static class AgentPartyTypeCode{
		/**
		 * 债券结算代理
		 */
		public final static String  AP00 = "AP00";
		/**
		 * 资金结算代理
		 */
		public final static String  AP01 = "AP01";
	}

	/**
	 * 现金了结交割方向代码
	 */
	public final static class CashSettlementDirectionCode{
		/**
		 * 融入方向融出方支付
		 */
		public final static String  SD00 = "SD00";
		/**
		 * 融出方向融入方支付
		 */
		public final static String  SD01 = "SD01";
	}

	/**
	 * 账户科目类型代码
	 */
	public final static class AccountLedgerTypeCode{
		/**
		 * 承销额度
		 */
		public final static String  AL00 = "AL00";
		/**
		 * 承销额度待付
		 */
		public final static String  AL01 = "AL01";
		/**
		 * 可用
		 */
		public final static String  AL02 = "AL02";
		/**
		 * 待付
		 */
		public final static String  AL03 = "AL03";
		/**
		 * 质押式待回购
		 */
		public final static String  AL04 = "AL04";
		/**
		 * 冻结
		 */
		public final static String  AL05 = "AL05";
		/**
		 * 质押
		 */
		public final static String  AL06 = "AL06";
		/**
		 * 待确认债权
		 */
		public final static String  AL07 = "AL07";
		/**
		 * 待确认债权待付
		 */
		public final static String  AL08 = "AL08";
	}

	/**
	 * 费用类型代码
	 */
	public final static class BillingCode{
		/**
		 * 债券登记服务费
		 */
		public final static String  BC00 = "BC00";
		/**
		 * 付息兑付服务费
		 */
		public final static String  BC01 = "BC01";
		/**
		 * 托管账户维护费
		 */
		public final static String  BC02 = "BC02";
		/**
		 * 债券托管服务费
		 */
		public final static String  BC03 = "BC03";
		/**
		 * 结算过户服务费
		 */
		public final static String  BC04 = "BC04";
		/**
		 * 应急业务服务费
		 */
		public final static String  BC05 = "BC05";
	}

	/**
	 * 功能点类型代码
	 */
	public final static class BatchFunctionCode{
		/**
		 * 结算指令下载
		 */
		public final static String  BF00 = "BF00";
		/**
		 * 结算合同下载
		 */
		public final static String  BF01 = "BF01";
		/**
		 * 债券总对账单下载
		 */
		public final static String  BF02 = "BF02";
		/**
		 * 债券总对账单（含估值）下载
		 */
		public final static String  BF03 = "BF03";
		/**
		 * 债券明细对账单下载
		 */
		public final static String  BF04 = "BF04";
		/**
		 * 清偿单据下载
		 */
		public final static String  BF05 = "BF05";
		/**
		 * 债券资料下载
		 */
		public final static String  BF06 = "BF06";
		/**
		 * 付息兑付单据下载
		 */
		public final static String  BF07 = "BF07";
		/**
		 * 结算成员资料下载
		 */
		public final static String  BF08 = "BF08";
		/**
		 * 代理统计成员资料下载
		 */
		public final static String  BF09 = "BF09";
		/**
		 * 债券资料复合下载
		 */
		public final static String  BF10 = "BF10";
		/**
		 * 付息兑付情况下载
		 */
		public final static String  BF11 = "BF11";
		/**
		 * 债券选择权资料下载
		 */
		public final static String  BF12 = "BF12";
		/**
		 * 实时现券结算行情(全价)下载
		 */
		public final static String  BF13 = "BF13";
		/**
		 * 实时现券结算行情(净价)下载
		 */
		public final static String  BF14 = "BF14";
		/**
		 * 实时质押式回购行情下载
		 */
		public final static String  BF15 = "BF15";
		/**
		 * 实时买断式回购行情下载
		 */
		public final static String  BF16 = "BF16";
		/**
		 * 实时行情统计下载
		 */
		public final static String  BF17 = "BF17";
		/**
		 * 标准待偿期收益率曲线(历史)下载
		 */
		public final static String  BF18 = "BF18";
		/**
		 * 任意待偿期收益率曲线数据下载
		 */
		public final static String  BF19 = "BF19";
		/**
		 * 中债估值(历史)下载
		 */
		public final static String  BF20 = "BF20";
		/**
		 * 老版标准待偿期收益率曲线数据下载
		 */
		public final static String  BF21 = "BF21";
		/**
		 * 老版任意待偿期收益率曲线数据下载
		 */
		public final static String  BF22 = "BF22";
		/**
		 * 老版收益率曲线明细数据下载
		 */
		public final static String  BF23 = "BF23";
		/**
		 * 老版中债估值下载
		 */
		public final static String  BF24 = "BF24";
		/**
		 * 持有债券市值下载
		 */
		public final static String  BF25 = "BF25";
	}

	/**
	 * 债券查询类型代码
	 */
	public final static class QueryClassCode{
		/**
		 * 发行
		 */
		public final static String  QC00 = "QC00";
		/**
		 * 兑付
		 */
		public final static String  QC01 = "QC01";
	}

	/**
	 * 消息种类代码
	 */
	public final static class MessageTypeCode{
		/**
		 * 指令状态通知消息
		 */
		public final static String  MT00 = "MT00";
		/**
		 * 合同状态通知消息
		 */
		public final static String  MT01 = "MT01";
		/**
		 * 公告信息通知消息
		 */
		public final static String  MT02 = "MT02";
		/**
		 * 文件生成完成通知消息
		 */
		public final static String  MT03 = "MT03";
	}

	/**
	 * 合同失败原因代码
	 */
	public final static class ContractFailedReasonCode{
		/**
		 * 发令方券不足
		 */
		public final static String  CF00 = "CF00";
		/**
		 * 对手方券不足
		 */
		public final static String  CF01 = "CF01";
		/**
		 * 款不足
		 */
		public final static String  CF02 = "CF02";
		/**
		 * 发令方保证券不足
		 */
		public final static String  CF03 = "CF03";
		/**
		 * 对手方保证券不足
		 */
		public final static String  CF04 = "CF04";
		/**
		 * 付券方保证金不足
		 */
		public final static String  CF05 = "CF05";
		/**
		 * 收券方保证金不足
		 */
		public final static String  CF06 = "CF06";
		/**
		 * 双方保证券不足
		 */
		public final static String  CF07 = "CF07";
		/**
		 * 保证金不足
		 */
		public final static String  CF08 = "CF08";
		/**
		 * 解押时付方保证券不足
		 */
		public final static String  CF09 = "CF09";
		/**
		 * 解押时收方保证券不足
		 */
		public final static String  CF10 = "CF10";
		/**
		 * 付券方保证金未解冻
		 */
		public final static String  CF11 = "CF11";
		/**
		 * 收券方保证金未解冻
		 */
		public final static String  CF12 = "CF12";
		/**
		 * 未发送解押确认指令
		 */
		public final static String  CF13 = "CF13";
		/**
		 * 其它
		 */
		public final static String  CF14 = "CF14";
		/**
		 * 无（当合同非失败时的代码取值）
		 */
		public final static String  CF15 = "CF15";
		/**
		 * 双方保证金未解冻
		 */
		public final static String  CF17 = "CF17";
		/**
		 * 未启动解冻/解押
		 */
		public final static String  CF18 = "CF18";
		/**
		 * 未分配额度不足
		 */
		public final static String  CF19 = "CF19";
		/**
		 * 融入方券不足
		 */
		public final static String  CF20 = "CF20";
		/**
		 * 融入方券款均不足
		 */
		public final static String  CF21 = "CF21";
		/**
		 * 融入方款不足
		 */
		public final static String  CF22 = "CF22";
		/**
		 * 融出方券不足
		 */
		public final static String  CF23 = "CF23";
		/**
		 * 原合同状态不正确
		 */
		public final static String  CF24 = "CF24";
		/**
		 * 交易双方券均不足
		 */
		public final static String  CF25 = "CF25";
		/**
		 * 换入券不足
		 */
		public final static String  CF26 = "CF26";
		/**
		 * 换出券不足
		 */
		public final static String  CF27 = "CF27";
		/**
		 * 出质方券不足
		 */
		public final static String  CF28 = "CF28";
		/**
		 * 债券未注册或不正常
		 */
		public final static String  CF30 = "CF30";
		/**
		 * 债券未流通
		 */
		public final static String  CF31 = "CF31";
		/**
		 * 债券被冻结
		 */
		public final static String  CF32 = "CF32";
		/**
		 * 付券方状态不正常
		 */
		public final static String  CF33 = "CF33";
		/**
		 * 收券方状态不正常
		 */
		public final static String  CF34 = "CF34";
		/**
		 * 双方状态不正常
		 */
		public final static String  CF35 = "CF35";
		/**
		 * 付券方保证券不足
		 */
		public final static String  CF36 = "CF36";
		/**
		 * 收券方保证券不足
		 */
		public final static String  CF37 = "CF37";
		/**
		 * 双方保证券不足
		 */
		public final static String  CF38 = "CF38";
		/**
		 * 冻结请求记录不正常
		 */
		public final static String  CF39 = "CF39";
		/**
		 * 解押时付方保证券不足
		 */
		public final static String  CF40 = "CF40";
		/**
		 * 解押时收方保证券不足
		 */
		public final static String  CF41 = "CF41";
		/**
		 * 解押时双方保证券不足
		 */
		public final static String  CF42 = "CF42";
		/**
		 * 保证券保证金均不足
		 */
		public final static String  CF43 = "CF43";
	}

	/**
	 * 指令来源代码
	 */
	public final static class InstructionOriginCode{
		/**
		 * 国债公司中心应急端
		 */
		public final static String  IO00 = "IO00";
		/**
		 * 成员客户端
		 */
		public final static String  IO01 = "IO01";
		/**
		 * 公开市场系统
		 */
		public final static String  IO02 = "IO02";
		/**
		 * 第三方外汇交易中心
		 */
		public final static String  IO03 = "IO03";
		/**
		 * 柜台系统
		 */
		public final static String  IO04 = "IO04";
	}

	/**
	 * 指令确认标识代码
	 */
	public final static class InstructionConfirmIndicatorCode{
		/**
		 * 无
		 */
		public final static String  IC00 = "IC00";
		/**
		 * 已确认
		 */
		public final static String  IC01 = "IC01";
		/**
		 * 未确认
		 */
		public final static String  IC02 = "IC02";
	}

	/**
	 * 请求类别代码
	 */
	public final static class RequestTypeCode{
		/**
		 * 普通分销
		 */
		public final static String  BJ0100 = "BJ0100";
		/**
		 * 现券
		 */
		public final static String  BJ0101 = "BJ0101";
		/**
		 * 质押式回购
		 */
		public final static String  BJ0102 = "BJ0102";
		/**
		 * 买断式回购
		 */
		public final static String  BJ0103 = "BJ0103";
		/**
		 * 债券远期
		 */
		public final static String  BJ0104 = "BJ0104";
		/**
		 * 债券借贷
		 */
		public final static String  BJ0105 = "BJ0105";
		/**
		 * 质押券置换
		 */
		public final static String  BJ0106 = "BJ0106";
		/**
		 * BEPS质押
		 */
		public final static String  BJ0107 = "BJ0107";
		/**
		 * BEPS解押
		 */
		public final static String  BJ0108 = "BJ0108";
		/**
		 * BEPS质押券置换
		 */
		public final static String  BJ0109 = "BJ0109";
		/**
		 * 投资人选择提前赎回
		 */
		public final static String  BJ0110 = "BJ0110";
		/**
		 * 转托管
		 */
		public final static String  BJ0111 = "BJ0111";
		/**
		 * 柜台专项结算
		 */
		public final static String  BJ0112 = "BJ0112";
		/**
		 * 柜台分销结算
		 */
		public final static String  BJ0113 = "BJ0113";
		/**
		 * 公开市场操作现券
		 */
		public final static String  BJ0114 = "BJ0114";
		/**
		 * 公开市场操作质押式回购
		 */
		public final static String  BJ0115 = "BJ0115";
		/**
		 * 国库现金商业银行定期存款
		 */
		public final static String  BJ0116 = "BJ0116";
		/**
		 * 收款确认
		 */
		public final static String  BJ0200 = "BJ0200";
		/**
		 * 付款确认
		 */
		public final static String  BJ0201 = "BJ0201";
		/**
		 * 质押式回购逾期返售
		 */
		public final static String  BJ0202 = "BJ0202";
		/**
		 * 现金了结交割
		 */
		public final static String  BJ0203 = "BJ0203";
		/**
		 * 撤销合同
		 */
		public final static String  BJ0204 = "BJ0204";
		/**
		 * 撤销指令
		 */
		public final static String  BJ0205 = "BJ0205";
		/**
		 * 指令查询
		 */
		public final static String  BJ0300 = "BJ0300";
		/**
		 * 合同查询
		 */
		public final static String  BJ0301 = "BJ0301";
		/**
		 * 债券账户对账单查询
		 */
		public final static String  BJ0400 = "BJ0400";
	}

	/**
	 * 账户对账单类型代码
	 */
	public final static class AccountSatementType{
		/**
		 * 债权
		 */
		public final static String  AS00 = "AS00";
		/**
		 * 额度
		 */
		public final static String  AS01 = "AS01";
	}

	/**
	 * 中债报文类型
	 */
	public final static class MsgTp{
		/**
		 * CSBS.001.001.01:通用结算指令请求
		 */
		public final static String  CSBS_001 = "CSBS.001.001.01";
		/**
		 * CSBS.002.001.01:通用结算指令状态报告
		 */
		public final static String  CSBS_002 = "CSBS.002.001.01";
		/**
		 * CSBS.003.001.01:通用辅助指令请求
		 */
		public final static String  CSBS_003 = "CSBS.003.001.01";
		/**
		 * CSBS.004.001.01:通用辅助指令状态报告
		 */
		public final static String  CSBS_004 = "CSBS.004.001.01";
		/**
		 * CSBS.005.001.01:结算业务查询请求
		 */
		public final static String  CSBS_005 = "CSBS.005.001.01";
		/**
		 * CSBS.006.001.01:结算业务批量查询状态报告
		 */
		public final static String  CSBS_006 = "CSBS.006.001.01";
		/**
		 * CSBS.007.001.01:托管账户对账单查询请求
		 */
		public final static String  CSBS_007 = "CSBS.007.001.01";
		/**
		 * CSBS.008.001.01:托管账户对账单报告
		 */
		public final static String  CSBS_008 = "CSBS.008.001.01";
		/**
		 * CSBS.009.001.01:系统异常通知
		 */
		public final static String  CSBS_009 = "CSBS.009.001.01";
		/**
		 * CSBS.010.001.01:业务消息通知
		 */
		public final static String  CSBS_010 = "CSBS.010.001.01";
		/**
		 * CSBS.011.001.01:批量数据查询请求
		 */
		public final static String  CSBS_011 = "CSBS.011.001.01";
		/**
		 * CSBS.012.001.01:心跳消息
		 */
		public final static String  CSBS_012 = "CSBS.012.001.01";
	}


}