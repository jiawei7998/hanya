package com.singlee.financial.cdcc.model.CSBS00700101;


import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName7;
import com.singlee.financial.cdcc.model.component.QueryDate;

import java.io.Serializable;

/**
 * 查询条目
 */
public class QryItm implements Serializable {
    /**
     * 查询类型
     */
    private String QryTp;
    /**
     * 查询日期
     */
    private QueryDate QryDt;
    /**
     * 查询账户与债券
     */
    private AccountIdentificationAndName7 SfkpgAcctBdQry;

    public String getQryTp() {
        return QryTp;
    }

    public void setQryTp(String qryTp) {
        QryTp = qryTp;
    }

    public QueryDate getQryDt() {
        return QryDt;
    }

    public void setQryDt(QueryDate qryDt) {
        QryDt = qryDt;
    }

    public AccountIdentificationAndName7 getSfkpgAcctBdQry() {
        return SfkpgAcctBdQry;
    }

    public void setSfkpgAcctBdQry(AccountIdentificationAndName7 sfkpgAcctBdQry) {
        SfkpgAcctBdQry = sfkpgAcctBdQry;
    }
}
