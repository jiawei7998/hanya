package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

public class QueryDate implements Serializable {

    /**
     * 起始日期
     */
    private String StartDt;

    /**
     * 截止日期
     */
    private String EndDt;

    public String getStartDt() {
        return StartDt;
    }

    public void setStartDt(String startDt) {
        StartDt = startDt;
    }

    public String getEndDt() {
        return EndDt;
    }

    public void setEndDt(String endDt) {
        EndDt = endDt;
    }
}
