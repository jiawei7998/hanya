package com.singlee.financial.cdcc.model.CSBS00600101;

import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;

import java.io.Serializable;

/**
 * 批量查询结果
 *
 * @Auther:zk
 */
public class BtchQryRslt implements Serializable {

    /**
     *结算指令标识
     */
    private String InstrId;

    /**
     *结算合同标识
     */
    private String CtrctId;

    /**
     *付券方账户
     */
    private AccountIdentificationAndName4 GivAcct;

    /**
     *收券方账户
     */
    private AccountIdentificationAndName4 TakAcct;

    /**
     *业务标识号
     */
    private String TxId;

    /**
     *业务类型
     */
    private String BizTp;

    /**
     *结算方式1
     */
    private String SttlmTp1;

    /**
     *结算方式2
     */
    private String SttlmTp2;

    /**
     *债券数目
     */
    private String BdCnt;

    /**
     *债券总额
     */
    private String AggtFaceAmt;


    /**
     *结算金额1
     */
    private String Val1;

    /**
     *结算金额2
     */
    private String Val2;

    /**
     *交割日1
     */
    private String Dt1;

    /**
     *交割日2
     */
    private String Dt2;

    /**
     *指令处理状态
     */
    private String InstrSts;

    /**
     *合同处理状态
     */
    private String CtrctSts;

    /**
     *合同冻结状态
     */
    private String CtrctBlckSts;

    /**
     *最新更新时间
     */
    private String LastUpdTm;

    /**
     *发令方确认标识
     */
    private String OrgtrCnfrmInd;

    /**
     *对手方确认标识
     */
    private String CtrCnfrmInd;

    /**
     *指令来源
     */
    private String InstrOrgn;


    public String getInstrId() {
        return InstrId;
    }

    public void setInstrId(String instrId) {
        InstrId = instrId;
    }

    public String getCtrctId() {
        return CtrctId;
    }

    public void setCtrctId(String ctrctId) {
        CtrctId = ctrctId;
    }

    public AccountIdentificationAndName4 getGivAcct() {
        return GivAcct;
    }

    public void setGivAcct(AccountIdentificationAndName4 givAcct) {
        GivAcct = givAcct;
    }

    public AccountIdentificationAndName4 getTakAcct() {
        return TakAcct;
    }

    public void setTakAcct(AccountIdentificationAndName4 takAcct) {
        TakAcct = takAcct;
    }

    public String getTxId() {
        return TxId;
    }

    public void setTxId(String txId) {
        TxId = txId;
    }

    public String getBizTp() {
        return BizTp;
    }

    public void setBizTp(String bizTp) {
        BizTp = bizTp;
    }

    public String getSttlmTp1() {
        return SttlmTp1;
    }

    public void setSttlmTp1(String sttlmTp1) {
        SttlmTp1 = sttlmTp1;
    }

    public String getSttlmTp2() {
        return SttlmTp2;
    }

    public void setSttlmTp2(String sttlmTp2) {
        SttlmTp2 = sttlmTp2;
    }

    public String getBdCnt() {
        return BdCnt;
    }

    public void setBdCnt(String bdCnt) {
        BdCnt = bdCnt;
    }

    public String getAggtFaceAmt() {
        return AggtFaceAmt;
    }

    public void setAggtFaceAmt(String aggtFaceAmt) {
        AggtFaceAmt = aggtFaceAmt;
    }

    public String getVal1() {
        return Val1;
    }

    public void setVal1(String val1) {
        Val1 = val1;
    }

    public String getVal2() {
        return Val2;
    }

    public void setVal2(String val2) {
        Val2 = val2;
    }

    public String getDt1() {
        return Dt1;
    }

    public void setDt1(String dt1) {
        Dt1 = dt1;
    }

    public String getDt2() {
        return Dt2;
    }

    public void setDt2(String dt2) {
        Dt2 = dt2;
    }

    public String getInstrSts() {
        return InstrSts;
    }

    public void setInstrSts(String instrSts) {
        InstrSts = instrSts;
    }

    public String getCtrctSts() {
        return CtrctSts;
    }

    public void setCtrctSts(String ctrctSts) {
        CtrctSts = ctrctSts;
    }

    public String getCtrctBlckSts() {
        return CtrctBlckSts;
    }

    public void setCtrctBlckSts(String ctrctBlckSts) {
        CtrctBlckSts = ctrctBlckSts;
    }

    public String getLastUpdTm() {
        return LastUpdTm;
    }

    public void setLastUpdTm(String lastUpdTm) {
        LastUpdTm = lastUpdTm;
    }

    public String getOrgtrCnfrmInd() {
        return OrgtrCnfrmInd;
    }

    public void setOrgtrCnfrmInd(String orgtrCnfrmInd) {
        OrgtrCnfrmInd = orgtrCnfrmInd;
    }

    public String getCtrCnfrmInd() {
        return CtrCnfrmInd;
    }

    public void setCtrCnfrmInd(String ctrCnfrmInd) {
        CtrCnfrmInd = ctrCnfrmInd;
    }

    public String getInstrOrgn() {
        return InstrOrgn;
    }

    public void setInstrOrgn(String instrOrgn) {
        InstrOrgn = instrOrgn;
    }

    @Override
    public String toString() {
        return "BtchQryRslt{" +
                "InstrId='" + InstrId + '\'' +
                ", CtrctId='" + CtrctId + '\'' +
                ", GivAcct=" + GivAcct +
                ", TakAcct=" + TakAcct +
                ", TxId='" + TxId + '\'' +
                ", BizTp='" + BizTp + '\'' +
                ", SttlmTp1='" + SttlmTp1 + '\'' +
                ", SttlmTp2='" + SttlmTp2 + '\'' +
                ", BdCnt='" + BdCnt + '\'' +
                ", AggtFaceAmt='" + AggtFaceAmt + '\'' +
                ", Val1='" + Val1 + '\'' +
                ", Val2='" + Val2 + '\'' +
                ", Dt1='" + Dt1 + '\'' +
                ", Dt2='" + Dt2 + '\'' +
                ", InstrSts='" + InstrSts + '\'' +
                ", CtrctSts='" + CtrctSts + '\'' +
                ", CtrctBlckSts='" + CtrctBlckSts + '\'' +
                ", LastUpdTm='" + LastUpdTm + '\'' +
                ", OrgtrCnfrmInd='" + OrgtrCnfrmInd + '\'' +
                ", CtrCnfrmInd='" + CtrCnfrmInd + '\'' +
                ", InstrOrgn='" + InstrOrgn + '\'' +
                '}';
    }
}
