package com.singlee.financial.cdcc.model.header;

import java.io.Serializable;

/**
 * 报文描述
 */
public class MsgDesc implements Serializable {
    /**
     * 报文原始发起人
     */
    private String MsgOrgnlSndr;
    /**
     * 报文最终接收人
     */
    private String MsgFnlRcvr;
    /**
     * 报文发起时间
     */
    private String MsgSndDateTime;
    /**
     * 报文类型
     */
    private String MsgTp;
    /**
     * 报文标识号
     */
    private String MsgId;
    /**
     *报文参考号
     */
    private String MsgRefId;
    /**
     * 报体校验值
     */
    private String MsgBodyChcksum;

    public String getMsgOrgnlSndr() {
        return MsgOrgnlSndr;
    }

    public void setMsgOrgnlSndr(String msgOrgnlSndr) {
        MsgOrgnlSndr = msgOrgnlSndr;
    }

    public String getMsgFnlRcvr() {
        return MsgFnlRcvr;
    }

    public void setMsgFnlRcvr(String msgFnlRcvr) {
        MsgFnlRcvr = msgFnlRcvr;
    }

    public String getMsgSndDateTime() {
        return MsgSndDateTime;
    }

    public void setMsgSndDateTime(String msgSndDateTime) {
        MsgSndDateTime = msgSndDateTime;
    }

    public String getMsgTp() {
        return MsgTp;
    }

    public void setMsgTp(String msgTp) {
        MsgTp = msgTp;
    }

    public String getMsgId() {
        return MsgId;
    }

    public void setMsgId(String msgId) {
        MsgId = msgId;
    }

    public String getMsgRefId() {
        return MsgRefId;
    }

    public void setMsgRefId(String msgRefId) {
        MsgRefId = msgRefId;
    }

    public String getMsgBodyChcksum() {
        return MsgBodyChcksum;
    }

    public void setMsgBodyChcksum(String msgBodyChcksum) {
        MsgBodyChcksum = msgBodyChcksum;
    }
}
