package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;
import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 15:06
 * =======================
 */
public class DetailedStatement implements Serializable {
    /**
     * 记帐日期
     */
    private String TxDt;
    /**
     * 记帐时间
     */
    private String TxTm;
    /**
     * 结算合同标识
     */
    private String CtrctId;
    /**
     * 记账摘要
     */
    private String TxLog;
    /**
     * 发生额
     */
    private TransactionAmount TxAmt;
    /**
     * 账户科目
     */
    private List<AccountLedger> AcctLdg;

    public String getTxDt() {
        return TxDt;
    }

    public void setTxDt(String txDt) {
        TxDt = txDt;
    }

    public String getTxTm() {
        return TxTm;
    }

    public void setTxTm(String txTm) {
        TxTm = txTm;
    }

    public String getCtrctId() {
        return CtrctId;
    }

    public void setCtrctId(String ctrctId) {
        CtrctId = ctrctId;
    }

    public String getTxLog() {
        return TxLog;
    }

    public void setTxLog(String txLog) {
        TxLog = txLog;
    }

    public TransactionAmount getTxAmt() {
        return TxAmt;
    }

    public void setTxAmt(TransactionAmount txAmt) {
        TxAmt = txAmt;
    }

    public List<AccountLedger> getAcctLdg() {
        return AcctLdg;
    }

    public void setAcctLdg(List<AccountLedger> acctLdg) {
        AcctLdg = acctLdg;
    }
}
