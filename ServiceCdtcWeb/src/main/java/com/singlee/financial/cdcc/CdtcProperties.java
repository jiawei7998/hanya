package com.singlee.financial.cdcc;

import com.singlee.capital.common.util.JY;
import com.singlee.capital.common.util.PropertiesUtil;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * 中债基础信息配置
 */
public class CdtcProperties {

    //xml 下载本地 保存地址
    public static String xmlDMSGPath;
    //中债前置机 FTP 请求地址
    public static String FTPIP;
    //中债前置机 FTP  端口
    public static String FTPPORT;
    //中债前置机 FTP  账户
    public static String FTPUSER;
    //中债前置机 FTP  密码
    public static String FTPPSWD;
    //中债前置机 中债报文存放地址
    public static String ftpFilePath;

    //中债合同报文备份地址
    public static String xmlCtrctDMSGBackUpPath;
    //中债指令报文备份地址
    public static String xmlDMSGBackUpPath;
    //中债前置机 was服务请求地址
    public static String wasIp;
    public static String beginWorkTime;
    public static String endWorkTime;
    //中债前置机  超时时间
    public static String timeOut;

    public static String hrbBankAcct;
    public static String batchQueryTime;


    static {
        PropertiesConfiguration config;
        try {
            config = PropertiesUtil.parseFile("cdtc.properties");

            xmlDMSGPath = config.getString("cdtc.xmlDMSGPath", "");
            FTPIP = config.getString("cdtc.FTPIP", "");
            FTPPORT = config.getString("cdtc.FTPPORT", "");
            FTPUSER = config.getString("cdtc.FTPUSER", "");
            FTPPSWD = config.getString("cdtc.FTPPSWD", "");
            ftpFilePath = config.getString("cdtc.ftpFilePath", "");

            xmlCtrctDMSGBackUpPath = config.getString("cdtc.xmlCtrctDMSGBackUpPath", "");
            xmlDMSGBackUpPath = config.getString("cdtc.xmlDMSGBackUpPath", "");
            wasIp = config.getString("cdtc.wasIp", "");
            beginWorkTime = config.getString("cdtc.beginWorkTime", "");
            endWorkTime = config.getString("cdtc.endWorkTime", "");
            timeOut = config.getString("cdtc.timeOut", "");

            hrbBankAcct = config.getString("cdtc.hrbBankAcct", "");
            batchQueryTime = config.getString("cdtc.batchQueryTime", "");

        } catch (ConfigurationException e) {
            JY.raise("system.properties文件解析失败   ", e);
        }
    }
    
}
