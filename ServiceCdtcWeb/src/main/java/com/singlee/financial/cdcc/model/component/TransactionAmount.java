package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * @author Liang
 * @date 2021/11/27 15:14
 * =======================
 */
public class TransactionAmount implements Serializable {
    /**
     * 方向标识
     */
    private String DrctnFlg;

    /**
     * 值域
     */
    private String Val;

    public String getDrctnFlg() {
        return DrctnFlg;
    }

    public void setDrctnFlg(String drctnFlg) {
        DrctnFlg = drctnFlg;
    }

    public String getVal() {
        return Val;
    }

    public void setVal(String val) {
        Val = val;
    }
}
