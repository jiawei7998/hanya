package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 适用的批量下载功能编号：
 * BF10
 */
public class BatchCondition4 implements Serializable {
    /**
     * 开始日期
     */
    private String StrtDt;
    /**
     * 结束日期
     */
    private String EndDt;
    /**
     * 待偿期开始年数
     */
    private String StrtYrsToMtrty;
    /**
     * 待偿期结束年数
     */
    private String EndYrsToMtrty;
    /**
     * 查询类别
     */
    private String QryCls;

    public String getStrtDt() {
        return StrtDt;
    }

    public void setStrtDt(String strtDt) {
        StrtDt = strtDt;
    }

    public String getEndDt() {
        return EndDt;
    }

    public void setEndDt(String endDt) {
        EndDt = endDt;
    }

    public String getStrtYrsToMtrty() {
        return StrtYrsToMtrty;
    }

    public void setStrtYrsToMtrty(String strtYrsToMtrty) {
        StrtYrsToMtrty = strtYrsToMtrty;
    }

    public String getEndYrsToMtrty() {
        return EndYrsToMtrty;
    }

    public void setEndYrsToMtrty(String endYrsToMtrty) {
        EndYrsToMtrty = endYrsToMtrty;
    }

    public String getQryCls() {
        return QryCls;
    }

    public void setQryCls(String qryCls) {
        QryCls = qryCls;
    }
}
