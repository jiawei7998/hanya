package com.singlee.financial.cdcc.model.CSBS00700101;

import com.singlee.financial.cdcc.model.component.AgentParty;
import com.singlee.financial.cdcc.model.component.GenericIdentification4;

import java.io.Serializable;
import java.util.List;

/**
 * 查询方
 */
public class QryPty implements Serializable {
    /**
     * 名称
     */
    private String Nm;
    /**
     * PrtryId
     */
    private GenericIdentification4 PrtryId;
    /**
     * 代理机构
     */
    private List<AgentParty> AgtPty;


    public String getNm() {
        return Nm;
    }

    public void setNm(String nm) {
        Nm = nm;
    }

    public GenericIdentification4 getPrtryId() {
        return PrtryId;
    }

    public void setPrtryId(GenericIdentification4 prtryId) {
        PrtryId = prtryId;
    }

    public List<AgentParty> getAgtPty() {
        return AgtPty;
    }

    public void setAgtPty(List<AgentParty> agtPty) {
        AgtPty = agtPty;
    }
}
