package com.singlee.financial.cdcc.model.CSBS00500101;

import com.singlee.financial.cdcc.model.component.AgentParty;
import com.singlee.financial.cdcc.model.component.GenericIdentification4;

import java.io.Serializable;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/27 14:43
 * @description：查询方
 * @modified By：
 * @version:
 */
public class QryPty implements Serializable {


    //名称
    private String ReqTp;
    //自定义标识
    private GenericIdentification4 PrtryId;
    //代理机构
    private AgentParty AgtPty;

    public String getReqTp() {
        return ReqTp;
    }

    public void setReqTp(String reqTp) {
        ReqTp = reqTp;
    }

    public GenericIdentification4 getPrtryId() {
        return PrtryId;
    }

    public void setPrtryId(GenericIdentification4 prtryId) {
        PrtryId = prtryId;
    }

    public AgentParty getAgtPty() {
        return AgtPty;
    }

    public void setAgtPty(AgentParty agtPty) {
        AgtPty = agtPty;
    }
}
