package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * @author ：chenguo
 * @date ：Created in 2021/11/27 14:16
 * @description：合同状态信息
 * @modified By：
 * @version:
 */
public class ContractStatusInfo  implements Serializable {

    //结算合同标识
    private  String  CtrctId;
    //合同处理状态
    private  String  CtrctSts;
    //合同冻结状态
    private  String  CtrctBlckSts;
    //合同失败原因
    private  String  CtrctFaildRsn;
    //最新更新时间
    private  String  LastUpdTm;
    //合同查询结果返回码
    private  String  CtrctQryRsltCd;
    //合同查询结果返回信息
    private  String  CtrctQryRsltInf;

    public String getCtrctId() {
        return CtrctId;
    }

    public void setCtrctId(String ctrctId) {
        CtrctId = ctrctId;
    }

    public String getCtrctSts() {
        return CtrctSts;
    }

    public void setCtrctSts(String ctrctSts) {
        CtrctSts = ctrctSts;
    }

    public String getCtrctBlckSts() {
        return CtrctBlckSts;
    }

    public void setCtrctBlckSts(String ctrctBlckSts) {
        CtrctBlckSts = ctrctBlckSts;
    }

    public String getCtrctFaildRsn() {
        return CtrctFaildRsn;
    }

    public void setCtrctFaildRsn(String ctrctFaildRsn) {
        CtrctFaildRsn = ctrctFaildRsn;
    }

    public String getLastUpdTm() {
        return LastUpdTm;
    }

    public void setLastUpdTm(String lastUpdTm) {
        LastUpdTm = lastUpdTm;
    }

    public String getCtrctQryRsltCd() {
        return CtrctQryRsltCd;
    }

    public void setCtrctQryRsltCd(String ctrctQryRsltCd) {
        CtrctQryRsltCd = ctrctQryRsltCd;
    }

    public String getCtrctQryRsltInf() {
        return CtrctQryRsltInf;
    }

    public void setCtrctQryRsltInf(String ctrctQryRsltInf) {
        CtrctQryRsltInf = ctrctQryRsltInf;
    }
}
