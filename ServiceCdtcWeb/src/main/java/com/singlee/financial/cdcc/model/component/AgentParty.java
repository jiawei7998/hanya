package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 代理组件
 */
public class AgentParty implements Serializable {
    /**
     * 代理类型
     */
    private String AgtPtyTP;
    /**
     * 代理机构标识
     */
    private PartyIdentification30 AgtPtyId;

    public String getAgtPtyTP() {
        return AgtPtyTP;
    }

    public void setAgtPtyTP(String agtPtyTP) {
        AgtPtyTP = agtPtyTP;
    }

    public PartyIdentification30 getAgtPtyId() {
        return AgtPtyId;
    }

    public void setAgtPtyId(PartyIdentification30 agtPtyId) {
        AgtPtyId = agtPtyId;
    }
}
