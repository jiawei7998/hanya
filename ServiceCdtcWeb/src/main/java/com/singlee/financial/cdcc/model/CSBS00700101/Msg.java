package com.singlee.financial.cdcc.model.CSBS00700101;

import com.singlee.financial.cdcc.model.component.PartyIdentification;

import java.io.Serializable;

/**
 * 报文标识
 */
public class Msg implements Serializable {
    /**
     * 交易流水标识
     */
    private String TxFlowId;
    /**
     * 经办人
     */
    private PartyIdentification Oprtr;
    /**
     * 优先级
     */
    private String Prty;
    /**
     * 报文创建时间
     */
    private String CreDtTm;

    public String getTxFlowId() {
        return TxFlowId;
    }

    public void setTxFlowId(String txFlowId) {
        TxFlowId = txFlowId;
    }

    public PartyIdentification getOprtr() {
        return Oprtr;
    }

    public void setOprtr(PartyIdentification oprtr) {
        Oprtr = oprtr;
    }

    public String getPrty() {
        return Prty;
    }

    public void setPrty(String prty) {
        Prty = prty;
    }

    public String getCreDtTm() {
        return CreDtTm;
    }

    public void setCreDtTm(String creDtTm) {
        CreDtTm = creDtTm;
    }
}
