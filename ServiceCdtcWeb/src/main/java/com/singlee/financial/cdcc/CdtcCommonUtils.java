package com.singlee.financial.cdcc;

import cdc.dmsg.client.DClient;
import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00900101.SysXcptn;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.util.EncodingUtil;
import com.singlee.xstream.utils.XmlUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 中债发送客户端
 */
public class CdtcCommonUtils {

    /**
     * 发送中债前置机方法
     * @param requestPakcet
     * @param <K>
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <K extends EsbBodyPacket> String send(
            EsbPacket<K> requestPakcet ) throws Exception {

        Log log = LogFactory.getLog(CdtcCommonUtils.class);
        log.debug("进入中债前置机发送方法");

        String xml = null;
        // 将esb的报文对象转换成xml文件
        xml = XmlUtils.toXml(requestPakcet);
        String ret = new SAXReader().read(new StringReader(xml)).getRootElement().element("Document").asXML();
        String requestXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + ret;
        log.info("中债请求报文:"  + requestXml);
        System.out.println("中债请求报文:"  + requestXml);

        // 调用服务器方法并返回结果
        DClient dClient = new DClient(CdtcProperties.wasIp);
        dClient.setTimeout(Integer.parseInt(CdtcProperties.timeOut));
        String backXml = dClient.sendMsg(XmlFormat.format(requestXml));
        backXml = new String(backXml.getBytes(EncodingUtil.getEncoding(backXml)), "GBK");
        log.info("Esb响应对象待转换XML:" + backXml);
        System.out.println("Esb响应对象待转换XML:" + backXml);
        return backXml;

    }

    /**
     * 将xml报文转化为 实体
     * @param backXml
     * @param responsePakcet
     * @param <K>
     * @param <T>
     * @return
     * @throws IllegalAccessException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     */
    public static <K extends EsbBodyPacket, T extends EsbBodyPacket> EsbPacket<T> transformRep(String backXml,EsbPacket<T> responsePakcet) throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        return (EsbPacket<T>)XmlUtils.fromXML(backXml, responsePakcet);
    }

    public static Map<String,Object> cdtcFailPacket(String backXml) throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Map<String,Object> map = new HashMap<>();
        String errorMessage = null;
        String detaiErrorlMessage = null;

        EsbPacket<com.singlee.financial.cdcc.model.CSBS00900101.Body> failePacket =
                EsbPacket.initEsbPacket(com.singlee.financial.cdcc.model.CSBS00900101.Body.class, MsgHeader.class, "Root");
        failePacket = CdtcCommonUtils.transformRep(backXml,failePacket);
        SysXcptn sysXcptn = failePacket.getBody().getSysXcptn();
        List<Extension> xtnsnList = failePacket.getBody().getXtnsn();

        errorMessage = sysXcptn!=null? sysXcptn.getXcptnRtrCd()+"--"+sysXcptn.getXcptnRtrTx():"";
        detaiErrorlMessage = xtnsnList!=null?xtnsnList.get(0).getXtnsnTxt():"";

        map.put("errorMessage",errorMessage);
        map.put("detaiErrorlMessage",detaiErrorlMessage);
        return map;

    }

    /**
     * 返回报文的处理状态
     * BJ000000代表是处理成功
     * BJ999999代表是处理失败
     * @param TxRsltCd
     * @return
     */
    public static  boolean msgReturn(String TxRsltCd){
        boolean flag = true;
        if(TxRsltCd.equals("BJ000000")){
            flag =  true;
        }else {
            flag =  false;
        }
        return flag;
    }

    /**
     *  根据交易方向判断p-收券方确认、s-付券方确认
     * @param ps
     * @return
     */
    public static String getPsOprtrSts(String ps){
        String oprtrSts = "";
        if(ps != null){
            if(ps.equals("P")){
                oprtrSts = CdtcDictConstants.OperatorStatusCode.OS06;
            }else{
                oprtrSts = CdtcDictConstants.OperatorStatusCode.OS05;
            }
        }
        return oprtrSts;
    }

    /**
     * 获取报文类型
     * @param backXml
     * @return
     * @throws IOException
     * @throws DocumentException
     */
    public static String getMsgTp(String backXml) throws IOException, DocumentException {
        SAXReader reader = new SAXReader();
        org.dom4j.Document xmlDoc = reader
                .read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));
        String msgTp = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp")
                .getText();
        return msgTp;
    }


    /**
     * 生成流水号
     * @return
     */
    public static String convertDateToYYYYMMDDHHMMSS() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    /**
     * 生成心跳
     * @return
     */
    public static String convertDateToHHMMSS() {
        return new SimpleDateFormat("HHmmss").format(new Date());
    }

    /**
     * 获取中债发送时间
     * @return
     */
    public static String getCdtcUtcTimes(){
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
        sdf.setDateFormatSymbols(new DateFormatSymbols(Locale.US));

        TimeZone timezone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timezone);
        return sdf.format(calendar.getTime());
    }


}
