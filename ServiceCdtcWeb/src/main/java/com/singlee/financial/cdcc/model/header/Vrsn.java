package com.singlee.financial.cdcc.model.header;

import java.io.Serializable;

/**
 * 报文版本标识
 */
public class Vrsn implements Serializable {

    /**
     * 报文版本标识号
     */
    private String VrsnID;

    public String getVrsnID() {
        return VrsnID;
    }

    public void setVrsnID(String vrsnID) {
        VrsnID = vrsnID;
    }
}
