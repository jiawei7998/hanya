package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 适用的批量下载功能编号：
 * BF13
 * BF14
 * BF15
 * BF16
 * BF17
 * BF19
 * BF22
 */
public class BatchCondition3 implements Serializable {
    /**
     * 开始日期
     */
    private String StrtDt;
    /**
     * 结束日期
     */
    private String EndDt;
    /**
     * 待偿期
     */
    private String YrsToMtrty;

    public String getStrtDt() {
        return StrtDt;
    }

    public void setStrtDt(String strtDt) {
        StrtDt = strtDt;
    }

    public String getEndDt() {
        return EndDt;
    }

    public void setEndDt(String endDt) {
        EndDt = endDt;
    }

    public String getYrsToMtrty() {
        return YrsToMtrty;
    }

    public void setYrsToMtrty(String yrsToMtrty) {
        YrsToMtrty = yrsToMtrty;
    }
}
