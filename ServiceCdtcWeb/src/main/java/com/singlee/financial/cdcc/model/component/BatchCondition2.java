package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 适用的批量下载功能编号：
 * BF11
 * BF12
 * BF18
 * BF20
 * BF21
 * BF23
 * BF24
 */
public class BatchCondition2 implements Serializable {
    /**
     * 开始日期
     */
    private String StrtDt;
    /**
     * 结束日期
     */
    private String EndDt;

    public String getStrtDt() {
        return StrtDt;
    }

    public void setStrtDt(String strtDt) {
        StrtDt = strtDt;
    }

    public String getEndDt() {
        return EndDt;
    }

    public void setEndDt(String endDt) {
        EndDt = endDt;
    }
}
