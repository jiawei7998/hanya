package com.singlee.financial.cdcc.model.component;

import java.io.Serializable;

/**
 * 适用的批量下载功能编号：
 * BF25
 */
public class BatchCondition5 implements Serializable {
    /**
     * 账户编号
     */
    private String Id;
    /**
     * 日期
     */
    private String Dt;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDt() {
        return Dt;
    }

    public void setDt(String dt) {
        Dt = dt;
    }
}
