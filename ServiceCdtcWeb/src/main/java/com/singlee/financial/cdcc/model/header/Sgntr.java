package com.singlee.financial.cdcc.model.header;

import java.io.Serializable;
import java.util.List;

/**
 * 报文签名
 */
public class Sgntr implements Serializable {
    /**
     * 结算成员签名
     */
    private List<String> SttlmMmbSgntr;
    /**
     * CSBS签名
     */
    private List<String> CSBSSgntr;

    public List<String> getSttlmMmbSgntr() {
        return SttlmMmbSgntr;
    }

    public void setSttlmMmbSgntr(List<String> sttlmMmbSgntr) {
        SttlmMmbSgntr = sttlmMmbSgntr;
    }

    public List<String> getCSBSSgntr() {
        return CSBSSgntr;
    }

    public void setCSBSSgntr(List<String> CSBSSgntr) {
        this.CSBSSgntr = CSBSSgntr;
    }
}
