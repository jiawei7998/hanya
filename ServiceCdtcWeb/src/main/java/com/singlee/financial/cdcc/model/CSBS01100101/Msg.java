package com.singlee.financial.cdcc.model.CSBS01100101;

import java.io.Serializable;

/**
 * 报文标识
 * @author copysun
 */
public class Msg implements Serializable {

	/**
	 * 交易流水标识
	 */
	private String TxFlowId;

	/**
	 * 优先级
	 */
	private String Prty;
	/**
	 * 报文创建时间
	 */
	private String CreDtTm;

	public String getTxFlowId() {
		return TxFlowId;
	}

	public void setTxFlowId(String txFlowId) {
		TxFlowId = txFlowId;
	}

	public String getPrty() {
		return Prty;
	}

	public void setPrty(String prty) {
		Prty = prty;
	}

	public String getCreDtTm() {
		return CreDtTm;
	}

	public void setCreDtTm(String creDtTm) {
		CreDtTm = creDtTm;
	}
}
