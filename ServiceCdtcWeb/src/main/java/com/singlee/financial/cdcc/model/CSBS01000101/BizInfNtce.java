package com.singlee.financial.cdcc.model.CSBS01000101;

import java.io.Serializable;

/**
 * 业务消息通知
 * @author copysun
 */
public class BizInfNtce implements Serializable {
	/**
	 * 消息标识号
	 */
	private String InfId;
	/**
	 * 消息种类
	 */
	private String InfTp;
	/**
	 * 关键字
	 */
	private String Key;
	/**
	 * 摘要
	 */
	private String Abstract;

	public String getInfId() {
		return InfId;
	}

	public void setInfId(String infId) {
		InfId = infId;
	}

	public String getInfTp() {
		return InfTp;
	}

	public void setInfTp(String infTp) {
		InfTp = infTp;
	}

	public String getKey() {
		return Key;
	}

	public void setKey(String key) {
		Key = key;
	}

	public String getAbstract() {
		return Abstract;
	}

	public void setAbstract(String anAbstract) {
		Abstract = anAbstract;
	}
}
