package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CSBSCodeBeanList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<CSBSCodeBean> csbsCodeList = new ArrayList<CSBSCodeBean>();

	/**
	 * @return the csbsCodeList
	 */
	public List<CSBSCodeBean> getCsbsCodeList() {
		return csbsCodeList;
	}

	/**
	 * @param csbsCodeList the csbsCodeList to set
	 */
	public void setCsbsCodeList(List<CSBSCodeBean> csbsCodeList) {
		this.csbsCodeList = csbsCodeList;
	}
	
	
	public CSBSCodeBeanList(List<CSBSCodeBean> csbsCodeList){
		super();
		this.csbsCodeList = csbsCodeList;
	}

	
	 /**
     * 根据下标返回机构对象
     * @param code
     * @return
     */
    public CSBSCodeBean getCSBSCodeBean(String code){
        int index = indexOf(code);
        return index >=0 ? (CSBSCodeBean)csbsCodeList.get(index) : null;
    }
    
    /**
     * 查找机构下标，没有找到返回-1
     * @param code
     * @return
     */
    public int indexOf(String code){
        for(int i=0; i<csbsCodeList.size(); i++){
        	CSBSCodeBean csbs = (CSBSCodeBean)csbsCodeList.get(i);
            if(csbs.getTYPE().concat(csbs.getCODE()).equals(code)){
                return i;
            }
        }
        return -1;
    }
    
    
    /**
     * 添加
     * @param SL_WF_PROD
     */
    public void add(CSBSCodeBean prod){
    	csbsCodeList.add(prod);
    }
    
    /**
     * 删除
     * @param SL_WF_PROD
     */
    public int del(CSBSCodeBean csbs){
        int index = indexOf(csbs.getTYPE().concat(csbs.getCODE()));
        if(index != -1) {
        	csbsCodeList.remove(index);}
        return index;
    }
    
    /**
     * 更新 = 删除->添加
     * @param SL_WF_PROD
     */
    public void upd(CSBSCodeBean prod){
        int index = del(prod);
        csbsCodeList.add(index, prod);
    }
}
