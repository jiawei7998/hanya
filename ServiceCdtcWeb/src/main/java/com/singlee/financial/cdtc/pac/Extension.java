/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class Extension extends Group {
	
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("XtnsnTxt", Field.class);
	}
	
	public Extension(String tag){
		super(tag,tag,new String[]{"XtnsnTxt"});
	}
	public Extension(String tag,String delim){
		super(tag,tag,new String[]{"XtnsnTxt"});
	}
	public void set(Max2048Text field){
		setField(field);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

}
