/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 * 
 */
public class TransactionMsg extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static {
		keyClassMap.put("TxFlowId", Field.class);
		keyClassMap.put("Oprtr", PartyIdentification.class);
		keyClassMap.put("Chckr", PartyIdentification.class);
		keyClassMap.put("Prty", Field.class);
		keyClassMap.put("CreDtTm", Field.class);
		keyClassMap.putAll(PartyIdentification.keyClassMap);
	}

	public TransactionMsg(String tag) {
		super(tag, tag, new String[] { "TxFlowId", "Oprtr", "Chckr", "Prty",
				"CreDtTm" });
	}

	public TransactionMsg(String tag, String delim) {

		super(tag, tag, new String[] { "TxFlowId", "Oprtr", "Chckr", "Prty",
				"CreDtTm" });
	}

	/*****************************/
	public void set(Max35Text value) {
		setField(value);
	}

	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max35Text getMax35Text() throws FieldNotFound {
		Max35Text value = new Max35Text("TxFlowId");
		getField(value);

		return value;
	}

	public boolean isSet(Max35Text field) {
		return isSetField(field);
	}

	public boolean isSetMax35Text() {
		return isSetField("TxFlowId");
	}

	/**************************/
	public void set(PartyIdentification group) {
		addGroup(group);
	}

	public boolean isSet(PartyIdentification field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetPartyIdentification() {
		return isSetField("Oprtr");
	}

	/************************/
	public void set(ISODateTime date) {
		setField(date);
	}

	/**************************/
	public void set(Priority4Code prty) {
		setField(prty);
	}

	public Priority4Code get(Priority4Code value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Priority4Code getPriority4Code() throws FieldNotFound {
		Priority4Code value = new Priority4Code("TxFlowId");
		getField(value);

		return value;
	}

	public boolean isSet(Priority4Code field) {
		return isSetField(field);
	}

	public boolean isSetPriority4Code() {
		return isSetField("TxFlowId");
	}

	/*******************************/
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
