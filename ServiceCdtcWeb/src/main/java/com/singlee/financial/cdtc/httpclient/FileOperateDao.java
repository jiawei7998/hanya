package com.singlee.financial.cdtc.httpclient;

public interface FileOperateDao {

	/**
	 * 保存XML报文保存到SERVER端
	 * @param data      写入文件数据
	 * @param filename  文件路径+文件名
	 * @throws Exception
	 */
	public void messageXMLFileSaveLocal(String data,String path, String filename, String date) throws Exception;
	
	/**
	 * 判断当天的文件目录是否存在
	 * 不存在则创建 
	 * @param localPath
	 * @param filename
	 * @param date
	 * @throws Exception
	 */
	public void FileIsNotExist(String localPath,String filename,String date) throws Exception;
}
