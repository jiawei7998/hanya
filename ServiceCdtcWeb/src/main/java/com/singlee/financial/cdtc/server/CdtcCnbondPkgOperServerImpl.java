package com.singlee.financial.cdtc.server;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.bean.CnbondPkgDetailBean;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.financial.cdtc.CdtcCnbondPkgOperServer;
import com.singlee.financial.cdtc.dao.CnbondManagerDao;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementInstruction;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.util.CnbondPkgToBean;
import com.singlee.financial.cdtc.util.XmlFormat;
import com.singlee.financial.pojo.component.RetStatusEnum;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.util.*;

/***
 * 
 * 中债直连 - CDTC 结算报文管理
 *
 */

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcCnbondPkgOperServerImpl implements CdtcCnbondPkgOperServer {
	
	@Autowired
	CnbondManagerDao cnbondManagerDao;
	
	private  Logger logger = LoggerFactory.getLogger(CdtcCnbondPkgOperServerImpl.class);
	
	private List<AbstractSlIntfcCnbondPkgId> listSpsh; //初始查询条件集合
	
	private List<AbstractSlIntfcCnbondPkgId> listSpshend;//报文体条件查询后的数据集合
	

	@Value("#{cdtcProperties['cdtc.jsBankAcct']}")
	private  String jsBankAcct ; //本方机构账号
	
	/***
	 * 结算报文查询-列表页面
	 */
	@Override
	public Pair<SmtOutBean,List<AbstractSlIntfcCnbondPkgId>> queryCdtcCnbondPkg(Map<String, String> map) throws  Exception {
		logger.info("正在向CDTC查询结算报文数据,请稍候......");
		Pair<SmtOutBean,List<AbstractSlIntfcCnbondPkgId>> pair = new Pair<SmtOutBean,List<AbstractSlIntfcCnbondPkgId>>();
		SmtOutBean smtOutBean = new SmtOutBean();
		
		String txtTxid = map.get("txtTxid");  //业务标识号
		String txtBiztp = map.get("txtBiztp");//业务类别
		String txtSecid = map.get("txtSecid");//债券代码
		String txtSerchType = map.get("txtSerchType"); //状态类别
		String txtMsgTypeName=map.get("txtMsgTypeName");//报文名称
		
		//如果页面条件框有值，则从页面拿出条件存入faceMap
		HashMap<Integer, String> faceMap = new HashMap<Integer, String>();
		int count1 = 0;
		if(StringUtils.isNotBlank(txtTxid)){
			faceMap.put(count1++,txtTxid.trim());
		}
		if(StringUtils.isNotBlank(txtBiztp)){
			faceMap.put(count1++,txtBiztp);
		}
		if(StringUtils.isNotBlank(txtSecid)){
			faceMap.put(count1++,txtSecid); 
		}
		if(StringUtils.isNotBlank(txtSerchType)){
			faceMap.put(count1++, txtSerchType);
		}
		
		
		String txtMsgType = map.get("txtMsgType"); //报文类别
		String txtDateBegin = map.get("txtDateBegin");//起始日期
		String txtDateEnd = map.get("txtDateEnd");    //截至日期
		
		try {
			if(StringUtils.isNotBlank(txtMsgType) || StringUtils.isNotBlank(txtDateBegin) || StringUtils.isNotBlank(txtDateEnd)||StringUtils.isNotBlank(txtMsgTypeName)){
				logger.info("请求初始查询条件报文");
				listSpsh = cnbondManagerDao.queryAllCSBSBeanFromDbByTrem(txtMsgType,txtDateBegin,txtDateEnd,txtMsgTypeName);
			}else{
				logger.info("请求初始查询条件报文");
				listSpsh = cnbondManagerDao.queryAllCSBSBeanFromDb();
			}
	
				logger.info("请求条件查询后的报文体数据");
				listSpshend = new ArrayList<AbstractSlIntfcCnbondPkgId>();
				HashMap<String, Boolean> msgXmlMap = null;
				for(AbstractSlIntfcCnbondPkgId cbstract1 : listSpsh){
					SAXReader reader = new SAXReader();
					//从报文体中拿出需要匹配的条件存入msgXmlMap
					msgXmlMap = new HashMap<String, Boolean>();
					/**
					 * 读取请求报文XML 反解析报文
					 */
					logger.info("读取请求报文XML："+cbstract1.getMsgxml()+ "反解析报文");
					org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(cbstract1.getMsgxml()).getBytes("UTF-8"))));  
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
					CommonDistributionSettlementInstruction commonDistributionSettlementInstruction = null;
					if("CSBS.002.001.01".equals(cbstract1.getMsgtype())){
						spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
					}
					else if("CSBS.001.001.01".equals(cbstract1.getMsgtype())){
						spotSettlementInstruction.unionHashMap(CommonDistributionSettlementInstruction.type);
					}
					System.out.println("xmlDoc.getRootElement():"+xmlDoc.getRootElement().getName());
					if("Document".equals(xmlDoc.getRootElement().getName())){
						  Document document = DocumentHelper.createDocument();
						  Element root = document.addElement("Root");
						  root.add(xmlDoc.getRootElement());
						  xmlDoc=document;
						spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					}else{
						spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					}
					
					//修改
					if(spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for(Group group : groupsList) {
							// 保存xml文件
							StringBuffer xmlBuffer = new StringBuffer();
							group.toXmlForParent(xmlBuffer);
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));
							//判断报文类别获取相应解析
							if("CSBS.002.001.01".equals(cbstract1.getMsgtype())){
								logger.info("开始解析报文，报文类别为："+cbstract1.getMsgtype().trim());
								commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
								CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
								if(null != commonDistributionSettlementStatusReport) {
									if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
										//指令标识
										Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
										msgXmlMap.put(stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString(),true);
									}
									if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
										Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
										//业务标识号
										if(sttlmDtl.getfieldsForFieldMap().get("TxId")!=null){
											msgXmlMap.put(sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString(),true);
										}
										//业务类别
										if(sttlmDtl.getfieldsForFieldMap().get("BizTp")!=null){
											msgXmlMap.put(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString(),true);
											cbstract1.setBizTp(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString());
										}
										//获取交易对手
									    String[] arr=new String[2];		
										//债券账号
										if(sttlmDtl.getfieldsForFieldMap().get("Bd1")!=null){
											Element element = xmlDoc.getRootElement().element("Document").element("SttlmDtl");
											@SuppressWarnings("unchecked")
											List<Element> elements = element.elements();
											for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();) {
												Element element2 = (Element)iterator.next();
												if(StringUtils.trimToEmpty(element2.getName()).endsWith("Bd1")){
													@SuppressWarnings("unchecked")
													List<Element> elements1 = element2.elements();
													for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();)
													{
														Element element3 = (Element)iterator1.next();
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("BdId")){
															msgXmlMap.put(element3.getText(),true);
														}
													}
												}
												if(StringUtils.trimToEmpty(element2.getName()).endsWith("TakAcct")){
													@SuppressWarnings("unchecked")
													List<Element> elements1 = element2.elements();
													for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();) {
														Element element3 = (Element)iterator1.next();
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Nm")){
															arr[0]=element3.getText();
														}
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Id")){
															if(jsBankAcct.endsWith(element3.getText())){
																arr[0]=null;
																break;
															}
														}
													}
												}
												if(StringUtils.trimToEmpty(element2.getName()).endsWith("GivAcct")){
													@SuppressWarnings("unchecked")
													List<Element> elements1 = element2.elements();
													for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();) {
														Element element3 = (Element)iterator1.next();
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Nm")){
															arr[1]=element3.getText();
														}
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Id")){
															if(jsBankAcct.endsWith(element3.getText())){
																arr[1]=null;
																break;
															}
														}
													}
												}
											}
										}
										cbstract1.setCust(arr[0]==null?arr[1]:arr[0]);
									}
								}
							}else if("CSBS.001.001.01".equals(cbstract1.getMsgtype())){
								logger.info("开始解析报文，报文类别为："+cbstract1.getMsgtype().trim());
								msgXmlMap = new HashMap<String, Boolean>();
								commonDistributionSettlementInstruction = new CommonDistributionSettlementInstruction("Document");
								CommonDistributionSettlementInstruction.getElementList(document.getRootElement(), commonDistributionSettlementInstruction);
								if(commonDistributionSettlementInstruction != null){
									if(commonDistributionSettlementInstruction.hasGroup("OprtrFlg")) {
										//操作标识
										Group stsFlg = commonDistributionSettlementInstruction.getGroups("OprtrFlg").get(0);
										msgXmlMap.put(stsFlg.getfieldsForFieldMap().get("OprtrSts").getObject().toString(),true);
									}
									if(commonDistributionSettlementInstruction.hasGroup("SttlmDtl")) {
										Group sttlmDtl = commonDistributionSettlementInstruction.getGroups("SttlmDtl").get(0);
										//业务标识号
										if(sttlmDtl.getfieldsForFieldMap().get("TxId")!=null){
											msgXmlMap.put(sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString(),true);
										}
										//业务类别
										if(sttlmDtl.getfieldsForFieldMap().get("BizTp")!=null){
											msgXmlMap.put(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString(),true);
											cbstract1.setBizTp(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString());
										}
										//获取交易对手
									    String[] arr=new String[2];								
										//债券账号
										if(sttlmDtl.getfieldsForFieldMap().get("Bd1")!=null){
											Element element = xmlDoc.getRootElement().element("Document").element("SttlmDtl");
											@SuppressWarnings("unchecked")
											List<Element> elements = element.elements();
											for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();) {
												Element element2 = (Element)iterator.next();
												if(StringUtils.trimToEmpty(element2.getName()).endsWith("Bd1")){
													@SuppressWarnings("unchecked")
													List<Element> elements1 = element2.elements();
													for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();) {
														Element element3 = (Element)iterator1.next();
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("BdId")){
															msgXmlMap.put(element3.getText(),true);
														}
													}
												}
												if(StringUtils.trimToEmpty(element2.getName()).endsWith("TakAcct")){
													@SuppressWarnings("unchecked")
													List<Element> elements1 = element2.elements();
													for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();) {
														Element element3 = (Element)iterator1.next();
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Nm")){
															arr[0]=element3.getText();
														}
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Id")){
															if(jsBankAcct.endsWith(element3.getText())){
																arr[0]=null;
																break;
															}
														}
													}
												}
												if(StringUtils.trimToEmpty(element2.getName()).endsWith("GivAcct")){
													@SuppressWarnings("unchecked")
													List<Element> elements1 = element2.elements();
													for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();) {
														Element element3 = (Element)iterator1.next();
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Nm")){
															arr[1]=element3.getText();
														}
														if(StringUtils.trimToEmpty(element3.getName()).endsWith("Id")){
															if(jsBankAcct.endsWith(element3.getText())){
																arr[1]=null;
																break;
															}
														}
													}
												}
											}
										}
										cbstract1.setCust(arr[0]==null?arr[1]:arr[0]);
									}
								}
							}
						}
					}
					//匹配查询条件和报文里面的数据，如果存在，则添加
					if(msgXmlMap!=null){
						
					}
					boolean isright = false;
					if(msgXmlMap!=null){
						for(int i=0;i<faceMap.size();i++){
							if(!msgXmlMap.containsKey(faceMap.get(i))){
								smtOutBean.setRetStatus(RetStatusEnum.F);
								smtOutBean.setRetMsg("该查询条件["+faceMap.get(i)+"]没有匹配到相应的报文数据，条件查询请求失败");
								pair.setLeft(smtOutBean);
								logger.info("该查询条件["+faceMap.get(i)+"]没有匹配到相应的报文数据，条件查询请求失败");
								isright=true;
								break;
							}
						}
					}
					if(!isright){
						listSpshend.add(cbstract1);
					}
				}
				logger.info("查询条件和报文里面的数据匹配成功，条件查询请求成功");
				smtOutBean.setRetStatus(RetStatusEnum.S);
				smtOutBean.setRetMsg("查询条件和报文里面的数据匹配成功，条件查询请求成功");
				pair.setLeft(smtOutBean);
				pair.setRight(listSpshend);
					
		} catch (Exception e) {
			throw e;
		}
		return pair;
	}


	
	/***
	 * 结算报文查询-详情页面
	 */
	@Override
	public Pair<SmtOutBean, CnbondPkgDetailBean> getCnbondPkgDetail(AbstractSlIntfcCnbondPkgId cbst1) throws RemoteConnectFailureException, Exception {
		Pair<SmtOutBean, CnbondPkgDetailBean> pair = new Pair<SmtOutBean, CnbondPkgDetailBean>();
		SmtOutBean smtOutBean = new SmtOutBean();
		CnbondPkgDetailBean cnbondPkgDetailBean = new CnbondPkgDetailBean();
		
		if(null != cbst1) {
			try{
				SAXReader reader = new SAXReader();
				/**
				 * 读取请求报文XML 反解析报文
				 */
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(cbst1.getMsgxml()).getBytes("UTF-8"))));  
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
				CommonDistributionSettlementInstruction commonDistributionSettlementInstruction = null;
				if("CSBS.002.001.01".equals(cbst1.getMsgtype().trim())){
					spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
				}
				else if("CSBS.001.001.01".equals(cbst1.getMsgtype().trim())){
					spotSettlementInstruction.unionHashMap(CommonDistributionSettlementInstruction.type);
				}
				
				if("Document".equals(xmlDoc.getRootElement().getName())){
					  Document document = DocumentHelper.createDocument();
					  Element root = document.addElement("Root");
					  root.add(xmlDoc.getRootElement());
					  xmlDoc=document;
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				}else{
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				}
				
				if(spotSettlementInstruction.hasGroup("Document")) {
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for(Group group : groupsList) {
						// 保存xml文件
						StringBuffer xmlBuffer = new StringBuffer();
						group.toXmlForParent(xmlBuffer);
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8")))); 
						//判断报文类别获取相应解析
						if("CSBS.002.001.01".equals(cbst1.getMsgtype().trim())){
							logger.info("开始解析报文，报文类别为："+cbst1.getMsgtype().trim());
							commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
							CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
							if(null != commonDistributionSettlementStatusReport) {
								if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
									Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
									if(sttlmDtl.getfieldsForFieldMap().get("BizTp")!=null){
										if("BT01".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
											cnbondPkgDetailBean = CnbondPkgToBean.zLDetail(sttlmDtl);
											
										}else if("BT02".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
											cnbondPkgDetailBean = CnbondPkgToBean.zLRepoDetail(sttlmDtl);
											
										}else if("BT03".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
											cnbondPkgDetailBean = CnbondPkgToBean.zLRepoMDDetail(sttlmDtl);
										}
									}else{
										smtOutBean.setRetStatus(RetStatusEnum.F);
										smtOutBean.setRetMsg("业务类型为空，无法展示详情！");
										pair.setLeft(smtOutBean);
										logger.info("业务类型为空，无法展示详情！");
										return pair;
									}
									
									smtOutBean.setRetStatus(RetStatusEnum.S);
									smtOutBean.setRetMsg("查询结算报文详情成功");
									pair.setLeft(smtOutBean);
									pair.setRight(cnbondPkgDetailBean);
									logger.info("查询结算报文详情成功！");
								}
							}
						}else if("CSBS.001.001.01".equals(cbst1.getMsgtype().trim())){
							logger.info("开始解析报文，报文类别为："+cbst1.getMsgtype().trim());
							commonDistributionSettlementInstruction = new CommonDistributionSettlementInstruction("Document");
							CommonDistributionSettlementInstruction.getElementList(document.getRootElement(), commonDistributionSettlementInstruction);
							if(commonDistributionSettlementInstruction != null){
								if(commonDistributionSettlementInstruction.hasGroup("SttlmDtl")) {
									Group sttlmDtl = commonDistributionSettlementInstruction.getGroups("SttlmDtl").get(0);
									if(sttlmDtl.getfieldsForFieldMap().get("BizTp")!=null){
										if("BT01".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
											cnbondPkgDetailBean = CnbondPkgToBean.zLDetail(sttlmDtl);
											
										}else if("BT02".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
											cnbondPkgDetailBean = CnbondPkgToBean.zLRepoDetail(sttlmDtl);
											
										}else if("BT03".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
											cnbondPkgDetailBean = CnbondPkgToBean.zLRepoMDDetail(sttlmDtl);
										}
									}else{
										smtOutBean.setRetStatus(RetStatusEnum.F);
										smtOutBean.setRetMsg("业务类型为空，无法展示详情！");
										pair.setLeft(smtOutBean);
										logger.info("业务类型为空，无法展示详情！");
										return pair;
									}
									
									smtOutBean.setRetStatus(RetStatusEnum.S);
									smtOutBean.setRetMsg("查询结算报文详情成功");
									pair.setLeft(smtOutBean);
									pair.setRight(cnbondPkgDetailBean);
									logger.info("查询结算报文详情成功！");
								}
							}
						}
					}
				}
			}catch (Exception e) {
				throw e;
			}
		}
		
		return pair;
	}


	
	
	/***
	 * 获取结算报文业务类型
	 */
	@Override
	public Pair<SmtOutBean, String> getCnbondPkgBizTp(AbstractSlIntfcCnbondPkgId cbst1) throws RemoteConnectFailureException, Exception {
		Pair<SmtOutBean, String> pair = new Pair<SmtOutBean, String>();
		SmtOutBean smtOutBean = new SmtOutBean();
		try{
			SAXReader reader = new SAXReader();
			/**
			 * 读取请求报文XML 反解析报文
			 */
			org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(cbst1.getMsgxml()).getBytes("UTF-8"))));  
			SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
			CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
			CommonDistributionSettlementInstruction commonDistributionSettlementInstruction = null;
			if("CSBS.002.001.01".equals(cbst1.getMsgtype())){
				spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
			}
			else if("CSBS.001.001.01".equals(cbst1.getMsgtype())){
				spotSettlementInstruction.unionHashMap(CommonDistributionSettlementInstruction.type);
			}
			if("Document".equals(xmlDoc.getRootElement().getName())){
				  Document document = DocumentHelper.createDocument();
				  Element root = document.addElement("Root");
				  root.add(xmlDoc.getRootElement());
				  xmlDoc=document;
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
			}else{
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
			}
			if(spotSettlementInstruction.hasGroup("Document"))
			{
				List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
				for(Group group : groupsList)
				{
					// 保存xml文件
					StringBuffer xmlBuffer = new StringBuffer();
					group.toXmlForParent(xmlBuffer);
					Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8")))); 
					//判断报文类别获取相应解析
					if("CSBS.002.001.01".equals(cbst1.getMsgtype())){
						logger.info("开始解析报文，报文类别为："+cbst1.getMsgtype().trim());
						commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
						CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
						if(null != commonDistributionSettlementStatusReport)
						{
							if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
								Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
								if(sttlmDtl.getfieldsForFieldMap().get("BizTp")!=null){
									//现券列表
									if("BT01".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
										pair.setRight("BT01");
									}
									//质押式回购列表
									else if("BT02".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
										pair.setRight("BT02");
									}
									//买断式回购列表
									else if("BT03".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
										pair.setRight("BT03");
									}
								}else{
									smtOutBean.setRetStatus(RetStatusEnum.F);
									smtOutBean.setRetMsg("业务类型为空，无法展示详情！");
									pair.setLeft(smtOutBean);
									logger.info("业务类型为空，无法展示详情！");
									return pair;
								}
								
								smtOutBean.setRetStatus(RetStatusEnum.S);
								smtOutBean.setRetMsg("查询结算报文业务类型成功");
								pair.setLeft(smtOutBean);
								logger.info("查询结算报文业务类型成功！");
								
							}
						}
					}else if("CSBS.001.001.01".equals(cbst1.getMsgtype())){
						logger.info("开始解析报文，报文类别为："+cbst1.getMsgtype().trim());
						commonDistributionSettlementInstruction = new CommonDistributionSettlementInstruction("Document");
						CommonDistributionSettlementInstruction.getElementList(document.getRootElement(), commonDistributionSettlementInstruction);
						if(commonDistributionSettlementInstruction != null){
							
							if(commonDistributionSettlementInstruction.hasGroup("SttlmDtl"))
							{
								Group sttlmDtl = commonDistributionSettlementInstruction.getGroups("SttlmDtl").get(0);
								if(sttlmDtl.getfieldsForFieldMap().get("BizTp")!=null){
									//现券列表
									if("BT01".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
										pair.setRight("BT01");
									}
									//质押式回购列表
									else if("BT02".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
										pair.setRight("BT02");
									}
									//买断式回购列表
									else if("BT03".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){
										pair.setRight("BT03");
									}
								}else{
									smtOutBean.setRetStatus(RetStatusEnum.F);
									smtOutBean.setRetMsg("业务类型为空，无法展示详情！");
									pair.setLeft(smtOutBean);
									logger.info("业务类型为空，无法展示详情！");
									return pair;
								}
								
								smtOutBean.setRetStatus(RetStatusEnum.S);
								smtOutBean.setRetMsg("查询结算报文业务类型成功");
								pair.setLeft(smtOutBean);
								logger.info("查询结算报文业务类型成功！");
							}
						}
					}
				}
			}
		}catch (Exception e) {
			throw e;
		}
		
		return pair;
	}
}
