package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 */
public class QueryDate extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("StartDt", Field.class);
		keyClassMap.put("EndDt", Field.class);
	}
	
	public QueryDate(String tag){
		super(tag,tag,new String[]{"StartDt","EndDt"});
	}
	public QueryDate(String tag,String delim){
		super(tag,tag,new String[]{"StartDt","EndDt"});
	}
	
	public void set(ISODate field){
		setField(field);
	}
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
