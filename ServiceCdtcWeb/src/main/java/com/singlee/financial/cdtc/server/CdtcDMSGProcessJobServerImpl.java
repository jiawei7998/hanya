package com.singlee.financial.cdtc.server;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.singlee.financial.cdtc.CdtcDMSGProcessJobServer;
import com.singlee.financial.cdtc.util.ExceptionUtil;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcDMSGProcessJobServerImpl implements CdtcDMSGProcessJobServer{

    private Logger logger = LoggerFactory.getLogger(CdtcDMSGProcessJobServerImpl.class);
	
    // 队列
	private BlockingQueue<String> msgQueue;

    // 本地中债报文文件保存路径
	@Value("#{cdtcProperties['cdtc.xmlDMSGPath']}")
	private String xmlDMSGPath;
    
	// 中债前置IP
	@Value("#{cdtcProperties['cdtc.FTPIP']}")
	private String FTPIP;
	
	// 前置端口
	@Value("#{cdtcProperties['cdtc.FTPPORT']}")
	private String FTPPORT;
	
	// 中债前置FTP用户名
	@Value("#{cdtcProperties['cdtc.FTPUSER']}")
	private String FTPUSER;
	
	// 中债前置FTP密码
	@Value("#{cdtcProperties['cdtc.FTPPSWD']}")
	private String FTPPSWD;
	
	// 中债前置报文文件生成路径 
	@Value("#{cdtcProperties['cdtc.ftpFilePath']}")
	private String ftpFilePath;
	
	/**
	 * @return the msgQueue
	 */
	public BlockingQueue<String> getMsgQueue() {
		return msgQueue;
	}

	/**
	 * @param msgQueue the msgQueue to set
	 */
	public void setMsgQueue(BlockingQueue<String> msgQueue) {
		this.msgQueue = msgQueue;
	}

	/**
	 *  连接FTP
	 * @throws IOException 
	 * */
	public FTPClient connectFtp() throws IOException{
		FTPClient FtpClient = null;
		if (FtpClient == null) {
			int reply;
			try {
				logger.info("======================"+FTPIP + FTPPORT + FTPUSER + FTPPSWD);
				FtpClient = new FTPClient();
				FtpClient.connect(FTPIP,Integer.valueOf(FTPPORT));
				FtpClient.login(FTPUSER, FTPPSWD);
				reply = FtpClient.getReplyCode();

				if (!FTPReply.isPositiveCompletion(reply)) {
					FtpClient.disconnect();
					FtpClient = null;
					System.err.println("FTP server refused connection.");
					logger.info("FTP 服务拒绝连接！");
					System.out.println(new Date()+"  ftp服务器连接失败");
				}
				logger.info("ftp服务器连接成功");
			} catch (SocketException e) {
				logger.error("登录ftp服务器【" + FTPIP + "】失败,连接超时！"+ e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				logger.error("登录ftp服务器【" + FTPIP + "】失败，FTP服务器无法打开！" + e.getMessage());
				e.printStackTrace();
			}
		}
		return FtpClient;
	}
	@Override
	public RetBean readDMSGXmlFiles() throws Exception {
		BufferedOutputStream outStream = null;
		try{
			File file = new File(xmlDMSGPath);
			if(!file.exists()){
				file.mkdir();
			}
			FTPClient ftpClient = connectFtp();
			if(ftpClient != null){
				String directory = ftpFilePath +"/";
				if(directory.startsWith("/") && directory.endsWith("/")){   
					if(ftpClient.changeWorkingDirectory(directory)){
						FTPFile[] files = ftpClient.listFiles(directory);
						logger.info("files length  ======== "+ files.length);
						for (int i = 0; i < files.length; i++) {
							if(!files[i].isDirectory()){
								String strFilePath = xmlDMSGPath + files[i].getName();
								outStream = new BufferedOutputStream(new FileOutputStream(strFilePath));
								if(ftpClient.retrieveFile(files[i].getName(), outStream)){
									logger.info(files[i].getName()+"下载成功");
									if(ftpClient.deleteFile(files[i].getName())){
										logger.info("ftp上的文件"+files[i].getName()+"删除成功");
									}
								}
								outStream.flush();
								outStream.close();
							}
						}
					}
				}
			}
			ftpClient.disconnect();
			if(!file.exists()){
				return new RetBean(RetStatusEnum.F, "fail", "ftp下载文件失败");
			}
			File[] fList = file.listFiles();
			logger.info("fList.length ======== "+fList.length);
			for(File xmlFile:fList) {
				/**
				 * 将消息抛给阻塞队列 即XML文件名
				 */
				logger.info("准备加载:"+xmlDMSGPath+"下的文件["+xmlFile.getName()+"]"+new Date());
				msgQueue = CdtcDownMsgXmlThreadServer.msgQueue; 
				boolean result = this.msgQueue.offer(xmlFile.getName());
				logger.info("result ======== "+result);
				if (!result) {
					//溢出队列的时候直接清空队列，重现往里面加载数据
					this.msgQueue.clear();
					this.msgQueue.offer(xmlFile.getName());
				}
			}
		}catch (Exception e) {
			logger.error(ExceptionUtil.getErrorInfoFromException(e));
			throw e;
		}
		return new RetBean(RetStatusEnum.S, "success", "ftp下载文件成功");
	}

}
