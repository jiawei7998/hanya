package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class Bond3 extends Group {
	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("BdId", Field.class);
		keyClassMap.put("ISIN", Field.class);
		keyClassMap.put("BdShrtNm", Field.class);
		keyClassMap.put("BdAmt", Field.class);
		keyClassMap.put("BdCollRate", Field.class);
		keyClassMap.put("AcrdIntrstInf", AccruedInterestInformation.class);
		keyClassMap.put("Pric1", Field.class);
		keyClassMap.put("Pric2", Field.class);
		keyClassMap.putAll(AccruedInterestInformation.keyClassMap);
	}
	
	public Bond3(String tag) {

		super(tag, tag, new String[] { "BdId", "ISIN", "BdShrtNm", "BdAmt",
				"BdCollRate", "AcrdIntrstInf", "Pric1", "Pric2" });
	}

	public Bond3(String tag,String delim) {

		super(tag, tag, new String[] { "BdId", "ISIN", "BdShrtNm", "BdAmt",
				"BdCollRate", "AcrdIntrstInf", "Pric1", "Pric2" });
	}

	public void set(Max30Text value) {
		setField(value);
	}

	public Max30Text get(Max30Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max30Text getMax30Text() throws FieldNotFound {
		Max30Text value = new Max30Text("BdId");
		getField(value);

		return value;
	}

	public boolean isSet(Max30Text field) {
		return isSetField(field);
	}

	public boolean isSetMax30Text() {
		return isSetField("BdId");
	}

	public void set(ISINIdentifier value) {
		setField(value);
	}

	public ISINIdentifier get(ISINIdentifier value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public ISINIdentifier getISINIdentifier() throws FieldNotFound {
		ISINIdentifier value = new ISINIdentifier("ISIN");
		getField(value);

		return value;
	}

	public boolean isSet(ISINIdentifier field) {
		return isSetField(field);
	}

	public boolean isSetISINIdentifier() {
		return isSetField("ISIN");
	}

	public void set(Max35Text value) {
		setField(value);
	}

	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max35Text getMax35Text() throws FieldNotFound {
		Max35Text value = new Max35Text("");
		getField(value);

		return value;
	}

	public boolean isSet(Max35Text field) {
		return isSetField(field);
	}

	public boolean isSetMax35Text() {
		return isSetField("BdShrtNm");
	}

	public void set(FaceCurrencyAndAmount value) {
		setField(value);
	}

	public FaceCurrencyAndAmount get(FaceCurrencyAndAmount value)
			throws FieldNotFound {
		getField(value);

		return value;
	}

	public FaceCurrencyAndAmount getFaceCurrencyAndAmount()
			throws FieldNotFound {
		FaceCurrencyAndAmount value = new FaceCurrencyAndAmount("", "");
		getField(value);

		return value;
	}

	public boolean isSet(FaceCurrencyAndAmount field) {
		return isSetField(field);
	}

	public boolean isSetFaceCurrencyAndAmount() {
		return isSetField("BdAmt");
	}

	public void set(PercentageRate value) {
		setField(value);
	}

	public PercentageRate get(PercentageRate value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public PercentageRate getPercentageRate() throws FieldNotFound {
		PercentageRate value = new PercentageRate("");
		getField(value);

		return value;
	}

	public boolean isSet(PercentageRate field) {
		return isSetField(field);
	}

	public boolean isSetPercentageRate() {
		return isSetField("BdCollRate");
	}

	public void set(AccruedInterestInformation group) {
		addGroup(group);
	}

	public boolean isSet(AccruedInterestInformation field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetAccruedInterestInformation() {
		return isSetField("AcrdIntrstInf");
	}

	public void set(ValueCurrencyAndAmount value) {
		setField(value);
	}

	public ValueCurrencyAndAmount get(ValueCurrencyAndAmount value)
			throws FieldNotFound {
		getField(value);

		return value;
	}

	public ValueCurrencyAndAmount getValueCurrencyAndAmount()
			throws FieldNotFound {
		ValueCurrencyAndAmount value = new ValueCurrencyAndAmount("", "");
		getField(value);

		return value;
	}

	public boolean isSet(ValueCurrencyAndAmount field) {
		return isSetField(field);
	}

	public boolean isSetValueCurrencyAndAmount() {
		return isSetField("Pric1");
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
