package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.AgentParty;
import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.GenericIdentification4;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.Max70Text;

public class ReportQryPty extends Group {

	/**
	 * 	QryPty  查询方  组件
	 * 
	 * 债券账户对帐单查询请求发起机构的信息
	 */
	private static final long serialVersionUID = 1L;
	

	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Nm",           Field.class);
		keyClassMap.put("PrtryId", 	    AgentParty.class);
		keyClassMap.put("AgtPty",      	GenericIdentification4.class);
		keyClassMap.putAll(AgentParty.keyClassMap);
		keyClassMap.putAll(GenericIdentification4.keyClassMap);
	}
	
	public ReportQryPty(String tag){
		/**
		 *----- QryPty  查询方--------
		 * Nm         名称
		 * PrtryId    自定义标识
		 * AgtPty     代理机构
		 */
		super(tag,tag, new String[] {"Nm","PrtryId","AgtPty"});
		
	}
	
	
	public ReportQryPty(String tag,String delim){
		/**
		 *----- QryPty  查询方--------
		 * Nm         名称
		 * PrtryId    自定义标识
		 * AgtPty     代理机构
		 */
		super(tag,tag, new String[] {"Nm","PrtryId","AgtPty"});
		
	}
	
	
	public void set(Max70Text max70Text){
		setField(max70Text);
	}

	public Max70Text get(Max70Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public void set(GenericIdentification4 genericIdentification4){
		addGroup(genericIdentification4);
	}
	
	public void set(AgentParty agentParty){
		addGroup(agentParty);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer)
	{
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
	}
}
