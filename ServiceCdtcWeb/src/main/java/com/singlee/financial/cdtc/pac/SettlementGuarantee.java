package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class SettlementGuarantee extends Group {


	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("GrteTp", Field.class);
		keyClassMap.put("GrteBd", CollateralBond1.class);
		keyClassMap.put("CshLctn", Field.class);
		keyClassMap.put("GrtePricAmt", Field.class);
		keyClassMap.putAll(CollateralBond1.keyClassMap);
	}
	
	public SettlementGuarantee(String tag){
		super(tag,tag, new String[] { "GrteTp", "GrteBd","CshLctn","GrtePricAmt"});
	}
	public SettlementGuarantee(String tag,String delim){
		super(tag,tag, new String[] { "GrteTp", "GrteBd","CshLctn","GrtePricAmt"});
	}
	 public void set(GuaranteeTypeCode value) {
        setField(value);
      }

      public GuaranteeTypeCode get(GuaranteeTypeCode value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public GuaranteeTypeCode getGuaranteeTypeCode() throws FieldNotFound
      {
    	  GuaranteeTypeCode value = new GuaranteeTypeCode("");
          getField(value);

          return value;
      }

      public boolean isSet(GuaranteeTypeCode field) {
        return isSetField(field);
      }

      public boolean isSetGuaranteeTypeCode() {
        return isSetField("GrteTp");
      }

      public void set(Exact11Text value) {
        setField(value);
      }

      public Exact11Text get(Exact11Text value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public Exact11Text getExact11Text() throws FieldNotFound
      {
    	  Exact11Text value = new Exact11Text("");
        getField(value);

        return value;
      }

      public boolean isSet(Exact11Text field) {
        return isSetField(field);
      }

      public boolean isSetExact11Text() {
        return isSetField("Id");
      }
      
      public void set(CollateralBond1 group){
    	  addGroup(group);
      }
      public void set(CashLocationCode field){
    	  setField(field);
      }
      public void set(CommonCurrencyAndAmount field){
    	  setField(field);
      }
      public void toXmlForSuperParent(StringBuffer xmlBuffer)
      {
    	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
    	  this.toXml(xmlBuffer);
    	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
      }
}
