package com.singlee.financial.cdtc.server;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.singlee.financial.bean.CdtcAcctTotalBean;
import com.singlee.financial.cdtc.CdtcSecCheckAccountServer;
import com.singlee.financial.cdtc.dao.CdtcCompareToSmtDao;

/***
 * 
 * 债券总对账单
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcSecCheckAccountServerImpl implements CdtcSecCheckAccountServer {

	private static Logger logger = LoggerFactory.getLogger(CdtcSecCheckAccountServerImpl.class);

	@Autowired
	private CdtcCompareToSmtDao cdtcCompareToSmtDao;

	/***
	 * 查询总对账单
	 */
	@Override
	public List<CdtcAcctTotalBean> getSecCheckAccount(Map<String, Object> map) {
		logger.info("已进入cdtcWeb中getSecCheckAccount方法,开始查询总对账单");
		List<CdtcAcctTotalBean> list = new ArrayList<CdtcAcctTotalBean>();
		try {
			LinkedHashMap<Integer, CdtcAcctTotalBean> cdtcHashMap = cdtcCompareToSmtDao.getSecCheckingMap(map);
			logger.info("正在处理CDTC报文并展示，请稍后......");
			logger.info("cdtcHashMap大小：" + cdtcHashMap.size());
			logger.info("开始遍历cdtcHashMap..........................");
			for (Map.Entry<Integer, CdtcAcctTotalBean> entry : cdtcHashMap.entrySet()) {
				logger.info("================key===============" + entry.getKey());
				logger.info("===============value==============" + entry.getValue());
				list.add(entry.getValue());
				System.out.println("key:" + entry.getKey() + " ,  value:" + entry.getValue());
			}
			logger.info("遍历cdtcHashMap结束..................");
			logger.info("返回的List<CdtcAcctTotalBean>大小：" + list.size());
		} catch (Exception e) {
			logger.error("CDTC查询总对账单异常，异常信息：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		return list;
	}

	/***
	 * 对比总对账单
	 */
	@Override
	public String compareSecCheckAccount(Map<String, Object> map) {
		logger.info("已进入cdtcWeb中compareSecCheckAccount方法,正在进行对比总对账单");
		
		try {
			//清空总对账单数据
			cdtcCompareToSmtDao.deleteCompareToSmt();
			//插入总对账单数据
			List<CdtcAcctTotalBean> list=getSecCheckAccount(map);
			if(list==null||list.size()==0) {
				throw new RuntimeException("查询总对账数据为空,请确认查询日期后再试!");}
			cdtcCompareToSmtDao.insertCompareToSmtByList(list);
			//核对总对账单数据
			map.put("I_SECACCT", "CDC");
			String callbackStr = cdtcCompareToSmtDao.checkingSummitData(map);
			System.out.println("结果:" + callbackStr);
			if ("999".equals(callbackStr)) {
				return callbackStr;
			} else {
				String tmpStr = (String) map.get("O_RETMSG");
				throw new RuntimeException(tmpStr != null && tmpStr.length() > 0 ? ",错误信息:" + tmpStr : "数据操作失败!"); 
			}
			
		} catch (Exception e) {
			logger.error("CDTC对比总对账单异常，异常信息：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}
}
