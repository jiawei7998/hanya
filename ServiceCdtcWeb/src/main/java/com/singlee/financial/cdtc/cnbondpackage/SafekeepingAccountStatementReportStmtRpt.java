package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.AccountIdentificationAndName5;
import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODate;
import com.singlee.financial.cdtc.pac.Max5NumericText;

public class SafekeepingAccountStatementReportStmtRpt extends Group {


	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("StartDt",          Field.class);
		keyClassMap.put("EndDt", 	        Field.class);
		keyClassMap.put("SfkpgAcctCnt",     Field.class);
		keyClassMap.put("SfkpgAcctStmtInf", AccountIdentificationAndName5.class);
		keyClassMap.putAll(AccountIdentificationAndName5.keyClassMap);
	}
	
	public SafekeepingAccountStatementReportStmtRpt(String tag){
		
		super(tag,tag, new String[] {"StartDt","EndDt","SfkpgAcctCnt","SfkpgAcctStmtInf"});
		
	}
	
	public SafekeepingAccountStatementReportStmtRpt(String tag,String delim){
		
		super(tag,tag, new String[] {"StartDt","EndDt","SfkpgAcctCnt","SfkpgAcctStmtInf"});
		
	}

	public void setStartDt(ISODate iSODate){
		setField(iSODate);
	}
	
	public void setEndDt(ISODate iSODate){
		setField(iSODate);
	}
	
	public void setSfkpgAcctCnt(Max5NumericText max5NumericText){
		setField(max5NumericText);
	}
	
	public void setSfkpgAcctStmtInf(AccountIdentificationAndName5 accountIdentificationAndName5){
		addGroup(accountIdentificationAndName5);
	}
	
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer)
	{
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
	}
}
