package com.singlee.financial.cdtc.cnbondpackage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max2048Text;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.Max8Text;
import com.singlee.financial.cdtc.pac.Priority4Code;

/****
 * 6.3.12 心跳消息（CSBS.044.001.01 HeartBeatMessageV01）
 * 
 * @author Fang
 * 
 */
public class HeartBeatMessageV01 extends Group {

	private static final long serialVersionUID = 1L;

	private static String type = "CSBS.012.001.01";

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("Msg", ReportMsgHeader.class);
		keyClassMap.put("HrtBtMsg", HrtBtMsg.class);
		keyClassMap.putAll(ReportMsgHeader.keyClassMap);
		keyClassMap.putAll(HrtBtMsg.keyClassMap);
	}

	public HeartBeatMessageV01(String tag) {

		super(tag, tag, new String[] { "Msg", "HrtBtMsg" });
	}

	public HeartBeatMessageV01(String tag, String belim) {

		super(tag, tag, new String[] { "Msg", "HrtBtMsg" });
	}

	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}

	public void set(Max35Text value) {
		setField(value);
	}

	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max35Text getMax35Text() throws FieldNotFound {
		Max35Text value = new Max35Text("TxFlowId");
		getField(value);

		return value;
	}

	public boolean isSet(Max35Text field) {
		return isSetField(field);
	}

	public boolean isSetMax35Text() {
		return isSetField("TxFlowId");
	}

	/** ****************************************************************** */
	public void set(Max2048Text value) {
		setField(value);
	}

	public Max2048Text get(Max2048Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max2048Text getMax2048Text() throws FieldNotFound {
		Max2048Text value = new Max2048Text("Xtnsn");
		getField(value);

		return value;
	}

	public boolean isSet(Max2048Text field) {
		return isSetField(field);
	}

	public boolean isSetMax2048Text() {
		return isSetField("Xtnsn");
	}

	/** ****************************************************************** */
	public void set(Priority4Code value) {
		setField(value);
	}

	public Priority4Code get(Priority4Code value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Priority4Code getPriority4Code() throws FieldNotFound {
		Priority4Code value = new Priority4Code("Prty");
		getField(value);

		return value;
	}

	public boolean isSet(Priority4Code field) {
		return isSetField(field);
	}

	public boolean isSetPriority4Code() {
		return isSetField("Prty");
	}

	/** ************************************************************************ */
	public void set(ISODateTime value) {
		setField(value);
	}

	public ISODateTime get(ISODateTime value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public ISODateTime getISODateTime() throws FieldNotFound {
		ISODateTime value = new ISODateTime("CreDtTm", new Date());
		getField(value);

		return value;
	}

	public boolean isSet(ISODateTime field) {
		return isSetField(field);
	}

	public boolean isSetISODateTime() {

		return isSetField("CreDtTm");
	}

	/** ********************************************************************* */

	public void set(HrtBtMsg group) {
		addGroup(group);
	}

	public boolean isSet(HrtBtMsg field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetHrtBtMsg() {
		return isSetField("HrtBtMsg");
	}

	public void set(ReportMsgHeader group) {
		addGroup(group);
	}

	/** ********************************************************************* */
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

	public static HeartBeatMessageV01 content(Bean1 bean1) {
		HeartBeatMessageV01 heartBeatMessageV01 = new HeartBeatMessageV01(
				"Document");
		ReportMsgHeader msgHeader = new ReportMsgHeader("Msg");
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("HeartBeatMessageV01");
		msgHeader.set(max35Text);
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT01.getType());
		msgHeader.set(priority4Code);
		ISODateTime isoDateTime = new ISODateTime("CreDtTm", new Date());
		msgHeader.set(isoDateTime);
		HrtBtMsg hrtBtMsg = new HrtBtMsg("HrtBtMsg");
		Max8Text max8Text = new Max8Text("HrtBtId");
		max8Text.setValue("00000003");
		hrtBtMsg.set(max8Text);
		Max8Text max8Text2 = new Max8Text("HrtBtRefId");
		max8Text2.setValue("00000004");
		hrtBtMsg.set(max8Text2);
		heartBeatMessageV01.set(msgHeader);
		heartBeatMessageV01.set(hrtBtMsg);

		HrtBtMsg hrtBtMsg2 = new HrtBtMsg("HrtBtMsg");
		Max8Text max8Text21 = new Max8Text("HrtBtId");
		max8Text21.setValue("00000001");
		hrtBtMsg2.set(max8Text21);
		Max8Text max8Text22 = new Max8Text("HrtBtRefId");
		max8Text22.setValue("00000002");
		hrtBtMsg2.set(max8Text22);
		heartBeatMessageV01.addGroup(hrtBtMsg2);
		return heartBeatMessageV01;

	}

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {

		StringBuffer xmlBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
		// 保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat
				.format(xmlBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out = new FileOutputStream("./cnbondxml/" + "CSBS.044.001.01"
				+ ".xml");
		XMLOutputter outputter = new XMLOutputter();
		// 如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的
		outputter.output((Document) doc, out);
		out.close();

		SAXReader reader = new SAXReader();
		/**
		 * 读取请求报文XML 反解析报文
		 */
		HeartBeatMessageV01 heartBeatMessageV02 = new HeartBeatMessageV01(
				"Document");
		org.dom4j.Document xmlDoc = reader.read(new File("./cnbondxml/"
				+ heartBeatMessageV02.type + ".xml"));
		getElementList(xmlDoc.getRootElement(), heartBeatMessageV02);
		ISODateTime isDateTime = new ISODateTime("CreDtTm", new Date());
		System.out.println(heartBeatMessageV02.getField("Xtnsn", isDateTime)
				.getObject());
		System.out.println(heartBeatMessageV02.hasGroup("HrtBtMsg"));
		System.out.println(heartBeatMessageV02.getGroups("HrtBtMsg").size());
		int count = 1;
		for (Group group : heartBeatMessageV02.getGroups("HrtBtMsg")) {
			TreeMap<String, Field<?>> fields = group.getfieldsForFieldMap();
			System.out.println("(" + count + ")HrtBtId:"
					+ fields.get("HrtBtId"));
			System.out.println("(" + count + ")HrtBtRefId:"
					+ fields.get("HrtBtRefId"));
			count++;
		}
		System.out
				.println("*********************************************************");
		StringBuffer xmlBuffer2 = new StringBuffer();
		heartBeatMessageV02.toXmlForSuperParent(xmlBuffer2);
		System.out
				.println("*********************************************************");
		System.out.println(XmlFormat.format(xmlBuffer2.toString()));
	}

	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static Group getGroups(Element element,
			HeartBeatMessageV01 heartBeatMessageV02)
			throws NoSuchMethodException, Exception {
		// Group group = new Group(StringUtils.trimToEmpty(element.getName()),
		// "");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = HeartBeatMessageV01.keyClassMap.get(
				StringUtils.trimToEmpty(element.getName())).getConstructor(
				parameterTypes2);
		Object[] parameters2 = { StringUtils.trimToEmpty(element.getName()), "" };
		Group group = (Group) constructor2.newInstance(parameters2);

		System.out.println("************************************");
		List<Element> elements = element.elements();
		for (Iterator<Element> iterator = elements.iterator(); iterator
				.hasNext();) {
			Element element2 = (Element) iterator.next();
			// getElementList(element2,heartBeatMessageV02);
			System.out.println(""
					+ String.format("%20s :", StringUtils.trimToEmpty(element2
							.getName())) + ""
					+ StringUtils.trimToEmpty(element2.getText()));
			if (element2.elements().size() > 0) {
				System.out.println(String.format("%20s :", StringUtils
						.trimToEmpty(element2.getName()))
						+ "IS GROUP");
				group.addGroup(getGroups(element2, heartBeatMessageV02));
			} else {
				System.out.println(String.format("%20s :", StringUtils
						.trimToEmpty(element2.getName()))
						+ "IS FIELD");
				Class<?>[] parameterTypes = { String.class, Object.class };
				Constructor<?> constructor = HeartBeatMessageV01.keyClassMap.get(
						StringUtils.trimToEmpty(element2.getName()))
						.getConstructor(parameterTypes);
				Object[] parameters = {
						StringUtils.trimToEmpty(element2.getName()),
						StringUtils.trimToEmpty(element2.getText()) };
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),
						(Field<?>) object);
			}
		}

		System.out.println("************************************");
		return group;
	}

	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	public static void getElementList(Element element,
			HeartBeatMessageV01 heartBeatMessageV02) throws Exception,
			NoSuchMethodException {
		List<?> elements = element.elements();
		for (Iterator<?> iterator = elements.iterator(); iterator.hasNext();) 
		{
			Element element2 = (Element) iterator.next();
			List<?> elements2 = element2.elements();
			if (elements2.size() >= 0) {
				System.out.println(""
						+ String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName())) + ""
						+ StringUtils.trimToEmpty(element2.getText()));
				if (heartBeatMessageV02.isHasTag(StringUtils
						.trimToEmpty(element2.getName()))) {
					if (element2.elements().size() > 0) {
						System.out.println(String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName()))
								+ "IS GROUP");

						heartBeatMessageV02.addGroup(getGroups(element2,
								heartBeatMessageV02));
					} else {
						System.out.println(String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName()))
								+ "IS FIELD");
						Class<?>[] parameterTypes = { String.class, Object.class };
						Constructor<?> constructor = HeartBeatMessageV01.keyClassMap
								.get(
										StringUtils.trimToEmpty(element2
												.getName())).getConstructor(
										parameterTypes);
						Object[] parameters = {
								StringUtils.trimToEmpty(element2.getName()),
								StringUtils.trimToEmpty(element2.getText()) };
						Object object = constructor.newInstance(parameters);
						heartBeatMessageV02.setField(StringUtils
								.trimToEmpty(element2.getName()),
								(Field<?>) object);
					}
				}
			}
			// getElementList(element2,heartBeatMessageV02);
		}
	}
}
