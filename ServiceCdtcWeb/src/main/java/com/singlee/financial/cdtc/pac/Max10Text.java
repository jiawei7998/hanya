package com.singlee.financial.cdtc.pac;

public class Max10Text extends StringField {

	public Max10Text(String field) {
		super(field);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public String toXmlString(){
		if(super.getLength() >= 1 && super.getLength() <= 10){
			return toXml();
		}
		return "";
	}

}
