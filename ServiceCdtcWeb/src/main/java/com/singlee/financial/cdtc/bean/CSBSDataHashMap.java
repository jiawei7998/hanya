package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.IdentityHashMap;

public class CSBSDataHashMap implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	HashMap<String, IdentityHashMap<String, CSBSDataBean>> csbsHashMap = new HashMap<String, IdentityHashMap<String, CSBSDataBean>>();
	
	/**
	 * @return the csbsHashMap
	 */
	public HashMap<String, IdentityHashMap<String, CSBSDataBean>> getCsbsHashMap() {
		return csbsHashMap;
	}

	/**
	 * @param csbsHashMap the csbsHashMap to set
	 */
	public void setCsbsHashMap(HashMap<String, IdentityHashMap<String, CSBSDataBean>> csbsHashMap) {
		this.csbsHashMap = csbsHashMap;
	}

	
	public CSBSDataHashMap(HashMap<String, IdentityHashMap<String, CSBSDataBean>> csbsHashMap){
		super();
		this.csbsHashMap = csbsHashMap;
	}
	
}
