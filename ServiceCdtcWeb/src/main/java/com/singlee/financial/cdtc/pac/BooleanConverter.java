package com.singlee.financial.cdtc.pac;

public class BooleanConverter
{
  public static String convert(boolean b)
  {
    return b ? "Ture" : "False";
  }

  public static boolean convert(String value)
    throws FieldConvertError
  {
    if ("Ture".equals(value)) {
      return true;}
    if ("False".equals(value)) {
      return false;
    }
    throw new FieldConvertError("invalid boolean value: " + value);
  }
}