/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class TransactionStsFlg extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("InstrStsInf", InstructionStatusInfo.class);
		keyClassMap.put("CtrctStsInf", Field.class);
		keyClassMap.putAll(InstructionStatusInfo.keyClassMap);
	}
	
	public TransactionStsFlg(String tag) {
		super(tag, tag, new String[] { "InstrStsInf","CtrctStsInf"});
	}
	public TransactionStsFlg(String tag,String delim) {
		super(tag, tag, new String[] { "InstrStsInf","CtrctStsInf"});
	}
	
	public void set(InstructionStatusInfo group){
		addGroup(group);
	}
	public void set(ContractStatusCode field){
		setField(field);
	}
}
