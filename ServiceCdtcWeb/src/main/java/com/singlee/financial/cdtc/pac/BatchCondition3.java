package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class BatchCondition3 extends Group{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("StrtDt",Field.class);
		keyClassMap.put("EndDt",Field.class);
		keyClassMap.put("YrsToMtrty",Field.class);
	}
	
	public BatchCondition3(String tag){
		super(tag,tag,new String[]{"StrtDt","EndDt","YrsToMtrty"});
	}

	public BatchCondition3(String tag,String delim){
		super(tag,tag,new String[]{"StrtDt","EndDt","YrsToMtrty"});
	}
	public void set(ISODate value){
		setField(value);
	}
	
	public ISODate get(ISODate value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(ISODate field) {
		return isSetField(field);
	}

	public boolean isSetISODate() {
		return isSetField("StrtDt");
	}
	
	
	public void set(CommonCurrencyAndAmount value){
		setField(value);
	}
	
	public CommonCurrencyAndAmount get(CommonCurrencyAndAmount value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(CommonCurrencyAndAmount field) {
		return isSetField(field);
	}

	public boolean isSetCommonCurrencyAndAmount() {
		return isSetField("YrsToMtrty");
	}

}
