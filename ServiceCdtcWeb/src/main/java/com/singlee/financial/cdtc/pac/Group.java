package com.singlee.financial.cdtc.pac;

public class Group extends FieldMap
{
	  private static final long serialVersionUID = 1L;
	  private StringField field;
	  private String delimTag;

	  public Group(String field, String delim)
	  {
	    this(field, delim, new String[] { delim });
	  }

	  public Group(){}
	  
	  public Group(Group group,String newTag)
	  {
	    this(newTag, group.delimTag, group.getFieldOrder());
	    setFields(group);
	    setGroups(group);
	  }
	  
	  public Group(Group group)
	  {
	    this(group.getFieldTag(), group.delimTag, group.getFieldOrder());
	    setFields(group);
	    setGroups(group);
	  }

	  public Group(String field, String delim, String[] order)
	  {
	    super(order);
	    this.field = new StringField(field);
	    this.delimTag = delim;
	  }

	  public String delimTag()
	  {
	    return this.delimTag;
	  }
	  @Override
	  public void addGroup(Group group)
	  {
	    super.addGroup(group);
	  }
	  @Override
	  public Group getGroup(int num, Group group)
	    throws FieldNotFound
	  {
	    return super.getGroup(num, group);
	  }

	  public String getFieldTag()
	  {
	    return this.field.getField();
	  }

	  /** @deprecated */
	  public String field()
	  {
	    return getFieldTag();
	  }
	  public void toXmlForParent(StringBuffer xmlBuffer)
	  {
		  toXml(xmlBuffer);
	  }
	}
