package com.singlee.financial.cdtc.pac;


public class Exact11Text extends StringField {

	private static final long serialVersionUID = 1L;
	
	public Exact11Text(String field) {
		
		super(field);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.singlee.client.cdtc.pac.IntField#getValue()
	 */
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(super.getLength()==11)
		{
			return toXml();
		}
		return "";
	}
}
