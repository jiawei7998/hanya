/**
 * 
 */
package com.singlee.financial.cdtc.cnbondpackage;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import com.singlee.financial.cdtc.cdtcacct.CdtcAcctDetailBean;

/**
 * @author Fang
 *
 */
public class DtldstmtBySafeKeepAccountReport {
	@SuppressWarnings("unchecked")
	public static LinkedHashMap<Integer, CdtcAcctDetailBean> analyzCSBSSafekeepAccountMsg(Element element){
		List<Element> elements = element.elements();
		LinkedHashMap<Integer, CdtcAcctDetailBean> cdtcHashMap = new LinkedHashMap<Integer, CdtcAcctDetailBean>();
		CdtcAcctDetailBean cdtcAcctDetailBean = new CdtcAcctDetailBean();
		//迭代取出XML文件
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			List<Element> elements2 = element2.elements();
			if(elements2.size()>=0)
			{
				if(element2.elements().size()>0)
				{
					group(element2,cdtcHashMap,cdtcAcctDetailBean);
				}
			}
		}
		return cdtcHashMap;
	}
	
	@SuppressWarnings("unchecked")
	public static void group(Element element,LinkedHashMap<Integer, CdtcAcctDetailBean> cdtcHashMap,CdtcAcctDetailBean cdtcAcctDetailBean){
		List<Element> elements = element.elements();
		int count = 0;
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			element2.getName();
			if(element2.elements().size()>0)
			{
				if(StringUtils.trimToEmpty(element2.getName()).endsWith("DtldStmt")){
					cdtcAcctDetailBean = new CdtcAcctDetailBean();
					String string = "";
					cdtcHashMap.put(count, groupDtldStmt(element2,cdtcAcctDetailBean,string));
					count++;
				}else{
					group(element2,cdtcHashMap,cdtcAcctDetailBean);
				}
			}
		}
	}
	@SuppressWarnings("unchecked")
	public static CdtcAcctDetailBean groupDtldStmt(Element element, CdtcAcctDetailBean bean, String string){
		String str = "";
		@SuppressWarnings("unused")
		String keyong ="";
		try {
			List<Element> elements = element.elements();
			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				if("TxDt".equals(StringUtils.trimToEmpty(element2.getName()))){
					bean.setSecurityDate(StringUtils.trimToEmpty(element2.getText()));
				}if("TxTm".equals(StringUtils.trimToEmpty(element2.getName()))){
					bean.setSecurityTime(StringUtils.trimToEmpty(element2.getText()));
				}if("CtrctId".equals(StringUtils.trimToEmpty(element2.getName()))){
					bean.setHtNo(StringUtils.trimToEmpty(element2.getText()));
				}if("TxLog".equals(StringUtils.trimToEmpty(element2.getName()))){
					bean.setRemark(StringUtils.trimToEmpty(element2.getText()));
				}if("TxAmt".equals(StringUtils.trimToEmpty(element2.getName()))){
					List<Element> elements1 = element2.elements();
					for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();)
					{
						Element element3 = (Element)iterator1.next();
						if("DrctnFlg".equals(StringUtils.trimToEmpty(element3.getName()))){
							str = StringUtils.trimToEmpty(element3.getText());
						}else if("Val".equals(StringUtils.trimToEmpty(element3.getName()))){
							if(str.endsWith("-")){
								bean.setDealAmt(new BigDecimal(StringUtils.trimToEmpty(element3.getText())).negate());
							}else{
								keyong = StringUtils.trimToEmpty(element3.getText());
								bean.setDealAmt(new BigDecimal(StringUtils.trimToEmpty(element3.getText())));
							}
							
						}
					}
				}
				if("AcctLdg".equals(StringUtils.trimToEmpty(element2.getName()))){
					List<Element> elements1 = element2.elements();
					for(Iterator<Element> iterator1 = elements1.iterator();iterator1.hasNext();)
					{
						Element element3 = (Element)iterator1.next();
						if("LdgTp".equals(StringUtils.trimToEmpty(element3.getName()))){
							str = StringUtils.trimToEmpty(element3.getText());
						}else if("Bal".equals(StringUtils.trimToEmpty(element3.getName()))){
							if("AL03".equals(str)){
								bean.setWaitPayAmt(new BigDecimal(StringUtils.trimToEmpty(element3.getText())));
							}else if("AL02".equals(str)){
								bean.setAbleUseAmt(new BigDecimal(StringUtils.trimToEmpty(element3.getText())));
							}else if("AL06".equals(str)){
								bean.setRepoZYAmt(new BigDecimal(StringUtils.trimToEmpty(element3.getText())));
							}else if("AL05".equals(str)){
								bean.setFrozenAmt(new BigDecimal(StringUtils.trimToEmpty(element3.getText())));
							}else if("AL04".equals(str)){
								bean.setRepoRevAmt(new BigDecimal(StringUtils.trimToEmpty(element3.getText())));
							}
						}
					}
				}
			}
			if(bean.getAbleUseAmt()==null){
				bean.setAbleUseAmt(new BigDecimal("0.00"));
			}
			if(bean.getWaitPayAmt()==null){
				bean.setWaitPayAmt(new BigDecimal("0.00"));
			}
			if(bean.getRepoZYAmt()==null){
				bean.setRepoZYAmt(new BigDecimal("0.00"));
			}
			if(bean.getRepoRevAmt()==null){
				bean.setRepoRevAmt(new BigDecimal("0.00"));
			}
			if(bean.getFrozenAmt()==null){
				bean.setFrozenAmt(new BigDecimal("0.00"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
}
