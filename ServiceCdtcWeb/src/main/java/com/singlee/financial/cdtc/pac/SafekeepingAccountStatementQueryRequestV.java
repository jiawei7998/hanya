/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;

/**
 * @author Fang
 * 6.3.7债券账户对账单查询请求报文结构表
 */
public class SafekeepingAccountStatementQueryRequestV extends Group{

	private static final long serialVersionUID = 1L;

	private static String type = "CSBS.007.001.01";
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static {
		keyClassMap.put("Msg", TransactionMsg.class);
		keyClassMap.put("OprtrFlg", TransactionOprtrFlg.class);
		keyClassMap.put("QryItm", TransactionQryItm.class);
		keyClassMap.put("QryPty", TransactionGivInf.class);
		keyClassMap.put("Xtnsn", Extension.class);
		keyClassMap.putAll(TransactionMsg.keyClassMap);
		keyClassMap.putAll(TransactionOprtrFlg.keyClassMap);
		keyClassMap.putAll(TransactionQryItm.keyClassMap);
		keyClassMap.putAll(TransactionGivInf.keyClassMap);
		keyClassMap.putAll(Extension.keyClassMap);
	}
	
	public SafekeepingAccountStatementQueryRequestV(String tag){
		super(tag,tag,new String[]{"Msg","OprtrFlg","QryItm","QryPty","Xtnsn"});
	}
	
	public SafekeepingAccountStatementQueryRequestV(String tag,String belim){
		super(tag,tag,new String[]{"Msg","OprtrFlg","QryItm","QryPty","Xtnsn"});
	}
	/*************************************************/
	public void set(TransactionOprtrFlg group){
		addGroup(group);
	}
	public boolean isSet(TransactionOprtrFlg field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetTransactionOprtrFlg() {
		return isSetField("OprtrFlg");
	}
	/*********************************************/
	public void set(TransactionQryItm group){
		addGroup(group);
	}
	public boolean isSet(TransactionQryItm field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetTransactionQryItm() {
		return isSetField("QryItm");
	}
	/********************************************/
	public void set(TransactionGivInf group){
		addGroup(group);
	}
	public boolean isSet(TransactionGivInf field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetTransactionGivInf() {
		return isSetField("QryPty");
	}
	/********************************************/
	public void set(Extension group){
		addGroup(group);
	}
	public boolean isSet(Extension field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetExtension() {
		return isSetField("Xtnsn");
	}
	/********************************************/
	public void set(TransactionMsg group) {
		addGroup(group);
	}
	
	public boolean isSet(TransactionMsg field) {
		return isSetField(field.getFieldTag());
	}
	
	public boolean isSetTransactionMsg() {
		return isSetField("Msg");
	}
	/********************************/
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	public static SafekeepingAccountStatementQueryRequestV content(Bean1 bean){
		SafekeepingAccountStatementQueryRequestV safekepp = new SafekeepingAccountStatementQueryRequestV("Document");
		/**********************************报文标识*********************************/
		TransactionMsg transactionMsg = new TransactionMsg("Msg");
		//交易流水标识
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("20120424000000017520");
		transactionMsg.set(max35Text);
		//经办人
//		PartyIdentification partyIdentification = new PartyIdentification("Oprtr");
//		Max35Text max35TextOprtr = new Max35Text("Nm");
//		max35TextOprtr.setValue("A");
//		partyIdentification.set(max35TextOprtr);
//		transactionMsg.set(partyIdentification);
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT02.getType());
		//报文创建时间
		transactionMsg.set(new ISODateTime("CreDtTm", new Date()));
		
		/*************************操作标识*******************/
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		//请求类别码
		RequestTypeCode reqTp = new RequestTypeCode("ReqTp");
		reqTp.setValue(RequestTypeCode.Type.BJ0400.getType());
		transactionOprtrFlg.set(reqTp);
		//操作码
		OperatorStatusCode operSc = new OperatorStatusCode("OprtrSts");
		operSc.setValue(OperatorStatusCode.Type.OS08.getType());
		transactionOprtrFlg.set(operSc);
		
		/*************************查询条目*************************/
		TransactionQryItm transactionQryItm = new TransactionQryItm("QryItm");
		//查询类型
		QueryType4Code qryTp = new QueryType4Code("QryTp");
		qryTp.setValue(QueryType4Code.Type.QT01.getType());
		transactionQryItm.set(qryTp);
		//查询日期
		QueryDate qryDt = new QueryDate("QryDt");
		qryDt.set(new ISODate("StartDt", new Date()));
		qryDt.set(new ISODate("EndDt",new Date()));
		transactionQryItm.set(qryDt);
		//查询账户与债券
		AccountIdentificationAndName7 sfkpgAcctBdQryn = new AccountIdentificationAndName7("SfkpgAcctBdQry");
//		Max35Text nm = new Max35Text("Nm");
//		nm.setValue("AAA");
//		sfkpgAcctBdQryn.set(nm);
		Exact11Text id = new Exact11Text("Id");
		id.setValue("00000001752");
		sfkpgAcctBdQryn.set(id);
		AccountStatementType accStTp = new AccountStatementType("AccStTp");
		accStTp.setValue(AccountStatementType.Type.AS01.getType());
		sfkpgAcctBdQryn.set(accStTp);
//		Bond1 qryBd = new Bond1("QryBd");
//		Max30Text bdId = new Max30Text("BdId");
//		bdId.setValue("黄金用户");
//		qryBd.set(bdId);
//		ISINIdentifier ISIN = new ISINIdentifier("ISIN");
//		ISIN.setValue("111123456789");
//		qryBd.set(ISIN);
//		Max35Text bdShrtNm = new Max35Text("BdShrtNm");
//		bdShrtNm.setValue("fang");
//		qryBd.set(bdShrtNm);
//		sfkpgAcctBdQryn.set(qryBd);
		transactionQryItm.set(sfkpgAcctBdQryn);
//		
//		/*******************查询方*****************/
//		TransactionGivInf qryPty = new TransactionGivInf("QryPty");
//		//名称
//		Max70Text max70TextGviInf = new Max70Text("Nm");
//		max70TextGviInf.setValue("ABC");
//		qryPty.set(max70TextGviInf);
//		//自定义标识
//		GenericIdentification4 genid = new GenericIdentification4("PrtryId");
//		Max35Text maxid = new Max35Text("Id");
//		maxid.setValue("SS123");
//		genid.set(maxid);
//		Max35Text maxIdIp = new Max35Text("IdTp");
//		maxIdIp.setValue("DD123");
//		genid.set(maxIdIp);
//		qryPty.set(genid);
//		//代理机构
//		AgentParty agentParty = new AgentParty("AgtPty");
//		AgentPartyTypeCode agtPtyTP = new AgentPartyTypeCode("AgtPtyTP");
//		agtPtyTP.setValue(AgentPartyTypeCode.Type.AP00.getType());
//		agentParty.set(agtPtyTP);
//		PartyIdentification30 agtPtyId = new PartyIdentification30("AgtPtyId");
//		Max70Text agtPtyIdNm = new Max70Text("Nm");
//		agtPtyIdNm.setValue("F");
//		agtPtyId.set(agtPtyIdNm);
//		GenericIdentification4 prtryId = new GenericIdentification4("PrtryId");
//		Max35Text prtryIdId = new Max35Text("Id");
//		prtryIdId.setValue("SFH000");
//		Max35Text paityIdTp = new Max35Text("IdTp");
//		paityIdTp.setValue("IdTp000");
//		prtryId.set(prtryIdId);
//		prtryId.set(paityIdTp);
//		agtPtyId.set(prtryId);
//		agentParty.set(agtPtyId);
//		qryPty.set(agentParty);
		
		//拓展项
//		Extension extension = new Extension("Xtnsn");
//		Max2048Text max2048Text = new Max2048Text("XtnsnTxt");
//		max2048Text.setValue("债券账户对账单查询请求报文完毕");
//		extension.set(max2048Text);

		safekepp.set(transactionMsg);
		safekepp.set(transactionOprtrFlg);
		safekepp.set(transactionQryItm);
	//	safekepp.set(qryPty);
	//	safekepp.set(extension);
		return safekepp;
	}

	public static void main(String[] args) throws Exception {
			
		StringBuffer stringBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(stringBuffer);

		System.out.println(XmlFormat.format(XmlFormat.format(stringBuffer.toString())));

		
		
		
		
		//保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat
				.format(stringBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out = new FileOutputStream("./cnbondxml/" + SafekeepingAccountStatementQueryRequestV.type
				+ ".xml");
		XMLOutputter outputter = new XMLOutputter();
		// 如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的
		outputter.output((Document) doc, out);
		out.close();
		
		System.out.println("反解析报文开始");
		SAXReader reader = new SAXReader();
		/**
		 * 反解析报文
		 */
		SafekeepingAccountStatementQueryRequestV safekeeping = new SafekeepingAccountStatementQueryRequestV(
				"Document");
		org.dom4j.Document docXml = reader.read(new File("./cnbondxml/"
				+ SafekeepingAccountStatementQueryRequestV.type + ".xml"));
		getElementList(docXml.getRootElement(), safekeeping);
		
		StringBuffer xmlBuffer = new StringBuffer();
		safekeeping.toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));

	}
	
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * @param element
	 * @param common
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	public static void getElementList(Element element,SafekeepingAccountStatementQueryRequestV common)throws Exception,
	NoSuchMethodException{
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			List<?> elements2 = element2.elements();
			if(elements2.size() >= 0){
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
				if(common.isHasTag(StringUtils.trimToEmpty(element2.getName()))){
					if(element2.elements().size() > 0){
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
						common.addGroup(getGroups(element2, common));
					}else{
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
						Class<?>[] parameterTypes = {String.class,Object.class};
						Constructor<?> constructor = SafekeepingAccountStatementQueryRequestV.keyClassMap
						.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
						Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element.getText())};
						Object object = constructor.newInstance(parameters);
						common.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
					}
				}
			}
		}
	}
	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception反解析报文
	 */
	public static Group getGroups(Element element, SafekeepingAccountStatementQueryRequestV common) throws NoSuchMethodException, Exception {
		//Group group = new Group(StringUtils.trimToEmpty(element.getName()),"");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = SafekeepingAccountStatementQueryRequestV.keyClassMap.get(StringUtils.trimToEmpty(element.getName())).getConstructor(parameterTypes2);
		Object[] parameters2 = {StringUtils.trimToEmpty(element.getName()),""};
		Group group = (Group) constructor2.newInstance(parameters2);
		
		System.out.println("************************************");
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
			if(element2.elements().size() > 0){
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
				group.addGroup(getGroups(element2, common));
			}else{
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
				Class<?>[] parameterTypes = {String.class,Object.class};
				Constructor<?> constructor = SafekeepingAccountStatementQueryRequestV.keyClassMap
				.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
				Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element2.getText())};
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
			}
		}
		System.out.println("************************************");
		return group;
	}
}
