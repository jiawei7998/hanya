package com.singlee.financial.cdtc.httpclient;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface HttpClientManagerDao {

	 /**  
     * 以get方式发送http请求  
     *   
     * @param url  
     * @return  
     */  
    public String sendRequest(String url) throws Exception;
    /**  
     * 以get方式发送http请求  
     *   
     * @param url  
     * @return  
     */  
    public boolean isActive(String url) throws Exception;
    
    /**  
     * 以post方式发送http请求  
     *   
     * @param url  
     * @return  
     */  
    public  int sendRequestAsPost(String url) throws Exception;
    
    /**  
     * 验证请求是否是本机发出  
     *   
     * @param request  
     *            true本机发出 false非本机发出  
     * @return  
     */  
    public boolean isRequestFromSelf(HttpServletRequest request)  throws Exception;
    
    /**  
     * 获取远程客户端IP地址  
     *   
     * @param request  
     * @return  
     */  
    public String getRemoteIpAddr(HttpServletRequest request) throws Exception;
    
    
    /**  
     * 获取本机IP地址  
     *   
     * @return  
     */  
    public String getLocalIpAddr() throws Exception;
    
    /**  
     * 判断某回调地址是否是指定的网关地址  
     *   
     * @param notifyUrl  
     * @param getwayList  
     * @return true 是网关 false不是网关地址  
     */  
	public boolean isLocalNotifyUrl(String notifyUrl, List<?> getwayList) throws Exception;
    
    
    public HashMap<String, Object> sendXmlToRequest(String url,String xmlString) throws Exception;
}
