package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class GenericIdentification4 extends Group {


	private static final long serialVersionUID = 1L;
	
	public GenericIdentification4(String tag){
		super(tag,tag, new String[] { "Id", "IdTp"});
	}
	public GenericIdentification4(String tag,String delim){
		super(tag,tag, new String[] { "Id", "IdTp"});
	}
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Id", Field.class);
		keyClassMap.put("IdTp", Field.class);
	}
	 public void set(Max35Text value) {
        setField(value);
      }

      public Max35Text get(Max35Text value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public Max35Text getMax35Text() throws FieldNotFound
      {
    	  Max35Text value = new Max35Text("");
          getField(value);

          return value;
      }

      public boolean isSet(Max35Text field) {
        return isSetField(field);
      }

      public boolean isSetMax35Text() {
        return isSetField("Nm");
      }
      @Override
      public void toXmlForParent(StringBuffer xmlBuffer)
      {
    	  this.toXml(xmlBuffer);
      }
}