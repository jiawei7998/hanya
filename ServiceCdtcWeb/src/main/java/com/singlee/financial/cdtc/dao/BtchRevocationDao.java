package com.singlee.financial.cdtc.dao;

import com.singlee.financial.cdtc.bean.BtchQryRslt;
import com.singlee.financial.cdtc.bean.BtchQryRsltBean;

public interface BtchRevocationDao {
	/**
	 * 是否存在
	 * @param btchQryRslt
	 * @return
	 * @throws Exception
	 */
	public boolean isExistBtchRevocationContract(BtchQryRslt btchQryRslt) throws Exception;
	
	public boolean isExistBtchRevocationInstruction(BtchQryRsltBean btchQryRsltBean) throws Exception;
}
