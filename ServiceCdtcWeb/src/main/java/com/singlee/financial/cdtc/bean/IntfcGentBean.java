package com.singlee.financial.cdtc.bean;

import java.io.Serializable;

public class IntfcGentBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String PARAMOD;
	
	private String BR;
	
	private String PARAID;
	
	private String PART1;
	
	private String PART2;
	
	private String OBJECT;
	
	private String OPERID;
	
	private String NOTE;
	
	private String  LSTDATE;

	/**
	 * @return the pARAMOD
	 */
	public String getPARAMOD() {
		return PARAMOD;
	}

	/**
	 * @param paramod the pARAMOD to set
	 */
	public void setPARAMOD(String paramod) {
		PARAMOD = paramod;
	}

	/**
	 * @return the bR
	 */
	public String getBR() {
		return BR;
	}

	/**
	 * @param br the bR to set
	 */
	public void setBR(String br) {
		BR = br;
	}

	/**
	 * @return the pARAID
	 */
	public String getPARAID() {
		return PARAID;
	}

	/**
	 * @param paraid the pARAID to set
	 */
	public void setPARAID(String paraid) {
		PARAID = paraid;
	}

	/**
	 * @return the pART1
	 */
	public String getPART1() {
		return PART1;
	}

	/**
	 * @param part1 the pART1 to set
	 */
	public void setPART1(String part1) {
		PART1 = part1;
	}

	/**
	 * @return the pART2
	 */
	public String getPART2() {
		return PART2;
	}

	/**
	 * @param part2 the pART2 to set
	 */
	public void setPART2(String part2) {
		PART2 = part2;
	}

	/**
	 * @return the oBJECT
	 */
	public String getOBJECT() {
		return OBJECT;
	}

	/**
	 * @param object the oBJECT to set
	 */
	public void setOBJECT(String object) {
		OBJECT = object;
	}

	/**
	 * @return the oPERID
	 */
	public String getOPERID() {
		return OPERID;
	}

	/**
	 * @param operid the oPERID to set
	 */
	public void setOPERID(String operid) {
		OPERID = operid;
	}

	/**
	 * @return the nOTE
	 */
	public String getNOTE() {
		return NOTE;
	}

	/**
	 * @param note the nOTE to set
	 */
	public void setNOTE(String note) {
		NOTE = note;
	}

	/**
	 * @return the lSTDATE
	 */
	public String getLSTDATE() {
		return LSTDATE;
	}

	/**
	 * @param lstdate the lSTDATE to set
	 */
	public void setLSTDATE(String lstdate) {
		LSTDATE = lstdate;
	}

}
