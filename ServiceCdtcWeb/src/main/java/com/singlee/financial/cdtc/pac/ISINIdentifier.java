package com.singlee.financial.cdtc.pac;


public class ISINIdentifier extends StringField {

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Z0-9]{12}$";
	
	public ISINIdentifier(String field) {
		
		super(field);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.singlee.client.cdtc.pac.IntField#getValue()
	 */
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
