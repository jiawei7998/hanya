package com.singlee.financial.cdtc.dao;

import java.util.Map;

public interface HldyDao {
	
	/**
	 * 判断是否需要自动生成账务文件
	 * @param postdate
	 * @return
	 * @throws Exception
	 */
	boolean isAutoToCreateFileForAcups(Map<String,Object> map) throws Exception;
	
}
