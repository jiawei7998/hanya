package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class BatchCondition1 extends Group{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Id",Field.class);
		keyClassMap.put("StrtDt",Field.class);
		keyClassMap.put("EndDt",Field.class);
	}
	
	public BatchCondition1(String tag){
		super(tag,tag,new String[]{"Id","StrtDt","EndDt"});
	}
	public BatchCondition1(String tag,String delim){
		super(tag,tag,new String[]{"Id","StrtDt","EndDt"});
	}
	public void set(Exact11Text value){
		setField(value);
	}
	
	public Exact11Text get(Exact11Text value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(Exact11Text field) {
		return isSetField(field);
	}

	public boolean isSetExact11Text() {
		return isSetField("Id");
	}
	
	public void set(ISODate exact11Text){
		setField(exact11Text);
	}
	

}
