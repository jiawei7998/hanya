/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class CashSettlementDelivery extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("CshSttlmAmt", Field.class);
		keyClassMap.put("CshSttlmDrctn", Field.class);
	}
	
	public CashSettlementDelivery(String tag){
		super(tag,tag,new String[]{"CshSttlmAmt","CshSttlmDrctn"});
	}
	
	public CashSettlementDelivery(String tag,String delim){
		super(tag,tag,new String[]{"CshSttlmAmt","CshSttlmDrctn"});
	}
	public void set(CommonCurrencyAndAmount field){
		setField(field);
	}
	public void set(CashSettlementDirectionCode field){
		setField(field);
	}
}
