package com.singlee.financial.cdtc.pac;

import java.util.Date;

public class ISODateTime extends UtcTimeStampField
{
	  static final long serialVersionUID = 20080903L;

	  public ISODateTime(String field,String data) {
	    super(field, data, true);
	  }

	  public ISODateTime(String field,Date data) {
	    super(field, data, true);
	  }
	  public static void main(String[] args) {
		  ISODateTime isTime = new ISODateTime("ISODateTime",new Date());
		  System.out.println(isTime.toXml());
	}
	  
	  
	}