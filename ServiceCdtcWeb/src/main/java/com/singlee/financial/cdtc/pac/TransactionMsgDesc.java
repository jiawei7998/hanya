/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 * 报文描述
 */
public class TransactionMsgDesc extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("MsgOrgnlSndr", Field.class);
		keyClassMap.put("MsgFnlRcvr", Field.class);
		keyClassMap.put("MsgSndDateTime", Field.class);
		keyClassMap.put("MsgTp", Field.class);
		keyClassMap.put("MsgId", Field.class);
		keyClassMap.put("MsgRefId", Field.class);
		keyClassMap.put("MsgBodyChcksum", Field.class);
	}
	
	public TransactionMsgDesc(String tag){
		super(tag,tag,new String[]{"MsgOrgnlSndr","MsgFnlRcvr","MsgSndDateTime","MsgTp","MsgId","MsgRefId","MsgBodyChcksum"});
	}
	public TransactionMsgDesc(String tag,String belim){
		super(tag,tag,new String[]{"MsgOrgnlSndr","MsgFnlRcvr","MsgSndDateTime","MsgTp","MsgId","MsgRefId","MsgBodyChcksum"});
	
	}
	public void set(Max12Text field){
		setField(field);
	}
	public void set(ISODateTime field){
		setField(field);
	}
	public void set(Max15Text field){
		setField(field);
	}
	public void set(Max26Text field){
		setField(field);
	}
	public void set(Max32Text field){
		setField(field);
	}
}
