/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *报文接收方
 */
public class TransactionRcvr extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("RcvrId", Field.class);
		keyClassMap.put("RcvrSysId", Field.class);
	}
	
	public TransactionRcvr(String tag){
		super(tag,tag,new String[]{"RcvrId","RcvrSysId"});
	}
	public TransactionRcvr(String tag,String belim){
		super(tag,tag,new String[]{"RcvrId","RcvrSysId"});
	
	}
	public void set(Max12Text field){
		setField(field);
	}
	public void set(Max2Text field){
		setField(field);
	}
}
