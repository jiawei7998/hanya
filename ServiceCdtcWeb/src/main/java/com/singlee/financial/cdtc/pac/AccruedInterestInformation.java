package com.singlee.financial.cdtc.pac;

import java.util.Date;
import java.util.HashMap;

public class AccruedInterestInformation  extends Group{
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("AcrdIntrst", Field.class);
		keyClassMap.put("AcrdIntrstDt", Field.class);
	}
	
	public AccruedInterestInformation(String tag){
		
		super(tag,tag, new String[] { "AcrdIntrst", "AcrdIntrstDt"});
	}
public AccruedInterestInformation(String tag,String delim){
		
		super(tag,tag, new String[] { "AcrdIntrst", "AcrdIntrstDt"});
	}
   public void set(ValueCurrencyAndAmount value) {
        setField(value);
   }

   public ValueCurrencyAndAmount get(ValueCurrencyAndAmount value) throws FieldNotFound
   {
    getField(value);

    return value;
  }

  public ValueCurrencyAndAmount getValueCurrencyAndAmount() throws FieldNotFound
  {
	  ValueCurrencyAndAmount value = new ValueCurrencyAndAmount("","");
      getField(value);

      return value;
  }

  public boolean isSet(ValueCurrencyAndAmount field) {
    return isSetField(field);
  }

  public boolean isSetValueCurrencyAndAmount() {
    return isSetField("AcrdIntrst");
  }
  public void set(ISODate value) {
        setField(value);
   }

 public ISODate get(ISODate value) throws FieldNotFound
 {
  getField(value);

  return value;
 }

 public ISODate getISODate() throws FieldNotFound
 {
	 ISODate value = new ISODate("AcrdIntrstDt",new Date());
    getField(value);

    return value;
 }

 public boolean isSet(ISODate field) {
  return isSetField(field);
 }

 public boolean isSetISODate() {
  return isSetField("AcrdIntrstDt");
 }
 
  public void toXmlForSuperParent(StringBuffer xmlBuffer)
  {
	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
	  this.toXml(xmlBuffer);
	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
  }
}
