package com.singlee.financial.cdtc.pac;

public class Max8Text extends StringField {

	private static final long serialVersionUID = 1L;
	
	public Max8Text(String field) {
		
		super(field);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.singlee.client.cdtc.pac.IntField#getValue()
	 */
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(super.getLength()>=1 && super.getLength()<=8)
		{
			return toXml();
		}
		return "";
	}
}
