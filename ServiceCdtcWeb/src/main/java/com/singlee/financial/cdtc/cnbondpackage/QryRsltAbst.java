package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.Max5NumericText;
import com.singlee.financial.cdtc.pac.Max8Text;

public class QryRsltAbst extends Group {

	/**
	 * 6.3.6	结算业务批量查询状态报告
	 * 查询结果摘要   QryRsltAbst组件
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("TxRsltCd",   Field.class);
		keyClassMap.put("QryRsltCnt", Field.class);
	}
	
	public QryRsltAbst(String tag){
		/**
		 * TxRsltCd    查询处理结果返回码
		 * QryRsltCnt  查询结果计数
		 */
		super(tag,tag,new String[]{"TxRsltCd","QryRsltCnt"});
	}
	
	public QryRsltAbst(String tag,String delim){
		/**
		 * TxRsltCd    查询处理结果返回码
		 * QryRsltCnt  查询结果计数
		 */
		super(tag,tag,new String[]{"TxRsltCd","QryRsltCnt"});
	}
	
	public void set(Max8Text value){
		setField(value);
	}
	
	public Max8Text get(Max8Text value) throws FieldNotFound {
		getField(value);
		return value;
	}
	

	public boolean isSet(Max8Text field) {
		return isSetField(field);
	}

	public boolean isSetMax8Text() {
		return isSetField("TxRsltCd");
	}
	
	
	public void set(Max5NumericText value){
		setField(value);
	}
	
	public Max5NumericText get(Max5NumericText value) throws FieldNotFound {
		getField(value);
		return value;
	}

	public boolean isSet(Max5NumericText field) {
		return isSetField(field);
	}

	public boolean isSetMax5NumericText() {
		return isSetField("QryRsltCnt");
	}
	
}
