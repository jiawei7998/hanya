/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 */
public class BatchCondition extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("BthCond1", BatchCondition1.class);
		keyClassMap.put("BthCond2", BatchCondition2.class);
		keyClassMap.put("BthCond3", BatchCondition3.class);
		keyClassMap.put("BthCond4", BatchCondition4.class);
		keyClassMap.put("BthCond5", BatchCondition5.class);
		keyClassMap.putAll(BatchCondition1.keyClassMap);
		keyClassMap.putAll(BatchCondition2.keyClassMap);
		keyClassMap.putAll(BatchCondition3.keyClassMap);
		keyClassMap.putAll(BatchCondition4.keyClassMap);
		keyClassMap.putAll(BatchCondition5.keyClassMap);
	}
	
	public BatchCondition(String tag){
		super(tag,tag,new String[]{"BthCond1","BthCond2","BthCond3","BthCond4","BthCond5"});
	}
	
	public BatchCondition(String tag,String belim){
		super(tag,tag,new String[]{"BthCond1","BthCond2","BthCond3","BthCond4","BthCond5"});
	}
	
	public void set(BatchCondition1 group){
		addGroup(group);
	}
	public void set(BatchCondition2 group){
		addGroup(group);
	}
	public void set(BatchCondition3 group){
		addGroup(group);
	}
	public void set(BatchCondition4 group){
		addGroup(group);
	}
	public void set(BatchCondition5 group){
		addGroup(group);
	}
	
}
