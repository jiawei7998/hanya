package com.singlee.financial.cdtc.dao;

import java.util.HashMap;
import java.util.List;
import java.util.IdentityHashMap;

import com.singlee.financial.cdtc.bean.CSBSCodeBean;
import com.singlee.financial.cdtc.bean.CSBSDataBean;

public interface CSBSDataManagerDao {

	/**
	 * 查询所有CSBS数据字典信息 
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, IdentityHashMap<String,CSBSDataBean>> queryAllCsbsData() throws Exception;
	
	/**
	 * 查询CSBS数据字典中MsgName消息名
	 * @return
	 * @throws Exception
	 */
	public List<String> queryAllCsbsDataMsgName() throws Exception;
	
	/**
	 * 查询单个CSBS报文 
	 * @param MsgName    报文名称
	 * @return
	 * @throws Exception
	 */
	public List<CSBSDataBean> queryCsbsMessageByMsgname(String MsgName) throws Exception;
	
	/**
	 * 查询12个中债登报文及组件信息
	 * @return
	 * @throws Exception
	 */
	public CSBSDataBean query12CsbsMsgName(String MsgName,String MsgIdentify) throws Exception;
	
	/**
	 * 查询所有数据类型代码
	 * @return
	 * @throws Exception
	 */
	public List<CSBSCodeBean> queryCsbsCode() throws Exception;
	
	/**
	 * 查询代码类型的描述说明
	 * @param type
	 * @return
	 * @throws Exception
	 */
	public CSBSCodeBean queryCsbsCodeName(String type) throws Exception;
	
}
