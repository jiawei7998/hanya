package com.singlee.financial.cdtc.bean;

import java.io.Serializable;

public class CSBSCodeBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String TYPE;
	
	private String CODE;
	
	private String REMARK;
	
	private String CODENAME;

	@Override
	public String toString() {
			// TODO Auto-generated method stub
		return getCODE();
	}
	/**
	 * @return the tYPE
	 */
	public String getTYPE() {
		return TYPE;
	}

	/**
	 * @param type the tYPE to set
	 */
	public void setTYPE(String type) {
		TYPE = type;
	}

	/**
	 * @return the cODE
	 */
	public String getCODE() {
		return CODE;
	}

	/**
	 * @param code the cODE to set
	 */
	public void setCODE(String code) {
		CODE = code;
	}

	/**
	 * @return the rEMARK
	 */
	public String getREMARK() {
		return REMARK;
	}

	/**
	 * @param remark the rEMARK to set
	 */
	public void setREMARK(String remark) {
		REMARK = remark;
	}

	/**
	 * @return the cODENAME
	 */
	public String getCODENAME() {
		return CODENAME;
	}

	/**
	 * @param codename the cODENAME to set
	 */
	public void setCODENAME(String codename) {
		CODENAME = codename;
	}
	
}
