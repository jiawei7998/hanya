/**
 * 
 */
package com.singlee.financial.cdtc.dao;

import java.sql.SQLException;
import java.util.List;

import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;


/**
 * @author Fang
 *
 */
public interface CnbondManagerDao{

	/**
	 * 添加报文数据
	 * @param cnbond
	 * @return
	 */
	public boolean addSlintfccnbondpkg(AbstractSlIntfcCnbondPkgId cnbond) throws Exception;
	
	
	/**
	 * 根据名称读取报文数据
	 */
	public AbstractSlIntfcCnbondPkgId getSlintfccnbondpkgByMsgtype(String msgfilename) throws SQLException, Exception;
	
	/**
	 * 修改报文数据
	 */
	public boolean updateCnbond(AbstractSlIntfcCnbondPkgId cnbond) throws Exception;

	/**
	 * 获取日期下的全部通用结算指令报告BEAN
	 */
	public List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDbByDateType(String date,String type) throws Exception;
	/**
	 * 获取全部通用结算指令报告BEAN
	 */
	public List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDb() throws Exception;
	/**
	 * 根据条件查询
	 */
	public List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDbByTrem(String type,String begindate,String enddate,String txtMsgTypeName) throws Exception;

}
