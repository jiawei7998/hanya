package com.singlee.financial.cdtc.pac;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class LocalMktTimeConverter extends AbstractDateTimeConverter
{
	  private static ThreadLocal<LocalMktTimeConverter> localTimeConverter = new ThreadLocal<LocalMktTimeConverter>();

	  private DateFormat localTimeFormat = createLocalDateFormat("HH:mm:ss");

	  private DateFormat localTimeFormatMillis = createLocalDateFormat("HH:mm:ss.SSS");

	  public static String convert(Date d, boolean includeMilliseconds)
	  {
	    return getFormatter(includeMilliseconds).format(d);
	  }

	  private static DateFormat getFormatter(boolean includeMillis) {
	    LocalMktTimeConverter converter = (LocalMktTimeConverter)localTimeConverter.get();
	    if (converter == null) {
	      converter = new LocalMktTimeConverter();
	      localTimeConverter.set(converter);
	    }
	    return includeMillis ? converter.localTimeFormatMillis : converter.localTimeFormat;
	  }

	  public static Date convert(String value)
	    throws FieldConvertError
	  {
	    verifyFormat(value);
	    Date d = null;
	    try {
	      d = getFormatter(value.length() == 12).parse(value);
	    } catch (ParseException e) {
	      throwFieldConvertError(value, "time");
	    }
	    return d;
	  }

	  private static void verifyFormat(String value) throws FieldConvertError
	  {
	    String type = "time";
	    if ((value.length() != 8) && (value.length() != 12)) {
	      throwFieldConvertError(value, type);
	    }
	    assertDigitSequence(value, 0, 2, type);

	    checkHour(value, 0, 2, type);
	    assertSeparator(value, 2, ':', type);
	    assertDigitSequence(value, 3, 5, type);

	    checkMinute(value, 3, 5, type);
	    assertSeparator(value, 5, ':', type);
	    assertDigitSequence(value, 6, 8, type);

	    checkSecond(value, 6, 8, type);
	    if (value.length() == 12) {
	      assertSeparator(value, 8, '.', type);
	      assertDigitSequence(value, 9, 12, type);
	    } else if (value.length() != 8) {
	      throwFieldConvertError(value, type);
	    }
	  }
	}