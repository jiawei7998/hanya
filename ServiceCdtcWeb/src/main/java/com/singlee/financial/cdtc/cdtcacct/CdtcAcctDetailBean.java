package com.singlee.financial.cdtc.cdtcacct;

import java.io.Serializable;
import java.math.BigDecimal;

public class CdtcAcctDetailBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String securityDate;
	
	private String securityTime;
	
	private String htNo;
	
	private String remark;
	/**
	 * 债券简称
	 */
	private String secidNm;
	/**
	 * 债券代码
	 */
	private String secid;
	/**
	 * 可用 万元
	 */
	private BigDecimal ableUseAmt;
	/**
	 * 待付 万元
	 */
	private BigDecimal waitPayAmt;
	/**
	 * 质押 万元
	 */
	private BigDecimal repoZYAmt;
	/**
	 * 待购回 万元
	 */
	private BigDecimal repoRevAmt;
	/**
	 * 冻结
	 */
	private BigDecimal frozenAmt;
	/**
	 * 发生额
	 */
	private BigDecimal dealAmt;

	public CdtcAcctDetailBean() {
		super();
	}
	/**
	 * @return the secidNm
	 */
	public String getSecidNm() {
		return secidNm;
	}
	/**
	 * @param secidNm the secidNm to set
	 */
	public void setSecidNm(String secidNm) {
		this.secidNm = secidNm;
	}
	/**
	 * @return the secid
	 */
	public String getSecid() {
		return secid;
	}
	/**
	 * @param secid the secid to set
	 */
	public void setSecid(String secid) {
		this.secid = secid;
	}
	/**
	 * @return the ableUseAmt
	 */
	public BigDecimal getAbleUseAmt() {
		return ableUseAmt;
	}
	/**
	 * @param ableUseAmt the ableUseAmt to set
	 */
	public void setAbleUseAmt(BigDecimal ableUseAmt) {
		this.ableUseAmt = ableUseAmt;
	}
	/**
	 * @return the waitPayAmt
	 */
	public BigDecimal getWaitPayAmt() {
		return waitPayAmt;
	}
	/**
	 * @param waitPayAmt the waitPayAmt to set
	 */
	public void setWaitPayAmt(BigDecimal waitPayAmt) {
		this.waitPayAmt = waitPayAmt;
	}
	/**
	 * @return the repoZYAmt
	 */
	public BigDecimal getRepoZYAmt() {
		return repoZYAmt;
	}
	/**
	 * @param repoZYAmt the repoZYAmt to set
	 */
	public void setRepoZYAmt(BigDecimal repoZYAmt) {
		this.repoZYAmt = repoZYAmt;
	}
	/**
	 * @return the repoRevAmt
	 */
	public BigDecimal getRepoRevAmt() {
		return repoRevAmt;
	}
	/**
	 * @param repoRevAmt the repoRevAmt to set
	 */
	public void setRepoRevAmt(BigDecimal repoRevAmt) {
		this.repoRevAmt = repoRevAmt;
	}
	/**
	 * @return the frozenAmt
	 */
	public BigDecimal getFrozenAmt() {
		return frozenAmt;
	}
	/**
	 * @param frozenAmt the frozenAmt to set
	 */
	public void setFrozenAmt(BigDecimal frozenAmt) {
		this.frozenAmt = frozenAmt;
	}
	/**
	 * @return the securityDate
	 */
	public String getSecurityDate() {
		return securityDate;
	}
	/**
	 * @param securityDate the securityDate to set
	 */
	public void setSecurityDate(String securityDate) {
		this.securityDate = securityDate;
	}
	/**
	 * @return the securityTime
	 */
	public String getSecurityTime() {
		return securityTime;
	}
	/**
	 * @param securityTime the securityTime to set
	 */
	public void setSecurityTime(String securityTime) {
		this.securityTime = securityTime;
	}
	/**
	 * @return the htNo
	 */
	public String getHtNo() {
		return htNo;
	}
	/**
	 * @param htNo the htNo to set
	 */
	public void setHtNo(String htNo) {
		this.htNo = htNo;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * @return the dealAmt
	 */
	public BigDecimal getDealAmt() {
		return dealAmt;
	}
	/**
	 * @param dealAmt the dealAmt to set
	 */
	public void setDealAmt(BigDecimal dealAmt) {
		this.dealAmt = dealAmt;
	}
}
