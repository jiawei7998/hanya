package com.singlee.financial.cdtc.pac;

public class BillingFee extends Group {
	private static final long serialVersionUID = 1L;

	public BillingFee(String tag) {

		super(tag, tag, new String[] { "BllgItem", "Val" });
	}

	public void set(BillingCode value) {
		setField(value);
	}

	public BillingCode get(BillingCode value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public BillingCode getBillingCode() throws FieldNotFound {
		BillingCode value = new BillingCode("BllgItem");
		getField(value);

		return value;
	}

	public boolean isSet(BillingCode field) {
		return isSetField(field);
	}

	public boolean isSetBillingCode() {
		return isSetField("BllgItem");
	}

	public void set(CommonCurrencyAndAmount value) {
		setField(value);
	}

	public CommonCurrencyAndAmount get(CommonCurrencyAndAmount value)
			throws FieldNotFound {
		getField(value);

		return value;
	}

	public CommonCurrencyAndAmount getCommonCurrencyAndAmount()
			throws FieldNotFound {
		CommonCurrencyAndAmount value = new CommonCurrencyAndAmount("", "");
		getField(value);

		return value;
	}

	public boolean isSet(CommonCurrencyAndAmount field) {
		return isSetField(field);
	}

	public boolean isSetCommonCurrencyAndAmount() {
		return isSetField("Val");
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

}
