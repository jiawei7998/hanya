/**
 * 
 */
package com.singlee.financial.cdtc.cdtcacct;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Fang
 * 
 */
public class CdtcAcctSpshBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String BR;// opics机构
	private String DEALNO;// 交易流水
	private String PS;// 买卖方向
	private String PRODCODE;// 产品
	private String PRODTYPE;// 类型
	private String SECID;// 债券ID
	private String SECIDNM;// 债券名称
	private String CIF;// 客户号
	private String CUSTNM;// 客户名称
	private String CFETSSN;// 交易中心编号
	private Date DEALDATE;// 交易日
	private Date VDATE;// 起息日/债券清算日
	private Date MATDATE;// 到期日
	private BigDecimal FACEAMT;// 面值
	private BigDecimal COMPROCDAMT;// 首期金额/净价金额
	private BigDecimal MATPROCDAMT;// 到期金额/全价金额
	private Date REVDATE; //冲销时间
	private String DEALSTATUS;// V/复核 S/清算添加 R/冲销 OPICS 资金的状态
	private String DEALTEXT;// 交易备注
	private String COMCCYSMEANS;// 首期CCY 结算方式
	private String MATCCYSMEANS;// 到期CCY 结算方式
	private String SETTTYPE_B;// 原始交易单结算方式 首期
	private String SETTDESCR_B;// 原始交易单结算方式描述 首期
	private String SETTTYPE_E;// 原始交易单结算方式 到期
	private String SETTDESCR_E;// 原始交易单结算方式描述 到期
	
	@Override
	public String toString() {
		return getDEALNO();
	}
	
	public String getBR() {
		return BR;
	}
	public void setBR(String bR) {
		BR = bR;
	}
	public String getDEALNO() {
		return DEALNO;
	}
	public void setDEALNO(String dEALNO) {
		DEALNO = dEALNO;
	}
	public String getPS() {
		return PS;
	}
	public void setPS(String pS) {
		PS = pS;
	}
	public String getPRODCODE() {
		return PRODCODE;
	}
	public void setPRODCODE(String pRODCODE) {
		PRODCODE = pRODCODE;
	}
	public String getPRODTYPE() {
		return PRODTYPE;
	}
	public void setPRODTYPE(String pRODTYPE) {
		PRODTYPE = pRODTYPE;
	}
	public String getSECID() {
		return SECID;
	}
	public void setSECID(String sECID) {
		SECID = sECID;
	}
	public String getSECIDNM() {
		return SECIDNM;
	}
	public void setSECIDNM(String sECIDNM) {
		SECIDNM = sECIDNM;
	}
	public String getCIF() {
		return CIF;
	}
	public void setCIF(String cIF) {
		CIF = cIF;
	}
	public String getCUSTNM() {
		return CUSTNM;
	}
	public void setCUSTNM(String cUSTNM) {
		CUSTNM = cUSTNM;
	}
	public String getCFETSSN() {
		return CFETSSN;
	}
	public void setCFETSSN(String cFETSSN) {
		CFETSSN = cFETSSN;
	}
	public Date getDEALDATE() {
		return DEALDATE;
	}
	public void setDEALDATE(Date dEALDATE) {
		DEALDATE = dEALDATE;
	}
	public Date getVDATE() {
		return VDATE;
	}
	public void setVDATE(Date vDATE) {
		VDATE = vDATE;
	}
	public Date getMATDATE() {
		return MATDATE;
	}
	public void setMATDATE(Date mATDATE) {
		MATDATE = mATDATE;
	}
	public BigDecimal getFACEAMT() {
		return FACEAMT;
	}
	public void setFACEAMT(BigDecimal fACEAMT) {
		FACEAMT = fACEAMT;
	}
	public BigDecimal getCOMPROCDAMT() {
		return COMPROCDAMT;
	}
	public void setCOMPROCDAMT(BigDecimal cOMPROCDAMT) {
		COMPROCDAMT = cOMPROCDAMT;
	}
	public BigDecimal getMATPROCDAMT() {
		return MATPROCDAMT;
	}
	public void setMATPROCDAMT(BigDecimal mATPROCDAMT) {
		MATPROCDAMT = mATPROCDAMT;
	}
	public String getDEALSTATUS() {
		return DEALSTATUS;
	}
	public void setDEALSTATUS(String dEALSTATUS) {
		DEALSTATUS = dEALSTATUS;
	}
	public String getDEALTEXT() {
		return DEALTEXT;
	}
	public void setDEALTEXT(String dEALTEXT) {
		DEALTEXT = dEALTEXT;
	}
	public String getCOMCCYSMEANS() {
		return COMCCYSMEANS;
	}
	public void setCOMCCYSMEANS(String cOMCCYSMEANS) {
		COMCCYSMEANS = cOMCCYSMEANS;
	}
	public String getMATCCYSMEANS() {
		return MATCCYSMEANS;
	}
	public void setMATCCYSMEANS(String mATCCYSMEANS) {
		MATCCYSMEANS = mATCCYSMEANS;
	}
	public String getSETTTYPE_B() {
		return SETTTYPE_B;
	}
	public void setSETTTYPE_B(String sETTTYPEB) {
		SETTTYPE_B = sETTTYPEB;
	}
	public String getSETTDESCR_B() {
		return SETTDESCR_B;
	}
	public void setSETTDESCR_B(String sETTDESCRB) {
		SETTDESCR_B = sETTDESCRB;
	}
	public String getSETTTYPE_E() {
		return SETTTYPE_E;
	}
	public void setSETTTYPE_E(String sETTTYPEE) {
		SETTTYPE_E = sETTTYPEE;
	}
	public String getSETTDESCR_E() {
		return SETTDESCR_E;
	}
	public void setSETTDESCR_E(String sETTDESCRE) {
		SETTDESCR_E = sETTDESCRE;
	}
	public Date getREVDATE() {
		return REVDATE;
	}
	public void setREVDATE(Date rEVDATE) {
		REVDATE = rEVDATE;
	}

}
