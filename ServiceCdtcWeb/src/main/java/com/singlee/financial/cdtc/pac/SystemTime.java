package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class SystemTime {
	public static final TimeZone Local_TIMEZONE = TimeZone.getTimeZone("Asia/Shanghai");

	public static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("Etc/UTC");

	private static SystemTimeSource DEFAULT_TIME_SOURCE = new SystemTimeSource() {
		@Override
		public long getTime() {
			return System.currentTimeMillis();
		}
	};

	private static SystemTimeSource systemTimeSource = DEFAULT_TIME_SOURCE;

	public static synchronized long currentTimeMillis() {
		return systemTimeSource.getTime();
	}

	public static Date getDate() {
		return new Date(currentTimeMillis());
	}

	public static synchronized void setTimeSource(SystemTimeSource systemTimeSource) {
		systemTimeSource = systemTimeSource != null ? systemTimeSource : DEFAULT_TIME_SOURCE;
	}

	public static Calendar getLocalCalendar() {
		Calendar c = Calendar.getInstance(Local_TIMEZONE);
		c.setTimeInMillis(currentTimeMillis());
		return c;
	}

	public static Calendar getLocalCalendar(Date date) {
		Calendar c = getLocalCalendar();
		c.setTime(date);
		return c;
	}

	public static Calendar getUtcCalendar() {
		Calendar c = Calendar.getInstance(UTC_TIMEZONE);
		c.setTimeInMillis(currentTimeMillis());
		return c;
	}

	public static Calendar getUtcCalendar(Date date) {
		Calendar c = getUtcCalendar();
		c.setTime(date);
		return c;
	}
}