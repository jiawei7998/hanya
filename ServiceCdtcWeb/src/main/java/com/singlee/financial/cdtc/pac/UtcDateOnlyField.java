package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UtcDateOnlyField extends DateField {

	  private static final long serialVersionUID = 1L;
	  private static TimeZone timezone = TimeZone.getTimeZone("UTC");
	  private static Calendar calendar;

	  public UtcDateOnlyField(String field)
	  {
	    super(field, UtcDateOnlyConverter.convert(createDate()));
	  }

	  protected UtcDateOnlyField(String field, Date data)
	  {
	    super(field, UtcDateOnlyConverter.convert(data));
	  }

	  protected UtcDateOnlyField(String field, String data)
	  {
	    super(field, data);
	  }

	  public void setValue(Date value) {
	    setValue(UtcDateOnlyConverter.convert(value));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(UtcDateOnlyConverter.convert(value));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }
}
