/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.dom4j.DocumentException;

import com.singlee.financial.cdtc.httpclient.XmlFormat;

/**
 * @author Fang
 *
 */
public class DetailedStatement1 extends Group {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DetailedStatement1(String tag){
		super(tag,tag,new String[]{"svcHdr"});
	}
	public void set(TransactionSvcHdr group) {
		addGroup(group);
	}
	public void toXmlForSuperParent(StringBuffer xmlBuffer)
    {
  	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
  	  this.toXml(xmlBuffer);
  	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
    }
	
	public static void main(String[] args) {
		DetailedStatement1 detailedStatement1 = new DetailedStatement1("reqt");
		 
		TransactionSvcHdr transactionSvcHdr = new TransactionSvcHdr("svcHdr");
		
		Max64Text max64Text1 = new Max64Text("corrId");
		max64Text1.setValue("");
		transactionSvcHdr.set(max64Text1);
		
		Max64Text max64Text = new Max64Text("svcId");
		transactionSvcHdr.set(max64Text);
		
		Max8Text max8Text = new Max8Text("verNbr");
		max8Text.setValue("1.0");
		transactionSvcHdr.set(max8Text);
		
		Max6Text max6Text = new Max6Text("csmrId");
		max6Text.setValue("197000");
		transactionSvcHdr.set(max6Text);
		
		Max6Text max6Text1 = new Max6Text("pvdrId");
		max6Text1.setValue("164000");
		transactionSvcHdr.set(max6Text1);
		
		Max32Text max32Text = new Max32Text("pvdrSerNbr");
		max32Text.setValue("200020002000");
		transactionSvcHdr.set(max32Text);
		
		Max256Text max256Text = new Max256Text("respMsg");
		max256Text.setValue("交易处理成功");
		transactionSvcHdr.set(max256Text);
		
		transactionSvcHdr.set(new ISODateTime("tmStamp",new Date()));

		detailedStatement1.set(transactionSvcHdr);
		
		StringBuffer stringBuffer = new StringBuffer();
		detailedStatement1.toXmlForSuperParent(stringBuffer);

		try {
			System.out.println(XmlFormat.format(stringBuffer.toString()));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
