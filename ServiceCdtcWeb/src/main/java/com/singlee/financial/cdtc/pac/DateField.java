package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateField extends Field<String>
{
	  private static final long serialVersionUID = 1L;

	  private static TimeZone timezone = TimeZone.getTimeZone("CST");
	  private static Calendar calendar;

	  protected DateField(String field, String data)
	  {
	    super(field, data);
	  }

	  @SuppressWarnings("unused")
	private static Date createDate() {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }

	  public void setValue(String value)
	  {
	    setObject(value);
	  }

	  public String getValue()
	  {
	    return (String)getObject();
	  }

	  public boolean valueEquals(String value)
	  {
	    return ((String)getObject()).equals(value);
	  }
}