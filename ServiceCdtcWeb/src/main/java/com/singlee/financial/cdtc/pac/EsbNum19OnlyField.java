package com.singlee.financial.cdtc.pac;

public class EsbNum19OnlyField extends StringField {

	private static final long serialVersionUID = 1L;

	public EsbNum19OnlyField(String field) {
		super(field);
	}
	
	public EsbNum19OnlyField(String field,int cifNo) {
		super(field,EsbNum19OnlyConverter.convert(cifNo));
	}
	
	public EsbNum19OnlyField(String field,String cifNo) {
		super(field,EsbNum19OnlyConverter.convert(cifNo));
	}

}
