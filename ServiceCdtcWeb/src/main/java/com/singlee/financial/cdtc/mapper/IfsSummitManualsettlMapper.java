package com.singlee.financial.cdtc.mapper;

import java.util.List;
import java.util.Map;

import com.singlee.financial.bean.BtchQryRslt;

public interface IfsSummitManualsettlMapper {
    
	int insert(BtchQryRslt record);

    int insertSelective(BtchQryRslt record);
    
    List<BtchQryRslt> selectBtchQryRslt(Map<String,Object> map);
    
    int update(BtchQryRslt record);
    
    int delete(BtchQryRslt record);
}