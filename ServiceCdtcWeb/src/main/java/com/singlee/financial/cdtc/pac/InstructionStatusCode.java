package com.singlee.financial.cdtc.pac;


/**
 *  IS00：成功
IS01：待复核
IS02：已复核
IS03：待确认
IS04：已确认
IS05：合法
IS06：非法
IS07：作废
IS08：撤销

 * @author Administrator
 *
 */
public class InstructionStatusCode extends StringField {
	public static enum Type {
	IS00("IS00","成功  "),
	IS01("IS01","待复核"),
	IS02("IS02","已复核"),
	IS03("IS03","待确认"),
	IS04("IS04","已确认"),
	IS05("IS05","合法  "),
	IS06("IS06","非法  "),
	IS07("IS07","作废  "),
	IS08("IS08","撤销  ");
	private String type;
	
	private String name;

	
	private Type(String type, String name) {
		this.type = type;
		this.name = name;
	}
	// 普通方法   
    public static String getName(String type) {   
        for (Type c : Type.values()) {   
            if (c.getType().equalsIgnoreCase(type)) {   
               return c.name;   
            }   
        }   
        return "";   
    }  
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.type+":"+this.name;
	}
}
	public InstructionStatusCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
