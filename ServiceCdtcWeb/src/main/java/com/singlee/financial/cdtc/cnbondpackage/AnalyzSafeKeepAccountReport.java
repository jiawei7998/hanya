package com.singlee.financial.cdtc.cnbondpackage;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.singlee.financial.bean.CdtcAcctTotalBean;
import com.singlee.financial.cdtc.pac.AccountLedgerTypeCode;
import com.singlee.financial.cdtc.server.CdtcSecCheckAccountServerImpl;

public class AnalyzSafeKeepAccountReport {
	private static Logger logger = LoggerFactory.getLogger(CdtcSecCheckAccountServerImpl.class);
	
	/**
	 * 传入XML报文，报文类型，和数据字典
	 * 解析报文
	 */
	@SuppressWarnings("unchecked")
	public static LinkedHashMap<Integer, CdtcAcctTotalBean> analyzCSBSSafekeepAccountMsg(Element element){

		List<Element> elements = element.elements();
		
		LinkedHashMap<Integer, CdtcAcctTotalBean> cdtcHashMap = new LinkedHashMap<Integer, CdtcAcctTotalBean>();
		CdtcAcctTotalBean cdtcAcctTotalBean = new CdtcAcctTotalBean();
		//迭代取出XML文件
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			List<Element> elements2 = element2.elements();
			
			if(elements2.size()>=0)
			{
				
				if(element2.elements().size()>0)
				{
//					System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
//							String.format("%30s :", "")+" "+" "+"IS GROUP");
					//判断GROUP是否是组件
					group(element2,cdtcHashMap,cdtcAcctTotalBean);
					
				}else {
					
					System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
							String.format("%20s :", StringUtils.trimToEmpty(element2.getText()))+" "+"IS FIELD");
					
				}
				
			}
		}
		return cdtcHashMap;
	}
	
	@SuppressWarnings("unchecked")
	public static void group(Element element,LinkedHashMap<Integer, CdtcAcctTotalBean> cdtcHashMap,CdtcAcctTotalBean cdtcAcctTotalBean){
		List<Element> elements = element.elements();
		
		int count = 0;
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			
			if(element2.elements().size()>0)
			{
				if(StringUtils.trimToEmpty(element2.getName()).endsWith("Bd")){
					System.out.println("I'm Bd");
					cdtcAcctTotalBean = new CdtcAcctTotalBean();
					String string = "";
					cdtcHashMap.put(count, groupBd(element2,cdtcAcctTotalBean,string));
					
					count++;
				}else{

					group(element2,cdtcHashMap,cdtcAcctTotalBean);
				}
				
			}else {
				System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
						String.format("%20s :", StringUtils.trimToEmpty(element2.getText()))+" "+"IS FIELD");
			}
		
		}
	}
	
	@SuppressWarnings("unchecked")
	public static CdtcAcctTotalBean groupBd(Element element, CdtcAcctTotalBean bean, String string){
		try {
			
			List<Element> elements = element.elements();

			for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
			{
				Element element2 = (Element)iterator.next();
				if(element2.elements().size()>0)
				{
					groupBd(element2,bean,string);
					System.out.println("xxxxxxxxxxx");
				}else {
					System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
							String.format("%20s :", StringUtils.trimToEmpty(element2.getText()))+" "+"IS FIELD3");		
					if("BdId".equals(StringUtils.trimToEmpty(element2.getName()))){
						bean.setSecid(StringUtils.trimToEmpty(element2.getText()));
					}else if("BdShrtNm".equals(StringUtils.trimToEmpty(element2.getName()))){
						bean.setSecidNm(StringUtils.trimToEmpty(element2.getText()));
					}else if("LdgTp".equals(StringUtils.trimToEmpty(element2.getName()))){
						string = StringUtils.trimToEmpty(element2.getText());
					}else if("Bal".equals(StringUtils.trimToEmpty(element2.getName()))){
						/**
						 * 	2	AL	AL03	待付	账户科目类型代码
							3	AL	AL04	质押式待回购	账户科目类型代码
							4	AL	AL02	可用	账户科目类型代码
							5	AL	AL00	承销额度	账户科目类型代码
							6	AL	AL01	承销额度待付	账户科目类型代码
							7	AL	AL08	待确认债权待付	账户科目类型代码
							8	AL	AL09 	三方回购待回购	账户科目类型代码
							9	AL	AL07	待确认债权	账户科目类型代码
							10	AL	AL05	冻结	账户科目类型代码
							11	AL	AL06	质押	账户科目类型代码
						 */
						
						if(string.equals(AccountLedgerTypeCode.Type.AL00.getType())){
							//承销额度
							bean.setCxAmt(new BigDecimal(StringUtils.trimToEmpty(element2.getText())));
						}
						if(string.equals(AccountLedgerTypeCode.Type.AL01.getType())){
							//承销额度待付
							bean.setCxWaitPayAmt(new BigDecimal(StringUtils.trimToEmpty(element2.getText())));
						}
						if(string.equals(AccountLedgerTypeCode.Type.AL02.getType())){
							//可用
							bean.setAbleUseAmt(new BigDecimal(StringUtils.trimToEmpty(element2.getText())));
						}else if(string.equals(AccountLedgerTypeCode.Type.AL03.getType())){
							//待付
						//	waitPayAmt
							bean.setWaitPayAmt(new BigDecimal(StringUtils.trimToEmpty(element2.getText())));
						}else if(string.equals(AccountLedgerTypeCode.Type.AL06.getType())){
							//质押
						//	repoZYAmt
							bean.setRepoZYAmt(new BigDecimal(StringUtils.trimToEmpty(element2.getText())));
						}else if(string.equals(AccountLedgerTypeCode.Type.AL04.getType())){
							//待回购
						//	repoRevAmt
							bean.setRepoRevAmt(new BigDecimal(StringUtils.trimToEmpty(element2.getText())));
						}else if(string.equals(AccountLedgerTypeCode.Type.AL05.getType())){
							//冻结
						//	frozenAmt
							bean.setFrozenAmt(new BigDecimal(StringUtils.trimToEmpty(element2.getText())));
						}
					}
				}
			}
			if(bean.getCxAmt()==null){
				bean.setCxAmt(new BigDecimal("0.00"));
			}
			if(bean.getCxWaitPayAmt()==null){
				bean.setCxWaitPayAmt(new BigDecimal("0.00"));
			}
			
			if(bean.getAbleUseAmt()==null){
				bean.setAbleUseAmt(new BigDecimal("0.00"));
			}
			if(bean.getWaitPayAmt()==null){
				bean.setWaitPayAmt(new BigDecimal("0.00"));
			}
			if(bean.getRepoZYAmt()==null){
				bean.setRepoZYAmt(new BigDecimal("0.00"));
			}
			if(bean.getRepoRevAmt()==null){
				bean.setRepoRevAmt(new BigDecimal("0.00"));
			}
			if(bean.getFrozenAmt()==null){
				bean.setFrozenAmt(new BigDecimal("0.00"));
			}
			
			bean.setTotalAmt(bean.getCxAmt().add(bean.getCxWaitPayAmt()).add(bean.getAbleUseAmt()).add(bean.getWaitPayAmt()).add(bean.getRepoZYAmt()).add(bean.getRepoRevAmt()).add(bean.getFrozenAmt()));
			
		} catch (Exception e) {
			logger.info("解析总对账单异常"+e.getMessage());
			throw new RuntimeException("解析总对账单异常"+e.getMessage());
		}
		return bean;
	}
}
