package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class PartyIdentification30 extends Group {


	private static final long serialVersionUID = 1L;
	
	public PartyIdentification30(String tag){
		
		super(tag,tag, new String[] { "Nm", "PrtryId"});
	}
	
	public PartyIdentification30(String tag,String delim){
		
		super(tag,tag, new String[] { "Nm", "PrtryId"});
	}
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Nm", Field.class);
		keyClassMap.put("PrtryId", GenericIdentification4.class);
		keyClassMap.putAll(GenericIdentification4.keyClassMap);
	}
	
	 public void set(Max70Text value) {
        setField(value);
      }

      public Max70Text get(Max70Text value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public Max70Text getMax70Text() throws FieldNotFound
      {
    	  Max70Text value = new Max70Text("");
          getField(value);

          return value;
      }

      public boolean isSet(Max70Text field) {
        return isSetField(field);
      }

      public boolean isSetMax70Text() {
        return isSetField("Nm");
      }
      
      
      public void set(GenericIdentification4 group) {
          addGroup(group);
        }

        public boolean isSet(GenericIdentification4 field) {
          return isSetField(field.getFieldTag());
        }

        public boolean isSetGenericIdentification4() {
          return isSetField("PrtryId");
        }
      
      public void toXmlForSuperParent(StringBuffer xmlBuffer)
      {
    	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
    	  this.toXml(xmlBuffer);
    	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
      }
      
      
}