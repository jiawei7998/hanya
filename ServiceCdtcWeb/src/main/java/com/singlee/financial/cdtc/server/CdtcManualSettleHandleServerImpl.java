package com.singlee.financial.cdtc.server;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cdc.dmsg.client.DClient;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.financial.cdtc.CdtcManualSettleHandleServer;
import com.singlee.financial.cdtc.cnbondpackage.CdtcBankAccount;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSysExceptionAdviceV01;
import com.singlee.financial.cdtc.dao.CdtcRequestMessageCreateDao;
import com.singlee.financial.cdtc.mapper.IfsSummitManualsettlMapper;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementInstruction;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.OperatorStatusCode;
import com.singlee.financial.cdtc.pac.Priority4Code;
import com.singlee.financial.cdtc.pac.RequestTypeCode;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.pac.TransactionMsg;
import com.singlee.financial.cdtc.pac.TransactionOprtrFlg;
import com.singlee.financial.cdtc.util.ExceptionUtil;
import com.singlee.financial.cdtc.util.XmlFormat;
import com.singlee.financial.pojo.component.RetStatusEnum;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcManualSettleHandleServerImpl implements CdtcManualSettleHandleServer {
	
	private  Logger logger = LoggerFactory.getLogger(CdtcManualSettleHandleServerImpl.class);
	@Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;
	@Autowired
	private CdtcRequestMessageCreateDao cdtcRequestMessageCreateDao;
	@Autowired
	IfsSummitManualsettlMapper ifsSummitManualsettlMapper;
	
	/***
	 * 手工结算-经办
	 */
	@Override
	public SmtOutBean settlInterCdtcMsg(BtchQryRslt btchQryRslt)throws RemoteConnectFailureException, Exception {
		SmtOutBean smtOutBean = new SmtOutBean();
		//结算指令标识
		String qryCondId = btchQryRslt.getInstrId();
		logger.info("---------------ioper:"+btchQryRslt.getIOper());
		
		btchQryRslt = getDetailForIS03Settls(btchQryRslt);
		if(btchQryRslt != null)
		{
			logger.info("---------------ioper:"+btchQryRslt.getIOper());
			
			BtchQryRslt btchqry = getDetailForIS03Settls(btchQryRslt);
			
			logger.info("=========ioper:"+getDetailForIS03Settls(btchqry).getIOper());
			logger.info(XmlFormat.format(btchqry.getCSBS001()));
			DClient dClient = new DClient(wasIp);
			String backXml = dClient.sendMsg(XmlFormat.format(btchqry.getCSBS001().toString()));
			//HashMap<String, String> resultHashMap = dClientDao.sendXmlMessageToCdtcForRequest(XmlFormat.format(btchqry.getCSBS001()));
			if(backXml != null) {
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));  
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
				if(null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {
					/**
					 * 读取请求报文XML 反解析报文
					 */
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
					spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					if(spotSettlementInstruction.hasGroup("Document")) {
						System.out.println("hasGroup(Document)");
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for(Group group : groupsList) {
							// 保存xml文件
							StringBuffer xmlBuffer = new StringBuffer();
							group.toXmlForParent(xmlBuffer);
							System.out.println(xmlBuffer.toString());
							commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
							CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
						}
					}
					String txRsltCd = "";
					if(null != commonDistributionSettlementStatusReport) {
						if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
							Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
							Group instrStsInf = stsFlg.hasGroup("InstrStsInf")?stsFlg.getGroups("InstrStsInf").get(0):null;
							if(null != instrStsInf)
							{
								txRsltCd = instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd")?instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject().toString():" ";
							}
						}
					}
					if("BJ000000".equalsIgnoreCase(txRsltCd)) {
						if(ifsSummitManualsettlMapper.insert(btchqry)>0){
							smtOutBean.setRetStatus(RetStatusEnum.S);
							smtOutBean.setRetCode("SUCCESS");
							smtOutBean.setRetMsg(qryCondId + "待结算手工处理成功");
							logger.info(qryCondId + "待结算手工处理成功");
						}else{
							smtOutBean.setRetStatus(RetStatusEnum.F);
							smtOutBean.setRetCode("ERROR");
							smtOutBean.setRetMsg(qryCondId + "待结算手工处理失败");
							logger.info(qryCondId + "待结算手工处理失败");
						}
					}
					
				}else if("CSBS.009.001.01".equalsIgnoreCase(type)){
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if(spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for(Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBufferT.toString()+"</Document>").getBytes("UTF-8"))));  
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
						}
					}
					if(null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";
						String XcptnRtrTx="";
						if(systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
						}
						smtOutBean.setRetStatus(RetStatusEnum.F);
						smtOutBean.setRetCode(XcptnRtrCd);
						smtOutBean.setRetMsg(XcptnRtrTx);
						logger.info("消息错误码["+XcptnRtrCd+"]\r\n错误信息[" + XcptnRtrTx+"]");
					}	
				}
			}	
		}
		return smtOutBean;
	}
	
	public BtchQryRslt getDetailForIS03Settls(BtchQryRslt btchQryRslt){
		String txFlowId = new SimpleDateFormat("yyyyMMdd").format(new Date()) + CdtcBankAccount.BANK_ACCOUNT + DateUtil.convertDateToYYYYMMDDHHMMSS(new Date());
		String qryCondId = btchQryRslt.getInstrId();
		logger.info("------------ioper:"+btchQryRslt.getIOper());
		try{
			SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS04AndBJ0300(txFlowId, CdtcBankAccount.BANK_ACCOUNT, qryCondId);
			StringBuffer xmlBuffer = new StringBuffer();
			settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);
			DClient dClient = new DClient(wasIp);
			String backXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
			//HashMap<String, String> resultHashMap = dClientDao.sendXmlMessageToCdtcForRequest(XmlFormat.format(xmlBuffer.toString()));
			if(null != backXml) {
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));  
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
				if(null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {
					String instrSts = xmlDoc.getRootElement().element("Document").element("StsFlg").element("InstrStsInf").element("InstrSts").getText();
					if(null == instrSts || !"IS03".equalsIgnoreCase(instrSts)) {return null;}
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					CommonDistributionSettlementInstruction document = new CommonDistributionSettlementInstruction("Document");
					Field<?> instrOrgn = null;
					Field<?> givAcctId = null;
					if(spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groups = spotSettlementInstruction.getGroups("Document");
						for(Group group : groups) {
							Field<?> fedealno = null;Field<?> fedealno2 = null;
							
							if(group.hasGroup("SttlmDtl")) {
								Group group2 = group.getGroups("SttlmDtl").get(0);
								fedealno = group2.getfieldsForFieldMap().get("TxId");
								instrOrgn=group2.getfieldsForFieldMap().get("InstrOrgn");
								group2.removeGroup("Oprtr");
								group2.removeGroup("Chckr");
								document.addGroup(group2);
								
								givAcctId = group2.hasGroup("GivAcct") && group2.getGroups("GivAcct").get(0).getfieldsForFieldMap().containsKey("Id")?group2.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id"):null;
							}
							if(group.hasGroup("Msg")) {
								TransactionMsg transactionMsg = new TransactionMsg("Msg");
								transactionMsg.set(new ISODateTime("CreDtTm",new Date()));
								fedealno2=group.getGroups("Msg").get(0).getfieldsForFieldMap().get("TxFlowId");
								Priority4Code priority4Code = new Priority4Code("Prty");
								priority4Code.setValue(Priority4Code.Type.PT02.getType());
								transactionMsg.set(priority4Code);
								Max35Text max35Text = new Max35Text("TxFlowId");
								max35Text.setValue(fedealno==null?fedealno2.getObject().toString():fedealno.getObject().toString());
								transactionMsg.set(max35Text);
								document.addGroup(transactionMsg);
							}
						}
						TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
						
						RequestTypeCode requestTypeCode = new RequestTypeCode("ReqTp");
						String bizTp = xmlDoc.getRootElement().element("Document").element("SttlmDtl").element("BizTp").getText();
						/**
						 *  BT00：普通分销
							BT01：现券
							BT02：质押式回购
							BT03：买断式回购
							BT04：债券远期
							BT05：债券借贷
							BJ0100   普通分销
							BJ0101   现券
							BJ0102   质押式回购
							BJ0103	买断式回购
							BJ0104	债券远期
							BJ0105	债券借贷
							BJ0106	质押券置换
						*/
						if(bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT00.getType())) {
							requestTypeCode.setValue(RequestTypeCode.Type.BJ0100.getType());}
						if(bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT01.getType())) {
							requestTypeCode.setValue(RequestTypeCode.Type.BJ0101.getType());}
						if(bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT02.getType())) {
							requestTypeCode.setValue(RequestTypeCode.Type.BJ0102.getType());}
						if(bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT03.getType())) {
							requestTypeCode.setValue(RequestTypeCode.Type.BJ0103.getType());}
						if(bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT04.getType())) {
							requestTypeCode.setValue(RequestTypeCode.Type.BJ0104.getType());}
						OperatorStatusCode operatorStatusCode = new OperatorStatusCode("OprtrSts");
						/**
						 * 03是第三方交易 那么确认分 收券和付券
						 */
						if(instrOrgn.getObject().toString().equalsIgnoreCase(InstructionOriginCode.Type.IO03.getType()) && null != givAcctId) {
							if(givAcctId.getObject().toString().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)) {
								operatorStatusCode.setValue(OperatorStatusCode.Type.OS05.getType());}
							else {
								operatorStatusCode.setValue(OperatorStatusCode.Type.OS06.getType());}
						}
						/**
						 * 成员客户端的只需要付券方确认即可
						 */
						if(instrOrgn.getObject().toString().equalsIgnoreCase(InstructionOriginCode.Type.IO01.getType()) && null != givAcctId) {
							if(!givAcctId.getObject().toString().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)) {
								operatorStatusCode.setValue(OperatorStatusCode.Type.OS01.getType());}
							else {
								return null;}
						}
						transactionOprtrFlg.set(operatorStatusCode);
						transactionOprtrFlg.set(requestTypeCode);
						document.addGroup(transactionOprtrFlg);
					}
					StringBuffer csbs001 = new StringBuffer();
					document.toXmlForSuperParent(csbs001);
					btchQryRslt.setCSBS001(XmlFormat.format(csbs001.toString()));
					btchQryRslt.setCSBS003("");
					btchQryRslt.setITime(new Date());
					btchQryRslt.setStdt(btchQryRslt.getDt1());
					
					
					
				}else if("CSBS.009.001.01".equalsIgnoreCase(type)){
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if(spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for(Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBufferT.toString()+"</Document>").getBytes("UTF-8"))));  
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
						}
					}
					if(null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";String XcptnRtrTx="";
						if(systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
						}
						System.out.println(XcptnRtrCd+XcptnRtrTx);
					}
				}
				
				
				
			}
			
			
			
			
		}catch (Exception e) {
			logger.error(ExceptionUtil.getErrorInfoFromException(e));
		}
		
		
		return btchQryRslt;
	}
	


}
