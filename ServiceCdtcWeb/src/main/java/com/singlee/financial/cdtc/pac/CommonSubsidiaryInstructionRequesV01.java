/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;

/**
 * @author Fang
 *6.3.3	通用辅助指令请求
 */
public class CommonSubsidiaryInstructionRequesV01 extends Group {

	private static final long serialVersionUID = 1L;
	private static String type = "CSBS.035.001.01";
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("Msg", TransactionMsg.class);
		keyClassMap.put("OprtrFlg", TransactionOprtrFlg.class);
		keyClassMap.put("CmonSbsdryItm", TransactionCmonSbsdryItm.class);
		keyClassMap.put("GivInf", TransactionGivInf.class);
		keyClassMap.put("TakInf", TransactionTakInf.class);
		keyClassMap.put("Xtnsn", Extension.class);
		keyClassMap.putAll(TransactionMsg.keyClassMap);
		keyClassMap.putAll(TransactionOprtrFlg.keyClassMap);
		keyClassMap.putAll(TransactionCmonSbsdryItm.keyClassMap);
		keyClassMap.putAll(TransactionGivInf.keyClassMap);
		keyClassMap.putAll(TransactionTakInf.keyClassMap);
		keyClassMap.putAll(Extension.keyClassMap);
	}

	public CommonSubsidiaryInstructionRequesV01(String tag) {

		super(tag, tag, new String[] { "Msg", "OprtrFlg","CmonSbsdryItm","GivInf","TakInf","Xtnsn"});
	}
	public CommonSubsidiaryInstructionRequesV01(String tag,String delim) {

		super(tag, tag, new String[] { "Msg", "OprtrFlg","CmonSbsdryItm","GivInf","TakInf","Xtnsn"});
	}
	
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}

	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	
	public void set(TransactionMsg group) {
		addGroup(group);
	}
	public void set(TransactionOprtrFlg group){
		addGroup(group);
	}
	public void set(TransactionCmonSbsdryItm group){
		addGroup(group);
	}
	public void set(TransactionGivInf group){
		addGroup(group);
	}
	public void set(TransactionTakInf group){
		addGroup(group);
	}
	public void set(Extension group){
		addGroup(group);
	}
	
	public static CommonSubsidiaryInstructionRequesV01 content(Bean1 bean1){
		CommonSubsidiaryInstructionRequesV01 common = new CommonSubsidiaryInstructionRequesV01("Document");
		/**********************************报文标识*********************************/
		TransactionMsg transactionMsg = new TransactionMsg("Msg");
		//交易流水标识
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("01");
		transactionMsg.set(max35Text);
		//经办人
		PartyIdentification partyIdentification = new PartyIdentification("Oprtr");
		Max35Text max35TextOprtr = new Max35Text("Nm");
		max35TextOprtr.setValue("A");
		partyIdentification.set(max35TextOprtr);
		transactionMsg.set(partyIdentification);
		//复核人
		PartyIdentification partyIdentificationB = new PartyIdentification("Chckr");
		Max35Text max35TextB = new Max35Text("Nm");
		max35TextB.setValue("B");
		partyIdentificationB.set(max35TextB);
		transactionMsg.set(partyIdentificationB);
		//优先级
		Priority4Code prty = new Priority4Code("Prty");
		prty.setValue(Priority4Code.Type.PT01.getType());
		transactionMsg.set(prty);
		//报文创建时间
		transactionMsg.set(new ISODateTime("CreDtTm", new Date()));
		/*********************************操作标识**************************/
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		//请求类别码
		RequestTypeCode reqTp = new RequestTypeCode("ReqTp");
		reqTp.setValue(RequestTypeCode.Type.BJ0100.getType());
		transactionOprtrFlg.set(reqTp);
		//操作码
		OperatorStatusCode operSc = new OperatorStatusCode("OprtrSts");
		operSc.setValue(OperatorStatusCode.Type.OS00.getType());
		transactionOprtrFlg.set(operSc);
	
		TransactionCmonSbsdryItm cmonSbsdryItm = new TransactionCmonSbsdryItm("CmonSbsdryItm");
		//结算指令来源
		InstructionOriginCode ioc = new InstructionOriginCode("InstrOrgn");
		ioc.setValue(InstructionOriginCode.Type.IO01.getType());
		cmonSbsdryItm.set(ioc);
		//辅助类型
		BusinessTypeCode btc = new BusinessTypeCode("BizTp");
		btc.setValue(BusinessTypeCode.Type.BT00.getType());
		cmonSbsdryItm.set(btc);
		//结算指令标识
		Exact12NumericText ent = new Exact12NumericText("InstrId");
		ent.setValue(123);
		cmonSbsdryItm.set(ent);
		//结算合同标识
		Exact9NumericText ctrctId = new Exact9NumericText("CtrctId");
		ctrctId.setValue(12345789);
		cmonSbsdryItm.set(ent);
		//付券方账户
		AccountIdentificationAndName4 aian = new AccountIdentificationAndName4("GivAcct");
		Max35Text maxAian = new Max35Text("Nm");
		maxAian.setValue("62285500");
		Exact11Text e11Text = new Exact11Text("Id");
		e11Text.setValue("12345678901");
		aian.set(maxAian);
		aian.set(e11Text);
		cmonSbsdryItm.set(aian);
		//收券方账户
		AccountIdentificationAndName4 aian1 = new AccountIdentificationAndName4("TakAcct");
		Max35Text maxAian1 = new Max35Text("Nm");
		maxAian1.setValue("62280055");
		Exact11Text e11Text1 = new Exact11Text("Id");
		e11Text1.setValue("10987654321");
		aian1.set(maxAian1);
		aian1.set(e11Text1);
		cmonSbsdryItm.set(aian1);
		//原结算指令标识
		Exact12NumericText orgnlInstrId = new Exact12NumericText("OrgnlInstrId");
		orgnlInstrId.setValue(0000000000000);
		cmonSbsdryItm.set(orgnlInstrId);
		//原结算合同标识
		Exact9NumericText orgnlCtrctId = new Exact9NumericText("OrgnlCtrctId");
		orgnlCtrctId.setValue(1234567809);
		cmonSbsdryItm.set(orgnlCtrctId);
		//执行日期
		cmonSbsdryItm.set(new ISODate("DlvryDt", new Date()));
		//现金了结交割
		CashSettlementDelivery cshSttlmDlvry = new CashSettlementDelivery("CshSttlmDlvry");
		CommonCurrencyAndAmount cshSttlmAmt = new CommonCurrencyAndAmount("CshSttlmAmt","CNY");
		cshSttlmAmt.setValue("1234567890.99");
		cshSttlmDlvry.set(cshSttlmAmt);
		CashSettlementDirectionCode cshSttlmDrctn = new CashSettlementDirectionCode("CshSttlmDrctn");
		cshSttlmDrctn.setValue(CashSettlementDirectionCode.Type.SD00.getType());
		cshSttlmDlvry.set(cshSttlmDrctn);
		cmonSbsdryItm.set(cshSttlmDlvry);
		//经办人
		PartyIdentification pifOprtr = new PartyIdentification("Oprtr");
		Max35Text max35TextpifOprtr = new Max35Text("Nm");
		max35TextpifOprtr.setValue("C");
		pifOprtr.set(max35TextpifOprtr);
		cmonSbsdryItm.set(pifOprtr);
		//复核人
		PartyIdentification pifOChckr = new PartyIdentification("Chckr");
		Max35Text max35TextpifChckr = new Max35Text("Nm");
		max35TextpifChckr.setValue("D");
		pifOChckr.set(max35TextpifChckr);
		cmonSbsdryItm.set(pifOChckr);
		//确认人
		PartyIdentification pifCnfrmr = new PartyIdentification("Cnfrmr");
		Max35Text max35TextpifCnfrmr = new Max35Text("Nm");
		max35TextpifCnfrmr.setValue("E");
		pifCnfrmr.set(max35TextpifCnfrmr);
		cmonSbsdryItm.set(pifCnfrmr);
		
		/*********************************付方信息**********************/
		TransactionGivInf transactionGivInf = new TransactionGivInf("GivInf");
		//名称
		Max70Text max70TextGviInf = new Max70Text("Nm");
		max70TextGviInf.setValue("ABC");
		transactionGivInf.set(max70TextGviInf);
		//自定义标识
		GenericIdentification4 genid = new GenericIdentification4("PrtryId");
		Max35Text maxid = new Max35Text("Id");
		maxid.setValue("SS123");
		genid.set(maxid);
		Max35Text maxIdIp = new Max35Text("IdTp");
		maxIdIp.setValue("DD123");
		genid.set(maxIdIp);
		transactionGivInf.set(genid);
		//代理机构
		AgentParty agentParty = new AgentParty("AgtPty");
		AgentPartyTypeCode agtPtyTP = new AgentPartyTypeCode("AgtPtyTP");
		agtPtyTP.setValue(AgentPartyTypeCode.Type.AP00.getType());
		agentParty.set(agtPtyTP);
		PartyIdentification30 agtPtyId = new PartyIdentification30("AgtPtyId");
		Max70Text agtPtyIdNm = new Max70Text("Nm");
		agtPtyIdNm.setValue("F");
		agtPtyId.set(agtPtyIdNm);
		GenericIdentification4 prtryId = new GenericIdentification4("PrtryId");
		Max35Text prtryIdId = new Max35Text("Id");
		prtryIdId.setValue("SFH000");
		Max35Text paityIdTp = new Max35Text("IdTp");
		paityIdTp.setValue("IdTp000");
		prtryId.set(prtryIdId);
		prtryId.set(paityIdTp);
		agtPtyId.set(prtryId);
		agentParty.set(agtPtyId);
		transactionGivInf.set(agentParty);
		/***************************收方信息***********************/
		TransactionTakInf transactionTakInf = new TransactionTakInf("TakInf");
		//名称
		Max70Text max70TextTakInf = new Max70Text("Nm");
		max70TextTakInf.setValue("EFG");
		transactionTakInf.set(max70TextTakInf);
		//自定义标识
		GenericIdentification4 genidTakInf = new GenericIdentification4("PrtryId");
		Max35Text maxidTakInf = new Max35Text("Id");
		maxidTakInf.setValue("SS123");
		genidTakInf.set(maxidTakInf);
		Max35Text maxIdIpTakInf = new Max35Text("IdTp");
		maxIdIpTakInf.setValue("DD123");
		genidTakInf.set(maxIdIpTakInf);
		transactionTakInf.set(genidTakInf);
		//代理机构
		AgentParty agentPartyTakInf = new AgentParty("AgtPty");
		AgentPartyTypeCode agtPtyTPTakInf = new AgentPartyTypeCode("AgtPtyTP");
		agtPtyTPTakInf.setValue(AgentPartyTypeCode.Type.AP01.getType());
		agentPartyTakInf.set(agtPtyTPTakInf);
		PartyIdentification30 agtPtyIdTakInf = new PartyIdentification30("AgtPtyId");
		Max70Text agtPtyIdNmTakInf = new Max70Text("Nm");
		agtPtyIdNmTakInf.setValue("H");
		agtPtyIdTakInf.set(agtPtyIdNmTakInf);
		GenericIdentification4 prtryIdTakInf = new GenericIdentification4("PrtryId");
		Max35Text prtryIdIdTakInf = new Max35Text("Id");
		prtryIdIdTakInf.setValue("SFH111");
		Max35Text paityIdTpTakInf = new Max35Text("IdTp");
		paityIdTpTakInf.setValue("IdTp111");
		prtryIdTakInf.set(prtryIdIdTakInf);
		prtryIdTakInf.set(paityIdTpTakInf);
		agtPtyIdTakInf.set(prtryIdTakInf);
		agentPartyTakInf.set(agtPtyIdTakInf);
		transactionTakInf.set(agentPartyTakInf);
		/********************************拓展项*****************************/
		Extension extension = new Extension("Xtnsn");
		Max2048Text max2048Text = new Max2048Text("XtnsnTxt");
		max2048Text.setValue("通用辅助指令请求");
		extension.set(max2048Text);
		
		common.set(extension);
		common.set(transactionTakInf);
		common.set(cmonSbsdryItm);
		common.set(transactionGivInf);
		common.set(transactionOprtrFlg);
		common.set(transactionMsg);
		return common;
	}
	
	public static void main(String[] args) throws Exception {
		
		StringBuffer stringBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(stringBuffer);

		System.out.println(XmlFormat.format(XmlFormat.format(stringBuffer.toString())));

		
		// 保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat
				.format(stringBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out = new FileOutputStream("./cnbondxml/" + CommonSubsidiaryInstructionRequesV01.type
				+ ".xml");
		XMLOutputter outputter = new XMLOutputter();
		// 如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的
		outputter.output((Document) doc, out);
		out.close();

		SAXReader reader = new SAXReader();
		/**
		 * 读取请求报文XML 反解析报文
		 */
		CommonSubsidiaryInstructionRequesV01 heartBeatMessageV02 = new CommonSubsidiaryInstructionRequesV01("Msg");
		org.dom4j.Document xmlDoc = reader.read(new File("./cnbondxml/"
				+ CommonSubsidiaryInstructionRequesV01.type + ".xml"));
		getElementList(xmlDoc.getRootElement(), heartBeatMessageV02);
		
		StringBuffer xmlBuffer = new StringBuffer();
		heartBeatMessageV02.toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
	}
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * 
	 * @param element
	 * @param common
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	public static void getElementList(Element element,
			CommonSubsidiaryInstructionRequesV01 common) throws Exception,
			NoSuchMethodException {
		List<?> elements = element.elements();
		for (Iterator<?> iterator = elements.iterator(); iterator.hasNext();) {
			Element element2 = (Element)iterator.next();
			List<?> elements2 = element2.elements();
			if (elements2.size() >= 0) {
				System.out.println(""
						+ String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName())) + ""
						+ StringUtils.trimToEmpty(element2.getText()));
				if (common
						.isHasTag(StringUtils.trimToEmpty(element2.getName()))) {
					if (element2.elements().size() > 0) {
						System.out.println(String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName()))
								+ "IS GROUP");
						common.addGroup(getGroups(element2, common));
					} else {
						System.out.println(String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName()))
								+ "IS FIELD");
						Class<?>[] parameterTypes = { String.class, Object.class };
						Constructor<?> constructor = CommonSubsidiaryInstructionRequesV01.keyClassMap
								.get(
										StringUtils.trimToEmpty(element2
												.getName())).getConstructor(
										parameterTypes);
						Object[] parameters = {
								StringUtils.trimToEmpty(element2.getName()),
								StringUtils.trimToEmpty(element.getText()) };
						Object object = constructor.newInstance(parameters);
						common.setField(StringUtils.trimToEmpty(element2
								.getName()), (Field<?>) object);
					}
				}
			}
		}
	}

	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception反解析报文
	 */
	@SuppressWarnings("unchecked")
	public static Group getGroups(Element element,
			CommonSubsidiaryInstructionRequesV01 common)
			throws NoSuchMethodException, Exception {
		//Group group = new Group(StringUtils.trimToEmpty(element.getName()), "");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = CommonSubsidiaryInstructionRequesV01.keyClassMap.get(StringUtils.trimToEmpty(element.getName())).getConstructor(parameterTypes2);
		Object[] parameters2 = {StringUtils.trimToEmpty(element.getName()),""};
		Group group = (Group) constructor2.newInstance(parameters2);
		System.out.println("************************************");
		List<Element> elements = element.elements();
		for (Iterator<Element> iterator = elements.iterator(); iterator
				.hasNext();) {
			Element element2 = (Element) iterator.next();
			System.out.println(""
					+ String.format("%20s :", StringUtils.trimToEmpty(element2
							.getName())) + ""
					+ StringUtils.trimToEmpty(element2.getText()));
			if (element2.elements().size() > 0) {
				System.out.println(String.format("%20s :", StringUtils
						.trimToEmpty(element2.getName()))
						+ "IS GROUP");
				group.addGroup(getGroups(element2, common));
			} else {
				System.out.println(String.format("%20s :", StringUtils
						.trimToEmpty(element2.getName()))
						+ "IS FIELD");
				Class<?>[] parameterTypes = { String.class, Object.class };
				Constructor<?> constructor = CommonSubsidiaryInstructionRequesV01.keyClassMap
						.get(StringUtils.trimToEmpty(element2.getName()))
						.getConstructor(parameterTypes);
				Object[] parameters = {
						StringUtils.trimToEmpty(element2.getName()),
						StringUtils.trimToEmpty(element2.getText()) };
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>) object);
			}
		}
		System.out.println("************************************");
		return group;
	}

	
}
