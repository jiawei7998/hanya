package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.Max70Text;
import com.singlee.financial.cdtc.pac.MessageTypeCode;

public class BizInfNtce extends Group {

	/**
	 * 业务消息通知
	 */
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("InfId",  	Field.class);
		keyClassMap.put("InfTp",	Field.class);
		keyClassMap.put("Key",     	Field.class);
		keyClassMap.put("Abstract", Field.class);
	}
	
	public BizInfNtce(String tag){
		/**
		 * 消息标识号   InfId
		 * 消息种类     InfTp
		 *  关键字	  Key
		 *  摘要     Abstract
		 */
		super(tag,tag,new String[]{"InfId","InfTp","Key","Abstract"});
	}
	
	public BizInfNtce(String tag,String delim){
		/**
		 * 消息标识号   InfId
		 * 消息种类     InfTp
		 *  关键字	  Key
		 *  摘要     Abstract
		 */
		super(tag,tag,new String[]{"InfId","InfTp","Key","Abstract"});
	}
	
	public void set(Max35Text value){
		setField(value);
	}
	
	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(Max35Text field) {
		return isSetField(field);
	}

	public boolean isSetMax35Text() {
		return isSetField("InfId");
	}
	
	
	public void set(MessageTypeCode value){
		setField(value);
	}
	
	public MessageTypeCode get(MessageTypeCode value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(MessageTypeCode field) {
		return isSetField(field);
	}

	public boolean isSetMessageTypeCode() {
		return isSetField("InfTp");
	}
	
	
	public void set(Max70Text value){
		setField(value);
	}
	
	public Max70Text get(Max70Text value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(Max70Text field) {
		return isSetField(field);
	}

	public boolean isSetMax70Text() {
		return isSetField("Abstract");
	}

}
