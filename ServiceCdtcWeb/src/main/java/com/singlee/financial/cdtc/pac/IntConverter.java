package com.singlee.financial.cdtc.pac;

public final class IntConverter
{
  public static String convert(int i)
  {
    return Long.toString(i);
  }

  public static int convert(String value)
    throws FieldConvertError
  {
    try
    {
      return Integer.parseInt(value); } catch (NumberFormatException e) {
    }
    throw new FieldConvertError("invalid integral value: " + value);
  }
}