package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.Max8Text;

public class HrtBtMsg extends Group {
	
	private static final long serialVersionUID = 1L;
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("HrtBtId",      Field.class);
		keyClassMap.put("HrtBtRefId", 	Field.class);
	}
	public HrtBtMsg(String tag){
		
		super(tag,tag, new String[] { "HrtBtId", "HrtBtRefId"});
	}
	public HrtBtMsg(String tag,String delim){
		
		super(tag,tag, new String[] { "HrtBtId", "HrtBtRefId"});
	}
	public void set(Max8Text value) {
        setField(value);
      }

      public Max8Text get(Max8Text value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public Max8Text getMax8Text() throws FieldNotFound
      {
    	  Max8Text value = new Max8Text("HrtBtId");
          getField(value);

          return value;
      }
      public boolean isSet(Max8Text field) {
        return isSetField(field);
      }
      public boolean isSetMax8Text() {
        return isSetField("HrtBtId");
      }
      /*********************************************************************/
}
