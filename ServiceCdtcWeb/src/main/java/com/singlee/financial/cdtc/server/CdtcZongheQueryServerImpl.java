package com.singlee.financial.cdtc.server;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.capital.common.pojo.Pair;
import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.bean.CdtcZLSecurity;
import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.financial.cdtc.CdtcZongheQueryServer;
import com.singlee.financial.cdtc.dao.CnbondManagerDao;
import com.singlee.financial.cdtc.pac.BondSettlementType2Code;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.InstructionStatusCode;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.util.ExceptionUtil;
import com.singlee.financial.cdtc.util.FormatUtil;
import com.singlee.financial.cdtc.util.XmlFormat;
import com.singlee.financial.pojo.component.RetStatusEnum;

/***
 * 
 * 中债直连-结算综合查询-详情查询
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcZongheQueryServerImpl implements CdtcZongheQueryServer {
	@Autowired
	private CnbondManagerDao cnbondManagerDao;
	private static Logger logger = LoggerFactory.getLogger(CdtcZongheQueryServerImpl.class);
	/***
	 * 综合详情查询
	 */
	@Override
	public Pair<SmtOutBean, ManualSettleDetail> getZongheDetailQuery(Map<String, Object> map) throws RemoteConnectFailureException,Exception {
		logger.info("已进入summitWeb中getZongheDetailQuery方法");
		Pair<SmtOutBean, ManualSettleDetail> pair = new Pair<SmtOutBean, ManualSettleDetail>();
		SmtOutBean smtOutBean = new SmtOutBean();
		if(map.get("xmlmatchid")==null){
			smtOutBean.setRetStatus(RetStatusEnum.F);
			smtOutBean.setRetMsg("xmlmatchid为空");
			pair.setLeft(smtOutBean);
			logger.info("xmlmatchid为空");
			return pair;
		}
		String detailType = map.get("type").toString();
		logger.info("detailType为："+detailType);
		if("detail".equals(detailType)){
			pair = zongheLoadMsg(map);
		}else if("mdRepo".equals(detailType)){//买断式回购
			pair = mdZongheLoadMsg(map);
		}else{//质押式回购
			pair = zyZongheLoadMsg(map);
		}
		return pair;
	}
	
	public Pair<SmtOutBean, ManualSettleDetail> zongheLoadMsg(Map<String, Object> map){
		logger.info("已进入zongheLoadMsg方法");
		Pair<SmtOutBean, ManualSettleDetail> pair = new Pair<SmtOutBean, ManualSettleDetail>();
		SmtOutBean smtOutBean = new SmtOutBean();
		ManualSettleDetail ManualSettleDetail = new ManualSettleDetail();
		
		String jsbwpath = String.valueOf(map.get("xmlmatchid"));
		logger.info("xmlmatchid:"+jsbwpath);
		if(null != jsbwpath && !"".equalsIgnoreCase(jsbwpath)) {
			try{
				AbstractSlIntfcCnbondPkgId abstractSlIntfcCnbondPkgId  =this.cnbondManagerDao.getSlintfccnbondpkgByMsgtype(jsbwpath.trim());
				if(abstractSlIntfcCnbondPkgId==null){
					smtOutBean.setRetStatus(RetStatusEnum.F);
					smtOutBean.setRetMsg("abstractSlIntfcCnbondPkgId为空");
					pair.setLeft(smtOutBean);
					logger.info("abstractSlIntfcCnbondPkgId为空");
					return pair;
				}
				SAXReader reader = new SAXReader();
				/**
				 * 读取请求报文XML 反解析报文
				 */
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(abstractSlIntfcCnbondPkgId.getMsgxml()).getBytes("UTF-8"))));  
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
				spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				if(spotSettlementInstruction.hasGroup("Document")) {
					logger.info("hasGroup(Document)");
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for(Group group : groupsList) {
						// 保存xml文件
						StringBuffer xmlBuffer = new StringBuffer();
						group.toXmlForParent(xmlBuffer);
						logger.info("xmlBuffer:"+xmlBuffer.toString());
						commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
						CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
					}
				}
				if(null != commonDistributionSettlementStatusReport) {
					if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
						Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
						String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
						ManualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
					}
					if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
						Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
						ManualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
						ManualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
						ManualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						ManualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						ManualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						ManualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						ManualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
						ManualSettleDetail.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
						ManualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2")?FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())):"");
						ManualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
						ManualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId")?sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString():"");
						List<CdtcZLSecurity> secList = new ArrayList<CdtcZLSecurity>();
						
						if(sttlmDtl.hasGroup("Bd1")) {
							Group bd1 = sttlmDtl.getGroups("Bd1").get(0);
							CdtcZLSecurity cdtcZLSecurity  =new CdtcZLSecurity();
							cdtcZLSecurity.setSecid(bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
							logger.info("Secid:"+bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
							cdtcZLSecurity.setSecidNm(bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
							logger.info("SecidNm:"+bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
							cdtcZLSecurity.setFaceAmt(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
							logger.info("FaceAmt:"+new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
							ManualSettleDetail.setBdAmt(FormatUtil.formatBigDecimal(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString())));
							cdtcZLSecurity.setAccrualAmt(new BigDecimal(bd1.getGroups("AcrdIntrstInf").get(0).getfieldsForFieldMap().get("AcrdIntrst").getObject().toString()));
							logger.info("AccrualAmt:"+new BigDecimal(bd1.getGroups("AcrdIntrstInf").get(0).getfieldsForFieldMap().get("AcrdIntrst").getObject().toString()));
							cdtcZLSecurity.setAccrualTotalAmt(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val3").getObject().toString()));
							logger.info("AccrualTotalAmt:"+new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val3").getObject().toString()));
							ManualSettleDetail.setPric1(bd1.getfieldsForFieldMap().get("Pric1").getObject().toString());
							ManualSettleDetail.setPric2(bd1.getfieldsForFieldMap().containsKey("Pric2")?bd1.getfieldsForFieldMap().get("Pric2").getObject().toString():"");
							secList.add(cdtcZLSecurity);
							ManualSettleDetail.setSecList(secList);
						}
						logger.info("secList size :"+secList.size());
						ManualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
						ManualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
					}
				}
			}catch (Exception e) {
				logger.error("CDTC综合结算详情查询异常，异常信息："+e.getMessage());
				logger.error(ExceptionUtil.getErrorInfoFromException(e));
				smtOutBean.setRetStatus(RetStatusEnum.F);
				smtOutBean.setRetMsg("CDTC综合结算详情查询异常，异常信息："+e.getMessage());
				pair.setLeft(smtOutBean);
				return pair;
			}
			
		}
		
		smtOutBean.setRetStatus(RetStatusEnum.S);
		smtOutBean.setRetMsg("CDTC综合结算详情查询成功");
		pair.setLeft(smtOutBean);
		pair.setRight(ManualSettleDetail);
		return  pair;
	}
	/***
	 * 买断式回购
	 * @return
	 */
	public Pair<SmtOutBean, ManualSettleDetail> mdZongheLoadMsg(Map<String, Object> map){
		logger.info("已进入mdZongheLoadMsg方法");
		Pair<SmtOutBean, ManualSettleDetail> pair = new Pair<SmtOutBean, ManualSettleDetail>();
		SmtOutBean smtOutBean = new SmtOutBean();
		ManualSettleDetail ManualSettleDetail = new ManualSettleDetail();
		
		String jsbwpath = String.valueOf(map.get("xmlmatchid"));
		logger.info("xmlmatchid:"+jsbwpath);
		
		if(null != jsbwpath && !"".equalsIgnoreCase(jsbwpath)) {
			try{
				AbstractSlIntfcCnbondPkgId abstractSlIntfcCnbondPkgId  =this.cnbondManagerDao.getSlintfccnbondpkgByMsgtype(jsbwpath.trim());
				if(abstractSlIntfcCnbondPkgId==null){
					smtOutBean.setRetStatus(RetStatusEnum.F);
					smtOutBean.setRetMsg("abstractSlIntfcCnbondPkgId为空");
					pair.setLeft(smtOutBean);
					logger.info("abstractSlIntfcCnbondPkgId为空");
					return pair;
				}
				SAXReader reader = new SAXReader();
				/**
				 * 读取请求报文XML 反解析报文
				 */
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(abstractSlIntfcCnbondPkgId.getMsgxml()).getBytes("UTF-8"))));  
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
				spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				if(spotSettlementInstruction.hasGroup("Document")) {
					logger.info("hasGroup(Document)");
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for(Group group : groupsList) {
						// 保存xml文件
						StringBuffer xmlBuffer = new StringBuffer();
						group.toXmlForParent(xmlBuffer);
						logger.info("xmlBuffer:"+xmlBuffer.toString());
						commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
						CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
					}
				}
				if(null != commonDistributionSettlementStatusReport) {
					if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
						Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
						String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
						ManualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
					}
					if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
						Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
						if("BT03".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){//买断式
							ManualSettleDetail.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
							ManualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
							ManualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
							ManualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
							ManualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
							ManualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
							ManualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
							ManualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
							ManualSettleDetail.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
							ManualSettleDetail.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
							ManualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2")?FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())):"");
							ManualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
							ManualSettleDetail.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
							ManualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId")?sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString():"");
							List<CdtcZLSecurity> secList = new ArrayList<CdtcZLSecurity>();
							if(sttlmDtl.hasGroup("Bd1")) {
								List<Group> groupsList = sttlmDtl.getGroups("Bd1");
								for(Group bd1 : groupsList) {
									CdtcZLSecurity cdtcZLSecurity  =new CdtcZLSecurity();
									cdtcZLSecurity.setSecid(bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
									cdtcZLSecurity.setSecidNm(bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
									cdtcZLSecurity.setFaceAmt(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
									secList.add(cdtcZLSecurity);
									logger.info("Secid:"+bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
									logger.info("SecidNm:"+bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
									logger.info("FaceAmt:"+new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
									
								}
								ManualSettleDetail.setSecList(secList);
							}
							logger.info("secList size :"+secList.size());
							if(sttlmDtl.hasGroup("SttlmGrte1")) {
								/**
								 * 按实例进行解析
								 */
							}else {
								ManualSettleDetail.setGrteTp("无");
							}
							ManualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
							ManualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
						}
					}
				}
			}catch (Exception e) {
				logger.error("CDTC综合结算[买断式回购]详情查询异常，异常信息："+e.getMessage());
				logger.error(ExceptionUtil.getErrorInfoFromException(e));
				smtOutBean.setRetStatus(RetStatusEnum.F);
				smtOutBean.setRetMsg("CDTC综合结算详情查询异常，异常信息："+e.getMessage());
				pair.setLeft(smtOutBean);
				return pair;
			}
			
		}
			
		smtOutBean.setRetStatus(RetStatusEnum.S);
		smtOutBean.setRetMsg("CDTC综合结算详情查询成功");
		pair.setLeft(smtOutBean);
		pair.setRight(ManualSettleDetail);
		return  pair;
	}
	/***
	 * 质押式回购
	 * @return
	 */
	public Pair<SmtOutBean, ManualSettleDetail> zyZongheLoadMsg(Map<String, Object> map){
		logger.info("已进入zyZongheLoadMsg方法");
		Pair<SmtOutBean, ManualSettleDetail> pair = new Pair<SmtOutBean, ManualSettleDetail>();
		SmtOutBean smtOutBean = new SmtOutBean();
		ManualSettleDetail ManualSettleDetail = new ManualSettleDetail();
		
		String jsbwpath = String.valueOf(map.get("xmlmatchid"));
		logger.info("xmlmatchid:"+jsbwpath);
		if(null != jsbwpath && !"".equalsIgnoreCase(jsbwpath)) {
			try{
				AbstractSlIntfcCnbondPkgId abstractSlIntfcCnbondPkgId  =this.cnbondManagerDao.getSlintfccnbondpkgByMsgtype(jsbwpath.trim());
				if(abstractSlIntfcCnbondPkgId==null){
					smtOutBean.setRetStatus(RetStatusEnum.F);
					smtOutBean.setRetMsg("abstractSlIntfcCnbondPkgId为空");
					pair.setLeft(smtOutBean);
					logger.info("abstractSlIntfcCnbondPkgId为空");
					return pair;
				}
				SAXReader reader = new SAXReader();
				/**
				 * 读取请求报文XML 反解析报文
				 */
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(abstractSlIntfcCnbondPkgId.getMsgxml()).getBytes("UTF-8"))));  
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
				spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				if(spotSettlementInstruction.hasGroup("Document")) {
					logger.info("hasGroup(Document)");
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for(Group group : groupsList) {
						// 保存xml文件
						StringBuffer xmlBuffer = new StringBuffer();
						group.toXmlForParent(xmlBuffer);
						logger.info("xmlBuffer:"+xmlBuffer.toString());
						commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
						CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
					}
				}
				if(null != commonDistributionSettlementStatusReport) {
					if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
						Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
						String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
						ManualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
					}
					if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
						Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
						if("BT02".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){//质押式
							ManualSettleDetail.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
							ManualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
							ManualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
							ManualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
							ManualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
							ManualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
							ManualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
							ManualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
							ManualSettleDetail.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
							ManualSettleDetail.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
							ManualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2")?FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())):"");
							ManualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
							ManualSettleDetail.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
							ManualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId")?sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString():"");
							ManualSettleDetail.setAggtFaceAmt(FormatUtil.formatBigDecimal4(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("AggtFaceAmt").getObject().toString())));
							ManualSettleDetail.setRate(sttlmDtl.getfieldsForFieldMap().get("Rate").getObject().toString());
							
							List<CdtcZLSecurity> secList = new ArrayList<CdtcZLSecurity>();
							if(sttlmDtl.hasGroup("Bd1")) {
								List<Group> groupsList = sttlmDtl.getGroups("Bd1");
								for(Group bd1 : groupsList) {
									CdtcZLSecurity cdtcZLSecurity  =new CdtcZLSecurity();
									cdtcZLSecurity.setSecid(bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
									cdtcZLSecurity.setSecidNm(bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
									cdtcZLSecurity.setFaceAmt(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
									secList.add(cdtcZLSecurity);
									logger.info("Secid:"+bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
									logger.info("SecidNm:"+bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
									logger.info("FaceAmt:"+new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
									
								}
								ManualSettleDetail.setSecList(secList);
							}
							logger.info("secList size :"+secList.size());
							
							ManualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
							ManualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
						}
					}
				}
			}catch (Exception e) {
				logger.error("CDTC综合结算[质押式回购]详情查询异常，异常信息："+e.getMessage());
				logger.error(ExceptionUtil.getErrorInfoFromException(e));
				smtOutBean.setRetStatus(RetStatusEnum.F);
				smtOutBean.setRetMsg("CDTC综合结算详情查询异常，异常信息："+e.getMessage());
				pair.setLeft(smtOutBean);
				return pair;
			}
			
		}
		smtOutBean.setRetStatus(RetStatusEnum.S);
		smtOutBean.setRetMsg("CDTC综合结算详情查询成功");
		pair.setLeft(smtOutBean);
		pair.setRight(ManualSettleDetail);
		return  pair;
	}

}
