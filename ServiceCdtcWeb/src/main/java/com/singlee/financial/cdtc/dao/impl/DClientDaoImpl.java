package com.singlee.financial.cdtc.dao.impl;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.cdtc.bean.CdtcDClientBean;
import com.singlee.financial.cdtc.dao.DClientDao;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.util.ObjectConvertToBytes;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class DClientDaoImpl implements DClientDao {
	
	//private static Logger logger = LogManager.getLogger(LogManager.MODEL_DCLIENT);
	private static Logger logger = LoggerFactory.getLogger(DClientDaoImpl.class);
	
	private String cdtcDClientServerIp;
	
	private Integer setSocketTimeOut;
	
	private Integer socketPort;
	
	@Override
	public HashMap<String, String> sendXmlMessageToCdtcForRequest(String xml) throws Exception {
		// TODO Auto-generated method stub
		
		Socket socket = null;
		
		DataOutputStream out = null;
		DataInputStream in = null;
		HashMap<String, String> resultHashMap = new HashMap<String, String>();
		try{
			socket = new Socket(this.cdtcDClientServerIp, this.socketPort);
			socket.setSoTimeout(this.setSocketTimeOut);
			out = new DataOutputStream(socket.getOutputStream());
			in = new DataInputStream(socket.getInputStream());
			CdtcDClientBean cdtcDClientBean = new CdtcDClientBean();
			cdtcDClientBean.setLoginName("");
			cdtcDClientBean.setType("XML");
			cdtcDClientBean.setXmlBody(XmlFormat.format(xml));
			out.write(ObjectConvertToBytes.objectToBytes(cdtcDClientBean));
			out.flush();
			logger.info("{sendXmlMessageToCdtcForRequestBySocket}"+XmlFormat.format(xml)+"SEND SUC!");
			byte type = 'S';
			byte[] str = new byte[1];
			int  i = in.read(str);
			type = str[0];
			List<Byte> list = new ArrayList<Byte>();
			 if(type !='O')
			 {
				 list.add(str[0]);
			 }
			 
			 while(i!=-1 && in.available()!=0)
			 {
				 i = in.read(str);
				 
				 list.add(str[0]);
			 }
			 byte[] total = new byte[list.size()];
			 int count = 0;
			 for(byte a : list)
			 {
				 total[count++] = a;
			 }
			 if(type =='O')
			 {
				 try{
					Object object = ObjectConvertToBytes.bytesToObject(total);
					if(object instanceof CdtcDClientBean)
					{
						CdtcDClientBean cdtcDClientBean2 = (CdtcDClientBean)object;
						if(null != cdtcDClientBean2.getXmlBody())
					    {
					    	resultHashMap.put("ISOK", "TRUE");
					    	resultHashMap.put("MSG", cdtcDClientBean2.getXmlBody());
					    }else
					    {
					    	resultHashMap.put("ISOK", "FALSE");
					    	resultHashMap.put("EXP", cdtcDClientBean2.getCode()+cdtcDClientBean2.getMsg());
					    }
					}
				 }catch (Exception e) {
					 resultHashMap.put("EXP", e.getMessage());
				 }
			 }else{
				 
				 resultHashMap.put("EXP", "回执报文不是所属XML对象！");
			 }
		}catch (Exception e) {
			e.printStackTrace();
			resultHashMap.put("EXP", e.getMessage());
			logger.info("{sendXmlMessageToCdtcForRequestBySocket}"+e.getMessage());
		}finally{
			if(null != in) {in.close();}
			if(null != out) {out.close();}
			if(null != socket) { socket.close();}
		}
		 
		return resultHashMap;
	}
	/**
	 * @return the cdtcDClientServerIp
	 */
	public String getCdtcDClientServerIp() {
		return cdtcDClientServerIp;
	}
	/**
	 * @param cdtcDClientServerIp the cdtcDClientServerIp to set
	 */
	public void setCdtcDClientServerIp(String cdtcDClientServerIp) {
		this.cdtcDClientServerIp = cdtcDClientServerIp;
	}
	/**
	 * @return the setSocketTimeOut
	 */
	public Integer getSetSocketTimeOut() {
		return setSocketTimeOut;
	}
	/**
	 * @param setSocketTimeOut the setSocketTimeOut to set
	 */
	public void setSetSocketTimeOut(Integer setSocketTimeOut) {
		this.setSocketTimeOut = setSocketTimeOut;
	}
	/**
	 * @return the socketPort
	 */
	public Integer getSocketPort() {
		return socketPort;
	}
	/**
	 * @param socketPort the socketPort to set
	 */
	public void setSocketPort(Integer socketPort) {
		this.socketPort = socketPort;
	}
	@Override
	public HashMap<String, String> sendXmlMessageToCdtcForRequest(String xml,
			String userName) throws Exception {
		// TODO Auto-generated method stub
			Socket socket = null;
			
			DataOutputStream out = null;
			DataInputStream in = null;
			HashMap<String, String> resultHashMap = new HashMap<String, String>();
			try{
				socket = new Socket(this.cdtcDClientServerIp, this.socketPort);
				socket.setSoTimeout(this.setSocketTimeOut);
				out = new DataOutputStream(socket.getOutputStream());
				in = new DataInputStream(socket.getInputStream());
				CdtcDClientBean cdtcDClientBean = new CdtcDClientBean();
				cdtcDClientBean.setLoginName(userName);
				cdtcDClientBean.setType("XML");
				cdtcDClientBean.setXmlBody(XmlFormat.format(xml));
				out.write(ObjectConvertToBytes.objectToBytes(cdtcDClientBean));
				out.flush();
				logger.info("{sendXmlMessageToCdtcForRequestBySocket}"+XmlFormat.format(xml)+"SEND SUC!");
				byte type = 'S';
				byte[] str = new byte[1];
				int  i = in.read(str);
				type = str[0];
				List<Byte> list = new ArrayList<Byte>();
				 if(type !='O')
				 {
					 list.add(str[0]);
				 }
				 
				 while(i!=-1 && in.available()!=0)
				 {
					 i = in.read(str);
					 
					 list.add(str[0]);
				 }
				 byte[] total = new byte[list.size()];
				 int count = 0;
				 for(byte a : list)
				 {
					 total[count++] = a;
				 }
				 if(type =='O')
				 {
					 try{
						Object object = ObjectConvertToBytes.bytesToObject(total);
						if(object instanceof CdtcDClientBean)
						{
							CdtcDClientBean cdtcDClientBean2 = (CdtcDClientBean)object;
							if(null != cdtcDClientBean2.getXmlBody() && !"NULLL".equalsIgnoreCase(cdtcDClientBean2.getXmlBody()))
						    {
						    	resultHashMap.put("ISOK", "TRUE");
						    	resultHashMap.put("MSG", cdtcDClientBean2.getXmlBody());
						    }else
						    {
						    	resultHashMap.put("ISOK", "FALSE");
						    	resultHashMap.put("EXP", cdtcDClientBean2.getCode()+cdtcDClientBean2.getMsg());
						    }
						}
					 }catch (Exception e) {
						 resultHashMap.put("EXP", e.getMessage());
					 }
				 }else{
					 
					 resultHashMap.put("EXP", "回执报文不是所属XML对象！");
				 }
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				resultHashMap.put("EXP", e.getMessage());
				logger.info("{sendXmlMessageToCdtcForRequestBySocket}"+e.getMessage());
			}finally{
				if(null != in) {in.close();}
				if(null != out) {out.close();}
				if(null != socket) { socket.close();}
			}
			 
			return resultHashMap;
		}

}
