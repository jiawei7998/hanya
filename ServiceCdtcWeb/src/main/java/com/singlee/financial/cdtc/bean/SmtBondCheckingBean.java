package com.singlee.financial.cdtc.bean;

import java.math.BigDecimal;

/**
 * 债券对账核对
 * 
 * @author SINGLEE
 *
 */
public class SmtBondCheckingBean {

	private String secId;

	private BigDecimal ableUseAmtZj;

	private BigDecimal repoRevAmtZj;

	private BigDecimal totalAmtZj;

	public String getSecId() {
		return secId;
	}

	public BigDecimal getAbleUseAmtZj() {
		return ableUseAmtZj;
	}

	public void setAbleUseAmtZj(BigDecimal ableUseAmtZj) {
		this.ableUseAmtZj = ableUseAmtZj;
	}

	public BigDecimal getRepoRevAmtZj() {
		return repoRevAmtZj;
	}

	public void setRepoRevAmtZj(BigDecimal repoRevAmtZj) {
		this.repoRevAmtZj = repoRevAmtZj;
	}

	public BigDecimal getTotalAmtZj() {
		return totalAmtZj;
	}

	public void setTotalAmtZj(BigDecimal totalAmtZj) {
		this.totalAmtZj = totalAmtZj;
	}

	public void setSecId(String secId) {
		this.secId = secId;
	}

}
