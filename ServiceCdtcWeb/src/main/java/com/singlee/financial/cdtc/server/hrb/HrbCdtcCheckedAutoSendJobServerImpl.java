package com.singlee.financial.cdtc.server.hrb;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cdc.dmsg.client.DClient;
import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.cdtc.bean.SlCnbondlinkSecurDeal;
import com.singlee.financial.cdtc.cnbondpackage.CdtcBankAccount;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSysExceptionAdviceV01;
import com.singlee.financial.cdtc.dao.BranprcdateGetDao;
import com.singlee.financial.cdtc.dao.CdtcRequestMessageCreateDao;
import com.singlee.financial.cdtc.dao.CnbondManagerDao;
import com.singlee.financial.cdtc.dao.TyjszlztbgDao;
import com.singlee.financial.cdtc.dao.impl.CdtcRequestMessageCreateDaoImpl;
import com.singlee.financial.cdtc.hrb.HrbCdtcCheckedAutoSendJobServer;
import com.singlee.financial.cdtc.hrb.HrbCdtcForCheckClearJobServer;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.InstructionConfirmIndicatorCode;
import com.singlee.financial.cdtc.pac.InstructionStatusCode;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
/**
 * 自动进行结算的任务,发送核对完成的确认指令报文
 * 筛选IFS_CDTC_CNBOND_SECURDEAL表和IFS_CDTC_CNBOND_PKG
 * 核对成功的数据进行确认请求
 * @author Administrator
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCdtcCheckedAutoSendJobServerImpl implements HrbCdtcCheckedAutoSendJobServer{
	
    private Logger logger = LoggerFactory.getLogger("AUTOSEND");
	
    @Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;
    
    @Autowired
	private TyjszlztbgDao tyjszlztbgDao;
    @Autowired
	private CnbondManagerDao cnbondManagerDao;
    @Autowired
	private BranprcdateGetDao branprcdateGetDao;
	@Autowired
	private HrbCdtcForCheckClearJobServer cdtcForCheckClearJobServer;
		
	@Override
	public RetBean autoSendCheckedXml(Map<String, Object> params) throws Exception {
		try{
			/*************************************************************************
			 需要判断日期是否切换，如果切换还需考虑有效时间区域
			**************************************************************************/
			Boolean isHolyDay=(Boolean) params.get("isHolyDay");
			String postDate=(String) params.get("postDate");
			logger.info("start CdtcForCheckClearJobServer doQuartzJobCheck---------------------");
			RetBean ret = cdtcForCheckClearJobServer.validCdtcWorkTime(isHolyDay);
			if(RetStatusEnum.F.equals(ret.getRetStatus())){
				logger.info("当前是节假期或非工作时间,不进行业务处理!");
				Thread.sleep(2000);
				return ret;
			}
			
			/**
			 * 获得系统日期 SELECT * FROM BRPS WHERE BR='01';
			 */
			String branchProcessDate = postDate;
			//branchProcessDate=“2017-08-01”
			logger.info("筛选通用结算指令的系统日期："+branchProcessDate);
			/**
			 * 筛选出 SELECT BR,DEALNO,PS,PRODCODE,PRODTYPE,SECID,SECIDNM,CIF,CUSTNM,CFETSSN,DEALDATE,VDATE,MATDATE,FACEAMT,COMPROCDAMT,MATPROCDAMT,COMPROCDAMTZZ,MATPROCDAMTZZ,COMPROCDAMTSUB,MATPROCDAMTSUB,DEALSTATUS,DEALTEXT,COMCCYSMEANS,MATCCYSMEANS,SETTTYPE_B,SETTDESCR_B,SETTTYPE_E,SETTDESCR_E,XMLMATCHID,CHECKSTS,CHECKTIME,REQUESTSN,SENDSTS,SENDTIME,CNBONDRETSTS,CNBONDRETTIME,ACCTQRYACCTID,INSTRID,TXFLOWID,INSTRSTS,TXRSLTCD,INSERRINF,ORGTRCNFRMIND,CTRCNFRMIND,CTRCTID,CTRCTSTS,CTRCTBLCKSTS,CTRCTFAILDRSN,QUERYRETTIME FROM IFS_CDTC_CNBONDLINK_SECURDEAL WHERE VDATE=TO_DATE(?,'YYYY-MM-DD') AND (REQUESTSN IS NOT NULL OR NVL(REQUESTSN,'XX')<>'XX') AND (SENDSTS IS NULL OR NVL(SENDSTS,'XX')='XX')
			 * 需要发送的通用结算指令
			 */
			List<SlCnbondlinkSecurDeal> dealList = this.tyjszlztbgDao.queryAllSlCnbondlinkSecurDealForAutoProcessDate(branchProcessDate);
			/**
			 * {SELECT BR,DEALNO,PS,PRODCODE,PRODTYPE,SECID,SECIDNM,CIF,CUSTNM,CFETSSN,DEALDATE,VDATE,MATDATE,FACEAMT,COMPROCDAMT,MATPROCDAMT,COMPROCDAMTZZ,MATPROCDAMTZZ,COMPROCDAMTSUB,MATPROCDAMTSUB,DEALSTATUS,DEALTEXT,COMCCYSMEANS,MATCCYSMEANS,SETTTYPE_B,SETTDESCR_B,SETTTYPE_E,SETTDESCR_E,XMLMATCHID,CHECKSTS,CHECKTIME,REQUESTSN,SENDSTS,SENDTIME,CNBONDRETSTS,CNBONDRETTIME,ACCTQRYACCTID,INSTRID,TXFLOWID,INSTRSTS,TXRSLTCD,INSERRINF,ORGTRCNFRMIND,CTRCNFRMIND,CTRCTID,CTRCTSTS,CTRCTBLCKSTS,CTRCTFAILDRSN,QUERYRETTIME FROM IFS_CDTC_CNBONDLINK_SECURDEAL WHERE VDATE=TO_DATE('"+branchProcessDate+"','YYYY-MM-DD') AND (REQUESTSN IS NOT NULL OR NVL(REQUESTSN,'XX')<>'XX') AND TRIM(NVL(CNBONDRETSTS,'XX')) <> 'IC01' AND (TXRSLTCD IS NULL OR NVL(TXRSLTCD,'XX')='XX' OR TXRSLTCD LIKE 'BJ%'  OR TXRSLTCD LIKE 'XTYC%')}
			 */
			logger.info("需要发送通用结算指令的交易有："+dealList.size()+"笔");
			
			for(SlCnbondlinkSecurDeal slCnbondlinkSecurDeal : dealList) {
				/**
				 * SELECT MSGSN,MSGFILENAME,MSGXML,MSGTYPE,MSGTIME FROM SL_INTFC_CNBOND_PKG WHERE WHERE TRIM(MSGSN) = TRIM(?)
				 */
				//根据名称读取报文数据
				AbstractSlIntfcCnbondPkgId ab = cnbondManagerDao.getSlintfccnbondpkgByMsgtype(slCnbondlinkSecurDeal.getRequestsn());
				if(null == ab){
					logger.info("根据名称读取报文数据为 ：" + ab);
					return new RetBean(RetStatusEnum.F, "fail", "根据名称读取报文数据为null！");
				}
				logger.info("得到的通用结算指令结算报文ID："+ab.getMsgsn()+"{SELECT MSGSN,MSGFILENAME,MSGXML,MSGTYPE,MSGTIME FROM SL_INTFC_CNBOND_PKG WHERE WHERE TRIM(MSGSN) = TRIM('"+slCnbondlinkSecurDeal.getRequestsn()+"')}");
				logger.info("SEND XML{\r\n"+ab.getMsgxml()+"\r\n}");
				slCnbondlinkSecurDeal.setSendtime(new Date());
				DClient dClient = new DClient(wasIp);//"http://10.1.81.60:9080/UServer");
				String backXml = dClient.sendMsg(ab.getMsgxml());
				logger.info("RETURN XML{\r\n"+backXml+"\r\n}");
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));  
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
				logger.info("TYJSZL PK TYPE:"+type);
				if(null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {
					/**
					 * 读取请求报文XML 反解析报文
					 */
					slCnbondlinkSecurDeal.setSendsts("SD01");
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
					spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					if(spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for(Group group : groupsList) {
							// 保存xml文件
							StringBuffer xmlBuffer = new StringBuffer();group.toXmlForParent(xmlBuffer);
							commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
							CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
						}
					}
					if(null != commonDistributionSettlementStatusReport) {
						if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
							/************************************************************************************************
							 *    <InstrStsInf>
							        <InstrSts>IS03</InstrSts>
							        <TxRsltCd>BJ000000</TxRsltCd>
							        <OrgtrCnfrmInd>IC01</OrgtrCnfrmInd>原结算合同标识
							        <CtrCnfrmInd>IC02</CtrCnfrmInd>
							      </InstrStsInf>
							      <InstrStsInf>
							        <TxRsltCd>BJ999999</TxRsltCd>
							        <InsErrInf>非待确认指令不允许确认</InsErrInf>
							      </InstrStsInf>
							 **************************************************************************************************/
							Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
							Group instrStsInf = stsFlg.hasGroup("InstrStsInf")?stsFlg.getGroups("InstrStsInf").get(0):null;
							if(null != instrStsInf) {
								slCnbondlinkSecurDeal.setInstrsts(instrStsInf.getfieldsForFieldMap().containsKey("InstrSts")?instrStsInf.getfieldsForFieldMap().get("InstrSts").getObject().toString():" ");
								slCnbondlinkSecurDeal.setTxrsltcd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd")?instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject().toString():" ");
								slCnbondlinkSecurDeal.setInserrinf(instrStsInf.getfieldsForFieldMap().containsKey("InsErrInf")?instrStsInf.getfieldsForFieldMap().get("InsErrInf").getObject().toString():" ");
								slCnbondlinkSecurDeal.setOrgtrcnfrmind(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd")?instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString():" ");
								slCnbondlinkSecurDeal.setCtrcnfrmind(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd")?instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString():" ");
							}
						}
					}
					logger.info("是否成功代码txRsltCd:"+slCnbondlinkSecurDeal.getTxrsltcd());
					if("BJ000000".equalsIgnoreCase(slCnbondlinkSecurDeal.getTxrsltcd())) {
						slCnbondlinkSecurDeal.setCnbondretsts(InstructionConfirmIndicatorCode.Type.IC01.getType());//我方已确认
					}else {
						slCnbondlinkSecurDeal.setCnbondretsts(InstructionConfirmIndicatorCode.Type.IC00.getType());//未明状态
						/**
						 * 如果失败了那么需要进行查询指令详情
						 */
						logger.info("进入指令详情查询，更新状态步骤!");
						String txFlowId = new SimpleDateFormat("yyyyMMdd").format(new Date())+CdtcBankAccount.BANK_ACCOUNT+DateUtil.convertDateToYYYYMMDDHHMMSS(new Date());
						slCnbondlinkSecurDeal.setAcctqryacctid(txFlowId);
						if(slCnbondlinkSecurDeal.getInstrid() == null){
							logger.info("获取结算指令标识为： " + slCnbondlinkSecurDeal.getInstrid());
							return new RetBean(RetStatusEnum.F, "fail", "获取结算指令标识为null！");
						}
					
						try{
							CdtcRequestMessageCreateDao cdtcRequestMessageCreateDao = new CdtcRequestMessageCreateDaoImpl();
							SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS04AndBJ0300(txFlowId, CdtcBankAccount.BANK_ACCOUNT, slCnbondlinkSecurDeal.getInstrid());
							StringBuffer xmlBuffer = new StringBuffer();
							settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);
							//开始查询指令详情 判断我方是否已经确认了，因为在出现网络异常或则数据库更新异常的时候 程序还是会再次发起指令结算的，这时候中债会返回处理失败，固程序需自动再次查询指令结算状态，更新数据库
							logger.info("指令详情 SEND{\r\n"+XmlFormat.format(xmlBuffer.toString())+"\r\n}");
							String detailXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
							logger.info("指令详情 RECV{\r\n"+detailXml+"\r\n}");
							xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(detailXml).getBytes("UTF-8")));  
							type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
							logger.info("指令详情 PK TYPE	:"+type);
							if(null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {
								//解析指令详情获得 InstrStsInf组件 和 SttlmDtl 组件
								spotSettlementInstruction = new SpotSettlementInstruction("Root");
								spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
								spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
								if(spotSettlementInstruction.hasGroup("Document")) {
									List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
									for(Group group : groupsList) {
										// 保存xml文件
										StringBuffer xmlBuffer2 = new StringBuffer();
										group.toXmlForParent(xmlBuffer2);
										commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
										Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer2.toString()+"</Document>").getBytes("UTF-8"))));  
										CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
									}
								}
								if(null != commonDistributionSettlementStatusReport) {
									if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
										/**
										 *    <InstrStsInf>
										        <InstrSts>IS03</InstrSts>
										        <TxRsltCd>BJ000000</TxRsltCd>
										        <OrgtrCnfrmInd>IC01</OrgtrCnfrmInd>
										        <CtrCnfrmInd>IC02</CtrCnfrmInd>
										      </InstrStsInf>
										      <InstrStsInf>
										        <TxRsltCd>BJ999999</TxRsltCd>
										        <InsErrInf>非待确认指令不允许确认</InsErrInf>
										      </InstrStsInf>
										 */
										Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
										Group instrStsInf = stsFlg.hasGroup("InstrStsInf")?stsFlg.getGroups("InstrStsInf").get(0):null;
										if(null != instrStsInf) {
											slCnbondlinkSecurDeal.setInstrsts(instrStsInf.getfieldsForFieldMap().containsKey("InstrSts")?instrStsInf.getfieldsForFieldMap().get("InstrSts").getObject().toString():" ");
											slCnbondlinkSecurDeal.setTxrsltcd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd")?instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject().toString():" ");
											slCnbondlinkSecurDeal.setInserrinf(instrStsInf.getfieldsForFieldMap().containsKey("InstrSts")?InstructionStatusCode.Type.getName(instrStsInf.getfieldsForFieldMap().get("InstrSts").getObject().toString()):" ");
											slCnbondlinkSecurDeal.setOrgtrcnfrmind(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd")?instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString():" ");
											slCnbondlinkSecurDeal.setCtrcnfrmind(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd")?instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString():" ");
										}
									}
									if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
										/**
										 * 	  <GivAcct>
										        <Nm>美国银行上海分行</Nm>
										        <Id>Z0489000000</Id>
										      </GivAcct>
										      <TakAcct>
										        <Nm>江苏银行</Nm>
										        <Id>J00000003995</Id>
										      </TakAcct>
										 */
										Group givAcct = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0).getGroups("GivAcct").get(0);
										String givAcctId=givAcct.getfieldsForFieldMap().containsKey("Id")?givAcct.getfieldsForFieldMap().get("Id").getObject().toString():"";
										if(null != givAcctId && !"".equalsIgnoreCase(givAcctId.trim())) {
											slCnbondlinkSecurDeal.setCnbondretsts(givAcctId.equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)?slCnbondlinkSecurDeal.getOrgtrcnfrmind():slCnbondlinkSecurDeal.getCtrcnfrmind());//我方已确认
										}
									}
								}
							}else if("CSBS.009.001.01".equalsIgnoreCase(type)){
								spotSettlementInstruction = new SpotSettlementInstruction("Root");
								spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
								spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
								SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
								if(spotSettlementInstruction.hasGroup("Document")) {
									List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
									for(Group group : groupsList) {
										StringBuffer xmlBufferT = new StringBuffer();
										group.toXmlForParent(xmlBufferT);
										systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
										Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBufferT.toString()+"</Document>").getBytes("UTF-8"))));  
										XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
									}
								}
								if(null != systemExceptionAdviceV01) {
									String XcptnRtrCd = "";String XcptnRtrTx="";
									if(systemExceptionAdviceV01.hasGroup("SysXcptn")) {
										XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
										XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
									}
									slCnbondlinkSecurDeal.setTxrsltcd(XcptnRtrCd);
									slCnbondlinkSecurDeal.setInserrinf(XcptnRtrTx);
								}
							}
						}catch (Exception e) {
							logger.info("EXCEPTION:{"+e.getMessage()+"}");
							e.printStackTrace();
						}
					}
					slCnbondlinkSecurDeal.setCnbondrettime(new Date());
					/**
					 * 发送后返回的交易状态更新
					 */
					if(this.tyjszlztbgDao.updateTyjszlztbgIdsForSend(slCnbondlinkSecurDeal)) {
						logger.info("更新"+slCnbondlinkSecurDeal.getId().getDealno()+"结算成功！CSBS.002.001.01");
					}else {
						logger.info("更新"+slCnbondlinkSecurDeal.getId().getDealno()+"结算失败！进入归档操作!");
					}
				}else if("CSBS.009.001.01".equalsIgnoreCase(type)){
					slCnbondlinkSecurDeal.setSendsts("SD01");
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if(spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for(Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBufferT.toString()+"</Document>").getBytes("UTF-8"))));  
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
						}
					}
					if(null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";String XcptnRtrTx="";
						if(systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
						}
						slCnbondlinkSecurDeal.setTxrsltcd(XcptnRtrCd);
						slCnbondlinkSecurDeal.setInserrinf(XcptnRtrTx);
						/**
						 * 发送后返回的交易状态更新
						 */
						if(this.tyjszlztbgDao.updateTyjszlztbgIdsForSend(slCnbondlinkSecurDeal)) {
							logger.info("更新"+slCnbondlinkSecurDeal.getId().getDealno()+"结算成功！CSBS.009.001.01");
						}else {
							logger.info("更新"+slCnbondlinkSecurDeal.getId().getDealno()+"结算失败！进入归档操作!");
						}
					}
				}
			}
			
		} catch (Exception e) {
			logger.error("EXCEPTION:{"+e.getMessage()+"}",e);
			throw e;
		}
		return new RetBean(RetStatusEnum.S, "success", "发送指令确认报文成功！");
	}

}
