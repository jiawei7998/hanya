package com.singlee.financial.cdtc.cnbondpackage;

import java.io.File;
import java.util.Date;
import java.util.HashMap;

import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.pac.AccountIdentificationAndName5;
import com.singlee.financial.cdtc.pac.Bond2;
import com.singlee.financial.cdtc.pac.Exact11Text;
import com.singlee.financial.cdtc.pac.GenericIdentification4;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODate;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max30Text;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.Max5NumericText;
import com.singlee.financial.cdtc.pac.Max70Text;
import com.singlee.financial.cdtc.pac.PartyIdentification;
import com.singlee.financial.cdtc.pac.Priority4Code;

public class SafekeepingAccountStatementReportV01  extends Group{

	/**
	 * @param args
	 * 	6.3.8	债券账户对帐单报告(CSBS.040.001.01 SafekeepingAccountStatementReportV01)
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private static String type = "00000077982012060102964266";


	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	
	static{
		keyClassMap.put("Msg",     ReportMsgHeader.class);
		keyClassMap.put("StmtRpt", SafekeepingAccountStatementReportStmtRpt.class);
		keyClassMap.put("QryPty",  ReportQryPty.class);
		keyClassMap.putAll(ReportMsgHeader.keyClassMap);
		keyClassMap.putAll(SafekeepingAccountStatementReportStmtRpt.keyClassMap);
		keyClassMap.putAll(ReportQryPty.keyClassMap);
	}
	
	public SafekeepingAccountStatementReportV01(String tag){
		super(tag,tag, new String[] { "Msg", "StmtRpt","QryPty","Xtnsn"});
	}
	public SafekeepingAccountStatementReportV01(String tag,String delim){
		super(tag,tag, new String[] { "Msg", "StmtRpt","QryPty","Xtnsn"});
	}
	
	public boolean isHasTag(String tag)
	{
		String[] strings = getFieldOrder();
		for(String key:strings)
		{
			if(key.equals(tag))
			{
				return true;
			}
		}
		return false;
	}
	
	public void setMsg(ReportMsgHeader msg){
		addGroup(msg);
	}
	
	public void setStmtRpt(SafekeepingAccountStatementReportStmtRpt stmtRpt){
		addGroup(stmtRpt);
	}
	
	public void setQryPty(ReportQryPty qryPty){
		addGroup(qryPty);
	}
	
	public void setXtnsn(){}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer)
	{
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
	}

	public static SafekeepingAccountStatementReportV01 content(Bean1 bean1){
		SafekeepingAccountStatementReportV01 report = new SafekeepingAccountStatementReportV01("Document");
		/***************MSG***************/
		ReportMsgHeader msg = new ReportMsgHeader("Msg");
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("交易流水标识");
		msg.set(max35Text);
		
		PartyIdentification partyIdentification = new PartyIdentification("Oprtr");
		Max35Text max35Text2 = new Max35Text("Nm");
		max35Text2.setValue("经办人");
		partyIdentification.set(max35Text2);
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT01.getType());
		msg.set(priority4Code);
		msg.set(partyIdentification);
		
		ISODateTime isDateTime = new ISODateTime("CreDtTm",new Date());
		msg.set(isDateTime);
		report.setMsg(msg);
		
		
		/***************************StmtRpt*************************/
		SafekeepingAccountStatementReportStmtRpt stmtRpt = new SafekeepingAccountStatementReportStmtRpt("StmtRpt");
		
		ISODate isoDate1 = new ISODate("StartDt",new Date());
		stmtRpt.setStartDt(isoDate1);
		
		ISODate isoDate2 = new ISODate("EndDt",new Date());
		stmtRpt.setEndDt(isoDate2);
		
		Max5NumericText max5NumericText = new Max5NumericText("SfkpgAcctCnt");
		max5NumericText.setValue(1111);
		stmtRpt.setSfkpgAcctCnt(max5NumericText);
		
		AccountIdentificationAndName5 accountIdentificationAndName5 = new AccountIdentificationAndName5("SfkpgAcctStmtInf");
		accountIdentificationAndName5.set(max35Text2);
		Exact11Text idExact11Text = new Exact11Text("Id");
		idExact11Text.setValue("12345678901");
		accountIdentificationAndName5.set(idExact11Text);
		Max5NumericText max5NumericText2 = new Max5NumericText("BdCnt");
		max5NumericText2.setValue(12345);
		accountIdentificationAndName5.set(max5NumericText2);
		Bond2 bond2 = new Bond2("Bd");
		Max30Text max30Text = new Max30Text("BdId");
		max30Text.setValue("1111111");
		bond2.set(max30Text);
		accountIdentificationAndName5.set(bond2);
		
		stmtRpt.setSfkpgAcctStmtInf(accountIdentificationAndName5);
		
		report.setStmtRpt(stmtRpt);
		/***************************QryPty***************************/
		ReportQryPty qryPty = new ReportQryPty("QryPty");
		Max70Text max70Text = new Max70Text("Nm");
		max70Text.setValue("名称");
		qryPty.set(max70Text);
		
		GenericIdentification4 genericIdentification4 = new GenericIdentification4("PrtryId");
		Max35Text value = new Max35Text("Id");
		value.setValue("自定义标示符。。。");
		Max35Text value2 = new Max35Text("IdTp");
		value2.setValue("XXXXXXXXXx");
		genericIdentification4.set(value);
		genericIdentification4.set(value2);
		qryPty.set(genericIdentification4);

		report.setQryPty(qryPty);
		return report;
	}
	public static void main(String[] args) throws Exception{
		
			
			/************************************sysout*********************************/
//			StringBuffer stringBuffer = new StringBuffer();
//			content(new Bean1()).toXmlForSuperParent(stringBuffer); 
//			
//			System.out.println(XmlFormat.format(stringBuffer.toString()));
//			
//			// 保存xml文件
//			org.desy.trd.jdom.input.SAXBuilder saxBuilder = new SAXBuilder();
//			Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat.format(stringBuffer.toString()).getBytes("UTF-8")));
//			FileOutputStream out=new FileOutputStream("./cnbondxml/"+SafekeepingAccountStatementReportV01.type+".xml"); 
//			XMLOutputter outputter = new XMLOutputter(); 
//			//如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的 
//			outputter.output((org.desy.trd.jdom.Document) doc, out); 
//			out.close(); 
//			

		
	        /**
	         * 读取请求报文XML  反解析报文
	         */
			SAXReader reader = new SAXReader();
			SafekeepingAccountStatementReportV01 report2 = new SafekeepingAccountStatementReportV01("Document");
			org.dom4j.Document xmlDoc = reader.read(new File("./cnbondxml/"+SafekeepingAccountStatementReportV01.type+".xml"));
			XMLRevSafeAcctStatReportV01.getElementList(xmlDoc.getRootElement().element("Document"),report2);
			
		 

	}

}
