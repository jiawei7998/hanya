package com.singlee.financial.cdtc.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.cdtc.dao.CnbondManagerDao;
import com.singlee.financial.cdtc.mapper.CnbondManagerMapper;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CnbondManagerDaoImpl implements CnbondManagerDao{
	@Autowired
	CnbondManagerMapper cnbondManagerMapper;
	
	@Override
	public boolean addSlintfccnbondpkg(AbstractSlIntfcCnbondPkgId cnbond)
			throws Exception {
		// TODO Auto-generated method stub
		boolean addReturn = true;
		try{
			cnbondManagerMapper.addSlintfccnbondpkg(cnbond);
		}catch(Exception e){
			addReturn = false;
			throw e;
		}
		return addReturn;
	}

	@Override
	public AbstractSlIntfcCnbondPkgId getSlintfccnbondpkgByMsgtype(
			String msgfilename) throws SQLException, Exception {
		// TODO Auto-generated method stub
		AbstractSlIntfcCnbondPkgId msgxml = null;
		try{
		    msgxml = cnbondManagerMapper.getSlintfccnbondpkgByMsgtype(msgfilename);
		}catch(Exception e){
			throw e;
		}
		return msgxml;
	}

	@Override
	public boolean updateCnbond(AbstractSlIntfcCnbondPkgId cnbond)
			throws Exception {
		// TODO Auto-generated method stub
		boolean flag = false;
		int i = cnbondManagerMapper.updateCnbond(cnbond);
		if(i > 0){
			flag = true;
		}
		return flag;
	}

	@Override
	public List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDbByDateType(String date,String type) throws Exception {
		// TODO Auto-generated method stub
		List<AbstractSlIntfcCnbondPkgId> queryList = null;
		try{
			 queryList = cnbondManagerMapper.queryAllCSBSBeanFromDbByDateType(date,type);
		}catch(Exception e){
			throw e;
		}
		return queryList;
	}
	
	
	
	@Override
	public List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDb() throws Exception {
		List<AbstractSlIntfcCnbondPkgId> cnbondPkList = null;
		try{
			cnbondPkList = cnbondManagerMapper.queryAllCSBSBeanFromDb();
		}catch(Exception e){
			throw e;
		}
		return cnbondPkList;
	}

	@Override
	public List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDbByTrem(String type, String begindate, String enddate,String txtMsgTypeName) throws Exception {
		List<AbstractSlIntfcCnbondPkgId> cnbondPkList = null;
		try{
			cnbondPkList = cnbondManagerMapper.queryAllCSBSBeanFromDbByTrem(type,begindate,enddate,txtMsgTypeName);
		}catch(Exception e){
			throw e;
		}
		return cnbondPkList;
	}

}
