package com.singlee.financial.cdtc.pac;


/**
 *  OS00：创建
OS01：确认
OS02：拒绝
OS03：批量
OS04：详情
OS05：付券方确认
OS06：收券方确认
OS07: 明细对账单
OS08: 总对账单

 * @author Administrator
 *
 */
public class OperatorStatusCode extends StringField {
	public static enum Type {
			OS00("OS00","创建        "),
			OS01("OS01","确认        "),
			OS02("OS02","拒绝        "),
			OS03("OS03","批量        "),
			OS04("OS04","详情        "),
			OS05("OS05","付券方确认  "),
			OS06("OS06","收券方确认  "),
			OS07("OS07","总对账单    "),
			OS08("OS08","明细对账单   ");
					private String type;
			
			private String name;

			
			private Type(String type, String name) {
				this.type = type;
				this.name = name;
			}
			// 普通方法   
		    public static String getName(String type) {   
		        for (Type c : Type.values()) {   
		            if (c.getType().equalsIgnoreCase(type)) {   
		               return c.name;   
		            }   
		        }   
		        return "";   
		    }  
			/**
			 * @return the type
			 */
			public String getType() {
				return type;
			}

			/**
			 * @param type the type to set
			 */
			public void setType(String type) {
				this.type = type;
			}

			/**
			 * @return the name
			 */
			public String getName() {
				return name;
			}

			/**
			 * @param name the name to set
			 */
			public void setName(String name) {
				this.name = name;
			}
			/* (non-Javadoc)
			 * @see java.lang.Enum#toString()
			 */
			@Override
			public String toString() {
				// TODO Auto-generated method stub
				return this.type+":"+this.name;
			}
		}

	public OperatorStatusCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
