package com.singlee.financial.cdtc.dao;

import java.util.List;

import com.singlee.financial.cdtc.bean.BtchQryRsltBean;
import com.singlee.financial.cdtc.bean.RepoSecurity;
import com.singlee.financial.cdtc.bean.SlCnbondlinkSecurDeal;
import com.singlee.financial.cdtc.pac.CheckStatusEnum;


public interface TyjszlztbgDao {
	/**
	 * 根据日期筛选出全部的DEALS
	 * @param processDate
	 * @return
	 * @throws Exception
	 */
	public List<SlCnbondlinkSecurDeal> queryAllSlCnbondlinkSecurDealForProcessDate(String processDate) throws Exception;
	/**
	 * 调用存储过程SL_SP_PROCESS_SPSH_CDTC
	 * @param processDate
	 * @return
	 * @throws Exception
	 */
	public String processAllSlCnbondlinkSecurDealForProcessDate(String processDate) throws Exception;
    
	/**
	 * 更新CHEK状态
	 * @param statsType
	 * @return
	 * @throws Exception
	 */
	public boolean updateCheckSts(CheckStatusEnum.Type statsType,SlCnbondlinkSecurDeal slCnbondlinkSecurDeal) throws Exception;
	/**
	 * 更新指令状态
	 * @param slCnbondlinkSecurDeal
	 * @return
	 * @throws Exception
	 */
	public boolean updateInstrInformations(SlCnbondlinkSecurDeal slCnbondlinkSecurDeal) throws Exception;
	/**
	 * 更新通用结算指令的全部ID
	 * @param slCnbondlinkSecurDeal
	 * @return
	 * @throws Exception
	 */
	public boolean updateTyjszlztbgIds(SlCnbondlinkSecurDeal slCnbondlinkSecurDeal) throws Exception;
	
	/**
	 * 更新生成的通用结算指令的全部ID
	 * @param slCnbondlinkSecurDeal
	 * @return
	 * @throws Exception
	 */
	public boolean updateTyjszlztbgForCreateId(SlCnbondlinkSecurDeal slCnbondlinkSecurDeal) throws Exception;
	
	/**
	 * 筛选出复核成功并且通用结算指令OK的交易
	 * @param processDate
	 * @return
	 * @throws Exception
	 */
	public List<SlCnbondlinkSecurDeal> queryAllSlCnbondlinkSecurDealForAutoProcessDate(String processDate) throws Exception;
	
	/**
	 * 查询回购的子券
	 * @param slCnbondlinkSecurDeal
	 * @return
	 * @throws Exception
	 */
	public List<RepoSecurity> queryRepoSecurityByDealno(SlCnbondlinkSecurDeal slCnbondlinkSecurDeal) throws Exception;
	/**
	/**
	 * 发送后返回的交易状态更新
	 * @param slCnbondlinkSecurDeal
	 * @return
	 * @throws Exception
	 */
	public boolean updateTyjszlztbgIdsForSend(SlCnbondlinkSecurDeal slCnbondlinkSecurDeal) throws Exception;
	
	/**
	 * 更新通用结算指令中合同状态信息
	 * @param btchQryRsltBean
	 * @return
	 * @throws Exception
	 */
	public boolean updateTyjszlContractMsgStatus(BtchQryRsltBean btchQryRsltBean) throws Exception;
}
