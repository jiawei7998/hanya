package com.singlee.financial.cdtc.mapper;


import java.util.Map;
public interface QueryRepoSecMapper {
	/**
	 * 查询回购的子券
	 * @param slCnbondlinkSecurDeal
	 * @return
	 * @throws Exception
	 */
	 void queryRepoSecurityByDealno(Map<String,Object> map) throws Exception;
}
