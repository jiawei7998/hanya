/**
 * 
 */
package com.singlee.financial.cdtc.dao;

import com.singlee.financial.cdtc.bean.Sl_Intfc_SettlmentRepoEttl;

/**
 * @author Fang
 *
 */
public interface SettlmentRepoEttlDao {

	/**
	 * 提交数据到数据库
	 */
	public boolean addSettlmentRepoEttl(Sl_Intfc_SettlmentRepoEttl settlmentRepoEttl) throws Exception;
	
	/**
	 * 判断是否存在已经办或者已复核信息
	 * @param btchQryRslt
	 * @return
	 * @throws Exception
	 */
	public boolean isExistSettlmentRepoEttl(Sl_Intfc_SettlmentRepoEttl settlmentRepoEttl) throws Exception;
	/**
	 * 更新状态
	 * @param btchList
	 * @return
	 * @throws Exception
	 */
	public boolean updateSettlmentRepoEttlForCondition(Sl_Intfc_SettlmentRepoEttl settlmentRepoEttl) throws Exception;
	
	/**
	 * @param btch
	 * @return BtchQryRsltBean
	 */
	public Sl_Intfc_SettlmentRepoEttl queryContractMsgIsExistSettlmentRepoEttl(Sl_Intfc_SettlmentRepoEttl settlmentRepoEttl) throws Exception;
	
	public boolean deleteSettlmentRepoEttl(Sl_Intfc_SettlmentRepoEttl settlmentRepoEttl) throws Exception;
}
