package com.singlee.financial.cdtc.pac;


public class Max19NumbericText extends IntField {

	private static final String regStr="^[0-9]*$";

	public Max19NumbericText(String field) {
		super(field);
	}
	private static final long serialVersionUID = 1L;
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(getLength()>=1 && getLength() <= 19)
		{
			if(objectAsString().matches(regStr))
			{
				return toXml();
			}
		}
		return "";
	}
	
	public static void main(String[] args) {
		Max19NumbericText max19NumericText = new Max19NumbericText("Max19NumbericText");
		max19NumericText.setObject(12346);
		System.out.println(max19NumericText.toXmlString());
	}
}
