package com.singlee.financial.cdtc.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.singlee.financial.bean.CdtcAcctTotalBean;

public interface CdtcCompareToSmtDao {

	/**
	 * 从中债获取总对账单数据
	 */
	public LinkedHashMap<Integer, CdtcAcctTotalBean> getSecCheckingMap(Map<String, Object> params)  throws Exception;
	
	/**
	 * 将总对账单数据保存到数据库
	 * @param list
	 * @return
	 */
	public int insertCompareToSmtByList(List<CdtcAcctTotalBean> list);
	
	/**
	 * 清空总对账单数据
	 * @return
	 */
	public int deleteCompareToSmt();

	/**
	 * 核对总对账单
	 * @param map
	 * @return
	 */
	public String checkingSummitData(Map<String, Object> map);
	
}
