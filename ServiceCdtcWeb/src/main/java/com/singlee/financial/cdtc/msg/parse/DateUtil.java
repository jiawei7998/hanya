package com.singlee.financial.cdtc.msg.parse;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static final SimpleDateFormat dfISODate = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat dfISODateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	public static Date convertISODateToDate(String isoDate) throws Exception {
		// ISODate YYYY-MM-DD
		return dfISODate.parse(isoDate);
	}

	public static Date convertISODateTimeToDate(String isoDate) throws Exception {
		// ISODateTime YYYY-MM-DDThh:mm:ss.sss
		return dfISODateTime.parse(isoDate);
	}

	public static String convertDateToYYYYMMDDHHMMSS(Date date) {
		return new SimpleDateFormat("yyyyMMddHHmmss").format(date);
	}

	public static String  getNow(String format)
	{
		Calendar cal = Calendar.getInstance();
		return new SimpleDateFormat(format).format(cal.getTime());
	}

	public static void main(String[] args) {
		try {
			System.out.println(DateUtil.convertISODateToDate("2009-12-11"));
			System.out.println(DateUtil.convertISODateTimeToDate("2007-03-11T11:09:05.123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
