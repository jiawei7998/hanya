package com.singlee.financial.cdtc.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectConvertToBytes {

	public static byte[] objectToBytes(Object object) throws Exception{
		
		ByteArrayOutputStream out = null;
		ObjectOutputStream oout = null;
		
		byte[] objectBytes = null;
		try{
			out = new ByteArrayOutputStream();
			
			out.write('O');
			
			oout = new ObjectOutputStream(out);
			
			oout.writeObject(object);   
			
			int length = out.toByteArray().length;
			
			objectBytes = new byte[length];
			
			objectBytes = out.toByteArray();
			
		}catch (Exception e) {
			// TODO: handle exception
			throw e;
		}finally{
			try{
				if(null != out) {
					out.close();}
				if(null != oout) {
					oout.close();}
			}catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		return objectBytes;
	}
	
	public static Object bytesToObject(byte[] bs) throws Exception {
		
		 ByteArrayInputStream byteIn = null;
		 ObjectInputStream ino = null;
		 Object getObject = null;
		 try{
			 byteIn = new ByteArrayInputStream(bs);   
			 ino = new ObjectInputStream(byteIn);   
			 getObject = (Object)ino.readObject();
		 }catch (Exception e) {
			// TODO: handle exception
		    throw e;
		}finally{
			try{
				if(null != byteIn) {
					byteIn.close();}
				if(null != ino) {
					ino.close();}
			}catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		return getObject;
	}
	
}
