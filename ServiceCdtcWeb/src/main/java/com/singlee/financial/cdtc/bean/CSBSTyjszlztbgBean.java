package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.util.Date;

import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
/**
 * 通用结算指令 PKG
 * @author Administrator
 *
 */
public class CSBSTyjszlztbgBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String msgsn;
	private String msgfilename;
	private String msgtype;
	private CommonDistributionSettlementStatusReport csbs002;
	private Date msgtime;
	/**
	 * @return the msgsn
	 */
	public String getMsgsn() {
		return msgsn;
	}
	/**
	 * @param msgsn the msgsn to set
	 */
	public void setMsgsn(String msgsn) {
		this.msgsn = msgsn;
	}
	/**
	 * @return the msgfilename
	 */
	public String getMsgfilename() {
		return msgfilename;
	}
	/**
	 * @param msgfilename the msgfilename to set
	 */
	public void setMsgfilename(String msgfilename) {
		this.msgfilename = msgfilename;
	}
	/**
	 * @return the msgtype
	 */
	public String getMsgtype() {
		return msgtype;
	}
	/**
	 * @param msgtype the msgtype to set
	 */
	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}
	/**
	 * @return the csbs002
	 */
	public CommonDistributionSettlementStatusReport getCsbs002() {
		return csbs002;
	}
	/**
	 * @param csbs002 the csbs002 to set
	 */
	public void setCsbs002(CommonDistributionSettlementStatusReport csbs002) {
		this.csbs002 = csbs002;
	}
	/**
	 * @return the msgtime
	 */
	public Date getMsgtime() {
		return msgtime;
	}
	/**
	 * @param msgtime the msgtime to set
	 */
	public void setMsgtime(Date msgtime) {
		this.msgtime = msgtime;
	}
	
}
