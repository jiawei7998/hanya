package com.singlee.financial.cdtc.dao;

import java.util.List;

import com.singlee.financial.cdtc.cdtcacct.OpicsSecurityTotalBean;

public interface OpicsSSAHDao {

	/**
	 * 根据条件进行筛选 SSAH
	 * @param opicsSecurityTotalBean
	 * @return
	 * @throws Exception
	 */
	public List<OpicsSecurityTotalBean> queryAllOpicsSecurityTotalBeanByCondition(OpicsSecurityTotalBean opicsSecurityTotalBean) throws Exception;
	
}
