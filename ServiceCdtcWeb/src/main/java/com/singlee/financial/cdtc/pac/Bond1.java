package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class Bond1 extends Group {
	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("BdId", Field.class);
		keyClassMap.put("ISIN", Field.class);
		keyClassMap.put("BdShrtNm", Field.class);
	}

	public Bond1(String tag) {

		super(tag, tag, new String[] { "BdId", "ISIN", "BdShrtNm" });
	}
	

	public Bond1(String tag,String delim){
		
		super(tag,tag, new String[] { "BdId", "ISIN","BdShrtNm"});
	}
   public void set(Max30Text value) {
        setField(value);
   }

	public Max30Text get(Max30Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max30Text getMax30Text() throws FieldNotFound {
		Max30Text value = new Max30Text("BdId");
		getField(value);

		return value;
	}

	public boolean isSet(Max30Text field) {
		return isSetField(field);
	}

	public boolean isSetMax30Text() {
		return isSetField("BdId");
	}

	public void set(ISINIdentifier value) {
		setField(value);
	}

	public ISINIdentifier get(ISINIdentifier value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public ISINIdentifier getISINIdentifier() throws FieldNotFound {
		ISINIdentifier value = new ISINIdentifier("ISIN");
		getField(value);

		return value;
	}

	public boolean isSet(ISINIdentifier field) {
		return isSetField(field);
	}

	public boolean isSetISINIdentifier() {
		return isSetField("ISIN");
	}

	public void set(Max35Text value) {
		setField(value);
	}

	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max35Text getMax35Text() throws FieldNotFound {
		Max35Text value = new Max35Text("");
		getField(value);

		return value;
	}

	public boolean isSet(Max35Text field) {
		return isSetField(field);
	}

	public boolean isSetMax35Text() {
		return isSetField("BdShrtNm");
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

}
