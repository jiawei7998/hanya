package com.singlee.financial.cdtc.bean;

import java.io.Serializable;

public class CdtcDClientBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String loginName;//用户名
	
	private String pwd;//SOCKET密钥

	private String type;//报文类型
	
	private String xmlBody;//报文体 返回和出去的报文
	
	private String code;//是否成功dclient的报文
	
	private String msg;//字符串返回信息

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the xmlBody
	 */
	public String getXmlBody() {
		return xmlBody;
	}

	/**
	 * @param xmlBody the xmlBody to set
	 */
	public void setXmlBody(String xmlBody) {
		this.xmlBody = xmlBody;
	}

	/**
	 * @return the loginName
	 */
	public String getLoginName() {
		return loginName;
	}

	/**
	 * @param loginName the loginName to set
	 */
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	/**
	 * @return the pwd
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * @param pwd the pwd to set
	 */
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
