package com.singlee.financial.cdtc.cdtcacct;

import java.io.Serializable;
import java.math.BigDecimal;

public class CdtcAcctTotalCompareBean implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
	 * 债券简称
	 */
	private String secidNm;
	/**
	 * 债券代码
	 */
	private String secid;
	/**
	 * 可用科目 万元
	 */
	private BigDecimal ableUseAmt;
	/**
	 * 待付科目 万元
	 */
	private BigDecimal waitPayAmt;
	/**
	 * 质押科目 万元
	 */
	private BigDecimal repoZYAmt;
	/**
	 * 待购回科目 万元
	 */
	private BigDecimal repoRevAmt;
	/**
	 * 冻结科目
	 */
	private BigDecimal frozenAmt;
	/**
	 * 债券余额 万元
	 */
	private BigDecimal totalAmt;
	
	/***************/
	/**
	 * 可用科目 万元
	 */
	private BigDecimal ableUseAmtZJ;
	
	//可用差额
	private BigDecimal ableGapAmt;
	/**
	 * 待付科目 万元
	 */
	private BigDecimal waitPayAmtZJ;
	/**
	 * 质押科目 万元
	 */
	private BigDecimal repoZYAmtZJ;
	/**
	 * 待购回科目 万元
	 */
	private BigDecimal repoRevAmtZJ;
	
	//待购回差额
	private BigDecimal repoRevGapAmt;
	/**
	 * 冻结科目
	 */
	private BigDecimal frozenAmtZJ;
	/**
	 * 债券余额 万元
	 */
	private BigDecimal totalAmtZJ;
	
	//债券总余额对比差额
	private BigDecimal totalGapAmt;
	
	
	public CdtcAcctTotalCompareBean(String secidNm, String secid,
			BigDecimal ableUseAmt, BigDecimal waitPayAmt, BigDecimal repoZYAmt,
			BigDecimal repoRevAmt, BigDecimal frozenAmt, BigDecimal totalAmt,
			BigDecimal totalGapAmt,BigDecimal repoRevGapAmt,BigDecimal ableGapAmt) {
		super();
		this.secidNm = secidNm;
		this.secid = secid;
		this.ableUseAmt = ableUseAmt;
		this.waitPayAmt = waitPayAmt;
		this.repoZYAmt = repoZYAmt;
		this.repoRevAmt = repoRevAmt;
		this.frozenAmt = frozenAmt;
		this.totalAmt = totalAmt;
		this.ableGapAmt = ableGapAmt;
		this.totalGapAmt = totalGapAmt;
		this.repoRevGapAmt = repoRevGapAmt;
	}
	public CdtcAcctTotalCompareBean() {
		super();
	}
	/**
	 * @return the secidNm
	 */
	public String getSecidNm() {
		return secidNm;
	}
	/**
	 * @param secidNm the secidNm to set
	 */
	public void setSecidNm(String secidNm) {
		this.secidNm = secidNm;
	}
	/**
	 * @return the secid
	 */
	public String getSecid() {
		return secid;
	}
	/**
	 * @param secid the secid to set
	 */
	public void setSecid(String secid) {
		this.secid = secid;
	}
	/**
	 * @return the ableUseAmt
	 */
	public BigDecimal getAbleUseAmt() {
		return ableUseAmt;
	}
	/**
	 * @param ableUseAmt the ableUseAmt to set
	 */
	public void setAbleUseAmt(BigDecimal ableUseAmt) {
		this.ableUseAmt = ableUseAmt;
	}
	/**
	 * @return the waitPayAmt
	 */
	public BigDecimal getWaitPayAmt() {
		return waitPayAmt;
	}
	/**
	 * @param waitPayAmt the waitPayAmt to set
	 */
	public void setWaitPayAmt(BigDecimal waitPayAmt) {
		this.waitPayAmt = waitPayAmt;
	}
	/**
	 * @return the repoZYAmt
	 */
	public BigDecimal getRepoZYAmt() {
		return repoZYAmt;
	}
	/**
	 * @param repoZYAmt the repoZYAmt to set
	 */
	public void setRepoZYAmt(BigDecimal repoZYAmt) {
		this.repoZYAmt = repoZYAmt;
	}
	/**
	 * @return the repoRevAmt
	 */
	public BigDecimal getRepoRevAmt() {
		return repoRevAmt;
	}
	/**
	 * @param repoRevAmt the repoRevAmt to set
	 */
	public void setRepoRevAmt(BigDecimal repoRevAmt) {
		this.repoRevAmt = repoRevAmt;
	}
	/**
	 * @return the frozenAmt
	 */
	public BigDecimal getFrozenAmt() {
		return frozenAmt;
	}
	/**
	 * @param frozenAmt the frozenAmt to set
	 */
	public void setFrozenAmt(BigDecimal frozenAmt) {
		this.frozenAmt = frozenAmt;
	}
	/**
	 * @return the totalAmt
	 */
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	/**
	 * @param totalAmt the totalAmt to set
	 */
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}
	/**
	 * @return the ableUseAmtZJ
	 */
	public BigDecimal getAbleUseAmtZJ() {
		return ableUseAmtZJ;
	}
	/**
	 * @param ableUseAmtZJ the ableUseAmtZJ to set
	 */
	public void setAbleUseAmtZJ(BigDecimal ableUseAmtZJ) {
		this.ableUseAmtZJ = ableUseAmtZJ;
	}
	/**
	 * @return the waitPayAmtZJ
	 */
	public BigDecimal getWaitPayAmtZJ() {
		return waitPayAmtZJ;
	}
	/**
	 * @param waitPayAmtZJ the waitPayAmtZJ to set
	 */
	public void setWaitPayAmtZJ(BigDecimal waitPayAmtZJ) {
		this.waitPayAmtZJ = waitPayAmtZJ;
	}
	/**
	 * @return the repoZYAmtZJ
	 */
	public BigDecimal getRepoZYAmtZJ() {
		return repoZYAmtZJ;
	}
	/**
	 * @param repoZYAmtZJ the repoZYAmtZJ to set
	 */
	public void setRepoZYAmtZJ(BigDecimal repoZYAmtZJ) {
		this.repoZYAmtZJ = repoZYAmtZJ;
	}
	/**
	 * @return the repoRevAmtZJ
	 */
	public BigDecimal getRepoRevAmtZJ() {
		return repoRevAmtZJ;
	}
	/**
	 * @param repoRevAmtZJ the repoRevAmtZJ to set
	 */
	public void setRepoRevAmtZJ(BigDecimal repoRevAmtZJ) {
		this.repoRevAmtZJ = repoRevAmtZJ;
	}
	/**
	 * @return the frozenAmtZJ
	 */
	public BigDecimal getFrozenAmtZJ() {
		return frozenAmtZJ;
	}
	/**
	 * @param frozenAmtZJ the frozenAmtZJ to set
	 */
	public void setFrozenAmtZJ(BigDecimal frozenAmtZJ) {
		this.frozenAmtZJ = frozenAmtZJ;
	}
	/**
	 * @return the totalAmtZJ
	 */
	public BigDecimal getTotalAmtZJ() {
		return totalAmtZJ;
	}
	/**
	 * @param totalAmtZJ the totalAmtZJ to set
	 */
	public void setTotalAmtZJ(BigDecimal totalAmtZJ) {
		this.totalAmtZJ = totalAmtZJ;
	}
	/**
	 * @return the ableGapAmt
	 */
	public BigDecimal getAbleGapAmt() {
		return ableGapAmt;
	}
	/**
	 * @param ableGapAmt the ableGapAmt to set
	 */
	public void setAbleGapAmt(BigDecimal ableGapAmt) {
		this.ableGapAmt = ableGapAmt;
	}
	/**
	 * @return the repoRevGapAmt
	 */
	public BigDecimal getRepoRevGapAmt() {
		return repoRevGapAmt;
	}
	/**
	 * @param repoRevGapAmt the repoRevGapAmt to set
	 */
	public void setRepoRevGapAmt(BigDecimal repoRevGapAmt) {
		this.repoRevGapAmt = repoRevGapAmt;
	}
	/**
	 * @return the totalGapAmt
	 */
	public BigDecimal getTotalGapAmt() {
		return totalGapAmt;
	}
	/**
	 * @param totalGapAmt the totalGapAmt to set
	 */
	public void setTotalGapAmt(BigDecimal totalGapAmt) {
		this.totalGapAmt = totalGapAmt;
	}

}
