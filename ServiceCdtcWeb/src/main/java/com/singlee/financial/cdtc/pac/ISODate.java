package com.singlee.financial.cdtc.pac;

import java.util.Date;

public class ISODate extends UtcDateOnlyField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ISODate(String field, String data) {
		super(field, data);
	}

	public ISODate(String field, Date data) {
		super(field, data);
	}

}
