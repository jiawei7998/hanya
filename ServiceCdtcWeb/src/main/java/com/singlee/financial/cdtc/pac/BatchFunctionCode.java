package com.singlee.financial.cdtc.pac;


/**
 *  BF00：结算指令下载
BF01：结算合同下载
BF02：债券总对账单下载
BF03：债券总对账单（含估值）下载
BF04：债券明细对账单下载
BF05：清偿单据下载
BF06：债券资料下载
BF07：付息兑付单据下载
BF08：结算成员资料下载
BF09：代理统计成员资料下载
BF10：债券资料复合下载
BF11：付息兑付情况下载
BF12：债券选择权资料下载
BF13：实时现券结算行情(全价)下载
BF14：实时现券结算行情(净价)下载
BF15：实时质押式回购行情下载
BF16：实时买断式回购行情下载
BF17：实时行情统计下载
BF18：标准待偿期收益率曲线(历史)下载
BF19：任意待偿期收益率曲线数据下载
BF20：中债估值(历史)下载
BF21：老版标准待偿期收益率曲线数据下载
BF22：老版任意待偿期收益率曲线数据下载
BF23：老版收益率曲线明细数据下载
BF24：老版中债估值下载
BF25：持有债券市值下载

 * @author Administrator
 *
 */
public class BatchFunctionCode extends StringField {
	public static enum Type {
	BF00("BF00","结算指令下载                    "),
	BF01("BF01","结算合同下载                    "),
	BF02("BF02","债券总对账单下载                "),
	BF03("BF03","债券总对账单（含估值）下载      "),
	BF04("BF04","债券明细对账单下载              "),
	BF05("BF05","清偿单据下载                    "),
	BF06("BF06","债券资料下载                    "),
	BF07("BF07","付息兑付单据下载                "),
	BF08("BF08","结算成员资料下载                "),
	BF09("BF09","代理统计成员资料下载            "),
	BF10("BF10","债券资料复合下载                "),
	BF11("BF11","付息兑付情况下载                "),
	BF12("BF12","债券选择权资料下载              "),
	BF13("BF13","实时现券结算行情(全价)下载      "),
	BF14("BF14","实时现券结算行情(净价)下载      "),
	BF15("BF15","实时质押式回购行情下载          "),
	BF16("BF16","实时买断式回购行情下载          "),
	BF17("BF17","实时行情统计下载                "),
	BF18("BF18","标准待偿期收益率曲线(历史)下载  "),
	BF19("BF19","任意待偿期收益率曲线数据下载    "),
	BF20("BF20","中债估值(历史)下载              "),
	BF21("BF21","老版标准待偿期收益率曲线数据下载"),
	BF22("BF22","老版任意待偿期收益率曲线数据下载"),
	BF23("BF23","老版收益率曲线明细数据下载      "),
	BF24("BF24","老版中债估值下载                "),
	BF25("BF25","持有债券市值下载                ");
	private String type;
	
	private String name;

	
	private Type(String type, String name) {
		this.type = type;
		this.name = name;
	}
	// 普通方法   
    public static String getName(String type) {   
        for (Type c : Type.values()) {   
            if (c.getType().equalsIgnoreCase(type)) {   
               return c.name;   
            }   
        }   
        return "";   
    }  
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.type+":"+this.name;
	}
}


	public BatchFunctionCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";

	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
