/**
 * 
 */
package com.singlee.financial.cdtc.pac;

/**
 * @author Fang
 *
 */
public class Exact19NumbericText extends IntField {
	private static final String regStr="^[0-9]{9}$";
	public Exact19NumbericText(String field) {
		super(field);
	}
	private static final long serialVersionUID = 1L;
	@Override
	public String toXmlString(){
		if(objectAsString().matches(regStr)){
			return toXml();
		}
		return "不合法数字";
	}
	public static void main(String[] args) {
		Exact19NumbericText exact19NumbericText = new Exact19NumbericText("cifNO");
		exact19NumbericText.setValue(001111111);
		System.out.println(exact19NumbericText.toXmlString());
	}

}
