/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Fang
 *
 */
public class EsbTimeOnlyField extends DateField {
	private static final long serialVersionUID = 1L;
	
	 private boolean includeMilliseconds = true;

	  private static TimeZone timezone = TimeZone.getTimeZone("ESB");
	  private static Calendar calendar;

	  public EsbTimeOnlyField(String field)
	  {
	    super(field, EsbTimeOnlyConverter.convert(createDate(), true));
	  }

	  protected EsbTimeOnlyField(String field, Date data)
	  {
	    super(field, EsbTimeOnlyConverter.convert(data, true));
	  }

	  protected EsbTimeOnlyField(String field, String data)
	  {
	    super(field, data);
	  }

	  public EsbTimeOnlyField(String field, boolean includeMilliseconds)
	  {
	    super(field, EsbTimeOnlyConverter.convert(createDate(), includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected EsbTimeOnlyField(String field, Date data, boolean includeMilliseconds)
	  {
	    super(field, EsbTimeOnlyConverter.convert(data, includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected EsbTimeOnlyField(String field, String data, boolean includeMilliseconds)
	  {
	    super(field, data);
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  boolean showMilliseconds() {
	    return this.includeMilliseconds;
	  }

	  public void setValue(Date value) {
	    setValue(EsbTimeOnlyConverter.convert(value, true));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(EsbTimeOnlyConverter.convert(value, true));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }
}
