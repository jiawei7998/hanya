package com.singlee.financial.cdtc.httpclient;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;

public class HttpClientManagerDaoImpl implements HttpClientManagerDao {

	private  HttpClient httpclient;   
	  
    public void setHttpclient(HttpClient httpclient) {   
        this.httpclient = httpclient;   
    }  
	
	/**
	 * @return the httpclient
	 */
	public HttpClient getHttpclient() {
		return httpclient;
	}
	/**  
     * 获取本机IP地址  
     *   
     * @return  
     */  

	@Override
	public String getLocalIpAddr() throws Exception {
		 try {   
	            Enumeration<?> netInterfaces = NetworkInterface.getNetworkInterfaces();   
	            InetAddress ip = null;   
	            String ipAddr = null;   
	            while (netInterfaces.hasMoreElements()) {   
	                NetworkInterface ni = (NetworkInterface) netInterfaces.nextElement();   
	                ip = (InetAddress) ni.getInetAddresses().nextElement();   
	                if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress() && ip.getHostAddress().indexOf(":") == -1) {   
	                    ipAddr = ip.getHostAddress();   
	                    break;   
	                }   
	            }   
	            return ipAddr;   
	        } catch (SocketException e) {   
	            e.printStackTrace();   
	            return null;   
	        } catch (Exception e) {   
	            e.printStackTrace();   
	            return null;   
	        }   

	}
	/**  
     * 获取远程客户端IP地址  
     *   
     * @param request  
     * @return  
     */  

	@Override
	public String getRemoteIpAddr(HttpServletRequest request) throws Exception {
		String ip = request.getHeader("X-Forwarded-For");   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("Proxy-Client-IP");   
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {  
            ip = request.getHeader("WL-Proxy-Client-IP");   
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))  {  
            ip = request.getHeader("HTTP_CLIENT_IP");   
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip))  {  
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");   
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getRemoteAddr();   
        }
        if ("0:0:0:0:0:0:0:1".equals(ip.trim()))   { 
            ip = "server";   
        }
        // 判断请求是否是本机发出,如果是本机发出,那么就获取本机地址   
        if ("127.0.0.1".equals(ip) || "localhost".equalsIgnoreCase(ip)) {
            ip = getLocalIpAddr();   
        }
        return ip;   

	}
	/**  
     * 以get方式发送http请求  
     *   
     * @param url  
     * @return  
     */  
	@Override
	public boolean isActive(String url) throws Exception {
		
		 boolean flag = false;   
	        GetMethod getMethod = new GetMethod(url);   
	        try {   
	            getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpRequestRetryHandler());   
	            int statusCode = httpclient.executeMethod(getMethod);   
	            if(statusCode==200){   
	                flag = true;   
	            }   
	            return flag;   
	        } catch (Exception e) {   
	            e.printStackTrace();   
	            return flag;   
	        }   
	        finally{   
	            getMethod.releaseConnection();   
	        }   
	}

    /**  
     * 判断某回调地址是否是指定的网关地址  
     *   
     * @param notifyUrl  
     * @param getwayList  
     * @return true 是网关 false不是网关地址  
     */  

	
	@Override
	public boolean isLocalNotifyUrl(String notifyUrl, List<?> getwayList)throws Exception {
		boolean flag = false;   
        for (Object object : getwayList) {   
            String getway = (String) object;   
            if (notifyUrl.toLowerCase().contains(getway)) {   
                flag = true;   
                break;   
            }   
        }   
        return flag;   

	}
	/**  
     * 验证请求是否是本机发出  
     *   
     * @param request  
     *            true本机发出 false非本机发出  
     * @return  
     */  

	@Override
	public boolean isRequestFromSelf(HttpServletRequest request)
			throws Exception {
		
		if (getRemoteIpAddr(request).equals(getLocalIpAddr()))  { 
            return true;   }
        else  {
            return false;}   

	}
	/**  
     * 以get方式发送http请求  
     *   
     * @param url  
     * @return  
     */  
	@Override
	public String sendRequest(String url) throws Exception {
		
		GetMethod getMethod = new GetMethod(url);   
        try {   
            getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpMethodRetryHandler());   
//          httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(6000);   
//          getMethod.getParams().setParameter(HttpMethodParams.SO_TIMEOUT,6000);   
            httpclient.executeMethod(getMethod);   
            return getMethod.getResponseBodyAsString();   
        } catch (Exception e) {   
            e.printStackTrace();   
            return "FAIL";   
        }   
        finally{   
            getMethod.releaseConnection();   
        }   

	}
	/**  
     * 以post方式发送http请求  
     *   
     * @param url  
     * @return  
     */  
	@Override
	public int sendRequestAsPost(String url) throws Exception {
		
		PostMethod postMethod = new PostMethod(url);   
        try {   
            postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpMethodRetryHandler());   
            httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(1000);   
            postMethod.getParams().setParameter(HttpMethodParams.SO_TIMEOUT,1000);   
  
            int statusCode = httpclient.executeMethod(postMethod);   
            return statusCode;   
        } catch (Exception e) {   
            e.printStackTrace();   
            return 500;   
        }   
        finally{   
            postMethod.releaseConnection();   
        }   

	}
	@Override
	public HashMap<String, Object> sendXmlToRequest(String url, String xmlString)
			throws Exception {
		PostMethod postMethod = null;
		InputStream inputStream = null;
		HashMap<String, Object> resultHashMap = new HashMap<String, Object>();
		try {

			httpclient.getHttpConnectionManager().getParams().setConnectionTimeout(30000);
			httpclient.getHttpConnectionManager().getParams().setSoTimeout(30000);
			postMethod = new PostMethod(url);
			postMethod.setRequestEntity(new StringRequestEntity(xmlString, "text/xml","UTF-8"));
			//(xmlString.getBytes("UTF-8").toString())		 
			//网上的以文件形式POST的方式
	        /*
	         * File input = new File("D:\\01\\simpass\\auth.xml"); try {
	         * post.setRequestBody(new FileInputStream(input)); if (input.length() <
	         * Integer.MAX_VALUE) post.setRequestContentLength(input.length()); else
	         * post.setRequestContentLength(EntityEnclosingMethod.CONTENT_LENGTH_CHUNKED);
	         *  // 指定请求内容的类型 post.setRequestHeader("Content-type", "text/xml;
	         * charset=UTF-8"); } catch (FileNotFoundException e) { // TODO
	         * Auto-generated catch block e.printStackTrace(); }
	         */

			//postMethod.setRequestBody(new NameValuePair[]{name}); 
			// 设置成了默认的恢复策略，在发生异常时候将自动重试3次，在这里你也可以设置成自定义的恢复策略 
			postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,new DefaultHttpMethodRetryHandler());

			// 执行getMethod 

			int statusCode = httpclient.executeMethod(postMethod);
			resultHashMap.put("statusCode", statusCode);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Call 4A check token method failed: "+ postMethod.getStatusLine());
			}
			inputStream = postMethod.getResponseBodyAsStream();
			
			byte[] str = new byte[1];
			 
			 int  i = inputStream.read(str);
			 
			 List<Byte> list = new ArrayList<Byte>();
			 
			 list.add(str[0]);
			 
			 while(i!=-1 && inputStream.available()!=0)
			 {
				 i = inputStream.read(str);
				 
				 list.add(str[0]);
			 }
			 byte[] total = new byte[list.size()];
			 int count = 0;
			 for(byte a : list)
			 {
				 total[count++] = a;
			 }
			//String rsp = postMethod.getResponseBodyAsString();
			//System.out.println(rsp);
			resultHashMap.put("Bytes", total); //中文环境
			//resultHashMap.put("Bytes", XmlFormat.format(new String(total)).getBytes("UTF-8"));//英文环境
			//System.out.println(XmlFormat.format(new String(total,"UTF-8")));
			//System.out.println(XmlFormat.format(new String(total)));
			// 解析应答报文 
			//TODO 

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (postMethod != null) {
				postMethod.releaseConnection();
			}
			if(null != inputStream)
			{
				inputStream.close();
			}
		}
		return resultHashMap;
	}   

}
