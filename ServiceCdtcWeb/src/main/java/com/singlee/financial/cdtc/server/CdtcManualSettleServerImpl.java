package com.singlee.financial.cdtc.server;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cdc.dmsg.client.DClient;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.financial.cdtc.CdtcManualSettleServer;
import com.singlee.financial.cdtc.cnbondpackage.CdtcBankAccount;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSysExceptionAdviceV01;
import com.singlee.financial.cdtc.dao.CdtcRequestMessageCreateDao;
import com.singlee.financial.cdtc.dao.DClientDao;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.util.ManualSettleLoadMsg;
import com.singlee.financial.cdtc.util.XmlFormat;
import com.singlee.financial.pojo.component.RetStatusEnum;

/***
 * 
 * 指令查询
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcManualSettleServerImpl implements CdtcManualSettleServer {
	private Logger logger = LoggerFactory.getLogger(CdtcManualSettleServerImpl.class);
	@Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;
	@Autowired
	CdtcRequestMessageCreateDao cdtcRequestMessageCreateDao;

	@Autowired
	DClientDao dClientDao;

	/***
	 * 指令批量查询-列表页面
	 */
	@Override
	public List<BtchQryRslt> getManualSettleListQuery(Map<String, Object> map) throws Exception {
		List<BtchQryRslt> btchqList = new ArrayList<BtchQryRslt>();
		// 查询日期
		String queryDate = (String) map.get("qryCondDt");
		if ("".equals(queryDate) || queryDate == null) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			queryDate = sdf.format(new Date());
		}

		SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS03AndBJ0300(CdtcBankAccount.BANK_ACCOUNT, queryDate);
		StringBuffer xmlBuffer = new StringBuffer();
		settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);

		logger.info(XmlFormat.format(xmlBuffer.toString()));
		logger.info("开始发送[指令批量查询]报文");
		DClient dClient = new DClient(wasIp);
		String backXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
		logger.info("回执报文:\n" + backXml);
		if (backXml != null) {
			logger.info("发送报文返回成功");
			SAXReader reader = new SAXReader();
			org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));
			String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
			if (null != type && "CSBS.006.001.01".equalsIgnoreCase(type)) {// 结算业务批量查询状态报告
				logger.info("消息类型：结算业务批量查询状态报告[CSBS.006.001.01]");
				try {
					btchqList = cdtcRequestMessageCreateDao.transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(backXml);

					// 添加查询条件
					String sInstrIdStr = ((String) map.get("sInstrId")).trim();
					String sTxIdStr = ((String) map.get("sTxId")).trim();
					String bizTpStr = ((String) map.get("bizTp")).trim();
					String instrStsStr = ((String) map.get("instrSts")).trim();

					Map<String, String> bizTpMap = null;
					if (bizTpStr.length() > 1) {
						bizTpMap = new HashMap<String, String>();
						for (String str : bizTpStr.split("\\,")) {
							bizTpMap.put(str, str);
						}
					} else {
						bizTpStr = null;
					}

					Map<String, String> instrStsStrMap = null;
					if (instrStsStr.length() > 1) {
						instrStsStrMap = new HashMap<String, String>();
						for (String str : instrStsStr.split("\\,")) {
							instrStsStrMap.put(str, str);
						}
					} else {
						instrStsStr = null;
					}

					List<BtchQryRslt> tmpBtchqList = new ArrayList<BtchQryRslt>();
					for (BtchQryRslt obj : btchqList) {
						if (!"".equals(sInstrIdStr)) {
							if (obj.getInstrId() == null || obj.getInstrId().trim().length() == 0 || !obj.getInstrId().contains(sInstrIdStr)) {
								continue;}
						}
						if (!"".equals(sTxIdStr)) {
							if (obj.getTxId() == null || obj.getTxId().trim().length() == 0 || !obj.getTxId().contains(sTxIdStr)) {
								continue;}
						}
						if (bizTpStr != null) {
							if (bizTpMap.get(obj.getBizTp()) == null) {
								continue;}
						}
						if (instrStsStr != null) {
							if (instrStsStrMap.get(obj.getInstrSts()) == null) {
								continue;}
						}
						tmpBtchqList.add(obj);
					}
					btchqList = tmpBtchqList;
				} catch (Exception e) {
					logger.error("查询异常，异常信息：" + e.getMessage());
					throw new RuntimeException("查询异常，异常信息：" + e.getMessage());
				}
			} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) { // 系统异常通知
				logger.info("消息类型：系统异常通知[CSBS.009.001.01]");
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
				if (spotSettlementInstruction.hasGroup("Document")) {
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for (Group group : groupsList) {
						StringBuffer xmlBufferT = new StringBuffer();
						group.toXmlForParent(xmlBufferT);
						systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
						XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
					}
				}
				if (null != systemExceptionAdviceV01) {
					String XcptnRtrCd = "";
					String XcptnRtrTx = "";
					if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
						XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
						XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
					}
					throw new RuntimeException("【CSBS.009.001.01系统异常通知:" + XcptnRtrCd + "," + XcptnRtrTx + "】");
				} else {
					throw new RuntimeException("【CSBS.009.001.01 系统异常:");
				}
			}
		}
		return btchqList;
	}

	/***
	 * 指令查询-详情页面
	 */
	@Override
	public Pair<SmtOutBean, ManualSettleDetail> getManualSettleDetailQuery(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		Pair<SmtOutBean, ManualSettleDetail> pair = new Pair<SmtOutBean, ManualSettleDetail>();
		SmtOutBean smtOutBean = new SmtOutBean();
		ManualSettleDetail manualSettleDetail = new ManualSettleDetail();

		String txFlowId = new SimpleDateFormat("yyyyMMdd").format(new Date()) + CdtcBankAccount.BANK_ACCOUNT + DateUtil.convertDateToYYYYMMDDHHMMSS(new Date());
		String qryCondId = String.valueOf(map.get("InstrId"));// 结算指令标识
		if (qryCondId == null || qryCondId == "") {
			smtOutBean.setRetStatus(RetStatusEnum.F);
			smtOutBean.setRetMsg("结算指令标识为空，无法查询详情！");
			pair.setLeft(smtOutBean);
			logger.info("结算指令标识为空，无法查询详情！");
			return pair;
		}

		SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS04AndBJ0300(txFlowId, CdtcBankAccount.BANK_ACCOUNT, qryCondId);
		StringBuffer xmlBuffer = new StringBuffer();
		settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);
		logger.info("开始发送结算详情报文！");
		// HashMap<String, String> resultHashMap = dClientDao.sendXmlMessageToCdtcForRequest(XmlFormat.format(xmlBuffer.toString()));
		DClient dClient = new DClient(wasIp);
		String backXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
		logger.info("回执报文\n" + backXml);
		if (backXml != null) {
			logger.info("发送结算详情报文返回成功");
			SAXReader reader = new SAXReader();
			org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));
			String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
			if (null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {

				// 业务类型
				String bizTp = xmlDoc.getRootElement().element("Document").element("SttlmDtl").element("BizTp").getText();
				if (bizTp == null || bizTp == "") {
					smtOutBean.setRetStatus(RetStatusEnum.F);
					smtOutBean.setRetMsg("业务类型为空，无法展示详情！");
					pair.setLeft(smtOutBean);
					logger.info("业务类型为空，无法展示详情！");
					return pair;
				}
				if ("BT01".equalsIgnoreCase(bizTp) || "BT00".equalsIgnoreCase(bizTp) || "BT04".equalsIgnoreCase(bizTp) || "BT05".equalsIgnoreCase(bizTp)) {
					manualSettleDetail = ManualSettleLoadMsg.zLDetail(backXml);
				}
				if ("BT02".equalsIgnoreCase(bizTp)) {
					manualSettleDetail = ManualSettleLoadMsg.zLRepoDetail(backXml);
				}
				if ("BT03".equalsIgnoreCase(bizTp)) {
					manualSettleDetail = ManualSettleLoadMsg.zLRepoMDDetail(backXml);
				}
				logger.info("查询手工结算详情成功！");
				smtOutBean.setRetStatus(RetStatusEnum.S);
				smtOutBean.setRetCode("CSBS.002.001.01");
				smtOutBean.setRetMsg("查询详情成功");

			} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) {
				logger.info("查询手工结算详情出现异常！");
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
				if (spotSettlementInstruction.hasGroup("Document")) {
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for (Group group : groupsList) {
						StringBuffer xmlBufferT = new StringBuffer();
						group.toXmlForParent(xmlBufferT);
						systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
						XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
					}
				}
				if (null != systemExceptionAdviceV01) {
					String XcptnRtrCd = "";
					String XcptnRtrTx = "";
					if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
						XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
						XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
					}
					smtOutBean.setRetStatus(RetStatusEnum.F);
					smtOutBean.setRetCode(XcptnRtrCd);
					smtOutBean.setRetMsg("【CSBS.009.001.01系统异常通知:" + XcptnRtrCd + "," + XcptnRtrTx + "】");
					logger.info("异常代码：" + XcptnRtrCd + ",异常信息：" + XcptnRtrTx);
				} else {
					smtOutBean.setRetStatus(RetStatusEnum.F);
					smtOutBean.setRetCode("CSBS.009.001.01");
					smtOutBean.setRetMsg("系统异常");
				}

			}
		}

		pair.setLeft(smtOutBean);
		pair.setRight(manualSettleDetail);

		return pair;
	}

	@Override
	public List<BtchQryRslt> getSettleList(Map<String, Object> map) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
