package com.singlee.financial.cdtc.pac;


/**
 *  CS00：成功
CS01：等券
CS02：等款
CS03：待生效
CS04：待履行
CS05：应履行
CS06：履行中
CS07：部分过户
CS08：现金了结
CS09：逾期完成
CS10：已清偿
CS11：失败
CS12：取消
CS13：撤消
CS14：作废

 * @author Administrator
 *
 */
public class ContractStatusCode extends StringField {
	public static enum Type {
	CS00("CS00","成功       "),
	CS01("CS01","等券       "),
	CS02("CS02","等款       "),
	CS03("CS03","待生效     "),
	CS04("CS04","待履行     "),
	CS05("CS05","应履行     "),
	CS06("CS06","履行中     "),
	CS07("CS07","部分过户   "),
	CS08("CS08","现金了结   "),
	CS09("CS09","逾期完成   "),
	CS10("CS10","已清偿     "),
	CS11("CS11","失败       "),
	CS12("CS12","取消       "),
	CS13("CS13","撤消       "),
	CS14("CS14","作废       ");
	private String type;
	
	private String name;

	
	private Type(String type, String name) {
		this.type = type;
		this.name = name;
	}
	// 普通方法   
    public static String getName(String type) {   
        for (Type c : Type.values()) {   
            if (c.getType().equalsIgnoreCase(type)) {   
               return c.name;   
            }   
        }   
        return "";   
    }  
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.type+":"+this.name;
	}
}
	public ContractStatusCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";

	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
