/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;

/**
 * @author Fang
 * 6.3.1通用结算指令请求报文结构表
 */
public class CommonDistributionSettlementInstruction extends Group {

	private static final long serialVersionUID = 1L;
	
	public static String type = "CSBS.001.001.01";
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("Msg", TransactionMsg.class);
		keyClassMap.put("OprtrFlg", TransactionOprtrFlg.class);
		keyClassMap.put("SttlmDtl", TransactionSttlmDtl.class);
		keyClassMap.put("GivInf", TransactionGivInf.class);
		keyClassMap.put("TakInf", TransactionTakInf.class);
		keyClassMap.put("Xtnsn", Extension.class);
		keyClassMap.putAll(TransactionMsg.keyClassMap);
		keyClassMap.putAll(TransactionOprtrFlg.keyClassMap);
		keyClassMap.putAll(TransactionSttlmDtl.keyClassMap);
		keyClassMap.putAll(TransactionGivInf.keyClassMap);
		keyClassMap.putAll(TransactionTakInf.keyClassMap);
		keyClassMap.putAll(Extension.keyClassMap);
	}
	
	public CommonDistributionSettlementInstruction(String tag) {

		super(tag, tag, new String[] { "Msg", "OprtrFlg","SttlmDtl","GivInf","TakInf","Xtnsn"});
	}
	public CommonDistributionSettlementInstruction(String tag,String belim) {

		super(tag, tag, new String[] { "Msg", "OprtrFlg","SttlmDtl","GivInf","TakInf","Xtnsn"});
	}
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

	public void set(TransactionMsg group) {
		addGroup(group);
	}
	public void set(TransactionOprtrFlg group){
		addGroup(group);
	}
	public void set(TransactionSttlmDtl group){
		addGroup(group);
	}
	public void set(TransactionGivInf group){
		addGroup(group);
	}
	public void set(TransactionTakInf group){
		addGroup(group);
	}
	public void set(Extension group){
		addGroup(group);
	}
	
	public static CommonDistributionSettlementInstruction content(Bean1 bean1){
		CommonDistributionSettlementInstruction common = new CommonDistributionSettlementInstruction("Document");
		/**********************************报文标识*********************************/
		TransactionMsg transactionMsg = new TransactionMsg("Msg");
		//交易流水标识
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("01");
		transactionMsg.set(max35Text);
		//经办人
		PartyIdentification partyIdentification = new PartyIdentification("Oprtr");
		Max35Text max35TextOprtr = new Max35Text("Nm");
		max35TextOprtr.setValue("A");
		partyIdentification.set(max35TextOprtr);
		transactionMsg.set(partyIdentification);
		//复核人
		PartyIdentification partyIdentificationB = new PartyIdentification("Chckr");
		Max35Text max35TextB = new Max35Text("Nm");
		max35TextB.setValue("B");
		partyIdentificationB.set(max35TextB);
		transactionMsg.set(partyIdentificationB);
		//优先级
		Priority4Code prty = new Priority4Code("Prty");
		prty.setValue(Priority4Code.Type.PT01.getType());
		transactionMsg.set(prty);
		//报文创建时间
		transactionMsg.set(new ISODateTime("CreDtTm", new Date()));
		
		/*********************************操作标识**************************/
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		//请求类别码
		RequestTypeCode reqTp = new RequestTypeCode("ReqTp");
		reqTp.setValue(RequestTypeCode.Type.BJ0100.getType());
		transactionOprtrFlg.set(reqTp);
		//操作码
		OperatorStatusCode operSc = new OperatorStatusCode("OprtrSts");
		operSc.setValue(OperatorStatusCode.Type.OS00.getType());
		transactionOprtrFlg.set(operSc);
	
		/*************************结算详情********************************/
		TransactionSttlmDtl transactionSttlmDtl = new TransactionSttlmDtl("SttlmDtl");
		//结算指令来源
		InstructionOriginCode ioc = new InstructionOriginCode("InstrOrgn");
		ioc.setValue(InstructionOriginCode.Type.IO01.getType());
		transactionSttlmDtl.set(ioc);
		//业务类别
		BusinessTypeCode btc = new BusinessTypeCode("BizTp");
		btc.setValue(BusinessTypeCode.Type.BT00.getType());
		transactionSttlmDtl.set(btc);
		//结算指令标识
		Exact12NumericText ent = new Exact12NumericText("InstrId");
		ent.setValue(123);
		transactionSttlmDtl.set(ent);
		//付券方账户
		AccountIdentificationAndName4 aian = new AccountIdentificationAndName4("GivAcct");
		Max35Text maxAian = new Max35Text("Nm");
		maxAian.setValue("62285500");
		aian.set(maxAian);
		Exact11Text e11Text = new Exact11Text("Id");
		e11Text.setValue("12345678901");
		aian.set(e11Text);
		transactionSttlmDtl.set(aian);
		//收券方账户
		AccountIdentificationAndName4 aian1 = new AccountIdentificationAndName4("TakAcct");
		Max35Text maxAian1 = new Max35Text("Nm");
		maxAian1.setValue("62280055");
		aian1.set(maxAian1);
		Exact11Text e11Text1 = new Exact11Text("Id");
		e11Text1.setValue("10987654321");
		aian1.set(e11Text1);
		transactionSttlmDtl.set(aian1);
		//结算方式1
		BondSettlementType2Code bstc = new BondSettlementType2Code("SttlmTp1");
		bstc.setValue(BondSettlementType2Code.Type.ST00.getType());
		transactionSttlmDtl.set(bstc);
		//结算方式2
		BondSettlementType2Code bstc2 = new BondSettlementType2Code("SttlmTp2");
		bstc2.setValue(BondSettlementType2Code.Type.ST01.getType());
		transactionSttlmDtl.set(bstc2);
		//债券1
		Bond3 bond1 = new Bond3("Bd1");
		Max30Text max30Text = new Max30Text("BdId");
		max30Text.setValue("STG00");
		bond1.set(max30Text);
		ISINIdentifier isin = new ISINIdentifier("ISIN");
		isin.setValue("121212121212");
		bond1.set(isin);
		Max35Text bondMax35Text = new Max35Text("BdShrtNm");
		bondMax35Text.setValue("债券简称...");
		bond1.set(bondMax35Text);
		FaceCurrencyAndAmount faceCurrencyAndAmount = new FaceCurrencyAndAmount("BdAmt","CNY");
		faceCurrencyAndAmount.setValue("11111234567890.99");
		bond1.set(faceCurrencyAndAmount);
		PercentageRate pgr = new PercentageRate("BdCollRate");
		pgr.setValue("10.123456%");
		bond1.set(pgr);
		AccruedInterestInformation accIf = new AccruedInterestInformation("AcrdIntrstInf");
		ValueCurrencyAndAmount valueCurrencyAndAmount = new ValueCurrencyAndAmount("AcrdIntrst","CNY");
		valueCurrencyAndAmount.setValue("34567890.99");
		accIf.set(valueCurrencyAndAmount);
		accIf.set(new ISODate("AcrdIntrstDt", new Date()));
		bond1.set(accIf);
		ValueCurrencyAndAmount vcaa = new ValueCurrencyAndAmount("Pric1","CNY");
		vcaa.setValue("345.99");
		bond1.set(vcaa);
		ValueCurrencyAndAmount vcaa1 = new ValueCurrencyAndAmount("Pric2","CNY");
		vcaa1.setValue("678.99");
		bond1.set(vcaa1);
		
		transactionSttlmDtl.set(bond1);
		//债券2
		Bond3 bond2 = new Bond3("Bd2");
		Max30Text max30Text1 = new Max30Text("BdId");
		max30Text1.setValue("STG00");
		bond2.set(max30Text1);
		ISINIdentifier isin1 = new ISINIdentifier("ISIN");
		isin1.setValue("121212121212");
		bond2.set(isin1);
		Max35Text bondMax35Text1 = new Max35Text("BdShrtNm");
		bondMax35Text1.setValue("债券简称...");
		bond2.set(bondMax35Text1);
		FaceCurrencyAndAmount faceCurrencyAndAmount1 = new FaceCurrencyAndAmount("BdAmt","CNY");
		faceCurrencyAndAmount1.setValue("11111234567890.99");
		bond2.set(faceCurrencyAndAmount1);
		PercentageRate pgr1 = new PercentageRate("BdCollRate");
		pgr1.setValue("10.123456%");
		bond2.set(pgr1);
		AccruedInterestInformation accIf1 = new AccruedInterestInformation("AcrdIntrstInf");
		ValueCurrencyAndAmount valueCurrencyAndAmount1 = new ValueCurrencyAndAmount("AcrdIntrst","CNY");
		valueCurrencyAndAmount1.setValue("34567890.99");
		accIf1.set(valueCurrencyAndAmount1);
		accIf1.set(new ISODate("AcrdIntrstDt", new Date()));
		bond2.set(accIf1);
		ValueCurrencyAndAmount vcaa2 = new ValueCurrencyAndAmount("Pric1","CNY");
		vcaa2.setValue("345.99");
		bond2.set(vcaa2);
		ValueCurrencyAndAmount vcaa3 = new ValueCurrencyAndAmount("Pric2","CNY");
		vcaa3.setValue("678.99");
		bond2.set(vcaa3);
		transactionSttlmDtl.set(bond2);
		//金额1
		CommonCurrencyAndAmount ccaa1 = new CommonCurrencyAndAmount("Val1", "CNY");
		ccaa1.setValue("1234567890.99");
		transactionSttlmDtl.set(ccaa1);
		//金额2
		CommonCurrencyAndAmount ccaa2 = new CommonCurrencyAndAmount("Val2", "CNY");
		ccaa2.setValue("1234567890.99");
		transactionSttlmDtl.set(ccaa2);
		//金额3
		CommonCurrencyAndAmount ccaa3 = new CommonCurrencyAndAmount("Val3", "CNY");
		ccaa3.setValue("1234567890.99");
		transactionSttlmDtl.set(ccaa3);
		//金额4
		CommonCurrencyAndAmount ccaa4 = new CommonCurrencyAndAmount("Val4", "CNY");
		ccaa4.setValue("1234567890.99");
		transactionSttlmDtl.set(ccaa4);
		//券面总额合计
		FaceCurrencyAndAmount fcaa = new FaceCurrencyAndAmount("AggtFaceAmt", "CNY");
		fcaa.setValue("1234567890.99");
		transactionSttlmDtl.set(fcaa);
		//利率
		PercentageRate pr = new PercentageRate("Rate");
		pr.setValue("10.123456%");
		transactionSttlmDtl.set(pr);
		//期限
		Max35NumericText mnt = new Max35NumericText("Terms");
		mnt.setValue(12345);
		transactionSttlmDtl.set(mnt);
		//日期1
		transactionSttlmDtl.set(new ISODate("Dt1", new Date()));
		//日期2
		transactionSttlmDtl.set(new ISODate("Dt2", new Date()));
		//结算担保1
		SettlementGuarantee seg = new SettlementGuarantee("SttlmGrte1");
		GuaranteeTypeCode gtc = new GuaranteeTypeCode("GrteTp");
		gtc.setValue(GuaranteeTypeCode.Type.DT01.getType());
		seg.set(gtc);
		CollateralBond1 collb = new CollateralBond1("GrteBd");
		Max30Text max30Textcollb = new Max30Text("BdId");
		max30Textcollb.setValue("STG00");
		collb.set(max30Textcollb);
		ISINIdentifier isincollb = new ISINIdentifier("ISIN");
		isincollb.setValue("121212121212");
		collb.set(isincollb);
		Max35Text bondMax35Textcollb = new Max35Text("BdShrtNm");
		bondMax35Textcollb.setValue("债券简称...");
		collb.set(bondMax35Textcollb);
		FaceCurrencyAndAmount faceCurrencyAndAmountcollb = new FaceCurrencyAndAmount("BdAmt","CNY");
		faceCurrencyAndAmountcollb.setValue("11111234567890.99");
		collb.set(faceCurrencyAndAmountcollb);
		seg.set(collb);
		CashLocationCode clcode = new CashLocationCode("CshLctn");
		clcode.setValue(CashLocationCode.Type.CL01.getType());
		seg.set(clcode);
		CommonCurrencyAndAmount ccaa = new CommonCurrencyAndAmount("GrtePricAmt","CNY");
		ccaa.setValue("1234567890.99");
		seg.set(ccaa);
		transactionSttlmDtl.set(seg);
		//结算担保2
		SettlementGuarantee seg1 = new SettlementGuarantee("SttlmGrte2");
		GuaranteeTypeCode gtcseg1 = new GuaranteeTypeCode("GrteTp");
		gtcseg1.setValue(GuaranteeTypeCode.Type.DT01.getType());
		seg1.set(gtcseg1);
		CollateralBond1 collbseg1 = new CollateralBond1("GrteBd");
		Max30Text max30Textcollbseg1 = new Max30Text("BdId");
		max30Textcollbseg1.setValue("STG00");
		collbseg1.set(max30Textcollbseg1);
		ISINIdentifier isincollbseg1 = new ISINIdentifier("ISIN");
		isincollbseg1.setValue("121212121212");
		collbseg1.set(isincollbseg1);
		Max35Text bondMax35Textcollbseg1 = new Max35Text("BdShrtNm");
		bondMax35Textcollbseg1.setValue("债券简称...");
		collbseg1.set(bondMax35Textcollbseg1);
		FaceCurrencyAndAmount faceCurrencyAndAmountcollbseg1 = new FaceCurrencyAndAmount("BdAmt","CNY");
		faceCurrencyAndAmountcollbseg1.setValue("11111234567890.99");
		collbseg1.set(faceCurrencyAndAmountcollbseg1);
		seg1.set(collbseg1);
		CashLocationCode clcodeseg1 = new CashLocationCode("CshLctn");
		clcodeseg1.setValue(CashLocationCode.Type.CL01.getType());
		seg1.set(clcodeseg1);
		CommonCurrencyAndAmount ccaaseg1 = new CommonCurrencyAndAmount("GrtePricAmt","CNY");
		ccaaseg1.setValue("1234567890.99");
		seg1.set(ccaaseg1);
		transactionSttlmDtl.set(seg1);
		//原结算指令标识
		Exact12NumericText ex12NT = new Exact12NumericText("OrgnlInstrId");
		ex12NT.setValue(1234567);
		transactionSttlmDtl.set(ex12NT);
		//原结算合同标识
		Exact9NumericText ex9NT = new Exact9NumericText("OrgnlCtrctId");
		ex9NT.setValue(7654321);
		transactionSttlmDtl.set(ex9NT);
		//转托管申请人
		PartyIdentification30 pif30 = new PartyIdentification30("CrossTrfAplt");
		Max70Text max70Textpif = new Max70Text("Nm");
		max70Textpif.setValue("B");
		pif30.set(max70Textpif);
		transactionSttlmDtl.set(pif30);
		//业务标识号
		Max20Text m20T = new Max20Text("TxId");
		m20T.setValue("AABB");
		transactionSttlmDtl.set(m20T);
		//经办人
		PartyIdentification pifOprtr = new PartyIdentification("Oprtr");
		Max35Text max35TextpifOprtr = new Max35Text("Nm");
		max35TextpifOprtr.setValue("C");
		pifOprtr.set(max35TextpifOprtr);
		transactionSttlmDtl.set(pifOprtr);
		//复核人
		PartyIdentification pifOChckr = new PartyIdentification("Chckr");
		Max35Text max35TextpifChckr = new Max35Text("Nm");
		max35TextpifChckr.setValue("D");
		pifOChckr.set(max35TextpifChckr);
		transactionSttlmDtl.set(pifOChckr);
		//确认人
		PartyIdentification pifCnfrmr = new PartyIdentification("Cnfrmr");
		Max35Text max35TextpifCnfrmr = new Max35Text("Nm");
		max35TextpifCnfrmr.setValue("E");
		pifCnfrmr.set(max35TextpifCnfrmr);
		transactionSttlmDtl.set(pifCnfrmr);
		/*********************************付方信息**********************/
		TransactionGivInf transactionGivInf = new TransactionGivInf("GivInf");
		//名称
		Max70Text max70TextGviInf = new Max70Text("Nm");
		max70TextGviInf.setValue("ABC");
		transactionGivInf.set(max70TextGviInf);
		//自定义标识
		GenericIdentification4 genid = new GenericIdentification4("PrtryId");
		Max35Text maxid = new Max35Text("Id");
		maxid.setValue("SS123");
		genid.set(maxid);
		Max35Text maxIdIp = new Max35Text("IdTp");
		maxIdIp.setValue("DD123");
		genid.set(maxIdIp);
		transactionGivInf.set(genid);
		//代理机构
		AgentParty agentParty = new AgentParty("AgtPty");
		AgentPartyTypeCode agtPtyTP = new AgentPartyTypeCode("AgtPtyTP");
		agtPtyTP.setValue(AgentPartyTypeCode.Type.AP00.getType());
		agentParty.set(agtPtyTP);
		PartyIdentification30 agtPtyId = new PartyIdentification30("AgtPtyId");
		Max70Text agtPtyIdNm = new Max70Text("Nm");
		agtPtyIdNm.setValue("F");
		agtPtyId.set(agtPtyIdNm);
		GenericIdentification4 prtryId = new GenericIdentification4("PrtryId");
		Max35Text prtryIdId = new Max35Text("Id");
		prtryIdId.setValue("SFH000");
		Max35Text paityIdTp = new Max35Text("IdTp");
		paityIdTp.setValue("IdTp000");
		prtryId.set(prtryIdId);
		prtryId.set(paityIdTp);
		agtPtyId.set(prtryId);
		agentParty.set(agtPtyId);
		transactionGivInf.set(agentParty);
		/***************************收方信息***********************/
		TransactionTakInf transactionTakInf = new TransactionTakInf("TakInf");
		//名称
		Max70Text max70TextTakInf = new Max70Text("Nm");
		max70TextTakInf.setValue("EFG");
		transactionTakInf.set(max70TextTakInf);
		//自定义标识
		GenericIdentification4 genidTakInf = new GenericIdentification4("PrtryId");
		Max35Text maxidTakInf = new Max35Text("Id");
		maxidTakInf.setValue("SS123");
		genidTakInf.set(maxidTakInf);
		Max35Text maxIdIpTakInf = new Max35Text("IdTp");
		maxIdIpTakInf.setValue("DD123");
		genidTakInf.set(maxIdIpTakInf);
		transactionTakInf.set(genidTakInf);
		//代理机构
		AgentParty agentPartyTakInf = new AgentParty("AgtPty");
		AgentPartyTypeCode agtPtyTPTakInf = new AgentPartyTypeCode("AgtPtyTP");
		agtPtyTPTakInf.setValue(AgentPartyTypeCode.Type.AP01.getType());
		agentPartyTakInf.set(agtPtyTPTakInf);
		PartyIdentification30 agtPtyIdTakInf = new PartyIdentification30("AgtPtyId");
		Max70Text agtPtyIdNmTakInf = new Max70Text("Nm");
		agtPtyIdNmTakInf.setValue("H");
		agtPtyIdTakInf.set(agtPtyIdNmTakInf);
		GenericIdentification4 prtryIdTakInf = new GenericIdentification4("PrtryId");
		Max35Text prtryIdIdTakInf = new Max35Text("Id");
		prtryIdIdTakInf.setValue("SFH111");
		Max35Text paityIdTpTakInf = new Max35Text("IdTp");
		paityIdTpTakInf.setValue("IdTp111");
		prtryIdTakInf.set(prtryIdIdTakInf);
		prtryIdTakInf.set(paityIdTpTakInf);
		agtPtyIdTakInf.set(prtryIdTakInf);
		agentPartyTakInf.set(agtPtyIdTakInf);
		transactionTakInf.set(agentPartyTakInf);
		/********************************拓展项*****************************/
		Extension extension = new Extension("Xtnsn");
		Max2048Text max2048Text = new Max2048Text("XtnsnTxt");
		max2048Text.setValue("通用结算指令请求报文完毕");
		extension.set(max2048Text);
		
		common.set(transactionMsg);
		common.set(transactionOprtrFlg);
		common.set(transactionSttlmDtl);
		common.set(transactionGivInf);
		common.set(transactionTakInf);
		common.set(extension);
		return common;
	}
	
	public static void main(String[] args) throws Exception{
		
		StringBuffer stringBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(stringBuffer);

		System.out.println(XmlFormat.format(XmlFormat.format(stringBuffer.toString())));
		
		
		// 保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat
				.format(stringBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out = new FileOutputStream("./cnbondxml/"
				+ CommonDistributionSettlementInstruction.type + ".xml");
		XMLOutputter outputter = new XMLOutputter();
		// 如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的
		outputter.output((Document) doc, out);
		out.close();

		SAXReader reader = new SAXReader();
		/**
		 * 反解析报文
		 */
		CommonDistributionSettlementInstruction commondssReport1 = new CommonDistributionSettlementInstruction(
				"Document");
		org.dom4j.Document docXml = reader.read(new File("./cnbondxml/"
				+ CommonDistributionSettlementInstruction.type + ".xml"));
		getElementList(docXml.getRootElement(), commondssReport1);
		
		StringBuffer xmlBuffer = new StringBuffer();
		commondssReport1.toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
	}
	
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * @param element
	 * @param common
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("unchecked")
	public static void getElementList(Element element,CommonDistributionSettlementInstruction common)throws Exception,
	NoSuchMethodException{
		List<Element> elements = element.elements();
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = iterator.next();
			List<Element> elements2 = element2.elements();
			if(elements2.size() >= 0){
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
				if(common.isHasTag(StringUtils.trimToEmpty(element2.getName()))){
					if(element2.elements().size() > 0){
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
						common.addGroup(getGroups(element2, common));
					}else{
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
						Class<?>[] parameterTypes = {String.class,Object.class};
						Constructor<?> constructor = CommonDistributionSettlementInstruction.keyClassMap
						.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
						Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element.getText())};
						Object object = constructor.newInstance(parameters);
						common.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
					}
				}
			}
		}
	}
	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception反解析报文
	 */
	public static Group getGroups(Element element,
			CommonDistributionSettlementInstruction common)
			throws NoSuchMethodException, Exception {
		//Group group = new Group(StringUtils.trimToEmpty(element.getName()),"");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = CommonDistributionSettlementInstruction.keyClassMap.get(StringUtils.trimToEmpty(element.getName())).getConstructor(parameterTypes2);
		Object[] parameters2 = {StringUtils.trimToEmpty(element.getName()),""};
		Group group = (Group) constructor2.newInstance(parameters2);
		
		System.out.println("************************************");
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
			if(element2.elements().size() > 0){
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
				group.addGroup(getGroups(element2, common));
			}else{
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
				Class<?>[] parameterTypes = {String.class,Object.class};
				Constructor<?> constructor = CommonDistributionSettlementInstruction.keyClassMap
				.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
				Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element2.getText())};
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
			}
		}
		System.out.println("************************************");
		return group;
	}
	
}
