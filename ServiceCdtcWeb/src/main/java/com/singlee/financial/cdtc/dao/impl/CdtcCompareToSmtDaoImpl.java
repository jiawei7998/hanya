package com.singlee.financial.cdtc.dao.impl;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cdc.dmsg.client.DClient;
import com.singlee.financial.bean.CdtcAcctTotalBean;
import com.singlee.financial.cdtc.cnbondpackage.AnalyzSafeKeepAccountReport;
import com.singlee.financial.cdtc.dao.CdtcCompareToSmtDao;
import com.singlee.financial.cdtc.mapper.CdtcCompareToSmtMapper;
import com.singlee.financial.cdtc.pac.AccountIdentificationAndName7;
import com.singlee.financial.cdtc.pac.AccountStatementType;
import com.singlee.financial.cdtc.pac.Exact11Text;
import com.singlee.financial.cdtc.pac.ISODate;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.OperatorStatusCode;
import com.singlee.financial.cdtc.pac.Priority4Code;
import com.singlee.financial.cdtc.pac.QueryDate;
import com.singlee.financial.cdtc.pac.RequestTypeCode;
import com.singlee.financial.cdtc.pac.SafekeepingAccountStatementQueryRequestV;
import com.singlee.financial.cdtc.pac.TransactionMsg;
import com.singlee.financial.cdtc.pac.TransactionOprtrFlg;
import com.singlee.financial.cdtc.pac.TransactionQryItm;
import com.singlee.financial.cdtc.server.CdtcSecCheckAccountServerImpl;
import com.singlee.financial.cdtc.util.XmlFormat;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcCompareToSmtDaoImpl implements CdtcCompareToSmtDao {

	private static Logger logger = LoggerFactory.getLogger(CdtcSecCheckAccountServerImpl.class);

	@Autowired
	CdtcCompareToSmtMapper cdtcCompareToSmtMapper;

	@Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;
	
	@Override
	public LinkedHashMap<Integer, CdtcAcctTotalBean> getSecCheckingMap(Map<String, Object> params) throws Exception {

		// 查询总对账单返回报文
		String backXml = sendMessageCdtc(params);

		if (backXml == null || "".equals(backXml)) {
			throw new RuntimeException("CDTC查询总对账单返回报文为空");
		}
		SAXReader reader = new SAXReader();
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(backXml.getBytes("UTF-8"));
		Document xmlDoc = reader.read(byteArrayInputStream);
		xmlDoc.setXMLEncoding("UTF-8");
		LinkedHashMap<Integer, CdtcAcctTotalBean> cdtcHashMap = null;
		System.out.println(xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText());
		logger.info("消息类型MsgTp：" + xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText());
		if (!"CSBS.008.001.01".equals(xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText())) {
			logger.info("消息类型不为：CSBS.008.001.01");
			String errorMsg = xmlDoc.getRootElement().element("Document").element("Xtnsn").element("XtnsnTxt").getText();
			logger.info("CDTC查询总对账单错误，错误信息:" + errorMsg);
			throw new RuntimeException("CDTC查询总对账单错误，错误信息：" + errorMsg);
		} else {
			logger.info("消息类型：CSBS.008.001.01");
			cdtcHashMap = AnalyzSafeKeepAccountReport.analyzCSBSSafekeepAccountMsg(xmlDoc.getRootElement().element("Document").element("StmtRpt"));
		}
		return cdtcHashMap;
	}

	// 发送报文 CSBS.008.001.01总对账单请求报告
	private String sendMessageCdtc(Map<String, Object> map) throws Exception {

		SafekeepingAccountStatementQueryRequestV accountStatementQueryRequestV = new SafekeepingAccountStatementQueryRequestV("Document");

		TransactionMsg transactionMsg = new TransactionMsg("Msg");
		// 交易流水标识
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("30004");
		transactionMsg.set(max35Text);
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT02.getType());
		transactionMsg.set(priority4Code);
		transactionMsg.set(new ISODateTime("CreDtTm", new Date()));

		/************************* 操作标识 *******************/
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		// 请求类别码
		RequestTypeCode reqTp = new RequestTypeCode("ReqTp");
		reqTp.setValue(RequestTypeCode.Type.BJ0400.getType());
		transactionOprtrFlg.set(reqTp);
		// 操作码
		OperatorStatusCode operSc = new OperatorStatusCode("OprtrSts");
		operSc.setValue(OperatorStatusCode.Type.OS07.getType());
		transactionOprtrFlg.set(operSc);

		/************************* 查询条目 *************************/
		TransactionQryItm transactionQryItm = new TransactionQryItm("QryItm");
		// 查询日期
		QueryDate qryDt = new QueryDate("QryDt");
		qryDt.set(new ISODate("StartDt", (String) map.get("queryDate")));
		qryDt.set(new ISODate("EndDt", (String) map.get("queryDate")));
		transactionQryItm.set(qryDt);

		AccountIdentificationAndName7 sfkpgAcctBdQryn = new AccountIdentificationAndName7("SfkpgAcctBdQry");
		Exact11Text id = new Exact11Text("Id");
		id.setValue((String) map.get("acctText"));
		sfkpgAcctBdQryn.set(id);
		AccountStatementType accStTp = new AccountStatementType("AccStTp");
		accStTp.setValue(AccountStatementType.Type.AS00.getType());
		sfkpgAcctBdQryn.set(accStTp);

		transactionQryItm.set(sfkpgAcctBdQryn);
		accountStatementQueryRequestV.set(transactionMsg);
		accountStatementQueryRequestV.set(transactionOprtrFlg);
		accountStatementQueryRequestV.set(transactionQryItm);

		StringBuffer buffer = new StringBuffer();
		accountStatementQueryRequestV.toXmlForSuperParent(buffer);
		String xmlbuffer = XmlFormat.format(buffer.toString());
		System.out.println(xmlbuffer);

		DClient dClient = new DClient(wasIp);
		String backXml = dClient.sendMsg(xmlbuffer);
		logger.info("总对账单查询总返回报文backXml为：" + backXml);

		return backXml;
	}


	@Override
	public int insertCompareToSmtByList(List<CdtcAcctTotalBean> list) {
		return cdtcCompareToSmtMapper.insertCompareToSmtByList(list);
	}


	@Override
	public int deleteCompareToSmt() {
		return cdtcCompareToSmtMapper.deleteCompareToSmt();
	}

	@Override
	public String checkingSummitData(Map<String, Object> map) {
		map.put("O_RETCODE", "");
		map.put("O_RETMSG", "");
		cdtcCompareToSmtMapper.procSummitSecPos(map);
		if("999".equals(map.get("O_RETCODE"))){
			map.put("O_RETCODE", "");
			map.put("O_RETMSG", "");
			cdtcCompareToSmtMapper.checkingSummitData(map);
		}
		return	(String) map.get("O_RETCODE");
	}
}
