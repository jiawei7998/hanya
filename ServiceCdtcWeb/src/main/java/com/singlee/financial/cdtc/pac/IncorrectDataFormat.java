package com.singlee.financial.cdtc.pac;

public class IncorrectDataFormat  extends Exception
{
	  private static final long serialVersionUID = 1L;
	  public final String field;
	  public final String data;

	  public IncorrectDataFormat(String field, String data)
	  {
	    this(field, data, "Field [" + field + "] contains badly formatted data.");
	  }

	  public IncorrectDataFormat(String message)
	  {
	    this("0", null, message);
	  }

	  private IncorrectDataFormat(String field, String data, String message) {
	    super(message);
	    this.field = field;
	    this.data = data;
	  }
	}
