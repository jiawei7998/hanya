/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 */
public class TransactionQryItm extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("QryTp", Field.class);
		keyClassMap.put("QryDt", QueryDate.class);
		keyClassMap.put("SfkpgAcctBdQry", AccountIdentificationAndName7.class);
		keyClassMap.putAll(QueryDate.keyClassMap);
		keyClassMap.putAll(AccountIdentificationAndName7.keyClassMap);
	}
	
	public TransactionQryItm(String tag){
		super(tag,tag,new String[]{"QryTp","QryDt","SfkpgAcctBdQry"});
	}
	public TransactionQryItm(String tag,String delim){
		super(tag,tag,new String[]{"QryTp","QryDt","SfkpgAcctBdQry"});
	}
	
	public void set(QueryType4Code field){
		setField(field);
	}
	public void set(QueryDate group){
		addGroup(group);
	}
	public void set(AccountIdentificationAndName7 group){
		addGroup(group);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	
}
