/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 * 
 */
public class TransactionCmonSbsdryItm extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("InstrOrgn", Field.class);
		keyClassMap.put("SbryTp", Field.class);
		keyClassMap.put("InstrId", Field.class);
		keyClassMap.put("CtrctId", Field.class);
		keyClassMap.put("GivAcct", AccountIdentificationAndName4.class);
		keyClassMap.put("TakAcct", AccountIdentificationAndName4.class);
		keyClassMap.put("OrgnlInstrId", Field.class);
		keyClassMap.put("OrgnlCtrctId", Field.class);
		keyClassMap.put("DlvryDt", Field.class);
		keyClassMap.put("CshSttlmDlvry", CashSettlementDelivery.class);
		keyClassMap.put("Oprtr", PartyIdentification.class);
		keyClassMap.put("Chckr", PartyIdentification.class);
		keyClassMap.put("Cnfrmr", PartyIdentification.class);
		keyClassMap.putAll(CashSettlementDelivery.keyClassMap);
		keyClassMap.putAll(AccountIdentificationAndName4.keyClassMap);
		keyClassMap.putAll(PartyIdentification.keyClassMap);
	}

	public TransactionCmonSbsdryItm(String tag) {
		super(tag, tag, new String[] { "InstrOrgn", "SbryTp", "InstrId",
				"CtrctId", "GivAcct", "TakAcct", "OrgnlInstrId",
				"OrgnlCtrctId", "DlvryDt", "CshSttlmDlvry", "Oprtr", "Chckr",
				"Cnfrmr" });
	}
	public TransactionCmonSbsdryItm(String tag,String delim) {
		super(tag, tag, new String[] { "InstrOrgn", "SbryTp", "InstrId",
				"CtrctId", "GivAcct", "TakAcct", "OrgnlInstrId",
				"OrgnlCtrctId", "DlvryDt", "CshSttlmDlvry", "Oprtr", "Chckr",
				"Cnfrmr" });
	}

	public void set(InstructionOriginCode field) {
		setField(field);
	}

	public void set(BusinessTypeCode field) {
		setField(field);
	}

	public void set(Exact12NumericText field) {
		setField(field);
	}

	public void set(Exact9NumericText field) {
		setField(field);
	}
	
	public void set(Max12Text field) {
		setField(field);
	}

	public void set(AccountIdentificationAndName4 group) {
		addGroup(group);
	}

	public void set(ISODate field) {
		setField(field);
	}

	public void set(PartyIdentification group) {
		addGroup(group);
	}
	public void set(CashSettlementDelivery group){
		addGroup(group);
	}

}
