/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean2;
import com.singlee.financial.cdtc.httpclient.XmlFormat;

/**
 * @author Fang
 *
 */
public class HeadMessage extends Group {

	private static String type = null;
	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static {
		keyClassMap.put("Vrsn", TransactionVrsn.class);
		keyClassMap.put("Sndr", TransactionSndr.class);
		keyClassMap.put("Rcvr", TransactionRcvr.class);
		keyClassMap.put("MsgDesc", TransactionMsgDesc.class);
		keyClassMap.put("Sgntr", TransactionSgntr.class);
		keyClassMap.put("Xtnsn", Extension.class);
		keyClassMap.putAll(TransactionVrsn.keyClassMap);
		keyClassMap.putAll(TransactionSndr.keyClassMap);
		keyClassMap.putAll(TransactionRcvr.keyClassMap);
		keyClassMap.putAll(TransactionMsgDesc.keyClassMap);
		keyClassMap.putAll(TransactionSgntr.keyClassMap);
		keyClassMap.putAll(Extension.keyClassMap);
	}
	public HeadMessage(){
	}
	public HeadMessage(String tag){
		super(tag,tag,new String[]{"Vrsn","Sndr","Rcvr","MsgDesc","Sgntr","Xtnsn"});
	}
	
	public HeadMessage(String tag,String belim){
		super(tag,tag,new String[]{"Vrsn","Sndr","Rcvr","MsgDesc","Sgntr","Xtnsn"});
	}
	
	public void set(TransactionVrsn group){
		addGroup(group);
	}
	public void set(TransactionSndr group){
		addGroup(group);
	}
	public void set(TransactionRcvr group){
		addGroup(group);
	}
	public void set(TransactionMsgDesc group){
		addGroup(group);
	}
	public void set(TransactionSgntr group){
		addGroup(group);
	}
	public void set(Extension group){
		addGroup(group);
	}
	
	public static String getType() {
		return type;
	}

	public static void setType(String type) {
		HeadMessage.type = type;
	}

	public static HeadMessage content(Bean2 bean2){
		HeadMessage headMessage = new HeadMessage("MsgHeader");
		/**********************报文版本标识*****************************/
		TransactionVrsn vrsn = new TransactionVrsn("Vrsn");
		Max2NumericText vrsnID = new Max2NumericText("VrsnID");//报文版本标识号
		vrsnID.setValue(01);
		vrsn.set(vrsnID);
		
		/***********************报文发起方********************************/
		TransactionSndr sndr = new TransactionSndr("Sndr");
		Max12Text sndrId = new Max12Text("SndrId");
		sndrId.setValue("SHG110");
		sndr.set(sndrId);
		Max2Text sndrSysId = new Max2Text("SndrSysId");
		sndrSysId.setValue("S0");
		sndr.set(sndrSysId);
		
		/*********************报文接收方*******************************/
		TransactionRcvr rcvr = new TransactionRcvr("Rcvr");
		Max12Text rcvrId = new Max12Text("RcvrId");
		rcvrId.setValue("JKM110");
		rcvr.set(rcvrId);
		Max2Text rcvrSysId = new Max2Text("RcvrSysId");
		rcvrSysId.setValue("J0");
		rcvr.set(rcvrSysId);
		
		/****************************报文描述********************************/
		TransactionMsgDesc msgDesc = new TransactionMsgDesc("MsgDesc");
		Max12Text msgOrgnlSndr = new Max12Text("MsgOrgnlSndr");//报文原始发起人
		msgOrgnlSndr.setValue("0000000013");
		msgDesc.set(msgOrgnlSndr);
		Max12Text msgFnlRcvr = new Max12Text("MsgFnlRcvr");//报文最终接收人
		msgFnlRcvr.setValue("0000007789");
		msgDesc.set(msgFnlRcvr);
		msgDesc.set(new ISODateTime("MsgSndDateTime",new Date()));
		Max15Text msgTp = new Max15Text("MsgTp");
		msgTp.setValue(HeadMessage.type);
		msgDesc.set(msgTp);
		Max26Text msgId = new Max26Text("MsgId");
		msgId.setValue("00000000132012031300067702");
		msgDesc.set(msgId);
		Max26Text msgRefId = new Max26Text("MsgRefId");
		msgRefId.setValue("msgRefId");
		msgDesc.set(msgRefId);
		Max32Text msgBodyChcksum = new Max32Text("MsgBodyChcksum");
		msgBodyChcksum.setValue("NULLL");
		msgDesc.set(msgBodyChcksum);
		
		/***************************报文签名*********************************/
		TransactionSgntr sgntr = new TransactionSgntr("Sgntr");
		Max2048Text cttlmMmbSgntr = new Max2048Text("SttlmMmbSgntr");
		cttlmMmbSgntr.setValue("NULLL");
		sgntr.set(cttlmMmbSgntr);
		Max2048Text cSBSSgntr = new Max2048Text("CSBSSgntr");
		cSBSSgntr.setValue("NULLL");
		sgntr.set(cSBSSgntr);
		
		//拓展项
//		Extension extension = new Extension("Xtnsn");
//		Max2048Text max2048Text = new Max2048Text("XtnsnTxt");
//		max2048Text.setValue("债券账户对账单查询请求报文完毕");
//		extension.set(max2048Text);
		
		headMessage.set(vrsn);
		headMessage.set(sndr);
		headMessage.set(rcvr);
		headMessage.set(msgDesc);
		headMessage.set(sgntr);
//		headMessage.set(extension);
		return headMessage;
	}
	
	
	public static void main(String[] args) throws Exception {
		
		
		StringBuffer stringBuffer = new StringBuffer();
		content(new Bean2()).toXmlForSuperParent(stringBuffer);

		System.out.println(XmlFormat.format(XmlFormat.format(stringBuffer.toString())));

		
		// 保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat
				.format(stringBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out = new FileOutputStream("./cnbondxml/" + HeadMessage.type
				+ ".xml");
		XMLOutputter outputter = new XMLOutputter();
		// 如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的
		outputter.output((Document) doc, out);
		out.close();

		
		
		
		SAXReader reader = new SAXReader();
		/**
		 * 反解析报文
		 */
		HeadMessage safekeeping = new HeadMessage("MsgHeader");
		org.dom4j.Document docXml = reader.read(new File("./cnbondxml/"+ HeadMessage.type + ".xml"));
		getElementList(docXml.getRootElement(), safekeeping);
		
		StringBuffer xmlBuffer = new StringBuffer();
		safekeeping.toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * @param element
	 * @param common
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	public static void getElementList(Element element,HeadMessage common)throws Exception,
	NoSuchMethodException{
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			List<?> elements2 = element2.elements();
			if(elements2.size() >= 0){
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
				if(common.isHasTag(StringUtils.trimToEmpty(element2.getName()))){
					if(element2.elements().size() > 0){
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
						common.addGroup(getGroups(element2, common));
					}else{
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
						Class<?>[] parameterTypes = {String.class,Object.class};
						Constructor<?> constructor = HeadMessage.keyClassMap
						.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
						Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element.getText())};
						Object object = constructor.newInstance(parameters);
						common.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
					}
				}
			}
		}
	}
	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception反解析报文
	 */
	public static Group getGroups(Element element,
			HeadMessage common)
			throws NoSuchMethodException, Exception {
		//Group group = new Group(StringUtils.trimToEmpty(element.getName()),"");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = HeadMessage.keyClassMap.get(StringUtils.trimToEmpty(element.getName())).getConstructor(parameterTypes2);
		Object[] parameters2 = {StringUtils.trimToEmpty(element.getName()),""};
		Group group = (Group) constructor2.newInstance(parameters2);
		
		System.out.println("************************************");
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
			if(element2.elements().size() > 0){
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
				group.addGroup(getGroups(element2, common));
			}else{
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
				Class<?>[] parameterTypes = {String.class,Object.class};
				Constructor<?> constructor = HeadMessage.keyClassMap
				.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
				Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element2.getText())};
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
			}
		}
		System.out.println("************************************");
		return group;
	}
	
	
}
