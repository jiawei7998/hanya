/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author Fang
 *
 */
public class EsbDateOnlyField extends DateField {
	 private static final long serialVersionUID = 1L;
	  private static TimeZone timezone = TimeZone.getTimeZone("ESB");
	  private static Calendar calendar;

	  public EsbDateOnlyField(String field)
	  {
	    super(field, EsbDateOnlyConverter.convert(createDate()));
	  }

	  protected EsbDateOnlyField(String field, Date data)
	  {
	    super(field, EsbDateOnlyConverter.convert(data));
	  }

	  protected EsbDateOnlyField(String field, String data)
	  {
	    super(field, data);
	  }

	  public void setValue(Date value) {
	    setValue(EsbDateOnlyConverter.convert(value));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(EsbDateOnlyConverter.convert(value));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }

}
