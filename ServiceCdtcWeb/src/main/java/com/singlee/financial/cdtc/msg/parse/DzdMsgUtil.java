package com.singlee.financial.cdtc.msg.parse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzdacct;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzdacctId;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzddt;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzddtId;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzddtsubject;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzddtsubjectId;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzdhd;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzdsecid;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzdsecidId;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzdsum;
import com.singlee.financial.cdtc.bean.SlCnbondlinkDzdsumId;

/**
 * "债券账户对帐单报告 "和 "债券账户对账单查询请求" 报文工具类
 * 
 * @author
 * 
 */
public class DzdMsgUtil {

	public Document getDocument(File file) throws Exception {
		SAXReader reader = new SAXReader();
		// reader.setEncoding("utf-8");
		return reader.read(new FileInputStream(file));
	}

	public Document getDocument(String msg) throws Exception {
		SAXReader reader = new SAXReader();
		// reader.setEncoding("utf-8");
		return reader.read(msg);
	}

	/**
	 * 解析 债券账户对帐单报告 报文
	 * 
	 * @return
	 * @throws Exception
	 */
	public SlCnbondlinkDzdhd parseMsg(Document doc) throws Exception {
		SlCnbondlinkDzdhd bean = new SlCnbondlinkDzdhd();
		// 债券账户对账单报告-债券账户信息
		List<SlCnbondlinkDzdacct> slCnbondlinkDzdaccts = null;
		SlCnbondlinkDzdacct slCnbondlinkDzdacct = null;
		SlCnbondlinkDzdacctId slCnbondlinkDzdacctId = null;
		// 债券账户对账单报告-债券信息
		List<SlCnbondlinkDzdsecid> slCnbondlinkDzdsecids = null;
		SlCnbondlinkDzdsecid slCnbondlinkDzdsecid = null;
		SlCnbondlinkDzdsecidId slCnbondlinkDzdsecidId = null;
		// 债券账户对账单报告-托管总对账单
		List<SlCnbondlinkDzdsum> slCnbondlinkDzdsums = null;
		SlCnbondlinkDzdsum slCnbondlinkDzdsum = null;
		SlCnbondlinkDzdsumId slCnbondlinkDzdsumId = null;
		Date summaryStatementDt = null;
		// 债券账户对账单报告-托管明细对账单
		List<SlCnbondlinkDzddt> slCnbondlinkDzddts = null;
		SlCnbondlinkDzddt slCnbondlinkDzddt = null;
		SlCnbondlinkDzddtId slCnbondlinkDzddtId = null;
		// 托管明细对账单-科目余额
		List<SlCnbondlinkDzddtsubject> slCnbondlinkDzddtsubjects = null;
		SlCnbondlinkDzddtsubject slCnbondlinkDzddtsubject = null;
		SlCnbondlinkDzddtsubjectId slCnbondlinkDzddtsubjectId = null;
		//
		Element root = null;
		//
		Element document = null;
		//
		Element dempParent = null;
		Element dempChild = null;
		Element dempChild2 = null;
		Element dempChild3 = null;
		Element dempChild4 = null;
		Element dempChild5 = null;
		//
		Date currentDate = new Date();
		synchronized (currentDate) {
			try {
				Thread.sleep(1000);
			} catch (Exception ex) {
				//
			}
			// 生成唯一流水号 SN Characters (15)
			bean.setSn("D" + DateUtil.convertDateToYYYYMMDDHHMMSS(new Date()));
		}// end synchronized

		// 得到根节点
		root = doc.getRootElement();

		// Document
		document = root.element("Document");

		// 1.Msg 报文标识
		dempParent = document.element("Msg");
		// 交易流水标识
		dempChild = dempParent.element("TxFlowId");// Max35Text
		bean.setTxFlowId(null == dempChild ? null : dempChild.getTextTrim());
		// 报文创建时间 CreDtTm ISODateTime
		dempChild = dempParent.element("CreDtTm");
		bean.setCreDtTm(null == dempChild ? null : DateUtil.convertISODateTimeToDate(dempChild.getTextTrim()));

		// 2.对账单报告 StmtRpt
		dempParent = document.element("StmtRpt");
		// 起始日期 StartDt ISODate
		dempChild = dempParent.element("StartDt");
		bean.setStartDt(null == dempChild ? null : DateUtil.convertISODateToDate(dempChild.getTextTrim()));
		// 截止日期 EndDt ISODate
		dempChild = dempParent.element("EndDt");
		bean.setEndDt(null == dempChild ? null : DateUtil.convertISODateToDate(dempChild.getTextTrim()));
		// 债券账户计数 SfkpgAcctCnt
		dempChild = dempParent.element("SfkpgAcctCnt");
		bean.setSfkpgAcctCnt(null == dempChild ? null : new BigDecimal(dempChild.getTextTrim()));
		// 债券账户对账单信息 <SfkpgAcctStmtInf/>
		slCnbondlinkDzdaccts = new ArrayList<SlCnbondlinkDzdacct>();
		for (Iterator<?> i = dempParent.elementIterator("SfkpgAcctStmtInf"); i.hasNext();) {
			slCnbondlinkDzdacct = new SlCnbondlinkDzdacct();
			//
			dempChild = (Element) i.next();
			// 账户名称 <Nm/>
			dempChild2 = dempChild.element("Nm");
			slCnbondlinkDzdacct.setNm(null == dempChild2 ? null : dempChild2.getTextTrim());
			// 账户编号 <Id/>
			dempChild2 = dempChild.element("Id");
			slCnbondlinkDzdacctId = new SlCnbondlinkDzdacctId();
			slCnbondlinkDzdacctId.setSn(bean.getSn());
			slCnbondlinkDzdacctId.setId(null == dempChild2 ? null : dempChild2.getTextTrim());
			slCnbondlinkDzdacct.setId(slCnbondlinkDzdacctId);
			// 债券数目 <BdCnt/>
			dempChild2 = dempChild.element("BdCnt");
			slCnbondlinkDzdacct.setBdCnt(null == dempChild2 ? null : new BigDecimal(dempChild2.getTextTrim()));
			// 债券对账单 <Bd/>
			slCnbondlinkDzdsecids = new ArrayList<SlCnbondlinkDzdsecid>();
			for (Iterator<?> j = dempChild.elementIterator("Bd"); j.hasNext();) {
				dempChild2 = (Element) j.next();
				slCnbondlinkDzdsecid = new SlCnbondlinkDzdsecid();
				slCnbondlinkDzdsecidId = new SlCnbondlinkDzdsecidId();
				slCnbondlinkDzdsecidId.setSn(bean.getSn());
				slCnbondlinkDzdsecidId.setId(slCnbondlinkDzdacctId.getId());
				// 债券代码 <BdId/>
				dempChild3 = dempChild2.element("BdId");
				slCnbondlinkDzdsecidId.setBdId(null == dempChild3 ? null : dempChild3.getTextTrim());
				//
				slCnbondlinkDzdsecid.setId(slCnbondlinkDzdsecidId);
				// ISIN编码 <ISIN/>
				dempChild3 = dempChild2.element("ISIN");
				slCnbondlinkDzdsecid.setIsin(null == dempChild3 ? null : dempChild3.getTextTrim());
				// 债券简称 <BdShrtNm/>
				dempChild3 = dempChild2.element("BdShrtNm");
				slCnbondlinkDzdsecid.setBdShrtNm(null == dempChild3 ? null : dempChild3.getTextTrim());
				// 债券币种 <CcyTp/>
				dempChild3 = dempChild2.element("CcyTp");
				slCnbondlinkDzdsecid.setCcyTp(null == dempChild3 ? null : dempChild3.getTextTrim());
				// 债券性质 <BdParam/>
				dempChild3 = dempChild2.element("BdParam");
				slCnbondlinkDzdsecid.setBdParam(null == dempChild3 ? null : dempChild3.getTextTrim());
				// 托管总对账单 <SummryStmt/> [0..1]
				dempChild3 = dempChild2.element("SummryStmt");
				if (null != dempChild3) {// 先判断有没有总对账单，如果没有，则解析明细对账单
					// 是否有托管总对账单 SummryStmtFLAG Characters (1)
					slCnbondlinkDzdsecid.setSummryStmtFlag('1');
					// 是否有托管明细对账单 DtldStmtFLAG Characters (1)
					slCnbondlinkDzdsecid.setDtldStmtFlag('0');
					// 对账单类型 DZDTYPE S: 汇总 D:明细
					bean.setDzdtype('S');
					//
					// 日期 <Dt/> [0..1]
					dempChild4 = dempChild3.element("Dt");
					summaryStatementDt = null == dempChild4 ? null : DateUtil.convertISODateToDate(dempChild4.getTextTrim());
					// 科目余额 <AcctLdg/> [1..n]
					slCnbondlinkDzdsums = new ArrayList<SlCnbondlinkDzdsum>();
					for (Iterator<?> l = dempChild3.elementIterator("AcctLdg"); l.hasNext();) {
						dempChild4 = (Element) l.next();
						slCnbondlinkDzdsum = new SlCnbondlinkDzdsum();
						slCnbondlinkDzdsumId = new SlCnbondlinkDzdsumId();
						slCnbondlinkDzdsumId.setSn(slCnbondlinkDzdsecidId.getSn());// 流水号 SN Characters (15)
						slCnbondlinkDzdsumId.setId(slCnbondlinkDzdsecidId.getId());// 账户编号 Id Variable characters (11)
						slCnbondlinkDzdsumId.setBdId(slCnbondlinkDzdsecidId.getBdId());// 债券代码 BdId Variable characters (30)
						slCnbondlinkDzdsumId.setDt(summaryStatementDt);// 托管总对账单日期 Dt Date
						// 托管总对账单科目类型 LdgTp Characters (4)
						dempChild5 = dempChild4.element("LdgTp");
						slCnbondlinkDzdsumId.setLdgTp(null == dempChild5 ? "" : dempChild5.getTextTrim());
						//
						slCnbondlinkDzdsum.setId(slCnbondlinkDzdsumId);
						// 托管总对账单科目余额 Bal Number (19,4)
						dempChild5 = dempChild4.element("Bal");
						slCnbondlinkDzdsum.setBal(null == dempChild5 ? null : new BigDecimal(dempChild5.getTextTrim()));
						//
						slCnbondlinkDzdsums.add(slCnbondlinkDzdsum);
					}// end inner for4
					slCnbondlinkDzdsecid.setSlCnbondlinkDzdsums(slCnbondlinkDzdsums);
				} else {// 托管明细对账单 <DtldStmt/>[0..n]
					// 是否有托管总对账单 SummryStmtFLAG Characters (1)
					slCnbondlinkDzdsecid.setSummryStmtFlag('0');
					// 是否有托管明细对账单 DtldStmtFLAG Characters (1)
					slCnbondlinkDzdsecid.setDtldStmtFlag('1');
					// 对账单类型 DZDTYPE S: 汇总 D:明细
					bean.setDzdtype('D');
					//
					slCnbondlinkDzddts = new ArrayList<SlCnbondlinkDzddt>();
					for (Iterator<?> l = dempChild2.elementIterator("DtldStmt"); l.hasNext();) {
						dempChild3 = (Element) l.next();
						slCnbondlinkDzddt = new SlCnbondlinkDzddt();
						slCnbondlinkDzddtId = new SlCnbondlinkDzddtId();
						//
						slCnbondlinkDzddtId.setSn(slCnbondlinkDzdsecidId.getSn());// 流水号 SN Characters (15)
						slCnbondlinkDzddtId.setId(slCnbondlinkDzdsecidId.getId());// 账户编号 Id Variable characters (11)
						slCnbondlinkDzddtId.setBdId(slCnbondlinkDzdsecidId.getBdId());// 债券代码 BdId Variable characters (30)
						// 记帐日期 TxDt Date
						dempChild4 = dempChild3.element("TxDt");
						slCnbondlinkDzddtId.setTxDt(DateUtil.convertISODateToDate(dempChild4.getTextTrim()));
						// 结算合同标识 CtrctId Number (19)
						dempChild4 = dempChild3.element("CtrctId");
						slCnbondlinkDzddtId.setCtrctId(null == dempChild4 ? BigDecimal.ZERO : new BigDecimal(dempChild4.getTextTrim()));
						//
						slCnbondlinkDzddt.setId(slCnbondlinkDzddtId);
						// 记帐时间 TxTm Date
						dempChild4 = dempChild3.element("TxTm");
						slCnbondlinkDzddt.setTxTm(null == dempChild4 ? null : DateUtil.convertISODateTimeToDate(dempChild4.getTextTrim()));
						// 记账摘要 TxLog Variable multibyte (35)
						dempChild4 = dempChild3.element("TxLog");
						slCnbondlinkDzddt.setTxLog(null == dempChild4 ? null : dempChild4.getTextTrim());
						// 发生额 TxAmt Number (19,4)
						dempChild4 = dempChild3.element("TxAmt");
						slCnbondlinkDzddt.setTxAmt(null == dempChild4 ? BigDecimal.ZERO : new BigDecimal(dempChild4.getTextTrim()));
						//
						// 账户科目 <AcctLdg/> [1..n] 组件
						slCnbondlinkDzddtsubjects = new ArrayList<SlCnbondlinkDzddtsubject>();
						for (Iterator<?> m = dempChild3.elementIterator("AcctLdg"); m.hasNext();) {
							dempChild4 = (Element) m.next();
							//
							slCnbondlinkDzddtsubject = new SlCnbondlinkDzddtsubject();
							slCnbondlinkDzddtsubjectId = new SlCnbondlinkDzddtsubjectId();
							//
							slCnbondlinkDzddtsubjectId.setSn(slCnbondlinkDzddtId.getSn());// 流水号 SN Characters (15)
							slCnbondlinkDzddtsubjectId.setId(slCnbondlinkDzddtId.getId());// 账户编号 Id Variable characters (11)
							slCnbondlinkDzddtsubjectId.setBdId(slCnbondlinkDzddtId.getBdId());// 债券代码 BdId Variable characters (30)
							slCnbondlinkDzddtsubjectId.setTxDt(slCnbondlinkDzddtId.getTxDt());// 记帐日期 TxDt Date
							slCnbondlinkDzddtsubjectId.setCtrctId(slCnbondlinkDzddtId.getCtrctId());// 结算合同标识 CtrctId Number (19)
							// 托管总对账单科目类型 LdgTp Characters (4)
							dempChild5 = dempChild4.element("LdgTp");
							slCnbondlinkDzddtsubjectId.setLdgTp(null == dempChild5 ? "" : dempChild5.getTextTrim());
							//
							slCnbondlinkDzddtsubject.setId(slCnbondlinkDzddtsubjectId);
							// 托管总对账单科目余额 Bal Number (19,4)
							dempChild5 = dempChild4.element("Bal");
							slCnbondlinkDzddtsubject.setBal(null == dempChild5 ? null : new BigDecimal(dempChild5.getTextTrim()));
							//
							slCnbondlinkDzddtsubjects.add(slCnbondlinkDzddtsubject);
						}// end inner for4
						slCnbondlinkDzddt.setSlCnbondlinkDzddtsubjects(slCnbondlinkDzddtsubjects);
						//
						slCnbondlinkDzddts.add(slCnbondlinkDzddt);
					}// end inner for3
					slCnbondlinkDzdsecid.setSlCnbondlinkDzddts(slCnbondlinkDzddts);
				}// end if else
				slCnbondlinkDzdsecids.add(slCnbondlinkDzdsecid);
			}// end for2
			slCnbondlinkDzdacct.setSlCnbondlinkDzdsecids(slCnbondlinkDzdsecids);
			//
			slCnbondlinkDzdaccts.add(slCnbondlinkDzdacct);
		}// end for1
		//
		bean.setSlCnbondlinkDzdaccts(slCnbondlinkDzdaccts);

		return bean;
	}

	/**
	 * 创建"债券账户对账单查询请求"QT01：托管总对账单
	 * 
	 * @param txFlowId
	 * @param requestPath
	 * @param queryDate
	 * @param acctName
	 * @return
	 * @throws Exception
	 */
	public String createCheckRequestQT01(String txFlowId, String requestPath, Date queryDate, String acctName) throws Exception {
		Element root = null;
		Element dempChild = null;
		Element dempChild2 = null;
		Element dempChild3 = null;
		// 创建根节点
		root = DocumentHelper.createElement("Root");
		Document doc = DocumentHelper.createDocument(root);
		// ----------------------------------------------------
		// 1.报文标识
		dempChild = root.addElement("Msg");
		dempChild2 = dempChild.addElement("TxFlowId");// 交易流水标识
		dempChild2.setText(txFlowId);
		//
		dempChild2 = dempChild.addElement("Prty");// 优先级
		// PT01：高级
		// PT02：普通
		// PT03：低级
		dempChild2.setText("PT02");
		//
		dempChild2 = dempChild.addElement("CreDtTm");// 报文创建时间
		dempChild2.setText(DateUtil.dfISODateTime.format(new Date()));
		// ----------------------------------------------------
		// 2.操作标识
		dempChild = root.addElement("OprtrFlg"); // 操作标识 <OprtrFlg/>
		dempChild2 = dempChild.addElement("ReqTp");// 请求类别码 <ReqTp/> RequestTypeCode
		// BJ0400 债券账户对账单查询
		dempChild2.setText("BJ0400");
		//
		dempChild2 = dempChild.addElement("OprtrSts");// 操作码 <OprtrSts/>OperatorStatusCode
		// OS07: 明细对账单
		// OS08: 总对账单
		dempChild2.setText("OS08");
		// ----------------------------------------------------
		// 3.查询条目
		dempChild = root.addElement("QryItm");// 查询条目 <QryItm/> [1..1]
		dempChild2 = dempChild.addElement("QryTp");// 查询类型 <QryTp/> [0..1] QueryType4Code
		// QT01：托管总对账单
		// QT02：托管明细对账单
		dempChild2.setText("QT01");
		//
		dempChild2 = dempChild.addElement("QryDt");// 查询日期 <QryDt/> [1..1] 组件
		// 当查询类型为“托管总对账单”时，该组件中的“起始日期”和“截止日期”应为同一天。
		// 当查询类型为“托管明细对账单”时，该组件中的“起始日期”和“截止日期”可为同一天或一时间段。
		dempChild3 = dempChild2.addElement("StartDt");
		dempChild3.setText(DateUtil.dfISODate.format(queryDate));
		dempChild3 = dempChild2.addElement("EndDt");
		dempChild3.setText(DateUtil.dfISODate.format(queryDate));
		// 查询账户与债券 <SfkpgAcctBdQry/> [1..1] 组件
		// 当查询类型为“托管总对账单”时，该组件中的债券账户必填，查询债券无用。
		// 当查询类型为“托管明细对账单”时，该组件中的债券账户必填，查询债券必填。
		dempChild2 = dempChild.addElement("SfkpgAcctBdQry");
		// 账户名称 <Nm/> 不需要
		// 账户编号 <Id/>
		dempChild3 = dempChild2.addElement("Id");
		dempChild3.setText(acctName);
		// 账户对账单类型 <AccStTp>AccountSatementType
		// AS00：债权
		// AS01：额度
		dempChild3 = dempChild2.addElement("AccStTp");
		dempChild3.setText("AS01");
		// 查询债券 <QryBd/>
		// 债券代码 <BdId/> [1..1] Max30Text
		// ISIN编码 <ISIN/> [0..1] ISINIdentifier
		// 债券简称 <BdShrtNm/> [0..1] Max35Text

		// ----------------------------------------------------
		// 4.查询方

		// ----------------------------------------------------
		// 5.扩展项

		// 保存xml文件
		OutputFormat format = OutputFormat.createPrettyPrint();
		// format.setEncoding("UTF-8");
		XMLWriter writer2 = new XMLWriter(new FileOutputStream(requestPath), format);
		writer2.write(doc); // 输出到文件
		writer2.flush();
		writer2.close();

		return requestPath;
	}

	/**
	 * 创建"债券账户对账单查询请求"QT02：托管明细对账单
	 * 
	 * @param txFlowId
	 * @param requestPath
	 * @param queryDateStart
	 * @param queryDateEnd
	 * @param secid
	 * @return
	 * @throws Exception
	 */
	public String createCheckRequestQT02(String txFlowId, String requestPath, Date queryDateStart, Date queryDateEnd, String acctName, String secid) throws Exception {
		Element root = null;
		Element dempChild = null;
		Element dempChild2 = null;
		Element dempChild3 = null;
		Element dempChild4 = null;
		// 创建根节点
		root = DocumentHelper.createElement("Root");
		Document doc = DocumentHelper.createDocument(root);
		// ----------------------------------------------------
		// 1.报文标识
		dempChild = root.addElement("Msg");
		dempChild2 = dempChild.addElement("TxFlowId");// 交易流水标识
		dempChild2.setText(txFlowId);
		//
		dempChild2 = dempChild.addElement("Prty");// 优先级
		// PT01：高级
		// PT02：普通
		// PT03：低级
		dempChild2.setText("PT02");
		//
		dempChild2 = dempChild.addElement("CreDtTm");// 报文创建时间
		dempChild2.setText(DateUtil.dfISODateTime.format(new Date()));
		// ----------------------------------------------------
		// 2.操作标识
		dempChild = root.addElement("OprtrFlg"); // 操作标识 <OprtrFlg/>
		dempChild2 = dempChild.addElement("ReqTp");// 请求类别码 <ReqTp/> RequestTypeCode
		// BJ0400 债券账户对账单查询
		dempChild2.setText("BJ0400");
		//
		dempChild2 = dempChild.addElement("OprtrSts");// 操作码 <OprtrSts/>OperatorStatusCode
		// OS07: 明细对账单
		// OS08: 总对账单
		dempChild2.setText("OS07");
		// ----------------------------------------------------
		// 3.查询条目
		dempChild = root.addElement("QryItm");// 查询条目 <QryItm/> [1..1]
		dempChild2 = dempChild.addElement("QryTp");// 查询类型 <QryTp/> [0..1] QueryType4Code
		// QT01：托管总对账单
		// QT02：托管明细对账单
		dempChild2.setText("QT02");
		//
		dempChild2 = dempChild.addElement("QryDt");// 查询日期 <QryDt/> [1..1] 组件
		// 当查询类型为“托管总对账单”时，该组件中的“起始日期”和“截止日期”应为同一天。
		// 当查询类型为“托管明细对账单”时，该组件中的“起始日期”和“截止日期”可为同一天或一时间段。
		dempChild3 = dempChild2.addElement("StartDt");
		dempChild3.setText(DateUtil.dfISODate.format(queryDateStart));
		dempChild3 = dempChild2.addElement("EndDt");
		dempChild3.setText(DateUtil.dfISODate.format(queryDateEnd));
		// 查询账户与债券 <SfkpgAcctBdQry/> [1..1] 组件
		// 当查询类型为“托管总对账单”时，该组件中的债券账户必填，查询债券无用。
		// 当查询类型为“托管明细对账单”时，该组件中的债券账户必填，查询债券必填。
		dempChild2 = dempChild.addElement("SfkpgAcctBdQry");
		// 账户名称 <Nm/> 不需要
		// 账户编号 <Id/>
		dempChild3 = dempChild2.addElement("Id");
		dempChild3.setText(acctName);
		// 账户对账单类型 <AccStTp>AccountSatementType
		// AS00：债权
		// AS01：额度
		dempChild3 = dempChild2.addElement("AccStTp");
		dempChild3.setText("AS01");
		// 查询债券 <QryBd/>
		dempChild3 = dempChild2.addElement("QryBd");
		// 债券代码 <BdId/> [1..1] Max30Text
		// ISIN编码 <ISIN/> [0..1] ISINIdentifier
		// 债券简称 <BdShrtNm/> [0..1] Max35Text
		dempChild4 = dempChild3.addElement("BdId");
		dempChild4.setText(secid);

		// ----------------------------------------------------
		// 4.查询方

		// ----------------------------------------------------
		// 5.扩展项

		// 保存xml文件
		OutputFormat format = OutputFormat.createPrettyPrint();
		// format.setEncoding("UTF-8");
		XMLWriter writer2 = new XMLWriter(new FileOutputStream(requestPath), format);
		writer2.write(doc); // 输出到文件
		writer2.flush();
		writer2.close();

		return requestPath;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			DzdMsgUtil dzdMsgUtil = new DzdMsgUtil();
			dzdMsgUtil.createCheckRequestQT01("01100001", "dzd_request_Qt01.xml", new Date(), "A10100001");
			dzdMsgUtil.createCheckRequestQT02("01100001", "dzd_request_Qt02.xml", new Date(), new Date(), "A10100001", "0100001");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
