package com.singlee.financial.cdtc.httpclient;

import java.awt.Component;

import javax.swing.ProgressMonitor;

/**
 * 调用有进度条的对话框的方法,使其能够正常显示
 * 
 * @author yangwq
 * 
 */
public class Monitor {

	/**
	 * @param args
	 */
	private ProgressMonitor pm=null;
	private Runnable frame=null;

	public Monitor(Runnable frame) {
		this.frame = frame;
	}

	
	public void spawn(final Component component) {
		Runnable worker = new Runnable() {
			@Override
			public void run() {
				startMonitor(component);
			}
		};
		Thread thread = new Thread(worker);
		thread.start();
	}

	public void startMonitor(Component c) {
		pm = new ProgressMonitor(c, null, "", 0, 100);
		pm.setProgress(1);
		pm.setMillisToDecideToPopup(0);
		if (null != frame) {
			frame.run();
		} 
	}

}// end class
