package com.singlee.financial.cdtc.cnbondpackage;

import java.awt.Color;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import com.singlee.financial.cdtc.bean.CSBSDataBean;

public class MessageDetailedList {

	/**
	 * 传入XML报文，报文类型，和数据字典
	 * 解析报文
	 */
	@SuppressWarnings("unchecked")
	public static void queryCSBSDataMessageNote(Element element,String msgname,HashMap<String, IdentityHashMap<String, CSBSDataBean>> csbsHashMap,JTextPane textPane){
		IdentityHashMap<String, CSBSDataBean> csbsMap = csbsHashMap.get(msgname);
		List<Element> elements = element.elements();
		
		//IdentitfyHashMap转换成HashMap
		HashMap<String, CSBSDataBean> changeHashMap = new HashMap<String, CSBSDataBean>();
		if(csbsMap.size()>0){
			Iterator<String> iterator = csbsMap.keySet().iterator();
			while(iterator.hasNext()){
				String keyString = iterator.next();
				changeHashMap.put(keyString,csbsMap.get(keyString));
			}
		}
		//迭代取出XML文件
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			List<Element> elements2 = element2.elements();
			
			if(elements2.size()>=0)
			{
				CSBSDataBean bean = changeHashMap.get(element2.getName());
				
				if(changeHashMap.get(element2.getName())!=null && changeHashMap.get(element2.getName()).getTAGXML().length()>0)
				{
					if(element2.elements().size()>0)
					{
						System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
								String.format("%30s :", "")+" "+bean.getTAGNAME()+" ");
						setFontV(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
								String.format("%30s :", "")+" "+bean.getTAGNAME()+" ",textPane);
						//判断GROUP是否是组件
						group(element2,"组件".equals(StringUtils.trimToEmpty(bean.getTYPE()))?bean.getMODULE():msgname,csbsHashMap, textPane);
					}else {
						int count = JudgeHanziNum(StringUtils.trimToEmpty(element2.getText()));
						String setStr = "%"+(30-count)+"s :";
						System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
								String.format(setStr, StringUtils.trimToEmpty(element2.getText()))+" "+bean.getTAGNAME()+" ");
						setFontV(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
								String.format(setStr, StringUtils.trimToEmpty(element2.getText()))+" "+bean.getTAGNAME()+" ",textPane);
						
					}
				}
				
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void group(Element element,String msgname,HashMap<String, IdentityHashMap<String, CSBSDataBean>> csbsHashMap,JTextPane textPane){
		List<Element> elements = element.elements();
		IdentityHashMap<String, CSBSDataBean> csbsMap = csbsHashMap.get(msgname);
		
		//IdentitfyHashMap转换成HashMap
		HashMap<String, CSBSDataBean> changeHashMap = new HashMap<String, CSBSDataBean>();
		if(csbsMap.size()>0){
			Iterator<String> iterator = csbsMap.keySet().iterator();
			while(iterator.hasNext()){
				String keyString = iterator.next();
				changeHashMap.put(keyString,csbsMap.get(keyString));
			}
		}
		
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			
			CSBSDataBean bean = changeHashMap.get(element2.getName());
			if(element2.elements().size()>0)
			{
				System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
						String.format("%30s :", "")+" "+bean.getTAGNAME()+" ");
				setFontV(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
						String.format("%30s :", "")+" "+bean.getTAGNAME()+" ",textPane);
				group(element2,bean.getMODULE(),csbsHashMap,textPane);
			}else {
				int count = JudgeHanziNum(StringUtils.trimToEmpty(element2.getText()));
				String setStr = "%"+(30-count)+"s :";
				System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
						String.format(setStr, StringUtils.trimToEmpty(element2.getText()))+" "+bean.getTAGNAME()+" ");
				setFontV(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+
						String.format(setStr, StringUtils.trimToEmpty(element2.getText()))+" "+bean.getTAGNAME()+" ",textPane);
			}
		}
	}
	
	public static void setFontV(String str,JTextPane textPane){

		SimpleAttributeSet attrset = new SimpleAttributeSet();
		StyleConstants.setForeground(attrset, Color.BLACK);
		StyleConstants.setLineSpacing(attrset, 1.0f);
		insert(str, attrset,textPane);
	}//end of setRed_UnderLine_Italic_24()
	public static void insert(String str, AttributeSet attrset,JTextPane textPane){

		Document docs = textPane.getDocument();
		str = str + "\n";
		try{
		  docs.insertString(docs.getLength(), str, attrset);
		}catch(BadLocationException ble){
		  System.out.println("BadLocationException: "+ble);
		}	
	}//end of insert()
	
	
	
	public static int JudgeHanziNum(String str){
		/**
		 * 判断字符串中中文的个数
		 */
		char[] chars = str.toCharArray();
		int count = 0;
		
		for(int i=0; i<chars.length;i++){
			byte[] bytes=(""+chars[i]).getBytes();
			if(bytes.length==2){
				int[] ints = new int[2];
				ints[0] = bytes[0]&0xff;
				ints[1] = bytes[1]&0xff;
				if(ints[0]>=0x81&&ints[0]<=0xfe && ints[1]>=0x40
						&&ints[1]<=0xfe){
					count++;
					
				}
			}
		}
		return count;
	}

}
