package com.singlee.financial.cdtc.pac;

public class FieldNotFound extends Exception{
	  private static final long serialVersionUID = 1L;
	  public String field;

	  public FieldNotFound(String field)
	  {
	    super("Field [" + field + "] was not found in message.");
	    this.field = field;
	  }
}
