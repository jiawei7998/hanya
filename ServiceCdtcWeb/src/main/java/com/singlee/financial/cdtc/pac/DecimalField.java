package com.singlee.financial.cdtc.pac;

import java.math.BigDecimal;

public class DecimalField  extends Field<BigDecimal>
{
	  private static final long serialVersionUID = 1L;
	  private int padding = 0;

	  public DecimalField(String field) {
	    super(field, BigDecimal.ZERO);
	  }

	  public DecimalField(String field, BigDecimal data) {
	    super(field, data);
	  }

	  public DecimalField(String field, double data)
	  {
	    super(field, new BigDecimal(Double.toString(data)));
	  }

	  public DecimalField(String field, BigDecimal data, int padding) {
	    super(field, data);
	    this.padding = padding;
	  }

	  public void setValue(BigDecimal value) {
	    setObject(value);
	  }

	  public void setValue(double value)
	  {
	    setObject(new BigDecimal(Double.toString(value)));
	  }

	  public BigDecimal getValue() {
	    return (BigDecimal)getObject();
	  }

	  public int getPadding() {
	    return this.padding;
	  }

	  public boolean valueEquals(BigDecimal value) {
	    return getValue().compareTo(value) == 0;
	  }

	  public boolean valueEquals(double value) {
	    return getValue().compareTo(new BigDecimal(Double.toString(value))) == 0;
	  }
	}
