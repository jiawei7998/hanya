/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class PartyIdentification34 extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("Nm", Field.class);
		keyClassMap.put("AcctInTrfOut", Group.class);
		keyClassMap.put("AcctInTrfIn", Group.class);
		keyClassMap.put("AgtPtyInTrfOut", Group.class);
		keyClassMap.put("AgtPtyInTrfIn", Group.class);
		keyClassMap.putAll(SafekeepingAccount.keyClassMap);
		keyClassMap.putAll(AgentParty.keyClassMap);
	}
	
	public PartyIdentification34(String tag){
		super(tag,tag, new String[] { "Nm","AcctInTrfOut","AcctInTrfIn","AgtPtyInTrfOut","AgtPtyInTrfIn"});
	}
	public void set(Max35Text field){
		setField(field);
	}
	public void set(SafekeepingAccount group){
		addGroup(group);
	}
	public void set(AgentParty group){
		addGroup(group);
	}
	
}
