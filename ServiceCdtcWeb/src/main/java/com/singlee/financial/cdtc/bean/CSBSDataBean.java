package com.singlee.financial.cdtc.bean;

import java.io.Serializable;

public class CSBSDataBean implements Serializable,Comparable<CSBSDataBean>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 //消息名称
	 private String MSGNAME    ;
	 //编号
	 private String MSGIDENTIFY;
	 //条目名称
	 private String TAGNAME    ;
	 //xml标签
	 private String TAGXML     ;
	 //出现次数
	 private String TAGNUM     ;
	 //数据类型
	 private String TYPE       ;
	 //说明
	 private String REMARK     ;
	 //组件
	 private String MODULE     ;
	 
	 private String NUM;
	 
	 @Override
	public String toString() {
			// TODO Auto-generated method stub
		return getTAGXML();
	}
	/**
	 * @return the mSGNAME
	 */
	public String getMSGNAME() {
		return MSGNAME;
	}
	/**
	 * @param msgname the mSGNAME to set
	 */
	public void setMSGNAME(String msgname) {
		MSGNAME = msgname;
	}
	/**
	 * @return the mSGIDENTIFY
	 */
	public String getMSGIDENTIFY() {
		return MSGIDENTIFY;
	}
	/**
	 * @param msgidentify the mSGIDENTIFY to set
	 */
	public void setMSGIDENTIFY(String msgidentify) {
		MSGIDENTIFY = msgidentify;
	}
	/**
	 * @return the tAGNAME
	 */
	public String getTAGNAME() {
		return TAGNAME;
	}
	/**
	 * @param tagname the tAGNAME to set
	 */
	public void setTAGNAME(String tagname) {
		TAGNAME = tagname;
	}
	/**
	 * @return the tAGXML
	 */
	public String getTAGXML() {
		return TAGXML;
	}
	/**
	 * @param tagxml the tAGXML to set
	 */
	public void setTAGXML(String tagxml) {
		TAGXML = tagxml;
	}
	/**
	 * @return the tAGNUM
	 */
	public String getTAGNUM() {
		return TAGNUM;
	}
	/**
	 * @param tagnum the tAGNUM to set
	 */
	public void setTAGNUM(String tagnum) {
		TAGNUM = tagnum;
	}
	/**
	 * @return the tYPE
	 */
	public String getTYPE() {
		return TYPE;
	}
	/**
	 * @param type the tYPE to set
	 */
	public void setTYPE(String type) {
		TYPE = type;
	}
	/**
	 * @return the rEMARK
	 */
	public String getREMARK() {
		return REMARK;
	}
	/**
	 * @param remark the rEMARK to set
	 */
	public void setREMARK(String remark) {
		REMARK = remark;
	}
	/**
	 * @return the mODULE
	 */
	public String getMODULE() {
		return MODULE;
	}
	/**
	 * @param module the mODULE to set
	 */
	public void setMODULE(String module) {
		MODULE = module;
	}
	@Override
	public int compareTo(CSBSDataBean bean) {
		// TODO Auto-generated method stub
		if(Integer.valueOf(this.getNUM())>Integer.valueOf(bean.getNUM())){
			return 1;
		}else if (Integer.valueOf(this.getNUM())<Integer.valueOf(bean.getNUM())) {
			return -1;
		}else {
			return 0;
		}
	}
	/**
	 * @return the nUM
	 */
	public String getNUM() {
		return NUM;
	}
	/**
	 * @param num the nUM to set
	 */
	public void setNUM(String num) {
		NUM = num;
	}

	 
}
