package com.singlee.financial.cdtc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.cdtc.dao.BranprcdateGetDao;
import com.singlee.financial.cdtc.mapper.BranprcdateGetMapper;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class BranprcdateGetImpl implements BranprcdateGetDao{

	@Autowired
	BranprcdateGetMapper branprcdateGetMapper;
	
	@Override
	public String queryBranchDate() throws Exception {
		String queryDate = null;
		Map<String,String> map = new HashMap<String,String>();
		map.put("SUMMITDATE", "");
		try{
			 branprcdateGetMapper.queryBranchDate(map);
			 queryDate = map.get("SUMMITDATE");
		    if(queryDate != null && !"".equals(queryDate)){
		    	return queryDate;
		    }
		}catch(Exception e){
			throw e;
		}
		return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	}


}
