package com.singlee.financial.cdtc.dao.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.cdtc.dao.HldyDao;
import com.singlee.financial.cdtc.mapper.HldyMapper;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HldyDaoImpl implements HldyDao{
	@Autowired
	HldyMapper hldyMapper;
	
	@Override
	public boolean isAutoToCreateFileForAcups(Map<String,Object> map)
			throws Exception {
		boolean flag = false;
		map.put("type", "C");
		map.put("ccy", "PEK");   //PEK表示中国区节假日
		map.put("RETMSG", "");
		map.put("RETCODE", "");
		try {
			hldyMapper.isAutoToCreateFileForAcups(map);
			if("1".equals(map.get("RETMSG").toString()))
			{
				flag = true;
			}
		} catch (Exception e) {
			throw e;
		}
		return flag;
	}

}
