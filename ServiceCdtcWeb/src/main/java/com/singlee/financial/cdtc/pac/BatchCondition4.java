package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class BatchCondition4 extends Group{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("StrtDt",Field.class);
		keyClassMap.put("EndDt",Field.class);
		keyClassMap.put("YrsToMtrty",Field.class);
		keyClassMap.put("EndYrsToMtrty",Field.class);
		keyClassMap.put("QryCls",Field.class);
	}
	
	public BatchCondition4(String tag){
		super(tag,tag,new String[]{"StrtDt","EndDt","YrsToMtrty","EndYrsToMtrty","QryCls"});
	}
	public BatchCondition4(String tag,String delim){
		super(tag,tag,new String[]{"StrtDt","EndDt","YrsToMtrty","EndYrsToMtrty","QryCls"});
	}
	public void set(ISODate value){
		setField(value);
	}
	
	public ISODate get(ISODate value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(ISODate field) {
		return isSetField(field);
	}

	public boolean isSetISODate() {
		return isSetField("StrtDt");
	}
	
	
	public void set(CommonCurrencyAndAmount value){
		setField(value);
	}
	
	public CommonCurrencyAndAmount get(CommonCurrencyAndAmount value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(CommonCurrencyAndAmount field) {
		return isSetField(field);
	}

	public boolean isSetCommonCurrencyAndAmount() {
		return isSetField("YrsToMtrty");
	}
	
	public void set(QueryClassCode value){
		setField(value);
	}
	
	public QueryClassCode get(QueryClassCode value) throws FieldNotFound {
		getField(value);
		return value;
	}

	public boolean isSet(QueryClassCode field) {
		return isSetField(field);
	}

	public boolean isSetQueryClassCode() {
		return isSetField("QryCls");
	}

}
