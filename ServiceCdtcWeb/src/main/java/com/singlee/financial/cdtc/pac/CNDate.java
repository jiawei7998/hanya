/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.Date;

/**
 * @author Fang
 *
 */
public class CNDate extends EsbDateOnlyField {

	private static final long serialVersionUID = 1L;

	public CNDate(String field,String data) {
	    super(field, data);
	  }

	  public CNDate(String field,Date data) {
	    super(field, data);
	  }
}
