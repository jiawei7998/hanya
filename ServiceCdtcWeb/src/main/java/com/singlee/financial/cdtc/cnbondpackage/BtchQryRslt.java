package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.AccountIdentificationAndName4;
import com.singlee.financial.cdtc.pac.BondSettlementType6Code;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.CommonCurrencyAndAmount;
import com.singlee.financial.cdtc.pac.ContractBlockedStatusCode;
import com.singlee.financial.cdtc.pac.ContractStatusCode;
import com.singlee.financial.cdtc.pac.Exact12NumericText;
import com.singlee.financial.cdtc.pac.Exact9NumericText;
import com.singlee.financial.cdtc.pac.FaceCurrencyAndAmount;
import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODate;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.InstructionConfirmIndicatorCode;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.InstructionStatusCode;
import com.singlee.financial.cdtc.pac.Max20Text;
import com.singlee.financial.cdtc.pac.Max5NumericText;

public class BtchQryRslt extends Group {

	/**
	 * BtchQryRslt组件    批量查询结果
	 *  
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 结算指令标识,结算合同标识,付券方账户,收券方账户,业务标识号,业务类型
		结算方式1,结算方式2,债券数目,债券总额,结算金额1,结算金额2
		交割日1,交割日2,指令处理状态,合同处理状态,合同冻结状态
		最新更新时间,发令方确认标识,对手方确认标识,指令来源
	 * @param tag
	 */
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("InstrId",      Field.class);
		keyClassMap.put("CtrctId", 	    Field.class);
		keyClassMap.put("GivAcct",      AccountIdentificationAndName4.class);
		keyClassMap.put("TakAcct",      AccountIdentificationAndName4.class);
		keyClassMap.put("TxId", 	    Field.class);
		keyClassMap.put("BizTp",      	Field.class);
		keyClassMap.put("SttlmTp1",     Field.class);
		keyClassMap.put("SttlmTp2", 	Field.class);
		keyClassMap.put("BdCnt",      	Field.class);
		keyClassMap.put("AggtFaceAmt",  Field.class);
		keyClassMap.put("Val1", 	    Field.class);
		keyClassMap.put("Val2",      	Field.class);
		keyClassMap.put("Dt1",          Field.class);
		keyClassMap.put("Dt2", 	        Field.class);
		keyClassMap.put("InstrSts",     Field.class);
		keyClassMap.put("CtrctSts", 	Field.class);
		keyClassMap.put("CtrctBlckSts", Field.class);
		keyClassMap.put("LastUpdTm", 	Field.class);
		keyClassMap.put("OrgtrCnfrmInd",Field.class);
		keyClassMap.put("CtrCnfrmInd", 	Field.class);
		keyClassMap.put("InstrOrgn",    Field.class);

		keyClassMap.putAll(AccountIdentificationAndName4.keyClassMap);
	}
	
	public BtchQryRslt(String tag){
		super(tag, tag, new String[]{"InstrId","CtrctId","GivAcct","TakAcct","TxId","BizTp",
							"SttlmTp1","SttlmTp2","BdCnt","AggtFaceAmt","Val1","Val2","Dt1",
							"Dt2","InstrSts","CtrctSts","CtrctBlckSts","LastUpdTm","OrgtrCnfrmInd",
							"CtrCnfrmInd","InstrOrgn"});
	}
	
	public BtchQryRslt(String tag,String delim){
		super(tag, tag, new String[]{"InstrId","CtrctId","GivAcct","TakAcct","TxId","BizTp",
							"SttlmTp1","SttlmTp2","BdCnt","AggtFaceAmt","Val1","Val2","Dt1",
							"Dt2","InstrSts","CtrctSts","CtrctBlckSts","LastUpdTm","OrgtrCnfrmInd",
							"CtrCnfrmInd","InstrOrgn"});
	}
	
	public void set(Exact12NumericText value){
		setField(value);
	}
	
	public Exact12NumericText get(Exact12NumericText value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(Exact9NumericText value){
		setField(value);
	}
	
	public Exact9NumericText get(Exact9NumericText value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(AccountIdentificationAndName4 group){
		addGroup(group);
	}
	
	public void set(Max20Text value){
		setField(value);
	}
	
	public Max20Text get(Max20Text value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(BusinessTypeCode value){
		setField(value);
	}
	
	public BusinessTypeCode get(BusinessTypeCode value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(BondSettlementType6Code value){
		setField(value);
	}
	
	public BondSettlementType6Code get(BondSettlementType6Code value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(Max5NumericText value){
		setField(value);
	}
	
	public Max5NumericText get(Max5NumericText value) throws FieldNotFound {
		getField(value);
		return value;
	}

	public void set(FaceCurrencyAndAmount value){
		setField(value);
	}
	
	public FaceCurrencyAndAmount get(FaceCurrencyAndAmount value) throws FieldNotFound {
		getField(value);
		return value;
	}
	

	public void set(CommonCurrencyAndAmount value){
		setField(value);
	}
	
	public CommonCurrencyAndAmount get(CommonCurrencyAndAmount value) throws FieldNotFound {
		getField(value);
		return value;
	}
	

	public void set(ISODate value){
		setField(value);
	}
	
	public ISODate get(ISODate value) throws FieldNotFound {
		getField(value);
		return value;
	}

	public void set(InstructionStatusCode value){
		setField(value);
	}
	
	public InstructionStatusCode get(InstructionStatusCode value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(ContractStatusCode value){
		setField(value);
	}
	
	public ContractStatusCode get(ContractStatusCode value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(ContractBlockedStatusCode value){
		setField(value);
	}
	
	public ContractBlockedStatusCode get(ContractBlockedStatusCode value) throws FieldNotFound {
		getField(value);
		return value;
	}
	public void set(ISODateTime value){
		setField(value);
	}
	
	public ISODateTime get(ISODateTime value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(InstructionConfirmIndicatorCode value){
		setField(value);
	}
	
	public InstructionConfirmIndicatorCode get(InstructionConfirmIndicatorCode value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(InstructionOriginCode value){
		setField(value);
	}
	
	public InstructionOriginCode get(InstructionOriginCode value) throws FieldNotFound {
		getField(value);
		return value;
	}
}
	
