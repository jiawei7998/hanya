package com.singlee.financial.cdtc.pac;

public class CheckStatusEnum {
	public static enum Type {
		SL00("SL00", "DB状态正常"), 
		SL01("SL01", "DB状态异常"), 
		SL02("SL02", "核对相符"), 
		SL03("SL03", "核对不符"), 
		SL04("SL04", "未找到符合的通用结算状态报文"), 
		SL05("SL05", "通用结算状态报文缺失状态标签"), 
		SL06("SL06", "交易被资金系统冲销"), 
		SL07("SL07", "DB交易更新通用结算指令状态正常"), 
		SL08("SL08", "DB交易更新通用结算指令状态异常"),
		SL09("SL09", "DB当日通用结算指令报文"), 
		SL10("SL10", "DB更新匹配XML路径正常"), 
		SL11("SL11", "DB更新匹配XML路径异常"), 
		SL12("SL12", "DB通用结算指令正常存在"), 
		SL13("SL13", "DB通用结算指令创建异常"), 
		SL14("SL14", "通用结算指令发送CDTC回执正常"), 
		SL15("SL15", "通用结算指令发送CDTC回执异常"), 
		SL16("SL16", "金额核对不符"), 
		SL17("SL17", "债券核对不符"), 
		SL18("SL18", "清算日核对不符"), 
		SL19("SL19", "结算方式核对不符"), 
		SL20("SL20", "无第三方业务编号"), 
		SD01("SD01", "已发送");
		private String type;

		private String name;

		private Type(String type, String name) {
			this.type = type;
			this.name = name;
		}

		// 普通方法
		public static String getName(String type) {
			for (Type c : Type.values()) {
				if (c.getType().equalsIgnoreCase(type)) {
					return c.name;
				}
			}
			return "";
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return this.type + ":" + this.name;
		}
	}
}
