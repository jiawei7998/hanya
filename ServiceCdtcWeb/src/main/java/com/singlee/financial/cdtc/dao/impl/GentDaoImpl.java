package com.singlee.financial.cdtc.dao.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.cdtc.bean.GentBean;
import com.singlee.financial.cdtc.dao.GentDao;
import com.singlee.financial.cdtc.mapper.GentMapper;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class GentDaoImpl implements GentDao{
	@Autowired
	GentMapper gentMapper;
	
	@Override
	public GentBean queryGent(Map<String,Object> map) throws Exception {
		GentBean gentBean = null;
		map.put("tableid", "SL_CDTC");
		map.put("tablevalue", "SL_CDTC_AMT");
		try{
			gentBean = gentMapper.queryGent(map);
		}catch(Exception e){
			throw e;
		}
		return gentBean;
	}

}
