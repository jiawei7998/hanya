package com.singlee.financial.cdtc.pac;

import java.util.Date;

public class ISOTime extends UtcTimeOnlyField {
	 static final long serialVersionUID = 20080903L;

	  public ISOTime(String field,String data) {
	    super(field, data, true);
	  }

	  public ISOTime(String field,Date data) {
	    super(field, data, true);
	  }
	  
	  
}
