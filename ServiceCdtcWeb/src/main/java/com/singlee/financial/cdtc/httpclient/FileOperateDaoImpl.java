package com.singlee.financial.cdtc.httpclient;

import java.io.File;

public class FileOperateDaoImpl implements FileOperateDao {

	@Override
	public void messageXMLFileSaveLocal(String data,String path, String filename, String date) throws Exception {
		WriteToFile.writeToFile(data, path, filename, date);
	}

	@Override
	public void FileIsNotExist(String localPath,String filename,String date) throws Exception {
		File oldFile = new File(localPath+filename);
		String newPath = localPath+date+"/";
		File newFilePath = new File(newPath);
		if(!newFilePath.exists()){
			newFilePath.mkdirs();
			
		}
		File newFile = new File(newPath+oldFile.getName());
		if(newFile.exists()){
			newFile.delete();
		}
		oldFile.renameTo(newFile);
		oldFile.delete();
	}

}
