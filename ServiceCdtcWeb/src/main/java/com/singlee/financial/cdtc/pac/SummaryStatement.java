package com.singlee.financial.cdtc.pac;

import java.util.Date;
import java.util.HashMap;

public class SummaryStatement  extends Group {


	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Dt",       Field.class);
		keyClassMap.put("AcctLdg",  Group.class);
		keyClassMap.putAll(AccountLedger.keyClassMap);
	}
	
	public SummaryStatement(String tag){
		
		super(tag,tag, new String[] { "Dt", "AcctLdg"});
	}
	
	public SummaryStatement(String tag,String delim){
		
		super(tag,tag, new String[] { "Dt", "AcctLdg"});
	}
	
	public void set(ISODate value) {
        setField(value);
      }

      public ISODate get(ISODate value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public ISODate getISODate() throws FieldNotFound
      {
    	  ISODate value = new ISODate("Dt",new Date());
          getField(value);

          return value;
      }

      public boolean isSet(ISODate field) {
        return isSetField(field);
      }

      public boolean isSetISODate() {
        return isSetField("Dt");
      }
      
      
      public void set(AccountLedger group) {
          addGroup(group);
        }

        public boolean isSet(AccountLedger field) {
          return isSetField(field.getFieldTag());
        }

        public boolean isSetAccountLedger() {
          return isSetField("AcctLdg");
        }
      
      public void toXmlForSuperParent(StringBuffer xmlBuffer)
      {
    	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
    	  this.toXml(xmlBuffer);
    	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
      }
      public static void main(String[] args) {
    	  CommonCurrencyAndAmount commonCurrencyAndAmount = new CommonCurrencyAndAmount("Bal","CNY");
    	  commonCurrencyAndAmount.setValue("12345678.67");
    	  AccountLedgerTypeCode accountLedgerTypeCode = new AccountLedgerTypeCode("LdgTp");
    	  accountLedgerTypeCode.setValue(AccountLedgerTypeCode.Type.AL00.getType());
    	  AccountLedger accountLedger = new AccountLedger("AcctLdg");
    	  accountLedger.set(commonCurrencyAndAmount);
    	  accountLedger.set(accountLedgerTypeCode);
    	  ISODate isDate = new ISODate("Dt",new Date());
    	  SummaryStatement summaryStatement = new SummaryStatement("SummaryStatement");
    	  summaryStatement.set(isDate);
    	  summaryStatement.set(accountLedger);
    	  StringBuffer stringBuffer = new StringBuffer();
    	  summaryStatement.toXmlForSuperParent(stringBuffer);
    	  System.out.println(stringBuffer.toString());
    	  
	}
}
