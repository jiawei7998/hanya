package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class LocalMktTimeField  extends LocalDateField
{
	  private static final long serialVersionUID = 1L;
	  private boolean includeMilliseconds = true;

	  private static TimeZone timezone = TimeZone.getTimeZone("GMT+08:00");
	  private static Calendar calendar;

	  public LocalMktTimeField(String field)
	  {
	    super(field, LocalMktTimeConverter.convert(createDate(), true));
	  }

	  protected LocalMktTimeField(String field, Date data)
	  {
	    super(field, LocalMktTimeConverter.convert(data, true));
	  }

	  protected LocalMktTimeField(String field, String data)
	  {
	    super(field, data);
	  }

	  public LocalMktTimeField(String field, boolean includeMilliseconds)
	  {
	    super(field, LocalMktTimeConverter.convert(createDate(), includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected LocalMktTimeField(String field, Date data, boolean includeMilliseconds)
	  {
	    super(field, LocalMktTimeConverter.convert(data, includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected LocalMktTimeField(String field, String data, boolean includeMilliseconds)
	  {
	    super(field, data);
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  boolean showMilliseconds() {
	    return this.includeMilliseconds;
	  }

	  public void setValue(Date value) {
	    setValue(LocalMktTimeConverter.convert(value, true));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(LocalMktTimeConverter.convert(value, true));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }
	}
