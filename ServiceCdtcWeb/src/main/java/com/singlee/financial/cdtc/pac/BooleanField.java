package com.singlee.financial.cdtc.pac;

public class BooleanField extends Field<Boolean>
{
	  private static final long serialVersionUID = 1L;

	  public BooleanField(String field)
	  {
	    super(field, Boolean.valueOf(false));
	  }

	  public BooleanField(String field, Boolean data) {
	    super(field, data);
	  }

	  public BooleanField(String field, boolean data) {
	    super(field, Boolean.valueOf(data));
	  }

	  public void setValue(Boolean value) {
	    setObject(value);
	  }

	  public void setValue(boolean value) {
	    setObject(Boolean.valueOf(value));
	  }

	  public boolean getValue() {
	    return ((Boolean)getObject()).booleanValue();
	  }

	  public boolean valueEquals(Boolean value) {
	    return ((Boolean)getObject()).equals(value);
	  }

	  public boolean valueEquals(boolean value) {
	    return ((Boolean)getObject()).equals(Boolean.valueOf(value));
	  }
	}