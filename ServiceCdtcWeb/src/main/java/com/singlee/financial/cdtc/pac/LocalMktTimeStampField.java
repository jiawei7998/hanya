package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class LocalMktTimeStampField  extends LocalDateField
{
	  private static final long serialVersionUID = 1L;
	  private boolean includeMilliseconds = true;

	  private static TimeZone timezone = TimeZone.getTimeZone("GMT+08:00");
	  private static Calendar calendar;

	  public LocalMktTimeStampField(String field)
	  {
	    super(field, LocalMktTimeStampConverter.convert(createDate(), true));
	  }

	  protected LocalMktTimeStampField(String field, Date data)
	  {
	    super(field, LocalMktTimeStampConverter.convert(data, true));
	  }

	  protected LocalMktTimeStampField(String field, String data)
	  {
	    super(field, data);
	  }

	  public LocalMktTimeStampField(String field, boolean includeMilliseconds)
	  {
	    super(field, LocalMktTimeStampConverter.convert(createDate(), includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected LocalMktTimeStampField(String field, Date data, boolean includeMilliseconds)
	  {
	    super(field, LocalMktTimeStampConverter.convert(data, includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected LocalMktTimeStampField(String field, String data, boolean includeMilliseconds)
	  {
	    super(field, data);
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  boolean showMilliseconds() {
	    return this.includeMilliseconds;
	  }

	  public void setValue(Date value) {
	    setValue(LocalMktTimeStampConverter.convert(value, true));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(LocalMktTimeStampConverter.convert(value, true));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }
	}
