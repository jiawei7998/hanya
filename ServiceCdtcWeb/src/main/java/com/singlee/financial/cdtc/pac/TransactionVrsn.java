/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *报文版本标识
 */
public class TransactionVrsn extends Group {

	private static final long serialVersionUID = 1L;
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("VrsnID", Field.class);
	}
	
	public TransactionVrsn(String tag){
		super(tag,tag,new String[]{"VrsnID"});
	}
	public TransactionVrsn(String tag,String belim){
		super(tag,tag,new String[]{"VrsnID"});
	
	}
	public void set(Max2NumericText field){
		setField(field);
	}
	
}
