package com.singlee.financial.cdtc.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.cdtc.bean.BtchQryRsltBean;
import com.singlee.financial.cdtc.cnbondpackage.SettlementBusinessBatchQueryStatusReportV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSettlBusiBatchStatusV01;
import com.singlee.financial.cdtc.dao.CdtcRequestMessageCreateDao;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.AccountIdentificationAndName4;
import com.singlee.financial.cdtc.pac.Condition;
import com.singlee.financial.cdtc.pac.Exact11Text;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODate;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max12Text;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.OperatorStatusCode;
import com.singlee.financial.cdtc.pac.Priority4Code;
import com.singlee.financial.cdtc.pac.RequestTypeCode;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.pac.TransactionMsg;
import com.singlee.financial.cdtc.pac.TransactionOprtrFlg;
import com.singlee.financial.cdtc.util.EncodingUtil;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcRequestMessageCreateDaoImpl implements
		CdtcRequestMessageCreateDao {

	@Override
	public synchronized SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS04AndBJ0300(
			String txFlowId, String acctQryId,String qryCondId) throws Exception {
		// TODO Auto-generated method stub
		SettlementBusinessQueryRequest settlementBusinessQueryRequest  =new SettlementBusinessQueryRequest("Document");
		TransactionMsg transactionMsg  = new TransactionMsg("Msg");
		Max35Text max35Text  = new Max35Text("TxFlowId");
		max35Text.setValue(txFlowId);
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setField(Priority4Code.Type.PT02.getType());
		ISODateTime isDateTime = new ISODateTime("CreDtTm",new Date());
		transactionMsg.set(max35Text);transactionMsg.set(priority4Code);transactionMsg.set(isDateTime);
		settlementBusinessQueryRequest.set(transactionMsg);
		
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		RequestTypeCode requestTypeCode = new RequestTypeCode("ReqTp");
		requestTypeCode.setValue(RequestTypeCode.Type.BJ0300.getType());
		OperatorStatusCode operatorStatusCode = new OperatorStatusCode("OprtrSts");
		operatorStatusCode.setValue(OperatorStatusCode.Type.OS04.getType());
		transactionOprtrFlg.set(requestTypeCode);transactionOprtrFlg.set(operatorStatusCode);
		settlementBusinessQueryRequest.set(transactionOprtrFlg);
		
		AccountIdentificationAndName4 accountIdentificationAndName4 = new AccountIdentificationAndName4("AcctQry");
		Exact11Text exact11Text = new Exact11Text("Id");
		exact11Text.setValue(acctQryId);
		accountIdentificationAndName4.set(exact11Text);
		settlementBusinessQueryRequest.set(accountIdentificationAndName4);
		
		Condition condition = new Condition("QryCond");
		Max12Text max12Text = new Max12Text("Id");
		max12Text.setValue(qryCondId);
		condition.set(max12Text);
		settlementBusinessQueryRequest.set(condition);
		return settlementBusinessQueryRequest;
		
	}
	@Override
	public synchronized SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS03AndBJ0300(
			String acctQryId, String qryCondDt) throws Exception {
		// TODO Auto-generated method stub
		SettlementBusinessQueryRequest settlementBusinessQueryRequest  =new SettlementBusinessQueryRequest("Document");
		TransactionMsg transactionMsg  = new TransactionMsg("Msg");
		Max35Text max35Text  = new Max35Text("TxFlowId");
		max35Text.setValue(qryCondDt.replace("-", "")+acctQryId+DateUtil.convertDateToYYYYMMDDHHMMSS(new Date()));
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setField(Priority4Code.Type.PT02.getType());
		ISODateTime isDateTime = new ISODateTime("CreDtTm",new Date());
		transactionMsg.set(max35Text);transactionMsg.set(priority4Code);transactionMsg.set(isDateTime);
		settlementBusinessQueryRequest.set(transactionMsg);
		
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		RequestTypeCode requestTypeCode = new RequestTypeCode("ReqTp");
		requestTypeCode.setValue(RequestTypeCode.Type.BJ0300.getType());
		OperatorStatusCode operatorStatusCode = new OperatorStatusCode("OprtrSts");
		operatorStatusCode.setValue(OperatorStatusCode.Type.OS03.getType());
		transactionOprtrFlg.set(requestTypeCode);transactionOprtrFlg.set(operatorStatusCode);
		settlementBusinessQueryRequest.set(transactionOprtrFlg);
		
		AccountIdentificationAndName4 accountIdentificationAndName4 = new AccountIdentificationAndName4("AcctQry");
		Exact11Text exact11Text = new Exact11Text("Id");
		exact11Text.setValue(acctQryId);
		accountIdentificationAndName4.set(exact11Text);
		settlementBusinessQueryRequest.set(accountIdentificationAndName4);
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Condition condition = new Condition("QryCond");
		ISODate isoDate = new ISODate("QryDt",df.parse(qryCondDt));
		condition.set(isoDate);
		settlementBusinessQueryRequest.set(condition);
		return settlementBusinessQueryRequest;
	}

	@Override
	public synchronized List<BtchQryRslt> transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(
			String xmlBufferStr) throws Exception {
		// TODO Auto-generated method stub
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDoc = reader.read(new InputStreamReader(new ByteArrayInputStream(xmlBufferStr.getBytes(EncodingUtil.getEncoding(xmlBufferStr)))));  
		SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
		spotSettlementInstruction.unionHashMap(SettlementBusinessBatchQueryStatusReportV01.type);
		spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
		SettlementBusinessBatchQueryStatusReportV01 settlementBusinessBatchQueryStatusReportV01 = null;
		if(spotSettlementInstruction.hasGroup("Document"))
		{
			List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
			for(Group group : groupsList)
			{
				StringBuffer xmlBuffer = new StringBuffer();
				group.toXmlForParent(xmlBuffer);
				settlementBusinessBatchQueryStatusReportV01 = new SettlementBusinessBatchQueryStatusReportV01("Document");
				Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
				XMLRevSettlBusiBatchStatusV01.getElementList(document.getRootElement(), settlementBusinessBatchQueryStatusReportV01);
			}
		}
		List<BtchQryRslt> btchQryRsltList = new ArrayList<BtchQryRslt>();
		BtchQryRslt btchQryRslt  = null;
		if(null != settlementBusinessBatchQueryStatusReportV01)
		{
			if(settlementBusinessBatchQueryStatusReportV01.hasGroup("BtchQryRslt"))
			{
				List<Group> grList = settlementBusinessBatchQueryStatusReportV01.getGroups("BtchQryRslt");
				for(Group group : grList)
				{
					btchQryRslt = new BtchQryRslt();
					if(group.getGroups("GivAcct").get(0).getfieldsForFieldMap() != null && group.getGroups("GivAcct").get(0).getfieldsForFieldMap().containsKey("Nm")) {
						btchQryRslt.setGivAcctNm(group.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
					}
					btchQryRslt.setGivAcctId(group.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
					
					if(group.getGroups("TakAcct").get(0).getfieldsForFieldMap() != null && group.getGroups("TakAcct").get(0).getfieldsForFieldMap().containsKey("Nm")) {
						btchQryRslt.setTakAcctNm(group.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
					}
					btchQryRslt.setTakAcctId(group.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
					
					btchQryRslt.setInstrId(!group.getfieldsForFieldMap().containsKey("InstrId")?"":group.getfieldsForFieldMap().get("InstrId").getObject().toString());
					btchQryRslt.setTxId(!group.getfieldsForFieldMap().containsKey("TxId")?"":group.getfieldsForFieldMap().get("TxId").getObject().toString());
					btchQryRslt.setBizTp(!group.getfieldsForFieldMap().containsKey("BizTp")?"":group.getfieldsForFieldMap().get("BizTp").getObject().toString());
					btchQryRslt.setSttlmTp1(!group.getfieldsForFieldMap().containsKey("SttlmTp1")?"":group.getfieldsForFieldMap().get("SttlmTp1").getObject().toString());
					btchQryRslt.setSttlmTp2(!group.getfieldsForFieldMap().containsKey("SttlmTp2")?"":group.getfieldsForFieldMap().get("SttlmTp2").getObject().toString());
					btchQryRslt.setBdCnt(!group.getfieldsForFieldMap().containsKey("BdCnt")?null:Integer.parseInt(group.getfieldsForFieldMap().get("BdCnt").getObject().toString()));
					btchQryRslt.setAggtFaceAmt(!group.getfieldsForFieldMap().containsKey("AggtFaceAmt")?null:new BigDecimal(group.getfieldsForFieldMap().get("AggtFaceAmt").getObject().toString()));
					btchQryRslt.setVal1(!group.getfieldsForFieldMap().containsKey("Val1")?null:new BigDecimal(group.getfieldsForFieldMap().get("Val1").getObject().toString()));
					btchQryRslt.setVal2(!group.getfieldsForFieldMap().containsKey("Val2")?null:new BigDecimal(group.getfieldsForFieldMap().get("Val2").getObject().toString()));
					btchQryRslt.setDt1(!group.getfieldsForFieldMap().containsKey("Dt1")?"":group.getfieldsForFieldMap().get("Dt1").getObject().toString());
					btchQryRslt.setDt2(!group.getfieldsForFieldMap().containsKey("Dt2")?"":group.getfieldsForFieldMap().get("Dt2").getObject().toString());
					btchQryRslt.setInstrSts(!group.getfieldsForFieldMap().containsKey("InstrSts")?"":group.getfieldsForFieldMap().get("InstrSts").getObject().toString());
					btchQryRslt.setInstrOrgn(!group.getfieldsForFieldMap().containsKey("InstrOrgn")?"":group.getfieldsForFieldMap().get("InstrOrgn").getObject().toString());
					btchQryRslt.setCtrctId(!group.getfieldsForFieldMap().containsKey("CtrctId")?"":group.getfieldsForFieldMap().get("CtrctId").getObject().toString());
					
					btchQryRslt.setCtrctSts(!group.getfieldsForFieldMap().containsKey("CtrctSts")?"":group.getfieldsForFieldMap().get("CtrctSts").getObject().toString());
					btchQryRslt.setCtrctBlckSts(!group.getfieldsForFieldMap().containsKey("CtrctBlckSts")?"":group.getfieldsForFieldMap().get("CtrctBlckSts").getObject().toString());
					btchQryRslt.setLastUpdTm(!group.getfieldsForFieldMap().containsKey("LastUpdTm")?"":group.getfieldsForFieldMap().get("LastUpdTm").getObject().toString());
					btchQryRslt.setOrgtrCnfrmInd(!group.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd")?"":group.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString());
					btchQryRslt.setCtrCnfrmInd(!group.getfieldsForFieldMap().containsKey("CtrCnfrmInd")?"":group.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString());
					btchQryRslt.setInstrOrgn(!group.getfieldsForFieldMap().containsKey("InstrOrgn")?"":group.getfieldsForFieldMap().get("InstrOrgn").getObject().toString());
					
					btchQryRsltList.add(btchQryRslt);
				}
			}
		}
		return btchQryRsltList;
	}

	@Override
	public SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS03AndBJ0301(
			String acctQryId, String qryCondDt) throws Exception {
		// TODO Auto-generated method stub
		SettlementBusinessQueryRequest settlementBusinessQueryRequest  =new SettlementBusinessQueryRequest("Document");
		TransactionMsg transactionMsg  = new TransactionMsg("Msg");
		Max35Text max35Text  = new Max35Text("TxFlowId");
		max35Text.setValue(qryCondDt.replace("-", "")+acctQryId+DateUtil.convertDateToYYYYMMDDHHMMSS(new Date()));
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setField(Priority4Code.Type.PT02.getType());
		ISODateTime isDateTime = new ISODateTime("CreDtTm",new Date());
		transactionMsg.set(max35Text);transactionMsg.set(priority4Code);transactionMsg.set(isDateTime);
		settlementBusinessQueryRequest.set(transactionMsg);
		
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		RequestTypeCode requestTypeCode = new RequestTypeCode("ReqTp");
		requestTypeCode.setValue(RequestTypeCode.Type.BJ0301.getType());
		OperatorStatusCode operatorStatusCode = new OperatorStatusCode("OprtrSts");
		operatorStatusCode.setValue(OperatorStatusCode.Type.OS03.getType());
		transactionOprtrFlg.set(requestTypeCode);transactionOprtrFlg.set(operatorStatusCode);
		settlementBusinessQueryRequest.set(transactionOprtrFlg);
		
		AccountIdentificationAndName4 accountIdentificationAndName4 = new AccountIdentificationAndName4("AcctQry");
		Exact11Text exact11Text = new Exact11Text("Id");
		exact11Text.setValue(acctQryId);
		accountIdentificationAndName4.set(exact11Text);
		settlementBusinessQueryRequest.set(accountIdentificationAndName4);
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Condition condition = new Condition("QryCond");
		ISODate isoDate = new ISODate("QryDt",df.parse(qryCondDt));
		condition.set(isoDate);
		settlementBusinessQueryRequest.set(condition);
		return settlementBusinessQueryRequest;
	}
	@Override
	public List<BtchQryRsltBean> transformatCSBS00600101ForQryCondConditionOS03AndBJ0301(
			String xmlBufferStr) throws Exception {
		// TODO Auto-generated method stub
		SAXReader reader = new SAXReader();
		org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(xmlBufferStr).getBytes("UTF-8")));  
		SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
		spotSettlementInstruction.unionHashMap(SettlementBusinessBatchQueryStatusReportV01.type);
		spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
		SettlementBusinessBatchQueryStatusReportV01 settlementBusinessBatchQueryStatusReportV01 = null;
		if(spotSettlementInstruction.hasGroup("Document"))
		{
			List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
			for(Group group : groupsList)
			{
				StringBuffer xmlBuffer = new StringBuffer();
				group.toXmlForParent(xmlBuffer);
				settlementBusinessBatchQueryStatusReportV01 = new SettlementBusinessBatchQueryStatusReportV01("Document");
				Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
				XMLRevSettlBusiBatchStatusV01.getElementList(document.getRootElement(), settlementBusinessBatchQueryStatusReportV01);
			}
		}
		List<BtchQryRsltBean> btchQryRsltList = new ArrayList<BtchQryRsltBean>();
		BtchQryRsltBean btchQryRslt  = null;
		if(null != settlementBusinessBatchQueryStatusReportV01)
		{
			if(settlementBusinessBatchQueryStatusReportV01.hasGroup("BtchQryRslt"))
			{
				List<Group> grList = settlementBusinessBatchQueryStatusReportV01.getGroups("BtchQryRslt");
				for(Group group : grList)
				{
					btchQryRslt = new BtchQryRsltBean();
					if(group.getGroups("GivAcct").get(0).getfieldsForFieldMap() != null && group.getGroups("GivAcct").get(0).getfieldsForFieldMap().containsKey("Nm")) {
						btchQryRslt.setGivAcctNm(group.hasGroup("GivAcct")?group.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
					}
					btchQryRslt.setGivAcctId(group.hasGroup("GivAcct")?group.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString():"");
					
					if(group.getGroups("TakAcct").get(0).getfieldsForFieldMap() != null && group.getGroups("TakAcct").get(0).getfieldsForFieldMap().containsKey("Nm")) {
						btchQryRslt.setTakAcctNm(group.hasGroup("TakAcct")?group.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
					}
					btchQryRslt.setTakAcctId(group.hasGroup("TakAcct")?group.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString():"");
					
					btchQryRslt.setInstrId(!group.getfieldsForFieldMap().containsKey("InstrId")?"":group.getfieldsForFieldMap().get("InstrId").getObject().toString());
					btchQryRslt.setBizTp(!group.getfieldsForFieldMap().containsKey("BizTp")?"":group.getfieldsForFieldMap().get("BizTp").getObject().toString());
					btchQryRslt.setAggtFaceAmt(!group.getfieldsForFieldMap().containsKey("AggtFaceAmt")?null:new BigDecimal(group.getfieldsForFieldMap().get("AggtFaceAmt").getObject().toString()));
					btchQryRslt.setDt1(!group.getfieldsForFieldMap().containsKey("Dt1")?"":group.getfieldsForFieldMap().get("Dt1").getObject().toString());
					btchQryRslt.setCtrctId(!group.getfieldsForFieldMap().containsKey("CtrctId")?"":group.getfieldsForFieldMap().get("CtrctId").getObject().toString());
					btchQryRslt.setCtrctSts(!group.getfieldsForFieldMap().containsKey("CtrctSts")?"":group.getfieldsForFieldMap().get("CtrctSts").getObject().toString());
					
					btchQryRsltList.add(btchQryRslt);
				}
			}
		}
		return btchQryRsltList;
	}
	@Override
	public SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS04AndBJ0301(
			String acctQryId, String qryCondId) throws Exception {
		// TODO Auto-generated method stub
		SettlementBusinessQueryRequest settlementBusinessQueryRequest  =new SettlementBusinessQueryRequest("Document");
		TransactionMsg transactionMsg  = new TransactionMsg("Msg");
		Max35Text max35Text  = new Max35Text("TxFlowId");
		max35Text.setValue(qryCondId+acctQryId+DateUtil.convertDateToYYYYMMDDHHMMSS(new Date()));
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setField(Priority4Code.Type.PT02.getType());
		ISODateTime isDateTime = new ISODateTime("CreDtTm",new Date());
		transactionMsg.set(max35Text);transactionMsg.set(priority4Code);transactionMsg.set(isDateTime);
		settlementBusinessQueryRequest.set(transactionMsg);
		
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		RequestTypeCode requestTypeCode = new RequestTypeCode("ReqTp");
		requestTypeCode.setValue(RequestTypeCode.Type.BJ0301.getType());
		OperatorStatusCode operatorStatusCode = new OperatorStatusCode("OprtrSts");
		operatorStatusCode.setValue(OperatorStatusCode.Type.OS04.getType());
		transactionOprtrFlg.set(requestTypeCode);transactionOprtrFlg.set(operatorStatusCode);
		settlementBusinessQueryRequest.set(transactionOprtrFlg);
		
		AccountIdentificationAndName4 accountIdentificationAndName4 = new AccountIdentificationAndName4("AcctQry");
		Exact11Text exact11Text = new Exact11Text("Id");
		exact11Text.setValue(acctQryId);
		accountIdentificationAndName4.set(exact11Text);
		settlementBusinessQueryRequest.set(accountIdentificationAndName4);
		
		Condition condition = new Condition("QryCond");
		Max12Text max12Text = new Max12Text("Id");
		max12Text.setValue(qryCondId);
		condition.set(max12Text);
		settlementBusinessQueryRequest.set(condition);
		return settlementBusinessQueryRequest;
	}
}
