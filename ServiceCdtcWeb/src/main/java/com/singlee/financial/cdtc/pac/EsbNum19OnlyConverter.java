/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.text.DecimalFormat;

/**
 * @author Fang
 * 
 */
public class EsbNum19OnlyConverter {

	public static String convert(int cifNo) {
		DecimalFormat df = new DecimalFormat("0000000000000000000");
		return df.format(cifNo);
	}
	
	/**
	 * 格式化字符，去掉前面零
	 * 
	 * @param str
	 * @return
	 */
	public static String convert(String str) {
		int num = 0;
		for (int i = 0; i < str.length(); i++) {
			if ('0' != str.charAt(i) || i == (str.length() - 1)) {
				num = i;
				break;
			}
		}
		return str.substring(num, str.length());
	}
}
