/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 * 
 */
public class ContractStatusInfo extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("CtrctId", Field.class);
		keyClassMap.put("CtrctSts", Field.class);
		keyClassMap.put("CtrctBlckSts", Field.class);
		keyClassMap.put("CtrctFaildRsn", Field.class);
		keyClassMap.put("LastUpdTm", Field.class);
		keyClassMap.put("CtrctQryRsltCd", Field.class);
		keyClassMap.put("CtrctQryRsltInf", Field.class);
	}
	
	public ContractStatusInfo(String tag) {
		super(tag, tag, new String[] { "CtrctId", "CtrctSts", "CtrctBlckSts",
				"CtrctFaildRsn", "LastUpdTm","CtrctQryRsltCd","CtrctQryRsltInf"});
	}
	public ContractStatusInfo(String tag,String delim) {
		super(tag, tag, new String[] { "CtrctId", "CtrctSts", "CtrctBlckSts",
				"CtrctFaildRsn", "LastUpdTm","CtrctQryRsltCd","CtrctQryRsltInf"});
	}
	public void set(Exact9NumericText field){
		setField(field);
	}
	public void set(ContractStatusCode field){
		setField(field);
	}
	public void set(ContractBlockedStatusCode field){
		setField(field);
	}
	public void set(ContractFailedReasonCode field){
		setField(field);
	}
	public void set(ISODateTime field){
		setField(field);
	}
	public void set(Max8Text field){
		setField(field);
	}
	public void set(Max35Text field){
		setField(field);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

}
