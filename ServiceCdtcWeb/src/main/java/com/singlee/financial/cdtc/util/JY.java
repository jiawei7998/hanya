package com.singlee.financial.cdtc.util;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;



/**
 * 检查条件，并将不符合条件的 情况下，扔出异常（RException运行期异常，CException编译器异常）
 */
public class JY {

	/**
	 * require 扔出的是 RException
	 */
	public static void require(final boolean condition, final String format,
			final Object... objects) throws RException {
		if (!condition) {
			throw new RException(String.format(format, objects));}
	}

	/**
	 */
	public static void require(final boolean condition, final String message)
			throws RException {
		if (!condition) {
			throw new RException(message);}
	}

	public static void raiseRException(final String title, final Throwable t) {
		if (t == null) {
			raiseRException(title);
		}else{
			JY.error(title,t);
			raiseRException(title + "\r\n" + ExceptionUtils.getRootCauseMessage(t));
		}
	}
	public static void raise(final String message, final Object... objects) throws RuntimeException {
		raiseRException(message, objects);
	}
	public static void raise(final String message) throws RuntimeException {
		raiseRException(message);
	}

	public static void raiseRException(final String message) throws RuntimeException {
		throw new RException(message);
	}

	public static void raiseRException(final String format, final Object... objects)
			throws RuntimeException {
		throw new RException(String.format(format, objects));
	}


	public static void raiseCException(final String title, final Throwable t)throws CException {
		if (t == null) {
			raiseCException(title);
		}else{
			JY.error(title,t);
			raiseCException(title + "\r\n" + ExceptionUtils.getRootCauseMessage(t));
		}
	}

	public static void raiseCException(final String message) throws CException {
		throw new CException(message);
	}

	public static void raiseCException(final String format, final Object... objects)
			throws CException {
		throw new CException(String.format(format, objects));
	}
	
	
	/**
	 * ensure 扔出的是 CException
	 * @param condition
	 * @param format
	 * @param objects
	 * @throws CException
	 */
	public static void ensure(final boolean condition, final String format,
			final Object... objects) throws CException {
		if (!condition) {
			throw new CException(String.format(format, objects));}
	}

	/**
	 * Throws an error if a <b>post-condition</b> is not verified
	 * <p>
	 * 
	 * @note this method should <b>never</b> be removed from bytecode by
	 *       AspectJ. If you do so, you must be plenty sure of effects and risks
	 *       of this decision.
	 *       <p>
	 * @param condition
	 *            is a condition to be verified
	 * @param message
	 *            is a message emitted.
	 * @throws a
	 *             LibraryException if the condition is not met
	 */
	public static void ensure(final boolean condition, final String message)
			throws CException {
		if (!condition) {
			throw new CException(message);}
	}

	private static Logger log;

	/**
	 * <p>
	 * 
	 */
	public final static void setLog(final Logger logger) {
		log = logger;
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void error(final String message) {
		if (log != null) {
			log.error(message);
		} else {
			System.err.printf("ERROR: %s\n", message);
		}
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void error(final String message, final Throwable t) {
		//String err = message;// + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		if (log != null) {
			log.error(message, t);
		} else {
			String err = message  + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
			System.err.printf("ERROR: %s\n", err);
		}
	}
	public static void error(final String message, final Throwable t, Object... obj) {
		String err = String.format(message, obj) ;// + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		error(err,t);
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void error(final Throwable t) {
		error("", t);
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void warn(final String message) {
		if (log != null) {
			log.warn(message);
		} else {
			System.err.printf("WARN: %s\n", message);
		}
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void warn(final String message, final Throwable t) {
		String wrn = message + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		warn(wrn);
	}
	public static void warn(final String message, final Throwable t, Object... obj) {
		String wrn = String.format(message, obj)  + "\r\n" + ExceptionUtils.getRootCauseMessage(t);
		warn(wrn);
	}
	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void warn(final Throwable t) {
		warn("",t);
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void info(final String message) {
		if (log != null) {
			log.info(message);
		} else {
			System.err.printf("INFO: %s\n", message);
		}
	}

	public static void info(final String message, Object... obj) {
		info(String.format(message, obj));
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void debug(final String message) {
		if (log != null) {
			log.debug(message);
		} else {
			System.err.printf("DEBUG: %s\n", message);
		}
	}

	/**
	 * This method unconditionally emits a message to the logging system but
	 * does not throw any exception.
	 * 
	 * @param message
	 *            is a message to be emitted
	 */
	public static void debug(String message, Object... obj) {
		debug(String.format(message, obj));
	}

	/**
	 * 是否debug打开
	 * 
	 * @return
	 */
	public static boolean isLogDebugEnabled() {
		if (log != null) {
			return log.isDebugEnabled();
		} else {
			return true;
		}
	}
	
}
