/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class TransactionSndr extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("SndrId", Field.class);
		keyClassMap.put("SndrSysId", Field.class);
	}
	
	public TransactionSndr(String tag){
		super(tag,tag,new String[]{"SndrId","SndrSysId"});
	}
	public TransactionSndr(String tag,String belim){
		super(tag,tag,new String[]{"SndrId","SndrSysId"});
	
	}
	public void set(Max12Text field){
		setField(field);
	}
	
	public void set(Max2Text field){
		setField(field);
	}
}
