/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class TransactionGivInf extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("Nm", Field.class);
		keyClassMap.put("PrtryId", AgentParty.class);
		keyClassMap.put("AgtPty", GenericIdentification4.class);
		keyClassMap.putAll(AgentParty.keyClassMap);
		keyClassMap.putAll(GenericIdentification4.keyClassMap);
	}
	
	public TransactionGivInf(String tag) {
		super(tag, tag, new String[] { "Nm","PrtryId","AgtPty"});
	}
	public TransactionGivInf(String tag,String delim) {
		super(tag, tag, new String[] { "Nm","PrtryId","AgtPty"});
	}
	
	public void set(Max70Text field){
		setField(field);
	}
	public void set(AgentParty group){
		addGroup(group);
	}
	public void set(GenericIdentification4 group){
		addGroup(group);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	
}
