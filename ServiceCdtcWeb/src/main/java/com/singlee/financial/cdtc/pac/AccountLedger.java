package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class AccountLedger extends Group{
private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("LdgTp",  Field.class);
		keyClassMap.put("Bal",    Field.class);
	}
	public AccountLedger(String tag){
		super(tag,tag, new String[] { "LdgTp", "Bal"});
	}
	
	public AccountLedger(String tag,String delim){
		super(tag,tag, new String[] { "LdgTp", "Bal"});
	}
	 
	public void set(AccountLedgerTypeCode value) {
        
		setField(value);
      
	}

      public AccountLedgerTypeCode get(AccountLedgerTypeCode value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public AccountLedgerTypeCode getAccountLedgerTypeCode() throws FieldNotFound
      {
    	  AccountLedgerTypeCode value = new AccountLedgerTypeCode("LdgTp");
          getField(value);

          return value;
      }

      public boolean isSet(AccountLedgerTypeCode field) {
        return isSetField(field);
      }

      public boolean isSetAccountLedgerTypeCode() {
        return isSetField("LdgTp");
      }
      
      
      
      public void set(CommonCurrencyAndAmount value) {
          setField(value);
        }

        public CommonCurrencyAndAmount get(CommonCurrencyAndAmount value) throws FieldNotFound
        {
          getField(value);

          return value;
        }

        public CommonCurrencyAndAmount getCommonCurrencyAndAmount() throws FieldNotFound
        {
        	CommonCurrencyAndAmount value = new CommonCurrencyAndAmount("Bal","CNY");
            getField(value);

            return value;
        }

        public boolean isSet(CommonCurrencyAndAmount field) {
          return isSetField(field);
        }

        public boolean isSetCommonCurrencyAndAmount() {
          return isSetField("Bal");
        }
      public void toXmlForSuperParent(StringBuffer xmlBuffer)
      {
    	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
    	  this.toXml(xmlBuffer);
    	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
      }
      
}
