package com.singlee.financial.cdtc.pac;


/**
 *  MT00：指令状态通知消息
	MT01：合同状态通知消息
	MT02：公告信息通知消息
	MT03：文件生成完成通知消息

 * @author Administrator
 *
 */
public class MessageTypeCode extends StringField {
	public static enum Type {
		
		MT00("MT00","指令状态通知消息"),
		MT01("MT01","合同状态通知消息"),
		MT02("MT02","公告信息通知消息"),
		MT03("MT03","文件生成完成通知消息");
		
		private String type;
		
		private String name;

		
		private Type(String type, String name) {
			this.type = type;
			this.name = name;
		}
		// 普通方法   
	    public static String getName(String type) {   
	        for (Type c : Type.values()) {   
	            if (c.getType().equalsIgnoreCase(type)) {   
	               return c.name;   
	            }   
	        }   
	        return "";   
	    }  
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return this.type+":"+this.name;
		}
	}
	
	public MessageTypeCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
