package com.singlee.financial.cdtc.pac;


/**
 *  SD00：融入方向融出方支付
SD01：融出方向融入方支付

 * @author Administrator
 *
 */
public class CashSettlementDirectionCode extends StringField {
	
	public static enum Type {
		SD00("SD00","融入方向融出方支付"),
		SD01("SD01","融出方向融入方支付");
		private String type;
		
		private String name;

		
		private Type(String type, String name) {
			this.type = type;
			this.name = name;
		}
		// 普通方法   
	    public static String getName(String type) {   
	        for (Type c : Type.values()) {   
	            if (c.getType().equalsIgnoreCase(type)) {   
	               return c.name;   
	            }   
	        }   
	        return "";   
	    }  
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return this.type+":"+this.name;
		}
	}
	public CashSettlementDirectionCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";

	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
