package com.singlee.financial.cdtc.pac;


/**
 *  AL00：承销额度
AL01：承销额度待付
AL02：可用
AL03：待付
AL04：质押式待回购
AL05：冻结
AL06：质押
AL07：待确认债权
AL08：待确认债权待付

 * @author Administrator
 *
 */
public class AccountLedgerTypeCode extends StringField {
	
	public static enum Type {
		AL00("AL00","承销额度"),
		AL01("AL01","承销额度待付"),
		AL02("AL02","可用"),
		AL03("AL03","待付"),
		AL04("AL04","质押式待回购"),
		AL05("AL05","冻结"),
		AL06("AL06","质押"),
		AL07("AL07","待确认债权"),
		AL08("AL08","待确认债权待付");
		private String type;
		
		private String name;

		
		private Type(String type, String name) {
			this.type = type;
			this.name = name;
		}
		// 普通方法   
	    public static String getName(String type) {   
	        for (Type c : Type.values()) {   
	            if (c.getType().equalsIgnoreCase(type)) {   
	               return c.name;   
	            }   
	        }   
	        return "";   
	    }  
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return this.type+":"+this.name;
		}
	}
	public AccountLedgerTypeCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";

	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
