package com.singlee.financial.cdtc.pac;

public class Exact9NumericText  extends IntField {
	
	private static final long serialVersionUID = 1L;
	/**
	 * 最短1位，最长2位数字[0-9]{1,2}
	 */
	private static final String regStr="^[0-9]{9}$";
	
	public Exact9NumericText(String field) {
		
		super(field);
		// TODO Auto-generated constructor stub
	}

	
	
	/* (non-Javadoc)
	 * @see com.singlee.client.cdtc.pac.IntField#getValue()
	 */
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
	public static void main(String[] args) {
		Exact9NumericText exact9NumericText = new Exact9NumericText("9位数字");
		exact9NumericText.setValue(111111111);
		System.out.println(exact9NumericText.toXmlString());
	}
	
}