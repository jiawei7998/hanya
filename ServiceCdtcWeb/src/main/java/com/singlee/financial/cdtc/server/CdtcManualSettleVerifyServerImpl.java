package com.singlee.financial.cdtc.server;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cdc.dmsg.client.DClient;
import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.SmtOutBean;
import com.singlee.financial.cdtc.CdtcManualSettleVerifyServer;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSysExceptionAdviceV01;
import com.singlee.financial.cdtc.mapper.IfsSummitManualsettlMapper;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.util.XmlFormat;
import com.singlee.financial.pojo.component.RetStatusEnum;
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcManualSettleVerifyServerImpl implements CdtcManualSettleVerifyServer {
	
	private  Logger logger = LoggerFactory.getLogger(CdtcManualSettleVerifyServerImpl.class);
	
	@Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;
	
	@Autowired
	IfsSummitManualsettlMapper  ifsSummitManualsettlMapper;
	
	/***
	 * 手工结算-复核
	 */
	@Override
	public SmtOutBean settlVerfiyCdtcMsg(BtchQryRslt btchQryRslt)throws RemoteConnectFailureException, Exception {
		
		SmtOutBean smtOutBean = new SmtOutBean();
		
		logger.info(XmlFormat.format(btchQryRslt.getCSBS001()));
		DClient dClient = new DClient(wasIp);
		String backXml = dClient.sendMsg(XmlFormat.format(btchQryRslt.getCSBS001().toString()));
		if(backXml != null) {
			SAXReader reader = new SAXReader();
			org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));  
			String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
			if(null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {
				/**
				 * 读取请求报文XML 反解析报文
				 */
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
				spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				if(spotSettlementInstruction.hasGroup("Document")) {
					System.out.println("hasGroup(Document)");
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for(Group group : groupsList) {
						// 保存xml文件
						StringBuffer xmlBuffer = new StringBuffer();
						group.toXmlForParent(xmlBuffer);
						System.out.println(xmlBuffer.toString());
						commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
						CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
					}
				}
				String txRsltCd = "";
				if(null != commonDistributionSettlementStatusReport) {
					if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
						Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
						Group instrStsInf = stsFlg.hasGroup("InstrStsInf")?stsFlg.getGroups("InstrStsInf").get(0):null;
						if(null != instrStsInf)
						{
							txRsltCd = instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd")?instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject().toString():" ";
						}
					}
				}
				if("BJ000000".equalsIgnoreCase(txRsltCd)) {
					btchQryRslt.setADealWith("Y");
					btchQryRslt.setADTime(new Date());
					if(ifsSummitManualsettlMapper.update(btchQryRslt)>0){
						smtOutBean.setRetStatus(RetStatusEnum.S);
						smtOutBean.setRetCode("SUCCESS");
						smtOutBean.setRetMsg("合同信息更新成功！");
						logger.info("合同信息更新成功！");
					}else{
						smtOutBean.setRetStatus(RetStatusEnum.F);
						smtOutBean.setRetCode("ERROR");
						smtOutBean.setRetMsg("合同信息更新失败！");
						logger.info("合同信息更新失败！");
					}
				}
			}else if("CSBS.009.001.01".equalsIgnoreCase(type)){
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
				if(spotSettlementInstruction.hasGroup("Document")) {
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for(Group group : groupsList) {
						StringBuffer xmlBufferT = new StringBuffer();
						group.toXmlForParent(xmlBufferT);
						systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBufferT.toString()+"</Document>").getBytes("UTF-8"))));  
						XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
					}
				}
				if(null != systemExceptionAdviceV01) {
					String XcptnRtrCd = "";String XcptnRtrTx="";
					if(systemExceptionAdviceV01.hasGroup("SysXcptn")) {
						XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
						XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
					}
					smtOutBean.setRetStatus(RetStatusEnum.F);
					smtOutBean.setRetCode(XcptnRtrCd);
					smtOutBean.setRetMsg(XcptnRtrTx);
					logger.info("消息错误码["+XcptnRtrCd+"]\r\n错误信息[" + XcptnRtrTx+"]");
				}
			}
			
			
		}
		
		
		return smtOutBean;
	}

	@Override
	public SmtOutBean cancelSettlCdtcMsg(BtchQryRslt btchQryRslt)throws RemoteConnectFailureException, Exception {
		SmtOutBean smtOutBean = new SmtOutBean();
		smtOutBean.setRetStatus(RetStatusEnum.F);
		smtOutBean.setRetMsg("交易"+btchQryRslt.getInstrId()+"撤销失败！");
		if(ifsSummitManualsettlMapper.delete(btchQryRslt)>0){
			smtOutBean.setRetStatus(RetStatusEnum.S);
			smtOutBean.setRetMsg("交易"+btchQryRslt.getInstrId()+"撤销成功！");
			logger.info("交易"+btchQryRslt.getInstrId()+"撤销成功！");
		}
		return smtOutBean;
		
	}

	@Override
	public List<BtchQryRslt> selectBtchQryRslt(Map<String, Object> map)throws RemoteConnectFailureException, Exception {
		List<BtchQryRslt> list =  ifsSummitManualsettlMapper.selectBtchQryRslt(map);
		return list;
	}

}
