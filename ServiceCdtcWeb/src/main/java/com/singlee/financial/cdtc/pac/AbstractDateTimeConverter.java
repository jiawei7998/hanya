package com.singlee.financial.cdtc.pac;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

abstract class AbstractDateTimeConverter
{
  protected static void assertLength(String value, int i, String type)
    throws FieldConvertError
  {
    if (value.length() != i) {
      throwFieldConvertError(value, type);}
  }

  protected static void assertDigitSequence(String value, int i, int j, String type) throws FieldConvertError
  {
    for (int offset = i; offset < j; offset++) {
      if (!Character.isDigit(value.charAt(offset))) {
        throwFieldConvertError(value, type);}
    }
  }

  protected static void assertSeparator(String value, int offset, char ch, String type)
    throws FieldConvertError
  {
    if (value.charAt(offset) != ch) {
      throwFieldConvertError(value, type);}
  }

  protected static void throwFieldConvertError(String value, String type) throws FieldConvertError
  {
    throw new FieldConvertError("invalid Local or UTC " + type + " value: " + value);
  }

  protected static long parseLong(String s) {
    long n = 0L;
    for (int i = 0; i < s.length(); i++) {
      n = n * 10L + (s.charAt(i) - '0');
    }
    return n;
  }

  protected DateFormat createUTCDateFormat(String format) {
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    sdf.setDateFormatSymbols(new DateFormatSymbols(Locale.US));
    return sdf;
  }

  protected DateFormat createLocalDateFormat(String format) {
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
    sdf.setDateFormatSymbols(new DateFormatSymbols(Locale.US));
    return sdf;
  }

  protected static void checkHour(String value, int i, int j, String type) throws FieldConvertError
  {
    int hour = Integer.parseInt(value.substring(i, j));
    if ((hour < 0) || (hour > 23)) {
      throwFieldConvertError(value, type);}
  }

  protected static void checkMinute(String value, int i, int j, String type)
    throws FieldConvertError
  {
    int minute = Integer.parseInt(value.substring(i, j));
    if ((minute < 0) || (minute > 59)) {
      throwFieldConvertError(value, type);}
  }

  protected static void checkSecond(String value, int i, int j, String type)
    throws FieldConvertError
  {
    int second = Integer.parseInt(value.substring(i, j));
    if ((second < 0) || (second > 59)) {
      throwFieldConvertError(value, type);}
  }

  protected static void checkUTCLeapSecond(String value, int i, int j, String type)
    throws FieldConvertError
  {
    int month = Integer.parseInt(value.substring(i - 11, i - 9));
    int day = Integer.parseInt(value.substring(i - 9, i - 7));
    int hour = Integer.parseInt(value.substring(i - 6, i - 4));
    int minute = Integer.parseInt(value.substring(i - 3, i - 1));

    int second = Integer.parseInt(value.substring(i, j));
    if ((second < 0) || (second > 60)) {
      throwFieldConvertError(value, type);}
    else if ((second == 60) && (
      ((month == 3) && (day == 31)) || ((month == 6) && (day == 30)) || ((month == 9) && (day == 30)) || ((month != 12) || (day != 31) || 
      (hour != 23) || (minute != 59)))) {
      throwFieldConvertError(value, type);}
  }

  protected static void checkLocalLeapSecond(String value, int i, int j, String type)
    throws FieldConvertError
  {
    int month = Integer.parseInt(value.substring(i - 11, i - 9));
    int day = Integer.parseInt(value.substring(i - 9, i - 7));
    int hour = Integer.parseInt(value.substring(i - 6, i - 4));
    int minute = Integer.parseInt(value.substring(i - 3, i - 1));

    int second = Integer.parseInt(value.substring(i, j));
    if ((second < 0) || (second > 60)) {
      throwFieldConvertError(value, type);}
    else if ((second == 60) && (
      ((month == 4) && (day == 1)) || ((month == 7) && (day == 1)) || ((month == 10) && (day == 1)) || ((month != 1) || (day != 1) || 
      (hour != 7) || (minute != 59)))) {
      throwFieldConvertError(value, type);}
  }

  protected static void checkDate(String value, int i, int j, String type)
    throws FieldConvertError
  {
    if (j - i != 8) {
      throwFieldConvertError(value, type);
    }

    int year = Integer.parseInt(value.substring(i, i + 4));
    if (year < 0) {
      throwFieldConvertError(value, type);
    }

    int month = Integer.parseInt(value.substring(i + 4, i + 6));
    if ((month < 1) || (month > 12)) {
      throwFieldConvertError(value, type);
    }

    int day = Integer.parseInt(value.substring(i + 6, i + 8));
    if ((day < 1) || (day > 31)) {
      throwFieldConvertError(value, type);}
    else if ((day == 31) && ((month == 2) || (month == 4) || (month == 6) || (month == 9) || (month == 11))) {
      throwFieldConvertError(value, type);}
    else if ((day == 30) && (month == 2)) {
      throwFieldConvertError(value, type);}
    else if ((day == 29) && (month == 2) && ((year % 4 != 0) || (year % 100 == 0)) && (year % 400 != 0)) {
      throwFieldConvertError(value, type);}
  }
}