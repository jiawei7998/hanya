/**
 * 
 */
package com.singlee.financial.cdtc.pac;

/**
 * @author Fang
 *
 */
public class Max256Text extends StringField {

	public Max256Text(String field) {
		super(field);
	}
	private static final long serialVersionUID = 1L;
	@Override
	public String toXmlString(){
		if(super.getLength()>=1 && super.getLength()<=256)
		{
			return toXml();
		}
		return "";
	}

}
