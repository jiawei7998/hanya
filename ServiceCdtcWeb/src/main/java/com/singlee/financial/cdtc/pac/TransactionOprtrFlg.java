package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class TransactionOprtrFlg extends Group{

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("ReqTp", Field.class);
		keyClassMap.put("OprtrSts", Field.class);
	}
	
	public TransactionOprtrFlg(String tag) {
		super(tag, tag, new String[] { "ReqTp","OprtrSts"});
	}
	public TransactionOprtrFlg(String tag,String delim) {
		super(tag, tag, new String[] { "ReqTp","OprtrSts"});
	}
	
	public void set(RequestTypeCode type){
		setField(type);
	}
	
	public void set(OperatorStatusCode field){
		setField(field);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
