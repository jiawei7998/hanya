package com.singlee.financial.cdtc.processdao;

import com.singlee.financial.cdtc.bean.BtchQryRslt;
import com.singlee.financial.cdtc.bean.BtchQryRsltBean;

public interface VerfiyCdtcMsgDao {

	/**
	 * 当日待结算[手工]复核
	 * @param btchQryRslt
	 * @return
	 * @throws Exception
	 */
	public String settlVerfiyCdtcMsg(BtchQryRslt btchQryRslt) throws Exception;
	

	/**
	 * 当日收款确认[手工]  收款复核
	 * @param btchQryRsltBean
	 * @return
	 * @throws Exception
	 */
	public String payVerfiyCdtcMsg(BtchQryRsltBean btchQryRsltBean) throws Exception;
	

	/**
	 * 当日付款确认[手工]  付款复核
	 * @param btchQryRsltBean
	 * @return
	 * @throws Exception
	 */
	public String receiveVerfiyCdtcMsg(BtchQryRsltBean btchQryRsltBean) throws Exception;
	
	/**
	 * 收付款经办撤销
	 * @return
	 * @throws Exception
	 */
	public boolean cancelPayOrReceiveCdtcMsg(BtchQryRsltBean btchQryRsltBean) throws Exception;
	

	/**
	 * 结算经办撤销
	 * @return
	 * @throws Exception
	 */
	public boolean cancelSettlCdtcMsg(BtchQryRslt btchQryRslt) throws Exception;
	
}
