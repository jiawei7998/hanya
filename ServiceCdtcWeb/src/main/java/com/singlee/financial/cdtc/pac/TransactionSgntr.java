/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class TransactionSgntr extends Group {

	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("SttlmMmbSgntr", Field.class);
		keyClassMap.put("CSBSSgntr", Field.class);
	}
	
	public TransactionSgntr(String tag){
		super(tag,tag,new String[]{"SttlmMmbSgntr","CSBSSgntr"});
	}
	public TransactionSgntr(String tag,String belim){
		super(tag,tag,new String[]{"SttlmMmbSgntr","CSBSSgntr"});
	
	}
	public void set(Max2048Text field){
		setField(field);
	}
}
