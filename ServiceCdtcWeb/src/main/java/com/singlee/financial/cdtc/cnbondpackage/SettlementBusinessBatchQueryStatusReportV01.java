package com.singlee.financial.cdtc.cnbondpackage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;

import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.pac.AccountIdentificationAndName4;
import com.singlee.financial.cdtc.pac.BondSettlementType6Code;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.CommonCurrencyAndAmount;
import com.singlee.financial.cdtc.pac.ContractBlockedStatusCode;
import com.singlee.financial.cdtc.pac.ContractStatusCode;
import com.singlee.financial.cdtc.pac.Exact11Text;
import com.singlee.financial.cdtc.pac.Exact12NumericText;
import com.singlee.financial.cdtc.pac.Exact9NumericText;
import com.singlee.financial.cdtc.pac.Extension;
import com.singlee.financial.cdtc.pac.FaceCurrencyAndAmount;
import com.singlee.financial.cdtc.pac.GenericIdentification4;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODate;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.InstructionConfirmIndicatorCode;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.InstructionStatusCode;
import com.singlee.financial.cdtc.pac.Max2048Text;
import com.singlee.financial.cdtc.pac.Max20Text;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.Max5NumericText;
import com.singlee.financial.cdtc.pac.Max70Text;
import com.singlee.financial.cdtc.pac.Max8Text;
import com.singlee.financial.cdtc.pac.PartyIdentification;
import com.singlee.financial.cdtc.pac.Priority4Code;

public class SettlementBusinessBatchQueryStatusReportV01 extends Group {

	/**
	 * 6.3.6	结算业务批量查询状态报告 （CSBS.038.001.01 SettlementBusinessBatchQueryStatusReport V01）
	 * 
	 * 结算业务批量查询状态报告  报文由中央债券综合业务系统发送给结算成员,用于:
	 * 返回由(结算业务查询请求)所发起的批量结算业务查询的结果
	 */
	private static final long serialVersionUID = 1L;
	
	public static String type = "CSBS.006.001.01";
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Msg",          ReportMsgHeader.class);
		keyClassMap.put("QryRsltAbst", 	QryRsltAbst.class);
		keyClassMap.put("BtchQryRslt",  BtchQryRslt.class);
		keyClassMap.put("QryPty", 		ReportQryPty.class);
		keyClassMap.put("Xtnsn", Extension.class);
		keyClassMap.putAll(ReportMsgHeader.keyClassMap);
		keyClassMap.putAll(QryRsltAbst.keyClassMap);
		keyClassMap.putAll(BtchQryRslt.keyClassMap);
		keyClassMap.putAll(ReportQryPty.keyClassMap);
		keyClassMap.putAll(Extension.keyClassMap);
	}
	
	public SettlementBusinessBatchQueryStatusReportV01(String tag){
		/**
		 * Msg          报文标识
		 * BtchQryRslt  查询结果摘要
		 * BtchQryRslt  批量查询结果
		 * QryPty       结算业务查询请求发起机构的信息
		 * Xtnsn        扩展项
		 */
		super(tag,tag,new String[]{"Msg","QryRsltAbst","BtchQryRslt","QryPty","Xtnsn"});
	}
	public SettlementBusinessBatchQueryStatusReportV01(String tag,String belim){
		
		super(tag,tag,new String[]{"Msg","QryRsltAbst","BtchQryRslt","QryPty","Xtnsn"});
	}
	
	
	public boolean isHasTag(String tag)
	{
		String[] strings = getFieldOrder();
		for(String key:strings)
		{
			if(key.equals(tag))
			{
				return true;
			}
		}
		return false;
	}
	
	public void set(ReportMsgHeader msgHeader){
		addGroup(msgHeader);
	}
	
	public void set(QryRsltAbst qryRsltAbst){
		addGroup(qryRsltAbst);
	}
	
	public void set(BtchQryRslt btchQryRslt){
		addGroup(btchQryRslt);
	}
	
	public void set(ReportQryPty reportQryPty){
		addGroup(reportQryPty);
	}
	public void set(Extension group){
		addGroup(group);
	}
	

	public void toXmlForSuperParent(StringBuffer xmlBuffer)
	{
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
	}
	
	public static SettlementBusinessBatchQueryStatusReportV01 content(Bean1 bean1){

		SettlementBusinessBatchQueryStatusReportV01 batchQueryStatusReportV01 = 
				new SettlementBusinessBatchQueryStatusReportV01("Document");
		/************************************1.0  MSG****************************/
		ReportMsgHeader msg = new ReportMsgHeader("Msg");
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("交易流水标识");
		msg.set(max35Text);
		
		PartyIdentification partyIdentification = new PartyIdentification("Oprtr");
		Max35Text max35Text2 = new Max35Text("Nm");
		max35Text2.setValue("经办人");
		partyIdentification.set(max35Text2);
		msg.set(partyIdentification);
		
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT01.getType());
		msg.set(priority4Code);
		
		ISODateTime isDateTime = new ISODateTime("CreDtTm",new Date());
		msg.set(isDateTime);
		batchQueryStatusReportV01.set(msg);
		
		/*****************************2.0 查询结果摘要************************************/
		QryRsltAbst qryRsltAbst = new QryRsltAbst("QryRsltAbst");//查询结果摘要
		Max8Text txRsltCd = new Max8Text("TxRsltCd"); //查询处理结果返回码
		txRsltCd.setValue("1111");
		qryRsltAbst.set(txRsltCd);
		
		Max5NumericText qryRsltCnt = new Max5NumericText("QryRsltCnt");//查询结果计数
		qryRsltCnt.setValue(12345);
		qryRsltAbst.set(qryRsltCnt);
		batchQueryStatusReportV01.set(qryRsltAbst);
		
		/******************************3.0 批量查询结果*************************************/
		BtchQryRslt btchQryRslt = new BtchQryRslt("BtchQryRslt");

		Exact12NumericText InstrId = new Exact12NumericText("InstrId");
		InstrId.setData(String.valueOf("123456789012"));
		btchQryRslt.set(InstrId);
		
		Exact9NumericText CtrctId = new Exact9NumericText("CtrctId");
		CtrctId.setValue(123456789);
		btchQryRslt.set(CtrctId);
		
		//AccountIdentificationAndName4
		AccountIdentificationAndName4 account41 = new AccountIdentificationAndName4("GivAcct");
		Max35Text Nm1 = new Max35Text("Nm");
		Nm1.setValue("江苏银行");
		account41.set(Nm1);
		Exact11Text Id1 = new Exact11Text("Id");
		Id1.setValue("1234567");
		account41.set(Id1);
		btchQryRslt.set(account41);
		
		AccountIdentificationAndName4 account42 = new AccountIdentificationAndName4("TakAcct");
		Max35Text Nm2 = new Max35Text("Nm");
		Nm2.setValue("恒丰银行");
		account42.set(Nm2);
		Exact11Text Id2 = new Exact11Text("Id");
		Id2.setValue("7654321");
		account42.set(Id2);
		btchQryRslt.set(account42);
		
		Max20Text TxId = new Max20Text("TxId");//业务标识号
		TxId.setValue("ABC1000900");
		btchQryRslt.set(TxId);
		
		BusinessTypeCode BizTp = new BusinessTypeCode("BizTp");//业务类型
		BizTp.setValue(BusinessTypeCode.Type.BT02.getType());
		btchQryRslt.set(BizTp);
		
		BondSettlementType6Code SttlmTp1 = new BondSettlementType6Code("SttlmTp1");//结算方式1
		SttlmTp1.setValue(BondSettlementType6Code.Type.ST02.getType());
		btchQryRslt.set(SttlmTp1);
		
		BondSettlementType6Code SttlmTp2 = new BondSettlementType6Code("SttlmTp2");//结算方式2
		SttlmTp2.setValue(BondSettlementType6Code.Type.ST03.getType());
		btchQryRslt.set(SttlmTp2);
		
		Max5NumericText BdCnt = new Max5NumericText("BdCnt");//债券数目
		BdCnt.setValue(12300);
		btchQryRslt.set(BdCnt);
		
		FaceCurrencyAndAmount AggtFaceAmt = new FaceCurrencyAndAmount("AggtFaceAmt","CNY");//债券总额
		AggtFaceAmt.setValue("123000000");
		btchQryRslt.set(AggtFaceAmt);
		
		CommonCurrencyAndAmount Val1 = new CommonCurrencyAndAmount("Val1","CNY");//结算金额1
		Val1.setValue("123000000");
		btchQryRslt.set(Val1);
		
		CommonCurrencyAndAmount Val2 = new CommonCurrencyAndAmount("Val2","CNY");//结算金额2
		Val2.setValue("123000000");
		btchQryRslt.set(Val2);
		
		ISODate Dt1 = new ISODate("Dt1",new Date());
		btchQryRslt.set(Dt1);
		ISODate Dt2 = new ISODate("Dt2",new Date());
		btchQryRslt.set(Dt2);
		
		InstructionStatusCode InstrSts = new InstructionStatusCode("InstrSts");//指令处理状态
		InstrSts.setValue(InstructionStatusCode.Type.IS01.getType());
		btchQryRslt.set(InstrSts);
		
		ContractStatusCode CtrctSts = new ContractStatusCode("CtrctSts");//合同处理状态
		CtrctSts.setValue("00");
		btchQryRslt.set(CtrctSts);
		
		ContractBlockedStatusCode CtrctBlckSts = new ContractBlockedStatusCode("CtrctBlckSts");//合同冻结状态
		CtrctBlckSts.setValue(ContractBlockedStatusCode.Type.CB00.getType());
		btchQryRslt.set(CtrctBlckSts);
		
		ISODateTime LastUpdTm = new ISODateTime("LastUpdTm",new Date());//最新更新时间
		btchQryRslt.set(LastUpdTm);
		
		InstructionConfirmIndicatorCode OrgtrCnfrmInd = new InstructionConfirmIndicatorCode("OrgtrCnfrmInd");//发令方确认标识
		OrgtrCnfrmInd.setValue(InstructionConfirmIndicatorCode.Type.IC01.getType());
		btchQryRslt.set(OrgtrCnfrmInd);
		
		InstructionConfirmIndicatorCode CtrCnfrmInd = new InstructionConfirmIndicatorCode("CtrCnfrmInd");//对手方确认标识
		CtrCnfrmInd.setValue(InstructionConfirmIndicatorCode.Type.IC02.getType());
		btchQryRslt.set(CtrCnfrmInd);
		
		InstructionOriginCode InstrOrgn = new InstructionOriginCode("InstrOrgn");//指令来源
		InstrOrgn.setValue(InstructionOriginCode.Type.IO01.getType());
		btchQryRslt.set(InstrOrgn);
		
		batchQueryStatusReportV01.set(btchQryRslt);
		
		batchQueryStatusReportV01.set(btchQryRslt);
		
		/**************************************4.0 查询方******************************************/
		ReportQryPty qryPty = new ReportQryPty("QryPty");
		Max70Text name = new Max70Text("Nm");
		name.setValue("江苏银行山东分行");
		qryPty.set(name);
		
		GenericIdentification4 genericIdentification4 = new GenericIdentification4("PrtryId");
		Max35Text gen4_Id = new Max35Text("Id");
		gen4_Id.setValue("10000191");
		genericIdentification4.set(gen4_Id);
		Max35Text gen4_IdTp = new Max35Text("IdTp");
		gen4_IdTp.setValue("XXXXMMXX");
		genericIdentification4.set(gen4_IdTp);
		qryPty.set(genericIdentification4);
		batchQueryStatusReportV01.set(qryPty);
		
		//拓展项
		Extension extension = new Extension("Xtnsn");
		Max2048Text max2048Text = new Max2048Text("XtnsnTxt");
		max2048Text.setValue("结算业务批量查询状态报告报文完毕");
		extension.set(max2048Text);
		batchQueryStatusReportV01.set(extension);
		
		return batchQueryStatusReportV01;
	}
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {

		/***********************************************************************************/
		/************************************sysout*********************************/
		StringBuffer stringBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(stringBuffer); 
		System.out.println(XmlFormat.format(stringBuffer.toString()));
		
		// 保存xml文件
		org.desy.trd.jdom.input.SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat.format(stringBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out=new FileOutputStream("./cnbondxml/"+SettlementBusinessBatchQueryStatusReportV01.type+".xml"); 
		XMLOutputter outputter = new XMLOutputter(); 
		//如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的 
		outputter.output((org.desy.trd.jdom.Document) doc, out); 
		out.close(); 
		

	
        /**
         * 读取请求报文XML  反解析报文
         */
		SAXReader reader = new SAXReader();
		SettlementBusinessBatchQueryStatusReportV01 batchStatusV01 = new SettlementBusinessBatchQueryStatusReportV01("Document");
		org.dom4j.Document xmlDoc = reader.read(new File("./cnbondxml/"+batchStatusV01.type+".xml"));
		XMLRevSettlBusiBatchStatusV01.getElementList(xmlDoc.getRootElement(),batchStatusV01);
		System.out.println("-----------------------------------------------------------------------------------------");
		StringBuffer stringBuffer1 = new StringBuffer();
		batchStatusV01.toXmlForSuperParent(stringBuffer1);
		System.out.println(XmlFormat.format(stringBuffer1.toString()));
		
	}

}
