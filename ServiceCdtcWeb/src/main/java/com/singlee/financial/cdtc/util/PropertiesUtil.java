package com.singlee.financial.cdtc.util;

import java.io.InputStream;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class PropertiesUtil {
	
	private static PropertiesConfiguration propertiesConfiguration = null;
	
	public static String getProperties(String name){
		return getProperties(name, "");
	}
	
	
	public static String getProperties(String name,String defaultValue)
	{
		if(propertiesConfiguration == null)
		{
			try {
				propertiesConfiguration = parseFile("cdtc.properties");
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		}
		return propertiesConfiguration.getString(name, defaultValue);
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws ConfigurationException
	 */
	public static PropertiesConfiguration parseFile(String fileName) throws ConfigurationException {
		return parseFile(fileName, "utf-8", ',');
	}

	/**
	 * 
	 * @param fileName
	 * @param encode
	 * @param s
	 * @return
	 * @throws ConfigurationException
	 */
	public static PropertiesConfiguration parseFile(String fileName, String encode, char s) throws ConfigurationException {
		// 生成输入流
		InputStream ins = PropertiesUtil.class.getResourceAsStream("/" + fileName);
		// 生成properties对象
		PropertiesConfiguration p = new PropertiesConfiguration();
		try {
			p.setListDelimiter(s);
			p.load(ins, encode);
			ins.close();
		} catch (Exception e) {
			throw new ConfigurationException(e);
		}
		return p;
	}
}
