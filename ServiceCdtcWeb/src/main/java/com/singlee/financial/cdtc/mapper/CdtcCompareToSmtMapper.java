package com.singlee.financial.cdtc.mapper;

import java.util.List;
import java.util.Map;
import com.singlee.financial.bean.CdtcAcctTotalBean;


public interface CdtcCompareToSmtMapper {

	/**
	 * 插入总对账单数据 
	 * @table IFS_CDTC_SEC_POS
	 * @param list
	 * @return
	 */
	public int insertCompareToSmtByList(List<CdtcAcctTotalBean> list);
	
	/**
	 * 删除总对账单数据
	 * @return
	 */
	public int deleteCompareToSmt();

	/**
	 * 核对总对账单
	 * @param map
	 * @return
	 */
	public String checkingSummitData(Map<String, Object> map);

	/**
	 * 调用生成SUMMIT余额
	 * @param map
	 */
	public void procSummitSecPos(Map<String, Object> map);
	
}
