package com.singlee.financial.cdtc.server.hrb;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.cdtc.CdtcForCheckClearJobServer;
import com.singlee.financial.cdtc.hrb.HrbCdtcForCheckClearJobServer;
import com.singlee.financial.cdtc.hrb.HrbCdtcForQueryAllShouldBeSettlXmlJobServer;
import com.singlee.financial.cdtc.bean.BtchQryRsltBean;
import com.singlee.financial.cdtc.cnbondpackage.CdtcBankAccount;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSysExceptionAdviceV01;
import com.singlee.financial.cdtc.dao.BranprcdateGetDao;
import com.singlee.financial.cdtc.dao.CdtcRequestMessageCreateDao;
import com.singlee.financial.cdtc.dao.CnbondManagerDao;
import com.singlee.financial.cdtc.dao.TyjszlztbgDao;
import com.singlee.financial.cdtc.dao.impl.CdtcRequestMessageCreateDaoImpl;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.InstructionConfirmIndicatorCode;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.util.EncodingUtil;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

import cdc.dmsg.client.DClient;

/**
 * 定时向中债综合结算业务系统查询当日批量待结算业务
 * 
 * @author singlee 主要作用是定时请求 获得CSBS.006.001.01待确认的结算报文入库 IFS_CDTC_CNBOND_PKG表
 *         用于轮询核对，产生确认报文 供确认报文发送任务使用
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCdtcForQueryAllShouldBeSettlXmlJobServerImpl implements HrbCdtcForQueryAllShouldBeSettlXmlJobServer {

	private Logger logger_settl = LoggerFactory.getLogger("QUERYSETTL");

	private Logger logger_con = LoggerFactory.getLogger("QUERYCONTRACT");

	@Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;

	@Autowired
	private BranprcdateGetDao branprcdateGetDao;
	@Autowired
	private CnbondManagerDao cnbondManagerDao;
	@Autowired
	private TyjszlztbgDao tyjszlztbgDao;
	@Autowired
	private HrbCdtcForCheckClearJobServer cdtcForCheckClearJobServer;

	// 定时调度查询当日待结算任务
	@Override
	public RetBean queryCdtcDetailToOracleDatabaseXmlTables(Map<String, Object> params) throws Exception {
		try {
			String postdate=(String) params.get("postDate");
			Boolean isHolyDay=(Boolean) params.get("isHolyDay");
			RetBean ret = cdtcForCheckClearJobServer.validCdtcWorkTime(isHolyDay);
			if (RetStatusEnum.F.equals(ret.getRetStatus())) {
				logger_settl.info("当前是节假期或非工作时间,不进行业务处理!");
				Thread.sleep(2000);
				return ret;
			}

			final CdtcRequestMessageCreateDao cdtcRequestMessageCreateDao = new CdtcRequestMessageCreateDaoImpl();
			/**
			 * 测试的时候可以直接指定postdate是CDTC的系统日期 postdate=“2017-08-01”
			 */
			// 通用结算指令查询报文
			SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao
					.createCSBS00500101ForQryCondConditionOS03AndBJ0300(CdtcBankAccount.BANK_ACCOUNT, postdate);
			StringBuffer xmlBuffer = new StringBuffer();
			settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);
			logger_settl.info("发送批量指令查询报文:" + xmlBuffer.toString());
			DClient dClient = new DClient(wasIp);
			// 发送中债前置系统查询
			String backXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
			// 返回报文
			backXml = new String(backXml.getBytes(EncodingUtil.getEncoding(backXml)), "GBK");
			logger_settl.info("回执报文:" + backXml);
			if (null != backXml) {
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader
						.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp")
						.getText();
				logger_settl.info("MsgTp:" + type);
				// 通用结算指令报文CSBS.006.001.01
				if (null != type && "CSBS.006.001.01".equalsIgnoreCase(type)) {
					try {
						java.util.List<BtchQryRslt> btchqList = cdtcRequestMessageCreateDao
								.transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(XmlFormat.format(backXml));
						List<BtchQryRslt> cdtcHashMap = new ArrayList<BtchQryRslt>();
						logger_settl.info("结算指令数量:" + btchqList.size());
						for (int i = 0; i < btchqList.size(); i++) {
							BtchQryRslt btchQryRslt = btchqList.get(i);
							btchQryRslt.setStdt(btchQryRslt.getDt1());
							// 需要判定是否当日可以结算的指令 指令状态为待确认IS03
							if (!btchQryRslt.getDt1().equals(postdate) || !"IS03".equals(btchQryRslt.getInstrSts())) {
							} else {
								/**
								 * 第三方 进行是否还需要确认的过滤 譬如江苏银行是付券方 那么 已经确认的话 就不需要再次进行确认了。 如果是 柜台那么只有江苏银行在付券方才需要确认
								 * 
								 */
								if (btchQryRslt.getInstrOrgn()
										.equalsIgnoreCase(InstructionOriginCode.Type.IO01.getType())
										&& btchQryRslt.getTakAcctId().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)
										&& !btchQryRslt.getOrgtrCnfrmInd().equalsIgnoreCase(
												InstructionConfirmIndicatorCode.Type.IC01.getType())) {
									cdtcHashMap.add(btchQryRslt);
								}
								if (btchQryRslt.getInstrOrgn()
										.equalsIgnoreCase(InstructionOriginCode.Type.IO03.getType())) {
									if (btchQryRslt.getGivAcctId().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)
											&& !btchQryRslt.getOrgtrCnfrmInd().equalsIgnoreCase(
													InstructionConfirmIndicatorCode.Type.IC01.getType())) {
										cdtcHashMap.add(btchQryRslt);
									}
									if (btchQryRslt.getTakAcctId().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)
											&& !btchQryRslt.getCtrCnfrmInd().equalsIgnoreCase(
													InstructionConfirmIndicatorCode.Type.IC01.getType())) {
										cdtcHashMap.add(btchQryRslt);
									}
								}
							}
						}
						// 获得需要进行确认的报文数量 cdtcHashMap
						for (BtchQryRslt btchQryRslt : cdtcHashMap) {
							String instrId = btchQryRslt.getInstrId();
							// 判定数据库指令表是否存在；
							if (null != cnbondManagerDao.getSlintfccnbondpkgByMsgtype(instrId)) {
								logger_settl.info("获得指令标识ID:" + instrId + "(已经存在待确认指令了，无需再进行详细指令查询)");
							} else {
								logger_settl.info("获得指令标识ID:" + instrId + "(准备查询详细指令)");
								{

									String txFlowId = new SimpleDateFormat("yyyyMMdd").format(new Date())
											+ CdtcBankAccount.BANK_ACCOUNT
											+ DateUtil.convertDateToYYYYMMDDHHMMSS(new Date());
									String qryCondId = instrId;
									if (qryCondId == null) {
										return new RetBean(RetStatusEnum.F, "Fail", "未获取到指令标识id");
									}
									try {
										SettlementBusinessQueryRequest settlementBusinessQueryRequest2 = cdtcRequestMessageCreateDao
												.createCSBS00500101ForQryCondConditionOS04AndBJ0300(txFlowId,
														CdtcBankAccount.BANK_ACCOUNT, qryCondId);
										StringBuffer xmlBuffer2 = new StringBuffer();
										settlementBusinessQueryRequest2.toXmlForSuperParent(xmlBuffer2);
										logger_settl.info("发送详细指令查询报文:" + xmlBuffer2.toString());
										dClient = new DClient(wasIp);
										String backXml2 = dClient.sendMsg(XmlFormat.format(xmlBuffer2.toString()));
										backXml2 = new String(backXml2.getBytes(EncodingUtil.getEncoding(backXml2)),
												"GBK");
										logger_settl.info("回执报文:" + backXml2);
										if (null != backXml2) {
											org.dom4j.Document xmlDoc2 = reader.read(new ByteArrayInputStream(
													XmlFormat.format(backXml2).getBytes("UTF-8")));
											String type2 = xmlDoc2.getRootElement().element("MsgHeader")
													.element("MsgDesc").element("MsgTp").getText();
											logger_settl.info("MsgTp:" + type2);
											if (null != type2 && "CSBS.002.001.01".equalsIgnoreCase(type2)) {
												AbstractSlIntfcCnbondPkgId abstractSlIntfcCnbondPkgId = new AbstractSlIntfcCnbondPkgId();
												abstractSlIntfcCnbondPkgId.setMsgfilename(txFlowId);

												SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction(
														"Root");
												spotSettlementInstruction.unionHashMap(type2);
												spotSettlementInstruction.getElementList(xmlDoc2.getRootElement(),
														spotSettlementInstruction);
												if (spotSettlementInstruction.hasGroup("Document")) {
													Group document = spotSettlementInstruction.hasGroup("Document")
															? spotSettlementInstruction.getGroups("Document").get(0)
															: null;
													if (null != document) {
														Group msgGroup = document.hasGroup("SttlmDtl")
																? document.getGroups("SttlmDtl").get(0)
																: null;
														if (null != msgGroup) {
															abstractSlIntfcCnbondPkgId.setMsgsn(msgGroup
																	.getfieldsForFieldMap().containsKey("InstrId")
																			? msgGroup.getfieldsForFieldMap()
																					.get("InstrId").getObject()
																					.toString()
																			: txFlowId);
															abstractSlIntfcCnbondPkgId.setMsgfedealno(
																	msgGroup.getfieldsForFieldMap().containsKey("TxId")
																			? msgGroup.getfieldsForFieldMap()
																					.get("TxId").getObject().toString()
																			: "NULL");
															abstractSlIntfcCnbondPkgId.setMsgtype(type2);
															abstractSlIntfcCnbondPkgId.setMsgopdt(
																	new SimpleDateFormat("yyy-MM-dd").parse(postdate));
															abstractSlIntfcCnbondPkgId.setMsgxml(xmlDoc2.asXML());
															if (null != cnbondManagerDao.getSlintfccnbondpkgByMsgtype(
																	abstractSlIntfcCnbondPkgId.getMsgsn())) {
																logger_settl.info(txFlowId + "["
																		+ abstractSlIntfcCnbondPkgId.getMsgsn()
																		+ " EXIST DB]");
															} else {
																if (cnbondManagerDao.addSlintfccnbondpkg(
																		abstractSlIntfcCnbondPkgId)) {
																	logger_settl.info(txFlowId + "["
																			+ abstractSlIntfcCnbondPkgId.getMsgsn()
																			+ " INSERT DB OK]");
																} else {
																	logger_settl.info(txFlowId + "["
																			+ abstractSlIntfcCnbondPkgId.getMsgsn()
																			+ " INSERT DB ERROR]");
																}
															}
														}
													}

												}

											} else if ("CSBS.009.001.01".equalsIgnoreCase(type2)) { // 异常情况无需入库。
												SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction(
														"Root");
												spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
												spotSettlementInstruction.getElementList(xmlDoc2.getRootElement(),
														spotSettlementInstruction);
												SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
												if (spotSettlementInstruction.hasGroup("Document")) {
													List<Group> groupsList = spotSettlementInstruction
															.getGroups("Document");
													for (Group group : groupsList) {
														StringBuffer xmlBufferT = new StringBuffer();
														group.toXmlForParent(xmlBufferT);
														systemExceptionAdviceV01 = new SystemExceptionAdviceV01(
																"Document");
														Document document = reader.read(new ByteArrayInputStream(
																(XmlFormat.format("<Document>" + xmlBufferT.toString()
																		+ "</Document>").getBytes("UTF-8"))));
														XMLRevSysExceptionAdviceV01.getElementList(
																document.getRootElement(), systemExceptionAdviceV01);
													}
												}
												if (null != systemExceptionAdviceV01) {
													String XcptnRtrCd = "";
													String XcptnRtrTx = "";
													if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
														XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn")
																.get(0).getfieldsForFieldMap().get("XcptnRtrCd")
																.getObject().toString();
														XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn")
																.get(0).getfieldsForFieldMap().get("XcptnRtrTx")
																.getObject().toString();
													}
													logger_settl.info("详细指令查询  消息错误码[" + XcptnRtrCd + "]\r\n错误信息["
															+ XcptnRtrTx + "]");
												}
											}
										}
									} catch (Exception e) {
										// TODO: handle exception
										logger_settl.info(e.getMessage());
										e.printStackTrace();
									}
								}

							}
						}
						// TODO Auto-generated method stub
					} catch (Exception e) {
						// TODO: handle exception
						logger_settl.info(e.getMessage());
						e.printStackTrace();
					}

				} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) {
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if (spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for (Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat
									.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(),
									systemExceptionAdviceV01);
						}
					}
					if (null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";
						String XcptnRtrTx = "";
						if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap()
									.get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap()
									.get("XcptnRtrTx").getObject().toString();
						}
						logger_settl.info("批量指令查询 消息错误码[" + XcptnRtrCd + "]\r\n错误信息[" + XcptnRtrTx + "]");
					}
				}
			}
		} catch (Exception e) {
			logger_settl.info(e.getMessage());
			throw e;
		}
		return new RetBean(RetStatusEnum.S, "success", "批量指令查询成功！");
	}

	// 定时调度查询当日合同状态任务
	@Override
	public RetBean doPayProcessAutoJobsForConstract(Map<String, Object> params) throws Exception {
		String postdate=(String) params.get("postDate");
		Boolean isHolyDay=(Boolean) params.get("isHolyDay");
		RetBean ret = cdtcForCheckClearJobServer.validCdtcWorkTime(isHolyDay);
		if (RetStatusEnum.F.equals(ret.getRetStatus())) {
			logger_con.info("当前是节假期或非工作时间,不进行业务处理!");
			Thread.sleep(2000);
			return ret;
		}

		RetBean rtb = null;
		HashMap<Integer, BtchQryRsltBean> btchqHashMap = new HashMap<Integer, BtchQryRsltBean>();
		try {
			CdtcRequestMessageCreateDao cdtcRequestMessageCreateDao = new CdtcRequestMessageCreateDaoImpl();
			// 指令批量查询
			SettlementBusinessQueryRequest settlementBusinessQueryRequestPL = cdtcRequestMessageCreateDao
					.createCSBS00500101ForQryCondConditionOS03AndBJ0300(CdtcBankAccount.BANK_ACCOUNT, postdate);
			StringBuffer bufferPL = new StringBuffer();
			settlementBusinessQueryRequestPL.toXmlForSuperParent(bufferPL);
			DClient clientPL = new DClient(wasIp);
			logger_con.info("CdtcAndCnapsAutoPayOperator批量结算指令请求报文：" + bufferPL.toString());
			String msgStr = clientPL.sendMsg(bufferPL.toString());
			logger_con.info("CdtcAndCnapsAutoPayOperator  批量结算指令返回报文：" + msgStr);
			List<BtchQryRslt> zhilingList = new ArrayList<BtchQryRslt>();
			if (null != msgStr) {
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader
						.read(new ByteArrayInputStream(XmlFormat.format(msgStr).getBytes("UTF-8")));
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp")
						.getText();
				logger_con.info("CdtcAndCnapsAutoPayOperator 批量结算指令返回报文类型：" + type);
				if (null != type && "CSBS.006.001.01".equalsIgnoreCase(type)) {
					// 反解析出批量业务查询返回的业务数据
					zhilingList = cdtcRequestMessageCreateDao
							.transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(msgStr);
				} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) {
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if (spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for (Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat
									.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(),
									systemExceptionAdviceV01);
						}
					}
					if (null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";
						String XcptnRtrTx = "";
						if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap()
									.get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap()
									.get("XcptnRtrTx").getObject().toString();
						}
						logger_con.info("批量指令查询 消息错误码[" + XcptnRtrCd + "]\r\n错误信息[" + XcptnRtrTx + "]");
					}
				}

			}

			/**
			 * ***************************************************************************************************************
			 */

			List<BtchQryRsltBean> hetongList = new ArrayList<BtchQryRsltBean>();

			// 合同批量查询
			SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao
					.createCSBS00500101ForQryCondConditionOS03AndBJ0301(CdtcBankAccount.BANK_ACCOUNT, postdate);
			StringBuffer xmlBuffer = new StringBuffer();
			settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);

			String msg = xmlBuffer.toString();
			logger_con.info("批量合同查询请求报文：" + msg);
			DClient client = new DClient(wasIp);
			String rtMsg = client.sendMsg(msg);
			logger_con.info("批量合同查询返回报文：" + rtMsg);
			if (null != rtMsg) {
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader
						.read(new ByteArrayInputStream(XmlFormat.format(rtMsg).getBytes("UTF-8")));
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp")
						.getText();
				logger_con.info("CdtcAndCnapsAutoPayOperator 批量合同返回报文类型：" + type);
				if (null != type && "CSBS.006.001.01".equalsIgnoreCase(type)) {
					// 返回合同列表
					List<BtchQryRsltBean> btchqList = cdtcRequestMessageCreateDao
							.transformatCSBS00600101ForQryCondConditionOS03AndBJ0301(rtMsg);
					logger_con.info("btchqList.size()=====" + btchqList.size());
					for (int i = 0; i < btchqList.size(); i++) {
						BtchQryRsltBean btchQryRslt = btchqList.get(i);
						if (btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT17.getType())
								|| btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT18.getType())
								|| btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT22.getType())
								|| btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT21.getType())) {

						} else {
							hetongList.add(btchqList.get(i));
						}
					}
				} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) {
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if (spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for (Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat
									.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(),
									systemExceptionAdviceV01);
						}
					}
					if (null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";
						String XcptnRtrTx = "";
						if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap()
									.get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap()
									.get("XcptnRtrTx").getObject().toString();
						}
						logger_con.info("批量指令查询 消息错误码[" + XcptnRtrCd + "]\r\n错误信息[" + XcptnRtrTx + "]");
					}
				}
			}
			/**
			 * 指令查询返回结果和合同查询返回结果进行比较
			 * 
			 * 通过结算指令标示号 匹配
			 */

			int count = 0;
			for (BtchQryRsltBean hetongBean : hetongList) {// 合同查询返回结果
				for (BtchQryRslt zhilingBean : zhilingList) {// 指令查询返回结果
					if (StringUtils.trimToEmpty(hetongBean.getInstrId())
							.equals(StringUtils.trimToEmpty(zhilingBean.getInstrId()))) {
						hetongBean.setTxId(StringUtils
								.trimToEmpty(zhilingBean.getTxId() == null ? "XX" : zhilingBean.getTxId().trim()));
						btchqHashMap.put(count++, hetongBean);
					}
				}
			}

			/**
			 * 合同信息更新到数据库
			 */
			System.out.println("btchqHashMap ++++++" + btchqHashMap.size());
			for (int i = 0; i < btchqHashMap.size(); i++) {
				// 数据库添加语句
				BtchQryRsltBean btchQryRsltBean = btchqHashMap.get(i);
				if (tyjszlztbgDao.updateTyjszlContractMsgStatus(btchQryRsltBean)) {
					logger_con.info("更新通用结算指令表中指令号为：" + btchQryRsltBean.getInstrId() + "的合同信息状态成功！");
					rtb = new RetBean(RetStatusEnum.S, "success", "更新通用结算指令表中指令号合同信息状态成功!");
				} else {
					logger_con.info("更新通用结算指令表中指令号为：" + btchQryRsltBean.getInstrId() + "的合同信息状态失败！");
					rtb = new RetBean(RetStatusEnum.F, "fail", "更新通用结算指令表中指令号合同信息状态失败!");
				}
			}

		} catch (Exception e) {
			logger_con.info("CdtcAndCnapsAutoPayOperator 异常：" + e.getMessage());
			throw e;
		}
		return rtb;

	}
}
