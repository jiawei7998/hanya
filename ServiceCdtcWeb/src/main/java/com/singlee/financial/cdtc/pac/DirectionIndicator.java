package com.singlee.financial.cdtc.pac;

public class DirectionIndicator extends BooleanField{
	
	private static final long serialVersionUID = 1L;

	public DirectionIndicator(String field)
	  {
	    super(field);
	  }

	  public DirectionIndicator(String field,boolean data) {
	    super(field, data);
	  }
}
