/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class BatchCondition5 extends Group {
	
	private static final long serialVersionUID = 1L;
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Id",Field.class);
		keyClassMap.put("Dt",Field.class);
	}
	
	public BatchCondition5(String tag){
		super(tag,tag,new String[]{"Id","Dt"});
	}
	public BatchCondition5(String tag,String delim){
		super(tag,tag,new String[]{"Id","Dt"});
	}

	public void set(Exact11Text value){
		setField(value);
	}
	
	public Exact11Text get(Exact11Text value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(Exact11Text field) {
		return isSetField(field);
	}

	public boolean isSetExact11Text() {
		return isSetField("Id");
	}
	
	public void set(ISODate exact11Text){
		setField(exact11Text);
	}
	
	public ISODate get(ISODate value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(ISODate field) {
		return isSetField(field);
	}

	public boolean isSetISODate() {
		return isSetField("Dt");
	}
	
}
