package com.singlee.financial.cdtc.server.hrb;

import cdc.dmsg.client.DClient;
import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.cdtc.cnbondpackage.HeartBeatMessageV01;
import com.singlee.financial.cdtc.cnbondpackage.HrtBtMsg;
import com.singlee.financial.cdtc.cnbondpackage.ReportMsgHeader;
import com.singlee.financial.cdtc.hrb.HrbCdtcTestConnectServer;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.Max8Text;
import com.singlee.financial.cdtc.pac.Priority4Code;
import com.singlee.financial.cdtc.util.EncodingUtil;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.Map;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName HrbCdtcTestConnectServerImpl.java
 * @Description 测试连通性
 * @createTime 2021年11月23日 16:42:00
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCdtcTestConnectServerImpl implements HrbCdtcTestConnectServer {

    @Value("#{cdtcProperties['cdtc.wasIp']}")
    private String wasIp;

    private Logger logger = LoggerFactory.getLogger(HrbCdtcTestConnectServerImpl.class);

    @Override
    public RetBean createHertMessageXml(Map<String,Object> paramer) throws Exception {
        RetBean ret = new RetBean();

        StringBuffer xmlBuffer = new StringBuffer();

        HeartBeatMessageV01 heartBeatMessageV01 = new HeartBeatMessageV01("Document");

        ReportMsgHeader msgHeader = new ReportMsgHeader("Msg");

        Max35Text max35Text  = new Max35Text("TxFlowId");
        String qryCondDt = ParameterUtil.getString(paramer,"qryCondDt","2021-11-11");
        max35Text.setValue(qryCondDt.replace("-", "")+"000000"+ DateUtil.convertDateToYYYYMMDDHHMMSS(new Date()));
        msgHeader.set(max35Text);

        Priority4Code priority4Code = new Priority4Code("Prty");
        priority4Code.setValue(Priority4Code.Type.PT01.getType());
        msgHeader.set(priority4Code);

        ISODateTime isoDateTime = new ISODateTime("CreDtTm", new Date());
        msgHeader.set(isoDateTime);

        HrtBtMsg hrtBtMsg = new HrtBtMsg("HrtBtMsg");
        Max8Text max8Text = new Max8Text("HrtBtId");
        max8Text.setValue(DateUtil.getNow("HHmmss"));
        hrtBtMsg.set(max8Text);

        heartBeatMessageV01.set(msgHeader);
        heartBeatMessageV01.set(hrtBtMsg);

        heartBeatMessageV01.toXmlForSuperParent(xmlBuffer);

        return new RetBean(RetStatusEnum.S, "success", xmlBuffer.toString());
    }

    @Override
    public RetBean sendHertMessageXml(Map<String,Object> paramer)  {
        RetBean ret = new RetBean();
        try {
            String sendMsg = ParameterUtil.getString(paramer,"",null);
            DClient clientPL = new DClient(wasIp);
            logger.info("HeartBeatMessageV01心跳信息请求报文：" + sendMsg);
            String backXml = null;

            backXml = clientPL.sendMsg(XmlFormat.format(sendMsg));
            backXml = new String(backXml.getBytes(EncodingUtil.getEncoding(backXml)),
                    "GBK");
            logger.info("回执报文:" + backXml);
            if (null != backXml) {
                SAXReader reader = new SAXReader();
                org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(
                        XmlFormat.format(backXml).getBytes("UTF-8")));
                String type = xmlDoc.getRootElement().element("MsgHeader")
                        .element("MsgDesc").element("MsgTp").getText();
                if (null != type&&type.equals("CSBS.009.001.01")){
                    return new RetBean(RetStatusEnum.F, "fail", "发送心跳报文，读取回执报文失败");
                }else {
                    return new RetBean(RetStatusEnum.S, "success", backXml);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
