package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IntfcGentBeanList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<IntfcGentBean> intfcGentList = new ArrayList<IntfcGentBean>();

	public IntfcGentBeanList(List<IntfcGentBean> intfcGentList){
		super();
		this.intfcGentList = intfcGentList;
	}
	
	 /**
     * 根据下标返回机构对象
     * @param code
     * @return
     */
    public IntfcGentBean getIntfcGentBean(String code){
        int index = indexOf(code);
        return index >=0 ? (IntfcGentBean)intfcGentList.get(index) : null;
    }
    
    /**
     * 查找机构下标，没有找到返回-1
     * @param code
     * @return
     */
    public int indexOf(String code){
        for(int i=0; i<intfcGentList.size(); i++){
        	IntfcGentBean IntfcGent = (IntfcGentBean)intfcGentList.get(i);
            if(IntfcGent.getPARAID().concat(IntfcGent.getPART1()).equals(code)){
                return i;
            }
        }
        return -1;
    }
    
    
    /**
     * 添加
     * @param SL_WF_PROD
     */
    public void add(IntfcGentBean prod){
    	intfcGentList.add(prod);
    }
    
    /**
     * 删除
     * @param SL_WF_PROD
     */
    public int del(IntfcGentBean IntfcGent){
        int index = indexOf(IntfcGent.getPARAID().concat(IntfcGent.getPART1()));
        if(index != -1) {
        	intfcGentList.remove(index);}
        return index;
    }
    
    /**
     * 更新 = 删除->添加
     * @param SL_WF_PROD
     */
    public void upd(IntfcGentBean prod){
        int index = del(prod);
        intfcGentList.add(index, prod);
    }

	/**
	 * @return the intfcGentList
	 */
	public List<IntfcGentBean> getIntfcGentList() {
		return intfcGentList;
	}

	/**
	 * @param intfcGentList the intfcGentList to set
	 */
	public void setIntfcGentList(List<IntfcGentBean> intfcGentList) {
		this.intfcGentList = intfcGentList;
	}
}
