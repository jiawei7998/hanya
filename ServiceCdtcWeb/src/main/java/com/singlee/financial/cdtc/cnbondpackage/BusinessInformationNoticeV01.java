package com.singlee.financial.cdtc.cnbondpackage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;

import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.Max70Text;
import com.singlee.financial.cdtc.pac.MessageTypeCode;
import com.singlee.financial.cdtc.pac.Priority4Code;

public class BusinessInformationNoticeV01 extends Group {

	/**
	 * 6.3.10	业务消息通知（CSBS.042.001.01 BusinessInformationNoticeV01）
	 * 
	 * 业务消息通知  报文由中央债券综合业务系统发送给结算成员，用于业务消息的通知
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static String type = "CSBS.010.001.01";
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	
	static{
		keyClassMap.put("Msg",        ReportMsgHeader.class);
		keyClassMap.put("BizInfNtce", BizInfNtce.class);
		
		keyClassMap.putAll(ReportMsgHeader.keyClassMap);
		keyClassMap.putAll(BizInfNtce.keyClassMap);
	}
	
	public boolean isHasTag(String tag)
	{
		String[] strings = getFieldOrder();
		for(String key:strings)
		{
			if(key.equals(tag))
			{
				return true;
			}
		}
		return false;
	}
	
	public BusinessInformationNoticeV01(String tag){
		super(tag,tag, new String[] { "Msg", "BizInfNtce"});
	} 
	public BusinessInformationNoticeV01(String tag,String deli){
		
		super(tag,tag, new String[] { "Msg", "BizInfNtce"});
	}
	
	public void set(ReportMsgHeader msg){
		addGroup(msg);
		
	}
	
	public void set(BizInfNtce bizInfNtce){
		addGroup(bizInfNtce);
	}
	
	
	/************************************************************************/
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	
	public static BusinessInformationNoticeV01 content(Bean1 bean1) {
		BusinessInformationNoticeV01 businessInformationNoticeV01 = new BusinessInformationNoticeV01("BusinessInformationNoticeV01");
		
		ReportMsgHeader msg = new ReportMsgHeader("Msg");
		
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("交易流水标识");
		msg.set(max35Text);
		
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT01.getType());
		msg.set(priority4Code);
		
		ISODateTime isoDateTime = new ISODateTime("CreDtTm",new Date());
		msg.set(isoDateTime);
		
		BizInfNtce bizInfNtce = new BizInfNtce("BizInfNtce");//业务消息通知
		Max35Text InfId = new Max35Text("InfId");
		InfId.setValue("消息标识号");
		bizInfNtce.set(InfId);
		
		MessageTypeCode InfTp = new MessageTypeCode("InfTp");
		InfTp.setValue(MessageTypeCode.Type.MT01.getType());
		bizInfNtce.set(InfTp);
		
		Max35Text Key = new Max35Text("Key");
		Key.setValue("关键字");
		bizInfNtce.set(Key);
		
		Max70Text Abstract = new Max70Text("Abstract");
		Abstract.setValue("摘要");
		bizInfNtce.set(Abstract);
		businessInformationNoticeV01.set(bizInfNtce);
		businessInformationNoticeV01.set(msg);
		
		return businessInformationNoticeV01;
	}
	
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception  {
		
		
		
		StringBuffer xmlBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
		
		// 保存xml文件
		org.desy.trd.jdom.input.SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat.format(xmlBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out=new FileOutputStream("./cnbondxml/"+BusinessInformationNoticeV01.type+".xml"); 
		XMLOutputter outputter = new XMLOutputter(); 
		//如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的 
		outputter.output((org.desy.trd.jdom.Document) doc, out); 
		out.close(); 
		
		 /**
         * 读取请求报文XML  反解析报文
         */
		SAXReader reader = new SAXReader();
		BusinessInformationNoticeV01 notice = new BusinessInformationNoticeV01("businessInformationNoticeV01");
		org.dom4j.Document xmlDoc = reader.read(new File("./cnbondxml/"+notice.type+".xml"));
		XMLRevBussiInforNoticeV01.getElementList(xmlDoc.getRootElement(),notice);
		
//		System.out.println("*********************************************************");
//		StringBuffer xmlBuffer2 = new StringBuffer();
//		notice.toXmlForSuperParent(xmlBuffer2);
//		System.out.println("*********************************************************");
//		System.out.println(XmlFormat.format(xmlBuffer2.toString()));
		
	}

}
