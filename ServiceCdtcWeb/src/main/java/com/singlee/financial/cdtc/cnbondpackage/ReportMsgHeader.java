package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.PartyIdentification;
import com.singlee.financial.cdtc.pac.Priority4Code;

public class ReportMsgHeader extends Group {

	/**
	 *  所有报文头信息
	 *  
	 *  描述报文基本信息
	 */
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("TxFlowId",     Field.class);
		keyClassMap.put("Oprtr", 	    PartyIdentification.class);
		keyClassMap.put("Prty",      	Field.class);
		keyClassMap.put("CreDtTm", 		Field.class);
		keyClassMap.putAll(PartyIdentification.keyClassMap);
	}
	

	/**
	 *  消息头
	 * TxFlowId 交易流水标识
	 * Oprtr    
	 * Prty     优先级
	 * CreDtTm  报文创建时间
	 * @param tag
	 */
	public ReportMsgHeader(String tag){
		
		super(tag,tag, new String[] {"TxFlowId","Oprtr","Prty","CreDtTm"});
		
	}
	
	public ReportMsgHeader(String tag,String deli){
		
		super(tag,tag, new String[] {"TxFlowId","Oprtr","Prty","CreDtTm"});
		
	}
	
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}
	
	public void set(Max35Text max35Text){
		setField(max35Text);
	}
	
	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(PartyIdentification partyIdentification){
		addGroup(partyIdentification);
		
	}
	
	public void set(Priority4Code priority4Code){
		setField(priority4Code);
	}
	
	public Priority4Code get(Priority4Code value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void set(ISODateTime iSODateTime){
		setField(iSODateTime);
	}
	
	public ISODateTime get(ISODateTime value) throws FieldNotFound {
		getField(value);
		return value;
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer)
	{
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
	}

}
