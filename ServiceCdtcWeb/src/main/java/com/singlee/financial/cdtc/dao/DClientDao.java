package com.singlee.financial.cdtc.dao;

import java.util.HashMap;

public interface DClientDao {
	/**
	 * 将XML 发送给CDTC 返回报文
	 * @param xml
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, String> sendXmlMessageToCdtcForRequest(String xml) throws Exception;

	public HashMap<String, String> sendXmlMessageToCdtcForRequest(String xml,String userName) throws Exception;
}
