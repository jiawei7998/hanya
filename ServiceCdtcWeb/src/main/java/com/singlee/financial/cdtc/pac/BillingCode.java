package com.singlee.financial.cdtc.pac;


/**
 *  BC00：债券登记服务费
BC01：付息兑付服务费
BC02：托管账户维护费
BC03：债券托管服务费
BC04：结算过户服务费
BC05：应急业务服务费

 * @author Administrator
 *
 */
public class BillingCode extends StringField {
	
	public static enum Type {
		BC00("BC00","债券登记服务费"),
		BC01("BC01","付息兑付服务费"),
		BC02("BC02","托管账户维护费"),
		BC03("BC03","债券托管服务费"),
		BC04("BC04","结算过户服务费"),
		BC05("BC05","应急业务服务费");
		private String type;
		
		private String name;

		
		private Type(String type, String name) {
			this.type = type;
			this.name = name;
		}
		// 普通方法   
	    public static String getName(String type) {   
	        for (Type c : Type.values()) {   
	            if (c.getType().equalsIgnoreCase(type)) {   
	               return c.name;   
	            }   
	        }   
	        return "";   
	    }  
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return this.type+":"+this.name;
		}
	}
	public BillingCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";

	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
