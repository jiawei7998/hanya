package com.singlee.financial.cdtc.pac;


/**
 * 
 * 3位英文币种属性和金额总长12位，最多2位小数
 * @author Administrator
 *
 */
public class CommonCurrencyAndAmount extends StringField{

	private String ccy;
	
	public CommonCurrencyAndAmount(String field,String ccy) {
		super(field);
		this.ccy = ccy;
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

	private static final String regStr12="^[0-9]{1,12}$";
	private static final String regStr2="^[0-9]{0,2}$";
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().replace(".", "").matches(regStr12))
		{
			if((objectAsString().indexOf(".")>0 && objectAsString().substring(objectAsString().indexOf(".")+1).matches(regStr2)) || objectAsString().indexOf(".")<0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append("<").append(getField()).append(" ccy=").append('"').append(ccy).append('"').append(">").append(objectAsString())
				.append("</").append(getField()).append(">");
				return stringBuilder.toString();
			}
		}
		return "";
	}
	
	public static void main(String[] args) {
		CommonCurrencyAndAmount faceCurrencyAndAmount = new CommonCurrencyAndAmount("amt","CNY");
		faceCurrencyAndAmount.setValue("1234567890.99");
		System.out.println(faceCurrencyAndAmount.toXmlString());
	}

	/**
	 * @return the ccy
	 */
	public String getCcy() {
		return ccy;
	}

	/**
	 * @param ccy the ccy to set
	 */
	public void setCcy(String ccy) {
		this.ccy = ccy;
	}
}
