/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;

/**
 * @author Fang
 * 6.3.5结算业务查询请求报文结构表
 */
public class SettlementBusinessQueryRequest extends Group {

	private static final long serialVersionUID = 1L;
	
	private static String type = "CSBS.005.001.01";
	
	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static {
		keyClassMap.put("Msg", TransactionMsg.class);
		keyClassMap.put("OprtrFlg", TransactionOprtrFlg.class);
		keyClassMap.put("AcctQry", AccountIdentificationAndName4.class);
		keyClassMap.put("QryCond", Condition.class);
		keyClassMap.put("QryPty", TransactionGivInf.class);
		keyClassMap.put("Xtnsn", Extension.class);
		keyClassMap.putAll(TransactionMsg.keyClassMap);
		keyClassMap.putAll(TransactionOprtrFlg.keyClassMap);
		keyClassMap.putAll(AccountIdentificationAndName4.keyClassMap);
		keyClassMap.putAll(Condition.keyClassMap);
		keyClassMap.putAll(TransactionGivInf.keyClassMap);
		keyClassMap.putAll(Extension.keyClassMap);
	}
	
	
	public SettlementBusinessQueryRequest(String tag){
		super(tag,tag,new String[]{"Msg","OprtrFlg","AcctQry","QryCond","QryPty","Xtnsn"});
	}
	public SettlementBusinessQueryRequest(String tag,String belim){
		super(tag,tag,new String[]{"Msg","OprtrFlg","AcctQry","QryCond","QryPty","Xtnsn"});
	}
	
	public void set(TransactionMsg group){
		addGroup(group);
	}
	
	public void set(TransactionOprtrFlg group){
		addGroup(group);
	}
	public void set(AccountIdentificationAndName4 group){
		addGroup(group);
	}
	public void set(Condition group){
		addGroup(group);
	}
	public void set(Extension group){
		addGroup(group);
	}
	public void set(TransactionGivInf group){
		addGroup(group);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	
	public static SettlementBusinessQueryRequest content(Bean1 bean1){
		SettlementBusinessQueryRequest setbuqu = new SettlementBusinessQueryRequest("Document");
		/**********************************报文标识*********************************/
		TransactionMsg transactionMsg = new TransactionMsg("Msg");
		//交易流水标识
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("01");
		transactionMsg.set(max35Text);
		//经办人
		PartyIdentification partyIdentification = new PartyIdentification("Oprtr");
		Max35Text max35TextOprtr = new Max35Text("Nm");
		max35TextOprtr.setValue("A");
		partyIdentification.set(max35TextOprtr);
		transactionMsg.set(partyIdentification);
		//报文创建时间
		transactionMsg.set(new ISODateTime("CreDtTm", new Date()));
		
		/**************************操作标识************************/
		TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");
		//请求类别码
		RequestTypeCode reqTp = new RequestTypeCode("ReqTp");
		reqTp.setValue(RequestTypeCode.Type.BJ0100.getType());
		transactionOprtrFlg.set(reqTp);
		//操作码
		OperatorStatusCode operSc = new OperatorStatusCode("OprtrSts");
		operSc.setValue(OperatorStatusCode.Type.OS00.getType());
		transactionOprtrFlg.set(operSc);
		
		/*********************查询账户*******************/
		AccountIdentificationAndName4 acctQry = new AccountIdentificationAndName4("AcctQry");
		Max35Text nm = new Max35Text("Nm");
		nm.setValue("Fang");
		Exact11Text id = new Exact11Text("Id");
		id.setValue("12345678900");
		acctQry.set(nm);
		acctQry.set(id);
		
		/******************查询条件******************/
		Condition qryCond = new Condition("QryCond");
		Max12Text condid = new Max12Text("Id");
		condid.setValue("SS000");
		qryCond.set(condid);
		qryCond.set(new ISODate("QryDt",new Date()));
		setbuqu.set(qryCond);
		
		/***************************查询方*****************/
		TransactionGivInf qryPty = new TransactionGivInf("QryPty");
		//名称
		Max70Text max70TextGviInf = new Max70Text("Nm");
		max70TextGviInf.setValue("ABC");
		qryPty.set(max70TextGviInf);
		//自定义标识
		GenericIdentification4 genid = new GenericIdentification4("PrtryId");
		Max35Text maxid = new Max35Text("Id");
		maxid.setValue("SS123");
		genid.set(maxid);
		Max35Text maxIdIp = new Max35Text("IdTp");
		maxIdIp.setValue("DD123");
		genid.set(maxIdIp);
		qryPty.set(genid);
		//代理机构
		AgentParty agentParty = new AgentParty("AgtPty");
		AgentPartyTypeCode agtPtyTP = new AgentPartyTypeCode("AgtPtyTP");
		agtPtyTP.setValue(AgentPartyTypeCode.Type.AP00.getType());
		agentParty.set(agtPtyTP);
		
		PartyIdentification30 agtPtyId = new PartyIdentification30("AgtPtyId");
		Max70Text agtPtyIdNm = new Max70Text("Nm");
		agtPtyIdNm.setValue("F");
		agtPtyId.set(agtPtyIdNm);
		
		GenericIdentification4 prtryId = new GenericIdentification4("PrtryId");
		Max35Text prtryIdId = new Max35Text("Id");
		prtryIdId.setValue("SFH000");
		prtryId.set(prtryIdId);
		Max35Text paityIdTp = new Max35Text("IdTp");
		paityIdTp.setValue("IdTp000");
		prtryId.set(paityIdTp);
		agtPtyId.set(prtryId);
		agentParty.set(agtPtyId);
		qryPty.set(agentParty);
		
		//拓展项
		Extension extension = new Extension("Xtnsn");
		Max2048Text max2048Text = new Max2048Text("XtnsnTxt");
		max2048Text.setValue("债券账户对账单查询请求报文完毕");
		extension.set(max2048Text);
		
		setbuqu.set(transactionMsg);
		setbuqu.set(transactionOprtrFlg);
		setbuqu.set(acctQry);
		setbuqu.set(qryPty);
		setbuqu.set(extension);
		return setbuqu;
	}
	
	public static void main(String[] args) throws Exception {
		
		StringBuffer stringBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(stringBuffer);

		System.out.println(XmlFormat.format(stringBuffer.toString()));

		
		// 保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat
				.format(stringBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out = new FileOutputStream("./cnbondxml/"
				+ SettlementBusinessQueryRequest.type + ".xml");
		XMLOutputter outputter = new XMLOutputter();
		// 如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的
		outputter.output((Document) doc, out);
		out.close();
		/**
		 * 反解析报文
		 */
		SAXReader reader = new SAXReader();
		SettlementBusinessQueryRequest commondssReport1 = new SettlementBusinessQueryRequest(
				"Document");
		org.dom4j.Document docXml = reader.read(new File("./cnbondxml/"
				+ SettlementBusinessQueryRequest.type + ".xml"));
		getElementList(docXml.getRootElement(), commondssReport1);
		
		
		StringBuffer xmlBuffer = new StringBuffer();
		commondssReport1.toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
		
		//commondssReport1.toXmlForSuperParent(xmlBuffer);
		
	}
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * @param element
	 * @param common
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	@SuppressWarnings("unchecked")
	public static void getElementList(Element element,SettlementBusinessQueryRequest common)throws Exception,
	NoSuchMethodException{
		List<Element> elements = element.elements();
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = iterator.next();
			List<Element> elements2 = element2.elements();
			if(elements2.size() >= 0){
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
				if(common.isHasTag(StringUtils.trimToEmpty(element2.getName()))){
					if(element2.elements().size() > 0){
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
						common.addGroup(getGroups(element2, common));
					}else{
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
						Class<?>[] parameterTypes = {String.class,Object.class};
						Constructor<?> constructor = SettlementBusinessQueryRequest.keyClassMap
						.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
						Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element.getText())};
						Object object = constructor.newInstance(parameters);
						common.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
					}
				}
			}
		}
	}
	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception反解析报文
	 */
	public static Group getGroups(Element element,
			SettlementBusinessQueryRequest common)
			throws NoSuchMethodException, Exception {
		//Group group = new Group(StringUtils.trimToEmpty(element.getName()),"");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = SettlementBusinessQueryRequest.keyClassMap.get(StringUtils.trimToEmpty(element.getName())).getConstructor(parameterTypes2);
		Object[] parameters2 = {StringUtils.trimToEmpty(element.getName()),""};
		Group group = (Group) constructor2.newInstance(parameters2);
		
		System.out.println("************************************");
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
			if(element2.elements().size() > 0){
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
				group.addGroup(getGroups(element2, common));
			}else{
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
				Class<?>[] parameterTypes = {String.class,Object.class};
				Constructor<?> constructor = SettlementBusinessQueryRequest.keyClassMap
				.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
				Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element2.getText())};
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
			}
		}
		System.out.println("************************************");
		return group;
	}
}
