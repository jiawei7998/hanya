/**
 * 
 */
package com.singlee.financial.cdtc.cnbondpackage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.pac.*;

/**
 * @author Fang
 * 6.3.11	批量数据查询请求(CSBS.043.001.01)
 */
public class BatchDataQueryRequestV01 extends Group {

	private static final long serialVersionUID = 1L;
	private static String type = "CSBS.043.001.01";

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("Msg",ReportMsgHeader.class);
		keyClassMap.put("QryFunc", Field.class);
		keyClassMap.put("QryCond", BatchCondition.class);
		keyClassMap.put("QryPty", TransactionGivInf.class);
		keyClassMap.put("Oprtr", PartyIdentification.class);
		keyClassMap.put("Xtnsn", Extension.class);
		keyClassMap.putAll(ReportMsgHeader.keyClassMap);
		keyClassMap.putAll(BatchCondition.keyClassMap);
		keyClassMap.putAll(TransactionGivInf.keyClassMap);
		keyClassMap.putAll(PartyIdentification.keyClassMap);
		keyClassMap.putAll(Extension.keyClassMap);
	}

	public BatchDataQueryRequestV01(String tag) {
		super(tag, tag, new String[] {"Msg","QryFunc","QryCond","QryPty","Oprtr","Xtnsn"});
	}
	public BatchDataQueryRequestV01(String tag,String belim) {
		super(tag, tag, new String[] {"Msg","QryFunc","QryCond","QryPty","Oprtr","Xtnsn"});
	}
	public void set(ReportMsgHeader group) {
		addGroup(group);
	}
	public void set(BatchFunctionCode field){
		setField(field);
	}
	public void set(BatchCondition group){
		addGroup(group);
	}
	public void set(TransactionGivInf group){
		addGroup(group);
	}
	public void set(PartyIdentification group){
		addGroup(group);
	}
	public void set(Extension group){
		addGroup(group);
	}
	/** ********************************************************************* */
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}
	
	public static BatchDataQueryRequestV01 content(Bean1 bean1) {
		BatchDataQueryRequestV01 batchDataQueryRequestV01 = new BatchDataQueryRequestV01(
				"Document");
		/***********************报文标识**********************/
		ReportMsgHeader msgHeader = new ReportMsgHeader("Msg");
		//交易流水标识
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("HeartBeatMessageV01");
		msgHeader.set(max35Text);
		//优先级
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT01.getType());
		msgHeader.set(priority4Code);
		//报文创建时间
		ISODateTime isoDateTime = new ISODateTime("CreDtTm", new Date());
		msgHeader.set(isoDateTime);
		batchDataQueryRequestV01.set(msgHeader);

		/**************************查询功能点************************/
		BatchFunctionCode qryFunc = new BatchFunctionCode("QryFunc");
		qryFunc.setValue(BatchFunctionCode.Type.BF02.getType());
		batchDataQueryRequestV01.set(qryFunc);
		
		/**************************查询条件*********************/
		BatchCondition qryCond = new BatchCondition("QryCond");
		//批量条件1
		BatchCondition1 bthCond1 = new BatchCondition1("BthCond1");
		Exact11Text id = new Exact11Text("Id");
		id.setValue("12345678910");
		bthCond1.set(id);
		bthCond1.set(new ISODate("StrtDt", new Date()));
		bthCond1.set(new ISODate("EndDt", new Date()));
		qryCond.set(bthCond1);
		//批量条件2
		BatchCondition2 bthCond2 = new BatchCondition2("BthCond2");
		bthCond2.set(new ISODate("StrtDt", new Date()));
		bthCond2.set(new ISODate("EndDt", new Date()));
		qryCond.set(bthCond2);
		//批量条件3
		BatchCondition3 bthCond3 = new BatchCondition3("BthCond3");
		bthCond3.set(new ISODate("StrtDt", new Date()));
		bthCond3.set(new ISODate("EndDt", new Date()));
		CommonCurrencyAndAmount yrsToMtrty = new CommonCurrencyAndAmount("YrsToMtrty","CNY");
		yrsToMtrty.setValue("1234567890.99");
		bthCond3.set(yrsToMtrty);
		qryCond.set(bthCond3);
		//批量条件4
		BatchCondition4 bthCond4 = new BatchCondition4("BthCond4");
		bthCond4.set(new ISODate("StrtDt", new Date()));
		bthCond4.set(new ISODate("EndDt", new Date()));
		CommonCurrencyAndAmount yrsToMtrty4 = new CommonCurrencyAndAmount("YrsToMtrty","CNY");
		yrsToMtrty4.setValue("1234567890.99");
		bthCond4.set(yrsToMtrty4);
		CommonCurrencyAndAmount endYrsToMtrty = new CommonCurrencyAndAmount("EndYrsToMtrty","CNY");
		endYrsToMtrty.setValue("1234567890.99");
		bthCond4.set(endYrsToMtrty);
		qryCond.set(bthCond4);
		//批量条件5
		BatchCondition5 bthCond5 = new BatchCondition5("BthCond5");
		Exact11Text id5 = new Exact11Text("Id");
		id5.setValue("12345678910");
		bthCond5.set(id5);
		bthCond5.set(new ISODate("Dt", new Date()));
		qryCond.set(bthCond5);
		batchDataQueryRequestV01.set(qryCond);
		
		/******************************查询方******************/
		TransactionGivInf qryPty = new TransactionGivInf("QryPty");
		//名称
		Max70Text max70TextGviInf = new Max70Text("Nm");
		max70TextGviInf.setValue("ABC");
		qryPty.set(max70TextGviInf);
		//自定义标识
		GenericIdentification4 genid = new GenericIdentification4("PrtryId");
		Max35Text maxid = new Max35Text("Id");
		maxid.setValue("SS123");
		genid.set(maxid);
		Max35Text maxIdIp = new Max35Text("IdTp");
		maxIdIp.setValue("DD123");
		genid.set(maxIdIp);
		qryPty.set(genid);
		batchDataQueryRequestV01.set(qryPty);
		/******************经办员******************/
		PartyIdentification oprtr = new PartyIdentification("Oprtr");
		Max35Text nm = new Max35Text("Nm");
		nm.setValue("ABC");
		oprtr.set(nm);
		batchDataQueryRequestV01.set(oprtr);
		
		/**********************拓展项******************/
		Extension extension = new Extension("Xtnsn");
		Max2048Text max2048Text = new Max2048Text("XtnsnTxt");
		max2048Text.setValue("债券账户对账单查询请求报文完毕");
		extension.set(max2048Text);
		batchDataQueryRequestV01.set(extension);
		
		return batchDataQueryRequestV01;
	}
	public static void main(String[] args) throws Exception {
		
		StringBuffer stringBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(stringBuffer);

		System.out.println(XmlFormat.format(XmlFormat.format(stringBuffer.toString())));

		//保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat
				.format(stringBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out = new FileOutputStream("./cnbondxml/" + BatchDataQueryRequestV01.type
				+ ".xml");
		XMLOutputter outputter = new XMLOutputter();
		// 如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的
		outputter.output((Document) doc, out);
		out.close();
		
		System.out.println("反解析报文开始");
		SAXReader reader = new SAXReader();
		/**
		 * 反解析报文
		 */
		BatchDataQueryRequestV01 safekeeping = new BatchDataQueryRequestV01(
				"Document");
		org.dom4j.Document docXml = reader.read(new File("./cnbondxml/"
				+ BatchDataQueryRequestV01.type + ".xml"));
		getElementList(docXml.getRootElement(), safekeeping);
		
		
		StringBuffer xmlBuffer = new StringBuffer();
		safekeeping.toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
	}
	
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * @param element
	 * @param common
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	public static void getElementList(Element element,BatchDataQueryRequestV01 common)throws Exception,
	NoSuchMethodException{
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			List<?> elements2 = element2.elements();
			if(elements2.size() >= 0){
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
				if(common.isHasTag(StringUtils.trimToEmpty(element2.getName()))){
					if(element2.elements().size() > 0){
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
						common.addGroup(getGroups(element2, common));
					}else{
						System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
						Class<?>[] parameterTypes = {String.class,Object.class};
						Constructor<?> constructor = BatchDataQueryRequestV01.keyClassMap
						.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
						Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element.getText())};
						Object object = constructor.newInstance(parameters);
						common.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
					}
				}
			}
		}
	}
	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception反解析报文
	 */
	public static Group getGroups(Element element,
			BatchDataQueryRequestV01 common)
			throws NoSuchMethodException, Exception {
		//Group group = new Group(StringUtils.trimToEmpty(element.getName()),"");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = BatchDataQueryRequestV01.keyClassMap.get(StringUtils.trimToEmpty(element.getName())).getConstructor(parameterTypes2);
		Object[] parameters2 = {StringUtils.trimToEmpty(element.getName()),""};
		Group group = (Group) constructor2.newInstance(parameters2);
		
		System.out.println("************************************");
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();){
			Element element2 = (Element)iterator.next();
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
			if(element2.elements().size() > 0){
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
				group.addGroup(getGroups(element2, common));
			}else{
				System.out.println(String.format("%20s :",StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
				Class<?>[] parameterTypes = {String.class,Object.class};
				Constructor<?> constructor = BatchDataQueryRequestV01.keyClassMap
				.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
				Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element2.getText())};
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
			}
		}
		System.out.println("************************************");
		return group;
	}
}
