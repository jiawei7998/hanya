package com.singlee.financial.cdtc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;


public interface CnbondManagerMapper {
	/**
	 * 添加报文数据
	 * @param cnbond
	 * @return
	 */
	void addSlintfccnbondpkg(AbstractSlIntfcCnbondPkgId cnbond);
	
	
	/**
	 * 根据名称读取报文数据
	 */
	AbstractSlIntfcCnbondPkgId getSlintfccnbondpkgByMsgtype(@Param(value = "msgfilename")String msgfilename);
	
	/**
	 * 修改报文数据
	 */
    int updateCnbond(AbstractSlIntfcCnbondPkgId cnbond);

	/**
	 * 获取日期下的全部通用结算指令报告BEAN
	 */
	List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDbByDateType(@Param(value = "date") String date ,@Param(value = "type") String type);
	
	/**
	 * 获取全部通用结算指令报告BEAN
	 */
	List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDb();
	/**
	 * 根据条件查询
	 */
	List<AbstractSlIntfcCnbondPkgId> queryAllCSBSBeanFromDbByTrem(@Param("type") String type,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("txtMsgTypeName") String txtMsgTypeName);

}
