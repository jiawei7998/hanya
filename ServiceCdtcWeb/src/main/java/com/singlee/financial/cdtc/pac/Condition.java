/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 * 查询条件
 */
public class Condition extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static {
		keyClassMap.put("Id", Field.class);
		keyClassMap.put("QryDt", Field.class);
	}
	
	public Condition(String tag){
		super(tag,tag,new String[]{"Id","QryDt"});
	}
	public Condition(String tag,String delim){
		super(tag,tag,new String[]{"Id","QryDt"});
	}
	public void set(Max12Text field){
		setField(field);
	}
	 public Max12Text get(Max12Text value) throws FieldNotFound
     {
       getField(value);

       return value;
     }

     public Max12Text getMax12Text() throws FieldNotFound
     {
    	 Max12Text value = new Max12Text("");
         getField(value);

         return value;
     }

     public boolean isSet(Max12Text field) {
       return isSetField(field);
     }

     public boolean isSetMax12Text() {
       return isSetField("Id");
     }
	
	public void set(ISODate field){
		setField(field);
	}
	
    public void toXmlForSuperParent(StringBuffer xmlBuffer)
    {
  	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
  	  this.toXml(xmlBuffer);
  	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
    }
}
