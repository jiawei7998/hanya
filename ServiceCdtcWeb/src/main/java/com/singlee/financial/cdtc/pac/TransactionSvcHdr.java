package com.singlee.financial.cdtc.pac;

public class TransactionSvcHdr extends Group {
	private static final long serialVersionUID = 1L;
	
	public TransactionSvcHdr(String tag) {
		super(tag, tag, new String[] {"corrId","svcId","verNbr", "csmrId","pvdrId","pvdrSerNbr","respMsg","tmStamp"});
	}

	public void set(Max8Text value) {
		setField(value);
	}
	


	public Max8Text get(Max8Text value)
			throws FieldNotFound {
		getField(value);

		return value;
	}

	public boolean isSet(Max8Text field) {
		return isSetField(field);
	}

	public boolean isSetDirectionIndicator() {
		return isSetField("verNbr");
	}

	public void set(Max6Text value){
		setField(value);
	}
	
	public Max6Text get(Max6Text value)
			throws FieldNotFound {
		getField(value);

		return value;
	}

	public boolean isSet(Max6Text field) {
		return isSetField(field);
	}

	public boolean isSetMax6Text() {
		return isSetField("csmrId");
	}
	
	public void set(Max32Text value){
		setField(value);
	}
	
	public void set(Max64Text value) {
		setField(value);
	}
	
	public void set(Max256Text value){
		setField(value);
	}
	public void set(ISODateTime value) {
		setField(value);
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

}
