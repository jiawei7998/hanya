package com.singlee.financial.cdtc.server;

public class DMSGconsumerThreadXMLInit {
	
private CdtcDownMsgXmlThreadServer cdtcDownMsgXmlThreadServer;

	public void init(){
		Thread myThread = new Thread(new Runnable(){
			@Override
			public void run() {
				cdtcDownMsgXmlThreadServer.start();
			}
		});
		myThread.start();
	}

	public CdtcDownMsgXmlThreadServer getCdtcDownMsgXmlThreadServer() {
		return cdtcDownMsgXmlThreadServer;
	}

	public void setCdtcDownMsgXmlThreadServer(
			CdtcDownMsgXmlThreadServer cdtcDownMsgXmlThreadServer) {
		this.cdtcDownMsgXmlThreadServer = cdtcDownMsgXmlThreadServer;
	}
	
}
