/**
 * 
 */
package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author shiting
 *
 */
public class Sl_Intfc_SettlmentRepoEttl implements Serializable {

	private static final long serialVersionUID = 1L;

	private String GivAcctId ;//付券方账户
	private String GivAcctNm ;//付券方账户名称
	private String TakAcctId ;//收券方账户
	private String TakAcctNm ;//收券方账户名称
	private String BizTp ;//业务类型   现券 质押式回购 买断式回购
	private BigDecimal AggtFaceAmt ;//债券总额
	private String Dt1 ;//交割日1
	private String CtrctSts ; //合同处理状态CS02
	private String InstrId ;// 结算指令标识
	private String CtrctId ; //结算合同标识
	private String PAYREVID; //收款确认  付款确认标识  P-付款  R-收款
	private String IOPER; //经办人员
	private Date ITIME; //经办时间
	private String VOPER; //复核人员
	private Date VTIME;//复核时间
	private String ADEALWITH; //报文处理标识  'N'初始   'Y'已处理
	private Date ADTIME; //报文处理时间
	private String CSBS003;//确认报文XML报文
	
	private String TxId ;//业务标识号
	private String SttlmTp1 ;//结算方式1
	private String SttlmTp2 ;//结算方式2
	private String BdCnt ;//债券数目
	private BigDecimal Val1 ;//结算金额1
	private BigDecimal Val2 ;//结算金额2
	private String Dt2 ;//交割日2
	private String InstrSts ;//指令处理状态
	private String CtrctBlckSts ;//合同冻结状态
	private String LastUpdTm ;//最新更新时间
	private String OrgtrCnfrmInd ;//发令方确认标识
	private String CtrCnfrmInd ;//对手方确认标识
	private String InstrOrgn;//指令来源
	
	public String getInstrId() {
		return InstrId;
	}
	public void setInstrId(String instrId) {
		InstrId = instrId;
	}
	public String getCtrctId() {
		return CtrctId;
	}
	public void setCtrctId(String ctrctId) {
		CtrctId = ctrctId;
	}
	public String getGivAcctId() {
		return GivAcctId;
	}
	public void setGivAcctId(String givAcctId) {
		GivAcctId = givAcctId;
	}
	public String getTakAcctId() {
		return TakAcctId;
	}
	public void setTakAcctId(String takAcctId) {
		TakAcctId = takAcctId;
	}
	public String getTxId() {
		return TxId;
	}
	public void setTxId(String txId) {
		TxId = txId;
	}
	public String getBizTp() {
		return BizTp;
	}
	public void setBizTp(String bizTp) {
		BizTp = bizTp;
	}
	public String getSttlmTp1() {
		return SttlmTp1;
	}
	public void setSttlmTp1(String sttlmTp1) {
		SttlmTp1 = sttlmTp1;
	}
	public String getSttlmTp2() {
		return SttlmTp2;
	}
	public void setSttlmTp2(String sttlmTp2) {
		SttlmTp2 = sttlmTp2;
	}
	public String getBdCnt() {
		return BdCnt;
	}
	public void setBdCnt(String bdCnt) {
		BdCnt = bdCnt;
	}
	public BigDecimal getAggtFaceAmt() {
		return AggtFaceAmt;
	}
	public void setAggtFaceAmt(BigDecimal aggtFaceAmt) {
		AggtFaceAmt = aggtFaceAmt;
	}
	public BigDecimal getVal1() {
		return Val1;
	}
	public void setVal1(BigDecimal val1) {
		Val1 = val1;
	}
	public BigDecimal getVal2() {
		return Val2;
	}
	public void setVal2(BigDecimal val2) {
		Val2 = val2;
	}
	public String getDt1() {
		return Dt1;
	}
	public void setDt1(String dt1) {
		Dt1 = dt1;
	}
	public String getDt2() {
		return Dt2;
	}
	public void setDt2(String dt2) {
		Dt2 = dt2;
	}
	public String getInstrSts() {
		return InstrSts;
	}
	public void setInstrSts(String instrSts) {
		InstrSts = instrSts;
	}
	public String getCtrctSts() {
		return CtrctSts;
	}
	public void setCtrctSts(String ctrctSts) {
		CtrctSts = ctrctSts;
	}
	public String getCtrctBlckSts() {
		return CtrctBlckSts;
	}
	public void setCtrctBlckSts(String ctrctBlckSts) {
		CtrctBlckSts = ctrctBlckSts;
	}
	public String getLastUpdTm() {
		return LastUpdTm;
	}
	public void setLastUpdTm(String lastUpdTm) {
		LastUpdTm = lastUpdTm;
	}
	public String getOrgtrCnfrmInd() {
		return OrgtrCnfrmInd;
	}
	public void setOrgtrCnfrmInd(String orgtrCnfrmInd) {
		OrgtrCnfrmInd = orgtrCnfrmInd;
	}
	public String getCtrCnfrmInd() {
		return CtrCnfrmInd;
	}
	public void setCtrCnfrmInd(String ctrCnfrmInd) {
		CtrCnfrmInd = ctrCnfrmInd;
	}
	public String getInstrOrgn() {
		return InstrOrgn;
	}
	public void setInstrOrgn(String instrOrgn) {
		InstrOrgn = instrOrgn;
	}
	public String getGivAcctNm() {
		return GivAcctNm;
	}
	public void setGivAcctNm(String givAcctNm) {
		GivAcctNm = givAcctNm;
	}
	public String getTakAcctNm() {
		return TakAcctNm;
	}
	public void setTakAcctNm(String takAcctNm) {
		TakAcctNm = takAcctNm;
	}
	public String getPAYREVID() {
		return PAYREVID;
	}
	public void setPAYREVID(String pAYREVID) {
		PAYREVID = pAYREVID;
	}
	public String getIOPER() {
		return IOPER;
	}
	public void setIOPER(String iOPER) {
		IOPER = iOPER;
	}
	public Date getITIME() {
		return ITIME;
	}
	public void setITIME(Date iTIME) {
		ITIME = iTIME;
	}
	public String getVOPER() {
		return VOPER;
	}
	public void setVOPER(String vOPER) {
		VOPER = vOPER;
	}
	public Date getVTIME() {
		return VTIME;
	}
	public void setVTIME(Date vTIME) {
		VTIME = vTIME;
	}
	public String getADEALWITH() {
		return ADEALWITH;
	}
	public void setADEALWITH(String aDEALWITH) {
		ADEALWITH = aDEALWITH;
	}
	public Date getADTIME() {
		return ADTIME;
	}
	public void setADTIME(Date aDTIME) {
		ADTIME = aDTIME;
	}
	public String getCSBS003() {
		return CSBS003;
	}
	public void setCSBS003(String cSBS003) {
		CSBS003 = cSBS003;
	}
}
