package com.singlee.financial.cdtc.pac;

public class CurrencyCode extends StringField {

	public CurrencyCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}
	private static final String regStr="^[A-Z]{3}$";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
