package com.singlee.financial.cdtc.pac;

public class Exact12NumericText  extends IntField {
	
	private static final long serialVersionUID = 1L;
	/**
	 * 最短1位，最长2位数字[0-9]{1,2}
	 */
	private static final String regStr="^[0-9]{3}$";
	
	public Exact12NumericText(String field) {
		
		super(field);
		// TODO Auto-generated constructor stub
	}

	
	
	/* (non-Javadoc)
	 * @see com.singlee.client.cdtc.pac.IntField#getValue()
	 */
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
	public static void main(String[] args) {
		Exact12NumericText orgnlInstrId = new Exact12NumericText("OrgnlInstrId");
		orgnlInstrId.setValue(123);
		System.out.println(orgnlInstrId.toXmlString());
	}
	
}
