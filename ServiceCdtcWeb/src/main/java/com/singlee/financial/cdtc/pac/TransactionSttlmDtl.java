package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 */
public class TransactionSttlmDtl extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("InstrOrgn", Field.class);
		keyClassMap.put("BizTp", Field.class);
		keyClassMap.put("InstrId", Field.class);
		keyClassMap.put("GivAcct", AccountIdentificationAndName4.class);
		keyClassMap.put("TakAcct", AccountIdentificationAndName4.class);
		keyClassMap.put("SttlmTp1", Field.class);
		keyClassMap.put("SttlmTp2", Field.class);
		keyClassMap.put("Bd1", Bond3.class);
		keyClassMap.put("Bd2", Bond3.class);
		keyClassMap.put("Val1", Field.class);
		keyClassMap.put("Val2", Field.class);
		keyClassMap.put("Val3", Field.class);
		keyClassMap.put("Val4", Field.class);
		keyClassMap.put("AggtFaceAmt", Field.class);
		keyClassMap.put("Rate", Field.class);
		keyClassMap.put("Terms", Field.class);
		keyClassMap.put("Dt1", Field.class);
		keyClassMap.put("Dt2", Field.class);
		keyClassMap.put("SttlmGrte1", SettlementGuarantee.class);
		keyClassMap.put("SttlmGrte2", SettlementGuarantee.class);
		keyClassMap.put("OrgnlInstrId", Field.class);
		keyClassMap.put("OrgnlCtrctId", Field.class);
		keyClassMap.put("CrossTrfAplt", PartyIdentification30.class);
		keyClassMap.put("TxId", Field.class);
		keyClassMap.put("Oprtr", PartyIdentification.class);
		keyClassMap.put("Chckr", PartyIdentification.class);
		keyClassMap.put("Cnfrmr", PartyIdentification.class);
		keyClassMap.putAll(AccountIdentificationAndName4.keyClassMap);
		keyClassMap.putAll(Bond3.keyClassMap);
		keyClassMap.putAll(SettlementGuarantee.keyClassMap);
		keyClassMap.putAll(PartyIdentification30.keyClassMap);
		keyClassMap.putAll(PartyIdentification.keyClassMap);
	}
	
	public TransactionSttlmDtl(String tag) {
		super(tag, tag, new String[] { "InstrOrgn", "BizTp", "InstrId",
				"GivAcct", "TakAcct", "SttlmTp1", "SttlmTp2", "Bd1", "Bd2",
				"Val1", "Val2", "Val3", "Val4", "AggtFaceAmt", "Rate", "Terms",
				"Dt1", "Dt2", "SttlmGrte1", "SttlmGrte2", "OrgnlInstrId",
				"OrgnlCtrctId", "CrossTrfAplt","TxId","Oprtr","Chckr","Cnfrmr" });
	}
	
	public TransactionSttlmDtl(String tag,String delim) {
		super(tag, tag, new String[] { "InstrOrgn", "BizTp", "InstrId",
				"GivAcct", "TakAcct", "SttlmTp1", "SttlmTp2", "Bd1", "Bd2",
				"Val1", "Val2", "Val3", "Val4", "AggtFaceAmt", "Rate", "Terms",
				"Dt1", "Dt2", "SttlmGrte1", "SttlmGrte2", "OrgnlInstrId",
				"OrgnlCtrctId", "CrossTrfAplt","TxId","Oprtr","Chckr","Cnfrmr" });
	}
	
	public void set(InstructionOriginCode field){
		setField(field);
	}
	public void set(BusinessTypeCode field){
		setField(field);
	}
	public void set(Exact12NumericText field){
		setField(field);
	}
	public void set(Exact9NumericText field){
		setField(field);
	}
	public void set(AccountIdentificationAndName4 group){
		addGroup(group);
	}
	public void set(BondSettlementType2Code field){
		setField(field);
	}
	public void set(Bond3 group){
		addGroup(group);
	}
	public void set(CommonCurrencyAndAmount field){
		setField(field);
	}
	public void set(FaceCurrencyAndAmount field){
		setField(field);
	}
	public void set(PercentageRate field){
		setField(field);
	}
	public void set(Max35NumericText field){
		setField(field);
	}
	public void set(ISODate field){
		setField(field);
	}
	public void set(SettlementGuarantee group){
		addGroup(group);
	}
	public void set(PartyIdentification30 group){
		addGroup(group);
	}
	public void set(PartyIdentification group){
		addGroup(group);
	}
	public void set(Max20Text field){
		setField(field);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}

}
