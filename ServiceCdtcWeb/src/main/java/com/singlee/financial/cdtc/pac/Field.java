package com.singlee.financial.cdtc.pac;

import java.io.Serializable;

public class Field<T> implements Serializable{

	private static final long serialVersionUID = -1082964650341603326L;
	/**
	 * XML的TAG NAME
	 */
	private String field;
	/**
	 * 类型
	 */
    private T object;
    /**
     * 转换后的字符串VALUE
     */
    private String data;
    
    public Field(String field, T object)
    {
      this.field = field;
      this.object = object;
    }
    @Override
    public String toString()
    {
      calculate();
      return this.data;
    }

    void toString(StringBuffer buffer) {
      buffer.append(this.field).append('=').append(objectAsString());
    }

    protected String objectAsString() {
      return this.object.toString();
    }
    
    private void calculate() {

        StringBuffer buffer = new StringBuffer();
        toString(buffer);
        this.data = buffer.toString();

      }
    
    int getDataLength()
    {
      calculate();
      byte[] bytes = getBytes(this.data);
      return bytes.length + 1;
    }
    
    int getLength()
    {
      byte[] bytes = getBytes(objectAsString());
      return bytes.length;
    }
    @Override
    public int hashCode() {
        return this.object.hashCode();
     }
    
    private static byte[] getBytes(String s)
    {
      try
      {
        return s.getBytes(CharsetSupport.getCharset()); 
       } catch (Exception e) {
        	throw new RuntimeException(e);
      }
      
    }
    
    public Field() {
		super();
	}

	public Field(String tag, T object, String data) {
		super();
		this.field = tag;
		this.object = object;
		this.data = data;
	}

	/**
	 * @return the tag
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param tag the tag to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * @return the object
	 */
	public T getObject() {
		return object;
	}

	/**
	 * @param object the object to set
	 */
	public void setObject(T object) {
		this.object = object;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	public String  toXml()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<").append(this.field).append(">").append(objectAsString())
		.append("</").append(this.field).append(">");
		return stringBuilder.toString();
	}
	
	public String  toXmlString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<").append(this.field).append(">").append(objectAsString())
		.append("</").append(this.field).append(">");
		return stringBuilder.toString();
	}
}
