package com.singlee.financial.cdtc.pac;
/**
 * 最短1位，最长2位数字
 * @author Administrator
 *
 */
public class Max2NumericText extends IntField {
	
	private static final long serialVersionUID = 1L;
	/**
	 * 最短1位，最长2位数字[0-9]{1,2}
	 */
	private static final String regStr="^[0-9]*$";
	
	public Max2NumericText(String field) {
		
		super(field);
		// TODO Auto-generated constructor stub
	}

	
	
	/* (non-Javadoc)
	 * @see com.singlee.client.cdtc.pac.IntField#getValue()
	 */
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(getLength()==1 || getLength() == 2)
		{
			if(objectAsString().matches(regStr))
			{
				return toXml();
			}
		}
		return "";
	}
}
