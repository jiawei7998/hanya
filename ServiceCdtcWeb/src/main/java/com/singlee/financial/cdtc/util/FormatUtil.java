package com.singlee.financial.cdtc.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class FormatUtil {
	
	/**
	 * 格式化
	 * @param bigDecimal
	 * @return
	 */
	public static String formatBigDecimal(BigDecimal bigDecimal) {
		DecimalFormat   f =new   DecimalFormat("###,###,###,##0.00");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	/**
	 * 格式化
	 * @param bigDecimal
	 * @return
	 */
	public static String formatBigDecimal4(BigDecimal bigDecimal){
		DecimalFormat   f =new   DecimalFormat("###,###,###,##0.0000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	/**
	 * 格式化
	 * @param bigDecimal
	 * @return
	 */
	public static String formatBigDecimal8(BigDecimal bigDecimal){
		DecimalFormat   f =new   DecimalFormat("###,###,###,##0.00000000");   
		
        f.setDecimalSeparatorAlwaysShown(true);  
        
        return f.format(bigDecimal);

	}
	
	/**
	 * 取消格式化
	 * */
	public static BigDecimal bigDecimalForString(String bigDecimal){
		return new BigDecimal(bigDecimal.replace(",", ""));
	}
	
}
