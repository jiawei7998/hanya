package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class AccountIdentificationAndName6 extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Nm",     Field.class);
		keyClassMap.put("Id",     Field.class);
		keyClassMap.put("BllgFee",Field.class);
	}
	public AccountIdentificationAndName6(String tag) {
		super(tag, tag, new String[] { "Nm", "Id", "BllgFee" });
	}

	public void set(Max35Text value) {
		setField(value);
	}

	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max35Text getMax35Text() throws FieldNotFound {
		Max35Text value = new Max35Text("");
		getField(value);

		return value;
	}

	public boolean isSet(Max35Text field) {
		return isSetField(field);
	}

	public boolean isSetMax35Text() {
		return isSetField("Nm");
	}

	public void set(Exact11Text value) {
		setField(value);
	}

	public Exact11Text get(Exact11Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Exact11Text getExact11Text() throws FieldNotFound {
		Exact11Text value = new Exact11Text("");
		getField(value);

		return value;
	}

	public boolean isSet(Exact11Text field) {
		return isSetField(field);
	}

	public boolean isSetExact11Text() {
		return isSetField("Id");
	}

	public void set(BillingFee group) {
		addGroup(group);
	}

	public boolean isSet(BillingFee field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetBillingFee() {
		return isSetField("BllgFee");
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
