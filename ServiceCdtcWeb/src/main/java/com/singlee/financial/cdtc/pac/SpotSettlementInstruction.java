/**
 * 
 */
package com.singlee.financial.cdtc.pac;


import java.io.File;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cdtc.cnbondpackage.BatchDataQueryRequestV01;
import com.singlee.financial.cdtc.cnbondpackage.BusinessInformationNoticeV01;
import com.singlee.financial.cdtc.cnbondpackage.HeartBeatMessageV01;
import com.singlee.financial.cdtc.cnbondpackage.SafekeepingAccountStatementReportV01;
import com.singlee.financial.cdtc.cnbondpackage.SettlementBusinessBatchQueryStatusReportV01;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.util.ExceptionUtil;


/**
 * @author Fang
 * 
 */
public class SpotSettlementInstruction extends Group {
	private  Logger logManager = LoggerFactory.getLogger(SpotSettlementInstruction.class);
	
	private static final long serialVersionUID = 1L;
	public HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	public SpotSettlementInstruction(String tag) {
		super(tag, tag, new String[] { "MsgHeader", "Document" });
	}
	public void unionHashMap(String type)
	{
		keyClassMap.clear();
		keyClassMap.put("MsgHeader", HeadMessage.class);
		keyClassMap.putAll(HeadMessage.keyClassMap);		
		if ("CSBS.001.001.01".equals(type)) {
			//6.3.1	通用结算指令请求(CSBS.001.001.01)
			keyClassMap.put("Document",CommonDistributionSettlementInstruction.class);
			keyClassMap.putAll(CommonDistributionSettlementInstruction.keyClassMap);
		}  else if ("CSBS.002.001.01".equals(type)) {
			//6.3.2	通用结算指令状态报告(CSBS.002.001.01)
			keyClassMap.putAll(CommonDistributionSettlementStatusReport.keyClassMap);
			keyClassMap.put("Document",CommonDistributionSettlementStatusReport.class);
		} else if("CSBS.003.001.01".equals(type)){
			//6.3.3	通用辅助指令请求(CSBS.003.001.01)
			keyClassMap.put("Document",CommonSubsidiaryInstructionRequesV01.class);
			keyClassMap.putAll(CommonSubsidiaryInstructionRequesV01.keyClassMap);
		}else if ("CSBS.004.001.01".equals(type)) {
			//6.3.4	通用辅助指令处理状态报告(CSBS.004.001.01)
			keyClassMap.put("Document",CommonSubsidiaryInstructionStatusReportV01.class);
			keyClassMap.putAll(CommonSubsidiaryInstructionStatusReportV01.keyClassMap);
		} else if ("CSBS.005.001.01".equals(type)) {
			//6.3.5	结算业务查询请求(CSBS.005.001.01)
			keyClassMap.put("Document",SettlementBusinessQueryRequest.class);
			keyClassMap.putAll(SettlementBusinessQueryRequest.keyClassMap);
		} else if ("CSBS.006.001.01".equals(type)) {
			//6.3.6	结算业务批量查询状态报告(CSBS.006.001.01)
			keyClassMap.put("Document",SettlementBusinessBatchQueryStatusReportV01.class);
			keyClassMap.putAll(SettlementBusinessBatchQueryStatusReportV01.keyClassMap);
		} else if ("CSBS.007.001.01".equals(type)) {
			//6.3.7	债券账户对账单查询请求(CSBS.007.001.01)
			keyClassMap.put("Document",SafekeepingAccountStatementQueryRequestV.class);
			keyClassMap.putAll(SafekeepingAccountStatementQueryRequestV.keyClassMap);
		} else if ("CSBS.008.001.01".equals(type)) {
			//6.3.8	债券账户对帐单报告(CSBS.008.001.01)
			keyClassMap.put("Document",SafekeepingAccountStatementReportV01.class);
			keyClassMap.putAll(SafekeepingAccountStatementReportV01.keyClassMap);
		} else if ("CSBS.009.001.01".equals(type)) {
			//6.3.9	系统异常通知(CSBS.009.001.01)
			keyClassMap.put("Document",SystemExceptionAdviceV01.class);
			keyClassMap.putAll(SystemExceptionAdviceV01.keyClassMap);
		} else if ("CSBS.010.001.01".equals(type)) {
			//6.3.10 业务消息通知(CSBS.010.001.01)
			keyClassMap.put("Document",BusinessInformationNoticeV01.class);
			keyClassMap.putAll(BusinessInformationNoticeV01.keyClassMap);
		} else if ("CSBS.011.001.01".equals(type)) {
			//6.3.11 批量数据查询请求(CSBS.011.001.01)
			keyClassMap.put("Document",BatchDataQueryRequestV01.class);
			keyClassMap.putAll(BatchDataQueryRequestV01.keyClassMap);
		} else if("CSBS.012.001.01".equals(type)) {
			//6.3.12 心跳消息(CSBS.012.001.01)
			keyClassMap.put("Document",HeartBeatMessageV01.class);
			keyClassMap.putAll(HeartBeatMessageV01.keyClassMap);
		}
	}


	public void set(HeadMessage group) {
		addGroup(group);
	}
	public void set(CommonDistributionSettlementInstruction group) {
		addGroup(group);
	}
	public void set(CommonSubsidiaryInstructionStatusReportV01 group) {
		addGroup(group);
	}
	public void set(CommonDistributionSettlementStatusReport group){
		addGroup(group);
	}
	public void set(CommonSubsidiaryInstructionRequesV01 group){
		addGroup(group);
	}
	public void set(SettlementBusinessBatchQueryStatusReportV01 group){
		addGroup(group);
	}
	public void set(SettlementBusinessQueryRequest group){
		addGroup(group);
	}
	public void set(SafekeepingAccountStatementQueryRequestV group){
		addGroup(group);
	}
	public void set(SafekeepingAccountStatementReportV01 group){
		addGroup(group);
	}
	public void set(SystemExceptionAdviceV01 group){
		addGroup(group);
	}
	public void set(BusinessInformationNoticeV01 group){
		addGroup(group);
	}
	public void set(HeartBeatMessageV01 group){
		addGroup(group);
	}
	public void set(BatchDataQueryRequestV01 group){
		addGroup(group);
	}
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	public boolean isHasTag(String tag) {
		String[] strings = getFieldOrder();
		for (String key : strings) {
			if (key.equals(tag)) {
				return true;
			}
		}
		return false;
	}

	/**
	 *读取文件路径获取类别头部类别信息
	 * @param file
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static String getMsgTpByXml(File file) throws Exception{
		String msgType = null;
		// 读取并解析XML文档 // SAXReader就是一个管道，用一个流的方式，把xml文件读出来 //
		org.jdom.input.SAXBuilder reader = new org.jdom.input.SAXBuilder(); // User.hbm.xml表示你要解析的xml文档
		org.jdom.Document document =  reader.build(file);
		// 下面的是通过解析xml字符串的
		org.jdom.Element root = document.getRootElement();
		List<org.jdom.Element> head  = root.getChildren("MsgHeader");
		Iterator<org.jdom.Element> it = head.iterator();
		while (it.hasNext()) {
			// 获得每一个student节点
			//List<org.jdom.Element> tp  = s.getChildren();
			org.jdom.Element stuElement = it.next();
			List<org.jdom.Element> msgTp = stuElement.getChildren("MsgDesc");
			Iterator<org.jdom.Element> msg = msgTp.iterator();
			while (msg.hasNext()) {
				org.jdom.Element msgElement = msg.next();
				msgType = msgElement.getChildText("MsgTp");
			}
		}
		return msgType;
	}
	
	
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * 
	 * @param element
	 * @param common
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	public void getElementList(Element element,
			SpotSettlementInstruction common) throws Exception,
			NoSuchMethodException {
		try{
		List<?> elements = element.elements();

		for (Iterator<?> iterator = elements.iterator(); iterator.hasNext();) {
			Element element2 = (Element)iterator.next();
			List<?> elements2 = element2.elements();
			if (elements2.size() >= 0) {
				logManager.info("" + String.format("%20s :", StringUtils.trimToEmpty(element2.getName())) + ""
						+ StringUtils.trimToEmpty(element2.getText()));
				if (common.isHasTag(StringUtils.trimToEmpty(element2.getName()))) {
					if (element2.elements().size() > 0) {
						logManager.info(String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName())) + "IS GROUP");
						common.addGroup(getGroups(element2, common));
					} else {
						logManager.info(String.format("%20s :", StringUtils
								.trimToEmpty(element2.getName())) + "IS FIELD");
						Class<?>[] parameterTypes = { String.class, Object.class };
						Constructor<?> constructor = 
								getKeyClassMap().get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
						Object[] parameters = {StringUtils.trimToEmpty(element2.getName()),
								StringUtils.trimToEmpty(element.getText()) };
						Object object = constructor.newInstance(parameters);
						common.setField(StringUtils.trimToEmpty(element2.getName()), (Field<?>) object);
					}
				}
			}
		}
		}catch (Exception e) {
			logManager.error(ExceptionUtil.getErrorInfoFromException(e));
		}
	}

	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP 二叉树原理
	 * 
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception反解析报文
	 */
	public Group getGroups(Element element,
			SpotSettlementInstruction common) throws NoSuchMethodException,
			Exception {
		// Group group = new Group(StringUtils.trimToEmpty(element.getName()),"");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = getKeyClassMap().get(
				StringUtils.trimToEmpty(element.getName())).getConstructor(
				parameterTypes2);
		Object[] parameters2 = { StringUtils.trimToEmpty(element.getName()), "" };
		Group group = (Group) constructor2.newInstance(parameters2);

		System.out.println("************************************");
		List<?> elements = element.elements();
		for (Iterator<?> iterator = elements.iterator(); iterator
				.hasNext();) {
			Element element2 = (Element) iterator.next();
			System.out.println(""+ String.format("%20s :", StringUtils.trimToEmpty(element2.getName())) + ""+ StringUtils.trimToEmpty(element2.getText()));
			if (element2.elements().size() > 0) {
				System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+ "IS GROUP");
				group.addGroup(getGroups(element2, common));
			} else {
				System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+ "IS FIELD");
				Class<?>[] parameterTypes = { String.class, Object.class };
				Constructor<?> constructor = getKeyClassMap()
						.get(StringUtils.trimToEmpty(element2.getName()))
						.getConstructor(parameterTypes);
				Object[] parameters = {
						StringUtils.trimToEmpty(element2.getName()),
						StringUtils.trimToEmpty(element2.getText()) };
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>) object);
			}
		}
		System.out.println("************************************");
		return group;
	}
	/**
	 * @return the keyClassMap
	 */
	public HashMap<String, Class<?>> getKeyClassMap() {
		return keyClassMap;
	}
	/**
	 * @param keyClassMap the keyClassMap to set
	 */
	public void setKeyClassMap(HashMap<String, Class<?>> keyClassMap) {
		this.keyClassMap = keyClassMap;
	}

}
