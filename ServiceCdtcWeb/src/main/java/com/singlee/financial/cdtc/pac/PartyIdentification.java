/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class PartyIdentification extends Group{

	private static final long serialVersionUID = 1L;
	

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Nm", Field.class);
	}
	
	public PartyIdentification(String tag){
		super(tag,tag,new String[]{"Nm"});
	}
	public PartyIdentification(String tag,String delim){
		super(tag,tag,new String[]{"Nm"});
	}
	public void set(Max35Text field){
		setField(field);
	}

}
