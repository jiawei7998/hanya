package com.singlee.financial.cdtc.pac;

public class StringField extends Field<String>
{
	  private static final long serialVersionUID = 1L;
	  
	  private static final String regStr="[^\\x00-\\xff]";

	  public StringField(String field)
	  {
	    super(field, "");
	  }

	  public StringField(String field, String data)
	  {
	    super(field, data);
	  }

	  public void setValue(String value)
	  {
	    setObject(value);
	  }

	  /* (non-Javadoc)
	 * @see com.singlee.client.cdtc.pac.Field#getLength()
	 */
	@Override
	int getLength() {
		// TODO Auto-generated method stub
		return getValue().replaceAll(regStr,"aa").length();
	}

	public String getValue() {
	    return (String)getObject();
	  }

	  public boolean valueEquals(String value) {
	    return getValue().equals(value);
	  }
}
