/**
 * 
 */
package com.singlee.financial.cdtc.pac;

/**
 * @author Fang
 *
 */
public class CNNum19 extends EsbNum19OnlyField {

	public CNNum19(String field) {
		super(field);
	}
	
	public CNNum19(String field,int cifNo) {
		super(field,cifNo);
	}
	
	public CNNum19(String field,String cifNo) {
		super(field,cifNo);
	}
	private static final long serialVersionUID = 1L;
	public static void main(String[] args) {
		CNNum19 isTime = new CNNum19("cifNo",13900);
		System.out.println(isTime.toXml());
	}
}
