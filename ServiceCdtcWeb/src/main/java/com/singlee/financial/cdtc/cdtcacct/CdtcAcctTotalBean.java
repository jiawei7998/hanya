package com.singlee.financial.cdtc.cdtcacct;

import java.io.Serializable;
import java.math.BigDecimal;

public class CdtcAcctTotalBean implements Serializable{

	private static final long serialVersionUID = 1L;
	/**
	 * 债券简称
	 */
	private String secidNm;
	/**
	 * 债券代码
	 */
	private String secid;
	/**
	 * 可用科目 万元
	 */
	private BigDecimal ableUseAmt;
	/**
	 * 待付科目 万元
	 */
	private BigDecimal waitPayAmt;
	/**
	 * 质押科目 万元
	 */
	private BigDecimal repoZYAmt;
	/**
	 * 待购回科目 万元
	 */
	private BigDecimal repoRevAmt;
	/**
	 * 冻结科目
	 */
	private BigDecimal frozenAmt;
	/**
	 * 债券余额 万元
	 */
	private BigDecimal totalAmt;
	
	public CdtcAcctTotalBean(String secidNm, String secid,
			BigDecimal ableUseAmt, BigDecimal waitPayAmt, BigDecimal repoZYAmt,
			BigDecimal repoRevAmt, BigDecimal frozenAmt, BigDecimal totalAmt) {
		super();
		this.secidNm = secidNm;
		this.secid = secid;
		this.ableUseAmt = ableUseAmt;
		this.waitPayAmt = waitPayAmt;
		this.repoZYAmt = repoZYAmt;
		this.repoRevAmt = repoRevAmt;
		this.frozenAmt = frozenAmt;
		this.totalAmt = totalAmt;
	}
	public CdtcAcctTotalBean() {
		super();
	}
	/**
	 * @return the secidNm
	 */
	public String getSecidNm() {
		return secidNm;
	}
	/**
	 * @param secidNm the secidNm to set
	 */
	public void setSecidNm(String secidNm) {
		this.secidNm = secidNm;
	}
	/**
	 * @return the secid
	 */
	public String getSecid() {
		return secid;
	}
	/**
	 * @param secid the secid to set
	 */
	public void setSecid(String secid) {
		this.secid = secid;
	}
	/**
	 * @return the ableUseAmt
	 */
	public BigDecimal getAbleUseAmt() {
		return ableUseAmt;
	}
	/**
	 * @param ableUseAmt the ableUseAmt to set
	 */
	public void setAbleUseAmt(BigDecimal ableUseAmt) {
		this.ableUseAmt = ableUseAmt;
	}
	/**
	 * @return the waitPayAmt
	 */
	public BigDecimal getWaitPayAmt() {
		return waitPayAmt;
	}
	/**
	 * @param waitPayAmt the waitPayAmt to set
	 */
	public void setWaitPayAmt(BigDecimal waitPayAmt) {
		this.waitPayAmt = waitPayAmt;
	}
	/**
	 * @return the repoZYAmt
	 */
	public BigDecimal getRepoZYAmt() {
		return repoZYAmt;
	}
	/**
	 * @param repoZYAmt the repoZYAmt to set
	 */
	public void setRepoZYAmt(BigDecimal repoZYAmt) {
		this.repoZYAmt = repoZYAmt;
	}
	/**
	 * @return the repoRevAmt
	 */
	public BigDecimal getRepoRevAmt() {
		return repoRevAmt;
	}
	/**
	 * @param repoRevAmt the repoRevAmt to set
	 */
	public void setRepoRevAmt(BigDecimal repoRevAmt) {
		this.repoRevAmt = repoRevAmt;
	}
	/**
	 * @return the frozenAmt
	 */
	public BigDecimal getFrozenAmt() {
		return frozenAmt;
	}
	/**
	 * @param frozenAmt the frozenAmt to set
	 */
	public void setFrozenAmt(BigDecimal frozenAmt) {
		this.frozenAmt = frozenAmt;
	}
	/**
	 * @return the totalAmt
	 */
	public BigDecimal getTotalAmt() {
		return totalAmt;
	}
	/**
	 * @param totalAmt the totalAmt to set
	 */
	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}

}
