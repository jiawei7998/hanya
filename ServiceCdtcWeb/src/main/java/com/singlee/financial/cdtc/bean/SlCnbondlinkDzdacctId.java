package com.singlee.financial.cdtc.bean;
// Generated 2012-2-6 15:18:40 by Hibernate Tools 3.4.0.CR1



/**
 * SlCnbondlinkDzdacctId generated by hbm2java
 */
public class SlCnbondlinkDzdacctId  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sn;
     private String id;

    public SlCnbondlinkDzdacctId() {
    }

    public SlCnbondlinkDzdacctId(String sn, String id) {
       this.sn = sn;
       this.id = id;
    }
   
    public String getSn() {
        return this.sn;
    }
    
    public void setSn(String sn) {
        this.sn = sn;
    }
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    @Override
   public boolean equals(Object other) {
         if ( (this == other ) ) {return true;}
		 if ( (other == null ) ) {return false;}
		 if ( !(other instanceof SlCnbondlinkDzdacctId) ) { return false;}
		 SlCnbondlinkDzdacctId castOther = ( SlCnbondlinkDzdacctId ) other; 
		 return ( (this.getSn()==castOther.getSn()) || ( this.getSn()!=null && castOther.getSn()!=null && this.getSn().equals(castOther.getSn()) ) )
 && ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
    @Override
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getSn() == null ? 0 : this.getSn().hashCode() );
         result = 37 * result + ( getId() == null ? 0 : this.getId().hashCode() );
         return result;
   }   


}


