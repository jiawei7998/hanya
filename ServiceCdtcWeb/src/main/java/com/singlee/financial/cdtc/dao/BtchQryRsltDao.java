package com.singlee.financial.cdtc.dao;

import java.util.List;

import com.singlee.financial.cdtc.bean.BtchQryRslt;


public interface BtchQryRsltDao {
	/**
	 * 批量或单个记录保存
	 * @param btchList
	 * @return
	 * @throws Exception
	 */
	public boolean insertBtchQryRsltSingleeOrBatchForCondition(List<BtchQryRslt> btchList) throws Exception;
	/**
	 * 根据清算日期查询出所有的记录
	 * @param settDate
	 * @return
	 * @throws Exception
	 */
	public List<BtchQryRslt> queryBtchQryRsltListBySettDate(String settDate) throws Exception;
	/**
	 * 是否存在
	 * @param btchQryRslt
	 * @return
	 * @throws Exception
	 */
	public boolean isExistBtchQryRslt(BtchQryRslt btchQryRslt) throws Exception;
	
	
	/**
	 * 根据清算日期查询出所有的记录
	 * @param settDate
	 * @return
	 * @throws Exception
	 */
	public List<BtchQryRslt> queryBtchQryRsltListBySettDateForVerify(String settDate) throws Exception;
	/**
	 * 批量更新状态
	 * @param btchList
	 * @return
	 * @throws Exception
	 */
	public boolean updateBtchQryRsltSingleeOrBatchForCondition(List<BtchQryRslt> btchList) throws Exception;
	
	/**
	 * 批量删除
	 * @param btchList
	 * @return
	 * @throws Exception
	 */
	public boolean deleteBtchQryRsltSingleeOrBatchForCondition(List<BtchQryRslt> btchList) throws Exception;
	
	/**
	 * 根据InstrId查询单笔信息
	 * @param instrId
	 * @return
	 * @throws Exception
	 */
	public BtchQryRslt queryBtchQryRsltListBySettDateForVerifyByInstrId(String instrId) throws Exception;
}
