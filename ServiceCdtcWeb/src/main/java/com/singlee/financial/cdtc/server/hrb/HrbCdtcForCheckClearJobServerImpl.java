package com.singlee.financial.cdtc.server.hrb;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.singlee.financial.bean.AbstractSlIntfcCnbondPkgId;
import com.singlee.financial.cdtc.bean.CSBSTyjszlztbgBean;
import com.singlee.financial.cdtc.bean.GentBean;
import com.singlee.financial.cdtc.bean.RepoSecurity;
import com.singlee.financial.cdtc.bean.SlCnbondlinkSecurDeal;
import com.singlee.financial.cdtc.cnbondpackage.CdtcBankAccount;
import com.singlee.financial.cdtc.dao.CnbondManagerDao;
import com.singlee.financial.cdtc.dao.GentDao;
import com.singlee.financial.cdtc.dao.TyjszlztbgDao;
import com.singlee.financial.cdtc.hrb.HrbCdtcForCheckClearJobServer;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.BondSettlementType3Code;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.CheckStatusEnum;
import com.singlee.financial.cdtc.pac.CheckStatusEnum.Type;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementInstruction;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.InstructionStatusCode;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.OperatorStatusCode;
import com.singlee.financial.cdtc.pac.Priority4Code;
import com.singlee.financial.cdtc.pac.RequestTypeCode;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.pac.TransactionMsg;
import com.singlee.financial.cdtc.pac.TransactionOprtrFlg;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

/**
 * 核对CDTC待确认报文IFS_CDTC_CNBOND_PKG与系统债券
 *  产生确认报文
 * 
 * @author singlee
 *
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class HrbCdtcForCheckClearJobServerImpl implements HrbCdtcForCheckClearJobServer {

	private Logger logger = LoggerFactory.getLogger("AUTOCHECK");

	private Logger logger_1 = LoggerFactory.getLogger(HrbCdtcForCheckClearJobServerImpl.class);

	// 中债直连接口报文自动匹配任务开始运行时间
	@Value("#{cdtcProperties['cdtc.beginWorkTime']}")
	private String beginWorkTime;
	// 中债直连接口报文自动匹配任务开始运行时间
	@Value("#{cdtcProperties['cdtc.endWorkTime']}")
	private String endWorkTime;
	
	@Autowired
	private CnbondManagerDao cnbondManagerDao;
	@Autowired
	private TyjszlztbgDao tyjszlztbgDao;
	@Autowired
	private GentDao gentDao;

	@Override
	public RetBean doQuartzJobCheck(Map<String, Object> params, Function<SlCnbondlinkSecurDeal, List<RepoSecurity>> getRepoList) throws Exception {
		String postdate=(String) params.get("postDate");
		Boolean isHolyDay=(Boolean) params.get("isHolyDay");
		logger.info("start CdtcForCheckClearJobServer doQuartzJobCheck---------------------");
		// 判断是否工做时间
		RetBean ret = validCdtcWorkTime(isHolyDay);
		if (RetStatusEnum.F.equals(ret.getRetStatus())) {
			logger.info("当前是节假期或非工作时间,不进行业务处理!");
			Thread.sleep(2000);
			return ret;
		}

		/**
		 * 设定未找到结算指令状态报文的计数器
		 */
		int csbsCountNotFoundIndex = 0;
		/**
		 * 在IFS_CDTC_CNBOND_PKG获取当日的所有通用指令报文
		 */
		List<AbstractSlIntfcCnbondPkgId> csList2 = new ArrayList<AbstractSlIntfcCnbondPkgId>();
		List<CSBSTyjszlztbgBean> csList = new ArrayList<CSBSTyjszlztbgBean>();
		try {
			csList2 = this.cnbondManagerDao.queryAllCSBSBeanFromDbByDateType(postdate,
					CommonDistributionSettlementStatusReport.type);
			logger.info("通用指令结算状态报文获取");
			logger.info("正在解析组装......");
			logger.info("csList2.size()数量:" + csList2.size());
			logger.info(CheckStatusEnum.Type.SL09.toString());
			CSBSTyjszlztbgBean csbsTyjszlztbgBean = null;
			CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
			SpotSettlementInstruction spotSettlementInstruction = null;
			for (AbstractSlIntfcCnbondPkgId abstractSlIntfcCnbondPkgId : csList2) {
				csbsTyjszlztbgBean = new CSBSTyjszlztbgBean();
				csbsTyjszlztbgBean.setMsgsn(abstractSlIntfcCnbondPkgId.getMsgsn());
				csbsTyjszlztbgBean.setMsgfilename(abstractSlIntfcCnbondPkgId.getMsgfilename());
				csbsTyjszlztbgBean.setMsgtype(abstractSlIntfcCnbondPkgId.getMsgtype());
				csbsTyjszlztbgBean.setMsgtime(abstractSlIntfcCnbondPkgId.getMsgtime());
				SAXReader reader = new SAXReader();
				/**
				 * 读取请求报文XML 反解析报文
				 */
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(
						(XmlFormat.format(abstractSlIntfcCnbondPkgId.getMsgxml()).getBytes("UTF-8"))));
				spotSettlementInstruction = new SpotSettlementInstruction("Root");
				spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				if (spotSettlementInstruction.hasGroup("Document")) {
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for (Group group : groupsList) {
						// 保存xml文件
						StringBuffer xmlBuffer = new StringBuffer();
						group.toXmlForParent(xmlBuffer);
						logger.info(xmlBuffer.toString());
						commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport(
								"Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat
								.format("<Document>" + xmlBuffer.toString() + "</Document>").getBytes("UTF-8"))));
						CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(),
								commonDistributionSettlementStatusReport);
						csbsTyjszlztbgBean.setCsbs002(commonDistributionSettlementStatusReport);
						logger.info(
								abstractSlIntfcCnbondPkgId.getMsgsn() + "的csbs002为：" + csbsTyjszlztbgBean.getCsbs002());
						csList.add(csbsTyjszlztbgBean);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(2000);
		logger.info("---------csList大小" + csList.size());

		/**
		 * SL_CDTC_CNBONDLINK_SECURDEAL查询当日所有的交易信息
		 */
		List<SlCnbondlinkSecurDeal> securDealList = this.tyjszlztbgDao
				.queryAllSlCnbondlinkSecurDealForProcessDate(postdate);
		logger.info("securDealList.size() ======== " + securDealList.size());
		if (securDealList.size() > 0) {
			logger.info(securDealList + "");
			Thread.sleep(2000);
		}
		for (SlCnbondlinkSecurDeal slCnbondlinkSecurDeal : securDealList) {
			logger.info(slCnbondlinkSecurDeal.getCfetssn() == null ? "资金流水:" + slCnbondlinkSecurDeal.getId().getDealno()
					: slCnbondlinkSecurDeal.getCfetssn());
			logger.info(formatBigDecimal(slCnbondlinkSecurDeal.getMatprocdamt()));
			logger.info(slCnbondlinkSecurDeal.getSetttype_b());
			if ('\0' != slCnbondlinkSecurDeal.getDealstatus() && slCnbondlinkSecurDeal.getDealstatus() == 'R') { // 资金系统冲销交易
				logger.info("核对结果：" + CheckStatusEnum.Type.SL06.toString());
				Thread.sleep(100);
				continue;
			}
			if (null == slCnbondlinkSecurDeal.getCfetssn() || "".equalsIgnoreCase(slCnbondlinkSecurDeal.getCfetssn())) {
				logger.info("核对结果：" + CheckStatusEnum.Type.SL20.toString());
				Thread.sleep(100);
				continue;
			}
			// 核对相符
			if (null != slCnbondlinkSecurDeal.getChecksts()
					&& !"".equals(StringUtils.trimToEmpty(slCnbondlinkSecurDeal.getChecksts()))
					&& slCnbondlinkSecurDeal.getChecksts().equalsIgnoreCase(CheckStatusEnum.Type.SL02.getType())) {
				logger.info("核对结果：" + CheckStatusEnum.Type.SL02.toString());
				Thread.sleep(100);
				/**
				 * 核对完成进行确认处理 发现核对是完成的 1：检查是否已经发送过确认结算报文？ 2：发送结算报文 3：同步等待回执 更新 REQUESTSN
				 * VARCHAR(15) NULL,--请求编号 确认指令ID CNBONDRETSTS CHAR(4) NULL,--发送返回的状态
				 * CNBONDRETTIME DATE NULL,--返回时间 4：失败的话，先要查询一次中债状态 再判断是否要再次结算
				 */

				if (null != slCnbondlinkSecurDeal.getRequestsn()
						&& !"".equalsIgnoreCase(slCnbondlinkSecurDeal.getRequestsn())) {
					logger.info("核对结果：" + CheckStatusEnum.Type.SL12.toString());
					Thread.sleep(100);
				} else {
					try {
						/**
						 * 生成结算报文 根据通用结算状态报告报文，根据名称读取报文数据IFS_CDTC_CNBOND_PKG
						 */
						AbstractSlIntfcCnbondPkgId abstractSlIntfcCnbondPkgId = cnbondManagerDao
								.getSlintfccnbondpkgByMsgtype(slCnbondlinkSecurDeal.getXmlmatchid());
						String NULL = "";
						/**********************************************************************************************************************/
						SAXReader saxReader = new SAXReader();
						/**
						 * 读取请求报文XML 反解析报文
						 */
						org.dom4j.Document xmlDoc = saxReader.read(new ByteArrayInputStream(
								(XmlFormat.format(abstractSlIntfcCnbondPkgId.getMsgxml()).getBytes("UTF-8"))));
						String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp")
								.getText();
						if (null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {
							String instrSts = xmlDoc.getRootElement().element("Document").element("StsFlg")
									.element("InstrStsInf").element("InstrSts").getText();
							if (null == instrSts || !"IS03".equalsIgnoreCase(instrSts)) {
								NULL = "NULL";}
							SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
							spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
							spotSettlementInstruction.getElementList(xmlDoc.getRootElement(),
									spotSettlementInstruction);
							CommonDistributionSettlementInstruction document = new CommonDistributionSettlementInstruction(
									"Document");
							Field<?> instrOrgn = null;
							Field<?> givAcctId = null;
							Field<?> fedealno = null;
							Field<?> fedealno2 = null;
							String instrId = "";
							if (spotSettlementInstruction.hasGroup("Document")) {
								List<Group> groups = spotSettlementInstruction.getGroups("Document");
								for (Group group : groups) {
									if (group.hasGroup("SttlmDtl")) {
										Group group2 = group.getGroups("SttlmDtl").get(0);
										fedealno = group2.getfieldsForFieldMap().get("TxId");
										instrOrgn = group2.getfieldsForFieldMap().get("InstrOrgn");
										instrId = group2.getfieldsForFieldMap().containsKey("InstrId")
												? group2.getfieldsForFieldMap().get("InstrId").getObject().toString()
												: "INSTRID";
										group2.removeGroup("Oprtr");
										group2.removeGroup("Chckr");
										document.addGroup(group2);
										givAcctId = group2.hasGroup("GivAcct") && group2.getGroups("GivAcct").get(0)
												.getfieldsForFieldMap().containsKey("Id")
														? group2.getGroups("GivAcct").get(0).getfieldsForFieldMap()
																.get("Id")
														: null;
									}
									if (group.hasGroup("Msg")) {
										TransactionMsg transactionMsg = new TransactionMsg("Msg");
										transactionMsg.set(new ISODateTime("CreDtTm", new Date()));
										fedealno2 = group.getGroups("Msg").get(0).getfieldsForFieldMap()
												.get("TxFlowId");
										Priority4Code priority4Code = new Priority4Code("Prty");
										priority4Code.setValue(Priority4Code.Type.PT02.getType());
										transactionMsg.set(priority4Code);
										Max35Text max35Text = new Max35Text("TxFlowId");
										max35Text.setValue(fedealno == null ? fedealno2.getObject().toString()
												: fedealno.getObject().toString());
										transactionMsg.set(max35Text);
										document.addGroup(transactionMsg);
									}
								}
								TransactionOprtrFlg transactionOprtrFlg = new TransactionOprtrFlg("OprtrFlg");

								RequestTypeCode requestTypeCode = new RequestTypeCode("ReqTp");
								String bizTp = xmlDoc.getRootElement().element("Document").element("SttlmDtl")
										.element("BizTp").getText();
								/**
								 * BT00：普通分销 BT01：现券 BT02：质押式回购 BT03：买断式回购 BT04：债券远期 BT05：债券借贷 BJ0100 普通分销
								 * BJ0101 现券 BJ0102 质押式回购 BJ0103 买断式回购 BJ0104 债券远期 BJ0105 债券借贷 BJ0106 质押券置换
								 */
								if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT00.getType())) {
									requestTypeCode.setValue(RequestTypeCode.Type.BJ0100.getType());}
								if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT01.getType())) {
									requestTypeCode.setValue(RequestTypeCode.Type.BJ0101.getType());}
								if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT02.getType())) {
									requestTypeCode.setValue(RequestTypeCode.Type.BJ0102.getType());}
								if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT03.getType())) {
									requestTypeCode.setValue(RequestTypeCode.Type.BJ0103.getType());}
								if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT04.getType())) {
									requestTypeCode.setValue(RequestTypeCode.Type.BJ0104.getType());}
								if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT05.getType())) {
									requestTypeCode.setValue(RequestTypeCode.Type.BJ0105.getType());}
								OperatorStatusCode operatorStatusCode = new OperatorStatusCode("OprtrSts");
								/**
								 * 03是第三方交易 那么确认分 收券和付券
								 */
								if (instrOrgn.getObject().toString().equalsIgnoreCase(InstructionOriginCode.Type.IO03.getType()) && null != givAcctId) { 
									if (givAcctId.getObject().toString().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)) {
										operatorStatusCode.setValue(OperatorStatusCode.Type.OS05.getType());}// 付券方确认
									else {
										operatorStatusCode.setValue(OperatorStatusCode.Type.OS06.getType());}// 收券方确认
								}
								/**
								 * 成员客户端的只需要付券方确认即可
								 */
								if (instrOrgn.getObject().toString().equalsIgnoreCase(
										InstructionOriginCode.Type.IO01.getType()) && null != givAcctId) {
									if (!givAcctId.getObject().toString()
											.equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)) {
										operatorStatusCode.setValue(OperatorStatusCode.Type.OS01.getType());}
									else {
										NULL = "NULL";
									}
								}
								transactionOprtrFlg.set(operatorStatusCode);
								transactionOprtrFlg.set(requestTypeCode);
								document.addGroup(transactionOprtrFlg);
							}
							StringBuffer csbs001 = new StringBuffer();
							document.toXmlForSuperParent(csbs001);
							/**
							 * 进行存储 将通用结算报文存入数据库
							 */
							AbstractSlIntfcCnbondPkgId abstractSlIntfcCnbondPkgIdCSBS001 = new AbstractSlIntfcCnbondPkgId();
							abstractSlIntfcCnbondPkgIdCSBS001
									.setMsgsn(instrId + "SEND" + DateUtil.convertDateToYYYYMMDDHHMMSS(new Date()));
							abstractSlIntfcCnbondPkgIdCSBS001.setMsgfilename(instrId + ".XML");
							abstractSlIntfcCnbondPkgIdCSBS001.setMsgtype(CommonDistributionSettlementInstruction.type);
							abstractSlIntfcCnbondPkgIdCSBS001.setMsgxml(
									"NULL".equalsIgnoreCase(NULL) ? NULL : XmlFormat.format(csbs001.toString()));
							abstractSlIntfcCnbondPkgIdCSBS001
									.setMsgfedealno(fedealno == null ? "NULL" : fedealno.getObject().toString());
							abstractSlIntfcCnbondPkgIdCSBS001
									.setMsgopdt(new SimpleDateFormat("yyyy-MM-dd").parse(postdate));
							slCnbondlinkSecurDeal.setRequestsn(abstractSlIntfcCnbondPkgIdCSBS001.getMsgsn());
							try {
								/**
								 * 添加报文信息到IFS_CDTC_CNBOND_PKG表，并将通用结算指令的ID更新到IFS_CDTC_CNBONDLINK_SECURDEAL表中
								 */

								if (this.cnbondManagerDao.addSlintfccnbondpkg(abstractSlIntfcCnbondPkgIdCSBS001)
										&& this.tyjszlztbgDao.updateTyjszlztbgForCreateId(slCnbondlinkSecurDeal)) {
									logger.info("核对结果：" + CheckStatusEnum.Type.SL12.toString());
									Thread.sleep(1000);
								} else {
									logger.info("核对结果：" + CheckStatusEnum.Type.SL13.toString());
									Thread.sleep(1000);
								}
							} catch (Exception e) {
								logger.info("核对结果：" + CheckStatusEnum.Type.SL13.toString());
								Thread.sleep(1000);
								logger.error(e.getMessage());
								e.printStackTrace();
							}
						}
						/**
						 * 发送 等待 检查
						 */
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				continue;
			}
			// 核对不相符
			if (null != slCnbondlinkSecurDeal.getChecksts()
					&& !"".equals(StringUtils.trimToEmpty(slCnbondlinkSecurDeal.getChecksts()))
					&& !slCnbondlinkSecurDeal.getChecksts().equalsIgnoreCase(CheckStatusEnum.Type.SL02.getType())) {
				logger.info("核对结果：" + CheckStatusEnum.Type.SL03.toString());
				continue;
			}
			// 未进行核对
			if (null == slCnbondlinkSecurDeal.getChecksts()
					|| "".equals(StringUtils.trimToEmpty(slCnbondlinkSecurDeal.getChecksts()))) {
				Thread.sleep(100);
				// 根据流水单找符合该笔流水的CSBS002.001.01 通用结算指令状态报告 LIST
				List<CSBSTyjszlztbgBean> lsList2 = HrbCdtcForCheckClearJobServerImpl
						.queryFromCommonDistributionSettlementStatusReportByTxId(slCnbondlinkSecurDeal.getCfetssn(),
								csList);
				logger.info("lsList2.size() ：" + lsList2.size());
				if (lsList2.size() > 0) {
					HashMap<String, CSBSTyjszlztbgBean> resultHashMap = HrbCdtcForCheckClearJobServerImpl
							.queryAllStatusForCommonDistributionSettlementStatusReport(lsList2);
					logger.info("resultHashMap.size() ：" + resultHashMap.size());
					if (resultHashMap.size() > 0) {
						StringBuffer statusBuffer = new StringBuffer();
						Iterator<String> statusIterator = resultHashMap.keySet().iterator();
						logger.info("循环查找所有符合该笔流水的CSBS002.001.01的状态列表");
						while (statusIterator.hasNext()) {
							String key = statusIterator.next();
							// 循环查找所有交易的状态
							logger.info(key + ":" + InstructionStatusCode.Type.getName(key));
						}
						Thread.sleep(100);
						InstructionStatusCode.Type type = HrbCdtcForCheckClearJobServerImpl
								.matchingCommonDistributionSettlementStatusReportAndDeals(resultHashMap);
						logger.info("type :" + type);
						List<Type> typeList = processStatusForCommonDistributionSettlementStatusReport(type,
								resultHashMap, slCnbondlinkSecurDeal,getRepoList);
						logger.info("typeList :" + typeList);
						statusBuffer = new StringBuffer();
						for (CheckStatusEnum.Type type2 : typeList) {
							logger.info("type2 :" + type2.toString());
						}
						logger.info("statusBuffer :" + statusBuffer.toString());
					} else {
						logger.info(CheckStatusEnum.Type.SL05.toString());
						continue;
					}
				} else {
					logger.info(CheckStatusEnum.Type.SL04.toString());
					csbsCountNotFoundIndex++;
					continue;
				}
			}
		} // end for
		/*****************************************************************************************************************************/
		/**
		 * 当计数器 csbsCountNotFoundIndex > 0 时表示未存在通用结算指令 那么需要发起请求
		 */
		if (csbsCountNotFoundIndex > 0) {
			/**
			 * 1：向中债CDTC发送批量查询当日结算业务 2：同步等待接收回执 失败轮询3次 3：解析回执XML 回填交易状态（更新状态，更新对应的查询到的报文ID等）
			 */

		}
		return new RetBean(RetStatusEnum.S, "success", " 结算指令核对完成！");
	}
	
	/**
	 * 根据流水单找寻符合该笔流水的CSBS002.001.01 通用结算指令状态报告 LIST
	 * 
	 * @param execId
	 * @param commonDistributionSettlementStatusReportList
	 * @return
	 * @throws FieldNotFound
	 */
	public static List<CSBSTyjszlztbgBean> queryFromCommonDistributionSettlementStatusReportByTxId(String execId,
			List<CSBSTyjszlztbgBean> commonDistributionSettlementStatusReportList) throws Exception {

		List<CSBSTyjszlztbgBean> resultCommonDistributionSettlementStatusReport = new ArrayList<CSBSTyjszlztbgBean>();
		for (CSBSTyjszlztbgBean csbsTyjszlztbgBean : commonDistributionSettlementStatusReportList) {
			CommonDistributionSettlementStatusReport csbs002 = csbsTyjszlztbgBean.getCsbs002();
			if (csbs002.hasGroup("SttlmDtl")) {
				List<Group> groups = csbs002.getGroups("SttlmDtl");
				for (Group group : groups) {
					if (group.isSetField("TxId")) {
						String fedealno = group.getfieldsForFieldMap().containsKey("TxId")
								? group.getfieldsForFieldMap().get("TxId").getObject().toString()
								: "NULLL";

						if (StringUtils.equalsIgnoreCase(StringUtils.trimToEmpty(fedealno),
								StringUtils.trimToEmpty(execId))) {
							resultCommonDistributionSettlementStatusReport.add(csbsTyjszlztbgBean);
						}
					}
				}
			}

		}
		return resultCommonDistributionSettlementStatusReport;
	}

	/**
	 * 将符合的所有CSBS002.001.01中筛选出全部的状态列表
	 * 
	 * @param commonDistributionSettlementStatusReportList
	 * @return
	 */
	public static HashMap<String, CSBSTyjszlztbgBean> queryAllStatusForCommonDistributionSettlementStatusReport(
			List<CSBSTyjszlztbgBean> commonDistributionSettlementStatusReportList) {
		HashMap<String, CSBSTyjszlztbgBean> resultHashMap = new HashMap<String, CSBSTyjszlztbgBean>();
		for (CSBSTyjszlztbgBean csbsTyjszlztbgBean : commonDistributionSettlementStatusReportList) {
			CommonDistributionSettlementStatusReport csbs002 = csbsTyjszlztbgBean.getCsbs002();
			if (csbs002.hasGroup("StsFlg")) {
				List<Group> stsFlgGroup = csbs002.getGroups("StsFlg");
				for (Group group : stsFlgGroup) {
					if (group.hasGroup("InstrStsInf")) {
						List<Group> instrStsInfGroups = group.getGroups("InstrStsInf");
						for (Group instrStsInf : instrStsInfGroups) {
							if (instrStsInf.isSetField("InstrSts")) {
								Field<?> fedealno = instrStsInf.getfieldsForFieldMap().get("InstrSts");
								resultHashMap.put(StringUtils.trimToEmpty(fedealno.getObject().toString()),
										csbsTyjszlztbgBean);
							}
						}
					}
				}
			}
		}
		return resultHashMap;
	}

	/**
	 * 找寻 HASHMAP中全部通用结算指令的最终状态是什么
	 * 
	 * @param commonDistributionSettlementStatusReportHashMap
	 * @return
	 */
	public static InstructionStatusCode.Type matchingCommonDistributionSettlementStatusReportAndDeals(
			HashMap<String, CSBSTyjszlztbgBean> commonDistributionSettlementStatusReportHashMap) {
		if (commonDistributionSettlementStatusReportHashMap.containsKey(InstructionStatusCode.Type.IS00.getType())) {
			/**
			 * IS00：成功 对于核对相符的交易发送中债返回成功的报文更新交易状态为已完成,避免核对相符报文重复发送 F
			 * “已完成”：交易双方均已确认交易，该笔交易已完成交割；
			 */
			return InstructionStatusCode.Type.IS00;
		} else if (commonDistributionSettlementStatusReportHashMap
				.containsKey(InstructionStatusCode.Type.IS07.getType())) {
			/**
			 * IS07 ：作废,R 已撤消
			 */
			return InstructionStatusCode.Type.IS07;
		} else if (commonDistributionSettlementStatusReportHashMap
				.containsKey(InstructionStatusCode.Type.IS08.getType())) {
			/**
			 * IS08 ：撤销 R 已撤消
			 */
			return InstructionStatusCode.Type.IS08;
		} else if (commonDistributionSettlementStatusReportHashMap
				.containsKey(InstructionStatusCode.Type.IS04.getType())) {
			/**
			 * IS04 ：已确认 F “已完成”：交易双方均已确认交易，该笔交易已完成交割；
			 */
			return InstructionStatusCode.Type.IS04;
		} else if (commonDistributionSettlementStatusReportHashMap
				.containsKey(InstructionStatusCode.Type.IS03.getType())) {
			/**
			 * IS03：待确认
			 */
			return InstructionStatusCode.Type.IS03;
		}
		return null;
	}

	public List<CheckStatusEnum.Type> processStatusForCommonDistributionSettlementStatusReport(
			InstructionStatusCode.Type type,
			HashMap<String, CSBSTyjszlztbgBean> commonDistributionSettlementStatusReportHashMap,
			SlCnbondlinkSecurDeal securDeal, Function<SlCnbondlinkSecurDeal, List<RepoSecurity>> getRepoList) throws Exception {
		List<CheckStatusEnum.Type> types = new ArrayList<CheckStatusEnum.Type>();
		/**
		 * 第一步 UPDATE SL_CNBONDLINK_SECURDEAL 合同状态
		 */
		CommonDistributionSettlementStatusReport report = commonDistributionSettlementStatusReportHashMap
				.get(type.getType()).getCsbs002();
		if (report.hasGroup("SttlmDtl")) {
			List<Group> groups = report.getGroups("SttlmDtl");
			for (Group group : groups) {
				if (group.isSetField("InstrId")) {
					Field<?> fedealno = group.getfieldsForFieldMap().get("InstrId");
					logger.info("InstrId :" + fedealno.getObject().toString());
					securDeal.setInstrid(fedealno.getObject().toString());
				}
			}
		}
		if (report.hasGroup("Msg")) {
			List<Group> groups = report.getGroups("Msg");
			for (Group group : groups) {
				if (group.isSetField("TxFlowId")) {
					Field<?> fedealno = group.getfieldsForFieldMap().get("TxFlowId");
					logger.info("TxFlowId :" + fedealno.getObject().toString());
					securDeal.setTxflowid(fedealno.getObject().toString());
				}
			}
		}
		logger.info("type.getType()).getMsgsn().trim()"
				+ commonDistributionSettlementStatusReportHashMap.get(type.getType()).getMsgsn().trim());
		securDeal.setXmlmatchid(commonDistributionSettlementStatusReportHashMap.get(type.getType()).getMsgsn().trim());
		try {
			/**
			 * 更新通用结算指令的全部ID
			 */
			if (this.tyjszlztbgDao.updateTyjszlztbgIds(securDeal)) {
				types.add(CheckStatusEnum.Type.SL10);
				logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL10);
			} else {
				logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL11);
				types.add(CheckStatusEnum.Type.SL11);
			}
		} catch (Exception e) {
			types.add(CheckStatusEnum.Type.SL11);
			e.printStackTrace();
		}
		if (report.hasGroup("StsFlg")) {
			List<Group> stsFlgGroup = report.getGroups("StsFlg");
			for (Group group : stsFlgGroup) {
				if (group.hasGroup("InstrStsInf")) {
					List<Group> instrStsInfGroups = group.getGroups("InstrStsInf");
					for (Group instrStsInf : instrStsInfGroups) {
						if (instrStsInf.isSetField("InstrSts")) {
							Field<?> instrSts = instrStsInf.getfieldsForFieldMap().get("InstrSts");
							logger.info("InstrSts:" + instrSts.getObject().toString());
							securDeal.setInstrsts(instrSts.getObject().toString());
						}
						if (instrStsInf.isSetField("TxRsltCd")) {
							Field<?> txRsltCd = instrStsInf.getfieldsForFieldMap().get("TxRsltCd");
							logger.info("TxRsltCd:" + txRsltCd.getObject().toString());
							securDeal.setTxrsltcd(txRsltCd.getObject().toString());
						}
						if (instrStsInf.isSetField("InsErrInf")) {
							Field<?> insErrInf = instrStsInf.getfieldsForFieldMap().get("InsErrInf");
							logger.info("InsErrInf:" + insErrInf.getObject().toString());
							securDeal.setInserrinf(insErrInf.getObject().toString());
						}

					}
				}
				try {
					/**
					 * 更新指令状态
					 */
					if (this.tyjszlztbgDao.updateInstrInformations(securDeal)) {
						types.add(CheckStatusEnum.Type.SL07);
						logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL07);
					} else {
						logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL08);
						types.add(CheckStatusEnum.Type.SL08);
					}
				} catch (Exception e) {
					types.add(CheckStatusEnum.Type.SL08);
					e.printStackTrace();
				}
			}
			/***
			 * 取金额的允许偏差值
			 **/
			BigDecimal amount = new BigDecimal(0.5);
			Map<String, Object> map = new HashMap<String, Object>();
			GentBean gent = null;
			//TODO 金额允许的最大偏差
			try {
				gent = gentDao.queryGent(map);
				if (gent != null) {
					amount = new BigDecimal(gent.getText().trim().toString());
					logger.info("amount :" + amount);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			/**
			 * 第二步如果 type 是 InstructionStatusCode.Type.IS03 那么进行核对金额 只有待确认状态的报文才需要核对金额
			 */
			if (type.compareTo(InstructionStatusCode.Type.IS03) == 0) {
				CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = commonDistributionSettlementStatusReportHashMap
						.get(InstructionStatusCode.Type.IS03.getType()).getCsbs002();
				if (commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
					List<Group> groups = commonDistributionSettlementStatusReport.getGroups("SttlmDtl");
					for (Group group : groups) {
						if (group.isSetField("BizTp")) {
							Field<?> fedealno = group.getfieldsForFieldMap().get("BizTp");
							String bizTp = StringUtils.trimToEmpty(fedealno.getObject().toString());
							logger.info("BizTp :" + fedealno.getObject().toString());
							/**
							 * 回购交易 -BT02 ：质押式回购,BT03 ：买断式回购
							 */
							if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT02.getType())
									|| bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT03.getType())) {
								if (group.isSetField("Val2")) {
									Field<?> val2 = group.getfieldsForFieldMap().get("Val2");
									BigDecimal val2BigDecimal = new BigDecimal(val2.getObject().toString());
									securDeal.setMatprocdamtzz(val2BigDecimal); // 到期交割金额
									securDeal.setMatprocdamtsub(
											securDeal.getMatprocdamt().subtract(val2BigDecimal).abs());
									BigDecimal val1BigDecimal = new BigDecimal(0);
									if (group.isSetField("Val1")) {
										Field<?> val1 = group.getfieldsForFieldMap().get("Val1");
										val1BigDecimal = new BigDecimal(val1.getObject().toString());
										securDeal.setComprocdamtzz(val1BigDecimal); // 首期交割金额
										securDeal.setComprocdamtsub(
												securDeal.getComprocdamt().subtract(val1BigDecimal).abs());
									}
									/**
									 * 允许偏差在多少范围之内
									 */
									if (securDeal.getMatprocdamt().subtract(val2BigDecimal).abs()
											.compareTo(amount) == -1
											&& securDeal.getComprocdamt().subtract(val1BigDecimal).abs()
													.compareTo(amount) == -1) {
										/**
										 * 数据匹配成功 更新YC 生成通用结算指令报文 SL16("SL16","金额核对不符"), SL17("SL17","债券核对不符"),
										 * SL18("SL18","清算日核对不符"), SL19("SL19","结算方式核对不符");
										 */

										// 债券核对 债券数量核对
										boolean mappingBoolean = false;
										// 查询出资金系统债券数
										try {
											// TODO 查询回购的子券
											List<RepoSecurity> repoList = getRepoList.apply(securDeal);
											HashMap<String, BigDecimal> repHashMap = new HashMap<String, BigDecimal>();
											for (RepoSecurity repSecurity : repoList) {
												repHashMap.put(StringUtils.trimToEmpty(repSecurity.getSecid()),
														repSecurity.getQty());
												logger.info("repHashMap is ：" + repHashMap);
											}
											int count = repHashMap.size();
											logger.info("-----------核对债券数量:" + count
													+ "-------------------------------------------");
											if (repoList.size() > 0 && group.hasGroup("Bd1")) {
												List<Group> bd1Groups = group.getGroups("Bd1");
												for (Group bd1 : bd1Groups) {
													Field<?> bdId = bd1.getfieldsForFieldMap().get("BdId");// 债券id
													Field<?> bdAmt = bd1.getfieldsForFieldMap().get("BdAmt");// 面额
													// 或者更改存储
													logger.info("-----------核对第" + count + "次数,债券ID匹配:"
															+ repHashMap.containsKey(StringUtils
																	.trimToEmpty(bdId.getObject().toString()))
															+ ",金额匹配:"
															+ (repHashMap
																	.get(StringUtils
																			.trimToEmpty(bdId.getObject().toString()))
																	.abs()
																	.compareTo(new BigDecimal(bdAmt == null ? "0"
																			: bdAmt.getObject().toString()).multiply(
																					new BigDecimal("10000"))) == 0));
													if (repHashMap.containsKey(
															StringUtils.trimToEmpty(bdId.getObject().toString()))
															&& repHashMap
																	.get(StringUtils
																			.trimToEmpty(bdId.getObject().toString()))
																	.abs()
																	.compareTo(new BigDecimal(bdAmt == null ? "0"
																			: bdAmt.getObject().toString()).multiply(
																					new BigDecimal("10000"))) == 0) {
														count--;
													} else {
														logger.info("-----------核对第" + count + "次数,repHashMap(指令)债券:"
																+ repHashMap
																+ "repHashMap.containsKey(StringUtils.trimToEmpty(bdId.getObject().toString()))"
																+ repHashMap.containsKey(StringUtils
																		.trimToEmpty(bdId.getObject().toString()))
																+ ",金额匹配:bdAmt:"
																+ new BigDecimal(bdAmt == null ? "0"
																		: bdAmt.getObject().toString())
																+ "repHashMap:" + repHashMap);
													}
												}
												if (count == 0) {
													mappingBoolean = true;
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
										if (!mappingBoolean)// 债券核对错误
										{
											types.add(CheckStatusEnum.Type.SL17);
											try {
												if (tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL17,
														securDeal)) {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL00);
													types.add(CheckStatusEnum.Type.SL00);
												} else {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL01);
													types.add(CheckStatusEnum.Type.SL01);
												}
											} catch (Exception e) {
												types.add(CheckStatusEnum.Type.SL01);
												e.printStackTrace();
											}
											continue;
										}
										mappingBoolean = false;
										// SttlmTp1 SttlmTp2 结算方式核对
										if (group.isSetField("SttlmTp1") && group.isSetField("SttlmTp2")) {
											Field<?> sttlmTp1 = group.getfieldsForFieldMap().get("SttlmTp1");
											Field<?> sttlmTp2 = group.getfieldsForFieldMap().get("SttlmTp2");
											String sttlmTp1Str = StringUtils
													.trimToEmpty(sttlmTp1.getObject().toString());
											String sttlmTp2Str = StringUtils
													.trimToEmpty(sttlmTp2.getObject().toString());
											String summitsettl = securDeal.getSetttype_b() == null
													? securDeal.getComccysmeans()
													: StringUtils.trimToEmpty(securDeal.getSetttype_b());
											String summitsett2 = securDeal.getSetttype_b() == null
													? securDeal.getMatccysmeans()
													: StringUtils.trimToEmpty(securDeal.getSetttype_b());
											logger.info("sttlmTp1Str【" + sttlmTp1Str + "】,sttlmTp2Str【" + sttlmTp2Str
													+ "】,summitsettl 【" + summitsettl + "】,summitsett2  【" + summitsett2
													+ "】");
											if (StringUtils.trimToEmpty(
													sttlmTp1Str.equals(BondSettlementType3Code.Type.ST01.getType())
															? "DAP"
															: (sttlmTp1Str
																	.equals(BondSettlementType3Code.Type.ST02.getType())
																			? "PAD"
																			: (sttlmTp1Str.equals(
																					BondSettlementType3Code.Type.ST03
																							.getType()) ? "DVP"
																									: "NON")))
													.equals(summitsettl)
													&& StringUtils.trimToEmpty(sttlmTp2Str
															.equals(BondSettlementType3Code.Type.ST01.getType())
																	? "DAP"
																	: (sttlmTp2Str.equals(
																			BondSettlementType3Code.Type.ST02.getType())
																					? "PAD"
																					: (sttlmTp2Str.equals(
																							BondSettlementType3Code.Type.ST03
																									.getType()) ? "DVP"
																											: "NON")))
															.equals(summitsett2)) {
												mappingBoolean = true;
											}
										}

										if (!mappingBoolean)// 首期到期结算方式核对
										{
											types.add(CheckStatusEnum.Type.SL19);
											try {
												if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL19,
														securDeal)) {
													logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL00);
													types.add(CheckStatusEnum.Type.SL00);
												} else {
													logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL01);
													types.add(CheckStatusEnum.Type.SL01);
												}
											} catch (Exception e) {
												types.add(CheckStatusEnum.Type.SL01);
												e.printStackTrace();
											}
											continue;
										}
										mappingBoolean = false;
										// 日期核对
										if (group.isSetField("Dt1") && group.isSetField("Dt2")) {
											Field<?> dt1 = group.getfieldsForFieldMap().get("Dt1");
											Field<?> dt2 = group.getfieldsForFieldMap().get("Dt2");
											Date dt1Date = null;
											Date dt2Date = null;
											Date summitCDate = null;
											Date summitMDate = null;
											try {
												dt1Date = new SimpleDateFormat("yyyy-MM-dd")
														.parse(dt1.getObject().toString().trim());
												logger.info("dt1Date:" + dt1Date);
												summitCDate = new SimpleDateFormat("yyyy-MM-dd")
														.parse(new SimpleDateFormat("yyyy-MM-dd")
																.format(securDeal.getVdate()));
												logger.info("summitCDate:" + summitCDate);
												dt2Date = new SimpleDateFormat("yyyy-MM-dd")
														.parse(dt2.getObject().toString().trim());
												logger.info("dt2Date:" + dt2Date);
												summitMDate = new SimpleDateFormat("yyyy-MM-dd")
														.parse(new SimpleDateFormat("yyyy-MM-dd")
																.format(securDeal.getMatdate()));
												logger.info("summitMDate:" + summitMDate);
											} catch (ParseException e) {
												e.printStackTrace();
											}

											if (null != summitCDate && null != dt1Date
													&& dt1Date.compareTo(summitCDate) == 0 && null != dt2Date
													&& null != summitMDate && dt2Date.compareTo(summitMDate) == 0) {
												mappingBoolean = true;
											}
										}
										if (!mappingBoolean)// 首期到期日期核对
										{
											types.add(CheckStatusEnum.Type.SL18);
											try {
												if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL18,
														securDeal)) {
													logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL00);
													types.add(CheckStatusEnum.Type.SL00);
												} else {
													logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL01);
													types.add(CheckStatusEnum.Type.SL01);
												}
											} catch (Exception e) {
												types.add(CheckStatusEnum.Type.SL01);
												e.printStackTrace();
											}
											continue;
										}
										try {
											if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL02,
													securDeal)) {
												logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL00);
												types.add(CheckStatusEnum.Type.SL00);
											} else {
												logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL01);
												types.add(CheckStatusEnum.Type.SL01);
											}
										} catch (Exception e) {
											types.add(CheckStatusEnum.Type.SL01);
											e.printStackTrace();
										}
									} else {
										/**
										 * 数据核对不符 更新NC
										 * 
										 */
										types.add(CheckStatusEnum.Type.SL16);
										try {
											if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL16,
													securDeal)) {
												logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL00);
												types.add(CheckStatusEnum.Type.SL00);
											} else {
												logger.info("CheckStatusEnum.Type :" + CheckStatusEnum.Type.SL01);
												types.add(CheckStatusEnum.Type.SL01);
											}
										} catch (Exception e) {
											types.add(CheckStatusEnum.Type.SL01);
											e.printStackTrace();
										}
									}
								}
							}
							/**************************************************************************************************************************************/
							// 债券交易
							if (bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT00.getType())
									|| bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT01.getType())
									|| bizTp.equalsIgnoreCase(BusinessTypeCode.Type.BT04.getType())) {
								/**
								 * BT01 ：现券, BT04 ：债券远期 如果是现券或债券远期的,只需对比VAL2即可
								 */
								logger.info("BusinessTypeCode.Type is :" + BusinessTypeCode.Type.BT01.getType());
								if (group.isSetField("Val2")) {
									Field<?> val2 = group.getfieldsForFieldMap().get("Val2");// 结算金额 全价金额
									BigDecimal val2BigDecimal = new BigDecimal(val2.getObject().toString());
									securDeal.setMatprocdamtzz(val2BigDecimal);
									securDeal.setMatprocdamtsub(
											securDeal.getMatprocdamt().subtract(val2BigDecimal).abs());
									if (group.isSetField("Val1")) {
										Field<?> val1 = group.getfieldsForFieldMap().get("Val1");// 净价金额
										BigDecimal val1BigDecimal = new BigDecimal(val1.getObject().toString());
										logger.info("val1BigDecimal : " + val1BigDecimal);
										securDeal.setComprocdamtzz(val1BigDecimal);
										securDeal.setComprocdamtsub(
												securDeal.getComprocdamt().subtract(val1BigDecimal).abs());
									}

									/**
									 * 允许偏差在1之内
									 */
									if (securDeal.getMatprocdamt().subtract(val2BigDecimal).abs()
											.compareTo(amount) == -1) {
										/**
										 * 数据匹配成功 更新YC 生成通用结算指令报文 SL16("SL16","金额核对不符"), SL17("SL17","债券核对不符"),
										 * SL18("SL18","清算日核对不符"), SL19("SL19","结算方式核对不符");
										 */

										// 债券id
										boolean mappingBoolean = false;
										if (group.hasGroup("Bd1")) {
											List<Group> bd1Groups = group.getGroups("Bd1");
											for (Group bd1 : bd1Groups) {
												Field<?> bdId = bd1.getfieldsForFieldMap().get("BdId");
												if (StringUtils.trim(securDeal.getSecid()).equalsIgnoreCase(
														StringUtils.trimToEmpty(bdId.getObject().toString()))) {
													logger.info("BdId :" + bdId.getObject().toString());
													mappingBoolean = true;
												}
											}
										}
										if (!mappingBoolean)// 债券核对错误
										{
											types.add(CheckStatusEnum.Type.SL17);
											try {
												if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL17,
														securDeal)) {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL00);
													types.add(CheckStatusEnum.Type.SL00);
												} else {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL01);
													types.add(CheckStatusEnum.Type.SL01);
												}
											} catch (Exception e) {
												types.add(CheckStatusEnum.Type.SL01);
												throw e;
											}
											continue;
										}
										mappingBoolean = false;
										// SttlmTp1
										if (group.isSetField("SttlmTp1")) {
											Field<?> sttlmTp1 = group.getfieldsForFieldMap().get("SttlmTp1");
											String sttlmTp1Str = StringUtils
													.trimToEmpty(sttlmTp1.getObject().toString());
											String summitsettl = securDeal.getSetttype_b() == null
													? securDeal.getComccysmeans()
													: StringUtils.trimToEmpty(securDeal.getSetttype_b());
											logger.info("SttlmTp1 ：" + sttlmTp1Str + "summitsettl:" + summitsettl);
											if (StringUtils.trimToEmpty(
													sttlmTp1Str.equals(BondSettlementType3Code.Type.ST01.getType())
															? "DAP"
															: (sttlmTp1Str
																	.equals(BondSettlementType3Code.Type.ST02.getType())
																			? "PAD"
																			: (sttlmTp1Str.equals(
																					BondSettlementType3Code.Type.ST03
																							.getType()) ? "DVP"
																									: "NON")))
													.equals(summitsettl)) {
												mappingBoolean = true;
											}
										}
										if (!mappingBoolean)// 结算方式核对错误
										{
											types.add(CheckStatusEnum.Type.SL19);
											try {
												if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL19,
														securDeal)) {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL00);
													types.add(CheckStatusEnum.Type.SL00);
												} else {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL01);
													types.add(CheckStatusEnum.Type.SL01);
												}
											} catch (Exception e) {
												types.add(CheckStatusEnum.Type.SL01);
												throw e;
											}
											continue;
										}
										mappingBoolean = false;
										if (group.isSetField("Dt1")) {
											Field<?> dt1 = group.getfieldsForFieldMap().get("Dt1");
											Date dt1Date = null;
											Date summitDate = null;
											try {
												dt1Date = new SimpleDateFormat("yyyy-MM-dd")
														.parse(dt1.getObject().toString().trim());
												summitDate = new SimpleDateFormat("yyyy-MM-dd")
														.parse(new SimpleDateFormat("yyyy-MM-dd")
																.format(securDeal.getVdate()));
												logger.info("Dt1 ：" + dt1Date + "summitDate:" + summitDate);
											} catch (ParseException e) {
												e.printStackTrace();
											}

											if (null != summitDate && null != dt1Date
													&& dt1Date.compareTo(summitDate) == 0) {
												mappingBoolean = true;
											}
										}
										if (!mappingBoolean)// 结算方式核对错误
										{
											types.add(CheckStatusEnum.Type.SL18);
											try {
												if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL18,
														securDeal)) {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL00);
													types.add(CheckStatusEnum.Type.SL00);
												} else {
													logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL01);
													types.add(CheckStatusEnum.Type.SL01);
												}
											} catch (Exception e) {
												types.add(CheckStatusEnum.Type.SL01);
												e.printStackTrace();
											}
											continue;
										}
										try {
											if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL02,
													securDeal)) {
												logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL00);
												types.add(CheckStatusEnum.Type.SL00);
											} else {
												logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL01);
												types.add(CheckStatusEnum.Type.SL01);
											}
										} catch (Exception e) {
											types.add(CheckStatusEnum.Type.SL01);
											e.printStackTrace();
										}
									} else {
										/**
										 * 数据核对不符 更新NC
										 * 
										 */
										types.add(CheckStatusEnum.Type.SL16);
										try {
											if (this.tyjszlztbgDao.updateCheckSts(CheckStatusEnum.Type.SL16,
													securDeal)) {
												logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL00);
												types.add(CheckStatusEnum.Type.SL00);
											} else {
												logger.info("CheckStatusEnum.Type ：" + CheckStatusEnum.Type.SL01);
												types.add(CheckStatusEnum.Type.SL01);
											}
										} catch (Exception e) {
											types.add(CheckStatusEnum.Type.SL01);
											e.printStackTrace();
										}
									}
								}
							}
						}
				  }
				}
			}
		}
		return types;
	}

	/**
	 * 格式化
	 * 
	 * @param bigDecimal
	 * @return
	 */
	public String formatBigDecimal(BigDecimal bigDecimal) {
		DecimalFormat f = new DecimalFormat("###,###,###,###.00");

		f.setDecimalSeparatorAlwaysShown(true);

		return f.format(bigDecimal);

	}
	@Override
	public RetBean validCdtcWorkTime(boolean isHolyday) throws Exception {
		RetBean ret = new RetBean();
		ret.setRetStatus(RetStatusEnum.S);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (isHolyday) {
			// 当前是节假期，不进行业务处理，休眠2秒
			logger_1.info("当前是节假期，不进行业务处理!");
			ret.setRetStatus(RetStatusEnum.F);
			ret.setRetCode("000001");
			ret.setRetMsg("当前是节假期，不进行业务处理!");
			return ret;
			
		} else {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String todayDate = sdf.format(new Date());
			
			String beginTime = todayDate + " " + this.beginWorkTime;
			Calendar beginTimec1 = Calendar.getInstance();
			beginTimec1.setTime(df.parse(beginTime));
			
			Calendar endTimec2 = Calendar.getInstance();
			String endTime = todayDate + " " + this.endWorkTime;
			endTimec2.setTime(df.parse(endTime));
			
			Calendar todayTimec3 = Calendar.getInstance();
			todayTimec3.setTime(new Date());
			
			if (beginTimec1.compareTo(todayTimec3) > 0 || endTimec2.compareTo(todayTimec3) < 0) {
				logger_1.info("[" + beginTime + "~" + endTime + "]当前是非工作时间，不进行业务处理："+ df.format(todayTimec3.getTime()));
				
				ret.setRetStatus(RetStatusEnum.F);
				ret.setRetCode("000001");
				ret.setRetMsg("[" + beginTime + "~" + endTime + "]当前是非工作时间，不进行业务处理："+ df.format(todayTimec3.getTime()));
				return ret;
			}
		}
		return ret;
	}
}
