package com.singlee.financial.cdtc.cnbondpackage;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;

import org.desy.trd.jdom.Document;
import org.desy.trd.jdom.input.SAXBuilder;
import org.desy.trd.jdom.output.XMLOutputter;
import org.dom4j.io.SAXReader;

import com.singlee.financial.cdtc.bean.Bean1;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.ISODateTime;
import com.singlee.financial.cdtc.pac.Max35Text;
import com.singlee.financial.cdtc.pac.Max40Text;
import com.singlee.financial.cdtc.pac.Max8Text;
import com.singlee.financial.cdtc.pac.Priority4Code;

public class SystemExceptionAdviceV01 extends Group {

	/**
	 * 6.3.9	系统异常通知（CSBS.041.001.01 SystemExceptionAdviceV01）
	 * 系统异常通知 报文由中央债券综合业务系统发送给结算成员,用于:
	 *  中央债券综合业务系统在处理由结算成员发送的报文出现系统异常时,向结算成员发起的系统通知.
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static String type = "CSBS.009.001.01";
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Msg",  	ReportMsgHeader.class);
		keyClassMap.put("SysXcptn",	SysXcptn.class);
		//keyClassMap.put("Xtnsn",    Group.class);
		keyClassMap.putAll(ReportMsgHeader.keyClassMap);
		keyClassMap.putAll(SysXcptn.keyClassMap);
	}
	
	/**
	 * Msg      报文标识
	 * SysXcptn 系统异常返回信息
	 * Xtnsn    扩展项
	 * @param tag
	 */
	public SystemExceptionAdviceV01(String tag){
		super(tag,tag,new String[]{"Msg","SysXcptn","Xtnsn"});
	}
	public SystemExceptionAdviceV01(String tag,String belim){
		super(tag,tag,new String[]{"Msg","SysXcptn","Xtnsn"});
	}
	
	public boolean isHasTag(String tag)
	{
		String[] strings = getFieldOrder();
		for(String key:strings)
		{
			if(key.equals(tag))
			{
				return true;
			}
		}
		return false;
	}
	
	public void set(ReportMsgHeader msg){
		addGroup(msg);
	}
	
	public void set(SysXcptn sysXcptn){
		addGroup(sysXcptn);
	}
	
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
	
	public static SystemExceptionAdviceV01 content(Bean1 bean1){
SystemExceptionAdviceV01 systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
		
		ReportMsgHeader msg = new ReportMsgHeader("Msg");
		
		Max35Text max35Text = new Max35Text("TxFlowId");
		max35Text.setValue("交易流水标识");
		msg.set(max35Text);
		
		Priority4Code priority4Code = new Priority4Code("Prty");
		priority4Code.setValue(Priority4Code.Type.PT01.getType());
		msg.set(priority4Code);
		
		ISODateTime isoDateTime = new ISODateTime("CreDtTm",new Date());
		msg.set(isoDateTime);
		systemExceptionAdviceV01.set(msg);
		
		SysXcptn sysXcptn = new SysXcptn("SysXcptn");
		Max8Text XcptnRtrCd = new Max8Text("XcptnRtrCd"); //异常返回代码
		XcptnRtrCd.setValue("10000001");
		sysXcptn.set(XcptnRtrCd);
		
		Max40Text XcptnRtrTx = new Max40Text("XcptnRtrTx");//异常返回描述
		XcptnRtrTx.setValue("异常返回描述");
		sysXcptn.set(XcptnRtrTx);
		systemExceptionAdviceV01.set(sysXcptn);
		return systemExceptionAdviceV01;
	}
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception{
		
		
		StringBuffer xmlBuffer = new StringBuffer();
		content(new Bean1()).toXmlForSuperParent(xmlBuffer);
		System.out.println(XmlFormat.format(xmlBuffer.toString()));
		
		// 保存xml文件
		SAXBuilder saxBuilder = new SAXBuilder();
		Document doc = saxBuilder.build(new ByteArrayInputStream(XmlFormat.format(xmlBuffer.toString()).getBytes("UTF-8")));
		FileOutputStream out=new FileOutputStream("./cnbondxml/"+SystemExceptionAdviceV01.type+".xml"); 
		XMLOutputter outputter = new XMLOutputter(); 
		//如果不设置format，仅仅是没有缩进，xml还是utf-8的，因此format不是必要的 
		outputter.output((Document) doc, out); 
		out.close(); 
		
		/**
         * 读取请求报文XML  反解析报文
         */
		SAXReader reader = new SAXReader();
		SystemExceptionAdviceV01 adviceV01 = new SystemExceptionAdviceV01("systemExceptionAdviceV01");
		org.dom4j.Document xmlDoc = reader.read(new File("./cnbondxml/"+adviceV01.type+".xml"));
		XMLRevSysExceptionAdviceV01.getElementList(xmlDoc.getRootElement(),adviceV01);
		
		
		System.out.println("*********************************************************");
		StringBuffer xmlBuffer2 = new StringBuffer();
		adviceV01.toXmlForSuperParent(xmlBuffer2);
		System.out.println("*********************************************************");
		System.out.println(XmlFormat.format(xmlBuffer2.toString()));
		
	}
	
}
