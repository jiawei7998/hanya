package com.singlee.financial.cdtc.util;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.cdtc.pac.BondSettlementType2Code;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.InstructionConfirmIndicatorCode;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.InstructionStatusCode;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;

/***
 * 
 * 手工结算解析报文
 *
 */
public class ManualSettleLoadMsg {
	/***
	 * 解析报文的业务类型为：BT00(普通分销)、BT01(现券)、BT04(债券远期)、BT05(债券借贷)
	 * @param str
	 * @return
	 */
	public static  ManualSettleDetail zLDetail(String str){
		ManualSettleDetail manualSettleDetail = new ManualSettleDetail();
		try{
			SAXReader reader = new SAXReader();
			org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(str).getBytes("UTF-8")))); 
			SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
			CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
			spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
			spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
			if(spotSettlementInstruction.hasGroup("Document")) {
				List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
				for(Group group : groupsList) {
					// 保存xml文件
					StringBuffer xmlBuffer = new StringBuffer();
					group.toXmlForParent(xmlBuffer);
					commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
					Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
					CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
				}
			}
			if(null != commonDistributionSettlementStatusReport) {
				if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
					Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
					String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
					//指令处理状态
					manualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
					manualSettleDetail.setInstrStscode(instrSts);
					Group instrStsInf = stsFlg.hasGroup("InstrStsInf")?stsFlg.getGroups("InstrStsInf").get(0):null;
					if(null != instrStsInf) {
						//发令方确认标识
						manualSettleDetail.setOrgtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd")?InstructionConfirmIndicatorCode.Type.getName(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString()):"未确认");
						manualSettleDetail.setOrgtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString());
						//对手方确认标识
						manualSettleDetail.setCtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd")?InstructionConfirmIndicatorCode.Type.getName(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString()):"未确认");
						manualSettleDetail.setCtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString());
						//处理结果状态
						manualSettleDetail.setTxRsltCd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd")?instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject().toString():" ");
					}
					if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
						Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
						String InstrOrgn = sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString();
						//指令来源
						manualSettleDetail.setInstrOrgn(InstrOrgn);
						if(InstrOrgn.equalsIgnoreCase(InstructionOriginCode.Type.IO01.getType())) {
							//发令方确认标识
							manualSettleDetail.setOrgtrCnfrmInd("--");
							//对手方确认标识
							manualSettleDetail.setCtrCnfrmInd("--");
						}
						//业务类型
						manualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
						manualSettleDetail.setBizTpcode(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString());
						//结算指令标识
						manualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
						//付券方账户id
						manualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						//付券方账户名称
						manualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						//收券方账户id
						manualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						//收券方账户名称
						manualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						//结算方式1
						manualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
						manualSettleDetail.setSttlmTp1code(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString());
						//结算金额1
						manualSettleDetail.setVal1(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString());
						//结算金额2
						manualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2")?FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())):"");
						//交割日1
						manualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
						//业务标识号
						manualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId")?sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString():"");
						if(sttlmDtl.hasGroup("Bd1")) {
							Group bd1 = sttlmDtl.getGroups("Bd1").get(0);
							manualSettleDetail.setBdAmt(FormatUtil.formatBigDecimal(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString())));
							manualSettleDetail.setPric1(bd1.getfieldsForFieldMap().get("Pric1").getObject().toString());
							manualSettleDetail.setPric2(bd1.getfieldsForFieldMap().containsKey("Pric2")?bd1.getfieldsForFieldMap().get("Pric2").getObject().toString():"");
						}
						manualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
						manualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
					
					
					}
					
				}
			}
		}catch (Exception e) {
			
		}
		
		return manualSettleDetail;
	}
	
	
	/***
	 * 解析报文的业务类型为：BT02（质押式回购）
	 * 
	 * @param str
	 * @return
	 */
	public  static ManualSettleDetail zLRepoDetail(String str){
		ManualSettleDetail manualSettleDetail = new ManualSettleDetail();
		try{
			SAXReader reader = new SAXReader();
			org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(str).getBytes("UTF-8")))); 
			SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
			CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
			spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
			spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
			if(spotSettlementInstruction.hasGroup("Document")) {
				List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
				for(Group group : groupsList) {
					// 保存xml文件
					StringBuffer xmlBuffer = new StringBuffer();
					group.toXmlForParent(xmlBuffer);
					commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
					Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
					CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
				}
			}
			if(null != commonDistributionSettlementStatusReport) {
				if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
					Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
					String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
					//指令处理状态
					manualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
					manualSettleDetail.setInstrStscode(instrSts);
					Group instrStsInf = stsFlg.hasGroup("InstrStsInf")?stsFlg.getGroups("InstrStsInf").get(0):null;
					if(null != instrStsInf) {
						//发令方确认标识
						manualSettleDetail.setOrgtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd")?InstructionConfirmIndicatorCode.Type.getName(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString()):"未确认");
						manualSettleDetail.setOrgtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString());
						//对手方确认标识
						manualSettleDetail.setCtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd")?InstructionConfirmIndicatorCode.Type.getName(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString()):"未确认");
						manualSettleDetail.setCtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString());
						//处理结果状态
						manualSettleDetail.setTxRsltCd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd")?instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject().toString():" ");
					}
					
				}
				if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
					Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
					String InstrOrgn = sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString();
					if(InstrOrgn.equalsIgnoreCase(InstructionOriginCode.Type.IO01.getType())) {
						//发令方确认标识
						manualSettleDetail.setOrgtrCnfrmInd("--");
						//对手方确认标识
						manualSettleDetail.setCtrCnfrmInd("--");
					}
					if("BT02".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){//质押式
						//指令来源
						manualSettleDetail.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
						manualSettleDetail.setInstrOrgncode(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString());
						//业务类型
						manualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
						manualSettleDetail.setBizTpcode(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString());
						//结算指令标识
						manualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
						//付券方账户id
						manualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						//付券方账户名称
						manualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						//收券方账户id
						manualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						//收券方账户名称
						manualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						//结算方式1
						manualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
						manualSettleDetail.setSttlmTp1code(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString());
						//结算方式2
						manualSettleDetail.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
						manualSettleDetail.setSttlmTp2code(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString());
						//结算金额1
						manualSettleDetail.setVal1(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString());
						//结算金额2
						manualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2")?FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())):"");
						//交割日1
						manualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
						//交割日2
						manualSettleDetail.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
						//业务标识号
						manualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId")?sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString():"");
						//债券总额
						manualSettleDetail.setAggtFaceAmt(FormatUtil.formatBigDecimal4(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("AggtFaceAmt").getObject().toString())));
						//回购利率
						manualSettleDetail.setRate(sttlmDtl.getfieldsForFieldMap().get("Rate").getObject().toString());
						//操作者
						manualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
						//操作者
						manualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
					}
					
				}
			}
			
		}catch(Exception e){
			
		}
		return manualSettleDetail;
	}
	
	/***
	 * 解析报文的业务类型为：BT03（买断式回购）
	 * @param str
	 * @return
	 */
	public  static ManualSettleDetail zLRepoMDDetail(String str){
		ManualSettleDetail manualSettleDetail = new ManualSettleDetail();
		try{
			SAXReader reader = new SAXReader();
			org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream((XmlFormat.format(str).getBytes("UTF-8")))); 
			SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
			CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
			spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
			spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
			if(spotSettlementInstruction.hasGroup("Document")) {
				List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
				for(Group group : groupsList) {
					// 保存xml文件
					StringBuffer xmlBuffer = new StringBuffer();
					group.toXmlForParent(xmlBuffer);
					commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
					Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBuffer.toString()+"</Document>").getBytes("UTF-8"))));  
					CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
				}
			}
			if(null != commonDistributionSettlementStatusReport) {
				if(commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
					Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
					String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
					//指令处理状态
					manualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
					manualSettleDetail.setInstrStscode(instrSts);
					Group instrStsInf = stsFlg.hasGroup("InstrStsInf")?stsFlg.getGroups("InstrStsInf").get(0):null;
					if(null != instrStsInf) {
						//发令方确认标识
						manualSettleDetail.setOrgtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd")?InstructionConfirmIndicatorCode.Type.getName(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString()):"未确认");
						manualSettleDetail.setOrgtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString());
						//对手方确认标识
						manualSettleDetail.setCtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd")?InstructionConfirmIndicatorCode.Type.getName(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString()):"未确认");
						manualSettleDetail.setCtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString());
						//处理结果状态
						manualSettleDetail.setTxRsltCd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd")?instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject().toString():" ");
					}
					
				}
				if(commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
					Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
					if("BT03".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())){//买断式
						String InstrOrgn  = sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString();
						if(InstrOrgn.equalsIgnoreCase(InstructionOriginCode.Type.IO01.getType())) {
							//发令方确认标识
							manualSettleDetail.setOrgtrCnfrmInd("--");
							//对手方确认标识
							manualSettleDetail.setCtrCnfrmInd("--");
						}
						//指令来源
						manualSettleDetail.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
						manualSettleDetail.setInstrOrgncode(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString());
						//业务类型
						manualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
						manualSettleDetail.setBizTpcode(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString());
						//结算指令标识
						manualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
						//付券方账户id
						manualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						//付券方账户名称
						manualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						//收券方账户id
						manualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
						//收券方账户名称
						manualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
						//结算方式1
						manualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
						manualSettleDetail.setSttlmTp1code(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString());
						//结算方式2
						manualSettleDetail.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
						manualSettleDetail.setSttlmTp2code(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString());
						//结算金额1
						manualSettleDetail.setVal1(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString());
						//结算金额2
						manualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2")?FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())):"");
						//交割日1
						manualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
						//交割日2
						manualSettleDetail.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
						//业务标识号
						manualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId")?sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString():"");
						if(sttlmDtl.hasGroup("SttlmGrte1")) {
							/**
							 * 按实例进行解析
							 */
						}else{
							//担保方式
							manualSettleDetail.setGrteTp("无");
						}
						//操作者
						manualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
						//操作者
						manualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm")?sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString():"");
							
					}
				}
			}
		}catch(Exception e){
			
		}
		return manualSettleDetail;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
