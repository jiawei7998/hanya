package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class AccountIdentificationAndName5 extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("Nm",       Field.class);
		keyClassMap.put("Id", 	    Field.class);
		keyClassMap.put("BdCnt",    Field.class);
		keyClassMap.put("Bd",       Bond2.class);
		keyClassMap.putAll(Bond2.keyClassMap);
	}
	
	public AccountIdentificationAndName5(String tag) {
		super(tag, tag, new String[] { "Nm", "Id", "BdCnt", "Bd" });
	}
	
	public AccountIdentificationAndName5(String tag,String delim) {
		super(tag, tag, new String[] { "Nm", "Id", "BdCnt", "Bd" });
	}

	public void set(Max35Text value) {
		setField(value);
	}

	public Max35Text get(Max35Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max35Text getMax35Text() throws FieldNotFound {
		Max35Text value = new Max35Text("");
		getField(value);

		return value;
	}

	public boolean isSet(Max35Text field) {
		return isSetField(field);
	}

	public boolean isSetMax35Text() {
		return isSetField("Nm");
	}

	public void set(Exact11Text value) {
		setField(value);
	}

	public Exact11Text get(Exact11Text value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Exact11Text getExact11Text() throws FieldNotFound {
		Exact11Text value = new Exact11Text("");
		getField(value);

		return value;
	}

	public boolean isSet(Exact11Text field) {
		return isSetField(field);
	}

	public boolean isSetExact11Text() {
		return isSetField("Id");
	}

	public void set(Max5NumericText value) {
		setField(value);
	}

	public Max5NumericText get(Max5NumericText value) throws FieldNotFound {
		getField(value);

		return value;
	}

	public Max5NumericText getMax5NumericText() throws FieldNotFound {
		Max5NumericText value = new Max5NumericText("");
		getField(value);

		return value;
	}

	public boolean isSet(Max5NumericText field) {
		return isSetField(field);
	}

	public boolean isSetMax5NumericText() {
		return isSetField("BdCnt");
	}

	public void set(Bond2 group) {
		addGroup(group);
	}

	public boolean isSet(Bond2 field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetBond2() {
		return isSetField("Bd");
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
