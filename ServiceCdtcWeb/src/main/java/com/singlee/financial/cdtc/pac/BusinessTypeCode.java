package com.singlee.financial.cdtc.pac;


/**
 *  BT00：普通分销
BT01：现券
BT02：质押式回购
BT03：买断式回购
BT04：债券远期
BT05：债券借贷
BT06：质押券置换
BT07：BEPS质押
BT08：BEPS解押
BT09：BEPS质押券置换
BT10：投资人选择提前赎回
BT11：转托管
BT12：柜台专项结算
BT13：柜台分销结算
BT14：公开市场操作现券
BT15：公开市场操作质押式回购
BT16：国库现金商业银行定期存款
BT17：收款确认
BT18：付款确认
BT19：质押式回购逾期返售
BT20：现金了结交割
BT21：撤销合同
BT22：撤销指令
BT23：质押式回购到期
BT24：买断式回购到期
BT25：债券借贷到期
BT26：公开市场操作质押式回购到期
BT27：国库现金商业银行定期存款到期

 * @author Administrator
 *
 */
public class BusinessTypeCode extends StringField {
	public static enum Type {
	BT00("BT00","普通分销                     "),
	BT01("BT01","现券                         "),
	BT02("BT02","质押式回购                   "),
	BT03("BT03","买断式回购                   "),
	BT04("BT04","债券远期                     "),
	BT05("BT05","债券借贷                     "),
	BT06("BT06","质押券置换                   "),
	BT07("BT07","BEPS质押                     "),
	BT08("BT08","BEPS解押                     "),
	BT09("BT09","BEPS质押券置换               "),
	BT10("BT10","投资人选择提前赎回           "),
	BT11("BT11","转托管                       "),
	BT12("BT12","柜台专项结算                 "),
	BT13("BT13","柜台分销结算                 "),
	BT14("BT14","公开市场操作现券             "),
	BT15("BT15","公开市场操作质押式回购       "),
	BT16("BT16","国库现金商业银行定期存款     "),
	BT17("BT17","收款确认                     "),
	BT18("BT18","付款确认                     "),
	BT19("BT19","质押式回购逾期返售           "),
	BT20("BT20","现金了结交割                 "),
	BT21("BT21","撤销合同                     "),
	BT22("BT22","撤销指令                     "),
	BT23("BT23","质押式回购到期               "),
	BT24("BT24","买断式回购到期               "),
	BT25("BT25","债券借贷到期                 "),
	BT26("BT26","公开市场操作质押式回购到期   "),
	BT27("BT27","国库现金商业银行定期存款到期 ");
	private String type;
	
	private String name;

	
	private Type(String type, String name) {
		this.type = type;
		this.name = name;
	}
	// 普通方法   
    public static String getName(String type) {   
        for (Type c : Type.values()) {   
            if (c.getType().equalsIgnoreCase(type)) {   
               return c.name;   
            }   
        }   
        return "";   
    }  
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.type+":"+this.name;
	}
}
	
	public BusinessTypeCode(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	
	private static final String regStr="^[A-Za-z]{2}[0-9]{2}$";

	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if(objectAsString().matches(regStr))
		{
			return toXml();
		}
		return "";
	}
}
