package com.singlee.financial.cdtc.pac;


/**
 * 
 * 3位英文币种属性和金额总长12位，最多2位小数
 * @author Administrator
 *
 */
public class PercentageRate extends StringField{
	
	public PercentageRate(String field) {
		super(field);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

	private static final String regStr12="^[0-9]{1,11}$";
	private static final String regStr2="^[0-9]{0,10}$";
	@Override
	public String toXmlString() {
		// TODO Auto-generated method stub
		if((objectAsString().replace(".", "").matches(regStr12) && !objectAsString().contains("%"))
				|| (objectAsString().replace(".", "").replace("%", "").matches(regStr2) && objectAsString().contains("%")))
		{
			if(objectAsString().replace("%", "").substring(objectAsString().indexOf(".")+1).matches(regStr2))
			{
				return toXml();
			}
		}
		return "";
	}
	
	public static void main(String[] args) {
		PercentageRate faceCurrencyAndAmount = new PercentageRate("amt");
		faceCurrencyAndAmount.setValue("10.123456789%");
		System.out.println(faceCurrencyAndAmount.toXmlString());
	}
}
