package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UtcTimeOnlyField extends DateField {
	private static final long serialVersionUID = 1L;
	  private boolean includeMilliseconds = true;

	  private static TimeZone timezone = TimeZone.getTimeZone("UTC");
	  private static Calendar calendar;

	  public UtcTimeOnlyField(String field)
	  {
	    super(field, UtcTimeOnlyConverter.convert(createDate(), true));
	  }

	  protected UtcTimeOnlyField(String field, Date data)
	  {
	    super(field, UtcTimeOnlyConverter.convert(data, true));
	  }

	  protected UtcTimeOnlyField(String field, String data)
	  {
	    super(field, data);
	  }

	  public UtcTimeOnlyField(String field, boolean includeMilliseconds)
	  {
	    super(field, UtcTimeOnlyConverter.convert(createDate(), includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected UtcTimeOnlyField(String field, Date data, boolean includeMilliseconds)
	  {
	    super(field, UtcTimeOnlyConverter.convert(data, includeMilliseconds));
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  protected UtcTimeOnlyField(String field, String data, boolean includeMilliseconds)
	  {
	    super(field, data);
	    this.includeMilliseconds = includeMilliseconds;
	  }

	  boolean showMilliseconds() {
	    return this.includeMilliseconds;
	  }

	  public void setValue(Date value) {
	    setValue(UtcTimeOnlyConverter.convert(value, true));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(UtcTimeOnlyConverter.convert(value, true));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }
}
