package com.singlee.financial.cdtc.cnbondpackage;

import java.util.HashMap;

import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.FieldNotFound;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.Max40Text;
import com.singlee.financial.cdtc.pac.Max8Text;

public class SysXcptn extends Group {

	/**
	 * 系统异常返回信息
	 */
	private static final long serialVersionUID = 1L;
	
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("XcptnRtrCd",  	Field.class);
		keyClassMap.put("XcptnRtrTx",	Field.class);
	}
	/**
	 * XcptnRtrCd   异常返回代码
	 * XcptnRtrTx   异常返回描述
	 * @param tag
	 */
	public SysXcptn(String tag){
		super(tag,tag,new String[]{"XcptnRtrCd","XcptnRtrTx"});
	}
	
	public SysXcptn(String tag,String delim){
		super(tag,tag,new String[]{"XcptnRtrCd","XcptnRtrTx"});
	}
	

	public void set(Max8Text value){
		setField(value);
	}
	
	public Max8Text get(Max8Text value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(Max8Text field) {
		return isSetField(field);
	}

	public boolean isSetMax8Text() {
		return isSetField("XcptnRtrCd");
	}
	
	

	public void set(Max40Text value){
		setField(value);
	}
	
	public Max40Text get(Max40Text value) throws FieldNotFound {
		getField(value);

		return value;
	}
	

	public boolean isSet(Max40Text field) {
		return isSetField(field);
	}

	public boolean isSetMax40Text() {
		return isSetField("XcptnRtrTx");
	}
	

}
