/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * @author Fang
 *
 */
public class EsbDateOnlyConverter extends AbstractDateTimeConverter {

	private static ThreadLocal<EsbDateOnlyConverter> utcDateConverter = new ThreadLocal<EsbDateOnlyConverter>();

	  private DateFormat dateFormat = createLocalDateFormat("yyyyMMdd");

	  public static String convert(Date d)
	  {
	    return getFormatter().format(d);
	  }

	  private static DateFormat getFormatter() {
		  EsbDateOnlyConverter converter = (EsbDateOnlyConverter)utcDateConverter.get();
	    if (converter == null) {
	      converter = new EsbDateOnlyConverter();
	      utcDateConverter.set(converter);
	    }
	    return converter.dateFormat;
	  }

	  public static Date convert(String value)
	    throws FieldConvertError
	  {
	    Date d = null;
	    String type = "date";
	    assertLength(value, 8, type);
	    assertDigitSequence(value, 0, 8, type);

	    checkDate(value, 0, 8, type);
	    try {
	      d = getFormatter().parse(value);
	    } catch (ParseException e) {
	      throwFieldConvertError(value, type);
	    }
	    return d;
	  }
	
}
