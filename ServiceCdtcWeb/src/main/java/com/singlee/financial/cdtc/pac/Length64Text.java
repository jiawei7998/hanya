package com.singlee.financial.cdtc.pac;

public class Length64Text extends StringField {

	public Length64Text(String field) {
		super(field);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public String toXmlString(){
		if(super.getLength() <= 64){
			return toXml();
		}
		return "";
	}

}
