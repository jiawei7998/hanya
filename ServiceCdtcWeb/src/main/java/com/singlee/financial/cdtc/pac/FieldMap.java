package com.singlee.financial.cdtc.pac;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class FieldMap implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String[] fieldOrder;
	private final TreeMap<String, Field<?>> fields;
	private final TreeMap<String, List<Group>> groups = new TreeMap<String, List<Group>>();

	public TreeMap<String, Field<?>> getfieldsForFieldMap() {
		return this.fields;
	}

	protected FieldMap(String[] fieldOrder) {
		this.fieldOrder = fieldOrder;
		this.fields = new TreeMap<String, Field<?>>();
	}

	protected FieldMap() {
		this(null);
	}

	protected void initializeFrom(FieldMap source) {
		this.fields.clear();
		this.fields.putAll(source.fields);
		Iterator<?> groupItr = source.groups.entrySet().iterator();
		while (groupItr.hasNext()) {
			Map.Entry<?,?> entry = (Map.Entry<?,?>) groupItr.next();
			ArrayList<Group> clonedMembers = new ArrayList<Group>();
			List<?> groupMembers = (List<?>) entry.getValue();
			for (int i = 0; i < groupMembers.size(); i++) {
				Group originalGroup = (Group) groupMembers.get(i);
				Group clonedGroup = new Group(originalGroup.getFieldTag(),
						originalGroup.delimTag(), originalGroup.getFieldOrder());
				clonedGroup.initializeFrom(originalGroup);
				clonedMembers.add(clonedGroup);
			}
			this.groups.put(String.valueOf(entry.getKey()), clonedMembers);
		}
	}

	private boolean isGroupField(String field) {
		return this.groups.containsKey(field);
	}

	public void setGroups(FieldMap fieldMap) {
		this.groups.clear();
		this.groups.putAll(fieldMap.groups);
	}

	protected void setGroups(String key, List<Group> groupList) {
		this.groups.put(key, groupList);
	}

	public String[] getFieldOrder() {
		return this.fieldOrder;
	}

	public void clear() {
		this.fields.clear();
		this.groups.clear();
	}

	public boolean isEmpty() {
		return this.fields.size() == 0;
	}

	public void setFields(FieldMap fieldMap) {
		this.fields.clear();
		this.fields.putAll(fieldMap.fields);
	}

	public void setString(String field, String value) {
		setField(new StringField(field, value));
	}

	public void setBoolean(String field, boolean value) {
		String s = BooleanConverter.convert(value);
		setField(new StringField(field, s));
	}

	public void setInt(String field, int value) {
		String s = IntConverter.convert(value);
		setField(new StringField(field, s));
	}

	public void setDouble(String field, String value) {
		setField(new StringField(field, value));
	}

	public void setUtcTimeStamp(String field, Date value) {
		setUtcTimeStamp(field, value, false);
	}

	public void setUtcTimeStamp(String field, String value) {
		setUtcTimeStamp(field, value, false);
	}

	public void setUtcTimeStamp(String field, Date value,
			boolean includeMilliseconds) {
		String s = UtcTimestampConverter.convert(value, includeMilliseconds);
		setField(new StringField(field, s));
	}

	public void setUtcTimeStamp(String field, String value,
			boolean includeMilliseconds) {
		setField(new StringField(field, value));
	}

	public void setUtcTimeOnly(String field, Date value) {
		setUtcTimeOnly(field, value, false);
	}

	public void setUtcTimeOnly(String field, String value) {
		setUtcTimeOnly(field, value, false);
	}

	public void setUtcTimeOnly(String field, Date value,
			boolean includeMillseconds) {
		String s = UtcTimeOnlyConverter.convert(value, includeMillseconds);
		setField(new StringField(field, s));
	}

	public void setUtcTimeOnly(String field, String value,
			boolean includeMillseconds) {
		setField(new StringField(field, value));
	}

	public void setUtcDateOnly(String field, Date value) {
		String s = UtcDateOnlyConverter.convert(value);
		setField(new StringField(field, s));
	}

	public void setUtcDateOnly(String field, String value) {
		setField(new StringField(field, value));
	}

	public String getString(String field) throws FieldNotFound {
		return ((String) getField(field).getObject()).toString();
	}

	public StringField getField(String field) throws FieldNotFound {
		StringField f = (StringField) this.fields.get(field);
		if (f == null) {
			throw new FieldNotFound(field);
		}
		return f;
	}

	protected Field<?> getField(String field, Field<?> defaultValue) {
		Field<?> f = this.fields.get(field);
		if (f == null) {
			return defaultValue;
		}
		return f;
	}

	public boolean getBoolean(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			return BooleanConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}

	}

	public int getInt(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			return IntConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}
	}

	public String getUtcTimeStamp(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			UtcTimestampConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}
		return value;
	}

	public String getUtcTimeOnly(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			UtcTimeOnlyConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}
		return value;
	}

	public String getUtcDateOnly(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			UtcDateOnlyConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}
		return value;
	}

	public void setField(String key, Field<?> field) {
		this.fields.put(key, field);
	}

	public void setField(StringField field) {
		if (field.getValue() == null) {
			throw new NullPointerException("Null field values are not allowed.");
		}
		this.fields.put(field.getField(), field);
	}

	public void setField(BooleanField field) {
		setBoolean(field.getField(), field.getValue());
	}

	public void setField(IntField field) {
		setInt(field.getField(), field.getValue());
	}

	public void setField(UtcTimeStampField field) {
		setUtcTimeStamp(field.getField(), field.getValue(), field
				.showMilliseconds());
	}

	public void setField(EsbDateOnlyField field) {
		setEsbDateOnly(field.getField(), field.getValue());
	}

	public void setField(UtcTimeOnlyField field) {
		setUtcTimeOnly(field.getField(), field.getValue(), field
				.showMilliseconds());
	}

	public void setField(UtcDateOnlyField field) {
		setUtcDateOnly(field.getField(), field.getValue());
	}

	public StringField getField(StringField field) throws FieldNotFound {
		return (StringField) getFieldInternal(field);
	}

	private Field<String> getFieldInternal(Field<String> field)
			throws FieldNotFound {
		field.setObject((String) getField(field.getField()).getObject());
		return field;
	}

	public void setEsbDateOnly(String field, String value) {
		setField(new StringField(field, value));
	}

	public void setField(EsbTimeOnlyField field) {
		setEsbTimeOnly(field.getField(), field.getValue(), field
				.showMilliseconds());
	}

	public void setEsbTimeOnly(String field, String value,
			boolean includeMillseconds) {
		setField(new StringField(field, value));
	}

	public EsbTimeOnlyField getField(EsbTimeOnlyField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			UtcTimeOnlyConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (EsbTimeOnlyField) getFieldInternal(field);
	}

	public EsbDateOnlyField getField(EsbDateOnlyField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			EsbDateOnlyConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (EsbDateOnlyField) getFieldInternal(field);
	}

	public BooleanField getField(BooleanField field) throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			field.setObject(new Boolean(BooleanConverter.convert(value)));
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return field;
	}

	public IntField getField(IntField field) throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			field.setObject(new Integer(IntConverter.convert(value)));
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return field;
	}

	public UtcTimeStampField getField(UtcTimeStampField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			UtcTimestampConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (UtcTimeStampField) getFieldInternal(field);
	}

	public UtcTimeOnlyField getField(UtcTimeOnlyField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			UtcTimeOnlyConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (UtcTimeOnlyField) getFieldInternal(field);
	}

	public UtcDateOnlyField getField(UtcDateOnlyField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			UtcDateOnlyConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (UtcDateOnlyField) getFieldInternal(field);
	}

	public boolean isSetField(String field) {
		return this.fields.containsKey(field);
	}

	public boolean isSetField(Field<?> field) {
		return isSetField(field.getField());
	}

	public void removeField(String field) {
		this.fields.remove(String.valueOf(field));
	}

	public Iterator<Field<?>> iterator() {
		return this.fields.values().iterator();
	}

	private boolean isOrderedField(String field, String[] afieldOrder) {
		if (afieldOrder != null) {
			for (int i = 0; i < afieldOrder.length; i++) {
				if (field == afieldOrder[i]) {
					return true;
				}
			}
		}
		return false;
	}

	public String getLocalMktTimeStamp(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			LocalMktTimeStampConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}
		return value;
	}

	public String getLocalMktTime(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			LocalMktTimeConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}
		return value;
	}

	public String getLocalMktDate(String field) throws FieldNotFound {
		String value = getField(field).getValue();
		try {
			LocalMktDateConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field);
		}
		return value;
	}

	public LocalMktTimeStampField getField(LocalMktTimeStampField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			LocalMktTimeStampConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (LocalMktTimeStampField) getFieldInternal(field);
	}

	public LocalMktTimeField getField(LocalMktTimeField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			LocalMktTimeConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (LocalMktTimeField) getFieldInternal(field);
	}

	public LocalMktDateField getField(LocalMktDateField field)
			throws FieldNotFound {
		try {
			String value = getField(field.getField()).getValue();
			LocalMktDateConverter.convert(value);
		} catch (FieldConvertError e) {
			throw new FieldNotFound(field.getField());
		} catch (FieldNotFound e) {
			throw e;
		}
		return (LocalMktDateField) getFieldInternal(field);
	}

	public void setField(LocalMktTimeStampField field) {
		setLocalMktTimeStamp(field.getField(), field.getValue(), field
				.showMilliseconds());
	}

	public void setField(LocalMktTimeField field) {
		setLocalMktTime(field.getField(), field.getValue(), field
				.showMilliseconds());
	}

	public void setField(LocalMktDateField field) {
		setLocalMktDate(field.getField(), field.getValue());
	}

	public void setLocalMktTimeStamp(String field, Date value) {
		setLocalMktTimeStamp(field, value, false);
	}

	public void setLocalMktTimeStamp(String field, String value) {
		setLocalMktTimeStamp(field, value, false);
	}

	public void setLocalMktTimeStamp(String field, Date value,
			boolean includeMilliseconds) {
		String s = LocalMktTimeStampConverter.convert(value,
				includeMilliseconds);
		setField(new StringField(field, s));
	}

	public void setLocalMktTimeStamp(String field, String value,
			boolean includeMilliseconds) {
		setField(new StringField(field, value));
	}

	public void setLocalMktTime(String field, Date value) {
		setLocalMktTime(field, value, false);
	}

	public void setLocalMktTime(String field, String value) {
		setLocalMktTime(field, value, false);
	}

	public void setLocalMktTime(String field, Date value,
			boolean includeMilliseconds) {
		String s = LocalMktTimeConverter.convert(value, includeMilliseconds);
		setField(new StringField(field, s));
	}

	public void setLocalMktTime(String field, String value,
			boolean includeMilliseconds) {
		setField(new StringField(field, value));
	}

	public void setLocalMktDate(String field, Date value) {
		String s = LocalMktDateConverter.convert(value);
		setField(new StringField(field, s));
	}

	public void setLocalMktDate(String field, String value) {
		setField(new StringField(field, value));
	}

	int getGroupCount(String tag) {
		return getGroups(tag).size();
	}

	public Iterator<String> groupKeyIterator() {
		return this.groups.keySet().iterator();
	}

	Map<String, List<Group>> getGroups() {
		return this.groups;
	}

	public void addGroup(Group group) {
		String countTag = group.getFieldTag();
		List<Group> currentGroups = getGroups(countTag);
		currentGroups.add(new Group(group));
		setGroupCount(countTag, currentGroups.size());
	}

	protected void setGroupCount(String countTag, int groupSize) {
		try {
			StringField count;
			if (groupSize == 1) {
				count = new StringField(countTag, "1");
				setField(countTag, count);
			} else {
				count = getField(countTag);
			}
			count.setValue(Integer.toString(groupSize));
		} catch (FieldNotFound e) {
			e.printStackTrace();
		}
	}

	public List<Group> getGroups(String field) {
		List<Group> groupList = this.groups.get(field);
		if (groupList == null) {
			groupList = new ArrayList<Group>();
			this.groups.put(field, groupList);
		}
		return groupList;
	}

	public Group getGroup(int num, Group group) throws FieldNotFound {
		List<?> groupList = getGroups(group.getFieldTag());
		if (num > groupList.size()) {
			throw new FieldNotFound(group.getFieldTag() + ", index=" + num);
		}
		group.setFields((Group) groupList.get(num - 1));
		group.setGroups((Group) groupList.get(num - 1));
		return group;
	}

	public void replaceGroup(int num, Group group) {
		int offset = num - 1;
		List<Group> groupList = getGroups(group.getFieldTag());

		groupList.set(offset, new Group(group));
	}

	public void removeGroup(String field) {
		getGroups(field).clear();
		removeField(field);

		this.groups.remove(field);
	}

	public void removeGroup(int num, String field) {
		List<?> groupList = getGroups(field);

		groupList.remove(num - 1);

		setGroupCount(field, groupList.size());

		if (groupList.size() == 0) {
			removeGroup(field);}
	}

	public void removeGroup(int num, Group group) {
		removeGroup(num, group.getFieldTag());
	}

	public void removeGroup(Group group) {
		removeGroup(group.getFieldTag());
	}

	public boolean hasGroup(String field) {
		return this.groups.containsKey(field);
	}

	public boolean hasGroup(int num, String field) {
		return (hasGroup(field)) && (num <= getGroups(field).size());
	}

	public boolean hasGroup(int num, Group group) {
		return hasGroup(num, group.getFieldTag());
	}

	public boolean hasGroup(Group group) {
		return hasGroup(group.getFieldTag());
	}

	public void toXml(StringBuffer buffer) {

		for (int i = 0; i < this.fieldOrder.length; i++) {
			Field<?> field = this.fields.get(this.fieldOrder[i]);
			if (null != field) {
				String tag = field.getField();
				if ((isOrderedField(tag, this.fieldOrder))
						&& (!isGroupField(tag))) {
					buffer.append(field.toXmlString());
					if (!"".equals(field.toXmlString())) {
						buffer.append('\r');}
				} else {
					if ((!isGroupField(tag))
							|| (!isOrderedField(tag, this.fieldOrder))) {
						continue;
					}
					List<?> groups = getGroups(tag);
					for (int j = 0; j < groups.size(); j++) {
						Group groupFields = (Group) groups.get(j);
						buffer.append("<").append(groupFields.getFieldTag())
								.append(">").append('\r');
						groupFields.toXml(buffer);
						buffer.append("</").append(groupFields.getFieldTag())
								.append(">").append('\r');
					}
				}
			}

		}

		for (Iterator<?> iter = this.groups.entrySet().iterator(); iter.hasNext();) 
		{
			Map.Entry<?,?> entry = (Map.Entry<?,?>) iter.next();
			String groupCountTag = (String) entry.getKey();
			if ((!isOrderedField(groupCountTag, this.fieldOrder))
					|| (!isSetField(groupCountTag))) 
			{
				List<?> groups = (List<?>) entry.getValue();
				/*
				 * try { IntField countField = new IntField(groupCountTag,
				 * getInt(groupCountTag)); buffer.append(countField.toXml());
				 * buffer.append('\r'); } catch (FieldNotFound
				 * localFieldNotFound) { }
				 */
				for (int j = 0; j < groups.size(); j++) {
					Group groupFields = (Group) groups.get(j);
					buffer.append("<").append(groupFields.getFieldTag())
							.append(">").append('\r');
					groupFields.toXml(buffer);
					buffer.append("</").append(groupFields.getFieldTag())
							.append(">").append('\r');
				}
			}
		}
	}
}
