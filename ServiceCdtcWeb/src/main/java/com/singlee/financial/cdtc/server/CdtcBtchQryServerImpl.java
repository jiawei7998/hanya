package com.singlee.financial.cdtc.server;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.remoting.RemoteConnectFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cdc.dmsg.client.DClient;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.bean.CdtcZLSecurity;
import com.singlee.financial.bean.ManualSettleDetail;
import com.singlee.financial.cdtc.CdtcBtchQryServer;
import com.singlee.financial.cdtc.bean.BtchQryRsltBean;
import com.singlee.financial.cdtc.cnbondpackage.CdtcBankAccount;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSysExceptionAdviceV01;
import com.singlee.financial.cdtc.dao.BranprcdateGetDao;
import com.singlee.financial.cdtc.dao.CdtcRequestMessageCreateDao;
import com.singlee.financial.cdtc.httpclient.XmlFormat;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import com.singlee.financial.cdtc.pac.BondSettlementType2Code;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.CommonDistributionSettlementStatusReport;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.InstructionConfirmIndicatorCode;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;
import com.singlee.financial.cdtc.pac.InstructionStatusCode;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.util.EncodingUtil;
import com.singlee.financial.cdtc.util.FormatUtil;
import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcBtchQryServerImpl implements CdtcBtchQryServer {

	@Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;

	@Value("#{cdtcProperties['cdtc.batchQueryTime']}")
	private String batchQueryTime ; //本方机构账号
	
	@Autowired
	private CdtcRequestMessageCreateDao cdtcRequestMessageCreateDao;

	@Autowired
	private BranprcdateGetDao branprcdateGetDao;
	
	private Map<String, Date> reportSendTimeMap = new HashMap<String, Date>();

	private Logger logger = LoggerFactory.getLogger(CdtcBtchQryServerImpl.class);

	@Override
	public List<BtchQryRslt> getCtrctBtchQry(Map<String, Object> map) throws RemoteConnectFailureException, Exception {
		// Search Condition
		String sInstrId = ((String) map.get("sInstrId")).trim();
		String sTxId = ((String) map.get("sTxId")).trim();
		String sCtrctSts = ((String) map.get("sCtrctSts")).trim();
		String postdate = (String) map.get("postdate");
		String bizTp = ((String) map.get("bizTp")).trim();
		logger.info("-------------postDate:" + postdate + ",postdate==null||postdate.trim().length()<1" + (postdate == null || postdate.trim().length() < 1));
		if (postdate == null || postdate.trim().length() < 1) {
			postdate = branprcdateGetDao.queryBranchDate();}
		logger.info("-------------实际查询日期postDate【" + postdate + "】--------------------------------------");
		List<BtchQryRslt> btchqList = new ArrayList<BtchQryRslt>();
		logger.info("正在创建指令批量报文,请稍后......");
		SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS03AndBJ0300(CdtcBankAccount.BANK_ACCOUNT, postdate);
		StringBuffer xmlBuffer = new StringBuffer();
		settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);
		logger.info("正在发送指令批量报文,等待CDTC回执,请稍后......" + xmlBuffer.toString());
		DClient dClient = new DClient(wasIp);
		String backXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
		logger.info("回执报文:" + backXml);
		// HashMap<String, String> resultHashMap = dClientDao.sendXmlMessageToCdtcForRequest(XmlFormat.format(xmlBuffer.toString()));
		if (backXml != null) {
			logger.info("指令批量报文发送成功......");
			SAXReader reader = new SAXReader();
			reader.setEncoding(EncodingUtil.getEncoding(backXml));
			Document xmlDoc = reader.read(new ByteArrayInputStream(backXml.getBytes(EncodingUtil.getEncoding(backXml))));
			String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
			if (null != type && "CSBS.006.001.01".equalsIgnoreCase(type)) {
				HashMap<String, BtchQryRsltBean> btchqHashMap = new HashMap<String, BtchQryRsltBean>();
				logger.info("正在创建合同批量报文,请稍后......");
				SettlementBusinessQueryRequest settlementBusinessQueryRequest2 = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS03AndBJ0301(CdtcBankAccount.BANK_ACCOUNT, postdate);
				StringBuffer xmlBuffer2 = new StringBuffer();
				settlementBusinessQueryRequest2.toXmlForSuperParent(xmlBuffer2);
				logger.info("正在发送合同批量报文,等待CDTC回执,请稍后......\n\r\t" + xmlBuffer2.toString());
				String backXml2 = dClient.sendMsg(XmlFormat.format(xmlBuffer2.toString()));
				logger.info("返回的报文内容为:" + backXml2);
				if (backXml2 != null) {
					logger.info("合同批量报文发送成功...... 编码格式:" + EncodingUtil.getEncoding(backXml2));
					reader.setEncoding(EncodingUtil.getEncoding(backXml2));
					Document xmlDoc2 = reader.read(new ByteArrayInputStream(backXml2.getBytes(EncodingUtil.getEncoding(backXml2))));
					String type2 = xmlDoc2.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
					List<BtchQryRsltBean> btchqList2 = new ArrayList<BtchQryRsltBean>();

					if (null != type2 && "CSBS.006.001.01".equalsIgnoreCase(type2)) {
						logger.info("返回合同列表......");
						btchqList2 = cdtcRequestMessageCreateDao.transformatCSBS00600101ForQryCondConditionOS03AndBJ0301(backXml2);
						for (BtchQryRsltBean btchQryRsltBean : btchqList2) {
							btchqHashMap.put(btchQryRsltBean.getInstrId().trim(), btchQryRsltBean);
						}

					}
				}
				btchqList = cdtcRequestMessageCreateDao.transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(backXml);
				logger.info("反解析出批量业务查询返回的业务数据......");
				final LinkedHashMap<Integer, BtchQryRslt> cdtcHashMap = new LinkedHashMap<Integer, BtchQryRslt>();
				int count = 0;
				for (int i = 0; i < btchqList.size(); i++) {
					BtchQryRslt btchQryRslt = btchqList.get(i);
					if (btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT19.getType()) || btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT17.getType())
							|| btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT18.getType()) || btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT22.getType())
							|| btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT21.getType())) {

					} else {
						btchQryRslt.setCtrctId(btchqHashMap.get(btchQryRslt.getInstrId().trim()) == null ? "" : btchqHashMap.get(btchQryRslt.getInstrId().trim()).getCtrctId());
						btchQryRslt.setCtrctSts(btchqHashMap.get(btchQryRslt.getInstrId().trim()) == null ? "" : btchqHashMap.get(btchQryRslt.getInstrId().trim()).getCtrctSts());
						cdtcHashMap.put(count, btchQryRslt);
						count++;
					}
				}

				// Filter Data
				Map<String, String> sCtrctStsMap = null;
				if (sCtrctSts.trim().length() > 1) {
					sCtrctStsMap = new HashMap<String, String>();
					for (String str : sCtrctSts.split("\\,")) {
						sCtrctStsMap.put(str, str);
					}
				} else {
					sCtrctSts = null;
				}

				Map<String, String> bizTpMap = null;
				if (bizTp.length() > 1) {
					bizTpMap = new HashMap<String, String>();
					for (String str : bizTp.split("\\,")) {
						bizTpMap.put(str, str);
					}
				} else {
					bizTp = null;
				}
				List<BtchQryRslt> tmpBtchqList = new ArrayList<BtchQryRslt>();
				for (BtchQryRslt obj : btchqList) {
					if (!"".equals(sInstrId)) {
						if (obj.getInstrId() == null || obj.getInstrId().trim().length() == 0 || !obj.getInstrId().contains(sInstrId)) {
							continue;}
					}
					if (!"".equals(sTxId)) {
						if (obj.getTxId() == null || obj.getTxId().trim().length() == 0 || !obj.getTxId().contains(sTxId)) {
							continue;}
					}
					if (sCtrctSts != null) {
						if (sCtrctStsMap.get(obj.getCtrctSts()) == null) {
							continue;}
					}
					if (bizTp != null) {
						if (bizTpMap.get(obj.getBizTp()) == null) {
							continue;}
					}
					tmpBtchqList.add(obj);
				}
				btchqList = tmpBtchqList;
			} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) {
				logger.info("系统异常通知......");
				SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
				spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
				spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
				SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
				if (spotSettlementInstruction.hasGroup("Document")) {
					List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
					for (Group group : groupsList) {
						StringBuffer xmlBufferT = new StringBuffer();
						group.toXmlForParent(xmlBufferT);
						System.out.println(xmlBufferT);
						systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
						Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes(reader.getEncoding()))));
						XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
					}
				}
				if (null != systemExceptionAdviceV01) {
					String XcptnRtrCd = "";
					String XcptnRtrTx = "";
					if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
						XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
						XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
						System.out.println(XcptnRtrCd + "" + XcptnRtrTx);
					}
					throw new RuntimeException("【CSBS.009.001.01系统异常通知:" + XcptnRtrCd + "," + XcptnRtrTx + "】");
				}
			}
		}
		return btchqList;
	}

	@Override
	public List<BtchQryRslt> getSettleQueryManage(Map<String, Object> map) throws RemoteConnectFailureException, Exception {

		logger.info("中央债券结算综合业务查询交互，开始");
		List<BtchQryRslt> listLast = new ArrayList<BtchQryRslt>();

		try {
			String dataTime = (String) map.get("dataTime");

			SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS03AndBJ0300(CdtcBankAccount.BANK_ACCOUNT, dataTime);
			StringBuffer xmlBuffer = new StringBuffer();
			settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);

			logger.info("正在发送报文,等待CDTC回执,请稍后......" + xmlBuffer.toString());
			DClient dClient = new DClient(wasIp);
			String backXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
			// final HashMap<String, String> resultHashMap = dClientDao.sendXmlMessageToCdtcForRequest(XmlFormat.format(xmlBuffer.toString()));
			if (backXml != null) {
				SAXReader reader = new SAXReader();

				// LogManager.getLogger(LogManager.MODEL_CDTCQUERY).info("------------------");
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
				if (null != type && "CSBS.006.001.01".equalsIgnoreCase(type)) {
					logger.info("解析回执报文,展现数据,请稍后......");

					// SwingUtilities.invokeLater(new Runnable(){
					// @Override
					// public void run() {
					try {
						java.util.List<BtchQryRslt> btchqList = cdtcRequestMessageCreateDao.transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(backXml);

						final LinkedHashMap<Integer, BtchQryRslt> cdtcHashMap = new LinkedHashMap<Integer, BtchQryRslt>();
						int count = 0;
						for (int i = 0; i < btchqList.size(); i++) {
							BtchQryRslt btchQryRslt = btchqList.get(i);
							btchQryRslt.setStdt(btchQryRslt.getDt1());

							// TODO 有个dataTime需要修改
							// if(!btchQryRslt.getDt1().equals(dataTime) || !btchQryRslt.getInstrSts().equals("IS03")||btchQryRsltDao.isExistBtchQryRslt(btchQryRslt)) {
							if (!btchQryRslt.getDt1().equals(dataTime) || !"IS03".equals(btchQryRslt.getInstrSts())) {

							} else {
								/**
								 * 第三方 进行是否还需要确认的过滤 譬如江苏银行是付券方 那么 已经确认的话 就不需要再次进行确认了。 如果是 柜台那么只有江苏银行在付券方才需要确认
								 * 
								 */
								listLast.add(btchQryRslt);// 增加数据
								if (btchQryRslt.getInstrOrgn().equalsIgnoreCase(InstructionOriginCode.Type.IO01.getType()) && btchQryRslt.getTakAcctId().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)
										&& !btchQryRslt.getOrgtrCnfrmInd().equalsIgnoreCase(InstructionConfirmIndicatorCode.Type.IC01.getType())) {
									cdtcHashMap.put(count, btchQryRslt);
									count++;
								}
								if (btchQryRslt.getInstrOrgn().equalsIgnoreCase(InstructionOriginCode.Type.IO03.getType())) {
									if (btchQryRslt.getGivAcctId().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)
											&& !btchQryRslt.getOrgtrCnfrmInd().equalsIgnoreCase(InstructionConfirmIndicatorCode.Type.IC01.getType())) {
										cdtcHashMap.put(count, btchQryRslt);
										count++;
									}
									if (btchQryRslt.getTakAcctId().equalsIgnoreCase(CdtcBankAccount.BANK_ACCOUNT)
											&& !btchQryRslt.getCtrCnfrmInd().equalsIgnoreCase(InstructionConfirmIndicatorCode.Type.IC01.getType())) {
										cdtcHashMap.put(count, btchQryRslt);
										count++;
									}
								}
							}
						}
						if (count <= 0) {
							for (int i = 0; i <= 14; i++) {
								cdtcHashMap.put(i, new BtchQryRslt());
							}
						}

						/*
						 * cdtcSecuritySettlHandleOperTableModel = new CdtcSecuritySettlHandleOperTableModel<BtchQryRslt>(cdtcHashMap); table.setModel(cdtcSecuritySettlHandleOperTableModel); if(count
						 * > 0) { table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); // 创建一个对象，并将要管理的table传入 tablesCheckBoxMgr = new TableCheckboxManager(table); // 设置table中checkbox的数量
						 * tablesCheckBoxMgr.addCheckBoxs(cdtcHashMap.size()); // 设置表头checkbox的列 tablesCheckBoxMgr.setHeaderShowCheckbox(0); }else {
						 * table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); }
						 * 
						 * // 添加序号列 DefaultListModel lmodel = new DefaultListModel(); for (int i = 1; i <= table.getModel().getRowCount(); i++) { lmodel.addElement(i); }
						 * 
						 * rowHeader = new JList(lmodel); rowHeader.setFixedCellWidth(30); rowHeader.setFixedCellHeight(table.getRowHeight()); rowHeader.setCellRenderer(new
						 * com.singlee.financial.ui.util.splitpage.SingleeSplitPageToolBarRowHeaderRenderer(table)); scrollPane.setRowHeaderView(rowHeader);
						 * com.singlee.financial.ui.util.splitpage.TableUtil2.setTableColumnAlign(table, new int[]{11,12,13}, "r"); TableColumnsUtil.FitTableColumns(table);
						 */
					} catch (Exception e) {
						e.printStackTrace();
					}
					// }
					// });
				} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) {
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if (spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for (Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
						}
					}
					if (null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";
						String XcptnRtrTx = "";
						if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
						}
						logger.error("消息错误码[" + XcptnRtrCd + "]\r\n错误信息[" + XcptnRtrTx + "]");
						Thread.sleep(10000);
						throw new RuntimeException("消息错误码[" + XcptnRtrCd + "]\r\n错误信息[" + XcptnRtrTx + "]");
					}
				}
			}

			logger.info("中央债券结算综合业务查询交互，成功，结束");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("中央债券结算综合业务查询,失败");
		}

		return listLast;
	}

	@Override
	public ManualSettleDetail getCtrctDetails(Map<String, Object> map) {
		logger.info("中央债券结算信息合同详情正在查询..............\r\n");
		String qryCondId = (String) map.get("qryCondId");
		if (qryCondId == null) {
			throw new RuntimeException("查询合同详情，合同id不能为空!");}
		try {
			String txFlowId = new SimpleDateFormat("yyyyMMdd").format(new Date()) + CdtcBankAccount.BANK_ACCOUNT + DateUtil.convertDateToYYYYMMDDHHMMSS(new Date());
			SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS04AndBJ0300(txFlowId, CdtcBankAccount.BANK_ACCOUNT,
					qryCondId);
			StringBuffer xmlBuffer = new StringBuffer();
			settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);
			logger.info("中央债券结算合同详情查询交互, 正在向CDTC查询指令详情数据,请稍候......发送报文:\r\n" + xmlBuffer);
			DClient dClient = new DClient(wasIp);
			String xmlBack = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
			if (xmlBack != null) {
				logger.info("查询指令详情成功,报文内容:\r\n" + xmlBack);
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(xmlBack).getBytes("UTF-8")));
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
				if (null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {
					logger.info("解析回执报文,展现数据,请稍后......");
					String bizTp = xmlDoc.getRootElement().element("Document").element("SttlmDtl").element("BizTp").getText();
					ManualSettleDetail manualSettleDetail = new ManualSettleDetail();
					if (null == bizTp) {
						throw new RuntimeException("查询合同报文,返回业务类型为空!!!");}
					if ("BT01".equalsIgnoreCase(bizTp) || "BT00".equalsIgnoreCase(bizTp) || "BT04".equalsIgnoreCase(bizTp) || "BT05".equalsIgnoreCase(bizTp)) {
						SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
						CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
						spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
						spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);

						if (spotSettlementInstruction.hasGroup("Document")) {
							logger.info("hasGroup(Document)");
							List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
							for (Group group : groupsList) {
								// 保存xml文件
								StringBuffer xmlBuffer1 = new StringBuffer();
								group.toXmlForParent(xmlBuffer1);
								commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
								Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBuffer1.toString() + "</Document>").getBytes("UTF-8"))));
								CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
							}
						}

						if (null != commonDistributionSettlementStatusReport) {
							if (commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
								Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
								String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
								// 指令处理状态
								manualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
								manualSettleDetail.setInstrStscode(instrSts);
								Group instrStsInf = stsFlg.hasGroup("InstrStsInf") ? stsFlg.getGroups("InstrStsInf").get(0) : null;
								if (null != instrStsInf) {
									// 发令方确认标识
									manualSettleDetail.setOrgtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd") ? InstructionConfirmIndicatorCode.Type.getName(instrStsInf
											.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString()) : "未确认");
									manualSettleDetail.setOrgtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString());
									// 对手方确认标识
									manualSettleDetail.setCtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd") ? InstructionConfirmIndicatorCode.Type.getName(instrStsInf
											.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString()) : "未确认");
									manualSettleDetail.setCtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString());
									// 处理结果状态
									manualSettleDetail.setTxRsltCd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd") ? instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject()
											.toString() : " ");
								}
							}

							if (commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
								Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
								manualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
								manualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
								manualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
								manualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
								manualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
								manualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
								manualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
								manualSettleDetail.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
								manualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2") ? FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2")
										.getObject().toString())) : "");
								manualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
								manualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId") ? sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString() : "");
								List<CdtcZLSecurity> secList = new ArrayList<CdtcZLSecurity>();

								if (sttlmDtl.hasGroup("Bd1")) {
									Group bd1 = sttlmDtl.getGroups("Bd1").get(0);
									CdtcZLSecurity cdtcZLSecurity = new CdtcZLSecurity();
									cdtcZLSecurity.setSecid(bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
									logger.info("Secid:" + bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
									cdtcZLSecurity.setSecidNm(bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
									logger.info("SecidNm:" + bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
									cdtcZLSecurity.setFaceAmt(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
									logger.info("FaceAmt:" + new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
									manualSettleDetail.setBdAmt(FormatUtil.formatBigDecimal(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString())));
									cdtcZLSecurity.setAccrualAmt(new BigDecimal(bd1.getGroups("AcrdIntrstInf").get(0).getfieldsForFieldMap().get("AcrdIntrst").getObject().toString()));
									logger.info("AccrualAmt:" + new BigDecimal(bd1.getGroups("AcrdIntrstInf").get(0).getfieldsForFieldMap().get("AcrdIntrst").getObject().toString()));
									cdtcZLSecurity.setAccrualTotalAmt(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val3").getObject().toString()));
									logger.info("AccrualTotalAmt:" + new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val3").getObject().toString()));
									manualSettleDetail.setPric1(bd1.getfieldsForFieldMap().get("Pric1").getObject().toString());
									manualSettleDetail.setPric2(bd1.getfieldsForFieldMap().containsKey("Pric2") ? bd1.getfieldsForFieldMap().get("Pric2").getObject().toString() : "");
									secList.add(cdtcZLSecurity);
									manualSettleDetail.setSecList(secList);
								}
								logger.info("secList size :" + secList.size());
								manualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm") ? sttlmDtl.getGroups("Oprtr")
										.get(0).getfieldsForFieldMap().get("Nm").getObject().toString() : "");
								manualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm") ? sttlmDtl.getGroups("Chckr")
										.get(0).getfieldsForFieldMap().get("Nm").getObject().toString() : "");
							}
						}

					}
					if ("BT02".equalsIgnoreCase(bizTp)) {
						SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
						CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
						spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
						spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
						if (spotSettlementInstruction.hasGroup("Document")) {
							logger.info("hasGroup(Document)");
							List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
							for (Group group : groupsList) {
								// 保存xml文件
								StringBuffer xmlBuffer1 = new StringBuffer();
								group.toXmlForParent(xmlBuffer1);
								logger.info("xmlBuffer:" + xmlBuffer1.toString());
								commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
								Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBuffer1.toString() + "</Document>").getBytes("UTF-8"))));
								CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
							}
						}
						if (null != commonDistributionSettlementStatusReport) {
							if (commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
								Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
								String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
								// 指令处理状态
								manualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
								manualSettleDetail.setInstrStscode(instrSts);
								Group instrStsInf = stsFlg.hasGroup("InstrStsInf") ? stsFlg.getGroups("InstrStsInf").get(0) : null;
								if (null != instrStsInf) {
									// 发令方确认标识
									manualSettleDetail.setOrgtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd") ? InstructionConfirmIndicatorCode.Type.getName(instrStsInf
											.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString()) : "未确认");
									manualSettleDetail.setOrgtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString());
									// 对手方确认标识
									manualSettleDetail.setCtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd") ? InstructionConfirmIndicatorCode.Type.getName(instrStsInf
											.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString()) : "未确认");
									manualSettleDetail.setCtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString());
									// 处理结果状态
									manualSettleDetail.setTxRsltCd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd") ? instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject()
											.toString() : " ");
								}
							}
							if (commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
								Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
								if ("BT02".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())) {// 质押式
									manualSettleDetail.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
									manualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
									manualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
									manualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
									manualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
									manualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
									manualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
									manualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
									manualSettleDetail.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
									manualSettleDetail.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
									manualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2") ? FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap()
											.get("Val2").getObject().toString())) : "");
									manualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
									manualSettleDetail.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
									manualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId") ? sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString() : "");
									manualSettleDetail.setAggtFaceAmt(FormatUtil.formatBigDecimal4(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("AggtFaceAmt").getObject().toString())));
									manualSettleDetail.setRate(sttlmDtl.getfieldsForFieldMap().get("Rate").getObject().toString());

									List<CdtcZLSecurity> secList = new ArrayList<CdtcZLSecurity>();
									if (sttlmDtl.hasGroup("Bd1")) {
										List<Group> groupsList = sttlmDtl.getGroups("Bd1");
										for (Group bd1 : groupsList) {
											CdtcZLSecurity cdtcZLSecurity = new CdtcZLSecurity();
											cdtcZLSecurity.setSecid(bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
											cdtcZLSecurity.setSecidNm(bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
											cdtcZLSecurity.setFaceAmt(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
											secList.add(cdtcZLSecurity);
											logger.info("Secid:" + bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
											logger.info("SecidNm:" + bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
											logger.info("FaceAmt:" + new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));

										}
										manualSettleDetail.setSecList(secList);
									}
									logger.info("secList size :" + secList.size());

									manualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm") ? sttlmDtl.getGroups("Oprtr")
											.get(0).getfieldsForFieldMap().get("Nm").getObject().toString() : "");
									manualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm") ? sttlmDtl.getGroups("Chckr")
											.get(0).getfieldsForFieldMap().get("Nm").getObject().toString() : "");
								}
							}
						}
					}
					if ("BT03".equalsIgnoreCase(bizTp)) {

						SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
						CommonDistributionSettlementStatusReport commonDistributionSettlementStatusReport = null;
						spotSettlementInstruction.unionHashMap(CommonDistributionSettlementStatusReport.type);
						spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
						if (spotSettlementInstruction.hasGroup("Document")) {
							logger.info("hasGroup(Document)");
							List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
							for (Group group : groupsList) {
								// 保存xml文件
								StringBuffer xmlBuffer1 = new StringBuffer();
								group.toXmlForParent(xmlBuffer1);
								logger.info("xmlBuffer:" + xmlBuffer1.toString());
								commonDistributionSettlementStatusReport = new CommonDistributionSettlementStatusReport("Document");
								Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBuffer1.toString() + "</Document>").getBytes("UTF-8"))));
								CommonDistributionSettlementStatusReport.getElementList(document.getRootElement(), commonDistributionSettlementStatusReport);
							}
						}
						if (null != commonDistributionSettlementStatusReport) {
							if (commonDistributionSettlementStatusReport.hasGroup("StsFlg")) {
								Group stsFlg = commonDistributionSettlementStatusReport.getGroups("StsFlg").get(0);
								String instrSts = stsFlg.getGroups("InstrStsInf").get(0).getfieldsForFieldMap().get("InstrSts").getObject().toString();
								// 指令处理状态
								manualSettleDetail.setInstrSts(InstructionStatusCode.Type.getName(instrSts));
								manualSettleDetail.setInstrStscode(instrSts);
								Group instrStsInf = stsFlg.hasGroup("InstrStsInf") ? stsFlg.getGroups("InstrStsInf").get(0) : null;
								if (null != instrStsInf) {
									// 发令方确认标识
									manualSettleDetail.setOrgtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("OrgtrCnfrmInd") ? InstructionConfirmIndicatorCode.Type.getName(instrStsInf
											.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString()) : "未确认");
									manualSettleDetail.setOrgtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("OrgtrCnfrmInd").getObject().toString());
									// 对手方确认标识
									manualSettleDetail.setCtrCnfrmInd(instrStsInf.getfieldsForFieldMap().containsKey("CtrCnfrmInd") ? InstructionConfirmIndicatorCode.Type.getName(instrStsInf
											.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString()) : "未确认");
									manualSettleDetail.setCtrCnfrmIndcode(instrStsInf.getfieldsForFieldMap().get("CtrCnfrmInd").getObject().toString());
									// 处理结果状态
									manualSettleDetail.setTxRsltCd(instrStsInf.getfieldsForFieldMap().containsKey("TxRsltCd") ? instrStsInf.getfieldsForFieldMap().get("TxRsltCd").getObject()
											.toString() : " ");
								}
							}
							if (commonDistributionSettlementStatusReport.hasGroup("SttlmDtl")) {
								Group sttlmDtl = commonDistributionSettlementStatusReport.getGroups("SttlmDtl").get(0);
								if ("BT03".equalsIgnoreCase(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString())) {// 买断式
									manualSettleDetail.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
									manualSettleDetail.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
									manualSettleDetail.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
									manualSettleDetail.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
									manualSettleDetail.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
									manualSettleDetail.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
									manualSettleDetail.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
									manualSettleDetail.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
									manualSettleDetail.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
									manualSettleDetail.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
									manualSettleDetail.setVal2(sttlmDtl.getfieldsForFieldMap().containsKey("Val2") ? FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap()
											.get("Val2").getObject().toString())) : "");
									manualSettleDetail.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
									manualSettleDetail.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
									manualSettleDetail.setTxId(sttlmDtl.getfieldsForFieldMap().containsKey("TxId") ? sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString() : "");
									List<CdtcZLSecurity> secList = new ArrayList<CdtcZLSecurity>();
									if (sttlmDtl.hasGroup("Bd1")) {
										List<Group> groupsList = sttlmDtl.getGroups("Bd1");
										for (Group bd1 : groupsList) {
											CdtcZLSecurity cdtcZLSecurity = new CdtcZLSecurity();
											cdtcZLSecurity.setSecid(bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
											cdtcZLSecurity.setSecidNm(bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
											cdtcZLSecurity.setFaceAmt(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));
											secList.add(cdtcZLSecurity);
											logger.info("Secid:" + bd1.getfieldsForFieldMap().get("BdId").getObject().toString());
											logger.info("SecidNm:" + bd1.getfieldsForFieldMap().get("BdShrtNm").getObject().toString());
											logger.info("FaceAmt:" + new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString()));

										}
										manualSettleDetail.setSecList(secList);
									}
									logger.info("secList size :" + secList.size());
									if (sttlmDtl.hasGroup("SttlmGrte1")) {
										/**
										 * 按实例进行解析
										 */
									} else {
										manualSettleDetail.setGrteTp("无");
									}
									manualSettleDetail.setOprtr(sttlmDtl.hasGroup("Oprtr") && sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().containsKey("Nm") ? sttlmDtl.getGroups("Oprtr")
											.get(0).getfieldsForFieldMap().get("Nm").getObject().toString() : "");
									manualSettleDetail.setChckr(sttlmDtl.hasGroup("Chckr") && sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().containsKey("Nm") ? sttlmDtl.getGroups("Chckr")
											.get(0).getfieldsForFieldMap().get("Nm").getObject().toString() : "");
								}
							}
						}

					}

					// 合同查询
					String ctrctId = (String) map.get("ctrctId");
					if (ctrctId != null && ctrctId.trim().length() > 0) {
						SettlementBusinessQueryRequest ctrctBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS04AndBJ0301(CdtcBankAccount.BANK_ACCOUNT, ctrctId);
						StringBuffer ctrctBuffer = new StringBuffer();
						ctrctBusinessQueryRequest.toXmlForSuperParent(ctrctBuffer);
						logger.info("合同详情发送的报文:\r\n" + ctrctBuffer.toString());
						DClient dClient1 = new DClient(wasIp);
						String ctrctxmlBack = dClient1.sendMsg(XmlFormat.format(ctrctBuffer.toString()));
						if (xmlBack != null) {
							logger.info("合同查询成功,返回报文的内容:\r\n" + ctrctxmlBack);
							org.dom4j.Document ctrctXmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(ctrctxmlBack).getBytes("UTF-8")));
							type = ctrctXmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
							if (null != type && "CSBS.002.001.01".equalsIgnoreCase(type)) {// 合同成功
								Element ctrctStsInfElement = ctrctXmlDoc.getRootElement().element("Document").element("StsFlg").element("CtrctStsInf");
								if (null != ctrctStsInfElement) {
									manualSettleDetail.setCtrctId(ctrctStsInfElement.elementText("CtrctId"));
									manualSettleDetail.setCtrctSts(ctrctStsInfElement.elementText("CtrctSts"));
									manualSettleDetail.setCtrctBlckSts(ctrctStsInfElement.elementText("CtrctBlckSts"));
									manualSettleDetail.setCtrctFaildRsn(ctrctStsInfElement.elementText("CtrctFaildRsn"));
									manualSettleDetail.setCtrctUpdate(ctrctStsInfElement.elementText("LastUpdTm"));
									manualSettleDetail.setCtrctQryRsltCd(ctrctStsInfElement.elementText("CtrctQryRsltCd"));
									manualSettleDetail.setCtrctQryRsltInf(ctrctStsInfElement.elementText("CtrctQryRsltInf"));

								}
							} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) { // 系统异常通知
								logger.info("消息类型：系统异常通知[CSBS.009.001.01]");
								SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
								spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
								spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
								SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
								if (spotSettlementInstruction.hasGroup("Document")) {
									List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
									for (Group group : groupsList) {
										StringBuffer xmlBufferT = new StringBuffer();
										group.toXmlForParent(xmlBufferT);
										systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
										Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
										XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
									}
								}
								if (null != systemExceptionAdviceV01) {
									String XcptnRtrCd = "";
									String XcptnRtrTx = "";
									if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
										XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
										XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
									}
									throw new RuntimeException("【CSBS.009.001.01系统异常通知:" + XcptnRtrCd + "," + XcptnRtrTx + "】");
								} else {
									throw new RuntimeException("【CSBS.009.001.01 系统异常:");
								}
							}
						}

					}
					return manualSettleDetail;
				} else if ("CSBS.009.001.01".equalsIgnoreCase(type)) {
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if (spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for (Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>" + xmlBufferT.toString() + "</Document>").getBytes("UTF-8"))));
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
						}
					}
					if (null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";
						String XcptnRtrTx = "";
						if (systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
						}
						throw new RuntimeException("消息错误码[" + XcptnRtrCd + "]\r\n错误信息[" + XcptnRtrTx + "]");
					}
				}
			}
			throw new RuntimeException("查询合同详情未找到相关数据!!!");
		} catch (Exception e) {
			logger.error("查询合同详情报错:" + e.getMessage());
			throw new RuntimeException("查询合同详情报错:" + e.getMessage());
		}
	}
	
	@Override
	public RetBean validCdtcConnectTime(String type) throws Exception {
		return validConnTime(type);
	}
	
	private synchronized RetBean validConnTime(String type)throws Exception{
		RetBean ret = new RetBean();
		Date last = reportSendTimeMap.get(type);
		if(last == null){
			reportSendTimeMap.put(type, new Date());
			
			ret.setRetStatus(RetStatusEnum.S);
			ret.setRetCode("000000");
			ret.setRetMsg("成功");
			
		}else{
			Date curr = new Date();
			long seconds = (curr.getTime() - last.getTime()) / 1000;
			long batchQueryTime_1 = Integer.parseInt(batchQueryTime); 
			if(seconds < batchQueryTime_1){
				ret.setRetStatus(RetStatusEnum.F);
				ret.setRetCode("0000001");
				ret.setRetMsg("距离上次查询时间间隔为["+seconds+"秒],必须大于"+batchQueryTime_1+"秒才能重新查询");
			}else{
				reportSendTimeMap.put(type, new Date());
				
				ret.setRetStatus(RetStatusEnum.S);
				ret.setRetCode("000000");
				ret.setRetMsg("成功");
			}
		}
		return ret;
	}

}
