package com.singlee.financial.cdtc.pac;

public abstract class MessageComponent extends FieldMap
{
	  private static final long serialVersionUID = 1L;

	  protected abstract String[] getFields();

	  protected abstract String[] getGroupFields();

	  protected MessageComponent()
	  {
	  }

	  protected MessageComponent(String[] fieldOrder)
	  {
	    super(fieldOrder);
	  }

	  public void copyFrom(FieldMap fields) {
	    try {
	      String[] componentFields = getFields();
	      for (int i = 0; i < componentFields.length; i++) {
	        if (fields.isSetField(componentFields[i])) {
	          setField(componentFields[i], fields.getField(componentFields[i]));
	        }
	      }
	      String[] groupFields = getGroupFields();
	      for (int i = 0; i < groupFields.length; i++) {
	        if (fields.isSetField(groupFields[i])) {
	          setField(groupFields[i], fields.getField(groupFields[i]));
	          setGroups(groupFields[i], fields.getGroups(groupFields[i]));
	        }
	      }
	    }
	    catch (FieldNotFound localFieldNotFound)
	    {
	    }
	  }

	  public void copyTo(FieldMap fields) {
	    try {
	    	String[] componentFields = getFields();
	      for (int i = 0; i < componentFields.length; i++) {
	        if (isSetField(componentFields[i])) {
	          fields.setField(componentFields[i], getField(componentFields[i]));
	        }
	      }
	      String[] groupFields = getGroupFields();
	      for (int i = 0; i < groupFields.length; i++) {
	        if (isSetField(groupFields[i])) {
	          fields.setField(groupFields[i], getField(groupFields[i]));
	          fields.setGroups(groupFields[i], getGroups(groupFields[i]));
	        }
	      }
	    }
	    catch (FieldNotFound localFieldNotFound)
	    {
	    }
	  }
	}