package com.singlee.financial.cdtc.util;

import java.math.BigDecimal;
import com.singlee.financial.bean.CnbondPkgDetailBean;
import com.singlee.financial.cdtc.pac.BondSettlementType2Code;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.InstructionOriginCode;


/***
 * 
 * 结算报文-解析报文
 *
 */

public class CnbondPkgToBean {
	/***
	 * 解析报文的业务类型为：BT01；即（现券详情）
	 * @param sttlmDtl
	 * @return
	 */
	public static CnbondPkgDetailBean zLDetail(Group sttlmDtl){
		CnbondPkgDetailBean cnbondPkgDetailBean = new CnbondPkgDetailBean();
		//业务类型
		cnbondPkgDetailBean.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
		//结算指令标识
		cnbondPkgDetailBean.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
		//付券方账户id
		cnbondPkgDetailBean.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
		//付券方账户名称
		cnbondPkgDetailBean.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		//收券方账户id
		cnbondPkgDetailBean.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
		//收券方账户名称
		cnbondPkgDetailBean.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		//结算方式1
		cnbondPkgDetailBean.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
		//结算金额1
		cnbondPkgDetailBean.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
		//结算金额2
		cnbondPkgDetailBean.setVal2(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())));
		//交割日1
		cnbondPkgDetailBean.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
		//业务标识号
		cnbondPkgDetailBean.setTxId(sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString());
		if(sttlmDtl.hasGroup("Bd1")) {
			Group bd1 = sttlmDtl.getGroups("Bd1").get(0);
			//债券总额
			cnbondPkgDetailBean.setBdAmt(FormatUtil.formatBigDecimal(new BigDecimal(bd1.getfieldsForFieldMap().get("BdAmt").getObject().toString())));
			//净价（百元面值）
			cnbondPkgDetailBean.setPric1(bd1.getfieldsForFieldMap().get("Pric1").getObject().toString());
			//全价（百元面值）
			cnbondPkgDetailBean.setPric2(bd1.getfieldsForFieldMap().get("Pric2").getObject().toString());
		}
		
		if(sttlmDtl.getGroups("Oprtr").size()>0){
			//操作员
			cnbondPkgDetailBean.setOprtr(sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		}
		if(sttlmDtl.getGroups("Chckr").size()>0){
			//操作员
			cnbondPkgDetailBean.setChckr(sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		}
		return cnbondPkgDetailBean;
	}
	
	
	/***
	 * 解析报文的业务类型为：BT02；即（质押式回购详情）
	 * @param sttlmDtl
	 * @return
	 */
	public static CnbondPkgDetailBean zLRepoDetail(Group sttlmDtl){
		CnbondPkgDetailBean cnbondPkgDetailBean = new CnbondPkgDetailBean();
		//指令来源
		cnbondPkgDetailBean.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
		//业务类型
		cnbondPkgDetailBean.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
		//结算指令标识
		cnbondPkgDetailBean.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
		//付券方账户id
		cnbondPkgDetailBean.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
		//付券方账户名称
		cnbondPkgDetailBean.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		//收券方账户id
		cnbondPkgDetailBean.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
		//收券方账户名称
		cnbondPkgDetailBean.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		//结算方式1
		cnbondPkgDetailBean.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
		//结算方式2
		cnbondPkgDetailBean.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
		//结算金额1
		cnbondPkgDetailBean.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
		//结算金额2
		cnbondPkgDetailBean.setVal2(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())));
		//交割日1
		cnbondPkgDetailBean.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
		//交割日2
		cnbondPkgDetailBean.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
		//业务标识号
		cnbondPkgDetailBean.setTxId(sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString());
		//全面金额
		cnbondPkgDetailBean.setAggtFaceAmt(FormatUtil.formatBigDecimal4(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("AggtFaceAmt").getObject().toString())));
		//回购利率
		cnbondPkgDetailBean.setRate(sttlmDtl.getfieldsForFieldMap().get("Rate").getObject().toString());
		
		if(sttlmDtl.getGroups("Oprtr").size()>0){
			//操作者
			cnbondPkgDetailBean.setOprtr(sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		}
		if(sttlmDtl.getGroups("Chckr").size()>0){
			//操作者
			cnbondPkgDetailBean.setChckr(sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		}
		return cnbondPkgDetailBean;
	}
	
	
	/***
	 * 解析报文的业务类型为：BT03；即（买断式回购详情）
	 * @param sttlmDtl
	 * @return
	 */
	public static CnbondPkgDetailBean zLRepoMDDetail(Group sttlmDtl){
		CnbondPkgDetailBean cnbondPkgDetailBean = new CnbondPkgDetailBean();
		
		String InstrOrgn  = sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString();
		if(InstrOrgn.equalsIgnoreCase(InstructionOriginCode.Type.IO01.getType())) {
			cnbondPkgDetailBean.setOrgtrCnfrmInd("--");
			cnbondPkgDetailBean.setCtrCnfrmInd("--");
		}
		//指令来源
		cnbondPkgDetailBean.setInstrOrgn(InstructionOriginCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("InstrOrgn").getObject().toString()));
		//业务类型
		cnbondPkgDetailBean.setBizTp(BusinessTypeCode.Type.getName(sttlmDtl.getfieldsForFieldMap().get("BizTp").getObject().toString()));
		//结算指令标识
		cnbondPkgDetailBean.setInstrId(sttlmDtl.getfieldsForFieldMap().get("InstrId").getObject().toString());
		//付券方账户id
		cnbondPkgDetailBean.setGivAcctId(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
		//付券方账户名称
		cnbondPkgDetailBean.setGivAcctNm(sttlmDtl.getGroups("GivAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		//收券方账户id
		cnbondPkgDetailBean.setTakAcctId(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Id").getObject().toString());
		//收券方账户名称
		cnbondPkgDetailBean.setTakAcctNm(sttlmDtl.getGroups("TakAcct").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		//结算方式1
		cnbondPkgDetailBean.setSttlmTp1(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp1").getObject().toString()));
		//结算方式2
		cnbondPkgDetailBean.setSttlmTp2(BondSettlementType2Code.Type.getName(sttlmDtl.getfieldsForFieldMap().get("SttlmTp2").getObject().toString()));
		//结算金额1
		cnbondPkgDetailBean.setVal1(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val1").getObject().toString())));
		//结算金额2
		cnbondPkgDetailBean.setVal2(FormatUtil.formatBigDecimal(new BigDecimal(sttlmDtl.getfieldsForFieldMap().get("Val2").getObject().toString())));
		//交割日1
		cnbondPkgDetailBean.setDt1(sttlmDtl.getfieldsForFieldMap().get("Dt1").getObject().toString());
		//交割日2
		cnbondPkgDetailBean.setDt2(sttlmDtl.getfieldsForFieldMap().get("Dt2").getObject().toString());
		//业务标识号
		cnbondPkgDetailBean.setTxId(sttlmDtl.getfieldsForFieldMap().get("TxId").getObject().toString());
		
		if(sttlmDtl.hasGroup("SttlmGrte1")) {
			/**
			 * 按实例进行解析
			 */
		}else{
			cnbondPkgDetailBean.setGrteTp("无");
		}
			
		if(sttlmDtl.getGroups("Oprtr").size()>0){
			//操作者
			cnbondPkgDetailBean.setOprtr(sttlmDtl.getGroups("Oprtr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		}
		if(sttlmDtl.getGroups("Chckr").size()>0){
			//操作者
			cnbondPkgDetailBean.setChckr(sttlmDtl.getGroups("Chckr").get(0).getfieldsForFieldMap().get("Nm").getObject().toString());
		}
		return cnbondPkgDetailBean;
	}
}
