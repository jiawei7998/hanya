package com.singlee.financial.cdtc.util;

public class StaticFinal {
	/** 所有的 */
	public static final String ALL = "ALL";
	/** 收付款标志 */
	public static final String PAYRECIND = "PAYRECIND";
	/** 产品类型*/
	public static final String PRODCODE = "PRODCODE";
	/** 清算方式 */
	public static final String SETMEANS = "SETMEANS";
}
