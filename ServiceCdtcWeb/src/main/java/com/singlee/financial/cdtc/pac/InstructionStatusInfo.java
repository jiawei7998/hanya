/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

/**
 * @author Fang
 *
 */
public class InstructionStatusInfo extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("InstrSts", Field.class);
		keyClassMap.put("TxRsltCd", Field.class);
		keyClassMap.put("InsErrInf", Field.class);
		keyClassMap.put("OrgtrCnfrmInd", Field.class);
		keyClassMap.put("CtrCnfrmInd", Field.class);
	}
	
	
	public InstructionStatusInfo(String tag){
		super(tag, tag, new String[]{"InstrSts","TxRsltCd","InsErrInf","OrgtrCnfrmInd","CtrCnfrmInd"});
	}
	public InstructionStatusInfo(String tag,String delim){
		super(tag, tag, new String[]{"InstrSts","TxRsltCd","InsErrInf","OrgtrCnfrmInd","CtrCnfrmInd"});
	}
	
	public void set(InstructionStatusCode field){
		setField(field);
	}
	public void set(Max8Text field){
		setField(field);
	}
	public void set(Max35Text field){
		setField(field);
	}
	public void set(InstructionConfirmIndicatorCode field){
		setField(field);
	}
	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
