/**
 * 
 */
package com.singlee.financial.cdtc.dao;

import java.util.List;

import com.singlee.financial.cdtc.bean.BtchQryRsltBean;

/**
 * @author Fang
 *
 */
public interface BtchQryDao {

	/**
	 * 提交经办
	 */
	public boolean addBtchQry(BtchQryRsltBean btchQryRsltBean) throws Exception;
	
	/**
	 * 查询已经办信息
	 */
	public List<BtchQryRsltBean> queryAllBtchQry(BtchQryRsltBean btch) throws Exception;
	
	/**
	 * 查询已复核信息
	 */
	public List<BtchQryRsltBean> queryAllBtchQryVerity() throws Exception;
	
	/**
	 * 在合同经办前查询合同是否已经被其他用户经办处理
	 * @param btch
	 * @return BtchQryRsltBean
	 */
	public BtchQryRsltBean queryContractMsgIsExistJB(BtchQryRsltBean btch) throws Exception;
	/**
	 * 判断是否存在已经办或者已复核信息
	 * @param btchQryRslt
	 * @return
	 * @throws Exception
	 */
	public boolean isExistBtchQryRslt(BtchQryRsltBean btchQryRsltBean) throws Exception;
	/**
	 * 更新状态
	 * @param btchList
	 * @return
	 * @throws Exception
	 */
	public boolean updateBtchQryRsltSingleeOrBatchForCondition(BtchQryRsltBean btchQryRsltBean) throws Exception;
	
	/**
	 * 执行经办撤销时 删除数据库中已经经办的合同记录
	 * @param btchList
	 * @return
	 * @throws Exception
	 */
	public boolean deleteBtchQryRslt(List<BtchQryRsltBean> btchList) throws Exception;
	
}
