package com.singlee.financial.cdtc.cnbondpackage;

import com.singlee.financial.cdtc.util.PropertiesUtil;


public class CdtcBankAccount {

	public static String BANK_ACCOUNT = null;
	
	static {
		BANK_ACCOUNT = PropertiesUtil.getProperties("cdtc.jsBankAcct");
		
		System.out.println("init BANK_ACCOUNT =>" + BANK_ACCOUNT);
	}
	
}
