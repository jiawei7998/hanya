package com.singlee.financial.cdtc.dao;


/**
 * * 获取summit系统当前日期
 * 
 * @author 
 * 
 */
public interface BranprcdateGetDao {
	
	/**
	 * 查询当前summit的系统日期
	 * @return
	 * @throws Exception
	 */
	public String queryBranchDate() throws Exception;
}
