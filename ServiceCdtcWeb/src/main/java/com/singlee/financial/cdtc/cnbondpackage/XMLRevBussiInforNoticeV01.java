package com.singlee.financial.cdtc.cnbondpackage;

import java.lang.reflect.Constructor;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;

import com.singlee.financial.cdtc.pac.Field;
import com.singlee.financial.cdtc.pac.Group;

public class XMLRevBussiInforNoticeV01 {

	/**
	 * 树形扩展GROUP 反解析封装 GROUP ADD GROUP  二叉树原理
	 * @param element
	 * @param heartBeatMessageV02
	 * @return
	 * @throws NoSuchMethodException
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static Group getGroups(Element element,BusinessInformationNoticeV01 businessInformationNoticeV01) throws NoSuchMethodException, Exception
	{
		//Group group = new Group(StringUtils.trimToEmpty(element.getName()),"");
		Class<?>[] parameterTypes2 = { String.class, String.class };
		Constructor<?> constructor2 = BusinessInformationNoticeV01.keyClassMap.get(StringUtils.trimToEmpty(element.getName())).getConstructor(parameterTypes2);
		Object[] parameters2 = {StringUtils.trimToEmpty(element.getName()),""};
		Group group = (Group) constructor2.newInstance(parameters2);
		
		System.out.println("************************************");
		List<Element> elements = element.elements();
		for(Iterator<Element> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			//getElementList(element2,heartBeatMessageV02);
			System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
			if(element2.elements().size()>0)
			{
				System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
				group.addGroup(getGroups(element2,businessInformationNoticeV01));
			}else {
				System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
				Class<?>[] parameterTypes={String.class,Object.class}; 
				Constructor<?> constructor = BusinessInformationNoticeV01.keyClassMap.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
				Object[] parameters={StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element2.getText())};
				Object object = constructor.newInstance(parameters);
				group.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
			}
		}
		
		System.out.println("************************************");
		return group;
	}
	/**
	 * 逆解析第一层报文结构 即ROOT ELEMENTS下面的节点
	 * @param element
	 * @param heartBeatMessageV02
	 * @throws Exception
	 * @throws NoSuchMethodException
	 */
	public static void getElementList(Element element,BusinessInformationNoticeV01 businessInformationNoticeV01) throws Exception, NoSuchMethodException
	{
		List<?> elements = element.elements();
		for(Iterator<?> iterator = elements.iterator();iterator.hasNext();)
		{
			Element element2 = (Element)iterator.next();
			List<?> elements2 = element2.elements();
			if(elements2.size()>=0)
			{
				System.out.println(""+String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+""+StringUtils.trimToEmpty(element2.getText()));
				if(businessInformationNoticeV01.isHasTag(StringUtils.trimToEmpty(element2.getName())))
				{
					if(element2.elements().size()>0)
					{
						System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+"IS GROUP");
						
						businessInformationNoticeV01.addGroup(getGroups(element2,businessInformationNoticeV01));
					}else {
						System.out.println(String.format("%20s :", StringUtils.trimToEmpty(element2.getName()))+"IS FIELD");
						Class<?>[] parameterTypes={String.class,Object.class}; 
						Constructor<?> constructor = BusinessInformationNoticeV01.keyClassMap.get(StringUtils.trimToEmpty(element2.getName())).getConstructor(parameterTypes);
						Object[] parameters={StringUtils.trimToEmpty(element2.getName()),StringUtils.trimToEmpty(element2.getText())};
						Object object = constructor.newInstance(parameters);
						businessInformationNoticeV01.setField(StringUtils.trimToEmpty(element2.getName()),(Field<?>)object);
					}
				}
			}
			//getElementList(element2,heartBeatMessageV02);
		}
	}
}
