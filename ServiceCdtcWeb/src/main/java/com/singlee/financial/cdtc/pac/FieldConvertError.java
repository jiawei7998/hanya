package com.singlee.financial.cdtc.pac;

public class FieldConvertError extends Exception
{
  private static final long serialVersionUID = 1L;

  public FieldConvertError(String s)
  {
    super(s);
  }
}