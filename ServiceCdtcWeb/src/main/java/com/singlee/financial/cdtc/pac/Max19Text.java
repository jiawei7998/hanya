/**
 * 
 */
package com.singlee.financial.cdtc.pac;

/**
 * @author Fang
 *
 */
public class Max19Text extends StringField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param field
	 */
	public Max19Text(String field) {
		super(field);
	}
	@Override
	public String toXmlString(){
		if(super.getLength() >= 1 && super.getLength() <= 19){
			return toXml();
		}
		return "";
	}
}
