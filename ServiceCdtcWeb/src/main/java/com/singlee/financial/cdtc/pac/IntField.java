package com.singlee.financial.cdtc.pac;

public class IntField extends Field<Integer> {

	private static final long serialVersionUID = 6565820607933258689L;

	 public IntField(String field)
	  {
	    super(field, Integer.valueOf(0));
	  }

	  public IntField(String field, Integer data) {
	    super(field, data);
	  }

	  public IntField(String field, int data) {
	    super(field, Integer.valueOf(data));
	  }

	  public void setValue(Integer value) {
	    setObject(value);
	  }

	  public void setValue(int value) {
	    setObject(Integer.valueOf(value));
	  }

	  public int getValue() {
	    return ((Integer)getObject()).intValue();
	  }

	  public boolean valueEquals(Integer value) {
	    return ((Integer)getObject()).equals(value);
	  }

	  public boolean valueEquals(int value) {
	    return ((Integer)getObject()).equals(Integer.valueOf(value));
	  }
}
