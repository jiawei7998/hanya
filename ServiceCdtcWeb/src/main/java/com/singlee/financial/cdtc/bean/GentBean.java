package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.util.Date;

public class GentBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tableid;	
	private String tablevalue;		
	private String tabletext;		
	private String tabletext1;			
	private String text;		
	private String text1;		
	private String note;		
	private Date lstmntdate;
	public String getTableid() {
		return tableid;
	}
	public void setTableid(String tableid) {
		this.tableid = tableid;
	}
	public String getTablevalue() {
		return tablevalue;
	}
	public void setTablevalue(String tablevalue) {
		this.tablevalue = tablevalue;
	}
	public String getTabletext() {
		return tabletext;
	}
	public void setTabletext(String tabletext) {
		this.tabletext = tabletext;
	}
	public String getTabletext1() {
		return tabletext1;
	}
	public void setTabletext1(String tabletext1) {
		this.tabletext1 = tabletext1;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getText1() {
		return text1;
	}
	public void setText1(String text1) {
		this.text1 = text1;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getLstmntdate() {
		return lstmntdate;
	}
	public void setLstmntdate(Date lstmntdate) {
		this.lstmntdate = lstmntdate;
	}
	
	
}
