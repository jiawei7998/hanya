package com.singlee.financial.cdtc.server;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cdc.dmsg.client.DClient;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.cdtc.CdtcSecurityBatchSettlServer;
import com.singlee.financial.cdtc.bean.BtchQryRsltBean;
import com.singlee.financial.cdtc.cnbondpackage.CdtcBankAccount;
import com.singlee.financial.cdtc.cnbondpackage.SystemExceptionAdviceV01;
import com.singlee.financial.cdtc.cnbondpackage.XMLRevSysExceptionAdviceV01;
import com.singlee.financial.cdtc.dao.BranprcdateGetDao;
import com.singlee.financial.cdtc.dao.CdtcRequestMessageCreateDao;
import com.singlee.financial.cdtc.dao.DClientDao;
import com.singlee.financial.cdtc.pac.BusinessTypeCode;
import com.singlee.financial.cdtc.pac.Group;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;
import com.singlee.financial.cdtc.pac.SpotSettlementInstruction;
import com.singlee.financial.cdtc.util.XmlFormat;
/***
 * 中债直连 - CDTC业务批量查询
 * @param 
 * @return
 * @throws Exception
 */
@Service
@Transactional(value="transactionManager",rollbackFor=Exception.class)
public class CdtcSecurityBatchSettlServerImpl implements CdtcSecurityBatchSettlServer{
	
	@Value("#{cdtcProperties['cdtc.wasIp']}")
	private String wasIp;
	@Autowired
	CdtcRequestMessageCreateDao cdtcRequestMessageCreateDao;
	@Autowired
	DClientDao dClientDao;
	@Autowired
	BranprcdateGetDao branprcdateGetDao;
	
	private  Logger logger = LoggerFactory.getLogger(CdtcSecurityBatchSettlServerImpl.class);
	
	List<BtchQryRslt> btchqList=null;
	
	@Override
	public List<BtchQryRslt> securityBatchSettlQuery(Map<String, Object> map) throws Exception {
		logger.info("中央债券结算综合业务查询交互,正在向CDTC批量查询结算数据,请稍候......");
		try{
			SettlementBusinessQueryRequest settlementBusinessQueryRequest = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS03AndBJ0300(CdtcBankAccount.BANK_ACCOUNT,branprcdateGetDao.queryBranchDate());
			StringBuffer xmlBuffer = new StringBuffer();
			settlementBusinessQueryRequest.toXmlForSuperParent(xmlBuffer);
			logger.info("正在发送指令批量报文,等待CDTC回执,请稍后......"+xmlBuffer.toString());
			DClient dClient = new DClient(wasIp);
			String backXml = dClient.sendMsg(XmlFormat.format(xmlBuffer.toString()));
			//HashMap<String, String> resultHashMap = dClientDao.sendXmlMessageToCdtcForRequest(XmlFormat.format(xmlBuffer.toString()));
			if(backXml != null) {
				SAXReader reader = new SAXReader();
				org.dom4j.Document xmlDoc = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml).getBytes("UTF-8")));  
				String type = xmlDoc.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
				if(null != type && "CSBS.006.001.01".equalsIgnoreCase(type)){
					HashMap<String, BtchQryRsltBean> btchqHashMap = new HashMap<String, BtchQryRsltBean>();
					SettlementBusinessQueryRequest settlementBusinessQueryRequest2 = cdtcRequestMessageCreateDao.createCSBS00500101ForQryCondConditionOS03AndBJ0301(CdtcBankAccount.BANK_ACCOUNT,branprcdateGetDao.queryBranchDate());
					StringBuffer xmlBuffer2 = new StringBuffer();
					settlementBusinessQueryRequest2.toXmlForSuperParent(xmlBuffer2);
					logger.info("正在发送合同批量报文,等待CDTC回执,请稍后......"+xmlBuffer2.toString());
					String backXml2 = dClient.sendMsg(XmlFormat.format(xmlBuffer2.toString()));
					//HashMap<String, String> resultHashMap2 = dClientDao.sendXmlMessageToCdtcForRequest(XmlFormat.format(xmlBuffer2.toString()));
					if(backXml2 != null){
						org.dom4j.Document xmlDoc2 = reader.read(new ByteArrayInputStream(XmlFormat.format(backXml2).getBytes("UTF-8")));  
						String type2 = xmlDoc2.getRootElement().element("MsgHeader").element("MsgDesc").element("MsgTp").getText();
						List<BtchQryRsltBean> btchqList2 = new ArrayList<BtchQryRsltBean>();
						
						if(null != type2 && "CSBS.006.001.01".equalsIgnoreCase(type2)) {
							btchqList2 =cdtcRequestMessageCreateDao.transformatCSBS00600101ForQryCondConditionOS03AndBJ0301(backXml2);
							for(BtchQryRsltBean btchQryRsltBean : btchqList2) {
								btchqHashMap.put(btchQryRsltBean.getInstrId().trim(), btchQryRsltBean);
							}
							
						}
					}
					logger.info("解析回执报文,展现数据,请稍后......");
					btchqList =cdtcRequestMessageCreateDao.transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(backXml);
					final LinkedHashMap<Integer, BtchQryRslt> cdtcHashMap = new LinkedHashMap<Integer, BtchQryRslt>();
					int count = 0;
					for(int i = 0 ;i < btchqList.size() ;i++) {
						BtchQryRslt btchQryRslt = btchqList.get(i);
						if(btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT19.getType())||btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT17.getType())||btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT18.getType())
						||btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT22.getType())||btchQryRslt.getBizTp().equalsIgnoreCase(BusinessTypeCode.Type.BT21.getType())){
							
						}else {
							btchQryRslt.setCtrctId(btchqHashMap.get(btchQryRslt.getInstrId().trim())==null?"":btchqHashMap.get(btchQryRslt.getInstrId().trim()).getCtrctId());
							btchQryRslt.setCtrctSts(btchqHashMap.get(btchQryRslt.getInstrId().trim())==null?"":btchqHashMap.get(btchQryRslt.getInstrId().trim()).getCtrctSts());
							cdtcHashMap.put(count, btchQryRslt);
							count++;
						}
						
					}
					
				}else if("CSBS.009.001.01".equalsIgnoreCase(type)){
					SpotSettlementInstruction spotSettlementInstruction = new SpotSettlementInstruction("Root");
					spotSettlementInstruction.unionHashMap(SystemExceptionAdviceV01.type);
					spotSettlementInstruction.getElementList(xmlDoc.getRootElement(), spotSettlementInstruction);
					SystemExceptionAdviceV01 systemExceptionAdviceV01 = null;
					if(spotSettlementInstruction.hasGroup("Document")) {
						List<Group> groupsList = spotSettlementInstruction.getGroups("Document");
						for(Group group : groupsList) {
							StringBuffer xmlBufferT = new StringBuffer();
							group.toXmlForParent(xmlBufferT);
							systemExceptionAdviceV01 = new SystemExceptionAdviceV01("Document");
							Document document = reader.read(new ByteArrayInputStream((XmlFormat.format("<Document>"+xmlBufferT.toString()+"</Document>").getBytes("UTF-8"))));  
							XMLRevSysExceptionAdviceV01.getElementList(document.getRootElement(), systemExceptionAdviceV01);
						}
					}
					if(null != systemExceptionAdviceV01) {
						String XcptnRtrCd = "";String XcptnRtrTx="";
						if(systemExceptionAdviceV01.hasGroup("SysXcptn")) {
							XcptnRtrCd = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrCd").getObject().toString();
							XcptnRtrTx = systemExceptionAdviceV01.getGroups("SysXcptn").get(0).getfieldsForFieldMap().get("XcptnRtrTx").getObject().toString();
						}
						logger.info("消息错误码["+XcptnRtrCd+"]\r\n错误信息[" + XcptnRtrTx+"]");
					}
				}
			}
		}catch (Exception e) {
			throw e;
		}
		
		return btchqList;
	}
	
}
