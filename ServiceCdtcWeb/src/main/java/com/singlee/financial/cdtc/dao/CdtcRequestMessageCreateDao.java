package com.singlee.financial.cdtc.dao;

import java.util.List;

import com.singlee.financial.bean.BtchQryRslt;
import com.singlee.financial.cdtc.bean.BtchQryRsltBean;
import com.singlee.financial.cdtc.pac.SettlementBusinessQueryRequest;

/**
 * 用于创建CDTC报文的工具类
 * @author Administrator
 *
 */
public interface CdtcRequestMessageCreateDao {
	/**
	 * 指令详情查询
	 * BJ0300 指令查询
	 * OS04   详情
	 * txFlowId 指令流水  acctQryId 托管账户ID  qryCondId 查询的指令唯一标识（即批量合同查询回来的每个TxFlowId的值）
	 * @return SettlementBusinessQueryRequest CSBS.005.001.01
	 * @throws Exception
	 */
	public SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS04AndBJ0300(String txFlowId,String acctQryId,String qryCondId) throws Exception;
	/**
	 * 指令批量查询
	 * BJ0300 指令查询
	 * OS03 批量
	 * @param acctQryId 托管账户ID
	 * @param qryCondDt 查询的日期
	 * @return
	 * @throws Exception
	 */
	public SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS03AndBJ0300(String acctQryId,String qryCondDt) throws Exception;
	
	/**
	 * 反解析出批量业务查询返回的业务数据
	 * @param xmlBufferStr 批量返回的XML串
	 * @return
	 * @throws Exception
	 */
	public List<BtchQryRslt> transformatCSBS00600101ForQryCondConditionOS03AndBJ0300(String xmlBufferStr) throws Exception;
	/**
	 * 合同批量查询
	 * @param acctQryId
	 * @param qryCondDt
	 * @return
	 * @throws Exception
	 */
	public SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS03AndBJ0301(String acctQryId,String qryCondDt) throws Exception;
	
	/**
	 * 返回合同列表
	 * @param xmlBufferStr
	 * @return
	 * @throws Exception
	 */
	public List<BtchQryRsltBean> transformatCSBS00600101ForQryCondConditionOS03AndBJ0301(String xmlBufferStr) throws Exception;
	/**
	 * 合同详细查询
	 * @param acctQryId
	 * @param qryCondId
	 * @return
	 * @throws Exception
	 */
	public SettlementBusinessQueryRequest createCSBS00500101ForQryCondConditionOS04AndBJ0301(String acctQryId,String qryCondId) throws Exception;
}
