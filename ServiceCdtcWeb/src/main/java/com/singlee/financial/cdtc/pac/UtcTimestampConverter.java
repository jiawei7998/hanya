package com.singlee.financial.cdtc.pac;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class UtcTimestampConverter extends AbstractDateTimeConverter
{
	  private static ThreadLocal<UtcTimestampConverter> utcTimestampConverter = new ThreadLocal<UtcTimestampConverter>();

	  private final DateFormat utcTimestampFormat = createLocalDateFormat("yyyy-MM-dd HH:mm:ss");
	  
	  private final DateFormat utcTimestampFormatMillis = createLocalDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	  private static HashMap<String, Calendar> dateCache = new HashMap<String, Calendar>();
	  
	  public static String convert(Date d, boolean includeMilliseconds)
	  {
	    return getFormatter(includeMilliseconds).format(d);
	  }

	  private static DateFormat getFormatter(boolean includeMillis) {
	    UtcTimestampConverter converter = (UtcTimestampConverter)utcTimestampConverter.get();
	    if (converter == null) {
	      converter = new UtcTimestampConverter();
	      utcTimestampConverter.set(converter);
	    }
	    return includeMillis ? converter.utcTimestampFormatMillis : converter.utcTimestampFormat;
	  }

	  public static Date convert(String value)
	    throws FieldConvertError
	  {
	    verifyFormat(value);
	    String dateString = value.substring(0, 8);
	    Calendar c = getCalendarForDay(value, dateString);
	    long timeOffset = parseLong(value.substring(9, 11)) * 3600000L + 
	      parseLong(value.substring(12, 14)) * 60000L + parseLong(value.substring(15, 17)) * 1000L;
	    if (value.length() == 21) {
	      timeOffset += parseLong(value.substring(18, 21));
	    }
	    return new Date(c.getTimeInMillis() + timeOffset);
	  }

	  private static Calendar getCalendarForDay(String value, String dateString) {
	    Calendar c = (Calendar)dateCache.get(dateString);
	    if (c == null) {
	      c = new GregorianCalendar(1970, 0, 1, 0, 0, 0);
	      c.setTimeZone(SystemTime.UTC_TIMEZONE);
	      int year = Integer.parseInt(value.substring(0, 4));
	      int month = Integer.parseInt(value.substring(4, 6));
	      int day = Integer.parseInt(value.substring(6, 8));
	      c.set(year, month - 1, day);
	      dateCache.put(dateString, c);
	    }
	    return c;
	  }

	  private static void verifyFormat(String value) throws FieldConvertError {
	    String type = "timestamp";
	    if ((value.length() != 17) && (value.length() != 21)) {
	      throwFieldConvertError(value, type);
	    }
	    assertDigitSequence(value, 0, 8, type);

	    checkDate(value, 0, 8, type);
	    assertSeparator(value, 8, '-', type);
	    assertDigitSequence(value, 9, 11, type);

	    checkHour(value, 9, 11, type);
	    assertSeparator(value, 11, ':', type);
	    assertDigitSequence(value, 12, 14, type);

	    checkMinute(value, 12, 14, type);
	    assertSeparator(value, 14, ':', type);
	    assertDigitSequence(value, 15, 17, type);

	    checkUTCLeapSecond(value, 15, 17, type);
	    if (value.length() == 21) {
	      assertSeparator(value, 17, '.', type);
	      assertDigitSequence(value, 18, 21, type);
	    } else if (value.length() != 17) {
	      throwFieldConvertError(value, type);
	    }
	  }
	}
