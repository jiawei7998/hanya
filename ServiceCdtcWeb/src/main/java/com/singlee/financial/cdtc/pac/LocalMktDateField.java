package com.singlee.financial.cdtc.pac;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class LocalMktDateField extends LocalDateField
{
	  private static final long serialVersionUID = 1L;
	  private static TimeZone timezone = TimeZone.getTimeZone("GMT+08:00");
	  private static Calendar calendar;

	  public LocalMktDateField(String field)
	  {
	    super(field, LocalMktDateConverter.convert(createDate()));
	  }

	  protected LocalMktDateField(String field, Date data)
	  {
	    super(field, LocalMktDateConverter.convert(data));
	  }

	  protected LocalMktDateField(String field, String data)
	  {
	    super(field, data);
	  }

	  public void setValue(Date value) {
	    setValue(LocalMktDateConverter.convert(value));
	  }

	  public boolean valueEquals(Date value) {
	    return valueEquals(LocalMktDateConverter.convert(value));
	  }

	  private static Date createDate()
	  {
	    calendar = Calendar.getInstance(timezone);
	    return calendar.getTime();
	  }
	}