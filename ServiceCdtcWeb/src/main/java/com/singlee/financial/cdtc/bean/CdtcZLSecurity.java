package com.singlee.financial.cdtc.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class CdtcZLSecurity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String secid;//债券ID
	
	private String secidNm;//债券名称
	
	private BigDecimal faceAmt;//面值
	
	private BigDecimal accrualAmt;//应计利息
	
	private BigDecimal accrualTotalAmt;//应计利息总和

	/**
	 * @return the secid
	 */
	public String getSecid() {
		return secid;
	}

	/**
	 * @param secid the secid to set
	 */
	public void setSecid(String secid) {
		this.secid = secid;
	}

	/**
	 * @return the secidNm
	 */
	public String getSecidNm() {
		return secidNm;
	}

	/**
	 * @param secidNm the secidNm to set
	 */
	public void setSecidNm(String secidNm) {
		this.secidNm = secidNm;
	}

	/**
	 * @return the faceAmt
	 */
	public BigDecimal getFaceAmt() {
		return faceAmt;
	}

	/**
	 * @param faceAmt the faceAmt to set
	 */
	public void setFaceAmt(BigDecimal faceAmt) {
		this.faceAmt = faceAmt;
	}

	/**
	 * @return the accrualAmt
	 */
	public BigDecimal getAccrualAmt() {
		return accrualAmt;
	}

	/**
	 * @param accrualAmt the accrualAmt to set
	 */
	public void setAccrualAmt(BigDecimal accrualAmt) {
		this.accrualAmt = accrualAmt;
	}

	/**
	 * @return the accrualTotalAmt
	 */
	public BigDecimal getAccrualTotalAmt() {
		return accrualTotalAmt;
	}

	/**
	 * @param accrualTotalAmt the accrualTotalAmt to set
	 */
	public void setAccrualTotalAmt(BigDecimal accrualTotalAmt) {
		this.accrualTotalAmt = accrualTotalAmt;
	}
}
