package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class AgentParty extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap = new HashMap<String, Class<?>>();

	static {
		keyClassMap.put("AgtPtyTP", Field.class);
		keyClassMap.put("AgtPtyId", PartyIdentification30.class);
		keyClassMap.putAll(PartyIdentification30.keyClassMap);
	}

	public AgentParty(String tag) {

		super(tag, tag, new String[] { "AgtPtyTP", "AgtPtyId" });
	}

	public AgentParty(String tag, String delim) {

		super(tag, tag, new String[] { "AgtPtyTP", "AgtPtyId" });
	}

	public void set(AgentPartyTypeCode value) {
		setField(value);
	}

	public void set(PartyIdentification30 group) {
		addGroup(group);
	}

	public boolean isSet(PartyIdentification30 field) {
		return isSetField(field.getFieldTag());
	}

	public boolean isSetPartyIdentification30() {
		return isSetField("AgtPtyId");
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}