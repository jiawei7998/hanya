package com.singlee.financial.cdtc.processdao;

import com.singlee.financial.cdtc.bean.BtchQryRslt;

public interface InterCdtcMsgDao {

	/**
	 * 当日待结算[手工]经办
	 * @param qryCondId
	 * @return
	 * @throws Exception
	 */
	public String settlInterCdtcMsg(BtchQryRslt btchQryRslt) throws Exception;
	
	
}
