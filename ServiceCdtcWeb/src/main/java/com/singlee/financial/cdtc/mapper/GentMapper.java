package com.singlee.financial.cdtc.mapper;

import java.util.Map;
import com.singlee.financial.cdtc.bean.GentBean;
public interface GentMapper {
	/**
	 * 取金额的允许偏差值
	 * @param gentbean
	 * @return
	 * @throws Exception
	 */
	public GentBean queryGent(Map<String,Object> map) throws Exception;
	
}
