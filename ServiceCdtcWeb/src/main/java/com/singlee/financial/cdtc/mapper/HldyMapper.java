package com.singlee.financial.cdtc.mapper;

import java.util.Map;


public interface HldyMapper {
	/**
	 * 判断是否需要自动生成账务文件
	 * @param postdate
	 * @return
	 * @throws Exception
	 */
	Map<String,Object> isAutoToCreateFileForAcups(Map<String,Object> map);
}
