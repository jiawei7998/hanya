/**
 * 
 */
package com.singlee.financial.cdtc.pac;

import java.util.Date;

/**
 * @author Fang
 * 
 */
public class CNTime extends EsbTimeOnlyField {

	public CNTime(String field, String data) {
		super(field, data, true);
	}

	public CNTime(String field, Date data) {
		super(field, data, true);
	}

	private static final long serialVersionUID = 1L;

}
