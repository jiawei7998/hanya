package com.singlee.financial.cdtc.pac;

import java.util.Date;
import java.util.HashMap;


public class Bond2 extends Group{
	
	private static final long serialVersionUID = 1L;
		
	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("BdId",        Field.class);
		keyClassMap.put("ISIN", 	   Field.class);
		keyClassMap.put("BdShrtNm",    Field.class);
		keyClassMap.put("CcyTp",       Field.class);
		keyClassMap.put("BdParam",     Field.class);
		keyClassMap.put("SummryStmt",  SummaryStatement.class);
		keyClassMap.put("DtldStmt",    DetailedStatement.class);
		keyClassMap.putAll(SummaryStatement.keyClassMap);
		keyClassMap.putAll(DetailedStatement.keyClassMap);
	}
		
	public Bond2(String tag){
			
		super(tag,tag, new String[] { "BdId", "ISIN","BdShrtNm","CcyTp","BdParam","SummryStmt","DtldStmt"});
		
	}
	
	public Bond2(String tag,String delim){
		
		super(tag,tag, new String[] { "BdId", "ISIN","BdShrtNm","CcyTp","BdParam","SummryStmt","DtldStmt"});
	}
	
	 public void set(Max30Text value) {

		 setField(value);
	 }

      public Max30Text get(Max30Text value) throws FieldNotFound
      {
        getField(value);

        return value;
      }

      public Max30Text getMax30Text() throws FieldNotFound
      {
    	  Max30Text value = new Max30Text("BdId");
          getField(value);

          return value;
      }

      public boolean isSet(Max30Text field) {
        return isSetField(field);
      }

      public boolean isSetMax30Text() {
        return isSetField("BdId");
      }
      public void set(ISINIdentifier value) {
	        setField(value);
	   }

     public ISINIdentifier get(ISINIdentifier value) throws FieldNotFound
     {
      getField(value);

      return value;
     }

     public ISINIdentifier getISINIdentifier() throws FieldNotFound
     {
    	ISINIdentifier value = new ISINIdentifier("ISIN");
        getField(value);

        return value;
     }

     public boolean isSet(ISINIdentifier field) {
      return isSetField(field);
     }

     public boolean isSetISINIdentifier() {
      return isSetField("ISIN");
     }
     
     public void set(Max35Text value) {
	        setField(value);
	   }

	  public Max35Text get(Max35Text value) throws FieldNotFound
	  {
	   getField(value);
	
	   return value;
	  }
	
	  public Max35Text getMax35Text() throws FieldNotFound
	  {
		  Max35Text value = new Max35Text("");
	     getField(value);
	
	     return value;
	  }
	
	  public boolean isSet(Max35Text field) {
	   return isSetField(field);
	  }
	
	  public boolean isSetMax35Text() {
	   return isSetField("BdShrtNm");
	  }
	  
	    public void set(CurrencyCode value) {
	        setField(value);
	   }

	  public CurrencyCode get(CurrencyCode value) throws FieldNotFound
	  {
	   getField(value);
	
	   return value;
	  }
	
	  public CurrencyCode getCurrencyCode() throws FieldNotFound
	  {
		  CurrencyCode value = new CurrencyCode("CcyTp");
	      getField(value);
	
	      return value;
	  }
	
	  public boolean isSet(CurrencyCode field) {
	   return isSetField(field);
	  }
	
	  public boolean isSetCurrencyCode() {
	   return isSetField("CcyTp");
	  }
	  public void set(SummaryStatement group) {
          addGroup(group);
	    }
	
	    public boolean isSet(SummaryStatement field) {
	      return isSetField(field.getFieldTag());
	    }
	
	    public boolean isSetSummaryStatement() {
	      return isSetField("SummryStmt");
	    }
	    public void set(DetailedStatement group) {
	          addGroup(group);
	    }
	
	    public boolean isSet(DetailedStatement field) {
	      return isSetField(field.getFieldTag());
	    }
	
	    public boolean isSetDetailedStatement() {
	      return isSetField("DtldStmt");
	    }
	    public void toXmlForSuperParent(StringBuffer xmlBuffer)
        {
    	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
    	  this.toXml(xmlBuffer);
    	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
        }
	    
	    public static void main(String[] args) {
			Bond2 bond2 = new Bond2("BOND");
			CurrencyCode currencyCode = new CurrencyCode("CcyTp");
			currencyCode.setValue("CNY");
			bond2.set(currencyCode);
			Max30Text max30Text = new Max30Text("BdId");
			max30Text.setValue("Max30Text");
			bond2.set(max30Text);
			Max30Text max30Text2 = new Max30Text("BdParam");
			max30Text2.setValue("Max30Text");
			bond2.set(max30Text2);
			Max30Text max30Text3 = new Max30Text("BdShrtNm");
			max30Text3.setValue("Max30Text");
			bond2.set(max30Text3);
			ISINIdentifier isIdentifier = new ISINIdentifier("ISIN");
			isIdentifier.setValue("ISIN12345678");
			bond2.set(isIdentifier);
			
			
		  CommonCurrencyAndAmount commonCurrencyAndAmount = new CommonCurrencyAndAmount("Bal","CNY");
    	  commonCurrencyAndAmount.setValue("12345678.67");
    	  AccountLedgerTypeCode accountLedgerTypeCode = new AccountLedgerTypeCode("LdgTp");
    	  accountLedgerTypeCode.setValue(AccountLedgerTypeCode.Type.AL00.getType());
    	  AccountLedger accountLedger = new AccountLedger("AcctLdg");
    	  accountLedger.set(commonCurrencyAndAmount);
    	  accountLedger.set(accountLedgerTypeCode);
    	  ISODate isDate = new ISODate("Dt",new Date());
    	  SummaryStatement summaryStatement = new SummaryStatement("SummryStmt");
    	  summaryStatement.set(isDate);
    	  summaryStatement.set(accountLedger);
    	  bond2.set(summaryStatement);
    	  
    	  
    	  DetailedStatement detailedStatement = new DetailedStatement("DtldStmt");
    	  
    	  detailedStatement.set(new ISODate("TxDt",new Date()));
    	  detailedStatement.set(new ISOTime("TxTm",new Date()));
    	  
    	  Exact9NumericText exact9NumericText = new Exact9NumericText("CtrctId");
    	  exact9NumericText.setValue(123456789);
    	  detailedStatement.set(exact9NumericText);
    	  
    	  Max35Text max35Text = new Max35Text("TxLog");
    	  max35Text.setValue("一直测试着的内容");
    	  detailedStatement.set(max35Text);
    	  
    	  TransactionAmount transactionAmount = new TransactionAmount("TxAmt");
    	  DirectionIndicator directionIndicator = new DirectionIndicator("DrctnFlg",Boolean.TRUE);
    	  transactionAmount.set(directionIndicator);
    	  CommonCurrencyAndAmount commonCurrencyAndAmount2 = new CommonCurrencyAndAmount("Val","CNY");
    	  commonCurrencyAndAmount2.setValue("12345678.67");
    	  transactionAmount.set(commonCurrencyAndAmount2);
    	  detailedStatement.set(transactionAmount);
    	  
    	  AccountLedgerTypeCode accountLedgerTypeCode2 = new AccountLedgerTypeCode("LdgTp");
    	  accountLedgerTypeCode2.setValue(AccountLedgerTypeCode.Type.AL00.getType());
    	  AccountLedger accountLedger2 = new AccountLedger("AcctLdg");
    	  CommonCurrencyAndAmount faceCurrencyAndAmount = new CommonCurrencyAndAmount("Bal","CNY");
  		  faceCurrencyAndAmount.setValue("1234567890");
    	  accountLedger2.set(faceCurrencyAndAmount);
    	  accountLedger2.set(accountLedgerTypeCode2);
    	  detailedStatement.set(accountLedger2);
    	  bond2.set(detailedStatement);
    	  
    	  StringBuffer stringBuffer = new StringBuffer();
    	  bond2.toXmlForSuperParent(stringBuffer);
    	  System.out.println(stringBuffer.toString());
		}
}
