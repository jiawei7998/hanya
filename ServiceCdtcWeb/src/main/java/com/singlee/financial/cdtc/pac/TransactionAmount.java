package com.singlee.financial.cdtc.pac;

import java.util.HashMap;

public class TransactionAmount extends Group {

	private static final long serialVersionUID = 1L;

	public static HashMap<String, Class<?>> keyClassMap= new HashMap<String, Class<?>>();
	static{
		keyClassMap.put("DrctnFlg", Field.class);
		keyClassMap.put("Val", 	    Field.class);
	}
	
	public TransactionAmount(String tag) {
		super(tag, tag, new String[] { "DrctnFlg", "Val" });
	}

	public TransactionAmount(String tag,String delim) {
		super(tag, tag, new String[] { "DrctnFlg", "Val" });
	}
	
	public void set(DirectionIndicator value) {
		setField(value);
	}
	
	public void set(Max35Text value) {
		setField(value);
	}

//	public DirectionIndicator get(DirectionIndicator value)
//			throws FieldNotFound {
//		getField(value);
//
//		return value;
//	}
//
//	public DirectionIndicator getDirectionIndicator() throws FieldNotFound {
//		DirectionIndicator value = new DirectionIndicator("LdgTp");
//		getField(value);
//
//		return value;
//	}
//
//	public boolean isSet(DirectionIndicator field) {
//		return isSetField(field);
//	}
//
//	public boolean isSetDirectionIndicator() {
//		return isSetField("DrctnFlg");
//	}

	public void set(CommonCurrencyAndAmount value) {
		setField(value);
	}

	public CommonCurrencyAndAmount get(CommonCurrencyAndAmount value)
			throws FieldNotFound {
		getField(value);

		return value;
	}

	public CommonCurrencyAndAmount getCommonCurrencyAndAmount()
			throws FieldNotFound {
		CommonCurrencyAndAmount value = new CommonCurrencyAndAmount("Bal",
				"CNY");
		getField(value);

		return value;
	}

	public boolean isSet(CommonCurrencyAndAmount field) {
		return isSetField(field);
	}

	public boolean isSetCommonCurrencyAndAmount() {
		return isSetField("Val");
	}

	public void toXmlForSuperParent(StringBuffer xmlBuffer) {
		xmlBuffer.append("<").append(this.getFieldTag()).append(">").append(
				'\r');
		this.toXml(xmlBuffer);
		xmlBuffer.append("</").append(this.getFieldTag()).append(">").append(
				'\r');
	}
}
