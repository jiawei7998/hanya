package com.singlee.financial.cdtc.pac;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.dom4j.DocumentException;

import com.singlee.financial.cdtc.httpclient.XmlFormat;

public class DetailedStatement2   extends Group{
	
	private static final long serialVersionUID = 1L;
		
		public DetailedStatement2(String tag){
			
			super(tag,tag, new String[] { "TxDt", "TxTm","CtrctId","TxLog","TxAmt","AcctLdg"});
		}
		 public void set(ISODate value) {
	        setField(value);
	      }

	      public ISODate get(ISODate value) throws FieldNotFound
	      {
	        getField(value);

	        return value;
	      }

	      public ISODate getISODate() throws FieldNotFound
	      {
	    	  ISODate value = new ISODate("TxDt",new Date());
	          getField(value);

	          return value;
	      }

	      public boolean isSet(ISODate field) {
	        return isSetField(field);
	      }

	      public boolean isSetISODate() {
	        return isSetField("TxDt");
	      }
	      
	      public void set(ISOTime value) {
		        setField(value);
		      }

	      public ISOTime get(ISOTime value) throws FieldNotFound
	      {
	        getField(value);

	        return value;
	      }

	      public ISOTime getISOTime() throws FieldNotFound
	      {
	    	  ISOTime value = new ISOTime("TxTm",new Date());
	          getField(value);

	          return value;
	      }

	      public boolean isSet(ISOTime field) {
	        return isSetField(field);
	      }

	      public boolean isSetISOTime() {
	        return isSetField("TxTm");
	      }
	      
	      public void set(Exact9NumericText value) {
		        setField(value);
		      }

	      public Exact9NumericText get(Exact9NumericText value) throws FieldNotFound
	      {
	        getField(value);

	        return value;
	      }

	      public Exact9NumericText getExact9NumericText() throws FieldNotFound
	      {
	    	  Exact9NumericText value = new Exact9NumericText("TxTm");
	          getField(value);

	          return value;
	      }

	      public boolean isSet(Exact9NumericText field) {
	        return isSetField(field);
	      }

	      public boolean isSetExact9NumericText() {
	        return isSetField("CtrctId");
	      }
	      
	      
	      public void set(Max35Text value) {
		        setField(value);
		      }

	      public Max35Text get(Max35Text value) throws FieldNotFound
	      {
	        getField(value);

	        return value;
	      }

	      public Max35Text getMax35Text() throws FieldNotFound
	      {
	    	  Max35Text value = new Max35Text("TxLog");
	          getField(value);

	          return value;
	      }

	      public boolean isSet(Max35Text field) {
	        return isSetField(field);
	      }

	      public boolean isSetMax35Text() {
	        return isSetField("CtrctId");
	      }
	      
	      public void set(AccountLedger group) {
	          addGroup(group);
	        }
	
	        public boolean isSet(AccountLedger field) {
	          return isSetField(field.getFieldTag());
	        }
	
	        public boolean isSetAccountLedger() {
	          return isSetField("AcctLdg");
	        }
	        public void set(TransactionAmount group) {
		          addGroup(group);
	        }
	
	        public boolean isSet(TransactionAmount field) {
	          return isSetField(field.getFieldTag());
	        }
	
	        public boolean isSetTransactionAmount() {
	          return isSetField("TxAmt");
	        }
	      public void toXmlForSuperParent(StringBuffer xmlBuffer)
	      {
	    	  xmlBuffer.append("<").append(this.getFieldTag()).append(">").append('\r');
	    	  this.toXml(xmlBuffer);
	    	  xmlBuffer.append("</").append(this.getFieldTag()).append(">").append('\r');
	      }
	      public static void main(String[] args) throws UnsupportedEncodingException, IOException, DocumentException, FieldNotFound {
	    	  DetailedStatement2 detailedStatement = new DetailedStatement2("DetailedStatement");
	    	  
	    	  detailedStatement.set(new ISODate("TxDt",new Date()));
	    	  detailedStatement.set(new ISOTime("TxTm",new Date()));
	    	  System.out.println(detailedStatement.getFieldTag());
	    	  
	    	  System.out.println(detailedStatement.getGroups().containsKey("CtrctId"));
	    	  Exact9NumericText exact9NumericText = new Exact9NumericText("CtrctId");
	    	  exact9NumericText.setValue(123456789);
	    	  detailedStatement.set(exact9NumericText);
	    	  
	    	  Max35Text max35Text = new Max35Text("TxLog");
	    	  max35Text.setValue("一直测试着的内容");
	    	  detailedStatement.set(max35Text);
	    	  
	    	  TransactionAmount transactionAmount = new TransactionAmount("TxAmt");
	    	  DirectionIndicator directionIndicator = new DirectionIndicator("DrctnFlg",Boolean.TRUE);
	    	  transactionAmount.set(directionIndicator);
	    	  CommonCurrencyAndAmount commonCurrencyAndAmount = new CommonCurrencyAndAmount("Val","CNY");
	    	  commonCurrencyAndAmount.setValue("12345678.67");
	    	  transactionAmount.set(commonCurrencyAndAmount);
	    	  detailedStatement.set(transactionAmount);
	    	  
	    	  AccountLedgerTypeCode accountLedgerTypeCode = new AccountLedgerTypeCode("LdgTp");
	    	  accountLedgerTypeCode.setValue(AccountLedgerTypeCode.Type.AL00.getType());
	    	  AccountLedger accountLedger = new AccountLedger("AcctLdg");
	    	  CommonCurrencyAndAmount faceCurrencyAndAmount = new CommonCurrencyAndAmount("Bal","CNY");
	  		  faceCurrencyAndAmount.setValue("1234567890");
	    	  accountLedger.set(faceCurrencyAndAmount);
	    	  accountLedger.set(accountLedgerTypeCode);
	    	  detailedStatement.set(accountLedger);
	   
	    	  StringBuffer stringBuffer = new StringBuffer();
	    	  detailedStatement.toXmlForSuperParent(stringBuffer);
	    	  System.out.println(XmlFormat.format(stringBuffer.toString()));
	    	  System.out.println(detailedStatement.getGroups().containsKey("AcctLdg"));
	    	  
		}
	}
