import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.component.*;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.xstream.utils.XmlUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;


/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName Test.java
 * @Description 测试
 * @createTime 2021年11月23日 19:49:00
 */
public class Test {
    public static void main(String[] args) throws Exception {

        File file = new File("C:\\Users\\shenzl\\Desktop\\I20211027143553_361.xml");
        InputStream in = new FileInputStream(file);
        byte[] buf = new byte[1024];
        int len;
        StringBuffer sb = new StringBuffer();
        while ((len = in.read(buf)) != -1) {
            sb.append(new String(buf, 0, len, "GB2312"));
        }
        System.out.println(sb.toString());
        in.close();
        //解析报文头初始化方法
        EsbPacket<com.singlee.financial.cdcc.model.CSBS01200101.Body> esbPacket = EsbPacket.initEsbPacket(null, MsgHeader.class, "Root");
        esbPacket = XmlUtils.fromXML(sb.toString(), esbPacket);
        MsgHeader msgHeader = esbPacket.getSysHeader(MsgHeader.class);
        String msgTy = msgHeader.getMsgDesc().getMsgTp();
        if (StringUtils.equals(msgTy, "CSBS.002.001.01")) {
            EsbPacket<com.singlee.financial.cdcc.model.CSBS00200101.Body> csbs002 = EsbPacket.initEsbPacket(com.singlee.financial.cdcc.model.CSBS00200101.Body.class, MsgHeader.class, "Root");
            csbs002 = XmlUtils.fromXML(sb.toString(), csbs002);
            com.singlee.financial.cdcc.model.CSBS00200101.Body csbs002Body = csbs002.getBody();
            com.singlee.financial.cdcc.model.CSBS00200101.Msg csbs002BodyMsg = csbs002Body.getMsg();
            com.singlee.financial.cdcc.model.CSBS00200101.StsFlg cdcc002BodyStsFlag = csbs002Body.getStsFlg();
            com.singlee.financial.cdcc.model.CSBS00200101.SttlmDtl csbs002BodySttlmDtl = csbs002Body.getSttlmDtl();
            com.singlee.financial.cdcc.model.CSBS00200101.GivInf csbs002BodyGivInf = csbs002Body.getGivInf();
            com.singlee.financial.cdcc.model.CSBS00200101.TakInf csbs002BodyTakInf = csbs002Body.getTakInf();
            List<Extension> xtnsn = csbs002Body.getXtnsn();
            //1.0	报文标识
            String txFlowId = csbs002BodyMsg.getTxFlowId();//交易流水标识
            String prty = csbs002BodyMsg.getPrty();//优先级
            String creDtTm = csbs002BodyMsg.getCreDtTm();//报文创建时间
            System.out.println("=>报文标识组件:" + String.format("交易流水标识：%s,优先级：%s,报文创建时间:%s", txFlowId, prty, creDtTm));
            //2.0	状态标识
            InstructionStatusInfo csbs002isi = cdcc002BodyStsFlag.getInstrStsInf();
            String instrsts = csbs002isi.getInstrSts(); //指令处理状态
            String txRsltCd = csbs002isi.getTxRsltCd(); //指令处理结果返回码
            String insErrInf = csbs002isi.getInsErrInf();//指令返回信息描述
            String orgtrCnfrmInd = csbs002isi.getOrgtrCnfrmInd();//发令方确认标识
            String ctrCnfrmInd = csbs002isi.getCtrCnfrmInd();//对手方确认标识
            System.out.println("=>状态标识组件:" + String.format("指令处理状态：%s,指令处理结果返回码：%s,指令返回信息描述：%s,发令方确认标识：%s,对手方确认标识:%s", instrsts, txRsltCd, insErrInf, orgtrCnfrmInd, ctrCnfrmInd));
            //合同状态(如果有)
            List<ContractStatusInfo> csbs002csrLst = cdcc002BodyStsFlag.getCtrctStsInf();
            if (null != csbs002csrLst) {
                for (ContractStatusInfo csbs002csi : csbs002csrLst) {
                    String ctrctId = csbs002csi.getCtrctId();//结算合同标识
                    String ctrctSts = csbs002csi.getCtrctSts();//合同处理状态
                    String ctrctBlckSts = csbs002csi.getCtrctBlckSts();//合同冻结状态
                    String ctrctFaildRsn = csbs002csi.getCtrctFaildRsn();//合同失败原因
                    String lastUpdTm = csbs002csi.getLastUpdTm();//最新更新时间
                    String ctrctQryRsltCd = csbs002csi.getCtrctQryRsltCd();//合同查询结果返回码
                    String ctrctQryRsltInf = csbs002csi.getCtrctQryRsltInf();//合同查询结果返回信息
                    System.out.println("=>状态标识组件:" + String.format("结算合同标识：%s,合同处理状态：%s,合同冻结状态：%s,合同失败原因：%s,最新更新时间:%s,合同查询结果返回码:%s,合同查询结果返回信息:%s", ctrctId, ctrctSts, ctrctBlckSts, ctrctFaildRsn, lastUpdTm, ctrctQryRsltCd, ctrctQryRsltInf));
                }
            }
            //3.0	结算详情
            String instrOrgn = csbs002BodySttlmDtl.getInstrOrgn();//结算指令来源
            String bizTp = csbs002BodySttlmDtl.getBizTp();//业务类别
            String instrId = csbs002BodySttlmDtl.getInstrId();//结算指令标识

            String takAcctId = (null != csbs002BodySttlmDtl.getTakAcct()) ? csbs002BodySttlmDtl.getTakAcct().getId() : ""; //收券方账户-帐户ID
            String takAcctNm = (null != csbs002BodySttlmDtl.getTakAcct()) ? csbs002BodySttlmDtl.getTakAcct().getNm() : ""; //收券方账户-帐户名称
            String gavAcctId = (null != csbs002BodySttlmDtl.getGivAcct()) ? csbs002BodySttlmDtl.getGivAcct().getId() : ""; //付券方账户-帐户ID
            String gavAcctNm = (null != csbs002BodySttlmDtl.getGivAcct()) ? csbs002BodySttlmDtl.getGivAcct().getNm() : ""; //付券方账户-帐户名称
            String sttlmTp1 = csbs002BodySttlmDtl.getSttlmTp1();//结算方式1
            String sttlmTp2 = csbs002BodySttlmDtl.getSttlmTp2();//结算方式2
            System.out.println("结算指令来源:" + instrOrgn);
            System.out.println("业务类别:" + bizTp);
            System.out.println("结算指令标识:" + instrId);
            System.out.println("收券方账户-帐户ID:" + takAcctId);
            System.out.println("收券方账户-帐户名称:" + takAcctNm);
            System.out.println("付券方账户-帐户ID:" + gavAcctId);
            System.out.println("付券方账户-帐户名称:" + gavAcctNm);
            System.out.println("结算方式1:" + sttlmTp1);
            System.out.println("结算方式1:" + sttlmTp2);

            List<Bond3> bd1Lst = csbs002BodySttlmDtl.getBd1();
            if (null != bd1Lst) {
                for (Bond3 csbs002bd1 : bd1Lst) {
                    String bdId1 = csbs002bd1.getBdId();//债券代码
                    String isin1 = csbs002bd1.getISIN();//ISIN编码
                    String bdShrtNm1 = csbs002bd1.getBdShrtNm();//债券简称
                    String BdAmt1 = csbs002bd1.getBdAmt();//券面总额
                    String BdCollRate1 = csbs002bd1.getBdCollRate();//债券质押率
                    String AcrdIntrst1 = csbs002bd1.getAcrdIntrstInf().getAcrdIntrst();//应计利息
                    String AcrdIntrstDt1 = csbs002bd1.getAcrdIntrstInf().getAcrdIntrstDt();//应计利息计息日
                    String Pric11 = csbs002bd1.getPric1();//债券价格1
                    String Pric21 = csbs002bd1.getPric2();//债券价格2
                    System.out.println("=>债券1信息:" + String.format("债券代码：%s,ISIN编码：%s,债券简称:%s,券面总额:%s,债券质押率:%s,应计利息:%s,应计利息计息日:%s,债券价格1:%s,债券价格2:%s", bdId1, isin1, bdShrtNm1, BdAmt1, BdCollRate1, AcrdIntrst1, AcrdIntrstDt1, Pric11, Pric21));
                }
            }
            // BJ0105债券借贷|BJ0106质押券置换|BJ0108BEPS解押|BJ0109BEPS质押券置换 会用到，其它的场景不涉及
            List<Bond3> bd2Lst = csbs002BodySttlmDtl.getBd2();
            if (null != bd2Lst) {
                for (Bond3 csbs002bd2 : bd2Lst) {
                    String bdId2 = csbs002bd2.getBdId();//债券代码
                    String isin2 = csbs002bd2.getISIN();//ISIN编码
                    String bdShrtNm2 = csbs002bd2.getBdShrtNm();//债券简称
                    String BdAmt2 = csbs002bd2.getBdAmt();//券面总额
                    String BdCollRate2 = csbs002bd2.getBdCollRate();//债券质押率
                    String AcrdIntrst2 = csbs002bd2.getAcrdIntrstInf().getAcrdIntrst();//应计利息
                    String AcrdIntrstDt2 = csbs002bd2.getAcrdIntrstInf().getAcrdIntrstDt();//应计利息计息日
                    String Pric12 = csbs002bd2.getPric1();//债券价格1
                    String Pric22 = csbs002bd2.getPric2();//债券价格2
                    System.out.println("=>债券2信息:" + String.format("债券代码：%s,ISIN编码：%s,债券简称:%s,券面总额:%s,债券质押率:%s,应计利息:%s,应计利息计息日:%s,债券价格1:%s,债券价格2:%s", bdId2, isin2, bdShrtNm2, BdAmt2, BdCollRate2, AcrdIntrst2, AcrdIntrstDt2, Pric12, Pric22));
                }
            }
            String val1 = csbs002BodySttlmDtl.getVal1();//金额1
            String val2 = csbs002BodySttlmDtl.getVal2();//金额2
            String val3 = csbs002BodySttlmDtl.getVal3();//金额3
            String val4 = csbs002BodySttlmDtl.getVal4();//金额4
            String aggtFaceAmt = csbs002BodySttlmDtl.getAggtFaceAmt();//券面总额合计
            String rate = csbs002BodySttlmDtl.getRate();//利率
            String terms = csbs002BodySttlmDtl.getTerms();//期限
            String dt1 = csbs002BodySttlmDtl.getDt1();//日期1
            String dt2 = csbs002BodySttlmDtl.getDt2();//日期2
            System.out.println("@" + String.format("金额1：%s,金额2：%s,金额3:%s,金额4:%s,券面总额合计:%s,利率:%s,期限:%s,日期1:%s,日期2:%s", val1, val2, val3, val4, aggtFaceAmt, rate, terms, dt1, dt2));
            //结算担保1
            SettlementGuarantee sttlmGrte1 = csbs002BodySttlmDtl.getSttlmGrte1();
            if (null != sttlmGrte1) {
                String grteTp1 = sttlmGrte1.getGrteTp();//保证方式
                CollateralBond1 grteBd1 = sttlmGrte1.getGrteBd();//保证券
                String collBdId1 = grteBd1.getBdId();//债券代码
                String collISIN1 = grteBd1.getISIN();//ISIN编码
                String collBdShrtNm1 = grteBd1.getBdShrtNm();//债券简称
                String collBdCollRate1 = grteBd1.getBdCollRate();//债券质押率
                String collBdAmt1 = grteBd1.getBdAmt();//券面总额
                String cshLctn1 = sttlmGrte1.getCshLctn();//保证金保管地
                String grtePricAmt1 = sttlmGrte1.getGrtePricAmt();//保证金金额
                System.out.println("=>结算担保1:" + String.format("保证方式：%s,债券代码:%s,ISIN编码:%s,债券简称:%s,债券质押率:%s,券面总额:%s,保证金保管地:%s,保证金金额:%s", grteTp1, collBdId1, collISIN1, collBdShrtNm1, collBdCollRate1, collBdAmt1, cshLctn1, grtePricAmt1));
            }
            //结算担保2
            SettlementGuarantee sttlmGrte2 = csbs002BodySttlmDtl.getSttlmGrte2();
            if (null != sttlmGrte2) {
                String grteTp2 = sttlmGrte2.getGrteTp();//保证方式
                CollateralBond1 grteBd2 = sttlmGrte1.getGrteBd();//保证券
                String collBdId2 = grteBd2.getBdId();//债券代码
                String collISIN2 = grteBd2.getISIN();//ISIN编码
                String collBdShrtNm2 = grteBd2.getBdShrtNm();//债券简称
                String collBdCollRate2 = grteBd2.getBdCollRate();//债券质押率
                String collBdAmt2 = grteBd2.getBdAmt();//券面总额
                String cshLctn2 = sttlmGrte2.getCshLctn();//保证金保管地
                String grtePricAmt2 = sttlmGrte2.getGrtePricAmt();//保证金金额
                System.out.println("=>结算担保2:" + String.format("保证方式：%s,债券代码:%s,ISIN编码:%s,债券简称:%s,债券质押率:%s,券面总额:%s,保证金保管地:%s,保证金金额:%s", grteTp2, collBdId2, collISIN2, collBdShrtNm2, collBdCollRate2, collBdAmt2, cshLctn2, grtePricAmt2));
            }
            String orgnlInstrId = csbs002BodySttlmDtl.getOrgnlInstrId();//原结算指令标识
            String orgnlCtrctId = csbs002BodySttlmDtl.getOrgnlCtrctId();//原结算合同标识
            System.out.println(String.format("原结算指令标识：%s,原结算合同标识:%s", orgnlInstrId, orgnlCtrctId));
            //转托管申请人
            PartyIdentification34 pi34 = csbs002BodySttlmDtl.getCrossTrfAplt();
            if (null != pi34) {
                String pi34Nm = pi34.getNm();//名称
                SafekeepingAccount saOut = pi34.getAcctInTrfOut();//在转出方债券账号
                String saOutAcctId = saOut.getAcctId();//帐户编号
                String saOutNm = saOut.getNm();//帐户名称
                SafekeepingAccount saIn = pi34.getAcctInTrfIn();//在转入方债券账号
                String saInAcctId = saIn.getAcctId();//帐户编号
                String saInNm = saIn.getNm();//帐户名称
                AgentParty apOut = pi34.getAgtPtyInTrfOut();//在转出方代理机构
                String apOutAptp = apOut.getAgtPtyTP();//代理类型
                PartyIdentification30 apOutPid30 = apOut.getAgtPtyId();
                String apOutPid30Nm = apOutPid30.getNm();//名称
                GenericIdentification4 apOutPid30Gi4 = apOutPid30.getPrtryId();
                String apOutPid30Gi4Id = apOutPid30Gi4.getId();//标识号
                String apOutPid30Gi4IdTp = apOutPid30Gi4.getIdTp();//标识类型
                AgentParty apIn = pi34.getAgtPtyInTrfOut();//在转入方代理机构
                String apInAptp = apIn.getAgtPtyTP();//代理类型
                PartyIdentification30 apInPid30 = apIn.getAgtPtyId();
                String apInPid30Nm = apInPid30.getNm();//名称
                GenericIdentification4 apInPid30Gi4 = apInPid30.getPrtryId();
                String apInPid30Gi4Id = apInPid30Gi4.getId();//标识号
                String apInPid30Gi4IdTp = apInPid30Gi4.getIdTp();//标识类型
                System.out.println("=>转托管申请人:" + String.format("名称：%s,在转出方债券账号-帐户编号:%s,在转出方债券账号-帐户名称:%s,帐户编号:%s,帐户名称:%s", pi34Nm, saOutAcctId, saOutNm, saInAcctId, saInNm));
                System.out.println("=>转托管申请人:" + String.format("代理类型：%s,名称:%s,标识号:%s,标识类型:%s,代理类型:%s", apOutAptp, apOutPid30Nm, apOutPid30Gi4Id, apOutPid30Gi4IdTp, apInAptp));
                System.out.println("=>转托管申请人:" + String.format("名称：%s,标识号:%s,标识类型:%s", apInPid30Nm, apInPid30Gi4Id, apInPid30Gi4IdTp));
            }
            String txId = csbs002BodySttlmDtl.getTxId();//业务标识号
            String pioNm = (null != csbs002BodySttlmDtl.getOprtr()) ? csbs002BodySttlmDtl.getOprtr().getNm() : ""; //经办人
            String picNm = (null != csbs002BodySttlmDtl.getChckr()) ? csbs002BodySttlmDtl.getChckr().getNm() : "";//复核人
            String picnNm = (null != csbs002BodySttlmDtl.getCnfrmr()) ? csbs002BodySttlmDtl.getCnfrmr().getNm() : "";//确认人
            System.out.println(String.format("业务标识号：%s,经办人:%s,复核人:%s,确认人:%s", txId, pioNm, picNm, picnNm));

        }

    }
}
