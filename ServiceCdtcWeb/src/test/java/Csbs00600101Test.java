import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00600101.*;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.component.GenericIdentification4;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdcc.model.header.Vrsn;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 2021/11/27
 *
 * @Auther:zk
 */
public class Csbs00600101Test {
        public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs00600101 = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Body body = csbs00600101.getBody();

        /**************************报文头*****************************/
        MsgHeader msgHeader = csbs00600101.getSysHeader(MsgHeader.class);

        Vrsn vrsn = new Vrsn();
        vrsn.setVrsnID("01");
        msgHeader.setVrsn(vrsn);

        Extension xtnsn1 = new Extension();
        xtnsn1.setXtnsnTxt("1");
        Extension xtnsn2 = new Extension();
        xtnsn2.setXtnsnTxt("12");
        List<Extension> xtnsns = new ArrayList<>();
        xtnsns.add(xtnsn1);
        xtnsns.add(xtnsn2);
        msgHeader.setXtnsn(xtnsns);


        /**************************报文体*****************************/
        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);

        QryRsltAbst qryRsltAbst = new QryRsltAbst();
        qryRsltAbst.setQryRsltCnt("qry");
        qryRsltAbst.setTxRsltCd("txrs");
        body.setQryRsltAbst(qryRsltAbst);

        ArrayList<BtchQryRslt> btchQryRslts = new ArrayList<>();
        BtchQryRslt btchQryRslt = new BtchQryRslt();
        btchQryRslt.setInstrId("85552145412");
        AccountIdentificationAndName4 accountIdentificationAndName4 = new AccountIdentificationAndName4();
        accountIdentificationAndName4.setId("123");
        accountIdentificationAndName4.setNm("阿斯蒂芬");
        btchQryRslt.setCtrctId("11");
        btchQryRslt.setGivAcct(accountIdentificationAndName4);
        btchQryRslt.setTxId("5548");
        btchQryRslt.setBizTp("776");
        btchQryRslt.setSttlmTp1("654");
        btchQryRslt.setSttlmTp2("65487");
        btchQryRslt.setBdCnt("5645");
        btchQryRslt.setAggtFaceAmt("100000.0");
        btchQryRslt.setVal1("654");
        btchQryRslt.setVal2("7666");
        btchQryRslt.setDt1("5768");
        btchQryRslt.setDt2("1000");
        btchQryRslt.setInstrSts("654987");
        btchQryRslt.setCtrctSts("654987");
        btchQryRslt.setCtrctBlckSts("654987");
        btchQryRslt.setLastUpdTm("654987");
        btchQryRslt.setOrgtrCnfrmInd("654987");
        btchQryRslt.setCtrCnfrmInd("654987");
        btchQryRslt.setInstrOrgn("654987");
        BtchQryRslt btchQryRslt2 = new BtchQryRslt();
        btchQryRslt2.setInstrId("8555");
        AccountIdentificationAndName4 accountIdentificationAndName47 = new AccountIdentificationAndName4();
        accountIdentificationAndName47.setId("12");
        accountIdentificationAndName47.setNm("自行车v");
        btchQryRslt2.setCtrctId("116");
        btchQryRslt2.setGivAcct(accountIdentificationAndName47);
        btchQryRslt2.setTxId("5548");
        btchQryRslt2.setBizTp("776");
        btchQryRslt2.setSttlmTp1("654");
        btchQryRslt2.setSttlmTp2("65487");
        btchQryRslt2.setBdCnt("5645");
        btchQryRslt2.setAggtFaceAmt("100000.0");
        btchQryRslt2.setVal1("654");
        btchQryRslt2.setVal2("7666");
        btchQryRslt2.setDt1("5768");
        btchQryRslt2.setDt2("1000");
        btchQryRslt2.setInstrSts("654987");
        btchQryRslt2.setCtrctSts("654987");
        btchQryRslt2.setCtrctBlckSts("654987");
        btchQryRslt2.setLastUpdTm("654987");
        btchQryRslt2.setOrgtrCnfrmInd("654987");
        btchQryRslt2.setCtrCnfrmInd("654987");
        btchQryRslt2.setInstrOrgn("654987");
        btchQryRslts.add(btchQryRslt);
        btchQryRslts.add(btchQryRslt2);
        body.setBtchQryRslt(btchQryRslts);

        QryPty qryPty = new QryPty();
        qryPty.setNm("ytj");
        GenericIdentification4 genericIdentification4 = new GenericIdentification4();
        genericIdentification4.setId("1234");
        genericIdentification4.setIdTp("fgh");
        qryPty.setPrtryId(genericIdentification4);
        body.setQryPty(qryPty);

        ArrayList<Extension> extensions = new ArrayList<>();
        Extension extension = new Extension();
        extension.setXtnsnTxt("321654");
        Extension extension1 = new Extension();
        extension1.setXtnsnTxt("321654");
        extensions.add(extension1);
        extensions.add(extension);
        body.setXtnsn(extensions);

        String xml = XmlUtils.toXml(csbs00600101);
        System.out.println(xml);
        //解析报文
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml, csbs00600101);
        //存在多个头，需要指定
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        Body body1 = bodyEsbPacket.getBody();
        System.out.println(msgHeader1.getVrsn().getVrsnID());


        List<BtchQryRslt> btchQryRslt1 = body1.getBtchQryRslt();
        for (BtchQryRslt qryRslt : btchQryRslt1) {
            System.out.println(qryRslt);
        }
//        for (Bond3 bond32 : bond3s1) {
//            System.out.println("反解结果:"+bond32.getBdId() + ":=>" + bond32.getBdShrtNm() + ":=>" + bond32.getBdAmt());
//        }

    }
}
