import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.CSBS00900101.Body;
import com.singlee.financial.cdcc.model.CSBS00900101.Msg;
import com.singlee.financial.cdcc.model.CSBS00900101.SysXcptn;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.component.PartyIdentification;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdcc.model.header.Vrsn;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 17:23
 * =======================
 */
public class Csbs00900101Test {
    public static void main(String[] args) throws Exception {

        EsbPacket<com.singlee.financial.cdcc.model.CSBS00900101.Body> csbs00900101 = EsbPacket.initEsbPacket(com.singlee.financial.cdcc.model.CSBS00900101.Body.class, MsgHeader.class, "Root");
        com.singlee.financial.cdcc.model.CSBS00900101.Body body = csbs00900101.getBody();

        /**************************报文头*****************************/
        MsgHeader msgHeader = csbs00900101.getSysHeader(MsgHeader.class);

        Vrsn vrsn = new Vrsn();
        vrsn.setVrsnID("01");
        msgHeader.setVrsn(vrsn);

        Extension xtnsn = new Extension();
        List<Extension> lst = new ArrayList<Extension>();
        xtnsn.setXtnsnTxt("1");
        lst.add(xtnsn);
        xtnsn.setXtnsnTxt("12");
        lst.add(xtnsn);
        xtnsn.setXtnsnTxt("123");
        lst.add(xtnsn);
        msgHeader.setXtnsn(lst);

        /**************************报文体*****************************/
        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        //经办人
        PartyIdentification partyIdentification = new PartyIdentification();
        partyIdentification.setNm("kaka");
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);

        //2.0系统异常信息
        SysXcptn sysXcptn= new SysXcptn();
        sysXcptn.setXcptnRtrCd("llkkkl");
        sysXcptn.setXcptnRtrTx("llkkkl");
        body.setSysXcptn(sysXcptn);

        //4.0扩展项
        ArrayList<Extension> extensions = new ArrayList<>();
        Extension extension = new Extension();
        extension.setXtnsnTxt("321654");
        Extension extension1 = new Extension();
        extension1.setXtnsnTxt("321654");
        extensions.add(extension1);
        extensions.add(extension);
        body.setXtnsn(extensions);

        String xml = XmlUtils.toXml(csbs00900101);
        System.out.println(xml);
        //解析报文
        EsbPacket<com.singlee.financial.cdcc.model.CSBS00900101.Body> bodyEsbPacket = XmlUtils.fromXML(xml, csbs00900101);
        //存在多个头，需要指定
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        Body body1 = bodyEsbPacket.getBody();
        System.out.println(msgHeader1.getVrsn().getVrsnID());


        EsbSubPacketConvert s= body1.getBodyConvert();
        System.out.println(s);

    }
}
