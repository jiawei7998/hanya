import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS01200101.Body;
import com.singlee.financial.cdcc.model.CSBS01200101.HrtBtMsg;
import com.singlee.financial.cdcc.model.CSBS01200101.Msg;
import com.singlee.financial.cdcc.model.header.MsgHeader;

/**
 *
 * CSBS.012.001.01	心跳消息	HeartBeatMessage 结算成员->CSBS CSBS->结算成员
 *
 */

public class Test012 {

    public static void main(String[] args) throws Exception {
        //1.1组装请求报文
        EsbPacket<Body> esbPacket = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Msg msg = new Msg();
        msg.setTxFlowId("9402");
        msg.setPrty("PT02");
        msg.setCreDtTm("2011-08-30T16:25:58.648");
        HrtBtMsg hrtBtMsg = new HrtBtMsg();
        hrtBtMsg.setHrtBtId("20211129");
        esbPacket.getBody().setHrtBtMsg(hrtBtMsg);
        esbPacket.getBody().setMsg(msg);

//        //1.2封装返回报文
//        EsbPacket<Body> responsePacket = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
//
//        //2.向中债发送请求报文,并获取返回实体
//        responsePacket = CdccClient.send(esbPacket,responsePacket);
//        if(responsePacket == null){
//            System.out.println("中债报文发送失败");
//        }
//        //3.解析返回报文并判断
//        if("CSBS.009.001.01".equals(responsePacket)){
//
//        }else {
//
//        }




    }


}
