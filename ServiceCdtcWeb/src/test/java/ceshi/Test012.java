package ceshi;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.CdtcDictConstants;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS01200101.Body;
import com.singlee.financial.cdcc.model.CSBS01200101.HrtBtMsg;
import com.singlee.financial.cdcc.model.CSBS01200101.Msg;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

public class Test012 {

    public static void main(String[] args) throws Exception {
        Log log = LogFactory.getLog(Test012.class);

        //1.1组装请求报文
        EsbPacket<Body> esbPacket = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Msg msg = new Msg();
        msg.setTxFlowId("9402");
        msg.setPrty(CdtcDictConstants.Priority4Code.PT02);
        msg.setCreDtTm("2011-08-30T16:25:58.648");
        HrtBtMsg hrtBtMsg = new HrtBtMsg();
        hrtBtMsg.setHrtBtId("20211129");
        esbPacket.getBody().setHrtBtMsg(hrtBtMsg);
        esbPacket.getBody().setMsg(msg);

        //2.向中债发送请求报文,并获取返回实体
        String backXml = CdtcCommonUtils.send(esbPacket);
        if(backXml == null){
            log.info("中债报文发送失败");
            return;
        }

        //3.解析返回报文并判断
        //获取报文返回类型
        String msgTp = CdtcCommonUtils.getMsgTp(backXml);
        log.info("MsgTp:" + msgTp);



        if(msgTp==null||"CSBS.009.001.01".equals(msgTp)){
            log.info("接受CSBS.009.001.01 失败报文");
            Map<String,Object> resultMap = CdtcCommonUtils.cdtcFailPacket(backXml);
            String errorMessage = ParameterUtil.getString(resultMap,"errorMessage","");
            String detaiErrorlMessage = ParameterUtil.getString(resultMap,"detaiErrorlMessage","");
            log.info("回购交易报文发送出现异常,异常描述为："+ errorMessage);
            log.info("详细异常描述为："+detaiErrorlMessage);
        }else {

            EsbPacket<Body> responsePacket = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
            responsePacket = CdtcCommonUtils.transformRep(backXml,responsePacket);

            HrtBtMsg hrtBtMsg1 = responsePacket.getBody().getHrtBtMsg();
            String hrtBtId = hrtBtMsg1.getHrtBtId();
            String hrtBtRefId = hrtBtMsg1.getHrtBtRefId();
            if(StringUtils.isNotBlank(hrtBtId)&&hrtBtId.equals(hrtBtRefId)){
                log.info("连接成功");
            }else{
                log.info("连接失败");
            }
        }




    }


}
