package ceshi;

/**
 *  通用结算指令请求（CSBS.001.001.01 CommonDistributionSettlementInstruction） 结算成员-->CSBS
 *
 *  “通用结算指令请求”报文由结算成员发送给中央债券综合业务系统，用于：
 *   结算成员发起初始结算指令；
 *   结算成员一方收到中央债券综合业务系统的“通用结算指令状态报告”报文后，回复中央债券综合业务系统表示其确认或拒绝该笔结算业务
 *
 */
public class Test001Repo {

    /**
     *  回购待确认指令的确认操作
     */
    public static void  repoInstructionConfirm(){

    }

}
