package ceshi;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class TestUtils {
    public static void main(String[] args) {

        System.out.println(getCdtcUtcTimes());
    }

    public static String getCdtcUtcTimes(){
        String format = "yyyy-MM-dd'T'HH:mm:ss.SSS";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
        sdf.setDateFormatSymbols(new DateFormatSymbols(Locale.US));

        TimeZone timezone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timezone);
        return sdf.format(calendar.getTime());
    }

    public static DateFormat createLocalDateFormat(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
        sdf.setDateFormatSymbols(new DateFormatSymbols(Locale.US));

        return sdf;
    }
}
