package ceshi;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.CdtcDictConstants;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00200101.SttlmDtl;
import com.singlee.financial.cdcc.model.CSBS00500101.Body;
import com.singlee.financial.cdcc.model.CSBS00500101.Msg;
import com.singlee.financial.cdcc.model.CSBS00500101.OprtrFlg;
import com.singlee.financial.cdcc.model.component.*;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdtc.msg.parse.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *   结算业务查询请求（CSBS.005.001.01 SettlementBusinessQueryRequest V01）结算成员-->CSBS
 *   “结算业务查询请求”报文由结算成员发送给中央债券综合业务系统，用于：
 *   结算成员进行结算业务信息详情查询及批量查询。
 */
public class TestInstrDetail {

    public static void main(String[] args) {
        instrDetailQuery();
    }
    /**
     * 指令详情查询
     */
    public static void instrDetailQuery(){
        try {
            //1。组装中债请求报文
            EsbPacket<Body> instrDetailPacket = packAge();

            //2.向中债发送请求报文,并获取返回实体
            String backXml = CdtcCommonUtils.send(instrDetailPacket);

            //3.解析返回报文并判断
            parsingXml(backXml);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static EsbPacket<Body> packAge() throws IllegalAccessException, InstantiationException {
        EsbPacket<Body> instrDetailPacket = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Body body = instrDetailPacket.getBody();

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-12-06T14:35:49.396");
        body.setMsg(msg);

        //操作标识
        OprtrFlg oprtrFlg = new OprtrFlg();
        //操作码
        oprtrFlg.setOprtrSts(CdtcDictConstants.OperatorStatusCode.OS04);
        oprtrFlg.setReqTp(CdtcDictConstants.RequestTypeCode.BJ0300);
        body.setOprtrFlg(oprtrFlg);

        //查询账户
        AccountIdentificationAndName4 acctQry = new AccountIdentificationAndName4();
        acctQry.setId("A0118000001");
        body.setAcctQry(acctQry);

        //查询条件
        Condition qryCond = new Condition();
        qryCond.setId("000100193447");
        body.setQryCond(qryCond);

        return instrDetailPacket;
    }

    private static void parsingXml(String backXml) throws IOException, DocumentException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        Log log = LogFactory.getLog(TestInstrBatch.class);

        if(backXml == null){
            log.info("指令详情查询报文发送后没有返回要解析的报文文件，请查看接口机是否正常工作");
            log.info("SendStatus:FAIL");
            log.info("SendMessage:没有返回要解析的报文文件，请查看接口机是否正常工作");
            return;
        }

        //获取报文返回类型
        String msgTp = CdtcCommonUtils.getMsgTp(backXml);
        log.info("MsgTp:" + msgTp);

        if(msgTp==null||"CSBS.009.001.01".equals(msgTp)){
            log.info("接收CSBS.009.001.01 失败报文");
            //解析失败报文
            Map<String,Object> resultMap = CdtcCommonUtils.cdtcFailPacket(backXml);
            String errorMessage = ParameterUtil.getString(resultMap,"errorMessage","");
            String detaiErrorlMessage = ParameterUtil.getString(resultMap,"detaiErrorlMessage","");
            log.info("指令详情查询发送出现异常,异常描述为："+ errorMessage);
            log.info("SendStatus:FAIL");
            log.info("SendMessage:"+errorMessage);
            log.info("详细异常描述为："+detaiErrorlMessage);
        }else{
            //解析成功返回报文
            EsbPacket<com.singlee.financial.cdcc.model.CSBS00200101.Body> reponsePacket = EsbPacket.initEsbPacket(com.singlee.financial.cdcc.model.CSBS00200101.Body.class,MsgHeader.class, "Root");
            reponsePacket = CdtcCommonUtils.transformRep(backXml,reponsePacket);
            com.singlee.financial.cdcc.model.CSBS00200101.Body body = reponsePacket.getBody();
            //指令状态信息
            InstructionStatusInfo InstrStsInf =  body.getStsFlg().getInstrStsInf();
            //指令处理状态
            String  InstrSts =  InstrStsInf.getInstrSts();
            //发令方确认标识
            String  OrgtrCnfrmInd = InstrStsInf.getOrgtrCnfrmInd();
            //对手方确认标识
            String  CtrCnfrmInd =  InstrStsInf.getCtrCnfrmInd();

            //结算详情
            SttlmDtl SttlmDtl = body.getSttlmDtl();
            //结算指令标识
            String InstrId = SttlmDtl.getInstrId();
            //结算指令来源
            String InstrOrgn = SttlmDtl.getInstrOrgn();
            //业务类别
            String BizTp = SttlmDtl.getBizTp();

            //付券方账户
            AccountIdentificationAndName4 giveAcct = SttlmDtl.getGivAcct();
            giveAcct.getId();
            giveAcct.getNm();
            //收券方账户
            AccountIdentificationAndName4 takAcct = SttlmDtl.getTakAcct();
            takAcct.getId();
            takAcct.getNm();

            //结算方式1
            String SttlmTp1 = SttlmDtl.getSttlmTp1();
            //结算方式2
            String SttlmTp2 = SttlmDtl.getSttlmTp2();
            //债券
            List<Bond3> Bd1List = SttlmDtl.getBd1();
            for(int i =0 ;i<Bd1List.size();i++){
                Bond3 bond3 = Bd1List.get(i);
                //债券代码
                bond3.getBdId();
                //债券简称
                bond3.getBdShrtNm();
                //券面总额
                bond3.getBdAmt();
                //应计利息信息
                AccruedInterestInformation AcrdIntrstInf = bond3.getAcrdIntrstInf();
                if(AcrdIntrstInf!=null){
                    //应计利息
                    String AcrdIntrst = AcrdIntrstInf.getAcrdIntrst();
                    //计息日
                    String AcrdIntrstDt = AcrdIntrstInf.getAcrdIntrstDt();
                }
                //债券价格1
                String Pric1 = bond3.getPric1();
                //债券价格2
                String Pric2 = bond3.getPric2();
            }
            //金额1
            String Val1 = SttlmDtl.getVal1();
            //金额2
            String Val2 = SttlmDtl.getVal2();
            //金额3
            String Val3 = SttlmDtl.getVal3();

            //日期1
            String Dt1 = SttlmDtl.getDt1();
            //日期2
            String Dt2 = SttlmDtl.getDt2();
            //业务标识号
            String TxId = SttlmDtl.getTxId();
            //券面总额合计
            String AggtFaceAmt = SttlmDtl.getAggtFaceAmt();
            //利率
            String Rate = SttlmDtl.getRate();

        }
    }
}
