package ceshi;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.CdtcDictConstants;
import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00100101.Body;
import com.singlee.financial.cdcc.model.CSBS00100101.Msg;
import com.singlee.financial.cdcc.model.CSBS00100101.OprtrFlg;
import com.singlee.financial.cdcc.model.CSBS00100101.SttlmDtl;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.Bond3;
import com.singlee.financial.cdcc.model.component.InstructionStatusInfo;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  通用结算指令请求（CSBS.001.001.01 CommonDistributionSettlementInstruction） 结算成员-->CSBS
 *
 *  “通用结算指令请求”报文由结算成员发送给中央债券综合业务系统，用于：
 *   结算成员发起初始结算指令；
 *   结算成员一方收到中央债券综合业务系统的“通用结算指令状态报告”报文后，回复中央债券综合业务系统表示其确认或拒绝该笔结算业务
 *
 */
public class Test001Cbt {

    /**
     *  现券待确认指令的确认操作
     */
    public static void  cbtInstructionConfirm(){


        try {
            //1。组装中债请求报文
            EsbPacket<Body> cbtSendPacket = packAge();

            //2.向中债发送请求报文,并获取返回实体
            String backXml = CdtcCommonUtils.send(cbtSendPacket);

            //3.解析返回报文并判断
            parsingXml(backXml);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 解析返回报文
     * @param backXml
     */
    private static void parsingXml(String backXml) throws IOException, DocumentException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        Log log = LogFactory.getLog(Test001Cbt.class);
        if(backXml == null){
            log.info("现券交易确认报文发送后没有返回要解析的报文文件，请查看接口机是否正常工作");
            log.info("SendStatus:FAIL");
            log.info("SendMessage:没有返回要解析的报文文件，请查看接口机是否正常工作");
            return;
        }
        //获取报文返回类型
        String msgTp = CdtcCommonUtils.getMsgTp(backXml);
        log.info("MsgTp:" + msgTp);

        if(msgTp==null||"CSBS.009.001.01".equals(msgTp)){
            log.info("接收CSBS.009.001.01 失败报文");
            //解析失败报文
            Map<String,Object> resultMap = CdtcCommonUtils.cdtcFailPacket(backXml);
            String errorMessage = ParameterUtil.getString(resultMap,"errorMessage","");
            String detaiErrorlMessage = ParameterUtil.getString(resultMap,"detaiErrorlMessage","");
            log.info("现券交易报文发送出现异常,异常描述为："+ errorMessage);
            log.info("SendStatus:FAIL");
            log.info("SendMessage:"+errorMessage);
            log.info("详细异常描述为："+detaiErrorlMessage);
        }else {
            //解析成功返回报文
            EsbPacket<com.singlee.financial.cdcc.model.CSBS00200101.Body> reponsePacket =  EsbPacket.initEsbPacket(com.singlee.financial.cdcc.model.CSBS00200101.Body.class, MsgHeader.class, "Root");
            reponsePacket = CdtcCommonUtils.transformRep(backXml,reponsePacket);
            com.singlee.financial.cdcc.model.CSBS00200101.Body reponseBody = reponsePacket.getBody();
            //指令状态信息
            InstructionStatusInfo instrStsInf = reponseBody.getStsFlg().getInstrStsInf();
            //指令处理结果返回码
            String txRsltCd = instrStsInf!=null?instrStsInf.getTxRsltCd():"";
            if(StringUtils.isNotEmpty(txRsltCd)){
                //判断是否
                if(CdtcCommonUtils.msgReturn(txRsltCd)){
                    //指令处理状态
                    String  InstrSts =instrStsInf.getInstrSts();;
                    if(CdtcDictConstants.InstructionStatusCode.IS00.equals(InstrSts)){
                        log.info("SendStatus:SUCCESS");
                        log.info("SendMessage:双方已成功确认");
                    }else {
                        log.info("SendStatus:SEND");
                        log.info("SendMessage:确认发送成功");
                    }
                    //发令方确认标识
                    String  OrgtrCnfrmInd = instrStsInf.getOrgtrCnfrmInd();
                    //对手方确认标识
                    String  CtrCnfrmInd = instrStsInf.getCtrCnfrmInd();
                    log.info("现券交易报文已发送确认");

                }else{
                    log.info("SendStatus:FAIL");
                    //错误信息
                    String InsErrInf = instrStsInf.getInsErrInf();
                    log.info("SendMessage:"+InsErrInf);
                    log.info("现券交易报文确认失败：");
                }

            }else {
                System.out.println("SendStatus:FAIL");
                log.info("现券交易确认指令返回报文不完整，缺少TxRsltCd标签");
            }
        }
    }

    /**
     * 组装报文
     * @param <T>
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private static <T extends EsbBodyPacket> EsbPacket<T> packAge() throws IllegalAccessException, InstantiationException {
        EsbPacket<Body> cbtSendPacket = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");

        Body body = cbtSendPacket.getBody();

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-12-02T14:35:49.396");
        body.setMsg(msg);

        OprtrFlg oprtrFlg = new OprtrFlg();
        oprtrFlg.setReqTp(CdtcDictConstants.RequestTypeCode.BJ0101);//类型
        //根据交易方向判断p-收券方确认、s-付券方确认
        oprtrFlg.setOprtrSts(CdtcCommonUtils.getPsOprtrSts("P"));
        body.setOprtrFlg(oprtrFlg);

        SttlmDtl sttlmDtl = new SttlmDtl();
        //交易来源
        sttlmDtl.setInstrOrgn(CdtcDictConstants.InstructionOriginCode.IO03);
        // 业务类别
        sttlmDtl.setBizTp(CdtcDictConstants.BusinessTypeCode.BT01);
        sttlmDtl.setInstrId("1111111");//结算指令标识
        //付券方
        AccountIdentificationAndName4 givAcct = new AccountIdentificationAndName4();
        givAcct.setId("00000007041");
        givAcct.setNm("江苏江南农商行");
        sttlmDtl.setGivAcct(givAcct);
        //收券方
        AccountIdentificationAndName4 takAcct = new AccountIdentificationAndName4();
        takAcct.setNm("哈尔滨银行");
        takAcct.setId("A0118000001");
        sttlmDtl.setTakAcct(takAcct);
        sttlmDtl.setSttlmTp1(CdtcDictConstants.BondSettlementTypeCode.ST03);
        sttlmDtl.setSttlmTp2("");

        Bond3 bond3 = new Bond3();
        bond3.setBdId("120223");
        bond3.setBdShrtNm("12国开23");
        bond3.setBdAmt("15000.00");
        //净价
        bond3.setPric1("98.97");
        //全价
        bond3.setPric2("99.29262295");
        List<Bond3> bond3List = new ArrayList<Bond3>();
        bond3List.add(bond3);
        sttlmDtl.setBd1(bond3List);

        //金额1--首期结算金额
        sttlmDtl.setVal1("148455000.00");
        //金额2--到期结算金额
        sttlmDtl.setVal2("148938934.43");
        //金额3--应计利息
        sttlmDtl.setVal3("483934.43");
        //首期交割日
        sttlmDtl.setDt1("2021-12-03");
        //业务标识号
        sttlmDtl.setTxId("CBT20150608002073");
        body.setSttlmDtl(sttlmDtl);

        return (EsbPacket<T>) cbtSendPacket;
    }

    public static void main(String[] args) {
        cbtInstructionConfirm();
    }

    public static void testResponseXml() {
        String xml="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<Root>\n" +
                "  <MsgHeader>\n" +
                "    <Vrsn>\n" +
                "      <VrsnID>01</VrsnID>\n" +
                "    </Vrsn>\n" +
                "    <Sndr>\n" +
                "      <SndrId>0000007798</SndrId>\n" +
                "      <SndrSysId>DS</SndrSysId>\n" +
                "    </Sndr>\n" +
                "    <Rcvr>\n" +
                "      <RcvrId>0000000002</RcvrId>\n" +
                "      <RcvrSysId>DI</RcvrSysId>\n" +
                "    </Rcvr>\n" +
                "    <MsgDesc>\n" +
                "      <MsgOrgnlSndr>0000007798</MsgOrgnlSndr>\n" +
                "      <MsgFnlRcvr>0000000002</MsgFnlRcvr>\n" +
                "      <MsgSndDateTime>2011-07-20T16:49:51.764</MsgSndDateTime>\n" +
                "      <MsgTp>CSBS.002.001.01</MsgTp>\n" +
                "      <MsgId>00000077982011072000129114</MsgId>\n" +
                "      <MsgRefId>00000077982011072000129114</MsgRefId>\n" +
                "      <MsgBodyChcksum>NULLL</MsgBodyChcksum>\n" +
                "    </MsgDesc>\n" +
                "    <Sgntr>\n" +
                "      <SttlmMmbSgntr>NULLL</SttlmMmbSgntr>\n" +
                "      <CSBSSgntr>NULLL</CSBSSgntr>\n" +
                "    </Sgntr>\n" +
                "  </MsgHeader>\n" +
                "  <Document>\n" +
                "    <Msg> \n" +
                "      <TxFlowId>9402</TxFlowId>  \n" +
                "      <Prty>PT02</Prty>  \n" +
                "      <CreDtTm>2011-07-20T16:49:51.764</CreDtTm> \n" +
                "    </Msg>\n" +
                "    <StsFlg>\n" +
                "      <InstrStsInf>\n" +
                "        <InstrSts>IS03</InstrSts>\n" +
                "        <TxRsltCd>BJ000000</TxRsltCd>\n" +
                "      </InstrStsInf>\n" +
                "    </StsFlg>\n" +
                "    <SttlmDtl>\n" +
                "      <InstrOrgn>IO01</InstrOrgn>\n" +
                "      <BizTp>BT01</BizTp>\n" +
                "      <InstrId>000005574700</InstrId>\n" +
                "      <GivAcct>\n" +
                "        <Nm>工商银行</Nm>\n" +
                "        <Id>A0001000001</Id>\n" +
                "      </GivAcct>\n" +
                "      <TakAcct>\n" +
                "        <Nm>中国银行</Nm>\n" +
                "        <Id>A0003000001</Id>\n" +
                "      </TakAcct>\n" +
                "      <SttlmTp1>ST00</SttlmTp1>\n" +
                "      <Bd1>\n" +
                "        <BdId>9802</BdId>\n" +
                "        <BdShrtNm>98国债2</BdShrtNm>\n" +
                "        <BdAmt>1.00</BdAmt>\n" +
                "        <AcrdIntrstInf>\n" +
                "          <AcrdIntrst>3.27452055</AcrdIntrst>\n" +
                "          <AcrdIntrstDt>2011-02-02</AcrdIntrstDt>\n" +
                "        </AcrdIntrstInf>\n" +
                "        <Pric1>96.72550000</Pric1>\n" +
                "        <Pric2>100.00002055</Pric2>\n" +
                "      </Bd1>\n" +
                "      <Val1>9672.55</Val1>\n" +
                "      <Val2>10000.00</Val2>\n" +
                "      <Val3>327.45</Val3>\n" +
                "      <Dt1>2011-02-02</Dt1>\n" +
                "      <TxId>222</TxId>\n" +
                "      <Oprtr>\n" +
                "        <Nm>工行直联用户</Nm>\n" +
                "      </Oprtr>\n" +
                "      <Chckr>\n" +
                "        <Nm>工行直联用户</Nm>\n" +
                "      </Chckr>\n" +
                "    </SttlmDtl>\n" +
                "  </Document>\n" +
                "</Root>";
        try {
            parsingXml(xml);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
