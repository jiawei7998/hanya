package ceshi;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.CdtcDictConstants;
import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00700101.Body;
import com.singlee.financial.cdcc.model.CSBS00700101.Msg;
import com.singlee.financial.cdcc.model.CSBS00700101.OprtrFlg;
import com.singlee.financial.cdcc.model.CSBS00700101.QryItm;
import com.singlee.financial.cdcc.model.CSBS00800101.StmtRpt;
import com.singlee.financial.cdcc.model.component.*;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *  债券账户对账单查询请求 （CSBS.007.001.01 SafekeepingAccountStatementQueryRequestV01）结算成员-->CSBS
 *  “债券账户对账单查询请求”报文由结算成员发送给中央债券综合业务系统，用于：
 *  结算成员进行债券账户账务信息查询。
 */
public class TestdetailCheck {

    public static void main(String[] args) {
        detailCheck();
    }
    /**
     * 明细对帐单查询
     */
    public static void detailCheck(){
        try {
            //1。组装中债请求报文
            EsbPacket<Body> detailCheckPacket = packAge();

            //2.向中债发送请求报文,并获取返回实体
            String backXml = CdtcCommonUtils.send(detailCheckPacket);

            //3.解析返回报文并判断
            parsingXml(backXml);
        }catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static  <T extends EsbBodyPacket> EsbPacket<T> packAge() throws IllegalAccessException, InstantiationException {
        EsbPacket<Body>  detailCheckPacket =  EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Body body = detailCheckPacket.getBody();

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-12-06T14:35:49.396");
        body.setMsg(msg);

        //操作标识
        OprtrFlg oprtrFlg = new OprtrFlg();
        //操作码
        oprtrFlg.setOprtrSts(CdtcDictConstants.OperatorStatusCode.OS08);
        oprtrFlg.setReqTp(CdtcDictConstants.RequestTypeCode.BJ0400);
        body.setOprtrFlg(oprtrFlg);

        QryItm qryItm = new QryItm();

        QueryDate QryDt = new QueryDate();

        //查询日期
        QryDt.setStartDt("2021-11-23");
        QryDt.setEndDt("2021-11-23");
        qryItm.setQryDt(QryDt);
        //查询账户与债券
        AccountIdentificationAndName7 SfkpgAcctBdQry = new AccountIdentificationAndName7();
        /**
         * 账户编号
         */
        SfkpgAcctBdQry.setId("A0118000001");
        /**
         * 账户对账单类型 债权--AS00,额度--AS01
         */
        SfkpgAcctBdQry.setAccStTp(CdtcDictConstants.AccountSatementType.AS01);
        //查询债券
        Bond1 bond1 = new Bond1();
        bond1.setBdId("160010");
        SfkpgAcctBdQry.setQryBd(bond1);
        qryItm.setSfkpgAcctBdQry(SfkpgAcctBdQry);
        body.setQryItm(qryItm);

        return (EsbPacket<T>) detailCheckPacket;
    }

    private static void parsingXml(String backXml) throws IOException, DocumentException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        Log log = LogFactory.getLog(TestdetailCheck.class);

        if(backXml == null){
            log.info("明细对帐单查询报文发送后没有返回要解析的报文文件，请查看接口机是否正常工作");
            log.info("SendStatus:FAIL");
            log.info("SendMessage:没有返回要解析的报文文件，请查看接口机是否正常工作");
            return;
        }

        //获取报文返回类型
        String msgTp = CdtcCommonUtils.getMsgTp(backXml);
        log.info("MsgTp:" + msgTp);

        if(msgTp==null||"CSBS.009.001.01".equals(msgTp)){
            log.info("接收CSBS.009.001.01 失败报文");
            //解析失败报文
            Map<String,Object> resultMap = CdtcCommonUtils.cdtcFailPacket(backXml);
            String errorMessage = ParameterUtil.getString(resultMap,"errorMessage","");
            String detaiErrorlMessage = ParameterUtil.getString(resultMap,"detaiErrorlMessage","");
            log.info("明细对帐单查询发送出现异常,异常描述为："+ errorMessage);
            log.info("SendStatus:FAIL");
            log.info("SendMessage:"+errorMessage);
            log.info("详细异常描述为："+detaiErrorlMessage);
        }else{
            //解析成功返回报文
            EsbPacket<com.singlee.financial.cdcc.model.CSBS00800101.Body> reponsePacket = EsbPacket.initEsbPacket(com.singlee.financial.cdcc.model.CSBS00800101.Body.class,MsgHeader.class, "Root");
            reponsePacket = CdtcCommonUtils.transformRep(backXml,reponsePacket);
            com.singlee.financial.cdcc.model.CSBS00800101.Body body = reponsePacket.getBody();
            //对账报告
            StmtRpt StmtRpt= body.getStmtRpt();
            // 起始日期
            String StartDt = StmtRpt.getStartDt();
            // 截止日期
            String EndDt = StmtRpt.getEndDt();
            // 债券账户计数
            String  SfkpgAcctCnt = StmtRpt.getSfkpgAcctCnt();
            //债券账户对账单信息
            List<AccountIdentificationAndName5> SfkpgAcctStmtInf = StmtRpt.getSfkpgAcctStmtInf();

            for(int i=0;i<SfkpgAcctStmtInf.size();i++){
                AccountIdentificationAndName5 andName5 = SfkpgAcctStmtInf.get(i);
                //账户名称
                String Nm = andName5.getNm();
                // 账户编号
                String Id = andName5.getId();
                //债券数目
                String BdCnt = andName5.getBdCnt();
                //债券对账单
                List<Bond2> Bd = andName5.getBd();
                int size = Bd==null?0:Bd.size();
                for(int j=0 ;j<size;j++){
                    Bond2 bond2 = Bd.get(j);
                    //债券代码
                    String BdId = bond2.getBdId();
                    //ISIN编码
                    String ISIN = bond2.getISIN();
                    //债券简称
                    String BdShrtNm = bond2.getBdShrtNm();
                    //债券币种
                    String CcyTp = bond2.getCcyTp();
                    //债券性质
                    String BdParam = bond2.getBdParam();

                    //托管总对账单
                    SummaryStatement SummryStmt = bond2.getSummryStmt();
                    //日期
                    String Dt = SummryStmt.getDt();
                    //科目余额
                    List<AccountLedger> AcctLdgList = SummryStmt.getAcctLdg();
                    Iterator AcctLdgAcctLdgIter = AcctLdgList.iterator();
                    while(AcctLdgAcctLdgIter.hasNext()){
                        AccountLedger accountLedger = (AccountLedger) AcctLdgAcctLdgIter.next();
                        // 科目类型
                        String LdgTp = accountLedger.getLdgTp();
                        //科目余额
                        String Bal = accountLedger.getBal();
                    }

                    //托管明细对账单
                    List<DetailedStatement> DtldStmt = bond2.getDtldStmt();
                    Iterator dtldStmtIter = DtldStmt.iterator();
                    while(dtldStmtIter.hasNext()){
                        DetailedStatement detailedStatement = (DetailedStatement) dtldStmtIter.next();
                        //记帐日期
                        String TxDt = detailedStatement.getTxDt();
                        //记帐时间
                        String TxTm = detailedStatement.getTxTm();
                        //结算合同标识
                        String CtrctId = detailedStatement.getCtrctId();
                        //记账摘要
                        String TxLog = detailedStatement.getTxLog();

                        //发生额
                        TransactionAmount TxAmt = detailedStatement.getTxAmt();
                        // 方向标识
                        String DrctnFlg = TxAmt.getDrctnFlg();
                        //值域
                        String Val = TxAmt.getVal();

                        //账户科目
                        List<AccountLedger> acctLdgList2 = detailedStatement.getAcctLdg();
                        Iterator AcctLdgAcctLdgIter2 = acctLdgList2.iterator();
                        while(AcctLdgAcctLdgIter2.hasNext()){
                            AccountLedger accountLedger = (AccountLedger) AcctLdgAcctLdgIter2.next();
                            // 科目类型
                            String LdgTp = accountLedger.getLdgTp();
                            //科目余额
                            String Bal = accountLedger.getBal();
                        }

                    }
                }
            }

        }


    }
}
