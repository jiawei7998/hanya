package ceshi;

import com.singlee.capital.common.util.ParameterUtil;
import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.CdtcDictConstants;
import com.singlee.financial.cdcc.CdtcRetStatus;
import com.singlee.financial.cdcc.convert.EsbBodyPacket;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00500101.Body;
import com.singlee.financial.cdcc.model.CSBS00500101.Msg;
import com.singlee.financial.cdcc.model.CSBS00500101.OprtrFlg;
import com.singlee.financial.cdcc.model.CSBS00600101.BtchQryRslt;
import com.singlee.financial.cdcc.model.CSBS00600101.QryRsltAbst;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.Condition;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 *   结算业务查询请求（CSBS.005.001.01 SettlementBusinessQueryRequest V01）结算成员-->CSBS
 *   “结算业务查询请求”报文由结算成员发送给中央债券综合业务系统，用于：
 *   结算成员进行结算业务信息详情查询及批量查询。
 */
public class TestInstrBatch {

    public static void main(String[] args) {
        instrBatchQuery();
    }

    /**
     * 指令批量查询
     */
    public static void instrBatchQuery(){
        try {
            //1。组装中债请求报文
            EsbPacket<Body> instrBatchPacket = packAge();

            //2.向中债发送请求报文,并获取返回实体
            String backXml = CdtcCommonUtils.send(instrBatchPacket);

            //3.解析返回报文并判断
            parsingXml(backXml);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private static <T extends EsbBodyPacket> EsbPacket<T> packAge() throws IllegalAccessException, InstantiationException {
        EsbPacket<Body> instrBatchPacket = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Body body = instrBatchPacket.getBody();

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-12-07T14:31:49.396");
        body.setMsg(msg);

        //操作标识
        OprtrFlg oprtrFlg = new OprtrFlg();
        //操作码
        oprtrFlg.setOprtrSts(CdtcDictConstants.OperatorStatusCode.OS03);
        oprtrFlg.setReqTp(CdtcDictConstants.RequestTypeCode.BJ0300);
        body.setOprtrFlg(oprtrFlg);

        //查询账户
        AccountIdentificationAndName4 acctQry = new AccountIdentificationAndName4();
        acctQry.setId("A0118000001");
        body.setAcctQry(acctQry);

        //查询条件
        Condition qryCond = new Condition();
        qryCond.setQryDt("2021-11-23");
        body.setQryCond(qryCond);

        return (EsbPacket<T>) instrBatchPacket;
    }

    private static void parsingXml(String backXml) throws IOException, DocumentException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        Log log = LogFactory.getLog(TestInstrBatch.class);

        if(backXml == null){
            log.info("指令批量查询报文发送后没有返回要解析的报文文件，请查看接口机是否正常工作");
            log.info("SendStatus:FAIL");
            log.info("SendMessage:没有返回要解析的报文文件，请查看接口机是否正常工作");
            return;
        }

        //获取报文返回类型
        String msgTp = CdtcCommonUtils.getMsgTp(backXml);
        log.info("MsgTp:" + msgTp);

        if(msgTp==null||"CSBS.009.001.01".equals(msgTp)){
            log.info("接收CSBS.009.001.01 失败报文");
            //解析失败报文
            Map<String,Object> resultMap = CdtcCommonUtils.cdtcFailPacket(backXml);
            String errorMessage = ParameterUtil.getString(resultMap,"errorMessage","");
            String detaiErrorlMessage = ParameterUtil.getString(resultMap,"detaiErrorlMessage","");
            log.info("指令批量查询发送出现异常,异常描述为："+ errorMessage);
            log.info("SendStatus:FAIL");
            log.info("SendMessage:"+errorMessage);
            log.info("详细异常描述为："+detaiErrorlMessage);
        }else {
            //解析成功返回报文
            EsbPacket<com.singlee.financial.cdcc.model.CSBS00600101.Body> reponsePacket = EsbPacket.initEsbPacket(
                    com.singlee.financial.cdcc.model.CSBS00600101.Body.class,MsgHeader.class, "Root");
            reponsePacket = CdtcCommonUtils.transformRep(backXml,reponsePacket);

            com.singlee.financial.cdcc.model.CSBS00600101.Body body = reponsePacket.getBody();
            System.out.println(body.toString());
            QryRsltAbst qryRsltAbst = body.getQryRsltAbst();
            //指令处理结果返回码
            String txRsltCd = qryRsltAbst!=null?qryRsltAbst.getTxRsltCd():null;
            if(StringUtils.isNotEmpty(txRsltCd)){
                //判断是否处理成功
                if(CdtcCommonUtils.msgReturn(txRsltCd)){
                    List<BtchQryRslt> btchQryRsltList = body.getBtchQryRslt();
                    for(int i = 0; i<btchQryRsltList.size(); i++){
                        BtchQryRslt btchQryRslt = btchQryRsltList.get(i);
                        //结算指令标识
                        btchQryRslt.getInstrId();

                        //付券方账户
                        AccountIdentificationAndName4 giveAcct = btchQryRslt.getGivAcct();
                        giveAcct.getId();
                        giveAcct.getNm();
                        //收券方账户
                        AccountIdentificationAndName4 takAcct = btchQryRslt.getTakAcct();
                        takAcct.getId();
                        takAcct.getNm();

                        //业务标识号
                        String txId = btchQryRslt.getTxId();
                        System.out.println(txId);
                        //业务类型
                        String bizTp = btchQryRslt.getBizTp();
                        //结算方式1
                        String SttlmTp1 = btchQryRslt.getSttlmTp1();
                        //结算方式2
                        String SttlmTp2 = btchQryRslt.getSttlmTp2();
                        //债券数目
                        String BdCnt = btchQryRslt.getBdCnt();
                        //债券总额
                        String AggtFaceAmt = btchQryRslt.getAggtFaceAmt();
                        //结算金额1
                        String Val1 = btchQryRslt.getVal1();

                        //结算金额2
                        String Val2 = btchQryRslt.getVal2();
                        //交割日1
                        String Dt1 = btchQryRslt.getDt1();
                        //交割日2
                        String Dt2 = btchQryRslt.getDt2();
                        //指令处理状态
                        String InstrSts = btchQryRslt.getInstrSts();
                        //发令方确认标识
                        String OrgtrCnfrmInd = btchQryRslt.getOrgtrCnfrmInd();
                        //对手方确认标识
                        String CtrCnfrmInd = btchQryRslt.getCtrCnfrmInd();
                        //指令来源
                        String InstrOrgn = btchQryRslt.getInstrOrgn();
                        //最新更新时间
                        String LastUpdTm =  btchQryRslt.getLastUpdTm();

                    }
                }else{
                    log.info("SendStatus:FAIL");
                    //错误信息
                    String desc = CdtcRetStatus.getCdtcRetStatus(txRsltCd).getDesc();
                    log.info("SendMessage:"+desc);
                    log.info("指令批量查询失败：");
                }

            }else {
                System.out.println("SendStatus:FAIL");
                log.info("指令批量查询返回报文不完整，缺少TxRsltCd标签");
            }

        }
    }

}
