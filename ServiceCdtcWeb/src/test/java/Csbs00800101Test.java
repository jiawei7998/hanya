import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.convert.EsbSubPacketConvert;
import com.singlee.financial.cdcc.model.CSBS00800101.Body;
import com.singlee.financial.cdcc.model.CSBS00800101.Msg;
import com.singlee.financial.cdcc.model.CSBS00800101.QryPty;
import com.singlee.financial.cdcc.model.CSBS00800101.StmtRpt;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName5;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.component.GenericIdentification4;
import com.singlee.financial.cdcc.model.component.PartyIdentification;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdcc.model.header.Vrsn;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Liang
 * @date 2021/11/27 16:34
 * =======================
 */
public class Csbs00800101Test {
    public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs00800101 = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Body body = csbs00800101.getBody();

        /**************************报文头*****************************/
        MsgHeader msgHeader = csbs00800101.getSysHeader(MsgHeader.class);

        Vrsn vrsn = new Vrsn();
        vrsn.setVrsnID("01");
        msgHeader.setVrsn(vrsn);

        Extension xtnsn = new Extension();
        List<Extension> lst = new ArrayList<Extension>();
        xtnsn.setXtnsnTxt("1");
        lst.add(xtnsn);
        xtnsn.setXtnsnTxt("12");
        lst.add(xtnsn);
        xtnsn.setXtnsnTxt("123");
        lst.add(xtnsn);

        /**************************报文体*****************************/
        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        //经办人
        PartyIdentification partyIdentification = new PartyIdentification();
        partyIdentification.setNm("kaka");
        msg.setOprtr(partyIdentification);
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);
        //对账报告
        StmtRpt stmtRpt=new StmtRpt();
        stmtRpt.setStartDt("22512");
        stmtRpt.setEndDt("55522");
        stmtRpt.setSfkpgAcctCnt("55ss522");
        List<AccountIdentificationAndName5> andName5s=new ArrayList<>();
        AccountIdentificationAndName5 SfkpgAcctStmtInf=new AccountIdentificationAndName5();
        SfkpgAcctStmtInf.setNm("kaka");
        SfkpgAcctStmtInf.setId("123456");
        SfkpgAcctStmtInf.setBdCnt("kaka");
        andName5s.add(SfkpgAcctStmtInf);
        stmtRpt.setSfkpgAcctStmtInf(andName5s);
        body.setStmtRpt(stmtRpt);

        //3.0查询方
        QryPty qryPty = new QryPty();
        qryPty.setNm("xxkaka");
        //自定义标识
        GenericIdentification4 genericIdentification4=new GenericIdentification4();
        genericIdentification4.setId("1127");
        genericIdentification4.setIdTp("1127");
        qryPty.setPrtryId(genericIdentification4);
        //   代理机构
//        AgentParty agentParty=new AgentParty();
//        agentParty.setAgtPtyTP("20211127");
//        //代理机构标识
//        PartyIdentification30 partyIdentification30 = new PartyIdentification30();
//        partyIdentification30.setNm("ggkka");
//        //自定义标识
//        GenericIdentification4 genericIdentification41 = new GenericIdentification4();
//        genericIdentification41.setId("2022020");
//        genericIdentification41.setIdTp("2022020111127");
//        partyIdentification30.setPrtryId(genericIdentification41);
//        agentParty.setAgtPtyId(partyIdentification30);
//        qryPty.setAgtPty(agentParty);
        body.setQryPty(qryPty);
        //4.0扩展项
        ArrayList<Extension> extensions = new ArrayList<>();
        Extension extension = new Extension();
        extension.setXtnsnTxt("321654");
        Extension extension1 = new Extension();
        extension1.setXtnsnTxt("321654");
        extensions.add(extension1);
        extensions.add(extension);
        body.setXtnsn(extensions);

        String xml = XmlUtils.toXml(csbs00800101);
        System.out.println(xml);
        //解析报文
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml, csbs00800101);
        //存在多个头，需要指定
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        Body body1 = bodyEsbPacket.getBody();
        System.out.println(msgHeader1.getVrsn().getVrsnID());


        EsbSubPacketConvert s= body1.getBodyConvert();
        System.out.println(s);

    }
}
