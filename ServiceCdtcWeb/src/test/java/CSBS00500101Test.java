import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00500101.Body;
import com.singlee.financial.cdcc.model.CSBS00500101.Msg;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdcc.model.header.Vrsn;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;


public class CSBS00500101Test {
    public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs00500101 = EsbPacket.initEsbPacket(Body.class, MsgHeader.class,"Root");
        Body body = csbs00500101.getBody();

        MsgHeader msgHeader = csbs00500101.getSysHeader(MsgHeader.class);

        Vrsn vrsn = new Vrsn();
        vrsn.setVrsnID("01");
        msgHeader.setVrsn(vrsn);

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);

        AccountIdentificationAndName4 AcctQry=new AccountIdentificationAndName4();
        AcctQry.setId("11");
        AcctQry.setNm("测试");
        body.setAcctQry(AcctQry);


         List<Extension>  list2=new ArrayList<>();
        Extension extension =new Extension();
        extension.setXtnsnTxt("123");

        Extension extension2 =new Extension();
        extension2.setXtnsnTxt("456");
        list2.add(extension2);
        list2.add(extension);
        body.setXtnsn(list2);



        String xml = XmlUtils.toXml(csbs00500101);
        System.out.println(xml);
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml,csbs00500101);
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        System.out.println(msgHeader1.getVrsn().getVrsnID());
        System.out.println(bodyEsbPacket.getBody().getMsg().getTxFlowId());
    }
}
