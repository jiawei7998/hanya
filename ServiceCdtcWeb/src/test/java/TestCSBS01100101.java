import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS01100101.Body;
import com.singlee.financial.cdcc.model.CSBS01100101.Msg;
import com.singlee.financial.cdcc.model.CSBS01100101.QryPty;
import com.singlee.financial.cdcc.model.component.*;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName Test.java
 * @Description 测试
 * @createTime 2021年11月23日 19:49:00
 */
public class TestCSBS01100101 {
    public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");

        Body body = csbs.getBody();

        MsgHeader msgHeader = csbs.getSysHeader(MsgHeader.class);

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);
        body.setQryFunc("111");

        BatchCondition qryCond=new BatchCondition();
        BatchCondition1 bthCond1=new BatchCondition1();
        bthCond1.setId("1");
        bthCond1.setEndDt("2");
        bthCond1.setStrtDt("3");
        BatchCondition2 bthCond2=new BatchCondition2();
        bthCond2.setEndDt("2212");
        bthCond2.setStrtDt("1212");
        BatchCondition3 bthCond3=new BatchCondition3();
        bthCond3.setStrtDt("89");
        bthCond3.setEndDt("98");
        bthCond3.setYrsToMtrty("46374");
        BatchCondition4 bthCond4=new BatchCondition4();
        bthCond4.setEndDt("wqw");
        bthCond4.setStrtDt("qwqwa");
        bthCond4.setEndYrsToMtrty("12eqw");
        bthCond4.setQryCls("iuoqw");
        bthCond4.setStrtYrsToMtrty("eew");
        BatchCondition5 bthCond5=new BatchCondition5();
        bthCond5.setId("jlska");
        bthCond5.setDt("9ui");
        qryCond.setBthCond1(bthCond1);
        qryCond.setBthCond2(bthCond2);
        qryCond.setBthCond3(bthCond3);
        qryCond.setBthCond4(bthCond4);
        qryCond.setBthCond5(bthCond5);
        body.setQryCond(qryCond);

        QryPty qryPty=new QryPty();
        qryPty.setNm("1i");

        GenericIdentification4 genericIdentification4=new GenericIdentification4();
        genericIdentification4.setId("12");
        genericIdentification4.setIdTp("ueie");
        qryPty.setPrtryId(genericIdentification4);
        body.setQryPty(qryPty);

        PartyIdentification oprtr=new PartyIdentification();
        oprtr.setNm("sas");
        body.setOprtr(oprtr);

        List<Extension> extensionList=new ArrayList<>();
        Extension extension1=new Extension();
        extension1.setXtnsnTxt("11");
        Extension extension2=new Extension();
        extension2.setXtnsnTxt("22");
        extensionList.add(extension1);
        extensionList.add(extension2);
        body.setXtnsn(extensionList);

        String xml = XmlUtils.toXml(csbs);
        System.out.println(xml);
        //解析报文
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml, csbs);
        //存在多个头，需要指定
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        Body body1 = bodyEsbPacket.getBody();

        System.out.println(body1);


    }
}
