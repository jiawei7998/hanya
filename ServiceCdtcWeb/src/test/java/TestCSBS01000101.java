import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS01000101.BizInfNtce;
import com.singlee.financial.cdcc.model.CSBS01000101.Body;
import com.singlee.financial.cdcc.model.CSBS01000101.Msg;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Luozb
 * @version 1.0.0
 * @ClassName Test.java
 * @Description 测试
 * @createTime 2021年11月23日 19:49:00
 */
public class TestCSBS01000101 {
    public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");

        Body body = csbs.getBody();

        MsgHeader msgHeader = csbs.getSysHeader(MsgHeader.class);


        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);


        BizInfNtce bizInfNtce=new BizInfNtce();
        bizInfNtce.setInfId("123");
        bizInfNtce.setInfTp("456");
        bizInfNtce.setAbstract("789");
        bizInfNtce.setKey("101112");
        body.setBizInfNtce(bizInfNtce);


        List<Extension> extensionList=new ArrayList<>();
        Extension extension1=new Extension();
        extension1.setXtnsnTxt("11");
        Extension extension2=new Extension();
        extension2.setXtnsnTxt("22");
        extensionList.add(extension1);
        extensionList.add(extension2);
        body.setXtnsn(extensionList);


        String xml = XmlUtils.toXml(csbs);
        System.out.println(xml);
        //解析报文
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml, csbs);
        //存在多个头，需要指定
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        Body body1 = bodyEsbPacket.getBody();

        System.out.println(body1.getMsg().toString());
        System.out.println(body1.getBizInfNtce().toString());
        System.out.println(body1.getXtnsn().toString());


    }
}
