import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00400101.Body;
import com.singlee.financial.cdcc.model.CSBS00400101.CmonSbsdryItm;
import com.singlee.financial.cdcc.model.CSBS00400101.Msg;
import com.singlee.financial.cdcc.model.CSBS00400101.StsFlg;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.ContractStatusInfo;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.component.InstructionStatusInfo;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdcc.model.header.Vrsn;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;


public class CSBS004Test {
    public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs00400101 = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Body body = csbs00400101.getBody();

        MsgHeader msgHeader = csbs00400101.getSysHeader(MsgHeader.class);

        Vrsn vrsn = new Vrsn();
        vrsn.setVrsnID("01");
        msgHeader.setVrsn(vrsn);

        List<Extension> xtnsns = new ArrayList<Extension>();
        Extension e1 = new Extension();
        e1.setXtnsnTxt("11111");
        Extension e2 = new Extension();
        e2.setXtnsnTxt("22222");
        Extension e3 = new Extension();
        e3.setXtnsnTxt("33333");
        xtnsns.add(e1);
        xtnsns.add(e2);
        xtnsns.add(e3);

        msgHeader.setXtnsn(xtnsns);

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);

        InstructionStatusInfo isi = new InstructionStatusInfo();
        isi.setInstrSts("111");
        isi.setTxRsltCd("222");
        isi.setInsErrInf("333");
        isi.setOrgtrCnfrmInd("444");
        isi.setCtrCnfrmInd("555");

        List<ContractStatusInfo> list2 = new ArrayList<ContractStatusInfo>();
        ContractStatusInfo csis = new ContractStatusInfo();
        csis.setCtrctId("1111");
        csis.setCtrctSts("2222");
        csis.setCtrctBlckSts("3333");
        csis.setCtrctFaildRsn("4444");
        csis.setLastUpdTm("5555");
        csis.setCtrctQryRsltCd("6666");
        csis.setCtrctQryRsltInf("7777");
        list2.add(csis);

        ContractStatusInfo csis2 = new ContractStatusInfo();
        csis2.setCtrctId("11112");
        csis2.setCtrctSts("22222");
        csis2.setCtrctBlckSts("33332");
        csis2.setCtrctFaildRsn("44442");
        csis2.setLastUpdTm("55552");
        csis2.setCtrctQryRsltCd("66662");
        csis2.setCtrctQryRsltInf("77772");
        list2.add(csis2);

        StsFlg sts = new StsFlg();
        sts.setInstrStsInf(isi);
        sts.setCtrctStsInf(list2);
        body.setStsFlg(sts);

        CmonSbsdryItm csi = new CmonSbsdryItm();
        csi.setInstrOrgn("1");
        csi.setSbryTp("2");
        csi.setInstrId("333");

        List<String> list = new ArrayList<>();
        list.add("1111");
        list.add("2222");
        csi.setCtrctId(list);

        AccountIdentificationAndName4 acc = new AccountIdentificationAndName4();
        acc.setId("1111");
        acc.setNm("2222");
        csi.setGivAcct(acc);
        csi.setTakAcct(acc);
        body.setCmonSbsdryItm(csi);



        body.setXtnsn(xtnsns);

        String xml = XmlUtils.toXml(csbs00400101);
        System.out.println(xml);
        //解析报文
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml, csbs00400101);
        //存在多个头，需要指定
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        Body body1 = bodyEsbPacket.getBody();
        System.out.println(msgHeader1.getVrsn().getVrsnID());


        List<String> ctrctId = body1.getCmonSbsdryItm().getCtrctId();
        for (String s : ctrctId) {
            System.out.println("反解结果:"+s);
        }


    }
}
