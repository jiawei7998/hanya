import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00200101.Body;
import com.singlee.financial.cdcc.model.CSBS00200101.Msg;
import com.singlee.financial.cdcc.model.CSBS00200101.StsFlg;
import com.singlee.financial.cdcc.model.component.ContractStatusInfo;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.component.InstructionStatusInfo;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdcc.model.header.Vrsn;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;


public class CSBS00200101Test {
    public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs00200101 = EsbPacket.initEsbPacket(Body.class, MsgHeader.class,"Root");
        Body body = csbs00200101.getBody();

        MsgHeader msgHeader = csbs00200101.getSysHeader(MsgHeader.class);

        Vrsn vrsn = new Vrsn();
        vrsn.setVrsnID("01");
        msgHeader.setVrsn(vrsn);

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);

        StsFlg stsFlg=new StsFlg();
        InstructionStatusInfo instructionStatusInfo=new InstructionStatusInfo();
        instructionStatusInfo.setInstrSts("处理成功");
        instructionStatusInfo.setTxRsltCd("666");
        instructionStatusInfo.setInsErrInf("嘿嘿嘿");
        List<ContractStatusInfo> list =new ArrayList<>();
        ContractStatusInfo contractStatusInfo =new ContractStatusInfo();
        contractStatusInfo.setCtrctBlckSts("牛皮");
        list.add(contractStatusInfo);
        stsFlg.setInstrStsInf(instructionStatusInfo);
        stsFlg.setCtrctStsInf(list);
        body.setStsFlg(stsFlg);

         List<Extension>  list2=new ArrayList<>();
        Extension extension =new Extension();
        extension.setXtnsnTxt("123");

        Extension extension2 =new Extension();
        extension2.setXtnsnTxt("456");
        list2.add(extension2);
        list2.add(extension);
        body.setXtnsn(list2);



        String xml = XmlUtils.toXml(csbs00200101);
        System.out.println(xml);
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml,csbs00200101);
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        System.out.println(msgHeader1.getVrsn().getVrsnID());
    }
}
