import com.singlee.financial.cdcc.CdtcCommonUtils;
import com.singlee.financial.cdcc.convert.EsbPacket;
import com.singlee.financial.cdcc.model.CSBS00300101.Body;
import com.singlee.financial.cdcc.model.CSBS00300101.CmonSbsdryItm;
import com.singlee.financial.cdcc.model.CSBS00300101.Msg;
import com.singlee.financial.cdcc.model.component.AccountIdentificationAndName4;
import com.singlee.financial.cdcc.model.component.Extension;
import com.singlee.financial.cdcc.model.header.MsgHeader;
import com.singlee.financial.cdcc.model.header.Vrsn;
import com.singlee.xstream.utils.XmlUtils;

import java.util.ArrayList;
import java.util.List;

public class CSBS003Test {
    public static void main(String[] args) throws Exception {

        EsbPacket<Body> csbs00300101 = EsbPacket.initEsbPacket(Body.class, MsgHeader.class, "Root");
        Body body = csbs00300101.getBody();

        MsgHeader msgHeader = csbs00300101.getSysHeader(MsgHeader.class);

        Vrsn vrsn = new Vrsn();
        vrsn.setVrsnID("01");
        msgHeader.setVrsn(vrsn);

        List<Extension> xtnsns = new ArrayList<Extension>();
        Extension e1 = new Extension();
        e1.setXtnsnTxt("11111");
        Extension e2 = new Extension();
        e2.setXtnsnTxt("22222");
        Extension e3 = new Extension();
        e3.setXtnsnTxt("33333");
        xtnsns.add(e1);
        xtnsns.add(e2);
        xtnsns.add(e3);

        msgHeader.setXtnsn(xtnsns);

        Msg msg = new Msg();
        msg.setTxFlowId(CdtcCommonUtils.convertDateToYYYYMMDDHHMMSS());
        msg.setPrty("PT02");
        msg.setCreDtTm("2021-10-27T14:35:49.396");
        body.setMsg(msg);


        CmonSbsdryItm csi = new CmonSbsdryItm();
        csi.setInstrOrgn("1");
        csi.setSbryTp("2");
        csi.setInstrId("333");

        List<String> list = new ArrayList<>();
        list.add("1111");
        list.add("2222");
        csi.setCtrctId(list);

        AccountIdentificationAndName4 acc = new AccountIdentificationAndName4();
        acc.setId("1111");
        acc.setNm("2222");
        csi.setGivAcct(acc);
        csi.setTakAcct(acc);
        body.setCmonSbsdryItm(csi);


        body.setXtnsn(xtnsns);

        String xml = XmlUtils.toXml(csbs00300101);
        System.out.println(xml);
        //解析报文
        EsbPacket<Body> bodyEsbPacket = XmlUtils.fromXML(xml, csbs00300101);
        //存在多个头，需要指定
        MsgHeader msgHeader1 = bodyEsbPacket.getSysHeader(MsgHeader.class);
        Body body1 = bodyEsbPacket.getBody();
        System.out.println(msgHeader1.getVrsn().getVrsnID());



    }
}
