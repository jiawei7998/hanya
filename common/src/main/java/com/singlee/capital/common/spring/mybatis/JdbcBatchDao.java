package com.singlee.capital.common.spring.mybatis;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.SqlSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;


/**
 * 
 * 批量操作 batch 
 * @author x230i
 *
 */

@Repository("jdbcBatchDao")
public class JdbcBatchDao  {
	private SqlSessionTemplate sqlSessionTemplate ;
	
	@Autowired
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
		sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
	}

	/**
	 * sqlId
	 * @param sql
	 * @param objectList
	 * @param toSql
	 * @throws SQLException 
	 */
	public <T> int [] executeBatch(String sql, List<T> objectList, SqlMergeHander<T> merge) throws SQLException {
		// 获得 原生的jdbc 链接 
		Connection conn = SqlSessionUtils.getSqlSession(sqlSessionTemplate.getSqlSessionFactory()).getConnection();
		try{
			PreparedStatement pstmt = conn.prepareStatement(sql);
			for(T o: objectList){
				merge.handle(pstmt, o);
				pstmt.addBatch();
			}
			int[] r = pstmt.executeBatch();
			JY.info("batch--> "+ r.length + sql);
			conn.commit();
			return r;
		}catch(Exception e){
			JY.info("batch--> "+ sql +"\r\n"+ExceptionUtils.getRootCauseMessage(e) );
			conn.rollback();
			return null;
		}finally{
			if(conn!=null){
				try {conn.close();} catch (SQLException e2) {
					throw new RException(e2);					
				}
			}	
		}
	}
}