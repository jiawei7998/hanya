package com.singlee.capital.common.util;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * 双缓冲List，提供两端操作。
 * 生产端可以非阻塞无限制插入，消费端可以批量处理 handleList(List<T> t)
 * 适合与多个生产者线程，一个消费者线程的场景！
 * @author x230i
 *
 * @param <T>
 */
public abstract class DoubleBufferedList<T> {
	
	private final List<T> l1;
	private final List<T> l2;
	/** 当前使用哪个list，true为1，false为2*/
	private boolean current;
	/** 锁 */
	private final ReentrantLock lock;
	/** 是否需要消费处理的条件 */
	private final Condition needResume;
	/** 消费处理线程  */
	private final Thread resumeThread;
	/** 总计消费处理的记录数 */
	private long resumeTotal;
	/** 总计生产的记录数 */
	private long produceTotal;
	/** list 名称 */
	private final String name;
	
		public DoubleBufferedList(String n){
		name = n;
		l1 = new ArrayList<T>();
		l2 = new ArrayList<T>();
		lock = new ReentrantLock();
		needResume = lock.newCondition();
		resumeThread = new Thread(){
			@Override
			public void run(){
				while(true){
					
					long s = System.currentTimeMillis();
					/** 处理消费 */
					/** 获得backupList */
					int i = DoubleBufferedList.this.handleList(backupList());
					resumeTotal += i;
					/** 打印处理时间 */
					JY.info(String.format("本次处理%d条记录,消耗%d(ms).%s ",i, System.currentTimeMillis() - s, 
							DoubleBufferedList.this.toString()));
					/** 清除backupList */
					DoubleBufferedList.this.backupList().clear();
					/** 检测currenList是否也是空，如果空，就await, 否则就切换队列 （将主队列改成备用队列后，进行上述重复步骤处理） */
					DoubleBufferedList.this.checkCurrentList();
				}
			}
		};
		resumeThread.setName(name + "处理线程");
		resumeThread.start();
	}
	
	/**
	 * 生产
	 * 往双缓冲list增加内容
	 * 非阻塞
	 */
	public final void produce(T t){
		if (t == null) {
			throw new NullPointerException();
		}
		lock.lock();
		try {
			insert(t);
			needResume.signal();
		} finally {
			lock.unlock();
		}
	}
	/**
	 * 总共处理了多少记录
	 * @return
	 */
	public final long getResumeTotal(){
		return resumeTotal;
	}
	public final long getProduceTotal(){
		return produceTotal;
	}
	@Override
	public final String toString(){
		return String.format("%s[%d/%d]", name,resumeTotal,produceTotal);
	}
	
	private final void insert(T t) {
		currentList().add(t);
		produceTotal ++;
	}

	/**
	 * 处理 备用队列的 数据，这里可以慢慢做。做完后，清空并切换成主队列
	 * 只要实现这里即可。
	 * @param t
	 * @return
	 */
	protected abstract int handleList(List<T> t);
	
	
	private final List<T> currentList(){
		if(current){
			return l1;
		}else{
			return l2;
		}
	}
	private final List<T> backupList(){
		if(!current){
			return l1;
		}else{
			return l2;
		}
	}
	
	private final void checkCurrentList(){
		lock.lock();
		try {
			if(currentList().size()==0){
				JY.debug(String.format("%s,主队列没有内容，等待有数据后再进行队列切换并消费处理！",toString()));		
				needResume.await();
			}
			current = !current;
		} catch (InterruptedException e) {
			JY.info("被打断！",e);
		} finally {
			lock.unlock();
		}
	}
}
