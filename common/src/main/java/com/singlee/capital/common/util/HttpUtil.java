package com.singlee.capital.common.util;

import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;


/**
 * http工具集合
 * @author LyonChen
 *
 */
public class HttpUtil {

	/**
	 * 
	 * 回写response
	 * @param response
	 * @param json
	 * @throws IOException
	 */
	public static final void flushHttpResponse(HttpServletResponse response, String contentType, String content) 
			throws IOException {
		response.setContentType(contentType);
		PrintWriter out = response.getWriter();
		StringBuilder builder = new StringBuilder();
		builder.append(content);
		out.print(builder.toString());
		out.close();
	}
	/**
	 * 
	 * 回写response
	 * @param response
	 * @param json
	 * @throws IOException
	 */
	public static final void flushHttpResponse(HttpServletResponse response, String contentType, byte[] data) 
			throws IOException {
		response.setContentType(contentType);
		OutputStream os = response.getOutputStream();
		IOUtils.write(data, os);
		os.flush();
	}
	
	/**
	 * 
	 * 回写response
	 * @param response
	 * @param json
	 * @throws IOException
	 */
	public static final void flushHttpResponseWithJson(HttpServletResponse response, String json) 
			throws IOException {
		response.setContentType("application/json;charset=UTF-8");
		PrintWriter out = response.getWriter();
		StringBuilder builder = new StringBuilder();
		builder.append(json);
		out.print(builder.toString());
		out.close();
	}
	
	/**
	 * 
	 * @param map			参数列表
	 * @param paramName		参数名称
	 * @param defaultValue  取值错误时的默认返回值
	 * @return
	 */
	public static String getParameterString(HttpServletRequest req,String paramName,String defaultValue){
		return ParameterUtil.getString(req.getParameterMap(), paramName, defaultValue);
	}
	public static int getParameterInt(HttpServletRequest req,String paramName,int defaultValue){
		return ParameterUtil.getInt(req.getParameterMap(), paramName, defaultValue);
	}
	/**
     * 获取ip
     * @param request
     * @return
     */
    public static String getIp(HttpServletRequest request) {
         String ip = request.getHeader("X-Real-IP");
         if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
              ip = request.getHeader("Proxy-Client-IP");
         }else{
              String[] ipArr = ip.split(",");
              if(ipArr.length > 0){
                   ip = ipArr[0];
              }
         }
         if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
              ip = request.getHeader("WL-Proxy-Client-IP");
         }
         if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
              ip = request.getRemoteAddr();
         }
         return ip;
    }
}
