package com.singlee.capital.common.spring.webmvc;

import com.singlee.capital.common.pojo.JQueryFileUploadResult;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.JY;
import org.springframework.web.multipart.MultipartFile;


/**
 * 处理 附件文件上传 的接口 
 * 入库，并返回编号
 * 
 * @author Lyon Chen
 * 
 */
public abstract class JqueryFileUploadHandlerMulipartFile implements HandlerMulipartFile {

	
	/**
	 * 
	 * @param book
	 * @return
	 */
	protected abstract JQueryFileUploadResult attachment(MultipartFile multipartFile);
	
	protected abstract void checkFileName(String fileName);
	
	@Override
	public String process(MultipartFile multipartFile) {
		String fileName = multipartFile.getOriginalFilename();
		checkFileName(fileName);
		JQueryFileUploadResult j = null;
		try {
			j = attachment(multipartFile);
		} catch (Exception e) {
			JY.raise("process file %s exception %s", fileName, e.getMessage());
			j = new JQueryFileUploadResult();
			j.addError(e.getMessage());
		}
		return FastJsonUtil.toJSONString(j);
	}
}
