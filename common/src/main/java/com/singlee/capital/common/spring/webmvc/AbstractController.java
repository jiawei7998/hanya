package com.singlee.capital.common.spring.webmvc;

import com.singlee.capital.common.exception.CException;
import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.pojo.JQueryFileUploadResult;
import com.singlee.capital.common.pojo.Pair;
import com.singlee.capital.common.pojo.RetMsg;
import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.spring.json.FastJsonUtil;
import com.singlee.capital.common.util.JY;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 控制器的抽象类，通用部分放置此处
 * 
 * @author cz
 * 
 */
public abstract class AbstractController {

	/**
	 * 
	 * 统一处理 业务处理 运行期 异常，不需要日志记录
	 * 
	 * @return
	 */
	@ExceptionHandler({ RException.class })
	@ResponseBody
	public <T> RetMsg<T> exception(RException e) {
		JY.error("控制器发生运行期异常RException：", e);
		return handleRException(e);
	}

	/**
	 * 统一处理 不在程序控制范围的运行期异常 RuntimeException，需要日志记录
	 * 
	 * @return
	 */
	@ExceptionHandler({ RuntimeException.class })
	@ResponseBody
	public <T> RetMsg<T> exception(RuntimeException e) {
		JY.error("控制器发生运行期异常RuntimeException：", e);
		return handleRException(e);
	}

	@ExceptionHandler({ CException.class })
	@ResponseBody
	public <T> RetMsg<T> exception(CException e) {
		if (JY.isLogDebugEnabled()) {
			JY.error("控制器发生异常CException：", e);
		}
		return handleCException(e);
	}

	@ExceptionHandler({ IOException.class })
	@ResponseBody
	public <T> RetMsg<T> exception(IOException e) {
		JY.error("控制器发生IO异常IOException：", e);
		return handleException(e);
	}

	/**
	 * 对运行期异常的处理
	 * 
	 * @param e
	 * @return
	 */
	protected abstract <T> RetMsg<T> handleRException(RuntimeException e);

	/**
	 * 对checked异常的处理
	 * 
	 * @param e
	 * @return
	 */
	protected abstract <T> RetMsg<T> handleCException(CException e);

	/**
	 * 对checked异常的处理
	 * 
	 * @param e
	 * @return
	 */
	protected abstract <T> RetMsg<T> handleException(Exception e);

	
	
	/**
	 * 
	 * @param request
	 * @param response
	 */
	protected RetMsg<List<Pair<String,String>>> uploadFiles(MultipartHttpServletRequest request,
			HttpServletResponse response, HandlerMulipartFile handler) {
		List<Pair<String,String>> ret = new LinkedList<Pair<String,String>>();
		
		// 1.文件获取
		Map<String, MultipartFile> itr = request.getFileMap();
		if(itr==null||itr.values()==null||itr.values().size()<1){
			return RetMsgHelper.ok("no files uploaded", ret);
		}
		// 2.顺序解析
		for (MultipartFile uploadedFile : itr.values()) {
			ret.add(new Pair<String,String>(uploadedFile.getOriginalFilename(), 
					handler.process(uploadedFile)));
		}
		return RetMsgHelper.ok("ok",ret);
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 */
	protected JQueryFileUploadResult uploadFile(MultipartHttpServletRequest request,
			HttpServletResponse response, HandlerMulipartFile handler) {
		// 1.文件获取
		MultipartFile multipartFile = request.getFile((String)request.getFileNames().next());
		// 2.顺序解析
		return FastJsonUtil.parseObject(handler.process(multipartFile), JQueryFileUploadResult.class);
	}
}
