package com.singlee.capital.common.exception;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.Serializable;




/**
 * checked 异常 明确需要 catch/throws的应该使用这个。
 * @author
 */
public class CException extends Exception implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new runtime exception with null as its detail message.
	 */
	public CException() {
		super("UnknownException");
	}

	/**
	 * Constructs a new runtime exception with the specified detail message.
	 * 
	 * @param message
	 */
	public CException(final String message) {
		super(message);
	}


	/**
	 * Constructs a new runtime exception with the specified detail message and
	 * cause.
	 * 
	 * @param message
	 * @param cause
	 */
	public CException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructs a new runtime exception with the specified cause and a detail
	 * message of (cause==null ? null : cause.toString()) (which typically
	 * contains the class and detail message of cause).
	 * 
	 * @param cause
	 */
	public CException(final Throwable cause) {
		super(cause);
	}


	@Override
	public String toString() {
		return ExceptionUtils.getRootCauseMessage(this);
	}

}
