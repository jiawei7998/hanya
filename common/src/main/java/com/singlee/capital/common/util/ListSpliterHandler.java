package com.singlee.capital.common.util;

import java.util.List;


/**
 * 列表切分 处理接口
 * @author LyonChen
 *
 * @param <T>
 */
public interface ListSpliterHandler<T> {
	
	
	public void handle(List<T> t);
	
	
}
