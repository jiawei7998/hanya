package com.singlee.capital.common.spring;

import com.singlee.capital.common.util.JY;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.io.*;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * 释放资源文件入口 每个模块/项目需要有资源文件 jsp/jpg/css等文件需要释放到最终的目标工程页面路径时 请遵守
 * standard的目录命名规范！！！ 支持 1：jar包方式 2：工程引用方式
 * 
 * @author LyonChen
 * 
 */
public abstract class ReleaseResourceInitListener implements
		ApplicationListener<ContextRefreshedEvent> {

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			try {
				// file:/E:/singlee-devp/capitalweb-test/lib/common.jar!/com/singlee/capital/common/spring/
				// zip:/wls/wldomains/fibs11/servers/fibs/tmp/_WL_user/capitalweb/opomsu/war/WEB-INF/lib/baseweb.jar!/com/singlee/capital/trust/mapper/TrustInterfaceMapper.class
				// /wls/wldomains/fibs11/servers/fibs/tmp/_WL_user/capitalweb/opomsu/war/WEB-INF/lib/common.jar!/com/singlee/capital/common/
				String jarFileFullPath = jarFilePath();
				boolean isJar = true;
				;
				if (jarFileFullPath.indexOf(".jar!") <= 0) {
					// /E:/singlee-devp/capital/product/capitalweb/trunk/src/main/web/WEB-INF/classes/
					jarFileFullPath = event.getApplicationContext()
							.getClassLoader().getResource("/").getPath();
					isJar = false;
				}
				JY.info("----------------------------------ReleaseResourceInitListener-----------------------------------");
				JY.info(jarFileFullPath);
				if (isJar) {
					// file:/E:/singlee-devp/capitalweb-test/lib/common.jar!/com/singlee/capital/common/spring/
					// zip:/wls/wldomains/fibs11/servers/fibs/tmp/_WL_user/capitalweb/opomsu/war/WEB-INF/lib/baseweb.jar!/com/singlee/capital/trust/mapper/TrustInterfaceMapper.class
					String webRoot = jarFileFullPath.substring(
							jarFileFullPath.indexOf(":") + 1,
							jarFileFullPath.lastIndexOf("/WEB-INF/") + 1);

					String jarFile = jarFileFullPath.substring(
							jarFileFullPath.indexOf(":") + 1,
							jarFileFullPath.indexOf("!"));
					String jarFileName = jarFile.substring(jarFile
							.lastIndexOf("/") + 1);

					JY.info("[" + jarFileName + "]模块-" + jarVersion()
							+ ",释放文件>" + webRoot + "开始");
					JY.info("--------------------------------------------------------------------");
					// 抽取jar包文件中的resource到webroot
					upzipFile(jarFile, webRoot);
					JY.info("--------------------------------------------------------------------");
					JY.info("[" + jarFileName + "]模块-" + jarVersion()
							+ ",释放文件>" + webRoot + "结束");
				} else {
					// /E:/singlee-devp/capital/product/capitalweb/trunk/src/main/web/WEB-INF/classes/
					String webRoot = jarFileFullPath.substring(
							jarFileFullPath.indexOf(":") - 1,
							jarFileFullPath.lastIndexOf("/WEB-INF/") + 1);
					jarFileFullPath = jarFilePath();
					String binPath = jarFileFullPath.substring(
							jarFileFullPath.indexOf(":") - 1,
							jarFileFullPath.indexOf(this.getClassNamePath()));
					JY.info("[" + binPath + "]模块-" + jarVersion() + ",复制文件>"
							+ webRoot + "开始");
					JY.info("--------------------------------------------------------------------");
					// 抽取jar包文件中的resource到webroot
					copyFile(binPath, webRoot);
					JY.info("--------------------------------------------------------------------");
					JY.info("[" + binPath + "]模块-" + jarVersion() + ",复制文件>"
							+ webRoot + "结束");
				}
			} catch (Exception e) {
				JY.info(e.getMessage());
				JY.info(jarFilePath());
			}
			JY.info("--------------------------------------------------------------------");
		}
	}

	/**
	 * 
	 */
	protected abstract String jarFilePath();

	protected abstract String jarVersion();

	private String getClassNamePath() {
		return StringUtils.replaceChars(this.getClass().getPackage().getName(),
				'.', '/');
	}

	/**
	 * 所有模块中的standard目录开始的文件处理
	 */
	private final String prifix = "standard";

	private void copyFile(String binPath, String webRoot) {
		try {
			binPath = binPath.replaceAll("%20", " ");
			File file = new File(binPath);
			File[] files = file.listFiles();
			for (File f : files) {
				String en = f.getPath();
				if (en.endsWith(prifix)) {
					String outFileName = webRoot + File.separator + prifix;
					File fo = new File(outFileName);
					makeSupDir(outFileName);
					JY.debug(f.getPath() + "==>" + fo.getPath());
					if (f.isDirectory()) {
						FileUtils.copyDirectory(f, fo, null, true);
					} else {
						FileUtils.copyFile(f, fo);
					}
				}
			}
		} catch (IOException e) {
			JY.raise(binPath + " copyFile 错误！\r\n" + e.getMessage());
		}
	}

	private void upzipFile(String fileName, String webRoot) {
		try {
			JarFile jarFile = new JarFile(fileName);
			Enumeration<JarEntry> jarEntrys = jarFile.entries();
			while (jarEntrys.hasMoreElements()) {
				JarEntry jarEntry = jarEntrys.nextElement();
				String en = jarEntry.getName();
				if (en.startsWith(prifix)) {
					JY.debug("====>" + en);
					String outFileName = webRoot + en;
					File f = new File(outFileName);
					makeSupDir(outFileName);
					if (jarEntry.isDirectory()) {
						continue;
					}
					writeFile(jarFile.getInputStream(jarEntry), f);
				}
			}
		} catch (IOException e) {
			JY.raise(fileName + " upzip 错误！\r\n" + e.getMessage());
		}
	}

	private final Pattern p = Pattern.compile("[/\\" + File.separator + "]");

	private void makeSupDir(String outFileName) {
		Matcher m = p.matcher(outFileName);
		while (m.find()) {
			int index = m.start();
			String subDir = outFileName.substring(0, index);
			File subDirFile = new File(subDir);
			if (!subDirFile.exists()) {
				subDirFile.mkdir();}
		}
	}

	private void writeFile(InputStream ips, File outputFile) throws IOException {
		OutputStream ops = new BufferedOutputStream(new FileOutputStream(
				outputFile));
		try {
			byte[] buffer = new byte[1024];
			int nBytes = 0;
			while ((nBytes = ips.read(buffer)) > 0) {
				ops.write(buffer, 0, nBytes);
			}
		} catch (IOException ioe) {
			throw ioe;
		} finally {
			try {
				if (null != ops) {
					ops.flush();
					ops.close();
				}
			} catch (IOException ioe) {
				throw ioe;
			} finally {
				if (null != ips) {
					ips.close();
				}
			}
		}
	}
}
