package com.singlee.capital.common.util;

import com.alibaba.fastjson.JSONArray;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;

public class ParameterUtil {
	
	/**
	 * 获取数组
	 * @param <T>
	 * @param map
	 * @param paramName
	 * @param defaultValue
	 * @return
	 */
	public static  Object[]  getList(Map<?,?> map,String paramName){
		Object obj = map.get(paramName);
		if(obj!=null){
			if(obj instanceof JSONArray){
				JSONArray jsonArray = (JSONArray)obj;
				return jsonArray.toArray();
			}
		}
		return null;
	}
	/**
	 * 
	 * @param map			参数列表
	 * @param paramName		参数名称
	 * @param defaultValue  取值错误时的默认返回值
	 * @return
	 */
	public static String getString(Map<?,?> map,String paramName,String defaultValue){
		Object obj = map.get(paramName);
		if(obj!=null){
			// 如果是数组，特殊处理
			if(obj instanceof String[]){
				String[] sa = (String[]) obj;
				return StringUtils.join(sa);
			}else{
				return map.get(paramName).toString();	
			}			
		}else{
			return defaultValue;
		}
	}
	/**
	 * 
	 * @param map			参数列表
	 * @param paramName		参数名称
	 * @param defaultValue  取值错误时的默认返回值
	 * @return
	 */
	public static int getInt(Map<?,?> map,String paramName,int defaultValue){
		try{
			return Integer.parseInt(map.get(paramName).toString());
		}catch(Exception ex){
			return defaultValue;
		}

	}
	
	/**
	 * 
	 * @param map			参数列表
	 * @param paramName		参数名称
	 * @param defaultValue  取值错误时的默认返回值
	 * @return
	 */
	public static double getDouble(Map<?,?> map,String paramName,double defaultValue){
		try{
			return Double.parseDouble(map.get(paramName).toString());
		}catch(Exception ex){
			return defaultValue;
		}
	}

	/**
	 * 
	 * @param map			参数列表
	 * @param paramName		参数名称
	 * @param defaultValue  取值错误时的默认返回值
	 * @return
	 */
	public static boolean getBoolean(Map<?,?> map,String paramName,boolean defaultValue){
		try{
			String r = map.get(paramName).toString();
			return Boolean.parseBoolean(r) || "Y".equalsIgnoreCase(r);
		}catch(Exception ex){
			return defaultValue;
		}
	}

	/**
	 * 
	 * @param map			参数列表
	 * @param paramName		参数名称
	 * @param defaultValue  取值错误时的默认返回值
	 * @return
	 */
	public static long getLong(Map<?,?> map,String paramName,long defaultValue){
		try{
			return Long.parseLong(map.get(paramName).toString());
		}catch(Exception ex){
			return defaultValue;
		}
	}
	
	// 获得 请求对象的 分页信息 
	public static final RowBounds getRowBounds(Map<String,?> map){
		// pageNumber":1,"pageSize"
		int pageNumber = ParameterUtil.getInt(map,"pageNumber",1);
		pageNumber = pageNumber==0?1:pageNumber;
		int pageSize = ParameterUtil.getInt(map,"pageSize",10);
		if(pageSize<0){
			return new RowBounds();
		}
		return new RowBounds((pageNumber-1)*pageSize,pageSize);
	}
}
