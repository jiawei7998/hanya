package com.singlee.capital.common.util;

import com.github.pagehelper.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.wltea.expression.ExpressionEvaluator;
import org.wltea.expression.datameta.Variable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 集合工具
 * 
 * @author LyonChen
 * 
 */
public class CollectionUtil {

	
	/**
	 * 将 list 中的 每个对象的prop属性 使用 join字符连接起来。
	 * @param list
	 * @param prop
	 * @param join
	 * @return
	 */
	public static final <T> String joinCollectionProperty(List<T> list, String prop, String join){
		List<Object> target = new ArrayList<Object>();
		for(T t : list){
			target.add(ReflectUtil.getVal(t, prop));
		}
		return StringUtils.join(target, join);
	}

	/**
	 * param 按照 key:val,格式平装string
	 * @param param
	 * @param join
	 * @return
	 */
	public static final String joinCollectionProperty(Map<String,String[]> param){
		if(param==null || param.size()==0){
			return "";
		}
		Iterator<String> iterator = param.keySet().iterator();
		StringBuffer sb = new StringBuffer();
		while(iterator.hasNext()){
			String key = iterator.next();
			String[] vals = param.get(key);
			sb.append(key).append(":").append(StringUtils.join(vals)).append(",");
		}
		return sb.toString();
	}
	
	/**
	 * 在List中查找符合条件的元素集合
	 * 
	 * @param list
	 *            目标集合
	 * @param exp
	 *            过滤表达式，例：(i_code == \"0001\" && type == \"1\") || a_type ==
	 *            \"CASH\"
	 * @return
	 */
	public static final <T> List<T> findAll(List<T> list, String exp) {
		if (StringUtil.isEmpty(exp)) {
			return list;
		}
		Class<?> clazz = null;
		Field[] fields = null;
		List<T> ret = new ArrayList<T>();
		for (T t : list) {
			if (clazz == null || fields == null) {
				clazz = t.getClass();
				fields = clazz.getDeclaredFields();
			}
			List<Variable> variables = new ArrayList<Variable>();
			if (t instanceof Map) {
				for (Object key : ((Map<?, ?>) t).keySet()) {
					if (key != null) {
						variables.add(Variable.createVariable(key.toString(),
								((Map<?, ?>) t).get(key)));
					}
				}
			} else {
				for (Field field : fields) {
					field.setAccessible(true);
					try {
						variables.add(Variable.createVariable(field.getName(),
								field.get(t)));
					} catch (IllegalArgumentException e) {
					} catch (IllegalAccessException e) {
					}
				}
			}
			Object result = ExpressionEvaluator.evaluate(exp, variables);
			if (result != null && result instanceof Boolean
					&& ((Boolean) result).booleanValue()) {
				ret.add(t);
			}
		}
		return ret;
	}

	/**
	 * 在List中查找符合条件的第一个元素
	 * 
	 * @param list
	 *            目标集合
	 * @param exp
	 *            过滤表达式，例：(i_code == \"0001\" && type == \"1\") || a_type ==
	 *            \"CASH\"
	 * @return
	 */
	public static final <T> T find(List<T> list, String exp) {
		List<T> tmpList = findAll(list, exp);
		if (tmpList == null || tmpList.size() == 0) {
			return null;
		}
		return tmpList.get(0);
	}

	public static final void testMap(Map<String, Object> map) {
		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			Object o = map.get(key);
			if (o instanceof List) {
				JY.debug(String.format("key=%s,length=%s", key,
						((List<?>) o).size()));
			} else {
				JY.debug(String.format("key=%s,value=%s", key, o.toString()));
			}
		}
	}

	public static final <T> List<T> initListWithElement(T t) {
		List<T> ret = new ArrayList<T>();
		ret.add(t);
		return ret;
	}

	/**
	 * List 切分工具
	 */
	public static final <T> void listSpliterHandler(List<T> list,
			ListSpliterHandler<T>  handler) {
		int maxCountPerRequest = 500; // 默认500条
		listSpliterHandler(list,handler,maxCountPerRequest);
	}
	public static final <T> void listSpliterHandler(List<T> list,
			ListSpliterHandler<T>  handler, int maxCountPerRequest) {
		JY.require(list != null && handler != null, "列表切分工具请确保输入参数不为空！");
		int total = list.size();
		int tmp = total / maxCountPerRequest; // 除数
		int mod = total % maxCountPerRequest; // 余数
		int reqCount = mod == 0 ? tmp : tmp + 1; // 分页次数
		for (int i = 0; i < reqCount; i++) {
			int fromIndex = i * maxCountPerRequest;
			int toIndex = (i + 1) * maxCountPerRequest;
			if (i + 1 == reqCount && mod != 0) {
				toIndex = total;
			}
			List<T> t = list.subList(fromIndex, toIndex);
			handler.handle(t);
			JY.debug(String.format("split handle %d--%d,%d", fromIndex, toIndex,
					t.size()));
		}
	}
	
	public static final <T> boolean contains(T s, T... ts){
		if(s == null || ts.length ==0 || ts == null) {return false;}
		for(T t : ts){
			if(s.equals(t)){
				return true;
			}
		}
		return false;
		
	}
}
