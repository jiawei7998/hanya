package com.singlee.capital.common.spring.json;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.NameFilter;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.singlee.capital.common.pojo.MyMap;
import com.singlee.capital.common.util.JY;

import java.io.UnsupportedEncodingException;
import java.util.List;


/**
 * fastjson的工具，根据 springmvc中的messageConvert的特性进行自动binding
 * 
 * @author Lyon Chen
 *
 */
public class FastJsonUtil {

	public static final SerializerFeature [] serializerFeatures = new SerializerFeature[]{
		SerializerFeature.WriteMapNullValue,
		//SerializerFeature.IgnoreNonFieldGetter,
		SerializerFeature.QuoteFieldNames};
	
	public static final SerializerFeature [] serializerFeatures4NoRef = new SerializerFeature[]{
		SerializerFeature.WriteMapNullValue,
		//SerializerFeature.IgnoreNonFieldGetter,
		SerializerFeature.DisableCircularReferenceDetect,
		SerializerFeature.QuoteFieldNames};

	public static final Feature [] features = new Feature[]{};
	
	private static final PropertyFilter propertyFilter = new MyPropertyFilter(){
		@Override
		public boolean apply(Object object, String name, Object value) {
			if("passwd".equalsIgnoreCase(name)
				|| "userImage".equalsIgnoreCase(name)){
				//false表示passwd、userImage字段将被排除在外
				return false;
			}
			return true;
		}
	};

	private static final NameFilter nameFilter = new NameFilter() {
		@Override
        public String process(Object object, String name, Object value) {
        	if(object instanceof MyMap){
       			return name.toLowerCase();
        	}else{
            	return name;
            }
        }
    };
    
    public static final SerializeFilter[] filters = new SerializeFilter[]{propertyFilter,nameFilter};
	/**
	 * 将对象写成 json字符串
	 * @param o
	 * @return
	 */
	public static String toJSONString(Object o) {
		try {
			return JSON.toJSONString(o, filters, serializerFeatures4NoRef);
		} catch (Exception e) {
			return JSON.toJSONString(o, filters, serializerFeatures);
		}
	}


	public static <T> List<T> parseArrays(String s, Class<T> clazz){
		return JSON.parseArray(s, clazz);
	}
	public static <T> T parseObject(String s, TypeReference<T> type){
		return JSON.parseObject(s, type);
	}
	public static <T> T parseObject(String text, Class<T> clazz){
		return JSON.parseObject(text, clazz);
	}
	
	public static <T>  T parseObject(byte[] bs, Class<T> clazz){
		try {
			return parseObject(new String(bs, "utf-8"), clazz);
		} catch (UnsupportedEncodingException e) {
			JY.info("parseObject(byte[], Class) --> %s", e.getMessage());
			return null;
		}
	}
	public static <T> byte[] serializeObject(T obj) {
		try {
			return toJSONString(obj).getBytes("utf-8");
		} catch (Exception e) {
			JY.info("serializeObject(T) --> %s", e.getMessage());
			return null;
		}
	}
}
