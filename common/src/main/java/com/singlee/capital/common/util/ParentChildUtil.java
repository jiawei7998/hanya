package com.singlee.capital.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/**
 * 父子类工具
 * 
 * @author LyonChen
 * 
 */
public class ParentChildUtil {

	/**
	 * 将子类所有的属性COPY到父类中。类定义中child一定要extends father；
	 * 而且child和father一定为严格javabean写法，属性为deleteDate，方法为getDeleteDate
	 * 父类没有的属性，就不做
	 */
	public static void childToFather(Object child, Object father)  {
		if (!(child.getClass().getSuperclass() == father.getClass())) {
			if(!(child.getClass().getSuperclass().getSuperclass() == father.getClass())) {
				JY.raiseRException("%s不是%s的子类", child.getClass().getName(), father.getClass().getName());
			}
		}
		Class<?> fatherClass = father.getClass();
		Field[] ff = fatherClass.getDeclaredFields();
		for (int i = 0; i < ff.length; i++) {
			Field f = ff[i];// 取出每一个属性，如deleteDate
			f.setAccessible(true);
			try {
				f.set(father, f.get(child));
			} catch (Exception e) {
			}
		}
	}

	/**
	 * 将父类所有的属性COPY到子类中。 类定义中child一定要extends father；
	 * 而且child和father一定为严格javabean写法，属性为deleteDate，方法为getDeleteDate
	 */
	public static void fatherToChild(Object father, Object child) throws Exception {
		if (!(child.getClass().getSuperclass() == father.getClass())) {
			JY.raiseRException("%s不是%s的子类", child.getClass().getName(), father.getClass().getName());
		}
		Class<?> fatherClass = father.getClass();
		Field[] ff = fatherClass.getDeclaredFields();
		for (int i = 0; i < ff.length; i++) {
			Field f = ff[i];// 取出每一个属性，如deleteDate
			if (!Modifier.isFinal(f.getModifiers())) {
				f.setAccessible(true);
				f.set(child, f.get(father));
			}
		}
	}

	public static <P, C extends P> void fatherToChildWithoutException(P father, C child) {
		Class<?> fatherClass = father.getClass();
		Field[] ff = fatherClass.getDeclaredFields();
		for (int i = 0; i < ff.length; i++) {
			Field f = ff[i];// 取出每一个属性，如deleteDate
			if (!Modifier.isFinal(f.getModifiers())) {
				f.setAccessible(true);
				try {
					f.set(child, f.get(father));
				} catch (Exception e) {
				}
			}
		}
	}

	/**
	 * 将hashmap转换为class对象
	 * @param map
	 * @param obj
	 */
	public static void HashMapToClass(Map<String, Object> map, Object obj) {
		Class<?> objClass = obj.getClass();
		Field[] ff = objClass.getDeclaredFields();
		for (int i = 0; i < ff.length; i++) {
			Field f = ff[i];// 取出每一个属性，如deleteDate
			if (!Modifier.isFinal(f.getModifiers())) {
				f.setAccessible(true);
				
				try {
					if (map.get(f.getName()) != null) {						
						// 此处仅当字段类型为int时报错，判断f的字段类型，当为int时强制转化
						if ("int".equals(f.getType().getName())) {
							f.set(obj, Integer.parseInt(map.get(f.getName()).toString()));
						} else if ("double".equals(f.getType().getName())) {
							f.set(obj, Double.parseDouble(map.get(f.getName()).toString()));
						}  else {
							f.set(obj, map.get(f.getName()));
						}
					}
				} catch (Exception e) {
					JY.warn(e);
				}
			}
		}
	}

	/**
	 * 将hashmap转换为class对象
	 * @param map
	 * @param obj
	 */
	public static HashMap<String, Object> ClassToHashMap(Object obj) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		@SuppressWarnings("unused")
		Class<?> objClass = obj.getClass();
		// 1.读取class的当前属性
		Field[] ff = obj.getClass().getDeclaredFields();
		for (int i = 0; i < ff.length; i++) {
			Field f = ff[i];// 取出每一个属性，如deleteDate
			if (!Modifier.isFinal(f.getModifiers())) {
				f.setAccessible(true);
				try {
					map.put(f.getName(), f.get(obj));
				} catch (IllegalArgumentException e) {
					JY.warn(e);
				} catch (IllegalAccessException e) {
					JY.warn(e);
				}
			}
		}
		// 2.循环判断父类属性
		getFieldFromObject(map, obj);
		return map;
	}

	public static void getFieldFromObject(HashMap<String, Object> map, Object obj) {
		Field[] ff = obj.getClass().getSuperclass().getDeclaredFields();
		if (ff.length == 0) {
			return;
		}
		for (int i = 0; i < ff.length; i++) {
			Field f = ff[i];// 取出每一个属性，如deleteDate
			if (!Modifier.isFinal(f.getModifiers())) {
				f.setAccessible(true);
				try {
					map.put(f.getName(), f.get(obj));
				} catch (IllegalArgumentException e) {
					JY.warn(e);
				} catch (IllegalAccessException e) {
					JY.warn(e);
				}
			}
		}
		getFieldFromObject(map, obj.getClass().getSuperclass());
	}

}
