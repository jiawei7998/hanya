/*
 *    Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import java.util.Map;

/**
 * Inteface allowing access to information about individual cron classes.
 * 
 * @author Jarrett Taylor
 */
public interface CronClass {
	/**
	 * returns the className
	 * 
	 * @return className
	 */
	public String getClassName();

	/**
	 * true if the next class is allowed to execute in the event this one fails
	 * 
	 * @return continuable
	 */
	public boolean getContinuable();

	/**
	 * the key/value pairs specified with this cron entry
	 * 
	 * @return properties
	 */
	public Map<String,Object> getParameters();

} // interface
