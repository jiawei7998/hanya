package com.singlee.capital.common.pojo;

import java.io.Serializable;

/**
 * 对对子
 * 
 * @author LyonChen
 */
public class FivePair<C1 extends Serializable, 
									C2 extends Serializable, 
									C3 extends Serializable, 
									C4 extends Serializable, 
									C5 extends Serializable> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private C1 c1;
	private C2 c2;
	private C3 c3;
	private C4 c4;
	private C5 c5;

	public FivePair() {
	}

	public void setC1(C1 c1) {
		this.c1 = c1;
	}

	public void setC2(C2 c2) {
		this.c2 = c2;
	}

	public FivePair(C1 c1, C2 c2, C3 c3, C4 c4, C5 c5) {
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
		this.c4 = c4;
		this.c5 = c5;
	}

	public C1 getC1() {
		return c1;
	}

	public C2 getC2() {
		return c2;
	}

	public C3 getC3() {
		return c3;
	}

	public void setC3(C3 c3) {
		this.c3 = c3;
	}

	public C4 getC4() {
		return c4;
	}

	public void setC4(C4 c4) {
		this.c4 = c4;
	}

	public C5 getC5() {
		return c5;
	}

	public void setC5(C5 c5) {
		this.c5 = c5;
	}

	@Override
	public String toString() {
		return String.format("FivePair [c1=%s, c2=%s, c3=%s, c4=%s, c5=%s]", c1,
				c2, c3, c4, c5);
	}


}
