package com.singlee.capital.common.util;

import com.singlee.capital.common.exception.RException;
import org.apache.commons.math3.util.Precision;
import org.wltea.expression.ExpressionEvaluator;
import org.wltea.expression.datameta.Variable;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/** 
 * 
 * @author LyonChen
 *
 */
public class MathUtil {
	/**
	 * 根据表达式进行比较运算,表达式的各个值来自于map中 表达式示例："a>=b";
	 * 
	 * @param map
	 * @param expression
	 * @return 
	 */
	public static boolean judgeExpression(Map<String, Object> map, String expression) {
		try {
			if (map == null || map.size() == 0) {
				return (Boolean) ExpressionEvaluator.evaluate(expression);
			}
			List<Variable> variables = new ArrayList<Variable>();
			Iterator<Entry<String, Object>> iter = map.entrySet().iterator();
			while (iter.hasNext()) {
				Entry<String, Object> entry = iter.next();
				// map中可能有多余的非数字参数字段，此时不需要抛出异常，程序继续执行，如果最终的expression中有非数字，计算函数会抛出异常
				try {
					Double value = Double.parseDouble(entry.getValue().toString());
					variables.add(Variable.createVariable(entry.getKey(), value));
				} catch (Exception e) {
					throw new RException(e);
				}
			}
			return (Boolean) ExpressionEvaluator.evaluate(expression, variables);
		} catch (Exception e) {
			throw new RException("表达式[" + expression + "]判断错误  " + map);
		}
	}

	/**
	 * 根据表达式进行四则运算,表达式的各个值来自于map中
	 * 
	 * @param map
	 * @param expression
	 * @return
	 */
	public static double calcExpression(Map<String, Object> map, String expression) {
		try {
			if (map == null || map.size() == 0) {
				return Double.parseDouble(ExpressionEvaluator.evaluate(expression).toString());
			}
			Object ret = evaluate(expression, map);
			if (ret instanceof Integer) {
			     return ((Integer) ret).doubleValue();
			   }else{
				   return (Double) ret;
			   }
			
		} catch (Exception e) {
			throw new RException("表达式[" + expression + "]计算错误  " + map);
		}
	}

	/**
	 * 计算表达式的值，使用参数 支持返回字符，boolean，double
	 * 
	 * @param express
	 * @param param
	 * @return
	 */
	static public final Object evaluate(String express, Map<String, Object> param) {
		JY.require(express != null, "表达式为null");
		List<Variable> variables = new ArrayList<Variable>();
		Iterator<Entry<String, Object>> iter = param.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, Object> entry = iter.next();
			try {
				Double value = Double.parseDouble(entry.getValue().toString());
				variables.add(Variable.createVariable(entry.getKey(), value));
			} catch (Exception e) {
				throw new RException(e);
			}
		}
		// 将表达式中的--替换成+，如果+号出现在第一个，则去除这个+号
		express = express.replaceAll("--", "+");
		if (express.startsWith("+")) {
			express = express.substring(1);
		}
		// 执行表达式
		return ExpressionEvaluator.evaluate(express, variables);
	}
	
	/** 
	 * 四舍五入
	 * @param num
	 * @param precision
	 * @return
	 */
	public static double roundWithNaN(double num, int precision){
		if(Double.isNaN(num)){
			return num;
		}else{
			return round(num,precision,BigDecimal.ROUND_HALF_UP);
		}
	}
	
	/** 
	 * 四舍五入
	 * @param num
	 * @param precision
	 * @return
	 */
	public static double round(double num, int precision){
		return round(num,precision,BigDecimal.ROUND_HALF_UP);
	}
	
	/** 
	 * @param num		 数字
	 * @param precision	  精度
	 * @param roundType  BigDecimal.ROUND_DOWN
	 * @return
	 */
	public static double round(double num, int precision, int roundType){
		BigDecimal   b   =   new   BigDecimal(Double.toString(num));
		double ret =  b.setScale(precision, roundType).doubleValue();
		return ret;
	}
	
	/**
	 * 比较数字是否为0 ，精度：10
	 * @param val1
	 * @return
	 */
	public static boolean isZero(double val1) {
		return isEqual(val1,0,8,BigDecimal.ROUND_HALF_UP);
	}
	/**
	 * 比较两个double是否相等，默认精度：8位
	 * @param roundType
	 * @param val1
	 * @param val2
	 * @return
	 */
	public static boolean isEqual(int roundType, double val1, double val2){
		return isEqual(val1,val2,8, roundType);
	}
	/**
	 * 指定精度比较两个double是否相等
	 * @param val1
	 * @param val2
	 * @param precision  精度
	 * @param roundType 舍入方式 
	 * @return
	 */
	public static boolean isEqual(double val1, double val2,int precision, int roundType){
		double t1 = round(val1, precision, roundType);
		double t2 = round(val2, precision, roundType);
		return Precision.equals(t1, t2);
	}

	/**
	 * 比较两个double数值 
	 * @param d1
	 * @param d2
	 * @param precision 小数点位数 
	 * @return
	 */
	public static int compare(double d1,double d2,int precision){
		//缺省调用 四舍五入 方式
		return compare(d1,d2,precision,BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * double数字比较    d1>d2:1   d1=d2:0   d1<d2:-1
	 * @param d1
	 * @param d2
	 * @param precision  精度，小数点几位
	 * @param roundType 舍入方式 
	 * @return
	 */
	public static int compare(double d1,double d2,int precision, int roundType){
		double t1 = round(d1, precision,roundType);
		double t2 = round(d2, precision,roundType);
		return Precision.compareTo(t1, t2, 1);
	}

	/**
	 * 有效的double类型数据
	 * @param d
	 * @return
	 */
	public static boolean validDouble(Double d){
		return d!=null && !d.isNaN() && !d.isInfinite();
	}

	public static final char convertChar(int k){
		return (char)(k+65);
//		
//		for(int i = 0;i<255;i++){
//            char a = (char) i;
//            System.out.println(a+"........."+i);
//        }
	}
	
	/**
	 * 保留precision位小数四舍五入后乘n后转字符串
	 * @param d
	 * @param precision
	 * @param roundType
	 * @param n
	 * @return
	 */
	public static String doubleToStr(Double d, int precision, 
			int n) {
		d = round(d, precision) * n;
		return String.format("%1$.0f", d);

	}
	
	/**
	 * 保留precision位小数四舍五入后乘n后转字符串
	 * @param d
	 * @param precision
	 * @param roundType
	 * @param n
	 * @return
	 */
	public static String doubleToStr(Double d, int precision) {
		d = round(d, precision) ;
		return String.format("%1$.02f", d);
	}
	/**
	 * 
	 * @param d
	 * @return
	 */
	public static String core(String d){
		char[] t = d.toCharArray();
		StringBuffer sb = new StringBuffer();
		sb.append(t, 0, t.length-2);
		sb.append('.');
		sb.append(t, t.length-2, 2);
		return sb.toString();
	}
	/**
	 * 字符串转Double后除n
	 * @param dStr
	 * @param n
	 * @return
	 */
	public static Double strToDouble(String dStr, int n) {
		try {
			BigDecimal bd = new BigDecimal(dStr);
			return bd.doubleValue() ;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 3123.234--> 000000000312323
	 * @param d
	 * @param len
	 * @return
	 */
	public static String doubleTo15Str(double d){
		d = round(d, 2);
		return String.format("%1$015.0f", d*100);
	}
	
	 /**
	  * 对比两个Double类型
	  * @param d
	  * @param d1
	  * @return
	  */
	 public static Boolean compare(Double d,Double d1){
       if(d==null){
    	   if(d1==null || d1==0){
    		   return true;
    	   }
    	   else{
    		   return false;
    	   }    	   
       }
       if(d1==null){
    	   if(d==null || d==0){
    		   return true;
    	   }
    	   else{
    		   return false;
    	   }
       }
       return 0==compare(d,d1,2);
       
	 }
	 
	 /**
	  * 判断double不等于null或者不等于0
	  * @param d
	  * @return
	  */
	 public static Boolean doubleIsNotEmpty(Double d){
		 if(d==null || d==0){
			 return false;
		 }
		 else{
			 return true;
		 }
	 }
	 
	 /**
	  * 判断double等于null或者等于0
	  * @param d
	  * @return
	  */
	 public static Boolean doubleIsEmpty(Double d){
		 if(d==null || d==0){
			 return true;
		 }
		 else{
			 return false;
		 }
	 }
	 
	 /**
	  * 数字转换字符串
	  * @param value 值
	  * @param precision 小数
	  * @return
	  */
	 public static String doubleToStrDF(double value,int precision){
		 StringBuilder sb = new StringBuilder("#,##0.");
		 for(int i=0;i<precision;i++){
			 sb.append("0");
		 }
		 DecimalFormat df = new DecimalFormat(sb.toString());
		 return df.format(value);
	 }
	 
	 /**
	  * double数字相减 v1-v2
	  * @param v1
	  * @param v2
	  * @return
	  */
	   public static Double sub(Double v1,Double v2){
	        BigDecimal b1 = new BigDecimal(v1.toString());
	        BigDecimal b2 = new BigDecimal(v2.toString());
	        return b1.subtract(b2).doubleValue();
	    }
	   
	    /**
	     * 两个Double数相加
	     * @param v1
	     * @param v2
	     * @return Double
	     */
	    public static Double add(Double v1,Double v2){
	        BigDecimal b1 = new BigDecimal(v1.toString());
	        BigDecimal b2 = new BigDecimal(v2.toString());
	        return b1.add(b2).doubleValue();
	    }
	    
	    /**
	     * 两个Double数相乘
	     * @param v1
	     * @param v2
	     * @return Double
	     */
	    public static Double mul(Double v1,Double v2){
	        BigDecimal b1 = new BigDecimal(v1.toString());
	        BigDecimal b2 = new BigDecimal(v2.toString());
	        return b1.multiply(b2).doubleValue();
	    }
	    
	    /**
	     * 两个Double数相除
	     * @param v1
	     * @param v2
	     * @return Double
	     */
	    public static Double div(Double v1,Double v2){
	        BigDecimal b1 = new BigDecimal(v1.toString());
	        BigDecimal b2 = new BigDecimal(v2.toString());
	        return b1.divide(b2,10,BigDecimal.ROUND_HALF_UP).doubleValue();
	    }
}
