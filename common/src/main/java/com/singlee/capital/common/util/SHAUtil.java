package com.singlee.capital.common.util;

import java.security.MessageDigest;

public class SHAUtil {

    // SHA512加密(128个字符)
    public final static String SHA512(String pwd) {
        String shaPwd = null;
        if (pwd != null && pwd.length() > 0) {
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
                messageDigest.update(pwd.getBytes());
                byte[] byteBuffer = messageDigest.digest();
                StringBuffer strHexString = new StringBuffer();
                for (int i = 0; i < byteBuffer.length; i++) {
                    String hex = Integer.toHexString(0xff & byteBuffer[i]);
                    if (hex.length() == 1) {
                        strHexString.append('0');
                    }
                    strHexString.append(hex);
                }
                shaPwd = strHexString.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return shaPwd;
    }

//    public static void main(String[] args) {
//        
//        String str = "123456";
//        String slta ="004969"+SHAUtil.SHA512(str);
//        String slta1 ="admin"+SHAUtil.SHA512(str).toUpperCase();//1179a1021428fcbf3be7148835b01084
//        System.out.println(MD5Util.MD5(slta));
//        System.out.println(MD5Util.MD5(slta1));
//    }
}
