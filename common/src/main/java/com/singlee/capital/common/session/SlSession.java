package com.singlee.capital.common.session;



import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;


/**
 * Singlee 会话对象
 * @author LyonChen
 *
 */
public class SlSession extends HashMap<String, Serializable> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private static final String localeKey = "com.singlee.capital.locale";
	
	/** 会话 的key */
	private String sessionKey;
	protected SlSession(){
	}
	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public boolean hasLocale(){
		return this.containsKey(localeKey);
	}
	public Locale getLocale(){
		return (Locale)this.get(localeKey);
	}
	public void setLocale(Locale l){
		this.put(localeKey, l);
	}
	public Locale getLocaleDefault(){
		if(!hasLocale()){
			return Locale.getDefault();
		}else{
			return getLocale();
		}
	}
	
}
