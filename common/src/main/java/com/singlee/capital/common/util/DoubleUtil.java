package com.singlee.capital.common.util;


import java.math.BigDecimal;

/**
 * 
 * @author LyonChen
 *
 */
public class DoubleUtil {
	/**
	 * 先匹配 第一个参数，如果为NaN，则取b
	 * @param a
	 * @param b
	 * @return
	 */
	public static final double firstMatch(double a, double b){
		return Double.isNaN(a) ? b: a;
	}

	/**
	 * 减
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static double sub(double d1, double d2) {
		BigDecimal b1 = new BigDecimal(Double.toString(d1));
		BigDecimal b2 = new BigDecimal(Double.toString(d2));

		return b1.subtract(b2).doubleValue();
	}

	/**
	 * 加
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static double add(double d1, double d2) {
		BigDecimal b1 = new BigDecimal(Double.toString(d1));
		BigDecimal b2 = new BigDecimal(Double.toString(d2));

		return b1.add(b2).doubleValue();
	}

}
