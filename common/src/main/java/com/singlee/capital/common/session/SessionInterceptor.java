package com.singlee.capital.common.session;

import com.singlee.capital.common.pojo.RetMsgHelper;
import com.singlee.capital.common.util.HttpUtil;
import com.singlee.capital.common.util.JY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * session拦截
 * 
 * @author cz 
 *  	
 */
public class SessionInterceptor implements HandlerInterceptor {

	@Autowired
	private SessionService sessionService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String uri = request.getRequestURI();
		String ctx = request.getContextPath();
		String ip = request.getRemoteAddr();
		String agent = request.getHeader("User-Agent");

		/** 禁止浏览器缓存 **/
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");

		JY.debug(String.format("SessionInterceptor--preHandle[%s]",uri));

		/**  1.对于不需要拦截的url地址放行 通过 **/
		if (sessionService.isNotNeedSessionInterceptor(uri)) {
			SessionStaticUtil.holdSlSessionThreadLocal(sessionService.anonymousSlSession(ip, uri, agent));
			return true;
		}

		/**  2.获得 slsession **/
		SlSession session = SessionStaticUtil.getSlSessionByHttp(request); 
		if (session == null){
			HttpUtil.flushHttpResponseWithJson(response,RetMsgHelper.simple("error.system.0001").toJsonString());
			return false;
		}else{
			// 设置 locale
			if(!session.hasLocale()){

			}
		}
		/** 3.检查 slsession 对象是否 符合后台逻辑  **/
		if(!sessionService.checkSlSession(session, ip, uri, agent)){
			HttpUtil.flushHttpResponseWithJson(response,RetMsgHelper.simple("error.common.0002").toJsonString());
			return false;
		}else{
			/** 4.根据请求地址，校验权限  **/
			String url = uri.substring(ctx.length(), uri.length());
			boolean ret = sessionService.checkSlSessionUrlPermission(session, url);
			if(!ret){
				HttpUtil.flushHttpResponseWithJson(response, RetMsgHelper.simple("error.common.0003").toJsonString());
				return false;
			}else{
				/** 5.注入threadLocal，允许后续的逻辑获得 slsession   **/
				SessionStaticUtil.holdSlSessionThreadLocal(session);
				return true;
			}
		}
	}

	/**
	 * 如果 modelAndView!=null
	 * 则根据preHandle的sessionKey注入view所需要显示控制
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView) throws Exception {
		String uri = request.getRequestURI();
		JY.debug(String.format("SessionInterceptor--postHandle[%s]",uri));
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		String uri = request.getRequestURI();
		JY.debug(String.format("SessionInterceptor--afterCompletion[%s]",uri));
		// * 如果在线程池模型下，譬如tomcat的线程池，如果不清空，在此线程重新执行后，
		// 有可能带来错误的session, 所以通过拦截器 的 afterCompletion 事件 将threadlocal清空
		SessionStaticUtil.clearSlSessionThreadLocal();
		if(ex!=null){
			// 记录 ex 异常 
			JY.warn(ex);
		}
	}
}
