package com.singlee.capital.common.session;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.HashMap;
import java.util.Map;


/**
 * 
 * 使用 容器 作为 session 存放处，超时需要有回调处理 ！！！！
 * @author cz
 *
 */
public class SessionListener implements HttpSessionListener {

	private SessionService sessionService;
	
	private void initializeService(HttpSession session){
		WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
		sessionService = context.getBean(SessionService.class);
		//JY.require(sessionService != null, "SessionService 没有spring托管的 实现的bean");
	}
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
	}
	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		HttpSession httpSession = arg0.getSession();
		SlSession session = (SlSession)httpSession.getAttribute(SessionService.SessionKey);
		// 没有了 那就什么都不做
		if(session == null){
			
		}
		if(sessionService == null){
			// 注入 sessionService
			initializeService(httpSession);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("sessionId", httpSession.getId());//sessionid
			map.put("isOnline", "0");//不在线
			sessionService.updateUserLoginInfo(map);
		}
		sessionService.logout(session);
	}


}
