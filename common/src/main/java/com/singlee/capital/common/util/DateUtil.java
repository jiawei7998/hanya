package com.singlee.capital.common.util;

import org.apache.commons.lang.time.FastDateFormat;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 日期类型与字符串类型相互转换工具类
 * 
 * @author cz
 */
public class DateUtil {

	private static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
	public static final FastDateFormat COMPACT_DATETIME_FORMAT = FastDateFormat.getInstance("yyyyMMddHHmmss");
	public static final FastDateFormat TIME_FORMAT_PATTERN = FastDateFormat.getInstance("HH:mm:ss");
	public static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd");
	public static final String DATETIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	private static String[] parsePatterns = { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy/MM/dd",
			"yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyyMMdd" };

	/**
	 * 取当前系统时间【HH:mm:ss】
	 */
	public static String getCurrentTimeAsString() {
		return TIME_FORMAT_PATTERN.format(new Date());
	}

	public static String getCurrentTimeAsString(String pattern) {
		FastDateFormat formatter = FastDateFormat.getInstance(pattern);
		return formatter.format(new Date());
	}

	/**
	 * 返回当前日期，按<code>yyyyMMddHHmmss</code>格式化日期成字符串
	 */
	public static String getCurrentCompactDateTimeAsString() {
		return COMPACT_DATETIME_FORMAT.format(new Date());
	}

	public static String getCurrentDateAsString(Date data, String pattern) {
		FastDateFormat formatter = FastDateFormat.getInstance(pattern);
		return formatter.format(data);
	}

	/**
	 * 返回当前日期，按<code>yyyy-MM-dd</code>格式化日期成字符串
	 */
	public static String getCurrentDateAsString() {
		return DATE_FORMAT.format(new Date());
	}

	/**
	 * 返回按照当前日期，按<code>pattern</code>格式化的字符串
	 */
	public static String getCurrentDateAsString(String pattern) {
		FastDateFormat formatter = FastDateFormat.getInstance(pattern);
		return formatter.format(new Date());
	}

	/**
	 * 返回当前日期，按 <code>yyyy-MM-dd HH:mm:ss</code>格式化成字符串
	 */
	public static String getCurrentDateTimeAsString() {
		return format(new Date(), DATETIME_FORMAT_PATTERN);
	}

	/**
	 * 返回当前日期，按 <code>yyyy-MM-dd HH:mm:ss</code>格式化成字符串
	 */
	public static String getCurrentDateTimeAsString(int field, int delta) {
		Calendar cal = Calendar.getInstance();
		cal.add(field, delta);
		Date date = cal.getTime();
		return format(date, DATETIME_FORMAT_PATTERN);
	}

	/**
	 * 日期调整 默认为天
	 * 
	 * @param date yyyy-MM-dd
	 * @param adj
	 * @return
	 */
	public static final String dateAdjust(String date, int adj) {
		return dateAdjust(date, adj, Calendar.DAY_OF_YEAR);
	}

	/**
	 * 日期调整
	 * 
	 * @param date     日期格式 yyyy-MM-dd
	 * @param adj      日期调整 正数 往后，负数往前
	 * @param timeUnit 为时间单位 天、月、年
	 * @return
	 * @throws ParseException
	 */
	public static final String dateAdjust(String date, int adj, int timeUnit) {
		Date d;
		d = DateUtil.parse(date, "yyyy-MM-dd");
		Date t = dateAdjust(d, adj, timeUnit);
		return DateFormatUtils.format(t, "yyyy-MM-dd");
	}

	public static final Date dateAdjust(Date date, int adj, int timeUnit) {
		switch (timeUnit) {
		case Calendar.DAY_OF_YEAR:
			return DateUtils.addDays(date, adj);
		case Calendar.MONTH:
			return DateUtils.addMonths(date, adj);
		case Calendar.YEAR:
			return DateUtils.addYears(date, adj);
		case Calendar.HOUR:
			return DateUtils.addHours(date, adj);
		case Calendar.MINUTE:
			return DateUtils.addMinutes(date, adj);
		default:
			JY.raiseRException("[%d] time unit not support !", timeUnit);
			return date;
		}
	}

	/**
	 * 返回当前日期所在月份的第一天
	 * 
	 * @return
	 */
	public static Date getStartDateTimeOfCurrentMonth() {
		return getStartDateTimeOfMonth(new Date());
	}

	/**
	 * 返回指定日期所在月份的第一天 The value of
	 * <ul>
	 * <li>Calendar.HOUR_OF_DAY
	 * <li>Calendar.MINUTE
	 * <li>Calendar.MINUTE
	 * </ul>
	 * will be set 0.
	 */
	public static Date getStartDateTimeOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * 返回当前日期所在月份的最后一天
	 * 
	 * @return
	 */
	public static Date getEndDateTimeOfCurrentMonth() {
		return getEndDateTimeOfMonth(new Date());
	}

	/**
	 * 返回指定日期所在月份的最后一天
	 * 
	 * @param date
	 * @return
	 */
	public static Date getEndDateTimeOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}

	/**
	 * 返回当天的凌晨时间
	 * 
	 * @return
	 */
	public static Date getStartTimeOfCurrentDate() {
		return getStartTimeOfDate(new Date());
	}

	/**
	 * 返回指定日期的凌晨时间
	 * 
	 * @param date
	 * @return
	 */
	public static Date getStartTimeOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * 返回当天的结束时间 <tt>2005-12-27 17:58:56</tt> will be returned as
	 * <tt>2005-12-27 23:59:59</tt>
	 */
	public static Date getEndTimeOfCurrentDate() {
		return getEndTimeOfDate(new Date());
	}

	/**
	 * 返回指定日期的结束时间
	 * 
	 * @param date
	 * @return
	 */
	public static Date getEndTimeOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}

	/**
	 * 获取指定日期的前某天或后某天
	 * @param curDate 指定日期
	 * @param days  天数
	 * @return
	 */
	public static String getPreDateOfDays(String curDate, int days) {
		Calendar c = Calendar.getInstance();
		Date date = parse(curDate);
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE,day+days);

		String result = format(c.getTime());
		return result;
	}

	/**
	 * 解析字符串成java.util.Date 使用yyyy-MM-dd
	 * 
	 * @param str
	 * @return
	 */
	public static Date parse(String str) {
		return parse(str, "yyyy-MM-dd");
	}

	/**
	 * 解析字符串成java.util.Date 使用pattern 【线程不安全】
	 * 
	 * @param str
	 * @param pattern
	 * @return
	 */
	public static Date parse(String str, String pattern) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		try {
			return DateUtils.parseDate(str, parsePatterns);
		} catch (ParseException e) {
			throw new IllegalArgumentException("Can't parse " + str + " using " + pattern);
		}
	}

	/**
	 * 根据时间变量返回时间字符串
	 */
	public static String format(Date date, String pattern) {
		if (date == null) {
			return null;
		}
		FastDateFormat df = FastDateFormat.getInstance(pattern);
		return df.format(date);
	}

	/**
	 * 按<code>yyyy-MM-dd</code>格式化日期成字符串
	 */
	public static String format(Date date) {
		return date == null ? null : DATE_FORMAT.format(date);
	}

	/**
	 * 计算两个日期之间的天数
	 */
	public static final int daysBetween(java.util.Date early, java.util.Date late) {
		Calendar ecal = Calendar.getInstance();
		Calendar lcal = Calendar.getInstance();
		ecal.setTime(early);
		lcal.setTime(late);

		long etime = ecal.getTimeInMillis();
		long ltime = lcal.getTimeInMillis();

		return (int) ((ltime - etime) / MILLIS_PER_DAY);
	}

	/**
	 * 计算两个日期之间的天数
	 */
	public static final int daysBetween(String early, String late) {
		java.util.Date dateEarly = DateUtil.parse(early, "yyyy-MM-dd");
		java.util.Date dateLate = DateUtil.parse(late, "yyyy-MM-dd");
		return daysBetween(dateEarly, dateLate);
	}

	/**
	 * yyyyMMdd yyyy-MM-dd互相转换
	 * 
	 * @param yyyy_mm_dd
	 * @return
	 */
	public static final String toDate8Char(String yyyy_mm_dd) {
		return StringUtil.replace(yyyy_mm_dd, "-", "");
	}

	public static final String toDate10Char(String yyyymmdd) {
		return toDate10Char(yyyymmdd, "-");
	}

	public static final String toDate10Char(String yyyymmdd, String splitChar) {
		if (yyyymmdd == null || yyyymmdd.length() < 8) {
			return yyyymmdd;
		} else {
			return yyyymmdd.substring(0, 4) + splitChar + yyyymmdd.substring(4, 6) + splitChar
					+ yyyymmdd.substring(6, 8);
		}
	}

	public static String nowDate() {
		return nowDate("-");
	}

	public static String nowDate(String sper) {
		GregorianCalendar Time = new GregorianCalendar();

		String s_nowD = "";
		String s_nowM = "";
		if (sper == null) {
			sper = "-";
		} else {
			sper = sper.trim();
		}

		int nowY = Time.get(1);
		int nowM = Time.get(2) + 1;
		s_nowM = StringUtil.leftPad(nowM, 2, '0');

		int nowD = Time.get(5);
		s_nowD = StringUtil.leftPad(nowD, 2, '0');

		String result = nowY + sper + s_nowM + sper + s_nowD;

		return result;
	}

	/**
	 * 计算两个日期之间小时差
	 * 
	 * @param early 之前时间 yyyy-MM-dd HH:mm:ss
	 * @param late  之后时间yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int hoursBetween(String early, String late) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date date1 = sdf.parse(early);
			Date date2 = sdf.parse(late);
			return (int) ((date2.getTime() - date1.getTime()) * 24 / MILLIS_PER_DAY);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return 0;
		}
	}

	/**
	 * 计算两个日期之差
	 * 
	 * @param early 之前时间 yyyy-MM-dd HH:mm:ss
	 * @param late  之后时间yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int timesBetween(String early, String late) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		try {
			Date date1 = sdf.parse(early);
			Date date2 = sdf.parse(late);
			return (int) ((date2.getTime() - date1.getTime()));
		} catch (ParseException e) {
			// e.printStackTrace();
			return 0;
		}
	}

	public static Date getYearLast(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, year);
		calendar.roll(Calendar.DAY_OF_YEAR, -1);
		Date currYearLast = calendar.getTime();
		return currYearLast;
	}

	/**
	 * 获取当年最后一天
	 * 
	 * @return
	 */
	public static Date getCurrYearLast() {
		Calendar currCalendar = Calendar.getInstance();
		int currYear = currCalendar.get(Calendar.YEAR);
		return getYearLast(currYear);
	}

	/*
	 * public static void main(String[] args) {
	 * System.out.println(""+format(getCurrYearLast())); }
	 */

	// 倒序
	public static String getDate() {
		Date now = Calendar.getInstance().getTime();
		return new SimpleDateFormat("ddMMyyyy").format(now);
	}

	/**
	 * 取到给定日期的下一天

	 */
	public static String getNextDayOfMonth(String date,String format) throws ParseException {
		Calendar cal = Calendar.getInstance();
		Date date1 = parse(date,format);
		cal.setTime(date1);
		cal.add(Calendar.DAY_OF_YEAR,1);
		String next = DateUtil.format(cal.getTime(),format);
		return next;
	}





}
