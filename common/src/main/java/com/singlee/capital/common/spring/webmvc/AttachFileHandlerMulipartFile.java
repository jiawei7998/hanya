package com.singlee.capital.common.spring.webmvc;

import com.singlee.capital.common.util.JY;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


/**
 * 处理 附件文件上传 的接口 
 * 入库，并返回编号
 * 
 * @author Lyon Chen
 * 
 */
public abstract class AttachFileHandlerMulipartFile implements HandlerMulipartFile {

	
	/**
	 * 
	 * @param book
	 * @return
	 */
	protected abstract String attachment(String fileName, String contentFile, byte[] bytes);
	
	protected abstract void checkFileName(String fileName);
	
	@Override
	public String process(MultipartFile uploadedFile) {
		String fileName = uploadedFile.getOriginalFilename();
		checkFileName(fileName);
		String contentFile = uploadedFile.getContentType();
		byte[] bytes;
		try {
			bytes = uploadedFile.getBytes();
			return attachment(fileName, contentFile, bytes);
		} catch (IOException e) {
			JY.raise("process file %s exception %s", fileName, e.getMessage());
			return e.getMessage();
		}
	}

}
