package com.singlee.capital.common.util;

import org.apache.commons.lang3.StringUtils;




/**
 * 
 * @author x230i
 *
 */
public class IdUtils {
	/**
	 * 通过审批单号、交易单号、委托单号获取到业务类型
	 * 单号的 11-15位
	 * @param id
	 * @return
	 */
	public static final String getTrdtypeById(String id){
		if(StringUtils.isEmpty(id) || id.length() != 21) {
			return null;}
		return id.substring(11, 15);
	}
	
}
