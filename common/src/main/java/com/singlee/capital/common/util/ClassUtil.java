package com.singlee.capital.common.util;

import com.singlee.capital.common.exception.RException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * 用于获取指定包名下的所有类名.<br/>
 * 并可设置是否遍历该包名下的子包的类名.<br/>
 * 并可通过Annotation(内注)来过滤，避免一些内部类的干扰.<br/>
 * 
 * @author Sodino E-mail:sodino@qq.com
 * @version Time：2014年2月10日 下午3:55:59
 */
public class ClassUtil {
	
	public static List<File> getFileList(String pkgName , boolean isRecursive ) {
		List<File> classList = new ArrayList<File>();
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			// 按文件的形式去查找
			String strFile = pkgName.replaceAll("\\.", "/");
			Enumeration<URL> urls = loader.getResources(strFile);
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
                if (url != null) {
                	String protocol = url.getProtocol();
                	String pkgPath = url.getPath();
                	JY.info("protocol:" + protocol +" path:" + pkgPath);
                    if ("file".equals(protocol)) {
                    	// 本地自己可见的代码
						findFileName(classList, pkgName, pkgPath, isRecursive);
					} else if ("jar".equals(protocol)) {
                    	// 引用第三方jar的代码
						findFileName(classList, pkgName, url, isRecursive);
                    }
                }
			}
		} catch (IOException e) {
			throw new RException(e);
		}
		
		return classList;
	}
	
	public static void findFileName(List<File> fileList, String pkgName, String pkgPath, boolean isRecursive) {
		if(fileList == null){
			return;
		}
		File[] files = filterFiles(pkgPath);// .js, .jsp, .jpg, .css 文件 或 类文件夹
		if(files != null){
			for (File f : files) {
				String fileName = f.getName();
				if (f.isFile()) {
					addFileName(fileList, f);
				} else {
					// 文件夹的情况
					if(isRecursive){
						// 需要继续查找该文件夹/包名下的类
						String subPkgName = pkgName +"."+ fileName;
						String subPkgPath = pkgPath +"/"+ fileName;
						findFileName(fileList, subPkgName, subPkgPath, isRecursive);
					}
				}
			}
		}
	}
	
	/**
	 * 第三方Jar类库的引用。<br/>
	 * @throws IOException 
	 * */
	public static void findFileName(List<File> fileList, String pkgName, URL url, boolean isRecursive) throws IOException {
		JarURLConnection jarURLConnection = (JarURLConnection) url.openConnection();
		JarFile jarFile = jarURLConnection.getJarFile();
		JY.info("jarFile: %s" , jarFile.getName());
		String outpathPrefix = jarFile.getName();
		outpathPrefix = outpathPrefix.substring(0, outpathPrefix.indexOf("src\\main\\web\\WEB-INF"));
		Enumeration<JarEntry> jarEntries = jarFile.entries();
		while (jarEntries.hasMoreElements()) {
			JarEntry jarEntry = jarEntries.nextElement();
			String jarEntryName = jarEntry.getName(); // 类似：sun/security/internal/interfaces/TlsMasterSecret.class
			InputStream is = jarFile.getInputStream(jarEntry);
			if ( check(jarEntryName)) {
				if(jarEntryName.equals(pkgName)){
					JY.info("jar entryName:" + jarEntryName);
					addFileName(fileList, outpathPrefix, jarEntryName);
				} else if(isRecursive && jarEntryName.startsWith(pkgName)){
					// 遍历子包名：子类
					JY.info("jar entryName:" + jarEntryName +" isRecursive:" + isRecursive);
					//addFileName(fileList, outpathPrefix, jarEntryName);
					processFileName(is, outpathPrefix, jarEntryName);
				}
			}
		}
	}
	
	private static void processFileName(InputStream is, String outpathPrefix,
			String jarEntryName) throws IOException {
		File f = new File(outpathPrefix+jarEntryName);
		byte[] data = IOUtils.toByteArray(is);
		JY.info("------add %s/%s",outpathPrefix, jarEntryName);
		FileUtils.writeByteArrayToFile(f, data);
	}

	private static File[] filterFiles(String pkgPath) {
		if(pkgPath == null){
			return null;
		}
		// 接收 .js, .jsp, .jpg, .css 文件 或 类文件夹
		return new File(pkgPath).listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return (file.isFile() && check(file.getName())) 
					 || file.isDirectory();
			}
		});
    }
	
	private static boolean check(String name){
		return name.endsWith(".js")
		|| name.endsWith(".jsp")
		|| name.endsWith(".jpg")
		|| name.endsWith(".css") ;
	}

	private static void addFileName(List<File> fileList,String prex,  String fname) {
		File f = new File(prex+fname);
		if (fileList != null &&  f!= null) {
			fileList.add(f);
		}
	}
	private static void addFileName(List<File> fileList, File f) {
		if (fileList != null &&  f!= null) {
			fileList.add(f);
		}
	}
}