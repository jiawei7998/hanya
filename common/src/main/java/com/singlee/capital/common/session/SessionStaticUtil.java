package com.singlee.capital.common.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;


/**
 * session的静态方法工具集合
 * 
 * 线程中自由获取定义session
 * @author x230i
 *
 */
public class SessionStaticUtil {

	/**
	 * ***********************************************************************
	 */

	/** 
	 * 存放自定义session，由httpsession转过来
	 */
	private static final ThreadLocal<SlSession> threadLocal = new ThreadLocal<SlSession>();
	
	public static final void holdSlSessionThreadLocal(SlSession session) {
		threadLocal.set(session);
	}

	public static final void clearSlSessionThreadLocal() {
		threadLocal.remove();
	}

	public static final SlSession getSlSessionThreadLocal() {
		return threadLocal.get();
	}
	
	/** 提供一个获得当前会话线程 的locale */
	public static final Locale getSlSessionLocale(){
		SlSession s = getSlSessionThreadLocal();
		if(s!=null){
			return s.getLocaleDefault();
		}else{
			return Locale.getDefault();
		}
	}
	
	

	/**
	 * ***********************************************************************
	 */
	
	/**
	 * 从容器中 的 httpsession中获得 的 slsession
	 * @param request
	 * @return
	 */
	public  static final SlSession getSlSessionByHttp(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return session!=null? (SlSession)session.getAttribute(SessionService.SessionKey) : null;
	}

	
	/**
	 * 从HttpServletRequest中获得session对象
	 * @param request
	 * @return
	 */
	public  static  void setSessionByHttp(HttpServletRequest request, SlSession session ){
		HttpSession httpSession = request.getSession();
		if(httpSession!=null){
			setSessionByHttpSession(httpSession,session);
		}
	}

	/**
	 * 设置Session对象进入http session
	 * @return
	 */
	public  static  void setSessionByHttpSession(HttpSession httpSession, SlSession session){
		httpSession.setAttribute(SessionService.SessionKey, session);
	}
	
	
	

	/**
	 * 创造 slsession 的入口 
	 * @return
	 */
	public static SlSession createSlSession(){
		return new SlSession();
	}

}
