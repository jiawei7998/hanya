/*
 *    Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import org.apache.commons.collections4.SetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Manages cron jobs. Format for schedule is as described in the man page from
 * crontab(5) (copied below).
 * 
 * <pre>
 * ------------------------------------------------------------------------------
 * The following was taken from the man page for crontab(5):
 * 
 *       The time and date fields are:
 * 
 *               field          allowed values
 *               -----          --------------
 *               minute         0-59
 *               hour           0-23
 *               day of month   0-31
 *               month          1-12
 *               day of week    0-6 (0 is Sunday)
 * 
 *        A field may be an asterisk (*), which always stands for "first-last".
 * 
 *        Ranges of numbers are allowed.  Ranges are two numbers separated with a
 *        hyphen.   The  specified  range is inclusive.  For example, 8-11 for an
 *        "hours" entry specifies execution at hours 8, 9, 10 and 11.
 * 
 *        Lists are allowed.  A list is a set of numbers (or ranges) separated by
 *        commas.  Examples: "1,2,5,9", "0-4,8-12".
 * 
 *        Step  values can be used in conjunction with ranges.  Following a range
 *        with "/&lt;number&gt;" specifies skips of the number's  value  through  the
 *        range.  For example, "0-23/2" can be used in the hours field to spec-
 *        ify command execution every other hour (the alternative in the V7 stan-
 *       dard  is "0,2,4,6,8,10,12,14,16,18,20,22").  Steps are also permitted
 *       after an asterisk, so if you want to say "every two hours", just  use
 *       "&#42;/2".
 * 
 *        Note:  The  day of a command's execution can be specified by two fields
 *        -- day of month, and day of week.  If both fields are  restricted  (ie,
 *        aren't  *),  the command will be run when either field matches the cur-
 *        rent time.  For example, "30 4 1,15 * 5" would cause a command to be 
 *        run at 4:30 am on the 1st and 15th of each month, plus every Friday.
 * 
 *        Instead of the first five fields, one of eight special strings may appear:
 * 
 *        string          meaning
 *        ------          -------
 *        &#64;reboot         Run once, at startup.
 *        &#64;yearly         Run once a year, "0 0 1 1 *".
 *        &#64;annually       (same as &#64;yearly)
 *        &#64;monthly        Run once a month, "0 0 1 * *".
 *        &#64;weekly         Run once a week, "0 0 * * 0".
 *        &#64;daily          Run once a day, "0 0 * * *".
 *        &#64;midnight       (same as &#64;daily)
 *        &#64;hourly         Run once an hour, "0 * * * *".
 * 
 * EXAMPLES
 *        run five minutes after midnight, every day
 *        5 0 * * *
 *        run at 2:15pm on the first of every month
 *        15 14 1 * *
 *        run at 10 pm on weekdays
 *        0 22 * * 1-5
 *        run 23 minutes after midn, 2am, 4am ..., everyday
 *        23 0-23/2 * * *
 *        run at 5 after 4 every sunday
 *        5 4 * * 0
 * 
 * 
 * AUTHOR (crontab documentation)
 *        Paul Vixie <paul@vix.com>
 * </pre>
 * 
 * @author Jarrett Taylor
 */
public class Cron extends Thread {
	/**
	 * logging facility for Cron
	 */
	//private Log log = LogFactory.getLog("CRON");
	private static final Logger log = LoggerFactory.getLogger("CRON");
	/**
	 * Logging facility for CronRunner
	 */
	//private Log innerLog = LogFactory.getLog("CronRunner");
	private static final Logger innerLog = LoggerFactory.getLogger("CronRunner");
	/**
	 * used as a synchronization lock
	 */
	private Integer lock = new Integer(3);
	/**
	 * used to keep track of drift compensation
	 */
	private long compensation;
	/**
	 * used to keep a reference to the cron thread
	 */
	private Thread thread;
	/**
	 * a set of all CronJob objects
	 */
	private Map<String, CronJob> cronJobs;
	/**
	 * a list of all CronRunner objects. Completed runners are removed when the
	 * thread wakes up on the next minute.
	 */
	private List<CronRunner> cronRunners;
	/**
	 * basically, it is true only when the thread is not sleeping (i.e.
	 * processing jobs)
	 */
	private boolean processing;
	/**
	 * true if terminate has been called. can be set to false by calling
	 * reset().
	 */
	private boolean killed;
	/**
	 * true if Cron has just loaded or if reload has been called with "startup
	 * mode" disabled.
	 */
	private boolean firstRun;
	/**
	 * true if Cron should not run. used for pausing cron globally.
	 */
	private boolean disabled;

	
	private static class SingletonHolder {
		private static final Cron INSTANCE = new Cron(new HashSet<CronJob>(), 4, true);
	}

	public static final Cron getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	/**
	 * creates a new Cron scheduler.
	 * 
	 * @param inCronJobs
	 *            a Set of CronJob objects
	 * @param inCompensation
	 *            drift compensation time in milliseconds
	 * @param fork
	 *            if true, frees the calling thread to return
	 */
	public Cron(Set<CronJob> inCronJobs, long inCompensation, boolean fork) {
		synchronized (lock) {
			compensation = inCompensation;
			cronJobs = toJobMap(inCronJobs);
			cronRunners = new LinkedList<CronRunner>();
			processing = false;
			killed = false;
			firstRun = false;
			disabled = false;
		} // synch
		if (fork) {
			thread = new Thread(this);
			thread.setDaemon(true);
			thread.setName("Cron-" + thread.getName());
			thread.start();
		} // if
		else {
			thread = Thread.currentThread();
			thread.setName("Cron-" + thread.getName());
			run();
		} // else
	} // constructor

	/**
	 * This loops while "killed" is false (i.e. terminate() has not been
	 * called). It wakes up right before the closest job is scheduled to run and
	 * kicks off a CronRunner thread and then goes back to sleep. The CronRunner
	 * thread decides what should run, so this thread never gets bogged down
	 * potentially missing the next time it should wake up.
	 */
	@Override
	public void run() {
		killed = false;
		Calendar cal = Calendar.getInstance();
		Date lastStartDate = cal.getTime();

		while (!killed) {
			synchronized (lock) {
				processing = true;
			} // synch

			Iterator<CronRunner> iter = cronRunners.iterator();
			while (iter.hasNext()) {
				CronRunner cronRunner = iter.next();
				if (!cronRunner.isAlive()) {
					iter.remove();}
			} // while

			if (!isDisabled()
					&& ((lastStartDate.getTime() / 60000) != (cal.getTime()
							.getTime() / 60000) || firstRun)) {
				CronRunner cronRunner = new CronRunner(cal.getTime(), firstRun);
				cronRunner.setDaemon(true);
				cronRunner.setName("CronRunner-" + cronRunner.getName());
				cronRunner.start();
				cronRunners.add(cronRunner);
				firstRun = false;
				lastStartDate = cal.getTime();
			} // if
			synchronized (lock) {
				processing = false;
			} // synch
			try {
				cal.add(Calendar.MINUTE, 1);
				cal.set(Calendar.SECOND, 0);
				Date nextRunDate = Utils.getClosestRun(cronJobs.values(), cal.getTime());
				if (nextRunDate == null) {
					// ((364 days in millis - millis left over in this minute) -
					// (drift compensation * number of days))
					Thread.sleep((31449600000L - (System.currentTimeMillis() % 60000))
							- (compensation * 364)); // sleep for 364 days
				} // if
				else {
					long temp = nextRunDate.getTime()
							- System.currentTimeMillis();
					// only sleep if it is more than 5 millis
					if (temp > 5) {
						// (millis until next run - (drift compensation * number
						// of days until run date))
						Thread.sleep(temp - (compensation * (temp / 86400000)));
					} // if
						// else continues because it is so close to the next run
						// time
				} // else
			} // try
			catch (InterruptedException e) {
				if (log.isInfoEnabled()) {
					log.info("run() - cron's sleep was interrupted");}
			} // catch
			cal.setTime(new Date());
		} // while
	} // run

	/**
	 * returns true if cron is kicking off CronRunner processes
	 * 
	 * @return true if cron is kicking off CronRunner processes
	 */
	public boolean isProcessing() {
		boolean returnVal;
		synchronized (lock) {
			returnVal = processing;
		} // synch
		return returnVal;
	} // isRunning

	/**
	 * returns true if cron is not allowed to kick off CronRunner processes
	 * 
	 * @return true if cron is not allowed to kick off CronRunner processes
	 */
	public boolean isDisabled() {
		boolean returnVal;
		synchronized (lock) {
			returnVal = disabled;
		} // synch
		return returnVal;
	} // isDisabled

	/**
	 * enables cron
	 */
	public void enable() {
		synchronized (lock) {
			disabled = false;
			thread.interrupt();
		} // synch
	} // enable

	/**
	 * disables cron (pause)
	 */
	public void disable() {
		synchronized (lock) {
			disabled = true;
			thread.interrupt();
		} // synch
	} // disable

	/**
	 * calls terminate on all CronRunners and CronJobs.
	 * 
	 * @return true if the process was running and has been stopped
	 */
	public boolean terminate() {
		boolean returnVal = false;
		synchronized (lock) {
			Iterator<CronRunner> iter = cronRunners.iterator();
			while (iter.hasNext()) {
				CronRunner cronRunner = (CronRunner) iter.next();
				if (cronRunner.isAlive()) {
					cronRunner.terminate();
				} // if
				iter.remove();
			} // while

			Iterator<CronJob> iter2 = cronJobs.values().iterator();
			while (iter2.hasNext()) {
				CronJob cronJob = (CronJob) iter2.next();
				cronJob.terminate();
			} // for

			if (!killed) {
				if (thread != null) {
					if (log.isInfoEnabled()) {
						log.info("terminate() - Attempting to interrupt: "
								+ thread.getName());}
					thread.interrupt();
				} // if
				killed = true;

				returnVal = true;
			} // if
		} // synch
		return returnVal;
	} // terminate

	/**
	 * sets the killed flag to false and resets all CronJob objects.
	 */
	public void reset() {
		// Probably serves no purpose since the original thread will have
		// exited.
		// not sure what the implications of starting a new thread here would
		// be.
		// would have to prevent reset from being called while a valid thread
		// was
		// still alive
		synchronized (lock) {
			Iterator<CronJob> iter = cronJobs.values().iterator();
			while (iter.hasNext()) {
				((CronJob) iter.next()).reset();}
			killed = false;
			thread.interrupt();
		} // synch
	} // reset

	/**
	 * Reloads the cron settings.
	 * 
	 * @param collection
	 *            a Set of CronJob objects
	 * @param inCompensation
	 *            drift compensation time in milliseconds
	 * @param terminateRunningJobs
	 *            if true, it will attempt to terminate any curretnly running
	 *            jobs
	 */
	public void reload(Collection<CronJob> collection, long inCompensation,
			boolean terminateRunningJobs) {
		if (log.isInfoEnabled()) {
			log.info("reload() - reloading jobs");}
		synchronized (lock) {
			Iterator<CronRunner> iter = cronRunners.iterator();
			while (iter.hasNext()) {
				CronRunner cronRunner = (CronRunner) iter.next();
				if (cronRunner.isAlive()) {
					cronRunner.terminate();
				} // if
				iter.remove();
			} // while

			// ensure other jobs are not running before reloading
			Iterator<CronJob> iter2 = cronJobs.values().iterator();
			while (iter2.hasNext()) {
				CronJob cronJob = (CronJob) iter2.next();
				cronJob.disable();
				if (terminateRunningJobs) {
					cronJob.terminate();}
			} // for
				// instead of replacing the list, should it loop and update in
				// the event
				// a job is still running? then remove the jobs that shouldn't
				// be there?
			compensation = inCompensation;
			cronJobs = toJobMap(collection);
			thread.interrupt();
		} // synch
		if (log.isInfoEnabled()) {
			log.info("reload() - done");}
	} // reload


	/**
	 * @param jobs
	 * @return
	 */
	private Map<String, CronJob> toJobMap(Collection<CronJob> jobs) {
		Map<String, CronJob> jobMapper = new HashMap<String, CronJob>();
		if (null!=jobs) {
			for (CronJob job: jobs) {
				jobMapper.put(job.getJobId(), job);
			}
		}
		return jobMapper;
	}
	
	/**
	 * if firstRun is true, all the enabled startup jobs will be run.
	 * 
	 * @param firstRun
	 *            true to request running of the startup jobs
	 */
	public void setFirstRun(boolean firstRun) {
		synchronized (lock) {
			this.firstRun = firstRun;
			thread.interrupt();
		} // synch
	} // setFirstRun

	/**
	 * looks up the job id in the list and returns it
	 * 
	 * @param id
	 *            the name of the cron job to start
	 * @return the requested cron job
	 * @throws RuntimeException
	 *             if the specified job name is not found
	 */
	private CronJob getJob(String id) throws RuntimeException {
		CronJob cronJob = null;
		// synching to make sure the jobs can't be reloaded during the lookup
		synchronized (lock) {
//			Iterator<CronJob> iter = cronJobs.iterator();
//			while (iter.hasNext()) {
//				cronJob = (CronJob) iter.next();
//				if (cronJob.getJobId().equals(id))
//					break;
//			} // for
			cronJob = cronJobs.get(id);
		} // synch

		if (cronJob == null) {
			throw new RuntimeException("The specified cron job id [" + id
					+ "] was not found.");
		}
		return cronJob;
	} // getJob

	/**
	 * Runs the specificed job immediately if is not already running (even if it
	 * is disabled. Returns true if it starts the job, false if it does not.
	 * 
	 * @param id
	 *            the id of the cron job to start
	 * @return true if the job was started; false if it is already running
	 * @throws RuntimeException
	 *             if the specified job name is not found
	 */
	public boolean forceRunCronJob(String id) throws RuntimeException {
		CronJob cronJob = getJob(id);
		if (!cronJob.getRunning()) {
			if (innerLog.isInfoEnabled()) {
				innerLog.info("Force running job [" + cronJob.getJobId() + "/"
						+ cronJob.getJobName() + "]");}
			if (innerLog.isDebugEnabled()) {
				innerLog.debug("Starting thread for job [" + cronJob.getJobId()
						+ "/" + cronJob.getJobName() + "]");}
			Thread newThread = new Thread(cronJob);
			newThread.setName("CronJob-" + newThread.getName());
			newThread.setDaemon(true);
			newThread.start();
			return true;
		} // if
		return false;
	} // forceRunCronJob

	/**
	 * requests for the specified cron job to terminate. actual terminate
	 * depends on the implementing class
	 * 
	 * @param id
	 *            the id of the cron job to terminate
	 * @throws RuntimeException
	 *             if the specified job name is not found
	 */
	public void terminateCronJob(String id) throws RuntimeException {
		getJob(id).terminate();
	} // enableCronJob

	/**
	 * disabled the specified cron job
	 * 
	 * @param id
	 *            the id of the cron job to enable
	 * @throws RuntimeException
	 *             if the specified job name is not found
	 */
	public void enableCronJob(String id) throws RuntimeException {
		getJob(id).enable();
		thread.interrupt();
	} // enableCronJob

	/**
	 * disabled the specified cron job
	 * 
	 * @param id
	 *            the id of the cron job to disable
	 * @throws RuntimeException
	 *             if the specified job name is not found
	 */
	public void disableCronJob(String id) throws RuntimeException {
		getJob(id).disable();
		thread.interrupt();
	} // disableCronJob

	/**
	 * returns a set of CronJob objects
	 * 
	 * @return a set of CronJob objects
	 */
	public Set<CronJob> getCronJobs() {
		return SetUtils.unmodifiableSet(new TreeSet<CronJob>(cronJobs.values()));
	} // getCronJobs

	// inner class
	/**
	 * This class is used by Cron to evaluate schedules and kickoff CronJobs for
	 * the specific minute it is assigned.
	 * 
	 * @author <a href="mailto:junkmail.java@jarretttaylor.com">Jarrett
	 *         Taylor</a>
	 */
	class CronRunner extends Thread {
		/**
		 * used to evaluate the cron schedules
		 */
		private GregorianCalendar cal;
		/**
		 * passed in from Cron (see its firstRun javadoc)
		 */
		private boolean firstRun;
		/**
		 * used to keep a reference to the runner thread
		 */
		private Thread thread;

		/**
		 * creats a new CronRunner object and sets the date and firstRun values
		 * 
		 * @param date
		 * @param firstRun
		 */
		public CronRunner(Date date, boolean firstRun) {
			cal = new GregorianCalendar();
			cal.setTime(date);
			this.firstRun = firstRun;
		} // constructor

		/**
		 * loops through all cronJobs, evalues scheules, and starts threads for
		 * jobs that need to run
		 */
		@Override
		public void run() {
			thread = Thread.currentThread();
			if (innerLog.isDebugEnabled() && this.firstRun) {
				innerLog.debug("Checking for startup enabled jobs.");}
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int minute = cal.get(Calendar.MINUTE);
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			int month = cal.get(Calendar.MONTH) + 1;
			int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

			Iterator<CronJob> iter = getCronJobs().iterator();
			while (iter.hasNext()) {
				CronJob cronJob = (CronJob) iter.next();
				try {
					if (!cronJob.getDisabled()
							&& !cronJob.getRunning()
							&& ((this.firstRun && (cronJob.getStartup() || Utils
									.isReboot(cronJob.getSchedule()))) || Utils
									.shouldRun(Utils.convertSchedule(cronJob
											.getSchedule()), minute, hour,
											dayOfMonth, month, dayOfWeek))) {
						if (innerLog.isDebugEnabled()) {
							innerLog.debug("Starting thread for job ["
									+ cronJob.getJobId() + "/"
									+ cronJob.getJobName() + "]");}
						Thread newThread = new Thread(cronJob);
						newThread.setDaemon(true);
						newThread.setName("CronJob-" + newThread.getName());
						newThread.start();
					} // if
				} // try
				catch (IllegalArgumentException e) {
					innerLog.warn("Invalid schedule for job ["
							+ cronJob.getJobId() + "/" + cronJob.getJobName()
							+ "]");
				} // catch
			} // for
			thread = null;
		} // run

		/**
		 * attempts to interrupt the CronRunner's thread
		 */
		public void terminate() {
			// probably pointless to interrupt a thread that shouldn't need
			// interrupting
			if (thread != null) {
				if (innerLog.isInfoEnabled()) {
					innerLog.info("terminate() - Attempting to interrupt: "
							+ thread.getName());}
				thread.interrupt();
			} // if
		} // terminate

	} // inner class

} // class
