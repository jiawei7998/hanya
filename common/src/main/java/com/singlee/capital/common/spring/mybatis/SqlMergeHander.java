package com.singlee.capital.common.spring.mybatis;

import java.sql.PreparedStatement;

public interface SqlMergeHander<T> {
	
	void handle(PreparedStatement pst, T o);
}
