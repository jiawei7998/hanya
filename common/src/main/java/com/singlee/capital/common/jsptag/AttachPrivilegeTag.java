package com.singlee.capital.common.jsptag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 附件权限标签 
 * 
 * @author LyonChen
 *
 */
public class AttachPrivilegeTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5933571425253885778L;

	private String privileges;
	
	private int position;

	public void setPrivileges(String privileges) {
		this.privileges = privileges;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	@Override
	public int doStartTag() throws JspException {
		char[] privilegeArray = privileges.toCharArray(); 
		if (privilegeArray[position] == '1') {
			return EVAL_BODY_INCLUDE;
		} else {
			return SKIP_BODY;
		}
	}
}
