package com.singlee.capital.common.util;

import java.util.List;

public interface TreeInterface {
	
	String getId();
	
	String getParentId();
	
	void children(List<? extends TreeInterface> list);
	
	List<? extends TreeInterface> children();
	
	String getText();
	
	String getValue();
	
	int getSort();
	
	String getIconCls();
	
}
