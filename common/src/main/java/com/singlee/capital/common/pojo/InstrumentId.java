package com.singlee.capital.common.pojo;

import com.singlee.capital.common.util.InstrumentIdParser;

import java.io.Serializable;


/**
 * 金融工具 ID 类
 * 
 * @author LyonChen
 * 
 */
public class InstrumentId implements Serializable, Comparable<InstrumentId>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**金融工具代码*/
	private String i_code;
	/**资产类型*/
	private String a_type;
	/**市场类型*/
	private String m_type;
	/**版本，最大者为最新*/
	private int ver_num=-1;

	public InstrumentId(String id){
		InstrumentIdParser.parseId(id, this);
	}
	public InstrumentId(String i_code,String a_type,String m_type){
		this(i_code,a_type,m_type,-1);
	}
	public InstrumentId(String i_code,String a_type,String m_type, int ver){
		setI_code(i_code);
		setA_type(a_type);
		setM_type(m_type);
		setVer_num(ver);
	}
	public InstrumentId() {
	}

	public String getI_code() {
		return i_code;
	}

	public void setI_code(String i_code) {
		this.i_code = i_code;
	}

	public String getA_type() {
		return a_type;
	}

	public void setA_type(String a_type) {
		this.a_type = a_type;
	}

	public String getM_type() {
		return m_type;
	}

	public void setM_type(String m_type) {
		this.m_type = m_type;
	}

	public int getVer_num() {
		return ver_num;
	}
	
	public String getVerNum() {
		return String.valueOf(ver_num);
	}

	public void setVer_num(int ver_num) {
		this.ver_num = ver_num;
	}

	public void setVerNum(String ver_num) {
		try{		this.ver_num = Integer.parseInt(ver_num);}catch(Exception e){	this.ver_num = -1;	}
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;}
		if (obj == null) {
			return false;}
		if (getClass() != obj.getClass()) {
			return false;}
		InstrumentId other = (InstrumentId) obj;
		if (a_type == null) {
			if (other.a_type != null) {
				return false;}
		} else if (!a_type.equals(other.a_type)) {
			return false;}
		if (i_code == null) {
			if (other.i_code != null) {
				return false;}
		} else if (!i_code.equals(other.i_code)) {
			return false;}
		if (m_type == null) {
			if (other.m_type != null) {
				return false;}
		} else if (!m_type.equals(other.m_type)) {
			return false;}
		if(ver_num>=0){ //说明需要有版本支持
			if (ver_num != other.ver_num) {
				return false;}
		}
		return true;
	}
	@Override
	public String toString() {
		return String.format("InstrumentId [i_code=%s, a_type=%s, m_type=%s, ver_num=%s]", i_code, a_type, m_type, ver_num);
	}
	/** 格式化  i_code,a_type,m_type,ver */
	public String toId(){
		return InstrumentIdParser.generateId(i_code,a_type,m_type,ver_num);
	}
	@Override
	public int compareTo(InstrumentId arg0) {
		int i = a_type.compareTo(arg0.getA_type());
		if(i == 0){
			i = m_type.compareTo(arg0.getM_type());
			if( i == 0){
				i = i_code.compareTo(arg0.getI_code());
				if( i == 0){
					if(ver_num == arg0.getVer_num()){
						i = 0;
					}else{
						i = ver_num > arg0.getVer_num() ? 1 : -1;
					}
				}
			}
		}
		return i;
	}

}
