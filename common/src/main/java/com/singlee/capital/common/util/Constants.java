package com.singlee.capital.common.util;

/**
 * 常量定义
 * 
 * @author cz
 * 
 */
public class Constants {
//	/** session key */
//	public static final String JoyardSession = "joyard_session";
//	/** custom http header key */
//	public static final String JoyardHttpHeader = "joyard_header";
//
//	/** admin所使用的机构编号 */
//	public static final String SUPER_INST_ID = "0";
//
	/** 券 */
	public static final String SECURITY = "S";
	/** 资金 */
	public static final String CASH = "C";
//	/** 分隔符 */
//	public static final String SEP_UNDERLINE = "_";
//	public static final String SEP_AND = "&&";
	/** 最大日期 */
	public static final String JudgmentDay = "2099-12-31";
	/** 最小日期 */
	public static final String SystemDay = "1901-01-01";
//	public static final String BusinessCenterCHINA_IB = "CHINA_IB";
//	/**
//	 * --------------------------------------------- 系统模块开始
//	 * -----------------------------------------
//	 **/
//	/**
//	 * 不需要session拦截的地址
//	 * 
//	 * @param url
//	 * @return
//	 */
//	public static final boolean noIntercept(String url) {
//		if (StringUtil.checkEmptyNull(url)) {
//			return false;
//		} else {
//			for (String s : NO_INTERCEPT_URL) {
//				if (url.indexOf(s) > 0) {
//					return true;
//				}
//			}
//			return false;
//		}
//	}
//
//	public static final String[] NO_INTERCEPT_URL = new String[] { 
//			"/LoginSystemController/login", // 登陆
//			"/LoginSystemController/logout", // 注销
//			// 针对 柜员 ，需要根据当前登录机构进行过滤 2014-01-09 
//			//"/FuzzyQueryController/",//模糊查询  
//			"/DictController/",// 字典项
//			"/MktBondController/generateFpML"//外部ETL工具生成FpML条款使用的地址
//	};
//	
//	/**
//	 * 日终执行时，请求拦截（与之前不同之处在于此处拦截的请求需要验证session通过）
//	 * @param url
//	 * @return
//	 */
//	public static final boolean dayEndIntercept(String url) {
//		if (StringUtil.checkEmptyNull(url)) {
//			return false;
//		}else {
//			for (String s : DAYEND_EXECUTING_URL) {
//				if (url.indexOf(s) > 0) {
//					return true;
//				}
//			}
//			return false;
//		}
//	};
//
//	public static final String[] DAYEND_EXECUTING_URL = new String[] {
//			"/LoginSystemController/search",//菜单查询
//			"__module_id=530001",//页面加载
//			"/DayEndManageController/", //日终函数
//			"/DayendRollingAccountController/"//轧帐相关
//	};


//	/**
//	 * --------------------------------------------- 业务日期
//	 * ----------------------------------------------
//	 **/
//	public static class SettlementDate {
//		private static String settlementDate;
//
//		/**
//		 * 当前业务日 + 系统时间 HH:MM:SS
//		 * @return
//		 */
//		public static final String getSettlementDateTime() {
//			return settlementDate + " " + DateTimeUtil.getLocalTime();
//		}
//		/**
//		 * 获得当前业务结算日 从结算日期表中获取
//		 * 
//		 * @return
//		 */
//		public static final String getSettlementDate() {
//			try {
//				Object obj = SpringContextHolder.getBean("dayendDateService");
//				Object dateObj = obj.getClass().getMethod("getDayendDate").invoke(obj);
//				settlementDate = dateObj.toString();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return settlementDate;
//		}
//
//		public static final void setSettlementDate(String date) {
//			settlementDate = date;
//		}
//		
//		public static final boolean isSettlementDate(String date){
//			return settlementDate.equals(date);
//		}
//	}

//	/**
//	 * --------------------------------------------- 系统模块结束
//	 * -----------------------------------------
//	 **/
//
//	/**
//	 * --------------------------------------------- 交易模块开始
//	 * -----------------------------------------
//	 **/
//	// 定义资产类型 规则：金融市场分类_结构分类_资产类型
////	public static class AType {
////		/** 债券 */
////		public static final String Bond = DictConstants.FinMarketType.CapitalMarket.value() + SEP_UNDERLINE + DictConstants.Structure.Spot.value() + SEP_UNDERLINE + DictConstants.Category.Bond.value();
////		/** 资产支持债券 */
////		public static final String BondAbs = DictConstants.FinMarketType.CapitalMarket.value() + SEP_UNDERLINE + DictConstants.Structure.Spot.value() + SEP_UNDERLINE + DictConstants.Category.Abs.value();
////		/** 债券远期 */
////		public static final String BondFward = DictConstants.FinMarketType.CapitalMarket.value() + SEP_UNDERLINE + DictConstants.Structure.Forward.value() + SEP_UNDERLINE + DictConstants.Category.Bond.value();
////		/** 回购 */
////		public static final String Repo = DictConstants.FinMarketType.CapitalMarket.value() + SEP_UNDERLINE + DictConstants.Structure.Spot.value() + SEP_UNDERLINE + DictConstants.Category.Repo.value();
////		/** 活期同业业务 */
////		public static final String IBCurrent = DictConstants.FinMarketType.MoneyMarket.value() + SEP_UNDERLINE + DictConstants.Structure.Current.value() + SEP_UNDERLINE + DictConstants.Category.Deposit.value();
////		/** 定期同业业务 */
////		public static final String IBFixed = DictConstants.FinMarketType.MoneyMarket.value() + SEP_UNDERLINE + DictConstants.Structure.Fixed.value() + SEP_UNDERLINE + DictConstants.Category.Deposit.value();
////		
////	}
//
//	// 定义资产小类 规则：AType + 自定义的后缀
////	public static class PType {
////		/** 债券 */
////		public static final String Bond = AType.Bond;
////		/** 债券远期 */
////		public static final String BondFward = AType.BondFward;
////		/** 债券承销 */
////		public static final String BondUnderwriting = AType.BondFward + SEP_UNDERLINE + "Underwriting";
////		/** 债券分销 */
////		public static final String BondDistribution = AType.BondFward + SEP_UNDERLINE + "Distribution";
////		/** 质押式回购 */
////		public static final String PledgeRepo = AType.Repo + SEP_UNDERLINE + "PLEDGE";
////		/** 买断式回购 */
////		public static final String OutRightRepo = AType.Repo + SEP_UNDERLINE + "OUTRIGHT";
////		/** 存放同业活期 */
////		public static final String DepsitToBankCurrent = AType.IBCurrent + SEP_UNDERLINE + "DTIB";
////		/** 同业存放活期 */
////		public static final String DepsitFromBankCurrent = AType.IBCurrent + SEP_UNDERLINE + "DFIB";
////		/** 存放同业定期 */
////		public static final String DepsitToBankFixed = AType.IBFixed + SEP_UNDERLINE + "DTIB";
////		/** 同业存放定期 */
////		public static final String DepsitFromBankFixed = AType.IBFixed + SEP_UNDERLINE + "DFIB";
////		/** 同业拆借 */
////		public static final String LBFromIBFixed  = AType.IBFixed + SEP_UNDERLINE + "LBFIB";
////		/** 同业代付 */
////		public static final String PayFromIBFixed  = AType.IBFixed + SEP_UNDERLINE + "PFIB";
////		/** 同业借款 */
////		public static final String LoanFromIBFixed  = AType.IBFixed + SEP_UNDERLINE + "LFIB";
////	}
//	
//
//	// 定义资产小类 规则：AType + 自定义的后缀
	public static class PClass {
//		/** 质押式正回购 */
		public static final String PledgeSellRepo = "质押式正回购";
//		/** 质押式逆回购 */
		public static final String PledgeReverseRepo = "质押式逆回购";
		/** 买断式正回购 */
		public static final String OutRightSellRepo = "买断式正回购";
		/** 买断式逆回购 */
		public static final String OutRightReverseRepo = "买断式逆回购";
//		/** 存放同业定期 **/
		public static final String DepsitToBankFixed = "存放同业定期";
//		/** 同业存放定期 **/
//		public static final String DepsitFromBankFixed = "同业存放定期";
		/** 拆入 **/
		public static final String IbBorrowing = "拆入";
		/** 拆出 **/
		public static final String IbLending = "拆出";
//		/** 融入 **/
//		public static final String BondlendIn = "融入";
//		/** 融出 **/
//		public static final String BondlendOut = "融出";
//		/** 债券远期买入 **/
//		public static final String BondFwdBuy = "债券远期买入";
//		/** 债券远期卖出 **/
//		public static final String BondFwdSell = "债券远期卖出";
	}
//
//	// 定义映射关系
//	public static class Mapping {
//		// 定义P_TYPE字典翻译
////		public static final Map<String, String> PTypeDictMap = new HashMap<String, String>();
//		// 定义A_TYPE字典翻译
////		public static final Map<String, String> ATypeDictMap = new LinkedHashMap<String, String>();
//		// 定义A_TYPE、P_TYPE对应关系
//		public static final Map<String, String[]> ATypePTypeMap = new HashMap<String, String[]>();
//		// 定义A_TYPE、P_TYPE、TRDTYPE对应关系
//		public static final Map<String, String[]> ATypePTypeTrdTypeMap = new HashMap<String, String[]>();
//
//		static {
////			ATypeDictMap.put(DictConstants.AType.Bond.value(), "债券");
////			ATypeDictMap.put(DictConstants.AType.BondFward.value(), "债券远期");
////			ATypeDictMap.put(DictConstants.AType.Repo.value(), "回购");
////			ATypeDictMap.put(DictConstants.AType.IBCurrent.value(), "活期同业业务");
////			ATypeDictMap.put(DictConstants.AType.IBFixed.value(), "定期同业业务");
////			
////			PTypeDictMap.put(DictConstants.PType.Bond.value(), "债券");
////			PTypeDictMap.put(DictConstants.PType.BondFward.value(), "债券远期");
//////			PTypeDictMap.put(DictConstants.PType.BondUnderwriting, "债券承销");
//////			PTypeDictMap.put(DictConstants.PType.BondDistribution, "债券分销");
////			PTypeDictMap.put(DictConstants.PType.PledgeRepo.value(), "质押式回购");
////			PTypeDictMap.put(DictConstants.PType.OutRightRepo.value(), "买断式回购");
////			PTypeDictMap.put(DictConstants.PType.DepsitToBankCurrent.value(), "存放同业活期");
////			PTypeDictMap.put(DictConstants.PType.DepsitFromBankCurrent.value(), "同业存放活期");
////			PTypeDictMap.put(DictConstants.PType.DepsitToBankFixed.value(), "存放同业定期");
////			PTypeDictMap.put(DictConstants.PType.DepsitFromBankFixed.value(), "同业存放定期");
//////			PTypeDictMap.put(DictConstants.PType.LBFromIBFixed, "同业拆借");
////			PTypeDictMap.put(DictConstants.PType.PayFromIBFixed, "同业代付");
////			PTypeDictMap.put(DictConstants.PType.LoanFromIBFixed, "同业借款");
//			
//			ATypePTypeMap.put(DictConstants.AType.Bond.value(), new String[] { DictConstants.PType.Bond.value()});
//			ATypePTypeMap.put(DictConstants.AType.BondAbs.value(), new String[] { DictConstants.PType.BondAbs.value()});
//			//ATypePTypeMap.put(DictConstants.AType.BondFward.value(), new String[] { DictConstants.PType.BondFward.value() });
//			ATypePTypeMap.put(DictConstants.AType.Repo.value(), new String[] { DictConstants.PType.PledgeRepo.value(),DictConstants.PType.OutRightRepo.value()});
//			ATypePTypeMap.put(DictConstants.AType.IBCurrent.value(), new String[] { DictConstants.PType.DepsitFromBankCurrent.value(), DictConstants.PType.DepsitToBankCurrent.value()});
//			ATypePTypeMap.put(DictConstants.AType.IBFixed.value(), new String[] { DictConstants.PType.DepsitFromBankFixed.value(), DictConstants.PType.DepsitToBankFixed.value() });
//			ATypePTypeMap.put(DictConstants.AType.Bill.value(), new String[] { DictConstants.PType.Bill.value() });
//			ATypePTypeMap.put(DictConstants.AType.UnAsset.value(), new String[] { DictConstants.PType.UnAsset.value() });
//			ATypePTypeMap.put(DictConstants.AType.Money.value(), new String[] { DictConstants.PType.MoneyOpen.value(),DictConstants.PType.MoneyClosed.value()});
//			ATypePTypeMap.put(DictConstants.AType.Assetplan.value(), new String[] { DictConstants.PType.AssetplanClosed.value(),DictConstants.PType.AssetplanOpen.value()});
//			ATypePTypeMap.put(DictConstants.AType.IBLb.value(), new String[] { DictConstants.PType.BankLoan.value()});
//			ATypePTypeMap.put(DictConstants.AType.Issue.value(), new String[] { DictConstants.PType.IbcdIssue.value(),DictConstants.PType.BondIssue.value()});
//			ATypePTypeMap.put(DictConstants.AType.Lend.value(), new String[] { DictConstants.PType.BondLend.value()});
//			
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Bond.value() + SEP_AND + DictConstants.PType.Bond.value(), new String[] { DictConstants.TrdType.BondBuy.value(), DictConstants.TrdType.BondSell.value(),DictConstants.TrdType.DistributeBuy.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.BondAbs.value() + SEP_AND + DictConstants.PType.BondAbs.value(), new String[] { DictConstants.TrdType.BondBuy.value(), DictConstants.TrdType.BondSell.value(),DictConstants.TrdType.DistributeBuy.value() });
//			//ATypePTypeTrdTypeMap.put(DictConstants.AType.BondFward.value() + SEP_AND + DictConstants.PType.BondFward.value(), new String[] { "远期买入","远期卖出" });
////			ATypePTypeTrdTypeMap.put(DictConstants.AType.BondFward + SEP_AND + DictConstants.PType.BondUnderwriting, new String[] { "承销注册"});
////			ATypePTypeTrdTypeMap.put(DictConstants.AType.BondFward + SEP_AND + DictConstants.PType.BondDistribution, new String[] { "分销买入","分销卖出" });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Repo.value() + SEP_AND + DictConstants.PType.PledgeRepo.value(), new String[] { DictConstants.TrdType.SellRepoPledged.value(),DictConstants.TrdType.ReverseRepoPledged.value() });
////			ATypePTypeTrdTypeMap.put(DictConstants.AType.Repo.value() + SEP_AND + DictConstants.PType.OutRightRepo.value(), new String[] { "买断式正回购","买断式逆回购" });
//			
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBCurrent.value() + SEP_AND + DictConstants.PType.DepsitToBankCurrent.value(), new String[] { DictConstants.TrdType.CurrentToBankTransfer.value(), DictConstants.TrdType.CurrentToBankBack.value(), DictConstants.TrdType.CurrentToBankAccOff.value()});
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBCurrent.value() + SEP_AND + DictConstants.PType.DepsitFromBankCurrent.value(), new String[] { DictConstants.TrdType.CurrentFromBankIn.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBFixed.value() + SEP_AND + DictConstants.PType.DepsitToBankFixed.value(), new String[] {  DictConstants.TrdType.DepositToBankFixed.value()  });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBFixed.value() + SEP_AND + DictConstants.PType.DepsitFromBankFixed.value(), new String[] {  DictConstants.TrdType.DepositFromBankFixed.value()  });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Bill.value() + SEP_AND + DictConstants.PType.Bill.value(), new String[] { DictConstants.TrdType.BuyRediscounts.value(), DictConstants.TrdType.SaleRediscounts.value(), DictConstants.TrdType.BuyingAndReturnSale.value(),DictConstants.TrdType.SaleAndRepo.value(),DictConstants.TrdType.Rediscount.value()});
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.UnAsset.value() + SEP_AND + DictConstants.PType.UnAsset.value(), new String[] { DictConstants.TrdType.PGNFinancial.value(), DictConstants.TrdType.NOPGNFinancial.value(),DictConstants.TrdType.Trust.value(),DictConstants.TrdType.DirectionalAsset.value(),DictConstants.TrdType.Other.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Money.value() + SEP_AND + DictConstants.PType.MoneyOpen.value(), new String[] { DictConstants.TrdType.MoneyBuy.value(), DictConstants.TrdType.MoneySell.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Money.value() + SEP_AND + DictConstants.PType.MoneyClosed.value(), new String[] { DictConstants.TrdType.MoneyBuy.value(), DictConstants.TrdType.MoneySell.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Assetplan.value() + SEP_AND + DictConstants.PType.AssetplanClosed.value(), new String[] { DictConstants.TrdType.AssetPlanBuy.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Assetplan.value() + SEP_AND + DictConstants.PType.AssetplanOpen.value(), new String[] { DictConstants.TrdType.AssetPlanSell.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBLb.value() + SEP_AND + DictConstants.PType.BankLoan.value(), new String[] { DictConstants.TrdType.IbBorrowing.value(),DictConstants.TrdType.IbLending.value()});
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Repo.value() + SEP_AND + DictConstants.PType.OutRightRepo.value(), new String[] { DictConstants.TrdType.SellRepoOutright.value(),DictConstants.TrdType.ReverseRepoOutright.value() });
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Issue.value() + SEP_AND + DictConstants.PType.IbcdIssue.value(), new String[] { DictConstants.TrdType.IBCDIssue.value()});
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Issue.value() + SEP_AND + DictConstants.PType.BondIssue.value(), new String[] { DictConstants.TrdType.BondIssue.value()});
//			ATypePTypeTrdTypeMap.put(DictConstants.AType.Lend.value() + SEP_AND + DictConstants.PType.BondLend.value(), new String[] { DictConstants.TrdType.BondLendIn.value(),DictConstants.TrdType.BondLendOut.value()});
//			//			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBFixed + SEP_AND + DictConstants.PType.LBFromIBFixed, new String[] { "拆入","拆出" });
////			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBFixed + SEP_AND + DictConstants.PType.PayFromIBFixed, new String[] { "" });
////			ATypePTypeTrdTypeMap.put(DictConstants.AType.IBFixed + SEP_AND + DictConstants.PType.LBFromIBFixed, new String[] { "" });
//		}
//	}
//
//	
//	
//	/**
//	 * 判断金融工具是否是债券
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isBond(InstrumentId id){
//		if(DictConstants.AType.Bond.value().equals(id.getA_type())||
//				DictConstants.AType.BondAbs.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是回购
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isPledgeRepo(InstrumentId id){
//		String trdType = getTrdtypeById(id.getI_code());
//		if(DictConstants.AType.Repo.value().equals(id.getA_type()) && 
//		   (DictConstants.TrdType.SellRepoPledged.value().equals(trdType) ||
//		   DictConstants.TrdType.ReverseRepoPledged.value().equals(trdType))){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是回购
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isOutRightRepo(InstrumentId id){
//		String trdType = getTrdtypeById(id.getI_code());
//		if(DictConstants.AType.Repo.value().equals(id.getA_type()) && 
//		   (DictConstants.TrdType.ReverseRepoOutright.value().equals(trdType) ||
//		   DictConstants.TrdType.SellRepoOutright.value().equals(trdType))){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	
//	
//	/**
//	 * 判断金融工具是否是回购
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isRepo(InstrumentId id){
//		return DictConstants.AType.Repo.compare(id.getA_type());
//	}
	/**
	 * 通过审批单号、交易单号、委托单号获取到业务类型
	 * @param id
	 * @return
	 */
	public static final String getTrdtypeById(String id){
		if(StringUtil.isNullOrEmpty(id) || id.length() != 21) {
			return null;}
		return id.substring(11, 15);
	}
//	
//	/**
//	 * 判断金融工具是否是银行间活期存款 （同业存放、存放同业)
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isIBCurrent(InstrumentId id){
//		if(DictConstants.AType.IBCurrent.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	
//	/**
//	 * 判断金融工具是否是非标资产
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isUnAsset(InstrumentId id){
//		if(DictConstants.AType.UnAsset.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	
//	/**
//	 * 判断金融工具是否是票据
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isBill(InstrumentId id){
//		if(DictConstants.AType.Bill.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是理财
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isMoney(InstrumentId id){
//		if(DictConstants.AType.Money.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是定向资产管理计划
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isAssetplan(InstrumentId id){
//		if(DictConstants.AType.Assetplan.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是银行间定期存款 （同业存放、存放同业)
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isIBFixed(InstrumentId id){
//		if(DictConstants.AType.IBFixed.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是同业借款（同业拆借、同业借款、质押借款)
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isIBLb(InstrumentId id){
//		if(DictConstants.AType.IBLb.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是发行类（同业存单发行、债券发行）
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isIssue(InstrumentId id){
//		if(DictConstants.AType.Issue.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	/**
//	 * 判断金融工具是否是借贷（债券借贷)
//	 * @param id
//	 * @return
//	 */
//	public static final boolean isLend(InstrumentId id){
//		if(DictConstants.AType.Lend.value().equals(id.getA_type())){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	public static class PartyKind {
//		public static final String BigKind = "1";//核算
//        public static final String SmallKind = "2";//抹账
//        public static final String Party = "0";//其他
//	}
//	public static class DbType {
//		public static final String ORACLE = "ORACLE";//核算
//        public static final String DB2 = "DB2";//抹账
//	}
//	/**
//	 * --------------------------------------------- 交易模块结束
//	 * -----------------------------------------
//	 **/
//	
//	/**
//	 * --------------------------------------------- 结算模块开始
//	 * -----------------------------------------
//	 **/
//	
//	/**
//	 * --------------------------------------------- 结算模块结束
//	 * -----------------------------------------
//	 **/
	public static class SettlePlatform {
		public static final String ZZDPlatform = "1";//中债登结算
        public static final String QSSPlatform = "2";//清算所
        public static final String OtherPlatform = "9";//其他
	}
	public static class settleOperateType {
		public static final String accounting = "1";//核算
        public static final String earsing = "2";//抹账
        public static final String Other = "9";//其他
	}
//	
//	
//	/**
//	 * --------------------------------------------- 限额模块开始
//	 * -----------------------------------------
//	 **/
//	public static class LimitCallType {
//		public static final String APPFLOW = "APPFLOW";
//		public static final String CONFIRM = "CONFIRM";
//	}
//	public static class LimitSingleDayType {
//		public static final String ORDERDATE = "orderDate";
//		public static final String SETTLEDATE = "settleDate";
//	}
//	public static class LimitSaveType{
//		public static final String INSERT = "INSERT";
//		public static final String DELETE = "DELETE";
//	}
//	public static class LimitVariType{
//		public static final String NUM = "num";
//		public static final String DEN = "den";
//	}
//	// 定义映射关系
//	public static class LimitMapping {
//			// 定义A_TYPE、P_TYPE对应关系
//			public static final Map<String, String[]> LimitLevelMap = new HashMap<String, String[]>();
//			static {
//				LimitLevelMap.put("2", new String[] { "1","7"});
//				LimitLevelMap.put("3", new String[] { "1","3","7"});
//				LimitLevelMap.put("4", new String[] { "1","3","4","7"});
//				LimitLevelMap.put("5", new String[] { "1","2","3","4","7"});
//				LimitLevelMap.put("6", new String[] { "1","2","3","4","5","7"});
//				LimitLevelMap.put("7", new String[] { "1","2","3","4","5","6","7"});
//		}
//	}
//	/**
//	 * --------------------------------------------- 限额模块结束
//	 * -----------------------------------------
//	 **/
//	
//
	// 核算任务
	public static class AccountTask {
		public static final String[] taskList = new String[]{"JOYARD"};
		public static final String KJHS = "JOYARD";//会计核算
        public static final String MNHS = "MNHS";//模拟核算
	}
	
	public static class AllProfile{
		public static final String test = "test";//测试类
		public static final String dft = "default";//默认
		public static final String crbc = "crbc";//华润银行
//		public static final String byk = "byk";//营口银行
	}
}
