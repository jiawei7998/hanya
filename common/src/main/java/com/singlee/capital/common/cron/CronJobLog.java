package com.singlee.capital.common.cron;

public interface CronJobLog {
	
	public boolean ok();
	
	void setResult(String s);

}
