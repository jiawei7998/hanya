package com.singlee.capital.common.util;

import com.singlee.capital.common.exception.RException;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BeanUtil {

	 
	private static final BigDecimal BIGDECIMAL_ZERO = new BigDecimal("0");    
	static {    
	    // 这里一定要注册默认值，使用null也可以    
	    BigDecimalConverter bd = new BigDecimalConverter(BIGDECIMAL_ZERO);    
	    ConvertUtils.register(bd, java.math.BigDecimal.class);    
	}   
	
	public static final <T> void populate(T bean, Map<String, Object> post) {
		try {
			BeanUtils.populate(bean, post);
		} catch (Exception e) {
			JY.raiseRException("Map-->%s error: %s", bean.getClass().getName(),
					e.getMessage());
		}
	}
	/**
	 * map转bean
	 * @param map
	 * @param obj
	 * @return
	 */
	public static Object mapToBean(Class<?> type, Map<?, ?> map)    
            throws IntrospectionException, IllegalAccessException,    
            InstantiationException, InvocationTargetException {    
        BeanInfo beanInfo = Introspector.getBeanInfo(type); // 获取类属性    
        Object obj = type.newInstance(); // 创建 JavaBean 对象    
        // 给 JavaBean 对象的属性赋值    
        PropertyDescriptor[] propertyDescriptors =  beanInfo.getPropertyDescriptors();    
        for (int i = 0; i< propertyDescriptors.length; i++) {    
            PropertyDescriptor descriptor = propertyDescriptors[i];    
            String propertyName = descriptor.getName();    
    
            if (map.containsKey(propertyName)) {    
                // 下面一句可以 try 起来，这样当一个属性赋值失败的时候就不会影响其他属性赋值。    
                Object value = map.get(propertyName);    
                Object[] args = new Object[1];    
                args[0] = value;    
                descriptor.getWriteMethod().invoke(obj, args);    
            }    
        }    
        return obj;    
    }  
	/**
	 * bean转map
	 * 
	 * @param obj
	 * @return
	 */
	public static Map<String, Object> beanToMap(Object obj) {
		Map<String, Object> params = new HashMap<String, Object>(0);
		try {
			PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
			PropertyDescriptor[] descriptors = propertyUtilsBean
					.getPropertyDescriptors(obj);
			for (int i = 0; i < descriptors.length; i++) {
				String name = descriptors[i].getName();
				if (!"class".equals(name)) {
					params.put(name,
							propertyUtilsBean.getNestedProperty(obj, name));
				}
			}
		} catch (Exception e) {
			throw new RException(e);
		}
		return params;
	}

	/**
	 * bean转hashmap
	 * 
	 * @param obj
	 * @return
	 */
	public static HashMap<String, Object> beanToHashMap(Object obj) {
		HashMap<String, Object> params = new HashMap<String, Object>(0);
		try {
			PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
			PropertyDescriptor[] descriptors = propertyUtilsBean
					.getPropertyDescriptors(obj);
			for (int i = 0; i < descriptors.length; i++) {
				String name = descriptors[i].getName();
				if (!"class".equals(name)) {
					params.put(name,
							propertyUtilsBean.getNestedProperty(obj, name));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return params;
	}
	
	/**
	 * 复制不为空的属性
	 * @param dest
	 * @param orig
	 */
	public static void copyNotEmptyProperties(Object dest,Object orig){
		try {
			PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
			PropertyDescriptor[] descriptors = propertyUtilsBean
					.getPropertyDescriptors(orig);
			Map<String,Object> map =new HashMap<String,Object>();
			for (int i = 0; i < descriptors.length; i++) {
				String name = descriptors[i].getName();
				if (!"class".equals(name)) {
					Object obj = propertyUtilsBean.getNestedProperty(orig, name);
				       if (obj == null)  
				        {  
				            continue; 
				        }  
				        /*if ((obj instanceof List))  tabs未点开时子表(增信，基础资产，风险资产等)上传为null,故对空数组不做处理,上传为null直接跳过删除保存逻辑
				        {  
				        	if(((List) obj).size() == 0){
				        		continue; 
				        	}
				        } */ 
				        if ((obj instanceof String))  
				        {  
				        	if("".equals(((String) obj).trim())){
				        		continue;
				        	}
				        }
				        map.put(name, obj);
				}
			}
			BeanUtils.populate(dest, map);
		} catch (Exception e) {
			throw new RException(e);
		}
	}
	
	/**
	 * 复制不为null的属性
	 * @param dest
	 * @param orig
	 */
	public static void copyNotNullProperties(Object dest,Object orig){
		try {
			PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
			PropertyDescriptor[] descriptors = propertyUtilsBean
					.getPropertyDescriptors(orig);
			Map<String,Object> map =new HashMap<String,Object>();
			for (int i = 0; i < descriptors.length; i++) {
				String name = descriptors[i].getName();
				if (!"class".equals(name)) {
					Object obj = propertyUtilsBean.getNestedProperty(orig, name);
				       if (obj == null)  
				        {  
				            continue; 
				        }  
				        /*if ((obj instanceof List))  tabs未点开时子表(增信，基础资产，风险资产等)上传为null,故对空数组不做处理,上传为null直接跳过删除保存逻辑
				        {  
				        	if(((List) obj).size() == 0){
				        		continue; 
				        	}
				        } 
				        if ((obj instanceof String))  
				        {  
				        	if(((String) obj).trim().equals("")){
				        		continue;
				        	}
				        }*/ 
				        map.put(name, obj);
				}
			}
			BeanUtils.populate(dest, map);
		} catch (Exception e) {
			throw new RException(e);
		}
	}
}
