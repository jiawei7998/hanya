package com.singlee.capital.common.spring;

import com.singlee.capital.common.util.JY;
import org.springframework.aop.framework.AopContext;


/**
 * spring aop 内部方法调用 缺陷，有需要在实现方法内部调用的可以继承此类通过myself()进行内部方法调用！
 * @author x230i
 *
 * @param <T>
 */
public abstract class SelfCallWrapperService<T> {

	/***
	 * ----以上为对外服务-------------------------------
	 * 为了 代理 能进入
	 * 采取这种方式的话，exposeProxy=true  
	 * @return
	 */
    @SuppressWarnings("unchecked")
	protected T myself(){  
    	JY.require(AopContext.currentProxy() != null, "请检查 aop:aspectj-autoproxy expose-proxy=true" );
    	
    	return (T) AopContext.currentProxy() ;  
    }  

}
