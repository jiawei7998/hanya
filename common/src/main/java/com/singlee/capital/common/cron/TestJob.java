package com.singlee.capital.common.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class TestJob implements CronRunnable {
	private static final Logger log = LoggerFactory.getLogger(TestJob.class);

	@Override
	public boolean execute(Map<String, Object> parameters) throws Exception {
		log.debug("定时任务测试");
		return true;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub
		
	}
	
}
