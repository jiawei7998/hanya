package com.singlee.capital.common.jsptag;


import com.singlee.capital.common.session.SessionService;
import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.spring.SpringContextHolder;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;


/**
 * 权限拦截标签 
 * 
 * @author LyonChen
 *
 */
public class PrivilegeTag extends TagSupport {
	
	private static final long serialVersionUID = -532517444654109642L;

	private String functionId; // 功能点
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	

	@Override
	public int doStartTag() throws JspException {
		// 获得当前session/其roleid 校验 roleid是否有functinId
		// 校验 是否有 functionId属性，没有的话，直接过滤掉 body部分
		if (StringUtils.isEmpty(functionId) ) {
			return Tag.SKIP_BODY;
		}
		// 没有配置 sessionService bean 同上处理
		SessionService sessionService = SpringContextHolder.getBean("sessionService");
		if(sessionService == null){
			return Tag.SKIP_BODY;
		}
		// 校验当前session,从httpsession中获得
		SlSession session = null; //SessionManager.getSessionByHttpSession(super.pageContext.getSession());
		if (session == null) {
			return Tag.SKIP_BODY;
		} 
		// 获得 url 地址 
		@SuppressWarnings("unused")
		HttpServletRequest hr = (HttpServletRequest) super.pageContext.getRequest();
		String uri = hr.getRequestURI();
		String ctx = hr.getContextPath();
		String url = uri.substring(ctx.length(), uri.length());
		// 校验 权限		
		if (sessionService.checkSlSessionUrlFunction(session, url, functionId)) {
			return Tag.EVAL_BODY_INCLUDE;
		}
		return Tag.SKIP_BODY;//
	}
	
}