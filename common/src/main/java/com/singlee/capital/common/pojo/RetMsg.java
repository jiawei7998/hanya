package com.singlee.capital.common.pojo;

import com.singlee.capital.common.spring.json.FastJsonUtil;


/**
 * 返回消息定义回复
 */
public class RetMsg<T> {

	private String code;
	private String desc;
	private String detail;
	private T obj;

	public RetMsg(String code, String desc, String detail, T obj) {
		super();
		this.code = code;
		this.desc = desc;
		this.obj = obj;
		this.detail = detail;
	}
	public RetMsg(String code, String desc) {
		this(code, desc, null, null);
	}
	
	public RetMsg(String code, String desc, String detail) {
		this(code, desc, detail, null);
	}

	public RetMsg(String code, String desc, Exception e) {
		this(code, desc, e.getLocalizedMessage());
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public T getObj() {
		return obj;
	}
	public void setObj(T obj) {
		this.obj = obj;
	}

	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	@Override
	public String toString(){
		return String.format("[%s]%s", code, desc);
	}
	
	/*public String toDetailString(){
		return String.format("%s\r\n %s \r\n %s", toString(),detail, obj.toString());
	}
	*/

	public String toJsonString() {
		return FastJsonUtil.toJSONString(this);
	}

}