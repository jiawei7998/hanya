/*
 *    Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import org.apache.commons.collections4.MapUtils;

import java.util.Map;

/**
 * represents one of potentially many classes in a cron job. serves as a data
 * holder for information that will be needed when executing the specific class.
 * 
 * @author Jarrett Taylor
 */
public class CronClassImpl implements CronClass {
	/**
	 * the class name (including package) represented by this object
	 */
	private String className;
	/**
	 * true if the next class is allowed to execute in the event this one fails
	 */
	private boolean continuable;
	/**
	 * key/value pairs available to the class at runtime
	 */
	private Map<String,Object> parameters;

	/**
	 * creates a new instance of this class
	 */
	public CronClassImpl(String inClassName, boolean inContinuable,
			Map<String,Object> inParameters) {
		className = inClassName;
		continuable = inContinuable;
		parameters = inParameters;
	} // constructor

	/**
	 * returns the className
	 * 
	 * @return className
	 */
	@Override
	public String getClassName() {
		return className;
	} // getClassName

	/**
	 * true if the next class is allowed to execute in the event this one fails
	 * 
	 * @return continuable
	 */
	@Override
	public boolean getContinuable() {
		return continuable;
	} // getContinuable

	/**
	 * the key/value pairs specified with this cron entry
	 * 
	 * @return parameters
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> getParameters() {
		if (parameters == null) {
			return null;}
		return MapUtils.unmodifiableMap(parameters);
	} // getParameters

} // class
