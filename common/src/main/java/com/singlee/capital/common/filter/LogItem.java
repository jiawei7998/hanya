package com.singlee.capital.common.filter;

public class LogItem {

	private String ip;
	private String reqTime;
	private String rspTime;
	private long spendTime;
	private String userId;
	private String reqUrl;
	private String reqContentType;
	private String reqPayload;
	private String rspCode;
	private String rspPayLoad;
	
	
	public String getReqContentType() {
		return reqContentType;
	}
	public void setReqContentType(String reqContentType) {
		this.reqContentType = reqContentType;
	}
	public String getReqUrl() {
		return reqUrl;
	}
	public void setReqUrl(String reqUrl) {
		this.reqUrl = reqUrl;
	}
	public long getSpendTime() {
		return spendTime;
	}
	public void setSpendTime(long spendTime) {
		this.spendTime = spendTime;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getReqTime() {
		return reqTime;
	}
	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}
	public String getRspTime() {
		return rspTime;
	}
	public void setRspTime(String rspTime) {
		this.rspTime = rspTime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getReqPayload() {
		return reqPayload;
	}
	public void setReqPayload(String reqPayload) {
		this.reqPayload = reqPayload;
	}
	public String getRspCode() {
		return rspCode;
	}
	public void setRspCode(String rspCode) {
		this.rspCode = rspCode;
	}
	public String getRspPayLoad() {
		return rspPayLoad;
	}
	public void setRspPayLoad(String rspPayLoad) {
		this.rspPayLoad = rspPayLoad;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LogItem [ip=");
		builder.append(ip);
		builder.append(", reqTime=");
		builder.append(reqTime);
		builder.append(", rspTime=");
		builder.append(rspTime);
		builder.append(", spendTime=");
		builder.append(spendTime);
		builder.append(", reqUrl=");
		builder.append(reqUrl);
		builder.append(", userId=");
		builder.append(userId);
		builder.append(", reqContentType=");
		builder.append(reqContentType);
		builder.append(", reqPayload=");
		builder.append(reqPayload);
		builder.append(", rspCode=");
		builder.append(rspCode);
		builder.append(", rspPayLoad=");
		builder.append(rspPayLoad);
		builder.append("]");
		return builder.toString();
	}
	public String toReqString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LogItem [ip=");
		builder.append(ip);
		builder.append(", reqTime=");
		builder.append(reqTime);
		builder.append(", reqUrl=");
		builder.append(reqUrl);
		builder.append(", reqContentType=");
		builder.append(reqContentType);
		builder.append(", reqPayload=");
		builder.append(reqPayload);
		builder.append("]");
		return builder.toString();
	}
	public String toRespString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LogItem [rspTime=");
		builder.append(rspTime);
		builder.append(", spendTime=");
		builder.append(spendTime);
		builder.append(", userId=");
		builder.append(userId);
		builder.append(", rspCode=");
		builder.append(rspCode);
		builder.append(", rspPayLoad=");
		builder.append(rspPayLoad);
		builder.append("]");
		return builder.toString();
	}
	
}
