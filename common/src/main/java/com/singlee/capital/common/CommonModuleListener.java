package com.singlee.capital.common;

import com.singlee.capital.common.spring.ReleaseResourceInitListener;
import org.springframework.stereotype.Service;


@Service
public class CommonModuleListener extends ReleaseResourceInitListener{

	@Override
	protected String jarFilePath() {
		return CommonModuleListener.class.getResource("").getPath();
	}

	@Override
	protected String jarVersion() {
		return "1.0.0.1";
	}
	
}
