package com.singlee.capital.common.flyway;

import com.singlee.capital.common.util.JY;
import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.util.Location;

import javax.sql.DataSource;
import java.net.URL;
public class FlywayMigration {
	private DataSource dataSource;
	// 设置存放flyway metadata数据的表名
	private String flywaySchemaMetadata;
	// 设置flyway扫描sql升级脚本、java升级脚本的目录路径或包路径
	private String locations;
	// 设置sql脚本文件的编码
	private String encoding;
	// 是否建立基线初始化
	private boolean baselineOnMigrate;
	// 是否迁移Y迁移N迁移
	private String whetherMigrate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setFlywaySchemaMetadata(String flywaySchemaMetadata) {
		this.flywaySchemaMetadata = flywaySchemaMetadata;
	}

	public void setLocations(String locations) {
		URL weburl = FlywayMigration.class.getClassLoader().getResource("WEB-INF/classes");
		URL url = FlywayMigration.class.getClassLoader().getResource("");
		String path = Location.FILESYSTEM_PREFIX + (null == weburl ? url.getPath() : weburl.getPath());
		this.locations = path + locations;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public void setBaselineOnMigrate(boolean baselineOnMigrate) {
		this.baselineOnMigrate = baselineOnMigrate;
	}

	public String getWhetherMigrate() {
		return whetherMigrate;
	}

	public void setWhetherMigrate(String whetherMigrate) {
		this.whetherMigrate = whetherMigrate;
	}

	public void migrate() {
		Flyway flyway = new Flyway();
		flyway.setDataSource(dataSource);
		flyway.setTable(flywaySchemaMetadata);
		flyway.setLocations(locations);
		flyway.setEncoding(encoding);
		flyway.setBaselineOnMigrate(baselineOnMigrate);
		flyway.setCleanDisabled(true);
		if (StringUtils.equals(whetherMigrate, "Y")) {
			JY.info("系统开始初始化脚本处理······");
			flyway.migrate();
		} else {
			JY.info("系统忽略初始化脚本处，开始加载资源······");
		}

	}
}
