/*
 *    Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import java.util.Map;

/**
 * Any class that should be run by Cron must implement CronRunnable.
 * CronRunnable is not a very creative name, but Executable seemed to imply too
 * much. execute() returns true/false representing the success/failure of the
 * action. terminate() is called when the class is asked to exit prematurely
 * (i.e. reload or shutdown). It is suggested that the implementing class use
 * the terminate method to manipulate a local variabe that is checked frequently
 * in loops or operations that may take a long time. This should give the class
 * the possibly of shutting down cleanly.
 * 
 * @author Jarrett Taylor
 */
public interface CronRunnable {
	/**
	 * execute returns true/false representing the success/failure of the
	 * action. "parameters" contains the key/value pairs specified for the class
	 * in the settings file (for the particular job and class).
	 * 
	 * @param parameters
	 *            key/value pairs specific to the class and cron job
	 * @return true if the class complete successfully
	 * @throws Exception
	 *             oops...you broke it
	 */
	public boolean execute(Map<String,Object> parameters) throws Exception;

	/**
	 * requests that the class cease executing
	 */
	public void terminate();
} // interface
