package com.singlee.capital.common.spring.mybatis;

import com.singlee.capital.common.exception.RException;
import com.singlee.capital.common.util.JY;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * 
 * 批量操作 batch 
 * @author x230i
 *
 */

@Repository("batchDao")
public class BatchDao  {

	private static final int defaultDoFlushSize = 100;
	private SqlSessionTemplate sqlSessionTemplate ;
	
	@Autowired
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
		sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
	}

	// 批量插入
	public <T> void batch(String sqlId, List<T> objectList, int doFlushSize) {
		if(objectList==null){
			return;
		}
		if (doFlushSize <= 0) {
			doFlushSize = defaultDoFlushSize;
		}
		// 从当前环境中根据connection生成批量提交的sqlSession
		SqlSession sqlSession = this.sqlSessionTemplate.getSqlSessionFactory()
				.openSession(ExecutorType.BATCH, false);
		int size = objectList.size();
		int commit = 0;
		JY.info(sqlId + " total:"+size+"--> prepare batch percent "+ doFlushSize );
		try {
			int count = 0;
			for (int i = 0; i < size; i++) {
				sqlSession.insert(sqlId, objectList.get(i));
				count ++;
				commit ++;
				if ((i+1) % doFlushSize == 0 || i+1 == size) {
					// 手动每doFlushSize个一提交，提交后无法回滚
					sqlSession.commit();
					// 清理缓存，防止溢出
					sqlSession.clearCache();
					JY.info(sqlId + " --> commit "+ count);
					count = 0;
				}
			}
		} catch (Exception e) {
			JY.info("batch-%s操作出现错误需要回滚，但已经提交了部分 %d/%d \r\n %s", sqlId, commit, size, ExceptionUtils.getRootCauseMessage(e));
			// 没有提交的数据可以回滚
			sqlSession.rollback();
		} finally {
			sqlSession.close();
		}
	}

	public <T> void batch(String sqlId, List<T> objectList) {
		if(objectList==null){
			return;
		}
		// 从当前环境中根据connection生成批量提交的sqlSession
		SqlSession sqlSession = this.sqlSessionTemplate.getSqlSessionFactory()
				.openSession(ExecutorType.BATCH, false);
		int size = objectList.size();
		JY.info(sqlId + " --> prepare batch "+ size);
		try {
			for (int i = 0; i < size; i++) {
				sqlSession.insert(sqlId, objectList.get(i));
			}
			// 手动每doFlushSize个一提交，提交后无法回滚
			sqlSession.commit();
			// 清理缓存，防止溢出
			sqlSession.clearCache();
			JY.info(sqlId + " --> commit "+ size);
		} catch (Exception e) {
			JY.warn("batch "+sqlId+"操作出现错误需要回滚", e);
			// 没有提交的数据可以回滚
			sqlSession.rollback();
			throw new RException(e);
		} finally {
			sqlSession.close();
		}
	}
	public <T> void batch(String sqlId, Set<T> objectList) {
		if(objectList==null){
			return;
		}
		// 从当前环境中根据connection生成批量提交的sqlSession
		SqlSession sqlSession = this.sqlSessionTemplate.getSqlSessionFactory()
				.openSession(ExecutorType.BATCH, false);
		int size = objectList.size();
		JY.info(sqlId + " --> prepare batch "+ size);
		try {
			Iterator<T> iter = objectList.iterator();
			while(iter.hasNext()){
				sqlSession.insert(sqlId, iter.next());
			}
			// 手动每doFlushSize个一提交，提交后无法回滚
			sqlSession.commit();
			// 清理缓存，防止溢出
			sqlSession.clearCache();
			JY.info(sqlId + " --> commit "+ size);
		} catch (Exception e) {
			JY.warn("batch "+sqlId+"操作出现错误需要回滚", e);
			// 没有提交的数据可以回滚
			sqlSession.rollback();
			throw new RException(e);
		} finally {
			sqlSession.close();
		}
	}
}