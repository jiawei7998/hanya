package com.singlee.capital.common.util;

//import java.io.UnsupportedEncodingException;

import com.singlee.capital.common.exception.CException;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;


public class HttpParameterParse {

	private static String encoding = "utf-8";

	public static String getRequestURLWithQueryStr(HttpServletRequest req) {
		String URL = null;

		try {
			URL = req.getRequestURL().toString();
			if (req.getQueryString() != null) {
				URL += '?' + req.getQueryString();}
			return java.net.URLEncoder.encode(URL, encoding);
		} catch (Exception e) {
			return URL;
		}
	}

	// private static final String DefaultCoding = "8859_1";
	public static String[] getStringArrayParameter(ServletRequest req, String name, String[] def) {
		try {
			return getStringArrayParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static String generateURLStringParameter(String s) {
		try {
			return URLEncoder.encode(s, encoding);
		} catch (Exception e) {
			return s;
		}
	}

	public static String getURLStringParameter(ServletRequest req, String name) throws CException  {
		String s = getStringParameter(req, name);
		try {
			return URLDecoder.decode(s, encoding);
		} catch (Exception e) {
			return s;
		}
	}

	public static String getURLStringParameter(ServletRequest req, String name, String def)  throws CException {
		try {
			String s = getStringParameter(req, name);
			return URLDecoder.decode(s, encoding);
		} catch (Exception e) {
			return def;
		}
	}

	public static String[] getStringArrayParameter(ServletRequest req, String name) throws CException {
		String[] values = req.getParameterValues(name);
		if (values == null) {
			throw new CException(name + " not found");
		}
		return values;
	}

	public static String getStringParameter(ServletRequest req, String name) throws CException {
		String[] values = req.getParameterValues(name);
		if (values == null) {
			throw new CException(name + " not found");
		} else if (values[0].length() == 0) {
			throw new CException(name + " was empty");
		} else {
			return values[0];
		}
	}

	public static String getStringParameter(ServletRequest req, String name, String def) {
		try {
			return getStringParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static boolean getBooleanParameter(ServletRequest req, String name) throws CException, NumberFormatException {
		String value = getStringParameter(req, name).toLowerCase();
		if (("true".equalsIgnoreCase(value)) || ("on".equalsIgnoreCase(value)) || ("yes".equalsIgnoreCase(value))) {
			return true;
		} else if (("false".equalsIgnoreCase(value)) || ("off".equalsIgnoreCase(value)) || ("no".equalsIgnoreCase(value))) {
			return false;
		} else {
			throw new NumberFormatException("Parameter " + name + " value " + value + " is not a boolean");
		}
	}

	public static boolean getBooleanParameter(ServletRequest req, String name, boolean def) {
		try {
			return getBooleanParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static boolean[] getBooleanArrayParameter(ServletRequest req, String name) throws CException {
		String[] values = getStringArrayParameter(req, name);
		boolean[] ret = new boolean[values.length];
		for (int i = 0; i < values.length; i++) {
			String value = values[i].toLowerCase();
			if (("true".equalsIgnoreCase(value)) || ("on".equalsIgnoreCase(value)) || ("yes".equalsIgnoreCase(value))) {
				ret[i] = true;
			} else if (("false".equalsIgnoreCase(value)) || ("off".equalsIgnoreCase(value)) || ("no".equalsIgnoreCase(value))) {
				ret[i] = false;
			} else {
				ret[i] = false;
			}
		}
		return ret;
	}

	public static byte getByteParameter(ServletRequest req, String name) throws CException, NumberFormatException {
		return Byte.parseByte(getStringParameter(req, name));
	}

	public static byte getByteParameter(ServletRequest req, String name, byte def) {
		try {
			return getByteParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static char getCharParameter(ServletRequest req, String name) throws CException {
		String param = getStringParameter(req, name);
		if (param.length() == 0) {
			throw new CException(name + " is empty string");}
		else {
			return (param.charAt(0));}
	}

	public static char getCharParameter(ServletRequest req, String name, char def) {
		try {
			return getCharParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static double getDoubleParameter(ServletRequest req, String name) throws CException, NumberFormatException {
		return new Double(getStringParameter(req, name)).doubleValue();
	}

	public static double getDoubleParameter(ServletRequest req, String name, double def) {
		try {
			return getDoubleParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static float getFloatParameter(ServletRequest req, String name) throws CException, NumberFormatException {
		return new Float(getStringParameter(req, name)).floatValue();
	}

	public static float getFloatParameter(ServletRequest req, String name, float def) {
		try {
			return getFloatParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static float[] getFloatArrayParameter(ServletRequest req, String name) throws CException, NumberFormatException {

		String[] values = getStringArrayParameter(req, name);
		float[] ret = new float[values.length];
		for (int i = 0; i < values.length; i++) {
			ret[i] = Float.parseFloat(values[i]);
		}
		return ret;
	}

	public static float[] getFloatArrayParameter(ServletRequest req, String name, float[] def) throws NumberFormatException {

		String[] values;
		try {
			values = getStringArrayParameter(req, name);
		} catch (CException e) {
			return def;
		}
		float[] ret = new float[values.length];
		for (int i = 0; i < values.length; i++) {
			ret[i] = Float.parseFloat(values[i]);
		}
		return ret;
	}

	public static int getIntParameter(ServletRequest req, String name) throws CException, NumberFormatException {
		String ints = getStringParameter(req, name);
		ints = ints.replaceAll("０", "0");
		ints = ints.replaceAll("１", "1");
		ints = ints.replaceAll("２", "2");
		ints = ints.replaceAll("３", "3");
		ints = ints.replaceAll("４", "4");
		ints = ints.replaceAll("５", "5");
		ints = ints.replaceAll("６", "6");
		ints = ints.replaceAll("７", "7");
		ints = ints.replaceAll("８", "8");
		ints = ints.replaceAll("９", "9");
		return Integer.parseInt(ints);
	}

	public static int getIntParameter(ServletRequest req, String name, int def) {
		try {
			return getIntParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static int[] getIntArrayParameter(ServletRequest req, String name) throws CException {
		String[] values = getStringArrayParameter(req, name);
		int[] ret = new int[values.length];
		for (int i = 0; i < values.length; i++) {
			String ints = values[i];
			ints = ints.replaceAll("０", "0");
			ints = ints.replaceAll("１", "1");
			ints = ints.replaceAll("２", "2");
			ints = ints.replaceAll("３", "3");
			ints = ints.replaceAll("４", "4");
			ints = ints.replaceAll("５", "5");
			ints = ints.replaceAll("６", "6");
			ints = ints.replaceAll("７", "7");
			ints = ints.replaceAll("８", "8");
			ints = ints.replaceAll("９", "9");
			ret[i] = Integer.parseInt(ints);
		}
		return ret;
	}

	public static int[] getIntArrayParameter(ServletRequest req, String name, int[] def) {
		try {
			return getIntArrayParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static long getLongParameter(ServletRequest req, String name) throws CException, NumberFormatException {
		return Long.parseLong(getStringParameter(req, name));
	}

	public static long[] getLongArrayParameter(ServletRequest req, String name) throws CException {
		String[] values = getStringArrayParameter(req, name);
		long[] ret = new long[values.length];
		for (int i = 0; i < values.length; i++) {
			String ints = values[i];
			ints = ints.replaceAll("０", "0");
			ints = ints.replaceAll("１", "1");
			ints = ints.replaceAll("２", "2");
			ints = ints.replaceAll("３", "3");
			ints = ints.replaceAll("４", "4");
			ints = ints.replaceAll("５", "5");
			ints = ints.replaceAll("６", "6");
			ints = ints.replaceAll("７", "7");
			ints = ints.replaceAll("８", "8");
			ints = ints.replaceAll("９", "9");
			ret[i] = Long.parseLong(ints);
		}
		return ret;
	}

	public static long[] getLongArrayParameter(ServletRequest req, String name, long[] def) {
		try {
			return getLongArrayParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static long getLongParameter(ServletRequest req, String name, long def) {
		try {
			return getLongParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}

	public static short getShortParameter(ServletRequest req, String name) throws CException, NumberFormatException {
		return Short.parseShort(getStringParameter(req, name));
	}

	public static short getShortParameter(ServletRequest req, String name, short def) {
		try {
			return getShortParameter(req, name);
		} catch (Exception e) {
			return def;
		}
	}
	/**
	 * 读取request中的参数,转换为map对象
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String,Object> requestParamToMap(HttpServletRequest request) {
		Map<String,Object> parameters = new HashMap<String,Object>();
		Enumeration<String> Enumeration = request.getParameterNames();
		while (Enumeration.hasMoreElements()) {
			String paramName = (String) Enumeration.nextElement();
			String paramValue = request.getParameter(paramName);
			// 形成键值对应的map
			parameters.put(paramName, paramValue);
		}
		return parameters;
	}
//	public static String[] getMissingParameters(ServletRequest req, String[] required) {
//		Vector missing = new Vector();
//		for (int i = 0; i < required.length; i++) {
//			String val = getStringParameter(req, required[i], null);
//			if (val == null) {
//				missing.addElement(required[i]);
//			}
//		}
//		if (missing.size() == 0) {
//			return null;
//		} else {
//			String[] ret = new String[missing.size()];
//			missing.copyInto(ret);
//			return ret;
//		}
//	}
}
