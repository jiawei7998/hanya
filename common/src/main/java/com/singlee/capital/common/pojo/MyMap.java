package com.singlee.capital.common.pojo;

import java.util.HashMap;

/**
 * 针对 那些 遗留需要小写key的的 map 的特殊实例 
 * @author x230i
 *
 */
public class MyMap extends HashMap<String, Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
