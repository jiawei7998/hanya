/*
 *    Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import com.singlee.capital.common.session.SlSession;
import com.singlee.capital.common.util.JY;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * represents a single cron job.
 * 
 * @author Jarrett Taylor
 */
public abstract class AbstractCronJobImpl implements CronJob, Runnable, Comparable<CronJob> {
	/**
	 * Logging facility for CronJob
	 */
	//private Log log = LogFactory.getLog(CronJobImpl.class);
	protected static final Logger log = LoggerFactory.getLogger("CronJobImpl");
	/**
	 * the id associated with the job
	 */
	protected String jobId;
	/**
	 * the name associated with the job
	 */
	protected String jobName;
	/**
	 * the schedule associated with the job
	 */
	protected String schedule;
	/**
	 * true if this job should be run at starup
	 */
	protected boolean startup;
	/**
	 * list of cronClass objects to be executed by this job
	 */
	protected List<? extends CronClass> cronClasses;
	/**
	 * true when the job is running
	 */
	protected boolean running;
	/**
	 * true if terminate has been called. can be set to false by calling
	 * reset().
	 */
	protected boolean killed;
	/**
	 * true if disable has been called. can be set to false by calling enable().
	 */
	protected boolean disabled;
	/**
	 * used to keep a reference to the cron job thread
	 */
	protected Thread thread;
	/**
	 * used to keep a reference to the instance of the currently executing cron
	 * class
	 */
	protected CronRunnable executingClass;
	/**
	 * used to keep track =of the currently executing class name
	 */
	protected String executingClassName;

	/**
	 * session
	 */
	protected SlSession session;
	/**
	 * creates a new instance of this class.
	 */
	public AbstractCronJobImpl(String inJobId, String inJobName, String inSchedule,
			boolean inStartup, boolean inDisabled, List<? extends CronClass> inCronClasses) {
		this.jobId = inJobId;
		this.jobName = inJobName;
		this.schedule = inSchedule;
		this.startup = inStartup;
		this.disabled = inDisabled;
		this.cronClasses = inCronClasses;
		this.running = false;
		this.killed = false;
		this.thread = null;
		this.executingClass = null;
		this.executingClassName = null;
	} // constructor

	/**
	 * loops throught cronClasses and creates an instance the class specified,
	 * calling execute. If the call returns false, the class in considered to
	 * have failed and the "continuable" attribute of the next class entry is
	 * checked. If it is true, the cron job continues executing, otherwise it
	 * ends. In the event of an exception, no more classes will be executed.
	 */
	@Override
	public void run() {
		CronJobLog l = beforeRun();
		if (l.ok()) {
			JY.info(String.format("==> 任务执行处理逻辑开始，【任务编号:%s,任务名称:%s】", jobId,jobName));
			thread = Thread.currentThread();
			if (log.isDebugEnabled()) {
				log.debug("run() - job [" + jobName + "] started");}
			synchronized (this) {
				running = true;
			} // synch
			try {
				boolean shouldContinue = true;
				for (int x = 0; !killed && shouldContinue && x < cronClasses.size(); x++) {
					CronClass cronClass = (CronClass) cronClasses.get(x);
					executingClassName = cronClass.getClassName();
					if (log.isDebugEnabled()) {
						log.debug("run() - job [" + jobId + "/" + jobName + "] executing class: " + executingClassName);}
					executingClass = (CronRunnable) Class.forName( executingClassName).newInstance();
					shouldContinue = executingClass.execute(cronClass.getParameters());
					if (!shouldContinue) {
						shouldContinue = cronClass.getContinuable();}
				} // for
			} // try
			catch (Throwable e) {
				log.error("run() - job: [" + jobId + "/" + jobName + "] in class: [" + executingClassName + "]", e);
				errHandle(l, e.getMessage());
				
			} // catch
			synchronized (this) {
				running = false;
			} // synch
			executingClass = null;
			executingClassName = null;
			thread = null;
		}
		JY.info(String.format("==> 任务执行处理逻辑结束，【任务编号:%s,任务名称:%s】", jobId,jobName));
		afterRun(l);
	} // run

	protected abstract void errHandle(CronJobLog l, String err);
	
	protected abstract CronJobLog beforeRun();

	protected abstract void afterRun(CronJobLog l) ;

	@Override
	public void setSession(SlSession session) {
		this.session = session;
	}

	/**
	 * attempts to interrupt the current process
	 * 
	 * @return true if the process was running and has been stopped
	 */
	@Override
	public boolean terminate() {
		boolean returnVal = false;
		synchronized (this) {
			if (!killed) {
				if (thread != null) {
					if (executingClass != null) {
						executingClass.terminate();
						executingClass = null;
						executingClassName = null;
					} // if
				} // if
				killed = true;
				returnVal = true;
			} // if
		} // synch
		return returnVal;
	} // terminate

	/**
	 * sets the killed flag to false
	 */
	@Override
	public void reset() {
		synchronized (this) {
			killed = false;
		} // synch
	} // reset

	/**
	 * returns true if this job is disabled and should not be run
	 * 
	 * @return boolean true if this cron job is disabled
	 */
	@Override
	public boolean getDisabled() {
		return disabled;
	} // getDisabled

	/**
	 * sets the disabled flag to false
	 */
	@Override
	public void enable() {
		synchronized (this) {
			disabled = false;
			killed=false;
		} // synch
	} // enable

	/**
	 * sets the disabled flag to true
	 */
	@Override
	public void disable() {
		synchronized (this) {
			disabled = true;
			killed=true;
		} // synch
	} // disable

	/**
	 * returns true if the job is currently running
	 * 
	 * @return boolean true if the job is running
	 */
	@Override
	public boolean getRunning() {
		synchronized (this) {
			return running;
		} // synch
	} // getRunning

	/**
	 * returns the cron schedule
	 * 
	 * @return String representing the cron schedule
	 */
	@Override
	public String getSchedule() {
		return schedule;
	} // getSchedule

	/**
	 * returns the cron class name that us currently executing
	 * 
	 * @return String representing the cron class that us currently executing
	 */
	@Override
	public String getExecutingClassName() {
		if (getRunning()) {
			return executingClassName;}
		else {
			return null;}
	} // getExecutingClass

	/**
	 * returns the id of the job
	 * 
	 * @return String the id associated with this cron job
	 */
	@Override
	public String getJobId() {
		return jobId;
	} // getId

	/**
	 * returns the name of the job
	 * 
	 * @return String the name associated with this cron job
	 */
	@Override
	public String getJobName() {
		return jobName;
	} // getJobName

	/**
	 * returns true if this job should be run on startup
	 * 
	 * @return boolean true if this cron job runs on startup
	 */
	@Override
	public boolean getStartup() {
		return startup;
	} // getStartup

	/**
	 * returns next run date of this job by calling Utils.getNextRun(schedule,
	 * new Date())
	 * 
	 * @return Date of next run
	 */
	@Override
	public Date getNextRunDate() {
		return Utils.getNextRun(schedule, new Date());
	}

	/**
	 * returns the list of CronClass objects associated with this job
	 * 
	 * @return java.util.LinkedList of CronClass objects for this cron job
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<? extends CronClass> getCronClasses() {
		if (cronClasses == null) {
			return null;}
		return ListUtils.unmodifiableList(cronClasses);
	} // getCronClasses

	/**
	 * compares job id's
	 * 
	 * @param j
	 * @return the value 0 if the job id is lexicographically equal to this job
	 *         id; a value less than 0 if the job id is lexicographically
	 *         greater than this job id; and a value greater than 0 if the job
	 *         id is lexicographically less than this string.
	 */
	@Override
	public int compareTo(CronJob j) {
		if (j == null) {
			throw new RuntimeException("CronJob cannot be null.");}
		if (jobId == null && j.getJobId() == null) {
			return 0;}
		else if (jobId == null && j.getJobId() != null) {
			return -1;}
		else if (jobId != null && j.getJobId() == null) {
			return 1;}
		else {
			return jobId.compareTo(j.getJobId());}
	} // compareTo

	/**
	 * compares job id's
	 * 
	 * @param j
	 * @return true if the id's are equal
	 */
	public boolean equals(CronJob j) {
		if (j == null) {
			throw new RuntimeException("CronJob cannot be null.");}
		if (jobId == null && j.getJobId() == null) {
			return true;}
		else if (jobId == null && j.getJobId() != null) {
			return false;}
		else {
			return jobId.equals(j.getJobId());}
	}
	
	
} // class
