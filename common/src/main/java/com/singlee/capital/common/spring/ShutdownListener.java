package com.singlee.capital.common.spring;

import com.singlee.capital.common.util.JY;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

/**
 * 系统关闭 
 * 实现者应该 声明 @Service @Scope("singleton")
 * @author LyonChen
 * 
 */
public abstract class ShutdownListener implements ApplicationListener<ContextClosedEvent> {

	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		JY.info("..............................................");
		JY.info(sysname()+"系统shutdown工作开始");
		JY.info("..............................................");
		shutdown(event.getApplicationContext());
		JY.info("..............................................");
		JY.info(sysname()+"系统shutdown工作结束");
		JY.info("..............................................");
	}

	/**
	 * 
	 */
	abstract protected void shutdown(ApplicationContext ac)   ;
	abstract protected String sysname();
}
