package com.singlee.capital.common.spring;

import com.singlee.capital.common.util.JY;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;


/**
 * 系统初始化 动作， 可以再init方法中顺序增加
 * 实现者应该 声明 @Service @Scope("singleton")
 * 
 * @author LyonChen
 * 
 */
public abstract class InitListener implements ApplicationListener<ContextRefreshedEvent> {

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			JY.info("--------------------------------------------------------------");
			JY.info("··················"+sysname()+"初始化工作开始····················");
			// 需要执行的逻辑代码，当spring容器初始化完成后就会执行该方法。
			init(event.getApplicationContext());
			JY.info("··················"+sysname()+"初始化工作结束····················");
			JY.info("--------------------------------------------------------------");
		}
	}

	/**
	 * 
	 */
	protected abstract void init(ApplicationContext ac) ;
	protected abstract String sysname();

}
