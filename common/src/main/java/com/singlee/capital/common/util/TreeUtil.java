package com.singlee.capital.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;



/**
 * 父子关系 生成树状结构 
 * @author x230i
 *
 */
public class TreeUtil {
	
	/**
	 * 
	 * @param mergedList
	 * @param targetId
	 * @return
	 */
	public static final List<? extends TreeInterface> getChildrenList(List<? extends TreeInterface> mergedList, 
			String targetId) {
		for(TreeInterface ti : mergedList){
			if(ti.getId().equals(targetId)){
				return ti.children();
			}else{
				if(ti.children()!=null){
					List<? extends TreeInterface> ret = getChildrenList(ti.children(), targetId);
					if(ret != null ){
						return ret;
					}
				}
			}
		}
		return null;
		
	}
	
	/**
	 * 根据查询出的树结构生成TreeModuleBo对象
	 * 
	 * @param hashList
	 * @param parentTreeModule
	 * @return
	 */
	public static final  <I extends TreeInterface>
		List<I> mergeChildrenList(List<I> hashList, String rootId) {
		// 1.将数组构建为hashMap<String,List<TreeModuleBo>> 对象
		HashMap<String, List<I>> taModuleHashMap = new HashMap<String, List<I>>();
		for (I taModule : hashList) {
			String pID = taModule.getParentId();
			// 判断taModuleHashMap是否已存在pId的对象。存在时从hashmap中读取到list，再项list中添加数据
			List<I> list = taModuleHashMap.get(pID);
			if (list == null) {
				list = new ArrayList<I>();
				taModuleHashMap.put(pID, list);
			}
			list.add(taModule);
		}
		// 2.从String = 0 的数组开始迭代，读取hashMap中module_id = PId 的数据
		List<I> TaModuleRoots = taModuleHashMap.get(rootId);
		if (TaModuleRoots != null) {
			for (I treeModuleBo : TaModuleRoots) {
				SetTreeModuleBoChildren(taModuleHashMap, treeModuleBo);
			}
			
		}else{
			//取出hashList中的最顶级节点,组装成树
			HashMap<String, I> tempMap = new HashMap<String, I>();
			for (I i : hashList) 
			{
				tempMap.put(i.getId(), i);
			}
			TaModuleRoots = new ArrayList<I>();
			for (Entry<String, List<I>> e : taModuleHashMap.entrySet()) {
				//查询tempMap中存不存在 List<I>对应的父节点,如果不存在说明List<I>中的节点为hashList中的最顶级节点
				if(tempMap.get(e.getKey()) == null){//不存在父节点
					TaModuleRoots.addAll(e.getValue());
					for (I treeModuleBo : e.getValue()) 
					{
						SetTreeModuleBoChildren(taModuleHashMap, treeModuleBo);
					}
				}
			}
		}
		return TaModuleRoots;
	}

	/**
	 * 设置TreeModuleBo对象中children属性
	 * 
	 * @param taModuleHashMap
	 * @param treeModuleBo
	 */
	private static final <I extends TreeInterface> void  SetTreeModuleBoChildren(
			HashMap<String, List<I>> taModuleHashMap, I treeModuleBo) {
		String id = treeModuleBo.getId();
		if (taModuleHashMap.get(id) != null) {
			List<I> list = taModuleHashMap.get(id);
			for (I treeModuleBo2 : list) {
				SetTreeModuleBoChildren(taModuleHashMap, treeModuleBo2);
			}
			treeModuleBo.children(list);
		}
	}

}
