package com.singlee.capital.common.spring.webmvc;

import org.springframework.web.multipart.MultipartFile;


/**
 * 处理 文件上传 的接口 
 * @author Lyon Chen
 *
 */
public interface HandlerMulipartFile {

	/**
	 * 处理好的结果，返回一个 字符串
	 * @param multipartFile 
	 * @return 
	 */
	public String process(MultipartFile multipartFile);

}
