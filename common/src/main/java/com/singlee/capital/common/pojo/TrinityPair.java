package com.singlee.capital.common.pojo;

import java.io.Serializable;

/**
 * 三子
 * 
 * @author LyonChen
 */
public class TrinityPair<C1 extends Serializable, C2 extends Serializable, C3 extends Serializable> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private C1 c1;
	private C2 c2;
	private C3 c3;

	public TrinityPair() {
	}

	public void setC1(C1 c1) {
		this.c1 = c1;
	}

	public void setC2(C2 c2) {
		this.c2 = c2;
	}

	public TrinityPair(C1 c1, C2 c2, C3 c3) {
		this.c1 = c1;
		this.c2 = c2;
		this.c3 = c3;
	}

	public C1 getC1() {
		return c1;
	}

	public C2 getC2() {
		return c2;
	}

	public C3 getC3() {
		return c3;
	}

	public void setC3(C3 c3) {
		this.c3 = c3;
	}

	@Override
	public String toString() {
		return String.format("TrinityPair [c1=%s, c2=%s, c3=%s]", c1, c2, c3);
	}

}
