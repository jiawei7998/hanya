/*
 *    Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import com.singlee.capital.common.session.SlSession;

import java.util.Date;
import java.util.List;

/**
 * Inteface allowing access to information about individual cron jobs.
 * 
 * @author Jarrett Taylor
 */
public interface CronJob extends Runnable {
	/**
	 * returns true if this job is disabled and should not be run
	 * 
	 * @return boolean true if this cron job is disabled
	 */
	public boolean getDisabled();

	/**
	 * returns the cron schedule
	 * 
	 * @return String representing the cron schedule
	 */
	public String getSchedule();

	/**
	 * returns the id of the job
	 * 
	 * @return String the id associated with this cron job
	 */
	public String getJobId();

	/**
	 * returns the name of the job
	 * 
	 * @return String the name associated with this cron job
	 */
	public String getJobName();

	/**
	 * returns true if this job should be run on startup
	 * 
	 * @return boolean true if this cron job runs on startup
	 */
	public boolean getRunning();

	/**
	 * returns true if this job is running
	 * 
	 * @return boolean true if this cron job is running
	 */
	public boolean getStartup();

	/**
	 * convenience method to return next run date/time for this job. the date
	 * should be computed, not stored.
	 * 
	 * @return Date of next run
	 */
	public Date getNextRunDate();

	/**
	 * returns the cron class name that is currently executing
	 * 
	 * @return String representing the cron class that is currently executing
	 */
	public String getExecutingClassName();

	/**
	 * returns the list of CronClass objects associated with this job
	 * 
	 * @return a list of CronClass objects for this cron job
	 */
	public List<? extends CronClass> getCronClasses();

	public void enable();

	public void disable();

	public boolean terminate();

	public void reset();

	//传入Session
	public void setSession(SlSession session);
	
} // interface
