package com.singlee.capital.common.pojo;

import org.apache.commons.lang3.StringUtils;

/**
 * 上传文件对象
 * @author LyonChen
 *
 */
public class UploadedFile {
	private byte[] bytes;
	private String fullname;
	
	public UploadedFile() {
	}
	
	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public int getLength() {
		return bytes.length;
	}
	public byte[] getBytes() {
		return bytes;
	}
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	public String getName() {
		String file_full_name = getFullname();
		String file_name = StringUtils.isEmpty(file_full_name)?file_full_name:file_full_name.substring(0, file_full_name.length()-getType().length()-1);	
		return file_name;
	}
	public String getType() {
		String[] strs = getFullname().split("\\.");
		return strs[strs.length-1];
	}
	//此方法用于上传后文件名解析 如：上传后文件为xxxx20130722131557.xls解析后为xxxx.xls
	public String getBeforeName(){
		String name = getName();
		String before_name = name.substring(0,name.length()-14);
		return before_name + "." + getType();
	}

}
