package com.singlee.capital.common.session;

/**
 * 登录 的监听器
 * @author x230i
 *
 */
public interface LoginListener {

	void loginOk(SlSession session);

}
