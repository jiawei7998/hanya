/*
 * spring-mvc-logger logs requests/responses
 *
 * Copyright (c) 2013. Israel Zalmanov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.singlee.capital.common.filter;

import com.singlee.capital.common.util.DateTimeUtil;
import com.singlee.capital.common.util.JY;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.atomic.AtomicLong;


public class LoggingFilter extends OncePerRequestFilter {

	private static final String REQUEST_PREFIX = "LoggingFilter---请求-->";
	private static final String RESPONSE_PREFIX = "LoggingFilter---响应-->";
	private static final String[] LOG_CONTENT_TYPE = new String[] { "application/json" };
	private AtomicLong id = new AtomicLong(1);

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		boolean needLog = false;
		String contentType = request.getContentType();
		if (StringUtils.isBlank(contentType )) {
			needLog = false;
		} else {
			for (String s : LOG_CONTENT_TYPE) {
				if (contentType.indexOf(s) >= 0) {
					needLog =  true;
					break;
				}
			}
		}
		LogItem li = null;
		if (JY.isLogDebugEnabled() && needLog) {
			li = new LogItem();
			long requestId = id.incrementAndGet();
			request = new RequestWrapper(requestId, request);
			response = new ResponseWrapper(requestId, response);
		}
		long time = 0;
		try {
			if (JY.isLogDebugEnabled() && needLog) {
				logRequest(request, li);
			}
			time = System.currentTimeMillis();
			filterChain.doFilter(request, response);
			time = System.currentTimeMillis() - time;
		} finally {
			if (JY.isLogDebugEnabled() && needLog) {
				logResponse((ResponseWrapper) response, time, li);
			}
			if(li!=null){
				handle(li);
			}
			
		}

	}

	protected void handle(LogItem li) {
		
	}

	private void logRequest(HttpServletRequest request, LogItem li) throws UnsupportedEncodingException {
		String uri = request.getRequestURI();
		String qryString = request.getQueryString();
		if (StringUtils.isNotBlank(qryString)) {
			uri += "?" + qryString;
		}
		String ip = request.getRemoteAddr();
		String ct = request.getContentType();
		String reqPayLoad = null;
		if (request instanceof RequestWrapper && !isMultipart(request)) {
			RequestWrapper requestWrapper = (RequestWrapper) request;
			String charEncoding = requestWrapper.getCharacterEncoding() != null ? requestWrapper
					.getCharacterEncoding() : "UTF-8";
					reqPayLoad = new String(requestWrapper.toByteArray(),
							charEncoding);
		}
		if(li!=null){
			li.setIp(ip);
			li.setReqUrl(uri);
			li.setReqContentType(ct);
			li.setReqPayload(reqPayLoad);
			li.setReqTime(DateTimeUtil.getLocalDateTime());
		}
		if (request instanceof RequestWrapper) {
			StringBuilder msg = new StringBuilder();
			msg.append(REQUEST_PREFIX);
			msg.append("ReqId[").append(((RequestWrapper) request).getId())
					.append("] ").append(li.toReqString());
			JY.debug(msg.toString());

		}
	}

	private boolean isMultipart(HttpServletRequest request) {
		return request.getContentType() != null
				&& request.getContentType().startsWith("multipart/form-data");
	}

	private void logResponse(ResponseWrapper response, long time, LogItem li) throws UnsupportedEncodingException {
		
		String charEncoding = response.getCharacterEncoding() != null ? response
				.getCharacterEncoding() : "UTF-8";
		String rspPayLoad = new String(response.toByteArray(), charEncoding);
		if(li!=null){
			li.setRspPayLoad(rspPayLoad);
			li.setSpendTime(time);
			li.setRspTime(DateTimeUtil.getLocalDateTime());
		}
		if (response instanceof ResponseWrapper) {
			StringBuilder msg = new StringBuilder();
			msg.append(RESPONSE_PREFIX);
			msg.append("ReqId[").append((response.getId())).append(" - ")
					.append(time).append("] ").append(li.toRespString());
			JY.debug(msg.toString());
		}
		
	}

}
