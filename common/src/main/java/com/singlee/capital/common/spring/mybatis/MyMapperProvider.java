package com.singlee.capital.common.spring.mybatis;

import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;
import tk.mybatis.mapper.mapperhelper.SqlHelper;

import java.util.Set;



public class MyMapperProvider extends MapperTemplate {

    public MyMapperProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

	
	public String select2(MappedStatement ms) {
	     final Class<?> entityClass = getEntityClass(ms);
        //修改返回值类型为实体类型
        setResultType(ms, entityClass);
        StringBuilder sql = new StringBuilder();
        sql.append(SqlHelper.selectAllColumns(entityClass));
        sql.append(SqlHelper.fromTable(entityClass, tableName(entityClass)));
        SqlHelper.whereAllIfColumns(entityClass, isNotEmpty());
        sql.append(SqlHelper.orderByDefault(entityClass));
        return sql.toString();
	}

    /**
     * 批量插入
     *
     * @param ms
     */
    public String insertListWithId(MappedStatement ms) {
        final Class<?> entityClass = getEntityClass(ms);
        //开始拼sql
        StringBuilder sql = new StringBuilder();
        sql.append(SqlHelper.insertIntoTable(entityClass, tableName(entityClass)));
        sql.append(SqlHelper.insertColumns(entityClass, false, false, false));
        sql.append(" VALUES ");
        sql.append("<foreach collection=\"list\" item=\"record\" separator=\" \" >");
        sql.append("<trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">");
        //获取全部列
        Set<EntityColumn> columnList = EntityHelper.getColumns(entityClass);
        for (EntityColumn column : columnList) {
            sql.append(column.getColumnHolder("record") + ",");
        }
        sql.append("</trim>");
        sql.append("</foreach>");
        return sql.toString();
    }


    /**
     * update ，根据Map
     *
     * @param ms
     */
    public String updateByMap(MappedStatement ms) {
        final  Class<?> entityClass = getEntityClass(ms);
        StringBuilder sql = new StringBuilder();
        sql.append(SqlHelper.updateTable(entityClass, tableName(entityClass)));
        sql.append("<set>");
        Set<EntityColumn> columnList = EntityHelper.getColumns(entityClass);
        for (EntityColumn column : columnList) {
            if (!column.isId() && column.isUpdatable()) {
            	sql.append("<if test=\"");
                sql.append(column.getProperty()).append(" != null");
                sql.append("\">");
                sql.append(column.getColumn()+"=#{"+column.getProperty()+"},");
                sql.append("</if>");
            }
        }
        sql.append("</set>");       
        
        sql.append("<where>");
        Set<EntityColumn> columnPKList = EntityHelper.getPKColumns(entityClass);
        for (EntityColumn column : columnPKList) {
        	// 防止 主键 属性没有的时候，update了所有的数据！！！！！！！
        	sql.append("<if test=\"");
            sql.append(column.getProperty()).append(" == null");
            sql.append("\">");
            sql.append(entityClass.getName() + " @ID [" + column.getProperty() + "] must not be null!!");
            sql.append("</if>");
            sql.append("<if test=\"");
            sql.append(column.getProperty()).append(" != null");
            sql.append("\">");
            sql.append(" AND " + column.getColumnEqualsHolder());
            sql.append("</if>");
        }
        sql.append("</where>");
        return sql.toString();
    }

}
