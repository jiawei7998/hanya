package com.singlee.capital.common.spring.mybatis;

import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;
import tk.mybatis.mapper.mapperhelper.SqlHelper;



public class SelectMapMapperProvider extends MapperTemplate {

    public SelectMapMapperProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

	
	public String select2(MappedStatement ms) {
	     final Class<?> entityClass = getEntityClass(ms);
        //修改返回值类型为实体类型
        setResultType(ms, entityClass);
        StringBuilder sql = new StringBuilder();
        sql.append(SqlHelper.selectAllColumns(entityClass));
        sql.append(SqlHelper.fromTable(entityClass, tableName(entityClass)));
        SqlHelper.whereAllIfColumns(entityClass, isNotEmpty());
        sql.append(SqlHelper.orderByDefault(entityClass));
        return sql.toString();
	}
}
