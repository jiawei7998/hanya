package com.singlee.capital.common.pojo;

import javax.persistence.Transient;



public class BaseDomain {

	//审批状态
	@Transient
	private String approveStatus;

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	
}
