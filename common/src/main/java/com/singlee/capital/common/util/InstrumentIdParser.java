package com.singlee.capital.common.util;

import com.singlee.capital.common.pojo.InstrumentId;

import java.util.regex.Matcher;
import java.util.regex.Pattern;




/**
 * 金融工具编号解析工具
 * @author LyonChen
 *
 */
public class InstrumentIdParser {
	/**
	 * 解析 i_code,a_type,m_type[,ver]
	 */
	private static final Pattern instrumentIdPattern=Pattern.compile("((\\w|\\W|\\)|\\(){1,32}),(\\w{1,20}),(\\w{1,20})(,(\\d{1,3}))?");
	
	public static final InstrumentId parseId(String id){
		Matcher match = instrumentIdPattern.matcher(id);
		if(match.find()){
			InstrumentId instId = new InstrumentId();
			instId.setI_code(match.group(1));
			instId.setA_type(match.group(3));
			instId.setM_type(match.group(4));
			instId.setVerNum(match.group(5));	
			return instId;
		}else{
			return null;
		}		
	}
	
	public static final void parseId(String id,InstrumentId instId){
		Matcher match = instrumentIdPattern.matcher(id);
		if(match.find()){
			instId.setI_code(match.group(1));
			instId.setA_type(match.group(3));
			instId.setM_type(match.group(4));
			instId.setVerNum(match.group(5));	
		}
	}
	public static final String generateId(String i, String a, String m){
		return generateId(i,a,m,-1);
	}
	public static final String generateId(String i, String a, String m, int v){
		if(v==-1){
			return String.format("%s,%s,%s",i,a,m);
		}else{
			return String.format("%s,%s,%s,%d",i,a,m,v);
		}
	}
}
