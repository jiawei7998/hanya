package com.singlee.capital.common.pojo;

import com.singlee.capital.common.spring.json.FastJsonUtil;

import java.util.LinkedList;
import java.util.List;

/**
 * data = {result:{files:[{thumbnailUrl:"123", url:"http://", name: "文件1",
 * size:"12345", delUrl: "http://////"}, {thumbnailUrl:"123", url:"http://",
 * name: "文件1", size:"12345", delUrl: "http://////"}] } };
 * 
 * @author x230i
 * 
 */
public class JQueryFileUploadResult {
	public static void main(String[] arg) {
		JQueryFileUploadResult j = new JQueryFileUploadResult();
		j.addFile("1", "123", "htt[://", "name", 100, "httpd;alf", "dt", "dwc", "aa", "bb", "SZ");
		j.addFile("1", "123", "htt[://", "name", 100, "httpd;alf", "dt", "dwc", "aa", "bb", "SZ01");
		System.out.println(FastJsonUtil.toJSONString(j));
	}

	private List<File> files;
	
	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}

	protected void addFileResult(File arg0) {
		if (this.files == null) {
			this.files = new LinkedList<File>();
		}
		this.files.add(arg0);
	}

	public void addFile(String id, String tu, String u, String name,
			int size, String du, String dt, String dwc, String ct, String cui, String cun) {
		File file = new File();
		file.setId(id);
		file.setThumbnailUrl(tu);
		file.setUrl(u);
		file.setName(name);
		file.setSize(size);
		file.setDeleteUrl(du);
		file.setDeleteType(dt);
		file.setDeleteWithCredentials(dwc);
		file.setCreateTime(ct);
		file.setCreateUserId(cui);
		file.setCreateUserName(cun);
		addFileResult(file);
	}
	
	public void addError(String e) {
		File file = new File();
		file.setError(e);
		addFileResult(file);
	}
	
	class File {
		private String id;
		private String thumbnailUrl;
		private String url;
		private String name;
		int size;
		private String deleteUrl;
		private String deleteType;
		private String deleteWithCredentials;
		private String createTime;
		private String createUserId;
		private String createUserName;
		private String error;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getThumbnailUrl() {
			return thumbnailUrl;
		}

		public void setThumbnailUrl(String thumbnailUrl) {
			this.thumbnailUrl = thumbnailUrl;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getSize() {
			return size;
		}

		public void setSize(int size) {
			this.size = size;
		}

		public String getDeleteUrl() {
			return deleteUrl;
		}

		public void setDeleteUrl(String deleteUrl) {
			this.deleteUrl = deleteUrl;
		}

		public String getDeleteType() {
			return deleteType;
		}

		public void setDeleteType(String deleteType) {
			this.deleteType = deleteType;
		}

		public String getDeleteWithCredentials() {
			return deleteWithCredentials;
		}

		public void setDeleteWithCredentials(String deleteWithCredentials) {
			this.deleteWithCredentials = deleteWithCredentials;
		}

		public String getCreateTime() {
			return createTime;
		}

		public void setCreateTime(String createTime) {
			this.createTime = createTime;
		}

		public String getCreateUserId() {
			return createUserId;
		}

		public void setCreateUserId(String createUserId) {
			this.createUserId = createUserId;
		}

		public String getCreateUserName() {
			return createUserName;
		}

		public void setCreateUserName(String createUserName) {
			this.createUserName = createUserName;
		}

		public String getError() {
			return error;
		}

		public void setError(String error) {
			this.error = error;
		}
	}
}
