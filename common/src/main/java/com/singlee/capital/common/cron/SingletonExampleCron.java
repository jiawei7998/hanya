/*
 *    Copyright 2003-2004 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import java.io.IOException;

/**
 * This class is an example of how to run jt-cron as a singleton.
 * @author Jarrett Taylor
 *
 */
public class SingletonExampleCron {
	@SuppressWarnings("unused")
	private static  Cron c;
	public static void main(String[] a) throws IOException{
		
		/*Set<CronJob> jobs = new HashSet<CronJob>();
		
		List<CronClass> clses = new LinkedList<CronClass>();
		Map<String, Object> parameter = new HashMap<String, Object>();
		CronClass cc = new CronClassImpl("com.singlee.cron.Abc", true, parameter);
		CronClass cd = new CronClassImpl("com.singlee.cron.Def", true, parameter);
		clses.add(cc);
		CronJob cj = new CronJobImpl("inJobId", "inJobName", "0-59/1 * * * *", false, false, clses );
		List<CronClass> clazz = new LinkedList<CronClass>();
		clazz.add(cd);
		CronJob cj2 = new CronJobImpl("inJobId1", "inJobName", "0-59/2 * * * *", false, false, clazz );
		jobs.add(cj);
		jobs.add(cj2);
		c = new Cron(jobs , 4, true);
		
		try {
			Thread.sleep(6100000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		cj2.disable();
		
		System.in.read();*/
		
	}
	
} //class
