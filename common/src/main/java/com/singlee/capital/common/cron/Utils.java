/*
 *    Copyright 2003-2005 Jarrett Taylor (http://www.jarretttaylor.com)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.singlee.capital.common.cron;

import java.util.*;

/**
 * provides static utility methods.
 * 
 * @author Jarrett Taylor
 * @author Troy Harrison
 */
public class Utils
{
	/**
	 * private so that it cannot be created. this class contains only static
	 * utility methods.
	 */
	private Utils()
	{
	}

	/**
	 * determines if the CronJob should run at the specified time.
	 * @param schedule the cron schedule to evaluate
	 * @param currentMinute int representing the minute in time
	 * @param currentHour int representing the hour in time
	 * @param currentDayOfMonth int representing the day of the month in time
	 * @param currentMonth int representing the month in time
	 * @param currentDayOfWeek int representing the day of the week in time
	 * @return true if the schedule should run at the specified time
	 */
	static boolean shouldRun(String schedule, int currentMinute, int currentHour, int currentDayOfMonth, int currentMonth, int currentDayOfWeek) throws IllegalArgumentException
	{
		StringTokenizer tok = new StringTokenizer(schedule, " ");
		if (5 != tok.countTokens()) {
			throw new IllegalArgumentException("Wrong number of arguments in schedule [" + schedule + "]");}
		GregorianCalendar cal = new GregorianCalendar();
		boolean minutePassed = runNow(tok.nextToken(), currentMinute, cal, Calendar.MINUTE);
		boolean hourPassed = runNow(tok.nextToken(), currentHour, cal, Calendar.HOUR_OF_DAY);
		boolean dayOfMonthPassed = runNow(tok.nextToken(), currentDayOfMonth, cal, Calendar.DAY_OF_MONTH);
		boolean monthPassed = runNow(tok.nextToken(), currentMonth, cal, Calendar.MONTH);
		boolean dayOfWeekPassed = runNow(tok.nextToken(), currentDayOfWeek, cal, Calendar.DAY_OF_WEEK);
		return (minutePassed && hourPassed && monthPassed && (dayOfMonthPassed || dayOfWeekPassed));
	} //shouldRun

	/**
	 * breaks down the fieldValue supplied and checks to see if it should run 
	 * based on the "currentValue" passed in. 
	 * @param fieldValue represents one element in the cron schedule string
	 * @param currentValue the point in time to evaluate against
	 * @param minValue the min allowed value of the current field
	 * @param maxValue the max allowed value of the current field
	 * @return true if the element should run at the supplied point in time
	 */
	private static boolean runNow(String fieldValue, int currentValue, Calendar cal, int field)
	{
		return (getList(fieldValue, cal, field).contains(new Integer(currentValue)));
	} //runNow

	/**
	 * returns a List of Integer objects representing all possible values
	 * that a produced from the supplied field value. 
	 * @param fieldValue the value of the field to evaluate
	 * @param minValue the min allowed value for the field
	 * @param maxValue the max allowed value for the field
	 * @return a LinkedList of Integer objects as a result of evaluating the field
	 */
	private static List<Integer> getList(String fieldValue, Calendar cal, int field) throws IllegalArgumentException
	{
		List<Integer> list = new ArrayList<Integer>();
		StringTokenizer tok2 = new StringTokenizer(fieldValue , ",");
		while (tok2.hasMoreTokens())
		{
			int increment = 1;
			String temp = tok2.nextToken();
			if (temp.indexOf('/') != -1)
			{	
				StringTokenizer tok3 = new StringTokenizer(temp, "/");
				if (tok3.countTokens() != 2) {
					throw new IllegalArgumentException(temp + " is not a valid increment value for " + getFieldName(field)+ " [" + fieldValue + "]");}
				temp = tok3.nextToken();
				String temp2 = tok3.nextToken();
				try
				{
					increment = Integer.parseInt(temp2);
				} //try
				catch(NumberFormatException e)
				{
					throw new IllegalArgumentException(temp2 + " is not a valid increment value for " + getFieldName(field)+ " [" + fieldValue + "]");
				} //catch
			} //if

			int minValue = getMinValue(cal, field);
			int maxValue = getMaxValue(cal, field);

			int start = -1;
			int stop = -1;
			if (temp.indexOf('-') != -1)
			{	
				StringTokenizer tok3 = new StringTokenizer(temp, "-");
				if (tok3.countTokens() != 2) {
					throw new IllegalArgumentException(temp + " is not a valid range value for " + getFieldName(field)+ " [" + fieldValue + "]");}
				start = Integer.parseInt(tok3.nextToken());
				stop = Integer.parseInt(tok3.nextToken());
			} //if
			else if ("*".equals(temp))
			{
				start = minValue;
				stop = maxValue;
			} //else if
			else
			{
				start = Integer.parseInt(temp);
				stop = start;
			} //else
			if (start < minValue || start > maxValue || stop < minValue || stop > maxValue || increment <= 0 || increment > maxValue) {
				throw new IllegalArgumentException("Schedule entry is invalid for " + getFieldName(field)+ " [" + fieldValue + "]");}
			while (start <= stop)
			{
				list.add(new Integer(start));
				start += increment;
			} //while
		} //while
		return list;
	} //getList

	/**
	 * gets the next run date for the supplied schedule, starting from the supplied date
	 * @param schedule the schedule to evaluate
	 * @param fromDate the date from which to start
	 * @return the earliest run date based on the supplied schedule and date
	 */
	public static Date getNextRun(String schedule, Date fromDate)
	{
		StringTokenizer tok = new StringTokenizer(schedule, " ");
		if (5 != tok.countTokens()) {
			return null;
		}
		String minuteField = tok.nextToken();
		String hourField = tok.nextToken();
		String dayOfMonthField = tok.nextToken();
		String monthField = tok.nextToken();
		String dayOfWeekField = tok.nextToken();
		
		int[] daysOfWeek = new int[0];
		int[] months = new int[0];
		int[] daysOfMonth = new int[0];
		int[] hours = new int[0];
		int[] minutes = new int[0];

		GregorianCalendar workingDate = new GregorianCalendar();

		// First see if there is a specific dayOfWeek specified
		if (!"*".equals(dayOfWeekField)) {
			daysOfWeek = getArray(getList(dayOfWeekField, workingDate, Calendar.DAY_OF_WEEK));}
		if (!"*".equals(dayOfMonthField)) {
			daysOfMonth = getArray(getList(dayOfMonthField, workingDate, Calendar.DAY_OF_MONTH));}
		if (!"*".equals(monthField)) {
			months = getArray(getList(monthField, workingDate, Calendar.MONTH));}
		if (!"*".equals(hourField)) {
			hours = getArray(getList(hourField, workingDate, Calendar.HOUR_OF_DAY));}
		if (!"*".equals(minuteField)) {
			minutes = getArray(getList(minuteField, workingDate, Calendar.MINUTE));
		}
		if (fromDate != null) {
			workingDate.setTime(fromDate);
		}
		workingDate.set(Calendar.SECOND, 0);
		
		GregorianCalendar stopDate = (GregorianCalendar) workingDate.clone();
		stopDate.add(Calendar.YEAR, 1);
		return checkDate(stopDate, workingDate, months, daysOfWeek, daysOfMonth, hours, minutes);
		
	} //getNextRun
	
	/**
	 * Recursive method that attempts to determine the next scheduled run of a
	 * cron based schedule given a specific date.  If any of the fields "roll"
	 * e.g. new month, this method recurses with the new date to see if the new 
	 * time period is still valid for the given date.  Should the date supplied
	 * be after the specified stopDate this method returns null.
	 * @param stopDate - Date after which to stop checking.
	 * @param date - Current Date to check.
	 * @param months - Array of valid months for the cron schedule.
	 * @param daysOfWeek - Array of valid daysOfWeek for the cron schedule.
	 * @param daysOfMonth - Array of valid daysOfMonth for the cron schedule.
	 * @param hours - Array of valid hours for the cron schedule.
	 * @param minutes - Array of valid minutes for the cron schedule.
	 * @return A Date object specifing the next run using the given information
	 * or null if no valid date is found prior to reaching the given stopDate.
	 */
	private static Date checkDate(GregorianCalendar stopDate, GregorianCalendar date, 
			int[] months, int[] daysOfWeek, int[] daysOfMonth, int[] hours, int[] minutes)
	{
		if (date.after(stopDate)) {
			return null;
		}
		boolean stopLoop = false;
		
		// Start with Months to narrow scope
		if (months.length > 0)
		{
			for (int i = date.getActualMinimum(Calendar.MONTH); i <= date.getActualMaximum(Calendar.MONTH); i++)
			{
				for (int j = 0; j < months.length; j++)
				{
					if ((date.get(Calendar.MONTH) + 1) == months[j])
					{
						stopLoop = true;
						break;
					} //if
				} //for
				
				if (stopLoop) {
					break;
				}
				date.add(Calendar.MONTH, 1);
				date.set(Calendar.DAY_OF_MONTH, 1);
				date.set(Calendar.HOUR_OF_DAY, 0);
				date.set(Calendar.MINUTE, 0);
			} //for i
		} //if
		
		// Next find the day for which to run
		stopLoop = false;
		if (daysOfWeek.length > 0 || daysOfMonth.length > 0)
		{
			for (int j = date.getActualMinimum(Calendar.DAY_OF_MONTH); j <= date.getActualMaximum(Calendar.DAY_OF_MONTH); j++)
			{
				for (int i = 0; i < daysOfWeek.length; i++)
				{
					if ((date.get(Calendar.DAY_OF_WEEK) - 1) == daysOfWeek[i])
					{
						stopLoop = true;
						break;
					} //if
				} //for
				
				if (!stopLoop)
				{
					for (int i = 0; i < daysOfMonth.length; i++)
					{
						if (date.get(Calendar.DAY_OF_MONTH) == daysOfMonth[i])
						{
							stopLoop = true;
							break;
						} //if
					} //for i
				} //if
				
				if (stopLoop) {
					break;
				}
				int month = date.get(Calendar.MONTH);
				date.add(Calendar.DAY_OF_MONTH, 1);
				date.set(Calendar.HOUR_OF_DAY, 0);
				date.set(Calendar.MINUTE, 0);

				if (month != date.get(Calendar.MONTH)) {  // we wrapped so let's recurse.
					return checkDate(stopDate, date, months, daysOfWeek, daysOfMonth, hours, minutes);}
			} //for j
		} //if
		
		stopLoop = false;
		if (hours.length > 0)
		{
			for (int i = date.getActualMinimum(Calendar.HOUR_OF_DAY); i <= date.getActualMaximum(Calendar.HOUR_OF_DAY); i++)
			{
				for (int j = 0; j < hours.length; j++)
				{
					if (date.get(Calendar.HOUR_OF_DAY) == hours[j])
					{
						stopLoop = true;
						break;
					} //if
				} //for j
				
				if (stopLoop) {
					break;
				}
				int day = date.get(Calendar.DAY_OF_MONTH);
				date.add(Calendar.HOUR_OF_DAY, 1);
				date.set(Calendar.MINUTE, 0);
				if (day != date.get(Calendar.DAY_OF_MONTH)) {
					return checkDate(stopDate, date, months, daysOfWeek, daysOfMonth, hours, minutes);}
			} //for i
		} //if

		stopLoop = false;

		if (minutes.length > 0)
		{
			for (int i = date.getActualMinimum(Calendar.MINUTE); i <= date.getActualMaximum(Calendar.MINUTE); i++)
			{
				for (int j = 0; j < minutes.length; j++)
				{
					if (date.get(Calendar.MINUTE) == minutes[j])
					{
						stopLoop = true;
						break;
					} //if
				} //for j
				
				if (stopLoop) {
					break;
				}
				int minute = date.get(Calendar.HOUR_OF_DAY);
				date.add(Calendar.MINUTE, 1);
				
				if (minute != date.get(Calendar.HOUR_OF_DAY)) {
					return checkDate(stopDate, date, months, daysOfWeek, daysOfMonth, hours, minutes);}
			} //for i
		} //if
		
		return date.getTime();
	} //checkDate
	
	/**
	 * convenience method for converting a List of Integer objects to and int[]
	 * @param list List of Integer objects
	 * @return the converted array
	 */
	private static int[] getArray(List<Integer> list)
	{
		if (list == null) {
			return new int[0];}
		int[] vals = new int[list.size()];
		for (int x = 0; x < list.size(); x++) {
			vals[x] = ((Integer) list.get(x)).intValue();}
		return vals;
	} //getArray
	
	/**
	 * returns the closest date at which a job is scheduled to run
	 * @param cronJobs the jobs to be evaluated
	 * @param fromDate the date from which to start the evaluation
	 * @return the date at which the ealiest job should run
	 */
	static Date getClosestRun(Collection<? extends CronJob> cronJobs, Date fromDate)
	{
		Set<Date> sorted = new TreeSet<Date>();
		CronJob job;
		Iterator<? extends CronJob> iter = cronJobs.iterator();
		while (iter.hasNext())
		{
			job = (CronJob) iter.next();
			if (!job.getDisabled())
			{
				Date date = getNextRun(convertSchedule(job.getSchedule()), fromDate);
				if (date != null) {
					sorted.add(date);}
			} //if
		} //for
		
		if (sorted.size() > 0) {
			return (Date) sorted.iterator().next();}
		else {
			return null;}
	} //getClosestRun
	
	/**
	 * validates a cron job's schedule. returns true if the schedule validates. 
	 * an IllegalArgumentException will be thrown in the event that it encounters
	 * an invalid value or bad format. this does not check to for things like 
	 * February 30, as it just verifies min/max ranges for fields as well as the 
	 * proper format of the fields.  
	 * @param job the CronJobInfo to be validated
	 * @return true if schedule is valid
	 * @throws IllegalArgumentException if it encounters an invalid value or format 
	 */
	public static boolean validateSchedule(CronJob job) throws IllegalArgumentException
	{
			StringTokenizer tok = new StringTokenizer(job.getSchedule(), " ");
			if (5 != tok.countTokens()) {
				throw new IllegalArgumentException("Wrong number of arguments for [" + job.getJobName() + "]");}
			GregorianCalendar cal = new GregorianCalendar();
			boolean minutePassed = checkRange(tok.nextToken(), cal, Calendar.MINUTE);
			boolean hourPassed = checkRange(tok.nextToken(), cal, Calendar.HOUR_OF_DAY);
			boolean dayOfMonthPassed = checkRange(tok.nextToken(), cal, Calendar.DAY_OF_MONTH);
			boolean monthPassed = checkRange(tok.nextToken(), cal, Calendar.MONTH);
			boolean dayOfWeekPassed = checkRange(tok.nextToken(), cal, Calendar.DAY_OF_WEEK);
			return (minutePassed && hourPassed && monthPassed && dayOfMonthPassed && dayOfWeekPassed);
	} //validateSchedule

	/**
	 * validates a particular field
	 * @param fieldValue the value of the field
	 * @param minValue the min allowed value for the field
	 * @param maxValue the max allowed value for the field
	 * @return true if the field is valid
	 * @throws IllegalArgumentException if it encounters an invalid value or format
	 */
	private static boolean checkRange(String fieldValue, Calendar cal, int field) throws IllegalArgumentException
	{
		return ("*".equals(fieldValue) || getList(fieldValue, cal, field) != null);
	} //runNow

	private static int getMinValue(Calendar cal, int field)
	{
		int value = cal.getMinimum(field);
		if (field == Calendar.MONTH) {
			value++;}
		else if (field == Calendar.DAY_OF_WEEK) {
			value--;}

		return value;	
	} //getMinValue
	
	private static int getMaxValue(Calendar cal, int field)
	{
		//don't use "getActualMaximum" as it doesn't represent the highest possible value out of all the months 
		int value = cal.getMaximum(field);
		if (field == Calendar.MONTH) {
			value++; }
		else if (field == Calendar.DAY_OF_WEEK) {
			value--;}

		return value;	
	} //getMaxValue
	
	private static String getFieldName(int field)
	{
		String fieldName;
		switch(field)
		{
			case Calendar.MONTH :
			{
				fieldName = "month";
				break;
			}
			case Calendar.DAY_OF_MONTH :
			{
				fieldName = "day of month";
				break;
			}
			case Calendar.DAY_OF_WEEK :
			{
				fieldName = "day of week";
				break;
			}
			case Calendar.HOUR_OF_DAY :
			{
				fieldName = "hour of day";
				break;
			}
			case Calendar.MINUTE :
			{
				fieldName = "minute";
				break;
			}
			default :
			{
				fieldName = "unknown";
				break;
			}
		}
		return fieldName;
	} //getFieldName

	public static String convertSchedule(String inSchedule)
	{
		if (inSchedule != null && inSchedule.startsWith("@"))
		{
			if ("@reboot".equals(inSchedule)) //Run once, at startup.
			{
				return "0 0 31 2 0"; //never run
			} //if
			else if ("@yearly".equals(inSchedule) 
					|| "@annually".equals(inSchedule)) //Run once a year
			{
				return "0 0 1 1 *";
			} //else if
			else if ("@monthly".equals(inSchedule)) //Run once a month
			{
				return "0 0 1 * *";
			} //else if
			else if ("@weekly".equals(inSchedule)) //Run once a week
			{
				return "0 0 * * 0";
			} //else if
			else if ("@daily".equals(inSchedule)
					|| "@midnight".equals(inSchedule)) //Run once a day
			{
				return "0 0 * * *";
			} //else if
			else if ("@hourly".equals(inSchedule)) //Run once an hour
			{
				return "0 * * * *";
			} //else if
		} //if

		return inSchedule;
	} //concertSchedule

	public static boolean isReboot(String inSchedule)
	{
		return ("@reboot".equals(inSchedule)); //Run once, at startup.
	} //isReboot

	public static void main(String [] args) {
		System.out.println(new Date());
		System.out.println(Utils.getNextRun("0-59/1 * * * *", new Date()));
	}
} //class
