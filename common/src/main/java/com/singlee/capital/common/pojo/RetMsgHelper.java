package com.singlee.capital.common.pojo;

import com.github.pagehelper.Page;
import com.singlee.capital.common.i18n.MsgUtils;
import com.singlee.capital.common.pojo.page.PageInfo;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;




/**
 * 返回消息定义回复
 */
public class RetMsgHelper {
	public static final String codeOk = "error.common.0000";
	public static final String codeFail = "error.common.0001";
	
	public static final boolean isOk(RetMsg<Serializable> msg){
		return codeOk.equals(msg.getCode());
	}
	

	public static final RetMsg<Pair<Object,Object>> ok(Object p1, Object p2) {
		Pair<Object,Object> p = Pair.of(p1, p2);
		return ok(p);		
	}
	
	public static final<T> RetMsg<T> ok(T ret) {
		return new RetMsg<T>(codeOk, MsgUtils.getOkMessage(), null, ret);
	}
	/**
	 * 针对 page 属性优化一次
	 * @param p
	 * @return
	 */
	public static final <T> RetMsg<PageInfo<T>> ok(Page<T> p) {
		PageInfo<T> pi = new PageInfo<T>(p);
		return new RetMsg<PageInfo<T>>(codeOk, MsgUtils.getOkMessage(), null, pi);
	}	
	
	public static final<T> RetMsg<T> ok(String msg, T ret) {
		return new RetMsg<T>(codeOk, msg, null,ret);
	}
	
	public static final RetMsg<Serializable> ok(String msg, String detail,Serializable ret) {
		return simple(codeOk, msg, detail, ret);
	}
	
	public static final RetMsg<Serializable> ok(String msg, String detail) {
		return simple(codeOk, msg, detail, null);
	}

	public static final RetMsg<Serializable> fail(String msg) {
		return simple(codeFail, msg, null, null);
	}
	
	public static final RetMsg<Serializable> ok(String msg) {
		return simple(codeOk, msg, null, null);
	}
	
	public static final RetMsg<Serializable> ok() {
		return simple(codeOk, MsgUtils.getOkMessage(), null, null);
	}
	public static final RetMsg<String> excetpion(String code, String msg, Throwable je) {
		String s = ExceptionUtils.getRootCauseMessage(je);
		return new RetMsg<String>(code, msg, je.getMessage(),s);
	}
	
	public static final RetMsg<Serializable> simple(String code) {
		return simple(code, MsgUtils.getMessage(code));
	}
	
	public static final RetMsg<Serializable> simple(String code, String... params) {
		return simple(code, MsgUtils.getMessage(code, params));
	}
	
	public static final RetMsg<Serializable> simple(String code, String msg) {
		return simple(code, msg, "");
	}
	
	public static final RetMsg<Serializable> simple(String code, String msg, String detail ) {
		return simple(code,msg,detail,null);
	}

	public static final <T> RetMsg<T> simple(String code, String msg, String detail, T s) {
		return new RetMsg<T>(code, msg, detail,s);
	}
}