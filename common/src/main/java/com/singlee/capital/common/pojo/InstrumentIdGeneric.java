package com.singlee.capital.common.pojo;

import java.io.Serializable;

/**
 * 金融工具泛型对象
 * @author LyonChen
 *
 */
public class InstrumentIdGeneric<T extends Serializable> extends InstrumentId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** T */
	private T t;

	public InstrumentIdGeneric(InstrumentId id, T _t) {
		this(id.getI_code(),id.getA_type(),id.getM_type(),id.getVer_num());
		t = _t;
	}
	
	public InstrumentIdGeneric(InstrumentId id) {
		this(id.getI_code(),id.getA_type(),id.getM_type(),id.getVer_num());
	}
	
	public InstrumentIdGeneric() {
		super();
	}

	public InstrumentIdGeneric(String i_code, String a_type, String m_type, int ver) {
		super(i_code, a_type, m_type, ver);
	}

	public InstrumentIdGeneric(String i_code, String a_type, String m_type) {
		super(i_code, a_type, m_type);
	}

	public InstrumentIdGeneric(String id) {
		super(id);
	}

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}

	@Override
	public String toString() {
		return String.format("InstrumentId[%s],[%s]",super.toId(), t);
	}
	

	
}
