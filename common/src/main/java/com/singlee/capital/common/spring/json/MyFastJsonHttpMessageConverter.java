package com.singlee.capital.common.spring.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.singlee.capital.common.util.JY;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.GenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

public class MyFastJsonHttpMessageConverter extends AbstractHttpMessageConverter<Object> implements GenericHttpMessageConverter<Object>{
	public final static Charset UTF8 = Charset.forName("UTF-8");
	private Charset charset = UTF8;
	
	public MyFastJsonHttpMessageConverter() {
		super(new MediaType("application", "json", UTF8), 
				new MediaType("application", "*+json", UTF8), 
				new MediaType("text", "html", UTF8)); //ie执行ajax 
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		return true;
	}

	public Charset getCharset() {
		return this.charset;
	}

	public void setCharset(Charset charset) {
		this.charset = charset;
	}

	@Override
	public Object read(Type type, Class<?> arg1, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
		byte[] bytes = copyBytes(inputMessage);
		return JSON.parseObject(bytes, 0, bytes.length, charset.newDecoder(),
				type, FastJsonUtil.features);
	}

	@Override
	public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
		return super.canWrite(clazz, mediaType);
	}

	@Override
	public void write(Object o, Type type, MediaType contentType, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
		super.write(o, contentType, outputMessage);
	}

	@Override
	protected Object readInternal(Class<? extends Object> clazz,
			HttpInputMessage inputMessage) throws IOException{
		byte[] bytes = copyBytes(inputMessage);
		return JSON.parseObject(bytes, 0, bytes.length, charset.newDecoder(),
				clazz);
	}

	private byte[] copyBytes(HttpInputMessage inputMessage) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream in = inputMessage.getBody();
		byte[] buf = new byte[1024];
		for (;;) {
			int len = in.read(buf);
			if (len == -1) {
				break;
			}
			if (len > 0) {
				baos.write(buf, 0, len);
			}
		}
		byte[] bytes = baos.toByteArray();
		String text = new String(bytes,charset);
		JY.info(String.format("-----read json <<<%s", text));
		return bytes;
	}

	@Override
	protected void writeInternal(Object obj, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
		OutputStream out = outputMessage.getBody();
		SimplePropertyPreFilter filter = new SimplePropertyPreFilter();
		filter.getExcludes().add("passwd");
		filter.getExcludes().add("userImage");
		String text = FastJsonUtil.toJSONString(obj);
		byte[] bytes = text.getBytes(charset);
		JY.info(String.format("-----write json >>>%s", text));
		out.write(bytes);
	}

	@Override
	public boolean canRead(Type arg0, Class<?> arg1, MediaType arg2) {
		return canRead(arg2);
	}
}