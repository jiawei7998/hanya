package com.singlee.capital.common.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.InputStream;

/**
 * 参数文件 解析工具
 * 
 * 使用apache configuration
 * @author LyonChen
 *
 */
public class PropertiesUtil {

	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws ConfigurationException
	 */
	public static PropertiesConfiguration parseFile(String fileName) throws ConfigurationException {
		return parseFile(fileName, "utf-8", ',');
	}

	/**
	 * 
	 * @param fileName
	 * @param encode
	 * @param s
	 * @return
	 * @throws ConfigurationException
	 */
	public static PropertiesConfiguration parseFile(String fileName, String encode, char s) throws ConfigurationException {
		// 生成输入流
		InputStream ins = PropertiesUtil.class.getResourceAsStream("/" + fileName);
		// 生成properties对象
		PropertiesConfiguration p = new PropertiesConfiguration();
		try {
			p.setListDelimiter(s);
			p.load(ins, encode);
			ins.close();
		} catch (Exception e) {
			JY.info("解析配置 %s 文件错误 %s",fileName, e.getMessage());
			throw new ConfigurationException(e);
		}
		return p;
	}
}
