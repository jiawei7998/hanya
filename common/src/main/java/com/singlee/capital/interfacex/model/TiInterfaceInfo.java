package com.singlee.capital.interfacex.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name ="TI_INTERFACE_INFO")
public class TiInterfaceInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 接口编号
	@Id
	private String infaceId;
	//接口名称
	private String infaceName;
	//接口功能描述
	private String infaceDescription;
	//接口使用状态
	private String infaceStatus;
	//接口描述
	private String infaceComm;
	
	
	public String getInfaceId() {
		return infaceId;
	}
	public void setInfaceId(String infaceId) {
		this.infaceId = infaceId;
	}
	public String getInfaceName() {
		return infaceName;
	}
	public void setInfaceName(String infaceName) {
		this.infaceName = infaceName;
	}
	public String getInfaceDescription() {
		return infaceDescription;
	}
	public void setInfaceDescription(String infaceDescription) {
		this.infaceDescription = infaceDescription;
	}
	public String getInfaceStatus() {
		return infaceStatus;
	}
	public void setInfaceStatus(String infaceStatus) {
		this.infaceStatus = infaceStatus;
	}
	public String getInfaceComm() {
		return infaceComm;
	}
	public void setInfaceComm(String infaceComm) {
		this.infaceComm = infaceComm;
	}
	
	
	
}
