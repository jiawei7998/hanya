package com.singlee.hessian;

import static org.springframework.util.Assert.notNull;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * hessian 接口服务端自动扫描注入
 * 
 * @author rocca.peng@hunteron.com
 * @Description
 * @Date 2015年2月8日 上午10:29:58
 */
public class CfetsRmbTradeServerScannerConfigurer implements BeanDefinitionRegistryPostProcessor, InitializingBean, ApplicationContextAware, BeanNameAware {

	private String beanName;
	private String basePackage;
	private boolean includeAnnotationConfig = true;
	private ApplicationContext applicationContext;

	// 实现了该接口
	private Class<?> markerInterface;
	// 配置了该注解
	private Class<? extends Annotation> annotationClass;
	// 存放spring 容器中的有接口的实现类的bean name
	private Map<String, String> implClassContextName = new HashMap<String, String>();
	private BeanNameGenerator nameGenerator = new AnnotationBeanNameGenerator() {
		@Override
		protected String buildDefaultBeanName(BeanDefinition definition) {
			AnnotationMetadata metadata = ((ScannedGenericBeanDefinition) definition).getMetadata();
			Map<String, Object> annotationAttributes = metadata.getAnnotationAttributes(annotationClass.getName());
			String uri = (String) annotationAttributes.get("uri");
			return uri;
		}
	};

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public void setIncludeAnnotationConfig(boolean includeAnnotationConfig) {
		this.includeAnnotationConfig = includeAnnotationConfig;
	}

	public Class<?> getMarkerInterface() {
		return markerInterface;
	}

	public void setMarkerInterface(Class<?> markerInterface) {
		this.markerInterface = markerInterface;
	}

	public Class<? extends Annotation> getAnnotationClass() {
		return annotationClass;
	}

	public void setAnnotationClass(Class<? extends Annotation> annotationClass) {
		this.annotationClass = annotationClass;
	}

	public BeanNameGenerator getNameGenerator() {
		return nameGenerator;
	}

	public void setNameGenerator(BeanNameGenerator nameGenerator) {
		this.nameGenerator = nameGenerator;
	}

	@Override
	public void setBeanName(String name) {
		this.beanName = name;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void afterPropertiesSet() throws Exception {
		notNull(this.basePackage, "请配置'basePackage'属性, " + beanName);
        notNull(this.annotationClass, "请配置'annotationClass'属性," + beanName);

		XmlWebApplicationContext xmlContext = (XmlWebApplicationContext) applicationContext;
		do {
			DefaultListableBeanFactory beanFoctory = (DefaultListableBeanFactory) xmlContext.getAutowireCapableBeanFactory();
			Field findField = ReflectionUtils.findField(beanFoctory.getClass(), "singletonObjects");
			ReflectionUtils.makeAccessible(findField);
			Map<String, Object> field = (Map<String, Object>) ReflectionUtils.getField(findField, beanFoctory);
			for (Entry<String, Object> entry : field.entrySet()) {
				Class<?>[] interfaces = AopUtils.getTargetClass(entry.getValue()).getInterfaces();
				for (Class<?> interfaceClass : interfaces) {
					Annotation annotation = interfaceClass.getAnnotation(annotationClass);
					if (annotation != null) {
						implClassContextName.put(interfaceClass.getName(), entry.getKey());
					}
				}
			}
		} while ((xmlContext = (XmlWebApplicationContext) xmlContext.getParentBeanFactory()) != null);
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		HessianClassPathScanner scan = new HessianClassPathScanner(registry);
		scan.setResourceLoader(this.applicationContext);
		scan.setBeanNameGenerator(this.nameGenerator);
		// 引入注解配置
		scan.setIncludeAnnotationConfig(this.includeAnnotationConfig);
		scan.registerFilters();

		String[] basePackages = StringUtils.tokenizeToStringArray(this.basePackage, ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);
		scan.scan(basePackages);
	}

	private class HessianClassPathScanner extends ClassPathBeanDefinitionScanner {

		public HessianClassPathScanner(BeanDefinitionRegistry registry) {
			super(registry, false);
		}

		@Override
		public Set<BeanDefinitionHolder> doScan(String... basePackages) {
			Set<BeanDefinitionHolder> beanDefinitions = super.doScan(basePackages);
			if (beanDefinitions.isEmpty()) {
				logger.warn(String.format("在%s包下,未找到hessian注解,请检查你的配置!", Arrays.toString(basePackages)));
			} else {
				for (BeanDefinitionHolder holder : beanDefinitions) {
					GenericBeanDefinition definition = (GenericBeanDefinition) holder.getBeanDefinition();

					if (logger.isDebugEnabled()) {
						logger.debug(String.format("创建hessian类名称%s和类定义%s的接口对象", holder.getBeanName(), definition.getBeanClassName()));
					}

					// the mapper interface is the original class of the bean
					// but, the actual class of the bean is
					// HessianServiceExporter
					definition.getPropertyValues().add("serviceInterface", definition.getBeanClassName());
					String beanNameRef = implClassContextName.get(definition.getBeanClassName());
					definition.getPropertyValues().add("service", new RuntimeBeanReference(beanNameRef));
					definition.setBeanClass(HessianServiceExporter.class);
					logger.info(String.format("[自定义注解/%s],注入的服务信息serviceInterface[%s],service[%s]",annotationClass.getName(),((ScannedGenericBeanDefinition) definition).getMetadata().getClassName(),beanNameRef));
				}
			}
			return beanDefinitions;

		}

		@Override
		protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
			return (beanDefinition.getMetadata().isInterface() && beanDefinition.getMetadata().isIndependent());
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected boolean checkCandidate(String beanName, BeanDefinition beanDefinition) throws IllegalStateException {
			String implBeanName = implClassContextName.get(beanDefinition.getBeanClassName());
			if (!(null == implBeanName || "".equals(implBeanName)) && super.checkCandidate(beanName, beanDefinition)) {
				// if (!StringUtils.isEmpty(implBeanName) &&
				// super.checkCandidate(beanName, beanDefinition)) {
				return true;
			} else {
	            logger.warn(String.format("跳过hessian类名称为%s和类定义为%s的对象,请检查该接口是否已实现或者实现类未加注解!", beanName, beanDefinition.getBeanClassName()));
				return false;
			}
		}

		public void registerFilters() {
			boolean acceptAllInterfaces = true;

			// if specified, use the given annotation and / or marker interface
			if (CfetsRmbTradeServerScannerConfigurer.this.annotationClass != null) {
				addIncludeFilter(new AnnotationTypeFilter(CfetsRmbTradeServerScannerConfigurer.this.annotationClass));
				acceptAllInterfaces = false;
			}

			// override AssignableTypeFilter to ignore matches on the actual
			// marker interface
			if (CfetsRmbTradeServerScannerConfigurer.this.markerInterface != null) {
				addIncludeFilter(new AssignableTypeFilter(CfetsRmbTradeServerScannerConfigurer.this.markerInterface) {
					@Override
					protected boolean matchClassName(String className) {
						return false;
					}
				});
				acceptAllInterfaces = false;
			}

			if (acceptAllInterfaces) {
				// default include filter that accepts all classes
				addIncludeFilter(new TypeFilter() {
					@Override
					public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
						return true;
					}
				});
			}

			// exclude package-info.java
			addExcludeFilter(new TypeFilter() {
				@Override
				public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
					String className = metadataReader.getClassMetadata().getClassName();
					return className.endsWith("package-info");
				}
			});
		}
	}
}
