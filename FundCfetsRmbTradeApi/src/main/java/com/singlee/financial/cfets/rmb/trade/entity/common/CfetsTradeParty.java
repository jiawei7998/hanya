package com.singlee.financial.cfets.rmb.trade.entity.common;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 交易机构信息
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_PARTY")
public class CfetsTradeParty implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableCid;//外键CFETS_TRADE_PROGRESS
	private String partyId;//交易账号
	private String partyRole;//账号角色
	private String partySubid2;//交易员ID
	private String partySubid266;//交易账户6位码
	private String partySubid162;//API用户id
	private String partySubid29;//机构来源
	private String partySubid135;//机构6位码
	private String partySubid125;//机构中文简称
	private String partySubid267;//交易账户中文简称
	private String partySubid101;//交易员名称
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableCid() {
		return tableCid;
	}
	public void setTableCid(String tableCid) {
		this.tableCid = tableCid;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getPartyRole() {
		return partyRole;
	}
	public void setPartyRole(String partyRole) {
		this.partyRole = partyRole;
	}
	public String getPartySubid2() {
		return partySubid2;
	}
	public void setPartySubid2(String partySubid2) {
		this.partySubid2 = partySubid2;
	}
	public String getPartySubid266() {
		return partySubid266;
	}
	public void setPartySubid266(String partySubid266) {
		this.partySubid266 = partySubid266;
	}
	public String getPartySubid29() {
		return partySubid29;
	}
	public void setPartySubid29(String partySubid29) {
		this.partySubid29 = partySubid29;
	}
	public String getPartySubid135() {
		return partySubid135;
	}
	public void setPartySubid135(String partySubid135) {
		this.partySubid135 = partySubid135;
	}
	public String getPartySubid125() {
		return partySubid125;
	}
	public void setPartySubid125(String partySubid125) {
		this.partySubid125 = partySubid125;
	}
	public String getPartySubid267() {
		return partySubid267;
	}
	public void setPartySubid267(String partySubid267) {
		this.partySubid267 = partySubid267;
	}
	public String getPartySubid101() {
		return partySubid101;
	}
	public void setPartySubid101(String partySubid101) {
		this.partySubid101 = partySubid101;
	}
	public String getPartySubid162() {
		return partySubid162;
	}
	public void setPartySubid162(String partySubid162) {
		this.partySubid162 = partySubid162;
	}
	@Override
	public String toString() {
		return "CfetsTradeParty [tableId=" + tableId + ", tableCid=" + tableCid + ", partyId=" + partyId
				+ ", partyRole=" + partyRole + ", partySubid2=" + partySubid2 + ", partySubid266=" + partySubid266
				+ ", partySubid162=" + partySubid162 + ", partySubid29=" + partySubid29 + ", partySubid135="
				+ partySubid135 + ", partySubid125=" + partySubid125 + ", partySubid267=" + partySubid267
				+ ", partySubid101=" + partySubid101 + "]";
	}


	
}
