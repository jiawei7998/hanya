package com.singlee.financial.cfets.rmb.trade;


import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeCommonOther;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMarketData;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.CfetsRmbTrade;
import com.singlee.hessian.annotation.CfetsRmbTradeContext;

@CfetsRmbTrade(context = CfetsRmbTradeContext.SINGLEE_API, uri = "/ImixRmbTradeManageServerExporter")
public interface IImixRmbTradeManageServer {
	/**
	 * CFETS登入
	 * 
	 * @return
	 */
	boolean startImixSession(String quoteType);

	/**
	 * CFETS登出
	 * 
	 * @return
	 */
	boolean stopImixSession(String quoteType);
	
	RetBean sendRMBTradeUp(CfetsTradePackage cfetsTradePackage,CfetsTradeProgress cfetsTradeProgress) throws  Exception;
	RetBean sendRMBTradeUp(CfetsTradePackage cfetsTradePackage,CfetsTradeMarketData cfetsTradeMarketData) throws  Exception;
	RetBean sendRMBTradeUp(CfetsTradePackage cfetsTradePackage,CfetsTradeCommonOther cfetsTradeCommonOther) throws  Exception;
	
	
	boolean getImixSessionState(String quoteType);
	 /**
     * 会话状态
     * @return
     */
    boolean isSessionStarted(String quoteType);

    /**
     * 执行cstp重启脚本
     * @return
     */
    String runShell(String shfile);
}
