package com.singlee.financial.cfets.rmb.trade.entity.common;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 分账表
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_ALLOCS")
public class CfetsTradeAllocs implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableCid;//外键CFETS_TRADE_PROGRESS
	private String allocAccount;//投资产品21位码
	private String individualAllocid;//分账序号
	private BigDecimal allocQty;//分账券面总额
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableCid() {
		return tableCid;
	}
	public void setTableCid(String tableCid) {
		this.tableCid = tableCid;
	}
	public String getAllocAccount() {
		return allocAccount;
	}
	public void setAllocAccount(String allocAccount) {
		this.allocAccount = allocAccount;
	}
	public String getIndividualAllocid() {
		return individualAllocid;
	}
	public void setIndividualAllocid(String individualAllocid) {
		this.individualAllocid = individualAllocid;
	}
	public BigDecimal getAllocQty() {
		return allocQty;
	}
	public void setAllocQty(BigDecimal allocQty) {
		this.allocQty = allocQty;
	}
	@Override
	public String toString() {
		return "CfetsTradeAllocs [tableId=" + tableId + ", tableCid=" + tableCid + ", allocAccount=" + allocAccount
				+ ", individualAllocid=" + individualAllocid + ", allocQty=" + allocQty + "]";
	}


	
}
