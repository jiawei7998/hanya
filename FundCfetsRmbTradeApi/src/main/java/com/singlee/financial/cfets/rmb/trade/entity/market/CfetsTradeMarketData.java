package com.singlee.financial.cfets.rmb.trade.entity.market;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;

/**
 * 行情主表
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_MARKET_DATA")
public class CfetsTradeMarketData implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableRid;//外键CFETS_TRADE_PACKAGE
	private String msgType;//报文类型
	private String mdReqId;//订阅请求ID
	private String mdBookType;//行情类型
	private String mdSubBooktype;//行情类型
	private BigDecimal marketDepth;//挡位信息
	private String subRequestType;//订阅标识
	private String subStatus;//订阅状态
	private String repoMethod;//回购方式
	private String symbol;//组件必须域
	private String marketindicator;//市场标识
	private String applErrorCode;//错误代码
	private String applErrorDesc;//错误原因
	private String transactTime;//业务发生时间
	private String securityId;//合约名称
	private String securityType;//债券类型
	
	List<CfetsTradeMdEntry> mdEntryList;//行情挡位信息
	List<CfetsTradeParty> partyList;//交易机构信息
	
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableRid() {
		return tableRid;
	}
	public void setTableRid(String tableRid) {
		this.tableRid = tableRid;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMdReqId() {
		return mdReqId;
	}
	public void setMdReqId(String mdReqId) {
		this.mdReqId = mdReqId;
	}
	public String getMdBookType() {
		return mdBookType;
	}
	public void setMdBookType(String mdBookType) {
		this.mdBookType = mdBookType;
	}
	public BigDecimal getMarketDepth() {
		return marketDepth;
	}
	public void setMarketDepth(BigDecimal marketDepth) {
		this.marketDepth = marketDepth;
	}
	public String getSubRequestType() {
		return subRequestType;
	}
	public void setSubRequestType(String subRequestType) {
		this.subRequestType = subRequestType;
	}
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	public String getRepoMethod() {
		return repoMethod;
	}
	public void setRepoMethod(String repoMethod) {
		this.repoMethod = repoMethod;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getMarketindicator() {
		return marketindicator;
	}
	public void setMarketindicator(String marketindicator) {
		this.marketindicator = marketindicator;
	}
	public String getApplErrorCode() {
		return applErrorCode;
	}
	public void setApplErrorCode(String applErrorCode) {
		this.applErrorCode = applErrorCode;
	}
	public String getApplErrorDesc() {
		return applErrorDesc;
	}
	public void setApplErrorDesc(String applErrorDesc) {
		this.applErrorDesc = applErrorDesc;
	}
	public String getTransactTime() {
		return transactTime;
	}
	public void setTransactTime(String transactTime) {
		this.transactTime = transactTime;
	}
	public String getSecurityId() {
		return securityId;
	}
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}
	
	public List<CfetsTradeMdEntry> getMdEntryList() {
		return mdEntryList;
	}
	public void setMdEntryList(List<CfetsTradeMdEntry> mdEntryList) {
		this.mdEntryList = mdEntryList;
	}
	
	public List<CfetsTradeParty> getPartyList() {
		return partyList;
	}
	public void setPartyList(List<CfetsTradeParty> partyList) {
		this.partyList = partyList;
	}
	
	public String getMdSubBooktype() {
		return mdSubBooktype;
	}
	public void setMdSubBooktype(String mdSubBooktype) {
		this.mdSubBooktype = mdSubBooktype;
	}
	
	public String getSecurityType() {
		return securityType;
	}
	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}
	@Override
	public String toString() {
		return "CfetsTradeMarketData [tableId=" + tableId + ", tableRid=" + tableRid + ", msgType=" + msgType
				+ ", mdReqId=" + mdReqId + ", mdBookType=" + mdBookType + ", mdSubBooktype=" + mdSubBooktype
				+ ", marketDepth=" + marketDepth + ", subRequestType=" + subRequestType + ", subStatus=" + subStatus
				+ ", repoMethod=" + repoMethod + ", symbol=" + symbol + ", marketindicator=" + marketindicator
				+ ", applErrorCode=" + applErrorCode + ", applErrorDesc=" + applErrorDesc + ", transactTime="
				+ transactTime + ", securityId=" + securityId + ", securityType=" + securityType + "]";
	}


	
}
