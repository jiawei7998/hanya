package com.singlee.financial.cfets.rmb.trade.entity.cbt;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 现券买卖
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_BOND")
public class CfetsTradeBond implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableCid;//外键CFETS_TRADE_PROGRESS
	private String dataSource;//报价来源
	private String securityId;//债券代码
	private String symbol;//债券名称
	private String securityTypeId;//债券类型ID
	private String securityDesc;//债券类型名称
	private String side;//交易方向
	private BigDecimal price;//净价
	private BigDecimal dirtyPrice;//全价
	private BigDecimal accruedInterestAmt;//应计利息
	private BigDecimal orderQty;//券面总额
	private BigDecimal tradeCashAmt;//交易金额
	private BigDecimal accruedInterestTotalamt;//应计利息总额
	private BigDecimal settlCurrAmt;//结算金额
	private String settlCurrency;//结算币种
	private BigDecimal settlCurrFxrate;//汇率
	private String deliveryType;//结算方式
	private String clearingMethod;//清算类型
	private String settType;//清算速度
	private String settlDate;//结算日
	private BigDecimal principal;//每百元本金额
	private BigDecimal totalPrincipal;//本金额
	private String yieldType;//到期收益率
	private BigDecimal yield;//到期收益率值
	private String stipulationType;//行权收益率
	private BigDecimal stipulationValue;//行权收益率值
	private String termToMaturity;//代偿期
	private BigDecimal maxFloor;//最大券面总额
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableCid() {
		return tableCid;
	}
	public void setTableCid(String tableCid) {
		this.tableCid = tableCid;
	}
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	public String getSecurityId() {
		return securityId;
	}
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getDirtyPrice() {
		return dirtyPrice;
	}
	public void setDirtyPrice(BigDecimal dirtyPrice) {
		this.dirtyPrice = dirtyPrice;
	}
	public BigDecimal getAccruedInterestAmt() {
		return accruedInterestAmt;
	}
	public void setAccruedInterestAmt(BigDecimal accruedInterestAmt) {
		this.accruedInterestAmt = accruedInterestAmt;
	}
	public BigDecimal getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}
	public BigDecimal getTradeCashAmt() {
		return tradeCashAmt;
	}
	public void setTradeCashAmt(BigDecimal tradeCashAmt) {
		this.tradeCashAmt = tradeCashAmt;
	}
	public BigDecimal getAccruedInterestTotalamt() {
		return accruedInterestTotalamt;
	}
	public void setAccruedInterestTotalamt(BigDecimal accruedInterestTotalamt) {
		this.accruedInterestTotalamt = accruedInterestTotalamt;
	}
	public BigDecimal getSettlCurrAmt() {
		return settlCurrAmt;
	}
	public void setSettlCurrAmt(BigDecimal settlCurrAmt) {
		this.settlCurrAmt = settlCurrAmt;
	}
	public String getSettlCurrency() {
		return settlCurrency;
	}
	public void setSettlCurrency(String settlCurrency) {
		this.settlCurrency = settlCurrency;
	}
	public BigDecimal getSettlCurrFxrate() {
		return settlCurrFxrate;
	}
	public void setSettlCurrFxrate(BigDecimal settlCurrFxrate) {
		this.settlCurrFxrate = settlCurrFxrate;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getClearingMethod() {
		return clearingMethod;
	}
	public void setClearingMethod(String clearingMethod) {
		this.clearingMethod = clearingMethod;
	}
	public String getSettType() {
		return settType;
	}
	public void setSettType(String settType) {
		this.settType = settType;
	}
	public String getSettlDate() {
		return settlDate;
	}
	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}
	public BigDecimal getPrincipal() {
		return principal;
	}
	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}
	public BigDecimal getTotalPrincipal() {
		return totalPrincipal;
	}
	public void setTotalPrincipal(BigDecimal totalPrincipal) {
		this.totalPrincipal = totalPrincipal;
	}
	public String getYieldType() {
		return yieldType;
	}
	public void setYieldType(String yieldType) {
		this.yieldType = yieldType;
	}
	public BigDecimal getYield() {
		return yield;
	}
	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}
	public String getStipulationType() {
		return stipulationType;
	}
	public void setStipulationType(String stipulationType) {
		this.stipulationType = stipulationType;
	}
	public BigDecimal getStipulationValue() {
		return stipulationValue;
	}
	public void setStipulationValue(BigDecimal stipulationValue) {
		this.stipulationValue = stipulationValue;
	}
	public String getTermToMaturity() {
		return termToMaturity;
	}
	public void setTermToMaturity(String termToMaturity) {
		this.termToMaturity = termToMaturity;
	}
	public BigDecimal getMaxFloor() {
		return maxFloor;
	}
	public void setMaxFloor(BigDecimal maxFloor) {
		this.maxFloor = maxFloor;
	}
	public String getSecurityTypeId() {
		return securityTypeId;
	}
	public void setSecurityTypeId(String securityTypeId) {
		this.securityTypeId = securityTypeId;
	}
	public String getSecurityDesc() {
		return securityDesc;
	}
	public void setSecurityDesc(String securityDesc) {
		this.securityDesc = securityDesc;
	}
	@Override
	public String toString() {
		return "CfetsTradeBond [tableId=" + tableId + ", tableCid=" + tableCid + ", dataSource=" + dataSource
				+ ", securityId=" + securityId + ", symbol=" + symbol + ", securityTypeId=" + securityTypeId
				+ ", securityDesc=" + securityDesc + ", side=" + side + ", price=" + price + ", dirtyPrice="
				+ dirtyPrice + ", accruedInterestAmt=" + accruedInterestAmt + ", orderQty=" + orderQty
				+ ", tradeCashAmt=" + tradeCashAmt + ", accruedInterestTotalamt=" + accruedInterestTotalamt
				+ ", settlCurrAmt=" + settlCurrAmt + ", settlCurrency=" + settlCurrency + ", settlCurrFxrate="
				+ settlCurrFxrate + ", deliveryType=" + deliveryType + ", clearingMethod=" + clearingMethod
				+ ", settType=" + settType + ", settlDate=" + settlDate + ", principal=" + principal
				+ ", totalPrincipal=" + totalPrincipal + ", yieldType=" + yieldType + ", yield=" + yield
				+ ", stipulationType=" + stipulationType + ", stipulationValue=" + stipulationValue
				+ ", termToMaturity=" + termToMaturity + ", maxFloor=" + maxFloor + "]";
	}
	
}
