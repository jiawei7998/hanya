package com.singlee.financial.cfets.rmb.trade.utils.dict;


/**
 * 交易中心码值：通用
 * @author xuqq
 *20210126
 */
public class DictConstantsCstp {
	
	/**
	 * 报文类型
	 * @author xuqq
	 *
	 */
	public static class MSG_TYPE {
		public final static String QUTERS = "S";
		public final static String QUOTE_STATUS_REPORT = "AI";
		public final static String QUOTE_RESPONSE = "AJ";
		public final static String QUOTE_CANCEL = "Z";
		public final static String EXECUTION_REPORT = "8";
		public final static String STR = ",S,AI,AJ,Z,8,";
	}
	/**
	 * 代码：
	 * TRADING-交易；MD-行情;CL-授信
	 * 1-现券买卖；2-质押式回购；3-利率互换、标准利率互换、标准债券远期
	 * @author xuqq
	 *
	 */
	public static class COMP_ID {
		public final static String TRADING_1 = "CFETS-TRADING-INFI-1";
		public final static String MD_1 = "CFETS-MD-INFI-1";
		public final static String TRADING_2 = "CFETS-TRADING-INFI-2";
		public final static String MD_2 = "CFETS-MD-INFI-2";
		public final static String TRADING_3 = "CFETS-TRADING-INFI-3";
		public final static String MD_3 = "CFETS-MD-INFI-3";
		public final static String CL_3 = "CFETS-CL-INFI-3";
		
	}
	/**
	 * 子标识符
	 * NDM-对话报价；QDM-请求报价、指示性报价；QDM_ESP：做市报价；ODM-匿名点击；
	 * @author xuqq
	 *
	 */
	public static class SUB_ID {
		public final static String NDM = "NDM";
		public final static String QDM = "QDM";
		public final static String QDM_ESP = "QDM-ESP";
		public final static String ODM = "ODM";
	}

	/**
	 * 市场
	 * 4-现券买卖；9-质押式回购；2-利率互换
	 * @author xuqq
	 *
	 */
	public final static class MARKETINDICATOR {
		public final static String MARKETINDICATOR4 = "4";//现券买卖
		public final static String MARKETINDICATOR9 = "9";//质押式回购
		public final static String MARKETINDICATOR2 = "2";//利率互换
//		public final static String IRS = "42";//标准利率互换
//		public final static String CR = "43";//标准债券远期
	}
	/**
	 * 渠道
	 * @author xuqq
	 *
	 */
	public final static class CHANNEL {
		public final static String IDEAL = "iDeal";
		public final static String CFETS = "CFETS";
	}
	/**
	 * 报价类别
	 * 1-对话报价;
	 * @author xuqq
	 *
	 */
	public final static class QUOTE_TYPE {
		public final static String TYPE_1 = "1";
	}
	/**
	 * 操作类型
	 * N-新增；R-修改；1-交谈
	 * @author xuqq
	 */
	public final static class QUOTE_TRANS_TYPE {
		public final static String TYPE_N = "N";
		public final static String TYPE_R = "R";
		public final static String TYPE_1 = "1";
	}
	/**
	 * 报价状态
	 * 16-正常；19-撤销；5-拒绝；21-过期；107-全部成交
	 * @author xuqq
	 */
	public final static class QUOTE_STATUS {
		public final static String STATUS_16 = "16";
		public final static String STATUS_19 = "19";
		public final static String STATUS_5 = "5";
		public final static String STATUS_21 = "21";
		public final static String STATUS_107 = "107";
	}
	/**
	 * 交易方向
	 * @author xuqq
	 */
	public final static class SIDE {
		public final static String SIDE_1 = "1";
		public final static String SIDE_2 = "2";
	}
	/**
	 * 回购方式
	 * 1-双边回购;3-通用回购
	 * @author xuqq
	 *
	 */
	public final static class REPO_METHOD {
		public final static String METHOD_1 = "1";
		public final static String METHOD_3 = "3";
	}
	/**
	 * 清算速度
	 * 1-T+0;2-T+1;一次类推（工作日）
	 * @author xuqq
	 *
	 */
	public final static class SETTL_TYPE {
		public final static String TYPE_1 = "1";
		public final static String TYPE_2 = "2";
		public final static String TYPE_3 = "3";
	}
	
	/**
	 * 结算方式
	 * 1-券款对付；4-见券付款；5-见款付券
	 * @author xuqq
	 *
	 */
	public final static class DELIVERY_TYPE {
		public final static String TYPE_1 = "1";
		public final static String TYPE_4 = "4";
		public final static String TYPE_5 = "5";
	}
	/**
	 * 清算类型
	 * 6-；13-全额清算
	 * @author xuqq
	 *
	 */
	public final static class CLEARING_METHOD {
		public final static String METHOD_6 = "6";
		public final static String METHOD_13 = "13";
		public final static String TYPE_5 = "5";
	}
	
	/**
	 * 交易时段状态
	 * @author xuqq
	 *
	 */
	public final static class TRADSESSTATUS {
		public final static String START="7";//开市
		public final static String OPEN="2";//开盘/交易恢复
		public final static String BREAK="8";//交易暂停
		public final static String CLOSED="3";//收盘
		public final static String ENDED="9";//闭市
		public final static String TRADSESSTATUS18="18";//集中报价开市时刻
	}
}
