package com.singlee.financial.cfets.rmb.trade.entity.common;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 上行接口通用接收报文
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_COMMON_OTHER")
public class CfetsTradeCommonOther implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableRid;//外键CFETS_TRADE_PACKAGE
	private String msgType;//报文类型
	private String queryRequestId;//查询请求编号
	private String transactTime;//时间
	private String marketindicator;//市场标识
	private String queryType;//操作类型
	private String quoteType;//报价类别
	private String clOrdId;//客户参考编号6
	private String quoteId;//报价编号9
	private String quoteRequestId;//报价请求编号11
	private String quoteRfqId;//RFQ报价提券编号12
	private String totNumReports;//为0：标识未查到
	private String applErrorCode;//错误代码
	private String applErrorDesc;//错误原因
	private String userReference1;//用户参考数据1
	private String userReference2;//用户参考数据2
	private String userReference3;//用户参考数据3
	private String userReference4;//用户参考数据4
	private String userReference5;//用户参考数据5
	private String userReference6;//用户参考数据6
	
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableRid() {
		return tableRid;
	}
	public void setTableRid(String tableRid) {
		this.tableRid = tableRid;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getQueryRequestId() {
		return queryRequestId;
	}
	public void setQueryRequestId(String queryRequestId) {
		this.queryRequestId = queryRequestId;
	}
	public String getTransactTime() {
		return transactTime;
	}
	public void setTransactTime(String transactTime) {
		this.transactTime = transactTime;
	}
	public String getMarketindicator() {
		return marketindicator;
	}
	public void setMarketindicator(String marketindicator) {
		this.marketindicator = marketindicator;
	}
	public String getQueryType() {
		return queryType;
	}
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	public String getClOrdId() {
		return clOrdId;
	}
	public void setClOrdId(String clOrdId) {
		this.clOrdId = clOrdId;
	}
	public String getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}
	public String getQuoteRequestId() {
		return quoteRequestId;
	}
	public void setQuoteRequestId(String quoteRequestId) {
		this.quoteRequestId = quoteRequestId;
	}
	public String getQuoteRfqId() {
		return quoteRfqId;
	}
	public void setQuoteRfqId(String quoteRfqId) {
		this.quoteRfqId = quoteRfqId;
	}
	public String getTotNumReports() {
		return totNumReports;
	}
	public void setTotNumReports(String totNumReports) {
		this.totNumReports = totNumReports;
	}
	public String getApplErrorCode() {
		return applErrorCode;
	}
	public void setApplErrorCode(String applErrorCode) {
		this.applErrorCode = applErrorCode;
	}
	public String getApplErrorDesc() {
		return applErrorDesc;
	}
	public void setApplErrorDesc(String applErrorDesc) {
		this.applErrorDesc = applErrorDesc;
	}
	public String getUserReference1() {
		return userReference1;
	}
	public void setUserReference1(String userReference1) {
		this.userReference1 = userReference1;
	}
	public String getUserReference2() {
		return userReference2;
	}
	public void setUserReference2(String userReference2) {
		this.userReference2 = userReference2;
	}
	public String getUserReference3() {
		return userReference3;
	}
	public void setUserReference3(String userReference3) {
		this.userReference3 = userReference3;
	}
	public String getUserReference4() {
		return userReference4;
	}
	public void setUserReference4(String userReference4) {
		this.userReference4 = userReference4;
	}
	public String getUserReference5() {
		return userReference5;
	}
	public void setUserReference5(String userReference5) {
		this.userReference5 = userReference5;
	}
	public String getUserReference6() {
		return userReference6;
	}
	public void setUserReference6(String userReference6) {
		this.userReference6 = userReference6;
	}
	@Override
	public String toString() {
		return "CfetsTradeCommonOther [tableId=" + tableId + ", tableRid=" + tableRid + ", msgType=" + msgType
				+ ", queryRequestId=" + queryRequestId + ", transactTime=" + transactTime + ", marketindicator="
				+ marketindicator + ", queryType=" + queryType + ", quoteType=" + quoteType + ", clOrdId=" + clOrdId
				+ ", quoteId=" + quoteId + ", quoteRequestId=" + quoteRequestId + ", quoteRfqId=" + quoteRfqId
				+ ", totNumReports=" + totNumReports + ", applErrorCode=" + applErrorCode + ", applErrorDesc="
				+ applErrorDesc + "]";
	}

	
}
