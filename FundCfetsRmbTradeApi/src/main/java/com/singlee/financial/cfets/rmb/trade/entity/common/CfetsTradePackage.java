package com.singlee.financial.cfets.rmb.trade.entity.common;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 报文保存表
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_PACKAGE")
public class CfetsTradePackage implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String upordown;//上行或下行
	private String quoteId;//报价编号
	private String clOrdId;//客户参考编号
	private String msgType;//报文类型
	private String tradedate;//成交日期
	private String marketindicator;//市场标识
	private String quoteType;//报价类别
	private String quoteTransType;//操作类型
	private String quoteStatus;//报价状态
	private String senderCompid;//
	private String senderSubid;//
	private String targetCompid;//
	private String targetSubid;//
	private String beginString;//报文版本
	private String packmsg;//报文
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getUpordown() {
		return upordown;
	}
	public void setUpordown(String upordown) {
		this.upordown = upordown;
	}
	public String getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}
	public String getClOrdId() {
		return clOrdId;
	}
	public void setClOrdId(String clOrdId) {
		this.clOrdId = clOrdId;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getTradedate() {
		return tradedate;
	}
	public void setTradedate(String tradedate) {
		this.tradedate = tradedate;
	}
	public String getMarketindicator() {
		return marketindicator;
	}
	public void setMarketindicator(String marketindicator) {
		this.marketindicator = marketindicator;
	}
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	public String getQuoteTransType() {
		return quoteTransType;
	}
	public void setQuoteTransType(String quoteTransType) {
		this.quoteTransType = quoteTransType;
	}
	public String getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}
	public String getSenderCompid() {
		return senderCompid;
	}
	public void setSenderCompid(String senderCompid) {
		this.senderCompid = senderCompid;
	}
	public String getSenderSubid() {
		return senderSubid;
	}
	public void setSenderSubid(String senderSubid) {
		this.senderSubid = senderSubid;
	}
	public String getTargetCompid() {
		return targetCompid;
	}
	public void setTargetCompid(String targetCompid) {
		this.targetCompid = targetCompid;
	}
	public String getTargetSubid() {
		return targetSubid;
	}
	public void setTargetSubid(String targetSubid) {
		this.targetSubid = targetSubid;
	}
	public String getBeginString() {
		return beginString;
	}
	public void setBeginString(String beginString) {
		this.beginString = beginString;
	}
	public String getPackmsg() {
		return packmsg;
	}
	public void setPackmsg(String packmsg) {
		this.packmsg = packmsg;
	}
	@Override
	public String toString() {
		return "CfetsTradePackage [tableId=" + tableId + ", upordown=" + upordown + ", quoteId=" + quoteId
				+ ", clOrdId=" + clOrdId + ", msgType=" + msgType + ", tradedate=" + tradedate + ", marketindicator="
				+ marketindicator + ", quoteType=" + quoteType + ", quoteTransType=" + quoteTransType + ", quoteStatus="
				+ quoteStatus + ", senderCompid=" + senderCompid + ", senderSubid=" + senderSubid + ", targetCompid="
				+ targetCompid + ", targetSubid=" + targetSubid + ", beginString=" + beginString + ", packmsg="
				+ packmsg + "]";
	}


	
}
