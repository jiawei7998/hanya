package com.singlee.financial.cfets.rmb.trade.entity.repo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 质押式回购
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_PLEDGE")
public class CfetsTradeBondPledge implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableCid;//外键CFETS_TRADE_PROGRESS
	private BigDecimal totalFaceAmt;//券面总额合计
	private BigDecimal totalDiscountAmt;//折算总金额
	private String tradeProduct;//交易品种
	private String side;//交易方向
	private String repoMethod;//回购方式
	private BigDecimal tradeLimitDays;//回购期限
	private BigDecimal realDays;//实际占款天数
	private BigDecimal price;//回购利率
	private BigDecimal benchmarkInterestRate;//基准利率
	private BigDecimal spread;//点差
	private BigDecimal tradeCashAmt;//交易金额
	private String settType;//清算速度
	private String deliveryType;//首次结算方式
	private String deliveryType2;//到期结算方式
	private String startSettleDate;//首期结算日
	private String endSettleDate;//到期结算日
	private String clearingMethod;//清算类型
	private String settlCurrency;//结算币种
	private String bondCurrency;//债券币种
	private BigDecimal settlCurrFxrate;//汇率
	private String crossCustInstname1;//跨托管机构名称1
	private BigDecimal crossCustAmt1;//跨托管交易金额1
	private String crossCustInstname2;//跨托管机构名称2
	private BigDecimal crossCustAmt2;//跨托管交易金额2
	private String securityId;//合同编号
	
	List<CfetsTradeSubbond> subBondList;//质押券
	
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableCid() {
		return tableCid;
	}
	public void setTableCid(String tableCid) {
		this.tableCid = tableCid;
	}
	public BigDecimal getTotalFaceAmt() {
		return totalFaceAmt;
	}
	public void setTotalFaceAmt(BigDecimal totalFaceAmt) {
		this.totalFaceAmt = totalFaceAmt;
	}
	public BigDecimal getTotalDiscountAmt() {
		return totalDiscountAmt;
	}
	public void setTotalDiscountAmt(BigDecimal totalDiscountAmt) {
		this.totalDiscountAmt = totalDiscountAmt;
	}
	public String getTradeProduct() {
		return tradeProduct;
	}
	public void setTradeProduct(String tradeProduct) {
		this.tradeProduct = tradeProduct;
	}
	public String getSide() {
		return side;
	}
	public void setSide(String side) {
		this.side = side;
	}
	public String getRepoMethod() {
		return repoMethod;
	}
	public void setRepoMethod(String repoMethod) {
		this.repoMethod = repoMethod;
	}
	public BigDecimal getTradeLimitDays() {
		return tradeLimitDays;
	}
	public void setTradeLimitDays(BigDecimal tradeLimitDays) {
		this.tradeLimitDays = tradeLimitDays;
	}
	public BigDecimal getRealDays() {
		return realDays;
	}
	public void setRealDays(BigDecimal realDays) {
		this.realDays = realDays;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getBenchmarkInterestRate() {
		return benchmarkInterestRate;
	}
	public void setBenchmarkInterestRate(BigDecimal benchmarkInterestRate) {
		this.benchmarkInterestRate = benchmarkInterestRate;
	}
	public BigDecimal getSpread() {
		return spread;
	}
	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}
	public BigDecimal getTradeCashAmt() {
		return tradeCashAmt;
	}
	public void setTradeCashAmt(BigDecimal tradeCashAmt) {
		this.tradeCashAmt = tradeCashAmt;
	}
	public String getSettType() {
		return settType;
	}
	public void setSettType(String settType) {
		this.settType = settType;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getDeliveryType2() {
		return deliveryType2;
	}
	public void setDeliveryType2(String deliveryType2) {
		this.deliveryType2 = deliveryType2;
	}
	public String getStartSettleDate() {
		return startSettleDate;
	}
	public void setStartSettleDate(String startSettleDate) {
		this.startSettleDate = startSettleDate;
	}
	public String getEndSettleDate() {
		return endSettleDate;
	}
	public void setEndSettleDate(String endSettleDate) {
		this.endSettleDate = endSettleDate;
	}
	public String getClearingMethod() {
		return clearingMethod;
	}
	public void setClearingMethod(String clearingMethod) {
		this.clearingMethod = clearingMethod;
	}
	public String getSettlCurrency() {
		return settlCurrency;
	}
	public void setSettlCurrency(String settlCurrency) {
		this.settlCurrency = settlCurrency;
	}
	public String getBondCurrency() {
		return bondCurrency;
	}
	public void setBondCurrency(String bondCurrency) {
		this.bondCurrency = bondCurrency;
	}
	public BigDecimal getSettlCurrFxrate() {
		return settlCurrFxrate;
	}
	public void setSettlCurrFxrate(BigDecimal settlCurrFxrate) {
		this.settlCurrFxrate = settlCurrFxrate;
	}
	public String getCrossCustInstname1() {
		return crossCustInstname1;
	}
	public void setCrossCustInstname1(String crossCustInstname1) {
		this.crossCustInstname1 = crossCustInstname1;
	}
	public BigDecimal getCrossCustAmt1() {
		return crossCustAmt1;
	}
	public void setCrossCustAmt1(BigDecimal crossCustAmt1) {
		this.crossCustAmt1 = crossCustAmt1;
	}
	public String getCrossCustInstname2() {
		return crossCustInstname2;
	}
	public void setCrossCustInstname2(String crossCustInstname2) {
		this.crossCustInstname2 = crossCustInstname2;
	}
	public BigDecimal getCrossCustAmt2() {
		return crossCustAmt2;
	}
	public void setCrossCustAmt2(BigDecimal crossCustAmt2) {
		this.crossCustAmt2 = crossCustAmt2;
	}
	public String getSecurityId() {
		return securityId;
	}
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}
	
	public List<CfetsTradeSubbond> getSubBondList() {
		return subBondList;
	}
	public void setSubBondList(List<CfetsTradeSubbond> subBondList) {
		this.subBondList = subBondList;
	}
	@Override
	public String toString() {
		return "CfetsTradePledge [tableId=" + tableId + ", tableCid=" + tableCid + ", totalFaceAmt=" + totalFaceAmt
				+ ", totalDiscountAmt=" + totalDiscountAmt + ", tradeProduct=" + tradeProduct + ", side=" + side
				+ ", repoMethod=" + repoMethod + ", tradeLimitDays=" + tradeLimitDays + ", realDays=" + realDays
				+ ", price=" + price + ", benchmarkInterestRate=" + benchmarkInterestRate + ", spread=" + spread
				+ ", tradeCashAmt=" + tradeCashAmt + ", settType=" + settType + ", deliveryType=" + deliveryType
				+ ", deliveryType2=" + deliveryType2 + ", startSettleDate=" + startSettleDate + ", endSettleDate="
				+ endSettleDate + ", clearingMethod=" + clearingMethod + ", settlCurrency=" + settlCurrency
				+ ", bondCurrency=" + bondCurrency + ", settlCurrFxrate=" + settlCurrFxrate + ", crossCustInstname1="
				+ crossCustInstname1 + ", crossCustAmt1=" + crossCustAmt1 + ", crossCustInstname2=" + crossCustInstname2
				+ ", crossCustAmt2=" + crossCustAmt2 + ", securityId=" + securityId + "]";
	}

	
}
