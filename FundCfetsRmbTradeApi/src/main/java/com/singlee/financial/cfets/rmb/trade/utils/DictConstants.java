package com.singlee.financial.cfets.rmb.trade.utils;

public class DictConstants {

	public final static class TrdType {
		public final static String SellRepoPledged = "0201";//质押式正回购
		public final static String ReverseRepoPledged = "0202";//质押式逆回购
		public final static String BondBuy = "0101";//现券买入
		public final static String BondSell = "0102";//现券卖出
	}
	
	public final static class YesNo {
		public final static String YES = "1";
		public final static String NO = "0";
	}
}
