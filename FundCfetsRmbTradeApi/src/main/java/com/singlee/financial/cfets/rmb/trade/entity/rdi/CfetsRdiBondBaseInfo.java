package com.singlee.financial.cfets.rmb.trade.entity.rdi;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 债券基本信息表
 */
@Table(name="CFETS_RDI_BOND_BASE_INFO")
@Entity
public class CfetsRdiBondBaseInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 债券代码 **/
    private String bondCode;

    /** 债券名称 **/
    private String bondName;

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    public String getBondName() {
        return bondName;
    }

    public void setBondName(String bondName) {
        this.bondName = bondName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CfetsRdiBondBaseInfo{");
        sb.append("bondCode='").append(bondCode).append('\'');
        sb.append(", bondName='").append(bondName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
