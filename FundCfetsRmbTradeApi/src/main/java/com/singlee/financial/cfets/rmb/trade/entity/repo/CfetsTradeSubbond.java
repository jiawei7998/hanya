package com.singlee.financial.cfets.rmb.trade.entity.repo;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 质押券
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_SUBBOND")
public class CfetsTradeSubbond implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tablePid;//外键CFETS_TRADE_PLEDGE
	private String tableCid;//外键CFETS_TRADE_PROGRESS
	private String bondCode;//债券编码
	private String bondName;//债券名称
	private BigDecimal faceAmt;//券面总额
	private String discountType;//折算比例
	private BigDecimal discountValue;//折算比例值
	private BigDecimal discountAmt;//折算金额
	private BigDecimal marketAmt;//估值净价(元)
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTablePid() {
		return tablePid;
	}
	public void setTablePid(String tablePid) {
		this.tablePid = tablePid;
	}
	public String getTableCid() {
		return tableCid;
	}
	public void setTableCid(String tableCid) {
		this.tableCid = tableCid;
	}
	public String getBondCode() {
		return bondCode;
	}
	public void setBondCode(String bondCode) {
		this.bondCode = bondCode;
	}
	public String getBondName() {
		return bondName;
	}
	public void setBondName(String bondName) {
		this.bondName = bondName;
	}
	public BigDecimal getFaceAmt() {
		return faceAmt;
	}
	public void setFaceAmt(BigDecimal faceAmt) {
		this.faceAmt = faceAmt;
	}
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	public BigDecimal getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}
	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}
	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}
	public BigDecimal getMarketAmt() {
		return marketAmt;
	}
	public void setMarketAmt(BigDecimal marketAmt) {
		this.marketAmt = marketAmt;
	}
	@Override
	public String toString() {
		return "CfetsTradesubbond [tableId=" + tableId + ", tablePid=" + tablePid + ", tableCid=" + tableCid
				+ ", bondCode=" + bondCode + ", bondName=" + bondName + ", faceAmt=" + faceAmt + ", discountType="
				+ discountType + ", discountValue=" + discountValue + ", discountAmt=" + discountAmt + ", marketAmt="
				+ marketAmt + "]";
	}


	
}
