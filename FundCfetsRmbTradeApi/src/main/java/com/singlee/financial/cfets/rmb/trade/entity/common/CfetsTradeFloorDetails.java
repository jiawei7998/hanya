package com.singlee.financial.cfets.rmb.trade.entity.common;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 分层回购
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_FLOOR_DETAILS")
public class CfetsTradeFloorDetails implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableCid;//外键CFETS_TRADE_PROGRESS
	private String fdId;//
	private BigDecimal fdPrice;//
	private String fdSubid253;//群组
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableCid() {
		return tableCid;
	}
	public void setTableCid(String tableCid) {
		this.tableCid = tableCid;
	}
	public String getFdId() {
		return fdId;
	}
	public void setFdId(String fdId) {
		this.fdId = fdId;
	}
	public BigDecimal getFdPrice() {
		return fdPrice;
	}
	public void setFdPrice(BigDecimal fdPrice) {
		this.fdPrice = fdPrice;
	}
	public String getFdSubid253() {
		return fdSubid253;
	}
	public void setFdSubid253(String fdSubid253) {
		this.fdSubid253 = fdSubid253;
	}
	@Override
	public String toString() {
		return "CfetsTradeFloorDetails [tableId=" + tableId + ", tableCid=" + tableCid + ", fdId=" + fdId + ", fdPrice="
				+ fdPrice + ", fdSubid253=" + fdSubid253 + "]";
	}


	
}
