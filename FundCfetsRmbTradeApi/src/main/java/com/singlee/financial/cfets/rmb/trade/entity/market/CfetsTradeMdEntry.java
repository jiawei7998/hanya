package com.singlee.financial.cfets.rmb.trade.entity.market;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeParty;

/**
 * 行情挡位信息
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_MD_ENTRY")
public class CfetsTradeMdEntry implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableCid;//外键CFETS_MARKET_DATA
	private String mdEntryType;//方向
	private BigDecimal mdPriceLevel;//挡位
	private BigDecimal mdEntryPx;//利率
	private BigDecimal mdEntrySize;//可成交量
	private BigDecimal tradeVolume;//总量
	private BigDecimal unMatchQty;//卖出未匹配量
	private String settType;//清算速度
	private BigDecimal lastPx;//净价
	private String yieldType;//到期收益率
	private BigDecimal yield;//到期收益率值
	private String mdEntryDate;//业务发生日期
	private String mdEntryTime;//业务发生时间
	private String partyId;//交易账号
	private String partyRole;//账号角色
	private String securityId;//债券代码
	private String symbol;//债券名称
	private String quoteType;//报价方式
	private String quoteId;//报价编号
	private String marketindicator;//市场标识
	private String settlDate;//结算日
	private String deliveryType;//结算方式
	private String settlCurrency;//结算币种
	private BigDecimal settlCurrFxrate;//汇率
	private String clearingMethod;//清算类型
	
	List<CfetsTradeParty> partyList;//交易机构信息
	
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableCid() {
		return tableCid;
	}
	public void setTableCid(String tableCid) {
		this.tableCid = tableCid;
	}
	public String getMdEntryType() {
		return mdEntryType;
	}
	public void setMdEntryType(String mdEntryType) {
		this.mdEntryType = mdEntryType;
	}
	public BigDecimal getMdPriceLevel() {
		return mdPriceLevel;
	}
	public void setMdPriceLevel(BigDecimal mdPriceLevel) {
		this.mdPriceLevel = mdPriceLevel;
	}
	public BigDecimal getMdEntryPx() {
		return mdEntryPx;
	}
	public void setMdEntryPx(BigDecimal mdEntryPx) {
		this.mdEntryPx = mdEntryPx;
	}
	public BigDecimal getMdEntrySize() {
		return mdEntrySize;
	}
	public void setMdEntrySize(BigDecimal mdEntrySize) {
		this.mdEntrySize = mdEntrySize;
	}
	public BigDecimal getTradeVolume() {
		return tradeVolume;
	}
	public void setTradeVolume(BigDecimal tradeVolume) {
		this.tradeVolume = tradeVolume;
	}
	public String getSettType() {
		return settType;
	}
	public void setSettType(String settType) {
		this.settType = settType;
	}
	public BigDecimal getLastPx() {
		return lastPx;
	}
	public void setLastPx(BigDecimal lastPx) {
		this.lastPx = lastPx;
	}
	public String getYieldType() {
		return yieldType;
	}
	public void setYieldType(String yieldType) {
		this.yieldType = yieldType;
	}
	public BigDecimal getYield() {
		return yield;
	}
	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}
	public String getMdEntryDate() {
		return mdEntryDate;
	}
	public void setMdEntryDate(String mdEntryDate) {
		this.mdEntryDate = mdEntryDate;
	}
	public String getMdEntryTime() {
		return mdEntryTime;
	}
	public void setMdEntryTime(String mdEntryTime) {
		this.mdEntryTime = mdEntryTime;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getPartyRole() {
		return partyRole;
	}
	public void setPartyRole(String partyRole) {
		this.partyRole = partyRole;
	}
	public String getSecurityId() {
		return securityId;
	}
	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	public String getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}
	public String getMarketindicator() {
		return marketindicator;
	}
	public void setMarketindicator(String marketindicator) {
		this.marketindicator = marketindicator;
	}
	public String getSettlDate() {
		return settlDate;
	}
	public void setSettlDate(String settlDate) {
		this.settlDate = settlDate;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getSettlCurrency() {
		return settlCurrency;
	}
	public void setSettlCurrency(String settlCurrency) {
		this.settlCurrency = settlCurrency;
	}
	public BigDecimal getSettlCurrFxrate() {
		return settlCurrFxrate;
	}
	public void setSettlCurrFxrate(BigDecimal settlCurrFxrate) {
		this.settlCurrFxrate = settlCurrFxrate;
	}
	
	public String getClearingMethod() {
		return clearingMethod;
	}
	public void setClearingMethod(String clearingMethod) {
		this.clearingMethod = clearingMethod;
	}
	public List<CfetsTradeParty> getPartyList() {
		return partyList;
	}
	public void setPartyList(List<CfetsTradeParty> partyList) {
		this.partyList = partyList;
	}
	public BigDecimal getUnMatchQty() {
		return unMatchQty;
	}
	public void setUnMatchQty(BigDecimal unMatchQty) {
		this.unMatchQty = unMatchQty;
	}
	@Override
	public String toString() {
		return "CfetsTradeMdEntry [tableId=" + tableId + ", tableCid=" + tableCid + ", mdEntryType=" + mdEntryType
				+ ", mdPriceLevel=" + mdPriceLevel + ", mdEntryPx=" + mdEntryPx + ", mdEntrySize=" + mdEntrySize
				+ ", tradeVolume=" + tradeVolume + ", settType=" + settType + ", lastPx=" + lastPx + ", yieldType="
				+ yieldType + ", yield=" + yield + ", mdEntryDate=" + mdEntryDate + ", mdEntryTime=" + mdEntryTime
				+ ", partyId=" + partyId + ", partyRole=" + partyRole + ", securityId=" + securityId + ", symbol="
				+ symbol + ", quoteType=" + quoteType + ", quoteId=" + quoteId + ", marketindicator=" + marketindicator
				+ ", settlDate=" + settlDate + ", deliveryType=" + deliveryType + ", settlCurrency=" + settlCurrency
				+ ", settlCurrFxrate=" + settlCurrFxrate + "]";
	}


	
}
