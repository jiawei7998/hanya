package com.singlee.financial.cfets.rmb.trade.utils;

import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

/**
 * 上行数据码值转换
 * @author xuqq
 *
 */
public class ConvertRmbTrade {
	
	public static String retSeccess = "error.common.0000";//返回成功
	public static String retError = "error.common.0001";//返回失败
	
	//TODO 待完成
	/**
	 * 9质押式回购，4现券买卖
	 */
	public static String[] marketIndicator = new String[] {"9,CR","4,CBT"};
	public static String[] quoteType = new String[] {"1,1"};
	public static String[] quoteTransType = new String[] {"N,N","R,R","1,1"};
	public static String[] quoteStatus = new String[] {"16,16","19,19","5,5","21,21","107,107","130,130"};
	public static String[] side = new String[] {"1,P","2,S"};
	public static String[] repoMethod = new String[] {"1,1","2,1"};
	public static String[] settlType = new String[] {"1,1"};
	public static String[] deliveryType = new String[] {"0,0","4,4","5,5"};
	
	/**
	 * 交易中心与本地码值转换
	 * @param key
	 * @param code
	 * @param flag：true:下行解析：转成本地码值，flase:上行组装：转成交易中心码值
	 * @return
	 */
	public static RetBean convertCode(String key,String[] code,boolean flag) {
		RetBean retBean = new RetBean(RetStatusEnum.S,retError , key);//默认转换失败
		for(int i=0;i<code.length;i++) {
			String[] temp = code[i].split(",");
			if(flag) {
				//下行解析：转成本地码值
				if(key.equals(temp[0])) {
					retBean.setRetCode(retSeccess);
					retBean.setRetMsg(temp[1]);
					break;
				}
			}else {
				//上行组装：转成交易中心码值
				if(key.equals(temp[1])) {
					retBean.setRetCode(retSeccess);
					retBean.setRetMsg(temp[0]);
					break;
				}
			}
		}
		return retBean;
	}
	
	public static String changeQuoteType(int type){
        String quoteType = null;
//        switch (type){
//            case QuoteType.TRADEABLE :
//                quoteType =  Constants.DialogQuote;
//                break;
//            case 113:
//                quoteType =  Constants.RequestQuote;
//                break;
//            case 107:
//                quoteType = Constants.MakeMarketQuote;
//                break;
//            case QuoteType.ORDER:
//                quoteType = Constants.ClickQuote;
//                break;
//            case 5:
//                quoteType = Constants.DialogQuote2;
//                break;
//            default:
//                quoteType =  Constants.DialogQuote;
//        }
        return quoteType;
    }
	
	public static final String convertCstp2ImsTradeDirection(String market,char code) {
//		switch(code){
//		case '1':
//			if ("4".equals(market)) { // 现券
//				return ExternalDictConstants.TrdType.BondBuy;
//			} else if ("9".equals(market)) { // 质押式回购
//				return ExternalDictConstants.TrdType.ReverseRepoPledged;
//			} else if ("10".equals(market)) { // 买断式回购
//				return ExternalDictConstants.TrdType.ReverseRepoOutright;
//			} else if ("1".equals(market)) { // 拆借
//				return ExternalDictConstants.TrdType.IbBorrowing;
//			} else if ("8".equals(market)) { // 债券借贷
//                return ExternalDictConstants.TrdType.SecuLendIn;
//            } else if ("5".equals(market)) { // 债券远期买入标的券
//				return ExternalDictConstants.TrdType.BDFBuy;
//			} else if ("3".equals(market)) { // 远期利率协议
//				return ExternalDictConstants.TrdType.ForwardRateAgreement;
//			}
//
//		case '4':
//			if ("4".equals(market)) { // 现券
//				return ExternalDictConstants.TrdType.BondSell;
//			} else if ("9".equals(market)) { // 质押式回购
//				return ExternalDictConstants.TrdType.SellRepoPledged;
//			} else if("10".equals(market)) { // 买断式回购
//				return ExternalDictConstants.TrdType.SellRepoOutright;
//			} else if ("1".equals(market)) { // 拆借
//				return ExternalDictConstants.TrdType.IbLending;
//			} else if ("8".equals(market)) { // 债券借贷
//                return ExternalDictConstants.TrdType.SecuLendOut;
//            } else if ("5".equals(market)) { // 债券远期卖出标的券
//				return ExternalDictConstants.TrdType.BDFSell;
//			} else if ("3".equals(market)) { // 远期利率协议
//				return ExternalDictConstants.TrdType.ForwardRateAgreement;
//			}
//		case 'J':
//			if ("2".equals(market)) {
//				return ExternalDictConstants.TrdType.InterestRateSwap;
//			}
//		case '2':
//			if ("4".equals(market)){
//				return ExternalDictConstants.TrdType.BondSell;
//			}
//		}
		return null;
	}
	
	public static final String convertCstp2ImsSettleMode(String market,int code) {
		switch(code){
		case 0:
			return "DVP";
		case 4:
			return "PAD";
		case 5:
			return "DAP";
        case 6:
            return "BVB";
        case 8:
            return "BVBF";
        case 9:
            return "BVP";
		case 11:
			return "NDVP";
        case 99:
            return "NOTDVP";
		}
		return null;
	}
	
}
