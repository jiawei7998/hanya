package com.singlee.financial.cfets.rmb.trade.entity.rdi;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * 本币银行间交易成员信息
 */
@Table(name="CFETS_RDI_TRADE_MEMBER")
@Entity
public class CfetsRdiTradeMember implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 交易成员ID **/
    private String partySubId;

    /** 机构代码 **/
    private String partyId;

    /**  交易成员全称 **/
    private String cnFullname;

    /** 中文简称 **/
    private String cnShortname;

    /** 英文全称 **/
    private String enFullname;

    /** 英文简称 **/
    private String enShortname;

    /** 交易员代码 **/
    @Transient
    private String tradeUserId;

    /** 交易员姓名 **/
    @Transient
    private String tradeUserName;

    /** 联系电话 **/
    @Transient
    private String contactPhone;

    /** 传真 **/
    @Transient
    private String fax;

    /** email **/
    @Transient
    private String email;

    /** msn **/
    @Transient
    private String msn;

    /** 通讯地址 **/
    @Transient
    private String contactAddress;

    /** 邮政编码 **/
    @Transient
    private String postCode;

    public String getPartySubId() {
        return partySubId;
    }

    public void setPartySubId(String partySubId) {
        this.partySubId = partySubId;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getCnFullname() {
        return cnFullname;
    }

    public void setCnFullname(String cnFullname) {
        this.cnFullname = cnFullname;
    }

    public String getCnShortname() {
        return cnShortname;
    }

    public void setCnShortname(String cnShortname) {
        this.cnShortname = cnShortname;
    }

    public String getEnFullname() {
        return enFullname;
    }

    public void setEnFullname(String enFullname) {
        this.enFullname = enFullname;
    }

    public String getEnShortname() {
        return enShortname;
    }

    public void setEnShortname(String enShortname) {
        this.enShortname = enShortname;
    }

    public String getTradeUserId() {
        return tradeUserId;
    }

    public void setTradeUserId(String tradeUserId) {
        this.tradeUserId = tradeUserId;
    }

    public String getTradeUserName() {
        return tradeUserName;
    }

    public void setTradeUserName(String tradeUserName) {
        this.tradeUserName = tradeUserName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CfetsRdiTradeMember{");
        sb.append("partySubId='").append(partySubId).append('\'');
        sb.append(", partyId='").append(partyId).append('\'');
        sb.append(", cnFullname='").append(cnFullname).append('\'');
        sb.append(", cnShortname='").append(cnShortname).append('\'');
        sb.append(", enFullname='").append(enFullname).append('\'');
        sb.append(", enShortname='").append(enShortname).append('\'');
        sb.append(", tradeUserId='").append(tradeUserId).append('\'');
        sb.append(", tradeUserName='").append(tradeUserName).append('\'');
        sb.append(", contactPhone='").append(contactPhone).append('\'');
        sb.append(", fax='").append(fax).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", msn='").append(msn).append('\'');
        sb.append(", contactAddress='").append(contactAddress).append('\'');
        sb.append(", postCode='").append(postCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

