package com.singlee.financial.cfets.rmb.trade.entity.rdi;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 交易成员通讯录信息
 */
@Table(name="CFETS_RDI_TRADE_MEMBER_CONTACT")
@Entity
public class CfetsRdiTradeMemberContact implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 交易成员id **/
    private String partyId;

    /** 机构名称 **/
    private String instName;

    /** 交易员id **/
    private String tradeUserId;

    /** 交易员姓名 **/
    private String tradeUserName;

    /** 联系电话 **/
    private String contactPhone;

    /** 传真 **/
    private String fax;

    /** EMAIL **/
    private String email;

    /** MSN **/
    private String msn;

    /** 通讯地址 **/
    private String contactAddress;

    /** 邮政编码 **/
    private String postCode;

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getInstName() {
        return instName;
    }

    public void setInstName(String instName) {
        this.instName = instName;
    }

    public String getTradeUserId() {
        return tradeUserId;
    }

    public void setTradeUserId(String tradeUserId) {
        this.tradeUserId = tradeUserId;
    }

    public String getTradeUserName() {
        return tradeUserName;
    }

    public void setTradeUserName(String tradeUserName) {
        this.tradeUserName = tradeUserName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RdiCfetsTradeMemberContact{");
        sb.append("partyId='").append(partyId).append('\'');
        sb.append(", instName='").append(instName).append('\'');
        sb.append(", tradeUserId='").append(tradeUserId).append('\'');
        sb.append(", tradeUserName='").append(tradeUserName).append('\'');
        sb.append(", contactPhone='").append(contactPhone).append('\'');
        sb.append(", fax='").append(fax).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", msn='").append(msn).append('\'');
        sb.append(", contactAddress='").append(contactAddress).append('\'');
        sb.append(", postCode='").append(postCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
