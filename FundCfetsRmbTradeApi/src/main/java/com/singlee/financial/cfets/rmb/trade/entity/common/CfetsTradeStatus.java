package com.singlee.financial.cfets.rmb.trade.entity.common;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 交易时段对象
 * 报文类型(h)
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_STATUS")
public class CfetsTradeStatus implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableRid;//外键CFETS_TRADE_PACKAGE
	private String marketindicator;//市场标识
	private String tradSesStatus;//交易时段状态
	private String tradeMethod;//交易方式
	private String tradSesStartTime;//时间
	private String tradSesOpenTime;//时间
	private String tradSesCloseTime;//时间
	private String tradSesEndTime;//时间
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTableRid() {
		return tableRid;
	}
	public void setTableRid(String tableRid) {
		this.tableRid = tableRid;
	}
	public String getMarketindicator() {
		return marketindicator;
	}
	public void setMarketindicator(String marketindicator) {
		this.marketindicator = marketindicator;
	}
	public String getTradSesStatus() {
		return tradSesStatus;
	}
	public void setTradSesStatus(String tradSesStatus) {
		this.tradSesStatus = tradSesStatus;
	}
	public String getTradeMethod() {
		return tradeMethod;
	}
	public void setTradeMethod(String tradeMethod) {
		this.tradeMethod = tradeMethod;
	}
	public String getTradSesStartTime() {
		return tradSesStartTime;
	}
	public void setTradSesStartTime(String tradSesStartTime) {
		this.tradSesStartTime = tradSesStartTime;
	}
	public String getTradSesOpenTime() {
		return tradSesOpenTime;
	}
	public void setTradSesOpenTime(String tradSesOpenTime) {
		this.tradSesOpenTime = tradSesOpenTime;
	}
	public String getTradSesCloseTime() {
		return tradSesCloseTime;
	}
	public void setTradSesCloseTime(String tradSesCloseTime) {
		this.tradSesCloseTime = tradSesCloseTime;
	}
	public String getTradSesEndTime() {
		return tradSesEndTime;
	}
	public void setTradSesEndTime(String tradSesEndTime) {
		this.tradSesEndTime = tradSesEndTime;
	}
	@Override
	public String toString() {
		return "CfetsTradeStatus [tableId=" + tableId + ", tableRid=" + tableRid + ", marketindicator="
				+ marketindicator + ", tradSesStatus=" + tradSesStatus + ", tradeMethod=" + tradeMethod
				+ ", tradSesStartTime=" + tradSesStartTime + ", tradSesOpenTime=" + tradSesOpenTime
				+ ", tradSesCloseTime=" + tradSesCloseTime + ", tradSesEndTime=" + tradSesEndTime + "]";
	}

	
}
