package com.singlee.financial.cfets.rmb.trade;

import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeCommonOther;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradePackage;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeProgress;
import com.singlee.financial.cfets.rmb.trade.entity.common.CfetsTradeStatus;
import com.singlee.financial.cfets.rmb.trade.entity.market.CfetsTradeMarketData;
import com.singlee.financial.cfets.rmb.trade.entity.rdi.*;
import com.singlee.financial.pojo.RetBean;
import com.singlee.hessian.annotation.CfetsRmbTrade;
import com.singlee.hessian.annotation.CfetsRmbTradeContext;
import org.springframework.remoting.RemoteConnectFailureException;

import java.util.List;

@CfetsRmbTrade(context = CfetsRmbTradeContext.SINGLEE_API, uri = "/IImixCfetsTradeProgressExporter")
public interface IImixCfetsTradeProgress {
	/**
	 * 上行交互：接收交易中心解析后的报文对象
	 * @param cfetsTradeProgress
	 * @param cfetsTradePackage
	 * @return
	 * @throws RemoteConnectFailureException
	 * @throws Exception
	 */
	RetBean sendRMBTrade(CfetsTradeProgress cfetsTradeProgress,CfetsTradePackage cfetsTradePackage) throws  Exception;
	RetBean sendRMBTrade(CfetsTradeStatus cfetsTradeStatus,CfetsTradePackage cfetsTradePackage) throws  Exception;
	RetBean sendRMBTrade(CfetsTradeMarketData cfetsTradeMarketData,CfetsTradePackage cfetsTradePackage) throws  Exception;
	RetBean sendRMBTrade(CfetsTradeCommonOther cfetsTradeCommonOther,CfetsTradePackage cfetsTradePackage) throws  Exception;

	RetBean sendRdiCfetsRdiBondBaseInfo(List<CfetsRdiBondBaseInfo> list) throws  Exception;
	RetBean sendRdiCfetsRdiTradeMemberContact(List<CfetsRdiTradeMemberContact> list) throws  Exception;
	RetBean sendRdiCfetsRdiTradeMember(List<CfetsRdiTradeMember> list) throws  Exception;
	RetBean sendRdiCfetsRdiXbondBaseInfo(List<CfetsRdiXbondBaseInfo> list) throws  Exception;
	RetBean sendRdiCfetsRdiLog(CfetsRdiLog cfetsRdiLog) throws  Exception;

}
