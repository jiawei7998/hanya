package com.singlee.financial.cfets.rmb.trade.entity.rdi;

import javax.persistence.*;
import java.io.Serializable;

/**
 * RDI相关定时任务日志类
 */
@Table(name="CFETS_RDI_LOG")
@Entity
public class CfetsRdiLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "SELECT SEQ_CFETS_RDI_LOG.NEXTVAL FROM DUAL")
    private String id;

    /** 操作时间 **/
    private String operateTime;

    /** 操作日期 **/
    private String operateDate;

    /** 操作路径 */
    private String targetPath;

    /** 定时任务对应的时间点 **/
    private String jobTime;

    /** 操作步骤 **/
    private int stepId;

    /** 操作状态 **/
    private String status;

    /** 操作日志 **/
    private String logs;

    public String getOperateDate() {
        return operateDate;
    }

    public void setOperateDate(String operateDate) {
        this.operateDate = operateDate;
    }

    public String getJobTime() {
        return jobTime;
    }

    public void setJobTime(String jobTime) {
        this.jobTime = jobTime;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int stepId) {
        this.stepId = stepId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLogs() {
        return logs;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(String operateTime) {
        this.operateTime = operateTime;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CfetsRdiLog{");
        sb.append("id='").append(id).append('\'');
        sb.append("operateTime='").append(operateTime).append('\'');
        sb.append(", operateDate='").append(operateDate).append('\'');
        sb.append(", targetPath='").append(targetPath).append('\'');
        sb.append(", jobTime='").append(jobTime).append('\'');
        sb.append(", stepId=").append(stepId);
        sb.append(", status='").append(status).append('\'');
        sb.append(", logs='").append(logs).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
