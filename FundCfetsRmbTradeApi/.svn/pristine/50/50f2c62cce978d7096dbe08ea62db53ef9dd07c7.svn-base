package com.singlee.financial.cfets.rmb.trade.entity.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.singlee.financial.cfets.rmb.trade.entity.cbt.CfetsTradeBond;
import com.singlee.financial.cfets.rmb.trade.entity.repo.CfetsTradeBondPledge;

/**
 * 报文解析公用表
 * @author xuqq
 *
 */
@Entity
@Table(name="CFETS_TRADE_PROGRESS")
public class CfetsTradeProgress implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String tableId;//主键
	private String tableRid;//外键CFETS_TRADE_PACKAGE
	private String tableDid;//业务外键FLOW_TRADE
	private String isCurrent;//是否当前
	private String upordown;//上行或下行
	private String retCode;//返回码
	private String retMsg;//提示信息
	private String sendFlag;//发送标识：0未发送，1发送成功，2发送失败，3报价成功，4报价失败，5过期
	private String msgType;//报文类型
	private String marketindicator;//市场标识
	private String tradeRecordid;//交易记录编号
	private String channel;//渠道
	private String channelType;//渠道类型
	private String quoteType;//报价类别
	private String quoteTransType;//操作类型
	private String quoteStatus;//报价状态
	private String quoteStatusDesc;//报价状态说明
	private String clOrdId;//客户参考编号
	private String queryRequestId;//查询请求编号
	private String origClOrdId;//原客户参考编号
	private String quoteReqId;//请求报价编号
	private String quoteId;//报价编号
	private String pledgeSubmissionCode;//提券编号
	private String transactTime;//业务发生时间
	private String quoteTime;//报价发生时间
	private String negotiationCount;//交谈轮次
	private String userReference1;//用户参考数据1
	private String userReference2;//用户参考数据2
	private String userReference3;//用户参考数据3
	private String userReference4;//用户参考数据4
	private String userReference5;//用户参考数据5
	private String userReference6;//用户参考数据6
	private String execId;//成交编号
	private String tradeMethod;//交易方式
	private String execType;//成交状态
	private String tradeDate;//成交日期yyyymmdd
	private String tradeTime;//成交时间
	private BigDecimal lastPx;//成交利率
	private BigDecimal lastQty;//成交金额
	private BigDecimal leavesQty;//剩余金额
	private String text;//备注
	private String anonymousIndicator;//匿名标识
	private String conIndicator;//应急标识
	private String validUntilTime;//有效时间
	private String routingType;//发送对象
	private String allocInd;//分账标识
	private String dateConfirmed;//场次日期
	
	List<CfetsTradeBond> bondList;//现券买卖
	CfetsTradeBondPledge cfetsTradeBondPledge;//质押式回购
	List<CfetsTradeParty> partyList;//交易机构信息
	List<CfetsTradeFloorDetails> fdList;//分层回购
	List<CfetsTradeAllocs> allocsList;//分账表

	public CfetsTradeProgress() {
		super();
		this.sendFlag="0";
		this.isCurrent="0";//默认无效
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getTableRid() {
		return tableRid;
	}

	public void setTableRid(String tableRid) {
		this.tableRid = tableRid;
	}

	public String getTableDid() {
		return tableDid;
	}

	public void setTableDid(String tableDid) {
		this.tableDid = tableDid;
	}

	public String getIsCurrent() {
		return isCurrent;
	}

	public void setIsCurrent(String isCurrent) {
		this.isCurrent = isCurrent;
	}

	public String getUpordown() {
		return upordown;
	}

	public void setUpordown(String upordown) {
		this.upordown = upordown;
	}

	public String getRetCode() {
		return retCode;
	}

	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}

	public String getRetMsg() {
		return retMsg;
	}

	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}

	public String getSendFlag() {
		return sendFlag;
	}

	public void setSendFlag(String sendFlag) {
		this.sendFlag = sendFlag;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMarketindicator() {
		return marketindicator;
	}

	public void setMarketindicator(String marketindicator) {
		this.marketindicator = marketindicator;
	}

	public String getTradeRecordid() {
		return tradeRecordid;
	}

	public void setTradeRecordid(String tradeRecordid) {
		this.tradeRecordid = tradeRecordid;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	public String getQuoteType() {
		return quoteType;
	}

	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}

	public String getQuoteTransType() {
		return quoteTransType;
	}

	public void setQuoteTransType(String quoteTransType) {
		this.quoteTransType = quoteTransType;
	}

	public String getQuoteStatus() {
		return quoteStatus;
	}

	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}

	public String getQuoteStatusDesc() {
		return quoteStatusDesc;
	}

	public void setQuoteStatusDesc(String quoteStatusDesc) {
		this.quoteStatusDesc = quoteStatusDesc;
	}

	public String getClOrdId() {
		return clOrdId;
	}

	public void setClOrdId(String clOrdId) {
		this.clOrdId = clOrdId;
	}

	public String getQueryRequestId() {
		return queryRequestId;
	}

	public void setQueryRequestId(String queryRequestId) {
		this.queryRequestId = queryRequestId;
	}

	public String getOrigClOrdId() {
		return origClOrdId;
	}

	public void setOrigClOrdId(String origClOrdId) {
		this.origClOrdId = origClOrdId;
	}

	public String getQuoteReqId() {
		return quoteReqId;
	}

	public void setQuoteReqId(String quoteReqId) {
		this.quoteReqId = quoteReqId;
	}

	public String getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(String quoteId) {
		this.quoteId = quoteId;
	}

	public String getPledgeSubmissionCode() {
		return pledgeSubmissionCode;
	}

	public void setPledgeSubmissionCode(String pledgeSubmissionCode) {
		this.pledgeSubmissionCode = pledgeSubmissionCode;
	}

	public String getTransactTime() {
		return transactTime;
	}

	public void setTransactTime(String transactTime) {
		this.transactTime = transactTime;
	}

	public String getQuoteTime() {
		return quoteTime;
	}

	public void setQuoteTime(String quoteTime) {
		this.quoteTime = quoteTime;
	}

	public String getNegotiationCount() {
		return negotiationCount;
	}

	public void setNegotiationCount(String negotiationCount) {
		this.negotiationCount = negotiationCount;
	}

	public String getUserReference1() {
		return userReference1;
	}

	public void setUserReference1(String userReference1) {
		this.userReference1 = userReference1;
	}

	public String getUserReference2() {
		return userReference2;
	}

	public void setUserReference2(String userReference2) {
		this.userReference2 = userReference2;
	}

	public String getUserReference3() {
		return userReference3;
	}

	public void setUserReference3(String userReference3) {
		this.userReference3 = userReference3;
	}

	public String getUserReference4() {
		return userReference4;
	}

	public void setUserReference4(String userReference4) {
		this.userReference4 = userReference4;
	}

	public String getUserReference5() {
		return userReference5;
	}

	public void setUserReference5(String userReference5) {
		this.userReference5 = userReference5;
	}

	public String getUserReference6() {
		return userReference6;
	}

	public void setUserReference6(String userReference6) {
		this.userReference6 = userReference6;
	}

	public String getExecId() {
		return execId;
	}

	public void setExecId(String execId) {
		this.execId = execId;
	}

	public String getTradeMethod() {
		return tradeMethod;
	}

	public void setTradeMethod(String tradeMethod) {
		this.tradeMethod = tradeMethod;
	}

	public String getExecType() {
		return execType;
	}

	public void setExecType(String execType) {
		this.execType = execType;
	}

	public String getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAnonymousIndicator() {
		return anonymousIndicator;
	}

	public void setAnonymousIndicator(String anonymousIndicator) {
		this.anonymousIndicator = anonymousIndicator;
	}

	public String getConIndicator() {
		return conIndicator;
	}

	public void setConIndicator(String conIndicator) {
		this.conIndicator = conIndicator;
	}

	public String getValidUntilTime() {
		return validUntilTime;
	}

	public void setValidUntilTime(String validUntilTime) {
		this.validUntilTime = validUntilTime;
	}

	public String getRoutingType() {
		return routingType;
	}

	public void setRoutingType(String routingType) {
		this.routingType = routingType;
	}

	public String getAllocInd() {
		return allocInd;
	}

	public void setAllocInd(String allocInd) {
		this.allocInd = allocInd;
	}

	public String getDateConfirmed() {
		return dateConfirmed;
	}

	public void setDateConfirmed(String dateConfirmed) {
		this.dateConfirmed = dateConfirmed;
	}

	public List<CfetsTradeBond> getBondList() {
		return bondList;
	}

	public void setBondList(List<CfetsTradeBond> bondList) {
		this.bondList = bondList;
	}

	public CfetsTradeBondPledge getCfetsTradeBondPledge() {
		return cfetsTradeBondPledge;
	}

	public void setCfetsTradeBondPledge(CfetsTradeBondPledge cfetsTradeBondPledge) {
		this.cfetsTradeBondPledge = cfetsTradeBondPledge;
	}

	public List<CfetsTradeParty> getPartyList() {
		return partyList;
	}

	public void setPartyList(List<CfetsTradeParty> partyList) {
		this.partyList = partyList;
	}

	public List<CfetsTradeFloorDetails> getFdList() {
		return fdList;
	}

	public void setFdList(List<CfetsTradeFloorDetails> fdList) {
		this.fdList = fdList;
	}

	public List<CfetsTradeAllocs> getAllocsList() {
		return allocsList;
	}

	public void setAllocsList(List<CfetsTradeAllocs> allocsList) {
		this.allocsList = allocsList;
	}

	public BigDecimal getLastPx() {
		return lastPx;
	}

	public void setLastPx(BigDecimal lastPx) {
		this.lastPx = lastPx;
	}

	public BigDecimal getLastQty() {
		return lastQty;
	}

	public void setLastQty(BigDecimal lastQty) {
		this.lastQty = lastQty;
	}

	public BigDecimal getLeavesQty() {
		return leavesQty;
	}

	public void setLeavesQty(BigDecimal leavesQty) {
		this.leavesQty = leavesQty;
	}

	@Override
	public String toString() {
		return "CfetsTradeProgress [tableId=" + tableId + ", tableRid=" + tableRid + ", tableDid=" + tableDid
				+ ", isCurrent=" + isCurrent + ", upordown=" + upordown + ", retCode=" + retCode + ", retMsg=" + retMsg
				+ ", sendFlag=" + sendFlag + ", msgType=" + msgType + ", marketindicator=" + marketindicator
				+ ", tradeRecordid=" + tradeRecordid + ", channel=" + channel + ", channelType=" + channelType
				+ ", quoteType=" + quoteType + ", quoteTransType=" + quoteTransType + ", quoteStatus=" + quoteStatus
				+ ", quoteStatusDesc=" + quoteStatusDesc + ", clOrdId=" + clOrdId + ", queryRequestId=" + queryRequestId
				+ ", origClOrdId=" + origClOrdId + ", quoteReqId=" + quoteReqId + ", quoteId=" + quoteId
				+ ", pledgeSubmissionCode=" + pledgeSubmissionCode + ", transactTime=" + transactTime + ", quoteTime="
				+ quoteTime + ", negotiationCount=" + negotiationCount + ", userReference1=" + userReference1
				+ ", userReference2=" + userReference2 + ", userReference3=" + userReference3 + ", userReference4="
				+ userReference4 + ", userReference5=" + userReference5 + ", userReference6=" + userReference6
				+ ", execId=" + execId + ", tradeMethod=" + tradeMethod + ", execType=" + execType + ", tradeDate="
				+ tradeDate + ", tradeTime=" + tradeTime + ", text=" + text + ", anonymousIndicator="
				+ anonymousIndicator + ", conIndicator=" + conIndicator + ", validUntilTime=" + validUntilTime
				+ ", routingType=" + routingType + ", allocInd=" + allocInd + ", dateConfirmed=" + dateConfirmed + "]";
	}
	
	
}
