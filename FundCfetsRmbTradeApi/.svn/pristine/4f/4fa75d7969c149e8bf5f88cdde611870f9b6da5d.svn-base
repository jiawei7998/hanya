package com.singlee.financial.cfets.rmb.trade.entity.rdi;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName CfetsRdiXbondBaseInfo
 * @Description X-Bond可交易债券信息表
 * @Author qgh
 * @Date 2020-04-09 17:32
 **/
@Table(name="CFETS_RDI_XBOND_BASE_INFO")
@Entity
public class CfetsRdiXbondBaseInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 债券代码 **/
    private String bondId;

    /** 债券名称 **/
    private String sym;

    /**  债券类型 **/
    private String bondDesc;

    /** 是否集中报价债券 **/
    private String centraQuoteBondIndic;

    /** 债券类型代码 **/
    private String securityTypeId;

    /** 标准代偿期 **/
    private String termToMaturityString;

    /** 发行人代码 **/
    private String partySubId;

    public String getBondId() {
        return bondId;
    }

    public void setBondId(String bondId) {
        this.bondId = bondId;
    }

    public String getSym() {
        return sym;
    }

    public void setSym(String sym) {
        this.sym = sym;
    }

    public String getBondDesc() {
        return bondDesc;
    }

    public void setBondDesc(String bondDesc) {
        this.bondDesc = bondDesc;
    }

    public String getCentraQuoteBondIndic() {
        return centraQuoteBondIndic;
    }

    public void setCentraQuoteBondIndic(String centraQuoteBondIndic) {
        this.centraQuoteBondIndic = centraQuoteBondIndic;
    }

    public String getSecurityTypeId() {
        return securityTypeId;
    }

    public void setSecurityTypeId(String securityTypeId) {
        this.securityTypeId = securityTypeId;
    }

    public String getTermToMaturityString() {
        return termToMaturityString;
    }

    public void setTermToMaturityString(String termToMaturityString) {
        this.termToMaturityString = termToMaturityString;
    }

    public String getPartySubId() {
        return partySubId;
    }

    public void setPartySubId(String partySubId) {
        this.partySubId = partySubId;
    }

    @Override
    public String toString() {
        return "CfetsRdiXbondBaseInfo{" +
                "bondId='" + bondId + '\'' +
                ", sym='" + sym + '\'' +
                ", bondDesc='" + bondDesc + '\'' +
                ", centraQuoteBondIndic='" + centraQuoteBondIndic + '\'' +
                ", securityTypeId='" + securityTypeId + '\'' +
                ", termToMaturityString='" + termToMaturityString + '\'' +
                ", partySubId='" + partySubId + '\'' +
                '}';
    }
}

