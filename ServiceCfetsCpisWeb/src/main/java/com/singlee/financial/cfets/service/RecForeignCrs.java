package com.singlee.financial.cfets.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecForeignCrsModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.AccountCurrency;
import imix.field.CalculateAgency;
import imix.field.CnfmTime;
import imix.field.CnfmTimeType;
import imix.field.ConfirmID;
import imix.field.ConfirmStatus;
import imix.field.DateAdjustmentIndic;
import imix.field.ExecID;
import imix.field.ExecType;
import imix.field.FinalExDate;
import imix.field.IniExDate;
import imix.field.LastSpotRate;
import imix.field.LegBenchmarkCurveName;
import imix.field.LegBenchmarkSpread;
import imix.field.LegBenchmarkTenor;
import imix.field.LegCouponPaymentDate;
import imix.field.LegCouponPaymentDateReset;
import imix.field.LegCouponPaymentFrequency;
import imix.field.LegCurrency;
import imix.field.LegDayCount;
import imix.field.LegInterestAccrualResetFrequency;
import imix.field.LegInterestFixDateAdjustment;
import imix.field.LegOrderQty;
import imix.field.LegPrice;
import imix.field.LegPriceType;
import imix.field.LegSign;
import imix.field.LegStubIndicator;
import imix.field.MarketIndicator;
import imix.field.MaturityDate;
import imix.field.NetGrossInd;
import imix.field.NoPartyIDs;
import imix.field.NoPartySubIDs;
import imix.field.NotionalExchangeType;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.SettlDate;
import imix.field.Text;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;
/**
 * 
 * <P>RecForeignSwap.java</P>
 * <P>接收外币掉期的推送消息处理类</P>
 * <P>Company: 新利</P>
 * <P>          修改者姓名 修改内容说明</P>
 * @author 
 * @see     参考类1
 */
public class RecForeignCrs {

	/**
	 * 解析报文
	 * @param message
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	private static Logger LogManager = LoggerFactory.getLogger(RecForeignCrs.class);
	public RecForeignCrsModel ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		RecForeignCrsModel recForeignCrsModel = new RecForeignCrsModel();
		MessageUtil msgUtil = new MessageUtil(message);
		
		try {
			recForeignCrsModel.setConfirmid(msgUtil.getStringValue(ConfirmID.FIELD));
			recForeignCrsModel.setExecType(msgUtil.getStringValue(ExecType.FIELD));
			recForeignCrsModel.setConfirmStatus(msgUtil.getStringValue(ConfirmStatus.FIELD));
			recForeignCrsModel.setMarketIndicator(msgUtil.getStringValue(MarketIndicator.FIELD));
			recForeignCrsModel.setExecId(msgUtil.getStringValue(ExecID.FIELD));
			recForeignCrsModel.setTradedate(msgUtil.getStringValue(TradeDate.FIELD));
			recForeignCrsModel.setSettldate(msgUtil.getStringValue(SettlDate.FIELD));
			recForeignCrsModel.setNetGrossInd(msgUtil.getStringValue(NetGrossInd.FIELD));
			recForeignCrsModel.setMaturitydate(msgUtil.getStringValue(MaturityDate.FIELD));
			recForeignCrsModel.setDateadjustmentindic(msgUtil.getStringValue(DateAdjustmentIndic.FIELD));
			recForeignCrsModel.setIniexdate(msgUtil.getStringValue(IniExDate.FIELD));
			recForeignCrsModel.setFinalexdate(msgUtil.getStringValue(FinalExDate.FIELD));
			recForeignCrsModel.setCalculateagency(msgUtil.getStringValue(CalculateAgency.FIELD));
			recForeignCrsModel.setNotionalexchangetype(msgUtil.getStringValue(NotionalExchangeType.FIELD));
			recForeignCrsModel.setLastspotrate(msgUtil.getStringValue(LastSpotRate.FIELD));
			recForeignCrsModel.setText(msgUtil.getStringValue(Text.FIELD));
			
			NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
			CnfmTimeType cnfmTimeType = new CnfmTimeType();
			CnfmTime cnfmTime = new CnfmTime();
			
			String ownCnfmTime = "";
			String othCnfmTime = "";
				
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
				message.getGroup(i, noCnfmTimes);
				cnfmTimeType = noCnfmTimes.getCnfmTimeType();
				cnfmTime = noCnfmTimes.getCnfmTime();
				if (cnfmTimeType.getValue() == 1) {
					ownCnfmTime = cnfmTime.getValue();
				}else if (cnfmTimeType.getValue() == 2) {
					othCnfmTime = cnfmTime.getValue();
				}
			}
			recForeignCrsModel.setOwnCnfmTime(ownCnfmTime);
			recForeignCrsModel.setOthCnfmTime(othCnfmTime);
			
			ExecutionReport.NoLegs noLegs = new ExecutionReport.NoLegs();
			
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoLegs.FIELD); i++) {
				message.getGroup(i, noLegs);
				//本方
				if("1".equals(noLegs.getString(LegSign.FIELD))){
					recForeignCrsModel.setLegstubindicator_1(msgUtil.getStringValue(noLegs,LegStubIndicator.FIELD));
					
					recForeignCrsModel.setLegcouponpaymentdate_1(msgUtil.getStringValue(noLegs,LegCouponPaymentDate.FIELD));
					
					recForeignCrsModel.setLegcurrency_1(msgUtil.getStringValue(noLegs,LegCurrency.FIELD));
					
					recForeignCrsModel.setLegorderqty_1(msgUtil.getStringValue(noLegs,LegOrderQty.FIELD));
					
					recForeignCrsModel.setLegpricetype_1(msgUtil.getStringValue(noLegs,LegPriceType.FIELD));
					
					recForeignCrsModel.setLegbenchmarkcurvename_1(msgUtil.getStringValue(noLegs,LegBenchmarkCurveName.FIELD));
					
					recForeignCrsModel.setLegbenchmarktenor_1(msgUtil.getStringValue(noLegs,LegBenchmarkTenor.FIELD));
					
					recForeignCrsModel.setLegbenchmarkspread_1(msgUtil.getStringValue(noLegs,LegBenchmarkSpread.FIELD));
					
					recForeignCrsModel.setLegprice_1(msgUtil.getStringValue(noLegs,LegPrice.FIELD));
					
					recForeignCrsModel.setLegintaccresetfrequency_1(msgUtil.getStringValue(noLegs,LegInterestAccrualResetFrequency.FIELD));
					
					recForeignCrsModel.setLegintfixdateadjustment_1(msgUtil.getStringValue(noLegs,LegInterestFixDateAdjustment.FIELD));
					
					recForeignCrsModel.setLegdaycount_1(msgUtil.getStringValue(noLegs,LegDayCount.FIELD));
					
					recForeignCrsModel.setLegcouponpaymentfrequency_1(msgUtil.getStringValue(noLegs,LegCouponPaymentFrequency.FIELD));
					
					recForeignCrsModel.setLegcouponpaymentdatereset_1(msgUtil.getStringValue(noLegs,LegCouponPaymentDateReset.FIELD));
					
				}
				//对手方
				if ("2".equals(noLegs.getString(LegSign.FIELD))) {
					recForeignCrsModel.setLegstubindicator_2(msgUtil.getStringValue(noLegs,LegStubIndicator.FIELD));
					
					recForeignCrsModel.setLegcouponpaymentdate_2(msgUtil.getStringValue(noLegs,LegCouponPaymentDate.FIELD));
					
					recForeignCrsModel.setLegcurrency_2(msgUtil.getStringValue(noLegs,LegCurrency.FIELD));
					
					recForeignCrsModel.setLegorderqty_2(msgUtil.getStringValue(noLegs,LegOrderQty.FIELD));
					
					recForeignCrsModel.setLegpricetype_2(msgUtil.getStringValue(noLegs,LegPriceType.FIELD));
					
					recForeignCrsModel.setLegbenchmarkcurvename_2(msgUtil.getStringValue(noLegs,LegBenchmarkCurveName.FIELD));
					
					recForeignCrsModel.setLegbenchmarktenor_2(msgUtil.getStringValue(noLegs,LegBenchmarkTenor.FIELD));
					
					recForeignCrsModel.setLegbenchmarkspread_2(msgUtil.getStringValue(noLegs,LegBenchmarkSpread.FIELD));
					
					recForeignCrsModel.setLegprice_2(msgUtil.getStringValue(noLegs,LegPrice.FIELD));
					
					recForeignCrsModel.setLegintaccresetfrequency_2(msgUtil.getStringValue(noLegs,LegInterestAccrualResetFrequency.FIELD));
					
					recForeignCrsModel.setLegintfixdateadjustment_2(msgUtil.getStringValue(noLegs,LegInterestFixDateAdjustment.FIELD));
					
					recForeignCrsModel.setLegdaycount_2(msgUtil.getStringValue(noLegs,LegDayCount.FIELD));
					
					recForeignCrsModel.setLegcouponpaymentfrequency_2(msgUtil.getStringValue(noLegs,LegCouponPaymentFrequency.FIELD));
					
					recForeignCrsModel.setLegcouponpaymentdatereset_2(msgUtil.getStringValue(noLegs,LegCouponPaymentDateReset.FIELD));
					
				}
			}
			
			String accountCurrency = "";
			int partyrole=-1; //本方对手方
			
			String partyid="";//21位机构码
			
			String partysubid="";
		    
		    int partysubidtype=-1;
			
			ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
			ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			int nopartysubids=-1;//清算信息循环组
			
			for (int k = 1; k <= msgUtil.getIntValue(NoPartyIDs.FIELD); k++) {
				message.getGroup(k, nopartyids1);
				
				partyrole=msgUtil.getIntValue(nopartyids1, PartyRole.FIELD);

				partyid=msgUtil.getStringValue(nopartyids1, PartyID.FIELD);
				
				if(partyrole==1){
					String ownPartyID = partyid;
					
					recForeignCrsModel.setOwnCfetsNo(ownPartyID);
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					
					accountCurrency=msgUtil.getStringValue(nopartyids1, AccountCurrency.FIELD);
					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);

						if(StringUtils.isBlank(recForeignCrsModel.getBankName()) || StringUtils.isBlank(recForeignCrsModel.getAcctName()) ||
								StringUtils.isBlank(recForeignCrsModel.getAcctOpenNo())||StringUtils.isBlank(recForeignCrsModel.getAcctNo())){
							String settlCcy = accountCurrency;
							recForeignCrsModel.setSettCcy(settlCcy);
							if (partysubidtype == PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM) {
								recForeignCrsModel.setOwnName(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_BANK_NAME) {
								recForeignCrsModel.setBankName(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NAME) {
								recForeignCrsModel.setAcctName(partysubid);
							}else if (partysubidtype == PartySubIDType.SWIFT_BIC_CODE) {
								recForeignCrsModel.setAcctOpenNo(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NUMBER) {
								recForeignCrsModel.setAcctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_CURRENCY_REMARK) {
								recForeignCrsModel.setRemark(partysubid);
							}
						}else{
							String settlCcy = accountCurrency;
							recForeignCrsModel.setC_settCcy(settlCcy);
							if (partysubidtype == PartySubIDType.SETTLEMENT_BANK_NAME) {
								recForeignCrsModel.setFarBankName(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NAME) {
								recForeignCrsModel.setFarAcctName(partysubid);
							}else if (partysubidtype == PartySubIDType.SWIFT_BIC_CODE) {
								recForeignCrsModel.setFarAcctOpenNo(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NUMBER) {
								recForeignCrsModel.setFarAcctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_CURRENCY_REMARK) {
								recForeignCrsModel.setFarRemark(partysubid);
							}else if (partysubidtype == PartySubIDType.INTERMEDIARY_BANK) {
								recForeignCrsModel.setFarIntermeDiaryBankName(partysubid);
							}else if (partysubidtype == PartySubIDType.INTERMEDIARY_BANK_BIC_CODE) {
								recForeignCrsModel.setFarIntermeDiaryBankBicCode(partysubid);
							}else if (partysubidtype == PartySubIDType.INTERMEDIARY_BANK_ACCOUNT) {
								recForeignCrsModel.setFarIntermeDiaryBankAcctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.CORRESPONDENT_BANK_ACCOUNT) {
								recForeignCrsModel.setFarBankAcctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_BANK_BIC_CODE) {
								recForeignCrsModel.setFarBankOpenNo(partysubid);
							}
						}
						
					}
				}
				
				
				if(partyrole==17){
					String	othPartyID = partyid;
					recForeignCrsModel.setPartyCfetsNo(othPartyID);
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					for (int g = 1; g <= nopartysubids; g++) {
						
						nopartyids1.getGroup(g, nopartysubids1);
						
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						
						if(StringUtils.isBlank(recForeignCrsModel.getC_bankName()) || StringUtils.isBlank(recForeignCrsModel.getC_acctName()) ||
								StringUtils.isBlank(recForeignCrsModel.getC_acctOpenNo())||StringUtils.isBlank(recForeignCrsModel.getC_acctNo())){
							if (partysubidtype == PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM) {
								recForeignCrsModel.setPartyName(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_BANK_NAME) {
								recForeignCrsModel.setC_bankName(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NAME) {
								recForeignCrsModel.setC_acctName(partysubid);
							}else if (partysubidtype == PartySubIDType.SWIFT_BIC_CODE) {
								recForeignCrsModel.setC_acctOpenNo(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NUMBER) {
								recForeignCrsModel.setC_acctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_CURRENCY_REMARK) {
								recForeignCrsModel.setC_remark(partysubid);
							}
						}else{
							if (partysubidtype == PartySubIDType.SETTLEMENT_BANK_NAME) {
								recForeignCrsModel.setC_farBankName(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NAME) {
								recForeignCrsModel.setC_farAcctName(partysubid);
							}else if (partysubidtype == PartySubIDType.SWIFT_BIC_CODE) {
								recForeignCrsModel.setC_farAcctOpenNo(partysubid);
							}else if (partysubidtype == PartySubIDType.CASH_ACCOUNT_NUMBER) {
								recForeignCrsModel.setC_farAcctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_CURRENCY_REMARK) {
								recForeignCrsModel.setC_farRemark(partysubid);
							}else if (partysubidtype == PartySubIDType.INTERMEDIARY_BANK) {
								recForeignCrsModel.setC_farIntermeDiaryBankName(partysubid);
							}else if (partysubidtype == PartySubIDType.INTERMEDIARY_BANK_BIC_CODE) {
								recForeignCrsModel.setC_farIntermeDiaryBankBicCode(partysubid);
							}else if (partysubidtype == PartySubIDType.INTERMEDIARY_BANK_ACCOUNT) {
								recForeignCrsModel.setC_farIntermeDiaryBankAcctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.CORRESPONDENT_BANK_ACCOUNT) {
								recForeignCrsModel.setC_farBankAcctNo(partysubid);
							}else if (partysubidtype == PartySubIDType.SETTLEMENT_BANK_BIC_CODE) {
								recForeignCrsModel.setC_farBankOpenNo(partysubid);
							}
						}
					}
				}
				
			}
			
			RecPageInfoModel pageInfo = RecPageInfo.msgPageToModel(message);
			
			recForeignCrsModel.setPageInfo(pageInfo);
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return recForeignCrsModel;
	}
	
	public RecForeignCrsModel ExecutionReportCancel(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		MessageUtil msgUtil = new MessageUtil(message);
		RecForeignCrsModel recForeignCrsModel = new RecForeignCrsModel();
		try {
			String execID = "";
			execID  = msgUtil.getStringValue(ExecID.FIELD);
			String tradeDate = "";
			tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);
			String marketIndicator = "";
			marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
			String execType = "";
			execType  = msgUtil.getStringValue(ExecType.FIELD);
			String confirmID = "";
			confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
			
			
			if ("".equals(execID)) {
				System.out.println("消息未收到域：ExecID "+ExecID.FIELD);
			}	
			if ("".equals(tradeDate)) {
				System.out.println("消息未收到域：TradeDate "+TradeDate.FIELD);
			}	
			if ("".equals(marketIndicator)) {
				System.out.println("消息未收到域：MarketIndicator "+MarketIndicator.FIELD);
			}			
			if ("".equals(execType)) {
				System.out.println("消息未收到域：ExecType "+ExecType.FIELD);
			}			
			if ("".equals(confirmID)) {
				System.out.println("消息未收到域：ConfirmID "+ConfirmID.FIELD);
			}			
			
			recForeignCrsModel.setExecId(execID);
			recForeignCrsModel.setTradedate(tradeDate);
			recForeignCrsModel.setMarketIndicator(marketIndicator);
			recForeignCrsModel.setConfirmid(confirmID);
			recForeignCrsModel.setExecType(execType);
			//分页信息
			RecPageInfoModel pageInfo = RecPageInfo.msgPageToModel(message);
			recForeignCrsModel.setPageInfo(pageInfo);
		} catch (Exception e) {
			LogManager.info("解析报文出错"+e);
			throw e;
			
		}
		return recForeignCrsModel;
	}
}
