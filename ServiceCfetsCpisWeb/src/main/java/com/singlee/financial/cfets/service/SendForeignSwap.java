package com.singlee.financial.cfets.service;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.model.AgencyBasicInformationModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.model.SendForeignSwapModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.financial.util.AgencyBasicInformationUtil;
import com.singlee.financial.util.BigDecimalUtil;
import imix.ConfigError;
import imix.field.*;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoLegs;
import imix.imix10.Message;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * 
 * <P>SendForeignSwap.java</P>
 * <P>发送外币掉期成交确认报文处理类</P>
 * <P>Copyright: Copyright (c) 2018</P>
 * <P>Company: 新利</P>
 * @author 彭前
 * <P>          修改者姓名 修改内容说明</P>
 */
public class SendForeignSwap extends ConfirmMsgService{
	
	public SendForeignSwap(ConfigBean configBean) {
		super(configBean);
	}
	
	@Override
	public void pkgMsgBody(imix.imix10.Message message, TradeConfirmBean bean)throws Exception {
		SendForeignSwapModel model = (SendForeignSwapModel)bean;
		// 成交编号， 需求未约束编码规则
		message.setField(new ExecID(model.getExecID())); 
		// 成交日期，格式为：YYYYMMDD
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>", "")));
		// 确认报文 ID
		message.setField(new ConfirmID(model.getConfirmID()));
		// 2 交易场所， 默认为CFETS
		message.setField(new MarketID("2"));
		// 11 外汇掉期 27-贵金属掉期
		message.setField(new MarketIndicator(model.getMarketIndicator()));
		//NoLegs 2 通过起息日区分近端远端
		message.setField(new imix.field.NoLegs(2)); 
		NoLegs noLegs1 = new NoLegs();
		// 近端起息日， 格式为：YYYYMMDD
		noLegs1.set(new LegSettlDate(model.getLegSettlDate_1().replace("<Date>", ""))); 
		//近端成交价格  未给出该字段精度 单位
		if (StringUtils.isNotBlank(model.getLegLastPx_1())) {
			noLegs1.set(new LegLastPx(BigDecimalUtil.round(new BigDecimal(model.getLegLastPx_1()).doubleValue(),8)));
		}
		noLegs1.set(new LegCalculatedCcyLastQty(model.getLegCalculatedCcyLastQty_1())); // 近端卖出金额，需求规格说明书未定义该字段精度、单位
		noLegs1.set(new LegLastQty(model.getLegLastQty_1())); // 近端买入金额， 需求规格说明书未定义该字段精度、单位
		

		NoLegs noLegs2 = new NoLegs();
		// 远端起息日， 格式为：YYYYMMDD
		noLegs2.set(new LegSettlDate(model.getLegSettlDate_2().replace("<Date>", ""))); 
		//远端成交价格  未给出该字段精度 单位
		if (StringUtils.isNotBlank(	model.getLegLastPx_2())) {
			noLegs2.set(new LegLastPx(BigDecimalUtil.round(new BigDecimal(model.getLegLastPx_2()).doubleValue(),8)));
		}
		noLegs2.set(new LegCalculatedCcyLastQty(model.getLegCalculatedCcyLastQty_2())); // 远端卖出金额，需求规格说明书未定义该字段精度、单位
		noLegs2.set(new LegLastQty(model.getLegLastQty_2())); // 远端买入金额， 需求规格说明书未定义该字段精度、单位
		//交收方式
		if(StringUtils.isNotEmpty(model.getDeliveryType())){
			message.setField(new DeliveryType(Integer.valueOf(model.getDeliveryType())));
		}

		if (MarketIndicator.GOLDSWP.equals(model.getMarketIndicator())) {//贵金属掉期
			message.setField(new SecurityID(model.getSecurityID()));
			noLegs1.set(new LegCurrency(model.getLegCurrency_1()));
			if ("1".equals(model.getLegSide_1())) {
				noLegs1.set(new LegSide('1'));
			}else if ("4".equals(model.getLegSide_1())) {
				noLegs1.set(new LegSide('4'));
			}
			noLegs2.set(new LegCurrency(model.getLegCurrency_2()));
			if ("1".equals(model.getLegSide_2())) {
				noLegs2.set(new LegSide('1'));
			}else if ("4".equals(model.getLegSide_2())) {
				noLegs2.set(new LegSide('4'));
			}
			pkgGoldSettlInfo(message, model);
		}else {//掉期
			if ("2".equals(model.getNetGrossInd())) {
				message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
			} else {
				message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
			}
			noLegs1.set(new LegSide('B')); // B
			noLegs1.set(new LegCurrency2(model.getLegCurrency2_1())); // 近端卖出币种
			noLegs1.set(new LegCurrency1(model.getLegCurrency1_1())); // 近端买入币种
			noLegs2.set(new LegSide('B')); // B
			noLegs2.set(new LegCurrency2(model.getLegCurrency2_2())); // 远端卖出币种
			noLegs2.set(new LegCurrency1(model.getLegCurrency1_2())); // 远端买入币种
			pkgSettlInfo(message, bean.getTradeSettlsBean(),bean.getNetGrossInd(),model.getDeliveryType());
		}
		
		message.addGroup(noLegs1);
		message.addGroup(noLegs2);
		
	}
	
	public void pkgSettlInfo(Message message,TradeSettlsBean bean,String netGrossInd,String deliveryType)throws Exception{
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyRole(PartyRole.EXECUTING_FIRM));
		// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		own.set(new PartyID(getConfigBean().getBankId()));
		
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		cust.set(new PartyRole(PartyRole.CONTRA_FIRM));
		cust.set(new PartyID(bean.getcInstitutionId()));
		
		// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getFullName());
		
		// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getcFullName());
		
		//如果交易方式为双边清算 (NetGrossInd=2)  则为双方传入账户信息  否则不需要传入这些域
		if("2".equals(netGrossInd)||!"12".equals(deliveryType))
		{
			String ccy1 = bean.getSettccy();
			String ccy2 = bean.getcSettccy();
			
			//近端本方清算信息
			pkgNearSettls(own, bean.getSettccy(), bean.getBankname(), bean.getBankopenno(), bean.getBankacctno(), 
					bean.getAcctname(), bean.getAcctopenno(), bean.getAcctno(), 
					bean.getIntermediarybankname(), bean.getIntermediarybankbiccode(), bean.getIntermediarybankacctno(), bean.getRemark());
			
			//远端本方清算信息
			pkgFarSettls(own, ccy2, bean.getFarBankname(), bean.getFarBankopenno(), bean.getFarBankacctno(), 
					bean.getFarAcctname(), bean.getFarAcctopenno(), bean.getFarAcctno(), 
					bean.getFarIntermediarybankname(), bean.getFarIntermediarybankbiccode(), bean.getFarIntermediarybankacctno(), bean.getFarRemark());
			
			//近端对手方清算信息
			pkgNearSettls(cust, bean.getcSettccy(), bean.getcBankname(), bean.getcBankopenno(), bean.getcBankacctno(), 
					bean.getcAcctname(), bean.getcAcctopenno(), bean.getcAcctno(), 
					bean.getcIntermediarybankname(), bean.getcIntermediarybankbiccode(), bean.getcIntermediarybankacctno(), bean.getcRemark());
			
			//远端对手方清算信息
			pkgFarSettls(cust, ccy1, bean.getcFarBankname(), bean.getcFarBankopenno(), bean.getcFarBankacctno(), 
					bean.getcFarAcctname(), bean.getcFarAcctopenno(), bean.getcFarAcctno(), 
					bean.getcFarIntermediarybankname(), bean.getcFarIntermediarybankbiccode(), bean.getcFarIntermediarybankacctno(), bean.getcFarRemark());
		}
		
		message.addGroup(own);
		message.addGroup(cust);
	}
	
	/***
	 * 
	 * 近端清算信息
	 * @param iDs
	 * @param settccy
	 * @param bankname
	 * @param bankopenno
	 * @param bankacctno
	 * @param acctname
	 * @param acctopenno
	 * @param acctno
	 * @param intermediarybankname
	 * @param intermediarybankbiccode
	 * @param intermediarybankacctno
	 * @param remark
	 */
	public void pkgNearSettls(ExecutionReport.NoPartyIDs iDs,String settccy,String bankname,String bankopenno,String bankacctno,
			String acctname,String acctopenno,String acctno,
			String intermediarybankname,String intermediarybankbiccode,String intermediarybankacctno,
			String remark){
		if("CNY".equals(settccy)){ //近端本币
			// 近端资金开户行  必填需匹配 178
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME_OF_NEAR_LEG, bankname);
			// 近端资金账户户名 必填需匹配 176
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME_OF_NEAR_LEG, acctname);
			// 近端支付系统行号 必填需匹配 182
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE_OF_NEAR_LEG, bankacctno);
			// 近端资金账号180
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER_OF_NEAR_LEG, acctno);
			// 本方附言   选填   若填值则需要匹配 139
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, remark);
			
		}else{ //近端外币
			// 近端开户行名称  178
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME_OF_NEAR_LEG , bankname);
			// 近 端 开 户  BICCODE 175
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_NAME_OF_NEAR_LEG, bankopenno);
			// 近端资金开户行账号 221
			convert(iDs,PartySubIDType.CORRESPONDENT_BANK_ACCOUNT_OF_NEAR_LEG, bankacctno);
			// 近 端 收款行名称  176
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME_OF_NEAR_LEG, acctname);
			
			// 近 端 收 款行BICCODE 182
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE_OF_NEAR_LEG, acctopenno);
			// 近端收款行账号 180
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER_OF_NEAR_LEG, acctno);
			// 近端附言 139
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, remark);
			
			// 近端中间行名称 215
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK_OF_NEAR_LEG, intermediarybankname);
			// 近 端 中 间行BICCODE 217
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK_BIC_CODE_OF_NEAR_LEG, intermediarybankbiccode);
			// 近端中间行账号 219
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK_ACCOUNT_OF_NEAR_LEG, intermediarybankacctno);
		}
	}
	
	/***
	 * 
	 * 远端清算信息
	 * @param iDs
	 * @param settccy
	 * @param bankname
	 * @param bankopenno
	 * @param bankacctno
	 * @param acctname
	 * @param acctopenno
	 * @param acctno
	 * @param intermediarybankname
	 * @param intermediarybankbiccode
	 * @param intermediarybankacctno
	 * @param remark
	 */
	public void pkgFarSettls(ExecutionReport.NoPartyIDs iDs,String settccy,String bankname,String bankopenno,String bankacctno,
			String acctname,String acctopenno,String acctno,
			String intermediarybankname,String intermediarybankbiccode,String intermediarybankacctno,
			String remark){
		if("CNY".equals(settccy)){
			// 远端资金开户行 194
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME_OF_FAR_LEG, bankname);
			// 远端资金账户户名 192
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME_OF_FAR_LEG, acctname);
			// 远端支付系统行号  198
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE_OF_FAR_LEG, bankacctno);
			// 远端资金账号 196
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER_OF_FAR_LEG, acctno);
			// 远端附言 145
			convert(iDs,PartySubIDType.CONTRA_CCY_REMARK, remark);
			
		}else{
			// 远端开户行名称 194
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME_OF_FAR_LEG, bankname);
			// 远 端 开 户行BICCODE 191
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_NAME_OF_FAR_LEG, bankopenno);
			// 远端资金开户行账号 222
			convert(iDs,PartySubIDType.CONTRA_CCY_CORRESPONDENT_BANK_ACCOUNT_OF_FAR_LEG, bankacctno);
			// 远端收款行名称 192
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME_OF_FAR_LEG, acctname);
			// 远 端 收 款行BICCODE 198
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE_OF_FAR_LEG, acctopenno);
			//  远端收款行账号 196
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER_OF_FAR_LEG, acctno);
			
			// 远端中间行名称 216
			convert(iDs,PartySubIDType.CONTRA_CCY_INTERMEDIARY_BANK_OF_FAR_LEG, intermediarybankname);
			// 远 端 中 间行BICCODE 218
			convert(iDs,PartySubIDType.CONTRA_CCY_INTERMEDIARY_BANK_BIC_CODE_OF_FAR_LEG, intermediarybankbiccode);
			// 远端中间行账号 220
			convert(iDs,PartySubIDType.CONTRA_CCY_INTERMEDIARY_BANK_ACCOUNT_OF_FAR_LEG, intermediarybankacctno);
			
			// 远端附言 145
			convert(iDs,PartySubIDType.CONTRA_CCY_REMARK, remark);
		}
	}
	
	public void pkgGoldSettlInfo(Message message,SendForeignSwapModel bean)throws Exception{
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyRole(PartyRole.EXECUTING_FIRM));
		// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		own.set(new PartyID(getConfigBean().getBankId()));
		
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		cust.set(new PartyRole(PartyRole.CONTRA_FIRM));
		cust.set(new PartyID(bean.getPartyID_2()));
		
		// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getOwnName());
		
		// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getPartyName());
		
		message.addGroup(own);
		message.addGroup(cust);
	}
	
	/**
	 * 拼装上送IMIX消息报文
	 */
	public Message packageMsg(SendForeignSwapModel model, ConfigBean configBean) throws ConfigError {

		Message message = MsgHeadAndStandardTrailer.packageMsgHeadAndStandardTrailer("AK", configBean);

		message.setField(new ExecID(model.getExecID())); // 成交编号， 需求未约束编码规则
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>", "")));// 成交日期，格式为：YYYYMMDD
		message.setField(new ConfirmID(model.getConfirmID()));// 确认报文 ID
		message.setField(new MarketID("2"));// 2 交易场所， 默认为CFETS
		
		if ("2".equals(model.getNetGrossInd())) {
			message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		} else {
			message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算

		}
		
		message.setField(new MarketIndicator("11"));// 11 外汇掉期

		message.setField(new imix.field.NoLegs(2)); //NoLegs 2 通过起息日区分近端远端

		NoLegs noLegs1 = new NoLegs();

		noLegs1.set(new LegSide('B')); // B
		noLegs1.set(new LegSettlDate(model.getLegSettlDate_1().replace("<Date>", ""))); // 近端起息日， 格式为：YYYYMMDD
		
		//近端成交价格  未给出该字段精度 单位
		if (StringUtils.isNotBlank(model.getLegLastPx_1())) {
			noLegs1.set(new LegLastPx(BigDecimalUtil.round(new BigDecimal(model.getLegLastPx_1()).doubleValue(),8)));
		}
		noLegs1.set(new LegCalculatedCcyLastQty(model.getLegCalculatedCcyLastQty_1())); // 近端卖出金额，需求规格说明书未定义该字段精度、单位
		noLegs1.set(new LegLastQty(model.getLegLastQty_1())); // 近端买入金额， 需求规格说明书未定义该字段精度、单位
		noLegs1.set(new LegCurrency2(model.getLegCurrency2_1())); // 近端卖出币种
		noLegs1.set(new LegCurrency1(model.getLegCurrency1_1())); // 近端买入币种

		message.addGroup(noLegs1);

		NoLegs noLegs2 = new NoLegs();
		noLegs2.set(new LegSide('B')); // B
		noLegs2.set(new LegSettlDate(model.getLegSettlDate_2().replace("<Date>", ""))); // 远端起息日， 格式为：YYYYMMDD
		
		//远端成交价格  未给出该字段精度 单位
		if (StringUtils.isNotBlank(	model.getLegLastPx_2())) {
			noLegs2.set(new LegLastPx(BigDecimalUtil.round(new BigDecimal(model.getLegLastPx_2()).doubleValue(),8)));
		}
		
		
		noLegs2.set(new LegCalculatedCcyLastQty(model.getLegCalculatedCcyLastQty_2())); // 远端卖出金额，需求规格说明书未定义该字段精度、单位
		noLegs2.set(new LegLastQty(model.getLegLastQty_2())); // 远端买入金额， 需求规格说明书未定义该字段精度、单位
		noLegs2.set(new LegCurrency2(model.getLegCurrency2_2())); // 远端卖出币种
		noLegs2.set(new LegCurrency1(model.getLegCurrency1_2())); // 远端买入币种

		message.addGroup(noLegs2);

		message.setField(new imix.field.NoPartyIDs(2));
		
		ExecutionReport.NoPartyIDs ownnopartyids = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs ownnopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		
		ownnopartyids.set(new PartyID(configBean.getBankId()));// 本方机构 21 位码,如果不传输机构 21 位码，此域作为协议必须域，请填写“-”
																						//从配置文件中取								
		ownnopartyids.set(new PartyRole(1));// 本方 （以近端买入人民币， 远端买入外币为例）

		ExecutionReport.NoPartyIDs othnopartyids = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs othnopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		othnopartyids.set(new PartyID(model.getPartyID_2()));// 对手方机构 21 位码,如果不传输机构 21 位码，此域作为协议必须域，请填写“-”
		othnopartyids.set(new PartyRole(17));
		
		String info = "";
		
		//如果交易方式为双边清算 则传入这些域值
		boolean flag = "2".equals(model.getNetGrossInd());
		//近端买入币种
		String boughtCcy = model.getLegCurrency1_1();
		//近端卖出币种
		String soldCcy = model.getLegCurrency1_2();
		
		//本方机构信息集合
		List<AgencyBasicInformationModel> list1 = model.getList1();
		//对手方机构信息集合
		List<AgencyBasicInformationModel> list2 = model.getList2();
		
		if (flag) {
			
			//ownnopartysubids1.set(new PartySubID(CPISStart.getProperties().getProperties("CFETSNAME")));// 中文全称，如果机构中文全称不传输，此组域不传输
			if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "124"))) {
				ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "124")));
				ownnopartysubids1.set(new PartySubIDType(124));
				ownnopartyids.addGroup(ownnopartysubids1);
			}
			
			if ("CNY".equals(boughtCcy)) {
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "178"))) {
					//ownnopartysubids1.set(new PartySubID(CPISStart.getProperties().getProperties("CFETSBANKNAME")));// 近端资金开户行  必填需匹配
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "178")));
					ownnopartysubids1.set(new PartySubIDType(178));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "176"))) {
					//ownnopartysubids1.set(new PartySubID(CPISStart.getProperties().getProperties("CFETSACCOUNTNAME")));// 近端资金账户户名 必填需匹配
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "176")));
					ownnopartysubids1.set(new PartySubIDType(176));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "182"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "182")));// 近端支付系统行号 必填需匹配
					ownnopartysubids1.set(new PartySubIDType(182));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "180"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "180")));// 近端资金账号
					ownnopartysubids1.set(new PartySubIDType(180));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "139");
				if (StringUtils.isNotBlank(info)) {
					ownnopartysubids1.set(new PartySubID(info));// 近端附言
					ownnopartysubids1.set(new PartySubIDType(139));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "216");
				if ((StringUtils.isNotBlank(info))) {
					ownnopartysubids1.set(new PartySubID(info));// 远端中间行名称
					ownnopartysubids1.set(new PartySubIDType(216));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "218");
				if ((StringUtils.isNotBlank(info))) {
					ownnopartysubids1.set(new PartySubID(info));// 远 端 中 间行BICCODE
					ownnopartysubids1.set(new PartySubIDType(218));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "220");
				if ((StringUtils.isNotBlank(info))) {
					ownnopartysubids1.set(new PartySubID(info));// 远端中间行账号
					ownnopartysubids1.set(new PartySubIDType(220));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "194"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "194")));// 远端开户行名称
					ownnopartysubids1.set(new PartySubIDType(194));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "191"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "191")));// 远 端 开 户行BICCODE
					ownnopartysubids1.set(new PartySubIDType(191));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "192"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "192")));// 远端收款行名称
					ownnopartysubids1.set(new PartySubIDType(192));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "198"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "198")));// 远 端 收 款行BICCODE
					ownnopartysubids1.set(new PartySubIDType(198));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "196"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "196")));// 远端收款行账号
					ownnopartysubids1.set(new PartySubIDType(196));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				//ownnopartysubids1.set(new PartySubID("-"));// 远端附言
				//ownnopartysubids1.set(new PartySubIDType(145));
//				ownnopartyids.addGroup(ownnopartysubids1);
			} else {
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "215");
				if ((StringUtils.isNotBlank(info))) {
					ownnopartysubids1.set(new PartySubID(info));// 近端中间行名称
					ownnopartysubids1.set(new PartySubIDType(215));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "217");
				if ((StringUtils.isNotBlank(info))) {
					ownnopartysubids1.set(new PartySubID(info));// 近 端 中 间行BICCODE
					ownnopartysubids1.set(new PartySubIDType(217));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "219");
				if ((StringUtils.isNotBlank(info))) {
					ownnopartysubids1.set(new PartySubID((info)));// 近端中间行账号
					ownnopartysubids1.set(new PartySubIDType(219));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "178"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "178")));// 近端开户行名称
					ownnopartysubids1.set(new PartySubIDType(178));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "175"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "175")));// 近 端 开 户
					ownnopartysubids1.set(new PartySubIDType(175));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "182"))) {
					ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil.getPartySubID(list1, "182"))));// 近 端 收 款行BICCODE
					ownnopartysubids1.set(new PartySubIDType(182));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "180"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "180")));// 近端收款行账号
					ownnopartysubids1.set(new PartySubIDType(180));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "139");
				if (StringUtils.isNotBlank(info)) {
					ownnopartysubids1.set(new PartySubID(info));// 近端附言
					ownnopartysubids1.set(new PartySubIDType(139));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
			
				
			//	ownnopartysubids1.set(new PartySubID(CPISStart.getProperties().getProperties("CFETSBANKNAME")));// 远端资金开户行
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "194"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "194")));
					ownnopartysubids1.set(new PartySubIDType(194));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				
			//	ownnopartysubids1.set(new PartySubID(CPISStart.getProperties().getProperties("CFETSACCOUNTNAME")));// 远端资金账户户名
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "192"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "192")));
					ownnopartysubids1.set(new PartySubIDType(192));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "198"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "198")));// 远端支付系统行号
					ownnopartysubids1.set(new PartySubIDType(198));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "196"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "196")));// 远端资金账号
					ownnopartysubids1.set(new PartySubIDType(196));
					ownnopartyids.addGroup(ownnopartysubids1);
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list1, "145");
				if (StringUtils.isNotBlank(info)) {
					ownnopartysubids1.set(new PartySubID(info));// 远端附言
					ownnopartysubids1.set(new PartySubIDType(145));
					ownnopartyids.addGroup(ownnopartysubids1);
					
				}
			}
		}

		if (flag) {
			
			info = AgencyBasicInformationUtil.getPartySubID(list2, "124");

			if (StringUtils.isNotBlank(info)) {
				othnopartysubids1.set(new PartySubID(info));// 中文全称， 如果机构中文全称不传输， 此组域不传输
				othnopartysubids1.set(new PartySubIDType(124));
				othnopartyids.addGroup(othnopartysubids1);
			}
			
			if ("CNY".equals(soldCcy)) {
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "178"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "178")));// 近端资金开户行
					othnopartysubids1.set(new PartySubIDType(178));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
				//20160824 zhuqx 修改取资金账户户名为受益人名称
//				othnopartysubids1.set(new PartySubID(prop.getProperties("Customer.LegalName")));// 资金账户户名
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "176"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "176")));// 资金账户户名				
					othnopartysubids1.set(new PartySubIDType(176));
					othnopartyids.addGroup(othnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "182"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "182")));// 近端支付系统行号
					othnopartysubids1.set(new PartySubIDType(182));
					othnopartyids.addGroup(othnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "180"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "180")));// 近端资金账号
					othnopartysubids1.set(new PartySubIDType(180));
					othnopartyids.addGroup(othnopartysubids1);
				}
				info = AgencyBasicInformationUtil.getPartySubID(list2, "139");
				if (StringUtils.isNotBlank(info)){
					othnopartysubids1.set(new PartySubID(info));// 近端附言
					othnopartysubids1.set(new PartySubIDType(139));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "216");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 远端中间行名称
					othnopartysubids1.set(new PartySubIDType(216));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "218");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 远 端 中 间行BICCODE
					othnopartysubids1.set(new PartySubIDType(218));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "220");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID((info)));// 远端中间行账号
					othnopartysubids1.set(new PartySubIDType(220));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "194"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "194")));// 远端开户行名称
					othnopartysubids1.set(new PartySubIDType(194));
					othnopartyids.addGroup(othnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "191"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "191")));// 远 端 开 户行BICCODE
					othnopartysubids1.set(new PartySubIDType(191));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "198"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "198")));// 远 端 收 款行BICCODE
					othnopartysubids1.set(new PartySubIDType(198));
					othnopartyids.addGroup(othnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "196"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "196")));// 远端收款行账号
					othnopartysubids1.set(new PartySubIDType(196));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "145");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 远端附言
					othnopartysubids1.set(new PartySubIDType(145));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
			} else {
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "215");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 近端中间行名称
					othnopartysubids1.set(new PartySubIDType(215));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "217");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 近 端 中 间行BICCODE
					othnopartysubids1.set(new PartySubIDType(217));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "219");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 近端中间行账号
					othnopartysubids1.set(new PartySubIDType(219));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "178"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "178")));// 近端开户行名称
					othnopartysubids1.set(new PartySubIDType(178));
					othnopartyids.addGroup(othnopartysubids1);
				}		
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "175"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "175")));// 近 端 开 户
					othnopartysubids1.set(new PartySubIDType(175));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "221");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 近端开户行账号
					othnopartysubids1.set(new PartySubIDType(221));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "182"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "182")));// 近 端 收 款行BICCODE
					othnopartysubids1.set(new PartySubIDType(182));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "180"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "180")));// 近端收款行账号
	//				}
					othnopartysubids1.set(new PartySubIDType(180));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "139");
				if (StringUtils.isNotBlank(info)) {
					othnopartysubids1.set(new PartySubID(info));// 近端附言
					othnopartysubids1.set(new PartySubIDType(139));
					othnopartyids.addGroup(othnopartysubids1);
					
					
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "194"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "194")));// 远端资金开户行
					othnopartysubids1.set(new PartySubIDType(194));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
//				othnopartysubids1.set(new PartySubID(prop.getProperties("Customer.LegalName")));// 远端资金账户户名
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "192"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "192")));// 远端资金账户户名
					othnopartysubids1.set(new PartySubIDType(192));
					othnopartyids.addGroup(othnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "198"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "198")));// 远端支付系统行号
					othnopartysubids1.set(new PartySubIDType(198));
					othnopartyids.addGroup(othnopartysubids1);
				}
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "196"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "196")));// 远端资金账号
					othnopartysubids1.set(new PartySubIDType(196));
					othnopartyids.addGroup(othnopartysubids1);
				}
				
				
				info = AgencyBasicInformationUtil.getPartySubID(list2, "145");
				if (StringUtils.isNotBlank(info)) {				
					othnopartysubids1.set(new PartySubID(info));// 远端附言
					othnopartysubids1.set(new PartySubIDType(145));
					othnopartyids.addGroup(othnopartysubids1);
					
				}
			}
		}
		message.addGroup(ownnopartyids);
		message.addGroup(othnopartyids);
		
		System.out.println(message);
		return message;
	}

	@Override
	public void pkgMsgBody(Message message, List<SendForeignOptionModel> beans)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
