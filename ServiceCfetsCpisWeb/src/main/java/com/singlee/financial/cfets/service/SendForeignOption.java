package com.singlee.financial.cfets.service;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.util.BigDecimalUtil;
import imix.field.*;
import imix.imix10.ExecutionReport;
import imix.imix10.MassCofirmation.NoMassCofirmations;
import imix.imix10.Message;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.List;

public class SendForeignOption extends ConfirmMsgService {

	public SendForeignOption(ConfigBean configBean) {
		super(configBean);
		setMsgType(MsgType.MASSCOFIRMATION);
	}
	

	@Override
	public void pkgMsgBody(Message message, TradeConfirmBean bean)
			throws Exception {
		SendForeignOptionModel model = (SendForeignOptionModel) bean;
		
		message.setField(new ConfirmID(model.getConfirmid()));// 确认报文 ID
		message.setField(new MarketIndicator("19"));// 市场标识，19-外汇期权
		message.setField(new OPTDataType(model.getOptdatatype()));
		if(!"1".equals(model.getOptpayouttype())){
			message.setField(new CombinationID(model.getCombinationid()));
		}
		message.setField(new OptPayoutType(Integer.valueOf(model.getOptpayouttype())));
		
		message.setField(new imix.field.NoMassCofirmations(1));
		NoMassCofirmations noMassCofirmations = new NoMassCofirmations();
		
		
		noMassCofirmations.setField(new TradeDate(model.getTradedate()));// 成交日期，格式为：YYYYMMDD
		noMassCofirmations.setField(new ExecID(model.getExecID())); // 成交编号， 需求未约束编码规则
		if ("S".equals(model.getSide())) {
			noMassCofirmations.setField(new Side('4'));// 交易方向， 本方对期权的买卖方向 1-买入 4-卖出
		}else if ("P".equals(model.getSide())){
			noMassCofirmations.setField(new Side('1'));// 交易方向， 本方对期权的买卖方向 1-买入 4-卖出
		}
		if ("2".equals(model.getNetGrossInd())) {
			noMassCofirmations.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		} else {
			noMassCofirmations.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		}
		noMassCofirmations.setField(new OptPremiumCurrency(model.getOptpremiumcurrency()));// 期权费货币
		noMassCofirmations.setField(new Currency1(model.getCurrency1()));// 基准货币名称
		noMassCofirmations.setField(new Currency1Amt(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getCurrency1amt()).doubleValue()),2),2,"0")));//基准货币金额， 单位为：元，精度为2
		noMassCofirmations.setField(new Currency2(model.getCurrency2()));//非基准货币名称
		noMassCofirmations.setField(new Currency2Amt(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getCurrency2amt()).doubleValue()),2),2,"0")));//非基准货币金额金额，单位：元，精度为 2
		noMassCofirmations.setField(new AgreementCurrency(model.getAgreementcurrency()));//期权货币
		if ("0".equals(model.getPutorcall())) {
			noMassCofirmations.setField(new PutOrCall('0'));//期权类型； 外汇成交单中的交易类型，枚举值：看涨期权、看跌期权 0-put 1-call
		}else if ("1".equals(model.getPutorcall())) {
			noMassCofirmations.setField(new PutOrCall('1'));//期权类型； 外汇成交单中的交易类型，枚举值：看涨期权、看跌期权 0-put 1-call
		}
		
		if(!"".equals(model.getStrikeprice())){
			noMassCofirmations.setField(new StrikePrice(BigDecimalUtil.fixZero(BigDecimalUtil.round(new BigDecimal(model.getStrikeprice()).doubleValue()/100,8),8,"0")));// 行权价格， 最大精度为 9
		}
		if ("0".equals(model.getDerivativeexercisestyle())) {
			noMassCofirmations.setField(new DerivativeExerciseStyle('0'));// 执行方式 0-欧式期权 1-美式期权
		}else {
			noMassCofirmations.setField(new DerivativeExerciseStyle('1'));// 执行方式 0-欧式期权 1-美式期权
		}
		noMassCofirmations.setField(new MaturityDate(model.getMaturitydate()));// 到期日，格式为：YYYYMMDD

		noMassCofirmations.setField(new ExpireTimeZone(model.getExpiretimezone()));// 时区，
		noMassCofirmations.setField(new DeliveryDate(model.getDeliverydate()));// 交割日，格式为：YYYYMMDD
		if ("1".equals(model.getOptsettlamttype())) {
			noMassCofirmations.setField(new OptSettlAmtType(1));// 1-全额交割 2-差额交割
			noMassCofirmations.setField(new ExpireTime(model.getExpiretime()));// 行权截止日： 格式为YYYYMMDD-HH:MM:SS 或YYYYMMDD-HH:MM:SS.sss，第 10-14位表示截止时间
			noMassCofirmations.setField(new BenchmarkCurveName("-"));
		}else {
			noMassCofirmations.setField(new ExpireTime(model.getExpiretime()));// 行权截止日： 格式为YYYYMMDD-HH:MM:SS 或YYYYMMDD-HH:MM:SS.sss，第 10-14位表示截止时间
//			String[] times = model.getExpiretime().split("-");
			noMassCofirmations.setField(new OptSettlAmtType(2));// 1-全额交割 2-差额交割
			if (StringUtils.isNotBlank(model.getBenchmarkcurvename())) {
				noMassCofirmations.setField(new BenchmarkCurveName(model.getBenchmarkcurvename()));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
			}else {
				noMassCofirmations.setField(new BenchmarkCurveName("1"));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
			}
		}
		noMassCofirmations.setField(new PaymentDate(model.getPaymentdate()));// 期权费支付日， 格式为：YYYYMMDD
		// MONEY =  2-百分比                        MONEYVMAIN =1-基点  
		if("Term%".equals(model.getOptpremiumbasis())){
			noMassCofirmations.setField(new OptPremiumBasis(2));// 期权费类型， 枚举值：Pips、Term% 1-基点 2-百分比
			if(!"".equals(model.getOptpremiumvalue())){
				noMassCofirmations.setField(new OptPremiumValue(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumvalue()).doubleValue()),9),9,"0")));// 期权价格，精度： 9 位
			}
		}else if ("Pips".equals(model.getOptpremiumbasis())) {
			noMassCofirmations.setField(new OptPremiumBasis(1));// 期权费类型， 枚举值：Pips、Term% 1-基点 2-百分比
			if(!"".equals(model.getOptpremiumvalue())){
				noMassCofirmations.setField(new OptPremiumValue(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumvalue()).doubleValue()),9),9,"0")));// 期权价格，精度： 9 位
			}
		}
		if(!"".equals(model.getOptpremiumamt())){
			noMassCofirmations.setField(new OptPremiumAmt(BigDecimalUtil.fixZero( BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumamt()).doubleValue()),2),2,"0")));// 期权费， 单位： 元，精度：2 位
		}
		pkgSettlInfo(noMassCofirmations, model);
		message.addGroup(noMassCofirmations);
		

		
	}

	public void pkgSettlInfo(NoMassCofirmations noMassCofirmations,SendForeignOptionModel bean)throws Exception{
		
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyID(getConfigBean().getBankId()));// 机构 21 位码
		own.set(new PartyRole(1));// 1-本方
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		cust.set(new PartyID(bean.getTradeSettlsBean().getcInstitutionId()));// 机构 21 位码
		cust.set(new PartyRole(17));// 17-对手方
		if("2".equals(bean.getNetGrossInd())){
			if ("S".equals(bean.getSide())) {
				//期权费收取方-资金开户行名称
				convert(own,PartySubIDType.SETTLEMENT_BANK_NAME, bean.getTradeSettlsBean().getBankname());
				//期权费收取方-资金账户名称
				convert(own,PartySubIDType.CASH_ACCOUNT_NAME, bean.getTradeSettlsBean().getAcctname());
				//期权费收取方-支付系统行号
				convert(own,PartySubIDType.SWIFT_BIC_CODE, bean.getTradeSettlsBean().getBankacctno());
				//期权费收取方-资金账号
				convert(own,PartySubIDType.CASH_ACCOUNT_NUMBER, bean.getTradeSettlsBean().getAcctno());
				//期权费收取方-附言
				convert(own,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, bean.getTradeSettlsBean().getRemark());
			}else if ("P".equals(bean.getSide())) {
				//期权费收取方-资金开户行名称
				convert(cust,PartySubIDType.SETTLEMENT_BANK_NAME, bean.getTradeSettlsBean().getcBankname());
				//期权费收取方-资金账户名称
				convert(cust,PartySubIDType.CASH_ACCOUNT_NAME, bean.getTradeSettlsBean().getcAcctname());
				//期权费收取方-支付系统行号
				convert(cust,PartySubIDType.SWIFT_BIC_CODE, bean.getTradeSettlsBean().getcBankacctno());
				//期权费收取方-资金账号
				convert(cust,PartySubIDType.CASH_ACCOUNT_NUMBER, bean.getTradeSettlsBean().getcAcctno());
				//期权费收取方-附言
				convert(cust,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, bean.getTradeSettlsBean().getcRemark());
			}
		}
		noMassCofirmations.addGroup(own);
		noMassCofirmations.addGroup(cust);

	}
	
	
	public Message packageMsg(SendForeignOptionModel model, ConfigBean configBean){
		Message message = MsgHeadAndStandardTrailer.packageMsgHeadAndStandardTrailer("AK", configBean);
		
		message.setField(new ConfirmID(model.getConfirmid()));// 确认报文 ID
		message.setField(new ExecID(model.getExecID())); // 成交编号， 需求未约束编码规则
		message.setField(new TradeDate(model.getTradedate()));// 成交日期，格式为：YYYYMMDD
		
		if ("S".equals(model.getSide())) {
			message.setField(new Side('4'));// 交易方向， 本方对期权的买卖方向 1-买入 4-卖出
		}else if ("P".equals(model.getSide())){
			message.setField(new Side('1'));// 交易方向， 本方对期权的买卖方向 1-买入 4-卖出
		}
		
		if ("N".equals(model.getNetGrossInd())) {
			message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		}else {
			message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		}
		message.setField(new OptPremiumCurrency(model.getOptpremiumcurrency()));// 期权费货币
		message.setField(new Currency1(model.getCurrency1()));// 基准货币名称
		message.setField(new GrossTradeAmt(model.getCurrency1amt()));//基准货币金额， 单位为：元，精度为2
		message.setField(new Currency2(model.getCurrency2()));//非基准货币名称
		message.setField(new CalculatedCcyLastTradeAmt(model.getCurrency2amt()));//非基准货币金额金额，单位：元，精度为 2
		message.setField(new AgreementCurrency(model.getAgreementcurrency()));//期权货币
		if ("P".equals(model.getPutorcall())) {
			message.setField(new PutOrCall('0'));//期权类型； 外汇成交单中的交易类型，枚举值：看涨期权、看跌期权 0-put 1-call
		}else if ("C".equals(model.getPutorcall())) {
			message.setField(new PutOrCall('1'));//期权类型； 外汇成交单中的交易类型，枚举值：看涨期权、看跌期权 0-put 1-call
		}
		
		message.setField(new MarketIndicator("19"));// 市场标识，19-外汇期权
		if(!"".equals(model.getStrikeprice())){
			message.setField(new StrikePrice(BigDecimalUtil.round(new BigDecimal(model.getStrikeprice()).doubleValue(),9)));// 行权价格， 最大精度为 9
		}
		if ("EURO".equals(model.getDerivativeexercisestyle())) {
			message.setField(new ExerciseStyle(0));// 执行方式 0-欧式期权 1-美式期权
		}else {
			message.setField(new ExerciseStyle(1));// 执行方式 0-欧式期权 1-美式期权
		}
		message.setField(new MaturityDate(model.getMaturitydate()));// 到期日，格式为：YYYYMMDD

		message.setField(new ExpireTimeZone(model.getExpiretimezone()));// 时区，
		message.setField(new SettlDate(model.getDeliverydate()));// 交割日，格式为：YYYYMMDD
		
		if ("PHYS".equals(model.getOptsettlamttype())) {
			message.setField(new OptSettlAmtType(1));// 1-全额交割 2-差额交割
			message.setField(new ExpireTime(model.getExpiretime()));// 行权截止日： 格式为YYYYMMDD-HH:MM:SS 或YYYYMMDD-HH:MM:SS.sss，第 10-14位表示截止时间
		}else {
			message.setField(new ExpireTime(model.getExpiretime()));// 行权截止日： 格式为YYYYMMDD-HH:MM:SS 或YYYYMMDD-HH:MM:SS.sss，第 10-14位表示截止时间
			String[] times = model.getExpiretime().split("-");
			message.setField(new OptSettlAmtType(2));// 1-全额交割 2-差额交割
			if (times[1].startsWith("15")) {
				message.setField(new BenchmarkCurveName("5"));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
			}else if (times[1].startsWith("14")) {
				message.setField(new BenchmarkCurveName("4"));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
			}else if (times[1].startsWith("11")) {
				message.setField(new BenchmarkCurveName("3"));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
			}else if (times[1].startsWith("10")) {
				message.setField(new BenchmarkCurveName("2"));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
			}else {
				message.setField(new BenchmarkCurveName("1"));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
			}
		}
		message.setField(new PaymentDate(model.getPaymentdate()));// 期权费支付日， 格式为：YYYYMMDD
		
		
		// MONEY =  2-百分比                        MONEYVMAIN =1-基点  
		if("2".equals(model.getOptpremiumbasis())){
			message.setField(new OptPremiumBasis(2));// 期权费类型， 枚举值：Pips、Term% 1-基点 2-百分比
			if(!"".equals(model.getOptpremiumvalue())){
				message.setField(new OptPremiumValue(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumvalue()).doubleValue()),9)));// 期权价格，精度： 9 位
			}
		}else if ("1".equals(model.getOptpremiumbasis())) {
			message.setField(new OptPremiumBasis(1));// 期权费类型， 枚举值：Pips、Term% 1-基点 2-百分比
			if(!"".equals(model.getOptpremiumvalue())){
				message.setField(new OptPremiumValue(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumvalue()).doubleValue()),9)));// 期权价格，精度： 9 位
			}
		}
		
//		message.setField(new OptPremiumValue("288.07"));// 期权价格，精度： 9 位


		if(!"".equals(model.getOptpremiumamt())){
			message.setField(new OptPremiumAmt(BigDecimalUtil.fixZero( BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumamt()).doubleValue()),2),2,"0")));// 期权费， 单位： 元，精度：2 位
		}
		

		
		ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
		
		nopartyids1.set(new PartyID(getConfigBean().getBankId()));// 机构 21 位码
		nopartyids1.set(new PartyRole(1));// 1-本方
		ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		
		if ("S".equals(model.getSide())) {
			noPartySubIDs1.set(new PartySubIDType(110));//期权费收取方-资金开户行名称
			noPartySubIDs1.set(new PartySubID(model.getTradeSettlsBean().getBankname()));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(23));//期权费收取方-资金账户名称
			noPartySubIDs1.set(new PartySubID(model.getTradeSettlsBean().getAcctname()));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(16));//期权费收取方-支付系统行号
			noPartySubIDs1.set(new PartySubID(model.getTradeSettlsBean().getBankacctno()));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(15));//期权费收取方-资金账号
			noPartySubIDs1.set(new PartySubID(model.getTradeSettlsBean().getAcctno()));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(139));//期权费收取方-附言
			noPartySubIDs1.set(new PartySubID(model.getTradeSettlsBean().getRemark()));
			nopartyids1.addGroup(noPartySubIDs1);
		}
		
		message.addGroup(nopartyids1);
		
		ExecutionReport.NoPartyIDs nopartyids2 = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs2 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		nopartyids2.set(new PartyID(model.getTradeSettlsBean().getcInstitutionId()));// 机构 21 位码
		nopartyids2.set(new PartyRole(17));// 17-对手方
		
		if ("P".equals(model.getSide())) {
			noPartySubIDs2.set(new PartySubIDType(110));//期权费收取方-资金开户行名称
			noPartySubIDs2.set(new PartySubID(model.getTradeSettlsBean().getcBankname()));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(23));//期权费收取方-资金账户名称
			noPartySubIDs2.set(new PartySubID(model.getTradeSettlsBean().getcAcctname()));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(16));//期权费收取方-支付系统行号
			noPartySubIDs2.set(new PartySubID(model.getTradeSettlsBean().getcBankacctno()));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(15));//期权费收取方-资金账号
			noPartySubIDs2.set(new PartySubID(model.getTradeSettlsBean().getcAcctno()));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(139));//期权费收取方-附言
			noPartySubIDs2.set(new PartySubID(model.getTradeSettlsBean().getcRemark()));
			nopartyids2.addGroup(noPartySubIDs2);
		
		}
		
		message.addGroup(nopartyids2);		
		

		System.out.println(message);
		return message;
	}


	@Override
	public void pkgMsgBody(Message message, List<SendForeignOptionModel> beans)
			throws Exception {
		SendForeignOptionModel bean =beans.get(0);
		
		message.setField(new ConfirmID(bean.getCombinationid()));// 确认报文 ID
		message.setField(new MarketIndicator("19"));// 市场标识，19-外汇期权
		message.setField(new OPTDataType(bean.getOptdatatype()));
		if(!"1".equals(bean.getOptpayouttype())){
			message.setField(new CombinationID(bean.getCombinationid()));
		}
		message.setField(new OptPayoutType(Integer.valueOf(bean.getOptpayouttype())));
		
		
		for (SendForeignOptionModel model : beans) {
			
			NoMassCofirmations noMassCofirmations = new NoMassCofirmations();
			
			noMassCofirmations.setField(new TradeDate(model.getTradedate()));// 成交日期，格式为：YYYYMMDD
			noMassCofirmations.setField(new ExecID(model.getExecID())); // 成交编号， 需求未约束编码规则
			if ("S".equals(model.getSide())) {
				noMassCofirmations.setField(new Side('4'));// 交易方向， 本方对期权的买卖方向 1-买入 4-卖出
			}else if ("P".equals(model.getSide())){
				noMassCofirmations.setField(new Side('1'));// 交易方向， 本方对期权的买卖方向 1-买入 4-卖出
			}
			if ("2".equals(model.getNetGrossInd())) {
				noMassCofirmations.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
			} else {
				noMassCofirmations.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
			}
			noMassCofirmations.setField(new OptPremiumCurrency(model.getOptpremiumcurrency()));// 期权费货币
			noMassCofirmations.setField(new Currency1(model.getCurrency1()));// 基准货币名称
			noMassCofirmations.setField(new Currency1Amt(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getCurrency1amt()).doubleValue()),2),2,"0")));//基准货币金额， 单位为：元，精度为2
			noMassCofirmations.setField(new Currency2(model.getCurrency2()));//非基准货币名称
			noMassCofirmations.setField(new Currency2Amt(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getCurrency2amt()).doubleValue()),2),2,"0")));//非基准货币金额金额，单位：元，精度为 2
			noMassCofirmations.setField(new AgreementCurrency(model.getAgreementcurrency()));//期权货币
			if ("0".equals(model.getPutorcall())) {
				noMassCofirmations.setField(new PutOrCall('0'));//期权类型； 外汇成交单中的交易类型，枚举值：看涨期权、看跌期权 0-put 1-call
			}else if ("1".equals(model.getPutorcall())) {
				noMassCofirmations.setField(new PutOrCall('1'));//期权类型； 外汇成交单中的交易类型，枚举值：看涨期权、看跌期权 0-put 1-call
			}
			
			if(!"".equals(model.getStrikeprice())){
				noMassCofirmations.setField(new StrikePrice(BigDecimalUtil.fixZero(BigDecimalUtil.round(new BigDecimal(model.getStrikeprice()).doubleValue()/100,8),8,"0")));// 行权价格， 最大精度为 9
			}
			if ("0".equals(model.getDerivativeexercisestyle())) {
				noMassCofirmations.setField(new DerivativeExerciseStyle('0'));// 执行方式 0-欧式期权 1-美式期权
			}else {
				noMassCofirmations.setField(new DerivativeExerciseStyle('1'));// 执行方式 0-欧式期权 1-美式期权
			}
			noMassCofirmations.setField(new MaturityDate(model.getMaturitydate()));// 到期日，格式为：YYYYMMDD

			noMassCofirmations.setField(new ExpireTimeZone(model.getExpiretimezone()));// 时区，
			noMassCofirmations.setField(new DeliveryDate(model.getDeliverydate()));// 交割日，格式为：YYYYMMDD
			if ("1".equals(model.getOptsettlamttype())) {
				noMassCofirmations.setField(new OptSettlAmtType(1));// 1-全额交割 2-差额交割
				noMassCofirmations.setField(new ExpireTime(model.getExpiretime()));// 行权截止日： 格式为YYYYMMDD-HH:MM:SS 或YYYYMMDD-HH:MM:SS.sss，第 10-14位表示截止时间
				noMassCofirmations.setField(new BenchmarkCurveName("-"));
			}else {
				noMassCofirmations.setField(new ExpireTime(model.getExpiretime()));// 行权截止日： 格式为YYYYMMDD-HH:MM:SS 或YYYYMMDD-HH:MM:SS.sss，第 10-14位表示截止时间
//				String[] times = model.getExpiretime().split("-");
				noMassCofirmations.setField(new OptSettlAmtType(2));// 1-全额交割 2-差额交割
				if (StringUtils.isNotBlank(model.getBenchmarkcurvename())) {
					noMassCofirmations.setField(new BenchmarkCurveName(model.getBenchmarkcurvename()));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
				}else {
					noMassCofirmations.setField(new BenchmarkCurveName("1"));// 参考价：1-中间价  2-10 点参考价 3-11 点参考价 4-14 点参考价 5-15 点参考价
				}
			}
			noMassCofirmations.setField(new PaymentDate(model.getPaymentdate()));// 期权费支付日， 格式为：YYYYMMDD
			// MONEY =  2-百分比                        MONEYVMAIN =1-基点  
			if("Term%".equals(model.getOptpremiumbasis())){
				noMassCofirmations.setField(new OptPremiumBasis(2));// 期权费类型， 枚举值：Pips、Term% 1-基点 2-百分比
				if(!"".equals(model.getOptpremiumvalue())){
					noMassCofirmations.setField(new OptPremiumValue(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumvalue()).doubleValue()),9),9,"0")));// 期权价格，精度： 9 位
				}
			}else if ("Pips".equals(model.getOptpremiumbasis())) {
				noMassCofirmations.setField(new OptPremiumBasis(1));// 期权费类型， 枚举值：Pips、Term% 1-基点 2-百分比
				if(!"".equals(model.getOptpremiumvalue())){
					noMassCofirmations.setField(new OptPremiumValue(BigDecimalUtil.fixZero(BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumvalue()).doubleValue()),9),9,"0")));// 期权价格，精度： 9 位
				}
			}
			if(!"".equals(model.getOptpremiumamt())){
				noMassCofirmations.setField(new OptPremiumAmt(BigDecimalUtil.fixZero( BigDecimalUtil.round(Math.abs(new BigDecimal(model.getOptpremiumamt()).doubleValue()),2),2,"0")));// 期权费， 单位： 元，精度：2 位
			}
			pkgSettlInfo(noMassCofirmations, model);
			message.addGroup(noMassCofirmations);
		}
	}
}
