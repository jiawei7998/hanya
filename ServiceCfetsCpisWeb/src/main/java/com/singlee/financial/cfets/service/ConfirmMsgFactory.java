package com.singlee.financial.cfets.service;

import imix.field.MarketIndicator;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.pojo.TradeConfirmBean;

public class ConfirmMsgFactory {
	
	private SendForeignForwardSpot foreignForwardSpot;
	
	private SendForeignFxmm foreignFxmm;

	private SendForeignSwap foreignSwap;
	
	private SendInterestRateSwap interestRateSwap;
	
	private SendForeignOption foreignOption;
	
	private SendForeignCrs foreignCrs;

	public ConfirmMsgService getMsgService(TradeConfirmBean bean,ConfigBean configBean)throws Exception{
		if(MarketIndicator.FXSPT.equals(bean.getMarketIndicator())){
			return getForeignForwardSpot(configBean);
			
		}else if(MarketIndicator.FXFOW.equals(bean.getMarketIndicator())){
			return getForeignForwardSpot(configBean);

		}else if(MarketIndicator.FXSWP.equals(bean.getMarketIndicator())){
			return getForeignSwap(configBean);
			
		}else if(MarketIndicator.FXDEPO.equals(bean.getMarketIndicator())){
			return getForeignFxmm(configBean);
			
		}else if(MarketIndicator.FXCRS.equals(bean.getMarketIndicator())){
			return getForeignCrs(configBean);
			
		}else if(MarketIndicator.INTEREST_RATE_SWAP.equals(bean.getMarketIndicator())){
			return getInterestRateSwap(configBean);
			
		}else if(MarketIndicator.FXOPT.equals(bean.getMarketIndicator())){
			return getForeignOption(configBean);
			
		}else if(MarketIndicator.FXIRS.equals(bean.getMarketIndicator())){
			return getInterestRateSwap(configBean);
			
		}else if(MarketIndicator.GOLDSPT.equals(bean.getMarketIndicator())){
			return getForeignForwardSpot(configBean);
			
		}else if(MarketIndicator.GOLDFWD.equals(bean.getMarketIndicator())){
			return getForeignForwardSpot(configBean);
			
		}else if(MarketIndicator.GOLDSWP.equals(bean.getMarketIndicator())){
			return getForeignSwap(configBean);
			
		}else{
			throw new RuntimeException("不支持组装此类交易报文");
		}
	}
	
	
	
	public SendForeignForwardSpot getForeignForwardSpot(ConfigBean configBean) {
		if(foreignForwardSpot == null){
			foreignForwardSpot = new SendForeignForwardSpot(configBean);
		}
		return foreignForwardSpot;
	}

	public SendForeignFxmm getForeignFxmm(ConfigBean configBean) {
		if(foreignFxmm == null){
			foreignFxmm = new SendForeignFxmm(configBean);
		}
		return foreignFxmm;
	}

	public SendForeignSwap getForeignSwap(ConfigBean configBean) {
		if(foreignSwap == null){
			foreignSwap = new SendForeignSwap(configBean);
		}
		return foreignSwap;
	}

	public SendInterestRateSwap getInterestRateSwap(ConfigBean configBean) {
		if(interestRateSwap == null){
			interestRateSwap = new SendInterestRateSwap(configBean);
		}
		return interestRateSwap;
	}
	
	public SendForeignOption getForeignOption(ConfigBean configBean) {
		if (foreignOption == null) {
			foreignOption = new SendForeignOption(configBean);
		}
		return foreignOption;
	}
	
	public SendForeignCrs getForeignCrs(ConfigBean configBean) {
		if (foreignCrs == null) {
			foreignCrs = new SendForeignCrs(configBean);
		}
		return foreignCrs;
	}
}
