package com.singlee.financial.cfets.cpis;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.service.ConfirmMsgFactory;
import com.singlee.financial.cfets.service.SendForeignForwardSpot;
import com.singlee.financial.cfets.service.SendForeignFxmm;
import com.singlee.financial.cfets.service.SendForeignSwap;
import com.singlee.financial.model.SendForeignForwardSpotModel;
import com.singlee.financial.model.SendForeignFxmmModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.model.SendForeignSwapModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.util.ClassNameUtil;
import imix.*;
import imix.client.core.ImixSession;
import imix.client.core.Listener;
import imix.client.core.MessageCracker;
import imix.imix10.Logout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * CFETS消息监听服务
 * 
 * @author chengmj
 * 
 */
public class ImixCPISClientListener extends MessageCracker implements Listener {

    /**
     * 登录次数
     */
    public static int count = 0;

    @Autowired
    private ImixCPISClient imixCPISClient;
	@Resource
	private ImixCPISCfetsApiService imixCPISCfetsApiService;
    
    private ConfirmMsgFactory confirmMsgFactory;
    
	//收报线程队列管理
	private ThreadPoolExecutor socketServerThreadPool;
	// CFETS配置类
    private ConfigBean configBean;
    // 用来返回查询反馈结果的集合
    private List<Object> list = new ArrayList<Object>();
    // 用来返回查询的集合
    private List<Object> queryList = new ArrayList<Object>();

    
    private static Logger LogManager = LoggerFactory.getLogger(ImixCPISClientListener.class);

    public ConfigBean getConfigBean() {
        return configBean;
    }

    public void setConfigBean(ConfigBean configBean) {
        this.configBean = configBean;
    }
    @Override
    public void fromAdmin(Message message, ImixSession imixSession)
        throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue {
        LogManager.info("******* fromAdmin() **************");
        LogManager.debug((new StringBuilder("imixSession:")).append(imixSession).toString());
        LogManager.info("*******pre connection:" + count + "**********");


        try {
            LogManager.info("*******String(35):" + message.getHeader().getString(35).toString() + "**********");
        } catch (FieldNotFound e) {
            LogManager.error("fromAdmin:", e);
        }
        LogManager.info("**********************************");
        try {
            if ("5".equals(message.getHeader().getString(35))) {
                // 登出
                Logout logout = (Logout) message;
                // 5表示登录失败
                if (logout.isSetLogoutStatus()) {
                    LogManager.info("用户登录失败,失败代码:" + logout.getLogoutStatus() + "[17-密码过期]");
                } else if (logout.isSetSessionStatus()) {
                    LogManager.info("用户注销成功,代码:" + logout.getSessionStatus() + "[4-注销成功]");
                } else if (logout.isSetText()) {
                    LogManager.info("登录失败,失败代码:" + logout.getText()+ "[2-用户信息验证失败]");
                } else {
                    LogManager.info("登录失败,原因未知");
                }
            } else {
                count = 0;
                LogManager.info("***********************************");
                LogManager.info("******* 登录 成功 !************");
                LogManager.info("******* count reset to zero ! [" + count + "]******");
                LogManager.info("***********************************");
                
                imixCPISClient.getImixCPISAutoConn().setConnStatus(true);
            }
        } catch (FieldNotFound e) {
            LogManager.error("fromAdmin:", e);
            return;
        }
    }
    @Override
    public void fromApp(Message message, ImixSession imixSession)
        throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
    	
    	LogManager.info("********fromApp:" + message);
    	
		// 接收到消息后,开启一个线程
		getThreadPool().execute(new ImixCPISMessageProcess(message, imixCPISCfetsApiService, configBean,ImixCPISClientListener.this));        
    }
    @Override
    public void onError(ImixSession imixSession, int type) {
        LogManager.info("connection failure:" + (new StringBuilder("imixSession:")).append(imixSession).toString());
        // 断开 重连
        if (1 == type) {
            try {
                LogManager.info("*******系统捕获CpisClient错误消息,立即进行重新连接!**********");
                imixSession.start();
            } catch (ConfigError e) {
                LogManager.error("onError exception", e);
            }
        }

    }
    @Override
    public void onLogon(ImixSession imixSession) {
        LogManager.info("********onLogon********" + (new StringBuilder("imixSession:")).append(imixSession).toString());

    }
    @Override
    public void onLogout(ImixSession imixSession) {
        LogManager.info("onLogonOut:" + (new StringBuilder("imixSession:")).append(imixSession).toString());
        count++;
        LogManager.info("*******开始累计登录次数:" + count + "次**********");
        if (count > configBean.getReconCount()) {
            count = 0;
            LogManager.info("*******当前重连累计大于" + configBean.getReconCount() + "次,断开连接!**********");
            try {
                imixSession.stop();
            } catch (Exception e) {
                LogManager.error("停止服务异常", e);
            }
        }

//        //登陆失败,系统开始连接
//        try {
//        	if(imixCPISClient.getImixCPISAutoConn().isStopImixSessionBySystem()){
//        		imixCPISClient.getImixCPISAutoConn().setStopImixSessionBySystem(false);
//        		return;
//        	}
//
//        	Thread t=new Thread(new Runnable() {
//
//				@Override
//				public void run() {
//					// TODO Auto-generated method stub
//					imixCPISClient.getImixCPISAutoConn().setConnStatus(false);
//
//		        	try {
//						imixCPISClient.getImixCPISAutoConn().startConn();
//					} catch (Exception e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			});
//        	t.start();
//
//
//
//		} catch (Exception e) {
//			LogManager.error("连接出错:"+e.getMessage(),e);
//			e.printStackTrace();
//		}
    }
    @Override
    public void toApp(Message message, ImixSession imixSession) {
        LogManager.info("********toApp:" + message);
        LogManager.info("********" + (new StringBuilder("imixSession:")).append(imixSession).toString());
    }

    // 接受参数 组装报文 分发处理
    public Message packageMsg(Object object) throws Exception {
        Message message = null;

        String serviceType = ClassNameUtil.getClassName(object);
        SendForeignForwardSpot sendForeignForwardSpot = new SendForeignForwardSpot(configBean);
        SendForeignSwap sendForeignSwap = new SendForeignSwap(configBean);
        SendForeignFxmm sendForeignFxmm = new SendForeignFxmm(configBean);
        if ("SendForeignForwardSpotModel".equals(serviceType)) {
            message = sendForeignForwardSpot.packageMsg((SendForeignForwardSpotModel)object, configBean);
        } else if ("SendForeignForwardSpotModel".equals(serviceType)) {
            message = sendForeignForwardSpot.packageMsg((SendForeignForwardSpotModel)object, configBean);
        } else if ("SendForeignFxmmModel".equals(serviceType)) {
            message = sendForeignFxmm.packageMsg((SendForeignFxmmModel)object, configBean);
        } else if ("SendForeignSwapModel".equals(serviceType)) {
            message = sendForeignSwap.packageMsg((SendForeignSwapModel)object, configBean);
        }
        return message;
    }
    
    // 接受参数 组装报文 分发处理 
    /***
     * 重载方法
     */
    public Message packageMsg(TradeConfirmBean confirmBean) throws Exception {
        return getConfirmMsgFactory().getMsgService(confirmBean, configBean).pkgMessage(confirmBean);
    }
    
    // 组合期权特殊处理
    /***
     * 重载方法
     */
    public Message packageOptMsg(List<SendForeignOptionModel> confirmBeans) throws Exception {
    	if (confirmBeans.size() > 1) {
    		
    		return getConfirmMsgFactory().getMsgService(confirmBeans.get(0), configBean).pkgMessage(confirmBeans);
		}
        return getConfirmMsgFactory().getMsgService(confirmBeans.get(0), configBean).pkgMessage(confirmBeans.get(0));
    }

	public ConfirmMsgFactory getConfirmMsgFactory() {
		if(confirmMsgFactory == null){
			confirmMsgFactory = new ConfirmMsgFactory();
		}
		return confirmMsgFactory;
	}

	public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public List<Object> getQueryList() {
        return queryList;
    }

    public void setQueryList(List<Object> queryList) {
        this.queryList = queryList;
    }
    
    public void addToList(Object object) {
        this.list.add(object);
    }

    public void addToQueryList(Object object) {
        this.queryList.add(object);
    }
    
	public ImixCPISClient getImixCPISClient() {
		return imixCPISClient;
	}

	private ThreadPoolExecutor getThreadPool(){
		// 创建客户羰线程池管理对象
		if (null == socketServerThreadPool) {
			socketServerThreadPool = new ThreadPoolExecutor(configBean.getCorePoolSize(), configBean.getMaximumPoolSize(), configBean.getKeepAliveTime(), TimeUnit.SECONDS,
					new ArrayBlockingQueue<Runnable>(configBean.getArrayBlockingQueueSize()), new ThreadPoolExecutor.CallerRunsPolicy());
		}
		return socketServerThreadPool;
	}

}
