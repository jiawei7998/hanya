package com.singlee.financial.cfets.service;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.AffirmStatus;
import imix.field.CombinationID;
import imix.field.ConfirmID;
import imix.field.ExecID;
import imix.field.MarketID;
import imix.field.MarketIndicator;
import imix.field.MatchStatus;
import imix.field.QueryRequestID;
import imix.field.Text;
import imix.field.TradeDate;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.model.FailRecCPISFeedBackModel;
import com.singlee.financial.cfets.bean.RecCPISFeedBackModel;

/**
 * 
 * <P>RecCPISFeedBack.java</P>
 * <P>接收CFETS的消息反馈</P>
 * <P>Company: 新利信息科技公司</P>
 * @author pengqian
 * <P>          修改者姓名 修改内容说明</P>
 * @see     参考类1
 */
public class RecCPISFeedBack {
	
	private static Logger LogManager = LoggerFactory.getLogger(RecCPISFeedBack.class);
	/**
	 * 解析报文
	 * @param executionReport
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	public RecCPISFeedBackModel ExecutionReport(Message message) throws FieldNotFound, IOException {
		
		RecCPISFeedBackModel recCPISFeedBackModel = new RecCPISFeedBackModel();
		
		MessageUtil msgUtil = new MessageUtil(message);
		
		String	text1 = "。";//异常报文校验
		
		//确认编号
		String	confirmID = msgUtil.getStringValue(ConfirmID.FIELD);
		recCPISFeedBackModel.setConfirmID(confirmID);
			
		//成交编号
		String	execID = msgUtil.getStringValue(ExecID.FIELD);
		
		String  combinationID=msgUtil.getStringValue(CombinationID.FIELD);
		//市场类型	
		String	marketIndicator = msgUtil.getStringValue(MarketIndicator.FIELD);
		if("".equals(marketIndicator)){
			text1=text1+"市场类型为空,";
		}
		if("".equals(confirmID)){
			//确认编号为空
			LogManager.info("******* 会员上传，接收反馈报文确认编号为空************");
		}else{
			if("19".equals(marketIndicator)){
				if("".equals(execID)&&"".equals(combinationID)){
					text1=text1+"期权成交编号为空,";
				}
			}else{
				if("".equals(execID)){
					text1=text1+"成交编号为空,";
				}else{
					if(!confirmID.contains(execID)){
						text1=text1+"成交编号与确认编号不一致,";
					}
				}
			}
		}
		if("19".equals(marketIndicator)){
			if("".equals(combinationID)){
				recCPISFeedBackModel.setExecID(execID);
			}else{
				recCPISFeedBackModel.setExecID(combinationID);
			}
			
		}else{
			recCPISFeedBackModel.setExecID(execID);
		}
		
			
		//校验状态	
		String	affirmStatus = msgUtil.getStringValue(AffirmStatus.FIELD);
		if("".equals(affirmStatus)){
			//校验状态为空
			affirmStatus="ERR";//异常报文，定时任务下次不再执行
			text1=text1+"校验状态为空,";
		}else{
			if(!",103,104,100,109,0,2,112,".contains(","+affirmStatus+",")){
				text1=text1+"无效校验状态为"+affirmStatus+",";
				affirmStatus="ERR";//异常报文，定时任务下次不再执行
			}
		}
		
		recCPISFeedBackModel.setAffirmStatus(affirmStatus);
		
		//成交日期
		String	tradeDate = msgUtil.getStringValue(TradeDate.FIELD);
		if("".equals(tradeDate)){
			text1=text1+"成交日期为空,";
		}
		recCPISFeedBackModel.setTradeDate(tradeDate);

		//交易场所	
		String	marketID = msgUtil.getStringValue(MarketID.FIELD);
		if("".equals(marketID)){
			text1=text1+"交易场所为空,";
		}
		recCPISFeedBackModel.setMarketID(marketID);
	

		recCPISFeedBackModel.setMarketIndicator(marketIndicator);
		//查询ID
		recCPISFeedBackModel.setQueryRequestID(msgUtil.getStringValue(QueryRequestID.FIELD));
		//匹配状态
		recCPISFeedBackModel.setMatchStatus(msgUtil.getStringValue(MatchStatus.FIELD));
		//未匹配字段
		String text = msgUtil.getStringValue(Text.FIELD);
		if(!"。".equals(text1)){
			LogManager.info("******* "+text1+"************");
			text=text+text1+"请到交易后平台处理";
		}
		recCPISFeedBackModel.setText(text);
		return recCPISFeedBackModel;
	}

	public FailRecCPISFeedBackModel ExecutionSendErrReport(Message message) throws FieldNotFound {
		
		FailRecCPISFeedBackModel failRecCPISFeedBackModel = new FailRecCPISFeedBackModel();
			
		MessageUtil msgUtil = new MessageUtil(message);
		
		String	confirmID = msgUtil.getStringValue(ConfirmID.FIELD);// 对应确认报文的编号
		
		failRecCPISFeedBackModel.setConfirmID(confirmID);

		String	execID = msgUtil.getStringValue(ExecID.FIELD); // 成交编号，需求未约束编码规则
		
		failRecCPISFeedBackModel.setExecID(execID);
		
		String	tradeDate = msgUtil.getStringValue(TradeDate.FIELD);
		
		failRecCPISFeedBackModel.setTradeDate(tradeDate);// 成交日期，格式为：YYYYMMDD
		
		String	marketID = msgUtil.getStringValue(MarketID.FIELD);
		
		failRecCPISFeedBackModel.setMarketID(marketID);// 在交易中心完成的交易
	
		String	marketIndicator = msgUtil.getStringValue(MarketIndicator.FIELD);
		
		failRecCPISFeedBackModel.setMarketIndicator(marketIndicator);
			
		String	queryRequestID = msgUtil.getStringValue(QueryRequestID.FIELD);
		failRecCPISFeedBackModel.setQueryRequestID(queryRequestID);
		
		String	matchStatus = msgUtil.getStringValue(MatchStatus.FIELD);
		failRecCPISFeedBackModel.setMatchStatus(matchStatus);
		
		failRecCPISFeedBackModel.setAffirmStatus("100");
		
		failRecCPISFeedBackModel.setText("Outofservice；消息发送失败");


		return failRecCPISFeedBackModel;
	}
}
