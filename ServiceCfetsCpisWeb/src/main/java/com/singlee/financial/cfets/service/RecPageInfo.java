package com.singlee.financial.cfets.service;


import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.Message;
import imix.field.ConfirmType;
import imix.field.CurrentNumReports;
import imix.field.LastRptCurrPage;
import imix.field.LastRptRequested;
import imix.field.MsgNumofCurrPage;
import imix.field.QueryPageNumber;
import imix.field.QueryRequestID;
import imix.field.QueryStartNumber;
import imix.field.TotNumReportPages;
import imix.field.TotNumReports;

/**
 * 
 * <P>
 * RecPageInfo.java
 * </P>
 * <P>
 * 接收查询返回报文的分页信息处理类
 * </P>
 * <P>
 * Copyright: Copyright (c) 2018
 * </P>
 * <P>
 * Company: 新利
 * </P>
 * <P>
 * 修改者姓名 修改内容说明
 * </P>
 * @author pengqian
 * 
 * @see 参考类1
 */
public class RecPageInfo {

	/**
	 * 解析分页信息存放到 分页对象中
	 * 
	 * @param message
	 * @return
	 * @throws FieldNotFound
	 */
	public static RecPageInfoModel msgPageToModel(Message message) throws FieldNotFound {

		MessageUtil msgUtil = new MessageUtil(message);
		RecPageInfoModel pageInfo = new RecPageInfoModel();

		String confirmType = msgUtil.getStringValue(ConfirmType.FIELD);
		pageInfo.setConfirmType(confirmType);

		String queryRequestID = msgUtil.getStringValue(QueryRequestID.FIELD);
		pageInfo.setQueryRequestID(queryRequestID);

		if (confirmType != null && !"".equals(confirmType) && !"".equals(queryRequestID)) {
			pageInfo.setQueryStartNumber(msgUtil.getStringValue(QueryStartNumber.FIELD));

			pageInfo.setTotNumReports(msgUtil.getStringValue(TotNumReports.FIELD));

			pageInfo.setTotNumReportPages(msgUtil.getStringValue(TotNumReportPages.FIELD));

			pageInfo.setQueryPageNumber(msgUtil.getStringValue(QueryPageNumber.FIELD));

			pageInfo.setMsgNumofCurrPage(msgUtil.getStringValue(MsgNumofCurrPage.FIELD));

			pageInfo.setCurrentNumReports(msgUtil.getStringValue(CurrentNumReports.FIELD));

			pageInfo.setLastRptCurrPage(msgUtil.getStringValue(LastRptCurrPage.FIELD));

			pageInfo.setLastRptRequested(msgUtil.getStringValue(LastRptRequested.FIELD));

		}
		return pageInfo;
	}
}
