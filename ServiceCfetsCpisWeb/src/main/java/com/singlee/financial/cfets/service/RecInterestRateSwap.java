package com.singlee.financial.cfets.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecInterestRateSwapModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.CalculateAgency;
import imix.field.CnfmTime;
import imix.field.CnfmTimeType;
import imix.field.ConfirmID;
import imix.field.ConfirmStatus;
import imix.field.ConfirmTransType;
import imix.field.ConfirmType;
import imix.field.ContactInfoID;
import imix.field.ContactInfoIDType;
import imix.field.CouponPaymentDateReset;
import imix.field.DealTransType;
import imix.field.EndDate;
import imix.field.ExecID;
import imix.field.ExecType;
import imix.field.FirstPeriodStartDate;
import imix.field.InterestAccuralDaysAdjustment;
import imix.field.LastQty;
import imix.field.LegBenchmarkCurveName;
import imix.field.LegBenchmarkSpread;
import imix.field.LegCouponPaymentDate;
import imix.field.LegCouponPaymentFrequency;
import imix.field.LegDayCount;
import imix.field.LegInterestAccrualDate;
import imix.field.LegInterestAccrualMethod;
import imix.field.LegInterestAccrualResetFrequency;
import imix.field.LegPrice;
import imix.field.LegPriceType;
import imix.field.LegSide;
import imix.field.MarketID;
import imix.field.MarketIndicator;
import imix.field.NetGrossInd;
import imix.field.NoContactInfos;
import imix.field.NoPartyIDs;
import imix.field.NoPartySubIDs;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.SettlementStatus;
import imix.field.Side;
import imix.field.StartDate;
import imix.field.Symbol;
import imix.field.Text;
import imix.field.TradeDate;
import imix.field.TradeLimitDays;
import imix.field.TradeMethod;
import imix.field.TrdType;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;

/**
 * 
 * <P>RecInterestRateSwap.java</P>
 * <P>接收利率互换的推送消息处理类</P>
 * <P>接收利率互换清算状态变更的推送消息处理类</P>
 * <P>Company:新利<P>
 * @author  彭前
 * @see     参考类1
 */
public class RecInterestRateSwap {

	/**
	 * 解析报文
	 * @param message
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	private static Logger LogManager = LoggerFactory.getLogger(RecForeignSwap.class);
	public RecInterestRateSwapModel ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		MessageUtil msgUtil = new MessageUtil(message);
		RecInterestRateSwapModel model = new RecInterestRateSwapModel();
	
		try{
			String confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);//确认报文ID
			
			model.setConfirmID(confirmID);
		
			String	confirmType  = msgUtil.getStringValue(ConfirmType.FIELD);//确认类型
			
			model.setConfirmType(confirmType);
			
			String	confirmTransType  = msgUtil.getStringValue(ConfirmTransType.FIELD);//交易确认报文类型
			
			model.setConfirmTransType(confirmTransType);
			
			String	confirmStatus  = msgUtil.getStringValue(ConfirmStatus.FIELD);//确认状态
			
			model.setConfirmStatus(confirmStatus);
			
			String	execType  = msgUtil.getStringValue(ExecType.FIELD);//成交状态
			
			model.setExecType(execType);
			
			String	dealTransType  = msgUtil.getStringValue(DealTransType.FIELD);
			
			model.setDealTransType(dealTransType);//交易处理类型
			
			String	marketID  = msgUtil.getStringValue(MarketID.FIELD);//交易场所
			
			model.setMarketID(marketID);
			
			String	execID  = msgUtil.getStringValue(ExecID.FIELD);//成交编号
			
			model.setExecID(execID);
			
			String	lastQty  = msgUtil.getStringValue(LastQty.FIELD);//名义本金（元） ；		
			
			model.setLastQty(lastQty);
			
			String	symbol  = msgUtil.getStringValue(Symbol.FIELD);//产品名称		
			
			model.setSymbol(symbol);
			
			String	text  = msgUtil.getStringValue(Text.FIELD);//未匹配字段		
			
			model.setText(text);
			
			String	startDate  = msgUtil.getStringValue(StartDate.FIELD);//起息日	
			
			model.setStartDate(startDate);
			
			String	tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);//成交日期
			
			model.setTradeDate(tradeDate);
			
			String	endDate  = msgUtil.getStringValue(EndDate.FIELD);//到期日
			
			model.setEndDate(endDate);
			
			String	netGrossInd  = msgUtil.getStringValue(NetGrossInd.FIELD);//清算方式	
			
			model.setNetGrossInd(netGrossInd);
			
			String	settlementStatus  = msgUtil.getStringValue(SettlementStatus.FIELD);//清算状态
			
			model.setSettlementStatus(settlementStatus);
			
			String	couponPaymentDateReset  = msgUtil.getStringValue(CouponPaymentDateReset.FIELD);//支付日调整
			
			model.setCouponPaymentDateReset(couponPaymentDateReset);
			
			String	interestAccuralDaysAdjustment  = msgUtil.getStringValue(InterestAccuralDaysAdjustment.FIELD);//计息天数调整			
			
			model.setInterestAccuralDaysAdjustment(interestAccuralDaysAdjustment);
			
			String	firstPeriodStartDate  = msgUtil.getStringValue(FirstPeriodStartDate.FIELD);//首期起息日			
			
			model.setFirstPeriodStartDate(firstPeriodStartDate);
	
			String	marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);//交易类型
			
			model.setMarketIndicator(marketIndicator);
			
			String	tradeLimitDays  = msgUtil.getStringValue(TradeLimitDays.FIELD);//期限	
			
			model.setTradeLimitDays(tradeLimitDays);
			
			String	tradeMethod  = msgUtil.getStringValue(TradeMethod.FIELD);			
			
			model.setTradeMethod(tradeMethod);//交易方式
						
			String	trdType  = msgUtil.getStringValue(TrdType.FIELD);			
			
			model.setTrdType(trdType);//交易方式
			
			String	calculateAgency  = msgUtil.getStringValue(CalculateAgency.FIELD);//计算机构
			
			model.setCalculateAgency(calculateAgency);
			
			NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
						
			CnfmTimeType cnfmTimeType = new CnfmTimeType();
			
			CnfmTime cnfmTime = new CnfmTime();
			
			int ownCnfmTimeType = 0 ;
			String ownCnfmTime = "";
			int othCnfmTimeType = 0 ;
			String othCnfmTime = "";
				
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
				message.getGroup(i, noCnfmTimes);
				cnfmTimeType = noCnfmTimes.getCnfmTimeType();
				cnfmTime = noCnfmTimes.getCnfmTime();
				if (cnfmTimeType.getValue() == 1) {
					ownCnfmTimeType = cnfmTimeType.getValue();
					ownCnfmTime = cnfmTime.getValue();
					model.setOwnCnfmTime(ownCnfmTime);
				}else if (cnfmTimeType.getValue() == 2) {
					othCnfmTimeType = cnfmTimeType.getValue();
					othCnfmTime = cnfmTime.getValue();
					model.setOthCnfmTime(othCnfmTime);
				}
			}
			
			String	side  = msgUtil.getStringValue(Side.FIELD);//交易方向	
			
			model.setSide(side);
			
			ExecutionReport.NoLegs noLegs = new ExecutionReport.NoLegs();
		
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoLegs.FIELD); i++) {
				message.getGroup(i, noLegs);
				if (Integer.parseInt(msgUtil.getStringValue(noLegs,LegPriceType.FIELD))==3) {
					//固 定 
					String	fixedLegSide = msgUtil.getStringValue(noLegs,LegSide.FIELD);
					
					model.setFixedLegSide(fixedLegSide);
					
					String	fixedLegPriceType = msgUtil.getStringValue(noLegs,LegPriceType.FIELD);//固 定 
					
					model.setFixedLegPriceType(fixedLegPriceType);
					
					String	fixedLegPrice = msgUtil.getStringValue(noLegs,LegPrice.FIELD);//固 定 利 率 ， 单位：%，精度：4位
					
					model.setFixedLegPrice(fixedLegPrice);
				
					String	fixedLegCouponPaymentFrequency = msgUtil.getStringValue(noLegs,LegCouponPaymentFrequency.FIELD); //固定利率支付周期0-半年；1-年；2-到期； 3-月； 4-季；5-两周；6-周；7-天；9-九个月
					
					model.setFixedLegCouponPaymentFrequency(fixedLegCouponPaymentFrequency);
					
					String	fixedLegCouponPaymentDate = msgUtil.getStringValue(noLegs,LegCouponPaymentDate.FIELD); //固定利率支付日
					
					model.setFixedLegCouponPaymentDate(fixedLegCouponPaymentDate);
					
					String	fixedLegDayCount = msgUtil.getStringValue(noLegs,LegDayCount.FIELD); //固定利率计息基准0-实际/实际；1-实 际 /360 ；2-30/360； 3-实际/365； D-30E/360；5-实际/365F
					
					model.setFixedLegDayCount(fixedLegDayCount);
						
					String	fixedLegInterestAccrualMethod =msgUtil.getStringValue(noLegs,LegInterestAccrualMethod.FIELD); //固定利率计息方法 0-单利 1-复利
					
					model.setFixedLegInterestAccrualMethod(fixedLegInterestAccrualMethod);
					
				}else if (Integer.parseInt(msgUtil.getStringValue(noLegs,LegPriceType.FIELD))==6) {
				
					String	floatingLegSide = msgUtil.getStringValue(noLegs,LegSide.FIELD);
					
					model.setFloatingLegSide(floatingLegSide);
						
					String	floatingLegPriceType = msgUtil.getStringValue(noLegs,LegPriceType.FIELD);//浮动
					
					model.setFloatingLegPriceType(floatingLegPriceType);
					
					String	floatingLegBenchmarkCurveName = msgUtil.getStringValue(noLegs,LegBenchmarkCurveName.FIELD);//参考利率
					
					model.setFloatingLegBenchmarkCurveName(floatingLegBenchmarkCurveName);
					
					String	floatingLegBenchmarkSpread = msgUtil.getStringValue(noLegs,LegBenchmarkSpread.FIELD);//利差，单位：bps，精度：2 位
					
					model.setFloatingLegBenchmarkSpread(floatingLegBenchmarkSpread);
					
					String	floatingLegCouponPaymentDate = msgUtil.getStringValue(noLegs,LegCouponPaymentDate.FIELD); //浮动利率支付日
					
					model.setFloatingLegCouponPaymentDate(floatingLegCouponPaymentDate);
					
					String	floatingLegCouponPaymentFrequency = msgUtil.getStringValue(noLegs,LegCouponPaymentFrequency.FIELD);//浮动利率支付周期0-半年；1-年；2-到期； 3-月； 4-季；5-两周；6-周；7-天；9-九个月
					
					model.setFloatingLegCouponPaymentFrequency(floatingLegCouponPaymentFrequency);
					
					String	floatingLegInterestAccrualDate = msgUtil.getStringValue(noLegs,LegInterestAccrualDate.FIELD);//利率确定
					
					model.setFloatingLegInterestAccrualDate(floatingLegInterestAccrualDate);
					
					String	floatingLegInterestAccrualResetFrequency = msgUtil.getStringValue(noLegs,LegInterestAccrualResetFrequency.FIELD);//重置频率 0-半年；1-年；2-到期； 3-月； 4-季；5-两周；6-周；7-天；9-九个月
					
					model.setFloatingLegInterestAccrualResetFrequency(floatingLegInterestAccrualResetFrequency);
					
					String	floatingLegInterestAccrualMethod =msgUtil.getStringValue(noLegs,LegInterestAccrualMethod.FIELD);//浮动利率计息方法 0-单利 1-复利
					
					model.setFloatingLegInterestAccrualMethod(floatingLegInterestAccrualMethod);
					
					String	floatingLegDayCount =msgUtil.getStringValue(noLegs,LegDayCount.FIELD);//浮动利计息基准0-实际/实际；1-实 际 /360 ；2-30/360； 3-实际/365； D-30E/360；5-实际/365F
					
					model.setFloatingLegDayCount(floatingLegDayCount);
				}
			}

			int fixedPartyRole = 0 ; //固定利率支付			
			int floatingPartyRole = 0 ; // 固定利率收取方  (浮动利率支付方）
			
			int partyrole=-1; //本方对手方
			
			String partyid="";//21位机构码
			
			String partysubid="";
		    
		    int partysubidtype=-1;
			
			ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
			ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			int nopartysubids=-1;//清算信息循环组
			
			
			
			for (int k = 1; k <= msgUtil.getIntValue(NoPartyIDs.FIELD); k++) {
				
				ExecutionReport.NoPartyIDs.NoContactInfos noContactInfos1 = new ExecutionReport.NoPartyIDs.NoContactInfos();
//				NoContactInfos noContactInfos = new NoContactInfos();
				int noContactInfos=0;
				
				message.getGroup(k, nopartyids1);
				
				partyrole=msgUtil.getIntValue(nopartyids1, PartyRole.FIELD);

				partyid=msgUtil.getStringValue(nopartyids1, PartyID.FIELD);
				
				if(partyrole==119){
					
					nopartyids1.getGroup(k, noContactInfos1);
					
					noContactInfos=noContactInfos1.isSetField(NoContactInfos.FIELD)?noContactInfos1.getInt(NoContactInfos.FIELD) :0;
					
					for (int i = 1; i <= noContactInfos; i++) {
						
						int contactInfoIDType=noContactInfos1.isSetField(ContactInfoIDType.FIELD)?noContactInfos1.getInt(ContactInfoIDType.FIELD):0;
						
						String contactInfoID=noContactInfos1.isSetField(ContactInfoID.FIELD)?noContactInfos1.getString(ContactInfoID.FIELD) : "";
						
						if (contactInfoIDType == 6) {
							String	fixedTel = contactInfoID;//电话
							
							model.setFixedTel(fixedTel);
						}
						if (contactInfoIDType == 8) {
							String	fixedFax = contactInfoID;//传真
							
							model.setFixedFax(fixedFax);
						}
					}

										
					String	fixedPartyID = partyid;	//机构 21 位码
					
					model.setFixedPartyID(fixedPartyID);
					
					fixedPartyRole = partyrole;//固定利率支付方

					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
				
					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						if (partysubidtype == 9) {
							String	fixedUser =  partysubid;//联系人
							
							model.setFixedUser(fixedUser);
						 
						}else if (partysubidtype == 124) {
							String	fixedBrhName =  partysubid;//机构中文全称
							
							model.setFixedBrhName(fixedBrhName);
						 
						}else if (partysubidtype == 15) {
							String	fixedAccount =  partysubid;//资金账号
							
							model.setFixedAccount(fixedAccount);
						 
						}else if (partysubidtype == 23) {
							String	fixedAccountName =  partysubid;//资金账户户名
							
							model.setFixedAccountName(fixedAccountName);
						 
						}else if (partysubidtype == 110) {
							String	fixedCapitalBankName =  partysubid;//资金开户行
							
							model.setFixedCapitalBankName(fixedCapitalBankName);
						 
						}else if (partysubidtype == 112) {
							String	fixedPayBankNo =  partysubid;//支付系统行号
							
							model.setFixedPayBankNo(fixedPayBankNo);
						 
						}
					}
				}else if (partyrole==120) {
					
					nopartyids1.getGroup(k, noContactInfos1);
					
					noContactInfos=noContactInfos1.isSetField(NoContactInfos.FIELD)?noContactInfos1.getInt(NoContactInfos.FIELD) :0;
					
					for (int i = 1; i <= noContactInfos; i++) {
						int contactInfoIDType=noContactInfos1.isSetField(ContactInfoIDType.FIELD)?noContactInfos1.getInt(ContactInfoIDType.FIELD):0;
						
						String contactInfoID=noContactInfos1.isSetField(ContactInfoID.FIELD)?noContactInfos1.getString(ContactInfoID.FIELD) : "";
						
						if (contactInfoIDType == 6) {
							String	fixedTel = contactInfoID;//电话
							
							model.setFixedTel(fixedTel);
						}
						if (contactInfoIDType == 8) {
							String	fixedFax = contactInfoID;//传真
							
							model.setFixedFax(fixedFax);
						}
					}
					
					String	floatingPartyID = partyid;//机构 21 位码
					
					model.setFloatingPartyID(floatingPartyID);
					
					floatingPartyRole = partyrole; // 固定利率收取方  (浮动利率支付方）
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						if (partysubidtype == 9) {
						String	floatingUser =  partysubid;//联系人
						
						model.setFloatingUser(floatingUser);
						 
						}else if (partysubidtype == 124) {
							String	floatingBrhName =  partysubid;//机构中文全称
						
							model.setFloatingBrhName(floatingBrhName);
						 
						}else if (partysubidtype == 15) {
							String	floatingAccount =  partysubid;//资金账号
							
							model.setFloatingAccount(floatingAccount);
						 
						}else if (partysubidtype == 23) {
							String	floatingAccountName =  partysubid;//资金账户户名
							
							model.setFloatingAccountName(floatingAccountName);
						 
						}else if (partysubidtype == 110) {
							String	floatingCapitalBankName =  partysubid; //资金开户行
							
							model.setFloatingCapitalBankName(floatingCapitalBankName);
						 
						}else if (partysubidtype == 112) {
							String	floatingPayBankNo =  partysubid;//支付系统行号
							
							model.setFloatingPayBankNo(floatingPayBankNo);
						 
						}
					}
				
				}
			}
			
			//存放分页信息
			 RecPageInfoModel pageInfoModel = RecPageInfo.msgPageToModel(message);
			 
			 model.setPageInfo(pageInfoModel);

		} catch (Exception e) {
			LogManager.info("解析报文出错"+e);
			throw e;
		}
		return model;
	}
	

}
