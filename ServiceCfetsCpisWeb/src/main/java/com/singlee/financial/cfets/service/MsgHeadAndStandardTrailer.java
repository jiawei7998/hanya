package com.singlee.financial.cfets.service;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.util.DateUtil;
import com.singlee.financial.util.PropertiesUtil;
import com.singlee.financial.util.SequenceUtil;
import imix.FieldNotFound;
import imix.field.*;
import imix.imix10.Message;

/**
 * 
 * <p>
 * 报文头和报文尾处理类
 * </p>
 * <p>
 * Company:新利信息科技有限公司
 * </p>
 * 
 * @author 彭前
 * 
 */

public class MsgHeadAndStandardTrailer {
	
	/**
	 * 拼装报文头和报文尾
	 * 
	 * @param MsgType
	 * @return
	 */
	public static Message packageMsgHeadAndStandardTrailer(String MsgType, ConfigBean configBean) {
		Message message = new Message();
		message.getHeader().setField(new MsgType(MsgType));// AK 报文类型
		message.getHeader().setField(
				new MsgSeqNum(SequenceUtil.getSeqInt("MsgSeqNum")));// 1 报文序号
		message.getHeader().setField(
				new SenderCompID(configBean.getSendercompid()));// XXXX 报文发送方， 为机构，
		// 取决于连接配置文件中的SenderCompID，
		message.getHeader().setField(
				new SenderSubID(configBean.getUserName()));// XXXX 发送方子标识， 为用户名，
		// 取决于连接配置文件中的SenderSubID
		message.getHeader().setField(
				new TargetCompID(configBean.getMarket()));// CFETS-RMB-TPP
		//new DateUtil();
		// 报文接收方
		message.getHeader().setField(new PossDupFlag(false));//可能重复标志，重复单送时，做此标记
		message.getHeader().setField(new SendingTime(DateUtil.getDateTime()));// 20061122-14:15:12报文发送时间，
		// 格式
		// ：YYYYMMDD-HH:MM:SS
		// 或
		// 者YYYYMMDD-HH:MM:SS.SSS
		message.getTrailer().setField(new SignatureLength());// 数字签名长度（不可加密）
		message.getTrailer().setField(new Signature());// 数字签名（不可加密）
		message.getTrailer().setField(new CheckSum());// 校验和;报文的最末域
		return message;
	}

	/**
	 * 解析报文头存放到 properties 中
	 * 
	 * @param message
	 * @param prop
	 * @return
	 * @throws FieldNotFound
	 */
	public static PropertiesUtil msgHeadToProp(imix.Message message,
			PropertiesUtil prop) throws FieldNotFound {

		MessageUtil msgUtil = new MessageUtil(message);
		String beginString = "";
		beginString = msgUtil.getHeadStringValue(BeginString.FIELD);
		String bodyLength = "";
		bodyLength = msgUtil.getHeadStringValue(BodyLength.FIELD);
		String msgType = "";
		msgType = msgUtil.getHeadStringValue(MsgType.FIELD);
		String senderCompID = "";
		senderCompID = msgUtil.getHeadStringValue(SenderCompID.FIELD);
		String targetCompID = "";
		targetCompID = msgUtil.getHeadStringValue(TargetCompID.FIELD);
		String targetSubID = "";
		targetSubID = msgUtil.getHeadStringValue(TargetSubID.FIELD);
		String msgSeqNum = "";
		msgSeqNum = msgUtil.getHeadStringValue(MsgSeqNum.FIELD);
		String sendingTime = "";
		sendingTime = msgUtil.getHeadStringValue(SendingTime.FIELD);
		String deliverToCompID = "";
		deliverToCompID = msgUtil.getHeadStringValue(DeliverToCompID.FIELD);
		String deliverToSubID = "";
		deliverToSubID = msgUtil.getHeadStringValue(DeliverToSubID.FIELD);
		String onBehalfOfCompID = "";
		onBehalfOfCompID = msgUtil.getHeadStringValue(OnBehalfOfCompID.FIELD);

		prop.setProperties("BeginString", beginString); // 协议包版本
		prop.setProperties("BodyLength", bodyLength); // 报文长度
		prop.setProperties("MsgType", msgType); // 报文类型
		prop.setProperties("SenderCompID", senderCompID); // 发送方为交易确认系统
		prop.setProperties("TargetCompID", targetCompID); // 报文接收方，为机构
		prop.setProperties("TargetSubID", targetSubID); // 报文接收方子标识，为会员
		prop.setProperties("MsgSeqNum", msgSeqNum); // 报文序列号
		prop.setProperties("SendingTime", sendingTime); // 报文发送时间
		prop.setProperties("DeliverToCompID", deliverToCompID); // 为机构，取决于连接配置文件中的SenderCompID
		prop.setProperties("DeliverToSubID", deliverToSubID); // 为用户名，取决于连接配置文件中的SenderSubID
		prop.setProperties("OnBehalfOfCompID", onBehalfOfCompID); // 最初发送方标识，会员接收时传输

		return prop;
	}

}
