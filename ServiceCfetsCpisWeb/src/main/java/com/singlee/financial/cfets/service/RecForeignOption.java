package com.singlee.financial.cfets.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecForeignOptionModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.AgreementCurrency;
import imix.field.BenchmarkCurveName;
import imix.field.CnfmTime;
import imix.field.CnfmTimeType;
import imix.field.CombinationID;
import imix.field.ConfirmID;
import imix.field.ConfirmStatus;
import imix.field.Currency1;
import imix.field.Currency1Amt;
import imix.field.Currency2;
import imix.field.Currency2Amt;
import imix.field.DeliveryDate;
import imix.field.DerivativeExerciseStyle;
import imix.field.ExecID;
import imix.field.ExpireTime;
import imix.field.ExpireTimeZone;
import imix.field.MarketIndicator;
import imix.field.MaturityDate;
import imix.field.NetGrossInd;
import imix.field.NoPartySubIDs;
import imix.field.OPTDataType;
import imix.field.OptPayoutType;
import imix.field.OptPremiumAmt;
import imix.field.OptPremiumBasis;
import imix.field.OptPremiumCurrency;
import imix.field.OptPremiumValue;
import imix.field.OptSettlAmtType;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.PaymentDate;
import imix.field.PutOrCall;
import imix.field.Side;
import imix.field.StrikePrice;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;
import imix.imix10.MassCofirmation.NoMassCofirmations;


/**
 * 
 * <P>RecForeignOption.java</P>
 * <P>接收外汇期权的消息推送处理类</P>
 * @see     参考类1
 */
public class RecForeignOption {

	/**
	 * 解析报文
	 * @param message
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	private static Logger LogManager = LoggerFactory.getLogger(RecForeignFxmm.class);
	public List<RecForeignOptionModel> ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		List<RecForeignOptionModel> list=new ArrayList<RecForeignOptionModel>();
		
		RecPageInfoModel pageInfo = new RecPageInfoModel();
		MessageUtil msgUtil = new MessageUtil(message);
		try {
			String confirmID = msgUtil.getStringValue(ConfirmID.FIELD);
			String marketIndicator = msgUtil.getStringValue(MarketIndicator.FIELD);
			String optDataType = msgUtil.getStringValue(OPTDataType.FIELD);
			String combinationID = msgUtil.getStringValue(CombinationID.FIELD);
			String optPayoutType = msgUtil.getStringValue(OptPayoutType.FIELD);
			String confirmStatus = msgUtil.getStringValue(ConfirmStatus.FIELD);
			//获取本方对手方确认时间
			NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
			CnfmTimeType cnfmTimeType = new CnfmTimeType();
			CnfmTime cnfmTime = new CnfmTime();
			int ownCnfmTimeType = 0 ;
			String ownCnfmTime = "";
			int othCnfmTimeType = 0 ;
			String othCnfmTime = "";
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
				message.getGroup(i, noCnfmTimes);
				cnfmTimeType = noCnfmTimes.getCnfmTimeType();
				cnfmTime = noCnfmTimes.getCnfmTime();
				if (cnfmTimeType.getValue() == 1) {
					ownCnfmTimeType = cnfmTimeType.getValue();
					ownCnfmTime = cnfmTime.getValue();
				}else if (cnfmTimeType.getValue() == 2) {
					othCnfmTimeType = cnfmTimeType.getValue();
					othCnfmTime = cnfmTime.getValue();
				}
				
			}
			String tradeDate = "";
			String side = "";
			String optPremiumCurrency = "";
			String currency1 = "";
			String currency1Amt = "";
			String currency2 = "";
			String currency2Amt = "";
			String calculatedCcyLastTradeAmt = "";
			String agreementCurrency = "";
			String putOrCall = "";
			String strikePrice = "";
			String exerciseStyle = "";
			String maturityDate = "";
			String benchmarkCurveName = "";
			String expireTime = "";
			String expireTimeZone = "";
			String deliveryDate = "";
			String optSettlAmtType = "";
			String paymentDate = "";
			String optPremiumBasis = "";
			String optPremiumValue = "";
			String optPremiumAmt = "";
			String execID = "";
			String netGrossInd = "";
			String derivativeExerciseStype = "";
			
			String ownPartyID = ""; //本方机构 21 位码
			int ownPartyRole = 0 ;
			
			String othPartyID = "";//对手方机构 21 位码
			int othPartyRole = 0 ;
			
			String optionCostCapitalBakeName = ""; //期权费收取方-资金开户行名称
			String optionCostCapitalAccountName = ""; //期权费收取方-资金账户名称
			String optionCostPayBankNo = "";//期权费收取方-支付系统行号
			String optionCostCapitalAccount = ""; //期权费收取方-资金账号
			String optionCostRemark = ""; //期权费收取方-附言
			
			NoMassCofirmations noMassCofirmations = new NoMassCofirmations();
			//MassCofirmation massCofirmation = new MassCofirmation();
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoMassCofirmations.FIELD); i++) {
				RecForeignOptionModel model = new RecForeignOptionModel();
				message.getGroup(i, noMassCofirmations);
				execID = getStringValue(noMassCofirmations, ExecID.FIELD);
				tradeDate = getStringValue(noMassCofirmations, TradeDate.FIELD);
				side = getStringValue(noMassCofirmations, Side.FIELD);
				netGrossInd = getStringValue(noMassCofirmations, NetGrossInd.FIELD);
				putOrCall = getStringValue(noMassCofirmations, PutOrCall.FIELD);
				agreementCurrency = getStringValue(noMassCofirmations, AgreementCurrency.FIELD);
				strikePrice = getStringValue(noMassCofirmations, StrikePrice.FIELD);
				currency1 = getStringValue(noMassCofirmations, Currency1.FIELD);
				currency1Amt = getStringValue(noMassCofirmations, Currency1Amt.FIELD);
				currency2 = getStringValue(noMassCofirmations, Currency2.FIELD);
				currency2Amt = getStringValue(noMassCofirmations, Currency2Amt.FIELD);
				maturityDate = getStringValue(noMassCofirmations, MaturityDate.FIELD);;
				paymentDate = getStringValue(noMassCofirmations, PaymentDate.FIELD);;
				optPremiumAmt = getStringValue(noMassCofirmations, OptPremiumAmt.FIELD);;
				optPremiumBasis = getStringValue(noMassCofirmations, OptPremiumBasis.FIELD);;
				optPremiumValue = getStringValue(noMassCofirmations, OptPremiumValue.FIELD);
				derivativeExerciseStype =getStringValue(noMassCofirmations, DerivativeExerciseStyle.FIELD);
				expireTime = getStringValue(noMassCofirmations, ExpireTime.FIELD);;
				expireTimeZone = getStringValue(noMassCofirmations, ExpireTimeZone.FIELD);
				deliveryDate = getStringValue(noMassCofirmations, DeliveryDate.FIELD);
				optSettlAmtType =getStringValue(noMassCofirmations, OptSettlAmtType.FIELD);
				benchmarkCurveName = getStringValue(noMassCofirmations, BenchmarkCurveName.FIELD);
				optPremiumCurrency = getStringValue(noMassCofirmations, OptPremiumCurrency.FIELD);
				
				int partyrole=-1; //本方对手方
				
				String partyid="";//21位机构码
				
				String partysubid="";
			    
			    int partysubidtype=-1;
				
				ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
				ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
				int nopartysubids=-1;//清算信息循环组
				
				for (int k = 1; k <= noMassCofirmations.getNoPartyIDs().getValue(); k++) {
					
					noMassCofirmations.getGroup(k, nopartyids1);
					
					partyrole=msgUtil.getIntValue(nopartyids1, PartyRole.FIELD);

					partyid=msgUtil.getStringValue(nopartyids1, PartyID.FIELD);
					
					if(partyrole==1){
						ownPartyID = partyid;
						ownPartyRole = partyrole;
						if("4".equals(side)){
						nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
							for (int g = 1; g <= nopartysubids; g++) {
								
								nopartyids1.getGroup(g, nopartysubids1);
							
								partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
								
								partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
								if (partysubidtype == 110) {
									optionCostCapitalBakeName =  partysubid;
									
								}else if (partysubidtype == 23) {
									optionCostCapitalAccountName =  partysubid;
									
								}else if (partysubidtype == 16) {
									optionCostPayBankNo =  partysubid;
									
								}else if (partysubidtype == 15) {
									optionCostCapitalAccount =  partysubid;
									
								}else if (partysubidtype == 139) {
									optionCostRemark =  partysubid;
								}
							}
						}
					}else if (partyrole==17) {
						othPartyID = partyid;
						othPartyRole = partyrole;
						if("1".equals(side)){
						nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
							for (int g = 1; g <=nopartysubids ; g++) {
								
								nopartyids1.getGroup(g, nopartysubids1);
								
								partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
								
								partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
								
								if (partysubidtype == 110) {
									optionCostCapitalBakeName =  partysubid;
									
								}else if (partysubidtype == 23) {
									optionCostCapitalAccountName =  partysubid;
									
								}else if (partysubidtype == 16) {
									optionCostPayBankNo =  partysubid;
									
								}else if (partysubidtype == 15) {
									optionCostCapitalAccount =  partysubid;
									
								}else if (partysubidtype == 139) {
									optionCostRemark =  partysubid;
								}
							}
						}
				}
			}
				model.setExecID(execID);
				model.setConfirmID(confirmID);
				model.setConfirmStatus(confirmStatus);
				model.setOptDataType(optDataType);
				model.setCombinationID(combinationID);
				model.setOptPayoutType(optPayoutType);
				model.setTradeDate(tradeDate);
				model.setSide(side);
				model.setNetGrossInd(netGrossInd);
				model.setOptPremiumCurrency(optPremiumCurrency);
				model.setCurrency1(currency1);
				model.setCurrency2(currency2);
				model.setCalculatedCcyLastTradeAmt(calculatedCcyLastTradeAmt);
				model.setAgreementCurrency(agreementCurrency);
				model.setPutOrCall(putOrCall);
				model.setMarketIndicator(marketIndicator);
				model.setStrikePrice(strikePrice);
				model.setExerciseStyle(exerciseStyle);
				model.setMaturityDate(maturityDate);
				model.setBenchmarkCurveName(benchmarkCurveName);
				model.setExpireTime(expireTime);
				model.setExpireTimeZone(expireTimeZone);
				model.setDeliveryDate(deliveryDate);
				model.setOptSettlAmtType(optSettlAmtType);
				model.setPaymentDate(paymentDate);
				model.setOptPremiumBasis(optPremiumBasis);
				model.setOptPremiumValue(optPremiumValue);
				model.setOptPremiumAmt(optPremiumAmt);
				model.setOwnCnfmTimeType(String.valueOf(ownCnfmTimeType));
				model.setOwnCnfmTime(ownCnfmTime);
				model.setOthCnfmTimeType(String.valueOf(othCnfmTimeType));
				model.setOthCnfmTime(othCnfmTime);
				model.setOwnPartyID(ownPartyID);
				model.setOwnPartyRole(String.valueOf(ownPartyRole));
				model.setOthPartyID(othPartyID);
				model.setOthPartyRole(String.valueOf(othPartyRole));
				model.setOptionCostCapitalBakeName(optionCostCapitalBakeName);
				model.setOptionCostCapitalAccountName(optionCostCapitalAccountName);
				model.setOptionCostPayBankNo(optionCostPayBankNo);
				model.setOptionCostCapitalAccount(optionCostCapitalAccount);
				model.setOptionCostRemark(optionCostRemark);
				model.setCurrency1Amt(currency1Amt);
				model.setCurrency2Amt(currency2Amt);
				model.setDerivativeExerciseStype(derivativeExerciseStype);
				pageInfo = RecPageInfo.msgPageToModel(message);
				model.setPageInfo(pageInfo);
				list.add(model);
			}
			
			if ("".equals(execID)) {
				System.out.println("消息未收到域：ExecID "+ExecID.FIELD);
			}	
			if ("".equals(tradeDate)) {
				System.out.println("消息未收到域：TradeDate "+TradeDate.FIELD);
			}	
			if ("".equals(netGrossInd)) {
				System.out.println("消息未收到域：NetGrossInd "+NetGrossInd.FIELD);
			}			
			if ("".equals(marketIndicator)) {
				System.out.println("消息未收到域：MarketIndicator "+MarketIndicator.FIELD);
			}			
			if ("".equals(confirmID)) {
				System.out.println("消息未收到域：ConfirmID "+ConfirmID.FIELD);
			}			
			if ("".equals(confirmStatus)) {
				System.out.println("消息未收到域：ConfirmStatus "+ConfirmStatus.FIELD);
			}
			
			
			
			
		} catch (Exception e) {
			LogManager.info("解析报文出错"+e);
			throw e;
		}
		return list;
	}
	
	public String getStringValue(NoMassCofirmations noMassCofirmations, int field) throws Exception{
		return noMassCofirmations.isSetField(field) ? noMassCofirmations.getString(field) : "";
	}

}
