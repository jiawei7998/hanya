package com.singlee.financial.cfets.service;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.util.DateUtil;
import com.singlee.financial.util.SequenceUtil;
import imix.field.*;
import imix.imix10.ExecutionReport;
import imix.imix10.Message;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public abstract class ConfirmMsgService { 
	
	private String msgType;
	
	private ConfigBean configBean;
	
	public ConfirmMsgService(ConfigBean configBean){
		this.configBean = configBean;
	}
	
	public Message pkgMessage(TradeConfirmBean bean)throws Exception{
		Message message = new Message();
		pkgMsgHeader(message);
		pkgMsgBody(message,bean);
		return message;
	}
	
	/***
	 * 组装报文头
	 * @param message
	 * @throws Exception
	 */
	private void pkgMsgHeader(Message message)throws Exception{
		// AK 报文类型
		message.getHeader().setField(new MsgType(getMsgType()));
		// 1 报文序号
		message.getHeader().setField(new MsgSeqNum(SequenceUtil.getSeqInt("MsgSeqNum")));
		// XXXX 报文发送方， 为机构，
		message.getHeader().setField(new SenderCompID(configBean.getSendercompid()));
		// 取决于连接配置文件中的SenderCompID，XXXX 发送方子标识， 为用户名，
		message.getHeader().setField(new SenderSubID(configBean.getUserName()));// 
		// 取决于连接配置文件中的SenderSubID CFETS-RMB-TPP
		message.getHeader().setField(new TargetCompID(configBean.getMarket()));
		// 20061122-14:15:12报文发送时间， 格式 YYYYMMDD-HH:MM:SS 或者YYYYMMDD-HH:MM:SS.SSS
		message.getHeader().setField(new SendingTime(DateUtil.getDateTime()));// 
		// 数字签名长度（不可加密）
		message.getTrailer().setField(new SignatureLength());
		// 数字签名（不可加密）
		message.getTrailer().setField(new Signature());
		// 校验和;报文的最末域
		message.getTrailer().setField(new CheckSum());
	}
	
	/***
	 * 
	 * 组装报文体 需要各个业务各自实现
	 * @param message
	 * @throws Exception
	 */
	public abstract void pkgMsgBody(Message message,TradeConfirmBean bean)throws Exception;
	
	
	protected void convert(ExecutionReport.NoPartyIDs ids,int type,String value)
	{
		if(StringUtils.isNotBlank(value)){
			ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			noPartySubIDs.set(new PartySubIDType(type));
			noPartySubIDs.set(new PartySubID(value));
			ids.addGroup(noPartySubIDs);
		}
	}
	
	protected void convert1(ExecutionReport.NoPartyIDs ids,int type,String value)
	{
		if(StringUtils.isNotBlank(value) && !"-".equals(value)){
			ExecutionReport.NoPartyIDs.NoContactInfos noContactInfos = new ExecutionReport.NoPartyIDs.NoContactInfos();
			noContactInfos.set(new ContactInfoIDType(type));
			noContactInfos.set(new ContactInfoID(value));
			ids.addGroup(noContactInfos);
		}
	}

	protected ConfigBean getConfigBean() {
		return configBean;
	}

	protected void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public String getMsgType() {
		if(msgType == null){
			msgType = MsgType.CONFIRMATION;
		}
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	
	public Message pkgMessage(List<SendForeignOptionModel> beans)throws Exception{
		Message message = new Message();
		pkgMsgHeader(message);
		pkgMsgBody(message,beans);
		return message;
	}
	
	/***
	 * 
	 * 组装报文体 期权组合
	 * @param message
	 * @throws Exception
	 */
	public abstract void pkgMsgBody(Message message,List<SendForeignOptionModel> beans)throws Exception;

}
