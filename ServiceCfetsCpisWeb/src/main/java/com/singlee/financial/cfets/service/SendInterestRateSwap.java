package com.singlee.financial.cfets.service;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.model.AgencyBasicInformationModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.model.SendInterestRateSwapModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.financial.util.AgencyBasicInformationUtil;
import com.singlee.financial.util.BigDecimalUtil;
import com.singlee.financial.util.DateUtil;
import imix.ConfigError;
import imix.FieldNotFound;
import imix.field.*;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoLegs;
import imix.imix10.ExecutionReport.NoPartyIDs.NoContactInfos;
import imix.imix10.Message;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.List;



/**
 * 
 * <P>SendInterestRateSwap.java</P>
 * <P>发送利率互换成交确认报文处理类</P>
 * <P>Copyright: Copyright (c) 2016</P>
 * <P>Company: 新利</P>
 * @author  彭前
 */
public class SendInterestRateSwap  extends ConfirmMsgService{
	
	public SendInterestRateSwap(ConfigBean configBean) {
		super(configBean);
	}
	@Override
	public void pkgMsgBody(imix.imix10.Message message, TradeConfirmBean bean)throws Exception {
		SendInterestRateSwapModel model = (SendInterestRateSwapModel) bean;
		
		try {
			message.getHeader().setField(new OnBehalfOfCompID(message.getHeader().getString(SenderCompID.FIELD)));
			message.getHeader().setField(new DeliverToCompID(message.getHeader().getString(TargetCompID.FIELD)));
		} catch (FieldNotFound e) {
			e.printStackTrace();
		}
		
		message.setField(new ConfirmID(model.getConfirmID()));// 确认报文 ID
		message.setField(new ConfirmType(106));// 106-确认成交明细
		message.setField(new MarketID("2"));// 2 交易场所， 默认为CFETS
		message.setField(new ExecID(model.getExecID()));// 成交编号
		message.setField(new LastQty(BigDecimalUtil.fixZero(BigDecimalUtil.round(new BigDecimal(model.getLastQty()).doubleValue(),2),2,"0")));// 名义本金（元）精度：2 位
		
		message.setField(new MarketIndicator("2"));// 市场类型 2-利率互换
		message.setField(new Symbol(model.getSymbol()==null?"-":model.getSymbol()));// 产品名称
		
		if(StringUtils.isNotBlank(model.getText())){
			message.setField(new Text(model.getText())); //用于填写交易后业务字段中的“补充条款信息”
		}
		
		message.setField(new StartDate(model.getStartDate().replace("<Date>", "")));// 起息日
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>", "")));// 成交日期
		message.setField(new EndDate(model.getEndDate().replace("<Date>", "")));// 到期日
		
		message.setField(new CouponPaymentDateReset(model.getCouponPaymentDateReset().charAt(0)));
		
		message.setField(new InterestAccuralDaysAdjustment(model.getInterestAccuralDaysAdjustment().charAt(0)) );
		
		
		String interestAccuralDaysAdjustment = model.getInterestAccuralDaysAdjustment();
		if ("1".equals(interestAccuralDaysAdjustment)) {
			message.setField(new InterestAccuralDaysAdjustment('1'));// 计息天数调整 0-不调整  1-实际天数调整
		}else {
			message.setField(new InterestAccuralDaysAdjustment('0'));// 计息天数调整 0-不调整  1-实际天数调整
		}
		message.setField(new FirstPeriodStartDate(model.getFirstPeriodStartDate().replace("<Date>", "")));//首期起息日
		
		String startDate = model.getStartDate().replace("<Date>", ""); //起息日
		String endDate = model.getEndDate().replace("<Date>", "");     //到期日
		
		String TradeLimitDays = DateUtil.getTradeLimitDays(startDate,endDate); //成交日
		
		message.setField(new TradeLimitDays(TradeLimitDays));// 期限，格式为：YYMMMDDD
													//6000=6M
													//32=32D
													//1000000=1Y
		message.setField(new CalculateAgency(Integer.valueOf((model.getCalculateAency()))));// 计算机构
																							//0-中国外汇交易中心；1-卖出方；
																							//2-买入方；3-交易双方
		
		//清算机构
		if ("2".equals(model.getNetGrossInd())) {
			message.setField(new NetGrossInd(2));// 2-双边自行清算 5-上海清算所清算
		}else if("5".equals(model.getNetGrossInd())){
			message.setField(new NetGrossInd(5));// 2-双边自行清算 5-上海清算所清算
		}
		
		message.setField(new Side('J'));// 交易方向 固定-浮动

		NoLegs noLegs1 = new NoLegs();
		
		boolean direction = false; //4-固定利率收取
		
		if ("1".equals(model.getLegSide_1())) {
			noLegs1.set(new LegSide('1')); //交易方向，1-固定利率支付 4-固定利率收取
			direction = true; //1-固定利率支付
		}else if ("4".equals(model.getLegSide_1())) {
			noLegs1.set(new LegSide('4')); //交易方向，1-固定利率支付 4-固定利率收取
		}
		
		noLegs1.set(new LegPriceType(3)); //参考利率类型
		
		if(StringUtils.isNotBlank(model.getLegPrice())){
			noLegs1.set(new LegPrice(BigDecimalUtil.fixZero(BigDecimalUtil.round(new BigDecimal(model.getLegPrice()).doubleValue(), 4),4,"0"))); //固定利率，单位：%，精度：4 位
		}
		
		
		//固定利率支付周期
		//0-半年；1-年；2-到期；3-月；
		//4-季；5-两周；6-周；7-天；9-九个月
		
		//固定利率支付周期
		noLegs1.set(new LegCouponPaymentFrequency(model.getLegCouponPaymentFrequency_1())); 
		
		noLegs1.set(new LegCouponPaymentDate(model.getLegCouponPaymentDate_1().replace("<Date>", ""))); //固定利率首期定期支付日
		
		
		
		//固定利率计息基准
		//0-实际/实际；1-实际/360；
		//2-30/360 ； 3- 实 际 /365 ；
		//D-30E/360；5-实际/365F
	
		//固定利率计息基准
		noLegs1.set(new LegDayCount(model.getLegDayCount_1().charAt(0)));
		
		message.addGroup(noLegs1);
		
		
		NoLegs noLegs2 = new NoLegs();
		
		noLegs2.set(new LegSide('B')); //浮动利率端
		noLegs2.set(new LegPriceType(6)); //参考利率类型，6-浮动利率
		
		//首次利率确定日
		/*if (prop.getProperties(floSSI+".PorS").equals("S")) {
			noLegs2.set(new LegInterestAccrualDate(prop.getProperties("Back.cCFETSInfo[0].cLegInterestAccrualDateP").replace("<Date>", "")));//
		}else if (prop.getProperties(floSSI+".PorS").equals("P")) {
			noLegs2.set(new LegInterestAccrualDate(prop.getProperties("Back.cCFETSInfo[0].cLegInterestAccrualDateS").replace("<Date>", "")));
		}*/
		noLegs2.set(new LegInterestAccrualDate(model.getLegInterestAccrualDate().replace("<Date>", "")));
		
	
	
		//参考利率
		noLegs2.set(new LegBenchmarkCurveName(model.getLegBenchmarkCurveName()==null?"":model.getLegBenchmarkCurveName()));
		

		noLegs2.set(new LegBenchmarkSpread(BigDecimalUtil.round(new BigDecimal(model.getLegBenchmarkSpread()).doubleValue(), 2))); //利差，单位：bps，精度：2 位

		noLegs2.set(new LegCouponPaymentDate(model.getLegCouponPaymentDate_2())); //浮动利率首期定期支付日
		
		//浮动利率支付周期
		//0-半年；1-年；2-到期；3-月；
		//4-季；5-两周；6-周；7-天；9-九个月
		noLegs2.set(new LegCouponPaymentFrequency(model.getLegCouponPaymentFrequency_2()));


		noLegs2.set(new LegInterestAccrualDate(model.getLegInterestAccrualDate().replace("<Date>", ""))); //首次利率确定日
		
		
		//浮动利率重置频率
		//0-半年；1-1 年；2-到期；3-月；
		//4-季；5-两周；6-周；7-天；9M-九个月
		noLegs2.set(new LegInterestAccrualResetFrequency(model.getLegInterestAccrualResetFrequency()==null?"":model.getLegInterestAccrualResetFrequency())); 
		
		
		if ("0".equals(model.getLegInterestAccrualMethod())) {
			noLegs2.set(new LegInterestAccrualMethod('0')); //浮动利率计息方法  0-单利  1-复利
		}else if ("1".equals(model.getLegInterestAccrualMethod())){
			noLegs2.set(new LegInterestAccrualMethod('1')); //浮动利率计息方法  0-单利  1-复利
		}
		
		//浮动利率计息基准
		//0-实际/实际；1-实际/360；
		//2-30/360 ； 3- 实 际 /365 ；
		//D-30E/360；5-实际/365F
		noLegs2.set(new LegDayCount(model.getLegDayCount_2().charAt(0))); 
		message.addGroup(noLegs2);
		
		message.setField(new imix.field.NoPartyIDs(2));
		pkgSettlInfo(message, model, direction);
	}
	
	public void pkgSettlInfo(Message message,SendInterestRateSwapModel model,boolean direction)throws Exception{
		TradeSettlsBean bean = model.getTradeSettlsBean();
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyRole(PartyRole.BUYER_OR_BORROWER_OR_REVERSE_REPOER_OR_FIXED_PAYER_OR_BENCHMARK_1_PAYER));
		
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		cust.set(new PartyRole(PartyRole.SELLER_OR_LENDER_OR_REPOER_OR_FIXED_RECEIVER_OR_BENCHMARK_1_RECEIVER));
		
//		//电话，该字段非必填
//		convert1(own, ContactInfoIDType.PHONE_NO_1, model.getContactInfoID_6());
//		
//		//传真，该字段非必填
//		convert1(own, ContactInfoIDType.FAX_NO_1, model.getContactInfoID_8());
//		
//		//联系人，该字段非必填
//		convert(own, PartySubIDType.CONTACT_NAME, model.getContactInfoID_4());
		
		//如果交易方式为双边清算 (NetGrossInd=2)  则为双方传入账户信息  否则不需要传入这些域
		if("2".equals(model.getNetGrossInd())||"5".equals(model.getNetGrossInd()))
		{
			if (direction) {
				// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
				own.set(new PartyID(getConfigBean().getBankId()));
				// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
				cust.set(new PartyID(bean.getcInstitutionId()));
				// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
				convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getFullName().trim());
				// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
				convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getcFullName().trim());
				
				pkgSettls(own, bean.getBankname(), bean.getBankacctno(), bean.getAcctname(), bean.getAcctno());//支付方
				
				pkgSettls(cust, bean.getcBankname(), bean.getcBankacctno(), bean.getcAcctname(), bean.getcAcctno());//收取方
			}else {
				// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
				cust.set(new PartyID(getConfigBean().getBankId()));
				// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
				own.set(new PartyID(bean.getcInstitutionId()));
				// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
				convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getFullName().trim());
				// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
				convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getcFullName().trim());
				
				pkgSettls(own, bean.getcBankname(), bean.getcBankacctno(), bean.getcAcctname(), bean.getcAcctno());//支付方
				
				pkgSettls(cust, bean.getBankname(), bean.getBankacctno(), bean.getAcctname(), bean.getAcctno());//收取方
			}
		}
		message.addGroup(own);
		message.addGroup(cust);
	}
	
	public void pkgSettls(ExecutionReport.NoPartyIDs iDs,String bankname,String bankacctno,
			String acctname,String acctno){
		// 资金开户行110
		convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME, bankname.trim());
		// 资金账户户名 23
		convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME, acctname.trim());
		// 资金账号15
		convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER, acctno);
		// 支付系统行号112
		convert(iDs,PartySubIDType.SETTLEMENT_BANK_SORT_CODE, bankacctno);
	}
	/**
	 * 拼装上送IMIX消息报文
	 * @param SendInterestRateSwapModel
	 * @return message
	 */
	public Message packageMsg(SendInterestRateSwapModel model, ConfigBean configBean) throws ConfigError {
		
		Message message = MsgHeadAndStandardTrailer.packageMsgHeadAndStandardTrailer("AK", configBean);
		
		try {
			message.getHeader().setField(new OnBehalfOfCompID(message.getHeader().getString(SenderCompID.FIELD)));
			message.getHeader().setField(new DeliverToCompID(message.getHeader().getString(TargetCompID.FIELD)));
		} catch (FieldNotFound e) {
			e.printStackTrace();
		}
		
		message.setField(new ConfirmID(model.getConfirmID()));// 确认报文 ID
		message.setField(new ConfirmType(106));// 106-确认成交明细
		message.setField(new MarketID("2"));// 2 交易场所， 默认为CFETS
		message.setField(new ExecID(model.getExecID()));// 成交编号
		message.setField(new LastQty(BigDecimalUtil.fixZero(BigDecimalUtil.round(new BigDecimal(model.getLastQty()).doubleValue(),2),2,"0")));// 名义本金（元）精度：2 位
		
		message.setField(new MarketIndicator("2"));// 市场类型 2-利率互换
		message.setField(new Symbol(model.getSymbol()));// 产品名称
		message.setField(new Text(model.getText())); //用于填写交易后业务字段中的“补充条款信息”
		message.setField(new StartDate(model.getStartDate().replace("<Date>", "")));// 起息日
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>", "")));// 成交日期
		message.setField(new EndDate(model.getEndDate().replace("<Date>", "")));// 到期日
		
		message.setField(new CouponPaymentDateReset(model.getCouponPaymentDateReset().charAt(0)));
		
		message.setField(new InterestAccuralDaysAdjustment(model.getInterestAccuralDaysAdjustment().charAt(0)) );
		
		
		String interestAccuralDaysAdjustment = model.getInterestAccuralDaysAdjustment();
		if ("1".equals(interestAccuralDaysAdjustment)) {
			message.setField(new InterestAccuralDaysAdjustment('1'));// 计息天数调整 0-不调整  1-实际天数调整
		}else {
			message.setField(new InterestAccuralDaysAdjustment('0'));// 计息天数调整 0-不调整  1-实际天数调整
		}
		message.setField(new FirstPeriodStartDate(model.getFirstPeriodStartDate().replace("<Date>", "")));//首期起息日
		
		String startDate = model.getStartDate().replace("<Date>", ""); //起息日
		String endDate = model.getEndDate().replace("<Date>", "");     //到期日
		
		String TradeLimitDays = DateUtil.getTradeLimitDays(startDate,endDate); //成交日
		
		message.setField(new TradeLimitDays(TradeLimitDays));// 期限，格式为：YYMMMDDD
													//6000=6M
													//32=32D
													//1000000=1Y
		message.setField(new CalculateAgency(Integer.valueOf((model.getCalculateAency()))));// 计算机构
																							//0-中国外汇交易中心；1-卖出方；
																							//2-买入方；3-交易双方
		
		//清算机构
		if ("2".equals(model.getNetGrossInd())) {
			message.setField(new NetGrossInd(2));// 2-双边自行清算 5-上海清算所清算
		}else if("5".equals(model.getNetGrossInd())){
			message.setField(new NetGrossInd(5));// 2-双边自行清算 5-上海清算所清算
		}
		
		message.setField(new Side('J'));// 交易方向 固定-浮动

		NoLegs noLegs1 = new NoLegs();
		
		boolean direction = false; //4-固定利率收取
		
		if ("1".equals(model.getLegSide_1())) {
			noLegs1.set(new LegSide('1')); //交易方向，1-固定利率支付 4-固定利率收取
			direction = true; //1-固定利率支付
		}else if ("4".equals(model.getLegSide_1())) {
			noLegs1.set(new LegSide('4')); //交易方向，1-固定利率支付 4-固定利率收取
		}
		
		noLegs1.set(new LegPriceType(3)); //参考利率类型
		
		if(StringUtils.isNotBlank(model.getLegPrice())){
			noLegs1.set(new LegPrice(BigDecimalUtil.decimalFormat(model.getLegPrice(), "4"))); //固定利率，单位：%，精度：4 位
		}
		
		
		//固定利率支付周期
		//0-半年；1-年；2-到期；3-月；
		//4-季；5-两周；6-周；7-天；9-九个月
		
		//固定利率支付周期
		noLegs1.set(new LegCouponPaymentFrequency(model.getLegCouponPaymentFrequency_1())); 
		
		noLegs1.set(new LegCouponPaymentDate(model.getLegCouponPaymentDate_1().replace("<Date>", ""))); //固定利率首期定期支付日
		
		
		
		//固定利率计息基准
		//0-实际/实际；1-实际/360；
		//2-30/360 ； 3- 实 际 /365 ；
		//D-30E/360；5-实际/365F
	
		//固定利率计息基准
		noLegs1.set(new LegDayCount(model.getLegDayCount_1().charAt(0)));
		
		message.addGroup(noLegs1);
		
		
		NoLegs noLegs2 = new NoLegs();
		
		noLegs2.set(new LegSide('B')); //浮动利率端
		noLegs2.set(new LegPriceType(6)); //参考利率类型，6-浮动利率
		
		//首次利率确定日
		/*if (prop.getProperties(floSSI+".PorS").equals("S")) {
			noLegs2.set(new LegInterestAccrualDate(prop.getProperties("Back.cCFETSInfo[0].cLegInterestAccrualDateP").replace("<Date>", "")));//
		}else if (prop.getProperties(floSSI+".PorS").equals("P")) {
			noLegs2.set(new LegInterestAccrualDate(prop.getProperties("Back.cCFETSInfo[0].cLegInterestAccrualDateS").replace("<Date>", "")));
		}*/
		noLegs2.set(new LegInterestAccrualDate(model.getLegInterestAccrualDate().replace("<Date>", "")));
		
	
	
		//参考利率
		noLegs2.set(new LegBenchmarkCurveName(model.getLegBenchmarkCurveName()));
		

		noLegs2.set(new LegBenchmarkSpread(BigDecimalUtil.decimalFormat(model.getLegBenchmarkSpread(), "2"))); //利差，单位：bps，精度：2 位

		noLegs2.set(new LegCouponPaymentDate(model.getLegCouponPaymentDate_2())); //浮动利率首期定期支付日
		
		//浮动利率支付周期
		//0-半年；1-年；2-到期；3-月；
		//4-季；5-两周；6-周；7-天；9-九个月
		noLegs2.set(new LegCouponPaymentFrequency(model.getLegCouponPaymentFrequency_2()));


		noLegs2.set(new LegInterestAccrualDate(model.getLegInterestAccrualDate().replace("<Date>", ""))); //首次利率确定日
		
		
		//浮动利率重置频率
		//0-半年；1-1 年；2-到期；3-月；
		//4-季；5-两周；6-周；7-天；9M-九个月
		noLegs2.set(new LegInterestAccrualResetFrequency(model.getLegInterestAccrualResetFrequency())); 
		
		
		if ("0".equals(model.getLegInterestAccrualMethod())) {
			noLegs2.set(new LegInterestAccrualMethod('0')); //浮动利率计息方法  0-单利  1-复利
		}else if ("1".equals(model.getLegInterestAccrualMethod())){
			noLegs2.set(new LegInterestAccrualMethod('1')); //浮动利率计息方法  0-单利  1-复利
		}
		
		//浮动利率计息基准
		//0-实际/实际；1-实际/360；
		//2-30/360 ； 3- 实 际 /365 ；
		//D-30E/360；5-实际/365F
		noLegs2.set(new LegDayCount(model.getLegDayCount_2().charAt(0))); 
		message.addGroup(noLegs2);
		
		message.setField(new imix.field.NoPartyIDs(2));
		
		ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
		
		//获取存放本方机构信息的集合
		List<AgencyBasicInformationModel> list1 = model.getList1();
		
		
		//获取对手方信息的集合
		List<AgencyBasicInformationModel> list2 = model.getList2();
		
		if (direction) {
			
			//nopartyids1.set(new PartyID(CPISStart.getProperties().getProperties("CFETSNO")));// 机构 21 位码 
			nopartyids1.set(new PartyID(model.getPartyID_1()));
			nopartyids1.set(new PartyRole(119));// 固定利率支付方 本方
		
			NoContactInfos noContactInfos1 = new NoContactInfos();
			//noContactInfos1.set(new ContactInfoID(CPISStart.getProperties().getProperties("IRSTEL"))); //我方电话号码
			noContactInfos1.set(new ContactInfoID(model.getContactInfoID_6()));
			noContactInfos1.set(new ContactInfoIDType(6)); //电话
			nopartyids1.addGroup(noContactInfos1);
			
			NoContactInfos noContactInfos2 = new NoContactInfos();
			//noContactInfos2.set(new ContactInfoID(CPISStart.getProperties().getProperties("IRSFAX"))); //我方传真号码
			noContactInfos2.set(new ContactInfoID(model.getContactInfoID_8()));
			noContactInfos2.set(new ContactInfoIDType(8)); //传真
			nopartyids1.addGroup(noContactInfos2);
			ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			
			
			noPartySubIDs1.set(new PartySubIDType(124));//机构中文全称
			//noPartySubIDs1.set(new PartySubID(CPISStart.getProperties().getProperties("CFETSNAME")))
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "124")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(9));//联系人
			//noPartySubIDs1.set(new PartySubID(CPISStart.getProperties().getProperties("IRSUSER")));
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "9")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(15));//资金账号
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "15")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(23));//资金账户户名
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "23")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(110));//资金开户行
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1,"110")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(112));//支付系统行号
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "112")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			message.addGroup(nopartyids1);
				
			ExecutionReport.NoPartyIDs nopartyids2 = new ExecutionReport.NoPartyIDs();
			
			nopartyids2.set(new PartyID(AgencyBasicInformationUtil.getPartySubID(list2, "120")));// 机构 21 位码
			nopartyids2.set(new PartyRole(120));// 120-固定利率收取方  对手方
			
			
			ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs2 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			
			noPartySubIDs2.set(new PartySubIDType(124));//机构中文全称
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "124")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(15));//资金账号
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "15")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(23));//资金账户户名
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "23")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(110));//资金开户行
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "110")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(112));//支付系统行号
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "112")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			message.addGroup(nopartyids2);

			
		}else {
			//'4'
			
			nopartyids1.set(new PartyID(model.getPartyID_2()));// 对手方机构 21 位码
			nopartyids1.set(new PartyRole(119));// 固定利率支付方 对手方
			
			ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();

			noPartySubIDs1.set(new PartySubIDType(124));//机构中文全称
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "124")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(15));//资金账号
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "15")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(23));//资金账户户名
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "23")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(110));//资金开户行
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "110")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			noPartySubIDs1.set(new PartySubIDType(112));//支付系统行号
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "112")));
			nopartyids1.addGroup(noPartySubIDs1);
			
			message.addGroup(nopartyids1);
			
			
			ExecutionReport.NoPartyIDs nopartyids2 = new ExecutionReport.NoPartyIDs();
			
			//nopartyids2.set(new PartyID(CPISStart.getProperties().getProperties("CFETSNO")));// 机构 21 位码
			nopartyids2.set(new PartyID(model.getPartyID_1()));
			
			
			//以下为本方
			
			nopartyids2.set(new PartyRole(120));// 120-固定利率收取方
		
			
			ExecutionReport.NoPartyIDs.NoPartySubIDs noPartySubIDs2 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			

			noPartySubIDs2.set(new PartySubIDType(124));//机构中文全称
			//noPartySubIDs2.set(new PartySubID(CPISStart.getProperties().getProperties("CFETSNAME")));
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "124")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			NoContactInfos noContactInfos1 = new NoContactInfos();
			noContactInfos1.set(new ContactInfoID(model.getContactInfoID_6())); //电话号码
			noContactInfos1.set(new ContactInfoIDType(6)); //电话
			nopartyids2.addGroup(noContactInfos1);
			
			NoContactInfos noContactInfos2 = new NoContactInfos();
			noContactInfos2.set(new ContactInfoID(model.getContactInfoID_8())); //传真号码
			noContactInfos2.set(new ContactInfoIDType(8)); //传真
			nopartyids2.addGroup(noContactInfos2);
			
			noPartySubIDs1.set(new PartySubIDType(9));//联系人
			noPartySubIDs1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "9")));
			nopartyids2.addGroup(noPartySubIDs1);
			
			noPartySubIDs2.set(new PartySubIDType(15));//资金账号
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "15")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(23));//资金账户户名
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "23")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(110));//资金开户行
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "110")));
			nopartyids2.addGroup(noPartySubIDs2);
			
			noPartySubIDs2.set(new PartySubIDType(112));//支付系统行号
			noPartySubIDs2.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "112")));
			nopartyids2.addGroup(noPartySubIDs2);

			message.addGroup(nopartyids2);
		}
		
		return message;
	}
	@Override
	public void pkgMsgBody(Message message, List<SendForeignOptionModel> beans)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
