package com.singlee.financial.cfets.service;


import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.util.PropertiesUtil;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.ConfirmID;
import imix.field.ConfirmType;
import imix.field.DealTransType;
import imix.field.ExecID;
import imix.field.ExecType;
import imix.field.MarketIndicator;
import imix.field.NoPartyIDs;
import imix.field.NoPartySubIDs;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;


/**
 * 
 * <P>RecFXRepeal.java</P>
 * <P>接收交易撤销的推送消息处理类</P>
 * <P>Copyright: Copyright (c) 2018</P>
 * <P>Company: 新利</P>
 * <P>          修改者姓名 修改内容说明</P>
 * @see     参考类1
 */

public class RecFXRepeal {

	//static String recFileName = CPISStart.getProperties().getProperties("FXREPEAL");
	/**
	 * 解析报文
	 * @param message
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	public PropertiesUtil ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		PropertiesUtil prop = new PropertiesUtil();
		MessageUtil msgUtil = new MessageUtil(message);
		
		
		prop = MsgHeadAndStandardTrailer.msgHeadToProp(message,prop);

		String confirmID = "";
			confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
		String execID = "";
			execID  = msgUtil.getStringValue(ExecID.FIELD);
		String tradeDate = "";
			tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);
		String execType = "";
			execType  = msgUtil.getStringValue(ExecType.FIELD);
		String marketIndicator = "";
			marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
		String dealTransType = "";
			dealTransType  = msgUtil.getStringValue(DealTransType.FIELD);
		
		
		if ("".equals(execID)) {
			System.out.println("消息未收到域：ExecID "+ExecID.FIELD);
		}	
		if ("".equals(tradeDate)) {
			System.out.println("消息未收到域：TradeDate "+TradeDate.FIELD);
		}	
		if ("".equals(marketIndicator)) {
			System.out.println("消息未收到域：MarketIndicator "+MarketIndicator.FIELD);
		}			
		if ("".equals(execType)) {
			System.out.println("消息未收到域：ExecType "+ExecType.FIELD);
		}			
		if ("".equals(dealTransType)) {
			System.out.println("消息未收到域：DealTransType "+DealTransType.FIELD);
		}		
		if ("".equals(confirmID)) {
			System.out.println("消息未收到域：ConfirmID "+ConfirmID.FIELD);
		}			
		
		
		String ownPartyID = ""; //本方机构 21 位码
		int ownPartyRole = 0 ;
		
		String othPartyID = "";//对手方机构 21 位码
		int othPartyRole = 0 ;
		
		String ownCnName = ""; //中文全称
		String othCnName = ""; //中文全称
		
		PartyID partyid = new PartyID();
		PartyRole partyrole = new PartyRole();
		PartySubID partysubid = new PartySubID();
		PartySubIDType partysubidtype = new PartySubIDType();
		ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		NoPartySubIDs nopartysubids = new NoPartySubIDs();
		
		for (int k = 1; k <= msgUtil.getIntValue(NoPartyIDs.FIELD); k++) {
			message.getGroup(k, nopartyids1);
			nopartyids1.get(partyrole);
			nopartyids1.get(partyid);
			
			if(partyrole.getValue()==1){
				ownPartyID = partyid.getValue();
				
				ownPartyRole = partyrole.getValue();
				
				nopartyids1.get(nopartysubids);
				for (int g = 1; g <= nopartysubids.getValue(); g++) {
					nopartyids1.getGroup(g, nopartysubids1);
					nopartysubids1.get(partysubid);
					nopartysubids1.get(partysubidtype);
					if (partysubidtype.getValue() == 124) {
						ownCnName =  partysubid.getValue();
					}
				}
			}
			if(partyrole.getValue()==17){
				othPartyID = partyid.getValue();
				
				othPartyRole = partyrole.getValue();
				
				nopartyids1.get(nopartysubids);
				for (int g = 1; g <= nopartysubids.getValue(); g++) {
					nopartyids1.getGroup(g, nopartysubids1);
					nopartysubids1.get(partysubid);
					nopartysubids1.get(partysubidtype);
					if (partysubidtype.getValue() == 124) {
						othCnName =  partysubid.getValue();
					}
				}
			}
			
		}
		
		//利率互换的返回报文比其他的多ConfirmType字段
		String confirmType = "";
		if (null!=marketIndicator&& "2".equals(marketIndicator)) {
			confirmType = msgUtil.getStringValue(ConfirmType.FIELD);
			prop.setProperties("ConfirmType", confirmType);

		}
		
		try {
			
			prop.setProperties("ExecID", execID);
			prop.setProperties("TradeDate", tradeDate);
			prop.setProperties("MarketIndicator", marketIndicator);
			prop.setProperties("ExecType", execType);
			prop.setProperties("DealTransType", dealTransType);
			prop.setProperties("ConfirmID", confirmID);
			prop.setProperties("ownCnName", ownCnName);
			prop.setProperties("ownPartyRole", ownPartyRole);
			prop.setProperties("ownPartyID", ownPartyID);
			
			prop.setProperties("othPartyID", othPartyID);
			prop.setProperties("othPartyRole", othPartyRole);
			prop.setProperties("othCnName", othCnName);
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
	}

}
