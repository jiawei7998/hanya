package com.singlee.financial.cfets.cpis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import imix.client.core.ImixSession;

import com.singlee.financial.pojo.RetBean;
import com.singlee.financial.pojo.component.RetStatusEnum;

public class ImixCPISAutoConn {
	private ImixSession imixSession = null;
	private int reConnCount = 10;
	private int reConnTime = 30000;
	private int reConnTimeOut = 8000;
	
	private static boolean isConnecting = false;
	
	private boolean loginState = false;
	
	private boolean loginSucc = false;
	
	private boolean stopImixSessionBySystem = true;
	
	private static Logger logger = LoggerFactory.getLogger(ImixCPISAutoConn.class);
	
	public ImixCPISAutoConn(ImixSession imixSession,int reConnCount,int reConnTime)
	{
		this.imixSession = imixSession; 
		this.reConnCount = reConnCount;
		this.reConnTime = reConnTime;
	}
	
	public boolean startConn()throws Exception{
		if(isConnecting){
			return isConn();
		}
		logger.info("======>系统开始登陆.....");
		
		isConnecting = true;
		try {
			logger.info("======>开始第一次登陆.....");
			loginState = connServer(reConnTimeOut);
			logger.info("======>第一次登陆结果:"+loginState);
			
			Thread.sleep(8000);
			loginState = imixSession.isStarted();
			logger.info("======>当前连接状态:"+isConn());
			if(isConn()){
				return true;
			}
			
			Thread connThread = null;
			//开始重连
			logger.info("======>准备开始重连......");
			for (int i = 1; i < reConnCount; i++) 
			{
				//暂停时长 reConnTime
				Thread.sleep(reConnTime);
				
				//如果登陆成功,直接退出
				loginState = imixSession.isStarted();
				logger.info("======>当前连接状态:"+isConn());
				if(isConn()){
					break;
				}
				
				logger.info("======>开始第" + i + "次重连......");
				connThread = new Thread(new Runnable() {
					
					@Override
					public void run() {
						try {							
							loginState = connServer(reConnTimeOut);
							logger.info("======>重连结果:"+loginState);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				connThread.start();
				//最后一次重连,需要等待连接超时时间后获取是否登陆成功状态
				if(i == reConnCount - 1){
					Thread.sleep(reConnTimeOut + 1000);
				}
			}// end for
			
			//判断最后是否连接成功,如果失败则断开连接
			if(!isConn()){
				logger.info("*******重连累计大于" + reConnCount + "次,断开连接!**********");
	            try {
	                imixSession.stop();
	            } catch (Exception e) {
	            	logger.error("停止服务异常", e);
	            }
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("连接服务器出错:"+e.getMessage(),e);
		} finally {
			isConnecting = false;
		}
		return isConn();
	}
	
	public boolean isConn(){
		return loginState && loginSucc;
	}
	
	public void setConnStatus(boolean isLoginSucc){
		this.loginSucc = isLoginSucc;
	}
	
	/***
	 * 
	 * 连接服务器,timeOut时间后超时
	 * @param timeOut
	 * @return
	 * @throws Exception
	 */
	private boolean connServer(int timeOut)throws Exception{
		final RetBean ret = new RetBean();
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					if(imixSession.start()){
						ret.setRetStatus(RetStatusEnum.S);
					}else{
						ret.setRetStatus(RetStatusEnum.F);
					}
				} catch (Exception e) {
					logger.error("连接服务出现异常:" + e.getMessage(), e);
					e.printStackTrace();
					ret.setRetStatus(RetStatusEnum.F);
				} 
			}
		});
		t.start();
		//线程等待timeOut 时长后自动停止
		t.join(timeOut);
		t.interrupt();
		
		return RetStatusEnum.S.equals(ret.getRetStatus());
	}
	
	public boolean isStopImixSessionBySystem() {
		return stopImixSessionBySystem;
	}

	public void setStopImixSessionBySystem(boolean stopImixSessionBySystem) {
		this.stopImixSessionBySystem = stopImixSessionBySystem;
	}
}
