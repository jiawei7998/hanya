package com.singlee.financial.cfets.cpis;

import com.singlee.financial.cfets.bean.*;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.service.*;
import com.singlee.financial.pojo.RetBean;
import imix.Message;
import imix.field.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * CFETS消息线程序处理
 * 参考脚本：V1.9__SYS_CPISTABLE.sql
 * @author chengmj
 *
 */

public class ImixCPISMessageProcess extends Thread {
	Message message;
	ImixCPISCfetsApiService imixCPISCfetsApiService;
	ImixCPISClientListener clientListener;
	// CFETS配置类
	private ConfigBean configBean;
	private SendResponse sendResponse = new SendResponse();
	private SendQuery sendFXQuery = new SendQuery();
	private RecConfirmStatus recConfirmStatus = new RecConfirmStatus();// 接收交易查询返回(确认状态)
	private RecCPISFeedBack recCPISFeedBack = new RecCPISFeedBack(); // 接收CFETS的交易反馈
	private RecForeignForwardSpot recForeignForwardSpot = new RecForeignForwardSpot();// 接收外币即远期反馈
	private RecForeignFxmm recForeignFxmm = new RecForeignFxmm();// 接收外币拆借消息处理类
	private RecForeignSwap recForeignSwap = new RecForeignSwap();// 接收外币掉期消息处理类
	private RecDueNotConfirm recDueNotConfirm = new RecDueNotConfirm();// 接收到期未确认消息处理类
	private RecInterestRateSwap recInterestRateSwap = new RecInterestRateSwap();// 接收利率互换消息处理类
	private RecMatching recMatching = new RecMatching();
	private RecForeignCrs recForeignCrs = new RecForeignCrs();
	private RecForeignOption recForeignOption=new RecForeignOption();

	private static Logger LogManager = LoggerFactory.getLogger(ImixCPISMessageProcess.class);

	public ImixCPISMessageProcess(Message message, ImixCPISCfetsApiService imixCPISCfetsApiService, ConfigBean configBean,ImixCPISClientListener clientListener) {
		this.message = message;
		this.imixCPISCfetsApiService = imixCPISCfetsApiService;
		this.configBean = configBean;
		this.clientListener = clientListener;
	}

	/****
	 * 1、交易确认流程：
	 * 本方上传AK确认报文  => CFETS
	 * CFETS返回 AU 确认结果报文
	 *
	 * 2、交易确认结果主动推送：
	 * 双方都已经确认完成，交易中心主动推送“双方都已经确认” AK报文 => 银行
	 * 银行返回AU报文，确认完成
	 *
	 * 3、银行会员主动查询流程：
	 * 银行发送 BH 类查询报文给CFETS ，可查询 “成交明细”、“确认状态”
	 * CFETS能正常查询到处理结果 => 返回AK报文
	 * CFETS校验会员无权限或者查询结果为空 =>返回AU类型报文 
	 *
	 */
	@Override
	public void run() {
		try {
			MessageUtil msgUtil = new MessageUtil(message); // 拿到一个报文处理类对象
			// 报文类型
			String msgType = msgUtil.getHeadStringValue(MsgType.FIELD);
			// 查询请求编号 (用于与返回的查询结果报文对应,应保证唯一性)
			String queryRequestID = msgUtil.getStringValue(QueryRequestID.FIELD);
			if (!"".equals(msgUtil.getHeadStringValue(OnBehalfOfCompID.FIELD))) {
				String compid = null;
				compid = msgUtil.getHeadStringValue(OnBehalfOfCompID.FIELD);
				if ("RESEND".equals(compid)) {// 如果是重发
					LogManager.info("不处理该重发数据");
					return;
				}
			}

			/****
			 * 1、交易确认流程：
			 * 本方上传AK确认报文  => CFETS
			 * CFETS返回 AU 确认结果报文
			 *
			 * 3、银行会员主动查询流程：
			 * 银行发送 BH 类查询报文给CFETS ，可查询 “成交明细”、“确认状态”
			 * CFETS能正常查询到处理结果 => 返回AK报文
			 * CFETS校验会员无权限或者查询结果为空 =>返回AU类型报文
			 *
			 */
			if (MsgType.CONFIRMATION_ACK.equals(msgType)) {
				//有queryRequestID字段     说明该笔报文是银行主动发起查询后 CFETS校验会员无权限或者查询结果为空返回的报文
				if (null != queryRequestID && !"".equals(queryRequestID)) {
					processQueryResult(message);
				} else {
					String matchMsgType=msgUtil.getStringValue(MatchMsgType.FIELD);
					if(!"".equals(matchMsgType)&&null!=matchMsgType){
						proccessTrade(recMatching.recMatchingReport(message));
						sendResponse.sendResponse(message, configBean, clientListener.getImixCPISClient().getImixSession());
					}else{
						//本方上传匹配报文后,交易后平台返回匹配结果通知的AU报文
						proccessTrade(recCPISFeedBack.ExecutionReport(message));
					}
				}

				/***
				 * 交易确认结果主动推送：
				 * 双方都已经确认完成，交易中心主动推送“双方都已经确认” AK报文 => 银行
				 * 银行返回AU报文，确认完成
				 *
				 * 银行会员主动查询流程：
				 * 银行发送 BH 类查询报文给CFETS ，可查询 “成交明细”、“确认状态”
				 * CFETS能正常查询到处理结果 => 返回AK报文
				 */
			} else if (MsgType.CONFIRMATION.equals(msgType)||MsgType.MASSCOFIRMATION.equals(msgType)){
				if (null != queryRequestID && !"".equals(queryRequestID)){
					processQueryResult(message);//查询处理结果
				}else{
					processConfirmResult(message);//主动推送
				}
			} else if (MsgType.QUERYRESULT.equals(msgType)){
				proccessTrade(recMatching.recMatchingReport(message));
			}
		} catch (Exception e) {
			LogManager.error("fromApp Message", e);
		}
	}

	/****
	 * 处理查询结果报文
	 * 查询失败 AU
	 * 查询成功 AK  confirmType=1确认状态  confirmType=106成交明细
	 * @param message
	 * @throws Exception
	 */
	public void processQueryResult(Message message)throws Exception{
		RecPageInfoModel pageInfoModel = null;
		RecMsgBean recMsgBean = parseMsg(message);
		recMsgBean.setMessage(message.toString());
		LogManager.info("收到查询结果信息");
		pageInfoModel = recMsgBean.getPageInfo();
		// 查询下一页
		if (pageInfoModel != null && StringUtils.isNotBlank(pageInfoModel.getQueryRequestID())) {
			sendFXQuery.sendNextPage(pageInfoModel, message);
		}
		proccessTrade(recMsgBean);
	}

	/****
	 * 交易确认结果主动推送：
	 * 1、双方都已经确认完成，交易中心主动推送“双方都已经确认” AK报文 => 银行 execType = F
	 * 银行返回AU报文，确认完成
	 *
	 * 2、交易撤销“成交状态为撤销” AK报文  execType = 4
	 * 银行返回AU报文，确认完成
	 *
	 * 4、到期未确认 AK报文 confirmType = 107
	 *
	 * @param message
	 * @throws Exception
	 */
	public void processConfirmResult(Message message)throws Exception{
		MessageUtil msgUtil = new MessageUtil(message);
		// 成交状态
		String execType = msgUtil.getStringValue(ExecType.FIELD);
		String marketIndicator = msgUtil.getStringValue(MarketIndicator.FIELD);
		String confirmStatus = msgUtil.getStringValue(ConfirmStatus.FIELD);
		RecMsgBean recMsgBean = parseMsg(message);
		recMsgBean.setMessage(message.toString());
		// 会员收到交易后发送的推送消息（确认完成及交易撤销）后，会员反馈给交易后的报文
		if(ExecType.TRADE.equals(execType) || ExecType.NEW.equals(execType)){//外币拆借0是有效值
			sendResponse.sendResponse(message,AffirmStatus.ACK_TO_CONFIRM_COMPLETE_NOTIFICATION, configBean, clientListener.getImixCPISClient().getImixSession());

		}else if(ExecType.CANCELED.equals(execType)){
			//撤销交易
			sendResponse.sendResponse(message,AffirmStatus.ACK_TO_EXECUTION_STATE_CHANGED, configBean, clientListener.getImixCPISClient().getImixSession());
		}else if (MarketIndicator.FXOPT.equals(marketIndicator)&&String.valueOf(ConfirmStatus.CONFIRMED).equals(confirmStatus)){
			sendResponse.sendResponse(message,AffirmStatus.ACK_TO_CONFIRM_COMPLETE_NOTIFICATION, configBean, clientListener.getImixCPISClient().getImixSession());
		}
		if(!MarketIndicator.FXOPT.equals(marketIndicator)){
			proccessTrade(recMsgBean);
		}
	}

	public RecMsgBean parseMsg(Message message)throws Exception{
		MessageUtil msgUtil = new MessageUtil(message);
		// 报文类型
		String msgType = msgUtil.getHeadStringValue(MsgType.FIELD);
		// 成交状态
		String execType = msgUtil.getStringValue(ExecType.FIELD);
		//交易类型
		String marketindicator = msgUtil.getStringValue(MarketIndicator.FIELD);
		//是否是查询结果
		boolean isQueryMsg = message.isSetField(QueryRequestID.FIELD);
		RecMsgBean recMsgBean = null;
		if(MsgType.CONFIRMATION.equals(msgType)||MsgType.MASSCOFIRMATION.equals(msgType)){ // AK 查询成功 的报文或者是 主动推送的报文
			//查询类型 1 状态查询, 106 明细查询
			int confirmType = msgUtil.getIntValue(ConfirmType.FIELD);

			if(isQueryMsg && ConfirmType.STATUS == confirmType){//状态查询结果
				recMsgBean = recConfirmStatus.ExecutionReport(message);

			}else if(!isQueryMsg && ConfirmType.UNCONFIRMED == confirmType) { //到期未确认提醒
				recMsgBean = recDueNotConfirm.ExecutionReport(message);

			}else if(!isQueryMsg || ConfirmType.BASIC_CONFIRMATION_INFORMATION == confirmType){//明细查询结果 或者是主动推送的交易
				// 外汇询价数据推送 ，外汇询价交易确认成交明细返回(106-确认成交明细信息)
				// 外币即期 远期
				if (MarketIndicator.FXSPT.equals(marketindicator) || MarketIndicator.FXFOW.equals(marketindicator)
						||MarketIndicator.GOLDSPT.equals(marketindicator)||MarketIndicator.GOLDFWD.equals(marketindicator)) {
					if(ExecType.TRADE.equals(execType)){
						recMsgBean = recForeignForwardSpot.ExecutionReport(message);

					}else if(ExecType.CANCELED.equals(execType)){//交易撤销
						recMsgBean = recForeignForwardSpot.ExecutionReportCancel(message);
					}

				} else if (MarketIndicator.FXSWP.equals(marketindicator)||MarketIndicator.GOLDSWP.equals(marketindicator)) {// 外汇掉期
					if(ExecType.TRADE.equals(execType)){
						recMsgBean = recForeignSwap.ExecutionReport(message);

					}else if(ExecType.CANCELED.equals(execType)){//交易撤销
						recMsgBean = recForeignSwap.ExecutionReportCancel(message);
					}

				} else if (MarketIndicator.FXDEPO.equals(marketindicator)) {// 外币拆借
					if(ExecType.NEW.equals(execType)){
						recMsgBean = recForeignFxmm.ExecutionReport(message);

					}else if(ExecType.CANCELED.equals(execType)){//交易撤销
						recMsgBean = recForeignFxmm.ExecutionReportCancel(message);

					}

				} else if (MarketIndicator.INTEREST_RATE_SWAP.equals(marketindicator)) {// IRS
					recMsgBean = recInterestRateSwap.ExecutionReport(message);

				} else if (MarketIndicator.FXCRS.equals(marketindicator)) {// IRS
					recMsgBean = recForeignCrs.ExecutionReport(message);

				} else if (MarketIndicator.FXOPT.equals(marketindicator)) {// fxoption
					List<RecForeignOptionModel> list=recForeignOption.ExecutionReport(message);
					for(RecForeignOptionModel model:list){
						imixCPISCfetsApiService.getCpisFxoptTradeService().send(model);
						proccessTrade(model);
					}
				} else {
					System.out.println("消息收到的域无效: MarketIndicator " + MarketIndicator.FIELD);
					throw new RuntimeException("消息收到的域无效: MarketIndicator " + MarketIndicator.FIELD);
				}
			}// end if
		}else if(isQueryMsg && MsgType.CONFIRMATION_ACK.equals(msgType)){// AU 查询失败
			recMsgBean = recConfirmStatus.FailMsgExecutionReport(message);

		}
		return recMsgBean;
	}

	public void proccessTrade(RecMsgBean recMsgBean)throws Exception{
		RetBean retBean = null;
		if(recMsgBean instanceof RecForeignForwardSpotModel){ //即远期的实体类
			retBean = imixCPISCfetsApiService.getCpisFxSptFwdTradeService().send((RecForeignForwardSpotModel)recMsgBean);

		}else if(recMsgBean instanceof RecForeignSwapModel){ //掉期的实体类
			retBean = imixCPISCfetsApiService.getCpisFxSwpTradeService().send((RecForeignSwapModel)recMsgBean);

		}else if(recMsgBean instanceof RecForeignFxmmModel){ //外币拆借的实体类
			retBean = imixCPISCfetsApiService.getCpisFxMMTradeService().send((RecForeignFxmmModel)recMsgBean);

		}else if(recMsgBean instanceof RecInterestRateSwapModel){ //利率互换返回的实体类
			retBean = imixCPISCfetsApiService.getCpisIrsTradeService().send((RecInterestRateSwapModel)recMsgBean);

		}else if(recMsgBean instanceof FailRecConfirmStatusModel){//接收查询(确认状态)失败的实体
			retBean = imixCPISCfetsApiService.getCpisRecMsgService().send((FailRecConfirmStatusModel)recMsgBean);

		}else if(recMsgBean instanceof RecCPISFeedBackModel){//发送匹配报文后CFETS返回的匹配状态
			retBean = imixCPISCfetsApiService.getCpisRecMsgService().send((RecCPISFeedBackModel)recMsgBean);

		}else if(recMsgBean instanceof RecConfirmStatusModel){//查询报文状态
			retBean = imixCPISCfetsApiService.getCpisRecMsgService().send((RecConfirmStatusModel)recMsgBean);

		}else if(recMsgBean instanceof RecDueNotConfirmModel){//接收到期未确认推送的实体类
			retBean = imixCPISCfetsApiService.getCpisRecMsgService().send((RecDueNotConfirmModel)recMsgBean);

		}else if(recMsgBean instanceof RecMatchingModel){//报文匹配结果
//			retBean = imixCPISCfetsApiService.getCpisSwiftMatchService().send((RecMatchingModel)recMsgBean);

		}else if(recMsgBean instanceof RecForeignCrsModel){//货币掉期
			retBean = imixCPISCfetsApiService.getCpisCrsTradeService().send((RecForeignCrsModel)recMsgBean);

		}else if(recMsgBean instanceof RecConfirmStatusModel){//查询结果返回
			retBean = imixCPISCfetsApiService.getCpisRecMsgService().send((RecConfirmStatusModel)recMsgBean);

		}else if(recMsgBean instanceof RecForeignOptionModel){//外汇期权
			retBean = imixCPISCfetsApiService.getCpisFxoptTradeService().send((RecForeignOptionModel)recMsgBean);
		}else{
			throw new Exception("不支持处理此类型");
		}

		LogManager.info(retBean.getRetCode() + "," + retBean.getRetMsg() );
	}
}
