package com.singlee.financial.cfets.service;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.model.AgencyBasicInformationModel;
import com.singlee.financial.model.SendForeignFxmmModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.financial.util.AgencyBasicInformationUtil;
import com.singlee.financial.util.BigDecimalUtil;
import imix.Message;
import imix.field.*;
import imix.imix10.ExecutionReport;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public class SendForeignFxmm extends ConfirmMsgService{
	
	public SendForeignFxmm(ConfigBean configBean) {
		super(configBean);
	}

	@Override
	public void pkgMsgBody(imix.imix10.Message message, TradeConfirmBean bean)throws Exception {
		SendForeignFxmmModel model = (SendForeignFxmmModel)bean;
		 // 成交编号， 需求未约束编码规则
		message.setField(new ExecID(model.getExecID()));
		// 成交日期，格式为：YYYYMMDD 必传需匹配
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>","")));
		// 确认报文 ID
		message.setField(new ConfirmID(model.getConfirmID()));
		// 2 交易场所， 默认为CFETS 取值方式待定
		message.setField(new MarketID("2"));
		if ("2".equals(model.getNetGrossInd())) 
		{
			message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		} else {
			message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		}
		// 21 - 外币拆借
		message.setField(new MarketIndicator(MarketIndicator.FXDEPO));
		// 起息日，格式为：YYYYMMDD
		message.setField(new SettlDate(model.getSettlDate().replace("<Date>","")));
		// 到期日,格式为YYYYMMDD
		message.setField(new SettlDate2(model.getSettlDate2().replace("<Date>","")));
		// 拆借币种
		message.setField(new Currency(model.getCurrency()));
		// 拆借利率 精确度为6
		message.setField(new LastPx(BigDecimalUtil.decimalFormat(model.getLastPx(), "0.000000")));
		// 计息基准
		message.setField(new DayCount(model.getDayCount().charAt(0))); 
		// 本方交易方向
		message.setField(new Side(model.getSide().charAt(0)));
		// 应计利息 精度为9
		message.setField(new AccruedInterestTotalAmt(BigDecimalUtil.decimalFormat(model.getAccruedInterestTotalAmt(), 9)));
		// 拆借金额， 单位:对应货币单位 精度:2
		message.setField(new LastQty(BigDecimalUtil.decimalFormat(model.getLastQty(), "0.00")));
		// 到期还款金额
		message.setField(new SettlCurrAmt2(BigDecimalUtil.decimalFormat(model.getSettleCurrAmt2(), 2)));
		//交收方式
		if(StringUtils.isNotEmpty(model.getDeliveryType())){
			message.setField(new DeliveryType(Integer.valueOf(model.getDeliveryType())));
		}
		pkgSettlInfo(message, bean.getTradeSettlsBean(),bean.getNetGrossInd(),model.getDeliveryType());
	}
	
	public void pkgSettlInfo(Message message, TradeSettlsBean bean, String netGrossInd,String deliveryType)throws Exception{
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyRole(PartyRole.EXECUTING_FIRM));
		// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		own.set(new PartyID(getConfigBean().getBankId()));
		
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		cust.set(new PartyRole(PartyRole.CONTRA_FIRM));
		cust.set(new PartyID(bean.getcInstitutionId()));
		
		// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getFullName());
		
		// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getcFullName());
		
		//如果交易方式为双边清算 (NetGrossInd=2)  则为双方传入账户信息  否则不需要传入这些域
		if("2".equals(netGrossInd)){
			//组装本方清算信息
			pkgSettls(own, bean.getSettccy(), bean.getBankname(), bean.getBankopenno(), bean.getBankacctno(), 
					bean.getAcctname(), bean.getAcctopenno(), bean.getAcctno(), 
					bean.getIntermediarybankname(), bean.getIntermediarybankbiccode(), bean.getIntermediarybankacctno(), bean.getRemark());
			
			//组装对手方清算信息
			pkgSettls(cust, bean.getcSettccy(), bean.getcBankname(), bean.getcBankopenno(), bean.getcBankacctno(), 
					bean.getcAcctname(), bean.getcAcctopenno(), bean.getcAcctno(), 
					bean.getcIntermediarybankname(), bean.getcIntermediarybankbiccode(), bean.getcIntermediarybankacctno(), bean.getcRemark());
		}
		
		message.addGroup(own);
		message.addGroup(cust);
	}
	
	public void pkgSettls(ExecutionReport.NoPartyIDs iDs,String settccy,String bankname,String bankopenno,String bankacctno,
			String acctname,String acctopenno,String acctno,
			String intermediarybankname,String intermediarybankbiccode,String intermediarybankacctno,
			String remark){
		// 本方开户行名称  选填 110
		convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME, bankname);
		// 本方收款行名称 23
		convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME, acctname);
		// 本方收款行BICCODE 16
		convert(iDs,PartySubIDType.SWIFT_BIC_CODE, acctopenno);
		// 收款行账号 15
		convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER, acctno);
		// 开户行BICCODE 138
		convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_NAME, bankopenno);
		// 资金开户行账号 207
		convert(iDs,PartySubIDType.CORRESPONDENT_BANK_ACCOUNT, bankacctno);
		
		// 中间行名称 209
		convert(iDs,PartySubIDType.INTERMEDIARY_BANK, intermediarybankname);
		// 中间行BICCODE 211
		convert(iDs,PartySubIDType.INTERMEDIARY_BANK_BIC_CODE, intermediarybankbiccode);
		// 中间行账号 213
		convert(iDs,PartySubIDType.INTERMEDIARY_BANK_ACCOUNT, intermediarybankacctno);
		
		// 本方附言   选填   若填值则需要匹配 139
		convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, remark);
	}
	
	public Message packageMsg(SendForeignFxmmModel model, ConfigBean configBean) {
		Message message = MsgHeadAndStandardTrailer
				.packageMsgHeadAndStandardTrailer("AK", configBean);
		
		message.setField(new ExecID(model.getExecID())); // 成交编号， 需求未约束编码规则
																// 必传需匹配
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>",
				"")));// 成交日期，格式为：YYYYMMDD 必传需匹配

		message.setField(new ConfirmID(model.getConfirmID()));// 确认报文 ID
																	// 必传不需匹配
		// message.setField(new MarketID("2"));// 2 交易场所， 默认为CFETS 取值方式待定
		if ("2".equals(model.getNetGrossInd())) {
			message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		} else {
			message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算

		}

		message.setField(new MarketIndicator("21"));// 21 - 外币拆借

		message.setField(new SettlDate(model.getSettlDate().replace("<Date>",
				"")));// 起息日，格式为：YYYYMMDD

		message.setField(new SettlDate2(model.getSettlDate2().replace("<Date>",
				"")));
		// 到期日,格式为YYYYMMDD

		message.setField(new Currency(model.getCurrency()));// 拆借币种

		message.setField(new LastPx(BigDecimalUtil.decimalFormat(
				model.getLastPx(), "0.000000")));// 拆借利率 精确度为6

		message.setField(new DayCount(model.getDayCount().charAt(0))); // 计息基准
																		// 必传需匹配

		message.setField(new Side(model.getSide().charAt(0)));// 本方交易方向

		// 应计利息 精度为9
		message.setField(new AccruedInterestTotalAmt(BigDecimalUtil
				.decimalFormat(model.getAccruedInterestTotalAmt(), 9)));

		message.setField(new LastQty(BigDecimalUtil.decimalFormat(
				model.getLastQty(), "0.00")));// 拆借金额， 单位:对应货币单位 精度:2

		// 到期还款金额
		message.setField(new SettlCurrAmt2(BigDecimalUtil.decimalFormat(
				model.getSettleCurrAmt2(), 2)));

		// 本方
		ExecutionReport.NoPartyIDs ownnopartyids1 = new ExecutionReport.NoPartyIDs();//第一层
		ExecutionReport.NoPartyIDs.NoPartySubIDs ownnopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs(); //第二层
		ownnopartyids1.set(new PartyRole(1));
		ownnopartyids1.set(new PartyID(configBean.getBankId()));// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”

		// 对手方
		ExecutionReport.NoPartyIDs othnopartyids1 = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs othnopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		othnopartyids1.set(new PartyRole(17));
		othnopartyids1.set(new PartyID(model.getPartyID_2()));// 对手方机构
																// 21位码,如果不传输机构
																// 21位码，此域作为协议必须域，请填写“-”
		

		// 获取存取本方机构信息的集合
		List<AgencyBasicInformationModel> list1 = model.getList1();

		// 当存在这个域的时候才进行赋值操作
		if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "124"))) {
//			ownnopartysubids1.set(new PartySubID("江苏银行股份有限公司"));// 中文全称，中文全称， 如果机构中文全称不传输，
			ownnopartysubids1.set(new PartySubID( AgencyBasicInformationUtil.getPartySubID(list1, "124")));// 中文全称，中文全称， 如果机构中文全称不传输，
//													// 此组域不传输  选填 AgencyBasicInformationUtil.getPartySubID(list1, "124")
			ownnopartysubids1.set(new PartySubIDType(124));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}

		if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "5"))) {
			ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "5")));// 中文全称，中文全称， 如果机构中文全称不传输，
			// 此组域不传输  选填 AgencyBasicInformationUtil.getPartySubID(list1, "124")
			ownnopartysubids1.set(new PartySubIDType(5));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}
//			ownnopartysubids1.set(new PartySubID("BANK OF JIANGSU"));// 中文全称，中文全称， 如果机构中文全称不传输，
			// 此组域不传输  选填 AgencyBasicInformationUtil.getPartySubID(list1, "124")
//			ownnopartysubids1.set(new PartySubIDType(5));
//			ownnopartyids1.addGroup(ownnopartysubids1);
		if( StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "110"))){
			ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list1, "110")));// 资金开户行 选填
			ownnopartysubids1.set(new PartySubIDType(110));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}
		
		
		
		if(	StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "23"))){
			ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list1, "23")));// 资金账户户名即收款行名称 选填
			ownnopartysubids1.set(new PartySubIDType(23));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}
		
		
		
		if(	StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "16"))){
			ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list1, "16")));
			ownnopartysubids1.set(new PartySubIDType(16)); // 收款行BICCODE
			ownnopartyids1.addGroup(ownnopartysubids1);
		}

		
		if((StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "15")))){
			ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list1, "15"))));// 收款行账号（一般是"173001562010000093"）
			ownnopartysubids1.set(new PartySubIDType(15)); // 收款行账号
			ownnopartyids1.addGroup(ownnopartysubids1);
		}

		
		if(	StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "138"))){
			ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list1, "138"))));// 开户行BICCODE
			ownnopartysubids1.set(new PartySubIDType(138));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}

		if(	StringUtils.isNotBlank( AgencyBasicInformationUtil
				.getPartySubID(list1, "207"))){
			ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list1, "207"))));// 开户行账号
			ownnopartysubids1.set(new PartySubIDType(207));
			ownnopartyids1.addGroup(ownnopartysubids1);
			
		}
		

		if ( StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "209"))) {
			ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list1, "209"))));// 中间行名称
			ownnopartysubids1.set(new PartySubIDType(209));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}

		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "211"))) {
			ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list1, "211"))));// 中间行BICCODE
			ownnopartysubids1.set(new PartySubIDType(211));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}

		
		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "213"))) {
			ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list1, "213"))));// 中间行账号
			ownnopartysubids1.set(new PartySubIDType(213));
			ownnopartyids1.addGroup(ownnopartysubids1);
		}
		
		
		if(	StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list1, "139"))){
			ownnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list1, "139"))));// 中间行名称
			ownnopartysubids1.set(new PartySubIDType(139));
			ownnopartyids1.addGroup(ownnopartysubids1);

		}

		
		// 对手方

		List<AgencyBasicInformationModel> list2 = model.getList2();
		
		if(	StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "124"))){
			othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list2, "124")));// 中文全称， 如果机构中文全称不传输， 此组域不传输
			othnopartysubids1.set(new PartySubIDType(124));
			othnopartyids1.addGroup(othnopartysubids1);
		}
		if(	StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "5"))){
			othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list2, "5")));// 中文全称， 如果机构中文全称不传输， 此组域不传输
			othnopartysubids1.set(new PartySubIDType(5));
			othnopartyids1.addGroup(othnopartysubids1);
		}
//			othnopartysubids1.set(new PartySubID("AGRICULTURAL BANK OF CHINA"));// 中文全称， 如果机构中文全称不传输， 此组域不传输
//			othnopartysubids1.set(new PartySubIDType(5));
//			othnopartyids1.addGroup(othnopartysubids1);

		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "110"))) {
			othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list2, "110")));// 资金开户行
			othnopartysubids1.set(new PartySubIDType(110));
			othnopartyids1.addGroup(othnopartysubids1);
		}
		

		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "23"))) {
			othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list2, "23")));// 资金账户户名
			othnopartysubids1.set(new PartySubIDType(23));
			othnopartyids1.addGroup(othnopartysubids1);
		}
		
		
		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "16"))) {
			othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list2, "16")));// 支付系统行号
			othnopartysubids1.set(new PartySubIDType(16));
			othnopartyids1.addGroup(othnopartysubids1);

		}

		
		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "15"))) {
			othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil
					.getPartySubID(list2, "15")));// 资金账号
			othnopartysubids1.set(new PartySubIDType(15));
			othnopartyids1.addGroup(othnopartysubids1);
		}
		
	

		if ((StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "138")))){
			othnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list2, "138"))));// 开户行BICCODE
			othnopartysubids1.set(new PartySubIDType(138));
			othnopartyids1.addGroup(othnopartysubids1);
		}
		

		if (StringUtils.isNotBlank(	AgencyBasicInformationUtil
				.getPartySubID(list2, "207"))) {
			othnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list2, "207"))));// 开户行账号
			othnopartysubids1.set(new PartySubIDType(207));
			othnopartyids1.addGroup(othnopartysubids1);
		}
		

		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "209"))) {
			othnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list2, "209"))));// 中间行名称
			othnopartysubids1.set(new PartySubIDType(209));
			othnopartyids1.addGroup(othnopartysubids1);

		}
		
		if (StringUtils.isNotBlank(	AgencyBasicInformationUtil
				.getPartySubID(list2, "211"))) {
			othnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list2, "211"))));// 中间行BICCODE
			othnopartysubids1.set(new PartySubIDType(211));
			othnopartyids1.addGroup(othnopartysubids1);
		}
		
		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "213"))) {
			othnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list2, "213"))));// 中间行账号
			othnopartysubids1.set(new PartySubIDType(213));
			othnopartyids1.addGroup(othnopartysubids1);
		}

		

		if (StringUtils.isNotBlank(AgencyBasicInformationUtil
				.getPartySubID(list2, "139"))) {
			othnopartysubids1.set(new PartySubID((AgencyBasicInformationUtil
					.getPartySubID(list2, "139"))));// 中间行名称
			othnopartysubids1.set(new PartySubIDType(139));

			othnopartyids1.addGroup(othnopartysubids1);

		}
		message.addGroup(ownnopartyids1);
		message.addGroup(othnopartyids1);
		System.out.println(message);
		return message;

	}

	@Override
	public void pkgMsgBody(imix.imix10.Message message,
			List<SendForeignOptionModel> beans) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
