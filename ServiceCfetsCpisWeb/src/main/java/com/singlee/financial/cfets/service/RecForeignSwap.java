package com.singlee.financial.cfets.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecForeignSwapModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.CnfmTime;
import imix.field.CnfmTimeType;
import imix.field.ConfirmID;
import imix.field.ConfirmStatus;
import imix.field.DealTransType;
import imix.field.ExecID;
import imix.field.ExecType;
import imix.field.LegCalculatedCcyLastQty;
import imix.field.LegCurrency;
import imix.field.LegCurrency1;
import imix.field.LegCurrency2;
import imix.field.LegLastPx;
import imix.field.LegLastQty;
import imix.field.LegSettlDate;
import imix.field.LegSide;
import imix.field.MarketIndicator;
import imix.field.NetGrossInd;
import imix.field.NoPartyIDs;
import imix.field.NoPartySubIDs;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.SecurityID;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;


/**
 * 
 * <P>RecForeignSwap.java</P>
 * <P>接收外币掉期的推送消息处理类</P>
 * <P>Company: 新利</P>
 * <P>          修改者姓名 修改内容说明</P>
 * @author pengqian
 * @see     参考类1
 */
public class RecForeignSwap {

	/**
	 * 解析报文
	 * @param message
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	
	private static Logger LogManager = LoggerFactory.getLogger(RecForeignSwap.class);
	
	public RecForeignSwapModel ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		
		MessageUtil msgUtil = new MessageUtil(message);
		
		//PropertiesUtil prop = new PropertiesUtil();
		
		RecForeignSwapModel recForeignSwapModel = new RecForeignSwapModel();
		
		try {
			
			String	execID = msgUtil.getStringValue(ExecID.FIELD);
			
			recForeignSwapModel.setExecID(execID);
				
			String	tradeDate = msgUtil.getStringValue(TradeDate.FIELD);
			
			recForeignSwapModel.setTradeDate(tradeDate);
				
			String	execType = msgUtil.getStringValue(ExecType.FIELD);
			
			recForeignSwapModel.setExecType(execType);
				
			String	dealTransType = msgUtil.getStringValue(DealTransType.FIELD);
			
			recForeignSwapModel.setDealTransType(dealTransType);
				
			String	confirmID = msgUtil.getStringValue(ConfirmID.FIELD);
			
			recForeignSwapModel.setConfirmID(confirmID);
				
			String	confirmStatus = msgUtil.getStringValue(ConfirmStatus.FIELD);
			
			recForeignSwapModel.setConfirmStatus(confirmStatus);

			String	netGrossInd = msgUtil.getStringValue(NetGrossInd.FIELD);
			
			recForeignSwapModel.setNetGrossInd(netGrossInd);
		
			String	marketIndicator = msgUtil.getStringValue(MarketIndicator.FIELD);
			
			recForeignSwapModel.setMarketIndicator(marketIndicator);
			
			NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
			
			
			CnfmTimeType cnfmTimeType = new CnfmTimeType();
			CnfmTime cnfmTime = new CnfmTime();
			
			int ownCnfmTimeType = 0 ;
			String ownCnfmTime = "";
			int othCnfmTimeType = 0 ;
			String othCnfmTime = "";
				
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
				message.getGroup(i, noCnfmTimes);
				cnfmTimeType = noCnfmTimes.getCnfmTimeType();
				cnfmTime = noCnfmTimes.getCnfmTime();
				if (cnfmTimeType.getValue() == 1) {
					ownCnfmTimeType = cnfmTimeType.getValue();
					ownCnfmTime = cnfmTime.getValue();
				}else if (cnfmTimeType.getValue() == 2) {
					othCnfmTimeType = cnfmTimeType.getValue();
					othCnfmTime = cnfmTime.getValue();
				}
				
			}
			recForeignSwapModel.setOwnCnfmTime(ownCnfmTime);
			recForeignSwapModel.setOthCnfmTime(othCnfmTime);
			ExecutionReport.NoLegs noLegs = new ExecutionReport.NoLegs();
			
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoLegs.FIELD); i++) {
				message.getGroup(i, noLegs);
				if("11".equals(marketIndicator)){
					//近端
					if("B".equals(msgUtil.getStringValue(noLegs,LegSide.FIELD)) && i == 1){
						String	nearEndValueDate = msgUtil.getStringValue(noLegs,LegSettlDate.FIELD);
						
						recForeignSwapModel.setNearEndValueDate(nearEndValueDate);
						
						String	nearEndPrice = msgUtil.getStringValue(noLegs,LegLastPx.FIELD);
						
						recForeignSwapModel.setNearEndPrice(nearEndPrice);
						
						String	nearEndSellAmount = msgUtil.getStringValue(noLegs,LegCalculatedCcyLastQty.FIELD);
						
						recForeignSwapModel.setNearEndSellAmount(nearEndSellAmount);
						
						String	nearEndBuyAmount = msgUtil.getStringValue(noLegs,LegLastQty.FIELD);
						
						recForeignSwapModel.setNearEndBuyAmount(nearEndBuyAmount);
						
						String	nearEndSellCcy = msgUtil.getStringValue(noLegs,LegCurrency2.FIELD);
						
						recForeignSwapModel.setNearEndSellCcy(nearEndSellCcy);
						
						String	nearEndBuyCcy = msgUtil.getStringValue(noLegs,LegCurrency1.FIELD);
						
						recForeignSwapModel.setNearEndBuyCcy(nearEndBuyCcy);
					}
					//远端
					if ("B".equals(msgUtil.getStringValue(noLegs,LegSide.FIELD)) && i == 2) {
						String	farEndValueDate = msgUtil.getStringValue(noLegs,LegSettlDate.FIELD);
						
						recForeignSwapModel.setFarEndValueDate(farEndValueDate);
						
						String	farEndPrice = msgUtil.getStringValue(noLegs,LegLastPx.FIELD);
						
						recForeignSwapModel.setFarEndPrice(farEndPrice);
						
						String	farEndSellAmount = msgUtil.getStringValue(noLegs,LegCalculatedCcyLastQty.FIELD);
						
						recForeignSwapModel.setFarEndSellAmount(farEndSellAmount);
						
						String	farEndBuyAmount = msgUtil.getStringValue(noLegs,LegLastQty.FIELD);
						
						recForeignSwapModel.setFarEndBuyAmount(farEndBuyAmount);
						
						String	farEndSellCcy = msgUtil.getStringValue(noLegs,LegCurrency2.FIELD);
						
						recForeignSwapModel.setFarEndSellCcy(farEndSellCcy);
						
						String	farEndBuyCcy = msgUtil.getStringValue(noLegs,LegCurrency1.FIELD);	
						
						recForeignSwapModel.setFarEndBuyCcy(farEndBuyCcy);
					}
				}else if ("27".equals(marketIndicator)){
					 String securityID=msgUtil.getStringValue(SecurityID.FIELD);
					 recForeignSwapModel.setSecurityID(securityID);
					//近端
					if( i == 1){
						String  legSide_1=msgUtil.getStringValue(noLegs,LegSide.FIELD);
						
						recForeignSwapModel.setLegSide_1(legSide_1);
						
						String	nearEndValueDate = msgUtil.getStringValue(noLegs,LegSettlDate.FIELD);
						
						recForeignSwapModel.setNearEndValueDate(nearEndValueDate);
						
						String	nearEndPrice = msgUtil.getStringValue(noLegs,LegLastPx.FIELD);
						
						recForeignSwapModel.setNearEndPrice(nearEndPrice);
						
						if("1".equals(msgUtil.getStringValue(noLegs,LegSide.FIELD))){
							String	nearEndSellAmount = msgUtil.getStringValue(noLegs,LegCalculatedCcyLastQty.FIELD);
							
							recForeignSwapModel.setNearEndSellAmount(nearEndSellAmount);
							
							String	nearEndBuyAmount = msgUtil.getStringValue(noLegs,LegLastQty.FIELD);
							
							recForeignSwapModel.setNearEndBuyAmount(nearEndBuyAmount);
							
							String	nearEndSellCcy ="CNY";
							
							recForeignSwapModel.setNearEndSellCcy(nearEndSellCcy);
							
							String	nearEndBuyCcy = msgUtil.getStringValue(noLegs,LegCurrency.FIELD);
							
							recForeignSwapModel.setNearEndBuyCcy(nearEndBuyCcy);
						}else{
							String	nearEndSellAmount = msgUtil.getStringValue(noLegs,LegLastQty.FIELD);
							
							recForeignSwapModel.setNearEndSellAmount(nearEndSellAmount);
							
							String	nearEndBuyAmount = msgUtil.getStringValue(noLegs,LegCalculatedCcyLastQty.FIELD);
							
							recForeignSwapModel.setNearEndBuyAmount(nearEndBuyAmount);
							
							String	nearEndSellCcy =msgUtil.getStringValue(noLegs,LegCurrency.FIELD);
							
							recForeignSwapModel.setNearEndSellCcy(nearEndSellCcy);
							
							String	nearEndBuyCcy = "CNY";
							
							recForeignSwapModel.setNearEndBuyCcy(nearEndBuyCcy);
						}
						
					}
					//远端
					if ( i == 2) {
						
						String  legSide_2=msgUtil.getStringValue(noLegs,LegSide.FIELD);
						
						recForeignSwapModel.setLegSide_2(legSide_2);
						
						String	farEndValueDate = msgUtil.getStringValue(noLegs,LegSettlDate.FIELD);
						
						recForeignSwapModel.setFarEndValueDate(farEndValueDate);
						
						String	farEndPrice = msgUtil.getStringValue(noLegs,LegLastPx.FIELD);
						
						recForeignSwapModel.setFarEndPrice(farEndPrice);
						
						if("1".equals(msgUtil.getStringValue(noLegs,LegSide.FIELD))){
							String	farEndSellAmount = msgUtil.getStringValue(noLegs,LegCalculatedCcyLastQty.FIELD);
							
							recForeignSwapModel.setFarEndSellAmount(farEndSellAmount);
							
							String	farEndBuyAmount = msgUtil.getStringValue(noLegs,LegLastQty.FIELD);
							
							recForeignSwapModel.setFarEndBuyAmount(farEndBuyAmount);
							
							String	farEndSellCcy = "CNY";
							
							recForeignSwapModel.setFarEndSellCcy(farEndSellCcy);
							
							String	farEndBuyCcy = msgUtil.getStringValue(noLegs,LegCurrency.FIELD);	
							
							recForeignSwapModel.setFarEndBuyCcy(farEndBuyCcy);
						}else{
							String	farEndSellAmount = msgUtil.getStringValue(noLegs,LegLastQty.FIELD);
							
							recForeignSwapModel.setFarEndSellAmount(farEndSellAmount);
							
							String	farEndBuyAmount = msgUtil.getStringValue(noLegs,LegCalculatedCcyLastQty.FIELD);
							
							recForeignSwapModel.setFarEndBuyAmount(farEndBuyAmount);
							
							String	farEndSellCcy = msgUtil.getStringValue(noLegs,LegCurrency.FIELD);
							
							recForeignSwapModel.setFarEndSellCcy(farEndSellCcy);
							
							String	farEndBuyCcy ="CNY";	
							
							recForeignSwapModel.setFarEndBuyCcy(farEndBuyCcy);
						}
					}
				}
		}
			
			
		
			int ownPartyRole = 0 ;
			
			int othPartyRole = 0 ;
			
			int partyrole=-1; //本方对手方
			
			String partyid="";//21位机构码
			
			String partysubid="";
		    
		    int partysubidtype=-1;
			
			ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
			
			ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			
			int nopartysubids=-1;//清算信息循环组
			
			for (int k = 1; k <= msgUtil.getIntValue(NoPartyIDs.FIELD); k++) {
				message.getGroup(k, nopartyids1);
				
				partyrole=msgUtil.getIntValue(nopartyids1, PartyRole.FIELD);

				partyid=msgUtil.getStringValue(nopartyids1, PartyID.FIELD);
				
				if(partyrole==1){
					String ownPartyID = partyid;
					
					recForeignSwapModel.setOwnPartyID(ownPartyID);
					
					ownPartyRole = partyrole;
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					
					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						
						if (partysubidtype== 124) {
							String ownCnName =  partysubid;
							recForeignSwapModel.setOwnCnName(ownCnName);
						
						}else if (partysubidtype== 178) {
							String nearEndOwnCapitalBakeName =  partysubid;
							recForeignSwapModel.setNearEndOwnCapitalBakeName(nearEndOwnCapitalBakeName);
						
						}else if (partysubidtype== 176) {
							String	nearEndOwnCapitalAccountName =  partysubid;
							
							recForeignSwapModel.setNearEndOwnCapitalAccountName(nearEndOwnCapitalAccountName);
							
						
						}else if (partysubidtype== 182) {
							
							String nearEndOwnPayBankNo =  partysubid;
							
							recForeignSwapModel.setNearEndOwnPayBankNo(nearEndOwnPayBankNo);
						
						}else if (partysubidtype== 180) {
							String	nearEndOwnCapitalAccount =  partysubid;
							
							recForeignSwapModel.setNearEndOwnCapitalAccount(nearEndOwnCapitalAccount);
						
						}else if (partysubidtype== 139) {
							String	nearEndOwnRemark =  partysubid;
							
							recForeignSwapModel.setNearEndOwnRemark(nearEndOwnRemark);
						
						}else if (partysubidtype== 216) {
							String	farEndMidCnName =  partysubid;
							recForeignSwapModel.setFarEndMidCnName(farEndMidCnName);
						
						}else if (partysubidtype== 218) {
							String  farEndMidBankBIC =  partysubid;
							recForeignSwapModel.setFarEndMidBankBIC(farEndMidBankBIC);
						
						}else if (partysubidtype== 220) {
							String	farEndMidBankAccount =  partysubid;
							recForeignSwapModel.setFarEndMidBankAccount(farEndMidBankAccount);
						
						}else if (partysubidtype== 194) {
							String	farEndOwnCapitalBakeName =  partysubid;
							recForeignSwapModel.setFarEndOwnCapitalBakeName(farEndOwnCapitalBakeName);
						
						}else if (partysubidtype== 191) {
							String	farEndOwnCapitalAccountBIC =  partysubid;
							recForeignSwapModel.setFarEndOwnCapitalAccountBIC(farEndOwnCapitalAccountBIC);
							
						}else if (partysubidtype== 222) {
							String	farEndOwnCapitalAccount =  partysubid;
							recForeignSwapModel.setFarEndOwnCapitalAccount(farEndOwnCapitalAccount);
						
						}else if (partysubidtype== 192) {
							String	farEndOwnReceiptBankName =  partysubid;
							recForeignSwapModel.setFarEndOwnReceiptBankName(farEndOwnReceiptBankName);
						
						}else if (partysubidtype== 198) {
							String	farEndOwnReceiptBankBIC =  partysubid;
							recForeignSwapModel.setFarEndOwnReceiptBankBIC(farEndOwnReceiptBankBIC);
						
						}else if (partysubidtype== 196) {
							String	farEndOwnReceiptBankAccount =  partysubid;
							recForeignSwapModel.setFarEndOwnReceiptBankAccount(farEndOwnReceiptBankAccount);
						
						}else if (partysubidtype== 145) {
							String	farEndOwnRemark =  partysubid;
							recForeignSwapModel.setFarEndOwnRemark(farEndOwnRemark);
						}
					}
				}
				if(partyrole==17){
					String	othPartyID = partyid;
					
					recForeignSwapModel.setOthPartyID(othPartyID);
					
					ownPartyRole = partyrole;
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					
					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						if (partysubidtype== 124) {
							String	othCnName =  partysubid;	
							recForeignSwapModel.setOthCnName(othCnName);
						}else if (partysubidtype== 194) {
							String	farEndOthCapitalBakeName =  partysubid;
							recForeignSwapModel.setFarEndOthCapitalBakeName(farEndOthCapitalBakeName);
							
						}else if (partysubidtype== 192) {
							String	farEndOthCapitalAccountName =  partysubid;
							recForeignSwapModel.setFarEndOthCapitalAccountName(farEndOthCapitalAccountName);
							
						}else if (partysubidtype== 198) {
							String	farEndOthPayBankNo =  partysubid;
							recForeignSwapModel.setFarEndOthPayBankNo(farEndOthPayBankNo);
							
						}else if (partysubidtype== 196) {
							String	farEndOthCapitalAccount =  partysubid;
							recForeignSwapModel.setFarEndOthCapitalAccount(farEndOthCapitalAccount);
							
						}else if (partysubidtype== 145) {
							String	farEndOthRemark =  partysubid;
							recForeignSwapModel.setFarEndOthRemark(farEndOthRemark);
							
						}else if (partysubidtype== 215) {
							String	nearEndMidCnName =  partysubid;
							recForeignSwapModel.setNearEndMidCnName(nearEndMidCnName);
							
						}else if (partysubidtype== 217) {
							String	nearEndMidBankBIC =  partysubid;
							recForeignSwapModel.setNearEndMidBankBIC(nearEndMidBankBIC);
							
						}else if (partysubidtype== 219) {
							String	nearEndMidBankAccount =  partysubid;
							recForeignSwapModel.setNearEndMidBankAccount(nearEndMidBankAccount);
							
						}else if (partysubidtype== 178) {
							String	nearEndOthCapitalBakeName =  partysubid;
							recForeignSwapModel.setNearEndOthCapitalBakeName(nearEndOthCapitalBakeName);
							
						}else if (partysubidtype== 175) {
							String	nearEndOthCapitalAccountBIC =  partysubid;
							recForeignSwapModel.setNearEndOthCapitalAccountBIC(nearEndOthCapitalAccountBIC);
							
						}else if (partysubidtype== 221) {
							String	nearEndOthCapitalAccount =  partysubid;
							recForeignSwapModel.setNearEndOthCapitalAccount(nearEndOthCapitalAccount);
							
						}else if (partysubidtype== 176) {
							String	nearEndOthReceiptBankName =  partysubid;
							recForeignSwapModel.setNearEndOthReceiptBankName(nearEndOthReceiptBankName);
							
						}else if (partysubidtype== 182) {
							String	nearEndOthReceiptBankBIC =  partysubid;
							recForeignSwapModel.setNearEndOthReceiptBankBIC(nearEndOthReceiptBankBIC);
							
						}else if (partysubidtype== 180) {
							String	nearEndOthReceiptAccount =  partysubid;
							recForeignSwapModel.setNearEndOthReceiptAccount(nearEndOthReceiptAccount);
							
						}else if (partysubidtype== 139) {
							String	nearEndOthRemark =  partysubid;
							recForeignSwapModel.setNearEndOthRemark(nearEndOthRemark);
						}
					}
				}
			}
			
			RecPageInfoModel pageInfo = RecPageInfo.msgPageToModel(message);
			
			recForeignSwapModel.setPageInfo(pageInfo);
			
			
			
		} catch (Exception e) {
			LogManager.info("解析报文出错"+e);
			throw e;
		}
		return recForeignSwapModel;
	}
	
	public RecForeignSwapModel ExecutionReportCancel(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		MessageUtil msgUtil = new MessageUtil(message);
		RecForeignSwapModel recForeignSwapModel = new RecForeignSwapModel();
		try {
			String execID  = msgUtil.getStringValue(ExecID.FIELD);
			String tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);
			String marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
			String execType  = msgUtil.getStringValue(ExecType.FIELD);
			String confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
			
			
			if ("".equals(execID)) {
				LogManager.info("消息未收到域：ExecID "+ExecID.FIELD);
			}	
			if ("".equals(tradeDate)) {
				LogManager.info("消息未收到域：TradeDate "+TradeDate.FIELD);
			}	
			if ("".equals(marketIndicator)) {
				LogManager.info("消息未收到域：MarketIndicator "+MarketIndicator.FIELD);
			}			
			if ("".equals(execType)) {
				LogManager.info("消息未收到域：ExecType "+ExecType.FIELD);
			}			
			if ("".equals(confirmID)) {
				LogManager.info("消息未收到域：ConfirmID "+ConfirmID.FIELD);
			}			
			
			recForeignSwapModel.setExecID(execID);
			recForeignSwapModel.setTradeDate(tradeDate);
			recForeignSwapModel.setMarketIndicator(marketIndicator);
			recForeignSwapModel.setExecType(execType);
			recForeignSwapModel.setConfirmID(confirmID);
			
			//分页信息
			RecPageInfoModel pageInfo = RecPageInfo.msgPageToModel(message);
			recForeignSwapModel.setPageInfo(pageInfo);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return recForeignSwapModel;
	}
}
