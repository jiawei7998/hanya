package com.singlee.financial.cfets.service;

import com.singlee.financial.cfets.bean.FailRecConfirmStatusModel;
import com.singlee.financial.cfets.bean.RecConfirmStatusModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;
import com.singlee.financial.cfets.cpis.MessageUtil;
import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.*;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;

import java.io.IOException;

/**
 * 
 * <P>
 * RecConfirmStatus.java
 * </P>
 * <P>
 * 接收交易确认查询返回（确认状态）IMIX 报文标准
 * Company: 新利
 * @author pengqian
 * @see 参考类1
 */
public class RecConfirmStatus {

	/**
	 * 解析报文
	 */
	public RecConfirmStatusModel ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {

		MessageUtil msgUtil = new MessageUtil(message);

		RecPageInfoModel pageInfo = new RecPageInfoModel();
		// 接受确认状态的实体
		RecConfirmStatusModel confirmStatusModel = new RecConfirmStatusModel();

		String confirmType = msgUtil.getStringValue(ConfirmType.FIELD);

		confirmStatusModel.setConfirmType(confirmType);

		String confirmStatus = msgUtil.getStringValue(ConfirmStatus.FIELD);

		confirmStatusModel.setConfirmStatus(confirmStatus);

		String queryRequestID = msgUtil.getStringValue(QueryRequestID.FIELD);

		confirmStatusModel.setQueryRequestID(queryRequestID);

		String execID = msgUtil.getStringValue(ExecID.FIELD);

		confirmStatusModel.setExecID(execID);

		String tradeDate = msgUtil.getStringValue(TradeDate.FIELD);

		confirmStatusModel.setTradeDate(tradeDate);

		String marketIndicator = msgUtil.getStringValue(MarketIndicator.FIELD);

		confirmStatusModel.setMarketIndicator(marketIndicator);

		String ownCnfmTimeType = "";
		String othCnfmTimeType = "";
		NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
		CnfmTimeType cnfmTimeType = new CnfmTimeType();
		CnfmTime cnfmTime = new CnfmTime();
		for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
			message.getGroup(i, noCnfmTimes);
			cnfmTimeType = noCnfmTimes.getCnfmTimeType();
			cnfmTime = noCnfmTimes.getCnfmTime();
			//String cnfmTimeType = msgUtil.getStringValue(CnfmTimeType.FIELD);
			if (cnfmTimeType.getValue()==1) {
				ownCnfmTimeType = cnfmTimeType.toString();
				confirmStatusModel.setOwnCnfmTimeType(ownCnfmTimeType);
				confirmStatusModel.setOwnCnfmTime(cnfmTime.getValue());
			} else if (cnfmTimeType.getValue() == 2) {
				othCnfmTimeType = cnfmTimeType.toString();
				confirmStatusModel.setOthCnfmTimeType(othCnfmTimeType);
				confirmStatusModel.setOthCnfmTime(cnfmTime.getValue());
			}
		}

		String ownPartyID = ""; // 本方机构 21 位码
		int ownPartyRole = 0;

		String othPartyID = "";// 对手方机构 21 位码
		int othPartyRole = 0;

		String ownCnName = ""; // 中文全称
		String othCnName = ""; // 中文全称

		PartyID partyid = new PartyID();
		PartyRole partyrole = new PartyRole();
		PartySubID partysubid = new PartySubID();
		PartySubIDType partysubidtype = new PartySubIDType();
		NoPartyIDs nopartyids = new NoPartyIDs();
		ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		NoPartySubIDs nopartysubids = new NoPartySubIDs();

		for (int k = 1; k <= message.getField(nopartyids).getValue(); k++) {
			message.getGroup(k, nopartyids1);
			nopartyids1.get(partyrole);
			nopartyids1.get(partyid);

			if (partyrole.getValue() == 1) {
				ownPartyID = partyid.getValue();
				confirmStatusModel.setOwnPartyID(ownPartyID);
				ownPartyRole = partyrole.getValue();
				confirmStatusModel.setOwnPartyRole(ownPartyRole);
				nopartyids1.get(nopartysubids);
				for (int g = 1; g <= nopartysubids.getValue(); g++) {
					nopartyids1.getGroup(g, nopartysubids1);
					nopartysubids1.get(partysubid);
					nopartysubids1.get(partysubidtype);
					if (partysubidtype.getValue() == 124) {
						ownCnName = partysubid.getValue();
						confirmStatusModel.setOwnCnName(ownCnName);
					}
				}
			}
			if (partyrole.getValue() == 17) {
				othPartyID = partyid.getValue();
				confirmStatusModel.setOthPartyID(othPartyID);
				othPartyRole = partyrole.getValue();
				confirmStatusModel.setOthPartyRole(othPartyRole);
				nopartyids1.get(nopartysubids);
				for (int g = 1; g <= nopartysubids.getValue(); g++) {
					nopartyids1.getGroup(g, nopartysubids1);
					nopartysubids1.get(partysubid);
					nopartysubids1.get(partysubidtype);
					if (partysubidtype.getValue() == 124) {
						othCnName = partysubid.getValue();
						confirmStatusModel.setOthCnName(othCnName);
					}
				}
			}

		}

		pageInfo = RecPageInfo.msgPageToModel(message);

		confirmStatusModel.setPageInfo(pageInfo);

		return confirmStatusModel;
	}

	/**
	 * 解析查询失败IMIX消息
	 * 
	 * @param executionReport
	 * @throws FieldNotFound
	 * @throws IOException
	 */
	public FailRecConfirmStatusModel FailMsgExecutionReport(Message message)throws FieldNotFound, IOException {
		MessageUtil msgUtil = new MessageUtil(message);
		// 接受查询(确认状态)失败的实体类
		FailRecConfirmStatusModel failRecConfirmStatusModel = new FailRecConfirmStatusModel();
		// 0-数据匹配 其他-对应的问题编码
		failRecConfirmStatusModel.setAffirmStatus(msgUtil.getStringValue(AffirmStatus.FIELD));
		// 查询匹配状态 1-数据不匹配
		failRecConfirmStatusModel.setMatchStatus(msgUtil.getStringValue(MatchStatus.FIELD));
		// 查询请求编号
		failRecConfirmStatusModel.setQueryRequestID(msgUtil.getStringValue(QueryRequestID.FIELD));
		// 未匹配成功字段
		failRecConfirmStatusModel.setText(msgUtil.getStringValue(Text.FIELD));
		return failRecConfirmStatusModel;
	}

}
