package com.singlee.financial.cfets.service;

import imix.ConfigError;
import imix.Message;
import imix.field.ConfirmType;
import imix.field.ExecID;
import imix.field.MarketID;
import imix.field.MarketIndicator;
import imix.field.MatchMsgPartyID;
import imix.field.MatchMsgPartyRole;
import imix.field.MatchMsgSubID;
import imix.field.MatchMsgSubType;
import imix.field.NetGrossInd;
import imix.field.QueryEndDate;
import imix.field.QueryExecType;
import imix.field.QueryRequestID;
import imix.field.QueryStartDate;
import imix.field.QueryStartNumber;
import imix.field.TradeInstrument;
import imix.imix10.QueryRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.cpis.ImixCPISClient;
import com.singlee.financial.model.MatchingModel;
import com.singlee.financial.model.QueryModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;
import com.singlee.financial.util.DateUtil;
/**
 * 
 * <P>
 * SendFXQuery.java
 * </P>
 * <P>
 * 发送即远掉的查询报文处理类
 * </P>
 * <P>
 * Copyright: Copyright (c) 2018
 * </P>
 * <P>
 * Company: 新利
 * </P>
 * <P>
 * 修改者姓名 修改内容说明
 * </P>
 * 
 * @see 参考类1
 */
public class SendQuery {
	/**
	 * 拼装上送IMIX消息报文
	 * 
	 * @param prop
	 * @return
	 * @throws ConfigError
	 */
//	@Autowired
	private ImixCPISClient imixClient;
	
	private static Logger LogManager = LoggerFactory.getLogger(SendQuery.class);
	
	public Message packageMsg(QueryModel model,ImixCPISClient imixCPISClient, ConfigBean configBean) throws ConfigError {
		Message message = MsgHeadAndStandardTrailer
				.packageMsgHeadAndStandardTrailer("BH", configBean);
		imixClient = imixCPISClient;
		String confirmType = model.getConfirmType(); // 请求类型：1-确认状态查询，106-确认成交明细信息
		if (StringUtils.isNotBlank(confirmType)) {
			if (confirmType.equals(String.valueOf(ConfirmType.BASIC_CONFIRMATION_INFORMATION))) 
			{
				message.setField(new ConfirmType(ConfirmType.BASIC_CONFIRMATION_INFORMATION));
			} else {
				message.setField(new ConfirmType(ConfirmType.STATUS));
			}
		} else {
			message.setField(new ConfirmType(ConfirmType.STATUS));
		}
		/***
		 * // 0-all， 当会员同时查
		// 询外汇业务时： 10176
		// 取值0，
		// 10315取值0；
		// 查询外汇即远掉的交
		// 易，分三条报文查询
		// 10176=11、
		// 10176=12、
		// 10176=14 。 不
		// 传 输10315
		 * 
		 * 
		 */
		String tradeInstrument = model.getTradeInstrucment();
		// 0-all； 11-外汇掉期；12-外汇即期；14-外汇远期;21-外币拆借
		String marketIndicator = model.getMarketIndicator();
		//交易场所 2-CFETS 4-其他                              
		 String marketID = model.getMarketID();
		if (StringUtils.isNotBlank(marketID)) {
			message.setField(new MarketID(marketID));
		}

		if (tradeInstrument != null && "0".equals(tradeInstrument)
				&& marketIndicator != null && "0".equals(marketIndicator)) {
			message.setField(new TradeInstrument('0'));
			message.setField(new MarketIndicator("0"));
		} else {
			message.setField(new MarketIndicator(marketIndicator));
		}

		String execID = model.getExecID(); // 查询成交编号
		if (StringUtils.isNotBlank(execID)) {
			message.setField(new ExecID(execID));
		}

		/***
		 * // 清算方式 2-双边自行清算 5-上海清算所清算
		// 4-净额+全额 （表示查询所有
		// 清算方式的交易）
		// 1-净额轧差（询价） ；
		// 2-双边全额清算；
	    // 4-netandgross-- 净额+全额（全部）
		 */
		String netGrossInd = model.getNetGrossInd(); 
		if (!"".equals(netGrossInd.trim()) && !"0".equals(netGrossInd)) {
			message.setField(new NetGrossInd(Integer.parseInt(netGrossInd)));
		} else {
			message.setField(new NetGrossInd(4));
		}

		String queryEndDate = model.getQueryEndDate();// 查询结束日 日期格式YYYYMMDD
		if (StringUtils.isNotBlank(queryEndDate)) {
			message.setField(new QueryEndDate(queryEndDate));
		}

		String queryStartDate = model.getQueryStartDate(); // 查询起始日 格式YYYYMMDD;
		if (StringUtils.isNotBlank(queryStartDate)) {
			message.setField(new QueryStartDate(queryStartDate));
		}

		String queryStartNumber = model.getQueryStartNumber(); // 查询返回起始结果数
		if (!"".equals(queryStartNumber.trim())) {
			message.setField(new QueryStartNumber(Integer.parseInt(queryStartNumber)));
		}
		// 查询请求编号和返回的 AK报文对应
		message.setField(new QueryRequestID(model.getQueryRequestID()));
		// 0--New--正 常有效）;4--Canceled--撤销;102--All--全部
		String queryExecType = model.getQueryExecType(); 
		if (StringUtils.isNotBlank(queryExecType)) {
			message.setField(new QueryExecType(queryExecType));
		}
		return message;
	}

	/**
	 * 组装自动查询的报文
	 * 
	 * @param type
	 * @param StartNum
	 * @return
	 */
	public Message packageMsg(char instrment, int type, int StartNum, ConfigBean configBean) {
		Message message = MsgHeadAndStandardTrailer
				.packageMsgHeadAndStandardTrailer("BH", configBean);

		message.setField(new ConfirmType(type));
		if ("0".equals(String.valueOf(instrment))) {
			message.setField(new TradeInstrument(instrment));
			message.setField(new MarketIndicator("0"));
		} else {
			message.setField(new MarketIndicator("2"));
		}
		message.setField(new NetGrossInd(4));
		message.setField(new QueryEndDate(DateUtil.getDate()));
		// message.setField(new QueryStartDate("20160501"));
		message.setField(new QueryStartDate(DateUtil.getDate()));
		message.setField(new QueryStartNumber(StartNum));
		message.setField(new QueryRequestID("" + System.currentTimeMillis()));
		message.setField(new QueryExecType("102"));

		return message;
	}
	/**
	 * 查询下一页
	 * 
	 * @param propUtil
	 */
	//p:根据返回的分页信息查询下一页
	public void sendNextPage(RecPageInfoModel model, Message message) {
		if (null != model && "N".equals(model.getLastRptRequested())// 是否是最后一条报文
				&& "Y".equals(model.getLastRptCurrPage())) { //是否是当前页最后一条报文

			//Message message = qryMap.get(model.getQueryRequestID()); //p:反向获取到刚刚查询时对应的报文
			if (message == null) {
				return;
			}
			message.setField(new QueryRequestID("" + System.currentTimeMillis()));
			message.setField(new QueryStartNumber(1 + Integer.parseInt(model.getCurrentNumReports())));
			boolean flag = imixClient.getImixSession().send(message);

			if (flag) {
				LogManager.info("发送查询下一页报文成功！");
			} else {									
				LogManager.info("发送查询下一页报文失败！");
			}
		}
	}
	
	public Message packageMsg(MatchingModel model,ImixCPISClient imixCPISClient, ConfigBean configBean) throws ConfigError {
		Message message = MsgHeadAndStandardTrailer.packageMsgHeadAndStandardTrailer("U02", configBean);
		imixClient = imixCPISClient;
		message.setField(new QueryRequestID(model.getQueryRequestId()));
		QueryRequest.NoMatchMsgDetailIDs msgDetail=new QueryRequest.NoMatchMsgDetailIDs();
		QueryRequest.NoMatchMsgDetailIDs.NoMatchMsgDetailSubIDs msgDetailSub =new QueryRequest.NoMatchMsgDetailIDs.NoMatchMsgDetailSubIDs();
		msgDetail.set(new MatchMsgPartyID("-"));
		msgDetail.set(new MatchMsgPartyRole(1));
		msgDetailSub.set(new MatchMsgSubID(model.getMatchMsgSubId()));
		msgDetailSub.set(new MatchMsgSubType(1));
		msgDetail.addGroup(msgDetailSub);
		message.addGroup(msgDetail);
		return message;
	}

}
																												