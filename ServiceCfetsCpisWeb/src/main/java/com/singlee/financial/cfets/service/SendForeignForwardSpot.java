package com.singlee.financial.cfets.service;
import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.model.AgencyBasicInformationModel;
import com.singlee.financial.model.SendForeignForwardSpotModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.financial.util.AgencyBasicInformationUtil;
import com.singlee.financial.util.BigDecimalUtil;
import imix.ConfigError;
import imix.Message;
import imix.field.*;
import imix.imix10.ExecutionReport;
import org.apache.commons.lang.StringUtils;

import java.util.List;
/**
 * <P>发送外汇即远期报文处理类</P>
 */
public class SendForeignForwardSpot extends ConfirmMsgService{
	
	public SendForeignForwardSpot(ConfigBean configBean) {
		super(configBean);
	}

	@Override
	public void pkgMsgBody(imix.imix10.Message message, TradeConfirmBean bean)throws Exception {
		SendForeignForwardSpotModel model = (SendForeignForwardSpotModel)bean;
		// 成交编号， 需求未约束编码规则
		message.setField(new ExecID(model.getExecID())); 
		// 成交日期，格式为：YYYYMMDD
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>", "")));
		// 确认报文 ID
		message.setField(new ConfirmID(model.getConfirmID()));
		// 2 交易场所， 默认为CFETS
		message.setField(new MarketID("2"));
		// 12 交易类型 （市场标识） ， 14-外汇远期 //12-外汇即期 25-贵金属即期 26-贵金属远期
		message.setField(new MarketIndicator(model.getMarketIndicator()));														
		// 起息日，格式为：YYYYMMDD
		message.setField(new SettlDate(model.getSettlDate().replace("<Date>", "")));
		// 需求规12CFETS/K0570—2015格说明书没有定义该字段精度、
		// 成交价格，
		if(model.getLastPx() != null){
			message.setField(new LastPx(BigDecimalUtil.decimalFormat(model.getLastPx(), "0.000000")));// 成交价格，
		}
		
		
		// 卖出金额，
		message.setField(new CalculatedCcyLastQty(BigDecimalUtil.decimalFormat(model.getCalculatedCcyLastQty(),"0.00")));
		
		if (MarketIndicator.GOLDSPT.equals(model.getMarketIndicator()) || MarketIndicator.GOLDFWD.equals(model.getMarketIndicator())) {//贵金属即远期
			message.setField(new SecurityID(model.getSecurityID()));
			if ("1".equals(model.getSide())) {
				message.setField(new Side('1'));
			}else if ("4".equals(model.getSide())) {
				message.setField(new Side('4'));
			}
			message.setField(new Currency(model.getCurrency()));
			// 买入金额， 需求规格说明书未定义该字段精度、 单位
			message.setField(new LastQty(BigDecimalUtil.decimalFormat(model.getLastQty(),"0.00")));
			pkgGoldSettlInfo(message, model);
		}else {//即远期
			if ("2".equals(model.getNetGrossInd())) {
				message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
			} else {
				message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
			}
			// 买入币种  必填需匹配
			message.setField(new Currency1(model.getCurrency1()));
			// 卖出币种
			message.setField(new Currency2(model.getCurrency2()));
			// 买入金额， 需求规格说明书未定义该字段精度、 单位
			message.setField(new LastQty(BigDecimalUtil.decimalFormat(model.getLastQty(),"0.00")));
			//交收方式
			if(StringUtils.isNotEmpty(model.getDeliveryType())){
				message.setField(new DeliveryType(Integer.valueOf(model.getDeliveryType())));
			}
			pkgSettlInfo(message, bean.getTradeSettlsBean(),bean.getNetGrossInd(),model.getDeliveryType());
		}
		
		
	}
	
	public void pkgSettlInfo(Message message,TradeSettlsBean bean,String netGrossInd,String deliveryType)throws Exception{
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyRole(PartyRole.EXECUTING_FIRM));
		// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		own.set(new PartyID(getConfigBean().getBankId()));
		
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		cust.set(new PartyRole(PartyRole.CONTRA_FIRM));
		cust.set(new PartyID(bean.getcInstitutionId()));
		
		// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getFullName());
		
		// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getcFullName());
		
		//如果交易方式为双边清算 (NetGrossInd=2)  则为双方传入账户信息  否则不需要传入这些域
		if("2".equals(netGrossInd)||!"12".equals(deliveryType)){
			//组装本方清算信息
			pkgSettls(own, bean.getSettccy(), bean.getBankname(), bean.getBankopenno(), bean.getBankacctno(), 
					bean.getAcctname(), bean.getAcctopenno(), bean.getAcctno(), 
					bean.getIntermediarybankname(), bean.getIntermediarybankbiccode(), bean.getIntermediarybankacctno(), bean.getRemark());
			
			//组装对手方清算信息
			pkgSettls(cust, bean.getcSettccy(), bean.getcBankname(), bean.getcBankopenno(), bean.getcBankacctno(), 
					bean.getcAcctname(), bean.getcAcctopenno(), bean.getcAcctno(), 
					bean.getcIntermediarybankname(), bean.getcIntermediarybankbiccode(), bean.getcIntermediarybankacctno(), bean.getcRemark());
		}
		
		message.addGroup(own);
		message.addGroup(cust);
	}
	
	public void pkgSettls(ExecutionReport.NoPartyIDs iDs,String settccy,String bankname,String bankopenno,String bankacctno,
			String acctname,String acctopenno,String acctno,
			String intermediarybankname,String intermediarybankbiccode,String intermediarybankacctno,
			String remark){
		if("CNY".equals(settccy)){
			// 资金开户行 该字段必填 需要匹配 110
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME, bankname);
			// 资金账户户名 该字段必填需匹配 23
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME, acctname);
			// 本方系统行号  该字段必填需匹配 16
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE, bankacctno);
			//本方资金账号 必填需匹配 15
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER, acctno);
			// 本方附言   选填   若填值则需要匹配 139
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, remark);
			
		}else{
			// 本方开户行名称  选填 110
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME, bankname);
			// 本方收款行名称 23
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME, acctname);
			// 本方收款行BICCODE 16
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE, acctopenno);
			// 收款行账号 15
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER, acctno);
			// 本方附言   选填   若填值则需要匹配 139
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, remark);
			
			// 中间行名称 209
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK, intermediarybankname);
			// 中间行BICCODE 211
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK_BIC_CODE, intermediarybankbiccode);
			// 中间行账号 213
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK_ACCOUNT, intermediarybankacctno);
			
			// 资金开户行账号 207
			convert(iDs,PartySubIDType.CORRESPONDENT_BANK_ACCOUNT, bankacctno);
			// 开户行BICCODE 138
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_NAME, bankopenno);
		}
	}
	
	public void pkgGoldSettlInfo(Message message,SendForeignForwardSpotModel bean)throws Exception{
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyRole(PartyRole.EXECUTING_FIRM));
		// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		own.set(new PartyID(getConfigBean().getBankId()));
		
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		cust.set(new PartyRole(PartyRole.CONTRA_FIRM));
		cust.set(new PartyID(bean.getPartyID_2()));
		
		// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getOwnName());
		
		// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getPartyName());
		
		message.addGroup(own);
		message.addGroup(cust);
	}
	
	/**
	 * 拼装上送IMIX消息报文
	 */
	public Message packageMsg(SendForeignForwardSpotModel model, ConfigBean configBean) throws ConfigError {
//		if (!model.getExecID().equals("FFC9-00DF-0003-0D")) {
//			return null;
//		}
		Message message = MsgHeadAndStandardTrailer.packageMsgHeadAndStandardTrailer("AK", configBean);
		
		message.setField(new ExecID(model.getExecID())); // 成交编号， 需求未约束编码规则
		message.setField(new TradeDate(model.getTradeDate().replace("<Date>", "")));// 成交日期，格式为：YYYYMMDD
		
		message.setField(new ConfirmID(model.getConfirmID()));// 确认报文 ID
		
		message.setField(new MarketID("2"));// 2 交易场所， 默认为CFETS
		if ("2".equals(model.getNetGrossInd())) {
			message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		} else {
			message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算

		}
		message.setField(new MarketIndicator(model.getMarketIndicator()));// 12 交易类型 （市场标识） ， 14-外汇远期
																		//12-外汇即期
		
		message.setField(new SettlDate(model.getSettlDate().replace("<Date>", "")));// 起息日，格式为：YYYYMMDD
		
		if(model.getLastPx() != null){
			message.setField(new LastPx(BigDecimalUtil.decimalFormat(model.getLastPx(), "0.000000")));// 成交价格，
			// 需求规12CFETS/K0570—2015格说明书没有定义该字段精度、
			// 单位
		}

		
		message.setField(new Currency1(model.getCurrency1()));// 买入币种  必填需匹配
		
		message.setField(new LastQty(BigDecimalUtil.decimalFormat(model.getLastQty(),"0.00")));// 买入金额， 需求规格说明书未定义该字段精度、 单位
		
		message.setField(new Currency2(model.getCurrency2()));// 卖出币种
		
		message.setField(new CalculatedCcyLastQty(BigDecimalUtil.decimalFormat(model.getCalculatedCcyLastQty(),"0.00")));// 卖出金额，
																// 需求规格说明书未定义该字段精度、
																// 单位
	
		
		ExecutionReport.NoPartyIDs ownnopartyids1 = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs ownnopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		
		ownnopartyids1.set(new PartyRole(1));
		ownnopartyids1.set(new PartyID(configBean.getBankId()));// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		
		ExecutionReport.NoPartyIDs othnopartyids1 = new ExecutionReport.NoPartyIDs();
		ExecutionReport.NoPartyIDs.NoPartySubIDs othnopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
		
		othnopartyids1.set(new PartyID(model.getPartyID_2()));// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		othnopartyids1.set(new PartyRole(17));
		
		List<AgencyBasicInformationModel> list1 = model.getList1();
		if (  StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "124"))) {
			ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "124")));// 中文全称， 如果机构中文全称不传输， 此组域不传输
			ownnopartysubids1.set(new PartySubIDType(124));
			ownnopartyids1.addGroup(ownnopartysubids1);
			
		}
		//获取得到对手方信息的集合
		List<AgencyBasicInformationModel> list2 = model.getList2();
		if (  StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "124"))) {
			othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "124")));// 中文全称， 如果机构中文全称不传输， 此组域不传输
			othnopartysubids1.set(new PartySubIDType(124));
			othnopartyids1.addGroup(othnopartysubids1);
			
		}
		//如果交易方式为双边清算 (NetGrossInd=2)  则为双方传入账户信息  否则不需要传入这些域
		if("2".equals(model.getNetGrossInd())){
			
//			ownnopartysubids1.set(new PartySubID(configBean.getBankName()));// 中文全称，中文全称， 如果机构中文全称不传输， 此组域不传输
//			ownnopartysubids1.set(new PartySubIDType(124));
//			ownnopartyids1.addGroup(ownnopartysubids1);
			
			//买入币种 Currency1
			String boughtCcy = model.getCurrency1();
			//卖出币种 Currency2
			String soldCcy = model.getCurrency2();
				
			if ("CNY".equals(boughtCcy)) {
				
				
				if ((StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "110")))){
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "110")));// 资金开户行 该字段必填 需要匹配
					ownnopartysubids1.set(new PartySubIDType(110));
					ownnopartyids1.addGroup(ownnopartysubids1);
				} else {
//					ownnopartysubids1.set(new PartySubID("-"));
//					ownnopartysubids1.set(new PartySubIDType(110));
//					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				
				if(	StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "23"))){
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "23")));// 资金账户户名 该字段必填需匹配
					ownnopartysubids1.set(new PartySubIDType(23));
					ownnopartyids1.addGroup(ownnopartysubids1);
				} else {
//					ownnopartysubids1.set(new PartySubID("-"));
//					ownnopartysubids1.set(new PartySubIDType(23));
//					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "16"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "16")));// 本方系统行号  该字段必填需匹配
					ownnopartysubids1.set(new PartySubIDType(16));
					ownnopartyids1.addGroup(ownnopartysubids1);
				} else {
//					ownnopartysubids1.set(new PartySubID("-"));
//					ownnopartysubids1.set(new PartySubIDType(16));
//					ownnopartyids1.addGroup(ownnopartysubids1);
				}
			
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "15"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "15"))); //本方资金账号 必填需匹配
					ownnopartysubids1.set(new PartySubIDType(15));
					ownnopartyids1.addGroup(ownnopartysubids1);
				} else {
//					ownnopartysubids1.set(new PartySubID("-"));
//					ownnopartysubids1.set(new PartySubIDType(15));
//					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "139"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "139")));// 本方附言   选填   若填值则需要匹配
					ownnopartysubids1.set(new PartySubIDType(139));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
			}else {		
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "110"))) {
//					ownnopartysubids1.set(new PartySubID(prop.getProperties(ownSSI+".OurSsi.BeneAccount")));// 本方开户行名称  选填
//					ownnopartysubids1.set(new PartySubID("-"));// 开户行账号
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "110")));
					ownnopartysubids1.set(new PartySubIDType(110));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "23"))) {
//					ownnopartysubids1.set(new PartySubID(prop.getProperties(ownSSI+".OurSsi.BeneName1")));// 本方收款行名称
//					ownnopartysubids1.set(new PartySubID("-"));// 本方收款行名称
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "23")));
					ownnopartysubids1.set(new PartySubIDType(23));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				

				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "16"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "16")));// 本方收款行BICCODE
					ownnopartysubids1.set(new PartySubIDType(16));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "15"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "15")));// // 收款行账号
					ownnopartysubids1.set(new PartySubIDType(15));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "139"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "139")));// 本方附言   选填   若填值则需要匹配
					ownnopartysubids1.set(new PartySubIDType(139));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "139"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "209")));// 中间行名称
					ownnopartysubids1.set(new PartySubIDType(209));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "211"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "211")));// 中间行BICCODE
					ownnopartysubids1.set(new PartySubIDType(211));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "213"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "213")));// 中间行账号
					ownnopartysubids1.set(new PartySubIDType(213));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list1, "207"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "207")));// // 资金开户行账号
					ownnopartysubids1.set(new PartySubIDType(207));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank( AgencyBasicInformationUtil.getPartySubID(list1, "138"))) {
					ownnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list1, "138")));// // 对方开户行BICCODE
					ownnopartysubids1.set(new PartySubIDType(138));
					ownnopartyids1.addGroup(ownnopartysubids1);
				}
				
			}
			
			
			
			if ("CNY".equals(soldCcy)) {
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "110"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "110")));// 资金开户行 必填
					othnopartysubids1.set(new PartySubIDType(110));
					othnopartyids1.addGroup(othnopartysubids1);
				} else {
//					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "-")));
				}
				
				
				if ( StringUtils.isNotBlank( AgencyBasicInformationUtil.getPartySubID(list2, "23"))	) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "23")));//对方 资金账户户名 必填
					othnopartysubids1.set(new PartySubIDType(23));
					othnopartyids1.addGroup(othnopartysubids1);
				} else {
//					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "-")));
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "16"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "16")));//对方支付系统账号 必填
					othnopartysubids1.set(new PartySubIDType(16));
					othnopartyids1.addGroup(othnopartysubids1);
				} else {
//					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "-")));
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "15"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "15")));//对方资金账号 必填
					othnopartysubids1.set(new PartySubIDType(15));
					othnopartyids1.addGroup(othnopartysubids1);
				} else {
//					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "-")));
				}
				
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "139"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "139")));//附言 选填
					othnopartysubids1.set(new PartySubIDType(139));
					othnopartyids1.addGroup(othnopartysubids1);
				} 
			}else {
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "138"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "138")));// 开户行 BICCODE开户行名称
					othnopartysubids1.set(new PartySubIDType(138));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "110"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "110")));//  资金开户行
					othnopartysubids1.set(new PartySubIDType(110));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "23"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "23")));//  资金开户行
					othnopartysubids1.set(new PartySubIDType(23));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "16"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "16"))); //对方收款行BICCODE
					othnopartysubids1.set(new PartySubIDType(16));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "15"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "15"))); //对方收款行账号
					othnopartysubids1.set(new PartySubIDType(15));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "139"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "139"))); //对手方附言
					othnopartysubids1.set(new PartySubIDType(139));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				if (StringUtils.isNotBlank( AgencyBasicInformationUtil.getPartySubID(list2, "209"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "209")));//对手方中间行名称
					othnopartysubids1.set(new PartySubIDType(209));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				
				if (StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "211"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "211")));//对方中间行BICCODE
					othnopartysubids1.set(new PartySubIDType(211));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				if ( StringUtils.isNotBlank( AgencyBasicInformationUtil.getPartySubID(list2, "213"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "213"))); //对手方中间行账号
					othnopartysubids1.set(new PartySubIDType(213));
					othnopartyids1.addGroup(othnopartysubids1);
				}
				
				
				if ( StringUtils.isNotBlank(AgencyBasicInformationUtil.getPartySubID(list2, "207"))) {
					othnopartysubids1.set(new PartySubID(AgencyBasicInformationUtil.getPartySubID(list2, "207")));//对手方开户行账号
					othnopartysubids1.set(new PartySubIDType(207));
					othnopartyids1.addGroup(othnopartysubids1);
				}		
			}
			
		}
		message.addGroup(ownnopartyids1);
		message.addGroup(othnopartyids1);		
		
		System.out.println(message);
		return message;
	}

	@Override
	public void pkgMsgBody(imix.imix10.Message message,
			List<SendForeignOptionModel> beans) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
