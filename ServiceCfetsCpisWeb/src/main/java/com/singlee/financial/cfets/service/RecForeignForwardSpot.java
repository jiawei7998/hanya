package com.singlee.financial.cfets.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecForeignForwardSpotModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.CalculatedCcyLastQty;
import imix.field.CnfmTime;
import imix.field.CnfmTimeType;
import imix.field.ConfirmID;
import imix.field.ConfirmStatus;
import imix.field.Currency;
import imix.field.Currency1;
import imix.field.Currency2;
import imix.field.DealTransType;
import imix.field.ExecID;
import imix.field.ExecType;
import imix.field.LastPx;
import imix.field.LastQty;
import imix.field.MarketIndicator;
import imix.field.NetGrossInd;
import imix.field.NoPartyIDs;
import imix.field.NoPartySubIDs;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.SecurityID;
import imix.field.SettlDate;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;


/**
 * 
 * <P>RecForeignForward.java</P>
 * <P>接收外币远期消息类</P>
 * <P>Company: 新利</P>
 * <P>          修改者姓名 修改内容说明</P>
 * @author pengqian
 * @see     参考类1
 */
public class RecForeignForwardSpot {
	
	/**
	 * 解析报文
	 * @param message
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	private static Logger LogManager = LoggerFactory.getLogger(RecForeignForwardSpot.class);
	public RecForeignForwardSpotModel ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		MessageUtil msgUtil = new MessageUtil(message);
		
		RecForeignForwardSpotModel model = new RecForeignForwardSpotModel();
		RecPageInfoModel pageInfo = new RecPageInfoModel();
		
		try {
			 String execID  = msgUtil.getStringValue(ExecID.FIELD);
			 String tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);
			 String netGrossInd  = msgUtil.getStringValue(NetGrossInd.FIELD);
			 String marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
			 String execType  = msgUtil.getStringValue(ExecType.FIELD);
			 String dealTransType  = msgUtil.getStringValue(DealTransType.FIELD);
			 String confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
			 String confirmStatus  = msgUtil.getStringValue(ConfirmStatus.FIELD);
			
			
			if ("".equals(execID)) {
				LogManager.info("消息未收到域：ExecID "+ExecID.FIELD);
			}	
			if ("".equals(tradeDate)) {
				LogManager.info("消息未收到域：TradeDate "+TradeDate.FIELD);
			}	
			if ("".equals(netGrossInd)) {
				LogManager.info("消息未收到域：NetGrossInd "+NetGrossInd.FIELD);
			}			
			if ("".equals(marketIndicator)) {
				LogManager.info("消息未收到域：MarketIndicator "+MarketIndicator.FIELD);
			}			
			if ("".equals(execType)) {
				LogManager.info("消息未收到域：ExecType "+ExecType.FIELD);
			}			
			if ("".equals(dealTransType)) {
				LogManager.info("消息未收到域：DealTransType "+DealTransType.FIELD);
			}		
			if ("".equals(confirmID)) {
				LogManager.info("消息未收到域：ConfirmID "+ConfirmID.FIELD);
			}			
			if ("".equals(confirmStatus)) {
				LogManager.info("消息未收到域：ConfirmStatus "+ConfirmStatus.FIELD);
			}
			
			NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
			
			CnfmTimeType cnfmTimeType = new CnfmTimeType();
			CnfmTime cnfmTime = new CnfmTime();
			
			String ownCnfmTime = "";
			String othCnfmTime = "";
				
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
				message.getGroup(i, noCnfmTimes);
				cnfmTimeType = noCnfmTimes.getCnfmTimeType();
				cnfmTime = noCnfmTimes.getCnfmTime();
				if (cnfmTimeType.getValue() == 1) {
					ownCnfmTime = cnfmTime.getValue();
				}else if (cnfmTimeType.getValue() == 2) {
					othCnfmTime = cnfmTime.getValue();
				}
				
			}
			String settlDate = "";
			settlDate  = msgUtil.getStringValue(SettlDate.FIELD);				
			String lastPx = "";
			lastPx  = msgUtil.getStringValue(LastPx.FIELD);		
			String currency1 = "";
			String currency2 = "";
			String securityID="";
			if("12".equals(msgUtil.getStringValue(MarketIndicator.FIELD))||
			   "14".equals(msgUtil.getStringValue(MarketIndicator.FIELD))){
				currency1  = msgUtil.getStringValue(Currency1.FIELD);				
				currency2  = msgUtil.getStringValue(Currency2.FIELD);				
			}else if("25".equals(msgUtil.getStringValue(MarketIndicator.FIELD))||
					 "26".equals(msgUtil.getStringValue(MarketIndicator.FIELD))){
				currency1  = msgUtil.getStringValue(Currency.FIELD);				
				currency2  = "CNY";		
				securityID=msgUtil.getStringValue(SecurityID.FIELD);	
			} 
			String lastQty = "";
			lastQty  = msgUtil.getStringValue(LastQty.FIELD);				
			String calculatedCcyLastQty = "";
			calculatedCcyLastQty  = msgUtil.getStringValue(CalculatedCcyLastQty.FIELD);				
			
			
			String ownPartyID = ""; //本方机构 21 位码
			
			String othPartyID = "";//对手方机构 21 位码
			
			String ownCnName = ""; //中文全称
			String ownCapitalBakeName = ""; //资金开户行
			String ownCapitalAccountName = ""; //资金账户户名
			String ownPayBankNo = "";//支付系统行号
			String ownCapitalAccountBIC = ""; //开户行 BICCODE
			String ownCapitalAccount = ""; //资金账号
			String ownRemark = ""; //附言
			
			String midCnName = ""; //中间行名称
			String midBankBIC = ""; //中间行 BICCODE
			String midBankAccount = ""; //中间行账号
			
			String ownOpenAccount = ""; //开户行账号

			String othCnName = ""; //中文全称
			String othCapitalBankName = ""; //资金开户行
			String othCapitalAccountName = ""; //资金账户户名
			String othCapitalAccount = ""; //资金账号
			String othPayBankNo = "";//支付系统行号
			String othRemark = ""; //附言

			int partyrole=-1; //本方对手方
			
			String partyid="";//21位机构码
			
			String partysubid="";
		    
		    int partysubidtype=-1;
			
			ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
			ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			int nopartysubids=-1;//清算信息循环组
			
			//开始循环清算信息
			for (int k = 1; k <= msgUtil.getIntValue(NoPartyIDs.FIELD); k++) {
				message.getGroup(k, nopartyids1);

				partyrole=msgUtil.getIntValue(nopartyids1, PartyRole.FIELD);

				partyid=msgUtil.getStringValue(nopartyids1, PartyID.FIELD);
				
				if(partyrole==1){
					ownPartyID = partyid;
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					
					//循环获取清算账户信息
					for (int g = 1; g <= nopartysubids; g++) {
						
						nopartyids1.getGroup(g, nopartysubids1);
						
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						
						if (partysubidtype == 124) {
							ownCnName =  partysubid;
						 
						}else if (partysubidtype == 110) {
							ownCapitalBakeName =  partysubid;
						 
						}else if (partysubidtype == 23) {
							ownCapitalAccountName =  partysubid;
						 
						}else if (partysubidtype == 16) {
							ownPayBankNo =  partysubid;
						 
						}else if (partysubidtype == 15) {
							ownCapitalAccount =  partysubid;
						 
						}else if (partysubidtype == 139) {
							ownRemark =  partysubid;
						 
						}else if (partysubidtype == 209) {
							midCnName =  partysubid;
						 
						}else if (partysubidtype == 211) {
							midBankBIC =  partysubid;
						 
						}else if (partysubidtype == 138) {
							ownCapitalAccountBIC =  partysubid;
						 
						}else if (partysubidtype == 213) {
							midBankAccount =  partysubid;
						 
						}else if (partysubidtype == 207) {
							ownOpenAccount =  partysubid;
						}
					}
				}else if (partyrole==17) {
					othPartyID = partyid;
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);

					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						
						if (partysubidtype == 124) {
							othCnName =  partysubid;
						 
						}else if (partysubidtype == 110) {
							othCapitalBankName =  partysubid;
						 
						}else if (partysubidtype == 23) {
							othCapitalAccountName =  partysubid;
						 
						}else if (partysubidtype == 16) {
							othPayBankNo =  partysubid;
						 
						}else if (partysubidtype == 15) {
							othCapitalAccount =  partysubid;
						 
						}else if (partysubidtype == 139) {
							othRemark =  partysubid;
						}
					}
				
				}
			}
			
			model.setExecID(execID);
			model.setTradeDate(tradeDate);
			model.setNetGrossInd(netGrossInd);
			model.setMarketIndicator(marketIndicator);
			model.setExecType(execType);
			model.setDealTransType(dealTransType);
			model.setConfirmID(confirmID);
			model.setConfirmStatus(confirmStatus);
			model.setOwnCnfmTime(ownCnfmTime);
			model.setOthCnfmTime(othCnfmTime);
			model.setSettlDate(settlDate);
			model.setLastPx(lastPx);
			model.setCurrency1(currency1);
			model.setLastQty(lastQty);
			model.setCurrency2(currency2);
			model.setCalculatedCcyLastQty(calculatedCcyLastQty);	
			model.setOwnPartyID(ownPartyID);
			model.setOwnCnName(ownCnName);
			model.setOwnCapitalBakeName(ownCapitalBakeName);
			model.setOwnCapitalAccountName(ownCapitalAccountName);
			model.setOwnPayBankNo(ownPayBankNo);
			model.setOwnCapitalAccount(ownCapitalAccount);
			model.setOwnRemark(ownRemark);
			model.setMidCnName(midCnName);
			model.setMidBankBIC(midBankBIC);
			model.setOwnCapitalAccountBIC(ownCapitalAccountBIC);
			model.setMidBankAccount(midBankAccount);
			model.setOwnOpenAccount(ownOpenAccount);
			model.setOthPartyID(othPartyID);
			model.setOthCnName(othCnName);
			model.setOthCapitalBankName(othCapitalBankName);
			model.setOthCapitalAccountName(othCapitalAccountName);
			model.setOthPayBankNo(othPayBankNo);
			model.setOthCapitalAccount(othCapitalAccount);
			model.setOthRemark(othRemark);
			model.setSecurityID(securityID);
			
			//分页信息
			pageInfo = RecPageInfo.msgPageToModel(message);
			model.setPageInfo(pageInfo);
			
		} catch (Exception e) {
			LogManager.info("解析报文出错"+e);
			throw e;
		}
		return model;
	}
	
	public RecForeignForwardSpotModel ExecutionReportCancel(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		MessageUtil msgUtil = new MessageUtil(message);
		RecForeignForwardSpotModel model = new RecForeignForwardSpotModel();
		try {
			String execID  = msgUtil.getStringValue(ExecID.FIELD);
			String tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);
			String marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
			String execType  = msgUtil.getStringValue(ExecType.FIELD);
			String confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
			
			
			if ("".equals(execID)) {
				LogManager.info("消息未收到域：ExecID "+ExecID.FIELD);
			}	
			if ("".equals(tradeDate)) {
				LogManager.info("消息未收到域：TradeDate "+TradeDate.FIELD);
			}	
			if ("".equals(marketIndicator)) {
				LogManager.info("消息未收到域：MarketIndicator "+MarketIndicator.FIELD);
			}			
			if ("".equals(execType)) {
				LogManager.info("消息未收到域：ExecType "+ExecType.FIELD);
			}			
			if ("".equals(confirmID)) {
				LogManager.info("消息未收到域：ConfirmID "+ConfirmID.FIELD);
			}			
			
			model.setExecID(execID);
			model.setTradeDate(tradeDate);
			model.setMarketIndicator(marketIndicator);
			model.setExecType(execType);
			model.setConfirmID(confirmID);
			
			RecPageInfoModel pageInfo = new RecPageInfoModel();
			//分页信息
			pageInfo = RecPageInfo.msgPageToModel(message);
			model.setPageInfo(pageInfo);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return model;
		
	}
	
	


}
