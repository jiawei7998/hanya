package com.singlee.financial.cfets.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.ConfirmID;
import imix.field.FieldMatchResult;
import imix.field.MatchMsgPartyID;
import imix.field.MatchMsgPartyRole;
import imix.field.MatchMsgSubID;
import imix.field.MatchMsgSubType;
import imix.field.MatchMsgType;
import imix.field.MatchStatus;
import imix.field.MsgType;
import imix.field.QueryRequestID;
import imix.field.ReceivedFieldID;
import imix.field.ReceivedFieldValue;
import imix.field.SendFieldID;
import imix.field.SendFieldValue;
import imix.field.Text;
import imix.field.TransactTime;
import imix.imix10.QueryResult.NoMatchFieldDetails;
import imix.imix10.component.MatchMsgDetails.NoMatchMsgDetailIDs;
import imix.imix10.component.MatchMsgDetails.NoMatchMsgDetailIDs.NoMatchMsgDetailSubIDs;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.MatchFieldDetails;
import com.singlee.financial.cfets.bean.RecMatchingModel;

/**
 * 
 * <P>RecDueNotConfirm.java</P>
 * <P>接收到匹配返回报文 IMIX 报文标准</P>
 * @author lisc
 * <P>Company:新利</P>
 */

public class RecMatching {
	
	private static Logger LogManager = LoggerFactory.getLogger(RecMatching.class);

	public RecMatchingModel recMatchingReport(Message message) throws FieldNotFound, Exception, UnsupportedMessageType,IncorrectTagValue {
		
			RecMatchingModel model = new RecMatchingModel();
			
			List<MatchFieldDetails> list=new ArrayList<MatchFieldDetails>();
			
			MessageUtil msgUtil = new MessageUtil(message);
			
			String msgtype=msgUtil.getHeadStringValue(MsgType.FIELD);
			
			model.setMsgtype(msgtype);
			
			String matchStatus=msgUtil.getStringValue(MatchStatus.FIELD);//匹配状态
			
			model.setMatchStatus(matchStatus);
			
			String transactTime=msgUtil.getStringValue(TransactTime.FIELD);//匹配时间
			
			model.setTransacTime(transactTime);
			//重复组数据
			MatchFieldDetails matchFieldDetails=null;
		try {
			if(MsgType.QUERYRESULT.equals(msgtype)){
				String queryRequestId=msgUtil.getStringValue(QueryRequestID.FIELD);//查询编号
				model.setQueryRequestId(queryRequestId);
				String text=msgUtil.getStringValue(Text.FIELD);
				model.setText(text);
				
				if("".equals(queryRequestId)){
					LogManager.info("消息未收到域：queryRequestId "+QueryRequestID.FIELD);
				}
				if("".equals(matchStatus)){
					LogManager.info("消息未收到域：matchStatus "+MatchStatus.FIELD);
				}
				
				
				//查询swift报文反馈的消息
				NoMatchFieldDetails noDetail=new NoMatchFieldDetails();
				
				String sendFieldID="";
				
				String sendFieldValue="";
				
				String receivedFieldID="";
				
				String receivedFieldValue="";
				
				String fieldMatchResult="";//匹配结果
				
				for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoMatchFieldDetails.FIELD); i++) {
					matchFieldDetails=new MatchFieldDetails();
					
					message.getGroup(i, noDetail);
					
					 sendFieldID = getStringValue(noDetail, SendFieldID.FIELD);
					
					matchFieldDetails.setSendFieldID(sendFieldID);
					
					 sendFieldValue = getStringValue(noDetail, SendFieldValue.FIELD);
					
					matchFieldDetails.setSendFieldValue(sendFieldValue);
					if(!"3".equals(matchStatus)){
						 receivedFieldID=getStringValue(noDetail, ReceivedFieldID.FIELD);
						
						 matchFieldDetails.setReceivedFieldID(receivedFieldID);
						
						 receivedFieldValue=getStringValue(noDetail, ReceivedFieldValue.FIELD);
						
						 matchFieldDetails.setReceivedFieldValue(receivedFieldValue);
						
						 fieldMatchResult=getStringValue(noDetail, FieldMatchResult.FIELD);
						
						 matchFieldDetails.setFieldMatchResult(Integer.valueOf(fieldMatchResult));
					}
					LogManager.info("sendFieldID:"+sendFieldID+"|"+
							"sendFieldValue:"+sendFieldValue+"|"+
							"receivedFieldID:"+receivedFieldID+"|"+
							"receivedFieldValue:"+receivedFieldValue+"|"+
							"fieldMatchResult:"+fieldMatchResult+"|"+
							"matchFieldDetails:"+matchFieldDetails);
					list.add(matchFieldDetails);
					model.setList(list);
				}
			}else if(MsgType.CONFIRMATION_ACK.equals(msgtype)){
				//报文类型
				String matchMsgType=msgUtil.getStringValue(MatchMsgType.FIELD);
				model.setMatchMsgType(matchMsgType);
				//报文编号
				String confirmID=msgUtil.getStringValue(ConfirmID.FIELD);
				model.setConfirmID(confirmID);	
				
				if("".equals(matchMsgType)){
					LogManager.info("消息未收到域：matchMsgType "+MatchMsgType.FIELD);
				}
				if("".equals(confirmID)){
					LogManager.info("消息未收到域：ConfirmID "+ConfirmID.FIELD);
				}
				if("".equals(matchStatus)){
					LogManager.info("消息未收到域：matchStatus "+MatchStatus.FIELD);
				}
				
				//解析主动推送的消息
//				MatchMsgPartyID matchMsgPartyid=new MatchMsgPartyID();
//				MatchMsgPartyRole matchMsgPartyRole=new MatchMsgPartyRole();
//				MatchMsgSubType matchMsgSubType=new MatchMsgSubType();
//				MatchMsgSubID matchMsgSubID=new MatchMsgSubID();
				
				String matchMsgPartyid="";
				String matchMsgPartyRole="";
				String matchMsgSubType="";
				String matchMsgSubID="";
				
				NoMatchMsgDetailIDs noMsgDetailIds=new NoMatchMsgDetailIDs();
				NoMatchMsgDetailSubIDs noMsgDetailSubIds=new NoMatchMsgDetailSubIDs();
//				imix.field.NoMatchMsgDetailSubIDs msgdetaIDs=new imix.field.NoMatchMsgDetailSubIDs();
				int msgdetaIDs=-1;
				
				String matchMsgPartyid_1="";//1-发送
				String matchMsgpartyRole_1="";
				String matchMsgPartyid_2="";//2-接收
				String matchMsgpartyRole_2="";
				String matchMsgSubid_1="";//本方发送报文的Sender's reference
				String matchMsgSubid_2="";//本方接收报文的Sender's reference
				String matchMsgSubid_3="";//本方发送报文的related reference
				String matchMsgSubid_4="";//本方接收报文的related reference
				String matchMsgSubid_5="";//本方发送报文的tytp of Operation
				String matchMsgSubid_6="";//本方接收报文的tytp of Operation
				
				for(int k = 1; k <= msgUtil.getIntValue(imix.field.NoMatchMsgDetailIDs.FIELD); k++){
					message.getGroup(k, noMsgDetailIds);
					
					matchMsgPartyRole=getStringValue(noMsgDetailIds, MatchMsgPartyRole.FIELD);
					
					matchMsgPartyid=getStringValue(noMsgDetailIds, MatchMsgPartyID.FIELD);
				
					if(Integer.parseInt(matchMsgPartyRole)==1){
						
						model.setMatchMsgPartyid_1(matchMsgPartyid_1);
						
						model.setMatchMsgpartyRole_1(matchMsgPartyRole);
						
						msgdetaIDs=getIntValue(noMsgDetailIds,imix.field.NoMatchMsgDetailSubIDs.FIELD );
						
						for(int g=1;g<=msgdetaIDs;g++){
							noMsgDetailIds.getGroup(g, noMsgDetailSubIds);
							
							matchMsgSubID=getStringValue(noMsgDetailSubIds, MatchMsgSubID.FIELD);
							
							matchMsgSubType=getStringValue(noMsgDetailSubIds, MatchMsgSubType.FIELD);
							
							if(Integer.parseInt(matchMsgSubType)==1){
								
								model.setMatchMsgSubid_1(matchMsgSubID);
							}else if(Integer.parseInt(matchMsgSubType)==3){
								model.setMatchMsgSubid_3(matchMsgSubID);
							}else if (Integer.parseInt(matchMsgSubType)==5){
								model.setMatchMsgSubid_5(matchMsgSubID);
							}
						}
					}else if(Integer.parseInt(matchMsgPartyRole)==2){
						model.setMatchMsgPartyid_2(matchMsgPartyid_2);
						
						model.setMatchMsgpartyRole_2(matchMsgPartyRole);
						
						msgdetaIDs=getIntValue(noMsgDetailIds,imix.field.NoMatchMsgDetailSubIDs.FIELD );
						for(int g=1;g<=msgdetaIDs;g++){
							noMsgDetailIds.getGroup(g, noMsgDetailSubIds);
							
							matchMsgSubID=getStringValue(noMsgDetailSubIds, MatchMsgSubID.FIELD);
							
							matchMsgSubType=getStringValue(noMsgDetailSubIds, MatchMsgSubType.FIELD);
							if(Integer.parseInt(matchMsgSubType)==2){
								
								model.setMatchMsgSubid_2(matchMsgSubID);
							}else if(Integer.parseInt(matchMsgSubType)==4){

								model.setMatchMsgSubid_4(matchMsgSubID);
							}else if (Integer.parseInt(matchMsgSubType)==6){
								model.setMatchMsgSubid_6(matchMsgSubID);
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LogManager.info("解析报文出错："+e);
			throw e;
		}
		return model;	
		
	}
	
	private String getStringValue(NoMatchFieldDetails noDetail, int field) throws Exception{
		return noDetail.isSetField(field) ? noDetail.getString(field) : "";
	}
	
	private String getStringValue(NoMatchMsgDetailIDs noMsgDetailIds, int field) throws Exception{
		return noMsgDetailIds.isSetField(field) ? noMsgDetailIds.getString(field) : "";
	}
	
	private int getIntValue(NoMatchMsgDetailIDs noMsgDetailIds, int field) throws Exception{
		return noMsgDetailIds.isSetField(field) ? noMsgDetailIds.getInt(field) : 0;
	}
	
	private String getStringValue(NoMatchMsgDetailSubIDs noMsgDetailSubIds, int field) throws Exception{
		return noMsgDetailSubIds.isSetField(field) ? noMsgDetailSubIds.getString(field) : "";
	}

}
