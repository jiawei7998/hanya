package com.singlee.financial.cfets.cpis;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.singlee.financial.cfets.service.SendQuery;
import com.singlee.financial.model.MatchingModel;
import com.singlee.financial.model.QueryModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import imix.Message;
import imix.client.core.ImixSession;

@Service
public class ImixCPISManageServer implements IImixCPISManageServer {

	private static Logger LogManager = LoggerFactory.getLogger(ImixCPISManageServer.class);

	/**
	 * CFETS配置信息
	 */
	@Autowired
	private ImixCPISClient imixCPISClient;
	
	private SendQuery sendFXQuery = new SendQuery();
	

	@Override
	public boolean startImixSession() {
		// 是否登录
		boolean isLogin = false;
		try {
			isLogin = imixCPISClient.getImixCPISAutoConn().startConn();
		} catch (Exception e) {
			LogManager.error("连接出错:"+e.getMessage(),e);
			e.printStackTrace();
		}
		return isLogin;
	}

	@Override
	public boolean stopImixSession() {
		imixCPISClient.getImixCPISAutoConn().setStopImixSessionBySystem(true);
		// 是否登录
		boolean isLogin = false;
		ImixSession imixSession = null;
		imixSession = imixCPISClient.getImixSession();
		if (imixSession.isStarted()) {
			imixSession.stop();
			isLogin = true;
		} else {
			isLogin = true;
		}
		return isLogin;
	}

	@Override
	public <T> boolean confirmCPISTransaction(T arg0) throws Exception{
		boolean flag = false;
		Message message = null;
		try {
			message = imixCPISClient.getListener().packageMsg(arg0);
			LogManager.info("receive msg "+":"+message);
			if (message!=null) {
				flag = imixCPISClient.getImixSession().send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			throw e;
		}
		return flag;
	}
	
	@Override
	public List<Object> getRecMessage(){
		List<Object> list = imixCPISClient.getListener().getList();
		int size = list.size();
		List<Object> recList = new ArrayList<Object>();
		if (list != null && size>0) {
			for(int i=0;i<size;i++){
//				String className = ClassNameUtil.getClassName(list.get(0));
//				if("RecCPISFeedBackModel".equals(className)){
					recList.add(list.get(0));
					//移除已返回的交易反馈信息  移一个存一个以免移除交易后新反馈的信息
					imixCPISClient.getListener().getList().remove(0);
//				}
			}
		}
		return recList;
	}

	@Override
	public boolean confirmCPISDetail(QueryModel arg0) {
		//发送查询报文
		boolean flag = false;
		Message message = null;
		try {
			message = sendFXQuery.packageMsg(arg0,imixCPISClient,imixCPISClient.getConfigBean());
			imixCPISClient.getQryMap().put(arg0.getQueryRequestID(), message);
			LogManager.info(""+":"+message);
			if (message!=null) {
				flag = imixCPISClient.getImixSession().send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	@Override
	public boolean matchingCPISDetail(MatchingModel arg0) {
		//发送查询报文
		boolean flag = false;
		Message message = null;
		try {
			message = sendFXQuery.packageMsg(arg0,imixCPISClient,imixCPISClient.getConfigBean());
			imixCPISClient.getQryMap().put(arg0.getQueryRequestId(), message);
			LogManager.info(""+":"+message);
			if (message!=null) {
				flag = imixCPISClient.getImixSession().send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Override
	public List<Object> getRecDetailMessage() {
		List<Object> list = imixCPISClient.getListener().getQueryList();
		int size = list.size();
		List<Object> recList = new ArrayList<Object>();
		if (list != null && size>0) {
			for(int i=0;i<size;i++){
				recList.add(list.get(0));
				//移除已返回的交易反馈信息  移一个存一个以免移除交易后新反馈的信息
				imixCPISClient.getListener().getQueryList().remove(0);
			}
		}
		return recList;
	}

	@Override
	public boolean confirmCPISTransaction(TradeConfirmBean confirmBean)throws Exception {
		boolean flag = false;
		Message message = null;
		try {
			message = imixCPISClient.getListener().packageMsg(confirmBean);
			LogManager.info("receive msg "+":"+message);
			if (message!=null) {
				flag = imixCPISClient.getImixSession().send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			throw e;
		}
		return flag;
	}
	
	@Override
	public boolean confirmCPISTransactionOPT(List<SendForeignOptionModel> confirmBeans)
			throws Exception {
		boolean flag = false;
		Message message = null;
		try {
			message = imixCPISClient.getListener().packageOptMsg(confirmBeans);
			LogManager.info("receive msg "+":"+message);
			if (message!=null) {
				flag = imixCPISClient.getImixSession().send(message);
			}
			LogManager.info("发送message返回的flag:"+ flag);
		} catch (Exception e) {
			throw e;
		}
		return flag;
	}

	@Override
	public boolean getImixSessionState() {
		ImixSession imixSession = imixCPISClient.getImixSession();
		return imixSession.isStarted();
	}

	@Override
	public boolean isSessionStarted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String runShell(String shfile) {
		// TODO Auto-generated method stub
		return null;
	}


}
