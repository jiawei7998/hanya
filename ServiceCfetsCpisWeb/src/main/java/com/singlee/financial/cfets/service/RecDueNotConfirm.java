package com.singlee.financial.cfets.service;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecDueNotConfirmModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.ConfirmType;
import imix.field.MarketIndicator;
import imix.field.TradeInstrument;
import imix.field.TransactionNum;

/**
 * 
 * <P>RecDueNotConfirm.java</P>
 * <P>接收到期未确认提醒 IMIX 报文标准</P>
 * @author pengqian
 * <P>Company:新利</P>
 */
public class RecDueNotConfirm {

	public RecDueNotConfirmModel ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		RecDueNotConfirmModel model = new RecDueNotConfirmModel();
		
		//分页信息
		RecPageInfoModel pageInfoModel = new RecPageInfoModel();
		
		MessageUtil msgUtil = new MessageUtil(message);
		
		//市场类型	
		String tradeInstrument = msgUtil.getStringValue(TradeInstrument.FIELD);
		
		model.setTradeInstrument(tradeInstrument);
		
		//交易类型
		String marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
		
		model.setMarketIndicator(marketIndicator);
		
		//类型
		String confirmType  = msgUtil.getStringValue(ConfirmType.FIELD);
		
		model.setConfirmType(confirmType);
		
		//到期未确认笔数
		String transactionNum  = msgUtil.getStringValue(TransactionNum.FIELD);
		
		model.setTransactionNum(transactionNum);
		
		pageInfoModel = RecPageInfo.msgPageToModel(message);
		
		model.setPageInfo(pageInfoModel);
		
		return model;
		
	}

}
