package com.singlee.financial.cfets.service;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.util.PropertiesUtil;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.CnfmTime;
import imix.field.CnfmTimeType;
import imix.field.ConfirmID;
import imix.field.ConfirmStatus;
import imix.field.ConfirmType;
import imix.field.EndDate;
import imix.field.ExecID;
import imix.field.LastQty;
import imix.field.MarketIndicator;
import imix.field.NetGrossInd;
import imix.field.NoPartyIDs;
import imix.field.OriAgreementValue;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PayTotalSettlAmt;
import imix.field.SettlDate;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;


/**
 * 
 * <P>RecPreTerIRS.java</P>
 * <P>IRS提前终止数据推送</P>
 * <P>Copyright: Copyright (c) 2018</P>
 * <P>Company: 新利</P>
 * <P>          修改者姓名 修改内容说明</P>
 * @author pengqian
 * @see     参考类1
 */
public class RecPreTerIRS {

	/**
	 * 解析报文
	 * @param message
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	public PropertiesUtil ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {

		PropertiesUtil prop = new PropertiesUtil();
		
		MessageUtil msgUtil = new MessageUtil(message);
		
		prop = MsgHeadAndStandardTrailer.msgHeadToProp(message,prop);

		try {
			String confirmID = "";
			confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
			String confirmStatus = "";
			confirmStatus  = msgUtil.getStringValue(ConfirmStatus.FIELD);
			String execID = "";
			execID  = msgUtil.getStringValue(ExecID.FIELD);
			String lastQty = "";
			lastQty  = msgUtil.getStringValue(LastQty.FIELD);
			String marketIndicator = "";
			marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
			String confirmType = "";
			confirmType  = msgUtil.getStringValue(ConfirmType.FIELD);
			String endDate = "";
			endDate  = msgUtil.getStringValue(EndDate.FIELD);
			String settlDate = "";
			settlDate  = msgUtil.getStringValue(SettlDate.FIELD);
			String oriAgreementValue = "";
			
			oriAgreementValue  = msgUtil.getStringValue(OriAgreementValue.FIELD);
			String payTotalSettlAmt = "";
			payTotalSettlAmt  = msgUtil.getStringValue(PayTotalSettlAmt.FIELD);
			String netGrossInd = "";
			netGrossInd  = msgUtil.getStringValue(NetGrossInd.FIELD);
			
			
			NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
			CnfmTimeType cnfmTimeType = new CnfmTimeType();
			CnfmTime cnfmTime = new CnfmTime();
			
			int ownCnfmTimeType = 0 ;
			String ownCnfmTime = "";
			int othCnfmTimeType = 0 ;
			String othCnfmTime = "";
			
			
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
				message.getGroup(i, noCnfmTimes);
				cnfmTimeType = noCnfmTimes.getCnfmTimeType();
				cnfmTime = noCnfmTimes.getCnfmTime();
				if (cnfmTimeType.getValue() == 1) {
					ownCnfmTimeType = cnfmTimeType.getValue();
					ownCnfmTime = cnfmTime.getValue();
				}else if (cnfmTimeType.getValue() == 2) {
					othCnfmTimeType = cnfmTimeType.getValue();
					othCnfmTime = cnfmTime.getValue();
				}
			}
			
			String othPartyId = "";
			String payPartyId = "";
			String receiptPartyId = "";
			
			PartyID partyid = new PartyID();
			PartyRole partyrole = new PartyRole();
			ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
			
			
			
			for (int k = 1; k <= msgUtil.getIntValue(NoPartyIDs.FIELD); k++) {
				message.getGroup(k, nopartyids1);
				nopartyids1.get(partyrole);
				nopartyids1.get(partyid);
				
				if(partyrole.getValue()==17){
					othPartyId = partyid.getValue();
				}else if (partyrole.getValue()==129) {
					payPartyId = partyid.getValue();
				}else if (partyrole.getValue()==130) {
					receiptPartyId = partyid.getValue();
				}
				
			}
			prop.setProperties("ConfirmID", confirmID);
			prop.setProperties("ConfirmStatus", confirmStatus);
			prop.setProperties("ExecID", execID);
			prop.setProperties("LastQty", lastQty);
			prop.setProperties("MarketIndicator", marketIndicator);
			prop.setProperties("ConfirmType", confirmType);
			prop.setProperties("EndDate", endDate);
			prop.setProperties("SettlDate", settlDate);
			prop.setProperties("OriAgreementValue", oriAgreementValue);
			prop.setProperties("PayTotalSettlAmt", payTotalSettlAmt);
			prop.setProperties("NetGrossInd", netGrossInd);
			prop.setProperties("ownCnfmTimeType", ownCnfmTimeType);
			prop.setProperties("ownCnfmTime", ownCnfmTime);
			prop.setProperties("othCnfmTimeType", othCnfmTimeType);
			prop.setProperties("othCnfmTime", othCnfmTime);
			prop.setProperties("othPartyId", othPartyId);
			prop.setProperties("payPartyId", payPartyId);
			prop.setProperties("receiptPartyId", receiptPartyId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
		
		
		
	}
}
