package com.singlee.financial.cfets.cpis;

import com.singlee.financial.cfets.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * CFETS外汇即远调及查询接口服务
 */
@Service
public class ImixCPISCfetsApiService {

	@Autowired
	ICpisFxMMTradeService cpisFxMMTradeService;
		
	@Autowired
	ICpisFxSptFwdTradeService cpisFxSptFwdTradeService;
	
	@Autowired
	ICpisFxSwpTradeService cpisFxSwpTradeService;
	
	@Autowired
	ICpisIrsTradeService cpisIrsTradeService;
	
	@Autowired
	ICpisRecMsgService cpisRecMsgService;
	
	@Autowired
	ICpisSwiftMatchService cpisSwiftMatchService;
	
	@Autowired
	ICpisCrsTradeService cpisCrsTradeService;
	
	@Autowired
	ICpisFxoptTradeService cpisFxoptTradeService;

	
	public ICpisCrsTradeService getCpisCrsTradeService() {
		return cpisCrsTradeService;
	}

	public void setCpisCrsTradeService(ICpisCrsTradeService cpisCrsTradeService) {
		this.cpisCrsTradeService = cpisCrsTradeService;
	}

	public ICpisFxMMTradeService getCpisFxMMTradeService() {
		return cpisFxMMTradeService;
	}

	public void setCpisFxMMTradeService(ICpisFxMMTradeService cpisFxMMTradeService) {
		this.cpisFxMMTradeService = cpisFxMMTradeService;
	}

	public ICpisFxSptFwdTradeService getCpisFxSptFwdTradeService() {
		return cpisFxSptFwdTradeService;
	}

	public void setCpisFxSptFwdTradeService(
			ICpisFxSptFwdTradeService cpisFxSptFwdTradeService) {
		this.cpisFxSptFwdTradeService = cpisFxSptFwdTradeService;
	}

	public ICpisFxSwpTradeService getCpisFxSwpTradeService() {
		return cpisFxSwpTradeService;
	}

	public void setCpisFxSwpTradeService(
			ICpisFxSwpTradeService cpisFxSwpTradeService) {
		this.cpisFxSwpTradeService = cpisFxSwpTradeService;
	}

	public ICpisIrsTradeService getCpisIrsTradeService() {
		return cpisIrsTradeService;
	}

	public void setCpisIrsTradeService(ICpisIrsTradeService cpisIrsTradeService) {
		this.cpisIrsTradeService = cpisIrsTradeService;
	}

	public ICpisRecMsgService getCpisRecMsgService() {
		return cpisRecMsgService;
	}

	public void setCpisRecMsgService(ICpisRecMsgService cpisRecMsgService) {
		this.cpisRecMsgService = cpisRecMsgService;
	}

	public ICpisSwiftMatchService getCpisSwiftMatchService() {
		return cpisSwiftMatchService;
	}

	public void setCpisSwiftMatchService(
			ICpisSwiftMatchService cpisSwiftMatchService) {
		this.cpisSwiftMatchService = cpisSwiftMatchService;
	}

	public ICpisFxoptTradeService getCpisFxoptTradeService() {
		return cpisFxoptTradeService;
	}

	public void setCpisFxoptTradeService(
			ICpisFxoptTradeService cpisFxoptTradeService) {
		this.cpisFxoptTradeService = cpisFxoptTradeService;
	}
	
	

	
}
