package com.singlee.financial.cfets.service;

import imix.ConfigError;
import imix.FieldNotFound;
import imix.Message;
import imix.client.core.ImixSession;
import imix.field.AffirmStatus;
import imix.field.BusinessMessageResponseID;
import imix.field.CombinationID;
import imix.field.ConfirmID;
import imix.field.DeliverToCompID;
import imix.field.ExecID;
import imix.field.MarketID;
import imix.field.MarketIndicator;
import imix.field.OnBehalfOfCompID;
import imix.field.ProcessingStatus;
import imix.field.SenderCompID;
import imix.field.TargetCompID;
import imix.field.TradeDate;
import imix.imix10.MassCofirmation.NoMassCofirmations;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.cfets.cpis.MessageUtil;


/**
 * 
 * <P>SendFXResponse.java</P>
 * <P>发送反馈信息报文处理类</P>
 * <P>Copyright: Copyright (c) 2018</P>
 * <P>Company: 新利</P>
 * <P>          修改者姓名 修改内容说明</P>
 * @author pengqian
 * @see     参考类1
 */
public class SendResponse {
	
	private static Logger LogManager = LoggerFactory.getLogger(SendQuery.class);
	/**
	 * 拼装上送IMIX消息报文
	 * @param prop
	 * @return
	 * @throws Exception 
	 * @throws ConfigError
	 */
	public void sendResponse(Message message,int type, ConfigBean configBean, ImixSession imixSession) throws Exception {
		Message respnse = MsgHeadAndStandardTrailer.packageMsgHeadAndStandardTrailer("AU", configBean);
		
		MessageUtil msgUtil = new MessageUtil(message);
		
		if("".equals(msgUtil.getStringValue(ConfirmID.FIELD))){
			System.out.println("消息未收到域：ConfirmID "+ConfirmID.FIELD);
		}else {
			respnse.setField(new ConfirmID(msgUtil.getStringValue(ConfirmID.FIELD)));// 确认报文 ID
		}
		if("".equals(msgUtil.getStringValue(TradeDate.FIELD))){
			System.out.println("消息未收到域：TradeDate "+TradeDate.FIELD);
		}else {
			respnse.setField(new TradeDate(msgUtil.getStringValue(TradeDate.FIELD)));
		}
		if ("".equals(msgUtil.getStringValue(ExecID.FIELD))) {
			System.out.println("消息未收到域：ExecID "+ExecID.FIELD);
		}else {
			respnse.setField(new ExecID(msgUtil.getStringValue(ExecID.FIELD)));
		}
		
		respnse.setField(new AffirmStatus(type));
		respnse.setField(new MarketID("2"));
		
		
		String marketIndicator = msgUtil.getStringValue(MarketIndicator.FIELD);
		//利率互换交易特殊处理
		if (null!=marketIndicator&& "2".equals(marketIndicator)) {
			try {
				respnse.getHeader().setField(new OnBehalfOfCompID(msgUtil.getHeadStringValue(SenderCompID.FIELD)));
				respnse.getHeader().setField(new DeliverToCompID(msgUtil.getHeadStringValue(TargetCompID.FIELD)));
				respnse.setField(new MarketIndicator(marketIndicator));
			} catch (FieldNotFound e) {
				e.printStackTrace();
			}
		}
		//外汇期权特殊处理
		if(null!=marketIndicator&& "19".equals(marketIndicator)){
		 String	combinationID=msgUtil.getStringValue(CombinationID.FIELD);
		 if(StringUtils.isNotBlank(combinationID)){
			 respnse.setField(new CombinationID(combinationID));
		 }else{
			 NoMassCofirmations noMassCofirmations = new NoMassCofirmations();
				for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoMassCofirmations.FIELD); i++) {
					message.getGroup(i, noMassCofirmations);
					respnse.setField(new ExecID(noMassCofirmations.getExecID().getValue()));
				}
			 
		 }
		}
		LogManager.info("发送反馈信息报文");
		LogManager.info(respnse.toString());
//		boolean flag = true;
		boolean flag = imixSession.send(respnse);
		
		if (flag) {
			LogManager.info("反馈信息发送成功！");
		}else {
			LogManager.info("反馈信息发送失败！");
		}
		
	}
	
	
	public void sendResponse(Message message,ConfigBean configBean, ImixSession imixSession) throws Exception {
		Message respnse = MsgHeadAndStandardTrailer.packageMsgHeadAndStandardTrailer("U204", configBean);
		
		MessageUtil msgUtil = new MessageUtil(message);
		
		if("".equals(msgUtil.getStringValue(ConfirmID.FIELD))){
			System.out.println("消息未收到域：ConfirmID "+ConfirmID.FIELD);
		}
		
		respnse.setField(new BusinessMessageResponseID(msgUtil.getStringValue(ConfirmID.FIELD)));
		respnse.setField(new ProcessingStatus(11));
		
	
		LogManager.info("发送反馈信息报文");
		LogManager.info(respnse.toString());
		boolean flag = imixSession.send(respnse);
		
		if (flag) {
			LogManager.info("反馈信息发送成功！");
		}else {
			LogManager.info("反馈信息发送失败！");
		}
		
	}
}
