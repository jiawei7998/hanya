package com.singlee.financial.cfets.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.singlee.financial.cfets.cpis.MessageUtil;
import com.singlee.financial.cfets.bean.RecForeignFxmmModel;
import com.singlee.financial.cfets.bean.RecPageInfoModel;

import imix.FieldNotFound;
import imix.IncorrectTagValue;
import imix.Message;
import imix.UnsupportedMessageType;
import imix.field.AccruedInterestTotalAmt;
import imix.field.CalculatedCcyLastQty;
import imix.field.CnfmTime;
import imix.field.CnfmTimeType;
import imix.field.ConfirmID;
import imix.field.ConfirmStatus;
import imix.field.Currency;
import imix.field.Currency2;
import imix.field.DayCount;
import imix.field.DealTransType;
import imix.field.ExecID;
import imix.field.ExecType;
import imix.field.LastPx;
import imix.field.LastQty;
import imix.field.MarketIndicator;
import imix.field.NetGrossInd;
import imix.field.NoPartyIDs;
import imix.field.NoPartySubIDs;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubID;
import imix.field.PartySubIDType;
import imix.field.SettlCurrAmt2;
import imix.field.SettlDate;
import imix.field.SettlDate2;
import imix.field.Side;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoRelatedReference.NoCnfmTimes;

/**
 * 
 * <P>RecForeignSpot.java</P>
 * <P>接受外币拆借消息处理类</P>
 * <P>Company:新利</P>
 * @author pengqian
 */
public class RecForeignFxmm {

	/**
	 * 解析报文
	 * @param message
	 * @return 
	 * @throws FieldNotFound
	 * @throws Exception
	 * @throws UnsupportedMessageType
	 * @throws IncorrectTagValue
	 */
	private static Logger LogManager = LoggerFactory.getLogger(RecForeignFxmm.class);
	
	public RecForeignFxmmModel ExecutionReport(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		
		
		MessageUtil msgUtil = new MessageUtil(message);
		RecForeignFxmmModel fxmmModel = new RecForeignFxmmModel();
		RecPageInfoModel pageInfo = new RecPageInfoModel();
		
		try {
			
			String execID  = msgUtil.getStringValue(ExecID.FIELD);
			fxmmModel.setExecID(execID);
			String tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);
			fxmmModel.setTradeDate(tradeDate);
			String	netGrossInd  = msgUtil.getStringValue(NetGrossInd.FIELD);
			fxmmModel.setNetGrossInd(netGrossInd);
			String marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
			fxmmModel.setMarketIndicator(marketIndicator);
			String execType  = msgUtil.getStringValue(ExecType.FIELD);
			fxmmModel.setExecType(execType);
			String dealTransType  = msgUtil.getStringValue(DealTransType.FIELD);
			fxmmModel.setDealTransType(dealTransType);
			String confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
			fxmmModel.setConfirmID(confirmID);
			String confirmStatus = msgUtil.getStringValue(ConfirmStatus.FIELD);
			fxmmModel.setConfirmStatus(confirmStatus);
			NoCnfmTimes noCnfmTimes = new NoCnfmTimes();
			CnfmTimeType cnfmTimeType = new CnfmTimeType();
			CnfmTime cnfmTime = new CnfmTime();
			
			int ownCnfmTimeType = 0 ;
			String ownCnfmTime = "";
			int othCnfmTimeType = 0 ;
			String othCnfmTime = "";
				
			for (int i = 1; i <= msgUtil.getIntValue(imix.field.NoCnfmTimes.FIELD); i++) {
				message.getGroup(i, noCnfmTimes);
				cnfmTimeType = noCnfmTimes.getCnfmTimeType();
				cnfmTime = noCnfmTimes.getCnfmTime();
				if (cnfmTimeType.getValue() == 1) {
					ownCnfmTimeType = cnfmTimeType.getValue();
					fxmmModel.setOwnCnfmTimeType(ownCnfmTimeType);
					ownCnfmTime = cnfmTime.getValue();
					fxmmModel.setOwnCnfmTime(ownCnfmTime);
				}else if (cnfmTimeType.getValue() == 2) {
					othCnfmTimeType = cnfmTimeType.getValue();
					fxmmModel.setOthCnfmTimeType(othCnfmTimeType);
					othCnfmTime = cnfmTime.getValue();
					fxmmModel.setOthCnfmTime(othCnfmTime);
				}
			}
			
			String	settlDate  = msgUtil.getStringValue(SettlDate.FIELD);
			fxmmModel.setSettlDate(settlDate);
			
			String settlDate2 = msgUtil.getStringValue(SettlDate2.FIELD);
			fxmmModel.setSettlDate2(settlDate2);
			
			String	lastPx  = msgUtil.getStringValue(LastPx.FIELD);			
			fxmmModel.setLastPx(lastPx);
			
			String	currency  = msgUtil.getStringValue(Currency.FIELD);				
			fxmmModel.setCurrency(currency);
			
			String	lastQty  = msgUtil.getStringValue(LastQty.FIELD);	
			fxmmModel.setLastQty(lastQty);
			
			String currency2  = msgUtil.getStringValue(Currency2.FIELD);				
			fxmmModel.setCurrency2(currency2);
		
			String calculatedCcyLastQty  = msgUtil.getStringValue(CalculatedCcyLastQty.FIELD);				
			fxmmModel.setCalculatedCcyLastQty(calculatedCcyLastQty);
			
			String side = msgUtil.getStringValue(Side.FIELD);
			fxmmModel.setSide(side);
			
			String dayCount = msgUtil.getStringValue(DayCount.FIELD);
			fxmmModel.setDayCount(dayCount);
			
			String accruedInterestTotalAmt = msgUtil.getStringValue(AccruedInterestTotalAmt.FIELD);
			fxmmModel.setAccruedInterestTotalAmt(accruedInterestTotalAmt);
			
			String settleCurrAmt2 = msgUtil.getStringValue(SettlCurrAmt2.FIELD);
			fxmmModel.setSettleCurrAmt2(settleCurrAmt2);
			
			int partyrole=-1; //本方对手方
			
			String partyid="";//21位机构码
			
			String partysubid="";
		    
		    int partysubidtype=-1;
			
			ExecutionReport.NoPartyIDs nopartyids1 = new ExecutionReport.NoPartyIDs();
			ExecutionReport.NoPartyIDs.NoPartySubIDs nopartysubids1 = new ExecutionReport.NoPartyIDs.NoPartySubIDs();
			int nopartysubids=-1;//清算信息循环组
			
			NoPartyIDs nopartyids = new NoPartyIDs();
			
			for (int k = 1; k <= message.getField(nopartyids).getValue(); k++) {
				message.getGroup(k, nopartyids1);
				
				partyrole=msgUtil.getIntValue(nopartyids1, PartyRole.FIELD);

				partyid=msgUtil.getStringValue(nopartyids1, PartyID.FIELD);
				
				if(partyrole==1){
					String ownPartyID = partyid;
					fxmmModel.setOwnPartyID(ownPartyID);
					
					int ownPartyRole = partyrole;
					fxmmModel.setOwnPartyRole(ownPartyRole);
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						
							partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						
						if (partysubidtype == 124) {
							String	ownCnName =  partysubid;
							fxmmModel.setOwnCnName(ownCnName);
						}else if (partysubidtype == 110) {
							String ownCapitalBakeName =  partysubid;
							fxmmModel.setOwnCapitalBakeName(ownCapitalBakeName);
						 
						}else if (partysubidtype == 23) {
							String	ownCapitalAccountName =  partysubid;
							fxmmModel.setOwnCapitalAccountName(ownCapitalAccountName);
						}else if (partysubidtype == 16) {
							String ownPayBankNo =  partysubid;
							fxmmModel.setOwnPayBankNo(ownPayBankNo);
						 
						}else if (partysubidtype == 15) {
							String ownCapitalAccount =  partysubid;
							fxmmModel.setOwnCapitalAccount(ownCapitalAccount);
						 
						}else if (partysubidtype == 139) {
							String	ownRemark =  partysubid;
							fxmmModel.setOwnRemark(ownRemark);
						 
						}else if (partysubidtype == 209) {
							String	midCnName =  partysubid;
							fxmmModel.setMidCnName(midCnName);
						 
						}else if (partysubidtype == 211) {
							String midBankBIC =  partysubid;
							fxmmModel.setMidBankBIC(midBankBIC);
						}else if (partysubidtype == 138) {
							String ownCapitalAccountBIC =  partysubid;
							fxmmModel.setOwnCapitalAccountBIC(ownCapitalAccountBIC);
						 
						}else if (partysubidtype == 213) {
							String	midBankAccount =  partysubid;
							fxmmModel.setMidBankAccount(midBankAccount);
						 
						}else if (partysubidtype == 207) {
							String ownOpenAccount =  partysubid;
							fxmmModel.setOwnOpenAccount(ownOpenAccount);
						}else if(partysubidtype == 5){
							String ownEnName =  partysubid;
							fxmmModel.setOwnEnName(ownEnName);
						}
					}
				}else if (partyrole==17) {
					String othPartyID = partyid;
					fxmmModel.setOthPartyID(othPartyID);
					
					int othPartyRole = partyrole;
					fxmmModel.setOthPartyRole(othPartyRole);
					
					nopartysubids=msgUtil.getIntValue(nopartyids1, NoPartySubIDs.FIELD);
					for (int g = 1; g <= nopartysubids; g++) {
						nopartyids1.getGroup(g, nopartysubids1);
						
						partysubidtype=msgUtil.getIntValue(nopartysubids1, PartySubIDType.FIELD);
						
						partysubid=msgUtil.getStringValue(nopartysubids1, PartySubID.FIELD);
						if (partysubidtype == 124) {
							String othCnName =  partysubid;
							fxmmModel.setOthCnName(othCnName);
						 
						}else if (partysubidtype == 110) {
							String	othCapitalBakeName =  partysubid;
							fxmmModel.setOthCapitalBakeName(othCapitalBakeName);
						 
						}else if (partysubidtype == 23) {
							String othCapitalAccountName =  partysubid;
							fxmmModel.setOthCapitalAccountName(othCapitalAccountName);
						 
						}else if (partysubidtype == 16) {
							String	othPayBankNo =  partysubid;
							fxmmModel.setOthPayBankNo(othPayBankNo);
						 
						}else if (partysubidtype == 15) {
							String othCapitalAccount =  partysubid;
							fxmmModel.setOthCapitalAccount(othCapitalAccount);
						 
						}else if (partysubidtype == 139) {
							String othRemark =  partysubid;
							fxmmModel.setOthRemark(othRemark);
						}else if(partysubidtype == 5){
							String othEnName =  partysubid;
							fxmmModel.setOthEnName(othEnName);
						}
					}
				
				}
			}
			
			//分页信息
			pageInfo = RecPageInfo.msgPageToModel(message);
			
			fxmmModel.setPageInfo(pageInfo);

		} catch (Exception e) {
			LogManager.info("解析报文出错"+e);
			throw e;
		}
		return fxmmModel;
	}

	public RecForeignFxmmModel ExecutionReportCancel(Message message)
			throws FieldNotFound, Exception, UnsupportedMessageType,
			IncorrectTagValue {
		MessageUtil msgUtil = new MessageUtil(message);
		RecForeignFxmmModel fxmmModel = new RecForeignFxmmModel();
		try {
			String execID = "";
			execID  = msgUtil.getStringValue(ExecID.FIELD);
			String tradeDate = "";
			tradeDate  = msgUtil.getStringValue(TradeDate.FIELD);
			String marketIndicator = "";
			marketIndicator  = msgUtil.getStringValue(MarketIndicator.FIELD);
			String execType = "";
			execType  = msgUtil.getStringValue(ExecType.FIELD);
			String confirmID = "";
			confirmID  = msgUtil.getStringValue(ConfirmID.FIELD);
			
			
			if ("".equals(execID)) {
				System.out.println("消息未收到域：ExecID "+ExecID.FIELD);
			}	
			if ("".equals(tradeDate)) {
				System.out.println("消息未收到域：TradeDate "+TradeDate.FIELD);
			}	
			if ("".equals(marketIndicator)) {
				System.out.println("消息未收到域：MarketIndicator "+MarketIndicator.FIELD);
			}			
			if ("".equals(execType)) {
				System.out.println("消息未收到域：ExecType "+ExecType.FIELD);
			}			
			if ("".equals(confirmID)) {
				System.out.println("消息未收到域：ConfirmID "+ConfirmID.FIELD);
			}			
			
			fxmmModel.setExecID(execID);
			fxmmModel.setTradeDate(tradeDate);
			fxmmModel.setMarketIndicator(marketIndicator);
			fxmmModel.setExecType(execType);
			fxmmModel.setConfirmID(confirmID);
			
			RecPageInfoModel pageInfo = new RecPageInfoModel();
			//分页信息
			pageInfo = RecPageInfo.msgPageToModel(message);
			fxmmModel.setPageInfo(pageInfo);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return fxmmModel;
		
	}
}
