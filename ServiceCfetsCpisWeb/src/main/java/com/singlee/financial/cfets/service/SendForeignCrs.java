package com.singlee.financial.cfets.service;

import imix.field.AccountCurrency;
import imix.field.AltSettlDate;
import imix.field.AltSettlDate2;
import imix.field.CalculateAgency;
import imix.field.ConfirmID;
import imix.field.DateAdjustmentIndic;
import imix.field.ExecID;
import imix.field.FinalExDate;
import imix.field.IniExDate;
import imix.field.LastSpotRate;
import imix.field.LegBenchmarkCurveName;
import imix.field.LegBenchmarkSpread;
import imix.field.LegBenchmarkTenor;
import imix.field.LegCouponPaymentDate;
import imix.field.LegCouponPaymentDateReset;
import imix.field.LegCouponPaymentFrequency;
import imix.field.LegCurrency;
import imix.field.LegDayCount;
import imix.field.LegInterestAccrualResetFrequency;
import imix.field.LegInterestFixDateAdjustment;
import imix.field.LegOrderQty;
import imix.field.LegPrice;
import imix.field.LegPriceType;
import imix.field.LegSide;
import imix.field.LegSign;
import imix.field.LegStubIndicator;
import imix.field.MarketIndicator;
import imix.field.MaturityDate;
import imix.field.NetGrossInd;
import imix.field.NoPayment;
import imix.field.NotionalExchangeType;
import imix.field.PartyID;
import imix.field.PartyRole;
import imix.field.PartySubIDType;
import imix.field.PayDate;
import imix.field.PaymentAmt;
import imix.field.PaymentCurrency;
import imix.field.SettlDate;
import imix.field.Text;
import imix.field.TradeDate;
import imix.imix10.ExecutionReport;
import imix.imix10.ExecutionReport.NoLegs;
import imix.imix10.Message;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.singlee.financial.cfets.config.ConfigBean;
import com.singlee.financial.model.IntPayModel;
import com.singlee.financial.model.SendForeignCrsModel;
import com.singlee.financial.model.SendForeignOptionModel;
import com.singlee.financial.pojo.TradeConfirmBean;
import com.singlee.financial.pojo.TradeSettlsBean;
import com.singlee.financial.util.BigDecimalUtil;


/**
 * 
 * <P>SendForeignSwap.java</P>
 * <P>发送货币掉期成交确认报文处理类</P>
 * <P>Company: 新利</P>
 * @author 
 * <P>          修改者姓名 修改内容说明</P>
 */
public class SendForeignCrs extends ConfirmMsgService{
	
	public SendForeignCrs(ConfigBean configBean) {
		super(configBean);
	}
	
	@Override
	public void pkgMsgBody(imix.imix10.Message message, TradeConfirmBean bean)throws Exception {
		SendForeignCrsModel model = (SendForeignCrsModel)bean;
		// 成交编号， 需求未约束编码规则
		message.setField(new ExecID(model.getExecID())); 
		// 成交日期，格式为：YYYYMMDD
		message.setField(new TradeDate(model.getTradedate().replace("<Date>", "")));
		// 确认报文 ID
		message.setField(new ConfirmID(model.getConfirmid()));
		// 生效日
		message.setField(new SettlDate(model.getSettldate()));
		// 市场类型
		message.setField(new MarketIndicator("17"));
		// 清算方式
		if ("2".equals(model.getNetGrossInd())) {
			message.setField(new NetGrossInd(2));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		} else {
			message.setField(new NetGrossInd(1));// 1 清算方式， 1-净额轧差（询价） ；2-双边全额清算
		}
		// 到期日
		message.setField(new MaturityDate(model.getMaturitydate()));
		// 起息日调整
		if ("Y".equals(model.getDateadjustmentindic())) {
			message.setField(new DateAdjustmentIndic(true));
		}else if ("N".equals(model.getDateadjustmentindic())) {
			message.setField(new DateAdjustmentIndic(false));
		}
		// 期初本金交换日
		if(StringUtils.isNotBlank(model.getIniexdate())) {message.setField(new IniExDate(model.getIniexdate()));}
		// 期末本金交换日
		if(StringUtils.isNotBlank(model.getFinalexdate())) {message.setField(new FinalExDate(model.getFinalexdate()));}
		// 计算机构
		if (StringUtils.isNotBlank(model.getCalculateagency())) {
			message.setField(new CalculateAgency(Integer.valueOf(model.getCalculateagency())));
		}
		// 本金交换形式
		if (StringUtils.isNotBlank(model.getNotionalexchangetype())) {
			message.setField(new NotionalExchangeType(Integer.valueOf(model.getNotionalexchangetype())));
		}
		// 即期汇率
		message.setField(new LastSpotRate(BigDecimalUtil.round(new BigDecimal(model.getLastspotrate()).doubleValue(),9)));
		// 备注
		if(StringUtils.isNotBlank(model.getText())) {message.setField(new Text(model.getText()));}
		message.setField(new imix.field.NoLegs(2));
		NoLegs noLegs1 = new NoLegs();
		if("1".equals(model.getLegSide_1())){
			noLegs1.set(new LegSide('1'));//组件必须域
		}else if("4".equals(model.getLegSide_1())){
			noLegs1.set(new LegSide('4'));//组件必须域
		}else{
			noLegs1.set(new LegSide('-'));//组件必须域
		}
		noLegs1.set(new LegSign('1'));//本方
		noLegs1.set(new LegCurrency(model.getLegcurrency_1()));//本方货币代码
		noLegs1.set(new LegCouponPaymentDate(model.getLegcouponpaymentdate_1()));//本方首次付息日
		if (StringUtils.isNotBlank(model.getLegorderqty_1())) {
			noLegs1.set(new LegOrderQty(BigDecimalUtil.round(new BigDecimal(model.getLegorderqty_1()).doubleValue(),2)));//本方名义本金
		}
		if ("3".equals(model.getLegpricetype_1())) {
			noLegs1.set(new LegPriceType(3));//本方利率类型 固定利率
			if (StringUtils.isNotBlank(model.getLegprice_1())) {
				noLegs1.set(new LegPrice(BigDecimalUtil.fixZero(BigDecimalUtil.round(new BigDecimal(model.getLegprice_1()).doubleValue()/100, 6),6,"0")));//本方固定利率
			}
		}else if ("6".equals(model.getLegpricetype_1())) {
			noLegs1.set(new LegPriceType(6));//本方利率类型 浮动利率
			if(StringUtils.isNotBlank(model.getLegbenchmarkcurvename_1())) {noLegs1.set(new LegBenchmarkCurveName(model.getLegbenchmarkcurvename_1()));}//本方使用利率
			if(StringUtils.isNotBlank(model.getLegbenchmarktenor_1())) {noLegs1.set(new LegBenchmarkTenor(model.getLegbenchmarktenor_1().trim()));}//本方利率期限
			if(StringUtils.isNotBlank(model.getLegintaccresetfrequency_1())) {noLegs1.set(new LegInterestAccrualResetFrequency(model.getLegintaccresetfrequency_1()));}//本方定息周期
			if ("1".equals(model.getLegintfixdateadjustment_1())) {
				noLegs1.set(new LegInterestFixDateAdjustment('1'));//本方定息规则
			}else if ("2".equals(model.getLegintfixdateadjustment_1())) {
				noLegs1.set(new LegInterestFixDateAdjustment('2'));//本方定息规则
			}else if ("0".equals(model.getLegintfixdateadjustment_1())) {
				noLegs1.set(new LegInterestFixDateAdjustment('0'));//本方定息规则
			}
			if (StringUtils.isNotBlank(model.getLegbenchmarkspread_1())) {
				noLegs1.set(new LegBenchmarkSpread(BigDecimalUtil.round(new BigDecimal(model.getLegbenchmarkspread_1()).doubleValue(), 4)));//本方利差
			}else{
				noLegs1.set(new LegBenchmarkSpread(BigDecimalUtil.round(new BigDecimal(0).doubleValue(), 4)));//本方利差
			}
		}
		noLegs1.set(new LegDayCount(model.getLegdaycount_1().charAt(0)));// 本方计息基准
		if (StringUtils.isNotBlank(model.getLegcouponpaymentfrequency_1())) {
			noLegs1.set(new LegCouponPaymentFrequency(model.getLegcouponpaymentfrequency_1()));// 本方计息周期
		}
		noLegs1.set(new LegCouponPaymentDateReset(model.getLegcouponpaymentdatereset_1().charAt(0)));// 本方付息日调整规则
		if ("1".equals(model.getLegstubindicator_1())){
			noLegs1.set(new LegStubIndicator(1));//残段标识
		}else if ("2".equals(model.getLegstubindicator_1())) {
			noLegs1.set(new LegStubIndicator(2));//残段标识
		}else if ("3".equals(model.getLegstubindicator_1())) {
			noLegs1.set(new LegStubIndicator(3));//残段标识
		}
		noLegs1.set(new NoPayment(model.getIntPayModels1().size()));//重复次数
		for(IntPayModel intpaymodel:model.getIntPayModels1()){
			imix.imix10.component.PaymentGrp.NoPayment noPayment = new imix.imix10.component.PaymentGrp.NoPayment();
			if (StringUtils.isNotBlank(intpaymodel.getAmorPaymentAmt())&&new BigDecimal(intpaymodel.getAmorPaymentAmt()).doubleValue()>0) {
				noPayment.setField(new PaymentAmt(BigDecimalUtil.round(new BigDecimal(intpaymodel.getAmorPaymentAmt()).doubleValue(), 2)));//摊销金额
			}
			noPayment.setField(new PaymentCurrency(model.getLegcurrency_1()));//StringUtils.isNotBlank(intpaymodel.getAmorPaymentCcy())?intpaymodel.getAmorPaymentCcy():"-"));//摊销币种
			noPayment.setField(new AltSettlDate(intpaymodel.getIntSettlDate()));// 计息开始日
			noPayment.setField(new AltSettlDate2(StringUtils.isNotBlank(intpaymodel.getIntSettlDate2())?intpaymodel.getIntSettlDate2():intpaymodel.getIntPayDate()));// 计息终止日
			noPayment.setField(new PayDate(intpaymodel.getIntPayDate()));// 付息日期
			noLegs1.addGroup(noPayment);
		}
		
		NoLegs noLegs2 = new NoLegs();
		if("1".equals(model.getLegSide_2())){
			noLegs2.set(new LegSide('1'));//组件必须域
		}else if("4".equals(model.getLegSide_2())){
			noLegs2.set(new LegSide('4'));//组件必须域
		}else{
			noLegs2.set(new LegSide('-'));//组件必须域
		}
		noLegs2.set(new LegSign('2'));//对手方
		noLegs2.set(new LegCurrency(model.getLegcurrency_2()));//对手方货币代码
		noLegs2.set(new LegCouponPaymentDate(model.getLegcouponpaymentdate_2()));//对手方首次付息日
		if (StringUtils.isNotBlank(model.getLegorderqty_2())) {
			noLegs2.set(new LegOrderQty(BigDecimalUtil.round(new BigDecimal(model.getLegorderqty_2()).doubleValue(),2)));//对手方名义本金
		}
		if ("3".equals(model.getLegpricetype_2())) {
			noLegs2.set(new LegPriceType(3));//对手方利率类型 固定利率
			if (StringUtils.isNotBlank(model.getLegprice_2())) {
				noLegs2.set(new LegPrice(BigDecimalUtil.fixZero(BigDecimalUtil.round(new BigDecimal(model.getLegprice_2()).doubleValue()/100, 6),6,"0")));//对手方固定利率
			}
		}else if ("6".equals(model.getLegpricetype_2())) {
			noLegs2.set(new LegPriceType(6));//对手方利率类型 浮动利率
			if(StringUtils.isNotBlank(model.getLegbenchmarkcurvename_2())) {noLegs2.set(new LegBenchmarkCurveName(model.getLegbenchmarkcurvename_2()));}//对手方使用利率
			if(StringUtils.isNotBlank(model.getLegbenchmarktenor_2())) {noLegs2.set(new LegBenchmarkTenor(model.getLegbenchmarktenor_2().trim()));}//对手方利率期限
			if(StringUtils.isNotBlank(model.getLegintaccresetfrequency_2())) {noLegs2.set(new LegInterestAccrualResetFrequency(model.getLegintaccresetfrequency_2()));}//对手方定息周期
			if ("1".equals(model.getLegintfixdateadjustment_2())) {
				noLegs2.set(new LegInterestFixDateAdjustment('1'));//对手方定息规则
			}else if ("2".equals(model.getLegintfixdateadjustment_2())) {
				noLegs2.set(new LegInterestFixDateAdjustment('2'));//对手方定息规则
			}else if ("0".equals(model.getLegintfixdateadjustment_2())) {
				noLegs2.set(new LegInterestFixDateAdjustment('0'));//对手方定息规则
			}
			if (StringUtils.isNotBlank(model.getLegbenchmarkspread_2())) {
				noLegs2.set(new LegBenchmarkSpread(BigDecimalUtil.round(new BigDecimal(model.getLegbenchmarkspread_2()).doubleValue(), 4)));//对手方利差
			}else{
				noLegs2.set(new LegBenchmarkSpread(BigDecimalUtil.round(new BigDecimal(0).doubleValue(), 4)));//对手方利差
			}
		}
		noLegs2.set(new LegDayCount(model.getLegdaycount_2().charAt(0)));// 对手方计息基准
		if (StringUtils.isNotBlank(model.getLegcouponpaymentfrequency_2())) {
			noLegs2.set(new LegCouponPaymentFrequency(model.getLegcouponpaymentfrequency_2()));// 对手方计息周期
		}
		noLegs2.set(new LegCouponPaymentDateReset(model.getLegcouponpaymentdatereset_2().charAt(0)));// 对手方付息日调整规则
		if ("1".equals(model.getLegstubindicator_2())){
			noLegs2.set(new LegStubIndicator(1));//残段标识
		}else if ("2".equals(model.getLegstubindicator_2())) {
			noLegs2.set(new LegStubIndicator(2));//残段标识
		}else if ("3".equals(model.getLegstubindicator_2())) {
			noLegs2.set(new LegStubIndicator(3));//残段标识
		}
		noLegs2.set(new NoPayment(model.getIntPayModels2().size()));//重复次数
		for(IntPayModel intpaymodel:model.getIntPayModels2()){
			//PaymentGrp grp2 = new PaymentGrp();
			imix.imix10.component.PaymentGrp.NoPayment noPayment = new imix.imix10.component.PaymentGrp.NoPayment();
			if (StringUtils.isNotBlank(intpaymodel.getAmorPaymentAmt())&&new BigDecimal(intpaymodel.getAmorPaymentAmt()).doubleValue()>0) {
				noPayment.setField(new PaymentAmt(BigDecimalUtil.round(new BigDecimal(intpaymodel.getAmorPaymentAmt()).doubleValue(), 2)));//摊销金额
			}
			noPayment.setField(new PaymentCurrency(model.getLegcurrency_2()));//StringUtils.isNotBlank(intpaymodel.getAmorPaymentCcy())?intpaymodel.getAmorPaymentCcy():"-"));//摊销币种
			noPayment.setField(new AltSettlDate(intpaymodel.getIntSettlDate()));// 计息开始日
			noPayment.setField(new AltSettlDate2(StringUtils.isNotBlank(intpaymodel.getIntSettlDate2())?intpaymodel.getIntSettlDate2():intpaymodel.getIntPayDate()));// 计息终止日
			noPayment.setField(new PayDate(intpaymodel.getIntPayDate()));// 付息日期
			noLegs2.addGroup(noPayment);
		}
		
		message.addGroup(noLegs1);
		message.addGroup(noLegs2);
		
		pkgSettlInfo(message, bean.getTradeSettlsBean(),bean.getNetGrossInd());
	}
	
	public void pkgSettlInfo(Message message,TradeSettlsBean bean,String netGrossInd)throws Exception{
		ExecutionReport.NoPartyIDs own = new ExecutionReport.NoPartyIDs();
		own.set(new PartyRole(PartyRole.EXECUTING_FIRM));
		// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		own.set(new PartyID(getConfigBean().getBankId()));
		// 本方资金账户货币
		own.set(new AccountCurrency(bean.getSettccy()));
		ExecutionReport.NoPartyIDs own2 = new ExecutionReport.NoPartyIDs();
		own2.set(new PartyRole(PartyRole.EXECUTING_FIRM));
		// 本方机构21位码,如果不传输机构1位码，此域作为协议必须域，请填写“-”
		own2.set(new PartyID(getConfigBean().getBankId()));
		// 本方资金账户货币
		own2.set(new AccountCurrency(bean.getcSettccy()));
		ExecutionReport.NoPartyIDs cust = new ExecutionReport.NoPartyIDs();
		// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		cust.set(new PartyRole(PartyRole.CONTRA_FIRM));
		cust.set(new PartyID(bean.getcInstitutionId()));
		// 对方资金账户货币
		cust.set(new AccountCurrency(bean.getcSettccy()));
		ExecutionReport.NoPartyIDs cust2 = new ExecutionReport.NoPartyIDs();
		// 对手方机构 21位码,如果不传输机构 21位码，此域作为协议必须域，请填写“-”
		cust2.set(new PartyRole(PartyRole.CONTRA_FIRM));
		cust2.set(new PartyID(bean.getcInstitutionId()));
		cust2.set(new AccountCurrency(bean.getSettccy()));
		// 本方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(own,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getFullName());
		convert(own2,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getFullName());
		
		// 对方中文全称， 如果机构中文全称不传输， 此组域不传输
		convert(cust,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getcFullName());
		convert(cust2,PartySubIDType.FULL_LEGAL_CHINESE_NAME_OF_FIRM, bean.getcFullName());
		
		//如果交易方式为双边清算 (NetGrossInd=2)  则为双方传入账户信息  否则不需要传入这些域
		if("2".equals(netGrossInd))
		{
			//组装本方清算信息
			pkgSettls(own, bean.getSettccy(), bean.getBankname(), bean.getBankopenno(), bean.getBankacctno(), 
					bean.getAcctname(), bean.getAcctopenno(), bean.getAcctno(), 
					bean.getIntermediarybankname(), bean.getIntermediarybankbiccode(), bean.getIntermediarybankacctno(), bean.getRemark());
			
			pkgSettls(own2, bean.getcSettccy(), bean.getFarBankname(), bean.getFarBankopenno(), bean.getFarBankacctno(), 
					bean.getFarAcctname(), bean.getFarAcctopenno(), bean.getFarAcctno(), 
					bean.getFarIntermediarybankname(), bean.getFarIntermediarybankbiccode(), bean.getFarIntermediarybankacctno(), bean.getFarRemark());
			
			//组装对手方清算信息
			pkgSettls(cust, bean.getcSettccy(), bean.getcBankname(), bean.getcBankopenno(), bean.getcBankacctno(), 
					bean.getcAcctname(), bean.getcAcctopenno(), bean.getcAcctno(), 
					bean.getcIntermediarybankname(), bean.getcIntermediarybankbiccode(), bean.getcIntermediarybankacctno(), bean.getcRemark());
			
			pkgSettls(cust2, bean.getSettccy(), bean.getcFarBankname(), bean.getcFarBankopenno(), bean.getcFarBankacctno(), 
					bean.getcFarAcctname(), bean.getcFarAcctopenno(), bean.getcFarAcctno(), 
					bean.getcFarIntermediarybankname(), bean.getcFarIntermediarybankbiccode(), bean.getcFarIntermediarybankacctno(), bean.getcFarRemark());
			
		}
		message.addGroup(own);
		message.addGroup(own2);
		message.addGroup(cust);
		message.addGroup(cust2);
	}
	
	public void pkgSettls(ExecutionReport.NoPartyIDs iDs,String settccy,String bankname,String bankopenno,String bankacctno,
			String acctname,String acctopenno,String acctno,
			String intermediarybankname,String intermediarybankbiccode,String intermediarybankacctno,
			String remark){
		if("CNY".equals(settccy)){
			// 资金开户行 该字段必填 需要匹配 110
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME, bankname);
			// 资金账户户名 该字段必填需匹配 23
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME, acctname);
			// 本方系统行号  该字段必填需匹配 16
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE, bankacctno);
			//本方资金账号 必填需匹配 15
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER, acctno);
			// 本方附言   选填   若填值则需要匹配 139
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, remark);
			
		}else{
			// 本方开户行名称  选填 110
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_NAME, bankname);
			// 本方收款行名称 23
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NAME, acctname);
			// 本方收款行BICCODE 16
			convert(iDs,PartySubIDType.SWIFT_BIC_CODE, acctopenno);
			// 收款行账号 15
			convert(iDs,PartySubIDType.CASH_ACCOUNT_NUMBER, acctno);
			// 本方附言   选填   若填值则需要匹配 139
			convert(iDs,PartySubIDType.SETTLEMENT_CURRENCY_REMARK, remark);
			
			// 中间行名称 209
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK, intermediarybankname);
			// 中间行BICCODE 211
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK_BIC_CODE, intermediarybankbiccode);
			// 中间行账号 213
			convert(iDs,PartySubIDType.INTERMEDIARY_BANK_ACCOUNT, intermediarybankacctno);
			
			// 资金开户行账号 207
			convert(iDs,PartySubIDType.CORRESPONDENT_BANK_ACCOUNT, bankacctno);
			// 开户行BICCODE 166
			convert(iDs,PartySubIDType.SETTLEMENT_BANK_BIC_CODE, bankopenno);
		}
	}

	@Override
	public void pkgMsgBody(Message message, List<SendForeignOptionModel> beans)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
