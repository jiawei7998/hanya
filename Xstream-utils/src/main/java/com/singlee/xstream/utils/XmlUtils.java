package com.singlee.xstream.utils;

import com.singlee.xstream.bean.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Field;

/**
 * XML与Java对象互转工具类
 *
 * @author chenxh
 */
public class XmlUtils {

    /**
     * 初始化XStream对象,并加载相信配置信息
     *
     * @param config
     * @return
     */
    private static <T extends XmlBaseBean> XStream getInstance(XstreamConfig config) {

        // 初始化XStream对象
        XStream xstream = new XstreamFilterDefinedMember(new XppDriver(new XmlFriendlyNameCoder("_-", "_")));
        //设置reference模型,不引用
        xstream.setMode(XStream.NO_REFERENCES);
        // 默认的安全级别
        XStream.setupDefaultSecurity(xstream);
        //允许包下所有的类型
        xstream.allowTypesByWildcard(new String[]{"com.singlee.**"});
        // 初始化XStream对象的别名配置
        initXstreamConfig(xstream, config);

        return xstream;
    }

    /**
     * 方法重载.将XML文件(字符串)转换成JAVA对象
     *
     * @param <T> 需要转换的java对象
     * @param xml XML文件(字符串型式)
     * @param dto 指定XML文件转换的JAVA对象类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     * @throws Exception
     */
    public static <T extends XmlBaseBean> T fromXML(String xml, T dto) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        if (null == xml) {
            throw new NullPointerException("xml content is null");
        }
        // 将XML文件转换成JAVA对象
        return fromXML(new StringReader(xml), dto);
    }

    /**
     * 方法重载.将XML文件(Reader)转换成JAVA对象
     *
     * @param <T>    需要转换的java对象
     * @param reader XML文件(Reader的型式)
     * @param dto    指定XML文件转换的JAVA对象类型
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static <T extends XmlBaseBean> T fromXML(Reader reader, T dto) throws InstantiationException, IllegalAccessException, ClassNotFoundException {

        if (null == reader) {
            throw new NullPointerException("xml reader is null");
        }

        if (null == dto) {
            throw new NullPointerException("JavaBean is null of convert to javaBean");
        }

        // 初始化第三方XML工具类
        XStream xstream = getInstance(dto.initXstreamConfig());

        // 将XML文件转换成JAVA对象
        return (T) xstream.fromXML(reader);
    }

    /**
     * 将对象转换成XML文件(字符串)
     *
     * @param <T> 需要转换的java对象
     * @param dto 指定转成XML文件的DTO对象
     * @return
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     * @throws Exception
     */
    public static <T extends XmlBaseBean> String toXml(T dto) throws IllegalArgumentException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        if (null == dto) {
            throw new NullPointerException("JavaBean is null of convert xml file");
        }

        // 初始化第三方XML工具类
        XStream xstream = getInstance(dto.initXstreamConfig());
        // 过滤空值
        filterNull(dto, xstream, "singlee");

        // 返回XML文件(字符串)
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + xstream.toXML(dto);
    }

    /**
     * 方法重载.初始化XStream的别名配置
     *
     * @param xstream
     * @throws Exception
     */
    private static void initXstreamConfig(XStream xstream, XstreamConfig config) {

        // 设置JAVA BEAN为根节点
        if (null != config && null != config.getAliasClass()) {
            for (AliasClass var : config.getAliasClass()) {
                xstream.alias(var.getLableName().trim(), var.getClassType());
            }
        }

        /*
         * 指定List属性标签的显示名称
         */
        if (null != config && null != config.getAliasList()) {
            for (AliasList list : config.getAliasList()) {
                xstream.addImplicitCollection(list.getListClassType(), list.getListFieldName(), list.getAliasListItemName(), list.getListItemClassType());
            }
        }

        /*
         * 添加XML标签、接口/抽象类、实现类的映射关系
         */
        if (null != config && null != config.getAliasInterface()) {
            for (AliasInterfaceClass interfaceClass : config.getAliasInterface()) {
                xstream.alias(interfaceClass.getFieldName(), interfaceClass.getInterfaceType(), interfaceClass.getActualClassType());
            }
        }
        /*
         * 添加XML标签、属性、实现类的映射关系
         */
        if (null != config && null != config.getAliasFiled()) {
            for (AliasField aliasField : config.getAliasFiled()) {
                xstream.aliasField(aliasField.getAliasName(), aliasField.getDefinedInClass(), aliasField.getFieldName());
            }
        }
        /*
         * 过滤指定类的某个属性
         */
        if (null != config && null != config.getOmitField()) {
            for (OmitField omitField : config.getOmitField()) {
                xstream.omitField(omitField.getClassType(), omitField.getFieldName());
            }
        }
        /*
         * 设置接点属性
         */
        if (null != config && null != config.getAttrs()) {
            for (Attribute attr : config.getAttrs()) {
                xstream.useAttributeFor(attr.getClassType(), attr.getaName());
            }
        }
        /*
         * 设置自定义转换器
         */
        if (null != config && null != config.getConvertField()) {
            for (ConvertField convertField : config.getConvertField()) {
                xstream.registerLocalConverter(convertField.getDefinedClass(), convertField.getFiledName(), convertField.getConverter());
            }
        }
    }

    /**
     * 过滤所有属性值为空的标签
     *
     * @param dto
     * @param xstream
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    private static void filterNull(Object dto, XStream xstream, String... packageName) throws IllegalArgumentException, IllegalAccessException {
        String proName = null;
        // 获取当前对象的类型
        Class<?> clz = dto.getClass();

        // 当前类中所有属性名称
        Field[] fields = clz.getDeclaredFields();

        /*
         * 循环读取每个属性
         */
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            // 属性名称
            proName = field.getName();

            // 设置属性可以使用反射安全访问
            field.setAccessible(true);

            // 属性值
            Object obj = field.get(dto);
            /*
             * 属性为空则不生成标签
             */
            if (obj == null) {
                // 设置需要过滤的属性
                xstream.omitField(clz, proName);
            } else {
                // 过滤指定包名下面的自定义对象
                if (null != packageName) {
                    for (String var : packageName) {
                        // 递归自定义对象属性
                        if (clz.getName().contains(var)) {
                            filterNull(obj, xstream);
                        }// end if
                    }// end for
                }// end if
            }// end if else

            // 设置属性不能使用反射访问
            field.setAccessible(false);
        }// end for
    }

    /**
     * 使用XSD校验xml文件
     *
     * @throws IOException
     * @throws SAXException
     */
    public static void validate(String schemaLocaltion, String xml) throws SAXException, IOException {
        Log log = LogFactory.getLog(XmlUtils.class);
        log.info("进入XSD报文解析效验...");

        // 获取Schema工厂类
        // 这里的XMLConstants.W3C_XML_SCHEMA_NS_URI的值就是：//http://www.w3.org/2001/XMLSchema
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        // 获取XSD文件，以流的方式读取到Source中
        // XSD文件的位置相对于类文件位置
        log.info("获取XSD文件，以流的方式读取到Source中");
        Source schemaSource = new StreamSource(new FileInputStream(schemaLocaltion));

        Schema schema = factory.newSchema(schemaSource);
        // 这里是将一个DOM树对象转换成流对象，以便对DOM树对象验证

        // 获取验证器，验证器的XML Schema源就是之前创建的Schema
        log.info("初始化 validator对象");
        Validator validator = schema.newValidator();

        Source source = new StreamSource(new StringReader(xml));

        // 执行验证
        log.info("XSD执行验证...");
        validator.validate(source);
        log.info("XSD验证完成");

    }
}
