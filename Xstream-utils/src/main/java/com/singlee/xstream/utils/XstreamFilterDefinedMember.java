package com.singlee.xstream.utils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConverterLookup;
import com.thoughtworks.xstream.converters.ConverterRegistry;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.core.ClassLoaderReference;
import com.thoughtworks.xstream.io.HierarchicalStreamDriver;
import com.thoughtworks.xstream.mapper.Mapper;
import com.thoughtworks.xstream.mapper.MapperWrapper;

/**
 * 自动过滤XML报文中,未在实体类中定义的属性
 * @author chenxh
 *
 */
public class XstreamFilterDefinedMember extends XStream {

	public XstreamFilterDefinedMember() {
		super();
	}

	public XstreamFilterDefinedMember(HierarchicalStreamDriver hierarchicalStreamDriver) {
		super(hierarchicalStreamDriver);
	}

	public XstreamFilterDefinedMember(ReflectionProvider reflectionProvider, HierarchicalStreamDriver driver, ClassLoaderReference classLoaderReference, Mapper mapper,
			ConverterLookup converterLookup, ConverterRegistry converterRegistry) {
		super(reflectionProvider, driver, classLoaderReference, mapper, converterLookup, converterRegistry);
	}

	public XstreamFilterDefinedMember(ReflectionProvider reflectionProvider, HierarchicalStreamDriver driver, ClassLoaderReference classLoaderReference, Mapper mapper) {
		super(reflectionProvider, driver, classLoaderReference, mapper);
	}

	public XstreamFilterDefinedMember(ReflectionProvider reflectionProvider, HierarchicalStreamDriver driver, ClassLoaderReference classLoaderReference) {
		super(reflectionProvider, driver, classLoaderReference);
	}

	public XstreamFilterDefinedMember(ReflectionProvider reflectionProvider, HierarchicalStreamDriver hierarchicalStreamDriver) {
		super(reflectionProvider, hierarchicalStreamDriver);
	}

	public XstreamFilterDefinedMember(ReflectionProvider reflectionProvider) {
		super(reflectionProvider);
	}

	@Override
	protected MapperWrapper wrapMapper(MapperWrapper next) {
		return new MapperWrapper(next){

			@SuppressWarnings("unchecked")
			@Override
			public boolean shouldSerializeMember(Class definedIn, String fieldName) {
				// 不能识别的节点掠过 
                if (definedIn == Object.class) { 
                     return false; 
                } 
                
				return super.shouldSerializeMember(definedIn, fieldName);
			}
			
			
		};
	}

}
