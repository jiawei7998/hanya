package com.singlee.xstream.bean;

import java.io.Serializable;

import com.thoughtworks.xstream.converters.Converter;

public class ConvertField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9010724484855356789L;

	private String filedName;
	
	private Class<?> definedClass;
	
	private Converter converter;

	public String getFiledName() {
		return filedName;
	}

	public void setFiledName(String filedName) {
		this.filedName = filedName;
	}

	public Class<?> getDefinedClass() {
		return definedClass;
	}

	public void setDefinedClass(Class<?> definedClass) {
		this.definedClass = definedClass;
	}

	public Converter getConverter() {
		return converter;
	}

	public void setConverter(Converter converter) {
		this.converter = converter;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ConvertField(Class<?> definedClass,String filedName,  Converter converter) {
		super();
		this.filedName = filedName;
		this.definedClass = definedClass;
		this.converter = converter;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConvertField [converter=");
		builder.append(converter);
		builder.append(", definedClass=");
		builder.append(definedClass);
		builder.append(", filedName=");
		builder.append(filedName);
		builder.append("]");
		return builder.toString();
	}
	
}
