package com.singlee.xstream.bean;

public enum AliasTypeEnum {
	/**
	 * 类对象别名
	 */
	AliasClass,
	
	/**
	 * 对象属性别名
	 */
	AliasField,
	
	/**
	 * 抽象类/接口对应实现类别名
	 */
	AliasInterfaceClass,
	
	/**
	 * List对象别名
	 */
	AliasList,
	
	/**
	 * 过滤
	 */
	OmitField,
	
	/**
	 * 指定的属情使用特定的转换器
	 */
	ConvertField,
	
	/**
	 * 反序列化
	 */
	Unmarshal,
	
	/**
	 * 指定属性为标签属性
	 */
	Attribute,
}
