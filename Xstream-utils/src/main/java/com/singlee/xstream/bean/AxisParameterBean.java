package com.singlee.xstream.bean;

import java.io.Serializable;

import javax.xml.namespace.QName;

public class AxisParameterBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2039568763868598050L;

	private String paramName;
	
	private QName xmlType;
	
	private String paramValue;
	
	public AxisParameterBean(String paramName,QName xmlType,String paramValue){
		this.paramName = paramName;
		this.xmlType = xmlType;
		this.paramValue = paramValue;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public QName getXmlType() {
		return xmlType;
	}

	public void setXmlType(QName xmlType) {
		this.xmlType = xmlType;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AxisParameterBean [paramName=");
		builder.append(paramName);
		builder.append(", paramValue=");
		builder.append(paramValue);
		builder.append(", xmlType=");
		builder.append(xmlType);
		builder.append("]");
		return builder.toString();
	}
	
	
}
