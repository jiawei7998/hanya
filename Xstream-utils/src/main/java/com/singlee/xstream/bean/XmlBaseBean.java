package com.singlee.xstream.bean;

import java.io.Serializable;

/**
 * XML文件与java对象互换,java对象的父类
 * 
 * @author chenxh
 * 
 */
public abstract class XmlBaseBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -412152088589282754L;


	/**
	 * 添加类的别名,集合类型的别名
	 */
	public abstract XstreamConfig initXstreamConfig() throws InstantiationException, IllegalAccessException, ClassNotFoundException;


}
