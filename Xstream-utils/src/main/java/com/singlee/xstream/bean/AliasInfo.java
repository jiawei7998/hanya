package com.singlee.xstream.bean;

import java.io.Serializable;

import com.thoughtworks.xstream.converters.Converter;

public class AliasInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1726367551223910084L;

	/**
	 * 类属性名称
	 */
	private String fieldName;
	
	/**
	 * XML标签显示名称
	 */
	private String aliasName;
	
	/**
	 * 接口/抽象类/父类
	 */
	private Class<?> definedClass;
	
	/**
	 * 属性所在的实现类
	 */
	private Class<?> implClass;
	
	/**
	 * 别名转换的类型
	 */
	private AliasTypeEnum aliasType;
	
	/**
	 * 自定义转换器
	 */
	private Converter converter;
	
	/**
	 * 实现类对应的别名
	 */
	private String aliasImplClassName;
	
	/**
	 * 已重载.构造方法,为List集合添加别名
	 * @param fieldName
	 * @param aliasName
	 * @param definedClass
	 * @param implClass
	 * @param aliasType
	 */
	public AliasInfo(String fieldName,String aliasName,Class<?> implClass,String aliasImplClassName,AliasTypeEnum aliasType) {
		super();
		this.fieldName = fieldName;
		this.implClass = implClass;
		this.aliasType = aliasType;
		this.aliasName = aliasName;
		this.aliasImplClassName = aliasImplClassName;
	}
	
	/**
	 * 已重载.构造方法,为List集合添加别名
	 * @param fieldName
	 * @param aliasName
	 * @param definedClass
	 * @param implClass
	 * @param aliasType
	 */
	public AliasInfo(Class<?> definedClass, String fieldName,Converter converter, AliasTypeEnum aliasType) {
		super();
		this.fieldName = fieldName;
		this.definedClass = definedClass;
		this.aliasType = aliasType;
		this.converter = converter;
	}

	/**
	 * 已重载.构造方法,为List集合添加别名
	 * @param fieldName
	 * @param aliasName
	 * @param definedClass
	 * @param implClass
	 * @param aliasType
	 */
	public AliasInfo(String fieldName, String aliasName, Class<?> definedClass, Class<?> implClass, AliasTypeEnum aliasType) {
		super();
		this.fieldName = fieldName;
		this.aliasName = aliasName;
		this.definedClass = definedClass;
		this.implClass = implClass;
		this.aliasType = aliasType;
	}
	
	/**
	 * 已重载.构造方法,指定抽象类/接口在XML标签中的class属性别名
	 * @param fieldName
	 * @param definedClass
	 * @param implClass
	 * @param aliasType
	 */
	public AliasInfo(String fieldName,Class<?> definedClass, Class<?> implClass, AliasTypeEnum aliasType) {
		super();
		this.fieldName = fieldName;
		this.definedClass = definedClass;
		this.implClass = implClass;
		this.aliasType = aliasType;
	}
	
	/**
	 * 已重载.构造方法,为对象指定属性添加别名
	 * @param fieldName
	 * @param aliasName
	 * @param definedClass
	 * @param aliasType
	 */
	public AliasInfo(String fieldName, String aliasName, Class<?> definedClass, AliasTypeEnum aliasType) {
		super();
		this.fieldName = fieldName;
		this.aliasName = aliasName;
		this.definedClass = definedClass;
		this.aliasType = aliasType;
	}
	
	/**
	 * 已重载.构造方法,为对象添加别名
	 * @param aliasName
	 * @param definedClass
	 * @param aliasType
	 */
	public AliasInfo(String aliasName,Class<?> definedClass, AliasTypeEnum aliasType) {
		super();
		this.aliasName = aliasName;
		this.definedClass = definedClass;
		this.aliasType = aliasType;
	}
	
	/**
	 * 已重载.构造方法,过滤对象指定属性
	 * @param definedClass
	 * @param fieldName
	 * @param aliasType
	 */
	public AliasInfo(Class<?> definedClass,String fieldName, AliasTypeEnum aliasType) {
		super();
		this.fieldName = fieldName;
		this.definedClass = definedClass;
		this.aliasType = aliasType;
	}

	public String getAliasImplClassName() {
		return aliasImplClassName;
	}

	public void setAliasImplClassName(String aliasImplClassName) {
		this.aliasImplClassName = aliasImplClassName;
	}

	public AliasTypeEnum getAliasType() {
		return aliasType;
	}

	public void setAliasType(AliasTypeEnum aliasType) {
		this.aliasType = aliasType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public Class<?> getDefinedClass() {
		return definedClass;
	}

	public void setDefinedClass(Class<?> definedClass) {
		this.definedClass = definedClass;
	}

	public Class<?> getImplClass() {
		return implClass;
	}

	public void setImplClass(Class<?> implClass) {
		this.implClass = implClass;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Converter getConverter() {
		return converter;
	}

	public void setConverter(Converter converter) {
		this.converter = converter;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AliasInfo [aliasImplClassName=");
		builder.append(aliasImplClassName);
		builder.append(", aliasName=");
		builder.append(aliasName);
		builder.append(", aliasType=");
		builder.append(aliasType);
		builder.append(", converter=");
		builder.append(converter);
		builder.append(", definedClass=");
		builder.append(definedClass);
		builder.append(", fieldName=");
		builder.append(fieldName);
		builder.append(", implClass=");
		builder.append(implClass);
		builder.append("]");
		return builder.toString();
	}
	
}
