package com.singlee.xstream.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.converters.Converter;

/**
 * 第三方插件Xstream对象别名映射配置类
 * @author chenxh
 *
 */
public class XstreamConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3263967627798191368L;

	/**
	 * 根据类取别名
	 */
	private List<AliasClass> aliasClass;
	
	/**
	 * List对象取别名
	 */
	private List<AliasList> aliasList;

	/**
	 * XML标签、接口/抽象类、实现类的映射关系
	 */
	private List<AliasInterfaceClass> aliasInterface;
	
	/**
	 * XML标签、属性、实现类的映射关系
	 */
	private List<AliasField> aliasFiled;
	
	/**
	 * 过滤指定类的某个属性
	 */
	private List<OmitField> omitField;
	
	/**
	 * 添加节点属性
	 */
	private List<Attribute> attrs;//add
	
	/**
	 * 指定属性为自定义转换器
	 */
	private List<ConvertField> convertField;
	
	
	/**
	 * 方法已重载.指定对象属性为节点属性并取别名
	 * @param clasType
	 * @param fieldName
	 * @param aliasFileName
	 */
	public void addattrs(Class<?> clasType,String fieldName,String aliasFileName){
		addattrs(clasType,fieldName);
		addAliasFiled(fieldName,aliasFileName,clasType);
	}
	
	/**
	 * 方法已重载.指定对象属性为节点属性
	 * @param clasType
	 * @param fieldName
	 */
	public void addattrs(Class<?> clasType,String fieldName){
		addattrs(new Attribute(fieldName,clasType));
	}
	
	/**
	 * 方法已重载.添加属性节点
	 * @param attrs
	 */
	public void addattrs(Attribute attrs){
		if(this.attrs == null){
			this.attrs = new ArrayList<Attribute>();
		}
		this.attrs.add(attrs);
	}
	
	/**
	 * 方法已重载.添加自定义转换器
	 * @param definedClass
	 * @param filedName
	 * @param converter
	 */
	public void addConvertField(Class<?> definedClass,String filedName,Converter converter){
		addConvertField(new ConvertField(definedClass,filedName,converter));
	}
	
	/**
	 * 方法已重载.添加自定义转换器
	 * @param convertField
	 */
	public void addConvertField(ConvertField convertField){
		if(this.convertField == null){
			this.convertField = new ArrayList<ConvertField>();
		}
		this.convertField.add(convertField);
	}
	
	/**
	 * 方法重载.添加指定类属性别名
	 * @param fieldName
	 * @param aliasName
	 * @param fieldClassType
	 */
	public void addAliasFiled(String fieldName,String aliasName,Class<?> fieldClassType){
		if(this.aliasFiled == null){
			this.aliasFiled = new ArrayList<AliasField>();
		}
		this.aliasFiled.add(new AliasField(fieldName,aliasName,fieldClassType));
	}
	
	/**
	 * 方法重载.添加指定过滤类的属性
	 * @param clasType	过滤的类
	 * @param fieldName	过滤类中的属性
	 * @throws ClassNotFoundException 
	 */
	public void addOmitField(Class<?> clasType,String fieldName){
		addOmitField(new OmitField(clasType,fieldName));
	}
	
	/**
	 * 方法重载.添加指定过滤类的属性
	 * @param aliasClass XML标签与类的映射关系实体类
	 */
	public void addOmitField(OmitField omitField){
		if(this.omitField == null){
			this.omitField = new ArrayList<OmitField>();
		}
		this.omitField.add(omitField);
	}
	
	/**
	 * 方法重载.添加接口/抽象类与实现类的关系映射
	 * @param fieldName	属性名称
	 * @param interfaceClasType	接口或者抽象类
	 * @param actualClassType	接口/抽象类的实现类
	 */
	public void addAliasInterfaceClass(String fieldName,Class<?> interfaceClasType,Class<?> actualClassType){
		addAliasInterfaceClass(new AliasInterfaceClass(fieldName,interfaceClasType,actualClassType));
	}
	
	/**
	 * 方法重载.添加接口/抽象类与实现类的关系映射
	 * @param aliasInterfaceClass XML标签、接口/抽象类、实现类的映射关系实体类
	 */
	public void addAliasInterfaceClass(AliasInterfaceClass aliasInterfaceClass){
		if(this.aliasInterface == null){
			this.aliasInterface = new ArrayList<AliasInterfaceClass>();
		}
		this.aliasInterface.add(aliasInterfaceClass);
	}
	
	/**
	 * 方法重载.添加类的别名
	 * @param lableName	标签名称
	 * @param clasType	标签对应的类
	 */
	public void addAliasClass(String lableName,Class<?> clasType){
		addAliasClass(new AliasClass(lableName,clasType));
	}
	
	/**
	 * 方法重载.添加类的别名
	 * @param aliasClass XML标签与类的映射关系实体类
	 */
	public void addAliasClass(AliasClass aliasClass){
		if(this.aliasClass == null){
			this.aliasClass = new ArrayList<AliasClass>();
		}
		this.aliasClass.add(aliasClass);
	}
	
	/**
	 * 方法重载.添加集合属性别名配置
	 * @param listClasType	包含List属性的类
	 * @param listFieldName	 LIST集合对应的属性名称
	 * @param aliasListItemName	List集合中的对象生成xml文件时使用的标签名称
	 * @param listItemClassType	List集合中的对象类型
	 */
	public void addAliasList(Class<?> listClasType,String listFieldName,String aliasListItemName,Class<?> listItemClassType){
		addAliasList(new AliasList(listClasType,listFieldName,aliasListItemName,listItemClassType));
	}
	
	/**
	 * 方法重载.添加集合属性别名
	 * @param aliasList	集合与别名映射关系实体类
	 */
	public void addAliasList(AliasList aliasList){
		if(this.aliasList == null){
			this.aliasList = new ArrayList<AliasList>();
		}
		this.aliasList.add(aliasList);
	}
	
	/**
	 * 将aliasInfo对象中转换别名的数据添加到XstreamConfig对象中
	 * @param config
	 * @param aliasInfos
	 */
	public void addAliasInfo(List<AliasInfo> aliasInfos){
		
		if(null == aliasInfos || aliasInfos.size() == 0)
			return;
		
		for (AliasInfo aliasInfo : aliasInfos) {
			/*
			 * 设置JAVA BEAN为根节点
			 */
			if (AliasTypeEnum.AliasClass.equals(aliasInfo.getAliasType())) {
				// 指定报文的根
				addAliasClass(aliasInfo.getAliasName(), aliasInfo.getDefinedClass());
			}else if(AliasTypeEnum.AliasField.equals(aliasInfo.getAliasType())){
				//指定对象属性别名
				addAliasFiled(aliasInfo.getFieldName(),aliasInfo.getAliasName(),aliasInfo.getDefinedClass());
			}else if(AliasTypeEnum.AliasInterfaceClass.equals(aliasInfo.getAliasType())){
				addAliasInterfaceClass(aliasInfo.getFieldName(),aliasInfo.getDefinedClass(),aliasInfo.getImplClass());
			}else if(AliasTypeEnum.AliasList.equals(aliasInfo.getAliasType())){
				addAliasList(aliasInfo.getDefinedClass(),aliasInfo.getFieldName(),aliasInfo.getAliasName(),aliasInfo.getImplClass());
			}else if(AliasTypeEnum.OmitField.equals(aliasInfo.getAliasType())){
				// 该属性不用生成标签,必须过滤
				addOmitField(aliasInfo.getDefinedClass(), aliasInfo.getAliasName());
			}else if(AliasTypeEnum.ConvertField.equals(aliasInfo.getAliasType())){
				// 该置自定义转换器
				addConvertField(aliasInfo.getDefinedClass(),aliasInfo.getFieldName(),aliasInfo.getConverter());
			}else if(AliasTypeEnum.Attribute.equals(aliasInfo.getAliasType())){
				// 指定属性为标签属性
				addattrs(aliasInfo.getDefinedClass(), aliasInfo.getFieldName(), aliasInfo.getAliasName());
			}//end if else if...//end if else if...
		}//end for
	}

	public List<AliasInterfaceClass> getAliasInterface() {
		return aliasInterface;
	}

	public void setAliasInterface(List<AliasInterfaceClass> aliasInterface) {
		this.aliasInterface = aliasInterface;
	}

	public List<AliasClass> getAliasClass() {
		return aliasClass;
	}

	public void setAliasClass(List<AliasClass> aliasClass) {
		this.aliasClass = aliasClass;
	}

	public List<AliasList> getAliasList() {
		return aliasList;
	}

	public void setAliasList(List<AliasList> aliasList) {
		this.aliasList = aliasList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<OmitField> getOmitField() {
		return omitField;
	}

	public void setOmitField(List<OmitField> omitField) {
		this.omitField = omitField;
	}

	 
	public List<Attribute> getAttrs() {
		return attrs;
	}

	public void setAttrs(List<Attribute> attrs) {
		this.attrs = attrs;
	}

	public List<AliasField> getAliasFiled() {
		return aliasFiled;
	}

	public void setAliasFiled(List<AliasField> aliasFiled) {
		this.aliasFiled = aliasFiled;
	}

	public List<ConvertField> getConvertField() {
		return convertField;
	}

	public void setConvertField(List<ConvertField> convertField) {
		this.convertField = convertField;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("XstreamConfig [aliasClass=");
		builder.append(aliasClass);
		builder.append(", aliasFiled=");
		builder.append(aliasFiled);
		builder.append(", aliasInterface=");
		builder.append(aliasInterface);
		builder.append(", aliasList=");
		builder.append(aliasList);
		builder.append(", attrs=");
		builder.append(attrs);
		builder.append(", convertField=");
		builder.append(convertField);
		builder.append(", omitField=");
		builder.append(omitField);
		builder.append("]");
		return builder.toString();
	}

}
