package com.singlee.xstream.bean;

import java.io.Serializable;

/**
 * 集合属性与标签映射关系类
 * <p>
 * 报文中出现多次的标签使用List集合,list集合的属性生成xml标签时默认使用的是包名+类名,
 * 这时需要设置该属性xml文件标签的显示名称
 * </p>
 *  
 * @author chenxh
 *
 */
public class AliasList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -270351688829840692L;
	
	/**
	 * 包含List属性的类
	 */
	private Class<?> listClassType;
	
	/**
	 * LIST集合对应的属性名称
	 */
	private String listFieldName;
	
	/**
	 * List集合中的对象生成xml文件时使用的标签名称
	 */
	private String aliasListItemName;
	
	/**
	 * List集合中的对象类型
	 */
	private Class<?> listItemClassType;

	/**
	 * 带参构造方法
	 * @param listClassType
	 * @param listFieldName
	 * @param aliasListItemName
	 * @param listItemClassType
	 */
	public AliasList(Class<?> listClassType, String listFieldName, String aliasListItemName, Class<?> listItemClassType) {
		super();
		this.listClassType = listClassType;
		this.listFieldName = listFieldName;
		this.aliasListItemName = aliasListItemName;
		this.listItemClassType = listItemClassType;
	}

	public Class<?> getListClassType() {
		return listClassType;
	}

	public void setListClassType(Class<?> listClassType) {
		this.listClassType = listClassType;
	}

	public String getListFieldName() {
		return listFieldName;
	}

	public void setListFieldName(String listFieldName) {
		this.listFieldName = listFieldName;
	}

	public String getAliasListItemName() {
		return aliasListItemName;
	}

	public void setAliasListItemName(String aliasListItemName) {
		this.aliasListItemName = aliasListItemName;
	}

	public Class<?> getListItemClassType() {
		return listItemClassType;
	}

	public void setListItemClassType(Class<?> listItemClassType) {
		this.listItemClassType = listItemClassType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AliasList [aliasListItemName=");
		builder.append(aliasListItemName);
		builder.append(", listClassType=");
		builder.append(listClassType);
		builder.append(", listFieldName=");
		builder.append(listFieldName);
		builder.append(", listItemClassType=");
		builder.append(listItemClassType);
		builder.append("]");
		return builder.toString();
	}
	
	
}
