package com.singlee.xstream.bean;

import java.io.Serializable;

/**
 * XML标签与类的映射关系,当标签与属性名不同时需要设值,根节点必须设值
 * @author chenxh
 *
 */
public class AliasClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2255369020675512572L;

	/**
	 * xml标签别名
	 */
	private String lableName;
	
	/**
	 * 对应xml标签的类
	 */
	private Class<?> classType;
	
	
	/**
	 * 带参构造方法
	 * @param lableName
	 * @param classType
	 */
	public AliasClass(String lableName, Class<?> classType) {
		super();
		this.lableName = lableName;
		this.classType = classType;
	}

	public String getLableName() {
		return lableName;
	}

	public void setLableName(String lableName) {
		this.lableName = lableName;
	}

	public Class<?> getClassType() {
		return classType;
	}

	public void setClassType(Class<?> classType) {
		this.classType = classType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AliasClass [classType=");
		builder.append(classType);
		builder.append(", lableName=");
		builder.append(lableName);
		builder.append("]");
		return builder.toString();
	}
	
	
}
