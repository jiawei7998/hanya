package com.singlee.xstream.bean;

import java.io.Serializable;

/**
 * XML标签、接口/抽象类、实现类的映射关系实体类
 * @author chenxh
 *
 */
public class AliasInterfaceClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7321369255610497289L;

	/**
	 * 属性名称
	 */
	private String fieldName;
	
	/**
	 * 接口或者抽象类
	 */
	private Class<?> interfaceType;
	
	/**
	 * 接口/抽象类的实现类
	 */
	private Class<?> actualClassType;

	/**
	 * 带参构造方法
	 * @param fieldName	属性名称
	 * @param interfaceType	接口或者抽象类
	 * @param actualClassType	接口/抽象类的实现类
	 */
	public AliasInterfaceClass(String fieldName, Class<?> interfaceType, Class<?> actualClassType) {
		super();
		this.fieldName = fieldName;
		this.interfaceType = interfaceType;
		this.actualClassType = actualClassType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Class<?> getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(Class<?> interfaceType) {
		this.interfaceType = interfaceType;
	}

	public Class<?> getActualClassType() {
		return actualClassType;
	}

	public void setActualClassType(Class<?> actualClassType) {
		this.actualClassType = actualClassType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AliasInterfaceClass [actualClassType=");
		builder.append(actualClassType);
		builder.append(", fieldName=");
		builder.append(fieldName);
		builder.append(", interfaceType=");
		builder.append(interfaceType);
		builder.append("]");
		return builder.toString();
	}
	
}
