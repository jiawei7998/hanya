package com.singlee.xstream.bean;

import java.io.Serializable;

/**
 * XML标签与属性的映射关系,当标签与属性名不同时需要设值
 * @author chenxh
 *
 */
public class AliasField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8533535461377519062L;

	/**
	 * 属性名
	 */
	private String fieldName;
	
	/**
	 * 属性所在的实现类
	 */
	private Class<?> definedInClass;
	
	/**
	 * 属性别名
	 */
	private String aliasName;

	public AliasField(String fieldName, String aliasName, Class<?> definedInClass) {
		super();
		this.fieldName = fieldName;
		this.definedInClass = definedInClass;
		this.aliasName = aliasName;
	}
	
	public AliasField() {
		super();
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Class<?> getDefinedInClass() {
		return definedInClass;
	}

	public void setDefinedInClass(Class<?> definedInClass) {
		this.definedInClass = definedInClass;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AliasField [aliasName=");
		builder.append(aliasName);
		builder.append(", definedInClass=");
		builder.append(definedInClass);
		builder.append(", fieldName=");
		builder.append(fieldName);
		builder.append("]");
		return builder.toString();
	}
	
	
}
