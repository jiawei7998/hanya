package test;

import java.io.Serializable;

/**
 * Esb报文体抽象类
 * @author chenxh
 *
 */
public abstract class EsbBodyPacket implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9057226295459382313L;

	/**
	 * 组装报文体的节点转换信息
	 * @return
	 */
	public abstract EsbSubPacketConvert getBodyConvert();
}
