package test.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import test.EsbAppHeaderPacket;
import test.EsbPacket;
import test.EsbSubPacketConvert;


public class AppHeaderDto extends EsbAppHeaderPacket {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4290464612083533403L;
	
	/**
	 * 服务编码
	 */
	private String AppCode;
	
	private List<ApprTellerDto> ApprTellers;
	
	private String AuthFlag;

	private List<AuthTellerDto> AuthTellers;
	
	private AuthTellerDto authTeller;
	
	private BigDecimal count;
	
	private Date dealDate;

	public Date getDealDate() {
		return dealDate;
	}

	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	public BigDecimal getCount() {
		return count;
	}

	public void setCount(BigDecimal count) {
		this.count = count;
	}

	public AuthTellerDto getAuthTeller() {
		return authTeller;
	}

	public void setAuthTeller(AuthTellerDto authTeller) {
		this.authTeller = authTeller;
	}

	public String getAuthFlag() {
		return AuthFlag;
	}

	public void setAuthFlag(String authFlag) {
		AuthFlag = authFlag;
	}

	public List<ApprTellerDto> getApprTellers() {
		return ApprTellers;
	}

	public void setApprTellers(List<ApprTellerDto> apprTeller) {
		ApprTellers = apprTeller;
	}

	public List<AuthTellerDto> getAuthTellers() {
		return AuthTellers;
	}

	public void setAuthTellers(List<AuthTellerDto> authTellers) {
		AuthTellers = authTellers;
	}

	public String getAppCode() {
		return AppCode;
	}

	public void setAppCode(String appCode) {
		AppCode = appCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public EsbSubPacketConvert getAppHeaderConvert() {
		EsbSubPacketConvert info = new EsbSubPacketConvert();
		
		info.setSubPacketAliasName("APP_HEADER");
		info.setSubPacketClassType(AppHeaderDto.class);
//		info.addAlias("ApprTellers", "array", AppHeaderDto.class);
//		info.addAlias("AuthTellers", "array", AppHeaderDto.class);
//		info.addAlias("ApprTellerArray", ApprTellerDto.class);
//		info.addAlias("AuthTellerArray", AuthTellerDto.class);
		info.addAlias("ApprTellers","array",ApprTellerDto.class,"ApprTellerArray");
		info.addAlias("AuthTellers","array",AuthTellerDto.class,"AuthTellerArray");
//		info.addAlias(AppHeaderDto.class, "ApprTellers", "array", ApprTellerDto.class);
//		info.addAlias(AppHeaderDto.class, "AuthTellers", "array", AuthTellerDto.class);
//		info.addAlias(AppHeaderDto.class, "ApprTellers", new ApprTellerConverter());
//		info.addAlias(AppHeaderDto.class, "AuthTellers", new AuthTellerConverter());
		info.addAlias(EsbPacket.class, "appHeader", new AppHeaderConvert(info));
		return info;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppHeaderDto [AppCode=");
		builder.append(AppCode);
		builder.append(", ApprTellers=");
		builder.append(ApprTellers);
		builder.append(", AuthFlag=");
		builder.append(AuthFlag);
		builder.append(", AuthTellers=");
		builder.append(AuthTellers);
		builder.append(", authTeller=");
		builder.append(authTeller);
		builder.append(", count=");
		builder.append(count);
		builder.append(", dealDate=");
		builder.append(dealDate);
		builder.append("]");
		return builder.toString();
	}


}
