package test.dto;

import java.util.ArrayList;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class AuthTellerConverter implements Converter {

	@Override
	public void marshal(Object arg0, HierarchicalStreamWriter arg1, MarshallingContext arg2) {
		ArrayList<AuthTellerDto> list = (ArrayList<AuthTellerDto>) arg0;
//		arg1.startNode("array");
		for (AuthTellerDto authTellerDto : list) {
			arg1.startNode("AuthTellerArray");
			
			arg1.startNode("branchCode");
			arg1.setValue(authTellerDto.getBranchCode());
			arg1.endNode();
			arg1.endNode();
		}
//		arg1.endNode();
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader arg0, UnmarshallingContext arg1) {
		
		ArrayList<AuthTellerDto> list = new ArrayList<AuthTellerDto>();
		arg0.moveDown();
		arg0.moveDown();
		arg0.moveDown();
		AuthTellerDto authTellerDto = new AuthTellerDto();
		authTellerDto.setBranchCode(arg0.getValue());
		list.add(authTellerDto);
	
		arg0.moveUp();
		arg0.moveUp();
		arg0.moveUp();
		return list;
	}

	@Override
	public boolean canConvert(Class arg0) {
		return true;
	}

}
