package test.dto;

import java.io.Serializable;

public class SysHeaderRetDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -801120408340172439L;

	private String ReturnCode;
	
	private String ReturnMsg;

	public String getReturnMsg() {
		return ReturnMsg;
	}

	public void setReturnMsg(String returnMsg) {
		ReturnMsg = returnMsg;
	}

	public String getReturnCode() {
		return ReturnCode;
	}

	public void setReturnCode(String returnCode) {
		ReturnCode = returnCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
